package patterns.config.validations;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.ContentManager;

public class LeaseValidator extends CommonValidator {

	public final DTObject valildateLesseCode(DTObject input) {
		DTObject result = new DTObject();
		String lesseCode = input.get("SUNDRY_DB_AC");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(lesseCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(lesseCode, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, lesseCode);
			input.set(ContentManager.TABLE, "CUSTOMERACDTL");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			input.set(ContentManager.COLUMN, "SUNDRY_DB_AC");
			result = validateEntityWiseCodesWithoutEnabled(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateCustomerAgreement(DTObject input) {
		DTObject result = new DTObject();
		String customerID = input.get("CUSTOMER_ID");
		String agreementNo = input.get("AGREEMENT_NO");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(agreementNo)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(agreementNo, LENGTH_1, LENGTH_15)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.TABLE, "CUSTAGREEMENT");
			String columns[] = new String[] { getContext().getEntityCode(), customerID, agreementNo };
			input.setObject(ContentManager.PROGRAM_KEY, columns);
			result = validatePK(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateLeaseAssetSerial(DTObject input) {
		DTObject result = new DTObject();
		String leaseCode = input.get("LESSEE_CODE");
		String agreementNumber = input.get("AGREEMENT_NO");
		String scheduleID = input.get("SCHEDULE_ID");
		String assetSerial = input.get("ASSET_SL");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidNumber(assetSerial)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(assetSerial, LENGTH_1, LENGTH_6)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.TABLE, "LEASEASSETS");
			String columns[] = new String[] { getContext().getEntityCode(), leaseCode, agreementNumber, scheduleID, assetSerial };
			input.setObject(ContentManager.PROGRAM_KEY, columns);
			result = validatePKExcludeTBA(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateLeaseAssetComponentSerial(DTObject input) {
		DTObject result = new DTObject();
		String leaseCode = input.get("LESSEE_CODE");
		String agreementNumber = input.get("AGREEMENT_NO");
		String scheduleID = input.get("SCHEDULE_ID");
		String assetSerial = input.get("ASSET_SL");
		String assetCompSerial = input.get("ASSET_COMP_SL");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidNumber(assetCompSerial)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(assetCompSerial, LENGTH_1, LENGTH_6)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.TABLE, "LEASEASSETCOMP");
			String columns[] = new String[] { getContext().getEntityCode(), leaseCode, agreementNumber, scheduleID, assetSerial, assetCompSerial };
			input.setObject(ContentManager.PROGRAM_KEY, columns);
			result = validatePKExcludeTBA(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateScheduleID(DTObject input) {
		DTObject result = new DTObject();
		String leaseCode = input.get("LESSEE_CODE");
		String agreementNo = input.get("AGREEMENT_NO");
		String scheduleID = input.get("SCHEDULE_ID");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(scheduleID)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(scheduleID, LENGTH_1, LENGTH_15)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.TABLE, "LEASE");
			String columns[] = new String[] { getContext().getEntityCode(), leaseCode, agreementNo, scheduleID };
			input.setObject(ContentManager.PROGRAM_KEY, columns);
			result = validatePK(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}


	public final DTObject validateLesseeCode(DTObject input) {
		DTObject result = new DTObject();
		String lesseeCode = input.get("SUNDRY_DB_AC");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(lesseeCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(lesseeCode, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, lesseeCode);
			input.set(ContentManager.TABLE, "CUSTOMERACDTL");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			input.set(ContentManager.COLUMN, "SUNDRY_DB_AC");
			result = validateEntityWiseCodesWithoutEnabled(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateLeaseProductCode(DTObject input) {
		DTObject result = new DTObject();
		String leasePdtCode = input.get("LEASE_PRODUCT_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {

				if (!isValidCodePattern(leasePdtCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(leasePdtCode, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, leasePdtCode);
			input.set(ContentManager.TABLE, "LEASEPRODUCT");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			input.set(ContentManager.COLUMN, "LEASE_PRODUCT_CODE");
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validatePreLeaseContractReferenceSerial(DTObject input) {
		DTObject result = new DTObject();
		String contractDate = input.get("CONTRACT_DATE");
		String contractSl = input.get("CONTRACT_SL");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidNumber(contractSl)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_NUMBER);
					return result;
				}
				if (!isValidLength(contractSl, LENGTH_1, LENGTH_5)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.TABLE, "PRELEASECONTRACT");
			String columns[] = new String[] { getContext().getEntityCode(), contractDate, contractSl };
			input.setObject(ContentManager.PROGRAM_KEY, columns);
			result = validatePK(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateLeasePoInvPay(DTObject input) {
		DTObject result = new DTObject();
		String contractDate = input.get("CONTRACT_DATE");
		String contractSl = input.get("CONTRACT_SL");
		String paymentSl = input.get("PAYMENT_SL");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidNumber(contractSl)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_NUMBER);
					return result;
				}
				if (!isValidLength(contractSl, LENGTH_1, LENGTH_5)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.TABLE, "LEASEPOINVPAY");
			String columns[] = new String[] { getContext().getEntityCode(), contractDate, contractSl, paymentSl };
			input.setObject(ContentManager.PROGRAM_KEY, columns);
			result = validatePK(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}
	
	public final DTObject validateLeasePoInv(DTObject input) {
		DTObject result = new DTObject();
		String contractDate = input.get("CONTRACT_DATE");
		String contractSl = input.get("CONTRACT_SL");
		String poSl=input.get("PO_SL");
		String invoiceSl=input.get("INVOICE_SL");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidNumber(contractSl)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_NUMBER);
					return result;
				}
				if (!isValidLength(contractSl, LENGTH_1, LENGTH_5)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.TABLE, "LEASEPOINV");
			String columns[] = new String[] { getContext().getEntityCode(), contractDate, contractSl,poSl,invoiceSl };
			input.setObject(ContentManager.PROGRAM_KEY, columns);
			result = validatePK(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}
}
