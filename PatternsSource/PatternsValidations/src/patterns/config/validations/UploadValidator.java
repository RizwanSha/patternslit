package patterns.config.validations;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.ContentManager;

public class UploadValidator extends InterfaceValidator{

	public final DTObject validatePurposeCode(DTObject input) {
		DTObject result = new DTObject();
		String purposeCode = input.get("FILE_PURPOSE_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(purposeCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(purposeCode, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, purposeCode);
			input.set(ContentManager.TABLE, "FILEPURPOSE");
			input.set(ContentManager.COLUMN, "FILE_PURPOSE_CODE");
			result = validateGenericCode(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}
	
	public final DTObject validateFileFormatCode(DTObject input) {
		DTObject result = new DTObject();
		String fileFormatCode = input.get("FORMAT_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(fileFormatCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(fileFormatCode, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, fileFormatCode);
			input.set(ContentManager.TABLE, "FILEFORMATS");
			input.set(ContentManager.COLUMN, "FORMAT_CODE");
			result = validateGenericCode(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}
	
	public final DTObject validateSystemFolderId(DTObject input) {
		DTObject result = new DTObject();
		String fileFormatCode = input.get("SYSTEM_FOLDER_ID");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(fileFormatCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(fileFormatCode, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, fileFormatCode);
			input.set(ContentManager.TABLE, "FILEDIRECTORIES");
			input.set(ContentManager.COLUMN, "SYSTEM_FOLDER_ID");
			result = validateGenericCode(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}
	
}
