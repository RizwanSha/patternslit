package patterns.config.validations;

import java.sql.ResultSet;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.ContentManager;

public class EodSodValidator extends InterfaceValidator {

	public final DTObject validateProcessName(DTObject input) {
		DTObject result = new DTObject();
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		String processId = input.get("PROCESS_ID");
		String processFlag = input.get("PROCESS_FLAG");
		DBUtil util = getDbContext().createUtilInstance();
		try {
			String sqlQuery = "SELECT PROCESS_NAME FROM EODSODPROCESSES WHERE PROCESS_ID=? AND ";
			if (processFlag.equalsIgnoreCase("E")) {
				sqlQuery += " PROCESS_ALLOW_EOD='1'";
			} else {
				sqlQuery += " PROCESS_ALLOW_SOD='1'";
			}
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, processId);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
			} else {
				result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return result;
	}

	public final DTObject readMainContDetails() {
		DTObject result = new DTObject();
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = getDbContext().createUtilInstance();
		try {
			String sqlQuery = "SELECT TO_CHAR(CURR_BUSINESS_DATE,?) CURR_BUSINESS_DATE,TO_CHAR(PREV_BUSINESS_DATE,?) PREV_BUSINESS_DATE,EOD_IN_PROGRESS,EOD_COMPL,SOD_IN_PROGRESS,SOD_COMPL,REMARKS FROM MAINCONT WHERE ENTITY_CODE=?";
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, getContext().getDateFormat());
			util.setString(2, getContext().getDateFormat());
			util.setString(3, getContext().getEntityCode());
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				decodeRow(rset, result);
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
			} else {
				result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return result;
	}

	public final boolean isUsersLoggedIn() {
		DBUtil util = getDbContext().createUtilInstance();
		try {
			String sqlQuery = "SELECT 1 FROM USERSLOGIN WHERE ENTITY_CODE=? AND USER_ID<>?";
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, getContext().getEntityCode());
			util.setString(2, getContext().getUserID());
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				return true;
			}
			return false;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			util.reset();
		}
		return true;
	}

	public final boolean isAuthorizationPending(boolean isFinancial) {
		DBUtil util = getDbContext().createUtilInstance();
		try {
			String sqlQuery = RegularConstants.EMPTY_STRING;
			ResultSet rset = null;

			if (isFinancial) {
				sqlQuery = "SELECT 1 FROM TBAAUTHQ T,MPGMCONFIG M WHERE T.ENTITY_CODE=? AND T.TBAQ_ENTRY_DATE=? AND T.TBAQ_PGM_ID=M.MPGM_ID AND M.MPGM_OPERATION='F'";
				util.reset();
				util.setSql(sqlQuery);
				util.setString(1, getContext().getDateFormat());
				util.setDate(2, getContext().getCurrentBusinessDate());
				rset = util.executeQuery();
				if (rset.next()) {
					return true;
				}
			} else {
				sqlQuery = "SELECT 1 FROM TBAAUTHQ T,MPGMCONFIG M WHERE T.ENTITY_CODE=? AND T.TBAQ_ENTRY_DATE=? AND T.TBAQ_PGM_ID=M.MPGM_ID AND M.MPGM_EOD_OPR='1'";
				util.reset();
				util.setSql(sqlQuery);
				util.setString(1, getContext().getDateFormat());
				util.setDate(2, getContext().getCurrentBusinessDate());
				rset = util.executeQuery();
				if (rset.next()) {
					return true;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return true;
		} finally {
			util.reset();
		}
		return false;
	}
	
	public final DTObject validateProcessId(DTObject input) {
		DTObject result = new DTObject();
		String code = input.get("PROCESS_ID");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				
				if (!isValidCodePattern(code)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(code,LENGTH_1,LENGTH_50)) {	
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, code);
			input.set(ContentManager.TABLE, "EODSODPROCESSES");
			input.set(ContentManager.COLUMN, "PROCESS_ID");
			result = validateGenericCode(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}
}