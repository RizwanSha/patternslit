/*
 * Copyright (c) 2014, 2015, Patterns Software Design Institute. All rights reserved. Unauthorized copying of this file, via any medium is strictly prohibited.
 * PATTERNS SDI PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package patterns.config.validations;

import java.io.InputStream;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.MiddlewareConstants;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.ContentManager;

import com.mashape.unirest.http.utils.Base64Coder;

/**
 * The Class MiddlewareValidator provides validations related to Middleware Module
 */
public class MiddlewareValidator extends InterfaceValidator {

	/**
	 * Instantiates a new middleware validator.
	 */
	public MiddlewareValidator() {
		super();
	}

	/**
	 * Instantiates a new middleware validator.
	 * 
	 * @param dbContext
	 *            the db context
	 */
	public MiddlewareValidator(DBContext dbContext) {
		super(dbContext);
	}

	/**
	 * Validate session id.
	 * 
	 * @param input
	 *            the input
	 * @return the DT object
	 */
	public DTObject validateSessionId(DTObject input) {
		DTObject result = new DTObject();
		String value = input.get("SESSION_ID");
		String fetch = input.get(ContentManager.FETCH);
		String sqlQuery = null;
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = getDbContext().createUtilInstance();
		try {
			if (fetch != null && fetch.equals(ContentManager.FETCH_ALL)) {
				sqlQuery = "SELECT * FROM SRVSIRLOGIN WHERE ENTITY_CODE = ? AND SESSION_ID = ?";
			} else {
				sqlQuery = "SELECT 1 FROM SRVSIRLOGIN WHERE ENTITY_CODE = ? AND SESSION_ID = ?";
			}
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, getContext().getEntityCode());
			util.setString(2, value);

			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				if (fetch != null && fetch.equals(ContentManager.FETCH_ALL)) {
					decodeRow(rset, result);
				}
			} else {
				result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getLogger().logError(e.getLocalizedMessage());
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return result;
	}

	/**
	 * Validate bank user session id.
	 * 
	 * @param input
	 *            the input
	 * @return the DT object
	 */
	public DTObject validateUserSessionId(DTObject input) {
		DTObject result = new DTObject();
		String sessionId = input.get("SESSION_ID");
		String userId = input.get("USER_ID");
		String sqlQuery = "SELECT 1 FROM SRVLOGINAUTH WHERE ENTITY_CODE = ? AND SESSION_ID = ? AND USER_ID = ?";
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = getDbContext().createUtilInstance();
		try {
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, getContext().getEntityCode());
			util.setString(2, sessionId);
			util.setString(3, userId);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
			} else {
				result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getLogger().logError(e.getLocalizedMessage());
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return result;
	}

	/**
	 * Gets the system control details.
	 * 
	 * @return the system control details
	 */
	public DTObject getSystemControlDetails() {
		DTObject resultDTO = new DTObject();
		DBUtil dbutil = getDbContext().createUtilInstance();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		String systemControlCode = RegularConstants.EMPTY_STRING;
		try {
			systemControlCode = getContext().getMobileContextParam().get("APPL_SYS_PARAM");
			dbutil.reset();
			dbutil.setMode(DBUtil.PREPARED);
			dbutil.setSql("SELECT * FROM SYSCPM WHERE ENTITY_CODE =? AND SYSCPM_CODE=? AND EFFT_DATE=(SELECT MAX(EFFT_DATE) FROM SYSCPM WHERE ENTITY_CODE=? AND SYSCPM_CODE=? AND EFFT_DATE <=FN_GETCD(?))");
			dbutil.setString(1, getContext().getEntityCode());
			dbutil.setString(2, systemControlCode);
			dbutil.setString(3, getContext().getEntityCode());
			dbutil.setString(4, systemControlCode);
			dbutil.setString(5, getContext().getEntityCode());
			ResultSet readSysCpmRSet = dbutil.executeQuery();
			if (readSysCpmRSet.next()) {
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				decodeRow(readSysCpmRSet, resultDTO);
			} else {
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getLogger().logError(e.getLocalizedMessage());
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			dbutil.reset();
		}
		return resultDTO;
	}

	/**
	 * Gets the app control parameters.
	 * 
	 * @param applicationType
	 *            the application type
	 * @param entityCode
	 *            the entity code
	 * @return the app control parameters
	 */
	public DTObject getAppControlParameters(String applicationType, String entityCode) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil dbutil = getDbContext().createUtilInstance();
		try {
			String sqlQuery = RegularConstants.EMPTY_STRING;
			dbutil.reset();
			dbutil.setMode(DBUtil.PREPARED);
			sqlQuery = "SELECT APPL_CODE FROM APPLCNTRLPARAM WHERE ENTITY_CODE=? AND EFFT_DATE=(SELECT MAX(EFFT_DATE) FROM APPLCNTRLPARAM WHERE ENTITY_CODE=? AND EFFT_DATE<=FN_GETCD(?))";

			dbutil.setSql(sqlQuery);
			dbutil.setString(1, entityCode);
			dbutil.setString(2, entityCode);
			dbutil.setString(3, entityCode);
			ResultSet rset = dbutil.executeQuery();
			if (rset.next()) {
				String applnControlCode = rset.getString("APPL_CODE");
				resultDTO.set("APPL_CODE", applnControlCode);
				sqlQuery = "SELECT * FROM APPLCONTROL WHERE APPL_CNTRL_ID = ? AND ENABLED='1'";
				dbutil.reset();
				dbutil.setMode(DBUtil.PREPARED);
				dbutil.setSql(sqlQuery);
				dbutil.setString(1, applnControlCode);
				ResultSet readSysCpmRSet = dbutil.executeQuery();
				if (readSysCpmRSet.next()) {
					decodeRow(readSysCpmRSet, resultDTO);
					resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			getLogger().logError(e.getLocalizedMessage());
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			dbutil.reset();
		}
		return resultDTO;
	}

	/**
	 * Generate random string.
	 * 
	 * @param otpLength
	 *            the otp length
	 * @param otpPattern
	 *            the otp pattern
	 * @return the string
	 */
	public String generateRandomString(int otpLength, String otpPattern) {
		StringBuffer randStr = new StringBuffer();
		for (int i = 0; i < otpLength; i++) {
			int number = getRandomNumber(otpPattern.length());
			char ch = otpPattern.charAt(number);
			randStr.append(ch);
		}
		return randStr.toString();
	}

	/**
	 * Gets the random number.
	 * 
	 * @param length
	 *            the length
	 * @return the random number
	 */
	private int getRandomNumber(int length) {
		int randomInt = 0;
		Random randomGenerator = new Random();
		randomInt = randomGenerator.nextInt(length);
		if (randomInt - 1 == -1) {
			return randomInt;
		} else {
			return randomInt - 1;
		}
	}

	/**
	 * Gets the application control parameters.
	 * 
	 * @param applicationType
	 *            the application type
	 * @return the application control parameters
	 */
	public DTObject getApplicationControlParameters(String applicationType) {
		DTObject result = new DTObject();
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil dbUtil = getDbContext().createUtilInstance();
		String sqlQuery = "CALL SP_GETAPPLCNTRL_CODE(?,?,?,?,?,?,?)";
		try {
			dbUtil.reset();
			dbUtil.setMode(DBUtil.CALLABLE);
			dbUtil.setSql(sqlQuery);
			dbUtil.setString(1, getContext().getEntityCode());
			dbUtil.setString(2, applicationType);
			dbUtil.registerOutParameter(3, Types.VARCHAR);
			dbUtil.registerOutParameter(4, Types.VARCHAR);
			dbUtil.registerOutParameter(5, Types.VARCHAR);
			dbUtil.registerOutParameter(6, Types.VARCHAR);
			dbUtil.registerOutParameter(7, Types.VARCHAR);
			dbUtil.execute();
			String errorStatus = dbUtil.getString(7);
			if (errorStatus.equals(RegularConstants.SP_SUCCESS)) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				result.set("APPL_SYS_PARAM", dbUtil.getString(3));
				result.set("APPL_SMS_INTF", dbUtil.getString(4));
				result.set("APPL_EMAIL_INTF", dbUtil.getString(5));
				result.set("APPL_EXT_APP_CODE", dbUtil.getString(6));
			}
		} catch (Exception e) {
			e.printStackTrace();
			getLogger().logError(e.getLocalizedMessage());
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			dbUtil.reset();
		}
		return result;
	}

	/**
	 * Gets the application key.
	 * 
	 * @return the application key
	 */
	public DTObject getApplicationKey() {
		DTObject result = new DTObject();
		String sqlQuery = null;
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = getDbContext().createUtilInstance();
		try {
			DTObject context = getContext().getMobileContextParam();
			sqlQuery = "SELECT EXT_GENERATED_KEY FROM EXTAPPL WHERE EXT_APPL_CODE = ? AND ENABLED='1' AND EXT_REGKEYS_KEYS='1'";
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, context.get("APPL_EXT_APP_CODE"));
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				result.set("EXT_GENERATED_KEY", rset.getString(1));
			} else {
				result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getLogger().logError(e.getLocalizedMessage());
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return result;
	}

	/**
	 * Validate session.
	 * 
	 * @param input
	 *            the input
	 * @return the DT object
	 */
	public DTObject validateSession(DTObject input) {
		DTObject result = new DTObject();
		String value = input.get("SESSION_ID");
		String sqlQuery = null;
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = getDbContext().createUtilInstance();
		try {
			sqlQuery = "SELECT 1 FROM SRVSESSIONDETAILS WHERE ENTITY_CODE = ? AND APP_ID=? AND SESSION_ID = ? AND USER_ID=?";
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, getContext().getEntityCode());
			util.setString(2, getContext().getMobileContextParam().get("APPL_EXT_APP_CODE"));
			util.setString(3, value);
			util.setString(4, getContext().getUserID());
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
			} else {
				result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getLogger().logError(e.getLocalizedMessage());
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return result;
	}

	/**
	 * Gets the application sequence.
	 * 
	 * @return the application sequence
	 */
	public int getApplicationSequence() {
		int result = 0;
		DBUtil util = getDbContext().createUtilInstance();
		try {
			String sqlQuery = "SELECT SESSION_SEQ.NEXTVAL FROM DUAL";
			util.reset();
			util.setSql(sqlQuery);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result = rset.getInt(1);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getLogger().logError(e.getLocalizedMessage());
		} finally {
			util.reset();
		}
		return result;
	}

	/**
	 * Checks if is user active.
	 * 
	 * @param loginId
	 *            the login id
	 * @return true, if is user active
	 */
	public boolean isUserActive(String loginId) {
		DBUtil dbUtil = getDbContext().createUtilInstance();
		try {
			String applicationCode = getContext().getMobileContextParam().get("APPL_EXT_APP_CODE");
			dbUtil.reset();
			dbUtil.setMode(DBUtil.PREPARED);
			dbUtil.setSql("SELECT 1 FROM SRVSERVICELOCK WHERE ENTITY_CODE=? AND APP_ID=? AND LOGIN_ID=? AND LOCK_EXPIRY_TIME>FN_GETCDT(?)");
			dbUtil.setString(1, getContext().getEntityCode());
			dbUtil.setString(2, applicationCode);
			dbUtil.setString(3, loginId);
			dbUtil.setString(4, getContext().getEntityCode());
			ResultSet readLockRSet = dbUtil.executeQuery();
			if (readLockRSet.next()) {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			getLogger().logError(e.getLocalizedMessage());
		} finally {
			dbUtil.reset();
		}
		return true;
	}

	/**
	 * Load catalog values.
	 * 
	 * @return the JSON object
	 */
	public JSONObject loadCatalogValues() {
		JSONObject catalog = new JSONObject();
		Locale locale = getContext().getLocale();
		if (locale == null)
			locale = Locale.US;
		String localeName = locale.toString().toUpperCase(locale);

		String sqlQuery = "SELECT PGM_ID,LOV_ID,SCRIPT_KEY FROM CM_LOVRECMAIN WHERE ENABLED='1' AND SENDTO_CLIENT='1'";
		DBUtil dbUtil = getDbContext().createUtilInstance();
		DBUtil readCmlovRecUtil = getDbContext().createUtilInstance();
		try {
			dbUtil.setMode(DBUtil.PREPARED);
			dbUtil.setSql(sqlQuery);
			ResultSet readCmlovrecMailRS = dbUtil.executeQuery();
			while (readCmlovrecMailRS.next()) {
				Map<String, String> catalogMap = new LinkedHashMap<String, String>();
				sqlQuery = "SELECT LOV_VALUE, LOV_LABEL_" + localeName + " FROM CM_LOVREC WHERE PGM_ID = ? AND LOV_ID = ? ORDER BY LOV_LABEL_" + localeName + " ASC";
				readCmlovRecUtil.reset();
				readCmlovRecUtil.setMode(DBUtil.PREPARED);
				readCmlovRecUtil.setSql(sqlQuery);
				readCmlovRecUtil.setString(1, readCmlovrecMailRS.getString(1));
				readCmlovRecUtil.setString(2, readCmlovrecMailRS.getString(2));
				ResultSet readCmlovRecDeatilRS = readCmlovRecUtil.executeQuery();
				while (readCmlovRecDeatilRS.next()) {
					catalogMap.put(readCmlovRecDeatilRS.getString(1), readCmlovRecDeatilRS.getString(2));
				}
				catalog.put(readCmlovrecMailRS.getString(3), catalogMap);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getLogger().logError(e.getLocalizedMessage());
		} finally {
			dbUtil.reset();
			readCmlovRecUtil.reset();
		}
		return catalog;
	}

	public DTObject fetchCustomerDocument(String documentType, String customerNo, String accountNo) {
		DTObject result = new DTObject();
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil dbUtil = getDbContext().createUtilInstance();
		try {
			String docType = "";
			StringBuffer accountQuery = new StringBuffer("SELECT C.FILE_INV_NO FROM ACCOUNTDOCREPO C INNER JOIN DOCTYPES D ON C.ENTITY_CODE=D.ENTITY_CODE AND C.DOCTYPE_CODE=D.DOCTYPE_CODE AND");
			if (documentType.equals(MiddlewareConstants.DOC_FOR_PHOTO)) {
				docType = " D.FOR_PHOTOGRAPH='1'";
			} else if (documentType.equals(MiddlewareConstants.DOC_FOR_SIGNATURE)) {
				docType = " D.FOR_SIGNATURE='1'";
			} else {
				return result;
			}
			accountQuery.append(docType);
			accountQuery.append(" WHERE C.ENTITY_CODE=? AND C.ACCOUNT_NO=?");
			accountQuery.append(" AND C.SL = ( SELECT MAX(SL) FROM ACCOUNTDOCREPO C INNER JOIN DOCTYPES D ON C.ENTITY_CODE=D.ENTITY_CODE AND C.DOCTYPE_CODE=D.DOCTYPE_CODE AND");
			accountQuery.append(docType);
			accountQuery.append(" WHERE C.ENTITY_CODE=? AND C.ACCOUNT_NO=?)");

			StringBuffer customerQuery = new StringBuffer("SELECT C.FILE_INV_NO FROM CUSTDOCREPO C INNER JOIN DOCTYPES D ON C.ENTITY_CODE=D.ENTITY_CODE AND C.DOCTYPE_CODE=D.DOCTYPE_CODE AND");
			if (documentType.equals(MiddlewareConstants.DOC_FOR_PHOTO)) {
				docType = " D.FOR_PHOTOGRAPH='1'";
			} else {
				docType = " D.FOR_SIGNATURE='1'";
			}
			customerQuery.append(docType);
			customerQuery.append(" WHERE C.ENTITY_CODE=? AND C.CIF_NO=?");
			customerQuery.append(" AND C.SL = ( SELECT MAX(SL) FROM CUSTDOCREPO C INNER JOIN DOCTYPES D ON C.ENTITY_CODE=D.ENTITY_CODE AND C.DOCTYPE_CODE=D.DOCTYPE_CODE AND");
			customerQuery.append(docType);
			customerQuery.append(" WHERE C.ENTITY_CODE=? AND C.CIF_NO=?)");

			long fileInventoryNo = 0;
			dbUtil.setSql(customerQuery.toString());
			dbUtil.setMode(DBUtil.PREPARED);
			dbUtil.setString(1, getContext().getEntityCode());
			dbUtil.setLong(2, Long.parseLong(customerNo));
			dbUtil.setString(3, getContext().getEntityCode());
			dbUtil.setLong(4, Long.parseLong(customerNo));
			ResultSet rset = dbUtil.executeQuery();
			if (rset.next()) {
				fileInventoryNo = Long.parseLong(rset.getString(1));
			} else {
				dbUtil.setSql(accountQuery.toString());
				dbUtil.setMode(DBUtil.PREPARED);
				dbUtil.setString(1, getContext().getEntityCode());
				dbUtil.setLong(2, Long.parseLong(accountNo));
				dbUtil.setString(3, getContext().getEntityCode());
				dbUtil.setLong(4, Long.parseLong(accountNo));
				rset = dbUtil.executeQuery();
				if (rset.next()) {
					fileInventoryNo = Long.parseLong(rset.getString(1));
				}
			}
			if (fileInventoryNo > 0) {
				dbUtil.reset();
				dbUtil.setSql("SELECT FILE_DATA,FILE_EXTENSION FROM CMNFILEINVENTORY WHERE ENTITY_CODE=? AND FILE_INV_NUM=?");
				dbUtil.setMode(DBUtil.PREPARED);
				dbUtil.setLong(1, Long.parseLong(getContext().getEntityCode()));
				dbUtil.setLong(2, fileInventoryNo);
				rset = dbUtil.executeQuery();
				if (rset.next()) {
					StringBuffer data = new StringBuffer("");
					InputStream input = rset.getBinaryStream(1);
					byte[] bytes = IOUtils.toByteArray(input);
					result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
					data.append("data:image/").append(rset.getString(2)).append(";base64,");
					data.append(new String(Base64Coder.encode(bytes)));
					result.set("DATA", data.toString());
				}
			}
			dbUtil.reset();
		} catch (Exception e) {
			dbUtil.reset();
		}
		return result;
	}

	public DTObject getSessionDetails(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		String sqlQuery = "SELECT USER_ID,USER_NAME,BRANCH_CODE,CLUSTER_CODE,FIN_YEAR,ROLE_CODE,LAST_LOGIN_DATETIME,ROLE_DESC,BRANCH_DESC,DATE_FORMAT,REQ_DATETIME FROM SRVSESSIONDETAILS WHERE ENTITY_CODE=? AND APP_ID=? AND SESSION_ID=?";
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		ResultSet rset;
		try {
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, inputDTO.get("ENTITY_CODE"));
			util.setString(2, inputDTO.get("APP_ID"));
			util.setString(3, inputDTO.get("SESSION_ID"));
			rset = util.executeQuery();
			while (rset.next()) {
				resultDTO.set("USER_ID", rset.getString(1));
				resultDTO.set("USER_NAME", rset.getString(2));
				resultDTO.set("BRANCH_CODE", rset.getString(3));
				resultDTO.set("CLUSTER_CODE", rset.getString(4));
				resultDTO.set("FIN_YEAR", rset.getString(5));
				resultDTO.set("ROLE_CODE", rset.getString(6));
				resultDTO.setObject("LAST_LOGIN_DATETIME", rset.getObject(7));
				resultDTO.set("ROLE_DESC", rset.getString(8));
				resultDTO.set("BRANCH_DESC", rset.getString(9));
				resultDTO.set("DATE_FORMAT", rset.getString(10));
				resultDTO.setObject("LOGIN_DATETIME", rset.getObject(11));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			dbContext.close();
		}
		return resultDTO;
	}

	public DTObject validateUserRole(DTObject inputDTO) {
		inputDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		String sqlQuery = "SELECT R.FIELD_ROLE,R.DESCRIPTION FROM ROLES R WHERE R.ENTITY_CODE=? AND R.CODE=?";
		DBUtil util = getDbContext().createUtilInstance();
		try {
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, getContext().getEntityCode());
			util.setString(2, inputDTO.get("ROLE_CODE"));
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				if (rset.getString(1).equals("1")) {
					inputDTO.set("ROLE_DESC", rset.getString(2));
					inputDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			util.reset();
		}
		return inputDTO;
	}

	public DTObject validateWalletAccount(DTObject inputDTO) {
		inputDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = getDbContext().createUtilInstance();
		try {
			String sqlQuery = "SELECT ACCOUNT_NO,AC_BAL,BC_BAL FROM WALLETBAL" + getContext().getClusterCode() + " WHERE ENTITY_CODE=? AND WALLET_NO=? AND CURR_CODE=? AND STATUS='02'";
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, getContext().getEntityCode());
			util.setLong(2, Long.parseLong(inputDTO.get("WALLET_NO")));
			util.setString(3, inputDTO.get("CURR_CODE"));
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				inputDTO.set("ACCOUNT_NO", rset.getString(1));
				inputDTO.set("AC_BAL", rset.getString(2));
				inputDTO.set("BC_BAL", rset.getString(3));
				inputDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			util.reset();
		}
		return inputDTO;
	}

	public DTObject fetchCashModeParam(DTObject input) {
		input.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = getDbContext().createUtilInstance();
		try {
			String sqlQuery = "SELECT FIELD_STAFF_CASH_GL_HEAD, CTC_FTRAN_CODE FROM CASHMODPARAM WHERE ENTITY_CODE=?";
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, getContext().getEntityCode());
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				input.set("FIELD_STAFF_CASH_GL_HEAD", rset.getString(1));
				input.set("CTC_FTRAN_CODE", rset.getString(2));
				input.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			util.reset();
		}
		return input;
	}

	public String getAccountDetails(String accountNumber) {
		final String DEFAULT_USER_IMAGE = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAlgAAAJYCAYAAAC+ZpjcAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAINySURBVHhe7Z1pe5vHkXb1/78lk3kzM5nE8SrJlqx9oShRO0VREkVtlOxMJrYWZ/4FXp/O3JgWQoobwGfp8wEXQBAkgKfvrjpdVV197N///d8n3rwGakANqAE1oAbUgBqYnwaOeTHndzG9ll5LNaAG1IAaUANqAA0IWEbwjGCqATWgBtSAGlADc9aAgDXnC+rKxZWLGlADakANqAE1IGAJWK5a1IAaUANqQA2ogTlrQMCa8wV11eKqRQ2oATWgBtSAGhCwBCxXLWpADagBNaAG1MCcNSBgzfmCumpx1aIG1IAaUANqQA0IWAKWqxY1oAbUgBpQA2pgzhoQsOZ8QV21uGpRA2pADagBNaAGBCwBy1WLGlADakANqAE1MGcNCFhzvqCuWly1qAE1oAbUgBpQAwKWgOWqRQ2oATWgBtSAGpizBgSsOV9QVy2uWtSAGlADakANqAEBS8By1aIG1IAaUANqQA3MWQMC1pwvqKsWVy1qQA2oATWgBtSAgCVguWpRA2pADagBNaAG5qwBAWvOF9RVi6sWNaAG1IAaUANqQMASsFy1qAE1oAbUgBpQA3PWwLH/+I//mHjzGqgBNaAG1IAaUANqYH4aELAETAFbDagBNaAG1IAamLMGjv3hD3+YePMaqAE1oAbUgBpQA2pgfhoQsARMAVsNqAE1oAbUgBqYswYErDlfUOl/fvTvtfRaqgE1oAbUwFA1YA3WnHOuFgjOr0DQa3k01xLj9Z//+Z+TP/7xj+We6/5v//Zvk9///vflZ7ab85gbv+O5vG63MYphzN/knvfiVr83j3kv3psbj3f7///v//2/8rnq19fGOO/Hc/yvbJ3Pe9R/N/va+vX5HLOGnv+X75DPn2v02Wef7fr5d/t+/v5o5oDX2eu8CA0IWAKWTqBxDQRqalABFgCgQEZAJIAR6NptZVkbrbovUA1RARQAJyAU4NvN6P3pT3/6JygEunKb7UW02/+bBalZ6Ap48v//9V//dZpS4X147ne/+125BUb3+n6+TgevBsanAQGrcefqpB7fpN7PmM5GrWqQqqNZwAS3OorF324XEaqjQ4l8bRdh4v/zfCI+iWoBTX/+858nX3zxxeTUqVOfvJ04cWLyzTfflNfyd7PRK57jlv8diMvr8llrKJuFydkoXP4X9/wd14DHfGaiVnm/vUb59jNevrbt+er4D2v8BSwBywhWwxpIii2prkSQEqlJJCvQA0AEjACuGjZqeIkjAJDOnDkzuXDhwmRpaWly8+bNyf379ycPHz6cPH78ePLy5cvJ1tbW5M2bN5Mffvhhev+Xv/xl8te//nXyX//1X5+8/fTTT5O//e1v5bX8zY8//lhuPOZvNzY2yvusra1N7t69O7lx48bk6tWrk4sXL07Onz9f4Oyrr74qgMZ3C3DWUFWnF+sIFtcuMHWQ9KbOcljO0vFyvParAQGrYee6X7H4+nEamEAUgJEITGAp93UdFo95ntcCJydPnpycO3dusry8PFldXZ1sbm4WUAJ6drsBQbwGSOL23//93+XG459//nn6c56fvZ8FsNn3q1+f9wDK6ht/w+d99uxZAb/bt29Prl27Nrl8+fLk888/L7fZ67LXY1CcM+OcM46r47oXDQhYApYRrIY1kAgMxoLoTF00nmgNMMHrvv7668nZs2cnKysrJSpE5CnQUsMRULMdWAWceG0AJ4AVEOLnRJ94jse73WYhK68nkpX3mQW4vB+v2Q7y8jn4e/7f8+fPCzwShTt9+nQBS6Crjvgl3VkXve/FCPsanbUaGKcGBKyGnauTepyTej/jmtojaqVSS0TEBpiivol03pMnT0qEp44qAR2k9HaDn1kA2y7tx3M1YM1C107RK55PinAW8AJzga8A32xEazaa9an/t93nuHfvXoncff/995Mvv/xyujMyKcb9jIWvdT6qgXFpQMASsIxgNawBoIri7O+++66kxahVevHiRQGnRG8CMvyc6FSe+xT8zP4ukFNHnOpoVR35yv/frQarrr0C+BKRSpQsn2GnVGXALP8nwJjXbxf5qv8XaUxuXBs+K5GuW7dulSiXbRrG5SyFH8dzvxoQsBp2rvsVi68/egOTeqfskCPSNLubry6wTm1QPVZJY2VHH/VTFHdTfE7NEbVHgEVdLA4s7JTq262uyt//X+0ZqVRSqgAs173eVUlKMa0weJ5oYp1mZAzTC2y2XxivQwfOyaOfk15zr/leNSBgCVga6R5rALBiMscxpyA9DUD/5V/+pfyeKBTOmtfxHA6Yv40TZpfct99+Wwq3qSUi5ffhw4dSR0XUp06p1TvxhKXdC/U/dY1++eWXEuHimqaO69KlS2XXIkBVt8KoW1mkN9lsG43UyKGDaGOvxt7XCQZq4Gg1IGD12Lk6GY52MvTxetctAgJZaQiKQwas+NyJaqWdAo4bZ8wOP9oSUEcFCLx7927y9u3bAlREqeoi8NRHJU2WWigh6+CQlRYUXEOu+/v370uk8NWrV2VMSCUyRtRv1f2zMu6Ma2rjEsVkvOtGsH3UrZ9J26UG/mMiYAlYRrB6rIE42qSLZtsDAFGJZqSbOEXq1FM9ffp0ussPB58+U0RTcPLAVp0WTO+puu5IuDo4XHHt0mqihta6LowxALao26JnGMBcHye03VE/dSNXnZggowb6qwEBq8fO1YnT34lzVGOTbumpz6kjVHG0OTOQaEjSf4EkYApHT6Qqxdh1QTdRKl6THYH1zjzh6nBwleseiK13S6a+DfDiMdEtxgjYog8X6dzAdY7g4R7dJT1sDZb24ajskO9zMK0JWAKWEawea6A+mgagor7qt7/9bam1Ar7ox0S0ip1/9W62OPdZSKp35dUpwey+S3QlbRiErMNBVr1Lst7ZGLDN77nO+T2/e/36dRlTNiKwISEF8imCT+NXHd/BHJ/Xzet2FBoQsHrsXI9CAL5Hvw1NDhTOocYpXqfv0vXr10vUIxEpHHMiVzhunHRdR7VT24SAVg1TOXZGwDocYM22pJitb0skK+PEeBLN4hZgBrTYiUitFlGt1ODV6WPncb/nsePT5vgIWAKWEawea4CanOwMBLKuXLlSjqIBpCiYTgpwtpYqNVZpnFnDVd2PKnC1HYjVqS1B62CglU0FuZYBrrqrfZ2yra9zXgtoAVwA8507d0qtVg6a1nG36bgd92GM+7G9nqnl6/697Obx5jXYjwZmj1JJX6MYyBycnDqq9EVK/yvqbo4fP14OSWabf/pT7dZAUyA6GBD17brVjVBTH0cUjFo7DqvOzsLZfll1dKs+uqfuk5b+afvRs6/V/qmBvWtAwBKahMYFaqBeaQJTOMRsx68bgJL6y/EqqbPhdUQsaFZJEXoK1RPxSPSqb1Dg55kf3KVGK2cj1pBF6vDu3buTixcvlgOpc+xRdpUC63VvrYBW+qqlBYQOc+8O02vltdqPBgSsBTrX/QyErx3nxJ3tsp4eRilSjjNMP6vf/e53Jf1DKpAO62kCmrqc9K+qi6MFmvkBTd+uZerqchRPdh0mbYge6LVFROvcuXOlzUP6pdWtPbZr87FT6w9t0ThtkeN69ONqDVaP62/Msw8jz/6pcQpg1UekBLK4J/KA4SOawNl1dPkmYoWjp9N6DVL1zjMcK062b0Dg55k/7OWQbe6jAcA7fcsCW3Tn5/BpNkCgJaJXOcQ7/bQCVXaCH75t0T/0fwwFLAHLIvcFaiApmvosuaQJcYKAFRErnOL9+/eL0wSscJpJC6ZQvS5IT6pIoJk/0PTpmiY1WDeBTRQzkIVOeB1RTorqAS16aaEpwCqp6PqYpaQJddL9d9KO0XDH6FjqPrz/w7QGxmvhtZiXBgAsnBwwlbPj0iAUw3nixIlSZ4WDBKyIUuAwkxoMXNXFzrO9rPoEBH6W+QIfwMQ1TZ+yHG2UTQ6AOHoAqoAwfubGY2q0lpaWyq5DNAjMp9YvgDUvnft/tJlq4J81IGD92ldGYXgNFqWB7AbM7sAcbcPOQGpmZg9armtu4ljr3kk1XHlW4Hxhpo9wCGAlYpXWDolkpgN8usCjE0ALQOd3tPHg8fr6ekk9c95hUoTpCL8o3ft/talq4A8TAUvAEjAXqIFsn6+L2kndUGeFY8xOQGApkatEq3JmYKIV6fRd97bqIxT4meYHfvS+4pboFGMf4E5N1na9tQJlABoRrfTQ4ggeo1fCj/BzNBoQsBboXBXx0Yi4y+tM6o/0y+yOLSJWFLDzewqNSc9w5AnpQKIMOMp08RZI5gckXsuPr2U68gfKXr58WQ6W/vrrrye/+c1vpilD9JoNGanVSo+tLueX7z1+GzrmMRawBCwjWIfQwGyDUEALZ5UeRDix7A589OhRiT4kAmWKT7BaNBAGrHifdJOnYS1F8PTPyhmHRFhp8YBuOe8yu1rH7Pz8bsLbojUgYB3CuS56cPz//TcAiVzVzUEBKsYO+KLAmHPkXr16Nd3lldTOop2r/1+AC8Rn1ylpafRH+pn+WRTBE82iNjAbMtB09KwN6r8Ncoz6O0YCloBlBOsQGkikqnZOgNUXX3wxuXDhwrR+JocwZ9t9itmFICHoKDRQn0UZLRJJJV394MGDshBAw2kbwiKBqJbOu7/O27Hp/9gIWIdwrgq8/wJf9BgBU7xHWjAQCSDVcv369QJXqbNKITLOdPag36NwsL5HuyBXa7DekRpNAFykDdlpyIIBTbvLUNu2aNvZwv8XsAQsV6mH0ACr/LRgIALALi22xQek0p8ozqyOYFnk3i70HDXw1lqrYT+HhwNZpAwpgGczRr3rtQVH6HcUKBehAQHrEM51EQPi/xzWRAeuiF6xY/Ds2bOTjY2NyS+//FJuwFVAK9vmacXALZ25j9rR+n5tQ12ipwEu7qnHSksQ9MFmDHq0pZZQmzQsm+R49We8BCwBywjWITRAOoWUIMXCABV9h9LTKnUv9RE3NXAJO23DzlGNfx01jf5qTaZlSHa4Ugi/ublZDhzXWffHWTsWwxsLAesQzlXBD0/w+x2zpADTEyhnC7K9negVR92sra2VZo44KpwZ9zmu5KicqO8jrC1CA8vLy2XDBq0bqMvKIdLMA6K2zKccJJ1+cPUc2e988/Xjt6ktjbGAJWC5Sv2EBnAu2bYeB8JOKx5Tb0VKEMfGsSTck/5LlCCPF+H4/J8C1VFogNosdhlytBMbOFhwAFboP41KmR84TRYcABg3HrsLUVhqCaa2+64CloAlYH1CAziJ+jzB7LCiSSM7r0inkBYkapValvQcOgoH6HsIWovUANoGsti48d1335W5kGa63Ne7DXlMpJczD0mb52Dp1p2s379d0BSwBCwB6xMaSLQqbRhwHFevXi07rv7nf/6ntFzIcST1jix3CAo+iwSfo/rfNMjlvVhAUJfFRg7AiYUGIJWoVTrC8zvmClGsRLYEjHYBo/WxF7AELAHrExqIo0jzULax43BY2ROxqg/azRE4PJ+Dm4/KEfo+At2iNJADyakr5CxDit+BKBYfAFWOhkprh0S4gK7WHazfv224FLAELI3gLilCnAirdeCKaNWHDx9K2iSNRHkMXHHL7qys+hfl9Py/AtVRaIAUOO+TNg7UGvL4xo0bJQ0YyMqmjxqqBKy24UK4/MNEwBKwBKxPaIDCXorZ2SmYHYI53y3OJ+lA4Cuv4Xes+I/CCfoewtaiNFAf6RTIInpLS5LV1dXSlDTHRdXpQjvBC1cCloAlXAiYn9QAu6cePnxYolbcctRIznNLryuex+kEsHB41GktyvH5f4Wqo9BA9I2u0Xe9S5bniGQBWXW6MLtudbBCVusaMIIlYDQNmdSSkAKcrSVhNQ5cHYUT8z2EpaFqAMi6c+fO5KuvviqF7zhU6hZJD9YbRKjPyq5Cnqevlp3iBbCxA5iAJWA1DVgY/WwvB7ZICfIcR4Wkx9VQnZ+fW3BbtAaI5AJZRHlp4wBkMYeYU2lxghNNTyx6aOVA6bE7V7+fAClgCVjNA1a2lacD9enTpwtc/f3vfzeC9Wuqc9FO2v8/3GtMapzCdzZ40JCUkw0SEU5rE6JZ6Z8FYKUDvH2yBJCxQ6iAJWA1DVg5rDlwRZ+fZ8+eWaAuWAmWe9BAzt0EkqnXoiHpyZMnSyQraXdAK5CVo3XqqNbYnazfr12QFLAErOYBK0eAAFc0U2RFzg7ANFk0wjLcCItjt9ixy45CCuATybp3797k66+/nh6tk2N0AlkeodMucLQGmwKWgNU0YKX4lrQgkSucBCtxdkvZjX2xzln4Gf71pQaLcSRVyH1al5AuZMHC/GIBA1xxrmd9RqF9sgStsQOXgCVgNQ1YGPwUtBO14pYu7KzOhYDhQ4BjuLgxrBci6fjOvGFxwoKFwvekA6m9qo/QEbAELAFLAGkaQMY+AWjFUBe0kxbEOeAk0lBUB704B+21Hce1rftlpVccY0u6kBYOadmAPUlxu4AlYI3dvxjBEiBHDZBsFyc1gVHHyLOKTo8eDL8OfhwO3nHs7zjS8Z3I1W9/+9tyvA5pw9/85jfl8dgdrN+vbYgUsASsURu5HECbHU0ptOX4G+pEdMz9dcyOzfDHhigwJxqsrKyU8zzTwsFC97bBoxXwFLAErNEDFpErJjTbxdk+jqFnVU1Bu058+E7cMezvGOYsQ+qzbt68WSJZzEFqH1txsn7PdmHyGCt6b16DsWqAlTLfLU0PSUtwfhp1Ir/88ouAtYdeRwJMfwFmCGPDXKMRKQXx165dK3ORSFb6Y43V9vi99KsCloA5asBOI1FAi9Xz9evXy6G1bC+3DYPgMARAGfpnJHpFJIsWDmwiOX/+fDlKJ01HBRFBZKwaOIYD8uY1GKsGgKrUe1y+fLmsojH0GHx3CQpYQ4eXvn/+pAjzOVnYPH78eHLq1KkyL8dqd/xe+lQ0IGAJmKM2cqySWS3T6+r58+clVYGxF66Eq77DyRg+H1FioIobqcLMuxwOLYgIImPWgIAlYI0asOgiTZuGR48elaJ2IlivX78uoJXu02NwZH4HgbGPGqBxLyl5PhuQlQgyj+/evTtq2zNmcPC77Q2MrcGyBmvQNVgUrQNRRKlIB1I8yy4lcvqp87h//34x7olcpfYqz/XRMfmZBKYWNEA9FnOVectcpvCdG/3qLIK3NmvotVkCloA1aMACojDEOeOMnzMpSQ9S1E4fHuqu0m1asBJeWoCXIXxHDlfnzEIWRdmIwhwOcA3dwfr524ZEAUvAGjxgpTs7BjrtGIArzkEjDUiaIjUgqQmxyF3IGgKAjP0zciTV+vp6OU7nd7/73XSxRFSaRZOA0jagDH38BSwBa9BGjLRCveIlTUhjUQw2ndqJXAFXbBWn/iNFtha5C1hjh5chfD8WPMxNOr1nccSCCbhKqn/oTtbP3y4kClgC1qABC2PMapeIFcXsacmwvLxcaq4w4PXhs4BVdjPZB0vIGgKEjPkzsujh+9EfizYqLJa4sXAiTSictAsnYxh7AUvAGrQRSyF70oSseimcpe6K1GCcE5Es0hH87O5BwWrM0DKk71bv5n369Onk5MmTJVUYyBqDk/U7tAuJApaANWjAAqhY7WbFe/z48dLIELhKUTsOB8DiuXSVTtpwSM7IzyoYjk0DASzmKtHl27dvl7mceS2ctAsnYxh7AUvAGjRgkRIkesU9q15Sg0AUAJVCdn7GMXFPKoLnMezpzzM2p+X3EcSGogHmYN2MlPl58eLFMp9zjugYHK3foU1QFLAErEEDVnYaUdj+7bffFmhKQ1EL2QWNoYBGq58zTX9Z/JC6z67CL7/8cnqUDilDYCsbWnLETt2SRYBpE2D6Pu4CloA1aMBK9IoCd9ILGOoUtrfqtPzeguWQNJDNJiyIeEwUa2lpqewknO1rlwVViuH77mD9fG2Dn4AlYA0esNhBeOXKlWl9VdIOQ3IyflahqEUNUBeZlH1aNvAc54YSkWYnIYuntG2gDUsOcBde2oaXIYy/gCVgDRqwWMmeOHFisrGxMfnw4cO01ooaK1OEQkuL0DKk75x5yj2ARdsG5jE1lLdu3SpgRWqQRVQaCSeyZSNSAavvkCVgCViDBixWuDdu3ChgxcqXOg6MMwY7xe1Dcjh+VqGwJQ0wX1Pknu8d2KLVyunTp4t9YlchQAVosajisWcVClgClgAzaIDpu4CJXnGeGW0YUihbd29vyVn5XYWzoWmAiFV9EHvaNvA8Be937tyZUPDOQiqd3nNmoYAlYPXdPxnBEgAHDYAch0PNVc4aTKHs0ByNn1c4alEDiWAlnR/AYrFERJqCd+orqbuqC96NYAlXfYcrPp+AJWANGrCSCkzvqxTKJlXYotPyOwtrQ9FAarCyMEpz4DQeBbIePnw4+frrr8uRWIBW2jVYgyVk9R2yjiFSb16DPmuASZQVLPfUYZAewPAOxZH4OYUeNbB/DbBwAsKuXr1abEB2EDL/+2yz/Gz6VDQgYAmYvTZUdWoAo5q6C4pfX7x4IWD9Wtyv4/YajFUDiWyxmOIYLNKEgSshS4jpO8gKWAJWrwGLdECOwcmBztzfvXvXNgzClXA5cg2kthKAvHz58rSjO1FtAUvAErAEmF4DTN8FmpQgK9fAFg0IOWIju4/Gunr3exmZal0DASx6YxHFSk+sRLL7br/8fG1DoBEsAbDXAJj6K+5T1Eo9BsWv9rkSQFoHkLF//xS7A1rM93Pnzk04dzQLLwGmbYDp+/gLWAJWrwGLgnZWrYGrkydPTtbX1wtgEcUau4Px+wmRLWsgNVhp23Dv3r1iCwQswarvcGWRu3DVa7gKVCUdgFGlazstGDC8RrCEj5bho4Xvnv5YOa+QjS1EsazBErAELAGm9wDTd5ECVzkeg0NfHz9+PPnll19Kc1HSBy04Gb+jINmyBoCrHIPF4oru7qnJ7Lv98vO1DYKmCIXAXkMghpRdg+wkZBdRVrTpAN2y4/G7C15j10BShKm5ZFH17NmzCaUCSRMSzcpO47R14WgdFmQCTtuA0/X4C1gCVq+NUIrbOY/s9u3bJWJFwatH4ggXY4cLv98/NM5iimajPGbec07h8vLy9NBnoAo7kQbEdcS7awfr+7cNeAKWgNVrwMJAsTo9f/58OdQZIxtjqwMSstTA+DVArSWRqxyLRTTr+fPnBaiwD0S4A1n18TnZHCPktA05XY6/gCVgDQKwqLtg9ZpiV+5Z2epgx+9gHeN2xzgbWuo6rLdv35Yo9nfffVeiWDlCJ440Jz7YiFSw6hKu3EUoXPUergj9s0Ld2NiY9r5iNYvhJVWg823X+Tr24x/7nEUIYNWNhXl8//79EsVKJKsGq2yO6drB+v5tQ54RLCGr15AFYF24cGGytbU1TREkeiVgjd/BClFtjzFzPDWXaCGLq9RiffXVV9NdxkSy6qhVwEvIaRtyuhx/AUvA6jVgYTAfPHgw3T1YpwZNEbbtfIWv8Y8/c5woViAru4dzf+nSpekRWgEswIqFWTbIdOlgfe+24U7AErB6DVgUqr569apEr2rj6jmE43euApRjjAaY/xS2J2r17t27aSSLQ9+//vrrshEmgMVjG5G2DTZ9AVsBS8DqFLCor0rTwNRNYBzpY8PPFy9eLEfipA8OoEWRa4pfdcI6YTXQrgYoHeDw99///vfFXgBXAFeK3/viaP0cbQKfgCVgdQpYaRZYb7OmQSAGklt2DwawiFzV0Syda7vO1bF37FloXblyZVroTnqQnnnpiSXYtAk2fRl3AUvA6hSwmAgYwxq0WIFmgrx8+bJEr9IDhyJXbtldpJPVyaqBdjWAXVhbWyt1WIEqHlt/JVj1AbIELAGrN4CFUQS00jjw1KlTBaYALJxoXewqYLXrVAUqxz4aALCo0Tp+/Pi0Dku4Eq76AFf2wRKuOoer1F2x6gSu0rkdyKKAlZRgIlakA1Lcnu7OOludrRpoVwPZVXzu3Llp3WYi4n1xsn6OdoHPCJaQ1Slkpfaq7sZMweo333xTittz/hhAlV2EAFe6uutc23Wujr1jT58s7MGtW7emdViJhBvJahds+gK1ApaA1TlgMRmIZGVrNffff//9NFqFAQ1o5bDnNBzUyepk1UC7GkipwLNnz0ppQWyIndyFqz5AloAlYHUKWPXxFullwy4gurdTX0FKMEXuSQfwfHYS6lzbda6OvWOPTaBtC/dEvQEsyg2wK+xC7oOT9TO0C3vHZs9v8uc/lcnp7eiuAQYIY5jQPtf+6dOnH509pjPVmaoBNbCTBign4MSHdHAPZGnHj86Oe63/+VoLWMJUpzAZQ5hVHrVYtGlI93adqk5VDaiBT2kgHd4fPXo0bfciYAlWfQA+AUvA6gVgZQchk4IdQRhU0oA6V52rGlADO2mgPvz5+fPn0y7us/VYfXC2fob2oE/AErA6BayAVZoDYoRWVlZKXUWK23WwOlg1oAa20wA1mtldzI7Cs2fPlnIDAas9mOkjwApYAlangJXdPrnHMK6vrxfA0qnqVNWAGviUBhLBygkPLM5S6F7vKOyj8/UzjR8CBSwBq1PAovYq/WpoEHjy5MlSf2X0SscqXKmBvWqAOixKCp48eTLdRZj2L4LM+EGmr2MsYAlYnQJWJkaK3M+fP1/gqu7avlcj6+t0yGqgLQ3ETpAezHmlbJLJuYR9dbx+rjagT8ASsDoFLGqvSAticKjHWlpamrx//36CwcwZhDrNtpym4+1471UDaTRKmwbqsSgt4FxC6rCwJ4JMGyDT13EWsASsTo0QK82cQ0ij0RcvXkyjV6YJdbR7dbS+rk2txEYAWizIuF9eXi6LNuxJXx2vn6sN8BOwBKxOjRCpwXqlSf0V9RQ55FnH2abjdNwd9/1oAJsR2Lpz505ZtBHFEmTaAJm+jrOAJWB1boRy4DOhfUL9GEoAaz8G1tfqkNVAmxoIXBG9wm48fvy4AJZ1WMJV1+AlYAlYnQJWOi6zk5DzB6m9wkhyw3DqNNt0mo67475XDVDoToE7izPShNx/9dVX5RzCrh2s79825AlYAlanRqgGrNu3b0/rrwQsHexeHayva1crOQAesNra2ppujDl16lSndk2wahusMv4CloDVqSGqzyLc2NiY1l+ZImzXaQpMjv1eNZAoN4D15s2bEvkmmnX16lV3EerbOvVtQJaApQg7FWEK3LknPRjDah2WTnavTtbXtauV1F+lB1Y2yNy7d2/acNRoktGkrjQgYAlYnQJWhE8kK7uAaqOp82zXeTr2jv1uGghQpaSAQnf+5tGjRwKWvq1z33aMyIE3r0FXGqC4nfc+d+7cR4DFinQ34+rvdcBqoG0NBLCygzCNR7nvyqb5vvrTaEDAEjA7NUQ5mPXixYsFsDCYMZo6z7adp+Pv+O+mgdiKHJlTg9aXX37ZqW0TtAQtAUvA6tQIAVh0Xab7cgALo2qRu851N+fq79VIygm2A6xvv/22U9smYAlYApaA1akRolfNF198MaEoFYeJoeTeY3J0ngKUGthNAwGsOlWI7SCSRV89IUfI6VIDApaA1akRArAI5T958qSAFYZRuNKx7uZY/b0amdVAwCp2ZGVlpVPb1qVj9737AZYCloDVqRECsL7++uvSJLAGLLu460CFKDWwVw0AV9wSAWeh9uDBg05tm5DTD8jpchwELAGrUyMEYH3zzTfTHYM5KieGcq8G1tfpjNVAmxpImjCAxc8AFo2Lu3SuvreAJWAJWJ0aIfpgAVhpyyBgtekkhSPH/aAa2Amwnj9/3qltE7AELAFLwOrUCFHgfvbs2ekZYtkNBHC5k1Cne1Cn69+pHTTAoc8s4tipDPDQ0JjHf/jDH8rmGiFICFqkBgQsAatTI4ORY7dPIlgAVo7JEbB0koKSGjiMBoiOC1hC1CIh6lP/W8ASsDoHrKWlpWm0KuF+4ErA0rkexrn6t+rnu+++m0au4giJYNF/zwiW4LVo8BKwBKxOAQtjd/v27X86hzC7gnSSOkk1oAYOqoHz589P7RvpwaQJ2VyTtOGinaz/v12QE7AErM4Ba319vRyPk/5Xs/cHNa7+nY5ZDbStAaLjgBVpwtl7Aatd8Dkq6BWwBKzOAYvdPqm9SuSqBi6dZNtO0vF3/A+qgZs3b5ZIVQCLyFUdyToqR+v7tAlzApaA1TlgvXnzZtrBnbqrnEloR3cd60Edq3+ndtAAR3ABWEkJClhtgk5XgCtgCVidAxbRK/pfZfdgGgYKWDpJQUkNHEYDq6ur2wJWolhdOV7ftw3QO0bo1JvXoCsNYGgArByNUxe3281d53oY5+rfqh/OOM2B8tg4dg+mbQNnoHZl93zfNnyugCVgdmpkOIcwkSogK7CVxzpJnaQaUAMH1QD1nXUfLGCLW54TdNoAna7GWcASsDoFrFOnTk2bjAJVpAo5R0zA0qke1Kn6d2onGsCeJB1HWpAbUSxu2VXYlfP1fccPd9ZgWYPVaQ3WmTNnSkPR7BoErrKj0BShjlJYUgOH1QAgkyJ3movyOIBlLVQbtVBdjbOAJWB1ClicQ4gBBbB+/vnncs+No3MELJ3rYZ2rf9+2hig/SO0VESvqrnC2PGcnd+Fq0eB1LGFT7/8RPvZ2tNeAcwgDVW/fvp0WuwNbAlbbzlE4cvwPqwEAqz7oGaiKja8fa/eP1u63cr0FLKGqU6ik03L6XgFYSREKWDrXwzpX/14NUX7ARppErlpx7H7PfgCjgCVgdQpYdFomHZgUIU1HKUw1RahzFJDUwGE1gB05efLk5Pjx49PUYHpgpemoMNIPGBnjOAhYAlangHXr1q1SgxWgyi7CQNdhDax/r5NWA+1qgOg4G2lw3qm5ImWYn8fo1P1O/QFGAUvA6hywUtSe1gykCQWsdp2iQOTYz0sDAlZ/YKNF8BOwBKxOAYsUIcXs1ErEqAa45mVk/T86bDXQpgYELAGrS7ATsASsTgGLFCERqxyRk4J3Ilg6xTadouPuuM9LAwKWgCVgCTmdQk6XArx7924BLAxqDns2gqWDnZeD9f+0rSUBS8Dq0r8ZwRLuOoW7tbW1ab+rAFZShjmjUCfZtpN0/B3/g2pAwBKwBCwhp1PI6VKAjx8/nnZyTxSrThke1LD6dzplNaAGBCwBq0v/ZgRLuOsU7p4+fTptNJrzCGnVkGiWTlInqQbUwEE1IGAJWAKWkNMp5HQpwCdPnpQIVo7FyXmEKXY/qGH173TKakANoIHvv/++NBml/xVnEKbRaPphdWn/fO9xA6ARLOGuU7jb2NiYRrDScDSRLO51kjpJNaAGDqOBNBoVsMYNM32EVQFLwOoUsDY3N0tT0RhQIlezfbEOY1z9W52zGmhbAwKWYNUVfB2D6r15DbrSADVYHPJMo9FErAAs+2C17RSFIsd/XhqoAYtUYVKEHJ3Tld3zfdvwuQKWgNmpkWEXIXVXORonx+UIWDrYeTlY/0+7WiIifvbs2WLjtjuLUNBpA3S6GudjiM6b16ArDTx48KBEr2rAqs8m1Dm26xwde8f+sBr4FGB9+eWX+j79/0I1IGApsIUKbDdwu3PnTqm/2g6wsrPwsEbWv9dRq4E2NcDirY5gkR7EJnEvYBlY2M0/Hfb3ApaA1Slg5bDn7QArR+joHNt0jo67435YDdSABVAFsEgZCVgC1mEBare/twbLGqxOa7AALECKaBU36rEOa1T9ex2zGlADaIByg6tXr5Y+WPTAqh1iV3U5vm87dV8CloDVKWDdunVrx92DnkWokxSU1MBhNABgXblypQAWNwGrHbjpA8gKWAJWp4B1+/bt0mi07n+VRqOE9w9jXP1bnbMaaFsD2JKLFy8WuEp6kNRgH5yvn2H8sCdgCVidGhsAK7sIc8izgNW2UxSKHP95aoAi9xyVk9or4Wb8cNOHMRawBKxOAWtlZaUAFrVXKWoHsHjOFKGOdp6O1v/Vpp5OnTo1TQ8GsIhm9cEB+xnGDXoCloDVqaFZWloqacDtAEuH2KZDdNwd93lq4MSJEwWwsouQ+zqiJeSMG3K6HF8BS8DqFLCoj8juwbotQ310zjyNrf9L560G2tLA119/XYDqq6++KnVY3AtYQtVRgJeAJWB1Cljnzp2b/PDDD6XR6Cxg2Wi0LUco+Dje89YAZQaJWNWgJWAJWAKW8NMp/ByFAClABa6ArNRe8bM1WDrbeTtb/197mnrz5s20czuRK2Drj3/8Y7k/Cvvme7QNckawhLhODc33339fYCrRK1ac/CxgtecMBSDHfN4a2Nra+uhoHAGrbeA5auAVsASsTgHr22+/LbsF05ohBpbniGTN2+D6/3TiaqAdDTx//nwKWDQZDWDx+Kidre/XHtwJWAJWp4bmm2++KRCV9CCPU3slYLXjCIUex3oRGnjy5MkUsNKigSNzhJ32YKeLMRewBKxOjQ0rSdKDApYOdhEO1v/Ztq7W1tamgJUdhACWfbAErKMALgFLwOoUsNjNQyEqUascjZMIlkfltO0chSPH/7AauHv3brFvAFVaNeRcwqNwsL5H2yAnYAlYnQIWq8nXr1+XXYQpbCeahWG1k7sO9rAO1r9vW0McJh/AwtakB5ZpwrbB56jAV8ASsDoHLHb6EMXKDkKdYttO0fF3/OelAY7iCmDRniG9sAQsAesoIOsYNTDevAZdamB1dXW6W5DoFTfShIlkzcvY+n903GqgLQ1cunRpwkYaUoT1zkEeA1ld2j3fe/x+V8ASMDs1Mqwi7t+/L2D9mhLV+XsN1MB8NXDhwoVpWjCAxX2OzRFyxg85XY6xgCVg9QKwEq2avdfhzNfheD29ni1p4MyZM8W+JVqVVg2kCrt0vL53G2AnYAlYnRqaRLAELB1/S47f73o0euekCGCGnYOzESwhpw3I6XKcBSwBq1PAojbi3r17pd4qt7qbu47oaByR19nrPEYNAFh0b8fO5D71WLnv0gH73uOGPAFLwOoUsFhZzgJW2jPYpkGnP0an73c6Ol2fPn16erBz6q6wOUTOE9UScsYNOV2Or4AlYHUOWDQDzK7BnEloy4ajc0I6fK/1WDXAWafZjl8DVp0u7NIB+97jhjsBS8ASsNzB5w5GNTBKDRw/frxEqrjVKUJgi58FnHEDTtfjK2AJWJ0aGVaXS0tLpf6KMwnTzZ0IlilCIytjjaz4veaj7dm6zbp+E1vStYP1/dsGOAFLwOrUCAFYV69eLatnAKsGKwFrPk5IZ+51HKsGPtWMGPsh4LQNOF2Pv4AlYHVqhAAsui3jAKjDErCEgbHCgN9rsdqejWZhT7p2sL5/24B3jDy0N69BVxrAAJ07d64AVn3Ic4rddUqLdUpeX6/v0DWQSHc2yvAz9oPzTbuya76vPhUNCFgCZqdGCMBiKzVGPoaS2olEs4Zu/P38AowaWKwGUrdZAxaPX7161altE7KErGPspvDmNehKAwDWyZMnC2BhKFl5CliLdUg6fK/vmDTw008/lcUZNZyAVezHs2fP9G369041IGApwE4FCGBx2j0GH0MZwErB+5gcgd9FsFED89cAdgOo+uGHH6aAhf14+vRpp7atq0Wr79ufgImAJWB1aoQIo3PwqoA1f8ejM/eatqCBn3/++SPAArgArI2NjU5tm6DTH9DpaiyswbIGq/M6BQBra2truoMwu4Fs0yAgtAAIfseD67xu0wBUAVep33zw4EHnts06rLbrsAQsAatzIwRgvXz5UsCym/gou4kLUAcHqN2u3U6Axd9xxqmA0zbgdD3+ApaA1bkRIny7ubn5UQ8s2zQszint5rT8vdd+aBog2k3kKnWcfP5bt251btu6dvC+f7eAKWAJWJ0bIQBrfX39o6NxBCyd/NCcvJ+3G83GVmQHYXrqcQSXgNEtYLR+/QUsAatTI8QuQiYh4fzUXM3e67i6cVxed6973zWQyFWai9Y9sc6fP9+pbWsdLvz+Nhp1AnYMmDlKYmVlpQBWHbmyyF0H33cH7+frVqPpf1UvygJdp06d0r53bN9bhywjWAqwUyMUwCKcXzcaTeNRHVi3Dszr7/XvswbS/6oGLJ5jR+Hx48c7tW2tw4Xf3wiWE7BjwASwOPD5ypUr0w7MGEgBS8feZ8fuZ+uHPgNYsRmAVgCL3ck6eWuwutSAEayOAaPLwe/DewNXfI7vvvuuGMY0HKVgVSfWDyfmODgOfdVA3aahLjF4/vz55M9//rOApX/rVAMClgLsVIABrG+//bbUXwlYOvO+OnM/V/+0mabEjE0NWByTE9vSh4Wkn6HNSJqAJWB1CljZRXjixInSzb0+7FmH1j+H5pg4Jn3SwCxgpYv72tqagKVv69S3AdUCliLsVIQBLA58ZtUZwKpD/30y6H4WAUMN9EcDOwEWu5JjW4wetRk96sO4C1gCVqeAVU8CVp1ZgerE+uPEHAvHoq8a2A6w+KyXLl0qBz33wcn6GdoFPAFLwOqFEaJe4vbt22V7tf2vdOh9deh+rn5pM4BVd3NnjNg04y7CdsGmL1ArYAlYnQJWVpns+FleXi6AVRes6tD65dAcD8ejTxqYBazYD+yKgCVgdQ1aApaA1QvA+uyzzyY0GxWwdOB9cuB+ln7rcSfAIiJOXWfXDtb3bxvyjkH63rwGXWuA1SYGMb2wEsXSwfXbwTk+jk/XGqCk4N27d5PXr19Pfv7558nDhw8n7EqmyL1ru+b7t+1bBSwBsxdGKIBVNxi1Fkvn3bXz9v2HoUHAiug39/fv358ekyPgtA04XY+/gCVg9QKwmAhEsOjAjFOrD33WyQ3DyTlOjtNRayCHPf/000/TJsW0aEhtZ9cO1vdvG/AELAGrV4D14MEDAetXwDxqR+X7ec2HqAFKCt68eVN2HgNZ3HOuKTVYwk3bcNOH8RewBKzODRGFoNn1w07CFK6aItTpD9Hp+5mPTrcBLK45gEWa8OzZswKWfq1zv4ZPE7AUYqdCrLdS8/jMmTMClhEsI3hqYE8aYBH2ww8/TFu7vHjxYsK5pkawjF4ZwRJuOoWbPghwFrCOHz8uYOlc9+RcjRQdXaSor9cawMrpD0S+nzx5Mm3P0Af75mdoG/SMYAl5nULeLGCxtdoUoY6zrw7dz9UvbQJY2RBDepAWDSk3EG7ahps+jL+AJWB1Clipv2IHIc1G6V+TMwnrlg06tn45NsfD8eiDBtKYmM+Cvbh8+XJp0cDJENiUPjhZP0O7oCdgCVidGqG6GSCPiWjduXOnpIgELJ14H5y4n6G/OsRGJIJFLdb58+eLDaEGi3vhpl246cPYC1gCVqdGKFBV37PNmt1BGE6dW3+dm2Pj2PRBA0AWOwhfvXo1OX36dOngbhd3wUrAEm46hZs+CBBDSCifkP7JkyfLPUZSuNJ598F5+xn6rUMWYkSu6OC+sbFRSgyygzDlB32wc36GNoHPCJaQ1ynk1YDF9urUTrAa1bn127k5Po5P1xogcgVgAVr37t0rtkzAahNm+gixApaA1Slg5YgcQIsIFsaRxxS6d228fX8BQg30WwNpLkrE++rVq9MjcoheWYMlaHUNXQKWgNUpYOWQZyYCu3+4B7CuX79umtB+WEK2GvikBgAsarA4Luf7778vUEXJQSLjXTtY379tyBOwBKxOAQtjmBvGMX2xLl68KGDpXAUsNfBJDWQzzNbWVlmgYUtSh5UFm5DTNuR0Of7H4tS8/4dz93a01yAF7hjFrD5JE/I8tRWE/jGirFR5TN+bHOxqIXy/0zem1xyfRWsgndzv379fwIrUIPdpNqo9P1p77vX++HoLWEJV51CZ+qvUTeR+c3Nz2gsrBz8HuICuRRtv/7+AoAb6rYF0cr9582ZZlNUlBzp74aprDQhYAlbngMVqkx2E6V2T4tTbt29P04SJVgFWGFVuQla/nZ9w4vgsWgPYgHRwr+s4Y0O6drC+f9uQJ2AJWJ0CVnYRAlikBoleUTvB/YULF0qUKqvURK/S5Z3eN4s24P5/IUEN9FcD2IY0GMVmsBO53kEo4LQNOF2Pv4AlYHUOWABVWjSkhiLGMl2acXI5d4zneCxg9dfxCSWOzVFoAMBaX1+fLsoCWEkXdu1gff+2AU/AErA6ByyMIZBFijARrOz8oA6LNEA6Nid6le7NR2HEfQ9hQQ30UwNEtZeXl6e1VzngeXZnsqDTNuh0Nf4CloDVOWAlTRioqo3jrVu3So3F27dvpzsIU3fBzkIdXz8dn+PiuByFBrANp06dKgXuiV6xWMOh5r4r5+r7CnUCloDVOWDlzLA6rB/Iog7r9evXk3fv3pWCd6AK2MJ4W+SuEz8KJ+579FdnlAqkrOC7774rdZyAFnCT1i+CjqDTlQYELAGrU8Dai/BfvHhRYIrVal1/lZosHWB/HaBj49gcVgM5Die7h5n32U28uro6CVgFqDjPNKUGe7EvvkYAW5QGBCwBq9eARdqQItZEqzCuiWQBW4c13v69AKAG+q2BAFZ2FDPvs+Ci/ir1m0SuiHxnF6EpQsFpUeC01/8rYAlYvQasnEuIccXQUtyeHlgCVr8do+Di+BxWAzm1oY5Wp11L2jOkdpMIVo7Lyf1eHaGvE8YWoQEBS8DqNWBRU8EhrtRhUXsFYAFa6Y11WAPu3wsBaqC/GqgBK82Gkyp8/Pjx9BzVdHDPvYAlMC0CmPb7PwUsAavXgJXu7k+ePClgVddf6Bj76xgdG8dmXhpIOpD/B2Tl/EGOx0mtVVKDSQ/WwLVfp+jrhbN5aUDAErB6DViE/ylapV0DxjW3rGLnZcT9PwKBGuifBgJUiV7l562trcm5c+dKzVWK23MSBJDFzWajgtK8QOmg/0fAErB6DVgYSQwn7Rowqmk4muajOsX+OUXHxDGZlwZSZ1lHrlhcPXv2bNpcFMAKVJEazKJMwBKwDgpG8/o7AUvA6jVgYTxJE7IVm3YNpAmpxxKwdOLzcuL+n/5qKbuG0/cO4AKwnj59OvnTn/5UolTcUkqAncA5EvUWsASseYHSQf/PsQjU+39MVG/9ugasTDkImvvbt29Pm4zWZxTqIPvrIB0bx+YwGgCmAlcsrpj3nEF6+vTp6Y5BbXa/bLbj8X/jIWAJVb2GyhSxcr+0tDQ9LoeVbYzvYQy4fysAqIF+ayD1V2kuSiSbRZeOXLDquwYELAGr14aK0GyO0jlz5sxkY2NjupswhlcH2W8H6fg4PgfVAHOcqFVqsGjVwu7BHBDfdwfr52sbAgUsAavXgJUuzTGopAlZyWY34UENt3+n01cDw9BAvXM46cGcNSjAtA0wfR9/AUvA6jVgcfwFESzu2R10+fLlaTd3HeQwHKTj5DgdVAM5IittWd68eTNtwZCjcfruZP187UKggCVg9R6wMFDUXLBqPXXq1IQOzjG8BzXc/p1OXw30XwMUtqfIHch68OBB2TFIZJsdxsJLu/AyhLEXsASs3hspjCmr1fS4uXHjhgXuv3a1FhC8BmPXQMoB0s39/PnzZaGV5qJDcLJ+xnYhUMASsHoNWNlFmBUr/W1oOpri17E7GL+fENW6BrJjmHNIA1fpjye8tAsvQxh7AUvAGgRgpRaLru4ckfHo0aPpgc+sbkklAF0xxvm5defk9xfQhqwB5jOF7e/evSu7BwEsFlssvEwRCld9hywBS8DqNWBR2F7XW1B/gWHNbsIcpYETqXcbWaMlWAwZLPzs/9BvGotS3H7lypXpqRM5a7DvDtbP1zYEClgCVq8BKyvWtGng588++2xCLQar2/TIqXvlxDnZiFTIElSGrQHmMNFoitvp3s78Z5GVo0sEmLYBpu/jf4zogDevQV81gCElPcg9k4nHnEFGFGt9fb2kCbklepXIFT+TWtDBDtvBOn5tj19S/USvssjKLsL83Ffb5efSrwpYAmavAZsUIS0auCctkMNc+fnSpUsFrOjuXO82SmqBug0ddNsO2vEf9vizSHr+/HmJXjHn0w8vu4qFGCGmzxo4RiTAm9egrxrAqAJVrFZz8HN6YvGZMb4YYW6kCZMW5LGANWznKhw5fmjg7t2707YMzPm0bemrzfJz6U+jAQFLwOw1YKfJaNKEdU8sHrOzCJgiipUdhOmZkyaFOmodtRoYpga2trZKvSV2ILsGA1nZ/CLQCDR91YCAJWD1GrCYOMAVUSwe5+BnniOSRcuGV69elQhW3aKh3l2ocx2mc3XcHLdnz56ViFVqMYlop8lo3ROrrw7Wz9U2/FmDZQ1Wr2uwdsuvs7JdXV0tO42SJsQx87O7CHXQQlr/NUAdZcaJhRENRblnDl+8eHFy5syZ6UaXbHbJ7rHd7IO/t0arSw0IWALW4AHr+vXrk9evXxfAmt1NqIPtv4N1jNoeI+ZsFkN5zP3Lly8LXBG9ThQrOwfrNGGXDtT3FuA+pQEBS8AaPGB9//33k4cPH5adhKm/0mm37bQd/2GNf91qhc0pRLDu3LkzPYM0fa9m664EHAGnzxoQsASsQQNWjtC5du3atOtzUg6mCIflZIWi9sYrczRtVpi7Hz58KHWVZ8+enTYUTWqwz87UzybszWpAwBKwBg1YpA84ABpjnJYNaT7qcTntOWwhbVhjnrnKfTamUHtFE+G6W3scFynCPJ9THgQbwaavGhCwBKzBA1bOJ7x3716pwUp7BgFrWM5WOGpvvJLWT3Ng7oleLS8vlx3DszVXQBU7CbkJWIJVX8Eqn0vAErAGDVgpdsUYs+OIwljrsNpz1MLZMMecucqCKGeJMo5ra2vTcwfT/yrzPICV5/vuYP18bUOggCVgDRqw0oj0888/L32xNjY2yo4ktnrX2791wMN0wI7buMctgEUPu+wgpHlw+l1tF8FK/yvmuwDTNsD0ffwFLAFr0EYKsDp16lTZbfTZZ5+VxqMvXrz4qPFo6jzirNPKQQAbt/MWzvo/vnVK//3795OnT59OG4mmPUPfnaifT8jbSQMCloA1aMDK7qIcpcOqlhRD6q/qIloBq/8OVyhqb4wCWczVW7duTeuuBBfBZegaELAErEEDFkBFFIsaDVa8pBaWlpYmb968mXZz365dQ6JYOvT2HLpj3p8xTzqfs0Q5FoeedqQF67NHh+5k/fztgqKAJWANGrCAKnYRYsR4TLE79zQepbO7zrQ/ztSxcCxmNZD2DESciV6l9kooaRdKxjT2ApaANWjAymGqKYZlcmKkr1y5Ug5/1qnr1NVAvzUAXKWxKHOXqHQWSmNytn6X9qBRwBKwBg1YgBVF7tzPGmeaFe6UCpwtfNcJ99sJOz7jHR/mKD3s0noBwCIqzbwWStqDkjGNuYAlYA3aiGGUSQkSycr27TQgvHTp0hSw6h2D2xW+68DH68Ad2/6OLXORExjOnz9fFkjUXuFg7XMlWI0BtAQsAWvQgJXDX1n1All13xweb9eSQcDqr8MVhtoaG9KD7PrNETjMYdKDzGeAawxO1u/QLiweSw2L9yeKg/Y2rmtALRbd3euC9xzPYRF8W85ceDv68c4uQeoh01Q00eR3796V6BW960gJAlWJRhPJ0haPyxa3OJ4CllA1akPGtu9Hjx4V454jdNIx2rMKj97hCjntXfMc4sx8S0Q5rVPYOQhUkdbnHrCqj8dp0Sn7nccDlgKWgDVqwMJwX79+ffL69evpmWfuLmzPyQt23Yx5QCoHsHOEVRY79L3KBpX0scsu4KT+hY3xwEaLYylgCVijBqzsMnzw4MFHUSwcrkfldON0hZ12rnuixLknmkVqkEXOyspKqbdiEUTkqi5wT91Si07Z7zweqBSwBKxRAxYrZOo72FFIFIuO0VlVC1jtOHqhrpuxJnJVbzThZ+Ygh7LX0atsUOG+3rAibIwHNlocSwFLwBo1YJ05c6YcpcPqmN1KONqkKwSsbpyusNPOdQ9gkRpk3IErjrEibZ++ddReZRch8xTAMkUoWI0ByAQsAWvUgFUbb6JYm5ub/5Qq1OG34/Ad66Md67REyS5CUoWrq6sFonLmIHM0va8CV6QNiXCNwcn6HdqFRQFLwBq1EYuhzs4kOkYnipXdTTrdo3W6Xu92rjdR4uwezP3Vq1cnn3322fRw9rolQ47JoTbr7Nmzo7ZNgtf4wUvAErBGbcROnz5dvl9WzKyWqf/48OFDSVWkPoSeWDgAUhmsuklluNuwHRAQ+hY31tQ+ZucgCxyAKoXtQsb4IaPlMRawBKxRA1ZWx0k9kIq4c+dOAausqOv0RaCK2hEBa3FOV6Bp49qm9oq5RlsGGv/WR+G07Hz97uOHSwFLwBo1YNUHyOa8QjpHP378uGwXB7RwAjVwpfhdwGoDAoS9xY0zUeL3799PSMffuHFj2orBNgzjhwsB8sTkWPqPeP+PPizexnUNUkjLZK/77CwtLZUIVaJYONm6Vw8OwV2Gi3O8Qk0b15Y5xEImbRmorYrjBbK0t+Oyt47nx+MpYAlVTRi5escS28OpxWI3U8AKuEp/LOCKm0fptAEBwt7ixpnaxrotA/MwR+Jk0aNTFrLGqoFj1KZ48xqMVQMY8xS4Y9Apek8NyPnz50vbBlbZsw0RkzLU+S7O+Xptx39tWbTQf44zQSlsZy7SfiEOdax2x++lT0UDApaAOXrABrKAK8CKx/XPNDx89erV9JzCHASt8x+/83eMFz/GdWE7UDUbvRJEBJExa0DAErBGDVg5RDZRLCCLKBaGnlQhq2nqQ1hpU5NVR67sk7V4ByzkjPsa379/v0SvciZoWjSkDmvMztXvJjxag2UNVhM1WDvl+DH8FLzXUaw0Is0WcyFg3BDg+B58fKlTTL0iveO4lvzMYoX0+1hra/xe1o3tRQMCloDVtBGkLoSI1t27d4tjoCg3xe651wEf3AF77cZ97ZJSZ65k1y3Q9eLFi8nNmzebti17ccC+ZtygJmAJWE0bQSJYpAo5luPp06dlS3k6uruLcNxwIPwdfnyJVOXwdHYLZlHCDt3UWwkR44YIx3fn8RWwBKymAauuzaIRYo7KwVlYg3V4ByzEjPsaJj1YtzYhNXjx4sWycNH5Clcta0DAErCaN4IUumeH4YMHD6apDju5jxsOhL/Dj+/scVPMGXbmpu9cy87V7y5cClgCVtOAlRosjCFdpll5s7W8bjyqIz68I/YajvMakh5MUTs1WOvr6yU1+Pnnn5fdg0KGkNGyBgQsAatpI1h3eAe2iGTVvbEEg3GCgeM6n3FNcTuRK9qdXLp0qTQUZV6xeaRl5+p3Fy4FLAGraSNIDRZQRZqQGytvnqP7tLsI5+OEhZnxXkfAKj3kbt++XeAqUWGPwhEwWodMAUvAahqwMAA4AlbcgFUakJ47d6708WFX4evXr0saJDsMc36hNVrjBQeh8B9jy6YPWpdwQ+/c8jNzIhGse/fuFTuSswaZS607V7+/gClgCVhNG0LSGNlJmKM8WIHzmFQhcBWgSp8ffq4f64wFrbFqAICq04A56YDnssuW1OCVK1dKJDgnJwgXwoUaODkRsASspgErB8+m/oqfcRKBLFbmOE+6VNetG7I9fayO1e8lNKKBwFUWFNznSCl+xwkI165dK3DFDaeaI2J0sEJW6xoQsASspgGLlAaOIfdJE/IckEWqkBV6tqPjYHKzRksIGTuIJkqVXbX1jkG+OycgkFb/4osvysIkdVc2GRWuWocrvr+AJWA1DVhAFE4hNSNZhcdB8PvLly+Xoz9y1lrSJKYJBawWAKuGK74vNViAFicfnDlzZrprMDVYzKf0ltPJClota0DAErCaBqykM6jFwilki3l2FdIbi+jWnTt3pnVXgFX6/4zdwfr92obIwBVpwRS8v3//fvL8+fNySDrzI7twZ2sZ3UUoXLUMV0awhKum4YoJAFilMJfHqSUBsIhipR6LVOHDhw+nR+kIHm2DRyvjXxe5Z7MHtYi0ZGD+sCBhrtQ9rxINdiehgCVgCRnNQ0bLkwDHwCo8jqKOaAFY2WUIeF29enWytbVV6rEschewWoCstCIhPU6bEnRPj7jz58+XswaZL6QJ635yzJtEtVq2LX53AfNYHIr33053v3gtvBbRQPr6JJLFdnQgC2dDqjCgRcqwLn5PUXwLTtjvOFzYRMdpO4Jm0XF+JnqVeit+x+MnT56UI3BYlGTxob3UXqqB7TUgYP26AlMcXoNPaSCpQlblpArv379fWjbgjD61kxCnJHwMFz5aGTuiVPVCoQas1Buic87opDdcXcCu7dR2qoGdNSBgCVgC5ic0kFV63d+H9MiDBw8KPLGqTyRrtl6F37XipP2ewwTJaDf1VYm8pj1DCttZUABXaWOSDSE6VwFLDQhYQoQgeSANpBEpRiRNSEkXUo9FugSHlJ1WAazcG8EaJnS0BIvRb+757sAVt7QhIcLFggL90+8qcyHHS+lghSw1YIrwQA5W4bRtPFKoSpoQB5OdURT2Uo9FJ2ucU90jqz6rrSVn7XcdHlCS5uZWLwZYINQLh0ePHk0uXLgwbchbR3O1j23bR8f/0+NvitDIjuD5CQ3UnakDWWnhgKOhkzVF70kV1ofhehj08ICjVUis09wsFrgBWTTYXV5eLu1K6n5XaD8LDp2skKUGjGAJEsLkvjUAVMV48Di3OpJF0Tu1KjlGJBGA1LG06rT93v0HzOx8TVqwjsa+fPmywNXZs2c/Osg52q/nhg5WyFID/6wBI1hCx76ho6WJxJb0OJR8b1byPOZ3n3/+eanHYodVnFR9GK6Q0X/IaHmMUi9Yt2oAsoAr6q7QeiK3iWKl0D1HTLVkD/yuguR+NOBROTZatdHqITSA86HhIu0bOJsN5wRgvX79uuwgTGSLSEH6CiX9YoRL+Ooa7tAkGo0++Tw8R91VzuW0YaYNM9XAwTQgYB3CuSq6g4luTNctOwsBLdIpnNGWwmFAa7ZPFo4sES53GQpYXQNWGogm+kot4cbGRunU7lmC2rcx2eouvouAJWAZwTqkBtLKAYd048aNkir88OFDuQewst09PYaIbgFZ2XnYtZP1/dsFvURVsxAgNXj58uXSjoFFQxdOyfcU7MaiAWuwrMGyBusQGuAMQ2qxaEhKqhDYWl1dLQCV43OSggGostsQ6LIRabtg0yeozc5X7qknRMc5CH0/9Sa+1vokNfCxBgSsQzhXxaRBSRFwfSg0u64oECaClbPceAxwpf4qtS59crR+lvaALxFWFgG0HEHPLBro8+YuQe2bPu5wGjhWbz338f9tw/daeC32ooHUYNUrflKFpFkePnxYAAsnNtsdO7u2hJr2oKZPY57i9nv37k1rroArHCs63ssc8DXaSjWwvQYErKq3kSLRUBxEA6kXqBuQkja8dOlSKRjmHLdAVtKFOZKkT87Wz9Ie7FFzxY5BdsF++eWXJdVNBHa2we5B5oV/oz1tXQMCloDlKvUQGuA8NsCq7pdVH6tDJAvIyvE5iRhke7xQ0x7U9GnM19fXC1CxSEjkiugVj4Gt1h2k319IPIwGBKxDONfDXHj/to2Ji+PizMIcDJ0id5zsu3fvSq+s3NLCwUalQtdeISwHjdc7VXO2IBDPMU6p90vfNX7PTta1tTUBSvuvBhaoAQFrgRdXiGoDoj41zqRaiASwO4toAU4OsMLJkTrMobpxlKnNyhEme3W0vq5NKKv7rKUrewrXA1S1ltAZuuN4p4sXL+pctf9qYIEaELAWeHEFLAErxcKAFs0bgazsJkxtVu0Y4zAFrDaBab+gXEeu0rg2BzfXh41HY69evSq7Bam5sohd+6SPWqwGBCwByxXMAjWQOha2v2d3IZCV3kNxhu4yFKj2C1d5fQ4Xr+E8v2NTRX5PQfvNmzdLRJXUNXWDOtjFOlivb9vXV8BaoHN1crU9uVLsnt2F6SlDJIsUDRGsgzpV/04gQwN1ajma4DmipGkRAsRvbm6WUwbQYg4rT68r7ZR2Sg0sRgMCloDlKnaBGiBqlVRMIgc4OHZurayslGJjolc5E65O8whRQtRuGgCkUnMFSAWuAl5AFoeQs9EiWszJA2mSq3NdjHP1unpdBawFOlcnmBMsPbKIXkUP9XOkbEgZAlpJ5SRtmPqa3Zysv28XxHK+Zb1DkOdIQZMSRFtLS0slJUhLETSY1KA1WNonfdRiNSBgCVhGsBaoAZwZqZi643siCDg4jiWhISmOkJThbKsG4aldeNrL2KeBbQrakxZMA1EcKJGq9Lkiigpoob0LFy449xc494WXxcLLEK6vgOUE08h2qAEcX2q0qMvyCB2Bai9gVfdOq4vbiV4RzUJLNgrVwQ8BQsb8GQWsDp3rmIXld9ubcRewBKr9ANXsa3MyQMBcwNrbvNM+eZ2OQgMCloBlBKtDDQhYApaApbM/Cmfvexy9zgSsDp2rgj96wfftmgtYApaApR3om13y88xHkwKWgGUEq0MNCFgCloA1H2cmFHgd+6YBAatD59o3Mfh5jt5ACVgCloB19PNOW+c1PwoNCFgClhGsDjUgYAlYApbO/iicve9x9DoTsDp0rgr+6AXft2suYAlYApZ2oG92yc8zH00eowePN6+BGuhOA2k8OtsHK/2NDuOA/dtxA9xubRqc193Na6+9117AEjAF7I41IGCNG4IWCbkClk5ckOuvBgSsjp2rk6O/k+OoxkbAErAOCmEClvbjqOyU77N/rQlYApYRrI41IGAJWALW/p2XDt9r1ncNCFgdO9e+C8TPt3gjJmAJWALW4ueZtsxrfNQaELAELCNYHWtAwBKwBCyd/1E7f99v8ZoTsDp2rop88SLv+zUWsAQsAUs70Hc75efbv0YFLAHLCFbHGhCwBCwBa//OS4fvNeu7BgSsjp1r3wXS+uc7e/bsBADiOnz77bflRhM+fj59+vT0Zx7zHL/jNfl9/dqAVP06/i7/586dOxN2hf3tb3+b/PTTT5ODOl3/ri1go18auvnxxx8neby6ujrVYDTGfTSILmlyG91mnue1+X10nL/l7/M/Zv+2dVvh9xf4ZjUgYAlYRrA+oYHtAAnHEpA6fvz4FKhqcAqMcX/x4sXJ3bt3JxsbG5PNzc3J2traZHl5eXL+/Pni4NI1eX19ffLzzz9P/vKXv0ydpbDUFiztd7zRCnDFPbeAeQDrxIkTRWOXL1+eavDp06eThw8flp9nFw8Bqmh5O02j14CYUCFUqIGdNSBgCVgC1ic0cObMmQm3OvpUr/ZnIwKJCly4cGFy9erVybNnzyYvX76c/PDDDyUiRXSKG49xjDdu3Cj/H0cIYL1//77A1Zs3b6av26/T9fXtQFkNWIw7gI6u7t27V8CfCOzt27cnr169KvCF9gLwvA7YAsbQKtCEDtHwdhCVRUW9eNC5Clhq4BOAVYePffyPdI03r0GdKkmEKat7gOjcuXMlAvXNN99MUy38fO3atQlH3jx//ryAEk6PtE19C2Rx//r168nKykpxbKQIcYJJ8wTEBKZ2gGm/Y41GopcAPDAPYAH/QDsa+/Dhw+SXX34peuRntAmMoTdACwAjwop20XCgv47g1mlCbaQ2Ug3sroFjXqTdL5LXyGtU15vUK3mcWJ1+2draKhECHN/bt2+L85qFrNTLEKUiYkWU68qVK8UpWoMlTO0HsgLhSRHytwDWo0ePSnSU30dvtbaAMv6G+9T8BbZevHhR0ti3bt0qi4i6DnF2kaFt1DaqgZ01cCxFi97/o3jTm9eg1gCpPhwMUJX0Cb+nrmppaanUVLH6D1TVDo8oAZCVuhgcXICL5/I7/ib1Wfl9Ujn7cba+tj04S7oPqErEFA0RpUKbdToamEJ3iZIGsOoarhy9k4PGnzx5Mnnw4EGBNebC7GYP7aX2Ug3srAEBS6gSKj+hgbreitX89evXy+qeuqraGcUhBaL4mRQMjg7nhzNLJIt7fs7z7969K84xdVp5bRym4NQeOO11zFODNfv62cJ3UoToLBGuLAjqyBcwlrQhr0N/iWqhd6JiRLWItrLomN1ZqKMVttTAxxoQsAQsAWsXwCJaRaEwBcFAUNIqSbPMHrhbw1QiBrmv66+SvsHx8X/zv/P3ud+rs/V17YFYolJEQ4GjaHIW6LOpIhpJTSB/Vxe+z2ouaUU0mpQ3kTHS2Sw2dKhClRr4RATL/Kk59JY1wI4pCta51VvPKfIlBUixOinAwE+dYhFo2gMax/zjMWd+kEK8dOlSmT/Mm/TQSpSrZfvid2/bv1rk7q7BpndNpgUD/ay4pWCdaBWreVJ8FKPXBcFJBxphErAErn+0GyGFSG8tarWIaLC7VrhoGy4c/9MTAUvAatoQErmizgq4osaKOpPA1E67AJNuEbAELAHrr2Un7N///veSamQH4s2bNwtkGcESsFqHLAFLwGoasOqO1fSjys4/4CnNPtNrKJErnapgpQb+oQHmCxGsFMeTQidtSCS4PlKndUfr928TNgUsAatpwEpPKwwgO6TSgHF2h2DgKsXBdUd2na3A1aoGmA+BLBYlgBb1igBWjukRLtqEC8fdFGHTcOEE+L/Db0kVsjMqB+ZmG/us45xtzNiqY/V7C5VpO5I5A1gRwWLusEHECJZg1bqPMYJlBKtpyMwWY5oo0t8qEar0r0rjz9lu7HmdoCFotKyBnJsJWKVvFnODw8zrnbmtO1q/f5uwKWAJWE0DFjVYpDJIFXIWYPr+zDb5rM8StMhdqGoZqma/eyJXNWCZImwTKATJj8ddwBKweg1YtFGYvW2Xekixen04bd0Ar/4feT59e/hbdhASwQKwaKqYVIeOVJhSAztrgJ22qU9kzmQnIU1Iv/322zJ3mW+ZnzzHHI0jnm1SmXnKDkRuOZ4qf5O5n/+nQxfk+qwBAUvA6jVg1YYZ45zDZmNoa6Da7ugO0hS8NmcJYrD5P4ErfodRv3r1aunjQ2ow6cCc46aDFbLUwM4aIHKVOqykCh8/flzaNdDIN3DEPAOauM/h6TWAMS95PfcBse0WWHnOGi/hqs9wxWcTsASsXgNWDUgxujV0xVDXxpnnMOREpWLAMxF5nq7T1IjQloEGiVtbW6UwNyvw+sBlHatwpQZ21kAOm05KPXOInzkBgblE2waOmmIuA1AscpjDzMXZCFY9n/O6nAdaL64CWX13sH6+tiFQwBKweg1YgaQYqtqwBqpYIScSxe8xxBhymodyz+84oJYaK1bWQBV1I2nFgFMg1ZFz2eoDm3WuApYa2FkD9WaPuk4xj+s5xnUEukjFX7t2bVoEPwtQiXSlCXD9+9nHAkzbANP38RewBKxeAxbwlFsiU3WaYbbGAwPMc+wKJFJF2u/JkydTqIqzZOWdA5br+7r/Vb2DUCcraKmBf9ZA3bYkKfXMm516x+V5XgdssfChrQPzOucYphwgEa7M89l6rr47WD9f2wAoYAlYvQaspAViqGZrsmoDRrSLlfH9+/cnm5ubpZ6KVAUwFXDCCeTAZh4TteLn7AzkOZolcj+7k1AHK2SpgY81kOOictpB5lHqGLebe/VrM/eYqxsbG6UXXaJbOcKKSHRqJ+tid2uw2oaXIcCrgCVg9RqwWNWSKqhrsTCyiVLxe1a/q6ur5Ry0NAhllQxUBaDq1XVSgNzHQfA4MJbXEtnSoQpVauDTNViBqcDVdjWMmXPMt9zyXBY62b3LvGOBxLmgpPYvXrw4Pdcwm1pSEjAEJ+tnbBcEBSwBq9eAlfqqpAVTpM6xNqT/Yqx3agSaFXSdtqhrRTDm/MxKugYu/p+HOQtXwtWnNZAjo+q0Xz3XAlG5jkkpzkaSZ8/5rOcom1CY67R+IEpNZCs7DYWXduFlCGN/bHYXhz9//087W7wmO1+TOoW3Xb1E+tlkx1CKVIlApdPzdj2s8nvSA/xfolRJ/XEIMwbbNgoCkAA0bg1kM0p2J/JzWkDQzLSuzZp1uIly1anEulVE6rl22qW43S5HfYH+cT8aELB+dd77uWC+9uPrxYoy0aXttlwnAsXrKDxPqq9uFBiY4jXc8hoMJnUZz549K7uPUhM1u2rWyY7byTq+bY9vHfXKoiqbVEgjztZtxeZgR2YXdtiolB2klUteV8Na3WtLm6+PPKgGBCwB61CASX1EDUSzK8RamABTXUMRQ8fzaTAIhN29e7fs+gtI1emErGRnUwo64badsOM/zvHfrng+NVvv378vG1LS/gHYonSAuq3aJu3UPyu2aKcIuhEsweqgYJW/E7AErEMBVr1CTBuFeuWYMP1sB+e66zoGkfQfDQlZmQae6oLYnR7rWMfpWB1XxzUa2K5Avi6Ux14AWoEtFmeJbCXCnjRh3S+Px1nY7XTE1mEdrH/fNqQJWALWoQCrrm+ojVeiVXVaMEYs7RTo7pweVHUxem1Q64jVrKG1T5VOWBAbtwZiF+pNLHVke7tmwdiM7CKmtIA+ePTa2i6yVZ99WKcK01FeQGobkA47/gKWgHUowKoNVH0sTR12x1hhvDBwnE+2vr5ejqepe+bstKuoNqZJF9TP6WDH7WAd37bHdzZFWLeAAKSIWmURVreJyGLs73//e2nVAqjRxoXdiNggmhDnTMS6x95s373DOlj/vm1AE7AErEMBVp0CrMPwRK4AKowXOwCBKnb/1Y08tzOIWakGoupGodutYnXAbTtgx3/c41/DU33uYSJbRMBjI7Zr1cLvgK3YnRxGTTkCaURaP3DQO1F1bFXOO00xvIDUNiAddvwFLAHrUICFQUo9VVaCGCsMF3VV6SVVn1k2e5TGbK1F3Qh0uwhW+lXZaX3czlV4cnzriBXRKqJRddRqJ7uSiHi967BOJ+Y0BzRGzRaNinNcT+yZRe7C1aEBawjNuvyMi2smV7dYyHWua6koAq2Pp0htAvcJsXOfuiq2TNNWAcPFLh+dpE5SDaiBvmqgXgACYMDWgwcPprBV98iqWzzE9s22gYhdjGPWdy3Odw3h2trJvfFO7tlFE8MwC1yzvamArYTR+RvSgGyNppCUcH1WjKQDqXnoq2H1c+n01YAaqHvr1WeQUhzPQpHi+OXl5Y86yOdA+cBVvTBNY+Vs8hkCBPgZFweBAlbjgLVdCLSecLMrMQwIUMUOwLW1tVJXBVhtlwrUgenA1IAa6LMG6p3I9fE8deoR2MLW3bhxoxTHYwNzXM9s4+S6SH62YF6QWRzI9PXaWoPVeA1WvfqqU4NZiX3zzTclRUjROnVV1CpwECtAlRVfbYxmC9H7bFz9bDp/NdC2Buqee0Sz6qJ54IsyB+q+iMzze6Ja1JZSGI+NTAYgUa3Zg+kPW8Pj3w+7DkzAahyw0nl99szA1Fpx3hfbmjn/6/Xr1yXlVx/sOtunJtuqZw9O1pG17cgcf8e/jxqoN9TUO5jTAiIpRBaTwNa7d++KDaT8IbsQcyZifUoFoMWiVEAaNiAddvyOpY+R92dK0XZrNwxBCjazCiMMTrSK2ir6VbGqA6pmt0zXUSwMFcZodst0H42qn0lnrwbUQNrEzLaGySIxzUrTHiKnTGQHYupNga06qlU3X27Nn/h9P2YIAatBqKonQVZdPMdKDKjigGVAqd4WjbHBwFBzxe9yPEW9jTotFTBYOYxVR6YjUwNqoM8aqGuvatuFrSM9yO+xeak3TbuH2UPnWYiSQuQsVaJXpA8FjvaCFvWYC1iNAxZioJ6gPguwTvMFqAJSSf3FyOTn2bMCPcZGp9pnp+pnU5/RQABru557ec12ULXT8V6UUgBalFUIWAKWIhgwZKU3C+0Uku5LsXp+5p7fM9lJA/L7ixcvTq5du1bqqlh5YUjq42pSZ6Uj0hGpATWgBg6mgRw8vbKyUuwzGQNu1LxigynHwDbXp2DwuxTL57DqgFqeTzsdAa7fAGcEa8BwxeRiknKfictErSchEzEF67yOFgs0A3369Om0vqpusTAb9tawHsywet28bmpADSS9GNCivQ02GFtMP8EUUfNzFsJpA0GKsT6KLBuReA57f+LECYMjPfffAlbPB2i3FUodtUo0K5EqJmgOWuYYCNKAhK6pJ6BQMztidAQ6AjWgBtTA/DWQcxCzQYiSC5oyp4FpIlfpmYXtDkhl4ZysQ+0LajDbzUf4++6iXALWwAGLyUMYmaJKYKvuIMxEJQ1I7yqa5SUFWKf/tjut3vqp+RtanZfXVA20p4E0Yc4ubECrLppn0UsDU+x3TsnIopjsRA1bs9GuZC8EqO4AardrL2CNALCSj8+5WUw8VkislGizkHYK7ArEyKedAo/r1gt1wbqQ1Z4zEAAcczUwXw3s1DOwPoyaOtj19fVpp/hErshAAF51fe1sx/LdHLy/7xa+BKyBA1baLNRH2ABWhKKZxGwzBqLS8yXglJ0z2wFWmu9pbOdrbL2eXk810JYGZgGr7hWInY395TGgRYscFscskmPbs3BOfW3qaqnhEqC6Bajdrr+ANXDAYrKlzQL1VfWOwDTCq4GJCV8Xss8ecyNcteUAdPiOtxpYnAbSeHk2IxA7O5tCxDYDWiySSR/SmzClH3XqMLvCd3Pw/r5bABOwBg5Y7EphMtZd1dOTKrn+tGDIz2kUapH74gyrTstrqwbUwE6HSdc9uJJd4D4L4ES+WDSz65t2DqnBqs87FKC6Bajdrr+A1TFg1UWM2UHCfXaXkIMnXJxzrQgXs6p5+PDhNFqlIdeQqwE1oAbGp4GcoPH8+fNph3hSg/gBfEf6aMVnzLZ1cLdhtwAmYHUMWIR9MynSr4r7NKJjMtHvhAlFqwUOGKW+KudjaVTHZ1QdU8dUDaiB2YgWmYfNzc0CWiyy8Rt1K540Ja0ha7tFe+1zdovA+PvDAZqA1TFgMRnqIkYmSTr7AldMBiYTYWJWMSmMzHZfDbGGWA2oATUwTg3UB0/nbFh8AI2iU6MFBAFa+AvSh2Q8uMev1H210qCU4nn8ivB0OHjay/UTsDoGrLRYqEO59VEJaQ6aPipEriiCTM2VhnWchtVxdVzVgBoAsLD1FMMTweJnQCv1tEkdAlRAVlKH6QYfkKpb+QhYiwerwJeA1TFgJR2YFQfpQCYDYWCOV5htr1CHjbNLUEOsIVYDakANjF8D6VVY9zJM6vDWrVslcgVoAVHppxUfs91ifi9RGF9zcCATsDoGrBQs5gBmuvpSZ8WKhR5WWa1kN0rdoE6DOn6D6hg7xmqgXQ3knNikCgNYaCI9tOInAC18x/Xr16cpwtnMSDZVeVj0waFpP8ApYHUMWEwAaq6YFI8fPy5hYKCKyZNeKTk3kPuEi7mvj7zRCLdrhB17x14NjFMD2Pn4gbTfYayBqg8fPpSUYcYe/8CNEpIHDx6UTVEcLJ0d6HW6EMBKMfx+gMHX7g/MBKyOAQvxU2eViBUTJs3peLxTg7r0S9GwjtOwOq6OqxpQA7OL6PycXeR1Z3j0kkhXanVv3rxZICsF7nVxu4C1P1g6CFweq0OGPj47PVxzr9eiPidqdkssBYfkwtNygfsUGCJ6moRqRDWiakANqAE1sAgNEOl69epVOX6HnlmBhLr/Ij4svRYT2Zrtp7VXf+jrPmYIAetX6DmMKAJVaa9AkWH9/wApfqbwMId3InZ2fyS/voiJ5f/UYKsBNaAG2tYAPROTYqSHFjW++KwUvpNBwWcFvNIlPqB1GN/o356dCFiHBKxaoCkoRFhZFUS8PLe8vDwtYCdXzupCA9i2AXT8HX81oAYWqYH4GtKHRLNWV1fL+bVp5QBcJUCQ4nf8Fr5NSDpcAEbAmgNgIUJWBMlvJ4qVvDdRLI62ofgwhyunWH2RE8v/reFWA2pADbSrgffv3097ZqEDNlBxv7GxUUpUErGq7wNb6QwvZB0csgSsQwJWuuXWKcKcIwhw0ZskuwPTfT2FibMF7BrCdg2hY+/YqwE1sCgNZGGf/592D/imlZWVaTQL0KJWi/vUDwtYAlZnocy0WaBIMEXsPHft2rWyOxCYyu6ObKlF3OnIu6gJ5f/VWKsBNaAG2tZA/E96ZqEH/E9OBuFnGlqTNqx3G+LDZuuJBa39g5YRrENGsACrnB1IxIrHbI2loDDnBgJT9S2rCftYtW38dH6OvxpQA4vUQHxMoCp+iPes4YvfkzakTjgF70SyhKr9Q1V9zQSsQwJWQql0ZAe2aPBGrVUdiq0n0Gz/kkVOLv+3xlsNqAE10K4GduoEH+CqAYzXUgSPD7t8+XLZ9S5gHRKwDtI8q7W/SQEg35uaq0SqCKlydiC/p9aK1gsIlt2BaRiqcWvXuDn2jr0aUAN91kBdB5zHNL1mUxZlLvF32RlP6hCfR30WuwxbY4H9fl87ue/SyT07Kbiw2bqa7auIj6gVfa1evHhR+lolQpX0YJ8nl59N468G1IAaaFcD6ZFFuhCwyk5Dnieadffu3RLNSoNsfF/AKhu89gsdLb1ewNoDYNU7BIEsfkZkwBWkjxDZ/krkKkcUcJ8tsRqwdg2YY+/YqwE10FcN5NzbuiaL57glC0PKMOcZJsjAvUft7H7UjjVYu9RgIaLAFORNrRXhUnqIpJA9OzLqXYK2YNCo9tWo+rnUphpQA2ggRe9pIZRdhkS06ALP80S2Hj16NLl+/XpJCeID8YsWwe9enyVg7QGwEtIMbFFvRUqQiFWiVIiwbsmAUD0KRyOmI1MDakAN9FUDs36LYEHShrlP4CBH7SR6ZQH8HgBru06uPnfuow636dLO8QKQPCCVviKIMPlrxFpDlW0YNKx9Nax+LrWpBtRAnXWpm5HyPAGE1GXldVtbW5P19fVy1A6RLFnhY1aYvR7HvECfvkBEr0gRckgmXW8BKIQHYAWuAlXpjltvjdWIacTUgBpQA2qgjxr48OFD8WX1Z8N/pRl2HiedSMaG3xFoYJeh/CBgHUoEUDpwRbfbhE+hekSWw5rrxqGzRxL0cVL5mTT2akANqAE1kEL2OlJV74QPWOHXAlt5zOYuAUvA+qQIEqHKLon8nAaihEPpb5W2C4GsCE8jpZFSA2pADaiBFjWAX2TDF2U01GTRG5Ld9albbh3Amk8RpqdVtp8iEorZEQpnCdKVHXKH9BNKTb8rdwpqVFs0qn5nda8G1EA0wIYvekHiSwGtOmiRQviA1mzLo7EDWPOAlUai6W+V08TX1tZK1Co9QWa3sQpXGhidjBpQA2qgZQ0k8EBLB5qS4j9pQMo9DUrTK6vuH9kSZDUPWDn2hkGHvIlckRbMTsGAVbaspudVfTp5yxPM766DUQNqQA20qQEAC5/IPZkdGm8nkoVP3Q6w+D2/G3v0iu/XPGBFAMkfI5BErrIbsC76Q0jpfmsbhjaNis7EcVcDakAN/LXAVfwkfpHARA6Lrn1rIlgtRa8ErF8JM8XtdKUFrlJvlRYMTKK0X0hhO5BV/96JprFVA2pADaiB1jSAv0ygIX0geY6WRmSDWohSfeo7Nh/BArBomkbNFQIh9ZeIVd1+gYlT/1xDV2uTyu+rI1EDakANqAF8ZcpoqMNKn8hAFoELfCwQQpYouwt53AJ8NQ9Y1GCtrq6WRmtEpoCsuidInQYMYNXnN2lkNDJqQA2oATXQoga2q03OdcBPEsUCsoAp4Iq0ITcB69cLMgbCZHAZ2PTpyK5BnuP70YahBipovN4x2OKk8TvrLNSAGlADamAeGqAmC7+bDWX0yQpwjYExmk4RBqi4Z2AhZ7aRAl737t0rcJUiveyGyM6IeYjL/6GRUgNqQA2ogRY1gC/l/EJ6SmZ3YaJXCXKMGbJGnyLMINKTg1vClDRGY8dDi6L3O2vs1YAaUANqYNEaIGiRQ6Nv3bo1TRG2spuwGcBK740c3Pzs2bPJ+/fvS91V6qyST7aJqIZn0YbH/6/G1IAaGLsG0vIIP4vP5VzfRKxSm2UEa+D1WIQk08KfAWagU9ReNwzNzsD6BPGxTwC/n0ZeDagBNaAGFqEBghfJFOFrOdd3aWmpRLJaaDY6+ggWEasUuZMifPToUYlYpSnadoCVSNYiBOf/1JCpATWgBtRACxpIehDI+vDhQ8kYUY9F1ArIGnP0qolGo2kkSoE7va5yBA6QBUil9ULELlxp+FowfH5Hda4G1MCiNUAAg/dIQ1IA69WrV+VwaCNYA08Npj0/pHz9+vXSkuHdu3fTLuxEsRYtMP+/RkwNqAE1oAZa1EACGnx3wIqfiWq9ePFicu3aNSNYfQ/hkf6jxqruEstnJnJFEd2JEycKXDGgtcCNVGnwWjR4fmd1rwbUwFFpoG59lA1lOWaOFkkEPyjdycHQ6fzO8+kA33cGGXUfrMDV7H2iV9zT6IzBDFSlmai7BTU0R2VofB+1pgbUQGsaqI+XqwErx9GtrKwUkCJQwj23+PIxpBBHUeSeAQlJJprFPb03Xr58WbqzZ5cgIUpSha2J3e+rgVcDakANqIGj0kAdxKjrnRPsILNEBCuRLHw4O/6HHLWqP/uxNPwa8n1NvPVRODze3NwsYAVUpdgO2OLn+pzBoxKc76NxUwNqQA2ogRY0sF1vyTqqhS++e/duSREmJViX/QyZS/jsgweshBRTdxXAYtfg8vLydKfgbHgScWeHQwtC9ztq0NWAGlADauAoNVADVqJZdV0WG80ofqc/JYGSpAkDWYMHrHyhId/nbCOgKnlbCtvX19enEJWWDIiLeix2MwBdRyk230vjpgbUgBpQA61oYDvA4rsHstKTkv6UYRD8eABryFzCZz829C+QsGIOc04kKwc5B6LqXLA7CDVwrRg4v6daVwNqoGsNzKYKA1gUu1OuQwslCt6zOS1+feh8MkrAgoAfPnxYBo6BTXqQwUxqkOfyc9fi8/01gGpADagBNTA2DWx3zm/d4DsNSPneNAIncsXtypUrH6UMhwpag6/BInL13XffldQgYMVuBLrE2oJBYzU2Y+X3UdNqQA2MTQNv3rwp/pp7dv0nPZiSnyHXYQ0esNJDI7sQEr2aPQJnbKL0+2ho1YAaUANqYMgaqA+DJppFFCs+naajQ4ar0ewirDu+kseltwY7BG3DoPEZsvHxs6tfNaAGxqyB+kxgynZyhA6ZKAHr17Rc14SZI3EYEHYT0rUdEk7d1ZjF6XfT+KoBNaAG1MBQNVCX8tCyAd99+/btUupjirAHgJVdg3R/ZUCePn1aIlfuFNToDNXo+LnVrhpQAy1ogHZJOWElWaekCbOjsOsgzmHef/A1WAxCjsYhPbi1tVUAKwPXgkj9jhpjNaAG1IAaGJoG8NNErbKbEN/N6Sv0scSvHwZu+vC3gwcsUoQJJ9JYNAKjmejQxObn1UCqATWgBtRAKxqYzTRlNyGlPkawepAipBCO9ODVq1dLgVzOHLRTu0aqFSPl91TrakANDFED2YiWhuCJZBHF6kME6rCfYfARLOqu6IPFgZFAFXlciuXqo3GGKDw/swZTDagBNaAGxqyB1Evjs3lM5okgCb782rVrpWVDfQRegCelQYcFoEX//eABKxeazu2hXyDLRqMapjEbJr+b+lYDamDoGgCq8Nv4bKJYAFZ2E9IwfCcAwu8PIYU4eMCi/mppaWny/Pnz6c5B+19peIZuePz8algNqIGxayBRK+6BrPronI2NjRLBqs8nBLjSOYDfLToCddj/P/izCE+dOjVNDyJGQovcC1kap7EbJ7+fGlcDamDIGghg8R1S2pPzg4lmcSZhAKtOFfKYW9/PKBw8YHFu0ePHj6eNRRmUNBo1TajxGbLx8bOrXzWgBsaugQRFUtoTwOJnziZMpIoNbUkNXr58eRiAFRIc6j0X+uXLl9OjcdJTI7sSxi5Ov58GWA2oATWgBoaogbRpyNnB+Rn/TUSL4AlRLFJ1ASwe89wQmOXYED7kpz7jjRs3SmFcgIoBgohNEWpwhmhw/MzqVg2ogVY0kGgV/jsd3fnu/MztzZs3peloACvNR2nLxHN955fBAxY7DYhaMShpzSBgaaBaMVB+T7WuBtTAUDWQHYSkA9PVvY5mAVk3b978CLBIGQJYpAt7D1h9LxLL55stcMtJ20MVlp9bo6gG1IAaUANqYGcNAFuUAOXEFnpjZSchacK+80vvi9wTBqwvJM+FYhWnBkoNqAE1oAbUwPg0AGC9fv26RKxoyRSoInpF/bWA9Ws47zAXIbnXQBUXlZ2DXGC6tzupxjepHFPHVA2oATWgBkgRkkYkTYjfr1nCNg2HhKs0GUs6kAsKyXL2IIBFIzInoZNQDagBNaAG1MD4NJBi97W1tZIapMj9MAGbo/7b3qcIAantAIvnCB06qcY3qRxTx1QNqAE1oAbS4f3Zs2clPTgbxTpqYNrv+/UesOrDHoGqpAjZumkjUSegRlgNqAE1oAbGqQEAiygWwZSVlZVpBGu72uz9ws9RvL73gFXnWQGsdHW9d++e0atfW1NoWLwGakANqAE1MGYNUId1//79qf8nVWiR+xxqsJIeBKx4zIUFuh4+fGgzUQFLwFQDakANqIGRaqBuPorPTzd3UoW2aZgDYAFTXEzAKs3FlpeX7dY+0gk15lWY380ogxpQA2pgfxogRUgTUvphEbVK0IX67KNI8x3mPQaRIswBj6FXtmxaf7U/kTqpvV5qQA2oATUwRA2QIqQOi0ajBF1yZM5h4Oco/rb3gDXbYBTYIhebs4uGKBY/s0ZODagBNaAG1MCnNZAUIYDFY47GS1d3+2DNIUWY2qvUX/HzkydPyknbRrE0UBooNaAG1IAaGKcG8PE//PBDqbEjqPLo0aMCWKdOnSqpwqOIQh3mPQYRweJCcqMWizoscrFv376dHvLs5Brn5HJcHVc1oAbUQNsaALDSD+vFixelDotm4wLWnCJYhALr+isK3ohgETZ08rU9+Rx/x18NqAE1ME4NAFb4+/TDArbY5DaUhqO9j2ABVzmPkFAd9Ve52KYIxzmpNJaOqxpQA2pADaTWOmcS4vPZ5AYT5Hziw6TwFv23gwAsCtsTxaIXxk8//VQgS8ByAmqE1YAaUANqYJwawNen/iqpQgrds5Nw0YB02P8/CMAiPUjOlcZiXOyEDJ1U45xUjqvjqgbUgBpQA8lWoQVKgt69ezd5/vx5iV65i3AONViJXH3//felB0YNWFx8J6GTUA2oATWgBtTA+DSAj4+fB7DY3EahO0EXAWsOgFUfkUPuNSTLvYA1vgmlkXRM1YAaUANqID4+fp6SIDa3bW1tlUL3QQAWH7LPtwAWIcHV1dUCVdk9KGA5CTXEakANqAE1MG4NpN6amqw3b95M7t6922tuCVMd6zNc1TsIebyxsTEtbueCC1jjnlQaTcdXDagBNaAG4u+zm5CGo31nFz7fsTTx7Ot9WjRQ4P7q1avpETlEsQQsJ57GVw2oATWgBsarAeCKG3CVtGHqsPrKLflcvQcsWjTwYengzu7BXGQBa7wTSmPp2KoBNaAG1AAaqAGLx6QJadnQd7ji8w0GsJaWlgq9pi+GgOXk0wCrATWgBtTAeDWQw54DVil0534QgNX3PCYXkRYN5FxJCUKuKXJnR4GTa7yTy7F1bNWAGlADbWuAovb6qJzUYdFwlHMJyW7BCensTjkRt/TK6pJxel/kno6tT548KelB0oQBrESznIBtT0DH3/FXA2pADYxTAwRV8P3c8pgI1oMHD0qhO5DFLYCVn+lA0CVclSL3rj/Abu+fi7S5uVnSg4ErdxGOczJpJB1XNaAG1IAaiAbw+fh+ACvRLCJaT58+nZ5TXDceTepQwNpDDy4uEiFAdhBykQNWnkPoBNQIqwE1oAbUwLg1kPqrRLBSi/369esJm+DSKzOQtVvQ5ih/P4gI1srKSgkN1gc/CljjnlQaTcdXDagBNaAGAlRErSgRys8wQA1LiVwdJUDt9l69Byy+AF1b68hVIllOPiefGlADakANqIHxaiAF7tzXDUcZc84nBqySDqT+KpGsPkS0eg9YXKS1tbWP+l/VqUIn1ngnlmPr2KoBNaAG2tVAfdgzOpj9+fbt26WEqN5BCGTtFlk6qt/3HrC4WOvr69P6K0KEAaw0HXUCtjsBHXvHXg2oATUwTg3UBz0zxvj8RLIofr9///6EHpmpxUqLhj5ErwZzVE4K3OsLa5pwnBNKQ+m4qgE1oAbUwF40ABucPXu2pAWBK1KFABf3/Nx1M9Led3LnAuUMQgHLSbeXSedr1IkaUANqYPwa2NramrZqELB+haX9EiU0ynbM+mBnQoOECt1JOP4JpJF0jNWAGlADamA7DcAGKWxP7RXMQHrOCNYegOvmzZulRUN9cQUsJ5sGVw2oATWgBtrWAGywvLw8hSzAiqL3dHjfb0Bn3q/vfYqQIjaAKidqE8kSsNqeVBpVx18NqAE1oAbY9MZOQqJV2RmYx4lszRua9vP/eg9Y7CCsT9QOYNUpQyeaE00NqAE1oAbUQFsagA1WV1enBe515ErA2kOKkPOG0sGVyJWA1dYE0mA63mpADagBNbCTBgjCpBeWgLUHqKpDcM+ePROwfu3/oYHxGqgBNaAG1IAa+D8NEHB58uRJASx6YQlYM4A123UVuOK5PK+YNChqQA2oATWgBtTArAYALNo4pdlojs2pj8vZT83UvF/beQ1WfXYQ9FkDFr9zUjmp1IAaUANqQA2oge0Ai15YnElIBCuAlQDNvIFpv/+vc8CaPQE7P3OBuGhOKieVGlADakANqAE1sB1g0Qvr+vXr04ajSRMmYLNfKJrn63sBWHUUK+TJPT2wnFROKjWgBtSAGlADamA7DdALC1bgeJykBtPVfZ6wdJD/1TlgpTHY7IUBsOiB5aRyUqkBNaAG1IAaUAPbaYDuAnfu3JmWF9XnEh4Eiub5N70ALArUAljZbglgsf3SSeWkUgNqQA2oATWgBmY1kOPy7t27N90cB0vAEYlozROY9vu/OgcsLgK1VkkT1oBFDywnlZNKDagBNaAG1IAa2A6w2EmYZqPpQEDQRsD6lTRnASvbLblQ9MByUjmp1IAaUANqQA2oge0A629/+9tkbW2tdHOvAev8+fMlM9blrfMIFhGr+gIQzeLCUJs1e8izE8wJpgbUgBpQA2pADUQDpAnphVWfRdiHY3Lgml4AVr2dkghWyJODHJ1ITiQ1oAbUgBpQA2pgOw0AWPTCCkcQyeoyalW/d+8AK8Vp3AtYTiiNqhpQA2pADaiBneCK5+mFlaiVgFXlRQNU9cWBRJeXlydsv3RiObHUgBpQA2pADaiBnXYRUk4UsOpLerA3KUJSgvVF4fHKyoqA5SHPArYaUANqQA2ogW01QHqQXYRkuwjW1OcY9yFN2HmKEOrkDKHQJ7sKeUzjsPS4cOXiykUNqAE1oAbUgBqoNQAjsIuQ+7rdUx+OyelFBGsWsIhmQaI0DnMyOZnUgBpQA2pADaiBnWqwfvrpp8IKlBUlE9aHHli9AayzZ89OI1gBLI/JcUJpVNWAGlADakAN7KQBIlcAFmlCAWubpl9EsICqHM5I9IrwHsfkcNGcXE4uNaAG1IAaUANqYFYDbIR7//795M2bN5OHDx+W9gxhCGuwfr0YASvuc/Az948ePRKwLGwUsNWAGlADakANbKsBAOvdu3elKTnMQIowzcv7sJuwF0Xu5Eu5GIBVQGtjY0PAclJpWNWAGlADakAN7AhYb9++LYD15MmTwhF9atfQC8ACrAJYuX/+/LmA5aTSsKoBNaAG1IAa2LFNAzVYtGnY3NyctmlIu4au04S9AiwuRs4TovW9NVjm3K27UANqQA2oATWwkwZo00Cq8MWLF9NdhHUkq0vI6gVgcQFCnKQLgSyPyXFCaVTVgBpQA2pADeymgZxHWJca9eHInN4BFjsKAazdLqi/d9KpATWgBtSAGmhXA2S50pCcnYQ1YKXYvfkIVqr9uQ9gmR5sd9JoMB17NaAG1IAa2E0DNWBR6A5D5DSYXgBWUnNd3tctGqDNGzdulNOxhSwn2G4TzN+rETWgBtRAmxogepUIFoCVHlhd8kz93sf68EECWMmZctAz4T4Bq81Jo7F03NWAGlADamA3DWwHWHVNd9d80zvA4oLcunWr9LXY7eL6eyegGlADakANqIE2NRDAIhjDxrilpaXSzZ1gTddwxfv3ArBmw3p37txxF6F9TwRsNaAG1IAaUAM7aqAGLFo1XLt2bdpsVMD6lfBmzw7ioty7d2+aV3Vl0ubKxHF33NWAGlADauBTGghg0QuLxznw2QjWryCV3YP14Yw8t7q6WuqvUrzmJHOSqQE1oAbUgBpQA7UGwgh0c+f569evl/SggFUBFnnTdHAHsB48eDAJkTqhnFBqQA2oATWgBtTArAZqwCIoI2D9L1jVZwXVgAV5rq2tTSBSI1hOKI2qGlADakANqIHtNDALWLR4IiNmBKuKYHEx6kL3hw8flrOFBCwnlYZVDagBNaAG1MB2GiDTxfOwAhGs9fX1kg1Ly6euC90730WYnhU1YHGRhCsnlEZVDagBNaAG1MBOGhCwZlKCOxFlAAvyfPTokVtz3ZqrBtSAGlADakAN7KgBAWuPgFUfl/P48WO7uDupNKxqQA2oATWgBgSsg+Y662I0cqcUvG9sbJRdhIaGDQ2rATWgBtSAGlAD1mDtMVpVw9gsYNGJ9enTp2UXoWcROqk0rGpADagBNaAGBKxDABbF7kSwAKzNzU0By7CwEUw1oAbUgBpQA6YID5oipLgduApg0er+2bNnk59//tkIlhNL46oG1IAaUANqYFsNWOS+S1Sr7oEFbHF78eKFE8oJpQbUgBpQA2pADeyoAfplkjr88ccfyz3lRXXQ5qCBn3n9Xed9sAQsc+vWV6gBNaAG1IAa2K8G0mBUwNohkiVgOan2O6l8vZpRA2pADaiB3gNWTp7u8j5NRk0ROmE0mmpADagBNaAG9qKBABapQroOsEEOjiDFR113l1zDex/r+gMYwXIi7WUi+Rp1ogbUgBpQA7MaoNC9Bix6aaaGqmu+OZaoUVf3ApYTRqOpBtSAGlADauAgGgCwEsmiAwGAlf6aXXFN3lfAcoeGu3TUgBpQA2pADQxSAwEs7gEsemkmciVg/ZqntAbLlctBVi7+jbpRA2pADbStgV4DVtc5SlOEbU8OjaPjrwbUgBpQAwfRAIXtuZEmfPnyZYlepQ6ra76xyN2w8CDDwgeZjP6NRlwNqAE1MB4NCFi/0uRuFGmKcDyC13g5lmpADagBNXAUGhCwBCwjREYJ1YAaUANqQA3MWQMCloDlpJrzpDqKlZHv4QpcDagBNdBvDQhYApaAJWCpATWgBtSAGpizBgQsActJNedJ5aqy36tKx8fxUQNq4Cg0IGAJWAKWgKUG1IAaUANqYM4aELAELCfVnCfVUayMfA9X4GpADaiBfmuA3lc//fRT6YXFfX1UDt0JdutgsOjf2wdL5y8AqgE1oAbUgBoYnAYALDq5A8IA1ubmZjkZhsOeBaz/jW7ZB6vfqwRXcY6PGlADakAN9E0DNWABWgLWNilDAcuJ27eJ6+dRk2pADaiBfmsggMU9acKnT5+WtKARrAq0BKx+i1gj4/ioATWgBtRA3zRQAxafbWNjQ8CaLSwTsJy4fZu4fh41qQbUgBrotwYCWH/5y18mPH7y5EmJXhnBMoI1uIJCjU2/jY3j4/ioATXQkgZmAevx48cClhEsjUBLRsDvqt7VgBpQA4vRAMXtP/7444QoFoB16dIlI1iBLEJ5PCZNyIXhnjCfYlyMGL2uXlc1oAbUgBoYgwYobAesgKyff/55cufOncnS0tI0irXoPle7/f/O+2BtB1iPHj0SsOzJogbUgBpQA2pADeyoAQAraUL6YN2+fbsAVnYS7gZAi/59LwArDcECWw8fPnRSOanUgBpQA2pADaiBPQEWUaybN28KWDU1pto/lf/8bnV1tVxQ6HQMYUy/g+F4NaAG1IAaUAPz1UAYIfc3btwogJVu7ouOUO32/zuPYPEBuSAXL16cnhtEHjWNwxTkfAXp9fR6qgE1oAbUwBg0UAMWtVjLy8sFrgSsqk3DtWvXJhcuXJheFMJ8dQv8MQjB76BBUwNqQA2oATUwPw0AWLn98MMP0+hVCt13izAt+ve9iGBBnefPn58C1vXr16c7AxTj/MTotfRaqgE1oAbUwFg0AFxRe8X969evp9Ergjap6V40RH3q//cKsEKdAFe2Xo5FCH4PjZoaUANqQA2ogflpoAasra2tacsnAet/U4Szbe35mUI1Dm1UiPMTotfSa6kG1IAaUANj0kBqsIhi0WSUIA39NCk5ArK6jF7x3p1HsLYL43Fh1tfXS+hvTGLwu2jc1IAaUANqQA3MTwOpwaK9U1o+AVnph9UlZPUCsNLFHdhKmvDu3bsTGocpxPkJ0WvptVQDakANqIGxaIBSIjiBe5ghDNElVNXv3Tlg8WGIWNGmgYtD/RXhPdKERrA0BGMxBH4PtawG1IAamK8GOIPw7du3pcAdZiByBU+4i7Bq01D3wcqOQp5j26WCnK8gvZ5eTzWgBtSAGhiDBgJYm5ub0/OMA1jAVteRrF5EsOqDntMTC8Dioo1BBH4HjZkaUANqQA2ogflqgNQghzxTsx2ggh2MYM3sIkzuNBcG0MqROYpyvqL0eno91YAaUANqYAwaoJSI+itKi3JMTrq5Nx/BAqxq8syFodnorVu3jGB50KcaUANqQA2oATXwTxrIDkJYIa0Z0pmgF7sIAzR9vc8ugTdv3pRure/fvy8XmaN0uI2BwP0OriTVgBpQA2pADexPAzABDUZTWsR9AjY87pprjnX9AXZ7fy5etmEiPnYMcM+FBb4U5P4E6fXyeqkBNaAG1MAYNEB68NmzZx8VuANYcMVsunA31ljE73sPWBsbGwWwAKr6AGgurIClkRiDkfA7qGM1oAbUwP41ABusra2VqFWK2/N4EcC03//Ze8C6f/9+6YfFToE6LcjPAtb+Bekk9pqpATWgBtTAGDQAG9y8ebP00aRuOx0JckzOfoFo3q/vPWCtrKwUsApgBaoELA3EGAyE30EdqwE1oAYOpgH6YKWPJo1G2TVIkbuA9WuOdC+0yMWjwH22q7uAdTBBOpG9bmpADagBNTAGDbx48WIKVUSw6h2Ee+GLRb+m9xEsiPTly5clikUdFqCVWix3EWokxmAk/A7qWA2oATWwfw08evSoANZ29VcWue8hisXFY5cAoUAAi6I2AWv/QnTyes3UgBpQA2pgTBqgGTlRKI7Y455arIBVHwDr/wODDFqOB2457gAAAABJRU5ErkJggg==";
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		JSONObject accountDetail = new JSONObject();
		ResultSet rset;
		String cifNumber = "";
		String sqlQuery = "SELECT H.ACCOUNT_NO, C.CIF_NO,CONCAT(C.TITLE_CODE, ' ',C.NAME) NAME,C.ADDRESS,C.GEO_UNIT_ID,C.MOBILE,D.PID_CODE,P.CONCISE_DESCRIPTION ,D.DOCUMENT_NUMBER FROM ACCOUNTHOLDERS H,CUSTOMERREG C LEFT OUTER JOIN CIFIDENTIFICATIONDOC D ON D.CIF_NO=C.CIF_NO LEFT OUTER JOIN PIDCODES P ON P.PID_CODE=D.PID_CODE WHERE H.ENTITY_CODE=? and H.ACCOUNT_NO=? and H.DTL_SL=1 AND P.DOC_USED_FOR_ID_PROOF='1' AND H.CIF_NO = C.CIF_NO";
		try {
			util.setSql(sqlQuery);
			util.setString(1, getContext().getEntityCode());
			util.setString(2, accountNumber);
			rset = util.executeQuery();
			if (rset.next()) {
				cifNumber = rset.getString(2);
				String name = rset.getString(3);
				String address = rset.getString(4);
				String pinCode = rset.getString(5);
				String mobileNumber = rset.getString(6);
				String pidCode = rset.getString(7);
				String description = rset.getString(8);
				String documentNumber = rset.getString(9);
				accountDetail.put("accountNumber", accountNumber);
				accountDetail.put("customerName", name);
				accountDetail.put("mobileNumber", mobileNumber);
				accountDetail.put("cifNumber", cifNumber);
				accountDetail.put("pinCode", pinCode);
				String[] lines = { "", "", "" };
				if (address != null && !address.equals("")) {
					lines = address.split("[\\r\\n]+");
				}
				accountDetail.put("address", lines);
				accountDetail.put("pidCode", pidCode);
				accountDetail.put("description", description);
				accountDetail.put("pidNumber", documentNumber);
			}
			DTObject result = fetchCustomerDocument(MiddlewareConstants.DOC_FOR_PHOTO, cifNumber, accountNumber);
			if (result.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
				accountDetail.put("personImageURL", result.get("DATA"));
			} else {
				accountDetail.put("personImageURL", DEFAULT_USER_IMAGE);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			dbContext.close();
			util.reset();
		}
		return accountDetail.toString();
	}

	public String getAccountBalance(String accountNumber) {
		JSONObject accountBalance = new JSONObject();
		CASAValidator validator = new CASAValidator();
		try {
			// TODO Fetching of balance using new procedures
			DTObject inputDTO = new DTObject();
			inputDTO.set("ACCOUNT_NO", accountNumber);
			DTObject detailsResultDTO = validator.getAccountDetails(inputDTO);
			if (detailsResultDTO.get(ContentManager.ERROR) == null) {
				accountBalance.put("name", detailsResultDTO.get("NAME"));
				accountBalance.put("branch", detailsResultDTO.get("BRANCH_CODE") + "- " + detailsResultDTO.get("BRANCH_DESC"));
				accountBalance.put("openedOn", detailsResultDTO.get("OPEN_DATE"));
				accountBalance.put("mode", detailsResultDTO.get("OPERATING_MODE") + "- " + detailsResultDTO.get("OPERATING_DESC"));
				accountBalance.put("accountHolders", detailsResultDTO.get("NOOF_ACNT_HOLDERS"));
				accountBalance.put("currency", detailsResultDTO.get("CCY") + "- " + detailsResultDTO.get("CCY_DESC"));
			}
			DTObject balanceResultDTO = validator.getAccountBalance(inputDTO);
			if (balanceResultDTO.get(ContentManager.ERROR) == null) {
				String currencyUnits = balanceResultDTO.get("CCY_UNITS");
				String currencyCode = balanceResultDTO.get("CURR_CODE");
				accountBalance.put("accountBalance", fmc(parseToBigDecimal(balanceResultDTO.get("AC_AUTH_BAL")), currencyCode, currencyUnits));
				accountBalance.put("debitsInPipeline", fmc(parseToBigDecimal(balanceResultDTO.get("AC_UNAUTH_DBS")), currencyCode, currencyUnits));
				accountBalance.put("creditsInPipeline", fmc(parseToBigDecimal(balanceResultDTO.get("AC_UNAUTH_CRS")), currencyCode, currencyUnits));
				accountBalance.put("clearingDebits", fmc(parseToBigDecimal(balanceResultDTO.get("AC_CLG_DBS")), currencyCode, currencyUnits));
				accountBalance.put("clearingCredits", fmc(parseToBigDecimal(balanceResultDTO.get("AC_CLG_CRS")), currencyCode, currencyUnits));
				accountBalance.put("onHold", fmc(parseToBigDecimal(balanceResultDTO.get("AC_LIEN_AMT")), currencyCode, currencyUnits));
				accountBalance.put("walletBalance", fmc(parseToBigDecimal(balanceResultDTO.get("WAL_AC_BAL")), currencyCode, currencyUnits));
				accountBalance.put("availableBalance", fmc(parseToBigDecimal(balanceResultDTO.get("AC_AVL_BAL")), currencyCode, currencyUnits));
				// accountBalance.put("accountNumber", "100000100001");
				// accountBalance.put("name", "NAVEEN R SHENOY");
				// accountBalance.put("branch", "0001 - MANDAVELI");
				// accountBalance.put("openedOn", "20-08-2016");
				// accountBalance.put("mode", "SINGLE THROUGH AGENT");
				// accountBalance.put("accountHolders", "01 (SINGLE)");
				// accountBalance.put("currency", "INR - INDIAN RUPEE");
				// accountBalance.put("accountBalance", "13,345.00");
				// accountBalance.put("debitsInPipeline", "0.00");
				// accountBalance.put("creditsInPipeline", "5,000.00");
				// accountBalance.put("clearingDebits", "2,500.00");
				// accountBalance.put("clearingCredits", "0.00");
				// accountBalance.put("onHold", "0.00");
				// accountBalance.put("walletBalance", "0.00");
				// accountBalance.put("availableBalance", "15,845.00");
			}
			if (balanceResultDTO.get(ContentManager.ERROR) == null && detailsResultDTO.get(ContentManager.ERROR) == null) {
				accountBalance.put("accountNumber", accountNumber);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			validator.close();
		}
		return accountBalance.toString();
	}

	public String getAccountWallets(String accountNumber) {
		JSONObject responseData = new JSONObject();
		JSONArray jsonRows = new JSONArray();
		JSONArray rowPage = new JSONArray();
		DBContext dbContext = new DBContext();
		try {
			DBUtil util = dbContext.createUtilInstance();
			DBUtil innerUtil = dbContext.createUtilInstance();
			String clusterCode = getContext().getClusterCode();
			String sqlQuery = "SELECT W.WALLET_NO, W.AC_BAL, DECODE_CMLOVREC('COMMON','WALLET_STATUS',W.STATUS), COALESCE(CONCAT(G.GOVT_SCHEME_CODE,' - ', S.CONCISE_DESCRIPTION),''), G.TRAN_CURR FROM WALLETBAL" + clusterCode
					+ " W LEFT OUTER JOIN WALLETGENDTL G ON W.WALLET_NO = G.WALLET_NO LEFT OUTER JOIN SCHEMES S ON G.GOVT_SCHEME_CODE = S.SCHEME_CODE WHERE W.ENTITY_CODE = ? AND W.ACCOUNT_NO = ? AND W.AC_BAL > 0";
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, getContext().getEntityCode());
			util.setString(2, accountNumber);
			ResultSet resultSet = util.executeQuery();
			JSONArray jsonRow;
			int rowNumber = 1;
			String currencyCode = "";
			String currencyUnits = "";
			while (resultSet.next()) {
				jsonRow = new JSONArray();
				if (rowNumber == 1 || currencyCode.equals("") || currencyCode == null) {
					currencyCode = resultSet.getString(5);
					if (currencyCode != null && !currencyCode.equals("")) {
						innerUtil.setSql("SELECT CONCISE_DESCRIPTION,SUB_CCY_UNITS_IN_CCY FROM CURRENCY WHERE CCY_CODE=?");
						innerUtil.setString(1, currencyCode);
						ResultSet rs = innerUtil.executeQuery();
						if (rs.next()) {
							responseData.put("currencyCode", currencyCode + "- " + rs.getString(1));
							currencyUnits = rs.getString(2);
						}
					}
				}
				jsonRow.put(resultSet.getString(1));
				if (!currencyUnits.equals("") && currencyUnits != null) {
					jsonRow.put(fmc(parseToBigDecimal(resultSet.getString(2)), currencyCode, currencyUnits));
				} else {
					jsonRow.put(fmc(parseToBigDecimal(resultSet.getString(2)), "INR", "2"));
				}
				jsonRow.put(resultSet.getString(3));
				jsonRow.put(resultSet.getString(4));
				rowPage.put(jsonRow);
				rowNumber++;
				if (rowNumber % 11 == 0) {
					jsonRows.put(rowPage);
					rowPage = new JSONArray();
				}
			}
			if (rowNumber % 11 != 0) {
				jsonRows.put(rowPage);
			}
			if (jsonRows.length() > 0) {
				responseData.put("accountNumber", accountNumber);
			}
			responseData.put("walletsGridData", jsonRows);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			dbContext.close();
		}
		return responseData.toString();
	}

	public DTObject fetchAdditionalDetails(DTObject inputDTO) {
		inputDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = getDbContext().createUtilInstance();
		try {
			String sqlQuery = "SELECT CONCISE_DESCRIPTION FROM ENTITYBRN WHERE ENTITY_CODE=? AND BRANCH_CODE=?";
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, getContext().getEntityCode());
			util.setString(2, inputDTO.get("BRANCH_CODE"));
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				inputDTO.set("BRANCH_DESC", rset.getString(1));
				inputDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			util.reset();
		}
		return inputDTO;
	}

	public boolean validateAadharNumber(String schemeCode, String accountNumber, String aadharNumber) {
		boolean isValid = false;
		DBUtil util = getDbContext().createUtilInstance();
		try {
			String sqlQuery = "SELECT AA.PID_NO FROM ACCOUNTACCESS AA LEFT OUTER JOIN SCHPAYPARAM SP ON SP.ENTITY_CODE=AA.ENTITY_CODE AND SP.SCHEME_CODE=? WHERE AA.ENTITY_CODE=? AND AA.ACCOUNT_NO=? AND AA.PID_TYPE=SP.DEFAULT_PID_TYPE";
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, schemeCode);
			util.setString(2, getContext().getEntityCode());
			util.setString(3, accountNumber);
			ResultSet rset = util.executeQuery();
			while (rset.next()) {
				String pidNo = rset.getString(1);
				if (pidNo != null && pidNo.replace("-", "").equals(aadharNumber)) {
					isValid = true;
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			util.reset();
		}
		return isValid;
	}

	public static String fmc(BigDecimal amount, String currency, String currUnit) {
		try {
			Integer currUnits = Integer.parseInt(currUnit);
			if (amount == null)
				return null;
			String famount = String.valueOf(amount);
			String deciPart = "";
			deciPart = StringUtils.rightPad(deciPart, currUnits, "0");
			deciPart = "0." + deciPart;
			double value = Double.parseDouble(famount);
			boolean isNegative = false;
			if (value < 0) {
				value = value * -1;
				isNegative = true;
			}
			java.text.DecimalFormat formatter = new java.text.DecimalFormat(deciPart);
			String formattedValue = formatter.format(value);
			String integral = formattedValue.replaceAll("\\D\\d++", "");
			String fraction = formattedValue.replaceAll("\\d++\\D", "");
			if (integral.length() <= 3)
				return formattedValue;
			char lastDigitOfIntegral = integral.charAt(integral.length() - 1);
			integral = integral.replaceAll("\\d$", "");
			integral = integral.replaceAll("(?<=.)(?=(?:\\d{2})+$)", ",") + lastDigitOfIntegral + "." + fraction;
			if (isNegative) {
				integral = "- " + integral;
			}
			return integral;

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Cannot format currency " + amount);
			return amount.toString();
		}
	}

	private BigDecimal parseToBigDecimal(String value) {
		value = (value == null || value.equals(RegularConstants.EMPTY_STRING)) ? RegularConstants.ZERO : value;
		return new BigDecimal(value);
	}
}