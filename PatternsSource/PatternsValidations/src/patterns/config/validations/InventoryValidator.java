package patterns.config.validations;
import java.sql.ResultSet;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.ContentManager;

public class InventoryValidator extends InterfaceValidator {
	
	public final DTObject validateItemCategoryCode (DTObject input) {
		DTObject result = new DTObject();
		String itemCatCode = input.get("ITEM_CAT_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				
				if (!isValidCodePattern(itemCatCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(itemCatCode,LENGTH_1,LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, itemCatCode);
			input.set(ContentManager.TABLE, "ITEMCATEGORY");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			input.set(ContentManager.COLUMN, "ITEM_CAT_CODE");
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}
	
	
	
	public final DTObject validateUOMCode  (DTObject input) {
		DTObject result = new DTObject();
		String uomCode = input.get("UOM_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				
				if (!isValidCodePattern(uomCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(uomCode,LENGTH_1,LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, uomCode);
			input.set(ContentManager.TABLE, "UNITOFMEASURE");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			input.set(ContentManager.COLUMN, "UOM_CODE");
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}
	
	public final DTObject validateItemIssueProfileCode(DTObject input) {
		DTObject result = new DTObject();
		String uomCode = input.get("ITEM_ISS_PROFILE_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				
				if (!isValidCodePattern(uomCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(uomCode,LENGTH_1,LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, uomCode);
			input.set(ContentManager.TABLE, "ITEMISSCTLPROFILE");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			input.set(ContentManager.COLUMN, "ITEM_ISS_PROFILE_CODE");
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}
	
	public final DTObject validateItemInvProfileCode (DTObject input) {
		DTObject result = new DTObject();
		String uomCode = input.get("ITEM_INV_PROFILE_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				
				if (!isValidCodePattern(uomCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(uomCode,LENGTH_1,LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, uomCode);
			input.set(ContentManager.TABLE, "ITEMINVCTLPROFILE");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			input.set(ContentManager.COLUMN, "ITEM_INV_PROFILE_CODE");
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}
	
	public final DTObject validateItemCode(DTObject input) {
		DTObject result = new DTObject();
		String itemCode = input.get("ITEM_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				
				if (!isValidCodePattern(itemCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(itemCode,LENGTH_1,LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, itemCode);
			input.set(ContentManager.TABLE, "ITEMCODES");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			input.set(ContentManager.COLUMN, "ITEM_CODE");
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}
	
	public final DTObject validateInvTranCode(DTObject input) {
		DTObject result = new DTObject();
		String invTranCode = input.get("INV_TRAN_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				
				if (!isValidCodePattern(invTranCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(invTranCode,LENGTH_1,LENGTH_2)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, invTranCode);
			input.set(ContentManager.TABLE, "INVTRANCODE");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			input.set(ContentManager.COLUMN, "INV_TRAN_CODE");
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}


	public final DTObject getBaseDate(DTObject input) {
		DTObject result = new DTObject();
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		String entityCode = input.get("ENTITY_CODE");
		DBUtil util = getDbContext().createUtilInstance();
		try {
			String sqlQuery = "SELECT TO_CHAR(BASE_DATE,?) BASE_DATE FROM INVCONTROL WHERE ENTITY_CODE=? ";
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, getContext().getDateFormat());
			util.setString(2, entityCode);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				result.set("BASE_DATE", rset.getString("BASE_DATE"));
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		} finally {
			util.reset();
		}
		return result;
	}
	public final DTObject validateSuppOrgReg(DTObject input) {
		DTObject result = new DTObject();
		String orgCode = input.get("ORG_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(orgCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(orgCode, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, orgCode);
			input.set(ContentManager.TABLE, "SUPPORGREG");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			input.set(ContentManager.COLUMN, "ORG_CODE");
			result = validateEntityWiseCodesWithoutEnabled(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}
	
	
	public final DTObject validateIndSuppReg(DTObject input) {
		DTObject result = new DTObject();
		String personID = input.get("PERSON_ID");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(personID)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(personID, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, personID);
			input.set(ContentManager.TABLE, "SUPPINDREG");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			input.set(ContentManager.COLUMN, "PERSON_ID");
			result = validateEntityWiseCodesWithoutEnabled(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}
	
	
	
	public final DTObject validateGRNumber (DTObject input) {	
		DTObject result = new DTObject();
		String grInvTranCode = input.get("GR_INV_TRAN_CODE");
		String grYear = input.get("GR_YEAR");
		String grRunningSerial = input.get("GR_RUNNING_SL");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidYear(grYear)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_YEAR);
					return result;
				}
				if (!isValidLength(grRunningSerial,LENGTH_1,LENGTH_8)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_LENGTH);
					return result;
				}
				if (!isValidNumber(grRunningSerial)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_NUMBER);
					return result;
				}
			}
			input.set(ContentManager.TABLE, "INVTRANCODE");
			String columns[]=new String[]{getContext().getEntityCode(),grInvTranCode};
			input.setObject(ContentManager.PROGRAM_KEY,columns);
			result = validatePK(input);
			
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}
	
	
	public final DTObject validateISITCode(DTObject input) {
		DTObject result = new DTObject();
		String ISITCode = input.get("ISIT_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(ISITCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(ISITCode,LENGTH_1,LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, ISITCode);
			input.set(ContentManager.TABLE, "ISIT");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			input.set(ContentManager.COLUMN, "ISIT_CODE");
			result = validateEntityWiseCodesWithoutEnabled(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}
	
	
	public final DTObject getInvControlDetails (DTObject input) {
		DTObject result = new DTObject();
		String fetch = input.get(ContentManager.FETCH);
		String fetchColumns = input.get(ContentManager.FETCH_COLUMNS);
		StringBuilder sqlQuery = new StringBuilder(RegularConstants.EMPTY_STRING);
		String entityCode = input.get("ENTITY_CODE");
		DBUtil util = getDbContext().createUtilInstance();
		try {
				if (fetch != null && fetch.equals(ContentManager.FETCH_ALL)) {
					sqlQuery.append("SELECT * FROM INVCONTROL WHERE ENTITY_CODE = ? ");
				} else {
					sqlQuery.append("SELECT ").append(fetchColumns).append(" FROM INVCONTROL WHERE ENTITY_CODE = ?  ");
				}
				util.reset();
				util.setSql(sqlQuery.toString());
				util.setString(1, entityCode);
				ResultSet rset = util.executeQuery();
				if (rset.next()) {
					result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
					decodeRow(rset, result);
				}else {
					result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
				}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		}finally{
			util.reset();
		}
		return result;
	}
	
	public final DTObject getItemBalDetails(DTObject input) {
		DTObject result = new DTObject();
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		String financialyear = input.get("FIN_YEAR");
		String itemCode = input.get("ITEM_CODE");
		String fetch = input.get(ContentManager.FETCH);
		String fetchColumns = input.get(ContentManager.FETCH_COLUMNS);
		String sqlQuery = null;
		if (fetch != null && fetch.equals(ContentManager.FETCH_ALL))
			sqlQuery = "SELECT * FROM ITEMBAL WHERE ENTITY_CODE=? AND ITEM_CODE=? AND FIN_YEAR = ?";
		else
			sqlQuery = "SELECT " + fetchColumns + " FROM ITEMBAL WHERE ENTITY_CODE=? AND ITEM_CODE=? AND FIN_YEAR = ?";
		DBUtil util = getDbContext().createUtilInstance();
		try {
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, getContext().getEntityCode());
			util.setString(2, itemCode);
			util.setString(3, financialyear);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				decodeRow(rset, result);
			}
			util.reset();
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		finally {
			util.reset();
		}
		return result;
	}

	public final DTObject getLotStockDetails(DTObject input) {
		DTObject result = new DTObject();
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		String itemCode = input.get("ITEM_CODE");
		String lotSerial = input.get("LOT_SL");
		String fetch = input.get(ContentManager.FETCH);
		String fetchColumns = input.get(ContentManager.FETCH_COLUMNS);
		String sqlQuery = null;
		if (fetch != null && fetch.equals(ContentManager.FETCH_ALL)) {
			sqlQuery = "SELECT * FROM ITEMLOTS WHERE ENTITY_CODE=? AND ITEM_CODE=? AND LOT_SL = ?  ";

		} else {
			sqlQuery = "SELECT " + fetchColumns + " FROM ITEMLOTS WHERE ENTITY_CODE=? AND ITEM_CODE=? AND LOT_SL = ?  ";
		}

		DBUtil util = getDbContext().createUtilInstance();
		try {
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, getContext().getEntityCode());
			util.setString(2, itemCode);
			util.setString(3, lotSerial);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				decodeRow(rset, result);
			}
			util.reset();
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		finally {
			util.reset();
		}
		return result;
	}

	public final DTObject validateDeptAccountingParam(DTObject input) {
		DTObject result = new DTObject();
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		String deptCode = input.get("DEPT_CODE");
		String sqlQuery = "SELECT 1 FROM INVDEPTACP WHERE ENTITY_CODE=? AND DEPT_CODE=? AND INV_MAINT_ALLOWED = 1";
		DBUtil util = getDbContext().createUtilInstance();
		try {
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, getContext().getEntityCode());
			util.setString(2, deptCode);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				// decodeRow(rset, result);
			}
			util.reset();
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateWardAccountingParam(DTObject input) {
		DTObject result = new DTObject();
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		String wardCode = input.get("WARD_CODE");
		String sqlQuery = "SELECT 1 FROM INVWARDACP WHERE ENTITY_CODE=? AND WARD_CODE=? AND INV_MAINT_ALLOWED = 1";
		DBUtil util = getDbContext().createUtilInstance();
		try {
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, getContext().getEntityCode());
			util.setString(2, wardCode);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				// decodeRow(rset, result);
			}
			util.reset();
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject getDeptLotDetails(DTObject input) {
		DTObject result = new DTObject();
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		String deptCode = input.get("DEPT_CODE");
		String itemCode = input.get("ITEM_CODE");
		String lotSerial = input.get("DEPT_LOT_SL");
		String fetch = input.get(ContentManager.FETCH);
		String fetchColumns = input.get(ContentManager.FETCH_COLUMNS);
		String sqlQuery = null;
		if (fetch != null && fetch.equals(ContentManager.FETCH_ALL)) {
			sqlQuery = "SELECT * FROM ITEMDEPTLOTS WHERE ENTITY_CODE=? AND DEPT_CODE = ? AND ITEM_CODE=? AND LOT_SL = ?";

		} else {
			sqlQuery = "SELECT " + fetchColumns + " FROM ITEMDEPTLOTS WHERE ENTITY_CODE=? AND DEPT_CODE = ? AND ITEM_CODE=? AND LOT_SL = ?";
		}

		DBUtil util = getDbContext().createUtilInstance();
		try {
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, getContext().getEntityCode());
			util.setString(2, deptCode);
			util.setString(3, itemCode);
			util.setString(4, lotSerial);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				decodeRow(rset, result);
			}
			util.reset();
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		finally {
			util.reset();
		}
		return result;
	}
	
	public final DTObject getWardLotDetails(DTObject input) {
		DTObject result = new DTObject();
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		String wardCode = input.get("WARD_CODE");
		String itemCode = input.get("ITEM_CODE");
		String lotSerial = input.get("WARD_LOT_SL");
		String fetch = input.get(ContentManager.FETCH);
		String fetchColumns = input.get(ContentManager.FETCH_COLUMNS);
		String sqlQuery = null;
		if (fetch != null && fetch.equals(ContentManager.FETCH_ALL)) {
			sqlQuery = "SELECT * FROM ITEMWARDLOTS WHERE ENTITY_CODE=? AND WARD_CODE = ? AND ITEM_CODE=? AND LOT_SL = ?";

		} else {
			sqlQuery = "SELECT " + fetchColumns + " FROM ITEMWARDLOTS WHERE ENTITY_CODE=? AND WARD_CODE = ? AND ITEM_CODE=? AND LOT_SL = ?";
		}

		DBUtil util = getDbContext().createUtilInstance();
		try {
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, getContext().getEntityCode());
			util.setString(2, wardCode);
			util.setString(3, itemCode);
			util.setString(4, lotSerial);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				decodeRow(rset, result);
			}
			util.reset();
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		finally {
			util.reset();
		}
		return result;
	}
	
	public final DTObject validateInvGRNumber(DTObject input) { 
		DTObject result = new DTObject();
		String grInvTranCode = input.get("GR_INV_TRAN_CODE");
		String grYear = input.get("GR_INV_TRAN_YEAR");
		String grSerial = input.get("GR_INV_TRAN_SERIAL_NUM");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidYear(grYear)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_YEAR);
					return result;
				}
				if (!isValidLength(grSerial, LENGTH_1, LENGTH_8)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_LENGTH);
					return result;
				}
				if (!isValidNumber(grSerial)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_NUMBER);
					return result;
				}
			}
			input.set(ContentManager.TABLE, "INVGR");
			String columns[] = new String[] { getContext().getEntityCode(), grInvTranCode, grYear, grSerial };
			input.setObject(ContentManager.PROGRAM_KEY, columns);
			result = validatePK(input);

		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}
	
	public final DTObject getItemDeptBalDetails(DTObject input) {
		DTObject result = new DTObject();
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		String deptCode = input.get("DEPT_CODE");
		String itemCode = input.get("ITEM_CODE");
		String financialyear = input.get("FIN_YEAR");
		String fetch = input.get(ContentManager.FETCH);
		String fetchColumns = input.get(ContentManager.FETCH_COLUMNS);
		String sqlQuery = null;
		if (fetch != null && fetch.equals(ContentManager.FETCH_ALL))
			sqlQuery = "SELECT * FROM ITEMDEPTBAL WHERE ENTITY_CODE=? AND DEPT_CODE = ? AND ITEM_CODE=? AND FIN_YEAR = ?";
		else
			sqlQuery = "SELECT " + fetchColumns + " FROM ITEMDEPTBAL WHERE ENTITY_CODE=? AND DEPT_CODE = ? AND ITEM_CODE=? AND FIN_YEAR = ?";
		DBUtil util = getDbContext().createUtilInstance();
		try {
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, getContext().getEntityCode());
			util.setString(2, deptCode);
			util.setString(3, itemCode);
			util.setString(4, financialyear);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				decodeRow(rset, result);
			}
			util.reset();
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		finally {
			util.reset();
		}
		return result;
	}
	
	public final DTObject getItemWardBalDetails(DTObject input) {
		DTObject result = new DTObject();
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		String wardCode = input.get("WARD_CODE");
		String itemCode = input.get("ITEM_CODE");
		String financialyear = input.get("FIN_YEAR");
		String fetch = input.get(ContentManager.FETCH);
		String fetchColumns = input.get(ContentManager.FETCH_COLUMNS);
		String sqlQuery = null;
		if (fetch != null && fetch.equals(ContentManager.FETCH_ALL))
			sqlQuery = "SELECT * FROM ITEMWARDBAL WHERE ENTITY_CODE=? AND WARD_CODE = ? AND ITEM_CODE=? AND FIN_YEAR = ?";
		else
			sqlQuery = "SELECT " + fetchColumns + " FROM ITEMWARDBAL WHERE ENTITY_CODE=? AND WARD_CODE = ? AND ITEM_CODE=? AND FIN_YEAR = ?";
		DBUtil util = getDbContext().createUtilInstance();
		try {
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, getContext().getEntityCode());
			util.setString(2, wardCode);
			util.setString(3, itemCode);
			util.setString(4, financialyear);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				decodeRow(rset, result);
			}
			util.reset();
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		finally {
			util.reset();
		}
		return result;
	}
	public final DTObject ValidateInvFieldId(DTObject input) {
		DTObject result = new DTObject();
		String fieldId = input.get("FIELD_ID");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(fieldId)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(fieldId, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, fieldId);
			input.set(ContentManager.TABLE, "INVOICEFLD");
			input.set(ContentManager.COLUMN, "FIELD_ID");
			input.set(ContentManager.ENTITY_CODE, "ENTITY_CODE");
			result = validateEntityWiseCodesWithoutEnabled(input);
			} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}
	
	public final DTObject ValidateInvTemplateCode(DTObject input) {
		DTObject result = new DTObject();
		String invTempCode = input.get("INVTEMPLATE_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(invTempCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(invTempCode, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, invTempCode);
			input.set(ContentManager.TABLE, "INVTEMPLATECODE");
			input.set(ContentManager.COLUMN, "INVTEMPLATE_CODE");
			input.set(ContentManager.ENTITY_CODE, "ENTITY_CODE");
			result = validateEntityWiseCodes(input);
			} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}
}