package patterns.config.validations;

import java.sql.ResultSet;
import java.util.ArrayList;

import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.framework.bo.BackOfficeErrorCodes;

public class AuthorizationValidator extends InterfaceValidator {
	public AuthorizationValidator() {
		super();
	}

	public AuthorizationValidator(DBContext dbContext) {
		super(dbContext);
	}

	public final DTObject validateAuthorizationUserID(DTObject inputDTO) {
		DTObject result = new DTObject();
		DBUtil util = getDbContext().createUtilInstance();
		String userID = inputDTO.get(ContentManager.USER_ID);
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		try {
			String query = null;
			query = "{CALL SP_ACMNAUTHUSERS(?,?,?)}";
			util.reset();
			util.setMode(DBUtil.CALLABLE);
			util.setSql(query);
			util.setString(1, getContext().getPartitionNo());
			util.setString(2, getContext().getEntityCode());
			util.setString(3, getContext().getUserID());
		    util.execute();
		    query = "SELECT * FROM TMPAUTHUSERS WHERE TMPACMNAUTH_ID =?";
			util.reset();
			util.setMode(DBUtil.PREPARED);
			util.setSql(query);
			util.setString(1, userID);
			ResultSet rset=util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
			} else {
				result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
			}

		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return result;
	}

	public final DTObject validateBranchCode(DTObject inputDTO) {
		DTObject result = new DTObject();
		DBUtil util = getDbContext().createUtilInstance();
		String branchCode = inputDTO.get("MBRN_CODE");
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		try {

			String query = null;
			query = "{CALL (SP_ACMNAUTHBRANCHES(?,?,?))}";
			util.reset();
			util.setMode(DBUtil.CALLABLE);
			util.setSql(query);
			util.setString(1, getContext().getEntityCode());
			util.setString(2, getContext().getUserID());
			util.setString(3, getContext().getRoleCode());
		    util.execute();
		    query = "SELECT * FROM TMPAUTHBRN WHERE TMPACMNAUTH_ID =?";
			util.reset();
			util.setMode(DBUtil.PREPARED);
			util.setSql(query);
			util.setString(1, branchCode);
			ResultSet rset=util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
			} else {
				result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return result;
	}

	public final DTObject validateOptionID(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		AccessValidator validation = new AccessValidator();
		String optionID = inputDTO.get("MPGM_ID");
		try {
			inputDTO = validation.validateOption(inputDTO);
			if (inputDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, inputDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (!inputDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_OPTION);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		DBUtil util = getDbContext().createUtilInstance();
		try {
			String query = "SELECT 1 FROM MPGM M WHERE M.MPGM_ID = ? AND M.MPGM_QUERY_PATH IN(SELECT RD.PGM_ID FROM ROLEPGMALLOCDTL RD WHERE RD.ENTITY_CODE = ? AND RD.ROLE_CODE = ? AND RD.EFFT_DATE = (SELECT MAX(R.EFFT_DATE) FROM ROLEPGMALLOC R WHERE R.ENTITY_CODE = ? AND R.ROLE_CODE = ? AND R.EFFT_DATE <= FN_GETCD(?)))";
			util.reset();
			util.setSql(query);
			util.setString(1, optionID);
			util.setString(2, getContext().getEntityCode());
			util.setString(3, getContext().getRoleCode());
			util.setString(4, getContext().getEntityCode());
			util.setString(5, getContext().getRoleCode());
			util.setString(6, getContext().getEntityCode());
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
			} else {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.OPTION_NOT_ALLOTED);
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return resultDTO;
	}

	public final ArrayList<String> getAccessBranchList() {
		ArrayList<String> branchArray = new ArrayList<String>();
		DBUtil util = getDbContext().createUtilInstance();
		int brn_auth_restrict = 0;
		int brnlist_visibility = 0;
		try {
			String query = "SELECT BRN_AUTH_RESTRICT ,BRNLIST_VISIBILITY  FROM SYSCPM  WHERE ENTITY_CODE=? AND EFFT_DATE=(SELECT MAX(EFFT_DATE) FROM SYSCPM WHERE ENTITY_CODE=? AND EFFT_DATE <= FN_GETCD(?) AND AU_ON IS NOT NULL)";
			util.reset();
			util.setSql(query);
			util.setString(1, getContext().getEntityCode());
			util.setString(2, getContext().getEntityCode());
			util.setString(3, getContext().getEntityCode());
			ResultSet rs = util.executeQuery();
			if (rs.next()) {
				brn_auth_restrict = Integer.parseInt(rs.getString(1));
				brnlist_visibility = Integer.parseInt(rs.getString(2));
			}
			rs.close();
			util.reset();
			if (brn_auth_restrict == 0 && brnlist_visibility == 0) {
				query = "SELECT MBRN_CODE FROM MBRN WHERE MBRN_ENTITY_NUM=?";
				util.setSql(query);
				util.setString(1, getContext().getEntityCode());
			} else if (brn_auth_restrict == 0 && brnlist_visibility == 1) {
				query = "SELECT MBRN_CODE FROM MBRN WHERE MBRN_ENTITY_NUM=? AND (MBRN_CODE=? OR  MBRN_CODE IN (SELECT B.BRANCH_CODE FROM BRANCHLISTDTL B,ROLEBRNLISTALLOC R WHERE B.ENTITY_CODE=? AND B.ENTITY_CODE=R.ENTITY_CODE AND B.CODE =R.BRANCH_LIST_CODE AND R.ROLE_CODE=?))";
				util.setSql(query);
				util.setString(1, getContext().getEntityCode());
				util.setString(2, getContext().getBranchCode());
				util.setString(3, getContext().getEntityCode());
				util.setString(4, getContext().getRoleCode());
			}
			if (brn_auth_restrict == 1 && brnlist_visibility == 0) {
				query = "SELECT MBRN_CODE FROM MBRN WHERE MBRN_ENTITY_NUM=? AND MBRN_CODE=?";
				util.setSql(query);
				util.setString(1, getContext().getEntityCode());
				util.setString(2, getContext().getBranchCode());
			}
			if (brn_auth_restrict == 1 && brnlist_visibility == 1) {
				query = "SELECT MBRN_CODE FROM MBRN WHERE MBRN_ENTITY_NUM=? AND (MBRN_CODE=? OR MBRN_CODE IN (SELECT B.BRANCH_CODE FROM BRANCHLISTDTL B,ROLEBRNLISTALLOC R WHERE B.ENTITY_CODE=? AND B.ENTITY_CODE=R.ENTITY_CODE AND B.CODE =R.BRANCH_LIST_CODE AND R.ROLE_CODE=?))";
				util.setSql(query);
				util.setString(1, getContext().getEntityCode());
				util.setString(2, getContext().getBranchCode());
				util.setString(3, getContext().getEntityCode());
				util.setString(4, getContext().getRoleCode());
			}
			rs = util.executeQuery();
			while (rs.next()) {
				branchArray.add(rs.getString(1));
			}
		} catch (Exception e) {
			e.printStackTrace();
			branchArray = null;
		} finally {
			util.reset();
		}
		return branchArray;
	}
}
