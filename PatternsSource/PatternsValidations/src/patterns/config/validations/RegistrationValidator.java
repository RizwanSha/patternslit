package patterns.config.validations;

import java.sql.ResultSet;
import java.sql.Types;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.ContentManager;

public class RegistrationValidator extends InterfaceValidator {

	public final DTObject validateNominee(DTObject input) {
		DTObject result = new DTObject();
		String nomineeYear = input.get("NOMINEE_YEAR");
		String nomineeSl = input.get("NOMINEE_SL");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidYear(nomineeYear)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_YEAR);
					return result;
				}
				if (!isValidLength(nomineeYear, LENGTH_1, LENGTH_8)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.TABLE, "ACCOUNTNOMINEE");
			String columns[] = new String[] { getContext().getEntityCode(), getContext().getBranchCode(), nomineeYear, nomineeSl };
			input.setObject(ContentManager.PROGRAM_KEY, columns);
			result = validatePK(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateRevokeDateSl(DTObject input) {
		DTObject result = new DTObject();
		String revokeDate = input.get("REVOKE_DATE");
		String revokeSl = input.get("REVOKE_SL");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (isValidDate(revokeDate) == null) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_DATE);
					return result;
				}
			}
			input.set(ContentManager.TABLE, "REVOKEACCNOMINEE");
			String columns[] = new String[] { getContext().getEntityCode(), revokeDate, revokeSl };
			input.setObject(ContentManager.PROGRAM_KEY, columns);
			result = validatePK(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject getAccountBalance(DTObject input) {
		DTObject resultDTO = new DTObject();
		DBUtil util = getDbContext().createUtilInstance();
		resultDTO.set(ContentManager.ERROR, RegularConstants.NULL);
		try {
			util.reset();
			util.setMode(DBUtil.CALLABLE);
			util.setSql("CALL SP_GET_ACCOUNT_BAL(?,?,?,?,?,?,?,?)");
			util.setLong(1, Long.parseLong(getContext().getEntityCode()));
			util.setLong(2, Long.parseLong(input.get("ACCOUNT_NO")));
			util.registerOutParameter(3, Types.VARCHAR);
			util.registerOutParameter(4, Types.DECIMAL);
			util.registerOutParameter(5, Types.DECIMAL);
			util.registerOutParameter(6, Types.DECIMAL);
			util.registerOutParameter(7, Types.DECIMAL);
			util.registerOutParameter(8, Types.VARCHAR);
			util.execute();
			String status = util.getString(8);
			if (status.equals(RegularConstants.SP_SUCCESS)) {
				resultDTO.set("CCY", util.getString(3));
				resultDTO.set("AC_BAL", util.getString(4));
				resultDTO.set("UNAUTH_AC_CREDITS", util.getString(5));
				resultDTO.set("UNAUTH_AC_DEBITS", util.getString(6));
				resultDTO.set("CURRENT_BAL", util.getString(7));

				util.setMode(DBUtil.PREPARED);
				util.setSql("SELECT SUB_CCY_UNITS_IN_CCY FROM CURRENCY WHERE CCY_CODE=?");
				util.setString(1, resultDTO.get("CCY"));
				ResultSet rs = util.executeQuery();
				if (rs.next()) {
					resultDTO.set("CCY_UNITS", rs.getString(1));
				}

			} else {
				resultDTO.set("SP_ERROR", status);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return resultDTO;
		} finally {
			util.reset();
		}
		return resultDTO;
	}
	
	public final DTObject valildateCustomerID(DTObject input) {
		DTObject result = new DTObject();
		String customerId = input.get("CUSTOMER_ID");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidNumber(customerId)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(customerId, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, customerId);
			input.set(ContentManager.TABLE, "CUSTOMER");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			input.set(ContentManager.COLUMN, "CUSTOMER_ID");
			result = validateEntityWiseCodesWithoutEnabled(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateCustomerAddressSerial(DTObject input) {
		DTObject result = new DTObject();
		String custId = input.get("CUSTOMER_ID");
		String addrSl = input.get("ADDR_SL");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidNumber(addrSl)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_NUMBER);
					return result;
				}
				if (!isValidLength(addrSl, LENGTH_1, LENGTH_5)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.TABLE, "CUSTOMERADDRESS");
			String columns[] = new String[] { getContext().getEntityCode(), custId, addrSl };
			input.setObject(ContentManager.PROGRAM_KEY, columns);
			result = validatePK(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}
}
