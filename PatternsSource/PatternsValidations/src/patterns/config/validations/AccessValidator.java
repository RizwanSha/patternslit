package patterns.config.validations;

import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.CM_LOVREC;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.framework.web.HTMLGridUtility;
import patterns.config.framework.web.PasswordUtils;
import patterns.config.framework.web.SessionConstants;
import patterns.config.framework.web.ajax.ContentManager;

public class AccessValidator extends InterfaceValidator {

	public AccessValidator() {
		super();
	}

	public AccessValidator(DBContext dbContext) {
		super(dbContext);
	}

	public DTObject getPwdDeliveryChoice(DTObject inputDTO) {
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		try {
			String sql = "SELECT PWD_DELIVERY_MECHANISM,PWD_MANUAL_TYPE FROM SYSCPM WHERE ENTITY_CODE=? AND EFFT_DATE =(SELECT MAX(EFFT_DATE) FROM SYSCPM WHERE ENTITY_CODE=? AND EFFT_DATE<= FN_GETCD(?))";
			util.reset();
			util.setSql(sql);
			util.setString(1, getContext().getEntityCode());
			util.setString(2, getContext().getEntityCode());
			util.setString(3, getContext().getEntityCode());
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				inputDTO.set("PWD_DELIVERY_MECHANISM", rset.getString(1));
				inputDTO.set("PWD_MANUAL_TYPE", rset.getString(2));
			}
		} catch (Exception e) {
			e.printStackTrace();
			inputDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			dbContext.close();
			util.reset();
		}
		return inputDTO;
	}

	public final DTObject validateInternalRole(DTObject input) {
		DTObject result = new DTObject();
		String roleCode = input.get("CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {

				if (!isValidCodePattern(roleCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(roleCode, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, roleCode);
			input.set(ContentManager.TABLE, "ROLES");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			input.set(ContentManager.COLUMN, "CODE");
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final Timestamp getSystemCurrentDateTime() {
		Timestamp dateTime = null;
		DBUtil util = getDbContext().createUtilInstance();
		try {
			String sqlQuery = "SELECT FN_GETCDT(?) FROM USERSLOGIN WHERE ENTITY_CODE = ? AND USER_ID = ? AND LOGIN_DATETIME = ?";
			util.reset();
			util.setMode(DBUtil.PREPARED);
			util.setSql(sqlQuery);
			util.setString(1, getContext().getEntityCode());
			util.setString(2, getContext().getEntityCode());
			util.setString(3, getContext().getUserID());
			util.setTimestamp(4, getContext().getLoginDateTime());
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				dateTime = rset.getTimestamp(1);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			util.reset();
		}
		return dateTime;
	}

	public final boolean isLoginValid() {
		boolean valid = false;
		DBUtil util = getDbContext().createUtilInstance();
		try {
			String sqlQuery = "SELECT 1 FROM USERSLOGIN WHERE ENTITY_CODE = ? AND USER_ID = ?  AND LOGIN_DATETIME = ?";
			util.reset();
			util.setMode(DBUtil.PREPARED);
			util.setSql(sqlQuery);
			util.setString(1, getContext().getEntityCode());
			util.setString(2, getContext().getUserID());
			util.setTimestamp(3, getContext().getLoginDateTime());
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				valid = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			util.reset();
		}
		return valid;
	}

	public final DTObject validateInternalUser(DTObject input) {
		DTObject result = new DTObject();
		String value = input.get("USER_ID");
		String table = "USERS";
		String fetch = input.get(ContentManager.FETCH);
		String sqlQuery = null;
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = getDbContext().createUtilInstance();
		try {
			if (fetch != null && fetch.equals(ContentManager.FETCH_ALL)) {
				sqlQuery = "SELECT * FROM " + table + " WHERE PARTITION_NO = ? AND USER_ID = ?";
			} else {
				sqlQuery = "SELECT USER_ID,USER_NAME,STATUS FROM " + table + " WHERE PARTITION_NO = ? AND USER_ID = ?";
			}
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, getContext().getPartitionNo());
			util.setString(2, value);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				decodeRow(rset, result);
				String status = rset.getString("STATUS");
				if (status.equals(CM_LOVREC.COMMON_USERSTATUS_D)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.USER_DEREGISTERED);
				}
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return result;
	}

	public final String getInternalUserPassword(String userID) {
		String result = RegularConstants.EMPTY_STRING;
		DBUtil util = getDbContext().createUtilInstance();
		try {
			String sqlQuery = "SELECT FIRST_PIN FROM USERS WHERE ENTITY_CODE = ? AND USER_ID = ?";
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, getContext().getEntityCode());
			util.setString(2, userID);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result = rset.getString(1);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getLogger().logError("getInternalUserPassword(1) : " + e.getLocalizedMessage());

		} finally {
			util.reset();
		}
		return result;
	}

	public final String getInternalUserRoleCode(String userID) {
		String result = RegularConstants.EMPTY_STRING;
		DBUtil util = getDbContext().createUtilInstance();
		try {
			String sqlQuery = "SELECT FN_GETUSERSROLECD(?,?,?) FROM DUAL";
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, getContext().getPartitionNo());
			util.setString(2, getContext().getEntityCode());
			util.setString(3, userID);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result = rset.getString(1);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			util.reset();
		}
		return result;
	}

	public final String getInternalUserBranchCode(String userID) {
		String result = RegularConstants.EMPTY_STRING;
		DBUtil util = getDbContext().createUtilInstance();
		try {
			String sqlQuery = "SELECT FN_GETUSERSBRANCHCD(?,?) FROM DUAL";
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, getContext().getEntityCode());
			util.setString(2, userID);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result = rset.getString(1);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			util.reset();
		}
		return result;
	}

	public final String getInternalUserCustomerCode(String userID) {
		String result = RegularConstants.EMPTY_STRING;
		DBUtil util = getDbContext().createUtilInstance();
		try {
			String sqlQuery = "SELECT FN_GETUSERSCUSTOMERCD(?,?) FROM DUAL";
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, getContext().getEntityCode());
			util.setString(2, userID);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result = rset.getString(1);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			util.reset();
		}
		return result;
	}

	public final DTObject validateBranchListCode(DTObject input) {
		DTObject result = null;
		input.set(ContentManager.TABLE, "BRANCHLIST");
		result = validateCode(input);
		return result;
	}

	public final DTObject validateAlertMaster(DTObject input) {
		DTObject result = new DTObject();
		String value = input.get("JOB_CODE");
		String sqlQuery = null;
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = getDbContext().createUtilInstance();
		try {
			sqlQuery = "SELECT * FROM JOBMAST WHERE ENTITY_CODE = ? AND JOB_CODE = ?";
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, getContext().getEntityCode());
			util.setString(2, value);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				decodeRow(rset, result);
			} else {
				result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return result;
	}



	public final DTObject validateOption(DTObject input) {
		DTObject result = new DTObject();
		String value = input.get("MPGM_ID");
		String fetch = input.get(ContentManager.FETCH);
		String sqlQuery = null;
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = getDbContext().createUtilInstance();
		try {
			if (fetch != null && fetch.equals(ContentManager.FETCH_ALL)) {
				sqlQuery = "SELECT * FROM MPGM M,MPGMCONFIG C WHERE M.MPGM_ID = ? AND M.MPGM_ID=C.MPGM_ID";
			} else {
				sqlQuery = "SELECT M.MPGM_ID,M.MPGM_DESCN,C.TBA_REQ,C.MPGM_AUTH_REQD FROM MPGM M,MPGMCONFIG C WHERE M.MPGM_ID = ? AND M.MPGM_ID=C.MPGM_ID ";
			}
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, value);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				decodeRow(rset, result);
			} else {
				result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return result;
	}

	public final DTObject validateConsoleOption(DTObject input) {
		DTObject result = new DTObject();
		String value = input.get("MPGM_ID");
		String fetch = input.get(ContentManager.FETCH);
		String sqlQuery = null;
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = getDbContext().createUtilInstance();
		try {
			if (fetch != null && fetch.equals(ContentManager.FETCH_ALL)) {
				sqlQuery = "SELECT * FROM MPGM M WHERE M.MPGM_ID = ?";
			} else {
				sqlQuery = "SELECT M.MPGM_ID,M.MPGM_DESCN FROM MPGM M  WHERE M.MPGM_ID = ? AND M.REQD_MENU='1' ";
			}
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, value);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				decodeRow(rset, result);
			} else {
				result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return result;
	}

	public final DTObject validateAnnouncementID(DTObject input) {
		DTObject result = null;
		input.set(ContentManager.TABLE, "ANNOUNCEMENTS");
		result = validateAnnounceID(input);
		return result;
	}

	public final DTObject isProgramMappedToRoleType(DTObject input) {
		DTObject result = new DTObject();
		String roleType = input.get("ROLE_TYPE");
		String programID = input.get("MPGM_ID");
		String sqlQuery = null;
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = getDbContext().createUtilInstance();
		try {
			sqlQuery = "SELECT 1 FROM ROLEPGMMAP WHERE ENTITY_CODE = ? AND ROLE_TYPE = ? AND PGM_ID = ?";
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, getContext().getEntityCode());
			util.setString(2, roleType);
			util.setString(3, programID);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
			} else {
				result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return result;
	}

	public final boolean isTFARequired(String roleCode, String programID) {
		boolean tfaRequired = false;
		String entityCode = getContext().getEntityCode();
		String sqlQuery = null;
		DBUtil util = getDbContext().createUtilInstance();
		try {
			sqlQuery = "SELECT TFA_MAND FROM USERSPKIAUTH WHERE ENTITY_CODE = ? AND ROLE_CODE = ? AND EFFT_DATE = (SELECT MAX(EFFT_DATE) FROM USERSPKIAUTH WHERE ENTITY_CODE = ? AND ROLE_CODE = ? AND EFFT_DATE <=FN_GETCD(?))";
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, entityCode);
			util.setString(2, roleCode);
			util.setString(3, entityCode);
			util.setString(4, roleCode);
			util.setString(5, entityCode);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				String tfaRequiredValue = rset.getString(1);
				if (tfaRequiredValue.equals(RegularConstants.COLUMN_ENABLE)) {
					tfaRequired = true;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			util.reset();
		}
		return tfaRequired;
	}

	public final boolean isPasswordFromPreviousList(String newHashedPassword, String passwordType, String previousCount) {
		boolean used = false;
		DBUtil util = getDbContext().createUtilInstance();
		try {
			String sqlQuery = "SELECT PIN, SALT FROM (SELECT  @ROWNUM:=@ROWNUM+1 AS RR , PIN, SALT FROM USERSPWDHIST , (SELECT @ROWNUM:=0) RW  WHERE ENTITY_CODE = ? AND USER_ID = ? AND PIN_TYPE= ?  ORDER BY CHANGE_DATETIME DESC) T  WHERE  RR <= ?";
			util.reset();
			util.setMode(DBUtil.PREPARED);
			util.setSql(sqlQuery);
			util.setString(1, getContext().getEntityCode());
			util.setString(2, getContext().getUserID());
			util.setString(3, passwordType);
			util.setString(4, previousCount);
			ResultSet rset = util.executeQuery();
			while (rset.next()) {
				String oldPasswordSaltHashed = rset.getString(1);
				String oldSalt = rset.getString(2);
				String newPasswordSaltHashed = PasswordUtils.getEncryptedHashSalt(newHashedPassword, oldSalt);
				if (newPasswordSaltHashed.equals(oldPasswordSaltHashed)) {
					used = true;
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			util.reset();
		}
		return used;
	}

	public final String getInternalUserPasswordSalt() {
		String passwordSalt = RegularConstants.EMPTY_STRING;
		DBUtil util = getDbContext().createUtilInstance();
		try {
			String sqlQuery = "SELECT FIRST_PIN_SALT FROM USERS WHERE ENTITY_CODE = ? AND USER_ID = ?";
			util.reset();
			util.setMode(DBUtil.PREPARED);
			util.setSql(sqlQuery);
			util.setString(1, getContext().getEntityCode());
			util.setString(2, getContext().getUserID());
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				passwordSalt = rset.getString(1);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			util.reset();
		}
		return passwordSalt;
	}

	public final boolean isRestrictedPassword(String newPasswordHash) {
		boolean restricted = false;
		DBUtil util = getDbContext().createUtilInstance();
		try {
			String sqlQuery = "SELECT PASSWORD FROM PWDNEGLISTDTL WHERE ENTITY_CODE = ? AND EFFT_DATE = (SELECT MAX(EFFT_DATE) FROM PWDNEGLIST WHERE ENTITY_CODE = ? AND EFFT_DATE <= FN_GETCD(?))";
			util.reset();
			util.setMode(DBUtil.PREPARED);
			util.setSql(sqlQuery);
			util.setString(1, getContext().getEntityCode());
			util.setString(2, getContext().getEntityCode());
			util.setString(3, getContext().getEntityCode());
			ResultSet rset = util.executeQuery();
			while (rset.next()) {
				String restrictedPassword = rset.getString(1);
				String userID = getContext().getUserID();
				String restrictedPasswordHash = PasswordUtils.getEncryptedUserIDPassword(restrictedPassword, userID);
				if (newPasswordHash.equals(restrictedPasswordHash)) {
					restricted = true;
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			util.reset();
		}
		return restricted;
	}

	public final DTObject isOptionAllocatedToRole(String programID, String roleCode) {
		DTObject result = new DTObject();
		DBUtil util = getDbContext().createUtilInstance();
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		result.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		String parentProgramID = getParentProgramID(programID);
		try {
			String sqlQuery = "SELECT * FROM ROLEPGMALLOCDTL WHERE ENTITY_CODE = ? AND ROLE_CODE = ? AND PGM_ID = ?";
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, getContext().getEntityCode());
			util.setString(2, roleCode);
			util.setString(3, parentProgramID);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				result.set("ADD_ALLOWED", rset.getString("ADD_ALLOWED"));
				result.set("MODIFY_ALLOWED", rset.getString("MODIFY_ALLOWED"));
				result.set("VIEW_ALLOWED", rset.getString("VIEW_ALLOWED"));
				result.set("AUTHORIZE_ALLOWED", rset.getString("AUTHORIZE_ALLOWED"));
				result.set("REJECTION_ALLOWED", rset.getString("REJECTION_ALLOWED"));
				result.set("PROCESS_ALLOWED", rset.getString("PROCESS_ALLOWED"));

			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			util.reset();
		}
		return result;
	}



	public final boolean isPrefixRestricted(String prefixCode) {
		boolean restricted = false;
		DBUtil util = getDbContext().createUtilInstance();
		try {
			String sqlQuery = "SELECT 1 FROM RESTRICTPFXDTL WHERE ENTITY_CODE = ? AND EFFT_DATE = (SELECT MAX(EFFT_DATE) FROM RESTRICTPFX WHERE ENTITY_CODE = ? AND EFFT_DATE <= FN_GETCD(?)) AND PFX = ?";
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, getContext().getEntityCode());
			util.setString(2, getContext().getEntityCode());
			util.setString(3, getContext().getEntityCode());
			util.setString(4, prefixCode);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				restricted = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			util.reset();
		}
		return restricted;
	}

	public DTObject validateRootCaCode(DTObject input) {
		DTObject result = null;
		input.set(ContentManager.TABLE, "PKICERTAUTH");
		result = validateCode(input);
		return result;
	}

	// added by swaroopa as on 14-05-2012 begins
	public DTObject tfaRequiredForRole(String roleCode, String userId) {
		DTObject result = new DTObject();
		String entityCode = getContext().getEntityCode();
		String sqlQuery = null;
		DBUtil util = getDbContext().createUtilInstance();
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		result.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		try {
			sqlQuery = "SELECT TFA_MAND,LOGIN_AUTH_REQD FROM USERSPKIAUTH WHERE ENTITY_CODE = ? AND ROLE_CODE = ?  AND EFFT_DATE = (SELECT MAX(EFFT_DATE) FROM USERSPKIAUTH WHERE ENTITY_CODE = ? AND ROLE_CODE = ? AND  EFFT_DATE <=FN_GETCD(?))";
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, entityCode);
			util.setString(2, roleCode);
			util.setString(3, entityCode);
			util.setString(4, roleCode);
			util.setString(5, entityCode);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				result.set(SessionConstants.TFA_REQ, rset.getString(1));
				result.set(SessionConstants.LOGIN_TFA_REQ, rset.getString(2));
				if (rset.getString(1).equals(RegularConstants.COLUMN_ENABLE)) {
					sqlQuery = "SELECT ELT(REVOKED,'1','0','1'),INV_NUM  FROM USERSTFA WHERE ENTITY_CODE =? AND USER_ID = ? AND EFFT_DATE = (SELECT MAX(EFFT_DATE) FROM USERSTFA WHERE ENTITY_CODE = ? AND USER_ID = ? AND EFFT_DATE <= FN_GETCD(?))";
					util.reset();
					util.setSql(sqlQuery);
					util.setString(1, entityCode);
					util.setString(2, userId);
					util.setString(3, entityCode);
					util.setString(4, userId);
					util.setString(5, entityCode);
					rset = util.executeQuery();
					if (rset.next()) {
						result.set(SessionConstants.CERT_INV_NUM, rset.getString(2));
						if (rset.getString(1).equals(RegularConstants.COLUMN_DISABLE)) {
							result.set(ContentManager.ERROR, BackOfficeErrorCodes.TFA_MANDATORY);
						}
					} else {
						result.set(ContentManager.ERROR, BackOfficeErrorCodes.TFA_MANDATORY);
					}
				}
			} else {
				result.set(SessionConstants.TFA_REQ, RegularConstants.COLUMN_DISABLE);
			}

		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return result;
	}

	public final DTObject validateCertificateCode(DTObject input) {
		DTObject result = null;
		input.set(ContentManager.TABLE, "PKICERTAUTH");
		result = validateCode(input);
		return result;
	}

	public DTObject validateCertificate(DTObject input) {
		DTObject result = new DTObject();
		DBUtil util = getDbContext().createUtilInstance();
		result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
		result.set(ContentManager.ERROR, RegularConstants.NULL);
		String userID = input.get("USER_ID");
		String fileInventoryNumber = input.get("INV_NUM");
		String rootCaCode = input.get("ROOT_CA_CODE");
		try {
			String sql = "SELECT CERT_SERIAL FROM PKICERTINVENTORY WHERE ENTITY_CODE = ? AND CERT_INV_NUM=?";
			util.reset();
			util.setMode(DBUtil.PREPARED);
			util.setSql(sql);
			util.setString(1, getContext().getEntityCode());
			util.setString(2, fileInventoryNumber);
			ResultSet rset = util.executeQuery();
			String serial = RegularConstants.EMPTY_STRING;
			if (rset.next()) {
				serial = rset.getString(1);
			}
			sql = "SELECT CERT_INV_NUM,DEPLOYMENT_CONTEXT,USER_ID FROM PKICERTINVENTORY WHERE ENTITY_CODE=? AND ROOT_CA_CODE=? AND CERT_SERIAL =? AND NOT (DEPLOYMENT_CONTEXT='I'AND USER_ID=?) AND CERT_IN_USE='1'";
			util.reset();
			util.setMode(DBUtil.PREPARED);
			util.setSql(sql);
			util.setString(1, getContext().getEntityCode());
			util.setString(2, rootCaCode);
			util.setString(3, serial);
			util.setString(4, userID);
			rset = util.executeQuery();
			while (rset.next()) {
				String deploymentContext = rset.getString(2);
				String inventoryNum = rset.getString(1);
				String userId = rset.getString(3);

				if (deploymentContext.equals("E")) {
					sql = "SELECT 1 FROM USERSTFA WHERE ENTITY_CODE=? AND USER_ID=? AND EFFT_DATE=(SELECT MAX(EFFT_DATE) FROM USERSTFA WHERE ENTITY_CODE=? AND USER_ID=? AND EFFT_DATE <=FN_GETCD(?)) AND INV_NUM = ?";
				} else {
					sql = "SELECT 1 FROM USERSTFA WHERE ENTITY_CODE=? AND USER_ID=? AND EFFT_DATE=(SELECT MAX(EFFT_DATE) FROM USERS WHERE ENTITY_CODE=? AND USER_ID=? AND EFFT_DATE <=FN_GETCD(?)) AND INV_NUM = ?";
				}
				DBUtil util1 = getDbContext().createUtilInstance();
				util1.reset();
				util1.setMode(DBUtil.PREPARED);
				util1.setSql(sql);
				util1.setString(1, getContext().getEntityCode());
				util1.setString(2, userId);
				util1.setString(3, getContext().getEntityCode());
				util1.setString(4, userId);
				util1.setString(5, getContext().getEntityCode());
				util1.setString(6, inventoryNum);
				ResultSet rset1 = util1.executeQuery();
				if (rset1.next()) {
					// rejected = true;
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.CERT_ALREADY_ALLOCATED);
					break;
				} else {
					if (deploymentContext.equals("E")) {
						sql = "SELECT 1 FROM USERSTFA WHERE ENTITY_CODE = ? AND USER_ID = ? AND INV_NUM= ? ";
					} else {
						sql = "SELECT 1 FROM USERSTFA WHERE ENTITY_CODE = ? AND USER_ID = ? AND INV_NUM= ?";
					}
					DBUtil util2 = getDbContext().createUtilInstance();
					util2.reset();
					util2.setMode(DBUtil.PREPARED);
					util2.setSql(sql);
					util2.setString(1, getContext().getEntityCode());
					util2.setString(2, userId);
					util2.setString(3, inventoryNum);
					ResultSet rset2 = util2.executeQuery();
					if (rset2.next()) {
						sql = "SELECT COUNT(1) FROM PKIENCDATALOG WHERE ENTITY_CODE=? AND CERT_INV_NUM=?";
						DBUtil util3 = getDbContext().createUtilInstance();
						util3.reset();
						util3.setMode(DBUtil.PREPARED);
						util3.setSql(sql);
						util3.setString(1, getContext().getEntityCode());
						util3.setString(2, inventoryNum);
						util3.executeQuery();
						ResultSet rset3 = util3.executeQuery();
						if (rset3.next()) {
							if (rset3.getInt(1) > 0) {
								// rejected = true;
								result.set(ContentManager.ERROR, BackOfficeErrorCodes.CERT_ALREADY_IN_USE);
								break;
							}
						}
					} else {
						// rejected = true;
						result.set(ContentManager.ERROR, BackOfficeErrorCodes.CERT_ALREADY_ALLOCATED_TO_ANOTHER_USER);
						break;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return result;
	}

	// added by swaroopa as on 14-05-2012 ends
	public final DTObject getAuditlogDetails(DTObject input) {

		DTObject result = new DTObject();
		String sqlQuery = null;
		DBUtil util = getDbContext().createUtilInstance();
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		result.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		try {
			if (!isEmpty(input.get("PK"))) {
				String[] pkInfo = input.get("PK").split("/");

				Date year = new java.sql.Date(BackOfficeFormatUtils.getDate(pkInfo[3], BackOfficeConstants.JAVA_DATE_FORMAT).getTime());
				Calendar c = Calendar.getInstance();
				c.setTime(year);
				Integer x = c.get(Calendar.YEAR);
				String Year = x.toString();

				HTMLGridUtility newImageData = new HTMLGridUtility();
				newImageData.init("stage-grid");
				HTMLGridUtility oldImageData = new HTMLGridUtility();
				oldImageData.init("stage-grid");
				HTMLGridUtility rectifyImageData = new HTMLGridUtility();
				rectifyImageData.init("stage-grid");
				
				newImageData.startHead();
				newImageData.setColumn("Sl", "50", HTMLGridUtility.ColumnAlignment.RIGHT);
				newImageData.setColumn("Table Name", "120", HTMLGridUtility.ColumnAlignment.LEFT);
				newImageData.setColumn("Data", "250", HTMLGridUtility.ColumnAlignment.LEFT);
				newImageData.endHead();

				oldImageData.startHead();
				oldImageData.setColumn("Sl", "50", HTMLGridUtility.ColumnAlignment.RIGHT);
				oldImageData.setColumn("Table Name", "120", HTMLGridUtility.ColumnAlignment.LEFT);
				oldImageData.setColumn("Data", "250", HTMLGridUtility.ColumnAlignment.LEFT);
				oldImageData.endHead();

				rectifyImageData.startHead();
				rectifyImageData.setColumn("Sl", "50", HTMLGridUtility.ColumnAlignment.RIGHT);
				rectifyImageData.setColumn("Table Name", "120", HTMLGridUtility.ColumnAlignment.LEFT);
				rectifyImageData.setColumn("Data", "250", HTMLGridUtility.ColumnAlignment.LEFT);
				rectifyImageData.endHead();

				
				String audittbakey = null;

				sqlQuery = "SELECT A.ATLOG_FORM_NAME ATLOG_FORM_NAME,M.MPGM_DESCN,A.ATLOG_PK ATLOG_PK,TO_CHAR(A.ATLOG_DATE,'" + BackOfficeConstants.TBA_DATE_FORMAT + "') ATLOG_DATE,A.ATLOG_LOG_SL ATLOG_LOG_SL,M.MPGM_DESCN MPGM_DESCN,(SELECT CASE A.ATLOG_ACTION WHEN 'A' THEN 'ADD' WHEN 'M' THEN 'MODIFY' END) AS ATLOG_ACTION,A.ATLOG_TBA_KEY FROM MPGM M,AUDITLOG" + Year
						+ " A WHERE A.ENTITY_CODE=? AND M.MPGM_ID= A.ATLOG_FORM_NAME AND A.ATLOG_FORM_NAME=? AND A.ATLOG_PK=? AND A.ATLOG_DATE = TO_DATE(?,'" + BackOfficeConstants.TBA_XML_DATETIME_FORMAT + "') AND A.ATLOG_LOG_SL=? ";
				util.reset();
				util.setSql(sqlQuery);
				util.setString(1, pkInfo[0]);
				util.setString(2, pkInfo[1]);
				util.setString(3, pkInfo[2]);
				util.setString(4, pkInfo[3]);
				util.setString(5, pkInfo[4]);
				ResultSet rset = util.executeQuery();
				if (rset.next()) {
					result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
					result.set("OPTION_ID", rset.getString(1));
					result.set("OPTION_DESCN", rset.getString(2));
					result.set("LOG_PK", rset.getString(3));
					result.set("LOG_DATETIME", rset.getString(4));
					result.set("LOG_SL", rset.getString(5));
					result.set("MPGM_NAME", rset.getString(6));
					result.set("ACTION", rset.getString(7));
					audittbakey = rset.getString(8);
					sqlQuery = "SELECT ATLOGD_TABLE_NAME,ATLOGD_IMAGE_TYPE, ATLOGD_DATA  FROM AUDITLOGDTL" + Year + " WHERE ENTITY_CODE =? AND ATLOGD_FORM_NAME = ? AND ATLOGD_PK=? AND ATLOGD_DATE =  TO_DATE(?,'" + BackOfficeConstants.TBA_XML_DATETIME_FORMAT + "') AND ATLOGD_LOG_SL=? ORDER BY ATLOGD_DTL_SL";

					util.reset();
					util.setSql(sqlQuery);
					util.setString(1, pkInfo[0]);
					util.setString(2, pkInfo[1]);
					util.setString(3, pkInfo[2]);
					util.setString(4, pkInfo[3]);
					util.setString(5, pkInfo[4]);
					rset = util.executeQuery();
					int oldImgCount = 1;
					int newImgCount = 1;
					int rectifyImgCount = 1;
					
					while (rset.next()) {
						if (rset.getString("ATLOGD_IMAGE_TYPE").equals("N")) {
							newImageData.startRow();
							newImageData.setCell(String.valueOf(newImgCount++));
							newImageData.setCell(rset.getString("ATLOGD_TABLE_NAME"));
							newImageData.setCell(rset.getString("ATLOGD_DATA"), true);
							newImageData.endRow();
						} else if (rset.getString("ATLOGD_IMAGE_TYPE").equals("O")) {
							oldImageData.startRow();
							oldImageData.setCell(String.valueOf(oldImgCount++));
							oldImageData.setCell(rset.getString("ATLOGD_TABLE_NAME"));
							oldImageData.setCell(rset.getString("ATLOGD_DATA"), true);
							oldImageData.endRow();
						}
					}
					util.reset();
					newImageData.finish();
					oldImageData.finish();
					result.set("NEW_IMAGE_XML", newImageData.getHTML());
					result.set("OLD_IMAGE_XML", oldImageData.getHTML());
					if (audittbakey!=null && !audittbakey.equals("")){
						input.set("TBA_KEY", audittbakey);
						input = getRectifyImage(input,rectifyImageData,rectifyImgCount);
						result.set("RECTIFY_IMAGE_XML", input.get("RECTIFY_IMAGE_XML"));
					}else{
						rectifyImageData.finish();
						result.set("RECTIFY_IMAGE_XML", rectifyImageData.getHTML());
					}



					
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return result;
	}


	/*public final DTObject getRectifyImage(DTObject input,HTMLGridUtility rectifyImageData,int rectifyImgCount ) {
		try {
			if (!isEmpty(input.get("PK"))) {
				String[] pkInfo = input.get("PK").split("/");
				String[] tbakey = input.get("TBA_KEY").split("\\|");
				java.sql.Date entrydate = new java.sql.Date(BackOfficeFormatUtils.getDate(tbakey[0],BackOfficeConstants.TBA_REV_DATE_FORMAT).getTime());
				String entrysl = tbakey[1];
				String rectifykey = null;
				String sqlQuery = null;
				DBUtil util = getDbContext().createUtilInstance();
				sqlQuery = "SELECT TBAAUTH_RECTIFY_KEY FROM TBAAUTH WHERE ENTITY_CODE=?  AND  TBAAUTH_PGM_ID = ?  AND TBAAUTH_MAIN_PK= ?  AND TBAAUTH_ENTRY_DATE =?  AND TBAAUTH_ENTRY_SL= ?";
				util.reset();
				util.setSql(sqlQuery);
				util.setString(1, pkInfo[0]);
				util.setString(2, pkInfo[1]);
				util.setString(3, pkInfo[2]);
				util.setDate(4, entrydate);
				util.setString(5, entrysl);
				ResultSet rset = util.executeQuery();
				if (rset.next()) {
					rectifykey = rset.getString("TBAAUTH_RECTIFY_KEY");
				}
				sqlQuery = "SELECT TBADTL_TABLE_NAME,'R',TBADTL_DATA_BLOCK FROM TBAAUTHDTL WHERE ENTITY_CODE =? AND TBADTL_PGM_ID =? AND TBADTL_MAIN_PK= ? AND " + "TBADTL_ENTRY_DATE=? AND TBADTL_ENTRY_SL= ? ORDER BY TBADTL_TABLE_NAME,TBADTL_BLOCK_SL";
				util.reset();
				util.setSql(sqlQuery);
				util.setString(1, pkInfo[0]);
				util.setString(2, pkInfo[1]);
				util.setString(3, pkInfo[2]);
				util.setDate(4, entrydate);
				util.setString(5, entrysl);
				rset = util.executeQuery();
				if (rset.next()) {
						rectifyImageData.startRow();
						rectifyImageData.setCell(String.valueOf(rectifyImgCount++));
						rectifyImageData.setCell(rset.getString("TBADTL_TABLE_NAME"));
						rectifyImageData.setCell(rset.getString("TBADTL_DATA_BLOCK"), true);
						rectifyImageData.endRow();
				}
				if (!isEmpty(rectifykey)) {
					input.set("TBA_KEY", rectifykey);
					input = getRectifyImage(input,rectifyImageData,rectifyImgCount);
				}else{
					util.reset();
					rectifyImageData.finish();
					input.set("RECTIFY_IMAGE_XML", rectifyImageData.getHTML());
					return input;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			input.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
		}
		return input;

	}

*/
	
	public final DTObject getRectifyImage(DTObject input,HTMLGridUtility rectifyImageData,int rectifyImgCount ) {
		try {
			if (!isEmpty(input.get("PK"))) {
				String[] pkInfo = input.get("PK").split("/");
				String[] tbakey = input.get("TBA_KEY").split("\\|");
				java.sql.Date entrydate = new java.sql.Date(BackOfficeFormatUtils.getDate(tbakey[0],BackOfficeConstants.TBA_REV_DATE_FORMAT).getTime());
				String entrysl = tbakey[1];
				String rectifykey = null;
				String sqlQuery = null;
				DBUtil util = getDbContext().createUtilInstance();
				sqlQuery = "SELECT TBAAUTH_RECTIFY_KEY FROM TBAAUTH WHERE ENTITY_CODE=?  AND  TBAAUTH_PGM_ID = ?  AND TBAAUTH_MAIN_PK= ?  AND TBAAUTH_ENTRY_DATE =?  AND TBAAUTH_ENTRY_SL= ?";
				util.reset();
				util.setSql(sqlQuery);
				util.setString(1, pkInfo[0]);
				util.setString(2, pkInfo[1]);
				util.setString(3, pkInfo[2]);
				util.setDate(4, entrydate);
				util.setString(5, entrysl);
				ResultSet rset = util.executeQuery();
				if (rset.next()) {
					rectifykey = rset.getString("TBAAUTH_RECTIFY_KEY");
				}
				sqlQuery = "SELECT TBADTL_TABLE_NAME,'R',TBADTL_DATA_BLOCK FROM TBAAUTHDTL WHERE ENTITY_CODE =? AND TBADTL_PGM_ID =? AND TBADTL_MAIN_PK= ? AND " + "TBADTL_ENTRY_DATE=? AND TBADTL_ENTRY_SL= ? ORDER BY TBADTL_TABLE_NAME,TBADTL_BLOCK_SL";
				util.reset();
				util.setSql(sqlQuery);
				util.setString(1, pkInfo[0]);
				util.setString(2, pkInfo[1]);
				util.setString(3, pkInfo[2]);
				util.setDate(4, entrydate);
				util.setString(5, entrysl);
				rset = util.executeQuery();
				if (rset.next()) {
						rectifyImageData.startRow();
						rectifyImageData.setCell(String.valueOf(rectifyImgCount++));
						rectifyImageData.setCell(rset.getString("TBADTL_TABLE_NAME"));
						rectifyImageData.setCell(rset.getString("TBADTL_DATA_BLOCK"), true);
						rectifyImageData.endRow();
				}
				if (!isEmpty(rectifykey)) {
					input.set("TBA_KEY", rectifykey);
					input = getRectifyImage(input,rectifyImageData,rectifyImgCount);
				}else{
					util.reset();
					rectifyImageData.finish();
					input.set("RECTIFY_IMAGE_XML", rectifyImageData.getHTML());
					return input;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			input.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
		}
		return input;

	}

	

	/*
	 * public final DTObject getAuditlogDetails(DTObject input) { DTObject
	 * result = new DTObject(); String sqlQuery = null; DBUtil util =
	 * getDbContext().createUtilInstance(); result.set(ContentManager.RESULT,
	 * ContentManager.DATA_UNAVAILABLE); result.set(ContentManager.ERROR,
	 * RegularConstants.EMPTY_STRING); try { if (!isEmpty(input.get("PK"))) {
	 * String[] pkInfo = input.get("PK").split("/"); HTMLGridUtility
	 * newImageData = new HTMLGridUtility(); newImageData.init("stage-grid");
	 * HTMLGridUtility oldImageData = new HTMLGridUtility(); DHTMLXGridUtility
	 * auditPKIData = new DHTMLXGridUtility(); oldImageData.init("stage-grid");
	 * 
	 * newImageData.startHead(); newImageData.setColumn("Sl", "50",
	 * HTMLGridUtility.ColumnAlignment.RIGHT);
	 * newImageData.setColumn("Table Name", "120",
	 * HTMLGridUtility.ColumnAlignment.LEFT); newImageData.setColumn("Data",
	 * "250", HTMLGridUtility.ColumnAlignment.LEFT); newImageData.endHead();
	 * 
	 * oldImageData.startHead(); oldImageData.setColumn("Sl", "50",
	 * HTMLGridUtility.ColumnAlignment.RIGHT);
	 * oldImageData.setColumn("Table Name", "120",
	 * HTMLGridUtility.ColumnAlignment.LEFT); oldImageData.setColumn("Data",
	 * "250", HTMLGridUtility.ColumnAlignment.LEFT); oldImageData.endHead();
	 * 
	 * sqlQuery =
	 * "SELECT A.ATLOG_FORM_NAME ATLOG_FORM_NAME,M.MPGM_DESCN,A.ATLOG_PK ATLOG_PK,TO_CHAR(A.ATLOG_DATETIME,'DD-MON-YYYY HH24:MI:SS') ATLOG_DATETIME,A.ATLOG_LOG_SL "
	 * +
	 * " ATLOG_LOG_SL,M.MPGM_DESCN MPGM_DESCN,DECODE(A.ATLOG_ACTION,'A','ADD','M','MODIFY'),DECODE(A.ATLOG_GRP_CODE,'0','',A.ATLOG_GRP_CODE)"
	 * +
	 * " ATLOG_GRP_CODE,DECODE(A.ATLOG_CUST_CODE,'0','',A.ATLOG_CUST_CODE) ATLOG_CUST_CODE,B.MBRN_NAME,C.CUSTOMER_NAME FROM MPGM M,AUDITLOG A"
	 * +
	 * " LEFT OUTER JOIN MBRN B ON  A.ATLOG_ENTITY_NUM=B.MBRN_ENTITY_NUM AND A.ATLOG_GRP_CODE=B.MBRN_CODE LEFT OUTER JOIN CUSTOMER C"
	 * +
	 * " ON A.ATLOG_ENTITY_NUM=C.ENTITY_CODE AND A.ATLOG_CUST_CODE=C.CUSTOMER_CODE WHERE A.ATLOG_ENTITY_NUM=? AND M.MPGM_ID= A.ATLOG_FORM_NAME"
	 * +
	 * " AND A.ATLOG_FORM_NAME=? AND A.ATLOG_PK=? AND A.ATLOG_DATETIME = TO_DATE(?,'DD-MON-YYYY HH24:MI:SS') AND A.ATLOG_LOG_SL=?"
	 * ;
	 * 
	 * util.reset(); util.setSql(sqlQuery); util.setString(1, pkInfo[0]);
	 * util.setString(2, pkInfo[1]); util.setString(3, pkInfo[2]);
	 * util.setString(4, pkInfo[3]); util.setString(5, pkInfo[4]); ResultSet
	 * rset = util.executeQuery(); if (rset.next()) {
	 * result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
	 * result.set("OPTION_ID", rset.getString(1)); result.set("OPTION_DESCN",
	 * rset.getString(2)); result.set("LOG_PK", rset.getString(3));
	 * result.set("LOG_DATETIME", rset.getString(4)); result.set("LOG_SL",
	 * rset.getString(5)); result.set("MPGM_NAME", rset.getString(6));
	 * result.set("ACTION", rset.getString(7)); result.set("GRP_CODE",
	 * rset.getString(8)); result.set("CUST_CODE", rset.getString(9));
	 * result.set("MBRN_NAME", rset.getString(10)); result.set("CUST_NAME",
	 * rset.getString(11)); sqlQuery =
	 * "SELECT ATLOGD_TABLE_NAME,ATLOGD_IMAGE_TYPE,extract(ATLOGD_DATA,'/').GETSTRINGVAL() ATLOGD_DATA  FROM AUDITLOGDTL WHERE ATLOGD_ENTITY_NUM =? AND ATLOGD_FORM_NAME = ? AND ATLOGD_PK=? AND ATLOGD_DATETIME =  TO_DATE(?,'DD-MON-YYYY HH24:MI:SS') AND ATLOGD_LOG_SL=? ORDER BY ATLOGD_DTL_SL"
	 * ; util.reset(); util.setSql(sqlQuery); util.setString(1, pkInfo[0]);
	 * util.setString(2, pkInfo[1]); util.setString(3, pkInfo[2]);
	 * util.setString(4, pkInfo[3]); util.setString(5, pkInfo[4]); rset =
	 * util.executeQuery(); int oldImgCount = 1; int newImgCount = 1; while
	 * (rset.next()) { if (rset.getString("ATLOGD_IMAGE_TYPE").equals("N")) {
	 * newImageData.startRow();
	 * newImageData.setCell(String.valueOf(newImgCount++));
	 * newImageData.setCell(rset.getString("ATLOGD_TABLE_NAME"));
	 * newImageData.setCell(rset.getString("ATLOGD_DATA"), true);
	 * newImageData.endRow(); } else if
	 * (rset.getString("ATLOGD_IMAGE_TYPE").equals("O")) {
	 * oldImageData.startRow();
	 * oldImageData.setCell(String.valueOf(oldImgCount++));
	 * oldImageData.setCell(rset.getString("ATLOGD_TABLE_NAME"));
	 * oldImageData.setCell(rset.getString("ATLOGD_DATA"), true);
	 * oldImageData.endRow(); } } util.reset(); newImageData.finish();
	 * oldImageData.finish(); result.set("NEW_IMAGE_XML",
	 * newImageData.getHTML()); result.set("OLD_IMAGE_XML",
	 * oldImageData.getHTML());
	 * 
	 * sqlQuery =
	 * "SELECT A.ATLOGP_ENTITY_NUM ATLOGP_ENTITY_NUM,A.ATLOGP_PKI_INV_NUM ATLOGP_PKI_INV_NUM,A.ATLOGP_DTL_SL ATLOGP_DTL_SL,A.ATLOGP_USER_ID ATLOGP_USER_ID,"
	 * +
	 * "B.USER_NAME USER_NAME,DECODE(A.ATLOGP_ACTION,'I','Initiator','F','First Authorizer','S','Second Authorizer')ATLOGP_ACTION,"
	 * +
	 * "TO_CHAR(A.ATLOGP_ACTION_ON,'DD-MON-YYYY HH24:MI:SS') ATLOGP_ACTION_ON,A.ATLOGP_IP ATLOGP_IP,"
	 * +
	 * "DECODE(A.ATLOGP_PKI_INV_NUM,NULL,'No','Yes')TFA_VERIFIED  FROM AUDITLOGPKIDTL A,USERS B WHERE A.ATLOGP_ENTITY_NUM =?"
	 * +
	 * " AND A.ATLOGP_FORM_NAME = ? AND A.ATLOGP_PK=? AND A.ATLOGP_DATETIME = TO_DATE(?,'DD-MON-YYYY HH24:MI:SS') AND A.ATLOGP_LOG_SL=? AND "
	 * +
	 * "A.ATLOGP_ENTITY_NUM=B.ENTITY_CODE AND A.ATLOGP_USER_ID=B.USER_ID ORDER BY ATLOGP_DTL_SL"
	 * ; util.reset(); util.setSql(sqlQuery); util.setString(1, pkInfo[0]);
	 * util.setString(2, pkInfo[1]); util.setString(3, pkInfo[2]);
	 * util.setString(4, pkInfo[3]); util.setString(5, pkInfo[4]); rset =
	 * util.executeQuery(); auditPKIData.init(); while (rset.next()) {
	 * auditPKIData.startRow();
	 * auditPKIData.setCell(RegularConstants.EMPTY_STRING);
	 * auditPKIData.setCell(rset.getString("ATLOGP_ENTITY_NUM") + "|" +
	 * rset.getString("ATLOGP_PKI_INV_NUM"));
	 * auditPKIData.setCell(rset.getString("ATLOGP_DTL_SL"));
	 * auditPKIData.setCell(rset.getString("ATLOGP_USER_ID"));
	 * auditPKIData.setCell(rset.getString("USER_NAME"));
	 * auditPKIData.setCell(rset.getString("ATLOGP_ACTION"));
	 * auditPKIData.setCell(rset.getString("ATLOGP_ACTION_ON"));
	 * auditPKIData.setCell(rset.getString("ATLOGP_IP"));
	 * auditPKIData.setCell(rset.getString("TFA_VERIFIED"));
	 * auditPKIData.endRow(); } util.reset(); auditPKIData.finish();
	 * result.set("AUDIT_PKI_XML", auditPKIData.getXML()); } } } catch
	 * (Exception e) { e.printStackTrace(); result.set(ContentManager.ERROR,
	 * BackOfficeErrorCodes.UNSPECIFIED_ERROR); } finally { util.reset(); }
	 * return result; }
	 */

	public final DTObject getAuditPKIDetails(DTObject input) {
		DTObject result = new DTObject();
		String sqlQuery = null;
		DBUtil util = getDbContext().createUtilInstance();
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		result.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		try {
			if (!isEmpty(input.get("INV_NUM_PK"))) {
				String[] pkInfo = input.get("INV_NUM_PK").split("\\|");
				sqlQuery = "SELECT A.ACTION_BY ACTION_BY,B.USER_ID,B.USER_NAME USER_NAME ,TO_CHAR(A.ACTION_ON,'DD-MON-YYYY HH24:MI:SS') ACTION_ON,A.ACTION_IP ACTION_IP," + "A.ACTUAL_VALUE ACTUAL_VALUE,A.ENCODED_VALUE ENCODED_VALUE,CERT_INV_NUM FROM PKIENCDATALOG A,USERS B WHERE A.ENTITY_CODE=? AND A.INV_NUM=? AND A.ENTITY_CODE=B.ENTITY_CODE AND A.ACTION_BY=B.USER_ID AND A.ACTION_CONTEXT='I' ";
				util.reset();
				util.setSql(sqlQuery);
				util.setString(1, pkInfo[0]);
				util.setString(2, pkInfo[1]);
				ResultSet rset = util.executeQuery();
				if (rset.next()) {
					result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
					result.set("ACTION_BY", rset.getString("ACTION_BY"));
					result.set("USER_ID", rset.getString("USER_ID"));
					result.set("USER_NAME", rset.getString("USER_NAME"));
					result.set("ACTION_ON", rset.getString("ACTION_ON"));
					result.set("ACTION_IP", rset.getString("ACTION_IP"));
					result.set("ACTUAL_VALUE", rset.getString("ACTUAL_VALUE"));
					result.set("ENCODED_VALUE", rset.getString("ENCODED_VALUE"));
					result.set("CERT_INV_NUM", rset.getString("CERT_INV_NUM"));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return result;
	}

	public final DTObject validateLocationCode(DTObject input) {
		input.set(ContentManager.TABLE, "CMNMLOCATION");
		input.set(ContentManager.COLUMN, "LOC_CODE");
		input = validateGenericCode(input);
		return input;
	}

	public final DTObject validateCountryCode(DTObject input) {
		input.set(ContentManager.TABLE, "CMNMCOUNTRY");
		input.set(ContentManager.COLUMN, "CNTRY_CODE");
		input = validateGenericCode(input);
		return input;
	}

	public final DTObject validateDesignation(DTObject input) {
		DTObject result = new DTObject();
		String designation = input.get("CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(designation)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_FORMAT);
					return result;
				}
			}
			input.set(ContentManager.TABLE, "PERSONDESIG");
			input.set(ContentManager.COLUMN, "PERSON_DESIG_CODE");
			result = validateGenericCode(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public DTObject validateUserId(DTObject input) {
		DTObject result = new DTObject();
		String userId = input.get("CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(userId)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_FORMAT);
					return result;
				}
			}
			input.set(ContentManager.TABLE, "USERS");
			input.set(ContentManager.COLUMN, "USER_ID");
			result = validateEntityWiseCodesWithoutEnabled(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateConsoleCode(DTObject input) {
		DTObject result = new DTObject();
		String consoleCode = input.get("CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {

				if (!isValidCodePattern(consoleCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(consoleCode, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.TABLE, "CONSOLE");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			input.set(ContentManager.COLUMN, "CODE");
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateEmployeeCode(DTObject input) {
		DTObject result = new DTObject();
		String employeeCode = input.get("EMP_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(employeeCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(employeeCode, LENGTH_1, LENGTH_15)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, employeeCode);
			input.set(ContentManager.TABLE, "EMPDB");
			input.set(ContentManager.COLUMN, "EMP_CODE");
			input.set(ContentManager.ENTITY_CODE, "ENTITY_CODE");
			result = validateEntityWiseCodesWithoutEnabled(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateEmployeeClosure(DTObject input) {
		DTObject result = new DTObject();
		DTObject employeeDTO = new DTObject();
		DTObject resultEmployeeDTO = new DTObject();
		String employeeCode = input.get("EMP_CODE");
		try {

			employeeDTO.set("EMP_CODE", employeeCode);
			employeeDTO.set("ACTION", USAGE);
			resultEmployeeDTO = validateEmployeeCode(employeeDTO);
			if (resultEmployeeDTO.get(ContentManager.ERROR) != null) {
				result.set(ContentManager.ERROR, resultEmployeeDTO.get(ContentManager.ERROR));
				return result;
			}
			if (resultEmployeeDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_EMP_CODE);
				return result;
			}

			input.set(ContentManager.TABLE, "EMPDBCLOSE");
			String columns[] = new String[] { getContext().getEntityCode(), employeeCode };
			input.setObject(ContentManager.PROGRAM_KEY, columns);
			result = validatePK(input);

		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}

		return result;
	}

	public final DTObject getEmployeeAndPersonDBDetails(DTObject input) {
		DTObject result = new DTObject();
		String employeeCode = input.get("EMP_CODE");
		try {
			input.set(ContentManager.CODE, employeeCode);
			input.set(ContentManager.COLUMN, "EMP_CODE");
			input.set(ContentManager.ENTITY_CODE, "ENTITY_CODE");
			result = getFetchEmployeeAndPersonDBDetails(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateModuleCode(DTObject input) {
		DTObject result = new DTObject();
		try {
			input.set(ContentManager.TABLE, "MODULE");
			input.set(ContentManager.COLUMN, "MODULE_ID");
			result = validateGenericCode(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validatePrintType(DTObject input) {
		DTObject result = new DTObject();
		String printTypeId = input.get("PRNTYPE_ID");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {

				if (!isValidCodePattern(printTypeId)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(printTypeId, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, printTypeId);
			input.set(ContentManager.TABLE, "PRNTYPE");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			input.set(ContentManager.COLUMN, "PRNTYPE_ID");
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validatePrinterId(DTObject input) {
		DTObject result = new DTObject();
		String printerId = input.get("PRINTER_ID");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {

				if (!isValidCodePattern(printerId)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(printerId, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, printerId);
			input.set(ContentManager.TABLE, "PRINTERREG");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			input.set(ContentManager.COLUMN, "PRINTER_ID");
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateEntityBranch(DTObject input) {
		DTObject result = new DTObject();
		String entityBranchCode = input.get("BRANCH_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {

				if (!isValidCodePattern(entityBranchCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(entityBranchCode, LENGTH_1, LENGTH_6)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, entityBranchCode);
			input.set(ContentManager.TABLE, "ENTITYBRN");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			input.set(ContentManager.COLUMN, "BRANCH_CODE");
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateEntityBranchList(DTObject input) {
		DTObject result = new DTObject();
		String entityBranchList = input.get("BRNLIST_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {

				if (!isValidCodePattern(entityBranchList)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(entityBranchList, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, entityBranchList);
			input.set(ContentManager.TABLE, "ENTITYBRNLIST");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			input.set(ContentManager.COLUMN, "BRNLIST_CODE");
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validatePrintQueueId(DTObject input) {
		DTObject result = new DTObject();
		String printQueueId = input.get("PRNQ_ID");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {

				if (!isValidCodePattern(printQueueId)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(printQueueId, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, printQueueId);
			input.set(ContentManager.TABLE, "PRNQ");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			input.set(ContentManager.COLUMN, "PRNQ_ID");
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public boolean isSODDone() {
		boolean flag = false;
		DBUtil util = getDbContext().createUtilInstance();
		try {
			String sqlQuery = "SELECT SOD_COMPL FROM MAINCONT WHERE ENTITY_CODE = ? ";
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, getContext().getEntityCode());
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				if (rset.getString(1).equals(RegularConstants.COLUMN_ENABLE)) {
					flag = true;
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
			flag = false;
		} finally {
			util.reset();
		}
		return flag;
	}


	public final DTObject validateProgramOption(DTObject input) {
		DTObject result = new DTObject();
		String programID = input.get("MPGM_ID");
		String sqlQuery = null;
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = getDbContext().createUtilInstance();
		try {
			sqlQuery = "SELECT * FROM MPGM WHERE  MPGM_ID = ?";
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, programID);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
			} else {
				result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return result;
	}

	public final DTObject validateEmailCode(DTObject input) {
		DTObject result = new DTObject();
		String emailCode = input.get("EMAIL_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {

				if (!isValidCodePattern(emailCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(emailCode, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, emailCode);
			input.set(ContentManager.TABLE, "EMAILINTF");
			input.set(ContentManager.COLUMN, "EMAIL_CODE");
			result = validateGenericCode(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}
	
	public final DTObject validateSMSCode(DTObject input) {
		DTObject result = new DTObject();
		String smsCode = input.get("SMS_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {

				if (!isValidCodePattern(smsCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(smsCode, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, smsCode);
			input.set(ContentManager.TABLE, "SMSINTF");
			input.set(ContentManager.COLUMN, "SMS_CODE");
			result = validateGenericCode(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}
	
	public final DTObject validateEmployeeExit(DTObject input) {
		DTObject result = new DTObject();
		String employeeNo = input.get("EMP_ID");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {

				if (!isValidCodePattern(employeeNo)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(employeeNo, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, employeeNo);
			input.set(ContentManager.TABLE, "EMPEXIT");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			input.set(ContentManager.COLUMN, "EMP_ID");
			result = validateEntityWiseCodesWithoutEnabled(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject getClusterCode(DTObject input) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = getDbContext().createUtilInstance();
		try {
			String sqlQuery = "SELECT FN_GET_CLUSTERCODE(?,?) FROM DUAL";
			util.reset();
			util.setMode(DBUtil.PREPARED);
			util.setSql(sqlQuery);
			util.setString(1, input.get(ContentManager.ENTITY_CODE));
			util.setString(2, input.get("OFFICE_CODE"));
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				decodeRow(rset, resultDTO);
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return resultDTO;
	}
	public final DTObject validateBranchCBSControl(DTObject input) {
		DTObject result = new DTObject();
		String entityBranchCode = input.get("BRANCH_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {

				if (!isValidCodePattern(entityBranchCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(entityBranchCode, LENGTH_1, LENGTH_6)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, entityBranchCode);
			input.set(ContentManager.TABLE, "BRNCBSCONTROL");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			input.set(ContentManager.COLUMN, "BRANCH_CODE");
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}
	
	

}