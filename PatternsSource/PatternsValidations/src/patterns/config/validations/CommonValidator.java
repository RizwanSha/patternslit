package patterns.config.validations;

import java.io.File;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

import org.apache.commons.lang3.ArrayUtils;
import org.jboss.resteasy.util.Base64;
import org.quartz.CronExpression;

import patterns.config.constants.ProgramConstants;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.BindParameterType;
import patterns.config.framework.database.CM_LOVREC;
import patterns.config.framework.database.DBEntry;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.objects.TableInfo;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBInfo;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.loggers.ApplicationLogger;
import patterns.config.framework.process.TBAActionType;
import patterns.config.framework.service.DTDObject;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.thread.ApplicationContext;
import patterns.config.framework.thread.WebContext;
import patterns.config.framework.web.BackOfficeDateUtils;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.framework.web.DHTMLXGridUtility;
import patterns.config.framework.web.ajax.ContentManager;

/**
 * This Class implements validator method for all master codes
 * 
 * @author Jainudeen
 * @version 1.0
 * @since 2014-NOV-07
 * 
 * 
 * 
 *        Sl.No VersioNo. Modified By Modified Date Changes Done
 *        ----------------
 *        --------------------------------------------------------
 *        ----------------
 *        ----------------------------------------------------------- 1 1.1
 *        Rizwan Sha S 14-Nov-2014 1.Modified in validTransactionalGL Changed
 *        from COLUMN_ENABLE to COLUMN_DISABLE 2.Removed unwanted import
 *        package.
 * 
 *        2 1.2 Rizwan Sha S 17-Nov-2014 1.Changed in
 *        validateCode,validateGenericCode method from action.equals(USAGE) to
 *        USAGE.equals(action). In access module validator methods was invoked
 *        with out setting action value ,methods was throwing Null pointer
 *        exception changes were made to avoid exception.
 * 
 *        3 1.3 Rizwan Sha S 19-Nov-2014 validRegPolicy new method added. 4 1.4
 *        Rizwan Sha S 27-Nov-2014 New
 *        validPersonIDWithDoctorCode,validRegNoWithDoctorCode methods added.
 * 
 */

public class CommonValidator {

	public static final String ADD = "A";
	public static final String MODIFY = "M";
	public static final String USAGE = "U";

	private ApplicationLogger logger = null;
	private ApplicationContext context = ApplicationContext.getInstance();
	private WebContext webContext = WebContext.getInstance();
	private DBContext dbContext = null;

	public ApplicationLogger getLogger() {
		return logger;
	}

	public CommonValidator() {
		logger = ApplicationLogger.getInstance(this.getClass().getSimpleName());
		this.dbContext = new DBContext();
	}

	public CommonValidator(DBContext dbContext) {
		logger = ApplicationLogger.getInstance(this.getClass().getSimpleName());
		this.dbContext = dbContext;
	}

	public DBContext getDbContext() {
		return dbContext;
	}

	public ApplicationContext getContext() {
		return context;
	}

	public WebContext getWebContext() {
		return webContext;
	}

	// COMMON FIELDS
	public static final String AUDIT_FIELDS = "CR_BY,CR_ON,MO_BY,MO_ON,AU_BY,AU_ON,TBA_KEY";
	public final int LENGTH_0 = 0;
	public final int LENGTH_1 = 1;
	public final int LENGTH_2 = 2;
	public final int LENGTH_3 = 3;
	public final int LENGTH_4 = 4;
	public final int LENGTH_5 = 5;
	public final int LENGTH_6 = 6;
	public final int LENGTH_8 = 8;
	public final int LENGTH_10 = 10;
	public final int LENGTH_20 = 20;
	public final int LENGTH_12 = 12;
	public final int LENGTH_15 = 15;
	public final int LENGTH_35 = 35;
	public final int LENGTH_25 = 25;
	public final int LENGTH_50 = 50;
	public static final int LENGTH_100 = 100;

	public static final int CODE_LENGTH = 12;
	public static final int BENFID_LENGTH = 35;
	public static final int SERVICE_LENGTH = 35;
	public static final int RPTCODE_LENGTH = 50;
	public static final int PREFIX_LENGTH = 6;
	// Rizwan Begins
	public static final int INFORMATION_LENGTH25 = 25;
	public static final int INFORMATION_LENGTH50 = 50;
	// Rizwan Ends
	public static final int DESCRIPTION_LENGTH = 100;
	public static final int CONCISE_DESCRIPTION_LENGTH = 20;
	public static final int EXTERNAL_DESCRIPTION_LENGTH = 50;
	public static final int EMAIL_LENGTH = 100;
	public static final int DESIGNATION_LENGTH = 100;
	public static final int PATH_LENGTH = 250;
	public static final int CRON_LENGTH = 50;
	public static final int REMARKS_LENGTH = 250;
	public static final int MOREREMARKS_LENGTH = 350;
	public static final int ADDRESS_LENGTH = 250;
	public static final int URL_LENGTH = 128;
	public static final int KEY_LENGTH = 128;
	public static final int VALUE_LENGTH = 128;
	public static final int MOBILE_NUMBER_LENGTH = 10;
	public static final int CHEQUE_LENGTH = 15;
	public static final int WITHDRAWAL_SLIP_LENGTH = 12;
	public static final int RECEIPT_LENGTH = 15;

	public static final int NAME_LENGTH = 100;
	public static final int USER_ID_LENGTH = 50;
	public static final int CREDENTIAL_LENGTH = 128;
	public static final int SERVER_ADDRESS_LENGTH = 128;
	public static final int PREFIX_CODE_LENGTH = 8;
	public static final int PROGRAM_ID_LENGTH = 50;
	public static final int ADDRESS_LINE = 50;

	public static final int CUSTOMERCODE_LENGTH = 50;
	public static final int INFORMATION_LENGTH100= 100;
	public static final int IFSC_LENGTH = 25;
	public static final int ACCOUNT_NUMBER_LENGTH = 35;

	public static final int TRAN_ID_LENGTH = 20;
	public static final int REFERENCE_NO_LENGTH = 25;
	public static final int INVALID_QUESTION_LENGTH = 250;
	public static final int INVALID_ANSWER_LENGTH = 4000;
	public static final int EXTENSION_LENGTH = 6;
	public static final int PAN_LENGTH = 10;
	public static final int TAN_LENGTH = 10;

	public void close() {
		getDbContext().close();
	}

	public final DTObject decodeRow(ResultSet rset, DTObject result) throws SQLException {
		for (int i = 1; i < rset.getMetaData().getColumnCount() + 1; i++) {
			if (rset.getMetaData().getColumnTypeName(i).equals("DATE")) {
				result.setObject(rset.getMetaData().getColumnName(i), rset.getTimestamp(i));
			} else if (rset.getMetaData().getColumnTypeName(i).equals("DATETIME") || rset.getMetaData().getColumnTypeName(i).equals("TIMESTAMP")) {
				result.setObject(rset.getMetaData().getColumnName(i), rset.getTimestamp(i));
			} else if (rset.getMetaData().getColumnTypeName(i).equals("BLOB") || rset.getMetaData().getColumnTypeName(i).equals("LONGBLOB")) {
				result.setObject(rset.getMetaData().getColumnName(i), rset.getBytes(i));
			} else {
				String value = rset.getString(i);
				if (value == null)
					value = "";
				result.set(rset.getMetaData().getColumnName(i), value);
			}
		}
		return result;
	}

	public final void removeAuditColumns(DTObject result) {
		result.remove("CR_BY");
		result.remove("CR_ON");
		result.remove("MO_BY");
		result.remove("MO_ON");
		result.remove("AU_BY");
		result.remove("AU_ON");
		result.remove("TBA_KEY");

	}

	public final boolean checkDuplicateRows(DTDObject dtdObject, int index) {
		ArrayList<String> list = new ArrayList<String>();
		for (int i = 0; i < dtdObject.getRowCount(); i++) {
			String value = dtdObject.getValue(i, index);
			if (list.contains(value)) {
				return false;
			}
			list.add(value);
		}
		return true;
	}

	public final boolean checkDuplicateRows(DTDObject dtdObject, int index1, int index2) {
		ArrayList<String> list = new ArrayList<String>();
		for (int i = 0; i < dtdObject.getRowCount(); i++) {
			String value = dtdObject.getValue(i, index1) + RegularConstants.PK_SEPARATOR + dtdObject.getValue(i, index2);
			if (list.contains(value)) {
				return false;
			}
			list.add(value);
		}
		return true;
	}

	public final boolean checkDuplicateRows(DTDObject dtdObject, int index1, int index2, int index3) {
		ArrayList<String> list = new ArrayList<String>();
		for (int i = 0; i < dtdObject.getRowCount(); i++) {
			String value = dtdObject.getValue(i, index1) + RegularConstants.PK_SEPARATOR + dtdObject.getValue(i, index2) + RegularConstants.PK_SEPARATOR + dtdObject.getValue(i, index3);
			if (list.contains(value)) {
				return false;
			}
			list.add(value);
		}
		return true;
	}

	public final boolean isRecordAvailable(String tableName, Map<Integer, DBEntry> whereClauseFieldsMap) {
		DBUtil dbutil = getDbContext().createUtilInstance();
		boolean available = false;
		ResultSet rset = null;
		StringBuffer sqlBuffer = new StringBuffer(160);
		sqlBuffer.append("SELECT COUNT(1) FROM ");
		sqlBuffer.append(tableName);
		sqlBuffer.append(" WHERE ");
		TreeSet<Integer> orderedSet = new TreeSet<Integer>(whereClauseFieldsMap.keySet());
		for (Integer fieldIndex : orderedSet) {
			DBEntry entry = whereClauseFieldsMap.get(fieldIndex);
			sqlBuffer.append(entry.getName());
			sqlBuffer.append(" = ?");
			sqlBuffer.append(" AND ");
		}
		String sql = sqlBuffer.substring(0, sqlBuffer.lastIndexOf(" AND "));
		try {

			dbutil.reset();
			dbutil.setMode(DBUtil.PREPARED);
			dbutil.setSql(sql);
			for (Map.Entry<Integer, DBEntry> mapEntry : whereClauseFieldsMap.entrySet()) {
				Integer fieldIndex = mapEntry.getKey();
				DBEntry entry = mapEntry.getValue();
				Object value = entry.getValue();
				switch (entry.getType()) {
				case VARCHAR:
					dbutil.setString(fieldIndex, (String) value);
					break;
				case DATE:
					if (value instanceof java.sql.Date) {
						dbutil.setDate(fieldIndex, (java.sql.Date) value);
					} else if (value instanceof java.util.Date) {
						dbutil.setDate(fieldIndex, new java.sql.Date(((java.util.Date) value).getTime()));
					} else {
						SimpleDateFormat sdf = new SimpleDateFormat(getContext().getDateFormat());
						java.sql.Date date = null;
						try {
							date = new java.sql.Date(sdf.parse((String) value).getTime());
						} catch (ParseException e) {
							e.printStackTrace();
						}
						dbutil.setDate(fieldIndex, date);
					}
					break;
				case TIMESTAMP:
					dbutil.setTimestamp(fieldIndex, (Timestamp) value);
					break;
				case INTEGER:
					dbutil.setInt(fieldIndex, (Integer) value);
					break;
				case LONG:
					dbutil.setLong(fieldIndex, (Long) value);
					break;
				case BIGDECIMAL:
					dbutil.setBigDecimal(fieldIndex, (BigDecimal) value);
					break;
				default:
					break;
				}
			}
			rset = dbutil.executeQuery();
			if (rset.next()) {
				int count = rset.getInt(1);
				if (count > 0)
					available = true;
			}
		} catch (SQLException e) {
			available = false;
		} finally {
			if (dbutil != null)
				dbutil.reset();
		}
		return available;
	}

	public final String decodeCMLOVREC(String programID, String lovID, String lovValue, String a) {
		Connection conn = null;// get connection
		PreparedStatement stmt = null;
		ResultSet rset = null;
		// DBUtil util = getDbContext().createUtilInstance();
		String description = RegularConstants.EMPTY_STRING;
		try {
			String sqlQuery = "SELECT DECODE_CMLOVREC(?,?,?) FROM DUAL";
			stmt = conn.prepareStatement(sqlQuery);
			stmt.setString(1, programID);
			stmt.setString(2, lovID);
			stmt.setString(3, lovValue);
			rset = stmt.executeQuery();
			if (rset.next()) {
				description = rset.getString(1);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (Exception e) {
				}
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (Exception e) {
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (Exception e) {
				}
			}

		}
		return description;
	}

	public final String decodeCMLOVREC(String programID, String lovID, String lovValue) {
		DBUtil util = getDbContext().createUtilInstance();
		String description = RegularConstants.EMPTY_STRING;
		try {
			String sqlQuery = "SELECT DECODE_CMLOVREC(?,?,?) FROM DUAL";
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, programID);
			util.setString(2, lovID);
			util.setString(3, lovValue);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				description = rset.getString(1);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			util.reset();
		}
		return description;
	}

	public final boolean isCMLOVREC(String programID, String lovID, String lovValue) {
		boolean available = false;
		DBUtil util = getDbContext().createUtilInstance();
		try {
			String sqlQuery = "SELECT 1 FROM CMLOVREC WHERE PGM_ID = ? AND LOV_ID = ? AND LOV_VALUE = ?";
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, programID);
			util.setString(2, lovID);
			util.setString(3, lovValue);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				available = true;
			} else {
				available = false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			available = false;
		} finally {
			util.reset();
		}
		return available;
	}

	public final String getCMLOVLABEL(String programID, String lovID, String lovValue) {
		String lovLabel = RegularConstants.EMPTY_STRING;
		DBUtil util = getDbContext().createUtilInstance();
		try {
			String sqlQuery = "SELECT LOV_LABEL_EN_US FROM CMLOVREC WHERE PGM_ID = ? AND LOV_ID = ? AND LOV_VALUE = ?";
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, programID);
			util.setString(2, lovID);
			util.setString(3, lovValue);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				lovLabel = rset.getString(1);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			util.reset();
		}
		return lovLabel;
	}

	public final DTObject validateFrameworkCode(DTObject input) {
		DTObject result = new DTObject();
		String value = input.get(ContentManager.CODE);
		String table = input.get(ContentManager.TABLE);
		String fetch = input.get(ContentManager.FETCH);
		String sqlQuery = null;
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = getDbContext().createUtilInstance();
		try {
			if (fetch != null && fetch.equals(ContentManager.FETCH_ALL)) {
				sqlQuery = "SELECT * FROM " + table + " WHERE ENTITY_CODE = ? AND CODE = ?";
			} else {
				sqlQuery = "SELECT CODE,DESCRIPTION FROM " + table + " WHERE ENTITY_CODE = ? AND CODE = ?";
			}
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, getContext().getEntityCode());
			util.setString(2, value);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				decodeRow(rset, result);
			} else {
				result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return result;
	}

	public final DTObject validateAnnounceID(DTObject input) {
		DTObject result = new DTObject();
		String value = input.get("ANNOUNCE_ID");
		String table = input.get(ContentManager.TABLE);
		String fetch = input.get(ContentManager.FETCH);
		String sqlQuery = null;
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = getDbContext().createUtilInstance();
		try {
			if (fetch != null && fetch.equals(ContentManager.FETCH_ALL)) {
				sqlQuery = "SELECT * FROM " + table + " WHERE ENTITY_CODE = ? AND ANNOUNCE_ID = ?";
			} else {
				sqlQuery = "SELECT ANNOUNCE_ID,ANNOUNCE_TITLE FROM " + table + " WHERE ENTITY_CODE = ? AND ANNOUNCE_ID = ?";
			}
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, getContext().getEntityCode());
			util.setString(2, value);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				decodeRow(rset, result);
			} else {
				result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
			}
		} catch (Exception e) {
			result.set(ContentManager.ERROR, e.getLocalizedMessage());
		} finally {
			util.reset();
		}
		return result;
	}

	public final boolean isEmpty(String code) {
		if (code == null || code.trim().length() == 0)
			return true;
		return false;
	}

	public final boolean isEmptySpace(String code) {
		if (code != null && code.trim().length() == 0)
			return true;
		return false;
	}

	public final boolean isValidLength(String value, int minLength, int maxLength) {
		if (value.length() >= minLength && value.length() <= maxLength)
			return true;
		return false;
	}

	/**** FILE VALIDATIONS BEGIN ****/
	public final boolean isFile(String path) {
		File f = new File(path);
		if (!f.isFile())
			return false;
		return true;
	}

	public final boolean isFileAvailable(String path) {
		File f = new File(path);
		if (!f.exists())
			return false;
		return true;
	}

	public final boolean isFileReadEnabled(String path) {
		File f = new File(path);
		if (!f.canRead())
			return false;
		return true;
	}

	public final boolean isFileWriteEnabled(String path) {
		File f = new File(path);
		if (!f.canWrite())
			return false;
		return true;
	}

	public final boolean isFolder(String path) {
		File f = new File(path);
		if (!f.isDirectory())
			return false;
		return true;
	}

	public final boolean isFolderAvailable(String path) {
		File f = new File(path);
		if (!f.exists())
			return false;
		return true;
	}

	public final boolean isFolderReadEnabled(String path) {
		File f = new File(path);
		if (!f.canRead())
			return false;
		return true;
	}

	public final boolean isFolderWriteEnabled(String path) {
		File f = new File(path);
		if (!f.canWrite())
			return false;
		return true;
	}

	/**** FILE VALIDATIONS END ****/

	/**** TIME VALIDATIONS BEGIN ****/

	public final boolean isValidTime(String value) {
		return isValidTime(value, BackOfficeConstants.TIME_HH24_MM);
	}

	public final boolean isValidTime(String value, String format) {
		try {
			String[] time = value.split(RegularConstants.TIME_SEPARATOR);
			int hh = -1;
			int mm = -1;
			int ss = -1;
			if (format.equals(BackOfficeConstants.TIME_HH24_MM)) {
				hh = Integer.parseInt(time[0]);
				mm = Integer.parseInt(time[1]);
				if ((hh >= 0 && hh < 24) && (mm >= 0 && mm < 60)) {
					return true;
				}
			} else if (format.equals(BackOfficeConstants.TIME_HH24_MM_SS)) {
				hh = Integer.parseInt(time[0]);
				mm = Integer.parseInt(time[1]);
				ss = Integer.parseInt(time[2]);
				if ((hh >= 0 && hh < 24) && (mm >= 0 && mm < 60) && (ss >= 0 && ss < 60)) {
					return true;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public final boolean isTimeGreater(String value1, String value2) {
		return isTimeGreater(value1, value2, BackOfficeConstants.TIME_HH24_MM);
	}

	public final boolean isTimeGreater(String value1, String value2, String format) {
		if (isValidTime(value1, format) && isValidTime(value2, format)) {
			String[] time1 = value1.split(RegularConstants.TIME_SEPARATOR);
			String[] time2 = value2.split(RegularConstants.TIME_SEPARATOR);
			long timevalue1 = -1;
			long timevalue2 = -1;
			if (format.equals(BackOfficeConstants.TIME_HH24_MM)) {
				timevalue1 = getTimeInSeconds(Integer.parseInt(time1[0]), Integer.parseInt(time1[1]), 0);
				timevalue2 = getTimeInSeconds(Integer.parseInt(time2[0]), Integer.parseInt(time2[1]), 0);
			} else if (format.equals(BackOfficeConstants.TIME_HH24_MM_SS)) {
				timevalue1 = getTimeInSeconds(Integer.parseInt(time1[0]), Integer.parseInt(time1[1]), Integer.parseInt(time2[2]));
				timevalue2 = getTimeInSeconds(Integer.parseInt(time2[0]), Integer.parseInt(time2[1]), Integer.parseInt(time2[2]));
			}
			return (timevalue1 > timevalue2);
		}
		return false;
	}

	public final boolean isTimeLesser(String value1, String value2) {
		return isTimeLesser(value1, value2, BackOfficeConstants.TIME_HH24_MM);
	}

	public final boolean isTimeLesser(String value1, String value2, String format) {
		if (isValidTime(value1, format) && isValidTime(value2, format)) {
			String[] time1 = value1.split(RegularConstants.TIME_SEPARATOR);
			String[] time2 = value2.split(RegularConstants.TIME_SEPARATOR);
			long timevalue1 = -1;
			long timevalue2 = -1;
			if (format.equals(BackOfficeConstants.TIME_HH24_MM)) {
				timevalue1 = getTimeInSeconds(Integer.parseInt(time1[0]), Integer.parseInt(time1[1]), 0);
				timevalue2 = getTimeInSeconds(Integer.parseInt(time2[0]), Integer.parseInt(time2[1]), 0);
			} else if (format.equals(BackOfficeConstants.TIME_HH24_MM_SS)) {
				timevalue1 = getTimeInSeconds(Integer.parseInt(time1[0]), Integer.parseInt(time1[1]), Integer.parseInt(time2[2]));
				timevalue2 = getTimeInSeconds(Integer.parseInt(time2[0]), Integer.parseInt(time2[1]), Integer.parseInt(time2[2]));
			}
			return (timevalue1 < timevalue2);
		}
		return false;
	}

	public final boolean isTimeEqual(String value1, String value2) {
		return isTimeEqual(value1, value2, BackOfficeConstants.TIME_HH24_MM);
	}

	public final boolean isTimeEqual(String value1, String value2, String format) {
		if (isValidTime(value1, format) && isValidTime(value2, format)) {
			String[] time1 = value1.split(RegularConstants.TIME_SEPARATOR);
			String[] time2 = value2.split(RegularConstants.TIME_SEPARATOR);
			long timevalue1 = -1;
			long timevalue2 = -1;
			if (format.equals(BackOfficeConstants.TIME_HH24_MM)) {
				timevalue1 = getTimeInSeconds(Integer.parseInt(time1[0]), Integer.parseInt(time1[1]), 0);
				timevalue2 = getTimeInSeconds(Integer.parseInt(time2[0]), Integer.parseInt(time2[1]), 0);
			} else if (format.equals(BackOfficeConstants.TIME_HH24_MM_SS)) {
				timevalue1 = getTimeInSeconds(Integer.parseInt(time1[0]), Integer.parseInt(time1[1]), Integer.parseInt(time2[2]));
				timevalue2 = getTimeInSeconds(Integer.parseInt(time2[0]), Integer.parseInt(time2[1]), Integer.parseInt(time2[2]));
			}
			return (timevalue1 == timevalue2);
		}
		return false;
	}

	public final boolean isTimeGreaterThanEqual(String value1, String value2) {
		return isTimeGreaterThanEqual(value1, value2, BackOfficeConstants.TIME_HH24_MM);
	}

	public final boolean isTimeGreaterThanEqual(String value1, String value2, String format) {
		if (isValidTime(value1, format) && isValidTime(value2, format)) {
			String[] time1 = value1.split(RegularConstants.TIME_SEPARATOR);
			String[] time2 = value2.split(RegularConstants.TIME_SEPARATOR);
			long timevalue1 = -1;
			long timevalue2 = -1;
			if (format.equals(BackOfficeConstants.TIME_HH24_MM)) {
				timevalue1 = getTimeInSeconds(Integer.parseInt(time1[0]), Integer.parseInt(time1[1]), 0);
				timevalue2 = getTimeInSeconds(Integer.parseInt(time2[0]), Integer.parseInt(time2[1]), 0);
			} else if (format.equals(BackOfficeConstants.TIME_HH24_MM_SS)) {
				timevalue1 = getTimeInSeconds(Integer.parseInt(time1[0]), Integer.parseInt(time1[1]), Integer.parseInt(time2[2]));
				timevalue2 = getTimeInSeconds(Integer.parseInt(time2[0]), Integer.parseInt(time2[1]), Integer.parseInt(time2[2]));
			}
			return (timevalue1 >= timevalue2);
		}
		return false;
	}

	public final boolean isTimeLesserThanEqual(String value1, String value2) {
		return isTimeLesserThanEqual(value1, value2, BackOfficeConstants.TIME_HH24_MM);
	}

	public final boolean isTimeLesserThanEqual(String value1, String value2, String format) {
		if (isValidTime(value1, format) && isValidTime(value2, format)) {
			String[] time1 = value1.split(RegularConstants.TIME_SEPARATOR);
			String[] time2 = value2.split(RegularConstants.TIME_SEPARATOR);
			long timevalue1 = -1;
			long timevalue2 = -1;
			if (format.equals(BackOfficeConstants.TIME_HH24_MM)) {
				timevalue1 = getTimeInSeconds(Integer.parseInt(time1[0]), Integer.parseInt(time1[1]), 0);
				timevalue2 = getTimeInSeconds(Integer.parseInt(time2[0]), Integer.parseInt(time2[1]), 0);
			} else if (format.equals(BackOfficeConstants.TIME_HH24_MM_SS)) {
				timevalue1 = getTimeInSeconds(Integer.parseInt(time1[0]), Integer.parseInt(time1[1]), Integer.parseInt(time2[2]));
				timevalue2 = getTimeInSeconds(Integer.parseInt(time2[0]), Integer.parseInt(time2[1]), Integer.parseInt(time2[2]));
			}
			return (timevalue1 <= timevalue2);
		}
		return false;
	}

	public final long getTimeInSeconds(int hh, int mm, int ss) {
		long seconds = 0;
		seconds = hh * 60L * 60;
		seconds = seconds + (mm * 60L);
		seconds = seconds + ss;
		return seconds;
	}

	public final long getTimeValue(String timeValue) {
		String[] timeValues = timeValue.split(RegularConstants.TIME_SEPARATOR);
		long seconds = getTimeInSeconds(Integer.parseInt(timeValues[0]), Integer.parseInt(timeValues[1]), 0);
		return seconds;
	}

	/**** TIME VALIDATIONS END ****/

	/**** DATE VALIDATIONS BEGIN ****/
	public final java.util.Date isValidDate(String date) {
		java.util.Date dateInstance = null;
		try {
			if (date == null || date.trim().equals(RegularConstants.EMPTY_STRING))
				return null;
			String dateFormat = getContext().getDateFormat();
			dateInstance = BackOfficeFormatUtils.getDate(date, dateFormat);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

		return dateInstance;
	}

	public final java.util.Date isValidDateTime(String date) {
		java.util.Date dateInstance = null;
		try {
			if (date == null || date.trim().equals(RegularConstants.EMPTY_STRING))
				return null;
			String dateFormat = getContext().getDateFormat() + " " + RegularConstants.TIME_FORMAT;
			dateInstance = BackOfficeFormatUtils.getDate(date, dateFormat);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

		return dateInstance;
	}

	public final boolean isEffectiveDateValid(String effectiveDate, TBAActionType action) {
		java.util.Date dateInstance = isValidDate(effectiveDate);
		if (dateInstance == null)
			return false;
		java.util.Date currentBusinessDate = getContext().getCurrentBusinessDate();
		if (BackOfficeDateUtils.isDateGreaterThanEqual(dateInstance, currentBusinessDate))
			return true;
		return false;
	}

	public final boolean isToDateGEFromDate(String toDate, String fromDate) {
		java.util.Date dateInstance1 = isValidDate(toDate);
		java.util.Date dateInstance2 = isValidDate(fromDate);

		if (dateInstance1 == null || dateInstance2 == null)
			return false;
		if (!BackOfficeDateUtils.isDateGreaterThanEqual(dateInstance1, dateInstance2))
			return false;

		return true;

	}

	public final boolean isDateGreaterThanEqual(String value1, String value2) {
		Date date1 = isValidDate(value1);
		Date date2 = isValidDate(value2);
		return isDateGreaterThanEqual(date1, date2);
	}

	public final boolean isDateGreaterThanEqual(Date date1, Date date2) {
		return date1.getTime() >= date2.getTime();
	}

	public final boolean isDateGreaterThanCBD(String Date) {
		java.util.Date dateInstance = isValidDate(Date);
		java.util.Date currentBusinessDate = getContext().getCurrentBusinessDate();
		if (BackOfficeDateUtils.isDateGreaterThan(dateInstance, currentBusinessDate))
			return true;
		return false;
	}

	public final boolean isDateGreaterThanEqualCBD(String Date) {
		java.util.Date dateInstance = isValidDate(Date);
		java.util.Date currentBusinessDate = getContext().getCurrentBusinessDate();
		if (BackOfficeDateUtils.isDateGreaterThanEqual(dateInstance, currentBusinessDate))
			return true;
		return false;
	}

	public final boolean isDateGreaterthanEqualFromDate(String fromDate, String toDate) {
		java.util.Date todate = isValidDate(toDate);
		java.util.Date fromdate = isValidDate(fromDate);
		if (BackOfficeDateUtils.isDateGreaterThanEqual(fromdate, todate))
			return true;
		return false;
	}

	public final boolean isDateLesserEqualCBD(String dateofreg) {
		java.util.Date dateInstance = isValidDate(dateofreg);
		if (dateInstance == null)
			return false;
		java.util.Date currentBusinessDate = getContext().getCurrentBusinessDate();
		if (BackOfficeDateUtils.isDateLesserThanEqual(dateInstance, currentBusinessDate))
			return true;
		return false;
	}

	public final boolean isDateLesserCBD(String dateofreg) {
		java.util.Date dateInstance = isValidDate(dateofreg);
		if (dateInstance == null)
			return false;
		java.util.Date currentBusinessDate = getContext().getCurrentBusinessDate();
		if (BackOfficeDateUtils.isDateLesserThan(dateInstance, currentBusinessDate))
			return true;
		return false;
	}

	public final boolean isDateEqualCBD(String date) {
		java.util.Date dateInstance = isValidDate(date);
		if (dateInstance == null)
			return false;
		java.util.Date currentBusinessDate = getContext().getCurrentBusinessDate();
		if (BackOfficeDateUtils.isDateEqual(dateInstance, currentBusinessDate))
			return true;
		return false;
	}

	public final boolean isDateLesser(String date1, String date2) {
		java.util.Date dateInstance1 = isValidDate(date1);
		if (dateInstance1 == null)
			return false;
		java.util.Date dateInstance2 = isValidDate(date2);
		if (dateInstance2 == null)
			return false;
		if (BackOfficeDateUtils.isDateLesserThan(dateInstance1, dateInstance2))
			return true;
		return false;
	}

	public final boolean isValidDateUptoDate(String uptoDate, String changeDate) {
		java.util.Date dateInstance1 = isValidDate(uptoDate);
		java.util.Date dateInstance2 = getContext().getCurrentBusinessDate();
		if (dateInstance1 == null)
			return false;
		if (BackOfficeDateUtils.isDateGreaterThanEqual(dateInstance2, dateInstance1))
			return true;
		return true;
	}

	public final boolean isValidUptoDate(String date) {
		boolean valid = false;
		java.util.Date uptoDate = isValidDate(date);
		if (uptoDate == null)
			return false;
		java.util.Date currentBusinessDate = getContext().getCurrentBusinessDate();
		if (BackOfficeDateUtils.isDateGreaterThanEqual(uptoDate, currentBusinessDate))
			return true;
		return valid;
	}

	/**** DATE VALIDATIONS END ****/

	/**** NUMBER VALIDATIONS BEGIN ****/
	private static final BigInteger zeroInteger = new BigInteger("0");

	public final boolean isValidNumber(String value) {
		try {
			new BigInteger(value);
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	public final boolean isZero(String value) {
		try {
			BigInteger integerValue = new BigInteger(value);
			if (integerValue.compareTo(zeroInteger) == 0)
				return true;
		} catch (Exception e) {
			return false;
		}
		return false;
	}

	public final boolean isValidPositiveNumber(String value) {
		try {
			BigInteger integerValue = new BigInteger(value);
			if (integerValue.compareTo(zeroInteger) >= 0)
				return true;
		} catch (Exception e) {
			return false;
		}
		return false;
	}

	public final boolean isValidNegativeNumber(String value) {
		try {
			BigInteger integerValue = new BigInteger(value);
			if (integerValue.compareTo(zeroInteger) < 0)
				return true;
		} catch (Exception e) {
			return false;
		}
		return false;
	}

	public final boolean isValidDecimal(String value) {
		try {
			new BigDecimal(value);
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	public final boolean isNumberEqual(String value1, String value2) {
		BigInteger number1 = new BigInteger(value1);
		BigInteger number2 = new BigInteger(value2);
		if (number1.compareTo(number2) == 0)
			return true;
		return false;
	}

	public final boolean isNumberLesser(String value1, String value2) {
		BigInteger number1 = new BigInteger(value1);
		BigInteger number2 = new BigInteger(value2);
		if (number1.compareTo(number2) < 0)
			return true;
		return false;
	}

	public final boolean isNumberLesserThanEqual(String value1, String value2) {
		BigInteger number1 = new BigInteger(value1);
		BigInteger number2 = new BigInteger(value2);
		if (number1.compareTo(number2) <= 0)
			return true;
		return false;
	}

	public final boolean isNumberGreater(String value1, String value2) {
		BigInteger number1 = new BigInteger(value1);
		BigInteger number2 = new BigInteger(value2);
		if (number1.compareTo(number2) > 0)
			return true;
		return false;
	}

	public final boolean isNumberGreaterThanEqual(String value1, String value2) {
		BigInteger number1 = new BigInteger(value1);
		BigInteger number2 = new BigInteger(value2);
		if (number1.compareTo(number2) >= 0)
			return true;
		return false;
	}

	/**** NUMBER VALIDATIONS END ****/

	/**** AMOUNT VALIDATIONS BEGIN ****/
	private static final BigDecimal zeroDecimal = new BigDecimal("0");
	private static final BigDecimal maxAmount = new BigDecimal("999999999999999999999999999.999");
	private static final BigDecimal smallAmount = new BigDecimal("999999999999999.999");
	private static final BigDecimal bigAmount = new BigDecimal("999999999999999999999999999.999");

	public final boolean isValidAmount(String value) {
		try {
			new BigDecimal(value);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public final boolean isMaxLimitExceeded(String value) {
		try {
			BigDecimal amountValue = new BigDecimal(value);
			if (amountValue.precision() > 30 || amountValue.scale() > 3 || (amountValue.precision() - amountValue.scale()) > 27) {
				return true;
			}
			if (amountValue.compareTo(maxAmount) > 0) {
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public final boolean isSmallAmount(String value, int scale) {
		try {
			BigDecimal amountValue = new BigDecimal(value);
			amountValue = amountValue.stripTrailingZeros();
			if (amountValue.precision() > 18 || amountValue.scale() > scale || (amountValue.precision() - amountValue.scale()) > 15) {
				return false;
			}
			if (amountValue.compareTo(smallAmount) > 0) {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}

	public final boolean isThreeDigitTwoDecimal(String value, int scale) {
		try {
			BigDecimal amountValue = new BigDecimal(value);
			if (amountValue.precision() > 5 || amountValue.scale() > 2 || (amountValue.precision() - amountValue.scale()) > 3) {
				return false;
			}
			if (amountValue.compareTo(smallAmount) > 0) {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}

	public final boolean isBigAmount(String value, int scale) {
		try {
			BigDecimal amountValue = new BigDecimal(value);
			amountValue = amountValue.stripTrailingZeros();
			if (amountValue.precision() > 30 || amountValue.scale() > scale || (amountValue.precision() - amountValue.scale()) > 27) {
				return false;
			}
			if (amountValue.compareTo(bigAmount) > 0) {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}

	public final boolean isZeroAmount(String value) {
		try {
			BigDecimal integerValue = new BigDecimal(value);
			if (integerValue.compareTo(zeroDecimal) == 0)
				return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public final boolean isValidPositiveAmount(String value) {
		try {
			BigDecimal integerValue = new BigDecimal(value);
			if (integerValue.compareTo(zeroDecimal) > 0)
				return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public final boolean isValidNegativeAmount(String value) {
		try {
			BigDecimal integerValue = new BigDecimal(value);
			if (integerValue.compareTo(zeroDecimal) < 0)
				return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public final boolean isAmountEqual(String value1, String value2) {
		BigDecimal amount1 = new BigDecimal(value1);
		BigDecimal amount2 = new BigDecimal(value2);
		if (amount1.compareTo(amount2) == 0)
			return true;
		return false;
	}

	public final boolean isAmountLesser(String value1, String value2) {
		BigDecimal amount1 = new BigDecimal(value1);
		BigDecimal amount2 = new BigDecimal(value2);
		if (amount1.compareTo(amount2) < 0)
			return true;
		return false;
	}

	public final boolean isAmountLesserThanEqual(String value1, String value2) {
		BigDecimal amount1 = new BigDecimal(value1);
		BigDecimal amount2 = new BigDecimal(value2);
		if (amount1.compareTo(amount2) <= 0)
			return true;
		return false;
	}

	public final boolean isAmountGreater(String value1, String value2) {
		BigDecimal amount1 = new BigDecimal(value1);
		BigDecimal amount2 = new BigDecimal(value2);
		if (amount1.compareTo(amount2) > 0)
			return true;
		return false;
	}

	public final boolean isAmountGreaterThanEqual(String value1, String value2) {
		BigDecimal amount1 = new BigDecimal(value1);
		BigDecimal amount2 = new BigDecimal(value2);
		if (amount1.compareTo(amount2) >= 0)
			return true;
		return false;
	}

	public final DTObject validateCurrencySmallAmount(DTObject input) {
		DTObject result = new DTObject();
		String currency = input.get("CURRENCY_CODE");
		// int units =
		// Integer.parseInt(input.get(ProgramConstants.CURRENCY_UNITS));
		String smallAmount = input.get(ProgramConstants.AMOUNT);
		MasterValidator validator = new MasterValidator();
		try {
			if (isEmpty(currency)) {
				result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_CURRENCYCODE);
				return result;
			}
			if (!isValidAmount(smallAmount)) {
				result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_SMALL_AMOUNT);
				return result;
			}
			input.set(ContentManager.FETCH_COLUMNS, "SUB_CCY_UNITS_IN_CCY");
			input.set("CCY_CODE", input.get("CURRENCY_CODE"));
			input.set(ContentManager.USER_ACTION, USAGE);
			int units = Integer.parseInt(validator.validateCurrency(input).get("SUB_CCY_UNITS_IN_CCY"));
			if (!isSmallAmount(smallAmount, units)) {
				result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_SMALL_AMOUNT);
				return result;
			}

		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return result;
	}

	public final DTObject validateCurrencyBigAmount(DTObject input) {
		DTObject result = new DTObject();
		String currency = input.get("CURRENCY_CODE");
		// int units =
		// Integer.parseInt(input.get(ProgramConstants.CURRENCY_UNITS));
		String bigAmount = input.get(ProgramConstants.AMOUNT);
		MasterValidator validator = new MasterValidator();
		try {
			if (isEmpty(currency)) {
				result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_AMOUNT);
				return result;
			}
			if (!isValidAmount(bigAmount)) {
				result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_BIG_AMOUNT);
				return result;
			}
			input.set(ContentManager.FETCH_COLUMNS, "SUB_CCY_UNITS_IN_CCY");
			input.set("CCY_CODE", input.get("CURRENCY_CODE"));
			input.set(ContentManager.USER_ACTION, USAGE);
			int units = Integer.parseInt(validator.validateCurrency(input).get("SUB_CCY_UNITS_IN_CCY"));
			if (!isBigAmount(bigAmount, units)) {
				result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_BIG_AMOUNT);
				return result;
			}

		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return result;

	}

	/**** AMOUNT VALIDATIONS END ****/

	public final boolean isValidDescription(String description) {
		if (description == null || description.length() == 0 || description.length() > DESCRIPTION_LENGTH)
			return false;
		return true;
	}

	public final boolean isValidDescription(String description, int length) {
		if (description == null || description.length() == 0 || description.length() > length)
			return false;
		return true;
	}

	public final boolean isValidConciseDescription(String conciseDescription) {
		if (conciseDescription == null || conciseDescription.length() == 0 || conciseDescription.length() > CONCISE_DESCRIPTION_LENGTH)
			return false;
		return true;
	}

	public final boolean isValidExternalDescription(String externalDescription) {
		if (externalDescription == null || externalDescription.length() == 0 || externalDescription.length() > EXTERNAL_DESCRIPTION_LENGTH)
			return false;
		return true;
	}

	public final boolean isValidWithDrawSlip(String withDrawSlip) {
		if (withDrawSlip == null || withDrawSlip.length() == 0 || withDrawSlip.length() > WITHDRAWAL_SLIP_LENGTH)
			return false;
		return true;
	}

	public final boolean isValidDesignation(String designation) {
		if (designation.length() > DESIGNATION_LENGTH)
			return false;
		return true;
	}

	public final boolean isValidPath(String filePath) {
		if (filePath == null || filePath.length() == 0 || filePath.length() > PATH_LENGTH)
			return false;
		return true;
	}

	public final boolean isValidEmail(String value) {
		// String regex =
		// "^([a-zA-Z0-9_\\.\\-\\!\\@\\#\\$\\%\\^\\&\\*\\(\\)\\+\\=\\{\\[\\}\\]\\;\\:\\?\\/\\,\\<\\>\\~\\*])+\\@(([a-zA-Z0-9_\\.\\-\\!\\@\\#\\$\\%\\^\\&\\*\\(\\)\\+\\=\\{\\[\\}\\]\\;\\:\\?\\/\\,\\<\\>\\~\\*])+\\.)+([a-zA-Z0-9_\\.\\-\\!\\@\\#\\$\\%\\^\\&\\*\\(\\)\\+\\=\\{\\[\\}\\]\\;\\:\\?\\/\\,\\<\\>\\~\\*]{2,4})+$";
		String regex = "^\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$";
		if (value.length() > EMAIL_LENGTH)
			return false;
		else if (value.matches(regex))
			return true;
		return false;
	}

	public final boolean isValidAddress(String address) {
		if (address == null || address.length() == 0 || address.length() > ADDRESS_LENGTH)
			return false;
		return true;
	}

	public final boolean isValidAddressLine(String address) {
		if (address == null || address.length() == 0 || address.length() > ADDRESS_LINE)
			return false;
		return true;
	}

	public final boolean isValidIFSCCode(String ifscCode) {
		if (ifscCode == null || ifscCode.length() == 0 || ifscCode.length() > IFSC_LENGTH)
			return false;
		return true;
	}

	public final boolean isValidKey(String key) {
		if (key.length() > KEY_LENGTH)
			return false;
		return true;
	}

	public final boolean isValidName(String name) {
		if (name.length() > NAME_LENGTH)
			return false;
		return true;
	}

	public final boolean isValidProgramID(String pgmid) {
		if (pgmid.length() > PROGRAM_ID_LENGTH)
			return false;
		String regex = "(((\\s)+)|((\\|)+)(.)*)|((.)*(\\s)+)|((.)*(\\|)+)|((.)*(\\s)+(.)*)|((.)*(\\|)+(.)*)";
		if (pgmid.matches(regex))
			return false;
		return true;
	}

	public final boolean isValidPhoneNumber(String value) {
		return value.matches("[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]");
	}

	public final boolean isValidPort(String code) {
		try {
			int i = Integer.parseInt(code);
			if (i >= 1 && i <= 65536)
				return true;
		} catch (Exception e) {
		}
		return false;
	}

	public final boolean isValidRemarks(String remarks) {
		if (remarks.length() > REMARKS_LENGTH)
			return false;
		return true;
	}

	public final boolean isValidMoreRemarks(String remarks) {
		if (remarks.length() > MOREREMARKS_LENGTH)
			return false;
		return true;
	}

	public final boolean isValidChequeNumber(String code) {
		if (code.length() > CHEQUE_LENGTH)
			return false;
		String regex = "^([A-Z0-9_])+$";
		if (code.matches(regex))
			return true;
		return false;
	}

	public final boolean isValidReceiptNumber(String code) {
		if (code.length() > RECEIPT_LENGTH)
			return false;
		String regex = "^([A-Z0-9_])+$";
		if (code.matches(regex))
			return true;
		return false;
	}

	public final boolean isValidUrl(String url) {
		if (url == null || url.length() == 0 || url.length() > URL_LENGTH)
			return false;
		return true;
	}

	public final boolean isValidHTTPUrl(String url) {
		if (url == null) {
			return false;
		}
		// String urlPattern =
		// "^http(s{0,1})://[a-zA-Z0-9_/\\-\\.]+\\.([A-Za-z/]{2,5})[a-zA-Z0-9_/\\&\\?\\=\\-\\.\\~\\%]*";
		// return url.matches(urlPattern);
		return true;
	}

	public final boolean isValidLDAPUrl(String url) {
		if (url == null) {
			return false;
		}
		// String urlPattern =
		// "^http(s{0,1})://[a-zA-Z0-9_/\\-\\.]+\\.([A-Za-z/]{2,5})[a-zA-Z0-9_/\\&\\?\\=\\-\\.\\~\\%]*";
		// return url.matches(urlPattern);
		return true;
	}

	public final boolean isValidUserID(String userid) {
		if (userid == null || userid.length() == 0 || userid.length() > USER_ID_LENGTH)
			return false;
		String regex = "^([A-Z0-9_@\\.\\-])+$";
		if (userid.matches(regex))
			return true;
		return false;
	}

	public final boolean isValidValue(String value) {
		if (value == null || value.length() == 0 || value.length() > VALUE_LENGTH)
			return false;
		return true;
	}

	public final boolean isValidServerAddress(String value) {
		if (value.length() > SERVER_ADDRESS_LENGTH) {
			return false;
		}
		return true;
	}

	public final boolean isValidCode(String code) {
		if (code.length() > CODE_LENGTH)
			return false;
		String regex = "^([A-Z0-9_])+$";
		if (code.matches(regex))
			return true;
		return false;
	}

	public final boolean isValidCode(String code, int length) {
		if (code.length() > length)
			return false;
		String regex = "^([A-Z0-9_])+$";
		if (code.matches(regex))
			return true;
		return false;
	}

	public final boolean isValidBeneficiary(String code) {
		if (code.length() > BENFID_LENGTH)
			return false;
		String regex = "^([A-Z0-9_@\\.\\-])+$";
		if (code.matches(regex))
			return true;
		return false;
	}

	public final boolean isValidService(String code) {
		if (code.length() > SERVICE_LENGTH)
			return false;
		String regex = "^([A-Z0-9_@\\.\\-])+$";
		if (code.matches(regex))
			return true;
		return false;
	}

	public final boolean isValidCredential(String value) {
		if (value.length() > CREDENTIAL_LENGTH)
			return false;
		return true;
	}

	public final boolean isValidCronExp(String cronExp) {
		if (cronExp.length() > CRON_LENGTH)
			return false;
		return CronExpression.isValidExpression(cronExp);
	}

	public final boolean isValidCustomerCode(String code) {
		if (!isValidNumber(code)) {
			return false;
		}
		if (code.length() > CUSTOMERCODE_LENGTH)
			return false;
		String regex = "^([0-9])+$";
		if (code.matches(regex))
			return true;
		return false;
	}

	public final boolean isValidPercentage(String value) {
		String regex = "^\\d{1,3}(?:\\.\\d{0,2})?$";
		if (value.matches(regex)) {
			BigDecimal percentage = new BigDecimal(value);
			if ((percentage.compareTo(new BigDecimal(0)) == -1) || (percentage.compareTo(new BigDecimal(100)) == 1)) {
				return false;
			}
			return true;
		}
		return false;
	}

	public final boolean isValidIPAddress(String ipAddress) {
		String[] parts = ipAddress.split("\\.");
		if (parts.length != 4) {
			return false;
		}
		for (String s : parts) {
			int i = Integer.parseInt(s);
			if ((s.length() > 3) || (i < 0) || (i > 255)) {
				return false;
			}
		}
		return true;
	}

	public final boolean isValidTranID(String tranid) {
		if (tranid == null || tranid.length() == 0 || tranid.length() > TRAN_ID_LENGTH)
			return false;
		String regex = "^([A-Z0-9_@\\.\\-])+$";
		if (tranid.matches(regex))
			return true;
		return false;
	}

	public final boolean isValidQuestion(String question) {
		if (question == null || question.length() == 0 || question.length() > INVALID_QUESTION_LENGTH)
			return false;
		return true;
	}

	public final boolean isValidAnswer(String answer) {
		if (answer == null || answer.length() == 0 || answer.length() > INVALID_ANSWER_LENGTH)
			return false;
		return true;
	}

	public final boolean isValidRefNumber(String refNumber) {
		if (refNumber == null || refNumber.length() == 0 || refNumber.length() > REFERENCE_NO_LENGTH)
			return false;
		return true;
	}

	public boolean isValidRefreshTime(int time) {
		if (time < 1 || time > 43200)
			return false;
		return true;
	}

	public final boolean isValidAccountNumber(String acntNum) {
		if (acntNum == null || acntNum.length() == 0 || acntNum.length() > ACCOUNT_NUMBER_LENGTH)
			return false;
		String regex = "^([A-Za-z0-9])+$";
		if (!acntNum.matches(regex))
			return false;
		return true;
	}

	public final boolean isValidBeneficiaryAccountNumber(String acntNum) {
		if (acntNum == null || acntNum.length() == 0 || acntNum.length() > 25)
			return false;
		String regex = "^([A-Za-z0-9])+$";
		if (!acntNum.matches(regex))
			return false;
		return true;
	}

	public final boolean isValidReportCode(String code) {
		if (code.length() > RPTCODE_LENGTH)
			return false;
		String regex = "^([A-Z0-9_@\\.\\-])+$";
		if (code.matches(regex))
			return true;
		return false;
	}

	public final boolean isValidPrefix(String code) {
		if (code.length() > PREFIX_LENGTH)
			return false;
		String regex = "^([A-Z0-9])+$";
		if (code.matches(regex))
			return true;
		return false;
	}

	public final boolean isSessionForInternalAdministrator() {
		String roleType = getContext().getRoleType();
		if (roleType.equals(CM_LOVREC.COMMON_ROLES_1) || roleType.equals(CM_LOVREC.COMMON_ROLES_2)) {
			return true;
		}
		return false;
	}

	public final boolean isSessionForInternalOperations() {
		String roleType = getContext().getRoleType();
		if (roleType.equals(CM_LOVREC.COMMON_ROLES_3) || roleType.equals(CM_LOVREC.COMMON_ROLES_4)) {
			return true;
		}
		return false;
	}

	public final boolean isSessionForCustomerAdministrator() {
		String roleType = getContext().getRoleType();
		if (roleType.equals(CM_LOVREC.COMMON_ROLES_5) || roleType.equals(CM_LOVREC.COMMON_ROLES_6)) {
			return true;
		}
		return false;
	}

	public final boolean isValidPrefixCode(String prefixCode) {
		if (prefixCode == null || prefixCode.length() == 0 || prefixCode.length() > PREFIX_CODE_LENGTH)
			return false;
		String regex = "^([A-Z0-9_@\\.\\-])+$";
		if (prefixCode.matches(regex))
			return true;
		return false;
	}

	public final boolean isvalidExtensionlist(String value) {
		int l1 = value.length() - 1;
		if (value.charAt(l1) == ',') {
			return false;
		}
		String[] ext = value.split(",");
		for (int i = 0; i < ext.length; i++) {
			if (!isValidExtension(ext[i])) {
				return false;
			}
		}
		return true;
	}

	public final boolean isValidExtension(String value) {
		String regex = "^([a-zA-Z0-9])+$";
		if (value.length() > EXTENSION_LENGTH)
			return false;
		else if (value.matches(regex))
			return true;
		return false;
	}

	public final boolean isObjectDuplicated(String objectClass, String objectAttribute, String objectValue, String objectID) {
		boolean available = false;
		DBUtil util = getDbContext().createUtilInstance();
		try {
			String sqlQuery = "SELECT 1 FROM DUPCHKGW WHERE ENTITY_CODE=? AND OBJECT_CLASS = ? AND OBJECT_ATTR = ?  AND OBJECT_VALUE = ? AND OBJECT_ID <> ?";
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, getContext().getEntityCode());
			util.setString(2, objectClass);
			util.setString(3, objectAttribute);
			util.setString(4, objectValue);
			util.setString(5, objectID);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				available = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			util.reset();
		}
		return available;
	}

	public final boolean isObjectDuplicated(String objectClass, String objectAttribute, String objectValue) {
		boolean available = false;
		DBUtil util = getDbContext().createUtilInstance();
		try {
			String sqlQuery = "SELECT 1 FROM DUPCHKGW WHERE ENTITY_CODE=? AND OBJECT_CLASS = ? AND OBJECT_ATTR = ?  AND OBJECT_VALUE = ?";
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, getContext().getEntityCode());
			util.setString(2, objectClass);
			util.setString(3, objectAttribute);
			util.setString(4, objectValue);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				available = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			util.reset();
		}
		return available;
	}

	/*
	 * public final DTObject validateOrganization(DTObject input) { DTObject
	 * result = new DTObject(); String sqlQuery = null; String _sqlQuery = null;
	 * String code = input.get(ContentManager.CODE);
	 * result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
	 * DBUtil util = getDbContext().createUtilInstance(); try { if (code != null
	 * && !code.equals(RegularConstants.EMPTY_STRING)) { sqlQuery =
	 * "SELECT SI_ORG_ID,SI_ORG_NAME FROM SYSINSTALL WHERE SI_ORG_ID = ?";
	 * util.reset(); util.setSql(sqlQuery); util.setString(1, code); ResultSet
	 * rset = util.executeQuery(); if (rset.next()) {
	 * result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
	 * result.set("ORG_ID", rset.getString(1)); result.set("ORG_DESC",
	 * rset.getString(2)); result.set("MULTIPLE_AVAIL",
	 * RegularConstants.COLUMN_DISABLE); } return result; } else { sqlQuery =
	 * "SELECT COUNT(1) FROM SYSINSTALL"; } util.reset(); util.setSql(sqlQuery);
	 * ResultSet rset = util.executeQuery(); if (rset.next()) { if
	 * (rset.getInt(1) > 1) { result.set(ContentManager.RESULT,
	 * ContentManager.DATA_AVAILABLE); result.set("MULTIPLE_AVAIL",
	 * RegularConstants.COLUMN_ENABLE); } else if (rset.getInt(1) == 1) {
	 * result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
	 * result.set("MULTIPLE_AVAIL", RegularConstants.COLUMN_DISABLE); _sqlQuery
	 * = "SELECT SI_ORG_ID,SI_ORG_NAME FROM SYSINSTALL"; util.reset();
	 * util.setSql(_sqlQuery); rset = util.executeQuery(); if (rset.next()) {
	 * result.set("ORG_ID", rset.getString(1)); result.set("ORG_DESC",
	 * rset.getString(2)); } } } else { result.set(ContentManager.RESULT,
	 * ContentManager.DATA_UNAVAILABLE); } } catch (Exception e) {
	 * e.printStackTrace(); result.set(ContentManager.ERROR,
	 * BackOfficeErrorCodes.UNSPECIFIED_ERROR); } finally { util.reset(); }
	 * return result; }
	 */

	/*
	 * public final DTObject validateBankBranch(DTObject input) { DTObject
	 * result = new DTObject(); String branchCode =
	 * input.get(ContentManager.CODE); String bankCode = input.get("BANK_CODE");
	 * String sqlQuery = null; result.set(ContentManager.RESULT,
	 * ContentManager.DATA_UNAVAILABLE); DBUtil util =
	 * getDbContext().createUtilInstance(); try { sqlQuery =
	 * "SELECT * FROM BRN WHERE BRN_BANK_CODE = ? AND BRN_BRN_CODE = ?";
	 * util.reset(); util.setSql(sqlQuery); util.setString(1, bankCode);
	 * util.setString(2, branchCode); ResultSet rset = util.executeQuery(); if
	 * (rset.next()) { result.set(ContentManager.RESULT,
	 * ContentManager.DATA_AVAILABLE); decodeRow(rset, result); } } catch
	 * (Exception e) { e.printStackTrace(); result.set(ContentManager.ERROR,
	 * BackOfficeErrorCodes.UNSPECIFIED_ERROR); } finally { util.reset(); }
	 * return result; }
	 */

	public DTObject getSystemCountryCode() {
		DTObject result = new DTObject();
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = getDbContext().createUtilInstance();
		try {
			String sqlQuery = "SELECT SC_DEF_CNTRY_CODE,CNTRY_DESCN FROM SYSINSTALL, CMNMCOUNTRY WHERE SI_ORG_ID=? AND SC_DEF_CNTRY_CODE=CNTRY_CODE";
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, getContext().getEntityCode());
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				result.set("SC_DEF_CNTRY_CODE", rset.getString(1));
				result.set("CNTRY_DESCN", rset.getString(2));
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return result;
	}

	public final DTObject getAddressDetails(DTObject input) {
		String value = input.get("INV_NUMBER");
		String sqlQuery = null;
		input.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = getDbContext().createUtilInstance();
		String addrStatus = "A";
		try {
			sqlQuery = "SELECT * FROM ADDRINV WHERE AI_ORG_ID = ? AND AI_ADDR_INV_NO=?";
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, getContext().getEntityCode());
			util.setString(2, value);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				input.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				decodeRow(rset, input);
				sqlQuery = "SELECT AKA_STATUS FROM ADDRESSSTATUS WHERE ORG_ID = ? AND ADDR_NO=?";
				util.reset();
				util.setSql(sqlQuery);
				util.setString(1, getContext().getEntityCode());
				util.setString(2, value);
				ResultSet _rset = util.executeQuery();
				if (_rset.next()) {
					if (_rset.getString(1).equals("1"))
						addrStatus = "D";
				}
				input.set("ADDR_STATUS", addrStatus);
			} else {
				input.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			input.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return input;
	}

	public final String getBaseCurrency() {
		String currency = RegularConstants.EMPTY_STRING;
		String sqlQuery = null;
		DBUtil util = getDbContext().createUtilInstance();
		try {
			sqlQuery = "SELECT BASE_CURR_CODE FROM INSTALL WHERE ENTITY_CODE=?";
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, getContext().getEntityCode());
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				currency = rset.getString(1);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			util.reset();
		}
		return currency;
	}

	public boolean getPickupAccess(DTObject inputDTO) {
		DBUtil util = getDbContext().createUtilInstance();
		try {
			String baseKey = RegularConstants.EMPTY_STRING;
			String sourceKey = RegularConstants.EMPTY_STRING;
			String sourceTable = RegularConstants.EMPTY_STRING;
			String pickupClass = RegularConstants.EMPTY_STRING;
			int expTime = 0;

			String sql = "SELECT MPGM_TABLE_NAME,MPGM_PCK_CLS FROM MPGM WHERE MPGM_ID = ?";
			util.reset();
			util.setMode(DBUtil.PREPARED);
			util.setSql(sql);
			util.setString(1, inputDTO.get("PROGRAM_ID"));
			ResultSet rs = util.executeQuery();
			if (rs.next()) {
				sourceTable = rs.getString(1);
				pickupClass = rs.getString(2);
			} else {
				baseKey = "";
			}
			if (!baseKey.equals(RegularConstants.EMPTY_STRING)) {
				sql = "SELECT BASE_KEY FROM FWRKPICKUPCFG WHERE PROGRAM_ID = ?";
				util.reset();
				util.setMode(DBUtil.PREPARED);
				util.setSql(sql);
				util.setString(1, inputDTO.get("PROGRAM_ID"));
				rs = util.executeQuery();
				if (rs.next()) {
					baseKey = rs.getString(1);
				}
			}

			sql = "SELECT PKCLS_TIME_INTRVL FROM FWRKPICKUPCLASS WHERE PKCLS_CODE=?";
			util.reset();
			util.setMode(DBUtil.PREPARED);
			util.setSql(sql);
			util.setString(1, pickupClass);
			rs = util.executeQuery();
			if (rs.next()) {
				expTime = rs.getInt(1);
			}
			if (!baseKey.equals(RegularConstants.EMPTY_STRING)) {
				sourceKey = inputDTO.get("SOURCE_KEY");
			} else {
				String[] sourceKeys = inputDTO.get("SOURCE_KEY").split("\\|");
				if (!baseKey.equals(RegularConstants.EMPTY_STRING)) {
					String[] baseKeys = baseKey.split("\\|");
					for (int i = 0; i < baseKeys.length; i++) {
						if (sourceKey.equals(RegularConstants.EMPTY_STRING)) {
							sourceKey = sourceKeys[Integer.parseInt(baseKeys[i])];
						} else {
							sourceKey = sourceKey + "|" + sourceKeys[Integer.parseInt(baseKeys[i])];
						}
					}
				}
			}
			sql = "INSERT INTO FWRKPICKUPPE (PKU_SRC_TABLE_NAME,PKU_SRC_KEY,PKU_EXP_TIME) VALUES (?,?,?)";
			util.reset();
			util.setMode(DBUtil.PREPARED);
			util.setSql(sql);
			util.setString(1, sourceTable);
			util.setString(2, sourceKey);
			util.setInt(3, expTime);
			int counter = util.executeUpdate();
			if (counter == 1) {
				sql = "INSERT INTO FWRKPICKUPLOG (PKUL_SRC_TABLE_NAME,PKUL_SRC_KEY,PKUL_DATE_TIME,PKUL_BY_PGM,PKUL_BY_USER_ID) VALUES (?,?,FN_GETCDT(?),?,?)";
				util.reset();
				util.setMode(DBUtil.PREPARED);
				util.setSql(sql);
				util.setString(1, sourceTable);
				util.setString(2, sourceKey);
				util.setString(3, getContext().getEntityCode());
				util.setString(4, inputDTO.get("PICKUP_PGM"));
				util.setString(5, getContext().getUserID());
				counter = util.executeUpdate();
			}
			if (counter != 0) {
				return true;
			}
			return false;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			util.reset();
		}
		return false;
	}

	public final DTObject validateCode(DTObject input) {
		DTObject result = new DTObject();
		String value = input.get(ContentManager.CODE);
		String table = input.get(ContentManager.TABLE);
		String fetch = input.get(ContentManager.FETCH);
		String sqlQuery = null;
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = getDbContext().createUtilInstance();
		String action = input.get(ContentManager.USER_ACTION);
		boolean usageAllowed = false;
		try {
			if (USAGE.equals(action)) {
				usageAllowed = getProgramUsageAllowed(input);
			}
			if (fetch != null && fetch.equals(ContentManager.FETCH_ALL)) {
				sqlQuery = "SELECT * FROM " + table + " WHERE ENTITY_CODE=? AND CODE=?";
			} else {
				sqlQuery = "SELECT CODE,DESCRIPTION,ENABLED,TBA_KEY FROM " + table + " WHERE ENTITY_CODE=? AND CODE=?";
			}
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, getContext().getEntityCode());
			util.setString(2, value);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				decodeRow(rset, result);
				String enabled = rset.getString("ENABLED");
				if (enabled != null && enabled.equals(RegularConstants.COLUMN_DISABLE)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.CODE_CLOSED);
				}
				if (usageAllowed) {
					if (USAGE.equals(action)) {
						if (!isEmpty(rset.getString("TBA_KEY"))) {
							result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_USAGE_NOT_ALLOWED);
						}
					}
				}

			} else {
				result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return result;
	}

	public final DTObject validateOrganization(DTObject input) {
		DTObject result = new DTObject();
		String sqlQuery = null;
		String _sqlQuery = null;
		String code = input.get(ContentManager.CODE);
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = getDbContext().createUtilInstance();
		try {
			if (code != null && !code.equals(RegularConstants.EMPTY_STRING)) {
				sqlQuery = "SELECT SI_ORG_ID,SI_ORG_NAME FROM SYSINSTALL WHERE SI_ORG_ID = ?";
				util.reset();
				util.setSql(sqlQuery);
				util.setString(1, code);
				ResultSet rset = util.executeQuery();
				if (rset.next()) {
					result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
					result.set("ORG_ID", rset.getString(1));
					result.set("ORG_DESC", rset.getString(2));
					result.set("MULTIPLE_AVAIL", RegularConstants.COLUMN_DISABLE);
				}
				return result;
			} else {
				sqlQuery = "SELECT COUNT(1) FROM SYSINSTALL";
			}
			util.reset();
			util.setSql(sqlQuery);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				if (rset.getInt(1) > 1) {
					result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
					result.set("MULTIPLE_AVAIL", RegularConstants.COLUMN_ENABLE);
				} else if (rset.getInt(1) == 1) {
					result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
					result.set("MULTIPLE_AVAIL", RegularConstants.COLUMN_DISABLE);
					_sqlQuery = "SELECT SI_ORG_ID,SI_ORG_NAME FROM SYSINSTALL";
					util.reset();
					util.setSql(_sqlQuery);
					rset = util.executeQuery();
					if (rset.next()) {
						result.set("ORG_ID", rset.getString(1));
						result.set("ORG_DESC", rset.getString(2));
					}
				}
			} else {
				result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return result;
	}

	public final DTObject validateEntityWiseCodes(DTObject input) {
		DTObject result = new DTObject();
		String value = input.get(ContentManager.CODE);
		String entityCode = input.get(ContentManager.ENTITY_CODE);
		String code = input.get(ContentManager.COLUMN);
		String table = input.get(ContentManager.TABLE);
		String fetch = input.get(ContentManager.FETCH);
		String fetchColumns = input.get(ContentManager.FETCH_COLUMNS);
		String sqlQuery = null;
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = getDbContext().createUtilInstance();
		String action = input.get(ContentManager.USER_ACTION);
		boolean usageAllowed = false;
		try {
			if (action.equals(USAGE)) {
				usageAllowed = getProgramUsageAllowed(input);
			}
			if (fetch != null && fetch.equals(ContentManager.FETCH_ALL)) {
				sqlQuery = "SELECT * FROM " + table + " WHERE " + entityCode + " = ? AND " + code + " = ?";
			} else {
				sqlQuery = "SELECT " + fetchColumns + " ,ENABLED,TBA_KEY FROM " + table + " WHERE " + entityCode + " = ? AND " + code + " = ?"; // jainudeen
			}
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, getContext().getEntityCode());
			util.setString(2, value);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				decodeRow(rset, result);
				String enabled = rset.getString("ENABLED");
				if (enabled != null && enabled.equals(RegularConstants.COLUMN_DISABLE)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.CODE_CLOSED);
				}
				if (usageAllowed) {
					if (action.equals(USAGE)) {
						if (rset.getString("TBA_KEY") != null) {
							result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_USAGE_NOT_ALLOWED);
						}
					}
				}

			} else {
				result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return result;
	}

	public final DTObject validateUKCode(DTObject input) {
		DTObject result = new DTObject();
		String table = input.get(ContentManager.TABLE);
		String fetch = input.get(ContentManager.FETCH);
		String[] arguments = (String[]) input.getObject(ContentManager.CODE);
		String[] columns = (String[]) input.getObject(ContentManager.COLUMN);
		StringBuffer sqlQuery = new StringBuffer();
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = getDbContext().createUtilInstance();
		String action = input.get(ContentManager.USER_ACTION);
		String fetchColumns = input.get(ContentManager.FETCH_COLUMNS);
		boolean usageAllowed = false;
		TableInfo tableInfo = null;
		try {
			if (action.equals(USAGE)) {
				usageAllowed = getProgramUsageAllowed(input);
			}

			int j = 0;
			if (fetch != null && fetch.equals(ContentManager.FETCH_ALL)) {
				sqlQuery.append("SELECT * FROM ").append(table).append(" WHERE ");

			} else {
				sqlQuery.append("SELECT " + fetchColumns + " ,ENABLED,TBA_KEY FROM ").append(table).append(" WHERE ");
			}
			for (String fieldName : columns) {
				if (j < columns.length - 1) {
					sqlQuery.append(fieldName).append(" = ?  AND  ");
				} else {
					sqlQuery.append(fieldName).append(" = ? ");
				}
				++j;
			}
			sqlQuery.trimToSize();
			util.reset();
			util.setSql(sqlQuery.toString());
			int k = 1;
			DBInfo info = new DBInfo();
			tableInfo = info.getTableInfo(table);
			Map<String, BindParameterType> columnType = tableInfo.getColumnInfo().getColumnMapping();
			for (int i = 0; i < columns.length; i++) {
				BindParameterType fieldType = columnType.get(columns[i]);
				switch (fieldType) {
				case LONG:
					util.setLong(k, Long.parseLong(arguments[i]));
					break;
				case DATE:
					String value = arguments[i];
					util.setDate(k, new java.sql.Date(BackOfficeFormatUtils.getDate(value, context.getDateFormat()).getTime()));
					break;
				case INTEGER:
					util.setInt(k, Integer.parseInt(arguments[i]));
					break;
				case BIGDECIMAL:
					util.setBigDecimal(k, new BigDecimal(columns[i]));
					break;
				case VARCHAR:
				default:
					util.setString(k, arguments[i]);
					break;
				}
				k++;
			}
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				decodeRow(rset, result);
				String enabled = rset.getString("ENABLED");
				if (enabled != null && enabled.equals(RegularConstants.COLUMN_DISABLE)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.CODE_CLOSED);
				}
				if (action.equals(USAGE)) {
					if (rset.getString("TBA_KEY") != null) {
						if (usageAllowed) {
							result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_USAGE_NOT_ALLOWED);
						}
					}
				}
			} else {
				result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
			}

		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return result;
	}

	public final DTObject validateGeneralOTN(DTObject input) {
		DTObject result = new DTObject();
		String entityCode = input.get(ContentManager.ENTITY_CODE);
		String tableName = input.get(ContentManager.TABLE);
		String key = input.get(ContentManager.KEY);
		String sqlQuery = null;
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = getDbContext().createUtilInstance();
		try {
			sqlQuery = "SELECT NEW_KEY_STRING_VALUE ,NEW_KEY_NUMERIC_VALUE  FROM GENERALOTN WHERE ENTITY_CODE =? AND TABLE_NAME=? AND OLD_KEY_VALUE=?";
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, entityCode);
			util.setString(2, tableName);
			util.setString(3, key);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				decodeRow(rset, result);
			} else {
				result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return result;
	}

	public final DTObject validateGenericCode(DTObject input) {
		DTObject result = new DTObject();
		String value = input.get(ContentManager.CODE);
		String column = input.get(ContentManager.COLUMN);
		String table = input.get(ContentManager.TABLE);
		String fetch = input.get(ContentManager.FETCH);
		String sqlQuery = null;
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = getDbContext().createUtilInstance();
		String action = input.get(ContentManager.USER_ACTION);
		String fetchColumns = input.get(ContentManager.FETCH_COLUMNS);
		TableInfo tableInfo = null;
		boolean usageAllowed = false;
		try {
			if (USAGE.equals(action)) {
				usageAllowed = getProgramUsageAllowed(input);
			}
			if (fetch != null && fetch.equals(ContentManager.FETCH_ALL)) {
				sqlQuery = "SELECT * FROM " + table + " WHERE " + column + " = ?";
			} else {
				sqlQuery = "SELECT " + fetchColumns + " ,ENABLED,TBA_KEY FROM " + table + " WHERE " + column + " = ?"; // jainudeen
			}
			util.reset();
			util.setSql(sqlQuery);
			// util.setString(1, getContext().getEntityCode());//jainudeen
			DBInfo info = new DBInfo();
			tableInfo = info.getTableInfo(table);
			Map<String, BindParameterType> columnType = tableInfo.getColumnInfo().getColumnMapping();
			util.setString(1, value);// jainudeen
			switch (columnType.get(column)) {
			case LONG:
				if (value == null)
					util.setNull(1, Types.NUMERIC);
				else
					util.setLong(1, Long.valueOf(value));
				break;
			case VARCHAR:
				if (value == null)
					util.setNull(1, Types.VARCHAR);
				else
					util.setString(1, value);
				break;
			case DATE:
				if (value == null)
					util.setNull(1, Types.DATE);
				else
					util.setDate(1, new java.sql.Date(((new java.util.Date(value)).getTime())));
				break;
			case INTEGER:
				if (value == null)
					util.setNull(1, Types.INTEGER);
				else
					util.setInt(1, Integer.valueOf(value));
				break;
			case BIGDECIMAL:
				if (value == null)
					util.setNull(1, Types.INTEGER);
				else
					util.setBigDecimal(1, new BigDecimal(value));
				break;
			case TIMESTAMP:
				if (value == null)
					util.setNull(1, Types.TIMESTAMP);
				else
					util.setTimestamp(1, Timestamp.valueOf(value));
				break;
			case DECIMAL:
				if (value == null)
					util.setNull(1, Types.DECIMAL);
				else
					util.setBigDecimal(1, new BigDecimal(value));
				break;
			case BIGINT:
				if (value == null)
					util.setNull(1, Types.DECIMAL);
				else
					util.setLong(1, Long.valueOf(value));
				break;
			}
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				decodeRow(rset, result);
				String enabled = rset.getString("ENABLED");
				if (enabled != null && enabled.equals(RegularConstants.COLUMN_DISABLE)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.CODE_CLOSED);
				}
				if (usageAllowed) {
					if (USAGE.equals(action)) {

						if (rset.getString("TBA_KEY") != null) {
							result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_USAGE_NOT_ALLOWED);
						}
					}
				}
			} else {
				result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return result;
	}

	public final DTObject validEntityCode(DTObject input) {
		DTObject result = new DTObject();
		String value = input.get(ContentManager.CODE);
		String column = input.get(ContentManager.COLUMN);
		String fetch = input.get(ContentManager.FETCH);
		String sqlQuery = null;
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = getDbContext().createUtilInstance();
		String action = input.get(ContentManager.USER_ACTION);
		String fetchColumns = input.get(ContentManager.FETCH_COLUMNS);
		boolean usageAllowed = false;
		try {
			if (action.equals(USAGE)) {
				usageAllowed = getProgramUsageAllowed(input);
			}
			if (fetch != null && fetch.equals(ContentManager.FETCH_ALL)) {
				sqlQuery = "SELECT * FROM  ENTITIES WHERE " + column + " = ?";
			} else {
				sqlQuery = "SELECT " + fetchColumns + " ,ENTITY_DISSOLVED,TBA_KEY FROM ENTITIES WHERE " + column + " = ?";
			}
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, value);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				decodeRow(rset, result);
				String entityDissolved = rset.getString("ENTITY_DISSOLVED");
				if (entityDissolved != null && entityDissolved.equals(RegularConstants.COLUMN_ENABLE)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_ENTITY_DISSOLVED);
				}
				if (usageAllowed) {
					if (action.equals(USAGE)) {
						if (rset.getString("TBA_KEY") != null) {
							result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_USAGE_NOT_ALLOWED);
						}
					}
				}
			} else {
				result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return result;
	}

	public final DTObject validBillHeadCode(DTObject input) {

		DTObject result = new DTObject();
		String value = input.get(ContentManager.CODE);
		String column = input.get(ContentManager.COLUMN);
		String entityCode = input.get(ContentManager.ENTITY_CODE);
		String fetch = input.get(ContentManager.FETCH);
		String sqlQuery = null;
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = getDbContext().createUtilInstance();
		String action = input.get(ContentManager.USER_ACTION);
		String fetchColumns = input.get(ContentManager.FETCH_COLUMNS);
		boolean usageAllowed = false;
		try {
			if (action.equals(USAGE)) {
				usageAllowed = getProgramUsageAllowed(input);
			}
			if (fetch != null && fetch.equals(ContentManager.FETCH_ALL)) {
				sqlQuery = "SELECT BILHEAD.ENTITY_CODE,BHEAD_INTERNAL_NO,INT_DESCRIPTION,CONCISE_DESCRIPTION,EXT_DESCRIPTION,PRN_DTLS_UNDER_BHEAD,PRN_DOC_NAME_UNDER_BHEAD,ENABLED,REMARKS,TBA_KEY FROM ";
				sqlQuery = sqlQuery + " BILLHEAD BILHEAD ,BILLHEADOTN BILHEADOTN WHERE BILHEADOTN." + entityCode + " = ? AND " + column + " = ?";
				sqlQuery = sqlQuery + " AND BILHEAD.BHEAD_INTERNAL_NO = BILHEADOTN.BHOTN_INTERNAL_NO AND BILHEAD.ENTITY_CODE = BILHEADOTN.ENTITY_CODE";
			} else {
				sqlQuery = "SELECT " + fetchColumns + " ,ENABLED,TBA_KEY FROM ";
				sqlQuery = sqlQuery + " BILLHEAD BILHEAD ,BILLHEADOTN BILHEADOTN WHERE BILHEADOTN." + entityCode + " = ? AND " + column + " = ?";
				sqlQuery = sqlQuery + " AND BILHEAD.BHEAD_INTERNAL_NO = BILHEADOTN.BHOTN_INTERNAL_NO AND BILHEAD.ENTITY_CODE = BILHEADOTN.ENTITY_CODE";
			}
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, getContext().getEntityCode());
			util.setString(2, value);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				decodeRow(rset, result);
				String enabled = rset.getString("ENABLED");
				if (enabled != null && enabled.equals(RegularConstants.COLUMN_DISABLE)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.CODE_CLOSED);
				}
				if (usageAllowed) {
					if (action.equals(USAGE)) {
						if (rset.getString("TBA_KEY") != null) {
							result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_USAGE_NOT_ALLOWED);
						}
					}
				}
			} else {
				result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return result;

	}

	public final DTObject validatePK(DTObject input) {
		DTObject result = new DTObject();
		String table = input.get(ContentManager.TABLE);
		String fetch = input.get(ContentManager.FETCH);
		StringBuffer sqlQuery = new StringBuffer();
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = getDbContext().createUtilInstance();
		String action = input.get(ContentManager.USER_ACTION);
		String[] arguments = (String[]) input.getObject(ContentManager.PROGRAM_KEY);
		String fetchColumns = input.get(ContentManager.FETCH_COLUMNS);
		boolean usageAllowed = false;
		String[] keyFields = null;
		BindParameterType[] keyFieldTypes = null;
		TableInfo tableInfo = null;
		try {
			if (action.equals(USAGE)) {
				usageAllowed = getProgramUsageAllowed(input);
			}
			DBInfo info = new DBInfo();
			tableInfo = info.getTableInfo(table);
			keyFields = tableInfo.getPrimaryKeyInfo().getColumnNames();
			keyFieldTypes = tableInfo.getPrimaryKeyInfo().getColumnTypes();
			if (fetch != null && fetch.equals(ContentManager.FETCH_ALL)) {
				sqlQuery.append("SELECT * FROM ").append(table).append(" WHERE ");
				int i = 0;
				for (String fieldName : keyFields) {
					if (i < keyFields.length - 1) {
						sqlQuery.append(fieldName).append(" = ?  AND  ");
					} else {
						sqlQuery.append(fieldName).append(" = ? ");
					}
					++i;
				}
				sqlQuery.trimToSize();
			} else {
				sqlQuery.append("SELECT " + fetchColumns + " ,TBA_KEY FROM ").append(table).append(" WHERE "); // jainudeen
				int i = 0;
				for (String fieldName : keyFields) {
					if (i < keyFields.length - 1) {
						sqlQuery.append(fieldName).append(" = ?  AND  ");
					} else {
						sqlQuery.append(fieldName).append(" = ? ");
					}
					++i;
				}
				sqlQuery.trimToSize();
			}
			util.reset();
			util.setSql(sqlQuery.toString());
			int k = 1;
			Map<String, BindParameterType> columnType = tableInfo.getColumnInfo().getColumnMapping();
			for (int i = 0; i < keyFieldTypes.length; i++) {
				BindParameterType fieldType = columnType.get(keyFields[i]);
				switch (fieldType) {
				case LONG:
					util.setLong(k, Long.parseLong(arguments[i]));
					break;
				case DATE:
					String value = arguments[i];
					util.setDate(k, new java.sql.Date(BackOfficeFormatUtils.getDate(value, context.getDateFormat()).getTime()));
					break;
				case INTEGER:
					util.setInt(k, Integer.parseInt(arguments[i]));
					break;
				case BIGDECIMAL:
					util.setBigDecimal(k, new BigDecimal(keyFields[i]));
					break;
				case VARCHAR:
				default:
					util.setString(k, arguments[i]);
					break;
				}
				k++;
			}
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				if (action.equals(USAGE)) {
					if (rset.getString("TBA_KEY") != null) {
						if (usageAllowed) {
							result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_USAGE_NOT_ALLOWED);
						}
					}
				}
				decodeRow(rset, result);

			} else {
				result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return result;
	}

	public boolean getProgramUsageAllowed(DTObject input) {
		boolean usageAllowed = false;
		// String processId =
		// context.getProcessID().toUpperCase(context.getDefaultLocale());
		String processId = input.get(ContentManager.TABLE);
		DBUtil util = getDbContext().createUtilInstance();
		try {
			String sqlQuery = "SELECT C.MPGM_TRANSIT_CHOICE FROM MPGM M,MPGMCONFIG C WHERE M.MPGM_TABLE_NAME = ? AND MPGM_CLASS IN (1,2,3) AND M.MPGM_ID=C.MPGM_ID LIMIT 1";
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, processId);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				if ("2".equals(rset.getString(1))) {
					usageAllowed = true;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			usageAllowed = false;
		} finally {
			util.reset();
		}
		return usageAllowed;
	}

	public final boolean isValidCodePattern(String code) {
		String regex = "^([A-Z0-9_])+$";
		if (code.matches(regex))
			return true;
		return false;
	}

	public final DTObject validatePersonName(DTObject input) {
		DTObject result = new DTObject();
		// String title1 = input.get("TITLE1");
		String title2 = input.get("TITLE2");
		String name = input.get(ProgramConstants.PERSON_NAME);
		try {
			if (isEmpty(title2)) {
				result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_NAME);
				return result;
			}
			if (isEmpty(name) || !isValidName(name)) {
				result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_NAME);
				return result;
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		}
		return result;

	}

	public final boolean isValidOtherInformation25(String information) {
		if (information == null || information.length() == LENGTH_0 || information.length() > INFORMATION_LENGTH25)
			return false;
		return true;
	}

	public final boolean isValidOtherInformation20(String information) {
		if (information == null || information.length() == LENGTH_0 || information.length() > CONCISE_DESCRIPTION_LENGTH)
			return false;
		return true;
	}

	public final boolean isValidOtherInformation50(String information) {
		if (information == null || information.length() == LENGTH_0 || information.length() > INFORMATION_LENGTH50)
			return false;
		return true;
	}

	public final boolean isValidISOCurrency(String currency) {
		if (currency == null || currency.length() == LENGTH_0 || currency.length() > LENGTH_3)
			return false;
		return true;
	}

	public final boolean isValidCurrencyNotation(String currencyNotation) {
		if (currencyNotation == null || currencyNotation.length() == LENGTH_0 || currencyNotation.length() > LENGTH_6)
			return false;
		return true;
	}

	public final boolean isValidOneDigit(String number) {
		if (number == null || number.length() == LENGTH_0 || number.length() > LENGTH_1)
			return false;
		if (!isValidNumber(number))
			return false;
		return true;
	}

	public final boolean isValidTwoDigit(String number) {
		if (number == null || number.length() == LENGTH_0 || number.length() > LENGTH_2)
			return false;
		if (!isValidNumber(number))
			return false;
		return true;
	}

	public final boolean isValidThreeDigit(String number) {
		if (number == null || number.length() == LENGTH_0 || number.length() > LENGTH_3)
			return false;
		if (!isValidNumber(number))
			return false;
		return true;
	}

	public final boolean isValidFiveDigit(String number) {
		if (number == null || number.length() == LENGTH_0 || number.length() > LENGTH_5)
			return false;
		if (!isValidNumber(number))
			return false;
		return true;
	}

	public final boolean isValidAge(String years, String months, String days) {
		boolean yearsAvailable = true, monthsAvailable = true, daysAvailable = true;
		if (years == null || years.length() == LENGTH_0 || years.length() > LENGTH_3 || !isValidPositiveNumber(years))
			yearsAvailable = false;
		if (months == null || months.length() == LENGTH_0 || months.length() > LENGTH_2 || !isValidPositiveNumber(months))
			monthsAvailable = false;
		if (days == null || days.length() == LENGTH_0 || days.length() > LENGTH_2 || !isValidPositiveNumber(days))
			daysAvailable = false;
		if (isZero(years) && isZero(months) && isZero(days))
			return false;
		if (!(years == null || years.length() == LENGTH_0))
			if (isNumberGreaterThanEqual(years, RegularConstants.MAX_AGE_YEARS))
				return false;

		if (!(months == null || months.length() == LENGTH_0))
			if (isNumberGreaterThanEqual(months, RegularConstants.MAX_AGE_MONTHS))
				return false;

		if (!(days == null || days.length() == LENGTH_0))
			if (isNumberGreaterThanEqual(days, RegularConstants.MAX_AGE_DAYS))
				return false;

		if (yearsAvailable == true || monthsAvailable == true || daysAvailable == true)
			return true;
		return true;
	}

	public final DTObject validateConnectedPerson(DTObject input) {
		DTObject result = new DTObject();
		String relationShip = input.get("RELATIONSHIP");
		String title2 = input.get("TITLE2");
		String name = input.get(ProgramConstants.PERSON_NAME);
		try {
			if (isEmpty(relationShip)) {
				result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_FIELD_BLANK);
				return result;
			}
			if (isEmpty(title2)) {
				result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_FIELD_BLANK);
				return result;
			}
			if (isEmpty(name) || !isValidName(name)) {
				result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_FIELD_BLANK);
				return result;
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		}
		return result;

	}

	public final DTObject validRegPolicy(DTObject input) {
		DTObject result = new DTObject();

		String fetch = input.get(ContentManager.FETCH);
		String sqlQuery = null;
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = getDbContext().createUtilInstance();
		String action = input.get(ContentManager.USER_ACTION);
		String fetchColumns = input.get(ContentManager.FETCH_COLUMNS);
		boolean usageAllowed = false;
		try {
			if (USAGE.equals(action)) {
				usageAllowed = getProgramUsageAllowed(input);
			}
			if (fetch != null && fetch.equals(ContentManager.FETCH_ALL)) {
				sqlQuery = "SELECT * FROM  REGPOLICY WHERE ENTITY_CODE = ?";
			} else {
				sqlQuery = "SELECT " + fetchColumns + " ,TBA_KEY FROM REGPOLICY WHERE  ENTITY_CODE  = ?";
			}
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, context.getEntityCode());
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				decodeRow(rset, result);
				if (usageAllowed) {
					if (USAGE.equals(action)) {
						if (rset.getString("TBA_KEY") != null) {
							result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_USAGE_NOT_ALLOWED);
						}
					}
				}
			} else {
				result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return result;
	}

	public final DTObject validPersonPID(DTObject input) {
		DTObject result = new DTObject();

		try {
			result = validatePK(input);
			if (result.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
				if (RegularConstants.COLUMN_ENABLE.equals(result.get("PID_DE_REG")))
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_PID_ALREADY_DEREG);
				return result;
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		}
		return result;
	}

	public final DTObject validGLAccessCode(DTObject input) {
		DTObject result = new DTObject();
		String value = input.get(ContentManager.CODE);
		String column = input.get(ContentManager.COLUMN);
		String fetch = input.get(ContentManager.FETCH);
		String entityCode = input.get(ContentManager.ENTITY_CODE);
		String sqlQuery = null;
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = getDbContext().createUtilInstance();
		String action = input.get(ContentManager.USER_ACTION);
		String fetchColumns = input.get(ContentManager.FETCH_COLUMNS);
		boolean usageAllowed = false;
		try {
			if (action.equals(USAGE)) {
				usageAllowed = getProgramUsageAllowed(input);
			}
			if (fetch != null && fetch.equals(ContentManager.FETCH_ALL)) {
				sqlQuery = "SELECT * FROM GLMAST WHERE  " + entityCode + " = ? AND " + column + " = ?";
			} else {
				sqlQuery = "SELECT " + fetchColumns + " ,TBA_KEY FROM GLMAST WHERE " + entityCode + " = ? AND " + column + " = ?";

			}
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, getContext().getEntityCode());
			util.setString(2, value);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				decodeRow(rset, result);
				if (usageAllowed) {
					if (action.equals(USAGE)) {
						if (rset.getString("TBA_KEY") != null) {
							result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_USAGE_NOT_ALLOWED);
						}
					}
				}
			} else {
				result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return result;
	}

	public final DTObject validPersonPIDWithPIDCode(DTObject input) {
		DTObject result = new DTObject();
		String[] arguments = (String[]) input.getObject(ContentManager.PROGRAM_KEY);
		String fetch = input.get(ContentManager.FETCH);
		String entityCode = input.get(ContentManager.ENTITY_CODE);
		String sqlQuery = null;
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = getDbContext().createUtilInstance();
		String action = input.get(ContentManager.USER_ACTION);
		String fetchColumns = input.get(ContentManager.FETCH_COLUMNS);
		boolean usageAllowed = false;
		try {
			if (action.equals(USAGE)) {
				usageAllowed = getProgramUsageAllowed(input);
			}
			if (fetch != null && fetch.equals(ContentManager.FETCH_ALL)) {
				sqlQuery = "SELECT * FROM PERSONPID WHERE PERSON_ID = ? AND " + entityCode + " = ? AND PID_CODE  = ? AND PID_DE_REG ='0' ";
			} else {
				sqlQuery = "SELECT " + fetchColumns + " ,PID_DE_REG,TBA_KEY FROM PERSONPID WHERE PERSON_ID = ? AND " + entityCode + " = ? AND PID_CODE  = ? AND PID_DE_REG ='0'";

			}
			util.reset();
			util.setSql(sqlQuery);

			util.setString(1, arguments[0]);
			util.setString(2, getContext().getEntityCode());
			util.setString(3, arguments[1]);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				decodeRow(rset, result);
				String deRegistred = rset.getString("PID_DE_REG");
				if (deRegistred != null && RegularConstants.COLUMN_DISABLE.equals(deRegistred)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_PID_EXISTS);
				}
				if (usageAllowed) {
					if (action.equals(USAGE)) {
						if (rset.getString("TBA_KEY") != null) {
							result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_USAGE_NOT_ALLOWED);
						}
					}
				}
			} else {
				result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return result;
	}

	public final DTObject validPersonIDWithDoctorCode(DTObject input) {
		DTObject result = new DTObject();
		String[] arguments = (String[]) input.getObject(ContentManager.PROGRAM_KEY);
		String fetch = input.get(ContentManager.FETCH);
		String entityCode = input.get(ContentManager.ENTITY_CODE);
		String sqlQuery = null;
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = getDbContext().createUtilInstance();
		String action = input.get(ContentManager.USER_ACTION);
		String fetchColumns = input.get(ContentManager.FETCH_COLUMNS);
		boolean usageAllowed = false;
		try {
			if (action.equals(USAGE)) {
				usageAllowed = getProgramUsageAllowed(input);
			}
			if (fetch != null && fetch.equals(ContentManager.FETCH_ALL)) {
				sqlQuery = "SELECT * FROM DOCTORS WHERE " + entityCode + " = ? AND DOCTOR_CODE <> ? AND DOCTOR_PERSON_ID = ? ";
			} else {
				sqlQuery = "SELECT " + fetchColumns + " ,ENABLED,TBA_KEY FROM DOCTORS WHERE " + entityCode + " = ? AND DOCTOR_CODE <> ? AND DOCTOR_PERSON_ID = ? ";

			}
			util.reset();
			util.setSql(sqlQuery);

			util.setString(1, getContext().getEntityCode());
			util.setString(2, arguments[1]);
			util.setString(3, arguments[0]);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				decodeRow(rset, result);
				String deRegistred = rset.getString("PID_DE_REG");
				if (deRegistred != null && RegularConstants.COLUMN_DISABLE.equals(deRegistred)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_PID_EXISTS);
				}
				if (usageAllowed) {
					if (action.equals(USAGE)) {
						if (rset.getString("TBA_KEY") != null) {
							result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_USAGE_NOT_ALLOWED);
						}
					}
				}
			} else {
				result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return result;
	}

	public final DTObject validRegNoWithDoctorCode(DTObject input) {
		DTObject result = new DTObject();
		String[] arguments = (String[]) input.getObject(ContentManager.PROGRAM_KEY);
		String fetch = input.get(ContentManager.FETCH);
		String entityCode = input.get(ContentManager.ENTITY_CODE);
		String sqlQuery = null;
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = getDbContext().createUtilInstance();
		String action = input.get(ContentManager.USER_ACTION);
		String fetchColumns = input.get(ContentManager.FETCH_COLUMNS);
		boolean usageAllowed = false;
		try {
			if (action.equals(USAGE)) {
				usageAllowed = getProgramUsageAllowed(input);
			}
			if (fetch != null && fetch.equals(ContentManager.FETCH_ALL)) {
				sqlQuery = "SELECT * FROM DOCTORS WHERE " + entityCode + " = ? AND DOCTOR_CODE <> ? AND DOCTOR_REG_NO = ? ";
			} else {
				sqlQuery = "SELECT " + fetchColumns + " ,ENABLED,TBA_KEY FROM DOCTORS WHERE " + entityCode + " = ? AND DOCTOR_CODE <> ? AND DOCTOR_REG_NO = ? ";

			}
			util.reset();
			util.setSql(sqlQuery);

			util.setString(1, getContext().getEntityCode());
			util.setString(2, arguments[1]);
			util.setString(3, arguments[0]);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				decodeRow(rset, result);
				String deRegistred = rset.getString("PID_DE_REG");
				if (deRegistred != null && RegularConstants.COLUMN_DISABLE.equals(deRegistred)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_PID_EXISTS);
				}
				if (usageAllowed) {
					if (action.equals(USAGE)) {
						if (rset.getString("TBA_KEY") != null) {
							result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_USAGE_NOT_ALLOWED);
						}
					}
				}
			} else {
				result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return result;
	}

	public final DTObject validateEntityWiseCodesWithoutEnabled(DTObject input) {
		DTObject result = new DTObject();
		String value = input.get(ContentManager.CODE);
		String entityCode = input.get(ContentManager.ENTITY_CODE);
		String code = input.get(ContentManager.COLUMN);
		String table = input.get(ContentManager.TABLE);
		String fetch = input.get(ContentManager.FETCH);
		String fetchColumns = input.get(ContentManager.FETCH_COLUMNS);
		String sqlQuery = null;
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = getDbContext().createUtilInstance();
		String action = input.get(ContentManager.USER_ACTION);
		boolean usageAllowed = false;
		try {
			if (action.equals(USAGE)) {
				usageAllowed = getProgramUsageAllowed(input);
			}
			if (fetch != null && fetch.equals(ContentManager.FETCH_ALL)) {
				sqlQuery = "SELECT * FROM " + table + " WHERE " + entityCode + " = ? AND " + code + " = ?";
			} else {
				sqlQuery = "SELECT " + fetchColumns + " ,TBA_KEY FROM " + table + " WHERE " + entityCode + " = ? AND " + code + " = ?";
			}
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, getContext().getEntityCode());
			util.setString(2, value);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				decodeRow(rset, result);

				if (usageAllowed) {
					if (action.equals(USAGE)) {
						if (rset.getString("TBA_KEY") != null) {
							result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_USAGE_NOT_ALLOWED);
						}
					}
				}

			} else {
				result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return result;
	}

	public final DTObject getFetchEmployeeAndPersonDBDetails(DTObject input) {
		DTObject result = new DTObject();
		String value = input.get(ContentManager.CODE);
		String column = input.get(ContentManager.COLUMN);
		String entityCode = input.get(ContentManager.ENTITY_CODE);
		String fetch = input.get(ContentManager.FETCH);
		String sqlQuery = null;
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = getDbContext().createUtilInstance();
		String fetchColumns = input.get(ContentManager.FETCH_COLUMNS);
		try {

			if (fetch != null && fetch.equals(ContentManager.FETCH_ALL)) {
				sqlQuery = "SELECT FN_GETEMP_STATUS(?,EMPDB.EMP_CODE) STATUS,EMPDB.ENTITY_CODE,EMPDB.EMP_CODE,EMPDB.PERSON_ID,EMPDB.DATE_OF_JOIN,EMPDB.PF_APPLICABLE,EMPDB.PF_NUMBER,EMPDB.PENSION_FUND_REQD,EMPDB.ELIGIBLE_FOR_ESI,EMPDB.EMP_ESI_NO,EMPDB.EMP_QUALIFICATION,EMPDB.REMARKS,EMPDB.TBA_KEY,"
						+ "PERSONDB.TITLE_CODE1,PERSONDB.TITLE_CODE2,PERSONDB.PERSON_NAME,PERSONDB.GENDER,PERSONDB.DOB,PERSONDB.AGE_YEARS,PERSONDB.AGE_MONTHS,PERSONDB.AGE_DAYS,PERSONDB.MARITAL_STATUS,PERSONDB.LEGAL_CONN_REL_CODE,PERSONDB.LEGAL_CONN_TITLE_CODE1,"
						+ "PERSONDB.LEGAL_CONN_TITLE_CODE2,PERSONDB.LEGAL_CONN_PERSON_NAME,PERSONDB.DEPENDENT_FLAG,PERSONDB.DEPEND_REL_CODE,PERSONDB.DEPEND_TITLE1,PERSONDB.DEPEND_TITLE2,PERSONDB.DEPEND_PERSON_NAME,PERSONDB.RELATIONSHIP_INFO,PERSONDB.RELIGION_CODE,"
						+ "PERSONDB.OCCUPATION_CODE,PERSONDB.EMPLOYED_WITH_ORG_CODE,PERSONDB.PERSON_DESIG_CODE,PERSONDB.EMPLOYMENT_DESIG_INFO,PERSONDB.EMPLOYEE_NO,PERSONDB.PERM_SAS_PRESENT," + "PERSONDB.COMM_DTLS_SAS,PERSONDB.BLOOD_GROUP,PERSONDB.SPECIAL_INFO,PERSONDB.DECEASED_ON,PERSONDB.DECEASED_ENTD_ON,PERSONDB.DECEASED_ENTD_BY,PERSONDB.DECEASED_CAUSE,"
						+ "PERSONDB.DECEASED_INFO,PERSONDB.ENABLED,PERSONDB.TBA_KEY FROM ";
				sqlQuery = sqlQuery + " EMPDB , PERSONDB WHERE EMPDB." + entityCode + " = ? AND EMPDB." + column + " = ?";
				sqlQuery = sqlQuery + " AND EMPDB.PERSON_ID = PERSONDB.PERSON_ID AND PERSONDB.PARTITION_NO = ? ";
			} else {
				sqlQuery = "SELECT " + fetchColumns + " ,FN_GETEMP_STATUS(?,EMPDB.EMP_CODE) STATUS,EMPDB.TBA_KEY,PERSONDB.ENABLED,PERSONDB.TBA_KEY FROM ";
				sqlQuery = sqlQuery + " EMPDB ,PERSONDB WHERE EMPDB." + entityCode + " = ? AND EMPDB." + column + " = ?";
				sqlQuery = sqlQuery + " AND EMPDB.PERSON_ID = PERSONDB.PERSON_ID AND PERSONDB.PARTITION_NO = ? ";
			}
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, getContext().getEntityCode());
			util.setString(2, getContext().getEntityCode());
			util.setString(3, value);
			util.setString(4, getContext().getPartitionNo());
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				decodeRow(rset, result);

			} else {
				result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return result;
	}

	public final DTObject validReportNumber(DTObject input) {
		DTObject result = new DTObject();
		String[] arguments = (String[]) input.getObject(ContentManager.PROGRAM_KEY);
		String fetch = input.get(ContentManager.FETCH);
		String sqlQuery = null;
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = getDbContext().createUtilInstance();
		String action = input.get(ContentManager.USER_ACTION);
		String fetchColumns = input.get(ContentManager.FETCH_COLUMNS);
		boolean usageAllowed = false;
		try {
			if (action.equals(USAGE)) {
				usageAllowed = getProgramUsageAllowed(input);
			}
			if (fetch != null && fetch.equals(ContentManager.FETCH_ALL)) {
				sqlQuery = "SELECT * FROM LABINVRES{0} WHERE  ENTITY_CODE = ? AND REPORT_PREFIX_CODE = ? AND REPORT_YEAR = ? AND REPORT_NUMBER = ?";
			} else {
				sqlQuery = "SELECT " + fetchColumns + " FROM LABINVRES{0} WHERE ENTITY_CODE = ? AND REPORT_PREFIX_CODE = ? AND REPORT_YEAR = ? AND REPORT_NUMBER = ? ";

			}
			util.reset();
			util.setSql(MessageFormat.format(sqlQuery.toString(), new Object[] { arguments[2] }));
			util.setString(1, arguments[0]);
			util.setString(2, arguments[1]);
			util.setString(3, arguments[2]);
			util.setString(4, arguments[3]);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				decodeRow(rset, result);
			} else {
				result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return result;
	}

	public final DTObject validOPBillNumber(DTObject input) {
		DTObject result = new DTObject();
		String[] arguments = (String[]) input.getObject(ContentManager.PROGRAM_KEY);
		String fetch = input.get(ContentManager.FETCH);
		String sqlQuery = null;
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = getDbContext().createUtilInstance();
		String action = input.get(ContentManager.USER_ACTION);
		String fetchColumns = input.get(ContentManager.FETCH_COLUMNS);
		boolean usageAllowed = false;
		try {
			if (action.equals(USAGE)) {
				usageAllowed = getProgramUsageAllowed(input);
			}
			if (fetch != null && fetch.equals(ContentManager.FETCH_ALL)) {
				sqlQuery = "SELECT * FROM OUTPATIENTBILL WHERE  ENTITY_CODE = ? AND BILL_TYPE_CODE = ? AND BILL_YEAR = ? AND BILL_SERIAL = ?";
			} else {
				sqlQuery = "SELECT " + fetchColumns + ",TBA_KEY FROM OUTPATIENTBILL WHERE ENTITY_CODE = ? AND BILL_TYPE_CODE = ? AND BILL_YEAR = ? AND BILL_SERIAL = ? ";
			}
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, arguments[0]);
			util.setString(2, arguments[1]);
			util.setString(3, arguments[2]);
			util.setString(4, arguments[3]);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				if (usageAllowed) {
					if (USAGE.equals(action)) {
						if (rset.getString("TBA_KEY") != null) {
							result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_USAGE_NOT_ALLOWED);
						}
					}
				}
				decodeRow(rset, result);
			} else {
				result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return result;
	}

	public final DTObject validIPNumber(DTObject input) {
		DTObject result = new DTObject();
		String[] arguments = (String[]) input.getObject(ContentManager.PROGRAM_KEY);
		String fetch = input.get(ContentManager.FETCH);
		String sqlQuery = null;
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = getDbContext().createUtilInstance();
		String action = input.get(ContentManager.USER_ACTION);
		String fetchColumns = input.get(ContentManager.FETCH_COLUMNS);
		boolean usageAllowed = false;
		try {
			if (action.equals(USAGE)) {
				usageAllowed = getProgramUsageAllowed(input);
			}
			if (fetch != null && fetch.equals(ContentManager.FETCH_ALL)) {
				sqlQuery = "SELECT * FROM INPATIENT WHERE  ENTITY_CODE = ? AND IP_NO_YEAR = ? AND IP_NO_SERIAL = ? ";
			} else {
				sqlQuery = "SELECT " + fetchColumns + " FROM INPATIENT WHERE ENTITY_CODE = ? AND IP_NO_YEAR = ? AND IP_NO_SERIAL = ? ";
			}
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, getContext().getEntityCode());
			util.setString(2, arguments[1]);
			util.setString(3, arguments[2]);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				decodeRow(rset, result);
			} else {
				result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return result;
	}

	public final DTObject validIPChargeRefNo(DTObject input) {
		DTObject result = new DTObject();
		String[] arguments = (String[]) input.getObject(ContentManager.PROGRAM_KEY);
		String fetch = input.get(ContentManager.FETCH);
		String sqlQuery = null;
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = getDbContext().createUtilInstance();
		String action = input.get(ContentManager.USER_ACTION);
		String fetchColumns = input.get(ContentManager.FETCH_COLUMNS);
		boolean usageAllowed = false;
		try {
			if (action.equals(USAGE)) {
				usageAllowed = getProgramUsageAllowed(input);
			}
			if (fetch != null && fetch.equals(ContentManager.FETCH_ALL)) {
				sqlQuery = "SELECT * FROM IPACLEDGER WHERE  ENTITY_CODE = ? AND IP_NO_YEAR = ? AND IP_NO_SERIAL = ? AND ENTRY_DATE = ? AND ENTRY_SL = ?";
			} else {
				sqlQuery = "SELECT " + fetchColumns + " FROM IPACLEDGER WHERE ENTITY_CODE = ? AND IP_NO_YEAR = ? AND IP_NO_SERIAL = ? AND ENTRY_DATE = ? AND ENTRY_SL = ?";
			}
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, getContext().getEntityCode());
			util.setString(2, arguments[0]);
			util.setString(3, arguments[1]);
			util.setDate(4, new java.sql.Date((isValidDate(arguments[2])).getTime()));
			util.setString(5, arguments[3]);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				decodeRow(rset, result);
			} else {
				result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return result;
	}

	public String lpadPrefix(String value, String prefix, int max) {
		return value.length() < max ? lpadPrefix(prefix + value, prefix, max) : value;
	}

	public final boolean isValidYear(String year) {
		String regex = "^(19|20)\\d{2}$";
		if (year.matches(regex))
			return true;
		return false;
	}

	public final DTObject validPatientRegistrationNumber(DTObject input) {

		DTObject result = new DTObject();
		String entityCode = input.get(ContentManager.ENTITY_CODE);
		String fetch = input.get(ContentManager.FETCH);
		String sqlQuery = null;
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = getDbContext().createUtilInstance();
		String action = input.get(ContentManager.USER_ACTION);
		String fetchColumns = input.get(ContentManager.FETCH_COLUMNS);
		boolean usageAllowed = false;
		try {
			if (action.equals(USAGE)) {
				usageAllowed = getProgramUsageAllowed(input);
			}
			if (fetch != null && fetch.equals(ContentManager.FETCH_ALL)) {
				sqlQuery = "SELECT * FROM ";
				sqlQuery = sqlQuery + " PATIENTREG WHERE " + entityCode + " = ? AND PATIENT_REG_YEAR  = ?";
				sqlQuery = sqlQuery + " AND PATIENT_REG_SL = ? ";
			} else {
				sqlQuery = "SELECT " + fetchColumns + " FROM ";
				sqlQuery = sqlQuery + " PATIENTREG REG  WHERE " + entityCode + " = ? AND PATIENT_REG_YEAR  = ?";
				sqlQuery = sqlQuery + " AND PATIENT_REG_SL = ? ";
			}
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, getContext().getEntityCode());
			util.setString(2, input.get("PATIENT_REG_YEAR"));
			util.setString(3, input.get("PATIENT_REG_SL"));
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				decodeRow(rset, result);
			} else {
				result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return result;

	}

	public final boolean isValidTelephoneNumber(String value) {
		if (value == null || value.length() == 0)
			return false;
		String regex = "^([0-9]{1,15})$";
		if (value.matches(regex))
			return true;
		return false;
	}

	public final String getParentProgramID(String programID) {
		String parentProgram = programID;
		DBUtil util = getDbContext().createUtilInstance();
		try {
			String sqlQuery = "SELECT MPGM_ID FROM MPGM WHERE MPGM_QUERY_PATH=? ";
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, programID);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				parentProgram = rset.getString(1);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			util.reset();
		}
		return parentProgram;
	}

	public final DTObject isDeDupAlreadyExist(DTObject input) {
		DTObject result = new DTObject();
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		result = isRecordDeDupCheck(input);
		if (result.get(ContentManager.ERROR) != null || result.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
			return result;
		}
		String sqlQuery = null;
		DBUtil util = getDbContext().createUtilInstance();
		try {
			sqlQuery = "SELECT PGM_PK FROM TBADEDUPCHECK WHERE PARTITION_NO=? AND PGM_ID=? AND PURPOSE_ID=? AND PGM_PK<>? AND COLUMN_VALUES=?";
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, getContext().getPartitionNo());
			util.setString(2, input.get(ContentManager.PROGRAM_ID));
			util.setString(3, input.get(ContentManager.PURPOSE_ID));
			util.setString(4, input.get(ContentManager.PROGRAM_KEY));
			util.setString(5, input.get(ContentManager.DEDUP_KEY));
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				result.set("LINKED_TO_PK", rset.getString("PGM_PK"));
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return result;
	}

	public DTObject isRecordDeDupCheck(DTObject input) {
		DTObject result = new DTObject();
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = getDbContext().createUtilInstance();
		String pgmdedupcfg_sqlquery = "SELECT TABLE_NAME,TABLE_TYPE FROM PGMDEDUPCFG WHERE PGM_ID=? AND PURPOSE_ID=?";
		List<String> purposeFields = new ArrayList<String>();
		String[] purposeFieldsValue = input.get(ContentManager.DEDUP_KEY).split("\\|");
		String tableName = RegularConstants.EMPTY_STRING;
		String tableType = RegularConstants.EMPTY_STRING;
		try {
			util.setMode(DBUtil.PREPARED);
			util.setSql(pgmdedupcfg_sqlquery);
			util.setString(1, input.get(ContentManager.PROGRAM_ID));
			util.setString(2, input.get(ContentManager.PURPOSE_ID));
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				tableName = rset.getString(1);
				tableType = rset.getString(2);
			} else {
				result.set(ContentManager.ERROR, BackOfficeErrorCodes.DEDUP_CONFIGURATION_NOT_AVAILABLE);
			}
			DBInfo info = new DBInfo();
			TableInfo tableInfo = info.getTableInfo(tableName);
			String[] keyFields = tableInfo.getPrimaryKeyInfo().getColumnNames();
			if (tableType.equals("D")) {
				keyFields = ArrayUtils.remove(keyFields, keyFields.length - 1);
			}
			Map<String, BindParameterType> columnType = tableInfo.getColumnInfo().getColumnMapping();
			purposeFields = getPurposeColumnName(input);
			String sqlQuery = generateDedupchkSQL(tableName, keyFields, purposeFields);
			String pkValue = executeDedupchkSQL(util, sqlQuery, keyFields, purposeFields, purposeFieldsValue, columnType, input.get(ContentManager.PROGRAM_KEY));
			if (!isEmpty(pkValue)) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				result.set("LINKED_TO_PK", pkValue);
			}
		} catch (Exception e) {
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			e.printStackTrace();
		} finally {
			util.reset();
		}
		return result;
	}

	private List<String> getPurposeColumnName(DTObject input) throws SQLException {
		List<String> result = new ArrayList<String>();
		DBUtil util = dbContext.createUtilInstance();
		String sql = "SELECT COLUMN_NAME FROM PGMDEDUPCFGDTL WHERE PGM_ID=? AND PURPOSE_ID=? ORDER BY SL";
		util.setMode(DBUtil.PREPARED);
		util.setSql(sql);
		util.setString(1, input.get("PROGRAM_ID"));
		util.setString(2, input.get("PURPOSE_ID"));
		ResultSet rset = util.executeQuery();
		while (rset.next()) {
			result.add(rset.getString(1));
		}
		return result;
	}

	private String generateDedupchkSQL(String tableName, String[] keyFields, List<String> purposeFields) {
		StringBuffer sqlQuery = new StringBuffer();
		StringBuffer fetchColumns = new StringBuffer(RegularConstants.EMPTY_STRING);
		StringBuffer selectClause = new StringBuffer(RegularConstants.EMPTY_STRING);
		StringBuffer filterColumns = new StringBuffer(RegularConstants.EMPTY_STRING);
		StringBuffer whereClause = new StringBuffer(RegularConstants.EMPTY_STRING);
		for (String fieldName : keyFields) {
			fetchColumns.append(fieldName).append(",");
		}
		selectClause.append("SELECT ").append(fetchColumns.substring(0, fetchColumns.length() - 1));

		for (String fieldName : purposeFields) {
			filterColumns.append(fieldName).append("=? AND ");
		}
		whereClause.append(" WHERE ").append(filterColumns.substring(0, filterColumns.length() - 5));
		sqlQuery.append(selectClause).append(" FROM ").append(tableName).append(whereClause);
		return sqlQuery.toString();

	}

	private String executeDedupchkSQL(DBUtil util, String sqlQuery, String[] keyFields, List<String> purposeFields, String[] purposeFieldsValue, Map<String, BindParameterType> columnType, String inputKey) throws SQLException {
		util.reset();
		util.setMode(DBUtil.PREPARED);
		util.setSql(sqlQuery);
		int i = 0;
		for (String fieldName : purposeFields) {
			switch (columnType.get(fieldName)) {
			case LONG:
				util.setLong(i + 1, Long.parseLong(purposeFieldsValue[i]));
				break;
			case VARCHAR:
				util.setString(i + 1, purposeFieldsValue[i]);
				break;
			case DATE:
				util.setDate(i + 1, new java.sql.Date(BackOfficeFormatUtils.getDate(purposeFieldsValue[i], getContext().getDateFormat()).getTime()));
				break;
			case INTEGER:
				util.setInt(i + 1, Integer.parseInt(purposeFieldsValue[i]));
				break;
			case TIMESTAMP:
				util.setTimestamp(i + 1, new java.sql.Timestamp(BackOfficeFormatUtils.getDate(purposeFieldsValue[i], getContext().getDateFormat() + " " + RegularConstants.TIME_FORMAT).getTime()));
				break;
			case DECIMAL:
				util.setBigDecimal(i + 1, BigDecimal.valueOf(Double.parseDouble(purposeFieldsValue[i])));
				break;
			case BIGINT:
				util.setLong(i + 1, Long.parseLong(purposeFieldsValue[i]));
			default:
				break;
			}
			++i;
		}
		ResultSet rs = util.executeQuery();
		SimpleDateFormat sdf = new SimpleDateFormat(BackOfficeFormatUtils.getSimpleDateFormat(getContext().getDateFormat()));
		SimpleDateFormat sdft = new SimpleDateFormat(BackOfficeFormatUtils.getSimpleDateFormat(getContext().getDateFormat()) + " HH:mm:ss");
		StringBuffer pkvalue = new StringBuffer(RegularConstants.EMPTY_STRING);
		if (rs.next()) {
			do {
				for (int j = 1; j < rs.getMetaData().getColumnCount() + 1; j++) {
					if (rs.getMetaData().getColumnTypeName(j).equals("DATE")) {
						pkvalue.append(sdf.format(rs.getDate(j))).append("|");
					} else if (rs.getMetaData().getColumnTypeName(j).equals("DATETIME") || rs.getMetaData().getColumnTypeName(j).equals("TIMESTAMP")) {
						pkvalue.append(sdft.format(rs.getTimestamp(j))).append("|");
					} else {
						pkvalue.append(rs.getString(j)).append("|");
					}
				}
				if (!pkvalue.substring(0, pkvalue.length() - 1).equals(inputKey))
					return pkvalue.substring(0, pkvalue.length() - 1).toString();
				pkvalue.delete(0, pkvalue.length());
			} while (rs.next());
		}
		return pkvalue.toString();
	}

	public Timestamp getCBDTime() {
		Timestamp sys_date_time = null;
		DBUtil dbutil = getDbContext().createUtilInstance();
		try {
			dbutil.reset();
			dbutil.setMode(DBUtil.PREPARED);
			dbutil.setSql("SELECT FN_GETCDT(?) FROM DUAL");
			dbutil.setString(1, getContext().getEntityCode());
			ResultSet rs = dbutil.executeQuery();
			if (rs.next()) {
				sys_date_time = rs.getTimestamp(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			dbutil.reset();
		}
		return sys_date_time;
	}

	public DTObject getPartitionInfo(String programID) {
		DTObject result = new DTObject();
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = dbContext.createUtilInstance();
		String sql = "SELECT PARTITION_YEAR_WISE,PARTITION_DATA_COLUMN,INTERCEPTOR_CLASS FROM MPGM WHERE MPGM_ID=?";
		try {
			util.setMode(DBUtil.PREPARED);
			util.setSql(sql);
			util.setString(1, programID);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				result.set("PARTITION_YEAR_WISE", rset.getString("PARTITION_YEAR_WISE"));
				result.set("PARTITION_DATA_COLUMN", rset.getString("PARTITION_DATA_COLUMN"));
				result.set("INTERCEPTOR_CLASS", rset.getString("INTERCEPTOR_CLASS"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			util.reset();
		}
		return result;
	}

	public DTObject loadOldAddress(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		String personId = inputDTO.get("PERSON_ID");
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = getDbContext().createUtilInstance();
		String sqlQuery = null;
		String addr1 = null;
		String addr2 = null;
		String addr3 = null;
		String addr4 = null;
		String addr5 = null;
		try {
			sqlQuery = " SELECT OMG_ADDRESS1,OMG_ADDRESS2,OMG_ADDRESS3,OMG_ADDRESS4,OMG_ADDRESS5 FROM PERSONOTHINFO WHERE PARTITION_NO=? AND  PERSON_ID=? ";
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, context.getPartitionNo());
			util.setString(2, personId);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				addr1 = (rset.getString(1));
				addr2 = (rset.getString(2));
				addr3 = (rset.getString(3));
				addr4 = (rset.getString(4));
				addr5 = (rset.getString(5));
			}
			if ((addr1 == null && addr2 == null || addr3 == null) || (addr1.equalsIgnoreCase(RegularConstants.EMPTY_STRING) && addr2.equalsIgnoreCase(RegularConstants.EMPTY_STRING) && addr3.equalsIgnoreCase(RegularConstants.EMPTY_STRING))) {
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
			} else {
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				resultDTO.set("OLD_ADDRESS1", addr1);
				resultDTO.set("OLD_ADDRESS2", addr2);
				resultDTO.set("OLD_ADDRESS3", addr3);
				resultDTO.set("OLD_ADDRESS4", addr4);
				resultDTO.set("OLD_ADDRESS5", addr5);
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return resultDTO;
	}

	public DTObject getSearchAddressDetail(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		String searchContent = inputDTO.get("SEARCH_CONTENT");
		String searchType = inputDTO.get("SEARCH_TYPE");
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = getDbContext().createUtilInstance();
		DBUtil dbutil = getDbContext().createUtilInstance();
		String sqlQuery = null;
		try {
			if (searchType.equals("AP")) {
				sqlQuery = "SELECT S.ADDR_INV_NO, S.ADDR_TYPE,S.ADDRDESC,S.COUNTRY_CODE,S.COUNTRYDESC,S.ADDRESS_LINE1,S.ADDRESS_LINE2,S.ADDRESS_LINE3,S.ADDRESS_LINE4,S.GEO_UNIT_ID,(SELECT COUNT(1) FROM PERSONADDRDB P WHERE P.PARTITION_NO=? AND P.ADDR_INV_NO = S.ADDR_INV_NO),S.GEO_UNIT_STRUC_CODE FROM(SELECT A.ADDR_INV_NO ,A.ADDR_TYPE,T.DESCRIPTION ADDRDESC,A.COUNTRY_CODE,C.DESCRIPTION COUNTRYDESC,C.GEO_UNIT_STRUC_CODE,A.ADDRESS_LINE1,A.ADDRESS_LINE2,A.ADDRESS_LINE3,A.ADDRESS_LINE4,A.GEO_UNIT_ID FROM ADDRESSDB A ,ADDRESSTYPE T,COUNTRY C WHERE(A.ADDRESS_LINE1 LIKE ? or A.ADDRESS_LINE2 LIKE ? or A.ADDRESS_LINE3 LIKE ? or A.ADDRESS_LINE4 LIKE ?) AND A.ADDR_TYPE=T.ADDR_TYPE  AND A.COUNTRY_CODE=C.COUNTRY_CODE)S";
			} else if (searchType.equals("PC")) {
				sqlQuery = "SELECT S.ADDR_INV_NO, S.ADDR_TYPE,S.ADDRDESC,S.COUNTRY_CODE,S.COUNTRYDESC,S.ADDRESS_LINE1,S.ADDRESS_LINE2,S.ADDRESS_LINE3,S.ADDRESS_LINE4,S.GEO_UNIT_ID,(SELECT COUNT(1) FROM PERSONADDRDB P WHERE P.PARTITION_NO=? AND P.ADDR_INV_NO = S.ADDR_INV_NO),S.GEO_UNIT_STRUC_CODE FROM(SELECT A.ADDR_INV_NO ,A.ADDR_TYPE,T.DESCRIPTION ADDRDESC,A.COUNTRY_CODE,C.DESCRIPTION COUNTRYDESC,C.GEO_UNIT_STRUC_CODE,A.ADDRESS_LINE1,A.ADDRESS_LINE2,A.ADDRESS_LINE3,A.ADDRESS_LINE4,A.GEO_UNIT_ID  FROM   ADDRESSDB A ,ADDRESSTYPE T,COUNTRY C WHERE  A.GEO_UNIT_ID=?  AND A.ADDR_TYPE=T.ADDR_TYPE AND  A.COUNTRY_CODE=C.COUNTRY_CODE )S ";
			} else {
				sqlQuery = "SELECT A.ADDR_INV_NO , A.ADDR_TYPE,T.DESCRIPTION,A.COUNTRY_CODE, C.DESCRIPTION,A.ADDRESS_LINE1,A.ADDRESS_LINE2, A.ADDRESS_LINE3,A.ADDRESS_LINE4,A.GEO_UNIT_ID,(SELECT COUNT(1) FROM PERSONADDRDB D WHERE D.PARTITION_NO=P.PARTITION_NO AND D.ADDR_INV_NO = P.ADDR_INV_NO),C.GEO_UNIT_STRUC_CODE FROM  PERSONADDRDB P , ADDRESSDB A ,ADDRESSTYPE T,COUNTRY C WHERE  P.PARTITION_NO =? AND P.PERSON_ID=?  AND P.REMOVED_ON_DATE IS NULL AND P.ADDR_INV_NO=A.ADDR_INV_NO AND  A.ADDR_TYPE=T.ADDR_TYPE AND A.COUNTRY_CODE=C.COUNTRY_CODE ";
			}
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, context.getPartitionNo());
			if (searchType.equals("AP")) {
				util.setString(2, "%" + searchContent + "%");
				util.setString(3, "%" + searchContent + "%");
				util.setString(4, "%" + searchContent + "%");
				util.setString(5, "%" + searchContent + "%");
			} else {
				util.setString(2, searchContent);
			}
			ResultSet rset = util.executeQuery();
			DHTMLXGridUtility gridUtility = new DHTMLXGridUtility();
			gridUtility.init();
			while (rset.next()) {
				gridUtility.startRow(rset.getString(1));
				gridUtility.setCell("0");
				gridUtility.setCell(rset.getString(1));
				gridUtility.setCell(rset.getString(2));
				gridUtility.setCell(rset.getString(3));
				gridUtility.setCell(rset.getString(4));
				gridUtility.setCell(rset.getString(5));
				gridUtility.setCell(rset.getString(6) + " , " + rset.getString(7) + " , " + rset.getString(8) + " , " + rset.getString(9));
				String location = RegularConstants.EMPTY_STRING;
				if (rset.getString(12) != null) {
					sqlQuery = "CALL SP_GET_GUS_STRUC_NAME(?,?,?,?)";
					dbutil.reset();
					dbutil.setMode(DBUtil.CALLABLE);
					dbutil.setSql(sqlQuery);
					dbutil.setString(1, rset.getString(12));
					dbutil.registerOutParameter(2, Types.VARCHAR);
					dbutil.registerOutParameter(3, Types.VARCHAR);
					dbutil.registerOutParameter(4, Types.VARCHAR);
					dbutil.execute();
					if (dbutil.getString(4) == null) {
						resultDTO.set("V_FIRST_GUS", dbutil.getString(2));
						resultDTO.set("V_GUS_STRUCTURE", dbutil.getString(3));
						resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
						String datavalue[] = dbutil.getString(3).split(";");
						for (int count = 0; count < datavalue.length; count++) {
							location = location + datavalue[count] + "\n";
						}
					}
				}
				gridUtility.setCell(location);
				gridUtility.setCell("VIEW^javascript:viewAddress(\"" + rset.getString(1) + "\")");
				if (rset.getInt(11) > 1) {
					gridUtility.setCell("Currently used by " + rset.getInt(11) + " other Persons");
				} else {
					gridUtility.setCell("");
				}
				gridUtility.endRow();
			}
			gridUtility.finish();
			String resultXML = gridUtility.getXML();
			resultDTO.set(ContentManager.RESULT_XML, resultXML);
			resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return resultDTO;
	}

	public DTObject loadAddressDetails(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		String invNumber = inputDTO.get("INVENTORY_NUMBER");
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = getDbContext().createUtilInstance();
		String sqlQuery = null;
		try {
			sqlQuery = "SELECT A.ADDR_INV_NO,A.ADDR_TYPE,A.COUNTRY_CODE,A.ADDRESS_LINE1,A.ADDRESS_LINE2,A.ADDRESS_LINE3,A.ADDRESS_LINE4,A.GEO_UNIT_ID FROM ADDRESSDB A WHERE A.PARTITION_NO=? AND A.ADDR_INV_NO=?";
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, context.getPartitionNo());
			util.setString(2, invNumber);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				resultDTO.set("ADDR_INV_NO", rset.getString(1));
				resultDTO.set("ADDR_TYPE", rset.getString(2));
				resultDTO.set("COUNTRY_CODE", rset.getString(3));
				resultDTO.set("ADDRESS_LINE1", rset.getString(4));
				resultDTO.set("ADDRESS_LINE2", rset.getString(5));
				resultDTO.set("ADDRESS_LINE3", rset.getString(6));
				resultDTO.set("ADDRESS_LINE4", rset.getString(7));
				resultDTO.set("GEO_UNIT_ID", rset.getString(8));
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return resultDTO;
	}

	public DTObject getInstallLabDetails(DTObject input) {
		DTObject result = new DTObject();
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = dbContext.createUtilInstance();
		String sql = "SELECT MODULE_LIVE,LIVE_FIRST_REPORT_NUMBER,DEF_REPORT_PREFIX_CODE,DEF_DEPT_CODE,DEF_PRINT_TYPE FROM INSTALL_LAB WHERE ENTITY_CODE = ? ";
		try {
			util.setMode(DBUtil.PREPARED);
			util.setSql(sql);
			util.setString(1, input.get("CODE"));
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				decodeRow(rset, result);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			util.reset();
		}
		return result;
	}

	public DTObject getRegPolicyDetails() {
		DTObject result = new DTObject();
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = dbContext.createUtilInstance();
		String sql = "SELECT * FROM REGPOLICY WHERE ENTITY_CODE=?";
		try {
			util.setMode(DBUtil.PREPARED);
			util.setSql(sql);
			util.setString(1, getContext().getEntityCode());
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				decodeRow(rset, result);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			util.reset();
		}
		return result;
	}

	public final DTObject getLinkProgramDetails(DTObject input) {
		DTObject result = new DTObject();
		String parentProgram = input.get("PARENT_PGM_ID");
		String linkProgram = input.get("LINKED_PGM_ID");
		DBUtil util = getDbContext().createUtilInstance();
		try {
			String sqlQuery = "SELECT RETURN_TYPE,AUDIT_REQD FROM MPGMLINKCOMP WHERE MPGM_ID = ? AND LINK_PGM_ID=?";
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, parentProgram);
			util.setString(2, linkProgram);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				result.set("RETURN_TYPE", rset.getString("RETURN_TYPE"));
				result.set("AUDIT_REQD", rset.getString("AUDIT_REQD"));
			} else {
				result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return result;
	}

	public DTObject getContactDetailGrid(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		String personID = inputDTO.get("PERSON_ID");
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = getDbContext().createUtilInstance();
		String sqlQuery = null;
		try {
			sqlQuery = "SELECT IF(C.CONTACT_TYPE ='L','Landline',IF(C.CONTACT_TYPE ='M','Mobile','E-Mail')),FN_GETCONTACT_DETAILS(C.CONTACT_TYPE,C.LL_COUNTRY_CODE,C.LL_LOC_STD_CODE,C.LL_CONTACT_NUMBER,C.MOB_COUNTRY_CODE,C.MOB_CONTACT_NUMBER,C.EMAIL_ID) CONTACT_DETAIL,IF(P.OFFICIAL_USE='1','Yes','No'),C.CONTACT_INV_NO,P.ADDED_ON_DATE,C.CONTACT_TYPE FROM CONTACTSDB C, PERSONCONTDB P WHERE P.ENTITY_CODE=? AND P.PERSON_ID=? AND P.CONTACT_INV_NO=C.CONTACT_INV_NO AND P.REMOVED_ON_DATE IS NULL";
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, context.getEntityCode());
			util.setString(2, personID);
			ResultSet rset = util.executeQuery();
			DHTMLXGridUtility gridUtility = new DHTMLXGridUtility();
			gridUtility.init();
			SimpleDateFormat sdf = new SimpleDateFormat(BackOfficeFormatUtils.getSimpleDateFormat(getContext().getDateFormat()));
			while (rset.next()) {
				gridUtility.startRow(rset.getString(4));
				gridUtility.setCell(rset.getString(6));
				gridUtility.setCell(rset.getString(1));
				gridUtility.setCell(rset.getString(2));
				gridUtility.setCell(rset.getString(3));
				gridUtility.setCell(rset.getString(4));
				gridUtility.setCell(sdf.format(rset.getDate(5)));
				gridUtility.setCell("EDIT^javascript:editContact(\"" + rset.getString(4) + "\")");
				gridUtility.setCell("REMOVE^javascript:removeContact(\"" + rset.getString(4) + "\")");
				gridUtility.endRow();
			}
			gridUtility.finish();
			String resultXML = gridUtility.getXML();
			resultDTO.set(ContentManager.RESULT_XML, resultXML);
			resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return resultDTO;
	}

	public DTObject getSearchContactDetail(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		String searchContent = inputDTO.get("SEARCH_CONTENT");
		String searchType = inputDTO.get("SEARCH_TYPE");
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = getDbContext().createUtilInstance();
		String sqlQuery = null;
		try {
			if (searchType.equals("T")) {
				sqlQuery = "SELECT C.CONTACT_TYPE,FN_GETCONTACT_DETAILS(C.CONTACT_TYPE,C.LL_COUNTRY_CODE,C.LL_LOC_STD_CODE,C.LL_CONTACT_NUMBER,C.MOB_COUNTRY_CODE,C.MOB_CONTACT_NUMBER,C.EMAIL_ID) CONTACT_DETAIL,P.OFFICIAL_USE,C.CONTACT_INV_NO,(SELECT COUNT(*) FROM PERSONCONTDB WHERE C.CONTACT_INV_NO=CONTACT_INV_NO) PERSON_USED FROM CONTACTDB C, PERSONCONTDB P WHERE P.PARTITION_NO=? AND (C.LL_CONTACT_NUMBER=? OR C.MOB_CONTACT_NUMBER=?) AND P.CONTACT_INV_NO=C.CONTACT_INV_NO AND P.REMOVED_ON_DATE IS NULL";
			} else if (searchType.equals("E")) {
				sqlQuery = "SELECT 'E',FN_GETCONTACT_DETAILS(C.CONTACT_TYPE,C.LL_COUNTRY_CODE,C.LL_LOC_STD_CODE,C.LL_CONTACT_NUMBER,C.MOB_COUNTRY_CODE,C.MOB_CONTACT_NUMBER,C.EMAIL_ID) CONTACT_DETAIL,P.OFFICIAL_USE,C.CONTACT_INV_NO,(SELECT COUNT(*) FROM PERSONCONTDB WHERE C.CONTACT_INV_NO=CONTACT_INV_NO) PERSON_USED FROM CONTACTDB C, PERSONCONTDB P WHERE P.PARTITION_NO=? AND C.EMAIL_ID=? AND P.CONTACT_INV_NO=C.CONTACT_INV_NO AND P.REMOVED_ON_DATE IS NULL";
			} else {
				sqlQuery = "SELECT C.CONTACT_TYPE,FN_GETCONTACT_DETAILS(C.CONTACT_TYPE,C.LL_COUNTRY_CODE,C.LL_LOC_STD_CODE,C.LL_CONTACT_NUMBER,C.MOB_COUNTRY_CODE,C.MOB_CONTACT_NUMBER,C.EMAIL_ID) CONTACT_DETAIL,P.OFFICIAL_USE,C.CONTACT_INV_NO,(SELECT COUNT(*) FROM PERSONCONTDB WHERE PARTITION_NO=p.PARTITION_NO AND CONTACT_INV_NO=P.CONTACT_INV_NO) PERSON_USED FROM CONTACTDB C, PERSONCONTDB P WHERE P.PARTITION_NO=? AND P.PERSON_ID=? AND P.CONTACT_INV_NO=C.CONTACT_INV_NO AND P.REMOVED_ON_DATE IS NULL";
			}
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, context.getPartitionNo());
			if (searchType.equals("T")) {
				util.setString(2, searchContent);
				util.setString(3, searchContent);
			} else {
				util.setString(2, searchContent);
			}
			ResultSet rset = util.executeQuery();
			DHTMLXGridUtility gridUtility = new DHTMLXGridUtility();
			gridUtility.init();
			while (rset.next()) {
				gridUtility.startRow(rset.getString(4));
				gridUtility.setCell("0");
				gridUtility.setCell(rset.getString(1));
				gridUtility.setCell(rset.getString(2));
				gridUtility.setCell(rset.getString(3));
				gridUtility.setCell(rset.getString(4));

				if (rset.getInt(5) > 1) {
					gridUtility.setCell("Currently used by " + rset.getInt(5) + "other Persons");
				} else {
					gridUtility.setCell("");
				}

				gridUtility.setCell("VIEW^javascript:viewContact(\"" + rset.getString(4) + "\")");
				gridUtility.endRow();
			}
			gridUtility.finish();
			String resultXML = gridUtility.getXML();
			resultDTO.set(ContentManager.RESULT_XML, resultXML);
			resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return resultDTO;
	}

	public final DTObject validateContactInventoryNumber(DTObject input) {
		DTObject result = new DTObject();
		String value = input.get("INV_NO");
		DBUtil util = getDbContext().createUtilInstance();
		try {
			String sqlQuery = "SELECT * FROM CONTACTSDB WHERE CONTACT_INV_NO = ?";
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, value);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
			} else {
				result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return result;
	}

	public final boolean validatePersonContactNumber(String personId, String inventoryNumber) {
		DBUtil util = getDbContext().createUtilInstance();
		try {
			String sqlQuery = "SELECT * FROM PERSONCONTDB WHERE ENTITY_CODE=? AND PERSON_ID=? AND CONTACT_INV_NO = ?";
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, getContext().getEntityCode());
			util.setString(2, personId);
			util.setString(3, inventoryNumber);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			util.reset();
		}
		return false;
	}

	public DTObject loadContactDetails(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		String invNumber = inputDTO.get("INVENTORY_NUMBER");
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = getDbContext().createUtilInstance();
		String sqlQuery = null;
		try {
			sqlQuery = "SELECT C.CONTACT_TYPE,C.LL_COUNTRY_CODE,C.LL_LOC_STD_CODE,C.LL_CONTACT_NUMBER,C.MOB_COUNTRY_CODE,C.MOB_CONTACT_NUMBER,C.EMAIL_ID,P.OFFICIAL_USE,(SELECT COUNT(*) FROM PERSONCONTDB WHERE C.CONTACT_INV_NO=CONTACT_INV_NO) PERSON_USED FROM CONTACTDB C, PERSONCONTDB P WHERE P.PARTITION_NO=? AND C.CONTACT_INV_NO=? AND P.CONTACT_INV_NO=C.CONTACT_INV_NO AND P.REMOVED_ON_DATE IS NULL";
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, context.getPartitionNo());
			util.setString(2, invNumber);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				resultDTO.set("CONTACT_TYPE", rset.getString(1));
				resultDTO.set("LL_CNTRY_CODE", rset.getString(2));
				resultDTO.set("LL_STD_CODE", rset.getString(3));
				resultDTO.set("LL_NUMBER", rset.getString(4));
				resultDTO.set("MOB_CNTRY_CODE", rset.getString(5));
				resultDTO.set("MOB_NUMBER", rset.getString(6));
				resultDTO.set("EMAIL_ID", rset.getString(7));
				resultDTO.set("OFFICIAL", rset.getString(8));
				resultDTO.set("DETAIL", rset.getString(9));
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return resultDTO;
	}

	public final DTObject validPersonOthinfo(DTObject input) {
		DTObject result = new DTObject();
		String value = input.get(ContentManager.CODE);
		String column = input.get(ContentManager.COLUMN);
		String fetch = input.get(ContentManager.FETCH);
		String sqlQuery = null;
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = getDbContext().createUtilInstance();
		String fetchColumns = input.get(ContentManager.FETCH_COLUMNS);
		try {
			if (fetch != null && fetch.equals(ContentManager.FETCH_ALL)) {
				sqlQuery = "SELECT * FROM PERSONOTHINFO WHERE  PARTITION_NO = ? AND " + column + " = ?  ";
			} else {
				sqlQuery = "SELECT " + fetchColumns + " FROM  PERSONOTHINFO WHERE PARTITION_NO = ? AND " + column + " = ? ";

			}
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, context.getPartitionNo());
			util.setString(2, value);

			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				decodeRow(rset, result);
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
			} else {
				result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return result;
	}

	public final DTObject validatePartitionWiseCodes(DTObject input) {
		DTObject result = new DTObject();
		String value = input.get(ContentManager.CODE);
		String code = input.get(ContentManager.COLUMN);
		String table = input.get(ContentManager.TABLE);
		String fetch = input.get(ContentManager.FETCH);
		String fetchColumns = input.get(ContentManager.FETCH_COLUMNS);
		String sqlQuery = null;
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = getDbContext().createUtilInstance();
		String action = input.get(ContentManager.USER_ACTION);
		boolean usageAllowed = false;
		try {
			if (action.equals(USAGE)) {
				usageAllowed = getProgramUsageAllowed(input);
			}
			if (fetch != null && fetch.equals(ContentManager.FETCH_ALL)) {
				sqlQuery = "SELECT * FROM " + table + " WHERE PARTITION_NO = ? AND " + code + " = ?";
			} else {
				sqlQuery = "SELECT " + fetchColumns + " ,ENABLED,TBA_KEY FROM " + table + " WHERE PARTITION_NO = ? AND " + code + " = ?";
			}
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, getContext().getPartitionNo());
			util.setString(2, value);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				decodeRow(rset, result);
				String enabled = rset.getString("ENABLED");
				if (enabled != null && enabled.equals(RegularConstants.COLUMN_DISABLE)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.CODE_CLOSED);
				}
				if (usageAllowed) {
					if (action.equals(USAGE)) {
						if (rset.getString("TBA_KEY") != null) {
							result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_USAGE_NOT_ALLOWED);
						}
					}
				}

			} else {
				result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return result;
	}

	public final DTObject validateDemand(DTObject input) {
		DTObject result = new DTObject();
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		String demandDate = input.get("DEMAND_DATE");
		String demandSerial = input.get("DEMAND_DAY_SL");
		String fetch = input.get(ContentManager.FETCH);
		String fetchColumns = input.get(ContentManager.FETCH_COLUMNS);
		StringBuilder sqlQuery = new StringBuilder(RegularConstants.EMPTY_STRING);
		DBUtil util = getDbContext().createUtilInstance();
		try {
			if (fetch != null && fetch.equals(ContentManager.FETCH_ALL)) {
				sqlQuery.append("SELECT * FROM DEMAND WHERE ENTITY_CODE = ? AND DEMAND_DATE = ? AND DEMAND_DAY_SL = ?");
			} else {
				sqlQuery.append("SELECT ").append(fetchColumns).append(" FROM DEMAND WHERE ENTITY_CODE=? AND DEMAND_DATE =? AND DEMAND_DAY_SL=?");
			}
			util.reset();
			util.setSql(sqlQuery.toString());
			util.setInt(1, Integer.parseInt(getContext().getEntityCode()));
			util.setDate(2, new java.sql.Date(BackOfficeFormatUtils.getDate(demandDate, getContext().getDateFormat()).getTime()));
			util.setInt(3, Integer.parseInt(demandSerial));
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				decodeRow(rset, result);
			} else {
				result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return result;
	}

	public boolean isUserCashierAtCashCounter() {
		DBUtil util = getDbContext().createUtilInstance();
		boolean cashierAtcashCounter = false;
		try {
			String sqlquery = "SELECT UP.USER_ROLE_CASHIER_AT_CC FROM USERROLESPLATTR UP JOIN USERSROLEALLOC U ON(UP.ENTITY_CODE=U.ENTITY_CODE AND UP.ROLE_CODE=U.ROLE_CODE AND U.USER_ID=?) WHERE UP.ENTITY_CODE=?";
			util.reset();
			util.setSql(sqlquery);
			util.setString(1, getContext().getUserID());
			util.setString(2, getContext().getEntityCode());

			ResultSet resutset = util.executeQuery();
			if (resutset.next()) {
				if (resutset.getString(1).equals(RegularConstants.ONE)) {
					cashierAtcashCounter = true;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			util.reset();
		}
		return cashierAtcashCounter;
	}

	public boolean isDemandAmountPaid(DTObject inputDTO) throws Exception {
		DTObject resultDTO = new DTObject();
		String amountPaid = null;
		try {
			inputDTO.set(ContentManager.FETCH_COLUMNS, "AMOUNT_PAID");
			resultDTO = validateDemand(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				throw new Exception();
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
				amountPaid = resultDTO.get("AMOUNT_PAID");
				if (isAmountGreater(amountPaid, RegularConstants.ZERO)) {
					return true;
				}
			} else {
				throw new Exception();
			}
		} catch (Exception e) {
			throw new Exception();
		}
		return false;
	}

	public final DTObject roundOff(DTObject input) {
		DTObject result = new DTObject();
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		String amount = input.get("AMOUNT");
		String roundOffFactor = input.get("RND_FACT");
		String roundOffChoice = input.get("RND_CHOICE");
		DBUtil dbUtil = getDbContext().createUtilInstance();
		try {
			dbUtil.reset();
			dbUtil.setMode(DBUtil.PREPARED);
			dbUtil.setSql("SELECT FN_ROUNDOFF(?,?,?,?)");
			dbUtil.setString(1, getContext().getPartitionNo());
			dbUtil.setString(2, amount);
			dbUtil.setString(3, roundOffChoice);
			dbUtil.setString(4, roundOffFactor);
			ResultSet rs = dbUtil.executeQuery();
			if (rs.next()) {
				result.set("RND_AMT", rs.getString(1));
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		}
		return result;
	}

	public final long getNoOfDays(String value1, String value2) {
		Date date1 = isValidDate(value1);
		Date date2 = isValidDate(value2);
		long noOfDays = BackOfficeDateUtils.getNoOfDays(date1, date2);
		return noOfDays;
	}

	public DTObject getSirmRates(DTObject inputDTO) {
		DTObject result = new DTObject();
		DBUtil util = getDbContext().createUtilInstance();
		String error = null;
		try {
			util.reset();
			util.setMode(DBUtil.CALLABLE);
			String sql = "CALL SP_GET_SIRM_RATES(?,?,?,?,?,?,?,?,?)";
			util.setSql(sql);
			util.setString(1, inputDTO.get("ENTITY_CODE"));
			util.setString(2, inputDTO.get("CURR_CODE"));
			util.setString(3, inputDTO.get("SIRM_CODE"));
			util.setString(4, inputDTO.get("ORG_CODE"));
			util.setString(5, inputDTO.get("TARIFF_CODE"));
			util.setString(6, inputDTO.get("ROOM_TYPE_CODE"));
			util.registerOutParameter(7, Types.DECIMAL);
			util.registerOutParameter(8, Types.CHAR);
			util.registerOutParameter(9, Types.VARCHAR);
			util.execute();
			error = util.getString(9);
			if (error.equals(RegularConstants.SP_SUCCESS)) {
				result.set("SIRM_RATE", util.getString(7));
				result.set("SIRM_OUTPUT", util.getString(8));
				// result.set("ERROR", util.getString(9));
			} else {
				result.set("ERROR", util.getString(9));
			}
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return result;
	}

	public DTObject fetchTariffGradeCode(DTObject inputDTO) {
		DTObject result = new DTObject();
		DBUtil util = getDbContext().createUtilInstance();
		StringBuffer sqlQuery = new StringBuffer();
		try {
			sqlQuery.append("SELECT TARIFF_GRADE_CODE,DESCRIPTION FROM TARIFFGRADES WHERE ENTITY_CODE = ? AND  ");
			if (inputDTO.get("ROOM_TYPE_CODE") != null)
				sqlQuery.append(" ROOM_TYPE_CODE = ? ");
			else
				sqlQuery.append(" ROOM_TYPE_CODE IS NULL ");
			if (inputDTO.get("PATIENT_CAT_CODE") != null)
				sqlQuery.append(" AND PATIENT_CAT_CODE = ? ");
			else
				sqlQuery.append(" AND PATIENT_CAT_CODE IS NULL ");
			sqlQuery.append(" AND ENABLED ='1' ");
			util.reset();
			util.setMode(DBUtil.PREPARED);
			util.setSql(sqlQuery.toString());
			util.setInt(1, Integer.parseInt(context.getEntityCode()));

			if (inputDTO.get("ROOM_TYPE_CODE") != null && inputDTO.get("PATIENT_CAT_CODE") != null) {
				util.setString(2, inputDTO.get("ROOM_TYPE_CODE"));
				util.setString(3, inputDTO.get("PATIENT_CAT_CODE"));
			} else if (inputDTO.get("ROOM_TYPE_CODE") == null && inputDTO.get("PATIENT_CAT_CODE") != null) {
				util.setString(2, inputDTO.get("PATIENT_CAT_CODE"));
			} else if (inputDTO.get("ROOM_TYPE_CODE") != null && inputDTO.get("PATIENT_CAT_CODE") == null) {
				util.setString(2, inputDTO.get("ROOM_TYPE_CODE"));
			}
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set("TARIFF_GRADE_CODE", rset.getString(1));
				result.set("DESCRIPTION", rset.getString(2));
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				return result;
			} else {
				result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
				return result;
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return result;
	}

	// ARIBALA ADDED ON 09102015 BEGIN
	public String getProgramType(String programID) {
		String result = "";
		DBUtil util = dbContext.createUtilInstance();
		String sql = "SELECT MPGM_TYPE FROM MPGMCONFIG WHERE MPGM_ID=?";
		try {
			util.setMode(DBUtil.PREPARED);
			util.setSql(sql);
			util.setString(1, programID);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result = rset.getString("MPGM_TYPE");
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			util.reset();
		}
		return result;
	}

	// ARIBALA ADDED ON 09102015 END

	public final boolean isValidTwelveDigit(String number) {
		if (number == null || number.length() == LENGTH_0 || number.length() > LENGTH_12)
			return false;
		if (!isValidNumber(number))
			return false;
		return true;
	}

	public DTObject getOPNumber(DTObject inputDTO) {
		DTObject resultDTObject = new DTObject();
		resultDTObject.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = dbContext.createUtilInstance();
		String sql = "SELECT OP_YEAR,OP_NUMBER FROM AGCOP WHERE ENTITY_CODE=? AND OP_YEAR=? AND PATIENT_REG_YEAR=? AND PATIENT_REG_SL=?";
		try {
			util.setMode(DBUtil.PREPARED);
			util.setSql(sql);
			util.setInt(1, Integer.parseInt(getContext().getEntityCode()));
			util.setInt(2, Integer.parseInt(inputDTO.get("OP_YEAR")));
			util.setInt(3, Integer.parseInt(inputDTO.get("PATIENT_REG_YEAR")));
			util.setInt(4, Integer.parseInt(inputDTO.get("PATIENT_REG_SL")));
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				resultDTObject.set("OP_YEAR", rset.getString("OP_YEAR"));
				resultDTObject.set("OP_NUMBER", rset.getString("OP_NUMBER"));
				resultDTObject.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTObject.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return resultDTObject;
	}

	public final boolean isUnAuthorizedRecordExist(String tableName, Map<Integer, DBEntry> whereClauseFieldsMap) throws Exception {
		DBUtil dbutil = getDbContext().createUtilInstance();
		boolean available = false;
		ResultSet rset = null;
		StringBuffer sqlBuffer = new StringBuffer(160);
		sqlBuffer.append("SELECT COUNT(1) FROM ");
		sqlBuffer.append(tableName);
		sqlBuffer.append(" WHERE E_STATUS IS NULL AND ");
		TreeSet<Integer> orderedSet = new TreeSet<Integer>(whereClauseFieldsMap.keySet());
		for (Integer fieldIndex : orderedSet) {
			DBEntry entry = whereClauseFieldsMap.get(fieldIndex);
			sqlBuffer.append(entry.getName());
			sqlBuffer.append(" = ?");
			sqlBuffer.append(" AND ");
		}
		String sql = sqlBuffer.substring(0, sqlBuffer.lastIndexOf(" AND "));
		try {

			dbutil.reset();
			dbutil.setMode(DBUtil.PREPARED);
			dbutil.setSql(sql);
			for (Map.Entry<Integer, DBEntry> mapEntry : whereClauseFieldsMap.entrySet()) {
				Integer fieldIndex = mapEntry.getKey();
				DBEntry entry = mapEntry.getValue();
				Object value = entry.getValue();
				switch (entry.getType()) {
				case VARCHAR:
					dbutil.setString(fieldIndex, (String) value);
					break;
				case DATE:
					if (value instanceof java.sql.Date) {
						dbutil.setDate(fieldIndex, (java.sql.Date) value);
					} else if (value instanceof java.util.Date) {
						dbutil.setDate(fieldIndex, new java.sql.Date(((java.util.Date) value).getTime()));
					} else {
						SimpleDateFormat sdf = new SimpleDateFormat(getContext().getDateFormat());
						java.sql.Date date = null;
						try {
							date = new java.sql.Date(sdf.parse((String) value).getTime());
						} catch (ParseException e) {
							e.printStackTrace();
						}
						dbutil.setDate(fieldIndex, date);
					}
					break;
				case TIMESTAMP:
					dbutil.setTimestamp(fieldIndex, (Timestamp) value);
					break;
				case INTEGER:
					dbutil.setInt(fieldIndex, (Integer) value);
					break;
				case LONG:
					dbutil.setLong(fieldIndex, (Long) value);
					break;
				case BIGDECIMAL:
					dbutil.setBigDecimal(fieldIndex, (BigDecimal) value);
					break;
				default:
					break;
				}
			}
			rset = dbutil.executeQuery();
			if (rset.next()) {
				int count = rset.getInt(1);
				if (count > 0)
					available = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new Exception();
		} finally {
			if (dbutil != null)
				dbutil.reset();
		}
		return available;
	}

	public int getFinYear(String date, int sof) {
		String month = date.substring(3, 5);
		if (Integer.parseInt(month) < sof)
			return Integer.parseInt(date.substring(6, date.length())) - 1;
		else
			return Integer.parseInt(date.substring(6, date.length()));
	}

	public final boolean isValidThreeDigitFiveDecimal(String value) {
		try {
			BigDecimal amountValue = new BigDecimal(value);
			if (amountValue.precision() > 8 || amountValue.scale() > 5 || (amountValue.precision() - amountValue.scale()) > 3) {
				return false;
			}
			if (amountValue.compareTo(smallAmount) > 0) {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}

	public final boolean isValidThreeDigitOneDecimal(String value) {
		if (value == null || value.length() == 0)
			return false;
		String regex = "^(\\d{1,3})(([\\.])([0,5]))?$";
		if (value.matches(regex))
			return true;
		return false;
	}

	public final boolean isValidSixDigitTwoDecimal(String value) {
		try {
			BigDecimal amountValue = new BigDecimal(value);
			if (amountValue.precision() > 8 || amountValue.scale() > 2 || (amountValue.precision() - amountValue.scale()) > 6) {
				return false;
			}
			if (amountValue.compareTo(smallAmount) > 0) {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}

	public final boolean isValidThreeDigitTwoDecimal(String value) {
		try {
			BigDecimal amountValue = new BigDecimal(value);
			if (amountValue.precision() > 5 || amountValue.scale() > 2 || (amountValue.precision() - amountValue.scale()) > 3) {
				return false;
			}
			if (amountValue.compareTo(smallAmount) > 0) {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}

	public final boolean isSevenDigitThreeDecimal(String value, int scale) {
		try {
			BigDecimal amountValue = new BigDecimal(value);
			if (amountValue.precision() > 10 || amountValue.scale() > 3 || (amountValue.precision() - amountValue.scale()) > 7) {
				return false;
			}
			if (amountValue.compareTo(smallAmount) > 0) {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}

	public final DTObject validateReportDateRangeParam(DTObject inputDTO) {
		DTObject result = new DTObject();
		Date fromDate = BackOfficeFormatUtils.getDate(inputDTO.get("FROM_DATE"), getContext().getDateFormat());
		Date toDate = BackOfficeFormatUtils.getDate(inputDTO.get("TO_DATE"), getContext().getDateFormat());
		int month1 = fromDate.getYear() * 12 + fromDate.getMonth();
		int month2 = toDate.getYear() * 12 + toDate.getMonth();
		int monthDiff = month2 - month1;
		DBUtil util = getDbContext().createUtilInstance();
		try {
			String sqlQuery = "SELECT TO_CHAR(FROMDATE_LOWER_LIMIT,?) LOWER_LIMIT,FROMDATE_LOWER_LIMIT,NO_OF_MONTHS_ALLOWED FROM REPORTDATERANGEPARAM WHERE ENTITY_CODE = ? AND PROGRAM_ID	= ?";
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, getContext().getDateFormat());
			util.setString(2, getContext().getEntityCode());
			util.setString(3, inputDTO.get("PROGRAM_ID"));
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				if (rset.getString("FROMDATE_LOWER_LIMIT") != null) {
					if (!isDateGreaterThanEqual(inputDTO.get("FROM_DATE"), rset.getString("LOWER_LIMIT"))) {
						Object[] errorParam = { rset.getString("LOWER_LIMIT") };
						result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_DATE_RANGE_LOWER_LIMIT);
						result.setObject(ContentManager.ERROR_PARAM, errorParam);
						return result;
					}
					if (rset.getString("NO_OF_MONTHS_ALLOWED") != null) {
						if (monthDiff > rset.getInt("NO_OF_MONTHS_ALLOWED")) {
							Object[] errorParam = { rset.getInt("NO_OF_MONTHS_ALLOWED") };
							result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_DATE_RANGE_DIFFERENCE);
							result.setObject(ContentManager.ERROR_PARAM, errorParam);
							return result;
						}
					}
				}
				return result;
			}
			sqlQuery = "SELECT TO_CHAR(FROMDATE_LOWER_LIMIT,?) LOWER_LIMIT,FROMDATE_LOWER_LIMIT,NO_OF_MONTHS_ALLOWED FROM REPORTDATERANGEPARAM WHERE ENTITY_CODE = ? AND PROGRAM_ID='COMMON' ";
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, getContext().getDateFormat());
			util.setString(2, getContext().getEntityCode());
			rset = util.executeQuery();
			if (rset.next()) {
				if (!isDateGreaterThanEqual(fromDate, rset.getDate("FROMDATE_LOWER_LIMIT"))) {
					Object[] errorParam = { rset.getString("LOWER_LIMIT") };
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_DATE_RANGE_LOWER_LIMIT);
					result.setObject(ContentManager.ERROR_PARAM, errorParam);
					return result;
				}
				if (rset.getString("NO_OF_MONTHS_ALLOWED") != null) {
					if (monthDiff > rset.getInt("NO_OF_MONTHS_ALLOWED")) {
						Object[] errorParam = { rset.getInt("NO_OF_MONTHS_ALLOWED") };
						result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_DATE_RANGE_DIFFERENCE);
						result.setObject(ContentManager.ERROR_PARAM, errorParam);
						return result;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return result;
	}

	public final DTObject validateEntityWiseAlphaCodes(DTObject input) {
		DTObject result = new DTObject();
		String value = input.get(ContentManager.CODE);
		String entityCode = input.get(ContentManager.ENTITY_CODE);
		String code = input.get(ContentManager.COLUMN);
		String alphaCode = input.get(ContentManager.ALPHA_CODE);
		String table = input.get(ContentManager.TABLE);
		String fetch = input.get(ContentManager.FETCH);
		String fetchColumns = input.get(ContentManager.FETCH_COLUMNS);
		String sqlQuery = null;
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = getDbContext().createUtilInstance();
		String action = input.get(ContentManager.USER_ACTION);
		boolean usageAllowed = false;
		try {
			if (action.equals(USAGE)) {
				usageAllowed = getProgramUsageAllowed(input);
			}
			if (fetch != null && fetch.equals(ContentManager.FETCH_ALL)) {
				sqlQuery = "SELECT * FROM " + table + " WHERE " + entityCode + " = ? AND ( CAST(" + code + " AS CHAR) = ? OR " + alphaCode + " = ?)";
			} else {
				sqlQuery = "SELECT " + fetchColumns + " ," + code + " ,ENABLED,TBA_KEY FROM " + table + " WHERE " + entityCode + " = ? AND ( CAST(" + code + " AS CHAR) = ? OR " + alphaCode + " = ?)";
			}
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, getContext().getEntityCode());
			util.setString(2, value);
			util.setString(3, value);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				decodeRow(rset, result);
				String enabled = rset.getString("ENABLED");
				if (enabled != null && enabled.equals(RegularConstants.COLUMN_DISABLE)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.CODE_CLOSED);
				}
				if (usageAllowed) {
					if (action.equals(USAGE)) {
						if (rset.getString("TBA_KEY") != null) {
							result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_USAGE_NOT_ALLOWED);
						}
					}
				}

			} else {
				result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return result;
	}

	public final boolean isValidFileFormat(String fileExtension, String fileFormat) {
		try {
			String[] extensions = { fileExtension };
			if (fileExtension.contains(",")) {
				extensions = fileExtension.split("\\,");
			}
			for (int i = 0; i < extensions.length; i++) {
				if (fileFormat.equalsIgnoreCase(extensions[i])) {
					return true;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return false;
	}

	public final DTObject validTransactionalGL(DTObject input) {
		DTObject result = new DTObject();
		String value = input.get(ContentManager.CODE);
		String column = input.get(ContentManager.COLUMN);
		String fetch = input.get(ContentManager.FETCH);
		String partitionNo = input.get(ContentManager.PARTITION_NO);
		String sqlQuery = null;
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = getDbContext().createUtilInstance();
		String action = input.get(ContentManager.USER_ACTION);
		String fetchColumns = input.get(ContentManager.FETCH_COLUMNS);
		boolean usageAllowed = false;
		try {
			if (action.equals(USAGE)) {
				usageAllowed = getProgramUsageAllowed(input);
			}
			if (fetch != null && fetch.equals(ContentManager.FETCH_ALL)) {
				sqlQuery = "SELECT *,TO_CHAR(GL_OPENING_DATE , '" + context.getDateFormat() + "') GLOPENFROMDATE FROM GLMAST WHERE  " + partitionNo + " = ? AND " + column + " = ?";
			} else {
				sqlQuery = "SELECT " + fetchColumns + " ,TO_CHAR(GL_OPENING_DATE , '" + context.getDateFormat() + "') GLOPENFROMDATE,TBA_KEY FROM GLMAST WHERE " + partitionNo + " = ? AND " + column + " = ?";

			}
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, getContext().getPartitionNo());
			util.setString(2, value);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				decodeRow(rset, result);
				String glOpenDate = rset.getString("GLOPENFROMDATE");
				if (glOpenDate != null && !isDateLesserEqualCBD(glOpenDate)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_GL_NOT_OPENED);
				}
				if (usageAllowed) {
					if (action.equals(USAGE)) {
						if (rset.getString("TBA_KEY") != null) {
							result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_USAGE_NOT_ALLOWED);
						}
					}
				}
			} else {
				result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return result;
	}

	public final boolean isValidChequePrefix(String chequePrefix) {
		if (chequePrefix == null || chequePrefix.length() == 0 || chequePrefix.length() > 6)
			return false;
		String regex = "^([A-Za-z0-9])+$";
		if (!chequePrefix.matches(regex))
			return false;
		return true;
	}

	public DTObject uploadImage(DTObject inputDTO) {
		BaseDomainValidator validator = new BaseDomainValidator();
		String inventoryNumber = "";
		DTObject resultDTO = new DTObject();
		try {
			byte[] uploadData;
			StringBuffer sb = new StringBuffer();
			sb.append(inputDTO.get("IMAGE_SOURCE"));
			uploadData = Base64.decode(sb.toString());
			resultDTO = validator.generateFileInventorySequence(resultDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FILE_INVENTORY_GENERROR);
				return resultDTO;
			}
			inventoryNumber = resultDTO.get("INV_SEQ");
			resultDTO.set("INV_NUM", inventoryNumber);
			resultDTO.setObject("FILE", uploadData);
			resultDTO.setObject("THUMB_NAIL", RegularConstants.NULL);
			resultDTO.set("FILE_NAME", "persondb" + inventoryNumber);
			resultDTO.set("IN_USE", RegularConstants.COLUMN_DISABLE);
			resultDTO.set("PURPOSE", RegularConstants.NULL);
			resultDTO.set("EXTENSION", "jpeg");
			resultDTO.set("GROUP_FILE_INV", RegularConstants.NULL);
			resultDTO.set("CHECKSUM", RegularConstants.NULL);

			try {
				boolean updated = validator.updateFileInventory(resultDTO);
				if (!updated) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FILE_UPLOAD);
					return resultDTO;
				}
			} catch (Exception e) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FILE_UPLOAD);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}

		return resultDTO;
	}

	public final DTObject validatePKExcludeTBA(DTObject input) {
		DTObject result = new DTObject();
		String table = input.get(ContentManager.TABLE);
		String fetch = input.get(ContentManager.FETCH);
		StringBuffer sqlQuery = new StringBuffer();
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = getDbContext().createUtilInstance();
		String[] arguments = (String[]) input.getObject(ContentManager.PROGRAM_KEY);
		String fetchColumns = input.get(ContentManager.FETCH_COLUMNS);
		String[] keyFields = null;
		BindParameterType[] keyFieldTypes = null;
		TableInfo tableInfo = null;
		try {
			DBInfo info = new DBInfo();
			tableInfo = info.getTableInfo(table);
			keyFields = tableInfo.getPrimaryKeyInfo().getColumnNames();
			keyFieldTypes = tableInfo.getPrimaryKeyInfo().getColumnTypes();
			if (fetch != null && fetch.equals(ContentManager.FETCH_ALL)) {
				sqlQuery.append("SELECT * FROM ").append(table).append(" WHERE ");
				int i = 0;
				for (String fieldName : keyFields) {
					if (i < keyFields.length - 1) {
						sqlQuery.append(fieldName).append(" = ?  AND  ");
					} else {
						sqlQuery.append(fieldName).append(" = ? ");
					}
					++i;
				}
				sqlQuery.trimToSize();
			} else {
				sqlQuery.append("SELECT " + fetchColumns + "  FROM ").append(table).append(" WHERE ");
				int i = 0;
				for (String fieldName : keyFields) {
					if (i < keyFields.length - 1) {
						sqlQuery.append(fieldName).append(" = ?  AND  ");
					} else {
						sqlQuery.append(fieldName).append(" = ? ");
					}
					++i;
				}
				sqlQuery.trimToSize();
			}
			util.reset();
			util.setSql(sqlQuery.toString());
			int k = 1;
			Map<String, BindParameterType> columnType = tableInfo.getColumnInfo().getColumnMapping();
			for (int i = 0; i < keyFieldTypes.length; i++) {
				BindParameterType fieldType = columnType.get(keyFields[i]);
				switch (fieldType) {
				case LONG:
					util.setLong(k, Long.parseLong(arguments[i]));
					break;
				case DATE:
					String value = arguments[i];
					util.setDate(k, new java.sql.Date(BackOfficeFormatUtils.getDate(value, context.getDateFormat()).getTime()));
					break;
				case INTEGER:
					util.setInt(k, Integer.parseInt(arguments[i]));
					break;
				case BIGDECIMAL:
					util.setBigDecimal(k, new BigDecimal(keyFields[i]));
					break;
				case VARCHAR:
				default:
					util.setString(k, arguments[i]);
					break;
				}
				k++;
			}
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				decodeRow(rset, result);

			} else {
				result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return result;
	}

	public final boolean isFiveDigitThreeDecimal(String value, int scale) {
		try {
			BigDecimal amountValue = new BigDecimal(value);
			if (amountValue.precision() > 8 || amountValue.scale() > 3 || (amountValue.precision() - amountValue.scale()) > 5) {
				return false;
			}
			if (amountValue.compareTo(smallAmount) > 0) {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}

	public final boolean isValidOtherInformation10(String information) {
		if (information == null || information.length() == LENGTH_0 || information.length() > LENGTH_10)
			return false;
		return true;
	}

	public final boolean isvalidNumericWithOperator(String number) {
		if (number.length() > LENGTH_20)
			return false;
		String regex = "^([0-9-\\+\\(\\)])+$";
		if (number.matches(regex))
			return true;
		return false;
	}

	public final DTObject isValidDynamicIntFractions(DTObject input) {
		DTObject result = new DTObject();
		String fieldValue = input.get("FIELD_VALUE");
		MasterValidator validator = new MasterValidator();
		try {
			if (isEmpty(fieldValue)) {
				result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_FIELD_BLANK);
				return result;
			}
			int scale = Integer.parseInt(input.get("SCALE"));// BEFORE POINT
			int precision = Integer.parseInt(input.get("PRECISION"));// AFTER
																		// POINT
			BigDecimal fieldValues = new BigDecimal(fieldValue);
			fieldValues = fieldValues.stripTrailingZeros();
			if (fieldValues.precision() - fieldValues.scale() > precision) {
				Object[] errorParam = { String.valueOf(precision) };
				result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_MAXINT_RANGE);
				result.setObject(ContentManager.ERROR_PARAM, errorParam);
				return result;
			}
			if (fieldValues.scale() > scale) {
				Object[] errorParam = { String.valueOf(scale) };
				result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_MAXFRACT_RANGE);
				result.setObject(ContentManager.ERROR_PARAM, errorParam);
				return result;
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return result;
	}

	public final boolean isValidPanNo(String code) {
		if (code.length() > LENGTH_25)
			return false;
		String regex = "^([A-Z0-9_])+$";
		if (code.matches(regex))
			return true;
		return false;
	}

	public final DTObject getFinancialTaxAmount(DTObject input) {
		DTObject resultDTO = new DTObject();
		DBUtil util = getDbContext().createUtilInstance();
		resultDTO.set(ContentManager.ERROR, RegularConstants.NULL);
		try {
			util.reset();
			util.setMode(DBUtil.CALLABLE);
			util.setSql("CALL SP_ELEASE_CALC_TAX(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
			util.setLong(1, Long.parseLong(getContext().getEntityCode()));
			util.setString(2, input.get("STATE_CODE"));
			util.setString(3, input.get("SHIP_STATE_CODE"));
			util.setString(4, input.get("TAX_SCHEDULE_CODE"));
			util.setString(5, input.get("TAX_CCY"));
			util.setString(6, input.get("TAX_ON_DATE"));
			util.setString(7, input.get("PRINCIPAL_AMOUNT"));
			util.setString(8, input.get("INTEREST_AMOUNT"));
			util.setString(9, input.get("RENTAL_AMOUNT"));
			util.setString(10, input.get("EXECUTORY_AMOUNT"));
			util.setString(11, input.get("MAIN_SL"));
			util.setString(12, input.get("SCHEDULE_SL"));
			util.setString(13, input.get("LESSEE_CODE"));
			util.setString(14, input.get("AGREEMENT_NO"));
			util.setString(15, input.get("SCHEDULE_ID"));
			util.setString(16, input.get("CUSTOMER_ID"));
			util.setString(17, input.get("LEASE_PRODUCT_CODE"));

			util.registerOutParameter(18, Types.DECIMAL);
			util.registerOutParameter(19, Types.DECIMAL);
			util.registerOutParameter(20, Types.DECIMAL);
			util.registerOutParameter(21, Types.DECIMAL);
			util.registerOutParameter(22, Types.DECIMAL);
			util.registerOutParameter(23, Types.DECIMAL);
			util.registerOutParameter(24, Types.DECIMAL);
			util.registerOutParameter(25, Types.BIGINT);
			util.registerOutParameter(26, Types.VARCHAR);
			util.execute();
			String error = util.getString(26);
			if (error.equals("S")) {
				resultDTO.set("O_STATE_TAX", util.getString(18));
				resultDTO.set("O_STATE_SERVICE_TAX", util.getString(19));
				resultDTO.set("O_STATE_CESS", util.getString(20));
				resultDTO.set("O_CENTRAL_TAX", util.getString(21));
				resultDTO.set("O_CENTRAL_SERVICE_TAX", util.getString(22));
				resultDTO.set("O_CENTRAL_CESS", util.getString(23));
				resultDTO.set("O_TOTAL_TAX", util.getString(24));
				resultDTO.set("O_SERIAL", util.getString(25));
				resultDTO.set("O_ERROR_CODE", util.getString(26));
			} else {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_AMOUNT);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return resultDTO;
		} finally {
			util.reset();
		}
		return resultDTO;
	}

	public final boolean isValidPANNumber(String code) {
		if (code.length() > PAN_LENGTH)
			return false;
		String regex = "^([a-zA-Z]){3}([P,F,C,H,T]){1}([a-zA-Z]){1}([0-9]){4}([a-zA-Z]){1}?$";
		if (code.matches(regex))
			return true;
		return false;
	}

	public final boolean isValidTANNumber(String code) {
		if (code.length() > TAN_LENGTH)
			return false;
		String regex = "^([a-zA-Z]){4}([0-9]){5}([a-zA-Z]){1}?$";
		if (code.matches(regex))
			return true;
		return false;
	}

	public final DTObject validateHierarchyCode(DTObject input) {
		DTObject result = new DTObject();
		String hierarchyCode = input.get("LAM_HIER_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(hierarchyCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(hierarchyCode, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, hierarchyCode);
			input.set(ContentManager.TABLE, "LAMHIER");
			input.set(ContentManager.COLUMN, "LAM_HIER_CODE");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			result = validateEntityWiseCodesWithoutEnabled(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}
	public final boolean isValidOtherInformation100(String information) {
		if (information == null || information.length() == LENGTH_0 || information.length() > INFORMATION_LENGTH100)
			return false;
		return true;
	}
}
