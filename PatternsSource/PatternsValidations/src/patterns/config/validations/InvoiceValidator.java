package patterns.config.validations;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.ContentManager;

public class InvoiceValidator extends CommonValidator {

	public final DTObject validateInvoiceCycleNumber(DTObject input) {
		DTObject result = new DTObject();
		String invoiceCycleNumber = input.get("INV_CYCLE_NUMBER");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(invoiceCycleNumber)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(invoiceCycleNumber, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, invoiceCycleNumber);
			input.set(ContentManager.TABLE, "INVCYCLE");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			input.set(ContentManager.COLUMN, "INV_CYCLE_NUMBER");
			result = validateEntityWiseCodesWithoutEnabled(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}
	
	public final DTObject ValidateInvFieldId(DTObject input) {
		DTObject result = new DTObject();
		String fieldId = input.get("FIELD_ID");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(fieldId)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(fieldId, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, fieldId);
			input.set(ContentManager.TABLE, "INVOICEFLD");
			input.set(ContentManager.COLUMN, "FIELD_ID");
			input.set(ContentManager.ENTITY_CODE, "ENTITY_CODE");
			result = validateEntityWiseCodesWithoutEnabled(input);
			} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}
	
	public final DTObject ValidateInvTemplateCode(DTObject input) {
		DTObject result = new DTObject();
		String invTempCode = input.get("INVTEMPLATE_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(invTempCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(invTempCode, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, invTempCode);
			input.set(ContentManager.TABLE, "INVTEMPLATECODE");
			input.set(ContentManager.COLUMN, "INVTEMPLATE_CODE");
			input.set(ContentManager.ENTITY_CODE, "ENTITY_CODE");
			result = validateEntityWiseCodes(input);
			} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

}