package patterns.config.validations;

import patterns.config.framework.bo.BackOfficeErrorCodes;

import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.ContentManager;

/**
 * This Class implements validator method for all master codes related to GL
 * Module
 * 
 * @author Jainudeen
 * @version 1.0
 * @since 2014-NOV-07
 * 
 * 
 */

public class GLValidator extends InterfaceValidator {

	public final DTObject validateBankAccountCode(DTObject input) {
		DTObject result = new DTObject();
		String bankAccountCode = input.get("BANK_AC_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(bankAccountCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(bankAccountCode, LENGTH_1, LENGTH_3)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, bankAccountCode);
			input.set(ContentManager.TABLE, "BANKACCOUNTS");
			input.set(ContentManager.COLUMN, "BANK_AC_CODE");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);

			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateFolioNumber(DTObject input) {
		DTObject result = new DTObject();
		String genFolioNumber = input.get("GEN_FOLIO_NUMBER");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(genFolioNumber)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(genFolioNumber, LENGTH_1, LENGTH_6)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, genFolioNumber);
			input.set(ContentManager.TABLE, "GENFOLIO");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			input.set(ContentManager.COLUMN, "GEN_FOLIO_NUMBER");
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateTransactionalGL(DTObject input) {
		DTObject result = new DTObject();
		String bankAccountCode = input.get("GL_HEAD_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(bankAccountCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(bankAccountCode, LENGTH_1, LENGTH_25)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, bankAccountCode);
			input.set(ContentManager.TABLE, "GLMAST");
			input.set(ContentManager.COLUMN, "GL_HEAD_CODE");
			input.set(ContentManager.PARTITION_NO, ContentManager.PARTITION_NO);
			result = validTransactionalGL(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}
}
