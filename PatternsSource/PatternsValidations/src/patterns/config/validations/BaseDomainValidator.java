package patterns.config.validations;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.sql.ResultSet;

import javax.servlet.ServletContext;

import net.coobird.thumbnailator.Thumbnails;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.monitor.ContextReference;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.framework.web.filters.LoadBalancerConfiguration;
import java.security.MessageDigest;

public class BaseDomainValidator extends CommonValidator {
	private static final int DEFAULT_BUFFER_SIZE = 10240; // 10KB.

	public BaseDomainValidator() {
		super();
	}

	public BaseDomainValidator(DBContext dbContext) {
		super(dbContext);
	}

	public final DTObject validateFileInventory(DTObject input) {
		String value = input.get(ContentManager.CODE);
		String sqlQuery = null;
		input.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = getDbContext().createUtilInstance();
		try {
			sqlQuery = "SELECT * FROM CMNFILEINVENTORY  WHERE ENTITY_CODE = ? AND FILE_INV_NUM =?";
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, getContext().getEntityCode());
			util.setString(2, value);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				input.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				decodeRow(rset, input);
			} else {
				input.set(ContentManager.RESULT,
						ContentManager.DATA_UNAVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			input.set(ContentManager.ERROR,
					BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return input;
	}

	public final DTObject validateSourceInventory(DTObject input) {
		String value = input.get(ContentManager.CODE);
		StringBuffer sqlQuery = new StringBuffer();
		input.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = getDbContext().createUtilInstance();
		String year = value.substring(0, 4);
		try {
			sqlQuery.append("SELECT * FROM CMNFILEINVENTORYSRC");
			sqlQuery.append(year);
			sqlQuery.append(" C JOIN CMNFILEINVENTORYSRCDTL D");
			sqlQuery.append(" ON D.ENTITY_CODE=C.ENTITY_CODE AND D.FILE_INV_NUM=C.FILE_INV_NUM");
			sqlQuery.append(" WHERE C.ENTITY_CODE = ? AND C.FILE_INV_NUM =?");
			util.reset();
			util.setSql(sqlQuery.toString());
			util.setString(1, getContext().getEntityCode());
			util.setString(2, value);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				input.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				decodeRow(rset, input);
			} else {
				input.set(ContentManager.RESULT,
						ContentManager.DATA_UNAVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			input.set(ContentManager.ERROR,
					BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return input;
	}

	public DTObject exportFileInventoryImages(DTObject formDTO) {
		DTObject resultDTO = new DTObject();
		DBUtil util = getDbContext().createUtilInstance();
		String value = formDTO.get("INVLIST");
		String filePath = "";
		ByteArrayInputStream input = null;
		BufferedOutputStream output = null;
		FileOutputStream fos = null;
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
		try {
			String[] invList = value.split("\\|");
			String inventoryList = "";
			for (int i = 0; i < invList.length; i++) {
				inventoryList += invList[i] + ",";
			}

			inventoryList = inventoryList.substring(0,
					inventoryList.length() - 1);
			util.reset();
			util.setSql("SELECT FILE_DOWNLOAD_PATH FROM SYSTEMFOLDERS WHERE ENTITY_CODE = ? AND EFFT_DATE = (SELECT MAX(EFFT_DATE) FROM SYSTEMFOLDERS WHERE ENTITY_CODE = ? AND EFFT_DATE <= FN_GETCD(?))");
			util.setString(1, getContext().getEntityCode());
			util.setString(2, getContext().getEntityCode());
			util.setString(3, getContext().getEntityCode());
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				filePath = rset.getString(1);
			}

			String sqlQuery = "SELECT FILE_NAME,FILE_DATA FROM CMNFILEINVENTORY WHERE ENTITY_CODE = ? AND FILE_INV_NUM IN("
					+ inventoryList + ")";
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, getContext().getEntityCode());
			rset = util.executeQuery();
			String fileName = "";
			byte[] fileData = null;
			while (rset.next()) {
				fileName = rset.getString(1);
				fileData = rset.getBytes(2);
				fileName = filePath + "\\" + fileName;
				input = new ByteArrayInputStream(fileData);
				fos = new FileOutputStream(new File(fileName));
				output = new BufferedOutputStream(fos, DEFAULT_BUFFER_SIZE);
				byte[] buffer = new byte[DEFAULT_BUFFER_SIZE];
				int length;
				while ((length = input.read(buffer)) > 0) {
					output.write(buffer, 0, length);
				}
				output.flush();
			}
			resultDTO.set("FOLDER_PATH", filePath);
			util.reset();

		} catch (Exception e) {
			e.printStackTrace();
			getLogger().logError(
					"validateReportDownload(1) : " + e.getLocalizedMessage());
			resultDTO.set(ContentManager.ERROR,
					BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			util.reset();
			try {
				if (fos != null) {
					fos.close();
				}
				if (input != null) {
					input.close();
				}
				if (output != null) {
					output.close();
				}
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
		return resultDTO;
	}

	public DTObject getSYSCPM(DTObject input) {
		DTObject result = new DTObject();
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = getDbContext().createUtilInstance();
		try {
			String sqlQuery = "SELECT * FROM SYSCPM WHERE ENTITY_CODE = ?";
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, getContext().getEntityCode());
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				decodeRow(rset, result);
				int minLength = rset.getInt("MIN_PWD_LEN");
				int minAlpha = rset.getInt("MIN_PWD_ALPHA");
				int minNumeric = rset.getInt("MIN_PWD_NUM");
				int minSpecial = rset.getInt("MIN_PWD_SPECIAL");
				int minPrevent = rset.getInt("PREVENT_PREV_PWD");
				String passwordPolicy = "Password must be a minimum of "
						+ minLength
						+ " characters long. Should contain atleast ";
				if (minAlpha > 0) {
					passwordPolicy += minAlpha + " alphabets , ";
				}
				if (minNumeric > 0) {
					passwordPolicy += minNumeric + " numeric characters , ";
				}
				if (minSpecial > 0) {
					passwordPolicy += minSpecial + " special characters , ";
				}
				if (minPrevent > 0) {
					passwordPolicy += " previous " + minPrevent
							+ " passwords must not be used ";
				}
				result.set("PASSWORD_POLICY", passwordPolicy);
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR,
					BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return result;
	}

	public DTObject getSystemFolders(DTObject input) {
		DTObject result = new DTObject();
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = getDbContext().createUtilInstance();
		try {
			String sqlQuery = "SELECT * FROM SYSTEMFOLDERS WHERE ENTITY_CODE = ? AND EFFT_DATE = (SELECT MAX(EFFT_DATE) FROM SYSTEMFOLDERS WHERE ENTITY_CODE = ? AND EFFT_DATE <= FN_GETCD(?))";
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, getContext().getEntityCode());
			util.setString(2, getContext().getEntityCode());
			util.setString(3, getContext().getEntityCode());
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				decodeRow(rset, result);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR,
					BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return result;
	}

	public DTObject getINSTALL(DTObject input) {
		DTObject result = new DTObject();
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = getDbContext().createUtilInstance();
		try {
			String sqlQuery = "SELECT * FROM INSTALL OC WHERE OC.PARTITION_NO = ? ";
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, getContext().getPartitionNo());
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				decodeRow(rset, result);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR,
					BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return result;
	}

	public final DTObject generateInventorySequence(DTObject input) {
		input.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = getDbContext().createUtilInstance();
		try {
			String sqlQuery = "SELECT FN_NEXTVAL('SEQ_FILEUPLOAD') AS INV_SEQ FROM DUAL";
			util.reset();
			util.setSql(sqlQuery);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				String invSequence = rset.getString(1);
				input.set("INV_SEQ", invSequence);
				input.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			input.set(ContentManager.ERROR,
					BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return input;
	}

	public final DTObject generateAnnouncementID(DTObject input) {
		input.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = getDbContext().createUtilInstance();
		try {
			String sqlQuery = "SELECT FN_GETANNOUNCEID() AS INV_SEQ FROM DUAL";
			util.reset();
			util.setMode(DBUtil.PREPARED);
			util.setSql(sqlQuery);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				String announceSequence = rset.getString(1);
				input.set("ANNOUNCE_ID", announceSequence);
				input.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			input.set(ContentManager.ERROR,
					BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return input;
	}

	public static final int BUFFER_VALUE = 1048576;

	public final byte[] getFileData(File file) throws Exception {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		BufferedInputStream bis = new BufferedInputStream(new FileInputStream(
				file));
		try {
			byte[] buffer = new byte[BUFFER_VALUE];
			do {
				int count = bis.read(buffer);
				if (count == -1) {
					break;
				} else {
					baos.write(buffer, 0, count);
				}
			} while (true);

		} finally {
			if (bis != null)
				bis.close();
			if (baos != null)
				baos.close();
		}
		return baos.toByteArray();
	}

	public final byte[] getThumbnail(File file) throws Exception {
		String fileName = file.getName();
		String format = fileName.substring(fileName.lastIndexOf(".") + 1,
				fileName.length());
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		Thumbnails.of(file).size(100, 100).outputFormat(format)
				.toOutputStream(baos);
		return baos.toByteArray();
	}

	public final boolean updateFileInventory(DTObject input) {
		boolean uploaded = false;
		String entityCode = getContext().getEntityCode();
		String inventoryNumber = input.get("INV_NUM");
		String uploadFileName = input.get("FILE_NAME");
		String uploadFileLoc = input.get("FILE_LOC");
		byte[] uploadData = (byte[]) input.getObject("FILE");
		byte[] thumbnailData = (byte[]) input.getObject("THUMB_NAIL");

		String status = input.get("IN_USE");
		String purpose = input.get("PURPOSE");
		String descn = input.get("FILE_DSCN");
		String fileFormat = input.get("EXTENSION");
		String checksum = "";

		try {
			getDbContext().setAutoCommit(true);
			DBUtil util = getDbContext().createUtilInstance();

			String sqlQuery = "INSERT INTO CMNFILEINVENTORY(ENTITY_CODE,FILE_INV_NUM,FILE_NAME,FILE_LOC,FILE_DATA,THUMBNAIL_FILE,FILE_CHECKSUM,IN_USE,PURPOSE,FILE_DESCN,FILE_EXTENSION)"
					+ " VALUES(?,?,?,?,?,?,?,?,?,?,?)";
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, entityCode);
			util.setString(2, inventoryNumber);
			util.setString(3, uploadFileName);
			util.setString(4, uploadFileLoc);
			util.setBytes(5, uploadData);
			util.setBytes(6, thumbnailData);
			util.setString(7, checksum);
			util.setString(8, status);
			util.setString(9, purpose);
			util.setString(10, descn);
			util.setString(11, fileFormat);
			util.executeUpdate();
			uploaded = true;
			util.reset();
			// uploaded = true;
		} catch (Exception e) {
			e.printStackTrace();
			uploaded = false;
		} finally {
		}
		return uploaded;
	}

	public final LoadBalancerConfiguration getLoadBalancerConfiguration() {
		LoadBalancerConfiguration loadBalancerConfiguration = new LoadBalancerConfiguration();
		DBUtil util = getDbContext().createUtilInstance();
		try {
			/*ServletContext servletContext = ContextReference.getContext();
			String sqlQuery = "SELECT SCHEME_ENABLED,SCHEME,SERVER_NAME_ENABLED,SERVER_NAME,CONTEXT_ENABLED,CONTEXT,PORT_ENABLED,PORT FROM LBALIASLIST WHERE ENTITY_CODE = ? AND ACCESS_TYPE = ? AND ADMIN_TYPE = ?";
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, servletContext
					.getInitParameter(RegularConstants.INIT_DEPLOYMENT_ENTITY));
			util.setString(2, servletContext
					.getInitParameter(RegularConstants.INIT_ACCESS_TYPE));
			util.setString(3, servletContext
					.getInitParameter(RegularConstants.INIT_ADMIN_TYPE));
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				String schemeEnabled = rset.getString(1);
				if (schemeEnabled.equals(RegularConstants.COLUMN_ENABLE)) {
					loadBalancerConfiguration.setSchemeEnabled(true);
					loadBalancerConfiguration.setScheme(rset.getString(2));
				} else {
					loadBalancerConfiguration.setSchemeEnabled(false);
				}
				String serverNameEnabled = rset.getString(3);
				if (serverNameEnabled.equals(RegularConstants.COLUMN_ENABLE)) {
					loadBalancerConfiguration.setServerNameEnabled(true);
					loadBalancerConfiguration.setServerName(rset.getString(4));
				} else {
					loadBalancerConfiguration.setServerNameEnabled(false);
				}
				String contextPathEnabled = rset.getString(5);
				if (contextPathEnabled.equals(RegularConstants.COLUMN_ENABLE)) {
					loadBalancerConfiguration.setContextPathEnabled(true);
					loadBalancerConfiguration.setContextPath(rset.getString(6));
				} else {
					loadBalancerConfiguration.setContextPathEnabled(false);
				}
				String serverPortEnabled = rset.getString(7);
				if (serverPortEnabled.equals(RegularConstants.COLUMN_ENABLE)) {
					loadBalancerConfiguration.setServerPortEnabled(true);
					loadBalancerConfiguration.setServerPort(rset.getInt(8));
				} else {
					loadBalancerConfiguration.setServerPortEnabled(false);
				}
			}*/
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			util.reset();
		}
		return loadBalancerConfiguration;
	}

	public DTObject validateReportDownload(DTObject input) {
		getLogger().logDebug("validateReportDownload()");
		input.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = getDbContext().createUtilInstance();
		try {
			String reportIdentifier = input.get(ContentManager.REPORT_ID);
			String sqlQuery = "SELECT * FROM REPORTDOWNLOAD WHERE ENTITY_CODE=? AND REPORT_IDENTIFIER=? AND DEPLOYMENT_CONTEXT =? AND  DOWNLOAD_DATE IS NULL";
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, getContext().getEntityCode());
			util.setString(2, reportIdentifier);
			util.setString(3, getContext().getDeploymentContext());
			// util.setString(4, getContext().getUserID());
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				decodeRow(rset, input);
				// sqlQuery = "CALL SP_UPDATEREPORTDOWNLOAD(?,?,?,?,?)";
				// util.reset();
				// util.reset();
				// util.setMode(DBUtil.CALLABLE);
				// util.setSql(sqlQuery);
				// util.setString(1, getContext().getEntityCode());
				// util.setString(2, reportIdentifier);
				// util.setString(3, getContext().getDeploymentContext());
				// util.setString(4, getContext().getUserID());
				// util.registerOutParameter(5, Types.VARCHAR);
				// util.execute();
				// String errorStatus = util.getString(5);
				// if (errorStatus != null &&
				// errorStatus.equals(RegularConstants.SP_SUCCESS)) {
				// input.set(ContentManager.RESULT,
				// ContentManager.DATA_AVAILABLE);

				// }
				// util.reset();
				input.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getLogger().logError(
					"validateReportDownload(1) : " + e.getLocalizedMessage());
			input.set(ContentManager.ERROR,
					BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return input;
	}

	public final DTObject validateUploadedFile(DTObject input) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		try {
			resultDTO = readFilePurposeParameter(input);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(
					ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR,
						BackOfficeErrorCodes.FILEPURPOSE_CFG_UNAVAILABLE);
				return resultDTO;
			}

			String allowedExtensions = resultDTO.get("ALLOWED_EXTENSIONS");
			String fileFormat = input.get("FILE_FORMAT");
			if (!isValidFileFormat(allowedExtensions, fileFormat)) {
				resultDTO.set(ContentManager.ERROR,
						BackOfficeErrorCodes.INVALID_FILE_FORMAT);
				return resultDTO;
			}
			resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
		} catch (Exception e) {
			resultDTO.set(ContentManager.ERROR, e.getLocalizedMessage());
		}
		return resultDTO;
	}

	public final boolean updateFileInventoryDetails(DTObject input) {
		boolean uploaded = false;
		String entityCode = getContext().getEntityCode();
		String inventoryNumber = input.get("INV_NUM");
		String uploadFileName = input.get("FILE_NAME");
		byte[] uploadData = (byte[]) input.getObject("FILE");
		byte[] thumbnailData = (byte[]) input.getObject("THUMB_NAIL");
		String fileChecksum = input.get("CHECKSUM");
		String fileExtension = input.get("EXTENSION");
		String programId = input.get("PGM_ID");
		String purpose = input.get("PURPOSE");
		String groupInventoryNumber = input.get("GRP_INV_NUM");
		String inUse = input.get("FILE_INUSE");

		try {
			getDbContext().setAutoCommit(true);
			DBUtil util = getDbContext().createUtilInstance();
			String sqlQuery = "INSERT INTO FILEINVENTORY(ENTITY_CODE,FILE_INVENTORY_NO,FILE_NAME,FILE_DATA,THUMBNAIL_DATA,FILE_CHECKSUM,FILE_EXTENSION,PGM_ID,FILE_UPLOAD_PURPOSE,GROUP_FILE_INVENTORY,FILE_IN_USE,UPLOAD_DATETIME,UPLOAD_BY)"
					+ " VALUES(?,?,?,?,?,?,?,?,?,?,?,FN_GETCDT(?),?)";
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, entityCode);
			util.setString(2, inventoryNumber);
			util.setString(3, uploadFileName);
			util.setBytes(4, uploadData);
			util.setBytes(5, thumbnailData);
			util.setString(6, fileChecksum);
			util.setString(7, fileExtension);
			util.setString(8, programId);
			util.setString(9, purpose);
			util.setString(10, groupInventoryNumber);
			util.setString(11, inUse);
			util.setString(12, entityCode);
			util.setString(13, getContext().getUserID());
			util.executeUpdate();
			uploaded = true;
			util.reset();
			// uploaded = true;
		} catch (Exception e) {
			e.printStackTrace();
			uploaded = false;
		} finally {
		}
		return uploaded;
	}

	public final DTObject readFilePurposeParameter(DTObject input) {
		DTObject result = new DTObject();
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = getDbContext().createUtilInstance();
		try {
			StringBuffer sqlQuery = new StringBuffer(
					RegularConstants.EMPTY_STRING);
			sqlQuery.append("SELECT FP.ALLOWED_EXTENSIONS,FP.THUMBNAIL_REQD,FP.CHECKSUM_REQD,M.MAXIMUM_UPLOADS ");
			sqlQuery.append("FROM FILEPURPOSE FP ");
			sqlQuery.append("JOIN MPGMFPCFG M ON(M.ENTITY_CODE=FP.ENTITY_CODE AND M.FILE_PURPOSE_CODE=FP.FILE_PURPOSE_CODE AND FP.ENABLED='1') ");
			sqlQuery.append("WHERE M.ENTITY_CODE=? AND M.PGM_ID=? AND M.FILE_PURPOSE_CODE=? ");
			util.setMode(DBUtil.PREPARED);
			util.setSql(sqlQuery.toString());
			util.setString(1, getContext().getEntityCode());
			util.setString(2, input.get("PGM_ID"));
			util.setString(3, input.get("PURPOSE"));
			ResultSet rs = util.executeQuery();
			if (rs.next()) {
				result.set("ALLOWED_EXTENSIONS", rs.getString(1));
				result.set("THUMBNAIL_REQD", rs.getString(2));
				result.set("CHECKSUM_REQD", rs.getString(3));
				result.set("MAXIMUM_UPLOADS", rs.getString(4));
			}
			if (result.containsKey("ALLOWED_EXTENSIONS")) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
			}
		} catch (Exception e) {
			result.set(ContentManager.ERROR, e.getLocalizedMessage());
		} finally {
			util.reset();
		}
		return result;
	}

	public final String getFileChecksum(File file) {
		StringBuilder sb = new StringBuilder("");
		try {
			FileInputStream fis = new FileInputStream(file);

			MessageDigest digest = MessageDigest.getInstance("SHA-256");

			byte[] byteArray = new byte[1024];
			int bytesCount = 0;

			while ((bytesCount = fis.read(byteArray)) != -1) {
				digest.update(byteArray, 0, bytesCount);
			}

			fis.close();
			byte[] bytes = digest.digest();
			for (int i = 0; i < bytes.length; i++) {
				sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16)
						.substring(1));
			}
		} catch (Exception e) {

		}
		return sb.toString();
	}

	public final DTObject generateFileInventorySequence(DTObject input) {
		input.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = getDbContext().createUtilInstance();
		try {
			// String sqlQuery =
			// "SELECT CONCAT(DATE_FORMAT(NOW(),'%Y%m%d%H%i%s'),LPAD(FN_NEXTVAL('SEQ_FILEINVENTORY'), 6, '0'))";
			// SEQ_FILEUPLOAD
			String sqlQuery = "SELECT FN_NEXTVAL('SEQ_FILEUPLOAD')";
			util.reset();
			util.setSql(sqlQuery);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				String invSequence = rset.getString(1);
				input.set("INV_SEQ", invSequence);
				input.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
			}
			util.reset();
		} catch (Exception e) {
			e.printStackTrace();
			input.set(ContentManager.ERROR,
					BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return input;
	}

	public final java.sql.Date getCurrentBusinessDate(String entityCode) {
		DBUtil util = getDbContext().createUtilInstance();
		java.sql.Date cbd = null;
		try {
			String sqlQuery = "SELECT FN_GETCD(?) AS SYS_DATE FROM DUAL";
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, entityCode);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				cbd = rset.getDate(1);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getLogger().logError(e.getLocalizedMessage());
		} finally {
			util.reset();
		}
		return cbd;
	}

}