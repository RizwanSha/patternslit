package patterns.config.validations;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.ContentManager;

public class StateCodeValidator extends InterfaceValidator {
	
	
	public final DTObject validateStateCode(DTObject input) {
		DTObject result = new DTObject();
		String stateCode = input.get("STATE_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(stateCode)) {
					result.set(ContentManager.ERROR,
							BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(stateCode, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR,
							BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, stateCode);
			input.set(ContentManager.TABLE, "STATE");
			input.set(ContentManager.COLUMN, "STATE_CODE");
			input.set(ContentManager.ENTITY_CODE, "ENTITY_CODE");
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR,
					BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}
	
	
	
}