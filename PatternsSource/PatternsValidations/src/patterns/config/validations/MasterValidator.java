package patterns.config.validations;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.ContentManager;

/**
 * This Class implements validator method for all master codes
 * 
 * @author Jainudeen
 * @version 1.0
 * @since 2014-NOV-07
 * 
 *        Sl.No VersioNo. Modified By Modified Date Changes Done
 *        ----------------
 *        --------------------------------------------------------
 *        ----------------
 *        ----------------------------------------------------------- 1 1.1
 *        Rizwan Sha S 14-Nov-2014 Modified in validatePersonId Changed from
 *        validateEntityWiseCodes to validateGenericCode 2 1.2 Rizwan Sha S
 *        18-Nov-2014 Changes made to a.validateBloodGroupCode method changed
 *        from BLG_CODE to BLOOD_GROUP_CODE
 *        b.validateGeographicalUnitStructureCode changed from GEOU_CODE to
 *        GEO_UNIT_STRUC_CODE c.validateGeographicalUnitType changed from
 *        GEOUT_STRUC_CODE.GEOUT_UNIT_TYPE to GEO_UNIT_STRUC_CODE and
 *        GEO_UNIT_TYPE d.validateCountryCode changed from COU_CODE to
 *        COUNTRY_CODE e.validatePIDType and validateSIRMClassCode new methods
 *        added to validate PIDTYPES and SIRMCLASSES 3 1.3 Rizwan Sha S
 *        24-Nov-2014 Changes made to a.The length size from LENGTH_8 TO
 *        LENGTH_12 in validatePIDType method. b.validatePIDRegSl method
 *        created. 4 1.3 Rizwan Sha S 25-Nov-2014 Changes made to a.The length
 *        size from LENGTH_6 TO LENGTH_12 in validateDepartment method. a.The
 *        length size from LENGTH_6 TO LENGTH_12 in validateRoomType method. 5
 *        1.4 Rizwan Sha S 27-Nov-2014 New
 *        validatePersonIDWithDoctorCode,validateRegNoWithDoctorCode methods
 *        added.
 * 
 * 
 * 
 */

public class MasterValidator extends InterfaceValidator {

	public final DTObject validateOccupationCode(DTObject input) {
		DTObject result = new DTObject();
		String occupationCode = input.get("OCCU_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {

				if (!isValidCodePattern(occupationCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(occupationCode, LENGTH_1, LENGTH_3)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, occupationCode);
			input.set(ContentManager.TABLE, "OCCUPATION");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			input.set(ContentManager.COLUMN, "OCCU_CODE");
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	// public final DTObject validateGeographicalUnitStructureCode(DTObject
	// input) {
	// DTObject result = new DTObject();
	// String geographicalUnitStructureCode = input.get("GEO_UNIT_STRUC_CODE");
	// String action = input.get(ContentManager.USER_ACTION);
	// try {
	// if (action.equals(ADD)) {
	//
	// if (!isValidCodePattern(geographicalUnitStructureCode)) {
	// result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
	// return result;
	// }
	// if (!isValidLength(geographicalUnitStructureCode, LENGTH_1, LENGTH_3)) {
	// result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
	// return result;
	// }
	// }
	// input.set(ContentManager.CODE, geographicalUnitStructureCode);
	// input.set(ContentManager.TABLE, "GEOUNITSTRUC");
	// input.set(ContentManager.COLUMN, "GEO_UNIT_STRUC_CODE");
	// result = validateGenericCode(input);
	// } catch (Exception e) {
	// e.printStackTrace();
	// result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
	// return result;
	// }
	// return result;
	// }

	public final DTObject validateGeographicalUnitType(DTObject input) {
		DTObject result = new DTObject();
		String geographicalUnitStructureCode = input.get("GEO_UNIT_STRUC_CODE");
		String geographicalUnitType = input.get("GEO_UNIT_TYPE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(geographicalUnitType)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(geographicalUnitType, LENGTH_1, LENGTH_2)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.TABLE, "GEOUNITTYPE");
			String columns[] = new String[] { geographicalUnitStructureCode, geographicalUnitType };
			input.setObject(ContentManager.PROGRAM_KEY, columns);
			result = validatePK(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateTitleCode(DTObject input) {
		DTObject result = new DTObject();
		String titleCode = input.get("TITLE_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {

				if (!isValidCodePattern(titleCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(titleCode, LENGTH_1, LENGTH_3)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, titleCode);
			input.set(ContentManager.TABLE, "TITLE");
			input.set(ContentManager.COLUMN, "TITLE_CODE");
			result = validateGenericCode(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	// public final DTObject validateCountryCode(DTObject input) {
	// DTObject result = new DTObject();
	// String countryCode = input.get("COUNTRY_CODE");
	// String action = input.get(ContentManager.USER_ACTION);
	// try {
	// if (action.equals(ADD)) {
	//
	// if (!isValidCodePattern(countryCode)) {
	// result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
	// return result;
	// }
	// if (!isValidLength(countryCode, LENGTH_1, LENGTH_2)) {
	// result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
	// return result;
	// }
	// }
	// input.set(ContentManager.CODE, countryCode);
	// input.set(ContentManager.TABLE, "COUNTRY");
	// input.set(ContentManager.COLUMN, "COUNTRY_CODE");
	// result = validateGenericCode(input);
	// } catch (Exception e) {
	// e.printStackTrace();
	// result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
	// return result;
	// }
	// return result;
	// }

	public final DTObject validateAddressTypeCode(DTObject input) {
		DTObject result = new DTObject();
		String addressTypeCode = input.get("ADDR_TYPE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {

				if (!isValidCodePattern(addressTypeCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(addressTypeCode, LENGTH_1, LENGTH_2)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, addressTypeCode);
			input.set(ContentManager.TABLE, "ADDRESSTYPE");
			input.set(ContentManager.COLUMN, "ADDR_TYPE");
			result = validateGenericCode(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateGeographicalUnitId(DTObject input) {

		DTObject result = new DTObject();
		String countryCode = input.get("GEOC_COU_CODE");
		String geographicalUnitId = input.get("GEOC_UNIT_ID");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(geographicalUnitId)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(geographicalUnitId, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.TABLE, "GEOUNITCON");
			String columns[] = new String[] { countryCode, geographicalUnitId };
			input.setObject(ContentManager.PROGRAM_KEY, columns);
			result = validatePK(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validatePersonDesignationCode(DTObject input) {
		DTObject result = new DTObject();
		String personDesignationCode = input.get("PERSON_DESIG_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {

				if (!isValidCodePattern(personDesignationCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(personDesignationCode, LENGTH_1, LENGTH_3)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, personDesignationCode);
			input.set(ContentManager.TABLE, "PERSONDESIG");
			input.set(ContentManager.COLUMN, "PERSON_DESIG_CODE");
			result = validateGenericCode(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateConnectedRoleCode(DTObject input) {
		DTObject result = new DTObject();
		String connnectedRoleCode = input.get("CONN_ROLE_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {

				if (!isValidCodePattern(connnectedRoleCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(connnectedRoleCode, LENGTH_1, LENGTH_6)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, connnectedRoleCode);
			input.set(ContentManager.TABLE, "CONNECTEDROLES");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			input.set(ContentManager.COLUMN, "CONN_ROLE_CODE");
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateOrganizationCode(DTObject input) {
		DTObject result = new DTObject();
		String organizationCode = input.get("ORG_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {

				if (!isValidCodePattern(organizationCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(organizationCode, LENGTH_1, LENGTH_6)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, organizationCode);
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			input.set(ContentManager.TABLE, "ORGANIZATIONS");
			input.set(ContentManager.COLUMN, "ORG_CODE");
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validatePatientCategoryCode(DTObject input) {
		DTObject result = new DTObject();
		String patientCategoryCode = input.get("PATIENT_CAT_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {

				if (!isValidCodePattern(patientCategoryCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(patientCategoryCode, LENGTH_1, LENGTH_3)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, patientCategoryCode);
			input.set(ContentManager.TABLE, "PATIENTCAT");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			input.set(ContentManager.COLUMN, "PATIENT_CAT_CODE");
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateRelationshipRefCode(DTObject input) {
		DTObject result = new DTObject();
		String relationshipRefCode = input.get("RELREF_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {

				if (!isValidCodePattern(relationshipRefCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(relationshipRefCode, LENGTH_1, LENGTH_3)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, relationshipRefCode);
			input.set(ContentManager.TABLE, "RELATIONREF");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			input.set(ContentManager.COLUMN, "RELREF_CODE");
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateMaritalStatusCode(DTObject input) {
		DTObject result = new DTObject();
		String maritalStatusCode = input.get("MARITAL_STATUS_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {

				if (!isValidCodePattern(maritalStatusCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(maritalStatusCode, LENGTH_1, LENGTH_2)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, maritalStatusCode);
			input.set(ContentManager.TABLE, "MARITALSTATUS");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			input.set(ContentManager.COLUMN, "MARITAL_STATUS_CODE");
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateReligionCode(DTObject input) {
		DTObject result = new DTObject();
		String religionCode = input.get("REL_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {

				if (!isValidCodePattern(religionCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(religionCode, LENGTH_1, LENGTH_3)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, religionCode);
			input.set(ContentManager.TABLE, "RELIGIONS");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			input.set(ContentManager.COLUMN, "REL_CODE");
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateBloodGroupCode(DTObject input) {
		DTObject result = new DTObject();
		String bloodGroupCode = input.get("BLOOD_GROUP_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {

				if (!isValidCodePattern(bloodGroupCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(bloodGroupCode, LENGTH_1, LENGTH_6)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, bloodGroupCode);
			input.set(ContentManager.TABLE, "BLOODGROUP");
			input.set(ContentManager.COLUMN, "BLOOD_GROUP_CODE");
			result = validateGenericCode(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateAllergyCode(DTObject input) {
		DTObject result = new DTObject();
		String allergyCode = input.get("ALLERGY_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {

				if (!isValidCodePattern(allergyCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(allergyCode, LENGTH_1, LENGTH_3)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, allergyCode);
			input.set(ContentManager.TABLE, "ALLERGIES");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			input.set(ContentManager.COLUMN, "ALLERGY_CODE");
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateBankCode(DTObject input) {
		DTObject result = new DTObject();
		String bankCode = input.get("BANK_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {

				if (!isValidCodePattern(bankCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(bankCode, LENGTH_1, LENGTH_6)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, bankCode);
			input.set(ContentManager.TABLE, "BANKS");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			input.set(ContentManager.COLUMN, "BANK_CODE");
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateBankBranchCode(DTObject input) {
		DTObject result = new DTObject();
		String bankCode = input.get("BANK_CODE");
		String branchCode = input.get("BRANCH_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(branchCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(branchCode, LENGTH_1, LENGTH_15)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.TABLE, "BRANCHES");
			String columns[] = new String[] { getContext().getEntityCode(), bankCode, branchCode };
			input.setObject(ContentManager.PROGRAM_KEY, columns);
			result = validatePK(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateProgramId(DTObject input) {
		DTObject result = new DTObject();
		String programId = input.get("PGM_ID");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {

				if (!isValidCodePattern(programId)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(programId, LENGTH_1, LENGTH_50)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, programId);
			input.set(ContentManager.TABLE, "MPGM");
			input.set(ContentManager.COLUMN, "PGM_ID");
			result = validateGenericCode(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateSpecializationCode(DTObject input) {
		DTObject result = new DTObject();
		String specializationCode = input.get("SPECIALIZATION_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(specializationCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(specializationCode, LENGTH_1, LENGTH_6)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, specializationCode);
			input.set(ContentManager.TABLE, "SPECIALIZATIONS");
			input.set(ContentManager.COLUMN, "SPECIALIZATION_CODE");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);

			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;

	}

	public final DTObject validateDepartment(DTObject input) {
		DTObject result = new DTObject();
		String departmentCode = input.get("DEPT_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(departmentCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(departmentCode, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, departmentCode);
			input.set(ContentManager.TABLE, "DEPARTMENTS");
			input.set(ContentManager.COLUMN, "DEPT_CODE");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);

			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;

	}

	public final DTObject validatePersonId(DTObject input) {
		DTObject result = new DTObject();
		String personId = input.get("PERSON_ID");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidNumber(personId)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(personId, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, personId);
			input.set(ContentManager.TABLE, "PERSONDB");
			input.set(ContentManager.COLUMN, "PERSON_ID");
			result = validatePartitionWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateEntityCode(DTObject input) {
		DTObject result = new DTObject();
		String entityCode = input.get("ENTITY_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidNumber(entityCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(entityCode, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, entityCode);
			input.set(ContentManager.TABLE, "ENTITIES");
			input.set(ContentManager.COLUMN, ContentManager.ENTITY_CODE);

			result = validEntityCode(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateSIRMCode(DTObject input) {
		DTObject result = new DTObject();
		String SIRM = input.get("SIRM_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(SIRM)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(SIRM, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, SIRM);
			input.set(ContentManager.TABLE, "SIRM");
			input.set(ContentManager.COLUMN, "SIRM_CODE");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);

			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateCollectionType(DTObject input) {
		DTObject result = new DTObject();
		String collectionType = input.get("COLL_TYPE_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(collectionType)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(collectionType, LENGTH_1, LENGTH_3)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, collectionType);
			input.set(ContentManager.TABLE, "COLLECTIONTYPES");
			input.set(ContentManager.COLUMN, "COLL_TYPE_CODE");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);

			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateBillHeadCode(DTObject input) {
		DTObject result = new DTObject();
		String billHeadCode = input.get("BHEAD_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(billHeadCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(billHeadCode, LENGTH_1, LENGTH_6)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.TABLE, "BILLHEAD");
			String values[] = new String[] { getContext().getEntityCode(), billHeadCode };
			input.setObject(ContentManager.CODE, values);
			String fields[] = new String[] { "ENTITY_CODE", "BHEAD_CODE" };
			input.setObject(ContentManager.COLUMN, fields);
			result = validateUKCode(input);

		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateBillHeadNumber(DTObject input) {
		DTObject result = new DTObject();
		String billHeadNum = input.get("BHEAD_INTERNAL_NO");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(billHeadNum)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(billHeadNum, LENGTH_1, LENGTH_6)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, billHeadNum);
			input.set(ContentManager.TABLE, "BILLHEAD");
			input.set(ContentManager.COLUMN, "BHEAD_INTERNAL_NO");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateDoctorCode(DTObject input) {
		DTObject result = new DTObject();
		String doctor = input.get("DOCTOR_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(doctor)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(doctor, LENGTH_1, LENGTH_6)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, doctor);
			input.set(ContentManager.TABLE, "DOCTORS");
			input.set(ContentManager.COLUMN, "DOCTOR_CODE");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);

			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateMedicalRole(DTObject input) {
		DTObject result = new DTObject();
		String medicalRoleCode = input.get("MED_ROLE_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(medicalRoleCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(medicalRoleCode, LENGTH_1, LENGTH_6)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, medicalRoleCode);
			input.set(ContentManager.TABLE, "MEDROLES");
			input.set(ContentManager.COLUMN, "MED_ROLE_CODE");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);

			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateSIRMGroup(DTObject input) {
		DTObject result = new DTObject();
		String SIRMGroup = input.get("SIRM_GROUP_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(SIRMGroup)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(SIRMGroup, LENGTH_1, LENGTH_6)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, SIRMGroup);
			input.set(ContentManager.TABLE, "SIRMGROUPS");
			input.set(ContentManager.COLUMN, "SIRM_GROUP_CODE");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);

			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateEmergencyCategory(DTObject input) {
		DTObject result = new DTObject();
		String emergencyCategory = input.get("EMCAT_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(emergencyCategory)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(emergencyCategory, LENGTH_1, LENGTH_3)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, emergencyCategory);
			input.set(ContentManager.TABLE, "EMERGENCYCAT");
			input.set(ContentManager.COLUMN, "EMCAT_CODE");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);

			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateRoomType(DTObject input) {
		DTObject result = new DTObject();
		String roomType = input.get("ROOM_TYPE_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(roomType)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(roomType, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, roomType);
			input.set(ContentManager.TABLE, "ROOMTYPES");
			input.set(ContentManager.COLUMN, "ROOM_TYPE_CODE");
			input.set(ContentManager.ENTITY_CODE, "ENTITY_CODE");

			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateCurrency(DTObject input) {
		DTObject result = new DTObject();
		String currency = input.get("CCY_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(currency)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(currency, LENGTH_1, LENGTH_3)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, currency);
			input.set(ContentManager.TABLE, "CURRENCY");
			input.set(ContentManager.COLUMN, "CCY_CODE");

			result = validateGenericCode(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateSIRMClassCode(DTObject input) {
		DTObject result = new DTObject();
		String classCode = input.get("CLASS_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(classCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(classCode, LENGTH_1, LENGTH_3)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, classCode);
			input.set(ContentManager.TABLE, "SIRMCLASSES");
			input.set(ContentManager.COLUMN, "CLASS_CODE");
			input.set(ContentManager.ENTITY_CODE, "ENTITY_CODE");

			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validatePIDCode(DTObject input) {
		DTObject result = new DTObject();
		String pidCode = input.get("PID_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(pidCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(pidCode, LENGTH_1, LENGTH_3)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, pidCode);
			input.set(ContentManager.TABLE, "PIDCODES");
			input.set(ContentManager.COLUMN, "PID_CODE");
			input.set(ContentManager.ENTITY_CODE, "ENTITY_CODE");

			result = validateGenericCode(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validatePIDRegSl(DTObject input) {
		DTObject result = new DTObject();
		String pidRegSl = input.get("PID_REG_SL");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidNumber(pidRegSl)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(pidRegSl, LENGTH_1, LENGTH_5)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, pidRegSl);
			input.set(ContentManager.TABLE, "PERSONPID");
			input.set(ContentManager.COLUMN, "PID_REG_SL");
			input.set(ContentManager.ENTITY_CODE, "ENTITY_CODE");

			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validatePersonPID(DTObject input) {
		DTObject result = new DTObject();
		String personId = input.get("PERSON_ID");
		String pid_Reg_Sl = input.get("PID_REG_SL");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidNumber(pid_Reg_Sl)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(pid_Reg_Sl, LENGTH_1, LENGTH_5)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}

			input.set(ContentManager.TABLE, "PERSONPID");
			String columns[] = new String[] { personId, pid_Reg_Sl };
			input.setObject(ContentManager.PROGRAM_KEY, columns);
			result = validPersonPID(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validatePersonPIDWithPIDCode(DTObject input) {
		DTObject result = new DTObject();
		String personId = input.get("PERSON_ID");
		String pid_Code = input.get("PID_CODE");
		try {
			input.set(ContentManager.TABLE, "PERSONPID");
			String columns[] = new String[] { personId, pid_Code };
			input.setObject(ContentManager.PROGRAM_KEY, columns);
			input.set(ContentManager.ENTITY_CODE, "ENTITY_CODE");
			result = validPersonPIDWithPIDCode(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validatePersonIDWithDoctorCode(DTObject input) {
		DTObject result = new DTObject();
		String personId = input.get("DOCTOR_PERSON_ID");
		String doctor_Code = input.get("DOCTOR_CODE");
		try {
			input.set(ContentManager.TABLE, "DOCTORS");
			String columns[] = new String[] { personId, doctor_Code };
			input.setObject(ContentManager.PROGRAM_KEY, columns);
			input.set(ContentManager.ENTITY_CODE, "ENTITY_CODE");
			result = validPersonIDWithDoctorCode(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateRegNoWithDoctorCode(DTObject input) {
		DTObject result = new DTObject();
		String regNo = input.get("DOCTOR_REG_NO");
		String doctor_Code = input.get("DOCTOR_CODE");
		try {
			input.set(ContentManager.TABLE, "DOCTORS");
			String columns[] = new String[] { regNo, doctor_Code };
			input.setObject(ContentManager.PROGRAM_KEY, columns);
			input.set(ContentManager.ENTITY_CODE, "ENTITY_CODE");
			result = validRegNoWithDoctorCode(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateConnOrgRelID(DTObject input) {
		DTObject result = new DTObject();
		String connOrgRelID = input.get("CONN_ORG_RELATION_ID");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(connOrgRelID)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(connOrgRelID, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, connOrgRelID);
			input.set(ContentManager.TABLE, "CONNECTEDORG");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			input.set(ContentManager.COLUMN, "CONN_ORG_RELATION_ID");
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateConnPerRelID(DTObject input) {
		DTObject result = new DTObject();
		String connPerRelID = input.get("CONN_PER_RELATION_ID");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(connPerRelID)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(connPerRelID, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, connPerRelID);
			input.set(ContentManager.TABLE, "CONNECTEDPER");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			input.set(ContentManager.COLUMN, "CONN_PER_RELATION_ID");
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateBillType(DTObject input) {
		DTObject result = new DTObject();
		String billType = input.get("BILL_TYPE_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(billType)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(billType, LENGTH_1, LENGTH_2)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, billType);
			input.set(ContentManager.TABLE, "BILLTYPES");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			input.set(ContentManager.COLUMN, "BILL_TYPE_CODE");
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateDiscApprCatCode(DTObject input) {
		DTObject result = new DTObject();
		String discappcatcode = input.get("DISC_APPROVAL_CAT_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(discappcatcode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(discappcatcode, LENGTH_1, LENGTH_8)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, discappcatcode);
			input.set(ContentManager.TABLE, "DISCAPPROVALCAT");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			input.set(ContentManager.COLUMN, "DISC_APPROVAL_CAT_CODE");
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateDiscProfileCode(DTObject input) {
		DTObject result = new DTObject();
		String discprofilecode = input.get("DISC_PROFILE_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(discprofilecode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(discprofilecode, LENGTH_1, LENGTH_6)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, discprofilecode);
			input.set(ContentManager.TABLE, "DISCPROFILE");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			input.set(ContentManager.COLUMN, "DISC_PROFILE_CODE");
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateTariffGrade(DTObject input) {
		DTObject result = new DTObject();
		String tariffGradeCode = input.get("TARIFF_GRADE_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(tariffGradeCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(tariffGradeCode, LENGTH_1, LENGTH_6)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, tariffGradeCode);
			input.set(ContentManager.TABLE, "TARIFFGRADES");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			input.set(ContentManager.COLUMN, "TARIFF_GRADE_CODE");
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateWardTypeCode(DTObject input) {
		DTObject result = new DTObject();
		String wardTypeCode = input.get("WARD_TYPE_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(wardTypeCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(wardTypeCode, LENGTH_1, LENGTH_3)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, wardTypeCode);
			input.set(ContentManager.TABLE, "WARDTYPES");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			input.set(ContentManager.COLUMN, "WARD_TYPE_CODE");
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateWardCode(DTObject input) {
		DTObject result = new DTObject();
		String wardCode = input.get("WARD_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(wardCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(wardCode, LENGTH_1, LENGTH_3)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, wardCode);
			input.set(ContentManager.TABLE, "WARDS");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			input.set(ContentManager.COLUMN, "WARD_CODE");
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateReportNumberPfx(DTObject input) {
		DTObject result = new DTObject();
		String reportNumPfx = input.get("REPORT_PREFIX_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(reportNumPfx)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(reportNumPfx, LENGTH_1, LENGTH_8)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, reportNumPfx);
			input.set(ContentManager.TABLE, "REPORTPREFIX");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			input.set(ContentManager.COLUMN, "REPORT_PREFIX_CODE");
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateAdmissionReqCat(DTObject input) {
		DTObject result = new DTObject();
		String admReqCat = input.get("ADMISSION_REQ_CATEGORY");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(admReqCat)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(admReqCat, LENGTH_1, LENGTH_3)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, admReqCat);
			input.set(ContentManager.TABLE, "IPREGREQCAT");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			input.set(ContentManager.COLUMN, "ADMISSION_REQ_CATEGORY");
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validatePaymentMode(DTObject input) {
		DTObject result = new DTObject();
		String paymentMode = input.get("PAY_MODE_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(paymentMode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(paymentMode, LENGTH_1, LENGTH_3)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, paymentMode);
			input.set(ContentManager.TABLE, "PAYMENTMODES");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			input.set(ContentManager.COLUMN, "PAY_MODE_CODE");
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateDocShareModelCode(DTObject input) {
		DTObject result = new DTObject();
		String docShareModelCode = input.get("DOCSHARE_MODEL_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(docShareModelCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(docShareModelCode, LENGTH_1, LENGTH_6)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, docShareModelCode);
			input.set(ContentManager.TABLE, "DOCSHAREMODEL");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			input.set(ContentManager.COLUMN, "DOCSHARE_MODEL_CODE");
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateRoomCode(DTObject input) {
		DTObject result = new DTObject();
		DTObject inputOTN = new DTObject();
		String wardCode = input.get("WARD_CODE");
		String roomCode = input.get("ROOM_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(wardCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(roomCode, LENGTH_1, LENGTH_8)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}

			input.set(ContentManager.TABLE, "ROOMS");
			String values[] = new String[] { getContext().getEntityCode(), wardCode, roomCode };
			input.setObject(ContentManager.CODE, values);
			String fields[] = new String[] { "ENTITY_CODE", "WARD_CODE", "ROOM_CODE" };
			input.setObject(ContentManager.COLUMN, fields);
			result = validateUKCode(input);
			if (result.get(ContentManager.ERROR) != null || result.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
				return result;
			} else {
				result.reset();
				inputOTN.set(ContentManager.ENTITY_CODE, getContext().getEntityCode());
				inputOTN.set(ContentManager.TABLE, "ROOMS");
				inputOTN.set(ContentManager.KEY, wardCode + RegularConstants.PK_SEPARATOR + roomCode);
				result = validateGeneralOTN(inputOTN);
				if (result.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
					input.set(ContentManager.CODE, result.get("NEW_KEY_NUMERIC_VALUE"));
					input.set(ContentManager.COLUMN, "INTERNAL_ROOM_CODE");
					input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
					result = validateEntityWiseCodes(input);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateBedCode(DTObject input) {
		DTObject result = new DTObject();
		DTObject inputOTN = new DTObject();
		String wardCode = input.get("WARD_CODE");
		String roomCode = input.get("ROOM_CODE");
		String bedCode = input.get("BED_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(wardCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(roomCode, LENGTH_1, LENGTH_8)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.TABLE, "BEDS");
			String values[] = new String[] { getContext().getEntityCode(), wardCode, roomCode, bedCode };
			input.setObject(ContentManager.CODE, values);
			String fields[] = new String[] { "ENTITY_CODE", "WARD_CODE", "ROOM_CODE", "BED_CODE" };
			input.setObject(ContentManager.COLUMN, fields);
			result = validateUKCode(input);
			if (result.get(ContentManager.ERROR) != null || result.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
				return result;
			} else {
				result.reset();
				inputOTN.set(ContentManager.ENTITY_CODE, getContext().getEntityCode());
				inputOTN.set(ContentManager.TABLE, "BEDS");
				inputOTN.set(ContentManager.KEY, wardCode + RegularConstants.PK_SEPARATOR + roomCode + RegularConstants.PK_SEPARATOR + bedCode);
				result = validateGeneralOTN(inputOTN);
				if (result.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
					input.set(ContentManager.CODE, result.get("NEW_KEY_NUMERIC_VALUE"));
					input.set(ContentManager.COLUMN, "INTERNAL_BED_CODE");
					input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
					result = validateEntityWiseCodes(input);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateInternalBedCode(DTObject input) {
		DTObject result = new DTObject();
		String internalBedCode = input.get("INTERNAL_BED_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(internalBedCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(internalBedCode, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, internalBedCode);
			input.set(ContentManager.TABLE, "BEDS");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			input.set(ContentManager.COLUMN, "INTERNAL_BED_CODE");
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateFolioNumber(DTObject input) {
		DTObject result = new DTObject();
		String wardCode = input.get("GEN_FOLIO_NUMBER");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(wardCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(wardCode, LENGTH_1, LENGTH_6)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, wardCode);
			input.set(ContentManager.TABLE, "GENFOLIO");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			input.set(ContentManager.COLUMN, "GEN_FOLIO_NUMBER");
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateStaffShits(DTObject input) {
		DTObject result = new DTObject();
		String staffShitCode = input.get("STAFF_SHIFT_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {

				if (!isValidCodePattern(staffShitCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(staffShitCode, LENGTH_1, LENGTH_10)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, staffShitCode);
			input.set(ContentManager.TABLE, "STAFFSHIFTS");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			input.set(ContentManager.COLUMN, "STAFF_SHIFT_CODE");
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validatePatientServices(DTObject input) {
		DTObject result = new DTObject();
		String staffShitCode = input.get("PAT_SERVICE_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(staffShitCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(staffShitCode, LENGTH_1, LENGTH_6)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, staffShitCode);
			input.set(ContentManager.TABLE, "PATIENTSERVICES");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			input.set(ContentManager.COLUMN, "PAT_SERVICE_CODE");
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validatePatientRegNo(DTObject input) {
		DTObject result = new DTObject();
		String regYear = input.get("PATIENT_REG_YEAR");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidLength(regYear, LENGTH_1, LENGTH_4)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			result = validPatientRegistrationNumber(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validatePersonOthinfo(DTObject input) {
		DTObject result = new DTObject();
		String personId = input.get("PERSON_ID");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidNumber(personId)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(personId, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, personId);
			input.set(ContentManager.TABLE, "PERSONOTHINFO");
			input.set(ContentManager.COLUMN, "PERSON_ID");
			result = validPersonOthinfo(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public DTObject ValidatePersonIDandLabpatcat(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		String personID = inputDTO.get("PERSON_ID");
		String labPatCatCode = inputDTO.get("LAB_PATCAT_CODE");
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		try {
			util.reset();
			util.setMode(DBUtil.CALLABLE);
			util.setSql("CALL SP_VAL_PERSONID_AND_LABPATCAT(?,?,?,?,?)");
			util.setString(1, getContext().getEntityCode());
			util.setString(2, getContext().getPartitionNo());
			util.setString(3, personID);
			util.setString(4, labPatCatCode);
			util.registerOutParameter(5, Types.VARCHAR);
			util.execute();
			String error = util.getString(5);
			resultDTO.set("LABPATERROR", error);

		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			dbContext.close();
			util.reset();
		}
		return resultDTO;
	}

	public final DTObject validateDefaultLegalConnRole(DTObject input) {
		DTObject result = new DTObject();
		String fetch = input.get(ContentManager.FETCH);
		String sqlQuery = null;
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = getDbContext().createUtilInstance();
		String action = input.get(ContentManager.USER_ACTION);
		String fetchColumns = input.get(ContentManager.FETCH_COLUMNS);
		boolean usageAllowed = false;
		try {
			if (fetch != null && fetch.equals(ContentManager.FETCH_ALL)) {
				sqlQuery = "SELECT * FROM DEFAULTLEGALCONNREL WHERE  ENTITY_CODE = ? AND TITLE_CODE = ? AND MARITAL_STATUS = ? ";
			} else {
				sqlQuery = "SELECT " + fetchColumns + ",TBA_KEY FROM DEFAULTLEGALCONNREL WHERE  ENTITY_CODE = ? AND TITLE_CODE = ? AND MARITAL_STATUS = ? ";
			}
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, getContext().getEntityCode());
			util.setString(2, input.get("TITLE_CODE"));
			util.setString(3, input.get("MARITAL_STATUS"));
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				if (usageAllowed) {
					if (USAGE.equals(action)) {
						if (rset.getString("TBA_KEY") != null) {
							result.set(ContentManager.ERROR, BackOfficeErrorCodes.USAGE_NOT_ALLOWED);
						}
					}
				}
				decodeRow(rset, result);
			} else {
				result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return result;
	}

	public final boolean validateExternalDoctor(DTObject input) {
		boolean status = false;
		String connPerRelID = input.get("CONN_PER_RELATION_ID");
		String connRoleCode = input.get("CONN_ROLE_CODE");
		DBUtil util = getDbContext().createUtilInstance();
		try {
			util.reset();
			util.setSql("SELECT CONN_ROLE_FOR_EXT_DOC FROM INSTALL WHERE PARTITION_NO=?");
			util.setString(1, getContext().getPartitionNo());
			ResultSet rset = util.executeQuery();
			if (rset.next() && connRoleCode.equalsIgnoreCase(rset.getString("CONN_ROLE_FOR_EXT_DOC"))) {
				status = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			util.reset();
		}
		return status;
	}

	public final DTObject validateWHODPCode(DTObject input) {
		DTObject result = new DTObject();
		String dpCode = input.get("DISEASE_PROC_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {

				if (!isValidCodePattern(dpCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(dpCode, LENGTH_1, LENGTH_20)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, dpCode);
			input.set(ContentManager.TABLE, "DISEASECODES");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			input.set(ContentManager.COLUMN, "DISEASE_PROC_CODE");
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateICDCode(DTObject input) {
		DTObject result = new DTObject();
		String icdCode = input.get("ICD_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {

				if (!isValidCodePattern(icdCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(icdCode, LENGTH_1, LENGTH_20)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, icdCode);
			input.set(ContentManager.TABLE, "DISEASEICDCODES");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			input.set(ContentManager.COLUMN, "ICD_CODE");
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateEntBrCode(DTObject input) {
		DTObject result = new DTObject();
		String entBrCode = input.get("ENT_BR_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidNumber(entBrCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(entBrCode, LENGTH_1, LENGTH_6)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, entBrCode);
			input.set(ContentManager.TABLE, "ENTBRCODE");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			input.set(ContentManager.COLUMN, "ENT_BR_CODE");
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateReceiptType(DTObject input) {
		DTObject result = new DTObject();
		String rcpType = input.get("RCPT_TYPE_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(rcpType)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(rcpType, LENGTH_1, LENGTH_2)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, rcpType);
			input.set(ContentManager.TABLE, "RECEIPTTYPES");
			input.set(ContentManager.COLUMN, "RCPT_TYPE_CODE");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateAutoDiscPolicyCode(DTObject input) {
		DTObject result = new DTObject();
		String autoDisc = input.get("AUTODISC_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(autoDisc)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(autoDisc, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, autoDisc);
			input.set(ContentManager.TABLE, "AUTODISCCODES");
			input.set(ContentManager.COLUMN, "AUTODISC_CODE");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;

	}

	public final DTObject validatedoctorGroupCode(DTObject input) {
		DTObject result = new DTObject();
		String doctorGroupCode = input.get("DOCTOR_GROUP_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {

				if (!isValidCodePattern(doctorGroupCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(doctorGroupCode, LENGTH_1, LENGTH_6)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, doctorGroupCode);
			input.set(ContentManager.TABLE, "DOCTORGROUP");
			input.set(ContentManager.COLUMN, "DOCTOR_GROUP_CODE");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateDocumentType(DTObject input) {
		DTObject result = new DTObject();
		String documentTypeCode = input.get("DOCUMENT_TYPE_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(documentTypeCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(documentTypeCode, LENGTH_1, LENGTH_3)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, documentTypeCode);
			input.set(ContentManager.TABLE, "DOCUMENTTYPE");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			input.set(ContentManager.COLUMN, "DOCUMENT_TYPE_CODE");
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validatePackage(DTObject input) {
		DTObject result = new DTObject();
		String pakageSIRM = input.get("PKG_SIRM_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(pakageSIRM)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(pakageSIRM, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, pakageSIRM);
			input.set(ContentManager.TABLE, "PACKAGES");
			input.set(ContentManager.COLUMN, "PKG_SIRM_CODE");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);

			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public boolean isValidPackage(DTObject input) throws SQLException {
		String sql = RegularConstants.EMPTY_STRING;
		boolean status = false;
		DBUtil dbutil = getDbContext().createUtilInstance();
		try {
			sql = "SELECT DISABLE_PKG FROM PKGCONFIG WHERE ENTITY_CODE=? AND PKG_SIRM_CODE=? ";
			dbutil.setSql(sql.toString());
			dbutil.setInt(1, Integer.parseInt(getContext().getEntityCode()));
			dbutil.setString(2, input.get("PKG_SIRM_CODE"));
			ResultSet rset = dbutil.executeQuery();
			if (rset.next()) {
				if (rset.getInt(1) == 0) {
					status = true;
				}
			}
		} finally {
			dbutil.reset();
		}
		return status;
	}

	public final DTObject validateProductCode(DTObject input) {
		DTObject result = new DTObject();
		String productCode = input.get("PRODUCT_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidNumber(productCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_NUMBER);
					return result;
				}
				if (!isValidLength(productCode, LENGTH_1, LENGTH_4)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, productCode);
			input.set(ContentManager.TABLE, "PRODUCTS");
			input.set(ContentManager.COLUMN, "PRODUCT_CODE");
			input.set(ContentManager.ALPHA_CODE, "PRODUCT_ALPHA_CODE");
			input.set(ContentManager.ENTITY_CODE, "ENTITY_CODE");
			result = validateEntityWiseAlphaCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public DTObject validateProductAlphaCode(DTObject input) {
		DTObject result = new DTObject();
		String productAlphaCode = input.get("PRODUCT_ALPHA_CODE");
		try {
			if (!isValidCodePattern(productAlphaCode)) {
				result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
				return result;
			}
			if (!isValidLength(productAlphaCode, LENGTH_1, LENGTH_8)) {
				result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
				return result;
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return result;
	}

	public final DTObject validateCustomerSegmentCode(DTObject input) {
		DTObject result = new DTObject();
		String custSegCode = input.get("CUST_SEGMENT_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {

				if (!isValidNumber(custSegCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_NUMBER);
					return result;
				}
				if (!isValidLength(custSegCode, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, custSegCode);
			input.set(ContentManager.TABLE, "CUSTOMERSEG");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			input.set(ContentManager.COLUMN, "CUST_SEGMENT_CODE");
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateAccountTypeCode(DTObject input) {
		DTObject result = new DTObject();
		String accountTypeCode = input.get("ACCOUNT_TYPE_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidNumber(accountTypeCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_NUMBER);
					return result;
				}
				if (!isValidLength(accountTypeCode, LENGTH_1, LENGTH_6)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, accountTypeCode);
			input.set(ContentManager.TABLE, "ACCOUNTTYPES");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			input.set(ContentManager.COLUMN, "ACCOUNT_TYPE_CODE");
			input.set(ContentManager.ALPHA_CODE, "ALPHA_CODE");
			result = validateEntityWiseAlphaCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateAccountSubTypeCode(DTObject input) {
		DTObject result = new DTObject();
		String accountTypeCode = input.get("ACCOUNT_TYPE_CODE");
		String accountSubTypeCode = input.get("ACCOUNT_SUBTYPE_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		String fetch = input.get(ContentManager.FETCH);
		String fetchColumns = input.get(ContentManager.FETCH_COLUMNS);
		String sqlQuery = RegularConstants.EMPTY_STRING;
		DBUtil util = getDbContext().createUtilInstance();
		boolean usageAllowed = false;
		try {
			if (action.equals(ADD)) {
				if (!isValidNumber(accountSubTypeCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_NUMBER);
					return result;
				}
				if (!isValidLength(accountSubTypeCode, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			if (action.equals(USAGE)) {
				usageAllowed = getProgramUsageAllowed(input);
			}
			if (fetch != null && fetch.equals(ContentManager.FETCH_ALL)) {
				sqlQuery = "SELECT * FROM ACCOUNTSUBTYPES  WHERE ENTITY_CODE=? AND ACCOUNT_TYPE_CODE=? AND (CAST(ACCOUNT_SUBTYPE_CODE AS CHAR)=? OR ACC_SUBTYPE_ALPHA_CODE=?) ";
			} else {
				sqlQuery = "SELECT " + fetchColumns + " ,ACCOUNT_SUBTYPE_CODE,ENABLED,TBA_KEY FROM ACCOUNTSUBTYPES  WHERE ENTITY_CODE=? AND ACCOUNT_TYPE_CODE=? AND (CAST(ACCOUNT_SUBTYPE_CODE AS CHAR)=? OR ACC_SUBTYPE_ALPHA_CODE=?) ";
			}
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, getContext().getEntityCode());
			util.setString(2, accountTypeCode);
			util.setString(3, accountSubTypeCode);
			util.setString(4, accountSubTypeCode);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				decodeRow(rset, result);
				String enabled = rset.getString("ENABLED");
				if (enabled != null && enabled.equals(RegularConstants.COLUMN_DISABLE)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.CODE_CLOSED);
				}
				if (usageAllowed) {
					if (action.equals(USAGE)) {
						if (rset.getString("TBA_KEY") != null) {
							result.set(ContentManager.ERROR, BackOfficeErrorCodes.USAGE_NOT_ALLOWED);
						}
					}
				}

			} else {
				result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
			}

		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateOperatingMode(DTObject input) {
		DTObject result = new DTObject();
		String operatingMode = input.get("OPERATING_MODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(operatingMode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(operatingMode, LENGTH_1, LENGTH_3)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, operatingMode);
			input.set(ContentManager.TABLE, "OPERATINGMODES");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			input.set(ContentManager.COLUMN, "OPERATING_MODE");
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateTranCode(DTObject input) {
		DTObject result = new DTObject();
		String tranCode = input.get("TRAN_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidNumber(tranCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_NUMBER);
					return result;
				}
				if (!isValidLength(tranCode, LENGTH_1, LENGTH_5)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, tranCode);
			input.set(ContentManager.TABLE, "TRANCODES");
			input.set(ContentManager.COLUMN, "TRAN_CODE");
			result = validateGenericCode(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateCIF(DTObject input) {
		DTObject result = new DTObject();
		String cifNo = input.get("CIF_NO");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidNumber(cifNo)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_NUMBER);
					return result;
				}
				if (!isValidLength(cifNo, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.TABLE, "CUSTOMERREG");
			String columns[] = new String[] { cifNo };
			input.setObject(ContentManager.PROGRAM_KEY, columns);
			result = validatePK(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validatePIDNumber(DTObject input) {
		DTObject result = new DTObject();
		String pidNumber = input.get("DOCUMENT_NUMBER");
		try {
			if (!isValidLength(pidNumber, LENGTH_1, LENGTH_25)) {
				result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
				return result;
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateAccountClusterCode(DTObject input) {
		DTObject result = new DTObject();
		String accClusterCode = input.get("ACNT_CLUSTER_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidNumber(accClusterCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_NUMBER);
					return result;
				}
				if (!isValidLength(accClusterCode, LENGTH_1, LENGTH_3)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, accClusterCode);
			input.set(ContentManager.TABLE, "ACNTCLUSTERS");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			input.set(ContentManager.COLUMN, "ACNT_CLUSTER_CODE");
			input.set(ContentManager.ALPHA_CODE, "CLUSTER_ALPHA_CODE");
			result = validateEntityWiseAlphaCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateClusterStateCode(DTObject input) {
		DTObject result = new DTObject();
		String clusterStateCode = input.get("CLUSTER_STATE_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(clusterStateCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(clusterStateCode, LENGTH_1, LENGTH_3)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, clusterStateCode);
			input.set(ContentManager.TABLE, "CLUSTERSTATES");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			input.set(ContentManager.COLUMN, "CLUSTER_STATE_CODE");
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateOrganizationUnitType(DTObject input) {
		DTObject result = new DTObject();
		String orgunitType = input.get("ORG_UNIT_TYPE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {

				if (!isValidCodePattern(orgunitType)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(orgunitType, LENGTH_1, LENGTH_8)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, orgunitType);
			input.set(ContentManager.TABLE, "ORGANIZAITIONUNITTYPE");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			input.set(ContentManager.COLUMN, "ORG_UNIT_TYPE");
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateBeneficiaryId(DTObject input) {
		DTObject result = new DTObject();
		String beneficiaryId = input.get("BENEFICIARY_ID");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {

				if (!isValidCodePattern(beneficiaryId)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(beneficiaryId, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, beneficiaryId);
			input.set(ContentManager.TABLE, "BENEFICIARY");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			input.set(ContentManager.COLUMN, "BENEFICIARY_ID");
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateNarrationCode(DTObject input) {
		DTObject result = new DTObject();
		String narrCode = input.get("NARR_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {

				if (!isValidCodePattern(narrCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(narrCode, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, narrCode);
			input.set(ContentManager.TABLE, "NARRCODES");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			input.set(ContentManager.COLUMN, "NARR_CODE");
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateBankingTranCode(DTObject input) {
		DTObject result = new DTObject();
		String bankingTranCode = input.get("BTRAN_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidNumber(bankingTranCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_NUMBER);
					return result;
				}
				if (!isValidLength(bankingTranCode, LENGTH_1, LENGTH_6)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, bankingTranCode);
			input.set(ContentManager.TABLE, "BTRANCODESM");
			input.set(ContentManager.COLUMN, "BTRAN_CODE");
			result = validateGenericCode(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateCashModuleParameters(DTObject input) {
		DTObject result = new DTObject();
		String fetch = input.get(ContentManager.FETCH);
		String fetchColumns = input.get(ContentManager.FETCH_COLUMNS);
		String sqlQuery = null;
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = getDbContext().createUtilInstance();
		try {
			if (fetch != null && fetch.equals(ContentManager.FETCH_ALL)) {
				sqlQuery = "SELECT * FROM CASHMODPARAM WHERE ENTITY_CODE = ?";
			} else {
				sqlQuery = "SELECT " + fetchColumns + " FROM CASHMODPARAM WHERE ENTITY_CODE = ?";
			}
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, getContext().getEntityCode());
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				decodeRow(rset, result);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		} finally {
			util.reset();
		}
		return result;
	}

	public final DTObject validateGetAccountNumber(DTObject input) {
		DTObject result = new DTObject();
		String fetch = input.get(ContentManager.FETCH);
		String fetchColumns = input.get(ContentManager.FETCH_COLUMNS);
		String sqlQuery = null;
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = getDbContext().createUtilInstance();
		try {
			if (fetch != null && fetch.equals(ContentManager.FETCH_ALL)) {
				sqlQuery = "SELECT * FROM ACCOUNTACCESS  WHERE ENTITY_CODE = ? AND PID_TYPE = ? AND PID_NO=? ";
			} else {
				sqlQuery = "SELECT " + fetchColumns + " FROM ACCOUNTACCESS  WHERE ENTITY_CODE = ? AND PID_TYPE = ? AND PID_NO=? ";
			}
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, getContext().getEntityCode());
			util.setString(2, input.get("PID_CODE"));
			util.setString(3, input.get("PID_NO"));
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				decodeRow(rset, result);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		} finally {
			util.reset();
		}
		return result;
	}

	public final DTObject validateCashInHand(DTObject input) {
		DTObject result = new DTObject();
		String fetch = input.get(ContentManager.FETCH);
		String fetchColumns = input.get(ContentManager.FETCH_COLUMNS);
		String sqlQuery = null;
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = getDbContext().createUtilInstance();
		try {
			if (fetch != null && fetch.equals(ContentManager.FETCH_ALL)) {
				sqlQuery = "SELECT * FROM CASHWP WHERE ENTITY_CODE = ? AND BRANCH_CODE = ? AND USER_ID = ?";
			} else {
				sqlQuery = "SELECT " + fetchColumns + " FROM CASHWP WHERE ENTITY_CODE = ? AND BRANCH_CODE = ? AND USER_ID = ?";
			}
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, getContext().getEntityCode());
			util.setString(2, input.get("BRANCH_CODE"));
			util.setString(3, input.get("USER_ID"));
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				decodeRow(rset, result);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
			return result;
		} finally {
			util.reset();
		}
		return result;
	}

	public final DTObject validateEmployeeCode(DTObject input) {
		DTObject result = new DTObject();
		String employeeCode = input.get("EMP_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(employeeCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(employeeCode, LENGTH_1, LENGTH_15)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, employeeCode);
			input.set(ContentManager.TABLE, "EMPDB");
			input.set(ContentManager.COLUMN, "EMP_CODE");
			input.set(ContentManager.ENTITY_CODE, "ENTITY_CODE");

			result = validateEntityWiseCodesWithoutEnabled(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public boolean isESINumberUsable(DTObject inputDTO) {
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		String sqlQuery = null;
		ResultSet rset = null;
		try {
			sqlQuery = "SELECT COUNT(1) FROM EMPDB  E  WHERE  EMP_ESI_NO=? AND FN_GETEMP_STATUS(?,E.EMP_CODE)='A' AND E.EMP_CODE<>? AND ENTITY_CODE=?";
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, inputDTO.get("EMP_ESI_NO"));
			util.setString(2, getContext().getEntityCode());
			util.setString(3, inputDTO.get("EMP_CODE"));
			util.setString(4, getContext().getEntityCode());
			rset = util.executeQuery();
			if (rset.next()) {
				if (rset.getInt(1) == 0)
					return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			dbContext.close();
		}
		return false;
	}

	public boolean isPFNumberUsable(DTObject inputDTO) {
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		String sqlQuery = null;
		ResultSet rset = null;
		try {
			sqlQuery = "SELECT COUNT(1) FROM EMPDB  E  WHERE  PF_NUMBER=? AND FN_GETEMP_STATUS(?,E.EMP_CODE)='A' AND E.EMP_CODE<>? AND ENTITY_CODE=?";
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, inputDTO.get("PF_NUMBER"));
			util.setString(2, getContext().getEntityCode());
			util.setString(3, inputDTO.get("EMP_CODE"));
			util.setString(4, getContext().getEntityCode());
			rset = util.executeQuery();
			if (rset.next()) {
				if (rset.getInt(1) == 0)
					return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			dbContext.close();
		}
		return false;
	}

	public final DTObject validateGeoRegionCode(DTObject input) {
		DTObject result = new DTObject();
		String geoRegionCode = input.get("GEO_REGION_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(geoRegionCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(geoRegionCode, LENGTH_1, LENGTH_8)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, geoRegionCode);
			input.set(ContentManager.TABLE, "GEOREGIONS");
			input.set(ContentManager.COLUMN, "GEO_REGION_CODE");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateRemittCode(DTObject input) {
		DTObject result = new DTObject();
		String remittCode = input.get("REMITT_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(remittCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(remittCode, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, remittCode);
			input.set(ContentManager.TABLE, "REMITTCODES");
			input.set(ContentManager.COLUMN, "REMITT_CODE");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateInstTypeCode(DTObject input) {
		DTObject result = new DTObject();
		String instTypeCode = input.get("INST_TYPE_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {

				if (!isValidCodePattern(instTypeCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(instTypeCode, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, instTypeCode);
			input.set(ContentManager.TABLE, "INSTTYPES");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			input.set(ContentManager.COLUMN, "INST_TYPE_CODE");
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateDocumentTypes(DTObject input) {
		DTObject result = new DTObject();
		String documentTypeCode = input.get("DOCTYPE_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(documentTypeCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(documentTypeCode, LENGTH_1, LENGTH_3)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, documentTypeCode);
			input.set(ContentManager.TABLE, "DOCTYPES");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			input.set(ContentManager.COLUMN, "DOCTYPE_CODE");
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateSchemeCode(DTObject input) {
		DTObject result = new DTObject();
		String schemeCode = input.get("SCHEME_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(schemeCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(schemeCode, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, schemeCode);
			input.set(ContentManager.TABLE, "SCHEMES");
			input.set(ContentManager.COLUMN, "SCHEME_CODE");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject ValidateDebitAccount(DTObject input) {
		DTObject result = new DTObject();
		DBUtil util = getDbContext().createUtilInstance();
		String debitAccount = input.get("ACCOUNT_NO");
		try {
			String sqlQuery = "SELECT CONCAT(A.TITLE_CODE,' ',A.NAME) NAME  FROM ACCOUNTS A,SCHEMES S WHERE A.ENTITY_CODE =S.ENTITY_CODE  AND A.ACCOUNT_UNDER_GOVT_SCHEME=S.SCHEME_CODE AND A.ACCOUNT_NO=?";
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, debitAccount);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set("NAME", rset.getString("NAME"));
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
			} else {
				result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return result;
	}

	public final DTObject validateDepositProductCode(DTObject input) {
		DTObject result = new DTObject();
		String depProdCode = input.get("DEPOSIT_PRODUCT_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(depProdCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(depProdCode, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, depProdCode);
			input.set(ContentManager.TABLE, "DEPOSITPRODUCTS");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			input.set(ContentManager.COLUMN, "DEPOSIT_PRODUCT_CODE");
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateEntityBrnCode(DTObject input) {
		DTObject result = new DTObject();
		String BRANCH_CODE = input.get("BRANCH_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(BRANCH_CODE)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(BRANCH_CODE, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, BRANCH_CODE);
			input.set(ContentManager.TABLE, "ENTITYBRN");
			input.set(ContentManager.COLUMN, "BRANCH_CODE");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);

			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateBranchCBSControl(DTObject input) {
		DTObject result = new DTObject();
		String entityOffCode = input.get("BRANCH_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(entityOffCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(entityOffCode, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, entityOffCode);
			input.set(ContentManager.TABLE, "BRNCBSCONTROL");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			input.set(ContentManager.COLUMN, "BRANCH_CODE");
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateInterBankTranType(DTObject input) {
		DTObject result = new DTObject();
		String interBankTranType = input.get("INTER_BANK_TRAN_TYPES");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(interBankTranType)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(interBankTranType, LENGTH_1, LENGTH_5)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, interBankTranType);
			input.set(ContentManager.TABLE, "INTERBANKTRANTYPES");
			input.set(ContentManager.COLUMN, "INTER_BANK_TRAN_TYPES");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);

			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateReportElement(DTObject input) {
		DTObject result = new DTObject();
		String reportElement = input.get("REPORT_ELEMENT_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(reportElement)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(reportElement, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, reportElement);
			input.set(ContentManager.TABLE, "FINRPTELEMENT");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			input.set(ContentManager.COLUMN, "REPORT_ELEMENT_CODE");
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateIntMethod(DTObject input) {
		DTObject result = new DTObject();
		String intMethod = input.get("INT_METHOD");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(intMethod)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(intMethod, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, intMethod);
			input.set(ContentManager.TABLE, "INTMETHODS");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			input.set(ContentManager.COLUMN, "INT_METHOD");
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateBankLicenceType(DTObject input) {
		DTObject result = new DTObject();
		String bankLicenceType = input.get("BANK_LIC_TYPES");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(bankLicenceType)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(bankLicenceType, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, bankLicenceType);
			input.set(ContentManager.TABLE, "BANKLICENSE");
			input.set(ContentManager.COLUMN, "BANK_LIC_TYPES");
			result = validateGenericCode(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateKYCCategoryCode(DTObject input) {
		DTObject result = new DTObject();
		String kycCategoryCode = input.get("KYC_CATEGORY");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(kycCategoryCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(kycCategoryCode, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, kycCategoryCode);
			input.set(ContentManager.TABLE, "KYCCATEGORY");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			input.set(ContentManager.COLUMN, "KYC_CATEGORY");
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateProductGroupCode(DTObject input) {
		DTObject result = new DTObject();
		String productGroupCode = input.get("PRODUCT_GROUP_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidNumber(productGroupCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_NUMBER);
					return result;
				}
				if (!isValidLength(productGroupCode, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, productGroupCode);
			input.set(ContentManager.TABLE, "PRODUCTGROUP");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			input.set(ContentManager.COLUMN, "PRODUCT_GROUP_CODE");
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public DTObject validateProductGroupAlphaCode(DTObject input) {
		DTObject result = new DTObject();
		String productAlphaCode = input.get("PRODUCT_GROUP_ALPHA_CODE");
		try {
			if (!isValidCodePattern(productAlphaCode)) {
				result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
				return result;
			}
			if (!isValidLength(productAlphaCode, LENGTH_1, LENGTH_8)) {
				result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
				return result;
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return result;
	}

	public final DTObject validateCustomerGroupType(DTObject input) {
		DTObject result = new DTObject();
		String custGrpType = input.get("CUST_GROUP_TYPE_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(custGrpType)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(custGrpType, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, custGrpType);
			input.set(ContentManager.TABLE, "CUSTGRPTYPE");
			input.set(ContentManager.COLUMN, "CUST_GROUP_TYPE_CODE");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateProductClassCode(DTObject input) {
		DTObject result = new DTObject();
		String productClassCode = input.get("PRODUCT_CLASS_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(productClassCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(productClassCode, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, productClassCode);
			input.set(ContentManager.TABLE, "PRODUCTCLASS");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			input.set(ContentManager.COLUMN, "PRODUCT_CLASS_CODE");
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateChannelCode(DTObject input) {
		DTObject result = new DTObject();
		String channelCode = input.get("CHANNEL_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(channelCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(channelCode, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, channelCode);
			input.set(ContentManager.TABLE, "CHANNELS");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			input.set(ContentManager.COLUMN, "CHANNEL_CODE");
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateAccountStatusCode(DTObject input) {
		DTObject result = new DTObject();
		String accStatusCode = input.get("ACNT_STATUS_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(accStatusCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(accStatusCode, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, accStatusCode);
			input.set(ContentManager.TABLE, "ACNTSTATUS");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			input.set(ContentManager.COLUMN, "ACNT_STATUS_CODE");
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateProductCurrency(DTObject input) {
		DTObject result = new DTObject();
		DBUtil util = getDbContext().createUtilInstance();
		try {
			String sqlQuery = "SELECT C.CCY_CODE AS CCY_CODE FROM CURRENCY C,PRODCURRDTL PCDTL WHERE PCDTL.ENTITY_CODE =?  AND PCDTL.PRODUCT_CODE =?  AND C.CCY_CODE = PCDTL.CURR_CODE";
			util.reset();
			util.setSql(sqlQuery);
			util.setLong(1, Long.parseLong(getContext().getEntityCode()));
			util.setString(2, input.get("PRODUCT_CODE"));
			ResultSet rs = util.executeQuery();
			if (rs.next()) {
				result.set("CCY_CODE", rs.getString("CCY_CODE"));
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
			} else {
				result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			util.reset();
		}
		return result;
	}

	public final DTObject validateReportLayoutCode(DTObject input) {
		DTObject result = new DTObject();
		String reportLayoutCode = input.get("REPORT_LAYOUT_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(reportLayoutCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(reportLayoutCode, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, reportLayoutCode);
			input.set(ContentManager.TABLE, "FINRPTLAYOUT");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			input.set(ContentManager.COLUMN, "REPORT_LAYOUT_CODE");
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateInterestRateTypeCode(DTObject input) {
		DTObject result = new DTObject();
		String interestRateType = input.get("INT_RATE_TYPE_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(interestRateType)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(interestRateType, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, interestRateType);
			input.set(ContentManager.TABLE, "INTRATETYPES");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			input.set(ContentManager.COLUMN, "INT_RATE_TYPE_CODE");
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateTenorSpecificationCode(DTObject input) {
		DTObject result = new DTObject();
		String tenerspecCode = input.get("TENOR_SPEC_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(tenerspecCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(tenerspecCode, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, tenerspecCode);
			input.set(ContentManager.TABLE, "TENORCODES");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			input.set(ContentManager.COLUMN, "TENOR_SPEC_CODE");
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateTaxCode(DTObject input) {
		DTObject result = new DTObject();
		String taxCode = input.get("TAX_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(taxCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(taxCode, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, taxCode);
			input.set(ContentManager.TABLE, "TAXCODES");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			input.set(ContentManager.COLUMN, "TAX_CODE");
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateInstBookCode(DTObject input) {
		DTObject result = new DTObject();
		String instTypeCode = input.get("INST_TYPE_CODE");
		String bookCode = input.get("BOOK_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(bookCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(bookCode, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.TABLE, "INSBOOKCD");
			String columns[] = new String[] { getContext().getEntityCode(), instTypeCode, bookCode };
			input.setObject(ContentManager.PROGRAM_KEY, columns);
			if (input.get(ContentManager.FETCH_COLUMNS) != null) {
				input.set(ContentManager.FETCH_COLUMNS, input.get(ContentManager.FETCH_COLUMNS) + ",ENABLED");
			}
			result = validatePK(input);
			String enabled = result.get("ENABLED");
			if (enabled != null && enabled.equals(RegularConstants.COLUMN_DISABLE)) {
				result.set(ContentManager.ERROR, BackOfficeErrorCodes.CODE_CLOSED);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateEntityCoreBrnCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		String branchCode = inputDTO.get("BRANCH_CODE");
		try {
			inputDTO.set(ContentManager.TABLE, "BRNCBSCONTROL");
			String columns[] = new String[] { getContext().getEntityCode(), branchCode };
			inputDTO.setObject(ContentManager.PROGRAM_KEY, columns);
			resultDTO = validatePK(inputDTO);

		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return resultDTO;
	}

	public final DTObject validateDepPerRestListCode(DTObject input) {
		DTObject result = new DTObject();
		String depListCode = input.get("DEP_PER_REST_LIST_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {

				if (!isValidCodePattern(depListCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(depListCode, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, depListCode);
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			input.set(ContentManager.TABLE, "DEPERRESTLIST");
			input.set(ContentManager.COLUMN, "DEP_PER_REST_LIST_CODE");
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateDepPrematureClosurePendTypeCode(DTObject input) {
		DTObject result = new DTObject();
		String depPCPenaltyTypeCode = input.get("DEP_PC_PENALTY_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(depPCPenaltyTypeCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(depPCPenaltyTypeCode, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, depPCPenaltyTypeCode);
			input.set(ContentManager.TABLE, "DEPPCPENALTYPE");
			input.set(ContentManager.COLUMN, "DEP_PC_PENALTY_CODE");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateDepPCPenaltyTypeCode(DTObject input) {
		DTObject result = new DTObject();
		String depPCPenaltyTypeCode = input.get("DEP_PC_PENALTY_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(depPCPenaltyTypeCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(depPCPenaltyTypeCode, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, depPCPenaltyTypeCode);
			input.set(ContentManager.TABLE, "DEPPCPENALTYPE");
			input.set(ContentManager.COLUMN, "DEP_PC_PENALTY_CODE");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateBankLicenseType(DTObject input) {
		DTObject result = new DTObject();
		String bankLicenseType = input.get("BANK_LIC_TYPES");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {

				if (!isValidCodePattern(bankLicenseType)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(bankLicenseType, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, bankLicenseType);
			input.set(ContentManager.TABLE, "BANKLICENSE");
			input.set(ContentManager.COLUMN, "BANK_LIC_TYPES");
			result = validateGenericCode(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	// public final DTObject validateCurrency(DTObject input) {
	// DTObject result = new DTObject();
	// String currency = input.get("CCY_CODE");
	// String action = input.get(ContentManager.USER_ACTION);
	// try {
	// if (action.equals(ADD)) {
	// if (!isValidCodePattern(currency)) {
	// result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
	// return result;
	// }
	// if (!isValidLength(currency, LENGTH_1, LENGTH_3)) {
	// result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
	// return result;
	// }
	// }
	// input.set(ContentManager.CODE, currency);
	// input.set(ContentManager.TABLE, "CURRENCY");
	// input.set(ContentManager.COLUMN, "CCY_CODE");
	//
	// result = validateGenericCode(input);
	// } catch (Exception e) {
	// e.printStackTrace();
	// result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
	// return result;
	// }
	// return result;
	// }

	public final DTObject validateBaseAddrType(DTObject input) {
		DTObject result = new DTObject();
		String baseAddrType = input.get("ADDR_TYPE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {

				if (!isValidCodePattern(baseAddrType)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(baseAddrType, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, baseAddrType);
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			input.set(ContentManager.TABLE, "ADDRESSTYPE");
			input.set(ContentManager.COLUMN, "ADDR_TYPE");
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateCountryCode(DTObject input) {
		DTObject result = new DTObject();
		String countryCode = input.get("COUNTRY_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {

				if (!isValidCodePattern(countryCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(countryCode, LENGTH_1, LENGTH_2)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, countryCode);
			input.set(ContentManager.TABLE, "COUNTRY");
			input.set(ContentManager.COLUMN, "COUNTRY_CODE");
			result = validateGenericCode(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateGeographicalUnitStructureCode(DTObject input) {
		DTObject result = new DTObject();
		String geographicalUnitStructureCode = input.get("GEO_UNIT_STRUC_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {

				if (!isValidCodePattern(geographicalUnitStructureCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(geographicalUnitStructureCode, LENGTH_1, LENGTH_3)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, geographicalUnitStructureCode);
			input.set(ContentManager.TABLE, "GEOUNITSTRUC");
			input.set(ContentManager.COLUMN, "GEO_UNIT_STRUC_CODE");
			result = validateGenericCode(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateDepositProductCodeParams(DTObject input) {
		DTObject result = new DTObject();
		String depProductCode = input.get("DEPOSIT_PRODUCT_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {

				if (!isValidCodePattern(depProductCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(depProductCode, LENGTH_1, LENGTH_4)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, depProductCode);
			input.set(ContentManager.TABLE, "DEPPRODPARAMS");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			input.set(ContentManager.COLUMN, "DEPOSIT_PRODUCT_CODE");
			result = validateEntityWiseCodesWithoutEnabled(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateAccountingYearStatus(DTObject input) {
		DTObject result = new DTObject();
		DBUtil util = getDbContext().createUtilInstance();
		try {
			String Query = "SELECT ACCOUNTING_YEAR_BOOK_STATUS FROM ACPERIODCTRL WHERE ENTITY_CODE=? AND ACCOUNTING_YEAR=?";
			util.reset();
			util.setSql(Query);
			util.setLong(1, Long.parseLong(getContext().getEntityCode()));
			util.setString(2, input.get("ACCOUNTING_YEAR"));
			ResultSet rs = util.executeQuery();
			if (rs.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				decodeRow(rs, result);
			} else
				result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);

		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return result;
	}

	public final DTObject validateAccountingQuarterStatus(DTObject input) {
		DTObject result = new DTObject();
		DBUtil util = getDbContext().createUtilInstance();
		try {
			String Query = "SELECT SUB_PERIOD_STATUS FROM ACPERIODCTRLDTL WHERE ENTITY_CODE=? AND ACCOUNTING_YEAR=?";
			util.reset();
			util.setSql(Query);
			util.setLong(1, Long.parseLong(getContext().getEntityCode()));
			util.setString(2, input.get("ACCOUNTING_YEAR"));
			ResultSet rs = util.executeQuery();
			if (rs.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				decodeRow(rs, result);
			} else
				result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);

		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return result;
	}

	public final DTObject validateCusotmerGroupType(DTObject input) {
		DTObject result = new DTObject();
		String cusotmerGroupType = input.get("CUST_GROUP_TYPE_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(cusotmerGroupType)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(cusotmerGroupType, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, cusotmerGroupType);
			input.set(ContentManager.TABLE, "CUSTGRPTYPES");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			input.set(ContentManager.COLUMN, "CUST_GROUP_TYPE_CODE");
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateAccountingYear(DTObject input) {
		DTObject result = new DTObject();
		String accYear = input.get("ACCOUNTING_YEAR");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidLength(accYear, LENGTH_1, LENGTH_4)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, accYear);
			input.set(ContentManager.TABLE, "ACPERIODCTRL");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			input.set(ContentManager.COLUMN, "ACCOUNTING_YEAR");
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public DTObject validateRelationshipCode(DTObject input) {
		DTObject result = new DTObject();
		String relationshipCode = input.get("RELATIONSHIP_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(relationshipCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(relationshipCode, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, relationshipCode);
			input.set(ContentManager.TABLE, "RELATIONSHIP");
			input.set(ContentManager.COLUMN, "RELATIONSHIP_CODE");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject ValidateCPCenterCode(DTObject input) {
		DTObject result = new DTObject();
		String cpCenterCode = input.get("CP_CENTER_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(cpCenterCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(cpCenterCode, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, cpCenterCode);
			input.set(ContentManager.TABLE, "CPCENTERCODE");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			input.set(ContentManager.COLUMN, "CP_CENTER_CODE");
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateLeaseProductCode(DTObject input) {
		DTObject result = new DTObject();
		String productCode = input.get("LEASE_PRODUCT_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCode(productCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_NUMBER);
					return result;
				}
				if (!isValidLength(productCode, LENGTH_1, LENGTH_4)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, productCode);
			input.set(ContentManager.TABLE, "LEASEPRODUCT");
			input.set(ContentManager.COLUMN, "LEASE_PRODUCT_CODE");
			input.set(ContentManager.ENTITY_CODE, "ENTITY_CODE");
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validatePortFolioCode(DTObject input) {
		DTObject result = new DTObject();
		String portFolioCode = input.get("PORTFOLIO_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(portFolioCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(portFolioCode, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, portFolioCode);
			input.set(ContentManager.TABLE, "PORTFOLIO");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			input.set(ContentManager.COLUMN, "PORTFOLIO_CODE");
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateStateCode(DTObject input) {
		DTObject result = new DTObject();
		String stateCode = input.get("STATE_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		AccessValidator validator = new AccessValidator();
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(stateCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(stateCode, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}

			DTObject installDTO = validator.getINSTALL(input);
			if (installDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
				if (stateCode.equals(installDTO.get("CENTRAL_CODE"))) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.CENTRAL_ACC_DEN);
					return result;
				}
			}
			input.set(ContentManager.CODE, stateCode);
			input.set(ContentManager.TABLE, "STATE");
			input.set(ContentManager.COLUMN, "STATE_CODE");
			input.set(ContentManager.ENTITY_CODE, "ENTITY_CODE");
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		} finally {
			validator.close();
		}
		return result;
	}

	public final DTObject validateCentralAndStateCode(DTObject input) {
		DTObject result = new DTObject();
		String stateCode = input.get("STATE_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(stateCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(stateCode, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, stateCode);
			input.set(ContentManager.TABLE, "STATE");
			input.set(ContentManager.COLUMN, "STATE_CODE");
			input.set(ContentManager.ENTITY_CODE, "ENTITY_CODE");
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateRegionCode(DTObject input) {
		DTObject result = new DTObject();
		String regionCode = input.get("REGION_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(regionCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(regionCode, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, regionCode);
			input.set(ContentManager.TABLE, "REGION");
			input.set(ContentManager.COLUMN, "REGION_CODE");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateTax(DTObject input) {
		DTObject result = new DTObject();
		String stateCode = input.get("STATE_CODE");
		String taxCode = input.get("TAX_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		String fetch = input.get(ContentManager.FETCH);
		String fetchColumns = input.get(ContentManager.FETCH_COLUMNS);
		String sqlQuery = RegularConstants.EMPTY_STRING;
		DBUtil util = getDbContext().createUtilInstance();
		boolean usageAllowed = false;
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(taxCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(taxCode, LENGTH_1, LENGTH_5)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			if (action.equals(USAGE)) {
				usageAllowed = getProgramUsageAllowed(input);
			}
			if (fetch != null && fetch.equals(ContentManager.FETCH_ALL)) {
				sqlQuery = "SELECT * FROM TAX  WHERE ENTITY_CODE=? AND STATE_CODE=? AND TAX_CODE=?";
			} else {
				sqlQuery = "SELECT " + fetchColumns + " ,TAX_CODE,ENABLED,TBA_KEY FROM TAX  WHERE ENTITY_CODE=? AND STATE_CODE=? AND TAX_CODE=?";
			}
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, getContext().getEntityCode());
			util.setString(2, stateCode);
			util.setString(3, taxCode);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				decodeRow(rset, result);
				String enabled = rset.getString("ENABLED");
				if (enabled != null && enabled.equals(RegularConstants.COLUMN_DISABLE)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.CODE_CLOSED);
				}
				if (usageAllowed) {
					if (action.equals(USAGE)) {
						if (rset.getString("TBA_KEY") != null) {
							result.set(ContentManager.ERROR, BackOfficeErrorCodes.USAGE_NOT_ALLOWED);
						}
					}
				}

			} else {
				result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
			}

		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final boolean isTaxScheduleAllowed(DTObject input) {
		String taxScheduleCode = input.get("TAX_SCHEDULE_CODE");
		DBUtil util = getDbContext().createUtilInstance();
		try {
			String sqlQuery = "SELECT 1 FROM TAXSCHEDULE WHERE ENTITY_CODE=? AND TAX_SCHEDULE_CODE=?";
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, getContext().getEntityCode());
			util.setString(2, taxScheduleCode);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			util.reset();
		}
		return false;
	}

	public final DTObject validatePortfoliocode(DTObject input) {
		DTObject result = new DTObject();
		String portCode = input.get("PORTFOLIO_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {

				if (!isValidCodePattern(portCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(portCode, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, portCode);
			input.set(ContentManager.TABLE, "PORTFOLIO");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			input.set(ContentManager.COLUMN, "PORTFOLIO_CODE");
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validatesubglHeadCode(DTObject input) {
		DTObject result = new DTObject();
		String glHeadCode = input.get("GL_HEAD_CODE");
		String subGlHeadCode = input.get("SUB_GL_HEAD_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(subGlHeadCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(subGlHeadCode, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.TABLE, "SUBGLHEAD");
			String columns[] = new String[] { getContext().getEntityCode(), glHeadCode, subGlHeadCode };
			input.setObject(ContentManager.PROGRAM_KEY, columns);
			result = validatePK(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateAssetType(DTObject input) {
		DTObject result = new DTObject();
		String assetTypeCode = input.get("ASSET_TYPE_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {

				if (!isValidCodePattern(assetTypeCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(assetTypeCode, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, assetTypeCode);
			input.set(ContentManager.TABLE, "ASSETTYPE");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			input.set(ContentManager.COLUMN, "ASSET_TYPE_CODE");
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject ValidateStaffRoleCode(DTObject input) {
		DTObject result = new DTObject();
		String staffCode = input.get("STAFF_ROLE_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(staffCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(staffCode, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, staffCode);
			input.set(ContentManager.TABLE, "STAFFROLE");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			input.set(ContentManager.COLUMN, "STAFF_ROLE_CODE");
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateBranchCode(DTObject input) {
		DTObject result = new DTObject();
		String branchCode = input.get("BRANCH_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(branchCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(branchCode, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, branchCode);
			input.set(ContentManager.TABLE, "BRANCH");
			input.set(ContentManager.COLUMN, "BRANCH_CODE");
			input.set(ContentManager.ENTITY_CODE, "ENTITY_CODE");
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateSupplierID(DTObject input) {
		DTObject result = new DTObject();
		String supplierID = input.get("SUPPLIER_ID");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(supplierID)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(supplierID, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, supplierID);
			input.set(ContentManager.TABLE, "SUPPLIER");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			input.set(ContentManager.COLUMN, "SUPPLIER_ID");
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject valildateCustomerID(DTObject input) {
		DTObject result = new DTObject();
		String customerId = input.get("CUSTOMER_ID");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidNumber(customerId)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(customerId, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, customerId);
			input.set(ContentManager.TABLE, "CUSTOMER");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			input.set(ContentManager.COLUMN, "CUSTOMER_ID");
			result = validateEntityWiseCodesWithoutEnabled(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateFieldId(DTObject input) {
		DTObject result = new DTObject();
		String fieldId = input.get("FIELD_ID");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(fieldId)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(fieldId, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, fieldId);
			input.set(ContentManager.TABLE, "ASSETFIELD");
			input.set(ContentManager.COLUMN, "FIELD_ID");
			input.set(ContentManager.ENTITY_CODE, "ENTITY_CODE");
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateBankingProcessCode(DTObject input) {
		DTObject result = new DTObject();
		String bankingProcessCode = input.get("BANK_PROCESS_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(bankingProcessCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(bankingProcessCode, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, bankingProcessCode);
			input.set(ContentManager.TABLE, "BANKPROCESSCODES");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			input.set(ContentManager.COLUMN, "BANK_PROCESS_CODE");
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateIFSCCode(DTObject input) {
		DTObject result = new DTObject();
		String ifsc = input.get("IFSC_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(ifsc)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(ifsc, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, ifsc);
			input.set(ContentManager.TABLE, "IFSC");
			input.set(ContentManager.COLUMN, "IFSC_CODE");
			input.set(ContentManager.ENTITY_CODE, "ENTITY_CODE");
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateAreaCode(DTObject input) {
		DTObject result = new DTObject();
		String regionCode = input.get("REGION_CODE");
		String areaCode = input.get("AREA_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(areaCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(areaCode, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.TABLE, "REGIONAREA");
			String columns[] = new String[] { getContext().getEntityCode(), regionCode, areaCode };
			input.setObject(ContentManager.PROGRAM_KEY, columns);
			result = validatePK(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateTaxOnStateCode(DTObject input) {
		DTObject result = new DTObject();
		String stateCode = input.get("STATE_CODE");
		String taxCode = input.get("TAX_CODE");
		String taxOnStateCode = input.get("TAX_ON_STATE_CODE");
		String action = input.get(ContentManager.ACTION);
		AccessValidator validator = new AccessValidator();
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(stateCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(stateCode, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.TABLE, "TAXONSTATEGL");
			String columns[] = new String[] { getContext().getEntityCode(), stateCode, taxCode, taxOnStateCode };
			input.setObject(ContentManager.PROGRAM_KEY, columns);
			result = validatePK(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		} finally {
			validator.close();
		}
		return result;
	}

	public final DTObject validateInvoiceCycleNumber(DTObject input) {
		DTObject result = new DTObject();
		String invoiceCycleNumber = input.get("INV_CYCLE_NUMBER");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(USAGE)) {

				if (!isValidCodePattern(invoiceCycleNumber)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(invoiceCycleNumber, LENGTH_1, LENGTH_2)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, invoiceCycleNumber);
			input.set(ContentManager.TABLE, "INVCYCLE");
			input.set(ContentManager.COLUMN, "INV_CYCLE_NUMBER");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			result = validateEntityWiseCodesWithoutEnabled(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject ValidateFieldID(DTObject input) {
		DTObject result = new DTObject();
		String fieldID = input.get("FIELD_ID");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(fieldID)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(fieldID, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, fieldID);
			input.set(ContentManager.TABLE, "ASSETFIELD");
			input.set(ContentManager.COLUMN, "FIELD_ID");
			input.set(ContentManager.ENTITY_CODE, "ENTITY_CODE");
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateLAMHierarchyCode(DTObject input) {
		DTObject result = new DTObject();
		String hierarchyCode = input.get("LAM_HIER_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(hierarchyCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(hierarchyCode, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, hierarchyCode);
			input.set(ContentManager.TABLE, "LAMHIER");
			input.set(ContentManager.COLUMN, "LAM_HIER_CODE");
			input.set(ContentManager.ENTITY_CODE, "ENTITY_CODE");
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject ValidateLeaseFld(DTObject input) {
		DTObject result = new DTObject();
		String fieldId = input.get("FIELD_ID");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(fieldId)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(fieldId, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, fieldId);
			input.set(ContentManager.TABLE, "LEASEFLD");
			input.set(ContentManager.COLUMN, "FIELD_ID");
			input.set(ContentManager.ENTITY_CODE, "ENTITY_CODE");
			result = validateEntityWiseCodesWithoutEnabled(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}
}