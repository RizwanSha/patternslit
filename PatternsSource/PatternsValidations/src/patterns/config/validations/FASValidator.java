package patterns.config.validations;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.Map;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.BindParameterType;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.objects.TableInfo;
import patterns.config.framework.database.utils.DBInfo;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.framework.web.ajax.ContentManager;

public class FASValidator extends BaseDomainValidator {

	public final DTObject validateBSSType(DTObject input) {
		DTObject result = new DTObject();
		String balSheetStructureCode = input.get("BSS_TYPE_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(balSheetStructureCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(balSheetStructureCode, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, balSheetStructureCode);
			input.set(ContentManager.TABLE, "BSSTYPES");
			input.set(ContentManager.PARTITION_NO, getContext().getPartitionNo());
			input.set(ContentManager.COLUMN, "BSS_TYPE_CODE");
			result = validateCommonCode(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateGLCategoryCode(DTObject input) {
		DTObject result = new DTObject();
		String glCatCode = input.get("GL_CAT_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(glCatCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(glCatCode, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, glCatCode);
			input.set(ContentManager.TABLE, "GLCAT");
			input.set(ContentManager.COLUMN, "GL_CAT_CODE");
			input.set(ContentManager.ENTITY_CODE, getContext().getEntityCode());
			result = validateCommonCode(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final boolean isGLCategoryHaveChildren(DTObject input) {
		String categoryCode = input.get("GL_CAT_CODE");
		String sqlQuery = "SELECT 1 FROM GLCAT WHERE PARTITION_NO = ? AND PARENT_CAT_CODE = ?";
		DBUtil util = getDbContext().createUtilInstance();
		try {
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, getContext().getPartitionNo());
			util.setString(2, categoryCode);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			util.reset();
		}
		return false;
	}

	public final DTObject validateBSHeadCode(DTObject input) {
		DTObject result = new DTObject();
		String BSSHeadCode = input.get("BS_HEAD_CODE");
		String BSSTypeCode = input.get("BS_STRUC_TYPE");
		String action = input.get("ACTION");
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		String fetch = input.get(ContentManager.FETCH);
		String fetchColumns = input.get(ContentManager.FETCH_COLUMNS);
		StringBuilder sqlQuery = new StringBuilder("");
		boolean usageAllowed = false;
		DBUtil util = getDbContext().createUtilInstance();
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(BSSHeadCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(BSSHeadCode, LENGTH_1, LENGTH_100)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}

			if (fetch != null && fetch.equals(ContentManager.FETCH_ALL)) {
				sqlQuery.append("SELECT * FROM BSHEADS WHERE PARTITION_NO = ? AND BS_STRUC_TYPE = ? AND BS_HEAD_CODE = ?");
			} else {
				sqlQuery.append("SELECT ").append(fetchColumns)
						.append(" ,ENABLED FROM BSHEADS WHERE PARTITION_NO=? AND BS_STRUC_TYPE =? AND BS_HEAD_CODE=?");
			}
			util.reset();
			util.setSql(sqlQuery.toString());
			util.setString(1, getContext().getPartitionNo());
			util.setString(2, BSSTypeCode);
			util.setString(3, BSSHeadCode);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				decodeRow(rset, result);
				String enabled = rset.getString("ENABLED");
				if (enabled != null && enabled.equals(RegularConstants.COLUMN_DISABLE)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.CODE_CLOSED);
				}
				if (usageAllowed) {
					if (action.equals(USAGE)) {
						if (rset.getString("TBA_KEY") != null) {
							result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
						}
					}
				}
			} else {
				result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		} finally {
			util.reset();
		}
		return result;
	}

	public final DTObject fetchBSHeadRootAttributes(DTObject input) {
		DTObject result = new DTObject();
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		String structureCode = input.get("BS_STRUC_TYPE");
		String headCode = input.get("BS_ROOT_HEAD");
		String sqlQuery = "SELECT * FROM BSHEADROOTATTR WHERE PARTITION_NO=? AND BS_STRUC_TYPE=? AND BS_ROOT_HEAD=?";
		DBUtil util = getDbContext().createUtilInstance();
		try {
			util.reset();
			util.setSql(sqlQuery.toString());
			util.setString(1, getContext().getPartitionNo());
			util.setString(2, structureCode);
			util.setString(3, headCode);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				decodeRow(rset, result);
			}
			util.reset();
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		} finally {
			util.reset();
		}
		return result;
	}

	public final boolean isBSHeadHaveChildren(DTObject input) {
		String bssHeadCode = input.get("BS_HEAD_CODE");
		String bssTypeCode = input.get("BS_STRUC_TYPE");
		String sqlQuery = "SELECT 1 FROM BSHEADS WHERE PARTITION_NO = ? AND BS_STRUC_TYPE = ? AND BS_PARENT_HEAD=?";
		DBUtil util = getDbContext().createUtilInstance();
		try {
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, getContext().getPartitionNo());
			util.setString(2, bssTypeCode);
			util.setString(3, bssHeadCode);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			util.reset();
		}
		return false;
	}

	public final DTObject validateGLHead(DTObject input) {
		DTObject result = new DTObject();
		String GLHeadCode = input.get("GL_HEAD_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(GLHeadCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(GLHeadCode, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, GLHeadCode);
			input.set(ContentManager.TABLE, "GLMAST");
			input.set(ContentManager.COLUMN, "GL_HEAD_CODE");
			input.set("BYPASS_USAGE_CHECK", RegularConstants.COLUMN_ENABLE);
			input.set(ContentManager.PARTITION_NO, getContext().getPartitionNo());
			result = validateCommonCode(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateGLAttributes(DTObject input) {
		DTObject result = new DTObject();
		String GLHeadCode = input.get("ROOT_GL_HEAD");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(GLHeadCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(GLHeadCode, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, GLHeadCode);
			input.set("BYPASS_USAGE_CHECK", RegularConstants.COLUMN_ENABLE);
			input.set("NOT_MAIN_TABLE", RegularConstants.COLUMN_ENABLE);
			input.set(ContentManager.TABLE, "GLMASTROOTATTR");
			input.set(ContentManager.COLUMN, "ROOT_GL_HEAD");
			input.set(ContentManager.PARTITION_NO, getContext().getPartitionNo());
			result = validateCommonCode(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final boolean isCurrencyAllowed(DTObject input) {
		String entityCode = input.get("ENTITY_CODE");
		String currencyCode = input.get("CURR_CODE");
		DBUtil util = getDbContext().createUtilInstance();
		try {
			String sqlQuery = "SELECT 1 FROM ENTCURRDTL WHERE ENTITY_CODE=? AND CURR_CODE=?";
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, entityCode);
			util.setString(2, currencyCode);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			util.reset();
		}
		return false;
	}

	public final DTObject validateGLFolioNumber(DTObject input) {
		DTObject result = new DTObject();
		String entityCode = input.get("ENTITY_CODE");
		String ledgerFolioNo = input.get("LDGR_FOLIO_NO");
		try {
			input.set(ContentManager.CODE, ledgerFolioNo);
			input.set(ContentManager.TABLE, "LEDGERFOLIO");
			input.set(ContentManager.COLUMN, "LDGR_FOLIO_NO");
			input.set(ContentManager.ENTITY_CODE, entityCode);
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject getBaseDate(DTObject input) {
		DTObject result = new DTObject();
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		String entityCode = input.get("ENTITY_CODE");
		String branchCode = input.get("BRANCH_CODE");
		DBUtil util = getDbContext().createUtilInstance();
		try {
			String sqlQuery = "SELECT TO_CHAR(BASE_DATE,?) BASE_DATE FROM FINACCCFG WHERE ENTITY_CODE=? AND BRANCH_CODE=?";
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, getContext().getDateFormat());
			util.setString(2, entityCode);
			util.setString(3, branchCode);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				result.set("BASE_DATE", rset.getString("BASE_DATE"));
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		} finally {
			util.reset();
		}
		return result;
	}

	public final DTObject getGLOpenDetails(DTObject input) {
		DTObject result = new DTObject();
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		String entityCode = input.get("ENTITY_CODE");
		String branchCode = input.get("BRANCH_CODE");
		String GLHeadCode = input.get("GL_HEAD_CODE");
		String fetch = input.get(ContentManager.FETCH);
		String fetchColumns = input.get(ContentManager.FETCH_COLUMNS);
		StringBuilder sqlQuery = new StringBuilder("");
		DBUtil util = getDbContext().createUtilInstance();
		try {

			if (fetch != null && fetch.equals(ContentManager.FETCH_ALL)) {
				sqlQuery.append("SELECT * FROM GLACOPEN WHERE ENTITY_CODE = ? AND BRANCH_CODE = ? AND GL_HEAD_CODE = ?");
			} else {
				sqlQuery.append("SELECT ").append(fetchColumns)
						.append(" FROM GLACOPEN WHERE ENTITY_CODE=? AND BRANCH_CODE =? AND GL_HEAD_CODE=?");
			}

			util.reset();
			util.setSql(sqlQuery.toString());
			util.setString(1, entityCode);
			util.setString(2, branchCode);
			util.setString(3, GLHeadCode);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				decodeRow(rset, result);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		} finally {
			util.reset();
		}
		return result;
	}

	@SuppressWarnings({ "incomplete-switch", "deprecation" })
	public final DTObject validateCommonCode(DTObject input) {
		DTObject result = new DTObject();
		String value = input.get(ContentManager.CODE);
		String column = input.get(ContentManager.COLUMN);
		String table = input.get(ContentManager.TABLE);
		String fetch = input.get(ContentManager.FETCH);
		String partitionNumber = input.get(ContentManager.PARTITION_NO);
		String entityCode = input.get("ENTITY_CODE");

		StringBuilder sqlQuery = new StringBuilder("SELECT ");
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = getDbContext().createUtilInstance();
		String action = input.get(ContentManager.USER_ACTION);
		String fetchColumns = input.get(ContentManager.FETCH_COLUMNS);
		TableInfo tableInfo = null;
		boolean bypassUssageCheck = input.containsKey("BYPASS_USAGE_CHECK") ? true : false;

		boolean isMainTable = true;
		if (input.containsKey("NOT_MAIN_TABLE")) {
			isMainTable = false;
		}
		boolean usageAllowed = false;
		try {
			if (USAGE.equals(action)) {
				usageAllowed = getProgramUsageAllowed(input);
			}

			if (fetch != null && fetch.equals(ContentManager.FETCH_ALL)) {
				sqlQuery.append(" * FROM ").append(table).append(" WHERE ");
				if (partitionNumber != null && !partitionNumber.equals(RegularConstants.EMPTY_STRING)) {
					sqlQuery.append("PARTITION_NO=? AND ");
				}
				if (entityCode != null && !entityCode.equals(RegularConstants.EMPTY_STRING)) {
					sqlQuery.append("ENTITY_CODE=? AND ");
				}

				sqlQuery.append(column).append(" = ?");
			} else {
				sqlQuery.append(fetchColumns);
				if (!bypassUssageCheck) {
					sqlQuery.append(" ,ENABLED");
				}
				if (isMainTable) {
					sqlQuery.append(" ,TBA_KEY");
				}

				sqlQuery.append(" FROM ").append(table).append(" WHERE ");
				if (partitionNumber != null && !partitionNumber.equals(RegularConstants.EMPTY_STRING)) {
					sqlQuery.append(" PARTITION_NO=? AND ");
				}
				if (entityCode != null && !entityCode.equals(RegularConstants.EMPTY_STRING)) {
					sqlQuery.append("ENTITY_CODE=? AND ");
				}
				sqlQuery.append(column).append(" = ?");
			}
			util.reset();
			util.setSql(sqlQuery.toString());
			int i = 1;
			if (partitionNumber != null && !partitionNumber.equals(RegularConstants.EMPTY_STRING)) {
				util.setString(i, getContext().getPartitionNo());
				i++;
			}
			if (entityCode != null && !entityCode.equals(RegularConstants.EMPTY_STRING)) {
				util.setString(i, entityCode);
				i++;
			}

			tableInfo = new DBInfo().getTableInfo(table);
			Map<String, BindParameterType> columnType = tableInfo.getColumnInfo().getColumnMapping();
			switch (columnType.get(column)) {
			case LONG:
				if (value == null)
					util.setNull(i, Types.NUMERIC);
				else
					util.setLong(i, Long.valueOf(value));
				break;
			case VARCHAR:
				if (value == null)
					util.setNull(i, Types.VARCHAR);
				else
					util.setString(i, value);
				break;
			case DATE:
				if (value == null)
					util.setNull(i, Types.DATE);
				else
					util.setDate(i, new java.sql.Date(((new java.util.Date(value)).getTime())));
				break;
			case INTEGER:
				if (value == null)
					util.setNull(i, Types.INTEGER);
				else
					util.setInt(i, Integer.valueOf(value));
				break;
			case BIGDECIMAL:
				if (value == null)
					util.setNull(i, Types.INTEGER);
				else
					util.setBigDecimal(i, new BigDecimal(value));
				break;
			case TIMESTAMP:
				if (value == null)
					util.setNull(i, Types.TIMESTAMP);
				else
					util.setTimestamp(i, Timestamp.valueOf(value));
				break;
			case DECIMAL:
				if (value == null)
					util.setNull(i, Types.DECIMAL);
				else
					util.setBigDecimal(i, new BigDecimal(value));
				break;
			case BIGINT:
				if (value == null)
					util.setNull(i, Types.DECIMAL);
				else
					util.setLong(i, Long.valueOf(value));
				break;
			}
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				decodeRow(rset, result);
				if (!bypassUssageCheck) {
					String enabled = rset.getString("ENABLED");
					if (enabled != null && enabled.equals(RegularConstants.COLUMN_DISABLE)) {
						result.set(ContentManager.ERROR, BackOfficeErrorCodes.CODE_CLOSED);
					}
				}
				if (isMainTable && usageAllowed) {
					if (USAGE.equals(action)) {
						if (rset.getString("TBA_KEY") != null) {
							result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
						}
					}
				}
			} else {
				result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return result;
	}

	public final DTObject validateBranchAvailability(DTObject input) {
		DTObject result = new DTObject();
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		String entityCode = input.get("ENTITY_CODE");
		String branchCode = input.get("BRANCH_CODE");
		String fetch = input.get(ContentManager.FETCH);
		String fetchColumns = input.get(ContentManager.FETCH_COLUMNS);
		StringBuilder sqlQuery = new StringBuilder("SELECT ");
		DBUtil util = getDbContext().createUtilInstance();
		try {
			if (fetch != null && fetch.equals(ContentManager.FETCH_ALL)) {
				sqlQuery.append("* FROM ");
			} else {
				sqlQuery.append(fetchColumns).append(",ENABLED FROM ");
			}
			sqlQuery.append("ENTITYBRN WHERE ENTITY_CODE=? AND BRANCH_CODE=?");
			util.reset();
			util.setSql(sqlQuery.toString());
			util.setString(1, entityCode);
			util.setString(2, branchCode);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				decodeRow(rset, result);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		} finally {
			util.reset();
		}
		return result;
	}

	/*public boolean getProgramUsageAllowed() {
		boolean usageAllowed = false;
		String processId = getContext().getProcessID().toUpperCase(getContext().getDefaultLocale());
		DBUtil util = getDbContext().createUtilInstance();
		try {
			String sqlQuery = "SELECT C.MPGM_TRANSIT_CHOICE FROM MPGM M,MPGMCONFIG C WHERE M.MPGM_ID=? AND M.MPGM_ID=C.MPGM_ID";
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, processId);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				if ("2".equals(rset.getString(1))) {
					usageAllowed = true;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			usageAllowed = false;
		} finally {
			util.reset();
		}
		return usageAllowed;
	}
*/
	public final DTObject currencyConvert(DTObject input) {
		DTObject result = new DTObject();
		String status = "F";
		result.set("STATUS", status);
		String actualCurr = input.get("ACTUAL_CURR");
		String baseCurr = input.get("BASE_CURR");
		String actualVal = input.get("ACTUAL_VALUE");
		DBUtil dbutil = getDbContext().createUtilInstance();
		String conversionCurrency;
		try {
			dbutil.reset();
			dbutil.setMode(DBUtil.CALLABLE);
			dbutil.setSql("CALL SP_CURR_CONVERT(?,?,?,?,?)");
			dbutil.setString(1, actualCurr);
			dbutil.setString(2, actualVal);
			dbutil.setString(3, baseCurr);
			dbutil.registerOutParameter(4, Types.VARCHAR);
			dbutil.registerOutParameter(5, Types.VARCHAR);
			dbutil.execute();
			conversionCurrency = dbutil.getString(4);
			status = dbutil.getString(5);
			result.set("FINAL_CURR", conversionCurrency);
			result.set("STATUS", status);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		} finally {
			dbutil.reset();
		}
		return result;
	}

	public final DTObject validateGLBaseBalance(DTObject input) {
		DTObject result = new DTObject();
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		String entityCode = input.get("ENTITY_CODE");
		String branchCode = input.get("BRANCH_CODE");
		String GLHeadCode = input.get("GL_HEAD_CODE");
		String fetch = input.get(ContentManager.FETCH);
		String fetchColumns = input.get(ContentManager.FETCH_COLUMNS);
		StringBuilder sqlQuery = new StringBuilder("");
		DBUtil util = getDbContext().createUtilInstance();
		try {
			if (fetch != null && fetch.equals(ContentManager.FETCH_ALL)) {
				sqlQuery.append("SELECT * FROM GLBASEBAL WHERE ENTITY_CODE = ? AND BRANCH_CODE = ? AND GL_HEAD_CODE = ?");
			} else {
				sqlQuery.append("SELECT ").append(fetchColumns)
						.append(" FROM GLBASEBAL WHERE ENTITY_CODE=? AND BRANCH_CODE =? AND GL_HEAD_CODE=?");
			}
			util.reset();
			util.setSql(sqlQuery.toString());
			util.setString(1, entityCode);
			util.setString(2, branchCode);
			util.setString(3, GLHeadCode);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				decodeRow(rset, result);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		} finally {
			util.reset();
		}
		return result;
	}

	public final DTObject validateFAClassificationCode(DTObject input) {
		DTObject result = new DTObject();
		String entityCode = input.get("ENTITY_CODE");
		String classificationCode = input.get("FAC_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(classificationCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(classificationCode, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}

			input.set(ContentManager.CODE, classificationCode);
			input.set(ContentManager.TABLE, "FACC");
			input.set(ContentManager.ENTITY_CODE, entityCode);
			input.set(ContentManager.COLUMN, "FAC_CODE");
			result = validateEntityWiseMasters(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final boolean isFACHaveChildren(DTObject input) {
		String entityCode = input.get("ENTITY_CODE");
		String classificationCode = input.get("FAC_CODE");
		String sqlQuery = "SELECT 1 FROM FACC WHERE ENTITY_CODE=? AND PARENT_FAC_CODE=?";
		DBUtil util = getDbContext().createUtilInstance();
		try {
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, entityCode);
			util.setString(2, classificationCode);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			util.reset();
		}
		return false;
	}

	public final DTObject fetchFACRootAttributes(DTObject input) {
		DTObject result = new DTObject();
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		String entityCode = input.get("ENTITY_CODE");
		String classificationCode = input.get("FAC_CODE");
		String sqlQuery = "SELECT * FROM FACCROOTATTR WHERE ENTITY_CODE=? AND FAC_CODE=?";
		DBUtil util = getDbContext().createUtilInstance();
		try {
			util.reset();
			util.setSql(sqlQuery.toString());
			util.setString(1, entityCode);
			util.setString(2, classificationCode);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				decodeRow(rset, result);
			}
			util.reset();
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		} finally {
			util.reset();
		}
		return result;
	}

	public final DTObject validateFADepreciationType(DTObject input) {
		DTObject result = new DTObject();
		String entityCode = input.get("ENTITY_CODE");
		String depreciationTypeCode = input.get("DEPR_TYPE_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(depreciationTypeCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(depreciationTypeCode, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, depreciationTypeCode);
			input.set(ContentManager.TABLE, "FADT");
			input.set(ContentManager.ENTITY_CODE, entityCode);
			input.set(ContentManager.COLUMN, "DEPR_TYPE_CODE");
			result = validateEntityWiseMasters(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final boolean isValidFADepreciation(String value) {
		BigDecimal maxAmount = new BigDecimal("999.99999");
		try {
			BigDecimal amountValue = new BigDecimal(value);
			if (amountValue.precision() > 8 || amountValue.scale() > 5 || (amountValue.precision() - amountValue.scale()) > 3) {
				return false;
			}
			if (amountValue.compareTo(maxAmount) > 0) {
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public final boolean isValidFADepreciationRoundOff(String value) {
		BigDecimal maxAmount = new BigDecimal("999999.999");
		try {
			BigDecimal amountValue = new BigDecimal(value);
			if (amountValue.precision() > 9 || amountValue.scale() > 3 || (amountValue.precision() - amountValue.scale()) > 6) {
				return false;
			}
			if (amountValue.compareTo(maxAmount) > 0) {
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public final boolean isValidFAResidualAmount(String value) {
		BigDecimal maxAmount = new BigDecimal("999999999999999.999");
		try {
			BigDecimal amountValue = new BigDecimal(value);
			if (amountValue.precision() > 18 || amountValue.scale() > 3 || (amountValue.precision() - amountValue.scale()) > 15) {
				return false;
			}
			if (amountValue.compareTo(maxAmount) > 0) {
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public final DTObject validateFALocation(DTObject input) {
		DTObject result = new DTObject();
		String entityCode = input.get("ENTITY_CODE");
		String locationCode = input.get("LOC_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(locationCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(locationCode, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}

			input.set(ContentManager.CODE, locationCode);
			input.set(ContentManager.TABLE, "FALOCATIONS");
			input.set(ContentManager.ENTITY_CODE, entityCode);
			input.set(ContentManager.COLUMN, "LOC_CODE");
			result = validateEntityWiseMasters(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final boolean isFALocationHaveChildren(DTObject input) {
		String entityCode = input.get("ENTITY_CODE");
		String locationCode = input.get("LOC_CODE");
		String sqlQuery = "SELECT 1 FROM FALOCATIONS WHERE ENTITY_CODE=? AND PARENT_LOC_CODE=?";
		DBUtil util = getDbContext().createUtilInstance();
		try {
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, entityCode);
			util.setString(2, locationCode);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			util.reset();
		}
		return false;
	}

	public final DTObject validateVoucherTypeCode(DTObject input) {
		DTObject result = new DTObject();
		String voucherTypeCode = input.get("VOUTYPE_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(voucherTypeCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(voucherTypeCode, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}

			input.set(ContentManager.CODE, voucherTypeCode);
			input.set(ContentManager.TABLE, "VOUCHERTYPE");
			input.set(ContentManager.ENTITY_CODE, getContext().getEntityCode());
			input.set(ContentManager.COLUMN, "VOUTYPE_CODE");
			result = validateEntityWiseMasters(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final boolean isVoucherTypeHaveChildren(DTObject input) {
		String voucherTypeCode = input.get("VOUTYPE_CODE");
		String sqlQuery = "SELECT 1 FROM VOUCHERTYPE WHERE ENTITY_CODE=? AND PARENT_VOUTYPE_CODE=?";
		DBUtil util = getDbContext().createUtilInstance();
		try {
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, getContext().getEntityCode());
			util.setString(2, voucherTypeCode);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			util.reset();
		}
		return false;
	}

	public final DTObject fetchVoucherTypeRootAttributes(DTObject input) {
		DTObject result = new DTObject();
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		String voucherTypeCode = input.get("VOUTYPE_CODE");
		String sqlQuery = "SELECT * FROM VOUTYPEROOTATTR WHERE ENTITY_CODE=? AND VOUTYPE_CODE=?";
		DBUtil util = getDbContext().createUtilInstance();
		try {
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, getContext().getEntityCode());
			util.setString(2, voucherTypeCode);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				decodeRow(rset, result);
			}
			util.reset();
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		} finally {
			util.reset();
		}
		return result;
	}

	public final DTObject validateStandardNarration(DTObject input) {
		DTObject result = new DTObject();
		String entityCode = input.get("ENTITY_CODE");
		String narrationCode = input.get("STD_NARR_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(narrationCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(narrationCode, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}

			input.set(ContentManager.CODE, narrationCode);
			input.set(ContentManager.TABLE, "STDNARR");
			input.set(ContentManager.ENTITY_CODE, entityCode);
			input.set(ContentManager.COLUMN, "STD_NARR_CODE");
			result = validateEntityWiseMasters(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject fetchGLHeadRootAttributes(DTObject input) {
		DTObject result = new DTObject();
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		String glHeadCode = input.get("ROOT_GL_HEAD");
		String sqlQuery = "SELECT * FROM GLMASTROOTATTR WHERE PARTITION_NO=? AND ROOT_GL_HEAD=?";
		DBUtil util = getDbContext().createUtilInstance();
		try {
			util.reset();
			util.setSql(sqlQuery.toString());
			util.setString(1, getContext().getPartitionNo());
			util.setString(2, glHeadCode);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				decodeRow(rset, result);
			}
			util.reset();
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		} finally {
			util.reset();
		}
		return result;
	}

	public final boolean isGLHeadHaveChildren(DTObject input) {
		String glHeadCode = input.get("GL_HEAD_CODE");
		String sqlQuery = "SELECT 1 FROM GLMAST WHERE PARTITION_NO=? AND PARENT_GL_HEAD=?";
		DBUtil util = getDbContext().createUtilInstance();
		try {
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, getContext().getPartitionNo());
			util.setString(2, glHeadCode);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			util.reset();
		}
		return false;
	}

	public final DTObject validateOrganizationCode(DTObject input) {
		DTObject result = new DTObject();
		String entityCode = input.get("ENTITY_CODE");
		String organizationCode = input.get("ORG_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(organizationCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(organizationCode, LENGTH_1, LENGTH_6)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, organizationCode);
			input.set(ContentManager.ENTITY_CODE, entityCode);
			input.set(ContentManager.TABLE, "ORGANIZATIONS");
			input.set(ContentManager.COLUMN, "ORG_CODE");
			result = validateEntityWiseMasters(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public DTObject validateEntityWiseMasters(DTObject input) {
		DTObject result = new DTObject();
		String value = input.get(ContentManager.CODE);
		String entityCode = input.get(ContentManager.ENTITY_CODE);
		String code = input.get(ContentManager.COLUMN);
		String table = input.get(ContentManager.TABLE);
		String fetch = input.get(ContentManager.FETCH);
		String fetchColumns = input.get(ContentManager.FETCH_COLUMNS);
		StringBuilder sqlQuery = new StringBuilder("");
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = getDbContext().createUtilInstance();
		String action = input.get(ContentManager.USER_ACTION);
		boolean usageAllowed = false;
		try {
			if (action.equals(USAGE)) {
				usageAllowed = getProgramUsageAllowed(input);
			}
			if (fetch != null && fetch.equals(ContentManager.FETCH_ALL)) {
				sqlQuery.append("SELECT * FROM ").append(table).append(" WHERE ENTITY_CODE=").append(" ? AND ").append(code)
						.append(" = ?");
			} else {
				sqlQuery.append("SELECT ").append(fetchColumns).append(" ,ENABLED,TBA_KEY FROM ").append(table)
						.append(" WHERE ENTITY_CODE=").append(" ? AND ").append(code).append(" = ?");
			}
			util.reset();
			util.setSql(sqlQuery.toString());
			util.setString(1, entityCode);
			util.setString(2, value);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				decodeRow(rset, result);
				String enabled = rset.getString("ENABLED");
				if (enabled != null && enabled.equals(RegularConstants.COLUMN_DISABLE)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.CODE_CLOSED);
				}
				if (usageAllowed) {
					if (action.equals(USAGE)) {
						if (rset.getString("TBA_KEY") != null) {
							result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_USAGE_NOT_ALLOWED);
						}
					}
				}
			} else {
				result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return result;
	}

	public final DTObject validateFAItemInventory(DTObject input) {
		DTObject result = new DTObject();
		String entityCode = input.get("ENTITY_CODE");
		String invNumber = input.get("FA_INV_NO");
		try {
			if (!isValidPositiveNumber(invNumber)) {
				result.set(ContentManager.ERROR, BackOfficeErrorCodes.POSITIVE_CHECK);
				return result;
			}
			input.set(ContentManager.CODE, invNumber);
			input.set(ContentManager.ENTITY_CODE, entityCode);
			input.set(ContentManager.TABLE, "FAITEMREG");
			input.set(ContentManager.COLUMN, "FA_INV_NO");
			input.set("BYPASS_USAGE_CHECK", RegularConstants.COLUMN_ENABLE);
			result = validateCommonCode(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject fetchFAItemDetails(DTObject input) {
		DTObject result = new DTObject();
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		String entityCode = input.get("ENTITY_CODE");
		String inventoryNumber = input.get("FA_INV_NO");
		String sqlQuery = "SELECT * FROM FAITEMREG WHERE ENTITY_CODE=? AND FA_INV_NO=?";
		DBUtil util = getDbContext().createUtilInstance();
		try {
			util.reset();
			util.setSql(sqlQuery.toString());
			util.setString(1, entityCode);
			util.setString(2, inventoryNumber);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				decodeRow(rset, result);
			}
			util.reset();
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		} finally {
			util.reset();
		}
		return result;
	}

	public final DTObject validateFAPhysicalInventory(DTObject input) {
		DTObject result = new DTObject();
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		String entityCode = input.get("ENTITY_CODE");
		String phyInventoryNumber = input.get("PHYSICAL_INV_NO");
		String sqlQuery = "SELECT FA_INV_NO FROM FAITEMREGDTL WHERE ENTITY_CODE=? AND PHYSICAL_INV_NO=?";
		DBUtil util = getDbContext().createUtilInstance();
		try {
			util.reset();
			util.setSql(sqlQuery.toString());
			util.setString(1, entityCode);
			util.setString(2, phyInventoryNumber);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				decodeRow(rset, result);
			}
			util.reset();
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		} finally {
			util.reset();
		}
		return result;
	}

	public final boolean isValidOtherInformation35(String information) {
		if (information == null || information.length() == LENGTH_0 || information.length() > 35)
			return false;
		return true;
	}

	public final boolean isValidOtherInformation150(String information) {
		if (information == null || information.length() == LENGTH_0 || information.length() > 150)
			return false;
		return true;
	}

	public final DTObject validateFAItemReferenceNo(DTObject input) {
		DTObject result = new DTObject();
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		String entityCode = input.get("ENTITY_CODE");
		String makeNumber = input.get("MAKER_CODE");
		String referenceNumber = input.get("ITEM_REF_NO");
		String sqlQuery = "SELECT FA_INV_NO FROM FAITEMREGDTL WHERE ENTITY_CODE=? AND MAKER_CODE=? AND ITEM_REF_NO=?";
		DBUtil util = getDbContext().createUtilInstance();
		try {
			util.reset();
			util.setSql(sqlQuery.toString());
			util.setString(1, entityCode);
			util.setString(2, makeNumber);
			util.setString(3, referenceNumber);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				decodeRow(rset, result);
			}
			util.reset();
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		} finally {
			util.reset();
		}
		return result;
	}

	public final DTObject validateManufactureCode(DTObject input) {
		DTObject result = new DTObject();
		String entityCode = input.get("ENTITY_CODE");
		String makeCode = input.get("MANUFT_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(makeCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(makeCode, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}

			input.set(ContentManager.CODE, makeCode);
			input.set(ContentManager.TABLE, "MANUFACTURER");
			input.set(ContentManager.ENTITY_CODE, entityCode);
			input.set(ContentManager.COLUMN, "MANUFT_CODE");
			result = validateEntityWiseMasters(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateNomTranReference(DTObject input) {
		DTObject result = new DTObject();
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		String batchNumber = input.get("SYS_TRAN_REFNUM_BATCH");
		String batchSl = input.get("SYS_TRAN_REFNUM_SL");
		String sqlQuery = "SELECT ENTITY_CODE,BRANCH_CODE,GL_HEAD_CODE,TO_CHAR(ENTRY_DATE,?) ENTRY_DATE,ENTRY_SL FROM NOMREV WHERE SYS_TRAN_REFNUM_BATCH=? AND SYS_TRAN_REFNUM_SL=?";
		DBUtil util = getDbContext().createUtilInstance();
		try {
			util.reset();
			util.setSql(sqlQuery.toString());
			util.setString(1, getContext().getDateFormat());
			util.setString(2, batchNumber);
			util.setString(3, batchSl);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				decodeRow(rset, result);
			}
			util.reset();
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		} finally {
			util.reset();
		}
		return result;
	}

	public final DTObject validateNomReferenceNumber(DTObject input) {
		DTObject result = new DTObject();
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		String entityCode = input.get("ENTITY_CODE");
		String glHead = input.get("GL_HEAD_CODE");
		String folioNumber = input.get("LEDG_FOLIO_NO");
		String sqlQuery = "SELECT ENTITY_CODE,BRANCH_CODE,GL_HEAD_CODE,TO_CHAR(ENTRY_DATE,?) ENTRY_DATE,ENTRY_SL FROM NOMREV WHERE ENTITY_CODE=? AND GL_HEAD_CODE=? AND LEDG_FOLIO_NO=?";
		DBUtil util = getDbContext().createUtilInstance();
		try {
			util.reset();
			util.setSql(sqlQuery.toString());
			util.setString(1, getContext().getDateFormat());
			util.setString(2, entityCode);
			util.setString(3, glHead);
			util.setString(4, folioNumber);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				decodeRow(rset, result);
			}
			util.reset();
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		} finally {
			util.reset();
		}
		return result;
	}

	public DTObject validateBankContacts(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		DBUtil util = getDbContext().createUtilInstance();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		String sqlQuery = null;
		try {
			sqlQuery = "SELECT CONTACT_INV_NO FROM BANKCONTACTS WHERE ENTITY_CODE=? AND BANK_CODE=? AND REMOVED_ON IS NULL";
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, getContext().getEntityCode());
			util.setString(2, inputDTO.get("BANK_CODE"));
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
			} else {
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return resultDTO;
	}

	public final DTObject validateBankCode(DTObject input) {
		DTObject result = new DTObject();
		String bankCode = input.get("BANK_CODE");
		String entityCode = input.get("ENTITY_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(bankCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(bankCode, LENGTH_1, LENGTH_6)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, bankCode);
			input.set(ContentManager.TABLE, "BANKS");
			input.set(ContentManager.ENTITY_CODE, entityCode);
			input.set(ContentManager.COLUMN, "BANK_CODE");
			result = validateCommonCode(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateAuditorFinYear(DTObject input) {
		DTObject result = new DTObject();
		String entityCode = input.get("ENTITY_CODE");
		String financialYear = input.get("FIN_YEAR");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			input.set(ContentManager.CODE, financialYear);
			input.set(ContentManager.TABLE, "AUDITORDTHIST");
			input.set(ContentManager.ENTITY_CODE, entityCode);
			input.set(ContentManager.COLUMN, "FIN_YEAR");
			input.set("BYPASS_USAGE_CHECK", RegularConstants.COLUMN_ENABLE);
			result = validateCommonCode(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject fetchAccountingParameter(DTObject input) {
		DTObject result = new DTObject();
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		String entityCode = input.get("ENTITY_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		String sqlQuery = "SELECT NOM_AC_ALLOWED,FA_MODULE_ALLOWED,ENABLED FROM ACCPARAM WHERE ENTITY_CODE=?";
		DBUtil util = getDbContext().createUtilInstance();
		try {
			util.reset();
			util.setSql(sqlQuery.toString());
			util.setString(1, entityCode);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				String enabled = rset.getString("ENABLED");
				if (enabled != null && enabled.equals(RegularConstants.COLUMN_DISABLE)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.CODE_CLOSED);
					return result;
				}
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				decodeRow(rset, result);
			}
			util.reset();
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		} finally {
			util.reset();
		}
		return result;
	}

	public final DTObject fetchMaximumEfftDate(DTObject input) {
		DTObject result = new DTObject();
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		String entityCode = input.get("ENTITY_CODE");
		String voucherCode = input.get("VOUTYPE_CODE");
		String sqlQuery = "SELECT TO_CHAR(MAX(EFFT_DATE),?) LATEST_EFFT_DATE FROM VTTEMPLATESHIST WHERE ENTITY_CODE=? AND VOUTYPE_CODE=? AND ENABLED='1'";
		DBUtil util = getDbContext().createUtilInstance();
		try {
			util.reset();
			util.setSql(sqlQuery.toString());
			util.setString(1, getContext().getDateFormat());
			util.setString(2, entityCode);
			util.setString(3, voucherCode);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				decodeRow(rset, result);
			}
			util.reset();
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final boolean isGLRemovalAllowed(DTObject input) {
		String entityCode = input.get("ENTITY_CODE");
		String glHeadCode = input.get("GL_HEAD_CODE");
		String sqlQuery = "SELECT M.GL_HEAD_CODE FROM GLMAST M,GLACOPEN O WHERE M.PARTITION_NO=? AND M.PARENT_GL_HEAD=? AND O.ENTITY_CODE=? AND M.GL_HEAD_CODE=O.GL_HEAD_CODE";
		DBUtil util = getDbContext().createUtilInstance();
		DBUtil innerUtil = getDbContext().createUtilInstance();
		String glBalQuery = "", tranQuery = "";
		ResultSet innerRset;
		try {
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, getContext().getPartitionNo());
			util.setString(2, glHeadCode);
			util.setString(3, entityCode);
			ResultSet rset = util.executeQuery();
			glBalQuery = "SELECT 1 FROM GLBAL WHERE ENTITY_CODE=? AND GL_HEAD_CODE=? AND CURR_BAL_ACTCURR <> 0 ";
			tranQuery = "SELECT 1 FROM FTRANENT WHERE ENTITY_CODE=? AND GL_HEAD_CODE=?";
			while (rset.next()) {
				innerUtil.reset();
				innerUtil.setSql(glBalQuery);
				innerUtil.setString(1, entityCode);
				innerUtil.setString(2, rset.getString("GL_HEAD_CODE"));
				innerRset = innerUtil.executeQuery();
				if (innerRset.next()) {
					return false;
				}

				innerUtil.reset();
				innerUtil.setSql(tranQuery);
				innerUtil.setString(1, entityCode);
				innerUtil.setString(2, rset.getString("GL_HEAD_CODE"));
				innerRset = innerUtil.executeQuery();
				if (innerRset.next()) {
					return false;
				}
			}
			util.reset();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public final DTObject validateRemittanceCode(DTObject input) {
		DTObject result = new DTObject();
		String remittanceCode = input.get("REM_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(remittanceCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(remittanceCode, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, remittanceCode);
			input.set(ContentManager.TABLE, "PAYREMCODES");
			input.set(ContentManager.PARTITION_NO, getContext().getPartitionNo());
			input.set(ContentManager.COLUMN, "REM_CODE");
			result = validateCommonCode(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final DTObject validateChequesOnHandGL(DTObject input) {
		DTObject result = new DTObject();
		String glHeadCode = input.get("GL_HEAD_CODE");
		try {
			input.set(ContentManager.CODE, glHeadCode);
			input.set(ContentManager.TABLE, "CHQSOHGLDTL");
			input.set(ContentManager.COLUMN, "GL_HEAD_CODE");
			input.set("NOT_MAIN_TABLE", "1");
			input.set("BYPASS_USAGE_CHECK", "1");
			result = validateCommonCode(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	public final boolean isValidRoundOff(String value) {
		BigDecimal maxAmount = new BigDecimal("99999.999");
		try {
			BigDecimal amountValue = new BigDecimal(value);
			if (amountValue.precision() > 8 || amountValue.scale() > 3 || (amountValue.precision() - amountValue.scale()) > 5) {
				return false;
			}
			if (amountValue.compareTo(maxAmount) > 0) {
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public final DTObject roundOffAmount(DTObject input) {
		DTObject result = new DTObject();
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		String entityCode = input.get("ENTITY_CODE");
		String currencyCode = input.get("CURR_CODE");
		String amount = input.get("AMOUNT");
		DBUtil dbUtil = getDbContext().createUtilInstance();
		try {
			dbUtil.reset();
			dbUtil.setMode(DBUtil.PREPARED);
			dbUtil.setSql("SELECT RND_FACT,RND_CHOICE,ENABLED FROM CURRO WHERE ENTITY_CODE=? AND CURR_CODE=?");
			dbUtil.setString(1, entityCode);
			dbUtil.setString(2, currencyCode);
			ResultSet rs = dbUtil.executeQuery();
			if (rs.next()) {
				if (rs.getString("ENABLED").equals(RegularConstants.COLUMN_DISABLE)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_CURR_RNDCFG_NOTAVAIL);
					return result;
				}

				String roundOffFactor = rs.getString("RND_FACT");
				String roundOffChoice = rs.getString("RND_CHOICE");
				dbUtil.reset();

				input.set("RND_FACT", roundOffFactor);
				input.set("RND_CHOICE", roundOffChoice);
				input.set("AMOUNT", amount);
				result = roundOff(input);
				return result;
			} else {
				result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_CURR_RNDCFG_NOTAVAIL);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		}
		return result;
	}
	
	public final DTObject validateNominalEntryDate(DTObject input) {
		DTObject result = new DTObject();
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		String entityCode = input.get("ENTITY_CODE");
		String branchCode = input.get("BRANCH_CODE");
		String glCode = input.get("GL_HEAD_CODE");
		String entryDate = input.get("ENTRY_DATE");
		String currencyCode = input.get("CURR_CODE");
		java.sql.Date date=new java.sql.Date(BackOfficeFormatUtils.getDate(entryDate, getContext().getDateFormat()).getTime());
		String sqlQuery = "SELECT TO_CHAR(ENTRY_DATE,?) ENTRY_DATE,TRANS_REV_ACTAMT FROM NOMENTRIES WHERE ENTITY_CODE=? AND BRANCH_CODE=? AND GL_HEAD_CODE=? AND ORG_TRAN_ACTCURR=? AND ENTRY_DATE=? AND SOURCE_KEY IS NOT NULL AND AUTH_ON IS NOT NULL";
		DBUtil util = getDbContext().createUtilInstance();
		try {
			util.reset();
			util.setSql(sqlQuery.toString());
			util.setString(1, getContext().getDateFormat());
			util.setString(2, entityCode);
			util.setString(3, branchCode);
			util.setString(4, glCode);
			util.setString(5, currencyCode);
			util.setDate(6, date);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				decodeRow(rset, result);
			}
			util.reset();
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		} finally {
			util.reset();
		}
		return result;
	}
	
	
	public final DTObject validateNomDaySerial(DTObject input) {
		DTObject result = new DTObject();
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		String entityCode = input.get("ENTITY_CODE");
		String branchCode = input.get("BRANCH_CODE");
		String glCode = input.get("GL_HEAD_CODE");
		String entryDate = input.get("ENTRY_DATE");
		String currencyCode = input.get("CURR_CODE");
		java.sql.Date date=new java.sql.Date(BackOfficeFormatUtils.getDate(entryDate, getContext().getDateFormat()).getTime());
		String daySerial = input.get("ENTRY_SL");
		String sqlQuery = "SELECT * FROM NOMENTRIES WHERE ENTITY_CODE=? AND BRANCH_CODE=? AND GL_HEAD_CODE=? AND ORG_TRAN_ACTCURR=? AND ENTRY_DATE=? AND ENTRY_SL=? AND SOURCE_KEY IS NOT NULL AND AUTH_ON IS NOT NULL";
		DBUtil util = getDbContext().createUtilInstance();
		try {
			util.reset();
			util.setSql(sqlQuery.toString());
			util.setString(1, entityCode);
			util.setString(2, branchCode);
			util.setString(3, glCode);
			util.setString(4, currencyCode);
			util.setDate(5, date);
			util.setString(6, daySerial);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				decodeRow(rset, result);
			}
			util.reset();
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		} finally {
			util.reset();
		}
		return result;
	}
	public final DTObject validateGLAccessCode(DTObject input) {
		DTObject result = new DTObject();
		String GLHeadCode = input.get("GL_HEAD_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(GLHeadCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(GLHeadCode, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, GLHeadCode);
			input.set(ContentManager.TABLE, "GLMAST");
			input.set(ContentManager.COLUMN, "GL_HEAD_CODE");
			input.set("BYPASS_USAGE_CHECK", RegularConstants.COLUMN_ENABLE);
			input.set(ContentManager.PARTITION_NO, getContext().getPartitionNo());
			result = validateCommonCode(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}
	public final DTObject validateGLHeadCode(DTObject input) {
		DTObject result = new DTObject();
		String GLHeadCode = input.get("GL_HEAD_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(GLHeadCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(GLHeadCode, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, GLHeadCode);
			input.set(ContentManager.TABLE, "GLHEAD");
			input.set(ContentManager.COLUMN, "GL_HEAD_CODE");
			input.set("BYPASS_USAGE_CHECK", RegularConstants.COLUMN_ENABLE);
			input.set(ContentManager.ENTITY_CODE, getContext().getEntityCode());
			result = validateCommonCode(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}
	public final DTObject validatesubglHeadCode(DTObject input) {
		DTObject result = new DTObject();
		String glHeadCode = input.get("GL_HEAD_CODE");
		String subGlHeadCode = input.get("SUB_GL_HEAD_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(subGlHeadCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(subGlHeadCode, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.TABLE, "SUBGLHEAD");
			String columns[] = new String[] { getContext().getEntityCode(), glHeadCode, subGlHeadCode };
			input.setObject(ContentManager.PROGRAM_KEY, columns);
			result = validatePK(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}
}
