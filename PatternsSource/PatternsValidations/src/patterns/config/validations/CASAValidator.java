package patterns.config.validations;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.framework.web.DHTMLXGridUtility;
import patterns.config.framework.web.ajax.ContentManager;

public class CASAValidator extends InterfaceValidator {
	
	public final DTObject validateAccountNumber(DTObject input) {
		DTObject result = new DTObject();
		String accountNumber = input.get("ACCOUNT_NO");
		try {
			input.set(ContentManager.TABLE, "ACCOUNTS");
			String columns[] = new String[] { getContext().getEntityCode(), accountNumber };
			input.setObject(ContentManager.PROGRAM_KEY, columns);
			result = validatePK(input);
			if (result.get(ContentManager.ERROR) != null) {
				return result;
			}
			if (ContentManager.DATA_UNAVAILABLE.equals(result.get(ContentManager.RESULT))) {
				if (RegularConstants.COLUMN_ENABLE.equals(input.get("ALT_ACNT_SEARCH_REQD"))) {	
					input.set("PID_NO", accountNumber);
					result=getAccntDetailsWithAlternateAccntNo(input);
					if (result.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE) && result.get("ACCOUNT_NO")!=null) {
						input.set(ContentManager.TABLE, "ACCOUNTS");
						String columns1[] = new String[] { getContext().getEntityCode(), result.get("ACCOUNT_NO") };
						input.setObject(ContentManager.PROGRAM_KEY, columns1);
						result = validatePK(input);
						if (result.get(ContentManager.ERROR) != null) {
							return result;
						}
					}
				}
			}else{
				result.set("ACCOUNT_NO",accountNumber);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

	
	public DTObject getAccntDetailsWithAlternateAccntNo(DTObject input) {
		DTObject result=new DTObject();
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = getDbContext().createUtilInstance();
		StringBuffer sqlQuery = new StringBuffer(RegularConstants.EMPTY_STRING);
		sqlQuery.append("SELECT '0',AC.ACCOUNT_NO,CR.NAME ");
		sqlQuery.append("FROM ACCOUNTACCESS AC ");
		sqlQuery.append("JOIN ACCOUNTHOLDERS AH ON (AH.ENTITY_CODE=? AND AH.ACCOUNT_NO=AC.ACCOUNT_NO) ");
		sqlQuery.append("JOIN CUSTOMERREG CR ON (CR.CIF_NO=AH.CIF_NO) ");
		sqlQuery.append("WHERE AC.ENTITY_CODE=? AND AC.PID_NO=? ");
		try {
			util.reset();
			util.setSql(sqlQuery.toString());
			util.setLong(1, Long.parseLong(getContext().getEntityCode()));
			util.setLong(2, Long.parseLong(getContext().getEntityCode()));
			util.setString(3, input.get("PID_NO"));
			ResultSet rset = util.executeQuery();
			DHTMLXGridUtility gridUtility = new DHTMLXGridUtility();
			int row=0;
			gridUtility.init();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				rset.last();
				if (rset.getRow() == 1) {
					result.set("ACCOUNT_NO", rset.getString(2));
					return result;
				}
				rset.first();
				do {
					row++;
					gridUtility.startRow();
					gridUtility.setCell(rset.getString(1));
					gridUtility.setCell(String.valueOf(row));
					gridUtility.setCell(rset.getString(2));
					gridUtility.setCell(rset.getString(3));
					gridUtility.endRow();
				} while (rset.next());
			}

			gridUtility.finish();
			String resultXML = gridUtility.getXML();
			result.set(ContentManager.RESULT_XML, resultXML);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return result;
	}
	
	public DTObject getAccntHolders(DTObject input) {
		DTObject result=new DTObject();
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = getDbContext().createUtilInstance();
		StringBuffer sqlQuery = new StringBuffer(RegularConstants.EMPTY_STRING);
		sqlQuery.append("SELECT AH.CIF_NO,CR.NAME,IMAGE_INV_NUMBER ");
		sqlQuery.append("FROM ACCOUNTHOLDERS AH ");
		sqlQuery.append("JOIN CUSTOMERREG CR ON (CR.CIF_NO=AH.CIF_NO) ");
		sqlQuery.append("WHERE AH.ENTITY_CODE=? AND AH.ACCOUNT_NO=? ");
		try {
			util.reset();
			util.setSql(sqlQuery.toString());
			util.setLong(1, Long.parseLong(getContext().getEntityCode()));
			util.setLong(2, Long.parseLong(input.get("ACCOUNT_NO")));
			ResultSet rset = util.executeQuery();
			DHTMLXGridUtility gridUtility = new DHTMLXGridUtility();
			int row=0;
			gridUtility.init();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				do {
					row++;
					gridUtility.startRow();
					gridUtility.setCell(String.valueOf(row));
					gridUtility.setCell(rset.getString(1));
					gridUtility.setCell(rset.getString(2));
					gridUtility.setCell("Click^javascript:viewAccntHolderPhoto(\"" + rset.getString(3) + "\")");
					gridUtility.endRow();
				} while (rset.next());
			}

			gridUtility.finish();
			String resultXML = gridUtility.getXML();
			result.set(ContentManager.RESULT_XML, resultXML);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return result;
	}
	
	public final DTObject validateCashModParam(DTObject input) {
		DTObject result = new DTObject();
		String fetch = input.get(ContentManager.FETCH);
		String fetchColumns = input.get(ContentManager.FETCH_COLUMNS);
		String sqlQuery = null;
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = getDbContext().createUtilInstance();
		try {
			if (fetch != null && fetch.equals(ContentManager.FETCH_ALL)) {
				sqlQuery = "SELECT * FROM CASHMODPARAM WHERE ENTITY_CODE = ?";
			} else {
				sqlQuery = "SELECT " + fetchColumns + " FROM CASHMODPARAM WHERE ENTITY_CODE = ?";
			}
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, getContext().getEntityCode());
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				decodeRow(rset, result);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
			return result;
		} finally {
			util.reset();
		}
		return result;
	}
	
	public final DTObject validateCashInHand(DTObject input) {
		DTObject result = new DTObject();
		String fetchColumns = input.get(ContentManager.FETCH_COLUMNS);
		String sqlQuery = null;
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = getDbContext().createUtilInstance();
		try {
			
			sqlQuery = "SELECT " + fetchColumns + " FROM CASHWP WHERE ENTITY_CODE = ? AND BRANCH_CODE = ? AND USER_ID = ?";
			
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, getContext().getEntityCode());
			util.setString(2, input.get("BRANCH_CODE"));
			util.setString(3, input.get("USER_ID"));
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				decodeRow(rset, result);
			}
		} catch(Exception e){
			e.printStackTrace();
		}finally {
			util.reset();
		}
		return result;
	}
	
	public final DTObject validatePaymentReferenceSerial(DTObject input) {
		DTObject result = new DTObject();
		String receiptDate = input.get("RECEIPT_DATE");
		String receiptSl = input.get("RECEIPT_SERIAL");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (isValidDate(receiptDate) == null) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_DATE);
					return result;
				}
			}
			input.set(ContentManager.TABLE, "CTCTRAN");
			String columns[] = new String[] { getContext().getEntityCode(), getContext().getBranchCode(), receiptDate, receiptSl };
			input.setObject(ContentManager.PROGRAM_KEY, columns);
			result = validatePK(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}
	
		
	public DTObject validGovtSchemsUnderAcType(DTObject input) {
		DTObject result=new DTObject();
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = getDbContext().createUtilInstance();
		String sqlQuery = null;
		try {
			sqlQuery = "SELECT S.SCHEME_CODE,S.DESCRIPTION,S.PID_CODE,S.ENROL_DESCRIPTION FROM SCHFORACCTYPEDTL A,SCHEMES S WHERE A.ENTITY_CODE=? AND A.ACCOUNT_TYPE_CODE=? AND A.SCHEME_CODE=? AND S.ENTITY_CODE=A.ENTITY_CODE AND  S.SCHEME_CODE=A.SCHEME_CODE AND S.SCHEME_FOR='A' ORDER BY A.SL";
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, getContext().getEntityCode());
			util.setString(2, input.get("ACCOUNT_TYPE_CODE"));
			util.setString(3, input.get("SCHEME_CODE"));
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				decodeRow(rset, result);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return result;
	}

	public DTObject getCIFNumber(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil dbutil = getDbContext().createUtilInstance();
		try {
			StringBuffer sqlQuery = new StringBuffer();
			dbutil.reset();
			dbutil.setMode(DBUtil.PREPARED);
			sqlQuery.append("SELECT C.CIF_NO FROM CUSTOMERREG C ");
			sqlQuery.append(" JOIN ACCOUNTHOLDERS AH ON C.CIF_NO = AH.CIF_NO AND AH.DTL_SL=1 ");
			sqlQuery.append(" JOIN ACCOUNTS A ON A.ENTITY_CODE = AH.ENTITY_CODE AND A.ACCOUNT_NO = AH.ACCOUNT_NO");
			sqlQuery.append(" WHERE A.ENTITY_CODE = ? AND A.ACCOUNT_NO = ?");
			dbutil.setSql(sqlQuery.toString());
			dbutil.setLong(1, Long.parseLong(getContext().getEntityCode()));
			dbutil.setLong(2, Long.parseLong(inputDTO.get("ACCOUNT_NO")));
			ResultSet rset = dbutil.executeQuery();
			if (rset.next()) {
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				resultDTO.set("CIF_NO", rset.getString("CIF_NO"));
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			dbutil.reset();
		}
		return resultDTO;
	}
	
	public String getTransactionCode(String trnCode, String trnType) throws SQLException{
        String tranCode = null;
		DBUtil util = getDbContext().createUtilInstance();
		StringBuffer sql = new StringBuffer(); 
		sql.append("SELECT T.TRAN_CODE FROM BTRANCODESDTL B,TRANCODES T ");
		sql.append("WHERE B.BTRAN_CODE=? AND T.TRAN_TYPE=?  AND B.TRAN_CODE=T.TRAN_CODE");
		try {
			util.setMode(DBUtil.PREPARED);
			util.setSql(sql.toString());
			util.setString(1, trnCode);
			util.setString(2, trnType);
			ResultSet rs = util.executeQuery();
			if (rs.next()) {
				tranCode = rs.getString("TRAN_CODE");
			}
		} finally {
			util.reset();
		}
		return tranCode;
    }
	
	public final DTObject getAccountDetails(DTObject input) {
		DTObject resultDTO = new DTObject();
		DBUtil util = getDbContext().createUtilInstance();
		resultDTO.set(ContentManager.ERROR, RegularConstants.NULL);
		try {
			util.reset();
			util.setMode(DBUtil.CALLABLE);
			util.setSql("CALL SP_GET_ACNT_DETAILS(?,?,?,?,?,?,?,?,?,?,?,?)");
			util.setLong(1, Long.parseLong(getContext().getEntityCode()));
			util.setLong(2, Long.parseLong(input.get("ACCOUNT_NO")));
			util.registerOutParameter(3, Types.VARCHAR);
			util.registerOutParameter(4, Types.VARCHAR);
			util.registerOutParameter(5, Types.VARCHAR);
			util.registerOutParameter(6, Types.DATE);
			util.registerOutParameter(7, Types.VARCHAR);
			util.registerOutParameter(8, Types.VARCHAR);
			util.registerOutParameter(9, Types.VARCHAR);
			util.registerOutParameter(10, Types.VARCHAR);
			util.registerOutParameter(11, Types.VARCHAR);
			util.registerOutParameter(12, Types.VARCHAR);
			util.execute();
			String status = util.getString(12);
			if (status.equals(RegularConstants.SP_SUCCESS)) {
				resultDTO.set("NAME", util.getString(3));
				resultDTO.set("BRANCH_CODE", util.getString(4));
				resultDTO.set("BRANCH_DESC", util.getString(5));
				resultDTO.set("OPEN_DATE", BackOfficeFormatUtils.getDate(util.getDate(6),getContext().getDateFormat()));
				resultDTO.set("OPERATING_MODE", util.getString(7));
				resultDTO.set("OPERATING_DESC", util.getString(8));
				resultDTO.set("NOOF_ACNT_HOLDERS", util.getString(9));
				resultDTO.set("CCY", util.getString(10));
				resultDTO.set("CCY_DESC", util.getString(11));
			} else {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_ACCOUNT_NUMBER);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return resultDTO;
		} finally {
			util.reset();
		}
		return resultDTO;
	}
	
	public final DTObject getAccountBalance(DTObject input) {
		DTObject resultDTO = new DTObject();
		DBUtil util = getDbContext().createUtilInstance();
		resultDTO.set(ContentManager.ERROR, RegularConstants.NULL);
		try {
			util.reset();
			util.setMode(DBUtil.CALLABLE);
			util.setSql("CALL SP_GET_ACNT_BAL(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
			util.setLong(1, Long.parseLong(getContext().getEntityCode()));
			util.setLong(2, Long.parseLong(input.get("ACCOUNT_NO")));
			util.registerOutParameter(3, Types.VARCHAR);
			util.registerOutParameter(4, Types.DECIMAL);
			util.registerOutParameter(5, Types.DECIMAL);
			util.registerOutParameter(6, Types.DECIMAL);
			util.registerOutParameter(7, Types.DECIMAL);
			util.registerOutParameter(8, Types.DECIMAL);
			util.registerOutParameter(9, Types.DECIMAL);
			util.registerOutParameter(10, Types.DECIMAL);
			util.registerOutParameter(11, Types.DECIMAL);
			util.registerOutParameter(12, Types.DECIMAL);
			util.registerOutParameter(13, Types.DECIMAL);
			util.registerOutParameter(14, Types.DECIMAL);
			util.registerOutParameter(15, Types.DECIMAL);
			util.registerOutParameter(16, Types.DECIMAL);
			util.registerOutParameter(17, Types.DECIMAL);
			util.registerOutParameter(18, Types.DECIMAL);
			util.registerOutParameter(19, Types.DECIMAL);
			util.registerOutParameter(20, Types.DECIMAL);
			util.registerOutParameter(21, Types.DECIMAL);
			util.registerOutParameter(22, Types.DECIMAL);
			util.registerOutParameter(23, Types.DECIMAL);
			util.registerOutParameter(24, Types.DECIMAL);
			util.registerOutParameter(25, Types.DECIMAL);
			util.registerOutParameter(26, Types.DECIMAL);
			util.registerOutParameter(27, Types.DECIMAL);
			util.registerOutParameter(28, Types.DECIMAL);
			util.registerOutParameter(29, Types.DECIMAL);
			util.registerOutParameter(30, Types.DECIMAL);
			util.registerOutParameter(31, Types.DECIMAL);
			util.registerOutParameter(32, Types.DECIMAL);
			util.registerOutParameter(33, Types.DECIMAL);
			util.registerOutParameter(34, Types.VARCHAR);
			util.execute();
			String error = util.getString(34);
			if (error.equals(RegularConstants.EMPTY_STRING)) {
				resultDTO.set("CURR_CODE", util.getString(3));
				resultDTO.set("AC_AUTH_BAL", util.getString(4));
				resultDTO.set("AC_DB_SUM", util.getString(5));
				resultDTO.set("AC_CR_SUM", util.getString(6));
				resultDTO.set("AC_UNAUTH_DBS", util.getString(7));
				resultDTO.set("AC_UNAUTH_CRS", util.getString(8));
				resultDTO.set("AC_FWDVAL_DBS", util.getString(9));
				resultDTO.set("AC_FWDVAL_CRS", util.getString(10));
				resultDTO.set("AC_AVL_BAL", util.getString(11));
				resultDTO.set("AC_TOT_BAL", util.getString(12));
				resultDTO.set("AC_LIEN_AMT", util.getString(13));
				resultDTO.set("BC_AUTH_BAL", util.getString(14));
				resultDTO.set("BC_DB_SUM", util.getString(15));
				resultDTO.set("BC_CR_SUM", util.getString(16));
				resultDTO.set("BC_UNAUTH_DBS", util.getString(17));
				resultDTO.set("BC_UNAUTH_CRS", util.getString(18));
				resultDTO.set("BC_FWDVAL_DBS", util.getString(19));
				resultDTO.set("BC_FWDVAL_CRS", util.getString(20));
				resultDTO.set("BC_AVL_BAL", util.getString(21));
				resultDTO.set("BC_TOT_BAL", util.getString(22));
				resultDTO.set("BC_LIEN_AMT", util.getString(23));
				resultDTO.set("HOLD_AMT", util.getString(24));
				resultDTO.set("MIN_BAL", util.getString(25));
				resultDTO.set("AC_EFFBAL", util.getString(26));
				resultDTO.set("BC_EFFBAL", util.getString(27));
				resultDTO.set("AC_CLG_CRS", util.getString(28));
				resultDTO.set("AC_CLG_DBS", util.getString(29));
				resultDTO.set("BC_CLG_CRS", util.getString(30));
				resultDTO.set("BC_CLG_DBS", util.getString(31));
				resultDTO.set("WAL_AC_BAL", util.getString(32));
				resultDTO.set("WAL_BC_BAL", util.getString(33));
				util.setMode(DBUtil.PREPARED);
				util.setSql("SELECT SUB_CCY_UNITS_IN_CCY FROM CURRENCY WHERE CCY_CODE=?");
				util.setString(1, resultDTO.get("CURR_CODE"));
				ResultSet rs = util.executeQuery();
				if (rs.next()) {
					resultDTO.set("CCY_UNITS", rs.getString(1));
				}
			} else {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_ACCOUNT_NUMBER);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return resultDTO;
		} finally {
			util.reset();
		}
		return resultDTO;
	}
}