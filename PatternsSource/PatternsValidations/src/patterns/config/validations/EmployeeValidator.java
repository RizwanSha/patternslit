package patterns.config.validations;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.ContentManager;

public class EmployeeValidator extends InterfaceValidator {

	public DTObject validateStaffCode(DTObject input) {
		DTObject result = new DTObject();
		String staffCode = input.get("STAFF_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(staffCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(staffCode, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, staffCode);
			input.set(ContentManager.TABLE, "STAFF");
			input.set(ContentManager.COLUMN, "STAFF_CODE");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			result = validateEntityWiseCodesWithoutEnabled(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}


	public final DTObject validateStaffEmpCode(DTObject input) {
		DTObject result = new DTObject();
		String staffEmpCode = input.get("STAFF_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(staffEmpCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(staffEmpCode, LENGTH_1, LENGTH_6)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, staffEmpCode);
			input.set(ContentManager.TABLE, "STAFF");
			input.set(ContentManager.COLUMN, "STAFF_CODE");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			result = validateEntityWiseCodesWithoutEnabled(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}
	
	public DTObject validateBranchCode(DTObject input) {
		DTObject result = new DTObject();
		String branchCode = input.get("BRANCH_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		try {
			if (action.equals(ADD)) {
				if (!isValidCodePattern(branchCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return result;
				}
				if (!isValidLength(branchCode, LENGTH_1, LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, branchCode);
			input.set(ContentManager.TABLE, "BRANCH");
			input.set(ContentManager.COLUMN, "BRANCH_CODE");
			input.set(ContentManager.ENTITY_CODE, ContentManager.ENTITY_CODE);
			result = validateEntityWiseCodes(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}

}