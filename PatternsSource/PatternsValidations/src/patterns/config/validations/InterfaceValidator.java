package patterns.config.validations;

import java.sql.ResultSet;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.ContentManager;

public class InterfaceValidator extends BaseDomainValidator {

	public InterfaceValidator() {
		super();
	}

	public InterfaceValidator(DBContext dbContext) {
		super(dbContext);
	}

	public final DTObject validateEntity(DTObject input) {
		DTObject result = new DTObject();
		String value = input.get("ENTITYNUM_NUMBER");
		String table = "ENTITYNUM";
		String fetch = input.get(ContentManager.FETCH);
		String sqlQuery = null;
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = getDbContext().createUtilInstance();
		try {
			if (fetch != null && fetch.equals(ContentManager.FETCH_ALL)) {
				sqlQuery = "SELECT * FROM " + table + " WHERE ENTITYNUM_NUMBER = ?";
			} else {
				sqlQuery = "SELECT ENTITYNUM_NUMBER,ENTITYNUM_DESCN FROM " + table + " WHERE ENTITYNUM_NUMBER = ?";
			}
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, value);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				decodeRow(rset, result);
			} else {
				result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return result;
	}

	public final DTObject validateTitle(DTObject input) {
		DTObject result = new DTObject();
		String value = input.get("TITLES_CODE");
		String table = "TITLES";
		String fetch = input.get(ContentManager.FETCH);
		String sqlQuery = null;
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = getDbContext().createUtilInstance();
		try {
			if (fetch != null && fetch.equals(ContentManager.FETCH_ALL)) {
				sqlQuery = "SELECT * FROM " + table + " WHERE TITLES_CODE = ?";
			} else {
				sqlQuery = "SELECT TITLES_CODE,TITLES_DESCN FROM " + table + " WHERE TITLES_CODE = ?";
			}
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, value);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				decodeRow(rset, result);
			} else {
				result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return result;
	}

	public final DTObject validateBranch(DTObject input) {
		DTObject result = new DTObject();
		String value = input.get("MBRN_CODE");
		String table = "MBRN";
		String fetch = input.get(ContentManager.FETCH);
		String sqlQuery = null;
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		if (!isValidNumber(value)) {
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
			return result;
		}

		DBUtil util = getDbContext().createUtilInstance();
		try {
			if (fetch != null && fetch.equals(ContentManager.FETCH_ALL)) {
				sqlQuery = "SELECT * FROM " + table + " WHERE MBRN_ENTITY_NUM = ? AND MBRN_CODE = ? AND MBRN_CLOSURE_DATE IS NULL";
			} else {
				sqlQuery = "SELECT MBRN_CODE,MBRN_NAME FROM " + table + " WHERE MBRN_ENTITY_NUM = ? AND MBRN_CODE = ? AND MBRN_CLOSURE_DATE IS NULL";
			}
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, getContext().getEntityCode());
			util.setString(2, value);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				decodeRow(rset, result);
			} else {
				result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return result;
	}

	public final DTObject validateClearingHouse(DTObject input) {
		DTObject result = new DTObject();
		String value = input.get("CLGHOUSE_CODE");
		String value1 = input.get("CLGHOUSE_CLIENT_NUM");
		String table = "CLGHOUSE";
		String fetch = input.get(ContentManager.FETCH);
		String sqlQuery = null;
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = getDbContext().createUtilInstance();
		try {
			if (fetch != null && fetch.equals(ContentManager.FETCH_ALL)) {
				sqlQuery = "SELECT * FROM " + table + " WHERE CLGHOUSE_CODE = ? AND CLGHOUSE_CLIENT_NUM=?";
			} else {
				sqlQuery = "SELECT CLGHOUSE_CODE,CLGHOUSE_NAME FROM " + table + " WHERE CLGHOUSE_CODE = ? AND CLGHOUSE_CLIENT_NUM=?";
			}
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, value);
			util.setString(2, value1);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				decodeRow(rset, result);
			} else {
				result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return result;
	}

	public final DTObject validateProductGroup(DTObject input) {
		DTObject result = new DTObject();
		String value = input.get("PRODGRP_CODE");
		String table = "PRODGROUPS";
		String fetch = input.get(ContentManager.FETCH);
		String sqlQuery = null;
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = getDbContext().createUtilInstance();
		try {
			if (fetch != null && fetch.equals(ContentManager.FETCH_ALL)) {
				sqlQuery = "SELECT * FROM " + table + " WHERE PRODGRP_CODE = ?";
			} else {
				sqlQuery = "SELECT PRODGRP_CODE,PRODGRP_DESCN FROM " + table + " WHERE PRODGRP_CODE = ?";
			}
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, value);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				decodeRow(rset, result);
			} else {
				result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return result;
	}

	public final DTObject validateProduct(DTObject input) {
		DTObject result = new DTObject();
		String value = input.get("PRODUCT_CODE");
		String table = "PRODUCTS";
		String fetch = input.get(ContentManager.FETCH);
		String sqlQuery = null;
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = getDbContext().createUtilInstance();
		try {
			if (fetch != null && fetch.equals(ContentManager.FETCH_ALL)) {
				sqlQuery = "SELECT * FROM " + table + " WHERE PRODUCT_CODE = ? AND PRODUCT_REVOKED_ON IS NULL";
			} else {
				sqlQuery = "SELECT PRODUCT_CODE,PRODUCT_NAME FROM " + table + " WHERE PRODUCT_CODE = ? AND PRODUCT_REVOKED_ON IS NULL";
			}
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, value);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				decodeRow(rset, result);
			} else {
				result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return result;
	}

	public final DTObject validateAccountType(DTObject input) {
		DTObject result = new DTObject();
		String value = input.get("ACTYPE_CODE");
		String table = "ACTYPES";
		String fetch = input.get(ContentManager.FETCH);
		String sqlQuery = null;
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = getDbContext().createUtilInstance();
		try {
			if (fetch != null && fetch.equals(ContentManager.FETCH_ALL)) {
				sqlQuery = "SELECT * FROM " + table + " WHERE ACTYPE_CODE = ?";
			} else {
				sqlQuery = "SELECT ACTYPE_CODE,ACTYPE_DESCN FROM " + table + " WHERE ACTYPE_CODE = ?";
			}
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, value);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				decodeRow(rset, result);
			} else {
				result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return result;
	}

	public final DTObject validateAccountSubType(DTObject input) {
		DTObject result = new DTObject();
		String accountTypeCode = input.get("ACSUB_ACTYPE_CODE");
		String value = input.get("ACSUB_SUBTYPE_CODE");
		String table = "ACSUBTYPES";
		String fetch = input.get(ContentManager.FETCH);
		String sqlQuery = null;
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = getDbContext().createUtilInstance();
		try {
			if (fetch != null && fetch.equals(ContentManager.FETCH_ALL)) {
				sqlQuery = "SELECT * FROM " + table + " WHERE ACSUB_ACTYPE_CODE = ? AND ACSUB_SUBTYPE_CODE = ?";
			} else {
				sqlQuery = "SELECT ACSUB_ACTYPE_CODE,ACSUB_SUBTYPE_CODE,ACSUB_DESCN FROM " + table + " WHERE ACSUB_ACTYPE_CODE = ? AND ACSUB_SUBTYPE_CODE = ?";
			}
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, accountTypeCode);
			util.setString(2, value);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				decodeRow(rset, result);
			} else {
				result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return result;
	}

	public final DTObject validateIndustry(DTObject input) {
		DTObject result = new DTObject();
		String value = input.get("INDUSTRY_CODE");
		String table = "INDUSTRIES";
		String fetch = input.get(ContentManager.FETCH);
		String sqlQuery = null;
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = getDbContext().createUtilInstance();
		try {
			if (fetch != null && fetch.equals(ContentManager.FETCH_ALL)) {
				sqlQuery = "SELECT * FROM " + table + " WHERE INDUSTRY_CODE = ?";
			} else {
				sqlQuery = "SELECT INDUSTRY_CODE,INDUSTRY_NAME FROM " + table + " WHERE INDUSTRY_CODE = ?";
			}
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, value);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				decodeRow(rset, result);
			} else {
				result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return result;
	}

	public final DTObject validateSubIndustry(DTObject input) {
		DTObject result = new DTObject();
		String industryCode = input.get("SUBINDUS_CODE");
		String value = input.get("SUBINDUS_SUB_CODE");
		String table = "SUBINDUSTRIES";
		String fetch = input.get(ContentManager.FETCH);
		String sqlQuery = null;
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = getDbContext().createUtilInstance();
		try {
			if (fetch != null && fetch.equals(ContentManager.FETCH_ALL)) {
				sqlQuery = "SELECT * FROM " + table + " WHERE SUBINDUS_CODE = ? AND SUBINDUS_SUB_CODE = ?";
			} else {
				sqlQuery = "SELECT SUBINDUS_CODE,SUBINDUS_SUB_CODE,SUBINDUS_INDUS_NAME FROM " + table + " WHERE SUBINDUS_CODE = ? AND SUBINDUS_SUB_CODE = ?";
			}
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, industryCode);
			util.setString(2, value);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				decodeRow(rset, result);
			} else {
				result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return result;
	}

	public final DTObject validateConstitution(DTObject input) {
		DTObject result = new DTObject();
		String value = input.get("CONST_CODE");
		String table = "CONSTITUTIONS";
		String fetch = input.get(ContentManager.FETCH);
		String sqlQuery = null;
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = getDbContext().createUtilInstance();
		try {
			if (fetch != null && fetch.equals(ContentManager.FETCH_ALL)) {
				sqlQuery = "SELECT * FROM " + table + " WHERE CONST_CODE = ?";
			} else {
				sqlQuery = "SELECT CONST_CODE,CONST_DESCN FROM " + table + " WHERE CONST_CODE = ?";
			}
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, value);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				decodeRow(rset, result);
			} else {
				result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return result;
	}

	public final DTObject validateCBSServiceCode(DTObject input) {
		DTObject result = new DTObject();
		String value = input.get("SERVCD_CODE");
		String table = "SERVCD";
		String fetch = input.get(ContentManager.FETCH);
		String sqlQuery = null;
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = getDbContext().createUtilInstance();
		try {
			if (fetch != null && fetch.equals(ContentManager.FETCH_ALL)) {
				sqlQuery = "SELECT * FROM " + table + " WHERE SERVCD_ENTITY_NUM = ? AND SERVCD_CODE = ?";
			} else {
				sqlQuery = "SELECT SERVCD_CODE,SERVCD_DESCN FROM " + table + " WHERE SERVCD_ENTITY_NUM = ? AND SERVCD_CODE = ?";
			}
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, getContext().getEntityCode());
			util.setString(2, value);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				decodeRow(rset, result);
			} else {
				result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return result;
	}

	public final DTObject validateTypeOfTaxPayment(DTObject input) {
		DTObject result = new DTObject();
		String serviceCode = input.get("SRVTXTP_SRV_CODE");
		String typeTaxPymt = input.get("SRVTXTP_TAX_PAY_CODE");
		String table = "SRVTAXTYPE";
		String fetch = input.get(ContentManager.FETCH);
		String sqlQuery = null;
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = getDbContext().createUtilInstance();
		try {
			if (fetch != null && fetch.equals(ContentManager.FETCH_ALL)) {
				sqlQuery = "SELECT * FROM " + table + " WHERE SRVTXTP_ENTITY_NUM = ? AND SRVTXTP_SRV_CODE = ? AND SRVTXTP_TAX_PAY_CODE = ?";
			} else {
				sqlQuery = "SELECT SRVTXTP_SRV_CODE,SRVTXTP_TAX_PAY_CODE,SRVTXTP_TAX_PAY_DESCN FROM " + table + " WHERE SRVTXTP_ENTITY_NUM = ? AND SRVTXTP_SRV_CODE = ? AND SRVTXTP_TAX_PAY_CODE = ?";
			}
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, getContext().getEntityCode());
			util.setString(2, serviceCode);
			util.setString(3, typeTaxPymt);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				decodeRow(rset, result);

			} else {
				result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return result;
	}


	public final DTObject validateIFSC(DTObject input) {
		DTObject result = new DTObject();
		String value = input.get("IFSC_CODE");
		String fetch = input.get(ContentManager.FETCH);
		String sqlQuery = null;
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = getDbContext().createUtilInstance();
		try {
			if (fetch != null && fetch.equals(ContentManager.FETCH_ALL)) {
				sqlQuery = "SELECT * FROM IFSRID WHERE IFSRID_IFS_CODE = ?";
			} else {
				sqlQuery = "SELECT IFSRID_IFS_CODE,IFSRID_BRN_NAME FROM IFSRID WHERE IFSRID_IFS_CODE = ?";
			}
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, value);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				decodeRow(rset, result);
			} else {
				result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return result;
	}

	public final DTObject validateDDPOLocations(DTObject input) {
		DTObject result = new DTObject();
		String value = input.get("LOCATION_CODE");
		String fetch = input.get(ContentManager.FETCH);
		String sqlQuery = null;
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = getDbContext().createUtilInstance();
		try {

			if (fetch != null && fetch.equals(ContentManager.FETCH_ALL)) {
				sqlQuery = "SELECT * FROM DDLOCATION WHERE FADDL_ENTITY_NUM = ? AND FADDL_LOC_CODE = ?";
			} else {
				sqlQuery = "SELECT FADDL_LOC_CODE,FADDL_LOC_NAME FROM DDLOCATION WHERE FADDL_ENTITY_NUM =? AND FADDL_LOC_CODE = ?";
			}
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, getContext().getEntityCode());
			util.setString(2, value);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				decodeRow(rset, result);
			} else {
				result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return result;
	}

	public final DTObject validateFormatCode(DTObject input) {
		DTObject result = new DTObject();
		String value = input.get(ContentManager.CODE);
		String table = "CMUPLDFMT";
		String fetch = input.get(ContentManager.FETCH);
		String sqlQuery = null;
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = getDbContext().createUtilInstance();
		try {
			if (fetch != null && fetch.equals(ContentManager.FETCH_ALL)) {
				sqlQuery = "SELECT * FROM " + table + "	WHERE CMUPLDFMT_CODE = ?";
			} else {
				sqlQuery = "SELECT CMUPLDFMT_CODE,CMUPLDFMT_DESCN FROM " + table + "WHERER CMUPLDFMT_CODE = ?";
			}
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, value);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				decodeRow(rset, result);
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return result;
	}

	public final boolean isCustomerPrefixAllowed(String userPrefix) {

		DBUtil util = getDbContext().createUtilInstance();
		String sqlQuery = RegularConstants.EMPTY_STRING;
		ResultSet rset = null;
		try {
			sqlQuery = "SELECT 1 FROM RESTRICTPFXDTL WHERE ENTITY_CODE=? AND EFFT_DATE =(SELECT EFFT_DATE FROM RESTRICTPFX WHERE ENTITY_CODE=? AND EFFT_DATE = (SELECT MAX(EFFT_DATE) FROM RESTRICTPFX WHERE ENTITY_CODE = ? AND EFFT_DATE <= FN_GETCD(?))AND PFX_RESTRICT_REQD = '1')AND PFX=?";
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, getContext().getEntityCode());
			util.setString(2, getContext().getEntityCode());
			util.setString(3, getContext().getEntityCode());
			util.setString(4, getContext().getEntityCode());
			util.setString(5, userPrefix);
			rset = util.executeQuery();
			if (rset.next()) {
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			util.reset();
		}
		return false;
	}

	public final boolean isCustomerPrefixConsumed(String customerCode, String customerPrefix) {
		DBUtil util = getDbContext().createUtilInstance();
		String sqlQuery = RegularConstants.EMPTY_STRING;
		try {
			sqlQuery = "SELECT 1 FROM CUSTOMERPFX WHERE  CUSTOMER_PFX = ? AND CUSTOMER_CODE <> ?";
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, customerPrefix);
			util.setString(2, customerCode);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			util.reset();
		}
		return false;
	}

	public final DTObject validateIssueType(DTObject input) {
		DTObject result = new DTObject();
		String value = input.get("ISSUE_TYPE_CODE");
		String table = "ISSUETYPE";
		String fetch = input.get(ContentManager.FETCH);
		String sqlQuery = null;
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = getDbContext().createUtilInstance();
		try {
			if (fetch != null && fetch.equals(ContentManager.FETCH_ALL)) {
				sqlQuery = "SELECT * FROM " + table + " WHERE ISSUE_ENTITY_NUM = ? AND ISSUE_TYPE_CODE = ? ";
			} else {
				sqlQuery = "SELECT ISSUE_TYPE_CODE,ISSUE_DESCN FROM " + table + " WHERE ISSUE_ENTITY_NUM = ? AND ISSUE_TYPE_CODE = ? ";
			}
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, getContext().getEntityCode());
			util.setString(2, value);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				decodeRow(rset, result);
			} else {
				result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return result;
	}

	/*
	 * public final DTObject validateInterface(DTObject inputDTO) { inputDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE); SyncRequestResponseInteractionHandler handler = new SyncRequestResponseInteractionHandler(); handler.setProcessingCode(InterfaceProcessingCodes.DATA_VALIDATION); inputDTO.set(InterfaceConstants.ENTITY_CODE, getContext().getEntityCode());
	 * handler.setRequestDTO(inputDTO); try { handler.process(); } catch (Exception e) { e.printStackTrace(); getLogger().logError("validateInterface(1)  : " + e.getLocalizedMessage()); inputDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR); return inputDTO; } inputDTO.reset(); inputDTO = handler.getResponseDTO(); String userMessage =
	 * inputDTO.get(InterfaceConstants.USER_MESSAGE); String errorCode = inputDTO.get(InterfaceConstants.ERROR_CODE); if (!isEmpty(errorCode)) { inputDTO.set(ContentManager.ERROR, errorCode); } else { errorCode = RegularConstants.NULL; } String errorCodeValue = inputDTO.get(ContentManager.ERROR); if (isEmpty(errorCodeValue)) { inputDTO.set(ContentManager.ERROR, RegularConstants.NULL); } if
	 * (userMessage == RegularConstants.NULL) { return inputDTO; } else { inputDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE); return inputDTO; } }
	 */

}