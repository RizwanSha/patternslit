package patterns.config.web.interceptors.access;

import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.AJAXInterceptor;
import patterns.config.framework.web.ajax.ContentManager;

public class erolebrnlistinterceptor extends AJAXInterceptor {
	public DTObject processLookupRequest(DTObject input) {
		String programID = input.get(ContentManager.PROGRAM_KEY);
		String tokenID = input.get(ContentManager.TOKEN_KEY);
		if (programID.equals("EROLEBRNLIST") && tokenID.equals("BANKROLES")) {
			if (isInternalAdministrator()) {
				input.set(ContentManager.PROGRAM_KEY, "COMMON");
				input.set(ContentManager.TOKEN_KEY, "BANKROLES_INTERNAL");
			}
		}
		if (programID.equals("EROLEBRNLIST") && tokenID.equals("BRANCHLIST")) {
			input.set(ContentManager.PROGRAM_KEY, "COMMON");
			input.set(ContentManager.TOKEN_KEY, "BRANCHLIST");
		}
		return input;
	}

	public DTObject processRecordFinderRequest(DTObject input) {
		throw new UnsupportedOperationException();
	}
}
