package patterns.config.web.interceptors.access;

import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.AJAXInterceptor;
import patterns.config.framework.web.ajax.ContentManager;

public class ebkannouncedtlsinterceptor extends AJAXInterceptor {
	public DTObject processLookupRequest(DTObject input) {
		String programID = input.get(ContentManager.PROGRAM_KEY);
		String tokenID = input.get(ContentManager.TOKEN_KEY);
		if (programID.equals("EBKANNOUNCEDTLS") && tokenID.equals("USERS")) {
			input.set(ContentManager.PROGRAM_KEY, "COMMON");
			input.set(ContentManager.TOKEN_KEY, "BANKUSERS");
		}
		if (programID.equals("EBKANNOUNCEDTLS") && tokenID.equals("BRANCHES")) {
			input.set(ContentManager.PROGRAM_KEY, "COMMON");
			input.set(ContentManager.TOKEN_KEY, "BRANCHES");
		}
		if (programID.equals("EBKANNOUNCEDTLS") && tokenID.equals("CUSTOMERS")) {
			input.set(ContentManager.PROGRAM_KEY, "COMMON");
			input.set(ContentManager.TOKEN_KEY, "CUSTOMERS_SELFADMIN");
		}
		return input;
	}

	public DTObject processRecordFinderRequest(DTObject input) {
		throw new UnsupportedOperationException();
	}
}
