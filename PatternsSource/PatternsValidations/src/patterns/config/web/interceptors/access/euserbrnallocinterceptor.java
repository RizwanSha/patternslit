package patterns.config.web.interceptors.access;

import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.AJAXInterceptor;
import patterns.config.framework.web.ajax.ContentManager;

public class euserbrnallocinterceptor extends AJAXInterceptor {

	public DTObject processLookupRequest(DTObject input) {
		String programID = input.get(ContentManager.PROGRAM_KEY);
		String tokenID = input.get(ContentManager.TOKEN_KEY);
		if (programID.equals("EUSERBRNALLOC") && tokenID.equals("BANKUSERS")) {
			if (isInternalAdministrator()) {
				input.set(ContentManager.PROGRAM_KEY, "COMMON");
				input.set(ContentManager.TOKEN_KEY, "BANKUSERS_INTERNAL");
			}
			if (isInternalOperations()) {
				input.set(ContentManager.PROGRAM_KEY, "COMMON");
				input.set(ContentManager.TOKEN_KEY, "BANKUSERS_CUSTOMER");
			}
		}
		if (programID.equals("EUSERBRNALLOC") && tokenID.equals("BRANCHES")) {
			input.set(ContentManager.PROGRAM_KEY, "COMMON");
		}
		return input;
	}

	public DTObject processRecordFinderRequest(DTObject input) {
		String programID = input.get(ContentManager.PROGRAM_KEY);
		String tokenID = input.get(ContentManager.TOKEN_KEY);
		if (programID.equals("EUSERBRNALLOC") && tokenID.equals("EUSERBRNALLOC_MAIN")) {
			if (isInternalAdministrator()) {
				input.set(ContentManager.TOKEN_KEY, "EUSERBRNALLOC_ADMIN");
			}
			if (isInternalOperations()) {
				input.set(ContentManager.TOKEN_KEY, "EUSERBRNALLOC_OPERATION");
			}
		}
		return input;
	}
}
