package patterns.config.web.interceptors.access;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.AJAXInterceptor;
import patterns.config.framework.web.ajax.ContentManager;

public class euseractivationinterceptor extends AJAXInterceptor {
	public DTObject processLookupRequest(DTObject input) {
		throw new UnsupportedOperationException();
	}

	public DTObject processRecordFinderRequest(DTObject input) {
		String programID = input.get(ContentManager.PROGRAM_KEY);
		String tokenID = input.get(ContentManager.TOKEN_KEY);
		if (programID.equals("EUSERACTIVATION") && tokenID.equals("EUSERACTIVATION_MAIN")) {
			if (isInternalAdministrator()) {
				input.set(ContentManager.TOKEN_KEY, "EUSERACTIVATION_ADMIN");
			}
			if (isInternalOperations()) {
				input.set(ContentManager.TOKEN_KEY, "EUSERACTIVATION_OPERATION");
			}
		}
		return input;
	}
}
