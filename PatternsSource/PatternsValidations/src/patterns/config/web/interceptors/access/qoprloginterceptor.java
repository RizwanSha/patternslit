package patterns.config.web.interceptors.access;

import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.AJAXInterceptor;
import patterns.config.framework.web.ajax.ContentManager;

public class qoprloginterceptor extends AJAXInterceptor {
	public DTObject processLookupRequest(DTObject input) {
		String programID = input.get(ContentManager.PROGRAM_KEY);
		String tokenID = input.get(ContentManager.TOKEN_KEY);
		if (programID.equals("QOPRLOG") && tokenID.equals("BANKUSERS")) {
			if (isInternalAdministrator()) {
				input.set(ContentManager.PROGRAM_KEY, "COMMON");
				input.set(ContentManager.TOKEN_KEY, "BANKUSERS_INTERNAL");
			}
			if (isInternalOperations()) {
				input.set(ContentManager.PROGRAM_KEY, "COMMON");
				input.set(ContentManager.TOKEN_KEY, "BANKUSERS_CUSTOMER");
			}
		}
		if (programID.equals("QOPRLOG") && tokenID.equals("OPTIONS")) {
			input.set(ContentManager.PROGRAM_KEY, "COMMON");
			input.set(ContentManager.TOKEN_KEY, "OPTIONS");
		}
		return input;
	}

	public DTObject processRecordFinderRequest(DTObject input) {
		String programID = input.get(ContentManager.PROGRAM_KEY);
		String tokenID = input.get(ContentManager.TOKEN_KEY);
		if (programID.equals("QOPRLOG") && tokenID.equals("QOPRLOG_O_MAIN")) {
			if (isInternalAdministrator()) {
				input.set(ContentManager.TOKEN_KEY, "QOPRLOG_O_ADMIN");
			}
			if (isInternalOperations()) {
				input.set(ContentManager.TOKEN_KEY, "QOPRLOG_O_OPERATION");
			}
		}
		if (programID.equals("QOPRLOG") && tokenID.equals("QOPRLOG_OU_MAIN")) {
			if (isInternalAdministrator()) {
				input.set(ContentManager.TOKEN_KEY, "QOPRLOG_OU_ADMIN");
			}
			if (isInternalOperations()) {
				input.set(ContentManager.TOKEN_KEY, "QOPRLOG_OU_OPERATION");
			}
		}

		return input;
	}
}