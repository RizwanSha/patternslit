package patterns.config.web.interceptors.access;

import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.AJAXInterceptor;
import patterns.config.framework.web.ajax.ContentManager;

public class epkicertcrlinterceptor extends AJAXInterceptor {
	public DTObject processLookupRequest(DTObject input) {
		String programID = input.get(ContentManager.PROGRAM_KEY);
		String tokenID = input.get(ContentManager.TOKEN_KEY);
		if (programID.equals("EPKICERTCRL") && tokenID.equals("CACODES")) {
			input.set(ContentManager.PROGRAM_KEY, "COMMON");
			input.set(ContentManager.TOKEN_KEY, "CACODES");
		}
		return input;
	}

	public DTObject processRecordFinderRequest(DTObject input) {
		String programID = input.get(ContentManager.PROGRAM_KEY);
		String tokenID = input.get(ContentManager.TOKEN_KEY);
		if (programID.equals("EPKICERTCRL") && tokenID.equals("EPKICERTCRL_MAIN")) {
			input.set(ContentManager.TOKEN_KEY, "EPKICERTCRL_ADMIN");
		}
		return input;
	}
}