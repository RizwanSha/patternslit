package patterns.config.web.interceptors.access;

import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.AJAXInterceptor;
import patterns.config.framework.web.ajax.ContentManager;

public class econsolepgmallocinterceptor extends AJAXInterceptor {
	public DTObject processLookupRequest(DTObject input) {
		String programID = input.get(ContentManager.PROGRAM_KEY);
		String tokenID = input.get(ContentManager.TOKEN_KEY);
		if (programID.equals("ECONSOLEPGMALLOC") && tokenID.equals("CONSOLES")) {
			input.set(ContentManager.PROGRAM_KEY, "COMMON");
			input.set(ContentManager.TOKEN_KEY, "CONSOLES");
		}
		if (programID.equals("ECONSOLEPGMALLOC") && tokenID.equals("OPTIONS")) {
			input.set(ContentManager.PROGRAM_KEY, "COMMON");
			input.set(ContentManager.TOKEN_KEY, "OPTIONS_MENU");
		}
		
		return input;
	}

	public DTObject processRecordFinderRequest(DTObject input) {
		throw new UnsupportedOperationException();
	}
}
