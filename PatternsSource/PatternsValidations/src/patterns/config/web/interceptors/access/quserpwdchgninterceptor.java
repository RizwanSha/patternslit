package patterns.config.web.interceptors.access;

import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.AJAXInterceptor;
import patterns.config.framework.web.ajax.ContentManager;

public class quserpwdchgninterceptor extends AJAXInterceptor {
	public DTObject processLookupRequest(DTObject input) {
		String programID = input.get(ContentManager.PROGRAM_KEY);
		String tokenID = input.get(ContentManager.TOKEN_KEY);
		if (programID.equals("QUSERPWDCHGN") && tokenID.equals("BANKUSERS")) {

			if (isInternalAdministrator()) {
				input.set(ContentManager.PROGRAM_KEY, "COMMON");
				input.set(ContentManager.TOKEN_KEY, "BANKUSERS_INTERNAL");
			}
			if (isInternalOperations()) {
				input.set(ContentManager.PROGRAM_KEY, "COMMON");
				input.set(ContentManager.TOKEN_KEY, "BANKUSERS_CUSTOMER");
			}
		}
		return input;
	}

	public DTObject processRecordFinderRequest(DTObject input) {
		String programID = input.get(ContentManager.PROGRAM_KEY);
		String tokenID = input.get(ContentManager.TOKEN_KEY);
		if (programID.equals("QUSERPWDCHGN") && tokenID.equals("QUSERPWDCHGN_MAIN")) {
			if (isInternalAdministrator()) {
				input.set(ContentManager.TOKEN_KEY, "QUSERPWDCHGN_ADMIN");
			}
			if (isInternalOperations()) {
				input.set(ContentManager.TOKEN_KEY, "QUSERPWDCHGN_OPERATION");
			}
		}
		return input;
	}
}
