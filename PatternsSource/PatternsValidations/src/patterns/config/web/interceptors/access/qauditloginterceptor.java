package patterns.config.web.interceptors.access;

import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.AJAXInterceptor;
import patterns.config.framework.web.ajax.ContentManager;

public class qauditloginterceptor extends AJAXInterceptor {
	public DTObject processLookupRequest(DTObject input) {
		String programID = input.get(ContentManager.PROGRAM_KEY);
		String tokenID = input.get(ContentManager.TOKEN_KEY);
		if (programID.equals("QAUDITLOG") && tokenID.equals("OPTIONS")) {
			input.set(ContentManager.PROGRAM_KEY, "COMMON");
			input.set(ContentManager.TOKEN_KEY, "OPTIONS");
		}
		if (programID.equals("QAUDITLOG") && tokenID.equals("CUSTOMERS")) {
			if (isInternalOperations()) {
				input.set(ContentManager.PROGRAM_KEY, "COMMON");
				input.set(ContentManager.TOKEN_KEY, "CUSTOMERS");
			}
		}
		if (programID.equals("QAUDITLOG") && tokenID.equals("BANKUSERS")) {
			if (isInternalAdministrator()) {
				input.set(ContentManager.PROGRAM_KEY, "COMMON");
				input.set(ContentManager.TOKEN_KEY, "BANKUSERS_INTERNAL");
			}
		}
		
		if (programID.equals("QAUDITLOG") && tokenID.equals("BANKUSERS_CUSTOMER")) {
			if (isInternalOperations()) {
				input.set(ContentManager.PROGRAM_KEY, "COMMON");
				input.set(ContentManager.TOKEN_KEY, "BANKUSERS_CUSTOMER");
			}
		}
		
		if (programID.equals("QAUDITLOG") && tokenID.equals("BANKUSERS_OWN_CUSTOMER")) {
			if (isInternalOperations()) {
				input.set(ContentManager.PROGRAM_KEY, "COMMON");
				input.set(ContentManager.TOKEN_KEY, "BANKUSERS_OWN_CUSTOMER");
			}
		}
		return input;
	}

	public DTObject processRecordFinderRequest(DTObject input) {
		String programID = input.get(ContentManager.PROGRAM_KEY);
		String tokenID = input.get(ContentManager.TOKEN_KEY);
		if (programID.equals("QAUDITLOG") && tokenID.equals("QAUDITLOG_MAIN")) {
			input.set(ContentManager.TOKEN_KEY, "QAUDITLOG_ALL");
		}
		return input;
	}
}