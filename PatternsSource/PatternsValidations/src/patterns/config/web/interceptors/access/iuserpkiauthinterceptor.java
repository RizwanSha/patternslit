package patterns.config.web.interceptors.access;

import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.AJAXInterceptor;
import patterns.config.framework.web.ajax.ContentManager;

public class iuserpkiauthinterceptor extends AJAXInterceptor {
	public DTObject processLookupRequest(DTObject input) {
		String programID = input.get(ContentManager.PROGRAM_KEY);
		String tokenID = input.get(ContentManager.TOKEN_KEY);
		if (programID.equals("IUSERPKIAUTH") && tokenID.equals("BANKROLES")) {
			input.set(ContentManager.PROGRAM_KEY, "COMMON");
			if (isInternalOperations()) {
				input.set(ContentManager.TOKEN_KEY, "BANKROLES_CUSTOMER");
			}
			if (isInternalAdministrator()) {
				input.set(ContentManager.TOKEN_KEY, "BANKROLES_INTERNAL");
			}
		}
		return input;
	}

	public DTObject processRecordFinderRequest(DTObject input) {
		String programID = input.get(ContentManager.PROGRAM_KEY);
		String tokenID = input.get(ContentManager.TOKEN_KEY);
		if (programID.equals("IUSERPKIAUTH") && tokenID.equals("IUSERPKIAUTH_MAIN")) {
			if (isInternalAdministrator()) {
				input.set(ContentManager.TOKEN_KEY, "IUSERPKIAUTH_ADMIN");
			}
			if (isInternalOperations()) {
				input.set(ContentManager.TOKEN_KEY, "IUSERPKIAUTH_OPERATION");
			}
		}
		return input;
	}
}