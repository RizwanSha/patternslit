package patterns.config.web.interceptors.common;

import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.framework.web.ajax.AJAXInterceptor;

public class qntfnviewinterceptor extends AJAXInterceptor {
	public DTObject processLookupRequest(DTObject input) {
		throw new UnsupportedOperationException();
	}

	public DTObject processRecordFinderRequest(DTObject input) {
		String argument = input.get(ContentManager.ARGUMENT_KEY);
		if (argument.equals("U")) {
			input.set(ContentManager.TOKEN_KEY, "QNTFNVIEW_UNREADNOTIFICATIONS");
			input.set(ContentManager.ARGUMENT_KEY, "");
		} else if (argument.equals("R")) {
			input.set(ContentManager.TOKEN_KEY, "QNTFNVIEW_ALLNOTIFICATIONS");
			input.set(ContentManager.ARGUMENT_KEY, "");
		}
		return input;
	}
}
