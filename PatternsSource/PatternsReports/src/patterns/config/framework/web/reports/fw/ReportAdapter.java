package patterns.config.framework.web.reports.fw;

import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JRResultSetDataSource;
import net.sf.jasperreports.engine.JasperReport;

import org.w3c.dom.Document;

import patterns.config.framework.service.DTObject;
import ar.com.fdvs.dj.domain.builders.FastReportBuilder;

public class ReportAdapter implements ReportInterface {

	protected ReportContext context;

	private String reportIdentifier;

	private boolean error;

	private String errorMessage;
	public final void init() {

	}

	public final void destroy() {

	}

	public final void setContext(ReportContext context) {
		this.context = context;
	}

	public final boolean isError() {
		return error;
	}

	protected final void setError(boolean error) {
		this.error = error;
	}

	public final String getErrorMessage() {
		return errorMessage;
	}

	protected final void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public final String getReportIdentifier() {
		return reportIdentifier;
	}

	public final String getCommonResourceBundle() {
		return "CommonMessages";
	}
	
	public int getReportProcessorType() {
		throw new UnsupportedOperationException();
	}

	public JRResultSetDataSource processResultSet() {
		throw new UnsupportedOperationException();
	}

	public Document processXMLData() {
		throw new UnsupportedOperationException();
	}

	public String getReportPath() {
		throw new UnsupportedOperationException();
	}

	public Map<String, Object> getReportParameters() {
		throw new UnsupportedOperationException();
	}

	public void initResource() {
		throw new UnsupportedOperationException();
	}

	public void releaseResource() {
		throw new UnsupportedOperationException();
	}

	public String getResourceBundle() {
		throw new UnsupportedOperationException();
	}
	
	
	
	// Dynamic Report generation changes begin
	
	public boolean isDynamic() {
		return false;
	}
	
	public  List<FastReportBuilder> getDyanmicReportContent(String reportBasePath){
		throw new UnsupportedOperationException();
	}
	
	public  List<Integer> getDyanmicReportProcessorType() {
		throw new UnsupportedOperationException();
	}
	
	public  DTObject getDyanmicReportProcessData() {
		throw new UnsupportedOperationException();
	}
	// Dynamic Report generation changes end
	

}