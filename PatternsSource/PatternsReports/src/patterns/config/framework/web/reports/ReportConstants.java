package patterns.config.framework.web.reports;

public class ReportConstants {

	public static final int REPORT_FORMAT_PDF = 1;
	public static final int REPORT_FORMAT_XLS = 2;
	public static final int REPORT_FORMAT_DOCX = 3;
	public static final int REPORT_FORMAT_RTF = 4;
	public static final int REPORT_FORMAT_CSV = 5;
	public static final int REPORT_FORMAT_TXT = 6;
	public static final int REPORT_FORMAT_HTML = 7;

	public static final String REPORT_EXT_PDF = ".pdf";
	public static final String REPORT_EXT_XLS = ".xls";
	public static final String REPORT_EXT_DOCX = ".docx";
	public static final String REPORT_EXT_RTF = ".rtf";
	public static final String REPORT_EXT_CSV = ".csv";
	public static final String REPORT_EXT_TXT = ".txt";
	public static final String REPORT_EXT_HTML = ".html";

	public static final int REPORT_PROCESSOR_RESULT_SET = 1;
	public static final int REPORT_PROCESSOR_JASPER = 2;
	public static final int REPORT_PROCESSOR_XML = 3;
	public static final int REPORT_PROCESSOR_CONNECTION=4;
}
