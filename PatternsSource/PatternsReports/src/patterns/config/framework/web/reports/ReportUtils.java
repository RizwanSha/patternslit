package patterns.config.framework.web.reports;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.servlet.ServletContext;

import org.w3c.dom.Document;

import ar.com.fdvs.dj.core.DynamicJasperHelper;
import ar.com.fdvs.dj.core.layout.ClassicLayoutManager;
import ar.com.fdvs.dj.domain.DynamicReport;
import ar.com.fdvs.dj.domain.builders.FastReportBuilder;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JRPrintPage;
import net.sf.jasperreports.engine.JRResultSetDataSource;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRCsvExporter;
import net.sf.jasperreports.engine.export.JRCsvExporterParameter;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRRtfExporter;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;
import net.sf.jasperreports.engine.export.ooxml.JRDocxExporter;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.monitor.ContextReference;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.reports.fw.ReportInterface;

public class ReportUtils {

	private DBContext dbContext;
	
	public ReportUtils() {
		this.dbContext = new DBContext();
	}

	public ReportUtils(DBContext dbContext) {
		this.dbContext = dbContext;
	}

	public void close() {
		dbContext.close();
	}

	public static final int EXPORT_PDF = 1;
	public static final int EXPORT_XLS = 2;
	public static final int EXPORT_DOCX = 3;
	public static final int EXPORT_RTF = 4;
	public static final int EXPORT_CSV = 5;
	public static final int EXPORT_TXT = 6;
	public static final int EXPORT_HTML = 7;
	

	public final String getReportSequence() {
		String reportID = RegularConstants.EMPTY_STRING;
		DBUtil utils = dbContext.createUtilInstance();
		try {
			utils.reset();
			String sqlQuery = "SELECT FN_GETREPORTDOWNLOADID() REPORTID FROM DUAL";
			utils.setSql(sqlQuery);
			ResultSet rset = utils.executeQuery();
			if (rset.next()) {
				reportID = rset.getString(1);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			utils.reset();
		}
		return reportID;
	}

	public final String getReportName(String programID, String reportIdentifier, int reportFormat) {
		String reportName = RegularConstants.EMPTY_STRING;
		switch (reportFormat) {
		case ReportConstants.REPORT_FORMAT_PDF:
			reportName = programID + "_" + reportIdentifier + ReportConstants.REPORT_EXT_PDF;
			break;
		case ReportConstants.REPORT_FORMAT_XLS:
			reportName = programID + "_" + reportIdentifier + ReportConstants.REPORT_EXT_XLS;
			break;
		case ReportConstants.REPORT_FORMAT_DOCX:
			reportName = programID + "_" + reportIdentifier + ReportConstants.REPORT_EXT_DOCX;
			break;
		case ReportConstants.REPORT_FORMAT_RTF:
			reportName = programID + "_" + reportIdentifier + ReportConstants.REPORT_EXT_RTF;
			break;
		case ReportConstants.REPORT_FORMAT_CSV:
			reportName = programID + "_" + reportIdentifier + ReportConstants.REPORT_EXT_CSV;
			break;
		case ReportConstants.REPORT_FORMAT_TXT:
			reportName = programID + "_" + reportIdentifier + ReportConstants.REPORT_EXT_TXT;
			break;
		case ReportConstants.REPORT_FORMAT_HTML:
			reportName = programID + "_" + reportIdentifier + ReportConstants.REPORT_EXT_HTML;
			break;
		}
		return reportName;
	}

	public final String getDeploymentContext() {
		String deploymentContext = RegularConstants.EMPTY_STRING;
		ServletContext servletContext = ContextReference.getContext();
		try {
			deploymentContext = servletContext.getInitParameter(RegularConstants.INIT_ACCESS_TYPE);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
		}
		return deploymentContext;
	}

	public final void exportJasperReport(JasperPrint jasperPrint, int exportFormat, OutputStream reportOutputStream) {
		try {
			switch (exportFormat) {
			case ReportConstants.REPORT_FORMAT_PDF:
				JasperExportManager.exportReportToPdfStream(jasperPrint, reportOutputStream);
				break;
			case ReportConstants.REPORT_FORMAT_XLS:
				JRXlsExporter xlsExporter = new JRXlsExporter();
				xlsExporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
				xlsExporter.setParameter(JRExporterParameter.OUTPUT_STREAM, reportOutputStream);
				xlsExporter.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.FALSE);
				xlsExporter.setParameter(JRXlsExporterParameter.MAXIMUM_ROWS_PER_SHEET, new Integer(100000));
				xlsExporter.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
				xlsExporter.setParameter(JRXlsExporterParameter.IGNORE_PAGE_MARGINS, Boolean.TRUE);
				xlsExporter.setParameter(JRXlsExporterParameter.PARAMETERS_OVERRIDE_REPORT_HINTS, Boolean.TRUE);
				xlsExporter.exportReport();
				break;
			case ReportConstants.REPORT_FORMAT_DOCX:
				JRDocxExporter docExporter = new JRDocxExporter();
				docExporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
				docExporter.setParameter(JRExporterParameter.OUTPUT_STREAM, reportOutputStream);
				docExporter.exportReport();
				break;
			case ReportConstants.REPORT_FORMAT_RTF:
				JRRtfExporter rtfExporter = new JRRtfExporter();
				rtfExporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
				rtfExporter.setParameter(JRExporterParameter.OUTPUT_STREAM, reportOutputStream);
				rtfExporter.exportReport();
				break;
			case ReportConstants.REPORT_FORMAT_CSV:
				JRCsvExporter csvExporter = new JRCsvExporter();
				csvExporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
				csvExporter.setParameter(JRExporterParameter.OUTPUT_STREAM, reportOutputStream);
				csvExporter.setParameter(JRCsvExporterParameter.IGNORE_PAGE_MARGINS, Boolean.TRUE);
				csvExporter.setParameter(JRCsvExporterParameter.FIELD_DELIMITER, ",");
				csvExporter.setParameter(JRCsvExporterParameter.RECORD_DELIMITER, "\r\n");
				csvExporter.setParameter(JRCsvExporterParameter.PARAMETERS_OVERRIDE_REPORT_HINTS, Boolean.TRUE);
				csvExporter.exportReport();
				break;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (reportOutputStream != null)
					reportOutputStream.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public final void exportJasperReport(JasperPrint jasperPrint, int exportFormat, String reportOutputPath) {
		try {
			switch (exportFormat) {
			case ReportConstants.REPORT_FORMAT_PDF:
				JasperExportManager.exportReportToPdfFile(jasperPrint, reportOutputPath);
				break;
			case ReportConstants.REPORT_FORMAT_XLS:
				JRXlsExporter xlsExporter = new JRXlsExporter();
				xlsExporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
				xlsExporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, reportOutputPath);
				xlsExporter.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.FALSE);
				xlsExporter.setParameter(JRXlsExporterParameter.MAXIMUM_ROWS_PER_SHEET, new Integer(100000));
				xlsExporter.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
				xlsExporter.setParameter(JRXlsExporterParameter.IGNORE_PAGE_MARGINS, Boolean.TRUE);
				xlsExporter.setParameter(JRXlsExporterParameter.PARAMETERS_OVERRIDE_REPORT_HINTS, Boolean.TRUE);
				xlsExporter.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
				xlsExporter.exportReport();
				break;
			case ReportConstants.REPORT_FORMAT_DOCX:
				JRDocxExporter docExporter = new JRDocxExporter();
				docExporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
				docExporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, reportOutputPath);
				docExporter.exportReport();
				break;
			case ReportConstants.REPORT_FORMAT_RTF:
				JRRtfExporter rtfExporter = new JRRtfExporter();
				rtfExporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
				rtfExporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, reportOutputPath);
				rtfExporter.exportReport();
				break;
			case ReportConstants.REPORT_FORMAT_CSV:
				JRCsvExporter csvExporter = new JRCsvExporter();
				csvExporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
				csvExporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, reportOutputPath);
				csvExporter.setParameter(JRCsvExporterParameter.IGNORE_PAGE_MARGINS, Boolean.TRUE);
				csvExporter.setParameter(JRCsvExporterParameter.FIELD_DELIMITER, ",");
				csvExporter.setParameter(JRCsvExporterParameter.RECORD_DELIMITER, "\r\n");
				csvExporter.setParameter(JRCsvExporterParameter.PARAMETERS_OVERRIDE_REPORT_HINTS, Boolean.TRUE);
				csvExporter.exportReport();
				break;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public final JasperPrint getJasperPrint(InputStream jasperInputStream,HashMap<String, Object> reportParameters, JRResultSetDataSource resultSetData) {
		JasperPrint jasperPrint = null;
		try {
			jasperPrint = JasperFillManager.fillReport(jasperInputStream, reportParameters, resultSetData);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
		}
		return jasperPrint;
	}

	public final JasperPrint getJasperPrint(InputStream jasperInputStream,HashMap<String, Object> reportParameters) {
		JasperPrint jasperPrint = null;
		try {
			jasperPrint = JasperFillManager.fillReport(jasperInputStream, reportParameters);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
		}
		return jasperPrint;
	}

	public final JasperPrint getJasperPrint(InputStream jasperInputStream,HashMap<String, Object> reportParameters, Connection connection) {
		JasperPrint jasperPrint = null;
		try {
			
				jasperPrint = JasperFillManager.fillReport(jasperInputStream, reportParameters, connection);
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
		}
		return jasperPrint;
	}

	public final InputStream getJasperStream(String jasperPath) {
		InputStream reportInputStream = null;
		try {
			reportInputStream = this.getClass().getClassLoader().getResourceAsStream(jasperPath);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
		}
		return reportInputStream;
	}
	
	// Dynamic Report generation changes begin
	public final JasperReport getJasperReport(FastReportBuilder fastReportBuilder) {
		JasperReport jasperReport = null;
		try {
			//fastReportBuilder.setTemplateFile(jrxmlPath,true,true,true,true);
			DynamicReport dynamicReport = fastReportBuilder.build();
			jasperReport = DynamicJasperHelper.generateJasperReport(dynamicReport, new ClassicLayoutManager(), null);
			//jasperPrint = JasperFillManager.fillReport(jasperReport, reportParameters, connection);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
		}
		return jasperReport;
	}
	
	public final List<JasperPrint> getDynamicJasperPrint(List<FastReportBuilder> content,List<Integer> processorType,DTObject processData,HashMap<String, Object> reportParameters) {
		List<JasperPrint> jasperPrint=new ArrayList<JasperPrint>();
		JasperReport jasperReport = null;
		DynamicReport dynamicReport=null;
		JasperPrint dynamicJasperPrint=null;
		int index=0;
		try {
			for (FastReportBuilder reportBuilder :content){
				dynamicReport=reportBuilder.build();
				jasperReport=DynamicJasperHelper.generateJasperReport(dynamicReport, new ClassicLayoutManager(), null);
				switch (processorType.get(index)) {
				case ReportConstants.REPORT_PROCESSOR_JASPER:
					dynamicJasperPrint = getJasperPrint(jasperReport, reportParameters,getConnection());
					break;
				case ReportConstants.REPORT_PROCESSOR_RESULT_SET:
					JRResultSetDataSource resultSetDataSource = (JRResultSetDataSource)processData.getObject(String.valueOf(index));
					reportParameters.put("REPORT_CONNECTION",getConnection());
					dynamicJasperPrint = getJasperPrint(jasperReport, reportParameters, resultSetDataSource);
					break;
				case ReportConstants.REPORT_PROCESSOR_XML:
					Document document = (Document)processData.getObject(String.valueOf(index));
					reportParameters.put("XML_DATA_DOCUMENT", document);
					dynamicJasperPrint = getJasperPrint(jasperReport, reportParameters);
					break;
				case ReportConstants.REPORT_PROCESSOR_CONNECTION:
					DBContext dbContext = new DBContext();
					dynamicJasperPrint = getJasperPrint(jasperReport, reportParameters, dbContext.openConnection());
					dbContext.close();
					break;
				}
				/*if(jasperPrint==null)
					jasperPrint=dynamicJasperPrint;
				else{
					dynamicJasperPrint.getOrientationValue();
					List<JRPrintPage> pages=dynamicJasperPrint.getPages();
					for(JRPrintPage jrprintPage:pages){
						jasperPrint.addPage(jrprintPage);
					}	
				}*/
				jasperPrint.add(dynamicJasperPrint);
				index++;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
		}
		return jasperPrint;
	}
	
	public final JasperPrint getJasperPrint(JasperReport jasperReport,HashMap<String, Object> reportParameters, JRResultSetDataSource resultSetData) {
		JasperPrint jasperPrint = null;
		try {
			jasperPrint = JasperFillManager.fillReport(jasperReport, reportParameters, resultSetData);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
		}
		return jasperPrint;
	}

	public final JasperPrint getJasperPrint(JasperReport jasperReport, HashMap<String, Object> reportParameters) {
		JasperPrint jasperPrint = null;
		try {
			jasperPrint = JasperFillManager.fillReport(jasperReport, reportParameters);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
		}
		return jasperPrint;
	}

	public final JasperPrint getJasperPrint(JasperReport jasperReport, HashMap<String, Object> reportParameters, Connection connection) {
		JasperPrint jasperPrint = null;
		try {
			
				jasperPrint = JasperFillManager.fillReport(jasperReport, reportParameters, connection);
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
		}
		return jasperPrint;
	}
	
	
	
	public final void exportJasperReport(List<JasperPrint> jasperPrintList, int exportFormat, String reportOutputPath) {
		try {
			switch (exportFormat) {
			case ReportConstants.REPORT_FORMAT_PDF:
				JRPdfExporter pdfExporter = new JRPdfExporter();
				pdfExporter.setParameter(JRExporterParameter.JASPER_PRINT_LIST, jasperPrintList);
				pdfExporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, reportOutputPath);
				pdfExporter.exportReport();
				break;
			case ReportConstants.REPORT_FORMAT_XLS:
				JRXlsExporter xlsExporter = new JRXlsExporter();
				xlsExporter.setParameter(JRExporterParameter.JASPER_PRINT_LIST, jasperPrintList);
				xlsExporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, reportOutputPath);
				xlsExporter.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.FALSE);
				xlsExporter.setParameter(JRXlsExporterParameter.MAXIMUM_ROWS_PER_SHEET, new Integer(100000));
				xlsExporter.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
				xlsExporter.setParameter(JRXlsExporterParameter.IGNORE_PAGE_MARGINS, Boolean.TRUE);
				xlsExporter.setParameter(JRXlsExporterParameter.PARAMETERS_OVERRIDE_REPORT_HINTS, Boolean.TRUE);
				xlsExporter.exportReport();
				break;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	// Dynamic Report generation changes end
	

	public final String getReportFilePath(String entityCode, String reportName) {
		String reportFilePath = RegularConstants.EMPTY_STRING;
		DBUtil utils = dbContext.createUtilInstance();
		try {
			utils.reset();
			String sqlQuery = "SELECT REPORT_GENERATION_PATH FROM SYSTEMFOLDERS WHERE ENTITY_CODE=? AND EFFT_DATE = (SELECT MAX(EFFT_DATE) FROM SYSTEMFOLDERS WHERE ENTITY_CODE = ?  AND EFFT_DATE <= FN_GETCD(?)) ";
			utils.setSql(sqlQuery);
			utils.setString(1, entityCode);
			utils.setString(2, entityCode);
			utils.setString(3, entityCode);
			ResultSet rset = utils.executeQuery();
			if (rset.next()) {
				reportFilePath = rset.getString(1);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			utils.reset();
		}
		return reportFilePath;
	}

	public final OutputStream getReportOutputStream(String reportFilePath, String reportName) {
		BufferedOutputStream reportOutputStream = null;
		try {
			String reportFinalPath = reportFilePath + reportName;
			reportOutputStream = new BufferedOutputStream(new FileOutputStream(reportFinalPath));
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
		}
		return reportOutputStream;
	}

	public final String getReportFileOutputPath(String reportFilePath, String reportName) {
		String reportFinalPath = reportFilePath + reportName;
		return reportFinalPath;
	}

	public final void createReportDownload(String entityCode, String reportIdentifier, String deploymentContext, String userID, int reportFormat, String reportFilePath) {
		DBUtil dbutil = dbContext.createUtilInstance();
		try {
			dbutil.reset();
			dbutil.setMode(DBUtil.PREPARED);
			dbutil.setSql("INSERT INTO REPORTDOWNLOAD VALUES (?,?,?,?,FN_GETCDT(?),?,?,NULL)");
			dbutil.setString(1, entityCode);
			dbutil.setString(2, reportIdentifier);
			dbutil.setString(3, deploymentContext);
			dbutil.setString(4, userID);
			dbutil.setString(5, entityCode);
			dbutil.setInt(6, reportFormat);
			dbutil.setString(7, reportFilePath);
			dbutil.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			dbutil.reset();
		}
	}

	public final void updateReportDownload(String entityCode, String reportIdentifier) {
		DBUtil dbutil = dbContext.createUtilInstance();
		try {
			dbutil.setMode(DBUtil.PREPARED);
			dbutil.setSql("UPDATE REPORTDOWNLOAD SET DOWNLOAD_DATE = FN_GETCDT(?) WHERE ENTITY_CODE = ? AND REPORT_IDENTIFIER= ?");
			dbutil.setString(1, entityCode);
			dbutil.setString(2, entityCode);
			dbutil.setString(3, reportIdentifier);
			dbutil.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			dbutil.reset();
		}
	}

	public final ReportInterface getReportHandlerInstance(String reportHandlerName) {
		ReportInterface instance = null;
		try {
			instance = (ReportInterface) Class.forName(reportHandlerName).newInstance();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return instance;
	}

	public final Connection getConnection() {
		Connection conn = null;
		try {
			conn = dbContext.openConnection();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return conn;
	}

	public final ResourceBundle getCommonResource(String module) {
		ResourceBundle bundle = ResourceBundle.getBundle("patterns.config.web.reports.resources/" + module);
		return bundle;
	}
	
	public final String getStationeryType(String entityCode, String UserId,String reportIdentifier) {
		DBUtil dbutil = dbContext.createUtilInstance();
		String chkRptHeaderPref=null;
		String stationeryType=RegularConstants.DEFAULT_STATIONERY_TYPE;
		try {
			
			dbutil.setMode(DBUtil.PREPARED);
			dbutil.setSql("SELECT CHK_RPT_HEADER_PREF FROM RCPTGENCONTROL WHERE ENTITY_CODE = ? AND SOURCE_OPERATION_PGM= ?");
			dbutil.setString(1, entityCode);
			dbutil.setString(2, reportIdentifier);
			ResultSet rs=dbutil.executeQuery();
			if(rs.next()){
				chkRptHeaderPref=rs.getString("CHK_RPT_HEADER_PREF");
			}
			if(chkRptHeaderPref==null){
				return stationeryType;
			}
			
			dbutil.reset();
			dbutil.setMode(DBUtil.PREPARED);
			dbutil.setSql("SELECT PRINT_STATIONERY_TYPE FROM USERPREFERENCES WHERE ENTITY_CODE = ? AND USER_ID= ?");
			dbutil.setString(1, entityCode);
			dbutil.setString(2, UserId);
			rs=dbutil.executeQuery();
			if(rs.next()){
				stationeryType=rs.getString("PRINT_STATIONERY_TYPE")!=null ? rs.getString("PRINT_STATIONERY_TYPE"):RegularConstants.DEFAULT_STATIONERY_TYPE;
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			dbutil.reset();
		}
		return stationeryType;
	}
	
}