package patterns.config.framework.web.reports;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.ResourceBundle;

import patterns.config.framework.web.NumberToWords;

import org.apache.commons.lang3.StringUtils;

public class ReportRM {

	public ReportRM(ResourceBundle resourceBundle) {
		this.bundle = resourceBundle;
	}

	private ResourceBundle bundle;

	public String msg(String key) {
		String value = "";
		try {
			value = bundle.getString(key);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Cannot Load Label Message for " + key);
		}
		return value;
	}

	public static String fmc(BigDecimal amount, String currency, String currUnit) {
		try {
			Integer currUnits = Integer.parseInt(currUnit);
			if(amount==null)
				return null;
			String famount = String.valueOf(amount);
			String deciPart = "";
			deciPart = StringUtils.rightPad(deciPart, currUnits, "0");
			deciPart = "0." + deciPart;
			double value = Double.parseDouble(famount);
			boolean isNegative = false;
			if (value < 0) {
				value = value * -1;
				isNegative = true;
			}
			java.text.DecimalFormat formatter = new java.text.DecimalFormat(deciPart);
			String formattedValue = formatter.format(value);
			String integral = formattedValue.replaceAll("\\D\\d++", "");
			String fraction = formattedValue.replaceAll("\\d++\\D", "");
			if (integral.length() <= 3)
				return formattedValue;
			char lastDigitOfIntegral = integral.charAt(integral.length() - 1);
			integral = integral.replaceAll("\\d$", "");
			integral = integral.replaceAll("(?<=.)(?=(?:\\d{2})+$)", ",") + lastDigitOfIntegral + "." + fraction;
			if (isNegative) {
				integral = "- " + integral;
			}
			return integral;

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Cannot format currency " + amount);
			return amount.toString();
		}
	}

	public String fmd(String value, String format) {
		try {

		} catch (Exception e) {
			System.out.println("Cannot format date " + value);
		}
		return value;
	}

	public static String FormatAmount(String amount) {
		try {
			double value = Double.parseDouble(amount);
			boolean isNegative = false;
			if (value < 0) {
				value = value * -1;
				isNegative = true;
			}
			java.text.DecimalFormat formatter = new java.text.DecimalFormat("0.00");
			String formattedValue = formatter.format(value);
			String integral = formattedValue.replaceAll("\\D\\d++", "");
			String fraction = formattedValue.replaceAll("\\d++\\D", "");
			if (integral.length() <= 3)
				return formattedValue;
			char lastDigitOfIntegral = integral.charAt(integral.length() - 1);
			integral = integral.replaceAll("\\d$", "");
			integral = integral.replaceAll("(?<=.)(?=(?:\\d{2})+$)", ",") + lastDigitOfIntegral + "." + fraction;
			if (isNegative) {
				integral = "- " + integral;
			}
			return integral;
		} catch (NumberFormatException e) {
			e.printStackTrace();
			return amount;
		}
	}
	
	public static String formatAmount(BigDecimal amount) {
		try {
			double value = Double.parseDouble(amount.toString());
			boolean isNegative = false;
			if (value < 0) {
				value = value * -1;
				isNegative = true;
			}
			java.text.DecimalFormat formatter = new java.text.DecimalFormat("0.00");
			String formattedValue = formatter.format(value);
			String integral = formattedValue.replaceAll("\\D\\d++", "");
			String fraction = formattedValue.replaceAll("\\d++\\D", "");
			if (integral.length() <= 3)
				return formattedValue;
			char lastDigitOfIntegral = integral.charAt(integral.length() - 1);
			integral = integral.replaceAll("\\d$", "");
			integral = integral.replaceAll("(?<=.)(?=(?:\\d{2})+$)", ",") + lastDigitOfIntegral + "." + fraction;
			if (isNegative) {
				integral = "- " + integral;
			}
			return integral;
		} catch (NumberFormatException e) {
			e.printStackTrace();
			return amount.toString();
		}
	}
	
	public String NumberToWords(BigDecimal value,String units, String subUnits) {
		try {
			String amount = value.toString();
			String[] amountInWords = new String[1];
			amountInWords[0] = "";
			if (NumberToWords.ConvertAmountToText(amount,units,subUnits,amountInWords)) {
				return amountInWords[0];
			}

		} catch (Exception e) {
			System.out.println("Cannot format date " + value);
		}
		return value.toString();
	}
	
	static public String fmtDecWithUnits(String num ,String units) {
		Integer decUnits = Integer.parseInt(units);
		if(num==null)
			return null;
		String famount = String.valueOf(num);
		String deciPart = "";
		deciPart = StringUtils.rightPad(deciPart, decUnits, "0");
		deciPart = "0." + deciPart;
		double value = Double.parseDouble(famount);
		boolean isNegative = false;
		if (value < 0) {
			value = value * -1;
			isNegative = true;
		}
		java.text.DecimalFormat formatter = new java.text.DecimalFormat(deciPart);
		String formattedValue = formatter.format(value);
		if (isNegative) {
			formattedValue = "- " + formattedValue;
		}
		return formattedValue;
	}

}
