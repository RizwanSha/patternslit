package patterns.config.framework.web.reports.fw;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import patterns.config.framework.service.DTObject;

public class ReportContext {

	private HttpServletRequest request;
	private HttpServletResponse response;
	private HttpSession session;
	private DTObject filters;

	public ReportContext(HttpServletRequest request, HttpServletResponse response, HttpSession session, DTObject filters) {
		this.request = request;
		this.response = response;
		this.session = session;
		this.filters = filters;
	}

	public HttpServletRequest getRequest() {
		return request;
	}

	public HttpServletResponse getResponse() {
		return response;
	}

	public HttpSession getSession() {
		return session;
	}

	public DTObject getFilters() {
		return filters;
	}
}
