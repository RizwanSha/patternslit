package patterns.config.framework.web.reports.fw;

import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JRResultSetDataSource;

import org.w3c.dom.Document;

import patterns.config.framework.service.DTObject;
import ar.com.fdvs.dj.domain.builders.FastReportBuilder;

public interface ReportInterface {

	public void init();

	public void destroy();

	public void initResource();

	public void releaseResource();

	public void setContext(ReportContext context);

	public boolean isError();
	
	public String getErrorMessage();

	public String getCommonResourceBundle();

	public String getReportPath();

	public String getResourceBundle();

	public Map<String, Object> getReportParameters();

	public int getReportProcessorType();

	public JRResultSetDataSource processResultSet();

	public Document processXMLData();

	public String getReportIdentifier();

	// Dynamic Report generation changes begin
	
	public boolean isDynamic();
	
	public  List<FastReportBuilder> getDyanmicReportContent(String reportBasePath);
	
	public  List<Integer> getDyanmicReportProcessorType();
	
	public  DTObject getDyanmicReportProcessData();
	
	// Dynamic Report generation changes end
}
