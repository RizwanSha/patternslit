package patterns.config.utils;

import java.sql.ResultSet;
import java.util.Map;

import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.BindParameterType;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.objects.PrimaryKeyInfo;
import patterns.config.framework.database.objects.TableInfo;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBInfo;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.thread.ApplicationContext;
import patterns.config.framework.web.BackOfficeFormatUtils;

public class BaseUtility {

	public static String resolvePartitionBucketID(String programId, String sourceKey, DBContext dbContext) {
		ApplicationContext context = ApplicationContext.getInstance();
		String bucketId = context.getFinYear();
		String yearFormat = "%Y";
		String[] primaryKey = sourceKey.split("\\|");
		boolean partitionReqd = false;
		String partitionDataColumn = RegularConstants.EMPTY_STRING;
		String partitionColumnValue = context.getCurrentYear();
		String tableName = RegularConstants.EMPTY_STRING;
		DBUtil dbUtil = dbContext.createUtilInstance();
		try {
			String query = "SELECT MPGM_TABLE_NAME,PARTITION_YEAR_WISE,PARTITION_DATA_COLUMN FROM MPGM WHERE MPGM_ID = ?";
			dbUtil.reset();
			dbUtil.setSql(query);
			dbUtil.setString(1, programId);
			ResultSet rset = dbUtil.executeQuery();
			if (rset.next()) {
				tableName = rset.getString("MPGM_TABLE_NAME");
				String yearWisePartitionReqd = rset.getString("PARTITION_YEAR_WISE");
				partitionReqd = (yearWisePartitionReqd == null) ? false : (yearWisePartitionReqd.equals("1") ? true : false);
				if (partitionReqd) {
					partitionDataColumn = rset.getString("PARTITION_DATA_COLUMN");
				}
			}
			dbUtil.reset();

			if (partitionReqd) {
				DBInfo dataBaseInfo = new DBInfo();
				TableInfo tableInfo = dataBaseInfo.getTableInfo(tableName);
				PrimaryKeyInfo keyInfo = tableInfo.getPrimaryKeyInfo();
				String[] keyFields = keyInfo.getColumnNames();
				Map<String, BindParameterType> keyDetails = tableInfo.getColumnInfo().getColumnMapping();
				for (int i = 0; i < keyFields.length; i++) {
					if (keyFields[i].equals(partitionDataColumn)) {
						partitionColumnValue = primaryKey[i];
						BindParameterType columnType = keyDetails.get(partitionDataColumn);
						switch (columnType) {
						case INTEGER:
							break;
						case DATE:
							partitionColumnValue = BackOfficeFormatUtils.getDate(partitionColumnValue, BackOfficeConstants.TBA_DATE_FORMAT,
									yearFormat);
							break;
						default:
							break;
						}
						return partitionColumnValue;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			dbUtil.reset();
		}
		return bucketId;
	}
}
