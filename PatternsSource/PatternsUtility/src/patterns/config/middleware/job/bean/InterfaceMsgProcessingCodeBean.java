package patterns.config.middleware.job.bean;

public class InterfaceMsgProcessingCodeBean {

	private String msgPCEntity;

	public String getMsgPCEntity() {
		return msgPCEntity;
	}

	public void setMsgPCEntity(String msgPCEntity) {
		this.msgPCEntity = msgPCEntity;
	}

	public String getSystemCode() {
		return systemCode;
	}

	public void setSystemCode(String systemCode) {
		this.systemCode = systemCode;
	}

	public String getProcessingCode() {
		return processingCode;
	}

	public void setProcessingCode(String processingCode) {
		this.processingCode = processingCode;
	}

	public String getEnabled() {
		return enabled;
	}

	public void setEnabled(String enabled) {
		this.enabled = enabled;
	}

	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

	private String systemCode = "";
	private String processingCode = "";
	private String enabled = "";
	private String direction = "";
	private String sendDirection = "";

	public String getSendDirection() {
		return sendDirection;
	}

	public void setSendDirection(String sendDirection) {
		this.sendDirection = sendDirection;
	}

}
