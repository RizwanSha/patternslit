package patterns.config.middleware.job.bean;

public class JobConfigBean {

	private String jobCode;
	private String jobName;
	private String jobCategory;
	private String systemCode;
	private int maxParallelInstances = 0;
	private String entity;

	public String getEntity() {
		return entity;
	}

	public void setEntity(String entity) {
		this.entity = entity;
	}

	public void setJobCode(String jobCode) {
		this.jobCode = jobCode;
	}

	public String getJobCode() {
		return jobCode;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobCategory(String jobCategory) {
		this.jobCategory = jobCategory;
	}

	public String getJobCategory() {
		return jobCategory;
	}

	public void setSystemCode(String systemCode) {
		this.systemCode = systemCode;
	}

	public String getSystemCode() {
		return systemCode;
	}

	public void setMaxParallelInstances(int maxParallelInstances) {
		this.maxParallelInstances = maxParallelInstances;
	}

	public int getMaxParallelInstances() {
		return maxParallelInstances;
	}
}