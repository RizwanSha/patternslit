package patterns.config.middleware.job.bean;

import java.io.Serializable;

public class SMSConfigBean implements Serializable {

	private static final long serialVersionUID = -2044059457888761606L;

	private String serverURL;
	private String httpMethod;
	private boolean proxyRequired;
	private String proxyServerIP;
	private int proxyServerPort;
	private boolean proxyAuthenticationRequired;
	private String proxyUsername;
	private String proxyPassword;
	private boolean authenticationRequired;
	private String userNameKey;
	private String userName;
	private String passwordKey;
	private String password;
	private String mobileNumberKey;
	private String textKey;
	private String senderKey;
	private String senderName;

	public String getServerURL() {
		return serverURL;
	}

	public void setServerURL(String serverURL) {
		this.serverURL = serverURL;
	}

	public String getHttpMethod() {
		return httpMethod;
	}

	public void setHttpMethod(String httpMethod) {
		this.httpMethod = httpMethod;
	}

	public boolean isProxyRequired() {
		return proxyRequired;
	}

	public void setProxyRequired(boolean proxyRequired) {
		this.proxyRequired = proxyRequired;
	}

	public String getProxyServerIP() {
		return proxyServerIP;
	}

	public void setProxyServerIP(String proxyServerIP) {
		this.proxyServerIP = proxyServerIP;
	}

	public int getProxyServerPort() {
		return proxyServerPort;
	}

	public void setProxyServerPort(int proxyServerPort) {
		this.proxyServerPort = proxyServerPort;
	}

	public boolean isProxyAuthenticationRequired() {
		return proxyAuthenticationRequired;
	}

	public void setProxyAuthenticationRequired(boolean proxyAuthenticationRequired) {
		this.proxyAuthenticationRequired = proxyAuthenticationRequired;
	}

	public String getProxyUsername() {
		return proxyUsername;
	}

	public void setProxyUsername(String proxyUsername) {
		this.proxyUsername = proxyUsername;
	}

	public String getProxyPassword() {
		return proxyPassword;
	}

	public void setProxyPassword(String proxyPassword) {
		this.proxyPassword = proxyPassword;
	}

	public boolean isAuthenticationRequired() {
		return authenticationRequired;
	}

	public void setAuthenticationRequired(boolean authenticationRequired) {
		this.authenticationRequired = authenticationRequired;
	}

	public String getUserNameKey() {
		return userNameKey;
	}

	public void setUserNameKey(String userNameKey) {
		this.userNameKey = userNameKey;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPasswordKey() {
		return passwordKey;
	}

	public void setPasswordKey(String passwordKey) {
		this.passwordKey = passwordKey;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getMobileNumberKey() {
		return mobileNumberKey;
	}

	public void setMobileNumberKey(String mobileNumberKey) {
		this.mobileNumberKey = mobileNumberKey;
	}

	public String getTextKey() {
		return textKey;
	}

	public void setTextKey(String textKey) {
		this.textKey = textKey;
	}

	public String getSenderKey() {
		return senderKey;
	}

	public void setSenderKey(String senderKey) {
		this.senderKey = senderKey;
	}

	public String getSenderName() {
		return senderName;
	}

	public void setSenderName(String senderName) {
		this.senderName = senderName;
	}
}
