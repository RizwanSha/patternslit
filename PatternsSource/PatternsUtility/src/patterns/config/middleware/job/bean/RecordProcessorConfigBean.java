package patterns.config.middleware.job.bean;

public class RecordProcessorConfigBean extends JobConfigBean {

	private int maxRecordsToProcess = 0;
	private int retryTimeout = 0;
	private String controller;
	private String invokeMode;
	private String tablePrefix;

	public int getMaxRecordsToProcess() {
		return maxRecordsToProcess;
	}

	public void setMaxRecordsToProcess(int maxRecordsToProcess) {
		this.maxRecordsToProcess = maxRecordsToProcess;
	}

	public int getRetryTimeout() {
		return retryTimeout;
	}

	public void setRetryTimeout(int retryTimeout) {
		this.retryTimeout = retryTimeout;
	}

	public String getController() {
		return controller;
	}

	public void setController(String controller) {
		this.controller = controller;
	}

	public String getInvokeMode() {
		return invokeMode;
	}

	public void setInvokeMode(String invokeMode) {
		this.invokeMode = invokeMode;
	}

	public String getTablePrefix() {
		return tablePrefix;
	}

	public void setTablePrefix(String tablePrefix) {
		this.tablePrefix = tablePrefix;
	}

}
