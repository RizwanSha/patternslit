package patterns.config.middleware.job.processors;

import java.sql.ResultSet;
import java.util.LinkedList;

import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.middleware.core.MessageFacadeThread;
import patterns.config.middleware.facade.bean.MessageBean;
import patterns.config.middleware.job.bean.RecordProcessorConfigBean;

public class RecordProcessor {
	public void processRecords(RecordProcessorConfigBean recordProcessorConfigBean) {
		int maxRecords = recordProcessorConfigBean.getMaxRecordsToProcess();
		String entityCode = recordProcessorConfigBean.getEntity();
		String systemCode = recordProcessorConfigBean.getSystemCode();
		String tablePrefix = recordProcessorConfigBean.getTablePrefix();
		int retryTimeout = recordProcessorConfigBean.getRetryTimeout();
		DBContext dbContext = new DBContext();
		try {
			dbContext.setAutoCommit(false);
			String pendingTableName = tablePrefix + "INQPE";
			String processingQueueTableName = tablePrefix + "PROCINQPE";

			String selectQuery = "SELECT INVENTORY_NO FROM " + pendingTableName + " WHERE ENTITY_CODE = ? AND ROWNUM <= ?";
			DBUtil dbUtilSelect = dbContext.createUtilInstance();
			dbUtilSelect.setMode(DBUtil.PREPARED);
			dbUtilSelect.setSql(selectQuery);
			dbUtilSelect.setString(1, entityCode);
			dbUtilSelect.setInt(2, maxRecords);
			ResultSet rset = dbUtilSelect.executeQuery();
			LinkedList<String> peList = new LinkedList<String>();
			while (rset.next()) {
				peList.add(rset.getString(1));
			}
			dbUtilSelect.reset();

			if (peList.size() > 0) {
				String lockQuery = "SELECT * FROM " + pendingTableName + " WHERE ENTITY_CODE=? AND INVENTORY_NO=? FOR UPDATE NOWAIT";
				String deleteQuery = "DELETE FROM " + pendingTableName + " WHERE ENTITY_CODE=? AND INVENTORY_NO=?";
				String processingQueueInsertQuery = "INSERT INTO " + processingQueueTableName + " (ENTITY_CODE,INVENTORY_NO,RETRY_DATETIME) VALUES(?,?,SYSDATE + ?/1440)";

				for (String inventoryNumber : peList) {
					boolean lockAcquired = false;
					DBUtil dbUtilLock = dbContext.createUtilInstance();
					try {
						dbUtilLock.reset();
						dbUtilLock.setMode(DBUtil.PREPARED);
						dbUtilLock.setSql(lockQuery);
						dbUtilLock.setString(1, entityCode);
						dbUtilLock.setString(2, inventoryNumber);
						ResultSet lockRset = dbUtilLock.executeQuery();
						if (lockRset.next()) {
							lockAcquired = true;
						}
					} catch (Exception e) {
						e.printStackTrace();
						lockAcquired = false;
					} finally {
						dbUtilLock.reset();
					}
					if (lockAcquired) {
						DBUtil dbUtilDelete = dbContext.createUtilInstance();
						dbUtilDelete.setMode(DBUtil.PREPARED);
						dbUtilDelete.setSql(deleteQuery);
						dbUtilDelete.setString(1, entityCode);
						dbUtilDelete.setString(2, inventoryNumber);
						dbUtilDelete.executeUpdate();
						dbUtilDelete.reset();
						if (retryTimeout != 0) {
							DBUtil dbUtilProcInsert = dbContext.createUtilInstance();
							dbUtilProcInsert.setMode(DBUtil.PREPARED);
							dbUtilProcInsert.setSql(processingQueueInsertQuery);
							dbUtilProcInsert.setString(1, entityCode);
							dbUtilProcInsert.setString(2, inventoryNumber);
							dbUtilProcInsert.setInt(3, retryTimeout);
							dbUtilProcInsert.executeUpdate();
							dbUtilProcInsert.reset();
						}
						MessageBean messageBean = new MessageBean();
						messageBean.setEntity(entityCode);
						messageBean.setInventoryNumber(inventoryNumber);
						messageBean.setSystemCode(systemCode);
						messageBean.setTablePrefix(tablePrefix);
						MessageFacadeThread facadeThread = new MessageFacadeThread(messageBean);
						if (facadeThread.isFacadeAlloted()) {
							dbContext.commit();
							System.out.println("Record Processing Start :: " + inventoryNumber);
							Thread thread = new Thread(facadeThread);
							thread.start();
							if (recordProcessorConfigBean.getInvokeMode().equalsIgnoreCase("S")) {
								thread.join();
								System.out.println("Record Processing End :: " + inventoryNumber);
							}
						} else {
							System.out.println("Record Processing Miss :: " + inventoryNumber);
							dbContext.rollback();
							break;
						}

					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			try {
				dbContext.rollback();
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		} finally {
			dbContext.close();
		}
	}
}