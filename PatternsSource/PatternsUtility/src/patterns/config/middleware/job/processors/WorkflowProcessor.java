package patterns.config.middleware.job.processors;

import java.sql.ResultSet;
import java.util.LinkedList;

import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.middleware.core.MessageFacadeThread;
import patterns.config.middleware.facade.bean.WorkflowBean;
import patterns.config.middleware.job.bean.RecordProcessorConfigBean;

public class WorkflowProcessor {

	public void processRecords(RecordProcessorConfigBean recordProcessorConfigBean) {
		int maxRecords = recordProcessorConfigBean.getMaxRecordsToProcess();
		String entityCode = recordProcessorConfigBean.getEntity();
		String tablePrefix = recordProcessorConfigBean.getTablePrefix();
		int retryTimeout = recordProcessorConfigBean.getRetryTimeout();
		DBContext dbContext = new DBContext();
		try {
			dbContext.setAutoCommit(false);
			String pendingTableName = tablePrefix + "PE";
			String processingQueueTableName = tablePrefix + "PROCPE";

			String selectQuery = "SELECT SRV_REF_NUM FROM " + pendingTableName + " WHERE ENTITY_NUM = ? AND ROWNUM <= ?";
			DBUtil dbUtilSelect = dbContext.createUtilInstance();
			dbUtilSelect.setMode(DBUtil.PREPARED);
			dbUtilSelect.setSql(selectQuery);
			dbUtilSelect.setString(1, entityCode);
			dbUtilSelect.setInt(2, maxRecords);
			ResultSet rset = dbUtilSelect.executeQuery();
			LinkedList<String> peList = new LinkedList<String>();
			while (rset.next()) {
				peList.add(rset.getString(1));
			}
			dbUtilSelect.reset();

			if (peList.size() > 0) {
				String lockQuery = "SELECT * FROM " + pendingTableName + " WHERE ENTITY_NUM=? AND SRV_REF_NUM=? FOR UPDATE NOWAIT";
				String deleteQuery = "DELETE FROM " + pendingTableName + " WHERE ENTITY_NUM=? AND SRV_REF_NUM=?";
				String processingQueueInsertQuery = "INSERT INTO " + processingQueueTableName + " (ENTITY_NUM,SRV_REF_NUM,RETRY_DATETIME) VALUES(?,?,SYSDATE + ?/1440)";
				for (String serviceReferenceNumber : peList) {
					boolean lockAcquired = false;
					DBUtil dbUtilLock = dbContext.createUtilInstance();
					try {
						dbUtilLock.reset();
						dbUtilLock.setMode(DBUtil.PREPARED);
						dbUtilLock.setSql(lockQuery);
						dbUtilLock.setString(1, entityCode);
						dbUtilLock.setString(2, serviceReferenceNumber);
						ResultSet lockRset = dbUtilLock.executeQuery();
						if (lockRset.next()) {
							lockAcquired = true;
						}
					} catch (Exception e) {
						e.printStackTrace();
						lockAcquired = false;
					} finally {
						dbUtilLock.reset();
					}
					if (lockAcquired) {
						DBUtil dbUtilDelete = dbContext.createUtilInstance();
						dbUtilDelete.setMode(DBUtil.PREPARED);
						dbUtilDelete.setSql(deleteQuery);
						dbUtilDelete.setString(1, entityCode);
						dbUtilDelete.setString(2, serviceReferenceNumber);
						dbUtilDelete.executeUpdate();
						dbUtilDelete.reset();
						if (retryTimeout != 0) {
							DBUtil dbUtilProcInsert = dbContext.createUtilInstance();
							dbUtilProcInsert.setMode(DBUtil.PREPARED);
							dbUtilProcInsert.setSql(processingQueueInsertQuery);
							dbUtilProcInsert.setString(1, entityCode);
							dbUtilProcInsert.setString(2, serviceReferenceNumber);
							dbUtilProcInsert.setInt(3, retryTimeout);
							dbUtilProcInsert.executeUpdate();
							dbUtilProcInsert.reset();
						}
						WorkflowBean workflowBean = new WorkflowBean();
						workflowBean.setEntity(entityCode);
						workflowBean.setServiceReferenceNumber(serviceReferenceNumber);
						workflowBean.setTablePrefix(tablePrefix);
						MessageFacadeThread facadeThread = new MessageFacadeThread(workflowBean);
						if (facadeThread.isFacadeAlloted()) {
							dbContext.commit();
							System.out.println("Workflow Processing Hit :: " + serviceReferenceNumber);
							Thread thread = new Thread(facadeThread);
							thread.start();
							if (recordProcessorConfigBean.getInvokeMode().equalsIgnoreCase("S")) {
								thread.join();
							}
						} else {
							System.out.println("Workflow Processing Miss :: " + serviceReferenceNumber);
							dbContext.rollback();
							break;
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			try {
				dbContext.rollback();
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		} finally {
			dbContext.close();
		}
	}
}