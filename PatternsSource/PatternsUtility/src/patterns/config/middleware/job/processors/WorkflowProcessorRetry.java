package patterns.config.middleware.job.processors;

import java.sql.ResultSet;
import java.util.LinkedList;

import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.middleware.job.bean.RecordProcessorConfigBean;

public class WorkflowProcessorRetry {

	public void processRecords(RecordProcessorConfigBean recordProcessorConfigBean) {
		int maxRecords = recordProcessorConfigBean.getMaxRecordsToProcess();
		String entityCode = recordProcessorConfigBean.getEntity();
		String tablePrefix = recordProcessorConfigBean.getTablePrefix();
		DBContext dbContext = new DBContext();
		try {
			dbContext.setAutoCommit(false);
			String pendingTableName = tablePrefix + "PE";
			String processingQueueTableName = tablePrefix + "PROCPE";

			String selectQuery = "SELECT SRV_REF_NUM FROM " + processingQueueTableName + " WHERE ENTITY_NUM = ? AND RETRY_DATETIME <= FN_GETCDT(?) AND ROWNUM <= ?";
			DBUtil dbUtilSelect = dbContext.createUtilInstance();
			dbUtilSelect.setMode(DBUtil.PREPARED);
			dbUtilSelect.setSql(selectQuery);
			dbUtilSelect.setString(1, entityCode);
			dbUtilSelect.setString(2, entityCode);
			dbUtilSelect.setInt(3, maxRecords);
			ResultSet rset = dbUtilSelect.executeQuery();
			LinkedList<String> peList = new LinkedList<String>();
			while (rset.next()) {
				peList.add(rset.getString(1));
			}
			dbUtilSelect.reset();

			if (peList.size() > 0) {
				String lockQuery = "SELECT * FROM " + processingQueueTableName + " WHERE ENTITY_NUM=? AND SRV_REF_NUM=? FOR UPDATE NOWAIT";
				String deleteQuery = "DELETE FROM " + processingQueueTableName + " WHERE ENTITY_NUM=? AND SRV_REF_NUM=?";
				String pendingTableInsertQuery = "INSERT INTO " + pendingTableName + " (ENTITY_NUM,SRV_REF_NUM) VALUES(?,?)";
				for (String serviceReferenceNumber : peList) {
					boolean lockAcquired = false;
					DBUtil dbUtilLock = dbContext.createUtilInstance();
					try {
						dbUtilLock.reset();
						dbUtilLock.setMode(DBUtil.PREPARED);
						dbUtilLock.setSql(lockQuery);
						dbUtilLock.setString(1, entityCode);
						dbUtilLock.setString(2, serviceReferenceNumber);
						ResultSet lockRset = dbUtilLock.executeQuery();
						if (lockRset.next()) {
							lockAcquired = true;
						}
					} catch (Exception e) {
						e.printStackTrace();
						lockAcquired = false;
					} finally {
						dbUtilLock.reset();
					}
					if (lockAcquired) {
						DBUtil dbUtilDelete = dbContext.createUtilInstance();
						dbUtilDelete.setMode(DBUtil.PREPARED);
						dbUtilDelete.setSql(deleteQuery);
						dbUtilDelete.setString(1, entityCode);
						dbUtilDelete.setString(2, serviceReferenceNumber);
						dbUtilDelete.executeUpdate();
						dbUtilDelete.reset();
						DBUtil dbUtilPendingInsert = dbContext.createUtilInstance();
						dbUtilPendingInsert.setMode(DBUtil.PREPARED);
						dbUtilPendingInsert.setSql(pendingTableInsertQuery);
						dbUtilPendingInsert.setString(1, entityCode);
						dbUtilPendingInsert.setString(2, serviceReferenceNumber);
						dbUtilPendingInsert.executeUpdate();
						dbUtilPendingInsert.reset();
						dbContext.commit();
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			try {
				dbContext.rollback();
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		} finally {
			dbContext.close();
		}
	}
}