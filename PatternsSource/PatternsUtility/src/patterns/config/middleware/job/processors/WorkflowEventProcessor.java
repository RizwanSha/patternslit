package patterns.config.middleware.job.processors;

import java.sql.ResultSet;
import java.util.LinkedList;

import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.middleware.core.MessageFacadeThread;
import patterns.config.middleware.facade.bean.WorkflowBean;
import patterns.config.middleware.job.bean.RecordProcessorConfigBean;

public class WorkflowEventProcessor {

	public void processRecords(RecordProcessorConfigBean recordProcessorConfigBean) {
		int maxRecords = recordProcessorConfigBean.getMaxRecordsToProcess();
		String entityCode = recordProcessorConfigBean.getEntity();
		String tablePrefix = recordProcessorConfigBean.getTablePrefix();
		int retryTimeout = recordProcessorConfigBean.getRetryTimeout();
		DBContext dbContext = new DBContext();
		try {
			dbContext.setAutoCommit(false);
			String pendingTableName = tablePrefix + "PE";
			String processingQueueTableName = tablePrefix + "PROCPE";

			String selectQuery = "SELECT SRV_REF_NUM, EVENT_CODE, STAGE_ID,EVENT_TIME FROM " + pendingTableName + " WHERE ENTITY_NUM = ? AND EVENT_TIME <= FN_GETCDT(?) AND ROWNUM <= ?";
			DBUtil dbUtilSelect = dbContext.createUtilInstance();
			dbUtilSelect.setMode(DBUtil.PREPARED);
			dbUtilSelect.setSql(selectQuery);
			dbUtilSelect.setString(1, entityCode);
			dbUtilSelect.setString(2, entityCode);
			dbUtilSelect.setInt(3, maxRecords);
			ResultSet rset = dbUtilSelect.executeQuery();
			LinkedList<WorkflowBean> peList = new LinkedList<WorkflowBean>();
			while (rset.next()) {
				WorkflowBean workflowBean = new WorkflowBean();
				workflowBean.setEntity(entityCode);
				workflowBean.setServiceReferenceNumber(rset.getString(1));
				workflowBean.setEventCode(rset.getString(2));
				workflowBean.setStageID(rset.getString(3));
				workflowBean.setTablePrefix(tablePrefix);
				workflowBean.setEventTimestamp(rset.getTimestamp(4));
				peList.add(workflowBean);
			}
			dbUtilSelect.reset();

			if (peList.size() > 0) {
				String lockQuery = "SELECT * FROM " + pendingTableName + " WHERE ENTITY_NUM=? AND SRV_REF_NUM=? AND EVENT_CODE = ? AND STAGE_ID = ? FOR UPDATE NOWAIT";
				String deleteQuery = "DELETE FROM " + pendingTableName + " WHERE ENTITY_NUM=? AND SRV_REF_NUM=? AND EVENT_CODE = ? AND STAGE_ID = ?";
				String processingQueueInsertQuery = "INSERT INTO " + processingQueueTableName + " (ENTITY_NUM,SRV_REF_NUM,EVENT_CODE,STAGE_ID,EVENT_TIME,RETRY_DATETIME) VALUES(?,?,?,?,?,SYSDATE + ?/1440)";

				for (WorkflowBean workflowBean : peList) {
					String serviceReferenceNumber = workflowBean.getServiceReferenceNumber();
					boolean lockAcquired = false;
					DBUtil dbUtilLock = dbContext.createUtilInstance();
					try {
						dbUtilLock.reset();
						dbUtilLock.setMode(DBUtil.PREPARED);
						dbUtilLock.setSql(lockQuery);
						dbUtilLock.setString(1, entityCode);
						dbUtilLock.setString(2, serviceReferenceNumber);
						dbUtilLock.setString(3, workflowBean.getEventCode());
						dbUtilLock.setString(4, workflowBean.getStageID());
						ResultSet lockRset = dbUtilLock.executeQuery();
						if (lockRset.next()) {
							lockAcquired = true;
						}
					} catch (Exception e) {
						e.printStackTrace();
						lockAcquired = false;
					} finally {
						dbUtilLock.reset();
					}
					if (lockAcquired) {
						DBUtil dbUtilDelete = dbContext.createUtilInstance();
						dbUtilDelete.setMode(DBUtil.PREPARED);
						dbUtilDelete.setSql(deleteQuery);
						dbUtilDelete.setString(1, entityCode);
						dbUtilDelete.setString(2, serviceReferenceNumber);
						dbUtilDelete.setString(3, workflowBean.getEventCode());
						dbUtilDelete.setString(4, workflowBean.getStageID());
						dbUtilDelete.executeUpdate();
						dbUtilDelete.reset();
						if (retryTimeout != 0) {
							DBUtil dbUtilProcInsert = dbContext.createUtilInstance();
							dbUtilProcInsert.setMode(DBUtil.PREPARED);
							dbUtilProcInsert.setSql(processingQueueInsertQuery);
							dbUtilProcInsert.setString(1, entityCode);
							dbUtilProcInsert.setString(2, workflowBean.getServiceReferenceNumber());
							dbUtilProcInsert.setString(3, workflowBean.getEventCode());
							dbUtilProcInsert.setString(4, workflowBean.getStageID());
							dbUtilProcInsert.setTimestamp(5, workflowBean.getEventTimestamp());
							dbUtilProcInsert.setInt(6, retryTimeout);
							dbUtilProcInsert.executeUpdate();
							dbUtilProcInsert.reset();
						}
						MessageFacadeThread facadeThread = new MessageFacadeThread(workflowBean);
						if (facadeThread.isFacadeAlloted()) {
							dbContext.commit();
							System.out.println("Workflow Event Processing Hit :: " + serviceReferenceNumber);
							Thread thread = new Thread(facadeThread);
							thread.start();
							if (recordProcessorConfigBean.getInvokeMode().equalsIgnoreCase("S")) {
								thread.join();
							}
						} else {
							System.out.println("Workflow Event Processing Miss :: " + serviceReferenceNumber);
							dbContext.rollback();
							break;
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			try {
				dbContext.rollback();
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		} finally {
			dbContext.close();
		}
	}
}