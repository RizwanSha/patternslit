package patterns.config.middleware.job.category;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import patterns.config.middleware.core.QuartzJob;
import patterns.config.middleware.job.bean.RecordProcessorConfigBean;
import patterns.config.middleware.job.processors.WorkflowEventProcessorRetry;

public class WorkflowEventProcessorRetryJob extends QuartzJob {

	public void executeJob(JobExecutionContext context) throws JobExecutionException {
		RecordProcessorConfigBean recordProcessorConfigBean = null;
		WorkflowEventProcessorRetry processor = new WorkflowEventProcessorRetry();
		try {
			recordProcessorConfigBean = (RecordProcessorConfigBean) context.getJobDetail().getJobDataMap().get("JOB_CONFIG");
			processor.processRecords(recordProcessorConfigBean);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
