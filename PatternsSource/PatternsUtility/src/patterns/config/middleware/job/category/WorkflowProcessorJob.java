package patterns.config.middleware.job.category;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import patterns.config.middleware.core.QuartzJob;
import patterns.config.middleware.job.bean.RecordProcessorConfigBean;
import patterns.config.middleware.job.processors.WorkflowProcessor;


public class WorkflowProcessorJob extends QuartzJob {

	public void executeJob(JobExecutionContext context) throws JobExecutionException {
		RecordProcessorConfigBean recordProcessorConfigBean = null;
		WorkflowProcessor processor = new WorkflowProcessor();
		try {
			recordProcessorConfigBean = (RecordProcessorConfigBean) context.getJobDetail().getJobDataMap().get("JOB_CONFIG");
			processor.processRecords(recordProcessorConfigBean);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
