package patterns.config.middleware.job.category;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import patterns.config.middleware.core.QuartzJob;
import patterns.config.middleware.job.bean.RecordProcessorConfigBean;
import patterns.config.middleware.job.processors.WorkflowProcessorRetry;

public class WorkflowProcessorRetryJob extends QuartzJob {

	public void executeJob(JobExecutionContext context) throws JobExecutionException {
		RecordProcessorConfigBean recordProcessorConfigBean = null;
		WorkflowProcessorRetry processor = new WorkflowProcessorRetry();
		try {
			recordProcessorConfigBean = (RecordProcessorConfigBean) context.getJobDetail().getJobDataMap().get("JOB_CONFIG");
			processor.processRecords(recordProcessorConfigBean);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
