package patterns.config.middleware.job.category;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import patterns.config.middleware.core.QuartzJob;
import patterns.config.middleware.job.bean.RecordProcessorConfigBean;
import patterns.config.middleware.job.dispatchers.SMSDispatcher;

public class SMSDispatcherJob extends QuartzJob {

	public void executeJob(JobExecutionContext context) throws JobExecutionException {
		try {
			RecordProcessorConfigBean recordProcessorConfigBean = (RecordProcessorConfigBean) context.getJobDetail().getJobDataMap().get("JOB_CONFIG");
			SMSDispatcher dispatcher = new SMSDispatcher();
			dispatcher.dispatchSMS(recordProcessorConfigBean);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}