package patterns.config.middleware.job.dispatchers;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.middleware.job.bean.RecordProcessorConfigBean;
import patterns.config.middleware.job.bean.SMSConfigBean;
import patterns.config.middleware.manager.SMSConfigManager;
import patterns.config.middleware.utils.SMSUtility;
import patterns.config.middleware.utils.SMSUtility.SMSConfiguration;
import patterns.config.middleware.utils.SMSUtility.SMSContent;

public class SMSDispatcher {

	public void dispatchSMS(RecordProcessorConfigBean dispatcherConfigBean) {
		String entityCode = dispatcherConfigBean.getEntity();
		int maxRecords = dispatcherConfigBean.getMaxRecordsToProcess();
		Map<String, SMSContent> sendMap = new HashMap<String, SMSContent>();
		SMSConfigBean bean = new SMSConfigManager().getSMSConfig(entityCode);
		if (bean == null) {
			return;
		}
		DBContext dbContext = new DBContext();
		try {
			dbContext.setAutoCommit(false);
			String sql = "SELECT SMS_SEQ_NO FROM SMSSAFPE WHERE ENTITY_CODE = ? AND ROWNUM <= ?";
			DBUtil dbUtilSelect = dbContext.createUtilInstance();
			dbUtilSelect.setSql(sql);
			dbUtilSelect.setString(1, entityCode);
			dbUtilSelect.setInt(2, maxRecords);
			ResultSet rset = dbUtilSelect.executeQuery();
			while (rset.next()) {
				String smsSequenceNumber = rset.getString(1);
				String smssafSql = "SELECT SMS_REQ_DATE,SMS_SUBJECT,SMS_TEXT FROM SMSSAF WHERE ENTITY_CODE = ? AND SMS_SEQ_NO = ?";
				DBUtil dbUtilEmail = dbContext.createUtilInstance();
				dbUtilEmail.setSql(smssafSql);
				dbUtilEmail.setString(1, entityCode);
				dbUtilEmail.setString(2, smsSequenceNumber);
				ResultSet smsRset = dbUtilEmail.executeQuery();
				if (smsRset.next()) {
					SMSContent smsContent = new SMSContent();
					smsContent.setRequestDate(smsRset.getDate(1));
					// emailContent.setSubject(emailRset.getString(2));
					smsContent.setText(smsRset.getString(3));
					String rcpListSql = "SELECT MOBILE_NO FROM SMSSAFRCP WHERE ENTITY_CODE = ? AND SMS_SEQ_NO = ?";
					DBUtil dbUtilInner = dbContext.createUtilInstance();
					dbUtilInner.setMode(DBUtil.PREPARED);
					dbUtilInner.setSql(rcpListSql);
					dbUtilInner.setString(1, entityCode);
					dbUtilInner.setString(2, smsSequenceNumber);
					ResultSet rcpSet = dbUtilInner.executeQuery();
					List<String> toList = new LinkedList<String>();
					while (rcpSet.next()) {
						toList.add(rcpSet.getString("MOBILE_NO"));
					}
					dbUtilInner.reset();
					String[] toListArray = new String[toList.size()];
					toList.toArray(toListArray);
					smsContent.setToList(toListArray);
					sendMap.put(smsSequenceNumber, smsContent);
				}
				dbUtilEmail.reset();
			}
			dbUtilSelect.reset();
			if (sendMap.size() > 0) {
				String lockQuery = "SELECT * FROM SMSSAFPE WHERE ENTITY_CODE = ? AND SMS_SEQ_NO = ? FOR UPDATE NOWAIT";
				String deleteQuery = "DELETE FROM SMSSAFPE WHERE ENTITY_CODE=? AND SMS_SEQ_NO=?";
				for (Map.Entry<String, SMSContent> smsSequenceEntry : sendMap.entrySet()) {
					String smsSequenceNumber = smsSequenceEntry.getKey();
					boolean lockAcquired = false;
					DBUtil dbUtilLock = dbContext.createUtilInstance();
					try {
						dbUtilLock.reset();
						dbUtilLock.setMode(DBUtil.PREPARED);
						dbUtilLock.setSql(lockQuery);
						dbUtilLock.setString(1, entityCode);
						dbUtilLock.setString(2, smsSequenceNumber);
						rset = dbUtilLock.executeQuery();
						if (rset.next()) {
							lockAcquired = true;
						}
					} catch (Exception e) {
						e.printStackTrace();
						lockAcquired = false;
					} finally {
						dbUtilLock.reset();
					}
					if (lockAcquired == true) {
						DBUtil dbUtilDelete = dbContext.createUtilInstance();
						dbUtilDelete.setMode(DBUtil.PREPARED);
						dbUtilDelete.setSql(deleteQuery);
						dbUtilDelete.setString(1, entityCode);
						dbUtilDelete.setString(2, smsSequenceNumber);
						dbUtilDelete.executeUpdate();
						dbUtilDelete.reset();
						dbContext.commit();
					} else {
						sendMap.remove(smsSequenceNumber);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			try {
				dbContext.rollback();
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		} finally {
			dbContext.close();
		}
		SMSUtility smsDispatcher = new SMSUtility();
		try {
			if (sendMap.size() > 0) {
				SMSConfiguration smsConfiguration = new SMSConfiguration();
				smsConfiguration.setServerURL(bean.getServerURL());
				smsConfiguration.setHttpMethod(bean.getHttpMethod());
				smsConfiguration.setProxyRequired(bean.isProxyRequired());
				smsConfiguration.setProxyServerIP(bean.getProxyServerIP());
				smsConfiguration.setProxyServerPort(bean.getProxyServerPort());
				smsConfiguration.setProxyAuthenticationRequired(bean.isProxyAuthenticationRequired());
				smsConfiguration.setProxyUsername(bean.getProxyUsername());
				smsConfiguration.setProxyPassword(bean.getProxyPassword());

				smsConfiguration.setAuthenticationRequired(bean.isAuthenticationRequired());
				smsConfiguration.setUserNameKey(bean.getUserNameKey());
				smsConfiguration.setUserName(bean.getUserName());
				smsConfiguration.setPasswordKey(bean.getPasswordKey());
				smsConfiguration.setPassword(bean.getPassword());
				smsConfiguration.setMobileNumberKey(bean.getMobileNumberKey());
				smsConfiguration.setTextKey(bean.getTextKey());
				smsConfiguration.setSenderKey(bean.getSenderKey());
				smsConfiguration.setSenderName(bean.getSenderName());

				smsDispatcher.init(smsConfiguration);
				for (Map.Entry<String, SMSContent> smsSequenceEntry : sendMap.entrySet()) {
					SMSContent smsContent = smsSequenceEntry.getValue();
					smsDispatcher.sendSMS(smsContent);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}