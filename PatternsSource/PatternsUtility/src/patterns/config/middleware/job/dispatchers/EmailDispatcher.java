package patterns.config.middleware.job.dispatchers;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.middleware.job.bean.EmailConfigBean;
import patterns.config.middleware.job.bean.RecordProcessorConfigBean;
import patterns.config.middleware.manager.EmailConfigManager;
import patterns.config.middleware.utils.EmailUtility;
import patterns.config.middleware.utils.EmailUtility.EmailConfiguration;
import patterns.config.middleware.utils.EmailUtility.EmailContent;

public class EmailDispatcher {

	public void dispatchMails(RecordProcessorConfigBean dispatcherConfigBean) {
		String entityCode = dispatcherConfigBean.getEntity();
		int maxRecords = dispatcherConfigBean.getMaxRecordsToProcess();
		Map<String, EmailContent> sendMap = new HashMap<String, EmailContent>();
		EmailConfigBean bean = new EmailConfigManager().getMailConfig(entityCode);
		if (bean == null) {
			return;
		}
		DBContext dbContext = new DBContext();
		try {
			dbContext.setAutoCommit(false);
			String sql = "SELECT EMAIL_SEQ_NO FROM EMAILSAFPE WHERE ENTITY_CODE = ? AND ROWNUM <= ?";
			DBUtil dbUtilSelect = dbContext.createUtilInstance();
			dbUtilSelect.setSql(sql);
			dbUtilSelect.setString(1, entityCode);
			dbUtilSelect.setInt(2, maxRecords);
			ResultSet rset = dbUtilSelect.executeQuery();
			while (rset.next()) {
				String emailSequenceNumber = rset.getString(1);
				String emailsafSql = "SELECT EMAIL_REQ_DATE,EMAIL_SUBJECT,EMAIL_TEXT FROM EMAILSAF WHERE ENTITY_CODE = ? AND EMAIL_SEQ_NO = ?";
				DBUtil dbUtilEmail = dbContext.createUtilInstance();
				dbUtilEmail.setSql(emailsafSql);
				dbUtilEmail.setString(1, entityCode);
				dbUtilEmail.setString(2, emailSequenceNumber);
				ResultSet emailRset = dbUtilEmail.executeQuery();
				if (emailRset.next()) {
					EmailContent emailContent = new EmailContent();
					emailContent.setRequestDate(emailRset.getDate(1));
					emailContent.setSubject(emailRset.getString(2));
					emailContent.setText(emailRset.getString(3));
					String rcpListSql = "SELECT EMAIL_RCP_TYPE,EMAIL_ID FROM EMAILSAFRCP WHERE ENTITY_CODE = ? AND EMAIL_SEQ_NO = ?";
					DBUtil dbUtilInner = dbContext.createUtilInstance();
					dbUtilInner.setMode(DBUtil.PREPARED);
					dbUtilInner.setSql(rcpListSql);
					dbUtilInner.setString(1, entityCode);
					dbUtilInner.setString(2, emailSequenceNumber);
					ResultSet rcpSet = dbUtilInner.executeQuery();
					List<String> toList = new LinkedList<String>();
					List<String> ccList = new LinkedList<String>();
					List<String> bccList = new LinkedList<String>();
					while (rcpSet.next()) {
						String rcpType = rcpSet.getString(1);
						String emailID = rcpSet.getString(2);
						if (rcpType != null) {
							if (rcpType.equals("T")) {
								toList.add(emailID);
							} else if (rcpType.equals("C")) {
								ccList.add(emailID);
							} else if (rcpType.equals("B")) {
								bccList.add(emailID);
							}
						}
					}
					dbUtilInner.reset();
					String[] toListArray = new String[toList.size()];
					toList.toArray(toListArray);
					emailContent.setToList(toListArray);
					String[] ccListArray = new String[ccList.size()];
					ccList.toArray(ccListArray);
					emailContent.setCcList(ccListArray);
					String[] bccListArray = new String[bccList.size()];
					bccList.toArray(bccListArray);
					emailContent.setBccList(bccListArray);

					// Add attachments here if required
					// emailContent.setAttachList(attachList);

					sendMap.put(emailSequenceNumber, emailContent);
				}
				dbUtilEmail.reset();
			}
			dbUtilSelect.reset();
			if (sendMap.size() > 0) {
				String lockQuery = "SELECT * FROM EMAILSAFPE WHERE ENTITY_CODE = ? AND EMAIL_SEQ_NO = ? FOR UPDATE NOWAIT";
				String deleteQuery = "DELETE FROM EMAILSAFPE WHERE ENTITY_CODE=? AND EMAIL_SEQ_NO=?";
				for (Map.Entry<String, EmailContent> emailSequenceEntry : sendMap.entrySet()) {
					String emailSequenceNumber = emailSequenceEntry.getKey();
					boolean lockAcquired = false;
					DBUtil dbUtilLock = dbContext.createUtilInstance();
					try {
						dbUtilLock.reset();
						dbUtilLock.setMode(DBUtil.PREPARED);
						dbUtilLock.setSql(lockQuery);
						dbUtilLock.setString(1, entityCode);
						dbUtilLock.setString(2, emailSequenceNumber);
						rset = dbUtilLock.executeQuery();
						if (rset.next()) {
							lockAcquired = true;
						}
					} catch (Exception e) {
						e.printStackTrace();
						lockAcquired = false;
					} finally {
						dbUtilLock.reset();
					}
					if (lockAcquired == true) {
						DBUtil dbUtilDelete = dbContext.createUtilInstance();
						dbUtilDelete.setMode(DBUtil.PREPARED);
						dbUtilDelete.setSql(deleteQuery);
						dbUtilDelete.setString(1, entityCode);
						dbUtilDelete.setString(2, emailSequenceNumber);
						dbUtilDelete.executeUpdate();
						dbUtilDelete.reset();
						dbContext.commit();
					} else {
						sendMap.remove(emailSequenceNumber);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			try {
				dbContext.rollback();
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		} finally {
			dbContext.close();
		}
		EmailUtility emailDispatcher = new EmailUtility();
		try {
			if (sendMap.size() > 0) {
				EmailConfiguration emailConfiguration = new EmailConfiguration();
				emailConfiguration.setSslRequired(bean.isRequireSSL());
				emailConfiguration.setFromName(bean.getSenderName());
				emailConfiguration.setFromEmail(bean.getSenderMail());
				emailConfiguration.setMailServerHost(bean.getServerIP());
				emailConfiguration.setMailServerPort(bean.getServerPort());
				emailConfiguration.setAuthenticationRequired(bean.isAuthenticationRequired());
				emailConfiguration.setUsername(bean.getAccountName());
				emailConfiguration.setPassword(bean.getAccountPassword());
				emailConfiguration.setProxyRequired(bean.isProxyRequired());
				emailConfiguration.setProxyServerIP(bean.getProxyServerIP());
				emailConfiguration.setProxyServerPort(bean.getProxyServerPort());
				emailConfiguration.setProxyAuthenticationRequired(bean.isProxyAuthenticationRequired());
				emailConfiguration.setProxyUsername(bean.getProxyUsername());
				emailConfiguration.setProxyPassword(bean.getProxyPassword());
				emailDispatcher.init(emailConfiguration);
				for (Map.Entry<String, EmailContent> emailSequenceEntry : sendMap.entrySet()) {
					EmailContent emailContent = emailSequenceEntry.getValue();
					emailDispatcher.sendEmail(emailContent);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			emailDispatcher.destroy();
		}
	}
}