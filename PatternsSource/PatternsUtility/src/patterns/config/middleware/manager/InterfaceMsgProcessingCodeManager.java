package patterns.config.middleware.manager;

import java.sql.ResultSet;

import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.middleware.job.bean.InterfaceMsgProcessingCodeBean;

public class InterfaceMsgProcessingCodeManager {

	public static InterfaceMsgProcessingCodeBean getIntfMsgPC(String entity, String systemCode, String pc) throws Exception {
		DBContext dbContext = new DBContext();
		InterfaceMsgProcessingCodeBean inftMsgPcBean = null;
		try {
			String query = "SELECT PC.ENABLED,PC.DIRECTION,PC.SEND_DIRECTION FROM INTFMSGPC PC WHERE PC.MSGPC_ENTITY_NUM = ? AND PC.SYSTEM_CODE = ? AND PC.PROCESSING_CODE = ? ";
			DBUtil dbUtil = dbContext.createUtilInstance();
			dbUtil.reset();
			dbUtil.setMode(DBUtil.PREPARED);
			dbUtil.setSql(query);
			dbUtil.setString(1, entity);
			dbUtil.setString(2, systemCode);
			dbUtil.setString(3, pc);

			ResultSet rset = dbUtil.executeQuery();
			if (rset.next()) {

				inftMsgPcBean = new InterfaceMsgProcessingCodeBean();
				inftMsgPcBean.setDirection(rset.getString("DIRECTION"));
				inftMsgPcBean.setSendDirection(rset.getString("SEND_DIRECTION"));
				inftMsgPcBean.setEnabled(rset.getString("ENABLED"));
				inftMsgPcBean.setMsgPCEntity(entity);
				inftMsgPcBean.setProcessingCode(pc);
				inftMsgPcBean.setSystemCode(systemCode);
			}
			dbUtil.reset();
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			dbContext.close();
		}
		return inftMsgPcBean;
	}
}
