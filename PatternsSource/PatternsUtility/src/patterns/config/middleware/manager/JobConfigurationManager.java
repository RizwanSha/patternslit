package patterns.config.middleware.manager;

import java.sql.ResultSet;
import java.util.HashMap;

import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.middleware.job.bean.RecordProcessorConfigBean;

public class JobConfigurationManager {

	private static JobConfigurationManager jobCfgUtil = null;
	private HashMap<String, RecordProcessorConfigBean> recProcConfigMap = new HashMap<String, RecordProcessorConfigBean>();

	private JobConfigurationManager() {
	}

	public static JobConfigurationManager getInstance() throws Exception {
		if (jobCfgUtil == null) {
			jobCfgUtil = new JobConfigurationManager();
		}
		return jobCfgUtil;
	}

	public RecordProcessorConfigBean getRecordProcessorJobConfig(String entity, String jobCode) throws Exception {

		if (recProcConfigMap.get(jobCode) == null) {
			loadRecordProcessorJobConfig(entity, jobCode);
		}
		return (RecordProcessorConfigBean) recProcConfigMap.get(jobCode);
	}

	public synchronized void loadRecordProcessorJobConfig(String entity, String jobCode) throws Exception {
		DBContext dbContext = new DBContext();
		try {
			if (recProcConfigMap.get(jobCode) != null)
				return;
			String query = "SELECT JM.ENTITY_CODE,JM.JOB_CODE,JM.JOB_CATEGORY,JM.MAX_PARALLEL_INST,JM.SYSTEM_CODE,JM.JOB_NAME,JR.CONTROLLER,JR.INVOKE_MODE,JR.MAX_RECORDS,JR.TABLE_PREFIX,JR.RETRY_TIMEOUT  FROM JOBMAST JM,JOBRECPROCCFG JR WHERE JM.ENTITY_CODE= ? AND JR.ENTITY_CODE = JM.ENTITY_CODE AND JM.JOB_CODE = ? AND JR.JOB_CODE= ?";
			DBUtil dbUtil = dbContext.createUtilInstance();
			dbUtil.reset();
			dbUtil.setMode(DBUtil.PREPARED);
			dbUtil.setSql(query);
			dbUtil.setString(1, entity);
			dbUtil.setString(2, jobCode);
			dbUtil.setString(3, jobCode);
			ResultSet rset = dbUtil.executeQuery();
			if (rset.next()) {
				RecordProcessorConfigBean recordProcessorConfigBean = new RecordProcessorConfigBean();
				recordProcessorConfigBean.setEntity(rset.getString("ENTITY_CODE"));
				recordProcessorConfigBean.setController(rset.getString("CONTROLLER"));
				recordProcessorConfigBean.setInvokeMode(rset.getString("INVOKE_MODE"));
				recordProcessorConfigBean.setJobCategory(rset.getString("JOB_CATEGORY"));
				recordProcessorConfigBean.setJobCode(rset.getString("JOB_CODE"));
				recordProcessorConfigBean.setJobName(rset.getString("JOB_NAME"));
				recordProcessorConfigBean.setMaxParallelInstances(rset.getInt("MAX_PARALLEL_INST"));
				recordProcessorConfigBean.setMaxRecordsToProcess(rset.getInt("MAX_RECORDS"));
				recordProcessorConfigBean.setSystemCode(rset.getString("SYSTEM_CODE"));
				recordProcessorConfigBean.setTablePrefix(rset.getString("TABLE_PREFIX"));
				recordProcessorConfigBean.setRetryTimeout(rset.getInt("RETRY_TIMEOUT"));
				recProcConfigMap.put(jobCode, recordProcessorConfigBean);
			}
			dbUtil.reset();
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			dbContext.close();
		}
	}
}