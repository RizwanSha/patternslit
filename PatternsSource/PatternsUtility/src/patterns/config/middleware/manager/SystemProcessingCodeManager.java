package patterns.config.middleware.manager;

import java.sql.ResultSet;

import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.middleware.manager.bean.SystemProcessingCodeConfigBean;

public class SystemProcessingCodeManager {
	DBContext dbContext;

	public SystemProcessingCodeManager(DBContext dbContext) {
		this.dbContext = dbContext;
	}

	public SystemProcessingCodeConfigBean getSystemParameterConfig(String entity, String systemCode, String processingCode) {
		SystemProcessingCodeConfigBean systemParameterConfig = null;
		String serviceType = null;
		String authorizationRequired = null;
		DBUtil dbUtil = null;
		try {
			String sql = "SELECT INTFMSGPC_REQ_TYPE,INTFMSGPC_AUTH_ALLOWED FROM INTFMSGPCMAST PC WHERE PC.INTFMSGPC_ENTITY_NUM = ? AND PC.INTFMSGPC_SYS_CODE = ? AND PC.INTFMSGPC_PROC_CODE = ? ";
			dbUtil = dbContext.createUtilInstance();
			dbUtil.setMode(DBUtil.PREPARED);
			dbUtil.setSql(sql);
			dbUtil.setString(1, entity);
			dbUtil.setString(2, systemCode);
			dbUtil.setString(3, processingCode);

			ResultSet rset = dbUtil.executeQuery();
			if (rset.next()) {
				serviceType = rset.getString(1);
				authorizationRequired = rset.getString(2);
				if (authorizationRequired.equals(RegularConstants.COLUMN_ENABLE)) {
					sql = "SELECT INTFMSGPCCFG_AUTH_REQ FROM INTFMSGPCCFG  WHERE INTFMSGPCCFG_ENTITY_NUM = ? AND INTFMSGPCCFG_SYS_CODE = ? AND INTFMSGPCCFG_PROC_CODE = ? ";
					dbUtil.reset();
					dbUtil.setSql(sql);
					dbUtil.setString(1, entity);
					dbUtil.setString(2, systemCode);
					dbUtil.setString(3, processingCode);
					rset = dbUtil.executeQuery();
					if (rset.next()) {
						authorizationRequired = rset.getString(1);
					}
				}
				systemParameterConfig = new SystemProcessingCodeConfigBean();
				systemParameterConfig.setAuthorizationRequired(authorizationRequired);
				systemParameterConfig.setServiceType(serviceType);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			dbUtil.reset();
		}
		return systemParameterConfig;
	}
}
