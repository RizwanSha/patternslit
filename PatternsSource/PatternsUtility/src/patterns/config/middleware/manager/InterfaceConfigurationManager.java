package patterns.config.middleware.manager;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.middleware.manager.bean.InterfaceConfigurationBean;

public class InterfaceConfigurationManager {

	private static InterfaceConfigurationManager intfConfigManager = null;
	private Map<String, InterfaceConfigurationBean> intfConfigMap = new HashMap<String, InterfaceConfigurationBean>();

	private InterfaceConfigurationManager() {

	}

	public static InterfaceConfigurationManager getInstance() throws Exception {

		if (intfConfigManager == null) {
			intfConfigManager = new InterfaceConfigurationManager();
		}

		return intfConfigManager;

	}

	public InterfaceConfigurationBean getIntfConfig(String systemCode) throws Exception {

		if (intfConfigMap.get(systemCode) == null) {
			loadIntfConfig(systemCode);
		}

		return (InterfaceConfigurationBean) intfConfigMap.get(systemCode);

	}

	public synchronized void loadIntfConfig(String systemCode) throws Exception {

		DBContext dbContext = new DBContext();
		DBUtil dbUtil = null;
		InterfaceConfigurationBean intfConfigBean = null;
		try {

			dbUtil = dbContext.createUtilInstance();
			dbUtil.reset();
			dbUtil.setSql("SELECT INTF_ENTITY_NUM,SYSTEM_CODE,INWPE_TABLE,INWLOG_TABLE,OWPE_TABLE,OWLOG_TABLE,STATUS_LOG_TABLE,HEADER_TABLE,FORMAT FROM INTFSYSCONFIG WHERE INTF_ENTITY_NUM = ? AND SYSTEM_CODE = ?");
			dbUtil.setString(1, "1");
			dbUtil.setString(2, systemCode);
			ResultSet lrs = dbUtil.executeQuery();
			if (lrs.next()) {

				intfConfigBean = new InterfaceConfigurationBean();
				intfConfigBean.setEntityNum(lrs.getInt("INTF_ENTITY_NUM"));
				intfConfigBean.setFormat(lrs.getInt("FORMAT"));
				intfConfigBean.setHeaderTable(lrs.getString("HEADER_TABLE"));
				intfConfigBean.setIwLogTable(lrs.getString("INWLOG_TABLE"));
				intfConfigBean.setIwPETable(lrs.getString("INWPE_TABLE"));
				intfConfigBean.setOwLogTable(lrs.getString("OWLOG_TABLE"));
				intfConfigBean.setOwPETable(lrs.getString("OWPE_TABLE"));
				intfConfigBean.setStatusLogTable(lrs.getString("STATUS_LOG_TABLE"));
				intfConfigBean.setSystemCode(lrs.getString("SYSTEM_CODE"));

				intfConfigMap.put(systemCode, intfConfigBean);

			}

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		finally{
			dbContext.close();
		}

	}

}
