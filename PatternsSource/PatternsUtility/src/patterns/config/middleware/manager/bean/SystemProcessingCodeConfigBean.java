package patterns.config.middleware.manager.bean;

public class SystemProcessingCodeConfigBean {

	private String serviceType;
	private String authorizationRequired;

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public String getAuthorizationRequired() {
		return authorizationRequired;
	}

	public void setAuthorizationRequired(String authorizationRequired) {
		this.authorizationRequired = authorizationRequired;
	}
}
