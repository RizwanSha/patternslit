package patterns.config.middleware.manager.bean;

public class InterfaceConfigurationBean {
	
	private int entityNum;
	private String systemCode;
	private String iwPETable;
	private String iwLogTable;
	private String owPETable;
	private String owLogTable;
	private String statusLogTable;
	private String headerTable;
	private int format;
	
	
	public int getFormat() {
		return format;
	}
	public void setFormat(int format) {
		this.format = format;
	}
	public void setEntityNum(int entityNum) {
		this.entityNum = entityNum;
	}
	public int getEntityNum() {
		return entityNum;
	}
	public void setSystemCode(String systemCode) {
		this.systemCode = systemCode;
	}
	public String getSystemCode() {
		return systemCode;
	}
	public void setIwPETable(String iwPETable) {
		this.iwPETable = iwPETable;
	}
	public String getIwPETable() {
		return iwPETable;
	}
	public void setOwPETable(String owPETable) {
		this.owPETable = owPETable;
	}
	public String getOwPETable() {
		return owPETable;
	}
	public void setIwLogTable(String iwLogTable) {
		this.iwLogTable = iwLogTable;
	}
	public String getIwLogTable() {
		return iwLogTable;
	}
	public void setOwLogTable(String owLogTable) {
		this.owLogTable = owLogTable;
	}
	public String getOwLogTable() {
		return owLogTable;
	}
	public void setStatusLogTable(String statusLogTable) {
		this.statusLogTable = statusLogTable;
	}
	public String getStatusLogTable() {
		return statusLogTable;
	}
	public void setHeaderTable(String headerTable) {
		this.headerTable = headerTable;
	}
	public String getHeaderTable() {
		return headerTable;
	}

}
