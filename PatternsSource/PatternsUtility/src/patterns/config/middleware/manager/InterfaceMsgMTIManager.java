package patterns.config.middleware.manager;

import java.sql.ResultSet;

import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;

public class InterfaceMsgMTIManager {

	public static int getMTIType(String entity, String systemCode, String processingCode, String mti) throws Exception {
		DBContext dbContext = new DBContext();
		int mtiType = 0;
		try {
			String query = " SELECT TYPE FROM INTFMSGMTI WHERE IMM_ENTITY_NUM = ? AND SYSTEM_CODE = ? AND PROCESSING_CODE = ? AND MTI = ?";
			DBUtil dbUtil = dbContext.createUtilInstance();
			dbUtil.setMode(DBUtil.PREPARED);
			dbUtil.setSql(query);
			dbUtil.setString(1, entity);
			dbUtil.setString(2, systemCode);
			dbUtil.setString(3, processingCode);
			dbUtil.setString(4, mti);
			ResultSet rset = dbUtil.executeQuery();

			if (rset.next()) {
				mtiType = rset.getInt("TYPE");
			}

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{
			dbContext.close();
		}

		return mtiType;

	}

}
