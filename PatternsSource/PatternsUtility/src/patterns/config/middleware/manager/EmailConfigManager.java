package patterns.config.middleware.manager;

import java.sql.ResultSet;

import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.middleware.job.bean.EmailConfigBean;

public class EmailConfigManager {

	public EmailConfigManager() {
	}

	public EmailConfigBean getMailConfig(String entityCode) {
		return loadEmailConfiguration(entityCode);
	}

	public EmailConfigBean loadEmailConfiguration(String entityCode) {
		EmailConfigBean emailConfigBean = null;
		DBContext dbContext = new DBContext();
		try {
			String sql = "SELECT SSL_REQUIRED,BANK_COMMUNICATION_NAME,BANK_COMMUNICATION_EMAIL,PROXY_REQUIRED,PROXY_SERVER,PROXY_SERVER_PORT,PROXY_AUTH_REQD,PROXY_USER_NAME,PROXY_PASSWORD,OUTGOING_MAIL_SERVER,OUTGOING_MAIL_SERVER_PORT,OUTGOING_AUTH_REQD,OUTGOING_USER_NAME,OUTGOING_PASSWORD FROM EMAILCONFIG WHERE ENTITY_CODE = ? AND EFFT_DATE = (SELECT MAX(EFFT_DATE) FROM EMAILCONFIG WHERE ENTITY_CODE  = ? AND EFFT_DATE <= FN_GETCD(?))";
			DBUtil dbUtilInner = dbContext.createUtilInstance();
			dbUtilInner.reset();
			dbUtilInner.setMode(DBUtil.PREPARED);
			dbUtilInner.setSql(sql);
			dbUtilInner.setString(1, entityCode);
			dbUtilInner.setString(2, entityCode);
			dbUtilInner.setString(3, entityCode);
			ResultSet rset = dbUtilInner.executeQuery();
			if (rset.next()) {
				emailConfigBean = new EmailConfigBean();
				emailConfigBean.setRequireSSL(rset.getString("SSL_REQUIRED").equals(RegularConstants.COLUMN_ENABLE) ? true : false);
				emailConfigBean.setSenderName(rset.getString("BANK_COMMUNICATION_NAME"));
				emailConfigBean.setSenderMail(rset.getString("BANK_COMMUNICATION_EMAIL"));
				emailConfigBean.setServerIP(rset.getString("OUTGOING_MAIL_SERVER"));
				emailConfigBean.setServerPort(rset.getInt("OUTGOING_MAIL_SERVER_PORT"));
				emailConfigBean.setAuthenticationRequired(rset.getString("OUTGOING_AUTH_REQD").equals(RegularConstants.COLUMN_ENABLE) ? true : false);
				if (emailConfigBean.isAuthenticationRequired()) {
					emailConfigBean.setAccountName(rset.getString("OUTGOING_USER_NAME"));
					emailConfigBean.setAccountPassword(rset.getString("OUTGOING_PASSWORD"));
				}
				emailConfigBean.setProxyRequired(rset.getString("PROXY_REQUIRED").equals(RegularConstants.COLUMN_ENABLE) ? true : false);
				if (emailConfigBean.isProxyRequired()) {
					emailConfigBean.setProxyServerIP(rset.getString("PROXY_SERVER"));
					emailConfigBean.setProxyServerPort(rset.getInt("PROXY_SERVER_PORT"));
					emailConfigBean.setProxyAuthenticationRequired(rset.getString("PROXY_AUTH_REQD").equals(RegularConstants.COLUMN_ENABLE) ? true : false);
					if (emailConfigBean.isProxyAuthenticationRequired()) {
						emailConfigBean.setProxyUsername(rset.getString("PROXY_USER_NAME"));
						emailConfigBean.setProxyPassword(rset.getString("PROXY_PASSWORD"));
					}
				}
			}
			dbUtilInner.reset();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			dbContext.close();
		}
		return emailConfigBean;
	}
}
