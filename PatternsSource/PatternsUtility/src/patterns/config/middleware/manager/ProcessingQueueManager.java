package patterns.config.middleware.manager;

import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;

public class ProcessingQueueManager {

	private DBContext dbContext;

	public ProcessingQueueManager(DBContext dbContext) {
		this.dbContext = dbContext;
	}

	public ProcessingQueueManager() {
		this.dbContext = new DBContext();
	}

	public void close() {
		if (dbContext != null) {
			dbContext.close();
		}
	}

	public boolean deleteWorkflowProcessingQueue(String processingQueueTable, String entityCode, String serviceReferenceNumber) {
		boolean successful = false;
		String deleteQuery = "DELETE FROM " + processingQueueTable + " WHERE ENTITY_NUM = ? AND SRV_REF_NUM = ?";
		DBUtil dbUtilDelete = dbContext.createUtilInstance();
		try {
			dbUtilDelete.setMode(DBUtil.PREPARED);
			dbUtilDelete.setSql(deleteQuery);
			dbUtilDelete.setString(1, entityCode);
			dbUtilDelete.setString(2, serviceReferenceNumber);
			dbUtilDelete.executeUpdate();
			dbUtilDelete.reset();
			successful = true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			dbUtilDelete.reset();
		}
		return successful;
	}

	public boolean deleteMessageProcessingQueue(String processingQueueTable, String entityCode, String inventoryNumber) {
		boolean successful = false;
		String deleteQuery = "DELETE FROM " + processingQueueTable + " WHERE ENTITY_CODE = ? AND INVENTORY_NO = ?";
		DBUtil dbUtilDelete = dbContext.createUtilInstance();
		try {
			dbUtilDelete.setMode(DBUtil.PREPARED);
			dbUtilDelete.setSql(deleteQuery);
			dbUtilDelete.setString(1, entityCode);
			dbUtilDelete.setString(2, inventoryNumber);
			dbUtilDelete.executeUpdate();
			dbUtilDelete.reset();
			successful = true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			dbUtilDelete.reset();
		}
		return successful;
	}

}
