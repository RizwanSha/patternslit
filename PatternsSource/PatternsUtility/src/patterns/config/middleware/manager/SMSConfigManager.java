package patterns.config.middleware.manager;

import java.sql.ResultSet;

import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.middleware.job.bean.SMSConfigBean;

public class SMSConfigManager {

	public SMSConfigManager() {
	}

	public SMSConfigBean getSMSConfig(String entityCode) {
		return loadSMSConfiguration(entityCode);
	}

	public SMSConfigBean loadSMSConfiguration(String entityCode) {
		SMSConfigBean smsConfigBean = null;
		DBContext dbContext = new DBContext();
		try {
			String sql = "SELECT SERVER_URL,HTTP_METHOD,PROXY_REQUIRED,PROXY_SERVER,PROXY_SERVER_PORT,PROXY_AUTH_REQD,PROXY_USER_NAME,PROXY_PASSWORD,AUTH_REQD,USER_NAME_KEY,USER_NAME_VALUE,PASSWORD_KEY,PASSWORD_VALUE,MOBILE_NUMBER_KEY,TEXT_KEY,SENDER_KEY,SENDER_VALUE FROM SMSCONFIG WHERE ENTITY_CODE = ? AND EFFT_DATE = (SELECT MAX(EFFT_DATE) FROM SMSCONFIG WHERE ENTITY_CODE  = ? AND EFFT_DATE <= FN_GETCD(?))";
			DBUtil dbUtilInner = dbContext.createUtilInstance();
			dbUtilInner.reset();
			dbUtilInner.setMode(DBUtil.PREPARED);
			dbUtilInner.setSql(sql);
			dbUtilInner.setString(1, entityCode);
			dbUtilInner.setString(2, entityCode);
			dbUtilInner.setString(3, entityCode);
			ResultSet rset = dbUtilInner.executeQuery();
			if (rset.next()) {
				smsConfigBean = new SMSConfigBean();
				smsConfigBean.setServerURL(rset.getString("SERVER_URL"));
				smsConfigBean.setHttpMethod(rset.getString("HTTP_METHOD"));
				smsConfigBean.setProxyRequired(rset.getString("PROXY_REQUIRED").equals(RegularConstants.COLUMN_ENABLE) ? true : false);
				if (smsConfigBean.isProxyRequired()) {
					smsConfigBean.setProxyServerIP(rset.getString("PROXY_SERVER"));
					smsConfigBean.setProxyServerPort(rset.getInt("PROXY_SERVER_PORT"));
					smsConfigBean.setProxyAuthenticationRequired(rset.getString("PROXY_AUTH_REQD").equals(RegularConstants.COLUMN_ENABLE) ? true : false);
					if (smsConfigBean.isProxyAuthenticationRequired()) {
						smsConfigBean.setProxyUsername(rset.getString("PROXY_USER_NAME"));
						smsConfigBean.setProxyPassword(rset.getString("PROXY_PASSWORD"));
					}
				}
				smsConfigBean.setAuthenticationRequired(rset.getString("AUTH_REQD").equals(RegularConstants.COLUMN_ENABLE) ? true : false);
				if (smsConfigBean.isAuthenticationRequired()) {
					smsConfigBean.setUserNameKey(rset.getString("USER_NAME_KEY"));
					smsConfigBean.setUserName(rset.getString("USER_NAME_VALUE"));
					smsConfigBean.setPasswordKey(rset.getString("PASSWORD_KEY"));
					smsConfigBean.setPassword(rset.getString("PASSWORD_VALUE"));
					smsConfigBean.setMobileNumberKey(rset.getString("MOBILE_NUMBER_KEY"));
					smsConfigBean.setTextKey(rset.getString("TEXT_KEY"));
					smsConfigBean.setSenderKey(rset.getString("SENDER_KEY"));
					smsConfigBean.setSenderName(rset.getString("SENDER_VALUE"));
				}
			}
			dbUtilInner.reset();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			dbContext.close();
		}
		return smsConfigBean;
	}
}
