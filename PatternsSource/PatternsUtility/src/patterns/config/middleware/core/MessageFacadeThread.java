package patterns.config.middleware.core;

import javax.ejb.EJBException;
import javax.naming.InitialContext;

import patterns.config.framework.DatasourceConfigurationManager;
import patterns.config.middleware.facade.MessageFacade;
import patterns.config.middleware.facade.MessageFacadeHome;
import patterns.config.middleware.facade.bean.MessageBean;
import patterns.config.middleware.facade.bean.WorkflowBean;

public class MessageFacadeThread implements Runnable {

	boolean isFacadeAlloted = false;
	MessageFacade remote = null;
	MessageBean messageBean = null;
	WorkflowBean workflowBean = null;

	public boolean isFacadeAlloted() {
		return isFacadeAlloted;
	}

	public MessageFacadeThread(MessageBean messageBean) {
		InitialContext iniCtx;
		MessageFacadeHome home = null;
		try {
			this.messageBean = messageBean;
			iniCtx = new InitialContext();
			home = (MessageFacadeHome) javax.rmi.PortableRemoteObject.narrow(iniCtx.lookup(DatasourceConfigurationManager.getMessageDispatcherHomeJNDI()), MessageFacadeHome.class);
			remote = home.create();
			isFacadeAlloted = true;
		} catch (EJBException e) {
			e.printStackTrace();
			isFacadeAlloted = false;
		} catch (Exception e) {
			e.printStackTrace();
			isFacadeAlloted = false;
		}
	}

	public MessageFacadeThread(WorkflowBean workflowBean) {
		InitialContext iniCtx;
		MessageFacadeHome home = null;
		try {
			this.workflowBean = workflowBean;
			iniCtx = new InitialContext();
			home = (MessageFacadeHome) javax.rmi.PortableRemoteObject.narrow(iniCtx.lookup(DatasourceConfigurationManager.getMessageDispatcherHomeJNDI()), MessageFacadeHome.class);
			remote = home.create();
			isFacadeAlloted = true;
		} catch (EJBException e) {
			e.printStackTrace();
			isFacadeAlloted = false;
		} catch (Exception e) {
			e.printStackTrace();
			isFacadeAlloted = false;
		}
	}

	public void run() {
		try {
			if (messageBean != null)
				remote.handleMessage(messageBean);
			else if (workflowBean != null)
				remote.handleMessage(workflowBean);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}