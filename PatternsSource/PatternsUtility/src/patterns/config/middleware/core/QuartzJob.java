package patterns.config.middleware.core;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;

public abstract class QuartzJob implements StatefulJob {

	public void execute(JobExecutionContext context) throws JobExecutionException {
		try {
			executeJob(context);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public abstract void executeJob(JobExecutionContext context) throws JobExecutionException;
}
