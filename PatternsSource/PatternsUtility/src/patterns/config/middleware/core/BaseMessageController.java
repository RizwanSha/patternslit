package patterns.config.middleware.core;

import patterns.config.framework.database.utils.DBContext;
import patterns.config.middleware.facade.bean.MessageBean;
import patterns.config.middleware.facade.bean.WorkflowBean;

public abstract class BaseMessageController {

	public BaseMessageController() {
		this.dbContext = new DBContext();
	}

	private DBContext dbContext;

	public DBContext getDbContext() {
		return dbContext;
	}

	public void close() {
		if (dbContext != null)
			dbContext.close();
	}

	public abstract void handleMessage(MessageBean inMessageBean) throws Exception;

	public abstract void handleMessage(WorkflowBean inMessageBean) throws Exception;
}