package patterns.config.middleware.utils;

public class SystemCodes {
	public static final String PORTAL = "PORTAL";
	public static final String SMS = "SMS";
	public static final String USSD = "USSD";
}
