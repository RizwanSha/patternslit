package patterns.config.middleware.utils;

import java.sql.ResultSet;
import java.util.HashMap;

import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.middleware.constants.StatusCodes;
import patterns.config.middleware.manager.InterfaceConfigurationManager;
import patterns.config.middleware.manager.bean.InterfaceConfigurationBean;

public class QueueUtil {

	public void updateMessageStatus(String entityCode, String systemCode, String inventoryNo, int status, int direction) throws Exception {
		DBContext dbContext = null;
		StringBuffer queryBuff = new StringBuffer("");
		String msgStatusTable = "", logTable = "";
		int maxSl = -1;
		try {
			dbContext = new DBContext();
			DBUtil dbUtil = dbContext.createUtilInstance();
			dbUtil.reset();

			InterfaceConfigurationBean intfCfgBean = InterfaceConfigurationManager.getInstance().getIntfConfig(systemCode);
			msgStatusTable = intfCfgBean.getStatusLogTable();

			if (direction == 1)
				logTable = intfCfgBean.getIwLogTable();
			else
				logTable = intfCfgBean.getOwLogTable();

			queryBuff.append("SELECT IFNULL(MAX(SL)+1,1) SL FROM ").append(msgStatusTable).append(" WHERE ENTITY_CODE = ?  AND INVENTORY_NO= ? ");

			dbUtil.setMode(DBUtil.PREPARED);
			dbUtil.setSql(queryBuff.toString());
			dbUtil.setString(1, entityCode);
			dbUtil.setString(2, inventoryNo);
			ResultSet rset = dbUtil.executeQuery();

			if (rset.next())
				maxSl = rset.getInt("SL");

			dbUtil.reset();

			queryBuff = new StringBuffer();
			queryBuff.append("INSERT INTO ").append(msgStatusTable);
			queryBuff.append("(ENTITY_CODE,INVENTORY_NO,SL,STATUS,SYSTEM_TIME) VALUES (?,?,?,?,FN_GETCDT(?))");

			dbUtil.setMode(DBUtil.PREPARED);
			dbUtil.setSql(queryBuff.toString());
			dbUtil.setString(1, entityCode);
			dbUtil.setString(2, inventoryNo);
			dbUtil.setInt(3, maxSl);
			dbUtil.setInt(4, status);
			dbUtil.setString(5, entityCode);
			dbUtil.executeUpdate();
			dbUtil.reset();

			queryBuff = new StringBuffer();
			queryBuff.append("UPDATE ").append(logTable);
			queryBuff.append(" SET STATUS= ? WHERE INVENTORY_NO = ?  AND ENTITY_CODE = ? ");

			dbUtil.setMode(DBUtil.PREPARED);
			dbUtil.setSql(queryBuff.toString());
			dbUtil.setInt(1, status);
			dbUtil.setString(2, inventoryNo);
			dbUtil.setString(3, entityCode);
			dbUtil.executeUpdate();
			dbUtil.reset();
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			dbContext.close();
		}
	}

	public HashMap<String, String> getMessageContent(String entity, String systemCode, String inventoryNo) throws Exception {
		DBContext dbContext = new DBContext();
		StringBuffer queryBuff = null;
		String iwLogTable = "";
		HashMap<String, String> messageMap = null;
		try {

			InterfaceConfigurationBean intfCfgBean = InterfaceConfigurationManager.getInstance().getIntfConfig(systemCode);
			iwLogTable = intfCfgBean.getIwLogTable();

			queryBuff = new StringBuffer();
			queryBuff.append("SELECT MSG_CONTENT,SOURCE_QUEUE,CORRELATION_ID FROM  ").append(iwLogTable).append(" WHERE ENTITY_CODE = ? AND  INVENTORY_NO= ? ");

			DBUtil dbUtil = dbContext.createUtilInstance();
			dbUtil.setMode(DBUtil.PREPARED);
			dbUtil.setSql(queryBuff.toString());
			dbUtil.setString(1, entity);
			dbUtil.setString(2, inventoryNo);
			ResultSet rset = dbUtil.executeQuery();
			if (rset.next()) {

				String messageContent = rset.getString("MSG_CONTENT");
				String sourceQueue = rset.getString("SOURCE_QUEUE");
				String correlationId = rset.getString("CORRELATION_ID");

				messageMap = new HashMap<String, String>();
				messageMap.put("MSG_CONTENT", messageContent);
				messageMap.put("SOURCE_QUEUE", sourceQueue);
				messageMap.put("CORRELATION_ID", correlationId);
			}

			dbUtil.reset();
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			dbContext.close();
		}
		return messageMap;
	}

	public void updateErrorStatus(String systemCode, String inventoryNo, String entityCode, String errorCode, String errorDesc, int logTable) {

		DBContext dbContext = null;
		DBUtil dbUtil = null;
		StringBuffer queryBuff = null, queryBuff1 = null, queryBuff2 = null;
		String statusLogTable = "", LogTable = "";
		int maxSl = 0;

		try {
			dbContext = new DBContext();
			dbUtil = dbContext.createUtilInstance();
			dbUtil.reset();

			InterfaceConfigurationBean intfCfgBean = InterfaceConfigurationManager.getInstance().getIntfConfig(systemCode);
			statusLogTable = intfCfgBean.getStatusLogTable();

			if (logTable == 1)
				LogTable = intfCfgBean.getIwLogTable();

			else if (logTable == 2)
				LogTable = intfCfgBean.getOwLogTable();

			queryBuff = new StringBuffer();
			queryBuff.append("UPDATE ").append(LogTable).append(" SET STATUS=?, ERROR_CODE = ? ,ERROR_DESC = ? WHERE INVENTORY_NO = ?  AND ENTITY_CODE = ? ");

			dbUtil.setMode(DBUtil.PREPARED);
			dbUtil.setSql(queryBuff.toString());
			dbUtil.setInt(1, 6);
			dbUtil.setString(2, errorCode);
			dbUtil.setString(3, errorDesc);
			dbUtil.setString(4, inventoryNo);
			dbUtil.setString(5, entityCode);
			dbUtil.executeUpdate();

			dbUtil.reset();
			queryBuff1 = new StringBuffer();
			queryBuff1.append("SELECT IFNULL(MAX(SL)+1,1) SL FROM ").append(statusLogTable).append(" WHERE ENTITY_CODE = ?  AND INVENTORY_NO= ? ");

			dbUtil.setMode(DBUtil.PREPARED);
			dbUtil.setSql(queryBuff1.toString());
			dbUtil.setString(1, entityCode);
			dbUtil.setString(2, inventoryNo);
			ResultSet rset = dbUtil.executeQuery();
			if (rset.next())
				maxSl = rset.getInt("SL");

			dbUtil.reset();

			queryBuff2 = new StringBuffer();
			queryBuff2.append("INSERT INTO ").append(statusLogTable);
			queryBuff2.append("(ENTITY_CODE,INVENTORY_NO,SL,STATUS,SYSTEM_TIME) VALUES (?,?,?,?,FN_GETCDT(?))");

			dbUtil.setMode(DBUtil.PREPARED);
			dbUtil.setSql(queryBuff2.toString());
			dbUtil.setString(1, entityCode);
			dbUtil.setString(2, inventoryNo);
			dbUtil.setInt(3, maxSl);
			dbUtil.setInt(4, StatusCodes.ERROR);
			dbUtil.setString(5, entityCode);
			dbUtil.executeUpdate();
			dbUtil.reset();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			dbUtil.reset();
		}
	}

	public void updateDirection(String systemCode, String inventoryNo, String entityCode, String direction, int logTable) throws Exception {

		DBContext dbContext = new DBContext();
		DBUtil dbUtil = null;

		try {
			dbUtil = dbContext.createUtilInstance();
			dbUtil.reset();

			InterfaceConfigurationBean intfCfgBean = InterfaceConfigurationManager.getInstance().getIntfConfig(systemCode);

			if (logTable == 1) {
				String iwLogTable = intfCfgBean.getIwLogTable();
				StringBuffer queryBuff = new StringBuffer();

				queryBuff.append("UPDATE ").append(iwLogTable).append(" SET DIRECTION = ? WHERE INVENTORY_NO = ?  AND ENTITY_CODE = ? ");

				dbUtil.setMode(DBUtil.PREPARED);
				dbUtil.setSql(queryBuff.toString());
				dbUtil.setString(1, direction);
				dbUtil.setString(2, inventoryNo);
				dbUtil.setString(3, entityCode);
				dbUtil.executeUpdate();
			}

			else if (logTable == 2) {
				String owLogTable = intfCfgBean.getOwLogTable();
				StringBuffer queryBuff = new StringBuffer();

				queryBuff.append("UPDATE ").append(owLogTable).append(" SET DIRECTION = ? WHERE INVENTORY_NO = ?  AND ENTITY_CODE = ? ");

				dbUtil.setMode(DBUtil.PREPARED);
				dbUtil.setSql(queryBuff.toString());
				dbUtil.setString(1, direction);
				dbUtil.setString(2, inventoryNo);
				dbUtil.setString(3, entityCode);
				dbUtil.executeUpdate();
			}

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			dbContext.close();
		}
	}
}
