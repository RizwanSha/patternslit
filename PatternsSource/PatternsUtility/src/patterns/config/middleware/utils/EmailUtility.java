package patterns.config.middleware.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.util.Map;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.Message.RecipientType;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class EmailUtility {

	public static class EmailContent {
		private Date requestDate;
		private String subject;
		private String text;
		private String[] toList;
		private String[] ccList;
		private String[] bccList;
		private Map<String, byte[]> attachList;

		public Map<String, byte[]> getAttachList() {
			return attachList;
		}

		public void setAttachList(Map<String, byte[]> attachList) {
			this.attachList = attachList;
		}

		public String getSubject() {
			return subject;
		}

		public void setSubject(String subject) {
			this.subject = subject;
		}

		public String getText() {
			return text;
		}

		public void setText(String text) {
			this.text = text;
		}

		public String[] getToList() {
			return toList;
		}

		public void setToList(String[] toList) {
			this.toList = toList;
		}

		public String[] getCcList() {
			return ccList;
		}

		public void setCcList(String[] ccList) {
			this.ccList = ccList;
		}

		public String[] getBccList() {
			return bccList;
		}

		public void setBccList(String[] bccList) {
			this.bccList = bccList;
		}

		public void setRequestDate(Date requestDate) {
			this.requestDate = requestDate;
		}

		public Date getRequestDate() {
			return requestDate;
		}

	}

	public static class EmailConfiguration {
		private boolean sslRequired;
		private String fromName;
		private String fromEmail;
		private String mailServerHost;
		private int mailServerPort;
		private boolean authenticationRequired;
		private String username;
		private String password;

		private boolean proxyRequired;
		private String proxyServerIP;
		private int proxyServerPort;
		private boolean proxyAuthenticationRequired;
		private String proxyUsername;
		private String proxyPassword;

		public void setSslRequired(boolean sslRequired) {
			this.sslRequired = sslRequired;
		}

		public boolean isSslRequired() {
			return sslRequired;
		}

		public void setFromEmail(String fromEmail) {
			this.fromEmail = fromEmail;
		}

		public String getFromEmail() {
			return fromEmail;
		}

		public void setFromName(String fromName) {
			this.fromName = fromName;
		}

		public String getFromName() {
			return fromName;
		}

		public void setMailServerHost(String mailServerHost) {
			this.mailServerHost = mailServerHost;
		}

		public String getMailServerHost() {
			return mailServerHost;
		}

		public void setMailServerPort(int mailServerPort) {
			this.mailServerPort = mailServerPort;
		}

		public int getMailServerPort() {
			return mailServerPort;
		}

		public void setAuthenticationRequired(boolean authenticationRequired) {
			this.authenticationRequired = authenticationRequired;
		}

		public boolean isAuthenticationRequired() {
			return authenticationRequired;
		}

		public void setUsername(String userName) {
			this.username = userName;
		}

		public String getUsername() {
			return username;
		}

		public void setPassword(String password) {
			this.password = password;
		}

		public String getPassword() {
			return password;
		}

		public boolean isProxyRequired() {
			return proxyRequired;
		}

		public void setProxyRequired(boolean proxyRequired) {
			this.proxyRequired = proxyRequired;
		}

		public void setProxyServerIP(String proxyServerIP) {
			this.proxyServerIP = proxyServerIP;
		}

		public String getProxyServerIP() {
			return proxyServerIP;
		}

		public int getProxyServerPort() {
			return proxyServerPort;
		}

		public void setProxyServerPort(int proxyServerPort) {
			this.proxyServerPort = proxyServerPort;
		}

		public boolean isProxyAuthenticationRequired() {
			return proxyAuthenticationRequired;
		}

		public void setProxyAuthenticationRequired(boolean proxyAuthenticationRequired) {
			this.proxyAuthenticationRequired = proxyAuthenticationRequired;
		}

		public String getProxyUsername() {
			return proxyUsername;
		}

		public void setProxyUsername(String proxyUsername) {
			this.proxyUsername = proxyUsername;
		}

		public String getProxyPassword() {
			return proxyPassword;
		}

		public void setProxyPassword(String proxyPassword) {
			this.proxyPassword = proxyPassword;
		}
	}

	public void init(EmailConfiguration configuration) throws Exception {
		this.configuration = configuration;
		Properties prop = new Properties();
		prop.put("mail.smtp.host", configuration.getMailServerHost());
		prop.put("mail.smtp.port", configuration.getMailServerPort());
		if (configuration.isAuthenticationRequired()) {
			prop.put("mail.smtp.auth", "true");
		} else {
			prop.remove("mail.smtp.auth");
		}
		if (configuration.isSslRequired()) {
			prop.put("mail.smtp.starttls.enable", "true");
			prop.put("mail.smtp.ssl.enable", "true");
			prop.put("mail.smtp.ssl.socketFactory", "javax.net.ssl.SSLSocketFactory");
			prop.put("mail.smtp.socketFactory.fallback", "false");
			prop.put("mail.smtp.socketFactory.port", configuration.getMailServerPort());

		} else {
			prop.remove("mail.smtp.starttls.enable");
		}

		session = Session.getInstance(prop);
		transport = session.getTransport("smtp");
		if (configuration.isAuthenticationRequired()) {
			transport.connect(configuration.getMailServerHost(), configuration.getUsername(), configuration.getPassword());
		} else {
			transport.connect();
		}
	}

	private Transport transport = null;
	private Session session = null;

	private EmailConfiguration configuration = null;

	public void destroy() {
		if (transport != null) {
			try {
				transport.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public void sendEmail(EmailContent content) throws Exception {
		Multipart multiPart = new MimeMultipart();
		if (content.getText() != null) {
			MimeBodyPart bodyPart = new MimeBodyPart();
			bodyPart.setText(content.getText());
			multiPart.addBodyPart(bodyPart);
		}
		if (content.getAttachList() != null) {
			Map<String, byte[]> attachmentList = content.getAttachList();
			for (String attachName : attachmentList.keySet()) {
				MimeBodyPart bodyPart = new MimeBodyPart();
				BufferedDataSource bds = new BufferedDataSource(attachmentList.get(attachName), attachName);
				bodyPart.setDataHandler(new DataHandler(bds));
				bodyPart.setFileName(bds.getName());
				multiPart.addBodyPart(bodyPart);
			}
		}

		MimeMessage message = new MimeMessage(session);
		if (content.getToList() != null && content.getToList().length > 0) {
			message.addRecipients(RecipientType.TO, getAddresses(content.getToList()));
		}
		if (content.getCcList() != null && content.getCcList().length > 0) {
			message.addRecipients(RecipientType.CC, getAddresses(content.getCcList()));
		}
		if (content.getBccList() != null && content.getBccList().length > 0) {
			message.addRecipients(RecipientType.BCC, getAddresses(content.getBccList()));
		}
		message.addFrom(getAddress(configuration.getFromEmail(), configuration.getFromName()));

		message.setSubject(content.getSubject());

		message.setSentDate(content.getRequestDate());

		message.setContent(multiPart);
		transport.sendMessage(message, message.getAllRecipients());
	}

	private InternetAddress[] getAddresses(String[] recepient) throws AddressException {
		InternetAddress[] addressList = new InternetAddress[recepient.length];
		for (int i = 0; i < recepient.length; ++i) {
			addressList[i] = new InternetAddress(recepient[i]);
		}
		return addressList;
	}

	private InternetAddress[] getAddress(String email, String name) throws UnsupportedEncodingException {
		InternetAddress[] addressList = new InternetAddress[1];
		InternetAddress address = new InternetAddress(email, name);
		addressList[0] = address;
		return addressList;
	}

	private static class BufferedDataSource implements DataSource {

		private byte[] _data;
		private java.lang.String _name;

		public BufferedDataSource(byte[] data, String name) {
			_data = data;
			_name = name;
		}

		public String getContentType() {
			return "application/octet-stream";

		}

		public InputStream getInputStream() throws IOException {
			return new ByteArrayInputStream(_data);

		}

		public String getName() {
			return _name;

		}

		public OutputStream getOutputStream() throws IOException {
			OutputStream out = new ByteArrayOutputStream();
			out.write(_data);
			return out;

		}

		@SuppressWarnings("unused")
		public Connection getConnection() throws SQLException {
			return null;
		}

		@SuppressWarnings("unused")
		public Connection getConnection(String username, String password) throws SQLException {
			return null;
		}

		@SuppressWarnings("unused")
		public PrintWriter getLogWriter() throws SQLException {
			return null;
		}

		@SuppressWarnings("unused")
		public int getLoginTimeout() throws SQLException {
			return 0;
		}

		@SuppressWarnings("unused")
		public void setLogWriter(PrintWriter out) throws SQLException {

		}

		@SuppressWarnings("unused")
		public void setLoginTimeout(int seconds) throws SQLException {

		}

	}

}
