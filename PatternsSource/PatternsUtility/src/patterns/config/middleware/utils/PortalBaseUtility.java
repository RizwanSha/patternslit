package patterns.config.middleware.utils;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;

public class PortalBaseUtility {

	private DBContext dbContext = null;

	public PortalBaseUtility(DBContext dbContext) {
		this.dbContext = dbContext;
	}

	public final DBContext getDbContext() {
		return dbContext;
	}

	public final void close() {
		if (dbContext != null)
			dbContext.close();
	}

	public static String decodeBooleanToString(boolean value) {
		return value ? RegularConstants.COLUMN_ENABLE : RegularConstants.COLUMN_DISABLE;
	}

	public static boolean decodeStringToBoolean(String value) {
		return (value != null && value.equals(RegularConstants.COLUMN_ENABLE)) ? true : false;
	}

	public static boolean isValidDateTime(String value) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		try {
			sdf.parse(value);
		} catch (ParseException e) {
			return false;
		}
		return true;
	}

	public static boolean isValidDate(String value) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		try {
			sdf.parse(value);
		} catch (ParseException e) {
			return false;
		}
		return true;
	}
	
	public static void main(String[] args){
		System.out.println(isValidDateTime("20110127155159"));
	}
}
