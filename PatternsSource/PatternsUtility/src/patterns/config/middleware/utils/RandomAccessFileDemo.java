package patterns.config.middleware.utils;

import java.io.RandomAccessFile;
import java.util.StringTokenizer;

public class RandomAccessFileDemo {

	public static void main(String[] args) throws Exception {
		fetchRecord(4940000, 4940988);
	}

	private static void fetchRecord(long startOffset, long endOffset) throws Exception {
		RandomAccessFile file = null;
		try {
			long lengthToRead = endOffset - startOffset;
			file = new RandomAccessFile("D:\\PPBS\\Work\\FileUpload\\NREGA\\nrega.txt", "r");
			file.seek(startOffset);
			byte[] bytes = new byte[(int) lengthToRead];
			file.read(bytes);

			StringTokenizer st = new StringTokenizer(new String(bytes),"\\n");
			while(st.hasMoreElements()){
				System.out.println(st.nextElement());
			}

		} catch (Exception e) {
			throw new Exception(e);
		} finally {
			if (file != null)
				file.close();
		}
	}
}