package patterns.config.middleware.utils;

import java.io.StringReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.xml.sax.InputSource;

import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.interfaceref.JAXBContextFactory;
import patterns.config.middleware.constants.PortalInterfaceErrorCodes;

public class PortalMessageParserUtility {

	public Object parseMessage(String messageContent) throws TBAFrameworkException {
		StringReader reader = null;
		JAXBContext jaxbContext = null;
		Unmarshaller unmarshaller = null;
		Object object;
		try {
			reader = new StringReader(messageContent);
			InputSource source = new InputSource(reader);
			jaxbContext = JAXBContextFactory.getJAXBContext();
			unmarshaller = jaxbContext.createUnmarshaller();
			object = unmarshaller.unmarshal(source);
		} catch (Exception e) {
			e.printStackTrace();
			throw new TBAFrameworkException(PortalInterfaceErrorCodes.ERROR_PARSING_FAILURE);
		}
		return object;
	}
}
