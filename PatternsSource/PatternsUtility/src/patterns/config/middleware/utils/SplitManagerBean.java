package patterns.config.middleware.utils;

import java.io.Serializable;

public class SplitManagerBean implements Serializable {
	private static final long serialVersionUID = -4953991114128626208L;
	private long numberOfRecords;
	private long startOffSet;
	private long length;
	private boolean isFileBasedTransaction;
	private long tranSl;

	public long getNumberOfRecords() {
		return numberOfRecords;
	}

	public void setNumberOfRecords(long numberOfRecords) {
		this.numberOfRecords = numberOfRecords;
	}

	public long getStartOffSet() {
		return startOffSet;
	}

	public void setStartOffSet(long startOffSet) {
		this.startOffSet = startOffSet;
	}

	public long getLength() {
		return length;
	}

	public void setLength(long length) {
		this.length = length;
	}

	public boolean isFileBasedTransaction() {
		return isFileBasedTransaction;
	}

	public void setFileBasedTransaction(boolean isFileBasedTransaction) {
		this.isFileBasedTransaction = isFileBasedTransaction;
	}

	public long getTranSl() {
		return tranSl;
	}

	public void setTranSl(long tranSl) {
		this.tranSl = tranSl;
	}
}