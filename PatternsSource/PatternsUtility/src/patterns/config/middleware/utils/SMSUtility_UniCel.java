package patterns.config.middleware.utils;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Date;
import java.util.ArrayList;

public class SMSUtility_UniCel {

	public static class SMSContent {
		private Date requestDate;
		private String text;
		private String[] toList;
		private ArrayList<String> attachList;

		public ArrayList<String> getAttachList() {
			return attachList;
		}

		public void setAttachList(ArrayList<String> attachList) {
			this.attachList = attachList;
		}

		public String getText() {
			return text;
		}

		public void setText(String text) {
			this.text = text;
		}

		public String[] getToList() {
			return toList;
		}

		public void setToList(String[] toList) {
			this.toList = toList;
		}

		public void setRequestDate(Date requestDate) {
			this.requestDate = requestDate;
		}

		public Date getRequestDate() {
			return requestDate;
		}
	}

	public static class SMSConfiguration {

		private String serverURL;
		private String httpMethod;
		private boolean proxyRequired;
		private String proxyServerIP;
		private int proxyServerPort;
		private boolean proxyAuthenticationRequired;
		private String proxyUsername;
		private String proxyPassword;
		private boolean authenticationRequired;
		private String userNameKey;
		private String userName;
		private String passwordKey;
		private String password;
		private String mobileNumberKey;
		private String textKey;
		private String senderKey;
		private String senderName;

		public String getServerURL() {
			return serverURL;
		}

		public void setServerURL(String serverURL) {
			this.serverURL = serverURL;
		}

		public String getHttpMethod() {
			return httpMethod;
		}

		public void setHttpMethod(String httpMethod) {
			this.httpMethod = httpMethod;
		}

		public boolean isProxyRequired() {
			return proxyRequired;
		}

		public void setProxyRequired(boolean proxyRequired) {
			this.proxyRequired = proxyRequired;
		}

		public String getProxyServerIP() {
			return proxyServerIP;
		}

		public void setProxyServerIP(String proxyServerIP) {
			this.proxyServerIP = proxyServerIP;
		}

		public int getProxyServerPort() {
			return proxyServerPort;
		}

		public void setProxyServerPort(int proxyServerPort) {
			this.proxyServerPort = proxyServerPort;
		}

		public boolean isProxyAuthenticationRequired() {
			return proxyAuthenticationRequired;
		}

		public void setProxyAuthenticationRequired(boolean proxyAuthenticationRequired) {
			this.proxyAuthenticationRequired = proxyAuthenticationRequired;
		}

		public String getProxyUsername() {
			return proxyUsername;
		}

		public void setProxyUsername(String proxyUsername) {
			this.proxyUsername = proxyUsername;
		}

		public String getProxyPassword() {
			return proxyPassword;
		}

		public void setProxyPassword(String proxyPassword) {
			this.proxyPassword = proxyPassword;
		}

		public boolean isAuthenticationRequired() {
			return authenticationRequired;
		}

		public void setAuthenticationRequired(boolean authenticationRequired) {
			this.authenticationRequired = authenticationRequired;
		}

		public String getUserNameKey() {
			return userNameKey;
		}

		public void setUserNameKey(String userNameKey) {
			this.userNameKey = userNameKey;
		}

		public String getUserName() {
			return userName;
		}

		public void setUserName(String userName) {
			this.userName = userName;
		}

		public String getPasswordKey() {
			return passwordKey;
		}

		public void setPasswordKey(String passwordKey) {
			this.passwordKey = passwordKey;
		}

		public String getPassword() {
			return password;
		}

		public void setPassword(String password) {
			this.password = password;
		}

		public String getMobileNumberKey() {
			return mobileNumberKey;
		}

		public void setMobileNumberKey(String mobileNumberKey) {
			this.mobileNumberKey = mobileNumberKey;
		}

		public String getTextKey() {
			return textKey;
		}

		public void setTextKey(String textKey) {
			this.textKey = textKey;
		}

		public String getSenderKey() {
			return senderKey;
		}

		public void setSenderKey(String senderKey) {
			this.senderKey = senderKey;
		}

		public String getSenderName() {
			return senderName;
		}

		public void setSenderName(String senderName) {
			this.senderName = senderName;
		}
	}

	private SMSConfiguration configuration = null;

	public void init(SMSConfiguration configuration) throws Exception {
		this.configuration = configuration;
	}

	public boolean sendSMS(SMSContent content) {
		if (configuration.getHttpMethod().equals("1")) {
			return sendGet(content);
		} else if (configuration.getHttpMethod().equals("2")) {
			return sendPost(content);
		}
		return false;
	}

	private boolean sendGet(SMSContent content) {
		boolean status = false;
		try {
			URL obj = null;
			HttpURLConnection con = null;

			StringBuffer urlParameters = new StringBuffer();

			urlParameters.append(configuration.getServerURL()).append("?");

			if (!configuration.proxyRequired) {

				if (configuration.authenticationRequired) {

					if (content.getToList() != null) {
						for (String destination : content.getToList()) {
							urlParameters.append(configuration.getUserNameKey()).append("=").append(configuration.getUserName()).append("&");
							urlParameters.append(configuration.getPasswordKey()).append("=").append(configuration.getPassword()).append("&").append(configuration.getSenderKey());
							urlParameters.append("=").append(configuration.getSenderName()).append("&").append(configuration.getMobileNumberKey()).append("=");
							urlParameters = urlParameters.append(destination).append("&").append(configuration.getTextKey()).append("=").append(content.getText());

							obj = new URL(urlParameters.toString());
							con = (HttpURLConnection) obj.openConnection();

							// add reuqest header
							con.setRequestMethod("GET");
							// con.setRequestProperty("User-Agent", USER_AGENT);

							int responseCode = con.getResponseCode();
							System.out.println("\nSending 'GET' request to URL : " + urlParameters.toString());
							System.out.println("Response Code : " + responseCode);

							BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
							String inputLine;
							StringBuffer response = new StringBuffer();

							while ((inputLine = in.readLine()) != null) {
								response.append(inputLine);
							}
							in.close();

							// print result
							System.out.println(response.toString());
							status = true;
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			status = false;
		}
		return status;
	}

	private boolean sendPost(SMSContent content) {
		boolean status = false;
		try {
			URL obj = null;
			HttpURLConnection con = null;
			if (!configuration.proxyRequired) {

				if (configuration.authenticationRequired) {

					if (content.getToList() != null) {

						obj = new URL(configuration.getServerURL());
						con = (HttpURLConnection) obj.openConnection();

						// add reuqest header
						con.setRequestMethod("POST");
						// con.setRequestProperty("User-Agent", USER_AGENT);
						con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

						StringBuffer urlParameters = new StringBuffer();

						for (String destination : content.getToList()) {
							urlParameters.append(configuration.getUserNameKey()).append("=").append(configuration.getUserName()).append("&");
							urlParameters.append(configuration.getPasswordKey()).append("=").append(configuration.getPassword()).append("&").append(configuration.getSenderKey());
							urlParameters.append("=").append(configuration.getSenderName()).append("&").append(configuration.getMobileNumberKey()).append("=");
							urlParameters = urlParameters.append(destination).append("&").append(configuration.getTextKey()).append("=").append(content.getText());

							// Send post request
							con.setDoOutput(true);
							DataOutputStream wr = new DataOutputStream(con.getOutputStream());
							wr.writeBytes(urlParameters.toString());
							wr.flush();
							wr.close();

							int responseCode = con.getResponseCode();
							System.out.println("\nSending 'POST' request to URL : " + configuration.getServerURL());
							System.out.println("Post parameters : " + urlParameters.toString());
							System.out.println("Response Code : " + responseCode);

							BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
							String inputLine;
							StringBuffer response = new StringBuffer();

							while ((inputLine = in.readLine()) != null) {
								response.append(inputLine);
							}
							in.close();
							// print result
							System.out.println(response.toString());
							con.disconnect();
							status = true;
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			status = false;
		}
		return status;
	}
}
