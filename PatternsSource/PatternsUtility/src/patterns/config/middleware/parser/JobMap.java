package patterns.config.middleware.parser;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import patterns.config.framework.loggers.ApplicationLogger;

public class JobMap {

	private final ReentrantReadWriteLock readWriteLock = new ReentrantReadWriteLock();

	private final Lock read = readWriteLock.readLock();

	private final Lock write = readWriteLock.writeLock();

	private static final ReentrantReadWriteLock readWriteLockGlobal = new ReentrantReadWriteLock();

	private static final Lock writeGlobal = readWriteLockGlobal.writeLock();

	private ApplicationLogger logger = null;

	private Map<String, JobConfiguration> internalMap;

	private static JobMap instance = null;

	private JobMap() {
		logger = ApplicationLogger.getInstance(this.getClass().getSimpleName());
		logger.logDebug("#()");
	}

	protected void resetConfiguration() {
		logger.logDebug("resetConfiguration()");
		if (internalMap == null)
			internalMap = new HashMap<String, JobConfiguration>();
		internalMap.clear();
	}

	public static JobMap getInstance() {
		if (instance == null) {
			reloadConfiguration();
		}
		return instance;
	}

	public static void unloadConfiguration() {
		writeGlobal.lock();
		if (instance != null) {
			instance.resetConfiguration();
			instance = null;
		}
		writeGlobal.unlock();
	}

	public static void reloadConfiguration() {
		writeGlobal.lock();
		if (instance == null) {
			instance = new JobMap();
		}
		instance.resetConfiguration();
		instance.loadConfiguration();
		writeGlobal.unlock();
	}

	public JobConfiguration getProgram(String tablePrefix) {
		logger.logDebug("getProgram()");
		read.lock();
		JobConfiguration configuration = null;
		try {
			configuration = internalMap.get(tablePrefix);
			if (configuration == null) {
				logger.logError("#getProgram(1) - Empty configuration: " + tablePrefix);
				return null;
			}

		} catch (Exception e) {
			e.printStackTrace();
			logger.logError(" getProgram(2) - " + e.getLocalizedMessage());
		} finally {
			read.unlock();
		}
		return configuration;
	}

	protected void loadConfiguration() {
		logger.logDebug("loadConfiguration()");
		write.lock();
		try {
			String configurationFileName = "/panacea/portal/middleware/parser/PORTAL-JobConfig.xml";
			InputStream is = null;
			JobConfiguration configuration = null;
			try {
				is = JobMap.class.getResourceAsStream(configurationFileName);
			} catch (Exception e) {
				logger.logError(" loadConfiguration - Error Reading File " + configurationFileName + " - " + e.getLocalizedMessage());
				return;
			}
			try {
				XMLDOMParser parser = new XMLDOMParser();
				Document doc = parser.parseXML(is);

				// Load map
				NodeList requestNodes = doc.getElementsByTagName("JobInfo");
				for (int li = 0; li < requestNodes.getLength(); li++) {
					configuration = new JobConfiguration();
					Element ele = (Element) requestNodes.item(li);
					NodeList prefixList = ele.getElementsByTagName("TPrefix");
					String tblPrefix = prefixList.item(0).getTextContent();
					NodeList handlerList = ele.getElementsByTagName("Handler");
					configuration.setMessageHandler(handlerList.item(0).getTextContent());
					NodeList transactionList = ele.getElementsByTagName("TransactionManager");
					configuration.setTransactionManager(transactionList.item(0).getTextContent());
					internalMap.put(tblPrefix, configuration);
				}
			} catch (Exception e) {
				logger.logError(" loadConfiguration() - Parsing Error " + configurationFileName + " - " + e.getLocalizedMessage());
				return;
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError(" loadConfiguration() - " + e.getLocalizedMessage());
		} finally {
			write.unlock();
		}
	}

}
