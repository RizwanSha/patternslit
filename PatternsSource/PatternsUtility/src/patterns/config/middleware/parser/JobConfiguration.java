package patterns.config.middleware.parser;

public class JobConfiguration {

	public String messageHandler;
	public String transactionManager;

	public String getMessageHandler() {
		return messageHandler;
	}

	public void setMessageHandler(String messageHandler) {
		this.messageHandler = messageHandler;
	}

	public String getTransactionManager() {
		return transactionManager;
	}

	public void setTransactionManager(String transactionManager) {
		this.transactionManager = transactionManager;
	}

}
