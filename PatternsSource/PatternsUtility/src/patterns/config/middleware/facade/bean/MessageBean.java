package patterns.config.middleware.facade.bean;

import java.io.Serializable;

public class MessageBean implements Serializable {

	private static final long serialVersionUID = 2648948863974322570L;
	private String entity;
	private String inventoryNumber;
	private String systemCode;
	private String utrNo;
	private String tranRefNo;
	private String messageSubType;
	private String senderIfsc;
	private String controller;
	private String tablePrefix;
	private String serviceRefNum;
	private String processingCode;

	public String getEntity() {
		return entity;
	}

	public void setEntity(String entity) {
		this.entity = entity;
	}

	public String getInventoryNumber() {
		return inventoryNumber;
	}

	public void setInventoryNumber(String inventoryNumber) {
		this.inventoryNumber = inventoryNumber;
	}

	public String getSystemCode() {
		return systemCode;
	}

	public void setSystemCode(String systemCode) {
		this.systemCode = systemCode;
	}

	public String getUtrNo() {
		return utrNo;
	}

	public void setUtrNo(String utrNo) {
		this.utrNo = utrNo;
	}

	public String getTranRefNo() {
		return tranRefNo;
	}

	public void setTranRefNo(String tranRefNo) {
		this.tranRefNo = tranRefNo;
	}

	public String getMessageSubType() {
		return messageSubType;
	}

	public void setMessageSubType(String messageSubType) {
		this.messageSubType = messageSubType;
	}

	public String getSenderIfsc() {
		return senderIfsc;
	}

	public void setSenderIfsc(String senderIfsc) {
		this.senderIfsc = senderIfsc;
	}

	public String getController() {
		return controller;
	}

	public void setController(String controller) {
		this.controller = controller;
	}

	public String getTablePrefix() {
		return tablePrefix;
	}

	public void setTablePrefix(String tablePrefix) {
		this.tablePrefix = tablePrefix;
	}

	public String getServiceRefNum() {
		return serviceRefNum;
	}

	public void setServiceRefNum(String serviceRefNum) {
		this.serviceRefNum = serviceRefNum;
	}

	public String getProcessingCode() {
		return processingCode;
	}

	public void setProcessingCode(String processingCode) {
		this.processingCode = processingCode;
	}

}
