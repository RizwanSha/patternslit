package patterns.config.middleware.facade.bean;

import java.io.Serializable;

public class NotificationBean implements Serializable {

	private static final long serialVersionUID = 6037463187815296731L;
	private String messageSubType;
	private String tranRefNum;
	private String utrNum;
	private String senderIFSC;
	private String receiverIFSC;

	/**
	 * @param messageSubType
	 *            the messageSubType to set
	 */
	public void setMessageSubType(String messageSubType) {
		this.messageSubType = messageSubType;
	}

	/**
	 * @return the messageSubType
	 */
	public String getMessageSubType() {
		return messageSubType;
	}

	/**
	 * @param tranRefNum
	 *            the tranRefNum to set
	 */
	public void setTranRefNum(String tranRefNum) {
		this.tranRefNum = tranRefNum;
	}

	/**
	 * @return the tranRefNum
	 */
	public String getTranRefNum() {
		return tranRefNum;
	}

	/**
	 * @param utrNum
	 *            the utrNum to set
	 */
	public void setUtrNum(String utrNum) {
		this.utrNum = utrNum;
	}

	/**
	 * @return the utrNum
	 */
	public String getUtrNum() {
		return utrNum;
	}

	/**
	 * @param senderIFSC
	 *            the senderIFSC to set
	 */
	public void setSenderIFSC(String senderIFSC) {
		this.senderIFSC = senderIFSC;
	}

	/**
	 * @return the senderIFSC
	 */
	public String getSenderIFSC() {
		return senderIFSC;
	}

	/**
	 * @param receiverIFSC
	 *            the receiverIFSC to set
	 */
	public void setReceiverIFSC(String receiverIFSC) {
		this.receiverIFSC = receiverIFSC;
	}

	/**
	 * @return the receiverIFSC
	 */
	public String getReceiverIFSC() {
		return receiverIFSC;
	}

}
