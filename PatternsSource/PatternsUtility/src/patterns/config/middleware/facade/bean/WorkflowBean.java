package patterns.config.middleware.facade.bean;

import java.io.Serializable;
import java.sql.Timestamp;

public class WorkflowBean implements Serializable {

	private static final long serialVersionUID = 3361775071434818279L;
	private String entity;
	private String serviceReferenceNumber;
	private String eventCode;
	private String stageID;
	private String tablePrefix;
	private Timestamp eventTimestamp;

	public String getEntity() {
		return entity;
	}

	public void setEntity(String entity) {
		this.entity = entity;
	}

	public String getServiceReferenceNumber() {
		return serviceReferenceNumber;
	}

	public void setServiceReferenceNumber(String serviceReferenceNumber) {
		this.serviceReferenceNumber = serviceReferenceNumber;
	}

	public String getTablePrefix() {
		return tablePrefix;
	}

	public void setTablePrefix(String tablePrefix) {
		this.tablePrefix = tablePrefix;
	}

	public String getEventCode() {
		return eventCode;
	}

	public void setEventCode(String eventCode) {
		this.eventCode = eventCode;
	}

	public String getStageID() {
		return stageID;
	}

	public void setStageID(String stageID) {
		this.stageID = stageID;
	}

	public Timestamp getEventTimestamp() {
		return eventTimestamp;
	}

	public void setEventTimestamp(Timestamp eventTimestamp) {
		this.eventTimestamp = eventTimestamp;
	}

}
