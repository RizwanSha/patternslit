package patterns.config.middleware.constants;

public class Constants {

	public static final String BO_RESULT = "middleware.bo.result";
	public static final String BO_INPUT = "middleware.bo.input";
	public static final String BO_CLASS = "_BO_CLASS";
	public static final String BO_SERVICE_TYPE = "middleware.bo.servicetype";
	public static final String BO_AUTH_REQUIRED = "middleware.bo.authrequired";

	public static final String BO_PC = "middleware.bo.PC";
	public static final String BO_SYSTEM_CODE = "middleware.bo.systemcode";
	public static final String BO_MSG_TYPE = "middleware.bo.messagetype";
	public static final String BO_INVENTORY_NUM = "middleware.bo.inventoryno";

	public static final String SERVICE_TYPE_ENQUIRY = "1";
	public static final String SERVICE_TYPE_REQUEST = "2";
	public static final String SERVICE_TYPE_TRANSACTION = "3";

	public static final String MSG_TYPE_REQUEST = "1";
	public static final String MSG_TYPE_RESPONSE = "2";

	public static final String MSG_DIRECTION_INBOUND_REQUEST = "1";
	public static final String MSG_DIRECTION_INBOUND_RESPONSE = "2";
	public static final String MSG_DIRECTION_NONE = "0";

	public static final int MTI_REQUEST = 1;

	public static final String COLUMN_ENABLE = "1";
	public static final String COLUMN_DISABLE = "0";

	public static final String JOB_ENABLE = "1";
	public static final String JOB_DISABLE = "2";

	public static final String EMPTY_STRING = "";

}
