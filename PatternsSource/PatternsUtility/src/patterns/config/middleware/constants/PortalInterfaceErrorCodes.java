package patterns.config.middleware.constants;

import java.util.ResourceBundle;

public class PortalInterfaceErrorCodes {
	public static final String SUCCESS = "S0000";
	public static final String SUCCESS_FORWARD_CBS = "S0001";
	public static final String SUCCESS_DUPLICATE_REFERENCE = "F0003";

	public static final String FAILURE = "F0000";
	public static final String FAILURE_PARAMETERS_INSUFFICIENT = "F0001";
	public static final String FAILURE_PARAMETERS_VALIDATION = "F0002";
	public static final String FAILURE_UPDATION_STAGING = "F0003";
	public static final String FAILURE_UPDATION_SERVICE_LOG = "F0004";

	public static final String ERROR_UNEXPECTED = "P0000";
	public static final String ERROR_PARSING_FAILURE = "P0001";
	public static final String ERROR_PARSING_FAILURE_INIT = "P0001a";
	public static final String ERROR_PC_INVALID = "P0002";
	public static final String ERROR_PC_INVALID_INIT = "P0002a";
	public static final String ERROR_PC_DISABLED = "P0003";
	public static final String ERROR_DIRECTION_UPDATION = "P0004";
	public static final String ERROR_DIRECTION_UPDATION_INIT = "P0004a";
	public static final String ERROR_MTI_CONFIG_UNAVAILABLE = "P0005";
	public static final String ERROR_MTI_CONFIG_INIT = "P0005a";
	public static final String ERROR_REQUEST_DATA_UNAVAILABLE = "P0006";
	public static final String ERROR_OUTBOUND_DATA_UNAVAILABLE = "P0007";
	public static final String ERROR_MESSAGE_HANDLER_UNAVAILABLE = "P0008";
	public static final String ERROR_MESSAGE_HANDLER_INIT = "P0008a";
	public static final String ERROR_MESSAGE_HANDLER_PROCESSING = "P0009";
	public static final String ERROR_QUEUE_CONFIGURATION_UNAVAILABLE = "P0010";
	public static final String ERROR_QUEUE_CONFIGURATION_INIT = "P0010a";
	public static final String ERROR_MESSAGE_MARSHALLING_FAILURE = "P0011";
	public static final String ERROR_HEADER_UPD_FAILURE = "P0012";
	public static final String ERROR_HEADER_UPD_FAILURE_INIT = "P0012a";
	public static final String ERROR_MESSAGE_CONTENT_RETRIEVAL = "P0013";
	public static final String ERROR_SYSTEM_PROCESSING_CODE_INIT = "P0200";
	public static final String ERROR_PAYLOAD_NULL = "P0201";
	public static final String ERROR_MESSAGE_STATUS_UPDATE = "P0202";
	public static final String ERROR_AUTO_VALIDATION_FAILURE = "P0203";
	public static final String ERROR_AUTO_UPDATION_FAILURE = "P0204";

	public static final String INVALID_CUSTOMER = "F0001";
	public static final String INSUFF_INPUT = "F0023";
	public static final String NO_ACNTS_FOUND = "F0022";
	 public static final String INVALID_ACCOUNT = "F0006"; 

	private static ResourceBundle resourceBundle = ResourceBundle.getBundle("panacea/middleware/portal/constants/PortalMessages");

	public static String getMessage(String key) {
		return resourceBundle.getString(key);
	}
}
