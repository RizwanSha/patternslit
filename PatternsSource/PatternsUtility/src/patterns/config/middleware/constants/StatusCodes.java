package patterns.config.middleware.constants;

public class StatusCodes {
	
	public static final int PENDING = 1;
	public static final int CONSUMED = 2;
	public static final int PROCESSED = 3;
	public static final int PENDING_DELIVERY = 4;
	public static final int DELIVERED = 5;
	public static final int ERROR = 6;
	public static final int DELIVERY_SUCCESS = 7;
	public static final int DELIVERY_FAILURE = 8;

}
