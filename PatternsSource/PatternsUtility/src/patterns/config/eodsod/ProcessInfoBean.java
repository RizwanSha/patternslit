package patterns.config.eodsod;

public class ProcessInfoBean {
	private String processName;
	private String processState;
	private boolean isRoutine;
	private boolean isHandler;
	private String routineName;
	private String handlerName;
	private String handlerMethodName;
	private boolean allowedEod;
	private boolean allowedSod;

	public String getProcessName() {
		return processName;
	}

	public void setProcessName(String processName) {
		this.processName = processName;
	}

	public String getProcessState() {
		return processState;
	}

	public void setProcessState(String processState) {
		this.processState = processState;
	}

	public boolean isRoutine() {
		return isRoutine;
	}

	public void setRoutine(boolean isRoutine) {
		this.isRoutine = isRoutine;
	}

	public boolean isHandler() {
		return isHandler;
	}

	public void setHandler(boolean isHandler) {
		this.isHandler = isHandler;
	}

	public String getRoutineName() {
		return routineName;
	}

	public void setRoutineName(String routineName) {
		this.routineName = routineName;
	}

	public String getHandlerName() {
		return handlerName;
	}

	public void setHandlerName(String handlerName) {
		this.handlerName = handlerName;
	}

	public String getHandlerMethodName() {
		return handlerMethodName;
	}

	public void setHandlerMethodName(String handlerMethodName) {
		this.handlerMethodName = handlerMethodName;
	}

	public boolean isAllowedEod() {
		return allowedEod;
	}

	public void setAllowedEod(boolean allowedEod) {
		this.allowedEod = allowedEod;
	}

	public boolean isAllowedSod() {
		return allowedSod;
	}

	public void setAllowedSod(boolean allowedSod) {
		this.allowedSod = allowedSod;
	}
}
