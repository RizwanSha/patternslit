package patterns.config.eodsod;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.Map;

import patterns.config.delegate.ProcessManager;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.loggers.ApplicationLogger;
import patterns.config.framework.pki.ProcessTFAInfo;
import patterns.config.framework.process.TBAProcessResult;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.thread.ApplicationContext;

public class EodSodUtil implements Runnable {

	private ApplicationLogger logger = null;
	private Map<String, ProcessInfoBean> processInfoMap;
	private DBContext dbContext;
	private String partitionNumber = RegularConstants.EMPTY_STRING;
	private String entityCode = RegularConstants.EMPTY_STRING;
	private static final String ROUTINE_PROCESSOR = "patterns.config.framework.bo.eodsod.EodSodRoutineProcessor";
	private Date currentDate;
	private int runSerial;
	int successCount = 0;
	private String processType;
	private static final String PROCESS_SUCCESS = "S";
	private static final String PROCESS_FAILURE = "F";
	private static final String PROCESS_PENDING = "P";
	private static final String PROCESS_STOP_ON_ERROR = "A";
	private static final String EOD_PROCESS = "E";
	private static final int PROCESS_TIMEOUT = 0;
	boolean isSuccess = false;
	private ApplicationContext context;

	public DBContext getDbContext() {
		return dbContext;
	}

	@SuppressWarnings("unchecked")
	public EodSodUtil(DTObject inputDTO) {
		logger = ApplicationLogger.getInstance(this.getClass().getSimpleName());
		this.processInfoMap = (Map<String, ProcessInfoBean>) inputDTO.getObject("PROCESS_MAP");
		this.context = (ApplicationContext) inputDTO.getObject("CONTEXT");
		this.entityCode = context.getEntityCode();
		this.partitionNumber = context.getPartitionNo();
		this.currentDate = (Date) inputDTO.getObject("CBD");
		this.runSerial = Integer.parseInt(inputDTO.get("RUN_SERIAL"));
		this.processType = inputDTO.get("PROCESS_TYPE");
	}

	public void run() {
		for (String processName : processInfoMap.keySet()) {
			ProcessInfoBean processBean = processInfoMap.get(processName);
			try {
				isSuccess = false;
				if (initiateProcess(processName, processType)) {
					if (processBean.isRoutine()) {
						DTObject inputDTO = new DTObject();
						inputDTO.set("ENTITY_CODE", context.getEntityCode());
						inputDTO.set("PARTITION_NO", partitionNumber);
						inputDTO.setObject("CBD", currentDate);
						inputDTO.set("ROUTINE_NAME", processBean.getRoutineName());

						ProcessTFAInfo processTFAInfo = new ProcessTFAInfo();
						processTFAInfo.setUserId(context.getUserID());
						processTFAInfo.setDeploymentContext(context.getDeploymentContext());
						processTFAInfo.setDigitalCertificateInventoryNumber(context.getDigitalCertificateInventoryNumber());
						processTFAInfo.setTfaRequired("0");

						TBAProcessResult processResult = null;
						try {
							ProcessManager processManager = new ProcessManager();
							inputDTO.set("PARTITION_NO", context.getPartitionNo());
							inputDTO.set("USER_ID", context.getUserID());
							inputDTO.set("IP_ADDRESS", context.getClientIP());
							inputDTO.setObject("CBD", context.getCurrentBusinessDate());
							inputDTO.setObject("PKI", processTFAInfo);
							inputDTO.setObject("PROCESS_TIMEOUT", PROCESS_TIMEOUT);
							inputDTO.set(RegularConstants.PROCESSBO_CLASS, ROUTINE_PROCESSOR);
							processResult = processManager.delegate(inputDTO);
						} catch (Exception e) {
							e.printStackTrace();
							logger.logError(e.getLocalizedMessage());
							processResult.setProcessStatus(TBAProcessStatus.FAILURE);
						}
						isSuccess = true;
						if (!processResult.getProcessStatus().equals(TBAProcessStatus.SUCCESS)) {
							inputDTO.set("STATUS", "F");
							inputDTO.set("ERROR_CODE", "999");
							isSuccess = false;
						}
						if (processResult.getProcessStatus().equals(TBAProcessStatus.SUCCESS) && processResult.getResponseDTO() != null) {
							if (processResult.getResponseDTO().get(RegularConstants.ERROR) != null
									|| processResult.getResponseDTO().get(RegularConstants.ADDITIONAL_INFO) != null) {
								inputDTO.set("STATUS", "F");
								inputDTO.set("ERROR_CODE", "999");
								isSuccess = false;
							}
						}
						if (isSuccess) {
							inputDTO = (DTObject) processResult.getAdditionalInfo();
						}
						if (inputDTO.get("STATUS").equals(PROCESS_SUCCESS)) {
							updateProcessStatus(false, PROCESS_SUCCESS, processName, null);
							successCount++;
							isSuccess = true;
						} else {
							updateProcessStatus(false, PROCESS_FAILURE, processName, inputDTO.get("ERROR_CODE"));
							isSuccess = false;
						}
					} else if (processBean.isHandler()) {
						DTObject inputObject = new DTObject();

						inputObject.set("ENTITY_CODE", entityCode);
						ProcessTFAInfo processTFAInfo = new ProcessTFAInfo();
						processTFAInfo.setUserId(context.getUserID());
						processTFAInfo.setDeploymentContext(context.getDeploymentContext());
						processTFAInfo.setDigitalCertificateInventoryNumber(context.getDigitalCertificateInventoryNumber());
						processTFAInfo.setTfaRequired("0");

						TBAProcessResult processResult = null;
						try {
							ProcessManager processManager = new ProcessManager();
							inputObject.set("PARTITION_NO", context.getPartitionNo());
							inputObject.set("USER_ID", context.getUserID());
							inputObject.set("IP_ADDRESS", context.getClientIP());
							inputObject.setObject("PKI", processTFAInfo);
							inputObject.setObject("CBD", currentDate);
							inputObject.setObject("PROCESS_TIMEOUT", PROCESS_TIMEOUT);
							inputObject.set(RegularConstants.PROCESSBO_CLASS, processBean.getHandlerName());
							processResult = processManager.delegate(inputObject);
						} catch (Exception e) {
							e.printStackTrace();
							logger.logError(e.getLocalizedMessage());
							processResult.setProcessStatus(TBAProcessStatus.FAILURE);
						}

						String errorCode = RegularConstants.EMPTY_STRING;
						String status = RegularConstants.EMPTY_STRING;
						if (processResult.getProcessStatus().equals(TBAProcessStatus.SUCCESS)) {
							isSuccess = true;
							status = PROCESS_SUCCESS;
							successCount++;
						} else {
							isSuccess = false;
							errorCode = BackOfficeErrorCodes.UNSPECIFIED_ERROR;
							status = PROCESS_FAILURE;
						}
						updateProcessStatus(false, status, processName, errorCode);
					}
				}

				if (!isSuccess && processBean.getProcessState().equals(PROCESS_STOP_ON_ERROR)) {
					break;
				}
			} catch (TBAFrameworkException e) {
				e.printStackTrace();
				logger.logError(e.getLocalizedMessage());
				try {
					throw new TBAFrameworkException(e.getLocalizedMessage());
				} catch (TBAFrameworkException e1) {
					e1.printStackTrace();
					logger.logError(e1.getLocalizedMessage());
				}
			}
		}

		String processStatus = PROCESS_FAILURE;
		if (successCount == processInfoMap.size()) {
			processStatus = PROCESS_SUCCESS;
		}
		EodSodProcess process = new EodSodProcess(entityCode, currentDate);
		try {
			if (processType.equals(EOD_PROCESS)) {
				process.updateEodStatus(runSerial, processStatus);
				process.updateMainContEod(false, processStatus.equals(PROCESS_SUCCESS) ? true : false, null);
			} else {
				process.updateSodStatus(runSerial, processStatus);
				if (!processStatus.equals(PROCESS_SUCCESS)) {
					process.updateMainContSod(false, false, null);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			process.close();
		}
	}

	private boolean initiateProcess(String processName, String processType) throws TBAFrameworkException {
		logger.logDebug("initiateProcess() Begin");
		dbContext = new DBContext();
		DBUtil dbUtil = dbContext.createUtilInstance();
		try {
			String status = RegularConstants.EMPTY_STRING;
			String sqlQuery = RegularConstants.EMPTY_STRING;

			if (processType.equals(EOD_PROCESS)) {
				sqlQuery = "SELECT PROCESS_STATUS FROM EODPROCESSRUNDTL WHERE ENTITY_CODE=? AND PROCESS_DATE=? AND PROCESS_ID=? AND RUN_SL=(SELECT MAX(RUN_SL) FROM EODPROCESSRUNDTL WHERE ENTITY_CODE=? AND PROCESS_DATE=? AND PROCESS_ID=?)";
			} else {
				sqlQuery = "SELECT PROCESS_STATUS FROM SODPROCESSRUNDTL WHERE ENTITY_CODE=? AND PROCESS_DATE=? AND PROCESS_ID=? AND RUN_SL=(SELECT MAX(RUN_SL) FROM SODPROCESSRUNDTL WHERE ENTITY_CODE=? AND PROCESS_DATE=? AND PROCESS_ID=?)";
			}

			dbUtil.reset();
			dbUtil.setSql(sqlQuery);
			dbUtil.setString(1, entityCode);
			dbUtil.setDate(2, currentDate);
			dbUtil.setString(3, processName);
			dbUtil.setString(4, entityCode);
			dbUtil.setDate(5, currentDate);
			dbUtil.setString(6, processName);
			ResultSet rset = dbUtil.executeQuery();
			if (rset.next()) {
				status = rset.getString(1);
			}
			if (status.equals(RegularConstants.EMPTY_STRING) || status.equals(PROCESS_FAILURE)) {
				return updateProcessStatus(true, PROCESS_PENDING, processName, null);
			}
			logger.logDebug("initiateProcess() End");
			successCount++;
			isSuccess = true;
			return false;
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError(e.getLocalizedMessage());
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			dbContext.close();
		}
	}

	private boolean updateProcessStatus(boolean isNew, String status, String processName, String errorCode) throws TBAFrameworkException {
		logger.logDebug("updateProcessStatus() Begin");
		dbContext = new DBContext();
		DBUtil dbUtil = dbContext.createUtilInstance();
		try {
			String sqlQuery = RegularConstants.EMPTY_STRING;
			int processSerial = 0;

			if (isNew) {
				if (processType.equals(EOD_PROCESS)) {
					sqlQuery = "SELECT COUNT(1)+1 FROM EODPROCESSRUNDTL WHERE ENTITY_CODE=? AND PROCESS_DATE=? AND RUN_SL=?";
				} else {
					sqlQuery = "SELECT COUNT(1)+1 FROM SODPROCESSRUNDTL WHERE ENTITY_CODE=? AND PROCESS_DATE=? AND RUN_SL=?";
				}
				dbUtil.reset();
				dbUtil.setSql(sqlQuery);
				dbUtil.setString(1, entityCode);
				dbUtil.setDate(2, currentDate);
				dbUtil.setInt(3, runSerial);
				ResultSet rset = dbUtil.executeQuery();
				if (rset.next()) {
					processSerial = rset.getInt(1);
				}

				if (processType.equals(EOD_PROCESS)) {
					sqlQuery = "INSERT INTO EODPROCESSRUNDTL (ENTITY_CODE,PROCESS_DATE,RUN_SL,SL,PROCESS_ID,PROCESS_START_TIME,PROCESS_STATUS) VALUES (?,?,?,?,?,FN_GETCDT(?),'P')";
				} else {
					sqlQuery = "INSERT INTO SODPROCESSRUNDTL (ENTITY_CODE,PROCESS_DATE,RUN_SL,SL,PROCESS_ID,PROCESS_START_TIME,PROCESS_STATUS) VALUES (?,?,?,?,?,FN_GETCDT(?),'P')";
				}
				dbUtil.reset();
				dbUtil.setSql(sqlQuery);
				dbUtil.setString(1, entityCode);
				dbUtil.setDate(2, currentDate);
				dbUtil.setInt(3, runSerial);
				dbUtil.setInt(4, processSerial);
				dbUtil.setString(5, processName);
				dbUtil.setString(6, entityCode);
				dbUtil.executeUpdate();
			} else {
				if (processType.equals(EOD_PROCESS)) {
					sqlQuery = "SELECT SL FROM EODPROCESSRUNDTL WHERE ENTITY_CODE=? AND PROCESS_DATE=? AND RUN_SL=? AND PROCESS_ID=?";
				} else {
					sqlQuery = "SELECT SL FROM SODPROCESSRUNDTL WHERE ENTITY_CODE=? AND PROCESS_DATE=? AND RUN_SL=? AND PROCESS_ID=?";
				}
				dbUtil.reset();
				dbUtil.setSql(sqlQuery);
				dbUtil.setString(1, entityCode);
				dbUtil.setDate(2, currentDate);
				dbUtil.setInt(3, runSerial);
				dbUtil.setString(4, processName);
				ResultSet rset = dbUtil.executeQuery();
				if (rset.next()) {
					processSerial = rset.getInt(1);
				}

				if (processType.equals(EOD_PROCESS)) {
					sqlQuery = "UPDATE EODPROCESSRUNDTL SET PROCESS_END_TIME=FN_GETCDT(?),PROCESS_STATUS=?,PROCESS_REMARKS=? WHERE ENTITY_CODE=? AND PROCESS_DATE=? AND RUN_SL=? AND SL=?";
				} else {
					sqlQuery = "UPDATE SODPROCESSRUNDTL SET PROCESS_END_TIME=FN_GETCDT(?),PROCESS_STATUS=?,PROCESS_REMARKS=? WHERE ENTITY_CODE=? AND PROCESS_DATE=? AND RUN_SL=? AND SL=?";
				}
				dbUtil.reset();
				dbUtil.setSql(sqlQuery);
				dbUtil.setString(1, entityCode);
				dbUtil.setString(2, status);
				dbUtil.setString(3, errorCode);
				dbUtil.setString(4, entityCode);
				dbUtil.setDate(5, currentDate);
				dbUtil.setInt(6, runSerial);
				dbUtil.setInt(7, processSerial);
				dbUtil.executeUpdate();
			}
			logger.logDebug("initiateProcess() End");
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError(e.getLocalizedMessage());
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			dbContext.close();
		}
	}
}