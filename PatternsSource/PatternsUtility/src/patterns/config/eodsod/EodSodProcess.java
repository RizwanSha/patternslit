package patterns.config.eodsod;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.LinkedHashMap;
import java.util.Map;

import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.loggers.ApplicationLogger;
import patterns.config.framework.thread.ApplicationContext;
import patterns.config.framework.web.FormatUtils;

public class EodSodProcess {
	private ApplicationLogger logger = null;
	private ApplicationContext context = ApplicationContext.getInstance();
	private DBContext dbContext = null;

	public static final String EOD_PROCESS = "E";
	public static final String SOD_PROCESS = "S";
	private Date currentDate;
	private String entityCode;

	public DBContext getDbContext() {
		return dbContext;
	}

	public ApplicationContext getContext() {
		return context;
	}

	public EodSodProcess(String entityCode) {
		logger = ApplicationLogger.getInstance(this.getClass().getSimpleName());
		this.dbContext = new DBContext();
		this.entityCode = entityCode;
	}

	public EodSodProcess(String entityCode, Date currentDate) {
		logger = ApplicationLogger.getInstance(this.getClass().getSimpleName());
		this.dbContext = new DBContext();
		this.currentDate = currentDate;
		this.entityCode = entityCode;
	}

	public EodSodProcess(DBContext dbContext, Date currentDate) {
		logger = ApplicationLogger.getInstance(this.getClass().getSimpleName());
		this.dbContext = dbContext;
		this.currentDate = currentDate;
		this.entityCode = context.getEntityCode();
	}

	public void updateMainContEod(boolean isProcessingStart, boolean isSuccess, String remarks) throws TBAFrameworkException {
		logger.logDebug("updateMainContEod() Begin");
		DBUtil dbUtil = getDbContext().createUtilInstance();
		try {
			String sqlQuery = RegularConstants.EMPTY_STRING;
			if (isProcessingStart) {
				sqlQuery = "UPDATE MAINCONT SET EOD_IN_PROGRESS='1',REMARKS=? WHERE ENTITY_CODE=?";
				dbUtil.reset();
				dbUtil.setSql(sqlQuery);
				dbUtil.setString(1, remarks);
				dbUtil.setString(2, entityCode);
			} else {
				if (isSuccess) {
					sqlQuery = "UPDATE MAINCONT SET EOD_IN_PROGRESS='0',EOD_COMPL='1',SOD_COMPL='0' WHERE ENTITY_CODE=?";
				} else {
					sqlQuery = "UPDATE MAINCONT SET EOD_IN_PROGRESS='0',EOD_COMPL='0' WHERE ENTITY_CODE=?";
				}
				dbUtil.reset();
				dbUtil.setSql(sqlQuery);
				dbUtil.setString(1, entityCode);
			}
			dbUtil.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError(e.getLocalizedMessage());
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			dbUtil.reset();
		}
		logger.logDebug("updateMainContEod() End");
	}

	public void updateMainContSod(boolean isProcessingStart, boolean isSuccess, String remarks) throws TBAFrameworkException {
		logger.logDebug("updateMainContSod() Begin");
		DBUtil dbUtil = getDbContext().createUtilInstance();
		try {
			String sqlQuery = RegularConstants.EMPTY_STRING;
			if (isProcessingStart) {
				sqlQuery = "UPDATE MAINCONT SET SOD_IN_PROGRESS='1',REMARKS=? WHERE ENTITY_CODE=?";
				dbUtil.reset();
				dbUtil.setSql(sqlQuery);
				dbUtil.setString(1, remarks);
				dbUtil.setString(2, entityCode);
			} else {
				if (isSuccess) {
					sqlQuery = "UPDATE MAINCONT SET CURR_BUSINESS_DATE=?,PREV_BUSINESS_DATE=DATE_SUB(?,INTERVAL 1 DAY),SOD_IN_PROGRESS='0',SOD_COMPL='1',EOD_COMPL='0' WHERE ENTITY_CODE=?";
					dbUtil.reset();
					dbUtil.setSql(sqlQuery);
					dbUtil.setDate(1, currentDate);
					dbUtil.setDate(2, currentDate);
					dbUtil.setString(3, entityCode);
				} else {
					sqlQuery = "UPDATE MAINCONT SET CURR_BUSINESS_DATE=PREV_BUSINESS_DATE,PREV_BUSINESS_DATE=DATE_SUB(PREV_BUSINESS_DATE,INTERVAL 1 DAY),SOD_IN_PROGRESS='0',SOD_COMPL='0',EOD_COMPL='1' WHERE ENTITY_CODE=?";
					dbUtil.reset();
					dbUtil.setSql(sqlQuery);
					dbUtil.setString(1, entityCode);
				}
			}
			dbUtil.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError(e.getLocalizedMessage());
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			dbUtil.reset();
		}
		logger.logDebug("updateMainContSod() End");
	}

	public int getEodRunSerial() throws TBAFrameworkException {
		logger.logDebug("getEodRunSerial() Begin");
		DBUtil dbUtil = getDbContext().createUtilInstance();
		int runSerial = 0;
		try {
			String sqlQuery = "SELECT COUNT(1)+1 FROM EODPROCESSRUN WHERE ENTITY_CODE=? AND PROCESS_DATE=? FOR UPDATE";
			dbUtil.reset();
			dbUtil.setSql(sqlQuery);
			dbUtil.setString(1, entityCode);
			dbUtil.setDate(2, currentDate);
			ResultSet rset = dbUtil.executeQuery();
			if (rset.next()) {
				runSerial = rset.getInt(1);
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError(e.getLocalizedMessage());
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			dbUtil.reset();
		}
		logger.logDebug("getEodRunSerial() End");
		return runSerial;
	}

	public int getSodRunSerial() throws TBAFrameworkException {
		logger.logDebug("getSodRunSerial() Begin");
		DBUtil dbUtil = getDbContext().createUtilInstance();
		int runSerial = 0;
		try {
			String sqlQuery = "SELECT COUNT(1)+1 FROM SODPROCESSRUN WHERE ENTITY_CODE=? AND PROCESS_DATE=?";
			dbUtil.reset();
			dbUtil.setSql(sqlQuery);
			dbUtil.setString(1, entityCode);
			dbUtil.setDate(2, currentDate);
			ResultSet rset = dbUtil.executeQuery();
			if (rset.next()) {
				runSerial = rset.getInt(1);
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError(e.getLocalizedMessage());
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			dbUtil.reset();
		}
		logger.logDebug("getSodRunSerial() End");
		return runSerial;
	}

	public void updateEodPendingStatus(int runSerial) throws TBAFrameworkException {
		logger.logDebug("updateEodPendingStatus() Begin");
		DBUtil dbUtil = getDbContext().createUtilInstance();
		try {
			String sqlQuery = "INSERT INTO EODPROCESSRUN VALUES (?,?,?,FN_GETCDT(?),NULL,'P',NULL)";
			dbUtil.reset();
			dbUtil.setSql(sqlQuery);
			dbUtil.setString(1, entityCode);
			dbUtil.setDate(2, currentDate);
			dbUtil.setInt(3, runSerial);
			dbUtil.setString(4, entityCode);
			dbUtil.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError(e.getLocalizedMessage());
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			dbUtil.reset();
		}
		logger.logDebug("updateEodPendingStatus() End");
	}

	public void updateSodPendingStatus(int runSerial) throws TBAFrameworkException {
		logger.logDebug("updateSodPendingStatus() Begin");
		DBUtil dbUtil = getDbContext().createUtilInstance();
		try {
			String sqlQuery = "INSERT INTO SODPROCESSRUN VALUES (?,?,?,FN_GETCDT(?),NULL,'P',NULL)";
			dbUtil.reset();
			dbUtil.setSql(sqlQuery);
			dbUtil.setString(1, entityCode);
			dbUtil.setDate(2, currentDate);
			dbUtil.setInt(3, runSerial);
			dbUtil.setString(4, entityCode);
			dbUtil.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError(e.getLocalizedMessage());
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			dbUtil.reset();
		}
		logger.logDebug("updateSodPendingStatus() End");
	}

	public void updateEodStatus(int runSerial, String status) throws TBAFrameworkException {
		logger.logDebug("updateEodStatus() Begin");
		DBUtil dbUtil = getDbContext().createUtilInstance();
		try {
			String sqlQuery = "UPDATE EODPROCESSRUN SET END_TIME=FN_GETCDT(?),RUN_STATUS=? WHERE ENTITY_CODE=? AND PROCESS_DATE=? AND RUN_SL=?";
			dbUtil.reset();
			dbUtil.setSql(sqlQuery);
			dbUtil.setString(1, entityCode);
			dbUtil.setString(2, status);
			dbUtil.setString(3, entityCode);
			dbUtil.setDate(4, currentDate);
			dbUtil.setInt(5, runSerial);
			dbUtil.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError(e.getLocalizedMessage());
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			dbUtil.reset();
		}
		logger.logDebug("updateEodStatus() End");
	}

	public void updateSodStatus(int runSerial, String status) throws TBAFrameworkException {
		logger.logDebug("updateSodStatus() Begin");
		DBUtil dbUtil = getDbContext().createUtilInstance();
		try {
			String sqlQuery = "UPDATE SODPROCESSRUN SET END_TIME=FN_GETCDT(?),RUN_STATUS=? WHERE ENTITY_CODE=? AND PROCESS_DATE=? AND RUN_SL=?";
			dbUtil.reset();
			dbUtil.setSql(sqlQuery);
			dbUtil.setString(1, entityCode);
			dbUtil.setString(2, status);
			dbUtil.setString(3, entityCode);
			dbUtil.setDate(4, currentDate);
			dbUtil.setInt(5, runSerial);
			dbUtil.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError(e.getLocalizedMessage());
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			dbUtil.reset();
		}
		logger.logDebug("updateSodStatus() End");
	}

	public Map<String, ProcessInfoBean> readProcessInfo(String processFlag) throws TBAFrameworkException {
		logger.logDebug("readProcessInfo() Begin");
		DBUtil dbUtil = getDbContext().createUtilInstance();
		Map<String, ProcessInfoBean> processMap = new LinkedHashMap<String, ProcessInfoBean>();
		try {
			ProcessInfoBean processInfo = new ProcessInfoBean();
			String sqlQuery = "SELECT D.PROCESS_ID,D.PROCESS_STATE,P.PROCESS_ROUTINE,P.PROCESS_CLASS,P.PROCESS_ALLOW_EOD,P.PROCESS_ALLOW_SOD FROM EODSODPROCDTL D,EODSODPROCESSES P WHERE D.ENTITY_CODE=? AND D.PROCESS_FLAG=? AND D.PROCESS_ID=P.PROCESS_ID ORDER BY D.SL";
			dbUtil.reset();
			dbUtil.setSql(sqlQuery);
			dbUtil.setString(1, entityCode);
			dbUtil.setString(2, processFlag);
			ResultSet rset = dbUtil.executeQuery();
			while (rset.next()) {
				processInfo = new ProcessInfoBean();
				String processName = rset.getString(1);
				processInfo.setProcessName(processName);
				processInfo.setProcessState(rset.getString("PROCESS_STATE"));
				processInfo.setRoutineName(rset.getString("PROCESS_ROUTINE"));
				if (processInfo.getRoutineName() != null && !processInfo.getRoutineName().equals(RegularConstants.EMPTY_STRING)) {
					processInfo.setRoutine(true);
				}
				processInfo.setHandlerName(rset.getString("PROCESS_CLASS"));
				if (processInfo.getHandlerName() != null && !processInfo.getHandlerName().equals(RegularConstants.EMPTY_STRING)) {
					processInfo.setHandler(true);
				}
				processInfo.setAllowedEod(FormatUtils.decodeStringToBoolean(rset.getString("PROCESS_ALLOW_EOD")));
				processInfo.setAllowedSod(FormatUtils.decodeStringToBoolean(rset.getString("PROCESS_ALLOW_SOD")));
				processMap.put(processName, processInfo);
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError(e.getLocalizedMessage());
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			dbUtil.reset();
		}
		logger.logDebug("readProcessInfo() End");
		return processMap;
	}

	public boolean isProcessingRequired(String processFlag) throws TBAFrameworkException {
		logger.logDebug("readProcessInfo() Begin");
		DBUtil dbUtil = getDbContext().createUtilInstance();
		boolean processingRequired = false;
		try {
			String sqlQuery = "SELECT 1 FROM EODSODPROCDTL D,EODSODPROCESSES P WHERE D.ENTITY_CODE=? AND D.PROCESS_FLAG=? AND D.PROCESS_ID=P.PROCESS_ID";
			dbUtil.reset();
			dbUtil.setSql(sqlQuery);
			dbUtil.setString(1, entityCode);
			dbUtil.setString(2, processFlag);
			ResultSet rset = dbUtil.executeQuery();
			if (rset.next()) {
				processingRequired = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError(e.getLocalizedMessage());
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			dbUtil.reset();
		}
		logger.logDebug("readProcessInfo() End");
		return processingRequired;
	}

	public void close() {
		dbContext.close();
	}
}