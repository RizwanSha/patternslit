package patterns.config.framework.exception;

public class TBAFrameworkException extends Exception {

	private static final long serialVersionUID = 5773510055346284536L;
	private String strErrMessage = null; // Stores the Error Message

	/**
	 * Default Constructor Constructor for Exception class
	 */
	public TBAFrameworkException() {
		super();
	}

	/**
	 * 
	 * Overloaded Constructor
	 * 
	 * @param Error
	 *            Message
	 */
	public TBAFrameworkException(String errMessage) {
		super(errMessage);
		strErrMessage = errMessage;
	}

	/**
	 * Fetches the Error Message
	 * 
	 * @return String - Error Message
	 */

	public String getErrorMessage() {
		return strErrMessage;
	}

}
