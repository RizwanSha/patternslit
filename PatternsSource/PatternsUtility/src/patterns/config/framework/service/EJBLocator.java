package patterns.config.framework.service;

import com.patterns.framework.ejb.CommonDispatcherLocal;
import com.patterns.framework.ejb.ProcessDispatcherLocal;

public class EJBLocator {

	private static EJBLocator serviceLocator;

	static {
		serviceLocator = new EJBLocator();
	}

	private EJBLocator() {
	}

	static public EJBLocator getInstance() {
		return serviceLocator;
	}


	public CommonDispatcherLocal getCommonDipatcher(String jndiName) {
		return (CommonDispatcherLocal) ServiceLocator.getInstance().lookupObject(jndiName);
	}

	public ProcessDispatcherLocal getProcessDipatcher(String jndiName) {
		return (ProcessDispatcherLocal) ServiceLocator.getInstance().lookupObject(jndiName);
	}

}
