package patterns.config.framework.process;

import java.io.Serializable;

import patterns.config.framework.pki.ProcessTFAInfo;
import patterns.config.framework.service.DTObject;

public class TBAProcessInfo implements Serializable {

	private static final long serialVersionUID = -3707773454639955245L;
	private String processBO;
	private TBAProcessAction processAction;
	private DTObject processData;

	// added by swaroopa as on 26-04-2012 begins
	private ProcessTFAInfo processTFAInfo;

	public ProcessTFAInfo getProcessTFAInfo() {
		return processTFAInfo;
	}

	public void setProcessTFAInfo(ProcessTFAInfo processTFAInfo) {
		this.processTFAInfo = processTFAInfo;
	}

	// added by swaroopa as on 26-04-2012 ends

	public DTObject getProcessData() {
		return processData;
	}

	public void setProcessData(DTObject processData) {
		this.processData = processData;
	}

	public void setProcessAction(TBAProcessAction processAction) {
		this.processAction = processAction;
	}

	public TBAProcessAction getProcessAction() {
		return processAction;
	}

	public String getProcessBO() {
		return processBO;
	}

	public void setProcessBO(String processBO) {
		this.processBO = processBO;
	}
}