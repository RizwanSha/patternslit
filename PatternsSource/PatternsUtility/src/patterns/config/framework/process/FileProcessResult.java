package patterns.config.framework.process;

import java.io.Serializable;

import patterns.config.framework.service.DTObject;

public class FileProcessResult implements Serializable {

	private static final long serialVersionUID = -3707773454639955245L;

	public FileProcessStatus getProcessStatus() {
		return processStatus;
	}

	public void setProcessStatus(FileProcessStatus processStatus) {
		this.processStatus = processStatus;
	}

	public String getGeneratedID() {
		return generatedID;
	}

	public void setGeneratedID(String generatedID) {
		this.generatedID = generatedID;
	}

	public String getErrorCode() {
		return errorCode == null ? "" : errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	private FileProcessStatus processStatus;
	private String generatedID;
	private String errorCode;
	private String errorMessage;
	private DTObject responseDTO;

	public DTObject getResponseDTO() {
		return responseDTO;
	}

	public void setResponseDTO(DTObject responseDTO) {
		this.responseDTO = responseDTO;
	}
}
