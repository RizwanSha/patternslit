package patterns.config.framework.process;

import java.io.Serializable;
import java.util.Date;

public class TBAProcessAction implements Serializable {
	private static final long serialVersionUID = 5248525400859472003L;

	private String dateFormat;

	private TBAActionType actionType;

	private String actionUser;

	private String actionBranch;

	private String actionEntity;

	private String partitionNo;

	private String processID;

	private String actionCustomer;

	private Date actionDate;

	private String actionIP;

	private String actionRole;

	private String finYear;

	private String currentYear;
	// ARIBALA ADDED ON 09102015 BEGIN
	private boolean rectification;
	// ARIBALA ADDED ON 09102015 END

	private String clusterCode;

	public String getCurrentYear() {
		return currentYear;
	}

	public void setCurrentYear(String currentYear) {
		this.currentYear = currentYear;
	}

	public String getFinYear() {
		return finYear;
	}

	public void setFinYear(String finYear) {
		this.finYear = finYear;
	}

	public TBAActionType getActionType() {
		return actionType;
	}

	public void setActionType(TBAActionType actionType) {
		this.actionType = actionType;
	}

	public String getProcessID() {
		return processID;
	}

	public void setProcessID(String processID) {
		this.processID = processID;
	}

	public void setActionBranch(String actionBranch) {
		this.actionBranch = actionBranch;
	}

	public String getActionBranch() {
		return actionBranch;
	}

	public void setActionEntity(String actionEntity) {
		this.actionEntity = actionEntity;
	}

	public String getActionEntity() {
		return actionEntity;
	}

	public void setActionUser(String actionUser) {
		this.actionUser = actionUser;
	}

	public String getActionUser() {
		return actionUser;
	}

	public void setActionCustomer(String actionCustomer) {
		this.actionCustomer = actionCustomer;
	}

	public String getActionCustomer() {
		return actionCustomer;
	}

	public void setActionDate(Date actionDate) {
		this.actionDate = actionDate;
	}

	public Date getActionDate() {
		return actionDate;
	}

	public void setActionIP(String actionIP) {
		this.actionIP = actionIP;
	}

	public String getActionIP() {
		return actionIP;
	}

	public void setDateFormat(String dateFormat) {
		this.dateFormat = dateFormat;
	}

	public String getDateFormat() {
		return dateFormat;
	}

	public String getActionRole() {
		return actionRole;
	}

	public void setActionRole(String actionRole) {
		this.actionRole = actionRole;
	}

	public String getPartitionNo() {
		return partitionNo;
	}

	public void setPartitionNo(String partitionNo) {
		this.partitionNo = partitionNo;
	}

	// ARIBALA ADDED ON 09102015 BEGIN
	public boolean isRectification() {
		return rectification;
	}

	public void setRectification(boolean rectification) {
		this.rectification = rectification;
	}

	// ARIBALA ADDED ON 09102015 END
	public String getClusterCode() {
		return clusterCode;
	}

	public void setClusterCode(String clusterCode) {
		this.clusterCode = clusterCode;
	}
}
