package patterns.config.framework.process;

import java.io.Serializable;

import patterns.config.framework.service.DTObject;

public class TBAProcessResult implements Serializable {

	private static final long serialVersionUID = -3707773454639955245L;

	public TBAProcessStatus getProcessStatus() {
		return processStatus;
	}

	public void setProcessStatus(TBAProcessStatus processStatus) {
		this.processStatus = processStatus;
	}

	public String getGeneratedID() {
		return generatedID;
	}

	public void setGeneratedID(String generatedID) {
		this.generatedID = generatedID;
	}

	public String getPrimaryKey() {
		return primaryKey;
	}

	public void setPrimaryKey(String primaryKey) {
		this.primaryKey = primaryKey;
	}

	public String getSourceKey() {
		return sourceKey;
	}

	public void setSourceKey(String sourceKey) {
		this.sourceKey = sourceKey;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public String getErrorCode() {
		return errorCode == null ? "" : errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public Object getAdditionalInfo() {
		return additionalInfo == null ? "" : additionalInfo;
	}

	public void setAdditionalInfo(Object additionalInfo) {
		this.additionalInfo = additionalInfo;
	}

	private TBAProcessStatus processStatus;
	private String generatedID;
	private String primaryKey;
	private String sourceKey;
	private int version;
	private String errorCode;
	private String errorMessage;
	private Object additionalInfo;
	private DTObject responseDTO;

	public DTObject getResponseDTO() {
		return responseDTO;
	}

	public void setResponseDTO(DTObject responseDTO) {
		this.responseDTO = responseDTO;
	}

	private String mainTableName;
	private boolean financialOperation;
	private String financialOperationBO;

	public boolean isFinancialOperation() {
		return financialOperation;
	}

	public String getFinancialOperationBO() {
		return financialOperationBO;
	}

	public String getMainTableName() {
		return mainTableName;
	}

	public void setMainTableName(String mainTableName) {
		this.mainTableName = mainTableName;
	}

	public void setFinancialOperation(boolean financialOperation) {
		this.financialOperation = financialOperation;
	}

	public void setFinancialOperationBO(String financialOperationBO) {
		this.financialOperationBO = financialOperationBO;
	}

}
