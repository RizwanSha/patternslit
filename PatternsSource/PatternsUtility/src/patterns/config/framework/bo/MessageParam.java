package patterns.config.framework.bo;

import java.io.Serializable;
import java.util.List;

public class MessageParam implements Serializable{

	private static final long serialVersionUID = 1L;
		private String code;
		public MessageParam(){
		}
		public MessageParam(String messagecode){
			this.code=messagecode;
		}
		public String getCode() {
			return code;
		}
		public void setCode(String messagecode) {
			this.code = messagecode;
		}
		private List<String>   parameters;

		public List<String> getParameters() {
			return parameters;
		}
		public void setParameters(List<String> parameters) {
			this.parameters = parameters;
		}
		public Object[] toParameterArray() {
			return this.parameters!=null?this.parameters.toArray():null;
		}
}
