package patterns.config.framework.bo;

import java.io.Serializable;
import java.util.List;

public class AdditionalDetailInfo implements Serializable{
	//AdditionalInfo
	private static final long serialVersionUID = 1L;
	private MessageParam heading;
	private List<MessageParam> key;
	private List<String> value;
    
	public AdditionalDetailInfo(){
	}
	
	public MessageParam getHeading() {
		return heading;
	}

	public void setHeading(MessageParam heading) {
		this.heading = heading;
	}

	 public List<MessageParam> getKey() {
			return key;
	}

	public void setKey(List<MessageParam> key) {
		this.key = key;
	}

	public List<String> getValue() {
		return value;
	}

	public void setValue(List<String> value) {
		this.value = value;
	}
	    
	public Object[] toParameterArray(List parameters) {
		return parameters!=null?parameters.toArray():null;
	}
}
