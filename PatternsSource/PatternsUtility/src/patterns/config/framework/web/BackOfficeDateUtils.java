package patterns.config.framework.web;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import patterns.config.framework.thread.ApplicationContext;

public class BackOfficeDateUtils {

	public static boolean isDateGreaterThan(String date1, String date2) {
		ApplicationContext context = ApplicationContext.getInstance();
		String dateFormat = context.getDateFormat();
		Date date1Instance = BackOfficeFormatUtils.getDate(date1, dateFormat);
		Date date2Instance = BackOfficeFormatUtils.getDate(date2, dateFormat);
		return isDateGreaterThan(date1Instance, date2Instance);
	}

	public static boolean isDateGreaterThan(Date date1, Date date2) {
		return date1.getTime() > date2.getTime();
	}

	public static boolean isDateGreaterThanEqual(String date1, String date2) {
		ApplicationContext context = ApplicationContext.getInstance();
		String dateFormat = context.getDateFormat();
		Date date1Instance = BackOfficeFormatUtils.getDate(date1, dateFormat);
		Date date2Instance = BackOfficeFormatUtils.getDate(date2, dateFormat);
		return isDateGreaterThanEqual(date1Instance, date2Instance);
	}

	public static boolean isDateGreaterThanEqual(Date date1, Date date2) {
		return date1.getTime() >= date2.getTime();
	}

	public static boolean isDateLesserThan(String date1, String date2) {
		ApplicationContext context = ApplicationContext.getInstance();
		String dateFormat = context.getDateFormat();
		Date date1Instance = BackOfficeFormatUtils.getDate(date1, dateFormat);
		Date date2Instance = BackOfficeFormatUtils.getDate(date2, dateFormat);
		return isDateLesserThan(date1Instance, date2Instance);
	}

	public static boolean isDateLesserThan(Date date1, Date date2) {
		return date1.getTime() < date2.getTime();
	}

	public static boolean isDateLesserThanEqual(String date1, String date2) {
		ApplicationContext context = ApplicationContext.getInstance();
		String dateFormat = context.getDateFormat();
		Date date1Instance = BackOfficeFormatUtils.getDate(date1, dateFormat);
		Date date2Instance = BackOfficeFormatUtils.getDate(date2, dateFormat);
		return isDateLesserThanEqual(date1Instance, date2Instance);
	}

	public static boolean isDateLesserThanEqual(Date date1, Date date2) {
		return date1.getTime() <= date2.getTime();
	}

	public static boolean isDateEqual(String date1, String date2) {
		ApplicationContext context = ApplicationContext.getInstance();
		String dateFormat = context.getDateFormat();
		Date date1Instance = BackOfficeFormatUtils.getDate(date1, dateFormat);
		Date date2Instance = BackOfficeFormatUtils.getDate(date2, dateFormat);
		return isDateEqual(date1Instance, date2Instance);
	}

	public static boolean isDateEqual(Date date1, Date date2) {
		return date1.getTime() == date2.getTime();
	}
	
	public static long getNoOfDays(Date date1, Date date2) {
		long diffDate = date1.getTime() - date2.getTime();
		long noOfDays = diffDate / (24 * 60 * 60 * 1000);
		//long noOfDays =TimeUnit.MILLISECONDS.toDays(diffDate);
		return noOfDays;
	}
}
