package patterns.config.framework.web.ajax;

import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.thread.ApplicationContext;
import patterns.config.utils.BaseUtility;

public abstract class AJAXInterceptor {

	private ApplicationContext context = ApplicationContext.getInstance();
	private boolean internalAdministrator;
	private boolean internalOperations;
	private boolean customerAdministrator;
	public static final String GQM_PRIMARY_KEY = "_GQMPrimaryKey";

	public ApplicationContext getContext() {
		return context;
	}

	public AJAXInterceptor() {
		setInternalAdministrator(context.isInternalAdministrator());
		setInternalOperations(context.isInternalOperations());
		setCustomerAdministrator(context.isCustomerAdministrator());
	}

	public abstract DTObject processLookupRequest(DTObject input);

	public DTObject resolveLookupRequest(DTObject input) {
		return input;
	}

	public abstract DTObject processRecordFinderRequest(DTObject input);

	public DTObject resolveRecordFinderRequest(DTObject input) {
		return input;
	}

	public DTObject resolveGridQueryRequest(DTObject input) {
		DBContext dbContext = new DBContext();
		String arguments = input.get(ContentManager.ARGUMENT_KEY);
		String programID = input.get(ContentManager.PROGRAM_KEY);
		try {
			String bucketVlaue = BaseUtility.resolvePartitionBucketID(programID, arguments, dbContext);
			input.set("BUCKET_VALUE", bucketVlaue);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			dbContext.close();
		}
		return input;
	}

	public DTObject resolveMtmRequest(DTObject input) {
		DBContext dbContext = new DBContext();
		String arguments = input.get(ContentManager.ARGUMENT_KEY);
		String programID = input.get(ContentManager.PROGRAM_KEY);
		try {
			String bucketVlaue = BaseUtility.resolvePartitionBucketID(programID, arguments, dbContext);
			input.set("BUCKET_VALUE", bucketVlaue);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			dbContext.close();
		}
		return input;
	}

	public void setInternalAdministrator(boolean internalAdministrator) {
		this.internalAdministrator = internalAdministrator;
	}

	public boolean isInternalAdministrator() {
		return internalAdministrator;
	}

	public void setInternalOperations(boolean internalOperations) {
		this.internalOperations = internalOperations;
	}

	public boolean isInternalOperations() {
		return internalOperations;
	}

	public void setCustomerAdministrator(boolean customerAdministrator) {
		this.customerAdministrator = customerAdministrator;
	}

	public boolean isCustomerAdministrator() {
		return customerAdministrator;
	}

	public String resolveParitionYear(String programId, String sourceKey) {
		DBContext dbContext = new DBContext();
		String partitionYear = context.getFinYear();
		try {
			partitionYear = BaseUtility.resolvePartitionBucketID(programId, sourceKey, dbContext);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			dbContext.close();
		}
		return partitionYear;
	}
}