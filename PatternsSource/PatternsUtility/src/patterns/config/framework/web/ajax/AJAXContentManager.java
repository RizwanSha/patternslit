package patterns.config.framework.web.ajax;

import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.service.ServiceLocator;
import patterns.config.framework.thread.ApplicationContext;
import patterns.config.framework.web.BackOfficeFormatUtils;

public abstract class AJAXContentManager extends ContentManager {

	public abstract DTObject getData(DTObject input);

	private ApplicationContext context = ApplicationContext.getInstance();

	public final ApplicationContext getContext() {
		return context;
	}

	public final String preProcessSQL(String sqlQuery) {
		sqlQuery = sqlQuery.replaceAll(SESSION_USER_ID, context.getUserID());
		sqlQuery = sqlQuery.replaceAll(SESSION_PARTITION_NO, context.getPartitionNo());
		sqlQuery = sqlQuery.replaceAll(SESSION_ENTITY_CODE, context.getEntityCode());
		sqlQuery = sqlQuery.replaceAll(SESSION_CUSTOMER_CODE, context.getCustomerCode());
		sqlQuery = sqlQuery.replaceAll(SESSION_DATE_FORMAT, context.getDateFormat());
		sqlQuery = sqlQuery.replaceAll(SESSION_DATETIME_FORMAT, context.getDateFormat() + " " + RegularConstants.TIME_FORMAT);
		sqlQuery = sqlQuery.replaceAll(SESSION_CBD, BackOfficeFormatUtils.getDate(context.getCurrentBusinessDate(), context.getDateFormat()));
		sqlQuery = sqlQuery.replaceAll(SESSION_TBA_FORMAT, BackOfficeConstants.DB_TBA_DATE_FORMAT);
		sqlQuery = sqlQuery.replaceAll(SESSION_BRANCH_CODE, context.getBranchCode());
		sqlQuery = sqlQuery.replaceAll(SESSION_ROLE_CODE, context.getRoleCode());
		sqlQuery = sqlQuery.replaceAll(SESSION_ROLE_TYPE, context.getRoleType());
		sqlQuery = sqlQuery.replaceAll(SESSION_CLUSTER_CODE, context.getClusterCode());
		sqlQuery = sqlQuery.replaceAll(SESSION_FIN_YEAR, context.getFinYear());
		return sqlQuery;
	}

	public final DTObject processFilter(InterceptorPurpose requestType, String interceptorName, DTObject input) {
		DTObject result = new DTObject();
		try {
			ServiceLocator locator = ServiceLocator.getInstance();
			AJAXInterceptor filterInstance = (AJAXInterceptor) locator.getInterceptor(interceptorName).newInstance();
			switch (requestType) {
			case LOOKUP_PROCESS_RESOLVE:
				result = filterInstance.processLookupRequest(input);
				break;
			case LOOKUP_ARGUMENTS_RESOLVE:
				result = filterInstance.resolveLookupRequest(input);
				break;
			case RECORDFINDER_PROCESS_RESOLVE:
				result = filterInstance.processRecordFinderRequest(input);
				break;
			case RECORDFINDER_ARGUMENTS_RESOLVE:
				result = filterInstance.resolveRecordFinderRequest(input);
				break;
			case MTM_ARGUMENTS_RESOLVE:
				result = filterInstance.resolveMtmRequest(input);
				break;
			case GRIDQUERY_ARGUMENTS_RESOLVE:
				result = filterInstance.resolveGridQueryRequest(input);
				break;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
}