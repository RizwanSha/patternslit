package patterns.config.framework.web;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.thread.ApplicationContext;

public class BackOfficeFormatUtils {
	private static String JAVA_DATE_FORMAT = "dd-MM-yyyy";
	private static String JAVA_DATETIME_FORMAT = "dd-MM-yyyy HH:mm:ss";
	private static String JAVA_DATETIME_SHORT_FORMAT = "dd-MM-yyyy HH:mm";
	private static String JAVA_REV_DATE_FORMAT = "yyyy-MM-dd";

	private static String convertSimpleFormat(String format) {
		if (format.equals("dd/MM/yyyy"))
			return JAVA_DATE_FORMAT;
		else if (format.equals("dd-MM-yyyy"))
			return JAVA_DATE_FORMAT;
		else if (format.equals("dd-MM-yyyy HH24:MI:SS"))
			return JAVA_DATETIME_FORMAT;
		else if (format.equals("dd/MM/yyyy HH24:MI:SS"))
			return JAVA_DATETIME_FORMAT;
		else if (format.equals("%d/%m/%Y"))
			return JAVA_DATE_FORMAT;
		else if (format.equals("%d-%m-%Y"))
			return JAVA_DATE_FORMAT;
		else if (format.equals("%Y-%m-%d"))
			return JAVA_REV_DATE_FORMAT;
		else if (format.equals("%d-%m-%Y %H:%i:%s"))
			return JAVA_DATETIME_FORMAT;
		else if (format.equals("%d-%m-%Y %H:%i"))
			return JAVA_DATETIME_SHORT_FORMAT;
		else
			return format;
	}

	public static String getDate(String date, String originalFormat, String targetFormat) {
		String result = null;
		if (date == null)
			return null;
		targetFormat = convertSimpleFormat(targetFormat);
		SimpleDateFormat sdf = new SimpleDateFormat(originalFormat);
		try {
			Date dateInstance = sdf.parse(date);
			sdf = null;
			sdf = new SimpleDateFormat(targetFormat);
			result = sdf.format(dateInstance);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return result;
	}

	public static String getDate(Date date, String format) {
		if (date == null)
			return null;
		format = convertSimpleFormat(format);
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		String result = null;
		result = sdf.format(date);
		return result;
	}

	public static String translateTBAtoSessionFormat(String date) {
		if (date == null)
			return null;
		SimpleDateFormat sdf = new SimpleDateFormat(BackOfficeConstants.TBA_DATE_FORMAT);
		String result = null;
		try {
			Date dateInstance = sdf.parse(date);
			sdf = null;
			sdf = new SimpleDateFormat(ApplicationContext.getInstance().getDateFormat());
			result = sdf.format(dateInstance);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return result;
	}

	public static String translateTBAtoSessionTimestampFormat(String date) {
		if (date == null)
			return null;
		SimpleDateFormat sdf = new SimpleDateFormat(BackOfficeConstants.TBA_DATE_FORMAT + " HH:mm:ss");
		String result = null;
		try {
			Date dateInstance = sdf.parse(date);
			sdf = null;
			sdf = new SimpleDateFormat(ApplicationContext.getInstance().getDateFormat() + " HH:mm:ss");
			result = sdf.format(dateInstance);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return result;
	}

	public static String[] splitSourceKey(String sourceKey) {
		String strsrcKey[] = null;
		if (sourceKey.trim().startsWith("|"))
			sourceKey = " " + sourceKey;
		if (sourceKey.trim().endsWith("|"))
			sourceKey = sourceKey + " ";
		strsrcKey = sourceKey.split("\\|");
		return strsrcKey;
	}

	public static Date getDate(String date, String format) {
		if (date == null)
			return null;
		format = convertSimpleFormat(format);
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		Date result = null;
		try {
			result = sdf.parse(date);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public static String getSimpleDateFormat(String format) {

		if (format.equals("dd/MM/yyyy"))
			return "dd-MM-yyyy";
		if (format.equals("dd-MM-yyyy"))
			return "dd-MM-yyyy";
		else if (format.equals("%d/%m/%Y"))
			return "dd-MM-yyyy";
		else if (format.equals("%d-%m-%Y"))
			return "dd-MM-yyyy";
		else
			return convertSimpleFormat(format);
	}

	public static int getYear(String date, String format) {
		if (date == null)
			return 0;
		format = convertSimpleFormat(format);
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		int result = 0;
		try {
			Date baseDate = sdf.parse(date);
			Calendar cal = Calendar.getInstance();
			cal.setTime(baseDate);
			result = cal.get(Calendar.YEAR);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public static void main(String args[]) {
		System.out.println(BackOfficeFormatUtils.getDate("2014-05-05", "yyyy-MM-dd"));
	}

}
