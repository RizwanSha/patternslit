package patterns.config.framework.web;

public class RequestConstants {

	public static final String AJAX_TOKEN = "AJAX_TOKEN";
	public static final String SUCCESS_FLAG = "_SF";
	public static final String SUCCESS_FLAG_SUCCESS = "1";
	public static final String SUCCESS_FLAG_FAILURE = "0";

	public static final String RESULT_CODE = "_RC";
	public static final String ADDITIONAL_INFO = "_AI";
	public static final String REDIRECT_LINK = "_RL";
	public static final String CURRENT_OPERATION = "_CO";
	public static final String CSRF_CONSTANT = "_CSRF";
	public static final String GENERATED_SL = "_GL";
	public static final String ADDITIONAL_DETAILS = "_AD";
	public static final String ADDITIONAL_DETAIL_INFO="XML_ADI";

	public static final String PROCESS_DESCN = "_PDESCN";
	public static final String PROCESS_ID = "_PID";
	public static final String PROCESS_ID_LOWER = "_PIDL";
	public static final String ERRORS_PRESENT = "show_errors";
	public static final String FORM_DTO = "FORM_DTO";

	public static final String PROCESS_CLASS = "_PIDCL";

	public static final String RENDERER_TYPE = "RENDERER_TYPE";
	public static final String RENDERER_TYPE_FORM = "1";
	public static final String RENDERER_TYPE_VIEW = "2";
	public static final String RENDERER_TYPE_CUSTOM = "3";
	public static final String RENDERER_TYPE_UTILITY = "4";
	public static final String RENDERER_TYPE_FORM_FULLSCREEEN = "5";

	public static final String APPLICATION_PATH = "/PortalWeb";

	public static final String FILE_PURPOSE_PARAMETER = "filePurpose";
	public static final String FILE_DESCN_PARAMETER = "fileDescn";
	public static final String FILE_ALTEXT_PARAMETER = "fileAltText";

	public static final String ADD_ALLOWED = "ADD_ALLOWED";
	public static final String MODIFY_ALLOWED = "MODIFY_ALLOWED";
	public static final String VIEW_ALLOWED = "VIEW_ALLOWED";
	public static final String TEMPLATE_ALLOWED = "TEMPLATE_ALLOWED";
	public static final String ERROR_MAP = "ERROR_MAP";

	private RequestConstants() {
	}
}