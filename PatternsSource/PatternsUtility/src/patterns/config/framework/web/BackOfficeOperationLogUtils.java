package patterns.config.framework.web;

import java.sql.Timestamp;

import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.thread.ApplicationContext;

public class BackOfficeOperationLogUtils {

	private ApplicationContext context = ApplicationContext.getInstance();

	public void logOperation() {
		DBContext dbContext = new DBContext();
		String entityCode = context.getEntityCode();
		String branchCode = context.getBranchCode();
		String userID = context.getUserID();
		String processID = context.getProcessID();
		String clientIP = context.getClientIP();
		String roleCode = context.getRoleCode();
		Timestamp loginDateTime = context.getLoginDateTime();
		try {
			dbContext.setAutoCommit(false);
			DBUtil util = dbContext.createUtilInstance();
			util.setMode(DBUtil.CALLABLE);
			util.setSql("CALL SP_AUDIT_LOG_OPERATION(?,?,?,?,?,?)");
			util.setString(1, entityCode);
			util.setString(2, userID);
			util.setString(3, processID);
			util.setString(4, clientIP);
			util.setString(5, roleCode);
			util.setTimestamp(6, loginDateTime);
			util.execute();
			util.reset();
			dbContext.commit();
		} catch (Exception e) {
			e.printStackTrace();
			try {
				dbContext.rollback();
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		} finally {
			dbContext.close();
		}
	}
}
