package patterns.config.framework.web;

public class SessionConstants {

	public static final String APPLICATION_CONTEXT = "_APP_CTX";
	public static final String CLIENT_IP = "_CIP";
	public static final String USER_AGENT = "_UA";
	public static final String SECURE_SITE = "_SS";
	public static final String CBD = "_CBD";
	public static final String CBD_STRING = "_CBDS";
	public static final String ENTITY_CODE = "_EC";
	public static final String USER_ID = "_UID";
	public static final String BASE_CURRENCY = "_BC";
	public static final String USER_NAME = "_UN";
	public static final String LAST_LOGIN_DATE_TIME = "_LLDT";
	public static final String LOGIN_DATE_TIME = "_LDT";
	public static final String SERVER_DATE_TIME = "_SDT";
	public static final String LOCALE = "_L";
	public static final String DEFAULT_DATE_FORMAT = "dd/MM/yyyy";
	public static final String DATE_FORMAT = "_DF";
	public static final String CUSTOMER_CODE = "_CC";
	public static final String CUSTOMER_NAME = "_CN";
	public static final String CURRENCY_UNITS = "_CU";
	public static final String PARTITION_NO = "_PN";

	public static final String EMPCODE_TYPE = "_ECT";
	public static final String EMPCODE_SIZE = "_ECS";
	public static final String REG_NO_YR_PREFIX_REQD = "_RYPR";

	public static final String LOGIN_AUTHENTICATION_CONTEXT = "_LUC";
	public static final String PASSWORD_RESET = "_PRS";

	public static final String RANDOM_SALT = "RANDOM_SALT";
	public static final String PASSWORD_SALT = "PASSWORD_SALT";
	public static final String LOGIN_TFA_REQ = "_LTFA";
	public static final String TFA_REQ = "_TFA";
	public static final String CERT_INV_NUM = "_CINV";
	public static final String MULTI_SESSION_CHECK = "MULTI_SESSION_CHECK";
	public static final int MAX_LOGIN_INACTIVE_INTERVAL_SECONDS = 1260;
	public static final String TEMP_SESSION_START = "TEMP_SESSION_START";

	public static final String ROLE_TYPE = "_RT";
	public static final String ROLE_CODE = "_RC";
	public static final String ROLE_LIST = "_RL";
	public static final String BRANCH_CODE = "_BRC";
	public static final String BRANCH_NAME = "_BRN";
	public static final String CONSOLE_CODE = "_CCODE";
	public static final String CONSOLE_LIST = "CONSOLE_LIST";
	public static final String NON_CUSTOMER = "0";

	// added by swaroopa as on 16-05-2012 begins
	public static final String TFA_NONCE = "_TFAN";
	public static final String DEPLOYMENT_CONTEXT = "_DPC";

	// added by swaroopa as on 16-05-2012 ends

	public static final String ORG_NAME = "_ON";
	public static final String CURR_YEAR = "_CY";
	public static final String FIN_YEAR = "_FY";
	public static final String START_FIN_MONTH = "_SFM";
// added by swaroopa as on 27/07/2016 begins
	public static final String CLUSTER_CODE = "_CLC";
	// added by swaroopa as on 27/07/2016 ends
	
	

	private SessionConstants() {
	}
}