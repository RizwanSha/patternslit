package patterns.config.framework.web;


public class NumberToWords {
	static public boolean HelperConvertNumberToText(int num, String[] result) {
		String[] strones = { "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen", };

		String[] strtens = { "Ten", "Twenty", "Thirty", "Fourty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety", "Hundred" };

		result[0] = "";
		int single, tens, hundreds;

		if (num > 1000)
			return false;

		hundreds = num / 100;
		num = num - hundreds * 100;
		if (num < 20) {
			tens = 0; // special case
			single = num;
		} else {
			tens = num / 10;
			num = num - tens * 10;
			single = num;
		}

		if (hundreds > 0) {
			result[0] += strones[hundreds - 1];
			result[0] += " Hundred ";
		}
		if (tens > 0) {
			result[0] += strtens[tens - 1];
			result[0] += " ";
		}
		if (single > 0) {
			result[0] += strones[single - 1];
			result[0] += " ";
		}
		return true;
	}

	static public boolean ConvertNumberToText(int num, String[] result) {
		String tempString[] = new String[1];
		tempString[0] = "";
		int thousands;
		int temp;
		int lakhs;
		result[0] = "";
		if (num < 0 || num > 10000000) {
			System.out.println(num + " \tNot Supported");
			return false;
		}

		if (num == 0) {
			System.out.println(num + " \tZero");
			return false;
		}

		if (num < 1000) {
			HelperConvertNumberToText(num, tempString);
			result[0] += tempString[0];
		} else if (num < 100000) {
			thousands = num / 1000;
			temp = num - thousands * 1000;
			HelperConvertNumberToText(thousands, tempString);
			result[0] += tempString[0];
			result[0] += "Thousand ";
			HelperConvertNumberToText(temp, tempString);
			result[0] += tempString[0];
		} else {
			lakhs = num / 100000;
			temp = num - lakhs * 100000;
			if (lakhs > 0) {
				HelperConvertNumberToText(lakhs, tempString);
				result[0] += tempString[0];
				result[0] += "Lakh ";
			}
			num = num - lakhs * 100000;
			thousands = num / 1000;
			temp = num - thousands * 1000;
			if (thousands > 0) {
				HelperConvertNumberToText(thousands, tempString);
				result[0] += tempString[0];
				result[0] += "Thousand ";
			}
			HelperConvertNumberToText(temp, tempString);
			result[0] += tempString[0];

		}
		return true;
	}
	
	static public boolean ConvertAmountToText(String amount,String units, String subUnits,String[] result) {
		String[] number = { null };
		String rupeeString[] = { null };
		String paisaString[] = { null };
		int integerPart = 0;
		int decimalPart = 0;
		if (amount.indexOf('.') != -1) {
			number = amount.split("\\.");
			if (!isEmpty(number[0])) {
				integerPart = Integer.parseInt(number[0]);
			}
			if (!isEmpty(number[1])) {
				if (number[1].length() > 2) {
					number[1] = number[1].substring(0, 2);
				}
				decimalPart = Integer.parseInt(number[1]);
			}
			if (integerPart > 0) {
				if (ConvertNumberToText(integerPart, rupeeString)) {
					result[0] += rupeeString[0];
				}
				result[0] += units;
			}

			if (decimalPart > 0) {
				if (integerPart > 0) {
					result[0] += " and ";
				}
				if (ConvertNumberToText(decimalPart, paisaString)) {
					result[0] += paisaString[0];
					result[0] += subUnits;
				}
			}
		}

		return true;
	}

	static public boolean isEmpty(String code) {
		if (code == null || code.trim().length() == 0)
			return true;
		return false;
	}
	
	public static void main(String[] args) {
		String[] result = new String[1];
		result[0] = "";
		String num = "9999.123";
		int[] arrNum = { -1, 0, 5, 10, 15, 19, 20, 21, 25, 33, 49, 50, 72, 99, 100, 101, 117, 199, 200, 214, 517, 589, 999, 1000, 1010, 1018, 1200, 9890, 10119, 13535, 57019, 99999, 100000, 100001 };
		// num = 99999;
		//if (ConvertAmountToText(num, result) == true)
			System.out.println(num + "\t" + result[0]);

	}
}
