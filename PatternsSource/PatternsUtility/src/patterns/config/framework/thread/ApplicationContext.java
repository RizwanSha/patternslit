package patterns.config.framework.thread;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Locale;

import patterns.config.framework.service.DTObject;

public abstract class ApplicationContext implements Serializable {

	private static final long serialVersionUID = -2828521186784559092L;

	public abstract String getPartitionNo();

	public abstract String getEntityCode();

	public abstract String getBaseCurrencyUnits();

	public abstract String getDeploymentContext();

	public abstract String getUserID();

	public abstract String getCustomerCode();

	public abstract String getBaseCurrency();

	public abstract String getDateFormat();

	public abstract Date getCurrentBusinessDate();

	public abstract String getClientIP();

	public abstract String getUserAgent();

	public abstract String getProcessID();

	public abstract String getProcessDescription();

	public abstract Locale getLocale();

	public abstract Locale getDefaultLocale();

	public abstract boolean isSecureSite();

	public abstract boolean isFormPosted();

	public abstract boolean isOperationLogRequired();

	public abstract Timestamp getLoginDateTime();

	public abstract String getRoleCode();

	public abstract String getRoleType();

	public abstract boolean isInternalAdministrator();

	public abstract boolean isInternalOperations();

	public abstract boolean isCustomerAdministrator();

	public abstract boolean isAuthorizationRole();

	public abstract String getBranchCode();

	public abstract String getCurrentYear();

	public abstract String getFinYear();

	public abstract String getDigitalCertificateInventoryNumber();
	
	public abstract String getCallSource();
	// added by swaroopa as on 27/07/2017 begins
	public abstract String getClusterCode();
	// added by swaroopa as on 27/07/2017 ends

	private static final ThreadLocal<ApplicationContext> context = new ThreadLocal<ApplicationContext>();
	
	public abstract DTObject getMobileContextParam();

	public static ApplicationContext getInstance() {
		ApplicationContext c = context.get();
		return c;
	}

	public static void setInstance(ApplicationContext c) {
		context.remove();
		context.set(c);
	}

	public static void destroyInstance() {
		context.remove();
	}
}
