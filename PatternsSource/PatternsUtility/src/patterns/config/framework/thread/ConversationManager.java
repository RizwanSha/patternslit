package patterns.config.framework.thread;

import java.util.HashMap;
import java.util.Map;

public class ConversationManager {

	private static final ConversationManager instance = new ConversationManager();
	private Map<String, Object> localData = new HashMap<>();

	private ConversationManager() {

	}

	public static ConversationManager getInstance() {
		return instance;
	}

	public Object getData(String key) {
		return localData.get(key);
	}

	public void setData(String key, Object value) {
		localData.put(key, value);
	}

}