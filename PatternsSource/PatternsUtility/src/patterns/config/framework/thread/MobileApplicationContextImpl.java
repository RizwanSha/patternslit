/*
 * Copyright (c) 2014, 2015, Patterns Software Design Institute. All
 * rights reserved. Unauthorized copying of this file, via any medium is
 * strictly prohibited. PATTERNS SDI PROPRIETARY/CONFIDENTIAL. Use is
 * subject to license terms.
 */
package patterns.config.framework.thread;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.Locale;

import patterns.config.framework.service.DTObject;

/**
 * The Class MobileApplicationContextImpl provides default implementation for
 * Mobile Application related ApplicationContext
 */
public class MobileApplicationContextImpl extends ApplicationContext {

	private static final long serialVersionUID = 6726734467707741162L;

	private String entityCode;
	private String dateFormat;
	private Date currentBusinessDate;
	private Timestamp loginDateTime;
	private Locale locale;
	private String clientIP;
	private DTObject mobileContextParam;
	private String partitionNo;
	private String branchCode;
	private String userID;
	private String userName;
	private String clusterCode;
	private Timestamp lastLoginDateTime;
	private String finYear;
	private String roleCode;
	private String roleDescription;
	private String branchDescription;

	public String getBranchDescription() {
		return branchDescription;
	}

	public void setBranchDescription(String branchDescription) {
		this.branchDescription = branchDescription;
	}

	public String getRoleDescription() {
		return roleDescription;
	}

	public void setRoleDescription(String roleDescription) {
		this.roleDescription = roleDescription;
	}

	public String getClusterCode() {
		return clusterCode;
	}

	public void setClusterCode(String clusterCode) {
		this.clusterCode = clusterCode;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Timestamp getLastLoginDateTime() {
		return lastLoginDateTime;
	}

	public void setLastLoginDateTime(Timestamp lastLoginDateTime) {
		this.lastLoginDateTime = lastLoginDateTime;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public void setPartitionNo(String partitionNo) {
		this.partitionNo = partitionNo;
	}

	public void init() {
		ApplicationContext.setInstance(this);
	}

	@Override
	public String getEntityCode() {
		return entityCode;
	}

	public void setEntityCode(String entityCode) {
		this.entityCode = entityCode;
	}

	@Override
	public String getDateFormat() {
		return dateFormat;
	}

	public void setDateFormat(String dateFormat) {
		this.dateFormat = dateFormat;
	}

	@Override
	public Date getCurrentBusinessDate() {
		return currentBusinessDate;
	}

	public void setCurrentBusinessDate(Date currentBusinessDate) {
		this.currentBusinessDate = currentBusinessDate;
	}

	public void setClientIP(String clientIP) {
		this.clientIP = clientIP;
	}

	@Override
	public Timestamp getLoginDateTime() {
		return loginDateTime;
	}

	public void setLoginDateTime(Timestamp loginDateTime) {
		this.loginDateTime = loginDateTime;
	}

	@Override
	public Locale getLocale() {
		return locale;
	}

	public void setLocale(Locale locale) {
		this.locale = locale;
	}

	public void destroy() {
		ApplicationContext.destroyInstance();
	}

	@Override
	public String getUserID() {
		return userID;
	}

	@Override
	public String getBaseCurrency() {
		return null;
	}

	@Override
	public String getClientIP() {
		return clientIP;
	}

	@Override
	public String getUserAgent() {
		return null;
	}

	@Override
	public String getProcessID() {
		return null;
	}

	@Override
	public Locale getDefaultLocale() {
		return null;
	}

	@Override
	public boolean isSecureSite() {
		return false;
	}

	@Override
	public boolean isFormPosted() {
		return false;
	}

	@Override
	public boolean isOperationLogRequired() {
		return false;
	}

	@Override
	public String getRoleCode() {
		return roleCode;
	}

	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}

	@Override
	public String getRoleType() {
		return null;
	}

	@Override
	public boolean isInternalAdministrator() {
		return false;
	}

	@Override
	public boolean isInternalOperations() {
		return false;
	}

	@Override
	public boolean isAuthorizationRole() {
		return false;
	}

	@Override
	public String getBranchCode() {
		return branchCode;
	}

	@Override
	public DTObject getMobileContextParam() {
		return mobileContextParam;
	}

	public void setMobileContextParam(DTObject mobileContextParam) {
		this.mobileContextParam = mobileContextParam;
	}

	@Override
	public String getPartitionNo() {
		return partitionNo;
	}

	@Override
	public String getBaseCurrencyUnits() {
		return null;
	}

	@Override
	public String getDeploymentContext() {
		return null;
	}

	@Override
	public String getCustomerCode() {
		return null;
	}

	@Override
	public String getProcessDescription() {
		return null;
	}

	@Override
	public boolean isCustomerAdministrator() {
		return false;
	}

	@Override
	public String getCurrentYear() {
		return null;
	}

	public void setFinYear(String finYear) {
		this.finYear = finYear;
	}

	@Override
	public String getFinYear() {
		return finYear;
	}

	@Override
	public String getDigitalCertificateInventoryNumber() {
		return null;
	}

	@Override
	public String getCallSource() {
		return null;
	}
}
