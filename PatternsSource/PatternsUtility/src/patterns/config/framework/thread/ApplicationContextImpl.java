package patterns.config.framework.thread;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.Locale;

import patterns.config.framework.database.CM_LOVREC;
import patterns.config.framework.service.DTObject;

public class ApplicationContextImpl extends ApplicationContext {

	private static final long serialVersionUID = -9208968922016854405L;

	private String entityCode;
	private String partitionNo;
	private String deploymentContext;
	private String userID;
	private String roleCode;
	private String customerCode;
	private String roleType;
	private String baseCurrency;
	private String baseCurrencyUnits;
	private String dateFormat;
	private Date currentBusinessDate;
	private String clientIP;
	private String userAgent;
	private String processID;
	private String processDescription;
	private Locale locale;
	private Locale defaultLocale = Locale.ENGLISH;
	private String branchCode;
	private boolean secureSite;
	private boolean operationLogRequired;
	private boolean formPosted;
	private Timestamp loginDateTime;
	private String digitalCertificateInventoryNumber;
	private String currentYear;
	private String finYear;
	private String callSource;
	private DTObject mobileContextParam;
	// added by swaroopa as on 27/07/2017 begins
	private String clusterCode;

	public String getClusterCode() {
		return clusterCode;
	}

	public void setClusterCode(String clusterCode) {
		this.clusterCode = clusterCode;
	}

	// added by swaroopa ends

	public String getBaseCurrencyUnits() {
		return baseCurrencyUnits;
	}

	public void setBaseCurrencyUnits(String baseCurrencyUnits) {
		this.baseCurrencyUnits = baseCurrencyUnits;
	}

	public String getPartitionNo() {
		return partitionNo;
	}

	public void setPartitionNo(String partitionNo) {
		this.partitionNo = partitionNo;
	}

	public void setEntityCode(String entityCode) {
		this.entityCode = entityCode;
	}

	public void setDeploymentContext(String deploymentContext) {
		this.deploymentContext = deploymentContext;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public void setRoleType(String roleType) {
		this.roleType = roleType;
	}

	public void setBaseCurrency(String baseCurrency) {
		this.baseCurrency = baseCurrency;
	}

	public void setDateFormat(String dateFormat) {
		this.dateFormat = dateFormat;
	}

	public void setCurrentBusinessDate(Date currentBusinessDate) {
		this.currentBusinessDate = currentBusinessDate;
	}

	public void setClientIP(String clientIP) {
		this.clientIP = clientIP;
	}

	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}

	public void setProcessID(String processID) {
		this.processID = processID;
	}

	public void setProcessDescription(String processDescription) {
		this.processDescription = processDescription;
	}

	public void setSecureSite(boolean secureSite) {
		this.secureSite = secureSite;
	}

	public void setOperationLogRequired(boolean operationLogRequired) {
		this.operationLogRequired = operationLogRequired;
	}

	public void setLoginDateTime(Timestamp loginDateTime) {
		this.loginDateTime = loginDateTime;
	}

	public void setFormPosted(boolean formPosted) {
		this.formPosted = formPosted;
	}

	public String getEntityCode() {
		return entityCode;
	}

	public String getUserID() {
		return userID;
	}

	public String getDeploymentContext() {
		return deploymentContext;
	}

	public String getRoleCode() {
		return roleCode;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public String getRoleType() {
		return roleType;
	}

	public String getBaseCurrency() {
		return baseCurrency;
	}

	public String getDateFormat() {
		return dateFormat;
	}

	public Date getCurrentBusinessDate() {
		return currentBusinessDate;
	}

	public String getClientIP() {
		return clientIP;
	}

	public String getUserAgent() {
		return userAgent;
	}

	public String getProcessID() {
		return processID;
	}

	public String getProcessDescription() {
		return processDescription;
	}

	public Locale getLocale() {
		return locale;
	}

	public void setLocale(Locale locale) {
		this.locale = locale;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public Locale getDefaultLocale() {
		return defaultLocale;
	}

	public void setDefaultLocale(Locale defaultLocale) {
		this.defaultLocale = defaultLocale;
	}

	public String getDigitalCertificateInventoryNumber() {
		return digitalCertificateInventoryNumber;
	}

	public void setDigitalCertificateInventoryNumber(
			String digitalCertificateInventoryNumber) {
		this.digitalCertificateInventoryNumber = digitalCertificateInventoryNumber;
	}

	public boolean isCustomerAdministrator() {
		if (roleType.equals(CM_LOVREC.COMMON_ROLES_5)
				|| roleType.equals(CM_LOVREC.COMMON_ROLES_6)) {
			return true;
		}
		return false;
	}

	public boolean isInternalAdministrator() {
		if (roleType.equals(CM_LOVREC.COMMON_ROLES_1)
				|| roleType.equals(CM_LOVREC.COMMON_ROLES_2)) {
			return true;
		}
		return false;
	}

	public boolean isAuthorizationRole() {
		if (roleType != null
				&& (roleType.equals(CM_LOVREC.COMMON_ROLES_2)
						|| roleType.equals(CM_LOVREC.COMMON_ROLES_4) || roleType
							.equals(CM_LOVREC.COMMON_ROLES_6))) {
			return true;
		}
		return false;
	}

	public boolean isInternalOperations() {
		if (roleType.equals(CM_LOVREC.COMMON_ROLES_3)
				|| roleType.equals(CM_LOVREC.COMMON_ROLES_4)) {
			return true;
		}
		return false;

	}

	public boolean isSecureSite() {
		return secureSite;
	}

	public boolean isOperationLogRequired() {
		return operationLogRequired;
	}

	public Timestamp getLoginDateTime() {
		return loginDateTime;
	}

	public boolean isFormPosted() {
		return formPosted;
	}

	/*
	 * public void init() { ApplicationContext.setInstance(this); }
	 * 
	 * public void destroy() { ApplicationContext.destroyInstance(); }
	 */

	@Override
	public String getCurrentYear() {
		return currentYear;
	}

	public void setCurrentYear(String currentYear) {
		this.currentYear = currentYear;
	}

	public void setFinYear(String finYear) {
		this.finYear = finYear;
	}

	@Override
	public String getFinYear() {
		return finYear;
	}

	public String getCallSource() {
		return callSource;
	}

	public void setCallSource(String callSource) {
		this.callSource = callSource;
	}

	@Override
	public DTObject getMobileContextParam() {
		return mobileContextParam;
	}

	public void setMobileContextParam(DTObject mobileContextParam) {
		this.mobileContextParam = mobileContextParam;
	}

}
