package patterns.config.framework.thread;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.LinkedHashMap;

import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.pki.ProcessTFAInfo;
import patterns.config.framework.process.TBAProcessAction;
import patterns.config.framework.service.DTObject;

public abstract class TBAContext implements Serializable {

	public static final String ADD = "A";
	public static final String MODIFY = "M";

	private static final long serialVersionUID = -2828521186784559092L;

	public abstract String getProcessID();

	public abstract TBAProcessAction getProcessAction();

	public abstract DTObject getProcessData();

	public abstract String getTbaPrimaryKey();

	public abstract String getDisplayDetails();

	public abstract String getProgramType();

	public abstract void setDisplayDetails(String details);

	public abstract long getTbaSerial();

	public abstract void setTbaSerial(long tbaSerial);

	public abstract boolean isTbaRequired();

	public abstract boolean isLogRequired();

	public abstract boolean isFinancialOperation();

	public abstract boolean isAddLogRequired();

	public abstract String getMainTableName();

	public abstract boolean isAuthorizationByPassed();

	public abstract String getTransitChoice();

	public abstract void setTbaPrimarykey(String tbaPrimarykey);

	public abstract Date getTbaActionDate();

	public abstract Timestamp getSystemTime();

	public abstract Date getSystemDate();

	public abstract boolean isCodeUniformityRequired();

	public abstract void setCodeUniformityRequired(boolean codeUniformityRequired);

	public abstract String getActionEntity();

	public abstract void setActionEntity(String controlEntity);

	public abstract String getPartitionNo();

	public abstract void setPartitionNo(String partitionNo);

	public abstract void setTbaActionDate(Date tbaActionDate);

	public abstract String getUserAction();

	public abstract Timestamp getProcessActionDateTime();

	public abstract DBContext getDBContext();

	public abstract LinkedHashMap<String, String> getAuditMap();

	public abstract void setAuditMap(LinkedHashMap<String, String> auditMap);

	// added by swaroopa as on 26-04-2012 begins
	public abstract ProcessTFAInfo getProcessTFAInfo();

	// added by swaroopa as on 26-04-2012 ends
	private static final ThreadLocal<TBAContext> context = new ThreadLocal<TBAContext>();

	public static TBAContext getInstance() {
		TBAContext c = context.get();
		return c;
	}

	public static void destroyInstance() {
		context.remove();
	}

	protected static void setInstance(TBAContext c) {
		context.set(c);
	}

}