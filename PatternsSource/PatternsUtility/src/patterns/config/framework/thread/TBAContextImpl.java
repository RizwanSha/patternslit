package patterns.config.framework.thread;

import java.lang.reflect.Method;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.StringTokenizer;

import patterns.config.authorization.CommonTBAUpdate;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.pki.ProcessTFAInfo;
import patterns.config.framework.process.TBAActionType;
import patterns.config.framework.process.TBAProcessAction;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeDateUtils;
import patterns.config.framework.web.BackOfficeFormatUtils;

public class TBAContextImpl extends TBAContext {

	private static final long serialVersionUID = -9208968922016854405L;
	private LinkedHashMap<String, String> auditMap;
	private String processID;
	private DTObject processData;
	private TBAProcessAction processAction;
	private String displayDetails;
	private boolean tbaRequired;
	private boolean logRequired;
	private int V_LOCK_STATUS;
	private String V_LOCK_ERR_MSG;
	private boolean addLogRequired;
	private String mainTableName;
	private Date actionDate;
	private Timestamp processActionDateTime;
	private DTObject resultDTO = null;
	private DBContext dbContext;
	private String UserAction;
	private String tbaPrimarykey;
	private long tbaSerial;
	private Date tbaEntryDate = null;
	private long tba_sl;
	private String tbaKey = null;
	private String mpgmEfftDateReq;
	private boolean codeUniformityRequired;
	private String actionEntity;
	private String partitionNo;
	private String transitChoice;
	private String tbaRectifyKey;
	// added by swaroopa as on 11-07-2016 for finanical operations
	private boolean financialOperation;

	public boolean isFinancialOperation() {
		return financialOperation;
	}

	public void setFinancialOperation(boolean financialOperation) {
		this.financialOperation = financialOperation;
	}

	// end by swaroopa as on 11-07-2016 for finanical operations
	public String getTransitChoice() {
		return transitChoice;
	}

	public void setTransitChoice(String transitChoice) {
		this.transitChoice = transitChoice;
	}

	public String getPartitionNo() {
		return partitionNo;
	}

	public void setPartitionNo(String partitionNo) {
		this.partitionNo = partitionNo;
	}

	public String getActionEntity() {
		return actionEntity;
	}

	public void setActionEntity(String actionEntity) {
		this.actionEntity = actionEntity;
	}

	public boolean isCodeUniformityRequired() {
		return codeUniformityRequired;
	}

	public void setCodeUniformityRequired(boolean codeUniformityRequired) {
		this.codeUniformityRequired = codeUniformityRequired;
	}

	public String getMpgmEfftDateReq() {
		return mpgmEfftDateReq;
	}

	public void setMpgmEfftDateReq(String mpgmEfftDateReq) {
		this.mpgmEfftDateReq = mpgmEfftDateReq;
	}

	private String programType = null;
	// added by swaroopa as on 30-10-2013
	private long auditlogSl;
	private Timestamp auditlogDateTime;
	private boolean isAuthbypassed;

	public boolean isAuthorizationByPassed() {
		return isAuthbypassed;
	}

	public void setAuthbypassed(boolean isAuthbypassed) {
		this.isAuthbypassed = isAuthbypassed;
	}

	// added by swaroopa as on 26-04-2012 begins
	private ProcessTFAInfo processTFAInfo;

	public ProcessTFAInfo getProcessTFAInfo() {
		return processTFAInfo;
	}

	public void setProcessTFAInfo(ProcessTFAInfo processTFAInfo) {
		this.processTFAInfo = processTFAInfo;
	}

	// added by swaroopa as on 26-04-2012 ends

	public long getTbaSerial() {
		return tbaSerial;
	}

	public void setTbaSerial(long tbaSerial) {
		this.tbaSerial = tbaSerial;
	}

	public String getTbaPrimaryKey() {
		return tbaPrimarykey;
	}

	public void setTbaPrimarykey(String tbaPrimarykey) {
		this.tbaPrimarykey = tbaPrimarykey;
	}

	public DTObject getResultDTO() {
		return resultDTO;
	}

	public void setResultDTO(DTObject resultDTO) {
		this.resultDTO = resultDTO;
	}

	public void init() {
		TBAContext.setInstance(this);
		auditMap = new LinkedHashMap<String, String>();
		switch (processAction.getActionType()) {
		case ADD:
			setUserAction(ADD);
			break;
		case MODIFY:
			setUserAction(MODIFY);
			break;
		case DELETE:
			setUserAction(MODIFY);
			break;
		}
		setProcessActionDateTime(getSystemTime());
	}

	public void destroy() {
		if (auditMap != null)
			auditMap.clear();

		if (dbContext != null) {
			dbContext.close();
		}
		TBAContext.destroyInstance();
	}

	public boolean isTbaRequired() {
		return tbaRequired;
	}

	public void setTbaRequired(boolean tbaRequired) {
		this.tbaRequired = tbaRequired;
	}

	public boolean isAddLogRequired() {
		return addLogRequired;
	}

	public void setAddLogRequired(boolean addLogRequired) {
		this.addLogRequired = addLogRequired;
	}

	public String getMainTableName() {
		return mainTableName;
	}

	public void setMainTableName(String mainTableName) {
		this.mainTableName = mainTableName;
	}

	public Date getTbaActionDate() {
		return actionDate;
	}

	public void setTbaActionDate(Date actionDate) {
		this.actionDate = actionDate;
	}

	public DBContext getDBContext() {
		return dbContext;
	}

	public void setDBContext(DBContext dBContext) {
		dbContext = dBContext;
	}

	public String getProcessID() {
		return processID;
	}

	public void setProcessID(String processID) {
		this.processID = processID;
	}

	public DTObject getProcessData() {
		return processData;
	}

	public void setProcessData(DTObject processData) {
		this.processData = processData;
	}

	public TBAProcessAction getProcessAction() {
		return processAction;
	}

	public void setProcessAction(TBAProcessAction processAction) {
		this.processAction = processAction;
	}

	public void setLogRequired(boolean logRequired) {
		this.logRequired = logRequired;
	}

	public boolean isLogRequired() {
		return logRequired;
	}

	public Timestamp getProcessActionDateTime() {
		return processActionDateTime;
	}

	public void setProcessActionDateTime(Timestamp processActionDateTime) {
		this.processActionDateTime = processActionDateTime;
	}

	private DTObject performPGMCTLOperation(boolean after) throws TBAFrameworkException {
		DTObject resultDTO = null;
		String _className = "";
		DTObject dto = new DTObject();

		boolean recordExists = false;
		DBUtil dbutil = getDBContext().createUtilInstance();
		try {
			dbutil.reset();
			dbutil.setMode(DBUtil.PREPARED);
			dbutil.setSql("SELECT PGMCTL_CLASS_NAME FROM PGMCTL WHERE PGMCTL_PGM_ID = ?");
			dbutil.setString(1, processID);
			ResultSet rs = dbutil.executeQuery();
			if (rs.next()) {
				_className = rs.getString("PGMCTL_CLASS_NAME");
				recordExists = true;
			}
			dbutil.reset();
			if (recordExists) {
				resultDTO = new DTObject();
				dto.set("PARTITION_NO", getPartitionNo());
				dto.set("SOURCE_KEY", getTbaPrimaryKey());
				dto.set("TBA_MAIN_KEY", tbaEntryDate + "|" + tba_sl);
				dto.set("CLASS_NAME", _className);

				dto.set("USER_ID", getProcessAction().getActionUser());
				dto.setObject("CBD", getProcessAction().getActionDate());
				dto.set("CLUSTER_CODE", getProcessAction().getClusterCode());
				dto.set("FIN_YEAR", getProcessAction().getFinYear());
				dto.set("ACTION_TYPE", getProcessAction().getActionType().equals(TBAActionType.ADD) ? CommonTBAUpdate.ADD : CommonTBAUpdate.MODIFY);
				if (after) {
					dto.set(CommonTBAUpdate.PROCESS_STAGE, CommonTBAUpdate.AFTER);
				} else {
					dto.set(CommonTBAUpdate.PROCESS_STAGE, CommonTBAUpdate.BEFORE);
				}
				dto.set(CommonTBAUpdate.PROCESS_OPTION, CommonTBAUpdate.AUTHORIZE);
				dto.set("METHOD_NAME", RegularConstants.PROCESS_METHOD);
				resultDTO = pgmctlreflection(dto);

			}
		} catch (Exception e) {
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			dbutil.reset();
		}
		return resultDTO;

	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private DTObject pgmctlreflection(DTObject inputObject) throws TBAFrameworkException {
		DTObject resultDTO = null;
		Class qmClass = null;
		Object qmObject = null;
		Method getDataMethod = null;
		Class[] parameterTypes = new Class[] { DTObject.class, DBContext.class };
		Object[] params = new Object[] { inputObject, getDBContext() };
		try {
			qmClass = Class.forName(inputObject.get("CLASS_NAME"));
			qmObject = qmClass.newInstance();
			getDataMethod = qmClass.getMethod(inputObject.get("METHOD_NAME"), parameterTypes);
			resultDTO = (DTObject) getDataMethod.invoke(qmObject, params);
		} catch (Exception e) {
			throw new TBAFrameworkException(e.getLocalizedMessage());
		}
		return resultDTO;
	}

	public void startTransaction() throws TBAFrameworkException {
		setPartitionNo(processAction.getPartitionNo());
		setActionEntity(processAction.getActionEntity());
		acquireLock();
		checkAuthorizationRequired();
		// changed by swaroopa to audit the entry details for every action
		// irrespective of authorization enabled on 27-04-2015
		if ((logRequired) && (!tbaRequired)) {
			startAuditLogAction();
		}
		performTBAInitialization();
		if (!tbaRequired) {
			resultDTO = performPGMCTLOperation(false);
		}
		// checkControlEntity();
	}

	public void commitTransaction() throws TBAFrameworkException {
		// changed by swaroopa to audit the entry details for every action
		// irrespective of authorization enabled on 27-04-2015
		if ((logRequired) && (!tbaRequired)) {
			updateAuditLog();
		}
		if (!tbaRequired) {
			resultDTO = performPGMCTLOperation(true);
			checkEffectiveDateParam();
		}
		if (financialOperation) {
			updateFinancialTransactionQueue();
		}
		checkTbaauthqRow();
	}

	private void updateFinancialTransactionQueue() throws TBAFrameworkException {
		DBUtil dbutil = getDBContext().createUtilInstance();
		try {
			dbutil.reset();
			dbutil.setMode(DBUtil.PREPARED);
			dbutil.setSql("INSERT INTO FINPGMTRANPROCQ(ENTITY_CODE,PROGRAM_ID,SOURCE_KEY) VALUES(?,?,?)");
			dbutil.setString(1, processAction.getActionEntity());
			dbutil.setString(2, processAction.getProcessID());
			dbutil.setString(3, getTbaPrimaryKey());
			dbutil.executeUpdate();
		} catch (Exception e) {
			System.out.println();
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			dbutil.reset();
		}
	}

	private void acquireLock() throws TBAFrameworkException {
		DBUtil dbutil = getDBContext().createUtilInstance();
		try {
			dbutil.reset();
			dbutil.setMode(DBUtil.CALLABLE);
			dbutil.setSql("{CALL SP_ACQUIRE_LOCK( ?,?,?,?,?,?)}");
			dbutil.setString(1, processAction.getActionEntity());
			dbutil.setString(2, processAction.getProcessID());
			dbutil.registerOutParameter(3, Types.INTEGER);
			dbutil.registerOutParameter(4, Types.INTEGER);
			dbutil.registerOutParameter(5, Types.INTEGER);
			dbutil.registerOutParameter(6, Types.VARCHAR);
			dbutil.execute();
			int V_LOG_REQ = dbutil.getInt(3);
			int V_ADD_LOG_REQ = dbutil.getInt(4);
			V_LOCK_STATUS = dbutil.getInt(5);
			V_LOCK_ERR_MSG = dbutil.getString(6) == RegularConstants.NULL ? RegularConstants.EMPTY_STRING : dbutil.getString(6);
			if (V_LOG_REQ == 1)
				setLogRequired(true);
			else
				setLogRequired(false);
			if (V_ADD_LOG_REQ == 1)
				setAddLogRequired(true);
			else
				setAddLogRequired(false);

			if (V_LOCK_STATUS == 1)
				throw new TBAFrameworkException(V_LOCK_ERR_MSG);

		} catch (Exception e) {
			System.out.println();
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			dbutil.reset();
		}
	}

	private void checkAuthorizationRequired() throws TBAFrameworkException {
		DBUtil dbutil = getDBContext().createUtilInstance();
		try {
			dbutil.reset();
			dbutil.setMode(DBUtil.PREPARED);
			dbutil.setSql("SELECT M.MPGM_TABLE_NAME,C.MPGM_AUTH_REQD ,C.TBA_REQ,C.MPGM_TYPE,C.MPGM_EFFDATE_REQ,C.CODE_UNIFORMITY_REQD ,C.MPGM_AUTH_BYPASS,C.MPGM_TRANSIT_CHOICE,C.MPGM_OPERATION FROM MPGM M, MPGMCONFIG C WHERE M.MPGM_ID =? AND M.MPGM_ID = C.MPGM_ID");
			dbutil.setString(1, processAction.getProcessID());
			try {
				ResultSet rs = dbutil.executeQuery();
				if (rs.next()) {
					mainTableName = rs.getString(1);
					String authStatus = rs.getString(2);
					programType = rs.getString(4);
					String efftReq = rs.getString(5);

					if (efftReq != null && efftReq.equals(RegularConstants.COLUMN_ENABLE)) {
						setMpgmEfftDateReq(RegularConstants.COLUMN_ENABLE);
					} else {
						setMpgmEfftDateReq(RegularConstants.COLUMN_DISABLE);
					}
					String codeUniformityReq = rs.getString(6);
					if ((codeUniformityReq == RegularConstants.NULL || codeUniformityReq.equals("0")))
						setCodeUniformityRequired(false);
					else
						setCodeUniformityRequired(true);

					if ((authStatus == RegularConstants.NULL || authStatus.equals("0"))) {
						setTbaRequired(false);
					} else {
						setTbaRequired(true);
					}

					if (rs.getString("MPGM_AUTH_BYPASS") != null && rs.getString("MPGM_AUTH_BYPASS").equals(RegularConstants.COLUMN_ENABLE)) {
						setAuthbypassed(true);
					} else {
						setAuthbypassed(false);
					}

					if (rs.getString("MPGM_TRANSIT_CHOICE") != null && rs.getString("MPGM_TRANSIT_CHOICE").equals(RegularConstants.COLUMN_ENABLE)) {
						setTransitChoice(rs.getString("MPGM_TRANSIT_CHOICE"));
					}

					// add by swaroopa as on 11/07/2016 for financial operations
					// start
					if (rs.getString("MPGM_OPERATION") != null && rs.getString("MPGM_OPERATION").equals("F")) {
						setFinancialOperation(true);
					}
					// end
				} else {
					throw new TBAFrameworkException("MPGM Defintion not found");
				}
			} catch (SQLException e) {
				throw new TBAFrameworkException(e.getLocalizedMessage());
			}
		} catch (SQLException e) {
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			dbutil.reset();
		}
	}

	@SuppressWarnings("rawtypes")
	private void updateAuditLog() throws TBAFrameworkException {
		int auditLogDtlsl = 1;
		try {
			Map.Entry IndRec;
			LinkedHashMap<String, String> collectionMap = getAuditMap();
			if (collectionMap != null) {
				Iterator it = collectionMap.entrySet().iterator();
				while (it.hasNext()) {
					IndRec = ((Map.Entry) it.next());
					if (IndRec != null) {
						saveLoggingDetails(IndRec.getKey().toString(), IndRec.getValue().toString(), auditLogDtlsl);
						auditLogDtlsl = auditLogDtlsl + 1;
					}
				}
			}

		} catch (Exception e) {
			throw new TBAFrameworkException(e.getLocalizedMessage());
		}
	}

	private void saveLoggingDetails(String Key, String dataValue, int auditlogDtlSl) throws TBAFrameworkException {
		String V_TABLE_NAME = "";
		String V_KEY_VALUE = "";
		String V_PK_VALUE = "";
		String V_IMAGE_TYPE = "";
		DBUtil dbutil = getDBContext().createUtilInstance();
		try {
			StringTokenizer stnSQLcol = new StringTokenizer(Key, "@");
			if (stnSQLcol.hasMoreTokens())
				V_TABLE_NAME = stnSQLcol.nextToken();
			if (stnSQLcol.hasMoreTokens())
				V_KEY_VALUE = stnSQLcol.nextToken();
			V_IMAGE_TYPE = V_KEY_VALUE.substring(0, 1);
			V_PK_VALUE = V_KEY_VALUE.substring(1);
			dbutil.reset();
			dbutil.setMode(DBUtil.CALLABLE);

			dbutil.setSql("{CALL SP_AUDITLOG_END_LOGACTION( ?,?,?,?,?,?,?,?,?,?)}");
			dbutil.setString(1, processAction.getActionEntity());
			dbutil.setString(2, processAction.getProcessID());
			dbutil.setString(3, V_PK_VALUE);
			dbutil.setTimestamp(4, auditlogDateTime);
			dbutil.setLong(5, auditlogSl);
			dbutil.setLong(6, auditlogDtlSl);
			dbutil.setString(7, V_TABLE_NAME);
			dbutil.setString(8, V_IMAGE_TYPE);
			dbutil.setString(9, RegularConstants.EMPTY_STRING);
			dbutil.setString(10, dataValue);

			dbutil.execute();
			dbutil.reset();
		} catch (SQLException e) {
			e.printStackTrace();
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			dbutil.reset();
		}
	}

	private void startAuditLogAction() throws TBAFrameworkException {
		DBUtil dbutil = getDBContext().createUtilInstance();
		try {
			// ADDED BY SWAROOPA AS ON 30-10-2013 BEGINS
			dbutil.reset();
			dbutil.setMode(DBUtil.CALLABLE);
			dbutil.setSql("CALL SP_AUDITLOG_START_LOGACTION( ?,?,?,?,?,?,?,?)");
			// dbutil.setSql("CALL AUDITLOG_START_LOG_ACTION( ?,?,?,?,?,?,?.?,?)");
			dbutil.setString(1, processAction.getActionEntity());
			dbutil.setString(2, processAction.getProcessID());
			dbutil.setString(3, getTbaPrimaryKey());
			dbutil.setString(4, processAction.getActionUser());
			dbutil.setString(5, getUserAction());
			dbutil.setString(6, "");
			dbutil.registerOutParameter(7, Types.BIGINT);
			dbutil.registerOutParameter(8, Types.TIMESTAMP);
			dbutil.execute();

			auditlogSl = dbutil.getLong(7);
			auditlogDateTime = dbutil.getTimestamp(8);
			// ADDED BY SWAROOPA AS ON 3-0-10-2013 ENDS

		} catch (SQLException e) {
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			dbutil.reset();
		}
	}

	public String getUserAction() {
		return UserAction;
	}

	public String getProgramType() {
		return programType;
	}

	public void setUserAction(String userAction) {
		UserAction = userAction;
	}

	public LinkedHashMap<String, String> getAuditMap() {
		return auditMap;
	}

	public void setAuditMap(LinkedHashMap<String, String> auditMap) {
		this.auditMap = auditMap;
	}

	private void checkTbaauthqRow() throws TBAFrameworkException {
		int w_count = 0;
		DBUtil dbutil = getDBContext().createUtilInstance();
		try {
			dbutil.reset();
			dbutil.setMode(DBUtil.PREPARED);
			dbutil.setSql("SELECT COUNT(1) CNT FROM TBAAUTHDTL WHERE  ENTITY_CODE = ? AND TBADTL_PGM_ID = ? AND TBADTL_MAIN_PK = ? AND TBADTL_ENTRY_DATE = ?");
			dbutil.setString(1, processAction.getActionEntity());
			dbutil.setString(2, processAction.getProcessID());
			dbutil.setString(3, getTbaPrimaryKey());
			dbutil.setDate(4, getTbaActionDate());
			ResultSet rs = dbutil.executeQuery();
			if (rs.next()) {
				w_count = rs.getInt("CNT");
			}
		} catch (SQLException e) {
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			dbutil.reset();
		}
		if (w_count == 0) {
			if (getProgramType().equals("A")) {
				deleteTbaauthqRow();
			}
		}
	}

	private void deleteTbaauthqRow() throws TBAFrameworkException {
		DBUtil dbutil = getDBContext().createUtilInstance();
		try {
			dbutil.reset();
			dbutil.setMode(DBUtil.PREPARED);
			dbutil.setSql("DELETE FROM TBAAUTHQ WHERE  ENTITY_CODE = ?  AND  TBAAUTHQ.TBAQ_PGM_ID =? AND TBAAUTHQ.TBAQ_MAIN_PK = ?");
			dbutil.setString(1, processAction.getActionEntity());
			dbutil.setString(2, processAction.getProcessID());
			dbutil.setString(3, getTbaPrimaryKey());
			dbutil.executeUpdate();
			dbutil.reset();
			dbutil.setSql("DELETE FROM TBAAUTH WHERE  ENTITY_CODE = ?  AND TBAAUTH_PGM_ID = ? AND TBAAUTH_MAIN_PK =? AND TBAAUTH_ENTRY_DATE = ?");
			dbutil.setString(1, processAction.getActionEntity());
			dbutil.setString(2, processAction.getProcessID());
			dbutil.setString(3, getTbaPrimaryKey());
			dbutil.setDate(4, getTbaActionDate());
			dbutil.executeUpdate();
		} catch (SQLException e) {
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			dbutil.reset();
		}
	}

	public void performTBAInitialization() throws TBAFrameworkException {
		if (isTbaRequired()) {
			updateTbaDetails();
		}

	}

	private void checkEffectiveDateParam() throws TBAFrameworkException {
		if (getMpgmEfftDateReq().equals("1")) {
			DBUtil dbutil = getDBContext().createUtilInstance();
			try {
				String[] strSourceKey = BackOfficeFormatUtils.splitSourceKey(getTbaPrimaryKey());
				String efftDateString = strSourceKey[strSourceKey.length - 1];
				Date cbd = getSystemDate();
				Date effectiveDate = new java.sql.Date(BackOfficeFormatUtils.getDate(efftDateString, BackOfficeConstants.TBA_DATE_FORMAT).getTime());

				boolean isCurrentDate = BackOfficeDateUtils.isDateEqual(effectiveDate, cbd);
				if (isCurrentDate) {
					performPGMCTLOperation();
				} else {
					StringBuffer _deletesql = new StringBuffer("DELETE FROM EFFDTFWDQ WHERE ENTITY_CODE=? AND PGM_ID=? AND EFFT_DATE=? AND MAIN_PK=?");
					dbutil.reset();
					dbutil.setSql(_deletesql.toString());
					dbutil.setString(1, processAction.getActionEntity());
					dbutil.setString(2, processAction.getProcessID());
					dbutil.setDate(3, effectiveDate);
					dbutil.setString(4, getTbaPrimaryKey());
					dbutil.executeUpdate();

					StringBuffer _insertsql = new StringBuffer("INSERT INTO EFFDTFWDQ  VALUES (?,?,?,?,?)");
					dbutil.reset();
					dbutil.setSql(_insertsql.toString());
					dbutil.setString(1, processAction.getActionEntity());
					dbutil.setString(2, processAction.getProcessID());
					dbutil.setDate(3, effectiveDate);
					dbutil.setString(4, getTbaPrimaryKey());
					dbutil.setString(5, getUserAction());
					dbutil.executeUpdate();
				}

			} catch (SQLException e) {
				throw new TBAFrameworkException(e.getLocalizedMessage());
			} finally {
				dbutil.reset();
			}
		}

	}

	private DTObject performPGMCTLOperation() throws TBAFrameworkException {
		DTObject resultDTO = new DTObject();
		String _className = "";
		DTObject dto = new DTObject();
		boolean recordExists = false;
		DBUtil dbutil = getDBContext().createUtilInstance();
		try {
			dbutil.reset();
			dbutil.setMode(DBUtil.PREPARED);
			dbutil.setSql("SELECT PGMCTL_CLASS_NAME FROM PGMCTL WHERE PGMCTL_PGM_ID = ?");
			dbutil.setString(1, processID);
			ResultSet rs = dbutil.executeQuery();
			if (rs.next()) {
				_className = rs.getString("PGMCTL_CLASS_NAME");
				recordExists = true;
			}
			dbutil.reset();
			if (recordExists) {
				dto.set("PARTITION_NO", getPartitionNo());
				dto.set("SOURCE_KEY", getTbaPrimaryKey());
				dto.set("TBA_MAIN_KEY", tbaEntryDate + "|" + tba_sl);
				dto.set("CLASS_NAME", _className);
				dto.set("USER_ID", getProcessAction().getActionUser());
				dto.setObject("CBD", getProcessAction().getActionDate());
				dto.set("ACTION_TYPE", getProcessAction().getActionType().equals(TBAActionType.ADD) ? CommonTBAUpdate.ADD : CommonTBAUpdate.MODIFY);
				dto.set(CommonTBAUpdate.PROCESS_OPTION, CommonTBAUpdate.AUTHORIZE);
				dto.set("CLUSTER_CODE", getProcessAction().getClusterCode());
				dto.set("FIN_YEAR", getProcessAction().getFinYear());
				dto.set("METHOD_NAME", RegularConstants.PROCESS_EFFTDATE_METHOD);
				pgmctlreflection(dto);

			}
		} catch (Exception e) {
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			dbutil.reset();
		}
		return resultDTO;

	}

	private void updateTbaDetails() throws TBAFrameworkException {
		getTbakeyDetails();
		if (!isFinancialOperation()) {
			updateTbaauthMain();
		} else {
			updateCTRANauthQueue();
		}
		setTbaActionDate(tbaEntryDate);
		setTbaSerial(tba_sl);

	}

	private void updateCTRANauthQueue() throws TBAFrameworkException {
		int _dirtyCount = 0;
		DBUtil dbutil = getDBContext().createUtilInstance();
		try {

			_dirtyCount = 0;
			StringBuffer _sql = new StringBuffer("INSERT INTO CTRANAUTHQ  VALUES (?,?,?,?,?,?,?,?,?,?,?)");
			dbutil.reset();
			dbutil.setSql(_sql.toString());
			dbutil.setString(++_dirtyCount, processAction.getActionEntity());
			dbutil.setString(++_dirtyCount, processAction.getProcessID());
			dbutil.setString(++_dirtyCount, getTbaPrimaryKey());
			dbutil.setDate(++_dirtyCount, tbaEntryDate);
			dbutil.setDouble(++_dirtyCount, tba_sl);
			dbutil.setString(++_dirtyCount, getMainTableName());
			dbutil.setString(++_dirtyCount, getUserAction());
			dbutil.setString(++_dirtyCount, processAction.getActionUser());
			dbutil.setTimestamp(++_dirtyCount, getProcessActionDateTime());
			dbutil.setString(++_dirtyCount, RegularConstants.NULL);
			dbutil.setString(++_dirtyCount, processAction.getActionRole());
			dbutil.executeUpdate();
		} catch (SQLException e) {
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			dbutil.reset();
		}
	}

	private void getTbakeyDetails() throws TBAFrameworkException {
		checkTbaAuthq(getProcessAction().getProcessID(), getTbaPrimaryKey());
		String str[] = tbaKey.split("\\|");
		if (str.length > 1) {
			tbaEntryDate = Date.valueOf(str[0]);
			tba_sl = Integer.parseInt(str[1]);
			tbaRectifyKey = tbaKey;
		} else {
			tbaEntryDate = getSystemDate();
			tba_sl = 1;
			tbaRectifyKey = "";
		}
	}

	public void checkTbaAuthq(String Pgmid, String primary_key) throws TBAFrameworkException {
		ResultSet rs = null;
		Date authq_entry_date = null;
		long authq_entry_sl = 0;
		DBUtil dbutil = getDBContext().createUtilInstance();
		try {
			tbaKey = "";
			dbutil.reset();
			dbutil.setMode(DBUtil.PREPARED);
			dbutil.setSql("SELECT TBAQ_ENTRY_DATE, TBAQ_ENTRY_SL,TBAQ_OPERN_FLG FROM TBAAUTHQ WHERE  ENTITY_CODE = ?  AND  TBAQ_PGM_ID = ? AND TBAQ_MAIN_PK = ? ");
			dbutil.setString(1, processAction.getActionEntity());
			dbutil.setString(2, processAction.getProcessID());
			dbutil.setString(3, getTbaPrimaryKey());
			rs = dbutil.executeQuery();
			if (rs.next()) {
				authq_entry_date = rs.getDate("TBAQ_ENTRY_DATE");
				authq_entry_sl = rs.getLong("TBAQ_ENTRY_SL");
				tbaKey = authq_entry_date.toString() + "|" + authq_entry_sl;
			} else {
				Date currentDate = getSystemDate();
				dbutil.reset();
				dbutil.setSql("SELECT TBAAUTH_ENTRY_DATE,TBAAUTH_ENTRY_SL,TBAAUTH_OPERN_FLG,TBAAUTH_STATUS FROM TBAAUTH WHERE ENTITY_CODE = ? AND TBAAUTH_PGM_ID = ?  AND TBAAUTH_MAIN_PK = ? AND TBAAUTH_ENTRY_DATE = ? ORDER BY TBAAUTH_ENTRY_SL DESC");
				dbutil.setString(1, processAction.getActionEntity());
				dbutil.setString(2, processAction.getProcessID());
				dbutil.setString(3, getTbaPrimaryKey());
				dbutil.setDate(4, currentDate);
				rs = dbutil.executeQuery();
				if (rs.next()) {
					authq_entry_date = rs.getDate("TBAAUTH_ENTRY_DATE");
					authq_entry_sl = rs.getLong("TBAAUTH_ENTRY_SL");
					String authStatus = rs.getString("TBAAUTH_STATUS");
					if (authStatus.equals("P")) {
						tbaKey = authq_entry_date.toString() + "|" + authq_entry_sl;
					} else {
						tbaKey = authq_entry_date.toString() + "|" + (authq_entry_sl + 1);
					}
				} else {
					tbaKey = "";
				}
			}
		} catch (SQLException e) {
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			dbutil.reset();
		}
	}

	public Timestamp getSystemTime() {
		Timestamp sys_date_time = null;
		DBUtil dbutil = getDBContext().createUtilInstance();
		try {
			dbutil.reset();
			dbutil.setMode(DBUtil.PREPARED);
			dbutil.setSql("SELECT FN_GETCDT(?) FROM DUAL");
			dbutil.setString(1, processAction.getActionEntity());
			ResultSet rs = dbutil.executeQuery();
			if (rs.next()) {
				sys_date_time = rs.getTimestamp(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			dbutil.reset();
		}
		return sys_date_time;
	}

	public Date getSystemDate() {
		Date sys_date_time = null;
		DBUtil dbutil = getDBContext().createUtilInstance();
		try {
			dbutil.reset();
			dbutil.setMode(DBUtil.PREPARED);
			dbutil.setSql("SELECT FN_GETCD(?) FROM DUAL");
			dbutil.setString(1, processAction.getActionEntity());
			ResultSet rs = dbutil.executeQuery();
			if (rs.next()) {
				sys_date_time = rs.getDate(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			dbutil.reset();
		}
		return sys_date_time;
	}

	private void updateTbaauthMain() throws TBAFrameworkException {
		int _dirtyCount = 0;
		boolean updateRecord;
		updateRecord = checkTbaauthrecord();
		DBUtil dbutil = getDBContext().createUtilInstance();
		try {
			if (updateRecord == true) {
				// delete previous rectified record from queue
				deleteTbaauthrecord();
				// update previous rectified serial
				dbutil.reset();
				dbutil.setMode(DBUtil.PREPARED);
				String sql = "UPDATE TBAAUTH SET TBAAUTH_STATUS = 'T' WHERE  ENTITY_CODE = ? AND TBAAUTH_PGM_ID = ? AND  TBAAUTH_MAIN_PK = ? AND TBAAUTH_ENTRY_DATE = ? AND TBAAUTH_ENTRY_SL = ?";
				dbutil.setSql(sql.toString());
				dbutil.setString(1, processAction.getActionEntity());
				dbutil.setString(2, processAction.getProcessID());
				dbutil.setString(3, getTbaPrimaryKey());
				dbutil.setDate(4, tbaEntryDate);
				dbutil.setLong(5, tba_sl);
				dbutil.executeUpdate();
				tba_sl = tba_sl + 1;
			}
			// insert new record with latest serial
			_dirtyCount = 0;

			StringBuffer _sql = new StringBuffer("INSERT INTO TBAAUTH" + "(ENTITY_CODE,TBAAUTH_PGM_ID,TBAAUTH_MAIN_PK,TBAAUTH_ENTRY_DATE,TBAAUTH_ENTRY_SL,TBAAUTH_OPERN_FLG,TBAAUTH_DONE_BY,TBAAUTH_DONE_ON,TBAAUTH_DONE_PKI,TBAAUTH_STATUS,TBAAUTH_MAIN_TABLE_NAME,TBAAUTH_RECTIFY_KEY) " + "VALUES " + "(?,?,?,?,?,?,?,?,?,?,?,?)");
			dbutil.reset();
			dbutil.setMode(DBUtil.PREPARED);
			dbutil.setSql(_sql.toString());
			dbutil.setString(++_dirtyCount, processAction.getActionEntity());
			dbutil.setString(++_dirtyCount, processAction.getProcessID());
			dbutil.setString(++_dirtyCount, getTbaPrimaryKey());
			dbutil.setDate(++_dirtyCount, tbaEntryDate);
			dbutil.setLong(++_dirtyCount, tba_sl);
			dbutil.setString(++_dirtyCount, getUserAction());
			dbutil.setString(++_dirtyCount, processAction.getActionUser());
			dbutil.setTimestamp(++_dirtyCount, getProcessActionDateTime());
			dbutil.setDate(++_dirtyCount, null);
			dbutil.setString(++_dirtyCount, "P");
			dbutil.setString(++_dirtyCount, getMainTableName());
			dbutil.setString(++_dirtyCount, tbaRectifyKey);
			// added by swaroopa as 26-04-2012 ends
			int i = dbutil.executeUpdate();
			if (i == 0)
				throw new TBAFrameworkException("TBAAUTH not inserted");

			_dirtyCount = 0;
			_sql = new StringBuffer("INSERT INTO TBAAUTHQ  VALUES (?,?,?,?,?,?,?,?,?,?,?,?)");
			dbutil.reset();
			dbutil.setSql(_sql.toString());
			dbutil.setString(++_dirtyCount, processAction.getActionEntity());
			dbutil.setString(++_dirtyCount, processAction.getProcessID());
			dbutil.setString(++_dirtyCount, getTbaPrimaryKey());
			dbutil.setDate(++_dirtyCount, tbaEntryDate);
			dbutil.setDouble(++_dirtyCount, tba_sl);
			dbutil.setString(++_dirtyCount, getDisplayDetails());
			dbutil.setString(++_dirtyCount, getMainTableName());
			dbutil.setString(++_dirtyCount, getUserAction());
			dbutil.setString(++_dirtyCount, processAction.getActionUser());
			dbutil.setTimestamp(++_dirtyCount, getProcessActionDateTime());
			dbutil.setString(++_dirtyCount, RegularConstants.NULL);
			dbutil.setString(++_dirtyCount, processAction.getActionRole());
			dbutil.executeUpdate();
		} catch (SQLException e) {
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			dbutil.reset();
		}
	}

	public boolean checkTbaauthrecord() throws TBAFrameworkException {
		boolean update_rec = false;
		DBUtil dbutil = getDBContext().createUtilInstance();
		try {
			dbutil.reset();
			dbutil.setMode(DBUtil.PREPARED);
			dbutil.setSql("SELECT 1 FROM TBAAUTH WHERE  ENTITY_CODE = ? AND  TBAAUTH_PGM_ID = ? AND TBAAUTH_MAIN_PK = ? AND TBAAUTH_ENTRY_DATE = ? AND TBAAUTH_ENTRY_SL = ?");
			dbutil.setString(1, processAction.getActionEntity());
			dbutil.setString(2, processAction.getProcessID());
			dbutil.setString(3, getTbaPrimaryKey());
			dbutil.setDate(4, tbaEntryDate);
			dbutil.setLong(5, tba_sl);
			ResultSet rs = dbutil.executeQuery();
			update_rec = rs.next();
		} catch (SQLException e) {
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			dbutil.reset();
		}
		return update_rec;
	}

	private void deleteTbaauthrecord() throws TBAFrameworkException {
		DBUtil dbutil = getDBContext().createUtilInstance();
		try {
			/*
			 * dbutil.reset(); dbutil.setMode(DBUtil.PREPARED); dbutil.setSql(
			 * "DELETE FROM TBAAUTHDTL WHERE  ENTITY_CODE = ? AND TBADTL_PGM_ID= ? AND TBADTL_MAIN_PK =? AND TBADTL_ENTRY_DATE = ? AND TBADTL_DTL_SL = ?"
			 * ); dbutil.setString(1, processAction.getActionEntity());
			 * dbutil.setString(2, processAction.getProcessID());
			 * dbutil.setString(3, getTbaPrimaryKey()); dbutil.setDate(4,
			 * tbaEntryDate); dbutil.setDouble(5, tba_sl);
			 * dbutil.executeUpdate();
			 */
			dbutil.reset();
			dbutil.setMode(DBUtil.PREPARED);
			dbutil.setSql("DELETE FROM TBAAUTHQ WHERE  ENTITY_CODE = ? AND  TBAAUTHQ.TBAQ_PGM_ID =? AND TBAAUTHQ.TBAQ_MAIN_PK = ?");
			dbutil.setString(1, processAction.getActionEntity());
			dbutil.setString(2, processAction.getProcessID());
			dbutil.setString(3, getTbaPrimaryKey());
			dbutil.executeUpdate();
			dbutil.reset();
		} catch (SQLException e) {
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			dbutil.reset();
		}
	}

	public void setDisplayDetails(String displayDetails) {
		this.displayDetails = displayDetails;
	}

	public String getDisplayDetails() {
		return displayDetails;
	}

	// added by swaroopa as on 26-04-2012 begins

	// added by swaroopa as on 26-04-2012 ends

	// added by swaroopa as on 01-05-2012 begins

	public void updatePKIEncodedData() throws TBAFrameworkException {
		DBUtil dbutil = getDBContext().createUtilInstance();
		try {
			dbutil.reset();
			dbutil.setMode(DBUtil.CALLABLE);
			dbutil.setSql("CALL PKG_AUDIT_PKI.UPDATE_PKI_ENC_DATA(?,?,?,FN_GETCDT(?),?,?,?,?,?)");
			dbutil.setString(1, processAction.getActionEntity());
			dbutil.setString(2, processTFAInfo.getDeploymentContext());
			dbutil.setString(3, processAction.getActionUser());
			dbutil.setString(4, processAction.getActionEntity());
			dbutil.setString(5, processTFAInfo.getDigitalCertificateInventoryNumber());
			dbutil.setString(6, processAction.getActionIP());
			dbutil.setString(7, processTFAInfo.getTfaValue());
			dbutil.setString(8, processTFAInfo.getTfaEncodedValue());
			dbutil.registerOutParameter(9, Types.VARCHAR);
			dbutil.execute();
			String tfaInventoryNumber = dbutil.getString(9);
			if (tfaInventoryNumber != null) {
				processTFAInfo.setTfaInventoryNumber(tfaInventoryNumber);
			}

		} catch (SQLException e) {
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			dbutil.reset();
		}
	}
	// added by swaroopa as on 01-05-2012 ends

}