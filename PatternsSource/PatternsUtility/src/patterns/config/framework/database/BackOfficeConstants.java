package patterns.config.framework.database;

public class BackOfficeConstants {

	public static final String TBA_DATE_FORMAT = "%d-%m-%Y";
	public static final String JAVA_TBA_DATE_FORMAT = "dd-MM-yyyy";
	public static final String JAVA_DATE_FORMAT = "dd-MM-yyyy";
	public static final String DB_DATE_FORMAT = "%d-%b-%Yy";
	public static final String TBA_XML_DATE_FORMAT = "%d-%m-%Y %H:%i:%s";
	//public static final String JAVA_TBA_DB_DATE_FORMAT = "%d-%c-%Y %T:%i:%f";
	public static final String JAVA_TBA_DB_DATE_FORMAT = "%d-%m-%Y %H:%i:%s";
	public static final String JAVA_DATE_TIME_FORMAT = "dd-MM-yyyy HH:mm:ss";
	public static final String DB_TBA_DATE_FORMAT = "%d-%m-%Y";
	// public static final String DB_TBA_DATE_FORMAT = "%d-%m-%Y";
	public static final String TBA_REV_DATE_FORMAT = "%Y-%m-%d";
	public static final String INFO_MESSAGE = "_INFO_MESSAGE";

	public static final String TIME_HH24_MM = "%h:%i";
	public static final String TIME_HH24_MM_SS = "%h:%i:%s";

	public static final String MAX_VAL_99 = "99";
	public static final String MAX_VAL_9999 = "9999";
	public static final String MAX_VAL_999999 = "999999";
	public static final String MAX_VAL_99999999 = "99999999";

	public static final String JAVA_TIME_HH24_MM_SS = "%h:%i:%s";

	public static final String TBA_DB_DATE_FORMAT = "%d-%m-%Y %H:%i:%s";
	public static final String TBA_XML_DATETIME_FORMAT = "%d-%m-%Y %H:%i:%s";
}
