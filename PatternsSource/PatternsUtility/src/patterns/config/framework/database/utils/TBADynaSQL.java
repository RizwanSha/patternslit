package patterns.config.framework.database.utils;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import patterns.config.framework.DatasourceConfigurationManager;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.BindParameterType;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.objects.TableInfo;
import patterns.config.framework.interfaces.OutwardMessageProcessor;
import patterns.config.framework.loggers.ApplicationLogger;
import patterns.config.framework.process.TBAActionType;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.thread.TBAContext;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.framework.web.ajax.ContentManager;

public class TBADynaSQL {
	ApplicationLogger logger = null;
	private HashMap<String, Object> internalStore = new HashMap<String, Object>();
	private HashMap<String, Object> OldImageStore = new HashMap<String, Object>();
	String tableName = null;
	boolean create = true;
	boolean mainTable = false;
	boolean dependentTable = false;
	private String[] keyFields;
	private BindParameterType[] keyFieldTypes;
	private String[] fields;
	@SuppressWarnings("unused")
	private BindParameterType[] fieldTypes;
	private TBAContext processContext;
	private DBContext dbContext = null;
	String errorMessage;
	StringBuffer sqlQuery = new StringBuffer();
	TableInfo tableInfo = null;
	private final String NewDataChar = "N";
	private final String OldDataChar = "O";
	private final String TableValueSep = "@";

	private String OldData_Key;
	private String NewData_Key;
	private String DataBlock_Old;
	private String DataBlock_New;
	private String AbstractionType = "A";
	private String MechanismType = "M";
	private long blockSerial = 1;
	private String AUTHORIZE = "A";
	private String ENTRY_MODE = "E";
	private String MODIFY_MODE = "M";
	private String TRANSIT_PREVENT_USAGE = "1";
	private String TRANSIT_USE_EXISTING = "2";
	private String partitionYear = "";
	private boolean istablePartioned;

	public boolean isIstablePartioned() {
		return istablePartioned;
	}

	public void setIstablePartioned(boolean istablePartioned) {
		this.istablePartioned = istablePartioned;
	}

	public String getPartitionYear() {
		return partitionYear;
	}

	public void setPartitionYear(String partitionYear) {
		this.partitionYear = partitionYear;
	}

	public boolean isDependentTable() {
		return dependentTable;
	}

	public void setDependentTable(boolean dependentTable) {
		this.dependentTable = dependentTable;
	}

	public TBADynaSQL(String tableName) {
		logger = ApplicationLogger.getInstance(this.getClass().getSimpleName());
		this.tableName = tableName;
		this.mainTable = false;
		init();
	}

	public TBADynaSQL(String tableName, boolean mainTable) {
		logger = ApplicationLogger.getInstance(this.getClass().getSimpleName());
		this.tableName = tableName;
		this.mainTable = mainTable;
		init();
	}

	private void init() {
		setTableName(tableName);
		processContext = TBAContext.getInstance();
		dbContext = processContext.getDBContext();
		String dataSource = DatasourceConfigurationManager.getDatabaseSource();
		DBInfo info = new DBInfo();
		tableInfo = info.getTableInfo(this.tableName);
		fields = tableInfo.getColumnInfo().getColumnNames();
		fieldTypes = tableInfo.getColumnInfo().getColumnTypes();
		keyFields = tableInfo.getPrimaryKeyInfo().getColumnNames();
		keyFieldTypes = tableInfo.getPrimaryKeyInfo().getColumnTypes();

	}

	// added by aribala for linked programs
	public TBADynaSQL(String tableName, boolean mainTable, DBContext databaseContext) {
		logger = ApplicationLogger.getInstance(this.getClass().getSimpleName());
		// logger.logDebug("#()");
		this.tableName = tableName;
		this.mainTable = mainTable;
		setTableName(tableName);
		dbContext = databaseContext;
		DBInfo info = new DBInfo();
		tableInfo = info.getTableInfo(this.tableName);
		fields = tableInfo.getColumnInfo().getColumnNames();
		fieldTypes = tableInfo.getColumnInfo().getColumnTypes();
		keyFields = tableInfo.getPrimaryKeyInfo().getColumnNames();
		keyFieldTypes = tableInfo.getPrimaryKeyInfo().getColumnTypes();
	}

	private void subClose() {
		internalStore.clear();
		keyFields = null;
		keyFieldTypes = null;
		tableInfo = null;
		create = true;
	}

	public void close() {
		subClose();
	}

	private void setTableName(String tableName) {
		subClose();
		this.tableName = tableName;
	}

	public String getTableName() {
		return tableName;
	}

	public void setField(String fieldName, Object fieldValue) {
		internalStore.put(fieldName, fieldValue);
	}

	public Object getField(String fieldName) {
		return internalStore.get(fieldName);
	}

	public void clearFieldStore() {
		internalStore.clear();
	}

	// Aribala added BEG
	public long getMaxSerial(String maxSerialColumn) {
		return getMaximumSerialByFields(maxSerialColumn, keyFields, keyFieldTypes);
	}

	public long getMaximumSerialByFields(String maxSerialColumn, String[] keyFields, BindParameterType[] keyFieldTypes) {
		long maxSerial = 0;
		sqlQuery.setLength(0);
		sqlQuery.append("SELECT IFNULL(MAX(").append(maxSerialColumn).append("),0)+ 1 FROM ").append(tableName);
		if (keyFields.length > 0)
			sqlQuery.append(" WHERE ");
		int i = 1;
		for (String fieldName : keyFields) {
			if (i < keyFields.length - 1) {
				sqlQuery.append(fieldName).append(" = ? AND ");
			} else {
				sqlQuery.append(fieldName).append(" = ?");
				break;
			}
			i++;
		}
		sqlQuery.trimToSize();
		DBUtil util = dbContext.createUtilInstance();
		try {
			util.reset();
			util.setSql(sqlQuery.toString());
			i = 0;
			for (BindParameterType fieldType : keyFieldTypes) {
				if (i >= keyFieldTypes.length - 1)
					break;
				++i;
				switch (fieldType) {
				case LONG:
					util.setLong(i, (Long) getField(keyFields[i - 1]));
					break;
				case VARCHAR:
					util.setString(i, (String) getField(keyFields[i - 1]));
					break;
				case DATE:
					Object value = getField(keyFields[i - 1]);
					if (value == null)
						util.setNull(i, Types.DATE);
					else if (value instanceof Timestamp)
						util.setTimestamp(i, (Timestamp) value);
					else if (value instanceof java.sql.Date)
						util.setDate(i, (Date) value);
					else if (value instanceof java.util.Date)
						util.setDate(i, new java.sql.Date(((java.util.Date) value).getTime()));
					break;
				case INTEGER:
					util.setInt(i, (Integer) getField(keyFields[i - 1]));
					break;
				case TIMESTAMP:
					util.setTimestamp(i, (Timestamp) getField(keyFields[i - 1]));
					break;
				case DECIMAL:
					util.setBigDecimal(i, (BigDecimal) getField(keyFields[i - 1]));
					break;
				case BIGINT:
					Object _value = getField(keyFields[i - 1]);
					if (_value instanceof Long)
						util.setLong(i, (Long) _value);
					else
						util.setString(i, (String) _value);
					break;
				}
			}
			ResultSet rset = util.executeQuery();
			if (rset.next())
				maxSerial = rset.getLong(1);
			else
				maxSerial = 1;
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError(" getMaxSerialByFields(1) - " + e.getLocalizedMessage());
			errorMessage = e.getLocalizedMessage();
			maxSerial = 0;
		} finally {
			util.reset();
		}
		return maxSerial;
	}

	// Aribala added END

	public int deleteByKeyFields() {
		return deleteByFields(keyFields, keyFieldTypes);
	}

	public int deleteByFields(String[] keyFields, BindParameterType[] keyFieldTypes) {
		int count = 0;
		sqlQuery.setLength(0);
		sqlQuery.append("DELETE FROM ").append(tableName);
		if (keyFields.length > 0)
			sqlQuery.append(" WHERE ");
		int i = 0;
		for (String fieldName : keyFields) {
			if (i < keyFields.length - 1) {
				sqlQuery.append(fieldName).append(" = ? AND ");
			} else {
				sqlQuery.append(fieldName).append(" = ?");
			}
			i++;
		}
		sqlQuery.trimToSize();
		DBUtil util = dbContext.createUtilInstance();
		try {
			util.reset();
			util.setSql(sqlQuery.toString());
			i = 0;
			for (BindParameterType fieldType : keyFieldTypes) {
				++i;
				switch (fieldType) {
				case LONG:
					util.setLong(i, (Long) getField(keyFields[i]));
					break;
				case VARCHAR:
					util.setString(i, (String) getField(keyFields[i]));
					break;
				case DATE:
					Object value = getField(keyFields[i]);
					if (value == null)
						util.setNull(i, Types.DATE);
					else if (value instanceof Timestamp)
						util.setTimestamp(i, (Timestamp) value);
					else if (value instanceof java.sql.Date)
						util.setDate(i, (Date) value);
					else if (value instanceof java.util.Date)
						util.setDate(i, new java.sql.Date(((java.util.Date) value).getTime()));
					break;
				case INTEGER:
					util.setInt(i, (Integer) getField(keyFields[i]));
					break;
				case TIMESTAMP:
					util.setTimestamp(i, (Timestamp) getField(keyFields[i]));
					break;
				case DECIMAL:
					util.setBigDecimal(i, (BigDecimal) getField(keyFields[i]));
					break;
				case BIGINT:
					Object _value = getField(keyFields[i]);
					if (_value instanceof Long)
						util.setLong(i, (Long) _value);
					else
						util.setString(i, (String) _value);
					break;
				}
			}
			count = util.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError("deleteByFields(1) - " + e.getLocalizedMessage());
			errorMessage = e.getLocalizedMessage();
			count = 0;
		} finally {
			util.reset();
		}
		return count;
	}

	public int deleteByFieldsAudit(String[] keyFields, BindParameterType[] keyFieldTypes) {
		// ARIBALA CHANGED ON 09102015 BEGIN
		if ((processContext.getProgramType().equals("A") && !processContext.isTbaRequired()) || processContext.getProgramType().equals("M")) {
			// ARIBALA CHANGED ON 09102015 END
			getOldValuesforAudit(keyFields, keyFieldTypes);
			int count = 0;
			sqlQuery.setLength(0);
			sqlQuery.append("DELETE FROM ").append(tableName);
			if (keyFields.length > 0)
				sqlQuery.append(" WHERE ");

			int i = 0;
			for (i = 0; i < keyFields.length - 1; i++) {

				if (i < keyFields.length - 2) {
					sqlQuery.append(keyFields[i]).append(" = ? AND ");
				} else {
					sqlQuery.append(keyFields[i]).append(" = ?");
				}
			}
			sqlQuery.trimToSize();
			DBUtil util = dbContext.createUtilInstance();
			try {
				util.reset();
				util.setSql(sqlQuery.toString());
				i = 0;
				int k = 1;
				for (i = 0; i < keyFieldTypes.length - 1; i++) {
					BindParameterType fieldType = keyFieldTypes[i];
					switch (fieldType) {
					case LONG:
						util.setLong(k, (Long) getField(keyFields[i]));
						break;
					case VARCHAR:
						util.setString(k, (String) getField(keyFields[i]));
						break;
					case DATE:
						Object value = getField(keyFields[i]);
						if (value == null)
							util.setNull(k, Types.DATE);
						else if (value instanceof Timestamp)
							util.setTimestamp(k, (Timestamp) value);
						else if (value instanceof java.sql.Date)
							util.setDate(k, (Date) value);
						else if (value instanceof java.util.Date)
							util.setDate(k, new java.sql.Date(((java.util.Date) value).getTime()));
						else
							util.setString(k, (String) value);
						break;
					case INTEGER:
						// ARIBALA CHANGED ON 09102015 BEGIN
						Object intValue = getField(keyFields[i]);
						if (intValue instanceof Integer)
							util.setInt(k, (Integer) getField(keyFields[i]));
						else
							util.setString(k, (String) intValue);
						break;
					// ARIBALA CHANGED ON 09102015 END
					case TIMESTAMP:
						util.setTimestamp(k, (Timestamp) getField(keyFields[i]));
						break;
					case DECIMAL:
						util.setBigDecimal(k, (BigDecimal) getField(keyFields[i]));
						break;
					case BIGINT:
						Object _value = getField(keyFields[i]);
						if (_value instanceof Long)
							util.setLong(k, (Long) _value);
						else
							util.setString(k, (String) _value);
						break;
					}
					k++;
				}
				count = util.executeUpdate();
			} catch (Exception e) {
				e.printStackTrace();
				logger.logError(" deleteByFieldsAudit(1) - " + e.getLocalizedMessage());
				errorMessage = e.getLocalizedMessage();
				count = 0;
			} finally {
				util.reset();
			}
			return count;
		}
		return -1;

	}

	private void getOldValuesforAudit(String[] keyFields, BindParameterType[] keyFieldTypes) {
		String primaryKeyValues = "";
		sqlQuery.setLength(0);
		OldImageStore.clear();
		sqlQuery.append("SELECT * FROM ").append(tableName);
		if (keyFields.length > 0)
			sqlQuery.append(" WHERE ");
		for (int i = 0; i < keyFields.length - 1; i++) {
			if (i < keyFields.length - 2) {
				sqlQuery.append(keyFields[i]).append(" = ? AND ");
			} else {
				sqlQuery.append(keyFields[i]).append(" = ?");
			}
		}
		sqlQuery.append(" ORDER BY " + keyFields[keyFields.length - 1]);
		sqlQuery.trimToSize();
		DBUtil util = dbContext.createUtilInstance();
		try {
			util.reset();
			util.setSql(sqlQuery.toString());
			int k = 1;
			for (int i = 0; i < keyFieldTypes.length - 1; i++) {
				BindParameterType fieldType = keyFieldTypes[i];
				// pras changes required

				switch (fieldType) {
				case LONG:
					util.setLong(k, (Long) getField(keyFields[i]));
					primaryKeyValues += (Long) getField(keyFields[i]) + "|";
					break;
				case VARCHAR:
					util.setString(k, (String) getField(keyFields[i]));
					primaryKeyValues += (String) getField(keyFields[i]) + "|";
					break;
				case DATE:
					Object value = getField(keyFields[i]);
					if (value == null) {
						util.setNull(k, Types.DATE);
						primaryKeyValues += RegularConstants.EMPTY_STRING + "|";
					} else if (value instanceof Timestamp) {
						util.setTimestamp(k, (Timestamp) value);
						primaryKeyValues += BackOfficeFormatUtils.getDate((java.util.Date) value, BackOfficeConstants.TBA_DATE_FORMAT) + "|";
					} else if (value instanceof java.sql.Date) {
						util.setDate(k, (Date) value);
						primaryKeyValues += BackOfficeFormatUtils.getDate((java.util.Date) value, BackOfficeConstants.TBA_DATE_FORMAT) + "|";
					} else if (value instanceof java.util.Date) {
						util.setDate(k, new java.sql.Date(((java.util.Date) value).getTime()));
						primaryKeyValues += BackOfficeFormatUtils.getDate((java.util.Date) value, BackOfficeConstants.TBA_DATE_FORMAT) + "|";
					} else {
						util.setString(k, (String) value);
						primaryKeyValues += (String) value + "|";
					}
					break;
				case INTEGER:
					Object intValue = getField(keyFields[i]);
					if (intValue instanceof Integer) {
						util.setInt(k, (Integer) getField(keyFields[i]));

						primaryKeyValues += (Integer) intValue + "|";
					} else {
						util.setString(k, (String) intValue);
						primaryKeyValues += (String) intValue + "|";
					}
					break;
				case TIMESTAMP:
					util.setTimestamp(k, (Timestamp) getField(keyFields[i]));
					primaryKeyValues += BackOfficeFormatUtils.getDate((Date) getField(keyFields[i]), BackOfficeConstants.TBA_DATE_FORMAT) + "|";
					break;
				case DECIMAL:
					util.setBigDecimal(k, (BigDecimal) getField(keyFields[i]));
					primaryKeyValues += (BigDecimal) getField(keyFields[i]) + "|";
					break;
				case BIGINT:
					Object _value = getField(keyFields[i]);
					if (_value instanceof Long) {
						util.setLong(k, (Long) _value);
						primaryKeyValues += (Long) _value + "|";
					} else {
						util.setString(k, (String) _value);
						primaryKeyValues += (String) _value + "|";
					}
					break;
				}
				k++;
			}
			primaryKeyValues = primaryKeyValues.substring(0, primaryKeyValues.length() - 1);
			ResultSet rs = util.executeQuery();
			// SimpleDateFormat sdf = new
			// SimpleDateFormat(BackOfficeConstants.DB_DATE_FORMAT);
			while (rs.next()) {
				for (int i = 1; i < rs.getMetaData().getColumnCount() + 1; i++) {
					if (rs.getMetaData().getColumnTypeName(i).equals("DATE")) {
						if (rs.getDate(i) != null) {
							OldImageStore.put(rs.getMetaData().getColumnName(i), rs.getDate(i));
						} else {
							OldImageStore.put(rs.getMetaData().getColumnName(i), null);
						}
					} else if (rs.getMetaData().getColumnTypeName(i).equals("DATETIME") || rs.getMetaData().getColumnTypeName(i).equals("TIMESTAMP")) {
						if (rs.getDate(i) != null) {
							OldImageStore.put(rs.getMetaData().getColumnName(i), rs.getTimestamp(i));
						} else {
							OldImageStore.put(rs.getMetaData().getColumnName(i), null);
						}
					} else {
						OldImageStore.put(rs.getMetaData().getColumnName(i), rs.getString(i));
					}
				}
				OldData_Key = getTableName() + TableValueSep + OldDataChar + primaryKeyValues + TableValueSep + rs.getString(keyFields[keyFields.length - 1]);
				// DataBlock_Old = getXMLData();
				DataBlock_Old = getRowOldData();
				processContext.getAuditMap().put(OldData_Key, DataBlock_Old);
			}
			OldData_Key = "";
			DataBlock_Old = "";
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError(" getOldValuesForAudit(1) - " + e.getLocalizedMessage());
			errorMessage = e.getLocalizedMessage();
		} finally {
			util.reset();
		}
	}

	public boolean loadByKeyFields() {
		boolean isRecExt = false;
		sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT * FROM ").append(tableName).append(" WHERE ");
		int i = 0;
		for (String fieldName : keyFields) {
			if (i < keyFields.length - 1) {
				sqlQuery.append(fieldName).append(" = ?  AND  ");
			} else {
				sqlQuery.append(fieldName).append(" = ? ");
			}
			++i;
		}
		sqlQuery.trimToSize();
		DBUtil util = dbContext.createUtilInstance();
		Map<String, BindParameterType> columnType = tableInfo.getColumnInfo().getColumnMapping();
		util.reset();
		try {
			util.setSql(sqlQuery.toString());
			i = 0;
			for (String fieldName : keyFields) {
				++i;
				switch (columnType.get(fieldName)) {
				case LONG:
					util.setLong(i, (Long) getField(fieldName));
					break;
				case VARCHAR:
					util.setString(i, (String) getField(fieldName));
					break;
				case DATE:
					Object value = getField(fieldName);
					if (value == null)
						util.setNull(i, Types.DATE);
					if (value instanceof Timestamp)
						util.setTimestamp(i, (Timestamp) value);
					else if (value instanceof java.sql.Date)
						util.setDate(i, (Date) value);
					else if (value instanceof java.util.Date)
						util.setDate(i, new java.sql.Date(((java.util.Date) value).getTime()));
					else
						util.setString(i, (String) value);
					break;
				case INTEGER:
					Object val = getField(fieldName);
					if (val instanceof Integer)
						util.setInt(i, (Integer) val);
					else
						util.setString(i, (String) val);
					break;
				case TIMESTAMP:
					util.setTimestamp(i, (Timestamp) getField(fieldName));
					break;
				case DECIMAL:
					util.setBigDecimal(i, (BigDecimal) getField(fieldName));
					break;
				case BIGINT:
					Object _value = getField(fieldName);
					if (_value instanceof Long)
						util.setLong(i, (Long) _value);
					else
						util.setString(i, (String) _value);
					break;
				}
			}
			ResultSet rs = util.executeQuery();
			int columnCount = rs.getMetaData().getColumnCount();
			if (rs.next()) {
				for (i = 1; i <= columnCount; i++) {
					if (rs.getMetaData().getColumnTypeName(i).equals("DATE")) {
						if (rs.getDate(i) != null) {
							setField(rs.getMetaData().getColumnName(i), rs.getDate(i));
						} else {
							setField(rs.getMetaData().getColumnName(i), null);
						}
					} else if (rs.getMetaData().getColumnTypeName(i).equals("DATETIME") || rs.getMetaData().getColumnTypeName(i).equals("TIMESTAMP")) {
						if (rs.getDate(i) != null) {
							setField(rs.getMetaData().getColumnName(i), rs.getTimestamp(i));
						} else {
							setField(rs.getMetaData().getColumnName(i), null);
						}
					} else {
						setField(rs.getMetaData().getColumnName(i), rs.getString(i));
					}
				}
				isRecExt = true;
				OldData_Key = getTableName() + TableValueSep + OldDataChar + processContext.getTbaPrimaryKey();
				DataBlock_Old = getRowData();
			}
			util.reset();
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError(" loadByKeyFields(1) - " + e.getLocalizedMessage());
			errorMessage = e.getLocalizedMessage();
			return false;
		} finally {
			util.reset();
		}
		return isRecExt;
	}

	public void setNew(boolean create) {
		this.create = create;
	}

	public boolean isNew() {
		return create;
	}

	public Timestamp getCurrentTimestamp() {
		Timestamp sys_date_time = null;
		ResultSet rs = null;
		DBUtil dbutil = dbContext.createUtilInstance();
		try {
			dbutil.reset();
			dbutil.setMode(DBUtil.PREPARED);
			dbutil.setSql("SELECT FN_GETCDT(?) FROM DUAL");
			dbutil.setString(1, processContext.getProcessAction().getActionEntity());
			rs = dbutil.executeQuery();
			if (rs.next()) {
				sys_date_time = rs.getTimestamp(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			logger.logError(" getCurrentTimestamp(1) - " + e.getLocalizedMessage());

		} finally {
			dbutil.reset();
		}
		return sys_date_time;
	}

	public boolean save() {
		boolean saved = false;
		try {
			if (this.mainTable) {
				setAuditDetails();
			}

			if (processContext.isLogRequired() && !processContext.isTbaRequired()) {
				DataBlock_New = getRowData();
				if (this.mainTable) {
					if (!isNew()) {
						if (OldData_Key != null && !OldData_Key.equals("")) {
							processContext.getAuditMap().put(OldData_Key, DataBlock_Old);
							setAuditOperationsToAuditLog(OldDataChar);
						}
					}
					NewData_Key = getTableName() + TableValueSep + NewDataChar + processContext.getTbaPrimaryKey();
					processContext.getAuditMap().put(NewData_Key, DataBlock_New);
				} else if (isDependentTable()) {
					if (!isNew()) {
						if (OldData_Key != null && !OldData_Key.equals("")) {
							// commented by jayashree as on 16/07/2015
							// loadByKeyFields();
							processContext.getAuditMap().put(OldData_Key, DataBlock_Old);
						}
					}
					NewData_Key = getTableName() + TableValueSep + NewDataChar + processContext.getTbaPrimaryKey();
					processContext.getAuditMap().put(NewData_Key, DataBlock_New);
				} else {
					if (isNew()) {
						if (getField(keyFields[keyFields.length - 1]) != null) {
							NewData_Key = getTableName() + TableValueSep + NewDataChar + processContext.getTbaPrimaryKey() + TableValueSep + getField(keyFields[keyFields.length - 1]);
						} else {
							NewData_Key = getTableName() + TableValueSep + NewDataChar + processContext.getTbaPrimaryKey();
						}
						processContext.getAuditMap().put(NewData_Key, DataBlock_New);
					}
				}
			}

			/* ARIBALA CHANGED FOR OPERATIONS LOG BEG */
			// changed by swaroopa as on 29-04-2015 starts
			if (processContext.getProgramType().equals(AbstractionType)) {
				if (mainTable) {
					if (processContext.isTbaRequired()) {
						saved = insertTBAData();
						if (!isNew()) {
							updateTBAKey();
						}
					} else {
						if (isNew()) {
							saved = insert();
						} else {
							saved = update();

						}

					}
				} else {
					if (processContext.isTbaRequired()) {
						saved = insertTBAData();
					} else {
						if (isNew()) {
							saved = insert();
						} else {
							saved = update();

						}
					}
				}
			} else if (processContext.getProgramType().equals(MechanismType)) {
				if (mainTable) {
					if (isNew()) {
						saved = insert();
						if (!processContext.isTbaRequired()) {
							updateProcessAuthRejFlag();
						}
					} else {
						if (!processContext.isTbaRequired()) {
							saved = update();
							updateProcessAuthRejFlag();
						} else {
							if (processContext.getTransitChoice().equals(TRANSIT_PREVENT_USAGE)) {
								// add the old image to TBA Table
								moveOldImageToTBA();
								// move the new image to source table
								saved = update();
							} else if (processContext.getTransitChoice().equals(TRANSIT_USE_EXISTING)) {
								// add the new image to TBA table
								saved = insertTBAData();
							}
							updateTBAKey();
						}
					}

				} /*
				 * else if (isDependentTable()) { if (isNew()) { saved = insert(); if (!processContext.isTbaRequired())
				 * { updateProcessAuthRejFlag(); } } else { if (!processContext.isTbaRequired()) { saved = update();
				 * updateProcessAuthRejFlag(); } else { if (processContext.getTransitChoice
				 * ().equals(TRANSIT_PREVENT_USAGE)) { // add the old image to TBA Table moveOldImageToTBA(); // move
				 * the new image to source table saved = update(); } else if (processContext.getTransitChoice
				 * ().equals(TRANSIT_USE_EXISTING)) { // add the new image to TBA table saved = insertTBAData(); } } }
				 * 
				 * }
				 */else {
					if (!processContext.isTbaRequired()) {
						if (isNew()) {
							saved = insert();
						} else {
							saved = update();

						}
					} else {
						if (processContext.getProcessAction().getActionType().equals(TBAActionType.ADD)) {
							if (isNew()) {
								saved = insert();
							}
						} else {
							if (processContext.getTransitChoice().equals(TRANSIT_PREVENT_USAGE)) {
								// add the old image to TBA Table
								moveOldImageToTBA();
								// move the new image to table
								saved = insert();
							} else if (processContext.getTransitChoice().equals(TRANSIT_USE_EXISTING)) {
								// add the new image to TBA table
								saved = insertTBAData();
							}
						}

					}
				}
			}
			OldData_Key = "";
			DataBlock_Old = "";
			DataBlock_New = "";
			NewData_Key = "";
			// update audit operations
			if (mainTable) {
				if (!processContext.isTbaRequired()) {
					if (isNew()) {
						updateAuditOperations(ENTRY_MODE);
					} else {
						updateAuditOperations(MODIFY_MODE);

					}
					updateAuditOperations(AUTHORIZE);
					setAuditOperationsToAuditLog(NewDataChar);
				}
			}
			// changed by swaroopa as on 29-04-2015 ends

		} catch (Exception e) {
			e.printStackTrace();
			logger.logError(" save(1) - " + e.getLocalizedMessage());

		}
		return saved;
	}

	/* added by swaroopa as on 02-06-2015 starts */
	public void sendInterfaceOutwardMessage() {
		/* ARIBALA ADDED FOR JAVA TO COBOL INTERFACE - END */
		if (!processContext.isTbaRequired() && outwardMessageGenerationReqd()) {
			OutwardMessageProcessor messageProcessor = new OutwardMessageProcessor();
			DTObject input = new DTObject();
			input.set("ENTITY_CODE", (String) getField("ENTITY_CODE"));
			input.set("PGM_ID", processContext.getProcessID());
			input.set("PRIMARY_KEY", processContext.getTbaPrimaryKey());
			input.set("ACTION", isNew() ? "W" : "R");
			input.setObject("DBCONTEXT", dbContext);
			messageProcessor.process(input);
		}
		/* ARIBALA ADDED FOR JAVA TO COBOL INTERFACE - END */
	}

	/* added by swaroopa as on 02-06-2015 ends */

	public boolean saveInventory() {
		boolean saved = false;
		try {
			saved = insertInventoryData();
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError(" save(1) - " + e.getLocalizedMessage());
		}
		return saved;
	}

	private boolean insertInventoryData() {
		boolean saved = false;
		DBUtil dbUtil = dbContext.createUtilInstance();
		try {
			String deleteSql = "DELETE FROM PGMLINKEDTBA WHERE ENTITY_CODE=? AND PARENT_PGM_ID=? AND CONVERSATION_ID=? AND LINKED_PGM_ID=? AND LINKED_PGM_PK=? AND TABLE_NAME=?";
			dbUtil.setMode(DBUtil.PREPARED);
			dbUtil.setSql(deleteSql);
			dbUtil.setString(1, (String) getField("ENTITY_CODE"));
			dbUtil.setString(2, (String) getField("PARENT_PGM_ID"));
			dbUtil.setString(3, (String) getField("CONVERSATION_ID"));
			dbUtil.setString(4, (String) getField("LINKED_PGM_ID"));
			dbUtil.setString(5, (String) getField("LINKED_PGM_PK"));
			dbUtil.setString(6, (String) getField("TABLE_NAME"));
			dbUtil.executeUpdate();

			dbUtil.reset();
			String insertSql = "INSERT INTO PGMLINKEDTBA VALUES (?,?,?,?,?,?,?,?,?,?,?)";
			dbUtil.setMode(DBUtil.PREPARED);
			dbUtil.setSql(insertSql);
			dbUtil.setString(1, (String) getField("ENTITY_CODE"));
			dbUtil.setString(2, (String) getField("PARENT_PGM_ID"));
			dbUtil.setString(3, (String) getField("CONVERSATION_ID"));
			dbUtil.setString(4, (String) getField("LINKED_PGM_ID"));
			dbUtil.setString(5, (String) getField("LINKED_PGM_PK"));
			dbUtil.setString(6, (String) getField("TABLE_NAME"));
			dbUtil.setLong(7, blockSerial);
			blockSerial++;
			dbUtil.setString(8, (String) getField("PARENT_PGM_PK"));
			dbUtil.setString(9, (String) getField("OPERATION_TYPE"));
			dbUtil.setString(10, getRowData());
			dbUtil.setString(11, (String) getField("TABLE_TYPE"));
			dbUtil.executeUpdate();
			saved = true;
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError(" insertInventoryData(1) - " + e.getLocalizedMessage());
			errorMessage = e.getLocalizedMessage();
		} catch (Throwable e) {
			e.printStackTrace();
			logger.logError(" insertInventoryData(2) - " + e.getLocalizedMessage());
			errorMessage = e.getLocalizedMessage();
		} finally {
			dbUtil.reset();
		}
		return saved;
	}

	public boolean deleteInventoryData() {
		boolean saved = false;
		DBUtil dbUtil = dbContext.createUtilInstance();
		try {
			String deleteSql = "DELETE FROM PGMLINKEDTBA WHERE ENTITY_CODE=? AND PARENT_PGM_ID=? AND PARENT_PGM_PK=? AND LINKED_PGM_ID=? AND LINKED_PGM_PK=? AND TABLE_NAME=?";
			dbUtil.setMode(DBUtil.PREPARED);
			dbUtil.setSql(deleteSql);
			dbUtil.setString(1, (String) getField("ENTITY_CODE"));
			dbUtil.setString(2, (String) getField("PARENT_PGM_ID"));
			dbUtil.setString(3, (String) getField("PARENT_PGM_PK"));
			dbUtil.setString(4, (String) getField("LINKED_PGM_ID"));
			dbUtil.setString(5, (String) getField("LINKED_PGM_PK"));
			dbUtil.setString(6, (String) getField("TABLE_NAME"));
			dbUtil.executeUpdate();
			saved = true;
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError(" insertInventoryData(1) - " + e.getLocalizedMessage());
			errorMessage = e.getLocalizedMessage();
		} catch (Throwable e) {
			e.printStackTrace();
			logger.logError(" insertInventoryData(2) - " + e.getLocalizedMessage());
			errorMessage = e.getLocalizedMessage();
		} finally {
			dbUtil.reset();
		}
		return saved;
	}

	private void updateProcessAuthRejFlag() {
		sqlQuery = new StringBuffer();
		sqlQuery.append("UPDATE ").append(tableName).append(" SET ");
		sqlQuery.append("E_STATUS").append(" = ?  , AUTH_ON=FN_GETCDT(?) ");
		sqlQuery.trimToSize();
		if (keyFields.length > 0)
			sqlQuery.append(" WHERE ");
		int i = 0;
		for (String fieldName : keyFields) {
			if (i < keyFields.length - 1) {
				sqlQuery.append(fieldName).append(" = ? AND ");
			} else {
				sqlQuery.append(fieldName).append(" = ?");
			}
			++i;
		}
		DBUtil util = dbContext.createUtilInstance();
		try {
			util.reset();
			util.setSql(sqlQuery.toString());
			i = 1;
			util.setString(i, AUTHORIZE);
			util.setString(++i, processContext.getActionEntity());
			int k = 0;
			++i;
			for (BindParameterType fieldType : keyFieldTypes) {
				Object value = null;
				switch (fieldType) {
				case LONG:
					value = getField(keyFields[k]);
					if (value == null)
						util.setNull(i, Types.NUMERIC);
					else if (value instanceof Long)
						util.setLong(i, (Long) value);
					else
						util.setString(i, (String) value);
					break;
				case VARCHAR:
					value = (String) getField(keyFields[k]);
					if (value == null)
						util.setNull(i, Types.VARCHAR);
					else
						util.setString(i, (String) value);
					break;
				case DATE:
					value = getField(keyFields[k]);
					if (value == null)
						util.setNull(i, Types.DATE);
					else if (value instanceof Timestamp)
						util.setTimestamp(i, (Timestamp) value);
					else if (value instanceof java.sql.Date)
						util.setDate(i, (Date) value);
					else if (value instanceof java.util.Date)
						util.setDate(i, new java.sql.Date(((java.util.Date) getField(keyFields[k])).getTime()));
					else
						util.setString(i, (String) value);
					break;
				case INTEGER:
					value = getField(keyFields[k]);
					if (value == null)
						util.setNull(i, Types.INTEGER);
					else if (value instanceof Integer)
						util.setInt(i, (Integer) value);
					else if (value instanceof BigDecimal)
						util.setBigDecimal(i, (BigDecimal) value);
					else
						util.setString(i, (String) value);
					break;
				case TIMESTAMP:
					value = getField(keyFields[k]);
					if (value == null)
						util.setNull(i, Types.TIMESTAMP);
					else if (value instanceof Timestamp)
						util.setTimestamp(i, (Timestamp) value);
					else
						util.setString(i, (String) value);
					break;
				case DECIMAL:
					value = getField(keyFields[k]);
					if (value == null)
						util.setNull(i, Types.DECIMAL);
					else if (value instanceof BigDecimal)
						util.setBigDecimal(i, (BigDecimal) value);
					else
						util.setString(i, (String) value);
					break;
				case BIGINT:
					value = getField(keyFields[k]);
					if (value == null)
						util.setNull(i, Types.DECIMAL);
					else if (value instanceof Long)
						util.setLong(i, (Long) value);
					else
						util.setString(i, (String) value);
					break;
				}
				++k;
				++i;
			}
			util.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			logger.logError(" update(1) - " + e.getLocalizedMessage());
			errorMessage = e.getLocalizedMessage();
		} finally {
			util.reset();
		}

	}

	private void updateTBAKey() {
		sqlQuery = new StringBuffer();
		sqlQuery.append("UPDATE ").append(tableName).append(" SET ");
		sqlQuery.append("TBA_KEY").append(" = ? ");
		sqlQuery.trimToSize();
		if (keyFields.length > 0)
			sqlQuery.append(" WHERE ");
		int i = 0;
		for (String fieldName : keyFields) {
			if (i < keyFields.length - 1) {
				sqlQuery.append(fieldName).append(" = ? AND ");
			} else {
				sqlQuery.append(fieldName).append(" = ?");
			}
			++i;
		}
		DBUtil util = dbContext.createUtilInstance();
		try {
			util.reset();
			util.setSql(sqlQuery.toString());
			i = 1;
			util.setString(i, processContext.getDisplayDetails());
			int k = 0;
			++i;
			for (BindParameterType fieldType : keyFieldTypes) {
				Object value = null;
				switch (fieldType) {
				case LONG:
					value = getField(keyFields[k]);
					if (value == null)
						util.setNull(i, Types.NUMERIC);
					else if (value instanceof Long)
						util.setLong(i, (Long) value);
					else
						util.setString(i, (String) value);
					break;
				case VARCHAR:
					value = (String) getField(keyFields[k]);
					if (value == null)
						util.setNull(i, Types.VARCHAR);
					else
						util.setString(i, (String) value);
					break;
				case DATE:
					value = getField(keyFields[k]);
					if (value == null)
						util.setNull(i, Types.DATE);
					else if (value instanceof Timestamp)
						util.setTimestamp(i, (Timestamp) value);
					else if (value instanceof java.sql.Date)
						util.setDate(i, (Date) value);
					else if (value instanceof java.util.Date)
						util.setDate(i, new java.sql.Date(((java.util.Date) getField(keyFields[k])).getTime()));
					else
						util.setString(i, (String) value);
					break;
				case INTEGER:
					value = getField(keyFields[k]);
					if (value == null)
						util.setNull(i, Types.INTEGER);
					else if (value instanceof Integer)
						util.setInt(i, (Integer) value);
					else if (value instanceof BigDecimal)
						util.setBigDecimal(i, (BigDecimal) value);
					else
						util.setString(i, (String) value);
					break;
				case TIMESTAMP:
					value = getField(keyFields[k]);
					if (value == null)
						util.setNull(i, Types.TIMESTAMP);
					else if (value instanceof Timestamp)
						util.setTimestamp(i, (Timestamp) value);
					else
						util.setString(i, (String) value);
					break;
				case DECIMAL:
					value = getField(keyFields[k]);
					if (value == null)
						util.setNull(i, Types.DECIMAL);
					else if (value instanceof BigDecimal)
						util.setBigDecimal(i, (BigDecimal) value);
					else
						util.setString(i, (String) value);
					break;
				case BIGINT:
					value = getField(keyFields[k]);
					if (value == null)
						util.setNull(i, Types.DECIMAL);
					else if (value instanceof Long)
						util.setLong(i, (Long) value);
					else
						util.setString(i, (String) value);
					break;
				}
				++k;
				++i;
			}
			util.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			logger.logError(" update(1) - " + e.getLocalizedMessage());
			errorMessage = e.getLocalizedMessage();
		} finally {
			util.reset();
		}

	}

	private void releasePickupAccess() {
		DBUtil dbutil = dbContext.createUtilInstance();
		try {
			dbutil.reset();
			dbutil.setMode(DBUtil.CALLABLE);
			dbutil.setSql("CALL SP_RELEASE_PICKUP(?,?,?,?,?)");
			dbutil.setString(1, processContext.getProcessAction().getActionEntity());
			dbutil.setString(2, this.tableName);
			dbutil.setString(3, processContext.getProcessAction().getProcessID());
			dbutil.setString(4, processContext.getTbaPrimaryKey());
			dbutil.registerOutParameter(5, Types.VARCHAR);
			dbutil.execute();
			String status = dbutil.getString(5);
			if (status == null || !status.equals(RegularConstants.SP_SUCCESS))
				errorMessage = status;
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError(" releasePickupAccess(1) - " + e.getLocalizedMessage());
			errorMessage = e.getLocalizedMessage();
		} catch (Throwable e) {
			e.printStackTrace();
			logger.logError(" releasePickupAccess(2) - " + e.getLocalizedMessage());
			errorMessage = e.getLocalizedMessage();
		} finally {
			dbutil.reset();
		}
	}

	private void updateAuditOperations(String operationType) {
		DBUtil dbutil = dbContext.createUtilInstance();
		try {

			dbutil.reset();
			dbutil.setMode(DBUtil.CALLABLE);
			dbutil.setSql("CALL SP_UPDATE_AUDITFIELDS(?,?,?,?,?,?,?,?,?,?,?)");
			dbutil.setString(1, processContext.getProcessAction().getActionEntity());
			dbutil.setString(2, processContext.getProcessID());
			dbutil.setString(3, processContext.getTbaPrimaryKey());
			dbutil.setString(4, operationType);
			dbutil.setString(5, processContext.getProcessAction().getActionUser());
			dbutil.setString(6, getCurrentTimestamp().toString());
			dbutil.setString(7, "0");
			dbutil.setDate(8, null);
			dbutil.setInt(9, 0);
			dbutil.setString(10, getPartitionYear());
			dbutil.registerOutParameter(11, Types.VARCHAR);
			dbutil.execute();
			String status = dbutil.getString(11);
			errorMessage = status;

		} catch (Exception e) {
			e.printStackTrace();
			logger.logError(" updateOperations(1) - " + e.getLocalizedMessage());
			errorMessage = e.getLocalizedMessage();
		} catch (Throwable e) {
			e.printStackTrace();
			logger.logError(" updateOperations(2) - " + e.getLocalizedMessage());

			errorMessage = e.getLocalizedMessage();
		} finally {
			dbutil.reset();
		}
	}

	private void setAuditOperationsToAuditLog(String imageType) {
		StringBuffer buffer = new StringBuffer();
		buffer.append("<ROWS>");
		DBUtil dbutil = dbContext.createUtilInstance();
		try {
			dbutil.reset();
			dbutil.setMode(DBUtil.CALLABLE);
			dbutil.setSql("SELECT * FROM FWRKOPERATIONS WHERE ENTITY_CODE=? AND FORM_NAME=? AND FORM_PK=?");
			dbutil.setString(1, processContext.getProcessAction().getActionEntity());
			dbutil.setString(2, processContext.getProcessID());
			dbutil.setString(3, processContext.getTbaPrimaryKey());
			ResultSet rs = dbutil.executeQuery();
			int columnCount = rs.getMetaData().getColumnCount();
			while (rs.next()) {
				buffer.append("<ROW>");
				for (int i = 1; i <= columnCount; i++) {
					if (rs.getMetaData().getColumnTypeName(i).equals("DATE")) {
						Date value = rs.getDate(i);
						if (value != null) {
							buffer.append("<").append(rs.getMetaData().getColumnName(i)).append("><![CDATA[").append(BackOfficeFormatUtils.getDate((java.util.Date) value, BackOfficeConstants.TBA_XML_DATE_FORMAT)).append("]]></").append(rs.getMetaData().getColumnName(i)).append(">");
						} else {
							buffer.append("<").append(rs.getMetaData().getColumnName(i)).append(" N=\"N\" />");
						}
					} else {
						String value = rs.getString(i);
						if (value == null) {
							buffer.append("<").append(rs.getMetaData().getColumnName(i)).append(" N=\"N\" />");
						} else {
							buffer.append("<").append(rs.getMetaData().getColumnName(i)).append("><![CDATA[").append(value).append("]]></").append(rs.getMetaData().getColumnName(i)).append(">");
						}
					}
				}
				buffer.append("</ROW>");

			}
			buffer.append("</ROWS>");
			buffer.trimToSize();
			String auditKey = "";
			auditKey = "FWRKOPERATIONS" + TableValueSep + imageType + processContext.getTbaPrimaryKey();
			processContext.getAuditMap().put(auditKey, buffer.toString());

		} catch (Exception e) {
			e.printStackTrace();
			logger.logError(" updateOperations(1) - " + e.getLocalizedMessage());
			errorMessage = e.getLocalizedMessage();
		}
	}

	/* ARIBALA CHANGED FOR OPERATIONS LOG END */

	private void setAuditDetails() {
		if (this.mainTable) {
			Timestamp currentTimestamp = processContext.getProcessActionDateTime();
			if (isNew()) {
				setField("CR_BY", processContext.getProcessAction().getActionUser());
				setField("CR_ON", currentTimestamp);
				setField("MO_BY", null);
				setField("MO_ON", null);
			} else {
				setField("MO_BY", processContext.getProcessAction().getActionUser());
				setField("MO_ON", currentTimestamp);
			}
			if (processContext.isTbaRequired()) {
				setField("AU_BY", null);
				setField("AU_ON", null);
			} else {
				setField("AU_BY", processContext.getProcessAction().getActionUser());
				setField("AU_ON", currentTimestamp);
			}
		}
	}

	private boolean insertTBAData() {
		boolean saved = false;
		DBUtil dbutil = dbContext.createUtilInstance();
		int _dirtyCount = 0;
		try {
			StringBuffer _sql = new StringBuffer("INSERT INTO TBAAUTHDTL  VALUES (?,?,?,?,?,?,?,?,?) ");
			dbutil.setSql(_sql.toString());
			dbutil.setString(++_dirtyCount, processContext.getProcessAction().getActionEntity());
			dbutil.setString(++_dirtyCount, processContext.getProcessID());
			dbutil.setString(++_dirtyCount, processContext.getTbaPrimaryKey());
			dbutil.setDate(++_dirtyCount, processContext.getTbaActionDate());
			dbutil.setDouble(++_dirtyCount, processContext.getTbaSerial());
			dbutil.setString(++_dirtyCount, this.tableName);
			dbutil.setLong(++_dirtyCount, blockSerial);
			blockSerial++;
			dbutil.setString(++_dirtyCount, getRowData());
			if (this.mainTable || this.dependentTable)
				dbutil.setString(++_dirtyCount, "S");
			else
				dbutil.setString(++_dirtyCount, "M");
			dbutil.executeUpdate();
			saved = true;
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError(" insertTBAData(1) - " + e.getLocalizedMessage());
			errorMessage = e.getLocalizedMessage();
		} catch (Throwable e) {
			e.printStackTrace();
			logger.logError(" insertTBAData(2) - " + e.getLocalizedMessage());
			errorMessage = e.getLocalizedMessage();
		} finally {
			dbutil.reset();
		}

		return saved;

	}

	private boolean moveOldImageToTBA() {
		boolean saved = false;
		DBUtil dbutil = dbContext.createUtilInstance();
		int _dirtyCount = 0;
		try {
			StringBuffer _sql = new StringBuffer("INSERT INTO TBAAUTHDTL  VALUES (?,?,?,?,?,?,?,?,?) ");
			dbutil.setSql(_sql.toString());
			dbutil.setString(++_dirtyCount, processContext.getProcessAction().getActionEntity());
			dbutil.setString(++_dirtyCount, processContext.getProcessID());
			dbutil.setString(++_dirtyCount, processContext.getTbaPrimaryKey());
			dbutil.setDate(++_dirtyCount, processContext.getTbaActionDate());
			dbutil.setDouble(++_dirtyCount, processContext.getTbaSerial());
			dbutil.setString(++_dirtyCount, this.tableName);
			dbutil.setLong(++_dirtyCount, blockSerial);
			blockSerial++;
			dbutil.setString(++_dirtyCount, DataBlock_Old);
			if (this.mainTable || this.dependentTable) {
				dbutil.setString(++_dirtyCount, "S");
			} else {
				dbutil.setString(++_dirtyCount, "M");
			}
			dbutil.executeUpdate();
			saved = true;
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError(" insertTBAData(1) - " + e.getLocalizedMessage());
			errorMessage = e.getLocalizedMessage();
		} catch (Throwable e) {
			e.printStackTrace();
			logger.logError(" insertTBAData(2) - " + e.getLocalizedMessage());
			errorMessage = e.getLocalizedMessage();
		} finally {
			dbutil.reset();
		}

		return saved;

	}

	private boolean update() {
		sqlQuery = new StringBuffer();
		boolean saved = false;
		Set<String> noUpdationTableInfo = tableInfo.getNoUpdationColumnInfo();
		sqlQuery.append("UPDATE ").append(tableName).append(" SET ");
		for (String fieldName : fields) {
			if (!noUpdationTableInfo.contains(fieldName)) {
				if (internalStore.containsKey(fieldName)) {
					sqlQuery.append(fieldName).append(" = ? ,");
				}
			}
		}
		sqlQuery.trimToSize();
		sqlQuery = new StringBuffer(sqlQuery.substring(0, sqlQuery.length() - 1));
		if (keyFields.length > 0)
			sqlQuery.append(" WHERE ");
		int i = 0;
		for (String fieldName : keyFields) {
			if (i < keyFields.length - 1) {
				sqlQuery.append(fieldName).append(" = ? AND ");
			} else {
				sqlQuery.append(fieldName).append(" = ?");
			}
			++i;
		}
		int count = 0;
		DBUtil util = dbContext.createUtilInstance();
		try {
			util.reset();
			util.setSql(sqlQuery.toString());
			i = 0;
			Map<String, BindParameterType> columnType = tableInfo.getColumnInfo().getColumnMapping();
			for (String fieldName : fields) {
				if (!noUpdationTableInfo.contains(fieldName)) {
					if (internalStore.containsKey(fieldName)) {
						++i;
						Object value = null;
						switch (columnType.get(fieldName)) {
						case LONG:
							value = getField(fieldName);
							if (value == null)
								util.setNull(i, Types.NUMERIC);
							else if (value instanceof Long)
								util.setLong(i, (Long) value);
							else
								util.setString(i, (String) value);
							break;
						case VARCHAR:
							value = getField(fieldName);
							if (value == null)
								util.setNull(i, Types.VARCHAR);
							else
								util.setString(i, (String) value);
							break;
						case DATE:
							value = getField(fieldName);
							if (value == null)
								util.setNull(i, Types.DATE);
							else if (value instanceof Timestamp)
								util.setTimestamp(i, (Timestamp) value);
							else if (value instanceof java.sql.Date)
								util.setDate(i, (Date) value);
							else if (value instanceof java.util.Date)
								util.setDate(i, new java.sql.Date(((java.util.Date) getField(fieldName)).getTime()));
							else
								util.setString(i, (String) value);
							break;
						case INTEGER:
							value = getField(fieldName);
							if (value == null)
								util.setNull(i, Types.INTEGER);
							else if (value instanceof Integer)

								util.setInt(i, (Integer) value);
							else if (value instanceof BigDecimal)
								util.setBigDecimal(i, (BigDecimal) value);
							else
								util.setString(i, (String) value);
							break;
						case TIMESTAMP:
							value = getField(fieldName);
							if (value == null)
								util.setNull(i, Types.TIMESTAMP);
							else if (value instanceof Timestamp)
								util.setTimestamp(i, (Timestamp) value);
							else
								util.setString(i, (String) value);
							break;
						case DECIMAL:
							value = getField(fieldName);
							if (value == null)
								util.setNull(i, Types.DECIMAL);
							else if (value instanceof BigDecimal)
								util.setBigDecimal(i, (BigDecimal) value);
							else
								util.setString(i, (String) value);
							break;
						case BIGINT:
							value = getField(fieldName);
							if (value == null)
								util.setNull(i, Types.DECIMAL);
							else if (value instanceof Long)
								util.setLong(i, (Long) value);
							else
								util.setString(i, (String) value);
							break;
						}
					}
				}
			}
			int k = 0;
			++i;
			for (BindParameterType fieldType : keyFieldTypes) {
				Object value = null;
				switch (fieldType) {
				case LONG:
					value = getField(keyFields[k]);
					if (value == null)
						util.setNull(i, Types.NUMERIC);
					else if (value instanceof Long)
						util.setLong(i, (Long) value);
					else
						util.setString(i, (String) value);
					break;
				case VARCHAR:
					value = (String) getField(keyFields[k]);
					if (value == null)
						util.setNull(i, Types.VARCHAR);
					else
						util.setString(i, (String) value);
					break;
				case DATE:
					value = getField(keyFields[k]);
					if (value == null)
						util.setNull(i, Types.DATE);
					else if (value instanceof Timestamp)
						util.setTimestamp(i, (Timestamp) value);
					else if (value instanceof java.sql.Date)
						util.setDate(i, (Date) value);
					else if (value instanceof java.util.Date)
						util.setDate(i, new java.sql.Date(((java.util.Date) getField(keyFields[k])).getTime()));
					else
						util.setString(i, (String) value);
					break;
				case INTEGER:
					value = getField(keyFields[k]);
					if (value == null)
						util.setNull(i, Types.INTEGER);
					else if (value instanceof Integer)
						util.setInt(i, (Integer) value);
					else if (value instanceof BigDecimal)
						util.setBigDecimal(i, (BigDecimal) value);
					else
						util.setString(i, (String) value);
					break;
				case TIMESTAMP:
					value = getField(keyFields[k]);
					if (value == null)
						util.setNull(i, Types.TIMESTAMP);
					else if (value instanceof Timestamp)
						util.setTimestamp(i, (Timestamp) value);
					else
						util.setString(i, (String) value);
					break;
				case DECIMAL:
					value = getField(keyFields[k]);
					if (value == null)
						util.setNull(i, Types.DECIMAL);
					else if (value instanceof BigDecimal)
						util.setBigDecimal(i, (BigDecimal) value);
					else
						util.setString(i, (String) value);
					break;
				case BIGINT:
					value = getField(keyFields[k]);
					if (value == null)
						util.setNull(i, Types.DECIMAL);
					else if (value instanceof Long)
						util.setLong(i, (Long) value);
					else
						util.setString(i, (String) value);
					break;
				}
				++k;
				++i;
			}
			count = util.executeUpdate();
			if (count > 0)
				saved = true;
		} catch (SQLException e) {
			e.printStackTrace();
			logger.logError(" update(1) - " + e.getLocalizedMessage());
			errorMessage = e.getLocalizedMessage();
		} finally {
			util.reset();
		}
		return saved;
	}

	public boolean insert() {
		sqlQuery = new StringBuffer();
		Set<String> noUpdationTableInfo = tableInfo.getNoUpdationColumnInfo();
		StringBuffer buffer = new StringBuffer();
		boolean saved = false;
		sqlQuery.append("INSERT INTO ").append(tableName).append(" ( ");
		for (String fieldName : fields) {
			if (!noUpdationTableInfo.contains(fieldName)) {
				if (internalStore.containsKey(fieldName)) {
					sqlQuery.append(fieldName).append(",");
					buffer.append("?").append(",");
				}
			}
		}
		buffer.trimToSize();
		buffer = new StringBuffer(buffer.substring(0, buffer.length() - 1));
		sqlQuery.trimToSize();
		sqlQuery = new StringBuffer(sqlQuery.substring(0, sqlQuery.length() - 1));
		sqlQuery.append(") VALUES(");
		sqlQuery.append(buffer.toString());
		sqlQuery.append(")");

		int count = 0;
		DBUtil util = dbContext.createUtilInstance();
		try {
			util.reset();
			util.setSql(sqlQuery.toString());
			int i = 0;
			Map<String, BindParameterType> columnType = tableInfo.getColumnInfo().getColumnMapping();
			for (String fieldName : fields) {
				if (!noUpdationTableInfo.contains(fieldName)) {
					if (internalStore.containsKey(fieldName)) {
						++i;
						Object value = null;
						switch (columnType.get(fieldName)) {
						case LONG:
							value = getField(fieldName);
							if (value == null)
								util.setNull(i, Types.NUMERIC);
							else if (value instanceof Long)
								util.setLong(i, (Long) value);
							else
								util.setString(i, (String) value);
							break;
						case VARCHAR:
							value = getField(fieldName);
							if (value == null)
								util.setNull(i, Types.VARCHAR);
							else
								util.setString(i, (String) value);
							break;
						case DATE:
							value = getField(fieldName);
							if (value == null)
								util.setNull(i, Types.DATE);
							else if (value instanceof Timestamp)
								util.setTimestamp(i, (Timestamp) value);
							else if (value instanceof java.sql.Date)
								util.setDate(i, (Date) value);
							else if (value instanceof java.util.Date)
								util.setDate(i, new java.sql.Date(((java.util.Date) getField(fieldName)).getTime()));
							else
								util.setString(i, (String) value);
							break;
						case INTEGER:
							value = getField(fieldName);
							if (value == null)
								util.setNull(i, Types.INTEGER);
							else if (value instanceof Integer)
								util.setInt(i, (Integer) value);
							else if (value instanceof BigDecimal)
								util.setBigDecimal(i, (BigDecimal) value);
							else
								util.setString(i, (String) value);
							break;
						case TIMESTAMP:
							value = getField(fieldName);
							if (value == null)
								util.setNull(i, Types.TIMESTAMP);
							else if (value instanceof Timestamp)
								util.setTimestamp(i, (Timestamp) value);
							else
								util.setString(i, (String) value);
							break;
						case DECIMAL:
							value = getField(fieldName);
							if (value == null)
								util.setNull(i, Types.DECIMAL);
							else if (value instanceof BigDecimal)
								util.setBigDecimal(i, (BigDecimal) value);
							else
								util.setString(i, (String) value);
							break;
						case BIGINT:
							value = getField(fieldName);
							if (value == null)
								util.setNull(i, Types.DECIMAL);
							else if (value instanceof Long)
								util.setLong(i, (Long) value);
							else
								util.setString(i, (String) value);
							break;
						}
					}
				}
			}
			count = util.executeUpdate();
			if (count > 0)
				saved = true;
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError(" insert(1) - " + e.getLocalizedMessage());
			errorMessage = e.getLocalizedMessage();
		} finally {
			util.reset();
		}
		return saved;
	}

	public String getRowData() {
		StringBuffer buffer = new StringBuffer();
		int i = 1;
		buffer.append("<ROW>");
		String[] fieldNames = tableInfo.getColumnInfo().getColumnNames();
		Set<String> noUpdationTableInfo = tableInfo.getNoUpdationColumnInfo();
		for (String fieldName : fieldNames) {
			if (!noUpdationTableInfo.contains(fieldName)) {
				Object value = (Object) internalStore.get(fieldName);
				if (value == null)
					buffer.append("<").append(fieldName).append(" N=\"N\" />");
				else {
					if (value instanceof java.util.Date)
						buffer.append("<").append(fieldName).append("><![CDATA[").append(BackOfficeFormatUtils.getDate((java.util.Date) value, BackOfficeConstants.TBA_XML_DATE_FORMAT)).append("]]></").append(fieldName).append(">");
					else
						buffer.append("<").append(fieldName).append("><![CDATA[").append(value).append("]]></").append(fieldName).append(">");
				}
				++i;
			}
		}
		buffer.append("</ROW>");
		buffer.trimToSize();
		return buffer.toString();
	}

	private String getRowOldData() {
		StringBuffer buffer = new StringBuffer();
		int i = 1;
		buffer.append("<ROW>");
		String[] fieldNames = tableInfo.getColumnInfo().getColumnNames();
		Set<String> noUpdationTableInfo = tableInfo.getNoUpdationColumnInfo();
		for (String fieldName : fieldNames) {
			if (!noUpdationTableInfo.contains(fieldName)) {
				Object value = (Object) OldImageStore.get(fieldName);
				if (value == null)
					buffer.append("<").append(fieldName).append(" N=\"N\" />");
				else {
					if (value instanceof java.util.Date)
						buffer.append("<").append(fieldName).append("><![CDATA[").append(BackOfficeFormatUtils.getDate((java.util.Date) value, BackOfficeConstants.TBA_XML_DATE_FORMAT)).append("]]></").append(fieldName).append(">");
					else
						buffer.append("<").append(fieldName).append("><![CDATA[").append(value).append("]]></").append(fieldName).append(">");
				}
				++i;
			}
		}
		buffer.append("</ROW>");
		buffer.trimToSize();
		return buffer.toString();
	}

	@SuppressWarnings("unused")
	private void decodeRow(ResultSet rs) throws SQLException {
		for (int i = 1; i < rs.getMetaData().getColumnCount() + 1; i++) {
			setField(rs.getMetaData().getColumnName(i), rs.getString(i));
		}
	}

	public String getError() {
		return errorMessage;
	}

	public long getTBAReferenceKey(DTObject processData) {

		DBUtil util = dbContext.createUtilInstance();
		long serial = 0;
		String sourceKey = RegularConstants.EMPTY_STRING;
		try {
			Map<String, BindParameterType> columnType = tableInfo.getColumnInfo().getColumnMapping();
			int i = 1;
			for (String fieldName : keyFields) {
				if (i < keyFields.length) {
					i++;
					switch (columnType.get(fieldName)) {
					case LONG:
						sourceKey = sourceKey + RegularConstants.PK_SEPARATOR + ((Long) processData.getObject(fieldName)).toString();
						break;
					case VARCHAR:
						sourceKey = sourceKey + RegularConstants.PK_SEPARATOR + processData.get(fieldName);
						break;
					case DATE:
						Object value = processData.getObject(fieldName);
						if (value instanceof java.sql.Date)
							sourceKey = sourceKey + RegularConstants.PK_SEPARATOR + BackOfficeFormatUtils.getDate((Date) value, BackOfficeConstants.TBA_DATE_FORMAT);
						else if (value instanceof java.util.Date)
							sourceKey = sourceKey + RegularConstants.PK_SEPARATOR + (new java.sql.Date(((java.util.Date) value).getTime())).toString();
						else
							sourceKey = sourceKey + RegularConstants.PK_SEPARATOR + processData.get(fieldName);
						break;
					case INTEGER:
						Object val = processData.getObject(fieldName);
						if (val instanceof Integer)
							sourceKey = sourceKey + RegularConstants.PK_SEPARATOR + ((Integer) val).toString();
						else
							sourceKey = sourceKey + RegularConstants.PK_SEPARATOR + (String) val;
						break;
					case TIMESTAMP:
						sourceKey = sourceKey + RegularConstants.PK_SEPARATOR + ((Timestamp) processData.getObject(fieldName)).toString();
						break;
					case DECIMAL:
						sourceKey = sourceKey + RegularConstants.PK_SEPARATOR + ((BigDecimal) processData.getObject(fieldName)).toString();
						break;
					case BIGINT:
						Object _value = processData.getObject(fieldName);
						if (_value instanceof Long)
							sourceKey = sourceKey + RegularConstants.PK_SEPARATOR + ((Long) _value).toString();
						else
							sourceKey = sourceKey + RegularConstants.PK_SEPARATOR + ((String) _value);
						break;
					}
				}
			}
			sourceKey = sourceKey.substring(1);

			util.reset();
			util.setMode(DBUtil.CALLABLE);
			String sql = "CALL FN_GET_TBA_REFSL(?,?,?)";
			util.setSql(sql);
			util.setString(1, processContext.getProcessAction().getProcessID());
			util.setString(2, sourceKey);
			util.registerOutParameter(3, Types.INTEGER);

			util.execute();

			serial = util.getInt(3);

			return serial;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			util.reset();
		}
		return serial;
	}

	public long getTBAReferenceSerial(DTObject input) {
		String sourceKey = input.get("SOURCE_KEY");
		String programId = input.get("PGM_ID");
		DBUtil util = dbContext.createUtilInstance();
		long serial = 0;

		try {
			util.reset();
			util.setMode(DBUtil.CALLABLE);
			String sql = "CALL FN_GET_TBA_REFSL(?,?,?)";
			util.setSql(sql);
			util.setString(1, programId);
			util.setString(2, sourceKey);
			util.registerOutParameter(3, Types.INTEGER);
			util.execute();
			serial = util.getInt(3);

		} catch (Exception e) {
			e.printStackTrace();

		} finally {
			util.reset();
		}
		return serial;
	}

	// For TBA DE DUPCHECK

	public boolean udateTBADuplicateCheck() {
		if (!processContext.isTbaRequired()) {
			return true;
		}
		String sql = "INSERT INTO TBADEDUPCHECK (PARTITION_NO,PGM_ID,PURPOSE_ID,PGM_PK,COLUMN_VALUES) VALUES (?,?,?,?,?)";
		DBUtil util = dbContext.createUtilInstance();
		try {
			util.setMode(DBUtil.PREPARED);
			util.setSql("SELECT COUNT(1) FROM PGMDEDUPCFG WHERE PGM_ID=? AND PURPOSE_ID=?");
			util.setString(1, processContext.getProcessID());
			util.setString(2, (String) getField(ContentManager.PURPOSE_ID));
			ResultSet rs = util.executeQuery();
			if (rs.next()) {
				if (rs.getInt(1) == 0)
					return false;
			}

			util.setMode(DBUtil.PREPARED);
			util.setSql("DELETE FROM TBADEDUPCHECK WHERE PARTITION_NO=? AND PGM_ID=? AND PURPOSE_ID=? AND PGM_PK=?");
			util.setString(1, processContext.getPartitionNo());
			util.setString(2, processContext.getProcessID());
			util.setString(3, (String) getField(ContentManager.PURPOSE_ID));
			util.setString(4, processContext.getTbaPrimaryKey());
			util.executeUpdate();

			util.reset();
			util.setMode(DBUtil.PREPARED);
			util.setSql(sql);
			util.setString(1, processContext.getPartitionNo());
			util.setString(2, processContext.getProcessID());
			util.setString(3, (String) getField(ContentManager.PURPOSE_ID));
			util.setString(4, processContext.getTbaPrimaryKey());
			util.setString(5, (String) getField(ContentManager.DEDUP_KEY));
			util.executeUpdate();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			util.reset();
		}
		return false;
	}

	private String getPurposeValue() {
		String result = RegularConstants.EMPTY_STRING;
		DBUtil util = dbContext.createUtilInstance();
		String sql = "SELECT COLUMN_NAME FROM PGMDEDUPCFGDTL WHERE ENTITY_CODE=? AND PGM_ID=? AND PURPOSE_ID=?";
		try {
			util.setMode(DBUtil.CALLABLE);
			util.setSql(sql);
			util.setString(1, (String) getField("ENTITY_CODE"));
			util.setString(2, processContext.getProcessID());
			util.setString(3, (String) getField("PURPOSE_ID"));
			ResultSet rset = util.executeQuery();
			while (rset.next()) {
				result = result + getField(rset.getString(1)) + "|";
			}
			if (result.length() > 0) {
				result = result.substring(0, result.length() - 1);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			util.reset();
		}
		return result;
	}

	/* ARIBALA ADDED FOR JAVA TO COBOL INTERFACE - BEGIN */
	private boolean outwardMessageGenerationReqd() {
		DBUtil dbutil = dbContext.createUtilInstance();
		boolean statusFlag = false;
		String sql = "SELECT PROC_CODE FROM JTCMSGCONFIG WHERE ENTITY_CODE=? AND PGM_ID=? AND OPERATION=?";
		try {
			dbutil.reset();
			dbutil.setMode(DBUtil.CALLABLE);
			dbutil.setSql(sql);
			dbutil.setString(1, (String) getField("ENTITY_CODE"));
			dbutil.setString(2, processContext.getProcessID());
			if (isNew())
				dbutil.setString(3, "W");
			else {
				dbutil.setString(3, "R");
			}
			dbutil.executeQuery();
			ResultSet rs = dbutil.executeQuery();
			if (rs.next()) {
				statusFlag = true;
			}
		} catch (Exception e) {

		} finally {
			dbutil.reset();
		}
		return statusFlag;
	}

	/* ARIBALA ADDED FOR JAVA TO COBOL INTERFACE - END */

	public Timestamp getCurrentDate() {
		Timestamp sys_date_time = null;
		ResultSet rs = null;
		DBUtil dbutil = dbContext.createUtilInstance();
		try {
			dbutil.reset();
			dbutil.setMode(DBUtil.PREPARED);
			dbutil.setSql("SELECT FN_GETCD(?) FROM DUAL");
			dbutil.setString(1, processContext.getProcessAction().getActionEntity());
			rs = dbutil.executeQuery();
			if (rs.next()) {
				sys_date_time = rs.getTimestamp(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			logger.logError(" getCurrentTimestamp(1) - " + e.getLocalizedMessage());

		} finally {
			dbutil.reset();
		}
		return sys_date_time;
	}

}