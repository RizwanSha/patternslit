package patterns.config.framework.interfaceref;

public class InterfaceConstants {
	/** REQUEST STANDARD KEYS **/
	public static final String STAGE_ID = "STAGE_ID";
	public static final String EVENT_CODE = "EVENT_CODE";
	public static final String EVENT_ID = "EVENT_ID";
	public static final String NOTFN_DATETIME = "NOTFN_DATETIME";
	public static final String ZIP_FILE_CONTENT = "ZIP_FILE_CONTENT";
	public static final String BANK_CODE = "BANK_CODE";

	public static final String BO_CLASS = "_BO_CLASS";
	public static final String SQL_DATETIME_FORMAT = "YYYYMMDDHH24MISS";
	public static final String JAVA_DATETIME_FORMAT = "yyyyMMddhhmmss";

	public static final String JAVA_DATE_FORMAT = "yyyyMMdd";
	public static final String SQL_DATE_FORMAT = "YYYYMMDD";
	public static final String SRV_REF_NUM = "SRV_REF_NUM";
	public static final String TRAN_AMOUNT = "TRAN_AMOUNT";
	public static final String REQUEST_DATE = "REQUEST_DATE";
	public static final String ENTITY_CODE = "ENTITY_CODE";
	public static final String ACNT_NUMBER = "ACNT_NUMBER";
	public static final String CUSTOMER_CODE = "CUSTOMER_CODE";

	public static final String TOTAL_DB = "TOTAL_DB";
	public static final String TOTAL_CR = "TOTAL_CR";

	/** RESPONSE STANDARD KEYS **/
	public static final String ADDITIONAL_INFO = "ADDITIONAL_INFO";

	/** Interface Standard Values **/
	public static final String VALUETYPE_VALUE = "1";
	public static final String VALUETYPE_VALUELIST = "2";
	public static final String VALUETYPE_PARAM = "3";

	public static final String VALUEDATATYPE_STRING = "1";
	public static final String VALUEDATATYPE_NUMBER = "2";
	public static final String VALUEDATATYPE_DATE = "3";
	public static final String VALUEDATATYPE_AMOUNT = "4";
	public static final String VALUEDATATYPE_TIME = "5";
	public static final String VALUEDATATYPE_DATETIME = "6";

	/** DOMAIN STANDARD KEYS **/
	public static final String ACNT_TITLE = "ACNT_TITLE";
	public static final String OPEN_DATE = "OPEN_DATE";
	public static final String BRANCH_NAME = "BRANCH_NAME";
	public static final String BRANCH_CODE = "BRANCH_CODE";
	public static final String ACNT_STATUS = "ACNT_STATUS";
	public static final String CURRENCY_CODE = "CURRENCY_CODE";
	public static final String PRODUCT_CODE = "PRODUCT_CODE";
	public static final String ACNT_TYPE = "ACNT_TYPE";
	public static final String ACNT_SUB_TYPE = "ACNT_SUB_TYPE";
	public static final String CLEAR_BAL = "CLEAR_BAL";
	public static final String UNCLEAR_BAL = "UNCLEAR_BAL";
	public static final String LEDGER_BAL = "LEDGER_BAL";
	public static final String AVAILABLE_BAL = "AVAILABLE_BAL";
	public static final String SUMMARY_TYPE = "SUMMARY_TYPE";

	public static final String INTEREST_TYPE = "INTEREST_TYPE";
	public static final String STD_INT_RATE = "STD_INT_RATE";
	public static final String DIFF_INT_RATE = "DIFF_INT_RATE";
	public static final String ACT_INTEREST = "ACT_INTEREST";
	public static final String CONT_DETAILS = "CONT_DETAILS";

	public static final String CONTRACT_NUM = "CONTRACT_NUM";
	public static final String SECURITY_CODE = "SECURITY_CODE";
	public static final String START_POS = "START_POS";
	public static final String REC_COUNT = "REC_COUNT";
	public static final String TEMP_SL = "TEMP_SL";
	public static final String DOWNLOAD_ALLOWED = "DOWNLOAD_ALLOWED";
	public static final String DOWNLOAD_FORMAT = "DOWNLOAD_FORMAT";

	public static final String BAL_LEVEL = "BAL_LEVEL";
	public static final String GRP_CRITERIA = "GRP_CRITERIA";
	public static final String BAL_CRITERIA = "BAL_CRITERIA";
	public static final String BAL_TYPE = "BAL_TYPE";
	public static final String CUST_GRP_CODE = "CUST_GRP_CODE";
	public static final String AS_ON_DATE = "AS_ON_DATE";
	public static final String TOTAL_BAL = "TOTAL_BAL";
	public static final String TOTAL_BAL_EX_LOAN = "TOTAL_BAL_EX_LOAN";
	public static final String TRAN_REF_NO = "TRAN_REF_NO";

	public static final String GRID_XML = "GRID_XML";
	public static final String ADDITIONAL_XML = "ADDITIONAL_XML";
	public static final String DOWNLOAD_DATA = "DOWNLOAD_DATA";

	public static final String CLASS_NAME = "CLASS_NAME";
	public static final String METHOD_NAME = "METHOD_NAME";
	public static final String PARAM_LIST = "PARAM_LIST";
	public static final String ERROR_CODE = "ERROR_CODE";
	public static final String ERROR_MESSAGE = "ERROR_MESSAGE";
	public static final String TOKEN_KEY = "TOKEN_KEY";
	public static final String SEARCH_TEXT = "SEARCH_TEXT";
	public static final String ARGUMENT_KEY = "ARGUMENT_KEY";
	public static final String SEARCH_COLUMN = "SEARCH_COLUMN";
	public static final String INIT_KEY = "INIT_KEY";
	public static final String PARAMS = "PARAMS";
	public static final String DEFAULT_BUFFER = "DEFAULT_BUFFER";
	public static final String TRANS_CHOICE = "TRANS_CHOICE";

	public static final String TRANSFER_DATE = "TRANSFER_DATE";
	public static final String TRANSFER_SERIAL = "TRANSFER_SERIAL";
	public static final String TRANSFER_CHOICE = "TRANSFER_CHOICE";
	public static final String DEAL_REF_NUM = "DEAL_REF_NUM";
	public static final String DEAL_AMT = "DEAL_AMT";
	public static final String TOTAL_TRF_VALUE = "TOTAL_TRF_VALUE";
	public static final String TRF_EFF_VALUE = "TRF_EFF_VALUE";

	public static final String OPEN_BAL = "OPEN_BAL";
	public static final String CLOSE_BAL = "CLOSE_BAL";
	public static final String USER_ID = "USER_ID";
	public static final String SERVICE_CLASS = "SERVICE_CLASS";
	public static final String SERVICE_ID = "SERVICE_ID";
	public static final String LOGIN_DATETIME = "LOGIN_DATETIME";
	public static final String IP_ADDRESS = "IP_ADDRESS";
	public static final String SESSION_ID = "SESSION_ID";

	public static final String CLIENT_TYPE = "CLIENT_TYPE";
	public static final String CUST_NAME = "CUST_NAME";
	public static final String TITLE_CODE = "TITLE_CODE";
	public static final String CONST_CODE = "CONST_CODE";
	public static final String CUST_OPEN_DATE = "CUST_OPEN_DATE";
	public static final String ADDRESS_LINE1 = "ADDRESS_LINE1";
	public static final String ADDRESS_LINE2 = "ADDRESS_LINE2";
	public static final String ADDRESS_LINE3 = "ADDRESS_LINE3";
	public static final String ADDRESS_LINE4 = "ADDRESS_LINE4";
	public static final String ADDRESS_LINE5 = "ADDRESS_LINE5";
	public static final String OTN = "OTN";
	public static final String INTF_NUM = "INTF_NUM";
	public static final String UDCH_SCHEME_CODE = "CLIENTS_UDCH_SCHEME";
	public static final String UDCH_CODE = "CLIENTS_UDCH_CODE";
	public static final String CUSTOMER_IFSC = "IFSC_CODE";

	public static final String DOB = "DOB";
	public static final String SEX = "SEX";
	public static final String ORG_QUAL = "ORG_QUAL";
	public static final String INDUS_CODE = "INDUS_CODE";
	public static final String SUB_INDUS_CODE = "SUB_INDUS_CODE";
	public static final String GROUP_CODE = "GROUP_CODE";
	public static final String NO_OF_CLIENTS = "NO_OF_CLIENTS";

	public static final String TRAN_DATE = "TRAN_DATE";
	public static final String VALUE_DATE = "VALUE_DATE";
	public static final String GL_HEAD = "GL_HEAD";
	public static final String DBCR_FLAG = "DBCR_FLAG";
	public static final String TRAN_CURR = "TRAN_CURR";
	public static final String TRAN_AMT = "TRAN_AMT";
	public static final String CHQ_DATE = "CHQ_DATE";
	public static final String TRAN_CODE = "TRAN_CODE";
	public static final String NARRATION1 = "NARRATION1";

	public static final String NARRATION2 = "NARRATION2";
	public static final String NARRATION3 = "NARRATION3";

	public static final String CUST_GRP_REQ_TYPE = "CUST_GRP_REQ_TYPE";
	public static final String LIEN_SL = "LIEN_SL";

	/* Transactions Begin */

	public static final String SCHEDULE_DATE = "SCHEDULE_DATE";
	public static final String BENEF_NAME1 = "BENEF_NAME1";
	public static final String IFSC_CODE = "IFSC_CODE";
	public static final String BENEF_ACNT_NUMBER = "BENEF_ACNT_NUMBER";
	public static final String PURPOSE_CODE = "PURPOSE_CODE";
	public static final String NARRATION_LINE1 = "NARRATION_LINE1";
	public static final String NARRATION_LINE2 = "NARRATION_LINE2";
	public static final String NARRATION_LINE3 = "NARRATION_LINE3";
	public static final String NARRATION_LINE4 = "NARRATION_LINE4";
	public static final String NARRATION_LINE5 = "NARRATION_LINE5";
	public static final String NARRATION_LINE6 = "NARRATION_LINE6";
	public static final String ALERT_CHOICE = "ALERT_CHOICE";
	public static final String MOBILE_NUMBER = "MOBILE_NUMBER";
	public static final String EMAIL_ID = "EMAIL_ID";
	public static final String CUST_REF_NUM = "CUST_REF_NUM";
	public static final String DEST_CUSTOMER_CODE = "DEST_CUSTOMER_CODE";
	public static final String BENEF_NAME2 = "BENEF_NAME2";
	public static final String ACCOUNT_OF = "ACCOUNT_OF";
	public static final String LOCATION_CODE = "LOCATION_CODE";

	public static final String LEAVES_PER_BOOK = "LEAVES_PER_BOOK";
	public static final String CHQ_BOOK_PFX = "CHQ_BOOK_PFX";
	public static final String START_LEAF_NUM = "START_LEAF_NUM";
	public static final String END_LEAF_NUM = "END_LEAF_NUM";
	public static final String REQUEST_TYPE = "REQUEST_TYPE";
	public static final String NUM_OF_LEAVES = "NUM_OF_LEAVES";
	public static final String CHQ_LOST = "CHQ_LOST";
	public static final String CHQ_AMT = "CHQ_AMT";
	public static final String CHQ_BOOK_NAME = "CHQ_BOOK_NAME";
	public static final String NUM_OF_BOOKS = "NUM_OF_BOOKS";
	public static final String TOTAL_LEAVES = "TOTAL_LEAVES";
	public static final String COLLECTION_TYPE = "COLLECTION_TYPE";
	public static final String COLLECTED_BY = "COLLECTED_BY";
	/* Transactions End */

	public static final String ACNT_FLAG = "ACNT_FLAG";
	public static final String TYPE_OF_CLEAR = "TYPE_OF_CLEAR";
	public static final String CHQ_NUM = "CHQ_NUM";

	public static final String CHALLAN_ENTRY = "CHALLAN_ENTRY";
	public static final String CHALLAN_AMT_DTLS = "CHALLAN_AMT_DTLS";
	public static final String CHALLAN_PYMT_DTLS = "CHALLAN_PYMT_DTLS";
	public static final String REQ_ID = "REQ_ID";
	public static final String STATUS = "STATUS";

	public static final String START_DATE = "START_DATE";
	public static final String END_DATE = "END_DATE";
	public static final String AVL_TXN_COUNT = "AVL_TXN_COUNT";

	public static final String TOTAL_SCRIPS = "TOTAL_SCRIPS";
	public static final String SCRIP_CLOSE_BAL = "SCRIP_CLOSE_BAL";
	public static final String SCRIP_OPEN_BAL = "SCRIP_OPEN_BAL";

	public static final String LIEN_YEAR = "LIEN_YEAR";

	public static final String CONST_ID = "CONST_ID";
	public static final String CONST_NAME = "CONST_NAME";

	public static final String LIEN_DATE = "LIEN_DATE";
	public static final String LOAN_COVERED_AMT = "LOAN_COVERED_AMT";
	public static final String TOTAL_LIEN_AMT = "TOTAL_LIEN_AMT";
	public static final String LIEN_EFF_VALUE = "LIEN_EFF_VALUE";
	public static final String LIEN_COVERED_AMOUNT = "LIEN_COVERED_AMOUNT";

	public static final String RUNNING_AC_SUMMARY = "1";
	public static final String PUBLIC_FUND_AC_SUMMARY = "2";
	public static final String PUBLIC_DEBT_AC_SUMMARY = "3";
	public static final String TERM_LOAN_AC_SUMMARY = "4";
	public static final String SECURITY_AC_SUMMARY = "5";

	public static final String REQUEST = "REQUEST";
	public static final String RESPONSE = "RESPONSE";

	public static final String DIRECTION_REQUEST = "2";
	public static final String DIRECTION_RESPONSE = "1";
	public static final String DIRECTION_NOTIFICATION = "3";

	public static final String MTI = "MTI";
	public static final String SSIC = "SSIC";
	public static final String STAN = "STAN";
	public static final String TDT = "TDT";

	public static final String SEND_DIRECTION = "SEND_DIRECTION";
	public static final String RECV_DIRECTION = "RECV_DIRECTION";

	public static final String QUEUE_NAME = "QUEUE_NAME";
	public static final String QUEUE_CODE = "QUEUE_CODE";
	public static final String MSG_DIR = "MSG_DIR";

	public static final String WF_INV_NO = "WF_INV_NO";
	public static final String INW_INV_NO = "INW_INV_NO";
	public static final String OUW_INV_NO = "OUW_INV_NO";
	public static final String MSG_TYPE = "MSG_TYPE";
	public static final String HOST_NAME = "HOST_NAME";
	public static final String PORT = "PORT";
	public static final String CHANNEL_NAME = "CHANNEL_NAME";
	public static final String QUEUE_PRIORITY = "QUEUE_PRIORITY";
	public static final String LOG_TABLE = "LOG_TABLE";
	public static final String PE_TABLE = "PE_TABLE";
	public static final String STATUS_TABLE = "STATUS_TABLE";
	public static final int INWARD = 1;
	public static final int OUTWARD = 2;
	public static final String SUCCESS = "S";

	public static final String INV_NO = "INV_NO";
	public static final String MSG_CONTENT = "MSG_CONTENT";
	public static final String CORELATION_ID = "CORELATION_ID";
	public static final String PROCESSING_CODE = "PROCESSING_CODE";
	public static final String MTI_TYPE = "MTI_TYPE";
	public static final String USER_MESSAGE = "USER_MESSAGE";

	public static final String SESSION_USER_ID = "#USER_ID#";
	public static final String SESSION_ENTITY_CODE = "#ENTITY_CODE#";
	public static final String SESSION_CUSTOMER_CODE = "#CUSTOMER_CODE#";
	public static final String SESSION_DATE_FORMAT = "#DATE_FORMAT#";
	public static final String SESSION_CBD = "#CBD#";
	public static final String INTERFACE = "interface";
	public static final String PARAM_MAP = "PARAM_MAP";
	public static final String SESSION_ACCOUNT_DOMAIN = "#ACCOUNT_DOMAIN#";
	public static final String SESSION_SERVICE_MODEL = "#SERVICE_MODEL#";
	public static final String SESSION_SERVICE_BOUQUET = "#SERVICE_BOUQUET#";

	public static final String CUT_OFF_PRD_CHOICE = "CUT_OFF_PRD_CHOICE";
	public static final String ALERT_CHOICE_INFO = "ALERT_CHOICE_INFO";
	public static final String CUST_REF_DATE = "CUST_REF_DATE";
	public static final String ACNT_LIST = "ACNT_LIST";

	public static final String INIT_BY = "INIT_BY";
	public static final String INIT_ON = "INIT_ON";
	public static final String ITERATION_SL = "ITERATION_SL";
	public static final String NOTES = "NOTES";

	public static final String SYSTEM_CODE = "SYSTEM_CODE";
	public static final String MSG_REQUEST_TYPE = "1";
	public static final String MSG_RESPONSE_TYPE = "2";

	public static final String FILE_VALIDATION_REQD = "FILE_VALIDATION_REQD";
	public static final String FILE_CONTENT = "FILE_CONTENT";
	public static final String FILE_CHECKSUM = "FILE_CHECKSUM";
	public static final String FILE_SERVICE_ENABLED = "FILE_SERVICE_ENABLED";

	public static final String SECURITY_NAME = "SECURITY_NAME";
	public static final String CONST_STATUS = "CONST_STATUS";

	// Aribala added
	public static final String INTF_MEDIUM = "INTF_MEDIUM";
	public static final String QUEUE = "1";
	public static final String WEB_SRVC = "2";
	public static final String WF_REQST = "WF_REQ";
	public static final String SRVC_INTER_MODE = "SRVC_INTER_MODE";
	public static final String SYNC="S";
	public static final String ASYNC="A";
	public static final String FAILURE = "F";
	// Aribala added
	
	public static final String ERROR = "error";
}