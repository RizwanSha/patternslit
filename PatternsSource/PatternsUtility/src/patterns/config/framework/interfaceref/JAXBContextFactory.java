package patterns.config.framework.interfaceref;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

public class JAXBContextFactory {

	private static JAXBContext jaxbContext = null;

	private static void loadJAXBContext() {
		String packageName = "panacea.middleware.portal.pojo";
		try {
			jaxbContext = JAXBContext.newInstance(packageName);
		} catch (JAXBException e) {
			e.printStackTrace();
		}
	}

	public static JAXBContext getJAXBContext() {
		if (jaxbContext == null)
			loadJAXBContext();
		return jaxbContext;
	}

}
