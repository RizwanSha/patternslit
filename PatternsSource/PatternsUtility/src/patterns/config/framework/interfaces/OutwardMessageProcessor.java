package patterns.config.framework.interfaces;

import java.lang.reflect.Method;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

import org.apache.commons.lang3.StringUtils;

import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.loggers.ApplicationLogger;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.thread.ApplicationContext;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.framework.web.ajax.AJAXContentManager;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.framework.web.ajax.InterceptorPurpose;
import patterns.config.utils.BaseUtility;

public class OutwardMessageProcessor extends InterfaceContentProcessor {
	ApplicationLogger logger = ApplicationLogger.getInstance(this.getClass().getSimpleName());
	DBContext dbContext = null;
	private String entityCode;
	private String programId;
	private String primaryKey;
	private String action;
	private String selectQuery;
	private String referenceNumber;
	private StringBuffer completeDataBlock = new StringBuffer("");
	private Map<Integer, StringBuffer> messageBlocks = new HashMap<Integer, StringBuffer>();

	public static final int FILE_NAME_LENGTH = 40;
	public static final int FILE_TYPE_LENGTH = 10;
	public static final int MDR_LENGTH = 18;
	public static final int TOTAL_MSG_LENGTH = 5;
	public static final int CURRENT_MSG_NUMBER = 5;
	public static final int PROGRAM_ID_LENGTH = 8;
	public static final int DATA_BLOCK_LENGTH = 4000;
	boolean partitionEnabled = false;
	Object[] bucketList;
	protected final ApplicationContext context = ApplicationContext.getInstance();
	private FileConfigBean configBean;

	public static final String ENTITY_CODE = "#ENTITY_CODE#";
	public static final String USER_ID = "#USER_ID#";
	public static final String CURRENT_DATE = "#CURRENT_DATE#";
	public static final String DATE_FORMAT = "#DATE_FORMAT#";

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void process(DTObject input) {
		logger.logDebug("process() - Begin");
		this.entityCode = input.get("ENTITY_CODE");
		this.programId = input.get("PGM_ID");
		this.primaryKey = input.get("PRIMARY_KEY");
		this.action = input.get("ACTION");
		dbContext = (DBContext) input.getObject("DBCONTEXT");
		try {
			referenceNumber = generateInventorySequence("SEQ_JTCREF_ID");
			logRequest(referenceNumber);
			partitionEnabled = isPartitionEnabled(programId);

			Map<String, FileConfigBean> fileConfigurationMap = MessageConfigManager.getInstance().getMessageConfiguration(entityCode, programId);

			for (String key : fileConfigurationMap.keySet()) {
				FileConfigBean fileBean = fileConfigurationMap.get(key);
				configBean = fileBean;
				if (partitionEnabled) {
					if (fileBean.isInterceptorResolverRequired()) {
						input = processFilter(fileBean.getInterceptorResolver(), input);
						if (input != null) {
							bucketList = (Object[]) input.getObject(ContentManager.YEAR_PARAM);
						}
					}
				}
				if (fileBean.isInterceptorHandlerRequired()) {
					Class classObject = null;
					Object object = null;
					Method methodObject = null;
					Class[] parameterTypes = new Class[] { Map.class };
					Object[] params = new Object[] { input };
					classObject = Class.forName(fileBean.getInterceptorHandler());
					object = classObject.newInstance();
					methodObject = classObject.getMethod("execute", parameterTypes);
					selectQuery = (String) methodObject.invoke(object, params);
				} else {
					selectQuery = fileBean.getSqlQuery();
				}
				generateSQLStatement();
				executeSQLStatement();
			}
			generateMessageBlock();
			updateMessageBlock();
		} catch (Exception e) {
			logger.logError("process() - Error1 - " + e.getLocalizedMessage());
			try {
				updateStatus(referenceNumber, "F");
			} catch (Exception e1) {
				logger.logError("process() - Error2 - " + e1.getLocalizedMessage());
			}
		}
		logger.logDebug("process() - Begin");
	}

	public boolean isPartitionEnabled(String programID) {
		boolean isPartitionEnabled = false;
		DBUtil util = dbContext.createUtilInstance();
		String sql = "SELECT PARTITION_YEAR_WISE,PARTITION_DATA_COLUMN,INTERCEPTOR_CLASS FROM MPGM WHERE MPGM_ID=?";
		try {
			util.setMode(DBUtil.PREPARED);
			util.setSql(sql);
			util.setString(1, programID);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				if (rset.getString("PARTITION_YEAR_WISE").equals("1")) {
					isPartitionEnabled = true;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			util.reset();
		}
		return isPartitionEnabled;
	}

	private void updateMessageBlock() throws SQLException {
		DBUtil util = dbContext.createUtilInstance();
		util.setMode(DBUtil.PREPARED);
		util.setSql("INSERT INTO JTCBLOCK (ENTITY_CODE,MSG_REF_ID,MSG_BLOCK_SEQ,MSG_DATA_CONTENT) VALUES (?,?,?,?)");
		for (int sequence : messageBlocks.keySet()) {
			util.setString(1, entityCode);
			util.setString(2, referenceNumber);
			util.setInt(3, sequence);
			util.setString(4, messageBlocks.get(sequence).toString());
			util.executeUpdate();
		}
		util.reset();

		if (messageBlocks.size() > 0) {
			util.setMode(DBUtil.PREPARED);
			util.setSql("INSERT INTO JTCOUTPE (ENTITY_CODE,MSG_REF_ID) VALUES (?,?)");
			util.setString(1, entityCode);
			util.setString(2, referenceNumber);
			util.executeUpdate();
			util.reset();
		}
	}

	private void logRequest(String referenceNumber) throws SQLException {
		DBUtil util = dbContext.createUtilInstance();
		util.setMode(DBUtil.PREPARED);
		util.setSql("INSERT INTO JTCOUTQREF (ENTITY_CODE,MSG_REF_ID,MSG_PGM_ID,MSG_PK,MSG_ACTION,STATUS) VALUES (?,?,?,?,?,'P')");
		util.setString(1, entityCode);
		util.setString(2, referenceNumber);
		util.setString(3, programId);
		util.setString(4, primaryKey);
		util.setString(5, action);
		util.executeUpdate();
		util.reset();
	}

	private void updateStatus(String referenceNumber, String status) throws SQLException {
		DBUtil util = dbContext.createUtilInstance();
		util.setMode(DBUtil.PREPARED);
		util.setSql("UPDATE JTCOUTQREF SET STATUS=? WHERE ENTITY_CODE=? AND MSG_REF_ID=?");
		util.setString(1, status);
		util.setString(2, entityCode);
		util.setString(3, referenceNumber);
		util.executeUpdate();
	}

	private void generateMessageBlock() {
		String[] blocks = completeDataBlock.toString().split("(?<=\\G.{" + DATA_BLOCK_LENGTH + "})");
		int currentBlock = 1;
		int totalBlocks = blocks.length;
		for (String data : blocks) {
			StringBuffer message = new StringBuffer();
			message.append(StringUtils.leftPad(referenceNumber, MDR_LENGTH, '0'));
			message.append(StringUtils.leftPad(String.valueOf(totalBlocks), TOTAL_MSG_LENGTH, '0'));
			message.append(StringUtils.leftPad(String.valueOf(currentBlock), CURRENT_MSG_NUMBER, '0'));
			if (programId.length() > PROGRAM_ID_LENGTH) {
				message.append(StringUtils.rightPad(programId.substring(0, 8), PROGRAM_ID_LENGTH, ' '));
			} else {
				message.append(StringUtils.rightPad(programId, PROGRAM_ID_LENGTH, ' '));
			}
			message.append(data);
			messageBlocks.put(currentBlock, message);
			currentBlock++;
		}
	}

	private String generateInventorySequence(String sequenceName) throws SQLException {
		logger.logDebug("generateInventorySequence() - Begin");
		DBUtil util = dbContext.createUtilInstance();
		String result = "";
		String sqlQuery = "SELECT FN_NEXTVAL(?) AS INV_SEQ FROM DUAL";
		util.reset();
		util.setSql(sqlQuery);
		util.setString(1, sequenceName);
		ResultSet rset = util.executeQuery();
		if (rset.next()) {
			result = rset.getString(1);
		}
		util.reset();
		logger.logDebug("generateInventorySequence() - Begin");
		System.out.println("generateInventorySequence() - Begin");
		return result;
	}

	private void generateSQLStatement() throws Exception {
		logger.logDebug("generateSQLStatement() - Begin");
		String basicSQL = selectQuery;

		basicSQL = basicSQL.trim();

		StringBuffer completeSQL = new StringBuffer();
		completeSQL.append("SELECT ");
		int columnCount = 0;
		for (String columnName : configBean.getColumnNames()) {
			columnCount++;
			completeSQL.append("XM." + columnName);
			if (columnCount != configBean.getColumnNames().length)
				completeSQL.append(",");
			completeSQL.append(" ");
		}
		completeSQL.append(",@rownum:=@rownum+1 ROWNUM FROM (");
		completeSQL.append(basicSQL);
		completeSQL.append(" ) XM,(SELECT @rownum:=0) XXM ");
		basicSQL = preProcessSQL(completeSQL.toString());
		completeSQL.setLength(0);
		completeSQL.append(basicSQL);
		if (partitionEnabled) {
			completeSQL = new StringBuffer(MessageFormat.format(completeSQL.toString(), bucketList));
		}
		selectQuery = completeSQL.toString();
		logger.logDebug("generateSQLStatement() - End");

	}

	private void executeSQLStatement() throws SQLException {
		logger.logDebug("executeSQLStatement() - Begin");
		DBUtil util = dbContext.createUtilInstance();
		int bindValuesCount = 0;
		int argumentCount = 0;
		int _index = 1;

		util.reset();
		util.setSql(selectQuery);
		if (selectQuery.indexOf("?") > -1) {
			bindValuesCount = (new StringTokenizer(selectQuery, "?").countTokens()) - 1;
			String[] sql_arguments = primaryKey.split("\\|");
			if (primaryKey.equals("")) {
				argumentCount = 0;
			} else {
				argumentCount = sql_arguments.length;
			}
			if ((bindValuesCount == argumentCount) && argumentCount > 0) {
				for (int k = 1; k <= argumentCount; k++) {
					util.setString(_index, sql_arguments[k - 1]);
					_index++;
				}
			}
		}

		ResultSet resultSet = util.executeQuery();
		StringBuffer dataBlock = formDataBlock(resultSet);
		completeDataBlock.append(dataBlock.toString());
		util.reset();

		logger.logDebug("executeSQLStatement() - End");
		System.out.println("EXECUTEDSQLSTATEMENT - End");
	}

	private StringBuffer formDataBlock(ResultSet resultSet) throws SQLException {
		logger.logDebug("formDataBlock() - Begin");
		StringBuffer message = new StringBuffer("");
		int colCount = resultSet.getMetaData().getColumnCount();

		while (resultSet.next()) {
			StringBuffer dataBuffer = new StringBuffer("");
			for (int i = 0; i < colCount - 1; i++) {
				String value = resultSet.getString(configBean.getColumnNames()[i]);
				if (value == null) {
					if (configBean.getColumnType()[i].equals("1"))
						value = StringUtils.rightPad(RegularConstants.EMPTY_STRING, configBean.getColumnSize()[i], ' ');
					else
						value = StringUtils.leftPad(RegularConstants.ZERO, configBean.getColumnSize()[i], '0');

				} else if (value.length() < configBean.getColumnSize()[i]) {
					if (configBean.getColumnType()[i].equals("1"))
						value = StringUtils.rightPad(value, configBean.getColumnSize()[i], ' ');
					else
						value = StringUtils.leftPad(value, configBean.getColumnSize()[i], '0');
				}

				else if (value.length() > configBean.getColumnSize()[i]) {
					value = value.substring(0, configBean.getColumnSize()[i]);
				}
				dataBuffer.append(value);
			}
			message.append(appendDataHeader(dataBuffer));
		}
		logger.logDebug("formDataBlock() - End");
		return message;
	}

	private StringBuffer appendDataHeader(StringBuffer dataBuffer) {
		logger.logDebug("appendDataHeader() - Begin");
		StringBuffer dataBlock = new StringBuffer("{");
		dataBlock.append(StringUtils.rightPad(configBean.getFileName(), FILE_NAME_LENGTH, ' '));
		dataBlock.append(StringUtils.rightPad(configBean.getFileType(), FILE_TYPE_LENGTH, ' '));
		dataBlock.append(action);
		dataBlock.append(dataBuffer);
		dataBlock.append("}");
		logger.logDebug("appendDataHeader() - End");
		return dataBlock;
	}

}
