package patterns.config.framework.interfaces;

import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.thread.ApplicationContext;
import patterns.config.utils.BaseUtility;

public abstract class InterfaceInterceptor {

	public static String ARGUMENT_KEY="PRIMARY_KEY";
	private ApplicationContext context = ApplicationContext.getInstance();

	public ApplicationContext getContext() {
		return context;
	}

	public InterfaceInterceptor() {
	}

	public abstract DTObject resolvePartitionArguments(DTObject input);


	public String resolveParitionYear(String programId, String sourceKey) {
		DBContext dbContext = new DBContext();
		String partitionYear = context.getFinYear();
		try {
			partitionYear = BaseUtility.resolvePartitionBucketID(programId, sourceKey, dbContext);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			dbContext.close();
		}
		return partitionYear;
	}
}