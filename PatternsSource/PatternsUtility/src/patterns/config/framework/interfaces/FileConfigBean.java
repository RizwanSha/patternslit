package patterns.config.framework.interfaces;

import patterns.config.framework.database.BindParameterType;

public class FileConfigBean {

	private String sqlQuery;
	private boolean interceptorHandlerRequired;
	private String interceptorHandler;
	private String fileName;
	private String fileType;
	private String interceptorResolver;
	private boolean interceptorResolverRequired;

	

	private BindParameterType[] columnTypes;
	private String[] columnNames;
	private int[] columnSize;
	private String[] columnType;
	

	public String[] getColumnType() {
		return columnType; 
	}

	public void setColumnType(String[] columnType) {
		this.columnType = columnType;
	}

	public String getSqlQuery() {
		return sqlQuery;
	}

	public void setSqlQuery(String sqlQuery) {
		this.sqlQuery = sqlQuery;
	}

	

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public BindParameterType[] getColumnTypes() {
		return columnTypes;
	}

	public void setColumnTypes(BindParameterType[] columnTypes) {
		this.columnTypes = columnTypes;
	}

	public String[] getColumnNames() {
		return columnNames;
	}

	public void setColumnNames(String[] columnNames) {
		this.columnNames = columnNames;
	}

	public int[] getColumnSize() {
		return columnSize;
	}

	public void setColumnSize(int[] columnSize) {
		this.columnSize = columnSize;
	}

	public boolean isInterceptorHandlerRequired() {
		return interceptorHandlerRequired;
	}

	public void setInterceptorHandlerRequired(boolean interceptorHandlerRequired) {
		this.interceptorHandlerRequired = interceptorHandlerRequired;
	}

	public String getInterceptorHandler() {
		return interceptorHandler;
	}

	public void setInterceptorHandler(String interceptorHandler) {
		this.interceptorHandler = interceptorHandler;
	}

	public String getInterceptorResolver() {
		return interceptorResolver;
	}

	public void setInterceptorResolver(String interceptorResolver) {
		this.interceptorResolver = interceptorResolver;
	}

	public boolean isInterceptorResolverRequired() {
		return interceptorResolverRequired;
	}

	public void setInterceptorResolverRequired(boolean interceptorResolverRequired) {
		this.interceptorResolverRequired = interceptorResolverRequired;
	}



}
