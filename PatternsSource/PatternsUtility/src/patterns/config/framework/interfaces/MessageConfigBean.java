package patterns.config.framework.interfaces;

import java.util.Map;

public class MessageConfigBean {

	private String processingCode;
	private String programId;

	private Map<String, FileConfigBean> configurationDetails;

	public String getProcessingCode() {
		return processingCode;
	}

	public void setProcessingCode(String processingCode) {
		this.processingCode = processingCode;
	}

	public String getProgramId() {
		return programId;
	}

	public void setProgramId(String programId) {
		this.programId = programId;
	}

	public Map<String, FileConfigBean> getConfigurationDetails() {
		return configurationDetails;
	}

	public void setConfigurationDetails(Map<String, FileConfigBean> configurationDetails) {
		this.configurationDetails = configurationDetails;
	}
}
