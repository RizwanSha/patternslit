package patterns.config.framework.interfaces;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.loggers.ApplicationLogger;
import patterns.config.framework.web.FormatUtils;

public class MessageConfigManager {
	private static final ReentrantReadWriteLock readWriteLockGlobal = new ReentrantReadWriteLock();

	private static final Lock writeGlobal = readWriteLockGlobal.writeLock();
	private static final Lock readGlobal = readWriteLockGlobal.readLock();
	private ApplicationLogger logger = null;

	private Map<String, MessageConfigBean> internalMap;
	private Map<String, MessageConfigBean> internalLocalMap;
	private DBContext dbContext = null;
	private DBUtil dbUtil = null;
	private DBUtil innerDBUtil = null;
	private DBUtil util = null;
	private static MessageConfigManager instance = null;
	private boolean readyToRead = false;

	public MessageConfigManager() {
		logger = ApplicationLogger.getInstance(this.getClass().getSimpleName());
		logger.logDebug("#()");
	}

	private void initConfiguration() {
		internalMap.putAll(internalLocalMap);
		internalLocalMap.clear();
	}

	private void resetConfiguration() {
		if (internalMap == null)
			internalMap = new HashMap<String, MessageConfigBean>();
		internalMap.clear();
	}

	public static void unloadConfiguration() {
		try {
			writeGlobal.lock();
			if (instance != null) {
				instance.resetConfiguration();
				instance = null;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			writeGlobal.unlock();
		}
	}

	public static void reloadConfiguration() {
		try {
			writeGlobal.lock();
			if (instance == null) {
				instance = new MessageConfigManager();
				instance.readyToRead = false;
				instance.loadConfiguration();
				instance.resetConfiguration();
				instance.initConfiguration();
				instance.readyToRead = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			writeGlobal.unlock();
		}
	}

	public static MessageConfigManager getInstance() {
		if (instance == null) {
			reloadConfiguration();
		} else {
			if (!instance.readyToRead) {
				try {
					readGlobal.lock();
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					readGlobal.unlock();
				}
			}
		}
		return instance;
	}

	public Map<String, FileConfigBean> getMessageConfiguration(String entity, String programId) {
		Map<String, FileConfigBean> fileConfigurationMap = null;
		String keyMap = "";
		keyMap = entity + "|" + programId;
		MessageConfigBean messageConfigBean = internalMap.get(keyMap);
		fileConfigurationMap = messageConfigBean.getConfigurationDetails();
		return fileConfigurationMap;
	}

	public void loadConfiguration() {
		logger.logDebug("loadConfiguration() - Begin");
		String sql = RegularConstants.EMPTY_STRING;
		String mapKey = RegularConstants.EMPTY_STRING;
		String entity = RegularConstants.EMPTY_STRING;
		if (internalLocalMap == null)
			internalLocalMap = new HashMap<String, MessageConfigBean>();
		internalLocalMap.clear();
		try {
			dbContext = new DBContext();
			dbContext.openConnection();
			dbUtil = dbContext.createUtilInstance();
			innerDBUtil = dbContext.createUtilInstance();
			util = dbContext.createUtilInstance();
			dbUtil.reset();
			sql = "SELECT PARTITION_NO FROM INSTALL";
			dbUtil.setSql(sql);
			ResultSet rset = dbUtil.executeQuery();
			while (rset.next()) {
				entity = rset.getString(1);
			}

			MessageConfigBean messageConfigBean = new MessageConfigBean();
			sql = "SELECT PGM_ID,PROC_CODE FROM JTCMSGCONFIG WHERE ENTITY_CODE=?";
			dbUtil.setSql(sql);
			dbUtil.setString(1, entity);
			rset = dbUtil.executeQuery();
			while (rset.next()) {
				String processingCode = rset.getString("PROC_CODE");
				String programId = rset.getString("PGM_ID");
				mapKey = entity + "|" + programId;
				Map<String, FileConfigBean> fileConfiguration = new HashMap<String, FileConfigBean>();

				messageConfigBean.setProcessingCode("PGM_IDENTFR");

				innerDBUtil.reset();
				sql = "SELECT FILE_NAME,FILE_TYPE,QUERY,INTERCEPTOR_HANDLER_REQD,INTERCEPTOR_HANDLER,INTERCEPTOR_RESOLVER_REQD,INTERCEPTOR_RESOLVER FROM JTCMSGFILETYPEDTL WHERE ENTITY_CODE=? AND PROC_CODE=?";
				innerDBUtil.setSql(sql);
				innerDBUtil.setString(1, entity);
				innerDBUtil.setString(2, processingCode);
				ResultSet readMsgCfgRSet = innerDBUtil.executeQuery();
				while (readMsgCfgRSet.next()) {
					String key = readMsgCfgRSet.getString("FILE_NAME") + "|" + readMsgCfgRSet.getString("FILE_TYPE");
					FileConfigBean configuration = new FileConfigBean();
					fileConfiguration.put(key, configuration);

					configuration.setInterceptorHandlerRequired(FormatUtils.decodeStringToBoolean(readMsgCfgRSet.getString("INTERCEPTOR_HANDLER_REQD")));
					configuration.setInterceptorHandler(readMsgCfgRSet.getString("INTERCEPTOR_HANDLER"));
					configuration.setInterceptorResolverRequired(FormatUtils.decodeStringToBoolean(readMsgCfgRSet.getString("INTERCEPTOR_RESOLVER_REQD")));
					configuration.setInterceptorResolver(readMsgCfgRSet.getString("INTERCEPTOR_RESOLVER"));
					configuration.setSqlQuery(readMsgCfgRSet.getString("QUERY"));
					configuration.setFileName(readMsgCfgRSet.getString("FILE_NAME"));
					configuration.setFileType(readMsgCfgRSet.getString("FILE_TYPE"));

					util.reset();
					sql = "SELECT COUNT(*) FROM JTCTABLECFG WHERE ENTITY_CODE=? AND PROC_CODE=? AND FILE_NAME=? AND FILE_TYPE=?";
					util.setSql(sql);
					util.setString(1, entity);
					util.setString(2, processingCode);
					util.setString(3, readMsgCfgRSet.getString("FILE_NAME"));
					util.setString(4, readMsgCfgRSet.getString("FILE_TYPE"));
					ResultSet rs = util.executeQuery();
					int columnCount = 0;
					if (rs.next()) {
						columnCount = rs.getInt(1);
					}

					if (columnCount > 0) {
						String[] columnNames = new String[columnCount];
						int[] columnSize = new int[columnCount];
						String[] columnType = new String[columnCount];
						util.reset();
						sql = "SELECT COLUMN_NAME,COLUMN_SIZE,COLUMN_TYPE FROM JTCTABLECFG WHERE ENTITY_CODE=? AND PROC_CODE=? AND FILE_NAME=? AND FILE_TYPE=?";
						util.setSql(sql);
						util.setString(1, entity);
						util.setString(2, processingCode);
						util.setString(3, readMsgCfgRSet.getString("FILE_NAME"));
						util.setString(4, readMsgCfgRSet.getString("FILE_TYPE"));
						rs = util.executeQuery();
						int i = 0;
						while (rs.next()) {
							columnNames[i] = rs.getString("COLUMN_NAME");
							columnSize[i] = rs.getInt("COLUMN_SIZE");
							columnType[i] = rs.getString("COLUMN_TYPE");
							i++;
						}
						configuration.setColumnNames(columnNames);
						configuration.setColumnSize(columnSize);
						configuration.setColumnType(columnType);
					}
				}
				messageConfigBean.setConfigurationDetails(fileConfiguration);
				internalLocalMap.put(mapKey, messageConfigBean);
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError("loadConfiguration() - Error - " + e.getLocalizedMessage());
		} finally {
			dbUtil.reset();
			innerDBUtil.reset();
			util.reset();
			if (dbContext != null) {
				dbContext.close();
			}
		}
		logger.logDebug("loadConfiguration() - End");
	}
}
