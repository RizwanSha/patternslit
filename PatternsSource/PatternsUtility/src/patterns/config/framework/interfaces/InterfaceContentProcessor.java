package patterns.config.framework.interfaces;

import patterns.config.framework.service.DTObject;
import patterns.config.framework.service.ServiceLocator;
import patterns.config.framework.thread.ApplicationContext;
import patterns.config.framework.web.BackOfficeFormatUtils;

public abstract class InterfaceContentProcessor {
	public static final String ENTITY_CODE = "#ENTITY_CODE#";
	public static final String USER_ID = "#USER_ID#";
	public static final String CURRENT_DATE = "#CURRENT_DATE#";
	public static final String DATE_FORMAT = "#DATE_FORMAT#";

	public abstract void process(DTObject input);

	private ApplicationContext context = ApplicationContext.getInstance();

	public final ApplicationContext getContext() {
		return context;
	}

	public final String preProcessSQL(String sqlQuery) {
		sqlQuery = sqlQuery.replaceAll(ENTITY_CODE, context.getEntityCode());
		sqlQuery = sqlQuery.replaceAll(USER_ID, context.getUserID());
		sqlQuery = sqlQuery.replaceAll(DATE_FORMAT, context.getDateFormat());
		sqlQuery = sqlQuery.replaceAll(CURRENT_DATE, BackOfficeFormatUtils.getDate(context.getCurrentBusinessDate(), context.getDateFormat()));
		return sqlQuery;
	}

	public final DTObject processFilter(String interceptorName, DTObject input) {
		DTObject result = new DTObject();
		try {
			ServiceLocator locator = ServiceLocator.getInstance();
			InterfaceInterceptor filterInstance = (InterfaceInterceptor) locator.getInterceptor(interceptorName).newInstance();
			result = filterInstance.resolvePartitionArguments(input);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
}