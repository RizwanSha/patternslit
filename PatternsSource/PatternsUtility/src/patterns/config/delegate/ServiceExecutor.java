/*
 * Copyright (c) 2014, 2015, Patterns Software Design Institute. All
 * rights reserved. Unauthorized copying of this file, via any medium is
 * strictly prohibited. PATTERNS SDI PROPRIETARY/CONFIDENTIAL. Use is
 * subject to license terms.
 */
package patterns.config.delegate;

import java.lang.reflect.Method;

import javax.ejb.EJB;
import javax.naming.Context;
import javax.naming.InitialContext;

import patterns.config.framework.DatasourceConfigurationManager;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.loggers.ApplicationLogger;
import patterns.config.framework.process.TBAProcessResult;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTObject;

import com.patterns.framework.ejb.TBACommonDispatcher;


/**
 * The Class ServiceExecutor is used by mobile web service handlers to
 * invokde business functionality
 */
public class ServiceExecutor {

	/** The logger. */
	protected ApplicationLogger logger = null;

	/** The initial context. */
	private static Context initialContext;

	/**
	 * Instantiates a new service executor.
	 */
	public ServiceExecutor() {
		logger = ApplicationLogger.getInstance(this.getClass().getSimpleName());
		// logger.logDebug("#()");
	}

	/** The dispatcher. */
	@EJB
	TBACommonDispatcher dispatcher;

	/**
	 * Delegate.
	 * 
	 * @param inputDTO
	 *            the input dto
	 * @return the TBA process result
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public TBAProcessResult delegate(DTObject inputDTO) {
		TBAProcessResult processResult = null;
		try {
			if (initialContext == null) {
				initialContext = new InitialContext();
			}
			Object obj = initialContext.lookup(DatasourceConfigurationManager.getCommonDispatcherHomeJNDI());
			Class c = obj.getClass();
			Method method = c.getDeclaredMethod("processRequest", DTObject.class);
			processResult = (TBAProcessResult) method.invoke(obj, inputDTO);
		} catch (Throwable e) {
			e.printStackTrace();
			logger.logError(e.getLocalizedMessage());
			logger.logError("delegate(inputDTO 1 ) " + e.getLocalizedMessage());
			processResult = new TBAProcessResult();
			processResult.setErrorCode(BackOfficeErrorCodes.BEAN_LOOKUP_ERROR);
			String error = e.getLocalizedMessage();
			processResult.setAdditionalInfo(error);
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
		}
		return processResult;
	}
}
