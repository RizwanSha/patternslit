package patterns.config.delegate;

import java.lang.reflect.Method;

import javax.naming.Context;
import javax.naming.InitialContext;

import patterns.config.framework.ConfigurationManager;
import patterns.config.framework.DatasourceConfigurationManager;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.loggers.ApplicationLogger;
import patterns.config.framework.process.TBAProcessInfo;
import patterns.config.framework.process.TBAProcessResult;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.service.EJBLocator;

import com.patterns.framework.ejb.CommonDispatcherLocal;

public final class BackOfficeProcessManager {
	private final ApplicationLogger logger;

	private static Context initialContext;
	public BackOfficeProcessManager() {
		logger = ApplicationLogger.getInstance(this.getClass().getSimpleName());
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public TBAProcessResult delegate(TBAProcessInfo processInfo) {

		// logger.logDebug("delegate()");
		TBAProcessResult processResult = null;
		try {

			if (initialContext == null) {
				initialContext = new InitialContext();
			}
			Object obj = initialContext.lookup(ConfigurationManager.getCommonDispatcherHomeJNDI());

			Class c = obj.getClass();
			Method method = c.getDeclaredMethod("processRequest", TBAProcessInfo.class);
			processResult = (TBAProcessResult) method.invoke(obj, processInfo);

		} catch (Exception e) {
			e.printStackTrace();
			logger.logError("delegate(1 ) " + e.getLocalizedMessage());
			processResult = new TBAProcessResult();
			String error = e.getLocalizedMessage();
			processResult.setErrorCode(BackOfficeErrorCodes.BEAN_LOOKUP_ERROR);
			processResult.setAdditionalInfo(error);
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
		} catch (Throwable e) {
			e.printStackTrace();
			logger.logError("delegate(2 ) " + e.getLocalizedMessage());
			processResult = new TBAProcessResult();
			String error = e.getLocalizedMessage();
			processResult.setErrorCode(BackOfficeErrorCodes.BEAN_LOOKUP_ERROR);
			processResult.setAdditionalInfo(error);
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);

		}
		return processResult;
	
	}

	public TBAProcessResult delegate(DTObject processInfo) {
		TBAProcessResult processResult = null;
		try {
			CommonDispatcherLocal commonDispatcher = EJBLocator.getInstance().getCommonDipatcher(DatasourceConfigurationManager.getCommonDispatcherHomeJNDI());
			processResult = commonDispatcher.processRequest(processInfo);
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError("delegate(inputDTO 1 ) " + e.getLocalizedMessage());
			processResult = new TBAProcessResult();
			processResult.setErrorCode(BackOfficeErrorCodes.BEAN_LOOKUP_ERROR);
			String error = e.getLocalizedMessage();
			processResult.setAdditionalInfo(error);
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
		} catch (Throwable e) {
			e.printStackTrace();
			logger.logError("delegate(inputDTO 2 ) " + e.getLocalizedMessage());
			processResult = new TBAProcessResult();
			String error = e.getLocalizedMessage();
			processResult.setErrorCode(BackOfficeErrorCodes.BEAN_LOOKUP_ERROR);
			processResult.setAdditionalInfo(error);
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);

		}
		return processResult;
	}
}
