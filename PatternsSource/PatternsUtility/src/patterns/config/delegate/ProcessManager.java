package patterns.config.delegate;

import patterns.config.framework.DatasourceConfigurationManager;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.loggers.ApplicationLogger;
import patterns.config.framework.process.TBAProcessInfo;
import patterns.config.framework.process.TBAProcessResult;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.service.EJBLocator;

import com.patterns.framework.ejb.ProcessDispatcherLocal;

public final class ProcessManager {
	private final ApplicationLogger logger;

	public ProcessManager() {
		logger = ApplicationLogger.getInstance(this.getClass().getSimpleName());
	}

	public TBAProcessResult delegate(TBAProcessInfo processInfo) {
		TBAProcessResult processResult = null;
		try {
			ProcessDispatcherLocal processDispatcher = EJBLocator.getInstance().getProcessDipatcher(DatasourceConfigurationManager.getProcessDispatcherHomeJNDI());
			processResult = processDispatcher.processRequest(processInfo);
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError("delegate(1 ) " + e.getLocalizedMessage());
			processResult = new TBAProcessResult();
			String error = e.getLocalizedMessage();
			processResult.setErrorCode(BackOfficeErrorCodes.BEAN_LOOKUP_ERROR);
			processResult.setAdditionalInfo(error);
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
		} catch (Throwable e) {
			e.printStackTrace();
			logger.logError("delegate(2 ) " + e.getLocalizedMessage());
			processResult = new TBAProcessResult();
			String error = e.getLocalizedMessage();
			processResult.setErrorCode(BackOfficeErrorCodes.BEAN_LOOKUP_ERROR);
			processResult.setAdditionalInfo(error);
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
		}
		return processResult;
	}

	public TBAProcessResult delegate(DTObject processInfo) {
		TBAProcessResult processResult = null;
		try {
			ProcessDispatcherLocal processDispatcher = EJBLocator.getInstance().getProcessDipatcher(DatasourceConfigurationManager.getProcessDispatcherHomeJNDI());
			processResult = processDispatcher.processRequest(processInfo);
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError("delegate(inputDTO 1 ) " + e.getLocalizedMessage());
			processResult = new TBAProcessResult();
			processResult.setErrorCode(BackOfficeErrorCodes.BEAN_LOOKUP_ERROR);
			String error = e.getLocalizedMessage();
			processResult.setAdditionalInfo(error);
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
		} catch (Throwable e) {
			e.printStackTrace();
			logger.logError("delegate(inputDTO 2 ) " + e.getLocalizedMessage());
			processResult = new TBAProcessResult();
			String error = e.getLocalizedMessage();
			processResult.setErrorCode(BackOfficeErrorCodes.BEAN_LOOKUP_ERROR);
			processResult.setAdditionalInfo(error);
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
		}
		return processResult;
	}
}
