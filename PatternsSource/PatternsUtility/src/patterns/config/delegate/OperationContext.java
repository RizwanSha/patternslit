package patterns.config.delegate;

public enum OperationContext {

	WEB_APPLICATION_CONTEXT, MIDDLEWARE_APPLICATION_CONTEXT, BO_CONTEXT
}
