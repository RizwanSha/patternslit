package patterns.config.constants;

public class ProgramConstants {

	public static final String CURRENCY_UNITS = "CURRENCY_UNITS";
	public static final String AMOUNT = "AMOUNT";
	public static final String PERSON_NAME="PERSONNAME";
	
}
