package patterns.config.authorization;

import java.sql.ResultSet;
import java.sql.Timestamp;

import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.service.DTObject;

public abstract class CommonTBAUpdate {
	protected String _programID;
	protected String source_key = "";
	protected String tba_key = "";
	protected String strsrcKey[] = null;
	public static final String ADD = "A";
	public static final String MODIFY = "M";

	public static final String PROCESS_STAGE = "PROCESS_STAGE";
	public static final String PROCESS_OPTION = "PROCESS_OPTION";
	public static final String BEFORE = "B";
	public static final String AFTER = "A";
	public static final String AUTHORIZE = "A";
	public static final String REJECT = "R";

	public java.sql.Timestamp getSystemDate(DBContext dbContext, String entityCode) throws Exception {
		DBUtil dbutil = dbContext.createUtilInstance();
		Timestamp sys_date_time = null;
		try {
			dbutil.reset();
			dbutil.setMode(DBUtil.PREPARED);
			dbutil.setSql("SELECT FN_GETCDT(?) FROM DUAL ");
			dbutil.setString(1, entityCode);
			ResultSet rs = dbutil.executeQuery();
			if (rs.next()) {
				sys_date_time = rs.getTimestamp(1);
			}
			rs.close();
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			dbutil.reset();
		}
		return sys_date_time;
	}

	public void splitSourceKey(DTObject Inputobj) {
		source_key = Inputobj.get("SOURCE_KEY").toString();
		tba_key = Inputobj.get("TBA_MAIN_KEY").toString();
		if (source_key.trim().startsWith("|"))
			source_key = " " + source_key;
		if (source_key.trim().endsWith("|"))
			source_key = source_key + " ";
		strsrcKey = source_key.split("\\|");
	}

	public DTObject getUserDetails(DBContext dbContext, String partitionNo, String UserId) {
		DTObject result = new DTObject();
		DBUtil dbutil = dbContext.createUtilInstance();
		try {
			dbutil.reset();
			dbutil.setMode(DBUtil.PREPARED);
			dbutil.setSql("SELECT ENTITY_CODE,ROLE_CODE,STATUS,FIRST_PIN,FIRST_PIN_SALT  FROM USERS WHERE PARTITION_NO =? AND USER_ID=? ");
			dbutil.setString(1, partitionNo);
			dbutil.setString(2, UserId);
			ResultSet rs = dbutil.executeQuery();
			if (rs.next()) {
				result.set("ENTITY_CODE", rs.getString("ENTITY_CODE"));
				result.set("ROLE_CODE", rs.getString("ROLE_CODE"));
				result.set("STATUS", rs.getString("STATUS"));
				result.set("FIRST_PIN", rs.getString("FIRST_PIN"));
				result.set("FIRST_PIN_SALT", rs.getString("FIRST_PIN_SALT"));
			}
			rs.close();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			dbutil.reset();
		}
		return result;
	}

	public abstract DTObject processTBAUpdate(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException;

	public abstract DTObject processEffDateTBAUpdate(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException;
}
