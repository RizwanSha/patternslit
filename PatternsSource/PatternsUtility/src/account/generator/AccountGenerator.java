package account.generator;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;

public class AccountGenerator {
	List<String> fileContentList = new ArrayList<String>();
	// private static final int NO_OF_ENTRIES = 10001;
	private static final String FILE_PATH = "D:\\PPBS\\File Upload\\Data\\";

	// private static void generateAccounts() throws IOException {
	// File ACCOUNTS = new File(FILE_PATH + "accounts.txt");
	// StringBuffer sb = new StringBuffer("");
	// for (int i = 1; i <= NO_OF_ENTRIES; i++) {
	// String a = String.valueOf(System.nanoTime());
	// a = a + RandomRange.generateRandomNumber(10, 99);
	// a = a.substring(8);
	// a = "1" + a;
	// sb.append(a).append("\n");
	// }
	// FileUtils.writeStringToFile(ACCOUNTS, sb.toString());
	// }

	public static void main(String[] args) throws Exception {
		// List<String> fileContentList = FileUtils.readLines(new File(FILE_PATH
		// + "accounts.txt"));
		// AccountGenerator.generateAccounts();
		// generateAccountSQL();
		// generateAccountBalanceRecords();

		// generateNREGAFile(fileContentList);
		// generateSplitID();

		generateFile();
	}

	// private static void generateAccountSQL() throws Exception {
	// List<String> fileContentList = FileUtils.readLines(new File(FILE_PATH +
	// "accounts.txt"));
	// File ACCOUNTS = new File(FILE_PATH + "ACCOUNTS.sql");
	// StringBuffer sb = new
	// StringBuffer("TRUNCATE TABLE ACCOUNTS;\nINSERT INTO ACCOUNTS VALUES ");
	// for (String acNo : fileContentList) {
	// sb.append("(");
	// sb.append("'1',");
	// sb.append("'").append(acNo).append("',");
	// sb.append("'1',");
	// sb.append("'1',");
	// sb.append("'110',");
	// sb.append("'1',");
	// sb.append("'XYZ',");
	// sb.append("'MR',");
	// sb.append("'ABC',");
	// sb.append("'1',");
	// sb.append("'A',"); /* OPERATING_MODE */
	// sb.append("'0',");
	// sb.append("'0',");
	// sb.append("'0',");
	// sb.append("'0',");
	// sb.append("'0',");
	// sb.append("'0',");
	// sb.append("'123',");
	// sb.append("'0',");
	// sb.append("'',");
	// sb.append("'0',");
	// sb.append("'0',");
	// sb.append("'',");
	// sb.append("'2016-07-25',");
	// sb.append("'0',");
	// sb.append("'A',");
	// sb.append("'2016-07-25',");
	// sb.append("NULL,");
	// sb.append("'',");
	// sb.append("'SYSTEM',");
	// sb.append("NULL");
	// sb.append("),").append("\n");
	// }
	// sb = new StringBuffer(sb.substring(0, sb.length() - 2));
	// sb.append(";");
	// FileUtils.writeStringToFile(ACCOUNTS, sb.toString());
	// }

	// private static void generateAccountBalanceRecords() throws Exception {
	// List<String> fileContentList = FileUtils.readLines(new File(FILE_PATH +
	// "accounts.txt"));
	// File ACCOUNTS = new File(FILE_PATH + "ACNTBALA00.sql");
	// StringBuffer sb = new
	// StringBuffer("TRUNCATE TABLE ACNTBALA00;\nINSERT INTO ACNTBALA00 VALUES ");
	// for (String acNo : fileContentList) {
	// sb.append("(");
	// sb.append("'1',");
	// sb.append("'").append(acNo).append("',");
	// sb.append("'INR',");
	// sb.append("'1000',");
	// sb.append("'1000',");
	// sb.append("'0',");
	// sb.append("'0',");
	// sb.append("'0',");
	// sb.append("'0',");
	// sb.append("'0',");
	// sb.append("'0',");
	// sb.append("'0',");
	// sb.append("'0',");
	// sb.append("'0',");
	// sb.append("'0',");
	// sb.append("'0'");
	// sb.append("),").append("\n");
	// }
	// sb = new StringBuffer(sb.substring(0, sb.length() - 2));
	// sb.append(";");
	// FileUtils.writeStringToFile(ACCOUNTS, sb.toString());
	// }
	//
	// private static void generateNREGAFile(List<String> fileContentList)
	// throws Exception {
	// File ACCOUNTS = new File(FILE_PATH + "07082016.txt");
	// StringBuffer sb = new StringBuffer("");
	// for (String acNo : fileContentList) {
	// sb.append(acNo).append("      ");
	// sb.append("INR49366301C          ");
	// sb.append("1000.0044-1714258-49200100-_13651");
	// for (int i = 1; i <= 915; i++) {
	// sb.append(" ");
	// }
	// sb.append("\r\n");
	// }
	// sb = new StringBuffer(sb.substring(0, sb.length() - 2));
	// FileUtils.writeStringToFile(ACCOUNTS, sb.toString());
	// }
	//
	// private static void generateSplitID() throws IOException {
	// File ACCOUNTS = new File(FILE_PATH + "GLSPLITLK.sql");
	// StringBuffer sb = new StringBuffer("INSERT INTO GLSPLITLK VALUES ");
	// for (int i = 1; i <= 99999; i++) {
	// sb.append("(").append(i).append("),\n");
	// }
	// sb = new StringBuffer(sb.substring(0, sb.length() - 2));
	// sb.append(";");
	// FileUtils.writeStringToFile(ACCOUNTS, sb.toString());
	// }

	private static void generateFile() throws SQLException {
		Connection con = null;
		try {
			String startAc = "100000100001";
			String endAc = "100000110000";
			String debitAc = "100000110001";
			String creditAmount = "1000.00";
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/ppbs", "root", "patterns");
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT ACCOUNT_NO FROM ACCOUNTS WHERE ACCOUNT_NO >=" + startAc + " AND ACCOUNT_NO <= " + endAc);
			File ACCOUNTS = new File(FILE_PATH + "09082016.txt");
			StringBuffer creditLegs = new StringBuffer("");
			BigDecimal debitAmount = new BigDecimal("0");
			while (rs.next()) {
				debitAmount = debitAmount.add(new BigDecimal(creditAmount));
				creditLegs.append(rs.getString(1)).append("    ");
				creditLegs.append("INR49366301C          ");
				creditLegs.append(creditAmount).append("44-1714258-49200100-_13651");
				for (int i = 1; i <= 915; i++) {
					creditLegs.append(" ");
				}
				creditLegs.append("\r\n");
			}
			StringBuffer debitLeg = new StringBuffer("");
			debitLeg.append(debitAc).append("    ");
			debitLeg.append("INR49366301D");
			NumberFormat format = NumberFormat.getNumberInstance();
			format.setMaximumFractionDigits(2);
			format.setMinimumFractionDigits(2);
			format.setGroupingUsed(false);
			String value = format.format(debitAmount);
			debitLeg.append(StringUtils.leftPad(value, 17, " "));
			debitLeg.append("44-1714258-49200100-_13651");
			for (int i = 1; i <= 915; i++) {
				debitLeg.append(" ");
			}
			debitLeg.append("\r\n");
			creditLegs.append(debitLeg);
			FileUtils.writeStringToFile(ACCOUNTS, creditLegs.toString());
		} catch (Exception e) {
			System.out.println(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}
}
