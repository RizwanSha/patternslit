package account.generator;

import java.io.File;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;

public class SampleFileGenerator {
	List<String> fileContentList = new ArrayList<String>();
	private static final String FILE_PATH = "D:\\PPBS\\Work\\FileUpload\\Data\\";
	private static String START_ACCOUNT_NUMBER = "100000100001";
	private static String DEBIT_ACCOUNT_NUMBER = "100000199999";
	//private static String START_ACCOUNT_NUMBER = "5002001250";
	//private static String DEBIT_ACCOUNT_NUMBER = "5002004199";
	private static String CREDIT_AMOUNT = "20.00";

	public static void main(String[] args) throws Exception {
		//generateFile(100);
		//generateFile(500);
		//generateFile(200);
		// generateFile(10000);
		 generateFile(20000);
	}

	private static void generateFile(int numberOfTransactions) throws SQLException {
		String endAc = new BigInteger(START_ACCOUNT_NUMBER).add(new BigInteger(String.valueOf(numberOfTransactions - 1))).toString();
		Connection con = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/atomfinal", "PATTERNSUSER", "PATTERNSUSER");
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT ACCOUNT_NO FROM ACCOUNTS WHERE ACCOUNT_NO >=" + START_ACCOUNT_NUMBER + " AND ACCOUNT_NO <= " + endAc);
			File ACCOUNTS = new File(FILE_PATH + "FILE-" + numberOfTransactions + ".txt");
			StringBuffer creditLegs = new StringBuffer("");
			BigDecimal debitAmount = new BigDecimal("0");
			int j = 1;
			while (rs.next()) {
				debitAmount = debitAmount.add(new BigDecimal(CREDIT_AMOUNT));
				creditLegs.append(StringUtils.rightPad(rs.getString(1), 16, " "));
				creditLegs.append("INR49366301C          ");
				creditLegs.append(CREDIT_AMOUNT).append("44-1714258-49200100-_").append(StringUtils.leftPad(String.valueOf(j++), 5, "0"));
				for (int i = 1; i <= 915; i++) {
					creditLegs.append(" ");
				}
				creditLegs.append("\r\n");
			}
			StringBuffer debitLeg = new StringBuffer("");
			debitLeg.append(StringUtils.rightPad(DEBIT_ACCOUNT_NUMBER, 16, " "));
			debitLeg.append("INR49366301D");
			NumberFormat format = NumberFormat.getNumberInstance();
			format.setMaximumFractionDigits(2);
			format.setMinimumFractionDigits(2);
			format.setGroupingUsed(false);
			String value = format.format(debitAmount);
			debitLeg.append(StringUtils.leftPad(value, 17, " "));
			debitLeg.append("44-1714258-49200100-_13651");
			for (int i = 1; i <= 915; i++) {
				debitLeg.append(" ");
			}
			debitLeg.append("\r\n");
			creditLegs.append(debitLeg);
			FileUtils.writeStringToFile(ACCOUNTS, creditLegs.toString());
		} catch (Exception e) {
			System.out.println(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}
}
