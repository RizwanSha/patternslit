package com.patterns.framework.ejb;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.transaction.UserTransaction;



public abstract class AbstractDispatcher {

	@Resource
	protected SessionContext sessionContext;
	protected UserTransaction userTransaction;

	protected void beginTransaction() throws Exception {
		try {
			userTransaction = sessionContext.getUserTransaction();
			/* DEFAULT TIMEOUT AS 30 MINUTES */
			userTransaction.setTransactionTimeout(1800);
			userTransaction.begin();
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	protected void commitTransaction() throws Exception {
		userTransaction.commit();
	}

	protected void rollbackTransaction() {
		try {
			userTransaction.rollback();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
