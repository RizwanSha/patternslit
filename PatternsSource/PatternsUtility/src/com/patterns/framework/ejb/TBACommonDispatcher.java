/*
 * Copyright (c) 2014, 2015, Patterns Software Design Institute. All
 * rights reserved. Unauthorized copying of this file, via any medium is
 * strictly prohibited. PATTERNS SDI PROPRIETARY/CONFIDENTIAL. Use is
 * subject to license terms.
 */
package com.patterns.framework.ejb;

import java.rmi.RemoteException;

import patterns.config.framework.process.TBAProcessInfo;
import patterns.config.framework.process.TBAProcessResult;
import patterns.config.framework.service.DTObject;


/**
 * The Interface TBACommonDispatcher interface for the Common EJB
 */
public interface TBACommonDispatcher extends javax.ejb.EJBObject {

	/**
	 * Process request.
	 * 
	 * @param processInfo
	 *            the process info
	 * @return the TBA process result
	 * @throws RemoteException
	 *             the remote exception
	 */
	public TBAProcessResult processRequest(TBAProcessInfo processInfo) throws java.rmi.RemoteException;

	/**
	 * Process request.
	 * 
	 * @param inputDTO
	 *            the input dto
	 * @return the TBA process result
	 * @throws RemoteException
	 *             the remote exception
	 */
	public TBAProcessResult processRequest(DTObject inputDTO) throws java.rmi.RemoteException;
}
