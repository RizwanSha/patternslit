/*
 * Copyright (c) 2014, 2015, Patterns Software Design Institute. All
 * rights reserved. Unauthorized copying of this file, via any medium is
 * strictly prohibited. PATTERNS SDI PROPRIETARY/CONFIDENTIAL. Use is
 * subject to license terms.
 */
package com.patterns.framework.ejb;

import java.rmi.RemoteException;

import javax.ejb.CreateException;

/**
 * The Interface TBACommonDispatcherHome for CommonEJB
 */
public interface TBACommonDispatcherHome extends javax.ejb.EJBHome {

	/** The Constant COMP_NAME. */
	public static final String COMP_NAME = "java:comp/env/ejb/PortalBackOffice/CommonDispatcher";

	/** The Constant JNDI_NAME. */
	public static final String JNDI_NAME = "ejb/PortalBackOffice/CommonDispatcher";

	/**
	 * Creates the.
	 * 
	 * @return the com.patterns.framework.ejb. tba common dispatcher
	 * @throws CreateException
	 *             the create exception
	 * @throws RemoteException
	 *             the remote exception
	 */
	public com.patterns.framework.ejb.TBACommonDispatcher create() throws javax.ejb.CreateException, java.rmi.RemoteException;

}
