package com.patterns.framework.ejb;

import javax.ejb.Local;

import patterns.config.framework.process.TBAProcessInfo;
import patterns.config.framework.process.TBAProcessResult;
import patterns.config.framework.service.DTObject;

@Local
public interface CommonDispatcherLocal {

	public TBAProcessResult processRequest(TBAProcessInfo processInfo);

	public TBAProcessResult processRequest(DTObject processInfo);


}
