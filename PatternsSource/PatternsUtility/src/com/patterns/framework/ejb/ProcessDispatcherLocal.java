package com.patterns.framework.ejb;

import javax.ejb.Remote;

import patterns.config.framework.process.TBAProcessInfo;
import patterns.config.framework.process.TBAProcessResult;
import patterns.config.framework.service.DTObject;

@Remote
public interface ProcessDispatcherLocal {
	public TBAProcessResult processRequest(TBAProcessInfo processInfo);

	public TBAProcessResult processRequest(DTObject processInfo);
}
