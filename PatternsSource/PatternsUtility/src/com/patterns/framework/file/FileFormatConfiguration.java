package com.patterns.framework.file;

import java.io.Serializable;
import java.util.List;

public class FileFormatConfiguration implements Serializable {

	private static final long serialVersionUID = 159331038951891229L;

	public class FileFormatDeatails implements Serializable {

		private static final long serialVersionUID = -2046993937432006599L;
		private int columnSerial;
		private String columnName;
		private String columnDescription;
		private int columnType;
		private int columnLength;
		private String dateFormat;
		private String regex;

		public int getColumnSerial() {
			return columnSerial;
		}

		public void setColumnSerial(int columnSerial) {
			this.columnSerial = columnSerial;
		}

		public String getColumnName() {
			return columnName;
		}

		public void setColumnName(String columnName) {
			this.columnName = columnName;
		}

		public String getColumnDescription() {
			return columnDescription;
		}

		public void setColumnDescription(String columnDescription) {
			this.columnDescription = columnDescription;
		}

		public int getColumnType() {
			return columnType;
		}

		public void setColumnType(int columnType) {
			this.columnType = columnType;
		}

		public int getColumnLength() {
			return columnLength;
		}

		public void setColumnLength(int columnLength) {
			this.columnLength = columnLength;
		}

		public String getDateFormat() {
			return dateFormat;
		}

		public void setDateFormat(String dateFormat) {
			this.dateFormat = dateFormat;
		}

		public String getRegex() {
			return regex;
		}

		public void setRegex(String regex) {
			this.regex = regex;
		}
	}

	private String formatCode;
	private String fileType;
	private String formatType;
	private int noOfHeaderLines;
	private boolean skipHeaders;
	private int noOfFooterLines;
	private boolean skipFooters;
	private int noOfColumns;
	private String recordSeperator;
	private String customSeperator;
	private List<FileFormatDeatails> formatDetails;

	public String getFormatCode() {
		return formatCode;
	}

	public void setFormatCode(String formatCode) {
		this.formatCode = formatCode;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public String getFormatType() {
		return formatType;
	}

	public void setFormatType(String formatType) {
		this.formatType = formatType;
	}

	public int getNoOfHeaderLines() {
		return noOfHeaderLines;
	}

	public void setNoOfHeaderLines(int noOfHeaderLines) {
		this.noOfHeaderLines = noOfHeaderLines;
	}

	public boolean isSkipHeaders() {
		return skipHeaders;
	}

	public void setSkipHeaders(boolean skipHeaders) {
		this.skipHeaders = skipHeaders;
	}

	public int getNoOfFooterLines() {
		return noOfFooterLines;
	}

	public void setNoOfFooterLines(int noOfFooterLines) {
		this.noOfFooterLines = noOfFooterLines;
	}

	public boolean isSkipFooters() {
		return skipFooters;
	}

	public void setSkipFooters(boolean skipFooters) {
		this.skipFooters = skipFooters;
	}

	public int getNoOfColumns() {
		return noOfColumns;
	}

	public void setNoOfColumns(int noOfColumns) {
		this.noOfColumns = noOfColumns;
	}

	public String getRecordSeperator() {
		return recordSeperator;
	}

	public void setRecordSeperator(String recordSeperator) {
		this.recordSeperator = recordSeperator;
	}

	public String getCustomSeperator() {
		return customSeperator;
	}

	public void setCustomSeperator(String customSeperator) {
		this.customSeperator = customSeperator;
	}

	public List<FileFormatDeatails> getFormatDetails() {
		return formatDetails;
	}

	public void setFormatDetails(List<FileFormatDeatails> formatDetails) {
		this.formatDetails = formatDetails;
	}
}
