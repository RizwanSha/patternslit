package com.patterns.framework.file;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import com.patterns.framework.file.FileFormatConfiguration.FileFormatDeatails;

import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.loggers.ApplicationLogger;

public class FileConfigManager {
	private static final ReentrantReadWriteLock _READ_WRITE_LOCK_GLOBAL = new ReentrantReadWriteLock();
	private static final Lock _WRITE_GLOBAL = _READ_WRITE_LOCK_GLOBAL.writeLock();
	private final ApplicationLogger _LOGGER;

	private FileConfigManager() {
		_LOGGER = ApplicationLogger.getInstance(this.getClass().getSimpleName());
	}

	private Map<String, FilePurposeConfig> internalMap;
	private Map<String, FilePurposeConfig> internalLocalMap;

	private static class FileFormatConfigManagerHelper {
		private static final FileConfigManager _INSTANCE = new FileConfigManager();
	}

	public static FileConfigManager getInstance() {
		return FileFormatConfigManagerHelper._INSTANCE;
	}

	public final void unloadConfiguration() {
		try {
			_WRITE_GLOBAL.lock();
			resetConfiguration();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			_WRITE_GLOBAL.unlock();
		}
	}

	public final void reloadConfiguration() {
		try {
			_WRITE_GLOBAL.lock();
			loadConfiguration();
			resetConfiguration();
			initConfiguration();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			_WRITE_GLOBAL.unlock();
		}
	}

	public FilePurposeConfig getFilePurposeConfiguration(String entity, String filePurposeCode) {
		FilePurposeConfig fileFormatBean = null;
		String keyMap = "";
		try {
			keyMap = entity + "|" + filePurposeCode;
			fileFormatBean = internalMap.get(keyMap);
			if (fileFormatBean == null) {
				return null;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
		}
		return fileFormatBean;
	}

	private void initConfiguration() {
		internalMap.putAll(internalLocalMap);
		internalLocalMap.clear();
	}

	private void resetConfiguration() {
		_LOGGER.logDebug("resetConfiguration()");
		if (internalMap == null)
			internalMap = new HashMap<String, FilePurposeConfig>();
		internalMap.clear();
	}

	public void loadConfiguration() {
		_LOGGER.logDebug("loadConfiguration()");
		String sql = RegularConstants.EMPTY_STRING;
		String mapKey = RegularConstants.EMPTY_STRING;
		String entity = RegularConstants.EMPTY_STRING;
		if (internalLocalMap == null)
			internalLocalMap = new HashMap<String, FilePurposeConfig>();
		internalLocalMap.clear();
		DBContext dbContext = new DBContext();
		try {
			dbContext.openConnection();
			DBUtil dbUtil = dbContext.createUtilInstance();
			DBUtil innerDBUtil = dbContext.createUtilInstance();
			dbUtil.reset();
			sql = "SELECT ENTITY_CODE FROM ENTITIES WHERE ENTITY_DISSOLVED='0'";
			dbUtil.setSql(sql);
			ResultSet rset = dbUtil.executeQuery();
			while (rset.next()) {
				entity = rset.getString(1);
			}
			dbUtil.reset();
			sql = "SELECT FILE_PURPOSE_CODE,DESCRIPTION,PROCESSOR_CODE,FILE_FORMAT_CODE,SYSTEM_FOLDER_ID FROM FILEPURPOSE WHERE ENTITY_CODE=? AND ENABLED='1'";
			dbUtil.setSql(sql);
			dbUtil.setString(1, entity);
			rset = dbUtil.executeQuery();
			while (rset.next()) {
				mapKey = entity + "|" + rset.getString(1);
				FilePurposeConfig fp = new FilePurposeConfig();
				fp.setFileProcessor(rset.getString(1));
				fp.setFileProcessorName(rset.getString(2));
				String processorCode = rset.getString(3);
				String fileFormatCode = rset.getString(4);
				String systemFolderId = rset.getString(5);

				innerDBUtil.reset();
				sql = "SELECT NREGA_FILE_PATH FROM FILEDIRECTORIES WHERE ENTITY_CODE=? AND SYSTEM_FOLDER_ID=? AND ENABLED='1'";
				innerDBUtil.setSql(sql);
				innerDBUtil.setString(1, entity);
				innerDBUtil.setString(2, systemFolderId);
				ResultSet rs = innerDBUtil.executeQuery();
				if (rs.next()) {
					fp.setFilePath(rs.getString(1));
				}
				innerDBUtil.reset();

				innerDBUtil.reset();
				sql = "SELECT PROCESSOR_HANDLER,TRANSACTION_LINKED,LINKED_PROGRAM,SPLIT_THRESH_HOLD,MAX_ALLOW_THREADS FROM FILEPROCESSOR WHERE PROCESSOR_CODE=?";
				innerDBUtil.setSql(sql);
				innerDBUtil.setString(1, processorCode);
				rs = innerDBUtil.executeQuery();
				if (rs.next()) {
					fp.setHandler(rs.getString(1));
					fp.setTransactionLinked(rs.getString(2) == "1" ? true : false);
					fp.setLinkedProgram(rs.getString(3));
					fp.setSplitThreshHold(rs.getString(4));
					fp.setAllowedThreads(rs.getInt(5));
				}
				innerDBUtil.reset();

				innerDBUtil.reset();
				sql = "SELECT FILE_TYPE,FILE_FORMAT_TYPE,NO_HEADER_LINES,SKIP_HEADER_LINES,NO_OF_FOOTER_LINES,SKIP_FOOTER_LINES,NO_OF_COLUMNS,RECORD_SEPERATOR,CUSTOM_SEPERATOR FROM FILEFORMATS WHERE ENTITY_CODE=? AND FORMAT_CODE=? AND ENABLED='1'";
				innerDBUtil.setSql(sql);
				innerDBUtil.setString(1, entity);
				innerDBUtil.setString(2, fileFormatCode);
				rs = innerDBUtil.executeQuery();
				FileFormatConfiguration ffc = new FileFormatConfiguration();
				if (rs.next()) {
					ffc.setFormatCode(fileFormatCode);
					ffc.setFileType(rs.getString(1));
					ffc.setFormatType(rs.getString(2));
					ffc.setNoOfHeaderLines(rs.getInt(3));
					ffc.setSkipHeaders(decodeStringToBoolean(rs.getString(4)));
					ffc.setNoOfFooterLines(rs.getInt(5));
					ffc.setSkipFooters(decodeStringToBoolean(rs.getString(6)));
					ffc.setNoOfColumns(rs.getInt(7));
					ffc.setRecordSeperator(rs.getString(8));
					ffc.setCustomSeperator(rs.getString(9));
				}
				innerDBUtil.reset();

				List<FileFormatDeatails> formatDetailList = new ArrayList<FileFormatDeatails>();
				innerDBUtil.reset();
				sql = "SELECT COL_NAME,COL_DESCRIPTION,COL_TYPE,COL_LENGTH,COL_DATE_FMT,COL_VAL_EXPR,COL_SL FROM FILEFORMATDTL WHERE ENTITY_CODE=? AND FORMAT_CODE=?";
				innerDBUtil.setSql(sql);
				innerDBUtil.setString(1, entity);
				innerDBUtil.setString(2, fileFormatCode);
				rs = innerDBUtil.executeQuery();
				while (rs.next()) {
					FileFormatDeatails ffd = ffc.new FileFormatDeatails();
					ffd.setColumnName(rs.getString(1));
					ffd.setColumnDescription(rs.getString(2));
					ffd.setColumnType(rs.getInt(3));
					ffd.setColumnLength(rs.getInt(4));
					ffd.setDateFormat(rs.getString(5));
					ffd.setRegex(rs.getString(6));
					ffd.setColumnSerial(rs.getInt(7));
					formatDetailList.add(ffd);
				}
				innerDBUtil.reset();
				ffc.setFormatDetails(formatDetailList);
				fp.setFormatConfiguration(ffc);
				internalLocalMap.put(mapKey, fp);
			}
			dbUtil.reset();
		} catch (Exception e) {
			e.printStackTrace();
			_LOGGER.logError("loadConfiguration() Exception " + e);
		} finally {
			if (dbContext != null) {
				dbContext.close();
			}
		}
	}

	public static boolean decodeStringToBoolean(String value) {
		return (value != null && value.equals(RegularConstants.COLUMN_ENABLE)) ? true : false;
	}
}
