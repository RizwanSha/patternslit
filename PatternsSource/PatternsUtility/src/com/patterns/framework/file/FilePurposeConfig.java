package com.patterns.framework.file;

import java.io.Serializable;

public class FilePurposeConfig implements Serializable {

	private static final long serialVersionUID = -565411732889976089L;
	private String fileProcessor;
	private String fileProcessorName;
	private String handler;
	private boolean isTransactionLinked;
	private String linkedProgram;
	private String filePath;
	private String splitThreshHold;
	private int allowedThreads;
	private FileFormatConfiguration formatConfiguration;

	public String getFileProcessor() {
		return fileProcessor;
	}

	public void setFileProcessor(String fileProcessor) {
		this.fileProcessor = fileProcessor;
	}

	public String getFileProcessorName() {
		return fileProcessorName;
	}

	public void setFileProcessorName(String fileProcessorName) {
		this.fileProcessorName = fileProcessorName;
	}

	public String getHandler() {
		return handler;
	}

	public void setHandler(String handler) {
		this.handler = handler;
	}

	public boolean isTransactionLinked() {
		return isTransactionLinked;
	}

	public void setTransactionLinked(boolean isTransactionLinked) {
		this.isTransactionLinked = isTransactionLinked;
	}

	public String getLinkedProgram() {
		return linkedProgram;
	}

	public void setLinkedProgram(String linkedProgram) {
		this.linkedProgram = linkedProgram;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public FileFormatConfiguration getFormatConfiguration() {
		return formatConfiguration;
	}

	public void setFormatConfiguration(FileFormatConfiguration formatConfiguration) {
		this.formatConfiguration = formatConfiguration;
	}

	public String getSplitThreshHold() {
		return splitThreshHold;
	}

	public void setSplitThreshHold(String splitThreshHold) {
		this.splitThreshHold = splitThreshHold;
	}

	public int getAllowedThreads() {
		return allowedThreads;
	}

	public void setAllowedThreads(int allowedThreads) {
		this.allowedThreads = allowedThreads;
	}
}
