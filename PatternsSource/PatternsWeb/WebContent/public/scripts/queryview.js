function clearViewFields() {
	for (var i = 0; i < jsonQueryResponse.length; i++) {
		$('#' + jsonQueryResponse[i].columnName).val(EMPTY_STRING);
		$('#' + jsonQueryResponse[i].columnName + '_error').html(EMPTY_STRING);
	}
	queryGrid.clearAll();
}

function validateQueryForm(id) {

	for (var i = 0; i < jsonQueryResponse.length; i++) {
		var columnName = jsonQueryResponse[i].columnName;
		var ismandatory = jsonQueryResponse[i].mandatory;
		if (id == columnName) {
			validateField(columnName, $('#' + columnName), jsonQueryResponse[i].columnType, ismandatory);
			setFocusLast(jsonQueryResponse[i + 1].columnName);
			return;
		}
	}
}

function doQueryViewSubmit() {
	var arguments = '';
	var iserror = false;
	for (var i = 0; i < jsonQueryResponse.length; i++) {
		var columnType = jsonQueryResponse[i].columnType;
		var ismandatory = jsonQueryResponse[i].mandatory;
		var value = '';
		if (columnType != '6') {
			value = $('#' + jsonQueryResponse[i].columnName).val();
		} else {
			value = decodeBTS($('#' + jsonQueryResponse[i].columnName).val());
		}
		arguments += value + '|';
		if (!validateField(jsonQueryResponse[i].columnName, value, columnType, ismandatory)) {
			iserror = true;
		}
	}
	if (!iserror) {
		arguments = arguments.substring(0, arguments.length - 1);
		loadQueryGridView(CURRENT_PROGRAM_ID, TOKEN_ID, arguments);
	}
}

function validateField(column, value, type, ismandatory) {
	clearError(column + '_error');
	if (isEmpty(value)) {
		if (ismandatory) {
			setError(column + '_error', MANDATORY);
			return false;
		}

	} else {
		if (type == '1') {
			if (!isValidCode(value)) {
				setError(column + '_error', INVALID_FORMAT);
				return false;
			}
		} else if (type == '3') {
			if (!isNumeric(value)) {
				setError(column + '_error', INVALID_NUMBER);
				return false;
			}
		} else if (type == '4') {
			if (!isValidEffectiveDate(value, column + '_error')) {
				return false;
			}
		}
	}
	return true;

}

function doGridSelect(pkValue) {
	//ARIBALA ADDED ADDED BEGIN 09102015
	$('#rectify').val(COLUMN_DISABLE);
	//ARIBALA ADDED ADDED END 09102015
	if (!isModifyOperationAllowed()) {
		alert(OPERATION_NOT_ALLOWED);
		return;
	}
	
	hide('po_view2', $('#po_view2_pic'));
	show('po_view1', $('#po_view1_pic'));
	clearFields();
	$('#spanMode').html(MODIFY_DESC);
	$('#actionDescription').val(MODIFY_DESC);
	SOURCE_VALUE = MAIN;
	$('#action').val(MODIFY);
	loadMainValues(pkValue, true);
	PK_VALUE = pkValue;
	
	modify();
}
