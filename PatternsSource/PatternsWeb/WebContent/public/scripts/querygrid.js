var WidgetConstants = {};

WidgetConstants.PROGRESS_MESSAGE = 'Loading data...';
WidgetConstants.PROGRAM = '_PROGRAM';
WidgetConstants.TOKEN = '_TOKEN';
WidgetConstants.INIT = 'init';
WidgetConstants.ORDER_COLUMN = '_ORDERCOL';
WidgetConstants.SORT_ORDER = '_SORTORDER';
WidgetConstants.SORT_ASC = 'ASC';
WidgetConstants.SORT_DESC = 'DESC';
WidgetConstants.SEARCH_TEXT = '_SRCHTXT';
WidgetConstants.SEARCH_COLUMN = '_SRCHCOL';
WidgetConstants.ARGUMENT = '_ARGS';
WidgetConstants.START_POSITION = 'posStart';
WidgetConstants.BUFFER = 'count';
WidgetConstants.CONSOLE_WIDTH = 260 + 8 + 8 + 8 + 8;
WidgetConstants.MAX_WIDTH = window.screen.availWidth
		- WidgetConstants.CONSOLE_WIDTH;
WidgetConstants.GRID_IMAGE_PATH = 'public/cdn/vendor/dhtmlxGrid/codebase/imgs/';
WidgetConstants.FILE_IMAGE_PATH = 'public/cdn/vendor/dhtmlxVault/codebase/imgs/';
WidgetConstants.MODAL_WIDTH = 980;
WidgetConstants.MODAL_HEIGHT = 550;

function QueryGrid(_queryProgramID, _queryTokenID, _gridID, _queryArguments,
		_create, _queryLoad) {
	this.queryInit = 1;
	this.queryProgramID = _queryProgramID;
	this.queryTokenID = _queryTokenID;
	this.queryArguments = '';
	if (_queryArguments) {
		this.queryArguments = _queryArguments;
	}
	this.gridID = _gridID;
	this.queryLoad = false;
	if (_queryLoad) {
		this.queryLoad = true;
	}
	this.create = false;
	if (_create) {
		this.create = true;
	}
	if (this.create) {
		this.gridContainer = document.createElement('DIV');
		this.gridContainer.id = this.gridID + '_container';
		this.gridContainer.style.display = 'none';
		this.gridContainer.style.position = 'absolute';
		document.getElementsByTagName('BODY')[0]
				.appendChild(this.gridContainer);
		this.gridDiv = document.createElement('DIV');
		this.gridDiv.id = this.gridID + '_div';
		this.gridDiv.style.marginLeft = '0.5em';
		this.gridDiv.style.backgroundColor = 'transparent';
		this.gridDiv.style.zIndex = '202';
		this.gridDiv.style.overflow = 'hidden';
		this.gridDiv.height = '340px';
		this.gridDiv.width = '980px';
		this.gridContainer.appendChild(this.gridDiv);
		this.gridPA = document.createElement('DIV');
		this.gridPA.id = this.gridID + '_PA';
		this.gridPA.style.marginLeft = '0.5em';
		this.gridPA.style.backgroundColor = 'transparent';
		this.gridPA.style.zIndex = '202';
		this.gridPA.style.overflow = 'hidden';
		this.gridContainer.appendChild(this.gridPA);
		this.gridST = document.createElement('DIV');
		this.gridST.id = this.gridID + '_ST';
		this.gridST.style.fontSize = '12px';
		this.gridST.style.color = 'red';
		this.gridST.style.marginLeft = '0.5em';
		this.gridST.style.backgroundColor = 'transparent';
		this.gridST.style.zIndex = '202';
		this.gridST.style.overflow = 'hidden';
		this.gridContainer.appendChild(this.gridST);
	}
	var divID = _gridID + '_div';
	this.gridObject = new dhtmlXGridObject(divID);
	this.gridObject.setCustomParentComponent(this);
	this.gridObject.preventIECaching(true);
	this.gridObject.setImagePath(WidgetConstants.GRID_IMAGE_PATH);
	this.gridObject.attachEvent('onBeforeSorting', function(ind,
			_outerGridObject, direct) {
		var gridObjectRef = this;
		this.clearAll();
		this
				.setXMLAutoLoading(this.customParentComponent.queryGrid_QString
						+ (this.customParentComponent.queryGrid_QString
								.indexOf('?') >= 0 ? '&' : '?')
						+ WidgetConstants.ORDER_COLUMN
						+ '='
						+ (ind + 1)
						+ '&'
						+ WidgetConstants.SORT_ORDER
						+ '='
						+ (direct == 'asc' ? WidgetConstants.SORT_ASC
								: WidgetConstants.SORT_DESC));
		this.load(
				this.customParentComponent.queryGrid_QString
						+ (this.customParentComponent.queryGrid_QString
								.indexOf('?') >= 0 ? '&' : '?')
						+ WidgetConstants.ORDER_COLUMN
						+ '='
						+ (ind + 1)
						+ '&'
						+ WidgetConstants.SORT_ORDER
						+ '='
						+ (direct == 'asc' ? WidgetConstants.SORT_ASC
								: WidgetConstants.SORT_DESC), function() {
					gridObjectRef.setSortImgState(true, ind, direct);
				}, 'xml');

		return false;
	});
	this.gridObject.attachEvent('onBeforePageChanged', function(index, count) {
		return true;
	});
	// this.gridObject.setSkin('light');
	this.gridObject.enableAlterCss('even', 'uneven');
	this.gridObject.enableRowsHover(true, 'grid_hover');
	this.gridObject.enablePaging(true, 10, 6, this.gridID + '_PA', false);
//	this.gridObject.setPagingSkin('toolbar', 'dhx_web');
	this.gridObject.setPagingSkin('bricks');
	this.gridObject.attachEvent('onXLS',
			function(grid) {
				document.getElementById(grid.customParentComponent.gridID
						+ '_ST').innerHTML = WidgetConstants.PROGRESS_MESSAGE;
				document.getElementById(grid.customParentComponent.gridID
						+ '_ST').style.display = 'block';
				return true;
			});
	this.gridObject.attachEvent('onXLE',
			function(grid, rows, three, xml) {
		// changed by swaroopa as on 15-06-2015
				if (checkSessionAvailability(xml)) {
					document.getElementById(grid.customParentComponent.gridID
							+ '_ST').style.display = 'none';
					return true;
				}
			});
	this.gridObject.init();
	var queryString = AJAX_TOKEN + '=' + '1&' + WidgetConstants.PROGRAM + '='
			+ this.queryProgramID + '&' + WidgetConstants.TOKEN + '='
			+ this.queryTokenID + '&' + WidgetConstants.INIT + '='
			+ this.queryInit + '&' + WidgetConstants.ARGUMENT + '='
			+ this.queryArguments + '&ts=' + (new Date().getTime());
	this.queryGrid_QString = 'servlet/RecordFinderProcessor?' + queryString;
	this.gridObject.setXMLAutoLoading(this.queryGrid_QString);
	this.initQueryGrid();
	this.queryInit = 0;
	queryString = AJAX_TOKEN + '=' + '1&' + WidgetConstants.PROGRAM + '='
			+ this.queryProgramID + '&' + WidgetConstants.TOKEN + '='
			+ this.queryTokenID + '&' + WidgetConstants.INIT + '='
			+ this.queryInit + '&' + WidgetConstants.ARGUMENT + '='
			+ this.queryArguments + '&ts=' + (new Date().getTime());
	this.queryGrid_QString = 'servlet/RecordFinderProcessor?' + queryString;
	return this;
}

QueryGrid.prototype.attachEvent = function(eventID, callback) {
	this.gridObject.attachEvent(eventID, callback);
};

QueryGrid.prototype.toggleDisplay = function() {
	this.gridTB.style.width = this.gridDiv.style.width;
	if (this.gridContainer.style.display == 'block') {
		this.gridContainer.style.display = 'none';
	} else {
		this.gridContainer.style.display = 'block';
	}
};

QueryGrid.prototype.show = function() {
	this.gridContainer.style.display = 'block';
};

QueryGrid.prototype.hide = function() {
	this.gridContainer.style.display = 'none';
};

QueryGrid.prototype.setArguments = function(_arguments) {
	this.queryArguments = _arguments;
};

QueryGrid.prototype.setWidth = function(_width) {
	this.gridDiv.style.width = _width;
};

QueryGrid.prototype.setHeight = function(_height) {
	this.gridDiv.style.height = _height;
};

QueryGrid.prototype.setTop = function(_top) {
	this.gridContainer.style.top = _top;
};

QueryGrid.prototype.setLeft = function(_left) {
	this.gridContainer.style.left = _left;
};

QueryGrid.prototype.reset = function() {
	this.gridObject.clearAll();
};

QueryGrid.prototype.initQueryGrid = function() {
	this.gridObject.load(this.queryGrid_QString);
};

QueryGrid.prototype.getOneChecked = function() {
	var ros = EMPTY_STRING;
	ros = this.gridObject.getCheckedRows(0);
	var rosaerr = ros.split(',');
	if (ros == EMPTY_STRING) {
		return -1;
	} else {
		if (rosaerr.length > 1) {
			return -2;
		} else {
			return rosaerr[0];
		}
	}
};

QueryGrid.prototype.doQueryGridReload = function() {
	var queryStringExtension = '';
	for (var i = 0; i < this.gridObject.querySearchColumns.length; ++i) {
		queryStringExtension += ('qg' + this.gridObject.querySearchColumns[i]
				+ '=' + $('#qg' + this.gridObject.querySearchColumns[i]).val());
		if (i != this.gridObject.querySearchColumns.length - 1) {
			queryStringExtension += '&';
		}
	}

	var queryString = AJAX_TOKEN + '=' + '1&' + WidgetConstants.PROGRAM + '='
			+ this.queryProgramID + '&' + WidgetConstants.TOKEN + '='
			+ this.queryTokenID + '&' + WidgetConstants.INIT + '='
			+ this.queryInit + '&' + WidgetConstants.ARGUMENT + '='
			+ this.queryArguments + '&ts=' + (new Date().getTime());

	if (this.gridObject.querySearchColumns.length > 0) {
		queryString += '&' + queryStringExtension;
	}
	this.queryGrid_QString = 'servlet/RecordFinderProcessor?' + queryString;
	this.gridObject.clearAll();
	this.gridObject.setXMLAutoLoading(this.queryGrid_QString);
	this.initQueryGrid();
};

dhtmlXGridObject.prototype.setCustomParentComponent = function(id) {
	this.customParentComponent = id;
};

dhtmlXGridObject.prototype.adjustQueryGrid = function(gridWidth) {

	if (parseInt(gridWidth) > WidgetConstants.MAX_WIDTH) {
		$('#' + this.customParentComponent.gridID + '_div').css('width',
				WidgetConstants.MAX_WIDTH + 'px');
	} else {
		$('#' + this.customParentComponent.gridID + '_div').css('width',
				gridWidth + 'px');
	}

};

dhtmlXGridObject.prototype.setQueryGrid = function(searchColumns) {
	this.querySearchColumns = searchColumns.split('|');
};

dhtmlXGridObject.prototype._in_header_server_text_filter = function(t, i) {
	var ele = document.createElement('INPUT');
	ele.id = 'qg' + i;
	ele.type = 'text';
	ele.style.width = '90%';
	ele.style.fontSize = '8pt';
	ele.style.fontFamily = 'Tahoma';
	ele.parentComponent = this;
	ele.onkeyup = function(e) {
		doQueryFilter(this, e, this.parentComponent.customParentComponent);
	};
	t.innerHTML = '';
	t.appendChild(ele);
	t.style.textAlign = 'center';
	t.onclick = t.onmousedown = function(e) {
		(e || event).cancelBubble = true;
		return true;
	};
	t.onselectstart = function() {
		return (event.cancelBubble = true);
	};
	// this.makeFilter(t.firstChild,i);
};

dhtmlXGridObject.prototype._in_header_server_date_filter = function(t, i) {
	var ele = document.createElement('INPUT');
	ele.id = 'qg' + i;
	ele.type = 'text';
	ele.style.width = '96px';
	ele.style.fontSize = '8pt';
	ele.style.fontFamily = 'Tahoma';
	// ele.readOnly = true;
	ele.parentComponent = this;
	ele.onkeyup = function(e) {
		doQueryFilter(this, e, this.parentComponent.customParentComponent);
	};

	var imgEle = document.createElement('IMG');
	imgEle.id = 'qg' + i + '_pic';
	imgEle.src = 'public/styles/images/date.png';
	imgEle.className = 'calendar pic';
	imgEle.parentComponent = this;
	imgEle.onclick = function(e) {

		showCalendar(this, e);

	};

	t.innerHTML = '';
	t.appendChild(ele);

	t.appendChild(imgEle);

	t.style.textAlign = 'center';
	t.onclick = t.onmousedown = function(e) {
		(e || event).cancelBubble = true;
		return true;
	};
	t.onselectstart = function() {
		return (event.cancelBubble = true);
	};
	// this.makeFilter(t.firstChild,i);
};

dhtmlXGridObject.prototype._in_header_server_dateTime_filter = function(t, i) {
	var ele = document.createElement('INPUT');
	ele.id = 'qg' + i;
	ele.type = 'text';
	ele.style.width = '110px';
	ele.style.fontSize = '8pt';
	ele.style.fontFamily = 'Tahoma';
	// ele.readOnly = true;
	ele.parentComponent = this;
	ele.onkeyup = function(e) {
		doQueryFilter(this, e, this.parentComponent.customParentComponent);
	};

	var imgEle = document.createElement('IMG');
	imgEle.id = 'qg' + i + '_pic';
	imgEle.src = 'public/styles/images/datetime.png';
	imgEle.className = 'calendar pic';
	imgEle.parentComponent = this;
	imgEle.onclick = function(e) {

		showCalendar(this, e,true);

	};

	t.innerHTML = '';
	t.appendChild(ele);

	t.appendChild(imgEle);

	t.style.textAlign = 'center';
	t.onclick = t.onmousedown = function(e) {
		(e || event).cancelBubble = true;
		return true;
	};
	t.onselectstart = function() {
		return (event.cancelBubble = true);
	};
	// this.makeFilter(t.firstChild,i);
};

dhtmlXGridObject.prototype._in_header_help_search = function(t, i) {
	var ele = document.createElement('INPUT');
	ele.type = 'text';
	ele.style.width = '90%';
	ele.style.fontSize = '8pt';
	ele.style.fontFamily = 'Tahoma';
	ele.prototype.parentComponent = this;
	ele.onkeydown = function(e) {
		doSearch(event, this, '' + i + '', this.parentComponent);
	};
	t.appendChild(ele);
	t.onclick = t.onmousedown = function(e) {
		(e || event).cancelBubble = true;
		return true;
	};
	t.onselectstart = function() {
		return (event.cancelBubble = true);
	};
};

function doQueryFilter(obj, event, parentComponent) {
	_event = (window.event) ? window.event : event;
	var key = isPressKeyCode(_event);
	if (key == 13) {
		parentComponent.doQueryGridReload();
	} else if (key == 27) {
		hideCalendar();
	}
	return true;
}

var helpSearchColumn;
var helpArguments;
var helpProgramID;
var helpTokenID;
var helpSearchText;
var helpReturnField;
var helpGrid_QString;
var helpNotReturnDescription;
var helpGrid = null;
var helpInit;
var helpVisible = false;
var helpPreviousPage = 0;
var helpPageCount = 0;
function help(_helpProgramID, _helpTokenID, _helpSearchText, _helpArguments,
		_helpReturnField, _helpNotReturnDescription, _helpCallback) {
	helpVisible = true;
	helpInit = 1;
	helpProgramID = _helpProgramID;
	helpTokenID = _helpTokenID;
	helpSearchText = _helpSearchText;
	var HELPGRID_HEIGHT = 380;
	var HELPGRID_WIDTH = 540;
	// Optional Help Arguments
	if (_helpArguments == null) {
		_helpArguments = '';
	}
	helpArguments = _helpArguments;
	helpReturnField = _helpReturnField;
	if (!isEmpty(helpSearchText)) {
		helpSearchColumn = '0';
	} else {
		helpSearchColumn = '1';
	}
	helpCallback = false;
	if (_helpCallback) {
		helpCallback = _helpCallback;
	}
	helpNotReturnDescription = false;
	if (_helpNotReturnDescription) {
		helpNotReturnDescription = _helpNotReturnDescription;
	}
	$('#help_text').val(EMPTY_STRING);
	$('#help_combo').html(EMPTY_STRING);
	$('#helpGrid_handle').removeClass('hidden');
	$('#helpGrid_handle').addClass('block');
	try {
		var position;
		if ($('#' + _helpReturnField.attr('id') + '_pic').length > 0)
			position = $('#' + _helpReturnField.attr('id') + '_pic').position();
		else
			position = $('#' + _helpReturnField.attr('id')).position();
		/* ARIBALA CHANGED ON 16032015 BEG */
		var top = position.top;
		var left = position.left;
		var vWidth = Math.max(document.documentElement.clientWidth,
				window.innerWidth || 0);
		var vHeight = Math.max(document.documentElement.clientHeight,
				window.innerHeight || 0);

		/*if ((top + HELPGRID_HEIGHT) > vHeight) {
			top = vHeight - HELPGRID_HEIGHT;
		}*/
		var winscroll =window.scrollY;
		if ((top + HELPGRID_HEIGHT-winscroll) > vHeight) {
			top = top - HELPGRID_HEIGHT;
			if(top<0){
				top=0;
			}
		}
		
		if ((left + HELPGRID_WIDTH) > vWidth) {
			left = vWidth - HELPGRID_WIDTH;
		}
		/* ARIBALA CHANGED ON 16032015 END */
		$('#helpGrid_handle').css('left', left);
		$('#helpGrid_handle').css('top', top);
		$('#helpGrid_handle').css('position', 'absolute');
	} catch (e) {
		alert(e.message);
	}

	var gridID = 'helpGrid';
	if (helpGrid) {
		helpGrid.destructor();
	}
	helpGrid = new dhtmlXGridObject(gridID + '_div');
	helpGrid.preventIECaching(true);
	helpGrid.setImagePath(WidgetConstants.GRID_IMAGE_PATH);
	helpGrid.attachEvent('onBeforeSorting', function(ind, gridObj, direct) {
		this.clearAll();
		/*
		this.setXMLAutoLoading(helpGrid_QString
				+ (helpGrid_QString.indexOf('?') >= 0 ? '&' : '?')
				+ WidgetConstants.ORDER_COLUMN
				+ '='
				+ (ind + 1)
				+ '&'
				+ WidgetConstants.SORT_ORDER
				+ '='
				+ (direct == 'asc' ? WidgetConstants.SORT_ASC
						: WidgetConstants.SORT_DESC));
		*/
		this.load(helpGrid_QString
				+ (helpGrid_QString.indexOf('?') >= 0 ? '&' : '?')
				+ WidgetConstants.ORDER_COLUMN
				+ '='
				+ (ind + 1)
				+ '&'
				+ WidgetConstants.SORT_ORDER
				+ '='
				+ (direct == 'asc' ? WidgetConstants.SORT_ASC
						: WidgetConstants.SORT_DESC), function(gridObject) {
			helpGrid.setSortImgState(true, ind, direct);
			if (helpGrid.getRowsNum() > 0) {
				helpGrid.selectRow(0);
			}
		});

		return false;
	});

	helpGrid.attachEvent('onPageChanged', function(ind, fInd, lInd) {
		if (ind > helpPreviousPage) {
			helpGrid.selectRow(fInd);
		} else if (ind < helpPreviousPage) {
			helpGrid.selectRow(lInd - 1);
		} else {
			if (ind == 1) {
				helpGrid.selectRow(fInd);
			} else if (ind == helpPageCount) {

			}
		}
	});
	helpGrid.attachEvent('onBeforePageChanged', function(ind, count) {
		helpPreviousPage = ind;
		helpPageCount = count;
		return true;
	});

	
	
	helpGrid.setActive(true);
	helpGrid.enableAlterCss('even', 'uneven');
	helpGrid.enableRowsHover(true, 'grid_hover');
	helpGrid.enablePaging(true, 10, 6, gridID + '_PA', false);
	//helpGrid.setPagingSkin('toolbar', 'dhx_web');
	helpGrid.setPagingSkin('bricks');
	helpGrid.attachEvent('onXLS', function(grid) {
		$('#help_text').focus();
		$('#help_text').blur();
		$('#' + gridID + '_ST').html(WidgetConstants.PROGRESS_MESSAGE);
		$('#' + gridID + '_ST').show();
		return true;
	});
	helpGrid.attachEvent('onXLE', function(grid, rows, three, xml) {
		// changed by swaroopa as on 15-06-2015
		if (checkSessionAvailability(xml)) {
			$('#' + gridID + '_ST').hide();
			return true;
		}
	});
	helpGrid.attachEvent('onRowDblClicked', function(id, index) {
		helpReturnField.val(helpGrid.cells(id, 0).getValue());
		if (helpNotReturnDescription) {
			helpNotReturnDescription(helpGrid, id);
		} else {
			if ($('#' + helpReturnField.attr('id') + '_desc').length > 0) {
				$('#' + helpReturnField.attr('id') + '_desc').html(
						helpGrid.cells(id, 1).getValue());
			}
		}
		$('#helpGrid_handle').removeClass('block');
		$('#helpGrid_handle').addClass('hidden');
		try {
			helpVisible = false;
			helpReturnField.focus();
			if (helpCallback)
				helpCallback(helpGrid, id, index);
		} catch (e) {
		}
		return true;
	});

	helpGrid.attachEvent('onEnter', function(id, index) {
		helpReturnField.val(helpGrid.cells(id, 0).getValue());
		if (helpNotReturnDescription) {
			helpNotReturnDescription(helpGrid, id);
		} else {
			if ($('#' + helpReturnField.attr('id') + '_desc').length > 0) {
				$('#' + helpReturnField.attr('id') + '_desc').html(
						helpGrid.cells(id, 1).getValue());
			}
		}
		$('#helpGrid_handle').removeClass('block');
		$('#helpGrid_handle').addClass('hidden');
		try {
			helpVisible = false;
			helpReturnField.focus();
			if (helpCallback)
				helpCallback(helpGrid, id, index);
		} catch (e) {
		}
		return true;
	});
	helpGrid.init();
	var queryString = AJAX_TOKEN + '=' + '1&' + WidgetConstants.PROGRAM + '='
			+ helpProgramID + '&' + WidgetConstants.TOKEN + '=' + helpTokenID
			+ '&' + WidgetConstants.SEARCH_TEXT + '=' + helpSearchText + '&'
			+ WidgetConstants.ARGUMENT + '=' + helpArguments + '&'
			+ WidgetConstants.SEARCH_COLUMN + '=' + helpSearchColumn + '&'
			+ WidgetConstants.INIT + '=' + helpInit + '&ts='
			+ (new Date().getTime());
	helpGrid_QString = 'servlet/LookupProcessor?' + queryString;
	helpGrid.clearAll();
	helpGrid.setXMLAutoLoading(helpGrid_QString);
	initHelpGrid();
}

function initHelpGrid() {
	helpGrid.load(helpGrid_QString, function() {
		if (helpGrid.getRowsNum() > 0) {
			helpGrid.selectRow(0);
			
		}
	});
}
function doHelpGridReload() {
	helpInit = 0;
	var queryString = AJAX_TOKEN + '=' + '1&' + WidgetConstants.PROGRAM + '='
			+ helpProgramID + '&' + WidgetConstants.TOKEN + '=' + helpTokenID
			+ '&' + WidgetConstants.SEARCH_TEXT + '=' + $('#help_text').val()
			+ '&' + WidgetConstants.ARGUMENT + '=' + helpArguments + '&'
			+ WidgetConstants.SEARCH_COLUMN + '=' + $('#help_combo').val()
			+ '&' + WidgetConstants.INIT + '=' + helpInit + '&ts='
			+ (new Date().getTime());
	helpGrid_QString = 'servlet/LookupProcessor?' + queryString;
	helpGrid.clearAll();
	helpGrid.setXMLAutoLoading(helpGrid_QString);
	initHelpGrid();
}
dhtmlXGridObject.prototype.setLookupCombo = function(comboValues) {
	var comboArray = comboValues.split('|');
	if (document.getElementById('help_combo').options.length == 0) {
		for (var i = 0; i < comboArray.length; ++i) {
			var nameValue = comboArray[i].split('#');
			var option = null;
			if (document.all) {
				document.getElementById('help_combo').options[document
						.getElementById('help_combo').options.length] = new Option(
						nameValue[1], nameValue[0]);
			} else {
				option = document.createElement('option');
				option.value = nameValue[0];
				option.text = nameValue[1];
				document.getElementById('help_combo').add(option, null);
			}
		}
	}
};

function eXcell_chr(cell) {
	if (cell) {
		this.cell = cell;
		this.grid = this.cell.parentNode.grid;
		this.cell.obj = this;
	}

	this.disabledF = function(fl) {
		if ((fl == true) || (fl == 1))
			this.cell.innerHTML = this.cell.innerHTML.replace('item_chk0.',
					'item_chk0_dis.').replace('item_chk1.', 'item_chk1_dis.');
		else
			this.cell.innerHTML = this.cell.innerHTML.replace('item_chk0_dis.',
					'item_chk0.').replace('item_chk1_dis.', 'item_chk1.');
	};

	this.changeState = function() {
		// nb:
		if ((!this.grid.isEditable) || (this.cell.parentNode._locked)
				|| (this.isDisabled()))
			return;

		if (this.grid.callEvent('onEditCell', [ 0, this.cell.parentNode.idd,
				this.cell._cellIndex ])) {
			this.val = this.getValue();

			if (this.val == '1')
				this.setValue('0');
			else
				this.setValue('1');

			this.cell.wasChanged = true;
			// nb:
			this.grid.callEvent('onEditCell', [ 1, this.cell.parentNode.idd,
					this.cell._cellIndex ]);

			this.grid.callEvent('onCheckbox', [ this.cell.parentNode.idd,
					this.cell._cellIndex, (this.val != '1') ]);

			this.grid.callEvent('onCheck', [ this.cell.parentNode.idd,
					this.cell._cellIndex, (this.val != '1') ]);
		} else { // preserve editing (not tested thoroughly for this editor)
			this.editor = null;
		}
	};
	this.getValue = function() {
		return this.cell.chstate ? this.cell.chstate.toString() : '0';
	};

	this.isCheckbox = function() {
		return true;
	};
	this.isChecked = function() {
		if (this.getValue() == '1')
			return true;
		else
			return false;
	};

	this.setChecked = function(fl) {
		this.setValue(fl.toString());
	};
	this.detach = function() {
		return this.val != this.getValue();
	};
	this.edit = null;
}
eXcell_chr.prototype = new eXcell;
eXcell_chr.prototype.setValue = function(val) {
	this.cell.style.verticalAlign = 'middle'; // nb:to center checkbox in line
	// val can be int
	if (val) {
		val = val.toString()._dhx_trim();

		if ((val == 'false') || (val == '0'))
			val = '';
	}

	if (val) {
		val = '1';
		this.cell.chstate = '1';
	} else {
		val = '0';
		this.cell.chstate = '0';
	}
	var obj = this;
	this.setCValue("<img src='" + this.grid.imgURL + "item_chk" + val
			+ ".gif' onclick='(arguments[0]||event).cancelBubble=true;'>",
			this.cell.chstate);
};

dhtmlXGridObject.prototype._in_header_column_checkbox = function(t, i, c) {
	t.innerHTML = c[0] + "<input type='checkbox' />" + c[1];
	var self = this;
	t.firstChild.onclick = function(e) {
		self._build_m_order();
		var j = self._m_order ? self._m_order[i] : i;
		var val = this.checked ? 1 : 0;
		self.forEachRow(function(id) {
			var c = this.cells(id, j);
			if (c.isCheckbox())
				c.setValue(val);
			this.callEvent("onCheckbox", [ id, j, val ]);
		});
		(e || event).cancelBubble = true;
	};
};

function datesort_custom(a, b, order) {
	a = a.split(DATE_SEPARATOR);
	b = a.split(DATE_SEPARATOR);
	if (a[2] == b[2]) {
		if (a[1] == b[1])
			return (a[0] > b[0] ? 1 : -1) * (order == "asc" ? 1 : -1);
		else
			return (a[1] > b[1] ? 1 : -1) * (order == "asc" ? 1 : -1);
	} else
		return (a[2] > b[2] ? 1 : -1) * (order == "asc" ? 1 : -1);
}

function timesort_custom(a, b, order) {
	a = a.split(TIME_SEPARATOR);
	b = a.split(TIME_SEPARATOR);
	if (a[0] == b[0])
		return (a[1] > b[1] ? 1 : -1) * (order == "asc" ? 1 : -1);
	else
		return (a[0] > b[0] ? 1 : -1) * (order == "asc" ? 1 : -1);
}

function datetimesort_custom(a, b, order) {
	a0 = a.split(" ");
	b0 = b.split(" ");
	a1 = a0[0].split(DATE_SEPARATOR);
	b1 = b0[0].split(DATE_SEPARATOR);
	if (a1[2] == b1[2]) {
		if (a1[1] == b1[1]) {
			if (a1[0] == b1[0]) {
				a2 = a0[1].split(TIME_SEPARATOR);
				b2 = b0[1].split(TIME_SEPARATOR);
				if (a2[0] == b2[0]) {
					if (a2[1] == b2[1]) {
						return (a2[2] > b2[2] ? 1 : -1)
								* (order == "asc" ? 1 : -1);
					} else
						return (a2[1] > b2[1] ? 1 : -1)
								* (order == "asc" ? 1 : -1);
				} else
					return (a2[0] > b2[0] ? 1 : -1) * (order == "asc" ? 1 : -1);
			} else
				return (a1[0] > b1[0] ? 1 : -1) * (order == "asc" ? 1 : -1);
		} else
			return (a1[1] > b1[1] ? 1 : -1) * (order == "asc" ? 1 : -1);
	} else
		return (a1[2] > b1[2] ? 1 : -1) * (order == "asc" ? 1 : -1);
}
function smartCombo(_helpProgramID, _helpTokenID, _helpArguments,
		_helpReturnField) {
	helpInit = 1;
	helpProgramID = _helpProgramID;
	helpTokenID = _helpTokenID;
	helpArguments = '';
	helpReturnField = _helpReturnField;
	if (_helpArguments) {
		this.helpArguments = _helpArguments;
	}

	var queryString = AJAX_TOKEN + '=' + '1&' + WidgetConstants.PROGRAM + '='
			+ helpProgramID + '&' + WidgetConstants.TOKEN + '=' + helpTokenID
			+ '&' + WidgetConstants.ARGUMENT + '=' + helpArguments + '&'
			+ WidgetConstants.INIT + '=' + helpInit + '&ts='
			+ (new Date().getTime());
	helpGrid_QString = 'servlet/SmartLookupProcessor?' + queryString;

	var loader = dhx4.ajax.postSync(helpGrid_QString);

	var combo = helpReturnField[0].id;
	eval(combo).load(loader.xmlDoc.responseText, function() {
		eval(combo).selectOption(0);
	});
}