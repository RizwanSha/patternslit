var CGQM_PARENT_PROGRAM_ID = '_CGQMParentProgramId';
var CGQM_PARENT_PK = '_CGQMParentPK';
var CGQM_SOURCE_KEY = '_CGQMSourceKey';
var CGQM_SOURCE_VALUE = '_CGQMSourceValue';
var CGQM_REQUEST_TYPE = '_CGQMRequestType';
var CGQM_PARENT_ACTION = '_CGQMParentAction';
var CGQM_CONV_ID = '_CGQMConversationId';
var CRMTM_PARENT_PROGRAM_ID = '_ParentProgramID';
var CRMTM_PARENT_PRIMARY_KEY = '_ParentPrimaryKey';
var CRMTM_LINKED_PROGRAM_ID = '_LinkedProgramID';
var CRMTM_LINKED_PRIMARY_KEY = '_LinkedPrimaryKey';
var CRMTM_TOKEN_ID = '_TokenID';
var CRMTM_REQUEST_TYPE = '_CRMtmReqType';
var CRMTM_CONV_ID = '_CRMtmConversationId';
var MTM_PARENT_PROGRAM_ID = '_MtmParentProgramID';
var MTM_PARENT_PRIMARY_KEY = '_MtmParentPrimaryKey';
var MTM_LINKED_PROGRAM_ID = '_MtmLinkedProgramID';
var MTM_LINKED_PRIMARY_KEY = '_MtmLinkedPrimaryKey';
var POPUP_RESPONSE = EMPTY_STRING;

function loadComponentValues(primaryKey, errorField) {
	validator.clearMap();
	validator.setCMtm(true);
	validator.setValue(MTM_PARENT_PROGRAM_ID, PARENT_PROGRAM_ID);
	if (SOURCE_VALUE == MAIN) {
		validator.setValue(MTM_REQUEST_TYPE, MTM_MAIN);
	} else if (SOURCE_VALUE == TBA) {
		validator.setValue(MTM_REQUEST_TYPE, MTM_TBA);
	} else {
		validator.setValue(MTM_REQUEST_TYPE, MTM_GENERIC);
	}
	validator.setValue(MTM_PARENT_PRIMARY_KEY, PARENT_PRIMARY_KEY);
	validator.setValue(MTM_LINKED_PROGRAM_ID, LINK_PROGRAM_ID);
	validator.setValue(MTM_LINKED_PRIMARY_KEY, primaryKey);
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError(errorField, validator.getValue(ERROR));
		return false;
	} else if (validator.getValue(RESULT) == DATA_UNAVAILABLE) {
		setError(errorField, HMS_RECORD_NOT_EXISTS);
		return false;
	}
	return true;
}

function loadLinkedRecordData(fref, programID, tokenID, args) {
	validator.clearMap();
	validator.setCRMtm(true);
	if (SOURCE_VALUE == TBA) {
		validator.setValue(RMTM_REQUEST_TYPE, MTM_TBA);
	} else if (SOURCE_VALUE == MAIN) {
		validator.setValue(RMTM_REQUEST_TYPE, MTM_MAIN);
	} else {
		validator.setValue(RMTM_REQUEST_TYPE, MTM_GENERIC);
	}
	validator.setValue(CRMTM_PARENT_PROGRAM_ID, PARENT_PROGRAM_ID);
	validator.setValue(CRMTM_PARENT_PRIMARY_KEY, PARENT_PRIMARY_KEY);
	validator.setValue(RMTM_TOKEN_ID, tokenID);
	validator.setValue(CRMTM_LINKED_PROGRAM_ID, LINK_PROGRAM_ID);
	validator.setValue(CRMTM_LINKED_PRIMARY_KEY, args);
	validator.setValue(CRMTM_CONV_ID, CGQM_CONV_ID);
	validator.sendAndReceiveAsync(fref);
}

function loadComponentGridQuery(programID, tokenID, gridObject, arguments) {
	var _key = EMPTY_STRING;
	if (arguments) {
		if (isEmpty(PK_VALUE)) {
			_key = arguments;
		} else {
			_key = PK_VALUE + "|" + arguments;
		}
	} else {
		_key = PK_VALUE;
	}
	if (typeof gridObject === 'function') {
		var params = {};
		if (SOURCE_VALUE == TBA) {
			params = '_ParentProgramId=' + PARENT_PROGRAM_ID + '&_TokenID=' + tokenID + '&_ParentPrimaryKey=' + _key + '&_ReqType=' + 'T';

		} else if (SOURCE_VALUE == MAIN) {
			params = '_ParentProgramId=' + PARENT_PROGRAM_ID + '&_TokenID=' + tokenID + '&_ParentPrimaryKey=' + _key + '&_ReqType=' + 'M';

		} else if (SOURCE_VALUE == PROGRAM) {
			params = '_ParentProgramId=' + PARENT_PROGRAM_ID + '&_TokenID=' + tokenID + '&_ParentPrimaryKey=' + _key + '&_ReqType=' + 'B';

		} else {
			params = '_ParentProgramId=' + PARENT_PROGRAM_ID + '&_TokenID=' + tokenID + '&_ParentPrimaryKey=' + _key + '&_ReqType=' + 'S';
		}
		params = params + '&_LinkProgramId=' + LINK_PROGRAM_ID + '&_LinkPrimaryKey=' + LINK_PRIMARY_KEY + '&_CGQMConversationId=' + CGQM_CONV_ID;
		dhx4.ajax.post("servlet/ComponentGridQueryProcessor", encodeURI(params), function(responseXML) {
			gridObject(responseXML.xmlDoc.responseText);
		});
	} else {
		var queryPath = '';
		if (SOURCE_VALUE == TBA) {
			queryPath = 'servlet/ComponentGridQueryProcessor?_TokenID=' + tokenID + '&_ReqType=T';

		} else if (SOURCE_VALUE == MAIN) {
			queryPath = 'servlet/ComponentGridQueryProcessor?_TokenID=' + tokenID + '&_ReqType=M';

		} else if (SOURCE_VALUE == PROGRAM) {
			queryPath = 'servlet/ComponentGridQueryProcessor?_TokenID=' + tokenID + '&_ReqType=B';
		} else {
			queryPath = 'servlet/ComponentGridQueryProcessor?_TokenID=' + tokenID + '&_ReqType=S';
		}
		queryPath = queryPath + '&_ParentProgramId=' + PARENT_PROGRAM_ID + '&_ParentPrimaryKey=' + PARENT_PRIMARY_KEY + '&_LinkProgramId=' + LINK_PROGRAM_ID + '&_LinkPrimaryKey=' + LINK_PRIMARY_KEY + '&_CGQMConversationId=' + CGQM_CONV_ID;
		gridObject.loadXML(queryPath);
	}
}
function popupResponse(callBack) {
	return callBack(POPUP_RESPONSE);
}
