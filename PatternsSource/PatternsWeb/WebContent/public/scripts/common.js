/** Global Keys BEGIN* */
var REPORT_ID = 'reportID';
var valMode = true;
var lastFocus = '';
var CRL_FILE_EXTENSION = 'crl';
var CER_FILE_EXTENSION = 'cer';
var PROGRAM_KEY = 'PROGRAM';
var REPORT_KEY = 'REPORT';
var REPORT_FORMAT_KEY = 'REPORT_FORMAT';
var REPORT_FORMAT_PDF = '1';
var REPORT_FORMAT_XLS = '2';
var REPORT_FORMAT_DOC = '3';
var TIMESTAMP = '_timestamp';
var RESULT = 'result';
var RESULT_XML = 'result_xml';
var CONTENT = 'content';
var ERROR = 'error';
var ERROR_FIELD = 'error_field';
var STATUS ='status';
var ADDITIONAL_INFO='_AI';
var USER_MESSAGE = 'USER_MESSAGE';
var ZERO = '0';
var ONE = '1';
var YES = '1';
var NO = '0';
var DEBIT = '1';
var CREDIT = '2';
var WITHDRAW = '1';
var DEPOSIT = '2';
var SUCCESS='S';
var FAILURE='F';

var XML_VERSION = "<?xml version='1.0'?>";
var XML_ROWS_START = "<rows>";
var XML_ROWS_END = "</rows>";
var XML_ROW_END = "</row>";
var XML_CELL_START = "<cell>";
var XML_CELL_END = "</cell>";

var GQM_PRIMARY_KEY = '_GQMPrimaryKey';

var RMTM_PRIMARY_KEY = '_PrimaryKey';
var RMTM_TOKEN_ID = '_TokenID';
var RMTM_PROGRAM_ID = '_ProgramID';
var RMTM_REQUEST_TYPE = '_RMtmReqType';

var MTM_REQUEST_TYPE = '_MtmReqType';
var MTM_PRIMARY_KEY = '_MtmPrimaryKey';
var MTM_PROGRAM_ID = '_MtmProgramID';
var MTM_ALT_TABLE_NAME = '_MtmAltTable';
var MTM_MAIN = 'M';
var MTM_TBA = 'T';
var MTM_GENERIC = 'B';

var PK = 'pk';
var SOURCE = 'source';
var CALLSOURCE = 'callSource';
var REFLECTION_CLASS = '_ReflectionClass';
var REFLECTION_METHOD = '_ReflectionMethod';

var FILE_PURPOSE_PARAMETER = 'filePurpose';

var SUBMIT = 'submit';
var RESET = 'reset';

var AJAX_TOKEN = 'AJAX_TOKEN';

var DEFYEARPREFIX = '0000';
var MINTIME = '00:00';
var MAXTIME = '23:59';
var Keys = {};
Keys.UP_ARROW_KEY = 38;
Keys.DOWN_ARROW_KEY = 40;
Keys.ESC_KEY = 27;
Keys.ENTER_KEY = 13;
/** Global Keys END* */

/** Frequently Used Constants BEGIN* */
var EMPTY_STRING = '';
var NULL_VALUE = null;
var ADD = 'A';
var MODIFY = 'M';
var USAGE = 'U';
var PK_SEPERATOR = "|";
var ACTION = 'ACTION';
var RECTIFY = 'RECTIFY';
var COLUMN_ENABLE = '1';
var COLUMN_DISABLE = '0';
var DATA_AVAILABLE = COLUMN_ENABLE;
var DATA_UNAVAILABLE = COLUMN_DISABLE;
var MANDATORY_HTML = '<span style="color: red" >*</span>';

var COMMON_BHOPT_1 = '1';
var COMMON_BHOPT_2 = '2';

var COMMON_TFA_T = 'T';
var COMMON_TFA_D = 'D';

var COMMON_PYMTTYPE_1 = '1';
var COMMON_PYMTTYPE_2 = '2';

var COMMON_SERVICECATEGORY_1 = '1';
var COMMON_SERVICECATEGORY_2 = '2';
var COMMON_SERVICECATEGORY_3 = '3';

var COMMON_FILEDUPLICATE_0 = '0';
var COMMON_FILEDUPLICATE_1 = '1';
var COMMON_FILEDUPLICATE_2 = '2';

var COMMON_ROLES_1 = '1';
var COMMON_ROLES_2 = '2';
var COMMON_ROLES_3 = '3';
var COMMON_ROLES_4 = '4';
var COMMON_ROLES_5 = '5';
var COMMON_ROLES_6 = '6';

var COMMON_VISIBILITY_A = 'A';
var COMMON_VISIBILITY_B = 'B';

var COMMON_PWDDELIVERY_E = 'E';
var COMMON_PWDDELIVERY_S = 'S';
var COMMON_PWDDELIVERY_P = 'P';
var ADDITIONAL_DETAILS = '_AD';
/** Frequently Used Constants END* */

/** Global Links BEGIN */
var ELOGOUT_JSP = 'Renderer/common/elogout.jsp';
var EUNAUTHACCESS_JSP = 'Renderer/common/eunauthaccess.jsp';
var IMAGE_BASE_PATH = 'public/styles/images/';
/** Global Links END */

/** Global Variables BEGIN* */
var queryGrid = null;
var _calObject = null;
var _targetObject = null;
var _redisplay = false;

var SOURCE_VALUE = EMPTY_STRING;
var PK_VALUE = EMPTY_STRING;
var CALENDAR_STATUS = EMPTY_STRING;
/** Global Variables END* */
// Length Variables Begins
var LENGTH_0 = 0;
var LENGTH_1 = 1;
var LENGTH_2 = 2;
var LENGTH_3 = 3;
var LENGTH_4 = 4;
var LENGTH_5 = 5;
var LENGTH_6 = 6;
var LENGTH_7 = 7;
var LENGTH_8 = 8;
var LENGTH_9 = 9;
var LENGTH_10 = 10;
var LENGTH_11 = 11;
var LENGTH_12 = 12;
var LENGTH_15 = 15;
var LENGTH_25 = 25;

var LENGTH_50 = 50;
var LENGTH_100 = 100;

var jsonQueryResponse = EMPTY_STRING;
// Length Variables Ends
/** Calendar Functions BEGIN* */
function loadCalendar() {
	_calObject = new dhtmlxCalendarObject('calID', true, {
		isYearEditable : true,
		isMonthEditable : true
	});
	_calObject.attachEvent('onClick', selectDate);
	_calObject.setDateFormat(CALENDAR_DATE_FORMAT);
	_calObject.setSkin('dhx_web');
}

function showCalendar(ele, event, showTime) {
	_event = (window.event) ? window.event : event;
	if (_event.type == 'click' && _event.clientX == 0 && _event.clientY == 0)
		return false;
	if (document.all)
		dostopevent(_event);
	if (_targetObject != null && _targetObject == ele && _calObject.isVisible()) {
		_calObject.hide();
	} else {
		if (!$('#' + ele.id).hasClass('display')) {
			var position = $('#' + ele.id).offset();
			var top = position.top;
			var left = position.left;
			var vWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
			var vHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);

			/*
			 * if ((top + 210) > vHeight) { top = vHeight - 210; }
			 */
			var winscroll = window.scrollY;
			if ((top + 210 - winscroll) > vHeight) {
				top = top - 210;
			}
			if ((left + 180) > vWidth) {
				left = vWidth - 180;
			}
			left = left + 25;
			$('#calID').css('left', left);
			$('#calID').css('top', top);

			$('#calID').css('position', 'absolute');
			$('#calID').show();
			if (ele.id.indexOf('_pic') != -1) {
				var _textId = ele.id.substring(0, ele.id.indexOf('_pic'));
				_targetObject = document.getElementById(_textId);
			} else {
				var _textId = ele.id;
				_targetObject = document.getElementById(_textId);
			}

			if (showTime) {
				_calObject.setDateFormat(CALENDAR_DATE_TIME_FORMAT);
				if (isDateTime(_targetObject.value))
					_calObject.setDate(_targetObject.value);
				else
					_calObject.setDate(getCBDT());
				_calObject.showTime();
			} else {
				_calObject.setDateFormat(CALENDAR_DATE_FORMAT);
				if (isDate(_targetObject.value))
					_calObject.setDate(_targetObject.value);
				else
					_calObject.setDate(getCBD());
				_calObject.hideTime();
			}
			_calObject.show();
			CALENDAR_STATUS = 'SHOW';
		}
	}
}

function hideCalendar() {
	try {
		_calObject.hide();
		CALENDAR_STATUS = EMPTY_STRING;
		_targetObject.focus();

	} catch (e) {

	}
}

function selectDate(date) {
	_targetObject.value = _calObject.getFormatedDate(null, date);
	_calObject.hide();
	if (_targetObject && _targetObject.focus) {
		_targetObject.focus();
	}
}
/** Calendar Functions END* */

function docheckclick(event, ele) {
	if (checkclick) {
		if ($('#' + ele.id).is(':checked')) {
			$('#' + ele.id + "_desc").html("Yes");
		} else {
			$('#' + ele.id + "_desc").html("No");
		}
		checkclick(ele.id);
	}
}

function dochecklabelclick(event, ele) {
	if (checkclick) {
		checkclick(ele.id);
	}
}

function setCheckbox(id, value) {
	$('#' + id).prop('checked', decodeSTB(value));
	if (document.getElementById(id + '_desc')) {
		if ($('#' + id).is(':checked')) {
			$('#' + id + "_desc").html("Yes");
		} else {
			$('#' + id + "_desc").html("No");
		}
	}
}

/** Lookup Functions BEGIN* */
function showHelp(id, event) {
	// DEFECT ID - 20120306-16572 begin
	_event = (window.event) ? window.event : event;
	if (_event.type == 'click' && _event.clientX == 0 && _event.clientY == 0)
		return false;

	if (document.all)
		dostopevent(_event);
	// DEFECT ID - 20120306-16572 end
	var shiftPressed = _event.shiftKey;

	doHelp(id, shiftPressed);
}

function doHelp(id, shiftMode) {
}

function hideHelp() {
	$('#helpGrid_handle').removeClass('block');
	$('#helpGrid_handle').addClass('hidden');
	try {
		helpVisible = false;
		helpReturnField.focus();
	} catch (e) {
	}
}
/** Lookup Functions END* */

/** Form Utility Functions BEGIN* */

$.Shortcuts.add({
	type : 'down',
	mask : 'Enter',
	enableInInput : true,
	propogate : true,
	handler : dovalidate
});

$.Shortcuts.add({
	type : 'down',
	mask : 'Tab',
	enableInInput : true,
	handler : dovalidate
});

$.Shortcuts.add({
	type : 'hold',
	mask : 'Tab',
	enableInInput : true,
	handler : dotabhold
});

$.Shortcuts.add({
	type : 'down',
	mask : 'F2',
	enableInInput : true,
	handler : dobacktrack
});

$.Shortcuts.add({
	type : 'down',
	mask : 'F5',
	enableInInput : true,
	handler : doF5stop
});

$.Shortcuts.add({
	type : 'down',
	mask : 'Shift+F5',
	enableInInput : true,
	handler : doF5stop
});

$.Shortcuts.add({
	type : 'up',
	mask : 'F5',
	enableInInput : true,
	handler : doF5help
});

$.Shortcuts.add({
	type : 'up',
	mask : 'Shift+F5',
	enableInInput : true,
	handler : doShiftF5help
});

$.Shortcuts.add({
	type : 'up',
	mask : 'Esc',
	enableInInput : true,
	handler : doesc
});

$.Shortcuts.add({
	type : 'down',
	mask : 'F7',
	enableInInput : true,
	handler : deleteGrid
});

$.Shortcuts.add({
	type : 'down',
	mask : 'F10',
	enableInInput : true,
	handler : doWindowExit
});

$.Shortcuts.add({
	type : 'down',
	mask : 'F12',
	enableInInput : true,
	handler : exitGrid
});

$.Shortcuts.add({
	type : 'down',
	mask : 'Alt+A',
	enableInInput : true,
	handler : doadd
});

$.Shortcuts.add({
	type : 'down',
	mask : 'Alt+M',
	enableInInput : true,
	handler : domodify
});

$.Shortcuts.add({
	type : 'down',
	mask : 'Alt+V',
	enableInInput : true,
	handler : doqview
});

$.Shortcuts.add({
	type : 'down',
	mask : 'Alt+T',
	enableInInput : true,
	handler : dotemplate
});

$.Shortcuts.add({
	type : 'down',
	mask : 'Alt+P',
	enableInInput : true,
	handler : doqTBA
});

$.Shortcuts.add({
	type : 'down',
	mask : 'F6',
	enableInInput : true,
	handler : doopenwindow
});

//Added by Dileep on 28-09-2016 start
$.Shortcuts.add({
	type : 'down',
	mask : 'Alt+F',
	enableInInput : true,
	handler : showProgramSearch
});
//Added by Dileep on 28-09-2016 end


$.Shortcuts.start();

function validate(id) {
}

function fieldfocus(id) {
}

function deleteGrid(e) {
	e.preventDefault();
	var element = e.target;
	gridDeleteRow(element.id);
}

function gridDeleteRow(id) {

}

function gridExit(id) {

}

function doqTBA() {
	show('po_view3', $('#po_view3_pic'));
	hide('po_view1');
	show('po_view2');

}

function exitGrid(e) {
	e.preventDefault();
	var element = e.target;
	gridExit(element.id);
}

function doesc(e) {

	var element = e.target;

	if (element.id == "smartNavigate") {
		return true;
	}
	if (helpVisible) {
		hideHelp();
		return true;
	}
	if (!isEmpty(CALENDAR_STATUS)) {
		hideCalendar();
		return true;
	}
	if (getProgramClass() == "6") {
		return true;
	}
	if (viewOpen) {
		return true;
	}

	/* ARIBALA CHANGED ON 16032015 BEG */
	if (window.confirm(HMS_CLEAR_FIELDS)) {

		if (dhxWindows) {
			closeInlinePopUp(win);
			return true;
		}
		if ($('#action').val() == ADD) {
			doadd();
			return true;
		}
		if ($('#action').val() == MODIFY) {
			domodify();
			return true;
		}
		clearFields();
		return true;
	}
	return false;
	/* ARIBALA CHANGED ON 16032015 END */
}

function doWindowExit(e) {
	viewOpen = false;
	/*
	 * if (dhxWindows) { closeInlinePopUp(win); return; }
	 */
	if (typeof parent.CURRENT_PROGRAM_ID !== 'undefined') {
		parent.currentWindow.setModal(false);
		parent.currentWindow.hide();
		parent.currentWindow = null;
		dhxWindows = null;
		return;
	}
	if (currentWindow) {
		currentWindow.setModal(false);
		currentWindow.hide();
		currentWindow = null;

	}
}

function doF5stop(e) {
}

function doF5help(e) {

	var element = e.target;
	doHelp(element.id, false);
}

function doShiftF5help(e) {

	var element = e.target;
	doHelp(element.id, true);
}

function backtrack(id) {

}

function dobacktrack(e) {
	e.preventDefault();
	var element = e.target;
	if ($(element).hasClass('focus-input')) {
		$(element).removeClass('focus-input');
		$(element).addClass('blur-input');
	}
	if ($(element).hasClass('code') || $(element).hasClass('code-help'))
		$(element).val($(element).val().toUpperCase());
	var id = element.id;
	switch (id) {
	case 'cmdSubmit':
		setFocusLast(lastFocus);
		break;
	default:
		backtrack(id);
		break;
	}

}

function doopenwindow(e) {
	var element = e.target;
	if ($(element).hasClass('code')) {
		openwindow(element.id);
	}
}

function openwindow(id) {

}

function setFocusLast(id) {
	$('#' + id).focus();
	lastFocus = id;
}

function setFocusOnSubmit() {
	$('#cmdSubmit').focus();
}

function setFocusLastOnGridSubmit(id) {
	$('#' + id + '_add').focus();
}

function setFocusOnReset() {
	$('#cmdReset').focus();
}

function dotabhold(event) {
	return false;
}

function dovalidate(event) {
	var element = event.target;
	if ($(element).is('textarea')) {
		var value = $(element).val();
		var expectedIndex = value.length - 1;
		if (value.lastIndexOf("\n") != expectedIndex) {
			return true;
		}
	}
	if ($(element).hasClass('focus-input')) {
		$(element).removeClass('focus-input');
		$(element).addClass('blur-input');
	}
	if ($(element).hasClass('code') || $(element).hasClass('code-help')) {
		$(element).val($(element).val().toUpperCase());
	} else if ($(element).hasClass('date')) {
		$(element).val(formatDate($(element).val()));
	} else if ($(element).hasClass('time')) {
		$(element).val(formatTime($(element).val()));
	} else if ($(element).hasClass('amount')) {
		var amountVal = decodeEvalAmount($(element).val());
		if (amountVal != null)
			$(element).val(amountVal);
	} else if ($(element).hasClass('empCodeFormat')) {
		formatEmpCode(element);
	} else if ($(element).hasClass('dynamicIntDec')) {
		if (!isValidDynamicIntFractions(element)) {
			return false;
		}
	} else if ($(element).hasClass('btn btn-xs btn-default')) {
		$(element).trigger('click');
		return true;
	}

	var id = element.id;
	switch (id) {
	case 'cmdForwardPath':
		$('#' + id).trigger('click');
		break;
	case 'cmdBackToTemplate':
		$('#' + id).trigger('click');
		break;
	case 'cmdPrint':
		$('#' + id).trigger('click');
		break;
	case 'cmdSubmit':
		domanualsubmit(element, false);
		break;
	default:
		validate(id);
		validateQueryForm(id);
		break;
	}
	return false;
}

// added by mspr as on 12-06-2015 starts
function dokeypress(event) {
	var element = event.target;
	var value = $(element).val();
	if ($(element).hasClass('date')) {
		if (event.which >= 48 && event.which <= 57) {
			if (parseFloat(value.length) == 2 || parseFloat(value.length) == 5) {
				if (value.charAt(0) != '-' && value.charAt(0) != '+')// condition
					// avoid
					// auto
					// append
					// '-'
					// for
					// decode
					// text
					// to
					// date
					$(element).val($(element).val() + '-');
			}
		}
	} else if ($(element).hasClass('time')) {
		if (event.which >= 48 && event.which <= 57) {
			if (parseInt(value.length) == 2)
				$(element).val($(element).val() + ':');
		}
	}
}

// added by mspr as on 12-06-2015 ends

function formatDate(value) {
	var text = textToDate(value);
	if (text != value)
		return text;

	var len = parseFloat(value.length);
	if (len < 10) {
		var year = moment(getCBD(), SCRIPT_DATE_FORMAT).year();
		var month = moment(getCBD(), SCRIPT_DATE_FORMAT).month();
		switch (len) {
		case 1:
			value = "0" + value.charAt(0) + '-' + month + '-' + year;
			break;
		case 2:
			value += '-' + month + '-' + year;
			break;
		case 3:
			value += month + '-' + year;
			break;
		case 4:
			value = value.substring(0, value.length - 1) + "0" + value.charAt(3) + '-' + year;

			break;
		case 5:
			value += '-' + year;
			break;
		case 6:
			value += year;
			break;
		}
	}
	return value;
}

function textToDate(value) {
	var currDate = getCBD();
	var text = value.toLowerCase().replace(/ /g, '');
	if (text == 'today' || text == 't') {
		return currDate;
	} else if (text == 'tomorrow') {
		return moment(currDate, SCRIPT_DATE_FORMAT).add(1, 'days').format(SCRIPT_DATE_FORMAT);
	} else if (text == 'yesterday') {
		return moment(currDate, SCRIPT_DATE_FORMAT).add(-1, 'days').format(SCRIPT_DATE_FORMAT);
	} else if (text.indexOf('n') == 0 || text.indexOf('l') == 0 || text.indexOf('p') == 0 || text.indexOf('+') == 0 || text.indexOf('-') == 0) {
		var addValue = 1;
		var addText = 'days';
		if (text.indexOf('l') == 0 || text.indexOf('p') == 0) {
			addValue = (-1) * addValue;
		} else if (text.indexOf('+') == 0 || text.indexOf('-') == 0) {
			addValue = parseInt(text, 10);
			text = text.replace(addValue.toString(), '');
		}
		text = text.replace('next', '').replace('last', '').replace('previous', '').replace('+', '').replace('-', '').replace('p', '').replace('l', '').replace('n', '');
		if (text.indexOf('w') == 0)
			addValue = parseInt(addValue) * 7;
		else if (text.indexOf('m') == 0)
			addText = 'months';
		else if (text.indexOf('y') == 0)
			addText = 'years';
		else if (text.charAt(0) != 'd' && text.charAt(0) != '' && !isNumeric(text))
			return value;
		return moment(currDate, SCRIPT_DATE_FORMAT).add(addValue, addText).format(SCRIPT_DATE_FORMAT);
	}
	return value;
}

function formatTimeDB(value) {
	var len = parseInt(value.length);
	if (len == 5) {
		value = "0" + value;
	} else if (len == 4) {
		value = "00" + value;
	} else if (len == 3) {
		value = "000" + value;
	}/*
		 * else if (len == 2) { value = "0000" + value; }else if (len == 1) {
		 * value = "00000" + value; }
		 */
	value = value.substring(0, 2) + ":" + value.substring(2, 4);
	return value;

}

function formatTime(value) {
	value = unformatTime(value);
	var len = parseInt(value.length);
	if (len > 0 && len < 5) {
		if (len == 1) {
			value = "0" + value + "00";
		}
		if (len == 2) {

			value += "00";
		}
		if (len == 3) {
			value = "0" + value;
		}
		value = value.substring(0, 2) + ":" + value.substring(2, 4);
	}
	return value;
}

function unformatTime(value) {
	if (value.indexOf(":")) {
		value = value.replace(":", "");
	}
	return value;
}

function getTimeDiff(value1, value2) {
	var time1 = moment(value1, 'hh:mm');
	var time2 = moment(value2, 'hh:mm');
	var difference = time1.diff(time2, 'second');
	return difference;
}

function doblur(event, element) {
	if ($(element).hasClass('focus-input')) {
		$(element).removeClass('focus-input');
		$(element).addClass('blur-input');
	}
	if ($(element).hasClass('code') || $(element).hasClass('code-help'))
		$(element).val($(element).val().toUpperCase());
	// validate(element.id);

	if ($(element).hasClass('amount')) {
		var currencyId = element.id;
		currencyId = currencyId.replace("Format", "_curr");
		// checking with max length of Amount Field Begin
		if (!isEmpty($(element).val())) {
			regexSmallAmount = /^[-]?\d{0,15}(?:\.\d{0,3})?$/;// max length 19
			// (15+3+1(.))with
			// with out
			// commas
			// currency
			// small amount
			regexBigAmount = /^[-]?\d{0,27}(?:\.\d{0,3})?$/;// max length
			// 31(27+3+1(.))
			// with with out
			// commas currency
			// big amount
			setError((element.id).replace("Format", "_error"), EMPTY_STRING);
			if ($(element)[0].getAttribute('maxLength') == '25') {// max
				// length 25
				// with
				// commas
				// currency
				// small
				// amount
				if (!regexSmallAmount.test(unformatAmount($(element).val()))) {
					setError((element.id).replace("Format", "_error"), HMS_INVALID_SMALL_AMOUNT);
					return;
				}
			} else if ($(element)[0].getAttribute('maxLength') == '43') {// max
				// length
				// 43
				// with
				// commas
				// currency
				// big
				// amount
				if (!regexBigAmount.test(unformatAmount($(element).val()))) {
					setError((element.id).replace("Format", "_error"), HMS_INVALID_BIG_AMOUNT);
					return;
				}
			}
			// checking with max length of Amount Field End
			$(element).val(formatAmount($(element).val(), $('#' + currencyId).val()));
		}

	}
}

function dofocus(e, element) {
	var id = e.target.id;
	if (id != "smartNavigate") {
		if (typeof smartNavigate_ref !== 'undefined') {
			smartNavigate_ref.hide();
		}
	}
	if (!$(element).hasClass('focus-input')) {
		$(element).removeClass('blur-input');
		$(element).addClass('focus-input');
	}
	if ($(element).hasClass('amount')) {
		$(element).val(unformatAmount($(element).val()));
	}
	fieldfocus(element.id);

}

function checkclick() {

}

function change(element) {
}
function dochange(event, element) {
	var id = element.id;
	switch (id) {
	case 'scenario':
		onScenarioChange(id);
		break;
	default:
		change(element.id);
		break;

	}
}

function doclick(event) {
	_event = (window.event) ? window.event : event;
}

function docontextmenu(event) {
	alert(OPERATION_NOT_ALLOWED);
	return false;
}

function toggle(id, ref) {
	if ($('#' + id).hasClass('hidden')) {
		$('#' + id).removeClass('hidden');
		if ($(ref).length > 0) {
			$(ref).find('.pic').attr('src', IMAGE_BASE_PATH + 'maximized.png');
		}
	} else {
		$('#' + id).addClass('hidden');
		if ($(ref).length > 0) {
			$(ref).find('.pic').attr('src', IMAGE_BASE_PATH + 'minimized.png');
		}
	}
	return false;
}

function hide(id, ref) {
	if (!$('#' + id).hasClass('hidden')) {
		$('#' + id).addClass('hidden');
	}
	if (ref) {
		if ($(ref).length > 0) {
			$(ref).find('.pic').attr('src', IMAGE_BASE_PATH + 'minimized.png');
		}
	}
}

function show(id, ref) {
	if ($('#' + id).hasClass('hidden')) {
		$('#' + id).removeClass('hidden');
	}
	if (ref) {
		if ($(ref).length > 0) {
			$(ref).find('.pic').attr('src', IMAGE_BASE_PATH + 'maximized.png');
		}
	}
}

function decodeSTB(value) {
	if (value == COLUMN_ENABLE)
		return true;
	return false;
}

function decodeBTS(value) {
	if (value == true)
		return COLUMN_ENABLE;
	return COLUMN_DISABLE;
}

function setError(id, message) {
	$('#' + id).addClass('level4_error');
	$('#' + id).css('display', 'block');
	$('#' + id).html(message);
}

function clearError(id, message) {
	$('#' + id).addClass('level4_error');
	$('#' + id).css('display', 'inline');
	$('#' + id).html(EMPTY_STRING);
}

function setLoading() {
	if ($('#spanLoad')) {
		$('#spanLoad').html(LOADING);
		$('#spanLoad').css('display', 'block');
	}
}

function resetLoading() {
	if ($('#spanLoad')) {
		$('#spanLoad').html(EMPTY_STRING);
		$('#spanLoad').css('display', 'none');
	}
}

function revalidate() {
}

function doreset(element) {
	setCommand(RESET);
	$(element.form).trigger('submit', [ element, false ]);
	return true;
}

function domanualsubmit(element, ajax) {
	progress();
	$(element.form).trigger('submit', [ element, ajax ]);
}
var RECORD_NOT_UPDATED = "Record not updated";
var RECORD_UPDATED = "Record is ipdated";
var formSubmission = false;
var errors = 0;
function dosubmit(element, ajax) {
	if (formSubmission) {
		alert(SUBMITTING_FORM_PROGRESS);
		$("cmdSubmit").attr("disabled", true);
		return false;
	}
	if (!formSubmission) {
		formSubmission = true;
	}
	var formElement;
	if (element.form) {
		formElement = element.form;
	} else {
		formElement = element[0];
	}
	errors = 0;
	if (revalidate) {
		revalidate(element.id);
	}
	if (errors > 0) {
		formSubmission = false;
		$("cmdSubmit").attr("disabled", false);
		progress();
		setCommand(EMPTY_STRING);
		return false;
	} else {
		setCommand(SUBMIT);
		if (!tfaRequired(formElement, false)) {
			formSubmission = false;
			$("cmdSubmit").attr("disabled", false);
			return false;
		}
		if (ajax) {
			var parameters = serialize(formElement);
			dhx4.ajax.post($(formElement).attr("action"), parameters, function(responseText) {

				var jsonObject = JSON.parse(responseText.xmlDoc.responseText);

				try {
					// var keys = Object.keys(jsonObject.transaction);
					var action = jsonObject.transaction['status'];
					var additionalInfo = jsonObject.transaction['_AI'];
					if (action == COLUMN_ENABLE) {
						alert(RECORD_UPDATED);
						response(true, additionalInfo);
					} else {

						alert(RECORD_NOT_UPDATED);
						response(false, additionalInfo);

					}

					clearFields();

				} catch (e) {
					var keys = Object.keys(jsonObject.errors);
					for (var i = 0; i < keys.length; i++) {
						setError(keys[i] + '_error', jsonObject.errors[keys[i]]);

					}
				}
			});
			return true;
		} else {
			return true;
		}
		return false;
	}
}

function getFormResponse(response) {
	alert("got response");
}

function serialize(form) {
	if (!form || form.nodeName !== "FORM") {
		return;
	}
	var i, j, q = [];
	for (i = form.elements.length - 1; i >= 0; i = i - 1) {
		if (form.elements[i].name === "") {
			continue;
		}
		switch (form.elements[i].nodeName) {
		case 'INPUT':
			switch (form.elements[i].type) {
			case 'text':
			case 'hidden':
			case 'password':
			case 'button':
			case 'reset':
			case 'submit':
				q.push(form.elements[i].name + "=" + encodeURIComponent(form.elements[i].value));

				break;
			case 'checkbox':
			case 'radio':
				if (form.elements[i].checked) {
					q.push(form.elements[i].name + "=" + encodeURIComponent(form.elements[i].value));

				}
				break;
			case 'file':
				break;
			}
			break;
		case 'TEXTAREA':
			q.push(form.elements[i].name + "=" + encodeURIComponent(form.elements[i].value));

			break;
		case 'SELECT':
			switch (form.elements[i].type) {
			case 'select-one':
				q.push(form.elements[i].name + "=" + encodeURIComponent(form.elements[i].value));

				break;
			case 'select-multiple':
				for (j = form.elements[i].options.length - 1; j >= 0; j = j - 1) {
					if (form.elements[i].options[j].selected) {

						q.push(form.elements[i].name + "=" + encodeURIComponent(form.elements[i].options[j].value));

					}
				}
				break;
			}
			break;
		case 'BUTTON':
			switch (form.elements[i].type) {
			case 'reset':
			case 'submit':
			case 'button':
				q.push(form.elements[i].name + "=" + encodeURIComponent(form.elements[i].value));

				break;
			}
			break;
		}
	}
	return q.join("&");
}
// added by swaroopa as on 15-05-2012 begins
function tfaRequired(formContainer, isValidator) {
	if (getTFARequired() == COLUMN_ENABLE) {
		var data = EMPTY_STRING;
		if (isValidator) {
			data = formContainer.getPostString(false);
		} else {
			data = getPOSTData(formContainer);
		}
		try {
			document.SIGNAPPLET.signatureCreation(data);
			if (!isEmpty(document.SIGNAPPLET.getErrorMessage())) {
				alert(document.SIGNAPPLET.getErrorMessage());
				setTFAValue(EMPTY_STRING);
				setTFAEncodedValue(EMPTY_STRING);
				return false;
			} else {
				var encodedData = document.SIGNAPPLET.getSignedText();
				setTFAEncodedValue(encodedData);
				setTFAValue(data);
				return true;
			}
		} catch (e) {
			alert(TFA_JAVA_REQD + " \n " + e.message);
			return false;
		}
	} else {
		setTFAValue(EMPTY_STRING);
		setTFAEncodedValue(EMPTY_STRING);
		return true;
	}
}

// added by swaroopa as on 15-05-2012 ends

var getPOSTData = function(form) {
	return serializeToQueryString(form);
};

var getEncodedPOSTData = function(postData) {
	return postData;
};

function getSelectedOptions(el) {
	var rv = [];
	for (var i = 0; i < el.options.length; i++) {
		if (el.options[i].selected) {
			rv.push(el.options[i]);
		}
	}
	return rv;
};

function serializeToQueryString(form) {

	var rv = EMPTY_STRING;
	var inputs = form.elements;

	for (var k = 0; k < inputs.length; k++) {
		var el = inputs[k];

		if (el == null || el.nodeName == undefined)
			continue;

		var tagName = el.nodeName.toLowerCase();
		if (!(tagName == 'input' || tagName == 'select' || tagName == 'textarea'))
			continue;
		if (getIgnoreElements()[el.name] == COLUMN_ENABLE) {
			continue;
		}
		if (getDefaultIgnoreElements()[el.name] == COLUMN_ENABLE) {
			continue;
		}
		var type = el.type;
		if (!el.name || el.disabled || type == 'submit' || type == 'reset' || type == 'file' || type == 'image')

			continue;

		var value = (tagName == 'select') ? getSelectedOptions(el).map(function(opt) {

			return $(opt).value;

		}) : ((type == 'radio' || type == 'checkbox') && !el.checked) ? null : el.value;

		if (value != null)
			rv = rv + '&' + encodeURIComponent(el.name) + '=' + encodeURIComponent(value);

	}

	return (rv.length > 0) ? rv.substring(1) : rv;
};

function getDefaultIgnoreElements() {
	return {
		'tfaValue' : '1',
		'tfaEncodedValue' : '1'
	};
}

function getIgnoreElements() {
	return {};
}

function add() {
	// alert('Default ADD Method');
}

function modify() {
	alert('Default MODIFY Method');
}

function view() {
	alert('Default VIEW Method');
}

function clearFields() {

}
function doadd() {
	// ARIBALA ADDED ADDED BEGIN 09102015
	$('#rectify').val(COLUMN_DISABLE);
	// ARIBALA ADDED ADDED END 09102015
	if (!isAddOperationAllowed()) {
		alert(OPERATION_NOT_ALLOWED);
		return;
	}
	hide('po_view2', $('#po_view2_pic'));

	show('po_view1');
	clearFields();
	$('#action').val(ADD);
	$('#actionDescription').val(ADD_DESC);
	$('#spanMode').html(ADD_DESC);
	add();
}

function domodify() {
	// ARIBALA ADDED ADDED BEGIN 09102015
	$('#rectify').val(COLUMN_DISABLE);
	// ARIBALA ADDED ADDED END 09102015
	if (!isModifyOperationAllowed()) {
		alert(OPERATION_NOT_ALLOWED);
		return;
	}

	hide('po_view2', $('#po_view2_pic'));
	show('po_view1');
	clearFields();
	SOURCE_VALUE = PROGRAM;
	$('#action').val(MODIFY);
	$('#spanMode').html(MODIFY_DESC);
	$('#actionDescription').val(MODIFY_DESC);
	modify();
}

function loadPKValues(primaryKey, errorField) {
	validator.clearMap();
	validator.setMtm(true);
	validator.setValue(MTM_PROGRAM_ID, CURRENT_PROGRAM_ID);
	if (SOURCE_VALUE == MAIN) {
		validator.setValue(MTM_REQUEST_TYPE, MTM_MAIN);
	} else {
		validator.setValue(MTM_REQUEST_TYPE, MTM_GENERIC);
	}
	validator.setValue(MTM_PRIMARY_KEY, primaryKey);
	validator.setValue(MODIFY, COLUMN_ENABLE);
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError(errorField, validator.getValue(ERROR));
		return false;
	} else if (validator.getValue(RESULT) == DATA_UNAVAILABLE) {
		setError(errorField, HMS_RECORD_NOT_EXISTS);
		return false;
	}
	clearNonPKFields();
	if ($('#po_view1').hasClass('hidden')) {
		$('#po_view1').removeClass('hidden');
	}
	loadData();
	return true;
}

// this is for programs having one or more main tabkes to query
function loadPKSpecificValues(primaryKey, errorField, altTableName) {
	validator.clearMap();
	validator.setMtm(true);
	validator.setValue(MTM_PROGRAM_ID, CURRENT_PROGRAM_ID);
	if (SOURCE_VALUE == MAIN) {
		validator.setValue(MTM_REQUEST_TYPE, MTM_MAIN);
	} else {
		validator.setValue(MTM_REQUEST_TYPE, MTM_GENERIC);
	}
	validator.setValue(MTM_PRIMARY_KEY, primaryKey);
	validator.setValue(MTM_ALT_TABLE_NAME, altTableName);
	validator.setValue(MODIFY, COLUMN_ENABLE);
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError(errorField, validator.getValue(ERROR));
		return false;
	} else if (validator.getValue(RESULT) == DATA_UNAVAILABLE) {
		setError(errorField, HMS_RECORD_NOT_EXISTS);
		return false;
	}
	clearNonPKFields();
	if ($('#po_view1').hasClass('hidden')) {
		$('#po_view1').removeClass('hidden');
	}
	loadData();
	return true;
}

function doview(type) {
	if (!isViewOperationAllowed()) {
		alert(OPERATION_NOT_ALLOWED);
		return;
	}
	$('#_PIDCL').val('6');

	if (type) {
		view(type, PK_VALUE);
	} else {
		var result = MTM();
		if (result) {
			SOURCE_VALUE = result[0];
			if (result[0] == MAIN) {
				PK_VALUE = queryGrid.cells(result[1], 1).getValue();
			} else if (result[0] == TBA) {
				PK_VALUE = tbaGrid.cells(result[1], 1).getValue();
			}
			view(result[0], PK_VALUE);
		}
	}
}
// comments added by swaroopa
// 1. on row selection of query grid or tba grid and then on click of template
// button dotemplate() is called
// which loads the selected row data and sets action as add
// 2. If no row is selected from the grid and data is loaded through keyboard
// inputs and template button is clicked then
// dotemplate is called which sets action as add.
function dotemplate(sourceKey, primaryKey) {
	if (!isTemplateAllowed()) {
		alert(OPERATION_NOT_ALLOWED);
		return;
	}
	var result = MTM();
	var actionType = "";
	// Rizwan Changes done related to Back to Template on 24 Jun 2015 begins
	if (result) {
		SOURCE_VALUE = result[0];
		if (SOURCE_VALUE == MAIN)
			PK_VALUE = queryGrid.cells(result[1], 1).getValue();
		else {
			PK_VALUE = tbaGrid.cells(result[1], 1).getValue();
			actionType = tbaGrid.cells(result[1], 7).getValue();
		}
	} else if (primaryKey != null && result == false) {
		result = [ sourceKey, primaryKey ];
		PK_VALUE = primaryKey;
		SOURCE_VALUE = sourceKey;
	}
	// Rizwan Changes done related to Back to Template on 24 Jun 2015 ends
	if (result) {
		// commented by swaroopa as on 19-06-2015
		// setLoading();
		clearFields();
		if (SOURCE_VALUE == MAIN) {
			$('#spanMode').html(ADD_DESC);
			$('#actionDescription').val(ADD_DESC);
			$('#action').val(ADD);
			loadMainValues(PK_VALUE);

		} else if (SOURCE_VALUE == TBA) {
			if (actionType == ADD) {
				$('#spanMode').html(ADD_DESC);
				$('#action').val(ADD);
			} else if (actionType == MODIFY) {
				$('#spanMode').html(ADD_DESC);
				$('#action').val(ADD);
			}
			PK_VALUE = tbaGrid.cells(result[1], 1).getValue();
			loadTBAValues(PK_VALUE);
		}
	} else {
		$('#spanMode').html(ADD_DESC);
		$('#actionDescription').val(ADD_DESC);
		$('#action').val(ADD);
	}
}
function doTemplateCustomization() {

}

function stripWhitespace(str, replacement) {
	if (replacement == null)
		replacement = EMPTY_STRING;
	var result = str;
	var re = /\s/g;
	if (str.search(re) != -1) {
		result = str.replace(re, replacement);
	}
	return result;
}

function replaceall(str, findStr, replaceStr) {
	while (str.lastIndexOf(findStr) >= 0) {
		str = str.replace(findStr, replaceStr);
	}
	return str;
}

function isPressKeyCode(evt) {
	evt = (evt) ? evt : (window.event) ? event : null;
	if (evt) {
		var charCode = (evt.charCode) ? evt.charCode : ((evt.keyCode) ? evt.keyCode : ((evt.which) ? evt.which : 0));

		return charCode;
	}
	return 0;
}

function dostopevent(event) {
	try {
		event.stopPropagation();
	} catch (e) {
	}
	try {
		event.preventDefault();
	} catch (e) {
	}
	try {
		window.event.keyCode = 0;
	} catch (e) {
	}
	try {
		window.event.returnValue = false;
	} catch (e) {
	}
}

function timeChange(event, id) {
	try {
		$('#' + id).val($('#' + id + '_hh').val() + ':' + $('#' + id + '_mm').val());

	} catch (e) {
	}
}

function loadTimeValues(id) {
	var time = $('#' + id).val().split(':');
	$('#' + id + '_hh').val(time[0]);
	$('#' + id + '_mm').val(time[1]);
}

function clearTimeValues(id) {

	$('#' + id + '_hh').val(EMPTY_STRING);
	$('#' + id + '_mm').val(EMPTY_STRING);
}

function init() {
}

function doclearfields(id) {
}

/* MSPR 16062015 OVERLAY TOP THE TOP BEGIN */
function doMarginAdjust() {
	if ($('.navbar').length == 0) {
		$('.main-content').css('margin-top', '0px');
	}
}
/* MSPR 16062015 OVERLAY TOP THE TOP END */

function doload(e) {
	/* MSPR 16062015 OVERLAY TOP THE TOP BEGIN */
	doMarginAdjust();
	/* MSPR 16062015 OVERLAY TOP THE TOP END */
	$('form').attr('autocomplete', 'off');
	$(document).bind('keydown', function(event) {
		var doPrevent = false;
		if (event.keyCode === 8) {
			var d = event.srcElement || event.target;
			if ((d.tagName.toUpperCase() === 'INPUT' && (d.type

			.toUpperCase() === 'TEXT' || d.type.toUpperCase() === 'PASSWORD' || d.type.toUpperCase() === 'FILE' || d.type.toUpperCase() === 'EMAIL' || d.type.toUpperCase() === 'SEARCH' || d.type.toUpperCase() === 'DATE'))

			|| d.tagName.toUpperCase() === 'TEXTAREA') {
				doPrevent = d.readOnly || d.disabled;

			} else {

				doPrevent = true;
			}
		}

		if (doPrevent) {
			event.preventDefault();
		}

	});

	$('.code,.text,.date,.amount,.empCodeFormat,.patientReg').bind('keyup', function(e) {
		if (e.which != 13 && e.which != 9) {// MSPR changes for empty
			// field enter
			// keypress
			// calling clear fields
			var id = $(this).attr('id');
			if (isEmpty($(this).val())) {
				if ($('#' + id + '_desc').length) {
					$('#' + id + '_desc').html('');
				}
				doclearfields(id);
			}
		}

	});

	// changed by MSPR as on 12-06-2015
	$('.date').bind('keypress', dokeypress);
	$('.time').bind('keypress', dokeypress);

	if ($('#po_view2_pic').length) {
		$('#po_view2_pic').on('click', function() {
			if (!$('#po_view2').hasClass('hidden')) {
				doqview();
			}
		});
	}

	if ($('#po_view3').length) {
		toggle('po_view3', $('#po_view3_pic'));
	}

	/*
	 * if (!document.oncontextmenu) { document.oncontextmenu = docontextmenu; }
	 */

	if (_redisplay) {
		if ($('#actionDescription') && $('#spanMode')) {
			if ($('#actionDescription').val() == ADD_DESC || $('#actionDescription').val() == MODIFY_DESC || $('#actionDescription').val() == RECTIFY_DESC) {

				$('#spanMode').html($('#actionDescription').val());
			}
		}
	}
	loadCalendar();

	// ARIBALA CHANGED ON 10/06/2015 FOR MENU CHANGE BEGIN

	if (doDisplayMenu()) {
		slideMenu();
	}
	// ARIBALA ADDED END
	// Jainudeen changes 11/08/2016 begin
	if (getCBD() != EMPTY_STRING)
		setFormStyle();
	// Jainudeen changes 11/08/2016 end
	document.getElementById("reland").href = window.location.toString() + '#';
	init();
	// Rizwan Changes for Back To Template Begins
	if ($('#backToTemplate').val() == COLUMN_ENABLE) {
		dotemplate($('#sourceKey').val(), $('#primaryKey').val());
		// $('#backToTemplateAllowed').val(COLUMN_DISABLE);
	}
	// Rizwan Changes for Back To Template Ends
	// loadViewQuery();
	if (_redisplay) {
		hide('po_view2', $('#po_view2_pic'));

	} else {
		if ($('#add').length) {
			doadd();
		}
	}
}

/** Form Utility Functions END* */

/** XML Utility Functions BEGIN* */
function xmlHTTPValidator() {
	var xmlDOM = null;

	var mtmX = false;
	var rmtmX = false;
	// added on 23-06-2015
	var cmtmX = false;
	var crmtmX = false;
	// added on 23-06-2015
	if (document.all) {
		/* IE */
		xmlDOM = new ActiveXObject('Microsoft.XMLDOM');
		xmlDOM.loadXML('<rows><record></record></rows>');
	} else {
		var objDOMParser = new DOMParser();
		xmlDOM = objDOMParser.parseFromString('<rows><record></record></rows>', 'text/xml');

		xmlDOM.normalize();
	}
	this.XMLDOM = xmlDOM;
	this.getValue = getValueX;
	this.setValue = setValueX;
	this.containsKey = containsKeyX;
	this.containsValue = containsValueX;
	this.sendAndReceive = sendAndReceiveX;
	this.sendAndReceiveAsync = sendAndReceiveAsyncX;
	this.setClass = setClassX;
	this.getClass = getClassX;
	this.setMethod = setMethodX;
	this.getMethod = getMethodX;
	this.clearMap = clearMapX;
	this.getAllXML = getAllXMLX;
	this.setMtm = setMtmX;
	this.mtm = mtmX;
	// added on 23-06-2015
	this.setCMtm = setCMtmX;
	this.cmtm = cmtmX;
	this.setCRMtm = setCRMtmX;
	this.crmtm = crmtmX;
	// added on 23-06-2015
	this.setRMtm = setRMtmX;
	this.rmtm = rmtmX;
	this.reset = resetX;
	this.getPostString = getPostStringX;

	var responseFunction = null;

	function resetX() {
		this.mtm = false;
		this.rmtm = false;
		// added on 23-06-2015
		this.cmtm = false;
		this.crmtm = false;
		// added on 23-06-2015
		this.clearMap();
	}

	function setRMtmX(rmtmRequired) {
		this.rmtm = rmtmRequired;
	}

	function setMtmX(mtmRequired) {
		this.mtm = mtmRequired;
	}
	// added on 23-06-2015
	function setCRMtmX(crmtmRequired) {
		this.crmtm = crmtmRequired;
	}

	function setCMtmX(cmtmRequired) {
		this.cmtm = cmtmRequired;
	}
	// added on 23-06-2015
	function clearMapX() {
		if (document.all) {
			xmlDOM.loadXML('<rows><record></record></rows>');
		} else {
			while (xmlDOM.documentElement.hasChildNodes())

				xmlDOM.documentElement.removeChild(xmlDOM.documentElement.lastChild);

			xmlDOM.documentElement.appendChild(xmlDOM.createElement('record'));
		}
		this.mtm = false;
		this.rmtm = false;
		// added on 23-06-2015
		this.cmtm = false;
		this.crmtm = false;
		// added on 23-06-2015
	}
	function getValueX(KeyName) {
		try {
			if (document.all) {
				return xmlDOM.documentElement.childNodes(0).getElementsByTagName(KeyName)(0).text;

			} else {
				if (xmlDOM.documentElement.childNodes[0].getElementsByTagName(KeyName).length == 0) {

					return EMPTY_STRING;
				} else {
					if (xmlDOM.documentElement.childNodes[0].getElementsByTagName(KeyName)[0].childNodes.length == 0)

						return EMPTY_STRING;
					return xmlDOM.documentElement.childNodes[0].getElementsByTagName(KeyName)[0].firstChild.nodeValue;

				}
			}
		} catch (e) {
			return EMPTY_STRING;
		}
	}
	function setValueX(KeyName, Value) {
		try {
			if (document.all) {
				var Element = xmlDOM.createNode('element', KeyName, EMPTY_STRING);

				xmlDOM.documentElement.childNodes(0).appendChild(Element);

				xmlDOM.documentElement.childNodes(0).getElementsByTagName(KeyName)(0).text = Value;

			} else {

				xmlDOM.documentElement.childNodes[0].appendChild(xmlDOM.createElement(KeyName)).appendChild(xmlDOM.createTextNode(Value));

			}
		} catch (e) {
			alert('setValueX @@ ' + KeyName + " -- " + Value);
		}
	}
	function containsKeyX(keyName) {
		if (document.all) {
			nodeListObj = xmlDOM.documentElement.childNodes(0).getElementsByTagName(keyName)(0);

		} else {
			nodeListObj = xmlDOM.documentElement.childNodes[0].getElementsByTagName(keyName)[0];

		}
		if (nodeListObj == null) {
			return false;
		} else {
			return true;
		}
	}
	function containsValueX(keyName) {
		if (containsKeyX(keyName)) {
			if (document.all) {
				textNodeValue = xmlDOM.documentElement.childNodes(0).getElementsByTagName(keyName)(0).text;

			} else {
				textNodeValue = xmlDOM.documentElement.childNodes[0].getElementsByTagName(keyName)[0].firstChild.nodeValue;

			}
			if ((textNodeValue == null) || (textNodeValue == EMPTY_STRING)) {
				return false;
			} else {
				return true;
			}
		} else {
			return false;
		}
	}
	function setClassX(className) {
		setValueX(REFLECTION_CLASS, className);
	}
	function getClassX() {
		return getValueX(REFLECTION_CLASS);
	}
	function setMethodX(methodName) {
		setValueX(REFLECTION_METHOD, methodName);
	}
	function getMethodX() {
		return getValueX(REFLECTION_METHOD);
	}

	function sendAndReceiveX() {

		var postParameters = EMPTY_STRING;
		postParameters = convertXMLToPostData(xmlDOM, true);
		var responseXML = null;
		if (this.mtm) {
			responseXML = dhx4.ajax.postSync('servlet/MtmProcessor', AJAX_TOKEN + '=' + '1&' + postParameters);

		} else if (this.rmtm) {
			responseXML = dhx4.ajax.postSync('servlet/RecordQueryProcessor', AJAX_TOKEN + '=' + '1&' + postParameters);

		}

		// added On 23-06-2015
		else if (this.cmtm) {
			responseXML = dhx4.ajax.postSync('servlet/ComponentMtmProcessor', AJAX_TOKEN + '=' + '1&' + postParameters);

		} else if (this.crmtm) {
			responseXML = dhx4.ajax.postSync('servlet/ComponentRecordQueryProcessor', AJAX_TOKEN + '=' + '1&' + postParameters);

		}// added On 23-06-2015

		else {
			responseXML = dhx4.ajax.postSync('servlet/DataProcessor', AJAX_TOKEN + '=' + '1&' + postParameters);

		}

		var rtnXML = responseXML.xmlDoc.responseText;
		// changed by swaroopa as on 16/6/2016
		if (checkSessionAvailability(responseXML.xmlDoc)) {
			clearMapX();
			if (document.all) {
				xmlDOM.loadXML(rtnXML);
			} else {
				var objDOMParser = new DOMParser();
				xmlDOM = objDOMParser.parseFromString(rtnXML, 'text/xml');
				xmlDOM.normalize();
			}
		}
		return true;
	}

	function sendAndReceiveAsyncX(func) {

		responseFunction = func;
		var postParameters = convertXMLToPostData(xmlDOM, true);
		if (this.mtm) {
			dhx4.ajax.post('servlet/MtmProcessor', AJAX_TOKEN + '=' + '1&' + postParameters, doAsyncProcessingX);

		} else if (this.rmtm) {
			dhx4.ajax.post('servlet/RecordQueryProcessor', AJAX_TOKEN + '=' + '1&' + postParameters, doAsyncProcessingX);

		}

		// added on 23-06-2015
		else if (this.cmtm) {
			dhx4.ajax.post('servlet/ComponentMtmProcessor', AJAX_TOKEN + '=' + '1&' + postParameters, doAsyncProcessingX);

		} else if (this.crmtm) {
			dhx4.ajax.post('servlet/ComponentRecordQueryProcessor', AJAX_TOKEN + '=' + '1&' + postParameters, doAsyncProcessingX);

		}// added on 23-06-2015
		else {
			dhx4.ajax.post('servlet/DataProcessor', AJAX_TOKEN + '=' + '1&' + postParameters, doAsyncProcessingX);

		}
	}

	function doAsyncProcessingX(responseXML) {
		clearMapX();
		if (responseXML.xmlDoc.status == '200') {
			// var rtnXML = responseXML.doSerialization();
			var rtnXML = responseXML.xmlDoc.responseText;
			// changed by swaroopa as on 16/6/2016
			if (checkSessionAvailability(responseXML.xmlDoc)) {
				if (document.all) {
					xmlDOM.loadXML(rtnXML);
				} else {
					var objDOMParser = new DOMParser();
					xmlDOM = objDOMParser.parseFromString(rtnXML, 'text/xml');
					xmlDOM.normalize();
				}
				responseFunction();
			}
		} else {
			setValueX(ERROR, COMMUNICATION_FAILURE);
			responseFunction();
		}
	}

	function getAllXMLX() {
		if (document.all) {
			return xmlDOM.xml;
		} else {
			var objXMLSerializer = new XMLSerializer();
			var strXML = objXMLSerializer.serializeToString(xmlDOM);
			return strXML;
		}
	}

	function getPostStringX(encodeRequired) {
		return convertXMLToPostData(xmlDOM, encodeRequired);
	}
}

function convertXMLToPostData(xmlDocObject, encodeRequired) {
	var postString = EMPTY_STRING;
	var formLength = xmlDocObject.documentElement.childNodes[0].childNodes.length;
	for (var j = 0; j < formLength; j++) {
		var itemName = EMPTY_STRING;
		try {
			if (j == 0) {
				itemName = xmlDocObject.documentElement.childNodes[0].childNodes[0].tagName;
			} else {
				itemName = '&' + xmlDocObject.documentElement.childNodes[0].childNodes[j].tagName;

			}
		} catch (e) {
		}
		var itemValue = EMPTY_STRING;
		try {
			itemValue = xmlDocObject.documentElement.childNodes[0].childNodes[j].firstChild.nodeValue;
			if (encodeRequired) {
				itemValue = encodeURIComponent(itemValue);
			}
		} catch (e) {
		}
		postString = postString + itemName + '=' + itemValue;
	}
	return postString;
}

function parseXML(xmlString) {
	var xmlDoc = null;
	try {
		xmlDoc = new ActiveXObject('Microsoft.XMLDOM');
		xmlDoc.async = false;
		xmlDoc.loadXML(xmlString);
	} catch (e) {
		var parser = new DOMParser();
		xmlDoc = parser.parseFromString(xmlString, 'text/xml');
		xmlDoc.normalize();
	}
	return xmlDoc;
}

function getXML(DOMobj) {
	if (document.all) {
		return DOMobj.xml;
	} else {
		var xmlSerializer = new XMLSerializer();
		return xmlSerializer.serializeToString(DOMobj);
	}
}

function buildGridObject(xmlString, gridObject) {
	if (typeof (gridObject) == 'function') {
		var docObj = dhx4.ajax.parse(xmlString);
		gridObject(docObj);
		// var dhtmlLoader = new dhtmlXMLLoaderObject(gridObject, null, true,
		// true);
		// dhtmlLoader.loadXMLString(xmlString);
	}
}
/** XML Utility Functions END* */

/** Data Loading Functions BEGIN* */
function loadInlineQueryGrid(programID, tokenID, gridID, arguments) {
	return (new QueryGrid(programID, tokenID, gridID, arguments)).gridObject;
}
var MAIN_ARGUMENTS = '';
var ALT_TABLE_NAME = '';
function loadQueryGrid(programID, tokenID, arguments, altTableName) {
	ALT_TABLE_NAME = '';
	CURRENT_PROGRAM_ID = programID;
	TOKEN_ID = tokenID;
	MAIN_ARGUMENTS = arguments;
	if (altTableName)
		ALT_TABLE_NAME = altTableName;
}

var qviewLoaded = false;
function doqview() {
	hide('po_view1');
	show('po_view2', $('#po_view2_pic'));

	$('#spanMode').html(VIEW_DESC);
	$('#actionDescription').val(VIEW_DESC);

	if (!qviewLoaded) {
		loadViewQuery();
		qviewLoaded = true;
	}
}

function loadViewQuery() {
	try {
		var jsonResponse = null;
		jsonResponse = dhx4.ajax.postSync('servlet/QueryViewProcessor', encodeURI(AJAX_TOKEN + '=' + '1&' + '_PROGRAM=' + CURRENT_PROGRAM_ID + '&_TOKEN=' + TOKEN_ID));

		var response = JSON.parse(jsonResponse.xmlDoc.responseText);
		var result = response.result;
		jsonQueryResponse = response.content;
		if (result == COLUMN_ENABLE) {
			var tableData = '';
			var tableRow = '';
			if (jsonQueryResponse.length > 0) {
				for (var i = 0; i < jsonQueryResponse.length; i++) {
					var element = createElement(jsonQueryResponse[i]);
					tableRow = '';
					if (i % 2 != 0) {
						tableRow += "<tr class='row_odd' height='26'>";
						tableRow += element + "</tr>";
					} else {
						tableRow += "<tr class='row_even' height='26'>";
						tableRow += element + "</tr>";
					}
					tableData += tableRow;

				}
				tableRow = '';
				tableRow += "<tr class='row_even' height='26'>";
				tableRow += "<td colspan='2'>";
				tableRow += "<input type='button' class='btn btn-xs btn-primary' value='Search' onclick='doQueryViewSubmit()' />";
				tableRow += "<input type='button' class='btn btn-xs' value='Reset' onclick='clearViewFields()' />";
				tableRow += "</td>";
				tableRow += "</tr>";
				tableData += tableRow;
				$('#filters').html("<div class='table_wrap'><table class='form_table'>" + tableData + "</table></div>");

				// $$('po_view2').removeClass('hidden');
				$('#' + jsonQueryResponse[0].columnName).focus();
			} else {
				loadQueryGridView(CURRENT_PROGRAM_ID, TOKEN_ID, MAIN_ARGUMENTS);
			}
		} else {
			loadQueryGridView(CURRENT_PROGRAM_ID, TOKEN_ID, MAIN_ARGUMENTS);

		}
	} catch (e) {
	}
}

function createElement(jsonElement) {
	var columnName = jsonElement.columnName;
	var element = '';
	element += "<td width='180px'><label class='control-label'>" + jsonElement.columnDescription + "";

	if (jsonElement.mandatory) {
		element += "<span style='color: red'>*</span>";
	}
	element + "</label></td>";
	switch (jsonElement.columnType) {
	case '1':

		element += "<td><input type='text' id='" + columnName + "' class='code' maxlength='12' size='15'  onblur='doblur(event,this)' onfocus='dofocus(event,this)'/><span id='" + columnName + "_error' class='level4_error'></span></td>";

		break;
	case '2':
		element += "<td><input type='text' id='" + columnName + "' class='text' maxlength='100' size='100' onblur='doblur(event,this)' onfocus='dofocus(event,this)'/><span id='" + columnName + "_error' class='level4_error'></span></td>";

		break;
	case '3':
		element += "<td><input type='text' id='" + columnName + "' class='text' maxlength='12' size='15'  onblur='doblur(event,this)' onfocus='dofocus(event,this)'/><span id='" + columnName + "_error' class='level4_error'></span></td>";

		break;
	case '4':
		element += "<td><input type='text' id='" + columnName + "' class='date' maxlength='10' size='14'  onblur='doblur(event,this)' onfocus='dofocus(event,this)'/><input type='image' id='" + columnName

		+ "_pic' class='calendar pic ' src='public/styles/images/calendar.gif'  style='cursor: default' onclick='return false'/><span id='" + columnName + "_error' class='level4_error'></span></td>";

		break;
	case '5':
		element += "<td><select id='" + columnName + "' class='text' onchange='dochange(event,this)' onblur='doblur(event,this)' onfocus='dofocus(event,this)'>";

		var comboList = jsonElement.comboList;
		element += "<option value=''>--</option>";
		for ( var key in comboList) {
			element += "<option value='" + key + "'>" + comboList[key] + "</option>";

		}
		element += "</select><span id='" + columnName + "_error' class='level4_error'><span></td>";

		break;
	case '6':
		element += "<td><input type='checkbox' id='" + columnName + "' class='checkbox'   onblur='doblur(event,this)' onfocus='dofocus(event,this)'/><span id='" + columnName + "_error' class='level4_error'></span></td>";

		break;
	}
	return element;
}

function loadQueryGridView(programID, tokenID, arguments) {
	try {
		if (arguments) {
		} else {
			arguments = '';
		}
		queryGrid = (new QueryGrid(programID, tokenID, 'queryGrid', arguments)).gridObject;
		queryGrid.attachEvent('onEnter', doQueryGridDblClicked);
		queryGrid.attachEvent('onRowDblClicked', doQueryGridDblClicked);
	} catch (e) {
	}
}

function loadTBAGrid(programID, gridObject) {
	try {
		gridObject.loadXML('servlet/TBAProcessor?_ProgramID=' + programID, callBackTBAGrid);

		gridObject.attachEvent('onCheckbox', doTBAGridCheck);
		gridObject.attachEvent('onEnter', doTBAGridDblClicked);
		gridObject.attachEvent('onRowDblClicked', doTBAGridDblClicked);
	} catch (e) {
	}
}

function callBackTBAGrid() {
	var count = tbaGrid.getRowsNum();

	$('#tbaRowCount').html("(" + count + ")");

}

function doQueryGridDblClicked(rowID) {
	PK_VALUE = this.cells(rowID, 1).getValue();
	doview(MAIN);
}

function doTBAGridDblClicked(rowID) {
	PK_VALUE = this.cells(rowID, 1).getValue();
	doview(TBA);
}

function doTBAGridEditRow(value) {
	hide('po_view3', $('#po_view3_pic'));
	show('po_view1', $('#po_view1_pic'));
	clearFields();
	var values = value.split(",");
	PK_VALUE = values[0];
	var OPR = values[1];
	if (OPR == ADD) {
		if (!isAddOperationAllowed()) {
			alert(OPERATION_NOT_ALLOWED);
			return;
		}
		$('#spanMode').html(RECTIFY_DESC);
		$('#actionDescription').val(RECTIFY_DESC);
		$('#action').val(ADD);
	} else if (OPR == MODIFY) {
		if (!isModifyOperationAllowed()) {
			alert(OPERATION_NOT_ALLOWED);
			return;
		}
		$('#spanMode').html(RECTIFY_DESC);
		$('#actionDescription').val(RECTIFY_DESC);
		$('#action').val(MODIFY);
	}
	// ARIBALA ADDED ADDED BEGIN 09102015
	$('#rectify').val(COLUMN_ENABLE);
	// ARIBALA ADDED ADDED END 09102015
	loadTBAValues(PK_VALUE, true);
	modify();
}
function doQueryGridCheck(rowID) {
	var tbaRowID = getOneSelectedRecord(tbaGrid);
	if (!(tbaRowID < 0)) {
		tbaGrid.clearSelection();
		tbaGrid.cells(tbaRowID, 0).setValue(false);
	}
	queryGrid.clearSelection();
	queryGrid.selectRowById(rowID, true);
	return true;
}

function doTBAGridCheck(rowID) {
	var queryRowID = getOneSelectedRecord(queryGrid);
	if (!(queryRowID < 0)) {
		queryGrid.clearSelection();
		queryGrid.cells(queryRowID, 0).setValue(false);
	}
	tbaGrid.clearSelection();
	tbaGrid.selectRowById(rowID, true);
	return true;
}

function loadGridQuery(programID, tokenID, gridObject, arguments) {
	var _key = EMPTY_STRING;
	if (arguments) {
		if (isEmpty(PK_VALUE)) {
			_key = arguments;
		} else {
			_key = PK_VALUE + "|" + arguments;
		}
	} else {
		_key = PK_VALUE;
	}
	if (typeof gridObject === 'function') {
		var params = {};
		if (SOURCE_VALUE == TBA) {
			params = '_ProgramID=' + programID + '&_TokenID=' + tokenID + '&_GQMPrimaryKey=' + _key + '&_GQMReqType=' + 'T';

		} else if (SOURCE_VALUE == MAIN) {
			params = '_ProgramID=' + programID + '&_TokenID=' + tokenID + '&_GQMPrimaryKey=' + _key + '&_GQMReqType=' + 'M';

		} else if (SOURCE_VALUE == PROGRAM) {
			params = '_ProgramID=' + programID + '&_TokenID=' + tokenID + '&_GQMPrimaryKey=' + _key + '&_GQMReqType=' + 'B';

		}
		dhx4.ajax.post("servlet/GridQueryProcessor", encodeURI(AJAX_TOKEN + '=' + '1&' + params), function(responseXML) {

			gridObject(responseXML.xmlDoc.responseText);
		});
	} else {
		if (SOURCE_VALUE == TBA) {
			gridObject.loadXML('servlet/GridQueryProcessor?_ProgramID=' + programID + '&_TokenID=' + tokenID + '&_GQMPrimaryKey=' + _key + '&_GQMReqType=T');

		} else if (SOURCE_VALUE == MAIN) {
			gridObject.loadXML('servlet/GridQueryProcessor?_ProgramID=' + programID + '&_TokenID=' + tokenID + '&_GQMPrimaryKey=' + _key + '&_GQMReqType=M');

		} else if (SOURCE_VALUE == PROGRAM) {
			gridObject.loadXML('servlet/GridQueryProcessor?_ProgramID=' + programID + '&_TokenID=' + tokenID + '&_GQMPrimaryKey=' + _key + '&_GQMReqType=B');

		}
	}
}

var MAIN = 1;
var TBA = 2;
var PROGRAM = 0;
function MTM() {
	// changed by swaroopa
	var mainRowID = '';
	var tbaRowID = '';
	if (queryGrid) {
		mainRowID = queryGrid.getSelectedRowId();
		queryGrid.clearSelection();
		if (mainRowID != null) {
			return [ 1, mainRowID ];
		}

	}
	if (tbaGrid) {
		tbaRowID = tbaGrid.getSelectedRowId();
		tbaGrid.clearSelection();
		if (tbaRowID != null) {
			return [ 2, tbaRowID ];
		}

	}
	/*
	 * if(mainRowID == null && tbaRowID == null) { alert(SELECT_ATLEAST_ONE);
	 * return false; }
	 */

	return false;
}

function loadTBAValues(primaryKey, edit) {
	// added by jayashree as on 16/07/2015
	SOURCE_VALUE = TBA;
	PK_VALUE = primaryKey;
	validator.clearMap();
	validator.setMtm(true);
	validator.setValue(MTM_PROGRAM_ID, CURRENT_PROGRAM_ID);
	validator.setValue(MTM_REQUEST_TYPE, MTM_TBA);
	validator.setValue(MTM_PRIMARY_KEY, primaryKey);
	if (edit)
		validator.setValue(MODIFY, COLUMN_ENABLE);
	validator.sendAndReceiveAsync(filterData);
}

function loadMainValues(primaryKey, edit, altTableName) {
	// added by jayashree as on 16/07/2015
	SOURCE_VALUE = MAIN;
	PK_VALUE = primaryKey;
	validator.clearMap();
	validator.setMtm(true);
	validator.setValue(MTM_REQUEST_TYPE, MTM_MAIN);
	validator.setValue(MTM_PRIMARY_KEY, primaryKey);
	validator.setValue(MTM_PROGRAM_ID, CURRENT_PROGRAM_ID);
	if (altTableName) {
		validator.setValue(MTM_ALT_TABLE_NAME, altTableName);
	}

	if (edit)
		validator.setValue(MODIFY, COLUMN_ENABLE);
	validator.sendAndReceiveAsync(filterData);
}

function loadRecordData(fref, programID, tokenID, args) {
	validator.clearMap();
	validator.setRMtm(true);
	if (SOURCE_VALUE == TBA) {
		validator.setValue(RMTM_REQUEST_TYPE, MTM_TBA);
	} else if (SOURCE_VALUE == MAIN) { // added by jayashree as on 16/07/2015
		validator.setValue(RMTM_REQUEST_TYPE, MTM_MAIN);
	} else {
		validator.setValue(RMTM_REQUEST_TYPE, MTM_GENERIC);
	}
	validator.setValue(RMTM_PRIMARY_KEY, args);
	validator.setValue(RMTM_TOKEN_ID, tokenID);
	validator.setValue(RMTM_PROGRAM_ID, programID);
	validator.sendAndReceiveAsync(fref);
}

function filterData() {
	if (validator.getValue(RESULT) == DATA_UNAVAILABLE) {
		if (ALT_TABLE_NAME != '') {
			loadMainValues(PK_VALUE, true, ALT_TABLE_NAME);
			return;
		}
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			alert(validator.getValue(ERROR));
		} else {
			alert(NO_RECORD);
		}
		// resetLoading();

	} else {
		loadData();
		// Rizwan Changes for Back To Template Begins
		if ($('#backToTemplate').val() == COLUMN_ENABLE) {
			doTemplateCustomization();
			$('#backToTemplate').val(COLUMN_DISABLE);
		}
		// added by swaroopa as on 18/06/2015
		hide('po_view2', $('#po_view2_pic'));
		show('po_view1', $('#po_view1_pic'));

	}
}

var dhxWindows = null;
function initDHTMLXWindows() {
	if (dhxWindows) {
		dhxWindows.unload();
	}
	{
		dhxWindows = new dhtmlXWindows({
			image_path : "public/cdn/dhtmlxWindows/codebase/imgs/",
			skin : "dhx_blue"
		});
		// dhxWindows.setImagePath('public/dhtmlxWindows/codebase/imgs/');
		dhxWindows.attachEvent('onContentLoaded', function(_win) {
			_win.progressOff();
		});
	}
}

function showContent(id, contentID, width, height, title, removeClose) {
	initDHTMLXWindows();
	var win = getDHTMLXWindow(id, width, height, true, true);
	win.attachEvent("onClose", function(id) {
		if (oncloseInlinePopUp()) {
			id.detachObject();
			id.hide();
			$('body').attr('class', '');
			$('body').attr('style', '');
		}
		return true;
	});

	win.setText(title);
	win.attachObject(contentID);
	if ($('#' + contentID).hasClass('hidden'))
		$('#' + contentID).removeClass('hidden');
	win.button('minmax1').hide();
	win.button('minmax2').hide();
	win.button('park').hide();
	if (removeClose) {
		win.button('close').hide();
	}
	return win;
}

function showWindow(id, url, title, params, modal, move, maximize, width, height) {

	initDHTMLXWindows();
	var _width = WidgetConstants.MODAL_WIDTH;
	var _height = WidgetConstants.MODAL_HEIGHT;
	if (width) {
		_width = width;
	}
	if (height) {
		_height = height;
	}
	var win = getDHTMLXWindow(id, _width, _height, modal, move);
	win.progressOn();
	win.setText(title);
	if (!isEmpty(url)) {
		params = CALLSOURCE + '=' + 'C' + '&' + params;
		win.attachURL(url + '?' + params);
		win.attachEvent("onClose", function(id) {
			if (oncloseInlinePopUp(win)) {
				// id.detachObject(); // Fix to avoid init method of child
				// window page being called on close
				id.hide();
				$('body').attr('class', '');
				$('body').attr('style', '');
			}
			afterClosePopUp(win);
			return true;
		});
	} else {
		show(id);
		win.attachObject(id);
		win.attachEvent("onClose", function(id) {
			if (oncloseInlinePopUp(win)) {
				id.detachObject();
				id.hide();
				$('body').attr('class', '');
				$('body').attr('style', '');
			}
			afterClosePopUp(win);
			return true;
		});
		onloadInlinePopUp(params, id);
		win.progressOff();
	}

	if (maximize) {
		win.maximize();
	}
	return win;
}

function afterClosePopUp(win){
	
}

function getDHTMLXWindow(id, width, height, modal, move) {
	var win = null;
	if (!dhxWindows.window(id)) {
		win = dhxWindows.createWindow(id, 0, 0, width, height);
	} else {
		win = dhxWindows.window(id);
	}
	win.setModal(modal);
	win.center();
	if (!move)
		win.denyMove();
	// var currPosition = win.getPosition();
	// win.setPosition(currPosition[1], 0);
	win.button('minmax1').hide();
	win.button('minmax2').hide();
	win.button('park').hide();

	return win;
}

var currentWindow = null;

function onloadInlinePopUp(params, id) {
	// to set values or perform any operation on load

}
function oncloseInlinePopUp(win) {

	return true;
}

function closeInlinePopUp(win, valMode) {
	if (win != null && win.hide != null) {
		win.detachObject();
		win.hide();
		$('body').attr('class', '');
		$('body').attr('style', '');
		win.setModal(false);
		dhxWindows = null;
		updateInlinePopUp(valMode);
		return true;
	}
}

function updateInlinePopUp(valMode) {
	// perform operations on save to parent form
}

function getKeyValue() {
	var keyvalue = $('#primaryKey').val();
	return keyvalue;
}

var viewOpen = false;
function hideParent(_url, _source, _primaryKey, _width) {

	var id = 'window_auth';
	var url = getBasePath() + 'Renderer/' + _url + '.jsp';
	var params = PK + '=' + _primaryKey + '&' + SOURCE + '=' + _source;
	var title = '';
	if (_source == MAIN) {
		title = SOURCE_MAIN_VIEW;
	} else if (_source == TBA) {
		title = SOURCE_TBA_VIEW;
	}
	var modal = true;
	var width = 1020;
	if (_width)
		width = _width;
	var win = showWindow(id, url, title, params, modal, false, false, width, 650);
	win.attachEvent("onClose", function(id) {
		viewOpen = false;
		return true;
	});
	win.detachToolbar();
	var bar = win.attachToolbar({
		icons_path : "public/styles/images/toolbar/",
		xml : "Renderer/auth/view_toolbar.xml"
	});
	bar.attachEvent('onClick', function(id) {
		switch (id) {
		case 'auth':
			doview(MAIN);
			break;
		case 'unauth':
			doview(TBA);
			break;
		}
	});
	viewOpen = true;
	currentWindow = win;
}

function loadAuditFields(validator) {
	$('#createdBy').val(validator.getValue('CR_BY'));
	$('#createdBy_desc').html(validator.getValue('CR_BY_NAME'));
	$('#createdOn').val(validator.getValue('CR_ON'));
	$('#modifiedBy').val(validator.getValue('MO_BY'));
	$('#modifiedBy_desc').html(validator.getValue('MO_BY_NAME'));
	$('#modifiedOn').val(validator.getValue('MO_ON'));
	$('#authBy').val(validator.getValue('AU_BY'));
	$('#authBy_desc').html(validator.getValue('AU_BY_NAME'));
	$('#authOn').val(validator.getValue('AU_ON'));
}
/** Data Loading Functions END* */

// Added by Swaroopa
/** Request Functions BEGIN * */

function setAlternateTable(altTableName) {
	ALT_TABLE_NAME = altTableName;
}

function getProgramClass() {
	return $('#_PIDCL').val();
}

function isAddOperationAllowed() {
	if ($('#ADD_ALLOWED').val() == ZERO) {
		return false;
	}
	return true;
}

function isModifyOperationAllowed() {
	if ($('#MODIFY_ALLOWED').val() == ZERO) {
		return false;
	}
	return true;
}

function isViewOperationAllowed() {
	if ($('#VIEW_ALLOWED').val() == ZERO) {
		return false;
	}
	return true;
}

function isTemplateAllowed() {
	if ($('#TEMPLATE_ALLOWED').val() == ZERO) {
		return false;
	}
	return true;
}

/** Request Functions END * */

/** Session Functions BEGIN* */

function getPartitionNo() {
	return $('#_PN').val();
}
function getUserID() {
	return $('#_UID').val();
}

function getBaseCurrency() {
	return $('#_BC').val();
}
function getBaseCurrencyUnit() {
	return $('#_CU').val();
}

function getCBD() {
	return $('#_CBDS').val();
}

function getCBDT() {
	return $('#_CBDS').val() + " " + moment().format(SCRIPT_TIME_FORMAT);
}

function getEntityCode() {
	return $('#_EC').val();
}
function getEntityName() {
	return $('#_ON').val();
}
function getUserName() {
	return $('#_UN').val();
}

function getRoleType() {
	return $('#_RT').val();
}

function getRoleCode() {
	return $('#_RC').val();
}

function getCustomerCode() {
	return $('#_CC').val();
}

function getCustomerName() {
	return $('#_CN').val();
}

function getBranchCode() {
	return $('#_BRC').val();
}

function getBranchName() {
	return $('#_BRN').val();
}

function getBasePath() {
	return document.getElementsByTagName('base')[0].href;
}

function getDateFormat() {
	return $('#_DF').val();
}

function getPatientRegYearRequired() {
	return $('#_RYPR').val();
}

function getTFANonce() {
	return $('#tfaNonce').val();
}

function getTFARequired() {
	return $('#tfaRequired').val();
}

function setTFAValue(value) {
	$('#tfaValue').val(value);
}

function getTFAValue() {
	return $('#tfaValue').val();
}

function setTFAEncodedValue(value) {
	$('#tfaEncodedValue').val(value);
}

function getTFAEncodedValue() {
	return $('#tfaEncodedValue').val();
}

function setCommand(value) {
	$('#command').val(value);
}

function getCommand() {
	return $('#command').val();
}

function getProgramID() {
	if (typeof CURRENT_PROGRAM_ID === 'undefined') {
		CURRENT_PROGRAM_ID = "";
	}
	return CURRENT_PROGRAM_ID;
}

/** Session Functions END* */

/** Grid Functions BEGIN* */
function getOneSelectedRecord(gridVariable) {
	var ros = EMPTY_STRING;
	if (gridVariable == null)
		return -1;
	ros = gridVariable.getCheckedRows(0);
	var rosaerr = ros.split(',');
	if (ros == EMPTY_STRING) {
		return -1;
	} else {
		if (rosaerr.length > 1) {
			return -2;
		} else {
			return rosaerr[0];
		}
	}
}

function _grid_updateRow(gridvar, data) {
	var rowId = gridvar.getUserData("", "grid_edit_id");
	var updatedRowId = EMPTY_STRING;
	gridvar.setUserData("", "updated_row_id", EMPTY_STRING);
	if (gridvar.getUserData("", "insert_first")) {

		var uid = gridvar.uid();

		gridvar.addRow(uid, data, 0);
		gridvar.clearSelection();
		updatedRowId = uid;
	} else {
		if (isEmpty(rowId)) {
			var uid = gridvar.uid();

			var ros = gridvar.getCheckedRows(0);
			if (ros == EMPTY_STRING) {
				gridvar.addRow(uid, data);
				gridvar.clearSelection();
			} else {
				var rosaerr = ros.split(',');
				gridvar.addRow(uid, data, gridvar.getRowIndex(rosaerr[0]) + 1);
				gridvar.clearSelection();
			}
			updatedRowId = uid;
		} else {
			for (var i = 0; i < data.length; i++) {
				gridvar.cells(rowId, i).setValue(data[i]);
			}
			gridvar.clearSelection();
			updatedRowId = rowId;
			gridvar.setUserData("", "grid_edit_id", EMPTY_STRING);
		}

	}
	gridvar.setUserData("", "updated_row_id", updatedRowId);
	gridvar.showRow(updatedRowId);
	return updatedRowId;
}

function _grid_addFunction(fref, afref, gridvar, insertFirst) {
	var rowId = gridvar.getUserData("", "grid_edit_id");
	gridvar.setUserData("", "insert_first", false);
	var editMode = !isEmpty(rowId);
	if (editMode && insertFirst) {
		alert(GRID_EDIT_MODE_INSERT);
		return;
	}
	if (!editMode) {
		if (insertFirst) {
			gridvar.setUserData("", "insert_first", true);
		} else {
			var ros = gridvar.getCheckedRows(0);
			if (ros != EMPTY_STRING) {
				var rosaerr = ros.split(',');
				if (rosaerr.length > 1) {
					alert(SELECT_ONE);
					return;
				}
			}
		}
	}
	fref(editMode, rowId);

	var updatedRowId = gridvar.getUserData("", "updated_row_id");
	if (!isEmpty(updatedRowId)) {
		gridvar.setRowColor(updatedRowId, EMPTY_STRING);
		gridvar.setUserData("", "grid_edit_id", EMPTY_STRING);
		if (afref) {
			afref(updatedRowId);
		}
	}

	var continuousEdit = gridvar.getUserData("", "continous_edit");
	if (continuousEdit) {
		var rowId = gridvar.getUserData("", "updated_row_id");
		gridvar.cells(rowId, 0).setValue(false);
		var rowID = gridvar.getRowIndex(rowId) + 1;
		if (rowID < gridvar.getRowsNum()) {
			gridvar.cells((gridvar.getRowId(rowID)), 0).setValue(true);
			_grid_modifyFunction(gridvar.getUserData("", "editFunction"), gridvar, continuousEdit);

		}
	}

}

function _grid_modifyFunction(fref, gridvar, continuousEdit) {
	var ros = EMPTY_STRING;
	ros = gridvar.getCheckedRows(0);

	gridvar.setUserData("", "continous_edit", continuousEdit);
	gridvar.setUserData("", "editFunction", fref);

	var rosaerr = ros.split(',');
	if (ros == EMPTY_STRING) {
		alert(SELECT_ATLEAST_ONE);
	} else {
		if (rosaerr.length > 1) {
			alert(SELECT_ONE);
		} else {
			var rowId = rosaerr[0];
			gridvar.setUserData("", "grid_edit_id", rowId);
			gridvar.setRowColor(rowId, 'GREY');
			fref(rowId);
		}
	}
}

function _grid_cancelFunction(fref, gridvar) {
	var rowId = gridvar.getUserData("", "grid_edit_id");
	if (!isEmpty(rowId)) {
		fref(rowId);
		gridvar.setRowColor(rowId, EMPTY_STRING);
		gridvar.cells(rowId, 0).setValue(false);
		gridvar.setUserData("", "grid_edit_id", EMPTY_STRING);
	}

}

function _grid_clearFunction(gridvar) {
	if (confirm(CLEAR_CONFIRM)) {
		gridvar.clearAll();
	}
}

function deleteGridRecord(gridvar, fref, aref) {
	// alert(aref);
	_grid_deleteFunction(fref, gridvar, aref);
}

function _grid_deleteFunction(fref, gridvar, aref) {
	if (fref) {
		if (!fref()) {
			return;
		}
	}
	var ros = gridvar.getCheckedRows(0);
	var rosaerr = ros.split(',');
	if (ros == EMPTY_STRING) {
		alert(SELECT_ATLEAST_ONE);
	} else {
		if (confirm(DELETE_CONFIRM)) {
			for (var i = 0; i < rosaerr.length; i++) {
				gridvar.deleteRow(rosaerr[i]);
			}
			if (aref) {
				aref();
			}
		}
		gridvar.setUserData("", "grid_edit_id", EMPTY_STRING);
	}
}

function _grid_duplicate_check(gridvar, currentValue, compareIndices) {
	var rowCount = gridvar.getRowsNum();
	for (var row = 0; row < rowCount; row++) {
		var gridValue = new Array();
		for (var i = 0; i < compareIndices.length; ++i) {
			gridValue[i] = gridvar.cells2(row, compareIndices[i]).getValue();
		}
		var same = (currentValue.length == gridValue.length) && currentValue.every(function(element, index) {

			return element === gridValue[index];
		});
		if (same) {
			if (_is_grid_editMode(gridvar)) {
				var currentRowId = gridvar.getRowId(row);
				var editRowId = gridvar.getUserData("", "grid_edit_id");
				if (currentRowId == editRowId) {
					continue;
				}
			}
			return false;
		}
	}
	return true;
}

function _is_grid_editMode(gridvar) {
	var editRowId = gridvar.getUserData("", "grid_edit_id");
	return !isEmpty(editRowId);
}

/** Grid Functions END* */

/** Validation Functions BEGIN* */
function isEmpty(str) {
	return (str == null) || (str.length == 0);
}

function isEmail(str) {
	var re = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
	// var re =
	// /^([a-zA-Z0-9_\.\-\!\@\#\$\%\^\&\*\(\)\+\=\{\[\}\]\;\:\?\/\,\<\>\~\*])+\@(([a-zA-Z0-9_\.\-\!\@\#\$\%\^\&\*\(\)\+\=\{\[\}\]\;\:\?\/\,\<\>\~\*])+\.)+([a-zA-Z0-9_\.\-\!\@\#\$\%\^\&\*\(\)\+\=\{\[\}\]\;\:\?\/\,\<\>\~\*]{2,4})+$/;
	return re.test(str);
}

function isMobile(str) {
	var re = /^\d{10}$/;
	return re.test(str);
}

function isAlpha(str) {
	var re = /[^a-zA-Z]/g;
	if (re.test(str))
		return false;
	return true;
}
function isValidAmount(number) {
	var regex = /^\d+(\.\d{1,2})?$/;
	return regex.test(number);
}

function isValidThreeDigitTwoDecimal(element) {

	var regex = /^\d{0,3}(\.\d{0,2})?$/;
	if (!regex.test(element.val())) {
		setError(element.id + "_error", HMS_INVALID_NUMBER);
		return false;
	}
	return true;
}
function isNumeric(number) {
	var regex = /^[\-]?\d+$/;
	return regex.test(number);
}
function isNumericWithDecimal(number) {
	return parseFloat(number) == number;
}
function isZero(number) {
	var value = parseFloat(number);
	if (value == 0)
		return true;
	return false;
}
function isPositive(number) {
	var regex = /^\d+$/;
	return regex.test(number);
}

function isNegative(number) {
	var regex = /^[\-]\d+$/;
	return regex.test(number);
}

function isNumberGreater(number1, number2) {
	var numberDecimal1 = parseFloat(number1);
	var numberDecimal2 = parseFloat(number2);
	if (numberDecimal1 > numberDecimal2)
		return true;
	return false;
}

function isNumberGreaterThanEqual(number1, number2) {
	var numberDecimal1 = parseFloat(number1);
	var numberDecimal2 = parseFloat(number2);
	if (numberDecimal1 >= numberDecimal2)
		return true;
	return false;
}

function isNumberLesser(number1, number2) {
	var numberDecimal1 = parseFloat(number1);
	var numberDecimal2 = parseFloat(number2);
	if (numberDecimal1 < numberDecimal2)
		return true;
	return false;
}

function isNumberLesserThanEqual(number1, number2) {
	var numberDecimal1 = parseFloat(number1);
	var numberDecimal2 = parseFloat(number2);
	if (numberDecimal1 <= numberDecimal2)
		return true;
	return false;
}

function isNumberEqual(number1, number2) {
	var numberDecimal1 = parseFloat(number1);
	var numberDecimal2 = parseFloat(number2);
	if (numberDecimal1 == numberDecimal2)
		return true;
	return false;
}

function isLength(str, len) {
	return str.length == len;
}

function isLengthBetween(str, min, max) {
	return (str.length >= min) && (str.length <= max);
}

function isValidFreeText_50(code) {
	if (code == null || code.length > 50)
		return false;
	var regex = /^([A-Z0-9_])+$/;
	if (regex.test(code))
		return true;
	return false;
}

function isPhoneNumber(str) {
	if (isLength(str, 10)) {
		if (isNumeric(str)) {
			return true;
		}
	}
	return false;
	return re.test(str);
}

function isValidEffectiveDate(value, errorField) {
	var cbd = getCBD();

	if (isEmpty(value)) {
		setError(errorField, MANDATORY);
		return false;
	}
	if (!isDate(value)) {
		setError(errorField, INVALID_DATE);
		return false;
	}
	if (!isDateGreaterEqual(value, cbd)) {
		setError(errorField, DATE_GECBD);
		return false;
	}
	return true;
}

function isValidTime(value) {
	var regex = /^\d{0,2}(:){1}\d{0,2}$/;
	if (regex.test(value)) {
		var time = value.split(':');
		var hrs = time[0];
		var mm = time[1];
		if ((parseInt(hrs) >= 0 && parseInt(hrs) < 24) && (parseInt(mm) >= 0 && parseInt(mm) < 60)) {

			return true;
		}
	}
	return false;
}

function isTimeGreater(value1, value2) {
	var time1 = value1.split(':');
	var time2 = value2.split(':');
	var timevalue1 = time1[0] + time1[1];
	var timevalue2 = time2[0] + time2[1];
	return parseInt(timevalue1, 10) > parseInt(timevalue2, 10);
}

function isTimeGreaterEqual(value1, value2) {
	var time1 = value1.split(':');
	var time2 = value2.split(':');
	var timevalue1 = time1[0] + time1[1];
	var timevalue2 = time2[0] + time2[1];
	return parseInt(timevalue1, 10) >= parseInt(timevalue2, 10);
}

function isTimeLesser(value1, value2) {
	var time1 = value1.split(':');
	var time2 = value2.split(':');
	var timevalue1 = time1[0] + time1[1];
	var timevalue2 = time2[0] + time2[1];
	return parseInt(timevalue1, 10) < parseInt(timevalue2, 10);
}
function isTimeLesserEqual(value1, value2) {
	var time1 = value1.split(':');
	var time2 = value2.split(':');
	var timevalue1 = time1[0] + time1[1];
	var timevalue2 = time2[0] + time2[1];
	return parseInt(timevalue1, 10) <= parseInt(timevalue2, 10);
}
function isTimeEqual(date1, date2) {
	var time1 = value1.split(':');
	var time2 = value2.split(':');
	var timevalue1 = time1[0] + time1[1];
	var timevalue2 = time2[0] + time2[1];
	return parseInt(timevalue1, 10) == parseInt(timevalue2, 10);
}

function isDate(str) {
	/*
	 * if (str.split("-").length > 3) { return false; }
	 */
	var dateInstance = moment(str, SCRIPT_DATE_FORMAT, true);
	if (dateInstance == null || !dateInstance.isValid()) {
		return false;
	}
	return dateInstance;
}

function isDateGreater(date1, date2) {
	var dateInstance1 = moment(date1, SCRIPT_DATE_FORMAT);
	var dateInstance2 = moment(date2, SCRIPT_DATE_FORMAT);
	return dateInstance1.isAfter(dateInstance2);
}

function isDateGreaterEqual(date1, date2) {
	var dateInstance1 = moment(date1, SCRIPT_DATE_FORMAT);
	var dateInstance2 = moment(date2, SCRIPT_DATE_FORMAT);
	return dateInstance2.isBefore(dateInstance1) || dateInstance2.isSame(dateInstance1);

}

function isDateLesser(date1, date2) {
	var dateInstance1 = moment(date1, SCRIPT_DATE_FORMAT);
	var dateInstance2 = moment(date2, SCRIPT_DATE_FORMAT);
	return dateInstance1.isBefore(dateInstance2);
}

function isDateLesserEqual(date1, date2) {
	var dateInstance1 = moment(date1, SCRIPT_DATE_FORMAT);
	var dateInstance2 = moment(date2, SCRIPT_DATE_FORMAT);
	return dateInstance2.isAfter(dateInstance1) || dateInstance2.isSame(dateInstance1);

}

function isDateEqual(date1, date2) {
	var dateInstance1 = moment(date1, SCRIPT_DATE_FORMAT);
	var dateInstance2 = moment(date2, SCRIPT_DATE_FORMAT);
	return dateInstance1.isSame(dateInstance2);
}

function getDateTimeWithFormat(str) {
	var dateInstance = moment(str, SCRIPT_DATE_TIME_FORMAT);
	if (dateInstance == null || !dateInstance.isValid()) {
		return false;
	}
	return str.substring(0, str.lastIndexOf(":"));
}

function getDateWithoutTime(str) {
	var dateInstance = moment(str, SCRIPT_DATE_TIME_FORMAT);
	if (dateInstance == null || !dateInstance.isValid()) {
		return false;
	}
	var tempDate = dateInstance.clone().hour(0).minute(0).second(0).millisecond(0);
	return tempDate.format(SCRIPT_DATE_FORMAT);
}

// Validation with Date and Time
function isDateTime(str) {
	if (str.length != SCRIPT_DATE_TIME_FORMAT.length) {
		return false;
	}
	var dateInstance = moment(str, SCRIPT_DATE_TIME_FORMAT);
	if (dateInstance == null || !dateInstance.isValid()) {
		return false;
	}
	return dateInstance;
}
function isDateTimeGreater(date1, date2) {
	var dateInstance1 = moment(date1, SCRIPT_DATE_TIME_FORMAT);
	var dateInstance2 = moment(date2, SCRIPT_DATE_TIME_FORMAT);
	return dateInstance1.isAfter(dateInstance2);
}

function isDateTimeGreaterEqual(date1, date2) {
	var dateInstance1 = moment(date1, SCRIPT_DATE_TIME_FORMAT);
	var dateInstance2 = moment(date2, SCRIPT_DATE_TIME_FORMAT);
	return dateInstance2.isBefore(dateInstance1) || dateInstance2.isSame(dateInstance1);

}

function isDateTimeLesser(date1, date2) {
	var dateInstance1 = moment(date1, SCRIPT_DATE_TIME_FORMAT);
	var dateInstance2 = moment(date2, SCRIPT_DATE_TIME_FORMAT);
	return dateInstance1.isBefore(dateInstance2);
}

function isDateTimeLesserEqual(date1, date2) {
	var dateInstance1 = moment(date1, SCRIPT_DATE_TIME_FORMAT);
	var dateInstance2 = moment(date2, SCRIPT_DATE_TIME_FORMAT);
	return dateInstance2.isAfter(dateInstance1) || dateInstance2.isSame(dateInstance1);

}

function isDateTimeEqual(date1, date2) {
	var dateInstance1 = moment(date1, SCRIPT_DATE_TIME_FORMAT);
	var dateInstance2 = moment(date2, SCRIPT_DATE_TIME_FORMAT);
	return dateInstance1.isSame(dateInstance2);
}
// Validation with Date and Time

function getTBADateFormat(str) {
	return str;

}

function convertSimpleFormat(format) {
	if (format == "dd/MM/yyyy")
		return "dd/MM/yyyy";
	else if (format == "dd-MM-yyyy")
		return "dd/MM/yyyy";
	else if (format == "dd-MM-yyyy HH24:MI:SS")
		return "dd-MM-yyyy HH:MM:SS";
	else if (format == "dd/MM/yyyy HH24:MI:SS")
		return "dd/MM/yyyy HH:MM:SS";
	else if (format == "%d/%m/%Y")
		return "dd/MM/yyyy";
	else if (format == "%d-%m-%Y")
		return "dd-MM-yyyy";
	else if (format == "%d-%m-%Y %h:%i:%s")
		return "dd-MM-yyyy HH:MM:SS";
	else
		return format;
}

function isMatch(str1, str2) {
	return str1 == str2;
}

function isWhitespace(str) {
	var re = /[\S]/g;
	if (re.test(str))
		return false;
	return true;
}

function isValidCredential(credential) {
	if (credential == null || credential.length > 128)
		return false;
	return true;
}

function isValidCronExp(cronExp) {
	if (cronExp == null || cronExp.length > 50)
		return false;
	return true;
}

function isValidCustomerCode(code) {
	if (code == null || code.length > 50)
		return false;
	var regex = /^([0-9])+$/;
	if (regex.test(code))
		return true;
	return false;
}

function isValidUserID(code) {
	if (code == null || code.length > 50)
		return false;
	var regex = /^([A-Z0-9_@\.\-])+$/;
	if (regex.test(code))
		return true;
	return false;
}

function isValidPort(code) {
	value = parseInt(code);
	if (value >= 1 && value <= 65536)
		return true;
	return false;
}

function isValidPrefixCode(code) {
	if ((code.length > 6))
		return false;
	var regex = /[^A-Z0-9]/g;
	if (regex.test(code))
		return false;
	return true;
}

function isValidCode(code) {
	if (code == null || code.length > 12)
		return false;
	var regex = /^([A-Z0-9_])+$/;
	if (regex.test(code))
		return true;
	return false;
}

function isValidCode(code, length) {
	if (code == null || code.length > length)
		return false;
	var regex = /^([A-Z0-9_])+$/;
	if (regex.test(code))
		return true;
	return false;
}

function isValidBeneficiary(code) {
	if (code == null || code.length > 35)
		return false;
	var regex = /^([A-Z0-9_@\.\-])+$/;
	if (regex.test(code))
		return true;
	return false;
}

function isValidService(code) {
	if (code == null || code.length > 35)
		return false;
	var regex = /^([A-Z0-9_@\.\-])+$/;
	if (regex.test(code))
		return true;
	return false;
}

// Rizwan Changes for Custom tags and look up validations

function isValidName(name) {
	if (name == null || name.length == 0 || name.length > 100)
		return false;
	return true;
}

function isValidShortName(name) {
	if (name == null || name.length == 0 || name.length > 20)
		return false;
	return true;
}

function isValidDescription(description) {
	if (description == null || description.length == 0 || description.length > 100)

		return false;
	return true;
}
// Rizwan Changes for Custom tags and look up validations
function isValidDescription(description, length) {
	if (description == null || description.length == 0 || description.length > length)

		return false;
	return true;
}

function isValidChequeNumber(code) {
	if (code == null || code.length > 15)
		return false;
	var regex = /^([A-Z0-9_])+$/;
	if (regex.test(code))
		return true;
	return false;
}

function isValidReceiptNumber(code) {
	if (code == null || code.length > 15)
		return false;
	var regex = /^([A-Z0-9_])+$/;
	if (regex.test(code))
		return true;
	return false;
}

function isValidDesignation(designation) {
	if (designation.length > 100)
		return false;
	return true;
}

function isValidAddress(address) {
	if (address == null || address.length > 250)
		return false;
	return true;
}

function isValidAccountNumber(acntNum) {
	if (acntNum != null && acntNum.length > 35)
		return false;
	var regex = /^([a-zA-Z0-9])+$/;
	if (!regex.test(acntNum))
		return false;
	return true;
}

function isValidRemarks(remarks) {
	if (remarks == null || remarks.length > 250)
		return false;
	return true;
}

function isValidMoreRemarks(remarks) {
	if (remarks == null || remarks.length > 350)
		return false;
	return true;
}

function isValidUrl(value) {
	if (value == null || value.length > 128)
		return false;
	return true;
}

function isValidServerAddress(value) {
	if (value == null || value.length > 128)
		return false;
	return true;
}

function isValidKey(value) {
	if (value == null || value.length > 128)
		return false;
	return true;
}

function isValidValue(value) {
	if (value == null || value.length > 128)
		return false;
	return true;
}

function isValidPercentage(number) {
	var regex = /^\d{1,3}(?:\.\d{0,2})?$/;
	if (regex.test(number)) {
		if (parseFloat(number) < 0 || parseFloat(number) > 100) {
			return false;
		}
	} else
		return false;
	return true;
}

function isValidPath(value) {
	if (value == null || value.length > 250)
		return false;
	return true;
}

function isValidQuestion(question) {
	if (question == null || question.length == 0 || question.length > 250)
		return false;
	return true;
}

function isValidAnswer(answer) {
	if (answer == null || answer.length == 0 || answer.length > 4000)
		return false;
	return true;
}

function isValidRefreshTime(value) {
	if (value > 43200 || value < 1) {
		return false;
	}
	return true;
}

function isValidAmount(number) {
	var regex = /^\d+(\.\d{1,2})?$/;
	return regex.test(number);
}
function isPositiveAmount(amount) {
	var regex = /^\d{0,27}(?:\.\d{0,3})?$/;
	return regex.test(amount);
}

function isNegativeAmount(amount) {
	var regex = /^[\-]\d{0,27}(?:\.\d{0,3})?$/;
	return regex.test(amount);
}

function isAmount(amount) {
	var regex = /^\d{0,27}(?:\.\d{0,2})?$/;
	return regex.test(amount);
}
function isValidSmallAmount(currency, amount) {
	var regex;

	var w_units = parseInt(getCurrencyUnit(currency));
	if (w_units == 3)
		regex = /^[-]?\d{0,15}(?:\.\d{0,3})?$/;
	else if (w_units == 2)
		regex = /^[-]?\d{0,15}(?:\.\d{0,2})?$/;
	else if (w_units == 1)
		regex = /^[-]?\d{0,15}(?:\.\d{0,1})?$/;
	else
		regex = /^[-]?\d{0,15}(?:\.\d{0,0})?$/;
	return regex.test(amount);
}
function isValidBigAmount(currency, amount) {
	var regex;
	var w_units = parseInt(getCurrencyUnit(currency));
	if (w_units == 3)
		regex = /^[-]?\d{0,27}(?:\.\d{0,3})?$/;
	else if (w_units == 2)
		regex = /^[-]?\d{0,27}(?:\.\d{0,2})?$/;
	else if (w_units == 1)
		regex = /^[-]?\d{0,27}(?:\.\d{0,1})?$/;
	else
		regex = /^[-]?\d{0,27}(?:\.\d{0,0})?$/;
	return regex.test(amount);
}
function BigDecimal(strVal) {
	var parts = strVal.split('\.');
	this.integralString = parts[0];
	this.integralString = this.integralString.replace(/^0+/g, EMPTY_STRING);
	this.decimalString = EMPTY_STRING;
	if (strVal.indexOf('.') >= 0)
		this.decimalString = parts[1];
	this.decimalString = this.decimalString.replace(/0+$/g, EMPTY_STRING);
	if (isEmpty(this.decimalString)) {
		this.decimalString = '0';
	}
	this.integralParts = new Array();
	this.decimalParts = new Array();

	for (var i = 0; i < this.decimalString.length; i++) {
		this.decimalParts[i] = this.decimalString.charAt(i);
	}

	for (var i = 0; i < this.integralString.length; i++) {
		this.integralParts[i] = this.integralString.charAt(i);
	}

	this.compare = compareX;

	function compareX(num) {
		var thisIntegralLength = this.integralParts.length;
		var thisDecimalLength = this.decimalParts.length;
		var numIntegralLength = num.integralParts.length;
		var numDecimalLength = num.decimalParts.length;
		var greater = 0;
		if (thisIntegralLength == numIntegralLength && thisIntegralLength > 0) {
			var cnt = 0;
			while (greater == 0 && cnt < thisIntegralLength) {
				greater = this.integralParts[cnt] - num.integralParts[cnt];
				cnt++;
			}
			if (greater == 0) {
				var thisDecimalValue = parseFloat('0.' + this.decimalString);
				var numDecimalValue = parseFloat('0.' + num.decimalString);
				if (thisDecimalValue > numDecimalValue)
					return 1;
				if (thisDecimalValue < numDecimalValue)
					return -1;
				if (thisDecimalValue == numDecimalValue)
					return 0;
			}
		} else {
			if (thisIntegralLength == numIntegralLength)
				return 0;
			else
				greater = (thisIntegralLength > numIntegralLength) ? (1) : (-1);
		}
		return greater;
	}
}

function isValidRefNumber(refNumber) {
	if (refNumber == null || refNumber.length == 0 || refNumber.length > 25)
		return false;
	return true;
}

function checkPasswordCriteria(password, minLength, minAlpha, minNumeric, minSpecial) {

	var ALPHA_STRING = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
	var NUMERIC_STRING = '0123456789';
	var SPECIAL_CHARACTERS_STRING = '<=:`[?;,\\^_}@>.)"(\'$#*%~-/&!]{|+';
	var alphaCount = 0;
	var numericCount = 0;
	var specialCount = 0;
	var w_char = null;
	if (password == null || password == EMPTY_STRING)
		return false;
	if (parseInt(password.length) < parseInt(minLength)) {
		return false;
	}
	for (var i = 0; i < password.length; ++i) {
		w_char = password.charAt(i);
		if (ALPHA_STRING.indexOf(w_char) >= 0) {
			alphaCount++;
		} else if (NUMERIC_STRING.indexOf(w_char) >= 0) {
			numericCount++;
		} else if (SPECIAL_CHARACTERS_STRING.indexOf(w_char) >= 0) {
			specialCount++;
		}
	}
	if (alphaCount < parseInt(minAlpha) || numericCount < parseInt(minNumeric) || specialCount < parseInt(minSpecial)) {

		return false;
	}
	return true;
}
/** Validation Functions END* */

function checkSessionAvailability(xmlDoc) {
	try {
		// chnaged by swaroopa as on 14-06-2015 starts
		var value = xmlDoc.getResponseHeader('Patterns-Session-Valid');
		if (value == COLUMN_DISABLE) {
			alert(SESSION_EXPIRED);
			redirectLink(getBasePath() + EUNAUTHACCESS_JSP);
			return false;

		}
		// chnaged by swaroopa as on 14-06-2015 ends

		/*
		 * if (xmlData.indexOf('<rows ') != -1) { buildGridObject(xmlData,
		 * function(result) { return checkSessionAvailabilityXMLObject(result);
		 * }); }
		 */

	} catch (e) {
	}
	return true;
}

function checkSessionAvailabilityXMLObject(result) {
	try {
		var sessionData = dhx4.ajax.xpath('//rows', result);
		for (var i = 0; i < sessionData.length; ++i) {
			try {
				var sessionExpired = sessionData[i].getAttribute('session-expired');

				if (sessionExpired == COLUMN_ENABLE) {
					alert(SESSION_EXPIRED);
					redirectLink(getBasePath() + EUNAUTHACCESS_JSP);
					return false;
				}
			} catch (e) {
			}
		}
	} catch (e) {
	}
	return true;
}

function getNavigateXml(first, second, third, fourth, Resultobj) {

}

/** Smart Navigation BEGIN* */
function AutoComplete(elementID) {
	var objID = elementID;
	var obj = document.getElementById(objID);
	var navigateLength = 0;
	var cursorPosition = -1;
	var popupInner;
	var _popupInner;
	var programLinks = new Array();
	this.hide = function() {
		var popup = $('#' + objID + '_popup');
		var iframe = $('#' + objID + '_iframe');
		popup.html('');
		popup.hide();
		iframe.hide();
		0
		obj.value = EMPTY_STRING;
	};
	obj.onkeyup = function(e) {
		var popup = document.getElementById(objID + '_popup');
		var iframe = document.getElementById(objID + '_iframe');
		if (!popup) {
			popup = document.createElement('DIV');
			iframe = document.createElement('IFRAME');
			document.body.appendChild(popup);
			document.body.appendChild(iframe);
			iframe.style.display = 'none';
			popup.style.display = 'none';
			popup.id = objID + '_popup';
			popup.style.border = 'solid 1px #999999';
			popup.style.backgroundColor = '#FFFFFF';
			iframe.id = objID + '_iframe';
			iframe.style.backgroundColor = '#FFFFFF';

		}

		var dimensions = $(obj).offset();
		popup.style.position = 'absolute';
		popup.style.left = dimensions.left + "px";
		popup.style.top = (dimensions.top + parseFloat($(obj).css('height').replace('px', ''))) + "px";

		popup.style.height = '260px';
		popup.style.width = '240px';
		popup.style.zIndex = 151;
		popup.style.overflow = 'auto';

		iframe.style.position = 'absolute';
		iframe.style.left = dimensions.left + "px";
		iframe.style.top = (dimensions.top + parseFloat($(obj).css('height').replace('px', ''))) + "px";

		iframe.style.height = '260px';
		iframe.style.width = '240px';
		iframe.style.zIndex = 150;
		iframe.style.border = 'none';

		var key = (e || event).keyCode;
		var userValue = this.value;
		if (key == Keys.ESC_KEY) {
			popup.innerHTML = EMPTY_STRING;
			popup.style.display = 'none';
			iframe.style.display = 'none';
			obj.value = EMPTY_STRING;
		} else if (key == Keys.DOWN_ARROW_KEY) {
			if (navigateLength > 0) {
				if (cursorPosition >= 0 || cursorPosition >= navigateLength - 1) {
					popupInner = $('#' + objID + '_popupInner_div_' + cursorPosition);

					_popupInner = $('#' + objID + '_popupInner_div_' + cursorPosition);

					if (_popupInner.hasClass('navigation-select'))
						popupInner.removeClass('navigation-select');
				}
				if (cursorPosition >= navigateLength - 1)
					cursorPosition = -1;
				cursorPosition++;
				popupInner = $('#' + objID + '_popupInner_div_' + cursorPosition);

				var navigateLink = $('#' + objID + '_navigateLink_' + cursorPosition);

				popupInner.addClass('navigation-select');
				navigateLink.focus();
				obj.focus();
				obj.value = navigateLink.html();
			}
		} else if (key == Keys.UP_ARROW_KEY) {
			if (navigateLength > 0) {
				if (cursorPosition <= 0) {
					cursorPosition = navigateLength;
					popupInner = $('#' + objID + '_popupInner_div_' + 0);
					_popupInner = $('#' + objID + '_popupInner_div_' + 0);
					if (_popupInner.hasClass('navigation-select'))
						popupInner.removeClass('navigation-select');
				} else {
					popupInner = $('#' + objID + '_popupInner_div_' + cursorPosition);

					_popupInner = $('#' + objID + '_popupInner_div_' + cursorPosition);

					if (_popupInner.hasClass('navigation-select'))
						popupInner.removeClass('navigation-select');
				}
				cursorPosition--;
				popupInner = $('#' + objID + '_popupInner_div_' + cursorPosition);

				var navigateLink = $('#' + objID + '_navigateLink_' + cursorPosition);

				popupInner.addClass('navigation-select');
				navigateLink.focus();
				obj.focus();
				obj.value = navigateLink.html();
			}
		} else if (key == Keys.ENTER_KEY) {
			if (obj.value != EMPTY_STRING) {
				if (navigateLength > 0) {
					var navigateLink = EMPTY_STRING;
					if (cursorPosition == -1)
						navigateLink = $('#' + objID + '_navigateLink_' + 0);
					else
						navigateLink = $('#' + objID + '_navigateLink_' + cursorPosition);

					var path = navigateLink.attr('href');
					if (path != EMPTY_STRING) {
						redirectLink(path);
					}
				}
			}
		} else {
			popup.innerHTML = EMPTY_STRING;
			if (userValue != EMPTY_STRING && userValue.length > 3) {
				popup.style.display = 'block';
				iframe.style.display = 'block';
				popup.className = 'frm_loading';
				programLinks = new Array();
				var loader = dhx4.ajax.postSync(getBasePath() + 'Renderer/common/esmartnav.jsp', encodeURI('userValue=' + userValue + '&objID=' + objID));

				var xmlData = loader.xmlDoc.responseText;
				// changed by swaroopa as on 16/6/2016
				if (checkSessionAvailability(loader.xmlDoc)) {
					buildGridObject(xmlData, function(result) {

						var resultData = dhx4.ajax.xpath('//rows/row/cell/a', result);

						for (var i = 0; i < resultData.length; ++i) {

							try {

								var _pgmPath = resultData[i].getAttribute('href');

								var _pgmID = resultData[i].getAttribute('id');

								var _pgmDesc = resultData[i].firstChild.nodeValue;
								programLinks[i] = '<a href="' + _pgmPath + '" id="' + _pgmID + '" class="navigation-link">' + _pgmDesc + '</a>';

							} catch (e) {

							}
						}
					});

					popup.className = EMPTY_STRING;
					navigateLength = 0;
					cursorPosition = -1;
					if (programLinks.length > 0) {
						for (var i = 0; i < programLinks.length; i++) {
							navigateLength++;
							var popupInner_div = document.createElement('div');
							popup.appendChild(popupInner_div);
							popupInner_div.id = objID + '_popupInner_div_' + i;
							popupInner_div.innerHTML = programLinks[i];
							if (i % 2 == 0)
								popupInner_div.className = 'navigate-div-even';
							else
								popupInner_div.className = 'navigate-div-odd';
							document.getElementById(objID + '_navigateLink_' + i).className = 'navigation-link';

							popupInner_div.onmousemove = function(e) {
								for (var j = 0; j < navigateLength; ++j) {
									popupInner = document.getElementById(objID + '_popupInner_div_' + j);

									popupInner.className = popupInner.className.replace('navigation-select', EMPTY_STRING);

								}
								this.className = this.className + ' navigation-select';

								cursorPosition = 0;
							};
							popupInner_div.onmouseout = function(e) {
								this.className = this.className.replace('navigation-select', EMPTY_STRING);

							};
							popupInner_div.onclick = function(e) {
							};
							popup.appendChild(popupInner_div);
						}
					} else {
						var popupInner_div = document.createElement('div');
						popup.appendChild(popupInner_div);
						popupInner_div.innerHTML = '<span>' + SEARCH_NO_RESULT + '</span>';

						popupInner_div.className = 'navigate-div-even';

					}
				}
			} else {
				popup.style.display = 'none';
				iframe.style.display = 'none';
			}
		}
	};
}
/** Smart Navigation END* */

/** Console Navigation BEGIN* */
function doConsoleChange(ele) {
	if (ele.val()) {
		$('#program_div').html(LOADING);
		$('#program_div').addClass('frm_loading');
		var loader = dhx4.ajax.postSync(getBasePath() + 'Renderer/common/econsolenav.jsp', encodeURI(AJAX_TOKEN + '=' + '1&consoleCode=' + ele.val()));

		$('#program_div').removeClass('frm_loading');
		var xmlData = loader.xmlDoc.responseText;
		// changed by swaroopa as on 16/6/2016
		if (checkSessionAvailability(loader.xmlDoc)) {
			$('#program_div').html(xmlData);
		}
	}
}
/** Console Navigation END* */
function isValidExtensionList(value) {
	var ext = value.trim().split(',');
	for (var i = 0; i < ext.length; i++) {
		if (!isValidExtension(ext[i])) {
			return false;
		}
	}
	return true;
}

function isValidExtension(value) {
	if (value == null || value.length > 100)
		return false;
	var regex = /^([A-Z0-9a-z])+$/;
	if (regex.test(value))
		return true;
	return false;
}

function disableElement(id) {
	$('#' + id).disabled = true;
}

function enableElement(id) {
	$('#' + id).disabled = false;
}

function hideElement(id) {
	if (!$('#' + id).hasClass('hidden')) {
		$('#' + id).addClass('hidden');
	}
}
function showElement(id) {
	if ($('#' + id).hasClass('hidden')) {
		$('#' + id).removeClass('hidden');
	}
}

String.prototype.trim = function() {
	return this.replace(/^\s+|\s+$/g, EMPTY_STRING);
};
String.prototype.ltrim = function() {
	return this.replace(/^\s+/, EMPTY_STRING);
};
String.prototype.rtrim = function() {
	return this.replace(/\s+$/, EMPTY_STRING);
};

function redirectLink(url) {
	if (/MSIE (\d+\.\d+);/.test(navigator.userAgent)) {
		var referLink = document.createElement('a');
		referLink.href = url;
		document.body.appendChild(referLink);
		referLink.click();
	} else {
		location.href = url;
	}
};

var Message = {
	INFO : 'info',
	ERROR : 'error',
	PROGRESS : 'progress',
	SUCCESS : 'success',
	WARN : 'warn'
};

function showMessage(id, messageType, value) {
	id = id.toLowerCase();
	hideAllMessages(id);
	if ($('#' + id + '_' + messageType).hasClass('hidden')) {
		$('#' + id + '_' + messageType).removeClass('hidden');
	}
	if (messageType == Message.PROGRESS) {
		value = PROGRESS_INFO;
	}
	$('#' + id + '_' + messageType).html(value);
}

function hideMessage(id) {
	id = id.toLowerCase();
	hideAllMessages(id);
}

function hideAllMessages(id) {
	id = id.toLowerCase();
	$('#' + id + '_' + Message.INFO).addClass('hidden');
	$('#' + id + '_' + Message.ERROR).addClass('hidden');
	$('#' + id + '_' + Message.SUCCESS).addClass('hidden');
	$('#' + id + '_' + Message.PROGRESS).addClass('hidden');
	$('#' + id + '_' + Message.WARN).addClass('hidden');
}

function showReportLink(_validator) {
	showMessage(getProgramID(), Message.SUCCESS, [ REPORT_SUCCESS_MESSAGE ]);
	$('#reportLink').attr('href', getBasePath() + 'servlet/ReportDownloadProcessor?' + REPORT_ID + '=' + _validator.getValue(REPORT_ID));
	$('#reportLink')[0].click();
}

function booleanToString(value) {
	if (value)
		return '1';
	else
		return '0';
}
// Jainudeen changes begins
function isValidConciseDescription(conciseDescription) {
	if (conciseDescription == null || conciseDescription.length == LENGTH_0)
		return false;
	return true;
}

function isValidExternalDescription(externalDescription) {
	if (externalDescription == null || externalDescription.length == LENGTH_0)
		return false;
	return true;
}

function isValidPersonName(title1, title2, personName) {
	if (title2 == null || title2.length == LENGTH_0)
		return false;
	if (!isValidName(personName))
		return false;
	return true;
}

function isCurrencySmallAmount(currency, smallAmount) {
	if (currency == null || currency.length == LENGTH_0)
		return false;
	if (!isValidSmallAmount(currency, smallAmount))
		return false;
	return true;
}

function isCurrencyBigAmount(currency, bigAmount) {
	if (currency == null || currency.length == LENGTH_0)
		return false;
	if (!isValidBigAmount(currency, bigAmount))
		return false;
	return true;
}
// Jainudeen changes ends
// Rizwan Changes Begins
function isValidOtherInformation25(otherInfomation) {
	if (otherInfomation == null || otherInfomation.length == LENGTH_0 || otherInfomation.length > LENGTH_25)

		return false;
	return true;
}
function isValidOtherInformation50(otherInfomation) {
	if (otherInfomation == null || otherInfomation.length == LENGTH_0 || otherInfomation.length > LENGTH_50)

		return false;
	return true;
}

function isValidAge(years, months, days) {

	var yearsAvailable = true, monthsAvailable = true, daysAvailable = true;
	if (years == null || years.length == LENGTH_0 || years.length > LENGTH_3 || !isPositive(years))

		yearsAvailable = false;
	if (months == null || months.length == LENGTH_0 || months.length > LENGTH_2 || !isPositive(months))

		monthsAvailable = false;
	if (days == null || days.length == LENGTH_0 || days.length > LENGTH_2 || !isPositive(days))

		daysAvailable = false;
	if (!(years == null || years.length == LENGTH_0)) {
		if (parseInt(years) >= 120 || !isPositive(years))
			return false;
	}
	if (!(months == null || months.length == LENGTH_0)) {
		if (parseInt(months) >= 12 || !isPositive(months))
			return false;
	}
	if (!(days == null || days.length == LENGTH_0)) {
		if (parseInt(days) >= 30 || !isPositive(days))
			return false;
	}

	if (isZero(years) && isZero(months) && isZero(days))
		return false;

	if (yearsAvailable == true || monthsAvailable == true || daysAvailable == true)

		return true;

	return true;
}
function isValidConnectedPerson(relationship, title1, title2, personName) {
	if (isEmpty(relationship))
		return false;
	if (title2 == null || title2.length == LENGTH_0)
		return false;
	if (!isValidName(personName))
		return false;
	return true;
}
function isValidCurrencyNotation(currencyNotation) {
	if (currencyNotation == null || currencyNotation.length == LENGTH_0 || currencyNotation.length > LENGTH_6)

		return false;
	return true;
}
function isValidISOCurrency(currency) {
	if (currency == null || currency.length == LENGTH_0 || currency.length > LENGTH_3)

		return false;
	return true;
}

function isValidOneDigit(number) {
	if (number == null || number.length == LENGTH_0 || number.length > LENGTH_1)
		return false;
	if (!isNumeric(number))
		return false;
	return true;
}
function isValidTwoDigit(number) {
	if (number == null || number.length == LENGTH_0 || number.length > LENGTH_2)
		return false;
	if (!isNumeric(number))
		return false;
	return true;
}
function isValidThreeDigit(number) {
	if (number == null || number.length == LENGTH_0 || number.length > LENGTH_3)
		return false;
	if (!isNumeric(number))
		return false;
	return true;
}
function isValidFiveDigit(number) {
	if (number == null || number.length == LENGTH_0 || number.length > LENGTH_5)
		return false;
	if (!isNumeric(number))
		return false;
	return true;
}
function isValidfiveDigitThreeDecimal(value) {
    var regex = /^\d{0,5}(\.\d{0,3})?$/;
    if (regex.test(value)) {
	return true;
    }
    return false;
}
function isValidConnectedPerson(relationship, title2, personName) {
	if (relationship == null || relationship.length == LENGTH_0)
		return false;
	if (title2 == null || title2.length == LENGTH_0)
		return false;
	if (!isValidName(personName))
		return false;
	return true;
}
// Rizwan Changes Ends

if (!Array.prototype.every) {
	Array.prototype.every = function(callbackfn, thisArg) {
		'use strict';
		var T, k;

		if (this == null) {
			throw new TypeError('this is null or not defined');
		}

		// 1. Let O be the result of calling ToObject passing the this
		// value as the argument.
		var O = Object(this);

		// 2. Let lenValue be the result of calling the Get internal method
		// of O with the argument "length".
		// 3. Let len be ToUint32(lenValue).
		var len = O.length >>> 0;

		// 4. If IsCallable(callbackfn) is false, throw a TypeError exception.
		if (typeof callbackfn !== 'function') {
			throw new TypeError();
		}

		// 5. If thisArg was supplied, let T be thisArg; else let T be
		// undefined.
		if (arguments.length > 1) {
			T = thisArg;
		}

		// 6. Let k be 0.
		k = 0;

		// 7. Repeat, while k < len
		while (k < len) {

			var kValue;

			// a. Let Pk be ToString(k).
			// This is implicit for LHS operands of the in operator
			// b. Let kPresent be the result of calling the HasProperty internal
			// method of O with argument Pk.
			// This step can be combined with c
			// c. If kPresent is true, then
			if (k in O) {

				// i. Let kValue be the result of calling the Get internal
				// method
				// of O with argument Pk.
				kValue = O[k];

				// ii. Let testResult be the result of calling the Call internal
				// method
				// of callbackfn with T as the this value and argument list
				// containing kValue, k, and O.
				var testResult = callbackfn.call(T, kValue, k, O);

				// iii. If ToBoolean(testResult) is false, return false.
				if (!testResult) {
					return false;
				}
			}
			k++;
		}
		return true;
	};
}

function getCurrencyUnit(currency) {
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('CCY_CODE', currency);
	validator.setValue(ACTION, USAGE);
	validator.setValue("fetchColumns", "SUB_CCY_UNITS_IN_CCY");
	validator.setClass('patterns.config.web.forms.cmn.mcurrencybean');
	validator.setMethod('validateCurrencyCode');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING)
		return 0;
	else if (validator.getValue(RESULT) == DATA_AVAILABLE)
		return validator.getValue("SUB_CCY_UNITS_IN_CCY");
	else
		return 0;
}
function formatAmount(value, currency, subCcyUnit) {

	var k;
	var indexCounter = 0;
	j = 0;
	var unitValue;
	if (subCcyUnit)
		unitValue = subCcyUnit;
	else
		unitValue = getCurrencyUnit(currency);
	value = unformatAmount(value);
	newnumString = '';
	dotPosition = value.indexOf('.');
	if (dotPosition >= 0) {
		intPart = value.substring(0, dotPosition);
		decPart = value.substring(dotPosition + 1, value.length);
		k = eval(unitValue - decPart.length);
		if (k < unitValue || decPart == '') {
			for (var i = 1; i <= k; i++) {
				decPart = decPart + '0';
			}
			decPart = '.' + decPart;
		} else {
			decPart = '.' + decPart;
		}
		StringLen = intPart.length;

	} else {
		if (!isNumeric(value)) {
			value = parseInt(value);
		}
		value = value.toString();
		intPart = value.substring(0, value.length);

		decPart = '';
		if (unitValue > 0) {
			for (var i = 1; i <= unitValue; i++) {
				decPart = decPart + '0';
			}
			decPart = '.' + decPart;
		}
		StringLen = intPart.length;
	}
	var _flg = false;
	for (var x = StringLen - 1; x >= 0; x--) {
		indexCounter++;
		if (_flg == false) {
			if (indexCounter == 4) {
				if (value.charAt(x) != '-') {
					numreplace = value.charAt(x) + ',';
					newnumString = numreplace + newnumString;
					indexCounter = 1;
					_flg = true;
				} else {
					newnumString = value.charAt(x) + newnumString;
				}
			} else {
				newnumString = value.charAt(x) + newnumString;
			}
		} else {
			if (indexCounter == 3) {
				if (value.charAt(x) != '-') {
					numreplace = value.charAt(x) + ',';
					newnumString = numreplace + newnumString;
					indexCounter = 1;
				} else {
					newnumString = value.charAt(x) + newnumString;
				}
			} else {
				newnumString = value.charAt(x) + newnumString;
			}
		}
	}
	if (dotPosition == 0) {
		newnumString = '0' + newnumString;
	}
	if (newnumString.charAt(0) == '') {
	}
	if (newnumString.charAt(0) == ',' || (newnumString.charAt(0) == '0' && dotPosition > 1)) {

		newnumString = newnumString.substring(1, newnumString.length) + decPart;
		value = newnumString;
		return trimNumber(newnumString, unitValue);
	} else {
		newnumString = newnumString + decPart;
		value = newnumString;
		return trimNumber(newnumString, unitValue);
	}
}

function unformatAmount(num) {
	newUnFormatStr = '';
	for (var i = 0; i < num.length; i++) {
		if (num.charAt(i) != ',') {
			newUnFormatStr = newUnFormatStr + num.charAt(i);
		}
	}
	var numLen;
	num = newUnFormatStr;
	numLen = num.length;
	for (var i = 0; i < numLen; i++) {
		if (num.charAt(0) == '0' && num.indexOf('.') > 1) {
			num = num.substring(1, num.length);
		}
	}
	return num;
}

function trimNumber(inputNum, unitValue) {
	var totLen;
	unitValue = parseInt(unitValue);
	dotPosition = inputNum.indexOf('.');
	totLen = eval(dotPosition + unitValue + 1);
	if (inputNum.indexOf('.') >= 0) {
		intPart = inputNum.substring(0, dotPosition);
		decPart = inputNum.substring(dotPosition + 1, inputNum.length);
		decPart = '.' + decPart;
		StringLen = intPart.length;
	} else {
		intPart = inputNum.substring(0, inputNum.length);
		decPart = '';
		StringLen = intPart.length;
	}
	if (decPart.length > unitValue) {
		decPart = inputNum.substring(dotPosition + 1, totLen);
		decPart = '.' + decPart;
	}
	inputNum = intPart + decPart;
	return inputNum;
}

function padZeroPrefix(str, max) {
	return str.length < parseInt(max) ? padZeroPrefix("0" + str, max) : str;
}

function getContentWindow(win) {
	return win.getFrame().contentWindow;
}

function lpadPrefix(value, prefix, max) {
	return value.length < max ? lpadPrefix(prefix + value, prefix, max) : value;
}
function getFinYear(w_date) {
	var month = w_date.substring(3, 5);
	var finStartOfMonth = $('#_SFM').val();
	if (parseInt(month) < parseInt(finStartOfMonth))
		return parseInt(w_date.substring(6, w_date.length) - 1);
	else
		return w_date.substring(6, w_date.length);
}
function getCalendarYear(w_date) {
	var year = w_date.substring(6, w_date.length);
	if (parseInt(year) == year)
		return year;
	else
		return 0;
}

function formatRegNo(element) {
	if ($('#' + element.id).val() != EMPTY_STRING)
		$('#' + element.id).val(padZeroPrefix($('#' + element.id).val(), LENGTH_8));

}
function formatEmpCode(element) {
	var dataType = $('#_ECT').val();
	var maxLen = $('#_ECS').val();
	if ($('#' + element.id).val() != EMPTY_STRING && dataType == "1") {
		$('#' + element.id).val(padZeroPrefix($$(element.id).val(), maxLen));
	}
}
function isValidYear(str) {
	// str=2015
	var regex = /^(19|20)\d{2}$/;
	return regex.test(str);
}

function prefixYear(value) {
	var temp = '';
	switch (value.length) {
	case 4:
		temp = value;
		break;
	case 3:
		temp = lpadPrefix(value, '2', LENGTH_4);
		break;
	case 2:
		temp = lpadPrefix(value, '20', LENGTH_4);
		break;
	case 1:
		temp = lpadPrefix(value, '0', LENGTH_3);
		temp = lpadPrefix(temp, '2', LENGTH_4);
		break;
	}
	return temp;
}

/** UI CHANGES SARAT BEGIN* */
function navToggle() {
	if ($('.role-popup').hasClass('hide-content')) {
		$('.side-navbar').toggle("slide");
		// $('.overlay').fadeToggle();
		$('body').toggleClass('over-flow');
	} else {
		rolePopup();
	}
}

function overlayPage() {
	if ($('.role-popup').hasClass('hide-content')) {
		$('.side-navbar').toggle("slide");
		// $('.overlay').fadeToggle();
		$('body').toggleClass('over-flow');
	}
}

$('#profile').click(function() {
	$('.list-unstyled').slideToggle(150);
});

/*
 * function slideMenu() { $('.offcanvas-menu').toggle();
 * $('.form-container').toggleClass('adjust-menu'); }
 */

// changed by mspr as 12-06-2015 starts
function slideMenu() {
	if ($('.offcanvas-menu').length > 0) {
		var offcanvasVisible = $('.offcanvas-menu').is(':visible');
		if (offcanvasVisible) {
			$('.form-container').removeClass('adjust-menu');
			$('.offcanvas-menu').hide();
		} else {
			if (!$('.form-container').hasClass('adjust-menu')) {
				$('.form-container').addClass('adjust-menu');
			}
			$('.offcanvas-menu').show();
		}
		offcanvasVisible = $('.offcanvas-menu').is(':visible');

		$.cookie('SLIDE_MODE', offcanvasVisible ? COLUMN_ENABLE : COLUMN_DISABLE, {
			path : '/'
		});

	}
}

function doDisplayMenu() {
	var value = $.cookie('SLIDE_MODE');
	if (typeof value === 'undefined') {
		$.cookie('SLIDE_MODE', COLUMN_ENABLE, {
			path : '/'
		});
	}
	if (value == COLUMN_ENABLE || getProgramID() == "ELANDING") {
		return true;
	}
	$('.form-container').removeClass('adjust-menu');
	return false;
}
// added by mspr as on 12-06-2015 ends
function rolePopup() {
	$('.overlay').fadeToggle();
	$('body').toggleClass('over-flow');
	$('.role-popup').toggleClass('hide-content');
}

function loginInfoPopup() {
	$('.overlay').fadeToggle();
	$('body').toggleClass('over-flow');
	$('.logindetails-popup').toggleClass('hide-content');
}

/** UI CHANGES SARAT END* */

function DhtmlxXmlBuilder() {
	var t = this;
	t.xmlDoc = $.parseXML('<?xml version="1.0"?><rows />');
	// t.addRow = function(rowData) {
	// var row = t.xmlDoc.createElement('row');
	t.addRow = function(rowData, id) {
		var row = t.xmlDoc.createElement('row');
		if (id)
			row.setAttribute('id', id);
		for (var i = 0; i < rowData.length; ++i) {
			var cell = t.xmlDoc.createElement('cell');
			var text = t.xmlDoc.createTextNode(rowData[i]);
			cell.appendChild(text);
			row.appendChild(cell);
		}
		t.xmlDoc.documentElement.appendChild(row);
	};
	t.serialize = function() {
		if (document.all) {
			return t.xmlDoc.xml;
		} else {
			var objXMLSerializer = new XMLSerializer();
			var strXML = objXMLSerializer.serializeToString(t.xmlDoc);
			return strXML;
		}
	};
}

function openPopup(_url, _primaryKey, _id, width, height) {
	var id = 'window_auth';
	if (_id) {
		id = _id;
	}

	var params = PK + '=' + _primaryKey;
	var url = getBasePath() + 'Renderer/' + _url + '.jsp';
	var title = '';
	var modal = true;
	var win = openWindow(id, url, title, modal, false, false, width, height, params);

	// var bar = win.attachToolbar();
}
function openWindow(id, url, title, modal, move, maximize, width, height, params) {

	initDHTMLXWindows();
	var _width = WidgetConstants.MODAL_WIDTH;
	var _height = WidgetConstants.MODAL_HEIGHT;
	if (width) {
		_width = width;
	}
	if (height) {
		_height = height;
	}
	var win = getDHTMLXWindow(id, _width, _height, modal, move);
	win.progressOn();
	win.setText(title);
	win.attachURL(url + '?' + params);
	if (maximize) {
		win.maximize();
	}
	return win;
}

function tabbarClicked(tabId) {
	doTabbarClick(tabId);
}

function doTabbarClick(tabId) {

}

function dosmartchange(id, value, text) {
	smartchange(id, value, text);
}
function smartchange(id, value, text) {

}
function response() {

}
function isValidTelePhoneNumber(value, length) {
	if (isLengthBetween(value, 0, length)) {
		if (isNumeric(value)) {
			if (isPositive(value)) {
				return true;
			}
			return false;
		}
		return false;
	}
	return false;
}

function isValidTelePhoneNumber(value) {
	if (isLengthBetween(value, 0, LENGTH_15)) {
		if (isNumeric(value)) {
			if (isZero(value))
				return false;
			if (isPositive(value)) {
				return true;
			}
			return false;
		}
		return false;
	}
	return false;
}

function progress() {
	$('.loader').toggleClass('hide-content');
	$('.overlay').fadeToggle();
	$('body').toggleClass('over-flow');
}
function stopProgress() {
	$('.loader').fadeToggle();
	$('.overlay').fadeToggle();
	$('body').toggleClass('over-flow');
}
function isValidDynamicIntFractions(element) {
	value = element.value;
	if (!isEmpty(value)) {
		if (parseFloat(value) == value) {
			var arr = value.split('.');
			var fracLength = 0;
			var intLength = parseInt(arr[0].length);
			if (arr[1]) {
				fracLength = parseInt(arr[1].length);

			}

			var intDigits = element.getAttribute("intDigits");
			var fracDigits = element.getAttribute("fracDigits");
			if (intLength > 0) {
				if (intLength > intDigits) {
					setError(element.id + "_error", HMS_INVALID_MAXINT_RANGE + intDigits);
					return false;
				}
			}
			if (fracLength > 0) {
				if (fracLength > fracDigits) {
					setError(element.id + "_error", HMS_INVALID_MAXFRACTION_RANGE + fracDigits);
					return false;
				}
			}
			return true;
		} else {
			setError(element.id + "_error", HMS_INVALID_VALUE);
			return false;
		}

	}

	else {
		return true;
	}
}

function showReturnWindow(id, url, title, params, modal, move, maximize, returnField, width, height) {
	initDHTMLXWindows();
	var _width = WidgetConstants.MODAL_WIDTH;
	var _height = WidgetConstants.MODAL_HEIGHT;
	if (width) {
		_width = width;
	}
	if (height) {
		_height = height;
	}
	var win = getDHTMLXWindow(id, _width, _height, modal, move);
	win.progressOn();
	win.setText(title);
	params = CALLSOURCE + '=' + 'C' + '&' + params;
	win.attachURL(url + '?' + params);
	win.attachEvent("onClose", function(id) {
		var returnValue = EMPTY_STRING;
		if (win != 'undefined' && win != null) {
			returnValue = getContentWindow(win).getKeyValue();
		}
		if (oncloseReturnPopUp(returnValue, returnField, win)) {
			// id.detachObject();
			id.hide();
			$('body').attr('class', '');
			$('body').attr('style', '');
			$('#' + returnField).focus();
		}
		aftercloseReturnPopUp(returnValue, returnField, win);
		return true;
	});

	if (maximize) {
		win.maximize();
	}
	return win;
}

function aftercloseReturnPopUp() {
	return true;
}

function oncloseReturnPopUp() {
	return true;
}

function roundOff(value, roundoffChoice, roundOffFact) {
	var isNegative = 'N';
	var tmpValue = 0;
	var fracPart = 0;
	if (value == 0 || isEmpty(value)) {
		return 0;
	} else if (isEmpty(roundoffChoice)) {
		return value;
	}

	tmpValue = parseFloat(value);
	if (tmpValue < 0) {
		tmpValue = tmpValue * -1;
		isNegative = 'Y';
	}

	tmpValue = tmpValue / roundOffFact;
	fracPart = tmpValue % 1;

	if (roundoffChoice == '1') {
		if (fracPart < 0.5) {
			tmpValue = tmpValue - fracPart;
		} else {
			tmpValue = tmpValue - fracPart + 1;
		}
	} else if (roundoffChoice == '2') {
		tmpValue = tmpValue - fracPart;
	} else if (roundoffChoice == '3') {
		if (fracPart > 0) {
			tmpValue = tmpValue + 1 - fracPart;
		}
	}
	tmpValue = tmpValue * roundOffFact;

	if (isNegative == 'Y') {
		tmpValue = tmpValue * -1;
	}
	return tmpValue;
}

function isValidTwelveDigit(number) {
	if (number == null || number.length == LENGTH_0 || number.length > LENGTH_12)
		return false;
	if (!isNumeric(number))
		return false;
	return true;
}

function isValidRoundOffFactor(amount) {
	var regex = /^\d{1,5}(?:\.\d{0,3})?$/;
	return regex.test(amount);
}

function isValidPayPrintOrderRoundOff(amount) {
	var regex = /^\d{1,5}(?:\.\d{0,2})?$/;
	return regex.test(amount);
}

function isValidThreeDigitFiveDecimal(element) {
	var regex = /^\d{0,3}(\.\d{0,5})?$/;
	if (!regex.test(element.val())) {
		setError(element.id + "_error", HMS_INVALID_NUMBER);
		return false;
	}
	return true;
}

function isValidSixDigitTwoDecimal(element) {
	var regex = /^\d{0,6}(\.\d{0,2})?$/;
	if (!regex.test(element.val())) {
		setError(element.id + "_error", HMS_INVALID_NUMBER);
		return false;
	}
	return true;
}

function isvalidThreeDigitOneDecimal(element) {
	var regex = /^(\d{1,3})(([\.])([0,5]))?$/;
	if (!regex.test(element.val())) {
		setError(element.id + "_error", HMS_INVALID_NUMBER);
		return false;
	}
	return true;
}

function getNoOfDays(date1, date2) {
	var dateInstance1 = moment(date1, SCRIPT_DATE_FORMAT);
	var dateInstance2 = moment(date2, SCRIPT_DATE_FORMAT);
	return dateInstance1.diff(dateInstance2, 'days');
}

function isValidSevenDigitThreeDecimal(value) {
	var regex = /^\d{0,7}(\.\d{0,3})?$/;
	return regex.test(value);
}

function formatMessage(source, params) {
	$.each(params, function(i, n) {
		source = source.replace(new RegExp("\\{" + i + "\\}", "g"), function() {
			return n;
		});
	})
	return source;
}

function isValidBeneficiaryAccountNumber(acntNum) {
	if (acntNum == null || acntNum.length > 25)
		return false;
	var regex = /^([A-Z0-9])+$/;
	if (regex.test(acntNum))
		return true;
	return false;
}

function isValidIFSCCode(code) {
	if (code == null || code.length > 25)
		return false;
	return true;
}

function isValidWithDrawSlip(value) {
	if (value == null || value.length == LENGTH_0)
		return false;
	return true;
}

function getClusterCode() {
	return $('#_CLC').val();
}

function decodeAmount(value) {
	var intValue = EMPTY_STRING;
	var conversionValue = 0;
	var decimalvalue = EMPTY_STRING;
	var convValue = 0;
	var convertedAmount = EMPTY_STRING;
	var formattedAmount = EMPTY_STRING;
	if (isEmpty(value)) {
		return EMPTY_STRING;
	}
	value = unformatAmount(value);
	var temp = value.split('.');
	value = temp[0];
	if (temp.length > 1) {
		if (isNumeric(temp[1])) {
			decimalvalue = '.' + temp[1];
		} else {
			return null;
			// return value;
		}
	}

	var formatValue = EMPTY_STRING;
	for (i = 0; i < value.length; i++) {
		formatValue = EMPTY_STRING;
		formatValue = value.charAt(i);
		if (isNumeric(formatValue)) {
			if (i == 0 && formatValue == '-') {
				intValue = formatValue;
			}
			intValue = intValue + formatValue;
		} else if (formatValue == 'C' || formatValue == 'c' || formatValue == 'M' || formatValue == 'm' || formatValue == 'L' || formatValue == 'l' || formatValue == 'T' || formatValue == 't' || formatValue == 'K' || formatValue == 'k' || formatValue == 'H' || formatValue == 'h'
				|| formatValue == 'B' || formatValue == 'b') {
			switch (formatValue) {
			case 'C':
			case 'c':
				conversionValue = 10000000;
				break;
			case 'B':
			case 'b':
				conversionValue = 1000000000;
				break;
			case 'M':
			case 'm':
				conversionValue = 1000000;
				break;
			case 'L':
			case 'l':
				conversionValue = 100000;
				break;
			case 'T':
			case 't':
			case 'K':
			case 'k':
				conversionValue = 1000;
				break;
			case 'H':
			case 'h':
				conversionValue = 100;
				break;
			}
			if (isEmpty(intValue))
				intValue = "1";
			convValue = convValue + (parseInt(intValue) * parseInt(conversionValue));
			intValue = EMPTY_STRING;
		} else {
			return null;
			// return value;
		}

	}

	if (!isEmpty(intValue)) {
		convValue = convValue + parseInt(intValue);
	}
	if (convValue < 0) {
		var retval = convValue * -1;
		convertedAmount = '-' + retval + decimalvalue;
	} else {
		convertedAmount = convValue + decimalvalue;
	}
	formattedAmount = convertedAmount;
	return formattedAmount;
}

function decodeEvalAmount(values) {
	if (isEmpty(values))
		return values;
	value = values.toLowerCase();
	if (!isNumeric(value.charAt(0)))
		if (value.charAt(0) != '-')
			value = value.replace(value.charAt(0), '');
	var lastValue = value.charAt(value.length - 1);
	var operation = [ '+', '-', '*', '/', '%' ];
	if (jQuery.inArray(lastValue, operation) != -1)
		value = value.slice(lastValue, -1);

	var convertedAmountValue = EMPTY_STRING;
	var formattedAmountValue = EMPTY_STRING;

	if (value.includes('c') || value.includes('m') || value.includes('l') || value.includes('t') || value.includes('k') || value.includes('h') || value.includes('b')) {
		if (value.includes('*') || value.includes('/') || value.includes('+') || value.includes('-') || value.includes('%')) {
			splitValues = EMPTY_STRING;
			for (var i = 0; i < value.length; i++) {
				if (isNumeric(value.charAt(i)) || value.charAt(i) == '.')
					splitValues = splitValues + value.charAt(i);
				else {
					if (value.charAt(i) == '+') {
						splitValues = splitValues + ',' + '+' + ',';
					} else if (value.charAt(i) == '-') {
						splitValues = splitValues + ',' + '-' + ',';
					} else if (value.charAt(i) == '*') {
						splitValues = splitValues + ',' + '*' + ',';
					} else if (value.charAt(i) == '/') {
						splitValues = splitValues + ',' + '/' + ',';
					} else if (value.charAt(i) == '%') {
						splitValues = splitValues + ',' + '%' + ',';
					} else {
						splitValues = splitValues + value.charAt(i);
					}
				}
			}
			var checkValues = splitValues.split(',');
			var decodedAmount = '';
			for (i = 0; i < checkValues.length; i++) {
				convValue = EMPTY_STRING;
				if (checkValues[i].includes('c') || checkValues[i].includes('m') || checkValues[i].includes('l') || checkValues[i].includes('t') || checkValues[i].includes('k') || checkValues[i].includes('h') || checkValues[i].includes('b')) {
					value = checkValues[i];
					var amountVal = decodeAmount(value);
					decodedAmount += amountVal;
				} else {
					decodedAmount += checkValues[i];
				}
			}
			if (!isvalidCalcNumber(decodedAmount))
				return values;
			value = eval(decodedAmount);
			value = unformatAmount(value.toString());
			convertedAmountValue = value;
		} else {
			convertedAmountValue = decodeAmount(value);
		}
	} else {
		if (!isvalidCalcNumber(value))
			return values;
		value = eval(value);
		value = unformatAmount(value.toString());
		convertedAmountValue = value;
	}
	formattedAmountValue = convertedAmountValue;
	return formattedAmountValue;
}

function isvalidCalcNumber(number) {
	var regex = /^([\+\-\*\/\%]?(\d+(\.\d{1,2})?))*$/;
	return regex.test(number);
}

function isValidChequePrefix(chequePrefix) {
	if (chequePrefix != null && chequePrefix.length > 6)
		return false;
	var regex = /^([a-zA-Z0-9])+$/;
	if (!regex.test(chequePrefix))
		return false;
	return true;
}

function onScenarioChange(id) {
	if (!isEmpty($('#' + id).val())) {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('SCENARIO_CODE', $('#' + id).val());
		validator.setValue('PROGRAM_ID', getProgramID());
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.utils.formScenarioExtractor');
		validator.setMethod('getScenarioData');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			return false;
		} else {
			clearFields();
			var modelDataJson = validator.getValue('MODEL_FILE_DATA');
			var scenarioDataJson = validator.getValue('SCENARIO_FILE_DATA');
			var modelDtl = JSON.parse(modelDataJson);
			var scenarioDtl = JSON.parse(scenarioDataJson);
			$.each(scenarioDtl, function(key, value) {
				if (modelDtl.model[key].type == 'CODE') {
					$('#' + key).val(value.value);
					$('#' + key + '_desc').html(value.description);
					$('#' + key).prop('readonly', value.readonly);
				} else if (modelDtl.model[key].type == 'SELECT') {
					$('#' + key).val(value.value);
					$('#' + key).prop('disabled', value.disabled);
				} else if (modelDtl.model[key].type == 'CHECKBOX') {
					setCheckbox(key, value.value);
					$('#' + key).prop('disabled', value.disabled);
				} else {
					$('#' + key).val(value.value);
					$('#' + key).prop('readonly', value.readonly);
				}
				if (value.validate) {
					validate(key);
				}
				if (value.focus) {
					$('#' + key).focus();
				}
			});
		}
	} else
		clearFields();
	// setFocusOnSubmit();
}

function setFormStyle() {
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('PROGRAM_ID', getProgramID());
	validator.setValue(ACTION, USAGE);
	validator.setClass('patterns.config.web.utils.formStyleExtractor');
	validator.setMethod('getStyleData');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		return false;
	} else {
		if (validator.getValue(RESULT) == DATA_UNAVAILABLE)
			return false;
		var sytleDataJson = validator.getValue('FORM_STYLE_DATA');
		var elementStyleDataJson = validator.getValue('ELEMENT_STYLE_MAP');
		var styleData = JSON.parse(sytleDataJson);
		var elementData = JSON.parse(elementStyleDataJson);
		var cssStyle = EMPTY_STRING;
		$.each(styleData, function(key, value) {
			cssStyle = cssStyle + "." + key + "{" + value + "}";
		});
		if (cssStyle != EMPTY_STRING) {
			$('#po_body').append('<style id="' + getProgramID() + '_custom">' + cssStyle + '</style>');
			$.each(elementData, function(key, value) {
				// $('#'+key).removeClass();
				$('#' + key).addClass(value);
			});
		}
	}
}

function showPopUpRemarks() {
	win = showWindow('remarks_view', EMPTY_STRING, PBS_REMARKS, EMPTY_STRING, true, false, false, 500, 200);
}

function loadLandingPage() {
	/*validator.clearMap();
	validator.setMtm(false);
	validator.setValue(ACTION, USAGE);
	validator.setClass('patterns.config.web.forms.framework.elanding');
	validator.setMethod('validateCashier');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		location.href = 'Renderer/common/elanding.jsp';
	} else {
		if (validator.getValue("DASHBOARD_REQ") == COLUMN_DISABLE) {
			location.href = 'Renderer/common/elanding.jsp';
			return;
		}

		if (validator.getValue("CASHIER") == COLUMN_ENABLE)
			location.href = 'Renderer/common/cashierDashboard.jsp';
		else if (validator.getValue("CASHIER") == COLUMN_DISABLE)
			location.href = 'Renderer/common/accountStatistics.jsp';
		else
			location.href = 'Renderer/common/elanding.jsp';
	}*/
	location.href = 'Renderer/common/elanding.jsp';
}

$(window).scroll(function() {
	if ($(window).scrollTop() > 30) {
		$('a.back-to-top').fadeIn('slow');
	} else {
		$('a.back-to-top').fadeOut('slow');
	}
});

$('a.back-to-top').click(function() {
	$('html, body').animate({
		scrollTop : 0
	}, 800);
	return false;
});

//Added by Dileep on 28-09-2016 start
function showProgramSearch() {
	if ($('.smartSearchBox').length > 0) {
		var searchVisible = $('.smartSearchBox').is(':visible');
		if (searchVisible) {
			$('.smartSearchBox').hide();
			$('#smartNavigate').val(EMPTY_STRING);
			$('#smartNavigate_popup').hide();
			$('#smartNavigate_iframe').hide();
		} else {
			$('.smartSearchBox').show();
			$('#smartNavigate').focus();
		}
	}
}
//Added by Dileep on 28-09-2016 end

//Added by Arul Prasath on 27-12-2016 start

function _grid_MoveUpFunction(gridvar) {
	var ros = EMPTY_STRING;
	ros = gridvar.getCheckedRows(0);

	var rosaerr = ros.split(',');
	if (ros == EMPTY_STRING) {
		alert(SELECT_ATLEAST_ONE);
	} else {
		if (rosaerr.length > 1) {
			alert(SELECT_ONE);
		} else {
			var rowId = rosaerr[0];
			gridvar.moveRowUp(rowId);
			//fref(rowId);
		}
	}
}

function _grid_MoveDownFunction(gridvar) {
	var ros = EMPTY_STRING;
	ros = gridvar.getCheckedRows(0);

	var rosaerr = ros.split(',');
	if (ros == EMPTY_STRING) {
		alert(SELECT_ATLEAST_ONE);
	} else {
		if (rosaerr.length > 1) {
			alert(SELECT_ONE);
		} else {
			var rowId = rosaerr[0];
			gridvar.moveRowDown(rowId);
			//fref(rowId);
		}
	}
}
//Added by Arul Prasath on 27-12-2016 end



function isValidOtherInformation10(otherInfomation) {
	if (otherInfomation == null || otherInfomation.length == LENGTH_0 || otherInfomation.length > LENGTH_10)

		return false;
	return true;
}

function isNumericWithOperator(number) {
	var regex = /^([\+\-\(\)]?(\d?))*$/;
	return regex.test(number);
}

function isValidPanNumber(code) {
	if (code == null || code.length > 10)
		return false;
	var regex = /^([A-Z0-9_])+$/;
	if (regex.test(code))
		return true;
	return false;
}



function isValidAccNumber(code) {
	if (code == null || code.length > 50)
		return false;
	var regex = /^([A-Z0-9_])+$/;
	if (regex.test(code))
		return true;
	return false;
}

function isValidPanNum(code) {
	if (code == null || code.length < 10)
		return false;
	var regex = /^([a-zA-Z]){3}([P,F,C,H,T]){1}([a-zA-Z]){1}([0-9]){4}([a-zA-Z]){1}?$/;
	if (regex.test(code))
		return true;
	return false;
}

function isValidTanNumber(code) {
	if (code == null || code.length < 10)
		return false;
	var regex = /^([a-zA-Z]){4}([0-9]){5}([a-zA-Z]){1}?$/;
	if (regex.test(code))
		return true;
	return false;
}

function _grid_generatexml_function(fref, columnArr) {
	var xmlRow = XML_VERSION + XML_ROWS_START;
	var rowID = fref.getCheckedRows(0).split(',');
	if (rowID==EMPTY_STRING)
		return EMPTY_STRING;
	for (var rowNumber = 0; rowNumber < rowID.length; rowNumber++) {
		xmlRow = xmlRow.concat("<row id='" + (rowID[rowNumber]) + "'>");
		for (var cellIndex = 0; cellIndex < columnArr.length; cellIndex++) {
			xmlRow = xmlRow.concat(XML_CELL_START + fref.cells(rowID[rowNumber], columnArr[cellIndex]).getValue() + XML_CELL_END);
		}
		xmlRow = xmlRow.concat(XML_ROW_END);
	}
	return xmlRow + XML_ROWS_END;
}

function setAccordMenuConsole(value) {
	if (!isEmpty(value)) {
		var loader = dhx4.ajax.postSync(getBasePath() + 'Renderer/common/accordMenuConsole.jsp', encodeURI(AJAX_TOKEN + '=' + '1&consoleCode=' + value));
	}
}
function isValidOtherInformation25(otherInfomation) {
	if (otherInfomation == null || otherInfomation.length == LENGTH_0 || otherInfomation.length > LENGTH_100)

			return false;
		return true;
	}
