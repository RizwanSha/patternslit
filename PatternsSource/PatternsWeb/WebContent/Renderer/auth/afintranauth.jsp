<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<%@taglib prefix="message" tagdir="/WEB-INF/tags/message"%>
<web:bundle baseName="patterns.config.web.forms.auth.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/auth/afintranauth.js" />
	</web:dependencies>
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="afintranauth.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/auth/afintranauth" id="afintranauth" method="POST">
				<web:viewContent id="po_view1">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="afintranauth.officecode" var="program" mandatory="true"/>
										</web:column>
										<web:column>
											<type:branch property="officeCode" id="officeCode" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="afintranauth.batchdate" var="program" />
										</web:column>
										<web:column>
											<type:date property="batchDate" id="batchDate" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="afintranauth.programid" var="program" />
										</web:column>
										<web:column>
											<type:optionID property="programId" id="programId" helpRequired="true" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle width="210px" />
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="afintranauth.frombatchno" var="program" />
										</web:column>
										<web:column>
											<type:number property="fromBatchNo" id="fromBatchNo" length="10px" size="10px" />
										</web:column>
										<web:column>
											<web:legend key="afintranauth.tobatchno" var="program" />
										</web:column>
										<web:column>
											<type:number property="toBatchNo" id="toBatchNo" length="10px" size="10px" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="form.userid" var="common" />
										</web:column>
										<web:column>
											<type:userHelp property="userID" id="userID" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:rowOdd>
										<web:column>
											<type:button id="submit" key="form.submit" var="common" onclick="loadPendingRecords();" />
											<type:button key="form.reset" id="reset" var="common" onclick="clearFields();" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<message:messageBox id="afintranauth" />
							</web:section>
							<web:section>
								<web:viewTitle var="program" key="afintranauth.gridtitle" />
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column span="2">
											<type:button key="form.approve" id="authorize" var="common" onclick="doAuthorize()" />
											<type:button key="form.reject" id="reject" var="common" onclick="doReject()" />
											<type:button key="form.refresh" id="refresh" var="common" onclick="doRefresh()" />
											<type:button key="form.view" id="view" var="common" onclick="doView()" />
										</web:column>
										<web:element property="gridbox_rowid" />
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:grid height="240px" width="1000px" id="queryGrid" src="auth/afintranauth.xml" paging="true">
											</web:grid>
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="form.notes" var="common" />
										</web:column>
										<web:column>
											<type:remarks property="notes" id="notes" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
						</web:sectionBlock>
					</web:dividerBlock>
				</web:viewContent>
				<web:element property="command" />
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
	<web:contextSearch />
</web:fragment>