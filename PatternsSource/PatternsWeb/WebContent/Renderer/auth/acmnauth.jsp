<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib prefix="message" tagdir="/WEB-INF/tags/message"%>
<%@taglib prefix="resource" tagdir="/WEB-INF/tags/resource"%>
<web:bundle baseName="patterns.config.web.forms.auth.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/auth/acmnauth.js" />
	</web:dependencies>
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="acmnauth.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/auth/acmnauth" id="acmnauth" method="POST">
				<web:viewContent id="po_view1">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle width="210px" />
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="form.optionid" var="common" />
										</web:column>
										<web:column span="3">
											<type:optionID property="optionID" id="optionID" helpRequired="true" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="form.fromdate" var="common" />
										</web:column>
										<web:column>
											<type:date property="fromDate" id="fromDate" />
										</web:column>
										<web:column>
											<web:legend key="form.uptodate" var="common" />
										</web:column>
										<web:column>
											<type:date property="toDate" id="toDate" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="form.userid" var="common" />
										</web:column>
										<web:column span="3">
											<type:userHelp property="userID" id="userID" />
										</web:column>
									</web:rowEven>
									
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:rowOdd>
										<web:column>
											<type:button id="submit" key="form.submit" var="common" onclick="loadPendingRecords();" />
											<type:button key="form.reset" id="reset" var="common" onclick="clearFields();"/>
											<span id="generic_error" class="level4_error"><html:errors property="genericError" /></span>&nbsp;
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<message:messageBox id="acmnauth" />
							</web:section>
							<web:section>
								<web:viewTitle var="program" key="acmnauth.gridtitle" />
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column span="2">
											<type:button key="form.approve" id="authorize" var="common" onclick="doAuthorize()" />
											<type:button key="form.reject" id="reject" var="common" onclick="doReject()" />
											<type:button key="form.refresh" id="refresh" var="common" onclick="doRefresh()" />
											<type:button key="form.view" id="view" var="common" onclick="doView()" />
										</web:column>
										<web:element property="gridbox_rowid" />
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:grid height="240px" width="100%" id="queryGrid" src="auth/acmnauth.xml" paging="true">
											</web:grid>
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
						</web:sectionBlock>
					</web:dividerBlock>
				</web:viewContent>
				<web:element property="command" />
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
	<web:contextSearch />
</web:fragment>