var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'ACMNAUTH';
var desc = EMPTY_STRING;
var srckey = EMPTY_STRING;
var opeflag = EMPTY_STRING;
var tbaEntryDate = EMPTY_STRING;
var tbadtlsl;
var fromdt = EMPTY_STRING;
var uptodt = EMPTY_STRING;
var opr_flag = EMPTY_STRING;
function init() {
	$('#optionID').focus();
}

function doHelp(id) {
	switch (id) {
	case 'optionID':
		help(CURRENT_PROGRAM_ID, 'OPTIONS', $('#optionID').val(), EMPTY_STRING,
				$('#optionID'));
		break;
	case 'userID':
		help(CURRENT_PROGRAM_ID, 'USERS', $('#userID').val(), EMPTY_STRING,
				$('#userID'));
		break;

	}
}
function clearFields() {

	$('#optionID').val(EMPTY_STRING);
	$('#optionID_error').html(EMPTY_STRING);
	$('#fromDate').val(EMPTY_STRING);
	$('#fromDate_error').html(EMPTY_STRING);
	$('#toDate').val(EMPTY_STRING);
	$('#toDate_error').html(EMPTY_STRING);
	$('#userID').val(EMPTY_STRING);
	$('#userID_error').html(EMPTY_STRING);
	queryGrid.clearAll();
}

function change(id) {
}

function validate(id) {
	clearError('generic_error');
	switch (id) {
	case 'optionID':
		optionID_val();
		break;
	case 'fromDate':
		fromDate_val();
		break;
	case 'toDate':
		toDate_val();
		break;
	case 'userID':
		userID_val();
		break;

	}
}

function backtrack(id) {
	switch (id) {
	case 'optionID':
		setFocusLast('optionID');
		break;
	case 'fromDate':
		setFocusLast('optionID');
		break;
	case 'toDate':
		setFocusLast('fromDate');
		break;
	case 'userID':
		setFocusLast('toDate');
		break;
	case 'submit':
		setFocusLast('userID');
		break;
	}
}
var rows = EMPTY_STRING;

function optionID_val() {
	var value = $('#optionID').val();
	clearError('optionID_error');
	if (isEmpty(value)) {
		setFocusLast('fromDate');
		return true;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('MPGM_ID', $('#optionID').val());
	validator.setClass('patterns.config.web.forms.auth.acmnauthbean');
	validator.setMethod('validateOptionID');
	validator.sendAndReceive();
	if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		$('#optionID_desc').html(validator.getValue('MPGM_DESCN'));
		return true;
	} else {
		setError('optionID_error', validator.getValue(ERROR));
		return false;
	}
	setFocusLast('fromDate');
	return true;
}

function fromDate_val() {
	var value = $('#fromDate').val();
	clearError('fromDate_error');
	if (isEmpty(value)) {
		setFocusLast('toDate');
		return true;
	}
	if (!isDate(value)) {
		setError('fromDate_error', INVALID_DATE);
		return false;
	}
	if (!isDateLesserEqual(value, getCBD())) {
		setError('fromDate_error', DATE_LECBD);
		return false;
	}
	setFocusLast('toDate');
	return true;
}

function toDate_val() {
	var value = $('#toDate').val();
	var fromDate = $('#fromDate').val();
	clearError('toDate_error');
	if (isEmpty(value)) {
		setFocusLast('userID');
		return true;
	}
	if (!isDate(value)) {
		setError('toDate_error', INVALID_DATE);
		return false;
	}
	if (!isDateLesserEqual(value, getCBD())) {
		setError('toDate_error', DATE_LECBD);
		return false;
	}
	if (!isDateGreaterEqual(value, fromDate)) {
		setError('toDate_error', DATE_GEFD);
		return false;
	}
	setFocusLast('userID');
	return true;
}

function userID_val() {
	var value = $('#userID').val();
	clearError('userID_error');
	if (isEmpty(value)) {
		setFocusLast('submit');
		return true;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('USER_ID', $('#userID').val());
	validator.setClass('patterns.config.web.forms.auth.acmnauthbean');
	validator.setMethod('validateUserID');
	validator.sendAndReceive();
	if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		$('#userID_desc').html(validator.getValue('USER_NAME'));
		return true;
	} else {
		setError('userID_error', validator.getValue(ERROR));
		return false;
	}
	setFocusLast('submit');
	return true;
}

function loadPendingRecords() {
	clearError('generic_error');

	if (revalidate()) {
		$('#submit').prop('disabled', true);
		showMessage(getProgramID(), Message.PROGRESS);
		queryGrid.clearAll();
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('MPGM_ID', $('#optionID').val());
		validator.setValue('USER_ID', $('#userID').val());
		validator.setValue('FROM_DATE', $('#fromDate').val());
		validator.setValue('TO_DATE', $('#toDate').val());
		validator.setClass('patterns.config.web.forms.auth.acmnauthbean');
		validator.setMethod('loadPendingRecords');
		// validator.sendAndReceive();
		validator.sendAndReceiveAsync(showStatus);

	}
}
function showStatus() {
	hideMessage(getProgramID());
	if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		queryGrid.clearAll();
		queryGrid
				.setColSorting("na,na,str,str,str,str,str,datetimesort_custom,na,na,na,na,na");
		queryGrid.attachEvent("onRowSelect", doRowSelect);
		queryGrid.attachEvent("onRowDblClicked", doRowDblClicked);
		queryGrid.loadXMLString(validator.getValue(RESULT_XML));
	} else if (validator.getValue(RESULT) == DATA_UNAVAILABLE) {
		setError('generic_error', NO_VALID_RECORDS);
	} else if (validator.getValue("SOD_COMPL") == '0') {
		setError('generic_error', HMS_SOD_NOT_DONE);
		alert(HMS_SOD_NOT_DONE);
	} else {
		setError('generic_error', validator.getValue(ERROR));
	}

	$('#submit').prop('disabled', false);
	if ($('#generic_error').html() == EMPTY_STRING) {
		return true;
	} else {
		return false;
	}
}

function revalidate() {
	errors = 0;
	if (!optionID_val()) {
		++errors;
	}
	if (!fromDate_val()) {
		++errors;
	}
	if (!toDate_val()) {
		++errors;
	}
	if (!userID_val()) {
		++errors;
	}
	if (errors > 0)
		return false;
	return true;
}

function processAction(actionType) {
	var rowID = getOneSelectedRecord(queryGrid);
	if ((rowID < 0)) {
		alert(SELECT_ATLEAST_ONE);
	} else if (rowID == -2) {
		alert(SELECT_ONE);
	} else {
		if (queryGrid.cells(rowID, 10).getValue() == 'A') {
			alert(RECORD_A_AUTHORIZED);
			return false;
		} else if (queryGrid.cells(rowID, 10).getValue() == 'R') {
			alert(RECORD_A_REJECTED);
			return false;
		} else {
			if (queryGrid.cells(rowID, 11).getValue() != COLUMN_ENABLE) {
				alert(ALERT_VIEW_RECORD);
				return false;
			}
			optionID = queryGrid.cells(rowID, 2).getValue();
			userID = queryGrid.cells(rowID, 6).getValue();
			srckey = queryGrid.cells(rowID, 4).getValue();
			opr_flag = queryGrid.cells(rowID, 5).getValue();
			tbaEntryDate = queryGrid.cells(rowID, 8).getValue();
			tbadtlsl = queryGrid.cells(rowID, 9).getValue();
			validator.clearMap();
			validator.setMtm(false);
			validator.setClass('patterns.config.web.forms.auth.acmnauthbean');
			if (actionType == 'A') {
				validator.setMethod('validateAuthorize');
			}
			if (actionType == 'R') {
				validator.setMethod('validateReject');
			}
			validator.setValue('TBAQ_PGM_ID', optionID);
			validator.setValue('TBAQ_DONE_BY', userID);
			validator.setValue('TBAQ_MAIN_PK', srckey);
			validator.setValue('TBAQ_ENTRY_DATE', tbaEntryDate);
			validator.setValue('ACTION_TYPE', opr_flag);
			validator.setValue('TBAQ_DTL_SL', tbadtlsl);
			validator.setValue('PROGRAM_ID', CURRENT_PROGRAM_ID);
			validator.setValue('tfaNonce', getTFANonce());
			if (!tfaRequired(validator, true)) {
				return false;
			}
			// added by swaroopa as on 10-05-2012 begins
			validator.setValue('tfaValue', getTFAValue());
			validator.setValue('tfaEncodedValue', getTFAEncodedValue());
			// progress();
			// added by swaroopa as on 10-05-2012 begins
			validator.sendAndReceive();
			if (validator.getValue(RESULT) == DATA_AVAILABLE) {
				// added by swaroopa
				if (validator.getValue("TFA_ERROR") == COLUMN_DISABLE) {
					if (actionType == 'A') {
						if (validator.getValue('auth_status') == '1') {
							var message = RECORD_AUTHORIZED;
							var additionalDetails = validator
									.getValue(ADDITIONAL_DETAILS);
							if (additionalDetails != null
									|| additionalDetails != "") {
								message = message + "   " + additionalDetails;
							}
							alert(message);
						} else if (validator.getValue('auth_status') == '2') {
							alert(RECORD_DBL_AUTHORIZED);
						}
						queryGrid.cells(rowID, 0).setValue(false);
						queryGrid.cells(rowID, 10).setValue('A');
						queryGrid.setRowColor(rowID, '#F34E04');
					}
					if (actionType == 'R') {
						alert(RECORD_REJECTED);
						queryGrid.cells(rowID, 0).setValue(false);
						queryGrid.cells(rowID, 10).setValue('R');
						queryGrid.setRowColor(rowID, '#F34E04');
					}
				} else {
					alert(RECORD_NOT_AUTHORIZED + ' :: '
							+ validator.getValue("TFA_ERROR_MSG"));
					redirectLink(getBasePath() + EUNAUTHACCESS_JSP);
				}
				return true;
			} else {
				if (actionType == 'A') {
					alert(RECORD_NOT_AUTHORIZED + ' :: '
							+ validator.getValue(ERROR));
				}
				if (actionType == 'R') {
					alert(RECORD_NOT_REJECTED + ' :: '
							+ validator.getValue(ERROR));
				}
				return false;
			}
		}
	}
}

function doAuthorize() {
	var rowID = getOneSelectedRecord(queryGrid);
	if (queryGrid.cells(rowID, 12).getValue() == '0') {
		alert(HMS_AUTHORIZE_NOT_ALLOWED);
		return;
	}
	return processAction('A');
}

function doReject() {
	var rowID = getOneSelectedRecord(queryGrid);
	if (queryGrid.cells(rowID, 13).getValue() == '0') {
		alert(HMS_REJECTION_NOT_ALLOWED);
		return;
	}
	return processAction('R');
}

function doRefresh() {
	queryGrid.clearAll();
	loadPendingRecords();
}

function doRowSelect(rowID) {
	queryGrid.clearSelection();
	queryGrid.selectRowById(rowID, true);
	queryGrid.cells(rowID, 0).setValue(true);
}

function doRowDblClicked(rowID) {
	queryGrid.clearSelection();
	queryGrid.selectRowById(rowID, true);
	queryGrid.cells(rowID, 0).setValue(true);
	doView();
}

function checkAccess() {
	var rowID = getOneSelectedRecord(queryGrid);
	var authAllowed = queryGrid.cells(rowID, 12).getValue();
	var rejAllowed = queryGrid.cells(rowID, 13).getValue();
	if (authAllowed == '0') {
		$('#authorize').prop("disabled", true);
	} else {
		$('#authorize').prop("disabled", false);
	}
	if (rejAllowed == '0') {
		$('#reject').prop("disabled", true);
	} else {
		$('#reject').prop("disabled", false);
	}

}

function doView() {
	var rowID = getOneSelectedRecord(queryGrid);
	if ((rowID < 0)) {
		alert(SELECT_ATLEAST_ONE);
	} else if (rowID == -2) {
		alert(SELECT_ONE);
	} else {
		var srckey = queryGrid.cells(rowID, 4).getValue();
		var source = TBA;
		PK_VALUE = srckey;
		view(source, srckey);
		queryGrid.setRowColor(rowID, '#C3C3C3');
		queryGrid.cells(rowID, 11).setValue(COLUMN_ENABLE);
	}
}

function view(source, primaryKey) {
	var rowID = getOneSelectedRecord(queryGrid);
	var querypath = queryGrid.cells(rowID, 1).getValue();
	hideParent(querypath, source, primaryKey);
	resetLoading();
}