var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'AFINTRANAUTH';
var desc = EMPTY_STRING;
var srckey = EMPTY_STRING;
var opeflag = EMPTY_STRING;
var tbaEntryDate = EMPTY_STRING;
var tbadtlsl;
var fromdt = EMPTY_STRING;
var uptodt = EMPTY_STRING;
var opr_flag = EMPTY_STRING;
var rows = EMPTY_STRING;
var tranPK=EMPTY_STRING;
function init() {
	$('#officeCode_desc').html(getBranchName());
	$('#officeCode').focus();
}

function doHelp(id) {
	switch (id) {
	case 'officeCode':
		help('COMMON', 'HLP_ENTITY_BRN', $('#officeCode').val(), getEntityCode(), $('#officeCode'));
		break;
	case 'programId':
		help(CURRENT_PROGRAM_ID, 'OPTIONS', $('#programId').val(), EMPTY_STRING, $('#programId'));
		break;
	case 'userID':
		help(CURRENT_PROGRAM_ID, 'USERS', $('#userID').val(), EMPTY_STRING, $('#userID'));
		break;
	}
}

function clearFields() {
	$('#officeCode').val(getBranchCode());
	$('#officeCode_error').html(EMPTY_STRING);
	$('#officeCode_desc').html(getBranchName());
	$('#batchDate').val(getCBD());
	$('#batchDate_error').html(EMPTY_STRING);
	$('#programId').val(EMPTY_STRING);
	$('#programId_error').html(EMPTY_STRING);
	$('#programId_desc').html(EMPTY_STRING);
	$('#fromBatchNo').val(EMPTY_STRING);
	$('#fromBatchNo_error').html(EMPTY_STRING);
	$('#toBatchNo').val(EMPTY_STRING);
	$('#toBatchNo_error').html(EMPTY_STRING);
	$('#userID').val(EMPTY_STRING);
	$('#userID_error').html(EMPTY_STRING);
	$('#userID_desc').html(EMPTY_STRING);
	$('#notes').val(EMPTY_STRING);
	$('#notes_error').html(EMPTY_STRING);
	queryGrid.clearAll();
}

function change(id) {
}

function validate(id) {
	clearError('generic_error');
	switch (id) {
	case 'officeCode':
		officeCode_val(true);
		break;
	case 'batchDate':
		batchDate_val(true);
		break;
	case 'programId':
		programId_val(true);
		break;
	case 'fromBatchNo':
		fromBatchNo_val(true);
		break;
	case 'toBatchNo':
		toBatchNo_val(true);
		break;
	case 'userID':
		userID_val(true);
		break;
	case 'notes':
		notes_val();
		break;
	}
}

function backtrack(id) {
	switch (id) {
	case 'officeCode':
		setFocusLast('userID');
		break;
	case 'batchDate':
		setFocusLast('officeCode');
		break;
	case 'programId':
		setFocusLast('batchDate');
		break;
	case 'fromBatchNo':
		setFocusLast('programId');
		break;
	case 'toBatchNo':
		setFocusLast('fromBatchNo');
		break;
	case 'userID':
		setFocusLast('toBatchNo');
		break;
	case 'notes':
		setFocusLast('userID');
		break;
	case 'submit':
		setFocusLast('notes');
		break;
	}
}

function officeCode_val(valMode) {
	var value = $('#officeCode').val();
	clearError('officeCode_error');
	if (isEmpty(value)) {
		setError('officeCode_error', MANDATORY);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue(ACTION, USAGE);
	validator.setValue('BRANCH_CODE', $('#officeCode').val());
	validator.setClass('patterns.config.web.forms.auth.afintranauthbean');
	validator.setMethod('validateOfficeCode');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('officeCode_error', validator.getValue(ERROR));
		return false;
	}
	$('#officeCode_desc').html(validator.getValue('DESCRIPTION'));
	if (valMode)
		setFocusLast('batchDate');
	return true;
}

function batchDate_val(valMode) {
	var value = $('#batchDate').val();
	clearError('batchDate_error');
	if (isEmpty(value)) {
		if (valMode) {
			$('#batchDate').val(getCBD());
			value = $('#batchDate').val();
		}
	}
	if (!isDate(value)) {
		setError('batchDate_error', INVALID_DATE);
		return false;
	}
	if (!isDateLesserEqual(value, getCBD())) {
		setError('batchDate_error', DATE_LECBD);
		return false;
	}
	if (valMode)
		setFocusLast('programId');
	return true;
}

function programId_val(valMode) {
	var value = $('#programId').val();
	clearError('programId_error');
	if (isEmpty(value)) {
		if (valMode)
			setFocusLast('fromBatchNo');
		return true;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue(ACTION, USAGE);
	validator.setValue('MPGM_ID', $('#programId').val());
	validator.setClass('patterns.config.web.forms.auth.afintranauthbean');
	validator.setMethod('validateProgramId');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('programId_error', validator.getValue(ERROR));
		return false;
	}
	$('#programId_desc').html(validator.getValue('MPGM_DESCN'));
	if (valMode)
		setFocusLast('fromBatchNo');
	return true;
}

function fromBatchNo_val(valMode) {
	var value = $('#fromBatchNo').val();
	clearError('fromBatchNo_error');
	if (isEmpty(value)) {
		setFocusLast('toBatchNo');
		return true;
	}
	if (!isNumeric(value)) {
		setError('fromBatchNo_error', INVALID_NUMBER);
		return false;
	}
	if (!isPositive(value)) {
		setError('fromBatchNo_error', POSITIVE_CHECK);
		return false;
	}
	if (isZero(value)) {
		setError('fromBatchNo_error', ZERO_CHECK);
		return false;
	}
	if (valMode)
		setFocusLast('toBatchNo');
	return true;
}

function toBatchNo_val(valMode) {
	var value = $('#toBatchNo').val();
	var fromBatchNo = $('#fromBatchNo').val();
	clearError('toBatchNo_error');
	if (!isEmpty(fromBatchNo)) {
		if (isEmpty(value)) {
			setError('toBatchNo_error', MANDATORY);
			return false;
		}
		if (!isNumeric(value)) {
			setError('toBatchNo_error', INVALID_NUMBER);
			return false;
		}
		if (!isPositive(value)) {
			setError('toBatchNo_error', POSITIVE_CHECK);
			return false;
		}
		if (isZero(value)) {
			setError('toBatchNo_error', ZERO_CHECK);
			return false;
		}
		if (!isNumberGreaterThanEqual(value, fromBatchNo)) {
			setError('toBatchNo_error', TO_BATCH_GREATER_THAN_FROM_BATCH);
			return false;
		}
	} else {
		if (!isEmpty(value)) {
			setError('toBatchNo_error', TO_BATCH_NO_NT_ALLOWED);
			return false;
		}
	}
	if (valMode)
		setFocusLast('userID');
	return true;
}

function userID_val(valMode) {
	var value = $('#userID').val();
	clearError('userID_error');
	if (isEmpty(value)) {
		setFocusLast('submit');
		return true;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue(ACTION, USAGE);
	validator.setValue('USER_ID', $('#userID').val());
	validator.setClass('patterns.config.web.forms.auth.acmnauthbean');
	validator.setMethod('validateUserID');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('userID_error', validator.getValue(ERROR));
		return false;
	}
	$('#userID_desc').html(validator.getValue('USER_NAME'));
	if (valMode)
		setFocusLast('submit');
	return true;
}

function notes_val() {
	var value = $('#notes').val();
	clearError('notes_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('notes_error', INVALID_REMARKS);
			return false;
		}
		if (!isValidnotes(value)) {
			setError('notes_error', INVALID_REMARKS);
			return false;
		}
	}
	return true;
}

function loadPendingRecords() {
	clearError('generic_error');
	queryGrid.clearAll();
	if (revalidate()) {
		$('#submit').prop('disabled', true);
		showMessage(getProgramID(), Message.PROGRESS);
		queryGrid.clearAll();
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('BRANCH_CODE', $('#officeCode').val());
		validator.setValue('BATCH_DATE', $('#batchDate').val());
		validator.setValue('MPGM_ID', $('#programId').val());
		validator.setValue('FROM_BATCH_NO', $('#fromBatchNo').val());
		validator.setValue('TO_BATCH_NO', $('#toBatchNo').val());
		validator.setValue('USER_ID', $('#userID').val());
		validator.setClass('patterns.config.web.forms.auth.afintranauthbean');
		validator.setMethod('loadPendingRecords');
		validator.sendAndReceiveAsync(showStatus);
	}
}

function showStatus() {
	$('#submit').prop('disabled', false);
	hideMessage(getProgramID());
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		showMessage(getProgramID(), Message.ERROR, validator.getValue(ERROR));
		return false;
	}
	if (validator.getValue(RESULT) == DATA_UNAVAILABLE) {
		showMessage(getProgramID(), Message.ERROR, NO_RECORD);
		return false;
	}
	if (validator.getValue("SOD_COMPL") == '0') {
		showMessage(getProgramID(), Message.ERROR, HMS_SOD_NOT_DONE);
		alert(HMS_SOD_NOT_DONE);
	}
	if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		queryGrid.clearAll();
		// queryGrid.setColSorting("na,na,str,str,str,str,str,datetimesort_custom,na,na,na,na,na");
		queryGrid.attachEvent("onRowSelect", doRowSelect);
		queryGrid.attachEvent("onRowDblClicked", doRowDblClicked);
		queryGrid.loadXMLString(validator.getValue(RESULT_XML));
	}
}

function revalidate() {
	errors = 0;
	if (!batchDate_val(false)) {
		++errors;
	}
	if (!programId_val(false)) {
		++errors;
	}
	if (!fromBatchNo_val(false)) {
		++errors;
	}
	if (!toBatchNo_val(false)) {
		++errors;
	}
	if (!userID_val(false)) {
		++errors;
	}
	if (!notes_val()) {
		++errors;
	}
	if (errors > 0)
		return false;
	return true;
}

function processAction(actionType) {
	var rowID = getOneSelectedRecord(queryGrid);
	if ((rowID < 0)) {
		alert(SELECT_ATLEAST_ONE);
	} else if (rowID == -2) {
		alert(SELECT_ONE);
	} else {
		if (queryGrid.cells(rowID, 12).getValue() == 'A') {
			alert(RECORD_A_AUTHORIZED);
			return false;
		} else if (queryGrid.cells(rowID, 12).getValue() == 'R') {
			alert(RECORD_A_REJECTED);
			return false;
		} else {
			if (queryGrid.cells(rowID, 13).getValue() != COLUMN_ENABLE) {
				alert(ALERT_VIEW_RECORD);
				return false;
			}
			programId = queryGrid.cells(rowID, 2).getValue();
			userID = queryGrid.cells(rowID, 9).getValue();
			srckey = queryGrid.cells(rowID, 4).getValue();
			officeCode = queryGrid.cells(rowID, 6).getValue();
			batchDate = queryGrid.cells(rowID, 7).getValue();
			batchNo = queryGrid.cells(rowID, 8).getValue();
			opr_flag = queryGrid.cells(rowID, 9).getValue();
			tranPK=queryGrid.cells(rowID, 5).getValue();
			validator.clearMap();
			validator.setMtm(false);
			validator.setClass('patterns.config.web.forms.auth.afintranauthbean');
			if (actionType == 'A') {
				validator.setMethod('validateAuthorize');
			}
			if (actionType == 'R') {
				validator.setMethod('validateReject');
			}

			validator.setValue('OFFICE_CODE', officeCode);
			validator.setValue('BATCH_DATE', batchDate);
			validator.setValue('BATCH_NO', batchNo);
			validator.setValue('SOURCE_PGM', programId);
			validator.setValue('SOURCE_KEY', srckey);
			validator.setValue('ACTION_TYPE', opr_flag);
			validator.setValue('DONE_BY', userID);
			validator.setValue('PROGRAM_ID', CURRENT_PROGRAM_ID);
			validator.setValue('TRAN_PK', tranPK);
			validator.setValue('tfaNonce', getTFANonce());
			if (!tfaRequired(validator, true)) {
				return false;
			}
			// added by swaroopa as on 10-05-2012 begins
			validator.setValue('tfaValue', getTFAValue());
			validator.setValue('tfaEncodedValue', getTFAEncodedValue());
			// progress();
			// added by swaroopa as on 10-05-2012 begins
			validator.sendAndReceive();
			if (validator.getValue(RESULT) == DATA_AVAILABLE) {
				// added by swaroopa
				if (validator.getValue("TFA_ERROR") == COLUMN_DISABLE) {
					if (actionType == 'A') {
						if (validator.getValue('auth_status') == '1') {
							var message = RECORD_AUTHORIZED;
							var additionalDetails = validator.getValue(ADDITIONAL_DETAILS);
							if (additionalDetails != null || additionalDetails != "") {
								message = message + "   " + additionalDetails;
							}
							alert(message);
						} else if (validator.getValue('auth_status') == '2') {
							alert(RECORD_DBL_AUTHORIZED);
						}
						queryGrid.cells(rowID, 0).setValue(false);
						queryGrid.cells(rowID, 11).setValue('A');
						queryGrid.setRowColor(rowID, '#F34E04');
					}
					if (actionType == 'R') {
						alert(RECORD_REJECTED);
						queryGrid.cells(rowID, 0).setValue(false);
						queryGrid.cells(rowID, 11).setValue('R');
						queryGrid.setRowColor(rowID, '#F34E04');
					}
				} else {
					alert(RECORD_NOT_AUTHORIZED + ' :: ' + validator.getValue("TFA_ERROR_MSG"));
					redirectLink(getBasePath() + EUNAUTHACCESS_JSP);
				}
				return true;
			} else {
				if (actionType == 'A') {
					alert(RECORD_NOT_AUTHORIZED + ' :: ' + validator.getValue(ERROR));
				}
				if (actionType == 'R') {
					alert(RECORD_NOT_REJECTED + ' :: ' + validator.getValue(ERROR));
				}
				return false;
			}
		}
	}
}

function doAuthorize() {
	var rowID = getOneSelectedRecord(queryGrid);
	if ((rowID < 0)) {
		alert(SELECT_ATLEAST_ONE);
		return;
	} else if (rowID == -2) {
		alert(SELECT_ONE);
		return;
	} else {
		if (queryGrid.cells(rowID, 14).getValue() == '0') {
			alert(HMS_AUTHORIZE_NOT_ALLOWED);
			return;
		}
	}
	return processAction('A');

}

function doReject() {
	var rowID = getOneSelectedRecord(queryGrid);
	if ((rowID < 0)) {
		alert(SELECT_ATLEAST_ONE);
		return;
	} else if (rowID == -2) {
		alert(SELECT_ONE);
		return;
	} else {
		if (queryGrid.cells(rowID, 15).getValue() == '0') {
			alert(HMS_REJECTION_NOT_ALLOWED);
			return;
		}
	}
	return processAction('R');
}

function doRefresh() {
	queryGrid.clearAll();
	loadPendingRecords();
}

function doRowSelect(rowID) {
	queryGrid.clearSelection();
	queryGrid.selectRowById(rowID, true);
	queryGrid.cells(rowID, 0).setValue(true);
}

function doRowDblClicked(rowID) {
	queryGrid.clearSelection();
	queryGrid.selectRowById(rowID, true);
	queryGrid.cells(rowID, 0).setValue(true);
	doView();
}

function doView() {
	var rowID = getOneSelectedRecord(queryGrid);
	if ((rowID < 0)) {
		alert(SELECT_ATLEAST_ONE);
	} else if (rowID == -2) {
		alert(SELECT_ONE);
	} else {
		var srckey = queryGrid.cells(rowID, 4).getValue();
		var source = TBA;
		PK_VALUE = srckey;
		view(source, srckey);
		queryGrid.setRowColor(rowID, '#C3C3C3');
		queryGrid.cells(rowID, 13).setValue(COLUMN_ENABLE);
	}
}

function view(source, primaryKey) {
	var rowID = getOneSelectedRecord(queryGrid);
	var querypath = queryGrid.cells(rowID, 1).getValue();
	hideParent(querypath, source, primaryKey);
	resetLoading();
}