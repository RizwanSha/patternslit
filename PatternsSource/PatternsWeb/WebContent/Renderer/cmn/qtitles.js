var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MTITLES';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}
function clearFields() {
	$('#titleCode').val(EMPTY_STRING);
	$('#titleCode_desc').html(EMPTY_STRING);
	$('#titleCode_error').html(EMPTY_STRING);
	clearNonPKFields();
}

function clearNonPKFields() {
	$('#extDescription').val(EMPTY_STRING);
	$('#extDescription_error').html(EMPTY_STRING);
	$('#intDescription').val(EMPTY_STRING);
	$('#intDescription_error').html(EMPTY_STRING);
	$('#shortDescription').val(EMPTY_STRING);
	$('#shortDescription_error').html(EMPTY_STRING);
	$('#titleUsage').val(EMPTY_STRING);
	$('#titleUsage_error').html(EMPTY_STRING);
	setCheckbox('titleChildrenUsage', NO);
	$('#titleChildrenUsage_error').html(EMPTY_STRING);
	setCheckbox('titleDeceasedUsage', NO);
	$('#titleDeceasedUsage_error').html(EMPTY_STRING);
	$('#titleOccupation').val(EMPTY_STRING);
	$('#titleOccupation_desc').html(EMPTY_STRING);
	$('#titleOccupation_error').html(EMPTY_STRING);
	$('#otherOccupation').val(EMPTY_STRING);
	$('#otherOccupation_error').html(EMPTY_STRING);
	$('#defMaritalStatus').val(EMPTY_STRING);
	$('#defMaritalStatus_desc').html(EMPTY_STRING);
	$('#defMaritalStatus_error').html(EMPTY_STRING);
	setCheckbox('fixMaritalStatus', NO);
	$('#fixMaritalStatus_error').html(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
}

function loadData() {
	$('#titleCode').val(validator.getValue('TITLE_CODE'));
	$('#titleCode_desc').html(validator.getValue('INT_DESCRIPTION'));
	$('#extDescription').val(validator.getValue('EXT_DESCRIPTION'));
	$('#intDescription').val(validator.getValue('INT_DESCRIPTION'));
	$('#shortDescription').val(validator.getValue('CONCISE_DESCRIPTION'));
	$('#titleUsage').val(validator.getValue('TITLE_USAGE'));
	setCheckbox('titleChildrenUsage', validator.getValue('TITLE_CHILDREN_USG'));
	setCheckbox('titleDeceasedUsage', validator.getValue('TITLE_DECEASED_USG'));
	$('#titleOccupation').val(validator.getValue('TITLE_OCCU'));
	$('#titleOccupation_desc').html(validator.getValue('F1_DESCRIPTION'));
	$('#otherOccupation').val(validator.getValue('TITLE_OTH_OCCU'));
	$('#defMaritalStatus').val(validator.getValue('DEFAULT_MARITAL_STATUS'));
	$('#defMaritalStatus_desc').html(validator.getValue('F2_DESCRIPTION'));
	setCheckbox('fixMaritalStatus', validator.getValue('MARITAL_STATUS_FIXED'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadAuditFields(validator);
}
