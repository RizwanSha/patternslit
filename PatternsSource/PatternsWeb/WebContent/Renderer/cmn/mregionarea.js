var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MREGIONAREA';
function init() {
	refreshTBAGrid();
	refreshQueryGrid();
	if (_redisplay) {
		if ($('#action').val() == ADD) {
			$('#enabled').prop('disabled', true);
			setCheckbox('enabled', YES);
		}
	}
}

function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'MREGIONAREA_MAIN', queryGrid);
}

function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}

function doHelp(id) {
	switch (id) {
	case 'regionCode':
		help('COMMON', 'HLP_REGION_CODE', $('#regionCode').val(), EMPTY_STRING, $('#regionCode'));
		break;
	case 'areaCode':
		help('COMMON', 'HLP_AREA_CODE', $('#areaCode').val(), $('#regionCode').val(), $('#areaCode'));
		break;
	}
}

function clearFields() {
	$('#regionCode').val(EMPTY_STRING);
	$('#regionCode_desc').html(EMPTY_STRING);
	$('#regionCode_error').html(EMPTY_STRING);
	$('#areaCode').val(EMPTY_STRING);
	$('#areaCode_desc').html(EMPTY_STRING);
	$('#areaCode_error').html(EMPTY_STRING);
	clearNonPKFields();
}

function clearNonPKFields() {
	$('#description').val(EMPTY_STRING);
	$('#description_error').html(EMPTY_STRING);
	$('#conciseDescription').val(EMPTY_STRING);
	$('#conciseDescription_error').html(EMPTY_STRING);
	setCheckbox('enabled', YES);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
}

function doclearfields(id) {
	switch (id) {
	case 'regionCode':
		if (isEmpty($('#regionCode').val())) {
			$('#regionCode_desc').html(EMPTY_STRING);
			$('#regionCode_error').html(EMPTY_STRING);
			clearNonPKFields();
			break;
		}
	case 'areaCode':
		if (isEmpty($('#areaCode').val())) {
			$('#areaCode_desc').html(EMPTY_STRING);
			$('#areaCode_error').html(EMPTY_STRING);
			clearNonPKFields();
			break;
		}
	}
}

function add() {
	$('#regionCode').focus();
	setCheckbox('enabled', YES);
	$('#enabled').prop('disabled', true);
}

function modify() {
	$('#regionCode').focus();
	if ($('#action').val() == MODIFY) {
		$('#enabled').prop('disabled', false);
	} else {
		$('#enabled').prop('disabled', true);
	}
}

function loadData() {
	$('#regionCode').val(validator.getValue('REGION_CODE'));
	$('#regionCode_desc').html(validator.getValue('F1_DESCRIPTION'));
	$('#areaCode').val(validator.getValue('AREA_CODE'));
	$('#description').val(validator.getValue('DESCRIPTION'));
	$('#conciseDescription').val(validator.getValue('CONCISE_DESCRIPTION'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	resetLoading();
}

function view(source, primaryKey) {
	hideParent('cmn/qregionarea', source, primaryKey);
	resetLoading();
}

function validate(id) {
	switch (id) {
	case 'regionCode':
		regionCode_val();
		break;
	case 'areaCode':
		areaCode_val();
		break;
	case 'description':
		description_val();
		break;
	case 'conciseDescription':
		conciseDescription_val();
		break;
	case 'enabled':
		enabled_val();
		break;
	case 'remarks':
		remarks_val();
		break;
	}
}

function backtrack(id) {
	switch (id) {
	case 'regionCode':
		setFocusLast('regionCode');
		break;
	case 'areaCode':
		setFocusLast('regionCode');
		break;
	case 'description':
		setFocusLast('areaCode');
		break;
	case 'conciseDescription':
		setFocusLast('description');
		break;
	case 'enabled':
		setFocusLast('conciseDescription');
		break;
	case 'remarks':
		if ($('#action').val() == MODIFY) {
			setFocusLast('enabled');
		} else {
			setFocusLast('conciseDescription');
		}
		break;
	}
}

function regionCode_val() {
	var value = $('#regionCode').val();
	clearError('regionCode_error');
	$('#regionCode_desc').html(EMPTY_STRING);
	if (isEmpty(value)) {
		setError('regionCode_error', MANDATORY);
		return false;
	}
	if (!isValidCode(value)) {
		setError('regionCode_error', INVALID_FORMAT);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('REGION_CODE', value);
	validator.setValue(ACTION, USAGE);
	validator.setClass('patterns.config.web.forms.cmn.mregionareabean');
	validator.setMethod('validateRegionCode');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('regionCode_error', validator.getValue(ERROR));
		return false;
	} else if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		$('#regionCode_desc').html(validator.getValue('DESCRIPTION'));

	}
	setFocusLast('areaCode');
	return true;
}

function areaCode_val() {
	var value = $('#areaCode').val();
	clearError('areaCode_error');
	$('#areaCode_desc').html(EMPTY_STRING);
	if (isEmpty(value)) {
		setError('areaCode_error', MANDATORY);
		return false;
	}
	if (!isValidCode(value)) {
		setError('areaCode_error', INVALID_FORMAT);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('REGION_CODE', $('#regionCode').val());
	validator.setValue('AREA_CODE', value);
	validator.setValue(ACTION, $('#action').val());
	validator.setClass('patterns.config.web.forms.cmn.mregionareabean');
	validator.setMethod('validateAreaCode');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('areaCode_error', validator.getValue(ERROR));
		return false;
	} else if ($('#action').val() == MODIFY) {
		PK_VALUE = getEntityCode() + PK_SEPERATOR + $('#regionCode').val() + PK_SEPERATOR + $('#areaCode').val();
		if (!loadPKValues(PK_VALUE, 'areaCode_error')) {
			return false;
		}
	}

	setFocusLast('description');
	return true;
}

function description_val() {
	var value = $('#description').val();
	clearError('description_error');
	if (isEmpty(value)) {
		setError('description_error', MANDATORY);
		return false;
	}
	if (!isValidDescription(value)) {
		setError('description_error', INVALID_DESCRIPTION);
		return false;
	}
	setFocusLast('conciseDescription');
	return true;
}

function conciseDescription_val() {
	var value = $('#conciseDescription').val();
	clearError('conciseDescription_error');
	if (isEmpty(value)) {
		setError('conciseDescription_error', MANDATORY);
		return false;
	}
	if (!isValidConciseDescription(value)) {
		setError('conciseDescription_error', INVALID_CONCISE_DESCRIPTION);
		return false;
	}
	if ($('#action').val() == MODIFY)
		setFocusLast('enabled');
	else
		setFocusLast('remarks');
}

function enabled_val() {
	if ($('#action').val() == MODIFY) {
		setFocusLast('remarks');
		return true;
	}
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', INVALID_REMARKS);
			setFocusLast('remarks');
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', INVALID_REMARKS);
			setFocusLast('remarks');
			return false;
		}
	}
	if ($('#action').val() == MODIFY) {
		if (isEmpty(value)) {
			setError('remarks_error', MANDATORY);
			setFocusLast('remarks');
			return false;
		}
	}
	setFocusOnSubmit();
	return true;
}
