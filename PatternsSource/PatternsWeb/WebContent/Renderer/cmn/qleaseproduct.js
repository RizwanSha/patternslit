var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MLEASEPRODUCT';

function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}

function clearFields() {
	$('#leaseProductCode').val(EMPTY_STRING);
	$('#leaseProductDescription').val(EMPTY_STRING);
	$('#leaseProductConciseDescription').val(EMPTY_STRING);
	$('#leaseType').prop('selectedIndex', 0);
	$('#sundryDebtorsHead').val(EMPTY_STRING);
	$('#rentReceivablePrincipal').val(EMPTY_STRING);
	$('#rentReceivableInterest').val(EMPTY_STRING);
	$('#unearnedIncomeHead').val(EMPTY_STRING);
	setCheckbox('enabled', NO);
	$('#remarks').val(EMPTY_STRING);
}

function loadData() {
	$('#leaseProductCode').val(validator.getValue('LEASE_PRODUCT_CODE'));
	$('#leaseProductCode_desc').html(validator.getValue('DESCRIPTION'));
	$('#leaseProductDescription').val(validator.getValue('DESCRIPTION'));
	$('#leaseProductConciseDescription').val(validator.getValue('CONCISE_DESCRIPTION'));
	$('#leaseType').val(validator.getValue('LEASE_TYPE'));
	$('#baseCurrCode').val(validator.getValue('LEASE_ASSET_IN_CCY'));
	$('#baseCurrCode_desc').html(validator.getValue('F5_DESCRIPTION'));
	$('#sundryDebtorsHead').val(validator.getValue('SUNDRY_DEBTOR_GL_HEAD_CODE'));
	$('#sundryDebtorsHead_desc').html(validator.getValue('F1_DESCRIPTION'));
	$('#rentReceivablePrincipal').val(validator.getValue('RENT_RECV_PRIN_GL_HEAD_CODE'));
	$('#rentReceivablePrincipal_desc').html(validator.getValue('F2_DESCRIPTION'));
	$('#rentReceivableInterest').val(validator.getValue('RENT_RECV_INT_GL_HEAD_CODE'));
	$('#rentReceivableInterest_desc').html(validator.getValue('F3_DESCRIPTION'));
	$('#unearnedIncomeHead').val(validator.getValue('UNEARNED_INCOME_GL_HEAD_CODE'));
	$('#unearnedIncomeHead_desc').html(validator.getValue('F4_DESCRIPTION'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));loadAuditFields(validator);
}


