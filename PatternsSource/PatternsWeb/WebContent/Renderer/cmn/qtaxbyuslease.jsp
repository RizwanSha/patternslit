<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.cmn.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/cmn/qtaxbyuslease.js" />
	</web:dependencies>
	<web:loadPK />
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="itaxbyuslease.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:viewContent id="po_view1">
				<web:dividerBlock>
					<web:sectionBlock width="width-100p" align="left">
						<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle width="180px" />
										<web:columnStyle width="100px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="itaxbyuslease.lesseeCode" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:productCodeDisplay property="lesseeCode" id="lesseeCode" />
										</web:column>
										<web:column>
											<web:legend key="itaxbyuslease.customerid" var="program"  />
										</web:column>
										<web:column>
											<type:customerIDDisplay property="customerId" id="customerId" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle width="180px" />
										<web:columnStyle width="100px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="itaxbyuslease.agreementnumber" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:agreementNoDisplay property="agreementNumber" id="agreementNumber" />
										</web:column>
										<web:column>
											<web:legend key="itaxbyuslease.name" var="program" />
										</web:column>
										<web:column>
											<type:nameDisplay property="customerName" id="customerName"  />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="itaxbyuslease.scheduleid" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:scheduleIDDisplay property="scheduleId" id="scheduleId" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="itaxbyuslease.statecode" var="program" mandatory="true"/>
										</web:column>
										<web:column>
											<type:stateCodeDisplay property="stateCode" id="stateCode" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="itaxbyuslease.taxcode" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:taxCodeDisplay property="taxCode" id="taxCode" />
										</web:column>
									</web:rowEven>
									</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
										<web:rowOdd>
										<web:column>
											<web:legend key="itaxbyuslease.effectivedate" var="program" mandatory="true"/>
										</web:column>
										<web:column>
											<type:dateDisplay property="effectiveDate" id="effectiveDate" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="itaxbyuslease.taxborne" var="program" />
										</web:column>
										<web:column>
											<type:checkboxDisplay property="taxBorne" id="taxBorne" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="itaxbyuslease.accountinghead" var="program" />
										</web:column>
										<web:column>
											<type:GLHeadCodeDisplay property="accountingHead" id="accountingHead" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="form.IsUseEnabled" var="common" />
										</web:column>
										<web:column>
											<type:checkboxDisplay property="enabled" id="enabled" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="form.remarks" var="common" />
										</web:column>
										<web:column>
											<type:remarksDisplay property="remarks" id="remarks" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
						<web:auditDisplay />
					</web:sectionBlock>
				</web:dividerBlock>
			</web:viewContent>
		</web:viewSubPart>
	</web:viewPart>
</web:fragment>
