<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.cmn.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/cmn/mgeounitcon.js" />
	</web:dependencies>
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="mgeounitcon.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/cmn/mgeounitcon" id="mgeounitcon" method="POST">
				<web:dividerBlock>
					<type:optionBox add="true" modify="true" view="true" />
				</web:dividerBlock>
				<web:viewContent id="po_view1" styleClass="hidden">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="mgeounitcon.countrycode" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:countryCode property="countryCode" id="countryCode" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="mgeounitcon.geoUnitid" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:geographicalUnitId property="geoUnitID" id="geoUnitID" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="mgeounitcon.name" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:description property="name" id="name" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="mgeounitcon.shortname" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:conciseDescription property="shortName" id="shortName" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="mgeounitcon.parentgeounitid" var="program" />
										</web:column>
										<web:column>
											<type:geographicalUnitId property="parentgeoUnitID" id="parentgeoUnitID" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="mgeounitcon.geostructure" var="program" mandatory="true"/>
										</web:column>
										<web:column>
											<type:geographicalUnitStructureCode property="geoStructure" id="geoStructure" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="mgeounitcon.parentgeostructure" var="program" />
										</web:column>
										<web:column>
											<type:geographicalUnitStructureCode property="parentGeoStructure" id="parentGeoStructure" readOnly="true" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="mgeounitcon.geounittype" var="program"/>
										</web:column>
										<web:column>
											<type:geographicalUnitType property="geoUnitType" id="geoUnitType" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="form.IsUseEnabled" var="common" />
										</web:column>
										<web:column>
											<type:checkbox property="enabled" id="enabled" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="form.remarks" var="common" />
										</web:column>
										<web:column>
											<type:remarks property="remarks" id="remarks" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column span="2">
											<web:submitReset />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
						</web:sectionBlock>
					</web:dividerBlock>
				</web:viewContent>
				<web:element property="command" />
				<web:element property="action" />
				<web:element property="unitTypeAllowed" />
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
	<web:contextSearch />
	<web:queryGrid />
	<web:TBAGrid />
</web:fragment>