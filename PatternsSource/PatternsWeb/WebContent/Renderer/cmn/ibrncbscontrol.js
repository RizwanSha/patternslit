var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'IBRNCBSCONTROL';
function init() {
	refreshTBAGrid();
	refreshQueryGrid();
	if ($('#action').val() == ADD) {
		setCheckbox('enabled', YES);
		$('#enabled').prop('disabled', true);
	}
}

function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'IBRNCBSCONTROL_MAIN', queryGrid);
}

function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}

function add() {
	$('#entityOffCode').focus();
	setCheckbox('enabled', YES);
	$('#enabled').prop('disabled', true);
	$('#currCbsLink').prop('selectedIndex' , 0);
}

function modify() {
	$('#entityOffCode').focus();
	$('#enabled').prop('disabled', false);
}

function view(source, primaryKey) {
	hideParent('cmn/qbrncbscontrol', source, primaryKey);
	resetLoading();
}

function clearFields() {
	$('#entityOffCode').val(EMPTY_STRING);
	$('#entityOffCode_desc').html(EMPTY_STRING);
	$('#entityOffCode_error').html(EMPTY_STRING);
	clearNonPKFields();
}

function clearNonPKFields() {
	$('#inCbsDate').val(EMPTY_STRING);
	$('#inCbsDate_error').html(EMPTY_STRING);
	$('#transDate').val(EMPTY_STRING);
	$('#transDate_error').html(EMPTY_STRING);
	$('#currCbsLink').val(EMPTY_STRING);
	$('#currCbsLink_error').html(EMPTY_STRING);
	$('#currCbsLink').prop('selectedIndex' , 0);
	setCheckbox('enabled', YES);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
}

function doclearfields(id) {
	switch (id) {
	case 'entityOffCode':
		if (isEmpty($('#entityOffCode').val())) {
			clearFields();
		}
		break;
	}
}

function loadData() {
	$('#entityOffCode').val(validator.getValue('BRANCH_CODE'));
	$('#entityOffCode_desc').html(validator.getValue('F1_DESCRIPTION'));
	$('#inCbsDate').val(validator.getValue('IN_CBS_FROM_DATE'));
	$('#transDate').val(validator.getValue('TRAN_MIGRATED_FROM_DATE'));
	$('#currCbsLink').val(validator.getValue('CURRENT_CBS_LINK_STATUS'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	resetLoading();
}

function doHelp(id) {
	switch (id) {
	case 'entityOffCode':
		help('COMMON', 'HLP_OFFICE_CODE', $('#entityOffCode').val(), EMPTY_STRING, $('#entityOffCode'));
		break;
	}
}

function validate(id) {
	switch (id) {
	case 'entityOffCode':
		entityOffCode_val();
		break;
	case 'inCbsDate':
		inCbsDate_val();
		break;
	case 'transDate':
		transDate_val();
		break;
	case 'currCbsLink':
		currCbsLink_val();
		break;
	case 'enabled':
		setFocusLast('remarks');
		break;
	case 'remarks':
		remarks_val();
		break;
	}
}

function backtrack(id) {
	switch (id) {
	case 'entityOffCode':
		setFocusLast('entityOffCode');
		break;
	case 'inCbsDate':
		setFocusLast('entityOffCode');
		break;
	case 'transDate':
		setFocusLast('inCbsDate');
		break;
	case 'currCbsLink':
		setFocusLast('transDate');
		break;
	case 'enabled':
		setFocusLast('currCbsLink');
		break;
	case 'remarks':
		if ($('#action').val() == ADD)
			setFocusLast('currCbsLink');
		else
			setFocusLast('enabled');
		break;

	}
}

function entityOffCode_val() {
	var value = $('#entityOffCode').val();
	clearError('entityOffCode_error');
	if (isEmpty(value)) {
		setError('entityOffCode_error', MANDATORY);
		return false;
	}
	if ($('#action').val() == ADD && !isValidCode(value)) {
		setError('entityOffCode_error', HMS_INVALID_FORMAT);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('BRANCH_CODE', $('#entityOffCode').val());
		validator.setValue(ACTION, $('#action').val());
		validator.setClass('patterns.config.web.forms.cmn.ibrncbscontrolbean');
		validator.setMethod('validateEntityOffCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('entityOffCode_error', validator.getValue(ERROR));
			return false;
		} else {
			$('#autoDisPolCode_desc').html(validator.getValue('DESCRIPTION'));
			if ($('#action').val() == MODIFY) {
				PK_VALUE = getEntityCode() + PK_SEPERATOR + $('#entityOffCode').val();
				if (!loadPKValues(PK_VALUE, 'entityOffCode_error')) {
					return false;
				}
			}
		}
	}
	setFocusLast('inCbsDate');
	return true;
}

function inCbsDate_val() {
	var value = $('#inCbsDate').val();
	clearError('inCbsDate_error');	
	if (isEmpty(value)) {
		setError('inCbsDate_error', MANDATORY);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('BRANCH_CODE', $('#entityOffCode').val());
		validator.setValue('CBS_DATE', $('#inCbsDate').val());
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.cmn.ibrncbscontrolbean');
		validator.setMethod('validateInCbsDate');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('inCbsDate_error', validator.getValue(ERROR));
			return false;
		}
	}
	setFocusLast('transDate');
	return true;
}

function transDate_val() {
	var value = $('#transDate').val();
	var cbsDate = $('#inCbsDate').val();
	var cbd = getCBD();
	clearError('transDate_error');
	if (isEmpty(value)) {
		setError('transDate_error', MANDATORY);
		return false;
	}
	if (!isDate(value)) {
		setError('transDate_error', HMS_INVALID_DATE);
		return false;
	}
	if (isDateLesser(value, cbsDate)) {
		setError('transDate_error', HMS_DATE_GR_CBD);
		return false;
	}
	if (isDateGreater(value, cbd)) {
		setError('transDate_error', HMS_DATE_LCBD);
		return false;
	}
	setFocusLast('currCbsLink');
	return true;
}

function currCbsLink_val() {
	var value = $('#currCbsLink').val();
	clearError('currCbsLink_error');
	if (isEmpty(value)) {
		setError('currCbsLink_error', MANDATORY);
		return false;
	}
	if ($('#action').val() == ADD)
		setFocusLast('remarks');
	else
		setFocusLast('enabled');
	return true;
}

function enabled_val() {
	if ($('#action').val() == MODIFY) {
		setFocusLast('remarks');
	}
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', INVALID_REMARKS);
			setFocusLast('remarks');
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', INVALID_REMARKS);
			setFocusLast('remarks');
			return false;
		}
	}
	if ($('#action').val() == MODIFY) {
		if (isEmpty(value)) {
			setError('remarks_error', MANDATORY);
			setFocusLast('remarks');
			return false;
		}
	}
	setFocusOnSubmit();
	return true;
}
