<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.cmn.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/cmn/qassetfields.js" />
	</web:dependencies>
	<web:loadPK />
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="massetfields.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:viewContent id="po_view1">
				<web:dividerBlock>
					<web:sectionBlock width="width-100p" align="left">
									<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="massetfields.fieldid" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:fieldIdDisplay property="fieldId" id="fieldId" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="massetfields.name" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:descriptionDisplay property="name" id="name" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="massetfields.shortname" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:conciseDescriptionDisplay property="shortName" id="shortName"  />
										</web:column>
									</web:rowEven>
									
									<web:rowOdd>
										<web:column>
										<web:legend key="massetfields.fieldfor"
											var="program" mandatory="false" />
									</web:column>
									<web:column>
										<type:comboDisplay property="fieldFor"
											id="fieldFor"
											datasourceid="MASSETFIELDS_FIELD_FOR" />
									</web:column>
									</web:rowOdd>
									
									<web:rowEven>
										<web:column>
										<web:legend key="massetfields.fieldvaluetype"
											var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:comboDisplay property="fieldValueType"
											id="fieldValueType"
											datasourceid="MASSETFIELDS_FIELD_VALUE_TYPE" />
									</web:column>
									</web:rowEven>
									<web:rowOdd>
									<web:column>
											<web:legend key="massetfields.fieldsize" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:threeDigitDisplay property="fieldSize" id="fieldSize" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="massetfields.nooffractiondigits" var="program" mandatory="false" />
										</web:column>
										<web:column>
											<type:twoDigitDisplay property="noOfFractionDigits" id="noOfFractionDigits" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="massetfields.fielddedicatedtocustomerid" var="program" mandatory="false" />
										</web:column>
										<web:column>
											<type:customerIDDisplay property="fieldDedicatedToCustomerId" id="fieldDedicatedToCustomerId" />
										</web:column>
										</web:rowOdd>
										
										<web:rowEven>
										<web:column>
											<web:legend key="massetfields.fieldassettype" var="program" mandatory="false" />
										</web:column>
										<web:column>
											<type:assetTypeCodeDisplay property="fieldAssetType" id="fieldAssetType" />
										</web:column>
										</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="form.enabled" var="common" />
										</web:column>
										<web:column>
											<type:checkboxDisplay property="enabled" id="enabled" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="form.remarks" var="common" />
										</web:column>
										<web:column>
											<type:remarksDisplay property="remarks" id="remarks" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
						<web:auditDisplay />
					</web:sectionBlock>
				</web:dividerBlock>
			</web:viewContent>
		</web:viewSubPart>
	</web:viewPart>
</web:fragment>
