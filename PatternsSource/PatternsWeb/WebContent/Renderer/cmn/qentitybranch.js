var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MENTITYBRANCH';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}

function clearFields() {
	$('#entityBranchCode').val(EMPTY_STRING);
	$('#description').val(EMPTY_STRING);
	$('#conciseDescription').val(EMPTY_STRING);
	$('#officeOpenDate').val(EMPTY_STRING);
	$('#branchDetails').val(EMPTY_STRING);
	$('#geoUnitStateCode').val(EMPTY_STRING);
	$('#geoUnitStateCode_desc').html(EMPTY_STRING);
	$('#organizationUnit').val(EMPTY_STRING);
	$('#organizationUnit_desc').html(EMPTY_STRING);
	$('#controlBranchOffice').val(EMPTY_STRING);
	$('#controlBranchOffice_desc').html(EMPTY_STRING);
	$('#clusterCode').val(EMPTY_STRING);
	$('#clusterCode_desc').html(EMPTY_STRING);
	$('#officeCloseDate').val(EMPTY_STRING);
	setCheckbox('enabled', NO);
	$('#remarks').val(EMPTY_STRING);
}

function loadData() {
	$('#entityCode').val(getEntityCode());
	$('#entityCode_desc').html(getEntityName());
	$('#entityBranchCode').val(validator.getValue('BRANCH_CODE'));
	$('#description').val(validator.getValue('DESCRIPTION'));
	$('#conciseDescription').val(validator.getValue('CONCISE_DESCRIPTION'));
	$('#officeOpenDate').val(validator.getValue('OPEN_ON_DATE'));
	$('#branchDetails').val(validator.getValue('BRANCH_DETAILS'));
	$('#geoUnitStateCode').val(validator.getValue('GEO_UNIT_STATE_CODE'));
	$('#geoUnitStateCode_desc').html(validator.getValue('F1_DESCRIPTION'));
	$('#organizationUnit').val(validator.getValue('TYPE_OF_ORG_UNIT'));
	$('#organizationUnit_desc').html(validator.getValue('F2_DESCRIPTION'));
	$('#controlBranchOffice').val(validator.getValue('CONTROLLING_OFFICE_CODE'));
	$('#controlBranchOffice_desc').html(validator.getValue('F4_DESCRIPTION'));
	$('#clusterCode').val(validator.getValue('CLUSTER_CODE'));
	$('#clusterCode_desc').html(validator.getValue('F3_DESCRIPTION'));
	$('#officeCloseDate').val(validator.getValue('CLOSED_ON_DATE'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadAuditFields(validator);
}
