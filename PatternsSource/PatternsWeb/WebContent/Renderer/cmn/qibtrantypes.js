var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MIBTRANTYPES';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}
function clearFields() {
	$('#interBankTranType').val(EMPTY_STRING);
	$('#description').val(EMPTY_STRING);
	$('#conciseDescription').val(EMPTY_STRING);
	$('#liability').selectedIndex = ZERO;
	setCheckbox('origDebitsAllowed', NO);
	setCheckbox('numberingReq', NO);
	setCheckbox('manualNosAllowed', NO);
	$('#remarks').val(EMPTY_STRING);
}

function loadData() {
	$('#interBankTranType').val(validator.getValue('INTER_BANK_TRAN_TYPES'));
	$('#description').val(validator.getValue('DESCRIPTION'));
	$('#conciseDescription').val(validator.getValue('CONCISE_DESCRIPTION'));
	$('#liability').val(validator.getValue('LIABILITY'));
	setCheckbox('origDebitsAllowed', validator.getValue('ORIGINATING_DEBITS_ALLOWED'));
	setCheckbox('numberingReq', validator.getValue('NUMBERING_REQUIRED'));
	setCheckbox('manualNosAllowed', validator.getValue('MANUAL_NUMBERS_ALLOWED'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadAuditFields(validator);
}