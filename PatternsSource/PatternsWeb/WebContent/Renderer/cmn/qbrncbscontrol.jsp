<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<web:bundle baseName="patterns.config.web.forms.cmn.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/cmn/qbrncbscontrol.js" />
	</web:dependencies>
	<web:loadPK />
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="qbrncbscontrol.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:viewContent id="po_view1">
				<web:dividerBlock>
	             <web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="220px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="ibrncbscontrol.entityoffcode" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:entitybranchCodeDisplay property="entityOffCode" id="entityOffCode"  />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="220px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="ibrncbscontrol.incbsdate" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:dateDisplay property="inCbsDate" id="inCbsDate"  />
										</web:column>
									</web:rowOdd>	
									<web:rowEven>
										<web:column>
											<web:legend key="ibrncbscontrol.transdate" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:dateDisplay property="transDate" id="transDate"  />
										</web:column>
									</web:rowEven>	
									<web:rowOdd>
										<web:column>
											<web:legend key="ibrncbscontrol.currcbslink" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:comboDisplay property="currCbsLink" id="currCbsLink" datasourceid="IBRNCBSCONTROL_CURRENT_CBS_LINK" />
										</web:column>
									</web:rowOdd>											
									<web:rowEven>
										<web:column>
											<web:legend key="form.enabled" var="common"/>
										</web:column>
										<web:column>
											<type:checkboxDisplay property="enabled" id="enabled"/>
										</web:column>
									</web:rowEven>									
									<web:rowOdd>
										<web:column>
											<web:legend key="form.remarks" var="common" />
										</web:column>
										<web:column>
											<type:remarksDisplay property="remarks" id="remarks" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
						<web:auditDisplay />
					</web:sectionBlock>
				</web:dividerBlock>
			</web:viewContent>
		</web:viewSubPart>
	</web:viewPart>
</web:fragment>