var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MGLHEAD';
function init() {
	refreshTBAGrid();
	refreshQueryGrid();
	if (_redisplay) {
		if ($('#action').val() == ADD) {
			$('#enabled').prop('disabled', true);
			setCheckbox('enabled', YES);
		}
		leasePdtCode_val();
		setFocusLast('glHeadCode');
	}
}

function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'MGLHEAD_MAIN', queryGrid);
}

function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}

function doHelp(id) {
	switch (id) {
	case 'glHeadCode':
		help('COMMON', 'HLP_GL_HEAD_CODE_M', $('#glHeadCode').val(), EMPTY_STRING, $('#glHeadCode'));
		break;
	case 'leasePdtCode':
		help('COMMON', 'HLP_LEASE_PROD_CODE', $('#leasePdtCode').val(), EMPTY_STRING, $('#leasePdtCode'));
		break;
	}
}

function clearFields() {
	$('#glHeadCode').val(EMPTY_STRING);
	$('#glHeadCode_desc').html(EMPTY_STRING);
	$('#glHeadCode_error').html(EMPTY_STRING);
	clearNonPKFields();
}

function clearNonPKFields() {
	$('#description').val(EMPTY_STRING);
	$('#description_error').html(EMPTY_STRING);
	$('#conciseDescription').val(EMPTY_STRING);
	$('#conciseDescription_error').html(EMPTY_STRING);
	$('#leasePdtCode').val(EMPTY_STRING);
	$('#leasePdtCode_desc').html(EMPTY_STRING);
	$('#leasePdtCode_error').html(EMPTY_STRING);
	setCheckbox('enabled', YES);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
}

function doclearfields(id) {
	switch (id) {
	case 'glHeadCode':
		if (isEmpty($('#glHeadCode').val())) {
			$('#glHeadCode_desc').html(EMPTY_STRING);
			$('#glHeadCode_error').html(EMPTY_STRING);
			clearNonPKFields();
			break;
		}
	}
}

function add() {
	$('#glHeadCode').focus();
	setCheckbox('enabled', YES);
	$('#enabled').prop('disabled', true);
}

function modify() {
	$('#glHeadCode').focus();
	if ($('#action').val() == MODIFY) {
		$('#enabled').prop('disabled', false);
	} else {
		$('#enabled').prop('disabled', true);
	}
}

function loadData() {
	$('#glHeadCode').val(validator.getValue('GL_HEAD_CODE'));
	$('#description').val(validator.getValue('DESCRIPTION'));
	$('#conciseDescription').val(validator.getValue('CONCISE_DESCRIPTION'));
	$('#leasePdtCode').val(validator.getValue('LEASE_PRODUCT_CODE'));
	$('#leasePdtCode_desc').html(validator.getValue('F1_DESCRIPTION'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	resetLoading();
}

function view(source, primaryKey) {
	hideParent('cmn/qglhead', source, primaryKey);
	resetLoading();
}

function validate(id) {
	switch (id) {
	case 'glHeadCode':
		glHeadCode_val();
		break;
	case 'description':
		description_val();
		break;
	case 'conciseDescription':
		conciseDescription_val();
		break;
	case 'leasePdtCode':
		leasePdtCode_val();
		break;
	case 'enabled':
		enabled_val();
		break;
	case 'remarks':
		remarks_val();
		break;
	}
}

function backtrack(id) {
	switch (id) {
	case 'glHeadCode':
		setFocusLast('glHeadCode');
		break;
	case 'description':
		setFocusLast('glHeadCode');
		break;
	case 'conciseDescription':
		setFocusLast('description');
		break;
	case 'leasePdtCode':
		setFocusLast('conciseDescription');
		break;
	case 'enabled':
		setFocusLast('leasePdtCode');
		break;
	case 'remarks':
		if ($('#action').val() == MODIFY) {
			setFocusLast('enabled');
		} else {
			setFocusLast('leasePdtCode');
		}
		break;
	}
}

function glHeadCode_val() {
	var value = $('#glHeadCode').val();
	clearError('glHeadCode_error');
	if (isEmpty(value)) {
		setError('glHeadCode_error', MANDATORY);
		return false;
	}
	if (!isValidCode(value)) {
		setError('glHeadCode_error', INVALID_FORMAT);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('GL_HEAD_CODE', value);
	validator.setValue(ACTION, $('#action').val());
	validator.setClass('patterns.config.web.forms.cmn.mglheadbean');
	validator.setMethod('validateGlHeadCode');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('glHeadCode_error', validator.getValue(ERROR));
		return false;
	}
	if ($('#action').val() == MODIFY) {
		PK_VALUE = getEntityCode() + PK_SEPERATOR + $('#glHeadCode').val();
		if (!loadPKValues(PK_VALUE, 'glHeadCode_error')) {
			return false;
		}
	}
	setFocusLast('description');
	return true;
}

function description_val() {
	var value = $('#description').val();
	clearError('description_error');
	if (isEmpty(value)) {
		setError('description_error', MANDATORY);
		return false;
	}
	if (!isValidDescription(value)) {
		setError('description_error', INVALID_DESCRIPTION);
		return false;
	}
	setFocusLast('conciseDescription');
	return true;
}

function conciseDescription_val() {
	var value = $('#conciseDescription').val();
	clearError('conciseDescription_error');
	if (isEmpty(value)) {
		setError('conciseDescription_error', MANDATORY);
		return false;
	}
	if (!isValidConciseDescription(value)) {
		setError('conciseDescription_error', INVALID_CONCISE_DESCRIPTION);
		return false;
	}
	setFocusLast('leasePdtCode');
	return true;
}

function leasePdtCode_val() {
	var value = $('#leasePdtCode').val();
	clearError('leasePdtCode_error');
	$('#leasePdtCode_desc').html(EMPTY_STRING);
	if (!isEmpty(value)) {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('LEASE_PRODUCT_CODE', value);
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.cmn.mglheadbean');
		validator.setMethod('validateLeaseProductCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('leasePdtCode_error', validator.getValue(ERROR));
			return false;
		}
		$('#leasePdtCode_desc').html(validator.getValue('DESCRIPTION'));
	}
	if ($('#action').val() == MODIFY)
		setFocusLast('enabled');
	else
		setFocusLast('remarks');
}

function enabled_val() {
	if ($('#action').val() == MODIFY) {
		setFocusLast('remarks');
		return true;
	}
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', INVALID_REMARKS);
			setFocusLast('remarks');
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', INVALID_REMARKS);
			setFocusLast('remarks');
			return false;
		}
	}
	if ($('#action').val() == MODIFY) {
		if (isEmpty(value)) {
			setError('remarks_error', MANDATORY);
			setFocusLast('remarks');
			return false;
		}
	}
	setFocusOnSubmit();
	return true;
}
