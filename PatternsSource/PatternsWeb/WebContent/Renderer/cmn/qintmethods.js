var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MINTMETHODS';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}
function clearFields() {
	$('#interestMethodCode').val(EMPTY_STRING);
	$('#interestMethodCode_desc').html(EMPTY_STRING);
	$('#description').val(EMPTY_STRING);
	$('#conciseDescription').val(EMPTY_STRING);
	$('#calculationFor').val(EMPTY_STRING);
	$('#intMethodChoice').val(EMPTY_STRING);
	setCheckbox('enabled', NO);
	$('#remarks').val(EMPTY_STRING);
}
function loadData() {
	$('#interestMethodCode').val(validator.getValue('INT_METHOD'));
	$('#description').val(validator.getValue('DESCRIPTION'));
	$('#conciseDescription').val(validator.getValue('CONCISE_DESCRIPTION'));
	$('#calculationFor').val(validator.getValue('CALC_FOR'));
	$('#intMethodChoice').val(validator.getValue('INT_METHOD_CHOICE'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadAuditFields(validator);
}
