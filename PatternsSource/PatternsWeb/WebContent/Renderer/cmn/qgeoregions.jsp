<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.cmn.Messages"
	var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/cmn/qgeoregions.js" />
		<script
			src="https://maps.googleapis.com/maps/api/js?key=AIzaSyByyFg6IWJ3Y7tTASIG7pzXyEGYTmdrJRk&libraries=drawing&callback=mapObject.initMap"
			async defer></script>
	</web:dependencies>
	<web:loadPK />
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="qgeoregions.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:viewContent id="po_view1">
				<web:dividerBlock>
					<web:sectionBlock width="width-100p" align="left">
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="170px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="mgeoregions.georegioncode" var="program"
											mandatory="true" />
									</web:column>
									<web:column>
										<type:geoRegionCodeDisplay property="geoRegionCode"
											id="geoRegionCode" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="170px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowOdd>
									<web:column>
										<web:legend key="form.description" var="common"
											mandatory="true" />
									</web:column>
									<web:column>
										<type:descriptionDisplay property="description"
											id="description" />
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>





						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="700px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<td style="vertical-align: top"><web:section>
											<web:table>
												<web:columnGroup>
													<web:columnStyle width="170px" />
													<web:columnStyle />
												</web:columnGroup>
												<web:rowEven>
													<web:column>
														<web:legend key="form.conciseDescription" var="common"
															mandatory="true" />
													</web:column>
													<web:column>
														<type:conciseDescriptionDisplay
															property="conciseDescription" id="conciseDescription" />
													</web:column>
												</web:rowEven>
												<web:rowOdd>
													<web:column>
														<web:legend key="mgeoregions.pincode" var="program"
															mandatory="true" />
													</web:column>
													<web:column>
														<type:geographicalUnitIdDisplay property="pinCode"
															id="pinCode" />
													</web:column>
												</web:rowOdd>
												<web:rowEven>
													<web:column>
														<web:legend key="mgeoregions.officecode" var="program"
															mandatory="true" />
													</web:column>
													<web:column>
														<type:branchDisplay property="officeCode" id="officeCode" />
													</web:column>
												</web:rowEven>
												<web:rowOdd>
													<web:column>
														<web:legend key="form.enabled" var="common" />
													</web:column>
													<web:column>
														<type:checkboxDisplay property="enabled" id="enabled" />
													</web:column>
												</web:rowOdd>
												<web:rowEven>
													<web:column>
														<web:legend key="form.remarks" var="common" />
													</web:column>
													<web:column>
														<type:remarksDisplay property="remarks" id="remarks" />
													</web:column>
												</web:rowEven>
											</web:table>
										</web:section>
									<td><web:column>
											<div id="map" style="height: 350px; width: 410px"></div>
										</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:auditDisplay />
					</web:sectionBlock>
				</web:dividerBlock>
			</web:viewContent>
		</web:viewSubPart>
	</web:viewPart>
</web:fragment>
