<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.cmn.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/cmn/qifsc.js" />
	</web:dependencies>
	<web:loadPK />
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="qifsc.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:viewContent id="po_view1">
				<web:dividerBlock>
					<web:sectionBlock width="width-100p" align="left">
								<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="mifsc.ifsccode" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:ifscCodeDisplay property="ifscCode" id="ifscCode" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="mifsc.bankname" var="program"
												mandatory="true" />
										</web:column>
										<web:column>
											<type:otherInformation50Display property="bankName" id="bankName" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="mifsc.branchname" var="program"
												mandatory="true" />
										</web:column>
										<web:column>
											<type:descriptionDisplay property="branchName" id="branchName" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="mifsc.address" var="program"
												mandatory="true" />
										</web:column>
										<web:column>
											<type:remarksDisplay property="address" id="address" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="mifsc.micr" var="program"
												mandatory="false" />
										</web:column>
										<web:column>
											<type:conciseDescriptionDisplay property="micr"
												id="micr" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="mifsc.city" var="program"
												mandatory="true" />
										</web:column>
										<web:column>
											<type:descriptionDisplay property="city"
												id="city" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="mifsc.statecode" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:stateCodeDisplay property="stateCode" id="stateCode" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="form.enabled" var="common" />
										</web:column>
										<web:column>
											<type:checkboxDisplay property="enabled" id="enabled" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="form.remarks" var="common" />
										</web:column>
										<web:column>
											<type:remarksDisplay property="remarks" id="remarks" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
						<web:auditDisplay />
					</web:sectionBlock>
				</web:dividerBlock>
			</web:viewContent>
		</web:viewSubPart>
	</web:viewPart>
</web:fragment>
