var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'ITAXSCHRATE';
var validator1 = new xmlHTTPValidator();
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
	taxschrate_innerGrid.setColumnHidden(0, true);
}

function loads() {
	loadGridQuery(CURRENT_PROGRAM_ID, 'ITAXSCHRATE_GRID', taxschrate_innerGrid);
}

function clearFields() {
	$('#stateCode').val(EMPTY_STRING);
	$('#stateCode_desc').html(EMPTY_STRING);
	$('#taxCode').val(EMPTY_STRING);
	$('#taxCode_desc').html(EMPTY_STRING);
	$('#effectiveDate').val(EMPTY_STRING);
	$('#taxOnAmtPortion').val(EMPTY_STRING);
	$('#taxOnAmtPortionDesc').html(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	taxschrate_innerGrid.clearAll();
}

function loadData() {
	var taxOn = null;
	$('#stateCode').val(validator.getValue('STATE_CODE'));
	$('#stateCode_desc').html(validator.getValue('F1_DESCRIPTION'));
	$('#taxCode').val(validator.getValue('TAX_CODE'));
	$('#taxCode_desc').html(validator.getValue('F2_DESCRIPTION'));
	$('#effectiveDate').val(validator.getValue('EFF_DATE'));
	$('#taxOnAmtPortion').val(validator.getValue('TAX_ON_AMT_PORTION'));
	taxOn = validator.getValue('F2_TAX_ON');
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadGrid();
	loadAuditFields(validator);
	taxOnAmtPortionDescription_val(taxOn);
}

function taxOnAmtPortionDescription_val(taxon) {
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('TAX_ON', taxon);
	validator.setClass('patterns.config.web.forms.cmn.itaxschratebean');
	validator.setMethod('getTaxRelatedInfo');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('taxCode_error', validator1.getValue(ERROR));
		return false;
	}
	$('#taxOnAmtPortionDesc').html(ON_LABEL + validator.getValue("TAX_ON_DESC"));

}

function loadGrid() {
	SOURCE_VALUE = _tableType;
	PK_VALUE = _primaryKey;
	if (_tableType == MAIN) {
		loads();
	} else if (_tableType == TBA) {
		loads();
	}
}
