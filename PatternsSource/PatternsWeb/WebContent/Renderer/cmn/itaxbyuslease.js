var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'ITAXBYUSLEASE';
function init() {
	refreshTBAGrid();
	refreshQueryGrid();
	if (_redisplay) {

		if (!$('#taxBorne').is(':checked')) {
			if ($('#accountingHead_error').html().trim() != EMPTY_STRING)
				$('#accountingHead').prop('readonly', false);
			else
				$('#accountingHead').prop('readonly', false);

		} else {
			$('#accountingHead').prop('readonly', false);
		}

		if ($('#action').val() == ADD) {
			$('#enabled').prop('disabled', true);
			setCheckbox('enabled', YES);
		}
	}
}

function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'ITAXBYUSLEASE_MAIN', queryGrid);
}

function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}

function doHelp(id) {
	switch (id) {
	case 'lesseeCode':
		help('COMMON', 'HLP_DEBTOR_CODE', $('#lesseeCode').val(), EMPTY_STRING, $('#lesseeCode'), null, function(gridObj, rowID) {
			$('#lesseeCode').val(gridObj.cells(rowID, 0).getValue());
			$('#lesseeCode_desc').html(EMPTY_STRING);
			$('#lesseeCode_error').html(EMPTY_STRING);
			$('#customerId').val(gridObj.cells(rowID, 1).getValue());
			$('#customerName').val(gridObj.cells(rowID, 2).getValue());
		});
		break;

	case 'agreementNumber':
		help('COMMON', 'HLP_CUST_AGGREEMENT', $('#agreementNumber').val(), $('#customerId').val(), $('#agreementNumber'), null, function(gridObj, rowID) {
			$('#agreementNumber').val(gridObj.cells(rowID, 0).getValue());
			$('#agreementNumber_desc').html(EMPTY_STRING);
			$('#agreementNumber_error').html(EMPTY_STRING);
		});
		break;
	case 'scheduleId':
		help('COMMON', 'HLP_LEASE_SCH_ID', $('#scheduleId').val(), $('#lesseeCode').val() + PK_SEPERATOR + $('#agreementNumber').val(), $('#scheduleId'), null, function(gridObj, rowID) {
			$('#scheduleId').val(gridObj.cells(rowID, 0).getValue());
			$('#scheduleId_desc').html(EMPTY_STRING);
			$('#scheduleId_error').html(EMPTY_STRING);
		});
		break;
	case 'stateCode':
		help('COMMON', 'HLP_STATE_CODE', $('#stateCode').val(), EMPTY_STRING, $('#stateCode'));
		break;
	case 'taxCode':
		help('ITAXBYUSLEASE', 'HLP_TAX_CODE', $('#taxCode').val(), $('#stateCode').val(), $('#taxCode'));
		break;
	case 'effectiveDate':
		if ($('#action').val() == MODIFY) {
			help('ITAXBYUSLEASE', 'HLP_EFF_DATE', $('#effectiveDate').val(), $('#lesseeCode').val() + PK_SEPERATOR + $('#agreementNumber').val() + PK_SEPERATOR + $('#scheduleId').val() + PK_SEPERATOR + $('#stateCode').val() + PK_SEPERATOR + $('#taxCode').val(), $('#effectiveDate'));
		}
		break;
	case 'accountingHead':
		help('COMMON', 'HLP_GL_HEAD_CODE', $('#accountingHead').val(), EMPTY_STRING, $('#accountingHead'));
		break;
	}
}

function clearFields() {
	$('#lesseeCode').val(EMPTY_STRING);
	$('#lesseeCode_desc').html(EMPTY_STRING);
	$('#lesseeCode_error').html(EMPTY_STRING);
	$('#customerId').val(EMPTY_STRING);
	$('#customerName').val(EMPTY_STRING);
	$('#agreementNumber').val(EMPTY_STRING);
	$('#agreementNumber_desc').html(EMPTY_STRING);
	$('#agreementNumber_error').html(EMPTY_STRING);
	$('#scheduleId').val(EMPTY_STRING);
	$('#scheduleId_desc').html(EMPTY_STRING);
	$('#scheduleId_error').html(EMPTY_STRING);
	$('#stateCode').val(EMPTY_STRING);
	$('#stateCode_desc').html(EMPTY_STRING);
	$('#stateCode_error').html(EMPTY_STRING);
	$('#taxCode').val(EMPTY_STRING);
	$('#taxCode_desc').html(EMPTY_STRING);
	$('#taxCode_error').html(EMPTY_STRING);
	$('#effectiveDate').val(EMPTY_STRING);
	$('#effectiveDate_desc').html(EMPTY_STRING);
	$('#effectiveDate_error').html(EMPTY_STRING);
	clearNonPKFields();
}

function clearNonPKFields() {
	setCheckbox('enabled', YES);
	setCheckbox('taxBorne', NO);
	$('#accountingHead').val(EMPTY_STRING);
	$('#accountingHead_desc').html(EMPTY_STRING);
	$('#accountingHead_error').html(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
}

function doclearfields(id) {
	switch (id) {
	case 'lesseeCode':
		if (isEmpty($('#lesseeCode').val())) {
			clearFields();
			break;
		}
	case 'agreementNumber':
		if (isEmpty($('#agreementNumber').val())) {
			$('#agreementNumber').val(EMPTY_STRING);
			$('#agreementNumber_desc').html(EMPTY_STRING);
			$('#agreementNumber_error').html(EMPTY_STRING);
			$('#scheduleId').val(EMPTY_STRING);
			$('#scheduleId_desc').html(EMPTY_STRING);
			$('#scheduleId_error').html(EMPTY_STRING);
			$('#stateCode').val(EMPTY_STRING);
			$('#stateCode_desc').html(EMPTY_STRING);
			$('#stateCode_error').html(EMPTY_STRING);
			$('#stateCode').val(EMPTY_STRING);
			$('#stateCode_desc').html(EMPTY_STRING);
			$('#stateCode_error').html(EMPTY_STRING);
			$('#effectiveDate').val(EMPTY_STRING);
			$('#effectiveDate_desc').html(EMPTY_STRING);
			$('#effectiveDate_error').html(EMPTY_STRING);
			clearNonPKFields();
			break;
		}
	case 'scheduleId':
		if (isEmpty($('#scheduleId').val())) {
			$('#stateCode').val(EMPTY_STRING);
			$('#stateCode_desc').html(EMPTY_STRING);
			$('#stateCode_error').html(EMPTY_STRING);
			$('#taxCode').val(EMPTY_STRING);
			$('#taxCode_desc').html(EMPTY_STRING);
			$('#taxCode_error').html(EMPTY_STRING);
			$('#effectiveDate').val(EMPTY_STRING);
			$('#effectiveDate_desc').html(EMPTY_STRING);
			$('#effectiveDate_error').html(EMPTY_STRING);
			clearNonPKFields();
			break;
		}
	case 'stateCode':
		if (isEmpty($('#stateCode').val())) {
			$('#stateCode').val(EMPTY_STRING);
			$('#stateCode_desc').html(EMPTY_STRING);
			$('#stateCode_error').html(EMPTY_STRING);
			$('#taxCode').val(EMPTY_STRING);
			$('#taxCode_desc').html(EMPTY_STRING);
			$('#taxCode_error').html(EMPTY_STRING);
			$('#effectiveDate').val(EMPTY_STRING);
			$('#effectiveDate_desc').html(EMPTY_STRING);
			$('#effectiveDate_error').html(EMPTY_STRING);
			clearNonPKFields();
			break;
		}
	case 'taxCode':
		if (isEmpty($('#taxCode').val())) {
			$('#taxCode').val(EMPTY_STRING);
			$('#taxCode_desc').html(EMPTY_STRING);
			$('#taxCode_error').html(EMPTY_STRING);
			$('#effectiveDate').val(EMPTY_STRING);
			$('#effectiveDate_desc').html(EMPTY_STRING);
			$('#effectiveDate_error').html(EMPTY_STRING);
			clearNonPKFields();
			break;
		}
	case 'effectiveDate':
		if (isEmpty($('#effectiveDate').val())) {
			$('#effectiveDate').val(EMPTY_STRING);
			$('#effectiveDate_desc').html(EMPTY_STRING);
			$('#effectiveDate_error').html(EMPTY_STRING);
			clearNonPKFields();
			break;
		}
	}
}

function add() {
	$('#lesseeCode').focus();
	if ($('#taxBorne').is(':checked')) {
		$('#accountingHead').attr('readOnly', false);
	} else {
		$('#accountingHead').prop('readOnly', true);
	}
	setCheckbox('enabled', YES);
	$('#enabled').prop('disabled', true);
}

function modify() {
	$('#lesseeCode').focus();
	if ($('#action').val() == MODIFY) {
		$('#enabled').prop('disabled', false);
	} else {
		$('#enabled').prop('disabled', true);
	}
}

function loadData() {
	$('#lesseeCode').val(validator.getValue('LESSEE_CODE'));
	$('#customerId').val(validator.getValue('F4_CUSTOMER_ID'));
	$('#customerName').val(validator.getValue('F5_CUSTOMER_NAME'));
	$('#agreementNumber').val(validator.getValue('AGREEMENT_NO'));
	$('#scheduleId').val(validator.getValue('SCHEDULE_ID'));
	$('#stateCode').val(validator.getValue('STATE_CODE'));
	$('#stateCode_desc').html(validator.getValue('F1_DESCRIPTION'));
	$('#taxCode').val(validator.getValue('TAX_CODE'));
	$('#taxCode_desc').html(validator.getValue('F2_DESCRIPTION'));
	$('#effectiveDate').val(validator.getValue('EFF_DATE'));
	setCheckbox('taxBorne', validator.getValue('TAX_BY_US'));
	$('#accountingHead').val(validator.getValue('TAX_BYUS_GL_HEAD_CODE'));
	$('#accountingHead_desc').html(validator.getValue('F3_DESCRIPTION'));
	if ($('#taxBorne').is(':checked')) {
		$('#accountingHead').attr('readOnly', false);
	} else {
		$('#accountingHead').prop('readOnly', true);
	}
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	resetLoading();
}

function view(source, primaryKey) {
	hideParent('cmn/qtaxbyuslease', source, primaryKey, 1020);
	resetLoading();
}

function validate(id) {
	switch (id) {
	case 'lesseeCode':
		lesseeCode_val();
		break;
	case 'agreementNumber':
		agreementNumber_val();
		break;
	case 'scheduleId':
		scheduleId_val();
		break;
	case 'stateCode':
		stateCode_val();
		break;
	case 'taxCode':
		taxCode_val();
		break;
	case 'effectiveDate':
		effectiveDate_val();
		break;
	case 'taxBorne':
		taxBorne_val();
		break;
	case 'accountingHead':
		accountingHead_val();
		break;
	case 'enabled':
		enabled_val();
		break;
	case 'remarks':
		remarks_val();
		break;
	}
}

function backtrack(id) {
	switch (id) {
	case 'lesseeCode':
		setFocusLast('lesseeCode');
		break;
	case 'agreementNumber':
		setFocusLast('lesseeCode');
		break;
	case 'scheduleId':
		setFocusLast('agreementNumber');
		break;
	case 'stateCode':
		setFocusLast('scheduleId');
		break;
	case 'taxCode':
		setFocusLast('stateCode');
		break;
	case 'effectiveDate':
		setFocusLast('taxCode');
		break;
	case 'taxBorne':
		setFocusLast('effectiveDate');
		break;
	case 'accountingHead':
		setFocusLast('taxBorne');
		break;
	case 'enabled':
		if ($('#taxBorne').is(':checked'))
			setFocusLast('accountingHead');
		else
			setFocusLast('taxBorne');
		break;

	case 'remarks':
		if ($('#taxBorne').is(':checked')) {
			setFocusLast('accountingHead');
		} else {
			setFocusLast('taxBorne');
			return true;
		}
		if ($('#action').val() == MODIFY) {
			setFocusLast('enabled');
		} else {
			setFocusLast('accountingHead');
		}
		break;
	}
}

function lesseeCode_val() {
	var value = $('#lesseeCode').val();
	clearError('lesseeCode_error');
	$('#lesseeCode_desc').html(EMPTY_STRING);
	if (isEmpty(value)) {
		setError('lesseeCode_error', MANDATORY);
		return false;
	}
	if (!isValidCode(value)) {
		setError('lesseeCode_error', INVALID_FORMAT);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('SUNDRY_DB_AC', value);
	validator.setValue(ACTION, USAGE);
	validator.setClass('patterns.config.web.forms.cmn.itaxbyusleasebean');
	validator.setMethod('validateCustLeaseCode');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('lesseeCode_error', validator.getValue(ERROR));
		return false;
	} else if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		$('#customerId').val(validator.getValue('CUSTOMER_ID'));
		$('#customerName').val(validator.getValue('CUSTOMER_NAME'));
	}
	setFocusLast('agreementNumber');
	return true;
}

function agreementNumber_val() {
	var value = $('#agreementNumber').val();
	clearError('agreementNumber_error');
	$('#agreementNumber_desc').html(EMPTY_STRING);
	if (isEmpty(value)) {
		setError('agreementNumber_error', MANDATORY);
		return false;
	}
	if (!isValidCode(value)) {
		setError('agreementNumber_error', INVALID_FORMAT);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('SUNDRY_DB_AC', $('#lesseCode').val());
	validator.setValue('CUSTOMER_ID', $('#customerId').val());
	validator.setValue('AGREEMENT_NO', value);
	validator.setValue(ACTION, USAGE);
	validator.setClass('patterns.config.web.forms.cmn.itaxbyusleasebean');
	validator.setMethod('validateAgreementNo');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('agreementNumber_error', validator.getValue(ERROR));
		return false;
	}
	setFocusLast('scheduleId');
	return true;
}

function scheduleId_val() {
	var value = $('#scheduleId').val();
	clearError('scheduleId_error');
	$('#scheduleId_desc').html(EMPTY_STRING);
	if (isEmpty(value)) {
		setError('scheduleId_error', MANDATORY);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('LESSEE_CODE', $('#lesseeCode').val());
	validator.setValue('AGREEMENT_NO', $('#agreementNumber').val());
	validator.setValue('SCHEDULE_ID', value);
	validator.setValue(ACTION, USAGE);
	validator.setClass('patterns.config.web.forms.cmn.itaxbyusleasebean');
	validator.setMethod('validateScheduleID');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('scheduleId_error', validator.getValue(ERROR));
		return false;
	}
	setFocusLast('stateCode');
	return true;
}

function stateCode_val() {
	var value = $('#stateCode').val();
	clearError('stateCode_error');
	$('#stateCode_desc').html(EMPTY_STRING);
	if (isEmpty(value)) {
		setError('stateCode_error', MANDATORY);
		return false;
	}
	if (!isValidCode(value)) {
		setError('stateCode_error', INVALID_FORMAT);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('SUNDRY_DB_AC', $('#lesseeCode').val());
	validator.setValue('AGREEMENT_NO', $('#agreementNo').val());
	validator.setValue('SCHEDULE_ID', $('#scheduleId').val());
	validator.setValue('STATE_CODE', value);
	validator.setValue(ACTION, $('#action').val());
	validator.setClass('patterns.config.web.forms.cmn.itaxbyusleasebean');
	validator.setMethod('validateStateCode');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('stateCode_error', validator.getValue(ERROR));
		return false;
	} else if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		$('#stateCode_desc').html(validator.getValue('DESCRIPTION'));

	}
	setFocusLast('taxCode');
	return true;
}

function taxCode_val() {
	var value = $('#taxCode').val();
	clearError('taxCode_error');
	$('#taxCode_desc').html(EMPTY_STRING);
	if (isEmpty(value)) {
		setError('taxCode_error', MANDATORY);
		return false;
	}
	if (!isValidCode(value)) {
		setError('taxCode_error', INVALID_FORMAT);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('SUNDRY_DB_AC', $('#lesseeCode').val());
		validator.setValue('AGREEMENT_NO', $('#agreementNo').val());
		validator.setValue('SCHEDULE_ID', $('#scheduleId').val());
		validator.setValue('STATE_CODE', $('#stateCode').val());
		validator.setValue('TAX_CODE', value);
		validator.setValue(ACTION, $('#action').val());
		validator.setClass('patterns.config.web.forms.cmn.itaxbyusleasebean');
		validator.setMethod('validateTaxCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('taxCode_error', validator.getValue(ERROR));
			return false;
		} else if (validator.getValue(RESULT) == DATA_AVAILABLE) {
			$('#taxCode_desc').html(validator.getValue('DESCRIPTION'));

		}
		setFocusLast('effectiveDate');
		return true;
	}
}
function effectiveDate_val() {
	var value = $('#effectiveDate').val();
	clearError('effectiveDate_error');
	if (isEmpty(value)) {
		$('#effectiveDate').val(getCBD());
		value = $('#effectiveDate').val();
	}
	if (!isValidEffectiveDate(value, 'effectiveDate_error')) {
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('SUNDRY_DB_AC', $('#lesseeCode').val());
		validator.setValue('AGREEMENT_NO', $('#agreementNumber').val());
		validator.setValue('SCHEDULE_ID', $('#scheduleId').val());
		validator.setValue('STATE_CODE', $('#stateCode').val());
		validator.setValue('TAX_CODE', $('#taxCode').val());
		validator.setValue('EFF_DATE', $('#effectiveDate').val());
		validator.setValue(ACTION, $('#action').val());
		validator.setClass('patterns.config.web.forms.cmn.itaxbyusleasebean');
		validator.setMethod('validateEffectiveDate');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('effectiveDate_error', validator.getValue(ERROR));
			return false;
		} else {
			if ($('#action').val() == MODIFY) {
				PK_VALUE = getEntityCode() + PK_SEPERATOR + $('#lesseeCode').val() + PK_SEPERATOR + $('#agreementNumber').val() + PK_SEPERATOR + $('#scheduleId').val() + PK_SEPERATOR + $('#stateCode').val() + PK_SEPERATOR + $('#taxCode').val() + PK_SEPERATOR + $('#effectiveDate').val();
				if (!loadPKValues(PK_VALUE, 'effectiveDate_error')) {
					return false;
				}

			}
		}
	}
	setFocusLast('taxBorne');
	return true;
}

function taxBorne_val() {
	clearError('taxBorne_error');
	if ($('#taxBorne').is(':checked')) {
		$('#accountingHead').attr('readonly', false);
		setFocusLast('accountingHead');
		return true;
	} else
		$('#accountingHead').prop('readonly', true);
	$('#accountingHead').val(EMPTY_STRING);
	$('#accountingHead_desc').html(EMPTY_STRING);
	$('#accountingHead_error').html(EMPTY_STRING);
	setFocusLast('remarks');
	return true;
}

function accountingHead_val() {
	var value = $('#accountingHead').val();
	clearError('accountingHead_error');
	$('#accountingHead_desc').html(EMPTY_STRING);
	if (isEmpty(value)) {
		setError('accountingHead_error', MANDATORY);
		return false;
	}
	if (!isValidCode(value)) {
		setError('accountingHead_error', INVALID_FORMAT);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('GL_HEAD_CODE', value);
	validator.setValue(ACTION, USAGE);
	validator.setClass('patterns.config.web.forms.cmn.itaxbyusleasebean');
	validator.setMethod('validateGlHeadCode');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('accountingHead_error', validator.getValue(ERROR));
		return false;
	} else if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		$('#accountingHead_desc').html(validator.getValue('DESCRIPTION'));

	}
	setFocusLast('remarks');
	return true;
}

function enabled_val() {
	if ($('#action').val() == MODIFY) {
		setFocusLast('remarks');
	}
	return true;
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', INVALID_REMARKS);
			setFocusLast('remarks');
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', INVALID_REMARKS);
			setFocusLast('remarks');
			return false;
		}
	}
	if ($('#action').val() == MODIFY) {
		if (isEmpty(value)) {
			setError('remarks_error', MANDATORY);
			setFocusLast('remarks');
			return false;
		}
	}
	setFocusOnSubmit();
	return true;
}
