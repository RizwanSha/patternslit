<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.cmn.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/cmn/qstate.js" />
	</web:dependencies>
	<web:loadPK />
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="qstate.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:viewContent id="po_view1">
				<web:dividerBlock>
					<web:sectionBlock width="width-100p" align="left">
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="mstate.statecode" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:stateCodeDisplay property="stateCode" id="stateCode" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowOdd>
									<web:column>
										<web:legend key="mstate.name" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:descriptionDisplay property="name" id="name" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="mstate.shortname" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:conciseDescriptionDisplay property="shortName" id="shortName" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="mstate.stateorunionterritory" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:comboDisplay property="stateOrUnionTerritory" id="stateOrUnionTerritory" datasourceid="MSTATE_STATE_OR_TERR" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="mstate.oldstatecodesplitondate" var="program" mandatory="false" />
									</web:column>
									<web:column>
										<type:dateDisplay property="oldStateCodeSplitOnDate" id="oldStateCodeSplitOnDate" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="mstate.newstateformationdate" var="program" mandatory="false" />
									</web:column>
									<web:column>
										<type:dateDisplay property="newStateFormationDate" id="newStateFormationDate" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="mstate.invoicetype" var="program" />
									</web:column>
									<web:column>
										<type:comboDisplay property="invoiceType" id="invoiceType" datasourceid="COMMON_INVOICE_TYPE" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="form.enabled" var="common" />
									</web:column>
									<web:column>
										<type:checkboxDisplay property="enabled" id="enabled" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="form.remarks" var="common" />
									</web:column>
									<web:column>
										<type:remarksDisplay property="remarks" id="remarks" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:auditDisplay />
					</web:sectionBlock>
				</web:dividerBlock>
			</web:viewContent>
		</web:viewSubPart>
	</web:viewPart>
</web:fragment>
