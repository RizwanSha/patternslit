var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'IPRODCPCCONFIG';

function init() {
	refreshTBAGrid();
	refreshQueryGrid();
	if (_redisplay) {
		if ($('#action').val() == MODIFY) {
			$('#enabled').prop('disabled', false);
		} else {
			setCheckbox('enabled', YES);
			$('#enabled').prop('disabled', true);
		}
		if (!isEmpty($('#remittanceIFSC').val())) {
			remittanceIFSC_val();
		}
	}
}

function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'IPRODCPCCONFIG_MAIN', queryGrid);
}

function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}
function doclearfields(id) {
	switch (id) {
	case 'productCode':
		if (isEmpty($('#productCode').val())) {
			$('#portfolioCode').val(EMPTY_STRING);
			$('#portfolioCode_error').html(EMPTY_STRING);
			$('#portfolioCode_desc').html(EMPTY_STRING);
			$('#effectiveDate').val(EMPTY_STRING);
			$('#effectiveDate_error').html(EMPTY_STRING);
			$('#effectiveDate').val(getCBD());
			clearNonPKFields();
			break;
		}
	case 'portfolioCode':
		if (isEmpty($('#portfolioCode').val())) {
			$('#effectiveDate').val(EMPTY_STRING);
			$('#effectiveDate_error').html(EMPTY_STRING);
			$('#effectiveDate').val(getCBD());
			clearNonPKFields();
			break;
		}
	case 'effectiveDate':
		if (isEmpty($('#effectiveDate').val())) {
			clearNonPKFields();
			break;
		}
	}
}
function clearFields() {
	$('#productCode').val(EMPTY_STRING);
	$('#productCode_error').html(EMPTY_STRING);
	$('#productCode_desc').html(EMPTY_STRING);
	$('#portfolioCode').val(EMPTY_STRING);
	$('#portfolioCode_error').html(EMPTY_STRING);
	$('#portfolioCode_desc').html(EMPTY_STRING);
	$('#effectiveDate').val(EMPTY_STRING);
	$('#effectiveDate_error').html(EMPTY_STRING);
	$('#effectiveDate').val(getCBD());
	clearNonPKFields();
}
function clearNonPKFields() {
	$('#costCenterCode').val(EMPTY_STRING);
	$('#costCenterCode_error').html(EMPTY_STRING);
	$('#costCenterCode_desc').html(EMPTY_STRING);
	$('#profitCenterCode').val(EMPTY_STRING);
	$('#profitCenterCode_error').html(EMPTY_STRING);
	$('#profitCenterCode_desc').html(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
}

function loadData() {
	$('#productCode').val(validator.getValue('LEASE_PRODUCT_CODE'));
	$('#productCode_desc').html(validator.getValue('F1_DESCRIPTION'));
	$('#portfolioCode').val(validator.getValue('PORTFOLIO_CODE'));
	$('#portfolioCode_desc').html(validator.getValue('F2_DESCRIPTION'));
	$('#effectiveDate').val(validator.getValue('EFF_DATE'));
	$('#costCenterCode').val(validator.getValue('COST_CP_CENTER_CODE'));
	$('#costCenterCode_desc').html(validator.getValue('F3_DESCRIPTION'));
	$('#profitCenterCode').val(validator.getValue('PROFIT_CP_CENTER_CODE'));
	$('#profitCenterCode_desc').html(validator.getValue('F4_DESCRIPTION'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	resetLoading();
}

function view(source, primaryKey) {
	hideParent('cmn/qprodcpcconfig', source, primaryKey);
	resetLoading();
}

function doHelp(id) {
	switch (id) {
	case 'productCode':
		help('COMMON', 'HLP_LEASE_PROD_CODE', $('#productCode').val(),
				EMPTY_STRING, $('#productCode'));
		break;
	case 'portfolioCode':
		help('COMMON', 'HLP_PORTFOLIO_CODE', $('#portfolioCode').val(),
				EMPTY_STRING, $('#portfolioCode'));
		break;
	case 'effectiveDate':
		if (!isEmpty($('#productCode').val())) {
			if (!isEmpty($('#portfolioCode').val())) {
				help(CURRENT_PROGRAM_ID, 'HLP_EFF_DATE', $('#effectiveDate')
						.val(), $('#productCode').val() + PK_SEPERATOR
						+ $('#portfolioCode').val(), $('#effectiveDate'));
			} else {
				setError('portfolioCode_error', MANDATORY);
			}
		} else {
			setError('productCode_error', MANDATORY);
		}
		break;
	case 'costCenterCode':
		help('COMMON', 'HLP_CPC_COST_CODE', $('#costCenterCode').val(),
				EMPTY_STRING, $('#costCenterCode'));
		break;
	case 'profitCenterCode':
		help('COMMON', 'HLP_CPC_PROFIT_CODE', $('#profitCenterCode').val(),
				EMPTY_STRING, $('#profitCenterCode'));
		break;

	}
}
function add() {
	$('#productCode').focus();
	$('#effectiveDate').val(getCBD());
	setCheckbox('enabled', YES);
	$('#enabled').prop('disabled', true);
}

function modify() {
	$('#productCode').focus();
	if ($('#action').val() == MODIFY) {
		$('#effectiveDate').val(EMPTY_STRING);
		$('#enabled').prop('disabled', false);
	} else {
		$('#enabled').prop('disabled', true);
	}
}

function validate(id) {
	switch (id) {
	case 'productCode':
		productCode_val();
		break;
	case 'portfolioCode':
		portfolioCode_val();
		break;
	case 'effectiveDate':
		effectiveDate_val();
		break;
	case 'costCenterCode':
		costCenterCode_val();
		break;
	case 'profitCenterCode':
		profitCenterCode_val();
		break;
	case 'enabled':
		enabled_val();
		break;
	case 'remarks':
		remarks_val();
		break;
	}
}

function backtrack(id) {
	switch (id) {
	case 'productCode':
		setFocusLast('productCode');
		break;
	case 'portfolioCode':
		setFocusLast('productCode');
		break;
	case 'effectiveDate':
		setFocusLast('portfolioCode');
		break;
	case 'costCenterCode':
		setFocusLast('effectiveDate');
		break;
	case 'profitCenterCode':
		setFocusLast('costCenterCode');
		break;
	case 'enabled':
		setFocusLast('profitCenterCode');
		break;
	case 'remarks':
		if ($('#action').val() == MODIFY) {
			setFocusLast('enabled');
		} else {
			setFocusLast('profitCenterCode');
		}
		break;
	}
}
function productCode_val() {
	var value = $('#productCode').val();
	clearError('productCode_error');
	if (isEmpty(value)) {
		setError('productCode_error', MANDATORY);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('ENTITY_CODE', getEntityCode());
		validator.setValue('LEASE_PRODUCT_CODE', value);
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.cmn.iprodcpcconfigbean');
		validator.setMethod('validateProductCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('productCode_error', validator.getValue(ERROR));
			$('#productCode_desc').html(EMPTY_STRING);
			return false;
		}
		$('#productCode_desc').html(validator.getValue('DESCRIPTION'));
	}
	setFocusLast('portfolioCode');
	return true;
}
function portfolioCode_val() {
	var value = $('#portfolioCode').val();
	clearError('portfolioCode_error');
	if (isEmpty(value)) {
		setError('portfolioCode_error', MANDATORY);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('ENTITY_CODE', getEntityCode());
		validator.setValue('LEASE_PRODUCT_CODE', value);
		validator.setValue('PORTFOLIO_CODE', value);
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.cmn.iprodcpcconfigbean');
		validator.setMethod('validatePortfolioCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('portfolioCode_error', validator.getValue(ERROR));
			$('#portfolioCode_desc').html(EMPTY_STRING);
			return false;
		}
		$('#portfolioCode_desc').html(validator.getValue('DESCRIPTION'));
	}
	setFocusLast('effectiveDate');
	return true;
}
function effectiveDate_val() {
	var value = $('#effectiveDate').val();
	clearError('effectiveDate_error');
	if (isEmpty(value)) {
		$('#effectiveDate').val(getCBD());
		value = $('#effectiveDate').val();
	}
	if (!isValidEffectiveDate(value, 'effectiveDate_error')) {
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('ENTITY_CODE', getEntityCode());
		validator.setValue('LEASE_PRODUCT_CODE', $('#productCode').val());
		validator.setValue('PORTFOLIO_CODE', $('#portfolioCode').val());
		validator.setValue('EFF_DATE', value);
		validator.setValue(ACTION, $('#action').val());
		validator.setClass('patterns.config.web.forms.cmn.iprodcpcconfigbean');
		validator.setMethod('validateEffectiveDate');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('effectiveDate_error', validator.getValue(ERROR));
			return false;
		} else {
			if ($('#action').val() == MODIFY) {
				PK_VALUE = getEntityCode() + PK_SEPERATOR
						+ $('#productCode').val() + PK_SEPERATOR
						+ $('#portfolioCode').val() + PK_SEPERATOR
						+ $('#effectiveDate').val();
				if (!loadPKValues(PK_VALUE, 'effectiveDate_error')) {
					return false;
				}
			}
		}
		setFocusLast('costCenterCode');
		return true;
	}
}
function costCenterCode_val() {
	var value = $('#costCenterCode').val();
	clearError('costCenterCode_error');
	if (isEmpty(value)) {
		setError('costCenterCode_error', MANDATORY);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('ENTITY_CODE', getEntityCode());
		validator.setValue('CP_CENTER_CODE', value);
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.cmn.iprodcpcconfigbean');
		validator.setMethod('validateCostCenterCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('costCenterCode_error', validator.getValue(ERROR));
			$('#costCenterCode_desc').html(EMPTY_STRING);
			return false;
		} else {
			$('#costCenterCode_desc').html(validator.getValue("DESCRIPTION"));
		}
	}
	setFocusLast('profitCenterCode');
	return true;
}

function profitCenterCode_val() {
	var value = $('#profitCenterCode').val();
	clearError('profitCenterCode_error');
	if (isEmpty(value)) {
		setError('profitCenterCode_error', MANDATORY);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('ENTITY_CODE', getEntityCode());
		validator.setValue('CP_CENTER_CODE', value);
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.cmn.iprodcpcconfigbean');
		validator.setMethod('validateProfitCenterCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('profitCenterCode_error', validator.getValue(ERROR));
			$('#profitCenterCode_desc').html(EMPTY_STRING);
			return false;
		} else {
			$('#profitCenterCode_desc').html(validator.getValue("DESCRIPTION"));
		}
	}
	if ($('#action').val() == MODIFY) {
		setFocusLast('enabled');
	} else {
		setFocusLast('remarks');
	}
	return true;
}
function enabled_val() {
	if ($('#action').val() == MODIFY) {
		setFocusLast('remarks');
	}
	return true;
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', INVALID_REMARKS);
			setFocusLast('remarks');
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', INVALID_REMARKS);
			setFocusLast('remarks');
			return false;
		}
	}
	if ($('#action').val() == MODIFY) {
		if (isEmpty(value)) {
			setError('remarks_error', MANDATORY);
			setFocusLast('remarks');
			return false;
		}
	}
	setFocusOnSubmit();
	return true;
}
