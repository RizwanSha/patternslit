var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MTENORCODES';
function init() {
	refreshTBAGrid();
	refreshQueryGrid();
	if ($('#action').val() == ADD) {
		$('#enabled').prop('disabled', true);
		setCheckbox('enabled', YES);
	}
}

function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'MTENORCODES_MAIN', queryGrid);
}

function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}

function doclearfields(id) {
	switch (id) {
	case 'tenorSpecificationCode':
		if (isEmpty($('#tenorSpecificationCode').val())) {
			clearFields();
			break;
		}
	}
}

function clearFields() {
	$('#tenorSpecificationCode').val(EMPTY_STRING);
	$('#tenorSpecificationCode_error').html(EMPTY_STRING);
	$('#tenorSpecificationCode_desc').html(EMPTY_STRING);
	clearNonPKFields();
}

function clearNonPKFields() {
	$('#description').val(EMPTY_STRING);
	$('#description_error').html(EMPTY_STRING);
	$('#conciseDescription').val(EMPTY_STRING);
	$('#conciseDescription_error').html(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
}

function doHelp(id) {
	switch (id) {
	case 'tenorSpecificationCode':
		help('COMMON', 'HLP_TENOR_SPEC_CODE', $('#tenorSpecificationCode').val(), EMPTY_STRING, $('#tenorSpecificationCode'));
		break;
	}
}

function add() {
	$('#tenorSpecificationCode').focus();
	setCheckbox('enabled', YES);
	$('#enabled').prop('disabled', true);
}

function modify() {
	$('#tenorSpecificationCode').focus();
	if ($('#action').val() == MODIFY) {
		$('#enabled').prop('disabled', false);
	} else {
		$('#enabled').prop('disabled', true);
	}
}

function loadData() {
	$('#tenorSpecificationCode').val(validator.getValue('TENOR_SPEC_CODE'));
	$('#description').val(validator.getValue('DESCRIPTION'));
	$('#conciseDescription').val(validator.getValue('CONCISE_DESCRIPTION'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	resetLoading();
}

function view(source, primaryKey) {
	hideParent('cmn/qtenorcodes', source, primaryKey);
	resetLoading();
}

function validate(id) {
	switch (id) {
	case 'tenorSpecificationCode':
		tenorSpecificationCode_val();
		break;
	case 'description':
		description_val();
		break;
	case 'conciseDescription':
		conciseDescription_val();
		break;
	case 'enabled':
		setFocusLast('remarks');
		break;
	case 'remarks':
		remarks_val();
		break;
	}
}

function backtrack(id) {
	switch (id) {
	case 'tenorSpecificationCode':
		setFocusLast('tenorSpecificationCode');
		break;
	case 'description':
		setFocusLast('tenorSpecificationCode');
		break;
	case 'conciseDescription':
		setFocusLast('description');
		break;
	case 'enabled':
		setFocusLast('conciseDescription');
		break;
	case 'remarks':
		if ($('#action').val() == MODIFY)
			setFocusLast('enabled');
		else
			setFocusLast('conciseDescription');
		break;
	}
}

function tenorSpecificationCode_val() {
	var value = $('#tenorSpecificationCode').val();
	$('#tenorSpecificationCode_desc').html(EMPTY_STRING);
	clearError('tenorSpecificationCode_error');
	if (isEmpty(value)) {
		setError('tenorSpecificationCode_error', MANDATORY);
		return false;
	} else if (!isValidCode(value)) {
		setError('tenorSpecificationCode_error', INVALID_FORMAT);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('TENOR_SPEC_CODE', value);
		validator.setValue(ACTION, $('#action').val());
		validator.setClass('patterns.config.web.forms.cmn.mtenorcodesbean');
		validator.setMethod('validateTenorSpecificationCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('tenorSpecificationCode_error', validator.getValue(ERROR));
			clearNonPKFields();
			return false;
		} else if ($('#action').val() == MODIFY) {
			PK_VALUE = getEntityCode() + PK_SEPERATOR + value;
			if (!loadPKValues(PK_VALUE, 'tenorSpecificationCode_error')) {
				clearNonPKFields();
				return false;
			}
		}
	}
	setFocusLast('description');
	return true;
}

function description_val() {
	var value = $('#description').val();
	clearError('description_error');
	if (isEmpty(value)) {
		setError('description_error', MANDATORY);
		return false;
	}
	if (!isValidDescription(value)) {
		setError('description_error', INVALID_DESCRIPTION);
		return false;
	}
	setFocusLast('conciseDescription');
	return true;
}

function conciseDescription_val() {
	var value = $('#conciseDescription').val();
	clearError('conciseDescription_error');
	if (isEmpty(value)) {
		setError('conciseDescription_error', MANDATORY);
		return false;
	}
	if (!isValidConciseDescription(value)) {
		setError('conciseDescription_error', INVALID_CONCISE_DESCRIPTION);
		return false;
	}
	if ($('#action').val() == MODIFY)
		setFocusLast('enabled');
	else
		setFocusLast('remarks');
	return true;
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', INVALID_REMARKS);
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', INVALID_REMARKS);
			return false;
		}
	}
	if ($('#action').val() == MODIFY) {
		if (isEmpty(value)) {
			setError('remarks_error', MANDATORY);
			return false;
		}
	}
	setFocusOnSubmit();
	return true;
}