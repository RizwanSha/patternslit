var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MASSETFIELDS';
function init() {
	refreshTBAGrid();
	refreshQueryGrid();
	if (_redisplay) {
		if ($('#fieldFor').val() == 'L') {
			$('#fieldAssetType').prop('disabled', true);
		} else {
			$('#fieldAssetType').prop('disabled', false);
		}
		if ($('#fieldValueType').val() == 'T' || $('#fieldValueType').val() == 'N') {
			$('#noOfFractionDigits').prop('disabled', true);
		} else {
			$('#noOfFractionDigits').prop('disabled', false);
		}
		if ($('#action').val() == ADD) {
			$('#enabled').prop('disabled', true);
			setCheckbox('enabled', YES);
		}
		makeEditable();
	}
}

function makeEditable() {
	if ($('#noOfFractionDigits_error').html().trim() != EMPTY_STRING)
		$('#noOfFractionDigits').prop('disabled', false);
	if ($('#fieldAssetType_error').html().trim() != EMPTY_STRING)
		$('#fieldAssetType').prop('disabled', false);
	if ($('#fieldFor').val() == 'L') {
		if ($('#fieldSize_error').html().trim() != EMPTY_STRING)
			$('#fieldSize').prop('disabled', false);
		else
			$('#fieldSize').prop('disabled', true);
	} else {
		$('#fieldAssetType').prop('disabled', false);
	}
	if ($('#fieldValueType').val() == 'T' || $('#fieldValueType').val() == 'N') {
		$('#noOfFractionDigits').prop('disabled', true);
	} else {
		$('#noOfFractionDigits').prop('disabled', false);
	}
	if ($('#enabled').is(':checked'))
		setCheckbox('enabled', YES);
	else
		setCheckbox('enabled', NO);
}

function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'MASSETFIELDS_MAIN', queryGrid);
}

function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}

function clearFields() {
	$('#fieldId').val(EMPTY_STRING);
	$('#fieldId_desc').html(EMPTY_STRING);
	$('#fieldId_error').html(EMPTY_STRING);
	clearNonPKFields();
}

function clearNonPKFields() {
	$('#name').val(EMPTY_STRING);
	$('#name_error').html(EMPTY_STRING);
	$('#shortName').val(EMPTY_STRING);
	$('#shortName_error').html(EMPTY_STRING);
	$('#fieldFor').val($("#fieldFor option:first-child").val());
	$('#fieldFor_error').html(EMPTY_STRING);
	$('#fieldValueType').val($("#fieldValueType option:first-child").val());
	$('#fieldValueType_error').html(EMPTY_STRING);
	$('#fieldSize').val(EMPTY_STRING);
	$('#fieldSize_error').html(EMPTY_STRING);
	$('#noOfFractionDigits').val(EMPTY_STRING);
	$('#noOfFractionDigits_error').html(EMPTY_STRING);
	$('#fieldDedicatedToCustomerId').val(EMPTY_STRING);
	$('#fieldDedicatedToCustomerId_desc').html(EMPTY_STRING);
	$('#fieldDedicatedToCustomerId_error').html(EMPTY_STRING);
	$('#fieldAssetType').val(EMPTY_STRING);
	$('#fieldAssetType_desc').html(EMPTY_STRING);
	$('#fieldAssetType_error').html(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
}

function doclearfields(id) {
	switch (id) {
	case 'fieldId':
		if (isEmpty($('#fieldId').val())) {
			clearFields();
			break;
		}
	}
}

function add() {
	$('#fieldId').focus();
	$('#fieldId_pic').remove();
	setCheckbox('enabled', YES);
	$('#enabled').prop('disabled', true);
}

function modify() {
	$('#fieldId').focus();
	if ($('#action').val() == MODIFY) {
		$('#enabled').prop('disabled', false);
	} else {
		$('#enabled').prop('disabled', true);
	}
}

function view(source, primaryKey) {
	hideParent('cmn/qassetfields', source, primaryKey);
	resetLoading();
}

function loadData() {
	$('#fieldId').val(validator.getValue('FIELD_ID'));
	$('#name').val(validator.getValue('DESCRIPTION'));
	$('#shortName').val(validator.getValue('CONCISE_DESCRIPTION'));
	$('#fieldFor').val(validator.getValue('FIELD_FOR'));
	$('#fieldValueType').val(validator.getValue('FIELD_VALUE_TYPE'));
	$('#fieldSize').val(validator.getValue('FIELD_SIZE'));
	$('#noOfFractionDigits').val(validator.getValue('FIELD_DECIMALS'));
	if ($('#fieldValueType').val() == 'T' || $('#fieldValueType').val() == 'N') {
		$('#noOfFractionDigits').prop('disabled', true);
	} else {
		$('#noOfFractionDigits').prop('disabled', false);
	}
	$('#fieldDedicatedToCustomerId').val(validator.getValue('CUSTOMER_ID'));
	$('#fieldDedicatedToCustomerId_desc').html(validator.getValue('F1_CUSTOMER_NAME'));
	$('#fieldAssetType').val(validator.getValue('ASSET_TYPE_CODE'));
	$('#fieldAssetType_desc').html(validator.getValue('F2_DESCRIPTION'));
	if ($('#fieldFor').val() == 'L') {
		$('#fieldAssetType').prop('disabled', true);
	} else {
		$('#fieldAssetType').prop('disabled', false);
	}
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	setFocusLast('fieldId');
	resetLoading();
}

function backtrack(id) {
	switch (id) {
	case 'fieldId':
		setFocusLast('fieldId');
		break;
	case 'name':
		setFocusLast('fieldId');
		break;
	case 'shortName':
		setFocusLast('name');
		break;
	case 'fieldFor':
		setFocusLast('shortName');
		break;
	case 'fieldValueType':
		setFocusLast('fieldFor');
		break;
	case 'fieldSize':
		setFocusLast('fieldValueType');
		break;
	case 'noOfFractionDigits':
		setFocusLast('fieldSize');
		break;
	case 'fieldDedicatedToCustomerId':
		if ($('#fieldValueType').val() == 'N' || $('#fieldValueType').val() == 'T')
			setFocusLast('fieldSize');
		else
			setFocusLast('noOfFractionDigits');
		break;
	case 'fieldAssetType':
		setFocusLast('fieldDedicatedToCustomerId');
		break;
	case 'enabled':
		if ($('#fieldFor').val() == 'L') {
			setFocusLast('fieldDedicatedToCustomerId');
		} else {
			setFocusLast('fieldAssetType');
		}
		break;
	case 'remarks':
		if ($('#action').val() == MODIFY) {
			setFocusLast('enabled');
		} else if ($('#fieldFor').val() == 'L') {
			setFocusLast('fieldDedicatedToCustomerId');
		} else {
			setFocusLast('fieldAssetType');
		}
		break;
	}
}

function doHelp(id) {
	switch (id) {
	case 'fieldId':
		help('COMMON', 'HLP_FIELD_ID', $('#fieldId').val(), EMPTY_STRING, $('#fieldId'), null, function(gridObj, rowID) {
			$('#fieldId').val(gridObj.cells(rowID, 0).getValue());
			$('#fieldId_desc').html(EMPTY_STRING);
		});
		break;
	case 'fieldDedicatedToCustomerId':
		help('COMMON', 'HLP_CUSTOMER_ID', $('#fieldDedicatedToCustomerId').val(), EMPTY_STRING, $('#fieldDedicatedToCustomerId'));
		break;
	case 'fieldAssetType':
		help('COMMON', 'HLP_ASSET_TYPE', $('#fieldAssetType').val(), EMPTY_STRING, $('#fieldAssetType'));
		break;
	}
}

function change(id) {
	switch (id) {
	case 'fieldFor':
		fieldFor_val();
		break;
	case 'fieldValueType':
		fieldValueType_val();
		break;
	}
}

function validate(id) {
	switch (id) {
	case 'fieldId':
		fieldId_val();
		break;
	case 'name':
		name_val();
		break;
	case 'shortName':
		shortName_val();
		break;
	case 'fieldFor':
		fieldFor_val();
		break;
	case 'fieldValueType':
		fieldValueType_val();
		break;
	case 'fieldSize':
		fieldSize_val();
		break;
	case 'noOfFractionDigits':
		noOfFractionDigits_val();
		break;
	case 'fieldDedicatedToCustomerId':
		fieldDedicatedToCustomerId_val();
		break;
	case 'fieldAssetType':
		fieldAssetType_val();
		break;
	case 'enabled':
		enabled_val();
		break;
	case 'remarks':
		remarks_val();
		break;
	}
}

function fieldId_val() {
	var value = $('#fieldId').val();
	clearError('fieldId_error');
	if (isEmpty(value)) {
		setError('fieldId_error', PBS_MANDATORY);
		return false;
	} else if ($('#action').val() == ADD && !isValidCode(value)) {
		setError('fieldId_error', INVALID_FORMAT);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('FIELD_ID', $('#fieldId').val());
		validator.setValue(ACTION, $('#action').val());
		validator.setClass('patterns.config.web.forms.cmn.massetfieldsbean');
		validator.setMethod('validateFieldId');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('fieldId_error', validator.getValue(ERROR));
			return false;
		} else {
			if ($('#action').val() == MODIFY) {
				PK_VALUE = getEntityCode() + PK_SEPERATOR + $('#fieldId').val();
				if (!loadPKValues(PK_VALUE, 'fieldId_error')) {
					return false;
				}
				setFocusLast('name');
				return true;
			}
		}
	}
	setFocusLast('name');
	return true;
}

function name_val() {
	var value = $('#name').val();
	clearError('name_error');
	if (isEmpty(value)) {
		setError('name_error', MANDATORY);
		return false;
	}
	if (!isValidDescription(value)) {
		setError('name_error', INVALID_DESCRIPTION);
		return false;
	} else {

		setFocusLast('shortName');
	}
	return true;
}

function shortName_val() {
	var value = $('#shortName').val();
	clearError('shortName_error');
	if (isEmpty(value)) {
		setError('shortName_error', MANDATORY);
		return false;
	}
	if (!isValidConciseDescription(value)) {
		setError('shortName_error', INVALID_DESCRIPTION);
		return false;
	} else {
		setFocusLast('fieldFor');
	}
	return true;
}
function fieldFor_val() {
	var value = $('#fieldFor').val();
	clearError('fieldFor_error');
	$('#fieldAssetType').val(EMPTY_STRING);
	if (isEmpty(value)) {
		setError('fieldFor_error', MANDATORY);
		return false;
	}
	disableFieldFor_val(value);
	setFocusLast('fieldValueType');
	return true;
}
function disableFieldFor_val(fieldFor) {
	if (fieldFor == 'L') {
		clearError('fieldAssetType_error');
		$('#fieldAssetType_desc').html(EMPTY_STRING);
		$('#fieldAssetType').val(EMPTY_STRING);
		$('#fieldAssetType').prop('disabled', true);
	} else {
		$('#fieldAssetType').prop('disabled', false);
	}
}
function fieldValueType_val() {
	var value = $('#fieldValueType').val();
	clearError('fieldValueType_error');
	if (isEmpty(value)) {
		setError('fieldValueType_error', MANDATORY);
		return false;
	}
	disableFieldValueType_val(value);
	setFocusLast('fieldSize');
	return true;
}

function disableFieldValueType_val(fieldValueType) {
	if (fieldValueType == 'T' || fieldValueType == 'N') {
		clearError('noOfFractionDigits_error');
		$('#noOfFractionDigits').prop('disabled', true);
		$('#noOfFractionDigits').val(EMPTY_STRING);
	} else {
		$('#noOfFractionDigits').prop('disabled', false);
	}
}

function fieldSize_val() {
	var value = $('#fieldSize').val();
	clearError('fieldSize_error');
	if (isEmpty(value)) {
		setError('fieldSize_error', MANDATORY);
		return false;
	}
	if (!isNumeric(value)) {
		setError('fieldSize_error', INVALID_NUMBER);
		return false;
	}
	if (isNegative(value)) {
		setError('fieldSize_error', POSITIVE_CHECK);
		return false;
	}
	if ($('#fieldValueType').val() == 'T') {
		if (value > 250) {
			setError('fieldSize_error', INVALID_INT_RANGE);
			return false;
		}
		setFocusLast('fieldDedicatedToCustomerId');
	} else {
		if (value > 35) {
			setError('fieldSize_error', INVALID_FIELD_RANGE);
			return false;
		}
		if ($('#fieldValueType').val() == 'N')
			setFocusLast('fieldDedicatedToCustomerId');
		else
			setFocusLast('noOfFractionDigits');
	}
	return true;
}

function noOfFractionDigits_val() {
	var value = $('#noOfFractionDigits').val();
	clearError('noOfFractionDigits_error');
	if ($('#fieldValueType').val() == 'A' || 'F') {
		if (isEmpty(value)) {
			setError('noOfFractionDigits_error', MANDATORY);
			return false;
		}
		if (!isNumeric(value)) {
			setError('noOfFractionDigits_error', INVALID_NUMBER);
			return false;
		}
		if (isNegative(value)) {
			setError('noOfFractionDigits_error', POSITIVE_CHECK);
			return false;
		}
		if (value < 0 || value > 30) {
			setError('noOfFractionDigits_error', INVALID_FRAC_RANGE);
			return false;
		}
	} else {
		$('#noOfFractionDigits').prop('disabled', true);
	}
	setFocusLast('fieldDedicatedToCustomerId');
	return true;
}

function fieldDedicatedToCustomerId_val() {
	var value = $('#fieldDedicatedToCustomerId').val();
	clearError('fieldDedicatedToCustomerId_error');
	if (!isEmpty(value)) {
		if (isEmpty(value)) {
			setError('fieldDedicatedToCustomerId_error', PBS_MANDATORY);
			return false;
		} else if ($('#action').val() == ADD && !isValidCode(value)) {
			setError('fieldDedicatedToCustomerId_error', INVALID_FORMAT);
			return false;
		} else {
			validator.clearMap();
			validator.setMtm(false);
			validator.setValue('CUSTOMER_ID', $('#fieldDedicatedToCustomerId').val());
			validator.setValue(ACTION, $('#action').val());
			validator.setClass('patterns.config.web.forms.cmn.massetfieldsbean');
			validator.setMethod('validateFieldDedicatedToCustomerId');
			validator.sendAndReceive();
			if (validator.getValue(ERROR) != EMPTY_STRING) {
				setError('fieldDedicatedToCustomerId_error', validator.getValue(ERROR));
				return false;
			}
			if (validator.getValue(RESULT) == DATA_AVAILABLE) {
				$('#fieldDedicatedToCustomerId_desc').html(validator.getValue('CUSTOMER_NAME'));
			}
		}
	}
	if ($('#fieldFor').val() == 'L') {
		setFocusLast('remarks');
	} else {
		setFocusLast('fieldAssetType');
	}
	return true;
}

function fieldAssetType_val() {
	var value = $('#fieldAssetType').val();
	clearError('fieldAssetType_error');
	if ($('#fieldFor').val() == 'A') {
		if (!isEmpty(value)) {
			if (isEmpty(value)) {
				setError('fieldAssetType_error', MANDATORY);
				return false;
			} else if ($('#action').val() == ADD && !isValidCode(value)) {
				setError('fieldAssetType_error', INVALID_FORMAT);
				return false;
			} else {
				validator.clearMap();
				validator.setMtm(false);
				validator.setValue('FIELD_FOR', $('#fieldFor').val());
				validator.setValue('ASSET_TYPE_CODE', $('#fieldAssetType').val());
				validator.setValue(ACTION, $('#action').val());
				validator.setClass('patterns.config.web.forms.cmn.massetfieldsbean');
				validator.setMethod('validateFieldAssetType');
				validator.sendAndReceive();
				if (validator.getValue(ERROR) != EMPTY_STRING) {
					setError('fieldAssetType_error', validator.getValue(ERROR));
					return false;
				}
				if (validator.getValue(RESULT) == DATA_AVAILABLE) {
					$('#fieldAssetType_desc').html(validator.getValue('DESCRIPTION'));
				}
			}
		}
	}
	setFocusLast('remarks');
	return true;
}

function enabled_val() {
	if ($('#action').val() == MODIFY) {
		setFocusLast('remarks');
	}
	return true;
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', INVALID_REMARKS);
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', INVALID_REMARKS);
			return false;
		}
	}
	if ($('#action').val() == MODIFY) {
		if (isEmpty(value)) {
			setError('remarks_error', MANDATORY);
			return false;
		}
	}
	setFocusOnSubmit();
	return true;
}