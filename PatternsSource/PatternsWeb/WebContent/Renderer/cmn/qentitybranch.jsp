<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.cmn.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/cmn/qentitybranch.js" />
	</web:dependencies>
	<web:loadPK />
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="qentitybranch.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:viewContent id="po_view1">
				<web:dividerBlock>
					<web:sectionBlock width="width-100p" align="left">
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="180px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="mentitybranch.entitycode" var="program" />
									</web:column>
									<web:column>
										<type:entityCodeDisplay property="entityCode" id="entityCode" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="mentitybranch.entitybranchcode" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:branchDisplay property="entityBranchCode" id="entityBranchCode" />
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="180px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="form.name" var="common" mandatory="true" />
									</web:column>
									<web:column>
										<type:descriptionDisplay property="description" id="description" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="form.shortName" var="common" mandatory="true" />
									</web:column>
									<web:column>
										<type:conciseDescriptionDisplay property="conciseDescription" id="conciseDescription" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
										<web:column>
											<web:legend key="mentitybranch.officeopendate" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:dateDisplay property="officeOpenDate" id="officeOpenDate" />
										</web:column>
									</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="mentitybranch.branchdetails" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:remarksDisplay property="branchDetails" id="branchDetails" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="mentitybranch.geounitstatecode" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:geographicalUnitIdDisplay property="geoUnitStateCode" id="geoUnitStateCode" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="mentitybranch.organizationunit" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:organizationUnitTypeDisplay property="organizationUnit" id="organizationUnit" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="mentitybranch.controlbranchoffice" var="program" />
									</web:column>
									<web:column>
										<type:branchDisplay property="controlBranchOffice" id="controlBranchOffice" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="mentitybranch.clustercode" var="program" />
									</web:column>
									<web:column>
										<type:accountClusterCodeDisplay property="clusterCode" id="clusterCode" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
										<web:column>
											<web:legend key="mentitybranch.officeclosedate" var="program" />
										</web:column>
										<web:column>
											<type:dateDisplay property="officeCloseDate" id="officeCloseDate" />
										</web:column>
									</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="form.enabled" var="common" />
									</web:column>
									<web:column>
										<type:checkboxDisplay property="enabled" id="enabled" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="form.remarks" var="common" />
									</web:column>
									<web:column>
										<type:remarksDisplay property="remarks" id="remarks" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:auditDisplay />
					</web:sectionBlock>
				</web:dividerBlock>
			</web:viewContent>
		</web:viewSubPart>
	</web:viewPart>
</web:fragment>
