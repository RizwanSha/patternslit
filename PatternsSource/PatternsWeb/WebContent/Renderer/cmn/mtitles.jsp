<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.cmn.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/cmn/mtitles.js" />
	</web:dependencies>
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="mtitles.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/cmn/mtitles" id="mtitles" method="POST">
				<web:dividerBlock>
					<type:optionBox add="true" modify="true" view="true" />
				</web:dividerBlock>
				<web:viewContent id="po_view1" styleClass="hidden">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="mtitles.TitleCode" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:title property="titleCode" id="titleCode" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="mtitles.ExtDescription" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:description property="extDescription" id="extDescription" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="mtitles.IntDescription" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:description property="intDescription" id="intDescription"  />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="mtitles.shortDescription" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:conciseDescription property="shortDescription" id="shortDescription"  />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="mtitles.TitleUsage" var="program" />
										</web:column>
										<web:column>
											<type:combo property="titleUsage" id="titleUsage" datasourceid="COMMON_CMNTITLES" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="mtitles.TitleChildrenUsage" var="program" />
										</web:column>
										<web:column>
											<type:checkbox property="titleChildrenUsage" id="titleChildrenUsage" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="mtitles.TitleDeceasedUsage" var="program" />
										</web:column>
										<web:column>
											<type:checkbox property="titleDeceasedUsage" id="titleDeceasedUsage" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="mtitles.TitleOccupation" var="program" />
										</web:column>
										<web:column>
											<type:occupationCode property="titleOccupation" id="titleOccupation" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="mtitles.OtherOccupation" var="program" />
										</web:column>
										<web:column>
											<type:freeText_100 property="otherOccupation" id="otherOccupation"  />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="mtitles.defMaritalStatus" var="program" />
										</web:column>
										<web:column>
											<type:maritalStatusCode property="defMaritalStatus" id="defMaritalStatus" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="mtitles.fixMaritalStatus" var="program" />
										</web:column>
										<web:column>
											<type:checkbox property="fixMaritalStatus" id="fixMaritalStatus" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="mtitles.enabled" var="program" />
										</web:column>
										<web:column>
											<type:checkbox property="enabled" id="enabled" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="form.remarks" var="common" />
										</web:column>
										<web:column>
											<type:remarks property="remarks" id="remarks" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column span="2">
											<web:submitReset />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
						</web:sectionBlock>
					</web:dividerBlock>
				</web:viewContent>
				<web:element property="command" />
				<web:element property="action" />
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
	<web:queryGrid />
	<web:TBAGrid />
	<web:contextSearch />
</web:fragment>
