var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MNARRCODES';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}
function clearFields() {
	$('#narrationCode').val(EMPTY_STRING);
	$('#description').val(EMPTY_STRING);
	$('#conciseDescription').val(EMPTY_STRING);
	$("#useTranCode").prop('selectedIndex', 0);
	setCheckbox('enabled', YES);
	$('#remarks').val(EMPTY_STRING);

}
function loadData() {
	$('#narrationCode').val(validator.getValue('NARR_CODE'));
	$('#description').val(validator.getValue('DESCRIPTION'));
	$('#conciseDescription').val(validator.getValue('CONCISE_DESCRIPTION'));
	$('#useTranCode').val(validator.getValue('TRAN_MODE'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadAuditFields(validator);
}