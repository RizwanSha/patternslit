<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.cmn.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/cmn/qleaseproduct.js" />
	</web:dependencies>
	<web:loadPK />
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="mleaseproduct.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:viewContent id="po_view1">
				<web:dividerBlock>
					<web:sectionBlock width="width-100p" align="left">
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="mleaseproduct.leaseproductcode" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:leaseProductCodeDisplay property="leaseProductCode" id="leaseProductCode" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowOdd>
									<web:column>
										<web:legend key="form.description" var="common" mandatory="true" />
									</web:column>
									<web:column>
										<type:descriptionDisplay property="leaseProductDescription" id="leaseProductDescription" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="form.conciseDescription" var="common" mandatory="true" />
									</web:column>
									<web:column>
										<type:conciseDescriptionDisplay property="leaseProductConciseDescription" id="leaseProductConciseDescription" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="mleaseproduct.leasetype" var="program" mandatory="false" />
									</web:column>
									<web:column>
										<type:comboDisplay property="leaseType" id="leaseType" datasourceid="COMMON_LEASE_TYPE" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="mleaseproduct.basecurrcode" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:currencyDisplay property="baseCurrCode" id="baseCurrCode" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:rowOdd>
									<web:column>
										<web:viewTitle key="mleaseproduct.glheadproduct" var="program" />
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="mleaseproduct.sundrydebtorshead" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:GLAccessCodeDisplay property="sundryDebtorsHead" id="sundryDebtorsHead" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="mleaseproduct.rentreceivableprincipal" var="program" />
									</web:column>
									<web:column>
										<type:GLAccessCodeDisplay property="rentReceivablePrincipal" id="rentReceivablePrincipal" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="mleaseproduct.rentreceivableinterest" var="program" />
									</web:column>
									<web:column>
										<type:GLAccessCodeDisplay property="rentReceivableInterest" id="rentReceivableInterest" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="mleaseproduct.unearnedincomehead" var="program" />
									</web:column>
									<web:column>
										<type:GLAccessCodeDisplay property="unearnedIncomeHead" id="unearnedIncomeHead" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="form.enabled" var="common" />
									</web:column>
									<web:column>
										<type:checkboxDisplay property="enabled" id="enabled" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="form.remarks" var="common" />
									</web:column>
									<web:column>
										<type:remarksDisplay property="remarks" id="remarks" />
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
						<web:auditDisplay />
					</web:sectionBlock>
				</web:dividerBlock>
			</web:viewContent>
		</web:viewSubPart>
	</web:viewPart>
</web:fragment>
