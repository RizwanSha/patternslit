
<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.cmn.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/cmn/qindasset.js" />
	</web:dependencies>
	<web:loadPK />
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="qindasset.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:viewContent id="po_view1">
				<web:dividerBlock>
					<web:sectionBlock width="width-100p" align="left">
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="225px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="mindasset.leasecode" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:codeDisplay property="leaseCode" id="leaseCode" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="mindasset.agreeno" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:agreementNoDisplay property="agreeNo" id="agreeNo" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="mindasset.scheduleid" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:scheduleCodeDisplay property="scheduleId" id="scheduleId" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="mindasset.assetsl" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:serialDisplay property="assetSl" id="assetSl" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="mindasset.compsl" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:serialDisplay property="compSl" id="compSl" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="225px" />
									<web:columnStyle width="225px" />
									<web:columnStyle width="200px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowOdd>
									<web:column>
										<web:legend key="mindasset.qtysl" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:serialDisplay property="qtySl" id="qtySl" />
									</web:column>
									<web:column>
										<web:legend key="mindasset.qtyofasset" var="program" />
									</web:column>
									<web:column>
										<type:serialDisplay property="qtyOfAsset" id="qtyOfAsset" />
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="225px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="mindasset.indassetid" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:assetIdDisplay property="indAssetId" id="indAssetId" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:viewTitle var="program" key="mindasset.details" />
						<web:section>
							<web:table>
								<web:rowOdd>
									<web:column>
										<web:grid height="250px" width="962px" id="pgmGrid" src="cmn/mindasset_Grid.xml">
										</web:grid>
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
						<web:viewContent id="enabledvalue" styleClass="hidden">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="225px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="form.enabled" var="common" />
										</web:column>
										<web:column>
											<type:checkboxDisplay property="enabled" id="enabled" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
						</web:viewContent>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="225px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="form.remarks" var="common" />
									</web:column>
									<web:column>
										<type:remarksDisplay property="remarks" id="remarks" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:auditDisplay />
					</web:sectionBlock>
				</web:dividerBlock>
			</web:viewContent>
		</web:viewSubPart>
	</web:viewPart>
</web:fragment>
