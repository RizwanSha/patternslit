var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MSCHFORACCTYPE';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}

function loadQuery() {
	loadGridQuery(CURRENT_PROGRAM_ID, 'MSCHFORACCTYPE_GRID', mschforacctype_innerGrid);
	mschforacctype_innerGrid.setColTypes("chr,ro,ro");
}

function clearFields() {
	$('#accountTypeCode').val(EMPTY_STRING);
	$('#accountTypeCode_desc').html(EMPTY_STRING);
	$('#effectiveDate').val(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	mschforacctype_innerGrid.clearAll();
}

function loadData() {
	$('#accountTypeCode').val(validator.getValue('ACCOUNT_TYPE_CODE'));
	$('#effectiveDate').val(validator.getValue('EFF_DATE'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	$('#accountTypeCode_desc').html(validator.getValue('F1_DESCRIPTION'));
	loadAuditFields(validator);
	loadGrid();
}

function loadGrid() {
	SOURCE_VALUE = _tableType;
	PK_VALUE = _primaryKey;
	if (_tableType == MAIN) {
		loadQuery();

	} else if (_tableType == TBA) {
		loadQuery();
	}
}
