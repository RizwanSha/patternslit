<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.cmn.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/cmn/qcustgeoalloc.js" />
	</web:dependencies>
	<web:loadPK />
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="ecustgeoalloc.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
				<web:viewContent id="po_view1" >
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="ecustgeoalloc.cusotmerIdFoNo" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:cifNumberDisplay property="cusotmerIdFoNo" id="cusotmerIdFoNo" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="ecustgeoalloc.address" var="program" />
										</web:column>
										<web:column>
											<type:addressDisplay property="address" id="address" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="ecustgeoalloc.pincode" var="program" />
										</web:column>
										<web:column>
											<type:geographicalUnitIdDisplay property="pincode" id="pincode" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="form.effectivedate" var="common" mandatory="true" />
										</web:column>
										<web:column>
											<type:dateDisplay property="effectiveDate" id="effectiveDate" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:viewTitle var="program" key="ecustgeoalloc.section" />
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:viewContent id="po_view4">
												<web:grid height="200px" width="630px" id="ecustgeoalloc_innerGrid" src="cmn/ecustgeoalloc_innerGrid.xml">
												</web:grid>
											</web:viewContent>
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="form.IsUseEnabled" var="common" />
										</web:column>
										<web:column>
											<type:checkboxDisplay property="enabled" id="enabled" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="form.remarks" var="common" />
										</web:column>
										<web:column>
											<type:remarksDisplay property="remarks" id="remarks" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
								</web:table>
							</web:section>
							<web:auditDisplay />
						</web:sectionBlock>
					</web:dividerBlock>
				</web:viewContent>
		</web:viewSubPart>
	</web:viewPart>
</web:fragment>
