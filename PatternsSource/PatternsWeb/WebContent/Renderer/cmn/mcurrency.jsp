<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.cmn.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/cmn/mcurrency.js" />
	</web:dependencies>
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="mcurrency.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/cmn/mcurrency" id="mcurrency" method="POST">
				<web:dividerBlock>
					<type:optionBox add="true" modify="true" view="true" />
				</web:dividerBlock>
				<web:viewContent id="po_view1" styleClass="hidden">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="mcurrency.currencycode" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:currency property="currencyCode" id="currencyCode" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="mcurrency.ccyname" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:description property="ccyName" id="ccyName" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="mcurrency.ccyshortname" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:conciseDescription property="ccyShortName" id="ccyShortName" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="mcurrency.ccynotation" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:currencyNotation property="ccyNotation" id="ccyNotation" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="mcurrency.hassubccy" var="program" />
										</web:column>
										<web:column>
											<type:checkbox property="hasSubCcy" id="hasSubCcy" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="mcurrency.sccyname" var="program"/>
										</web:column>
										<web:column>
											<type:description property="sccyName" id="sccyName"/>
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="mcurrency.sccyshortname" var="program"/>
										</web:column>
										<web:column>
											<type:conciseDescription property="sccyShortName" id="sccyShortName" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="mcurrency.sccynotation" var="program"/>
										</web:column>
										<web:column>
											<type:currencyNotation property="sccyNotation" id="sccyNotation" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="mcurrency.sccyunitsforccy" var="program"/>
										</web:column>
										<web:column>
											<type:fiveDigit property="sccyUnitsForCcy" id="sccyUnitsForCcy" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="mcurrency.isoccycode" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:ISOcurrency property="isoCcyCode" id="isoCcyCode" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="form.IsUseEnabled" var="common" />
										</web:column>
										<web:column>
											<type:checkbox property="enabled" id="enabled" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="form.remarks" var="common" />
										</web:column>
										<web:column>
											<type:remarks property="remarks" id="remarks" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column span="2">
											<web:submitReset />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
						</web:sectionBlock>
					</web:dividerBlock>
				</web:viewContent>
				<web:element property="command" />
				<web:element property="action" />
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
	<web:queryGrid />
	<web:TBAGrid />
	<web:contextSearch />
</web:fragment>