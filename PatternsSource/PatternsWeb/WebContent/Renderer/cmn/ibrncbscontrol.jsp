<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.cmn.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/cmn/ibrncbscontrol.js" />
	</web:dependencies>
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="ibrncbscontrol.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/cmn/ibrncbscontrol" id="ibrncbscontrol" method="POST">
				<web:dividerBlock>
					<type:optionBox add="true" modify="true" view="true" />
				</web:dividerBlock>
				<web:viewContent id="po_view1" styleClass="hidden">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="220px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="ibrncbscontrol.entityoffcode" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:entitybranchCode property="entityOffCode" id="entityOffCode"  />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="220px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="ibrncbscontrol.incbsdate" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:date property="inCbsDate" id="inCbsDate"  />
										</web:column>
									</web:rowOdd>	
									<web:rowEven>
										<web:column>
											<web:legend key="ibrncbscontrol.transdate" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:date property="transDate" id="transDate"  />
										</web:column>
									</web:rowEven>	
									<web:rowOdd>
										<web:column>
											<web:legend key="ibrncbscontrol.currcbslink" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:combo property="currCbsLink" id="currCbsLink" datasourceid="IBRNCBSCONTROL_CURRENT_CBS_LINK" />
										</web:column>
									</web:rowOdd>					
									<web:rowEven>
										<web:column>
											<web:legend key="form.enabled" var="common"/>
										</web:column>
										<web:column>
											<type:checkbox property="enabled" id="enabled"/>
										</web:column>
									</web:rowEven>									
									<web:rowOdd>
										<web:column>
											<web:legend key="form.remarks" var="common" />
										</web:column>
										<web:column>
											<type:remarks property="remarks" id="remarks" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:rowEven>
										<web:column span="2">
											<web:submitReset />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
						</web:sectionBlock>
					</web:dividerBlock>
				</web:viewContent>
				<web:element property="command" />
				<web:element property="action" />
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
	<web:queryGrid />
	<web:TBAGrid />
	<web:contextSearch />
</web:fragment>