var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MTAX';
var editModeFlag = false;
function init() {
	refreshTBAGrid();
	refreshQueryGrid();
	if (_redisplay) {
		if ($('#action').val() == MODIFY) {
			$('#enabled').prop('disabled', false);
		} else {
			setCheckbox('enabled', YES);
			$('#enabled').prop('disabled', true);
		}
		if ($('#taxOn').val() == 4 || ($('#stateCode').val() == $('#installCenteralCode').val())) {
			$('#noOfTaxSchedules').val(ONE);
			$('#noOfTaxSchedules').attr('readonly', true);
			$('#taxOnTaxCode').attr('readonly', true);
			$('#taxOnTaxCode_pic').prop('disabled', true);
			hide("viewTaxscheduleCode");
			gridDisable();
		} else {
			show("viewTaxscheduleCode");
			$('#taxOnTaxCode').attr('readonly', true);
			$('#taxOnTaxCode_pic').prop('disabled', true);
			$('#noOfTaxSchedules').attr('readonly', false);
		}
		tax_innerGrid.clearAll();
		if (!isEmpty($('#xmlTax').val())) {
			tax_innerGrid.loadXMLString($('#xmlTax').val());
		}
	}
}

function loadGrid() {
	loadGridQuery(CURRENT_PROGRAM_ID, 'MTAX_GRID', tax_innerGrid);
}
function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'MTAX_MAIN', queryGrid);
}

function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}

function doHelp(id) {
	switch (id) {
	case 'stateCode':
		help('COMMON', 'HLP_STATE_CODE', $('#stateCode').val(), EMPTY_STRING, $('#stateCode'));
		break;
	case 'taxCode':
	case 'taxOnTaxCode':
		help('COMMON', 'HLP_TAX_CODE', $('#' + id).val(), $('#stateCode').val(), $('#' + id), function(gridObj, rowID) {
		});
		break;
	case 'taxIpGLAcctHead':
	case 'taxPayGLAcctHead':
	case 'taxBorneGLAcctHead':
		help('COMMON', 'HLP_GL_HEAD_CODE', $('#' + id).val(), EMPTY_STRING, $('#' + id));
		break;
	}
}

function clearFields() {
	$('#stateCode').val(EMPTY_STRING);
	$('#stateCode_desc').html(EMPTY_STRING);
	$('#stateCode_error').html(EMPTY_STRING);
	$('#taxCode').val(EMPTY_STRING);
	$('#taxCode_desc').html(EMPTY_STRING);
	$('#taxCode_error').html(EMPTY_STRING);
	clearNonPKFields();
}

function clearNonPKFields() {
	$('#description').val(EMPTY_STRING);
	$('#description_error').html(EMPTY_STRING);
	$('#conciseDescription').val(EMPTY_STRING);
	$('#conciseDescription_error').html(EMPTY_STRING);
	$('#taxType').prop('selectedIndex', 0);
	$('#taxType_error').html(EMPTY_STRING);
	$('#taxOn').prop('selectedIndex', 0);
	$('#taxOn_error').html(EMPTY_STRING);
	$('#taxOnTaxCode').val(EMPTY_STRING);
	if ($('#taxOn').val() != 4) {
		$('#taxOnTaxCode').attr('readonly', true);
		$('#taxOnTaxCode_pic').prop('disabled', true);
	}
	$('#taxOnTaxCode_desc').html(EMPTY_STRING);
	$('#taxOnTaxCode_error').html(EMPTY_STRING);
	$('#taxIpGLAcctHead').val(EMPTY_STRING);
	$('#taxIpGLAcctHead_desc').html(EMPTY_STRING);
	$('#taxIpGLAcctHead_error').html(EMPTY_STRING);
	$('#taxPayGLAcctHead').val(EMPTY_STRING);
	$('#taxPayGLAcctHead_desc').html(EMPTY_STRING);
	$('#taxPayGLAcctHead_error').html(EMPTY_STRING);
	$('#noOfTaxSchedules').val(EMPTY_STRING);
	$('#noOfTaxSchedules_error').html(EMPTY_STRING);
	setCheckbox('prdctWiseTaxInclusive', NO);
	setCheckbox('cstmerWiseTaxInclusive', NO);
	setCheckbox('leaseWiseTaxInclusive', NO);
	show("viewTaxscheduleCode");
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
	tax_clearGridFields();
	tax_innerGrid.clearAll();
}

function doclearfields(id) {
	switch (id) {
	case 'stateCode':
		if (isEmpty($('#stateCode').val())) {
			clearFields();
		}
		break;
	case 'taxCode':
		if (isEmpty($('#taxCode').val())) {
			$('#taxCode').val(EMPTY_STRING);
			$('#taxCode_desc').html(EMPTY_STRING);
			$('#taxCode_error').html(EMPTY_STRING);
			clearNonPKFields();
		}
		break;
	}
}

function add() {
	$('#stateCode').focus();
	setCheckbox('enabled', YES);
	setCheckbox('prdctWiseTaxInclusive', NO);
	setCheckbox('cstmerWiseTaxInclusive', NO);
	setCheckbox('leaseWiseTaxInclusive', NO);
	$('#enabled').prop('disabled', true);

}

function modify() {
	$('#stateCode').focus();
	if ($('#action').val() == MODIFY) {
		$('#enabled').prop('disabled', false);
	} else {
		$('#enabled').prop('disabled', true);
	}
}

function loadData() {
	$('#stateCode').val(validator.getValue('STATE_CODE'));
	$('#stateCode_desc').html(validator.getValue('F1_DESCRIPTION'));
	$('#taxCode').val(validator.getValue('TAX_CODE'));
	$('#description').val(validator.getValue('DESCRIPTION'));
	$('#conciseDescription').val(validator.getValue('CONCISE_DESCRIPTION'));
	$('#taxType').val(validator.getValue('TAX_TYPE'));
	$('#taxOn').val(validator.getValue('TAX_ON'));
	$('#taxOnTaxCode').val(validator.getValue('TAX_ON_TAX_CODE'));
	$('#taxOnTaxCode_desc').html(validator.getValue('F4_DESCRIPTION'));
	$('#noOfTaxSchedules').val(validator.getValue('NOF_TAX_SCHEDULES'));
	if ($('#taxOn').val() != '4') {
		$('#taxOnTaxCode').attr('readonly', true);
		$('#noOfTaxSchedules').attr('readonly', false);
		show("viewTaxscheduleCode");
	} else {
		hide("viewTaxscheduleCode");
		gridDisable();
		$('#noOfTaxSchedules').val(ONE);
		$('#noOfTaxSchedules').attr('readonly', true);
		$('#taxOnTaxCode').attr('readonly', false);
	}
	$('#taxIpGLAcctHead').val(validator.getValue('TAX_INPUT_GL_HEAD_CODE'));
	$('#taxIpGLAcctHead_desc').html(validator.getValue('F2_DESCRIPTION'));
	$('#taxPayGLAcctHead').val(validator.getValue('TAX_PAY_GL_HEAD_CODE'));
	$('#taxPayGLAcctHead_desc').html(validator.getValue('F3_DESCRIPTION'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	setCheckbox('prdctWiseTaxInclusive', validator.getValue('TAX_BYUS_PROD_ALLOWED'));
	setCheckbox('cstmerWiseTaxInclusive', validator.getValue('TAX_BYUS_CUST_ALLOWED'));
	setCheckbox('leaseWiseTaxInclusive', validator.getValue('TAX_BYUS_LEASE_ALLOWED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	centeralCode = validator.getValue('F5_CENTRAL_CODE');
	if (centeralCode == $('#stateCode').val()) {
		$('#noOfTaxSchedules').val(ONE);
		$('#noOfTaxSchedules').attr('readonly', true);
		hide("viewTaxscheduleCode");
	} else {
		if ($('#taxOn').val() != '4') {
			show("viewTaxscheduleCode");
			$('#noOfTaxSchedules').attr('readonly', false);
		}
	}
	loadGrid();
	resetLoading();

}

function view(source, primaryKey) {
	hideParent('cmn/qtax', source, primaryKey);
	resetLoading();
}

function validate(id) {
	switch (id) {
	case 'stateCode':
		stateCode_val();
		break;
	case 'taxCode':
		taxCode_val();
		break;
	case 'description':
		description_val();
		break;
	case 'conciseDescription':
		conciseDescription_val();
		break;
	case 'taxType':
		taxType_val();
		break;
	case 'taxOn':
		taxOn_val();
		break;
	case 'taxOnTaxCode':
		taxOnTaxCode_val();
		break;
	case 'taxIpGLAcctHead':
		taxIpGLAcctHead_val();
		break;
	case 'taxPayGLAcctHead':
		taxPayGLAcctHead_val();
		break;
	case 'noOfTaxSchedules':
		noOfTaxSchedules_val();
		break;
	case 'taxScheduleCode':
		taxScheduleCode_val();
		break;
	case 'prdctWiseTaxInclusive':
		prdctWiseTaxInclusive_val();
		break;
	case 'cstmerWiseTaxInclusive':
		cstmerWiseTaxInclusive_val();
		break;
	case 'leaseWiseTaxInclusive':
		leaseWiseTaxInclusive_val();
		break;
	case 'enabled':
		enabled_val();
		break;
	case 'remarks':
		remarks_val();
		break;
	}
}

function backtrack(id) {
	switch (id) {
	case 'stateCode':
		setFocusLast('stateCode');
		break;
	case 'taxCode':
		setFocusLast('stateCode');
		break;
	case 'description':
		setFocusLast('taxCode');
		break;
	case 'conciseDescription':
		setFocusLast('description');
		break;
	case 'taxType':
		setFocusLast('conciseDescription');
		break;
	case 'taxOn':
		setFocusLast('taxType');
		break;
	case 'taxOnTaxCode':
		setFocusLast('taxOn');
		break;
	case 'taxIpGLAcctHead':
		if ($('#taxOn').val() == '4') {
			setFocusLast('taxOnTaxCode');
		} else {
			setFocusLast('taxOn');
		}
		break;
	case 'taxPayGLAcctHead':
		setFocusLast('taxIpGLAcctHead');
		break;
	case 'noOfTaxSchedules':
		setFocusLast('taxPayGLAcctHead');
		break;
	case 'taxScheduleCode':
		setFocusLast('noOfTaxSchedules');
		break;
	case 'tax_innerGrid_add':
		setFocusLast('taxScheduleCode');
		break;
	case 'prdctWiseTaxInclusive':
		if ($('#stateCode').val() == $('#installCenteralCode').val()) {
			setFocusLast('taxPayGLAcctHead');
		} else if ($('#taxOn').val() == '4') {
			setFocusLast('taxPayGLAcctHead');
		} else {
			setFocusLast('taxScheduleCode');
		}
		break;
	case 'cstmerWiseTaxInclusive':
		setFocusLast('prdctWiseTaxInclusive');
		break;
	case 'leaseWiseTaxInclusive':
		setFocusLast('cstmerWiseTaxInclusive');
		break;
	case 'enabled':
		setFocusLast('leaseWiseTaxInclusive');
		break;
	case 'remarks':
		if ($('#action').val() == MODIFY) {
			setFocusLast('enabled');
		} else {
			setFocusLast('leaseWiseTaxInclusive');
		}
		break;
	}
}
function stateCode_val() {
	var value = $('#stateCode').val();
	clearError('stateCode_error');
	if (isEmpty(value)) {
		setError('stateCode_error', MANDATORY);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('STATE_CODE', value);
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.cmn.mtaxbean');
		validator.setMethod('validateStateCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('stateCode_error', validator.getValue(ERROR));
			$('#stateCode_desc').html(EMPTY_STRING);
			return false;
		}
		$('#stateCode_desc').html(validator.getValue('DESCRIPTION'));
	}
	setFocusLast('taxCode');
	return true;
}

function taxCode_val() {
	var value = $('#taxCode').val();
	clearError('taxCode_error');
	if (isEmpty(value)) {
		setError('taxCode_error', MANDATORY);
		return false;
	} else if ($('#action').val() == ADD && !isValidCode(value)) {
		setError('taxCode_error', INVALID_FORMAT);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('STATE_CODE', $('#stateCode').val());
	validator.setValue('CENTRAL_CODE', $('#installCenteralCode').val());
	validator.setValue('TAX_CODE', value);
	validator.setValue(ACTION, $('#action').val());
	validator.setClass('patterns.config.web.forms.cmn.mtaxbean');
	validator.setMethod('validateTaxCode');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('taxCode_error', validator.getValue(ERROR));
		return false;
	} else {
		if ($('#action').val() == MODIFY) {
			PK_VALUE = getEntityCode() + PK_SEPERATOR + $('#stateCode').val() + PK_SEPERATOR + $('#taxCode').val();
			if (!loadPKValues(PK_VALUE, 'taxCode_error')) {
				return false;
			}
		}
		if ($('#stateCode').val() == $('#installCenteralCode').val()) {
			$('#noOfTaxSchedules').val(ONE);
			var field_values = [ 'false', $('#taxScheduleCodeTaxOnTaxCode').val() ];
			_grid_updateRow(tax_innerGrid, field_values);
			gridDisable();
		} else {
			show("viewTaxscheduleCode");
			if ($('#taxOn').val() != 4) {
				$('#noOfTaxSchedules').attr('readonly', false);
			}
		}
	}
	setFocusLast('description');
	return true;
}

function description_val() {
	var value = $('#description').val();
	clearError('description_error');
	if (isEmpty(value)) {
		setError('description_error', MANDATORY);
		return false;
	}
	if (!isValidDescription(value)) {
		setError('description_error', INVALID_DESCRIPTION);
		return false;
	}
	setFocusLast('conciseDescription');
	return true;
}

function conciseDescription_val() {
	var value = $('#conciseDescription').val();
	clearError('conciseDescription_error');
	if (isEmpty(value)) {
		setError('conciseDescription_error', MANDATORY);
		return false;
	}
	if (!isValidConciseDescription(value)) {
		setError('conciseDescription_error', INVALID_CONCISE_DESCRIPTION);
		return false;
	}
	setFocusLast('taxType');
	return true;
}

function taxType_val() {
	var value = $('#taxType').val();
	clearError('taxType_error');
	if (isEmpty(value)) {
		setError('taxCode_error', MANDATORY);
		return false;
	}
	setFocusLast('taxOn');
	return true;
}

function taxOn_val() {
	var value = $('#taxOn').val();
	clearError('taxOn_error');
	if (isEmpty(value)) {
		setError('taxOn_error', MANDATORY);
		return false;
	}
	if (value == '4') {
		tax_clearGridFields();
		tax_innerGrid.clearAll();
		$('#noOfTaxSchedules').val(ONE);
		var field_values = [ 'false', $('#taxScheduleCodeTaxOnTaxCode').val() ];
		_grid_updateRow(tax_innerGrid, field_values);
		gridDisable();
		setFocusLast('taxOnTaxCode');
		return true;
	} else {
		$('#taxOnTaxCode').val(EMPTY_STRING);
		clearError('taxOnTaxCode_error');
		$('#taxOnTaxCode_desc').html(EMPTY_STRING);
		$('#taxOnTaxCode').attr('readonly', true);
		$('#taxOnTaxCode_pic').prop('disabled', true);
		$('#taxScheduleCode').attr('readonly', false);
		$('#noOfTaxSchedules').attr('readonly', false);
		if ($('#stateCode').val() == $('#installCenteralCode').val()) {
			gridDisable();
			hide('viewTaxscheduleCode');
		} else {
			show('viewTaxscheduleCode');
			if (tax_innerGrid.getRowsNum() > ZERO) {
				if (tax_innerGrid.cells2(0, 1).getValue() == $('#taxScheduleCodeTaxOnTaxCode').val()) {
					$('#noOfTaxSchedules').val(EMPTY_STRING);
					tax_clearGridFields();
					tax_innerGrid.clearAll();
				}
			}
		}
	}
	setFocusLast('taxIpGLAcctHead');
	return true;
}
function gridDisable() {
	if ($('#taxOn').val() == 4) {
		$('#taxOnTaxCode').attr('readonly', false);
		$('#taxOnTaxCode_pic').prop('disabled', false);
	}
	$('#noOfTaxSchedules_error').html(EMPTY_STRING);
	$('#noOfTaxSchedules').attr('readonly', true);
	hide('viewTaxscheduleCode');
	var noofrows = tax_innerGrid.getRowsNum();
	for (var i = 0; i < noofrows; i++) {
		tax_innerGrid.cells2(i, 0).setValue(false);
		tax_innerGrid.cells2(i, 0).setDisabled(true);
		tax_innerGrid.cells2(i, 1).setDisabled(true);
	}

}
function taxOnTaxCode_val() {
	var value = $('#taxOnTaxCode').val();
	clearError('taxOnTaxCode_error');

	if (isEmpty(value)) {
		setError('taxOnTaxCode_error', MANDATORY);
		return false;
	} else if ($('#action').val() == ADD && !isValidCode(value)) {
		setError('taxOnTaxCode_error', INVALID_FORMAT);
		return false;
	} else if (value == $('#taxCode').val() && $('#action').val() == MODIFY) {
		setError('taxOnTaxCode_error', INVALID_TaxOnCode);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('STATE_CODE', $('#stateCode').val());
		validator.setValue('ACTUAL_TAX_CODE', $('#taxCode').val());
		validator.setValue('TAX_ON_TAX_CODE', value);
		validator.setValue('TAX_ON', $('#taxOn').val());
		validator.setValue(ACTION, $('#action').val());
		validator.setClass('patterns.config.web.forms.cmn.mtaxbean');
		validator.setMethod('validateTaxOnTaxCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('taxOnTaxCode_error', validator.getValue(ERROR));
			return false;
		}
		$('#taxOnTaxCode_desc').html(validator.getValue('DESCRIPTION'));
		setFocusLast('taxIpGLAcctHead');
		return true;
	}
}

function taxIpGLAcctHead_val() {
	var value = $('#taxIpGLAcctHead').val();
	clearError('taxIpGLAcctHead_error');
	if (isEmpty(value)) {
		setError('taxIpGLAcctHead_error', MANDATORY);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('GL_HEAD_CODE', $('#taxIpGLAcctHead').val());
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.cmn.mtaxbean');
		validator.setMethod('validateTaxIpGLAcctHead');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('taxIpGLAcctHead_error', validator.getValue(ERROR));
			$('#taxIpGLAcctHead_desc').html(EMPTY_STRING);
			return false;
		}
		$('#taxIpGLAcctHead_desc').html(validator.getValue('DESCRIPTION'));
	}
	setFocusLast('taxPayGLAcctHead');
	return true;
}

function taxPayGLAcctHead_val() {
	var value = $('#taxPayGLAcctHead').val();
	clearError('taxPayGLAcctHead_error');
	if (isEmpty(value)) {
		setError('taxPayGLAcctHead_error', MANDATORY);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('GL_HEAD_CODE', $('#taxPayGLAcctHead').val());
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.cmn.mtaxbean');
		validator.setMethod('validateTaxPayGLAcctHead');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('taxPayGLAcctHead_error', validator.getValue(ERROR));
			$('#taxPayGLAcctHead_desc').html(EMPTY_STRING);
			return false;
		}
		$('#taxPayGLAcctHead_desc').html(validator.getValue('DESCRIPTION'));
	}
	if ($('#installCenteralCode').val() == $('#stateCode').val()) {
		setFocusLast('prdctWiseTaxInclusive');
	} else if ($('#taxOn').val() != 4) {
		setFocusLast('noOfTaxSchedules');
	} else {
		setFocusLast('prdctWiseTaxInclusive');
	}
	return true;
}

function noOfTaxSchedules_val() {
	var value = $('#noOfTaxSchedules').val();
	if ($('#taxOn').val() != 4) {
		clearError('noOfTaxSchedules_error');
		if (isEmpty(value)) {
			setError('noOfTaxSchedules_error', MANDATORY);
			return false;
		}
		if (!isValidTwoDigit(value)) {
			setError('noOfTaxSchedules_error', INVALID_VALUE);
			return false;
		}
		if (isZero(value)) {
			setError('noOfTaxSchedules_error', ZERO_CHECK);
			return false;
		}
		if (isNegative(value)) {
			setError('noOfTaxSchedules_error', POSITIVE_CHECK);
			return false;
		}
		if (value == tax_innerGrid.getRowsNum()) {
			setFocusLast('prdctWiseTaxInclusive');
		} else {
			setFocusLast('taxScheduleCode');
		}
		return true;
	}
}

function taxScheduleCode_val() {
	if ($('#taxOn').val() != 4) {
		var value = $('#taxScheduleCode').val();
		clearError('taxScheduleCode_error');
		if (isEmpty(value)) {
			setError('taxScheduleCode_error', MANDATORY);
			return false;
		}
		if (isEmpty($('#noOfTaxSchedules').val())) {
			setError('taxScheduleCode_error', TAXSCHEDULES_MANDATORY);
			return false;
		}
		if (!isValidOtherInformation25(value)) {
			setError('taxScheduleCode_error', INVALID_VALUE);
			return false;
		}
		if (value == $('#taxScheduleCodeTaxOnTaxCode').val()) {
			setError('taxScheduleCode_error', INVALID_VALUE_CENTERAL_CODE);
			return false;
		}
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('TAX_SCHEDULE_CODE', $('#taxScheduleCode').val());
		validator.setValue('TAX_ON', $('#taxOn').val());
		validator.setClass('patterns.config.web.forms.cmn.mtaxbean');
		validator.setMethod('validateTaxScheduleCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('taxScheduleCode_error', validator.getValue(ERROR));
			return false;
		}
		setFocusLastOnGridSubmit('tax_innerGrid');
		return true;
	}
}

function prdctWiseTaxInclusive_val() {
	setFocusLast('cstmerWiseTaxInclusive');
	return true;
}

function cstmerWiseTaxInclusive_val() {
	setFocusLast('leaseWiseTaxInclusive');
	return true;
}

function leaseWiseTaxInclusive_val() {
	if ($('#action').val() == ADD) {
		setFocusLast('remarks');
	} else {
		setFocusLast('enabled');
	}
	return true;
}

function enabled_val() {
	if ($('#action').val() == MODIFY) {
		setFocusLast('remarks');
	}
	return true;
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', INVALID_REMARKS);
			setFocusLast('remarks');
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', INVALID_REMARKS);
			setFocusLast('remarks');
			return false;
		}
	}
	if ($('#action').val() == MODIFY) {
		if (isEmpty(value)) {
			setError('remarks_error', MANDATORY);
			setFocusLast('remarks');
			return false;
		}
	}
	setFocusOnSubmit();
	return true;
}

function tax_addGrid(editMode, editRowId) {
	if ($('#taxOn').val() != '4') {
		if (taxScheduleCode_val(false)) {
			if (!editModeFlag) {
				if (isNumberGreaterThanEqual(tax_innerGrid.getRowsNum(), $('#noOfTaxSchedules').val())) {
					setError('taxScheduleCode_error', TAX_SCH_GREATER + ' ' + $('#noOfTaxSchedules').val());
					return false;
				}
			}
			if (!tax_gridDuplicateCheck(editMode, editRowId)) {
				setError('taxScheduleCode_error', DUPLICATE_DATA);
				$('#taxScheduleCode_desc').html(EMPTY_STRING);
				return false;
			}
			var field_values = [ 'false', $('#taxScheduleCode').val() ];
			_grid_updateRow(tax_innerGrid, field_values);
			if (!editModeFlag) {
				if (tax_innerGrid.getRowsNum() == $('#noOfTaxSchedules').val()) {
					setFocusLast('prdctWiseTaxInclusive');
				} else {
					setFocusLast('taxScheduleCode');
				}
			}
			tax_clearGridFields();
			editModeFlag = false;
			return true;
		} else {
			return false;
		}
	}
}

function tax_gridDuplicateCheck(editMode, editRowId) {
	var currentValue = [ $('#taxScheduleCode').val() ];
	return _grid_duplicate_check(tax_innerGrid, currentValue, [ 1 ]);
}

function tax_editGrid(rowId) {
	editModeFlag = true;
	$('#taxScheduleCode').val(tax_innerGrid.cells(rowId, 1).getValue());
	$('#taxScheduleCode').focus();
}

function tax_cancelGrid(rowId) {
	tax_clearGridFields();
}

function tax_clearGridFields() {
	$('#taxScheduleCode').val(EMPTY_STRING);
	$('#taxScheduleCode_error').html(EMPTY_STRING);
}

function gridExit(id) {
	switch (id) {
	case 'taxScheduleCode':
		$('#xmlTax').val(tax_innerGrid.serialize());
		tax_clearGridFields();
		setFocusLast('prdctWiseTaxInclusive');
		break;
	}
}

function gridDeleteRow(id) {
	switch (id) {
	case 'taxScheduleCode':
		deleteGridRecord(tax_innerGrid);
		break;
	}
}

function revalidate() {
	tax_revaildateGrid();
}

function tax_revaildateGrid() {
	if ($('#taxOn').val() == '4' && tax_innerGrid.getRowsNum() == 1) {
		$('#xmlTax').val(tax_innerGrid.serialize());
	} else if ($('#noOfTaxSchedules').val() == ONE && tax_innerGrid.getRowsNum() == 1) {
		$('#xmlTax').val(tax_innerGrid.serialize());
	} else if ($('#taxOn').val() == '4' && tax_innerGrid.getRowsNum() > 1 && $('#noOfTaxSchedules').val() == ONE) {
		$('#xmlTax').val(tax_innerGrid.serialize());
		setError('taxScheduleCode_error', GRID_MUST_EMPTY);
		errors++;
	} else if ($('#taxOn').val() != '4' && $('#noOfTaxSchedules').val() != ONE && tax_innerGrid.getRowsNum() > 0) {
		$('#xmlTax').val(tax_innerGrid.serialize());
		$('#taxScheduleCode_error').html(EMPTY_STRING);
	} else {
		tax_innerGrid.clearAll();
		$('#xmlTax').val(tax_innerGrid.serialize());
		setError('taxScheduleCode_error', ADD_ATLEAST_ONE_ROW);
		errors++;
	}
	if (tax_innerGrid.getRowsNum() != $('#noOfTaxSchedules').val() && $('#taxOn').val() != '4' && $('#noOfTaxSchedules').val() != ONE) {
		setError('taxScheduleCode_error', TAX_SCHEDULE_GRID_VALUE_SAME + " " + $('#noOfTaxSchedules').val());
		errors++;
	}
}
