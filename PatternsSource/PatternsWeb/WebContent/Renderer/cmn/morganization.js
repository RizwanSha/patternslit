var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MORGANIZATION';
function init() {
	refreshTBAGrid();
	refreshQueryGrid();
	if (_redisplay) {
		if ($('#action').val() == ADD) {
			$('#enabled').prop('disabled', true);
			setCheckbox('enabled', YES);
		}
	}
}
function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'MORGANIZATION_MAIN');
}

function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}

function doclearfields(id) {
	switch (id) {
	case 'organizationCode':
		if (isEmpty($('#organizationCode').val())) {
			$('#organizationCode_error').html(EMPTY_STRING);
			$('#organizationCode_desc').html(EMPTY_STRING);
			clearNonPKFields();
			break;
		}
	}
}

function clearFields() {
	$('#organizationCode').val(EMPTY_STRING);
	$('#organizationCode_error').html(EMPTY_STRING);
	$('#organizationCode_desc').html(EMPTY_STRING);
	clearNonPKFields();
}

function clearNonPKFields() {
	$('#description').val(EMPTY_STRING);
	$('#description_error').html(EMPTY_STRING);
	$('#conciseDescription').val(EMPTY_STRING);
	$('#conciseDescription_error').html(EMPTY_STRING);
	$('#contactName_title').val(EMPTY_STRING);
	$('#contactName').val(EMPTY_STRING);
	$('#emailID').val(EMPTY_STRING);
	$('#emailID_error').html(EMPTY_STRING);
	$('#address').val(EMPTY_STRING);
	$('#address_error').val(EMPTY_STRING);
	setCheckbox('enabled', YES);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
}

function add() {
	$('#organizationCode').focus();
	setCheckbox('enabled', YES);
	$('#enabled').prop('disabled', true);

}
function modify() {
	$('#organizationCode').focus();
	if ($('#action').val() == MODIFY) {
		$('#enabled').prop('disabled', false);
	} else {
		$('#enabled').prop('disabled', true);
	}
}

function doHelp(id) {
	switch (id) {
	case 'organizationCode':
		help('COMMON', 'HLP_ORG_CODE_M', $('#organizationCode').val(), EMPTY_STRING, $('#organizationCode'));
		break;
	}
}

function loadData() {
	$('#organizationCode').val(validator.getValue('ORG_CODE'));
	$('#description').val(validator.getValue('DESCRIPTION'));
	$('#conciseDescription').val(validator.getValue('CONCISE_DESCRIPTION'));
	$('#contactName_title').val(validator.getValue('CONTACT_PERSON_TITLE'));
	$('#contactName').val(validator.getValue('CONTACT_PERSON_NAME'));
	$('#emailID').val(validator.getValue('CONTACT_EMAIL_ID'));
	$('#address').val(validator.getValue('ADDRESS'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	resetLoading();
}

function view(source, primaryKey) {
	hideParent('cmn/qorganization', source, primaryKey);
	resetLoading();
}

function validate(id) {

	switch (id) {
	case 'organizationCode':
		organizationCode_val();
		break;
	case 'description':
		description_val();
		break;
	case 'conciseDescription':
		conciseDescription_val();
		break;
	case 'contactName_title':
		contactName_title();
		break;
	case 'contactName':
		contactName_val();
		break;
	case 'emailID':
		emailID_val();
		break;
	case 'address':
		address_val();
		break;
	case 'enabled':
		enabled_val();
		break;
	case 'remarks':
		remarks_val();
		break;
	}
}

function backtrack(id) {
	switch (id) {
	case 'organizationCode':
		setFocusLast('organizationCode');
		break;
	case 'description':
		setFocusLast('organizationCode');
		break;
	case 'conciseDescription':
		setFocusLast('description');
		break;
	case 'contactName_title':
		setFocusLast('conciseDescription');
		break;
	case 'contactName':
		setFocusLast('contactName_title');
		break;
	case 'emailID':
		setFocusLast('contactName');
		break;
	case 'address':
		setFocusLast('emailID');
		break;
	case 'enabled':
		setFocusLast('address');
		break;
	case 'remarks':
		if ($('#action').val() == MODIFY) {
			setFocusLast('enabled');
		} else {
			setFocusLast('address');
		}
		break;
	}
}

function organizationCode_val() {
	var value = $('#organizationCode').val();
	clearError('organizationCode_error');
	if (isEmpty(value)) {
		setError('organizationCode_error', MANDATORY);
		return false;
	} else if ($('#action').val() == ADD && !isValidCode(value)) {
		setError('organizationCode_error', INVALID_FORMAT);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('ORG_CODE', $('#organizationCode').val());
		validator.setValue(ACTION, $('#action').val());
		validator.setClass('patterns.config.web.forms.cmn.morganizationbean');
		validator.setMethod('validateOrganizationCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('organizationCode_error', validator.getValue(ERROR));
			$('#organizationCode_desc').html(EMPTY_STRING);
			return false;
		} else {
			if ($('#action').val() == MODIFY) {
				PK_VALUE = getEntityCode() + PK_SEPERATOR + $('#organizationCode').val();
				if (!loadPKValues(PK_VALUE, 'organizationCode_error')) {
					return false;
				}
			}
		}
	}
	setFocusLast('description');
	return true;
}

function description_val() {
	var value = $('#description').val();
	clearError('description_error');
	if (!isValidDescription(value)) {
		setError('description_error', INVALID_DESCRIPTION);
		return false;
	}
	setFocusLast('conciseDescription');
	return true;
}

function conciseDescription_val() {
	var value = $('#conciseDescription').val();
	clearError('conciseDescription_error');
	if (!isValidConciseDescription(value)) {
		setError('conciseDescription_error', INVALID_CONCISE_DESCRIPTION);
		return false;
	}
	setFocusLast('contactName_title');
	return true;
}
function contactName_title() {
	setFocusLast('contactName');
	return true;
}

function contactName_val() {
	var value = $('#contactName').val();
	clearError('contactName_error');
	if (isEmpty(value)) {
		setError('contactName_error', MANDATORY);
		return false;
	}
	if (!isValidName(value)) {
		setError('contactName_error',INVALID_NAME);
		return false;
	}
	setFocusLast('emailID');
	return true;
}

function emailID_val() {
	var value = $('#emailID').val();
	clearError('emailID_error');
	if (isEmpty(value)) {
		setError('emailID_error', MANDATORY);
		return false;
	}
	if (!isEmail(value)) {
		setError('emailID_error', INVALID_EMAIL_ID);
		return false;
	}
	setFocusLast('address');
	return true;
}

function address_val() {
	var value = $('#address').val();
	clearError('address_error');
	if (isEmpty(value)) {
		setError('address_error', MANDATORY);
		return false;
	} else if (isWhitespace(value)) {
		setError('address_error', INVALID_ADDRESS);
		return false;
	} else if (!isValidRemarks(value)) {
		setError('address_error', INVALID_ADDRESS);
		return false;
	}
	setFocusLast('remarks');
	return true;
}

function enabled_val() {
	if ($('#action').val() == MODIFY) {
		setFocusLast('remarks');
	}
	return true;
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', INVALID_REMARKS);
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', INVALID_REMARKS);
			return false;
		}
	}
	if ($('#action').val() == MODIFY) {
		if (isEmpty(value)) {
			setError('remarks_error', MANDATORY);
			return false;
		}
	}
	setFocusOnSubmit();
	return true;
}
