
<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.cmn.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/cmn/itaxbyuslease.js" />
	</web:dependencies>
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="itaxbyuslease.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/cmn/itaxbyuslease" id="itaxbyuslease" method="POST">
				<web:dividerBlock>
					<type:optionBox add="true" modify="true" view="true" />
				</web:dividerBlock>
				<web:viewContent id="po_view1" styleClass="hidden">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle width="180px" />
										<web:columnStyle width="100px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="itaxbyuslease.lesseeCode" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:productCode property="lesseeCode" id="lesseeCode" />
										</web:column>
										<web:column>
											<web:legend key="itaxbyuslease.customerid" var="program" />
										</web:column>
										<web:column>
											<type:customerID property="customerId" id="customerId" readOnly="true" />
										</web:column>
									</web:rowEven>
									
									<web:rowOdd>
										<web:column>
											<web:legend key="itaxbyuslease.agreementnumber" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:agreementNo property="agreementNumber" id="agreementNumber" />
										</web:column>
										<web:column>
											<web:legend key="itaxbyuslease.name" var="program" />
										</web:column>
										<web:column>
											<type:name property="customerName" id="customerName" readOnly="true" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="itaxbyuslease.scheduleid" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:scheduleID property="scheduleId" id="scheduleId" lookup="true"/>
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="itaxbyuslease.statecode" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:stateCode property="stateCode" id="stateCode" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="itaxbyuslease.taxcode" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:taxCode property="taxCode" id="taxCode" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="itaxbyuslease.effectivedate" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:date property="effectiveDate" id="effectiveDate" lookup="true" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="itaxbyuslease.taxborne" var="program" />
										</web:column>
										<web:column>
											<type:checkbox property="taxBorne" id="taxBorne" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="itaxbyuslease.accountinghead" var="program" />
										</web:column>
										<web:column>
											<type:GLHeadCode property="accountingHead" id="accountingHead" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="form.IsUseEnabled" var="common" />
										</web:column>
										<web:column>
											<type:checkbox property="enabled" id="enabled" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="form.remarks" var="common" />
										</web:column>
										<web:column>
											<type:remarks property="remarks" id="remarks" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column span="2">
											<web:submitReset />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
						</web:sectionBlock>
					</web:dividerBlock>
				</web:viewContent>
				<web:element property="command" />
				<web:element property="action" />
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
	<web:queryGrid />
	<web:TBAGrid />
	<web:contextSearch />
</web:fragment>
