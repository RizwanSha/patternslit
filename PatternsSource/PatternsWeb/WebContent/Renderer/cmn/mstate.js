var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MSTATE';
function init() {
	refreshTBAGrid();
	refreshQueryGrid();
	if (_redisplay) {
		if ($('#action').val() == ADD) {
			$('#enabled').prop('disabled', true);
			setCheckbox('enabled', YES);
		}

	}
}
function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'MSTATE_MAIN', queryGrid);
}
function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}
function clearFields() {
	$('#stateCode').val(EMPTY_STRING);
	$('#stateCode_desc').html(EMPTY_STRING);
	$('#stateCode_error').html(EMPTY_STRING);
	clearNonPKFields();
}
function clearNonPKFields() {
	$('#name').val(EMPTY_STRING);
	$('#name_error').html(EMPTY_STRING);
	$('#shortName').val(EMPTY_STRING);
	$('#shortName_error').html(EMPTY_STRING);
	$('#stateOrUnionTerritory').val($("#stateOrUnionTerritory option:first-child").val());
	$('#stateOrUnionTerritory_error').html(EMPTY_STRING);
	$('#oldStateCodeSplitOnDate').val(EMPTY_STRING);
	$('#oldStateCodeSplitOnDate_error').html(EMPTY_STRING);
	$('#newStateFormationDate').val(EMPTY_STRING);
	$('#newStateFormationDate_error').html(EMPTY_STRING);
	$('#invoiceType').prop('selectedIndex', 0);
	$('#invoiceType_error').html(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
}
function doclearfields(id) {
	switch (id) {
	case 'stateCode':
		if (isEmpty($('#stateCode').val())) {
			clearFields();
			break;
		}
	}
}
function add() {
	$('#stateCode').focus();
	setCheckbox('enabled', YES);
	$('#enabled').prop('disabled', true);
}
function modify() {
	$('#stateCode').focus();
	if ($('#action').val() == MODIFY) {
		$('#enabled').prop('disabled', false);
	} else {
		$('#enabled').prop('disabled', true);
	}
}
function view(source, primaryKey) {
	hideParent('cmn/qstate', source, primaryKey);
	resetLoading();
}
function loadData() {
	$('#stateCode').val(validator.getValue('STATE_CODE'));
	$('#name').val(validator.getValue('DESCRIPTION'));
	$('#shortName').val(validator.getValue('CONCISE_DESCRIPTION'));
	$('#stateOrUnionTerritory').val(validator.getValue('STATE_UT'));
	$('#oldStateCodeSplitOnDate').val(validator.getValue('OLD_STATE_SPLIT_ON'));
	$('#newStateFormationDate').val(validator.getValue('NEW_STATE_FORMATION_ON'));
	$('#invoiceType').val(validator.getValue('INVOICE_TYPE'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	setFocusLast('stateCode');
	resetLoading();
}
function backtrack(id) {
	switch (id) {
	case 'stateCode':
		setFocusLast('stateCode');
		break;
	case 'name':
		setFocusLast('stateCode');
		break;
	case 'shortName':
		setFocusLast('name');
		break;
	case 'stateOrUnionTerritory':
		setFocusLast('shortName');
		break;
	case 'oldStateCodeSplitOnDate':
		setFocusLast('stateOrUnionTerritory');
		break;
	case 'newStateFormationDate':
		setFocusLast('oldStateCodeSplitOnDate');
		break;
	case 'invoiceType':
		setFocusLast('newStateFormationDate');
		break;
	case 'enabled':
		setFocusLast('invoiceType');
		break;
	case 'remarks':
		if ($('#action').val() == MODIFY) {
			setFocusLast('enabled');
		} else {
			setFocusLast('invoiceType');
		}
		break;
	}
}
function doHelp(id) {
	switch (id) {
	case 'stateCode':
		help('COMMON', 'HLP_STATE_CODE', $('#stateCode').val(), EMPTY_STRING, $('#stateCode'));
		break;
	}
}
function validate(id) {
	switch (id) {
	case 'stateCode':
		stateCode_val();
		break;
	case 'name':
		name_val();
		break;
	case 'shortName':
		shortName_val();
		break;
	case 'stateOrUnionTerritory':
		stateOrUnionTerritory_val();
		break;
	case 'oldStateCodeSplitOnDate':
		oldStateCodeSplitOnDate_val();
		break;
	case 'newStateFormationDate':
		newStateFormationDate_val();
		break;
	case 'invoiceType':
		invoiceTypeForState_val();
		break;
	case 'enabled':
		enabled_val();
		break;
	case 'remarks':
		remarks_val();
		break;
	}
}
function stateCode_val() {
	var value = $('#stateCode').val();
	clearError('stateCode_error');
	if (isEmpty(value)) {
		setError('stateCode_error', PBS_MANDATORY);
		return false;
	} else if ($('#action').val() == ADD && !isValidCode(value)) {
		setError('stateCode_error', INVALID_FORMAT);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('STATE_CODE', $('#stateCode').val());
		validator.setValue(ACTION, $('#action').val());
		validator.setClass('patterns.config.web.forms.cmn.mstatebean');
		validator.setMethod('validateStateCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('stateCode_error', validator.getValue(ERROR));
			return false;
		} else {
			if ($('#action').val() == MODIFY) {
				PK_VALUE = getEntityCode() + PK_SEPERATOR + $('#stateCode').val();
				if (!loadPKValues(PK_VALUE, 'stateCode_error')) {
					return false;
				}
				setFocusLast('name');
				return true;
			}
		}
	}
	setFocusLast('name');
	return true;
}
function name_val() {
	var value = $('#name').val();
	clearError('name_error');
	if (isEmpty(value)) {
		setError('name_error', MANDATORY);
		return false;
	}
	if (!isValidDescription(value)) {
		setError('name_error', INVALID_DESCRIPTION);
		return false;
	} else {

		setFocusLast('shortName');
	}
	return true;
}
function shortName_val() {
	var value = $('#shortName').val();
	clearError('shortName_error');
	if (isEmpty(value)) {
		setError('shortName_error', MANDATORY);
		return false;
	}
	if (!isValidConciseDescription(value)) {
		setError('shortName_error', INVALID_DESCRIPTION);
		return false;
	} else {
		setFocusLast('stateOrUnionTerritory');
	}
	return true;
}
function stateOrUnionTerritory_val() {
	var value = $('#stateOrUnionTerritory').val();
	clearError('stateOrUnionTerritory_error');
	if (isEmpty(value)) {
		setError('stateOrUnionTerritory_error', MANDATORY);
		return false;
	}
	setFocusLast('oldStateCodeSplitOnDate');
	return true;
}
function oldStateCodeSplitOnDate_val() {
	var value = $('#oldStateCodeSplitOnDate').val();
	clearError('oldStateCodeSplitOnDate_error');

	if (!isEmpty(value)) {
		if (!isDate(value)) {
			setError('oldStateCodeSplitOnDate_error', INVALID_DATE);
			return false;
		}
		if (isDateGreaterEqual(value, getCBD())) {
			setError('oldStateCodeSplitOnDate_error', GREATER_DATE);
			return false;
		}
	}

	setFocusLast('newStateFormationDate');

	return true;
}
function newStateFormationDate_val() {
	var value = $('#newStateFormationDate').val();
	clearError('newStateFormationDate_error');
	if (!isEmpty(value)) {
		if (!isDate(value)) {
			setError('newStateFormationDate_error', INVALID_DATE);
			return false;
		}
		if (isDateGreaterEqual(value, getCBD())) {
			setError('newStateFormationDate_error', GREATER_DATE);
			return false;
		}
		if (isDateLesserEqual(value, $('#oldStateCodeSplitOnDate').val())) {
			setError('newStateFormationDate_error', GREATER_OLD_SPLIT_DATE);
			return false;
		}
	}
	setFocusLast('invoiceType');
	return true;
}
function invoiceTypeForState_val() {
	var value = $('#invoiceType').val();
	clearError('invoiceType_error');
	if (isEmpty(value)) {
		setError('invoiceType_error', MANDATORY);
		return false;
	}
	if ($('#action').val() == MODIFY) {
		setFocusLast('enabled');
	} else {
		setFocusLast('remarks');
		return true;
	}
}
function enabled_val() {
	if ($('#action').val() == MODIFY) {
		setFocusLast('remarks');
	}
	return true;
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', INVALID_REMARKS);
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', INVALID_REMARKS);
			return false;
		}
	}
	if ($('#action').val() == MODIFY) {
		if (isEmpty(value)) {
			setError('remarks_error', MANDATORY);
			return false;
		}
	}
	setFocusOnSubmit();
	return true;
}
