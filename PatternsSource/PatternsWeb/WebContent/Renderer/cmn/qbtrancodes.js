var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MBTRANCODES';

function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
	trancodes_innerGrid.setColumnHidden(0, true);
}

function loads() {
	loadGridQuery(CURRENT_PROGRAM_ID, 'MBTRANCODES_GRID', trancodes_innerGrid);
}

function clearFields() {
	$('#bankingTranCode').val(EMPTY_STRING);
	$('#description').val(EMPTY_STRING);
	$('#conciseDescription').val(EMPTY_STRING);
	$('#bankingTranAccCode').val(EMPTY_STRING);
	setCheckbox('localCurrTranFlag', NO);
	$('#transactionCode').val(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);	
	trancodes_innerGrid.clearAll();
}

function loadData() {
	$('#bankingTranCode').val(validator.getValue('BTRAN_CODE'));
	$('#description').val(validator.getValue('DESCRIPTION'));
	$('#conciseDescription').val(validator.getValue('CONCISE_DESCRIPTION'));
	$('#bankingTranAccCode').val(validator.getValue('BTRAN_ACCESS_CODE'));
	setCheckbox('localCurrTranFlag', validator.getValue('BTRAN_LC_FLAG'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadGrid();
	loadAuditFields(validator);
}

function loadGrid() {
	SOURCE_VALUE = _tableType;
	PK_VALUE = _primaryKey;
	if (_tableType == MAIN) {
		loads();
	} else if (_tableType == TBA) {
		loads();
	}
}
