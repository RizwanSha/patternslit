var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'ECUSTGEOALLOC';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}

function loadQuery() {
	loadGridQuery(CURRENT_PROGRAM_ID, 'ECUSTGEOALLOC_GRID', ecustgeoalloc_innerGrid);
	ecustgeoalloc_innerGrid.setColumnsVisibility("true,false,false,false,false");

}

function clearFields() {
	$('#cusotmerIdFoNo').val(EMPTY_STRING);
	$('#cusotmerIdFoNo_error').html(EMPTY_STRING);
	$('#effectiveDate').val(EMPTY_STRING);
	$('#effectiveDate_error').html(EMPTY_STRING);
}

function loadData() {
	$('#cusotmerIdFoNo').val(validator.getValue('CIF_NO'));
	$('#cusotmerIdFoNo_desc').html(validator.getValue('F1_NAME'));
	$('#address').val(validator.getValue('F1_ADDRESS'));
	$('#pincode').val(validator.getValue('F1_GEO_UNIT_ID'));
	$('#effectiveDate').val(validator.getValue('EFF_DATE'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadAuditFields(validator);
	loadGrid();
}

function loadGrid() {
	SOURCE_VALUE = _tableType;
	PK_VALUE = _primaryKey;
	if (_tableType == MAIN) {
		loadQuery();

	} else if (_tableType == TBA) {
		loadQuery();
	}
}
