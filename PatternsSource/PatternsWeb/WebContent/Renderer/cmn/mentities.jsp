<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.cmn.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/cmn/mentities.js" />
	</web:dependencies>
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="mentities.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/cmn/mentities" id="mentities" method="POST">
				<web:dividerBlock>
					<type:optionBox add="false" modify="true" view="true" />
				</web:dividerBlock>
				<web:viewContent id="po_view1" styleClass="hidden">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="210px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="mentities.entitycode" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:entityCode property="entityCode" id="entityCode" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="210px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="form.name" var="common" mandatory="true" />
										</web:column>
										<web:column>
											<type:name property="name" id="name" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="210px" />
										<web:columnStyle width="210px" />
										<web:columnStyle width="210px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="mentities.defaultlancode" var="program" />
										</web:column>
										<web:column>
											<type:textDescription property="defaultLanCode" id="defaultLanCode" length="6" size="15" />
										</web:column>
										<web:column>
											<web:legend key="mentities.defaultdateformat" var="program" />
										</web:column>
										<web:column>
											<type:textDescription property="defaultDateFormat" id="defaultDateFormat" length="10" size="15" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="210px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="mentities.dateofincorporation" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:date property="dateOfIncorporation" id="dateOfIncorporation" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="mentities.banklicensetype" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:code property="bankLicenseType" id="bankLicenseType" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="mentities.basecurrcode" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:currency property="baseCurrCode" id="baseCurrCode" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="mentities.baseaddrtype" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:addressTypeCode property="baseAddrType" id="baseAddrType" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="mentities.countrycode" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:countryCode property="countryCode" id="countryCode" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="210px" />
										<web:columnStyle width="210px" />
										<web:columnStyle width="210px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="mentities.finyear" var="program" />
										</web:column>
										<web:column>
											<type:combo property="finYear" id="finYear" datasourceid="COMMON_MONTHS" />
										</web:column>
										<web:column>
											<web:legend key="mentities.weekstartsfrom" var="program" />
										</web:column>
										<web:column>
											<type:combo property="weekStartsFrom" id="weekStartsFrom" datasourceid="COMMON_WEEK_DAY" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="210px" />
										<web:columnStyle width="210px" />
										<web:columnStyle width="210px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="mentities.hasbranches" var="program" />
										</web:column>
										<web:column>
											<type:checkbox property="hasBranches" id="hasBranches" />
										</web:column>
										<web:column>
											<web:legend key="mentities.lengthbranchcode" var="program" />
										</web:column>
										<web:column>
											<type:twoDigit property="lengthBranchCode" id="lengthBranchCode" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="210px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="mentities.multicuraccount" var="program" />
										</web:column>
										<web:column>
											<type:checkbox property="multiCurAccount" id="multiCurAccount" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="mentities.geounitstatecode" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:geographicalUnitId property="geoUnitStateCode" id="geoUnitStateCode" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="mentities.geounitdistrictcode" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:geographicalUnitId property="geoUnitDistrictCode" id="geoUnitDistrictCode" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="mentities.geounitpincode" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:geographicalUnitId property="geoUnitPinCode" id="geoUnitPinCode" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="210px" />
										<web:columnStyle width="210px" />
										<web:columnStyle width="210px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="mentities.entitydissolved" var="program" />
										</web:column>
										<web:column>
											<type:checkbox property="entityDissolved" id="entityDissolved" />
										</web:column>
										<web:column>
											<web:legend key="mentities.dissolvedon" var="program" />
										</web:column>
										<web:column>
											<type:textDescription property="dissolvedOn" id="dissolvedOn" length="10" size="15" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="210px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="form.remarks" var="common" />
										</web:column>
										<web:column>
											<type:remarks property="remarks" id="remarks" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column span="2">
											<web:submitReset />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
						</web:sectionBlock>
					</web:dividerBlock>
				</web:viewContent>
				<web:element property="command" />
				<web:element property="action" />
				<web:element property="w_version" />
				<web:element property="w_add_struc_reqd" />
				<web:element property="w_country_struc" />
				<web:element property="hiddenhas" />
				<web:element property="fin" />
				<web:element property="weekStarts" />
				<web:element property="beforeNoOfDigits" />
				<web:element property="multiCur" />
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
	<web:queryGrid />
	<web:TBAGrid />
	<web:contextSearch />
</web:fragment>
