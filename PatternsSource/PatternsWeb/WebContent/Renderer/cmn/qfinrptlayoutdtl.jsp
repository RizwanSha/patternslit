<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<web:bundle baseName="patterns.config.web.forms.cmn.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/cmn/qfinrptlayoutdtl.js" />
	</web:dependencies>
	<web:loadPK />
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="qfinrptlayoutdtl.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:viewContent id="po_view1">
				<web:dividerBlock>
					<web:sectionBlock width="width-100p" align="left">
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="mfinrptlayoutdtl.reportlayoutcode" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:reportLayoutCodeDisplay property="reportLayoutCode" id="reportLayoutCode" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle width="240px" />
									<web:columnStyle width="200px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowOdd>
									<web:column>
										<web:legend key="mfinrptlayoutdtl.sectionsl" var="program" />
									</web:column>
									<web:column>
										<type:comboDisplay property="sectionSl" id="sectionSl" datasourceid="COMMON_NOF_SECTIONS" />
									</web:column>
									<web:column>
										<web:legend key="mfinrptlayoutdtl.accntngtypeallowed" var="program" />
									</web:column>
									<web:column>
										<type:comboDisplay property="accntngTypeAllowed" id="accntngTypeAllowed" datasourceid="COMMON_ACCT_TYPE" />
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle width="240px" />
									<web:columnStyle width="200px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="form.effectivedate" var="common" mandatory="true" />
									</web:column>
									<web:column>
										<type:dateDisplay property="effectiveDate" id="effectiveDate" />
									</web:column>
									<web:column>
										<web:legend key="mfinrptlayoutdtl.reportfor" var="program" />
									</web:column>
									<web:column>
										<type:comboDisplay property="reportFor" id="reportFor" datasourceid="COMMON_ELE_BELONGS_TO" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:viewTitle var="program" key="mfinrptlayoutdtl.section" />
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:rowOdd>
									<web:column>
										<web:grid height="240px" width="381px" id="finrptlayoutdtl_innerGrid" src="cmn/mfinrptlayoutdtl_innerGrid.xml">
										</web:grid>
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="form.IsUseEnabled" var="common" />
									</web:column>
									<web:column>
										<type:checkboxDisplay property="enabled" id="enabled" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="form.remarks" var="common" />
									</web:column>
									<web:column>
										<type:remarksDisplay property="remarks" id="remarks" />
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
						<web:auditDisplay />
					</web:sectionBlock>
				</web:dividerBlock>
			</web:viewContent>
		</web:viewSubPart>
	</web:viewPart>
</web:fragment>
