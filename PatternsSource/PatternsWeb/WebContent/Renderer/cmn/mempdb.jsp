<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.cmn.Messages"
	var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/cmn/mempdb.js" />
	</web:dependencies>
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="mempdb.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/cmn/mempdb" id="mempdb" method="POST">
				<web:dividerBlock>
					<type:optionBox add="true" modify="true" view="true" />
				</web:dividerBlock>
				<web:viewContent id="po_view1" styleClass="hidden">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="mempdb.empcode" var="program"
												mandatory="true" />
										</web:column>
										<web:column>
											<type:employeeCode property="employeeCode" id="employeeCode" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="mempdb.name" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:generalTitleName property="employee" id="employee"
												datasourceid_title="COMMON_TITLE" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="mempdb.gender" var="program"
												mandatory="true" />
										</web:column>
										<web:column>
											<type:combo property="gender" id="gender"
												datasourceid="COMMON_GENDER" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="mempdb.dateofbirth" var="program"
												mandatory="true" />
										</web:column>
										<web:column>
											<type:date property="dateOfBirth" id="dateOfBirth" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="mempdb.relationship" var="program"
												mandatory="true" />
										</web:column>
										<web:column>
											<type:combo property="relationShip" id="relationShip"
												datasourceid="COMMON_RELATION_SHIP" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="mempdb.relationname" var="program"
												mandatory="true" />
										</web:column>
										<web:column>
											<type:description property="relationName" id="relationName" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="mempdb.dateofjoining" var="program"
												mandatory="true" />
										</web:column>
										<web:column>
											<type:date property="dateOfJoining" id="dateOfJoining" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="mempdb.dateofretirement" var="program" />
										</web:column>
										<web:column>
											<type:date property="dateOfRetirement" id="dateOfRetirement" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="mempdb.pfavailable" var="program" />
										</web:column>
										<web:column>
											<type:checkbox property="provideFundApplicable"
												id="provideFundApplicable" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="mempdb.pfnumber" var="program" />
										</web:column>
										<web:column>
											<type:otherInformation25 property="pfNumber" id="pfNumber" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="mempdb.personfundrequired" var="program" />
										</web:column>
										<web:column>
											<type:checkbox property="personFundRequired"
												id="personFundRequired" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="mempdb.eligibleesi" var="program" />
										</web:column>
										<web:column>
											<type:checkbox property="eligibleEsi" id="eligibleEsi" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="mempdb.employeeesinumber" var="program" />
										</web:column>
										<web:column>
											<type:otherInformation25 property="esiNumber" id="esiNumber" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="mempdb.qualification" var="program"
												mandatory="true" />
										</web:column>
										<web:column>
											<type:description property="qualification" id="qualification" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="mempdb.emaild" var="program" />
										</web:column>
										<web:column>
											<type:email property="emailId" id="emailId" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="mempdb.mobileno" var="program" />
										</web:column>
										<web:column>
											<type:mobile property="mobileNumber" id="mobileNumber" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="form.remarks" var="common" />
										</web:column>
										<web:column>
											<type:remarks property="remarks" id="remarks" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column span="2">
											<web:submitReset />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
						</web:sectionBlock>
					</web:dividerBlock>
				</web:viewContent>
				<web:element property="command" />
				<web:element property="action" />
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
	<web:queryGrid />
	<web:TBAGrid />
	<web:contextSearch />
</web:fragment>
