var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'IENTITIES';
function init() {
	$('#entityId_pic').remove();
	refreshTBAGrid();
	refreshQueryGrid();
	if (_redisplay) {
	} else {
		domodify();
	}

}
function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'IENTITIES_MAIN', queryGrid);
}
function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}
function clearFields() {
	clearNonPKFields();
}
function clearNonPKFields() {
	$('#entityId').val(EMPTY_STRING);
	$('#entityId_error').html(EMPTY_STRING);
	$('#entityName').val(EMPTY_STRING);
	$('#entityName_error').html(EMPTY_STRING);
	$('#regOfficeAdd').val(EMPTY_STRING);
	$('#regOfficeAdd_error').html(EMPTY_STRING);
	$('#pinCode').val(EMPTY_STRING);
	$('#pinCode_error').html(EMPTY_STRING);
	$('#stateCode').val(EMPTY_STRING);
	$('#stateCode_error').html(EMPTY_STRING);
	$('#panCard').val(EMPTY_STRING);
	$('#panCard_error').html(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
}

function modify() {
	$('#entityId').focus();
	PK_VALUE = getEntityCode() + PK_SEPERATOR + $('#entityId').val();
	if (!loadPKValues(PK_VALUE, 'entityId_error')) {
		setError('entityId_error', RECORD_NOT_THERE);
		return false;
	}
	setFocusLast('entityId');
	return true;
}

function view(source, primaryKey) {
	hideParent('cmn/qentities', source, primaryKey);
	resetLoading();
}
function loadData() {
	$('#entityId_pic').remove();
	$('#entityId').val(validator.getValue('ENTITY_ID'));
	$('#entityName').val(validator.getValue('ENTITY_NAME'));
	$('#regOfficeAdd').val(validator.getValue('REG_ADDRESS'));
	$('#pinCode').val(validator.getValue('PIN_CODE'));
	$('#stateCode').val(validator.getValue('STATE_CODE'));
	$('#stateCode_desc').html(validator.getValue('F2_DESCRIPTION'));
	$('#panCard').val(validator.getValue('PAN_NO'));
	$('#remarks').val(validator.getValue('REMARKS'));
	setFocusLast('entityId');
	resetLoading();
}
function backtrack(id) {
	switch (id) {
	case 'entityId':
		setFocusLast('entityId');
		break;
	case 'entityName':
		setFocusLast('entityId');
		break;
	case 'regOfficeAdd':
		setFocusLast('entityName');
		break;
	case 'pinCode':
		setFocusLast('regOfficeAdd');
		break;
	case 'stateCode':
		setFocusLast('pinCode');
		break;
	case 'panCard':
		setFocusLast('stateCode');
		break;
	case 'remarks':
		setFocusLast('panCard');
		break;
	}
}
function doHelp(id) {
	switch (id) {
	case 'stateCode':
		help('COMMON', 'HLP_STATE_CODE', $('#stateCode').val(), EMPTY_STRING, $('#stateCode'));
		break;
	}
}
function validate(id) {
	switch (id) {
	case 'entityId':
		entityId_val();
		break;
	case 'entityName':
		entityName_val();
		break;
	case 'regOfficeAdd':
		regOfficeAdd_val();
		break;
	case 'pinCode':
		pinCode_val();
		break;
	case 'stateCode':
		stateCode_val();
		break;
	case 'panCard':
		panCard_val();
		break;
	case 'remarks':
		remarks_val();
		break;
	}
}

function entityId_val() {
	var value = $('#entityId').val();
	clearError('entityId_error');
	if (isEmpty(value)) {
		setError('entityId_error', MANDATORY);
		return false;
	} else {
		setFocusLast('entityName');
	}
	return true;
}
function entityName_val() {
	var value = $('#entityName').val();
	clearError('entityName_error');
	if (isEmpty(value)) {
		setError('entityName_error', MANDATORY);
		return false;
	} else {
		setFocusLast('regOfficeAdd');
	}
	return true;
}
function regOfficeAdd_val() {
	var value = $('#regOfficeAdd').val();
	clearError('regOfficeAdd_error');
	if (isEmpty(value)) {
		setError('regOfficeAdd_error', MANDATORY);
		return false;
	} else {
		setFocusLast('pinCode');
	}
	return true;
}
function pinCode_val() {
	var value = $('#pinCode').val();
	clearError('pinCode_error');
	if (isEmpty(value)) {
		setError('pinCode_error', MANDATORY);
		return false;
	}
	if (!isNumeric(value)) {
		setError('pinCode_error', NUMERIC_CHECK);
		return false;
	}
	if (isNegative(value)) {
		setError('pinCode_error', POSITIVE_CHECK);
		return false;
	}
	if (!isLength(value, 6)) {
		setError('pinCode_error', PINCODE_LENGTH);
		return false;
	}
	setFocusLast('stateCode');
	return true;
}

function stateCode_val() {
	var value = $('#stateCode').val();
	clearError('stateCode_error');
	if (isEmpty(value)) {
		setError('stateCode_error', MANDATORY);
		return false;
	} else if ($('#action').val() == ADD && !isValidCode(value)) {
		setError('stateCode_error', INVALID_FORMAT);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('STATE_CODE', $('#stateCode').val());
		validator.setValue(ACTION, $('#action').val());
		validator.setClass('patterns.config.web.forms.cmn.ientitiesbean');
		validator.setMethod('validateStateCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('stateCode_error', validator.getValue(ERROR));
			return false;
		}
	}
	setFocusLast('panCard');
	return true;
}
function panCard_val() {
	var value = $('#panCard').val();
	clearError('panCard_error');
	if (isEmpty(value)) {
		setError('panCard_error', MANDATORY);
		return false;
	}
	if (!isValidPanNumber(value)) {
		setError('panCard_error', INVALID_PAN_NO);
		return false;
	} else {

		setFocusLast('remarks');
	}
	return true;
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', INVALID_REMARKS);
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', INVALID_REMARKS);
			return false;
		}
	}
	if ($('#action').val() == MODIFY) {
		if (isEmpty(value)) {
			setError('remarks_error', MANDATORY);
			return false;
		}
	}
	setFocusOnSubmit();
	return true;
}
