var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MCPCENTERCODE';
function init() {
	refreshTBAGrid();
	refreshQueryGrid();
	if (_redisplay) {
		if ($('#action').val() == ADD) {
			$('#enabled').prop('disabled', true);
			$('#enabled').prop('checked', true);
		}
		forProd_val();
		forPort_val();
		setFocusLast('costCode');
	}
}
function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'MCPCENTERCODE_MAIN', queryGrid);
}
function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}
function clearFields() {
	$('#costCode').val(EMPTY_STRING);
	$('#costCode_error').html(EMPTY_STRING);
	$('#costCode_desc').html(EMPTY_STRING);
	$('#costCode').focus();
	clearNonPKFields();
}

function clearNonPKFields() {
	$('#description').val(EMPTY_STRING);
	$('#description_error').html(EMPTY_STRING);
	$('#conciseDescription').val(EMPTY_STRING);
	$('#conciseDescription_error').html(EMPTY_STRING);
	$('#profCost').selectedIndex = ZERO;
	$('#profCost_error').html(EMPTY_STRING);
	$("#profCost").prop('selectedIndex', 0);
	setCheckbox('enabled', YES);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
}
function add() {
	setCheckbox('enabled', YES);
	$('#enabled').prop('disabled', true);
}
function modify() {
	$('#costCode').focus();
	$('#enabled').prop('disabled', false);
}
function loadData() {
	$('#costCode').val(validator.getValue('CP_CENTER_CODE'));
	$('#description').val(validator.getValue('DESCRIPTION'));
	$('#conciseDescription').val(validator.getValue('CONCISE_DESCRIPTION'));
	$('#profCost').val(validator.getValue('PROFIT_COST'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	setFocusLast('costCode');
	resetLoading();
}
function view(source, primaryKey) {
	hideParent('cmn/qcpcentercode', source, primaryKey);
	resetLoading();
}

function doHelp(id) {
	switch (id) {
	case 'costCode':
		help('COMMON', 'HLP_CPC_CODE', $('#costCode').val(), EMPTY_STRING, $('#costCode'));
		break;
	}
}

function doclearfields(id) {
	switch (id) {
	case 'costCode':
		if (isEmpty($('#costCode').val())) {
			clearNonPKFields();
			break;
		}
	}
}

function validate(id) {
	switch (id) {
	case 'costCode':
		costCode_val();
		break;
	case 'description':
		description_val();
		break;
	case 'conciseDescription':
		conciseDescription_val();
		break;
	case 'profCost':
		profCost_val();
		break;
	case 'enabled':
		enabled_val();
		break;
	case 'remarks':
		remarks_val();
		break;

	}
}

function backtrack(id) {
	switch (id) {
	case 'costCode':
		setFocusLast('costCode');
		break;
	case 'description':
		setFocusLast('costCode');
		break;
	case 'conciseDescription':
		setFocusLast('description');
		break;
	case 'profCost':
		setFocusLast('conciseDescription');
		break;
	case 'enabled':
		setFocusLast('profCost');
		break;
	case 'remarks':
		if ($('#action').val() == MODIFY) {
			setFocusLast('enabled');
		} else {
			setFocusLast('profCost');
		}
		break;
	}
}

function costCode_val() {
	var value = $('#costCode').val();
	clearError('costCode_error');
	if (isEmpty(value)) {
		setError('costCode_error', HMS_MANDATORY);
		return false;
	} else if ($('#action').val() == ADD && !isValidCode(value)) {
		setError('costCode_error', HMS_INVALID_FORMAT);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('CP_CENTER_CODE', $('#costCode').val());
		validator.setValue(ACTION, $('#action').val());
		validator.setClass('patterns.config.web.forms.cmn.mcpcentercodebean');
		validator.setMethod('validateCostCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('costCode_error', validator.getValue(ERROR));
			return false;
		} else {
			if ($('#action').val() == MODIFY) {
				PK_VALUE = getEntityCode() + PK_SEPERATOR + $('#costCode').val();
				if (!loadPKValues(PK_VALUE, 'costCode_error')) {
					return false;
				}
			}
		}
	}
	setFocusLast('description');
	return true;
}
function description_val() {
	var value = $('#description').val();
	clearError('description_error');
	if (!isValidDescription(value)) {
		setError('description_error', INVALID_DESCRIPTION);
		return false;
	} else {
		setFocusLast('conciseDescription');
	}
	return true;
}

function conciseDescription_val() {
	var value = $('#conciseDescription').val();
	clearError('conciseDescription_error');
	if (isEmpty(value)) {
		setError('conciseDescription_error', HMS_MANDATORY);
		return false;
	}
	if (!isValidShortName(value)) {
		setError('conciseDescription_error', INVALID_CONCISE_DESCRIPTION);
		return false;
	}
	setFocusLast('profCost');
	return true;
}

function profCost_val() {
	var value = $('#profCost').val();
	clearError('profCost_error');
	if (isEmpty(value)) {
		setError('profCost_error', HMS_MANDATORY);
		return false;
	} else {
		if ($('#action').val() == ADD) {
			setFocusLast('remarks');
		} else {
			setFocusLast('enabled');
		}
	}
	return true;
}

function enabled_val() {
	if ($('#action').val() == MODIFY) {
		setFocusLast('remarks');
	}
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', HMS_INVALID_REMARKS);
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', HMS_INVALID_REMARKS);
			return false;
		}
	}
	if ($('#action').val() == MODIFY) {
		if (isEmpty(value)) {
			setError('remarks_error', HMS_MANDATORY);
			return false;
		}

	}
	setFocusOnSubmit();
	return true;
}
