<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.cmn.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/cmn/qgeounitstruc.js" />
	</web:dependencies>
	<web:loadPK />
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="qgeounitstruc.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:viewContent id="po_view1">
				<web:dividerBlock>
					<web:sectionBlock width="width-100p" align="left">
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="180px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="mgeounitstruc.geounitstructurecode" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:geographicalUnitStructureCodeDisplay property="geoUnitStructureCode" id="geoUnitStructureCode" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="180px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowOdd>
									<web:column>
										<web:legend key="form.description" var="common" mandatory="true" />
									</web:column>
									<web:column>
										<type:descriptionDisplay property="description" id="description"  />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="form.conciseDescription" var="common" mandatory="true" />
									</web:column>
									<web:column>
										<type:conciseDescriptionDisplay property="conciseDescription" id="conciseDescription"/>
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="mgeounitstruc.parentgeounitstructureCode" var="program" />
									</web:column>
									<web:column>
										<type:geographicalUnitStructureCodeDisplay property="parentgeoUnitStructureCode" id="parentgeoUnitStructureCode" />
									</web:column>
								</web:rowOdd>

								<web:rowEven>
									<web:column>
										<web:legend key="mgeounitstruc.lasthierarchy" var="program" />
									</web:column>
									<web:column>
										<type:checkboxDisplay property="lastHierarchy" id="lastHierarchy" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
											<web:legend key="mgeounitstruc.requireaunittype" var="program" />
									</web:column>
									<web:column>
											<type:checkboxDisplay property="requireaUnitType" id="requireaUnitType" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="mgeounitstruc.datapatternrequired" var="program" />
									</web:column>
									<web:column>
										<type:checkboxDisplay property="dataPatternRequired" id="dataPatternRequired" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="mgeounitstruc.dataInputPattern" var="program" />
									</web:column>
									<web:column>
										<type:descriptionDisplay property="dataInputPattern" id="dataInputPattern" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="form.IsUseEnabled" var="common" />
									</web:column>
									<web:column>
										<type:checkboxDisplay property="enabled" id="enabled" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="form.remarks" var="common" />
									</web:column>
									<web:column>
										<type:remarksDisplay property="remarks" id="remarks" />
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
						<web:auditDisplay />
					</web:sectionBlock>
				</web:dividerBlock>
			</web:viewContent>
		</web:viewSubPart>
	</web:viewPart>
</web:fragment>