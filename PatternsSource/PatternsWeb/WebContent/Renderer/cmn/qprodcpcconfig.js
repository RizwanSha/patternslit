var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'IPRODCPCCONFIG';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}
function clearFields() {
	$('#productCode').val(EMPTY_STRING);
	$('#productCode_desc').html(EMPTY_STRING);
	$('#portfolioCode').val(EMPTY_STRING);
	$('#portfolioCode_desc').html(EMPTY_STRING);
	$('#effectiveDate').val(EMPTY_STRING);
	$('#costCenterCode').val(EMPTY_STRING);
	$('#costCenterCode_desc').html(EMPTY_STRING);
	$('#profitCenterCode').val(EMPTY_STRING);
	$('#profitCenterCode_desc').html(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
}

function loadData() {
	$('#productCode').val(validator.getValue('LEASE_PRODUCT_CODE'));
	$('#productCode_desc').html(validator.getValue('F1_DESCRIPTION'));
	$('#portfolioCode').val(validator.getValue('PORTFOLIO_CODE'));
	$('#portfolioCode_desc').html(validator.getValue('F2_DESCRIPTION'));
	$('#effectiveDate').val(validator.getValue('EFF_DATE'));
	$('#costCenterCode').val(validator.getValue('COST_CP_CENTER_CODE'));
	$('#costCenterCode_desc').html(validator.getValue('F3_DESCRIPTION'));
	$('#profitCenterCode').val(validator.getValue('PROFIT_CP_CENTER_CODE'));
	$('#profitCenterCode_desc').html(validator.getValue('F4_DESCRIPTION'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadAuditFields(validator);
}
