var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MASSETTYPE';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}

function clearFields() {
	$('#assetTypeCode').val(EMPTY_STRING);
	$('#assetTypeCode_desc').html(EMPTY_STRING);
	$('#description').val(EMPTY_STRING);
	$('#conciseDescription').val(EMPTY_STRING);
	$('#carNonCar').prop('selectedIndex', 0);
	$('#assetModelNumber').prop('selectedIndex', 0);
	$('#assetConfig').prop('selectedIndex', 0);
	$('#remarks').val(EMPTY_STRING);
}
function loadData() {
	$('#assetTypeCode').val(validator.getValue('ASSET_TYPE_CODE'));
	$('#description').val(validator.getValue('DESCRIPTION'));
	$('#conciseDescription').val(validator.getValue('CONCISE_DESCRIPTION'));
	$('#carNonCar').val(validator.getValue('AUTO_NON_AUTO'));
	$('#assetModelNumber').val(validator.getValue('MODEL_NO'));
	$('#assetConfig').val(validator.getValue('CONFIG_DTLS'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadAuditFields(validator);
}
