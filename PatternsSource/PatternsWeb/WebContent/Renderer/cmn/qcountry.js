var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MCOUNTRY';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}
function clearFields() {
	$('#countryCode').val(EMPTY_STRING);
	$('#countryName').val(EMPTY_STRING);
	$('#shortName').val(EMPTY_STRING);
	$('#geoUnitStructureCode').val(EMPTY_STRING);
	$('#geoUnitStructureCode_desc').html(EMPTY_STRING);
	setCheckbox('enabled', NO);
	$('#remarks').val(EMPTY_STRING);
}
function loadData() {
	
	$('#countryCode').val(validator.getValue('COUNTRY_CODE'));
	$('#countryName').val(validator.getValue('DESCRIPTION'));
	$('#shortName').val(validator.getValue('CONCISE_DESCRIPTION'));
	$('#geoUnitStructureCode').val(validator.getValue('GEO_UNIT_STRUC_CODE'));
	$('#geoUnitStructureCode_desc').html(validator.getValue('F1_DESCRIPTION'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	resetLoading();
	loadAuditFields(validator);
}