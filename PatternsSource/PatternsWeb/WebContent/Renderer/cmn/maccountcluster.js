var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MACCOUNTCLUSTER';
function init() {
	refreshTBAGrid();
	refreshQueryGrid();
	if ($('#action').val() == ADD) {
		$('#enabled').prop('disabled', true);
		setCheckbox('enabled', YES);
	}
}

function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'MACCOUNTCLUSTER_MAIN', queryGrid);
}

function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}

function doclearfields(id) {
	switch (id) {
	case 'accountClusterCode':
		if (isEmpty($('#accountClusterCode').val())) {
			$('#accountClusterCode_error').html(EMPTY_STRING);
			$('#accountClusterCode_desc').html(EMPTY_STRING);
			clearNonPKFields();
			break;
		}
	}
}

function clearFields() {
	$('#accountClusterCode').val(EMPTY_STRING);
	$('#accountClusterCode_error').html(EMPTY_STRING);
	$('#accountClusterCode_desc').html(EMPTY_STRING);
	clearNonPKFields();
}

function clearNonPKFields() {
	$('#description').val(EMPTY_STRING);
	$('#description_error').html(EMPTY_STRING);
	$('#conciseDescription').val(EMPTY_STRING);
	$('#conciseDescription_error').html(EMPTY_STRING);
	$('#accountClusterAlphaCode').val(EMPTY_STRING);
	$('#accountClusterAlphaCode_error').html(EMPTY_STRING);
	$('#clusterStateCode').val(EMPTY_STRING);
	$('#clusterStateCode_error').html(EMPTY_STRING);
	$('#clusterStateCode_desc').html(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
}

function doHelp(id) {
	switch (id) {
	case 'accountClusterCode':
		help('COMMON', 'HLP_ACCNT_CLUSTER_CODE_M', $('#accountClusterCode').val(), EMPTY_STRING, $('#accountClusterCode'));
		break;
	case 'clusterStateCode':
		help('COMMON', 'HLP_GEO_UNIT_CODE', $('#clusterStateCode').val(), EMPTY_STRING, $('#clusterStateCode'));
		break;
	}
}

function add() {
	$('#accountClusterCode').focus();
	setCheckbox('enabled', YES);
	$('#enabled').prop('disabled', true);
}

function modify() {
	$('#accountClusterCode').focus();
	if ($('#action').val() == MODIFY) {
		$('#enabled').prop('disabled', false);
	} else {
		$('#enabled').prop('disabled', true);
	}
}

function loadData() {
	$('#accountClusterCode').val(validator.getValue('ACNT_CLUSTER_CODE'));
	$('#description').val(validator.getValue('DESCRIPTION'));
	$('#conciseDescription').val(validator.getValue('CONCISE_DESCRIPTION'));
	$('#accountClusterAlphaCode').val(validator.getValue('CLUSTER_ALPHA_CODE'));
	$('#clusterStateCode').val(validator.getValue('CLUSTER_STATE_CODE'));
	$('#clusterStateCode_desc').html(validator.getValue('F1_DESCRIPTION'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	resetLoading();
}

function view(source, primaryKey) {
	hideParent('cmn/qaccountcluster', source, primaryKey);
	resetLoading();
}

function validate(id) {
	switch (id) {
	case 'accountClusterCode':
		accountClusterCode_val();
		break;
	case 'description':
		description_val();
		break;
	case 'conciseDescription':
		conciseDescription_val();
		break;
	case 'accountClusterAlphaCode':
		accountClusterAlphaCode_val();
		break;
	case 'clusterStateCode':
		clusterStateCode_val();
		break;
	case 'enabled':
		setFocusLast('remarks');
		break;
	case 'remarks':
		remarks_val();
		break;
	}
}

function backtrack(id) {
	switch (id) {
	case 'accountClusterCode':
		setFocusLast('accountClusterCode');
		break;
	case 'description':
		setFocusLast('accountClusterCode');
		break;
	case 'conciseDescription':
		setFocusLast('description');
		break;
	case 'accountClusterAlphaCode':
		setFocusLast('conciseDescription');
		break;
	case 'clusterStateCode':
		setFocusLast('accountClusterAlphaCode');
		break;
	case 'enabled':
		setFocusLast('clusterStateCode');
		break;
	case 'remarks':
		if ($('#action').val() == MODIFY) {
			setFocusLast('enabled');
		} else if (isEmpty($('#clusterStateCode').val())) {
			setFocusLast('accountClusterAlphaCode');
		} else {
			setFocusLast('clusterStateCode');
		}
		break;
	}
}

function accountClusterCode_val() {
	var value = $('#accountClusterCode').val();
	$('#accountClusterCode_desc').html(EMPTY_STRING);
	clearError('accountClusterCode_error');
	if (isEmpty(value)) {
		setError('accountClusterCode_error', MANDATORY);
		return false;
	}
	if (!isNumeric(value)) {
		setError('accountClusterCode_error', INVALID_NUMBER);
		return false;
	}
	value = parseInt($('#accountClusterCode').val());
	$('#accountClusterCode').val(value);
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('ACNT_CLUSTER_CODE', $('#accountClusterCode').val());
	validator.setValue(ACTION, $('#action').val());
	validator.setClass('patterns.config.web.forms.cmn.maccountclusterbean');
	validator.setMethod('validateAccountClusterCode');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('accountClusterCode_error', validator.getValue(ERROR));
		$('#accountClusterCode_desc').html(EMPTY_STRING);
		return false;
	}
	if ($('#action').val() == MODIFY) {
		PK_VALUE = getEntityCode() + PK_SEPERATOR + $('#accountClusterCode').val();
		if (!loadPKValues(PK_VALUE, 'accountClusterCode_error')) {
			return false;
		}
	}
	setFocusLast('description');
	return true;
}

function description_val() {
	var value = $('#description').val();
	clearError('description_error');
	if (isEmpty(value)) {
		setError('description_error', MANDATORY);
		return false;
	}
	if (!isValidDescription(value)) {
		setError('description_error', INVALID_DESCRIPTION);
		return false;
	}
	setFocusLast('conciseDescription');
	return true;
}

function conciseDescription_val() {
	var value = $('#conciseDescription').val();
	clearError('conciseDescription_error');
	if (isEmpty(value)) {
		setError('conciseDescription_error', MANDATORY);
		return false;
	}
	if (!isValidConciseDescription(value)) {
		setError('conciseDescription_error', INVALID_CONCISE_DESCRIPTION);
		return false;
	}
	setFocusLast('accountClusterAlphaCode');
	return true;
}

function accountClusterAlphaCode_val() {
	var value = $('#accountClusterAlphaCode').val();
	var accCode = $('#accountClusterCode').val();
	clearError('accountClusterAlphaCode_error');
	if (isEmpty(value)) {
		setError('accountClusterAlphaCode_error', MANDATORY);
		return false;
	}
	if (value == accCode) {
		setError('accountClusterAlphaCode_error', PPBS_ACC_NOT_EQ_ACCALPHA);
		return false;
	}
	if (!isValidCode(value)) {
		setError('accountClusterAlphaCode_error', INVALID_CODE);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue(ACTION, USAGE);
	validator.setValue('PROGRAM_ID', CURRENT_PROGRAM_ID);
	validator.setValue('CLUSTER_ALPHA_CODE', $('#accountClusterAlphaCode').val());
	validator.setValue('ACNT_CLUSTER_CODE', $('#accountClusterCode').val());
	validator.setClass('patterns.config.web.forms.cmn.maccountclusterbean');
	validator.setMethod('validateAccountClusterAlphaCode');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('accountClusterAlphaCode_error', validator.getValue(ERROR));
		return false;
	}
	setFocusLast('clusterStateCode');
	return true;
}

function clusterStateCode_val() {
	var value = $('#clusterStateCode').val();
	$('#clusterStateCode_desc').html(EMPTY_STRING);
	clearError('clusterStateCode_error');
	if (!isEmpty(value)) {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue(ACTION, USAGE);
		validator.setValue('GEO_UNIT_ID', $('#clusterStateCode').val());
		validator.setClass('patterns.config.web.forms.cmn.maccountclusterbean');
		validator.setMethod('validateClusterStateCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('clusterStateCode_error', validator.getValue(ERROR));
			$('#clusterStateCode_desc').html(EMPTY_STRING);
			return false;
		} else {
			$('#clusterStateCode_desc').html(validator.getValue('DESCRIPTION'));
		}
	}
	if ($('#action').val() == ADD) {
		setFocusLast('remarks');
	} else {
		setFocusLast('enabled');
	}
	return true;
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', INVALID_REMARKS);
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', INVALID_REMARKS);
			return false;
		}
	}
	if ($('#action').val() == MODIFY) {
		if (isEmpty(value)) {
			setError('remarks_error', MANDATORY);
			return false;
		}

	}
	setFocusOnSubmit();
	return true;
}
