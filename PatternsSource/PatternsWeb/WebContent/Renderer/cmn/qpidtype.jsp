<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<web:bundle baseName="patterns.config.web.forms.cmn.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/cmn/qpidtype.js" />
	</web:dependencies>
	<web:loadPK />
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="mpidtype.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:viewContent id="po_view1">
				<web:dividerBlock>
					<web:sectionBlock width="width-100p" align="left">
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="mpidtype.pidtypecode" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:pidCodeDisplay property="pidTypeCode" id="pidTypeCode" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowOdd>
									<web:column>
										<web:legend key="form.description" var="common" mandatory="true" />
									</web:column>
									<web:column>
										<type:descriptionDisplay property="description" id="description" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="form.conciseDescription" var="common" mandatory="true" />
									</web:column>
									<web:column>
										<type:conciseDescriptionDisplay property="conciseDescription" id="conciseDescription" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle width="100px" />
									<web:columnStyle width="150px" />
									<web:columnStyle width="130px" />
									<web:columnStyle width="120px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowOdd>
									<web:column>
										<web:legend key="mpidtype.codemeantforincometaxpayerid" var="program" />
									</web:column>
									<web:column>
										<type:checkboxDisplay property="codeMeantForIncomeTaxPayerId" id="codeMeantForIncomeTaxPayerId" />
									</web:column>
									<web:column>
										<web:legend key="mpidtype.codemeantfornationalID" var="program" />
									</web:column>
									<web:column>
										<type:checkboxDisplay property="codemeantfornationalID" id="codemeantfornationalID" />
									</web:column>
									<web:column>
										<web:legend key="mpidtype.codemeantforpasport" var="program" />
									</web:column>
									<web:column>
										<type:checkboxDisplay property="codeMeantForPassport" id="codeMeantForPassport" />
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle width="100px" />
									<web:columnStyle width="150px" />
									<web:columnStyle width="130px" />
									<web:columnStyle width="120px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="mpidtype.documentuseforpersonidefication" var="program" />
									</web:column>
									<web:column>
										<type:checkboxDisplay property="documentCanUseForPersonIdentification" id="documentCanUseForPersonIdentification" />
									</web:column>
									<web:column>
										<web:legend key="mpidtype.officiallyvaliddoc" var="program" />
									</web:column>
									<web:column>
										<type:checkboxDisplay property="officiallyValidDoc" id="officiallyValidDoc" />
									</web:column>
									<web:column>
										<web:legend key="mpidtype.undersimpmeasureskyc" var="program" />
									</web:column>
									<web:column>
										<type:checkboxDisplay property="underSimpMeasuresKyc" id="underSimpMeasuresKyc" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle width="100px" />
									<web:columnStyle width="150px" />
									<web:columnStyle width="130px" />
									<web:columnStyle width="120px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowOdd>
									<web:column>
										<web:legend key="mpidtype.documentuseforaddressidefication" var="program" />
									</web:column>
									<web:column>
										<type:checkboxDisplay property="documentCanUseForAddressIdentifaction" id="documentCanUseForAddressIdentifaction" />
									</web:column>
									<web:column>
										<web:legend key="mpidtype.officiallyvaliddoc" var="program" />
									</web:column>
									<web:column>
										<type:checkboxDisplay property="officiallyValidDocument" id="officiallyValidDocument" />
									</web:column>
									<web:column>
										<web:legend key="mpidtype.undersimpmeasureskyc" var="program" />
									</web:column>
									<web:column>
										<type:checkboxDisplay property="underSimplifiedMeasuresKyc" id="underSimplifiedMeasuresKyc" />
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="mpidtype.applicableforcitizensofcountry" var="program" />
									</web:column>
									<web:column>
										<type:countryCodeDisplay property="countryCode" id="countryCode" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="mpidtype.issueofdocument" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:descriptionDisplay property="issueOfDocument" id="issueOfDocument" />
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle width="240px" />
									<web:columnStyle width="200px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="mpidtype.doucmenthasissudate" var="program" />
									</web:column>
									<web:column>
										<type:checkboxDisplay property="documentHasIssueDate" id="documentHasIssueDate" />
									</web:column>
									<web:column>
										<web:legend key="mpidtype.doucmenthasexpirydate" var="program" />
									</web:column>
									<web:column>
										<type:checkboxDisplay property="documenthasExpiryDate" id="documenthasExpiryDate" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle width="240px" />
									<web:columnStyle width="200px" />
									<web:columnStyle width="50px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowOdd>
									<web:column>
										<web:legend key="mpidtype.altaccntid" var="program" />
									</web:column>
									<web:column>
										<type:checkboxDisplay property="altAccntId" id="altAccntId" />
									</web:column>
									<web:column>
										<web:legend key="mpidtype.pidforbplcard" var="program" />
									</web:column>
									<web:column>
										<type:checkboxDisplay property="pidForBplCard" id="pidForBplCard" />
									</web:column>
									<web:column>
										<web:legend key="mpidtype.belowpoverty" var="program" />
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="form.IsUseEnabled" var="common" />
									</web:column>
									<web:column>
										<type:checkboxDisplay property="enabled" id="enabled" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="form.remarks" var="common" />
									</web:column>
									<web:column>
										<type:remarksDisplay property="remarks" id="remarks" />
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
						<web:auditDisplay />
					</web:sectionBlock>
				</web:dividerBlock>
			</web:viewContent>
		</web:viewSubPart>
	</web:viewPart>
</web:fragment>
