var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'EUSERREGIONALLOC';
var currentRoleDescription = EMPTY_STRING;
function init() {
	refreshTBAGrid();
	refreshQueryGrid();
	if (_redisplay) {
		if ($('#action').val() == ADD) {
			$('#enabled').prop('disabled', true);
			setCheckbox('enabled', '1');
		}
		userRegAlloc_innerGrid.clearAll();
		if (!isEmpty($('#xmlUserRegAllocGrid').val())) {
			userRegAlloc_innerGrid.loadXMLString($('#xmlUserRegAllocGrid').val());
		}
		//fieldUser_val(false);
	}
}

function loadGrid() {
	loadGridQuery(CURRENT_PROGRAM_ID, 'EUSERREGION_ALLOCATION_GRID', userRegAlloc_innerGrid);
}

function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'EUSERREGIONALLOC_MAIN', queryGrid);
}
function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}

function doHelp(id, shiftmode) {
	switch (id) {
	case 'fieldUser':
		help('COMMON', 'HLP_USER_AS_FIELD', $('#fieldUser').val(), EMPTY_STRING, $('#fieldUser'));
		break;
	case 'effectiveDate':
		help(CURRENT_PROGRAM_ID, 'HLP_EFF_DATE', $('#effectiveDate').val(), $('#userID').val(), $('#effectiveDate'));
		break;
	case 'geoRegionCode':
		help('COMMON', 'HLP_GEO_REG_CODE', $('#geoRegionCode').val(), EMPTY_STRING, $('#geoRegionCode'));
		break;
	}
}

function clearFields() {
	$('#fieldUser').val(EMPTY_STRING);
	$('#fieldUser_desc').html(EMPTY_STRING);
	$('#fieldUser_error').html(EMPTY_STRING);
	$('#fieldUserType').val(EMPTY_STRING);
	$('#effectiveDate').val(EMPTY_STRING);
	$('#effectiveDate_error').html(EMPTY_STRING);
	clearNonPKFields();
}

function clearNonPKFields() {
	geoRegionAlloc_clearGridFields();
	userRegAlloc_innerGrid.clearAll();
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
}

function doclearfields(id) {
	switch (id) {
	case 'fieldUser':
		if (isEmpty($('#fieldUser').val())) {
			$('#fieldUser_error').html(EMPTY_STRING);
			$('#fieldUser_desc').html(EMPTY_STRING);
			$('#fieldUserType').val(EMPTY_STRING);
			$('#effectiveDate').val(EMPTY_STRING);
			$('#effectiveDate_error').html(EMPTY_STRING);
			clearNonPKFields();
			break;
		}
	case 'effectiveDate':
		if (isEmpty($('#effectiveDate').val())) {
			$('#effectiveDate_error').html(EMPTY_STRING);
			clearNonPKFields();
			break;
		}
	case 'geoRegionCode':
		if (isEmpty($('#geoRegionCode').val())) {
			$('#geoRegionCode_error').html(EMPTY_STRING);
			$('#geoRegionCode_desc').html(EMPTY_STRING);
			break;
		}
	}
}

function add() {
	$('#fieldUser').focus();
	setCheckbox('enabled', YES);
	$('#enabled').prop('disabled', true);
}

function modify() {
	$('#fieldUser').focus();
	if ($('#action').val() == MODIFY) {
		$('#enabled').prop('disabled', false);
	} else {
		$('#enabled').prop('disabled', true);
	}
}

function loadData() {
	$('#fieldUser').val(validator.getValue('FIELD_USER_ID'));
	$('#fieldUser_desc').html(validator.getValue('F1_USER_NAME'));
	if (validator.getValue('F1_THIRD_PARTY_ORG_CODE')!=EMPTY_STRING)
		$('#fieldUserType').val(PBS_BUSINS_CORRES);
	else if (validator.getValue('F1_EMP_CODE')!=EMPTY_STRING)
		$('#fieldUserType').val(PBS_EMPLOYEE);
	else
		$('#fieldUserType').val(EMPTY_STRING);
	$('#effectiveDate').val(validator.getValue('EFF_DATE'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadGrid();
	resetLoading();
}
function view(source, primaryKey) {
	hideParent('cmn/quserregionalloc', source, primaryKey);
	resetLoading();
}

function validate(id) {
	switch (id) {
	case 'fieldUser':
		fieldUser_val(true);
		break;
	case 'effectiveDate':
		effectiveDate_val();
		break;
	case 'geoRegionCode':
		geoRegionCode_val();
		break;
	case 'enabled':
		enabled_val();
		break;
	case 'remarks':
		remarks_val();
		break;
	}
}

function backtrack(id) {
	switch (id) {
	case 'fieldUser':
		setFocusLast('fieldUser');
		break;
	case 'effectiveDate':
		setFocusLast('fieldUser');
		break;
	case 'geoRegionCode':
		setFocusLast('effectiveDate');
		break;
	case 'enabled':
		setFocusLast('geoRegionCode');
		break;
	case 'remarks':
		if ($('#action').val() == MODIFY) {
			setFocusLast('enabled');
		} else {
			setFocusLast('geoRegionCode');
		}
		break;
	}
}

function gridExit(id) {
	switch (id) {
	case 'geoRegionCode':
		$('#xmlUserRegAllocGrid').val(userRegAlloc_innerGrid.serialize());
		if ($('#action').val() == ADD) {
			setFocusLast('remarks');
		} else {
			setFocusLast('enabled');
		}
		break;
	}
}

function gridDeleteRow(id) {
	switch (id) {
	case 'xmlUserRegAllocGrid':
		deleteGridRecord(userRegAlloc_innerGrid);
		break;
	}
}

function fieldUser_val(valMode) {
	var value = $('#fieldUser').val();
	clearError('fieldUser_error');
	if (isEmpty(value)) {
		setError('fieldUser_error', MANDATORY);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('USER_ID', $('#fieldUser').val());
	validator.setValue(ACTION, USAGE);
	validator.setClass('patterns.config.web.forms.cmn.euserregionallocbean');
	validator.setMethod('validateFieldUser');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('fieldUser_error', validator.getValue(ERROR));
		$('#fieldUser_desc').html(EMPTY_STRING);
		$('#fieldUserType').val(EMPTY_STRING);
		return false;
	}
	$('#fieldUser_desc').html(validator.getValue("USER_NAME"));
	if (validator.getValue('USER_TYPE') == ONE) {
		$('#fieldUserType').val(PBS_BUSINS_CORRES);
	} else if (validator.getValue('USER_TYPE') == ZERO) {
		$('#fieldUserType').val(PBS_EMPLOYEE);
	} else {
		$('#fieldUserType').val(EMPTY_STRING);
	}
	if (valMode)
		setFocusLast('effectiveDate');
	return true;
}

function effectiveDate_val() {
	var fieldUser = $('#fieldUser').val();
	var value = $('#effectiveDate').val();
	clearError('effectiveDate_error');
	if (isEmpty(value)) {
		$('#effectiveDate').val(getCBD());
		value = $('#effectiveDate').val();
	}
	if (!isValidEffectiveDate(value, 'effectiveDate_error')) {
		return false;
	}
	if (!isEmpty(fieldUser)) {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('USER_ID', $('#fieldUser').val());
		validator.setValue('EFF_DATE', $('#effectiveDate').val());
		validator.setValue('ACTION', $('#action').val());
		validator.setClass('patterns.config.web.forms.cmn.euserregionallocbean');
		validator.setMethod('validateEffectiveDate');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('effectiveDate_error', validator.getValue(ERROR));
			return false;
		} else {
			if ($('#action').val() == MODIFY) {
				PK_VALUE = getEntityCode() + PK_SEPERATOR + $('#fieldUser').val() + PK_SEPERATOR + getTBADateFormat($('#effectiveDate').val());
				if (!loadPKValues(PK_VALUE, 'effectiveDate_error')) {
					return false;
				}
			}
		}
	} else {
		setError('fieldUser_error', MANDATORY);
		setFocusLast('fieldUser');
		return false;
	}
	setFocusLast('geoRegionCode');
	return true;
}

function geoRegionCode_val() {
	var value = $('#geoRegionCode').val();
	clearError('geoRegionCode_error');
	if (isEmpty(value)) {
		setError('geoRegionCode_error', MANDATORY);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('GEO_REGION_CODE', value);
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.cmn.euserregionallocbean');
		validator.setMethod('validateGeoRegionCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('geoRegionCode_error', validator.getValue(ERROR));
			$('#geoRegionCode_desc').html(EMPTY_STRING);
			return false;
		}
		$('#geoRegionCode_desc').html(validator.getValue("DESCRIPTION"));
		$('#pincode').val(validator.getValue('PINCODE_GEOUNITID'));
		$('#officecode').val(validator.getValue('OFFICE_CODE'));
	}
	setFocusLastOnGridSubmit('userRegAlloc_innerGrid');
	return true;
}

function enabled_val() {
	if ($('#action').val() == MODIFY) {
		setFocusLast('remarks');
	}
	return true;
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', INVALID_REMARKS);
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', INVALID_REMARKS);
			return false;
		}
	}
	if ($('#action').val() == MODIFY) {
		if (isEmpty(value)) {
			setError('remarks_error', MANDATORY);
			return false;
		}
	}
	setFocusOnSubmit();
	return true;

}

function userRegAlloc_addGrid(editMode, editRowId) {
	if (geoRegionCode_val()) {
		if (gridDuplicateCheck(editMode, editRowId)) {
			var field_values = [ COLUMN_DISABLE, $('#geoRegionCode').val(), $('#geoRegionCode_desc').html(), $('#pincode').val(), $('#officecode').val() ];
			_grid_updateRow(userRegAlloc_innerGrid, field_values);
			geoRegionAlloc_clearGridFields();
			$('#geoRegionCode').focus();
			return true;
		} else {
			setError('geoRegionCode_error', DUPLICATE_DATA);
			return false;
		}
	} else {
		return false;
	}

}

function gridDuplicateCheck(editMode, editRowId) {
	var currentValue = [ $('#geoRegionCode').val() ];
	return _grid_duplicate_check(userRegAlloc_innerGrid, currentValue, [ 1 ]);
}

function userRegAlloc_editGrid(rowId) {
	$('#geoRegionCode').val(userRegAlloc_innerGrid.cells(rowId, 1).getValue());
	$('#geoRegionCode_desc').html(userRegAlloc_innerGrid.cells(rowId, 2).getValue());
	$('#geoRegionCode').focus();

}
function geoRegionAlloc_clearGridFields() {
	$('#geoRegionCode').val(EMPTY_STRING);
	$('#geoRegionCode_error').html(EMPTY_STRING);
	$('#geoRegionCode_desc').html(EMPTY_STRING);
	$('#pincode').val(EMPTY_STRING);
	$('#officecode').val(EMPTY_STRING);
}

function userRegAlloc_cancelGrid(rowId) {
	geoRegionAlloc_clearGridFields();
}

function revalidate() {
	geoRegionAlloc_revaildateGrid();
}

function geoRegionAlloc_revaildateGrid() {
	if (userRegAlloc_innerGrid.getRowsNum() > 0) {
		$('#xmlUserRegAllocGrid').val(userRegAlloc_innerGrid.serialize());
		$('#geoRegionCode_error').html(EMPTY_STRING);
	} else {
		userRegAlloc_innerGrid.clearAll();
		$('#xmlUserRegAllocGrid').val(userRegAlloc_innerGrid.serialize());
		setError('geoRegionCode_error', ADD_ATLEAST_ONE_ROW);
		errors++;
	}
}
