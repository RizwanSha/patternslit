var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MEMPDB';
function init() {
    clearFields();
    if (_tableType == MAIN) {
	loadMainValues(_primaryKey);
    } else if (_tableType == TBA) {
	loadTBAValues(_primaryKey);
    }
}
function clearFields() {
	$('#employeeCode').val(EMPTY_STRING);
	$('#employeeCode_desc').html(EMPTY_STRING);
	$('#employeeCode_error').html(EMPTY_STRING);
	clearNonPKFields();
}

function clearNonPKFields() {
	$('#employee').val(EMPTY_STRING);
	$('#employee_title').val(EMPTY_STRING);
	$('#employee_error').html(EMPTY_STRING);
	$('#gender').val(EMPTY_STRING);
	$('#gender_error').html(EMPTY_STRING);
	$('#dateOfBirth').val(EMPTY_STRING);
	$('#dateOfBirth_error').html(EMPTY_STRING);
	$('#relationShip').val(EMPTY_STRING);
	$('#relationShip_error').html(EMPTY_STRING);
	$('#relationName').val(EMPTY_STRING);
	$('#relationName_error').html(EMPTY_STRING);
	$('#dateOfJoining').val(EMPTY_STRING);
	$('#dateOfJoining_error').html(EMPTY_STRING);
	$('#dateOfRetirement').val(EMPTY_STRING);
	$('#dateOfRetirement_error').html(EMPTY_STRING);
	setCheckbox('provideFundApplicable', NO);
	$('#provideFundApplicable_error').html(EMPTY_STRING);
	setCheckbox('personFundRequired', NO);
	$('#personFundRequired_error').html(EMPTY_STRING);
	setCheckbox('eligibleEsi', NO);
	$('#eligibleEsi_error').html(EMPTY_STRING);
	$('#pfNumber').val(EMPTY_STRING);
	$('#pfNumber_error').html(EMPTY_STRING);
	$('#esiNumber').val(EMPTY_STRING);
	$('#esiNumber_error').html(EMPTY_STRING);
	$('#qualification').val(EMPTY_STRING);
	$('#qualification_error').html(EMPTY_STRING);
	$('#emailId').val(EMPTY_STRING);
	$('#emailId_error').html(EMPTY_STRING);
	$('#mobileNumber').val(EMPTY_STRING);
	$('#mobileNumber_error').html(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
}
function loadData() {
	$('#employeeCode').val(validator.getValue('EMP_CODE'));
	$('#employee').val(validator.getValue('PERSON_NAME'));
	$('#employee_title').val(validator.getValue('TITLE_CODE'));
	$('#gender').val(validator.getValue('GENDER'));
	$('#dateOfBirth').val(validator.getValue('DATE_OF_BIRTH'));
	$('#relationShip').val(validator.getValue('FATHER_HUSBAND_FLAG'));
	$('#relationName').val(validator.getValue('FATHER_OR_HUSBAND_NAME'));
	$('#dateOfRetirement').val(validator.getValue('RETIREMENT_DATE'));
	$('#dateOfJoining').val(validator.getValue('DATE_OF_JOIN'));
	setCheckbox('provideFundApplicable', validator.getValue('PF_APPLICABLE'));
	$('#pfNumber').val(validator.getValue('PF_NUMBER'));
	setCheckbox('personFundRequired', validator.getValue('PENSION_FUND_REQD'));
	setCheckbox('eligibleEsi', validator.getValue('ELIGIBLE_FOR_ESI'));
	$('#esiNumber').val(validator.getValue('EMP_ESI_NO'));
	$('#qualification').val(validator.getValue('EMP_QUALIFICATION'));
	$('#emailId').val(validator.getValue('EMAIL_ID'));
	$('#mobileNumber').val(validator.getValue('MOBILE_NO'));
	$('#remarks').val(validator.getValue('REMARKS'));
    loadAuditFields(validator);
    resetLoading();
}
