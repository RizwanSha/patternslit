<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.cmn.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/cmn/qtitles.js" />
	</web:dependencies>
	<web:loadPK />
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="qtitles.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:viewContent id="po_view1">
				<web:dividerBlock>
					<web:sectionBlock width="width-100p" align="left">
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="174px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="mtitles.TitleCode" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:titleDisplay property="titleCode" id="titleCode" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowOdd>
										<web:column>
											<web:legend key="mtitles.ExtDescription" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:descriptionDisplay property="extDescription" id="extDescription" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="mtitles.IntDescription" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:descriptionDisplay property="intDescription" id="intDescription"  />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="mtitles.shortDescription" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:conciseDescriptionDisplay property="shortDescription" id="shortDescription"  />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="mtitles.TitleUsage" var="program" />
										</web:column>
										<web:column>
											<type:comboDisplay property="titleUsage" id="titleUsage" datasourceid="COMMON_CMNTITLES" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="mtitles.TitleChildrenUsage" var="program" />
										</web:column>
										<web:column>
											<type:checkboxDisplay property="titleChildrenUsage" id="titleChildrenUsage" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="mtitles.TitleDeceasedUsage" var="program" />
										</web:column>
										<web:column>
											<type:checkboxDisplay property="titleDeceasedUsage" id="titleDeceasedUsage" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="mtitles.TitleOccupation" var="program" />
										</web:column>
										<web:column>
											<type:occupationCodeDisplay property="titleOccupation" id="titleOccupation" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="mtitles.OtherOccupation" var="program" />
										</web:column>
										<web:column>
											<type:freeTextDisplay_100 property="otherOccupation" id="otherOccupation"  />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="mtitles.defMaritalStatus" var="program" />
										</web:column>
										<web:column>
											<type:maritalStatusCodeDisplay property="defMaritalStatus" id="defMaritalStatus" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="mtitles.fixMaritalStatus" var="program" />
										</web:column>
										<web:column>
											<type:checkboxDisplay property="fixMaritalStatus" id="fixMaritalStatus" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="mtitles.enabled" var="program" />
										</web:column>
										<web:column>
											<type:checkboxDisplay property="enabled" id="enabled" />
										</web:column>
									</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="form.remarks" var="common" />
									</web:column>
									<web:column>
										<type:remarksDisplay property="remarks" id="remarks" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:auditDisplay />
					</web:sectionBlock>
				</web:dividerBlock>
			</web:viewContent>
		</web:viewSubPart>
	</web:viewPart>
</web:fragment>
