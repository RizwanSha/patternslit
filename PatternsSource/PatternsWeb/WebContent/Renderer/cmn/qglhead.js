var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MGLHEAD';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}
function clearFields() {
	$('#glHeadCode').val(EMPTY_STRING);
	$('#glHeadCode_desc').html(EMPTY_STRING);
	clearNonPKFields();
}

function clearNonPKFields() {
	$('#description').val(EMPTY_STRING);
	$('#conciseDescription').val(EMPTY_STRING);
	$('#leasePdtCode').val(EMPTY_STRING);
	setCheckbox('enabled', YES);
	$('#remarks').val(EMPTY_STRING);
}

function loadData() {
	$('#glHeadCode').val(validator.getValue('GL_HEAD_CODE'));
	$('#description').val(validator.getValue('DESCRIPTION'));
	$('#conciseDescription').val(validator.getValue('CONCISE_DESCRIPTION'));
	$('#leasePdtCode').val(validator.getValue('LEASE_PRODUCT_CODE'));
	$('#leasePdtCode_desc').html(validator.getValue('F1_DESCRIPTION'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadAuditFields(validator);
}
