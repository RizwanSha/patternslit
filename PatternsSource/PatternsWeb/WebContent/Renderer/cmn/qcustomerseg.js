var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MCUSTOMERSEG';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}

function clearFields() {
	$('#customerSegmentCode').val(EMPTY_STRING);
	$('#customerSegmentCode_desc').html(EMPTY_STRING);
	$('#description').val(EMPTY_STRING);
	$('#conciseDescription').val(EMPTY_STRING);
	$('#segmentFor').val(EMPTY_STRING);
	setCheckbox('prioritySectorSegment', NO);
	setCheckbox('seniorCitizenSegment', NO);
	setCheckbox('womenSegment', NO);
	setCheckbox('staffSegment', NO);
	setCheckbox('enabled', YES);
	$('#remarks').val(EMPTY_STRING);
}

function loadData() {
	$('#customerSegmentCode').val(validator.getValue('CUST_SEGMENT_CODE'));
	$('#description').val(validator.getValue('DESCRIPTION'));
	$('#conciseDescription').val(validator.getValue('CONCISE_DESCRIPTION'));
	$('#segmentFor').val(validator.getValue('SEGMENT_FOR'));
	setCheckbox('prioritySectorSegment', validator.getValue('PRIORTY_SECTOR_SEG'));
	setCheckbox('seniorCitizenSegment', validator.getValue('SENIOR_CIT_SEG'));
	setCheckbox('womenSegment', validator.getValue('WOMEN_SEG'));
	setCheckbox('staffSegment', validator.getValue('STAFF_SEG'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
    loadAuditFields(validator);
}