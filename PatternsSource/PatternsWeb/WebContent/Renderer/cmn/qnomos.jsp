<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.cmn.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/cmn/qnomos.js" />
	</web:dependencies>
	<web:loadPK />
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="enomos.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:viewContent id="po_view1">
				<web:dividerBlock>
					<web:sectionBlock width="width-100p" align="left">
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="enomos.glhead" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:GLAccessCodeDisplay property="glHead" id="glHead" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle width="320px" />
									<web:columnStyle width="180px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowOdd>
									<web:column>
										<web:legend key="enomos.precedingentry" var="program" />
									</web:column>
									<web:column>
										<type:comboDisplay property="precedingEntry" id="precedingEntry" datasourceid="COMMON_DEBIT_CREDIT" />
									</web:column>
									<web:column>
										<web:legend key="enomos.reversal" var="program" />
									</web:column>
									<web:column>
										<type:comboDisplay property="reversal" id="reversal" datasourceid="COMMON_GL_RECON_TYPE" />
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle width="320px" />
									<web:columnStyle width="180px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="enomos.systemreferenceno" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:twelveDigitDisplay property="systemRefNo" id="systemRefNo" />
									</web:column>
									<web:column>
										<web:legend key="enomos.dateofentry" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:dateDisplay property="dateOfEntry" id="dateOfEntry" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowOdd>
									<web:column>
										<web:legend key="enomos.interbranchtrantype" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:interBrnTranTypeDisplay property="interBranchTranType" id="interBranchTranType" />
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle width="320px" />
									<web:columnStyle width="180px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="enomos.entrytype" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:comboDisplay property="entryType" id="entryType" datasourceid="ENOMOS_ENTRY_TYPE" />
									</web:column>
									<web:column>
										<web:legend key="enomos.debitorcredit" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:comboDisplay property="debitOrCredit" id="debitOrCredit" datasourceid="COMMON_DEBIT_CREDIT" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle width="320px" />
									<web:columnStyle width="180px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowOdd>
									<web:column>
										<web:legend key="enomos.originalentryamountactual" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:currencyBigAmountDisplay property="orgEntryAmtActl" id="orgEntryAmtActl" />
									</web:column>
									<web:column>
										<web:legend key="enomos.originalentryamountbase" var="program" />
									</web:column>
									<web:column>
										<type:currencyBigAmountDisplay property="orgEntryAmtBase" id="orgEntryAmtBase" />
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle width="320px" />
									<web:columnStyle width="180px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="enomos.manualentryrefno" var="program" />
									</web:column>
									<web:column>
										<type:otherInformation25Display property="manualEntryRefNo" id="manualEntryRefNo" />
									</web:column>
									<web:column>
										<web:legend key="enomos.systementryrefno" var="program" />
									</web:column>
									<web:column>
										<type:systemRefNumberDisplay property="systemEntryRef" id="systemEntryRef" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:viewContent id="view_normal" styleClass="hidden">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle width="315px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="enomos.openbalofentryactual" var="program" />
										</web:column>
										<web:column>
											<type:currencyBigAmountDisplay property="openBalOfEntryActl" id="openBalOfEntryActl" />
										</web:column>
										<web:column>
											<type:comboDisplay property="openBalActDbOrCr" id="openBalActDbOrCr" datasourceid="COMMON_DEBIT_CREDIT" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="enomos.openbalofentrybase" var="program" />
										</web:column>
										<web:column>
											<type:currencyBigAmountDisplay property="openBalOfEntryBase" id="openBalOfEntryBase" />
										</web:column>
										<web:column>
											<type:comboDisplay property="openBalBaseDbOrCr" id="openBalBaseDbOrCr" datasourceid="COMMON_DEBIT_CREDIT" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="enomos.openingbalancebasedate" var="program" />
										</web:column>
										<web:column>
											<type:dateDisplay property="openingBalbaseDate" id="openingBalbaseDate"  />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:viewContent id="view_cif" styleClass="hidden">
								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="200px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowEven>
											<web:column>
												<web:legend key="enomos.cif" var="program" />
											</web:column>
											<web:column>
												<type:cifNumberDisplay property="cifNumber" id="cifNumber" />
											</web:column>
										</web:rowEven>
									</web:table>
								</web:section>
							</web:viewContent>
							<web:viewContent id="view_staff" styleClass="hidden">
								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="200px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowEven>
											<web:column>
												<web:legend key="enomos.staffid" var="program" />
											</web:column>
											<web:column>
												<type:employeeCodeDisplay property="staffId" id="staffId" />
											</web:column>
										</web:rowEven>
									</web:table>
								</web:section>
							</web:viewContent>
						</web:viewContent>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowOdd>
									<web:column>
										<web:legend key="enomos.notes" var="program" />
									</web:column>
									<web:column>
										<type:description250Display property="notes" id="notes" />
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
						<web:auditDisplay />
					</web:sectionBlock>
				</web:dividerBlock>
			</web:viewContent>
		</web:viewSubPart>
	</web:viewPart>
</web:fragment>