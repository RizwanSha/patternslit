var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MCIFCONTROL';
function init() {
	refreshTBAGrid();
	refreshQueryGrid();
	if ($('#action').val() == ADD) {
		$('#effectiveDate_look').prop('disabled', true);
		setCheckbox('enabled', YES);
		$('#enabled').prop('disabled', true);
	}
}

function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'MCIFCONTROL_MAIN');
}

function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}

function add() {
	$('#customerSegmentCode').focus();
	$('#effectiveDate_look').prop('disabled', true);
	setCheckbox('enabled', YES);
	$('#enabled').prop('disabled', true);
}

function modify() {
	$('#customerSegmentCode').focus();
	$('#effectiveDate_look').prop('disabled', false);
	$('#enabled').prop('disabled', false);
}

function view(source, primaryKey) {
	hideParent('cmn/qcifcontrol', source, primaryKey);
	resetLoading();
}

function clearFields() {
	$('#customerSegmentCode').val(EMPTY_STRING);
	$('#customerSegmentCode_desc').html(EMPTY_STRING);
	$('#customerSegmentCode_error').html(EMPTY_STRING);
	$('#effectiveDate').val(EMPTY_STRING);
	$('#effectiveDate_error').html(EMPTY_STRING);
	clearNonPKFields();
}

function clearNonPKFields() {
	$('#cifNumber').val(EMPTY_STRING);
	$('#cifNumber_error').html(EMPTY_STRING);
	$('#enabled_error').html(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
}

function doclearfields(id) {
	switch (id) {
	case 'customerSegmentCode':
		if (isEmpty($('#customerSegmentCode').val())) {
			clearFields();
		}
		break;
	case 'effectiveDate':
		if (isEmpty($('#effectiveDate').val())) {
			$('#effectiveDate_error').html(EMPTY_STRING);
			clearNonPKFields();
		}
		break;
	}
}
function loadData() {
	$('#customerSegmentCode').val(validator.getValue('CUST_SEGMENT_CODE'));
	$('#customerSegmentCode_desc').html(validator.getValue('F1_DESCRIPTION'));
	$('#effectiveDate').val(validator.getValue('EFF_DATE'));
	$('#cifNumber').val(validator.getValue('STARTING_CIF'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	resetLoading();
}

function doHelp(id) {
	switch (id) {
	case 'customerSegmentCode':
		help('COMMON', 'HLP_CUST_SEGMENT_CODE', $('#customerSegmentCode').val(), EMPTY_STRING, $('#customerSegmentCode'));
		break;
	case 'effectiveDate':
		if ($('#action').val() == MODIFY)
			help(CURRENT_PROGRAM_ID, 'HLP_EFF_DATE', $('#effectiveDate').val(), $('#customerSegmentCode').val(), $('#effectiveDate'));
		break;
	}
}

function validate(id) {
	switch (id) {
	case 'customerSegmentCode':
		customerSegmentCode_val();
		break;
	case 'effectiveDate':
		effectiveDate_val();
		break;
	case 'cifNumber':
		cifNumber_val();
		break;
	case 'enabled':
		setFocusLast('remarks');
		break;
	case 'remarks':
		remarks_val();
		break;
	}
}

function backtrack(id) {
	switch (id) {
	case 'customerSegmentCode':
		setFocusLast('customerSegmentCode');
		break;
	case 'effectiveDate':
		setFocusLast('customerSegmentCode');
		break;
	case 'cifNumber':
		setFocusLast('effectiveDate');
		break;
	case 'enabled':
		setFocusLast('cifNumber');
		break;
	case 'remarks':
		if ($('#action').val() == ADD)
			setFocusLast('cifNumber');
		else
			setFocusLast('enabled');
		break;

	}
}

function customerSegmentCode_val() {
	var value = $('#customerSegmentCode').val();
	clearError('customerSegmentCode_error');
	if (isEmpty(value)) {
		setError('customerSegmentCode_error', MANDATORY);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('CUST_SEGMENT_CODE', $('#customerSegmentCode').val());
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.cmn.mcifcontrolbean');
		validator.setMethod('validateCustomerSegmentCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('customerSegmentCode_error', validator.getValue(ERROR));
			return false;
		} else {
			$('#customerSegmentCode_desc').html(validator.getValue('DESCRIPTION'));
		}
	}
	setFocusLast('effectiveDate');
	return true;
}

function effectiveDate_val() {
	var value = $('#effectiveDate').val();
	clearError('effectiveDate_error');
	if (isEmpty(value)) {
		$('#effectiveDate').val(getCBD());
		value = $('#effectiveDate').val();
	}
	if (!isValidEffectiveDate(value, 'effectiveDate_error')) {
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('CUST_SEGMENT_CODE', $('#customerSegmentCode').val());
		validator.setValue('EFF_DATE', $('#effectiveDate').val());
		validator.setValue(ACTION, $('#action').val());
		validator.setClass('patterns.config.web.forms.cmn.mcifcontrolbean');
		validator.setMethod('validateEffectiveDate');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('effectiveDate_error', validator.getValue(ERROR));
			return false;
		} else {
			if ($('#action').val() == MODIFY) {
				PK_VALUE = getEntityCode()+ PK_SEPERATOR + $('#customerSegmentCode').val() + PK_SEPERATOR + $('#effectiveDate').val();
				if (!loadPKValues(PK_VALUE, 'effectiveDate_error')) {
					return false;
				}
			}
		}
	}
	setFocusLast('cifNumber');
	return true;
}

function cifNumber_val() {
	var value = $('#cifNumber').val();
	clearError('cifNumber_error');
	if (isEmpty(value)) {
		setError('cifNumber_error', MANDATORY);
		return false;
	}
	if (!isValidTwelveDigit(value)){
		setError('cifNumber_error', INVALID_NUMBER_FORMAT);
		return false;
	}
	if (!isPositive(value)){
		setError('cifNumber_error', POSITIVE_CHECK);
		return false;
	}
	if ($('#action').val() == ADD)
		setFocusLast('remarks');
	else
		setFocusLast('enabled');
	return true;
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', INVALID_REMARKS);
			setFocusLast('remarks');
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', INVALID_REMARKS);
			setFocusLast('remarks');
			return false;
		}
	}
	if ($('#action').val() == MODIFY) {
		if (isEmpty(value)) {
			setError('remarks_error', MANDATORY);
			setFocusLast('remarks');
			return false;
		}
	}
	setFocusOnSubmit();
	return true;
}
