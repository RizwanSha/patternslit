<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.cmn.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/cmn/qcountry.js" />
	</web:dependencies>
	<web:loadPK />
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="qcountry.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:viewContent id="po_view1">
				<web:dividerBlock>
					<web:sectionBlock width="width-100p" align="left">
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="180px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="mcountry.countrycode" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:countryCodeDisplay property="countryCode" id="countryCode" />
									</web:column>
								</web:rowEven>
							</web:table>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="180px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowOdd>
									<web:column>
										<web:legend key="form.name" var="common" mandatory="true" />
									</web:column>
									<web:column>
										<type:descriptionDisplay property="countryName" id="countryName" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="form.shortName" var="common" mandatory="true" />
									</web:column>
									<web:column>
										<type:conciseDescriptionDisplay property="shortName" id="shortName" />
									</web:column>
								</web:rowEven>

								<web:rowOdd>
									<web:column>
										<web:legend key="mcountry.geounitstructurecode" var="program" />
									</web:column>
									<web:column>
										<type:geographicalUnitStructureCodeDisplay property="geoUnitStructureCode" id="geoUnitStructureCode" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="form.enabled" var="common" />
									</web:column>
									<web:column>
										<type:checkboxDisplay property="enabled" id="enabled" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="form.remarks" var="common" />
									</web:column>
									<web:column>
										<type:remarksDisplay property="remarks" id="remarks" />
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
						<web:auditDisplay />
					</web:sectionBlock>
				</web:dividerBlock>
			</web:viewContent>
		</web:viewSubPart>
	</web:viewPart>
</web:fragment>