var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'ECUSTGEOALLOC';
var cbd = getCBD();
function init() {
	refreshTBAGrid();
	refreshQueryGrid();
	$('#address').prop('readOnly', true);
	$('#pincode').prop('readOnly', true);

	if (_redisplay) {
		if ($('#action').val() == ADD) {
			$('#enabled').prop('disabled', true);
			setCheckbox('enabled', YES);
		}
	ecustgeoalloc_innerGrid.clearAll();
		if (!isEmpty($('#xmlCustGrid').val())) {
			ecustgeoalloc_innerGrid.loadXMLString($('#xmlCustGrid').val());
		}
	}
}
function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'ECUSTGEOALLOC_MAIN', queryGrid);
}

function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}

function loadGrid() {
	loadGridQuery(CURRENT_PROGRAM_ID, 'ECUSTGEOALLOC_GRID', postProcessing);
}

function postProcessing(result) {
	$xml = $($.parseXML(result));
	($xml).find("row").each(function() {
		try {
			var rowID = $(this).find("cell")[1].textContent;
			ecustgeoalloc_innerGrid.cells(rowID, 0).setValue(true);
		} catch (e) {

		}
	});

}

function doclearfields(id) {
	switch (id) {
	case 'cusotmerIdFoNo':
		if (isEmpty($('#cusotmerIdFoNo').val())) {
			clearFields();
			break;
		}
	case 'effectiveDate':
		if (isEmpty($('#effectiveDate').val())) {
			$('#effectiveDate_error').html(EMPTY_STRING);
			clearNonPKFields();
			break;
		}
	}
}

function clearFields() {
	$('#cusotmerIdFoNo').val(EMPTY_STRING);
	$('#cusotmerIdFoNo_desc').html(EMPTY_STRING);
	$('#cusotmerIdFoNo_error').html(EMPTY_STRING);
	$('#effectiveDate').val(EMPTY_STRING);
	$('#effectiveDate_error').html(EMPTY_STRING);
	displayFields();
	clearNonPKFields();
	ecustgeoalloc_innerGrid.clearAll();
}

function clearNonPKFields() {
	setCheckbox('enabled', YES);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
	$('#gridCheck_error').html(EMPTY_STRING);

}

function displayFields() {
	$('#address').val(EMPTY_STRING);
	$('#pincode').val(EMPTY_STRING);
}

function doHelp(id) {
	switch (id) {
	case 'cusotmerIdFoNo':
		help('COMMON', 'HLP_CUST_ID_FOLIO', $('#cusotmerIdFoNo').val(), EMPTY_STRING, $('#cusotmerIdFoNo'));
		break;
	case 'effectiveDate':
		if (!isEmpty($('#cusotmerIdFoNo').val()))
			help('ECUSTGEOALLOC', 'EFF_DATE', $('#effectiveDate').val(), $('#cusotmerIdFoNo').val(), $('#effectiveDate'));
		else {
			setError('cusotmerIdFoNo_error', MANDATORY);
			return false;
		}
		break;
	}
}

function add() {
	$('#cusotmerIdFoNo').focus();
	setCheckbox('enabled', YES);
	$('#enabled').prop('disabled', true);
}

function modify() {
	$('#cusotmerIdFoNo').focus();
	if ($('#action').val() == MODIFY) {
		$('#enabled').prop('disabled', false);
	} else {
		$('#enabled').prop('disabled', true);
	}
}

function backtrack(id) {
	switch (id) {
	case 'cusotmerIdFoNo':
		setFocusLast('cusotmerIdFoNo');
		break;
	case 'effectiveDate':
		setFocusLast('cusotmerIdFoNo');
		break;
	case 'enabled':
		setFocusLast('effectiveDate');
		break;
	case 'remarks':
		if ($('#action').val() == MODIFY) {
			setFocusLast('enabled');
		} else {
			setFocusLast('effectiveDate');
		}
		break;
	}
}

function loadData() {
	$('#cusotmerIdFoNo').val(validator.getValue('CIF_NO'));
	$('#cusotmerIdFoNo_desc').html(validator.getValue('F1_NAME'));
	$('#address').val(validator.getValue('F1_ADDRESS'));
	$('#pincode').val(validator.getValue('F1_GEO_UNIT_ID'));
	$('#effectiveDate').val(validator.getValue('EFF_DATE'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadGridValues_val();
	loadGrid();
	resetLoading();
}
function view(source, primaryKey) {
	hideParent('cmn/qcustgeoalloc', source, primaryKey);
	resetLoading();
}
function validate(id) {
	switch (id) {
	case 'cusotmerIdFoNo':
		cusotmerIdFoNo_val();
		break;
	case 'effectiveDate':
		effectiveDate_val();
		break;
	case 'enabled':
		enabled_val();
		break;
	case 'remarks':
		remarks_val();
		break;
	}
}

function backtrack(id) {
	switch (id) {
	case 'cusotmerIdFoNo':
		setFocusLast('cusotmerIdFoNo');
		break;
	case 'effectiveDate':
		setFocusLast('cusotmerIdFoNo');
		break;
	case 'enabled':
		setFocusLast('effectiveDate');
		break;
	case 'remarks':
		if ($('#action').val() == MODIFY) {
			setFocusLast('enabled');
		} else {
			setFocusLast('effectiveDate');
		}
		break;
	}
}

function cusotmerIdFoNo() {
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('CIF_NO', $('#cusotmerIdFoNo').val());
	validator.setValue(ACTION, $('#action').val());
	if ($('#action').val() == ADD)
		validator.setValue('FETCH_REGIONS', COLUMN_ENABLE);
	else
		validator.setValue('FETCH_REGIONS', COLUMN_DISABLE);
	validator.setClass('patterns.config.web.forms.cmn.ecustgeoallocbean');
	validator.setMethod('validateCustomerIdNo');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('cusotmerIdFoNo_error', validator.getValue(ERROR));
		displayFields();
		return false;
	} else {
		if (validator.getValue(RESULT) == DATA_AVAILABLE) {
			$('#address').val(validator.getValue("ADDRESS"));
			$('#pincode').val(validator.getValue("GEO_UNIT_ID"));
			$('#cusotmerIdFoNo_desc').html(validator.getValue("NAME"));
			if (validator.getValue("GEOREGION_ERROR") != EMPTY_STRING) {
				setError('cusotmerIdFoNo_error', validator.getValue("GEOREGION_ERROR"));
				return false;
			}else{
				ecustgeoalloc_innerGrid.loadXMLString(validator.getValue(RESULT_XML));
			}	
		}
	}
	return true;
}

function cusotmerIdFoNo_val() {
	var value = $('#cusotmerIdFoNo').val();
	clearError('cusotmerIdFoNo_error');
	$('#cusotmerIdFoNo_desc').html(EMPTY_STRING);
	ecustgeoalloc_innerGrid.clearAll();
	$('#gridCheck_error').html(EMPTY_STRING);
	if (isEmpty(value)) {
		setError('cusotmerIdFoNo_error', MANDATORY);
		return false;
	}
	if (!isValidCode(value)) {
		setError('cusotmerIdFoNo_error', INVALID_FORMAT);
		displayFields();
		return false;
	} else {
		if(cusotmerIdFoNo()){
			setFocusLast('effectiveDate');
			return true;
		}else{
			return false;
		}	
		
	}
}

function effectiveDate_val() {
	var value = $('#effectiveDate').val();
	clearError('effectiveDate_error');
	if (isEmpty(value)) {
		$('#effectiveDate').val(getCBD());
		value = $('#effectiveDate').val();
	}
	if (!isValidEffectiveDate(value, 'effectiveDate_error')) {
		return false;
	}
	if ($('#action').val() == ADD) {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('CIF_NO', $('#cusotmerIdFoNo').val());
		validator.setValue('EFF_DATE', $('#effectiveDate').val());
		validator.setValue('PIN_CODE', $('#pincode').val());
		validator.setValue('ACTION', $('#action').val());
		validator.setClass('patterns.config.web.forms.cmn.ecustgeoallocbean');
		validator.setMethod('validateEffectiveDate');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('effectiveDate_error', validator.getValue(ERROR));
			return false;
		} else if (validator.getValue(RESULT) == DATA_AVAILABLE) {
			// ecustgeoalloc_innerGrid.loadXMLString(validator.getValue(RESULT_XML));
		}
	}
	if ($('#action').val() == MODIFY) {
		PK_VALUE = getEntityCode() + PK_SEPERATOR + $('#cusotmerIdFoNo').val() + PK_SEPERATOR + getTBADateFormat(value);
		if (!loadPKValues(PK_VALUE, 'effectiveDate_error'))
			return false;
	}
	setFocusLast('remarks');
	return true;
}

function loadGridValues_val() {
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('PIN_CODE', $('#pincode').val());
	validator.setClass('patterns.config.web.forms.cmn.ecustgeoallocbean');
	validator.setMethod('loadGridValues');
	validator.sendAndReceive();
	if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		ecustgeoalloc_innerGrid.loadXMLString(validator.getValue(RESULT_XML));
	}
}

function enabled_val() {
	if ($('#action').val() == MODIFY) {
		setFocusLast('remarks');
	}
	return true;
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', INVALID_REMARKS);
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', INVALID_REMARKS);
			return false;
		}
	}
	if ($('#action').val() == MODIFY) {
		if (isEmpty(value)) {
			setError('remarks_error', MANDATORY);
			return false;
		}
	}
	setFocusOnSubmit();
	return true;
}

function revalidate() {
	$('#xmlCustGrid').val(ecustgeoalloc_innerGrid.serialize());
}

function ecustgeoalloc_revaildateGrid() {
	if (ecustgeoalloc_innerGrid.getRowsNum() > 0) {
		$('#gridCheck_error').html(EMPTY_STRING);
	} else {
		setError('gridCheck_error', ADD_ATLEAST_ONE_ROW);
		errors++;
	}
	if (ecustgeoalloc_innerGrid.getCheckedRows(0) == 0) {
		setError('gridCheck_error', CHECK_ATLEAST_ONE);
		errors++;
	}
}
