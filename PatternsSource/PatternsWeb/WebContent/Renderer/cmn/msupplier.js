var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MSUPPLIER';
function init() {
	refreshTBAGrid();
	refreshQueryGrid();
	if (_redisplay) {
		if ($('#action').val() == ADD) {
			$('#enabled').prop('disabled', true);
			setCheckbox('enabled', YES);
		}
	}
	setFocusLast('supplierID');
}

function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'MSUPPLIER_MAIN', queryGrid);
}

function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}

function doclearfields(id) {
	switch (id) {
	case 'supplierID':
		if (isEmpty($('#supplierID').val())) {
			$('#supplierID_error').html(EMPTY_STRING);
			$('#supplierID_desc').html(EMPTY_STRING);
			clearNonPKFields();
			break;
		}
	}
}

function clearFields() {
	$('#supplierID').val(EMPTY_STRING);
	$('#supplierID_error').html(EMPTY_STRING);
	$('#supplierID_desc').html(EMPTY_STRING);
	clearNonPKFields();
}

function clearNonPKFields() {
	$('#name').val(EMPTY_STRING);
	$('#name_error').html(EMPTY_STRING);
	$('#shortName').val(EMPTY_STRING);
	$('#shortName_error').html(EMPTY_STRING);
	$('#pan').val(EMPTY_STRING);
	$('#pan_error').html(EMPTY_STRING);
	$('#ifsc').val(EMPTY_STRING);
	$('#ifsc_error').html(EMPTY_STRING);
	$('#branchName').val(EMPTY_STRING);
	$('#branchName_error').html(EMPTY_STRING);
	$('#bankName').val(EMPTY_STRING);
	$('#bankName_error').html(EMPTY_STRING);
	$('#city').val(EMPTY_STRING);
	$('#city_error').html(EMPTY_STRING);
	$('#stateCode').val(EMPTY_STRING);
	$('#stateCode_error').html(EMPTY_STRING);
	$('#stateCode_desc').html(EMPTY_STRING);
	setCheckbox('enabled', YES);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
}

function doHelp(id) {
	switch (id) {
	case 'supplierID':
		help('COMMON', 'HLP_SUPPLIER_IDS', $('#supplierID').val(),
				EMPTY_STRING, $('#supplierID'));
		break;
	case 'ifsc':
		help('COMMON', 'HLP_IFSC_CODE', $('#ifsc').val(), EMPTY_STRING,
				$('#ifsc'));
		break;
	}
}

function add() {
	setCheckbox('enabled', YES);
	$('#enabled').prop('disabled', true);
	$('#supplierID').focus();
}

function modify() {
	$('#enabled').prop('disabled', false);
	$('#supplierID').focus();
}

function loadData() {
	$('#supplierID').val(validator.getValue('SUPPLIER_ID'));
	$('#name').val(validator.getValue('NAME'));
	$('#shortName').val(validator.getValue('SHORT_NAME'));
	$('#pan').val(validator.getValue('PAN_NO'));
	$('#ifsc').val(validator.getValue('IFSC_CODE'));
	$('#bankName').val(validator.getValue('F1_BANK_NAME'));
	$('#branchName').val(validator.getValue('F1_BRANCH_NAME'));
	$('#city').val(validator.getValue('F1_CITY'));
	$('#stateCode').val(validator.getValue('F1_STATE_CODE'));
	$('#stateCode_desc').html(validator.getValue('F2_DESCRIPTION'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	resetLoading();
}

function view(source, primaryKey) {
	hideParent('cmn/qsupplier', source, primaryKey);
	resetLoading();
}

function validate(id) {
	switch (id) {
	case 'supplierID':
		supplierID_val();
		break;
	case 'name':
		name_val();
		break;
	case 'shortName':
		shortName_val();
		break;
	case 'pan':
		pan_val();
		break;
	case 'ifsc':
		ifsc_val();
		break;

	case 'bankName':
		bankName_val();
		break;
	case 'branchName':
		branchName_val();
		break;
	case 'city':
		city_val();
		break;
	case 'stateCode':
		stateCode_val();
		break;

	case 'enabled':
		enabled_val();
		break;
	case 'remarks':
		remarks_val();
		break;
	}
}

function backtrack(id) {
	switch (id) {
	case 'supplierID':
		setFocusLast('supplierID');
		break;
	case 'name':
		setFocusLast('supplierID');
		break;
	case 'shortName':
		setFocusLast('name');
		break;
	case 'pan':
		setFocusLast('shortName');
		break;
	case 'ifsc':
		setFocusLast('pan');
		break;

	case 'bankName':
		setFocusLast('ifsc');
		break;

	case 'branchName':
		setFocusLast('bankName');
		break;

	case 'city':
		setFocusLast('branchName');
		break;

	case 'stateCode':
		setFocusLast('city');
		break;

	case 'enabled':
		setFocusLast('ifsc');
		break;
	case 'remarks':
		if ($('#action').val() == MODIFY) {
			setFocusLast('enabled');
		} else {
			setFocusLast('ifsc');
		}
	}
}

function supplierID_val() {
	var value = $('#supplierID').val();
	clearError('supplierID_error');
	if (isEmpty(value)) {
		setError('supplierID_error', MANDATORY);
		return false;
	}
	if ($('#action').val() == ADD && !isValidCode(value)) {
		setError('supplierID_error', INVALID_FORMAT);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('SUPPLIER_ID', value);
	validator.setValue(ACTION, $('#action').val());
	validator.setClass('patterns.config.web.forms.cmn.msupplierbean');
	validator.setMethod('validateSupplierID');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('supplierID_error', validator.getValue(ERROR));
		return false;
	}
	if ($('#action').val() == MODIFY) {
		PK_VALUE = getEntityCode() + PK_SEPERATOR + value;
		if (!loadPKValues(PK_VALUE, 'supplierID_error')) {
			return false;
		}
	}
	setFocusLast('name');
	return true;
}

function name_val() {
	var value = $('#name').val();
	clearError('name_error');
	if (isEmpty(value)) {
		setError('name_error', MANDATORY);
		return false;
	}
	if (!isValidDescription(value)) {
		setError('name_error', HMS_INVALID_NAME);
		return false;
	}
	setFocusLast('shortName');
	return true;
}

function shortName_val() {
	var value = $('#shortName').val();
	clearError('shortName_error');
	if (isEmpty(value)) {
		setError('shortName_error', MANDATORY);
		return false;
	}
	if (!isValidConciseDescription(value)) {
		setError('shortName_error', HMS_INVALID_SHORT_NAME);
		return false;
	}
	setFocusLast('pan');
	return true;
}

function pan_val() {
	var value = $('#pan').val();
	clearError('pan_error');
	if (isEmpty(value)) {
		setError('pan_error', MANDATORY);
		return false;
	}
	if (!isValidPanNum(value)) {
		setError('pan_error', INVALID_PAN);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('PAN_NO', value);
	validator.setValue('PROGRAM_ID', CURRENT_PROGRAM_ID);
	validator.setValue('SUPPLIER_ID', $('#supplierID').val());
	validator.setValue(ACTION, $('#action').val());
	validator.setClass('patterns.config.web.forms.cmn.msupplierbean');
	validator.setMethod('validatePanNoExist');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('pan_error', validator.getValue(ERROR));
		return false;
	}
	setFocusLast('ifsc');
	return true;
}

function ifsc_val() {
	var value = $('#ifsc').val();
	clearError('ifsc_error');
	$('#bankName').val(EMPTY_STRING);
	$('#branchName').val(EMPTY_STRING);
	$('#city').val(EMPTY_STRING);
	$('#stateCode').val(EMPTY_STRING);
	if (isEmpty(value)) {
		setError('ifsc_error', MANDATORY);
		return false;
	}
	if (!isValidIFSCCode(value)) {
		setError('ifsc_error', INVALID_IFSC_CODE);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('IFSC_CODE', value);
	validator.setValue(ACTION, $('#action').val());
	validator.setClass('patterns.config.web.forms.cmn.msupplierbean');
	validator.setMethod('validateIFSCCode');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('ifsc_error', validator.getValue(ERROR));
		return false;
	} else {
		$('#bankName').val(validator.getValue("BANK_NAME"));
		$('#branchName').val(validator.getValue("BRANCH_NAME"));
		$('#city').val(validator.getValue("CITY"));
		$('#stateCode').val(validator.getValue("STATE_CODE"));
	}
	if ($('#action').val() == MODIFY)
		setFocusLast('enabled');
	else
		setFocusLast('remarks');
	return true;
}

function enabled_val() {
	setFocusLast('remarks');
	return true;
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', INVALID_REMARKS);
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', INVALID_REMARKS);
			return false;
		}
	}
	if ($('#action').val() == MODIFY) {
		if (isEmpty(value)) {
			setError('remarks_error', MANDATORY);
			return false;
		}
	}
	setFocusOnSubmit();
	return true;
}
