<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.cmn.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/cmn/qcashparam.js" />
	</web:dependencies>
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="qcashparam.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/cmn/qcashparam" id="qcashparam" method="POST">
				<web:viewContent id="po_view1">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="icashparam.cashGlOfc" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:codeDisplay property="cashGlOfc" id="cashGlOfc" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="icashparam.cashGlHeadcash" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:codeDisplay property="cashGlHeadcash" id="cashGlHeadcash" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="icashparam.counterCashGl" var="program" />
										</web:column>
										<web:column>
											<type:codeDisplay property="counterCashGl" id="counterCashGl" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="icashparam.fieldStaffCashGl" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:codeDisplay property="fieldStaffCashGl" id="fieldStaffCashGl" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="icashparam.fieldStaffCashGlbanking" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:codeDisplay property="fieldStaffCashGlbanking" id="fieldStaffCashGlbanking" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="icashparam.interOfcCashTransfer" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:codeDisplay property="interOfcCashTransfer" id="interOfcCashTransfer" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="icashparam.transitgl" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:codeDisplay property="transitGl" id="transitGl" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="icashparam.dominationDtlReg" var="program" />
										</web:column>
										<web:column>
											<type:checkboxDisplay property="dominationDtlReg" id="dominationDtlReg" />
										</web:column>
									</web:rowOdd>

									<web:rowEven>
										<web:column>
											<web:legend key="icashparam.vaultwithtrans" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:bankingTranCodeDisplay property="vaultWithTrans" id="vaultWithTrans" />
										</web:column>
									</web:rowEven>


									<web:rowOdd>
										<web:column>
											<web:legend key="icashparam.vaultdeptrans" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:bankingTranCodeDisplay property="vaultDepTrans" id="vaultDepTrans" />
										</web:column>
									</web:rowOdd>


									<web:rowEven>
										<web:column>
											<web:legend key="icashparam.cashtocashtrans" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:bankingTranCodeDisplay property="cashtoCashTrans" id="cashtoCashTrans" />
										</web:column>
									</web:rowEven>

									<web:rowOdd>
										<web:column>
											<web:legend key="form.remarks" var="common" />
										</web:column>
										<web:column>
											<type:remarksDisplay property="remarks" id="remarks" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
						</web:sectionBlock>
					</web:dividerBlock>
				</web:viewContent>
				<web:element property="recordExist" />
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
</web:fragment>
