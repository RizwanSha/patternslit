var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MTAXONSTATEGL';

function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}

function clearFields() {
	$('#stateCode').val(EMPTY_STRING);
	$('#taxCode').val(EMPTY_STRING);
	$('#taxCode_desc').html(EMPTY_STRING);
	$('#taxonStateCode').val(EMPTY_STRING);
	$('#taxonStateCode_desc').html(EMPTY_STRING);
	$('#taxPayGLAcctHead').val(EMPTY_STRING);
	$('#taxPayGLAcctHead_desc').html(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);

}

function loadData() {
	$('#stateCode').val(validator.getValue('STATE_CODE'));
	$('#stateCode_desc').html(validator.getValue('F1_DESCRIPTION'));
	$('#taxCode').val(validator.getValue('TAX_CODE'));
	$('#taxCode_desc').html(validator.getValue('F2_DESCRIPTION'));
	$('#taxonStateCode').val(validator.getValue('TAX_ON_STATE_CODE'));
	$('#taxonStateCode_desc').html(validator.getValue('F3_DESCRIPTION'));
	$('#taxPayGLAcctHead').val(validator.getValue('TAX_PAY_GL_HEAD_CODE'));
	$('#taxPayGLAcctHead_desc').html(validator.getValue('F4_DESCRIPTION'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadAuditFields(validator);
}


