<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.cmn.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/cmn/qentities.js" />
	</web:dependencies>
	<web:loadPK />
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="qentities.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:viewContent id="po_view1">
				<web:dividerBlock>
					<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="ientities.entityid" var="program"
												mandatory="true" />
										</web:column>
										<web:column>
											<type:codeDisplay property="entityId" id="entityId" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="ientities.entityname" var="program"
												mandatory="true" />
										</web:column>
										<web:column>
											<type:descriptionDisplay property="entityName" id="entityName" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="ientities.regofficeadd" var="program"
												mandatory="true" />
										</web:column>
										<web:column>
											<type:remarksDisplay property="regOfficeAdd" id="regOfficeAdd" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="ientities.pincode" var="program"
												mandatory="true" />
										</web:column>
										<web:column>
											<type:pinCodeDisplay property="pinCode" id="pinCode" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="ientities.statecode" var="program"
												mandatory="true" />
										</web:column>
										<web:column>
											<type:stateCodeDisplay property="stateCode" id="stateCode" />
										</web:column>
									</web:rowEven>
							<web:rowOdd>
										<web:column>
											<web:legend key="ientities.pancard" var="program"
												mandatory="true" />
										</web:column>
										<web:column>
											<type:otherInformation25Display property="panCard" id="panCard" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="form.remarks" var="common" />
										</web:column>
										<web:column>
											<type:remarksDisplay property="remarks" id="remarks" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
						<web:auditDisplay />
					</web:sectionBlock>
				</web:dividerBlock>
			</web:viewContent>
		</web:viewSubPart>
	</web:viewPart>
</web:fragment>
