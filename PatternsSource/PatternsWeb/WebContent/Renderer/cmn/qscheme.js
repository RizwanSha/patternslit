var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MSCHEME';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}

function clearFields() {
	$('#schemeCode').val(EMPTY_STRING);
	$('#description').val(EMPTY_STRING);
	$('#conciseDescription').val(EMPTY_STRING);
	$('#schemeFor').selectedIndex = 0;
	$('#parentSchemeCode').val(EMPTY_STRING);
	setCheckbox('schemeHasEnrol', NO);
	$('#usingPidType').val(EMPTY_STRING);
	$('#nameOfIdEnrolNo').val(EMPTY_STRING);
	setCheckbox('paymentConrolTokenRef', NO);
	$('#nameOfTokenRef').val(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
}

function loadData() {
	$('#schemeCode').val(validator.getValue('SCHEME_CODE'));
	$('#description').val(validator.getValue('DESCRIPTION'));
	$('#conciseDescription').val(validator.getValue('CONCISE_DESCRIPTION'));
	$('#schemeFor').val(validator.getValue('SCHEME_FOR'));
	$('#parentSchemeCode').val(validator.getValue('SCHEME_CODE'));
	$('#parentSchemeCode_desc').html(validator.getValue('F2_DESCRIPTION'));
	setCheckbox('schemeHasEnrol', validator.getValue('SCHEME_HAS_ENROL'));
	$('#usingPidType').val(validator.getValue('PID_CODE'));
	$('#usingPidType_desc').html(validator.getValue('F1_DESCRIPTION'));
	$('#nameOfIdEnrolNo').val(validator.getValue('ENROL_DESCRIPTION'));
	setCheckbox('paymentConrolTokenRef', validator.getValue('PAY_CONTROL_TOKEN_REF'));
	$('#nameOfTokenRef').val(validator.getValue('TOKEN_REF_DESCRIPTION'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadAuditFields(validator);
}