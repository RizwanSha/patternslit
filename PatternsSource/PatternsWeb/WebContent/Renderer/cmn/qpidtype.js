var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MPIDTYPE';

function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}
function clearFields() {
	$('#pidTypeCode').val(EMPTY_STRING);
	$('#description').val(EMPTY_STRING);
	$('#conciseDescription').val(EMPTY_STRING);
	setCheckbox('codeMeantForIncomeTaxPayerId', NO);
	setCheckbox('codemeantfornationalID', NO);
	setCheckbox('codeMeantForPassport', NO);
	setCheckbox('documentCanUseForPersonIdentification', NO);
	setCheckbox('documentCanUseForAddressIdentifaction', NO);
	$('#countryCode').val(EMPTY_STRING);
	$('#issueOfDocument').val(EMPTY_STRING);
	setCheckbox('documentHasIssueDate', NO);
	setCheckbox('documenthasExpiryDate', NO);
	setCheckbox('officiallyValidDoc', NO);
	setCheckbox('underSimpMeasuresKyc', NO);
	setCheckbox('officiallyValidDocument', NO);
	setCheckbox('underSimplifiedMeasuresKyc', NO);
	setCheckbox('altAccntId', NO);
	setCheckbox('pidForBplCard', NO);
	$('#remarks').val(EMPTY_STRING);
}

function loadData() {
	$('#pidTypeCode').val(validator.getValue('PID_CODE'));
	$('#description').val(validator.getValue('DESCRIPTION'));
	$('#conciseDescription').val(validator.getValue('CONCISE_DESCRIPTION'));
	setCheckbox('codeMeantForIncomeTaxPayerId', validator.getValue('INCOME_TAX_PAYER_ID'));
	setCheckbox('codemeantfornationalID', validator.getValue('NATIONAL_ID'));
	setCheckbox('codeMeantForPassport', validator.getValue('PASSPORT_ID'));
	setCheckbox('documentCanUseForPersonIdentification', validator.getValue('DOC_USED_FOR_ID_PROOF'));
	setCheckbox('officiallyValidDoc', validator.getValue('ID_PROOF_OFFICIALLY_VALID_DOC'));
	setCheckbox('underSimpMeasuresKyc', validator.getValue('ID_PROOF_SIMPLIFIED_MEASURE_KYC'));
	setCheckbox('documentCanUseForAddressIdentifaction', validator.getValue('DOC_USED_FOR_ADDR_PROOF'));
	setCheckbox('officiallyValidDocument', validator.getValue('ADDR_PROOF_OFFICIALLY_VALID_DOC'));
	setCheckbox('underSimplifiedMeasuresKyc', validator.getValue('ADDR_PROOF_SIMPLIFIED_MEASURE_KYC'));
	$('#countryCode').val(validator.getValue('DOC_APPL_FOR_COUNTRY'));
	$('#countryCode_desc').html(validator.getValue('F1_DESCRIPTION'));
	$('#issueOfDocument').val(validator.getValue('DOC_ISSUER_NAME'));
	setCheckbox('documentHasIssueDate', validator.getValue('DOC_HAS_ISSUE_DATE'));
	setCheckbox('documenthasExpiryDate', validator.getValue('DOC_HAS_EXPIRY_DATE'));
	setCheckbox('altAccntId', validator.getValue('ALT_ACCOUNT_ID'));
	setCheckbox('pidForBplCard', validator.getValue('PID_FOR_BPL_CARD'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadAuditFields(validator);
}
