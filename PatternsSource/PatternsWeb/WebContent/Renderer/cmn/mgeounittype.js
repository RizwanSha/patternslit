var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MGEOUNITTYPE';
function init() {
	refreshTBAGrid();
	refreshQueryGrid();
	if ($('#action').val() == ADD) {
		$('#enabled').prop('disabled', true);
		setCheckbox('enabled', YES);
	}
}

function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'MGEOUNITTYPE_MAIN', queryGrid);
}

function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}

function clearFields() {
	$('#geoUnitStructureCode').val(EMPTY_STRING);
	$('#geoUnitStructureCode_error').html(EMPTY_STRING);
	$('#geoUnitStructureCode_desc').html(EMPTY_STRING);
	$('#geoUnitType').val(EMPTY_STRING);
	$('#geoUnitType_error').html(EMPTY_STRING);
	$('#geoUnitType_desc').html(EMPTY_STRING);
	clearNonPKFields();
}
function clearNonPKFields() {
	$('#description').val(EMPTY_STRING);
	$('#description_error').html(EMPTY_STRING);
	$('#conciseDescription').val(EMPTY_STRING);
	$('#conciseDescription_error').html(EMPTY_STRING);
	setCheckbox('enabled', YES);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
}

function doclearfields(id) {
	switch (id) {
	case 'geoUnitStructureCode':
		if (isEmpty($('#geoUnitStructureCode').val())) {
			$('#geoUnitStructureCode_error').html(EMPTY_STRING);
			$('#geoUnitStructureCode_desc').html(EMPTY_STRING);
			break;
		}
	case 'geoUnitType':
		if (isEmpty($('#geoUnitType').val())) {
			$('#geoUnitType_error').html(EMPTY_STRING);
			$('#geoUnitType_desc').html(EMPTY_STRING);
			clearNonPKFields();
			break;
		}
	}
}

function doHelp(id) {
	switch (id) {
	case 'geoUnitStructureCode':
		help('COMMON', 'HLP_GEOUNITSTRUC_REQ', $('#geoUnitStructureCode').val(), EMPTY_STRING, $('#geoUnitStructureCode'));
		break;

	case 'geoUnitType':
		help('COMMON', 'HLP_GEOUNITTYPE_M', $('#geoUnitType').val(), $('#geoUnitStructureCode').val(), $('#geoUnitType'));
		break;
	}
}

function add() {
	
	$('#geoUnitStructureCode').focus();
	setCheckbox('enabled', YES);
	$('#enabled').prop('disabled', true);
}
function modify() {
	
	$('#geoUnitStructureCode').focus();
	if ($('#action').val() == MODIFY) {
		$('#enabled').prop('disabled', false);
	} else {
		$('#enabled').prop('disabled', true);
	}
}
function loadData() {
	$('#geoUnitStructureCode').val(validator.getValue('GEO_UNIT_STRUC_CODE'));
	$('#geoUnitStructureCode_desc').val(validator.getValue('F1_DESCRIPTION'));
	$('#geoUnitType').val(validator.getValue('GEO_UNIT_TYPE'));
	$('#description').val(validator.getValue('DESCRIPTION'));
	$('#conciseDescription').val(validator.getValue('CONCISE_DESCRIPTION'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	resetLoading();
}
function view(source, primaryKey) {
	hideParent('cmn/qgeounittype', source, primaryKey);
	resetLoading();
}
function validate(id) {
	switch (id) {
	case 'geoUnitStructureCode':
		geoUnitStructureCode_val();
		break;
	case 'geoUnitType':
		geoUnitType_val();
		break;
	case 'description':
		description_val();
		break;
	case 'conciseDescription':
		conciseDescription_val();
		break;
	case 'enabled':
		enabled_val();
		break;
	case 'remarks':
		remarks_val();
		break;
	}
}

function backtrack(id) {
	switch (id) {
	case 'geoUnitStructureCode':
		setFocusLast('geoUnitStructureCode');
		break;
	case 'geoUnitType':
		setFocusLast('geoUnitStructureCode');
		break;
	case 'description':
		setFocusLast('geoUnitType');
		break;
	case 'conciseDescription':
		setFocusLast('description');
		break;
	case 'enabled':
		setFocusLast('conciseDescription');
		break;
	case 'remarks':
		if ($('#action').val() == MODIFY) {
			setFocusLast('enabled');
		} else {
			setFocusLast('conciseDescription');
		}
		break;
	}
}

function geoUnitStructureCode_val() {
	var value = $('#geoUnitStructureCode').val();
	clearError('geoUnitStructureCode_error');
	if (isEmpty(value)) {
		setError('geoUnitStructureCode_error', HMS_MANDATORY);
		return false;
	} else if ($('#action').val() == ADD && !isValidCode(value)) {
		setError('geoUnitStructureCode_error', HMS_INVALID_FORMAT);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('GEO_UNIT_STRUC_CODE', $('#geoUnitStructureCode').val());
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.cmn.mgeounittypebean');
		validator.setMethod('validateGeoUnitStructureCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('geoUnitStructureCode_error', validator.getValue(ERROR));
			return false;
		}else{
			if (validator.getValue(RESULT) == DATA_AVAILABLE) {
				$('#geoUnitStructureCode_desc').html(validator.getValue('DESCRIPTION'));
			}
		}
	}
	setFocusLast('geoUnitType');
	return true;
}
function geoUnitType_val() {
	var value = $('#geoUnitType').val();
	clearError('geoUnitType_error');
	if (isEmpty(value)) {
		setError('geoUnitType_error', HMS_MANDATORY);
		setFocusLast('geoUnitType');
		return false;
	} else if ($('#action').val() == ADD && !isValidCode(value)) {
		setError('geoUnitType_error', HMS_INVALID_FORMAT);
		setFocusLast('geoUnitType');
		return false;
	} else {
		if ($('#action').val() == ADD) {
			validator.clearMap();
			validator.setMtm(false);
			validator.setValue('GEO_UNIT_STRUC_CODE', $('#geoUnitStructureCode').val());
			validator.setValue('GEO_UNIT_TYPE', $('#geoUnitType').val());
			validator.setValue(ACTION, $('#action').val());
			validator.setClass('patterns.config.web.forms.cmn.mgeounittypebean');
			validator.setMethod('validateGeoUnitType');
			validator.sendAndReceive();
			if (validator.getValue(ERROR) != EMPTY_STRING) {
				setError('geoUnitType_error', validator.getValue(ERROR));
				setFocusLast('geoUnitType');
				return false;
			}
		} else {
			PK_VALUE = $('#geoUnitStructureCode').val() + PK_SEPERATOR + $('#geoUnitType').val();
			if(!loadPKValues(PK_VALUE, 'geoUnitType_error')){
				return false;
			}
		}
		
	}
	setFocusLast('description');
	return true;
}

function description_val() {
	var value = $('#description').val();
	clearError('description_error');
	if (isEmpty(value)) {
		setError('description_error', HMS_MANDATORY);
		return false;
	}
	if (!isValidDescription(value)) {
		setError('description_error', HMS_INVALID_DESCRIPTION);
		return false;
	}
	setFocusLast('conciseDescription');
	return true;
}

function conciseDescription_val() {
	var value = $('#conciseDescription').val();
	clearError('conciseDescription_error');
	if (isEmpty(value)) {
		setError('conciseDescription_error', HMS_MANDATORY);
		return false;
	}
	if (!isValidConciseDescription(value)) {
		setError('conciseDescription_error', HMS_INVALID_CONCISE_DESCRIPTION);
		return false;
	}
	if ($('#action').val() == ADD) {
		setFocusLast('remarks');
	} else {
		setFocusLast('enabled');
	}
	return true;
}

function enabled_val() {
	if ($('#action').val() == MODIFY) {
		setFocusLast('remarks');
	}
	return true;
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', HMS_INVALID_REMARKS);
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', HMS_INVALID_REMARKS);
			return false;
		}
	}
	if ($('#action').val() == MODIFY) {
		if (isEmpty(value)) {
			setError('remarks_error', HMS_MANDATORY);
			return false;
		}
	}
	setFocusOnSubmit();
	return true;

}
