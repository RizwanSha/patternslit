var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MORGBUSINESSCORR';

function init() {
	if (isEmpty($('#countryCode').val()) || isEmpty($('#geoUnitStructStateCodeHid').val()) || isEmpty($('#geoUnitStructDistCodeHid').val())) {
		alert(PBS_GEO_PARAM_NOT_CONFIGURED);
		var url = getBasePath() + $('#redirectLandingPagePath').val();
		window.location.href = url;
	} else {
		refreshTBAGrid();
		refreshQueryGrid();
		if (_redisplay) {
			if ($('#action').val() == ADD) {
				$('#enabled').prop('disabled', true);
				setCheckbox('enabled', YES);
			}
			orgBusinessCorrGrid.clearAll();
			if (!isEmpty($('#xmlOrgBusinessCorrGrid').val())) {
				orgBusinessCorrGrid.loadXMLString($('#xmlOrgBusinessCorrGrid').val());
			}
		}
	}
}

function loadGrid() {
	loadGridQuery(CURRENT_PROGRAM_ID, 'MORGBUSINESSCORR_GRID', orgBusinessCorrGrid);
}

function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'MORGBUSINESSCORR_MAIN');
}

function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}

function add() {
	$('#organizationCode').focus();
	$('#enabled').prop('disabled', true);
	setCheckbox('enabled', YES);
}

function modify() {
	$('#organizationCode').focus();
	$('#accrsDays').prop('disabled', true);
	if ($('#action').val() == MODIFY) {
		$('#enabled').prop('disabled', false);
	} else {
		$('#enabled').prop('disabled', true);
	}
}

function view(source, primaryKey) {
	hideParent('cmn/qorgbusinesscorr', source, primaryKey);
	resetLoading();
}

function clearFields() {
	$('#organizationCode').val(EMPTY_STRING);
	$('#organizationCode_desc').html(EMPTY_STRING);
	$('#organizationCode_error').html(EMPTY_STRING);
	$('#effectiveDate').val(EMPTY_STRING);
	$('#effectiveDate_error').html(EMPTY_STRING);
	clearNonPKFields();
}

function clearNonPKFields() {
	if ($('#action').val() == MODIFY)
		setCheckbox('enabled', NO);
	setCheckbox('enableBusinesCorr', NO);
	$('#enableBusinesCorr_error').html(EMPTY_STRING);
	$('#enabled_error').html(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
	orgBusinessCorrGrid.clearAll();
	orgBusinessCorrGridClearGridFields();
}

function orgBusinessCorrGridClearGridFields() {
	$('#geoStateCode').val(EMPTY_STRING);
	$('#geoStateCode_desc').html(EMPTY_STRING);
	$('#geoStateCode_error').html(EMPTY_STRING);
	$('#geoDistCode').val(EMPTY_STRING);
	$('#geoDistCode_desc').html(EMPTY_STRING);
	$('#geoDistCode_error').html(EMPTY_STRING);
	$('#xmlOrgBusinessCorrGrid').val(EMPTY_STRING);
}

function loadData() {
	$('#organizationCode').val(validator.getValue('ORG_CODE'));
	$('#organizationCode_desc').html(validator.getValue('F1_DESCRIPTION'));
	$('#effectiveDate').val(validator.getValue('EFF_DATE'));
	setCheckbox('enableBusinesCorr', decodeSTB(validator.getValue('ENABLEDASCORRDENT')));
	setCheckbox('enabled', decodeSTB(validator.getValue('ENABLED')));
	$('#remarks').val(validator.getValue('REMARKS'));
	resetLoading();
	loadGrid();
}

function doHelp(id) {
	switch (id) {
	case 'organizationCode':
		help('COMMON', 'HLP_ORG_CODE', $('#organizationCode').val(), EMPTY_STRING, $('#organizationCode'));
		break;
	case 'effectiveDate':
		help('MORGBUSINESSCORR', 'HLP_ORGBUSSCORR_EFF_DT', $('#effectiveDate').val(), $('#organizationCode').val(), $('#effectiveDate'));
		break;
	case 'geoStateCode':
		help('MORGBUSINESSCORR', 'HLP_GEOUNIT_STATE_CODE', $('#geoStateCode').val(), $('#countryCode').val() + PK_SEPERATOR + $('#geoUnitStructStateCodeHid').val(), $('#geoStateCode'));
		break;
	case 'geoDistCode':
		help('MORGBUSINESSCORR', 'HLP_GEOUNIT_DISTRICT_CODE', $('#geoDistCode').val(), $('#countryCode').val() + PK_SEPERATOR + $('#geoStateCode').val() + PK_SEPERATOR + $('#geoUnitStructDistCodeHid').val(), $('#geoDistCode'));
		break;
	}
}

function validate(id) {
	valMode = true;
	switch (id) {
	case 'organizationCode':
		organizationCode_val(valMode);
		break;
	case 'effectiveDate':
		effectiveDate_val(valMode);
		break;
	case 'enableBusinesCorr':
		enableBusinesCorr_val(valMode);
		break;
	case 'geoStateCode':
		geoStateCode_val(valMode);
		break;
	case 'geoDistCode':
		geoDistCode_val(valMode);
		break;
	case 'enabled':
		enabled_val(valMode);
		break;
	case 'remarks':
		remarks_val(valMode);
		break;
	}
}
function doclearfields(id) {
	switch (id) {
	case 'organizationCode':
		if (isEmpty($('#organizationCode').val())) {
			clearFields();
			break;
		}
	case 'effectiveDate':
		if (isEmpty($('#effectiveDate').val())) {
			$('#effectiveDate_error').html(EMPTY_STRING);
			$('#effectiveDate_desc').html(EMPTY_STRING);
			clearNonPKFields();
			break;
		}
	}
}

function backtrack(id) {
	switch (id) {
	case 'organizationCode':
		setFocusLast('organizationCode');
		break;
	case 'effectiveDate':
		setFocusLast('organizationCode');
		break;
	case 'enableBusinesCorr':
		setFocusLast('effectiveDate');
		break;
	case 'geoStateCode':
		setFocusLast('enableBusinesCorr');
		break;
	case 'geoDistCode':
		setFocusLast('geoStateCode');
		break;
	case 'orgBusinessCorrGrid_add':
		setFocusLast('geoDistCode');
		break;
	case 'enabled':
		setFocusLast('geoStateCode');
		break;
	case 'remarks':
		if ($('#action').val() == MODIFY) {
			setFocusLast('enabled');
		} else {
			setFocusLast('geoStateCode');
		}
		break;
	}
}

function organizationCode_val(valMode) {
	var value = $('#organizationCode').val();
	clearError('organizationCode_error');
	if (isEmpty(value)) {
		setError('organizationCode_error', MANDATORY);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('ORG_CODE', $('#organizationCode').val());
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.cmn.morgbusinesscorrbean');
		validator.setMethod('validateOrganizationCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('organizationCode_error', validator.getValue(ERROR));
			return false;
		} else if (validator.getValue(RESULT) == DATA_AVAILABLE) {
			$('#organizationCode_desc').html(validator.getValue("DESCRIPTION"));
		}
	}
	if (valMode)
		setFocusLast('effectiveDate');
	return true;
}

function effectiveDate_val(valMode) {
	var value = $('#effectiveDate').val();
	clearError('effectiveDate_error');
	if (isEmpty(value)) {
		value = getCBD();
		$('#effectiveDate').val(value);
	} else if (!isValidEffectiveDate(value, 'effectiveDate_error')) {
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('ORG_CODE', $('#organizationCode').val());
	validator.setValue('EFF_DATE', $('#effectiveDate').val());
	validator.setValue(ACTION, $('#action').val());
	validator.setClass('patterns.config.web.forms.cmn.morgbusinesscorrbean');
	validator.setMethod('validateEffectiveDate');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('effectiveDate_error', validator.getValue(ERROR));
		return false;
	}
	if ($('#action').val() == MODIFY) {
		PK_VALUE = getEntityCode() + PK_SEPERATOR + $('#organizationCode').val() + PK_SEPERATOR + getTBADateFormat($('#effectiveDate').val());
		if (!loadPKValues(PK_VALUE, 'effectiveDate_error'))
			return false;
	}
	if (valMode)
		setFocusLast('enableBusinesCorr');
	return true;
}

function enableBusinesCorr_val(valMode) {
	if (valMode)
		setFocusLast('geoStateCode');
	return true;
}

function geoStateCode_val(valMode) {
	var value = $('#geoStateCode').val();
	clearError('geoStateCode_error');
	if (isEmpty(value)) {
		setError('geoStateCode_error', MANDATORY);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('GEOC_COU_CODE', $('#countryCode').val());
		validator.setValue('GEO_UNIT_STATE_ID', $('#geoStateCode').val());
		validator.setValue('GEO_UNIT_STRUC_STATE_CODE', $('#geoUnitStructStateCodeHid').val());
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.cmn.morgbusinesscorrbean');
		validator.setMethod('validateGeoStateCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('geoStateCode_error', validator.getValue(ERROR));
			return false;
		} else if (validator.getValue(RESULT) == DATA_AVAILABLE) {
			$('#geoStateCode_desc').html(validator.getValue("DESCRIPTION"));
		}
	}
	if (valMode)
		setFocusLast('geoDistCode');
	return true;
}

function geoDistCode_val(valMode) {
	var value = $('#geoDistCode').val();
	clearError('geoDistCode_error');
	if (isEmpty(value)) {
		setError('geoDistCode_error', MANDATORY);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('GEOC_COU_CODE', $('#countryCode').val());
		validator.setValue('GEO_UNIT_STATE_ID', $('#geoStateCode').val());
		validator.setValue('GEO_UNIT_DISTRICT_ID', $('#geoDistCode').val());
		validator.setValue('GEO_UNIT_STRUC_DIST_CODE', $('#geoUnitStructDistCodeHid').val());
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.cmn.morgbusinesscorrbean');
		validator.setMethod('validateGeoDistCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('geoDistCode_error', validator.getValue(ERROR));
			return false;
		} else if (validator.getValue(RESULT) == DATA_AVAILABLE) {
			$('#geoDistCode_desc').html(validator.getValue("DESCRIPTION"));
		}
	}
	if (valMode)
		setFocusLastOnGridSubmit('orgBusinessCorrGrid');
	return true;
}

function enabled_val(valMode) {
	if (valMode)
		if ($('#action').val() == MODIFY) {
			setFocusLast('remarks');
		}
	return true;
}

function remarks_val(valMode) {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', INVALID_REMARKS);
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', INVALID_REMARKS);
			return false;
		}
	}
	if ($('#action').val() == MODIFY) {
		if (isEmpty(value)) {
			setError('remarks_error', MANDATORY);
			return false;
		}
	}
	if (valMode)
		setFocusOnSubmit();
	return true;
}

function gridExit(id) {
	switch (id) {
	case 'geoStateCode':
		orgBusinessCorrGridClearGridFields();
		$('#xmlOrgBusinessCorrGrid').val(orgBusinessCorrGrid.serialize());
		if ($('#action').val() == MODIFY) {
			setFocusLast('enabled');
		} else
			setFocusLast('remarks');
		break;
	}
}

function gridDeleteRow(id) {
	switch (id) {
	case 'geoStateCode':
		deleteGridRecord(orgBusinessCorrGrid);
		break;
	}
}

function orgBusinessCorrGridDuplicateCheck(editMode, editRowId) {
	var currentValue = [ $('#geoStateCode').val(), $('#geoDistCode').val() ];
	return _grid_duplicate_check(orgBusinessCorrGrid, currentValue, [ 2, 4 ]);
}

function orgBusinessCorrGridAddGrid(editMode, editRowId) {
	if (geoStateCode_val(false)) {
		if (orgBusinessCorrGridDuplicateCheck(editMode, editRowId)) {
			var field_values = [ 'false', $('#countryCode').val(), $('#geoStateCode').val(), $('#geoStateCode_desc').html(), $('#geoDistCode').val(), $('#geoDistCode_desc').html() ];
			_grid_updateRow(orgBusinessCorrGrid, field_values);
			orgBusinessCorrGridClearGridFields();
			$('#geoStateCode').focus();
			return true;
		} else {
			setError('geoStateCode_error', DUPLICATE_DATA);
			return false;
		}
	} else {
		$('#geoStateCode').focus();
		return false;
	}
}

function orgBusinessCorrGridEditGrid(rowId) {
	$('#countryCode').val(orgBusinessCorrGrid.cells(rowId, 1).getValue());
	$('#geoStateCode').val(orgBusinessCorrGrid.cells(rowId, 2).getValue());
	$('#geoStateCode_desc').html(orgBusinessCorrGrid.cells(rowId, 3).getValue());
	$('#geoDistCode').val(orgBusinessCorrGrid.cells(rowId, 4).getValue());
	$('#geoDistCode_desc').html(orgBusinessCorrGrid.cells(rowId, 5).getValue());
	$('#geoStateCode').focus();
}

function revalidate() {
	orgBusinessCorr_revaildateGrid();
}

function orgBusinessCorr_revaildateGrid() {
	if (orgBusinessCorrGrid.getRowsNum() > 0) {
		$('#xmlOrgBusinessCorrGrid').val(orgBusinessCorrGrid.serialize());
		$('#geoStateCode_error').html(EMPTY_STRING);
	} else {
		orgBusinessCorrGrid.clearAll();
		$('#xmlOrgBusinessCorrGrid').val(orgBusinessCorrGrid.serialize());
		setError('geoStateCode_error', ADD_ATLEAST_ONE_ROW);
		errors++;
	}
}