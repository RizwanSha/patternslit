var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'ITAXBYUSPRODUCT';
function init() {
	refreshTBAGrid();
	refreshQueryGrid();
	if (_redisplay) {

		if (!$('#taxBorne').is(':checked')) {
			if ($('#accountingHead_error').html().trim() != EMPTY_STRING)
				$('#accountingHead').prop('disabled', false);
			else
				$('#accountingHead').prop('disabled', true);
		} else {
			$('#accountingHead').prop('disabled', false);
		}

		if ($('#action').val() == ADD) {
			$('#enabled').prop('disabled', true);
			setCheckbox('enabled', YES);
		}
	}
}

function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'ITAXBYUSPRODUCT_MAIN', queryGrid);
}

function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}

function doHelp(id) {
	switch (id) {
	case 'productCode':
		help('COMMON', 'HLP_LEASE_PROD_CODE', $('#productCode').val(), EMPTY_STRING, $('#productCode'));
		break;
	case 'stateCode':
		help('COMMON', 'HLP_STATE_CODE', $('#stateCode').val(), EMPTY_STRING, $('#stateCode'));
		break;
	case 'taxCode':
		help('ITAXBYUSPRODUCT', 'HLP_TAX_CODE', $('#taxCode').val(), $('#stateCode').val(), $('#taxCode'));
		break;
	case 'effectiveDate':
		if ($('#action').val() == MODIFY) {
			help('ITAXBYUSPRODUCT', 'HLP_EFF_DATE', $('#effectiveDate').val(), $('#productCode').val() + PK_SEPERATOR + $('#stateCode').val() + PK_SEPERATOR + $('#taxCode').val(), $('#effectiveDate'));
		}
		break;
	case 'accountingHead':
		help('COMMON', 'HLP_GL_HEAD_CODE', $('#accountingHead').val(), EMPTY_STRING, $('#accountingHead'));
		break;
	}
}

function clearFields() {
	$('#productCode').val(EMPTY_STRING);
	$('#productCode_desc').html(EMPTY_STRING);
	$('#productCode_error').html(EMPTY_STRING);
	$('#stateCode').val(EMPTY_STRING);
	$('#stateCode_desc').html(EMPTY_STRING);
	$('#stateCode_error').html(EMPTY_STRING);
	$('#taxCode').val(EMPTY_STRING);
	$('#taxCode_desc').html(EMPTY_STRING);
	$('#taxCode_error').html(EMPTY_STRING);
	$('#effectiveDate').val(EMPTY_STRING);
	$('#effectiveDate_desc').html(EMPTY_STRING);
	$('#effectiveDate_error').html(EMPTY_STRING);
	clearNonPKFields();
}

function clearNonPKFields() {
	setCheckbox('enabled', YES);
	setCheckbox('taxBorne', NO);
	$('#accountingHead').val(EMPTY_STRING);
	$('#accountingHead_desc').html(EMPTY_STRING);
	$('#accountingHead_error').html(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
}

function doclearfields(id) {
	switch (id) {
	case 'productCode':
		if (isEmpty($('#productCode').val())) {
			clearFields();
			break;
		}
	case 'stateCode':
		if (isEmpty($('#stateCode').val())) {
			$('#stateCode_desc').html(EMPTY_STRING);
			$('#stateCode_error').html(EMPTY_STRING);
			$('#taxCode').val(EMPTY_STRING);
			$('#taxCode_desc').html(EMPTY_STRING);
			$('#taxCode_error').html(EMPTY_STRING);
			$('#effectiveDate').val(EMPTY_STRING);
			$('#effectiveDate_desc').html(EMPTY_STRING);
			$('#effectiveDate_error').html(EMPTY_STRING);
			clearNonPKFields();
			break;
		}
	case 'taxCode':
		if (isEmpty($('#taxCode').val())) {
			$('#taxCode_desc').html(EMPTY_STRING);
			$('#taxCode_error').html(EMPTY_STRING);
			$('#effectiveDate').val(EMPTY_STRING);
			$('#effectiveDate_desc').html(EMPTY_STRING);
			$('#effectiveDate_error').html(EMPTY_STRING);
			clearNonPKFields();
			break;
		}
	case 'effectiveDate':
		if (isEmpty($('#effectiveDate').val())) {
			$('#effectiveDate').val(EMPTY_STRING);
			$('#effectiveDate_desc').html(EMPTY_STRING);
			$('#effectiveDate_error').html(EMPTY_STRING);
			clearNonPKFields();
			break;
		}
	}
}

function add() {
	$('#lesseeCode').focus();
	if ($('#taxBorne').is(':checked')) {
		$('#accountingHead').attr('readOnly', false);
	} else {
		$('#accountingHead').prop('readOnly', true);
	}
	$('#effectiveDate_look').prop('disabled', true);
	setCheckbox('enabled', YES);
	$('#enabled').prop('disabled', true);
}

function modify() {
	$('#productCode').focus();
	if ($('#action').val() == MODIFY) {
		$('#enabled').prop('disabled', false);
	} else {
		$('#enabled').prop('disabled', true);
	}
}

function loadData() {
	$('#productCode').val(validator.getValue('LEASE_PRODUCT_CODE'));
	$('#productCode_desc').html(validator.getValue('F1_DESCRIPTION'));
	$('#stateCode').val(validator.getValue('STATE_CODE'));
	$('#stateCode_desc').html(validator.getValue('F3_DESCRIPTION'));
	$('#taxCode').val(validator.getValue('TAX_CODE'));
	$('#taxCode_desc').html(validator.getValue('F2_DESCRIPTION'));
	$('#effectiveDate').val(validator.getValue('EFF_DATE'));
	setCheckbox('taxBorne', validator.getValue('TAX_BY_US'));
	$('#accountingHead').val(validator.getValue('TAX_BYUS_GL_HEAD_CODE'));
	$('#accountingHead_desc').html(validator.getValue('F4_DESCRIPTION'));
	if ($('#taxBorne').is(':checked')) {
		$('#accountingHead').attr('readOnly', false);
	} else {
		$('#accountingHead').prop('readOnly', true);
	}
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	resetLoading();
}

function view(source, primaryKey) {
	hideParent('cmn/qtaxbyusproduct', source, primaryKey);
	resetLoading();
}

function validate(id) {
	switch (id) {
	case 'productCode':
		productCode_val();
		break;
	case 'stateCode':
		stateCode_val();
		break;
	case 'taxCode':
		taxCode_val();
		break;
	case 'effectiveDate':
		effectiveDate_val();
		break;
	case 'taxBorne':
		taxBorne_val();
		break;
	case 'accountingHead':
		accountingHead_val();
		break;
	case 'enabled':
		enabled_val();
		break;
	case 'remarks':
		remarks_val();
		break;
	}
}

function backtrack(id) {
	switch (id) {
	case 'productCode':
		setFocusLast('productCode');
		break;
	case 'stateCode':
		setFocusLast('productCode');
		break;
	case 'taxCode':
		setFocusLast('stateCode');
		break;
	case 'effectiveDate':
		setFocusLast('taxCode');
		break;
	case 'taxBorne':
		setFocusLast('effectiveDate');
		break;
	case 'accountingHead':
		setFocusLast('taxBorne');
		break;
	case 'enabled':
		if ($('#taxBorne').is(':checked'))
			setFocusLast('accountingHead');
		else
			setFocusLast('taxBorne');
		break;
	case 'remarks':
		if ($('#taxBorne').is(':checked')) {
			setFocusLast('accountingHead');
		} else {
			setFocusLast('taxBorne');
			return true;
		}
		if ($('#action').val() == MODIFY) {
			setFocusLast('enabled');
		} else {
			setFocusLast('accountingHead');
		}
		break;
	}
}

function productCode_val() {
	var value = $('#productCode').val();
	clearError('productCode_error');
	$('#productCode_desc').html(EMPTY_STRING);
	if (isEmpty(value)) {
		setError('productCode_error', MANDATORY);
		return false;
	}
	if (!isValidCode(value)) {
		setError('productCode_error', INVALID_FORMAT);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('LEASE_PRODUCT_CODE', value);
	validator.setValue(ACTION, USAGE);
	validator.setClass('patterns.config.web.forms.cmn.itaxbyusproductbean');
	validator.setMethod('validateProductCode');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('productCode_error', validator.getValue(ERROR));
		return false;
	} else if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		$('#productCode_desc').html(validator.getValue('DESCRIPTION'));

	}
	setFocusLast('stateCode');
	return true;
}

function stateCode_val() {
	var value = $('#stateCode').val();
	clearError('stateCode_error');
	$('#stateCode_desc').html(EMPTY_STRING);
	if (isEmpty(value)) {
		setError('stateCode_error', MANDATORY);
		return false;
	}
	if (!isValidCode(value)) {
		setError('stateCode_error', INVALID_FORMAT);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('LEASE_PRODUCT_CODE', $('#productCode').val());
	validator.setValue('STATE_CODE', value);
	validator.setValue(ACTION, $('#action').val());
	validator.setClass('patterns.config.web.forms.cmn.itaxbyusproductbean');
	validator.setMethod('validateStateCode');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('stateCode_error', validator.getValue(ERROR));
		return false;
	} else if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		$('#stateCode_desc').html(validator.getValue('DESCRIPTION'));

	}
	setFocusLast('taxCode');
	return true;
}

function taxCode_val() {
	var value = $('#taxCode').val();
	clearError('taxCode_error');
	$('#taxCode_desc').html(EMPTY_STRING);
	if (isEmpty(value)) {
		setError('taxCode_error', MANDATORY);
		return false;
	}
	if (!isValidCode(value)) {
		setError('taxCode_error', INVALID_FORMAT);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('LEASE_PRODUCT_CODE', $('#productCode').val());
		validator.setValue('STATE_CODE', $('#stateCode').val());
		validator.setValue('TAX_CODE', value);
		validator.setValue(ACTION, $('#action').val());
		validator.setClass('patterns.config.web.forms.cmn.itaxbyusproductbean');
		validator.setMethod('validateTaxCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('taxCode_error', validator.getValue(ERROR));
			return false;
		} else if (validator.getValue(RESULT) == DATA_AVAILABLE) {
			$('#taxCode_desc').html(validator.getValue('DESCRIPTION'));

		}
		setFocusLast('effectiveDate');
		return true;
	}
}
function effectiveDate_val() {
	var value = $('#effectiveDate').val();
	clearError('effectiveDate_error');
	if (isEmpty(value)) {
		$('#effectiveDate').val(getCBD());
		value = $('#effectiveDate').val();
	}
	if (!isValidEffectiveDate(value, 'effectiveDate_error')) {
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('LEASE_PRODUCT_CODE', $('#productCode').val());
		validator.setValue('STATE_CODE', $('#stateCode').val());
		validator.setValue('TAX_CODE', $('#taxCode').val());
		validator.setValue('EFF_DATE', $('#effectiveDate').val());
		validator.setValue(ACTION, $('#action').val());
		validator.setClass('patterns.config.web.forms.cmn.itaxbyusproductbean');
		validator.setMethod('validateEffectiveDate');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('effectiveDate_error', validator.getValue(ERROR));
			return false;
		} else {
			if ($('#action').val() == MODIFY) {
				PK_VALUE = getEntityCode() + PK_SEPERATOR + $('#productCode').val() + PK_SEPERATOR + $('#stateCode').val() + PK_SEPERATOR + $('#taxCode').val() + PK_SEPERATOR + $('#effectiveDate').val();
				if (!loadPKValues(PK_VALUE, 'effectiveDate_error')) {
					return false;
				}

			}
		}
	}
	setFocusLast('taxBorne');
	return true;
}

function taxBorne_val() {
	clearError('taxBorne_error');
	if ($('#taxBorne').is(':checked')) {
		$('#accountingHead').attr('readOnly', false);
		setFocusLast('accountingHead');
		return true;
	} else
		$('#accountingHead').prop('readOnly', true);
	$('#accountingHead').val(EMPTY_STRING);
	$('#accountingHead_desc').html(EMPTY_STRING);
	$('#accountingHead_error').html(EMPTY_STRING);
	setFocusLast('remarks');
	return true;
}

function accountingHead_val() {
	var value = $('#accountingHead').val();
	clearError('accountingHead_error');
	$('#accountingHead_desc').html(EMPTY_STRING);
	if (isEmpty(value)) {
		setError('accountingHead_error', MANDATORY);
		return false;
	}
	if (!isValidCode(value)) {
		setError('accountingHead_error', INVALID_FORMAT);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('GL_HEAD_CODE', value);
	validator.setValue(ACTION, USAGE);
	validator.setClass('patterns.config.web.forms.cmn.itaxbyusproductbean');
	validator.setMethod('validateGlHeadCode');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('accountingHead_error', validator.getValue(ERROR));
		return false;
	} else if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		$('#accountingHead_desc').html(validator.getValue('DESCRIPTION'));

	}
	setFocusLast('remarks');
	return true;
}

function enabled_val() {
	if ($('#action').val() == MODIFY) {
		setFocusLast('remarks');
	}
	return true;
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', INVALID_REMARKS);
			setFocusLast('remarks');
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', INVALID_REMARKS);
			setFocusLast('remarks');
			return false;
		}
	}
	if ($('#action').val() == MODIFY) {
		if (isEmpty(value)) {
			setError('remarks_error', MANDATORY);
			setFocusLast('remarks');
			return false;
		}
	}
	setFocusOnSubmit();
	return true;
}
