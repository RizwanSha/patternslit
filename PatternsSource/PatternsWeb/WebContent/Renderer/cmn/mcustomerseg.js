var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MCUSTOMERSEG';
function init() {
	refreshTBAGrid();
	refreshQueryGrid();
	if (_redisplay) {
		if ($('#action').val() == ADD) {
			$('#enabled').prop('disabled', true);
			setCheckbox('enabled', YES);
		}
		segmentFor_val(false);
	}
	setFocusLast('customerSegmentCode');
}

function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'MCUSTOMERSEG_MAIN', queryGrid);
}

function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}

function doclearfields(id) {
	switch (id) {
	case 'customerSegmentCode':
		if (isEmpty($('#customerSegmentCode').val())) {
			clearFields();
			break;
		}
	}
}

function clearFields() {
	$('#customerSegmentCode').val(EMPTY_STRING);
	$('#customerSegmentCode_error').html(EMPTY_STRING);
	$('#customerSegmentCode_desc').html(EMPTY_STRING);
	clearNonPKFields();
}

function clearNonPKFields() {
	$('#segmentFor').val(EMPTY_STRING);
	$('#segmentFor').prop('selectedIndex', 0);
	$('#segmentFor_error').html(EMPTY_STRING);
	$('#description').val(EMPTY_STRING);
	$('#description_error').html(EMPTY_STRING);
	$('#conciseDescription').val(EMPTY_STRING);
	$('#conciseDescription_error').html(EMPTY_STRING);
	setCheckbox('prioritySectorSegment', NO);
	setCheckbox('seniorCitizenSegment', NO);
	setCheckbox('womenSegment', NO);
	setCheckbox('staffSegment', NO);
	setCheckbox('enabled', YES);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
}

function doHelp(id) {
	switch (id) {
	case 'customerSegmentCode':
		help('COMMON', 'HLP_CUSTSEG_CODE', $('#customerSegmentCode').val(),EMPTY_STRING, $('#customerSegmentCode'));
		break;
	}
}

function add() {
	setCheckbox('enabled', YES);
	$('#enabled').prop('disabled', true);
	$('#customerSegmentCode').focus();
}

function modify() {
	if ($('#action').val() == MODIFY) {
		$('#enabled').prop('disabled', false);
	} else {
		$('#enabled').prop('disabled', true);
	}
	$('#customerSegmentCode').focus();
}

function loadData() {
	$('#customerSegmentCode').val(validator.getValue('CUST_SEGMENT_CODE'));
	$('#description').val(validator.getValue('DESCRIPTION'));
	$('#conciseDescription').val(validator.getValue('CONCISE_DESCRIPTION'));
	$('#segmentFor').val(validator.getValue('SEGMENT_FOR'));
	setCheckbox('prioritySectorSegment', validator.getValue('PRIORTY_SECTOR_SEG'));
	setCheckbox('seniorCitizenSegment', validator.getValue('SENIOR_CIT_SEG'));
	setCheckbox('womenSegment', validator.getValue('WOMEN_SEG'));
	setCheckbox('staffSegment', validator.getValue('STAFF_SEG'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	resetLoading();
}

function change(id) {
	switch (id) {
	case 'segmentFor':
		segmentFor_val(false);
		break;
	}
}

function view(source, primaryKey) {
	hideParent('cmn/qcustomerseg', source, primaryKey);
	resetLoading();
}

function validate(id) {
	switch (id) {
	case 'customerSegmentCode':
		customerSegmentCode_val();
		break;
	case 'description':
		description_val();
		break;
	case 'conciseDescription':
		conciseDescription_val();
		break;
	case 'segmentFor':
		segmentFor_val(true);
		break;
	case 'prioritySectorSegment':
		prioritySectorSegment_val();
		break;
	case 'seniorCitizenSegment':
		seniorCitizenSegment_val();
		break;
	case 'womenSegment':
		womenSegment_val();
		break;
	case 'enabled':
		enabled_val();
		break;
	case 'staffSegment':
		staffSegment_val();
		break;
	case 'remarks':
		remarks_val();
		break;
	}
}

function backtrack(id) {
	switch (id) {
	case 'customerSegmentCode':
		setFocusLast('customerSegmentCode');
		break;
	case 'description':
		setFocusLast('customerSegmentCode');
		break;
	case 'conciseDescription':
		setFocusLast('description');
		break;
	case 'segmentFor':
		setFocusLast('conciseDescription');
		break;
	case 'prioritySectorSegment':
		setFocusLast('segmentFor');
		break;
	case 'seniorCitizenSegment':
		if ($('#segmentFor').val() == '1') {
			setFocusLast('segmentFor');
		}
		break;
	case 'womenSegment':
		setFocusLast('seniorCitizenSegment');
		break;
	case 'staffSegment':
		setFocusLast('womenSegment');
		break;
	case 'enabled':
		setFocusLast('prioritySectorSegment');
		break;
	case 'remarks':
		if ($('#action').val() == MODIFY)
			setFocusLast('enabled');
		else if ($('#segmentFor').val() == '1') {
			setFocusLast('staffSegment');
		} else if ($('#segmentFor').val() == '2') {
			setFocusLast('prioritySectorSegment');
		} else
			setFocusLast('segmentFor');
		break;
	case 'cmdSubmit':
		setFocusLast('remarks');
		break;
	}
}

function customerSegmentCode_val() {
	var value = $('#customerSegmentCode').val();
	clearError('customerSegmentCode_error');
	if (isEmpty(value)) {
		setError('customerSegmentCode_error', MANDATORY);
		return false;
	}
	if (!isNumeric(value)) {
		setError('customerSegmentCode_error', INVALID_NUMBER);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('CUST_SEGMENT_CODE', value);
	validator.setValue(ACTION, $('#action').val());
	validator.setClass('patterns.config.web.forms.cmn.mcustomersegbean');
	validator.setMethod('validateCustSegmentCode');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('customerSegmentCode_error', validator.getValue(ERROR));
		$('#customerSegmentCode_desc').html(EMPTY_STRING);
		return false;
	}
	if ($('#action').val() == MODIFY) {
		PK_VALUE = getEntityCode() + PK_SEPERATOR + $('#customerSegmentCode').val();
		if (!loadPKValues(PK_VALUE, 'customerSegmentCode_error')) {
			return false;
		}
	}
	setFocusLast('description');
	return true;
}

function description_val() {
	var value = $('#description').val();
	clearError('description_error');
	if (isEmpty(value)) {
		setError('description_error', MANDATORY);
		return false;
	}
	if (!isValidDescription(value)) {
		setError('description_error', INVALID_DESCRIPTION);
		return false;
	}
	setFocusLast('conciseDescription');
	return true;
}

function conciseDescription_val() {
	var value = $('#conciseDescription').val();
	clearError('conciseDescription_error');
	if (isEmpty(value)) {
		setError('conciseDescription_error', MANDATORY);
		return false;
	}
	if (!isValidConciseDescription(value)) {
		setError('conciseDescription_error', INVALID_CONCISE_DESCRIPTION);
		return false;
	}
	setFocusLast('segmentFor');
	return true;
}

function segmentFor_val(valMode) {
	var value = $('#segmentFor').val();
	clearError('segmentFor_error');
	if (isEmpty(value)) {
		setError('segmentFor_error', MANDATORY);
		return false;
	}
	if ($('#segmentFor').val() == '1') {
		$('#prioritySectorSegment').prop('disabled', true);

		$('#seniorCitizenSegment').prop('disabled', false);
		$('#womenSegment').prop('disabled', false);
		$('#staffSegment').prop('disabled', false);
		if (valMode)
			setFocusLast('seniorCitizenSegment');
	} else if ($('#segmentFor').val() == '2') {
		setCheckbox('seniorCitizenSegment', NO);
		setCheckbox('womenSegment', NO);
		setCheckbox('staffSegment', NO);
		$('#prioritySectorSegment').prop('disabled', false);
		$('#seniorCitizenSegment').prop('disabled', true);
		$('#womenSegment').prop('disabled', true);
		$('#staffSegment').prop('disabled', true);
		if (valMode)
			setFocusLast('prioritySectorSegment');
	}
	return true;
}

function prioritySectorSegment_val() {
	if ($('#segmentFor').val() == '1') {
		setFocusLast('seniorCitizenSegment');
	} else if ($('#segmentFor').val() == '2') {
		if ($('#action').val() == MODIFY) {
			setFocusLast('enabled');
		} else {
			setFocusLast('remarks');
		}
	}
}

function seniorCitizenSegment_val() {
	setFocusLast('womenSegment');
}

function womenSegment_val() {
	setFocusLast('staffSegment');
}

function staffSegment_val() {
	if ($('#seniorCitizenSegment').is(':checked')) {
		if (!confirm(SENIOR_STAFF)) {
			return false;
		}
	}
	if ($('#action').val() == MODIFY) {
		setFocusLast('enabled');
	} else {
		setFocusLast('remarks');

	}
}

function enabled_val() {
	setFocusLast('remarks');
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', HMS_INVALID_REMARKS);
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', HMS_INVALID_REMARKS);
			return false;
		}
	}
	if ($('#action').val() == MODIFY) {
		if (isEmpty(value)) {
			setError('remarks_error', HMS_MANDATORY);
			return false;
		}
	}
	setFocusOnSubmit();
	return true;
}
