var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MSUBGLHEAD';
function init() {
	refreshTBAGrid();
	refreshQueryGrid();
	if (_redisplay) {
		if ($('#action').val() == ADD) {
			$('#enabled').prop('disabled', true);
			setCheckbox('enabled', YES);
		}
	}
}

function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'MSUBGLHEAD_MAIN', queryGrid);
}

function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}

function doHelp(id) {
	switch (id) {
	case 'glHeadCode':
		help('COMMON', 'HLP_GL_HEAD_CODE', $('#glHeadCode').val(), EMPTY_STRING, $('#glHeadCode'));
		break;
	case 'subGlHeadCode':
		help('COMMON', 'HLP_SUBGL_HEAD_CODE', $('#subGlHeadCode').val(), $('#glHeadCode').val(), $('#subGlHeadCode'));
		break;
	case 'custSegCode':
		help('COMMON', 'HLP_CUST_SEG_CODE', $('#custSegCode').val(), EMPTY_STRING, $('#custSegCode'));
		break;
	case 'portCode':
		help('COMMON', 'HLP_PORTFOLIO_CODE', $('#portCode').val(), EMPTY_STRING, $('#portCode'));
		break;
	}
}

function clearFields() {
	$('#glHeadCode').val(EMPTY_STRING);
	$('#glHeadCode_desc').html(EMPTY_STRING);
	$('#glHeadCode_error').html(EMPTY_STRING);
	$('#subGlHeadCode').val(EMPTY_STRING);
	$('#subGlHeadCode_desc').html(EMPTY_STRING);
	$('#subGlHeadCode_error').html(EMPTY_STRING);
	clearNonPKFields();
}

function clearNonPKFields() {
	$('#description').val(EMPTY_STRING);
	$('#description_error').html(EMPTY_STRING);
	$('#conciseDescription').val(EMPTY_STRING);
	$('#conciseDescription_error').html(EMPTY_STRING);
	$('#custSegCode').val(EMPTY_STRING);
	$('#custSegCode_desc').html(EMPTY_STRING);
	$('#custSegCode_error').html(EMPTY_STRING);
	$('#portCode').val(EMPTY_STRING);
	$('#portCode_desc').html(EMPTY_STRING);
	$('#portCode_error').html(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
	setCheckbox('enabled', YES);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
}

function doclearfields(id) {
	switch (id) {
	case 'glHeadCode':
		if (isEmpty($('#glHeadCode').val())) {
			$('#glHeadCode_desc').html(EMPTY_STRING);
			$('#glHeadCode_error').html(EMPTY_STRING);
			$('#subGlHeadCode_desc').html(EMPTY_STRING);
			$('#subGlHeadCode_error').html(EMPTY_STRING);
			clearNonPKFields();
			break;
		}
	case 'subGlHeadCode':
		if (isEmpty($('#subGlHeadCode').val())) {
			$('#subGlHeadCode_desc').html(EMPTY_STRING);
			$('#subGlHeadCode_error').html(EMPTY_STRING);
			clearNonPKFields();
			break;
		}
	}
}

function add() {
	$('#glHeadCode').focus();
	setCheckbox('enabled', YES);
	$('#enabled').prop('disabled', true);
}

function modify() {
	$('#glHeadCode').focus();
	if ($('#action').val() == MODIFY) {
		$('#enabled').prop('disabled', false);
	} else {
		$('#enabled').prop('disabled', true);
	}
}

function loadData() {
	$('#glHeadCode').val(validator.getValue('GL_HEAD_CODE'));
	$('#glHeadCode_desc').html(validator.getValue('F1_DESCRIPTION'));
	$('#subGlHeadCode').val(validator.getValue('SUB_GL_HEAD_CODE'));
	$('#description').val(validator.getValue('DESCRIPTION'));
	$('#conciseDescription').val(validator.getValue('CONCISE_DESCRIPTION'));
	$('#custSegCode').val(validator.getValue('CUST_SEGMENT_CODE'));
	$('#custSegCode_desc').html(validator.getValue('F2_DESCRIPTION'));
	$('#portCode').val(validator.getValue('PORTFOLIO_CODE'));
	$('#portCode_desc').html(validator.getValue('F3_DESCRIPTION'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	resetLoading();
}

function view(source, primaryKey) {
	hideParent('cmn/qsubglhead', source, primaryKey);
	resetLoading();
}

function validate(id) {
	switch (id) {
	case 'glHeadCode':
		glHeadCode_val();
		break;
	case 'subGlHeadCode':
		subGlHeadCode_val();
		break;
	case 'description':
		description_val();
		break;
	case 'conciseDescription':
		conciseDescription_val();
		break;
	case 'custSegCode':
		custSegCode_val();
		break;
	case 'portCode':
		portCode_val();
		break;
	case 'enabled':
		enabled_val();
		break;
	case 'remarks':
		remarks_val();
		break;
	}
}

function backtrack(id) {
	switch (id) {
	case 'glHeadCode':
		setFocusLast('glHeadCode');
		break;
	case 'subGlHeadCode':
		setFocusLast('glHeadCode');
		break;
	case 'description':
		setFocusLast('subGlHeadCode');
		break;
	case 'conciseDescription':
		setFocusLast('description');
		break;
	case 'custSegCode':
		setFocusLast('conciseDescription');
		break;
	case 'portCode':
		setFocusLast('custSegCode');
		break;
	case 'enabled':
		setFocusLast('portCode');
		break;
	case 'remarks':
		if ($('#action').val() == MODIFY) {
			setFocusLast('enabled');
		} else {
			setFocusLast('portCode');
		}
		break;
	}
}

function glHeadCode_val() {
	var value = $('#glHeadCode').val();
	clearError('glHeadCode_error');
	$('#glHeadCode_desc').html(EMPTY_STRING);
	if (isEmpty(value)) {
		setError('glHeadCode_error', MANDATORY);
		return false;
	}
	if (!isValidCode(value)) {
		setError('glHeadCode_error', INVALID_FORMAT);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('GL_HEAD_CODE', value);
	validator.setValue(ACTION, USAGE);
	validator.setClass('patterns.config.web.forms.cmn.msubglheadbean');
	validator.setMethod('validateGlHeadCode');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('glHeadCode_error', validator.getValue(ERROR));
		return false;
	} else if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		$('#glHeadCode_desc').html(validator.getValue('DESCRIPTION'));

	}
	setFocusLast('subGlHeadCode');
	return true;
}

function subGlHeadCode_val() {
	var value = $('#subGlHeadCode').val();
	clearError('subGlHeadCode_error');
	$('#subGlHeadCode_desc').html(EMPTY_STRING);
	if (isEmpty(value)) {
		setError('subGlHeadCode_error', MANDATORY);
		return false;
	}
	if (!isValidCode(value)) {
		setError('subGlHeadCode_error', INVALID_FORMAT);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('GL_HEAD_CODE', $('#glHeadCode').val());
	validator.setValue('SUB_GL_HEAD_CODE', value);
	validator.setValue(ACTION, $('#action').val());
	validator.setClass('patterns.config.web.forms.cmn.msubglheadbean');
	validator.setMethod('validatesubGlCode');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('subGlHeadCode_error', validator.getValue(ERROR));
		return false;
	} else if ($('#action').val() == MODIFY) {
		PK_VALUE = getEntityCode() + PK_SEPERATOR + $('#glHeadCode').val() + PK_SEPERATOR + $('#subGlHeadCode').val();
		if (!loadPKValues(PK_VALUE, 'subGlHeadCode_error')) {
			return false;
		}
	}

	setFocusLast('description');
	return true;
}

function description_val() {
	var value = $('#description').val();
	clearError('description_error');
	if (isEmpty(value)) {
		setError('description_error', MANDATORY);
		return false;
	}
	if (!isValidDescription(value)) {
		setError('description_error', INVALID_DESCRIPTION);
		return false;
	}
	setFocusLast('conciseDescription');
	return true;
}

function conciseDescription_val() {
	var value = $('#conciseDescription').val();
	clearError('conciseDescription_error');
	if (isEmpty(value)) {
		setError('conciseDescription_error', MANDATORY);
		return false;
	}
	if (!isValidConciseDescription(value)) {
		setError('conciseDescription_error', INVALID_CONCISE_DESCRIPTION);
		return false;
	}
	setFocusLast('custSegCode');
	return true;
}

function custSegCode_val() {
	var value = $('#custSegCode').val();
	clearError('custSegCode_error');
	$('#custSegCode_desc').html(EMPTY_STRING);
	if (isEmpty(value)) {
		setError('custSegCode_error', MANDATORY);
		return false;
	}
	if (!isValidCode(value)) {
		setError('custSegCode_error', INVALID_FORMAT);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('CUST_SEGMENT_CODE', value);
	validator.setValue(ACTION, USAGE);
	validator.setClass('patterns.config.web.forms.cmn.msubglheadbean');
	validator.setMethod('validateCustomerSegmentCode');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('custSegCode_error', validator.getValue(ERROR));
		return false;
	} else {
		if (validator.getValue(RESULT) == DATA_AVAILABLE) {
			$('#custSegCode_desc').html(validator.getValue('DESCRIPTION'));
		}
	}

	setFocusLast('portCode');
	return true;
}

function portCode_val() {
	var value = $('#portCode').val();
	clearError('portCode_error');
	$('#portCode_desc').html(EMPTY_STRING);
	if (isEmpty(value)) {
		setError('portCode_error', MANDATORY);
		return false;
	}
	if (!isValidCode(value)) {
		setError('portCode_error', INVALID_FORMAT);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('PORTFOLIO_CODE', value);
	validator.setValue(ACTION, USAGE);
	validator.setClass('patterns.config.web.forms.cmn.msubglheadbean');
	validator.setMethod('validatePortfoliocode');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('portCode_error', validator.getValue(ERROR));
		return false;
	} else {
		if (validator.getValue(RESULT) == DATA_AVAILABLE) {
			$('#portCode_desc').html(validator.getValue('DESCRIPTION'));
			if ($('#action').val() == MODIFY) {
				setFocusLast('enabled');
				return true;
			} else {
				setFocusLast('remarks');
				return true;
			}
		}
	}
}

function enabled_val() {
	if ($('#action').val() == MODIFY) {
		setFocusLast('remarks');
	}
	return true;
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', INVALID_REMARKS);
			setFocusLast('remarks');
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', INVALID_REMARKS);
			setFocusLast('remarks');
			return false;
		}
	}
	if ($('#action').val() == MODIFY) {
		if (isEmpty(value)) {
			setError('remarks_error', MANDATORY);
			setFocusLast('remarks');
			return false;
		}
	}
	setFocusOnSubmit();
	return true;
}
