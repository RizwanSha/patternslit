<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<web:bundle baseName="patterns.config.web.forms.cmn.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/cmn/qbklicentypes.js" />
	</web:dependencies>
	<web:loadPK />
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="qbklicentypes.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:viewContent id="po_view1">
				<web:dividerBlock>
					<web:sectionBlock width="width-100p" align="left">
						<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="230px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="mbklicentypes.banklicencetype" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:bankLicenseTypeDisplay property="bankLicenceType" id="bankLicenceType" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="form.description" var="common" mandatory="true" />
										</web:column>
										<web:column>
											<type:descriptionDisplay property="description" id="description" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="form.conciseDescription" var="common" mandatory="true" />
										</web:column>
										<web:column>
											<type:conciseDescriptionDisplay property="conciseDescription" id="conciseDescription" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="mbklicentypes.casaallowed" var="program" />
										</web:column>
										<web:column>
											<type:checkboxDisplay property="casaAllowed" id="casaAllowed" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="mbklicentypes.termdepositallowed" var="program" />
										</web:column>
										<web:column>
											<type:checkboxDisplay property="termDepositAllowed" id="termDepositAllowed" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="mbklicentypes.loansallowed" var="program" />
										</web:column>
										<web:column>
											<type:checkboxDisplay property="loansAllowed" id="loansAllowed" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="mbklicentypes.paymentservicesallowed" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:checkboxDisplay property="paymentServicesAllowed" id="paymentServicesAllowed" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="mbklicentypes.collectionserviceallowed" var="program" />
										</web:column>
										<web:column>
											<type:checkboxDisplay property="collectionServiceAllowed" id="collectionServiceAllowed" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="form.IsUseEnabled" var="common" />
										</web:column>
										<web:column>
											<type:checkboxDisplay property="enabled" id="enabled" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="form.remarks" var="common" />
										</web:column>
										<web:column>
											<type:remarksDisplay property="remarks" id="remarks" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
						<web:auditDisplay />
					</web:sectionBlock>
				</web:dividerBlock>
			</web:viewContent>
		</web:viewSubPart>
	</web:viewPart>
</web:fragment>
