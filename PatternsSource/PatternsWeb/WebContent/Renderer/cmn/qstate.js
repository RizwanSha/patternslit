var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MSTATE';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}

function clearFields() {
	$('#stateCode').val(EMPTY_STRING);
	$('#stateCode_desc').html(EMPTY_STRING);
	$('#name').val(EMPTY_STRING);
	$('#shortName').val(EMPTY_STRING);
	$('#stateOrUnionTerritory').val($("#stateOrUnionTerritory option:first-child").val());
	$('#oldStateCodeSplitOnDate').val(EMPTY_STRING);
	$('#newStateFormationDate').val(EMPTY_STRING);
	$('#invoiceType').prop('selectedIndex', 0);
	$('#remarks').val(EMPTY_STRING);
}
function loadData() {
	$('#stateCode').val(validator.getValue('STATE_CODE'));
	$('#name').val(validator.getValue('DESCRIPTION'));
	$('#shortName').val(validator.getValue('CONCISE_DESCRIPTION'));
	$('#stateOrUnionTerritory').val(validator.getValue('STATE_UT'));
	$('#oldStateCodeSplitOnDate').val(validator.getValue('OLD_STATE_SPLIT_ON'));
	$('#newStateFormationDate').val(validator.getValue('NEW_STATE_FORMATION_ON'));
	$('#invoiceType').val(validator.getValue('INVOICE_TYPE'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadAuditFields(validator);
}
