<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.cmn.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/cmn/massetfields.js" />
	</web:dependencies>
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="massetfields.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/cmn/massetfields" id="massetfields" method="POST">
				<web:dividerBlock>
					<type:optionBox add="true" modify="true" view="true" />
				</web:dividerBlock>
				<web:viewContent id="po_view1" styleClass="hidden">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="massetfields.fieldid" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:fieldId property="fieldId" id="fieldId" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="massetfields.name" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:description property="name" id="name" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="massetfields.shortname" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:conciseDescription property="shortName" id="shortName"  />
										</web:column>
									</web:rowEven>
									
									<web:rowOdd>
										<web:column>
										<web:legend key="massetfields.fieldfor"
											var="program" mandatory="false" />
									</web:column>
									<web:column>
										<type:combo property="fieldFor"
											id="fieldFor"
											datasourceid="MASSETFIELDS_FIELD_FOR" />
									</web:column>
									</web:rowOdd>
									
									<web:rowEven>
										<web:column>
										<web:legend key="massetfields.fieldvaluetype"
											var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:combo property="fieldValueType"
											id="fieldValueType"
											datasourceid="MASSETFIELDS_FIELD_VALUE_TYPE" />
									</web:column>
									</web:rowEven>
									<web:rowOdd>
									<web:column>
											<web:legend key="massetfields.fieldsize" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:threeDigit property="fieldSize" id="fieldSize" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="massetfields.nooffractiondigits" var="program" mandatory="false" />
										</web:column>
										<web:column>
											<type:twoDigit property="noOfFractionDigits" id="noOfFractionDigits" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="massetfields.fielddedicatedtocustomerid" var="program" mandatory="false" />
										</web:column>
										<web:column>
											<type:customerID property="fieldDedicatedToCustomerId" id="fieldDedicatedToCustomerId" />
										</web:column>
										</web:rowOdd>
										
										<web:rowEven>
										<web:column>
											<web:legend key="massetfields.fieldassettype" var="program" mandatory="false" />
										</web:column>
										<web:column>
											<type:assetTypeCode property="fieldAssetType" id="fieldAssetType" />
										</web:column>
										</web:rowEven>
										
									<web:rowOdd>
										<web:column>
											<web:legend key="form.enabled" var="common" />
										</web:column>
										<web:column>
											<type:checkbox property="enabled" id="enabled" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="form.remarks" var="common" />
										</web:column>
										<web:column>
											<type:remarks property="remarks" id="remarks" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column span="2">
											<web:submitReset />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
						</web:sectionBlock>
					</web:dividerBlock>
				</web:viewContent>
				<web:element property="command" />
				<web:element property="action" />
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
	<web:queryGrid />
	<web:TBAGrid />
	<web:contextSearch />
</web:fragment>
