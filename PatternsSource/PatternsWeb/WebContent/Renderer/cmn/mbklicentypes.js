var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MBKLICENTYPES';

function init() {
	refreshTBAGrid();
	refreshQueryGrid();
	if (_redisplay) {
		if ($('#action').val() == ADD) {
			$('#enabled').prop('disabled', true);
			setCheckbox('enabled', YES);
		}
	}
}

function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'MBKLICENTYPES_MAIN');
}

function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}

function add() {
	$('#bankLicenceType').focus();
	$('#enabled').prop('disabled', true);
	setCheckbox('enabled', YES);
}

function modify() {
	$('#bankLicenceType').focus();
	if ($('#action').val() == MODIFY) {
		$('#enabled').prop('disabled', false);
	} else {
		$('#enabled').prop('disabled', true);
	}
}

function view(source, primaryKey) {
	hideParent('cmn/qbklicentypes', source, primaryKey);
	resetLoading();
}

function clearFields() {
	$('#bankLicenceType').val(EMPTY_STRING);
	$('#bankLicenceType_desc').html(EMPTY_STRING);
	$('#bankLicenceType_error').html(EMPTY_STRING);
	clearNonPKFields();
}

function clearNonPKFields() {
	$('#description').val(EMPTY_STRING);
	$('#description_error').html(EMPTY_STRING);
	$('#conciseDescription').val(EMPTY_STRING);
	$('#conciseDescription_error').html(EMPTY_STRING);
	setCheckbox('casaAllowed', NO);
	setCheckbox('termDepositAllowed', NO);
	setCheckbox('loansAllowed', NO);
	setCheckbox('paymentServicesAllowed', NO);
	setCheckbox('collectionServiceAllowed', NO);
	if ($('#action').val() == MODIFY)
		setCheckbox('enabled', NO);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
}

function doclearfields(id) {
	switch (id) {
	case 'bankLicenceType':
		if (isEmpty($('#bankLicenceType').val())) {
			clearNonPKFields();
			break;
		}
	}
}

function loadData() {
	$('#bankLicenceType').val(validator.getValue('BANK_LIC_TYPES'));
	$('#description').val(validator.getValue('DESCRIPTION'));
	$('#conciseDescription').val(validator.getValue('CONCISE_DESCRIPTION'));
	setCheckbox('casaAllowed', validator.getValue('CASA_ALLOWED'));
	setCheckbox('termDepositAllowed', validator.getValue('TERM_DEPOSIT_ALLOWED'));
	setCheckbox('loansAllowed', validator.getValue('LOANS_ALLOWED'));
	setCheckbox('paymentServicesAllowed', validator.getValue('PAYMENT_SERVICES_ALLOWED'));
	setCheckbox('collectionServiceAllowed', validator.getValue('COLLECTION_SERVICES_ALLOWED'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	resetLoading();
}

function doHelp(id) {
	switch (id) {
	case 'bankLicenceType':
		help('COMMON', 'HLP_BANK_LIC_TYPE_M', EMPTY_STRING, $('#bankLicenceType').val(), $('#bankLicenceType'), null, function(gridObj, rowID) {
			$('#bankLicenceType').val(gridObj.cells(rowID, 0).getValue());
			$('#bankLicenceType_desc').html(EMPTY_STRING);
		});
		break;
	}
}

function validate(id) {
	switch (id) {
	case 'bankLicenceType':
		bankLicenceType_val();
		break;
	case 'description':
		description_val();
		break;
	case 'conciseDescription':
		conciseDescription_val();
		break;
	case 'casaAllowed':
		casaAllowed_val();
		break;
	case 'termDepositAllowed':
		termDepositAllowed_val();
		break;
	case 'loansAllowed':
		loansAllowed_val();
		break;
	case 'paymentServicesAllowed':
		paymentServicesAllowed_val();
		break;
	case 'collectionServiceAllowed':
		collectionServiceAllowed_val();
		break;
	case 'enabled':
		enabled_val();
		break;
	case 'remarks':
		remarks_val();
		break;
	}
}

function backtrack(id) {
	switch (id) {
	case 'bankLicenceType':
		setFocusLast('bankLicenceType');
		break;
	case 'description':
		setFocusLast('bankLicenceType');
		break;
	case 'conciseDescription':
		setFocusLast('description');
		break;
	case 'casaAllowed':
		setFocusLast('conciseDescription');
		break;
	case 'termDepositAllowed':
		setFocusLast('casaAllowed');
		break;
	case 'loansAllowed':
		setFocusLast('termDepositAllowed');
		break;
	case 'paymentServicesAllowed':
		setFocusLast('loansAllowed');
		break;
	case 'collectionServiceAllowed':
		setFocusLast('paymentServicesAllowed');
		break;
	case 'enabled':
		setFocusLast('collectionServiceAllowed');
		break;
	case 'remarks':
		if ($('#action').val() == MODIFY) {
			setFocusLast('enabled');
		} else {
			setFocusLast('collectionServiceAllowed');
		}
		break;
	}
}

function bankLicenceType_val() {
	var value = $('#bankLicenceType').val();
	clearError('bankLicenceType_error');
	if (isEmpty(value)) {
		setError('bankLicenceType_error', MANDATORY);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('BANK_LIC_TYPES', $('#bankLicenceType').val());
		validator.setValue(ACTION, $('#action').val());
		validator.setClass('patterns.config.web.forms.cmn.mbklicentypesbean');
		validator.setMethod('validateBankLicenceType');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('bankLicenceType_error', validator.getValue(ERROR));
			return false;
		}
		if ($('#action').val() == MODIFY) {
			PK_VALUE =$('#bankLicenceType').val();
			if (!loadPKValues(PK_VALUE, 'bankLicenceType_error')) {
				return false;
			}
		}
	}
	setFocusLast('description');
	return true;
}

function description_val() {
	var value = $('#description').val();
	clearError('description_error');
	if (isEmpty(value)) {
		setError('description_error', MANDATORY);
		return false;
	}
	if (!isValidDescription(value)) {
		setError('description_error', INVALID_DESCRIPTION);
		return false;
	}
	setFocusLast('conciseDescription');
	return true;
}

function conciseDescription_val() {
	var value = $('#conciseDescription').val();
	clearError('conciseDescription_error');
	if (isEmpty(value)) {
		setError('conciseDescription_error', MANDATORY);
		return false;
	}
	if (!isValidConciseDescription(value)) {
		setError('conciseDescription_error', INVALID_CONCISE_DESCRIPTION);
		return false;
	}
	setFocusLast('casaAllowed');
	return true;
}

function casaAllowed_val() {
	setFocusLast('termDepositAllowed');
}

function termDepositAllowed_val() {
	setFocusLast('loansAllowed');
}

function loansAllowed_val() {
	setFocusLast('paymentServicesAllowed');
}

function paymentServicesAllowed_val() {
	setFocusLast('collectionServiceAllowed');
}

function collectionServiceAllowed_val() {
	if ($('#action').val() == MODIFY) {
		setFocusLast('enabled');
	} else {
		setFocusLast('remarks');
	}
}

function enabled_val() {
	if ($('#action').val() == MODIFY) {
		setFocusLast('remarks');
	}
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', INVALID_REMARKS);
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', INVALID_REMARKS);
			return false;
		}
	}
	if ($('#action').val() == MODIFY) {
		if (isEmpty(value)) {
			setError('remarks_error', MANDATORY);
			return false;
		}
	}
	setFocusOnSubmit();
	return true;
}
