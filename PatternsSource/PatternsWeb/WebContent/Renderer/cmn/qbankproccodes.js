var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MBANKPROCCODES';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}

function clearFields() {
	$('#bankingProcessCode').val(EMPTY_STRING);
	$('#name').val(EMPTY_STRING);
	$('#shortName').val(EMPTY_STRING);
	$('#bankGLToBeUsed').val(EMPTY_STRING);
	setCheckbox('enabled', NO);
	$('#remarks').val(EMPTY_STRING);
}

function loadData() {
	$('#bankingProcessCode').val(validator.getValue('BANK_PROCESS_CODE'));
	$('#name').val(validator.getValue('NAME'));
	$('#shortName').val(validator.getValue('SHORT_NAME'));
	$('#bankGLToBeUsed').val(validator.getValue('GL_TOBE_USED'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadAuditFields(validator);
}
