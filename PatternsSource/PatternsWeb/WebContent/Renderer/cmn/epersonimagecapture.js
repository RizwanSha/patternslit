var validator = new xmlHTTPValidator();
var invNum = EMPTY_STRING;
var data = EMPTY_STRING;
var linkchangeVariable = false;
function initFileUpload() {
	evault.setAutoStart(false);
	evault.attachEvent("onFileRemove", function(file) {
		evault.setFilesLimit(1);
	});
}

function beforeFileAdd(file) {
	invNum = EMPTY_STRING;
	$('#image_preview').attr('src', "public/styles/images/default-user-image.png");
	var FileExtensionWithLowerCase = (file.name.substring(file.name.lastIndexOf(".") + 1)).toLowerCase();
	if (FileExtensionWithLowerCase == "png" || FileExtensionWithLowerCase == "jpeg" || FileExtensionWithLowerCase == "jpg" || FileExtensionWithLowerCase == "jpe") {
		evault.setFormField("PGM_ID", CURRENT_PROGRAM_ID);
		evault.setFormField("PURPOSE", 'CMN');
		evault.setAutoStart(true);
		return true;
	} else {
		alert(HMS_INVALID_FILE_EXTENSION);
	}

}

function afterFileUploadFail(file, extra) {
	$('#image_preview').attr('src', "public/styles/images/default-user-image.png");
	invNum = EMPTY_STRING;
	alert(extra.param);
}
function afterFileUpload(file, extra) {
	$('#fileName').val(file.name);
	invNum = extra.invNo;
	$('#image_preview').attr('src', getBasePath() + "servlet/ImageDownloadProcessor?TYPE=M&INVNUM=" + invNum + "&INVSRC=" + "T");
	setFocusLast('selected');

}
function removingDataFromfileUploadScreen() {
	$('.dhx_vault_file_delete').click();
	$('#image_preview').attr('src', "public/styles/images/default-user-image.png");
	$('#fileName').val(EMPTY_STRING);
	invNum = EMPTY_STRING;

}
function changeCaptureMode() {
	$('#capturefromcamera').val(HMS_CAPTURE_FROM_CAMERA);
	if ($(takeasnapshot).hasClass('hidden')) {
		$('#capturefromcamera').val(HMS_CHOOSE_FROM_GALLERY);
		removingDataFromfileUploadScreen();
		$('#takeasnapshot').removeClass('hidden');
		show('my_camera');
		hide('result_of_snap');
		$('#fileUpload').addClass('hidden');
		$("#linkId").html(HMS_TAKE_SNAPSHOT);
		linkchangeVariable = false;
		enableCamera();
	} else if ($(fileUpload).hasClass('hidden')) {
		$('#capturefromcamera').val(HMS_CAPTURE_FROM_CAMERA);
		removingDataFromfileUploadScreen();
		$('#fileUpload').removeClass('hidden');
		$('#takeasnapshot').addClass('hidden');
		Webcam.reset();
	}
}

function enableCamera() {
	Webcam.set({
		width : 200,
		height : 150,
		image_format : 'jpeg',
		jpeg_quality : 90,
		force_flash : false,
		// flip_horiz : true,
		fps : 45
	});

	Webcam.attach('#my_camera');
	Webcam.on();

}
function CancelFuction() {
	if (!$(fileUpload).hasClass('hidden')) {
		removingDataFromfileUploadScreen();
	} else {
		retake_snapshot();
	}
}
function take_snapshot() {
	if (linkchangeVariable == false) {
		Webcam.snap(function(data_uri) {
			invNum = EMPTY_STRING;
			$('#result_of_snap').attr('src', data_uri);
			data = data_uri;
		});
		Webcam.reset();
		hide('my_camera');
		show('result_of_snap');
		$("#linkId").html(HMS_RETAKE_SNAPSHOT);
		linkchangeVariable = true;
		setFocusLast('selected');
	} else {
		retake_snapshot();
	}
}

function retake_snapshot() {
	Webcam.reset();
	$('#result_of_snap').attr('src', "public/styles/images/default-user-image.png");
	data = EMPTY_STRING;
	show('my_camera');
	hide('result_of_snap');
	$("#linkId").html(HMS_TAKE_SNAPSHOT);
	linkchangeVariable = false;
	enableCamera();
}
function updateImageDetails() {
	$('#persondb_screen').attr('src', getBasePath() + "servlet/ImageDownloadProcessor?TYPE=M&INVNUM=" + invNum + "&INVSRC=" + "T");
	$('#tempInvNumber').val(invNum);
	$('#invSrc').val("T");
	show('removePhoto');
	return true;

}
function ResultBack() {
	Webcam.reset();
	if (!isEmpty(invNum)) {
		updateImageDetails();
		removingDataFromfileUploadScreen();
		closeInlinePopUp(win);
	} else if (!isEmpty(data)) {
		var raw_image_data = data.replace(/^data\:image\/\w+\;base64\,/, '');
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('IMAGE_SOURCE', raw_image_data);
		validator.setClass('patterns.config.validations.CommonValidator');
		validator.setMethod('uploadImage');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('personName_error', validator.getValue(ERROR));
			return false;
		} else if (validator.getValue(RESULT) == DATA_AVAILABLE) {
			data=EMPTY_STRING;
			invNum = (validator.getValue('INV_NUM'));
			updateImageDetails();
			Webcam.reset();
			closeInlinePopUp(win);
		}
	} else {
		var r = confirm(HMS_IMAGE_IS_STILL_NOT_UPDATED);
		if (r == true) {
			closeInlinePopUp(win);
			if ($('#tempInvNumber').val != EMPTY_STRING)
				show('removePhoto');
		} else if (!$(takeasnapshot).hasClass('hidden')) {
			enableCamera();
		}
	}

}
