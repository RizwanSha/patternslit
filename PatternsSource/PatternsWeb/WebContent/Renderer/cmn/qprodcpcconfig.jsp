<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.cmn.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/cmn/qprodcpcconfig.js" />
	</web:dependencies>
	<web:loadPK />
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="iprodcpcconfig.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:viewContent id="po_view1">
				<web:dividerBlock>
					<web:sectionBlock width="width-100p" align="left">
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="iprodcpcconfig.productcode" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:productCodeDisplay property="productCode" id="productCode" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="iprodcpcconfig.portfoliocode" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:portfolioCodeDisplay property="portfolioCode" id="portfolioCode" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="form.effectivedate" var="common" mandatory="true" />
									</web:column>
									<web:column>
										<type:dateDisplay property="effectiveDate" id="effectiveDate" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowOdd>
									<web:column>
										<web:legend key="iprodcpcconfig.costcentercode" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:cpCenterCodeDisplay property="costCenterCode" id="costCenterCode" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="iprodcpcconfig.profitcentercode" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:cpCenterCodeDisplay property="profitCenterCode" id="profitCenterCode" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="form.enabled" var="common" />
									</web:column>
									<web:column>
										<type:checkboxDisplay property="enabled" id="enabled" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="form.remarks" var="common" />
									</web:column>
									<web:column>
										<type:remarksDisplay property="remarks" id="remarks" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:auditDisplay />
					</web:sectionBlock>
				</web:dividerBlock>
			</web:viewContent>
		</web:viewSubPart>
	</web:viewPart>
</web:fragment>
