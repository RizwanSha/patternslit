<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.cmn.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/cmn/epidreg.js" />
	</web:dependencies>
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="epidreg.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/cmn/epidreg" id="epidreg" method="POST">
				<web:dividerBlock>
					<type:optionBox add="true" modify="true" view="true" />
				</web:dividerBlock>
				<web:viewContent id="po_view1" styleClass="hidden">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="epidreg.personid" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:cifNumber property="personId" id="personId" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="epidreg.pidregsl" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:PIDRegSl property="PIDRegSl" id="PIDRegSl" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="epidreg.deregister" var="program" />
										</web:column>
										<web:column>
											<type:checkbox property="deRegister" id="deRegister" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="epidreg.dateofregistration" var="program" />
										</web:column>
										<web:column>
											<type:date property="dateOfRegistration" id="dateOfRegistration" readOnly="true" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:sectionTitle var="program" key="epidreg.section" />
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="epidreg.pidcode" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:pidCode property="PIDCode" id="PIDCode" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="epidreg.pidnumber" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:otherInformation25 property="PIDNumber" id="PIDNumber" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="epidreg.pidissuedate" var="program" />
										</web:column>
										<web:column>
											<type:date property="PIDIssueDate" id="PIDIssueDate" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="epidreg.pidexpirydate" var="program" />
										</web:column>
										<web:column>
											<type:date property="PIDExpiryDate" id="PIDExpiryDate" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="form.remarks" var="common" mandatory="true" />
										</web:column>
										<web:column>
											<type:remarks property="PIDRemarks" id="PIDRemarks" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:viewContent id="po_view5">
								<web:section>
									<web:sectionTitle var="program" key="epidreg.section1" />
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="200px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowOdd>
											<web:column>
												<web:legend key="epidreg.dateofdereg" var="program" />
											</web:column>
											<web:column>
												<type:date property="dateOfDereg" id="dateOfDereg" readOnly="true" />
											</web:column>
										</web:rowOdd>
										<web:rowEven>
											<web:column>
												<web:legend key="epidreg.reasonofdereg" var="program" />
											</web:column>
											<web:column>
												<type:remarks property="reasonOfDereg" id="reasonOfDereg" />
											</web:column>
										</web:rowEven>
									</web:table>
								</web:section>
							</web:viewContent>
							<web:section>
								<web:table>
									<web:rowOdd>
										<web:column span="2">
											<web:submitReset />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
						</web:sectionBlock>
					</web:dividerBlock>
				</web:viewContent>
				<web:element property="command" />
				<web:element property="action" />
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
	<web:queryGrid />
	<web:TBAGrid />
	<web:contextSearch />
</web:fragment>
