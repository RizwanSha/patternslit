var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MGEOUNITTYPE';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}
function clearFields() {

	$('#geoUnitStructureCode').val(EMPTY_STRING);
	$('#geoUnitType').val(EMPTY_STRING);
	$('#description').val(EMPTY_STRING);
	$('#conciseDescription').val(EMPTY_STRING);
	$('#enabled').prop('checked', false);
	$('#remarks').val(EMPTY_STRING);

}
function loadData() {
	$('#geoUnitStructureCode').val(validator.getValue('GEO_UNIT_STRUC_CODE'));
	$('#geoUnitStructureCode_desc').html(validator.getValue('F1_DESCRIPTION'));
	$('#geoUnitType').val(validator.getValue('GEO_UNIT_TYPE'));
	$('#description').val(validator.getValue('DESCRIPTION'));
	$('#conciseDescription').val(validator.getValue('CONCISE_DESCRIPTION'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadAuditFields(validator);
}
