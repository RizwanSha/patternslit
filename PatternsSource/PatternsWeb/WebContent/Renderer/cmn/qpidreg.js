var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'EPIDREG';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}
function clearFields() {

	$('#personId').val(EMPTY_STRING);
	$('#PIDRegSl').val(EMPTY_STRING);
	setCheckbox('deRegister', NO);
	$('#dateOfRegistration').val(EMPTY_STRING);
	$('#PIDCode').val(EMPTY_STRING);
	$('#PIDNumber').val(EMPTY_STRING);
	$('#PIDIssueDate').val(EMPTY_STRING);
	$('#PIDExpiryDate').val(EMPTY_STRING);
	$('#dateOfDereg').val(EMPTY_STRING);
	$('#reasonOfDereg').val(EMPTY_STRING);
}
function loadData() {

	$('#personId').val(validator.getValue('CIF_NO'));
	$('#personId_desc').html(validator.getValue('F1_NAME'));
	$('#PIDRegSl').val(validator.getValue('DOC_SL'));
	setCheckbox('deRegister', validator.getValue('PID_DE_REG'));
	if (validator.getValue('PID_DE_REG') == '1') {
		show('po_view5');
	} else {
		hide('po_view5');
	}
	$('#dateOfRegistration').val(validator.getValue('DATE_OF_REG'));
	$('#PIDCode').val(validator.getValue('PID_CODE'));
	$('#PIDCode_desc').html(validator.getValue('F2_DESCRIPTION'));
	$('#PIDNumber').val(validator.getValue('DOCUMENT_NUMBER'));
	$('#PIDIssueDate').val(validator.getValue('DOCUMENT_ISSUE_DATE'));
	$('#PIDExpiryDate').val(validator.getValue('DOCUMENT_EXPIRY_DATE'));
	$('#PIDRemarks').val(validator.getValue('REMARKS'));
	$('#dateOfDereg').val(validator.getValue('DATE_OF_DE_REG'));
	$('#reasonOfDereg').val(validator.getValue('REASON_FOR_DE_REG'));
	loadAuditFields(validator);
}
