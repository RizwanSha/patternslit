var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MORGBUSINESSCORR';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}

function loads() {
	loadGridQuery(CURRENT_PROGRAM_ID, 'MORGBUSINESSCORR_GRID', orgBusinessCorrGrid);
}

function clearFields() {
	$('#organizationCode').val(EMPTY_STRING);
	$('#organizationCode_desc').html(EMPTY_STRING);
	$('#effectiveDate').val(EMPTY_STRING);
	setCheckbox('enableBusinesCorr', NO);
	$('#geoStateCode').val(EMPTY_STRING);
	$('#geoStateCode_desc').html(EMPTY_STRING);
	$('#geoDistCode').val(EMPTY_STRING);
	$('#geoDistCode_desc').html(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	orgBusinessCorrGrid.clearAll();
}

function loadData() {
	$('#organizationCode').val(validator.getValue('ORG_CODE'));
	$('#organizationCode_desc').html(validator.getValue('F1_DESCRIPTION'));
	$('#effectiveDate').val(validator.getValue('EFF_DATE'));
	setCheckbox('enableBusinesCorr', decodeSTB(validator.getValue('ENABLEDASCORRDENT')));
	setCheckbox('enabled', decodeSTB(validator.getValue('ENABLED')));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadAuditFields(validator);
	loadGrid();
}

function loadGrid() {
	SOURCE_VALUE = _tableType;
	PK_VALUE = _primaryKey;
	if (_tableType == MAIN) {
		loads();
	} else if (_tableType == TBA) {
		loads();
	}
}
