var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MEMPDB';
var prevDoj = null;
var titleUsage = null;
function init() {
	refreshQueryGrid();
	refreshTBAGrid();
	redisplayCheckBoxValues();
	if (_redisplay) {
		if ($('#eligibleEsi').is(':checked'))
			$('#esiNumber').prop('readonly', false);
		else
			$('#esiNumber').prop('readonly', true);
		if ($('#provideFundApplicable').is(':checked'))
			$('#pfNumber').prop('readonly', false);
		else
			$('#pfNumber').prop('readonly', true);
		if ($('#action').val() == ADD)
			$('#employeeCode_pic').prop('disabled', true);
	}
	// payRollPolicy_val();
}

function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'MEMPDB_MAIN', queryGrid);
}

function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}

function clearFields() {
	$('#employeeCode').val(EMPTY_STRING);
	$('#employeeCode_desc').html(EMPTY_STRING);
	$('#employeeCode_error').html(EMPTY_STRING);
	clearNonPKFields();
}

function clearNonPKFields() {
	$('#employee').val(EMPTY_STRING);
	$('#employee_title').val(EMPTY_STRING);
	$('#employee_error').html(EMPTY_STRING);
	$('#gender').val(EMPTY_STRING);
	$('#gender_error').html(EMPTY_STRING);
	$('#dateOfBirth').val(EMPTY_STRING);
	$('#dateOfBirth_error').html(EMPTY_STRING);
	$('#emailId').val(EMPTY_STRING);
	$('#emailId_error').html(EMPTY_STRING);
	$('#mobileNumber').val(EMPTY_STRING);
	$('#mobileNumber_error').html(EMPTY_STRING);

	$('#relationShip').val(EMPTY_STRING);
	$('#relationShip_error').html(EMPTY_STRING);
	$('#relationName').val(EMPTY_STRING);
	$('#relationName_error').html(EMPTY_STRING);
	$('#dateOfJoining').val(EMPTY_STRING);
	$('#dateOfJoining_error').html(EMPTY_STRING);
	$('#dateOfRetirement').val(EMPTY_STRING);
	$('#dateOfRetirement_error').html(EMPTY_STRING);

	setCheckbox('provideFundApplicable', NO);
	$('#provideFundApplicable_error').html(EMPTY_STRING);
	setCheckbox('personFundRequired', NO);
	$('#personFundRequired_error').html(EMPTY_STRING);
	setCheckbox('eligibleEsi', NO);
	$('#eligibleEsi_error').html(EMPTY_STRING);
	$('#pfNumber').val(EMPTY_STRING);
	$('#pfNumber_error').html(EMPTY_STRING);
	$('#esiNumber').val(EMPTY_STRING);
	$('#esiNumber_error').html(EMPTY_STRING);
	$('#qualification').val(EMPTY_STRING);
	$('#qualification_error').html(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
}

function doclearfields(id) {
	switch (id) {
	case 'employeeCode':
		if (isEmpty($('#employeeCode').val())) {
			clearFields();
		}
		break;
	}
}

function doHelp(id) {
	switch (id) {
	case 'employeeCode':
		if ($('#action').val() == MODIFY)
			help('COMMON', 'HLP_EMP_DB', $('#employeeCode').val(), EMPTY_STRING, $('#employeeCode'), function(gridObj, rowID) {
			});
		break;
	}
}

function add() {
	$('#employeeCode').focus();
	$('#employeeCode_pic').prop('disabled', true);
	$('#pfNumber').prop('readonly', true);
	$('#esiNumber').prop('readonly', true);
	setCheckbox('provideFundApplicable', NO);
	setCheckbox('personFundRequired', NO);
	setCheckbox('eligibleEsi', NO);
}

function modify() {
	$('#employeeCode').focus();
	$('#employeeCode_pic').prop('disabled', false);
}

function view(source, primaryKey) {
	hideParent('cmn/qempdb', source, primaryKey);
	resetLoading();
}

function loadData() {
	$('#employeeCode').val(validator.getValue('EMP_CODE'));
	$('#employee').val(validator.getValue('PERSON_NAME'));
	$('#employee_title').val(validator.getValue('TITLE_CODE'));
	$('#gender').val(validator.getValue('GENDER'));
	$('#dateOfBirth').val(validator.getValue('DATE_OF_BIRTH'));
	$('#relationShip').val(validator.getValue('FATHER_HUSBAND_FLAG'));
	$('#relationName').val(validator.getValue('FATHER_OR_HUSBAND_NAME'));
	$('#dateOfRetirement').val(validator.getValue('RETIREMENT_DATE'));
	$('#dateOfJoining').val(validator.getValue('DATE_OF_JOIN'));
	prevDoj = validator.getValue('DATE_OF_JOIN');
	setCheckbox('provideFundApplicable', validator.getValue('PF_APPLICABLE'));
	$('#pfNumber').val(validator.getValue('PF_NUMBER'));
	if ($('#provideFundApplicable').is(':checked')) {
		$('#pfNumber').prop('readonly', false);
	} else {
		$('#pfNumber').prop('readonly', true);
	}
	setCheckbox('personFundRequired', validator.getValue('PENSION_FUND_REQD'));
	setCheckbox('eligibleEsi', validator.getValue('ELIGIBLE_FOR_ESI'));
	$('#esiNumber').val(validator.getValue('EMP_ESI_NO'));
	if ($('#eligibleEsi').is(':checked')) {
		$('#esiNumber').prop('readonly', false);
	} else {
		$('#esiNumber').prop('readonly', true);
	}
	$('#qualification').val(validator.getValue('EMP_QUALIFICATION'));
	$('#emailId').val(validator.getValue('EMAIL_ID'));
	$('#mobileNumber').val(validator.getValue('MOBILE_NO'));
	$('#remarks').val(validator.getValue('REMARKS'));
	titleUsage = validator.getValue('F1_TITLE_USAGE');
	resetLoading();
}

function validate(id) {
	switch (id) {
	case 'employeeCode':
		employeeCode_val();
		break;
	case 'employee_title':
		validateEmployeeTitle_val();
		break;
	case 'employee':
		employeeName_val();
		break;
	case 'gender':
		gender_val();
		break;
	case 'dateOfBirth':
		dateOfBirth_val();
		break;
	case 'relationShip':
		relationShip_val();
		break;
	case 'relationName':
		relationName_val();
		break;
	case 'dateOfJoining':
		dateOfJoining_val();
		break;
	case 'dateOfRetirement':
		dateOfRetirement_val();
		break;
	case 'provideFundApplicable':
		provideFundApplicable_val();
		break;
	case 'pfNumber':
		pfNumber_val();
		break;
	case 'personFundRequired':
		setFocusLast('eligibleEsi');
		break;
	case 'eligibleEsi':
		eligibleEsi_val();
		break;
	case 'esiNumber':
		esiNumber_val();
		break;
	case 'qualification':
		qualification_val();
		break;
	case 'emailId':
		emailId_val();
		break;
	case 'mobileNumber':
		mobileNumber_val();
		break;
	case 'remarks':
		remarks_val();
		break;

	}
}

function backtrack(id) {
	switch (id) {
	case 'employeeCode':
		setFocusLast('employeeCode');
		break;
	case 'employee_title':
		setFocusLast('employeeCode');
		break;
	case 'employee':
		setFocusLast('employee_title');
		break;
	case 'gender':
		setFocusLast('employee');
		break;
	case 'dateOfBirth':
		setFocusLast('gender');
		break;

	case 'relationShip':
		setFocusLast('dateOfBirth');
		break;
	case 'relationName':
		setFocusLast('relationShip');
		break;
	case 'dateOfJoining':
		setFocusLast('relationName');
		break;
	case 'dateOfRetirement':
		setFocusLast('dateOfJoining');
		break;
	case 'provideFundApplicable':
		setFocusLast('dateOfRetirement');
		break;
	case 'pfNumber':
		setFocusLast('provideFundApplicable');
		break;
	case 'personFundRequired':
		if ($('#provideFundApplicable').is(':checked')) {
			setFocusLast('pfNumber');
		} else {
			setFocusLast('provideFundApplicable');
		}
		break;
	case 'eligibleEsi':
		setFocusLast('personFundRequired');
		break;
	case 'esiNumber':
		setFocusLast('eligibleEsi');
		break;
	case 'qualification':
		if ($('#eligibleEsi').is(':checked')) {
			setFocusLast('esiNumber');
		} else {
			setFocusLast('eligibleEsi');
		}
		break;
	case 'emailId':
		setFocusLast('qualification');
		break;
	case 'mobileNumber':
		setFocusLast('emailId');
		break;
	case 'remarks':
		setFocusLast('mobileNumber');
		break;
	}
}

function employeeCode_val() {
	var value = $('#employeeCode').val();
	clearError('employeeCode_error');
	if (isEmpty(value)) {
		setError('employeeCode_error', MANDATORY);
		return false;
	}
	if ($('#action').val() == ADD && !isValidCode(value)) {
		setError('employeeCode_error', INVALID_FORMAT);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('EMP_CODE', $('#employeeCode').val());
	validator.setValue(ACTION, $('#action').val());
	validator.setClass('patterns.config.web.forms.cmn.mempdbbean');
	validator.setMethod('validateEmployeeCode');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('employeeCode_error', validator.getValue(ERROR));
		return false;
	}
	if ($('#action').val() == MODIFY) {
		PK_VALUE = getEntityCode() + PK_SEPERATOR + $('#employeeCode').val();
		if (!loadPKValues(PK_VALUE, 'employeeCode_error')) {
			return false;
		}
	}
	setFocusLast('employee_title');
	return true;
}

function validateEmployeeTitle_val() {
	var value = $('#employee_title').val();
	titleUsage = EMPTY_STRING;
	clearError('employee_error');
	if (isEmpty(value)) {
		setError('employee_error', MANDATORY);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('TITLE_CODE', $('#employee_title').val());
	validator.setClass('patterns.config.web.forms.cmn.mempdbbean');
	validator.setMethod('validateEmployeeTitle');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('employee_error', validator.getValue(ERROR));
		return false;
	} else {
		titleUsage = validator.getValue('TITLE_USAGE');
	}
	setFocusLast('employee');
	return true;
}

function employeeName_val() {
	var value = $('#employee').val();
	clearError('employee_error');
	if (isEmpty(value)) {
		setError('employee_error', MANDATORY);
		return false;
	} else if (!isValidName(value)) {
		setError('employee_error', INVALID_NAME);
		return false;
	}
	setFocusLast('gender');
	return true;
}

function gender_val() {
	var value = $('#gender').val();
	clearError('gender_error');
	if (isEmpty(value)) {
		setError('gender_error', MANDATORY);
		return false;
	} else if (titleUsage == '2') {
		if (value == 'F') {
			setError('gender_error', PBS_GENDER_CANT_BE_USED);
			return false;
		}
	} else if (titleUsage == '3') {
		if (value == 'M') {
			setError('gender_error', PBS_GENDER_CANT_BE_USED);
			return false;
		}
	}
	setFocusLast('dateOfBirth');
	return true;

}
function dateOfBirth_val(valMode) {
	var value = $('#dateOfBirth').val();
	clearError('dateOfBirth_error');
	if (isEmpty(value)) {
		setError('dateOfBirth_error', MANDATORY);
		return false;
	} else if (!isDate(value)) {
		setError('dateOfBirth_error', PBS_INVALID_DOB);
		return false;
	} else if (isDateGreaterEqual(value, getCBD())) {
		setError('dateOfBirth_error', DATE_LCBD);
		return false;
	}
	setFocusLast('relationShip');
	return true;
}

function relationShip_val() {
	var value = $('#relationShip').val();
	clearError('relationShip_error');
	if (isEmpty(value)) {
		setError('relationShip_error', MANDATORY);
		return false;
	}
	setFocusLast('relationName');
	return true;
}

function relationName_val() {
	var value = $('#relationName').val();
	clearError('relationName_error');
	if (isEmpty(value)) {
		setError('relationName_error', MANDATORY);
		return false;
	} else if (!isValidName(value)) {
		setError('relationName_error', INVALID_NAME);
		return false;
	}
	setFocusLast('dateOfJoining');
	return true;
}

function dateOfJoining_val() {
	var value = $('#dateOfJoining').val();
	clearError('dateOfJoining_error');
	if (isEmpty(value)) {
		setError('dateOfJoining_error', MANDATORY);
		return false;
	}
	if ($('#action').val() == ADD || ($('#action').val() == MODIFY && prevDoj != value)) {
		if (!isValidEffectiveDate(value, 'dateOfJoining_error')) {
			return false;
		}
	}
	setFocusLast('dateOfRetirement');
	return true;
}
function dateOfRetirement_val() {
	var value = $('#dateOfRetirement').val();
	clearError('dateOfRetirement_error');
	if (!isEmpty(value)) {
		if (!isDate(value)) {
			setError('dateOfRetirement_error', INVALID_DATE);
			return false;
		} else if (!isDateGreater(value, $('#dateOfJoining').val())) {
			setError('dateOfRetirement_error', PBS_DATE_GCBD_DATE_OF_JOIN);
			return false;
		}
	}
	setFocusLast('provideFundApplicable');
	return true;
}

function provideFundApplicable_val() {
	if ($('#provideFundApplicable').is(':checked')) {
		$('#pfNumber').prop('readonly', false);
		setFocusLast('pfNumber');
	} else {
		$('#pfNumber').prop('readonly', true);
		$('#pfNumber').val(EMPTY_STRING);
		clearError('pfNumber_error');
		setFocusLast('personFundRequired');
	}
}

function pfNumber_val() {
	var value = $('#pfNumber').val();
	clearError('pfNumber_error');
	if ($('#provideFundApplicable').is(':checked') && isEmpty(value)) {
		setError('pfNumber_error', MANDATORY);
		return false;
	}
	if (!isEmpty(value)) {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('PF_NUMBER', $('#pfNumber').val());
		validator.setValue('EMP_CODE', $('#employeeCode').val());
		validator.setClass('patterns.config.web.forms.cmn.mempdbbean');
		validator.setMethod('isPFNumberUsable');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('pfNumber_error', validator.getValue(ERROR));
			return false;
		}
	}
	setFocusLast('personFundRequired');
	return true;
}

function eligibleEsi_val() {
	if ($('#eligibleEsi').is(':checked')) {
		$('#esiNumber').prop('readonly', false);
		setFocusLast('esiNumber');
	} else {
		$('#esiNumber').prop('readonly', true);
		$('#esiNumber').val(EMPTY_STRING);
		clearError('esiNumber_error');
		setFocusLast('qualification');
	}
}

function esiNumber_val() {
	var value = $('#esiNumber').val();
	clearError('esiNumber_error');
	if ($('#eligibleEsi').is(':checked') && isEmpty(value)) {
		setError('esiNumber_error', MANDATORY);
		return false;
	}
	if (!isEmpty(value)) {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('EMP_ESI_NO', $('#esiNumber').val());
		validator.setValue('EMP_CODE', $('#employeeCode').val());
		validator.setClass('patterns.config.web.forms.cmn.mempdbbean');
		validator.setMethod('isESINumberUsable');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('esiNumber_error', validator.getValue(ERROR));
			return false;
		}
	}
	setFocusLast('qualification');
	return true;
}

function qualification_val() {
	var value = $('#qualification').val();
	clearError('qualification_error');
	if (isEmpty(value)) {
		setError('qualification_error', MANDATORY);
		return false;
	}
	if (!isValidDescription(value)) {
		setError('qualification_error', INVALID_VALUE);
		return false;
	}
	setFocusLast('emailId');
	return true;
}
function emailId_val() {
	var value = $('#emailId').val();
	clearError('emailId_error');
	if (!isEmpty(value) && !isEmail(value)) {
		setError('emailId_error', INVALID_EMAIL_ID);
		return false;
	}
	setFocusLast('mobileNumber');
	return true;
}
function mobileNumber_val() {
	var value = $('#mobileNumber').val();
	clearError('mobileNumber_error');
	if (!isEmpty(value) && !isPhoneNumber(value)) {
		setError('mobileNumber_error', INVALID_MOBILE_NUMBER);
		return false;
	}
	setFocusLast('remarks');
	return true;
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > ZERO) {
		if (isWhitespace(value)) {
			setError('remarks_error', INVALID_REMARKS);
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', INVALID_REMARKS);
			return false;
		}
	}
	if ($('#action').val() == MODIFY) {
		if (isEmpty(value)) {
			setError('remarks_error', MANDATORY);
			return false;
		}
	}
	setFocusOnSubmit();
	return true;
}
function redisplayCheckBoxValues() {
	if ($('#provideFundApplicable').is(':checked'))
		setCheckbox('provideFundApplicable', YES);
	else
		setCheckbox('provideFundApplicable', NO);
	if ($('#personFundRequired').is(':checked'))
		setCheckbox('personFundRequired', YES);
	else
		setCheckbox('personFundRequired', NO);
	if ($('#eligibleEsi').is(':checked'))
		setCheckbox('eligibleEsi', YES);
	else
		setCheckbox('eligibleEsi', NO);
}