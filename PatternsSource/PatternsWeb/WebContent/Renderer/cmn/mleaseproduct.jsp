<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.cmn.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/cmn/mleaseproduct.js" />
	</web:dependencies>
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="mleaseproduct.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/cmn/mleaseproduct" id="mleaseproduct" method="POST">
				<web:dividerBlock>
					<type:optionBox add="true" modify="true" view="true" />
				</web:dividerBlock>
				<web:viewContent id="po_view1" styleClass="hidden">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="mleaseproduct.leaseproductcode" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:leaseProductCode property="leaseProductCode" id="leaseProductCode" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="form.description" var="common" mandatory="true" />
										</web:column>
										<web:column>
											<type:description property="leaseProductDescription" id="leaseProductDescription" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="form.conciseDescription" var="common" mandatory="true" />
										</web:column>
										<web:column>
											<type:conciseDescription property="leaseProductConciseDescription" id="leaseProductConciseDescription" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="mleaseproduct.leasetype" var="program" mandatory="false"/>
										</web:column>
										<web:column>
											<type:combo property="leaseType" id="leaseType" datasourceid="COMMON_LEASE_TYPE" />
										</web:column>	
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="mleaseproduct.basecurrcode" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:currency property="baseCurrCode" id="baseCurrCode" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:rowOdd>
										<web:column>
											<web:viewTitle var="program" key="mleaseproduct.glheadproduct" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="mleaseproduct.sundrydebtorshead" var="program" mandatory="true"/>
										</web:column>
										<web:column>
											<type:GLAccessCode property="sundryDebtorsHead" id="sundryDebtorsHead"/>
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="mleaseproduct.rentreceivableprincipal" var="program" />
										</web:column>
										<web:column>
											<type:GLAccessCode property="rentReceivablePrincipal" id="rentReceivablePrincipal"/>
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="mleaseproduct.rentreceivableinterest" var="program" />
										</web:column>
										<web:column>
											<type:GLAccessCode property="rentReceivableInterest" id="rentReceivableInterest"/>
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="mleaseproduct.unearnedincomehead" var="program" />
										</web:column>
										<web:column>
											<type:GLAccessCode property="unearnedIncomeHead" id="unearnedIncomeHead"/>
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="form.enabled" var="common" />
										</web:column>
										<web:column>
											<type:checkbox property="enabled" id="enabled" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="form.remarks" var="common" />
										</web:column>
										<web:column>
											<type:remarks property="remarks" id="remarks" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column span="2">
											<web:submitReset />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
						</web:sectionBlock>
					</web:dividerBlock>
				</web:viewContent>
				<web:element property="command" />
				<web:element property="action" />
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
	<web:queryGrid />
	<web:TBAGrid />
	<web:contextSearch />
</web:fragment>
