var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'ENOMOS';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}
function clearFields() {
	$('#glHead').val(EMPTY_STRING);
	$('#glHead_desc').html(EMPTY_STRING);
	$('#precedingEntry').val(EMPTY_STRING);
	$('#reversal').val(EMPTY_STRING);
	$('#systemRefNo').val(EMPTY_STRING);
	clearNonPKFields();
}

function clearNonPKFields() {
	$('#dateOfEntry').val(EMPTY_STRING);
	$('#entryType').val(EMPTY_STRING);
	$('#interBranchTranType').val(EMPTY_STRING);
	$('#interBranchTranType_desc').html(EMPTY_STRING);
	$('#debitOrCredit').val(EMPTY_STRING);
	$('#orgEntryAmtActl').val(EMPTY_STRING);
	$('#orgEntryAmtActl_curr').val(EMPTY_STRING);
	$('#orgEntryAmtActlFormat').val(EMPTY_STRING);
	$('#orgEntryAmtBase').val(EMPTY_STRING);
	$('#orgEntryAmtBase_curr').val(getBaseCurrency());
	$('#orgEntryAmtBaseFormat').val(EMPTY_STRING);
	$('#manualEntryRefNo').val(EMPTY_STRING);
	$('#systemEntryRefDate').val(EMPTY_STRING);
	$('#systemEntryRefBatchNo').val(EMPTY_STRING);
	$('#systemEntryRefSerial').val(EMPTY_STRING);
	clearOpenbalSection();
	$('#cifNumber').val(EMPTY_STRING);
	$('#cifNumber_desc').html(EMPTY_STRING);
	$('#staffId').val(EMPTY_STRING);
	$('#staffId_desc').html(EMPTY_STRING);
	$('#notes').val(EMPTY_STRING);
	hide('view_normal');
	hide('view_cif');
	hide('view_staff');
}

function clearOpenbalSection() {
	$('#openBalOfEntryActl').val(EMPTY_STRING);
	$('#openBalOfEntryActl_curr').val(EMPTY_STRING);
	$('#openBalOfEntryActlFormat').val(EMPTY_STRING);
	$('#openBalActDbOrCr').val(EMPTY_STRING);
	$('#openBalOfEntryBase').val(EMPTY_STRING);
	$('#openBalOfEntryBase_curr').val(getBaseCurrency());
	$('#openBalOfEntryBaseFormat').val(EMPTY_STRING);
	$('#openBalBaseDbOrCr').val(EMPTY_STRING);
	$('#openingBalbaseDate').val(EMPTY_STRING);
}

function loadData() {
	$('#glHead').val(validator.getValue('GL_HEAD_CODE'));
	$('#glHead_desc').html(validator.getValue('F1_DESCRIPTION'));
	$('#precedingEntry').val(validator.getValue('F2_PRECEED_ENTRY_TYPE'));
	var folioRelationship=validator.getValue('F2_FOLIO_RELATIONSHIP');
	$('#reversal').val(validator.getValue('F2_RECON_TYPE'));
	$('#systemRefNo').val(validator.getValue('SYSTEM_REF_NO'));
	$('#dateOfEntry').val(validator.getValue('ENTRY_DATE'));
	$('#entryType').val(validator.getValue('ENTRY_TYPE'));
	$('#interBranchTranType').val(validator.getValue('INTER_BRN_TRAN_TYPES'));
	$('#interBranchTranType_desc').html(validator.getValue('F3_DESCRIPTION'));
	$('#debitOrCredit').val(validator.getValue('TRAN_DB_CR_FLAG'));
	$('#orgEntryAmtActl').val(validator.getValue('ORIG_ENTRY_AMT_AC'));
	$('#orgEntryAmtActl_curr').val(validator.getValue('ORIG_ENTRY_CCY_AC'));
	$('#orgEntryAmtActlFormat').val(validator.getValue('ORIG_ENTRY_AMT_AC'));
	$('#orgEntryAmtBase').val(validator.getValue('ORIG_ENTRY_AMT_BC'));
	$('#orgEntryAmtBase_curr').val(validator.getValue('ORIG_ENTRY_CCY_BC'));
	$('#orgEntryAmtBaseFormat').val(validator.getValue('ORIG_ENTRY_AMT_BC'));
	$('#manualEntryRefNo').val(validator.getValue('MANUAL_ENTRY_REF_NO'));
	$('#systemEntryRefDate').val(validator.getValue('SYSTEM_TRAN_REF_NO_DATE'));
	$('#systemEntryRefBatchNo').val(validator.getValue('SYSTEM_TRAN_REF_NO_BATCH'));
	$('#systemEntryRefSerial').val(validator.getValue('SYSTEM_TRAN_REF_NO_SERIAL'));
	

	$('#openBalOfEntryActl').val(validator.getValue('OPEN_BAL_OF_ENTRY_AC'));
	$('#openBalOfEntryActl_curr').val(validator.getValue('ORIG_ENTRY_CCY_AC'));
	$('#openBalOfEntryActlFormat').val(validator.getValue('OP_BAL_ENTRY'));
	$('#openBalActDbOrCr').val(validator.getValue('OPEN_BAL_OF_ENTRY_DBCR_AC'));
	$('#openBalOfEntryBase').val(validator.getValue('OPEN_BAL_OF_ENTRY_BC'));
	$('#openBalOfEntryBase_curr').val(validator.getValue('ORIG_ENTRY_CCY_BC'));
	$('#openBalOfEntryBaseFormat').val(validator.getValue('OPEN_BAL_OF_ENTRY_BC'));
	$('#openBalBaseDbOrCr').val(validator.getValue('OPEN_BAL_OF_ENTRY_DBCR_BC'));
	$('#openingBalbaseDate').val(validator.getValue('OPEN_BALANCE_BASE_DATE'));

	$('#cifNumber').val(validator.getValue('CIF_NO'));
	$('#cifNumber_desc').html(validator.getValue('F4_NAME'));
	$('#staffId').val(validator.getValue('EMP_ID'));
	$('#staffId_desc').html(validator.getValue('F5_PERSON_NAME'));
	$('#notes').val(validator.getValue('REMARKS'));
	if ($('#entryType').val() == 'N') {
		show('view_normal');
		if (folioRelationship == '1' || folioRelationship == '3') {
			show('view_cif');
			hide('view_staff');
		} else if (folioRelationship == '2') {
			show('view_staff');
			hide('view_cif');
		}
	} else {
		hide('view_normal');
		hide('view_cif');
		hide('view_staff');
	}
	loadAuditFields(validator);
	$('#orgEntryAmtActlFormat').val(formatAmount($('#orgEntryAmtActl').val(), $('#orgEntryAmtActl_curr').val()));
	$('#orgEntryAmtBaseFormat').val(formatAmount($('#orgEntryAmtBase').val(), $('#orgEntryAmtBase_curr').val()));
	$('#openBalOfEntryActlFormat').val(formatAmount($('#openBalOfEntryActl').val(), $('#openBalOfEntryActl_curr').val()));
	$('#openBalOfEntryBaseFormat').val(formatAmount($('#openBalOfEntryBase').val(), $('#openBalOfEntryBase_curr').val()));

}