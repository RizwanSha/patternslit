var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'IBKACBALREGU';
var sendAllowed;
var receiveAlloewd;
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}

function clearFields() {
	$('#bankLicenseType').val(EMPTY_STRING);
	$('#bankLicenseType_desc').html(EMPTY_STRING);
	$('#typeOfProduct').val(EMPTY_STRING);
	$('#ccyCode').val(EMPTY_STRING);
	$('#ccyCode_desc').html(EMPTY_STRING);
	$('#effectiveDate').val(EMPTY_STRING);
	setCheckbox('dayBalRestriction', NO);
	$('#maxallwDayEnd').val(EMPTY_STRING);
	$('#maxallwDayEnd_curr').val(EMPTY_STRING);
	$('#maxallwDayEndFormat').val(EMPTY_STRING);
	setCheckbox('perBalRestriction', NO);
	$('#maxallwperTrans').val(EMPTY_STRING);
	$('#maxallwperTrans_curr').val(EMPTY_STRING);
	$('#maxallwperTransFormat').val(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
}

function loadData() {
	$('#bankLicenseType').val(validator.getValue('BANK_LIC_TYPES'));
	$('#bankLicenseType_desc').html(validator.getValue('F1_DESCRIPTION'));
	$('#typeOfProduct').val(validator.getValue('PROD_TYPE'));
	$('#ccyCode').val(validator.getValue('CCY_CODE'));
	$('#ccyCode_desc').html(validator.getValue('F2_DESCRIPTION'));
	$('#effectiveDate').val(validator.getValue('EFF_DATE'));
	setCheckbox('dayBalRestriction', validator.getValue('DAY_END_BAL_REST_REQ'));
	setCheckbox('perBalRestriction', validator.getValue('PER_TXN_REST_REQ'));
	var dayBalReq = validator.getValue('DAY_END_BAL_REST_REQ');
	var perBalReq = validator.getValue('PER_TXN_REST_REQ');
	var maxDayBal = validator.getValue('DAY_END_MAX_BALANCE');
	var maxPerBal = validator.getValue('PER_TXN_MAX_ALLOWED');
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadAuditFields(validator);
	if (dayBalReq == COLUMN_ENABLE) {
		$('#maxallwDayEnd_curr').val($('#ccyCode').val());
		$('#maxallwDayEndFormat').val(formatAmount(maxDayBal, $('#maxallwDayEnd_curr').val()));
	}
	if (perBalReq == COLUMN_ENABLE) {
		$('#maxallwperTrans_curr').val($('#ccyCode').val());
		$('#maxallwperTransFormat').val(formatAmount(maxPerBal, $('#maxallwperTrans_curr').val()));
	}
}
