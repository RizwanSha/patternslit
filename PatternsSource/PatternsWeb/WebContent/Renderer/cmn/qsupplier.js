var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MSUPPLIER';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}

function clearFields() {
	$('#supplierID').val(EMPTY_STRING);
	$('#name').val(EMPTY_STRING);
	$('#shortName').val(EMPTY_STRING);
	$('#name').val(EMPTY_STRING);
	$('#shortName').val(EMPTY_STRING);
	$('#pan').val(EMPTY_STRING);
	$('#ifsc').val(EMPTY_STRING);
	$('#ifsc_desc').html(EMPTY_STRING);
	$('#branchName').val(EMPTY_STRING);
	$('#bankName').val(EMPTY_STRING);
	$('#city').val(EMPTY_STRING);
	$('#stateCode').val(EMPTY_STRING);
	$('#stateCode_desc').html(EMPTY_STRING);
	setCheckbox('enabled', YES);
	$('#remarks').val(EMPTY_STRING);
}

function loadData() {
	$('#supplierID').val(validator.getValue('SUPPLIER_ID'));
	$('#name').val(validator.getValue('NAME'));
	$('#shortName').val(validator.getValue('SHORT_NAME'));
	$('#pan').val(validator.getValue('PAN_NO'));
	$('#ifsc').val(validator.getValue('IFSC_CODE'));
	$('#bankName').val(validator.getValue('F1_BANK_NAME'));
	$('#branchName').val(validator.getValue('F1_BRANCH_NAME'));
	$('#city').val(validator.getValue('F1_CITY'));
	$('#stateCode').val(validator.getValue('F1_STATE_CODE'));
	$('#stateCode_desc').html(validator.getValue('F2_DESCRIPTION'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	resetLoading();
	loadAuditFields(validator);
}