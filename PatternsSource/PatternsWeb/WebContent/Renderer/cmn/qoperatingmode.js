var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MOPERATINGMODE';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}

function clearFields() {
	$('#operatingMode').val(EMPTY_STRING);
	clearNonPKFields();
}

function clearNonPKFields() {
	$('#description').val(EMPTY_STRING);
	$('#conciseDescription').val(EMPTY_STRING);
	setCheckbox('singleAccount', NO);
	setCheckbox('jointAccount', NO);
	setCheckbox('illtAccountAgent', NO);
	setCheckbox('enabled', NO);
	$('#remarks').val(EMPTY_STRING);
}

function loadData() {
	$('#operatingMode').val(validator.getValue('OPERATING_MODE'));
	$('#description').val(validator.getValue('DESCRIPTION'));
	$('#conciseDescription').val(validator.getValue('CONCISE_DESCRIPTION'));
	setCheckbox('singleAccount', validator.getValue('FOR_SINGLE_ACC'));
	setCheckbox('jointAccount', validator.getValue('FOR_JOINT_ACC'));
	setCheckbox('illtAccountAgent', validator.getValue('FOR_ILLITERATE_ACC'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadAuditFields(validator);
}