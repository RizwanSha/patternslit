var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MGEOREGIONS';
var map = "map";
var overlays = new Array();
function init() {
	refreshTBAGrid();
	refreshQueryGrid();
	if (_redisplay) {
		if ($('#action').val() == MODIFY) {
			$('#enabled').prop('disabled', false);
		} else {
			$('#enabled').prop('disabled', true);
			setCheckbox('enabled', YES);
		}
		var overlayObject = JSON.parse($('#mapData').val());
		mapObject.drawMapWithOverlay(overlayObject);
	}

}

function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'MGEOREGIONS_MAIN', queryGrid);
}

function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}

function clearFields() {
	$('#geoRegionCode').val(EMPTY_STRING);
	$('#geoRegionCode_error').html(EMPTY_STRING);
	$('#geoRegionCode_desc').html(EMPTY_STRING);
	clearNonPKFields();
}
function clearNonPKFields() {
	$('#description').val(EMPTY_STRING);
	$('#description_error').html(EMPTY_STRING);
	$('#conciseDescription').val(EMPTY_STRING);
	$('#conciseDescription_error').html(EMPTY_STRING);
	$('#pinCode').val(EMPTY_STRING);
	$('#pinCode_desc').html(EMPTY_STRING);
	$('#pinCode_error').html(EMPTY_STRING);
	$('#officeCode').val(EMPTY_STRING);
	$('#officeCode_desc').html(EMPTY_STRING);
	$('#officeCode_error').html(EMPTY_STRING);
	if ($('#action').val() == ADD) {
		setCheckbox('enabled', YES);
		$('#enabled').prop('disabled', true);
	} else {
		setCheckbox('enabled', NO);
		$('#enabled').prop('disabled', false);
	}
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
	$('#mapData').val(EMPTY_STRING);
	$('#mapInvNo').val(EMPTY_STRING);

	if (mapObject.overlays.length >= 1) {
		for (var i = 0; i < mapObject.overlays.length; i++) {
			mapObject.overlays[i].setMap(null);
		}
		mapObject.overlays = [];
	}
	mapObject.setDefaultLocation();
}

function doclearfields(id) {
	switch (id) {
	case 'geoRegionCode':
		if (isEmpty($('#geoRegionCode').val())) {
			$('geoRegionCode_desc').html(EMPTY_STRING);
			$('#geoRegionCode_error').html(EMPTY_STRING);
			clearNonPKFields();
			break;
		}
	}
}

function doHelp(id) {
	switch (id) {
	case 'geoRegionCode':
		help('COMMON', 'HLP_GEO_REG_CODE_M', $('#geoRegionCode').val(), EMPTY_STRING, $('#geoRegionCode'), function(gridObj, rowID) {
		});
		break;
	case 'pinCode':
		help('COMMON', 'HLP_GEOUNIT_PINCODE', $('#pinCode').val(), EMPTY_STRING, $('#pinCode'));
		break;
	case 'officeCode':
		help('COMMON', 'HLP_OFFICE_CODE', $('#officeCode').val(), EMPTY_STRING, $('#officeCode'));
		break;
	}
}

function add() {
	$('#geoRegionCode').focus();
	setCheckbox('enabled', YES);
	$('#enabled').prop('disabled', true);

}
function modify() {
	$('#geoRegionCode').focus();
	if ($('#action').val() == MODIFY) {
		$('#enabled').prop('disabled', false);
	} else {
		$('#enabled').prop('disabled', true);
	}
}
function loadData() {
	$('#geoRegionCode').val(validator.getValue('GEO_REGION_CODE'));
	$('#description').val(validator.getValue('DESCRIPTION'));
	$('#conciseDescription').val(validator.getValue('CONCISE_DESCRIPTION'));
	$('#pinCode').val(validator.getValue('PINCODE_GEOUNITID'));
	$('#pinCode_desc').html(validator.getValue('F1_DESCRIPTION'));
	$('#officeCode').val(validator.getValue('OFFICE_CODE'));
	$('#officeCode_desc').html(validator.getValue('F2_DESCRIPTION'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	$('#mapInvNo').val(validator.getValue('MAP_INV_NO'));
	$('#mapData').val(validator.getValue('F3_FILE_DATA'));
	var overlayObject = JSON.parse($('#mapData').val());
	mapObject.drawMapWithOverlay(overlayObject);
	resetLoading();
}
function view(source, primaryKey) {
	hideParent('cmn/qgeoregions', source, primaryKey);
	resetLoading();
}
function validate(id) {
	switch (id) {
	case 'geoRegionCode':
		geoRegionCode_val();
		break;
	case 'description':
		description_val();
		break;
	case 'conciseDescription':
		conciseDescription_val();
		break;
	case 'pinCode':
		pinCode_val();
		break;
	case 'officeCode':
		officeCode_val();
		break;
	case 'enabled':
		enabled_val();
		break;
	case 'remarks':
		remarks_val();
		break;
	}
}

function backtrack(id) {
	switch (id) {
	case 'geoRegionCode':
		setFocusLast('geoRegionCode');
		break;
	case 'description':
		setFocusLast('geoRegionCode');
		break;
	case 'conciseDescription':
		setFocusLast('description');
		break;
	case 'pinCode':
		setFocusLast('conciseDescription');
		break;
	case 'officeCode':
		setFocusLast('pinCode');
		break;
	case 'enabled':
		setFocusLast('officeCode');
		break;
	case 'remarks':
		if ($('#action').val() == MODIFY) {
			setFocusLast('enabled');
		} else {
			setFocusLast('officeCode');
		}
		break;
	}
}

function geoRegionCode_val() {
	var value = $('#geoRegionCode').val();
	clearError('geoRegionCode_error');
	if (isEmpty(value)) {
		setError('geoRegionCode_error', MANDATORY);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('GEO_REGION_CODE', $('#geoRegionCode').val());
	validator.setValue(ACTION, $('#action').val());
	validator.setClass('patterns.config.web.forms.cmn.mgeoregionsbean');
	validator.setMethod('validateGeoRegionCode');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('geoRegionCode_error', validator.getValue(ERROR));
		return false;
	} else {
		if ($('#action').val() == MODIFY) {
			PK_VALUE = getEntityCode() + PK_SEPERATOR + $('#geoRegionCode').val();
			if (!loadPKValues(PK_VALUE, 'geoRegionCode_error')) {
				return false;
			}
		}
	}
	setFocusLast('description');
	return true;
}

function description_val() {
	var value = $('#description').val();
	clearError('description_error');
	if (isEmpty(value)) {
		setError('description_error', HMS_MANDATORY);
		return false;
	}
	if (!isValidDescription(value)) {
		setError('description_error', HMS_INVALID_DESCRIPTION);
		return false;
	}
	setFocusLast('conciseDescription');
	return true;
}

function conciseDescription_val() {
	var value = $('#conciseDescription').val();
	clearError('conciseDescription_error');
	if (isEmpty(value)) {
		setError('conciseDescription_error', HMS_MANDATORY);
		return false;
	}
	if (!isValidConciseDescription(value)) {
		setError('conciseDescription_error', HMS_INVALID_CONCISE_DESCRIPTION);
		return false;
	}
	setFocusLast('pinCode');
	return true;
}

function pinCode_val() {
	var value = $('#pinCode').val();
	$('#pinCode_desc').html(EMPTY_STRING);
	clearError('pinCode_error');
	if (isEmpty(value)) {
		setError('pinCode_error', MANDATORY);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue(ACTION, USAGE);
	validator.setValue('GEO_UNIT_ID', $('#pinCode').val());
	validator.setClass('patterns.config.web.forms.cmn.mgeoregionsbean');
	validator.setMethod('validatePinCode');
	validator.sendAndReceive();
	if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		$('#pinCode_desc').html(validator.getValue('DESCRIPTION'));
	} else if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('pinCode_error', validator.getValue(ERROR));
		$('#pinCode_desc').html(EMPTY_STRING);
		return false;
	}
	setFocusLast('officeCode');
	return true;
}

function officeCode_val() {
	var value = $('#officeCode').val();
	clearError('officeCode_error');
	$('#officeCode_desc').html(EMPTY_STRING);
	if (isEmpty(value)) {
		setError('officeCode_error', MANDATORY);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('BRANCH_CODE', $('#officeCode').val());
		validator.setValue('ACTION', USAGE);
		validator.setClass('patterns.config.web.forms.cmn.mgeoregionsbean');
		validator.setMethod('validateOfficeCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('officeCode_error', validator.getValue(ERROR));
			return false;
		} else {
			if (validator.getValue(RESULT) == DATA_AVAILABLE) {
				$('#officeCode_desc').html(validator.getValue('DESCRIPTION'));
			}
		}
	}
	if ($('#action').val() == ADD) {
		setFocusLast('remarks');
	} else {
		setFocusLast('enabled');
	}
	return true;
}

function enabled_val() {
	if ($('#action').val() == MODIFY) {
		setFocusLast('remarks');
	}
	return true;
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', HMS_INVALID_REMARKS);
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', HMS_INVALID_REMARKS);
			return false;
		}
	}
	if ($('#action').val() == MODIFY) {
		if (isEmpty(value)) {
			setError('remarks_error', HMS_MANDATORY);
			return false;
		}
	}
	setFocusOnSubmit();
	return true;
}

/*
 * function geoRegionvalidation_val() { if (pinCode_val() && officeCode_val()) {
 * validator.clearMap(); validator.setMtm(false);
 * validator.setValue('PROGRAM_ID', CURRENT_PROGRAM_ID);
 * validator.setValue('GEO_REGION_CODE', $('#geoRegionCode').val());
 * validator.setValue('PINCODE_GEOUNITID', $('#pinCode').val());
 * validator.setValue('OFFICE_CODE', $('#officeCode').val());
 * validator.setValue(ACTION, USAGE);
 * validator.setClass('patterns.config.web.forms.cmn.mgeoregionsbean');
 * validator.setMethod('validateGeoRegion'); validator.sendAndReceive(); if
 * (validator.getValue(ERROR) != EMPTY_STRING) { setError('geoRegionCode_error',
 * validator.getValue(ERROR)); return false; } return true; } }
 * 
 * function revalidate() { var errors = 0; if (!geoRegionvalidation_val()) {
 * errors++; } }
 */

function mapUpdates(overlays) {
	$('#mapData').val(JSON.stringify(overlays));
	$('#mapInvNo').val(EMPTY_STRING);
}

var mapObject = {
	map : "map",
	overlays : [],
	index : 1,
	geocoder : {},
	location : "India",
	drawingManager : {},
	selectedOverlay : -1,
	defaultRegionOptions : {
		fillColor : '#3dafed',
		fillOpacity : 0.6,
		strokeColor : '#3dafed',
		strokeOpacity : 0.9,
		strokeWeight : 2,
		clickable : true,
		editable : true,
		draggable : true,
		zIndex : 1
	},
	defaultRegionStyleOptions : {
		fillColor : '#3dafed',
		strokeColor : '#3dafed'
	},
	selectedRegionStyleOptions : {
		fillColor : '#cccccc',
		strokeColor : '#cccccc'
	},
	initMap : function() {
		var that = this;
		this.map = new google.maps.Map(document.getElementById('map'), {
			center : {
				lat : -34.397,
				lng : 150.644
			},
			zoom : 5,
			scaleControl : false
		});
		this.geocoder = new google.maps.Geocoder();
		this.setDefaultLocation();
		this.drawingManager = new google.maps.drawing.DrawingManager({
			drawingControl : true,
			drawingControlOptions : {
				position : google.maps.ControlPosition.TOP_CENTER,
				drawingModes : [ google.maps.drawing.OverlayType.MARKER, google.maps.drawing.OverlayType.CIRCLE, google.maps.drawing.OverlayType.POLYGON, google.maps.drawing.OverlayType.RECTANGLE ]
			},
			markerOptions : {
				icon : 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png'
			},
			circleOptions : that.defaultRegionOptions,
			polygonOptions : that.defaultRegionOptions,
			rectangleOptions : that.defaultRegionOptions
		});
		this.drawingManager.setMap(this.map);
		var regionTrashControl = new this.createControl(this.map, {
			icon : "fa fa-trash-o",
			title : "Click to remove the selected region",
			clickHandler : function() {
				that.removeOverlay(that.selectedOverlay);
			}
		});
		var circularPolygonConversionControl = new this.createControl(this.map, {
			icon : "fa fa-unlink",
			title : "Click to change circle to polygon",
			clickHandler : function() {
				if (that.selectedOverlay != -1) {
					if (that.selectedOverlay instanceof google.maps.Circle) {
						var overlay = that.convertToCircularPolygon(that.map, that.selectedOverlay);
						that.addOverlay(overlay);
					}
				}
			}
		});

		google.maps.event.addListener(this.map, 'click', function(event) {
			$.each(that.overlays, function(index, value) {
				value.setOptions(that.defaultRegionStyleOptions);
			});

		});
		google.maps.event.addListener(this.drawingManager, 'overlaycomplete', function(event) {
			that.drawingManager.setDrawingMode(null);
			var overlay = event.overlay;
			if (that.overlays.length == 0) {
				that.addOverlay(overlay);
			} else {
				alert(PBS_MAP_REGION_ALREADY_CREATED);
				overlay.setMap(null);
			}

			if (overlay instanceof google.maps.Polygon) {
				var path = overlay.getPath();
				google.maps.event.addListener(path, 'set_at', function(event) {
					that.polygonListener(path, overlay);
				});
				google.maps.event.addListener(path, 'insert_at', function(event) {
					that.polygonListener(path, overlay);
				});

			} else if (overlay instanceof google.maps.Circle) {
				google.maps.event.addListener(overlay, 'center_changed', function(event) {
					that.circleListener(overlay);
				});
				google.maps.event.addListener(overlay, 'radius_changed', function(event) {
					that.circleListener(overlay);
				});

			} else if (overlay instanceof google.maps.Rectangle) {
				google.maps.event.addListener(overlay, 'bounds_changed', function(event) {
					that.rectangleListener(overlay);
				});

			}

		});
	},
	setDefaultLocation : function() {
		var that = this;

		this.geocoder.geocode({
			'address' : this.location
		}, function(results, status) {
			if (status == google.maps.GeocoderStatus.OK) {
				that.map.setCenter(results[0].geometry.location);
			} else {
				alert("Could not find location: " + that.location);
			}
			that.map.setZoom(5);
		});
	},
	createControl : function(map, options) {
		var position = google.maps.ControlPosition.TOP_CENTER;
		var controlDiv = document.createElement('div');
		controlDiv.index = this.index++;
		map.controls[position].push(controlDiv);
		var controlUI = document.createElement('div');
		controlUI.style.backgroundColor = '#fff';
		controlUI.style.border = '2px solid #fff';
		controlUI.style.borderRadius = '3px';
		controlUI.style.boxShadow = '0 2px 6px rgba(0,0,0,.3)';
		controlUI.style.cursor = 'pointer';
		controlUI.style.marginTop = '5px';
		controlUI.style.marginBottom = '22px';
		controlUI.style.textAlign = 'center';
		controlUI.title = options.title;
		controlDiv.appendChild(controlUI);

		var controlText = document.createElement('div');
		controlText.style.color = 'rgb(25,25,25)';
		controlText.style.fontFamily = 'Roboto,Arial,sans-serif';
		controlText.style.fontSize = '16px';
		controlText.style.lineHeight = '20px';
		controlText.style.paddingLeft = '5px';
		controlText.style.paddingRight = '5px';
		controlText.innerHTML = '<i class="' + options.icon + '"></i>';
		controlUI.appendChild(controlText);
		controlUI.addEventListener('click', options.clickHandler);
	},
	convertToCircularPolygon : function(map, circleOverlay) {
		var center = circleOverlay.getCenter();
		var rad = circleOverlay.getRadius() * 0.000621371;
		var d2r = Math.PI / 180;
		circleLatLngs = new Array(); // latLngs of circle
		var circleLat = (rad / 3963.189) / d2r; // miles
		var circleLng = circleLat / Math.cos(center.lat() * d2r);
		// Create polygon points (extra point to close polygon)
		for (var i = 0; i < 361; i = i + 18) {
			// Convert degrees to radians
			var theta = i * d2r;
			var vertexLat = center.lat() + (circleLat * Math.sin(theta));
			var vertexLng = center.lng() + (circleLng * Math.cos(theta));
			var vertextLatLng = new google.maps.LatLng(parseFloat(vertexLat), parseFloat(vertexLng));
			circleLatLngs.push(vertextLatLng);
		}
		var polygonOptions = this.clone(this.defaultRegionOptions);
		polygonOptions.paths = circleLatLngs;
		var circularPolygonOverlay = new google.maps.Polygon(polygonOptions);
		circularPolygonOverlay.setMap(map);
		this.removeOverlay(circleOverlay);
		return circularPolygonOverlay;
	},
	clone : function(obj) {
		if (null == obj || "object" != typeof obj)
			return obj;
		var copy = obj.constructor();
		for ( var attr in obj) {
			if (obj.hasOwnProperty(attr))
				copy[attr] = obj[attr];
		}
		return copy;
	},
	addOverlay : function(overlay) {
		var that = this;
		this.overlays.push(overlay);
		google.maps.event.addListener(overlay, 'click', function(e) {
			that.selectedOverlay = this;
			$.each(that.overlays, function(index, value) {
				value.setOptions(that.defaultRegionStyleOptions);
			});
			this.setOptions(that.selectedRegionStyleOptions);
		});
		this.updateMapData();
	},
	updateMapData : function() {
		for (var i = 0; i < this.overlays.length; ++i) {
			var overlay = this.overlays[i];
			if (overlay instanceof google.maps.Polygon) {
				var pathsObj = overlay.getPaths().getArray()[0].getArray();
				mapUpdates({
					type : "Polygon",
					paths : pathsObj
				});
			} else if (overlay instanceof google.maps.Circle) {
				var paths = {
					type : "Circle",
					center : overlay.getCenter(),
					radius : overlay.getRadius()
				};
				mapUpdates(paths);
			} else if (overlay instanceof google.maps.Rectangle) {
				var paths = {
					type : "Rectangle",
					bounds : overlay.getBounds().toJSON()
				};
				mapUpdates(paths);
			}
		}
	},
	removeOverlay : function(overlay) {
		if (overlay != -1) {
			overlay.setMap(null);
			this.overlays.splice(this.overlays.indexOf(overlay), 1);
			overlay = -1;
		}
		this.updateMapData();
	},
	drawMapWithOverlay : function(overlayObject) {
		var type = overlayObject.type;
		switch (type) {
		case "Polygon":
			this.drawPolygon(overlayObject.paths);
			break;
		case "Circle":
			this.drawCircle(overlayObject.center, overlayObject.radius);
			break;
		case "Rectangle":
			this.drawRectangle(overlayObject.bounds);
			break;
		default:
			this.drawDefaultMap();
			break;
		}
	},
	drawPolygon : function(polygonPath) {
		var that = this;
		var mapPolygon = new google.maps.Polygon({
			paths : polygonPath,
			strokeColor : '#3dafed',
			strokeOpacity : 0.9,
			strokeWeight : 2,
			fillColor : '#3dafed',
			clickable : true,
			editable : true,
			draggable : true,
			fillOpacity : 0.35
		});
		mapPolygon.setMap(this.map);
		setTimeout(function() {
			that.map.fitBounds(that.getBounds(mapPolygon));
		}, 100);
		this.overlays.push(mapPolygon);
		google.maps.event.addListener(mapPolygon, 'click', function(event) {
			that.selectedOverlay = this;
			this.setOptions(that.selectedRegionStyleOptions);
		});
		var path = mapPolygon.getPath();
		google.maps.event.addListener(path, 'set_at', function(event) {
			that.polygonListener(path, mapPolygon);
		});
		google.maps.event.addListener(path, 'insert_at', function(event) {
			that.polygonListener(path, mapPolygon);
		});
	},
	drawCircle : function(center, radius) {
		var that = this;
		var mapCircle = new google.maps.Circle({
			strokeColor : '#3dafed',
			strokeOpacity : 0.9,
			strokeWeight : 2,
			fillColor : '#3dafed',
			fillOpacity : 0.35,
			center : center,
			radius : radius,
			clickable : true,
			editable : true,
			draggable : true
		});
		mapCircle.setMap(this.map);
		setTimeout(function() {
			that.map.fitBounds(mapCircle.getBounds());
		}, 100);
		this.overlays.push(mapCircle);
		google.maps.event.addListener(mapCircle, 'click', function(event) {
			that.selectedOverlay = this;
			this.setOptions(that.selectedRegionStyleOptions);
		});
		google.maps.event.addListener(mapCircle, 'center_changed', function(event) {
			that.circleListener(mapCircle);
		});
		google.maps.event.addListener(mapCircle, 'radius_changed', function(event) {
			that.circleListener(mapCircle);
		});
	},
	drawRectangle : function(bounds) {
		var that = this;
		var mapRectangle = new google.maps.Rectangle({
			strokeColor : '#3dafed',
			strokeOpacity : 0.9,
			strokeWeight : 2,
			fillColor : '#3dafed',
			fillOpacity : 0.35,
			clickable : true,
			editable : true,
			draggable : true,
			bounds : bounds
		});
		mapRectangle.setMap(this.map);
		setTimeout(function() {
			that.map.fitBounds(mapRectangle.getBounds());
		}, 100);
		this.overlays.push(mapRectangle);
		google.maps.event.addListener(mapRectangle, 'click', function(event) {
			that.selectedOverlay = this;
			this.setOptions(that.selectedRegionStyleOptions);
		});
		google.maps.event.addListener(mapRectangle, 'bounds_changed', function(event) {
			that.rectangleListener(mapRectangle);
		});

	},
	getBounds : function(polygon) {
		var bounds = new google.maps.LatLngBounds();
		polygon.getPath().forEach(function(element, index) {
			bounds.extend(element);
		})
		return bounds;
	},
	getCenter : function(polygon) {
		return this.getBounds(polygon).getCenter();
	},
	circleListener : function(mapCircle) {
		var paths = {
			type : "Circle",
			center : mapCircle.getCenter(),
			radius : mapCircle.getRadius()
		};
		mapUpdates(paths);
	},
	rectangleListener : function(mapRectangle) {
		var paths = {
			type : "Rectangle",
			bounds : mapRectangle.getBounds().toJSON()
		};
		mapUpdates(paths);
	},
	polygonListener : function(path, mapPolygon) {
		var pathsObj = mapPolygon.getPaths().getArray()[0].getArray();
		mapUpdates({
			type : "Polygon",
			paths : pathsObj
		});
	}

};

function revalidate() {
	if ($('#mapData').val() == EMPTY_STRING) {
		setError('geoRegionCode_error', PBS_MAP_REGION_NOT_CREATED);
		errors++;
	}
}