var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MGEOUNITCON';
function init() {
	refreshTBAGrid();
	refreshQueryGrid();
	if (_redisplay) {
		if ($('#unitTypeAllowed').val() == 1) {
			$('#geoUnitType').prop('readOnly', false);
			$('#geoUnitType_pic').prop('disabled', false);
		} else {
			$('#geoUnitType').prop('readOnly', true);
			$('#geoUnitType_pic').prop('disabled', true);
		}
		if ($('#action').val() == ADD) {
			setCheckbox('enabled', YES);
			$('#enabled').prop('disabled', true);
		}
		if ($('#action').val() == MODIFY) {
			$('#enabled').prop('disabled', false);
		}
	}
}

function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'MGEOUNITCON_MAIN');
}
function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}
function add() {
	setFocusLast('countryCode');
	setCheckbox('enabled', YES);
	$('#enabled').prop('disabled', true);
}
function modify() {
	setFocusLast('countryCode');
	if ($('#action').val() == MODIFY) {
		$('#enabled').prop('disabled', false);
		setCheckbox('enabled', NO);
	} else {
		setCheckbox('enabled', YES);
		$('#enabled').prop('disabled', true);
	}
}

function loadData() {
	$('#countryCode').val(validator.getValue('COUNTRY_CODE'));
	$('#countryCode_desc').html(validator.getValue('F1_DESCRIPTION'));
	$('#geoUnitID').val(validator.getValue('GEO_UNIT_ID'));
	$('#name').val(validator.getValue('DESCRIPTION'));
	$('#shortName').val(validator.getValue('CONCISE_DESCRIPTION'));
	$('#parentgeoUnitID').val(validator.getValue('PARENT_GEO_UNIT_ID'));
	$('#parentgeoUnitID_desc').html(validator.getValue('F2_DESCRIPTION'));
	$('#geoStructure').val(validator.getValue('GEO_UNIT_STRUC_CODE'));
	$('#geoStructure_desc').html(validator.getValue('F3_DESCRIPTION'));
	$('#parentGeoStructure').val(validator.getValue('F3_PARENT_GUS_CODE'));
	$('#parentGeoStructure_desc').html(validator.getValue('F5_DESCRIPTION'));
	$('#geoUnitType').val(validator.getValue('GEO_UNIT_TYPE'));
	$('#geoUnitType_desc').html(validator.getValue('F4_DESCRIPTION'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	if (isEmpty($('#geoUnitType').val())) {
		$('#geoUnitType').prop('readOnly', true);
		$('#geoUnitType_pic').prop('disabled', true);
	} else {
		$('#geoUnitType').prop('readOnly', false);
		$('#geoUnitType_pic').prop('disabled', false);
	}
}

function view(source, primaryKey) {
	hideParent('cmn/qgeounitcon', source, primaryKey);
	resetLoading();
}

function clearFields() {
	$('#countryCode').val(EMPTY_STRING);
	$('#countryCode_desc').html(EMPTY_STRING);
	$('#countryCode_error').html(EMPTY_STRING);
	$('#geoUnitID').val(EMPTY_STRING);
	$('#geoUnitID_desc').html(EMPTY_STRING);
	$('#geoUnitID_error').html(EMPTY_STRING);
	clearNonPKFields();
}

function clearNonPKFields() {
	$('#name').val(EMPTY_STRING);
	$('#name_error').html(EMPTY_STRING);
	$('#shortName').val(EMPTY_STRING);
	$('#shortName_error').html(EMPTY_STRING);
	$('#parentgeoUnitID').val(EMPTY_STRING);
	$('#parentgeoUnitID_desc').html(EMPTY_STRING);
	$('#parentgeoUnitID_error').html(EMPTY_STRING);
	$('#geoStructure').val(EMPTY_STRING);
	$('#geoStructure_desc').html(EMPTY_STRING);
	$('#geoStructure_error').html(EMPTY_STRING);
	$('#parentGeoStructure').val(EMPTY_STRING);
	$('#parentGeoStructure_desc').html(EMPTY_STRING);
	$('#parentGeoStructure_error').html(EMPTY_STRING);
	$('#geoUnitType').val(EMPTY_STRING);
	$('#geoUnitType_desc').html(EMPTY_STRING);
	$('#geoUnitType_error').html(EMPTY_STRING);
	$('#enabled_error').html(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
}

function backtrack(id) {
	switch (id) {
	case 'countryCode':
		setFocusLast('countryCode');
		break;
	case 'geoUnitID':
		setFocusLast('countryCode');
		break;
	case 'name':
		setFocusLast('geoUnitID');
		break;
	case 'shortName':
		setFocusLast('name');
		break;
	case 'parentgeoUnitID':
		setFocusLast('shortName');
		break;
	case 'geoStructure':
		setFocusLast('parentgeoUnitID');
		break;
	case 'geoUnitType':
		setFocusLast('geoStructure');
		break;
	case 'enabled':
		if ($('#geoUnitType').prop('readOnly'))
			setFocusLast('geoStructure');
		else
			setFocusLast('geoUnitType');
		break;
	case 'remarks':
		if ($('#action').val() == MODIFY) {
			setFocusLast('enabled');
		} else {
			if ($('#geoUnitType').prop('readOnly'))
				setFocusLast('geoStructure');
			else
				setFocusLast('geoUnitType');
		}
		break;
	}
}
function validate(id) {
	switch (id) {
	case 'countryCode':
		countryCode_val();
		break;
	case 'geoUnitID':
		geoUnitID_val();
		break;
	case 'name':
		name_val();
		break;
	case 'shortName':
		shortName_val();
		break;
	case 'parentgeoUnitID':
		parentgeoUnitID_val();
		break;
	case 'geoStructure':
		geoStructure_val();
		break;
	case 'parentGeoStructure':
		parentGeoStructure_val();
		break;
	case 'geoUnitType':
		geoUnitType_val();
		break;
	case 'enabled':
		enabled();
		break;
	case 'remarks':
		remarks_val();
		break;
	}
}
function doclearfields(id) {
	switch (id) {
	case 'countryCode':
		$('#countryCode').val(EMPTY_STRING);
		$('#countryCode_desc').html(EMPTY_STRING);
		$('#countryCode_error').html(EMPTY_STRING);
		clearNonPKFields();
		break;
	case 'geoUnitID':
		$('#geoUnitID').val(EMPTY_STRING);
		$('#geoUnitID_desc').html(EMPTY_STRING);
		$('#geoUnitID_error').html(EMPTY_STRING);
		clearNonPKFields();
		break;
	case 'geoStructure':
		$('#geoStructure').val(EMPTY_STRING);
		$('#geoStructure_desc').html(EMPTY_STRING);
		$('#geoStructure_error').html(EMPTY_STRING);
		$('#parentGeoStructure').val(EMPTY_STRING);
		$('#parentGeoStructure_desc').html(EMPTY_STRING);
		break;
	}
}

function doHelp(id) {
	switch (id) {
	case 'countryCode':
		help('COMMON', 'HLP_COUNTRY', $('#countryCode').val(), EMPTY_STRING, $('#countryCode'));
		break;
	case 'geoUnitID':
		help('COMMON', 'HLP_GEOUNITCON_M', $('#geoUnitID').val(), $('#countryCode').val(), $('#geoUnitID'));
		break;
	case 'parentgeoUnitID':
		help('COMMON', 'HLP_GEOUNITCON_M', $('#parentgeoUnitID').val(), $('#countryCode').val(), $('#parentgeoUnitID'));
		break;
	case 'geoStructure':
		help('COMMON', 'HLP_GEOUNITSTRUC_M', $('#geoStructure').val(), EMPTY_STRING, $('#geoStructure'));
		break;
	case 'geoUnitType':
		help('COMMON', 'HLP_GEOUNITTYPE_M', $('#geoUnitType').val(), $('#geoStructure').val(), $('#geoUnitType'));
		break;
	}
}

function countryCode_val() {
	var value = $('#countryCode').val();
	$('#countryCode_desc').html(EMPTY_STRING);
	clearError('countryCode_error');
	if (isEmpty(value)) {
		setError('countryCode_error', MANDATORY);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('COUNTRY_CODE', value);
	validator.setValue(ACTION, USAGE);
	validator.setClass('patterns.config.web.forms.cmn.mgeounitconbean');
	validator.setMethod('validateCountryCode');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('countryCode_error', validator.getValue(ERROR));
		return false;
	}
	$('#countryCode_desc').html(validator.getValue('DESCRIPTION'));
	setFocusLast('geoUnitID');
	return true;
}

function geoUnitID_val() {
	var value = $('#geoUnitID').val();
	$('#geoUnitID_desc').html(EMPTY_STRING);
	clearError('geoUnitID_error');
	if (isEmpty(value)) {
		setError('geoUnitID_error', MANDATORY);
		return false;
	}
	if ($('#action').val() == ADD && !isValidCode(value)) {
		setError('geoUnitID_error', INVALID_FORMAT);
		return false;
	}
	if ($('#action').val() == ADD) {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('COUNTRY_CODE', $('#countryCode').val());
		validator.setValue('GEO_UNIT_ID', value);
		validator.setValue('ACTION', $('#action').val());
		validator.setClass('patterns.config.web.forms.cmn.mgeounitconbean');
		validator.setMethod('validateGeoUnitID');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('geoUnitID_error', validator.getValue(ERROR));
			return false;
		}
		setFocusLast('name');
		return true;
	} else {
		PK_VALUE = $('#countryCode').val() + PK_SEPERATOR + $('#geoUnitID').val();
		loadPKValues(PK_VALUE, 'geoUnitID_error');
		if (!isEmpty($('#geoUnitID_error').html()))
			setFocusLast('geoUnitID');
		else
			setFocusLast('name');
		return true;
	}
}

function name_val() {
	var value = $('#name').val();
	clearError('name_error');
	if (isEmpty(value)) {
		setError('name_error', MANDATORY);
		return false;
	}
	if (!isValidName(value)) {
		setError('name_error', INVALID_NAME);
		return false;
	}
	setFocusLast('shortName');
	return true;
}

function shortName_val() {
	var value = $('#shortName').val();
	clearError('shortName_error');
	if (isEmpty(value)) {
		setError('shortName_error', MANDATORY);
		return false;
	}
	if (!isValidShortName(value)) {
		setError('shortName_error', PBS_INVALID_SHORT_NAME);
		return false;
	}
	setFocusLast('parentgeoUnitID');
	return true;
}

function parentgeoUnitID_val() {
	var parentGeoUnitID = $('#parentgeoUnitID').val();
	var geoUnitID = $('#geoUnitID').val();
	$('#parentgeoUnitID_desc').html(EMPTY_STRING);
	clearError('parentgeoUnitID_error');
	if (!isEmpty(parentGeoUnitID)) {
		if (geoUnitID == parentGeoUnitID) {
			setError('parentgeoUnitID_error', PBS_GEOUNIT_NT_SAME_PARENT);
			return false;
		}
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue(ACTION, USAGE);
		validator.setValue('COUNTRY_CODE', $('#countryCode').val());
		validator.setValue('UNIT_ID', geoUnitID);
		validator.setValue('GEO_UNIT_ID', parentGeoUnitID);
		validator.setClass('patterns.config.web.forms.cmn.mgeounitconbean');
		validator.setMethod('validateParentGeoUnitID');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('parentgeoUnitID_error', validator.getValue(ERROR));
			return false;
		}
		$('#parentgeoUnitID_desc').html(validator.getValue('DESCRIPTION'));
	}
	setFocusLast('geoStructure');
	return true;
}

function geoStructure_val() {
	var value = $('#geoStructure').val();
	$('#geoStructure_desc').html(EMPTY_STRING);
	clearError('geoStructure_error');
	if (isEmpty(value)) {
		setError('geoStructure_error', MANDATORY);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue(ACTION, USAGE);
	validator.setValue('COUNTRY_CODE', $('#countryCode').val());
	validator.setValue('GEO_UNIT_STRUC_CODE', value);
	validator.setClass('patterns.config.web.forms.cmn.mgeounitconbean');
	validator.setMethod('validateGeoStructure');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('geoStructure_error', validator.getValue(ERROR));
		return false;
	}
	$('#geoStructure_desc').html(validator.getValue('DESCRIPTION'));
	$('#parentGeoStructure_desc').html(validator.getValue('PARENT_DESCRIPTION'));
	$('#parentGeoStructure').val(validator.getValue('PARENT_GUS_CODE'));
	$('#unitTypeAllowed').val(validator.getValue('UNIT_TYPE_REQ'));
	if($('#unitTypeAllowed').val() == 1) {
		$('#geoUnitType').prop('readOnly', false);
		$('#geoUnitType_pic').prop('disabled', false);
		setFocusLast('geoUnitType');
	} else {
		$('#geoUnitType').val(EMPTY_STRING);
		$('#geoUnitType_error').html(EMPTY_STRING);
		$('#geoUnitType_desc').html(EMPTY_STRING);
		$('#geoUnitType').prop('readOnly', true);
		$('#geoUnitType_pic').prop('disabled', true);
		if ($('#action').val() == ADD) {
			setFocusLast('remarks');
		} else {
			setFocusLast('enabled');
		}
	}
	return true;
}
function geoUnitType_val() {
	var value = $('#geoUnitType').val();
	$('#geoUnitType_desc').html(EMPTY_STRING);
	clearError('geoUnitType_error');
	if ($('#unitTypeAllowed').val() == 1) {
		if (isEmpty(value)) {
			setError('geoUnitType_error', MANDATORY);
			return false;
		}
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue(ACTION, USAGE);
		validator.setValue('GEO_UNIT_STRUC_CODE', $('#geoStructure').val());
		validator.setValue('GEO_UNIT_TYPE', value);
		validator.setClass('patterns.config.web.forms.cmn.mgeounitconbean');
		validator.setMethod('validateGeoUnitType');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('geoUnitType_error', validator.getValue(ERROR));
			return false;
		}
		$('#geoUnitType_desc').html(validator.getValue('DESCRIPTION'));
	} else {
		if (!isEmpty(value)) {
			setError('geoUnitType_error', PBS_VALUE_NT_ALLOWED);
			return false;
		}
	}
	if ($('#action').val() == ADD) {
		setFocusLast('remarks');
	} else {
		setFocusLast('enabled');
	}
	return true;
}

function enabled() {
	if ($('#action').val() == MODIFY)
		setFocusLast('remarks');
	return true;
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', HMS_INVALID_REMARKS);
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', HMS_INVALID_REMARKS);
			return false;
		}
	}
	if ($('#action').val() == MODIFY) {
		if (isEmpty(value)) {
			setError('remarks_error', HMS_MANDATORY);
			return false;
		}

	}
	setFocusOnSubmit();
	return true;
}
