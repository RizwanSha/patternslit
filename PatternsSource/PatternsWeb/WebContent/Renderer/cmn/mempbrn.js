var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MEMPBRN';
function init() {
	refreshTBAGrid();
	refreshQueryGrid();
	if ($('#action').val() == ADD) {
		$('#enabled').prop('disabled', true);
		setCheckbox('enabled', YES);
	}
}

function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'MEMPBRN_MAIN', queryGrid);
}

function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}

function clearFields() {
	$('#employeeCode').val(EMPTY_STRING);
	$('#employeeCode_error').html(EMPTY_STRING);
	$('#employeeCode_desc').html(EMPTY_STRING);
	$('#effectiveDate').val(EMPTY_STRING);
	$('#effectiveDate_error').html(EMPTY_STRING);
	displayFields();
	clearNonPKFields();
}
function clearNonPKFields() {
	$('#newOfficeCode').val(EMPTY_STRING);
	$('#newOfficeCode_desc').html(EMPTY_STRING);
	$('#newOfficeCode_error').html(EMPTY_STRING);
	setCheckbox('enabled', NO);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
}

function displayFields() {
	$('#currtOfficeCode').val(EMPTY_STRING);
	$('#currtOfficeCode_desc').html(EMPTY_STRING);
}

function doclearfields(id) {
	switch (id) {
	case 'employeeCode':
		if (isEmpty($('#employeeCode').val())) {
			clearFields();
		}
		break;
	case 'effectiveDate':
		if (isEmpty($('#effectiveDate').val())) {
			clearNonPKFields();
		}
		break;
	}
}

function doHelp(id) {
	switch (id) {
	case 'employeeCode':
		help('COMMON', 'HLP_EMPDB_CODE', $('#employeeCode').val(), EMPTY_STRING, $('#employeeCode'));
		break;

	case 'effectiveDate':
		help(CURRENT_PROGRAM_ID, 'HLP_MEMPBRN_EFF', $('#effectiveDate').val(), $('#employeeCode').val(), $('#effectiveDate'));
		break;

	case 'newOfficeCode':
		help('COMMON', 'HLP_OFFICE_CODE', $('#newOfficeCode').val(), EMPTY_STRING, $('#newOfficeCode'));
		break;

	}
}

function add() {
	$('#employeeCode').focus();
	setCheckbox('enabled', YES);
	$('#enabled').prop('disabled', true);
}
function modify() {
	$('#employeeCode').focus();
	if ($('#action').val() == MODIFY) {
		$('#enabled').prop('disabled', false);
	} else {
		$('#enabled').prop('disabled', true);
	}
}
function loadData() {
	$('#employeeCode').val(validator.getValue('EMP_CODE'));
	$('#employeeCode_desc').html(validator.getValue('F1_PERSON_NAME'));
	$('#effectiveDate').val(validator.getValue('EFF_DATE'));
	$('#newOfficeCode').val(validator.getValue('BRANCH_CODE'));
	$('#newOfficeCode_desc').html(validator.getValue('F2_DESCRIPTION'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	currentOfficeDetails();
	resetLoading();
}

function currentOfficeDetails() {
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('ENTITY_CODE', getEntityCode());
	validator.setValue('EMP_CODE', $('#employeeCode').val());
	validator.setValue(ACTION, USAGE);
	validator.setClass('patterns.config.web.forms.cmn.mempbrnbean');
	validator.setMethod('validateEmployeeCodeDetails');
	validator.sendAndReceive();
	if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		$('#currtOfficeCode').val(validator.getValue("BRANCH_CODE"));
		$('#currtOfficeCode_desc').html(validator.getValue("DESCRIPTION"));
	}
	return true;
}

function view(source, primaryKey) {
	hideParent('cmn/qempbrn', source, primaryKey);
	resetLoading();
}
function validate(id) {
	switch (id) {
	case 'employeeCode':
		employeeCode_val();
		break;
	case 'effectiveDate':
		effectiveDate_val();
		break;
	case 'newOfficeCode':
		newOfficeCode_val();
		break;
	case 'enabled':
		enabled_val();
		break;
	case 'remarks':
		remarks_val();
		break;
	}
}

function backtrack(id) {
	switch (id) {
	case 'employeeCode':
		setFocusLast('employeeCode');
		break;
	case 'effectiveDate':
		setFocusLast('employeeCode');
		break;
	case 'newOfficeCode':
		setFocusLast('effectiveDate');
		break;
	case 'enabled':
		setFocusLast('newOfficeCode');
		break;
	case 'remarks':
		if ($('#action').val() == MODIFY) {
			setFocusLast('enabled');
		} else {
			setFocusLast('newOfficeCode');
		}
		break;
	}
}

function employeeCode_val() {
	var value = $('#employeeCode').val();
	clearError('employeeCode_error');
	if (isEmpty(value)) {
		setError('employeeCode_error', HMS_MANDATORY);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('ENTITY_CODE', getEntityCode());
		validator.setValue('EMP_CODE', $('#employeeCode').val());
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.cmn.mempbrnbean');
		validator.setMethod('validateEmployeeCode');
		validator.sendAndReceive();
		if (validator.getValue(RESULT) == DATA_AVAILABLE) {
			$('#currtOfficeCode').val(validator.getValue("BRANCH_CODE"));
			$('#currtOfficeCode_desc').html(validator.getValue("DESCRIPTION"));
			$('#employeeCode_desc').html(validator.getValue("PERSON_NAME"));
		} else if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('employeeCode_error', validator.getValue(ERROR));
			$('#employeeCode_desc').html(EMPTY_STRING);
			displayFields();
			return false;
		}
	}
	setFocusLast('effectiveDate');
	return true;
}

function effectiveDate_val() {
	var value = $('#effectiveDate').val();
	clearError('effectiveDate_error');
	if (isEmpty(value)) {
		$('#effectiveDate').val(getCBD());
	}
	if (!isValidEffectiveDate($('#effectiveDate').val(), 'effectiveDate_error')) {
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('ENTITY_CODE', getEntityCode());
		validator.setValue('EMP_CODE', $('#employeeCode').val());
		validator.setValue('EFF_DATE', $('#effectiveDate').val());
		validator.setValue(ACTION, $('#action').val());
		validator.setClass('patterns.config.web.forms.cmn.mempbrnbean');
		validator.setMethod('validateEffectiveDate');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('effectiveDate_error', validator.getValue(ERROR));
			return false;
		}
	}
	if ($('#action').val() == MODIFY) {
		PK_VALUE = getEntityCode() + PK_SEPERATOR + $('#employeeCode').val() + PK_SEPERATOR + $('#effectiveDate').val();
		if (!loadPKValues(PK_VALUE, 'effectiveDate_error')) {
			return false;
		}
	}
	setFocusLast('newOfficeCode');
	return true;
}

function newOfficeCode_val() {
	var value = $('#newOfficeCode').val();
	clearError('newOfficeCode_error');
	$('#newOfficeCode_desc').html(EMPTY_STRING);
	if (isEmpty(value)) {
		setError('newOfficeCode_error', MANDATORY);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('CURR_BRANCH_CODE', $('#currtOfficeCode').val());
		validator.setValue('BRANCH_CODE', $('#newOfficeCode').val());
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.cmn.mempbrnbean');
		validator.setMethod('validateNewOfficeCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('newOfficeCode_error', validator.getValue(ERROR));
			return false;
		} else {
			if (validator.getValue(RESULT) == DATA_AVAILABLE) {
				$('#newOfficeCode_desc').html(validator.getValue('DESCRIPTION'));
			}
		}
	}
	if ($('#action').val() == ADD) {
		setFocusLast('remarks');
	} else {
		setFocusLast('enabled');
	}
	return true;
}

function enabled_val() {
	if ($('#action').val() == MODIFY) {
		setFocusLast('remarks');
	}
	return true;
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', HMS_INVALID_REMARKS);
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', HMS_INVALID_REMARKS);
			return false;
		}
	}
	if ($('#action').val() == MODIFY) {
		if (isEmpty(value)) {
			setError('remarks_error', HMS_MANDATORY);
			return false;
		}
	}
	setFocusOnSubmit();
	return true;

}
