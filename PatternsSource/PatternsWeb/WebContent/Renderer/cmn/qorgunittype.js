var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MORGUNITTYPE';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}
function clearFields() {
	$('#orgUnitType').val(EMPTY_STRING);
	$('#description').val(EMPTY_STRING);
	$('#concDescription').val(EMPTY_STRING);
	$('#parentorgUnitType').val(EMPTY_STRING);
	setCheckbox('operunit', NO);
	setCheckbox('selfBankingUnit', NO);
	setCheckbox('backOffOperUnit', NO);
	setCheckbox('itCell', NO);
	$('#remarks').val(EMPTY_STRING);
}
function loadData() {
	$('#orgUnitType').val(validator.getValue('ORG_UNIT_TYPE'));
	$('#description').val(validator.getValue('DESCRIPTION'));
	$('#concDescription').val(validator.getValue('CONCISE_DESCRIPTION'));
	if (!isEmpty(validator.getValue('PARENT_ORG_UNIT_TYPE'))) {
		$('#parentorgUnitType').val(validator.getValue('PARENT_ORG_UNIT_TYPE'));
		$('#parentorgUnitType_desc').html(validator.getValue('F1_DESCRIPTION'));
	}
	setCheckbox('operunit', validator.getValue('FO_OPERATING_UNIT'));
	setCheckbox('selfBankingUnit', validator.getValue('SELF_BANKING_UNIT'));
	setCheckbox('backOffOperUnit', validator.getValue('BO_OPERATING_UNIT'));
	setCheckbox('itCell', validator.getValue('IT_CELL'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadAuditFields(validator);
}