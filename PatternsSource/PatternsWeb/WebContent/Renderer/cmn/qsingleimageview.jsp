<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="resource" tagdir="/WEB-INF/tags/resource"%>
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/cmn/qsingleimageview.js" />
	</web:dependencies>
	<web:loadPK />
	<web:viewPartNew>
		<web:viewSubPart>
		</web:viewSubPart>
		<web:viewSubPart>
			<div class="content" id="content">
				<web:form action="/Renderer/cmn/qsingleimageview" id="qsingleimageview" method="POST">
					<div class="image-content" id="img-content">
						<div class="singleimage" id="singleimage">
							<p>
								<span id="info" style="color: green"> </span>
							</p>
							<img id="picture-slides-image" name="picture-slides-image" />
						</div>
					</div>
				</web:form>
			</div>
		</web:viewSubPart>
	</web:viewPartNew>
</web:fragment>