var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MCURRENCY';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}
function clearFields() {
	$('#currencyCode').val(EMPTY_STRING);
	$('#ccyName').val(EMPTY_STRING);
	$('#ccyShortName').val(EMPTY_STRING);
	$('#ccyNotation').val(EMPTY_STRING);
	setCheckbox('hasSubCcy', '0');
	$('#sccyName').val(EMPTY_STRING);
	$('#sccyShortName').val(EMPTY_STRING);
	$('#sccyNotation').val(EMPTY_STRING);
	$('#sccyUnitsForCcy').val(EMPTY_STRING);
	$('#isoCcyCode').val(EMPTY_STRING);
	setCheckbox('enabled', '0');
	$('#remarks').val(EMPTY_STRING);

}
function loadData() {
	$('#currencyCode').val(validator.getValue('CCY_CODE'));
	$('#ccyName').val(validator.getValue('DESCRIPTION'));
	$('#ccyShortName').val(validator.getValue('CONCISE_DESCRIPTION'));
	$('#ccyNotation').val(validator.getValue('CCY_NOTATION'));
	setCheckbox('hasSubCcy', validator.getValue('HAS_SUB_CCY'));
	$('#sccyName').val(validator.getValue('SUB_CCY_DESCRIPTION'));
	$('#sccyShortName').val(validator.getValue('SUB_CCY_CONCISE_DESCRIPTION'));
	$('#sccyNotation').val(validator.getValue('SUB_CCY_NOTATION'));
	$('#sccyUnitsForCcy').val(validator.getValue('SUB_CCY_UNITS_IN_CCY'));
	$('#isoCcyCode').val(validator.getValue('ISO_CCY_CODE'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadAuditFields(validator);
}