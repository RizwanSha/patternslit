var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'EUSERREGIONALLOC';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
	userRegAlloc_innerGrid.setColumnHidden(0, true);
}

function clearFields() {
	$('#fieldUser').val(EMPTY_STRING);
	$('#fieldUser_desc').html(EMPTY_STRING);
	$('#fieldUser_error').html(EMPTY_STRING);
	$('#fieldUserType').val(EMPTY_STRING);
	$('#effectiveDate').val(EMPTY_STRING);
	$('#effectiveDate_error').html(EMPTY_STRING);
	userRegAlloc_innerGrid.clearAll();
	setCheckbox('enabled', NO);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);

}

function loadData() {
	$('#fieldUser').val(validator.getValue('FIELD_USER_ID'));
	$('#fieldUser_desc').html(validator.getValue('F1_USER_NAME'));
	if (validator.getValue('F1_THIRD_PARTY_ORG_CODE')!=EMPTY_STRING)
		$('#fieldUserType').val(PBS_BUSINS_CORRES);
	else if (validator.getValue('F1_EMP_CODE')!=EMPTY_STRING)
		$('#fieldUserType').val(PBS_EMPLOYEE);
	else
		$('#fieldUserType').val(EMPTY_STRING);

	$('#effectiveDate').val(validator.getValue('EFF_DATE'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadAuditFields(validator);
	loadGrid();
}

function loadGrid() {
	SOURCE_VALUE = _tableType;
	PK_VALUE = _primaryKey;
	if (_tableType == MAIN) {
		loads();
	} else if (_tableType == TBA) {
		loads();
	}
}

function loads() {
	loadGridQuery(CURRENT_PROGRAM_ID, 'EUSERREGION_ALLOCATION_GRID', userRegAlloc_innerGrid);
}
