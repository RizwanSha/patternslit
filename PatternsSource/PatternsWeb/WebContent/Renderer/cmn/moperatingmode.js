var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MOPERATINGMODE';
function init() {
	refreshTBAGrid();
	refreshQueryGrid();
	if (_redisplay) {
		if ($('#action').val() == ADD) {
			$('#enabled').prop('disabled', true);
			setCheckbox('enabled', YES);
		}
	}
}

function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'MOPERATINGMODE_MAIN', queryGrid);
}

function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}

function doclearfields(id) {
	switch (id) {
	case 'operatingMode':
		if (isEmpty($('#operatingMode').val())) {
			$('#operatingMode_error').html(EMPTY_STRING);
			$('#operatingMode_desc').html(EMPTY_STRING);
			clearNonPKFields();
			break;
		}
	}
}

function clearFields() {
	$('#operatingMode').val(EMPTY_STRING);
	$('#operatingMode_error').html(EMPTY_STRING);
	$('#operatingMode_desc').html(EMPTY_STRING);
	clearNonPKFields();
}
function clearNonPKFields() {
	$('#description').val(EMPTY_STRING);
	$('#description_error').html(EMPTY_STRING);
	$('#conciseDescription').val(EMPTY_STRING);
	$('#conciseDescription_error').html(EMPTY_STRING);
	setCheckbox('singleAccount', NO);
	setCheckbox('jointAccount', NO);
	$('#jointAccount_error').html(EMPTY_STRING);
	setCheckbox('illtAccountAgent', NO);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
}

function doHelp(id) {
	switch (id) {
	case 'operatingMode':
		help('COMMON', 'HLP_OPERATING_MODE_M', $('#operatingMode').val(), EMPTY_STRING, $('#operatingMode'), function(gridObj, rowID) {
		});
		break;
	}
}

function add() {
	setCheckbox('enabled', YES);
	$('#enabled').prop('disabled', true);
	$('#operatingMode_pic').prop('disabled', true);
	$('#operatingMode').focus();
}

function modify() {
	if ($('#action').val() == MODIFY) {
		$('#enabled').prop('disabled', false);
		$('#operatingMode_pic').prop('disabled', false);
	} else {
		$('#enabled').prop('disabled', true);
		$('#operatingMode_pic').prop('disabled', true);
	}
	$('#operatingMode').focus();
}

function loadData() {
	$('#operatingMode').val(validator.getValue('OPERATING_MODE'));
	$('#description').val(validator.getValue('DESCRIPTION'));
	$('#conciseDescription').val(validator.getValue('CONCISE_DESCRIPTION'));
	setCheckbox('singleAccount', validator.getValue('FOR_SINGLE_ACC'));
	setCheckbox('jointAccount', validator.getValue('FOR_JOINT_ACC'));
	setCheckbox('illtAccountAgent', validator.getValue('FOR_ILLITERATE_ACC'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	resetLoading();
}

function view(source, primaryKey) {
	hideParent('cmn/qoperatingmode', source, primaryKey);
	resetLoading();
}

function validate(id) {
	switch (id) {
	case 'operatingMode':
		operatingMode_val();
		break;
	case 'description':
		description_val();
		break;
	case 'conciseDescription':
		conciseDescription_val();
		break;
	case 'singleAccount':
		singleAccount_val(true);
		break;
	case 'jointAccount':
		jointAccount_val(true);
		break;
	case 'illtAccountAgent':
		illtAccountAgent_val();
		break;
	case 'enabled':
		enabled_val();
		break;
	case 'remarks':
		remarks_val();
		break;
	}
}

function backtrack(id) {
	switch (id) {
	case 'operatingMode':
		setFocusLast('operatingMode');
		break;
	case 'description':
		setFocusLast('operatingMode');
		break;
	case 'conciseDescription':
		setFocusLast('description');
		break;
	case 'singleAccount':
		setFocusLast('conciseDescription');
		break;
	case 'jointAccount':
		setFocusLast('singleAccount');
		break;
	case 'illtAccountAgent':
		setFocusLast('jointAccount');
		break;
	case 'enabled':
		setFocusLast('illtAccountAgent');
		break;
	case 'remarks':
		if ($('#action').val() == MODIFY) {
			setFocusLast('enabled');
		} else {
			setFocusLast('illtAccountAgent');
		}
		break;
	}
}

function checkclick(id) {
	switch (id) {
	case 'singleAccount':
		singleAccount_val(false);
		break;
	case 'jointAccount':
		jointAccount_val(false);
		break;
	}
}

function operatingMode_val() {
	var value = $('#operatingMode').val();
	clearError('operatingMode_error');
	if (isEmpty(value)) {
		setError('operatingMode_error', MANDATORY);
		return false;
	} else if ($('#action').val() == ADD && !isValidCode(value)) {
		setError('operatingMode_error', INVALID_FORMAT);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('OPERATING_MODE', $('#operatingMode').val());
	validator.setValue(ACTION, $('#action').val());
	validator.setClass('patterns.config.web.forms.cmn.moperatingmodebean');
	validator.setMethod('validateoperatingMode');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('operatingMode_error', validator.getValue(ERROR));
		return false;
	}
	if ($('#action').val() == MODIFY) {
		PK_VALUE = getEntityCode() + PK_SEPERATOR + $('#operatingMode').val();
		if (!loadPKValues(PK_VALUE, 'operatingMode_error')) {
			return false;
		}
	}
	setFocusLast('description');
	return true;
}

function description_val() {
	var value = $('#description').val();
	clearError('description_error');
	if (isEmpty(value)) {
		setError('description_error', MANDATORY);
		return false;
	}
	if (!isValidDescription(value)) {
		setError('description_error', INVALID_DESCRIPTION);
		return false;
	}
	setFocusLast('conciseDescription');
	return true;
}

function conciseDescription_val() {
	var value = $('#conciseDescription').val();
	clearError('conciseDescription_error');
	if (isEmpty(value)) {
		setError('conciseDescription_error', MANDATORY);
		return false;
	}
	if (!isValidConciseDescription(value)) {
		setError('conciseDescription_error', INVALID_CONCISE_DESCRIPTION);
		return false;
	}
	setFocusLast('singleAccount');
	return true;
}

function singleAccount_val(valMode) {
	clearError('jointAccount_error');
	if ($('#singleAccount').prop('checked')) {
		setCheckbox('jointAccount', NO);
		if (valMode)
			setFocusLast('illtAccountAgent');
	} else {
		setCheckbox('jointAccount', YES);
		if (valMode)
			setFocusLast('jointAccount');
	}
	return true;
}

function jointAccount_val(valMode) {
	clearError('jointAccount_error');
	if ($('#jointAccount').prop('checked')) {
		setCheckbox('singleAccount', NO);
	} else {
		setCheckbox('singleAccount', YES);
	}
	if (valMode)
		setFocusLast('illtAccountAgent');
	return true;
}

function illtAccountAgent_val() {
	if ($('#action').val() == ADD) {
		setFocusLast('remarks');
	} else {
		setFocusLast('enabled');
	}
	return true;
}

function enabled_val() {
	if ($('#action').val() == MODIFY) {
		setFocusLast('remarks');
	}
	return true;
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', INVALID_REMARKS);
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', INVALID_REMARKS);
			return false;
		}
	}
	if ($('#action').val() == MODIFY) {
		if (isEmpty(value)) {
			setError('remarks_error', MANDATORY);
			return false;
		}
	}
	setFocusOnSubmit();
	return true;
}
