var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MBANKPROCCODES';
function init() {

	refreshTBAGrid();
	refreshQueryGrid();
	if (_redisplay) {
		if ($('#action').val() == ADD) {
			$('#enabled').prop('disabled', true);
			setCheckbox('enabled', YES);
		}
	}
}
function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'MBANKPROCCODES_MAIN', queryGrid);
}

function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}

function clearFields() {
	$('#bankingProcessCode').val(EMPTY_STRING);
	$('#bankingProcessCode_error').html(EMPTY_STRING);
	$('#bankingProcessCode_desc').html(EMPTY_STRING);
	clearNonPKFields();
}

function clearNonPKFields() {
	$('#name').val(EMPTY_STRING);
	$('#name_error').html(EMPTY_STRING);
	$('#shortName').val(EMPTY_STRING);
	$('#shortName_error').html(EMPTY_STRING);
	$('#bankGLToBeUsed').val(EMPTY_STRING);
	$('#bankGLToBeUsed_error').html(EMPTY_STRING);
	setCheckbox('enabled', YES);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
}
function add() {
	$('#bankingProcessCode').focus();
	setCheckbox('enabled', YES);
	$('#enabled').prop('disabled', true);
}
function modify() {
	$('#bankingProcessCode').focus();
	if ($('#action').val() == MODIFY) {
		$('#enabled').prop('disabled', false);
	} else {
		$('#enabled').prop('disabled', true);
	}
}

function change(id) {
	switch (id) {
	case 'bankingProcessCode':
		bankingProcessCode_val();
		break;

	}
}

function loadData() {
	$('#bankingProcessCode').val(validator.getValue('BANK_PROCESS_CODE'));
	$('#name').val(validator.getValue('NAME'));
	$('#shortName').val(validator.getValue('SHORT_NAME'));
	$('#bankGLToBeUsed').val(validator.getValue('GL_TOBE_USED'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
}
function view(source, primaryKey) {
	hideParent('cmn/qbankproccodes', source, primaryKey);
	resetLoading();

}

function doHelp(id) {

	switch (id) {
	case 'bankingProcessCode':
		help('COMMON', 'HLP_BANK_PROC_CODE', $('#bankingProcessCode').val(), EMPTY_STRING, $('#bankingProcessCode'));
		break;
	}
	switch (id) {
	case 'bankGLToBeUsed':
		help('COMMON', 'HLP_GL_HEAD_CODE', $('#bankGLToBeUsed').val(), EMPTY_STRING, $('#bankGLToBeUsed'));
		break;
	}
}

function doclearfields(id) {
	switch (id) {
	case 'bankingProcessCode':
		if (isEmpty($('#bankingProcessCode').val())) {
			clearFields();
			break;
		}
	}

}

function validate(id) {
	var valMode = true;
	switch (id) {
	case 'bankingProcessCode':
		bankingProcessCode_val();
		break;
	case 'name':
		name_val();
		break;
	case 'shortName':
		shortName_val();
		break;
	case 'bankGLToBeUsed':
		bankGLToBeUsed_val(valMode);
		break;
	case 'enabled':
		enabled_val();
		break;
	case 'remarks':
		remarks_val();
		break;
	}

}

function backtrack(id) {
	switch (id) {
	case 'bankingProcessCode':
		setFocusLast('bankingProcessCode');
		break;
	case 'name':
		setFocusLast('bankingProcessCode');
		break;
	case 'conciseDescription':
		setFocusLast('description');
		break;
	case 'shortName':
		setFocusLast('name');
		break;
	case 'bankGLToBeUsed':
		setFocusLast('shortName');
		break;
	case 'enabled':
		setFocusLast('bankGLToBeUsed');
		break;
	case 'remarks':
		if ($('#action').val() == MODIFY) {
			setFocusLast('enabled');
		} else {
			setFocusLast('bankGLToBeUsed');
		}
		break;
	}

}

function bankingProcessCode_val() {
	var value = $('#bankingProcessCode').val();
	clearError('bankingProcessCode_error');
	$('#bankingProcessCode_desc').html(EMPTY_STRING);
	if (isEmpty(value)) {
		setError('bankingProcessCode_error', MANDATORY);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('BANK_PROCESS_CODE', value);
		validator.setValue(ACTION, $('#action').val());
		validator.setClass('patterns.config.web.forms.cmn.mbankproccodesbean');
		validator.setMethod('validateBankingProcessCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('bankingProcessCode_error', validator.getValue(ERROR));
			$('#bankingProcessCode_desc').html(EMPTY_STRING);
			return false;
		} else {
			$('#bankingProcessCode_desc').html(validator.getValue('DESCRIPTION'));
			if ($('#action').val() == MODIFY) {
				PK_VALUE = getEntityCode() + PK_SEPERATOR + $('#bankingProcessCode').val();
				if (!loadPKValues(PK_VALUE, 'bankingProcessCode_error')) {
					return false;
				}
			}
		}
	}
	setFocusLast('name');
	return true;
}

function name_val() {
	var value = $('#name').val();
	clearError('name_error');
	if (isEmpty(value)) {
		setError('name_error', MANDATORY);
		return false;
	}
	if (!isValidDescription(value)) {
		setError('name_error', INVALID_DESCRIPTION);
		return false;
	}
	setFocusLast('shortName');
	return true;
}

function shortName_val() {
	var value = $('#shortName').val();
	clearError('shortName_error');
	if (isEmpty(value)) {
		setError('shortName_error', MANDATORY);
		return false;
	}
	if (!isValidShortName(value)) {
		setError('shortName_error', INVALID_SHORT_NAME);
		return false;
	}
	setFocusLast('bankGLToBeUsed');
	return true;
}
function bankGLToBeUsed_val() {
	var value = $('#bankGLToBeUsed').val();
	clearError('bankGLToBeUsed_error');
	$('#bankGLToBeUsed_desc').html(EMPTY_STRING);
	if (isEmpty(value)) {
		setError('bankGLToBeUsed_error', MANDATORY);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('GL_HEAD_CODE', value);
		validator.setValue(ACTION, $('#action').val());
		validator.setClass('patterns.config.web.forms.cmn.mbankproccodesbean');
		validator.setMethod('validateGLHeadCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('bankGLToBeUsed_error', validator.getValue(ERROR));
			$('#bankGLToBeUsed_desc').html(EMPTY_STRING);
			return false;
		} else {
			$('#bankGLToBeUsed_desc').html(validator.getValue('DESCRIPTION'));
		}
	}
	if ($('#action').val() == MODIFY) {
		setFocusLast('enabled');
	} else {
		setFocusLast('remarks');
	}
	return true;
}

function enabled_val() {
	if ($('#action').val() == MODIFY) {
		setFocusLast('remarks');
	}
}
function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', INVALID_REMARKS);
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', INVALID_REMARKS);
			return false;
		}
	}
	if ($('#action').val() == MODIFY) {
		if (isEmpty(value)) {
			setError('remarks_error', MANDATORY);
			return false;
		}
	}
	setFocusOnSubmit();
	return true;

}
