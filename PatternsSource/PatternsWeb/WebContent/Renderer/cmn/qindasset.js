var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MINDASSET';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
	pgmGrid.setColumnHidden(0, true);
}
function clearFields() {
	$('#leaseCode').val(EMPTY_STRING);
	$('#leaseCode_desc').html(EMPTY_STRING);
	$('#agreeNo').val(EMPTY_STRING);
	$('#agreeNo_desc').html(EMPTY_STRING);
	$('#scheduleId').val(EMPTY_STRING);
	$('#scheduleId_desc').html(EMPTY_STRING);
	$('#assetSl').val(EMPTY_STRING);
	$('#assetSl_desc').html(EMPTY_STRING);
	$('#compSl').val(EMPTY_STRING);
	$('#compSl_desc').html(EMPTY_STRING);
	$('#qtySl').val(EMPTY_STRING);
	$('#qtySl_desc').html(EMPTY_STRING);
	$('#qtyOfAsset').val(EMPTY_STRING);
	clearNonPKFields();
}

function clearNonPKFields() {
	$('#indAssetId').val(EMPTY_STRING);
	pgmGrid.clearAll();
	mindasset_clearGridFields();
	setCheckbox('enabled', YES);
	$('#remarks').val(EMPTY_STRING);
}

function mindasset_clearGridFields() {
	$('#fieldId').val(EMPTY_STRING);
	$('#fieldId_desc').html(EMPTY_STRING);
	$('#fieldvalue').removeClass('hidden');
	$('#fieldremarks').addClass('hidden');
	$('#fieldValue').val(EMPTY_STRING);
	$('#fieldRemarks').val(EMPTY_STRING);
}

function loadGrid() {
	loadGridQuery(CURRENT_PROGRAM_ID, 'MINDASSET_GRID', pgmGrid);
}
function loadData() {
	$('#leaseCode').val(validator.getValue('LESSEE_CODE'));
	$('#leaseCode_desc').html(validator.getValue('F2_CUSTOMER_NAME'));
	$('#custIDHidden').val(validator.getValue('F1_CUSTOMER_ID'));
	$('#agreeNo').val(validator.getValue('AGREEMENT_NO'));
	$('#scheduleId').val(validator.getValue('SCHEDULE_ID'));
	$('#assetSl').val(validator.getValue('ASSET_SL'));
	$('#compSl').val(validator.getValue('ASSET_COMP_SL'));
	$('#qtySl').val(validator.getValue('ASSET_QTY_SL'));
	$('#qtyOfAsset').val(validator.getValue('F3_ASSET_QTY'));
	$('#indAssetId').val(validator.getValue('INDIVIDUAL_ASSET_ID'));
	loadGrid();
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadAuditFields(validator);
}
