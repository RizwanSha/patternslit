var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MEMPBRN';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}

function clearFields() {
	$('#employeeCode').val(EMPTY_STRING);
	$('#employeeCode_desc').html(EMPTY_STRING);
	$('#effectiveDate').val(EMPTY_STRING);
	$('#newOfficeCode').val(EMPTY_STRING);
	$('#newOfficeCode_desc').html(EMPTY_STRING);
	setCheckbox('enabled', NO);
	$('#remarks').val(EMPTY_STRING);
}

function loadData() {
	$('#employeeCode').val(validator.getValue('EMP_CODE'));
	$('#employeeCode_desc').html(validator.getValue('F1_PERSON_NAME'));
	$('#effectiveDate').val(validator.getValue('EFF_DATE'));
	$('#newOfficeCode').val(validator.getValue('BRANCH_CODE'));
	$('#newOfficeCode_desc').html(validator.getValue('F2_DESCRIPTION'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadAuditFields(validator);
}

