<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.cmn.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/cmn/qschpayacparam.js" />
	</web:dependencies>
	<web:loadPK />
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="qschpayacparam.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:viewContent id="po_view1">
				<web:dividerBlock>
					<web:sectionBlock width="width-100p" align="left">
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="180px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="ischpayacparam.schemecode" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:codeDisplay property="schemeCode" id="schemeCode" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="form.effectivedate" var="common" mandatory="true" />
									</web:column>
									<web:column>
										<type:dateDisplay property="effectiveDate" id="effectiveDate" />
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="180px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="ischpayacparam.bulkpayfund" var="program" />
									</web:column>
									<web:column>
										<type:checkboxDisplay property="bulkPayFund" id="bulkPayFund" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="ischpayacparam.capfile" var="program" />
									</web:column>
									<web:column>
										<type:checkboxDisplay property="capFile" id="capFile" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="ischpayacparam.debitamusour" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:comboDisplay property="debitAmuSour" id="debitAmuSour" datasourceid="ISCHPAYACPARAM_DB_AMT_SCOURCE" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="ischpayacparam.debitgl" var="program" />
									</web:column>
									<web:column>
										<type:codeDisplay property="debitGl" id="debitGl" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="ischpayacparam.debitaccount" var="program" />
									</web:column>
									<web:column>
										<type:codeDisplay property="debitAccount" id="debitAccount" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="ischpayacparam.creditgl" var="program" />
									</web:column>
									<web:column>
										<type:codeDisplay property="creditGl" id="creditGl" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="ischpayacparam.glhead" var="program" />
									</web:column>
									<web:column>
										<type:codeDisplay property="glHead" id="glHead" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="form.enabled" var="common" />
									</web:column>
									<web:column>
										<type:checkboxDisplay property="enabled" id="enabled" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="form.remarks" var="common" />
									</web:column>
									<web:column>
										<type:remarksDisplay property="remarks" id="remarks" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:auditDisplay />
					</web:sectionBlock>
				</web:dividerBlock>
			</web:viewContent>
		</web:viewSubPart>
	</web:viewPart>
</web:fragment>
