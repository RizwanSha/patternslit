<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="resource" tagdir="/WEB-INF/tags/resource"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.cmn.Messages"
	var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/cmn/mgeoregions.js" />
	</web:dependencies>
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="mgeoregions.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/cmn/mgeoregions" id="mgeoregions"
				method="POST">
				<web:dividerBlock>
					<type:optionBox add="true" modify="true" view="true" />
				</web:dividerBlock>
				<web:viewContent id="po_view1">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="mgeoregions.georegioncode" var="program"
												mandatory="true" />
										</web:column>
										<web:column>
											<type:geoRegionCode property="geoRegionCode"
												id="geoRegionCode" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="form.description" var="common"
												mandatory="true" />
										</web:column>
										<web:column>
											<type:description property="description" id="description" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							
							
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="700px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<td style="vertical-align: top"><web:section>
												<web:table>
													<web:columnGroup>
														<web:columnStyle width="180px" />
														<web:columnStyle />
													</web:columnGroup>
													<web:rowEven>
														<web:column>
															<web:legend key="form.conciseDescription" var="common"
																mandatory="true" />
														</web:column>
														<web:column>
															<type:conciseDescription property="conciseDescription"
																id="conciseDescription" />
														</web:column>
													</web:rowEven>
													<web:rowOdd>
														<web:column>
															<web:legend key="mgeoregions.pincode" var="program"
																mandatory="true" />
														</web:column>
														<web:column>
															<type:geographicalUnitId property="pinCode" id="pinCode" />
														</web:column>
													</web:rowOdd>
													<web:rowEven>
														<web:column>
															<web:legend key="mgeoregions.officecode" var="program"
																mandatory="true" />
														</web:column>
														<web:column>
															<type:branch property="officeCode" id="officeCode" />
														</web:column>
													</web:rowEven>
													<web:rowOdd>
														<web:column>
															<web:legend key="form.enabled" var="common" />
														</web:column>
														<web:column>
															<type:checkbox property="enabled" id="enabled" />
														</web:column>
													</web:rowOdd>
													<web:rowEven>
														<web:column>
															<web:legend key="form.remarks" var="common" />
														</web:column>
														<web:column>
															<type:remarks property="remarks" id="remarks" />
														</web:column>
													</web:rowEven>
												</web:table>
											</web:section>
										<td>
											<web:column>
												<div id="map" style="height: 350px; width: 500px"></div>
											</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:rowOdd>
										<web:column span="2">
											<web:submitReset />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
						</web:sectionBlock>
					</web:dividerBlock>
				</web:viewContent>
				<web:element property="command" />
				<web:element property="action" />
				<web:element property="mapData" />
				<web:element property="mapInvNo" />
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
	<web:queryGrid />
	<web:TBAGrid />
	<web:contextSearch />
	<web:dependencies>
		<script
			src="https://maps.googleapis.com/maps/api/js?key=AIzaSyByyFg6IWJ3Y7tTASIG7pzXyEGYTmdrJRk&libraries=drawing&callback=mapObject.initMap"
			async defer></script>
	</web:dependencies>
</web:fragment>
