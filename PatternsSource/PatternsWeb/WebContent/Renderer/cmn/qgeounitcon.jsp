<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.cmn.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/cmn/qgeounitcon.js" />
	</web:dependencies>
	<web:loadPK />
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="qgeounitcon.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:viewContent id="po_view1">
				<web:dividerBlock>
					<web:sectionBlock width="width-100p" align="left">
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="mgeounitcon.countrycode" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:countryCodeDisplay property="countryCode" id="countryCode" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="mgeounitcon.geoUnitid" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:geographicalUnitIdDisplay property="geoUnitID" id="geoUnitID" />
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="mgeounitcon.name" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:descriptionDisplay property="name" id="name" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="mgeounitcon.shortname" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:conciseDescriptionDisplay property="shortName" id="shortName" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="mgeounitcon.parentgeounitid" var="program" />
									</web:column>
									<web:column>
										<type:geographicalUnitIdDisplay property="parentgeoUnitID" id="parentgeoUnitID" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="mgeounitcon.geostructure" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:geographicalUnitStructureCodeDisplay property="geoStructure" id="geoStructure" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="mgeounitcon.parentgeostructure" var="program" />
									</web:column>
									<web:column>
										<type:geographicalUnitStructureCodeDisplay property="parentGeoStructure" id="parentGeoStructure" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="mgeounitcon.geounittype" var="program" />
									</web:column>
									<web:column>
										<type:geographicalUnitTypeDisplay property="geoUnitType" id="geoUnitType" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="form.IsUseEnabled" var="common" />
									</web:column>
									<web:column>
										<type:checkboxDisplay property="enabled" id="enabled" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="form.remarks" var="common" />
									</web:column>
									<web:column>
										<type:remarksDisplay property="remarks" id="remarks" />
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
						<web:auditDisplay />
					</web:sectionBlock>
				</web:dividerBlock>
			</web:viewContent>
		</web:viewSubPart>
	</web:viewPart>
</web:fragment>