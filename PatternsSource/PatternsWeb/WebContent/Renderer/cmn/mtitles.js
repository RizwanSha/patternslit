var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MTITLES';
function init() {
	refreshTBAGrid();
	refreshQueryGrid();
	if (_redisplay) {
		titleDeceasedUsage_chk();
		if  (!$('#titleDeceasedUsage').is(':checked')) {
			titleOccupation_val();
			}
		if ($('#action').val() == ADD) {
			$('#enabled').prop('disabled', true);
			setCheckbox('enabled', YES);
		}
	}
}

function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'MTITLES_MAIN', queryGrid);
}

function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}

function clearFields() {
	$('#titleCode').val(EMPTY_STRING);
	$('#titleCode_desc').html(EMPTY_STRING);
	$('#titleCode_error').html(EMPTY_STRING);
	clearNonPKFields();
}
function clearNonPKFields() {
	$('#extDescription').val(EMPTY_STRING);
	$('#extDescription_error').html(EMPTY_STRING);
	$('#intDescription').val(EMPTY_STRING);
	$('#intDescription_error').html(EMPTY_STRING);
	$('#shortDescription').val(EMPTY_STRING);
	$('#shortDescription_error').html(EMPTY_STRING);
	$('#titleUsage').prop('selectedIndex' , 0);
	setCheckbox('titleChildrenUsage', NO);
	setCheckbox('titleDeceasedUsage', NO);
	$('#titleOccupation').val(EMPTY_STRING);
	$('#titleOccupation_error').html(EMPTY_STRING);
	$('#titleOccupation_desc').html(EMPTY_STRING);
	$('#titleOccupation').prop('readOnly', false);
	$('#titleOccupation_pic').prop('disabled', false);
	$('#otherOccupation').val(EMPTY_STRING);
	$('#otherOccupation').prop('readOnly', false);
	$('#otherOccupation_error').html(EMPTY_STRING);
	$('#defMaritalStatus').val(EMPTY_STRING);
	$('#defMaritalStatus_desc').html(EMPTY_STRING);
	$('#defMaritalStatus_error').html(EMPTY_STRING);
	setCheckbox('fixMaritalStatus', NO);
	setCheckbox('enabled', YES);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
}

function add() {
	$('#titleCode').focus();
	$('#titleUsage').prop('selectedIndex' , 0);
	setCheckbox('enabled', YES);
	$('#enabled').prop('disabled', true);
	setCheckbox('fixMaritalStatus', NO);
	$('#fixMaritalStatus').prop('disabled', true);
}

function doclearfields(id) {
	switch (id) {
	case 'titleCode':
		if (isEmpty($('#titleCode').val())) {
			$('#titleCode_error').html(EMPTY_STRING);
			$('#titleCode_desc').html(EMPTY_STRING);
			clearNonPKFields();
			break;
		}
	}
}

function modify() {
	$('#titleCode').focus();
	setCheckbox('fixMaritalStatus', NO);
	$('#fixMaritalStatus').prop('disabled', true);
	if ($('#action').val() == MODIFY) {
		$('#enabled').prop('disabled', false);
	} else {
		$('#enabled').prop('disabled', true);
	}
}

function doHelp(id) {
	switch (id) {
	case 'titleCode':
		help('COMMON', 'HLP_TITLE_M', $('#titleCode').val(), EMPTY_STRING, $('#titleCode'));
		break;
	case 'titleOccupation':
		help('COMMON', 'HLP_OCCN_M', $('#titleOccupation').val(), EMPTY_STRING, $('#titleOccupation'));
		break;
	case 'defMaritalStatus':
		help('COMMON', 'HLP_MARITALSTATUS_M', $('#defMaritalStatus').val(), EMPTY_STRING, $('#defMaritalStatus'));
		break;
	}
}

function loadData() {
	{
		$('#titleCode').val(validator.getValue('TITLE_CODE'));
		$('#titleCode_desc').html(validator.getValue('INT_DESCRIPTION'));
		$('#extDescription').val(validator.getValue('EXT_DESCRIPTION'));
		$('#intDescription').val(validator.getValue('INT_DESCRIPTION'));
		$('#shortDescription').val(validator.getValue('CONCISE_DESCRIPTION'));
		$('#titleUsage').val(validator.getValue('TITLE_USAGE'));
		setCheckbox('titleChildrenUsage', validator.getValue('TITLE_CHILDREN_USG'));
		setCheckbox('titleDeceasedUsage', validator.getValue('TITLE_DECEASED_USG'));
		var titleDeceasedUsage = validator.getValue('TITLE_DECEASED_USG');
		if (titleDeceasedUsage ==COLUMN_ENABLE ) {
			$('#titleOccupation').val(EMPTY_STRING);
			$('#titleOccupation_desc').html(EMPTY_STRING);
			$('#titleOccupation').prop('readOnly', true);
			$('#titleOccupation_pic').prop('disabled', true);
			$('#otherOccupation').val(EMPTY_STRING);
			$('#otherOccupation').prop('readOnly', true);
		} else if (titleDeceasedUsage == COLUMN_DISABLE) {
			$('#titleOccupation').prop('readOnly', false);
			$('#titleOccupation_pic').prop('disabled', false);
			$('#titleOccupation').val(validator.getValue('TITLE_OCCU'));
			$('#titleOccupation_desc').html(validator.getValue('F1_DESCRIPTION'));
			$('#otherOccupation').val(EMPTY_STRING);
			$('#otherOccupation').prop('readOnly', false);
		}
		if (titleDeceasedUsage == COLUMN_DISABLE) {
		titleOccupation_val();
		}
		$('#otherOccupation').val(validator.getValue('TITLE_OTH_OCCU'));
		$('#defMaritalStatus').val(validator.getValue('DEFAULT_MARITAL_STATUS'));
		if (isEmpty($('#defMaritalStatus').val())) {
			$('#fixMaritalStatus').prop('disabled', true);
		} else {
			$('#defMaritalStatus_desc').html(validator.getValue('F2_DESCRIPTION'));
			$('#fixMaritalStatus').prop('disabled', false);
		}
		setCheckbox('fixMaritalStatus', validator.getValue('MARITAL_STATUS_FIXED'));
		setCheckbox('enabled', validator.getValue('ENABLED'));
		$('#remarks').val(validator.getValue('REMARKS'));
		$('#titleCode').focus();
		resetLoading();
	}
}

function view(source, primaryKey) {
	hideParent('cmn/qtitles', source, primaryKey);
	resetLoading();
}

function validate(id) {
	switch (id) {
	case 'titleCode':
		titleCode_val();
		break;
	case 'extDescription':
		extDescription_val();
		break;
	case 'intDescription':
		intDescription_val();
		break;
	case 'shortDescription':
		shortDescription_val();
		break;
	case 'titleUsage':
		titleUsage_val();
		break;
	case 'titleChildrenUsage':
		titleChildrenUsage_val();
		break;
	case 'titleDeceasedUsage':
		titleDeceasedUsage_val();
		break;
	case 'titleOccupation':
		titleOccupation_val();
		break;
	case 'otherOccupation':
		otherOccupation_val();
		break;
	case 'defMaritalStatus':
		defMaritalStatus_val();
		break;
	case 'fixMaritalStatus':
		fixMaritalStatus_val();
		break;
	case 'enabled':
		enabled_val();
		break;
	case 'remarks':
		remarks_val();
		break;
	}
}

function backtrack(id) {
	switch (id) {
	case 'titleCode':
		setFocusLast('titleCode');
		break;
	case 'extDescription':
		setFocusLast('titleCode');
		break;
	case 'intDescription':
		setFocusLast('extDescription');
		break;
	case 'shortDescription':
		setFocusLast('intDescription');
		break;
	case 'titleUsage':
		setFocusLast('shortDescription');
		break;
	case 'titleChildrenUsage':
		setFocusLast('titleUsage');
		break;
	case 'titleDeceasedUsage':
		setFocusLast('titleChildrenUsage');
		break;
	case 'titleOccupation':
		setFocusLast('titleDeceasedUsage');
		break;
	case 'otherOccupation':
		setFocusLast('titleOccupation');
		break;
	case 'defMaritalStatus':
		if ($('#titleDeceasedUsage').is(':checked')) {
			setFocusLast('titleDeceasedUsage');
		} else {
				if ($('#otherOccupation').is('[readonly]')) {
				setFocusLast('titleOccupation');
			} else {
				setFocusLast('otherOccupation');
			}
		}
		break;

	case 'fixMaritalStatus':
		setFocusLast('defMaritalStatus');
		break;
	case 'enabled':
		if ($('#fixMaritalStatus').is('[disabled]')) {
			setFocusLast('defMaritalStatus');
		} else {
			setFocusLast('fixMaritalStatus');
		}
		break;
	case 'remarks':
		if ($('#action').val() == MODIFY) {
			setFocusLast('enabled');
		} else {
			if ($('#fixMaritalStatus').is('[disabled]')) {
				setFocusLast('defMaritalStatus');
			} else {
				setFocusLast('fixMaritalStatus');
			}
		}
		break;
	}
}

function checkclick(id) {
	switch (id) {
	case 'titleDeceasedUsage':
		titleDeceasedUsage_chk();
		break;
	}
}

function titleDeceasedUsage_chk() {
	var titleDeceasedUsage = $('#titleDeceasedUsage').is(':checked');
	if (titleDeceasedUsage) {
		$('#titleOccupation').val(EMPTY_STRING);
		$('#titleOccupation_error').html(EMPTY_STRING);
		$('#titleOccupation_desc').html(EMPTY_STRING);
		$('#titleOccupation').prop('readOnly', true);
		$('#titleOccupation_pic').prop('disabled', true);
		$('#otherOccupation').val(EMPTY_STRING);
		$('#otherOccupation').prop('readOnly', true);

	} else {
		$('#titleOccupation').prop('readOnly', false);
		$('#titleOccupation_pic').prop('disabled', false);
		$('#otherOccupation').prop('readOnly', false);

	}
}

function enabled_val() {
	if ($('#action').val() == MODIFY) {
		setFocusLast('remarks');
	}
}
function titleChildrenUsage_val() {
	setFocusLast('titleDeceasedUsage');
}

function titleCode_val() {
	var value = $('#titleCode').val();
	clearError('titleCode_error');
	if (isEmpty(value)) {
		setError('titleCode_error', HMS_MANDATORY);
		setFocusLast('titleCode');
		return false;
	} else if ($('#action').val() == ADD && !isValidCode(value)) {
		setError('titleCode_error', HMS_INVALID_FORMAT);
		setFocusLast('titleCode');
		return false;
	} else {
		if ($('#action').val() == ADD) {
			validator.clearMap();
			validator.setMtm(false);
			validator.setValue('TITLE_CODE', $('#titleCode').val());
			validator.setValue(ACTION, $('#action').val());
			validator.setClass('patterns.config.web.forms.cmn.mtitlesbean');
			validator.setMethod('validateTitleCode');
			validator.sendAndReceive();
			if (validator.getValue(ERROR) != EMPTY_STRING) {
				setError('titleCode_error', validator.getValue(ERROR));
				setFocusLast('titleCode');
				return false;
			}
		} else {
			PK_VALUE = getEntityCode() + PK_SEPERATOR + $('#titleCode').val();
			if (!loadPKValues(PK_VALUE, 'titleCode_error')) {
				return false;
			}
		}
	}
	setFocusLast('extDescription');
	return true;
}

function extDescription_val() {
	var value = $('#extDescription').val();
	clearError('extDescription_error');
	if (isEmpty(value)) {
		setError('extDescription_error', MANDATORY);
		setFocusLast('extDescription');
		return false;
	}
	if (!isValidDescription(value)) {
		setError('extDescription_error', HMS_INVALID_DESCRIPTION);
		setFocusLast('extDescription');
		return false;
	}
	setFocusLast('intDescription');
	return true;
}

function intDescription_val() {
	var value = $('#intDescription').val();
	clearError('intDescription_error');
	if (isEmpty(value)) {
		setError('intDescription_error', MANDATORY);
		setFocusLast('intDescription');
		return false;
	}
	if (!isValidDescription(value)) {
		setError('intDescription_error', HMS_INVALID_DESCRIPTION);
		setFocusLast('intDescription');
		return false;
	}
	setFocusLast('shortDescription');
	return true;
}

function shortDescription_val() {
	var value = $('#shortDescription').val();
	clearError('shortDescription_error');
	if (isEmpty(value)) {
		setError('shortDescription_error', HMS_MANDATORY);
		setFocusLast('shortDescription');
		return false;
	}
	if (!isValidConciseDescription(value)) {
		setError('shortDescription_error', HMS_INVALID_CONCISE_DESCRIPTION);
		setFocusLast('shortDescription');
		return false;
	}
	setFocusLast('titleUsage');
	return true;
}

function titleUsage_val() {
	var value = $('#titleUsage').val();
	clearError('titleUsage_error');
	if (!isEmpty(value)) {
		setFocusLast('titleChildrenUsage');
		return true;
	}
}

function titleDeceasedUsage_val() {
	var titleDeceasedUsage = $('#titleDeceasedUsage').is(':checked');
	clearError('titleDeceasedUsage_error');
	if (titleDeceasedUsage) {
		$('#titleOccupation').val(EMPTY_STRING);
		$('#titleOccupation_desc').html(EMPTY_STRING);
		$('#titleOccupation_error').html(EMPTY_STRING);
		$('#titleOccupation').prop('readOnly', true);
		$('#titleOccupation_pic').prop('disabled', true);
		$('#otherOccupation').val(EMPTY_STRING);
		$('#otherOccupation').prop('readOnly', true);
		setFocusLast('defMaritalStatus');
	} else {
		$('#titleOccupation').prop('readOnly', false);
		$('#titleOccupation_pic').prop('disabled', false);
		$('#otherOccupation').prop('readOnly', false);
		setFocusLast('titleOccupation');
	}

	return true;
}

function titleOccupation_val() {
	var value = $('#titleOccupation').val();
	clearError('titleOccupation_error');
	$('#titleOccupation_desc').html(EMPTY_STRING);
	if (isEmpty(value)) {
		clearError('otherOccupation_error');
		$('#otherOccupation').prop('readOnly', false);
		setFocusLast('otherOccupation');
	}
	if (!isEmpty(value)) {
		{
			validator.clearMap();
			validator.setMtm(false);
			validator.setValue('OCCU_CODE', $('#titleOccupation').val());
			validator.setValue(ACTION, USAGE);
			validator.setClass('patterns.config.web.forms.cmn.mtitlesbean');
			validator.setMethod('validateOccupationCode');
			validator.sendAndReceive();
			if (validator.getValue(ERROR) != EMPTY_STRING) {
				setError('titleOccupation_error', validator.getValue(ERROR));
				return false;
			}

			$('#titleOccupation_desc').html(validator.getValue('DESCRIPTION'));
			var titleOccupation = validator.getValue('OCCU_FOR_OTHER');
			if (titleOccupation == COLUMN_DISABLE) {
				$('#otherOccupation').val(EMPTY_STRING);
				$('#otherOccupation').prop('readOnly', true);
				setFocusLast('defMaritalStatus');
			} else if (titleOccupation == COLUMN_ENABLE) {
				$('#otherOccupation').prop('readOnly', false);
				setFocusLast('otherOccupation');
			}
		}
	}
	return true;
}

function otherOccupation_val() {
	var value = $('#otherOccupation').val();
	clearError('otherOccupation_error');
	if (!isEmpty(value)) {
		if (!isValidDescription(value)) {
			setError('otherOccupation_error', HMS_INVALID_DESCRIPTION);
			return false;
		}
	}
	setFocusLast('defMaritalStatus');
	return true;
}

function defMaritalStatus_val() {
	var value = $('#defMaritalStatus').val();
	clearError('defMaritalStatus_error');
	if (isEmpty(value)) {
		$('#fixMaritalStatus').prop('disabled', true);
		setCheckbox('fixMaritalStatus', NO);
		clearError('fixMaritalStatus_error');
		if ($('#action').val() == ADD)
			setFocusLast('remarks');
		else
			setFocusLast('enabled');
	}
	if (!isEmpty(value)) {
		{
			$('#fixMaritalStatus').prop('disabled', false);
			validator.clearMap();
			validator.setMtm(false);
			validator.setValue('MARITAL_STATUS_CODE', value);
			validator.setValue(ACTION, USAGE);
			validator.setClass('patterns.config.web.forms.cmn.mtitlesbean');
			validator.setMethod('validateMaritalStatusCode');
			validator.sendAndReceive();
			if (validator.getValue(ERROR) != EMPTY_STRING) {
				setError('defMaritalStatus_error', validator.getValue(ERROR));
				return false;
			}
			$('#defMaritalStatus_desc').html(validator.getValue('DESCRIPTION'));
			setFocusLast('fixMaritalStatus');
		}
	}
	return true;
}

function fixMaritalStatus_val() {
	if (isEmpty($('#defMaritalStatus').val()) && $('#fixMaritalStatus').prop('checked', true)) {
		setError('fixMaritalStatus_error', HMS_VALUE_NT_ALLWD);
		return false;
	}
	if ($('#action').val() == ADD) {
		setFocusLast('remarks');
	} else {
		setFocusLast('enabled');
	}
	return true;
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if ($('#action').val() == ADD && value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', HMS_MANDATORY);
			setFocusLast('remarks');
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', HMS_INVALID_REMARKS);
			setFocusLast('remarks');
			return false;
		}
	} else if ($('#action').val() == MODIFY) {
		if (isEmpty(value)) {
			setError('remarks_error', HMS_MANDATORY);
			setFocusLast('remarks');
			return false;
		} else if (!isValidRemarks(value)) {
			setError('remarks_error', HMS_INVALID_REMARKS);
			setFocusLast('remarks');
			return false;
		}
	}
	setFocusOnSubmit();
	return true;
}
