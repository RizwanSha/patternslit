<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.cmn.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/cmn/morgbusinesscorr.js" />
	</web:dependencies>
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="morgbusinesscorr.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/cmn/morgbusinesscorr" id="morgbusinesscorr" method="POST">
				<web:dividerBlock>
					<type:optionBox add="true" modify="true" view="true" />
				</web:dividerBlock>
				<web:viewContent id="po_view1" styleClass="hidden">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="morgbusinesscorr.organization" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:organization property="organizationCode" id="organizationCode" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="form.effectivedate" var="common" mandatory="true" />
										</web:column>
										<web:column>
											<type:date property="effectiveDate" id="effectiveDate" lookup="true" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="morgbusinesscorr.enablebusinescorr" var="program" />
										</web:column>
										<web:column>
											<type:checkbox property="enableBusinesCorr" id="enableBusinesCorr" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:viewTitle var="program" key="morgbusinesscorr.coverage" />
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="morgbusinesscorr.geostatecode" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:geographicalUnitId property="geoStateCode" id="geoStateCode" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="morgbusinesscorr.geodistcode" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:geographicalUnitId property="geoDistCode" id="geoDistCode" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column span="2">
											<web:gridToolbar gridName="orgBusinessCorrGrid" editFunction="orgBusinessCorrGridEditGrid" addFunction="orgBusinessCorrGridAddGrid" cancelNotReq="true" insertAtFirsstNotReq="true" />
											<web:element property="xmlOrgBusinessCorrGrid" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column span="2">
											<web:viewContent id="po_view4">
												<web:grid height="200px" width="670px" id="orgBusinessCorrGrid" src="cmn/morgbusinesscorr_innerGrid.xml">
												</web:grid>
											</web:viewContent>
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="form.IsUseEnabled" var="common" />
										</web:column>
										<web:column>
											<type:checkbox property="enabled" id="enabled" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="form.remarks" var="common" />
										</web:column>
										<web:column>
											<type:remarks property="remarks" id="remarks" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column span="2">
											<web:submitReset />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
						</web:sectionBlock>
					</web:dividerBlock>
				</web:viewContent>
				<web:element property="command" />
				<web:element property="action" />
				<web:element property="countryCode" />
				<web:element property="geoUnitStructStateCodeHid" />
				<web:element property="geoUnitStructDistCodeHid" />
				<web:element property="redirectLandingPagePath" />
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
	<web:queryGrid />
	<web:TBAGrid />
	<web:contextSearch />
</web:fragment>
