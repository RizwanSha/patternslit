var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MREGION';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}

function clearFields() {
	$('#regionCode').val(EMPTY_STRING);
	$('#description').val(EMPTY_STRING);
	$('#conciseDescription').val(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
}

function loadData() {
	$('#regionCode').val(validator.getValue('REGION_CODE'));
	$('#description').val(validator.getValue('DESCRIPTION'));
	$('#conciseDescription').val(validator.getValue('CONCISE_DESCRIPTION'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadAuditFields(validator);
}