<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.cmn.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/cmn/massettype.js" />
	</web:dependencies>
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="massettype.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/cmn/massettype" id="massettype" method="POST">
				<web:dividerBlock>
					<type:optionBox add="true" modify="true" view="true" />
				</web:dividerBlock>
				<web:viewContent id="po_view1" styleClass="hidden">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="massettype.assettypecode" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:assetTypeCode property="assetTypeCode" id="assetTypeCode" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
						</web:sectionBlock>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowOdd>
									<web:column>
										<web:legend key="form.description" var="common" mandatory="true" />
									</web:column>
									<web:column>
										<type:description property="description" id="description" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="form.conciseDescription" var="common" mandatory="true" />
									</web:column>
									<web:column>
										<type:conciseDescription property="conciseDescription" id="conciseDescription" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="massettype.carnoncar" var="program" />
									</web:column>
									<web:column>
										<type:combo property="carNonCar" id="carNonCar" datasourceid="COMMON_CAR_NONCAR" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="massettype.assetmodelnumber" var="program" />
									</web:column>
									<web:column>
										<type:combo property="assetModelNumber" id="assetModelNumber" datasourceid="COMMON_FIELD_MNO" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="massettype.assetconfiguration" var="program" />
									</web:column>
									<web:column>
										<type:combo property="assetConfig" id="assetConfig" datasourceid="COMMON_FIELD_MNO" />
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="form.enabled" var="common" />
									</web:column>
									<web:column>
										<type:checkbox property="enabled" id="enabled" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="form.remarks" var="common" />
									</web:column>
									<web:column>
										<type:remarks property="remarks" id="remarks" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column span="2">
										<web:submitReset />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
					</web:dividerBlock>
				</web:viewContent>
				<web:element property="command" />
				<web:element property="action" />
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
	<web:queryGrid />
	<web:TBAGrid />
	<web:contextSearch />
</web:fragment>
