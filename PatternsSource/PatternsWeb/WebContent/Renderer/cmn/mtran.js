var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MTRAN';

function init() {
	refreshTBAGrid();
	refreshQueryGrid();
	if (_redisplay) {
		if ($('#action').val() == ADD) {
			$('#enabled').prop('disabled', true);
			setCheckbox('enabled', YES);
		}
		redisplayCheckBoxValues();
		if ($('#slipNumberRequired_error').html().trim() != EMPTY_STRING) {
			$('#slipNumberRequired').prop('disabled', false);
		}

	}
}

function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'MTRAN_MAIN');
}

function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}

function add() {
	$('#transactionCode').focus();
	$('#enabled').prop('disabled', true);
	setCheckbox('enabled', YES);
}

function modify() {
	$('#transactionCode').focus();
	if ($('#action').val() == MODIFY) {
		$('#enabled').prop('disabled', false);
	} else {
		$('#enabled').prop('disabled', true);
	}
}

function view(source, primaryKey) {
	hideParent('cmn/qtran', source, primaryKey);
	resetLoading();
}

function clearFields() {
	$('#transactionCode').val(EMPTY_STRING);
	$('#transactionCode_desc').html(EMPTY_STRING);
	$('#transactionCode_error').html(EMPTY_STRING);
	clearNonPKFields();
}

function clearNonPKFields() {
	$('#description').val(EMPTY_STRING);
	$('#description_error').html(EMPTY_STRING);
	$('#conciseDescription').val(EMPTY_STRING);
	$('#conciseDescription_error').html(EMPTY_STRING);

	$('#alphaAccessCode').val(EMPTY_STRING);
	$('#alphaAccessCode_error').html(EMPTY_STRING);
	$('#transactionMode').val($("#transactionMode option:first-child").val());
	$('#transactionMode_error').html(EMPTY_STRING);
	$('#depositWithdrawal').val($("#depositWithdrawal option:first-child").val());
	$('#depositWithdrawal_error').html(EMPTY_STRING);
	$('#transactionUsingDocument').val($("#transactionUsingDocument option:first-child").val());
	$('#transactionUsingDocument_error').html(EMPTY_STRING);
	$('#slipNumberRequired').prop('disabled', true);
	setCheckbox('slipNumberRequired', NO);
	$('#slipNumberRequired_error').html(EMPTY_STRING);
	$('#numberOfLegsInTheTransaction').val($("#numberOfLegsInTheTransaction option:first-child").val());
	$('#numberOfLegsInTheTransaction_error').html(EMPTY_STRING);
	setCheckbox('enabled', YES);
	$('#enabled_error').html(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
}
function doclearfields(id) {
	switch (id) {
	case 'transactionCode':
		if (isEmpty($('#transactionCode').val())) {
			clearFields();
			break;
		}
	}
}
function loadData() {
	$('#transactionCode').val(validator.getValue('TRAN_CODE'));
	$('#description').val(validator.getValue('DESCRIPTION'));
	$('#conciseDescription').val(validator.getValue('CONCISE_DESCRIPTION'));
	$('#alphaAccessCode').val(validator.getValue('TRAN_ACCESS_CODE'));
	$('#transactionMode').val(validator.getValue('TRAN_MODE'));
	$('#depositWithdrawal').val(validator.getValue('TRAN_TYPE'));
	$('#transactionUsingDocument').val(validator.getValue('TRAN_DOC'));
	setCheckbox('slipNumberRequired', validator.getValue('SLIP_NUM_REQUIRED'));
	$('#numberOfLegsInTheTransaction').val(validator.getValue('TRAN_LEGS'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	makeslipNumberRequiredReadOnly();
	resetLoading();
}

function doHelp(id) {
	switch (id) {
	case 'transactionCode':
		help('COMMON', 'HLP_TRAN_CODE_M', $('#transactionCode').val(), EMPTY_STRING, $('#transactionCode'), function(gridObj, rowID) {
		});
		break;
	}
}

function validate(id) {
	switch (id) {
	case 'transactionCode':
		transactionCode_val();
		break;
	case 'description':
		description_val();
		break;
	case 'conciseDescription':
		conciseDescription_val();
		break;
	case 'alphaAccessCode':
		alphaAccessCode_val();
		break;
	case 'transactionMode':
		transactionMode_val();
		break;
	case 'depositWithdrawal':
		depositWithdrawal_val();
		break;
	case 'transactionUsingDocument':
		transactionUsingDocument_val();
		break;
	case 'slipNumberRequired':
		slipNumberRequired_val();
		break;
	case 'numberOfLegsInTheTransaction':
		numberOfLegsInTheTransaction_val();
		break;
	case 'enabled':
		enabled_val(valMode);
		break;
	case 'remarks':
		remarks_val(valMode);
		break;

	}
}

function backtrack(id) {
	switch (id) {
	case 'transactionCode':
		setFocusLast('transactionCode');
		break;
	case 'description':
		setFocusLast('transactionCode');
		break;
	case 'conciseDescription':
		setFocusLast('description');
		break;

	case 'alphaAccessCode':
		setFocusLast('conciseDescription');
		break;

	case 'transactionMode':
		setFocusLast('alphaAccessCode');
		break;

	case 'depositWithdrawal':
		setFocusLast('transactionMode');
		break;
	case 'transactionUsingDocument':
		setFocusLast('depositWithdrawal');
		break;

	case 'slipNumberRequired':
		setFocusLast('transactionUsingDocument');
		break;

	case 'numberOfLegsInTheTransaction':
		if ($('#transactionUsingDocument').val() == 2) {
			makeslipNumberRequiredReadOnly();
			setFocusLast('slipNumberRequired');
		} else {
			setFocusLast('transactionUsingDocument');
		}
		break;

	case 'enabled':
		setFocusLast('numberOfLegsInTheTransaction');
		break;
	case 'remarks':
		if ($('#action').val() == MODIFY) {
			setFocusLast('enabled');
		} else {
			setFocusLast('numberOfLegsInTheTransaction');
		}
		break;
	}
}

function transactionCode_val() {
	var value = $('#transactionCode').val();
	$('#transactionCode_desc').html(EMPTY_STRING);
	clearError('transactionCode_error');
	if (isEmpty(value)) {
		setError('transactionCode_error', MANDATORY);
		return false;
	} else if (!isNumeric(value)) {
		setError('transactionCode_error', INVALID_NUMBER);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('TRAN_CODE', value);
		validator.setValue(ACTION, $('#action').val());
		validator.setClass('patterns.config.web.forms.cmn.mtranbean');
		validator.setMethod('validateTransactionCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('transactionCode_error', validator.getValue(ERROR));
			return false;
		} else if ($('#action').val() == MODIFY) {
			PK_VALUE = value;
			if (!loadPKValues(PK_VALUE, 'transactionCode_error')) {
				return false;
			}
		}
	}
	setFocusLast('description');
	return true;
}

function description_val(valMode) {
	var value = $('#description').val();
	clearError('description_error');
	if (isEmpty(value)) {
		setError('description_error', MANDATORY);
		return false;
	}
	if (!isValidDescription(value)) {
		setError('description_error', INVALID_DESCRIPTION);
		return false;
	}
	setFocusLast('conciseDescription');
	return true;
}

function conciseDescription_val(valMode) {
	var value = $('#conciseDescription').val();
	clearError('conciseDescription_error');
	if (isEmpty(value)) {
		setError('conciseDescription_error', MANDATORY);
		return false;
	}
	if (!isValidConciseDescription(value)) {
		setError('conciseDescription_error', INVALID_CONCISE_DESCRIPTION);
		return false;
	}
	setFocusLast('alphaAccessCode');
	return true;
}

function alphaAccessCode_val() {
	var value = $('#alphaAccessCode').val();
	clearError('alphaAccessCode_error');
	if (!isEmpty(value)) {
		if (!isValidCode(value)) {
			setError('alphaAccessCode_error', INVALID_FORMAT);
			return false;
		} else {
			validator.clearMap();
			validator.setMtm(false);
			validator.setValue('TRAN_CODE', $('#transactionCode').val());
			validator.setValue('TRAN_ACCESS_CODE', value);
			validator.setValue('PROGRAM_ID', CURRENT_PROGRAM_ID);
			validator.setValue(ACTION, USAGE);
			validator.setClass('patterns.config.web.forms.cmn.mtranbean');
			validator.setMethod('validateAlphaAccessCode');
			validator.sendAndReceive();
			if (validator.getValue(ERROR) != EMPTY_STRING) {
				setError('alphaAccessCode_error', validator.getValue(ERROR));
				return false;
			}
		}
	}
	setFocusLast('transactionMode');
	return true;
}

function transactionMode_val() {
	var value = $('#transactionMode').val();
	clearError('transactionMode_error');
	if (isEmpty(value)) {
		setError('transactionMode_error', MANDATORY);
		return false;
	}
	setFocusLast('depositWithdrawal');
	return true;

}
function depositWithdrawal_val() {
	var value = $('#depositWithdrawal').val();
	clearError('depositWithdrawal_error');
	if (isEmpty(value)) {
		setError('depositWithdrawal_error', MANDATORY);
		return false;
	}
	setFocusLast('transactionUsingDocument');
	return true;

}
function transactionUsingDocument_val() {
	var value = $('#transactionUsingDocument').val();
	clearError('transactionUsingDocument_error');
	if (isEmpty(value)) {
		setError('transactionUsingDocument_error', MANDATORY);
		return false;
	}
	var depositWithdrawalVal = $('#depositWithdrawal').val();
	if (depositWithdrawalVal == 1 && value == 3) {
		setError('transactionUsingDocument_error', PBS_VALUE_NT_ALLOWED_FOR_WITHDRAWAL);
		return false;
	}
	if (depositWithdrawalVal == 2 && value != 3) {
		setError('transactionUsingDocument_error', PBS_VALUE_NT_ALLOWED_FOR_DEPOSIT);
		return true;

	}
	if (value == 2) {
		makeslipNumberRequiredReadOnly();
		setFocusLast('slipNumberRequired');

	} else {
		if (!$('#slipNumberRequired').is(':checked')) {
			makeslipNumberRequiredReadOnly();
			setFocusLast('numberOfLegsInTheTransaction');
		} else {
			$('#slipNumberRequired').prop('disabled', false);
			setError('slipNumberRequired_error', PBS_TRAN_USING_DOC_IS_NOT_WITHDRAWAL_SLIP);
			setFocusLast('slipNumberRequired');
			return false;
		}

	}

}
function slipNumberRequired_val() {
	clearError('slipNumberRequired_error');
	if (($('#transactionUsingDocument').val() != 2) && ($('#slipNumberRequired').is(':checked'))) {
		setError('slipNumberRequired_error', PBS_TRAN_USING_DOC_IS_NOT_WITHDRAWAL_SLIP);
		return false;
	}
	setFocusLast('numberOfLegsInTheTransaction');
	return true;
}

function numberOfLegsInTheTransaction_val() {
	var value = $('#numberOfLegsInTheTransaction').val();
	clearError('numberOfLegsInTheTransaction_error');
	if (isEmpty(value)) {
		setError('transactionUsingDocument_error', MANDATORY);
		return false;
	}
	if ($('#action').val() == MODIFY)
		setFocusLast('enabled');
	else
		setFocusLast('remarks');
	return true;

}

function enabled_val() {
	if ($('#action').val() == MODIFY) {
		setFocusLast('remarks');
	}
}

function remarks_val(valMode) {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', INVALID_REMARKS);
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', INVALID_REMARKS);
			return false;
		}
	}
	if ($('#action').val() == MODIFY) {
		if (isEmpty(value)) {
			setError('remarks_error', MANDATORY);
			return false;
		}
	}
	setFocusOnSubmit();
	return true;
}
function redisplayCheckBoxValues() {
	if ($('#slipNumberRequired').is(':checked'))
		setCheckbox('slipNumberRequired', YES);
	else
		setCheckbox('slipNumberRequired', NO);
	if ($('#enabled').is(':checked'))
		setCheckbox('enabled', YES);
	else
		setCheckbox('enabled', NO);
	makeslipNumberRequiredReadOnly();
}
function makeslipNumberRequiredReadOnly() {
	if ($('#transactionUsingDocument').val() == 2)
		$('#slipNumberRequired').prop('disabled', false);
	else
		$('#slipNumberRequired').prop('disabled', true);
}