var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MIFSC';
function init() {
	refreshTBAGrid();
	refreshQueryGrid();
	if (_redisplay) {
		if ($('#action').val() == ADD) {
			$('#enabled').prop('disabled', true);
			setCheckbox('enabled', YES);
		}

	}
}
function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'mifsc_main', queryGrid);
}
function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}
function clearFields() {
	$('#ifscCode').val(EMPTY_STRING);
	$('#ifscCode_error').html(EMPTY_STRING);
	clearNonPKFields();
}
function clearNonPKFields() {
	$('#bankName').val(EMPTY_STRING);
	$('#bankName_error').html(EMPTY_STRING);
	$('#branchName').val(EMPTY_STRING);
	$('#branchName_error').html(EMPTY_STRING);
	$('#address').val(EMPTY_STRING);
	$('#address_error').html(EMPTY_STRING);
	$('#micr').val(EMPTY_STRING);
	$('#micr_error').html(EMPTY_STRING);
	$('#city').val(EMPTY_STRING);
	$('#city_error').html(EMPTY_STRING);
	$('#stateCode').val(EMPTY_STRING);
	$('#stateCode_error').html(EMPTY_STRING);
	$('#stateCode_desc').html(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
}
function doclearfields(id) {
	switch (id) {
	case 'ifscCode':
		if (isEmpty($('#ifscCode').val())) {
			clearFields();
			break;
		}
	}
}
function add() {
	$('#ifscCode').focus();
	setCheckbox('enabled', YES);
	$('#enabled').prop('disabled', true);
}
function modify() {
	$('#ifscCode').focus();
	if ($('#action').val() == MODIFY) {
		$('#enabled').prop('disabled', false);
	} else {
		$('#enabled').prop('disabled', true);
	}
}
function view(source, primaryKey) {
	hideParent('cmn/qifsc', source, primaryKey);
	resetLoading();
}
function loadData() {
	$('#ifscCode').val(validator.getValue('IFSC_CODE'));
	$('#bankName').val(validator.getValue('BANK_NAME'));
	$('#branchName').val(validator.getValue('BRANCH_NAME'));
	$('#address').val(validator.getValue('ADDRESS'));
	$('#micr').val(validator.getValue('MICR_CODE'));
	$('#city').val(validator.getValue('CITY'));
	$('#stateCode').val(validator.getValue('STATE_CODE'));
	$('#stateCode_desc').html(validator.getValue('F1_DESCRIPTION'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	setFocusLast('ifscCode');
	resetLoading();
}
function backtrack(id) {
	switch (id) {
	case 'ifscCode':
		setFocusLast('ifscCode');
		break;
	case 'bankName':
		setFocusLast('ifscCode');
		break;
	case 'branchName':
		setFocusLast('bankName');
		break;
	case 'address':
		setFocusLast('branchName');
		break;
	case 'micr':
		setFocusLast('address');
		break;
	case 'city':
		setFocusLast('micr');
		break;
	case 'stateCode':
		setFocusLast('city');
		break;
	case 'enabled':
		setFocusLast('stateCode');
		break;
	case 'remarks':
		if ($('#action').val() == MODIFY) {
			setFocusLast('enabled');
		} else {
			setFocusLast('stateCode');
		}
		break;
	}
}
function doHelp(id) {
	switch (id) {
	case 'ifscCode':
		help('COMMON', 'HLP_IFSC_CODE', $('#ifscCode').val(), EMPTY_STRING, $('#ifscCode'));
		break;
	case 'stateCode':
		help('COMMON', 'HLP_STATE_CODE', $('#stateCode').val(), EMPTY_STRING, $('#stateCode'));
		break;
	}
}
function validate(id) {
	switch (id) {
	case 'ifscCode':
		ifscCode_val();
		break;
	case 'bankName':
		bankName_val();
		break;
	case 'branchName':
		branchName_val();
		break;
	case 'address':
		address_val();
		break;
	case 'micr':
		micr_val();
		break;
	case 'city':
		city_val();
		break;
	case 'stateCode':
		stateCode_val();
		break;
	case 'enabled':
		enabled_val();
		break;
	case 'remarks':
		remarks_val();
		break;
	}
}
function ifscCode_val() {
	var value = $('#ifscCode').val();
	clearError('ifscCode_error');
	if (isEmpty(value)) {
		setError('ifscCode_error', PBS_MANDATORY);
		return false;
	} else if ($('#action').val() == ADD && !isValidCode(value)) {
		setError('ifscCode_error', INVALID_FORMAT);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('IFSC_CODE', $('#ifscCode').val());
		validator.setValue(ACTION, $('#action').val());
		validator.setClass('patterns.config.web.forms.cmn.mifscbean');
		validator.setMethod('validateIFSCCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('ifscCode_error', validator.getValue(ERROR));
			return false;
		} else {
			if ($('#action').val() == MODIFY) {
				PK_VALUE = getEntityCode() + PK_SEPERATOR + $('#ifscCode').val();
				if (!loadPKValues(PK_VALUE, 'ifscCode_error')) {
					return false;
				}
				setFocusLast('bankName');
				return true;
			}
		}
	}
	setFocusLast('bankName');
	return true;
}
function bankName_val() {
	var value = $('#bankName').val();
	clearError('bankName_error');
	if (isEmpty(value)) {
		setError('bankName_error', MANDATORY);
		return false;
	}
	if (!isValidOtherInformation50(value)) {
		setError('bankName_error', INVALID_VALUE);
		return false;
	}
	setFocusLast('branchName');
	return true;
}
function branchName_val() {
	var value = $('#branchName').val();
	clearError('branchName_error');
	if (isEmpty(value)) {
		setError('branchName_error', MANDATORY);
		return false;
	}
	if (!isValidDescription(value)) {
		setError('branchName_error', INVALID_DESCRIPTION);
		return false;
	}
	setFocusLast('address');
	return true;
}
function address_val() {
	var value = $('#address').val();
	clearError('address_error');
	if (isEmpty(value)) {
		setError('address_error', MANDATORY);
		return false;
	}
	if (isWhitespace(value)) {
		setError('branchName_error', INVALID_REMARKS);
		return false;
	}
	if (!isValidRemarks(value)) {
		setError('branchName_error', INVALID_REMARKS);
		return false;
	}
	setFocusLast('micr');
	return true;
}
function micr_val() {
	var value = $('#micr').val();
	clearError('micr_error');
	if (!isEmpty(value)) {
		if (!isValidConciseDescription(value)) {
			setError('micr_error', INVALID_DESCRIPTION);
			return false;
		}
	}
	setFocusLast('city');
	return true;
}

function city_val() {
	var value = $('#city').val();
	clearError('city_error');
	if (isEmpty(value)) {
		setError('city_error', MANDATORY);
		return false;
	}
	if (!isValidDescription(value)) {
		setError('city_error', INVALID_DESCRIPTION);
		return false;
	}
	setFocusLast('stateCode');
	return true;
}
function stateCode_val() {
	var value = $('#stateCode').val();
	clearError('stateCode_error');
	if (isEmpty(value)) {
		setError('stateCode_error', MANDATORY);
		return false;
	} else if ($('#action').val() == ADD && !isValidCode(value)) {
		setError('stateCode_error', INVALID_FORMAT);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('STATE_CODE', $('#stateCode').val());
		validator.setValue(ACTION, $('#action').val());
		validator.setClass('patterns.config.web.forms.cmn.mifscbean');
		validator.setMethod('validateStateCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('stateCode_error', validator.getValue(ERROR));
			return false;
		}
		if (validator.getValue(RESULT) == DATA_AVAILABLE) {
			$('#stateCode_desc').html(validator.getValue('DESCRIPTION'));
		}
	}

	setFocusLast('remarks');
	return true;
}

function enabled_val() {
	if ($('#action').val() == MODIFY) {
		setFocusLast('remarks');
	}
	return true;
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', INVALID_REMARKS);
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', INVALID_REMARKS);
			return false;
		}
	}
	if ($('#action').val() == MODIFY) {
		if (isEmpty(value)) {
			setError('remarks_error', MANDATORY);
			return false;
		}
	}
	setFocusOnSubmit();
	return true;
}