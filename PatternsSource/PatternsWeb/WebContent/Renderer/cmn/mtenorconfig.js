var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MTENORCONFIG';
var error_edit = false;
var MONTHS_IN_DAYS = 30;
function init() {
	refreshTBAGrid();
	refreshQueryGrid();
	if (_redisplay) {
		if ($('#action').val() == ADD) {
			$('#enabled').prop('disabled', true);
			setCheckbox('enabled', YES);
		}
		mtenorconfig_innerGrid.clearAll();
		if (!isEmpty($('#xmlMtenorconfigGrid').val())) {
			mtenorconfig_innerGrid.loadXMLString($('#xmlMtenorconfigGrid').val());
		}

	}
}
function loadGrid() {
	loadGridQuery(CURRENT_PROGRAM_ID, 'MTENORCONFIG_GRID', mtenorconfig_innerGrid);
}

function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'MTENORCONFIG_MAIN');
}

function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}

function add() {
	$('#tenorSpecificationCode').focus();
	$('#enabled').prop('disabled', true);
	setCheckbox('enabled', YES);
}

function modify() {
	$('#tenorSpecificationCode').focus();
	if ($('#action').val() == MODIFY) {
		$('#enabled').prop('disabled', false);
	} else {
		$('#enabled').prop('disabled', true);
	}
}

function view(source, primaryKey) {
	hideParent('cmn/qtenorconfig', source, primaryKey);
	resetLoading();
}

function clearFields() {
	$('#tenorSpecificationCode').val(EMPTY_STRING);
	$('#tenorSpecificationCode_desc').html(EMPTY_STRING);
	$('#tenorSpecificationCode_error').html(EMPTY_STRING);
	$('#efftDate').val(EMPTY_STRING);
	$('#efftDate_error').html(EMPTY_STRING);
	clearNonPKFields();
}

function clearNonPKFields() {
	setCheckbox('enabled', YES);
	$('#enabled_error').html(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
	clearGridFields();
	mtenorconfig_innerGrid.clearAll();

}
function clearGridFields() {
	$('#uptoPeriod').val(EMPTY_STRING);
	$('#uptoPeriod_error').html(EMPTY_STRING);
	$('#periodFlag').val($("#periodFlag option:first-child").val());
	$('#periodFlag_error').html(EMPTY_STRING);
	$('#tenorDescription').val(EMPTY_STRING);
	$('#tenorDescription_error').html(EMPTY_STRING);
}

function doclearfields(id) {
	switch (id) {
	case 'tenorSpecificationCode':
		if (isEmpty($('#tenorSpecificationCode').val())) {
			clearFields();
		}
		break;
	case 'efftDate':
		if (isEmpty($('#efftDate').val())) {
			$('#efftDate_error').html(EMPTY_STRING);
			clearNonPKFields();
		}
		break;
	}
}
function loadData() {
	$('#tenorSpecificationCode').val(validator.getValue('TENOR_SPEC_CODE'));
	$('#tenorSpecificationCode_desc').html(validator.getValue('F1_DESCRIPTION'));
	$('#efftDate').val(validator.getValue('EFF_DATE'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadGrid();
	resetLoading();
}

function doHelp(id) {
	switch (id) {
	case 'tenorSpecificationCode':
		help('COMMON', 'HLP_TENOR_SPEC_CODE', $('#tenorSpecificationCode').val(), EMPTY_STRING, $('#tenorSpecificationCode'));
		break;
	case 'efftDate':
		help('MTENORCONFIG', 'HLP_EFF_DATE', $('#efftDate').val(), $('#tenorSpecificationCode').val(), $('#efftDate'));
		break;
	}
}

function validate(id) {
	var valMode = true;
	switch (id) {
	case 'tenorSpecificationCode':
		tenorSpecificationCode_val();
		break;
	case 'efftDate':
		effectiveDate_val();
		break;
	case 'uptoPeriod':
		uptoPeriod_val(valMode);
		break;
	case 'periodFlag':
		periodFlag_val(valMode);
		break;
	case 'tenorDescription':
		tenorDescription_val(valMode);
		break;
	case 'enabled':
		enabled_val();
		break;
	case 'remarks':
		remarks_val();
		break;

	}
}

function backtrack(id) {
	switch (id) {
	case 'efftDate':
		setFocusLast('tenorSpecificationCode');
		break;
	case 'uptoPeriod':
		setFocusLast('efftDate');
		break;
	case 'periodFlag':
		setFocusLast('uptoPeriod');
		break;
	case 'tenorDescription':
		setFocusLast('periodFlag');
		break;
	case 'mtenorconfig_innerGrid_add':
		setFocusLast('tenorDescription');
		break;
	case 'enabled':
		setFocusLast('uptoPeriod');
		break;
	case 'remarks':
		if ($('#action').val() == MODIFY)
			setFocusLast('enabled');
		else
			setFocusLast('uptoPeriod');
		break;
	}
}

function tenorSpecificationCode_val() {
	var value = $('#tenorSpecificationCode').val();
	$('#tenorSpecificationCode_desc').html(EMPTY_STRING);
	clearError('tenorSpecificationCode_error');
	if (isEmpty(value)) {
		setError('tenorSpecificationCode_error', MANDATORY);
		return false;
	} else if (!isValidCode(value)) {
		setError('tenorSpecificationCode_error', INVALID_FORMAT);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('TENOR_SPEC_CODE', value);
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.cmn.mtenorconfigbean');
		validator.setMethod('validateTenorSpecificationCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('tenorSpecificationCode_error', validator.getValue(ERROR));
			return false;
		} else if (validator.getValue(RESULT) == DATA_AVAILABLE) {
			$('#tenorSpecificationCode_desc').html(validator.getValue('DESCRIPTION'));
		}
	}
	setFocusLast('efftDate');
	return true;
}

function effectiveDate_val(valMode) {
	var value = $('#efftDate').val();
	clearError('efftDate_error');
	if (isEmpty(value)) {
		$('#efftDate').val(getCBD);
		value = $('#efftDate').val();
	}
	if (!isValidEffectiveDate(value, 'efftDate_error')) {
		return false;
	} else {

		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('TENOR_SPEC_CODE', $('#tenorSpecificationCode').val());
		validator.setValue('EFF_DATE', value);
		validator.setValue('ACTION', $('#action').val());
		validator.setClass('patterns.config.web.forms.cmn.mtenorconfigbean');
		validator.setMethod('validateEffectiveDate');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('efftDate_error', validator.getValue(ERROR));
			return false;
		} else if ($('#action').val() == MODIFY) {
			PK_VALUE = getEntityCode() + PK_SEPERATOR + $('#tenorSpecificationCode').val() + PK_SEPERATOR + getTBADateFormat($('#efftDate').val());
			if (!loadPKValues(PK_VALUE, 'efftDate_error')) {
				clearNonPKFields();
				return false;
			}
		}
	}
	setFocusLast('uptoPeriod');
	return true;

}

function uptoPeriod_val(valMode) {
	var value = $('#uptoPeriod').val();
	clearError('uptoPeriod_error');
	if (isEmpty(value)) {
		setError('uptoPeriod_error', MANDATORY);
		return false;
	} else if (!isNumeric(value)) {
		setError('uptoPeriod_error', NUMERIC_CHECK);
		return false;
	} else if (isZero(value)) {
		setError('uptoPeriod_error', ZERO_CHECK);
		return false;
	} else if (isNegative(value)) {
		setError('uptoPeriod_error', POSITIVE_CHECK);
		return false;
	}
	if (valMode)
		setFocusLast('periodFlag');
	return true;
}

function periodFlag_val(valMode) {
	var value = $('#periodFlag').val();
	clearError('periodFlag_error');
	if (isEmpty(value)) {
		setError('periodFlag_error', MANDATORY);
		return false;
	}
	if (valMode)
		setFocusLast('tenorDescription');
	return true;
}

function tenorDescription_val(valMode) {
	var value = $('#tenorDescription').val();
	clearError('tenorDescription_error');
	if (isEmpty(value)) {
		setError('tenorDescription_error', MANDATORY);
		return false;
	} else if (isWhitespace(value)) {
		setError('tenorDescription_error', INVALID_REMARKS);
		return false;
	} else if (!isValidRemarks(value)) {
		setError('tenorDescription_error', INVALID_REMARKS);
		return false;
	}
	if (valMode)
		setFocusLast('mtenorconfig_innerGrid_add');
	return true;
}

function enabled_val() {
	setFocusLast('remarks');
}

function remarks_val(valMode) {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', INVALID_REMARKS);
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', INVALID_REMARKS);
			return false;
		}
	}
	if ($('#action').val() == MODIFY) {
		if (isEmpty(value)) {
			setError('remarks_error', MANDATORY);
			return false;
		}
	}
	setFocusOnSubmit();
	return true;
}
// grid validation start
function gridExit(id) {
	switch (id) {
	case 'uptoPeriod':
		$('#xmlMtenorconfigGrid').val(mtenorconfig_innerGrid.serialize());
		clearGridFields();
		if ($('#action').val() == MODIFY)
			setFocusLast('enabled');
		else
			setFocusLast('remarks');
		break;
	}
}

function gridDeleteRow(id) {
	switch (id) {
	case 'uptoPeriod':
		deleteGridRecord(mtenorconfig_innerGrid);
		break;
	}
}
function mtenorconfig_editGrid(rowId) {
	$('#uptoPeriod').val(mtenorconfig_innerGrid.cells(rowId, 1).getValue());
	$('#periodFlag').val(mtenorconfig_innerGrid.cells(rowId, 4).getValue());
	$('#tenorDescription').val(mtenorconfig_innerGrid.cells(rowId, 3).getValue());
	$('#uptoPeriod').focus();
}
function mtenorconfig_CancelGrid() {
	clearGridFields();
}

function revalidate() {
	mtenorconfig_revaildateGrid();
}
function mtenorconfig_revaildateGrid() {
	if (mtenorconfig_innerGrid.getRowsNum() > 0) {
		$('#xmlMtenorconfigGrid').val(mtenorconfig_innerGrid.serialize());
		clearGridFields();
	} else {
		mtenorconfig_innerGrid.clearAll();
		$('#xmlMtenorconfigGrid').val(mtenorconfig_innerGrid.serialize());
		setError('uptoPeriod_error', ADD_ATLEAST_ONE_ROW);
		errors++;
	}
}

function mtenorconfig_addGrid(editMode, editRowId) {
	var value;
	if ($('#periodFlag').val() == 'M')
		value = $('#uptoPeriod').val() * MONTHS_IN_DAYS;
	else
		value = $('#uptoPeriod').val();

	if (uptoPeriod_val(false) && periodFlag_val(false) && tenorDescription_val(false)) {
		if (mtenorconfig_innerGrid.getUserData("", "insert_first")) {
			if (!insertFirst(value)) {
				if (editMode)
					error_edit = true;
				setFocusLast('uptoPeriod');
				return false;
			}
		} else {
			if (!gridAddingValidation(value, editRowId)) {
				if (editMode)
					error_edit = true;
				setFocusLast('uptoPeriod');
				return false;
			}
		}
		var field_values = [ 'false', $('#uptoPeriod').val(), $('#periodFlag option:selected').text(), $('#tenorDescription').val(), $('#periodFlag').val() ];
		_grid_updateRow(mtenorconfig_innerGrid, field_values);
		clearGridFields();
		setFocusLast('uptoPeriod');
		return true;
	} else {
		if (editMode)
			error_edit = true;
	}

}

function gridAddingValidation(value, editRowId) {
	var CurrentRowInd;
	var nextRowInd;
	var previousRowInd;
	if (!isEmpty(editRowId)) {
		CurrentRowInd = mtenorconfig_innerGrid.getRowIndex(editRowId);
		nextRowInd = CurrentRowInd + 1;
		previousRowInd = CurrentRowInd - 1;
	} else if (!isEmpty(mtenorconfig_innerGrid.getCheckedRows(0))) {
		CurrentRowInd = mtenorconfig_innerGrid.getRowIndex(mtenorconfig_innerGrid.getCheckedRows(0));
		nextRowInd = CurrentRowInd + 1;
		previousRowInd = CurrentRowInd;
	} else {
		CurrentRowInd = mtenorconfig_innerGrid.getRowsNum();
		nextRowInd = CurrentRowInd;
		previousRowInd = CurrentRowInd - 1;
	}
	var nextRowUptoPeriodInDays;
	noOfRows = mtenorconfig_innerGrid.getRowsNum();
	if (noOfRows >= 1) {
		if (previousRowInd > -1) {
			if (mtenorconfig_innerGrid.cells2(previousRowInd, 4).getValue() == 'M') {
				if ($('#periodFlag').val() == 'D') {
					setError('uptoPeriod_error', PBS_SAME_PERIOD_FLAG);
					setFocusLast('uptoPeriod');
					return false;
				}
				previousRowIndUptoPeriodInDays = (mtenorconfig_innerGrid.cells2(previousRowInd, 1).getValue()) * MONTHS_IN_DAYS;
			} else {
				previousRowIndUptoPeriodInDays = (mtenorconfig_innerGrid.cells2(previousRowInd, 1).getValue());
			}
			if (isNumberLesserThanEqual(value, previousRowIndUptoPeriodInDays)) {
				setError('uptoPeriod_error', PBS_TENOR_INCR_ORDER);
				setFocusLast('uptoPeriod');
				return false;
			}
		}
		if (nextRowInd <= noOfRows - 1) {
			if (mtenorconfig_innerGrid.cells2(nextRowInd, 4).getValue() == 'D' && $('#periodFlag').val() == 'M') {
				setError('uptoPeriod_error', PBS_SAME_PERIOD_FLAG);
				setFocusLast('uptoPeriod');
				return false;
			}
			if (mtenorconfig_innerGrid.cells2(nextRowInd, 4).getValue() == 'M')
				nextRowUptoPeriodInDays = (mtenorconfig_innerGrid.cells2(nextRowInd, 1).getValue()) * MONTHS_IN_DAYS;
			else
				nextRowUptoPeriodInDays = (mtenorconfig_innerGrid.cells2(nextRowInd, 1).getValue());

			if (!isNumberLesser(value, nextRowUptoPeriodInDays)) {
				setError('uptoPeriod_error', PBS_TENOR_INCR_ORDER);
				return false;
			}
		}

	}
	return true;
}

function mtenorconfig_afteraddGrid(updatedRowId) {
	if (error_edit) {
		mtenorconfig_innerGrid.setUserData("", "grid_edit_id", updatedRowId);
	}
	error_edit = false;
}

function insertFirst(value) {
	var uptoPeriodInDays;
	if (mtenorconfig_innerGrid.cells2(0, 4).getValue() == 'M')
		uptoPeriodInDays = mtenorconfig_innerGrid.cells2(0, 1).getValue() * MONTHS_IN_DAYS;
	else
		uptoPeriodInDays = mtenorconfig_innerGrid.cells2(0, 1).getValue();
	if (!isNumberLesser(value, uptoPeriodInDays)) {
		setError('uptoPeriod_error', PBS_TENOR_INCR_ORDER);
		setFocusLast('uptoPeriod');
		return false;
	}
	return true;
}
