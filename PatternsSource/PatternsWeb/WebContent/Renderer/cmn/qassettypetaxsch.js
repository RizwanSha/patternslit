var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'IASSETTYPETAXSCH';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}

function loadQuery() {
	iassettypetaxsch_innerGrid.setColumnsVisibility("true,true,false,false,false");
	loadGridQuery(CURRENT_PROGRAM_ID, 'IASSETTYPETAXSCH_GRID', iassettypetaxsch_innerGrid);
}

function clearFields() {
	$('#assetTypeCode').val(EMPTY_STRING);
	$('#assetTypeCode_desc').html(EMPTY_STRING);
	$('#effectiveDate').val(EMPTY_STRING);
	iassettypetaxsch_innerGrid.clearAll();
	setCheckbox('enabled', NO);
	$('#remarks').val(EMPTY_STRING);
}
function loadData() {
	$('#assetTypeCode').val(validator.getValue('ASSET_TYPE_CODE'));
	$('#assetTypeCode_desc').html(validator.getValue('F1_DESCRIPTION'));
	$('#effectiveDate').val(validator.getValue('EFF_DATE'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadAuditFields(validator);
	loadGrid();
}

function loadGrid() {
	SOURCE_VALUE = _tableType;
	PK_VALUE = _primaryKey;
	if (_tableType == MAIN) {
		loadQuery();

	} else if (_tableType == TBA) {
		loadQuery();
	}
}
