var validator = new xmlHTTPValidator();
var validator1 = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MSCHFORACCTYPE';

function init() {
	refreshTBAGrid();
	refreshQueryGrid();
	if (_redisplay) {
		if ($('#action').val() == ADD) {
			$('#enabled').prop('disabled', true);
			setCheckbox('enabled', YES);
		}
		mschforacctype_innerGrid.clearAll();
		if (!isEmpty($('#xmlMschforacctype').val())) {
			mschforacctype_innerGrid.loadXMLString($('#xmlMschforacctype').val());
		}
	}

}

function loadSchemesGrid() {
	validator1.clearMap();
	validator1.setMtm(false);
	validator1.setValue('ACTION', USAGE);
	validator1.setClass('patterns.config.web.forms.cmn.mschforacctypebean');
	validator1.setMethod('GetSchemesCode');
	validator1.sendAndReceive();
	if (validator1.getValue(ERROR) != EMPTY_STRING) {
		setError('accountTypeCode_error', validator1.getValue(ERROR));
		return false;
	}
	mschforacctype_innerGrid.loadXMLString(validator1.getValue(RESULT_XML));
}

function loadGrid() {
	loadGridQuery(CURRENT_PROGRAM_ID, 'MSCHFORACCTYPE_GRID', postProcessing);
}

function postProcessing(result) {
	$xml = $($.parseXML(result));
	($xml).find("row").each(function() {
		try {
			var rowID = $(this).find("cell")[1].textContent;
			mschforacctype_innerGrid.cells(rowID, 0).setValue(true);
		} catch (e) {

		}
	});

}

function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'MSCHFORACCTYPE_MAIN', queryGrid);
}

function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}

function doHelp(id) {
	switch (id) {
	case 'accountTypeCode':
		help('COMMON', 'HLP_ACCOUNT_TYPE_CODE', $('#accountTypeCode').val(), EMPTY_STRING, $('#accountTypeCode'));
		break;
	case 'effectiveDate':
		help('MSCHFORACCTYPE', 'HLP_EFF_DATE', $('#effectiveDate').val(), EMPTY_STRING, $('#effectiveDate'));
		break;

	}
}
function doclearfields(id) {
	switch (id) {
	case 'accountTypeCode':
		if (isEmpty($('#accountTypeCode').val())) {
			clearFields();
			break;
		}

	case 'effectiveDate':
		if (isEmpty($('#effectiveDate').val())) {
			$('#effectiveDate_error').html(EMPTY_STRING);
			clearNonPKFields();
			break;
		}
	}
}
function validate(id) {
	switch (id) {
	case 'accountTypeCode':
		accountTypeCode_val();
		break;
	case 'effectiveDate':
		effectiveDate_val();
		break;
	case 'enabled':
		enabled_val();
		break;
	case 'remarks':
		remarks_val();
		break;

	}
}
function backtrack(id) {
	switch (id) {
	case 'accountTypeCode':
		setFocusLast('accountTypeCode');
		break;
	case 'effectiveDate':
		setFocusLast('accountTypeCode');
		break;
	case 'enabled':
		setFocusLast('effectiveDate');
		break;
	case 'remarks':
		if ($('#action').val() == MODIFY) {
			setFocusLast('enabled');
		} else {
			setFocusLast('effectiveDate');
		}
		break;
	}
}
function add() {
	$('#accountTypeCode').focus();
	setCheckbox('enabled', YES);
	$('#enabled').prop('disabled', true);
}

function modify() {
	$('#accountTypeCode').focus();
	if ($('#action').val() == MODIFY) {
		$('#enabled').prop('disabled', false);
	} else {
		$('#enabled').prop('disabled', true);
	}
}

function view(source, primaryKey) {
	hideParent('cmn/qschforacctype', source, primaryKey);
	resetLoading();
}
function clearFields() {
	$('#accountTypeCode').val(EMPTY_STRING);
	$('#accountTypeCode_desc').html(EMPTY_STRING);
	$('#accountTypeCode_error').html(EMPTY_STRING);
	$('#effectiveDate').val(EMPTY_STRING);
	$('#effectiveDate_error').html(EMPTY_STRING);
	clearNonPKFields();
}
function clearNonPKFields() {
	setCheckbox('enabled', YES);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
	mschforacctype_innerGrid.clearAll();
	loadSchemesGrid();
}
function loadData() {
	$('#accountTypeCode').val(validator.getValue('ACCOUNT_TYPE_CODE'));
	$('#effectiveDate').val(validator.getValue('EFF_DATE'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	$('#accountTypeCode_desc').html(validator.getValue('F1_DESCRIPTION'));
	loadSchemesGrid();
	loadGrid();
	resetLoading();
}
function accountTypeCode_val() {
	var value = $('#accountTypeCode').val();
	clearError('accountTypeCode_error');
	if (isEmpty(value)) {
		setError('accountTypeCode_error', MANDATORY);
		return false;
	} else if (!isNumeric(value)) {
		setError('accountTypeCode_error', NUMERIC_CHECK);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('ACCOUNT_TYPE_CODE', $('#accountTypeCode').val());
		validator.setValue('ACTION', USAGE);
		validator.setClass('patterns.config.web.forms.cmn.mschforacctypebean');
		validator.setMethod('validateAccountTypeCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('accountTypeCode_error', validator.getValue(ERROR));
			$('#accountTypeCode_desc').html(EMPTY_STRING);
			return false;
		} else {
			if (validator.getValue(RESULT) == DATA_AVAILABLE) {
				$('#accountTypeCode_desc').html(validator.getValue('DESCRIPTION'));
			}
		}
	}
	setFocusLast('effectiveDate');
	return true;
}

function effectiveDate_val(valMode) {
	var value = $('#effectiveDate').val();
	clearError('effectiveDate_error');
	if (isEmpty(value)) {
		$('#effectiveDate').val(getCBD());
		value = $('#effectiveDate').val();
	}
	if (!isValidEffectiveDate(value, 'effectiveDate_error')) {
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('ACCOUNT_TYPE_CODE', $('#accountTypeCode').val());
	validator.setValue('EFF_DATE', $('#effectiveDate').val());
	validator.setValue('ACTION', $('#action').val());
	validator.setClass('patterns.config.web.forms.cmn.mschforacctypebean');
	validator.setMethod('validateEffectiveDate');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('effectiveDate_error', validator.getValue(ERROR));
		return false;
	} else {
		if ($('#action').val() == MODIFY) {
			PK_VALUE = getEntityCode() + '|' + $('#accountTypeCode').val() + '|' + getTBADateFormat($('#effectiveDate').val());
			if (!loadPKValues(PK_VALUE, 'effectiveDate_error')) {
				return false;
			}
		}
	}
	if ($('#action').val() == ADD) {
		setFocusLast('remarks');
	} else {
		setFocusLast('enabled');
	}

	return true;
}
function enabled_val() {
	if ($('#action').val() == MODIFY) {
		setFocusLast('remarks');
	}
	return true;
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', INVALID_REMARKS);
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', INVALID_REMARKS);
			return false;
		}
	}
	if ($('#action').val() == MODIFY) {
		if (isEmpty(value)) {
			setError('remarks_error', MANDATORY);
			return false;
		}
	}
	setFocusOnSubmit();
	return true;
}
function revalidate() {

	mschforacctype_innerGrid_revaildateGrid();
}

function mschforacctype_innerGrid_revaildateGrid() {
	if (mschforacctype_innerGrid.getRowsNum() > 0) {
		$('#xmlMschforacctype').val(mschforacctype_innerGrid.serialize());
		$('#accountTypeCode_error').html(EMPTY_STRING);
	} else {
		mschforacctype_innerGrid.clearAll();
		$('#xmlMschforacctype').val(mschforacctype_innerGrid.serialize());
		setError('accountTypeCode_error', ADD_ATLEAST_ONE_ROW);
		errors++;
	}
	if (mschforacctype_innerGrid.getCheckedRows(0) == 0) {
		$('#xmlMschforacctype').val(mschforacctype_innerGrid.serialize());
		setError('accountTypeCode_error', CHECK_ATLEAST_ONE);
		errors++;
	}
}
