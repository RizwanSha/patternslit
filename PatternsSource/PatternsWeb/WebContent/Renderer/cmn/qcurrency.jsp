<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.cmn.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/cmn/qcurrency.js" />
	</web:dependencies>
	<web:loadPK />
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="qcurrency.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:viewContent id="po_view1">
				<web:dividerBlock>
					<web:sectionBlock width="width-100p" align="left">
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="180px" />
									<web:columnStyle />
								</web:columnGroup>

								<web:rowEven>
									<web:column>
										<web:legend key="mcurrency.currencycode" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:currencyDisplay property="currencyCode" id="currencyCode" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="180px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowOdd>
									<web:column>
										<web:legend key="mcurrency.ccyname" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:descriptionDisplay property="ccyName" id="ccyName" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="mcurrency.ccyshortname" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:conciseDescriptionDisplay property="ccyShortName" id="ccyShortName" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="mcurrency.ccynotation" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:currencyNotationDisplay property="ccyNotation" id="ccyNotation" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="mcurrency.hassubccy" var="program" />
									</web:column>
									<web:column>
										<type:checkboxDisplay property="hasSubCcy" id="hasSubCcy" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="mcurrency.sccyname" var="program"/>
									</web:column>
									<web:column>
										<type:descriptionDisplay property="sccyName" id="sccyName"/>
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="mcurrency.sccyshortname" var="program"/>
									</web:column>
									<web:column>
										<type:conciseDescriptionDisplay property="sccyShortName" id="sccyShortName" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="mcurrency.sccynotation" var="program"/>
									</web:column>
									<web:column>
										<type:currencyNotationDisplay property="sccyNotation" id="sccyNotation" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="mcurrency.sccyunitsforccy" var="program"/>
									</web:column>
									<web:column>
										<type:fiveDigitDisplay property="sccyUnitsForCcy" id="sccyUnitsForCcy" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="mcurrency.isoccycode" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:ISOCurrencyDisplay property="isoCcyCode" id="isoCcyCode" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="form.IsUseEnabled" var="common" />
									</web:column>
									<web:column>
										<type:checkboxDisplay property="enabled" id="enabled" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="form.remarks" var="common" />
									</web:column>
									<web:column>
										<type:remarksDisplay property="remarks" id="remarks" />
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
						<web:auditDisplay />
					</web:sectionBlock>
				</web:dividerBlock>
			</web:viewContent>
		</web:viewSubPart>
	</web:viewPart>
</web:fragment>