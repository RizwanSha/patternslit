var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MBKLICENTYPES';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}

function clearFields() {
	$('#bankLicenceType').val(EMPTY_STRING);
	$('#bankLicenceType_desc').html(EMPTY_STRING);
	$('#description').val(EMPTY_STRING);
	$('#conciseDescription').val(EMPTY_STRING);
	setCheckbox('casaAllowed', NO);
	setCheckbox('termDepositAllowed', NO);
	setCheckbox('loansAllowed', NO);
	setCheckbox('paymentServicesAllowed', NO);
	setCheckbox('collectionServiceAllowed', NO);
	setCheckbox('enabled', NO);
	$('#remarks').val(EMPTY_STRING);
}

function loadData() {
	$('#bankLicenceType').val(validator.getValue('BANK_LIC_TYPES'));
	$('#description').val(validator.getValue('DESCRIPTION'));
	$('#conciseDescription').val(validator.getValue('CONCISE_DESCRIPTION'));
	setCheckbox('casaAllowed', validator.getValue('CASA_ALLOWED'));
	setCheckbox('termDepositAllowed', validator.getValue('TERM_DEPOSIT_ALLOWED'));
	setCheckbox('loansAllowed', validator.getValue('LOANS_ALLOWED'));
	setCheckbox('paymentServicesAllowed', validator.getValue('PAYMENT_SERVICES_ALLOWED'));
	setCheckbox('collectionServiceAllowed', validator.getValue('COLLECTION_SERVICES_ALLOWED'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadAuditFields(validator);
}
