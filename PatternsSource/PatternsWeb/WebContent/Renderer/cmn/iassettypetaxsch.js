var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'IASSETTYPETAXSCH';

function init() {
	refreshTBAGrid();
	refreshQueryGrid();
	if (_redisplay) {
		if ($('#action').val() == ADD) {
			$('#enabled').prop('disabled', true);
			setCheckbox('enabled', YES);
		}
		iassettypetaxsch_innerGrid.clearAll();
		if (!isEmpty($('#xmliassettypetaxschGrid').val())) {
			iassettypetaxsch_innerGrid.loadXMLString($('#xmliassettypetaxschGrid').val());
		}
		hide('scheduleCode_Fields');
	}
}

function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'IASSETTYPETAXSCH_MAIN');
}

function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}

function add() {
	$('#assetTypeCode').focus();
	setCheckbox('enabled', YES);
	$('#enabled').prop('disabled', true);
	$('#effectiveDate').val(getCBD());

}

function modify() {
	$('#assetTypeCode').focus();
	if ($('#action').val() == MODIFY) {
		$('#enabled').prop('disabled', false);
	} else {
		$('#enabled').prop('disabled', true);
	}
}

function view(source, primaryKey) {
	hideParent('cmn/qassettypetaxsch', source, primaryKey);
	resetLoading();
}

function clearFields() {
	$('#assetTypeCode').val(EMPTY_STRING);
	$('#assetTypeCode_error').html(EMPTY_STRING);
	$('#assetTypeCode_desc').html(EMPTY_STRING);
	$('#effectiveDate').val(EMPTY_STRING);
	$('#effectiveDate_error').html(EMPTY_STRING);
	clearNonPKFields();
}

function clearNonPKFields() {
	clearGridFields();
	iassettypetaxsch_innerGrid.clearAll();
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
}
function doclearfields(id) {
	switch (id) {
	case 'assetTypeCode':
		if (isEmpty($('#assetTypeCode').val())) {
			clearFields();
			break;
		}
	case 'effectiveDate':
		if (isEmpty($('#effectiveDate').val())) {
			$('#effectiveDate_error').html(EMPTY_STRING);
			clearNonPKFields();
			break;
		}
	}
}

function loadGrid() {
	loadGridQuery(CURRENT_PROGRAM_ID, 'IASSETTYPETAXSCH_GRID', iassettypetaxsch_PostProcessing);
}

function iassettypetaxsch_PostProcessing(result) {
	iassettypetaxsch_innerGrid.parse(result);
	iassettypetaxsch_innerGrid.setColumnHidden(0, true);
	var noofrows = iassettypetaxsch_innerGrid.getRowsNum();
	if (noofrows > 0) {
		for (var i = 0; i < noofrows; i++) {
			var stateCode = iassettypetaxsch_innerGrid.cells2(i, 2).getValue();
			var linkValue = "<a href='javascript:void(0);' onclick=iassettypetaxsch_editGrid('" + stateCode + "')>Edit</a>";
			iassettypetaxsch_innerGrid.cells2(i, 1).setValue(linkValue);
			iassettypetaxsch_innerGrid.changeRowId(iassettypetaxsch_innerGrid.getRowId(i), stateCode);
		}
	}
}

function loadData() {
	$('#assetTypeCode').val(validator.getValue('ASSET_TYPE_CODE'));
	$('#assetTypeCode_desc').html(validator.getValue('F1_DESCRIPTION'));
	$('#effectiveDate').val(validator.getValue('EFF_DATE'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadGrid();
	resetLoading();
}

function doHelp(id) {
	switch (id) {
	case 'assetTypeCode':
		help('COMMON', 'HLP_ASSET_TYPE', $('#assetTypeCode').val(), EMPTY_STRING, $('#assetTypeCode'));
		break;
	case 'taxSheduleCode':
		help('COMMON', 'HLP_TAX_SCHEDULE_CODE', $('#taxSheduleCode').val(), $('#stateCode').val(), $('#taxSheduleCode'), function() {
		});
		break;
	}
}

function validate(id) {
	var valMode = true;
	switch (id) {
	case 'assetTypeCode':
		assetTypeCode_val();
		break;
	case 'effectiveDate':
		effectiveDate_val();
		break;
	case 'stateCode':
		stateCode_val(valMode);
		break;
	case 'taxSheduleCode':
		taxSheduleCode_val(valMode);
		break;
	case 'enabled':
		enabled_val();
		break;
	case 'remarks':
		remarks_val();
		break;

	}
}

function backtrack(id) {
	switch (id) {
	case 'assetTypeCode':
		setFocusLast('assetTypeCode');
		break;
	case 'effectiveDate':
		setFocusLast('assetTypeCode');
		break;
	case 'stateCode':
		setFocusLast('effectiveDate');
		break;
	case 'taxSheduleCode':
		setFocusLast('stateCode');
		break;
	case 'scheduleConfig':
		setFocusLast('taxSheduleCode');
		break;
	case 'enabled':
		setFocusLast('effectiveDate');
		break;
	case 'remarks':
		if ($('#action').val() == ADD)
			setFocusLast('effectiveDate');
		else
			setFocusLast('enabled');
		break;
	}
}

function gridExit(id) {
	switch (id) {
	case 'stateCode':
	case 'taxSheduleCode':
		$('#xmliassettypetaxschGrid').val(iassettypetaxsch_innerGrid.serialize());
		clearGridFields();
		if ($('#action').val() == ADD)
			setFocusLast('remarks');
		else
			setFocusLast('enabled');
		break;
	}
}

function assetTypeCode_val() {
	var value = $('#assetTypeCode').val();
	clearError('assetTypeCode_error');
	if (isEmpty(value)) {
		setError('assetTypeCode_error', MANDATORY);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('ASSET_TYPE_CODE', value);
	validator.setValue(ACTION, USAGE);
	validator.setClass('patterns.config.web.forms.cmn.iassettypetaxschbean');
	validator.setMethod('validateAssetTypeCode');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('assetTypeCode_error', validator.getValue(ERROR));
		$('#assetTypeCode_desc').html(EMPTY_STRING);
		return false;
	}
	$('#assetTypeCode_desc').html(validator.getValue("DESCRIPTION"));
	setFocusLast('effectiveDate');
	return true;
}
function effectiveDate_val(valMode) {
	var value = $('#effectiveDate').val();
	clearError('effectiveDate_error');
	if (isEmpty(value)) {
		value = getCBD();
		$('#effectiveDate').val(value);
	}
	if (!isValidEffectiveDate(value, 'effectiveDate_error')) {
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('ASSET_TYPE_CODE', $('#assetTypeCode').val());
		validator.setValue('EFF_DATE', value);
		validator.setValue(ACTION, $('#action').val());
		validator.setClass('patterns.config.web.forms.cmn.iassettypetaxschbean');
		validator.setMethod('validateEffectiveDate');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('effectiveDate_error', validator.getValue(ERROR));
			return false;
		} else if (validator.getValue(RESULT) == DATA_AVAILABLE) {
			iassettypetaxsch_innerGrid.loadXMLString(validator.getValue(RESULT_XML));
			hideMessage(getProgramID());
		}
		if ($('#action').val() == MODIFY) {
			PK_VALUE = getEntityCode() + PK_SEPERATOR + $('#assetTypeCode').val() + PK_SEPERATOR + $('#effectiveDate').val();
			if (!loadPKValues(PK_VALUE, 'effectiveDate_error')) {
				return false;
			}
		}
	}
	if ($('#action').val() == ADD)
		setFocusLast('remarks');
	else
		setFocusLast('enabled');
	return true;
}

function stateCode_val(valMode) {
	if (valMode)
		setFocusLast('taxSheduleCode');
	return true;
}

function taxSheduleCode_val(valMode) {
	var value = $('#taxSheduleCode').val();
	clearError('taxSheduleCode_error');
	if (!isEmpty(value)) {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('STATE_CODE', $('#stateCode').val());
		validator.setValue('TAX_SCHEDULE_CODE', value);
		validator.setClass('patterns.config.web.forms.cmn.iassettypetaxschbean');
		validator.setMethod('validateTaxScheduleCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('taxSheduleCode_error', validator.getValue(ERROR));
			return false;
		}
	}
	if (valMode)
		setFocusLast('scheduleConfig');
	return true;
}

function enabled_val() {
	if ($('#action').val() == MODIFY)
		setFocusLast('remarks');
	return true;
}
function remarks_val(valMode) {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', INVALID_REMARKS);
			setFocusLast('remarks');
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', INVALID_REMARKS);
			setFocusLast('remarks');
			return false;
		}
	}
	if ($('#action').val() == MODIFY) {
		if (isEmpty(value)) {
			setError('remarks_error', MANDATORY);
			setFocusLast('remarks');
			return false;
		}
	}
	setFocusOnSubmit();
	return true;
}

function updateScheduleConfigDetails() {
	if (ecustomerregproductDetails_gridDuplicateCheck() && stateCode_val(false) && taxSheduleCode_val(false)) {
		var linkValue = "<a href='javascript:void(0);' onclick=iassettypetaxsch_editGrid('" + $('#stateCode').val() + "')>Edit</a>";
		var field_values = [ '', linkValue, $('#stateCode').val(), $('#stateCode_desc').html(), $('#taxSheduleCode').val() ];
		var uid = _grid_updateRow(iassettypetaxsch_innerGrid, field_values);
		iassettypetaxsch_innerGrid.changeRowId(uid, $('#stateCode').val());
		clearGridFields();
		closeInlinePopUp(win);
		return true;
	}
}

function ecustomerregproductDetails_gridDuplicateCheck() {
	var currentValue = [ $('#stateCode').val(), $('#taxSheduleCode').val() ];
	if (!_grid_duplicate_check(iassettypetaxsch_innerGrid, currentValue, [ 2, 4 ])) {
		setError('taxSheduleCode_error', DUPLICATE_DATA);
		return false;
	}
	return true;
}

function openScheduleCodeConfig() {
	win = showWindow('scheduleCode_Fields', EMPTY_STRING, SCHEDULE_CODE_CONFIG, EMPTY_STRING, true, false, false, 650, 180);
}

function iassettypetaxsch_editGrid(rowId) {
	clearGridFields();
	openScheduleCodeConfig();
	iassettypetaxsch_innerGrid.setUserData("", "grid_edit_id", rowId);
	$('#stateCode').val(iassettypetaxsch_innerGrid.cells(rowId, 2).getValue());
	$('#stateCode_desc').html(iassettypetaxsch_innerGrid.cells(rowId, 3).getValue());
	$('#taxSheduleCode').val(iassettypetaxsch_innerGrid.cells(rowId, 4).getValue());
	setFocusLast('taxSheduleCode');
}

function clearGridFields() {
	$('#stateCode').val(EMPTY_STRING);
	$('#stateCode_desc').html(EMPTY_STRING);
	$('#stateCode_error').html(EMPTY_STRING);
	clearTaxScheduleCode();
}
function clearTaxScheduleCodeFields() {
	clearTaxScheduleCode();
	setFocusLast('taxSheduleCode');
}
function clearTaxScheduleCode() {
	$('#taxSheduleCode').val(EMPTY_STRING);
	$('#taxSheduleCode_error').html(EMPTY_STRING);
	$('#gridError_error').html(EMPTY_STRING);
}

function revalidate() {

	iassettypetaxsch_revaildateGrid();
}

function iassettypetaxsch_revaildateGrid() {
	var checkEmpty = 0;
	if (iassettypetaxsch_innerGrid.getRowsNum() > 0) {
		for (var i = 0; i < iassettypetaxsch_innerGrid.getRowsNum(); i++) {
			if (!isEmpty(iassettypetaxsch_innerGrid.cells2(i, 4).getValue())) {
				checkEmpty++;
			}
		}
		if (checkEmpty == 0) {
			setError('gridError_error', ATLEAST_ONE_CODE_MANDATORY);
			errors++;
			return false;
		}
		iassettypetaxsch_innerGrid.setSerializationLevel(false, false, false, false, false, true);
		$('#xmliassettypetaxschGrid').val(iassettypetaxsch_innerGrid.serialize());
		$('#stateCode_error').html(EMPTY_STRING);
		$('#gridError_error').html(EMPTY_STRING);
	} else {
		iassettypetaxsch_innerGrid.clearAll();
		$('#xmliassettypetaxschGrid').val(iassettypetaxsch_innerGrid.serialize());
		setError('stateCode_error', ADD_ATLEAST_ONE_ROW);
		setError('gridError_error', ADD_ATLEAST_ONE_ROW);
		errors++;
	}
}

function closePopup(value) {
	if (value == 0) {
		closeInlinePopUp(win);
	}
	return true;
}
