var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MENTITYBRANCH';
var cbdval = getCBD();
function init() {
	refreshTBAGrid();
	refreshQueryGrid();
	$('#entityCode').val(getEntityCode());
	$('#entityCode_desc').html(getEntityName());
	$('#entityBranchCode').focus();
	if ($('#action').val() == ADD) {
		$('#enabled').prop('disabled', true);
		setCheckbox('enabled', YES);
	}
}

function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'MENTITYBRANCH_MAIN', queryGrid);
}

function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}

function doclearfields(id) {
	switch (id) {
	case 'entityBranchCode':
		if (isEmpty($('#entityBranchCode').val())) {
			$('#entityBranchCode_error').html(EMPTY_STRING);
			$('#entityBranchCode_desc').html(EMPTY_STRING);
			clearNonPKFields();
			break;
		}
	}
}

function clearFields() {
	$('#entityBranchCode').val(EMPTY_STRING);
	$('#entityBranchCode_error').html(EMPTY_STRING);
	$('#entityBranchCode_desc').html(EMPTY_STRING);
	clearNonPKFields();
}

function clearNonPKFields() {
	$('#description').val(EMPTY_STRING);
	$('#description_error').html(EMPTY_STRING);
	$('#conciseDescription').val(EMPTY_STRING);
	$('#conciseDescription_error').html(EMPTY_STRING);
	$('#officeOpenDate').val(EMPTY_STRING);
	$('#officeOpenDate_error').html(EMPTY_STRING);
	$('#branchDetails').val(EMPTY_STRING);
	$('#branchDetails_error').html(EMPTY_STRING);
	$('#geoUnitStateCode').val(EMPTY_STRING);
	$('#geoUnitStateCode_error').html(EMPTY_STRING);
	$('#geoUnitStateCode_desc').html(EMPTY_STRING);
	$('#organizationUnit').val(EMPTY_STRING);
	$('#organizationUnit_error').html(EMPTY_STRING);
	$('#organizationUnit_desc').html(EMPTY_STRING);
	$('#controlBranchOffice').val(EMPTY_STRING);
	$('#controlBranchOffice_error').html(EMPTY_STRING);
	$('#controlBranchOffice_desc').html(EMPTY_STRING);
	$('#clusterCode').val(EMPTY_STRING);
	$('#clusterCode_error').html(EMPTY_STRING);
	$('#clusterCode_desc').html(EMPTY_STRING);
	$('#officeCloseDate').val(EMPTY_STRING);
	$('#officeCloseDate_error').html(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
}

function doHelp(id) {
	switch (id) {
	case 'entityBranchCode':
		help('COMMON', 'HLP_OFFICE_CODE_M', $('#entityBranchCode').val(), EMPTY_STRING, $('#entityBranchCode'));
		break;
	case 'geoUnitStateCode':
		help('COMMON', 'HLP_GEOUNIT_STATECODE', $('#geoUnitStateCode').val(), EMPTY_STRING, $('#geoUnitStateCode'));
		break;
	case 'organizationUnit':
		help('COMMON', 'HLP_ORG_UNIT_TYPE', $('#organizationUnit').val(), EMPTY_STRING, $('#organizationUnit'));
		break;
	case 'controlBranchOffice':
		help('COMMON', 'HLP_OFFICE_CODE', $('#controlBranchOffice').val(), EMPTY_STRING, $('#controlBranchOffice'));
		break;
	case 'clusterCode':
		help('COMMON', 'HLP_ACCNT_CLUSTER_CODE', $('#clusterCode').val(), EMPTY_STRING, $('#clusterCode'));
		break;
	}
}

function add() {
	$('#entityBranchCode').focus();
	$('#entityCode').val(getEntityCode());
	$('#entityCode_desc').html(getEntityName());
	setCheckbox('enabled', YES);
	$('#enabled').prop('disabled', true);
}

function modify() {
	$('#entityBranchCode').focus();
	$('#entityCode').val(getEntityCode());
	$('#entityCode_desc').html(getEntityName());
	if ($('#action').val() == MODIFY) {
		$('#enabled').prop('disabled', false);
	} else {
		$('#enabled').prop('disabled', true);
	}
}

function loadData() {
	$('#entityCode').val(getEntityCode());
	$('#entityCode_desc').html(getEntityName());
	$('#entityBranchCode').val(validator.getValue('BRANCH_CODE'));
	$('#description').val(validator.getValue('DESCRIPTION'));
	$('#conciseDescription').val(validator.getValue('CONCISE_DESCRIPTION'));
	$('#officeOpenDate').val(validator.getValue('OPEN_ON_DATE'));
	$('#branchDetails').val(validator.getValue('BRANCH_DETAILS'));
	$('#geoUnitStateCode').val(validator.getValue('GEO_UNIT_STATE_CODE'));
	$('#geoUnitStateCode_desc').html(validator.getValue('F1_DESCRIPTION'));
	$('#organizationUnit').val(validator.getValue('TYPE_OF_ORG_UNIT'));
	$('#organizationUnit_desc').html(validator.getValue('F2_DESCRIPTION'));
	$('#controlBranchOffice').val(validator.getValue('CONTROLLING_OFFICE_CODE'));
	$('#controlBranchOffice_desc').html(validator.getValue('F4_DESCRIPTION'));
	$('#clusterCode').val(validator.getValue('CLUSTER_CODE'));
	$('#clusterCode_desc').html(validator.getValue('F3_DESCRIPTION'));
	$('#officeCloseDate').val(validator.getValue('CLOSED_ON_DATE'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	resetLoading();
}

function view(source, primaryKey) {
	hideParent('cmn/qentitybranch', source, primaryKey);
	resetLoading();
}

function validate(id) {
	switch (id) {
	case 'entityBranchCode':
		entityBranchCode_val();
		break;
	case 'description':
		description_val();
		break;
	case 'conciseDescription':
		conciseDescription_val();
		break;
	case 'officeOpenDate':
		officeOpenDate_val();
		break;
	case 'branchDetails':
		branchDetails_val();
		break;
	case 'geoUnitStateCode':
		geoUnitStateCode_val();
		break;
	case 'organizationUnit':
		organizationUnit_val();
		break;
	case 'controlBranchOffice':
		controlBranchOffice_val();
		break;
	case 'clusterCode':
		clusterCode_val();
		break;
	case 'officeCloseDate':
		officeCloseDate_val();
		break;
	case 'enabled':
		setFocusLast('remarks');
		break;
	case 'remarks':
		remarks_val();
		break;
	}
}

function backtrack(id) {
	switch (id) {
	case 'entityBranchCode':
		setFocusLast('entityBranchCode');
		break;
	case 'description':
		setFocusLast('entityBranchCode');
		break;
	case 'conciseDescription':
		setFocusLast('description');
		break;
	case 'branchDetails':
		setFocusLast('conciseDescription');
		break;
	case 'geoUnitStateCode':
		setFocusLast('branchDetails');
		break;
	case 'organizationUnit':
		setFocusLast('geoUnitStateCode');
		break;
	case 'controlBranchOffice':
		setFocusLast('organizationUnit');
		break;
	case 'clusterCode':
		if (!isEmpty($('#controlBranchOffice').val()))
			setFocusLast('controlBranchOffice');
		else
			setFocusLast('organizationUnit');
		break;
	case 'enabled':
		setFocusLast('clusterCode');
		break;
	case 'remarks':
		if ($('#action').val() == MODIFY) {
			setFocusLast('enabled');
		} else if (isEmpty($('#controlBranchOffice').val() && isEmpty($('#clusterCode').val()))) {
			setFocusLast('organizationUnit');
		} else if (isEmpty($('#clusterCode').val())) {
			setFocusLast('controlBranchOffice');
		} else {
			setFocusLast('clusterCode');
		}
		break;
	}
}

function entityBranchCode_val() {
	var value = $('#entityBranchCode').val();
	$('#entityBranchCode_desc').html(EMPTY_STRING);
	clearError('entityBranchCode_error');
	if ($('#action').val() == ADD) {
		if (isEmpty(value)) {
			setError('entityBranchCode_error', MANDATORY);
			return false;
		}
		if (!isNumeric(value)) {
			setError('entityBranchCode_error', INVALID_NUMBER);
			return false;
		}
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('BRANCH_CODE', $('#entityBranchCode').val());
		validator.setValue(ACTION, $('#action').val());
		validator.setClass('patterns.config.web.forms.cmn.mentitybranchbean');
		validator.setMethod('validateEntityBranchCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('entityBranchCode_error', validator.getValue(ERROR));
			$('#entityBranchCode_desc').html(EMPTY_STRING);
			return false;
		}
	} else if ($('#action').val() == MODIFY) {
		PK_VALUE = getEntityCode() + PK_SEPERATOR + $('#entityBranchCode').val();
		if (!loadPKValues(PK_VALUE, 'entityBranchCode_error')) {
			$('#entityBranchCode_desc').html(EMPTY_STRING);
			return false;
		}
	}
	setFocusLast('description');
	return true;
}

function description_val() {
	var value = $('#description').val();
	clearError('description_error');
	if (isEmpty(value)) {
		setError('description_error', MANDATORY);
		return false;
	}
	if (!isValidDescription(value)) {
		setError('description_error', INVALID_DESCRIPTION);
		return false;
	}
	setFocusLast('conciseDescription');
	return true;
}

function conciseDescription_val() {
	var value = $('#conciseDescription').val();
	clearError('conciseDescription_error');
	if (isEmpty(value)) {
		setError('conciseDescription_error', MANDATORY);
		return false;
	}
	if (!isValidConciseDescription(value)) {
		setError('conciseDescription_error', INVALID_CONCISE_DESCRIPTION);
		return false;
	}
	setFocusLast('officeOpenDate');
	return true;
}

function officeOpenDate_val() {
	var value = $('#officeOpenDate').val();
	clearError('officeOpenDate_error');
	if (isEmpty(value)) {
		$('#officeOpenDate').val(cbdval);
		value = $('#officeOpenDate').val();
	}
	if (!isDate(value)) {
		setError('officeOpenDate_error', INVALID_DATE);
		return false;
	}
	if (isDateGreater(value, getCBD())) {
		setError('officeOpenDate_error', DATE_LCBD);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('ENTITY_CODE', getEntityCode());
	validator.setValue('OPEN_ON_DATE', $('#officeOpenDate').val());
	validator.setValue(ACTION, $('#action').val());
	validator.setClass('patterns.config.web.forms.cmn.mentitybranchbean');
	validator.setMethod('validateOfficeOpenDate');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('officeOpenDate_error', validator.getValue(ERROR));
		return false;
	}
	setFocusLast('branchDetails');
	return true;
}

function branchDetails_val() {
	var value = $('#branchDetails').val();
	clearError('branchDetails_error');
	if (isEmpty(value)) {
		setError('branchDetails_error', MANDATORY);
		return false;
	}
	if (isWhitespace(value)) {
		setError('branchDetails_error', INVALID_ADDRESS);
		return false;
	}
	if (!isValidRemarks(value)) {
		setError('branchDetails_error', INVALID_ADDRESS);
		return false;
	}
	setFocusLast('geoUnitStateCode');
	return true;
}

function geoUnitStateCode_val() {
	var value = $('#geoUnitStateCode').val();
	$('#geoUnitStateCode_desc').html(EMPTY_STRING);
	clearError('geoUnitStateCode_error');
	if (isEmpty(value)) {
		setError('geoUnitStateCode_error', MANDATORY);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue(ACTION, USAGE);
	validator.setValue('GEO_UNIT_ID', $('#geoUnitStateCode').val());
	validator.setClass('patterns.config.web.forms.cmn.mentitybranchbean');
	validator.setMethod('validateGeoUnitStateCode');
	validator.sendAndReceive();
	if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		$('#geoUnitStateCode_desc').html(validator.getValue('DESCRIPTION'));
	} else if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('geoUnitStateCode_error', validator.getValue(ERROR));
		$('#geoUnitStateCode_desc').html(EMPTY_STRING);
		return false;
	}
	setFocusLast('organizationUnit');
	return true;
}

function organizationUnit_val() {
	var value = $('#organizationUnit').val();
	$('#organizationUnit_desc').html(EMPTY_STRING);
	clearError('organizationUnit_error');
	if (isEmpty(value)) {
		setError('organizationUnit_error', MANDATORY);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue(ACTION, USAGE);
	validator.setValue('ORG_UNIT_TYPE', $('#organizationUnit').val());
	validator.setClass('patterns.config.web.forms.cmn.mentitybranchbean');
	validator.setMethod('validateOrganizationUnit');
	validator.sendAndReceive();
	if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		$('#organizationUnit_desc').html(validator.getValue('DESCRIPTION'));
	} else if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('organizationUnit_error', validator.getValue(ERROR));
		$('#organizationUnit_desc').html(EMPTY_STRING);
		return false;
	}
	setFocusLast('controlBranchOffice');
	return true;
}

function controlBranchOffice_val() {
	var value = $('#controlBranchOffice').val();
	var branch = $('#entityBranchCode').val();
	$('#controlBranchOffice_desc').html(EMPTY_STRING);
	clearError('controlBranchOffice_error');
	if (value == branch) {
		setError('controlBranchOffice_error', PBS_BRN_NOT_EQUAL_CONTROLBRN);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue(ACTION, USAGE);
	validator.setValue('CONTROLLING_OFFICE_CODE', $('#controlBranchOffice').val());
	validator.setValue('BRANCH_CODE', $('#entityBranchCode').val());
	validator.setClass('patterns.config.web.forms.cmn.mentitybranchbean');
	validator.setMethod('validateControlBranchOffice');
	validator.sendAndReceive();
	if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		if (validator.getValue("ROOT_BRANCH_EXIST") != EMPTY_STRING) {
			alert(validator.getValue("ROOT_BRANCH_EXIST"));
		} else {
			$('#controlBranchOffice_desc').html(validator.getValue('DESCRIPTION'));
		}
	} else if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('controlBranchOffice_error', validator.getValue(ERROR));
		$('#controlBranchOffice_desc').html(EMPTY_STRING);
		return false;
	}
	setFocusLast('clusterCode');
	return true;
}

function clusterCode_val() {
	var value = $('#clusterCode').val();
	clearError('clusterCode_error');
	$('#clusterCode_desc').html(EMPTY_STRING);
	if (!isEmpty(value)) {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue(ACTION, USAGE);
		validator.setValue('ACNT_CLUSTER_CODE', $('#clusterCode').val());
		validator.setClass('patterns.config.web.forms.cmn.mentitybranchbean');
		validator.setMethod('validateClusterCode');
		validator.sendAndReceive();
		if (validator.getValue(RESULT) == DATA_AVAILABLE) {
			$('#clusterCode_desc').html(validator.getValue('DESCRIPTION'));
		} else if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('clusterCode_error', validator.getValue(ERROR));
			$('#clusterCode_desc').html(EMPTY_STRING);
			return false;
		}
	}
	setFocusLast('officeCloseDate');
	return true;
}

function officeCloseDate_val() {
	var openDate = $('#officeOpenDate').val();
	var value = $('#officeCloseDate').val();
	clearError('officeCloseDate_error');
	if (!isEmpty(value)) {
		if (!isDate(value)) {
			setError('officeCloseDate_error', INVALID_DATE);
			return false;
		}
		if (isDateGreater(value, getCBD())) {
			setError('officeCloseDate_error', DATE_LCBD);
			return false;
		}
		if (isDateLesser(value, openDate)) {
			setError('officeCloseDate_error', PBS_DATE_GCOD);
			return false;
		}
	}
	if ($('#action').val() == ADD) {
		setFocusLast('remarks');
	} else {
		setFocusLast('enabled');
	}
	return true;
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', INVALID_REMARKS);
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', INVALID_REMARKS);
			return false;
		}
	}
	if ($('#action').val() == MODIFY) {
		if (isEmpty(value)) {
			setError('remarks_error', MANDATORY);
			return false;
		}

	}
	setFocusOnSubmit();
	return true;
}
