<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.cmn.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/cmn/qtax.js" />
	</web:dependencies>
	<web:loadPK />
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="qtax.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:viewContent id="po_view1">
				<web:dividerBlock>
					<web:sectionBlock width="width-100p" align="left">
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="220px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="mtax.statecode" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:stateCodeDisplay property="stateCode" id="stateCode" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="220px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowOdd>
									<web:column>
										<web:legend key="mtax.taxcode" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:taxCodeDisplay property="taxCode" id="taxCode" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="form.description" var="common" mandatory="true" />
									</web:column>
									<web:column>
										<type:descriptionDisplay property="description" id="description" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="form.conciseDescription" var="common" mandatory="true" />
									</web:column>
									<web:column>
										<type:conciseDescriptionDisplay property="conciseDescription" id="conciseDescription" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="mtax.taxtype" var="program" />
									</web:column>
									<web:column>
										<type:comboDisplay property="taxType" id="taxType" datasourceid="MTAX_TAX_TYPE" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="mtax.taxon" var="program" />
									</web:column>
									<web:column>
										<type:comboDisplay property="taxOn" id="taxOn" datasourceid="MTAX_TAX_ON" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="mtax.taxontaxcode" var="program" />
									</web:column>
									<web:column>
										<type:taxCodeDisplay property="taxOnTaxCode" id="taxOnTaxCode" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="mtax.taxipglaccthead" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:GLHeadCodeDisplay property="taxIpGLAcctHead" id="taxIpGLAcctHead" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="mtax.taxpayglacctHead" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:GLHeadCodeDisplay property="taxPayGLAcctHead" id="taxPayGLAcctHead" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="mtax.nooftaxschedules" var="program" />
									</web:column>
									<web:column>
										<type:serialPlaceHolderDisplay property="noOfTaxSchedules" id="noOfTaxSchedules" />
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="220px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:viewTitle var="program" key="mtax.section" />
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:rowEven>
									<web:column>
										<web:grid height="200px" width="210px" id="tax_innerGrid" src="cmn/mtax_innerGrid.xml">
										</web:grid>
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="220px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="mtax.prdctwisetaxinclusive" var="program" />
										</web:column>
										<web:column>
											<type:checkboxDisplay property="prdctWiseTaxInclusive" id="prdctWiseTaxInclusive" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="mtax.cstmerwisetaxinclusive" var="program" />
										</web:column>
										<web:column>
											<type:checkboxDisplay property="cstmerWiseTaxInclusive" id="cstmerWiseTaxInclusive" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="mtax.leasewisetaxinclusive" var="program" />
										</web:column>
										<web:column>
											<type:checkboxDisplay property="leaseWiseTaxInclusive" id="leaseWiseTaxInclusive" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="220px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowOdd>
									<web:column>
										<web:legend key="form.IsUseEnabled" var="common" />
									</web:column>
									<web:column>
										<type:checkboxDisplay property="enabled" id="enabled" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="form.remarks" var="common" />
									</web:column>
									<web:column>
										<type:remarksDisplay property="remarks" id="remarks" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:auditDisplay />
					</web:sectionBlock>
				</web:dividerBlock>
			</web:viewContent>
		</web:viewSubPart>
	</web:viewPart>
</web:fragment>