<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<web:bundle baseName="patterns.config.web.forms.cmn.Messages"
	var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/cmn/qempdb.js" />
	</web:dependencies>
	<web:loadPK />
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="qempdb.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:viewContent id="po_view1">
				<web:dividerBlock>
					<web:sectionBlock width="width-100p" align="left">
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="180px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="mempdb.empcode" var="program"
											mandatory="true" />
									</web:column>
									<web:column>
										<type:employeeCodeDisplay property="employeeCode"
											id="employeeCode" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="180px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowOdd>
									<web:column>
										<web:legend key="mempdb.name" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:generalTitleNameDisplay property="employee"
											id="employee"/>
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="mempdb.gender" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:comboDisplay property="gender" id="gender" datasourceid="COMMON_GENDER" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="mempdb.dateofbirth" var="program"
											mandatory="true" />
									</web:column>
									<web:column>
										<type:dateDisplay property="dateOfBirth" id="dateOfBirth" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="mempdb.relationship" var="program"
											mandatory="true" />
									</web:column>
									<web:column>
										<type:comboDisplay property="relationShip" id="relationShip" datasourceid="COMMON_RELATION_SHIP" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="mempdb.relationname" var="program"
											mandatory="true" />
									</web:column>
									<web:column>
										<type:descriptionDisplay property="relationName"
											id="relationName" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="mempdb.dateofjoining" var="program"
											mandatory="true" />
									</web:column>
									<web:column>
										<type:dateDisplay property="dateOfJoining" id="dateOfJoining" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="mempdb.dateofretirement" var="program" />
									</web:column>
									<web:column>
										<type:dateDisplay property="dateOfRetirement"
											id="dateOfRetirement" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="mempdb.pfavailable" var="program" />
									</web:column>
									<web:column>
										<type:checkboxDisplay property="provideFundApplicable"
											id="provideFundApplicable" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="mempdb.pfnumber" var="program" />
									</web:column>
									<web:column>
										<type:otherInformation25Display property="pfNumber"
											id="pfNumber" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="mempdb.personfundrequired" var="program" />
									</web:column>
									<web:column>
										<type:checkboxDisplay property="personFundRequired"
											id="personFundRequired" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="mempdb.eligibleesi" var="program" />
									</web:column>
									<web:column>
										<type:checkboxDisplay property="eligibleEsi" id="eligibleEsi" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="mempdb.employeeesinumber" var="program" />
									</web:column>
									<web:column>
										<type:otherInformation25Display property="esiNumber"
											id="esiNumber" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="mempdb.qualification" var="program"
											mandatory="true" />
									</web:column>
									<web:column>
										<type:descriptionDisplay property="qualification"
											id="qualification" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="mempdb.emaild" var="program" />
									</web:column>
									<web:column>
										<type:emailDisplay property="emailId" id="emailId" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="mempdb.mobileno" var="program" />
									</web:column>
									<web:column>
										<type:mobileDisplay property="mobileNumber" id="mobileNumber" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="form.remarks" var="common" />
									</web:column>
									<web:column>
										<type:remarksDisplay property="remarks" id="remarks" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:auditDisplay />
					</web:sectionBlock>
				</web:dividerBlock>
			</web:viewContent>
		</web:viewSubPart>
	</web:viewPart>
</web:fragment>
