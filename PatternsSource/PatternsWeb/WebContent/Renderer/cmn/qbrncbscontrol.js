var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'IBRNCBSCONTROL';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}
function clearFields() {
	$('#entityOffCode').val(EMPTY_STRING);
	$('#inCbsDate').val(EMPTY_STRING);
	$('#transDate').val(EMPTY_STRING);
	$('#currCbsLink').val(EMPTY_STRING);
	setCheckbox('enabled', NO);
	$('#remarks').val(EMPTY_STRING);
}
function loadData() {
	$('#entityOffCode').val(validator.getValue('BRANCH_CODE'));
	$('#entityOffCode_desc').html(validator.getValue('F1_DESCRIPTION'));
	$('#inCbsDate').val(validator.getValue('IN_CBS_FROM_DATE'));
	$('#transDate').val(validator.getValue('TRAN_MIGRATED_FROM_DATE'));
	$('#currCbsLink').val(validator.getValue('CURRENT_CBS_LINK_STATUS'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadAuditFields(validator);
}
