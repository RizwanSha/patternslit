var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MSUBGLHEAD';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}
function clearFields() {
	$('#glHeadCode').val(EMPTY_STRING);
	$('#glHeadCode_desc').html(EMPTY_STRING);
	$('#glHeadCode_error').html(EMPTY_STRING);
	$('#subGlHeadCode').val(EMPTY_STRING);
	$('#subGlHeadCode_desc').html(EMPTY_STRING);
	$('#subGlHeadCode_error').html(EMPTY_STRING);
	clearNonPKFields();
}

function clearNonPKFields() {
	$('#description').val(EMPTY_STRING);
	$('#description_error').html(EMPTY_STRING);
	$('#conciseDescription').val(EMPTY_STRING);
	$('#conciseDescription_error').html(EMPTY_STRING);
	$('#custSegCode').val(EMPTY_STRING);
	$('#custSegCode_error').html(EMPTY_STRING);
	$('#portCode').val(EMPTY_STRING);
	$('#portCode_error').html(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
	setCheckbox('enabled', YES);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
}

function loadData() {
	$('#glHeadCode').val(validator.getValue('GL_HEAD_CODE'));
	$('#glHeadCode_desc').html(validator.getValue('F1_DESCRIPTION'));
	$('#subGlHeadCode').val(validator.getValue('SUB_GL_HEAD_CODE'));
	$('#description').val(validator.getValue('DESCRIPTION'));
	$('#conciseDescription').val(validator.getValue('CONCISE_DESCRIPTION'));
	$('#custSegCode').val(validator.getValue('CUST_SEGMENT_CODE'));
	$('#custSegCode_desc').html(validator.getValue('F2_DESCRIPTION'));
	$('#portCode').val(validator.getValue('PORTFOLIO_CODE'));
	$('#portCode_desc').html(validator.getValue('F3_DESCRIPTION'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadAuditFields(validator);
	resetLoading();
}
