<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.cmn.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/cmn/mtax.js" />
	</web:dependencies>
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="mtax.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/cmn/mtax" id="mtax" method="POST">
				<web:dividerBlock>
					<type:optionBox add="true" modify="true" view="true" />
				</web:dividerBlock>
				<web:viewContent id="po_view1" styleClass="hidden">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="230px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="mtax.statecode" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:stateCode property="stateCode" id="stateCode" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="230px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="mtax.taxcode" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:taxCode property="taxCode" id="taxCode" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="form.description" var="common" mandatory="true" />
										</web:column>
										<web:column>
											<type:description property="description" id="description" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="form.conciseDescription" var="common" mandatory="true" />
										</web:column>
										<web:column>
											<type:conciseDescription property="conciseDescription" id="conciseDescription" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="mtax.taxtype" var="program" />
										</web:column>
										<web:column>
											<type:combo property="taxType" id="taxType" datasourceid="MTAX_TAX_TYPE" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="mtax.taxon" var="program" />
										</web:column>
										<web:column>
											<type:combo property="taxOn" id="taxOn" datasourceid="MTAX_TAX_ON" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="mtax.taxontaxcode" var="program" />
										</web:column>
										<web:column>
											<type:taxCode property="taxOnTaxCode" id="taxOnTaxCode" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="mtax.taxipglaccthead" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:GLHeadCode property="taxIpGLAcctHead" id="taxIpGLAcctHead" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="mtax.taxpayglacctHead" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:GLHeadCode property="taxPayGLAcctHead" id="taxPayGLAcctHead" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="mtax.nooftaxschedules" var="program" />
										</web:column>
										<web:column>
											<type:serialPlaceHolder property="noOfTaxSchedules" id="noOfTaxSchedules" key="form.text1to99" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:viewTitle var="program" key="mtax.section" />
							<web:viewContent id="viewTaxscheduleCode">
								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="270px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowEven>
											<web:column>
												<web:legend key="mtax.taxschedulecode" var="program" mandatory="true" />
											</web:column>
											<web:column>
												<type:scheduleCode property="taxScheduleCode" id="taxScheduleCode" />
											</web:column>
										</web:rowEven>
										<web:rowOdd>
											<web:column>	
												<web:gridToolbar gridName="tax_innerGrid" cancelFunction="tax_cancelGrid" editFunction="tax_editGrid" addFunction="tax_addGrid" insertAtFirsstNotReq="false" />
												<web:element property="xmlTax" />
											</web:column>
										</web:rowOdd>
									</web:table>
								</web:section>
							</web:viewContent>
							<web:section>
								<web:table>
									<web:rowEven>
										<web:column>
											<web:grid height="200px" width="300px" id="tax_innerGrid" src="cmn/mtax_innerGrid.xml">
											</web:grid>
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="230px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="mtax.prdctwisetaxinclusive" var="program" />
										</web:column>
										<web:column>
											<type:checkbox property="prdctWiseTaxInclusive" id="prdctWiseTaxInclusive" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="mtax.cstmerwisetaxinclusive" var="program" />
										</web:column>
										<web:column>
											<type:checkbox property="cstmerWiseTaxInclusive" id="cstmerWiseTaxInclusive" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="mtax.leasewisetaxinclusive" var="program" />
										</web:column>
										<web:column>
											<type:checkbox property="leaseWiseTaxInclusive" id="leaseWiseTaxInclusive" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
								<web:columnGroup>
										<web:columnStyle width="230px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="form.IsUseEnabled" var="common" />
										</web:column>
										<web:column>
											<type:checkbox property="enabled" id="enabled" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="form.remarks" var="common" />
										</web:column>
										<web:column>
											<type:remarks property="remarks" id="remarks" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column span="2">
											<web:submitReset />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
						</web:sectionBlock>
					</web:dividerBlock>
				</web:viewContent>
				<web:element property="taxScheduleCodeTaxOnTaxCode" />
				<web:element property="installCenteralCode" />
				<web:element property="command" />
				<web:element property="action" />
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
	<web:queryGrid />
	<web:TBAGrid />
	<web:contextSearch />
</web:fragment>