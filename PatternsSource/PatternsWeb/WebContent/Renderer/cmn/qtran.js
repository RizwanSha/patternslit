var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MTRAN';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}

function clearFields() {
	$('#transactionCode').val(EMPTY_STRING);
	$('#transactionCode_desc').html(EMPTY_STRING);
	$('#transactionCode_error').html(EMPTY_STRING);
	clearNonPKFields();
}

function clearNonPKFields() {
	$('#description').val(EMPTY_STRING);
	$('#description_error').html(EMPTY_STRING);
	$('#conciseDescription').val(EMPTY_STRING);
	$('#conciseDescription_error').html(EMPTY_STRING);

	$('#alphaAccessCode').val(EMPTY_STRING);
	$('#alphaAccessCode_error').html(EMPTY_STRING);
	$('#transactionMode').val($("#transactionMode option:first-child").val());
	$('#transactionMode_error').html(EMPTY_STRING);
	$('#depositWithdrawal').val($("#depositWithdrawal option:first-child").val());
	$('#depositWithdrawal_error').html(EMPTY_STRING);
	$('#transactionUsingDocument').val($("#transactionUsingDocument option:first-child").val());
	$('#transactionUsingDocument_error').html(EMPTY_STRING);
	$('#slipNumberRequired').prop('disabled', true);
	setCheckbox('slipNumberRequired', NO);
	$('#slipNumberRequired_error').html(EMPTY_STRING);
	$('#numberOfLegsInTheTransaction').val($("#numberOfLegsInTheTransaction option:first-child").val());
	$('#numberOfLegsInTheTransaction_error').html(EMPTY_STRING);
	setCheckbox('enabled', YES);
	$('#enabled_error').html(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
}
function loadData() {
	$('#transactionCode').val(validator.getValue('TRAN_CODE'));
	$('#description').val(validator.getValue('DESCRIPTION'));
	$('#conciseDescription').val(validator.getValue('CONCISE_DESCRIPTION'));
	$('#alphaAccessCode').val(validator.getValue('TRAN_ACCESS_CODE'));
	$('#transactionMode').val(validator.getValue('TRAN_MODE'));
	$('#depositWithdrawal').val(validator.getValue('TRAN_TYPE'));
	$('#transactionUsingDocument').val(validator.getValue('TRAN_DOC'));
	setCheckbox('slipNumberRequired', validator.getValue('SLIP_NUM_REQUIRED'));
	$('#numberOfLegsInTheTransaction').val(validator.getValue('TRAN_LEGS'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadAuditFields(validator);
}

function loadGrid() {
	SOURCE_VALUE = _tableType;
	PK_VALUE = _primaryKey;
	if (_tableType == MAIN) {
		loads();
	} else if (_tableType == TBA) {
		loads();
	}
}