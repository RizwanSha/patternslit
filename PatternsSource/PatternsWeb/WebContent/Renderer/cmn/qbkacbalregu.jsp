<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.cmn.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/cmn/qbkacbalregu.js" />
	</web:dependencies>
	<web:loadPK />
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="qbkacbalregu.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:viewContent id="po_view1">
				<web:dividerBlock>
					<web:sectionBlock width="width-100p" align="left">
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="240px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="ibkacbalregu.bankLicenseType" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:bankLicenseTypeDisplay property="bankLicenseType" id="bankLicenseType" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="ibkacbalregu.typeOfProduct" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:comboDisplay property="typeOfProduct" id="typeOfProduct" datasourceid="IBKACBALREGU_PROD_TYPE" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="ibkacbalregu.ccyCode" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:currencyDisplay property="ccyCode" id="ccyCode" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="form.effectivedate" var="common" mandatory="true" />
									</web:column>
									<web:column>
										<type:dateDisplay property="effectiveDate" id="effectiveDate" />
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
						<web:viewTitle var="program" key="ibkacbalregu.section" />
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="240px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="ibkacbalregu.dayBalRestriction" var="program" />
									</web:column>
									<web:column>
										<type:checkboxDisplay property="dayBalRestriction" id="dayBalRestriction" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="ibkacbalregu.maxallwDayEnd" var="program" />
									</web:column>
									<web:column>
										<type:currencyBigAmountDisplay property="maxallwDayEnd" id="maxallwDayEnd" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="ibkacbalregu.perBalRestriction" var="program" />
									</web:column>
									<web:column>
										<type:checkboxDisplay property="perBalRestriction" id="perBalRestriction" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="ibkacbalregu.maxallwperTrans" var="program" />
									</web:column>
									<web:column>
										<type:currencyBigAmountDisplay property="maxallwperTrans" id="maxallwperTrans" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="form.enabled" var="common" />
									</web:column>
									<web:column>
										<type:checkboxDisplay property="enabled" id="enabled" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="form.remarks" var="common" />
									</web:column>
									<web:column>
										<type:remarksDisplay property="remarks" id="remarks" />
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
						<web:auditDisplay />
					</web:sectionBlock>
				</web:dividerBlock>
			</web:viewContent>
		</web:viewSubPart>
	</web:viewPart>
</web:fragment>