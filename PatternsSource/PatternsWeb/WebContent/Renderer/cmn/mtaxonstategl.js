var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MTAXONSTATEGL';
function init() {
	refreshTBAGrid();
	refreshQueryGrid();
	if (_redisplay) {
		if ($('#action').val() == ADD) {
			$('#enabled').prop('disabled', true);
			setCheckbox('enabled', YES);
		}
	}
}
function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'MTAXONSTATEGL_MAIN', queryGrid);
}
function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}
function clearFields() {
	$('#stateCode').val(EMPTY_STRING);
	$('#stateCode_desc').html(EMPTY_STRING);
	$('#stateCode_error').html(EMPTY_STRING);
	$('#taxCode').val(EMPTY_STRING);
	$('#taxCode_desc').html(EMPTY_STRING);
	$('#taxCode_error').html(EMPTY_STRING);
	$('#taxonStateCode').val(EMPTY_STRING);
	$('#taxonStateCode_desc').html(EMPTY_STRING);
	$('#taxonStateCode_error').html(EMPTY_STRING);
	clearNonPKFields();
}
function clearNonPKFields() {
	$('#taxPayGLAcctHead').val(EMPTY_STRING);
	$('#taxPayGLAcctHead_desc').html(EMPTY_STRING);
	$('#taxPayGLAcctHead_error').html(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
}
function doclearfields(id) {
	switch (id) {
	case 'stateCode':
		if (isEmpty($('#stateCode').val())) {
			clearFields();
			break;

		}
	case 'taxCode':
		if (isEmpty($('#taxCode').val())) {
			clearNonPKFields();
			break;
		}
	case 'taxonStateCode':
		if (isEmpty($('#taxonStateCode').val())) {
			clearNonPKFields();
			break;
		}
	}
}
function add() {
	$('#taxCode').focus();
	stateCode_val();
	setCheckbox('enabled', YES);
	$('#enabled').prop('disabled', true);
}
function modify() {
	$('#taxCode').focus();
	stateCode_val();
	if ($('#action').val() == MODIFY) {
		$('#enabled').prop('disabled', false);
	} else {
		$('#enabled').prop('disabled', true);
	}
}
function view(source, primaryKey) {
	hideParent('cmn/qtaxonstategl', source, primaryKey);
	resetLoading();
}
function loadData() {
	$('#stateCode').val(validator.getValue('STATE_CODE'));
	$('#stateCode_desc').html(validator.getValue('F1_DESCRIPTION'));
	$('#taxCode').val(validator.getValue('TAX_CODE'));
	$('#taxCode_desc').html(validator.getValue('F2_DESCRIPTION'));
	$('#taxonStateCode').val(validator.getValue('TAX_ON_STATE_CODE'));
	$('#taxonStateCode_desc').html(validator.getValue('F3_DESCRIPTION'));
	$('#taxPayGLAcctHead').val(validator.getValue('TAX_PAY_GL_HEAD_CODE'));
	$('#taxPayGLAcctHead_desc').html(validator.getValue('F4_DESCRIPTION'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	resetLoading();
}
function backtrack(id) {
	switch (id) {
	case 'taxCode':
		setFocusLast('taxCode');
		break;
	case 'taxonStateCode':
		setFocusLast('taxCode');
		break;
	case 'taxPayGLAcctHead':
		setFocusLast('taxonStateCode');
		break;
	case 'enabled':
		setFocusLast('taxPayGLAcctHead');
		break;
	case 'remarks':
		if ($('#action').val() == MODIFY) {
			setFocusLast('enabled');
		} else {
			setFocusLast('taxPayGLAcctHead');
		}
		break;
	}
}
function doHelp(id) {
	switch (id) {
	case 'taxCode':
		if (!isEmpty(stateCode))
			help('COMMON', 'HLP_CEN_TAX_CODE', $('#taxCode').val(), $('#stateCode').val(), $('#taxCode'));
		break;
	case 'taxonStateCode':
		help('MTAXONSTATEGL', 'HLP_STATE_ONLY', $('#taxonStateCode').val(), $('#stateCode').val(), $('#taxonStateCode'));
		break;
	case 'taxPayGLAcctHead':
		help('COMMON', 'HLP_GL_HEAD_CODE', $('#taxPayGLAcctHead').val(), EMPTY_STRING, $('#taxPayGLAcctHead'));
		break;
	}
}
function validate(id) {
	switch (id) {
	case 'taxCode':
		taxCode_val();
		break;
	case 'taxonStateCode':
		taxonStateCode_val();
		break;
	case 'taxPayGLAcctHead':
		taxPayGLAcctHead_val();
		break;
	case 'enabled':
		enabled_val();
		break;
	case 'remarks':
		remarks_val();
		break;
	}
}
function stateCode_val() {
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue(ACTION, USAGE);
	validator.setClass('patterns.config.web.forms.cmn.mtaxonstateglbean');
	validator.setMethod('validateStateCode');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('stateCode_error', validator.getValue(ERROR));
		return false;
	} else {
		$('#stateCode').val(validator.getValue('CENTRAL_CODE'));
		$('#stateCode_desc').html(validator.getValue('DESCRIPTION'));
	}
	setFocusLast('taxCode');
	return true;
}

function taxCode_val() {
	var value = $('#taxCode').val();
	clearError('taxCode_error');
	if (isEmpty(value)) {
		setError('taxCode_error', MANDATORY);
		return false;
	} else if ($('#action').val() == ADD && !isValidCode(value)) {
		setError('taxCode_error', INVALID_FORMAT);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('STATE_CODE', $('#stateCode').val());
	validator.setValue('TAX_CODE', value);
	validator.setValue(ACTION, USAGE);
	validator.setClass('patterns.config.web.forms.cmn.mtaxonstateglbean');
	validator.setMethod('validateTaxCode');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('taxCode_error', validator.getValue(ERROR));
		return false;
	} else {
		$('#taxCode_desc').html(validator.getValue('DESCRIPTION'));
	}
	setFocusLast('taxonStateCode');
	return true;
}
function taxonStateCode_val() {
	var value = $('#taxonStateCode').val();
	clearError('taxonStateCode_error');
	if (isEmpty(value)) {
		setError('taxonStateCode_error', MANDATORY);
		return false;
	} else if ($('#action').val() == ADD && !isValidCode(value)) {
		setError('taxonStateCode_error', INVALID_FORMAT);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('STATE_CODE', $('#stateCode').val());
		validator.setValue('TAX_CODE', $('#taxCode').val());
		validator.setValue('TAX_ON_STATE_CODE', value);
		validator.setValue(ACTION, $('#action').val());
		validator.setClass('patterns.config.web.forms.cmn.mtaxonstateglbean');
		validator.setMethod('validateTaxOnStateCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('taxonStateCode_error', validator.getValue(ERROR));
			return false;
		} else {
			$('#taxonStateCode_desc').html(validator.getValue('DESCRIPTION'));
			if ($('#action').val() == MODIFY) {
				PK_VALUE = getEntityCode() + PK_SEPERATOR + $('#stateCode').val() + PK_SEPERATOR + $('#taxCode').val() + PK_SEPERATOR + $('#taxonStateCode').val();
				if (!loadPKValues(PK_VALUE, 'taxonStateCode_error')) {
					return false;
				}
			}
		}
		
	}
	setFocusLast('taxPayGLAcctHead');
	return true;
}

function taxPayGLAcctHead_val() {
	var value = $('#taxPayGLAcctHead').val();
	clearError('taxPayGLAcctHead_error');
	if (isEmpty(value)) {
		setError('taxPayGLAcctHead_error', MANDATORY);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('GL_HEAD_CODE', $('#taxPayGLAcctHead').val());
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.cmn.mtaxonstateglbean');
		validator.setMethod('validateTaxPayGLAcctHead');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('taxPayGLAcctHead_error', validator.getValue(ERROR));
			$('#taxPayGLAcctHead_desc').html(EMPTY_STRING);
			return false;
		}
		$('#taxPayGLAcctHead_desc').html(validator.getValue('DESCRIPTION'));
	}
	if ($('#action').val() == MODIFY) {
		setFocusLast('enabled');
	} else {
		setFocusLast('remarks');
	}
	return true;
}

function enabled_val() {
	if ($('#action').val() == MODIFY) {
		setFocusLast('remarks');
	}
	return true;
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', INVALID_REMARKS);
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', INVALID_REMARKS);
			return false;
		}
	}
	if ($('#action').val() == MODIFY) {
		if (isEmpty(value)) {
			setError('remarks_error', MANDATORY);
			return false;
		}
	}
	setFocusOnSubmit();
	return true;
}
