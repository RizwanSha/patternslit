<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.cmn.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/cmn/mindasset.js" />
	</web:dependencies>
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="mindasset.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/cmn/mindasset" id="mindasset" method="POST">
				<web:dividerBlock>
					<type:optionBox add="true" modify="true" view="true" />
				</web:dividerBlock>
				<web:viewContent id="po_view1" styleClass="hidden">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="225px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="mindasset.leasecode" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:code property="leaseCode" id="leaseCode" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="mindasset.agreeno" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:agreementNo property="agreeNo" id="agreeNo" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="mindasset.scheduleid" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:scheduleCode property="scheduleId" id="scheduleId" lookup="true" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="mindasset.assetsl" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:serial property="assetSl" id="assetSl" lookup="true" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="mindasset.compsl" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:serial property="compSl" id="compSl" lookup="true" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="225px" />
										<web:columnStyle width="225px" />
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="mindasset.qtysl" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:serial property="qtySl" id="qtySl" />
										</web:column>
										<web:column>
											<web:legend key="mindasset.qtyofasset" var="program" />
										</web:column>
										<web:column>
											<type:serialDisplay property="qtyOfAsset" id="qtyOfAsset" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="225px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="mindasset.indassetid" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:assetId property="indAssetId" id="indAssetId" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:viewTitle var="program" key="mindasset.details" />
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="225px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="mindasset.fieldid" var="program" />
										</web:column>
										<web:column>
											<type:fieldId property="fieldId" id="fieldId" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:viewContent id="fieldvalue" styleClass="hidden">
								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="225px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowEven>
											<web:column>
												<web:legend key="mindasset.fieldvalue" var="program" />
											</web:column>
											<web:column>
												<type:dynamicIntegerAndDecimal property="fieldValue" id="fieldValue" />
											</web:column>
										</web:rowEven>
									</web:table>
								</web:section>
							</web:viewContent>
							<web:viewContent id="fieldremarks" styleClass="hidden">
								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="225px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowOdd>
											<web:column>
												<web:legend key="mindasset.fieldvalue" var="program" />
											</web:column>
											<web:column>
												<type:remarks property="fieldRemarks" id="fieldRemarks" />
											</web:column>
										</web:rowOdd>
									</web:table>
								</web:section>
							</web:viewContent>
							<web:section>
								<web:table>
									<web:rowEven>
										<web:column span="2">
											<web:gridToolbar gridName="pgmGrid" cancelFunction="mindasset_cancelGrid" editFunction="mindasset_editGrid" addFunction="mindasset_addGrid" continuousEdit="true" />
											<web:element property="xmlGrid" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:grid height="250px" width="962px" id="pgmGrid" src="cmn/mindasset_Grid.xml">
											</web:grid>
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:viewContent id="enabledvalue" styleClass="hidden">
								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="225px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowEven>
											<web:column>
												<web:legend key="form.enabled" var="common" />
											</web:column>
											<web:column>
												<type:checkbox property="enabled" id="enabled" />
											</web:column>
										</web:rowEven>
									</web:table>
								</web:section>
							</web:viewContent>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="225px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="form.remarks" var="common" />
										</web:column>
										<web:column>
											<type:remarks property="remarks" id="remarks" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column span="2">
											<web:submitReset />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
						</web:sectionBlock>
					</web:dividerBlock>
				</web:viewContent>
				<web:element property="command" />
				<web:element property="action" />
				<web:element property="custIDHidden" />
				<web:element property="assetSerialAssetType" />
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
	<web:queryGrid />
	<web:TBAGrid />
	<web:contextSearch />
</web:fragment>
