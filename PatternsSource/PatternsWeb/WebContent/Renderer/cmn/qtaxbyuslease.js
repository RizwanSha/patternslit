var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'ITAXBYUSLEASE';

function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}

function clearFields() {
	$('#customerId').val(EMPTY_STRING);
	$('#customerId_desc').html(EMPTY_STRING);
	$('#customerName').val(EMPTY_STRING);
	$('#stateCode').val(EMPTY_STRING);
	$('#stateCode_desc').html(EMPTY_STRING);
	$('#taxCode').val(EMPTY_STRING);
	$('#taxCode_desc').html(EMPTY_STRING);
	$('#effectiveDate').val(EMPTY_STRING);
	$('#effectiveDate_desc').html(EMPTY_STRING);
	clearNonPKFields();
}
function clearNonPKFields() {
	$('#taxBorne').val(EMPTY_STRING);
	$('#accountingHead_desc').html(EMPTY_STRING);
	$('#accountingHead').val(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_curr').val(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
}
function loadData() {
	$('#lesseeCode').val(validator.getValue('LESSEE_CODE'));
	$('#customerId').val(validator.getValue('F4_CUSTOMER_ID'));
	$('#customerName').val(validator.getValue('F5_CUSTOMER_NAME'));
	$('#agreementNumber').val(validator.getValue('AGREEMENT_NO'));
	$('#scheduleId').val(validator.getValue('SCHEDULE_ID'));
	$('#stateCode').val(validator.getValue('STATE_CODE'));
	$('#stateCode_desc').html(validator.getValue('F1_DESCRIPTION'));
	$('#taxCode').val(validator.getValue('TAX_CODE'));
	$('#taxCode_desc').html(validator.getValue('F2_DESCRIPTION'));
	$('#effectiveDate').val(validator.getValue('EFF_DATE'));
	setCheckbox('taxBorne', validator.getValue('TAX_BY_US'));
	$('#accountingHead').val(validator.getValue('TAX_BYUS_GL_HEAD_CODE'));
	$('#accountingHead_desc').html(validator.getValue('F3_DESCRIPTION'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadAuditFields(validator);
}
