var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MASSETTYPE';

function init() {
	refreshTBAGrid();
	refreshQueryGrid();
	if (_redisplay) {
		if ($('#action').val() == ADD) {
			$('#enabled').prop('disabled', true);
			setCheckbox('enabled', YES);
		}
	}
}

function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'MASSETTYPE_MAIN');
}

function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}

function add() {
	$('#assetTypeCode').focus();
	setCheckbox('enabled', YES);
	$('#enabled').prop('disabled', true);
}

function modify() {
	$('#assetTypeCode').focus();
	if ($('#action').val() == MODIFY) {
		$('#enabled').prop('disabled', false);
	} else {
		$('#enabled').prop('disabled', true);
	}
}

function view(source, primaryKey) {
	hideParent('cmn/qassettype', source, primaryKey);
	resetLoading();
}

function clearFields() {
	$('#assetTypeCode').val(EMPTY_STRING);
	$('#assetTypeCode_error').html(EMPTY_STRING);
	$('#assetTypeCode_desc').html(EMPTY_STRING);
	clearNonPKFields();
}

function clearNonPKFields() {
	$('#description').val(EMPTY_STRING);
	$('#description_error').html(EMPTY_STRING);
	$('#conciseDescription').val(EMPTY_STRING);
	$('#conciseDescription_error').html(EMPTY_STRING);
	$('#carNonCar').prop('selectedIndex', 0);
	$('#assetModelNumber').prop('selectedIndex', 0);
	$('#assetConfig').prop('selectedIndex', 0);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
}
function doclearfields(id) {
	switch (id) {
	case 'assetTypeCode':
		if (isEmpty($('#assetTypeCode').val())) {
			clearFields();
			break;
		}
	}
}
function loadData() {
	$('#assetTypeCode').val(validator.getValue('ASSET_TYPE_CODE'));
	$('#description').val(validator.getValue('DESCRIPTION'));
	$('#conciseDescription').val(validator.getValue('CONCISE_DESCRIPTION'));
	$('#carNonCar').val(validator.getValue('AUTO_NON_AUTO'));
	$('#assetModelNumber').prop('selectedIndex', 0);
	$('#assetConfig').prop('selectedIndex', 0);
	$('#assetModelNumber').val(validator.getValue('MODEL_NO'));
	$('#assetConfig').val(validator.getValue('CONFIG_DTLS'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	resetLoading();
}

function doHelp(id) {
	switch (id) {
	case 'assetTypeCode':
		help('COMMON', 'HLP_ASSET_TYPE', $('#assetTypeCode').val(), EMPTY_STRING, $('#assetTypeCode'));
		break;
	}
}

function validate(id) {
	switch (id) {
	case 'assetTypeCode':
		assetTypeCode_val();
		break;
	case 'description':
		description_val();
		break;
	case 'conciseDescription':
		conciseDescription_val();
		break;
	case 'carNonCar':
		carNonCar_val();
		break;
	case 'assetModelNumber':
		assetModelNumber_val();
		break;
	case 'assetConfig':
		assetConfig_val();
		break;
	case 'enabled':
		enabled_val();
		break;
	case 'remarks':
		remarks_val();
		break;
	}
}

function backtrack(id) {
	switch (id) {
	case 'assetTypeCode':
		setFocusLast('assetTypeCode');
		break;
	case 'description':
		setFocusLast('assetTypeCode');
		break;
	case 'conciseDescription':
		setFocusLast('description');
		break;
	case 'carNonCar':
		setFocusLast('conciseDescription');
		break;
	case 'assetModelNumber':
		setFocusLast('carNonCar');
		break;
	case 'assetConfig':
		setFocusLast('assetModelNumber');
		break;
	case 'enabled':
		setFocusLast('assetConfig');
		break;
	case 'remarks':
		if ($('#action').val() == MODIFY) {
			setFocusLast('enabled');
		} else {
			setFocusLast('carNonCar');
		}
		break;
	}
}

function assetTypeCode_val() {
	var value = $('#assetTypeCode').val();
	clearError('assetTypeCode_error');
	if (isEmpty(value)) {
		setError('assetTypeCode_error', MANDATORY);
		return false;
	} else if ($('#action').val() == ADD && !isValidCode(value)) {
		setError('assetTypeCode_error', INVALID_FORMAT);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('ASSET_TYPE_CODE', value);
	validator.setValue(ACTION, $('#action').val());
	validator.setClass('patterns.config.web.forms.cmn.massettypebean');
	validator.setMethod('validateAssetTypeCode');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('assetTypeCode_error', validator.getValue(ERROR));
		$('#assetTypeCode_desc').html(EMPTY_STRING);
		return false;
	} else {
		if ($('#action').val() == MODIFY) {
			PK_VALUE = getEntityCode() + PK_SEPERATOR + $('#assetTypeCode').val();
			if (!loadPKValues(PK_VALUE, 'assetTypeCode_error')) {
				return false;
			}
		}
	}
	setFocusLast('description');
	return true;
}

function description_val() {
	var value = $('#description').val();
	clearError('description_error');
	if (isEmpty(value)) {
		setError('description_error', MANDATORY);
		return false;
	}
	if (!isValidDescription(value)) {
		setError('description_error', INVALID_DESCRIPTION);
		return false;
	}
	setFocusLast('conciseDescription');
	return true;
}

function conciseDescription_val() {
	var value = $('#conciseDescription').val();
	clearError('conciseDescription_error');
	if (isEmpty(value)) {
		setError('conciseDescription_error', MANDATORY);
		return false;
	}
	if (!isValidConciseDescription(value)) {
		setError('conciseDescription_error', INVALID_CONCISE_DESCRIPTION);
		return false;
	}
	setFocusLast('carNonCar');
	return true;
}

function carNonCar_val() {
	var value = $('#carNonCar').val();
	clearError('carNonCar_error');
	if (isEmpty(value)) {
		setError('carNonCar_error', MANDATORY);
		return false;
	}
	setFocusLast('assetModelNumber');
	return true;
}

function assetModelNumber_val() {
	var value = $('#assetModelNumber').val();
	clearError('assetModelNumber_error');
	if (isEmpty(value)) {
		setError('assetModelNumber_error', MANDATORY);
		return false;
	}
	setFocusLast('assetConfig');
	return true;
}

function assetConfig_val() {
	var value = $('#assetConfig').val();
	clearError('assetConfigr_error');
	if (isEmpty(value)) {
		setError('assetConfig_error', MANDATORY);
		return false;
	}
	if ($('#action').val() == MODIFY) {
		setFocusLast('enabled');
	} else {
		setFocusLast('remarks');
	}
}

function enabled_val() {
	if ($('#action').val() == MODIFY)
		setFocusLast('remarks');
	return true;
}
function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', INVALID_REMARKS);
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', INVALID_REMARKS);
			return false;
		}
	}
	if ($('#action').val() == MODIFY) {
		if (isEmpty(value)) {
			setError('remarks_error', MANDATORY);
			setFocusLast('remarks');
			return false;
		}
	}
	setFocusOnSubmit();
	return true;
}
