var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'ITAXSCHRATE';

function init() {
	refreshTBAGrid();
	refreshQueryGrid();
	if (_redisplay) {
		if ($('#action').val() == ADD) {
			$('#enabled').prop('disabled', true);
			setCheckbox('enabled', YES);
		}
		if (!isEmpty($('#taxCode').val())) {
			taxCode_val();
		}
		taxschrate_innerGrid.clearAll();
		if (!isEmpty($('#xmlTaxSchrate').val())) {
			taxschrate_innerGrid.loadXMLString($('#xmlTaxSchrate').val());
		}
	}
}

function loadGrid() {
	loadGridQuery(CURRENT_PROGRAM_ID, 'ITAXSCHRATE_GRID', taxschrate_innerGrid);
}
function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'ITAXSCHRATE_MAIN', queryGrid);
}

function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}

function doHelp(id) {
	switch (id) {
	case 'stateCode':
		help('COMMON', 'HLP_STATE_CODE', $('#stateCode').val(), EMPTY_STRING, $('#stateCode'));
		break;
	case 'taxCode':
		if (!isEmpty($('#stateCode').val())) {
			help('COMMON', 'HLP_STATE_TAX', $('#taxCode').val(), $('#stateCode').val(), $('#taxCode'));
		} else {
			setError('stateCode_error', MANDATORY);
		}
		break;
	case 'scheduleCode':
		if (!isEmpty($('#stateCode').val())) {
			if (!isEmpty($('#taxCode').val())) {
		help('COMMON', 'HLP_TAX_SCHED_CODE', $('#scheduleCode').val(), $('#stateCode').val() + PK_SEPERATOR + $('#taxCode').val(), $('#scheduleCode'), function() {
		});
		break;
			}
		else {
			setError('taxCode_error', MANDATORY);
		}
	} else {
		setError('stateCode_error', MANDATORY);
	}
	case 'effectiveDate':
		if ($('#action').val() == MODIFY) {
			if (!isEmpty($('#stateCode').val())) {
				if (!isEmpty($('#taxCode').val())) {
					help(CURRENT_PROGRAM_ID, 'HLP_EFF_DATE', EMPTY_STRING, $('#stateCode').val() + PK_SEPERATOR + $('#taxCode').val(), $('#effectiveDate'), null, function(gridObj, rowID) {
						$('#effectiveDate').val(gridObj.cells(rowID, 2).getValue());
					});
					break;
				} else {
					setError('taxCode_error', MANDATORY);
				}
			} else {
				setError('stateCode_error', MANDATORY);
			}
		}
		break;
	}
}

function clearFields() {
	$('#stateCode').val(EMPTY_STRING);
	$('#stateCode_desc').html(EMPTY_STRING);
	$('#stateCode_error').html(EMPTY_STRING);
	$('#taxCode').val(EMPTY_STRING);
	$('#taxCode_desc').html(EMPTY_STRING);
	$('#taxCode_error').html(EMPTY_STRING);
	$('#effectiveDate').val(EMPTY_STRING);
	$('#effectiveDate_error').html(EMPTY_STRING);
	clearNonPKFields();
}

function clearNonPKFields() {
	$('#taxOnAmtPortion').val(EMPTY_STRING);
	$('#taxOnAmtPortion_error').html(EMPTY_STRING);
	$('#taxOnAmtPortionDesc').html(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
	taxschrate_clearGridFields();
	taxschrate_innerGrid.clearAll();
}

function doclearfields(id) {
	switch (id) {
	case 'stateCode':
		if (isEmpty($('#stateCode').val())) {
			$('#taxCode').val(EMPTY_STRING);
			$('#taxCode_desc').html(EMPTY_STRING);
			$('#taxCode_error').html(EMPTY_STRING);
			$('#effectiveDate').val(EMPTY_STRING);
			$('#effectiveDate_error').html(EMPTY_STRING);
			$('#effectiveDate').val(getCBD());
		}
		break;
	case 'taxCode':
		if (isEmpty($('#taxCode').val())) {
			$('#taxCode').val(EMPTY_STRING);
			$('#taxCode_desc').html(EMPTY_STRING);
			$('#taxCode_error').html(EMPTY_STRING);
			clearNonPKFields();
		}
		break;
	}
}

function add() {
	$('#stateCode').focus();
	$('#effectiveDate').val(getCBD());
	$('#effectiveDate_look').prop('disabled', true);
	setCheckbox('enabled', YES);
	$('#enabled').prop('disabled', true);
}

function modify() {
	$('#stateCode').focus();
	$('#effectiveDate_look').prop('disabled', false);
	if ($('#action').val() == MODIFY) {
		$('#effectiveDate').val(EMPTY_STRING);
		$('#enabled').prop('disabled', false);
	} else {
		$('#enabled').prop('disabled', true);
	}
}

function loadData() {
	var taxOn=null;
	$('#stateCode').val(validator.getValue('STATE_CODE'));
	$('#stateCode_desc').html(validator.getValue('F1_DESCRIPTION'));
	$('#taxCode').val(validator.getValue('TAX_CODE'));
	$('#taxCode_desc').html(validator.getValue('F2_DESCRIPTION'));
	$('#effectiveDate').val(validator.getValue('EFF_DATE'));
	$('#taxOnAmtPortion').val(validator.getValue('TAX_ON_AMT_PORTION'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadGrid();
	resetLoading();
	taxOn = validator.getValue('F2_TAX_ON');
	taxOnAmtPortionDescription_val(taxOn);
}

function view(source, primaryKey) {
	hideParent('cmn/qtaxschrate', source, primaryKey);
	resetLoading();
}

function validate(id) {
	switch (id) {
	case 'stateCode':
		stateCode_val();
		break;
	case 'taxCode':
		taxCode_val();
		break;
	case 'effectiveDate':
		effectiveDate_val();
		break;
	case 'scheduleCode':
		scheduleCode_val();
		break;
	case 'taxRates':
		taxRates_val();
		break;
	case 'taxOnAmtPortion':
		taxOnAmtPortion_val();
		break;
	case 'enabled':
		enabled_val();
		break;
	case 'remarks':
		remarks_val();
		break;
	}
}

function backtrack(id) {
	switch (id) {
	case 'stateCode':
		setFocusLast('stateCode');
		break;
	case 'taxCode':
		setFocusLast('stateCode');
		break;
	case 'effectiveDate':
		setFocusLast('taxCode');
		break;
	case 'scheduleCode':
		setFocusLast('effectiveDate');
		break;
	case 'taxRates':
		setFocusLast('scheduleCode');
		break;
	case 'taxOnAmtPortion':
		setFocusLast('taxRates');
		break;
	case 'enabled':
		setFocusLast('taxOnAmtPortion');
		break;
	case 'remarks':
		if ($('#action').val() == MODIFY) {
			setFocusLast('enabled');
		} else {
			setFocusLast('taxonamtportion');
		}
		break;
	case 'taxschrate_innerGrid_add':
		setFocusLast('taxRates');
		break;
	}
}

function stateCode_val() {
	var value = $('#stateCode').val();
	clearError('stateCode_error');
	if (isEmpty(value)) {
		setError('stateCode_error', MANDATORY);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('STATE_CODE', $('#stateCode').val());
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.cmn.mtaxbean');
		validator.setMethod('validateStateCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('stateCode_error', validator.getValue(ERROR));
			$('#stateCode_desc').html(EMPTY_STRING);
			return false;
		}
		$('#stateCode_desc').html(validator.getValue('DESCRIPTION'));
	}
	setFocusLast('taxCode');
	return true;
}

function taxCode_val() {
	var value = $('#taxCode').val();
	clearError('taxCode_error');
	if (isEmpty(value)) {
		setError('taxCode_error', MANDATORY);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('STATE_CODE', $('#stateCode').val());
		validator.setValue('TAX_CODE', $('#taxCode').val());
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.cmn.itaxschratebean');
		validator.setMethod('validateTaxCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('taxCode_error', validator.getValue(ERROR));
			$('#taxCode_desc').html(EMPTY_STRING);
			return false;
		}
		$('#taxCode_desc').html(validator.getValue('DESCRIPTION'));
		$('#taxOnAmtPortionDesc').html(ON_LABEL+EMPTY_STRING+validator.getValue("TAX_ON_DESC"));

	}
	setFocusLast('effectiveDate');
	return true;
}

function effectiveDate_val(valMode) {
	var value = $('#effectiveDate').val();
	clearError('effectiveDate_error');
	if (isEmpty(value)) {
		$('#effectiveDate').val(getCBD());
		value = $('#effectiveDate').val();
	}
	if (!isValidEffectiveDate(value, 'effectiveDate_error')) {
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('STATE_CODE', $('#stateCode').val());
		validator.setValue('TAX_CODE', $('#taxCode').val());
		validator.setValue('EFF_DATE', $('#effectiveDate').val());
		validator.setValue(ACTION, $('#action').val());
		validator.setClass('patterns.config.web.forms.cmn.itaxschratebean');
		validator.setMethod('validateEffectiveDate');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('effectiveDate_error', validator.getValue(ERROR));
			return false;
		} else {
			if ($('#action').val() == MODIFY) {
				PK_VALUE = getEntityCode() + PK_SEPERATOR + $('#stateCode').val() + PK_SEPERATOR + $('#taxCode').val() + PK_SEPERATOR + $('#effectiveDate').val();
				if (!loadPKValues(PK_VALUE, 'effectiveDate_error')) {
					return false;
				}
			}
		}
	}
	setFocusLast('scheduleCode');
	return true;
}

function scheduleCode_val() {
	var value = $('#scheduleCode').val();
	clearError('scheduleCode_error');
	if (isEmpty(value)) {
		setError('scheduleCode_error', MANDATORY);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('TAX_SCHEDULE_CODE', value);
	validator.setClass('patterns.config.web.forms.cmn.itaxschratebean');
	validator.setMethod('validateScheduleCode');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('scheduleCode_error', validator.getValue(ERROR));
		return false;
	}
	setFocusLast('taxRates');
	return true;
}

function taxRates_val() {
	var value = $('#taxRates').val();
	clearError('taxRates_error');
	if (isEmpty(value)) {
		setError('taxRates_error', MANDATORY);
		return false;
	}
	if (!isNumericWithDecimal($('#taxRates').val())) {
		setError('taxRates_error', INVALID_PERCENTAGE);
		return false;
	}
	value = parseFloat($('#taxRates').val()).toFixed(2);
	$('#taxRates').val(value);
	if (!isValidThreeDigitTwoDecimal($('#taxRates'))) {
		setError('taxRates_error', INVALID_PERCENTAGE);
		return false;
	}
	if (!isValidPercentage(value)) {
		setError('taxRates_error', PERCENTAGE_CHECK);
		return false;
	}
	if (isZero(value)) {
		setError('taxRates_error', ZERO_CHECK);
		return false;
	}
	setFocusLastOnGridSubmit('taxschrate_innerGrid');
	return true;
}

function taxOnAmtPortion_val() {
	var value = $('#taxOnAmtPortion').val();
	clearError('taxOnAmtPortion_error');
	if (isEmpty(value)) {
		setError('taxOnAmtPortion_error', MANDATORY);
		return false;
	}
	if (!isNumericWithDecimal($('#taxOnAmtPortion').val())) {
		setError('taxOnAmtPortion_error', INVALID_PERCENTAGE);
		return false;
	}
	value = parseFloat($('#taxOnAmtPortion').val()).toFixed(2);
	$('#taxOnAmtPortion').val(value);
	if (!isValidThreeDigitTwoDecimal($('#taxOnAmtPortion'))) {
		setError('taxOnAmtPortion_error', INVALID_PERCENTAGE);
		return false;
	}
	if (!isValidPercentage(value)) {
		setError('taxOnAmtPortion_error', PERCENTAGE_CHECK);
		return false;
	}
	if (isZero(value)) {
		setError('taxOnAmtPortion_error', ZERO_CHECK);
		return false;
	}
	setFocusLast('remarks');
	if ($('#action').val() == MODIFY) {
		setFocusLast('enabled');
	}
	return true;
}

function enabled_val() {
	if ($('#action').val() == MODIFY) {
		setFocusLast('remarks');
	}
	return true;
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', INVALID_REMARKS);
			setFocusLast('remarks');
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', INVALID_REMARKS);
			setFocusLast('remarks');
			return false;
		}
	}
	if ($('#action').val() == MODIFY) {
		if (isEmpty(value)) {
			setError('remarks_error', MANDATORY);
			setFocusLast('remarks');
			return false;
		}
	}
	setFocusOnSubmit();
	return true;
}

function taxOnAmtPortionDescription_val(taxon) {
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('TAX_ON', taxon);
	validator.setValue(ACTION, USAGE);
	validator.setClass('patterns.config.web.forms.cmn.itaxschratebean');
	validator.setMethod('getTaxRelatedInfo');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('taxCode_error', validator.getValue(ERROR));
		return false;
	}
	$('#taxOnAmtPortionDesc').html(ON_LABEL + validator.getValue("TAX_ON_DESC"));

}

function taxschrate_addGrid(editMode, editRowId) {
	if (scheduleCode_val(false) && taxRates_val()) {
		if (taxschrate_gridDuplicateCheck(editMode, editRowId)) {
			var field_values = [ 'false', $('#scheduleCode').val(), $('#taxRates').val() ];
			_grid_updateRow(taxschrate_innerGrid, field_values);
			taxschrate_clearGridFields();
			$('#scheduleCode').focus();
			return false;
		} else {
			setError('scheduleCode_error', DUPLICATE_DATA);
			return false;
		}
	} else {
		return false;
	}
}

function taxschrate_gridDuplicateCheck(editMode, editRowId) {
	var currentValue = [ $('#scheduleCode').val() ];
	return _grid_duplicate_check(taxschrate_innerGrid, currentValue, [ 1 ]);
}

function taxschrate_editGrid(rowId) {
	$('#scheduleCode').val(taxschrate_innerGrid.cells(rowId, 1).getValue());
	$('#taxRates').val(taxschrate_innerGrid.cells(rowId, 2).getValue());
	$('#scheduleCode').focus();
}

function taxschrate_cancelGrid(rowId) {
	taxschrate_clearGridFields();
}

function taxschrate_clearGridFields() {
	$('#scheduleCode').val(EMPTY_STRING);
	$('#scheduleCode_error').html(EMPTY_STRING);
	$('#taxRates').val(EMPTY_STRING);
	$('#taxRates_error').html(EMPTY_STRING);
}

function gridExit(id) {
	switch (id) {
	case 'scheduleCode':
	case 'taxRates':
		$('#xmlTaxSchrate').val(taxschrate_innerGrid.serialize());
		taxschrate_clearGridFields();
		setFocusLast('taxOnAmtPortion');
		break;
	}
}

function gridDeleteRow(id) {
	switch (id) {
	case 'scheduleCode':
		deleteGridRecord(taxschrate_innerGrid);
		break;
	}
}

function revalidate() {
	taxschrate_revaildateGrid();
}

function taxschrate_revaildateGrid() {
	if (taxschrate_innerGrid.getRowsNum() > 0) {
		$('#xmlTaxSchrate').val(taxschrate_innerGrid.serialize());
		$('#scheduleCode_error').html(EMPTY_STRING);
	} else {
		taxschrate_innerGrid.clearAll();
		$('#xmlTaxSchrate').val(taxschrate_innerGrid.serialize());
		setError('scheduleCode_error', ADD_ATLEAST_ONE_ROW);
		errors++;
	}
}
