<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.cmn.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/cmn/qscheme.js" />
	</web:dependencies>
	<web:loadPK />
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="mscheme.querytitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:viewContent id="po_view1">
				<web:dividerBlock>
					<web:sectionBlock width="width-100p" align="left">
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="180px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="mscheme.schemecode" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:schemeCodeDisplay property="schemeCode" id="schemeCode" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="180px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowOdd>
									<web:column>
										<web:legend key="form.description" var="common" mandatory="true" />
									</web:column>
									<web:column>
										<type:descriptionDisplay property="description" id="description" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="form.conciseDescription" var="common" mandatory="true" />
									</web:column>
									<web:column>
										<type:conciseDescriptionDisplay property="conciseDescription" id="conciseDescription" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="mscheme.schemefor" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:comboDisplay property="schemeFor" id="schemeFor" datasourceid="COMMON_SCHEMEFOR" errorNotRequired="false" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="mscheme.parentschemecode" var="program" />
									</web:column>
									<web:column>
										<type:schemeCodeDisplay property="parentSchemeCode" id="parentSchemeCode" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="mscheme.idenrolforbene" var="program" />
									</web:column>
									<web:column>
										<type:checkboxDisplay property="schemeHasEnrol" id="schemeHasEnrol" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="mscheme.usingpidtype" var="program" />
									</web:column>
									<web:column>
										<type:pidCodeDisplay property="usingPidType" id="usingPidType" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="mscheme.nameofid" var="program" />
									</web:column>
									<web:column>
										<type:otherInformation50Display property="nameOfIdEnrolNo" id="nameOfIdEnrolNo" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="mscheme.paymentconrol" var="program" />
									</web:column>
									<web:column>
										<type:checkboxDisplay property="paymentConrolTokenRef" id="paymentConrolTokenRef" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="mscheme.nameoftokenref" var="program" />
									</web:column>
									<web:column>
										<type:otherInformation50Display property="nameOfTokenRef" id="nameOfTokenRef" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="form.enabled" var="common" />
									</web:column>
									<web:column>
										<type:checkboxDisplay property="enabled" id="enabled" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="form.remarks" var="common" />
									</web:column>
									<web:column>
										<type:remarksDisplay property="remarks" id="remarks" />
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
						<web:auditDisplay />
					</web:sectionBlock>
				</web:dividerBlock>
			</web:viewContent>
		</web:viewSubPart>
	</web:viewPart>
</web:fragment>