var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MCPCENTERCODE';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}
function clearFields() {
	$('#costCode').val(EMPTY_STRING);
	$('#description').val(EMPTY_STRING);
	$('#conciseDescription').val(EMPTY_STRING);
	$('#profCost').val(EMPTY_STRING);
	$('#enabled').prop('checked', false);
	setCheckbox('enabled', NO);
	$('#remarks').val(EMPTY_STRING);
}
function loadData() {
	$('#costCode').val(validator.getValue('CP_CENTER_CODE'));
	$('#description').val(validator.getValue('DESCRIPTION'));
	$('#conciseDescription').val(validator.getValue('CONCISE_DESCRIPTION'));
	$('#profCost').val(validator.getValue('PROFIT_COST'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadAuditFields(validator);
}
