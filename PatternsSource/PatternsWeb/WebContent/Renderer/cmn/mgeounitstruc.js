var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MGEOUNITSTRUC';
function init() {
	refreshTBAGrid();
	refreshQueryGrid();
	if (_redisplay) {
		if ($('#action').val() == ADD) {
			setCheckbox('enabled', YES);
			$('#enabled').prop('disabled', true);
			$('#geoUnitStructureCode_pic').attr("disabled", "disabled"); 
		}
		if ($('#action').val() == MODIFY) {
			$('#enabled').prop('disabled', false);
		}

	}
}
function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}
function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'MGEOUNITSTRUC_MAIN', queryGrid);
}
function clearFields() {

	$('#geoUnitStructureCode').val(EMPTY_STRING);
	$('#geoUnitStructureCode_error').html(EMPTY_STRING);
	clearNonPKFields();

}
function clearNonPKFields() {
	$('#description').val(EMPTY_STRING);
	$('#description_error').html(EMPTY_STRING);
	$('#conciseDescription').val(EMPTY_STRING);
	$('#conciseDescription_error').html(EMPTY_STRING);
	$('#parentgeoUnitStructureCode').val(EMPTY_STRING);
	$('#parentgeoUnitStructureCode_desc').html(EMPTY_STRING);
	$('#parentgeoUnitStructureCode_error').html(EMPTY_STRING);
	//setCheckbox('enabled', NO);
	setCheckbox('requireaUnitType', NO);
	setCheckbox('lastHierarchy', NO);
	setCheckbox('dataPatternRequired', NO);
	$('#dataInputPattern').val(EMPTY_STRING);
	$('#dataInputPattern_error').html(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
	$('#lastHierarchy_error').html(EMPTY_STRING);
}
function doclearfields(id) {
	switch (id) {
	case 'geoUnitStructureCode':
		if (isEmpty($('#geoUnitStructureCode').val())) {
			$('#geoUnitStructureCode_error').html(EMPTY_STRING);
			$('#geoUnitStructureCode_desc').html(EMPTY_STRING);
			clearNonPKFields();
			break;
		}
	}
}
function add() {

	setCheckbox('enabled', YES);
	$('#geoUnitStructureCode').focus();
	$('#dataInputPattern').attr('readonly', true);
	$('#enabled').prop('disabled', true);

}
function modify() {

	if ($('#action').val() == MODIFY) {
		$('#enabled').prop('disabled', false);
	} else {
		$('#enabled').prop('disabled', true);
	}
}
function loadData() {
	$('#geoUnitStructureCode').val(validator.getValue('GEO_UNIT_STRUC_CODE'));
	$('#description').val(validator.getValue('DESCRIPTION'));
	$('#conciseDescription').val(validator.getValue('CONCISE_DESCRIPTION'));
	$('#parentgeoUnitStructureCode').val(validator.getValue('PARENT_GUS_CODE'));
	$('#previoushier').val(validator.getValue('LAST_IN_HIERARCHY'));
	$('#parentgeoUnitStructureCode_desc').html(validator.getValue('F1_DESCRIPTION'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	setCheckbox('lastHierarchy', validator.getValue('LAST_IN_HIERARCHY'));
	setCheckbox('requireaUnitType', validator.getValue('UNIT_TYPE_REQ'));
	setCheckbox('dataPatternRequired', validator.getValue('DATA_PATTERN_REQ'));
	if ($('#dataPatternRequired').is(':checked')) {
		setCheckbox('dataPatternRequired', YES);
		$('#dataInputPattern').attr('readonly', false);
		$('#dataInputPattern').val(validator.getValue('DATA_PATTERN'));
	}
	$('#remarks').val(validator.getValue('REMARKS'));
	resetLoading();
}

function doHelp(id) {
	switch (id) {
	case 'geoUnitStructureCode':
		help('COMMON', 'HLP_GEOUNITSTRUC_M', $('#geoUnitStructureCode').val(), '', $('#geoUnitStructureCode'));
		break;
	case 'parentgeoUnitStructureCode':
		help('COMMON', 'HLP_PARENT_GEOUNITSTRUC', $('#parentgeoUnitStructureCode').val(), $('#geoUnitStructureCode').val(), $('#parentgeoUnitStructureCode'));
		break;
	}
}
function view(source, primaryKey) {
	hideParent('cmn/qgeounitstruc', source, primaryKey);
	resetLoading();
}
function checkclick(id) {
	switch (id) {
	case 'dataPatternRequired':
		dataPatternRequired_val();
		break;

	case 'lastHierarchy':
		lastHierarchy_val();
		break;
	}
}

function validate(id) {
	switch (id) {
	case 'geoUnitStructureCode':
		geoUnitStructureCode_val();
		break;
	case 'description':
		description_val();
		break;
	case 'conciseDescription':
		conciseDescription_val();
		break;
	case 'parentgeoUnitStructureCode':
		parentgeoUnitStructureCode_val();
		break;
	case 'lastHierarchy':
		lastHierarchy_val();
		break;
	case 'requireaUnitType':
		requireaUnitType_val();
		break;
	case 'dataPatternRequired':
		dataPatternRequired_val();
		break;
	case 'dataInputPattern':
		dataInputPattern_val();
		break;
	case 'enabled':
		enabled_val();
		break;
	case 'remarks':
		remarks_val();
		break;
	}
}

function backtrack(id) {
	switch (id) {
	case 'geoUnitStructureCode':
		setFocusLast('geoUnitStructureCode');
		break;
	case 'description':
		setFocusLast('geoUnitStructureCode');
		break;
	case 'conciseDescription':
		setFocusLast('description');
		break;
	case 'parentgeoUnitStructureCode':
		setFocusLast('conciseDescription');
		break;
	case 'lastHierarchy':
		setFocusLast('parentgeoUnitStructureCode');
		break;
	case 'requireaUnitType':
		setFocusLast('lastHierarchy');
		break;
	case 'dataPatternRequired':
		setFocusLast('requireaUnitType');
		break;
	case 'dataInputPattern':
		setFocusLast('dataPatternRequired');
		break;
	case 'enabled':
		if ($('#dataPatternRequired').is(':checked')) {
			setFocusLast('dataInputPattern');
		} else {
			setFocusLast('dataPatternRequired');
		}
		break;
	case 'remarks':
		if ($('#action').val() == MODIFY) {
			setFocusLast('enabled');
		} else {
			if ($('#dataPatternRequired').is(':checked')) {
				setFocusLast('dataInputPattern');
			} else {
				setFocusLast('dataPatternRequired');
			}
		}
		break;
	}
}

function requireaUnitType_val() {
	setFocusLast('dataPatternRequired');
}
function dataPatternRequired_val() {
	clearError('dataPatternRequired_error');
	$('#dataInputPattern').val(EMPTY_STRING);
	clearError('dataInputPattern_error');
	if ($('#dataPatternRequired').is(':checked')) {
		$('#dataInputPattern').attr('readonly', false);
		$('#dataInputPattern').val(EMPTY_STRING);
		$('#dataInputPattern_error').html(EMPTY_STRING);
		$('#dataInputPattern').focus();
	} else {
		clearError('dataInputPattern_error');
		$('#dataInputPattern').attr('readonly', true);
		if ($('#action').val() == ADD) {
			setFocusLast('remarks');
		} else {
			setFocusLast('enabled');
		}
	}
	return true;
}

function enabled_val() {
	if ($('#action').val() == MODIFY) {
		setFocusLast('remarks');
	}
	return true;
}
function geoUnitStructureCode_val() {
	var value = $('#geoUnitStructureCode').val();
	clearError('geoUnitStructureCode_error');
	if (isEmpty(value)) {
		setError('geoUnitStructureCode_error', HMS_MANDATORY);
		setFocusLast('geoUnitStructureCode');
		return false;
	} else if ($('#action').val() == ADD && !isValidCode(value)) {
		setError('geoUnitStructureCode_error', HMS_INVALID_FORMAT);
		setFocusLast('geoUnitStructureCode');
		return false;
	} else {
		if ($('#action').val() == ADD) {
			validator.clearMap();
			validator.setMtm(false);
			validator.setValue('PARENT_GUS_CODE', $('#parentgeoUnitStructureCode').val());
			validator.setValue('GEO_UNIT_STRUC_CODE', $('#geoUnitStructureCode').val());
			validator.setValue('ACTION', $('#action').val());
			validator.setClass('patterns.config.web.forms.cmn.mgeounitstrucbean');
			validator.setMethod('validateGeographicalUnitStructureCode');
			validator.sendAndReceive();
			if (validator.getValue(ERROR) != EMPTY_STRING) {
				setError('geoUnitStructureCode_error', validator.getValue(ERROR));
				setFocusLast('geoUnitStructureCode');
				return false;
			}
		} else {
			PK_VALUE = $('#geoUnitStructureCode').val();
			loadPKValues(PK_VALUE, 'geoUnitStructureCode_error');
			setFocusLast('description');
			return true;
		}
	}
	setFocusLast('description');
	return true;
}

function description_val() {
	var value = $('#description').val();
	clearError('description_error');
	if (isEmpty(value)) {
		setError('description_error', HMS_MANDATORY);
		setFocusLast('description');
		return false;
	}
	if (!isValidName(value)) {
		setError('description_error', HMS_INVALID_DESCRIPTION);
		setFocusLast('description');
		return false;
	}
	setFocusLast('conciseDescription');
	return true;
}
function conciseDescription_val() {
	var value = $('#conciseDescription').val();
	clearError('conciseDescription_error');
	if (isEmpty(value)) {
		setError('conciseDescription_error', HMS_MANDATORY);
		setFocusLast('conciseDescription');
		return false;
	}
	if (!isValidShortName(value)) {
		setError('conciseDescription_error', HMS_INVALID_CONCISE_DESCRIPTION);
		setFocusLast('conciseDescription');
		return false;
	}
	setFocusLast('parentgeoUnitStructureCode');
	return true;
}

function parentgeoUnitStructureCode_val() {
	var value = $('#parentgeoUnitStructureCode').val();
	clearError('parentgeoUnitStructureCode_error');
	if (!isEmpty(value)) {
		if (!isValidCode(value)) {
			setError('parentgeoUnitStructureCode_error', HMS_INVALID_FORMAT);
			setFocusLast('parentgeoUnitStructureCode');
			return false;
		} else {
			validator.clearMap();
			validator.setMtm(false);
			validator.setValue('PARENT_GUS_CODE', $('#parentgeoUnitStructureCode').val());
			validator.setValue('GEO_UNIT_CODE', $('#geoUnitStructureCode').val());
			validator.setValue('ACTION', $('#action').val());
			validator.setClass('patterns.config.web.forms.cmn.mgeounitstrucbean');
			validator.setMethod('validateParentgeoUnitStructureCode');
			validator.sendAndReceive();
			if (validator.getValue(ERROR) != EMPTY_STRING) {
				setError('parentgeoUnitStructureCode_error', validator.getValue(ERROR));
				setFocusLast('parentgeoUnitStructureCode');
				return false;
			} else {
				$('#parentgeoUnitStructureCode_desc').html(validator.getValue('DESCRIPTION'));
			}
		}
	}
	setFocusLast('lastHierarchy');
	return true;
}
function lastHierarchy_val() {
	clearError('lastHierarchy_error');
	if ($('#action').val() == MODIFY) {
		if ($('#previoushier').val() == "0" && $('#lastHierarchy').is(':checked')) {
			validator.clearMap();
			validator.setMtm(false);
			validator.setValue('GEO_UNIT_STRUC_CODE', $('#geoUnitStructureCode').val());
			validator.setValue('PREV_LAST_IN_HIERARCHY', $('#previoushier').val());
			validator.setValue('LAST_IN_HIERARCHY', booleanToString($('#lastHierarchy').val()));
			validator.setValue('ACTION', $('#action').val());
			validator.setClass('patterns.config.web.forms.cmn.mgeounitstrucbean');
			validator.setMethod('validatelastHierarchy');
			validator.sendAndReceive();
			if (validator.getValue(ERROR) != EMPTY_STRING) {
				setError('lastHierarchy_error', validator.getValue(ERROR));
				setFocusLast('lastHierarchy');
				return false;
			}
		} else {
			clearError('lastHierarchy_error');
		}
	}
	setFocusLast('requireaUnitType');
	return true;
}
function dataInputPattern_val() {
	var value = $('#dataInputPattern').val();
	clearError('dataInputPattern_error');
	if ($('#dataPatternRequired').is(':checked')) {
		if (isEmpty(value)) {
			setError('dataInputPattern_error', HMS_MANDATORY);
			setFocusLast('dataInputPattern');
			return false;
		}
		if (!isValidName(value)) {
			setError('dataInputPattern_error', HMS_INVALID_DESCRIPTION);
			setFocusLast('dataInputPattern');
			return false;
		}

		if ($('#action').val() == ADD) {
			setFocusLast('remarks');
		} else {
			setFocusLast('enabled');
		}
		return true;
	}
}
function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', HMS_INVALID_REMARKS);
			setFocusLast('remarks');
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', HMS_INVALID_REMARKS);
			setFocusLast('remarks');
			return false;
		}
	}
	if ($('#action').val() == MODIFY) {
		if (isEmpty(value)) {
			setError('remarks_error', HMS_MANDATORY);
			setFocusLast('remarks');
			return false;
		}

	}
	setFocusOnSubmit();
	return true;
}
