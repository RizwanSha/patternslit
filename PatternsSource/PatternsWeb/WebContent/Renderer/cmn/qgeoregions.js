var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MGEOREGIONS';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}
function clearFields() {
	$('#geoRegionCode').val(EMPTY_STRING);
	$('#description').val(EMPTY_STRING);
	$('#conciseDescription').val(EMPTY_STRING);
	$('#pinCode').val(EMPTY_STRING);
	$('#officeCode').val(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
}

function loadData() {
	$('#geoRegionCode').val(validator.getValue('GEO_REGION_CODE'));
	$('#description').val(validator.getValue('DESCRIPTION'));
	$('#conciseDescription').val(validator.getValue('CONCISE_DESCRIPTION'));
	$('#pinCode').val(validator.getValue('PINCODE_GEOUNITID'));
	$('#pinCode_desc').html(validator.getValue('F1_DESCRIPTION'));
	$('#officeCode').val(validator.getValue('OFFICE_CODE'));
	$('#officeCode_desc').html(validator.getValue('F2_DESCRIPTION'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	var overlayObject = JSON.parse(validator.getValue('F3_FILE_DATA'));
	loadAuditFields(validator);
	mapObject.drawMapWithOverlay(overlayObject);
}

var mapObject = {
	map : "map",
	overlays : [],
	index : 1,
	geocoder : {},
	location : "India",
	drawingManager : {},
	selectedOverlay : -1,
	defaultRegionOptions : {
		fillColor : '#3dafed',
		fillOpacity : 0.6,
		strokeColor : '#3dafed',
		strokeOpacity : 0.9,
		strokeWeight : 2,
		clickable : true,
		editable : true,
		draggable : true,
		zIndex : 1
	},
	defaultRegionStyleOptions : {
		fillColor : '#3dafed',
		strokeColor : '#3dafed'
	},
	selectedRegionStyleOptions : {
		fillColor : '#cccccc',
		strokeColor : '#cccccc'
	},
	initMap : function() {
		var that = this;
		this.map = new google.maps.Map(document.getElementById('map'), {
			center : {
				lat : -34.397,
				lng : 150.644
			},
			zoom : 5,
			scaleControl : false
		});
		this.geocoder = new google.maps.Geocoder();
		this.setDefaultLocation();
	},
	setDefaultLocation : function() {
		var that = this;
		this.geocoder.geocode({
			'address' : this.location
		}, function(results, status) {
			if (status == google.maps.GeocoderStatus.OK) {
				that.map.setCenter(results[0].geometry.location);
			} else {
				alert("Could not find location: " + that.location);
			}
		});
	},
	drawMapWithOverlay : function(overlayObject) {
		var type = overlayObject.type;
		switch (type) {
		case "Polygon":
			this.drawPolygon(overlayObject.paths);
			break;
		case "Circle":
			this.drawCircle(overlayObject.center, overlayObject.radius);
			break;
		case "Rectangle":
			this.drawRectangle(overlayObject.bounds);
			break;
		default:
			this.drawDefaultMap();
			break;
		}
	},
	drawPolygon : function(polygonPath) {
		var that = this;
		var mapPolygon = new google.maps.Polygon({
			paths : polygonPath,
			strokeColor : '#3dafed',
			strokeOpacity : 0.9,
			strokeWeight : 2,
			fillColor : '#3dafed',
			clickable : false,
			editable : false,
			draggable : false,
			fillOpacity : 0.35
		});
		mapPolygon.setMap(this.map);
		this.map.fitBounds(this.getBounds(mapPolygon));
		this.overlays.push(mapPolygon);
	},
	drawCircle : function(center, radius) {
		var that = this;
		var mapCircle = new google.maps.Circle({
			strokeColor : '#3dafed',
			strokeOpacity : 0.9,
			strokeWeight : 2,
			fillColor : '#3dafed',
			fillOpacity : 0.35,
			center : center,
			radius : radius,
			clickable : false,
			editable : false,
			draggable : false
		});
		mapCircle.setMap(this.map);
		this.map.fitBounds(mapCircle.getBounds());
		this.overlays.push(mapCircle);
	},
	drawRectangle : function(bounds) {
		var that = this;
		var mapRectangle = new google.maps.Rectangle({
			strokeColor : '#3dafed',
			strokeOpacity : 0.9,
			strokeWeight : 2,
			fillColor : '#3dafed',
			fillOpacity : 0.35,
			clickable : false,
			editable : false,
			draggable : false,
			bounds : bounds
		});
		mapRectangle.setMap(this.map);
		this.map.fitBounds(mapRectangle.getBounds());
		this.overlays.push(mapRectangle);
	},
	getBounds : function(polygon) {
		var bounds = new google.maps.LatLngBounds();
		polygon.getPath().forEach(function(element, index) {
			bounds.extend(element);
		})
		return bounds;
	},
	getCenter : function(polygon) {
		return this.getBounds(polygon).getCenter();
	},

};
