<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.cmn.Messages"
	var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/cmn/mtenorconfig.js" />
	</web:dependencies>
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="mtenorconfig.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/cmn/mtenorconfig" id="mtenorconfig"
				method="POST">
				<web:dividerBlock>
					<type:optionBox add="true" modify="true" view="true" />
				</web:dividerBlock>
				<web:viewContent id="po_view1">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="mtenorconfig.tenorspecificationcode"
												var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:tenorSpecificationCode
												property="tenorSpecificationCode"
												id="tenorSpecificationCode" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="mtenorconfig.efftdate" var="program"
												mandatory="true" />
										</web:column>
										<web:column>
											<type:date property="efftDate" id="efftDate" lookup="true" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle width="200px" />
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:viewTitle var="program" key="mtenorconfig.tenorsrequired" />
									<web:rowEven>
										<web:column>
											<web:legend key="mtenorconfig.uptoperiod" var="program"
												mandatory="true" />
										</web:column>
										<web:column>
											<type:threeDigit property="uptoPeriod" id="uptoPeriod" />
										</web:column>
										<web:column>
											<web:legend key="mtenorconfig.periodflag" var="program" mandatory="true"/>
										</web:column>
										<web:column>
											<type:combo property="periodFlag" id="periodFlag"
												datasourceid="MTENORCONFIG_UPTO_PERIOD" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="mtenorconfig.tenordescription" var="program" mandatory="true"/>
										</web:column>
										<web:column>
											<type:remarks property="tenorDescription" id="tenorDescription" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
										<web:rowEven>
										<web:column>
											<web:gridToolbar gridName="mtenorconfig_innerGrid"
												editFunction="mtenorconfig_editGrid"
												cancelFunction="mtenorconfig_CancelGrid"
												afterAddFunction="mtenorconfig_afteraddGrid"
												addFunction="mtenorconfig_addGrid" continuousEdit="false"
												insertAtFirsstNotReq="false" />
											<web:element property="xmlMtenorconfigGrid" />
										</web:column>
									</web:rowEven>
									<web:rowEven>
										<web:column>
											<web:viewContent id="po_view4">
												<web:grid height="240px" width="601px"
													id="mtenorconfig_innerGrid"
													src="cmn/mtenorconfig_innerGrid.xml">
												</web:grid>
											</web:viewContent>
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="form.IsUseEnabled" var="common" />
										</web:column>
										<web:column>
											<type:checkbox property="enabled" id="enabled" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="form.remarks" var="common" />
										</web:column>
										<web:column>
											<type:remarks property="remarks" id="remarks" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column span="2">
											<web:submitReset />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
						</web:sectionBlock>
					</web:dividerBlock>
				</web:viewContent>
				<web:element property="command" />
				<web:element property="action" />
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
	<web:queryGrid />
	<web:TBAGrid />
	<web:contextSearch />
</web:fragment>