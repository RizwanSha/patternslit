var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'EPIDREG';
function init() {
	refreshTBAGrid();
	refreshQueryGrid();
	if (_redisplay) {
		if ($('#action').val() == ADD) {
			$('#PIDRegSl').prop('readOnly', true);
			$('#PIDRegSl_pic').prop('disabled', true);
			$('#deRegister').prop('disabled', true);
			setCheckbox('deRegister', NO);
			$('#reasonOfDereg').prop('readOnly', true);
			hide('po_view5');
		} else {
			if ($('#deRegister').is(':checked')) {
				show('po_view5');
			} else {
				hide('po_view5');
			}
		}

	}
}

function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'EPIDREG_MAIN', queryGrid);
}
function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}

function clearFields() {
	$('#personId').val(EMPTY_STRING);
	$('#personId_desc').html(EMPTY_STRING);
	$('#personId_error').html(EMPTY_STRING);
	$('#PIDRegSl').val(EMPTY_STRING);
	$('#PIDRegSl_error').html(EMPTY_STRING);
	clearNonPKFields();
}
function clearNonPKFields() {
	setCheckbox('deRegister', NO);
	$('#deRegister_error').html(EMPTY_STRING);
	$('#dateOfRegistration').val(EMPTY_STRING);
	$('#dateOfRegistration_error').html(EMPTY_STRING);
	$('#PIDCode').val(EMPTY_STRING);
	$('#PIDCode_desc').html(EMPTY_STRING);
	$('#PIDCode_error').html(EMPTY_STRING);
	$('#PIDNumber').val(EMPTY_STRING);
	$('#PIDNumber_error').html(EMPTY_STRING);
	$('#PIDIssueDate').val(EMPTY_STRING);
	$('#PIDIssueDate_error').html(EMPTY_STRING);
	$('#PIDExpiryDate').val(EMPTY_STRING);
	$('#PIDExpiryDate_error').html(EMPTY_STRING);
	$('#PIDRemarks').val(EMPTY_STRING);
	$('#PIDRemarks_error').html(EMPTY_STRING);
	$('#dateOfDereg').val(EMPTY_STRING);
	$('#dateOfDereg_error').html(EMPTY_STRING);
	$('#reasonOfDereg').val(EMPTY_STRING);
	$('#reasonOfDereg_error').html(EMPTY_STRING);
}
function add() {
	$('#personId').focus();
	$('#PIDRegSl').prop('readOnly', true);
	$('#PIDRegSl_pic').prop('disabled', true);
	$('#deRegister').prop('disabled', true);
	setCheckbox('deRegister', NO);
	$('#dateOfRegistration').val(getCBD());
	hide('po_view5');
	doDeRegister_chk(false);

}

function modify() {
	$('#personId').focus();
	$('#PIDRegSl').prop('readOnly', false);
	$('#PIDRegSl_pic').prop('disabled', false);
	$('#deRegister').prop('disabled', false);
	$('#reasonOfDereg').prop('readOnly', true);

	doDeRegister_chk(false);
}

function doHelp(id) {
	switch (id) {
	case 'personId':
		help('COMMON', 'HLP_CUST_ID_FOLIO', $('#personId').val(), EMPTY_STRING, $('#personId'), null, function(gridObj, rowID) {
			$('#personId').val(gridObj.cells(rowID, 0).getValue());
		});
		break;
	case 'PIDRegSl':
		help('COMMON', 'HLP_CUSTDOCID_REGSL', $('#PIDRegSl').val(), $('#personId').val(), $('#PIDRegSl'));
		break;
	case 'PIDCode':
		help('COMMON', 'HLP_PID_TYPE', $('#PIDCode').val(), EMPTY_STRING, $('#PIDCode'));
		break;
	}
}

function doclearfields(id) {
	switch (id) {
	case 'personId':
		if (isEmpty($('#personId').val())) {
			$('#personId_error').html(EMPTY_STRING);
			$('#personId_desc').html(EMPTY_STRING);
			break;
		}

	case 'PIDRegSl':
		if (isEmpty($('#PIDRegSl').val())) {
			$('#PIDRegSl_error').html(EMPTY_STRING);
			$('#PIDRegSl_error').html(EMPTY_STRING);
			clearNonPKFields();
			break;
		}
	}
}

function loadData() {
	$('#personId').val(validator.getValue('CIF_NO'));
	$('#personId_desc').html(validator.getValue('F1_NAME'));
	$('#PIDRegSl').val(validator.getValue('DOC_SL'));
	setCheckbox('deRegister', validator.getValue('PID_DE_REG'));
	$('#dateOfRegistration').val(validator.getValue('DATE_OF_REG'));
	$('#PIDCode').val(validator.getValue('PID_CODE'));
	$('#PIDCode_desc').html(validator.getValue('F2_DESCRIPTION'));
	$('#PIDNumber').val(validator.getValue('DOCUMENT_NUMBER'));
	$('#PIDIssueDate').val(validator.getValue('DOCUMENT_ISSUE_DATE'));
	$('#PIDExpiryDate').val(validator.getValue('DOCUMENT_EXPIRY_DATE'));
	$('#PIDRemarks').val(validator.getValue('REMARKS'));
	$('#dateOfDereg').val(validator.getValue('DATE_OF_DE_REG'));
	$('#reasonOfDereg').val(validator.getValue('REASON_FOR_DE_REG'));
	doDeRegister_chk(false);
	resetLoading();
}
function view(source, primaryKey) {
	hideParent('cmn/qpidreg', source, primaryKey);
	resetLoading();
}
function validate(id) {

	switch (id) {
	case 'personId':
		personId_val();
		break;
	case 'PIDRegSl':
		PIDRegSl_val();
		break;
	case 'deRegister':
		deRegister_val();
		break;
	case 'dateOfRegistration':
		dateOfRegistration_val();
		break;
	case 'PIDCode':
		PIDCode_val();
		break;
	case 'PIDNumber':
		PIDNumber_val();
		break;
	case 'PIDIssueDate':
		PIDIssueDate_val();
		break;
	case 'PIDExpiryDate':
		PIDExpiryDate_val();
		break;
	case 'PIDRemarks':
		PIDRemarks_val();
		break;
	case 'dateOfDereg':
		dateOfDereg_val();
		break;
	case 'reasonOfDereg':
		reasonOfDereg_val();
		break;
	}
}

function backtrack(id) {
	switch (id) {
	case 'personId':
		setFocusLast('personId');
		break;
	case 'PIDRegSl':
		setFocusLast('personId');
		break;
	case 'deRegister':
		setFocusLast('PIDRegSl');
		break;
	case 'dateOfRegistration':
		setFocusLast('deRegister');
		break;
	case 'PIDCode':
		if ($('#action').val() == ADD) {
			setFocusLast('personId');
		} else {
			setFocusLast('deRegister');
		}
		break;
	case 'PIDNumber':
		setFocusLast('PIDCode');
		break;
	case 'PIDIssueDate':
		setFocusLast('PIDNumber');
		break;
	case 'PIDExpiryDate':
		setFocusLast('PIDIssueDate');
		break;
	case 'PIDRemarks':
		setFocusLast('PIDExpiryDate');
		break;
	case 'dateOfDereg':
		setFocusLast('PIDRemarks');
		break;
	case 'reasonOfDereg':
		if ($('#deRegister').is(':checked')) {
			setFocusLast('deRegister');
		} else {
			setFocusLast('PIDRemarks');
		}
		break;
	}
}

function checkclick(id) {
	var valMode = true;
	switch (id) {

	case 'deRegister':
		deRegister_chk(valMode);
		break;
	}
}

function deRegister_chk(valMode) {
	$('#dateOfDereg').val(EMPTY_STRING);
	$('#dateOfDereg_error').html(EMPTY_STRING);
	$('#reasonOfDereg').val(EMPTY_STRING);
	$('#reasonOfDereg_error').html(EMPTY_STRING);
	if ($('#deRegister').is(':checked')) {
		$('#dateOfDereg').val(getCBD());
	}
	doDeRegister_chk();
}

function doDeRegister_chk(valMode) {
	if ($('#deRegister').is(':checked')) {
		$('#PIDCode').prop('readOnly', true);
		$('#PIDNumber').prop('readOnly', true);
		$('#PIDIssueDate').prop('readOnly', true);
		$('#PIDIssueDate_pic').prop('disabled', true);
		$('#PIDExpiryDate').prop('readOnly', true);
		$('#PIDExpiryDate_pic').prop('disabled', true);
		$('#PIDRemarks').prop('readOnly', true);
		$('#reasonOfDereg').prop('readOnly', false);
		show('po_view5');
		if (valMode)
			setFocusLast('reasonOfDereg');
	} else {
		$('#PIDCode').prop('readOnly', false);
		$('#PIDNumber').prop('readOnly', false);
		$('#PIDIssueDate').prop('readOnly', false);
		$('#PIDIssueDate_pic').prop('disabled', false);
		$('#PIDExpiryDate_pic').prop('disabled', false);
		$('#PIDExpiryDate').prop('readOnly', false);
		$('#PIDRemarks').prop('readOnly', false);
		$('#reasonOfDereg').prop('readOnly', true);
		hide('po_view5');
		if (valMode)
			setFocusLast('PIDCode');
	}
	return true;
}

function personId_val() {
	var value = $('#personId').val();
	clearError('personId_error');
	if (isEmpty(value)) {
		setError('personId_error', MANDATORY);
		return false;
	} else if ($('#action').val() == ADD && !isNumeric(value)) {
		setError('personId_error', INVALID_NUMBER);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('CIF_NO', $('#personId').val());
		validator.setValue('ACTION', USAGE);
		validator.setClass('patterns.config.web.forms.cmn.epidregbean');
		validator.setMethod('validateCIFNumber');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('personId_error', validator.getValue(ERROR));
			$('#personId_desc').html(EMPTY_STRING);
			return false;
		} else {
			if (validator.getValue(RESULT) == DATA_AVAILABLE) {
				$('#personId_desc').html(validator.getValue("NAME"));
			}
		}
	}
	if ($('#action').val() == MODIFY) {
		setFocusLast('PIDRegSl');
	} else {
		setFocusLast('PIDCode');
	}

	return true;
}

function PIDRegSl_val() {
	if ($('#action').val() == MODIFY) {
		var value = $('#PIDRegSl').val();
		clearError('PIDRegSl_error');
		if (isEmpty(value)) {
			setError('PIDRegSl_error', MANDATORY);
			return false;
		}
		if (!isNumeric(value)) {
			setError('PIDRegSl_error', INVALID_NUMBER);
			return false;
		} else {
			validator.clearMap();
			validator.setMtm(false);
			validator.setValue('CIF_NO', $('#personId').val());
			validator.setValue('DOC_SL', $('#PIDRegSl').val());
			validator.setValue(ACTION, $('#action').val());
			validator.setClass('patterns.config.web.forms.cmn.epidregbean');
			validator.setMethod('validatePIDRegSl');
			validator.sendAndReceive();
			if (validator.getValue(ERROR) != EMPTY_STRING) {
				setError('PIDRegSl_error', validator.getValue(ERROR));
				return false;
			} else {
				PK_VALUE = $('#personId').val() + PK_SEPERATOR + $('#PIDRegSl').val();
				if (!loadPKValues(PK_VALUE, 'PIDRegSl_error')) {
					return false;
				}
			}
		}
	}
	setFocusLast('deRegister');
	return true;
}

function deRegister_val() {
	if ($('#deRegister').is(':checked')) {
		setFocusLast('reasonOfDereg');
	} else {
		setFocusLast('PIDCode');
	}
	return true;
}

function dateOfRegistration_val() {
	var value = $('#dateOfRegistration').val();
	clearError('dateOfRegistration_error');
	if (isEmpty(value)) {
		setError('dateOfRegistration_error', MANDATORY);
		return false;
	} else if (isDate(value)) {
		setError('dateOfRegistration_error', INVALID_DATE);
		return false;
	}
	setFocusLast('PIDCode');
	return true;
}

function PIDCode_val() {
	var value = $('#PIDCode').val();
	clearError('PIDCode_error');
	if (isEmpty(value)) {
		setError('PIDCode_error', MANDATORY);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('CIF_NO', $('#personId').val());
		validator.setValue('PID_CODE', $('#PIDCode').val());
		validator.setValue('DOC_SL', $('#PIDRegSl').val());
		validator.setValue('MODE', $('#action').val());
		validator.setValue('ACTION', USAGE);
		validator.setClass('patterns.config.web.forms.cmn.epidregbean');
		validator.setMethod('validatePIDCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('PIDCode_error', validator.getValue(ERROR));
			$('#PIDCode_desc').html(EMPTY_STRING);
			return false;
		} else {
			if (validator.getValue(RESULT) == DATA_AVAILABLE) {
				$('#PIDCode_desc').html(validator.getValue("DESCRIPTION"));
			}
		}
	}
	setFocusLast('PIDNumber');
	return true;
}
function PIDNumber_val() {
	var value = $('#PIDNumber').val();
	clearError('PIDNumber_error');
	if (isEmpty(value)) {
		setError('PIDNumber_error', MANDATORY);
		return false;
	} else if (!isValidCode(value, 25)) {
		setError('PIDNumber_error', INVALID_FORMAT);
		return false;
	} else if (isZero(value)) {
		setError('PIDNumber_error', ZERO_CHECK);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('CIF_NO', $('#personId').val());
	validator.setValue('PID_CODE', $('#PIDCode').val());
	validator.setValue('DOC_SL', $('#PIDRegSl').val());
	validator.setValue('DOCUMENT_NUMBER', $('#PIDNumber').val());
	validator.setValue(ACTION, USAGE);
	validator.setClass('patterns.config.web.forms.cmn.epidregbean');
	validator.setMethod('validatePIDNumber');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('PIDNumber_error', validator.getValue(ERROR));
		return false;
	}
	setFocusLast('PIDIssueDate');
	return true;
}

function PIDIssueDate_val() {
	var value = $('#PIDIssueDate').val();
	var PIDIssDate = 0;
	clearError('PIDIssueDate_error');
	validator.setMtm(false);
	validator.setValue('PID_CODE', $('#PIDCode').val());
	validator.setValue('ACTION', USAGE);
	validator.setClass('patterns.config.web.forms.cmn.epidregbean');
	validator.setMethod('validatePIDIssueDate');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('PIDIssueDate_error', validator.getValue(ERROR));
		return false;
	}
	if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		PIDIssDate = validator.getValue("DOC_HAS_ISSUE_DATE");
	}
	if (PIDIssDate == 1) {
		if (isEmpty(value)) {
			setError('PIDIssueDate_error', MANDATORY);
			return false;
		} else if (!isDate(value)) {
			setError('PIDIssueDate_error', INVALID_DATE);
			return false;
		}
		if (isDateGreater(value, getCBD())) {
			setError('PIDIssueDate_error', DATE_LECBD);
			return false;
		}
	} else {
		if (!isEmpty(value)) {
			setError('PIDIssueDate_error', PBS_PID_ISSUEDT_NR);
			return false;
		}
	}
	setFocusLast('PIDExpiryDate');
	return true;
}

function PIDExpiryDate_val() {
	var value = $('#PIDExpiryDate').val();
	var PIDExpDate = 0;
	clearError('PIDExpiryDate_error');
	validator.setMtm(false);
	validator.setValue('PID_CODE', $('#PIDCode').val());
	validator.setValue('ACTION', USAGE);
	validator.setClass('patterns.config.web.forms.cmn.epidregbean');
	validator.setMethod('validatePIDExpiryDate');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('PIDExpiryDate_error', validator.getValue(ERROR));
		return false;
	}
	if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		PIDExpDate = validator.getValue("DOC_HAS_EXPIRY_DATE");
	}
	if (PIDExpDate == 1) {
		if (isEmpty(value)) {
			setError('PIDExpiryDate_error', MANDATORY);
			return false;
		}else if(!isDate(value)){
			setError('PIDExpiryDate_error', INVALID_DATE);
			return false;
		}else if ($('#PIDIssueDate').val()!=EMPTY_STRING &&  isDateLesser(value, $('#PIDIssueDate').val())) {
			setError('PIDExpiryDate_error', PBS_DATE_GE_ISSUEDATE);
			return false;
		}else if (isDateLesser(value, getCBD())) {
			setError('PIDExpiryDate_error', DATE_GECBD);
			return false;
		}
	} else {
		if (!isEmpty(value)) {
			setError('PIDExpiryDate_error', PBS_PID_EXPIRYDT_NR);
			return false;
		}
	}
	setFocusLast('PIDRemarks');
	return true;
}

function PIDRemarks_val() {
	var value = $('#PIDRemarks').val();
	clearError('PIDRemarks_error');
	if (isEmpty(value)) {
		setError('PIDRemarks_error', MANDATORY);
		return false;
	} else if (isWhitespace(value)) {
		setError('PIDRemarks_error', MANDATORY);
		return false;
	} else if (!isValidRemarks(value)) {
		setError('PIDRemarks_error', INVALID_REMARKS);
		return false;
	}
	if ($('#deRegister').is(':checked')) {
		setFocusLast('reasonOfDereg');
	} else {
		setFocusOnSubmit();
	}
	return true;
}
function dateOfDereg_val() {
	var value = $('#dateOfDereg').val();
	clearError('dateOfDereg_error');
	if ($('#deRegister').is(':checked')) {
		if (isEmpty(value)) {
			setError('dateOfDereg_error', MANDATORY);
			return false;
		} else if (isDate(value)) {
			setError('dateOfDereg_error', INVALID_DATE);
			return false;
		}
	} else {
		if (!isEmpty(value)) {
			setError('dateOfDereg_error', HMS_DODREG_NR);
			return false;
		}
	}
	setFocusLast('reasonOfDereg');
	return true;
}
function reasonOfDereg_val() {
	var value = $('#reasonOfDereg').val();
	clearError('reasonOfDereg_error');
	if ($('#deRegister').is(':checked')) {
		if (isEmpty(value)) {
			setError('reasonOfDereg_error', MANDATORY);
			return false;
		} else if (isWhitespace(value)) {
			setError('reasonOfDereg_error', MANDATORY);
			return false;
		} else if (!isValidRemarks(value)) {
			setError('reasonOfDereg_error', INVALID_REMARKS);
			return false;
		}
	} else {
		if (!isEmpty(value)) {
			setError('reasonOfDereg_error', HMS_DREG_REASON_NR);
			return false;
		}
	}
	setFocusOnSubmit();
	return true;
}
