var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'ICASHPARAM';
function init() {
	refreshTBAGrid();
	if (!_redisplay && $('#recordExist').val() == ONE) {
		doGridSelect(getEntityCode());
	}
	setFocusLast('cashGlOfc');
}

function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}



function clearFields() {
	$('#cashGlOfc').val(EMPTY_STRING);
	$('#cashGlOfc_error').html(EMPTY_STRING);
	$('#cashGlOfc_desc').html(EMPTY_STRING);
	clearNonPKFields();
}
function clearNonPKFields() {
	$('#cashGlHeadcash').val(EMPTY_STRING);
	$('#cashGlHeadcash_desc').html(EMPTY_STRING);
	$('#cashGlHeadcash_error').html(EMPTY_STRING);
	$('#counterCashGl').val(EMPTY_STRING);
	$('#counterCashGl_desc').html(EMPTY_STRING);
	$('#counterCashGl_error').html(EMPTY_STRING);
	$('#fieldStaffCashGl').val(EMPTY_STRING);
	$('#fieldStaffCashGl_desc').html(EMPTY_STRING);
	$('#fieldStaffCashGl_error').html(EMPTY_STRING);
	$('#fieldStaffCashGlbanking').val(EMPTY_STRING);
	$('#fieldStaffCashGlbanking_desc').html(EMPTY_STRING);
	$('#fieldStaffCashGlbanking_error').html(EMPTY_STRING);
	$('#interOfcCashTransfer').val(EMPTY_STRING);
	$('#interOfcCashTransfer_desc').html(EMPTY_STRING);
	$('#interOfcCashTransfer_error').html(EMPTY_STRING);
	$('#transitGl').val(EMPTY_STRING);
	$('#transitGl_desc').html(EMPTY_STRING);
	$('#transitGl_error').html(EMPTY_STRING);
	$('#vaultWithTrans').val(EMPTY_STRING);
	$('#vaultWithTrans_desc').html(EMPTY_STRING);
	$('#vaultWithTrans_error').html(EMPTY_STRING);
	$('#vaultDepTrans').val(EMPTY_STRING);
	$('#vaultDepTrans_desc').html(EMPTY_STRING);
	$('#vaultDepTrans_error').html(EMPTY_STRING);
	$('#cashtoCashTrans').val(EMPTY_STRING);
	$('#cashtoCashTrans_desc').html(EMPTY_STRING);
	$('#cashtoCashTrans_error').html(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
}

function doHelp(id) {
	switch (id) {
	case 'cashGlOfc':
	case 'cashGlHeadcash':
	case 'counterCashGl':
	case 'fieldStaffCashGl':
	case 'fieldStaffCashGlbanking':
	case 'interOfcCashTransfer':
	case 'transitGl':
		help('COMMON', 'HLP_GL_CODE_POSTING', $('#' + id).val(), EMPTY_STRING,
				$('#' + id));
		break;
	case 'vaultWithTrans':
	case 'vaultDepTrans':
	case 'cashtoCashTrans':
		help('COMMON', 'HLP_BTRAN_CODE_M', $('#' + id).val(), EMPTY_STRING,
				$('#' + id));
		break;

	}
}

function loadData() {
	$('#cashGlOfc').val(validator.getValue('OFFICE_GL_HEAD'));
	$('#cashGlOfc_desc').html(validator.getValue('F1_DESCRIPTION'));
	$('#cashGlHeadcash').val(validator.getValue('HEAD_CASHIER_GL_HEAD'));
	$('#cashGlHeadcash_desc').html(validator.getValue('F2_DESCRIPTION'));
	$('#counterCashGl').val(validator.getValue('COUNTER_CASH_GL_HEAD'));
	$('#counterCashGl_desc').html(validator.getValue('F3_DESCRIPTION'));
	$('#fieldStaffCashGl').val(validator.getValue('FIELD_STAFF_CASH_GL_HEAD'));
	$('#fieldStaffCashGl_desc').html(validator.getValue('F4_DESCRIPTION'));
	$('#fieldStaffCashGlbanking').val(validator.getValue('BANK_CORR_CASH_GL_HEAD'));
	$('#fieldStaffCashGlbanking_desc').html(validator.getValue('F5_DESCRIPTION'));
	$('#interOfcCashTransfer').val(validator.getValue('INTER_OFF_CASH_TRF_GL_HEAD'));
	$('#interOfcCashTransfer_desc').html(validator.getValue('F6_DESCRIPTION'));
	$('#transitGl').val(validator.getValue('SAME_OFFICE_CASH_TRF_GL'));
	$('#transitGl_desc').html(validator.getValue('F10_DESCRIPTION'));
	setCheckbox('dominationDtlReg', validator.getValue('DENOMINATION_REQ'));
	$('#vaultWithTrans').val(validator.getValue('VAULT_WITHDRAWAL_FTRAN_CODE'));
	$('#vaultWithTrans_desc').html(validator.getValue('F7_DESCRIPTION'));
	$('#vaultDepTrans').val(validator.getValue('VAULT_DEPOSIT_FTRAN_CODE'));
	$('#vaultDepTrans_desc').html(validator.getValue('F8_DESCRIPTION'));
	$('#cashtoCashTrans').val(validator.getValue('CTC_FTRAN_CODE'));
	$('#cashtoCashTrans_desc').html(validator.getValue('F9_DESCRIPTION'));
	$('#remarks').val(validator.getValue('REMARKS'));
	resetLoading();
}
function view(source, primaryKey) {
	hideParent('cmn/qcashparam', source, primaryKey);
	resetLoading();
}
function validate(id) {
	switch (id) {
	case 'cashGlOfc':
		cashGlOfc_val();
		break;
	case 'cashGlHeadcash':
		cashGlHeadcash_val();
		break;
	case 'counterCashGl':
		counterCashGl_val();
		break;
	case 'fieldStaffCashGl':
		fieldStaffCashGl_val();
		break;
	case 'fieldStaffCashGlbanking':
		fieldStaffCashGlbanking_val();
		break;
	case 'interOfcCashTransfer':
		interOfcCashTransfer_val();
		break;
	case 'transitGl':
		transitGl_val();
		break;
	case 'dominationDtlReg':
		dominationDtlReg_val();
		break;
	case 'vaultWithTrans':
		vaultWithTrans_val();
		break;
	case 'vaultDepTrans':
		vaultDepTrans_val();
		break;
	case 'cashtoCashTrans':
		cashtoCashTrans_val();
		break;
	case 'remarks':
		remarks_val();
		break;
	}
}

function backtrack(id) {
	switch (id) {
	case 'cashGlOfc':
		setFocusLast('cashGlOfc');
		break;
	case 'cashGlHeadcash':
		setFocusLast('cashGlOfc');
		break;
	case 'counterCashGl':
		setFocusLast('cashGlHeadcash');
		break;
	case 'fieldStaffCashGl':
		setFocusLast('counterCashGl');
		break;
	case 'fieldStaffCashGlbanking':
		setFocusLast('fieldStaffCashGl');
		break;
	case 'interOfcCashTransfer':
		setFocusLast('fieldStaffCashGlbanking');
		break;
	case 'transitGl':
		setFocusLast('interOfcCashTransfer');
		break;
	case 'dominationDtlReg':
		setFocusLast('transitGl');
		break;
	case 'vaultWithTrans':
		setFocusLast('dominationDtlReg');
		break;
	case 'vaultDepTrans':
		setFocusLast('vaultWithTrans');
		break;
	case 'cashtoCashTrans':
		setFocusLast('vaultDepTrans');
		break;
	case 'remarks':
		setFocusLast('cashtoCashTrans');
		break;
	}
}

function cashGlOfc_val() {
	var value = $('#cashGlOfc').val();
	clearError('cashGlOfc_error');
	if (isEmpty(value)) {
		setError('cashGlOfc_error', MANDATORY);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('GL_HEAD_CODE', $('#cashGlOfc').val());
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.cmn.icashparambean');
		validator.setMethod('validateGlHeadCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('cashGlOfc_error', validator.getValue(ERROR));
			$('#cashGlOfc_desc').html(EMPTY_STRING);
			return false;
		}
		$('#cashGlOfc_desc').html(validator.getValue('DESCRIPTION'));
	}
	setFocusLast('cashGlHeadcash');
	return true;
}

function cashGlHeadcash_val() {
	var value = $('#cashGlHeadcash').val();
	clearError('cashGlHeadcash_error');
	if (isEmpty(value)) {
		setError('cashGlHeadcash_error', MANDATORY);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('GL_HEAD_CODE', $('#cashGlHeadcash').val());
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.cmn.icashparambean');
		validator.setMethod('validateGlHeadCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('cashGlHeadcash_error', validator.getValue(ERROR));
			$('#cashGlHeadcash_desc').html(EMPTY_STRING);
			return false;
		}
		$('#cashGlHeadcash_desc').html(validator.getValue('DESCRIPTION'));
	}
	setFocusLast('counterCashGl');
	return true;
}

function counterCashGl_val() {
	var value = $('#counterCashGl').val();
	clearError('counterCashGl_error');
	if (!isEmpty(value)) {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('GL_HEAD_CODE', $('#counterCashGl').val());
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.cmn.icashparambean');
		validator.setMethod('validateGlHeadCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('counterCashGl_error', validator.getValue(ERROR));
			$('#counterCashGl_desc').html(EMPTY_STRING);
			return false;
		}
		$('#counterCashGl_desc').html(validator.getValue('DESCRIPTION'));
	}
	setFocusLast('fieldStaffCashGl');
	return true;
}

function fieldStaffCashGl_val() {
	var value = $('#fieldStaffCashGl').val();
	clearError('fieldStaffCashGl_error');
	if (isEmpty(value)) {
		setError('fieldStaffCashGl_error', MANDATORY);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('GL_HEAD_CODE', $('#fieldStaffCashGl').val());
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.cmn.icashparambean');
		validator.setMethod('validateGlHeadCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('fieldStaffCashGl_error', validator.getValue(ERROR));
			$('#fieldStaffCashGl_desc').html(EMPTY_STRING);
			return false;
		}
		$('#fieldStaffCashGl_desc').html(validator.getValue('DESCRIPTION'));
	}
	setFocusLast('fieldStaffCashGlbanking');
	return true;
}

function fieldStaffCashGlbanking_val() {
	var value = $('#fieldStaffCashGlbanking').val();
	clearError('fieldStaffCashGlbanking_error');
	if (isEmpty(value)) {
		setError('fieldStaffCashGlbanking_error', MANDATORY);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('GL_HEAD_CODE', $('#fieldStaffCashGlbanking').val());
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.cmn.icashparambean');
		validator.setMethod('validateGlHeadCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('fieldStaffCashGlbanking_error', validator.getValue(ERROR));
			$('#fieldStaffCashGlbanking_desc').html(EMPTY_STRING);
			return false;
		}
		$('#fieldStaffCashGlbanking_desc').html(
				validator.getValue('DESCRIPTION'));
	}
	setFocusLast('interOfcCashTransfer');
	return true;
}

function interOfcCashTransfer_val() {
	var value = $('#interOfcCashTransfer').val();
	clearError('interOfcCashTransfer_error');
	if (isEmpty(value)) {
		setError('interOfcCashTransfer_error', MANDATORY);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('GL_HEAD_CODE', $('#interOfcCashTransfer').val());
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.cmn.icashparambean');
		validator.setMethod('validateGlHeadCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('interOfcCashTransfer_error', validator.getValue(ERROR));
			$('#interOfcCashTransfer_desc').html(EMPTY_STRING);
			return false;
		}
		$('#interOfcCashTransfer_desc').html(validator.getValue('DESCRIPTION'));
	}
	setFocusLast('transitGl');
	return true;
}

function transitGl_val() {
	var value = $('#transitGl').val();
	clearError('transitGl_error');
	if (isEmpty(value)) {
		setError('transitGl_error', MANDATORY);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('GL_HEAD_CODE', $('#transitGl').val());
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.cmn.icashparambean');
		validator.setMethod('validateGlHeadCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('transitGl_error', validator.getValue(ERROR));
			$('#transitGl_desc').html(EMPTY_STRING);
			return false;
		}
		$('#transitGl_desc').html(validator.getValue('DESCRIPTION'));
	}
	setFocusLast('dominationDtlReg');
	return true;
}

function dominationDtlReg_val() {
	setFocusLast('vaultWithTrans');
	return true;
}
function vaultWithTrans_val() {
	var value = $('#vaultWithTrans').val();
	clearError('vaultWithTrans_error');
	if (isEmpty(value)) {
		setError('vaultWithTrans_error', MANDATORY);
		return false;
	}
	if (!isNumeric(value)) {
		setError('vaultWithTrans_error', INVALID_NUMBER);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('BTRAN_CODE', $('#vaultWithTrans').val());
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.cmn.icashparambean');
		validator.setMethod('validateFinancialTransCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('vaultWithTrans_error', validator.getValue(ERROR));
			$('#vaultWithTrans_desc').html(EMPTY_STRING);
			return false;
		}
		$('#vaultWithTrans_desc').html(validator.getValue('DESCRIPTION'));
	}
	setFocusLast('vaultDepTrans');
	return true;
}

function vaultDepTrans_val() {
	var value = $('#vaultDepTrans').val();
	clearError('vaultDepTrans_error');
	if (isEmpty(value)) {
		setError('vaultDepTrans_error', MANDATORY);
		return false;
	}
	if (!isNumeric(value)) {
		setError('vaultDepTrans_error', INVALID_NUMBER);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('BTRAN_CODE', $('#vaultDepTrans').val());
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.cmn.icashparambean');
		validator.setMethod('validateFinancialTransCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('vaultDepTrans_error', validator.getValue(ERROR));
			$('#vaultDepTrans_desc').html(EMPTY_STRING);
			return false;
		}
		$('#vaultDepTrans_desc').html(validator.getValue('DESCRIPTION'));
	}
	setFocusLast('cashtoCashTrans');
	return true;
}

function cashtoCashTrans_val() {
	var value = $('#cashtoCashTrans').val();
	clearError('cashtoCashTrans_error');
	if (isEmpty(value)) {
		setError('cashtoCashTrans_error', MANDATORY);
		return false;
	}
	if (!isNumeric(value)) {
		setError('cashtoCashTrans_error', INVALID_NUMBER);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('BTRAN_CODE', $('#cashtoCashTrans').val());
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.cmn.icashparambean');
		validator.setMethod('validateFinancialTransCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('cashtoCashTrans_error', validator.getValue(ERROR));
			$('#cashtoCashTrans_desc').html(EMPTY_STRING);
			return false;
		}
		$('#cashtoCashTrans_desc').html(validator.getValue('DESCRIPTION'));
	}
	setFocusLast('remarks');
	return true;
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', INVALID_REMARKS);
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', INVALID_REMARKS);
			return false;
		}
	}
	if ($('#action').val() == MODIFY) {
		if (isEmpty(value)) {
			setError('remarks_error', MANDATORY);
			return false;
		}
	}
	setFocusOnSubmit();
}
