var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MGEOUNITSTRUC';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}
function clearFields() {
	$('#geoUnitStructureCode').val(EMPTY_STRING);
	clearNonPKFields();
}
function clearNonPKFields() {
	$('#description').val(EMPTY_STRING);
	$('#conciseDescription').val(EMPTY_STRING);
	$('#parentgeoUnitStructureCode').val(EMPTY_STRING);
	$('#parentgeoUnitStructureCode_desc').html(EMPTY_STRING);
	setCheckbox('enabled', NO);
	setCheckbox('requireaUnitType', NO);
	setCheckbox('lastHierarchy', NO);
	setCheckbox('dataPatternRequired', NO);
	$('#dataInputPattern').val(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
}
function loadData() {
	$('#geoUnitStructureCode').val(validator.getValue('GEO_UNIT_STRUC_CODE'));
	$('#description').val(validator.getValue('DESCRIPTION'));
	$('#conciseDescription').val(validator.getValue('CONCISE_DESCRIPTION'));
	$('#parentgeoUnitStructureCode').val(validator.getValue('PARENT_GUS_CODE'));
	$('#parentgeoUnitStructureCode_desc').html(validator.getValue('F1_DESCRIPTION'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	setCheckbox('lastHierarchy', validator.getValue('LAST_IN_HIERARCHY'));
	setCheckbox('requireaUnitType', validator.getValue('UNIT_TYPE_REQ'));
	setCheckbox('dataPatternRequired', validator.getValue('DATA_PATTERN_REQ'));
	if ($('#dataPatternRequired').is(':checked')) {
		setCheckbox('dataPatternRequired', '1');
		$('#dataInputPattern').attr('readonly', false);
		$('#dataInputPattern').val(validator.getValue('DATA_PATTERN'));
	}
	$('#remarks').val(validator.getValue('REMARKS'));
	loadAuditFields(validator);

}