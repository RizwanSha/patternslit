var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MASSETFIELDS';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}

function clearFields() {
	$('#fieldId').val(EMPTY_STRING);
	$('#fieldId_desc').html(EMPTY_STRING);
	clearNonPKFields();
}
function clearNonPKFields() {
	$('#name').val(EMPTY_STRING);
	$('#shortName').val(EMPTY_STRING);
	$('#fieldFor').val($("#fieldFor option:first-child").val());
	$('#fieldValueType').val($("#fieldValueType option:first-child").val());
	$('#fieldValueType_error').html(EMPTY_STRING);
	$('#fieldSize').val(EMPTY_STRING);
	$('#noOfFractionDigits').val(EMPTY_STRING);
	$('#fieldDedicatedToCustomerId').val(EMPTY_STRING);
	$('#fieldDedicatedToCustomerId_desc').html(EMPTY_STRING);
	$('#fieldAssetType').val(EMPTY_STRING);
	$('#fieldAssetType_desc').html(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
}
function loadData() {
	$('#fieldId').val(validator.getValue('FIELD_ID'));
	$('#name').val(validator.getValue('DESCRIPTION'));
	$('#shortName').val(validator.getValue('CONCISE_DESCRIPTION'));
	$('#fieldFor').val(validator.getValue('FIELD_FOR'));
	$('#fieldValueType').val(validator.getValue('FIELD_VALUE_TYPE'));
	$('#fieldSize').val(validator.getValue('FIELD_SIZE'));
	$('#noOfFractionDigits').val(validator.getValue('FIELD_DECIMALS'));
	$('#fieldDedicatedToCustomerId').val(validator.getValue('CUSTOMER_ID'));
	$('#fieldDedicatedToCustomerId_desc').html(validator.getValue('F1_CUSTOMER_NAME'));
	$('#fieldAssetType').val(validator.getValue('ASSET_TYPE_CODE'));
	$('#fieldAssetType_desc').html(validator.getValue('F2_DESCRIPTION'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadAuditFields(validator);
	resetLoading();
}
