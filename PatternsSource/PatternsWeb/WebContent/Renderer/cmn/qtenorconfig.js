var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MTENORCONFIG';

function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}

}
function loads() {
	loadGridQuery(CURRENT_PROGRAM_ID, 'MTENORCONFIG_GRID', mtenorconfig_innerGrid);
}

function clearFields() {
	$('#tenorSpecificationCode').val(EMPTY_STRING);
	$('#tenorSpecificationCode_desc').html(EMPTY_STRING);
	$('#tenorSpecificationCode_error').html(EMPTY_STRING);
	$('#efftDate').val(EMPTY_STRING);
	$('#efftDate_error').html(EMPTY_STRING);
	clearNonPKFields();
	mtenorconfig_innerGrid.setColumnHidden(0, true);
}

function clearNonPKFields() {
	setCheckbox('enabled', YES);
	$('#enabled_error').html(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
	mtenorconfig_innerGrid.clearAll();
}

function clearNonPKFields() {
	$('#profileLimit_error').html(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
}
function loadData() {
	$('#tenorSpecificationCode').val(validator.getValue('TENOR_SPEC_CODE'));
	$('#tenorSpecificationCode_desc').html(validator.getValue('F1_DESCRIPTION'));
	$('#efftDate').val(validator.getValue('EFF_DATE'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadAuditFields(validator);
	loadGrid();
}

function loadGrid() {
	SOURCE_VALUE = _tableType;
	PK_VALUE = _primaryKey;
	if (_tableType == MAIN) {
		loads();
	} else if (_tableType == TBA) {
		loads();
	}
}