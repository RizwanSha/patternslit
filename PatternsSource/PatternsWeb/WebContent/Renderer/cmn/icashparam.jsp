<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.cmn.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/cmn/icashparam.js" />
	</web:dependencies>
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="icashparam.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/cmn/icashparam" id="icashparam" method="POST">
				<web:viewContent id="po_view1">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="icashparam.cashGlOfc" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:code property="cashGlOfc" id="cashGlOfc" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="icashparam.cashGlHeadcash" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:code property="cashGlHeadcash" id="cashGlHeadcash" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="icashparam.counterCashGl" var="program" />
										</web:column>
										<web:column>
											<type:code property="counterCashGl" id="counterCashGl" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="icashparam.fieldStaffCashGl" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:code property="fieldStaffCashGl" id="fieldStaffCashGl" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="icashparam.fieldStaffCashGlbanking" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:code property="fieldStaffCashGlbanking" id="fieldStaffCashGlbanking" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="icashparam.interOfcCashTransfer" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:code property="interOfcCashTransfer" id="interOfcCashTransfer" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="icashparam.transitgl" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:code property="transitGl" id="transitGl" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="icashparam.dominationDtlReg" var="program" />
										</web:column>
										<web:column>
											<type:checkbox property="dominationDtlReg" id="dominationDtlReg" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="icashparam.vaultwithtrans" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:bankingTranCode property="vaultWithTrans" id="vaultWithTrans" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="icashparam.vaultdeptrans" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:bankingTranCode property="vaultDepTrans" id="vaultDepTrans" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="icashparam.cashtocashtrans" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:bankingTranCode property="cashtoCashTrans" id="cashtoCashTrans" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="form.remarks" var="common" />
										</web:column>
										<web:column>
											<type:remarks property="remarks" id="remarks" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column span="2">
											<web:submitReset />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
						</web:sectionBlock>
					</web:dividerBlock>
				</web:viewContent>
				<web:element property="command" />
				<web:element property="action" />
				<web:element property="recordExist" />
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
	<web:TBAGrid />
	<web:contextSearch />
</web:fragment>
