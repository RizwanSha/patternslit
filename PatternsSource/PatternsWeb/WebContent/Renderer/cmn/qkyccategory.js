var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MKYCCATEGORY';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}
function clearFields() {
	$('#kycCategoryCode').val(EMPTY_STRING);
	$('#description').val(EMPTY_STRING);
	$('#conciseDescription').val(EMPTY_STRING);
	$('#categoryType').val(EMPTY_STRING);
	setCheckbox('proofOfIdentify', NO);
	setCheckbox('proofOfAddress', NO);
	setCheckbox('enabled', NO);
	$('#remarks').val(EMPTY_STRING);

}
function loadData() {
	$('#kycCategoryCode').val(validator.getValue('KYC_CATEGORY'));
	$('#description').val(validator.getValue('DESCRIPTION'));
	$('#conciseDescription').val(validator.getValue('CONCISE_DESCRIPTION'));
	$('#categoryType').val(validator.getValue('CATEGORY_FOR'));
	setCheckbox('proofOfIdentify', validator.getValue('POI_REQD'));
	setCheckbox('proofOfAddress', validator.getValue('POA_REQD'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadAuditFields(validator);
}
