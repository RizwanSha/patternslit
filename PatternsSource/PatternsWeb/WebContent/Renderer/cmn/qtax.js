var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MTAX';

function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
	tax_innerGrid.setColumnHidden(0, true);
}

function loads() {
	loadGridQuery(CURRENT_PROGRAM_ID, 'MTAX_GRID', tax_innerGrid);
}

function clearFields() {
	$('#stateCode').val(EMPTY_STRING);
	$('#stateCode_desc').html(EMPTY_STRING);
	$('#taxCode').val(EMPTY_STRING);
	$('#taxCode_desc').html(EMPTY_STRING);
	$('#description').val(EMPTY_STRING);
	$('#conciseDescription').val(EMPTY_STRING);
	$('#taxType_error').html(EMPTY_STRING);
	$('#taxOnTaxCode_desc').html(EMPTY_STRING);
	$('#taxOnTaxCode_error').html(EMPTY_STRING);
	setCheckbox('taxBorne', NO);
	$('#taxIpGLAcctHead').val(EMPTY_STRING);
	$('#taxIpGLAcctHead_desc').html(EMPTY_STRING);
	$('#taxPayGLAcctHead').val(EMPTY_STRING);
	$('#taxPayGLAcctHead_desc').html(EMPTY_STRING);
	$('#noOfTaxSchedules').val(EMPTY_STRING);
	setCheckbox('prdctWiseTaxInclusive', NO);
	setCheckbox('cstmerWiseTaxInclusive', NO);
	setCheckbox('leaseWiseTaxInclusive', NO);
	$('#remarks').val(EMPTY_STRING);
	tax_innerGrid.clearAll();
}

function loadData() {
	$('#stateCode').val(validator.getValue('STATE_CODE'));
	$('#stateCode_desc').html(validator.getValue('F1_DESCRIPTION'));
	$('#taxCode').val(validator.getValue('TAX_CODE'));
	$('#description').val(validator.getValue('DESCRIPTION'));
	$('#conciseDescription').val(validator.getValue('CONCISE_DESCRIPTION'));
	$('#taxType').val(validator.getValue('TAX_TYPE'));
	$('#taxOn').val(validator.getValue('TAX_ON'));
	$('#taxOnTaxCode').val(validator.getValue('TAX_ON_TAX_CODE'));
	$('#taxOnTaxCode_desc').html(validator.getValue('F4_DESCRIPTION'));
	$('#taxIpGLAcctHead').val(validator.getValue('TAX_INPUT_GL_HEAD_CODE'));
	$('#taxIpGLAcctHead_desc').html(validator.getValue('F2_DESCRIPTION'));
	$('#taxPayGLAcctHead').val(validator.getValue('TAX_PAY_GL_HEAD_CODE'));
	$('#taxPayGLAcctHead_desc').html(validator.getValue('F3_DESCRIPTION'));
	$('#noOfTaxSchedules').val(validator.getValue('NOF_TAX_SCHEDULES'));
	setCheckbox('prdctWiseTaxInclusive', validator.getValue('TAX_BYUS_PROD_ALLOWED'));
	setCheckbox('cstmerWiseTaxInclusive', validator.getValue('TAX_BYUS_CUST_ALLOWED'));
	setCheckbox('leaseWiseTaxInclusive', validator.getValue('TAX_BYUS_LEASE_ALLOWED'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadGrid();
	loadAuditFields(validator);
}

function loadGrid() {
	SOURCE_VALUE = _tableType;
	PK_VALUE = _primaryKey;
	if (_tableType == MAIN) {
		loads();
	} else if (_tableType == TBA) {
		loads();
	}
}
