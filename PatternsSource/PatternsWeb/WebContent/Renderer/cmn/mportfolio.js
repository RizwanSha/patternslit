var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MPORTFOLIO';
function init() {
	refreshTBAGrid();
	refreshQueryGrid();
	if (_redisplay) {
		if ($('#action').val() == ADD) {
			$('#enabled').prop('disabled', true);
			setCheckbox('enabled', YES);
			$('#status').prop('selectedIndex', 0);
			$('#status').prop('disabled', true);
		}
	}
}

function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'MPORTFOLIO_MAIN', queryGrid);
}

function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}

function clearFields() {
	$('#portFolioCode').val(EMPTY_STRING);
	$('#portFolioCode_error').html(EMPTY_STRING);
	$('#portFolioCode_desc').html(EMPTY_STRING);
	clearNonPKFields();
}

function clearNonPKFields() {
	$('#description').val(EMPTY_STRING);
	$('#description').val(EMPTY_STRING);
	$('#description_error').html(EMPTY_STRING);
	$('#conciseDescription').val(EMPTY_STRING);
	$('#conciseDescription_error').html(EMPTY_STRING);
	$('#parentPortfolioCode').val(EMPTY_STRING);
	$('#parentPortfolioCode_error').html(EMPTY_STRING);
	$('#parentPortfolioCode_desc').html(EMPTY_STRING);
	$('#enabled_error').html(EMPTY_STRING);
	$('#status').prop('selectedIndex', 0);
	$('#status_error').html(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
}

function add() {
	$('#portFolioCode').focus();
	setCheckbox('enabled', YES);
	$('#enabled').prop('disabled', true);
	$('#status').prop('selectedIndex', 0);
	$('#status').prop('disabled', true);
}

function modify() {
	$('#portFolioCode').focus();
	$('#status').prop('selectedIndex', 0);
	$('#status').prop('disabled', false);
	if ($('#action').val() == MODIFY) {
		$('#enabled').prop('disabled', false);
	} else {
		$('#enabled').prop('disabled', true);
	}
}

function doHelp(id) {
	switch (id) {
	case 'portFolioCode':
		help('COMMON', 'HLP_PORTFOLIO_CODE', $('#portFolioCode').val(), EMPTY_STRING, $('#portFolioCode'));
		break;
	case 'parentPortfolioCode':
		help('COMMON', 'HLP_PORTFOLIO_CODE', $('#parentPortfolioCode').val(), EMPTY_STRING, $('#parentPortfolioCode'));
		break;
	}
}

function loadData() {
	$('#portFolioCode').val(validator.getValue('PORTFOLIO_CODE'));
	$('#description').val(validator.getValue('DESCRIPTION'));
	$('#conciseDescription').val(validator.getValue('CONCISE_DESCRIPTION'));
	$('#parentPortfolioCode').val(validator.getValue('PARENT_PORTFOLIO_CODE'));
	$('#parentPortfolioCode_desc').html(validator.getValue('F1_DESCRIPTION'));
	$('#status').val(validator.getValue('STATUS'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	resetLoading();
}

function view(source, primaryKey) {
	hideParent('cmn/qportfolio', source, primaryKey);
	resetLoading();
}

function doclearfields(id) {
	switch (id) {
	case 'portFolioCode':
		if (isEmpty($('#portFolioCode').val())) {
			clearFields();
			break;
		}
	}
}

function change(id) {
	switch (id) {
	case 'status':
		status_val(false);
		break;
	}
}

function checkclick(id) {
	switch (id) {
	case 'enabled':
		enabled_val(false);
		break;
	}
}

function validate(id) {
	switch (id) {
	case 'portFolioCode':
		portFolioCode_val();
		break;
	case 'description':
		description_val();
		break;
	case 'conciseDescription':
		conciseDescription_val();
		break;
	case 'parentPortfolioCode':
		parentPortfolioCode_val();
		break;
	case 'status':
		status_val(true);
		break;
	case 'enabled':
		enabled_val(true);
		break;
	case 'remarks':
		remarks_val();
		break;

	}
}

function backtrack(id) {
	switch (id) {
	case 'portFolioCode':
		setFocusLast('portFolioCode');
		break;
	case 'description':
		setFocusLast('portFolioCode');
		break;
	case 'conciseDescription':
		setFocusLast('description');
		break;
	case 'parentPortfolioCode':
		setFocusLast('conciseDescription');
		break;
	case 'status':
		setFocusLast('parentPortfolioCode');
		break;
	case 'enabled':
		setFocusLast('status');
		break;
	case 'remarks':
		if ($('#action').val() == MODIFY) {
			setFocusLast('enabled');
		} else {
			setFocusLast('parentPortfolioCode');
		}
		break;
	}

}

function portFolioCode_val() {
	var value = $('#portFolioCode').val();
	clearError('portFolioCode_error');
	if (isEmpty(value)) {
		setError('portFolioCode_error', MANDATORY);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('PORTFOLIO_CODE', value);
	validator.setValue(ACTION, $('#action').val());
	validator.setClass('patterns.config.web.forms.cmn.mportfoliobean');
	validator.setMethod('validatePortFolioCode');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('portFolioCode_error', validator.getValue(ERROR));
		$('#portFolioCode_desc').html(EMPTY_STRING);
		return false;
	} else {
		if ($('#action').val() == MODIFY) {
			PK_VALUE = getEntityCode() + PK_SEPERATOR + $('#portFolioCode').val();
			if (!loadPKValues(PK_VALUE, 'portFolioCode_error')) {
				return false;
			}
		}
	}
	setFocusLast('description');
	return true;
}

function description_val() {
	var value = $('#description').val();
	clearError('description_error');
	if (isEmpty(value)) {
		setError('description_error', MANDATORY);
		return false;
	}
	if (!isValidDescription(value)) {
		setError('description_error', INVALID_DESCRIPTION);
		return false;
	}
	setFocusLast('conciseDescription');
	return true;
}

function conciseDescription_val() {
	var value = $('#conciseDescription').val();
	clearError('conciseDescription_error');
	if (isEmpty(value)) {
		setError('conciseDescription_error', MANDATORY);
		return false;
	}
	if (!isValidConciseDescription(value)) {
		setError('conciseDescription_error', INVALID_CONCISE_DESCRIPTION);
		return false;
	}
	setFocusLast('parentPortfolioCode');
	return true;
}

function parentPortfolioCode_val() {
	var value = $('#parentPortfolioCode').val();
	clearError('parentPortfolioCode_error');
	if (!isEmpty(value)) {
		if (value == $('#portFolioCode').val()) {
			setError('parentPortfolioCode_error', PBS_PARENT_CANT_SAME);
			return false;
		}
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('FOLIO_CODE', $('#portFolioCode').val());
		validator.setValue('PORTFOLIO_CODE', value);
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.cmn.mportfoliobean');
		validator.setMethod('validateParentPortfolioCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('parentPortfolioCode_error', validator.getValue(ERROR));
			$('#parentPortfolioCode_desc').html(EMPTY_STRING);
			return false;
		}
		$('#parentPortfolioCode_desc').html(validator.getValue("DESCRIPTION"));
	}
	if ($('#action').val() == ADD)
		setFocusLast('remarks');
	else
		setFocusLast('status');
	return true;
}

function status_val(valMode) {
	var value = $('#status').val();
	clearError('status_error');
	if ($('#action').val() == MODIFY) {
		if (isEmpty(value)) {
			setError('status_error', MANDATORY);
			return false;
		}
		if (value == 'C')
			checkParentPortFolio('status');
	}
	if (valMode)
		setFocusLast('enabled');
	return true;

}

function checkParentPortFolio(errorField) {
	clearError('portFolioCode_error');
	if (isEmpty($('#portFolioCode').val())) {
		setError('portFolioCode_error', MANDATORY);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('PORTFOLIO_CODE', $('#portFolioCode').val());
	validator.setClass('patterns.config.web.forms.cmn.mportfoliobean');
	validator.setMethod('validateCheckParentDisabled');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError(errorField + '_error', validator.getValue(ERROR));
		return false;
	}
}

function enabled_val(valMode) {
	clearError('enabled_error');
	if ($('#action').val() == MODIFY && !$('#enabled').is(':checked'))
		checkParentPortFolio('enabled');
	if (valMode)
		setFocusLast('remarks');
	return true;
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', INVALID_REMARKS);
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', INVALID_REMARKS);
			return false;
		}
	}
	if ($('#action').val() == MODIFY) {
		if (isEmpty(value)) {
			setError('remarks_error', MANDATORY);
			return false;
		}
	}
	setFocusOnSubmit();
	return true;
}
