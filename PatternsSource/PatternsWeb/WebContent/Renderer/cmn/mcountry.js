var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MCOUNTRY';

function init() {
	refreshTBAGrid();
	refreshQueryGrid();
	if (_redisplay) {
		if ($('#action').val() == ADD) {
			$('#enabled').prop('disabled', true);
			setCheckbox('enabled', YES);
		}
	}
}
function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'MCOUNTRY_ADMIN');
}
function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}
function doHelp(id) {
	switch (id) {
	case 'countryCode':
		help('COMMON', 'HLP_COUNTRY_M', $('#countryCode').val(), EMPTY_STRING, $('#countryCode'));
		break;
	case 'geoUnitStructureCode':
		help('COMMON', 'HLP_GEOUNITSTRUC_REQ', $('#geoUnitStructureCode').val(), EMPTY_STRING, $('#geoUnitStructureCode'));
		break;
	}
}

function doclearfields(id) {
	switch (id) {
	case 'countryCode':
		if (isEmpty($('#countryCode').val())) {
			$('#countryCode_error').html(EMPTY_STRING);
			clearNonPKFields();
			break;
		}
	}
}

function clearFields() {

	$('#countryCode').val(EMPTY_STRING);
	$('#countryCode_error').html(EMPTY_STRING);
	clearNonPKFields();
}

function clearNonPKFields() {
	$('#countryName').val(EMPTY_STRING);
	$('#countryName_error').html(EMPTY_STRING);
	$('#shortName').val(EMPTY_STRING);
	$('#shortName_error').html(EMPTY_STRING);
	$('#geoUnitStructureCode').val(EMPTY_STRING);
	$('#geoUnitStructureCode_desc').html(EMPTY_STRING);
	$('#geoUnitStructureCode_error').html(EMPTY_STRING);
//	setCheckbox('enabled', YES);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);

}
function add() {

	$('#countryCode').focus();
	setCheckbox('enabled', YES);
	$('#enabled').prop('disabled', true);
}

function modify() {

	$('#countryCode').focus();
	if ($('#action').val() == MODIFY) {
		$('#enabled').prop('disabled', false);
	} else {
		$('#enabled').prop('disabled', true);
	}
}

function loadData() {
	$('#countryCode').val(validator.getValue('COUNTRY_CODE'));
	$('#countryName').val(validator.getValue('DESCRIPTION'));
	$('#shortName').val(validator.getValue('CONCISE_DESCRIPTION'));
	$('#geoUnitStructureCode').val(validator.getValue('GEO_UNIT_STRUC_CODE'));
	$('#geoUnitStructureCode_desc').html(validator.getValue('F1_DESCRIPTION'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	resetLoading();

}

function view(source, primaryKey) {
	hideParent('cmn/qcountry', source, primaryKey);
	resetLoading();
}

function countryCode_val() {
	var value = $('#countryCode').val();
	clearError('countryCode_error');

	if (isEmpty(value)) {
		setError('countryCode_error', HMS_MANDATORY);
		setFocusLast('countryCode');
		return false;
	} else if ($('#action').val() == ADD && !isValidCode(value)) {
		setError('countryCode_error', HMS_INVALID_FORMAT);
		setFocusLast('countryCode');
		return false;
	} else {
		if ($('#action').val() == ADD) {
			validator.clearMap();
			validator.setMtm(false);
			validator.setValue('COUNTRY_CODE', $('#countryCode').val());
			validator.setValue('ACTION', $('#action').val());
			validator.setClass('patterns.config.web.forms.cmn.mcountrybean');
			validator.setMethod('validateCountryCode');
			validator.sendAndReceive();

			if (validator.getValue(ERROR) != EMPTY_STRING) {
				setError('countryCode_error', validator.getValue(ERROR));
				setFocusLast('countryCode');
				return false;
			}
		} else {
			PK_VALUE = $('#countryCode').val();
			if(!loadPKValues(PK_VALUE, 'countryCode_error')){
				return false;
			}
			
		}
	}
	setFocusLast('countryName');
	return true;
}
function geographicalUnitStructureCode_val() {
	var value = $('#geoUnitStructureCode').val();
	clearError('geoUnitStructureCode_error');
	if (!isEmpty(value)) {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('GEO_UNIT_STRUC_CODE', $('#geoUnitStructureCode').val());
		validator.setValue('ACTION', USAGE);
		validator.setClass('patterns.config.web.forms.cmn.mcountrybean');
		validator.setMethod('validateGeographicalUnitStructureCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('geoUnitStructureCode_error', validator.getValue(ERROR));
			setFocusLast('geoUnitStructureCode');
			return false;
		} else {
			$('#geoUnitStructureCode_desc').html(validator.getValue('DESCRIPTION'));
		}

	}
	if ($('#action').val() == ADD) {
		setFocusLast('remarks');
	} else {
		setFocusLast('enabled');
	}
	return true;
}

function countryName_val() {
	var value = $('#countryName').val();
	clearError('countryName_error');
	if (isEmpty(value)) {
		setError('countryName_error', HMS_MANDATORY);
		setFocusLast('countryName');
		return false;
	}

	if (!isValidDescription(value)) {
		setError('countryName_error', HMS_INVALID_NAME);
		setFocusLast('countryName');
		return false;
	}
	setFocusLast('shortName');
	return true;

}

function shortName_val() {

	var value = $('#shortName').val();
	clearError('shortName_error');
	if (isEmpty(value)) {
		setError('shortName_error', HMS_MANDATORY);
		setFocusLast('shortName');
		return false;
	}
	if (!isValidConciseDescription(value)) {
		setError('shortName_error', HMS_INVALID_SHORT_NAME);
		setFocusLast('shortName');
		return false;
	}
	setFocusLast('geoUnitStructureCode');
	return true;
}

function validate(id) {
	switch (id) {
	case 'countryCode':
		countryCode_val();

		break;
	case 'countryName':
		countryName_val();
		break;
	case 'shortName':
		shortName_val();
		break;
	case 'geoUnitStructureCode':
		geographicalUnitStructureCode_val();
		break;
	case 'enabled':
		enabled_val();
		break;
	case 'remarks':
		remarks_val();
		break;
	}
}

function backtrack(id) {
	switch (id) {
	case 'countryCode':
		setFocusLast('countryCode');
		break;
	case 'countryName':
		setFocusLast('countryCode');
		break;
	case 'shortName':
		setFocusLast('countryName');
		break;
	case 'geoUnitStructureCode':
		setFocusLast('shortName');
		break;
	case 'enabled':
		setFocusLast('geoUnitStructureCode');
		break;
	case 'remarks':
		if ($('#action').val() == MODIFY) {
			setFocusLast('enabled');
		} else {
			setFocusLast('geoUnitStructureCode');
		}
		break;
	}
}

function enabled_val() {
	if ($('#action').val() == MODIFY) {
		setFocusLast('remarks');
	}
	return true;
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', HMS_INVALID_REMARKS);
			setFocusLast('remarks');
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', HMS_INVALID_REMARKS);
			setFocusLast('remarks');
			return false;
		}
	}
	if ($('#action').val() == MODIFY) {
		if (isEmpty(value)) {
			setError('remarks_error', HMS_MANDATORY);
			setFocusLast('remarks');
			return false;
		}

	}
	setFocusOnSubmit();
	return true;
}
