var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MGEOUNITCON';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}

function clearFields() {
	$('#countryCode').val(EMPTY_STRING);
	$('#countryCode_desc').html(EMPTY_STRING);
	$('#geoUnitID').val(EMPTY_STRING);
	$('#geoUnitID_desc').html(EMPTY_STRING);
	clearNonPKFields();
}

function clearNonPKFields() {
	$('#name').val(EMPTY_STRING);
	$('#shortName').val(EMPTY_STRING);
	$('#parentgeoUnitID').val(EMPTY_STRING);
	$('#parentgeoUnitID_desc').html(EMPTY_STRING);
	$('#geoStructure').val(EMPTY_STRING);
	$('#geoStructure_desc').html(EMPTY_STRING);
	$('#parentGeoStructure').val(EMPTY_STRING);
	$('#parentGeoStructure_desc').html(EMPTY_STRING);
	$('#geoUnitType').val(EMPTY_STRING);
	$('#geoUnitType_desc').html(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
}

function loadData() {
	$('#countryCode').val(validator.getValue('COUNTRY_CODE'));
	$('#countryCode_desc').html(validator.getValue('F1_DESCRIPTION'));
	$('#geoUnitID').val(validator.getValue('GEO_UNIT_ID'));
	$('#name').val(validator.getValue('DESCRIPTION'));
	$('#shortName').val(validator.getValue('CONCISE_DESCRIPTION'));
	$('#parentgeoUnitID').val(validator.getValue('PARENT_GEO_UNIT_ID'));
	$('#parentgeoUnitID_desc').html(validator.getValue('F2_DESCRIPTION'));
	$('#geoStructure').val(validator.getValue('GEO_UNIT_STRUC_CODE'));
	$('#geoStructure_desc').html(validator.getValue('F3_DESCRIPTION'));
	$('#parentGeoStructure').val(validator.getValue('F3_PARENT_GUS_CODE'));	
	$('#parentGeoStructure_desc').html(validator.getValue('F5_DESCRIPTION'));
	$('#geoUnitType').val(validator.getValue('GEO_UNIT_TYPE'));
	$('#geoUnitType_desc').html(validator.getValue('F4_DESCRIPTION'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadAuditFields(validator);

}