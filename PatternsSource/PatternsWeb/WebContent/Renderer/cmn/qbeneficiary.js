var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MBENEFICIARY';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}
function clearFields() {
	$('#beneficiaryId').val(EMPTY_STRING);
	$('#name').val(EMPTY_STRING);
	$('#address').val(EMPTY_STRING);
	$('#accountNumber').val(EMPTY_STRING);
	$('#accountType').val(EMPTY_STRING);
	$('#ifscCode').val(EMPTY_STRING);
	setCheckbox('enabled', YES);
	$('#remarks').val(EMPTY_STRING);

}
function loadData() {
	$('#beneficiaryId').val(validator.getValue('BENEFICIARY_ID'));
	$('#name').val(validator.getValue('BENEFICIARY_NAME'));
	$('#address').val(validator.getValue('BENEFICIARY_ADDRESS'));
	$('#accountNumber').val(validator.getValue('BENEFICIARY_ACCOUNT_NO'));
	$('#accountType').val(validator.getValue('BENEFICIARY_ACCOUNT_TYPE'));
	$('#ifscCode').val(validator.getValue('BENEFICIARY_IFSC_CODE'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadAuditFields(validator);
}