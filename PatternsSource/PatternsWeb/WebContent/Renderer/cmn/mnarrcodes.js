var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MNARRCODES';
function init() {
	refreshTBAGrid();
	refreshQueryGrid();
	if (_redisplay) {
		if ($('#action').val() == ADD) {
			$('#enabled').prop('disabled', true);
			setCheckbox('enabled', YES);
		}
	}
}

function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'MNARRCODES_MAIN', queryGrid);
}

function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}

function doclearfields(id) {
	switch (id) {
	case 'narrationCode':
		if (isEmpty($('#narrationCode').val())) {
			$('#narrationCode_error').html(EMPTY_STRING);
			$('#narrationCode_desc').html(EMPTY_STRING);
			clearNonPKFields();
			break;
		}
	}
}

function clearFields() {
	$('#narrationCode').val(EMPTY_STRING);
	$('#narrationCode_error').html(EMPTY_STRING);
	$('#narrationCode_desc').html(EMPTY_STRING);
	clearNonPKFields();
}

function clearNonPKFields() {
	$('#description').val(EMPTY_STRING);
	$('#description_error').html(EMPTY_STRING);
	$('#conciseDescription').val(EMPTY_STRING);
	$('#conciseDescription_error').html(EMPTY_STRING);
	$("#useTranCode").prop('selectedIndex', 0);
	$('#useTranCode_error').html(EMPTY_STRING);
	setCheckbox('enabled', YES);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
}

function doHelp(id) {
	switch (id) {
	case 'narrationCode':
		if ($('#action').val() == MODIFY) {
		help('COMMON', 'HLP_NARR_CODE_M', $('#narrationCode').val(), EMPTY_STRING, $('#narrationCode'));
		break;
		}
	}
}

function add() {
	$('#narrationCode').focus();
	setCheckbox('enabled', YES);
	$('#enabled').prop('disabled', true);
}

function modify() {
	$('#narrationCode').focus();
	if ($('#action').val() == MODIFY) {
		$('#enabled').prop('disabled', false);
	} else {
		$('#enabled').prop('disabled', true);
	}
}

function loadData() {
	$('#narrationCode').val(validator.getValue('NARR_CODE'));
	$('#description').val(validator.getValue('DESCRIPTION'));
	$('#conciseDescription').val(validator.getValue('CONCISE_DESCRIPTION'));
	$('#useTranCode').val(validator.getValue('TRAN_MODE'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	resetLoading();
}

function view(source, primaryKey) {
	hideParent('cmn/qnarrcodes', source, primaryKey);
	resetLoading();
}

function validate(id) {
	switch (id) {
	case 'narrationCode':
		narrationCode_val();
		break;
	case 'description':
		description_val();
		break;
	case 'conciseDescription':
		conciseDescription_val();
		break;
	case 'useTranCode':
		useTranCode_val();
		break;
	case 'enabled':
		enabled_val();
		break;
	case 'remarks':
		remarks_val();
		break;
	}
}

function backtrack(id) {
	switch (id) {
	case 'narrationCode':
		setFocusLast('narrationCode');
		break;
	case 'description':
		setFocusLast('narrationCode');
		break;
	case 'conciseDescription':
		setFocusLast('description');
		break;
	case 'useTranCode':
		setFocusLast('conciseDescription');
		break;
	case 'enabled':
		setFocusLast('useTranCode');
		break;
	case 'remarks':
		if ($('#action').val() == MODIFY) {
			setFocusLast('enabled');
		} else {
			setFocusLast('useTranCode');
		}
		break;
	}
}



function narrationCode_val() {
	var value = $('#narrationCode').val();
	clearError('narrationCode_error');
	if (isEmpty(value)) {
		setError('narrationCode_error', MANDATORY);
		return false;
	} else if (!isValidCode(value)) {
		setError('narrationCode_error', INVALID_FORMAT);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('NARR_CODE', $('#narrationCode').val());
		validator.setValue(ACTION, $('#action').val());
		validator.setClass('patterns.config.web.forms.cmn.mnarrcodesbean');
		validator.setMethod('validateNarrationCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('narrationCode_error', validator.getValue(ERROR));
			$('#narrationCode_desc').html(EMPTY_STRING);
			return false;
		} else {
			if ($('#action').val() == MODIFY) {
				PK_VALUE = getEntityCode() + PK_SEPERATOR + $('#narrationCode').val();
				if (!loadPKValues(PK_VALUE, 'narrationCode_error')) {
					return false;
				}
			}
		}
	}
	setFocusLast('description');
	return true;
}

function description_val() {
	var value = $('#description').val();
	clearError('description_error');
	if (isEmpty(value)) {
		setError('description_error', MANDATORY);
		return false;
	}
	if (!isValidRemarks(value)) {
		setError('description_error', PBS_INVALID_NARRTION_DESCRIPTION);
		return false;
	}
	setFocusLast('conciseDescription');
	return true;
}

function conciseDescription_val() {
	var value = $('#conciseDescription').val();
	clearError('conciseDescription_error');
	if (isEmpty(value)) {
		setError('conciseDescription_error', MANDATORY);
		return false;
	}
	if (!isValidConciseDescription(value)) {
		setError('conciseDescription_error', INVALID_CONCISE_DESCRIPTION);
		return false;
	}
		setFocusLast('useTranCode');
	return true;
}

function useTranCode_val() {
	var value = $('#useTranCode').val();
	clearError('useTranCode_error');
	if (isEmpty(value)) {
		setError('useTranCode_error', MANDATORY);
		return false;
	}
	if ($('#action').val() == ADD) {
		setFocusLast('remarks');
	} else {
		setFocusLast('enabled');
	}
	return true;
}

function enabled_val() {
	if ($('#action').val() == MODIFY) {
		setFocusLast('remarks');
	}
	return true;
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', INVALID_REMARKS);
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', INVALID_REMARKS);
			return false;
		}
	}
	if ($('#action').val() == MODIFY) {
		if (isEmpty(value)) {
			setError('remarks_error', MANDATORY);
			return false;
		}

	}
	setFocusOnSubmit();
	return true;

}
