var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'ISCHPAYACPARAM';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}
function clearFields() {
	$('#schemeCode').val(EMPTY_STRING);
	$('#effectiveDate').val(EMPTY_STRING);
	setCheckbox('bulkPayFund', NO);
	setCheckbox('capFile', NO);
	$('#debitAmuSour').val(EMPTY_STRING);
	$('#debitGl').val(EMPTY_STRING);
	$('#debitAccount').val(EMPTY_STRING);
	$('#creditGl').val(EMPTY_STRING);
	$('#glHead').val(EMPTY_STRING);
	setCheckbox('enabled', YES);
	$('#remarks').val(EMPTY_STRING);
}

function enable_val() {
	if ($('#bulkPayFund').is(':checked') && !$('#capFile').is(':checked')) {
		$('#capFile').prop('disabled', false);
		$('#debitGl').prop('readOnly', false);
		$('#debitGl_pic').prop('disabled', false);
		$('#debitAccount').prop('readOnly', false);
		$('#debitAccount_pic').prop('disabled', false);
		$('#creditGl').prop('readOnly', false);
		$('#creditGl_pic').prop('disabled', false);
		$('#glHead').prop('readOnly', false);
		$('#glHead_pic').prop('disabled', false);
	} else {
		$('#debitGl').val(EMPTY_STRING);
		$('#debitGl_error').html(EMPTY_STRING);
		$('#debitGl_desc').html(EMPTY_STRING);
		$('#debitGl').prop('readOnly', true);
		$('#debitGl_pic').prop('disabled', true);
		$('#debitAccount').val(EMPTY_STRING);
		$('#debitAccount_error').html(EMPTY_STRING);
		$('#debitAccount_desc').html(EMPTY_STRING);
		$('#debitAccount').prop('readOnly', true);
		$('#debitAccount_pic').prop('disabled', true);
		$('#creditGl').val(EMPTY_STRING);
		$('#creditGl_error').html(EMPTY_STRING);
		$('#creditGl_desc').html(EMPTY_STRING);
		$('#creditGl').prop('readOnly', true);
		$('#creditGl_pic').prop('disabled', true);
		$('#glHead').val(EMPTY_STRING);
		$('#glHead_error').html(EMPTY_STRING);
		$('#glHead_desc').html(EMPTY_STRING);
		$('#glHead').prop('readOnly', true);
		$('#glHead_pic').prop('disabled', true);
	}
}
function loadData() {
	$('#schemeCode').val(validator.getValue('SCHEME_CODE'));
	$('#schemeCode_desc').html(validator.getValue('F1_DESCRIPTION'));
	$('#effectiveDate').val(validator.getValue('EFFT_DATE'));
	setCheckbox('bulkPayFund', validator.getValue('BULK_PAY_PREFUNDED'));
	checkclick('bulkPayFund');
	setCheckbox('capFile', validator.getValue('DB_FROM_FILE'));
	checkclick('capFile');
	enable_val();
	$('#debitAmuSour').val(validator.getValue('DB_AMOUNT_SRC'));
	$('#debitGl').val(validator.getValue('DB_GL_HEAD'));
	$('#debitGl_desc').html(validator.getValue('F3_DESCRIPTION'));
	$('#debitAccount').val(validator.getValue('DB_AC_NON_PREFUND'));
	$('#debitAccount_desc').html(validator.getValue('F2_NAME'));
	$('#creditGl').val(validator.getValue('CR_GL_HEAD'));
	$('#creditGl_desc').html(validator.getValue('F4_DESCRIPTION'));
	$('#glHead').val(validator.getValue('FAILED_CR_GL_HEAD'));
	$('#glHead_desc').html(validator.getValue('F4_DESCRIPTION'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadAuditFields(validator);
}
