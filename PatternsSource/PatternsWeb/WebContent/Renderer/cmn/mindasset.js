var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MINDASSET';
var fieldType = EMPTY_STRING;
var fieldDecimals = EMPTY_STRING;
var field = EMPTY_STRING;
var fieldSize = EMPTY_STRING;
$('#assetSerialAssetType').val(EMPTY_STRING);
$('#custIDHidden').val(EMPTY_STRING);
function init() {
	refreshTBAGrid();
	refreshQueryGrid();
	if (_redisplay) {
		if ($('#action').val() == ADD) {
			$('#enabled').prop('disabled', true);
			$('#enabled').prop('checked', true);
		}
		if (!isEmpty($('#xmlGrid').val())) {
			pgmGrid.loadXMLString($('#xmlGrid').val());
		}
		compSl_val(false);
		$('#fieldvalue').removeClass('hidden');
		$('#fieldremarks').addClass('hidden');
	}
	$('#fieldvalue').removeClass('hidden');
	$('#fieldremarks').addClass('hidden');
	setFocusLast('leaseCode');
}
function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'MINDASSET_MAIN', queryGrid);
}
function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}
function loadGrid() {
	loadGridQuery(CURRENT_PROGRAM_ID, 'MINDASSET_GRID', pgmGrid);
}
function view(source, primaryKey) {
	hideParent('cmn/qindasset', source, primaryKey);
	resetLoading();
}
function doHelp(id) {
	switch (id) {
	case 'leaseCode':
		help('COMMON', 'HLP_DEBTOR_CODE', $('#leaseCode').val(), EMPTY_STRING, $('#leaseCode'));
		break;
	case 'agreeNo':
		if (!isEmpty($('#custIDHidden').val())) {
			help('COMMON', 'HLP_CUST_AGGREEMENT', $('#agreeNo').val(), $('#custIDHidden').val(), $('#agreeNo'), null, function(gridObj, rowID) {
				$('#agreeNo').val(gridObj.cells(rowID, 1).getValue());
			});
		} else {
			setError('leaseCode_error', MANDATORY);
		}
		break;
	case 'scheduleId':
		if (!isEmpty($('#agreeNo').val())) {
			help('COMMON', 'HLP_LEASE_SCH_ID', $('#scheduleId').val(), $('#leaseCode').val() + PK_SEPERATOR + $('#agreeNo').val(), $('#scheduleId'), function() {
			});
		} else {
			setError('agreeNo_error', MANDATORY);
		}
		break;
	case 'assetSl':
		if (!isEmpty($('#scheduleId').val())) {
			help('COMMON', 'HLP_LEASE_ASSETSL', $('#assetSl').val(), $('#leaseCode').val() + PK_SEPERATOR + $('#agreeNo').val() + PK_SEPERATOR + $('#scheduleId').val(), $('#assetSl'));
		} else {
			setError('scheduleId_error', MANDATORY);
		}
		break;
	case 'compSl':
		if (!isEmpty($('#assetSl').val())) {
			help('COMMON', 'HLP_LEASE_ASSET_COMPSL', $('#compSl').val(), $('#leaseCode').val() + PK_SEPERATOR + $('#agreeNo').val() + PK_SEPERATOR + $('#scheduleId').val() + PK_SEPERATOR + $('#assetSl').val(), $('#compSl'));
		} else {
			setError('assetSl_error', MANDATORY);
		}
		break;
	case 'fieldId':
		help('MINDASSET', 'HLP_ASSET_TYPE_SPEC', $('#fieldId').val(), EMPTY_STRING, $('#fieldId'));
		break;
	}
}

function clearFields() {
	$('#leaseCode').val(EMPTY_STRING);
	$('#leaseCode_error').html(EMPTY_STRING);
	$('#leaseCode_desc').html(EMPTY_STRING);
	$('#agreeNo').val(EMPTY_STRING);
	$('#agreeNo_error').html(EMPTY_STRING);
	$('#agreeNo_desc').html(EMPTY_STRING);
	$('#scheduleId').val(EMPTY_STRING);
	$('#scheduleId_error').html(EMPTY_STRING);
	$('#scheduleId_desc').html(EMPTY_STRING);
	$('#assetSl').val(EMPTY_STRING);
	$('#assetSl_error').html(EMPTY_STRING);
	$('#assetSl_desc').html(EMPTY_STRING);
	$('#compSl').val(EMPTY_STRING);
	$('#compSl_error').html(EMPTY_STRING);
	$('#compSl_desc').html(EMPTY_STRING);
	$('#qtySl').val(EMPTY_STRING);
	$('#qtySl_error').html(EMPTY_STRING);
	$('#qtySl_desc').html(EMPTY_STRING);
	$('#leaseCode').focus();
	clearNonPKFields(true);
}

function clearNonPKFields(valmode) {
	$('#indAssetId').val(EMPTY_STRING);
	$('#indAssetId_error').html(EMPTY_STRING);
	pgmGrid.clearAll();
	mindasset_clearGridFields();
	setCheckbox('enabled', YES);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
	if (valmode) {
		$('#custIDHidden').val(EMPTY_STRING);
		$('#qtyOfAsset').val(EMPTY_STRING);
		$('#qtyOfAsset_error').html(EMPTY_STRING);
	}
	$('#assetSerialAssetType').val(EMPTY_STRING);
}

function add() {
	setCheckbox('enabled', YES);
	$('#enabled').prop('disabled', true);
	$('#leaseCode').focus();
}
function modify() {
	$('leaseCode').focus();
	$('#enabled').prop('disabled', false);
}
function loadData() {
	$('#leaseCode').val(validator.getValue('LESSEE_CODE'));
	$('#leaseCode_desc').html(validator.getValue('F2_CUSTOMER_NAME'));
	$('#custIDHidden').val(validator.getValue('F1_CUSTOMER_ID'));
	$('#agreeNo').val(validator.getValue('AGREEMENT_NO'));
	$('#scheduleId').val(validator.getValue('SCHEDULE_ID'));
	$('#assetSl').val(validator.getValue('ASSET_SL'));
	$('#assetSerialAssetType').val(validator.getValue('F4_ASSET_TYPE_CODE'));
	$('#compSl').val(validator.getValue('ASSET_COMP_SL'));
	$('#qtySl').val(validator.getValue('ASSET_QTY_SL'));
	$('#qtyOfAsset').val(validator.getValue('F3_ASSET_QTY'));
	$('#indAssetId').val(validator.getValue('INDIVIDUAL_ASSET_ID'));
	loadGrid();
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	// $('#prevIndAssetId').val($('#indAssetId').val());
}
function mindasset_clearGridFields() {
	$('#fieldId').val(EMPTY_STRING);
	$('#fieldId_error').html(EMPTY_STRING);
	$('#fieldId_desc').html(EMPTY_STRING);
	$('#fieldvalue').removeClass('hidden');
	$('#fieldremarks').addClass('hidden');
	$('#fieldValue').val(EMPTY_STRING);
	$('#fieldValue_error').html(EMPTY_STRING);
	$('#fieldRemarks').val(EMPTY_STRING);
	$('#fieldRemarks_error').html(EMPTY_STRING);
	$('#fieldValue').prop('readOnly', false);
	$('#fieldRemarks').prop('readOnly', false);
	field = EMPTY_STRING;
	fieldType = EMPTY_STRING;
	fieldDecimals = EMPTY_STRING;
	fieldSize = EMPTY_STRING;
	// $('#fieldValue').addClass('blur-input');
	// $('#fieldRemarks').addClass('blur-input');
}

function validate(id) {
	switch (id) {
	case 'leaseCode':
		leaseCode_val();
		break;
	case 'agreeNo':
		agreeNo_val();
		break;
	case 'scheduleId':
		scheduleId_val();
		break;
	case 'assetSl':
		assetSl_val();
		break;
	case 'compSl':
		compSl_val(true);
		break;
	case 'qtySl':
		qtySl_val();
		break;
	case 'indAssetId':
		indAssetId_val();
		break;
	case 'fieldId':
		fieldId_val(true);
		break;
	case 'fieldValue':
		fieldValue_val(true);
		break;
	case 'fieldRemarks':
		fieldRemarks_val(true);
		break;
	case 'enabled':
		enabled_val();
		break;
	case 'remarks':
		remarks_val();
		break;
	}
}

function doclearfields(id) {
	switch (id) {
	case 'leaseCode':
		if (isEmpty($('#leaseCode').val())) {
			$('#agreeNo').val(EMPTY_STRING);
			$('#agreeNo_error').html(EMPTY_STRING);
			$('#agreeNo_desc').html(EMPTY_STRING);
			$('#scheduleId').val(EMPTY_STRING);
			$('#scheduleId_error').html(EMPTY_STRING);
			$('#scheduleId_desc').html(EMPTY_STRING);
			$('#assetSl').val(EMPTY_STRING);
			$('#assetSl_error').html(EMPTY_STRING);
			$('#assetSl_desc').html(EMPTY_STRING);
			$('#compSl').val(EMPTY_STRING);
			$('#compSl_error').html(EMPTY_STRING);
			$('#compSl_desc').html(EMPTY_STRING);
			$('#qtySl').val(EMPTY_STRING);
			$('#qtySl_error').html(EMPTY_STRING);
			$('#qtySl_desc').html(EMPTY_STRING);
			$('#qtyOfAsset').val(EMPTY_STRING);
			$('#qtyOfAsset_error').html(EMPTY_STRING);
			$('#qtyOfAsset').val(EMPTY_STRING);
			$('#qtyOfAsset_error').html(EMPTY_STRING);
			clearNonPKFields(false);
			break;
		}
	case 'agreeNo':
		if (isEmpty($('#agreeNo').val())) {
			$('#scheduleId').val(EMPTY_STRING);
			$('#scheduleId_error').html(EMPTY_STRING);
			$('#scheduleId_desc').html(EMPTY_STRING);
			$('#assetSl').val(EMPTY_STRING);
			$('#assetSl_error').html(EMPTY_STRING);
			$('#assetSl_desc').html(EMPTY_STRING);
			$('#compSl').val(EMPTY_STRING);
			$('#compSl_error').html(EMPTY_STRING);
			$('#compSl_desc').html(EMPTY_STRING);
			$('#qtySl').val(EMPTY_STRING);
			$('#qtySl_error').html(EMPTY_STRING);
			$('#qtySl_desc').html(EMPTY_STRING);
			$('#qtyOfAsset').val(EMPTY_STRING);
			$('#qtyOfAsset_error').html(EMPTY_STRING);
			clearNonPKFields(false);
			break;
		}
	case 'scheduleId':
		if (isEmpty($('#scheduleId').val())) {
			$('#assetSl').val(EMPTY_STRING);
			$('#assetSl_error').html(EMPTY_STRING);
			$('#assetSl_desc').html(EMPTY_STRING);
			$('#compSl').val(EMPTY_STRING);
			$('#compSl_error').html(EMPTY_STRING);
			$('#compSl_desc').html(EMPTY_STRING);
			$('#qtySl').val(EMPTY_STRING);
			$('#qtySl_error').html(EMPTY_STRING);
			$('#qtySl_desc').html(EMPTY_STRING);
			$('#qtyOfAsset').val(EMPTY_STRING);
			$('#qtyOfAsset_error').html(EMPTY_STRING);
			clearNonPKFields(false);
			break;
		}
	case 'assetSl':
		if (isEmpty($('#assetSl').val())) {
			$('#compSl').val(EMPTY_STRING);
			$('#compSl_error').html(EMPTY_STRING);
			$('#compSl_desc').html(EMPTY_STRING);
			$('#qtySl').val(EMPTY_STRING);
			$('#qtySl_error').html(EMPTY_STRING);
			$('#qtySl_desc').html(EMPTY_STRING);
			$('#qtyOfAsset').val(EMPTY_STRING);
			$('#qtyOfAsset_error').html(EMPTY_STRING);
			clearNonPKFields(false);
			break;
		}
	case 'compSl':
		if (isEmpty($('#compSl').val())) {
			$('#qtySl').val(EMPTY_STRING);
			$('#qtySl_error').html(EMPTY_STRING);
			$('#qtySl_desc').html(EMPTY_STRING);
			$('#qtyOfAsset').val(EMPTY_STRING);
			$('#qtyOfAsset_error').html(EMPTY_STRING);
			clearNonPKFields(false);
			break;
		}
	case 'qtySl':
		if (isEmpty($('#qtySl').val())) {
			$('#indAssetId').val(EMPTY_STRING);
			$('#indAssetId_error').html(EMPTY_STRING);
			pgmGrid.clearAll();
			mindasset_clearGridFields();
			setCheckbox('enabled', YES);
			$('#remarks').val(EMPTY_STRING);
			$('#remarks_error').html(EMPTY_STRING);
			break;
		}
	case 'fieldId':
		if (isEmpty($('#fieldId').val())) {
			mindasset_clearGridFields();
			break;
		}
	}
}

function backtrack(id) {
	switch (id) {
	case 'leaseCode':
		setFocusLast('leaseCode');
		break;
	case 'agreeNo':
		setFocusLast('leaseCode');
		break;
	case 'scheduleId':
		setFocusLast('agreeNo');
		break;
	case 'assetSl':
		setFocusLast('scheduleId');
		break;
	case 'compSl':
		setFocusLast('assetSl');
		break;
	case 'qtySl':
		setFocusLast('compSl');
		break;
	case 'qtyOfAsset':
		setFocusLast('qtySl');
		break;
	case 'indAssetId':
		setFocusLast('qtySl');
		break;
	case 'fieldId':
		setFocusLast('indAssetId');
		break;
	case 'pgmGrid_add':
		setFocusLast('fieldId');
		break;
	case 'fieldValue':
		setFocusLast('fieldId');
		break;
	case 'fieldRemarks':
		setFocusLast('fieldId');
		break;
	case 'remarks':
		setFocusLast('fieldId');
		break;
	}
}

function leaseCode_val() {
	var value = $('#leaseCode').val();
	clearError('leaseCode_error');
	if (isEmpty(value)) {
		setError('leaseCode_error', MANDATORY);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('SUNDRY_DB_AC', $('#leaseCode').val());
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.cmn.mindassetbean');
		validator.setMethod('validateLeaseCode');
		validator.sendAndReceive();
		if (validator.getValue(RESULT) == DATA_AVAILABLE) {
			$('#leaseCode_desc').html(validator.getValue("CUSTOMER_NAME"));
			$('#custIDHidden').val(validator.getValue("CUSTOMER_ID"));
		} else if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('leaseCode_error', validator.getValue(ERROR));
			$('#leaseCode_desc').html(EMPTY_STRING);
			$('#custIDHidden').val(EMPTY_STRING);
			return false;
		}
	}
	setFocusLast('agreeNo');
	return true;
}

function agreeNo_val() {
	var value = $('#agreeNo').val();
	clearError('agreeNo_error');
	if (isEmpty(value)) {
		setError('agreeNo_error', MANDATORY);
		return false;
	}
	if (isEmpty($('#custIDHidden').val())) {
		setError('agreeNo_error', MANDATORY);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('CUSTOMER_ID', $('#custIDHidden').val());
		validator.setValue('AGREEMENT_NO', $('#agreeNo').val());
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.cmn.mindassetbean');
		validator.setMethod('validateAgreementNo');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('agreeNo_error', validator.getValue(ERROR));
			return false;
		}
	}
	setFocusLast('scheduleId');
	return true;
}

function scheduleId_val() {
	var value = $('#scheduleId').val();
	clearError('scheduleId_error');
	if (isEmpty(value)) {
		setError('scheduleId_error', MANDATORY);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('LESSEE_CODE', $('#leaseCode').val());
		validator.setValue('AGREEMENT_NO', $('#agreeNo').val());
		validator.setValue('SCHEDULE_ID', $('#scheduleId').val());
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.cmn.mindassetbean');
		validator.setMethod('validateScheduleID');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('scheduleId_error', validator.getValue(ERROR));
			return false;
		}
	}
	setFocusLast('assetSl');
	return true;
}

function assetSl_val() {
	var value = $('#assetSl').val();
	clearError('assetSl_error');
	if (isEmpty(value)) {
		setError('assetSl_error', MANDATORY);
		return false;
	}
	if (!isNumeric(value)) {
		setError('assetSl_error', INVALID_NUMBER);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('LESSEE_CODE', $('#leaseCode').val());
		validator.setValue('AGREEMENT_NO', $('#agreeNo').val());
		validator.setValue('SCHEDULE_ID', $('#scheduleId').val());
		validator.setValue('ASSET_SL', $('#assetSl').val());
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.cmn.mindassetbean');
		validator.setMethod('validateAssetSerial');
		validator.sendAndReceive();
		if (validator.getValue(RESULT) == DATA_AVAILABLE) {
			$('#assetSl_desc').html(validator.getValue("DESCRIPTION"));
			$('#assetSerialAssetType').val(validator.getValue("ASSET_TYPE_CODE"));
		} else if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('assetSl_error', validator.getValue(ERROR));
			$('#assetSl_desc').html(EMPTY_STRING);
			w_asset_type = EMPTY_STRING;
			return false;
		}
	}
	setFocusLast('compSl');
	return true;
}

function compSl_val(valmode) {
	var value = $('#compSl').val();
	clearError('compSl_error');
	if (isEmpty(value)) {
		setError('compSl_error', MANDATORY);
		return false;
	}
	if (!isNumeric(value)) {
		setError('compSl_error', INVALID_NUMBER);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('LESSEE_CODE', $('#leaseCode').val());
		validator.setValue('AGREEMENT_NO', $('#agreeNo').val());
		validator.setValue('SCHEDULE_ID', $('#scheduleId').val());
		validator.setValue('ASSET_SL', $('#assetSl').val());
		validator.setValue('ASSET_COMP_SL', $('#compSl').val());
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.cmn.mindassetbean');
		validator.setMethod('validateCompSerial');
		validator.sendAndReceive();
		if (validator.getValue(RESULT) == DATA_AVAILABLE) {
			$('#compSl_desc').html(validator.getValue("ASSET_DESCRIPTION"));
			$('#qtyOfAsset').val(validator.getValue("ASSET_QTY"));
		} else if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('compSl_error', validator.getValue(ERROR));
			$('#compSl_desc').html(EMPTY_STRING);
			$('#qtyOfAsset').val(EMPTY_STRING);
			return false;
		}
	}
	if (valmode)
		setFocusLast('qtySl');
	return true;
}

function qtySl_val() {
	var value = $('#qtySl').val();
	clearError('qtySl_error');
	if (isEmpty(value)) {
		setError('qtySl_error', MANDATORY);
		return false;
	}
	if (!isNumeric(value)) {
		setError('qtySl_error', INVALID_NUMBER);
		return false;
	}
	if (value > $('#qtyOfAsset').val()) {
		setError('qtySl_error', ASSETID_MAPPED); //
		return false;
	}
	if ($('#action').val() == ADD) {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('LESSEE_CODE', $('#leaseCode').val());
		validator.setValue('AGREEMENT_NO', $('#agreeNo').val());
		validator.setValue('SCHEDULE_ID', $('#scheduleId').val());
		validator.setValue('ASSET_SL', $('#assetSl').val());
		validator.setValue('ASSET_COMP_SL', $('#compSl').val());
		validator.setValue('ASSET_QTY_SL', $('#qtySl').val());
		validator.setValue('ACTION', $('#action').val());
		validator.setClass('patterns.config.web.forms.cmn.mindassetbean');
		validator.setMethod('validateQtySerial');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('qtySl_error', validator.getValue(ERROR));
			return false;
		}
	}
	if ($('#action').val() == MODIFY) {
		clearNonPKFields(false);
		PK_VALUE = getEntityCode() + PK_SEPERATOR + $('#leaseCode').val() + PK_SEPERATOR + $('#agreeNo').val() + PK_SEPERATOR + $('#scheduleId').val() + PK_SEPERATOR + $('#assetSl').val() + PK_SEPERATOR + $('#compSl').val() + PK_SEPERATOR + $('#qtySl').val(); // checkin
		loadPKValues(PK_VALUE, 'qtySl_error');
		loadGrid();
	}
	setFocusLast('indAssetId');
	return true;
}
function indAssetId_val() {
	var value = $('#indAssetId').val();
	clearError('indAssetId_error');
	if (isEmpty(value)) {
		setError('indAssetId_error', MANDATORY);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('ENTITY_CODE', getEntityCode());
		validator.setValue('LESSEE_CODE', $('#leaseCode').val());
		validator.setValue('AGREEMENT_NO', $('#agreeNo').val());
		validator.setValue('SCHEDULE_ID', $('#scheduleId').val());
		validator.setValue('ASSET_SL', $('#assetSl').val());
		validator.setValue('ASSET_COMP_SL', $('#compSl').val());
		validator.setValue('ASSET_QTY_SL', $('#qtySl').val());
		validator.setValue('INDIVIDUAL_ASSET_ID', $('#indAssetId').val());
		validator.setClass('patterns.config.web.forms.cmn.mindassetbean');
		validator.setMethod('validateIndAssetID');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('indAssetId_error', validator.getValue(ERROR));
			return false;
		}
	}
	setFocusLast('fieldId');
	return true;
}

function fieldId_val(valmode) {
	var value = $('#fieldId').val();
	clearError('fieldId_error');
	if (valmode) {
		$('#fieldValue_error').html(EMPTY_STRING);
		$('#fieldRemarks_error').html(EMPTY_STRING);
	}
	if (isEmpty(value)) {
		setError('fieldId_error', MANDATORY);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('ASSET_SERIAL', $('#assetSerialAssetType').val());
		validator.setValue('CUSTOMER_ID_HIDDEN', $('#custIDHidden').val());
		validator.setValue('FIELD_ID', $('#fieldId').val());
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.cmn.mindassetbean');
		validator.setMethod('validateFieldID');
		validator.sendAndReceive();
		if (validator.getValue(RESULT) == DATA_AVAILABLE) {
			$('#fieldId_desc').html(validator.getValue("DESCRIPTION"));
			fieldType = validator.getValue("FIELD_VALUE_TYPE");
			fieldDecimals = validator.getValue("FIELD_DECIMALS");
			fieldSize = validator.getValue("FIELD_SIZE");
			if (isEmpty(fieldDecimals)) {
				fieldDecimals = ZERO;
			}
			if (fieldType == 'T') {
				$('#fieldValue').attr('intDigits', fieldSize);
				$('#fieldValue').attr('fracDigits', fieldDecimals);
				$('#fieldValue').prop('readOnly', true);
				$('#fieldRemarks').prop('readOnly', false);
				$('#fieldvalue').addClass('hidden');
				$('#fieldremarks').removeClass('hidden');
				if (valmode)
					setFocusLast('fieldRemarks');
			} else if (fieldType != 'T') {
				$('#fieldValue').attr('intDigits', parseInt(fieldSize - fieldDecimals));
				$('#fieldValue').prop('readOnly', false);
				$('#fieldRemarks').prop('readOnly', true);
				$('#fieldvalue').removeClass('hidden');
				$('#fieldremarks').addClass('hidden');
				if ((fieldType == 'A') || (fieldType == 'F')) {
					$('#fieldValue').attr('fracDigits', fieldDecimals);
				} else if (fieldType == 'N') {
					$('#fieldValue').attr('fracDigits', fieldDecimals);
				}
				if (valmode)
					setFocusLast('fieldValue');
			}
		} else if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('fieldId_error', validator.getValue(ERROR));
			$('#fieldId_desc').html(EMPTY_STRING);
			return false;
		}
	}
	return true;
}

function fieldValue_val(valmode) {
	var value = $('#fieldValue').val();
	clearError('fieldValue_error');
	if (isEmpty(value)) {
		setError('fieldValue_error', MANDATORY);
		return false;
	}
	if (valmode)
		setFocusLastOnGridSubmit('pgmGrid');
	return true;
}

function fieldRemarks_val(valmode) {
	var value = $('#fieldRemarks').val();
	clearError('fieldRemarks_error');
	if (isEmpty(value)) {
		setError('fieldRemarks_error', MANDATORY);
		return false;
	}
	var lastChar = value.substring(value.length, value.length - 1);
	if (lastChar == "\n")
		value = value.substring(0, value.length - 1);
	var intLength = parseInt(value.length);
	if (intLength > 0) {
		if (intLength > fieldSize) {
			setError('fieldRemarks_error', HMS_INVALID_MAXINT_RANGE + fieldSize);
			return false;
		}
	}
	if (valmode) {
		setFocusLastOnGridSubmit('pgmGrid');
	}
	return true;
}

function mindasset_addGrid(editMode, editRowId) {
	if (fieldId_val(false)) {
		if (fieldType != 'T') {
			if (!fieldValue_val(false)) {
				return false;
			}
		} else {
			if (!fieldRemarks_val(false)) {
				return false;
			}
		}
		if (mindasset_gridDuplicateCheck(editMode, editRowId)) {
			if (fieldType == 'T') {
				field = $('#fieldRemarks').val();
			} else if (fieldType != 'T') {
				field = $('#fieldValue').val();
			}
			var field_values = [ 'false', $('#fieldId').val(), $('#fieldId_desc').html(), field, fieldType, fieldDecimals, fieldSize ];
			_grid_updateRow(pgmGrid, field_values);
			mindasset_clearGridFields();
			$('#fieldId').focus();
			return true;
		} else {
			setError('fieldId_error', DUPLICATE_DATA);
			$('#fieldId_desc').html(EMPTY_STRING);
			return false;
		}
	} else {
		return false;
	}
}

function mindasset_gridDuplicateCheck(editMode, editRowId) {
	var currentValue = [ $('#fieldId').val() ];
	return _grid_duplicate_check(pgmGrid, currentValue, [ 1 ]);
}

function mindasset_editGrid(rowId) {
	$('#fieldId').val(pgmGrid.cells(rowId, 1).getValue());
	$('#fieldId_desc').html(pgmGrid.cells(rowId, 2).getValue());
	fieldType = pgmGrid.cells(rowId, 4).getValue();
	fieldSize = pgmGrid.cells(rowId, 6).getValue();
	fieldDecimals = pgmGrid.cells(rowId, 5).getValue();
	if (isEmpty(fieldDecimals)) {
		fieldDecimals = ZERO;
	}
	if (fieldType == 'T') {
		$('#fieldValue').prop('readOnly', true);
		$('#fieldRemarks').prop('readOnly', false);
		$('#fieldvalue').addClass('hidden');
		$('#fieldremarks').removeClass('hidden');
		$('#fieldRemarks').val(pgmGrid.cells(rowId, 3).getValue());
		$('#fieldValue').attr('intDigits', parseInt(pgmGrid.cells(rowId, 6).getValue()));
		$('#fieldValue').attr('fracDigits', fieldDecimals);
	} else if (fieldType != 'T') {
		$('#fieldValue').val(pgmGrid.cells(rowId, 3).getValue());
		$('#fieldValue').attr('intDigits', parseInt(fieldSize) - parseInt(fieldDecimals));
		$('#fieldValue').prop('readOnly', false);
		$('#fieldRemarks').prop('readOnly', true);
		$('#fieldvalue').removeClass('hidden');
		$('#fieldremarks').addClass('hidden');
		if ((fieldType == 'A') || (fieldType == 'F')) {
			$('#fieldValue').attr('fracDigits', fieldDecimals);
		} else if (fieldType == 'N') {
			$('#fieldValue').attr('fracDigits', fieldDecimals);
		}
	}
	$('#fieldId').focus();
}

function mindasset_cancelGrid(rowId) {
	mindasset_clearGridFields();
}

function gridExit(id) {
	switch (id) {
	case 'fieldId':
		$('#xmlGrid').val(pgmGrid.serialize());
		if ($('#action').val() == MODIFY) {
			setFocusLast('enabled');
		} else {
			setFocusLast('remarks');
		}
		break;
	case 'fieldValue':
		$('#xmlGrid').val(pgmGrid.serialize());
		if ($('#action').val() == MODIFY) {
			setFocusLast('enabled');
		} else {
			setFocusLast('remarks');
		}
		break;
	case 'fieldRemarks':
		$('#xmlGrid').val(pgmGrid.serialize());
		if ($('#action').val() == MODIFY) {
			setFocusLast('enabled');
		} else {
			setFocusLast('remarks');
		}
		break;
	}
}
function gridDeleteRow(id) {
	switch (id) {
	case 'fieldId':
		deleteGridRecord(pgmGrid);
		break;
	}
}
function revalidate() {
	mindasset_revaildateGrid();
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', HMS_INVALID_REMARKS);
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', HMS_INVALID_REMARKS);
			return false;
		}
	}
	if ($('#action').val() == MODIFY) {
		if (isEmpty(value)) {
			setError('remarks_error', HMS_MANDATORY);
			return false;
		}

	}
	setFocusOnSubmit();
	return true;

}

function mindasset_revaildateGrid() {
	if (pgmGrid.getRowsNum() > 0) {
		$('#xmlGrid').val(pgmGrid.serialize());
		$('#fieldId_error').html(EMPTY_STRING);
	} else {
		pgmGrid.clearAll();
		$('#xmlGrid').val(pgmGrid.serialize());
		setError('fieldId_error', ADD_ATLEAST_ONE_ROW);
		errors++;
	}
}
