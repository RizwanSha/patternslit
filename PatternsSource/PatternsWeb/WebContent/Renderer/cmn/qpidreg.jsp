<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.cmn.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/cmn/qpidreg.js" />
	</web:dependencies>
	<web:loadPK />
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="qpidreg.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:viewContent id="po_view1">
				<web:dividerBlock>
					<web:sectionBlock width="width-100p" align="left">
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="epidreg.personid" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:cifNumberDisplay property="personId" id="personId" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:section>
							<web:sectionTitle var="program" key="epidreg.section" />
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowOdd>
									<web:column>
										<web:legend key="epidreg.pidregsl" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:PIDRegSlDisplay property="PIDRegSl" id="PIDRegSl" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="epidreg.deregister" var="program" />
									</web:column>
									<web:column>
										<type:checkboxDisplay property="deRegister" id="deRegister" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="epidreg.dateofregistration" var="program" />
									</web:column>
									<web:column>
										<type:dateDisplay property="dateOfRegistration" id="dateOfRegistration" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="epidreg.pidcode" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:pidCodeDisplay property="PIDCode" id="PIDCode" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="epidreg.pidnumber" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:otherInformation25Display property="PIDNumber" id="PIDNumber" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="epidreg.pidissuedate" var="program" />
									</web:column>
									<web:column>
										<type:dateDisplay property="PIDIssueDate" id="PIDIssueDate" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="epidreg.pidexpirydate" var="program" />
									</web:column>
									<web:column>
										<type:dateDisplay property="PIDExpiryDate" id="PIDExpiryDate" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="form.remarks" var="common" mandatory="true" />
									</web:column>
									<web:column>
										<type:remarksDisplay property="PIDRemarks" id="PIDRemarks" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:viewContent id="po_view5">
							<web:section>
								<web:sectionTitle var="program" key="epidreg.section1" />
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="epidreg.dateofdereg" var="program" />
										</web:column>
										<web:column>
											<type:dateDisplay property="dateOfDereg" id="dateOfDereg" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="epidreg.reasonofdereg" var="program" />
										</web:column>
										<web:column>
											<type:remarksDisplay property="reasonOfDereg" id="reasonOfDereg" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
						</web:viewContent>
						<web:auditDisplay />
					</web:sectionBlock>
				</web:dividerBlock>
			</web:viewContent>
		</web:viewSubPart>
	</web:viewPart>
</web:fragment>