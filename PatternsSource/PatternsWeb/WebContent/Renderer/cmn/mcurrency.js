
var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MCURRENCY';
function init() {
	refreshTBAGrid();
	refreshQueryGrid();
	if (_redisplay) {
		if ($('#action').val() == ADD) {
			$('#enabled').prop('disabled', true);
			setCheckbox('enabled', '1');
		}
		hasSubCcyOnChange();
	}
}
function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'MCURRENCY_MAIN', queryGrid);
}
function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}

function doHelp(id) {
	switch (id) {
	case 'currencyCode':
		help('COMMON', 'HLP_CURRENCY_M', $('#currencyCode').val(), EMPTY_STRING, $('#currencyCode'));
		break;

	}
}

function doclearfields(id) {
	switch (id) {
	case 'currencyCode':
		if (isEmpty($('#currencyCode').val())) {
			$('#currencyCode_error').html(EMPTY_STRING);
			clearNonPKFields();
			break;
		}
	}
}

function clearFields() {

	$('#currencyCode').val(EMPTY_STRING);
	$('#currencyCode_error').html(EMPTY_STRING);
	clearNonPKFields();
}

function clearNonPKFields() {
	$('#ccyName').val(EMPTY_STRING);
	$('#ccyName_error').html(EMPTY_STRING);
	$('#ccyShortName').val(EMPTY_STRING);
	$('#ccyShortName_error').html(EMPTY_STRING);
	$('#ccyNotation').val(EMPTY_STRING);
	$('#ccyNotation_error').html(EMPTY_STRING);
	setCheckbox('hasSubCcy', '0');
	$('#hasSubCcy_error').html(EMPTY_STRING);
	$('#sccyName').val(EMPTY_STRING);
	$('#sccyName_error').html(EMPTY_STRING);
	$('#sccyShortName').val(EMPTY_STRING);
	$('#sccyShortName_error').html(EMPTY_STRING);
	$('#sccyNotation').val(EMPTY_STRING);
	$('#sccyNotation_error').html(EMPTY_STRING);
	$('#sccyUnitsForCcy').val(EMPTY_STRING);
	$('#sccyUnitsForCcy_error').html(EMPTY_STRING);
	$('#isoCcyCode').val(EMPTY_STRING);
	$('#isoCcyCode_error').html(EMPTY_STRING);
//	setCheckbox('enabled', YES);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);

}
function add() {

	setCheckbox('enabled', '1');
	$('#enabled').prop('disabled', true);
	setCheckbox('hasSubCcy', '0');
	hasSubCcyOnChange();
	$('#currencyCode').focus();

}
function hasSubCcyOnChange() {
	if ($('#hasSubCcy').is(':checked')) {
		$('#sccyName').attr('readonly', false);
		$('#sccyShortName').attr('readonly', false);
		$('#sccyNotation').attr('readonly', false);
		$('#sccyUnitsForCcy').attr('readonly', false);
	} else {
		$('#sccyName').val(EMPTY_STRING);
		clearError('sccyName_error');
		$('#sccyShortName').val(EMPTY_STRING);
		clearError('sccyShortName_error');
		$('#sccyNotation').val(EMPTY_STRING);
		clearError('sccyNotation_error');
		$('#sccyUnitsForCcy').val(EMPTY_STRING);
		clearError('sccyUnitsForCcy_error');
		$('#sccyName').attr('readonly', true);
		$('#sccyShortName').attr('readonly', true);
		$('#sccyNotation').attr('readonly', true);
		$('#sccyUnitsForCcy').attr('readonly', true);
	}

}
function checkclick(id) {
	switch (id) {
	case 'hasSubCcy':
		hasSubCcyOnChange();
		if ($('#hasSubCcy').is(':checked'))
			$('#sccyName').focus();
		else
			$('#isoCcyCode').focus();
	}

}
function modify() {

	$('#currencyCode').focus();
	if ($('#action').val() == MODIFY) {
		$('#enabled').prop('disabled', false);
	} else {
		$('#enabled').prop('disabled', true);
	}
}
function loadData() {
	$('#currencyCode').val(validator.getValue('CCY_CODE'));
	$('#ccyName').val(validator.getValue('DESCRIPTION'));
	$('#ccyShortName').val(validator.getValue('CONCISE_DESCRIPTION'));
	$('#ccyNotation').val(validator.getValue('CCY_NOTATION'));
	setCheckbox('hasSubCcy', validator.getValue('HAS_SUB_CCY'));
	hasSubCcyOnChange();
	$('#sccyName').val(validator.getValue('SUB_CCY_DESCRIPTION'));
	$('#sccyShortName').val(validator.getValue('SUB_CCY_CONCISE_DESCRIPTION'));
	$('#sccyNotation').val(validator.getValue('SUB_CCY_NOTATION'));
	$('#sccyUnitsForCcy').val(validator.getValue('SUB_CCY_UNITS_IN_CCY'));
	$('#isoCcyCode').val(validator.getValue('ISO_CCY_CODE'));
	$('#remarks').val(validator.getValue('REMARKS'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	resetLoading();
}
function view(source, primaryKey) {
	hideParent('cmn/qcurrency', source, primaryKey);
	resetLoading();
}
function validate(id) {
	switch (id) {
	case 'currencyCode':
		currencyCode_val();
		break;
	case 'ccyName':
		ccyName_val();
		break;
	case 'ccyShortName':
		ccyShortName_val();
		break;
	case 'ccyNotation':
		ccyNotation_val();
		break;
	case 'sccyShortName':
		sccyShortName_val();
		break;
	case 'sccyName':
		sccyName_val();
		break;
	case 'hasSubCcy':
		hasSubCcy_val();
		break;
	case 'sccyNotation':
		sccyNotation_val();
		break;
	case 'sccyUnitsForCcy':
		sccyUnitsForCcy_val();
		break;

	case 'isoCcyCode':
		isoCcyCode_val();
		break;

	case 'enabled':
		enabled_val();
		break;
	case 'remarks':
		remarks_val();
		break;
	}
}

function backtrack(id) {
	switch (id) {
	case 'currencyCode':
		setFocusLast('currencyCode');
		break;
	case 'ccyName':
		setFocusLast('currencyCode');
		break;
	case 'ccyShortName':
		setFocusLast('ccyName');
		break;
	case 'ccyNotation':
		setFocusLast('ccyShortName');
		break;
	case 'hasSubCcy':
		setFocusLast('ccyNotation');
		break;
	case 'sccyName':
		setFocusLast('hasSubCcy');
		break;
	case 'sccyShortName':
		setFocusLast('sccyName');
		break;
	case 'sccyNotation':
		setFocusLast('sccyShortName');
		break;
	case 'sccyUnitsForCcy':
		setFocusLast('sccyNotation');
		break;
	case 'isoCcyCode':
		setFocusLast('hasSubCcy');
		break;
	case 'enabled':
		if ($('#hasSubCcy').is(':checked')) {
			setFocusLast('sccyUnitsForCcy');
		} else {
			setFocusLast('isoCcyCode');
		}
		break;
	case 'remarks':
		if ($('#action').val() == MODIFY) {
			setFocusLast('enabled');
		} else {
			if ($('#hasSubCcy').is(':checked')) {
				setFocusLast('sccyUnitsForCcy');
			} else {
				setFocusLast('isoCcyCode');
			}
		}
		break;
	}
}

function hasSubCcy_val() {
	hasSubCcyOnChange();
	if ($('#hasSubCcy').is(':checked')) {
		setFocusLast('sccyName');
	} else {
		setFocusLast('isoCcyCode');
	}
	return true;
}
function enabled_val() {
	if ($('#action').val() == MODIFY) {
		setFocusLast('remarks');
	}
	return true;
}
function currencyCode_val() {
	var value = $('#currencyCode').val();
	clearError('currencyCode_error');
	if (isEmpty(value)) {
		setError('currencyCode_error', HMS_MANDATORY);
		setFocusLast('currencyCode');
		return false;
	} else if ($('#action').val() == ADD && !isValidCode(value)) {
		setError('currencyCode_error', HMS_INVALID_FORMAT);
		setFocusLast('currencyCode');
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('CCY_CODE', $('#currencyCode').val());
		validator.setValue(ACTION, $('#action').val());
		validator.setClass('patterns.config.web.forms.cmn.mcurrencybean');
		validator.setMethod('validateCurrencyCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('currencyCode_error', validator.getValue(ERROR));
			setFocusLast('currencyCode');
			return false;
		} else {
			if ($('#action').val() == MODIFY) {
				PK_VALUE = $('#currencyCode').val();
				if(!loadPKValues(PK_VALUE, 'currencyCode_error')){
					return false;
				}
			}
		}
	}
	setFocusLast('ccyName');
	return true;
}
function ccyName_val() {
	var value = $('#ccyName').val();
	clearError('ccyName_error');
	if (isEmpty(value)) {
		setError('ccyName_error', HMS_MANDATORY);
		setFocusLast('ccyName');
		return false;
	}
	if (!isValidName(value)) {
		setError('ccyName_error', HMS_INVALID_NAME);
		setFocusLast('ccyName');
		return false;
	}
	setFocusLast('ccyShortName');
	return true;
}
function ccyShortName_val() {
	var value = $('#ccyShortName').val();
	clearError('ccyShortName_error');
	if (isEmpty(value)) {
		setError('ccyShortName_error', HMS_MANDATORY);
		setFocusLast('ccyShortName');
		return false;
	}

	if (!isValidShortName(value)) {
		setError('ccyShortName_error', HMS_INVALID_SHORT_NAME);
		setFocusLast('ccyShortName');
		return false;
	}
	setFocusLast('ccyNotation');
	return true;
}
function ccyNotation_val() {
	var value = $('#ccyNotation').val();
	clearError('ccyNotation_error');
	if (isEmpty(value)) {
		setError('ccyNotation_error', HMS_MANDATORY);
		setFocusLast('ccyNotation');
		return false;
	}
	if (!isValidCurrencyNotation(value)) {
		setError('ccyNotation_error', HMS_INVALID_VALUE);
		setFocusLast('ccyNotation');
		return false;
	}
	setFocusLast('hasSubCcy');
	return true;
}
function sccyName_val() {
	var value = $('#sccyName').val();
	clearError('sccyName_error');
	if ($('#hasSubCcy').is(':checked')) {
		if (isEmpty(value)) {
			setError('sccyName_error', HMS_MANDATORY);
			setFocusLast('sccyName');
			return false;
		}

		if (!isValidName(value)) {
			setError('sccyName_error', HMS_INVALID_NAME);
			setFocusLast('sccyName');
			return false;
		}
	}
	setFocusLast('sccyShortName');
	return true;
}
function sccyShortName_val() {
	var value = $('#sccyShortName').val();
	clearError('sccyShortName_error');
	if ($('#hasSubCcy').is(':checked')) {
		if (isEmpty(value)) {
			setError('sccyShortName_error', HMS_MANDATORY);
			setFocusLast('sccyShortName');
			return false;
		}
		if (!isValidShortName(value)) {
			setError('sccyShortName_error', HMS_INVALID_SHORT_NAME);
			setFocusLast('sccyShortName');
			return false;
		}

	}
	setFocusLast('sccyNotation');
	return true;
}
function sccyNotation_val() {
	var value = $('#sccyNotation').val();
	clearError('sccyNotation_error');
	if ($('#hasSubCcy').is(':checked')) {
		if (isEmpty(value)) {
			setError('sccyNotation_error', HMS_MANDATORY);
			setFocusLast('sccyNotation');
			return false;
		}

		if (!isValidCurrencyNotation(value)) {
			setError('sccyNotation_error', HMS_INVALID_VALUE);
			setFocusLast('sccyNotation');
			return false;
		}
	}
	setFocusLast('sccyUnitsForCcy');
	return true;
}
function sccyUnitsForCcy_val() {
	var value = $('#sccyUnitsForCcy').val();
	clearError('sccyUnitsForCcy_error');
	if ($('#hasSubCcy').is(':checked')) {
		if (isEmpty(value)) {
			setError('sccyUnitsForCcy_error', HMS_MANDATORY);
			setFocusLast('sccyUnitsForCcy');
			return false;
		}
		if (!isValidFiveDigit(value)) {
			setError('sccyUnitsForCcy_error', HMS_INVALID_VALUE);
			setFocusLast('sccyUnitsForCcy');
			return false;
		}
	}
	setFocusLast('isoCcyCode');
	return true;
}
function isoCcyCode_val() {
	var value = $('#isoCcyCode').val();
	clearError('isoCcyCode_error');
	if (isEmpty(value)) {
		setError('isoCcyCode_error', HMS_MANDATORY);
		setFocusLast('isoCcyCode');
		return false;
	}
	if (!isNumeric(value)) {
		setError('isoCcyCode_error', HMS_INVALID_NUMBER);
		setFocusLast('isoCcyCode');
		return false;
	}

	if (!isValidISOCurrency(value)) {
		setError('isoCcyCode_error', HMS_INVALID_VALUE);
		setFocusLast('isoCcyCode');
		return false;
	}
	if ($('#action').val() == ADD) {
		setFocusLast('remarks');
	} else {
		setFocusLast('enabled');
	}
	return true;
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', HMS_INVALID_REMARKS);
			setFocusLast('remarks');
			return false;

		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', HMS_INVALID_REMARKS);
			setFocusLast('remarks');
			return false;
		}
	}
	if ($('#action').val() == MODIFY) {
		if (isEmpty(value)) {
			setError('remarks_error', HMS_MANDATORY);
			setFocusLast('remarks');
			return false;
		}

	}
	setFocusOnSubmit();
	return true;
}