var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MCHANNELS';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}
function clearFields() {
	$('#channelCode').val(EMPTY_STRING);
	$('#description').val(EMPTY_STRING);
	$('#conciseDescription').val(EMPTY_STRING);
	setCheckbox('retailBank', NO);
	setCheckbox('corporateBank', NO);
	$('#purposeChennel').val(EMPTY_STRING);
	setCheckbox('enabled', NO);
	$('#remarks').val(EMPTY_STRING);

}
function loadData() {
	$('#channelCode').val(validator.getValue('CHANNEL_CODE'));
	$('#description').val(validator.getValue('DESCRIPTION'));
	$('#conciseDescription').val(validator.getValue('CONCISE_DESCRIPTION'));
	setCheckbox('retailBank', validator.getValue('RETAIL_BANKING_FLAG'));
	setCheckbox('corporateBank', validator.getValue('CORPORATE_BANKING_FLAG'));
	$('#purposeChennel').val(validator.getValue('PURPOSE_OF_CHANNEL'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadAuditFields(validator);
}
