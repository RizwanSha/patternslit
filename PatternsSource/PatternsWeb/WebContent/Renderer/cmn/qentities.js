var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'IENTITIES';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}

function clearFields() {
	clearNonPKFields();
}
function clearNonPKFields() {
	$('#entityId').val(EMPTY_STRING);
	$('#entityId_error').html(EMPTY_STRING);
	$('#entityName').val(EMPTY_STRING);
	$('#entityName_error').html(EMPTY_STRING);
	$('#regOfficeAdd').val(EMPTY_STRING);
	$('#regOfficeAdd_error').html(EMPTY_STRING);
	$('#pinCode').val(EMPTY_STRING);
	$('#pinCode_error').html(EMPTY_STRING);
	$('#stateCode').val(EMPTY_STRING);
	$('#stateCode_error').html(EMPTY_STRING);
	$('#panCard').val(EMPTY_STRING);
	$('#panCard_error').html(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
}
function loadData() {
	$('#entityId_pic').remove();
	$('#entityId').val(validator.getValue('ENTITY_ID'));
	$('#entityName').val(validator.getValue('ENTITY_NAME'));
	$('#regOfficeAdd').val(validator.getValue('REG_ADDRESS'));
	$('#pinCode').val(validator.getValue('PIN_CODE'));
	$('#stateCode').val(validator.getValue('STATE_CODE'));
	$('#stateCode_desc').html(validator.getValue('F2_DESCRIPTION'));
	$('#panCard').val(validator.getValue('PAN_NO'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadAuditFields(validator);
	resetLoading();
}
