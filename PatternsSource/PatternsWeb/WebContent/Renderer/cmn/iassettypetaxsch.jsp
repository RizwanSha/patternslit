<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.cmn.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/cmn/iassettypetaxsch.js" />
	</web:dependencies>
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="iassettypetaxsch.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/cmn/iassettypetaxsch" id="iassettypetaxsch" method="POST">
				<web:dividerBlock>
					<type:optionBox add="true" modify="true" view="true" template="true" />
				</web:dividerBlock>
				<web:viewContent id="po_view1" styleClass="hidden">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="iassettypetaxsch.assettypecode" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:assetTypeCode property="assetTypeCode" id="assetTypeCode" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="form.effectivedate" var="common" mandatory="true" />
										</web:column>
										<web:column>
											<type:date property="effectiveDate" id="effectiveDate" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:sectionTitle var="program" key="iassettypetaxsch.section" />
							<type:error property="gridError" />
							<web:section>
								<web:table>
									<web:rowOdd>
										<web:column>
											<web:grid height="230px" width="701px" id="iassettypetaxsch_innerGrid" src="cmn/iassettypetaxsch_innerGrid.xml">
											</web:grid>
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:viewContent id="scheduleCode_Fields" styleClass="form-horizontal hidden">
								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="200px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowEven>
											<web:column>
												<web:legend key="iassettypetaxsch.statecode" var="program" />
											</web:column>
											<web:column>
												<type:stateCode property="stateCode" id="stateCode" readOnly="true" />
											</web:column>
										</web:rowEven>
										<web:rowOdd>
											<web:column>
												<web:legend key="iassettypetaxsch.taxschedulecode" var="program" />
											</web:column>
											<web:column>
												<type:scheduleCode property="taxSheduleCode" id="taxSheduleCode" lookup="true" />
											</web:column>
										</web:rowOdd>
									</web:table>
								</web:section>
								<web:section>
									<web:table>
										<web:rowEven>
											<web:column>
												<web:button id="scheduleConfig" key="iassettypetaxsch.update" var="program" onclick="updateScheduleConfigDetails()" />
												<web:button id="clearProductGrid" key="iassettypetaxsch.clearfields" var="program" onclick="clearTaxScheduleCodeFields()" />
												<web:button key="form.close" id="close" var="common" onclick="closePopup('0')" />
											</web:column>
										</web:rowEven>
									</web:table>
								</web:section>
							</web:viewContent>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="form.enabled" var="common" />
										</web:column>
										<web:column>
											<type:checkbox property="enabled" id="enabled" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="form.remarks" var="common" />
										</web:column>
										<web:column>
											<type:remarks property="remarks" id="remarks" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column span="2">
											<web:submitReset />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
						</web:sectionBlock>
					</web:dividerBlock>
				</web:viewContent>
				<web:element property="xmliassettypetaxschGrid" />
				<web:element property="command" />
				<web:element property="action" />
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
	<web:queryGrid />
	<web:TBAGrid />
	<web:contextSearch />
</web:fragment>
