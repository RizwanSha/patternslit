var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MENTITIES';
var beforeNoOfDigits;
var w_country_struc;
var w_version = null;
function init() {
	refreshTBAGrid();
	refreshQueryGrid();
	if (_redisplay) {
		$('#finYear').val($('#fin').val());
		$('#weekStartsFrom').val($('#weekStarts').val());
		var multiCurAccount = $('#multiCur').val();
		multiCur_val(multiCurAccount);
		$('#w_version').val($('#w_version').val());
		var hasBranches = $('#hiddenhas').val();
		length_enable_val(hasBranches);
		var w_version = $('#w_version').val();
		enable_val(w_version);
		entityDissolved_val();
	}
}

function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'MENTITIES_MAIN');
}

function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}

function doclearfields(id) {
	switch (id) {
	case 'entityCode':
		if (isEmpty($('#entityCode').val())) {
			$('#entityCode_error').html(EMPTY_STRING);
			$('#entityCode_desc').html(EMPTY_STRING);
			clearNonPKFields();
			break;
		}
	}
}

function clearFields() {
	$('#entityCode').val(EMPTY_STRING);
	$('#entityCode_error').html(EMPTY_STRING);
	$('#entityCode_desc').html(EMPTY_STRING);
	clearNonPKFields();
}

function clearNonPKFields() {
	$('#name').val(EMPTY_STRING);
	$('#name_error').html(EMPTY_STRING);
	$('#defaultLanCode').val(EMPTY_STRING);
	$('#defaultLanCode_error').html(EMPTY_STRING);
	$('#defaultDateFormat').val(EMPTY_STRING);
	$('#defaultDateFormat_error').html(EMPTY_STRING);
	$('#dateOfIncorporation').val(EMPTY_STRING);
	$('#dateOfIncorporation_error').html(EMPTY_STRING);
	$('#bankLicenseType').val(EMPTY_STRING);
	$('#bankLicenseType_error').html(EMPTY_STRING);
	$('#bankLicenseType_desc').html(EMPTY_STRING);
	$('#baseCurrCode').val(EMPTY_STRING);
	$('#baseCurrCode_error').html(EMPTY_STRING);
	$('#baseCurrCode_desc').html(EMPTY_STRING);
	$('#baseCurrCode').prop('readOnly', false);
	$('#baseAddrType').val(EMPTY_STRING);
	$('#baseAddrType_error').html(EMPTY_STRING);
	$('#baseAddrType_desc').html(EMPTY_STRING);
	$('#baseAddrType').prop('readOnly', false);
	$('#countryCode').val(EMPTY_STRING);
	$('#countryCode_error').html(EMPTY_STRING);
	$('#countryCode_desc').html(EMPTY_STRING);
	$('#countryCode').prop('readOnly', false);
	$('#finYear').val(EMPTY_STRING);
	$('#finYear_error').html(EMPTY_STRING);
	$('#finYear').prop('disabled', false);
	$('#weekStartsFrom').val(EMPTY_STRING);
	$('#weekStartsFrom_error').html(EMPTY_STRING);
	$('#weekStartsFrom').prop('disabled', false);
	setCheckbox('hasBranches', NO);
	$('#hasBranches').prop('disabled', false);
	$('#lengthBranchCode').val(EMPTY_STRING);
	$('#lengthBranchCode_error').html(EMPTY_STRING);
	$('#lengthBranchCode').prop('readOnly', false);
	setCheckbox('multiCurAccount', NO);
	$('#geoUnitStateCode').val(EMPTY_STRING);
	$('#geoUnitStateCode_error').html(EMPTY_STRING);
	$('#geoUnitStateCode_desc').html(EMPTY_STRING);
	$('#geoUnitStateCode').prop('readOnly', false);
	$('#geoUnitDistrictCode').val(EMPTY_STRING);
	$('#geoUnitDistrictCode_error').html(EMPTY_STRING);
	$('#geoUnitDistrictCode_desc').html(EMPTY_STRING);
	$('#geoUnitDistrictCode').prop('readOnly', false);
	$('#geoUnitPinCode').val(EMPTY_STRING);
	$('#geoUnitPinCode_error').html(EMPTY_STRING);
	$('#geoUnitPinCode_desc').html(EMPTY_STRING);
	$('#geoUnitPinCode').prop('readOnly', false);
	setCheckbox('entityDissolved', NO);
	$('#entityDissolved').prop('disabled', false);
	$('#dissolvedOn').val(EMPTY_STRING);
	$('#dissolvedOn_error').html(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
}

function modify() {
	$('#entityCode').focus();
}

function doHelp(id) {
	switch (id) {
	case 'entityCode':
		help('COMMON', 'HLP_ENTITY_CODE', $('#entityCode').val(), EMPTY_STRING, $('#entityCode'));
		break;
	case 'bankLicenseType':
		help('COMMON', 'HLP_BANK_LIC_TYPE', $('#bankLicenseType').val(), EMPTY_STRING, $('#bankLicenseType'));
		break;
	case 'baseCurrCode':
		help('COMMON', 'HLP_CURRENCY_M', $('#baseCurrCode').val(), EMPTY_STRING, $('#baseCurrCode'));
		break;
	case 'baseAddrType':
		help('COMMON', 'HLP_ADDR_TYPE_M', $('#baseAddrType').val(), EMPTY_STRING, $('#baseAddrType'));
		break;
	case 'countryCode':
		help('COMMON', 'HLP_COUNTRY', $('#countryCode').val(), EMPTY_STRING, $('#countryCode'));
		break;
	case 'geoUnitStateCode':
		help('COMMON', 'HLP_GEOUNIT_FOR_COUNTRY', $('#geoUnitStateCode').val(), w_country_struc, $('#geoUnitStateCode'));
		break;
	case 'geoUnitDistrictCode':
		help('COMMON', 'HLP_GEOUNIT_STRUC_SPEC', $('#geoUnitDistrictCode').val(), $('#geoUnitStateCode').val(), $('#geoUnitDistrictCode'));
		break;
	case 'geoUnitPinCode':
		help('COMMON', 'HLP_GEOUNIT_STRUC_SPEC_LEAF', $('#geoUnitPinCode').val(), $('#geoUnitDistrictCode').val(), $('#geoUnitPinCode'));
		break;
	}
}

function loadData() {
	$('#entityCode').val(validator.getValue('ENTITY_CODE'));
	$('#name').val(validator.getValue('ENTITY_NAME'));
	var defaultLanCode = validator.getValue('DEFA_LANG_CODE');
	var defaultDateFormat = (validator.getValue('DEFAULT_DATE_FORMAT'));
	var dateOfIncorporation = (validator.getValue('DATE_OF_INCORP'));
	var bankLicenseType = validator.getValue('BANK_LIC_TYPES');
	var bankLicenseType_desc = validator.getValue('F1_DESCRIPTION');
	var baseCurrCode = (validator.getValue('BASE_CCY_CODE'));
	var baseCurrCode_desc = validator.getValue('F2_DESCRIPTION');
	var baseAddrType = validator.getValue('BASE_ADDR_TYPE');
	var baseAddrType_desc = validator.getValue('F3_DESCRIPTION');
	var countryCode = (validator.getValue('OUR_COUNTRY_CODE'));
	var countryCode_desc = validator.getValue('F4_DESCRIPTION');
	var finYear = (validator.getValue('FIN_YEAR_START_MONTH'));
	var weekStartsFrom = validator.getValue('START_DAY_OF_WEEK');
	var hasBranches = validator.getValue('BRANCH_REQD');
	var lengthBranchCode = (validator.getValue('BRN_OFFICE_NO_OF_DIGITS'));
	var multiCurAccount = (validator.getValue('MULTI_CCY_ALLOWED'));
	var geoUnitStateCode = validator.getValue('GEO_UNIT_STRUC_STATE_CODE');
	var geoUnitStateCode_desc = validator.getValue('F5_DESCRIPTION');
	var geoUnitDistrictCode = (validator.getValue('GEO_UNIT_STRUC_DIST_CODE'));
	var geoUnitDistrictCode_desc = validator.getValue('F6_DESCRIPTION');
	var geoUnitPinCode = (validator.getValue('GEO_UNIT_STRUC_PINCODE'));
	var geoUnitPinCode_desc = validator.getValue('F7_DESCRIPTION');
	var entityDissolved = validator.getValue('ENTITY_DISSOLVED');
	var dissolvedOn = (validator.getValue('ENTITY_DISSOLVED_ON'));
	var remarks = (validator.getValue('REMARKS'));
	$('#countryCode').val(countryCode);
	beforeNoOfDigits = validator.getValue('BRN_OFFICE_NO_OF_DIGITS');
	w_version = validator.getValue('DATA_VERSION');
	enable_val(w_version);
	$('#w_version').val(w_version);
	countryCode_val();
	if (hasBranches) {
		$('#hasBranches').prop('disabled', true);
		$('#lengthBranchCode').val(lengthBranchCode);
		$('#lengthBranchCode').prop('readOnly', false);
	} else {
		setCheckbox('hasBranches', NO);
		$('#hasBranches').prop('disabled', false);
		$('#lengthBranchCode').val(EMPTY_STRING);
		$('#lengthBranchCode_error').html(EMPTY_STRING);
		$('#lengthBranchCode').prop('readOnly', true);
	}
	$('#defaultLanCode').val(defaultLanCode);
	$('#defaultDateFormat').val(defaultDateFormat);
	$('#dateOfIncorporation').val(dateOfIncorporation);
	$('#bankLicenseType').val(bankLicenseType);
	$('#bankLicenseType_desc').html(bankLicenseType_desc);
	$('#baseCurrCode').val(baseCurrCode);
	$('#baseCurrCode_desc').html(baseCurrCode_desc);
	$('#baseAddrType').val(baseAddrType);
	$('#baseAddrType_desc').html(baseAddrType_desc);
	$('#countryCode').val(countryCode);
	$('#countryCode_desc').html(countryCode_desc);
	$('#finYear').val(finYear);
	$('#weekStartsFrom').val(weekStartsFrom);
	setCheckbox('hasBranches', hasBranches);
	$('#lengthBranchCode').val(lengthBranchCode);
	setCheckbox('multiCurAccount', multiCurAccount);
	$('#geoUnitStateCode').val(geoUnitStateCode);
	$('#geoUnitStateCode_desc').html(geoUnitStateCode_desc);
	$('#geoUnitDistrictCode').val(geoUnitDistrictCode);
	$('#geoUnitDistrictCode_desc').html(geoUnitDistrictCode_desc);
	$('#geoUnitPinCode').val(geoUnitPinCode);
	$('#geoUnitPinCode_desc').html(geoUnitPinCode_desc);
	$('#remarks').val(remarks);
	setCheckbox('entityDissolved', entityDissolved);
	$('#dissolvedOn').val(dissolvedOn);
	setFocusLast('entityCode');
	resetLoading();
}

function view(source, primaryKey) {
	hideParent('cmn/qentities', source, primaryKey);
	resetLoading();
}

function validate(id) {
	switch (id) {
	case 'entityCode':
		entityCode_val();
		break;
	case 'name':
		name_val();
		break;
	case 'defaultLanCode':
		defaultLanCode_val();
		break;
	case 'defaultDateFormat':
		defaultDateFormat_val();
		break;
	case 'dateOfIncorporation':
		dateOfIncorporation_val();
		break;
	case 'bankLicenseType':
		bankLicenseType_val();
		break;
	case 'baseCurrCode':
		baseCurrCode_val();
		break;
	case 'baseAddrType':
		baseAddrType_val();
		break;
	case 'countryCode':
		countryCode_val();
		break;
	case 'finYear':
		finYear_val();
		break;
	case 'weekStartsFrom':
		weekStartsFrom_val();
		break;
	case 'hasBranches':
		hasBranches_val();
		break;
	case 'lengthBranchCode':
		lengthBranchCode_val();
		break;
	case 'multiCurAccount':
		multiCurAccount_val();
		break;
	case 'geoUnitStateCode':
		geoUnitStateCode_val();
		break;
	case 'geoUnitDistrictCode':
		geoUnitDistrictCode_val();
		break;
	case 'geoUnitPinCode':
		geoUnitPinCode_val();
		break;
	case 'entityDissolved':
		entityDissolved_val();
		break;
	case 'dissolvedOn':
		dissolvedOn_val();
		break;
	case 'remarks':
		remarks_val();
		break;
	}
}

function backtrack(id) {
	switch (id) {
	case 'entityCode':
		setFocusLast('entityCode');
		break;
	case 'name':
		setFocusLast('entityCode');
		break;
	case 'defaultLanCode':
		setFocusLast('name');
		break;
	case 'defaultDateFormat':
		setFocusLast('defaultLanCode');
		break;
	case 'dateOfIncorporation':
		setFocusLast('name');
		break;
	case 'bankLicenseType':
		if (w_version > 0) {
			setFocusLast('name');
		} else {
			setFocusLast('dateOfIncorporation');
			if ($('#dateOfIncorporation').prop('readOnly')) {
				setFocusLast('name');
			}
		}
		break;
	case 'baseCurrCode':
		setFocusLast('bankLicenseType');
		break;
	case 'baseAddrType':
		setFocusLast('baseCurrCode');
		break;
	case 'countryCode':
		setFocusLast('baseAddrType');
		break;
	case 'finYear':
		setFocusLast('countryCode');
		break;
	case 'weekStartsFrom':
		setFocusLast('finYear');
		break;
	case 'hasBranches':
		if (w_version > 0) {
			setFocusLast('bankLicenseType');
		} else {
			setFocusLast('weekStartsFrom');
		}
		break;
	case 'lengthBranchCode':
		if ($('#hasBranches').is(':checked')) {
			if (w_version > 0) {
				setFocusLast('bankLicenseType');
			} else {
				setFocusLast('weekStartsFrom');
				if ($('#weekStartsFrom').prop('disabled')) {
					setFocusLast('bankLicenseType');
				}
			}
		} else {
			setFocusLast('hasBranches');
		}
		break;
	case 'geoUnitStateCode':
		setFocusLast('lengthBranchCode');
		if ($('#lengthBranchCode').prop('readOnly')) {
			setFocusLast('lengthBranchCode');
		} else {
			setFocusLast('hasBranches');
		}
		break;
	case 'geoUnitDistrictCode':
		setFocusLast('geoUnitStateCode');
		break;
	case 'geoUnitPinCode':
		setFocusLast('geoUnitDistrictCode');
		break;
	case 'entityDissolved':
		if (w_version == 0) {
			setFocusLast('geoUnitPinCode');
		}
		if (w_add_struc_reqd == 0) {
			setFocusLast('geoUnitStateCode');
		} else {
			setFocusLast('geoUnitPinCode');
			if ($('#geoUnitPinCode').prop('readOnly')) {
				setFocusLast('geoUnitStateCode');
				if ($('#geoUnitStateCode').prop('readOnly')) {
					setFocusLast('lengthBranchCode');
					if ($('#lengthBranchCode').prop('readOnly')) {
						setFocusLast('lengthBranchCode');
					} else {
						setFocusLast('hasBranches');
					}
				} else {
					setFocusLast('hasBranches');
				}
			}
		}
		break;
	case 'dissolvedOn':
		setFocusLast('entityDissolved');
		break;
	case 'remarks':
		if (w_version == 0) {
			setFocusLast('geoUnitPinCode');
		} else {
			setFocusLast('entityDissolved');
			if ($('#entityDissolved').prop('disabled')) {
				if (w_add_struc_reqd == 0) {
					setFocusLast('geoUnitStateCode');
				} else {
					setFocusLast('geoUnitPinCode');
				}
			}
		}
		break;
	}
}

function checkclick(id) {
	switch (id) {
	case 'hasBranches':
		if ($('#hasBranches').is(':checked')) {
			$('#hasBranches').prop('disabled', true);
			$('#lengthBranchCode').prop('readOnly', false);
			setFocusLast('lengthBranchCode');
		} else {
			setCheckbox('hasBranches', NO);
			$('#hasBranches').prop('disabled', false);
			$('#lengthBranchCode').val(EMPTY_STRING);
			$('#lengthBranchCode_error').html(EMPTY_STRING);
			$('#lengthBranchCode').prop('readOnly', true);
		}
		break;
	case 'multiCurAccount':
		if (w_version > 0) {
			$('#multiCurAccount').prop('readOnly', true);
		} else if (w_version == 0) {
			$('#multiCurAccount').prop('readOnly', true);
		}
		break;
	case 'entityDissolved':
		$('#entityDissolved').prop('disabled', false);
		if ($('#entityDissolved').is(':checked')) {
			$('#dissolvedOn').val(getCBD());
			$('#dissolvedOn').prop('readOnly', true);
		} else {
			$('#dissolvedOn').val(EMPTY_STRING);
			$('#dissolvedOn_error').html(EMPTY_STRING);
			$('#dissolvedOn').prop('readOnly', true);
		}
		break;
	}
}

function enable_val(w_version) {
	if (w_version > 0) {
		$('#defaultLanCode').prop('readOnly', true);
		$('#defaultDateFormat').prop('readOnly', true);
		$('#dateOfIncorporation').prop('readOnly', true);
		$('#baseCurrCode').prop('readOnly', true);
		$('#baseCurrCode_pic').prop('disabled', true);
		$('#baseAddrType').prop('readOnly', true);
		$('#baseAddrType_pic').prop('disabled', true);
		$('#countryCode').prop('readOnly', true);
		$('#countryCode_pic').prop('disabled', true);
		$('#finYear').prop('disabled', true);
		$('#weekStartsFrom').prop('disabled', true);
		$('#multiCurAccount').prop('disabled', true);
		$('#geoUnitStateCode').prop('readOnly', true);
		$('#geoUnitStateCode_pic').prop('readOnly', true);
		$('#geoUnitDistrictCode').prop('readOnly', true);
		$('#geoUnitDistrictCode_pic').prop('readOnly', true);
		$('#geoUnitPinCode').prop('readOnly', true);
		$('#geoUnitPinCode_pic').prop('readOnly', true);
		setCheckbox('entityDissolved', NO);
		$('#entityDissolved').prop('disabled', false);
		$('#dissolvedOn').val(EMPTY_STRING);
		$('#dissolvedOn_error').html(EMPTY_STRING);
		$('#dissolvedOn').prop('readOnly', true);
	} else if (w_version == 0) {
		$('#defaultLanCode').prop('readOnly', true);
		$('#defaultDateFormat').prop('readOnly', true);
		$('#dateOfIncorporation').prop('readOnly', false);
		$('#baseCurrCode').prop('readOnly', false);
		$('#baseCurrCode_pic').prop('disabled', false);
		$('#baseAddrType').prop('readOnly', false);
		$('#baseAddrType_pic').prop('disabled', false);
		$('#countryCode').prop('readOnly', false);
		$('#countryCode_pic').prop('disabled', false);
		$('#finYear').prop('disabled', false);
		$('#weekStartsFrom').prop('disabled', false);
		$('#multiCurAccount').prop('disabled', true);
		$('#geoUnitStateCode').prop('readOnly', false);
		$('#geoUnitStateCode_pic').prop('readOnly', false);
		$('#geoUnitDistrictCode').prop('readOnly', false);
		$('#geoUnitDistrictCode_pic').prop('readOnly', false);
		$('#geoUnitPinCode').prop('readOnly', false);
		$('#geoUnitPinCode_pic').prop('readOnly', false);
		setCheckbox('entityDissolved', NO);
		$('#entityDissolved').prop('disabled', true);
		$('#dissolvedOn').val(EMPTY_STRING);
		$('#dissolvedOn_error').html(EMPTY_STRING);
		$('#dissolvedOn').prop('readOnly', true);
	}
}

function length_enable_val(hasBranches) {
	if (hasBranches == 0) {
		$('#lengthBranchCode').val(EMPTY_STRING);
		$('#lengthBranchCode_error').html(EMPTY_STRING);
		$('#lengthBranchCode').prop('readOnly', true);
	} else {
		setCheckbox('hasBranches', YES);
		$('#hasBranches').prop('disabled', true);
		$('#lengthBranchCode').prop('readOnly', false);
	}

}

function multiCur_val(multiCurAccount) {
	var w_version = $('#w_version').val();
	if (multiCurAccount) {
		if (w_version > 0) {
			setCheckbox('multiCurAccount', YES);
			$('#multiCurAccount').prop('readOnly', true);
		} else if (w_version == 0) {
			setCheckbox('multiCurAccount', YES);
			$('#multiCurAccount').prop('readOnly', true);
		}
	} else {
		if (w_version > 0) {
			setCheckbox('multiCurAccount', NO);
			$('#multiCurAccount').prop('readOnly', true);
		} else if (w_version == 0) {
			setCheckbox('multiCurAccount', NO);
			$('#multiCurAccount').prop('readOnly', true);
		}
	}
	return true;
}

function entityCode_val() {
	var value = $('#entityCode').val();
	clearError('entityCode_error');
	if (isEmpty(value)) {
		setError('entityCode_error', MANDATORY);
		return false;
	}
	if (!isValidCode(value)) {
		setError('entityCode_error', HMS_INVALID_FORMAT);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('ENTITY_CODE', $('#entityCode').val());
		validator.setValue(ACTION, $('#action').val());
		validator.setClass('patterns.config.web.forms.cmn.mentitiesbean');
		validator.setMethod('validateEntityCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('entityCode_error', validator.getValue(ERROR));
			$('#entityCode_desc').html(EMPTY_STRING);
			return false;
		}
		$('#entityCode_desc').html(validator.getValue('ENTITY_NAME'));
		w_version = validator.getValue('DATA_VERSION');
		enable_val(w_version);
		if ($('#action').val() == MODIFY) {
			PK_VALUE = $('#entityCode').val();
			if (!loadPKValues(PK_VALUE, 'entityCode_error')) {
				return false;
			}
		}
	}
	setFocusLast('name');
	return true;
}

function name_val() {
	var value = $('#name').val();
	var w_version = $('#w_version').val();
	clearError('name_error');
	if (isEmpty(value)) {
		setError('name_error', MANDATORY);
		return false;
	}
	if (!isValidDescription(value)) {
		setError('name_error', INVALID_DESCRIPTION);
		return false;
	}
	if (w_version > 0) {
		setFocusLast('bankLicenseType');
	} else if (w_version == 0) {
		setFocusLast('dateOfIncorporation');
	}
	return true;
}

function defaultLanCode_val() {
	var w_version = $('#w_version').val();
	if (w_version > 0) {
		$('#defaultLanCode').prop('readOnly', true);
	} else if (w_version == 0) {
		$('#defaultLanCode').prop('readOnly', true);
	}
	return true;
}

function defaultDateFormat_val() {
	var w_version = $('#w_version').val();
	if (w_version > 0) {
		$('#defaultDateFormat').prop('readOnly', true);
	} else if (w_version == 0) {
		$('#defaultDateFormat').prop('readOnly', true);
	}
	return true;
}

function dateOfIncorporation_val() {
	var value = $('#dateOfIncorporation').val();
	clearError('dateOfIncorporation_error');
	var w_version = $('#w_version').val();
	var cbd = getCBD();
	if (w_version > 0) {
		$('#dateOfIncorporation').prop('readOnly', true);
	} else if (w_version == 0) {
		$('#dateOfIncorporation').prop('readOnly', false);
		if (isEmpty(value)) {
			$('#dateOfIncorporation').val(cbd);
			value = cbd;
		}
		if (!isDate(value)) {
			setError('dateOfIncorporation_error', HMS_INVALID_DATE);
			return false;
		}
		if (isDateGreater(value, cbd)) {
			setError('dateOfIncorporation_error', HMS_DATE_LCBD);
			return false;
		}
	}
	setFocusLast('bankLicenseType');
	return true;
}

function bankLicenseType_val() {
	var value = $('#bankLicenseType').val();
	clearError('bankLicenseType_error');
	var w_version = $('#w_version').val();
	if (isEmpty(value)) {
		setError('bankLicenseType_error', MANDATORY);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('BANK_LIC_TYPES', $('#bankLicenseType').val());
	validator.setValue(ACTION, USAGE);
	validator.setClass('patterns.config.web.forms.cmn.mentitiesbean');
	validator.setMethod('validateBankLicenseType');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('bankLicenseType_error', validator.getValue(ERROR));
		$('#bankLicenseType_desc').html(EMPTY_STRING);
		return false;
	}
	$('#bankLicenseType_desc').html(validator.getValue("DESCRIPTION"));
	if (w_version > 0) {
		setFocusLast('hasBranches');
		if ($('#hasBranches').is(':checked')) {
			$('#hasBranches').prop('disabled', true);
			$('#lengthBranchCode').prop('readOnly', false);
			setFocusLast('lengthBranchCode');
		}
	} else if (w_version == 0) {
		setFocusLast('baseCurrCode');
	}
	return true;
}

function baseCurrCode_val() {
	var value = $('#baseCurrCode').val();
	clearError('baseCurrCode_error');
	var w_version = $('#w_version').val();
	if (w_version > 0) {
		$('#baseCurrCode').prop('readOnly', true);
		$('#baseCurrCode_pic').prop('disabled', true);
	} else if (w_version == 0) {
		$('#baseCurrCode').prop('readOnly', false);
		$('#baseCurrCode_pic').prop('disabled', false);
		if (isEmpty(value)) {
			setError('baseCurrCode_error', MANDATORY);
			return false;
		}
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('CCY_CODE', $('#baseCurrCode').val());
		validator.setValue('W_VERSION', w_version);
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.cmn.mentitiesbean');
		validator.setMethod('validateBaseCurrCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('baseCurrCode_error', validator.getValue(ERROR));
			$('#baseCurrCode_desc').html(EMPTY_STRING);
			return false;
		}
		$('#baseCurrCode_desc').html(validator.getValue("DESCRIPTION"));
	}
	setFocusLast('baseAddrType');
	return true;
}

function baseAddrType_val() {
	var value = $('#baseAddrType').val();
	clearError('baseAddrType_error');
	var w_version = $('#w_version').val();
	if (w_version > 0) {
		$('#baseAddrType').prop('readOnly', true);
		$('#baseAddrType_pic').prop('disabled', true);
	} else if (w_version == 0) {
		$('#baseAddrType').prop('readOnly', false);
		$('#baseAddrType_pic').prop('disabled', false);
		if (isEmpty(value)) {
			setError('baseAddrType_error', MANDATORY);
			return false;
		}
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('ADDR_TYPE', $('#baseAddrType').val());
		validator.setValue('W_VERSION', w_version);
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.cmn.mentitiesbean');
		validator.setMethod('validateBaseAddrType');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('baseAddrType_error', validator.getValue(ERROR));
			$('#baseAddrType_desc').html(EMPTY_STRING);
			return false;
		}
		$('#baseAddrType_desc').html(validator.getValue("DESCRIPTION"));
	}
	setFocusLast('countryCode');
	return true;
}

function countryCode_val() {
	var value = $('#countryCode').val();
	clearError('countryCode_error');
	var w_version = $('#w_version').val();
	if (w_version > 0) {
		$('#countryCode').prop('readOnly', true);
		$('#countryCode_pic').prop('disabled', true);
	} else if (w_version == 0) {
		$('#countryCode').prop('readOnly', false);
		$('#countryCode_pic').prop('disabled', false);
		if (isEmpty(value)) {
			setError('countryCode_error', MANDATORY);
			return false;
		}
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('COUNTRY_CODE', $('#countryCode').val());
		validator.setValue('W_VERSION', w_version);
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.cmn.mentitiesbean');
		validator.setMethod('validateCountryCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('countryCode_error', validator.getValue(ERROR));
			$('#countryCode_desc').html(EMPTY_STRING);
			return false;
		}
		$('#countryCode_desc').html(validator.getValue("DESCRIPTION"));
		w_country_struc = validator.getValue('GEO_UNIT_STRUC_CODE');
		w_add_struc_reqd = validator.getValue('GEO_UNIT_STRUC_REQ');
		if (w_add_struc_reqd == COLUMN_ENABLE) {
			$('#geoUnitDistrictCode').prop('readOnly', false);
			$('#geoUnitDistrictCode_pic').prop('readOnly', false);
			$('#geoUnitPinCode').prop('readOnly', false);
			$('#geoUnitPinCode_pic').prop('readOnly', false);
		} else {
			$('#geoUnitDistrictCode').val(EMPTY_STRING);
			$('#geoUnitDistrictCode_error').html(EMPTY_STRING);
			$('#geoUnitDistrictCode_desc').html(EMPTY_STRING);
			$('#geoUnitDistrictCode').prop('readOnly', true);
			$('#geoUnitDistrictCode_pic').prop('readOnly', true);
			$('#geoUnitPinCode').val(EMPTY_STRING);
			$('#geoUnitPinCode_error').html(EMPTY_STRING);
			$('#geoUnitPinCode_desc').html(EMPTY_STRING);
			$('#geoUnitPinCode').prop('readOnly', true);
			$('#geoUnitPinCode_pic').prop('readOnly', true);
		}
	}
	setFocusLast('finYear');
	return true;
}

function finYear_val() {
	var value = $('#finYear').val();
	clearError('finYear_error');
	var w_version = $('#w_version').val();
	if (w_version > 0) {
		$('#finYear').prop('disabled', true);
	} else if (w_version == 0) {
		$('#finYear').prop('disabled', false);
		if (isEmpty(value)) {
			setError('finYear_error', MANDATORY);
			return false;
		}
	}
	setFocusLast('weekStartsFrom');
}

function weekStartsFrom_val() {
	var value = $('#weekStartsFrom').val();
	clearError('weekStartsFrom_error');
	var w_version = $('#w_version').val();
	if (w_version > 0) {
		$('#weekStartsFrom').prop('disabled', true);
	} else if (w_version == 0) {
		$('#weekStartsFrom').prop('disabled', false);
		if (isEmpty(value)) {
			setError('weekStartsFrom_error', MANDATORY);
			return false;
		}
	}
	if ($('#hasBranches').is(':checked')) {
		setFocusLast('lengthBranchCode');
	} else {
		setFocusLast('hasBranches');
	}
}

function hasBranches_val() {
	var w_version = $('#w_version').val();
	if ($('#hasBranches').is(':checked')) {
		setCheckbox('hasBranches', YES);
		$('#hasBranches').prop('disabled', true);
		$('#lengthBranchCode').prop('readOnly', false);
		setFocusLast('lengthBranchCode');
	} else {
		setCheckbox('hasBranches', NO);
		$('#hasBranches').prop('disabled', false);
		$('#lengthBranchCode').val(EMPTY_STRING);
		$('#lengthBranchCode_error').html(EMPTY_STRING);
		$('#lengthBranchCode').prop('readOnly', true);
		if (w_version > 0) {
			setFocusLast('entityDissolved');
		} else if (w_version == 0) {
			setFocusLast('geoUnitStateCode');
		}
	}
	return true;
}

function lengthBranchCode_val() {
	var value = $('#lengthBranchCode').val();
	var w_version = $('#w_version').val();
	beforeNoOfDigits;
	var value1 = (parseInt(value));
	var value2 = (parseInt(beforeNoOfDigits));
	clearError('lengthBranchCode_error');
	if ($('#hasBranches').is(':checked')) {
		$('#lengthBranchCode').prop('readOnly', false);
		if (w_version > 0) {
			if (isEmpty(value1)) {
				setError('lengthBranchCode_error', MANDATORY);
				return false;
			}
			if (!isPositive(value1)) {
				setError('lengthBranchCode_error', HMS_POSITIVE_AMOUNT);
				return false;
			}
			if (isZero(value1)) {
				setError('lengthBranchCode_error', ZERO_CHECK);
				return false;
			}
			if (value1 < value2) {
				setError('lengthBranchCode_error', BRN_CODE_LEN_CANNOT_REDUCE);
				return false;
			}
			if (value1 > 12) {
				setError('lengthBranchCode_error', BRN_CODE_LS);
				return false;
			}
		} else if (w_version == 0) {
			if (isEmpty(value1)) {
				setError('lengthBranchCode_error', MANDATORY);
				return false;
			}
			if (!isPositive(value1)) {
				setError('lengthBranchCode_error', HMS_POSITIVE_AMOUNT);
				return false;
			}
			if (isZero(value1)) {
				setError('lengthBranchCode_error', ZERO_CHECK);
				return false;
			}
			if (value1 > 12) {
				setError('lengthBranchCode_error', BRN_CODE_LS);
				return false;
			}
		}
	} else {
		$('#lengthBranchCode').val(EMPTY_STRING);
		$('#lengthBranchCode_error').html(EMPTY_STRING);
		$('#lengthBranchCode').prop('readOnly', true);
	}
	if (w_version > 0) {
		setFocusLast('geoUnitStateCode');
		if ($('#geoUnitStateCode').prop('readOnly')) {
			setFocusLast('entityDissolved');
			if ($('#entityDissolved').prop('disabled')) {
				setFocusLast('remarks');
			}
		}

	} else if (w_version == 0) {
		setFocusLast('geoUnitStateCode');
	}
	return true;

}

function multiCurAccount_val() {
	var w_version = $('#w_version').val();
	if ($('#hasBranches').is(':checked')) {
		if (w_version > 0) {
			setCheckbox('multiCurAccount', YES);
			$('#multiCurAccount').prop('readOnly', true);
		} else if (w_version == 0) {
			setCheckbox('multiCurAccount', YES);
			$('#multiCurAccount').prop('readOnly', true);
		}
	} else {
		if (w_version > 0) {
			setCheckbox('multiCurAccount', NO);
			$('#multiCurAccount').prop('readOnly', true);
		} else if (w_version == 0) {
			setCheckbox('multiCurAccount', NO);
			$('#multiCurAccount').prop('readOnly', true);
		}

	}
	return true;
}

function geoUnitStateCode_val() {
	var value = $('#geoUnitStateCode').val();
	clearError('geoUnitStateCode_error');
	var w_version = $('#w_version').val();
	if (w_version > 0) {
		$('#geoUnitStateCode').prop('readOnly', true);
		$('#geoUnitStateCode_pic').prop('readOnly', true);
	} else if (w_version == 0) {
		$('#geoUnitStateCode').prop('readOnly', false);
		$('#geoUnitStateCode_pic').prop('readOnly', false);
		if (isEmpty(value)) {
			setError('geoUnitStateCode_error', MANDATORY);
			return false;
		}
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('GEO_UNIT_STRUC_CODE', $('#geoUnitStateCode').val());
		validator.setValue('W_VERSION', w_version);
		validator.setValue('GEO_UNIT_STRUC_REQ', w_add_struc_reqd);
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.cmn.mentitiesbean');
		validator.setMethod('validateGeoUnitStateCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('geoUnitStateCode_error', validator.getValue(ERROR));
			$('#geoUnitStateCode_desc').html(EMPTY_STRING);
			return false;
		}
		$('#geoUnitStateCode_desc').html(validator.getValue("DESCRIPTION"));
	}
	if (w_add_struc_reqd == COLUMN_ENABLE) {
		setFocusLast('geoUnitDistrictCode');
	} else {
		setFocusLast('remarks');
	}
	return true;
}

function geoUnitDistrictCode_val() {
	var value = $('#geoUnitDistrictCode').val();
	clearError('geoUnitDistrictCode_error');
	var w_version = $('#w_version').val();
	if (w_version > 0) {
		$('#geoUnitDistrictCode').prop('readOnly', true);
		$('#geoUnitDistrictCode_pic').prop('readOnly', true);
	} else if (w_version == 0) {
		if (w_add_struc_reqd == COLUMN_ENABLE) {
			$('#geoUnitDistrictCode').prop('readOnly', false);
			$('#geoUnitDistrictCode_pic').prop('readOnly', false);
			if (isEmpty(value)) {
				setError('geoUnitDistrictCode_error', MANDATORY);
				return false;
			}
			validator.clearMap();
			validator.setMtm(false);
			validator.setValue('W_CONTRY_STRUC', w_country_struc);
			validator.setValue('GEO_UNIT', $('#geoUnitDistrictCode').val());
			validator.setValue('GEO_UNIT_STRUC_REQ', w_add_struc_reqd);
			validator.setValue('W_VERSION', w_version);
			validator.setValue(ACTION, USAGE);
			validator.setClass('patterns.config.web.forms.cmn.mentitiesbean');
			validator.setMethod('validateGeoUnitDistrictCode');
			validator.sendAndReceive();
			if (validator.getValue(ERROR) != EMPTY_STRING) {
				setError('geoUnitDistrictCode_error', validator.getValue(ERROR));
				$('#geoUnitDistrictCode_desc').html(EMPTY_STRING);
				return false;
			}
			$('#geoUnitDistrictCode_desc').html(validator.getValue("DESCRIPTION"));
		} else {
			$('#geoUnitDistrictCode').val(EMPTY_STRING);
			$('#geoUnitDistrictCode_error').html(EMPTY_STRING);
			$('#geoUnitDistrictCode').prop('readOnly', true);
			$('#geoUnitDistrictCode_pic').prop('readOnly', true);
		}
	}
	setFocusLast('geoUnitPinCode');
	return true;
}

function geoUnitPinCode_val() {
	var value = $('#geoUnitPinCode').val();
	clearError('geoUnitPinCode_error');
	var w_version = $('#w_version').val();
	if (w_version > 0) {
		$('#geoUnitPinCode').prop('readOnly', true);
		$('#geoUnitPinCode_pic').prop('readOnly', true);
	} else if (w_version == 0) {
		if (w_add_struc_reqd == COLUMN_ENABLE) {
			$('#geoUnitPinCode').prop('readOnly', false);
			$('#geoUnitPinCode_pic').prop('readOnly', false);
			if (isEmpty(value)) {
				setError('geoUnitPinCode_error', MANDATORY);
				return false;
			}
			validator.clearMap();
			validator.setMtm(false);
			validator.setValue('W_CONTRY_STRUC', w_country_struc);
			validator.setValue('GEO_UNIT_STRUC', $('#geoUnitPinCode').val());
			validator.setValue('W_VERSION', w_version);
			validator.setValue('GEO_UNIT_STRUC_REQ', w_add_struc_reqd);
			validator.setValue(ACTION, USAGE);
			validator.setClass('patterns.config.web.forms.cmn.mentitiesbean');
			validator.setMethod('validateGeoUnitPinCode');
			validator.sendAndReceive();
			if (validator.getValue(ERROR) != EMPTY_STRING) {
				setError('geoUnitPinCode_error', validator.getValue(ERROR));
				$('#geoUnitPinCode_desc').html(EMPTY_STRING);
				return false;
			}
			$('#geoUnitPinCode_desc').html(validator.getValue("DESCRIPTION"));
		} else {
			$('#geoUnitPinCode').val(EMPTY_STRING);
			$('#geoUnitPinCode_error').html(EMPTY_STRING);
			$('#geoUnitPinCode').prop('readOnly', true);
			$('#geoUnitPinCode_pic').prop('readOnly', true);
		}
	}
	if (w_version == 0) {
		setFocusLast('remarks');
	} else {
		setFocusLast('entityDissolved');
	}
	return true;
}

function entityDissolved_val() {
	var w_version = $('#w_version').val();
	if (w_version == 0) {
		setCheckbox('entityDissolved', NO);
		$('#entityDissolved').prop('disabled', true);
		$('#dissolvedOn').val(EMPTY_STRING);
		$('#dissolvedOn_error').html(EMPTY_STRING);
		$('#dissolvedOn').prop('readOnly', true);
	} else if (w_version > 0) {
		$('#entityDissolved').prop('disabled', false);
		if ($('#entityDissolved').is(':checked')) {
			setCheckbox('entityDissolved', YES);
			$('#dissolvedOn').val(getCBD());
			$('#dissolvedOn').prop('readOnly', true);
		} else {
			$('#dissolvedOn').val(EMPTY_STRING);
			$('#dissolvedOn_error').html(EMPTY_STRING);
			$('#dissolvedOn').prop('readOnly', true);
		}
	}
	setFocusLast('remarks');
	return true;
}

function dissolvedOn_val() {
	setFocusLast('remarks');
	return true;
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	var w_version = $('#w_version').val();
	if (w_version == 0) {
		if (!isEmpty(value)) {
			if (!isValidRemarks(value)) {
				setError('remarks_error', INVALID_REMARKS);
				return false;
			}
		}
	}
	if (w_version > 0) {
		if (isEmpty(value)) {
			setError('remarks_error', MANDATORY);
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', INVALID_REMARKS);
			return false;
		}
	}
	setFocusOnSubmit();
	return true;
}

function revalidate() {
	$('#w_version').val(w_version);
	$('#w_add_struc_reqd').val(w_add_struc_reqd);
	$('#w_country_struc').val(w_country_struc);
	$('#hiddenhas').val($('#hasBranches').val());
	$('#fin').val($('#finYear').val());
	$('#weekStarts').val($('#weekStartsFrom').val());
	$('#multiCur').val($('#multiCurAccount').val());
}
