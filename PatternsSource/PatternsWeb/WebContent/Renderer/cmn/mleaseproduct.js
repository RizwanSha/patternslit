var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MLEASEPRODUCT';

function init() {
	refreshTBAGrid();
	refreshQueryGrid();
	if (_redisplay) {
		if ($('#action').val() == MODIFY) {
			$('#enabled').prop('disabled', false);
		} else {
			setCheckbox('enabled', YES);
			$('#enabled').prop('disabled', true);
		}
	}
}

function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'MLEASEPRODUCT_MAIN', queryGrid);
}

function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}
function doclearfields(id) {
	switch (id) {
	case 'leaseProductCode':
		if (isEmpty($('#leaseProductCode').val())) {
			clearFields();
			break;
		}
	}
}
function clearFields() {
	$('#leaseProductCode').val(EMPTY_STRING);
	$('#leaseProductCode_error').html(EMPTY_STRING);
	$('#leaseProductCode_desc').html(EMPTY_STRING);
	clearNonPKFields();
}

function clearNonPKFields() {
	$('#leaseProductDescription').val(EMPTY_STRING);
	$('#leaseProductDescription_error').html(EMPTY_STRING);
	$('#leaseProductConciseDescription').val(EMPTY_STRING);
	$('#leaseProductConciseDescription_error').html(EMPTY_STRING);
	$('#leaseType').prop('selectedIndex', 0);
	$('#leaseType_error').html(EMPTY_STRING);
	$('#baseCurrCode').val(EMPTY_STRING);
	$('#baseCurrCode_error').html(EMPTY_STRING);
	$('#baseCurrCode_desc').html(EMPTY_STRING);
	$('#sundryDebtorsHead').val(EMPTY_STRING);
	$('#sundryDebtorsHead_error').html(EMPTY_STRING);
	$('#sundryDebtorsHead_desc').html(EMPTY_STRING);
	$('#rentReceivablePrincipal').val(EMPTY_STRING);
	$('#rentReceivablePrincipal_error').html(EMPTY_STRING);
	$('#rentReceivablePrincipal_desc').html(EMPTY_STRING);
	$('#rentReceivableInterest').val(EMPTY_STRING);
	$('#rentReceivableInterest_error').html(EMPTY_STRING);
	$('#rentReceivableInterest_desc').html(EMPTY_STRING);
	$('#unearnedIncomeHead').val(EMPTY_STRING);
	$('#unearnedIncomeHead_error').html(EMPTY_STRING);
	$('#unearnedIncomeHead_desc').html(EMPTY_STRING);
	setCheckbox('enabled', YES);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
}

function loadData() {
	$('#leaseProductCode').val(validator.getValue('LEASE_PRODUCT_CODE'));
	$('#leaseProductCode_desc').html(validator.getValue('DESCRIPTION'));
	$('#leaseProductDescription').val(validator.getValue('DESCRIPTION'));
	$('#leaseProductConciseDescription').val(validator.getValue('CONCISE_DESCRIPTION'));
	$('#leaseType').val(validator.getValue('LEASE_TYPE'));
	$('#baseCurrCode').val(validator.getValue('LEASE_ASSET_IN_CCY'));
	$('#baseCurrCode_desc').html(validator.getValue('F5_DESCRIPTION'));
	$('#sundryDebtorsHead').val(validator.getValue('SUNDRY_DEBTOR_GL_HEAD_CODE'));
	$('#sundryDebtorsHead_desc').html(validator.getValue('F1_DESCRIPTION'));
	$('#rentReceivablePrincipal').val(validator.getValue('RENT_RECV_PRIN_GL_HEAD_CODE'));
	$('#rentReceivablePrincipal_desc').html(validator.getValue('F2_DESCRIPTION'));
	$('#rentReceivableInterest').val(validator.getValue('RENT_RECV_INT_GL_HEAD_CODE'));
	$('#rentReceivableInterest_desc').html(validator.getValue('F3_DESCRIPTION'));
	$('#unearnedIncomeHead').val(validator.getValue('UNEARNED_INCOME_GL_HEAD_CODE'));
	$('#unearnedIncomeHead_desc').html(validator.getValue('F4_DESCRIPTION'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	resetLoading();
}

function view(source, primaryKey) {
	hideParent('cmn/qleaseproduct', source, primaryKey);
	resetLoading();
}

function doHelp(id) {
	switch (id) {
	case 'leaseProductCode':
		help('COMMON', 'HLP_LEASE_PROD_CODE', $('#leaseProductCode').val(), EMPTY_STRING, $('#leaseProductCode'));
		break;
	case 'baseCurrCode':
		help('COMMON', 'HLP_CURRENCY_CODE', $('#baseCurrCode').val(), EMPTY_STRING, $('#baseCurrCode'));
		break;
	case 'sundryDebtorsHead':
		help('COMMON', 'HLP_GL_HEAD_CODE', $('#sundryDebtorsHead').val(), $('#leaseProductCode').val(), $('#sundryDebtorsHead'));
		break;
	case 'rentReceivablePrincipal':
		help('COMMON', 'HLP_GL_HEAD_CODE', $('#rentReceivablePrincipal').val(), $('#leaseProductCode').val(), $('#rentReceivablePrincipal'));
		break;
	case 'rentReceivableInterest':
		help('COMMON', 'HLP_GL_HEAD_CODE', $('#rentReceivableInterest').val(), $('#leaseProductCode').val(), $('#rentReceivableInterest'));
		break;
	case 'unearnedIncomeHead':
		help('COMMON', 'HLP_GL_HEAD_CODE', $('#unearnedIncomeHead').val(), $('#leaseProductCode').val(), $('#unearnedIncomeHead'));
		break;
	}
}

function add() {
	$('#leaseProductCode').focus();
	setCheckbox('enabled', YES);
	$('#enabled').prop('disabled', true);
}

function modify() {
	$('#leaseProductCode').focus();
	if ($('#action').val() == MODIFY) {
		$('#enabled').prop('disabled', false);
	} else {
		$('#enabled').prop('disabled', true);
	}
}

function validate(id) {
	switch (id) {
	case 'leaseProductCode':
		leaseProductCode_val();
		break;
	case 'leaseProductDescription':
		leaseProductDescription_val();
		break;
	case 'leaseProductConciseDescription':
		leaseProductConciseDescription_val();
		break;
	case 'leaseType':
		leaseType_val();
		break;
	case 'baseCurrCode':
		baseCurrCode_val();
		break;
	case 'sundryDebtorsHead':
		sundryDebtorsHead_val();
		break;
	case 'rentReceivablePrincipal':
		rentReceivablePrincipal_val();
		break;
	case 'rentReceivableInterest':
		rentReceivableInterest_val();
		break;
	case 'unearnedIncomeHead':
		unearnedIncomeHead_val();
		break;
	case 'enabled':
		enabled_val();
		break;
	case 'remarks':
		remarks_val();
		break;
	}
}

function backtrack(id) {
	switch (id) {
	case 'leaseProductCode':
		setFocusLast('leaseProductCode');
		break;
	case 'leaseProductDescription':
		setFocusLast('leaseProductCode');
		break;
	case 'leaseProductConciseDescription':
		setFocusLast('leaseProductDescription');
		break;
	case 'leaseType':
		setFocusLast('leaseProductConciseDescription');
		break;
	case 'baseCurrCode':
		setFocusLast('leaseType');
		break;
	case 'sundryDebtorsHead':
		setFocusLast('baseCurrCode');
		break;
	case 'rentReceivablePrincipal':
		setFocusLast('sundryDebtorsHead');
		break;
	case 'rentReceivableInterest':
		setFocusLast('rentReceivablePrincipal');
		break;
	case 'unearnedIncomeHead':
		setFocusLast('rentReceivableInterest');
		break;
	case 'enabled':
		setFocusLast('unearnedIncomeHead');
		break;
	case 'remarks':
		if ($('#action').val() == MODIFY) {
			setFocusLast('enabled');
		} else {
			setFocusLast('unearnedIncomeHead');
		}
		break;
	}
}

function leaseProductCode_val() {
	var value = $('#leaseProductCode').val();
	clearError('leaseProductCode_error');
	$('#leaseProductCode_desc').html(EMPTY_STRING);
	if (isEmpty(value)) {
		setError('leaseProductCode_error', MANDATORY);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('LEASE_PRODUCT_CODE', $('#leaseProductCode').val());
		validator.setValue(ACTION, $('#action').val());
		validator.setClass('patterns.config.web.forms.cmn.mleaseproductbean');
		validator.setMethod('validateLeaseProductCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('leaseProductCode_error', validator.getValue(ERROR));
			return false;
		} else {
			if ($('#action').val() == MODIFY) {
				PK_VALUE = getEntityCode() + PK_SEPERATOR + $('#leaseProductCode').val();
				if (!loadPKValues(PK_VALUE, 'leaseProductCode_error')) {
					return false;
				}
			}
		}
	}
	setFocusLast('leaseProductDescription');
	return true;
}

function leaseProductDescription_val() {
	var value = $('#leaseProductDescription').val();
	clearError('leaseProductDescription_error');
	if (isEmpty(value)) {
		setError('leaseProductDescription_error', MANDATORY);
		return false;
	}
	if (!isValidDescription(value)) {
		setError('leaseProductDescription_error', INVALID_DESCRIPTION);
		return false;
	}
	setFocusLast('leaseProductConciseDescription');
	return true;
}

function leaseProductConciseDescription_val() {
	var value = $('#leaseProductConciseDescription').val();
	clearError('leaseProductConciseDescription_error');
	if (isEmpty(value)) {
		setError('leaseProductConciseDescription_error', MANDATORY);
		return false;
	}
	if (!isValidConciseDescription(value)) {
		setError('leaseProductConciseDescription_error', INVALID_CONCISE_DESCRIPTION);
		return false;
	}
	setFocusLast('leaseType');
	return true;
}

function leaseType_val() {
	var value = $('#leaseType').val();
	clearError('leaseType_error');
	if (isEmpty(value)) {
		setError('leaseType_error', MANDATORY);
		return false;
	}
	setFocusLast('baseCurrCode');
	return true;
}

function baseCurrCode_val() {
	var value = $('#baseCurrCode').val();
	clearError('baseCurrCode_error');
	$('#baseCurrCode_desc').html(EMPTY_STRING);
	if (isEmpty(value)) {
		setError('baseCurrCode_error', MANDATORY);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('CCY_CODE', $('#baseCurrCode').val());
	validator.setValue(ACTION, USAGE);
	validator.setClass('patterns.config.web.forms.cmn.mleaseproductbean');
	validator.setMethod('validateBaseCurrCode');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('baseCurrCode_error', validator.getValue(ERROR));
		$('#baseCurrCode_desc').html(EMPTY_STRING);
		return false;
	} else {
		$('#baseCurrCode_desc').html(validator.getValue("DESCRIPTION"));
	}
	setFocusLast('sundryDebtorsHead');
	return true;
}

function sundryDebtorsHead_val() {
	var value = $('#sundryDebtorsHead').val();
	clearError('sundryDebtorsHead_error');
	$('#sundryDebtorsHead_desc').html(EMPTY_STRING);
	if (isEmpty(value)) {
		setError('sundryDebtorsHead_error', MANDATORY);
		return false;
	} else if (!isValidCode(value)) {
		setError('sundryDebtorsHead_error', INVALID_FORMAT);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('LEASE_PRODUCT_CODE', $('#leaseProductCode').val());
		validator.setValue('GL_HEAD_CODE', $('#sundryDebtorsHead').val());
		validator.setValue(ACTION, $('#action').val());
		validator.setClass('patterns.config.web.forms.cmn.mleaseproductbean');
		validator.setMethod('validateSundryDebtorsHead');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('sundryDebtorsHead_error', validator.getValue(ERROR));
			return false;
		}
		$('#sundryDebtorsHead_desc').html(validator.getValue('DESCRIPTION'));
	}
	setFocusLast('rentReceivablePrincipal');
	return true;
}
function rentReceivablePrincipal_val() {
	var value = $('#rentReceivablePrincipal').val();
	clearError('rentReceivablePrincipal_error');
	$('#rentReceivablePrincipal_desc').html(EMPTY_STRING);
	if (!isEmpty(value)) {
		if (!isValidCode(value)) {
			setError('rentReceivablePrincipal_error', INVALID_FORMAT);
			return false;
		}
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('LEASE_PRODUCT_CODE', $('#leaseProductCode').val());
		validator.setValue('GL_HEAD_CODE', $('#rentReceivablePrincipal').val());
		validator.setValue(ACTION, $('#action').val());
		validator.setClass('patterns.config.web.forms.cmn.mleaseproductbean');
		validator.setMethod('validateGlHeadCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('rentReceivablePrincipal_error', validator.getValue(ERROR));
			return false;
		}
		$('#rentReceivablePrincipal_desc').html(validator.getValue('DESCRIPTION'));
	}
	setFocusLast('rentReceivableInterest');
	return true;
}

function rentReceivableInterest_val() {
	var value = $('#rentReceivableInterest').val();
	clearError('rentReceivableInterest_error');
	$('#rentReceivableInterest_desc').html(EMPTY_STRING);
	if (!isEmpty(value)) {
		if (!isValidCode(value)) {
			setError('rentReceivableInterest_error', INVALID_FORMAT);
			return false;
		}
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('LEASE_PRODUCT_CODE', $('#leaseProductCode').val());
		validator.setValue('GL_HEAD_CODE', $('#rentReceivableInterest').val());
		validator.setValue(ACTION, $('#action').val());
		validator.setClass('patterns.config.web.forms.cmn.mleaseproductbean');
		validator.setMethod('validateGlHeadCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('rentReceivableInterest_error', validator.getValue(ERROR));
			return false;
		}
		$('#rentReceivableInterest_desc').html(validator.getValue('DESCRIPTION'));
	}
	setFocusLast('unearnedIncomeHead');
	return true;
}
function unearnedIncomeHead_val() {
	var value = $('#unearnedIncomeHead').val();
	clearError('unearnedIncomeHead_error');
	$('#unearnedIncomeHead_desc').html(EMPTY_STRING);
	if (!isEmpty(value)) {
		if (!isValidCode(value)) {
			setError('unearnedIncomeHead_error', INVALID_FORMAT);
			return false;
		}
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('LEASE_PRODUCT_CODE', $('#leaseProductCode').val());
		validator.setValue('GL_HEAD_CODE', $('#unearnedIncomeHead').val());
		validator.setValue(ACTION, $('#action').val());
		validator.setClass('patterns.config.web.forms.cmn.mleaseproductbean');
		validator.setMethod('validateGlHeadCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('unearnedIncomeHead_error', validator.getValue(ERROR));
			return false;
		}
		$('#unearnedIncomeHead_desc').html(validator.getValue('DESCRIPTION'));
	}
	if ($('#action').val() == ADD) {
		setFocusLast('remarks');
	} else {
		setFocusLast('enabled');
	}
	return true;
}

function enabled_val() {
	setFocusLast('remarks');
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', INVALID_REMARKS);
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', INVALID_REMARKS);
			return false;
		}
	}
	if ($('#action').val() == MODIFY) {
		if (isEmpty(value)) {
			setError('remarks_error', MANDATORY);
			return false;
		}
	}
	setFocusOnSubmit();
	return true;
}
