var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MORGANIZATION';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}
function clearFields() {
	$('#organizationCode').val(EMPTY_STRING);
	$('#description').val(EMPTY_STRING);
	$('#conciseDescription').val(EMPTY_STRING);
	$('#contactName_title').val(EMPTY_STRING);
	$('#contactName').val(EMPTY_STRING);
	$('#emailID').val(EMPTY_STRING);
	setCheckbox('enabled', NO);
	$('#remarks').val(EMPTY_STRING);

}
function loadData() {
	$('#organizationCode').val(validator.getValue('ORG_CODE'));
	$('#description').val(validator.getValue('DESCRIPTION'));
	$('#conciseDescription').val(validator.getValue('CONCISE_DESCRIPTION'));
	$('#contactName_title').val(validator.getValue('CONTACT_PERSON_TITLE'));
	$('#contactName').val(validator.getValue('CONTACT_PERSON_NAME'));
	$('#emailID').val(validator.getValue('CONTACT_EMAIL_ID'));
	$('#address').val(validator.getValue('ADDRESS'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadAuditFields(validator);
}
