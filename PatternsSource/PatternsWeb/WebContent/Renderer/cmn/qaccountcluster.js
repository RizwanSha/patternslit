var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MACCOUNTCLUSTER';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}

function clearFields() {
	$('#accountClusterCode').val(EMPTY_STRING);
	$('#accountClusterCode_desc').html(EMPTY_STRING);
	$('#description').val(EMPTY_STRING);
	$('#conciseDescription').val(EMPTY_STRING);
	$('#accountClusterAlphaCode').val(EMPTY_STRING);
	$('#clusterStateCode').val(EMPTY_STRING);
	$('#clusterStateCode_desc').html(EMPTY_STRING);
	setCheckbox('enabled', NO);
	$('#remarks').val(EMPTY_STRING);
}

function loadData() {
	$('#accountClusterCode').val(validator.getValue('ACNT_CLUSTER_CODE'));
	$('#description').val(validator.getValue('DESCRIPTION'));
	$('#conciseDescription').val(validator.getValue('CONCISE_DESCRIPTION'));
	$('#accountClusterAlphaCode').val(validator.getValue('CLUSTER_ALPHA_CODE'));
	$('#clusterStateCode').val(validator.getValue('CLUSTER_STATE_CODE'));
	$('#clusterStateCode_desc').html(validator.getValue('F1_DESCRIPTION'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadAuditFields(validator);
}
