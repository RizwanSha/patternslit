var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MSCHEME';
function init() {
	refreshTBAGrid();
	refreshQueryGrid();
	if (_redisplay) {
		if ($('#action').val() == ADD) {
			$('#enabled').prop('disabled', true);
			setCheckbox('enabled', YES);
		}
		if (isEmpty($('#paymentConrolTokenRef_error').html().trim())) {
			if ($('#schemeFor').val() == 'P')
				$('#paymentConrolTokenRef').prop('disabled', false);
			else
				$('#paymentConrolTokenRef').prop('disabled', true);
		}
		if (isEmpty($('#usingPidType_error').html().trim()) && isEmpty($('#nameOfIdEnrolNo_error').html().trim())) {
			schemeHasEnrol_val(false);
		}
		if (isEmpty($('#nameOfTokenRef_error').html().trim()) && isEmpty($('#paymentConrolTokenRef_error').html().trim())) {
			paymentConrolTokenRef_val(false);
		}

	}
}

function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'MSCHEME_MAIN', queryGrid);
}

function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}

function doclearfields(id) {
	switch (id) {
	case 'schemeCode':
		if (isEmpty($('#schemeCode').val())) {
			$('#schemeCode_error').html(EMPTY_STRING);
			$('#schemeCode_desc').html(EMPTY_STRING);
			clearNonPKFields();
			break;
		}
	}
}

function clearFields() {
	$('#schemeCode').val(EMPTY_STRING);
	$('#schemeCode_error').html(EMPTY_STRING);
	$('#schemeCode_desc').html(EMPTY_STRING);
	clearNonPKFields();
}
function clearNonPKFields() {
	$('#description').val(EMPTY_STRING);
	$('#description_error').html(EMPTY_STRING);
	$('#conciseDescription').val(EMPTY_STRING);
	$('#conciseDescription_error').html(EMPTY_STRING);
	$('#schemeFor').val($("#schemeFor option:first-child").val());
	$('#schemeFor_error').html(EMPTY_STRING);
	$('#parentSchemeCode').val(EMPTY_STRING);
	$('#parentSchemeCode_error').html(EMPTY_STRING);
	$('#parentSchemeCode_desc').html(EMPTY_STRING);
	setCheckbox('schemeHasEnrol', NO);
	$('#usingPidType').val(EMPTY_STRING);
	$('#usingPidType_error').html(EMPTY_STRING);
	$('#usingPidType_desc').html(EMPTY_STRING);
	$('#usingPidType').prop('readonly', true);
	$('#usingPidType_pic').prop('disabled', true);
	$('#nameOfIdEnrolNo').val(EMPTY_STRING);
	$('#nameOfIdEnrolNo_error').html(EMPTY_STRING);
	$('#nameOfIdEnrolNo').prop('readonly', true);
	setCheckbox('paymentConrolTokenRef', NO);
	$('#paymentConrolTokenRef').prop('disabled', true);
	clearError('paymentConrolTokenRef_error');
	$('#nameOfTokenRef').val(EMPTY_STRING);
	$('#nameOfTokenRef_error').html(EMPTY_STRING);
	$('#nameOfTokenRef').prop('readonly', true);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
}

function doHelp(id) {
	switch (id) {
	case 'schemeCode':
		help('COMMON', 'HLP_SCHEME_CODE_M', $('#schemeCode').val(), EMPTY_STRING, $('#schemeCode'));
		break;
	case 'usingPidType':
		help('COMMON', 'HLP_PIDCODE', $('#usingPidType').val(), EMPTY_STRING, $('#usingPidType'));
		break;
	case 'parentSchemeCode':
		if (!isEmpty($('#schemeCode').val()))
			help(CURRENT_PROGRAM_ID, 'HLP_SCHEME_CODE', $('#parentSchemeCode').val(), $('#schemeCode').val(), $('#parentSchemeCode'));
		else {
			setError('schemeCode_error', MANDATORY);
			return false;
		}
		break;
	}
}

function add() {
	setCheckbox('enabled', YES);
	$('#enabled').prop('disabled', true);
	$('#schemeCode_pic').prop('disabled', true);
	$('#schemeCode').focus();
}

function modify() {
	if ($('#action').val() == MODIFY) {
		$('#enabled').prop('disabled', false);
		$('#schemeCode_pic').prop('disabled', false);
	} else {
		$('#enabled').prop('disabled', true);
		$('#schemeCode_pic').prop('disabled', true);
	}
	$('#schemeCode').focus();
}

function loadData() {
	$('#schemeCode').val(validator.getValue('SCHEME_CODE'));
	$('#description').val(validator.getValue('DESCRIPTION'));
	$('#conciseDescription').val(validator.getValue('CONCISE_DESCRIPTION'));
	$('#schemeFor').val(validator.getValue('SCHEME_FOR'));
	$('#parentSchemeCode').val(validator.getValue('PARENT_SCH_CODE'));
	$('#parentSchemeCode_desc').html(validator.getValue('F2_DESCRIPTION'));
	setCheckbox('schemeHasEnrol', validator.getValue('SCHEME_HAS_ENROL'));
	$('#usingPidType').val(validator.getValue('PID_CODE'));
	$('#usingPidType_desc').html(validator.getValue('F1_DESCRIPTION'));
	$('#nameOfIdEnrolNo').val(validator.getValue('ENROL_DESCRIPTION'));
	setCheckbox('paymentConrolTokenRef', validator.getValue('PAY_CONTROL_TOKEN_REF'));
	$('#nameOfTokenRef').val(validator.getValue('TOKEN_REF_DESCRIPTION'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	if ($('#schemeFor').val() == 'P')
		$('#paymentConrolTokenRef').prop('disabled', false);
	else
		$('#paymentConrolTokenRef').prop('disabled', true);
	schemeHasEnrol_val(false);
	paymentConrolTokenRef_val(false);
	resetLoading();
}

function view(source, primaryKey) {
	hideParent('cmn/qscheme', source, primaryKey);
	resetLoading();
}

function validate(id) {
	switch (id) {
	case 'schemeCode':
		schemeCode_val();
		break;
	case 'description':
		description_val();
		break;
	case 'conciseDescription':
		conciseDescription_val();
		break;
	case 'schemeFor':
		schemeFor_val(true);
		break;
	case 'parentSchemeCode':
		parentSchemeCode_val();
		break;
	case 'schemeHasEnrol':
		schemeHasEnrol_val(true);
		break;
	case 'usingPidType':
		usingPidType_val();
		break;
	case 'nameOfIdEnrolNo':
		nameOfIdEnrolNo_val();
		break;
	case 'paymentConrolTokenRef':
		paymentConrolTokenRef_val(true);
		break;
	case 'nameOfTokenRef':
		nameOfTokenRef_val();
		break;
	case 'enabled':
		enabled_val();
		break;
	case 'remarks':
		remarks_val();
		break;
	}
}

function backtrack(id) {
	switch (id) {
	case 'schemeCode':
		setFocusLast('schemeCode');
		break;
	case 'description':
		setFocusLast('schemeCode');
		break;
	case 'conciseDescription':
		setFocusLast('description');
		break;
	case 'schemeFor':
		setFocusLast('conciseDescription');
		break;
	case 'parentSchemeCode':
		setFocusLast('schemeFor');
		break;
	case 'schemeHasEnrol':
		if (!isEmpty($('#parentSchemeCode').val()))
			setFocusLast('parentSchemeCode');
		else
			setFocusLast('schemeFor');
		break;
	case 'usingPidType':
		setFocusLast('schemeHasEnrol');
		break;
	case 'nameOfIdEnrolNo':
		setFocusLast('usingPidType');
		break;
	case 'paymentConrolTokenRef':
		if ($('#schemeHasEnrol').prop('checked'))
			setFocusLast('nameOfIdEnrolNo');
		else
			setFocusLast('schemeHasEnrol');
		break;
	case 'nameOfTokenRef':
		setFocusLast('paymentConrolTokenRef');
		break;
	case 'enabled':
		if ($('#paymentConrolTokenRef').prop('checked'))
			setFocusLast('nameOfTokenRef');
		else {
			if ($('#schemeFor').val() == 'P')
				setFocusLast('paymentConrolTokenRef');
			else {
				if ($('#schemeHasEnrol').prop('checked'))
					setFocusLast('nameOfIdEnrolNo');
				else
					setFocusLast('schemeHasEnrol');
			}
		}
		break;
	case 'remarks':
		if ($('#action').val() == MODIFY) {
			setFocusLast('enabled');
		} else {
			if ($('#paymentConrolTokenRef').prop('checked'))
				setFocusLast('nameOfTokenRef');
			else {
				if ($('#schemeFor').val() == 'P')
					setFocusLast('paymentConrolTokenRef');
				else {
					if ($('#schemeHasEnrol').prop('checked'))
						setFocusLast('nameOfIdEnrolNo');
					else
						setFocusLast('schemeHasEnrol');
				}
			}
		}
		break;
	}
}

function change(id) {
	switch (id) {
	case 'schemeFor':
		schemeFor_val(false);
		break;
	}
}

function checkclick(id) {
	switch (id) {
	case 'schemeHasEnrol':
		schemeHasEnrol_val(false);
		break;
	case 'paymentConrolTokenRef':
		paymentConrolTokenRef_val(false);
		break;
	}
}

function schemeCode_val() {
	var value = $('#schemeCode').val();
	clearError('schemeCode_error');
	if (isEmpty(value)) {
		setError('schemeCode_error', MANDATORY);
		return false;
	}
	if ($('#action').val() == ADD && !isValidCode(value)) {
		setError('schemeCode_error', INVALID_FORMAT);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('SCHEME_CODE', $('#schemeCode').val());
	validator.setValue(ACTION, $('#action').val());
	validator.setClass('patterns.config.web.forms.cmn.mschemebean');
	validator.setMethod('validateSchemeCode');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('schemeCode_error', validator.getValue(ERROR));
		$('#schemeCode_desc').html(EMPTY_STRING);
		return false;
	}
	if ($('#action').val() == MODIFY) {
		PK_VALUE = getEntityCode() + PK_SEPERATOR + $('#schemeCode').val();
		if (!loadPKValues(PK_VALUE, 'schemeCode_error')) {
			return false;
		}
	}
	setFocusLast('description');
	return true;
}

function description_val() {
	var value = $('#description').val();
	clearError('description_error');
	if (isEmpty(value)) {
		setError('description_error', MANDATORY);
		return false;
	}
	if (!isValidDescription(value)) {
		setError('description_error', INVALID_DESCRIPTION);
		return false;
	}
	setFocusLast('conciseDescription');
	return true;
}

function conciseDescription_val() {
	var value = $('#conciseDescription').val();
	clearError('conciseDescription_error');
	if (isEmpty(value)) {
		setError('conciseDescription_error', MANDATORY);
		return false;
	}
	if (!isValidConciseDescription(value)) {
		setError('conciseDescription_error', INVALID_CONCISE_DESCRIPTION);
		return false;
	}
	setFocusLast('schemeFor');
	return true;
}

function schemeFor_val(valMode) {
	var value = $('#schemeFor').val();
	clearError('schemeFor_error');
	if (isEmpty(value)) {
		setError('schemeFor_error', MANDATORY);
		return false;
	}
	if ($('#schemeFor').val() == 'P') {
		$('#paymentConrolTokenRef').prop('disabled', false);
	} else {
		setCheckbox('paymentConrolTokenRef', ZERO);
		$('#paymentConrolTokenRef').prop('disabled', true);
		$('#nameOfTokenRef').val(EMPTY_STRING);
		$('#nameOfTokenRef_error').html(EMPTY_STRING);
		$('#nameOfTokenRef').prop('readonly', true);
	}
	if (valMode)
		setFocusLast('parentSchemeCode');
	return true;
}

function parentSchemeCode_val() {
	var schCode = $('#schemeCode').val();
	var value = $('#parentSchemeCode').val();
	clearError('parentSchemeCode_error');
	if (!isEmpty(value)) {
		if (value == schCode) {
			setError('parentSchemeCode_error', PBS_SCHEME_CODE_NOT_EQ_PARENT_CODE);
			return false;
		}
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('SCHEME_CODE', $('#schemeCode').val());
		validator.setValue('PARENT_SCH_CODE', $('#parentSchemeCode').val());
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.cmn.mschemebean');
		validator.setMethod('validateParentSchemeCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('parentSchemeCode_error', validator.getValue(ERROR));
			$('#parentSchemeCode_desc').html(EMPTY_STRING);
			return false;
		} else
			$('#parentSchemeCode_desc').html(validator.getValue("DESCRIPTION"));
	}
	setFocusLast('schemeHasEnrol');
	return true;
}

function schemeHasEnrol_val(valMode) {
	if ($('#schemeHasEnrol').prop('checked')) {
		$('#usingPidType').prop('readonly', false);
		$('#usingPidType_pic').prop('disabled', false);
		$('#nameOfIdEnrolNo').prop('readonly', false);
		if (valMode)
			setFocusLast('usingPidType');
	} else {
		$('#usingPidType').val(EMPTY_STRING);
		$('#usingPidType_error').html(EMPTY_STRING);
		$('#usingPidType_desc').html(EMPTY_STRING);
		$('#usingPidType').prop('readonly', true);
		$('#usingPidType_pic').prop('disabled', true);
		$('#nameOfIdEnrolNo').val(EMPTY_STRING);
		$('#nameOfIdEnrolNo_error').html(EMPTY_STRING);
		$('#nameOfIdEnrolNo').prop('readonly', true);
		if (valMode) {
			if ($('#schemeFor').val() == 'P') {
				setFocusLast('paymentConrolTokenRef');
			} else {
				if ($('#action').val() == ADD) {
					setFocusLast('remarks');
				} else {
					setFocusLast('enabled');
				}
			}
		}
	}
	return true;
}

function usingPidType_val() {
	var value = $('#usingPidType').val();
	clearError('usingPidType_error');
	if ($('#schemeHasEnrol').prop('checked')) {
		if (!isEmpty(value)) {
			validator.clearMap();
			validator.setMtm(false);
			validator.setValue('PID_CODE', value);
			validator.setValue(ACTION, USAGE);
			validator.setClass('patterns.config.web.forms.cmn.mschemebean');
			validator.setMethod('validatePidCode');
			validator.sendAndReceive();
			if (validator.getValue(ERROR) != EMPTY_STRING) {
				setError('usingPidType_error', validator.getValue(ERROR));
				$('#usingPidType_desc').html(EMPTY_STRING);
				return false;
			}
			$('#usingPidType_desc').html(validator.getValue("DESCRIPTION"));
		}
	}
	setFocusLast('nameOfIdEnrolNo');
	return true;
}

function nameOfIdEnrolNo_val() {
	var value = $('#nameOfIdEnrolNo').val();
	clearError('nameOfIdEnrolNo_error');
	if ($('#schemeHasEnrol').prop('checked')) {
		if (isEmpty(value)) {
			setError('nameOfIdEnrolNo_error', MANDATORY);
			return false;
		}
		if (!isValidOtherInformation50(value)) {
			setError('nameOfIdEnrolNo_error', MAXVAL_50);
			return false;
		}
	}
	if ($('#schemeFor').val() == 'P')
		setFocusLast('paymentConrolTokenRef');
	else {
		if ($('#action').val() == ADD) {
			setFocusLast('remarks');
		} else {
			setFocusLast('enabled');
		}
	}

	return true;
}

function paymentConrolTokenRef_val(valMode) {
	clearError('paymentConrolTokenRef_error');
	if ($('#paymentConrolTokenRef').prop('checked')) {
		$('#nameOfTokenRef').prop('readonly', false);
		if (valMode)
			setFocusLast('nameOfTokenRef');
	} else {
		$('#nameOfTokenRef').val(EMPTY_STRING);
		$('#nameOfTokenRef_error').html(EMPTY_STRING);
		$('#nameOfTokenRef').prop('readonly', true);
		if (valMode) {
			if ($('#action').val() == ADD) {
				setFocusLast('remarks');
			} else {
				setFocusLast('enabled');
			}
		}
	}
	return true;
}

function nameOfTokenRef_val() {
	var value = $('#nameOfTokenRef').val();
	clearError('nameOfTokenRef_error');
	if ($('#paymentConrolTokenRef').prop('checked')) {
		if (isEmpty(value)) {
			setError('nameOfTokenRef_error', MANDATORY);
			return false;
		}
		if (!isValidOtherInformation50(value)) {
			setError('nameOfTokenRef_error', MAXVAL_50);
			return false;
		}
	}
	if ($('#action').val() == ADD) {
		setFocusLast('remarks');
	} else {
		setFocusLast('enabled');
	}
	return true;
}

function enabled_val() {
	setFocusLast('remarks');
	return true;
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', INVALID_REMARKS);
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', INVALID_REMARKS);
			return false;
		}
	}
	if ($('#action').val() == MODIFY) {
		if (isEmpty(value)) {
			setError('remarks_error', MANDATORY);
			return false;
		}
	}
	setFocusOnSubmit();
	return true;
}
