var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MIFSC';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}

function clearFields() {
	$('#ifscCode').val(EMPTY_STRING);
	clearNonPKFields();
}
function clearNonPKFields() {
	$('#bankName').val(EMPTY_STRING);
	$('#branchName').val(EMPTY_STRING);
	$('#address').val(EMPTY_STRING);
	$('#micr').val(EMPTY_STRING);
	$('#city').val(EMPTY_STRING);
	$('#stateCode').val(EMPTY_STRING);
	$('#stateCode_desc').html(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
}
function loadData() {
	$('#ifscCode').val(validator.getValue('IFSC_CODE'));
	$('#bankName').val(validator.getValue('BANK_NAME'));
	$('#branchName').val(validator.getValue('BRANCH_NAME'));
	$('#address').val(validator.getValue('ADDRESS'));
	$('#micr').val(validator.getValue('MICR_CODE'));
	$('#city').val(validator.getValue('CITY'));
	$('#stateCode').val(validator.getValue('STATE_CODE'));
	$('#stateCode_desc').html(validator.getValue('F1_DESCRIPTION'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadAuditFields(validator);
	resetLoading();
}
