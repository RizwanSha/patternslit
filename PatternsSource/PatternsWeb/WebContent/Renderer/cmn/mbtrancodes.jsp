<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.cmn.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/cmn/mbtrancodes.js" />
	</web:dependencies>
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="mbtrancodes.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/cmn/mbtrancodes" id="mbtrancodes" method="POST">
				<web:dividerBlock>
					<type:optionBox add="true" modify="true" view="true" />
				</web:dividerBlock>
				<web:viewContent id="po_view1" styleClass="hidden">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="mbtrancodes.bankingtranCode" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:bankingTranCode property="bankingTranCode" id="bankingTranCode" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="form.description" var="common" mandatory="true" />
										</web:column>
										<web:column>
											<type:description property="description" id="description" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="form.conciseDescription" var="common" mandatory="true" />
										</web:column>
										<web:column>
											<type:conciseDescription property="conciseDescription" id="conciseDescription" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="mbtrancodes.bankingtranacccode" var="program" />
										</web:column>
										<web:column>
											<type:bankingTranAccessCode property="bankingTranAccCode" id="bankingTranAccCode" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="mbtrancodes.localcurrtranflag" var="program" />
										</web:column>
										<web:column>
											<type:checkbox property="localCurrTranFlag" id="localCurrTranFlag" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:viewTitle var="program" key="mbtrancodes.section" />
									<web:rowOdd>
										<web:column>
											<web:legend key="mbtrancodes.transactioncode" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:tranCode property="transactionCode" id="transactionCode" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column span="2">
											<web:gridToolbar gridName="trancodes_innerGrid" cancelFunction="trancodes_cancelGrid" editFunction="trancodes_editGrid" addFunction="trancodes_addGrid" continuousEdit="true" />
											<web:element property="xmlTranCodesGrid" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:viewContent id="po_view4">
												<web:grid height="240px" width="745px" id="trancodes_innerGrid" src="cmn/mbtrancodes_innerGrid.xml">
												</web:grid>
											</web:viewContent>
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="form.IsUseEnabled" var="common" />
										</web:column>
										<web:column>
											<type:checkbox property="enabled" id="enabled" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="form.remarks" var="common" />
										</web:column>
										<web:column>
											<type:remarks property="remarks" id="remarks" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:submitReset />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
						</web:sectionBlock>
					</web:dividerBlock>
				</web:viewContent>
				<web:element property="command" />
				<web:element property="action" />
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
	<web:queryGrid />
	<web:TBAGrid />
	<web:contextSearch />
</web:fragment>
