var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'ITAXBYUSPRODUCT';
var _oldRoleType = EMPTY_STRING;

function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}

function clearFields() {
	$('#taxCode').val(EMPTY_STRING);
	$('#taxCode_desc').html(EMPTY_STRING);
	clearNonPKFields();

}
function clearNonPKFields() {
	$('#currencyCode').val(EMPTY_STRING);
	$('#currencyCode_desc').html(EMPTY_STRING);
	$('#effectiveDate').val(EMPTY_STRING);
	$('#cutOffAmount').val(EMPTY_STRING);
	$('#cutOffAmount_curr').val(EMPTY_STRING);
	$('#cutOffAmountFormat').val(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
}
function loadData() {
	$('#productCode').val(validator.getValue('LEASE_PRODUCT_CODE'));
	$('#productCode_desc').html(validator.getValue('F1_DESCRIPTION'));
	$('#stateCode').val(validator.getValue('STATE_CODE'));
	$('#stateCode_desc').html(validator.getValue('F3_DESCRIPTION'));
	$('#taxCode').val(validator.getValue('TAX_CODE'));
	$('#taxCode_desc').html(validator.getValue('F2_DESCRIPTION'));
	$('#effectiveDate').val(validator.getValue('EFF_DATE'));
	setCheckbox('taxBorne', validator.getValue('TAX_BY_US'));
	$('#accountingHead').val(validator.getValue('TAX_BYUS_GL_HEAD_CODE'));
	$('#accountingHead_desc').html(validator.getValue('F4_DESCRIPTION'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadAuditFields(validator);
}
