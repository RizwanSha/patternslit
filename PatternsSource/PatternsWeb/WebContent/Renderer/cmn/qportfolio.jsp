<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.cmn.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/cmn/qportfolio.js" />
	</web:dependencies>
	<web:loadPK />
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="mportfolio.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:dividerBlock>
				<web:viewContent id="po_view1" styleClass="hidden">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="mportfolio.portfoliocode" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:portfolioCodeDisplay property="portFolioCode" id="portFolioCode" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="mportfolio.description" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:descriptionDisplay property="description" id="description" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="mportfolio.concisedescription" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:conciseDescriptionDisplay property="conciseDescription" id="conciseDescription" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="mportfolio.parentportpolio" var="program"  />
										</web:column>
										<web:column>
											<type:portfolioCodeDisplay property="parentPortfolioCode" id="parentPortfolioCode" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="mportfolio.status" var="program" />
										</web:column>
										<web:column>
											<type:comboDisplay property="status" id="status" datasourceid="COMMON_PORTFOLIO_STATUS" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="form.enabled" var="common" />
										</web:column>
										<web:column>
											<type:checkboxDisplay property="enabled" id="enabled" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="form.remarks" var="common" />
										</web:column>
										<web:column>
											<type:remarksDisplay property="remarks" id="remarks" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:auditDisplay />
						</web:sectionBlock>
					</web:dividerBlock>
				</web:viewContent>
			</web:dividerBlock>
		</web:viewSubPart>
	</web:viewPart>
</web:fragment>