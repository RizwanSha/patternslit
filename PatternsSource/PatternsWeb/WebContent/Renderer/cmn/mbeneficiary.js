var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MBENEFICIARY';
function init() {
	refreshTBAGrid();
	refreshQueryGrid();
	if (_redisplay) {
		if ($('#action').val() == ADD) {
			$('#enabled').prop('disabled', true);
			setCheckbox('enabled', YES);
		}
	}
}

function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'MBENEFICIARY_MAIN', queryGrid);
}

function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}

function doclearfields(id) {
	switch (id) {
	case 'beneficiaryId':
		if (isEmpty($('#beneficiaryId').val())) {
			$('#beneficiaryId_error').html(EMPTY_STRING);
			$('#beneficiaryId_desc').html(EMPTY_STRING);
			clearNonPKFields();
			break;
		}
	}
}

function clearFields() {
	$('#beneficiaryId').val(EMPTY_STRING);
	$('#beneficiaryId_error').html(EMPTY_STRING);
	$('#beneficiaryId_desc').html(EMPTY_STRING);
	clearNonPKFields();
}

function clearNonPKFields() {
	$('#name').val(EMPTY_STRING);
	$('#name_error').html(EMPTY_STRING);
	$('#address').val(EMPTY_STRING);
	$('#address_error').html(EMPTY_STRING);
	$('#accountNumber').val(EMPTY_STRING);
	$('#accountNumber_error').html(EMPTY_STRING);
	$('#accountType').val(EMPTY_STRING);
	$('#accountType_error').html(EMPTY_STRING);
	$('#ifscCode').val(EMPTY_STRING);
	$('#ifscCode_error').html(EMPTY_STRING);
	setCheckbox('enabled', YES);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
}

function doHelp(id) {
	switch (id) {
	case 'beneficiaryId':
		
		help('COMMON', 'HLP_MBENEFICIARY_M', $('#beneficiaryId').val(), EMPTY_STRING, $('#beneficiaryId'));
		break;
		
	}
}

function add() {
	$('#beneficiaryId').focus();
	setCheckbox('enabled', YES);
	$('#enabled').prop('disabled', true);
}

function modify() {
	$('#beneficiaryId').focus();
	if ($('#action').val() == MODIFY) {
		$('#enabled').prop('disabled', false);
	} else {
		$('#enabled').prop('disabled', true);
	}
}

function loadData() {
	$('#beneficiaryId').val(validator.getValue('BENEFICIARY_ID'));
	$('#name').val(validator.getValue('BENEFICIARY_NAME'));
	$('#address').val(validator.getValue('BENEFICIARY_ADDRESS'));
	$('#accountNumber').val(validator.getValue('BENEFICIARY_ACCOUNT_NO'));
	$('#accountType').val(validator.getValue('BENEFICIARY_ACCOUNT_TYPE'));
	$('#ifscCode').val(validator.getValue('BENEFICIARY_IFSC_CODE'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	resetLoading();
}

function view(source, primaryKey) {
	hideParent('cmn/qbeneficiary', source, primaryKey);
	resetLoading();
}

function validate(id) {
	switch (id) {
	case 'beneficiaryId':
		beneficiaryId_val();
		break;
	case 'name':
		name_val();
		break;
	case 'address':
		address_val();
		break;
	case 'accountNumber':
		accountNumber_val();
		break;
	case 'accountType':
		accountType_val();
		break;
	case 'ifscCode':
		ifscCode_val();
		break;
	case 'enabled':
		enabled_val();
		break;
	case 'remarks':
		remarks_val();
		break;
	}
}

function backtrack(id) {
	switch (id) {
	case 'beneficiaryId':
		setFocusLast('beneficiaryId');
		break;
	case 'name':
		setFocusLast('beneficiaryId');
		break;
	case 'address':
		setFocusLast('name');
		break;
	case 'accountNumber':
		setFocusLast('address');
		break;
	case 'accountType':
		setFocusLast('accountNumber');
		break;
	case 'ifscCode':
		setFocusLast('accountType');
		break;
	case 'enabled':
		setFocusLast('ifscCode');
		break;
	case 'remarks':
		if ($('#action').val() == MODIFY) {
			setFocusLast('enabled');
		} else {
			setFocusLast('ifscCode');
		}
		break;
	}
}

function beneficiaryId_val() {
	var value = $('#beneficiaryId').val();
	clearError('beneficiaryId_error');
	if (isEmpty(value)) {
		setError('beneficiaryId_error', HMS_MANDATORY);
		return false;
	} else if (!isValidCode(value)) {
		setError('beneficiaryId_error', HMS_INVALID_FORMAT);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('BENEFICIARY_ID', $('#beneficiaryId').val());
		validator.setValue(ACTION, $('#action').val());
		validator.setClass('patterns.config.web.forms.cmn.mbeneficiarybean');
		validator.setMethod('validateBeneficiaryId');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('beneficiaryId_error', validator.getValue(ERROR));
			$('#beneficiaryId_desc').html(EMPTY_STRING);
			return false;
		} else {
			if ($('#action').val() == MODIFY) {
				PK_VALUE = getEntityCode() + PK_SEPERATOR + $('#beneficiaryId').val();
				if (!loadPKValues(PK_VALUE, 'beneficiaryId_error')) {
					return false;
				}
			}
		}
	}
	setFocusLast('name');
	return true;
}

function name_val() {
	var value = $('#name').val();
	clearError('name_error');
	if (isEmpty(value)) {
		setError('name_error', MANDATORY);
		return false;
	}
	if (!isValidName(value)) {
		setError('name_error', INVALID_NAME);
		return false;
	}
	setFocusLast('address');
	return true;
}

function address_val() {
	var value = $('#address').val();
	clearError('address_error');
	if (isEmpty(value)) {
		setError('address_error', MANDATORY);
		return false;
	}
	if (!isValidAddress(value)) {
		setError('address_error', INVALID_ADDRESS);
		return false;
	}
	setFocusLast('accountNumber');
	return true;
}
function accountNumber_val() {
	var value = $('#accountNumber').val();
	clearError('accountNumber_error');
	if (isEmpty(value)) {
		setError('accountNumber_error', MANDATORY);
		return false;
	}
	if (!isValidBeneficiaryAccountNumber(value)) {
		setError('accountNumber_error', PBS_INVALID_BENEFICIARY_ACCOUNT_NUMBER);
		return false;
	}
	setFocusLast('accountType');
	return true;
}

function accountType_val() {
	var value = $('#accountType').val();
	clearError('accountType_error');
	if (!isEmpty(value)) {
		if (!isValidConciseDescription(value)) {
			setError('accountType_error', PBS_INVALID_BENEFICIARY_ACCOUNT_TYPE);
			return false;
		}
	}
	setFocusLast('ifscCode');
	return true;
}

function ifscCode_val() {
	var value = $('#ifscCode').val();
	clearError('ifscCode_error');
	if (isEmpty(value)) {
		setError('ifscCode_error', MANDATORY);
		return false;
	}
	if (!isValidIFSCCode(value)) {
		setError('ifscCode_error', INVALID_IFSC_CODE);
		return false;
	}
	if ($('#action').val() == ADD) {
		setFocusLast('remarks');
	} else {
		setFocusLast('enabled');
	}
	return true;
}

function enabled_val() {
	if ($('#action').val() == MODIFY) {
		setFocusLast('remarks');
	}
	return true;
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', INVALID_REMARKS);
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', INVALID_REMARKS);
			return false;
		}
	}
	if ($('#action').val() == MODIFY) {
		if (isEmpty(value)) {
			setError('remarks_error', MANDATORY);
			return false;
		}

	}
	setFocusOnSubmit();
	return true;

}
