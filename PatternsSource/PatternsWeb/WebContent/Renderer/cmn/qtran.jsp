<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<web:bundle baseName="patterns.config.web.forms.cmn.Messages"
	var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/cmn/qtran.js" />
	</web:dependencies>
	<web:loadPK />
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="qcustsegment.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:viewContent id="po_view1" styleClass="hidden">
				<web:dividerBlock>
					<web:sectionBlock width="width-100p" align="left">
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="180px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="mtran.transactioncode" var="program"
											mandatory="true" />
									</web:column>
									<web:column>
										<type:tranCodeDisplay property="transactionCode"
											id="transactionCode" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="180px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowOdd>
									<web:column>
										<web:legend key="form.description" var="common"
											mandatory="true" />
									</web:column>
									<web:column>
										<type:descriptionDisplay property="description"
											id="description" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="form.conciseDescription" var="common"
											mandatory="true" />
									</web:column>
									<web:column>
										<type:conciseDescriptionDisplay property="conciseDescription"
											id="conciseDescription" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="mtran.alphaaccesscode" var="program" />
									</web:column>
									<web:column>
										<type:tranAccessCodeDisplay property="alphaAccessCode"
											id="alphaAccessCode" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="mtran.transactionmode" var="program" />
									</web:column>
									<web:column>
										<type:comboDisplay property="transactionMode"
											id="transactionMode" datasourceid="COMMON_TRANMODE" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="mtran.depositwithdrawal" var="program" />
									</web:column>
									<web:column>
										<type:comboDisplay property="depositWithdrawal"
											id="depositWithdrawal" datasourceid="COMMON_TRANTYPE" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="mtran.transactionusingdocument" var="program" />
									</web:column>
									<web:column>
										<type:comboDisplay property="transactionUsingDocument"
											id="transactionUsingDocument" datasourceid="COMMON_TRANDOC" />
									</web:column>
								</web:rowEven>

								<web:rowOdd>
									<web:column>
										<web:legend key="mtran.slipnumberrequired" var="program" />
									</web:column>
									<web:column>
										<type:checkboxDisplay property="slipNumberRequired"
											id="slipNumberRequired" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="mtran.numberoflegsinthetransaction"
											var="program" />
									</web:column>
									<web:column>
										<type:comboDisplay property="numberOfLegsInTheTransaction"
											id="numberOfLegsInTheTransaction"
											datasourceid="COMMON_TRANLEG" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="form.IsUseEnabled" var="common" />
									</web:column>
									<web:column>
										<type:checkboxDisplay property="enabled" id="enabled" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="form.remarks" var="common" />
									</web:column>
									<web:column>
										<type:remarksDisplay property="remarks" id="remarks" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:auditDisplay />
					</web:sectionBlock>
				</web:dividerBlock>
			</web:viewContent>
		</web:viewSubPart>
	</web:viewPart>
</web:fragment>