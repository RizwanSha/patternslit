var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MBTRANCODES';
var tranMode = EMPTY_STRING;
var tranType = EMPTY_STRING;

function init() {
	refreshTBAGrid();
	refreshQueryGrid();
	if (_redisplay) {
		if ($('#action').val() == ADD) {
			$('#enabled').prop('disabled', true);
			setCheckbox('enabled', YES);
		}
		setCheckbox('localCurrTranFlag', decodeBTS($('#localCurrTranFlag').prop('checked')));
		trancodes_innerGrid.clearAll();
		if (!isEmpty($('#xmlTranCodesGrid').val())) {
			trancodes_innerGrid.loadXMLString($('#xmlTranCodesGrid').val());
		}
	}
}

function loadGrid() {
	loadGridQuery(CURRENT_PROGRAM_ID, 'MBTRANCODES_GRID', trancodes_innerGrid);
}
function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'MBTRANCODES_MAIN', queryGrid);
}

function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}

function doHelp(id) {
	switch (id) {
	case 'bankingTranCode':
		help('COMMON', 'HLP_BTRAN_CODE_M', $('#bankingTranCode').val(), EMPTY_STRING, $('#bankingTranCode'), function(gridObj, rowID) {
		});
		break;
	case 'transactionCode':
		help('COMMON', 'HLP_TRAN_CODE', $('#transactionCode').val(), EMPTY_STRING, $('#transactionCode'));
		break;
	}
}

function clearFields() {
	$('#bankingTranCode').val(EMPTY_STRING);
	$('#bankingTranCode_desc').html(EMPTY_STRING);
	$('#bankingTranCode_error').html(EMPTY_STRING);
	clearNonPKFields();
}

function clearNonPKFields() {
	$('#description').val(EMPTY_STRING);
	$('#description_error').html(EMPTY_STRING);
	$('#conciseDescription').val(EMPTY_STRING);
	$('#conciseDescription_error').html(EMPTY_STRING);
	$('#bankingTranAccCode').val(EMPTY_STRING);
	$('#bankingTranAccCode_error').html(EMPTY_STRING);
	setCheckbox('localCurrTranFlag', NO);
	$('#localCurrTranFlag_error').html(EMPTY_STRING);
	$('#transactionCode').val(EMPTY_STRING);
	$('#transactionCode_desc').html(EMPTY_STRING);
	$('#transactionCode_error').html(EMPTY_STRING);
	if ($('#action').val() == ADD) {
		setCheckbox('enabled', YES);
		$('#enabled').prop('disabled', true);
	} else {
		setCheckbox('enabled', NO);
		$('#enabled').prop('disabled', false);
	}
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
	trancodes_clearGridFields();
	trancodes_innerGrid.clearAll();
}

function doclearfields(id) {
	switch (id) {
	case 'bankingTranCode':
		if (isEmpty($('#bankingTranCode').val())) {
			$('#bankingTranCode_desc').html(EMPTY_STRING);
			$('#bankingTranCode_error').html(EMPTY_STRING);
			clearNonPKFields();
		}
		break;
	}
}

function add() {
	$('#bankingTranCode').focus();
	setCheckbox('enabled', YES);
	$('#enabled').prop('disabled', true);
}

function modify() {
	$('#bankingTranCode').focus();
	if ($('#action').val() == MODIFY) {
		$('#enabled').prop('disabled', false);
	} else {
		$('#enabled').prop('disabled', true);
	}
}

function loadData() {
	$('#bankingTranCode').val(validator.getValue('BTRAN_CODE'));
	$('#description').val(validator.getValue('DESCRIPTION'));
	$('#conciseDescription').val(validator.getValue('CONCISE_DESCRIPTION'));
	$('#bankingTranAccCode').val(validator.getValue('BTRAN_ACCESS_CODE'));
	setCheckbox('localCurrTranFlag', validator.getValue('BTRAN_LC_FLAG'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadGrid();
	resetLoading();
}

function view(source, primaryKey) {
	hideParent('cmn/qbtrancodes', source, primaryKey);
	resetLoading();
}

function validate(id) {
	switch (id) {
	case 'bankingTranCode':
		bankingTranCode_val();
		break;
	case 'description':
		description_val();
		break;
	case 'conciseDescription':
		conciseDescription_val();
		break;
	case 'bankingTranAccCode':
		bankingTranAccCode_val();
		break;
	case 'localCurrTranFlag':
		localCurrTranFlag_val();
		break;
	case 'transactionCode':
		transactionCode_val();
		break;
	case 'enabled':
		enabled_val();
		break;
	case 'remarks':
		remarks_val();
		break;
	}
}

function backtrack(id) {
	switch (id) {
	case 'bankingTranCode':
		setFocusLast('bankingTranCode');
		break;
	case 'description':
		setFocusLast('bankingTranCode');
		break;
	case 'conciseDescription':
		setFocusLast('description');
		break;
	case 'bankingTranAccCode':
		setFocusLast('conciseDescription');
		break;
	case 'localCurrTranFlag':
		setFocusLast('bankingTranAccCode');
		break;
	case 'transactionCode':
		setFocusLast('localCurrTranFlag');
		break;
	case 'trancodes_innerGrid_add':
		setFocusLast('transactionCode');
		break;
	case 'enabled':
		setFocusLast('transactionCode');
		break;
	case 'remarks':
		if ($('#action').val() == MODIFY) {
			setFocusLast('enabled');
		} else {
			setFocusLast('transactionCode');
		}
		break;
	}
}

function bankingTranCode_val() {
	var value = $('#bankingTranCode').val();
	clearError('bankingTranCode_error');
	if (isEmpty(value)) {
		setError('bankingTranCode_error', MANDATORY);
		return false;
	} else if (!isNumeric(value)) {
		setError('bankingTranCode_error', INVALID_NUMBER);
		return false;
	}
	value = parseInt($('#bankingTranCode').val());
	$('#bankingTranCode').val(value);
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('BTRAN_CODE', $('#bankingTranCode').val());
	validator.setValue('PROGRAM_ID', CURRENT_PROGRAM_ID);
	validator.setValue(ACTION, $('#action').val());
	validator.setClass('patterns.config.web.forms.cmn.mbtrancodesbean');
	validator.setMethod('validateBankingTranCode');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('bankingTranCode_error', validator.getValue(ERROR));
		return false;
	} else {
		if ($('#action').val() == MODIFY) {
			PK_VALUE = $('#bankingTranCode').val();
			if (!loadPKValues(PK_VALUE, 'bankingTranCode_error')) {
				return false;
			}
		}
	}
	setFocusLast('description');
	return true;
}

function description_val() {
	var value = $('#description').val();
	clearError('description_error');
	if (isEmpty(value)) {
		setError('description_error', MANDATORY);
		return false;
	}
	if (!isValidDescription(value)) {
		setError('description_error', INVALID_DESCRIPTION);
		return false;
	}
	setFocusLast('conciseDescription');
	return true;
}

function conciseDescription_val() {
	var value = $('#conciseDescription').val();
	clearError('conciseDescription_error');
	if (isEmpty(value)) {
		setError('conciseDescription_error', MANDATORY);
		return false;
	}
	if (!isValidConciseDescription(value)) {
		setError('conciseDescription_error', INVALID_CONCISE_DESCRIPTION);
		return false;
	}
	setFocusLast('bankingTranAccCode');
	return true;
}

function bankingTranAccCode_val() {
	var value = $('#bankingTranAccCode').val();
	clearError('bankingTranAccCode_error');
	if (!isEmpty(value)) {
		if (!isValidCode(value)) {
			setError('bankingTranAccCode_error', INVALID_FORMAT);
			return false;
		}
		if (value == $('#bankingTranCode').val()) {
			setError('bankingTranAccCode_error', PBS_ALPHA_ACCESS_CODE_NOTEQUAL);
			return false;
		}
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('PROGRAM_ID', CURRENT_PROGRAM_ID);
		validator.setValue('BTRAN_CODE', $('#bankingTranCode').val());
		validator.setValue('BTRAN_ACCESS_CODE', $('#bankingTranAccCode').val());
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.cmn.mbtrancodesbean');
		validator.setMethod('validateBankingTranAccCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('bankingTranAccCode_error', validator.getValue(ERROR));
			return false;
		}
	}
	setFocusLast('localCurrTranFlag');
	return true;
}

function localCurrTranFlag_val() {
	setFocusLast('transactionCode');
	return true;
}

function transactionCode_val() {
	var value = $('#transactionCode').val();
	clearError('transactionCode_error');
	$('#transactionCode_desc').html(EMPTY_STRING);
	if (isEmpty(value)) {
		setError('transactionCode_error', MANDATORY);
		return false;
	}
	if (!isNumeric(value)) {
		setError('transactionCode_error', INVALID_NUMBER);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('TRAN_CODE', $('#transactionCode').val());
		validator.setValue('ACTION', USAGE);
		validator.setClass('patterns.config.web.forms.cmn.mbtrancodesbean');
		validator.setMethod('validateTransactionCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('transactionCode_error', validator.getValue(ERROR));
			return false;
		} else {
			if (validator.getValue(RESULT) == DATA_AVAILABLE) {
				$('#transactionCode_desc').html(validator.getValue('DESCRIPTION'));
				tranMode = validator.getValue('TRAN_MODE');
				tranType = validator.getValue('TRAN_TYPE');
			}
		}
	}
	setFocusLastOnGridSubmit('trancodes_innerGrid');
	return true;
}

function enabled_val() {
	if ($('#action').val() == MODIFY) {
		setFocusLast('remarks');
	}
	return true;
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', INVALID_REMARKS);
			setFocusLast('remarks');
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', INVALID_REMARKS);
			setFocusLast('remarks');
			return false;
		}
	}
	if ($('#action').val() == MODIFY) {
		if (isEmpty(value)) {
			setError('remarks_error', MANDATORY);
			setFocusLast('remarks');
			return false;
		}
	}
	setFocusOnSubmit();
	return true;
}

function trancodes_addGrid(editMode, editRowId) {
	if (transactionCode_val(false)) {
		if (trancodes_gridDuplicateCheck(editMode, editRowId)) {
			var field_values = [ 'false', $('#transactionCode').val(), $('#transactionCode_desc').html(), tranMode, tranType ];
			_grid_updateRow(trancodes_innerGrid, field_values);
			trancodes_clearGridFields();
			$('#transactionCode').focus();
			return true;
		} else {
			setError('transactionCode_error', DUPLICATE_DATA);
			$('#transactionCode_desc').html(EMPTY_STRING);
			return false;
		}
	} else {
		return false;
	}
}

function trancodes_gridDuplicateCheck(editMode, editRowId) {
	var currentValue = [ $('#transactionCode').val() ];
	return _grid_duplicate_check(trancodes_innerGrid, currentValue, [ 1 ]);
}

function trancodes_editGrid(rowId) {
	$('#transactionCode').val(trancodes_innerGrid.cells(rowId, 1).getValue());
	$('#transactionCode_desc').html(trancodes_innerGrid.cells(rowId, 2).getValue());
	$('#transactionCode').focus();
}

function trancodes_cancelGrid(rowId) {
	trancodes_clearGridFields();
}

function trancodes_clearGridFields() {
	$('#transactionCode').val(EMPTY_STRING);
	$('#transactionCode_desc').html(EMPTY_STRING);
	clearError('transactionCode_error');
}

function gridExit(id) {
	switch (id) {
	case 'transactionCode':
		$('#xmlTranCodesGrid').val(trancodes_innerGrid.serialize());
		trancodes_clearGridFields();
		setFocusLast('remarks');
		if ($('#action').val() == MODIFY) {
			setFocusLast('enabled');
		}
		break;
	}
}

function gridDeleteRow(id) {
	switch (id) {
	case 'transactionCode':
		deleteGridRecord(trancodes_innerGrid);
		break;
	}
}

function revalidate() {
	trancodes_revaildateGrid();
}

function trancodes_revaildateGrid() {
	if (trancodes_innerGrid.getRowsNum() > 0) {
		$('#xmlTranCodesGrid').val(trancodes_innerGrid.serialize());
		$('#transactionCode_error').html(EMPTY_STRING);
	} else {
		trancodes_innerGrid.clearAll();
		$('#xmlTranCodesGrid').val(trancodes_innerGrid.serialize());
		setError('transactionCode_error', ADD_ATLEAST_ONE_ROW);
		errors++;
	}
}
