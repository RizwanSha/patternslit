var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MPORTFOLIO';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}

function clearFields() {
	$('#portFolioCode').val(EMPTY_STRING);
	$('#description').val(EMPTY_STRING);
	$('#conciseDescription').val(EMPTY_STRING);
	$('#parentPortfolioCode').val(EMPTY_STRING);
	$('#parentPortfolioCode_desc').html(EMPTY_STRING);
	$('#status').val(EMPTY_STRING);
	setCheckbox('enabled', NO);
	$('#remarks').val(EMPTY_STRING);
}

function loadData() {
	$('#portFolioCode').val(validator.getValue('PORTFOLIO_CODE'));
	$('#description').val(validator.getValue('DESCRIPTION'));
	$('#conciseDescription').val(validator.getValue('CONCISE_DESCRIPTION'));
	$('#parentPortfolioCode').val(validator.getValue('PARENT_PORTFOLIO_CODE'));
	$('#parentPortfolioCode_desc').html(validator.getValue('F1_DESCRIPTION'));
	$('#status').val(validator.getValue('STATUS'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadAuditFields(validator);
}
