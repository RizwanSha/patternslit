var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'ENOMOS';
function init() {
	refreshTBAGrid();
	refreshQueryGrid();
	if (_redisplay) {
		$('#precedingEntry').val($('#tempPrecedingEntry').val());
		$('#reversal').val($('#tempReversal').val());
		setReadOnly();
	}
}
function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'ENOMOS_MAIN');
}
function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}
function add() {
	$('#systemRefNo').prop('readonly', true);
	$('#systemRefNo_pic').prop('disabled', true);
	setFocusLast('glHead');
}

function doHelp(id) {
	switch (id) {
	case 'glHead':
		help('MGLMAST', 'HLP_GL_CODE', $('#glHead').val(), EMPTY_STRING, $('#glHead'));
		break;
	case 'systemRefNo':
		help('ENOMOS', 'HLP_NOMOS_SYS_REF_NO', $('#systemRefNo').val(),  getBranchCode()+ PK_SEPERATOR +$('#glHead').val() , $('#systemRefNo'));
		break;
	case 'interBranchTranType':
		help('COMMON', 'HLP_INTER_BANK_TRAN_TYPES', $('#interBranchTranType').val(), EMPTY_STRING, $('#interBranchTranType'));
		break;
	case 'cifNumber':
		help('COMMON', 'HLP_CUST_ID_FOLIO', $('#cifNumber').val(), EMPTY_STRING, $('#cifNumber'));
		break;
	case 'staffId':
		help('COMMON', 'HLP_EMP_CODE', $('#staffId').val(), EMPTY_STRING, $('#staffId'));
		break;
	}
}

function modify() {
	if ($('#action').val() == MODIFY) {
		$('#systemRefNo').prop('readonly', false);
		$('#systemRefNo_pic').prop('disabled', false);
	} else {
		$('#systemRefNo').prop('readonly', true);
		$('#systemRefNo_pic').prop('disabled', true);
	}
	setFocusLast('glHead');
}
function loadData() {
	$('#glHead').val(validator.getValue('GL_HEAD_CODE'));
	$('#glHead_desc').html(validator.getValue('F1_DESCRIPTION'));
	$('#precedingEntry').val(validator.getValue('F2_PRECEED_ENTRY_TYPE'));
	$('#folioRelationship').val(validator.getValue('F2_FOLIO_RELATIONSHIP'));
	$('#reversal').val(validator.getValue('F2_RECON_TYPE'));
	$('#tempPrecedingEntry').val($('#precedingEntry').val());
	$('#tempReversal').val($('#reversal').val());

	$('#systemRefNo').val(validator.getValue('SYSTEM_REF_NO'));
	$('#dateOfEntry').val(validator.getValue('ENTRY_DATE'));
	$('#entryType').val(validator.getValue('ENTRY_TYPE'));
	$('#interBranchTranType').val(validator.getValue('INTER_BRN_TRAN_TYPES'));
	$('#interBranchTranType_desc').html(validator.getValue('F3_DESCRIPTION'));
	$('#numberingRequired').val(validator.getValue('F3_NUMBERING_REQUIRED'));
	$('#manualNumbersAllowed').val(validator.getValue('F3_MANUAL_NUMBERS_ALLOWED'));
	$('#originatingDebitsAllowed').val(validator.getValue('F3_ORIGINATING_DEBITS_ALLOWED'));
	$('#debitOrCredit').val(validator.getValue('TRAN_DB_CR_FLAG'));
	$('#orgEntryAmtActl').val(validator.getValue('ORIG_ENTRY_AMT_AC'));
	$('#orgEntryAmtActl_curr').val(validator.getValue('ORIG_ENTRY_CCY_AC'));
	$('#orgEntryAmtBase').val(validator.getValue('ORIG_ENTRY_AMT_BC'));
	$('#orgEntryAmtBase_curr').val(validator.getValue('ORIG_ENTRY_CCY_BC'));
	$('#manualEntryRefNo').val(validator.getValue('MANUAL_ENTRY_REF_NO'));
	$('#systemEntryRefDate').val(validator.getValue('SYSTEM_TRAN_REF_NO_DATE'));
	$('#systemEntryRefBatchNo').val(validator.getValue('SYSTEM_TRAN_REF_NO_BATCH'));
	$('#systemEntryRefSerial').val(validator.getValue('SYSTEM_TRAN_REF_NO_SERIAL'));

	$('#openBalOfEntryActl').val(validator.getValue('OPEN_BAL_OF_ENTRY_AC'));
	$('#openBalOfEntryActl_curr').val(validator.getValue('ORIG_ENTRY_CCY_AC'));
	$('#openBalActDbOrCr').val(validator.getValue('OPEN_BAL_OF_ENTRY_DBCR_AC'));
	$('#openBalOfEntryBase').val(validator.getValue('OPEN_BAL_OF_ENTRY_BC'));
	$('#openBalOfEntryBase_curr').val(validator.getValue('ORIG_ENTRY_CCY_BC'));
	$('#openBalOfEntryBaseFormat').val(validator.getValue('OPEN_BAL_OF_ENTRY_BC'));
	$('#openBalBaseDbOrCr').val(validator.getValue('OPEN_BAL_OF_ENTRY_DBCR_BC'));
	$('#openingBalbaseDate').val(validator.getValue('OPEN_BALANCE_BASE_DATE'));

	$('#cifNumber').val(validator.getValue('CIF_NO'));
	$('#cifNumber_desc').html(validator.getValue('F4_NAME'));
	$('#staffId').val(validator.getValue('EMP_ID'));
	$('#staffId_desc').html(validator.getValue('F5_PERSON_NAME'));
	$('#notes').val(validator.getValue('REMARKS'));
	setReadOnly();
	$('#orgEntryAmtActlFormat').val(formatAmount($('#orgEntryAmtActl').val(), $('#orgEntryAmtActl_curr').val()));
	$('#orgEntryAmtBaseFormat').val(formatAmount($('#orgEntryAmtBase').val(), $('#orgEntryAmtBase_curr').val()));
	$('#openBalOfEntryActlFormat').val(formatAmount($('#openBalOfEntryActl').val(), $('#openBalOfEntryActl_curr').val()));
	$('#openBalOfEntryBaseFormat').val(formatAmount($('#openBalOfEntryBase').val(), $('#openBalOfEntryBase_curr').val()));
}

function view(source, primaryKey) {
	hideParent('cmn/qnomos', source, primaryKey,1200);
	resetLoading();
}
function clearFields() {
	$('#glHead').val(EMPTY_STRING);
	$('#glHead_desc').html(EMPTY_STRING);
	$('#glHead_error').html(EMPTY_STRING);
	$('#precedingEntry').val(EMPTY_STRING);
	$('#tempPrecedingEntry').val(EMPTY_STRING);
	$('#folioRelationship').val(EMPTY_STRING);
	$('#reversal').val(EMPTY_STRING);
	$('#tempReversal').val(EMPTY_STRING);
	$('#systemRefNo').val(EMPTY_STRING);
	$('#systemRefNo_error').html(EMPTY_STRING);
	$('#systemRefNo').prop('readonly', false);
	$('#systemRefNo_pic').prop('disabled', false);
	clearNonPKFields();
}

function clearNonPKFields() {
	$('#dateOfEntry').val(EMPTY_STRING);
	$('#dateOfEntry_error').html(EMPTY_STRING);
	$('#entryType').val(EMPTY_STRING);
	$('#entryType_error').html(EMPTY_STRING);
	$('#interBranchTranType').val(EMPTY_STRING);
	$('#interBranchTranType_desc').html(EMPTY_STRING);
	$('#interBranchTranType_error').html(EMPTY_STRING);
	$('#numberingRequired').val(EMPTY_STRING);
	$('#manualNumbersAllowed').val(EMPTY_STRING);
	$('#originatingDebitsAllowed').val(EMPTY_STRING);
	$('#debitOrCredit').val(EMPTY_STRING);
	$('#debitOrCredit_error').html(EMPTY_STRING);
	$('#orgEntryAmtActl').val(EMPTY_STRING);
	$('#orgEntryAmtActl_error').html(EMPTY_STRING);
	$('#orgEntryAmtActl_curr').val(EMPTY_STRING);
	$('#orgEntryAmtActlFormat').val(EMPTY_STRING);
	$('#orgEntryAmtBase').val(EMPTY_STRING);
	$('#orgEntryAmtBase_error').html(EMPTY_STRING);
	$('#orgEntryAmtBase_curr').val(getBaseCurrency());
	$('#orgEntryAmtBaseFormat').val(EMPTY_STRING);
	$('#manualEntryRefNo').val(EMPTY_STRING);
	$('#manualEntryRefNo_error').html(EMPTY_STRING);
	$('#systemEntryRefDate').val(EMPTY_STRING);
	$('#systemEntryRefBatchNo').val(EMPTY_STRING);
	$('#systemEntryRefSerial').val(EMPTY_STRING);
	$('#systemEntryRef_error').html(EMPTY_STRING);
	clearOpenbalSection();
	$('#cifNumber').val(EMPTY_STRING);
	$('#cifNumber_desc').html(EMPTY_STRING);
	$('#cifNumber_error').html(EMPTY_STRING);
	$('#staffId').val(EMPTY_STRING);
	$('#staffId_desc').html(EMPTY_STRING);
	$('#staffId_error').html(EMPTY_STRING);
	$('#notes').val(EMPTY_STRING);
	$('#notes_error').html(EMPTY_STRING);
	$('#orgEntryAmtBaseFormat').prop('readonly', true);
	$('#openBalOfEntryBaseFormat').prop('readonly', true);
	$('#manualEntryRefNo').prop('readonly', true);
	$('#cifNumber').prop('readonly', true);
	$('#cifNumber_pic').prop('disabled', true);
	$('#staffId').prop('readonly', true);
	$('#staffId_pic').prop('disabled', true);
	hide('view_normal');
	hide('view_cif');
	hide('view_staff');
}

function clearOpenbalSection() {
	$('#openBalOfEntryActl').val(EMPTY_STRING);
	$('#openBalOfEntryActl_error').html(EMPTY_STRING);
	$('#openBalOfEntryActl_curr').val(EMPTY_STRING);
	$('#openBalOfEntryActlFormat').val(EMPTY_STRING);
	$('#openBalActDbOrCr').val(EMPTY_STRING);
	$('#openBalActDbOrCr_error').html(EMPTY_STRING);
	$('#openBalOfEntryBase').val(EMPTY_STRING);
	$('#openBalOfEntryBase_error').html(EMPTY_STRING);
	$('#openBalOfEntryBase_curr').val(getBaseCurrency());
	$('#openBalOfEntryBaseFormat').val(EMPTY_STRING);
	$('#openBalBaseDbOrCr').val(EMPTY_STRING);
	$('#openBalBaseDbOrCr_error').html(EMPTY_STRING);
	$('#openingBalbaseDate').val(EMPTY_STRING);
	$('#openingBalbaseDate_error').html(EMPTY_STRING);
}

function setReadOnly() {
	if ($('#action').val() == MODIFY) {
		$('#systemRefNo').prop('readonly', false);
		$('#systemRefNo_pic').prop('disabled', false);
	} else {
		$('#systemRefNo').prop('readonly', true);
		$('#systemRefNo_pic').prop('disabled', true);
	}
	if ($('#orgEntryAmtActl_curr').val() == $('#orgEntryAmtBase_curr').val()) {
		$('#orgEntryAmtBaseFormat').prop('readonly', true);
	} else {
		$('#orgEntryAmtBaseFormat').prop('readonly', false);
	}
	if ($('#openBalOfEntryActl_curr').val() == $('#openBalOfEntryBase_curr').val()) {
		$('#openBalOfEntryBaseFormat').prop('readonly', true);
	} else {
		$('#openBalOfEntryBaseFormat').prop('readonly', false);
	}
	$('#cifNumber').prop('readonly', true);
	$('#cifNumber_pic').prop('disabled', true);
	$('#staffId').prop('readonly', true);
	$('#staffId_pic').prop('disabled', true);
	$('#manualEntryRefNo').prop('readonly', true);
	if ($('#entryType').val() == 'N') {
		show('view_normal');
		if ($('#manualNumbersAllowed').val() == ONE)
			$('#manualEntryRefNo').prop('readonly', false);
		if ($('#folioRelationship').val() == '1' || $('#folioRelationship').val() == '3') {
			show('view_cif');
			hide('view_staff');
			$('#cifNumber').prop('readonly', false);
			$('#cifNumber_pic').prop('disabled', false);
		} else if ($('#folioRelationship').val() == '2') {
			show('view_staff');
			hide('view_cif');
			$('#staffId').prop('readonly', false);
			$('#staffId_pic').prop('disabled', false);
		}
	} else {
		hide('view_normal');
		hide('view_cif');
		hide('view_staff');
	}
}

function backtrack(id) {
	switch (id) {
	case 'glHead':
		setFocusLast('glHead');
		break;
	case 'systemRefNo':
		setFocusLast('glHead');
		break;
	case 'dateOfEntry':
		if ($('#action').val() == MODIFY)
			setFocusLast('systemRefNo');
		else
			setFocusLast('glHead');
		break;
	case 'interBranchTranType':
		setFocusLast('dateOfEntry');
		break;
	case 'entryType':
		setFocusLast('interBranchTranType');
		break;
	case 'debitOrCredit':
		setFocusLast('entryType');
		break;
	case 'orgEntryAmtActlFormat':
		setFocusLast('debitOrCredit');
		break;
	case 'orgEntryAmtBaseFormat':
		setFocusLast('orgEntryAmtActlFormat');
		break;
	case 'manualEntryRefNo':
		if ($('#orgEntryAmtActl_curr').val() == $('#orgEntryAmtBase_curr').val())
			setFocusLast('orgEntryAmtActlFormat');
		else
			setFocusLast('orgEntryAmtBaseFormat');
		break;
	case 'openBalOfEntryActlFormat':
		if ($('#entryType').val() == 'N' && $('#manualNumbersAllowed').val() == ONE)
			setFocusLast('manualEntryRefNo');
		else if ($('#orgEntryAmtActl_curr').val() == $('#orgEntryAmtBase_curr').val())
			setFocusLast('orgEntryAmtActlFormat');
		else
			setFocusLast('orgEntryAmtBaseFormat');
		break;
	case 'openBalActDbOrCr':
		setFocusLast('openBalOfEntryActlFormat');
		break;
	case 'openBalOfEntryBaseFormat':
		setFocusLast('openBalActDbOrCr');
		break;
	case 'cifNumber':
		if ($('#openBalOfEntryActl_curr').val() != $('#openBalOfEntryBase_curr').val())
			setFocusLast('openBalOfEntryBaseFormat');
		else
			setFocusLast('openBalActDbOrCr');
		break;
	case 'staffId':
		if ($('#openBalOfEntryActl_curr').val() != $('#openBalOfEntryBase_curr').val())
			setFocusLast('openBalOfEntryBaseFormat');
		else
			setFocusLast('openBalActDbOrCr');
		break;
	case 'notes':
		if ($('#entryType').val() == 'N') {
			if ($('#folioRelationship').val() == '1' || $('#folioRelationship').val() == '3')
				setFocusLast('cifNumber');
			else if ($('#folioRelationship').val() == '2')
				setFocusLast('staffId');
			else if ($('#openBalOfEntryActl_curr').val() != $('#openBalOfEntryBase_curr').val())
				setFocusLast('openBalOfEntryBaseFormat');
			else
				setFocusLast('openBalActDbOrCr');
		} else {
			if ($('#orgEntryAmtActl_curr').val() == $('#orgEntryAmtBase_curr').val())
				setFocusLast('orgEntryAmtActlFormat');
			else
				setFocusLast('orgEntryAmtBaseFormat');
		}
		break;
	}
}
function validate(id) {
	switch (id) {
	case 'glHead':
		glHead_val();
		break;
	case 'systemRefNo':
		systemRefNo_val();
		break;
	case 'dateOfEntry':
		dateOfEntry_val();
		break;
	case 'entryType':
		entryType_val(true);
		break;
	case 'interBranchTranType':
		interBranchTranType_val();
		break;
	case 'debitOrCredit':
		debitOrCredit_val(true);
		break;
	case 'orgEntryAmtActlFormat':
		orgEntryAmtActlFormat_val();
		break;
	case 'orgEntryAmtBaseFormat':
		orgEntryAmtBaseFormat_val();
		break;
	case 'manualEntryRefNo':
		manualEntryRefNo_val();
		break;
	case 'openBalOfEntryActlFormat':
		openBalOfEntryActlFormat_val();
		break;
	case 'openBalActDbOrCr':
		openBalActDbOrCr_val();
		break;
	case 'openBalOfEntryBaseFormat':
		openBalOfEntryBaseFormat_val();
		break;
	case 'cifNumber':
		cifNumber_val();
		break;
	case 'staffId':
		staffId_val();
		break;
	case 'notes':
		notes_val();
		break;
	}
}
function doclearfields(id) {
	switch (id) {
	case 'glHead':
		$('#glHead').val(EMPTY_STRING);
		$('#glHead_error').html(EMPTY_STRING);
		break;
	case 'systemRefNo':
		$('#systemRefNo').val(EMPTY_STRING);
		$('#systemRefNo_error').html(EMPTY_STRING);
		clearNonPKFields();
		break;
	}
}

function change(id) {
	switch (id) {
	case 'entryType':
		entryType_val(false);
		break;
	case 'debitOrCredit':
		debitOrCredit_val(false);
		break;

	}
}

function glHead_val() {
	var value = $('#glHead').val();
	clearError('glHead_error');
	$('#glHead_desc').html(EMPTY_STRING);
	$('#orgEntryAmtActl_curr').val(EMPTY_STRING);
	$('#openBalOfEntryActl_curr').val(EMPTY_STRING);
	$('#folioRelationship').val(EMPTY_STRING);

	$('#precedingEntry').val(EMPTY_STRING);
	$('#tempPrecedingEntry').val(EMPTY_STRING);
	$('#reversal').val(EMPTY_STRING);
	$('#tempReversal').val(EMPTY_STRING);
	if (isEmpty(value)) {
		setError('glHead_error', MANDATORY);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('GL_HEAD_CODE', $('#glHead').val());
	validator.setValue(ACTION, USAGE);
	validator.setClass('patterns.config.web.forms.cmn.enomosbean');
	validator.setMethod('validateGlHead');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('glHead_error', validator.getValue(ERROR));
		return false;
	} else {
		$('#glHead_desc').html(validator.getValue('DESCRIPTION'));
		$('#precedingEntry').val(validator.getValue('PRECEED_ENTRY_TYPE'));
		$('#tempPrecedingEntry').val(validator.getValue('PRECEED_ENTRY_TYPE'));
		$('#reversal').val(validator.getValue('RECON_TYPE'));
		$('#tempReversal').val(validator.getValue('RECON_TYPE'));
		$('#orgEntryAmtActl_curr').val(validator.getValue('GL_CURR_CODE'));
		$('#openBalOfEntryActl_curr').val(validator.getValue('GL_CURR_CODE'));
		$('#folioRelationship').val(validator.getValue('FOLIO_RELATIONSHIP'));
	}
	if ($('#action').val() == MODIFY)
		setFocusLast('systemRefNo');
	else
		setFocusLast('dateOfEntry');
	return true;
}

function systemRefNo_val() {
	var value = $('#systemRefNo').val();
	clearError('systemRefNo_error');
	if ($('#action').val() == MODIFY) {
		if (isEmpty(value)) {
			setError('systemRefNo_error', MANDATORY);
			return false;
		} else {
			validator.clearMap();
			validator.setMtm(false);
			validator.setValue('GL_HEAD_CODE', $('#glHead').val());
			validator.setValue('SYSTEM_REF_NO', $('#systemRefNo').val());
			validator.setValue(ACTION, $('#action').val());
			validator.setClass('patterns.config.web.forms.cmn.enomosbean');
			validator.setMethod('validateSystemRefNo');
			validator.sendAndReceive();
			if (validator.getValue(ERROR) != EMPTY_STRING) {
				setError('systemRefNo_error', validator.getValue(ERROR));
				return false;
			} else {
				if ($('#action').val() == MODIFY) {
					PK_VALUE = getEntityCode() + PK_SEPERATOR + getBranchCode() + PK_SEPERATOR+$('#glHead').val()+ PK_SEPERATOR + $('#systemRefNo').val();
					if (!loadPKValues(PK_VALUE, 'systemRefNo_error')) {
						return false;
					}
				}
			}
		}
	}
	setFocusLast('dateOfEntry');
	return true;
}

function dateOfEntry_val() {
	var value = $('#dateOfEntry').val();
	clearError('dateOfEntry_error');
	if (isEmpty(value)) {
		setError('dateOfEntry_error', MANDATORY);
		return false;
	}
	if (!isDate(value)) {
		setError('dateOfEntry_error', INVALID_DATE);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('GL_HEAD_CODE', $('#glHead').val());
	validator.setValue('ENTRY_DATE', $('#dateOfEntry').val());
	validator.setClass('patterns.config.web.forms.cmn.enomosbean');
	validator.setMethod('validateDateOfEntry');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('dateOfEntry_error', validator.getValue(ERROR));
		return false;
	}
	setFocusLast('interBranchTranType');
	return true;
}

function interBranchTranType_val() {
	var value = $('#interBranchTranType').val();
	clearError('interBranchTranType_error');
	$('#interBranchTranType_desc').html(EMPTY_STRING);
	$('#originatingDebitsAllowed').val(EMPTY_STRING);
	$('#numberingRequired').val(EMPTY_STRING);
	$('#manualNumbersAllowed').val(EMPTY_STRING);
	if (isEmpty(value)) {
		setError('interBranchTranType_error', MANDATORY);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('INTER_BRN_TRAN_TYPES', $('#interBranchTranType').val());
	validator.setValue(ACTION, USAGE);
	validator.setClass('patterns.config.web.forms.cmn.enomosbean');
	validator.setMethod('validateInterBranchTranType');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('interBranchTranType_error', validator.getValue(ERROR));
		return false;
	} else {
		$('#interBranchTranType_desc').html(validator.getValue('DESCRIPTION'));
		$('#originatingDebitsAllowed').val(validator.getValue('ORIGINATING_DEBITS_ALLOWED'));
		$('#numberingRequired').val(validator.getValue('NUMBERING_REQUIRED'));
		$('#manualNumbersAllowed').val(validator.getValue('MANUAL_NUMBERS_ALLOWED'));
	}
	setFocusLast('entryType');
	return true;
}

function entryType_val(valMode) {
	var value = $('#entryType').val();
	clearError('entryType_error');
	clearError('debitOrCredit_error');
	if (isEmpty(value)) {
		setError('entryType_error', MANDATORY);
		return false;
	}
	hide('view_normal');
	hide('view_cif');
	hide('view_staff');
	if ($('#entryType').val() == 'N') {
		show('view_normal');
		$('#debitOrCredit').val($('#precedingEntry').val());
		$('#openingBalbaseDate').val(getCBD());
		if ($('#manualNumbersAllowed').val() == ONE)
			$('#manualEntryRefNo').prop('readonly', false);
		else {
			$('#manualEntryRefNo').val(EMPTY_STRING);
			$('#manualEntryRefNo_erroe').html(EMPTY_STRING);
			$('#manualEntryRefNo').prop('readonly', true);
		}
		if ($('#folioRelationship').val() == '1' || $('#folioRelationship').val() == '3') {
			show('view_cif');
			$('#cifNumber').prop('readonly', false);
			$('#cifNumber_pic').prop('disabled', false);
			$('#staffId').val(EMPTY_STRING);
			$('#staffId_desc').val(EMPTY_STRING);
			$('#staffId_error').html(EMPTY_STRING);
			$('#staffId').prop('readonly', true);
			$('#staffId_pic').prop('disabled', true);
		} else if ($('#folioRelationship').val() == '2') {
			show('view_staff');
			$('#cifNumber').val(EMPTY_STRING);
			$('#cifNumber_desc').val(EMPTY_STRING);
			$('#cifNumber_error').html(EMPTY_STRING);
			$('#cifNumber').prop('readonly', true);
			$('#cifNumber_pic').prop('disabled', true);
			$('#staffId').prop('readonly', false);
			$('#staffId_pic').prop('disabled', false);
		} else {
			$('#cifNumber').val(EMPTY_STRING);
			$('#cifNumber_desc').val(EMPTY_STRING);
			$('#cifNumber_error').html(EMPTY_STRING);
			$('#staffId').val(EMPTY_STRING);
			$('#staffId_desc').val(EMPTY_STRING);
			$('#staffId_error').html(EMPTY_STRING);
			$('#cifNumber').prop('readonly', true);
			$('#cifNumber_pic').prop('disabled', true);
			$('#staffId').prop('readonly', true);
			$('#staffId_pic').prop('disabled', true);
		}
	} else {
		if ($('#precedingEntry').val() == DEBIT) {
			$('#debitOrCredit').val(CREDIT);
		} else {
			$('#debitOrCredit').val(DEBIT);
		}
		$('#cifNumber').val(EMPTY_STRING);
		$('#cifNumber_desc').val(EMPTY_STRING);
		$('#cifNumber_error').html(EMPTY_STRING);
		$('#staffId').val(EMPTY_STRING);
		$('#staffId_desc').val(EMPTY_STRING);
		$('#staffId_error').html(EMPTY_STRING);
		$('#cifNumber').prop('readonly', true);
		$('#cifNumber_pic').prop('disabled', true);
		$('#staffId').prop('readonly', true);
		$('#staffId_pic').prop('disabled', true);
		clearOpenbalSection();
		$('#manualEntryRefNo').val(EMPTY_STRING);
		$('#manualEntryRefNo_erroe').html(EMPTY_STRING);
		$('#manualEntryRefNo').prop('readonly', true);
	}
	debitOrCredit_val(false);
	if (valMode)
		setFocusLast('debitOrCredit');
	return true;
}

function debitOrCredit_val(valMode) {
	var value = $('#debitOrCredit').val();
	clearError('debitOrCredit_error');
	if (isEmpty(value)) {
		setError('debitOrCredit_error', MANDATORY);
		return false;
	}
	if (value == DEBIT) {
		if ($('#entryType').val() == 'N' && $('#precedingEntry').val() == CREDIT) {
			setError('debitOrCredit_error', PBS_ONLY_CREDIT_ALLOWED_FOR_REVERSAL);
			return false;
		}
		if ($('#entryType').val() == 'R' && $('#precedingEntry').val() == DEBIT) {
			setError('debitOrCredit_error', PBS_ONLY_CREDIT_ALLOWED_FOR_REVERSAL);
			return false;
		}
		if ($('#originatingDebitsAllowed').val() == ZERO) {
			setError('debitOrCredit_error', PBS_ORGINATE_DEBIT_NOT_ALLOWED_FOR_TRAN_TYPE);
			return false;
		}
	} else if (value == CREDIT) {
		if ($('#entryType').val() == 'N' && $('#precedingEntry').val() == DEBIT) {
			setError('debitOrCredit_error', PBS_ONLY_DEBIT_ALLOWED_FOR_REVERSAL);
			return false;
		}
		if ($('#entryType').val() == 'R' && $('#precedingEntry').val() == CREDIT) {
			setError('debitOrCredit_error', PBS_ONLY_DEBIT_ALLOWED_FOR_REVERSAL);
			return false;
		}
	}
	if (valMode)
		setFocusLast('orgEntryAmtActlFormat');
	return true;
}
function orgEntryAmtActlFormat_val() {
	var value = unformatAmount($('#orgEntryAmtActlFormat').val());
	clearError('orgEntryAmtActl_error');
	$('#orgEntryAmtActl').val(EMPTY_STRING);
	if (isEmpty(value)) {
		setError('orgEntryAmtActl_error', MANDATORY);
		return false;
	}
	if (isZero(value)) {
		setError('orgEntryAmtActl_error', ZERO_AMOUNT);
		return false;
	}
	if (!isCurrencyBigAmount($('#orgEntryAmtActl_curr').val(), value)) {
		setError('orgEntryAmtActl_error', INVALID_BIG_AMOUNT);
		return false;
	}
	$('#orgEntryAmtActlFormat').val(formatAmount(value, $('#orgEntryAmtActl_curr').val()));
	$('#orgEntryAmtActl').val(value);
	if ($('#orgEntryAmtActl_curr').val() == $('#orgEntryAmtBase_curr').val()) {
		$('#orgEntryAmtBaseFormat').val($('#orgEntryAmtActlFormat').val());
		$('#orgEntryAmtBase').val($('#orgEntryAmtActl').val());
		$('#orgEntryAmtBaseFormat').prop('readonly', true);
		if (!$("#manualEntryRefNo").attr('readonly'))
			setFocusLast('manualEntryRefNo');
		else
			setFocusLast('openBalOfEntryActlFormat');
	} else {
		$('#orgEntryAmtBaseFormat').val(EMPTY_STRING);
		$('#orgEntryAmtBase').val(EMPTY_STRING);
		$('#orgEntryAmtBaseFormat').prop('readonly', false);
		setFocusLast('orgEntryAmtBaseFormat');
	}
	return true;
}

function orgEntryAmtBaseFormat_val() {
	var value = unformatAmount($('#orgEntryAmtBaseFormat').val());
	clearError('orgEntryAmtBase_error');
	$('#orgEntryAmtBase').val(EMPTY_STRING);
	if (isEmpty(value)) {
		setError('orgEntryAmtBase_error', MANDATORY);
		return false;
	}
	if (isZero(value)) {
		setError('orgEntryAmtBase_error', ZERO_AMOUNT);
		return false;
	}
	if (!isCurrencyBigAmount($('#orgEntryAmtBase_curr').val(), value)) {
		setError('orgEntryAmtBase_error', INVALID_BIG_AMOUNT);
		return false;
	}
	$('#orgEntryAmtBaseFormat').val(formatAmount(value, $('#orgEntryAmtBase_curr').val()));
	$('#orgEntryAmtBase').val(value);
	if ($('#entryType').val() == 'N') {
		if ($('#manualNumbersAllowed').val() == ONE) {
			setFocusLast('manualEntryRefNo');
		} else {
			setFocusLast('openBalOfEntryActlFormat');
		}
	} else {
		setFocusLast('notes');
	}
	return true;
}

function manualEntryRefNo_val() {
	var value = $('#manualEntryRefNo').val();
	clearError('manualEntryRefNo_error');
	if ($('#manualNumbersAllowed').val() == ONE && $('#entryType').val() == 'N') {
		if (isEmpty(value)) {
			setError('manualEntryRefNo_error', MANDATORY);
			return false;
		}
	} else if (!isEmpty(value)) {
		setError('manualEntryRefNo_error', PBS_FIELD_SH_BE_BLANK);
		return false;
	}
	setFocusLast('openBalOfEntryActlFormat');
	return true;
}
function openBalOfEntryActlFormat_val() {
	var value = unformatAmount($('#openBalOfEntryActlFormat').val());
	clearError('openBalOfEntryActl_error');
	$('#openBalOfEntryActl').val(EMPTY_STRING);
	if (isEmpty(value)) {
		$('#openBalOfEntryActlFormat').val(ZERO);
		value = unformatAmount($('#openBalOfEntryActlFormat').val());
	}
	if (!isCurrencyBigAmount($('#openBalOfEntryActl_curr').val(), value)) {
		setError('openBalOfEntryActl_error', INVALID_BIG_AMOUNT);
		return false;
	}
	$('#openBalOfEntryActlFormat').val(formatAmount(value, $('#openBalOfEntryActl_curr').val()));
	$('#openBalOfEntryActl').val(value);
	if ($('#openBalOfEntryActl_curr').val() == $('#openBalOfEntryBase_curr').val()) {
		$('#openBalOfEntryBaseFormat').val($('#openBalOfEntryActlFormat').val());
		$('#openBalOfEntryBase').val($('#openBalOfEntryActl').val());
		$('#openBalOfEntryBaseFormat').prop('readonly', true);
	} else {
		$('#openBalOfEntryBaseFormat').val(EMPTY_STRING);
		$('#openBalOfEntryBase').val(EMPTY_STRING);
		$('#openBalOfEntryBaseFormat').prop('readonly', false);
	}
	setFocusLast('openBalActDbOrCr');
	return true;
}

function openBalActDbOrCr_val() {
	var value = $('#openBalActDbOrCr').val();
	clearError('openBalActDbOrCr_error');
	$('#openBalBaseDbOrCr').val(value);
	if ($('#openBalOfEntryActl_curr').val() == $('#openBalOfEntryBase_curr').val()) {
		if ($('#entryType').val() == 'N') {
			if ($('#folioRelationship').val() == '1' || $('#folioRelationship').val() == '3')
				setFocusLast('cifNumber');
			else if ($('#folioRelationship').val() == '2')
				setFocusLast('staffId');
			else
				setFocusLast('notes');
		} else {
			setFocusLast('notes');
		}
	} else {
		setFocusLast('openBalOfEntryBaseFormat');
	}
	return true;
}

function openBalOfEntryBaseFormat_val() {
	var value = unformatAmount($('#openBalOfEntryBaseFormat').val());
	clearError('openBalOfEntryBase_error');
	$('#openBalOfEntryBase').val(EMPTY_STRING);
	if (isEmpty(value)) {
		$('#openBalOfEntryBaseFormat').val(ZERO);
		value = unformatAmount($('#openBalOfEntryBaseFormat').val());
	}
	if (!isCurrencyBigAmount($('#openBalOfEntryBase_curr').val(), value)) {
		setError('openBalOfEntryBase_error', INVALID_BIG_AMOUNT);
		return false;
	}
	$('#openBalOfEntryBaseFormat').val(formatAmount(value, $('#openBalOfEntryBase_curr').val()));
	$('#openBalOfEntryBase').val(value);
	if ($('#entryType').val() == 'N') {
		if ($('#folioRelationship').val() == '1' || $('#folioRelationship').val() == '3')
			setFocusLast('cifNumber');
		else if ($('#folioRelationship').val() == '2')
			setFocusLast('staffId');
		else
			setFocusLast('notes');
	} else {
		setFocusLast('notes');
	}
	return true;
}

function cifNumber_val() {
	var value = $('#cifNumber').val();
	clearError('cifNumber_error');
	$('#cifNumber_desc').html(EMPTY_STRING);
	if ($('#entryType').val() == 'N' && $('#folioRelationship').val() == '1' || $('#folioRelationship').val() == '3') {
		if (isEmpty(value)) {
			setError('cifNumber_error', MANDATORY);
			return false;
		}
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('CIF_NO', $('#cifNumber').val());
		validator.setValue('FOLIO_RELATIONSHIP', $('#folioRelationship').val());
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.cmn.enomosbean');
		validator.setMethod('validateCIFNumber');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('cifNumber_error', validator.getValue(ERROR));
			return false;
		} else {
			$('#cifNumber_desc').html(validator.getValue('NAME'));
		}
	} else {
		if (!isEmpty(value)) {
			setError('cifNumber_error', PBS_FIELD_SH_BE_BLANK);
			return false;
		}
	}
	setFocusLast('notes');
	return true;
}
function staffId_val() {
	var value = $('#staffId').val();
	clearError('staffId_error');
	$('#staffId_desc').html(EMPTY_STRING);
	if ($('#entryType').val() == 'N' && $('#folioRelationship').val() == '2') {
		if (isEmpty(value)) {
			setError('staffId_error', MANDATORY);
			return false;
		}
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('EMP_CODE', $('#staffId').val());
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.cmn.enomosbean');
		validator.setMethod('validateStaffId');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('cifNumber_error', validator.getValue(ERROR));
			return false;
		} else {
			$('#staffId_desc').html(validator.getValue('PERSON_NAME'));
		}
	} else {
		if (!isEmpty(value)) {
			setError('staffId_error', PBS_FIELD_SH_BE_BLANK);
			return false;
		}
	}
	setFocusLast('notes');
	return true;
}

function notes_val() {
	var value = $('#notes').val();
	clearError('notes_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('notes_error', INVALID_REMARKS);
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('notes_error', INVALID_REMARKS);
			return false;
		}
	}
	if ($('#action').val() == MODIFY) {
		if (isEmpty(value)) {
			setError('notes_error', MANDATORY);
			return false;
		}
	}
	setFocusOnSubmit();
	return true;
}