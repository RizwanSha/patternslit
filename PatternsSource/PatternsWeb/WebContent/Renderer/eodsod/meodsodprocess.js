var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MEODSODPROCESS';
function init() {
	refreshQueryGrid();
	refreshTBAGrid();
	if ($('#action').val() == ADD) {
		$('#enabled').prop('disabled', true);
		setCheckbox('enabled', YES);
	}
	}
function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'MEODSODPROCESS_MAIN', queryGrid);
}
function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}

function clearFields() {
	$('#processId').val(EMPTY_STRING);
	$('#processId_error').html(EMPTY_STRING);
	$('#processId_desc').html(EMPTY_STRING);
	clearNonPKFields();
}

function clearNonPKFields() {
	$('#description').val(EMPTY_STRING);
	$('#description_error').html(EMPTY_STRING);
	$('#routine').val(EMPTY_STRING);
	$('#routine_error').html(EMPTY_STRING);
	$('#className').val(EMPTY_STRING);
	$('#className_error').html(EMPTY_STRING);
	setCheckbox('runAtEOD', NO);
	$('#runAtEOD_error').html(EMPTY_STRING);
	setCheckbox('runAtSOD', NO);
	$('#runAtSOD_error').html(EMPTY_STRING);
	$('#runFreq').selectedIndex = 0;
	$('#runFreq_error').html(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
}
function doclearfields(id) {
	switch (id) {
	case 'processId':
		if (isEmpty($('#processId').val())) {
			$('#processId_error').html(EMPTY_STRING);
			$('#processId_desc').html(EMPTY_STRING);
			clearNonPKFields();
			break;
		}
	}
}
function validate(id) {
	switch (id) {
	case 'processId':
		processId_val();
		break;
	case 'description':
		description_val();
		break;
	case 'routine':
		routine_val();
		break;
	case 'className':
		className_val();
		break;
	case 'runAtEOD':
		runAtEOD_val();
		break;
	case 'runAtSOD':
		runAtSOD_val();
		break;
	case 'runFreq':
		runFreq_val();
		break;
	case 'enabled':
		enabled_val();
		break;
	case 'remarks':
		remarks_val();
		break;
	}

}
function backtrack(id) {
	switch (id) {
	case 'processId':
		setFocusLast('processId');
		break;
	case 'description':
		setFocusLast('processId');
		break;
	case 'routine':
		setFocusLast('description');
		break;
	case 'className':
		setFocusLast('routine');
		break;
	case 'runAtEOD':
		setFocusLast('className');
		break;
	case 'runAtSOD':
		setFocusLast('runAtEOD');
		break;
	case 'runFreq':
		setFocusLast('runAtSOD');
		break;
	case 'enabled':
		setFocusLast('runFreq');
		break;
	case 'remarks':
		if ($('#action').val() == MODIFY) {
			setFocusLast('enabled');
		} else {
			setFocusLast('runFreq');
		}
		break;
	}
}
function doHelp(id) {
	switch (id) {
	case 'processId':
		help(CURRENT_PROGRAM_ID, 'HLP_PROC_ID', $('#processId').val(), EMPTY_STRING, $('#processId'));
		break;
	}
}

function add() {
	setCheckbox('enabled', YES);
	$('#enabled').prop('disabled', true);
	$('#processId').focus();
}
function modify() {
	if ($('#action').val() == MODIFY) {
		$('#enabled').prop('disabled', false);
	} else {
		$('#enabled').prop('disabled', true);
	}
	$('#processId').focus();
}

function loadData() {
	$('#processId').val(validator.getValue('PROCESS_ID'));
	$('#description').val(validator.getValue('PROCESS_NAME'));
	$('#routine').val(validator.getValue('PROCESS_ROUTINE'));
	$('#className').val(validator.getValue('PROCESS_CLASS'));
	setCheckbox('runAtEOD', validator.getValue('PROCESS_ALLOW_EOD'));
	setCheckbox('runAtSOD', validator.getValue('PROCESS_ALLOW_SOD'));
	$('#runFreq').val(validator.getValue('PROCESS_RUN_FREQ'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	resetLoading();
}
function view(source, primaryKey) {
	hideParent('eodsod/qeodsodprocess', source, primaryKey);
	resetLoading();
}
function processId_val() {
	var value = $('#processId').val();
	clearError('processId_error');
	if (isEmpty(value)) {
		setError('processId_error', HMS_MANDATORY);
		return false;
	} else if ($('#action').val() == ADD && !isValidCode(value)) {
		setError('processId_error', HMS_INVALID_FORMAT);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('PROCESS_ID', $('#processId').val());
		validator.setValue(ACTION, $('#action').val());
		validator.setClass('patterns.config.web.forms.eodsod.meodsodprocessbean');
		validator.setMethod('validateProcessId');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('processId_error', validator.getValue(ERROR));
			return false;
		} else {
			if ($('#action').val() == MODIFY) {
				PK_VALUE = $('#processId').val();
				if (!loadPKValues(PK_VALUE, 'processId_error')) {
					return false;
				}
			}
		}
	}
	setFocusLast('description');
	return true;
}
function description_val() {
	var value = $('#description').val();
	clearError('description_error');
	if (isEmpty(value)) {
		setError('description_error', HMS_MANDATORY);
		return false;
	}
	if (!isValidDescription(value)) {
		setError('description_error', HMS_INVALID_DESCRIPTION);
		return false;
	}
	setFocusLast('routine');
	return true;
}
function routine_val() {
	var value = $('#routine').val();
	clearError('routine_error');
	if (!isEmpty(value)) {
		if (!isValidDescription(value)) {
			setError('routine_error', HMS_INVALID_DESCRIPTION);
			return false;
		}
	}
	setFocusLast('className');
	return true;
}
function className_val() {
	var routine = $('#routine').val();
	var classname = $('#className').val();
	clearError('className_error');
	if (isEmpty(routine) && isEmpty(classname)) {
		setError('className_error', HMS_EODSOD_PROC_MAND);
		return false;
	} else if (!isEmpty(routine) && !isEmpty(classname)) {
		setError('className_error', HMS_EODSOD_PROC_MAND);
		return false;
	} else {
		if (!isEmpty(classname)) {
			if (!isValidDescription(classname)) {
				setError('className_error', HMS_INVALID_DESCRIPTION);
				return false;
			}
		}
	}
	setFocusLast('runAtEOD');
	return true;
}
function runAtEOD_val() {
	setFocusLast('runAtSOD');
	return true;
}

function runAtSOD_val() {
	clearError('runAtSOD_error');
	if (!($('#runAtEOD').is(':checked')) && !($('#runAtSOD').is(':checked'))) {
		setError('runAtSOD_error', HMS_RUN_EODSOD_MAND);
		return false;
	}
	setFocusLast('runFreq');
	return true;
}
function runFreq_val() {
	if ($('#action').val() == ADD) {
		setFocusLast('remarks');
	} else {
		setFocusLast('enabled');
	}
	return true;
}

function enabled_val() {
	if ($('#action').val() == MODIFY) {
		setFocusLast('remarks');
	}
	return true;
}
function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', HMS_INVALID_REMARKS);
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', HMS_INVALID_REMARKS);
			return false;
		}
	}
	if ($('#action').val() == MODIFY) {
		if (isEmpty(value)) {
			setError('remarks_error', HMS_MANDATORY);
			return false;
		}
	}
	setFocusOnSubmit();
	return true;
}
