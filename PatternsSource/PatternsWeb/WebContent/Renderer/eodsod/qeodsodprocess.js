var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MEODSODPROCESS';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}
function clearFields() {
	$('#processId').val(EMPTY_STRING);
	$('#processId_error').html(EMPTY_STRING);
	$('#processId_desc').html(EMPTY_STRING);
	clearNonPKFields();
}

function clearNonPKFields() {
	$('#description').val(EMPTY_STRING);
	$('#description_error').html(EMPTY_STRING);
	$('#routine').val(EMPTY_STRING);
	$('#routine_error').html(EMPTY_STRING);
	$('#className').val(EMPTY_STRING);
	$('#className_error').html(EMPTY_STRING);
	setCheckbox('runAtEOD', NO);
	$('#runAtEOD_error').html(EMPTY_STRING);
	setCheckbox('runAtSOD', NO);
	$('#runAtSOD_error').html(EMPTY_STRING);
	$('#runFreq').selectedIndex = 0;
	$('#runFreq_error').html(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
}
function loadData() {
	$('#processId').val(validator.getValue('PROCESS_ID'));
	$('#description').val(validator.getValue('PROCESS_NAME'));
	$('#routine').val(validator.getValue('PROCESS_ROUTINE'));
	$('#className').val(validator.getValue('PROCESS_CLASS'));
	setCheckbox('runAtEOD', validator.getValue('PROCESS_ALLOW_EOD'));
	setCheckbox('runAtSOD', validator.getValue('PROCESS_ALLOW_SOD'));
	$('#runFreq').val(validator.getValue('PROCESS_RUN_FREQ'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadAuditFields(validator);
}