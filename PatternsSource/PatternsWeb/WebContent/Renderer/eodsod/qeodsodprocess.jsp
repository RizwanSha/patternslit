<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.eodsod.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/eodsod/qeodsodprocess.js" />
	</web:dependencies>
	<web:loadPK />
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="qeodsodprocess.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:viewContent id="po_view1">
				<web:dividerBlock>
					<web:sectionBlock width="width-100p" align="left">
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="180px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
										<web:column>
											<web:legend key="meodsodprocess.processId" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:processIdDisplay property="processId" id="processId" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="form.description" var="common" mandatory="true"/>
										</web:column>
										<web:column>
											<type:descriptionDisplay property="description" id="description" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="meodsodprocess.routine" var="program" />
										</web:column>
										<web:column>
											<type:descriptionDisplay property="routine" id="routine" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="meodsodprocess.className" var="program" />
										</web:column>
										<web:column>
											<type:descriptionDisplay property="className" id="className" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="meodsodprocess.runAtEOD" var="program" />
										</web:column>
										<web:column>
											<type:checkboxDisplay property="runAtEOD" id="runAtEOD" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="meodsodprocess.runAtSOD" var="program" />
										</web:column>
										<web:column>
											<type:checkboxDisplay property="runAtSOD" id="runAtSOD" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="meodsodprocess.runFreq" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:comboDisplay property="runFreq" id="runFreq" datasourceid="COMMON_PROC_FREQ" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="form.IsUseEnabled" var="common" />
										</web:column>
										<web:column>
											<type:checkboxDisplay property="enabled" id="enabled" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="form.remarks" var="common" />
										</web:column>
										<web:column>
											<type:remarksDisplay property="remarks" id="remarks" />
										</web:column>
									</web:rowEven>
							</web:table>
						</web:section>
						<web:auditDisplay />
					</web:sectionBlock>
				</web:dividerBlock>
			</web:viewContent>
		</web:viewSubPart>
	</web:viewPart>
</web:fragment>