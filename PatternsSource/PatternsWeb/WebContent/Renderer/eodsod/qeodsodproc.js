var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'IEODSODPROC';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}
function loads() {
	loadGridQuery(CURRENT_PROGRAM_ID, 'IEODSODPROC',ieodsodproc_innerGrid);
}

function clearFields() {
	$('#efftCDate').val(EMPTY_STRING);
	ieodsodproc_innerGrid.clearAll();
}
function loadData() {
	$('#processType').val(validator.getValue('PROCESS_FLAG'));
	$('#efftCDate').val(validator.getValue('EFFT_DATE'));
	loadAuditFields(validator);
	loadGrid();
	
}
function loadGrid() {
	SOURCE_VALUE = _tableType;
	PK_VALUE = _primaryKey;
	if (_tableType == MAIN) {
		loads();
	} else if (_tableType == TBA) {
		loads();
	}
}