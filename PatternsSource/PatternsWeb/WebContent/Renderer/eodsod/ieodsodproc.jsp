<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.eodsod.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/eodsod/ieodsodproc.js" />
	</web:dependencies>
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="ieodsodproc.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/eodsod/ieodsodproc" id="ieodsodproc" method="POST">
				<web:dividerBlock>
					<type:optionBox add="true" modify="true" view="true" />
				</web:dividerBlock>
				<web:viewContent id="po_view1" styleClass="hidden">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="ieodsodproc.processtype" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:combo property="processType" id="processType" datasourceid="IEODSODPROC_PROCESS_FLAG" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="form.effectivedate" var="common" mandatory="true" />
										</web:column>
										<web:column>
											<type:date property="efftCDate" id="efftCDate" lookup="true" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:viewTitle var="program" key="ieodsodproc.linkedprocesses" />
									<web:rowEven>
										<web:column>
											<web:legend key="ieodsodproc.processid" var="program" mandatory="true"/>
										</web:column>
										<web:column>
											<type:processId property="processId" id="processId" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="ieodsodproc.processstate" var="program" mandatory="true"/>
										</web:column>
										<web:column>
											<type:combo property="processState" id="processState" datasourceid="IEODSODPROC_PROCESS_FLAG_S" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:rowEven>
										<web:column>
											<web:gridToolbar gridName="ieodsodproc_innerGrid" cancelFunction="ieodsodproc_cancelGrid" editFunction="ieodsodproc_editGrid" addFunction="ieodsodproc_addGrid" continuousEdit="true" />
											<web:element property="xmlieodsodprocGrid" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:viewContent id="po_view4">
												<web:grid height="240px" width="642px" id="ieodsodproc_innerGrid" src="eodsod/ieodsodproc_innerGrid.xml">
												</web:grid>
											</web:viewContent>
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column span="2">
											<web:submitReset />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
						</web:sectionBlock>
					</web:dividerBlock>
				</web:viewContent>
				<web:element property="command" />
				<web:element property="action" />
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
	<web:queryGrid />
	<web:TBAGrid />
	<web:contextSearch />
</web:fragment>