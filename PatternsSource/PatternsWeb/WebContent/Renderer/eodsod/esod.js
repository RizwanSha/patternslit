var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'ESOD';
var eventID;
function init() {
	var submitallow = true;
	if (!isEmpty($('#processXml').val())) {
		$('#po_view5').removeClass('hidden');
		innerGrid.loadXMLString($('#processXml').val());
		$xml = $($.parseXML($('#processXml').val()));
		($xml).find("row").each(function() {
			try {
				var rowID = $(this).find("cell")[1].textContent;
				if ($(this).find("cell")[3].textContent == 'FAILED') {
					innerGrid.setRowColor(rowID, "red");
				}
			} catch (e) {
			}
		});
	}
	if ($('#sodCompl').val() == '1') {
		setError('currentDate_error', HMS_SOD_ALREADY_DONE);
		alert(HMS_SOD_ALREADY_DONE);
		submitallow = false;
	} else if ($('#eodInProcess').val() == '1') {
		setError('currentDate_error', HMS_EOD_IN_PROGRESS);
		alert(HMS_EOD_IN_PROGRESS);
		submitallow = false;
	} else if ($('#eodCompl').val() == '0') {
		setError('currentDate_error', HMS_EOD_NOT_DONE);
		alert(HMS_EOD_NOT_DONE);
		submitallow = false;
	} else if ($('#sodInProcess').val() == '1') {
		showMessage(getProgramID(), Message.PROGRESS);
		eventID = setInterval(function() {
			processStatusLoad();
		}, 5000);
		submitallow = false;
	}

	if (submitallow) {
		$('#sodSubmit').prop('disabled', false);
	} else {
		$('#sodSubmit').prop('disabled', true);
	}
	$('#currentDate').prop('readonly', true);
	$('#currentDate_pic').prop('disabled', true);
	$('#previousDate').prop('readonly', true);
	$('#previousDate_pic').prop('disabled', true);
	$('#nextDate').prop('readonly', true);
	$('#nextDate_pic').prop('disabled', true);
	$('#nextDate').val($('#currentDate').val());
	var dateInstance = moment($('#currentDate').val(), SCRIPT_DATE_FORMAT);
	dateInstance.add("days", 1).toString();
	dateInstance = moment(dateInstance).format(SCRIPT_DATE_FORMAT);
	$('#nextDate').val(dateInstance);
	$('#action').val(ADD);
	setFocusLast('remarks');
}

function clearFields() {
}

function clearFormFields() {
	innerGrid.clearAll();
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
	// $('#eodSubmit').prop('disabled', false);
	$('#po_view5').addClass('hidden');
}

function validate(id) {
	switch (id) {
	case 'currentDate':
		currentDate_val();
		break;
	case 'previousDate':
		previousDate_val();
		break;
	case 'nextDate':
		nextDate_val();
		break;
	case 'remarks':
		remarks_val();
		break;
	}
}

function currentDate_val() {
	var value = $('#currentDate').val();
	clearError('currentDate_error');
	if (isEmpty(value)) {
		setError('currentDate_error', MANDATORY);
		return false;
	}
	if (!isDate(value)) {
		setError('currentDate_error', INVALID_DATE);
		return false;
	}
	return true;
}
function previousDate_val() {
	var value = $('#previousDate').val();
	clearError('previousDate_error');
	if (isEmpty(value)) {
		setError('previousDate_error', MANDATORY);
		return false;
	}
	if (!isDate(value)) {
		setError('previousDate_error', INVALID_DATE);
		return false;
	}
	return true;
}
function nextDate_val() {
	var value = $('#nextDate').val();
	clearError('nextDate_error');
	if (isEmpty(value)) {
		setError('nextDate_error', MANDATORY);
		return false;
	}
	if (!isDate(value)) {
		setError('nextDate_error', INVALID_DATE);
		return false;
	}
	return true;
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (isEmpty(value)) {
		setError('remarks_error', MANDATORY);
		return false;
	}
	if (!isValidRemarks(value)) {
		setError('remarks_error', INVALID_REMARKS);
		return false;
	}
	return true;
}

function revalidate() {
	errors = 0;
	if (!currentDate_val()) {
		errors++;
	}
	if (!previousDate_val()) {
		errors++;
	}
	if (!nextDate_val()) {
		errors++;
	}
	if (!remarks_val()) {
		errors++;
	}
	if (errors == 0) {
		$('#sodSubmit').prop('disabled', true);
		showMessage(getProgramID(), Message.PROGRESS);
		// innerGrid.clearAll();
		validator.clearMap();
		validator.setClass('patterns.config.web.forms.eodsod.esodbean');
		validator.setMethod('processAction');
		validator.setValue("REMARKS", $('#remarks').val());
		validator.setValue("CURRENT_DATE", $('#currentDate').val());
		validator.sendAndReceiveAsync(showStatus);

	}else{
		return false;
	}
}

function showStatus() {
	if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		$('#runSerial').val(validator.getValue('SERIAL'));
		status = validator.getValue('STATUS');
		if (status == 'P') {
			$('#po_view5').removeClass('hidden');
			if (isEmpty($('#processXml').val())) {
				innerGrid.loadXMLString(validator.getValue(RESULT_XML));
			}
			eventID = setInterval(function() {
				processStatusLoad();
			}, 5000);
		} else {
			hideMessage(getProgramID());
			alert(HMS_SOD_SUCCESS);
			redirectLink(getBasePath() + ELOGOUT_JSP);
		}
		return true;
	} else {
		hideMessage(getProgramID());
		alert(validator.getValue(ERROR));
		return false;
	}
}
function processStatusLoad() {
	validator.clearMap();
	validator.setMtm(false);
	validator.setClass('patterns.config.web.forms.eodsod.esodbean');
	validator.setMethod('loadProcessStatus');
	validator.setValue('SERIAL', $('#runSerial').val());
	validator.setValue('CBD', $('#currentDate').val());
	validator.sendAndReceive();
	if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		var xmlString = validator.getValue(RESULT_XML);
		$xml = $($.parseXML(xmlString));
		($xml)
				.find("row")
				.each(
						function() {
							try {
								var rowID = $(this).find("cell")[1].textContent;
								innerGrid.cells(rowID, 3).setValue(
										$(this).find("cell")[3].textContent);
								innerGrid.cells(rowID, 4).setValue(
										$(this).find("cell")[4].textContent);
								innerGrid.cells(rowID, 5).setValue(
										$(this).find("cell")[5].textContent);
								if ($(this).find("cell")[3].textContent == 'FAILED') {
									innerGrid.setRowColor(rowID, "red");
								} else {
									var sl = innerGrid.cells(rowID, 0)
											.getValue();
									innerGrid.deleteRow(rowID);
									var processName = $(this).find("cell")[2].textContent;
									var status = $(this).find("cell")[3].textContent;
									var startTime = $(this).find("cell")[4].textContent;
									var endTime = $(this).find("cell")[5].textContent;
									var values = [ sl, rowID, processName,
											status, startTime, endTime ];
									innerGrid.addRow(rowID, values);
								}
							} catch (e) {
							}
						});
	}
	if (validator.getValue("STATUS") == 'S') {
		hideMessage(getProgramID());
		clearInterval(eventID);
		alert(HMS_SOD_SUCCESS);
		redirectLink(getBasePath() + ELOGOUT_JSP);
	} else if (validator.getValue("STATUS") == 'F') {
		hideMessage(getProgramID());
		clearInterval(eventID);
		alert(HMS_SOD_FAILURE);
		$('#sodSubmit').prop('disabled', false);
	}
}