var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'IEODSODPROC';

function init() {
	refreshTBAGrid();
	refreshQueryGrid();
	if (_redisplay) {
		ieodsodproc_innerGrid.clearAll();
		if (!isEmpty($('#xmlieodsodprocGrid').val())) {
			ieodsodproc_innerGrid.loadXMLString($('#xmlieodsodprocGrid').val());
		}
	}
}
function loadGrid() {
	loadGridQuery(CURRENT_PROGRAM_ID, 'IEODSODPROC', ieodsodproc_innerGrid);
}
function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'IEODSODPROC_MAIN');
}
function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}
function doHelp(id) {
	switch (id) {
	case 'efftCDate':
		help('EODSOD', 'HLP_IEODSOD_EFF_DT', $('#efftCDate').val(), $('#processType').val(), $('#efftCDate'));
		break;
	case 'processId':
		if ($('#processType').val() == "E") {
			help('EODSOD', 'HLP_EODSODPROCESS_E', $('#processId').val(), EMPTY_STRING, $('#processId'));
		} else if ($('#processType').val() == "S") {
			help('EODSOD', 'HLP_EODSODPROCESS_S', $('#processId').val(), EMPTY_STRING, $('#processId'));
		}
		break;

	}
}

function clearFields() {
	$('#processType').val($("#processType option:first-child").val());
	$('#processType_error').html(EMPTY_STRING);
	$('#efftCDate').val(EMPTY_STRING);
	$('#efftCDate_error').html(EMPTY_STRING);
	clearNonPKFields();
}

function clearNonPKFields() {
	ieodsodproc_clearGridFields();
	ieodsodproc_innerGrid.clearAll();
}
function add() {
	$('#processType').focus();
	$('#efftCDate').val(getCBD());
	$('#efftCDate_look').prop('disabled', true);

}
function modify() {
	$('#processType').focus();
	$('#efftCDate_look').prop('disabled', false);
	$('#efftCDate').html(EMPTY_STRING);
}

function doclearfields(id) {
	switch (id) {
	case 'processType':
		if (isEmpty($('#processType').val())) {
			$('#processType_error').html(EMPTY_STRING);
			$('#processType_desc').html(EMPTY_STRING);
			break;
		}
	case 'efftCDate':
		if (isEmpty($('#efftCDate').val())) {
			$('#efftCDate_error').html(EMPTY_STRING);
			$('#efftCDate_desc').html(EMPTY_STRING);
			clearNonPKFields();
			break;
		}
	}
}

function loadData() {
	$('#processType').val(validator.getValue('PROCESS_FLAG'));
	$('#efftCDate').val(validator.getValue('EFFT_DATE'));
	loadGrid();
	resetLoading();
}
function view(source, primaryKey) {
	hideParent('eodsod/qeodsodproc', source, primaryKey);
	resetLoading();
}

function validate(id) {
	valMode = true;
	switch (id) {
	case 'processType':
		processType_val(valMode);
		break;
	case 'efftCDate':
		efftCDate_val(valMode);
		break;
	case 'processId':
		processId_val(valMode);
		break;
	case 'processState':
		processState_val(valMode);
		break;

	}
}
function backtrack(id) {
	switch (id) {
	case 'processType':
		setFocusLast('processType');
		break;
	case 'efftCDate':
		setFocusLast('processType');
		break;
	case 'processId':
		setFocusLast('efftCDate');
		break;
	case 'processState':
		setFocusLast('processId');
		break;
	}
}
function gridExit(id) {
	switch (id) {
	case 'processId':
		$('#xmlieodsodprocGrid').val(ieodsodproc_innerGrid.serialize());
		setFocusOnSubmit();
		break;
	}
}
function gridDeleteRow(id) {
	switch (id) {
	case 'processId':
		deleteGridRecord(ieodsodproc_innerGrid);
		break;
	}
}

function processType_val(valMode) {
	var value = $('#processType').val();
	clearError('processType_error');
	if (isEmpty(value)) {
		setError('processType_error', HMS_MANDATORY);
		return false;
	}
	setFocusLast('efftCDate');
	return true;
}

function efftCDate_val(valMode) {
	var value = $('#efftCDate').val();
	clearError('efftCDate_error');
	if (!isValidEffectiveDate(value, 'efftCDate_error')) {
		return false;
	} else {
		if ($('#action').val() == ADD) {
			validator.clearMap();
			validator.setMtm(false);
			validator.setValue('PROCESS_FLAG', $('#processType').val());
			validator.setValue('EFFT_DATE', $('#efftCDate').val());
			validator.setValue('ACTION', $('#action').val());
			validator.setClass('patterns.config.web.forms.eodsod.ieodsodprocbean');
			validator.setMethod('validateEffectiveDate');
			validator.sendAndReceive();
			if (validator.getValue(ERROR) != EMPTY_STRING) {
				setError('efftCDate_error', validator.getValue(ERROR));
				return false;
			}
		} else {
			PK_VALUE = getEntityCode() + PK_SEPERATOR + $('#processType').val() + PK_SEPERATOR + getTBADateFormat($('#efftCDate').val());
			if (!loadPKValues(PK_VALUE, 'efftCDate_error')) {
				return false;
			}
		}
	}
	setFocusLast('processId');
	return true;
}

function processId_val(valMode) {
	var value = $('#processId').val();
	clearError('processId_error');
	if (isEmpty(value)) {
		setError('processId_error', HMS_MANDATORY);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('PROCESS_FLAG', $('#processType').val());
		validator.setValue('PROCESS_ID', $('#processId').val());
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.eodsod.ieodsodprocbean');
		validator.setMethod('validateprocessId');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('processId_error', validator.getValue(ERROR));
			return false;
		} else {
			$('#processId_desc').html(validator.getValue('PROCESS_NAME'));

		}
	}
	setFocusLast('processState');
	return true;
}

function processState_val(valMode) {
	var value = $('#processState').val();
	clearError('processState_error');
	if (isEmpty(value)) {
		setError('processState_error', HMS_MANDATORY);
		return false;
	}
	if (valMode) {
		setFocusLastOnGridSubmit('ieodsodproc_innerGrid');
	}
	return true;

}

function ieodsodproc_gridDuplicateCheck(editMode, editRowId) {
	var currentValue = [ $('#processId').val() ];
	return _grid_duplicate_check(ieodsodproc_innerGrid, currentValue, [ 1 ]);
}

function ieodsodproc_clearGridFields() {
	$('#processId').val(EMPTY_STRING);
	$('#processId_desc').html(EMPTY_STRING);
	$('#processId_error').html(EMPTY_STRING);
	$('#processState').val($("#processState option:first-child").val());
	$('#processState_error').html(EMPTY_STRING);
}
function ieodsodproc_editGrid(rowId) {
	$('#processId').val(ieodsodproc_innerGrid.cells(rowId, 1).getValue());
	$('#processId_desc').html(ieodsodproc_innerGrid.cells(rowId, 2).getValue());
	$('#processState').val(ieodsodproc_innerGrid.cells(rowId, 4).getValue());
	$('#processId').focus();
}
function revalidate() {

	ieodsodproc_revaildateGrid();

}
function ieodsodproc_revaildateGrid() {
	if (ieodsodproc_innerGrid.getRowsNum() > 0) {
		$('#xmlieodsodprocGrid').val(ieodsodproc_innerGrid.serialize());
		$('#processId_error').html(EMPTY_STRING);
	} else {
		ieodsodproc_innerGrid.clearAll();
		$('#xmlieodsodprocGrid').val(ieodsodproc_innerGrid.serialize());
		setError('processId_error', HMS_ADD_ATLEAST_ONE_ROW);
		errors++;
	}

}
function ieodsodproc_addGrid(editMode, editRowId) {

	if (processId_val(false) && processState_val(false)) {
		if (ieodsodproc_gridDuplicateCheck(editMode, editRowId)) {
			if ($('#processState').val() == "A") {
				processStatedesc = 'A-Abort on failure';
			} else if ($('#processState').val() == "P") {
				processStatedesc = 'P-Proceed even on failure';
			}
			var field_values = [ 'false', $('#processId').val(), $('#processId_desc').html(), processStatedesc, $('#processState').val() ];
			_grid_updateRow(ieodsodproc_innerGrid, field_values);
			ieodsodproc_clearGridFields();
			$('#processId').focus();
			return true;
		} else {
			setError('processId_error', HMS_DUPLICATE_DATA);
			$('#processId').focus();
			return false;
		}
	} else {

		$('#processId').focus();
		return false;
	}
}

function ieodsodproc_cancelGrid(rowId) {
	ieodsodproc_clearGridFields();
}
