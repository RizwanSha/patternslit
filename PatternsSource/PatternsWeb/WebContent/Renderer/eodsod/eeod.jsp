<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<%@taglib prefix="message" tagdir="/WEB-INF/tags/message"%>
<web:bundle baseName="patterns.config.web.forms.eodsod.Messages"
	var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/eodsod/eeod.js" />
	</web:dependencies>
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="eeod.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/eodsod/eeod" id="eeod" method="POST">
				<web:viewContent id="po_view1">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="eeod.currentdate" var="program" />
										</web:column>
										<web:column>
											<type:date property="currentDate" id="currentDate" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="eeod.previousdate" var="program" />
										</web:column>
										<web:column>
											<type:date property="previousDate" id="previousDate" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="eeod.nextdate" var="program" />
										</web:column>
										<web:column>
											<type:date property="nextDate" id="nextDate" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="form.remarks" var="common" mandatory="true" />
										</web:column>
										<web:column>
											<type:remarks property="remarks" id="remarks" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="eeod.confirm" var="program" />
										</web:column>
										<web:column>
											<type:checkbox property="confirmClose" id="confirmClose" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<message:messageBox id="eeod" />
							</web:section>
						</web:sectionBlock>
					</web:dividerBlock>
				</web:viewContent>
				<web:viewContent id="po_view5" styleClass="hidden">
					<web:section>
						<web:viewTitle key="eeod.grid" var="program" />
						<web:table>
							<web:rowOdd>
								<web:column>
									<web:grid height="200px" width="802px" id="innerGrid"
										src="eodsod/eeod_innerGrid.xml">
									</web:grid>
								</web:column>
							</web:rowOdd>
						</web:table>
					</web:section>
				</web:viewContent>
				<web:section>
					<web:table>
						<web:rowEven>
							<web:column>
								<type:button key="form.submit" id="eodSubmit" var="common"
									onclick="revalidate()" />
								<type:button key="form.cancel" id="clear" var="common"
									onclick="clearFormFields()" />
							</web:column>
						</web:rowEven>
					</web:table>
				</web:section>
				<web:element property="command" />
				<web:element property="action" />
				<web:element property="eodInProcess" />
				<web:element property="eodCompl" />
				<web:element property="sodInProcess" />
				<web:element property="sodCompl" />
				<web:element property="processXml" />
				<web:element property="runSerial" />
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
</web:fragment>