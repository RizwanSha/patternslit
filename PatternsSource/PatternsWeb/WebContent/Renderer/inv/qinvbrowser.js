var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'QINVBROWSER';
function init() {
	validateDateRangeParam();
	var offcanvasVisible = $('.offcanvas-menu').is(':visible');
	if (offcanvasVisible)
		slideMenu();
	clearFields();
}

function clearFields() {
	setFocusLast('fromDate');
	$('#fromDate').val(EMPTY_STRING);
	$('#uptoDate').val(EMPTY_STRING);
	clearNonPkFields();
}
function clearNonPkFields(){
	$('#lesseeCode').val(EMPTY_STRING);
	$('#lesseeCode_desc').html(EMPTY_STRING);
	$('#customerId').val(EMPTY_STRING);
	$('#customerId_desc').html(EMPTY_STRING);
	$('#productCode').val(EMPTY_STRING);
	$('#productCode_desc').html(EMPTY_STRING);
	$('#dueFor').val('0');
	$('#dueFor_error').html(EMPTY_STRING);
	$('#status').val('B');
	$('#status_error').html(EMPTY_STRING);
	clearFieldsError();
	qinvbrowser_innerGrid.clearAll();
	lesseeDependFields();
}

function clearFieldsError() {
	$('#fromDate_error').html(EMPTY_STRING);
	$('#uptoDate_error').html(EMPTY_STRING);
	$('#lesseeCode_error').html(EMPTY_STRING);
	$('#customerId_error').html(EMPTY_STRING);
	$('#productCode_error').html(EMPTY_STRING);
}
function doclearfields(id) {
	switch (id) {
	case 'fromDate':
		if(isEmpty($('#fromDate').val())){
			clearFields();
		}
		break;
	case 'uptoDate':
		if(isEmpty($('#uptoDate').val())){
			clearNonPkFields();
			$('#uptoDate_error').html(EMPTY_STRING);
		}
		break;
	case 'lesseeCode':
		if (isEmpty($('#lesseeCode').val())) {
			$('#customerId').val(EMPTY_STRING);
			$('#customerId_desc').html(EMPTY_STRING);
			$('#customerId_error').html(EMPTY_STRING);
			$('#productCode').val(EMPTY_STRING);
			$('#productCode_desc').html(EMPTY_STRING);
			$('#productCode_error').html(EMPTY_STRING);
			break;
		}
	}
}

function lesseeDependFields() {
	if (isEmpty($('#lesseeCode').val())) {
		$('#customerId').prop('readonly', false);
		$('#customerId_pic').prop('disabled', false);
		$('#productCode').prop('readonly', false);
		$('#productCode_pic').prop('disabled', false);
	} else {
		$('#customerId').prop('readonly', true);
		$('#customerId_pic').prop('disabled', true);
		$('#productCode').prop('readonly', true);
		$('#productCode_pic').prop('disabled', true);
	}
}

function doHelp(id) {
	switch (id) {
	case 'lesseeCode':
		help('COMMON', 'HLP_DEBTOR_CODE', $('#lesseeCode').val(), EMPTY_STRING, $('#lesseeCode'));
		break;
	case 'customerId':
		help('COMMON', 'HLP_CUSTOMER_ID', $('#customerId').val(), EMPTY_STRING, $('#customerId'));
		break;
	case 'productCode':
		help('COMMON', 'HLP_LEASE_PROD_CODE', $('#productCode').val(), EMPTY_STRING, $('#productCode'));
		break;
	}
}

function backtrack(id) {
	switch (id) {
	case 'fromDate':
		setFocusLast('fromDate');
		break;
	case 'uptoDate':
		setFocusLast('fromDate');
		break;
	case 'lesseeCode':
		setFocusLast('uptoDate');
		break;
	case 'customerId':
		setFocusLast('lesseeCode');
		break;
	case 'productCode':
		setFocusLast('customerId');
		break;
	case 'dueFor':
		setFocusLast('productCode');
		break;
	case 'status':
		setFocusLast('dueFor');
		break;
	case 'submit':
		setFocusLast('status');
		break;
	}
}

function validate(id) {
	switch (id) {
	case 'fromDate':
		fromDate_val();
		break;
	case 'uptoDate':
		uptoDate_val();
		break;
	case 'lesseeCode':
		lesseeCode_val();
		break;
	case 'customerId':
		customerId_val();
		break;
	case 'productCode':
		productCode_val();
		break;
	case 'dueFor':
		dueFor_val();
		break;
	case 'status':
		status_val();
		break;
	}
}

function validateDateRangeParam() {
	if (isEmpty($('#fromDateLowerLimit').val()) || isEmpty($('#nofMonthAllowed').val()))
		setError('fromDate_error', TC_PGM_DATE_RANGE_NOT_CONFIG);
}

function fromDate_val() {
	var value = $('#fromDate').val();
	clearError('fromDate_error');
	if (isEmpty(value)) {
		setError('fromDate_error', MANDATORY);
		return false;
	}
	if (!isDate(value)) {
		setError('fromDate_error', INVALID_DATE);
		return false;
	}
	if (isDateLesser(value, $('#fromDateLowerLimit').val())) {
		setError('fromDate_error', TC_DATE_SH_GE_LOWER_LIMIT_DATE + $('#fromDateLowerLimit').val());
		return false;
	}
	setFocusLast('uptoDate');
	return true;
}

function uptoDate_val() {
	var frmDate = $('#fromDate').val();
	var value = $('#uptoDate').val();
	clearError('uptoDate_error');
	if (isEmpty(value)) {
		setError('uptoDate_error', MANDATORY);
		return false;
	}
	if (!isDate(value)) {
		setError('uptoDate_error', INVALID_DATE);
		return false;
	}
	if (!isDateGreaterEqual(value, frmDate)) {
		setError('uptoDate_error', DATE_GECBD);
		return false;
	}
	var nofMonths = moment($('#uptoDate').val(), SCRIPT_DATE_FORMAT, true).diff(moment($('#fromDate').val(), SCRIPT_DATE_FORMAT, true), 'months');
	if (nofMonths > parseInt($('#nofMonthAllowed').val())) {
		setError('uptoDate_error', formatMessage(TC_RANGE_GR_MONTHS, [ $('#nofMonthAllowed').val() ]));
		return false;
	}
	setFocusLast('lesseeCode');
	return true;
}
function lesseeCode_val() {
	var value = $('#lesseeCode').val();
	$('#lesseeCode_desc').html(EMPTY_STRING);
	clearError('lesseeCode_error');
	if (!isEmpty(value)) {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('SUNDRY_DB_AC', $('#lesseeCode').val());
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.inv.qinvbrowserbean');
		validator.setMethod('validateLesseeCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('lesseeCode_error', validator.getValue(ERROR));
			return false;
		} else {
			$('#customerId').val(EMPTY_STRING);
			$('#customerId_desc').html(EMPTY_STRING);
			$('#customerId_error').html(EMPTY_STRING);
			$('#productCode').val(EMPTY_STRING);
			$('#productCode_desc').html(EMPTY_STRING);
			$('#productCode_error').html(EMPTY_STRING);
			$('#productCode').val(validator.getValue('LEASE_PRODUCT_CODE'));
			$('#productCode_desc').html(validator.getValue('LEASE_PRODUCT_CODE_DESC'));
			$('#customerId').val(validator.getValue('CUSTOMER_ID'));
			$('#customerId_desc').html(validator.getValue('CUSTOMER_NAME'));
			$('#customerId').prop('readonly', true);
			$('#customerId_pic').prop('disabled', true);
			$('#productCode').prop('readonly', true);
			$('#productCode_pic').prop('disabled', true);
		}
	} else {
		$('#customerId').prop('readonly', false);
		$('#customerId_pic').prop('disabled', false);
		$('#productCode').prop('readonly', false);
		$('#productCode_pic').prop('disabled', false);
	}
	if ($('#customerId').prop('readOnly'))
		setFocusLast('dueFor');
	else
		setFocusLast('customerId');
	return true;
}
function customerId_val() {
	var value = $('#customerId').val();
	clearError('customerId_error');
	$('#customerId_desc').html(EMPTY_STRING);
	if (!isEmpty(value)) {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('CUSTOMER_ID', $('#customerId').val());
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.inv.qinvbrowserbean');
		validator.setMethod('validateCustomerId');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('customerId_error', validator.getValue(ERROR));
			return false;
		} else {
			$('#customerId_desc').html(validator.getValue('CUSTOMER_NAME'));
		}
	}
	setFocusLast('productCode');
	return true;
}
function productCode_val() {
	var value = $('#productCode').val();
	clearError('productCode_error');
	$('#productCode_desc').html(EMPTY_STRING);
	if (!isEmpty(value)) {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('LEASE_PRODUCT_CODE', $('#productCode').val());
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.inv.qinvbrowserbean');
		validator.setMethod('validateProductCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('productCode_error', validator.getValue(ERROR));
			return false;
		} else {
			$('#productCode_desc').html(validator.getValue('DESCRIPTION'));
		}
	}
	setFocusLast('dueFor');
	return true;
}
function dueFor_val() {
	var value = $('#dueFor').val();
	clearError('dueFor_error');
	if (isEmpty(value)) {
		$('#dueFor').val('0');
		value = $('#dueFor').val();
	}
	setFocusLast('status');
	return true;
}
function status_val() {
	var value = $('#status').val();
	clearError('status_error');
	if (isEmpty(value)) {
		$('#status').val('B');
		value = $('#status').val();
	}
	setFocusLast('submit');
	return true;
}

function loadGridRecords() {
	clearFieldsError();
	showMessage(getProgramID(), Message.PROGRESS);
	qinvbrowser_innerGrid.clearAll();
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('FROM_DATE', $('#fromDate').val());
	validator.setValue('UPTO_DATE', $('#uptoDate').val());
	validator.setValue('SUNDRY_DB_AC', $('#lesseeCode').val());
	validator.setValue('LEASE_PRODUCT_CODE', $('#productCode').val());
	validator.setValue('CUSTOMER_ID', $('#customerId').val());
	validator.setValue('DUE_FOR', $('#dueFor').val());
	validator.setValue('STATUS', $('#status').val());
	validator.setValue('FROMDATE_LOWER_LIMIT', $('#fromDateLowerLimit').val());
	validator.setValue('NO_OF_MONTH_ALLOWED', $('#nofMonthAllowed').val());
	validator.setClass('patterns.config.web.forms.inv.qinvbrowserbean');
	validator.setMethod('doLoadGrid');
	validator.sendAndReceiveAsync(processDownloadResult);
	qinvbrowser_innerGrid.clearAll();
}

function processDownloadResult() {
	if (validator.getValue(ERROR) != null && validator.getValue(ERROR) != EMPTY_STRING) {
		setError(validator.getValue(ERROR_FIELD) + '_error', validator.getValue(ERROR));
		setFocusLast(validator.getValue(ERROR_FIELD));
		hideMessage(getProgramID());
		return false;
	} else if (validator.getValue(RESULT) == DATA_UNAVAILABLE) {
		showMessage(getProgramID(), Message.WARN, NO_RECORD);
		return false;
	} else if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		hideMessage(getProgramID());
		xmlString = validator.getValue(RESULT_XML);
		qinvbrowser_innerGrid.loadXMLString(xmlString);
	}
	return true;
}

function viewHideFilters() {
	if ($('#filters_view').hasClass('hidden')) {
		$('#filters_view').removeClass('hidden');
		$("#filter").prop('value', TC_HIDE_FILTERS);
	} else {
		$('#filters_view').addClass('hidden');
		$("#filter").prop('value', TC_VIEW_FILTERS);
	}
}

function viewLease(pk) {
	if (!isEmpty(pk)) {
		var primaryKey = getEntityCode() + PK_SEPERATOR + pk;
		var params = PK + '=' + primaryKey + '&' + SOURCE + '=' + MAIN;
		// alert(params);
		win = showWindow('qlease', getBasePath() + 'Renderer/lss/qlease.jsp', '', params, true, false, false);
	}
}

function viewCustomerReg(customerId) {
	if (!isEmpty(customerId)) {
		var primaryKey = getEntityCode() + PK_SEPERATOR + customerId;
		var params = PK + '=' + primaryKey + '&' + SOURCE + '=' + MAIN;
		// alert(params);
		win = showWindow('qcustomerreg', getBasePath() + 'Renderer/reg/qcustomerreg.jsp', '', params, true, false, false);
	}
}

function invoiceReference(invoiceReference) {
	if (!isEmpty(invoiceReference)) {
		var primaryKey = getEntityCode() + PK_SEPERATOR + invoiceReference;
		var params = PK + '=' + primaryKey + '&' + SOURCE + '=' + MAIN;
		// alert(params);
		win = showWindow('qinvcompview', getBasePath() + 'Renderer/inv/qinvcompview.jsp', '', params, true, false, false);
	}
}

function invoiceNumber(invoiceNumber) {
	if (!isEmpty(invoiceNumber)) {
		var primaryKey = getEntityCode() + PK_SEPERATOR + replaceall(invoiceNumber, '/', PK_SEPERATOR);
		var params = PK + '=' + primaryKey + '&' + SOURCE + '=' + MAIN;
		win = showWindow('qinvoices', getBasePath() + 'Renderer/inv/qinvoices.jsp', '', params, true, false, false);
	}
}
