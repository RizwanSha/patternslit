var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'IINVSIGNPOLICY';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}

function clearFields() {
	$('#lesseBranchCode').val(EMPTY_STRING);
	$('#lesseBranchCode_desc').html(EMPTY_STRING);
	$('#effectiveDate').val(EMPTY_STRING);
	clearNonPKFields();
}

function clearNonPKFields() {
	$('#firstSign').val(EMPTY_STRING);
	$('#firstSign_desc').html(EMPTY_STRING);
	$('#secondSign').val(EMPTY_STRING);
	$('#secondSign_desc').html(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
}

function loadData() {
	$('#lesseBranchCode').val(validator.getValue('BRANCH_CODE'));
	$('#lesseBranchCode_desc').html(validator.getValue('F1_BRANCH_NAME'));
	$('#effectiveDate').val(validator.getValue('EFF_DATE'));
	$('#firstSign').val(validator.getValue('FIRST_SIGN_STAFF_CODE'));
	$('#firstSign_desc').html(validator.getValue('F2_NAME'));
	$('#secondSign').val(validator.getValue('SECOND_SIGN_STAFF_CODE'));
	$('#secondSign_desc').html(validator.getValue('F3_NAME'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadAuditFields(validator);
	resetLoading();
}

