validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MINVTEMPLATE';
var tabSize = [ '273px', '200px', '470px', '500px' ];
var fldtype = EMPTY_STRING;
var fieldTypeValue = EMPTY_STRING;
var fieldTypeValue2 = EMPTY_STRING;
var gridAnexValue = EMPTY_STRING;
var groupTypeValue = EMPTY_STRING;
function init() {
	$('#minvtemplate_innerGrid_ins').remove();
	refreshTBAGrid();
	refreshQueryGrid();
	if (_redisplay) {
		if ($('#templconsol').is(':checked')) {
			if (!isEmpty($('#xmlMinvtemplateGrid').val())) {
				minvtemplate_innerGrid.loadXMLString($('#xmlMinvtemplateGrid').val());
			}
		}
		if (!isEmpty($('#xmlMinvtemplateGrid2').val())) {
			minvtemplate_innerGrid2.loadXMLString($('#xmlMinvtemplateGrid2').val());
		}
		if ($('#action').val() == ADD) {
			$('#enabled').prop('disabled', true);
			setCheckbox('enabled', YES);
		}
		makeDisable();
		if (!$('#templconsol').is(':checked')) {
			template.tabs('template_1').disable();
			template.tabs('template_2').disable();
		} else {
			template.tabs('template_1').enable();
			template.tabs('template_2').enable();
		}
	}
}

function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'MINVTEMPLATE_MAIN', queryGrid);
}

function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}

function loadGrid() {
	loadGridQuery(CURRENT_PROGRAM_ID, 'MINVTEMPLATE_GRID', minvtemplate_innerGrid);
}

function loadGrid2() {
	loadGridQuery(CURRENT_PROGRAM_ID, 'MINVTEMPLATE_GRID2', minvtemplate_innerGrid2);
}

function clearFields() {
	$('#invTempCode').val(EMPTY_STRING);
	$('#invTempCode_error').html(EMPTY_STRING);
	$('#invTempCode_desc').html(EMPTY_STRING);
	clearNonPKFields();
}

function clearNonPKFields() {
	$('#transactionErrors').html(EMPTY_STRING);
	$('#transactionErrors_error').html(EMPTY_STRING);
	$('#transactionErrors').html(EMPTY_STRING);
	$('#description').val(EMPTY_STRING);
	$('#description_error').html(EMPTY_STRING);
	$('#conciseDescription').val(EMPTY_STRING);
	$('#conciseDescription_error').html(EMPTY_STRING);
	setCheckbox('templconsol', NO);
	setCheckbox('invtempl', NO);
	setCheckbox('sign', NO);
	setCheckbox('printleasesch', NO);
	setCheckbox('printleasedeal', NO);
	setCheckbox('printdistamt', NO);

	$('#groupType').val($("#groupType option:first-child").val());
	$('#groupType_error').html(EMPTY_STRING);

	$('#vatCst').val($("#vatCst option:first-child").val());
	$('#vatCst_error').html(EMPTY_STRING);
	setCheckbox('serviceTax', NO);
	setCheckbox('shadedInvoice', NO);
	setCheckbox('generateDate', NO);
	setCheckbox('captionTnc', NO);
	setCheckbox('eoaPrint', NO);
	setCheckbox('consoAnnex', NO);
	setCheckbox('frequency', NO);
	setCheckbox('disbursalAmt', NO);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
	clearTab2Fields();
	clearTab3Fields();
	clearTab4Fields();
}

function clearTab2Fields() {
	template.tabs('template_1').disable();
	setCheckbox('consoGrid', NO);
	$('#consoGrid_error').html(EMPTY_STRING);
	setCheckbox('grpreqd', NO);
	$('#grpreqd_error').html(EMPTY_STRING);
	setCheckbox('listanex', NO);
	$('#listanex_error').html(EMPTY_STRING);
	setCheckbox('grpreqdanex', NO);
	$('#grpreqdanex_error').html(EMPTY_STRING);
	$('#grpreqd').prop('readOnly', true);
	$('#grpreqd').prop('disabled', true);
	$('#grpreqd_error').html(EMPTY_STRING);
	$('#grpreqdanex').prop('readOnly', true);
	$('#grpreqdanex').prop('disabled', true);
	$('#annexMode').prop('disabled', true);
	$('#annexMode').val(EMPTY_STRING);
	$('#annexMode_error').html(EMPTY_STRING);
}

function clearTab3Fields() {
	template.tabs('template_2').disable();
	$('#fieldType').val(EMPTY_STRING);
	$('#fieldType_error').html(EMPTY_STRING);
	template.tabs('template_2').enable();
	minvtemplate_clearGridFields();
	minvtemplate_innerGrid.clearAll();
	$('#consoName').prop('readOnly', true);
	$('#consoName_error').html(EMPTY_STRING);
	setCheckbox('consoBasis', NO);
	$('#consoBasis_error').html(EMPTY_STRING);
	$('#consoName').val(EMPTY_STRING);
	$('#consoName_error').html(EMPTY_STRING);
}

function clearTab4Fields() {
	minvtemplate_clearGridFields2();
	minvtemplate_innerGrid2.clearAll();
	$('#fieldId2').val(EMPTY_STRING);
	$('#fieldId2_error').html(EMPTY_STRING);
	$('#fieldType2').val(EMPTY_STRING);
	$('#fieldType2_error').html(EMPTY_STRING);
	$('#gridAnex').val(EMPTY_STRING);
	$('#gridAnex_error').html(EMPTY_STRING);
	$('#gridAnex').prop('disabled', true);
	$('#groupType_error').html(EMPTY_STRING);
	$('#groupType').prop('disabled', true);
	$('#groupType').val('N');
}

function makeDisable() {
	if (($('#consoGrid').is(':checked'))) {
		$('#grpreqd').prop('readOnly', false);
		$('#grpreqd').prop('disabled', false);
		template.tabs('template_1').enable();
		template.tabs('template_2').enable();
	} else {
		setCheckbox('grpreqd', NO);
		$('#grpreqd').prop('readOnly', true);
		$('#grpreqd').prop('disabled', true);
		template.tabs('template_1').disable();
		template.tabs('template_2').disable();
	}
	if (!$('#consoGrid').is(':checked') && !$('#listanex').is(':checked')) {
		$('#gridAnex').prop('disabled', true);
		$('#gridAnex').val(EMPTY_STRING);
	}
	if ($('#consoGrid').is(':checked') && !$('#listanex').is(':checked')) {
		$('#gridAnex').prop('disabled', true);
		$('#gridAnex').val('G');
	}
	if (!$('#consoGrid').is(':checked') && $('#listanex').is(':checked')) {
		$('#gridAnex').prop('disabled', true);
		$('#gridAnex').val('A');
	}
	if ($('#consoGrid').is(':checked') && $('#listanex').is(':checked')) {
		$('#gridAnex').prop('disabled', false);
	}
	if (($('#templconsol').is(':checked'))) {
		if (!$('#listanex').is(':checked')) {
			$('#annexMode').prop('disabled', true);
			$('#gridAnex').val('G');
			$('#gridAnex').prop('disabled', true);
			$('#grpreqdanex').prop('disabled', true);
		} else if ($('#listanex').is(':checked')) {
			$('#annexMode').prop('disabled', false);
			$('#gridAnex').val('A');
			$('#gridAnex').prop('disabled', true);
			$('#grpreqdanex').prop('disabled', false);
		}
		if (!$('#consoBasis').is(':checked')) {
			$('#consoName').prop('readOnly', true);
		} else {
			$('#consoName').prop('readOnly', false);
		}
	}
	$('#template').height(tabSize[0]);
	template._setTabActive('template_0', true);
	doTabbarClick('template_0');
}

function doclearfields(id) {
	switch (id) {
	case 'invTempCode':
		if (isEmpty($('#invTempCode').val())) {
			clearFields();
			$('#template').height(tabSize[0]);
			template.tabs('template_0').setActive();
			doTabbarClick('template_0');
			break;
		}
	case 'fieldId':
		if (isEmpty($('#fieldId').val())) {
			minvtemplate_clearGridFields();
			break;
		}
	case 'fieldId2':
		if (isEmpty($('#fieldId2').val())) {
			minvtemplate_clearGridFields2();
			break;
		}
	}
}

function add() {
	$('#invTempCode_pic').prop('disabled', true);
	$('#invTempCode').focus();
	setCheckbox('enabled', YES);
	$('#enabled').prop('disabled', true);
	template.tabs('template_1').disable();
	template.tabs('template_2').disable();
}

function modify() {
	$('#invTempCode').focus();
	template.tabs('template_1').disable();
	template.tabs('template_2').disable();
	if ($('#action').val() == MODIFY) {
		$('#enabled').prop('disabled', false);
	} else {
		$('#enabled').prop('disabled', true);
	}
}

function doTabbarClick(id) {
	if (id == 'template_0') {
		$('#template').height(tabSize[0]);
	} else if (id == 'template_1') {
		if (!$('#templconsol').is(':checked')) {
			template.tabs(id).disable();
			alert(CONF_REQ); // new
		} else {
			$('#template').height(tabSize[1]);
			template.tabs(id).enable();
		}
	} else if (id == 'template_2') {
		if (!$('#templconsol').is(':checked')) {
			template.tabs(id).disable();
			alert(CONF_REQ); // new
		} else {
			$('#template').height(tabSize[2]);
			template.tabs(id).enable();
		}
	} else if (id == 'template_3') {
		$('#template').height(tabSize[3]);
	}
	// template.tabs(id).setActive();
}

function loadData() {
	$('#invTempCode').val(validator.getValue('INVTEMPLATE_CODE'));
	$('#description').val(validator.getValue('DESCRIPTION'));
	$('#conciseDescription').val(validator.getValue('CONCISE_DESCRIPTION'));
	$('#vatCst').val(validator.getValue('VAT_CST_AT_HEADER_FOOTER'));
	setCheckbox('serviceTax', validator.getValue('SERVICE_TAX_NO_PRINTED'));
	setCheckbox('shadedInvoice', validator.getValue('SHADED_INVOICE_HEADER'));
	setCheckbox('generateDate', validator.getValue('GENERATION_DATE_PRINTED'));
	setCheckbox('captionTnc', validator.getValue('CAPTION_FOR_TC_PRINTED'));
	setCheckbox('eoaPrint', validator.getValue('EOA_DISCLAIMER_PRINTED'));
	setCheckbox('templconsol', validator.getValue('CONSOLIDATED'));
	setCheckbox('printleasesch', validator.getValue('SCHEDULE_ID_PRINTED'));
	setCheckbox('printleasedeal', validator.getValue('LEASE_FREQDEAL_TYPE_PRINTED'));
	setCheckbox('printdistamt', validator.getValue('DISB_AMOUNT_PRINTED'));
	setCheckbox('consoBasis', validator.getValue('PRINT_CONSOLIDATION_BASIS'));
	$('#consoName').val(validator.getValue('CONSOLIDATION_NAME'));
	setCheckbox('consoGrid', validator.getValue('CONSOLIDATION_IN_GRID'));
	setCheckbox('listanex', validator.getValue('CONSOLIDATION_IN_ANNEX'));
	$('#annexMode').val(validator.getValue('ANNEX_MODE'));
	setCheckbox('grpreqd', validator.getValue('GROUPING_IN_GRID'));
	setCheckbox('grpreqdanex', validator.getValue('GROUPING_IN_ANNEX'));
	setCheckbox('invtempl', validator.getValue('ELECTRONIC_COPY_ONLY'));
	setCheckbox('sign', validator.getValue('REQUIRES_SIGN_ATTACH'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	if (validator.getValue('CONSOLIDATED') == '1') {
		template.tabs('template_1').enable();
		template.tabs('template_2').enable();
		loadGrid();
	} else {
		template.tabs('template_1').disable();
		template.tabs('template_2').disable();
	}
	loadGrid2();
	makeDisable();
	resetLoading();
}

function checkclick(id) {
	switch (id) {
	case 'templconsol':
		templconsol_val();
		break;
	case 'invtempl':
		invtempl_val();
		break;
	case 'sign':
		sign_val();
		break;
	case 'serviceTax':
		serviceTax_val();
		break;
	case 'shadedInvoice':
		shadedInvoice_val();
		break;
	case 'generateDate':
		generateDate_val();
		break;
	case 'captionTnc':
		captionTnc_val();
		break;
	case 'eoaPrint':
		eoaPrint_val();
		break;
	case 'printleasesch':
		printleasesch_val();
		break;
	case 'printleasedeal':
		printleasedeal_val();
		break;

	case 'printdistamt':
		printdistamt_val();
		break;
	case 'consoGrid':
		consoGrid_val();
		break;
	case 'grpreqd':
		grpreqd_val();
		break;
	case 'listanex':
		listanex_val();
		break;
	case 'grpreqdanex':
		grpreqdanex_val();
		break;

	}
}

function view(source, primaryKey) {
	hideParent('inv/qinvtemplate', source, primaryKey);
	resetLoading();
}

function doHelp(id) {
	switch (id) {
	case 'invTempCode':
		help('COMMON', 'HLP_INV_TEMPLATE_CODE', $('#invTempCode').val(), EMPTY_STRING, $('#invTempCode'));
		break;
	case 'fieldId':
		help('MINVTEMPLATE', 'HLP_INV_FIELD_ID', $('#fieldId').val(), EMPTY_STRING, $('#fieldId'));
		break;
	case 'fieldId2':
		help('MINVTEMPLATE', 'HLP_SORT_FIELDS', $('#fieldId2').val(), EMPTY_STRING, $('#fieldId2'));
		break;
	}
}

function validate(id) {
	switch (id) {
	case 'invTempCode':
		invTempCode_val();
		break;
	case 'description':
		description_val();
		break;
	case 'conciseDescription':
		conciseDescription_val();
		break;
	case 'templconsol':
		templconsol_val();
		break;
	case 'invtempl':
		invtempl_val();
		break;
	case 'sign':
		sign_val();
		break;
	case 'vatCst':
		vatCst_val();
		break;
	case 'serviceTax':
		serviceTax_val();
		break;
	case 'shadedInvoice':
		shadedInvoice_val();
		break;
	case 'generateDate':
		generateDate_val();
		break;
	case 'captionTnc':
		captionTnc_val();
		break;
	case 'eoaPrint':
		eoaPrint_val();
		break;
	case 'printleasesch':
		printleasesch_val();
		break;
	case 'printleasedeal':
		printleasedeal_val();
		break;
	case 'printdistamt':
		printdistamt_val();
		break;
	case 'consoGrid':
		consoGrid_val();
		break;
	case 'grpreqd':
		grpreqd_val();
		break;
	case 'listanex':
		listanex_val();
		break;
	case 'grpreqdanex':
		grpreqdanex_val();
		break;
	case 'annexMode':
		annexMode_val();
		break;
	case 'fieldId':
		fieldId_val(true);
		break;
	case 'fieldType':
		fieldType_val();
		break;
	case 'consoBasis':
		consoBasis_val();
		break;
	case 'consoName':
		consoName_val();
		break;
	case 'fieldId2':
		fieldId2_val(true);
		break;
	case 'gridAnex':
		gridAnex_val();
		break;
	case 'groupType':
		groupType_val();
		break;
	case 'enabled':
		enabled_val();
		break;
	case 'remarks':
		remarks_val();
		break;
	}
}
function backtrack(id) {
	switch (id) {
	case 'invTempCode':
		setFocusLast('invTempCode');
		break;
	case 'description':
		setFocusLast('invTempCode');
		break;
	case 'conciseDescription':
		setFocusLast('description');
		break;
	case 'templconsol':
		setFocusLast('conciseDescription');
		break;
	case 'invtempl':
		setFocusLast('templconsol');
		break;
	case 'sign':
		setFocusLast('invtempl');
		break;
	case 'vatCst':
		setFocusLast('sign');
		break;
	case 'serviceTax':
		setFocusLast('vatCst');
		break;
	case 'shadedInvoice':
		setFocusLast('serviceTax');
		break;
	case 'generateDate':
		setFocusLast('shadedInvoice');
		break;
	case 'captionTnc':
		setFocusLast('generateDate');
		break;
	case 'eoaPrint':
		setFocusLast('captionTnc');
		break;
	case 'printleasesch':
		setFocusLast('eoaPrint');
		break;
	case 'printleasedeal':
		setFocusLast('printleasesch');
		break;
	case 'printdistamt':
		setFocusLast('printleasedeal');
		break;
	case 'consoGrid':
		$('#template').height(tabSize[0]);
		template.tabs('template_0').setActive();
		doTabbarClick('template_0');
		setFocusLast('printdistamt');
		break;

	case 'grpreqd':
		setFocusLast('consoGrid');
		break;
	case 'listanex':
		if ($('#consoGrid').is(':checked')) {
			setFocusLast('grpreqd');
		} else {
			setFocusLast('consoGrid');
		}
		break;
	case 'grpreqdanex':
		setFocusLast('listanex');
		break;
	case 'annexMode':
		if ($('#listanex').is(':checked')) {
			setFocusLast('grpreqdanex');
		} else {
			setFocusLast('listanex');
		}
		break;
	case 'fieldId':
		$('#template').height(tabSize[1]);
		template.tabs('template_1').setActive();
		doTabbarClick('template_1');
		if ($('#listanex').is(':checked')) {
			setFocusLast('annexMode');
		} else {
			setFocusLast('listanex');
		}
		break;
	case 'minvtemplate_innerGrid_add':
		setFocusLast('fieldId');
		break;

	case 'consoBasis':
		setFocusLast('fieldId');
		break;
	case 'consoName':
		setFocusLast('consoBasis');
		break;

	case 'fieldId2':
		if ($('#templconsol').is(':checked')) {
			$('#template').height(tabSize[2]);
			template.tabs('template_2').setActive();
			doTabbarClick('template_2');

			if ($('#consoBasis').is(':checked')) {
				setFocusLast('consoName');
			} else {
				setFocusLast('consoBasis');
			}
		} else {
			$('#template').height(tabSize[0]);
			template.tabs('template_0').setActive();
			doTabbarClick('template_0');
			setFocusLast('printdistamt');
		}
		break;
	case 'minvtemplate_innerGrid2_add':
		if ($('#grpreqd').is(':checked') || $('#grpreqdanex').is(':checked')) {
			setFocusLast('groupType');
		} else {
			if ($('#consoGrid').is(':checked') && $('#listanex').is(':checked')) {
				setFocusLast('gridAnex');
			} else {
				setFocusLast('fieldId2');
			}
		}
		break;
	case 'groupType':
		if ($('#consoGrid').is(':checked') && $('#listanex').is(':checked')) {
			setFocusLast('gridAnex');
		} else {
			setFocusLast('fieldId2');
		}
		break;
	case 'gridAnex':
		setFocusLast('fieldId2');
		break;
	case 'enabled':
		$('#template').height(tabSize[3]);
		template.tabs('template_3').setActive();
		doTabbarClick('template_3');
		setFocusLast('fieldId2');
		break;
	case 'remarks':
		if ($('#action').val() == MODIFY) {
			setFocusLast('enabled');
		} else {
			$('#template').height(tabSize[3]);
			template.tabs('template_3').setActive();
			doTabbarClick('template_3');
			setFocusLast('fieldId2');
		}
		break;
	}
}
function revalidate() {
	$('#vatCstDisplay').val($('#vatCst').val());
	$('#annexModeDisplay').val($('#annexMode').val());
	$('#fieldTypeDisplay').val($('#fieldType').val());
	$('#fieldType2Display').val($('#fieldType2').val());
	$('#gridAnexDisplay').val($('#gridAnex').val());
	$('#groupTypeDisplay').val($('#groupType').val());
	
	$('#grpreqdHidden').val($('#grpreqd').val());
	$('#grpreqdanexHidden').val($('#grpreqdanex').val());
	//setCheckbox('grpreqdHidden', $('#grpreqd').is(':checked'));
	//setCheckbox('grpreqdanexHidden', $('#grpreqdanex').is(':checked'));
	minvtemplate_revaildateGrid();
}

function invTempCode_val() {
	var value = $('#invTempCode').val();
	clearError('invTempCode_error');
	$('#invTempCode_desc').html(EMPTY_STRING);
	if (isEmpty(value)) {
		setError('invTempCode_error', MANDATORY);
		return false;
	} else if ($('#action').val() == ADD && !isValidCode(value)) {
		setError('invTempCode_error', INVALID_FORMAT);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('INVTEMPLATE_CODE', $('#invTempCode').val());
		validator.setValue(ACTION, $('#action').val());
		validator.setClass('patterns.config.web.forms.inv.minvtemplatebean');
		validator.setMethod('validateInvTempCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('invTempCode_error', validator.getValue(ERROR));
			$('#invTempCode_desc').html(EMPTY_STRING);
			return false;
		} else {
			if ($('#action').val() == MODIFY) {
				PK_VALUE = getEntityCode() + PK_SEPERATOR + $('#invTempCode').val();
				if (!loadPKValues(PK_VALUE, 'invTempCode_error')) {
					return false;
				}
			}
		}
	}
	setFocusLast('description');
	return true;
}

function description_val() {
	var value = $('#description').val();
	clearError('description_error');
	if (isEmpty(value)) {
		setError('description_error', MANDATORY);
		return false;
	}
	if (!isValidDescription(value)) {
		setError('description_error', INVALID_DESCRIPTION);
		return false;
	} else {
		setFocusLast('conciseDescription');
	}
	return true;
}

function conciseDescription_val() {
	var value = $('#conciseDescription').val();
	clearError('conciseDescription_error');
	if (isEmpty(value)) {
		setError('conciseDescription_error', MANDATORY);
		return false;
	}
	if (!isValidConciseDescription(value)) {
		setError('conciseDescription_error', INVALID_DESCRIPTION);
		return false;
	}
	setFocusLast('templconsol');
	return true;
}

function templconsol_val() {
	clearError('templconsol_error');
	clearError('groupType_error');
	clearError('gridAnex_error');
	clearError('consoGrid_error');
	$('#transactionErrors').html(EMPTY_STRING);
	if (!$('#templconsol').is(':checked')) {
		$('#groupType').prop('disabled', true);
		$('#groupType').val('N');
		if (confirm(TEMPL_CODE)) { // newly
			clearTab2Fields();
			clearTab3Fields();
			$('#gridAnex').val(EMPTY_STRING);
			$('#gridAnex_error').html(EMPTY_STRING);
			$('#gridAnex').prop('disabled', true);
			template.tabs('template_1').disable();
			template.tabs('template_2').disable();
			setFocusLast('invtempl');
		} else {
			template.tabs('template_1').enable();
			template.tabs('template_2').enable();
			$('#gridAnex').prop('disabled', false);
			setCheckbox('templconsol', YES);
			setFocusLast('templconsol');
		}
	} else {
		template.tabs('template_1').enable();
		template.tabs('template_2').enable();
		if ($('#consoGrid').is(':checked') && !$('#grpreqd').is(':checked')) {
			$('#gridAnex').val('G');
			$('#gridAnex').prop('disabled', true);
		} else {
			$('#gridAnex').val('N');
			$('#gridAnex').prop('disabled', true);
		}
		if ($('#grpreqd').is(':checked') || $('#grpreqdanex').is(':checked')) {
			$('#groupType').prop('disabled', false);
		} else {
			$('#groupType').prop('disabled', true);
			$('#groupType').val('N');
		}
		setFocusLast('invtempl');
	}
	$('#template').height(tabSize[0]);
	template._setTabActive('template_0', true);
	doTabbarClick('template_0');
	return true;
}

function invtempl_val() {
	setFocusLast('sign');
	return true;
}

function sign_val() {
	$('#template').height(tabSize[0]);
	template._setTabActive('template_0', true);
	doTabbarClick('template_0');
	setFocusLast('vatCst');
	return true;
}

// first tab

function vatCst_val() {
	clearError('vatCst_error');
	setFocusLast('serviceTax');
	return true;
}

function serviceTax_val() {
	clearError('serviceTax_error');
	setFocusLast('shadedInvoice');
	return true;
}

function shadedInvoice_val() {
	clearError('shadedInvoice_error');
	setFocusLast('generateDate');
	return true;
}

function generateDate_val() {
	clearError('generateDate_error');
	setFocusLast('captionTnc');
	return true;
}

function captionTnc_val() {
	clearError('captionTnc_error');
	setFocusLast('eoaPrint');
	return true;
}

function eoaPrint_val() {
	clearError('eoaPrint_error');
	setFocusLast('printleasesch');
	return true;
}

function printleasesch_val() {
	setFocusLast('printleasedeal');
	return true;
}

function printleasedeal_val() {
	setFocusLast('printdistamt');
	return true;
}

function printdistamt_val() {
	if ($('#templconsol').is(':checked')) {
		$('#template').height(tabSize[1]);
		template.tabs('template_1').setActive();
		doTabbarClick('template_1');
		setFocusLast('consoGrid');
	} else {
		$('#template').height(tabSize[3]);
		template.tabs('template_3').setActive();
		doTabbarClick('template_3');
		setFocusLast('fieldId2');
	}
	return true;
}

// second tab

function consoGrid_val() {
	var value = $('#consoGrid').val();
	clearError('consoGrid_error');
	clearError('grpreqd_error');
	if (($('#consoGrid').is(':checked'))) {
		$('#grpreqd').prop('disabled', false);
		setFocusLast('grpreqd');
	} else {
		setCheckbox('grpreqd', NO);
		$('#grpreqd').prop('disabled', true);
		setFocusLast('listanex');
	}
	if ($('#consoGrid').is(':checked') && !$('#listanex').is(':checked')) {
		$('#gridAnex').prop('disabled', true);
		$('#gridAnex').val('G');
	} else if (!$('#consoGrid').is(':checked') && $('#listanex').is(':checked')) {
		$('#gridAnex').prop('disabled', true);
		$('#gridAnex').val('A');
	} else if ($('#consoGrid').is(':checked') && $('#listanex').is(':checked')) {
		$('#gridAnex').prop('disabled', false);
	}
	return true;
}

function grpreqd_val() {
	clearError('grpreqd_error');
	if (!$('#consoGrid').is(':checked') && $('#grpreqd').is(':checked')) {
		setError('grpreqd_error', HMS_VALUE_NT_ALLWD);
		return false;
	}
	if ($('#grpreqd').is(':checked') || $('#grpreqdanex').is(':checked')) {
		$('#groupType').prop('disabled', false);
	} else {
		if (!$('#grpreqd').is(':checked') || !$('#grpreqdanex').is(':checked')) {
			$('#groupType').prop('disabled', true);
			$('#groupType').val('N');
		}
	}
	setFocusLast('listanex');
	return true;
}

function listanex_val() {
	clearError('listanex_error');
	clearError('grpreqdanex_error');
	if ($('#templconsol').is(':checked')) {
		if (!$('#consoGrid').is(':checked') && !$('#listanex').is(':checked')) {
			setError('listanex_error', SELECT_EITHER_ONE_CONS);
			return false;
		}
		if ($('#consoGrid').is(':checked') && !$('#listanex').is(':checked')) {
			$('#gridAnex').prop('disabled', true);
			$('#gridAnex').val('G');
		}
		if (!$('#consoGrid').is(':checked') && $('#listanex').is(':checked')) {
			$('#gridAnex').prop('disabled', true);
			$('#gridAnex').val('A');
		}
		if ($('#consoGrid').is(':checked') && $('#listanex').is(':checked')) {
			$('#gridAnex').prop('disabled', false);
		}
		if (!$('#listanex').is(':checked')) {
			$('#annexMode').prop('disabled', true);
			$('#gridAnex').val('G');
			$('#groupType').prop('disabled', true);
			$('#grpreqdanex').prop('disabled', true);
			setCheckbox('grpreqdanex', NO);
			clearError('annexMode_error');
			$('#annexMode').val(EMPTY_STRING);
			clearError('annexMode_error');
			$('#template').height(tabSize[2]);
			template.tabs('template_2').setActive();
			doTabbarClick('template_2');
			setFocusLast('fieldId');
		} else if ($('#listanex').is(':checked')) {
			$('#annexMode').prop('disabled', false);
			$('#gridAnex').val('A');
			$('#groupType').prop('disabled', true);
			$('#grpreqdanex').prop('disabled', false);
			setFocusLast('grpreqdanex');
		}
	} else {
		$('#grpreqdanex').prop('disabled', false);
		setFocusLast('grpreqdanex');
		$('#grpreqdanex').val(EMPTY_STRING);
		$('#groupType').prop('disabled', true);
	}
	return true;
}

function grpreqdanex_val() {
	clearError('grpreqdanex_error');
	if (!$('#listanex').is(':checked') && $('#grpreqdanex').is(':checked')) {
		setError('grpreqdanex_error', HMS_VALUE_NT_ALLWD);
		return false;
	}
	if ($('#listanex').is(':checked')) {
		$('#annexMode').prop('disabled', false);
		setFocusLast('annexMode');
	} else {
		$('#groupType').prop('disabled', true);
		$('#annexMode').prop('disabled', true);
		$('#annexMode').val(EMPTY_STRING);
		$('#template').height(tabSize[2]);
		template.tabs('template_2').setActive();
		doTabbarClick('template_2');
		setFocusLast('fieldId');
	}
	if ($('#grpreqd').is(':checked') || $('#grpreqdanex').is(':checked')) {
		$('#groupType').prop('disabled', false);
	} else {
		if (!$('#grpreqd').is(':checked') || !$('#grpreqdanex').is(':checked')) {
			$('#groupType').prop('disabled', true);
			$('#groupType').val('N');
		}
	}
	return true;
}

function annexMode_val() {
	clearError('annexMode_error');
	if (!$('#listanex').is(':checked') && !isEmpty($('#annexMode').val())) {
		setError('annexMode_error', HMS_VALUE_NT_ALLWD);
		return false;
	}
	if ($('#listanex').is(':checked') && isEmpty($('#annexMode').val())) {
		setError('annexMode_error', MANDATORY);
		return false;
	}

	$('#template').height(tabSize[2]);
	template.tabs('template_2').setActive();
	doTabbarClick('template_2');
	setFocusLast('fieldId');
	return true;
}

// third tab
function fieldId_val(valMode) {
	var value = $('#fieldId').val();
	$('#fieldType').val(EMPTY_STRING);
	clearError('fieldId_error');
	if ($('#templconsol').is(':checked')) {
		if (isEmpty(value)) {
			setError('fieldId_error', MANDATORY);
			return false;
		} else {
			validator.clearMap();
			validator.setMtm(false);
			validator.setValue('FIELD_ID', $('#fieldId').val());
			validator.setValue(ACTION, USAGE);
			validator.setClass('patterns.config.web.forms.inv.minvtemplatebean');
			validator.setMethod('validateFieldId');
			validator.sendAndReceive();
			if (validator.getValue(ERROR) != EMPTY_STRING) {
				$('#fieldType').val(EMPTY_STRING);
				$('#fieldId_desc').html(EMPTY_STRING);
				setError('fieldId_error', validator.getValue(ERROR));
				return false;
			} else if (validator.getValue(RESULT) == DATA_AVAILABLE) {
				$('#fieldId_desc').html(validator.getValue('DESCRIPTION'));
				$('#fieldType').val(validator.getValue('FIELD_REF_ASSET_TABLE'));
			}
		}
	}
	if (valMode)
		setFocusLast('minvtemplate_innerGrid_add');
	return true;
}

function consoBasis_val() {
	clearError('consoBasis_error');
	clearError('consoName_error');
	if ($('#templconsol').is(':checked')) {
		if (!$('#consoBasis').is(':checked')) {
			$('#consoName').val(EMPTY_STRING);
			$('#consoName').prop('readOnly', true);
			$('#template').height(tabSize[3]);
			template.tabs('template_3').setActive();
			doTabbarClick('template_3');
			setFocusLast('fieldId2');
		} else {
			$('#consoName').prop('readOnly', false);
			setFocusLast('consoName');
			return true;
		}
	}
	return true;
}

function consoName_val() {
	var value = $('#consoName').val();
	clearError('consoName_error');
	if ($('#templconsol').is(':checked')) {
		if ($('#consoBasis').is(':checked')) {
			if (isEmpty(value)) {
				setError('consoName_error', MANDATORY);
				return false;
			}
			if (!isValidOtherInformation50(value)) {
				setError('consoName_error', INVALID_VALUE);
				return false;
			}
		}
	}
	$('#template').height(tabSize[3]);
	template.tabs('template_3').setActive();
	doTabbarClick('template_3');
	setFocusLast('fieldId2');
	return true;
}

function fieldId2_val(valMode) {
	var value = $('#fieldId2').val();
	clearError('fieldId2_error');
	$('#fieldType2').val(EMPTY_STRING);
	$('#fieldId2_desc').html(EMPTY_STRING);
	if (isEmpty(value)) {
		setError('fieldId2_error', MANDATORY);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('FIELD_ID', $('#fieldId2').val());
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.inv.minvtemplatebean');
		validator.setMethod('validateFieldId2');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			$('#fieldId2_desc').html(EMPTY_STRING);
			$('#fieldType2').val(EMPTY_STRING);
			setError('fieldId2_error', validator.getValue(ERROR));
			return false;
		} else if (validator.getValue(RESULT) == DATA_AVAILABLE) {
			$('#fieldId2_desc').html(validator.getValue('DESCRIPTION'));
			$('#fieldType2').val(validator.getValue('FIELD_REF_ASSET_TABLE'));
		}
	}
	if ($('#consoGrid').is(':checked') && $('#listanex').is(':checked')) {
		setFocusLast('gridAnex');
	} else {
		if ($('#grpreqd').is(':checked') || $('#grpreqd').is(':checked')) {
			setFocusLast('groupType');
		} else {
			setFocusLast('minvtemplate_innerGrid2_add');
		}
	}
	return true;
}

function groupType_val() {
	var value = $('#groupType').val();
	clearError('groupType_error');
	if ($('#grpreqd').is(':checked') || $('#grpreqdanex').is(':checked')) {
		if (isEmpty(value)) {
			setError('groupType_error', MANDATORY);
			return false;
		}
	}
	setFocusLast('minvtemplate_innerGrid2_add');
	return true;
}

function gridAnex_val() {
	var value = $('#gridAnex').val();
	clearError('gridAnex_error');
	if (!$('#templconsol').is(':checked')) {
		$('#gridAnex').val(EMPTY_STRING);
		$('#gridAnex_error').html(EMPTY_STRING);
		$('#gridAnex').prop('disabled', true);
	} else {
		if ($('#consoGrid').is(':checked') || $('#grpreqd').is(':checked')) {
			if (isEmpty(value)) {
				setError('gridAnex_error', MANDATORY);
				return false;
			}
		}
	}
	if ($('#grpreqd').is(':checked') || $('#grpreqdanex').is(':checked')) {
		setFocusLast('groupType');
	} else {
		setFocusLast('minvtemplate_innerGrid2_add');
	}

	return true;
}

function enabled_val() {
	if ($('#action').val() == MODIFY) {
		setFocusLast('remarks');
	}
	return true;
}
function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', INVALID_REMARKS);
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', INVALID_REMARKS);
			return false;
		}
	}
	if ($('#action').val() == MODIFY) {
		if (isEmpty(value)) {
			setError('remarks_error', MANDATORY);
			return false;
		}
	}
	setFocusOnSubmit();
	return true;
}
// grid start
function minvtemplate_addGrid(editMode, editRowId) {
	if ($('#templconsol').is(':checked')) {
		if (fieldId_val(false)) {
			if ($('#fieldType').val() == 'S') {
				fieldTypeValue = $('#fieldType option:selected').text();
			} else if ($('#fieldType').val() == 'U') {
				fieldTypeValue = $('#fieldType option:selected').text();
			}
			if (minvtemplate_gridDuplicateCheck(editMode, editRowId)) {
				var field_values = [ 'false', $('#fieldId').val(), $('#fieldId_desc').html(), fieldTypeValue, $('#fieldType').val() ];
				_grid_updateRow(minvtemplate_innerGrid, field_values);
				minvtemplate_clearGridFields();
				$('#fieldId').focus();
				return true;
			} else {
				setError('fieldId_error', DUPLICATE_DATA);
				$('#fieldId_desc').html(EMPTY_STRING);
				return false;
			}
		} else {
			return false;
		}
	}
}
function minvtemplate_addGrid2(editMode, editRowId) {
	fieldTypeValue2 = EMPTY_STRING;
	gridAnexValue = EMPTY_STRING;
	groupTypeValue = EMPTY_STRING;
	if ($('#gridAnex').val() == 'G') {
		gridAnexValue = $('#gridAnex option:selected').text();
	} else if ($('#gridAnex').val() == 'A') {
		gridAnexValue = $('#gridAnex option:selected').text();
	} else if ($('#gridAnex').val() == 'B') {
		gridAnexValue = $('#gridAnex option:selected').text();
	}
	if ($('#groupType').val() == 'N') {
		groupTypeValue = $('#groupType option:selected').text();
	} else if ($('#groupType').val() == 'C') {
		groupTypeValue = $('#groupType option:selected').text();
	} else if ($('#groupType').val() == 'S') {
		groupTypeValue = $('#groupType option:selected').text();
	}
	if (fieldId2_val(false) && gridAnex_val() && groupType_val()) {
		if ($('#fieldType2').val() == 'S') {
			fieldTypeValue2 = $('#fieldType2 option:selected').text();
		} else if ($('#fieldType2').val() == 'U') {
			fieldTypeValue2 = $('#fieldType2 option:selected').text();
		}
		if (minvtemplate_gridDuplicateCheck2(editMode, editRowId)) {
			var field_values = [ 'false', $('#fieldId2').val(), $('#fieldId2_desc').html(), fieldTypeValue2, $('#fieldType2').val(), gridAnexValue, $('#gridAnex').val(), groupTypeValue, $('#groupType').val() ];
			_grid_updateRow(minvtemplate_innerGrid2, field_values);
			minvtemplate_clearGridFields2();
			$('#fieldId2').focus();
			return true;
		} else {
			setError('fieldId2_error', DUPLICATE_DATA);
			$('#fieldId2_desc').html(EMPTY_STRING);
			return false;
		}
	} else {
		return false;
	}
}
function minvtemplate_gridDuplicateCheck(editMode, editRowId) {
	var currentValue = [ $('#fieldId').val(), $('#fieldType').val() ];
	return _grid_duplicate_check(minvtemplate_innerGrid, currentValue, [ 1, 4 ]);
}

function minvtemplate_gridDuplicateCheck2(editMode, editRowId) {
	var currentValue = [ $('#fieldId2').val(), $('#fieldType2').val() ];
	return _grid_duplicate_check(minvtemplate_innerGrid2, currentValue, [ 1, 4 ]);
}

function minvtemplate_editGrid(rowId) {
	$('#fieldId').val(minvtemplate_innerGrid.cells(rowId, 1).getValue());
	$('#fieldId_desc').html(minvtemplate_innerGrid.cells(rowId, 2).getValue());
	$('#fieldType').val(minvtemplate_innerGrid.cells(rowId, 4).getValue());
	$('#fieldId').focus();
	// if ($('#fieldType').val() == 'S') {
	// fieldTypeValue = "Standard";
	// } else if ($('#fieldType').val() == 'U') {
	// fieldTypeValue = "User-Defined";
	// }
}
function minvtemplate_editGrid2(rowId) {
	$('#fieldId2').val(minvtemplate_innerGrid2.cells(rowId, 1).getValue());
	$('#fieldId2_desc').html(minvtemplate_innerGrid2.cells(rowId, 2).getValue());
	$('#fieldType2').val(minvtemplate_innerGrid2.cells(rowId, 4).getValue());
	// if ($('#fieldType2').val() == 'S') {
	// fieldTypeValue2 = "Standard";
	// } else if ($('#fieldType2').val() == 'U') {
	// fieldTypeValue2 = "User-Defined";
	// }
	// if ($('#gridAnex').val() == 'G') {
	// gridAnexValue = 'Grid';
	// } else if ($('#gridAnex').val() == 'A') {
	// gridAnexValue = 'Annexure';
	// } else if ($('#gridAnex').val() == 'B') {
	// gridAnexValue = 'Both';
	// }
	// if ($('#groupType').val() == 'N') {
	// groupTypeValue = 'Normal';
	// } else if ($('#groupType').val() == 'C') {
	// groupTypeValue = 'Count';
	// } else if ($('#groupType').val() == 'S') {
	// groupTypeValue = 'Sum';
	// }

	if (!$('#consoGrid').is(':checked') && !$('#listanex').is(':checked')) {
		$('#gridAnex').val(EMPTY_STRING);
	} else if ($('#consoGrid').is(':checked') && !$('#listanex').is(':checked')) {
		if (!isEmpty(minvtemplate_innerGrid2.cells(rowId, 6).getValue()))
			$('#gridAnex').val('G');
	} else if (!$('#consoGrid').is(':checked') && $('#listanex').is(':checked')) {
		if (!isEmpty(minvtemplate_innerGrid2.cells(rowId, 6).getValue()))
			$('#gridAnex').val('A');
	} else if ($('#consoGrid').is(':checked') && $('#listanex').is(':checked')) {
		if (!isEmpty(minvtemplate_innerGrid2.cells(rowId, 6).getValue()))
			$('#gridAnex').val(minvtemplate_innerGrid2.cells(rowId, 6).getValue());
	} else
		$('#gridAnex').val(EMPTY_STRING);
	$('#fieldId2').focus();
}

function minvtemplate_cancelGrid(rowId) {
	minvtemplate_clearGridFields();
}
function minvtemplate_cancelGrid2(rowId) {
	minvtemplate_clearGridFields2();
}

function minvtemplate_clearGridFields() {
	$('#fieldId').val(EMPTY_STRING);
	$('#fieldId_desc').html(EMPTY_STRING);
	$('#fieldType').val(EMPTY_STRING);
	// fieldTypeValue = EMPTY_STRING;
	clearError('fieldId_error');
}

function minvtemplate_clearGridFields2() {
	$('#fieldId2').val(EMPTY_STRING);
	$('#fieldId2_desc').html(EMPTY_STRING);
	$('#fieldType2').val(EMPTY_STRING);
	// fieldTypeValue2 = EMPTY_STRING;
	clearError('fieldId2_error');
}

function gridExit(id) {
	switch (id) {
	case 'fieldId':
		setFocusLast('consoBasis');
		break;
	case 'fieldId2':
	case 'grouptype':
	case 'gridAnex':
		$('#xmlMinvtemplateGrid2').val(minvtemplate_innerGrid2.serialize());
		minvtemplate_clearGridFields2();
		if ($('#action').val() == MODIFY)
			setFocusLast('enabled');
		else
			setFocusLast('remarks');
		break;
	}
}
function gridDeleteRow(id) {
	switch (id) {
	case 'fieldId':
		deleteGridRecord(minvtemplate_innerGrid);
		break;
	case 'fieldId2':
		deleteGridRecord(minvtemplate_innerGrid2);
		break;
	}
}
function minvtemplate_revaildateGrid() {
	$('#transactionErrors').html(EMPTY_STRING);
	if ($('#templconsol').is(':checked')) {
		if (minvtemplate_innerGrid.getRowsNum() > 0) {
			$('#xmlMinvtemplateGrid').val(minvtemplate_innerGrid.serialize());
			$('#fieldId_error').html(EMPTY_STRING);
		} else {
			minvtemplate_innerGrid.clearAll();
			$('#xmlMinvtemplateGrid').val(minvtemplate_innerGrid.serialize());
			setError('transactionErrors', ADD_ATLEAST_ONE_ROW + ' Consolidation Basis Section having errors');
			setError('fieldId_error', ADD_ATLEAST_ONE_ROW);
			errors++;
		}
	}
	if (minvtemplate_innerGrid2.getRowsNum() > 0) {
		$('#xmlMinvtemplateGrid2').val(minvtemplate_innerGrid2.serialize());
		$('#fieldId2_error').html(EMPTY_STRING);
	} else {
		minvtemplate_innerGrid2.clearAll();
		$('#xmlMinvtemplateGrid2').val(minvtemplate_innerGrid2.serialize());
		setError('transactionErrors', ADD_ATLEAST_ONE_ROW + ' Print Attributes and Grouping Section having errors');
		setError('fieldId2_error', ADD_ATLEAST_ONE_ROW);
		errors++;
	}

}
