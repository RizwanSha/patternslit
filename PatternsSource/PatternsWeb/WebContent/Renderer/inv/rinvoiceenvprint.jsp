<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<%@taglib prefix="message" tagdir="/WEB-INF/tags/message"%>
<web:bundle baseName="patterns.config.web.forms.inv.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/inv/rinvoiceenvprint.js" />
	</web:dependencies>
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="rinvoiceenvprint.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/inv/rinvoiceenvprint" id="rinvoiceenvprint" method="POST">
				<web:dividerBlock>
					<web:sectionBlock width="width-100p" align="left">
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="rinvoiceenvprint.invoicecycleno" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:invoiceCycleNumber property="invoiceCycleNo" id="invoiceCycleNo" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="rinvoiceenvprint.invmonthyear" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:yearMonth property="invMonth" id="invMonth" datasourceid="COMMON_MONTH" />
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle width="120px" />
									<web:columnStyle width="10px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="rinvoiceenvprint.invoicedate" var="program"/>
									</web:column>
									<web:column>
										<type:dateDisplay property="invoiceFromDate" id="invoiceFromDate" />
									</web:column>
									<web:column>
										<web:legend key="rinvoiceenvprint.dashseparator" var="program" />
									</web:column>
									<web:column>
										<type:dateDisplay property="invoiceUptoDate" id="invoiceUptoDate" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowOdd>
									<web:column>
										<web:legend key="rinvoiceenvprint.noofenvelopes" var="program" />
									</web:column>
									<web:column>
										<type:fiveDigitDisplay property="noOfEnvelopes" id="noOfEnvelopes" />
									</web:column>
								</web:rowOdd>
								
									<web:rowEven>
									<web:column>
										<web:legend key="rinvoiceenvprint.carriername" var="program" />
									</web:column>
									<web:column>
										<type:otherInformation25 property="carrierName" id="carrierName" />
									</web:column>
								</web:rowEven>
								
									<web:rowOdd>
									<web:column>
										<web:legend key="rinvoiceenvprint.senderinfo" var="program" />
									</web:column>
									<web:column>
										<type:otherInformation100 property="senderInfo" id="senderInfo" />
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
						
						<web:section>
							<web:table>
								<web:rowEven>
									<web:column>
										<type:button id="print" key="form.print" var="common" onclick="doPrint()" />
										<type:reset id="reset" key="form.reset" var="common" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:section>
							<message:messageBox id="rinvoiceenvprint" />
						</web:section>
					</web:sectionBlock>
				</web:dividerBlock>
				<web:element property="command" />
				<web:element property="action" />
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
	<web:contextSearch />
</web:fragment>
