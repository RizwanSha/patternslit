var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MINVTEMPLATE';
var tabSize = [ '273px', '200px', '370px', '310px' ];
function init() {
	clearFields();
	minvtemplate_innerGrid.setColumnHidden(0, true);
	minvtemplate_innerGrid2.setColumnHidden(0, true);
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}

function loadQuery2() {
	loadGridQuery(CURRENT_PROGRAM_ID, 'MINVTEMPLATE_GRID2', minvtemplate_innerGrid2);
}

function loadQuery() {
	loadGridQuery(CURRENT_PROGRAM_ID, 'MINVTEMPLATE_GRID', minvtemplate_innerGrid);
}

function clearFields() {
	$('#invTempCode').val(EMPTY_STRING);
	clearNonPKFields();
}
function clearNonPKFields() {
	$('#transactionErrors').val(EMPTY_STRING);
	$('#description').val(EMPTY_STRING);
	$('#conciseDescription').val(EMPTY_STRING);
	setCheckbox('templconsol', NO);
	setCheckbox('invtempl', NO);
	setCheckbox('sign', NO);
	setCheckbox('printleasesch', NO);
	setCheckbox('printleasedeal', NO);
	setCheckbox('printdistamt', NO);
	$('#groupType').val($("#groupType option:first-child").val());
	$('#vatCst').val($("#vatCst option:first-child").val());
	setCheckbox('serviceTax', NO);
	setCheckbox('shadedInvoice', NO);
	setCheckbox('generateDate', NO);
	setCheckbox('captionTnc', NO);
	setCheckbox('eoaPrint', NO);
	// setCheckbox('consolidated', NO);
	setCheckbox('consoAnnex', NO);
	// setCheckbox('scheduleId', NO);
	setCheckbox('frequency', NO);
	setCheckbox('disbursalAmt', NO);
	// setCheckbox('asset', NO);
	// setCheckbox('assetDetails', NO);
	$('#remarks').val(EMPTY_STRING);
	clearTab2Fields();
	clearTab3Fields();
	clearTab4Fields();
}
function clearTab2Fields() {
	template.tabs('template_1').enable();
	setCheckbox('consoGrid', NO);
	setCheckbox('grpreqd', NO);
	setCheckbox('listanex', NO);
	setCheckbox('grpreqdanex', NO);
	$('#grpreqd').prop('readOnly', true);
	$('#grpreqd').prop('disabled', true);
	$('#grpreqdanex').prop('readOnly', true);
	$('#grpreqdanex').prop('disabled', true);
	$('#annexMode').prop('disabled', true);
	$('#annexMode').val(EMPTY_STRING);
}
function clearTab3Fields() {
	$('#fieldType').val(EMPTY_STRING);
	template.tabs('template_2').enable();
	minvtemplate_clearGridFields();
	minvtemplate_innerGrid.clearAll();
	$('#consoName').prop('readOnly', true);
	setCheckbox('consoBasis', NO);
	$('#consoName').val(EMPTY_STRING);

}
function clearTab4Fields() {
	minvtemplate_clearGridFields2();
	minvtemplate_innerGrid2.clearAll();
	$('#fieldId2').val(EMPTY_STRING);
	$('#fieldType2').val(EMPTY_STRING);
	$('#gridAnex').val(EMPTY_STRING);
	$('#gridAnex').prop('disabled', true);
	// $('#groupType').val(EMPTY_STRING);
	$('#groupType').prop('disabled', true);
	$('#groupType').val('N');
}
function minvtemplate_clearGridFields() {
	$('#fieldId').val(EMPTY_STRING);
	$('#fieldId_desc').html(EMPTY_STRING);
	$('#fieldType').val(EMPTY_STRING);
	// fieldTypeValue = EMPTY_STRING;
	clearError('fieldId_error');
	setCheckbox('mandatory', NO);
}
function minvtemplate_clearGridFields2() {
	$('#fieldId2').val(EMPTY_STRING);
	$('#fieldId2_desc').html(EMPTY_STRING);
	// fieldTypeValue2 = EMPTY_STRING;
	clearError('fieldId2_error');
}
function loadData() {
	$('#invTempCode').val(validator.getValue('INVTEMPLATE_CODE'));
	$('#description').val(validator.getValue('DESCRIPTION'));
	$('#conciseDescription').val(validator.getValue('CONCISE_DESCRIPTION'));
	$('#vatCst').val(validator.getValue('VAT_CST_AT_HEADER_FOOTER'));
	setCheckbox('serviceTax', validator.getValue('SERVICE_TAX_NO_PRINTED'));
	setCheckbox('shadedInvoice', validator.getValue('SHADED_INVOICE_HEADER'));
	setCheckbox('generateDate', validator.getValue('GENERATION_DATE_PRINTED'));
	setCheckbox('captionTnc', validator.getValue('CAPTION_FOR_TC_PRINTED'));
	setCheckbox('eoaPrint', validator.getValue('EOA_DISCLAIMER_PRINTED'));
	setCheckbox('templconsol', validator.getValue('CONSOLIDATED'));
	setCheckbox('printleasesch', validator.getValue('SCHEDULE_ID_PRINTED'));
	setCheckbox('printleasedeal', validator.getValue('LEASE_FREQDEAL_TYPE_PRINTED'));
	setCheckbox('printdistamt', validator.getValue('DISB_AMOUNT_PRINTED'));
	setCheckbox('consoBasis', validator.getValue('PRINT_CONSOLIDATION_BASIS'));
	$('#consoName').val(validator.getValue('CONSOLIDATION_NAME'));
	setCheckbox('consoGrid', validator.getValue('CONSOLIDATION_IN_GRID'));
	setCheckbox('listanex', validator.getValue('CONSOLIDATION_IN_ANNEX'));
	$('#annexMode').val(validator.getValue('ANNEX_MODE'));
	setCheckbox('grpreqd', validator.getValue('GROUPING_IN_GRID'));
	setCheckbox('grpreqdanex', validator.getValue('GROUPING_IN_ANNEX'));
	setCheckbox('invtempl', validator.getValue('ELECTRONIC_COPY_ONLY'));
	setCheckbox('sign', validator.getValue('REQUIRES_SIGN_ATTACH'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadAuditFields(validator);
	if (validator.getValue('CONSOLIDATED') == '1') {
		template.tabs('template_1').enable();
		template.tabs('template_2').enable();
		loadGrid();
	} else {
		template.tabs('template_1').disable();
		template.tabs('template_2').disable();
	}
	loadGrid2();
	$('#template').height(tabSize[0]);
	template._setTabActive('template_0', true);
	doTabbarClick('template_0');
	resetLoading();
}
function loadGrid() {
	SOURCE_VALUE = _tableType;
	PK_VALUE = _primaryKey;
	if (_tableType == MAIN) {
		loadQuery();

	} else if (_tableType == TBA) {
		loadQuery();
	}
}

function loadGrid2() {
	SOURCE_VALUE = _tableType;
	PK_VALUE = _primaryKey;
	if (_tableType == MAIN) {
		loadQuery2();

	} else if (_tableType == TBA) {
		loadQuery2();
	}
}
function doTabbarClick(id) {
	if (id == 'template_0') {
		$('#template').height(tabSize[0]);
	} else if (id == 'template_1') {
		if (!$('#templconsol').is(':checked')) {
			template.tabs(id).disable();
			alert(CONF_REQ); // new
		} else {
			$('#template').height(tabSize[1]);
			template.tabs(id).enable();
		}
	} else if (id == 'template_2') {
		if (!$('#templconsol').is(':checked')) {
			template.tabs(id).disable();
			alert(CONF_REQ); // new
		} else {
			$('#template').height(tabSize[2]);
			template.tabs(id).enable();
		}
	} else if (id == 'template_3') {
		$('#template').height(tabSize[3]);
	}
}

function clearTabErrors() {
	clearError('template_0_error');
	clearError('template_1_error');
	clearError('template_2_error');
}

function minvtemplate_clearGridFields() {
	$('#fieldId').val(EMPTY_STRING);
	$('#fieldId_desc').html(EMPTY_STRING);
	$('#fieldType').val(EMPTY_STRING);
	clearError('fieldId_error');
	setCheckbox('mandatory', NO);
}