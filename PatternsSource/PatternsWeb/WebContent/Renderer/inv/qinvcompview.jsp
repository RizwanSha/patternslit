<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.inv.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/inv/qinvcompview.js" />
	</web:dependencies>
	<web:loadPK />
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="qinvcompview.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:viewContent id="po_view1">
				<web:dividerBlock>
					<web:sectionBlock width="width-100p" align="left">
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="qinvcompview.componentreference" var="program" />
									</web:column>
									<web:column>
										<type:dateDaySLDisplay property="componentReference" id="componentReference" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle width="200px" />
									<web:columnStyle width="200px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowOdd>
									<web:column>
										<web:legend key="qinvcompview.invoicedate" var="program" />
									</web:column>
									<web:column>
										<type:dateDisplay property="invoiceDate" id="invoiceDate" />
									</web:column>
									<web:column>
										<web:legend key="qinvcompview.generationdate" var="program" />
									</web:column>
									<web:column>
										<type:dateDisplay property="generationDate" id="generationDate" />
									</web:column>
								</web:rowOdd>

							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle width="200px" />
									<web:columnStyle width="200px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="qinvcompview.lesseecode" var="program" />
									</web:column>
									<web:column>
										<type:lesseeCodeDisplay property="lesseeCode" id="lesseeCode" />
									</web:column>
									<web:column>
										<web:legend key="qinvcompview.customerid" var="program" />
									</web:column>
									<web:column>
										<type:customerIDDisplay property="customerId" id="customerId" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>

						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle width="200px" />
									<web:columnStyle width="200px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowOdd>
									<web:column>
										<web:legend key="qinvcompview.agreementserial" var="program" />
									</web:column>
									<web:column>
										<type:agreementSerialDisplay property="agreementSerial" id="agreementSerial" />
									</web:column>
									<web:column>
										<web:legend key="qinvcompview.scheduleid" var="program" />
									</web:column>
									<web:column>
										<type:scheduleIDDisplay property="scheduleId" id="scheduleId" />
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="qinvcompview.customername" var="program" />
									</web:column>
									<web:column>
										<type:nameDisplay property="customerName" id="customerName" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="qinvcompview.productcode" var="program" />
									</web:column>
									<web:column>
										<type:productCodeDisplay property="productCode" id="productCode" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="qinvcompview.billingaddressserial" var="program" />
									</web:column>
									<web:column>
										<type:addressSerialDisplay property="billingAddressSerial" id="billingAddressSerial" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="qinvcompview.lessorbranchcode" var="program" />
									</web:column>
									<web:column>
										<type:branchCodeDisplay property="lessorBranchCode" id="lessorBranchCode" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="qinvcompview.lessorstatecode" var="program" />
									</web:column>
									<web:column>
										<type:stateCodeDisplay property="lessorStateCode" id="lessorStateCode" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="qinvcompview.invoicetype" var="program" />
									</web:column>
									<web:column>
										<type:comboDisplay property="invoiceType" id="invoiceType" datasourceid="COMMON_INVOICE_TYPE" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="qinvcompview.componentamount" var="program" />
									</web:column>
									<web:column>
										<type:currencySmallAmountDisplay property="componentAmount" id="componentAmount" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="qinvcompview.createdby" var="program" />
									</web:column>
									<web:column>
										<type:userDisplay property="createdBy" id="createdBy" />
									</web:column>
								</web:rowOdd>

								<web:rowEven>
									<web:column>
										<web:legend key="qinvcompview.createdon" var="program" />
									</web:column>
									<web:column>
										<type:dateTimeDisplay property="createdOn" id="createdOn" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:rowOdd>
									<web:column>
										<web:grid height="240px" width="836px" id="qinvcompview_innerGrid" src="inv/qinvcompview_innerGrid.xml">
										</web:grid>
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
						<web:viewContent id="taxAmount" styleClass='hidden'>
						<web:section>
							<web:table>
								<web:rowOdd>
									<web:column>
										<web:grid height="240px" width="860px" id="invcomptaxdtl_innerGrid" src="inv/invcomptaxdtl_innerGrid.xml">
										</web:grid>
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:rowOdd>
									<web:column>
										<web:button key="qinvcompview.close" id="close" var="program" onclick="closePopup()" />
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
						</web:viewContent>
					</web:sectionBlock>
				</web:dividerBlock>
			</web:viewContent>
		</web:viewSubPart>
	</web:viewPart>
</web:fragment>
