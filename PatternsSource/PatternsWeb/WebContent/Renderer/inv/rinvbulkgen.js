var validator = new xmlHTTPValidator();
var validator1 = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'RINVBULKGEN';
var fromDay = EMPTY_STRING;
var uptoDay = EMPTY_STRING;
function init() {
	clearFields();
}

function clearFields() {
	$('#billingCycle').val(EMPTY_STRING);
	$('#billingCycle_error').html(EMPTY_STRING);
	$('#month').val(getCalendarMonth(getCBD()));
	$('#monthYear').val(getCalendarYear(getCBD()));
	$('#month_error').html(EMPTY_STRING);
	$('#fromDate').val(EMPTY_STRING);
	$('#fromDate_error').html(EMPTY_STRING);
	$('#uptoDate').val(EMPTY_STRING);
	$('#uptoDate_error').html(EMPTY_STRING);
	$('#productCode').val(EMPTY_STRING);
	$('#productCode_error').html(EMPTY_STRING);
	$('#customerId').val(EMPTY_STRING);
	$('#customerId_error').html(EMPTY_STRING);
	$('#nofInvoices').val(EMPTY_STRING);
	fromDay = EMPTY_STRING;
	uptoDay = EMPTY_STRING;
	hideAllMessages(getProgramID());
	setFocusLast('billingCycle');
}

function doHelp(id) {
	switch (id) {
	case 'billingCycle':
		help('COMMON', 'HLP_INVOICE_CYCLE_NUMBER', $('#billingCycle').val(), $(
				'#billingCycle').val(), $('#billingCycle'), null, function(
				gridObj, rowID) {
			$('#billingCycle').val(gridObj.cells(rowID, 0).getValue());
			$('#billingCycle_error').html(EMPTY_STRING);
		});
		break;
	case 'productCode':
		help('COMMON', 'HLP_LEASE_PROD_CODE', $('#productCode').val(),
				EMPTY_STRING, $('#productCode'));
		break;
	case 'customerId':
		help('COMMON', 'HLP_CUSTOMER_ID', $('#customerId').val(), EMPTY_STRING,
				$('#customerId'));
		break;
	}
}
function backtrack(id) {
	switch (id) {
	case 'billingCycle':
		setFocusLast('billingCycle');
		break;
	case 'month':
		setFocusLast('billingCycle');
		break;
	case 'monthYear':
		setFocusLast('month');
		break;
	case 'productCode':
		setFocusLast('monthYear');
		break;
	case 'customerId':
		setFocusLast('productCode');
		break;
	case 'submit':
		setFocusLast('customerId');
		break;
	}
}
function doclearfields(id) {

}

function validate(id) {
	switch (id) {
	case 'billingCycle':
		billingCycle_val();
		break;
	case 'month':
		month_val(true);
		break;
	case 'monthYear':
		monthYear_val(true);
		break;
	case 'productCode':
		productCode_val();
		break;
	case 'customerId':
		customerId_val();
		break;
	case 'submit':
		doPrint();
		break;
	}
}
function billingCycle_val() {
	var value = $('#billingCycle').val();
	clearError('billingCycle_error');
	fromDay = EMPTY_STRING;
	uptoDay = EMPTY_STRING;
	if (isEmpty(value)) {
		setError('billingCycle_error', MANDATORY);
		return false;
	} else if (!isNumeric(value)) {
		setError('billingCycle_error', INVALID_NUMBER);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('INV_CYCLE_NUMBER', value);
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.inv.rinvbulkgenbean');
		validator.setMethod('validateBillingCycle');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('billingCycle_error', validator.getValue(ERROR));
			return false;
		} else if (validator.getValue(RESULT) == DATA_AVAILABLE) {
			fromDay = validator.getValue('INV_RENTAL_DAY_FROM');
			uptoDay = validator.getValue('INV_RENTAL_DAY_UPTO');
		}
	}
	setFocusLast('month');
	return true;
}

function month_val(valMode) {
	var value = $('#month').val();
	clearError('month_error');
	if (isEmpty(value)) {
		$('#month').val(getCalendarMonth(getCBD()));
	}
	if (valMode)
		setFocusLast('monthYear');
	return true;
}

function getCalendarMonth(w_date) {
	var month = parseInt(w_date.substring(3, 5));
	if (isNumeric(month))
		return month;
	else
		return 0;
}

function monthYear_val(valMode) {
	var value = $('#monthYear').val();
	clearError('month_error');
	$('#fromDate').val(EMPTY_STRING);
	$('#uptoDate').val(EMPTY_STRING);
	if (isEmpty(value)) {
		$('#monthYear').val(getCalendarYear(getCBD()));
	}
	if (isEmpty($('#month').val())) {
		$('#month').val(getCalendarMonth(getCBD()));
	}
	value = $('#monthYear').val();
	if (!isValidYear(value)) {
		setError('month_error', INVALID_YEAR);
		return false;
	}

	if (parseInt(value) < 1990) {
		setError('month_error', formatMessage(TC_YEAR_CANNOT_BE_LT, [ '1990' ]));
		return false;
	}
	$('#fromDate')
			.val(
					moment(
							[ parseInt($('#monthYear').val()),
									parseInt($('#month').val()) - 1,
									parseInt(fromDay) ]).format(
							SCRIPT_DATE_FORMAT));
	$('#uptoDate')
			.val(
					moment(
							[ parseInt($('#monthYear').val()),
									parseInt($('#month').val()) - 1,
									parseInt(uptoDay) ]).format(
							SCRIPT_DATE_FORMAT));
	if (valMode)
		setFocusLast('productCode');
	return true;
}

function productCode_val() {
	var value = $('#productCode').val();
	clearError('productCode_error');
	$('#productCode_desc').html(EMPTY_STRING);
	$('#nofInvoices').val(EMPTY_STRING);
	if (isEmpty(value)) {
		setError('productCode_error', MANDATORY);
		return false;
	}
	if (!isEmpty(value)) {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('LEASE_PRODUCT_CODE', value);
		validator.setValue('CUSTOMER_ID',  $('#customerId').val());
		validator.setValue('FROM_DATE', $('#fromDate').val());
		validator.setValue('UPTO_DATE', $('#uptoDate').val());
		validator.setValue('SERVER_SIDE',ZERO);
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.inv.rinvbulkgenbean');
		validator.setMethod('validateLeaseProductCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('productCode_error', validator.getValue(ERROR));
			return false;
		}
		if (validator.getValue(RESULT) == DATA_AVAILABLE) {
			$('#productCode_desc').html(validator.getValue('DESCRIPTION'));
			$('#nofInvoices').val(validator.getValue('NO_OF_INVOICES'));
		}
	}
	setFocusLast('customerId');
	return true;
}

function customerId_val() {
	var value = $('#customerId').val();
	clearError('customerId_error');
	$('#customerId_desc').html(EMPTY_STRING);
	if (!isEmpty(value)) {
		if (!isNumeric(value)) {
			setError('customerId_error', NUMERIC_CHECK);
			return false;
		}
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('CUSTOMER_ID', value);
	validator.setValue('FROM_DATE', $('#fromDate').val());
	validator.setValue('UPTO_DATE', $('#uptoDate').val());
	validator.setValue('LEASE_PRODUCT_CODE', $('#productCode').val());
	validator.setValue('SERVER_SIDE',ZERO);
	validator.setValue(ACTION, USAGE);
	validator.setClass('patterns.config.web.forms.inv.rinvbulkgenbean');
	validator.setMethod('validateCustomerId');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('customerId_error', validator.getValue(ERROR));
		return false;
	} else {
		$('#customerId_desc').html(validator.getValue('CUSTOMER_NAME'));
		$('#nofInvoices').val(validator.getValue('NO_OF_INVOICES'));
	}
	setFocusLast('submit');
	return true;
}

function doSubmit() {
	clearError('customerId_error');
	clearError('productCode_error');
	clearError('month_error');
	clearError('month_error');
	clearError('billingCycle_error');
	if (!reval())
		return;
	showMessage(getProgramID(), Message.PROGRESS);
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('ENTITY_CODE', getEntityCode());
	validator.setValue('INV_CYCLE_NUMBER', $('#billingCycle').val());
	validator.setValue('FROM_DATE', $('#fromDate').val());
	validator.setValue('UPTO_DATE', $('#uptoDate').val());
	validator.setValue('MONTH', $('#month').val());
	validator.setValue('YEAR', $('#monthYear').val());
	validator.setValue('FIN_YEAR', getFinYear($('#fromDate').val()));
	validator.setValue('LEASE_PRODUCT_CODE', $('#productCode').val());
	validator.setValue('CUSTOMER_ID', $('#customerId').val());
	validator.setValue('PROGRAM_ID', CURRENT_PROGRAM_ID);
	validator.setClass('patterns.config.web.forms.inv.rinvbulkgenbean');
	validator.setMethod('doSubmit');
	validator.sendAndReceiveAsync(processDownloadResult);
}

function processDownloadResult() {
	if (validator.getValue(ERROR) != null
			&& validator.getValue(ERROR) != EMPTY_STRING) {
		if (validator.getValue(ERROR_FIELD) != EMPTY_STRING) {
			hideAllMessages(getProgramID());
			setError(validator.getValue(ERROR_FIELD) + '_error', validator
					.getValue(ERROR));
			return false;
		} else {
			showMessage(getProgramID(), Message.ERROR, validator
					.getValue(ERROR));
			return false;
		}
	} else {
		showMessage(getProgramID(), Message.SUCCESS,
				[ TC_INVOICES_GEN_COMPLETED ]);
		return true;
	}
}

function reval() {
	var err = 0;
	if (!month_val(false)) {
		err++;
	}
	if (!monthYear_val(false)) {
		err++;
	}
	if (err > 0)
		return false;
	return true;
}