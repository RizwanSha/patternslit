var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'RINVOICESEQPRINT';
var fromDay = EMPTY_STRING;
var uptoDay = EMPTY_STRING;
function init() {
	clearFields();
}

function clearFields() {
	$('#billingCycle').val(EMPTY_STRING);
	$('#billingCycle_error').html(EMPTY_STRING);
	$('#month').val(getCalendarMonth(getCBD()));
	$('#monthYear').val(getCalendarYear(getCBD()));
	$('#month_error').html(EMPTY_STRING);
	$('#fromDate').val(EMPTY_STRING);
	$('#fromDate_error').html(EMPTY_STRING);
	$('#uptoDate').val(EMPTY_STRING);
	$('#uptoDate_error').html(EMPTY_STRING);
	$('#nofInvoices').val(EMPTY_STRING);
	$('#nofInvoices_error').html(EMPTY_STRING);
	$('#printingMode').val(EMPTY_STRING);
	$('#printingMode_error').html(EMPTY_STRING);
	$('#nofInvoicesInSplit').val(EMPTY_STRING);
	$('#nofInvoicesInSplit_error').html(EMPTY_STRING);
	$('#nofGroupedInvoices').val(EMPTY_STRING);
	$('#nofGroupedInvoices_error').html(EMPTY_STRING);
	fromDay = EMPTY_STRING;
	uptoDay = EMPTY_STRING;
	$('#printingMode').prop('disabled', false);
	$('#nofInvoicesInSplit').prop('readonly', false);
	setFocusLast('billingCycle');
}

function doHelp(id) {
	switch (id) {
	case 'billingCycle':
		help('COMMON', 'HLP_INVOICE_CYCLE_NUMBER', $('#billingCycle').val(), $('#billingCycle').val(), $('#billingCycle'), null, function(gridObj, rowID) {
			$('#billingCycle').val(gridObj.cells(rowID, 0).getValue());
			$('#billingCycle_error').html(EMPTY_STRING);
		});
		break;
	}
}
function backtrack(id) {
	switch (id) {
	case 'billingCycle':
		setFocusLast('billingCycle');
		break;
	case 'month':
		setFocusLast('billingCycle');
		break;
	case 'monthYear':
		setFocusLast('month');
		break;
	case 'printingMode':
		setFocusLast('monthYear');
		break;
	case 'nofInvoicesInSplit':
		if (!$("#printingMode").prop('disabled'))
			setFocusLast('printingMode');
		else
			setFocusLast('monthYear');
		break;
	case 'nofGroupedInvoices':
		setFocusLast('nofInvoicesInSplit');
		break;
	case 'print':
		if (!$("#nofInvoicesInSplit").prop('readonly'))
			setFocusLast('nofInvoicesInSplit');
		else if (!$("#printingMode").prop('disabled'))
			setFocusLast('printingMode');
		else
			setFocusLast('monthYear');
		break;
	}
}
function doclearfields(id) {

}

function validate(id) {
	switch (id) {
	case 'billingCycle':
		billingCycle_val();
		break;
	case 'month':
		month_val();
		break;
	case 'monthYear':
		monthYear_val();
		break;
	case 'printingMode':
		printingMode_val();
		break;
	case 'nofInvoicesInSplit':
		nofInvoicesInSplit_val();
		break;
	case 'nofGroupedInvoices':
		nofGroupedInvoices_val();
		break;
	case 'print':
		doPrint();
		break;
	}
}
function billingCycle_val() {
	var value = $('#billingCycle').val();
	clearError('billingCycle_error');
	fromDay = EMPTY_STRING;
	uptoDay = EMPTY_STRING;
	if (isEmpty(value)) {
		setError('billingCycle_error', MANDATORY);
		return false;
	} else if (!isNumeric(value)) {
		setError('billingCycle_error', INVALID_NUMBER);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('INV_CYCLE_NUMBER', value);
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.inv.rinvoiceseqprintbean');
		validator.setMethod('validateBillingCycle');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('billingCycle_error', validator.getValue(ERROR));
			return false;
		} else if (validator.getValue(RESULT) == DATA_AVAILABLE) {
			fromDay = validator.getValue('INV_RENTAL_DAY_FROM');
			uptoDay = validator.getValue('INV_RENTAL_DAY_UPTO');
		}
	}
	setFocusLast('month');
	return true;
}

function month_val() {
	var value = $('#month').val();
	clearError('month_error');
	if (isEmpty(value)) {
		$('#month').val(getCalendarMonth(getCBD()));
	}
	setFocusLast('monthYear');
	return true;
}

function getCalendarMonth(w_date) {
	var month = parseInt(w_date.substring(3, 5));
	if (isNumeric(month))
		return month;
	else
		return 0;
}

function monthYear_val() {
	var value = $('#monthYear').val();
	clearError('month_error');
	$('#fromDate').val(EMPTY_STRING);
	$('#uptoDate').val(EMPTY_STRING);
	$('#nofInvoices').val(EMPTY_STRING);
	if (isEmpty(value)) {
		$('#monthYear').val(getCalendarYear(getCBD()));
	}
	if (isEmpty($('#month').val())) {
		$('#month').val(getCalendarMonth(getCBD()));
	}
	value = $('#monthYear').val();
	if (!isValidYear(value)) {
		setError('month_error', INVALID_YEAR);
		return false;
	}

	if (parseInt(value) < 1990) {
		setError('month_error', formatMessage(TC_YEAR_CANNOT_BE_LT, [ '1990' ]));
		return false;
	}
	$('#fromDate').val(moment([ parseInt($('#monthYear').val()), parseInt($('#month').val()) - 1, parseInt(fromDay) ]).format(SCRIPT_DATE_FORMAT));
	$('#uptoDate').val(moment([ parseInt($('#monthYear').val()), parseInt($('#month').val()) - 1, parseInt(uptoDay) ]).format(SCRIPT_DATE_FORMAT));
	if (isDate($('#fromDate').val()) && isDate($('#uptoDate').val())) {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('FROM_DATE', $('#fromDate').val());
		validator.setValue('UPTO_DATE', $('#uptoDate').val());
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.inv.rinvoiceseqprintbean');
		validator.setMethod('getInvoiceCount');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('month_error', validator.getValue(ERROR));
			return false;
		} else if (parseInt(validator.getValue('NO_OF_INVOICES')) == 0) {
			setError('month_error', TC_NO_INVOICES_FOR_MONTH_YEAR);
			return false;
		} else {
			$('#nofInvoices').val(validator.getValue('NO_OF_INVOICES'));
		}
	}
	if (parseInt($('#nofInvoices').val()) > 1) {
		$('#printingMode').val(EMPTY_STRING);
		$('#printingMode').prop('disabled', false);
		$('#nofInvoicesInSplit').val(EMPTY_STRING);
		$('#nofGroupedInvoices').val(EMPTY_STRING);
		$('#nofInvoicesInSplit').prop('readonly', false);
		setFocusLast('printingMode');
	} else {
		$('#printingMode_error').html(EMPTY_STRING);
		$('#printingMode').val('A');
		$('#printingMode').prop('disabled', true);
		$('#nofInvoicesInSplit').val($('#nofInvoices').val());
		$('#nofGroupedInvoices_error').html(EMPTY_STRING);
		$('#nofGroupedInvoices').val(ONE);
		$('#nofInvoicesInSplit').prop('readonly', true);
		setFocusLast('print');
	}
	return true;
}

function printingMode_val() {
	var value = $('#printingMode').val();
	clearError('printingMode_error');
	if (isEmpty(value)) {
		setError('printingMode_error', MANDATORY);
		return false;
	}
	if (value == 'A') {
		$('#nofInvoicesInSplit').val($('#nofInvoices').val());
		$('#nofGroupedInvoices').val(ONE);
		$('#nofInvoicesInSplit').prop('readonly', true);
		setFocusLast('print');
	} else if (value == 'S') {
		$('#nofInvoicesInSplit').val(EMPTY_STRING);
		$('#nofGroupedInvoices').val(EMPTY_STRING);
		$('#nofInvoicesInSplit').prop('readonly', false);
		setFocusLast('nofInvoicesInSplit');
	}
	else if (value == 'C') {
		$('#nofInvoicesInSplit').val(EMPTY_STRING);
		$('#nofGroupedInvoices').val(EMPTY_STRING);
		$('#nofInvoicesInSplit').prop('readonly', true);
		setFocusLast('print');
	}
	return true;
}
function nofInvoicesInSplit_val() {
	var value = $('#nofInvoicesInSplit').val();
	clearError('nofInvoicesInSplit_error');
	if (isEmpty(value)) {
		setError('nofInvoicesInSplit_error', MANDATORY);
		return false;
	}
	if (!isNumeric(value)) {
		setError('nofInvoicesInSplit_error', INVALID_NUMBER);
		return false;
	}
	if (isNegative(value)) {
		setError('nofInvoicesInSplit_error', NEGATIVE_CHECK);
		return false;
	}
	if (isZero(value)) {
		setError('nofInvoicesInSplit_error', ZERO_CHECK);
		return false;
	}
	if (parseInt(value) > parseInt(parseInt($('#nofInvoices').val()) / 2)) {
		setError('nofInvoicesInSplit_error', formatMessage(TC_NUM_CANNxOT_BE_GT, [ parseInt(parseInt($('#nofInvoices').val()) / 2) ]));
		return false;
	}
	$('#nofGroupedInvoices').val(Math.ceil(parseInt($('#nofInvoices').val()) / parseInt(value)));
	setFocusLast('print');
	return true;
}

function doPrint() {
	showMessage(getProgramID(), Message.PROGRESS);
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('ENTITY_CODE', getEntityCode());
	validator.setValue('FROM_DATE', $('#fromDate').val());
	validator.setValue('UPTO_DATE', $('#uptoDate').val());
	validator.setValue('INV_CYCLE_NUMBER', $('#billingCycle').val());
	validator.setValue('MONTH', $('#month').val());
	validator.setValue('YEAR', $('#monthYear').val());
	validator.setValue('PRINT_MODE', $('#printingMode').val());
	validator.setValue('NO_OF_INVOICES', $('#nofInvoices').val());
	validator.setValue('NO_OF_INVOICES_IN_SPLIT', $('#nofInvoicesInSplit').val());
	validator.setValue('NO_OF_GROUPED_INVOICES', $('#nofGroupedInvoices').val());
	validator.setValue('FIN_YEAR', getFinYear($('#fromDate').val()));
	validator.setValue('PROGRAM_ID', CURRENT_PROGRAM_ID);
	validator.setClass('patterns.config.web.forms.inv.rinvoiceseqprintbean');
	validator.setMethod('doPrint');
	validator.sendAndReceiveAsync(processDownloadResult);
}

function processDownloadResult() {
	if (validator.getValue(ERROR) != null && validator.getValue(ERROR) != EMPTY_STRING) {
		showMessage(getProgramID(), Message.ERROR, validator.getValue(ERROR));
		return false;
	} else {
		showDownloadLink(validator);
		return true;
	}
}

function showDownloadLink(_validator) {
	showMessage(getProgramID(), Message.SUCCESS, [ REPORT_SUCCESS_MESSAGE ]);
	if ($('#printingMode').val() == 'A') {
		$('#reportLink').attr('href', getBasePath() + 'servlet/ReportDownloadProcessor?' + REPORT_ID + '=' + _validator.getValue(REPORT_ID));
		$('#reportLink')[0].click();
	}else{
		$('#reportLink').attr('href', getBasePath() + 'servlet/fileDownloadProcessor?FILENAME=' + _validator.getValue('FILENAME')+'&FILEPATH='+_validator.getValue('FILEPATH'));
		$('#reportLink')[0].click();
	}
}