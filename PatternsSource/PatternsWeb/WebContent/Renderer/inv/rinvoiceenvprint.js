var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'RINVOICEENVPRINT';
var invoiceFromDay;
var invoiceUptoDay;

function init() {
	clearFields();
	setFocusLast('invoiceCycleNo');
}

function doclearfields(id) {
	switch (id) {
	case 'invoiceCycleNo':
		if (isEmpty($('#invoiceCycleNo').val())) {
			clearFields();
		}
		break;
	}
}

function clearFields() {
	$('#invoiceCycleNo').val(EMPTY_STRING);
	$('#invoiceCycleNo_error').html(EMPTY_STRING);
	$('#invoiceCycleNo_desc').html(EMPTY_STRING);
	$('#invMonth').val(EMPTY_STRING);
	$('#invMonthYear').val(EMPTY_STRING);
	$('#invMonth_error').html(EMPTY_STRING);
	$('#invoiceFromDate').val(EMPTY_STRING);
	$('#invoiceUptoDate').val(EMPTY_STRING);
	$('#noOfEnvelopes').val(EMPTY_STRING);

	$('#carrierName').val(EMPTY_STRING);
	$('#senderInfo').val(EMPTY_STRING);
	hideMessage(getProgramID());
}

function doHelp(id) {
	switch (id) {
	case 'invoiceCycleNo':
		help('COMMON', 'HLP_INVOICE_CYCLE', $('#invoiceCycleNo').val(), EMPTY_STRING, $('#invoiceCycleNo'), descNotRequired, function(gridObj, rowID) {
			$('#invoiceCycleNo').val(gridObj.cells(rowID, 0).getValue());
		});
		break;
	}
}

function descNotRequired() {
	return true;
}

function loadData() {

}

function backtrack(id) {
	switch (id) {
	case 'invoiceCycleNo':
		setFocusLast('invoiceCycleNo');
		break;
	case 'invMonth':
		setFocusLast('invoiceCycleNo');
		break;
	case 'invMonthYear':
		setFocusLast('invMonth');
		break;
	case 'carrierName':
		setFocusLast('invMonthYear');
		break;
	case 'senderInfo':
		setFocusLast('carrierName');
		break;
	case 'print':
		setFocusLast('senderInfo');
		break;
	}
}

function validate(id) {
	switch (id) {
	case 'invoiceCycleNo':
		invoiceCycleNo_val();
		break;
	case 'invMonth':
		invMonth_val();
		break;
	case 'invMonthYear':
		invMonthYear_val();
		break;
	case 'carrierName':
		carrierName_val();
		break;
	case 'senderInfo':
		senderInfo_val();
		break;
	case 'print':
		doPrint();
		break;
	}
}
function carrierName_val() {
	var value = $('#carrierName').val();
	clearError('carrierName_error');
	if (!isEmpty(value)) {
		if (!isValidOtherInformation25(value)) {
			setError('carrierName_error', INVALID_FORMAT);
			return false;
		}
	}
	setFocusLast('senderInfo');
	return true;
}

function senderInfo_val() {
	var value = $('#senderInfo').val();
	clearError('senderInfo_error');
	if (!isEmpty(value)) {
		if (!isValidOtherInformation100(value)) {
			setError('senderInfo_error', INVALID_FORMAT);
			return false;
		}
	}
	setFocusLast('print');
	return true;
}

function carrierOrSender() {
	if ($('#carrierName').val() == EMPTY_STRING || $('#senderInfo').val() == EMPTY_STRING) {
		if (!confirm(CARRIER_OR_SENDER)) {
			setFocusLast('carrierName');
			return false;
		}
	}
	return true;
}

function invoiceCycleNo_val() {
	var value = $('#invoiceCycleNo').val();
	clearError('invoiceCycleNo_error');
	if (isEmpty(value)) {
		setError('invoiceCycleNo_error', MANDATORY);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('INV_CYCLE_NUMBER', value);
	validator.setValue(ACTION, USAGE);
	validator.setClass('patterns.config.web.forms.inv.rinvoiceenvprintbean');
	validator.setMethod('validateInvoiceCycleNo');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('invoiceCycleNo_error', validator.getValue(ERROR));
		return false;
	}
	invoiceFromDay = validator.getValue('INV_RENTAL_DAY_FROM');
	invoiceUptoDay = validator.getValue('INV_RENTAL_DAY_UPTO');
	setFocusLast('invMonth');
	return true;
}

function processGetBillDetails() {
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		showMessage(getProgramID(), Message.ERROR, validator.getValue(ERROR));
	} else {
		var xmlString = EMPTY_STRING;
		if (validator.getValue(RESULT) == DATA_AVAILABLE) {
			xmlString = validator.getValue(RESULT_XML);
			inPatientBillGrid.loadXMLString(xmlString);
		}
		hideMessage(getProgramID());
	}

}

function invMonth_val() {
	var value = $('#invMonth').val();
	clearError('invMonth_error');
	if (isEmpty(value)) {
		setError('invMonth_error', MANDATORY);
		return false;
	}
	setFocusLast('invMonthYear');
	return true;
}

function invMonthYear_val() {
	var value = $('#invMonthYear').val();
	clearError('invMonth_error');
	if (isEmpty(value)) {
		setError('invMonth_error', MANDATORY);
		return false;
	}
	if (!isValidYear(value)) {
		setError('invMonth_error', INVALID_YEAR);
		return false;
	}
	if (isNumberLesser(value, '1990')) {
		setError('invMonth_error', YEAR_CANT_LE_1990);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('INV_CYCLE_NUMBER', $('#invoiceCycleNo').val());
	validator.setValue('INV_MONTH', $('#invMonth').val());
	validator.setValue('INV_MONTH_YEAR', $('#invMonthYear').val());
	validator.setValue('INV_FROM_DAY', invoiceFromDay);
	validator.setValue('INV_UPTO_DAY', invoiceUptoDay);
	validator.setValue(ACTION, USAGE);
	validator.setClass('patterns.config.web.forms.inv.rinvoiceenvprintbean');
	validator.setMethod('getEnvolopeCount');
	validator.sendAndReceive();
	if (validator.getValue('ERROR_INV_CYCLE') != EMPTY_STRING) {
		setError('invoiceCycleNo_error', validator.getValue('ERROR_INV_CYCLE'));
		hideMessage(getProgramID());
		return false;
	}
	if (validator.getValue('ERROR_INV_MONTH') != EMPTY_STRING) {
		setError('invMonth_error', validator.getValue('ERROR_INV_MONTH'));
		hideMessage(getProgramID());
		return false;
	}
	if (validator.getValue('ERROR_INV_YEAR') != EMPTY_STRING) {
		setError('invMonth_error', validator.getValue('ERROR_INV_YEAR'));
		hideMessage(getProgramID());
		return false;
	}
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('invMonth_error', validator.getValue(ERROR));
		return false;
	}
	$('#invoiceFromDate').val(validator.getValue("INV_FROM_DATE"));
	$('#invoiceUptoDate').val(validator.getValue("INV_UPTO_DATE"));
	$('#noOfEnvelopes').val(validator.getValue("ENVOLOPE_COUNT"));
	setFocusLast('carrierName');
	return true;
}
// ad
function doPrint() {
	if (carrierOrSender()) {
		showMessage(getProgramID(), Message.PROGRESS);
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('INV_CYCLE_NUMBER', $('#invoiceCycleNo').val());
		validator.setValue('INV_MONTH', $('#invMonth').val());
		validator.setValue('INV_MONTH_YEAR', $('#invMonthYear').val());
		validator.setValue('INV_FROM_DATE', $('#invoiceFromDate').val());
		validator.setValue('INV_UPTO_DATE', $('#invoiceUptoDate').val());

		validator.setValue('CARRIER_NAME', $('#carrierName').val());
		validator.setValue('SENDER_INFO', $('#senderInfo').val());

		validator.setValue('VALIDATE_COURIER_DETAILS', ONE);
		validator.setValue('REPORT_TYPE', CURRENT_PROGRAM_ID);
		validator.setValue('ACTION', USAGE);
		validator.setClass('patterns.config.web.forms.inv.rinvoiceenvprintbean');
		validator.setMethod('printInvoiceEvnvolopes');
		validator.sendAndReceiveAsync(processPrintEnvolopeDetails);
	}
}

function processPrintEnvolopeDetails() {
	enableElement('print');
	if (validator.getValue('ERROR_INV_CYCLE') != EMPTY_STRING) {
		setError('invoiceCycleNo_error', validator.getValue('ERROR_INV_CYCLE'));
		hideMessage(getProgramID());
		return false;
	}
	if (validator.getValue('ERROR_INV_MONTH') != EMPTY_STRING) {
		setError('invMonth_error', validator.getValue('ERROR_INV_MONTH'));
		hideMessage(getProgramID());
		return false;
	}
	if (validator.getValue('ERROR_INV_YEAR') != EMPTY_STRING) {
		setError('invMonth_error', validator.getValue('ERROR_INV_YEAR'));
		hideMessage(getProgramID());
		return false;
	}
	if (validator.getValue('ERROR_INV_DATE_RANGE') != EMPTY_STRING) {
		setError('invMonth_error', validator.getValue('ERROR_INV_DATE_RANGE'));
		hideMessage(getProgramID());
		return false;
	}
	if (validator.getValue('ERROR_CARRIER_NAME') != EMPTY_STRING) {
		setError('carrierName_error', validator.getValue('ERROR_CARRIER_NAME'));
		hideMessage(getProgramID());
		return false;
	}
	if (validator.getValue('ERROR_SENDER_INFO') != EMPTY_STRING) {
		setError('senderInfo_error', validator.getValue('ERROR_SENDER_INFO'));
		hideMessage(getProgramID());
		return false;
	}
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		showMessage(getProgramID(), Message.ERROR, validator.getValue(ERROR));
		return false;
	}
	if (validator.getValue(ERROR) != null && validator.getValue(ERROR) != EMPTY_STRING) {
		showMessage(getProgramID(), Message.ERROR, validator.getValue(ERROR));
		return false;
	} else {
		showReportLink(validator);
		return true;
	}
}