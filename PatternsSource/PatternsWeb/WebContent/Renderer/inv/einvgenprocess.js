var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'EINVGENPROCESS';
var VIEW_DETAILS = 'einvgenprocessgridmessage';
function init() {
	clearFields();
	einvgenprocess_componentGrid.attachEvent("onRowDblClicked", viewinvComponentDetails);
	einvgenprocessGrid.attachEvent("onRowDblClicked", doRowDblClicked);
}

function clearFields() {
	setFocusLast('invoiceCycleNumber');
	$('#invoiceCycleNumber').val(EMPTY_STRING);
	$('#invoiceCycleNumber_desc').html(EMPTY_STRING);

	// $('#invMonth').val(EMPTY_STRING);
	$('#invMonth').prop('selectedIndex', 0);
	$('#invMonthYear').val(EMPTY_STRING);
	$('#genDate').val(getCBD());
	$('#invoiceBranch').val(EMPTY_STRING);
	$('#invoiceBranch_desc').html(EMPTY_STRING);
	$('#invoiceTempCode').val(EMPTY_STRING);
	$('#invoiceTempCode_desc').html(EMPTY_STRING);
	$('#leaseProdCode').val(EMPTY_STRING);
	$('#leaseProdCode_desc').html(EMPTY_STRING);
	$('#custCode').val(EMPTY_STRING);
	$('#custCode_desc').html(EMPTY_STRING);
	hideAllMessages(getProgramID());
	clearError();
	
	
	einvgenprocessGrid.clearAll();
	einvgenprocess_componentGrid.clearAll();
	einvgenprocess_breakupGrid.clearAll();
	hide('view_dtl');
	hide('view_breakupdtl');
	}

function clearError() {
	$('#invoiceCycleNumber_error').html(EMPTY_STRING);
	$('#invMonth_error').html(EMPTY_STRING);
	$('#genDate_error').html(EMPTY_STRING);
	$('#invoiceBranch_error').html(EMPTY_STRING);
	$('#invoiceTempCode_error').html(EMPTY_STRING);
	$('#leaseProdCode_error').html(EMPTY_STRING);
	$('#custCode_error').html(EMPTY_STRING);
	hideAllMessages(getProgramID());

	hideAllMessages(VIEW_DETAILS);
	$('#processXml').addClass('hidden');
}

function clearNonPKFields() {

}

function doclearfields(id) {
	switch (id) {
	case 'invoiceCycleNumber':
		if (isEmpty($('#invoiceCycleNumber').val())) {
			clearFields();
			break;
		}
	}
}

function doHelp(id) {
	switch (id) {

	case 'invoiceCycleNumber':
		// help('COMMON', 'HLP_INVOICE_CYCLE_NUMBER',
		// $('#invoiceCycleNumber').val(), EMPTY_STRING,
		// $('#invoiceCycleNumber'));

		help('COMMON', 'HLP_INVOICE_CYCLE_NUMBER', $('#invoiceCycleNumber').val(), EMPTY_STRING, $('#invoiceCycleNumber'), null, function(gridObj, rowID) {
			var fromDay = gridObj.cells(rowID, 1).getValue();
			var endDay = gridObj.cells(rowID, 2).getValue();
			$('#invoiceCycleNumber_desc').html(fromDay + " - " + endDay);
		});

		break;
	case 'invoiceBranch':
		help('COMMON', 'HLP_BRANCH_CODE', $('#invoiceBranch').val(), EMPTY_STRING, $('#invoiceBranch'));
		break;
	case 'invoiceTempCode':
		help('COMMON', 'HLP_INV_TEMPLATE_CODE', $('#invoiceTempCode').val(), EMPTY_STRING, $('#invoiceTempCode'));
		break;
	case 'leaseProdCode':
		help('COMMON', 'HLP_LEASE_PROD_CODE', $('#leaseProdCode').val(), EMPTY_STRING, $('#leaseProdCode'));
		break;
	case 'custCode':
		help('COMMON', 'HLP_DEBTOR_CODE', $('#custCode').val(), EMPTY_STRING, $('#custCode'));
		break;

	}
}
function validate(id) {
	switch (id) {
	case 'invoiceCycleNumber':
		invoiceCycleNumber_val();
		break;

	case 'invMonth':
		invoiceMonth_val();
		break;

	case 'invMonthYear':
		invoiceMonthYear_val();
		break;

	case 'genDate':
		genDate_val();
		break;

	case 'invoiceBranch':
		invoiceBranch_val();
		break;

	case 'invoiceTempCode':
		invoiceTempCode_val();
		break;

	case 'leaseProdCode':
		leaseProdCode_val();
		break;

	case 'custCode':
		custCode_val();
		break;
	case 'submit':
		processAction();
		break;
	case 'clear':
		clearFields();
		break;

	}
}

function backtrack(id) {
	switch (id) {
	case 'invMonth':
		setFocusLast('invoiceCycleNumber');
		break;
	case 'invMonthYear':
		setFocusLast('invMonth');
		break;
	case 'genDate':
		setFocusLast('invMonthYear');
		break;
	case 'invoiceBranch':
		setFocusLast('invMonthYear');
		break;
	case 'invoiceTempCode':
		setFocusLast('invoiceBranch');
		break;
	case 'leaseProdCode':
		setFocusLast('invoiceTempCode');
		break;
	case 'custCode':
		setFocusLast('leaseProdCode');
		break;
	case 'submit':
		setFocusLast('custCode');
		break;
	}
}

function invoiceCycleNumber_val() {
	var value = $('#invoiceCycleNumber').val();
	clearError('invoiceCycleNumber_error');
	if (isEmpty(value)) {
		setError('invoiceCycleNumber_error', MANDATORY);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('INV_CYCLE_NUMBER', value);
	validator.setValue(ACTION, USAGE);
	validator.setClass('patterns.config.web.forms.inv.einvgenprocessbean');
	validator.setMethod('validateInvoiceCycleNumber');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('invoiceCycleNumber_error', validator.getValue(ERROR));
		return false;
	}

	setFocusLast('invMonth');
	return true;
}

function invoiceMonth_val() {
	var value = $('#invMonth').val();
	clearError('invMonth_error');
	if (isEmpty(value)) {
		setError('invMonth_error', MANDATORY);
		return false;
	}
	setFocusLast('invMonthYear');
	return true;
}

function invoiceMonthYear_val() {
	var value = $('#invMonthYear').val();
	clearError('invMonth_error');
	if (isEmpty(value)) {
		setError('invMonth_error', MANDATORY);
		return false;
	}
	if (!isValidYear(value)) {
		setError('invMonth_error', INVALID_YEAR);
		return false;
	}
	setFocusLast('invoiceBranch');
	return true;
}

function invoiceBranch_val() {
	var value = $('#invoiceBranch').val();
	clearError('invoiceBranch_error');
	if (!isEmpty(value)) {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('BRANCH_CODE', value);
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.inv.einvgenprocessbean');
		validator.setMethod('validateInvoiceBranchCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('invoiceBranch_error', validator.getValue(ERROR));
			return false;
		}
		if (validator.getValue(RESULT) == DATA_AVAILABLE) {
			$('#invoiceBranch_desc').html(validator.getValue('BRANCH_NAME'));
		}
	}
	setFocusLast('invoiceTempCode');
	return true;
}

function invoiceTempCode_val() {
	var value = $('#invoiceTempCode').val();
	clearError('invoiceTempCode_error');
	if (!isEmpty(value)) {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('INVTEMPLATE_CODE', value);
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.inv.einvgenprocessbean');
		validator.setMethod('validateInvoiceTemplateCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('invoiceTempCode_error', validator.getValue(ERROR));
			return false;
		}
		if (validator.getValue(RESULT) == DATA_AVAILABLE) {
			$('#invoiceTempCode_desc').html(validator.getValue('DESCRIPTION'));
		}
	}
	setFocusLast('leaseProdCode');
	return true;
}

function leaseProdCode_val() {
	var value = $('#leaseProdCode').val();
	clearError('leaseProdCode_error');
	if (!isEmpty(value)) {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('LEASE_PRODUCT_CODE', value);
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.inv.einvgenprocessbean');
		validator.setMethod('validateLeaseProductCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('leaseProdCode_error', validator.getValue(ERROR));
			return false;
		}
		if (validator.getValue(RESULT) == DATA_AVAILABLE) {
			$('#leaseProdCode_desc').html(validator.getValue('DESCRIPTION'));
		}
	}
	setFocusLast('custCode');
	return true;
}

function custCode_val() {
	var value = $('#custCode').val();
	clearError('custCode_error');
	if (!isEmpty(value)) {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('SUNDRY_DB_AC', value);
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.inv.einvgenprocessbean');
		validator.setMethod('validatecustCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('custCode_error', validator.getValue(ERROR));
			return false;
		}
		if (validator.getValue(RESULT) == DATA_AVAILABLE) {
			$('#custCode_desc').html(validator.getValue('CUSTOMER_NAME'));
		}
	}
	setFocusLast('submit');
	return true;
}

function processAction() {
	clearError();
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('invoiceCycleNumber', $('#invoiceCycleNumber').val());
	validator.setValue('invMonth', $('#invMonth').val());
	validator.setValue('invMonthYear', $('#invMonthYear').val());
	validator.setValue('genDate', $('#genDate').val());
	validator.setValue('invoiceBranch', $('#invoiceBranch').val());
	validator.setValue('invoiceTempCode', $('#invoiceTempCode').val());
	validator.setValue('leaseProdCode', $('#leaseProdCode').val());
	validator.setValue('custCode', $('#custCode').val());
	validator.setValue(ACTION, USAGE);
	validator.setClass('patterns.config.web.forms.inv.einvgenprocessbean');
	validator.setMethod('processAction');
	validator.sendAndReceiveAsync(checkResult);
	$('#po_viewSubmit').addClass('hidden');
	showMessage(getProgramID(), Message.PROGRESS);
}


function checkResult() {
	
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		if (validator.getValue(ERROR_FIELD) != EMPTY_STRING) {
			hideAllMessages(getProgramID());
			setError(validator.getValue(ERROR_FIELD) + '_error', validator.getValue(ERROR));
			setFocusLast(validator.getValue(ERROR_FIELD));
		} else {
			showMessage(getProgramID(), Message.ERROR, validator.getValue(ERROR));
		}

	} else {
		var msg = validator.getValue(RESULT);
		if (validator.getValue(ADDITIONAL_INFO) != EMPTY_STRING) {
			msg = msg + ' - ' + validator.getValue(ADDITIONAL_INFO);
		}
		if (validator.getValue(STATUS) == SUCCESS) {
			showMessage(getProgramID(), Message.SUCCESS, msg);
		} else {
			showMessage(getProgramID(), Message.ERROR, msg);
		}

	}
	$('#po_viewSubmit').removeClass('hidden');

}

/*function checkResult() {
	alert(validator.getValue(ERROR));
	alert(validator.getValue(ERROR_FIELD));
	alert(validator.getValue(ADDITIONAL_INFO));
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		if (validator.getValue(ERROR_FIELD) != EMPTY_STRING) {
			hideAllMessages(getProgramID());
			setError(validator.getValue(ERROR_FIELD) + '_error', validator.getValue(ERROR));
			setFocusLast(validator.getValue(ERROR_FIELD));
		} else {
			var msg = validator.getValue(ERROR);
			if (validator.getValue(ADDITIONAL_INFO) != EMPTY_STRING) {
				msg = msg + ' - ' + validator.getValue(ADDITIONAL_INFO);
			}
			showMessage(getProgramID(), Message.ERROR,msg);
		}

	} else {
		var msg = validator.getValue(RESULT);
		if (validator.getValue(ADDITIONAL_INFO) != EMPTY_STRING) {
			msg = msg + ' - ' + validator.getValue(ADDITIONAL_INFO);
		}
		showMessage(getProgramID(), Message.SUCCESS, msg);
	}
	$('#po_viewSubmit').removeClass('hidden');

}*/

function showResultGrid(tempSerial) {
	showMessage(VIEW_DETAILS, Message.PROGRESS);
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('TEMP_SERIAL', tempSerial);
	validator.setClass('patterns.config.web.forms.inv.einvgenprocessbean');
	validator.setMethod('showResultGrid');
	validator.sendAndReceiveAsync(loadResultGrid);
}
function loadResultGrid() {
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		showMessage(VIEW_DETAILS, Message.ERROR, validator.getValue(ERROR));
		return false;
	}
	if (validator.getValue(RESULT) == DATA_UNAVAILABLE) {
		showMessage(VIEW_DETAILS, Message.WARN, NO_RECORD);
		return false;
	}
	if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		$('#processXml').removeClass('hidden');
		einvgenprocessGrid.clearAll();
		einvgenprocessGrid.loadXMLString(validator.getValue(RESULT_XML));
		hideMessage(VIEW_DETAILS);
	}
}





//Add on 07-04-2017 popup GridDetails
function doRowDblClicked(rowID) {
	hide('view_breakupdtl');
	hideMessage('breakup');
	einvgenprocess_componentGrid.clearAll();
	einvgenprocess_breakupGrid.clearAll();
	$('#invoiceBreakUp').html(EMPTY_STRING);
	win = showWindow('view_dtl', EMPTY_STRING, COMPONENT_DETAILS, EMPTY_STRING, true, false, false, 1260, 560);
	showMessage('component', Message.PROGRESS);
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('ENTRY_DATE', einvgenprocessGrid.cells(rowID, 9).getValue());
	validator.setValue('ENTRY_SL', einvgenprocessGrid.cells(rowID, 10).getValue());
	validator.setClass('patterns.config.web.forms.inv.einvgenprocessbean');
	validator.setMethod('loadComponentDetails');
	validator.sendAndReceiveAsync(showInnerGrid);
}

function showInnerGrid() {
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		showMessage('component', Message.ERROR, validator.getValue(ERROR));
		return false;
	}
	if (validator.getValue(RESULT) == DATA_UNAVAILABLE) {
		showMessage('component', Message.WARN, NO_RECORD);
		return false;
	}
	if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		einvgenprocess_componentGrid.clearAll();
		einvgenprocess_componentGrid.loadXMLString(validator.getValue(RESULT_XML));
		hideMessage('component');
	}
}

function viewBreakUpDetails(primaryKey) {
	hide('view_breakupdtl');
	showMessage('breakup', Message.PROGRESS);
	var pk = primaryKey.split('|');
	var entryDate = pk[0];
	var entrySl = pk[1];
	var breakUp = pk[2] + " / " + pk[3] + " / " + pk[4];
	einvgenprocess_breakupGrid.clearAll();
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('ENTRY_DATE', entryDate);
	validator.setValue('ENTRY_SL', entrySl);
	validator.setClass('patterns.config.web.forms.inv.einvgenprocessbean');
	validator.setMethod('loadBreakUpDetails');
	validator.sendAndReceiveAsync(showBreakupGrid);
	$('#invoiceBreakUp').html(BREAKUP_DETAILS + breakUp);
}
function showBreakupGrid() {
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		showMessage('breakup', Message.ERROR, validator.getValue(ERROR));
		return false;
	}
	if (validator.getValue(RESULT) == DATA_UNAVAILABLE) {
		showMessage('breakup', Message.WARN, NO_RECORD);
		return false;
	}
	if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		einvgenprocess_breakupGrid.clearAll();
		show('view_breakupdtl');
		einvgenprocess_breakupGrid.loadXMLString(validator.getValue(RESULT_XML));
		hideMessage('breakup');
	}
}

function afterClosePopUp(win) {
	if (win.getId() == 'lease' || win.getId() == 'invcomponent') {
		dhxWindows = null;
		win = null;
		win = showWindow('view_dtl', EMPTY_STRING, COMPONENT_DETAILS, EMPTY_STRING, true, false, false, 1250, 535);
		return true;
	}
}
function viewinvComponentDetails() {
	// currently pk not passed
	window.scroll(0, 0);
	showWindow('invcomponent', getBasePath() + 'Renderer/inv/qinvcompview.jsp', 'QINVCOMPVIEW', PK + '=' + primaryKey + "&" + SOURCE + "=" + MAIN, true, false, false, 1000, 500);
	resetLoading();
}

function viewLeaseDetails(primaryKey) {
	window.scroll(0, 0);
	showWindow('lease', getBasePath() + 'Renderer/lss/qlease.jsp', QLEASE, PK + '=' + primaryKey + "&" + SOURCE + "=" + MAIN, true, false, false, 1000, 500);
	resetLoading();
}
