<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<%@taglib prefix="message" tagdir="/WEB-INF/tags/message"%>
<web:bundle baseName="patterns.config.web.forms.inv.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/inv/rinvoice.js" />
	</web:dependencies>
	<web:loadPK />
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="inv.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/inv/rinvoice" id="inv" method="POST">
				<web:viewContent id="po_view1">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="rinvoice.invoicecyclenumber" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:twoDigit property="invoiceCycleNumber" id="invoiceCycleNumber" lookup="true" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="rinvoice.invoicemonthoryear" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:yearMonth property="invoiceMonth" id="invoiceMonth" datasourceid="COMMON_MONTH" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:rowEven>
										<web:column>
											<web:button id="submit" key="form.submit" var="common" onclick="loadGrid();" />
											<web:button id="reset" key="form.reset" var="common" onclick="clearFields()" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<message:messageBox id="rinvoice" />
							</web:section>
							<web:section>
								<web:table>
									<web:rowOdd>
										<web:column>
											<web:viewContent id="po_view4">
												<web:grid height="350px" width="1000px" id="rinvoice_innerGrid" src="inv/rinvoice_innerGrid.xml">
												</web:grid>
											</web:viewContent>
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="80px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:button id="generateReport" key="rinvoice.generatereport" var="program" onclick="generateInvoiceReport();" />
										</web:column>
										<web:column>
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
						</web:sectionBlock>
					</web:dividerBlock>
				</web:viewContent>
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
	<web:contextSearch />
</web:fragment>

