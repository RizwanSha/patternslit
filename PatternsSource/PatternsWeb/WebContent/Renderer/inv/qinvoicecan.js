var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'EINVOICECAN';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
	einvoiceCanGrid.setColumnHidden(0, true);
}
function clearFields() {
	$('#cancelRefDate').val(EMPTY_STRING);
	$('#cancelRefDate_error').html(EMPTY_STRING);
	$('#cancelRefSerial').val(EMPTY_STRING);
	$('#cancelRefSerial_error').html(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
	einvoiceCanGrid.clearAll();
}

function loadGrid() {
	loadGridQuery(CURRENT_PROGRAM_ID, 'EINVOICECAN_GRID', einvoiceCanGrid);
}
function loadData() {
	$('#cancelRefDate').val(validator.getValue('ENTRY_DATE'));
	$('#cancelRefSerial').val(validator.getValue('ENTRY_SL'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadGrid();
	loadAuditFields(validator);
}
