var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'IINVSIGNPOLICY';
function init() {
	refreshTBAGrid();
	refreshQueryGrid();
	if (_redisplay) {
		if ($('#action').val() == ADD) {
			$('#enabled').prop('disabled', true);
			setCheckbox('enabled', YES);
		}
	}
}

function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'IINVSIGNPOLICY_MAIN', queryGrid);
}
function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}

function clearFields() {
	$('#lesseBranchCode').val(EMPTY_STRING);
	$('#lesseBranchCode_desc').html(EMPTY_STRING);
	$('#lesseBranchCode_error').html(EMPTY_STRING);
	$('#effectiveDate').val(EMPTY_STRING);
	$('#effectiveDate_error').html(EMPTY_STRING);
	clearNonPKFields();
}

function clearNonPKFields() {
	$('#firstSign').val(EMPTY_STRING);
	$('#firstSign_desc').html(EMPTY_STRING);
	$('#firstSign_error').html(EMPTY_STRING);
	$('#secondSign').val(EMPTY_STRING);
	$('#secondSign_desc').html(EMPTY_STRING);
	$('#secondSign_error').html(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
}

function doclearfields(id) {
	switch (id) {
	case 'lesseBranchCode':
		if (isEmpty($('#lesseBranchCode').val())) {
			clearFields();
			break;
		}
	case 'effectiveDate':
		if (isEmpty($('#effectiveDate').val())) {
			$('#effectiveDate_error').html(EMPTY_STRING);
			clearNonPKFields();
			break;
		}
	}
}

function add() {
	$('#lesseBranchCode').focus();
	$('#effectiveDate_look').prop('disabled', true);
	setCheckbox('enabled', YES);
	$('#enabled').prop('disabled', true);
}

function modify() {
	$('#lesseBranchCode').focus();
	if ($('#action').val() == MODIFY) {
		$('#enabled').prop('disabled', false);
	} else {
		$('#enabled').prop('disabled', true);
	}
}

function loadData() {
	$('#lesseBranchCode').val(validator.getValue('BRANCH_CODE'));
	$('#lesseBranchCode_desc').html(validator.getValue('F1_BRANCH_NAME'));
	$('#effectiveDate').val(validator.getValue('EFF_DATE'));
	$('#firstSign').val(validator.getValue('FIRST_SIGN_STAFF_CODE'));
	$('#firstSign_desc').html(validator.getValue('F2_NAME'));
	$('#secondSign').val(validator.getValue('SECOND_SIGN_STAFF_CODE'));
	$('#secondSign_desc').html(validator.getValue('F3_NAME'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	resetLoading();
}
function view(source, primaryKey) {
	hideParent('inv/qinvsignpolicy', source, primaryKey);
	resetLoading();
}

function doHelp(id) {
	switch (id) {
	case 'lesseBranchCode':
		help('COMMON', 'HLP_BRANCH_CODE', $('#lesseBranchCode').val(), EMPTY_STRING, $('#lesseBranchCode'));
		break;
	case 'firstSign':
		help('COMMON', 'HLP_STAFF_CODE', $('#firstSign').val(), EMPTY_STRING, $('#firstSign'));
		break;
	case 'secondSign':
		help('COMMON', 'HLP_STAFF_CODE', $('#secondSign').val(), EMPTY_STRING, $('#secondSign'));
		break;
	case 'effectiveDate':
		if ($('#action').val() == MODIFY)
		help('IINVSIGNPOLICY', 'HLP_EFF_DATE', $('#effectiveDate').val(), EMPTY_STRING, $('#effectiveDate'));
		break;
	
	}
}
function validate(id) {
	switch (id) {
	case 'lesseBranchCode':
		lesseBranchCode_val();
		break;
	case 'effectiveDate':
		effectiveDate_val();
		break;
	case 'firstSign':
		firstSign_val();
		break;
	case 'secondSign':
		secondSign_val();
		break;
	case 'enabled':
		enabled_val();
		break;
	case 'remarks':
		remarks_val();
		break;
	}
}

function backtrack(id) {
	switch (id) {
	case 'lesseBranchCode':
		setFocusLast('lesseBranchCode');
		break;
	case 'effectiveDate':
		setFocusLast('lesseBranchCode');
		break;
	case 'firstSign':
		setFocusLast('effectiveDate');
		break;
	case 'secondSign':
		setFocusLast('firstSign');
		break;
	case 'enabled':
		setFocusLast('secondSign');
		break;
	case 'remarks':
		if ($('#action').val() == MODIFY) {
			setFocusLast('enabled');
		} else {
			setFocusLast('secondSign');
		}
		break;
	}
}

function lesseBranchCode_val() {
	var value = $('#lesseBranchCode').val();
	clearError('lesseBranchCode_error');
	if (isEmpty(value)) {
		setError('lesseBranchCode_error', MANDATORY);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('BRANCH_CODE', value);
	validator.setValue(ACTION, USAGE);
	validator.setClass('patterns.config.web.forms.inv.iinvsignpolicybean');
	validator.setMethod('validateLesseBranchCode');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('lesseBranchCode_error', validator.getValue(ERROR));
		return false;
	}
	if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		$('#lesseBranchCode_desc').html(validator.getValue('BRANCH_NAME'));
	}

	setFocusLast('effectiveDate');
	return true;
}

function effectiveDate_val() {
	var value = $('#effectiveDate').val();
	clearError('effectiveDate_error');
	if (isEmpty(value)) {
		$('#effectiveDate').val(getCBD());
		value = $('#effectiveDate').val();
	}
	if (!isValidEffectiveDate(value, 'effectiveDate_error')) {
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('BRANCH_CODE', $('#lesseBranchCode').val());
		validator.setValue('EFF_DATE', value);
		validator.setValue('ACTION', $('#action').val());
		validator.setClass('patterns.config.web.forms.inv.iinvsignpolicybean');
		validator.setMethod('validateEffectiveDate');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('effectiveDate_error', validator.getValue(ERROR));
			return false;
		} else if ($('#action').val() == MODIFY) {
			PK_VALUE = getEntityCode() + PK_SEPERATOR + $('#lesseBranchCode').val() + PK_SEPERATOR + getTBADateFormat($('#effectiveDate').val());
			if (!loadPKValues(PK_VALUE, 'effectiveDate_error')) {
				clearNonPKFields();
				return false;
			}
		}
	}
	setFocusLast('firstSign');
	return true;
}
function firstSign_val() {
	var value = $('#firstSign').val();
	clearError('firstSign_error');
	if (isEmpty(value)) {
		setError('firstSign_error', MANDATORY);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('STAFF_CODE', value);
	validator.setValue(ACTION, USAGE);
	validator.setClass('patterns.config.web.forms.inv.iinvsignpolicybean');
	validator.setMethod('validateFirstSign');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('firstSign_error', validator.getValue(ERROR));
		$('#firstSign_desc').html(EMPTY_STRING);
		return false;
	}
	if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		$('#firstSign_desc').html(validator.getValue('NAME'));
	}

	setFocusLast('secondSign');
	return true;
}
function secondSign_val() {

	var value = $('#secondSign').val();
	if (!isEmpty(value)) {
		clearError('secondSign_error');
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('STAFF_CODE', value);
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.inv.iinvsignpolicybean');
		validator.setMethod('validateSecondSign');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('secondSign_error', validator.getValue(ERROR));
			$('#secondSign_desc').html(EMPTY_STRING);
			return false;
		}
		if (validator.getValue(RESULT) == DATA_AVAILABLE) {
			$('#secondSign_desc').html(validator.getValue('NAME'));
		}
		if (value == $('#firstSign').val()) {
			setError('secondSign_error', SECOND_DUPLICATE);
			return false;
		}
	}
	setFocusLast('remarks');
	return true;
}

function enabled_val() {
	if ($('#action').val() == MODIFY) {
		setFocusLast('remarks');
	}
	return true;
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', INVALID_REMARKS);
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', INVALID_REMARKS);
			return false;
		}
	}
	if ($('#action').val() == MODIFY) {
		if (isEmpty(value)) {
			setError('remarks_error', MANDATORY);
			return false;
		}
	}
	setFocusOnSubmit();
	return true;
}
