<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<%@taglib prefix="message" tagdir="/WEB-INF/tags/message"%>
<web:bundle baseName="patterns.config.web.forms.inv.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/inv/einvgenprocess.js" />
	</web:dependencies>
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="einvgenprocess.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/inv/einvgenprocess" id="einvgenprocess" method="POST">
				<web:viewContent id="po_view1">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="einvgenprocess.invoiceCycleNumber" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:invoiceCycleNumber property="invoiceCycleNumber" id="invoiceCycleNumber" />
										</web:column>
									</web:rowEven>

									<web:rowOdd>
										<web:column>
											<web:legend key="einvgenprocess.invoiceMonthYear" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:yearMonth property="invMonth" id="invMonth" datasourceid="COMMON_MONTHS" />
										</web:column>
									</web:rowOdd>

									<web:rowEven>
										<web:column>
											<web:legend key="einvgenprocess.genDate" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:date property="genDate" id="genDate" readOnly="true" />
										</web:column>
									</web:rowEven>

									<web:rowOdd>
										<web:column>
											<web:legend key="einvgenprocess.invoiceBranch" var="program" />
										</web:column>
										<web:column>
											<type:branchCode property="invoiceBranch" id="invoiceBranch" />
										</web:column>
									</web:rowOdd>

									<web:rowEven>
										<web:column>
											<web:legend key="einvgenprocess.invoiceTempCode" var="program" />
										</web:column>
										<web:column>
											<type:invoiceTemplateCode property="invoiceTempCode" id="invoiceTempCode" />
										</web:column>
									</web:rowEven>

									<web:rowOdd>
										<web:column>
											<web:legend key="einvgenprocess.leaseProdCode" var="program" />
										</web:column>
										<web:column>
											<type:leaseProductCode property="leaseProdCode" id="leaseProdCode" />
										</web:column>
									</web:rowOdd>

									<web:rowEven>
										<web:column>
											<web:legend key="einvgenprocess.custCode" var="program" />
										</web:column>
										<web:column>
											<type:invoiceTemplateCode property="custCode" id="custCode" />
										</web:column>
									</web:rowEven>

								</web:table>
							</web:section>

							<web:viewContent id="po_viewSubmit">
								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="180px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowOdd>
											<web:column>
												<web:button id="submit" key="form.submit" var="common" onclick="processAction()" />
												<web:button id="clear" key="form.clear" var="common" onclick="clearFields()" />
											</web:column>
										</web:rowOdd>
									</web:table>
								</web:section>
							</web:viewContent>
							<web:section>
								<message:messageBox id="einvgenprocess" />
							</web:section>
							<web:section>
								<message:messageBox id="einvgenprocessgridmessage" />
							</web:section>
							<web:viewContent id="processXml" styleClass="hidden">
								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="180px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowEven>
											<web:column>
												<web:grid height="280px" width="905px" id="einvgenprocessGrid" src="inv/einvgenprocess_innerGrid.xml" paging="true">
												</web:grid>
											</web:column>
										</web:rowEven>
									</web:table>
								</web:section>
							</web:viewContent>



							<web:viewContent id="view_dtl" styleClass="hidden">
								<web:section>
									<message:messageBox id="component" />
								</web:section>
								<web:section>
									<web:table>
										<web:rowOdd>
											<web:column>
												<web:grid height="275px" width="1190px" id="einvgenprocess_componentGrid" src="inv/einvgenprocess_componentGrid.xml">
												</web:grid>
											</web:column>
										</web:rowOdd>
									</web:table>
								</web:section>
								<web:section>
									<message:messageBox id="breakup" />
								</web:section>
								<web:viewContent id="view_breakupdtl" styleClass="hidden">
									<b><span id="invoiceBreakUp" style="font-size: 14px; color: black; padding: 8px;"></span></b>
									<web:section>
										<web:table>
											<web:rowOdd>
												<web:column>
													<web:grid height="180px" width="1110px" id="einvgenprocess_breakupGrid" src="inv/einvgenprocess_breakupGrid.xml">
													</web:grid>
												</web:column>
											</web:rowOdd>
										</web:table>
									</web:section>
								</web:viewContent>
							</web:viewContent>




						</web:sectionBlock>
					</web:dividerBlock>
				</web:viewContent>
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
	<web:contextSearch />
</web:fragment>
