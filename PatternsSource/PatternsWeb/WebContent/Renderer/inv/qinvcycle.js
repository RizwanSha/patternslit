var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MINVCYCLE';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}

function clearFields() {
	$('#invoiceCycleNumber').val(EMPTY_STRING);
}

function loadData() {
	$('#invoiceCycleNumber').val(validator.getValue('INV_CYCLE_NUMBER'));
	$('#invoicesForRentalDateDaysFrom').val(validator.getValue('INV_RENTAL_DAY_FROM'));
	$('#upto').val(validator.getValue('INV_RENTAL_DAY_UPTO'));
	$('#invoiceGenerationInAdvanceBy').val(validator.getValue('INV_GEN_ADV_DAYS'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadAuditFields(validator);
}
