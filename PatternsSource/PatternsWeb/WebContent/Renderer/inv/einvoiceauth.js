var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'EINVOICEAUTH';
var REPORT_PROGRAM_ID = 'RINVOICEAUTH';
var AUTHORIZE = 'A';
var REJECT = 'R';
function init() {
	$('#rowCountLabel').html(SELECTED_ROW_COUNT);
	if (!_redisplay) {
		clearFields();
	}
	setFocusLast('leaseProdCode');
	queryGrid.attachEvent("onRowDblClicked", doRowDblClicked);
	queryGrid.attachEvent("onCheckbox", function(id, ind, value) {
		if (!value) {
			$("#masterCheckBox").removeClass("fa fa-check-square-o").addClass("fa fa-square-o");
			$('#numberOfCheckedRowInGrid').html(--selectedRowCount);
			return true;
		} else if (queryGrid.getCheckedRows(0).split(',').length == queryGrid.getRowsNum()) {
			$("#masterCheckBox").removeClass("fa fa-square-o").addClass("fa fa-check-square-o");
			$("#masterCheckBox").attr('title', 'Unselect all');
		} else
			$("#masterCheckBox").attr('title', 'Select all');
		$('#numberOfCheckedRowInGrid').html(++selectedRowCount);
		return true;
	});
}
function masterCheckBox() {
	if (queryGrid.getRowsNum() >= 1) {
		if ($("#masterCheckBox").hasClass("fa-square-o")) {
			$("#masterCheckBox").removeClass("fa fa-square-o").addClass("fa fa-check-square-o");
			queryGrid.checkAll(true);
			$("#masterCheckBox").attr('title', 'Unselect all');
		} else if ($("#masterCheckBox").hasClass("fa-check-square-o")) {
			$("#masterCheckBox").removeClass("fa fa-check-square-o").addClass("fa fa-square-o");
			queryGrid.checkAll(false);
			$("#masterCheckBox").attr('title', 'Select all');
		}
	} else {
		$("#masterCheckBox").attr('title', '');
	}
	var getCheckedRows = queryGrid.getCheckedRows(0);
	if (getCheckedRows.trim() == EMPTY_STRING)
		selectedRowCount = ZERO;
	else
		selectedRowCount = getCheckedRows.split(',').length;
	$('#numberOfCheckedRowInGrid').html(selectedRowCount);
}

function doHelp(id) {
	switch (id) {
	case 'leaseProdCode':
		help('COMMON', 'HLP_LEASE_PROD_CODE', $('#leaseProdCode').val(), EMPTY_STRING, $('#leaseProdCode'));
		break;
	}
}

function doadd() {

}
function clearFields() {
	setFocusLast('leaseProdCode');
	$('#leaseProdCode').val(EMPTY_STRING);
	$('#leaseProdCode_desc').html(EMPTY_STRING);
	$('#invMonth').prop('selectedIndex', 0);
	$('#invMonthYear').val(EMPTY_STRING);
	$('#invoiceDate').val(EMPTY_STRING);
	$('#invoiceBreakUp').html(EMPTY_STRING);
	$('#action').val() == ADD;
	queryGrid.clearAll();
	einvoiceauth_innerGrid.clearAll();
	hideMessage(getProgramID());
	hide('view_dtl');
	hide('view_breakupdtl');
	resetSelectedRowCount();
	clearErrors();
}
function resetSelectedRowCount() {
	selectedRowCount = ZERO;
	$('#numberOfCheckedRowInGrid').html(selectedRowCount);
}

function unCheckMaterCheckBox() {
	$("#masterCheckBox").removeClass("fa fa-check-square-o");
	$("#masterCheckBox").addClass("fa fa-square-o");
	$("#masterCheckBox").attr('title', '');
}

function clearErrors() {
	$('#leaseProdCode_error').html(EMPTY_STRING);
	$('#invMonth_error').html(EMPTY_STRING);
	$('#invoiceDate_error').html(EMPTY_STRING);
	$('#invAuthGrid_error').html(EMPTY_STRING);
	$('#processAction_error').html(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
}

function doclearfields(id) {
	switch (id) {
	case 'leaseProdCode':
		if (isEmpty($('#leaseProdCode').val())) {
			clearFields();
			break;
		}
	case 'invMonthYear':
		if (isEmpty($('#invMonthYear').val())) {
			$('#invoiceDate').val(EMPTY_STRING);
			$('#invMonth_error').html(EMPTY_STRING);
			$('#invoiceDate_error').html(EMPTY_STRING);
			$('#invAuthGrid_error').html(EMPTY_STRING);
			$('#processAction_error').html(EMPTY_STRING);
			$('#remarks_error').html(EMPTY_STRING);
			unCheckMaterCheckBox();
			queryGrid.clearAll();
			hideMessage(getProgramID());
			selresetSelectedRowCount();
			break;
		}
	}
}

function validate(id) {
	switch (id) {
	case 'leaseProdCode':
		leaseProdCode_val();
		break;
	case 'invMonth':
		invoiceMonth_val();
		break;
	case 'invMonthYear':
		invoiceMonthYear_val();
		break;
	case 'invoiceDate':
		invoiceDate_val();
		break;
	case 'processAction':
		action_val();
		break;
	case 'remarks':
		remarks_val();
		break;
	case 'cmdfetch':
		loadPendingInvoiceRecords();
		break;
	case 'cmdsubmit':
		processRequest();
		break;
	case 'cmdreset':
		clearFields();
		break;

	}
}

function backtrack(id) {
	switch (id) {
	case 'invMonth':
		setFocusLast('leaseProdCode');
		break;
	case 'invMonthYear':
		setFocusLast('invMonth');
		break;
	case 'invoiceDate':
		setFocusLast('invMonthYear');
		break;
	case 'cmdfetch':
		setFocusLast('invMonthYear');
		break;
	case 'processAction':
		setFocusLast('invoiceDate');
		break;
	case 'remarks':
		setFocusLast('processAction');
		break;
	case 'cmdsubmit':
		setFocusLast('remarks');
		break;
	}
}

function leaseProdCode_val() {
	var value = $('#leaseProdCode').val();
	clearError('leaseProdCode_error');
	if (isEmpty(value)) {
		setError('leaseProdCode_error', MANDATORY);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('LEASE_PRODUCT_CODE', value);
	validator.setValue(ACTION, USAGE);
	validator.setClass('patterns.config.web.forms.inv.einvoiceauthbean');
	validator.setMethod('validateLeaseProductCode');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('leaseProdCode_error', validator.getValue(ERROR));
		return false;
	}
	if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		$('#leaseProdCode_desc').html(validator.getValue('DESCRIPTION'));
	}
	setFocusLast('invMonth');
	return true;
}
function invoiceMonth_val() {
	var value = $('#invMonth').val();
	clearError('invMonth_error');
	if (isEmpty(value)) {
		setError('invMonth_error', MANDATORY);
		return false;
	}
	setFocusLast('invMonthYear');
	return true;
}

function invoiceMonthYear_val() {
	var value = $('#invMonthYear').val();
	clearError('invMonth_error');
	if (isEmpty(value)) {
		setError('invMonth_error', MANDATORY);
		return false;
	}
	if (!isValidYear(value)) {
		setError('invMonth_error', INVALID_YEAR);
		return false;
	}
	setFocusLast('invoiceDate');
	return true;
}

function invoiceDate_val() {
	var value = $('#invoiceDate').val();
	clearError('invoiceDate_error');
	if (!isEmpty(value)) {
		if (!isDate(value)) {
			setError('invoiceDate_error', INVALID_DATE);
			return false;
		}
		/*
		 * if (!isDateLesserEqual(value, getCBD())) {
		 * setError('invoiceDate_error', DATE_LECBD); return false; }
		 */
	}
	setFocusLast('cmdfetch');
	return true;
}

function action_val() {
	var value = $('#processAction').val();
	clearError('processAction_error');
	if (isEmpty(value)) {
		setError('processAction_error', MANDATORY);
		return false;
	}
	setFocusLast('remarks');
	return true;
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', INVALID_REMARKS);
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', INVALID_REMARKS);
			return false;
		}
	}
	if ($('#processAction').val() == REJECT) {
		if (isEmpty(value)) {
			setError('remarks_error', MANDATORY);
			return false;

		}
	}
	setFocusLast('cmdsubmit');
	return true;
}

function loadPendingInvoiceRecords() {
	unCheckMaterCheckBox();
	clearErrors();
	queryGrid.clearAll();
	showMessage(getProgramID(), Message.PROGRESS);
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('LEASE_PRODUCT_CODE', $('#leaseProdCode').val());
	validator.setValue('INVOICE_MONTH', $('#invMonth').val());
	validator.setValue('INVOICE_YEAR', $('#invMonthYear').val());
	validator.setValue('INVOICE_DATE', $('#invoiceDate').val());
	validator.setClass('patterns.config.web.forms.inv.einvoiceauthbean');
	validator.setMethod('loadPendingRecords');
	validator.sendAndReceiveAsync(showStatus);
}

function showStatus() {
	hideMessage(getProgramID());
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		if (validator.getValue(ERROR_FIELD) != EMPTY_STRING) {
			setError(validator.getValue(ERROR_FIELD) + '_error', validator.getValue(ERROR));
		} else {
			showMessage(getProgramID(), Message.ERROR, validator.getValue(ERROR));
		}
	} else {
		if (validator.getValue(RESULT) == DATA_UNAVAILABLE) {
			showMessage(getProgramID(), Message.WARN, NO_RECORD);
			return false;
		} else if (validator.getValue(RESULT) == DATA_AVAILABLE) {
			resetSelectedRowCount();
			queryGrid.clearAll();
			queryGrid.loadXMLString(validator.getValue(RESULT_XML));
			$("#masterCheckBox").attr('title', 'Select all');
			$('#cmdfetch').blur();
		}
	}
}

function processRequest() {
	if (queryGrid.getCheckedRows(0).split(',') < ZERO) {
		alert(ATLEAST_ONE_CHECK);
		return false;
	} else {
		clearErrors();
		showMessage(getProgramID(), Message.PROGRESS);
		hide('cmdsubmit');
		hide('cmdreset');
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('LEASE_PRODUCT_CODE', $('#leaseProdCode').val());
		validator.setValue('INVOICE_MONTH', $('#invMonth').val());
		validator.setValue('INVOICE_YEAR', $('#invMonthYear').val());
		validator.setValue('XML_DATA', _grid_generatexml_function(queryGrid, [ 10, 11 ]));
		validator.setValue('REMARKS', $('#remarks').val());
		validator.setValue('PROCESS_ACTION', $('#processAction').val());
		validator.setClass('patterns.config.web.forms.inv.einvoiceauthbean');
		validator.setMethod('processRequest');
		validator.sendAndReceiveAsync(authorizeStatus);
	}
}

function authorizeStatus() {
	show('cmdsubmit');
	show('cmdreset');
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		if (validator.getValue(ERROR_FIELD) != EMPTY_STRING) {
			hideAllMessages(getProgramID());
			setError(validator.getValue(ERROR_FIELD) + '_error', validator.getValue(ERROR));
			setFocusLast(validator.getValue(ERROR_FIELD));
		} else {
			showMessage(getProgramID(), Message.ERROR, validator.getValue(ERROR));
		}
		return false;
	} else {
		var msg = validator.getValue(RESULT);
		if (validator.getValue(ADDITIONAL_INFO) != EMPTY_STRING) {
			msg = msg + ' - ' + validator.getValue(ADDITIONAL_INFO);
		}
		if (validator.getValue(STATUS) == SUCCESS) {
			alert(msg);
		} else {
			showMessage(getProgramID(), Message.ERROR, msg);
			return false;
		}
	}
	loadPendingInvoiceRecords();
	return true;
}

function revalidate() {
	errors = 0;
	if (!invoiceDate_val(false)) {
		++errors;
	}
	if (errors > 0)
		return false;
	return true;
}

function doRowSelect(rowID) {
	queryGrid.clearSelection();
	queryGrid.selectRowById(rowID, true);
	queryGrid.cells(rowID, 0).setValue(true);
}

function doRowDblClicked(rowID) {
	hide('view_breakupdtl');
	hideMessage('breakup');
	queryGrid.clearSelection();
	queryGrid.selectRowById(rowID, true);
	queryGrid.cells(rowID, 0).setValue(true);
	queryGrid.cells(rowID, 12).setValue(COLUMN_ENABLE);
	einvoiceauth_innerGrid.clearAll();
	einvoiceauth_breakupGrid.clearAll();
	$('#invoiceBreakUp').html(EMPTY_STRING);
	win = showWindow('view_dtl', EMPTY_STRING, COMPONENT_DETAILS, EMPTY_STRING, true, false, false, 1250, 620);
	showMessage('component', Message.PROGRESS);
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('ENTRY_DATE', queryGrid.cells(rowID, 10).getValue());
	validator.setValue('ENTRY_SL', queryGrid.cells(rowID, 11).getValue());
	validator.setClass('patterns.config.web.forms.inv.einvoiceauthbean');
	validator.setMethod('loadComponentDetails');
	validator.sendAndReceiveAsync(showInnerGrid);
}

function showInnerGrid() {
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		showMessage('component', Message.ERROR, validator.getValue(ERROR));
		return false;
	}
	if (validator.getValue(RESULT) == DATA_UNAVAILABLE) {
		showMessage('component', Message.WARN, NO_RECORD);
		return false;
	}
	if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		einvoiceauth_innerGrid.clearAll();
		einvoiceauth_innerGrid.attachEvent("onRowDblClicked", viewinvComponentDetails);
		einvoiceauth_innerGrid.loadXMLString(validator.getValue(RESULT_XML));
		hideMessage('component');
	}
}

function viewBreakUpDetails(primaryKey) {
	// iprodccyconf_innerGrid.setColumnHidden(0, true);
	hide('view_breakupdtl');
	showMessage('breakup', Message.PROGRESS);
	var pk = primaryKey.split('|');
	var entryDate = pk[0];
	var entrySl = pk[1];
	var breakUp = pk[2] + " / " + pk[3] + " / " + pk[4];
	einvoiceauth_breakupGrid.clearAll();
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('ENTRY_DATE', entryDate);
	validator.setValue('ENTRY_SL', entrySl);
	validator.setClass('patterns.config.web.forms.inv.einvoiceauthbean');
	validator.setMethod('loadBreakUpDetails');
	validator.sendAndReceiveAsync(showBreakupGrid);
	$('#invoiceBreakUp').html(BREAKUP_DETAILS + breakUp);
}
function showBreakupGrid() {
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		showMessage('breakup', Message.ERROR, validator.getValue(ERROR));
		return false;
	}
	if (validator.getValue(RESULT) == DATA_UNAVAILABLE) {
		showMessage('breakup', Message.WARN, NO_RECORD);
		return false;
	}
	if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		einvoiceauth_breakupGrid.clearAll();
		show('view_breakupdtl');
		// einvoiceauth_breakupGrid.setColumnHidden(4, true);
		// einvoiceauth_breakupGrid.setColumnHidden(5, true);
		// <div id="gridbox" style="width:600px;height:400px;"></div>
		einvoiceauth_breakupGrid.loadXMLString(validator.getValue(RESULT_XML));
		hideMessage('breakup');
	}
}

function doView() {
	var rowID = queryGrid.getCheckedRows(0).split(',');
	if (rowID.length > 1) {
		alert(SELECT_ONE);
		return;
	} else if (rowID < ZERO) {
		alert(ATLEAST_ONE_CHECK);
		return false;
	}
	doRowDblClicked(rowID);
	queryGrid.cells(rowID, 12).setValue(COLUMN_ENABLE);
}

function afterClosePopUp(win) {
	if (win.getId() == 'lease' || win.getId() == 'invcomponent') {
		dhxWindows = null;
		win = null;
		win = showWindow('view_dtl', EMPTY_STRING, COMPONENT_DETAILS, EMPTY_STRING, true, false, false, 1250, 620);
		return true;
	}
}

function viewinvComponentDetails() {
	window.scroll(0, 0);
	showWindow('invcomponent', getBasePath() + 'Renderer/inv/qinvcompview.jsp', 'QINVCOMPVIEW', PK + '=' + primaryKey + "&" + SOURCE + "=" + MAIN, true, false, false, 1000, 500);
	resetLoading();
}

function viewLeaseDetails(primaryKey) {
	window.scroll(0, 0);
	showWindow('lease', getBasePath() + 'Renderer/lss/qlease.jsp', QLEASE, PK + '=' + primaryKey + "&" + SOURCE + "=" + MAIN, true, false, false, 1000, 500);
	resetLoading();
}

// add on 26-03-2017
function doPrint() {
	clearErrors();
	if (revalidate()) {
		disableElement('xlsDiv');
		showMessage(getProgramID(), Message.PROGRESS);
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('LEASE_PRODUCT_CODE', $('#leaseProdCode').val());
		validator.setValue('INVOICE_MONTH', $('#invMonth').val());
		validator.setValue('INVOICE_YEAR', $('#invMonthYear').val());
		validator.setValue('INVOICE_DATE', $('#invoiceDate').val());
		validator.setValue('REPORT_FORMAT', EMPTY_STRING);
		validator.setValue('REPORT_TYPE', REPORT_PROGRAM_ID);
		validator.setClass('patterns.config.web.forms.inv.einvoiceauthbean');
		validator.setMethod('doPrint');
		validator.sendAndReceiveAsync(processDownloadResult);
	}
}
function processDownloadResult() {
	enableElement('xlsDiv');
	hideMessage(getProgramID());
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		if (validator.getValue(ERROR_FIELD) != EMPTY_STRING) {
			hideAllMessages(getProgramID());
			setError(validator.getValue(ERROR_FIELD) + '_error', validator.getValue(ERROR));
		} else {
			showMessage(getProgramID(), Message.ERROR, validator.getValue(ERROR));
		}
	} else if (validator.getValue(RESULT) == DATA_UNAVAILABLE) {
		showMessage(getProgramID(), Message.WARN, NO_RECORD);
		return false;
	} else if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		var link = getBasePath() + 'servlet/ReportDownloadProcessor?' + REPORT_ID + '=' + validator.getValue(REPORT_ID);
		var win = window.open(link, '_blank');
		if (win)
			win.focus();
	}
}
