<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<%@taglib prefix="message" tagdir="/WEB-INF/tags/message"%>
<web:bundle baseName="patterns.config.web.forms.inv.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/inv/rinvoiceseqprint.js" />
	</web:dependencies>
	<web:loadPK />
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="inv.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/inv/rinvoiceseqprint" id="inv" method="POST">
				<web:viewContent id="po_view1">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="rinvoiceseqprint.billingcycle" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:twoDigit property="billingCycle" id="billingCycle" lookup="true"/>
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="rinvoiceseqprint.formonthyear" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:yearMonth property="month" id="month" datasourceid="COMMON_MONTH" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle width="100px" />
										<web:columnStyle width="10px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="rinvoiceseqprint.invoicedaterange" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:dateDisplay property="fromDate" id="fromDate" />
										</web:column>
										<web:column>
											<web:legend key="rinvoiceseqprint.minus" var="program" />
										</web:column>
										<web:column>
											<type:dateDisplay property="uptoDate" id="uptoDate" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="rinvoiceseqprint.numberofinvoices" var="program" />
										</web:column>
										<web:column>
											<type:fiveDigitDisplay property="nofInvoices" id="nofInvoices" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="rinvoiceseqprint.printingmode" var="program" />
										</web:column>
										<web:column>
											<type:combo property="printingMode" id="printingMode" datasourceid="RINVOICESEQPRINT_PRINT_MODE"/>
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="rinvoiceseqprint.numberofinvoicesinasplit" var="program"  />
										</web:column>
										<web:column>
											<type:fiveDigit property="nofInvoicesInSplit" id="nofInvoicesInSplit"  />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="rinvoiceseqprint.numberofgroupedinvoices" var="program" />
										</web:column>
										<web:column>
											<type:fiveDigitDisplay property="nofGroupedInvoices" id="nofGroupedInvoices" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:rowOdd>
										<web:column>
											<web:button id="print" key="form.print" var="common" onclick="doPrint();" />
											<web:button id="reset" key="form.reset" var="common" onclick="clearFields()" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<message:messageBox id="rinvoiceseqprint" />
							</web:section>
						</web:sectionBlock>
					</web:dividerBlock>
				</web:viewContent>
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
	<web:contextSearch />
</web:fragment>

