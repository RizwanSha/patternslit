<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<%@taglib prefix="message" tagdir="/WEB-INF/tags/message"%>
<web:bundle baseName="patterns.config.web.forms.inv.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/inv/einvoicecan.js" />
	</web:dependencies>
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="einvoicecan.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/inv/einvoicecan" id="einvoicecan" method="POST">
				<web:dividerBlock>
					<type:optionBox add="true" modify="false" view="true" />
				</web:dividerBlock>
				<web:viewContent id="po_view1" styleClass="hidden">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="einvoicecan.cancelref" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:dateDaySL property="cancelRef" id="cancelRef" readOnly="true" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle width="350px" />
										<web:columnStyle width="100px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="einvoicecan.productcode" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:productCode property="leaseProductCode" id="leaseProductCode" />
										</web:column>
										<web:column>
											<web:legend key="einvoicecan.monthyear" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:yearMonth property="invMonth" id="invMonth" datasourceid="COMMON_MONTHS" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:viewContent id="additionalFilter" styleClass="hidden">
								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="200px" />
											<web:columnStyle width="350px" />
											<web:columnStyle width="100px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowEven>
											<web:column>
												<web:legend key="einvoicecan.statecode" var="program" />
											</web:column>
											<web:column>
												<type:stateCode property="stateCode" id="stateCode" />
											</web:column>
											<web:column>
												<web:legend key="einvoicecan.duedate" var="program" />
											</web:column>
											<web:column>
												<type:date property="dueDate" id="dueDate" />
											</web:column>
										</web:rowEven>
									</web:table>
								</web:section>
								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="200px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowOdd>
											<web:column>
												<web:legend key="einvoicecan.customerid" var="program" />
											</web:column>
											<web:column>
												<type:customerID property="customerID" id="customerID" />
											</web:column>
										</web:rowOdd>
									</web:table>
								</web:section>
							</web:viewContent>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="850" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<type:button var="program" onclick="loadInvoiceDetails()" key="einvoicecan.fetch" id="fetch" />
										</web:column>
										<web:column>
											<type:button var="program" onclick="viewHideFilters()" key="einvoicecan.showfilter" id="filter" />
										</web:column>
										<web:column>
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<message:messageBox id="einvoicecan" />
								<type:error property="einvoiceCanGridValue"/>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column span="2">
											<web:grid height="270px" width="1013px" id="einvoiceCanGrid" src="inv/einvoicecan_innerGrid.xml" paging="true">
											</web:grid>
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="form.remarks" var="common" mandatory="true" />
										</web:column>
										<web:column>
											<type:remarks property="remarks" id="remarks" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column span="2">
											<web:submitReset />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
						</web:sectionBlock>
					</web:dividerBlock>
				</web:viewContent>
				<web:element property="command" />
				<web:element property="action" />
				<web:element property="xmleinvoiceCanGrid" />
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
	<web:queryGrid />
	<web:contextSearch />
</web:fragment>
