var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'QINVOICES';
var PDF_REPORT_IDENTIFIER = EMPTY_STRING;
var EXCEL_REPORT_IDENTIFIER = EMPTY_STRING;
var pkvalues = EMPTY_STRING;
function init() {
	clearFields();
	if (!isEmpty(_primaryKey)) {
		 pkvalues = _primaryKey.split(PK_SEPERATOR);
		loadInvoiceDetails(pkvalues);
	}
}
function loadData() {
}
function clearFields() {
}
function clearNonPKFields() {
}
function clearGridFields() {
}
function loadInvoiceDetails(pk) {
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('LEASE_PRODUCT_CODE', pkvalues[1]);
	validator.setValue('INV_YEAR', pkvalues[2]);
	validator.setValue('INV_MONTH', pkvalues[3]);
	validator.setValue('INV_SERIAL', pkvalues[4]);
	validator.setClass('patterns.config.web.forms.inv.qinvoicesbean');
	validator.setMethod('getInvoiceDetails');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		return false;
	} else {
		$('#invoiceNum').val(validator.getValue('INVOICE_NO'));
		$('#invDate').val(validator.getValue('INV_DATE'));
		$('#dueDate').val(validator.getValue('DUE_DATE'));
		$('#customerID').val(validator.getValue('CUSTOMER_ID'));
		$('#customerID_desc').html(validator.getValue('CUSTOMER_NAME'));
		$('#productCode').val(validator.getValue('LEASE_PRODUCT_CODE'));
		$('#productCode_desc').html(validator.getValue('PRODUCT_DESC'));
		$('#invoiceType').val(validator.getValue('INV_TYPE'));
		$('#templateCode').val(validator.getValue('INVOICE_TEMPLATE'));
		$('#templateCode_desc').html(validator.getValue('INVTEMPLATECODE_DESC'));
		$('#stateCode').val(validator.getValue('STATE_CODE'));
		$('#stateCode_desc').html(validator.getValue('STATE_DESC'));
		$('#asset_desc').html(validator.getValue('ASSET_DESC'));
		$('#invConso').val(validator.getValue('TOTAL_INV'));
		$('#fromDate').val(validator.getValue('RENTAL_START_DATE'));
		$('#toDate').val(validator.getValue('RENTAL_END_DATE'));
		$('#taxAmt').val(validator.getValue('TAX_AMT'));
		$('#taxAmt_curr').val(validator.getValue('INV_CCY'));
		$('#invAmt').val(validator.getValue('TOTAL_INV_AMT'));
		$('#invAmt_curr').val(validator.getValue('INV_CCY'));
		$('#generate').val(validator.getValue('CREATED_BY'));
		$('#genDate').val(validator.getValue('CREATED_ON'));
		setCheckbox('invPrint', validator.getValue('IS_PDF_GENERATED'));
		PDF_REPORT_IDENTIFIER = validator.getValue('PDF_REPORT_IDENTIFIER');
		EXCEL_REPORT_IDENTIFIER = validator.getValue('EXCEL_REPORT_IDENTIFIER');
		xmlString = validator.getValue(RESULT_XML);
		qinvoices_innerGrid.loadXMLString(xmlString);
		setCheckbox('invMail', validator.getValue('EMAIL_SENT'));
		$('#taxAmtFormat').val(validator.getValue('TAX_AMT'));
		$('#invAmtFormat').val(validator.getValue('TOTAL_INV_AMT'));
		if (validator.getValue('EMAIL_SENT') == ZERO)
			$('#mailSentLogButton').addClass('hidden');
		else
			$('#mailSentLogButton').removeClass('hidden');
	}
}
function viewInvDetails(pkValueComp) {
	var pkValue = getEntityCode() + PK_SEPERATOR + pkValueComp;
	window.scroll(0, 0);
	showWindow('PREVIEW_VIEW', getBasePath() + 'Renderer/inv/qinvcompview.jsp', 'QINVCOMPVIEW', PK + '=' + pkValue + "&" + SOURCE + "=" + MAIN, true, false, false, 1175, 575);
	resetLoading();
}
function clearSentRcpFields() {
	mailSentRcp_innerGrid.clearAll();
	$('#mailSentRecpLog_error').html(EMPTY_STRING);
	$('#invoiceRecpDate').val(EMPTY_STRING);
	$('#customerNameRecp').val(EMPTY_STRING);
	$('#attachment').removeClass('hidden');
}
function viewAddressRcp(EmailLogInvNo) {
	var attachment = false;
	clearSentRcpFields();
	validator.clearMap();
	validator.setMtm(false);
	$('#customerNameRecp').val($('#customerID_desc').html());
	$('#invoiceRecpDate').val($('#invDate').val());
	validator.setValue('EMAIL_LOG_INVENTORY_NO', EmailLogInvNo);
	validator.setValue('PDF_REPORT_IDENTIFIER', PDF_REPORT_IDENTIFIER);
	validator.setValue('EXCEL_REPORT_IDENTIFIER', EXCEL_REPORT_IDENTIFIER);
	validator.setClass('patterns.config.web.forms.inv.qinvoicesbean');
	validator.setMethod('viewAddressRcp');
	win = showWindow('mailSentRecpLog', EMPTY_STRING, EMAIL_SENT_DETAILS, '', true, false, false, 780, 470);
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('mailSentRecpLog_error', validator.getValue(ERROR));
		return false;
	} else if (validator.getValue(RESULT) == DATA_UNAVAILABLE) {
		setError('mailSentRecpLog_error', NO_ROWS_AVAILABLE);
		return false;
	} else if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		xmlString = validator.getValue(RESULT_XML);
		mailSentRcp_innerGrid.loadXMLString(xmlString);
		if (PDF_REPORT_IDENTIFIER == EMPTY_STRING && validator.getValue("PDF_REPORT_FILE_NAME").trim() == EMPTY_STRING) {
			$('#pdfDiv').hide();
		} else {
			$('#pdfDiv').show();
			$('#pdfCaption').html(validator.getValue("PDF_REPORT_FILE_NAME"));
			$('#pdf').attr('href', getBasePath() + 'servlet/ReportDownloadProcessor?' + REPORT_ID + '=' + PDF_REPORT_IDENTIFIER);
			$('#pdfFile').attr('href', getBasePath() + 'servlet/ReportDownloadProcessor?' + REPORT_ID + '=' + PDF_REPORT_IDENTIFIER);
			attachment = true;
		}
		if (EXCEL_REPORT_IDENTIFIER == EMPTY_STRING && validator.getValue("EXCEL_REPORT_FILE_NAME").trim() == EMPTY_STRING) {
			$('#xlsDiv').hide();
		} else {
			$('#xlsDiv').show();
			$('#xlsCaption').html(validator.getValue("EXCEL_REPORT_FILE_NAME"));
			$('#xls').attr('href', getBasePath() + 'servlet/ReportDownloadProcessor?' + REPORT_ID + '=' + EXCEL_REPORT_IDENTIFIER);
			$('#xlsFile').attr('href', getBasePath() + 'servlet/ReportDownloadProcessor?' + REPORT_ID + '=' + EXCEL_REPORT_IDENTIFIER);
			attachment = true;
		}
		if (!attachment)
			$('#attachment').addClass('hidden');
	}
}
function afterClosePopUp(win) {
	switch (win._idd) {
	case 'mailSentRecpLog':
		dhxWindows = null;
		win = showWindow('mailSentLog', EMPTY_STRING, EMAIL_SENT_DETAILS, '', true, false, false, 810, 390);
		break;
	}
}
function clearSentDetailsFields() {
	mailSentLog_innerGrid.clearAll();
	$('#invoiceDate').val(EMPTY_STRING);
	$('#mailSentLogError_error').html(EMPTY_STRING);
}
function viewSentDetails() {
	win = showWindow('mailSentLog', EMPTY_STRING, EMAIL_SENT_DETAILS, '', true, false, false, 810, 390);
	clearSentDetailsFields();
	invLogPkArray = $('#invoiceNum').val().split('|');
	$('#invoiceDate').val($('#invDate').val());
	$('#customerName').val($('#customerID_desc').html());
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('LEASE_PRODUCT_CODE', pkvalues[1]);
	validator.setValue('INV_YEAR', pkvalues[2]);
	validator.setValue('INV_MONTH', pkvalues[3]);
	validator.setValue('INV_SERIAL', pkvalues[4]);
	validator.setClass('patterns.config.web.forms.inv.qinvoicesbean');
	validator.setMethod('viewSentDetails');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('mailSentLogError_error', validator.getValue(ERROR));
		return false;
	} else if (validator.getValue(RESULT) == DATA_UNAVAILABLE) {
		setError('mailSentLogError_error', NO_ROWS_AVAILABLE);
		return false;
	} else if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		xmlString = validator.getValue(RESULT_XML);
		mailSentLog_innerGrid.loadXMLString(xmlString);
	}
}
