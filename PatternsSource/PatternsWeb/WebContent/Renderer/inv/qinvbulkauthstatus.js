var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'QINVBULKAUTHSTATUS';
function init() {
	$('#authSuccessLabel').html(STATUS_SUCCESS);
	$('#authUnderProcessLabel').html(STATUS_UNDER_PROCESS);
	$('#authFailureLabel').html(STATUS_FAILURE);
	$('#authTotalLabel').html(Total_Count);
	$('#authFetchProcessLabel').html(STATUS_FETCH);
	clearFields();
	setFocusLast('dueOnDate');
	qinvbulkauthstatus_innerGrid.attachEvent("onRowDblClicked", doRowDblClicked);
}

function doHelp(id) {
	switch (id) {
	case 'dueOnDate':
		help('COMMON', 'HLP_INVOICE_DATE', $('#dueOnDate').val(), EMPTY_STRING, $('#dueOnDate'));
		break;
	}
}

function clearFields() {
	if (isEmpty($('#dueOnDate').val()))
		$('#dueOnDate').val(getCBD());
	$('#processStatus').prop('selectedIndex', 0);
	$('#authSuccess').html(ZERO);
	$('#authUnderProcess').html(ZERO);
	$('#authFailure').html(ZERO);
	$('#authTotal').html(ZERO);
	$('#authFetchProcess').html(ZERO);
	clearFieldsError();
}
function doclearfields(id) {
	switch (id) {
	case 'dueOnDate':
		if (isEmpty($('#dueOnDate').val())) {
			clearFields();
		}
		break;
	}
}
function clearFieldsError() {
	$('#dueOnDate_error').html(EMPTY_STRING);
	$('#dueOnDate_desc').html(EMPTY_STRING);
	$('#processStatus_desc').html(EMPTY_STRING);
	$('#processStatus_error').html(EMPTY_STRING);
	qinvbulkauthstatus_innerGrid.clearAll();
	hideMessage(getProgramID());
}

function validate(id) {
	switch (id) {
	case 'dueOnDate':
		dueOnDate_val();
		break;
	case 'processStatus':
		processStatus_val();
		break;
	case 'submit':
		loadGrid();
		break;
	case 'reset':
		clearFields();
		break;

	}
}

function backtrack(id) {
	switch (id) {
	case 'processStatus':
		setFocusLast('dueOnDate');
		break;
	case 'submit':
		setFocusLast('processStatus');
		break;
	case 'reset':
		setFocusLast('dueOnDate');
		break;
	}
}

function dueOnDate_val(valMode) {
	var value = $('#dueOnDate').val();
	clearError('dueOnDate_error');
	if (isEmpty(value)) {
		$('#dueOnDate').val(getCBD());
		value = $('#dueOnDate').val();
	}
	if (!isDate(value)) {
		setError('dueOnDate_error', INVALID_DATE);
		return false;
	}
	/*
	 * if (!isDateLesserEqual(value, getCBD())) { setError('dueOnDate_error',
	 * DATE_LECBD); return false; }
	 */
	setFocusLast('processStatus');
	return true;
}
function processStatus_val(valMode) {
	var value = $('#processStatus').val();
	clearError('processStatus_error');
	if (isEmpty(value)) {
		setError('processStatus_error', MANDATORY);
		return false;
	}
	setFocusLast('submit');
	return true;
}

function loadGrid() {
	clearFieldsError();
	qinvbulkauthstatus_innerGrid.clearAll();
	showMessage(getProgramID(), Message.PROGRESS);
	qinvbulkauthstatus_innerGrid.clearAll();
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('DUE_DATE', $('#dueOnDate').val());
	validator.setValue('PROC_STATUS', $('#processStatus').val());
	validator.setClass('patterns.config.web.forms.inv.qinvbulkauthstatusbean');
	validator.setMethod('loadGrid');
	validator.sendAndReceiveAsync(loadGridAsync);
}
function loadGridAsync() {
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		showMessage(getProgramID(), Message.ERROR, validator.getValue(ERROR));
		return false;
	} else if (validator.getValue(RESULT) == DATA_UNAVAILABLE) {
		showMessage(getProgramID(), Message.WARN, NO_RECORD);
	} else if (validator.getValue('ERROR_PRESENT') == ONE) {
		hideMessage(CURRENT_PROGRAM_ID);
		if (validator.getValue('DUE_DATE_ERROR').trim() != EMPTY_STRING)
			$('#dueOnDate_error').html(validator.getValue('DUE_DATE_ERROR'));
		if (validator.getValue('PROC_STATUS_ERROR').trim() != EMPTY_STRING)
			$('#processStatus_error').html(validator.getValue('PROC_STATUS_ERROR'));
	} else if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		xmlString = validator.getValue(RESULT_XML);
		qinvbulkauthstatus_innerGrid.loadXMLString(xmlString);
		hideMessage(CURRENT_PROGRAM_ID);
		if (validator.getValue('SUCCESS') != ZERO) {
			$('#authSuccess').html(validator.getValue('SUCCESS'));
		} else {
			$('#authSuccess').html(ZERO);
		}
		if (validator.getValue("UNDERPROCESS") != ZERO) {
			$('#authUnderProcess').html(validator.getValue('UNDERPROCESS'));
		} else {
			$('#authUnderProcess').html(ZERO);
		}
		if (validator.getValue("FAILURE") != ZERO) {
			$('#authFailure').html(validator.getValue('FAILURE'));
		} else {
			$('#authFailure').html(ZERO);
		}
		if (validator.getValue("FETCH") != ZERO) {
			$('#authFetchProcess').html(validator.getValue('FETCH'));
		} else {
			$('#authFetchProcess').html(ZERO);
		}
		$('#authTotal').html(validator.getValue('TOTAL'));
		$('#submit').blur();
	}
	return true;
}

// popup
function doRowDblClicked(rowID) {
	hide('view_breakupdtl');
	hideMessage('breakup');
	qinvbulkauthstatus_innerGrid.clearSelection();
	qinvbulkauthstatus_detailGrid.clearAll();
	qinvbulkauthstatus_breakupGrid.clearAll();
	$('#invoiceBreakUp').html(EMPTY_STRING);
	win = showWindow('view_dtl', EMPTY_STRING, COMPONENT_DETAILS, EMPTY_STRING, true, false, false, 1255, 535);
	showMessage('component', Message.PROGRESS);
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('ENTRY_DATE', qinvbulkauthstatus_innerGrid.cells(rowID, 8).getValue());
	validator.setValue('ENTRY_SL', qinvbulkauthstatus_innerGrid.cells(rowID, 9).getValue());
	validator.setClass('patterns.config.web.forms.inv.qinvbulkauthstatusbean');
	validator.setMethod('loadComponentDetails');
	validator.sendAndReceiveAsync(showInnerGrid);
}

function showInnerGrid() {
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		showMessage('component', Message.ERROR, validator.getValue(ERROR));
		return false;
	}
	if (validator.getValue(RESULT) == DATA_UNAVAILABLE) {
		showMessage('component', Message.WARN, NO_RECORD);
		return false;
	}
	if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		qinvbulkauthstatus_detailGrid.clearAll();
		qinvbulkauthstatus_detailGrid.attachEvent("onRowDblClicked", viewinvComponentDetails);
		qinvbulkauthstatus_detailGrid.loadXMLString(validator.getValue(RESULT_XML));
		hideMessage('component');
	}
}

function viewBreakUpDetails(primaryKey) {
	// iprodccyconf_innerGrid.setColumnHidden(0, true);
	hide('view_breakupdtl');
	showMessage('breakup', Message.PROGRESS);
	var pk = primaryKey.split('|');
	var entryDate = pk[0];
	var entrySl = pk[1];
	var breakUp = pk[2] + " / " + pk[3] + " / " + pk[4];
	qinvbulkauthstatus_breakupGrid.clearAll();
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('ENTRY_DATE', entryDate);
	validator.setValue('ENTRY_SL', entrySl);
	validator.setClass('patterns.config.web.forms.inv.qinvbulkauthstatusbean');
	validator.setMethod('loadBreakUpDetails');
	validator.sendAndReceiveAsync(showBreakupGrid);
	$('#invoiceBreakUp').html(BREAKUP_DETAILS + breakUp);
}
function showBreakupGrid() {
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		showMessage('breakup', Message.ERROR, validator.getValue(ERROR));
		return false;
	}
	if (validator.getValue(RESULT) == DATA_UNAVAILABLE) {
		showMessage('breakup', Message.WARN, NO_RECORD);
		return false;
	}
	if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		qinvbulkauthstatus_breakupGrid.clearAll();
		show('view_breakupdtl');
		// qinvbulkauthstatus_breakupGrid.setColumnHidden(4, true);
		// qinvbulkauthstatus_breakupGrid.setColumnHidden(5, true);
		// <div id="gridbox" style="width:600px;height:400px;"></div>
		qinvbulkauthstatus_breakupGrid.loadXMLString(validator.getValue(RESULT_XML));
		hideMessage('breakup');
	}
}

function afterClosePopUp(win) {
	if (win.getId() == 'lease' || win.getId() == 'invcomponent') {
		dhxWindows = null;
		win = null;
		win = showWindow('view_dtl', EMPTY_STRING, COMPONENT_DETAILS, EMPTY_STRING, true, false, false, 1250, 535);
		return true;
	}
}
function viewinvComponentDetails() {
	// currently pk not passed
	window.scroll(0, 0);
	showWindow('invcomponent', getBasePath() + 'Renderer/inv/qinvcompview.jsp', 'QINVCOMPVIEW', PK + '=' + primaryKey + "&" + SOURCE + "=" + MAIN, true, false, false, 1000, 500);
	resetLoading();
}

function viewLeaseDetails(primaryKey) {
	window.scroll(0, 0);
	showWindow('lease', getBasePath() + 'Renderer/lss/qlease.jsp', QLEASE, PK + '=' + primaryKey + "&" + SOURCE + "=" + MAIN, true, false, false, 1000, 500);
	resetLoading();
}
