<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<%@taglib prefix="message" tagdir="/WEB-INF/tags/message"%>
<web:bundle baseName="patterns.config.web.forms.inv.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/inv/qinvcomponent.js" />
	</web:dependencies>
	<web:loadPK />
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="qinvcomponent.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/inv/qinvcomponent" id="qinvcomponent" method="POST">
				<web:viewContent id="po_view1">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="qinvcomponent.invoicedate" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:date property="invoiceDate" id="invoiceDate" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:viewTitle var="program" key="qinvcomponent.section" collapsable="addMoreFilters" status="close" />
							</web:section>
							<web:viewContent id="addMoreFilters" styleClass="hidden">
								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="180px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowOdd>
											<web:column>
												<web:legend key="qinvcomponent.invoicecyclenumber" var="program" />
											</web:column>
											<web:column>
												<type:invFieldCode property="invoiceCycleNumber" id="invoiceCycleNumber" />
											</web:column>
										</web:rowOdd>
									</web:table>
								</web:section>
								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="180px" />
											<web:columnStyle width="100px" />
											<web:columnStyle width="120px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowEven>
											<web:column>
												<web:legend key="qinvcomponent.intialinterim" var="program" />
											</web:column>
											<web:column>
												<type:checkbox property="intialInterim" id="intialInterim" />
											</web:column>
											<web:column>
												<web:legend key="qinvcomponent.monthlyrent" var="program" />
											</web:column>
											<web:column>
												<type:checkbox property="monthlyRent" id="monthlyRent" />
											</web:column>
										</web:rowEven>
									</web:table>
								</web:section>
								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="180px" />
											<web:columnStyle width="100px" />
											<web:columnStyle width="120px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowOdd>
											<web:column>
												<web:legend key="qinvcomponent.finalinterim" var="program" />
											</web:column>
											<web:column>
												<type:checkbox property="finalInterim" id="finalInterim" />
											</web:column>
											<web:column>
												<web:legend key="qinvcomponent.interest" var="program" />
											</web:column>
											<web:column>
												<type:checkbox property="interest" id="interest" />
											</web:column>
										</web:rowOdd>
									</web:table>
								</web:section>
								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="180px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowEven>
											<web:column>
												<web:legend key="qinvcomponent.leaseproductcode" var="program" />
											</web:column>
											<web:column>
												<type:leaseProductCode property="leaseProductCode" id="leaseProductCode" />
											</web:column>
										</web:rowEven>
										<web:rowOdd>
											<web:column>
												<web:legend key="qinvcomponent.customercode" var="program" />
											</web:column>
											<web:column>
												<type:lesseeCode property="customerCode" id="customerCode" />
											</web:column>
										</web:rowOdd>
										<web:rowEven>
											<web:column>
												<web:legend key="qinvcomponent.agreementno" var="program" />
											</web:column>
											<web:column>
												<type:agreementNo property="agreementNo" id="agreementNo" />
											</web:column>
										</web:rowEven>
										<web:rowOdd>
											<web:column>
												<web:legend key="qinvcomponent.scheduleid" var="program" />
											</web:column>
											<web:column>
												<type:scheduleID property="scheduleId" id="scheduleId" />
											</web:column>
										</web:rowOdd>
									</web:table>
								</web:section>
							</web:viewContent>
							<web:section>
								<web:table>
									<web:rowEven>
										<web:column>
											<type:button id="submit" key="form.submit" var="common" onclick="loadGrid();" />
											<type:button id="reset" key="form.reset" var="common" onclick="clearFields();" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<message:messageBox id="qinvcomponent" />
							</web:section>
							<web:section>
								<web:table>
									<web:rowOdd>
										<web:column>
											<web:grid height="300px" width="927px" id="qinvcomponent_Grid" src="inv/qinvcomponent_Grid.xml" paging="true">
											</web:grid>
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:viewContent id="innerGridView" styleClass="hidden">
								<web:section>
									<web:sectionTitle var="program" key="qinvcomponent.grid" />
								</web:section>
								<web:section>
									<message:messageBox id="qinvcomponent_popup" />
									<web:table>
										<web:rowOdd>
											<web:column>
												<web:grid height="200px" width="882px" id="qinvcomponent_innerGrid" src="inv/qinvcomponent_innerGrid.xml">
												</web:grid>
											</web:column>
										</web:rowOdd>
									</web:table>
								</web:section>
							</web:viewContent>
						</web:sectionBlock>
					</web:dividerBlock>
				</web:viewContent>
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
	<web:contextSearch />
</web:fragment>