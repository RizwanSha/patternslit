var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'RINVREPAYSCH';
function init() {
	clearFields();
}

function clearFields() {
	$('#lesseeCode').val(EMPTY_STRING);
	$('#lesseeCode_error').html(EMPTY_STRING);
	$('#lesseeCode_desc').html(EMPTY_STRING);
	$('#agreementNo').val(EMPTY_STRING);
	$('#agreementNo_error').html(EMPTY_STRING);
	$('#agreementNo_desc').html(EMPTY_STRING);
	$('#scheduleId').val(EMPTY_STRING);
	$('#scheduleId_error').html(EMPTY_STRING);
	$('#scheduleId_desc').html(EMPTY_STRING);
	hideMessage(CURRENT_PROGRAM_ID);

}

function doHelp(id) {
	switch (id) {
	case 'lesseeCode':
		help('COMMON', 'HLP_DEBTOR_CODE', $('#lesseeCode').val(), EMPTY_STRING, $('#lesseeCode'));
		break;
	case 'agreementNo':
		if (!isEmpty($('#custIDHidden').val())) {
			help('COMMON', 'HLP_CUST_AGGREEMENT', $('#agreementNo').val(), $('#custIDHidden').val(), $('#agreementNo'), null, function(gridObj, rowID) {
				$('#agreementNo').val(gridObj.cells(rowID, 1).getValue());
			});
		} else {
			setError('lesseeCode_error', MANDATORY);
		}
		break;
	case 'scheduleId':
		if (!isEmpty($('#agreementNo').val())) {
			help('COMMON', 'HLP_LEASE_SCH_ID', $('#scheduleId').val(), $('#lesseeCode').val() + PK_SEPERATOR + $('#agreementNo').val(), $('#scheduleId'), function() {
			});
		} else {
			setError('agreementNo_error', MANDATORY);
		}
		break;
	}
}
function backtrack(id) {
	switch (id) {
	case 'lesseeCode':
		setFocusLast('lesseeCode');
		break;
	case 'agreementNo':
		setFocusLast('lesseeCode');
		break;
	case 'scheduleId':
		setFocusLast('agreementNo');
		break;
	case 'print':
		setFocusLast('scheduleId');
		break;
	case 'reset':
		setFocusLast('print');
		break;
	}
}
function doclearfields(id) {
	switch (id) {
	case 'lesseeCode':
		if (isEmpty($('#lesseeCode').val())) {
			$('#agreementNo').val(EMPTY_STRING);
			$('#agreementNo_error').html(EMPTY_STRING);
			$('#agreementNo_desc').html(EMPTY_STRING);
			$('#scheduleId').val(EMPTY_STRING);
			$('#scheduleId_error').html(EMPTY_STRING);
			$('#scheduleId_desc').html(EMPTY_STRING);
		}
		break;
	case 'agreementNo':
		if (isEmpty($('#agreementNo').val())) {
			$('#scheduleId').val(EMPTY_STRING);
			$('#scheduleId_error').html(EMPTY_STRING);
			$('#scheduleId_desc').html(EMPTY_STRING);
		}
		break;
	case 'scheduleId':
		if (isEmpty($('#scheduleId').val())) {
			hideMessage(CURRENT_PROGRAM_ID);
		}
		break;
	}
}

function validate(id) {
	switch (id) {
	case 'lesseeCode':
		lesseeCode_val();
		break;
	case 'agreementNo':
		agreementNo_val();
		break;
	case 'scheduleId':
		scheduleId_val();
		break;
	case 'print':
		generateReport();
		break;
	case 'reset':
		clearFields();
		break;
	}
}
function lesseeCode_val() {
	var value = $('#lesseeCode').val();
	clearError('lesseeCode_error');
	if (isEmpty(value)) {
		setError('lesseeCode_error', MANDATORY);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('SUNDRY_DB_AC', $('#lesseeCode').val());
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.inv.rinvrepayschbean');
		validator.setMethod('validateLesseeCode');
		validator.sendAndReceive();
		if (validator.getValue(RESULT) == DATA_AVAILABLE) {
			$('#lesseeCode_desc').html(validator.getValue("CUSTOMER_NAME"));
			$('#custIDHidden').val(validator.getValue("CUSTOMER_ID"));
		} else if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('lesseeCode_error', validator.getValue(ERROR));
			$('#lesseeCode_desc').html(EMPTY_STRING);
			$('#custIDHidden').val(EMPTY_STRING);
			return false;
		}
	}
	setFocusLast('agreementNo');
	return true;
}

function agreementNo_val() {
	var value = $('#agreementNo').val();
	clearError('agreementNo_error');
	if (isEmpty(value)) {
		setError('agreementNo_error', MANDATORY);
		return false;
	}
	if (isEmpty($('#custIDHidden').val())) {
		setError('agreementNo_error', MANDATORY);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('CUSTOMER_ID', $('#custIDHidden').val());
		validator.setValue('AGREEMENT_NO', $('#agreementNo').val());
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.inv.rinvrepayschbean');
		validator.setMethod('validateAgreementNo');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('agreementNo_error', validator.getValue(ERROR));
			return false;
		}
	}
	setFocusLast('scheduleId');
	return true;
}

function scheduleId_val() {
	var value = $('#scheduleId').val();
	clearError('scheduleId_error');
	if (isEmpty(value)) {
		setError('scheduleId_error', MANDATORY);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('LESSEE_CODE', $('#lesseeCode').val());
		validator.setValue('AGREEMENT_NO', $('#agreementNo').val());
		validator.setValue('SCHEDULE_ID', $('#scheduleId').val());
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.inv.rinvrepayschbean');
		validator.setMethod('validateScheduleID');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('scheduleId_error', validator.getValue(ERROR));
			return false;
		}
	}
	setFocusLast('print');
	return true;
}

function generateReport() {
	$('#print').prop('disabled', false);
	if (lesseeCode_val() && agreementNo_val() && scheduleId_val()) {
		showMessage(getProgramID(), Message.PROGRESS);
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('ENTITY_CODE', getEntityCode());
		validator.setValue('LESSEE_CODE', $('#lesseeCode').val());
		validator.setValue('AGREEMENT_NO', $('#agreementNo').val());
		validator.setValue('SCHEDULE_ID', $('#scheduleId').val());
		validator.setValue('REPORT_TYPE', CURRENT_PROGRAM_ID);
		validator.setClass('patterns.config.web.forms.inv.rinvrepayschbean');
		validator.setMethod('generateReport');
		validator.sendAndReceiveAsync(reportStatus);
	}
}
function reportStatus() {
	$('#print').prop('disabled', false);
	hideMessage(CURRENT_PROGRAM_ID);
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		showMessage(getProgramID(), Message.ERROR, validator.getValue(ERROR));
		return false;
	} else {
		showReportLink(validator);
	}

}
