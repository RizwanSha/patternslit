var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MINVCYCLE';
function init() {
	refreshTBAGrid();
	refreshQueryGrid();

}

function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'MINVCYCLE_MAIN', queryGrid);
}

function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}

function clearFields() {
	$('#invoiceCycleNumber').val(EMPTY_STRING);
	$('#invoiceCycleNumber_error').html(EMPTY_STRING);
	$('#invoiceCycleNumber_desc').html(EMPTY_STRING);
	clearNonPKFields();
}

function clearNonPKFields() {
	$('#invoicesForRentalDateDaysFrom').val(EMPTY_STRING);
	$('#invoicesForRentalDateDaysFrom_error').html(EMPTY_STRING);
	$('#invoicesForRentalDateDaysFrom_desc').val(EMPTY_STRING);
	$('#upto').val(EMPTY_STRING);
	$('#upto_error').html(EMPTY_STRING);
	$('#invoiceGenerationInAdvanceBy').val(EMPTY_STRING);
	$('#invoiceGenerationInAdvanceBy_error').html(EMPTY_STRING);
	$('#invoiceGenerationInAdvanceBy_desc').val(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
}

function add() {
	$('#invoiceCycleNumber').focus();
}

function modify() {
	$('#invoiceCycleNumber').focus();
}

function loadData() {
	$('#invoiceCycleNumber').val(validator.getValue('INV_CYCLE_NUMBER'));
	$('#invoicesForRentalDateDaysFrom').val(validator.getValue('INV_RENTAL_DAY_FROM'));
	$('#upto').val(validator.getValue('INV_RENTAL_DAY_UPTO'));
	$('#invoiceGenerationInAdvanceBy').val(validator.getValue('INV_GEN_ADV_DAYS'));
	$('#remarks').val(validator.getValue('REMARKS'));
	resetLoading();
}

function view(source, primaryKey) {
	hideParent('inv/qinvcycle', source, primaryKey);
	resetLoading();
}

function doclearfields(id) {
	switch (id) {
	case 'invoiceCycleNumber':
		if (isEmpty($('#invoiceCycleNumber').val())) {
			clearFields();
			break;
		}
	}
}

function validate(id) {
	switch (id) {
	case 'invoiceCycleNumber':
		invoiceCycleNumber_val();
		break;
	case 'invoicesForRentalDateDaysFrom':
		invoicesForRentalDateDaysFrom_val();
		break;
	case 'upto':
		upto_val();
		break;
	case 'invoiceGenerationInAdvanceBy':
		invoiceGenerationInAdvanceBy_val();
		break;
	case 'remarks':
		remarks_val();
		break;

	}
}

function backtrack(id) {
	switch (id) {
	case 'invoiceCycleNumber':
		setFocusLast('invoiceCycleNumber');
		break;
	case 'invoicesForRentalDateDaysFrom':
		setFocusLast('invoiceCycleNumber');
		break;
	case 'upto':
		setFocusLast('invoicesForRentalDateDaysFrom');
		break;
	case 'invoiceGenerationInAdvanceBy':
		setFocusLast('upto');
		break;
	case 'remarks':
		setFocusLast('invoiceGenerationInAdvanceBy');
		break;
	}
}
function invoiceCycleNumber_val() {
	var value = $('#invoiceCycleNumber').val();
	clearError('invoiceCycleNumber_error');
	$('#invoiceCycleNumber_desc').html(EMPTY_STRING);
	if (isEmpty(value)) {
		setError('invoiceCycleNumber_error', MANDATORY);
		return false;
	}
	if (isZero(value)) {
		setError('invoiceCycleNumber_error', ZERO_CHECK);
		return false;
	}
	if (!isNumeric(value)) {
		setError('invoiceCycleNumber_error', NUMERIC_CHECK);
		return false;
	}
	if (isNumberGreater(value, 99)) {
		setError('invoiceCycleNumber_error', MAXVAL_99);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('INV_CYCLE_NUMBER', $('#invoiceCycleNumber').val());
	validator.setValue(ACTION, $('#action').val());
	validator.setClass('patterns.config.web.forms.inv.minvcyclebean');
	validator.setMethod('validateInvoiceCycleNumber');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('invoiceCycleNumber_error', validator.getValue(ERROR));
		$('#invoiceCycleNumber_desc').html(EMPTY_STRING);
		return false;
	} else {

		$('#invoiceCycleNumber_desc').html(validator.getValue('INV_CYCLE_NUMBER'));
		if ($('#action').val() == MODIFY) {
			PK_VALUE = getEntityCode() + PK_SEPERATOR + $('#invoiceCycleNumber').val();
			if (!loadPKValues(PK_VALUE, 'invoiceCycleNumber_error')) {
				return false;
			}
		}
	}
	setFocusLast('invoicesForRentalDateDaysFrom');
	return true;
}

function invoicesForRentalDateDaysFrom_val() {
	var value = $('#invoicesForRentalDateDaysFrom').val();
	clearError('invoicesForRentalDateDaysFrom_error');
	if (isEmpty(value)) {
		setError('invoicesForRentalDateDaysFrom_error', MANDATORY);
		return false;
	}
	if (isZero(value)) {
		setError('invoicesForRentalDateDaysFrom_error', ZERO_CHECK);
		return false;
	}
	if (!isNumeric(value)) {
		setError('invoicesForRentalDateDaysFrom_error', NUMERIC_CHECK);
		return false;
	}
	if (isNegative(value)) {
		setError('invoicesForRentalDateDaysFrom_error', NEGATIVE_CHECK);
		return false;
	}
	if (isNumberGreater(value, 28)) {
		setError('invoicesForRentalDateDaysFrom_error', MAXVAL_28);
		return false;
	}
	setFocusLast('upto');
	return true;
}

function upto_val() {
	var value = $('#upto').val();
	clearError('upto_error');
	if (isEmpty(value)) {
		setError('upto_error', MANDATORY);
		return false;
	}
	if (isZero(value)) {
		setError('upto_error', ZERO_CHECK);
		return false;
	}
	if (!isNumeric(value)) {
		setError('upto_error', NUMERIC_CHECK);
		return false;
	}
	if (isNumberGreater(value, 28)) {
		setError('upto_error', MAXVAL_28);
		return false;
	}
	if (isNegative(value)) {
		setError('upto_error', NEGATIVE_CHECK);
		return false;
	}
	if (isNumberLesserThanEqual(value, $('#invoicesForRentalDateDaysFrom').val())) {
		setError('upto_error', TC_RENTALDAYS_SHUDNOT_LESS_N_EQUAL);
		return false;
	}
	setFocusLast('invoiceGenerationInAdvanceBy');
	return true;
}

function invoiceGenerationInAdvanceBy_val() {
	var value = $('#invoiceGenerationInAdvanceBy').val();
	clearError('invoiceGenerationInAdvanceBy_error');
	if (isEmpty(value)) {
		setError('invoiceGenerationInAdvanceBy_error', MANDATORY);
		return false;
	}
	if (isZero(value)) {
		setError('invoiceGenerationInAdvanceBy_error', ZERO_CHECK);
		return false;
	}
	if (isNegative(value)) {
		setError('invoiceGenerationInAdvanceBy_error', NEGATIVE_CHECK);
		return false;
	}
	if (!isNumeric(value)) {
		setError('invoiceGenerationInAdvanceBy_error', NUMERIC_CHECK);
		return false;
	}
	if (isNumberGreater(value, 30)) {
		setError('invoiceGenerationInAdvanceBy_error', HMS_INVALID_DEC_RANGE);
		return false;
	}
	setFocusLast('remarks');
	return true;
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', HMS_INVALID_REMARKS);
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', HMS_INVALID_REMARKS);
			return false;
		}
	}
	if ($('#action').val() == MODIFY) {
		if (isEmpty(value)) {
			setError('remarks_error', HMS_MANDATORY);
			return false;
		}
	}
	setFocusOnSubmit();
	return true;
}
