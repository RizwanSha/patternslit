var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'RINVOICE';
var rowId =EMPTY_STRING;
function init() {
	clearFields();
}

function clearFields() {
	$('#invoiceCycleNumber').focus();
	$('#invoiceCycleNumber').val(EMPTY_STRING);
	$('#invoiceCycleNumber_desc').html(EMPTY_STRING);
	$('#invoiceMonth').val(EMPTY_STRING);
	$('#invoiceMonthYear').val(EMPTY_STRING);
	$('#invoiceMonth_desc').html(EMPTY_STRING);
	clearNonPkFields();
	clearPkFieldsError();

}

function clearNonPkFields() {
	rinvoice_innerGrid.clearAll();
}

function clearPkFieldsError() {
	$('#invoiceCycleNumber_error').html(EMPTY_STRING);
	$('#invoiceMonth_error').html(EMPTY_STRING);
}

function doHelp(id) {
	switch (id) {
	case 'invoiceCycleNumber':
		help('COMMON', 'HLP_INVOICE_CYCLE_NUMBER',
				$('#invoiceCycleNumber').val(), $('#invoiceCycleNumber').val(),
				$('#invoiceCycleNumber'), null, function(gridObj, rowID) {
					$('#invoiceCycleNumber').val(
							gridObj.cells(rowID, 0).getValue());
					$('#invoiceCycleNumber_desc').html(EMPTY_STRING);
					$('#invoiceCycleNumber_error').html(EMPTY_STRING);
				});
		break;
	}
}
function backtrack(id) {
	switch (id) {
	case 'invoiceMonth':
		setFocusLast('invoiceCycleNumber');
		break;
	case 'invoiceMonthYear':
		setFocusLast('invoiceMonth');
		break;
	case 'submit':
		setFocusLast('invoiceMonthYear');
		break;
	}
}
function doclearfields(id) {
	switch (id) {
	case 'invoiceCycleNumber':
		if (isEmpty($('#invoiceCycleNumber').val())) {
			clearFields();
		}
		break;
	case 'invoiceMonthYear':
		if (isEmpty($('#invoiceMonthYear').val())) {
			$('#invoiceMonth_desc').html(EMPTY_STRING);
			clearNonPkFields();
		}
		break;
	}
}
function downloadReport(reportId) {
	window.location = getBasePath() + 'servlet/ReportDownloadProcessor?'
			+ REPORT_ID + '=' + reportId;
}

function validate(id) {
	valMode = true;
	switch (id) {
	case 'invoiceCycleNumber':
		invoiceCycleNumber_val(valMode);
		break;
	case 'invoiceMonth':
		invoiceMonth_val(valMode);
		break;
	case 'invoiceMonthYear':
		invoiceMonthYear_val(valMode);
		break;
	case 'submit':
		loadGrid();
		break;
	case 'generateReport':
		generateReport();
		break;
	case 'reset':
		clearFields();
		break;
	}
}
function invoiceCycleNumber_val(valMode) {
	var value = $('#invoiceCycleNumber').val();
	$('#invoiceCycleNumber_desc').html(EMPTY_STRING);
	clearError('invoiceCycleNumber_error');
	if (isEmpty(value)) {
		setError('invoiceCycleNumber_error', MANDATORY);
		return false;
	} else if (!isNumeric(value)) {
		setError('invoiceCycleNumber_error', INVALID_NUMBER);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('INV_CYCLE_NUMBER', value);
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.inv.rinvoicebean');
		validator.setMethod('validateInvoicesForCycleNumber');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('invoiceCycleNumber_error', validator.getValue(ERROR));
			return false;
		} else if (validator.getValue(RESULT) == DATA_AVAILABLE) {
			fromDays = validator.getValue('INV_RENTAL_DAY_FROM');
			uptoDays = validator.getValue('INV_RENTAL_DAY_UPTO');
			$('#invoiceCycleNumber_desc').html(
					validator.getValue('DESCRIPTION'));
		}
	}
	if (valMode)
		setFocusLast('invoiceMonth');
	return true;
}

function invoiceMonth_val(valMode) {
	var value = $('#invoiceMonth').val();
	clearError('invoiceMonth_error');
	if (isEmpty(value)) {
		setError('invoiceMonth_error', MANDATORY);
		return false;
	}
	if (valMode)
		setFocusLast('invoiceMonthYear');
	return true;
}
function invoiceMonthYear_val(valMode) {
	var value = $('#invoiceMonthYear').val();
	clearError('invoiceMonth_error');
	if (isEmpty(value)) {
		setError('invoiceMonth_error', MANDATORY);
		return false;
	}
	if (!isValidYear(value)) {
		setError('invoiceMonth_error', INVALID_YEAR);
		return false;
	}
	if (valMode)
		setFocusLast('submit');
	return true;
}

function loadGrid() {
	clearNonPkFields();
	$('#submit').prop('disabled', true);
	showMessage(getProgramID(), Message.PROGRESS);
	if (invoiceCycleNumber_val(false) && invoiceMonth_val(false)
			&& invoiceMonthYear_val(false)) {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue(ACTION, USAGE);
		validator.setValue('INV_CYCLE_NUMBER', $('#invoiceCycleNumber').val());
		validator.setValue('INV_YEAR', $('#invoiceMonthYear').val());
		validator.setValue('INV_MONTH', $('#invoiceMonth').val());
		validator.setValue('REVALIDATE', "1");
		validator.setClass('patterns.config.web.forms.inv.rinvoicebean');
		validator.setMethod('loadGrid');
		validator.sendAndReceiveAsync(showGridStatus);
	}
}
function showGridStatus() {
	$('#submit').prop('disabled', false);
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		showMessage(getProgramID(), Message.ERROR, validator.getValue(ERROR));
		return false;
	} else if (validator.getValue(RESULT) == DATA_UNAVAILABLE) {
		showMessage(getProgramID(), Message.WARN, NO_RECORD);
	} else if (validator.getValue('ERROR_PRESENT') == ONE) {
		if (validator.getValue('INV_MONTH_ERROR').trim() != EMPTY_STRING)
			$('#invoiceMonth_error')
					.html(validator.getValue('INV_MONTH_ERROR'));
		if (validator.getValue('INV_YEAR_ERROR').trim() != EMPTY_STRING)
			$('#invoiceMonth_error').html(validator.getValue('INV_YEAR_ERROR'));
		if (validator.getValue('INVOICECYCLENUMBER_ERROR').trim() != EMPTY_STRING)
			$('#invoiceCycleNumber_error').html(
					validator.getValue('INVOICECYCLENUMBER_ERROR'));
		hideMessage(CURRENT_PROGRAM_ID);
	} else if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		xmlString = validator.getValue(RESULT_XML);
		rinvoice_innerGrid.loadXMLString(xmlString);
		hideMessage(CURRENT_PROGRAM_ID);
	}
}

function generateInvoiceReport() {
	$('#generateReport').prop('disabled', false);
	if (rinvoice_innerGrid.getCheckedRows(0) != EMPTY_STRING) {
		 rowId = rinvoice_innerGrid.getCheckedRows(0);
		var reportGen = rinvoice_innerGrid.cells(rowId, 7).getValue();
		if (reportGen == '1') {
			if (!confirm(OVERRIDE_REPORT)) {
				return;
			}
		}
		var invNo = rinvoice_innerGrid.cells(rowId, 1).getValue().split('/');
		showMessage(getProgramID(), Message.PROGRESS);
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('ENTITY_CODE', getEntityCode());
		validator.setValue('LEASE_PROD_CODE', invNo[0]);
		validator.setValue('INV_YEAR', invNo[1]);
		validator.setValue('INV_MONTH', invNo[2]);
		validator.setValue('INV_SERIAL', invNo[3]);
		validator.setValue('REPORT_TYPE', CURRENT_PROGRAM_ID);
		validator.setValue('TOTAL_INV_AMT', rinvoice_innerGrid.cells(rowId, 10)
				.getValue());
		validator.setValue('INV_CCY', rinvoice_innerGrid.cells(rowId, 5)
				.getValue());
		validator.setValue('INV_CCY_UNITS', rinvoice_innerGrid.cells(rowId, 11)
				.getValue());
		validator.setClass('patterns.config.web.forms.inv.rinvoicebean');
		validator.setMethod('generateInvoiceReport');
		validator.sendAndReceiveAsync(reportStatus);
	} else {
		alert(SELECT_ATLEAST_ONE);
		$('#generateReport').unFocus();
		return false;
	}
}
function reportStatus() {
	$('#generateReport').prop('disabled', false);
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		showMessage(getProgramID(), Message.ERROR, validator.getValue(ERROR));
		return false;
	} else if (validator.getValue(RESULT) == DATA_UNAVAILABLE) {
		showMessage(getProgramID(), validator.getValue(ERROR));
		return false;
	} else if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		hideMessage(getProgramID());
		if (validator.getValue("PDF_REPORT_IDENTIFIER") != EMPTY_STRING
				|| validator.getValue("EXCEL_REPORT_IDENTIFIER") != EMPTY_STRING)
			rinvoice_innerGrid.cells(rowId, 7).setValue('1');
		if (validator.getValue("PDF_REPORT_IDENTIFIER") != EMPTY_STRING)
			rinvoice_innerGrid.cells(rowId, 8).setValue(
					"Download^javascript:downloadReport(\""
							+ validator.getValue("PDF_REPORT_IDENTIFIER")
							+ "\")");
		if (validator.getValue("EXCEL_REPORT_IDENTIFIER") != EMPTY_STRING)
			rinvoice_innerGrid.cells(rowId, 9).setValue(
					"Download^javascript:downloadReport(\""
							+ validator.getValue("EXCEL_REPORT_IDENTIFIER")
							+ "\")");

	}

}
