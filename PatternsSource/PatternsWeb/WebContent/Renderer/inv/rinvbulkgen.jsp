<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<%@taglib prefix="message" tagdir="/WEB-INF/tags/message"%>
<web:bundle baseName="patterns.config.web.forms.inv.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/inv/rinvbulkgen.js" />
	</web:dependencies>
	<web:loadPK />
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="inv.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/inv/rinvbulkgen" id="inv" method="POST">
				<web:viewContent id="po_view1">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="rinvbulkgen.billingcycle" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:twoDigit property="billingCycle" id="billingCycle" lookup="true"/>
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="rinvbulkgen.formonthyear" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:yearMonth property="month" id="month" datasourceid="COMMON_MONTH" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle width="100px" />
										<web:columnStyle width="10px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="rinvbulkgen.invoicedaterange" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:dateDisplay property="fromDate" id="fromDate" />
										</web:column>
										<web:column>
											<web:legend key="rinvbulkgen.minus" var="program" />
										</web:column>
										<web:column>
											<type:dateDisplay property="uptoDate" id="uptoDate" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="rinvbulkgen.productcode" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:productCode property="productCode" id="productCode" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="rinvbulkgen.customerid" var="program"/>
										</web:column>
										<web:column>
											<type:customerID property="customerId" id="customerId"/>
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="rinvbulkgen.numberofinvoices" var="program" />
										</web:column>
										<web:column>
											<type:fiveDigitDisplay property="nofInvoices" id="nofInvoices" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:rowEven>
										<web:column>
											<web:button id="submit" key="form.submit" var="common" onclick="doSubmit();" />
											<web:button id="reset" key="form.reset" var="common" onclick="clearFields()" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<message:messageBox id="rinvbulkgen" />
							</web:section>
						</web:sectionBlock>
					</web:dividerBlock>
				</web:viewContent>
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
	<web:contextSearch />
</web:fragment>

