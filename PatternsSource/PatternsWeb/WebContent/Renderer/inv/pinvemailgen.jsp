<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<%@taglib prefix="message" tagdir="/WEB-INF/tags/message"%>
<web:bundle baseName="patterns.config.web.forms.inv.Messages" var="program" />
<web:fragment>
	<style>
p {
	border: 1px solid;
	padding: 5px;
	font-size: small;
	font-weight: initial;
}

.setCursorPointer {
	cursor: pointer;
}

.mailContent {
	height: 240px;
	overflow: auto;
}

.pdfColor {
	color: #ff0000;
}

.pdfColor:hover {
	color: #ff0000;
}

.xlsColor {
	color: #008000;
}

.xlsColor:hover {
	color: #008000;
}
</style>
	<web:dependencies>
		<web:script src="Renderer/inv/pinvemailgen.js" />
	</web:dependencies>
	<web:loadPK />
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="inv.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/inv/pinvemailgen" id="pinvemailgen" method="POST">
				<web:viewContent id="po_view1">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="pinvemailgen.invoicecyclenumber" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:twoDigit property="invoiceCycleNumber" id="invoiceCycleNumber" lookup="true" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="pinvemailgen.invoicemonthoryear" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:yearMonth property="invoiceMonth" id="invoiceMonth" datasourceid="COMMON_MONTHS" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:rowEven>
										<web:column>
											<web:button id="submit" key="form.submit" var="common" onclick="loadGrid();" />
											<web:button id="reset" key="form.reset" var="common" onclick="clearFields()" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>

							<web:section>
								<message:messageBox id="pinvemailgen" />
							</web:section>
							<web:section>
								<web:table>
									<web:rowOdd>
										<web:column>
											<web:viewContent id="po_view4">
												<web:grid height="350px" width="1053px" id="pinvemailgen_innerGrid" src="inv/pinvemailgen_innerGrid.xml">
												</web:grid>
											</web:viewContent>
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>


							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="80px" />
										<web:columnStyle width="360px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:button id="sendMailId" key="pinvemailgen.sendmail" var="program" onclick="sendMail();" />
										</web:column>
										<web:column>
											<message:messageBox id="emailinfo" />
										</web:column>
										<web:column>
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>

							<web:viewContent id="mailSentLog" styleClass="hidden">
								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="140px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowEven>
											<web:column>
												<web:legend key="pinvemailgen.invoiceDate" var="program" />
											</web:column>
											<web:column>
												<type:dateDisplay property="invoiceDate" id="invoiceDate" />
											</web:column>
										</web:rowEven>
										<web:rowOdd>
											<web:column>
												<web:legend key="pinvemailgen.customername" var="program" />
											</web:column>
											<web:column>
												<type:nameDisplay property="customerName" id="customerName" />
											</web:column>
										</web:rowOdd>
										<type:error property="mailSentLogError" />
									</web:table>
								</web:section>
								<web:section>
									<web:table>
										<web:rowEven>
											<web:column>
												<web:viewContent id="po_view4">
													<web:grid height="230px" width="757px" id="mailSentLog_innerGrid" src="inv/pinvemailgen_mailSentLog_innerGrid.xml">
													</web:grid>
												</web:viewContent>
											</web:column>
										</web:rowEven>
									</web:table>
								</web:section>
							</web:viewContent>
							<web:viewContent id="mail" styleClass="hidden">
								<web:grid height="0px" width="0px" id="mailGrid" src="inv/pinvemailgen_mailGrid.xml">
								</web:grid>
							</web:viewContent>
							<web:viewContent id="mailLogDetails" styleClass="hidden">
								<type:error property="mailLogDetailsError" />
								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="140px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowEven>
											<web:column>
												<web:legend key="pinvemailgen.recipient" var="program" />
											</web:column>
											<web:column>
												<web:viewContent id="po_view4">
													<web:grid height="150px" width="416px" id="mailSentRcp_innerGrid" src="inv/pinvemailgen_mailSentRcp_innerGrid.xml">
													</web:grid>
												</web:viewContent>
											</web:column>
										</web:rowEven>
										<web:rowOdd>
											<web:column>
												<web:legend key="pinvemailgen.subject" var="program" />
											</web:column>
											<web:column>
												<p id="subject" />
											</web:column>
										</web:rowOdd>
										<web:rowEven>
											<web:column>
												<web:legend key="pinvemailgen.senton" var="program" />
											</web:column>
											<web:column>
												<type:dateTimeDisplay property="sentOn" id="sentOn" />
											</web:column>
										</web:rowEven>
										<web:rowOdd>
											<web:column>
												<web:legend key="pinvemailgen.content" var="program" />
											</web:column>
											<web:column>
												<p class="mailContent" id="emailContent" />
											</web:column>
										</web:rowOdd>
									</web:table>
								</web:section>
								<web:viewContent id="attachment">
									<web:section>
										<web:table>
											<web:columnGroup>
												<web:columnStyle width="140px" />
												<web:columnStyle />
											</web:columnGroup>
											<web:rowEven>
												<web:column>
													<web:legend key="pinvemailgen.attachment" var="program" />
													<i class="fa fa-paperclip fa-2x" style="color: #000000"></i>
												</web:column>
												<web:column>
													<div id="pdfDiv" class="setCursorPointer">
														<a id="pdfFile" href="#" target="_blank" class="pdfColor"> <web:legend id="pdf" key="pinvemailgen.pdf" var="program" /></a> <a class="fa fa-file-pdf-o fa-2x pdfColor" id="pdf" href="#" target="_blank"></a>
													</div>
													<div id="xlsDiv" class="setCursorPointer">
														<a id="xlsFile" href="#" target="_blank" class="xlsColor"> <web:legend id="xls" key="pinvemailgen.xls" var="program" /></a> <a class="fa fa-file-excel-o fa-2x xlsColor" id="xls" href="#" target="_blank"></a>
													</div>
												</web:column>
											</web:rowEven>
										</web:table>
									</web:section>
								</web:viewContent>
							</web:viewContent>
						</web:sectionBlock>
					</web:dividerBlock>
				</web:viewContent>
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
	<web:contextSearch />
</web:fragment>

