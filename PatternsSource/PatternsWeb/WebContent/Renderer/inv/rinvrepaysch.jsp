<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<%@taglib prefix="message" tagdir="/WEB-INF/tags/message"%>
<web:bundle baseName="patterns.config.web.forms.inv.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/inv/rinvrepaysch.js" />
	</web:dependencies>
	<web:loadPK />
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="rinvrepaysch.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/inv/rinvrepaysch" id="rinvrepaysch" method="POST">
				<web:viewContent id="po_view1">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="rinvrepaysch.lesseecode" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:code property="lesseeCode" id="lesseeCode" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="rinvrepaysch.agreementno" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:agreementNo property="agreementNo" id="agreementNo" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="rinvrepaysch.scheduleid" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:scheduleCode property="scheduleId" id="scheduleId" lookup="true" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:rowEven>
										<web:column>
											<web:button id="print" key="form.print" var="common" onclick="generateReport();" />
											<web:button id="reset" key="form.reset" var="common" onclick="clearFields()" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<message:messageBox id="rinvrepaysch" />
							</web:section>
						</web:sectionBlock>
					</web:dividerBlock>
				</web:viewContent>
				<web:element property="custIDHidden" />
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
	<web:contextSearch />
</web:fragment>

