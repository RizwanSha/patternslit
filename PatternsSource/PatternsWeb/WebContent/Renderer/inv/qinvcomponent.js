var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'QINVCOMPONENT';
var customerId = null;
function init() {
	clearFields();
}

function clearFields() {
	$('#invoiceDate').val(EMPTY_STRING);
	$('#invoiceDate_desc').html(EMPTY_STRING);
	$('#invoiceDate_error').html(EMPTY_STRING);
	setFocusLast('invoiceDate');
	clearNonPKFields();
}

function clearNonPKFields() {
	$('#moduleId').val(EMPTY_STRING);
	$('#moduleId_error').html(EMPTY_STRING);
	$('#invoiceCycleNumber').val(EMPTY_STRING);
	$('#invoiceCycleNumber_error').html(EMPTY_STRING);
	$('#invoiceCycleNumber_desc').html(EMPTY_STRING);
	setCheckbox('intialInterim', NO);
	setCheckbox('monthlyRent', NO);
	setCheckbox('finalInterim', NO);
	setCheckbox('interest', NO);
	setCheckbox('enabled', NO);
	$('#leaseProductCode').val(EMPTY_STRING);
	$('#leaseProductCode_desc').html(EMPTY_STRING);
	$('#leaseProductCode_error').html(EMPTY_STRING);
	$('#customerCode').val(EMPTY_STRING);
	$('#customerCode_desc').html(EMPTY_STRING);
	$('#customerCode_error').html(EMPTY_STRING);
	$('#agreementNo').val(EMPTY_STRING);
	$('#agreementNo_error').html(EMPTY_STRING);
	$('#agreementNo_desc').html(EMPTY_STRING);
	$('#scheduleId').val(EMPTY_STRING);
	$('#scheduleId_desc').html(EMPTY_STRING);
	$('#scheduleId_error').html(EMPTY_STRING);
	qinvcomponent_Grid.clearAll();
	hideAllMessages(getProgramID());
}

function doHelp(id) {
	switch (id) {
	case 'invoiceCycleNumber':
		help('COMMON', 'HLP_INVOICE_CYCLE_NUMBER', $('#invoiceCycleNumber').val(), EMPTY_STRING, $('#invoiceCycleNumber'), null, function(gridObj, rowID) {
			var fromDay = gridObj.cells(rowID, 1).getValue();
			var endDay = gridObj.cells(rowID, 2).getValue();
			$('#invoiceCycleNumber_desc').html(fromDay + " - " + endDay);
		});
		break;
	case 'leaseProductCode':
		help('COMMON', 'HLP_LEASE_PROD_CODE', $('#leaseProductCode').val(), EMPTY_STRING, $('#leaseProductCode'));
		break;

	case 'customerCode':
		help('COMMON', 'HLP_DEBTOR_CODE', $('#customerCode').val(), $('#customerCode').val(), $('#customerCode'), null, function(gridObj, rowID) {
			$('#customerCode').val(gridObj.cells(rowID, 0).getValue());
			customerId = gridObj.cells(rowID, 1).getValue();
		});
		break;
	case 'agreementNo':
		if (!isEmpty($('#customerCode').val())) {
			help('COMMON', 'HLP_CUST_AGGREEMENT', $('#agreementNo').val(), customerId, $('#agreementNo'));
		} else {
			setError('customerCode_error', MANDATORY);
		}
		break;
	case 'scheduleId':
		if (!isEmpty($('#customerCode').val())) {
			help('COMMON', 'HLP_LEASE_SCH_ID', $('#scheduleId').val(), $('#customerCode').val() + PK_SEPERATOR + $('#agreementNo').val(), $('#scheduleId'), function() {
			});
		} else {
			setError('agreementNo_error', MANDATORY);
		}
		break;
	}
}

function doclearfields(id) {
	switch (id) {
	case 'invoiceDate':
		if (isEmpty($('#invoiceDate').val())) {
			$('#invoiceDate_desc').html(EMPTY_STRING);
			$('#invoiceDate_error').html(EMPTY_STRING);
			clearNonPKFields();
			break;
		}
	}
}

function validate(id) {
	switch (id) {
	case 'invoiceDate':
		invoiceDate_val();
		break;
	case 'invoiceCycleNumber':
		invoiceCycleNumber_val();
		break;
	case 'intialInterim':
		intialInterim_val();
		break;
	case 'monthlyRent':
		monthlyRent_val();
		break;
	case 'finalInterim':
		finalInterim_val();
		break;
	case 'interest':
		interest_val();
		break;
	case 'leaseProductCode':
		leaseProductCode_val();
		break;
	case 'customerCode':
		customerCode_val();
		break;
	case 'agreementNo':
		agreementNo_val();
		break;
	case 'scheduleId':
		scheduleId_val();
		break;

	}
}

function backtrack(id) {
	switch (id) {
	case 'invoiceDate':
		setFocusLast('invoiceDate');
		break;
	case 'invoiceCycleNumber':
		setFocusLast('invoiceDate');
		break;
	case 'intialInterim':
		setFocusLast('invoiceCycleNumber');
		break;
	case 'monthlyRent':
		setFocusLast('intialInterim');
		break;
	case 'finalInterim':
		setFocusLast('monthlyRent');
		break;
	case 'interest':
		setFocusLast('finalInterim');
		break;
	case 'leaseProductCode':
		setFocusLast('interest');
		break;
	case 'customerCode':
		setFocusLast('leaseProductCode');
		break;
	case 'agreementNo':
		setFocusLast('customerCode');
		break;
	case 'scheduleId':
		setFocusLast('agreementNo');
		break;
	}
}

function invoiceDate_val() {
	var value = $('#invoiceDate').val();
	clearError('invoiceDate_error');
	if (isEmpty(value)) {
		$('#invoiceDate').val(getCBD());
		value = $('#invoiceDate').val();
	}
	if (!isDate(value)) {
		setError('invoiceDate_error', INVALID_DATE);
		return false;
	}
	if (isDateLesser(value, getCBD())) {
		setError('invoiceDate_error', DATE_GECBD);
		return false;
	}
	if ($('#addMoreFilters').hasClass('hidden')) {
		setFocusLast('submit');
		return true;
	} else {
		setFocusLast('invoiceCycleNumber');
		return true;
	}
}

function invoiceCycleNumber_val() {
	var value = $('#invoiceCycleNumber').val();
	clearError('invoiceCycleNumber_error');
	if (!isEmpty(value)) {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('INV_CYCLE_NUMBER', value);
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.inv.qinvcomponentbean');
		validator.setMethod('ValidateInvoiceCycleNumber');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('invoiceCycleNumber_error', validator.getValue(ERROR));
			return false;
		}
		$('#invoiceCycleNumber_desc').html(validator.getValue('INV_RENTAL_DAY_FROM') + "-" + validator.getValue('INV_RENTAL_DAY_UPTO'));
	}

	setFocusLast('intialInterim');
	return true;
}

function intialInterim_val() {
	setFocusLast('monthlyRent');
	return true;
}

function monthlyRent_val() {
	setFocusLast('finalInterim');
	return true;
}

function finalInterim_val() {
	setFocusLast('interest');
	return true;
}

function interest_val() {
	setFocusLast('leaseProductCode');
	return true;
}

function leaseProductCode_val() {
	var value = $('#leaseProductCode').val();
	clearError('leaseProductCode_error');
	$('#leaseProductCode_desc').html(EMPTY_STRING);
	if (!isEmpty(value)) {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('LEASE_PRODUCT_CODE', $('#leaseProductCode').val());
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.inv.qinvcomponentbean');
		validator.setMethod('ValidateLeaseProductCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('leaseProductCode_error', validator.getValue(ERROR));
			return false;
		} else if (validator.getValue(RESULT) == DATA_AVAILABLE) {
			$('#leaseProductCode_desc').html(validator.getValue('DESCRIPTION'));

		}
	}
	setFocusLast('customerCode');
	return true;
}

function customerCode_val() {
	var value = $('#customerCode').val();
	clearError('customerCode_error');
	$('#customerCode_desc').html(EMPTY_STRING);
	if (!isEmpty(value)) {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('SUNDRY_DB_AC', value);
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.inv.qinvcomponentbean');
		validator.setMethod('ValidateCustomerCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('customerCode_error', validator.getValue(ERROR));
			return false;
		}
		customerId = validator.getValue("CUSTOMER_ID");
	}
	setFocusLast('agreementNo');
	return true;
}

function agreementNo_val() {
	var value = $('#agreementNo').val();
	clearError('agreementNo_error');
	$('#agreementNo_desc').html(EMPTY_STRING);
	if (!isEmpty($('#customerCode').val())) {
		if (isEmpty(value)) {
			setError('agreementNo_error', MANDATORY);
			return false;
		}
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('CUSTOMER_ID', customerId);
		validator.setValue('AGREEMENT_NO', value);
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.inv.qinvcomponentbean');
		validator.setMethod('ValidateAgreementNumber');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('agreementNo_error', validator.getValue(ERROR));
			return false;
		}

	} else {
		if (!isEmpty(value)) {
			setError('customerCode_error', MANDATORY);
			return false;
		}
	}
	setFocusLast('scheduleId');
	return true;
}

function scheduleId_val() {
	var value = $('#scheduleId').val();
	clearError('scheduleId_error');
	$('#scheduleId_desc').html(EMPTY_STRING);
	if (!isEmpty($('#customerCode').val())) {
		if (isEmpty(value)) {
			setError('scheduleId_error', MANDATORY);
			return false;
		}
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('LESSEE_CODE', $('#customerCode').val());
		validator.setValue('AGREEMENT_NO', $('#agreementNo').val());
		validator.setValue('SCHEDULE_ID', value);
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.inv.qinvcomponentbean');
		validator.setMethod('ValidateScheduleId');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('scheduleId_error', validator.getValue(ERROR));
			return false;
		}
	}
	 else {
		if (!isEmpty(value)) {
			setError('customerCode_error', MANDATORY);
			return false;
		}
	}
	setFocusLast('submit');
	return true;
}

function loadGrid() {
	showMessage(getProgramID(), Message.PROGRESS);
	qinvcomponent_Grid.clearAll();
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('INV_DATE', $('#invoiceDate').val());
	validator.setValue('INV_CYCLE_NUMBER', $('#invoiceCycleNumber').val());
	validator.setValue('INV_FOR_INTIAL_INTERIM', decodeBTS($('#intialInterim').prop('checked')));
	validator.setValue('INV_FOR_RENTAL', decodeBTS($('#monthlyRent').prop('checked')));
	validator.setValue('INV_FOR_FINAL_INTERIM', decodeBTS($('#finalInterim').prop('checked')));
	validator.setValue('INV_FOR_INTEREST', decodeBTS($('#interest').prop('checked')));
	validator.setValue('PRODUCT_CODE', $('#leaseProductCode').val());
	validator.setValue('LESSEE_CODE', $('#customerCode').val());
	validator.setValue('CUSTOMER_ID', customerId);
	validator.setValue('AGREEMENT_NO', $('#agreementNo').val());
	validator.setValue('SCHEDULE_ID', $('#scheduleId').val());
	validator.setClass('patterns.config.web.forms.inv.qinvcomponentbean');
	validator.setMethod('getInvoiceDetails');
	validator.sendAndReceiveAsync(showStatus);
}

function showStatus() {
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		if (validator.getValue(ERROR_FIELD) != EMPTY_STRING) {
			hideAllMessages(getProgramID());
			setError(validator.getValue(ERROR_FIELD) + '_error', validator.getValue(ERROR));
			return false;
		} else {
			showMessage(getProgramID(), Message.ERROR, validator.getValue(ERROR));
			hideAllMessages(getProgramID());
		}
	}
	if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		qinvcomponent_Grid.attachEvent("onRowDblClicked", doRowDblClicked);
		qinvcomponent_Grid.loadXMLString(validator.getValue(RESULT_XML));
		hideMessage(getProgramID());
	} else {
		showMessage(getProgramID(), Message.ERROR, validator.getValue(ERROR));
		return false;
	}
}
function doRowDblClicked(rowID) {
	loadDetailView(rowID);
}

function loadDetailView(rowID) {
	showMessage(getProgramID() + "_popup", Message.PROGRESS);
	qinvcomponent_innerGrid.clearAll();
	DetailView();
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('ENTITY_CODE', getEntityCode());
	validator.setValue('ENTRY_DATE', qinvcomponent_Grid.cells(rowID, 9).getValue());
	validator.setValue('ENTRY_SL', qinvcomponent_Grid.cells(rowID, 10).getValue());
	validator.setClass('patterns.config.web.forms.inv.qinvcomponentbean');
	validator.setMethod('loadDetailView');
	validator.sendAndReceiveAsync(showDetail);
}

function showDetail() {
	if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		// queryGrid.attachEvent("onRowDblClicked", doRowDblClicked);
		qinvcomponent_innerGrid.loadXMLString(validator.getValue(RESULT_XML));
		hideMessage(getProgramID() + "_popup");
	} else {
		showMessage(getProgramID() + "_popup", Message.ERROR, validator.getValue(ERROR));
		return false;
	}
}

function DetailView() {
	win = showWindow('innerGridView', EMPTY_STRING, DETAIL_VIEW, EMPTY_STRING, true, false, false, 950, 450);
}