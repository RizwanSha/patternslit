<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<%@taglib prefix="message" tagdir="/WEB-INF/tags/message"%>
<web:bundle baseName="patterns.config.web.forms.inv.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/inv/qinvbulkauthstatus.js" />
	</web:dependencies>
	<style>
.rowCountCaption {
	color: #666d73;
	font-size: 12px;
	font-weight: bold;
}
</style>
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="qinvbulkauthstatus.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/inv/qinvbulkauthstatus" id="qinvbulkauthstatus" method="POST">
				<web:viewContent id="po_view1">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="qinvbulkauthstatus.dueondate" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:date property="dueOnDate" id="dueOnDate" lookup="true" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="qinvbulkauthstatus.processstatus" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:combo property="processStatus" id="processStatus" datasourceid="INV_PROCESS_STATUS" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column span="2">
											<web:button id="submit" key="form.submit" var="common" onclick="loadGrid();" />
											<web:button key="form.reset" id="reset" var="common" onclick="clearFields();" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<message:messageBox id="qinvbulkauthstatus" />
							</web:section>
							<web:section>
								<web:viewTitle var="program" key="qinvbulkauthstatus.section" />
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="3px" />
										<web:columnStyle width="80px" />
										<web:columnStyle width="3px" />
										<web:columnStyle width="80px" />
										<web:columnStyle width="3px" />
										<web:columnStyle width="80px" />
										<web:columnStyle width="3px" />
										<web:columnStyle width="80px" />
										<web:columnStyle width="3px" />
										<web:columnStyle width="80px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<div class="rowCountCaption" id="authTotalLabel"></div>
										</web:column>
										<web:column>
											<span id="authTotal" class="rowCountCaption"></span>
										</web:column>
										<web:column>
											<div class="rowCountCaption" id="authFetchProcessLabel"></div>
										</web:column>
										<web:column>
											<span id="authFetchProcess" class="rowCountCaption"></span>
										</web:column>
										<web:column>
											<div class="rowCountCaption" id="authUnderProcessLabel"></div>
										</web:column>
										<web:column>
											<span id="authUnderProcess" class="rowCountCaption"></span>
										</web:column>
										<web:column>
											<div class="rowCountCaption" id="authSuccessLabel"></div>
										</web:column>
										<web:column>
											<span id="authSuccess" class="rowCountCaption"></span>
										</web:column>
										<web:column>
											<div class="rowCountCaption" id="authFailureLabel"></div>
										</web:column>
										<web:column>
											<span id="authFailure" class="rowCountCaption"></span>
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:rowEven>
										<web:column>
											<web:viewContent id="po_view4">
												<web:grid height="350px" width="996px" id="qinvbulkauthstatus_innerGrid" src="inv/qinvbulkauthstatus_innerGrid.xml" paging="true">
												</web:grid>
											</web:viewContent>
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:viewContent id="view_dtl" styleClass="hidden">
								<web:section>
									<message:messageBox id="component" />
								</web:section>
								<web:section>
									<web:table>
										<web:rowEven>
											<web:column>
												<web:grid height="200px" width="1195px" id="qinvbulkauthstatus_detailGrid" src="inv/qinvbulkauthstatus_detailGrid.xml">
												</web:grid>
											</web:column>
										</web:rowEven>
									</web:table>
								</web:section>
								<web:section>
									<message:messageBox id="breakup" />
								</web:section>
								<web:viewContent id="view_breakupdtl" styleClass="hidden">
									<b><span id="invoiceBreakUp" style="font-size: 14px; color: black; padding: 8px;"></span></b>
									<web:section>
										<web:table>
											<web:rowEven>
												<web:column>
													<web:grid height="200px" width="1105px" id="qinvoiceauth_breakupGrid" src="inv/qinvbulkauthstatus_breakupGrid.xml">
													</web:grid>
												</web:column>
											</web:rowEven>
										</web:table>
									</web:section>
								</web:viewContent>
							</web:viewContent>
						</web:sectionBlock>
					</web:dividerBlock>
				</web:viewContent>
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
	<web:contextSearch />
</web:fragment>