var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'PINVEMAILGEN';
var fromDate = EMPTY_STRING;
var uptoDate = EMPTY_STRING;

function init() {
	showMessage("emailInfo", Message.INFO, PINVEMAILGEN_EMAIL_INFO);
	pinvemailgen_innerGrid.attachEvent("onCheckbox", function(id, ind, value) {
		if (!value)
			$("#masterCheckBox").removeClass("fa fa-check-square-o").addClass("fa fa-square-o");
		if (pinvemailgen_innerGrid.getCheckedRows(0).split(',').length == pinvemailgen_innerGrid.getRowsNum()) {
			$("#masterCheckBox").removeClass("fa fa-square-o").addClass("fa fa-check-square-o");
			$("#masterCheckBox").attr('title', 'Unselect all');
		} else
			$("#masterCheckBox").attr('title', 'Select all');
		return true;
	});
	clearFields();
}

function masterCheckBox() {
	if (pinvemailgen_innerGrid.getRowsNum() >= 1) {
		if ($("#masterCheckBox").hasClass("fa-square-o")) {
			$("#masterCheckBox").removeClass("fa fa-square-o").addClass("fa fa-check-square-o");
			pinvemailgen_innerGrid.checkAll(true);
			$("#masterCheckBox").attr('title', 'Unselect all');
		} else if ($("#masterCheckBox").hasClass("fa-check-square-o")) {
			$("#masterCheckBox").removeClass("fa fa-check-square-o").addClass("fa fa-square-o");
			pinvemailgen_innerGrid.checkAll(false);
			$("#masterCheckBox").attr('title', 'Select all');
		}
	} else {
		$("#masterCheckBox").attr('title', '');
	}
}

function clearFields() {
	$('#invoiceCycleNumber').focus();
	$('#invoiceCycleNumber').val(EMPTY_STRING);
	$('#invoiceCycleNumber_desc').html(EMPTY_STRING);
	$('#invoiceMonth').val(EMPTY_STRING);
	$('#invoiceMonthYear').val(EMPTY_STRING);
	$('#invoiceMonth_desc').html(EMPTY_STRING);
	clearPkFieldsError();
	clearNonPkFields();

}
function clearPkFieldsError() {
	$('#invoiceCycleNumber_error').html(EMPTY_STRING);
	$('#invoiceMonth_error').html(EMPTY_STRING);
}
function clearNonPkFields() {
	hideMessage(CURRENT_PROGRAM_ID);
	pinvemailgen_innerGrid.clearAll();
	mailGrid.clearAll();
	fromDate = EMPTY_STRING;
	uptoDate = EMPTY_STRING;
	
	$("#masterCheckBox").removeClass("fa fa-check-square-o");
	$("#masterCheckBox").addClass("fa fa-square-o");
	$("#masterCheckBox").attr('title', '');
}

function doHelp(id) {
	switch (id) {
	case 'invoiceCycleNumber':
		help('PINVEMAILGEN', 'HLP_INV_CYCLE_NUMBER', $('#invoiceCycleNumber').val(), $('#invoiceCycleNumber').val(), $('#invoiceCycleNumber'), null, function(gridObj, rowID) {
			$('#invoiceCycleNumber').val(gridObj.cells(rowID, 0).getValue());
			$('#invoiceCycleNumber_desc').html(EMPTY_STRING);
			$('#invoiceCycleNumber_error').html(EMPTY_STRING);
		});
		break;
	}
}

function backtrack(id) {
	switch (id) {
	case 'invoiceMonth':
		setFocusLast('invoiceCycleNumber');
		break;
	case 'invoiceMonthYear':
		setFocusLast('invoiceMonth');
		break;
	case 'submit':
		setFocusLast('invoiceMonthYear');
		break;
	}
}

function doclearfields(id) {
	switch (id) {
	case 'invoiceCycleNumber':
		if (isEmpty($('#invoiceCycleNumber').val())) {
			clearFields();
		}
		break;
	case 'invoiceMonthYear':
		if (isEmpty($('#invoiceMonthYear').val())) {
			$('#invoiceMonth_desc').html(EMPTY_STRING);
			clearNonPkFields();
		}
		break;
	}
}
function validate(id) {
	valMode = true;
	switch (id) {
	case 'invoiceCycleNumber':
		invoiceCycleNumber_val(valMode);
		break;
	case 'invoiceMonth':
		invoiceMonth_val(valMode);
		break;
	case 'invoiceMonthYear':
		invoiceMonthYear_val(valMode);
		break;
	case 'submit':
		loadGrid();
		break;
	case 'sendMailId':
		sendMail();
		break;
	case 'reset':
		clearFields();
		break;
	}
}
function invoiceCycleNumber_val(valMode) {
	var value = $('#invoiceCycleNumber').val();
	$('#invoiceCycleNumber_desc').html(EMPTY_STRING);
	clearError('invoiceCycleNumber_error');
	if (isEmpty(value)) {
		setError('invoiceCycleNumber_error', MANDATORY);
		return false;
	} else if (!isNumeric(value)) {
		setError('invoiceCycleNumber_error', INVALID_NUMBER);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('INV_CYCLE_NUMBER', value);
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.inv.pinvemailgenbean');
		validator.setMethod('validateInvoicesForCycleNumber');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('invoiceCycleNumber_error', validator.getValue(ERROR));
			return false;
		} else if (validator.getValue(RESULT) == DATA_AVAILABLE) {
			$('#invoiceCycleNumber_desc').html(validator.getValue('DESCRIPTION'));
		}
	}
	if (valMode)
		setFocusLast('invoiceMonth');
	return true;
}

function invoiceMonth_val(valMode) {
	var value = $('#invoiceMonth').val();
	clearError('invoiceMonth_error');
	if (isEmpty(value)) {
		setError('invoiceMonth_error', MANDATORY);
		return false;
	}
	if (valMode)
		setFocusLast('invoiceMonthYear');
	return true;
}
function invoiceMonthYear_val(valMode) {
	var value = $('#invoiceMonthYear').val();
	clearError('invoiceMonth_error');
	if (isEmpty(value)) {
		setError('invoiceMonth_error', MANDATORY);
		return false;
	}
	if (!isValidYear(value)) {
		setError('invoiceMonth_error', INVALID_YEAR);
		return false;
	}
	if (valMode)
		setFocusLast('submit');
	return true;
}

function loadGrid() {
	clearNonPkFields();
	clearPkFieldsError();
	showMessage(getProgramID(), Message.PROGRESS);
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue(ACTION, USAGE);
	validator.setValue('INV_CYCLE_NUMBER', $('#invoiceCycleNumber').val());
	validator.setValue('INV_YEAR', $('#invoiceMonthYear').val());
	validator.setValue('INV_MONTH', $('#invoiceMonth').val());
	validator.setValue('REVALIDATE', "1");
	validator.setClass('patterns.config.web.forms.inv.pinvemailgenbean');
	validator.setMethod('loadGrid');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		showMessage(getProgramID(), Message.ERROR, validator.getValue(ERROR));
		return false;
	} else if (validator.getValue(RESULT) == DATA_UNAVAILABLE) {
		showMessage(getProgramID(), Message.WARN, NO_RECORD);
	} else if (validator.getValue('ERROR_PRESENT') == ONE) {
		if (validator.getValue('INV_MONTH_ERROR').trim() != EMPTY_STRING)
			$('#invoiceMonth_error').html(validator.getValue('INV_MONTH_ERROR'));
		if (validator.getValue('INV_YEAR_ERROR').trim() != EMPTY_STRING)
			$('#invoiceMonth_error').html(validator.getValue('INV_YEAR_ERROR'));
		if (validator.getValue('INVOICECYCLENUMBER_ERROR').trim() != EMPTY_STRING)
			$('#invoiceCycleNumber_error').html(validator.getValue('INVOICECYCLENUMBER_ERROR'));
		hideMessage(CURRENT_PROGRAM_ID);
	} else if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		xmlString = validator.getValue(RESULT_XML);
		pinvemailgen_innerGrid.loadXMLString(xmlString);
		fromDate = validator.getValue('FROM_DATE');
		uptoDate = validator.getValue('UPTO_DATE');
		hideMessage(CURRENT_PROGRAM_ID);
		$("#masterCheckBox").attr('title', 'Select all');
		$('#submit').blur();
	}
	return true;
}

function sendMail() {
	mailGrid.clearAll();
	if (pinvemailgen_innerGrid.getCheckedRows(0) != EMPTY_STRING) {
		var rowId = pinvemailgen_innerGrid.getCheckedRows(0).split(',');
		for (var i = 0; i < rowId.length; i++) {
			var field_values = [ 'false', pinvemailgen_innerGrid.cells(rowId[i], 1).getValue(), pinvemailgen_innerGrid.cells(rowId[i], 2).getValue(), pinvemailgen_innerGrid.cells(rowId[i], 3).getValue(), pinvemailgen_innerGrid.cells(rowId[i], 4).getValue(),
					pinvemailgen_innerGrid.cells(rowId[i], 5).getValue(), pinvemailgen_innerGrid.cells(rowId[i], 6).getValue(), pinvemailgen_innerGrid.cells(rowId[i], 7).getValue(), pinvemailgen_innerGrid.cells(rowId[i], 8).getValue(), pinvemailgen_innerGrid.cells(rowId[i], 9).getValue(),
					pinvemailgen_innerGrid.cells(rowId[i], 10).getValue(), pinvemailgen_innerGrid.cells(rowId[i], 11).getValue(), pinvemailgen_innerGrid.cells(rowId[i], 12).getValue(), pinvemailgen_innerGrid.cells(rowId[i], 13).getValue(), pinvemailgen_innerGrid.cells(rowId[i], 14).getValue(),
					pinvemailgen_innerGrid.cells(rowId[i], 15).getValue(), pinvemailgen_innerGrid.cells(rowId[i], 16).getValue(), pinvemailgen_innerGrid.cells(rowId[i], 17).getValue(), pinvemailgen_innerGrid.cells(rowId[i], 18).getValue(), pinvemailgen_innerGrid.cells(rowId[i], 19).getValue(),
					pinvemailgen_innerGrid.cells(rowId[i], 20).getValue(), pinvemailgen_innerGrid.cells(rowId[i], 21).getValue() ];
			_grid_updateRow(mailGrid, field_values);
		}
	} else {
		alert(SELECT_ATLEAST_ONE);
		return false;
	}
	showMessage(getProgramID(), Message.PROGRESS);
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue(ACTION, USAGE);
	validator.setValue('INV_CYCLE_NUMBER', $('#invoiceCycleNumber').val());
	validator.setValue('INV_YEAR', $('#invoiceMonthYear').val());
	validator.setValue('INV_MONTH', $('#invoiceMonth').val());
	validator.setValue('REVALIDATE', "0");
	validator.setValue('FROM_DATE', fromDate);
	validator.setValue('UPTO_DATE', uptoDate);
	validator.setValue('XML', mailGrid.serialize());
	validator.setClass('patterns.config.web.forms.inv.pinvemailgenbean');
	validator.setMethod('processAction');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		showMessage(getProgramID(), Message.ERROR, validator.getValue(ERROR));
		return false;
	} else if (validator.getValue(RESULT) == DATA_UNAVAILABLE) {
		showMessage(getProgramID(), validator.getValue(ERROR));
		return false;
	} else if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		showMessage(getProgramID(), Message.SUCCESS, EMAIL_HAS_SENT_FOR_PROCESS);
		pinvemailgen_innerGrid.clearAll();
		xmlString = validator.getValue(RESULT_XML);
		pinvemailgen_innerGrid.loadXMLString(xmlString);
		$("#masterCheckBox").removeClass("fa fa-check-square-o");
		$("#masterCheckBox").addClass("fa fa-square-o");
		$("#masterCheckBox").attr('title', 'Select all');
	}
}

// pop up part
function viewCustomerDetails(customerId) {
	var pkValue = getEntityCode() + PK_SEPERATOR + customerId;
	window.scroll(0, 0);
	showWindow('PREVIEW_VIEW', getBasePath() + 'Renderer/reg/qcustomerreg.jsp', 'QCUSTOMERREG', PK + '=' + pkValue + "&" + SOURCE + "=" + MAIN, true, false, false, 1020, 575);
	resetLoading();
}
function viewInvDetails(invPk) {
	var pkValue = getEntityCode() + PK_SEPERATOR + invPk;
	window.scroll(0, 0);
	showWindow('PREVIEW_VIEW', getBasePath() + 'Renderer/inv/qinvoices.jsp', 'QINVOICES', PK + '=' + pkValue + "&" + SOURCE + "=" + MAIN, true, false, false, 940, 575);
	resetLoading();
}


//Mail Details
function clearSentDetailsFields() {
	mailSentLog_innerGrid.clearAll();
	$('#invoiceDate').val(EMPTY_STRING);
	$('#mailSentLogError_error').html(EMPTY_STRING);
}
function viewSentDetails(invLogPk) {
	win = showWindow('mailSentLog', EMPTY_STRING, EMAIL_SENT_DETAILS, '', true, false, false, 810, 405);
	clearSentDetailsFields();
	invLogPkArray = invLogPk.split(PK_SEPERATOR);
	$('#invoiceDate').val(invLogPkArray[4]);
	$('#customerName').val(invLogPkArray[5]);
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('LEASE_PRODUCT_CODE', invLogPkArray[0]);
	validator.setValue('INV_YEAR', invLogPkArray[1]);
	validator.setValue('INV_MONTH', invLogPkArray[2]);
	validator.setValue('INV_SERIAL', invLogPkArray[3]);
	validator.setClass('patterns.config.web.forms.inv.pinvemailgenbean');
	validator.setMethod('viewSentDetails');
	validator.sendAndReceiveAsync(viewSentDetailsAsync);
}
function viewSentDetailsAsync() {
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('mailSentLogError_error', validator.getValue(ERROR));
		return false;
	} else if (validator.getValue(RESULT) == DATA_UNAVAILABLE) {
		setError('mailSentLogError_error', NO_ROWS_AVAILABLE);
		return false;
	} else if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		xmlString = validator.getValue(RESULT_XML);
		mailSentLog_innerGrid.loadXMLString(xmlString);
	}
}

function clearSentMailFields() {
	mailSentRcp_innerGrid.clearAll();
	$('#mailLogDetailsError_error').html(EMPTY_STRING);
	$('#invoiceRecpDate').val(EMPTY_STRING);
	$('#attachment').removeClass('hidden');
	$('#subject').val(EMPTY_STRING);
	$('#emailContent').val(EMPTY_STRING);
}

function viewMailDetails(EmailLogInvNo) {
	clearSentMailFields();
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('EMAIL_LOG_INVENTORY_NO', EmailLogInvNo);
	validator.setClass('patterns.config.web.forms.inv.pinvemailgenbean');
	validator.setMethod('viewMailDetails');
	win = showWindow('mailLogDetails', EMPTY_STRING, EMAIL_DETAILS, '', true, false, false, 900, 615);
	validator.sendAndReceiveAsync(viewMailDetailsAsync);
}
function viewMailDetailsAsync() {
	var attachment = false;
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('mailLogDetailsError_error', validator.getValue(ERROR));
		return false;
	} else if (validator.getValue(RESULT) == DATA_UNAVAILABLE) {
		setError('mailLogDetailsError_error', NO_ROWS_AVAILABLE);
		return false;
	} else if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		$('#mailLogDetailsError_error').html(EMPTY_STRING);
		$('#subject').html(validator.getValue('EMAIL_SUBJECT'));
		$('#emailContent').html(validator.getValue('EMAIL_TEXT'));
		$('#sentOn').val(validator.getValue('SENT_DATE'));
		xmlString = validator.getValue(RESULT_XML);
		mailSentRcp_innerGrid.loadXMLString(xmlString);
		if (validator.getValue("PDF_REPORT_IDENTIFIER").trim() == EMPTY_STRING || validator.getValue("PDF_REPORT_FILE_NAME").trim() == EMPTY_STRING) {
			$('#pdfDiv').hide();
		} else {
			$('#pdfDiv').show();
			$('#pdfCaption').html(validator.getValue("PDF_REPORT_FILE_NAME"));
			$('#pdf').attr('href', getBasePath() + 'servlet/ReportDownloadProcessor?' + REPORT_ID + '=' + validator.getValue("PDF_REPORT_IDENTIFIER"));
			$('#pdfFile').attr('href', getBasePath() + 'servlet/ReportDownloadProcessor?' + REPORT_ID + '=' + validator.getValue("PDF_REPORT_IDENTIFIER"));
			attachment = true;
		}
		if (validator.getValue("EXCEL_REPORT_IDENTIFIER").trim() == EMPTY_STRING || validator.getValue("EXCEL_REPORT_FILE_NAME").trim() == EMPTY_STRING) {
			$('#xlsDiv').hide();
		} else {
			$('#xlsDiv').show();
			$('#xlsCaption').html(validator.getValue("EXCEL_REPORT_FILE_NAME"));
			$('#xls').attr('href', getBasePath() + 'servlet/ReportDownloadProcessor?' + REPORT_ID + '=' + validator.getValue("EXCEL_REPORT_IDENTIFIER"));
			$('#xlsFile').attr('href', getBasePath() + 'servlet/ReportDownloadProcessor?' + REPORT_ID + '=' + validator.getValue("EXCEL_REPORT_IDENTIFIER"));
			attachment = true;
		}
		if (!attachment)
			$('#attachment').addClass('hidden');
	}
}

function afterClosePopUp(win) {
	switch (win._idd) {
	case 'mailLogDetails':
		dhxWindows = null;
		win = showWindow('mailSentLog', EMPTY_STRING, EMAIL_SENT_DETAILS, '', true, false, false, 810, 390);
		break;

	}
}
