var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'QINVCOMPVIEW';
var pk = '';

function init() {
	clearFields();
	if (!isEmpty(_primaryKey)) {
		loadMainValues(_primaryKey);
		pk = _primaryKey;
	}
}

function loadData() {
	// $('#invoiceNum').val(validator.getValue(''));
	$('#componentReferenceDate').val(validator.getValue('ENTRY_DATE'));
	$('#componentReferenceSerial').val(validator.getValue('ENTRY_SL'));
	$('#invoiceDate').val(validator.getValue('INV_DATE'));
	$('#generationDate').val(validator.getValue('GENERATION_DATE'));
	$('#lesseeCode').val(validator.getValue('LESSEE_CODE'));
	$('#customerId').val(validator.getValue('CUSTOMER_ID'));
	$('#agreementSerial').val(validator.getValue('AGREEMENT_NO'));
	$('#scheduleId').val(validator.getValue('SCHEDULE_ID'));
	$('#customerName').val(validator.getValue('F1_CUSTOMER_NAME'));
	$('#productCode').val(validator.getValue('LEASE_PRODUCT_CODE'));
	$('#productCode_desc').html(validator.getValue('F2_LEASE_PRODUCT_CODE'));
	$('#billingAddressSerial').val(validator.getValue('BILLING_ADDR_SL'));
	$('#lessorBranchCode').val(validator.getValue('LESSOR_BRANCH_CODE'));
	$('#lessorBranchCode_desc').html(validator.getValue('F3_BRANCH_NAME'));
	$('#lessorStateCode').val(validator.getValue('LESSOR_STATE_CODE'));
	$('#lessorStateCode_desc').html(validator.getValue('F4_DESCRIPTION'));
	$('#invoiceType').val(validator.getValue('INV_TYPE'));
	$('#componentAmount_curr').val(getBaseCurrency());
	$('#componentAmount').val(validator.getValue('INV_AMOUNT'));
	$('#createdBy').val(validator.getValue('CREATED_BY'));
	$('#createdOn').val(validator.getValue('CREATED_ON'));
	// $('#invPrint').val(validator.getValue(''));
	// $('#invMail').val(validator.getValue(''));
	loadGrid();
	resetLoading();
	$('#componentAmountFormat').val(formatAmount($('#componentAmount').val(), $('#componentAmount_curr').val()));

}

function clearFields() {
}

function clearNonPKFields() {
}

function clearGridFields() {
}
function loadGrid() {
	var pkvalues = pk.split(PK_SEPERATOR);
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('ENTRY_DATE', pkvalues[1]);
	validator.setValue('ENTRY_SL', pkvalues[2]);
	validator.setClass('patterns.config.web.forms.inv.qinvcompviewbean');
	validator.setMethod('validateLoadGrid');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		return false;
	}
	xmlString = validator.getValue(RESULT_XML);
	qinvcompview_innerGrid.loadXMLString(xmlString);
	// function viewInvoice(pk) {
	// validator.clearMap();
	// validator.setMtm(false);
	// validator.setValue(ACTION, USAGE);
	// validator.setClass('patterns.config.web.forms.inv.qinvoicesbean');
	// validator.setMethod('validateViewInvoice');
	// validator.sendAndReceive();
	// $('#invDate').val(validator.getValue('INV_DATE'));
	// $('#dueDate').val(validator.getValue('DUE_DATE'));
	// $('#customerID').val(validator.getValue('CUSTOMER_ID'));
	// $('#customerID_desc').html(validator.getValue('F1_CUSTOMER_NAME'));
	// $('#productCode').val(validator.getValue('LEASE_PRODUCT_CODE'));
	// $('#productCode_desc').html(validator.getValue('F2_DESCRIPTION'));
	// $('#invoiceType').val(validator.getValue('INV_TYPE'));
	// $('#conso').val(validator.getValue('CONSOLIDATION_BY'));
	// $('#templateCode').val(validator.getValue('INVOICE_TEMPLATE'));
	// $('#templateCode_desc').html(validator.getValue('F2_DESCRIPTION'));
	// $('#stateCode').val(validator.getValue('STATE_CODE'));
	// $('#stateCode_desc').html(validator.getValue('F1_DESCRIPTION'));
	// $('#asset').val(validator.getValue('ASSET_CLASS_CODE'));
	// $('#asset_desc').html(validator.getValue('F3_DESCRIPTION'));
	// $('#famCode').val(validator.getValue('FAM_CODE'));
	// $('#invConso').val(validator.getValue('TOTAL_INV'));
	// $('#fromDate').val(validator.getValue('RENTAL_START_DATE'));
	// $('#toDate').val(validator.getValue('RENTAL_END_DATE'));
	// $('#invAmt').val(validator.getValue('TOTAL_INV_AMT'));
	// $('#generate').val(validator.getValue('CREATED_BY'));
	// $('#genDate').val(validator.getValue('CREATED_ON'));
	//
	// }

	// validator.setValue('CUSTOMER_ID', $('#customerID').val());
	// validator.setValue('ADDR_SL', pk.split(PK_SEPERATOR)[2]);

}

function viewTaxAmtDetails(dtlSl) {
	win = showWindow('taxAmount', EMPTY_STRING, 'DETAIL_VIEW', EMPTY_STRING, true, false, false, 900, 350);
	var pkvalues = pk.split(PK_SEPERATOR);
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('ENTRY_DATE', pkvalues[1]);
	validator.setValue('ENTRY_SL', pkvalues[2]);
	validator.setValue('DTL_SL', dtlSl);
	validator.setClass('patterns.config.web.forms.inv.qinvcompviewbean');
	validator.setMethod('validateTaxGrid');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		return false;
	} else {
		xmlString = validator.getValue(RESULT_XML);
		invcomptaxdtl_innerGrid.clearAll();
		invcomptaxdtl_innerGrid.loadXMLString(xmlString);
	}
}


function closePopup() {
		closeInlinePopUp(win);
	return true;
}