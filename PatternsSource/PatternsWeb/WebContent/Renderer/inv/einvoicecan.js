var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'EINVOICECAN';

function init() {
	refreshQueryGrid();
	if (_redisplay) {
		$('#cancelRefDate').val(getCBD());
		if (!isEmpty($('#xmleinvoiceCanGrid').val())) {
			einvoiceCanGrid.loadXMLString($('#xmleinvoiceCanGrid').val());
		}
		viewHideFilters();
	}
}

function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'EINVOICECAN_MAIN', queryGrid);
}

function doclearfields(id) {
	switch (id) {
	case 'cancelRefDate':
		if (isEmpty($('#cancelRefDate').val())) {
			$('#cancelRefDate').val(EMPTY_STRING);
			$('#cancelRefDate_error').html(EMPTY_STRING);
			$('#cancelRefDate_desc').html(EMPTY_STRING);
			clearNonPKFields();
			break;
		}
	case 'cancelRefSerial':
		if (isEmpty($('#cancelRefSerial').val())) {
			$('#cancelRefSerial').val(EMPTY_STRING);
			$('#cancelRefSerial_error').html(EMPTY_STRING);
			break;
		}
	}
}
function clearFields() {
	$('#cancelRefDate').val(getCBD());
	$('#cancelRefDate_error').html(EMPTY_STRING);
	$('#cancelRefDate_desc').html(EMPTY_STRING);
	$('#cancelRefSerial').val(EMPTY_STRING);
	$('#cancelRefSerial_error').html(EMPTY_STRING);
	$('#cancelRefSerial_desc').html(EMPTY_STRING);
	clearNonPKFields();
}

function clearNonPKFields() {
	$('#leaseProductCode').val(EMPTY_STRING);
	$('#leaseProductCode_desc').html(EMPTY_STRING);
	$('#invMonthYear').val(EMPTY_STRING);
	$('#invMonth').val($("#invMonth option:first-child").val());
	$('#stateCode').val(EMPTY_STRING);
	$('#stateCode_desc').html(EMPTY_STRING);
	$('#dueDate').val(EMPTY_STRING);
	$('#dueDate_error').html(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	einvoiceCanGrid.clearAll();
	clearErrorFields();
}
function clearErrorFields(){
	$('#leaseProductCode_error').html(EMPTY_STRING);
	$('#invMonth_error').html(EMPTY_STRING);
	$('#stateCode_error').html(EMPTY_STRING);
	$('#invMonth_error').html(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
	$('#einvoiceCanGridValue_error').html(EMPTY_STRING);
}
function loadData() {
	$('#cancelRefDate').val(validator.getValue('ENTRY_DATE'));
	$('#cancelRefSerial').val(validator.getValue('ENTRY_SL'));
	$('#remarks').val(validator.getValue('REMARKS'));
	resetLoading();
}

function viewHideFilters() {
	if ($('#additionalFilter').hasClass('hidden')) {
		$('#additionalFilter').removeClass('hidden');
		$("#filter").prop('value', HIDE_FILTER_DETAILS);
	} else {
		$('#additionalFilter').addClass('hidden');
		$("#filter").prop('value', SHOW_FILTER_DETAILS);
	}
}

function view(source, primaryKey) {
	hideParent('inv/qinvoicecan', source, primaryKey);
	resetLoading();
}

function doHelp(id) {
	switch (id) {
	case 'leaseProductCode':
		help('COMMON', 'HLP_LEASE_PROD_CODE', $('#leaseProductCode').val(), EMPTY_STRING, $('#leaseProductCode'));
		break;
	case 'stateCode':
		help('COMMON', 'HLP_STATE_CODE', $('#stateCode').val(), EMPTY_STRING, $('#stateCode'));
		break;
	case 'customerID':
		help('COMMON', 'HLP_CUSTOMER_ID', $('#customerID').val(), EMPTY_STRING, $('#customerID'));
		break;
	}
}

function add() {
	$('#leaseProductCode').focus();
	$('#cancelRefDate').val(getCBD());
}
function modify() {

}

function validate(id) {
	switch (id) {
	case 'cancelRefDate':
		cancelRefDate_val();
		break;
	case 'cancelRefSerial':
		cancelRefSerial_val();
		break;
	case 'leaseProductCode':
		leaseProductCode_val();
		break;
	case 'invMonth':
		invMonth_val();
		break;
	case 'invMonthYear':
		invMonthYear_val();
		break;
	case 'stateCode':
		stateCode_val();
		break;
	case 'customerID':
		customerID_val();
		break;
	case 'dueDate':
		dueDate_val();
		break;
	case 'fetch':
		fetchButton_val();
		break;
	case 'remarks':
		remarks_val();
		break;
	}
}

function backtrack(id) {
	switch (id) {
	case 'leaseProductCode':
		setFocusLast('leaseProductCode');
		break;
	case 'invMonth':
		setFocusLast('leaseProductCode');
		break;
	case 'invMonthYear':
		setFocusLast('invMonth');
		break;
	case 'stateCode':
		setFocusLast('invMonthYear');
		break;
	case 'dueDate':
		setFocusLast('stateCode');
		break;
	case 'customerID':
		setFocusLast('dueDate');
		break;
	case 'fetch':
		if ($('#additionalFilter').hasClass('hidden')) {
			setFocusLast('invMonthYear');
		} else {
			setFocusLast('customerID');
		}
		break;
	case 'remarks':
		setFocusLast('fetch');
		break;

	}
}
function cancelRefDate_val() {
	var value = $('#cancelRefDate').val();
	clearError('cancelRefDate_error');
	if (isEmpty(value)) {
		$('#cancelRefDate').val(getCBD());
		value = $('#cancelRefDate').val();
	}
	if (!isDate(value)) {
		setError('cancelRefDate_error', INVALID_DATE);
		return false;
	}
	setFocusLast('cancelRefSerial');
	return true;
}
function cancelRefSerial_val(valMode) {
	var value = $('#cancelRefSerial').val();
	clearError('cancelRefSerial_error');
	if (isEmpty(value)) {
		setError('cancelRefSerial_error', MANDATORY);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('ENTRY_DATE', $('#cancelRefDate').val());
		validator.setValue('ENTRY_SL', value);
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.inv.einvoicecanbean');
		validator.setMethod('validateCancelRefSerial');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('cancelRefSerial_error', validator.getValue(ERROR));
			return false;
		}
	}
	setFocusLast('lesseCode');
	return true;
}
function leaseProductCode_val() {
	var value = $('#leaseProductCode').val();
	clearError('leaseProductCode_error');
	$('#leaseProductCode_desc').html(EMPTY_STRING);
	if (isEmpty(value)) {
		setError('leaseProductCode_error', MANDATORY);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('LEASE_PRODUCT_CODE', $('#leaseProductCode').val());
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.inv.einvoicecanbean');
		validator.setMethod('validateLeaseProductCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('leaseProductCode_error', validator.getValue(ERROR));
			return false;
		} else {
			$('#leaseProductCode_desc').html(validator.getValue("DESCRIPTION"));
		}
	}
	setFocusLast('invMonth');
	return true;
}

function invMonth_val() {
	var value = $('#invMonth').val();
	clearError('invMonth_error');
	if (isEmpty(value)) {
		setError('invMonth_error', MANDATORY);
		return false;
	}
	setFocusLast('invMonthYear');
	return true;
}
function invMonthYear_val() {
	var value = $('#invMonthYear').val();
	clearError('invMonth_error');
	if (isEmpty(value)) {
		setError('invMonth_error', MANDATORY);
		return false;
	}
	if (!isValidYear(value)) {
		setError('invMonth_error', INVALID_YEAR);
		return false;
	}
	if ($('#additionalFilter').hasClass('hidden')) {
		setFocusLast('fetch');
	} else {
		setFocusLast('stateCode');
	}
	return true;
}
function stateCode_val() {
	var value = $('#stateCode').val();
	clearError('stateCode_error');
	if (!isEmpty(value)) {
		if ($('#action').val() == ADD && !isValidCode(value)) {
			setError('stateCode_error', INVALID_FORMAT);
			return false;
		} else {
			validator.clearMap();
			validator.setMtm(false);
			validator.setValue('STATE_CODE', $('#stateCode').val());
			validator.setValue(ACTION, $('#action').val());
			validator.setClass('patterns.config.web.forms.inv.einvoicecanbean');
			validator.setMethod('validateStateCode');
			validator.sendAndReceive();
			if (validator.getValue(ERROR) != EMPTY_STRING) {
				setError('stateCode_error', validator.getValue(ERROR));
				return false;
			} else {
				$('#stateCode_desc').html(validator.getValue('DESCRIPTION'));
			}
		}
	}
	setFocusLast('dueDate');
	return true;
}
function dueDate_val() {
	var value = $('#dueDate').val();
	clearError('dueDate_error');
	if (!isEmpty(value)) {
		if (!isDate(value)) {
			setError('dueDate_error', INVALID_DATE);
			return false;
		}
		if (!isDateGreater(value, getCBD())) {
			setError('dueDate_error', DATE_LECBD);
			return false;
		}
	}
	setFocusLast('customerID');
	return true;
}
function customerID_val() {
	var value = $('#customerID').val();
	clearError('customerID_error');
	if (!isEmpty(value)) {
		if (!isNumeric(value)) {
			setError('customerID_error', NUMERIC_CHECK);
			return false;
		} else {
			validator.clearMap();
			validator.setMtm(false);
			validator.setValue('CUSTOMER_ID', value);
			validator.setValue(ACTION, USAGE);
			validator.setClass('patterns.config.web.forms.inv.einvoicecanbean');
			validator.setMethod('validateCustomerID');
			validator.sendAndReceive();
			if (validator.getValue(ERROR) != EMPTY_STRING) {
				setError('customerID_error', validator.getValue(ERROR));
				$('#customerID_desc').html(EMPTY_STRING);
				return false;
			}
			$('#customerID_desc').html(validator.getValue('CUSTOMER_NAME'));
		}
	}
	setFocusLast('fetch');
	return true;
}
function fetchButton_val() {
	setFocusLast('remarks');
	return true;
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', INVALID_REMARKS);
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', INVALID_REMARKS);
			return false;
		}
	}
	if (isEmpty(value)) {
		setError('remarks_error', MANDATORY);
		return false;
	}
	setFocusOnSubmit();
	return true;
}
function loadInvoiceDetails() {
	clearErrorFields();
	einvoiceCanGrid.clearAll();
	showMessage(getProgramID(), Message.PROGRESS);
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('LEASE_PRODUCT_CODE', $('#leaseProductCode').val());
	validator.setValue('INV_MONTH', $('#invMonth').val());
	validator.setValue('INV_YEAR', $('#invMonthYear').val());
	validator.setValue('STATE_CODE', $('#stateCode').val());
	validator.setValue('DUE_DATE', $('#dueDate').val());
	validator.setValue('CUSTOMER_ID', $('#customerID').val());
	validator.setValue('ACTION', USAGE);
	validator.setClass('patterns.config.web.forms.inv.einvoicecanbean');
	validator.setMethod('loadInvoiceDetails');
	validator.sendAndReceiveAsync(loadGrid);
}
function revalidate() {
	einvoicecan_revaildateGrid();
}
function einvoicecan_revaildateGrid() {
	var rowID = einvoiceCanGrid.getCheckedRows(0);
	if (rowID != "") {
		$('#xmleinvoiceCanGrid').val(_grid_generatexml_function(einvoiceCanGrid, [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 ]));
		$('#einvoiceCanGridValue_error').html(EMPTY_STRING);
	} else {
		$('#xmleleaseuserattrGrid').val(einvoiceCanGrid.serialize());
		setError('einvoiceCanGridValue_error', SELECT_ATLEAST_ONE);
		errors++;
	}
}
function loadGrid() {
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		if (validator.getValue(ERROR_FIELD) != EMPTY_STRING) {
			hideAllMessages(getProgramID());
			setError(validator.getValue(ERROR_FIELD) + '_error',validator.getValue(ERROR));
			setError('einvoiceCanGridValue_error', EMPTY_STRING);
			return false;
		}else{
			showMessage(getProgramID(), Message.ERROR, validator.getValue(ERROR));
			return false;
		}
	}
	if (validator.getValue(RESULT) == DATA_UNAVAILABLE) {
		showMessage(getProgramID(), Message.WARN, NO_RECORD);
		return false;
	}else{
		einvoiceCanGrid.loadXMLString(validator.getValue(RESULT_XML));
		einvoiceCanGrid.attachEvent("onCheck", doRowSelect);
		hideMessage(CURRENT_PROGRAM_ID);
	}
}
function doRowSelect()
{
	$('#einvoiceCanGridValue_error').html(EMPTY_STRING);
}
