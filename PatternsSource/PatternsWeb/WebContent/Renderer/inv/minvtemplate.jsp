<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<web:bundle baseName="patterns.config.web.forms.inv.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/inv/minvtemplate.js" />
	</web:dependencies>
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="minvtemplate.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/inv/minvtemplate" id="minvtemplate" method="POST">
				<web:dividerBlock>
					<type:optionBox add="true" modify="true" view="true" />
				</web:dividerBlock>
				<web:viewContent id="po_view1">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="minvtemplate.invtempcode" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:invoiceTemplateCode property="invTempCode" id="invTempCode" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="form.description" var="common" mandatory="true" />
										</web:column>
										<web:column>
											<type:description property="description" id="description" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="form.conciseDescription" var="common" mandatory="true" />
										</web:column>
										<web:column>
											<type:conciseDescription property="conciseDescription" id="conciseDescription" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="minvtemplate.templconsol" var="program" />
										</web:column>
										<web:column>
											<type:checkbox property="templconsol" id="templconsol" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="minvtemplate.invtempl" var="program" />
										</web:column>
										<web:column>
											<type:checkbox property="invtempl" id="invtempl" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="minvtemplate.sign" var="program" />
										</web:column>
										<web:column>
											<type:checkbox property="sign" id="sign" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>


							<span id="transactionErrors" class="level4_error"><html:errors property="transactionErrors" /> </span>
							<web:viewContent id="first">
								<type:tabbarBody id="template_TAB_0">
									<web:section>
										<web:table>
											<web:columnGroup>
												<web:columnStyle width="200px" />
												<web:columnStyle width="200px" />
												<web:columnStyle width="200px" />
												<web:columnStyle />
											</web:columnGroup>
											<web:rowEven>
												<web:column>
													<web:legend key="minvtemplate.vatcst" var="program" mandatory="false" />
												</web:column>
												<web:column>
													<type:combo property="vatCst" id="vatCst" datasourceid="COMMON_CONNECT_POSITION" />
												</web:column>
												<web:column>
													<web:legend key="minvtemplate.servicetax" var="program" mandatory="false" />
												</web:column>
												<web:column>
													<type:checkbox property="serviceTax" id="serviceTax" />
												</web:column>
											</web:rowEven>
											<web:rowOdd>

												<web:column>
													<web:legend key="minvtemplate.shadedinvoice" var="program" mandatory="false" />
												</web:column>
												<web:column>
													<type:checkbox property="shadedInvoice" id="shadedInvoice" />
												</web:column>
												<web:column>
													<web:legend key="minvtemplate.generatedate" var="program" mandatory="false" />
												</web:column>
												<web:column>
													<type:checkbox property="generateDate" id="generateDate" />
												</web:column>
											</web:rowOdd>
											<web:rowEven>
												<web:column>
													<web:legend key="minvtemplate.captiontnc" var="program" mandatory="false" />
												</web:column>
												<web:column>
													<type:checkbox property="captionTnc" id="captionTnc" />
												</web:column>
												<web:column>
													<web:legend key="minvtemplate.eoaprint" var="program" mandatory="false" />
												</web:column>
												<web:column>
													<type:checkbox property="eoaPrint" id="eoaPrint" />
												</web:column>
											</web:rowEven>
											<web:rowOdd>
												<web:column>
													<web:legend key="minvtemplate.printleasesch" var="program" mandatory="false" />
												</web:column>
												<web:column>
													<type:checkbox property="printleasesch" id="printleasesch" />
												</web:column>
												<web:column>
													<web:legend key="minvtemplate.printleasedeal" var="program" mandatory="false" />
												</web:column>
												<web:column>
													<type:checkbox property="printleasedeal" id="printleasedeal" />
												</web:column>
											</web:rowOdd>
										</web:table>
									</web:section>
									<web:section>
										<web:table>
											<web:columnGroup>
												<web:columnStyle width="200px" />
												<web:columnStyle />
											</web:columnGroup>
											<web:rowEven>
												<web:column>
													<web:legend key="minvtemplate.printdistamt" var="program" mandatory="false" />
												</web:column>
												<web:column>
													<type:checkbox property="printdistamt" id="printdistamt" />
												</web:column>
											</web:rowEven>
										</web:table>
									</web:section>
								</type:tabbarBody>
							</web:viewContent>
							<web:viewContent id="second">
								<type:tabbarBody id="template_TAB_1">
									<web:section>
										<web:table>
											<web:columnGroup>
												<web:columnStyle width="200px" />
												<web:columnStyle width="200px" />
												<web:columnStyle width="200px" />
												<web:columnStyle />
											</web:columnGroup>
											<web:rowOdd>
												<web:column>
													<web:legend key="minvtemplate.consogrid" var="program" mandatory="false" />
												</web:column>
												<web:column>
													<type:checkbox property="consoGrid" id="consoGrid" />
												</web:column>
												<web:column>
													<web:legend key="minvtemplate.grpreqd" var="program" mandatory="false" />
												</web:column>
												<web:column>
													<type:checkbox property="grpreqd" id="grpreqd" />
												</web:column>
											</web:rowOdd>
											<web:rowEven>
												<web:column>
													<web:legend key="minvtemplate.listanex" var="program" mandatory="false" />
												</web:column>
												<web:column>
													<type:checkbox property="listanex" id="listanex" />
												</web:column>
												<web:column>
													<web:legend key="minvtemplate.grpreqdanex" var="program" mandatory="false" />
												</web:column>
												<web:column>
													<type:checkbox property="grpreqdanex" id="grpreqdanex" />
												</web:column>
											</web:rowEven>
										</web:table>
									</web:section>
									<web:section>
										<web:table>
											<web:columnGroup>
												<web:columnStyle width="200px" />
												<web:columnStyle />
											</web:columnGroup>
											<web:rowOdd>
												<web:column>
													<web:legend key="minvtemplate.annexmode" var="program" mandatory="false" />
												</web:column>
												<web:column>
													<type:combo property="annexMode" id="annexMode" datasourceid="COMMON_ANNEX_MODE" />
												</web:column>
											</web:rowOdd>
										</web:table>
									</web:section>

								</type:tabbarBody>
							</web:viewContent>
							<web:viewContent id="third">
								<type:tabbarBody id="template_TAB_2">
									<web:section>
										<web:table>
											<web:columnGroup>
												<web:columnStyle width="180px" />
												<web:columnStyle />
											</web:columnGroup>
											<web:rowEven>
												<web:column>
													<web:legend key="minvtemplate.fieldid" var="program" mandatory="true" />
												</web:column>
												<web:column>
													<type:fieldId property="fieldId" id="fieldId" />
												</web:column>

											</web:rowEven>
											<web:rowOdd>
												<web:column>
													<web:legend key="minvtemplate.fieldtype" var="program" mandatory="false" />
												</web:column>
												<web:column>
													<type:comboDisplay property="fieldType" id="fieldType" datasourceid="MINVTEMPLATE_FIELD_TYPE" />
												</web:column>
											</web:rowOdd>
										</web:table>
									</web:section>


									<web:section>
										<web:table>
											<web:columnGroup>
												<web:columnStyle width="180px" />
												<web:columnStyle />
											</web:columnGroup>
											<web:rowEven>
												<web:column span="2">
													<web:gridToolbar gridName="minvtemplate_innerGrid" cancelFunction="minvtemplate_cancelGrid" editFunction="minvtemplate_editGrid" addFunction="minvtemplate_addGrid" moveUpDownReq="true" continuousEdit="true" />
													<web:element property="xmlMinvtemplateGrid" />
												</web:column>
											</web:rowEven>
											<web:rowOdd>
												<web:column>
													<web:viewContent id="po_view4">
														<web:grid height="240px" width="582px" id="minvtemplate_innerGrid" src="inv/minvtemplate_innerGrid.xml">
														</web:grid>
													</web:viewContent>
												</web:column>
											</web:rowOdd>
										</web:table>
									</web:section>
									<web:section>
										<web:table>
											<web:columnGroup>
												<web:columnStyle width="200px" />
												<web:columnStyle width="200px" />
												<web:columnStyle width="200px" />
												<web:columnStyle />
											</web:columnGroup>
											<web:rowEven>
												<web:column>
													<web:legend key="minvtemplate.consobasis" var="program" mandatory="false" />
												</web:column>
												<web:column>
													<type:checkbox property="consoBasis" id="consoBasis" />
												</web:column>

												<web:column>
													<web:legend key="minvtemplate.consoname" var="program" mandatory="false" />
												</web:column>
												<web:column>
													<type:otherInformation50 property="consoName" id="consoName" />
												</web:column>
											</web:rowEven>
										</web:table>
									</web:section>

								</type:tabbarBody>
							</web:viewContent>
							<web:viewContent id="fourth">
								<type:tabbarBody id="template_TAB_3">
									<web:section>
										<web:table>
											<web:columnGroup>
												<web:columnStyle width="200px" />
												<web:columnStyle />
											</web:columnGroup>
											<web:rowOdd>
												<web:column>
													<web:legend key="minvtemplate.fieldid" var="program" mandatory="true" />
												</web:column>
												<web:column>
													<type:fieldId property="fieldId2" id="fieldId2" />
												</web:column>
											</web:rowOdd>
										</web:table>
									</web:section>
									<web:section>
										<web:table>
											<web:columnGroup>
												<web:columnStyle width="200px" />
												<web:columnStyle />
											</web:columnGroup>
											<web:rowEven>
												<web:column>
													<web:legend key="minvtemplate.fieldtype" var="program" mandatory="false" />
												</web:column>
												<web:column>
													<type:comboDisplay property="fieldType2" id="fieldType2" datasourceid="MINVTEMPLATE_FIELD_TYPE" />
												</web:column>
											</web:rowEven>
											<web:rowOdd>
												<web:column>
													<web:legend key="minvtemplate.gridanex" var="program" mandatory="true" />
												</web:column>
												<web:column>
													<type:combo property="gridAnex" id="gridAnex" datasourceid="MINVTEMPLATE_GRID_ANEX" />
												</web:column>
											</web:rowOdd>
											<web:rowEven>
												<web:column>
													<web:legend key="minvtemplate.grouptype" var="program" />
												</web:column>
												<web:column>
													<type:combo property="groupType" id="groupType" datasourceid="MINVTEMPLATE_GROUP_TYPE" />
												</web:column>
											</web:rowEven>
										</web:table>
									</web:section>
									<web:section>
										<web:table>
											<web:columnGroup>
												<web:columnStyle width="200px" />
												<web:columnStyle />
											</web:columnGroup>
											<web:rowOdd>
												<web:column span="2">
													<web:gridToolbar gridName="minvtemplate_innerGrid2" cancelFunction="minvtemplate_cancelGrid2" editFunction="minvtemplate_editGrid2" addFunction="minvtemplate_addGrid2" moveUpDownReq="true" continuousEdit="true" />
													<web:element property="xmlMinvtemplateGrid2" />
												</web:column>
											</web:rowOdd>
											<web:rowEven>
												<web:column>
													<web:viewContent id="po_view4">
														<web:grid height="240px" width="782px" id="minvtemplate_innerGrid2" src="inv/minvtemplate_innerGrid2.xml">
														</web:grid>
													</web:viewContent>
												</web:column>
											</web:rowEven>
										</web:table>
									</web:section>
								</type:tabbarBody>
							</web:viewContent>
							<type:tabbar height="height:273px;" name="Layout Configuration $$ Consolidation Configuration  $$ 	Consolidation Basis  $$  Print Attributes and Grouping " width="width:1100px;" required="3" id="template" selected="0">
							</type:tabbar>
							<web:viewContent id="generalFields">
								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="180px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowOdd>
											<web:column>
												<web:legend key="form.enabled" var="common" />
											</web:column>
											<web:column>
												<type:checkbox property="enabled" id="enabled" />
											</web:column>
										</web:rowOdd>
										<web:rowEven>
											<web:column>
												<web:legend key="form.remarks" var="common" />
											</web:column>
											<web:column>
												<type:remarks property="remarks" id="remarks" />
											</web:column>
										</web:rowEven>
										<web:rowOdd>
											<web:column span="2">
												<web:submitReset />
											</web:column>
										</web:rowOdd>
									</web:table>
								</web:section>
							</web:viewContent>
						</web:sectionBlock>
					</web:dividerBlock>
				</web:viewContent>
				<web:element property="command" />
				<web:element property="action" />
				<web:element property="vatCstDisplay" />
				<web:element property="annexModeDisplay" />
				<web:element property="fieldTypeDisplay" />
				<web:element property="fieldType2Display" />
				<web:element property="gridAnexDisplay" />
				<web:element property="groupTypeDisplay" />
				<web:element property="grpreqdHidden" />
				<web:element property="grpreqdanexHidden" />
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
	<web:queryGrid />
	<web:TBAGrid />
	<web:contextSearch />
</web:fragment>
