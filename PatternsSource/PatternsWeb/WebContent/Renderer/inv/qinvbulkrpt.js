var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'EINVBULKRPT';

function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}

}
function clearFields() {
	$('#entryDate').val(EMPTY_STRING);
	$('#entrySerial').val(EMPTY_STRING);
	$('#entrySerial_error').html(EMPTY_STRING);
	$('#entrySerial_desc').html(EMPTY_STRING);
	clearNonPKFields();
}
function clearNonPKFields() {
	$('#invoiceCycleNumber').val(EMPTY_STRING);
	$('#invoiceCycleNumber_error').html(EMPTY_STRING);
	$('#invoiceCycleNumber_desc').html(EMPTY_STRING);
	// $('#invMonth').val(EMPTY_STRING);
	$('#invMonthYear').val(EMPTY_STRING);
	$('#invMonth_error').html(EMPTY_STRING);
	$('#productCode').val(EMPTY_STRING);
	$('#productCode_error').html(EMPTY_STRING);
	$('#productCode_desc').html(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
}

function loadData() {
	$('#entryDate').val(validator.getValue('ENTRY_DATE'));
	$('#entrySerial').val(validator.getValue('ENTRY_SL'));
	$('#invoiceCycleNumber').val(validator.getValue('INV_CYCLE_NUMBER'));
	$('#invMonth').val(validator.getValue('INV_MONTH'));
	$('#invMonthYear').val(validator.getValue('INV_YEAR'));
	$('#productCode').val(validator.getValue('PRODUCT_CODE'));
	$('#productCode_desc').html(validator.getValue('F1_DESCRIPTION'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadAuditFields(validator);
}

function loadGrid() {
	SOURCE_VALUE = _tableType;
	PK_VALUE = _primaryKey;
	if (_tableType == MAIN) {
		loads();
	} else if (_tableType == TBA) {
		loads();
	}
}