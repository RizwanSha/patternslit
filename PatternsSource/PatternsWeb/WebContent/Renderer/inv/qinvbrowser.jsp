<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<%@taglib prefix="message" tagdir="/WEB-INF/tags/message"%>
<web:bundle baseName="patterns.config.web.forms.inv.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/inv/qinvbrowser.js" />
	</web:dependencies>
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="qinvbrowser.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/inv/qinvbrowser" id="qinvbrowser" method="POST">
				<web:viewContent id="po_view1">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle width="400px" />
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="qinvbrowser.duebetweenfromdate" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:date property="fromDate" id="fromDate" />
										</web:column>
										<web:column>
											<web:legend key="qinvbrowser.uptodate" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:date property="uptoDate" id="uptoDate" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:viewContent id="filters_view">
								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="180px" />
											<web:columnStyle width="400px" />
											<web:columnStyle width="180px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowOdd>
											<web:column>
												<web:legend key="qinvbrowser.lesseecode" var="program" />
											</web:column>
											<web:column>
												<type:lesseeCode property="lesseeCode" id="lesseeCode" />
											</web:column>
											<web:column>
												<web:legend key="qinvbrowser.customerid" var="program" />
											</web:column>
											<web:column>
												<type:customerID property="customerId" id="customerId" />
											</web:column>
										</web:rowOdd>
										<web:rowEven>
											<web:column>
												<web:legend key="qinvbrowser.productcode" var="program" />
											</web:column>
											<web:column>
												<type:productCode property="productCode" id="productCode" />
											</web:column>
											<web:column>
												<web:legend key="qinvbrowser.duefor" var="program" mandatory="true" />
											</web:column>
											<web:column>
												<type:combo property="dueFor" id="dueFor" datasourceid="QINVBROWSER_DUE_FOR" />
											</web:column>
										</web:rowEven>
									</web:table>
								</web:section>
								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="180px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowOdd>
											<web:column>
												<web:legend key="qinvbrowser.status" var="program" mandatory="true" />
											</web:column>
											<web:column>
												<type:combo property="status" id="status" datasourceid="QINVBROWSER_STATUS" />
											</web:column>
										</web:rowOdd>
									</web:table>
								</web:section>
							</web:viewContent>
							<web:section>
								<message:messageBox id="qinvbrowser" />
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column span="2">
											<web:button key="form.submit" id="submit" var="common" onclick="loadGridRecords()" />
											<web:button key="form.reset" id="cmdReset" var="common" onclick="clearFields()"  />
										</web:column>
										<web:column align="right">
											<web:button key="qinvbrowser.hidefilter" id="filter" var="program" onclick="viewHideFilters()" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:viewContent id="po_view4">
										<web:grid height="370px" width="1315px" paging="true" id="qinvbrowser_innerGrid" src="inv/qinvbrowser_innerGrid.xml">
										</web:grid>
									</web:viewContent>
								</web:table>
							</web:section>
						</web:sectionBlock>
					</web:dividerBlock>
				</web:viewContent>
				<web:element property="fromDateLowerLimit" />
				<web:element property="nofMonthAllowed" />
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
	<web:contextSearch />
</web:fragment>
