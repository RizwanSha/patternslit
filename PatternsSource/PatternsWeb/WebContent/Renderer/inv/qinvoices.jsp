<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.inv.Messages"
	var="program" />
<web:fragment>

	<style>
.pdfColor {
	color: #ff0000;
}

.pdfColor:hover {
	color: #ff0000;
}

.xlsColor {
	color: #008000;
}

.xlsColor:hover {
	color: #008000;
}
</style>

	<web:dependencies>
		<web:script src="Renderer/inv/qinvoices.js" />
	</web:dependencies>
	<web:loadPK />
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="qinvoices.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:viewContent id="po_view1">
				<web:dividerBlock>
					<web:sectionBlock width="width-100p" align="left">
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="qinvoices.invoicenum" var="program"
											mandatory="true" />
									</web:column>
									<web:column>
										<type:otherInformation50Display property="invoiceNum"
											id="invoiceNum" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle width="200px" />
									<web:columnStyle width="200px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowOdd>
									<web:column>
										<web:legend key="qinvoices.invdate" var="program"
											mandatory="true" />
									</web:column>
									<web:column>
										<type:dateDisplay property="invDate" id="invDate" />
									</web:column>

									<web:column>
										<web:legend key="qinvoices.duedate" var="program"
											mandatory="true" />
									</web:column>
									<web:column>
										<type:dateDisplay property="dueDate" id="dueDate" />
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>


						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="qinvoices.customerid" var="program"
											mandatory="true" />
									</web:column>
									<web:column>
										<type:customerIDDisplay property="customerID" id="customerID" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>


						<web:viewContent id="first">
							<type:tabbarBody id="invoices_TAB_0">
								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="200px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowOdd>
											<web:column>
												<web:legend key="qinvoices.productcode" var="program"
													mandatory="false" />
											</web:column>
											<web:column>
												<type:leaseProductCodeDisplay property="productCode"
													id="productCode" />
											</web:column>
										</web:rowOdd>

										<web:rowEven>
											<web:column>
												<web:legend key="qinvoices.invoicetype" var="program"
													mandatory="false" />
											</web:column>
											<web:column>
												<type:comboDisplay property="invoiceType" id="invoiceType"
													datasourceid="COMMON_INVOICE_TYPE" />
											</web:column>
										</web:rowEven>

									</web:table>
								</web:section>


								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="200px" />
											<web:columnStyle />
										</web:columnGroup>

										<web:rowOdd>
											<web:column>
												<web:legend key="qinvoices.templatecode" var="program"
													mandatory="false" />
											</web:column>
											<web:column>
												<type:codeDisplay property="templateCode" id="templateCode" />
											</web:column>
										</web:rowOdd>

										<web:rowEven>
											<web:column>
												<web:legend key="qinvoices.statecode" var="program"
													mandatory="false" />
											</web:column>
											<web:column>
												<type:stateCodeDisplay property="stateCode" id="stateCode" />
											</web:column>
										</web:rowEven>

									</web:table>
								</web:section>

								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="200px" />
											<web:columnStyle width="200px" />
											<web:columnStyle width="200px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowOdd>
											<web:column>
												<web:legend key="qinvoices.fromdate" var="program"
													mandatory="false" />
											</web:column>
											<web:column>
												<type:dateDisplay property="fromDate" id="fromDate" />
											</web:column>

											<web:column>
												<web:legend key="qinvoices.todate" var="program"
													mandatory="false" />
											</web:column>
											<web:column>
												<type:dateDisplay property="toDate" id="toDate" />
											</web:column>
										</web:rowOdd>
									</web:table>
								</web:section>

								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="200px" />
											<web:columnStyle />
										</web:columnGroup>

										<web:rowEven>
											<web:column>
												<web:legend key="qinvoices.taxamt" var="program"
													mandatory="false" />
											</web:column>
											<web:column>
												<type:currencySmallAmountDisplay property="taxAmt" id="taxAmt" />
											</web:column>
										</web:rowEven>


										<web:rowOdd>
											<web:column>
												<web:legend key="qinvoices.invamt" var="program"
													mandatory="false" />
											</web:column>
											<web:column>
												<type:currencySmallAmountDisplay property="invAmt" id="invAmt" />
											</web:column>
										</web:rowOdd>


										<web:rowEven>
											<web:column>
												<web:legend key="qinvoices.generate" var="program"
													mandatory="false" />
											</web:column>
											<web:column>
												<type:codeDisplay property="generate" id="generate" />
											</web:column>
										</web:rowEven>

										<web:rowOdd>
											<web:column>
												<web:legend key="qinvoices.gendate" var="program"
													mandatory="false" />
											</web:column>
											<web:column>
												<type:dateTimeDisplay property="genDate" id="genDate" />
											</web:column>
										</web:rowOdd>

										<web:rowEven>
											<web:column>
												<web:legend key="qinvoices.invprint" var="program"
													mandatory="false" />
											</web:column>
											<web:column>
												<type:checkboxDisplay property="invPrint" id="invPrint" />
											</web:column>
										</web:rowEven>
									</web:table>
								</web:section>



								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="200px" />
											<web:columnStyle width="50px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowOdd>
											<web:column>
												<web:legend key="qinvoices.invmail" var="program"
													mandatory="false" />
											</web:column>
											<web:column>
												<type:checkboxDisplay property="invMail" id="invMail" />
											</web:column>

											<web:column>
											</web:column>
											<web:column>
												<web:button id="mailSentLogButton" key="qinvoices.mailsent"
													var="program" onclick="viewSentDetails()" />
											</web:column>
										</web:rowOdd>
									</web:table>
								</web:section>
							</type:tabbarBody>
						</web:viewContent>

						<web:viewContent id="second">
							<type:tabbarBody id="invoices_TAB_1">
								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="180px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowEven>
											<web:column span="2">
												<web:grid height="250px" width="993px"
													id="qinvoices_innerGrid" src="inv/qinvoices_innerGrid.xml">
												</web:grid>
											</web:column>
										</web:rowEven>
									</web:table>
								</web:section>
							</type:tabbarBody>
						</web:viewContent>

						<type:tabbar height="height:510px;"
							name="Invoice Details $$ Consolidated Invoices "
							width="width:1350px;" required="2" id="invoices" selected="0">
						</type:tabbar>


						<web:viewContent id="mailSentLog" styleClass="hidden">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="140px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="pinvemailgen.invoiceDate" var="program" />
										</web:column>
										<web:column>
											<type:dateDisplay property="invoiceDate" id="invoiceDate" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="pinvemailgen.customername" var="program" />
										</web:column>
										<web:column>
											<type:nameDisplay property="customerName" id="customerName" />
										</web:column>
									</web:rowOdd>
									<type:error property="mailSentLogError" />
									<web:viewContent id="po_view4">
										<web:rowEven>
											<web:grid height="230px" width="737px"
												id="mailSentLog_innerGrid"
												src="inv/pinvemailgen_mailSentLog_innerGrid.xml">
											</web:grid>
										</web:rowEven>
									</web:viewContent>
								</web:table>
							</web:section>
						</web:viewContent>

						<web:viewContent id="mailSentRecpLog" styleClass="hidden">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="140px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="pinvemailgen.invoiceDate" var="program" />
										</web:column>
										<web:column>
											<type:dateDisplay property="invoiceRecpDate"
												id="invoiceRecpDate" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="pinvemailgen.customername" var="program" />
										</web:column>
										<web:column>
											<type:nameDisplay property="customerNameRecp"
												id="customerNameRecp" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:viewContent id="attachment">
								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="140px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowEven>
											<web:column>
												<web:legend key="pinvemailgen.attachment" var="program" />
												<i class="fa fa-paperclip fa-2x" style="color: #000000"></i>
											</web:column>
											<web:column>
												<div id="pdfDiv">
													<a id="pdfFile" href="#" target="_blank" class="pdfColor">
														<web:legend id="pdf" key="pinvemailgen.pdf" var="program" />
													</a> <a class="fa fa-file-pdf-o fa-2x pdfColor" id="pdf"
														href="#" target="_blank"></a>
												</div>
												<div id="xlsDiv">
													<a id="xlsFile" href="#" target="_blank" class="xlsColor">
														<web:legend id="xls" key="pinvemailgen.xls" var="program" />
													</a> <a class="fa fa-file-excel-o fa-2x xlsColor" id="xls"
														href="#" target="_blank"></a>
												</div>
											</web:column>
										</web:rowEven>
									</web:table>
								</web:section>
							</web:viewContent>
							<type:error property="mailSentRecpLog" />
							<web:viewContent id="po_view4">
								<web:rowEven>
									<web:grid height="240px" width="416px"
										id="mailSentRcp_innerGrid"
										src="inv/pinvemailgen_mailSentRcp_innerGrid.xml">
									</web:grid>
								</web:rowEven>
							</web:viewContent>

						</web:viewContent>

					</web:sectionBlock>
				</web:dividerBlock>
			</web:viewContent>
		</web:viewSubPart>
	</web:viewPart>
</web:fragment>