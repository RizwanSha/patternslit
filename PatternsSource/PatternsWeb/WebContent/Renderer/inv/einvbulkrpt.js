var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'EINVBULKRPT';
function init() {
	refreshTBAGrid();
	refreshQueryGrid();
	$('#entryDate').val(getCBD());
	if (_redisplay) {

	}
}


function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'EINVBULKRPT_MAIN', queryGrid);
}
function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}

function clearFields() {
	setFocusLast('invoiceCycleNumber');
	$('#entryDate').val(getCBD());
	$('#entrySerial').val(EMPTY_STRING);
	$('#entrySerial_error').html(EMPTY_STRING);
	$('#entrySerial_desc').html(EMPTY_STRING);
	clearNonPKFields();
}
function clearNonPKFields() {
	$('#invoiceCycleNumber').val(EMPTY_STRING);
	$('#invoiceCycleNumber_error').html(EMPTY_STRING);
	$('#invoiceCycleNumber_desc').html(EMPTY_STRING);
	//$('#invMonth').val(EMPTY_STRING);
	$('#invMonthYear').val(EMPTY_STRING);
	$('#invMonth_error').html(EMPTY_STRING);
	$('#productCode').val(EMPTY_STRING);
	$('#productCode_error').html(EMPTY_STRING);
	$('#productCode_desc').html(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
}

function loadData() {
	$('#entryDate').val(validator.getValue('ENTRY_DATE'));
	$('#entrySerial').val(validator.getValue('ENTRY_SL'));
	$('#invoiceCycleNumber').val(validator.getValue('INV_CYCLE_NUMBER'));
	$('#invMonth').val(validator.getValue('INV_MONTH'));
	$('#invMonthYear').val(validator.getValue('INV_YEAR'));
	$('#productCode').val(validator.getValue('PRODUCT_CODE'));
	$('#productCode_desc').html(validator.getValue('F1_DESCRIPTION'));
	$('#remarks').val(validator.getValue('REMARKS'));
	resetLoading();
}

function view(source, primaryKey) {
	hideParent('inv/qinvbulkrpt', source, primaryKey);
	resetLoading();
}

function doHelp(id) {
	switch (id) {
	case 'invoiceCycleNumber':
		help('COMMON', 'HLP_INVOICE_CYCLE_NUMBER', $('#invoiceCycleNumber').val(), EMPTY_STRING, $('#invoiceCycleNumber'), null, function(gridObj, rowID) {
			var fromDay = gridObj.cells(rowID, 1).getValue();
			var endDay = gridObj.cells(rowID, 2).getValue();
			$('#invoiceCycleNumber_desc').html(fromDay + " - " + endDay);
		});
		break;
	case 'productCode':
		help('COMMON', 'HLP_LEASE_PROD_CODE', $('#productCode').val(), EMPTY_STRING, $('#productCode'));
		break;
	}
}

function add() {
}
function modify() {
}

function validate(id) {
	switch (id) {
	case 'invoiceCycleNumber':
		invoiceCycleNumber_val();
		break;

	case 'invMonth':
		invoiceMonth_val();
		break;

	case 'invMonthYear':
		invoiceMonthYear_val();
		break;

	case 'productCode':
		productCode_val();
		break;

	case 'remarks':
		remarks_val();
		break;
	}
}

function backtrack(id) {
	switch (id) {
	case 'invMonth':
		setFocusLast('invoiceCycleNumber');
		break;
	case 'invMonthYear':
		setFocusLast('invMonth');
		break;
	case 'productCode':
		setFocusLast('invMonthYear');
		break;
	case 'remarks':
		setFocusLast('productCode');
		break;

	}
}
function invoiceCycleNumber_val() {
	var value = $('#invoiceCycleNumber').val();
	clearError('invoiceCycleNumber_error');
	if (isEmpty(value)) {
		setError('invoiceCycleNumber_error', MANDATORY);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('INV_CYCLE_NUMBER', value);
	validator.setValue(ACTION, USAGE);
	validator.setClass('patterns.config.web.forms.inv.einvbulkrptbean');
	validator.setMethod('validateInvoiceCycleNumber');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('invoiceCycleNumber_error', validator.getValue(ERROR));
		return false;
	}

	setFocusLast('invMonth');
	return true;
}

function invoiceMonth_val() {
	var value = $('#invMonth').val();
	clearError('invMonth_error');
	if (isEmpty(value)) {
		setError('invMonth_error', MANDATORY);
		return false;
	}
	setFocusLast('invMonthYear');
	return true;
}

function invoiceMonthYear_val() {
	var value = $('#invMonthYear').val();
	clearError('invMonth_error');
	if (isEmpty(value)) {
		setError('invMonth_error', MANDATORY);
		return false;
	}
	if (!isValidYear(value)) {
		setError('invMonth_error', INVALID_YEAR);
		return false;
	}
	setFocusLast('productCode');
	return true;
}

function productCode_val() {
	var value = $('#productCode').val();
	clearError('productCode_error');
	if (!isEmpty(value)) {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('LEASE_PRODUCT_CODE', value);
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.inv.einvbulkrptbean');
		validator.setMethod('validateLeaseProductCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('productCode_error', validator.getValue(ERROR));
			return false;
		}
		if (validator.getValue(RESULT) == DATA_AVAILABLE) {
			$('#productCode_desc').html(validator.getValue('DESCRIPTION'));
		}
	}
	setFocusLast('remarks');
	return true;
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', INVALID_REMARKS);
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', INVALID_REMARKS);
			return false;
		}
	}
	if (isEmpty(value)) {
		setError('remarks_error', MANDATORY);
		return false;
	}
	setFocusOnSubmit();
	return true;
}
