<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<%@taglib prefix="message" tagdir="/WEB-INF/tags/message"%>
<web:bundle baseName="patterns.config.web.forms.inv.Messages" var="program" />
<web:fragment>
	<style>
.setCursorPointer {
	cursor: pointer;
	margin-top: -5px;
}

.xlsColor {
	color: #008000;
}

.xlsColor:hover {
	color: #008000;
}

.rowCountCaption {
	color: #008000;
	font-size: 14px;	
	font-weight: bold;
}
</style>
	<web:dependencies>
		<web:script src="Renderer/inv/einvoiceauth.js" />
	</web:dependencies>
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="einvoiceauth.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/inv/einvoiceauth" id="einvoiceauth" method="POST">
				<web:viewContent id="po_view1">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="einvoiceauth.leaseProdCode" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:leaseProductCode property="leaseProdCode" id="leaseProdCode" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="einvoiceauth.invoiceMonthYear" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:yearMonth property="invMonth" id="invMonth" datasourceid="COMMON_MONTHS" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="einvoiceauth.invoiceDate" var="program" />
										</web:column>
										<web:column>
											<type:date property="invoiceDate" id="invoiceDate" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="60px" />
										<web:columnStyle width="600px" />
										<web:columnStyle width="156px" />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:button id="cmdfetch" key="form.fetch" var="common" onclick="loadPendingInvoiceRecords()" />
										</web:column>
										<web:column>
											<div id="xlsDiv" class="setCursorPointer">
												<a id="xlsFile" onclick="doPrint()" class="xlsColor"> <web:legend id="xls" key="form.download" var="common" /></a> <a class="fa fa-file-excel-o fa-2x xlsColor" id="xls" onclick="doPrint()"></a>
											</div>
										</web:column>
										<web:column>
											<div class="rowCountCaption" id="rowCountLabel"></div>
										</web:column>
										<web:column>
											<div id="numberOfCheckedRowInGrid" class="rowCountCaption"></div>
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<message:messageBox id="einvoiceauth" />
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<type:error property="invAuthGrid" />
									<web:rowEven>
										<web:column>
											<web:grid height="358px" width="958px" id="queryGrid" src="inv/einvoiceauth.xml" paging="true">
											</web:grid>
										</web:column>
									</web:rowEven>
									<web:element property="gridXML" />
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="100px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="einvoiceauth.processsAction" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:combo property="processAction" id="processAction" datasourceid="COMMON_AUTHORIZATION_STATUS" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="form.remarks" var="common" />
										</web:column>
										<web:column>
											<type:remarks property="remarks" id="remarks" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column span="2">
											<web:button id="cmdsubmit" key="form.submit" var="common" onclick="processRequest();" />
											<web:button id="cmdreset" key="form.reset" var="common" onclick="clearFields();" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:viewContent id="processXml" styleClass="hidden">
								<web:grid height="0px" width="0px" id="einvoiceauth_processGrid" src="inv/einvoiceauth_process.xml">
								</web:grid>
							</web:viewContent>
							<web:viewContent id="view_dtl" styleClass="hidden">
								<web:section>
									<message:messageBox id="component" />
								</web:section>
								<web:section>
									<web:table>
										<web:rowOdd>
											<web:column>
												<web:grid height="280px" width="1185px" id="einvoiceauth_innerGrid" src="inv/einvoiceauth_innerGrid.xml">
												</web:grid>
											</web:column>
										</web:rowOdd>
									</web:table>
								</web:section>
								<web:section>
									<message:messageBox id="breakup" />
								</web:section>
								<web:viewContent id="view_breakupdtl" styleClass="hidden">
									<b><span id="invoiceBreakUp" style="font-size: 14px; color: black; padding: 8px;"></span></b>
									<web:section>
										<web:table>
											<web:rowOdd>
												<web:column>
													<web:grid height="170px" width="1105px" id="einvoiceauth_breakupGrid" src="inv/einvoiceauth_breakupGrid.xml">
													</web:grid>
												</web:column>
											</web:rowOdd>
										</web:table>
									</web:section>
								</web:viewContent>
							</web:viewContent>
						</web:sectionBlock>
					</web:dividerBlock>
				</web:viewContent>
				<web:element property="command" />
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
	<web:contextSearch />
</web:fragment>