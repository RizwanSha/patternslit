<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.emp.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/emp/qbranch.js" />
	</web:dependencies>
	<web:loadPK />
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="qbranchcode.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:viewContent id="po_view1">
				<web:dividerBlock>
					<web:sectionBlock width="width-100p" align="left">
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="180px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="mbranch.branchcode" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:branchCodeDisplay property="branchCode" id="branchCode" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="mbranch.branchname" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:descriptionDisplay property="branchName" id="branchName" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="mbranch.branchadd" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:remarksDisplay property="branchAdd" id="branchAdd" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="180px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowOdd>
									<web:column>
										<web:legend key="mbranch.pin" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:pinCodeDisplay property="pinCode" id="pinCode" />
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="180px" />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="mbranch.statecode" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:stateCodeDisplay property="stateCode" id="stateCode" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="mbranch.regioncode" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:regionCodeDisplay property="regionCode" id="regionCode" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="mbranch.areacode" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:areaCodeDisplay property="areaCode" id="areaCode" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="180px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowOdd>
									<web:column>
										<web:legend key="mbranch.vattin" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:otherInformation50Display property="vatTin" id="vatTin" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="mbranch.csttin" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:otherInformation50Display property="cstTin" id="cstTin" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="form.enabled" var="common" />
									</web:column>
									<web:column>
										<type:checkboxDisplay property="enabled" id="enabled" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="form.remarks" var="common" />
									</web:column>
									<web:column>
										<type:remarksDisplay property="remarks" id="remarks" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:auditDisplay />
					</web:sectionBlock>
				</web:dividerBlock>
			</web:viewContent>
		</web:viewSubPart>
	</web:viewPart>
</web:fragment>
