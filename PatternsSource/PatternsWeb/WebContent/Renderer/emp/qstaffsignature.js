var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'ISTAFFSIGNATURE';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}

function clearFields() {
	$('#staffCode').val(EMPTY_STRING);
	$('#staffCode_desc').html(EMPTY_STRING);
	$('#effectiveDate').val(EMPTY_STRING);
	$('#fileInventoryNumber').val(EMPTY_STRING);
	$('#referenceNo').val(EMPTY_STRING);
}

function loadData() {
	$('#staffCode').val(validator.getValue('STAFF_CODE'));
	$('#staffCode_desc').html(validator.getValue('F1_NAME'));
	$('#effectiveDate').val(validator.getValue('EFF_DATE'));
	$('#fileInventoryNumber').val(validator.getValue('FILE_INV_NO'));
	$('#referenceNo').val(validator.getValue('REFERENCE_NO'));
	loadAuditFields(validator);
	if (!isEmpty($('#fileInventoryNumber').val()))
		$('#imageScreenView').attr('src', getBasePath() + "servlet/ImageDownloadProcessor?TYPE=M&INVNUM=" + $('#fileInventoryNumber').val() + "&INVSRC=" + MTM_TBA);
}
