var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'ISTAFFSIGNATURE';
var fileExtension;
function init() {
	if ($('#fileExtensionListError').val() != null && $('#fileExtensionListError').val() != EMPTY_STRING) {
		alert($('#fileExtensionListError').val());
		window.location.href = getBasePath() + $('#redirectLandingPagePath').val();
		return false;
	}
	refreshQueryGrid();
	if (_redisplay) {
		$('#staffCode').focus();
		clearExistingFile();
		hide('imageView');
		if (!isEmpty($('#fileInventoryNumberExit').val()))
			$('#imageScreenView').attr('src', getBasePath() + "servlet/ImageDownloadProcessor?TYPE=M&INVNUM=" + $('#fileInventoryNumberExit').val() + "&INVSRC=" + MTM_TBA);
	}
	vault.attachEvent("onFileRemove", function(file) {
		clearExistingFile();
	});
}

function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'ISTAFFSIGNATURE_MAIN');
}

function view(source, primaryKey) {
	hideParent('emp/qstaffsignature', source, primaryKey);
	resetLoading();
}

function clearFields() {
	$('#staffCode').val(EMPTY_STRING);
	$('#staffCode_desc').html(EMPTY_STRING);
	$('#staffCode_error').html(EMPTY_STRING);
	$('#effectiveDate').val(EMPTY_STRING);
	$('#effectiveDate_error').html(EMPTY_STRING);
	clearNonPKFields();
	clearExistingFile();
}

function clearNonPKFields() {
	$('#referenceNo').val(EMPTY_STRING);
	$('#referenceNo_error').html(EMPTY_STRING);
	$('#fileInventoryNumberExit').val(EMPTY_STRING);
	vault.clear();
	$('#imageScreen').attr('src', "");
	$('#imageScreenView').attr('src', "");
}

function clearExistingFile() {
	$('#fileName').val(EMPTY_STRING);
	$('#fileInventoryNumber').val(EMPTY_STRING);
	$('#fileExt').val(EMPTY_STRING);
	if ($('#action').val() == ADD) {
		signature.tabs('signature_1').disable();
		signature._setTabActive('signature_0', true);
	} else {
		signature.tabs('signature_1').enable();
	}
}

function doclearfields(id) {
	switch (id) {
	case 'staffCode':
		if (isEmpty($('#staffCode').val())) {
			$('#staffCode_error').html(EMPTY_STRING);
			$('#staffCode_desc').html(EMPTY_STRING);
			clearFields();
			break;
		}
	case 'effectiveDate':
		if (isEmpty($('#effectiveDate').val())) {
			$('#effectiveDate_error').html(EMPTY_STRING);
			clearNonPKFields();
			break;
		}
	}
}

function loadData() {
	$('#staffCode').val(validator.getValue('STAFF_CODE'));
	$('#staffCode_desc').html(validator.getValue('F1_NAME'));
	$('#effectiveDate').val(validator.getValue('EFF_DATE'));
	$('#fileInventoryNumberExit').val(validator.getValue('FILE_INV_NO'));
	$('#fileExt').val(validator.getValue('F2_FILE_EXTENSION'));
	$('#referenceNo').val(validator.getValue('REFERENCE_NO'));
	if (!isEmpty($('#fileInventoryNumberExit').val()))
		$('#imageScreenView').attr('src', getBasePath() + "servlet/ImageDownloadProcessor?TYPE=M&INVNUM=" + $('#fileInventoryNumberExit').val() + "&INVSRC=" + MTM_TBA);
	resetLoading();
	signature._setTabActive('signature_1', true);
}

function add() {
	$('#staffCode').focus();
	$('#effectiveDate').val(getCBD());
	signature.tabs('signature_1').disable();
	signature._setTabActive('signature_0', true);
}

function modify() {
	$('#staffCode').focus();
	signature.tabs('signature_1').enable();
	signature._setTabActive('signature_1', true);
}

function doHelp(id) {
	switch (id) {
	case 'staffCode':
		help('COMMON', 'HLP_STAFF_CODE', $('#staffCode').val(), EMPTY_STRING, $('#staffCode'));
		break;
	case 'effectiveDate':
		if (!isEmpty($('#staffCode').val()))
			help('ISTAFFSIGNATURE', 'HLP_EFF_DATE', $('#effectiveDate').val(), $('#staffCode').val(), $('#effectiveDate'));
		else {
			setError('staffCode_error', MANDATORY);
			return false;
		}
		break;
	}
}

function validate(id) {
	valMode = true;
	switch (id) {
	case 'staffCode':
		staffCode_val();
		break;
	case 'effectiveDate':
		effectiveDate_val();
		break;
	case 'referenceNo':
		referenceNo_val();
		break;
	}
}

function backtrack(id) {
	switch (id) {
	case 'staffCode':
		setFocusLast('staffCode');
		break;
	case 'effectiveDate':
		setFocusLast('staffCode');
		break;
	case 'referenceNo':
		setFocusLast('effectiveDate');
		break;
	}
}

function staffCode_val(valMode) {
	var value = $('#staffCode').val();
	clearError('staffCode_error');
	$('#staffCode_desc').html(EMPTY_STRING);
	if (isEmpty(value)) {
		setError('staffCode_error', MANDATORY);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('ENTITY_CODE', getEntityCode());
		validator.setValue('STAFF_CODE', $('#staffCode').val());
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.emp.istaffsignaturebean');
		validator.setMethod('validateStaffCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			$('#staffCode_desc').html(EMPTY_STRING);
			setError('staffCode_error', validator.getValue(ERROR));
			return false;
		} else {
			$('#staffCode_desc').html(validator.getValue("NAME"));
		}
	}
	setFocusLast('effectiveDate');
	return true;
}

function effectiveDate_val() {
	var value = $('#effectiveDate').val();
	clearError('effectiveDate_error');
	if (isEmpty(value)) {
		$('#effectiveDate').val(getCBD());
		value = $('#effectiveDate').val();
	}
	if (!isValidEffectiveDate(value, 'effectiveDate_error')) {
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('ENTITY_CODE', getEntityCode());
	validator.setValue('STAFF_CODE', $('#staffCode').val());
	validator.setValue('EFF_DATE', $('#effectiveDate').val());
	validator.setValue(ACTION, $('#action').val());
	validator.setClass('patterns.config.web.forms.emp.istaffsignaturebean');
	validator.setMethod('validateEffectiveDate');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('effectiveDate_error', validator.getValue(ERROR));
		return false;
	}
	if ($('#action').val() == MODIFY) {
		PK_VALUE = getEntityCode() + PK_SEPERATOR + $('#staffCode').val() + PK_SEPERATOR + $('#effectiveDate').val();
		if (!loadPKValues(PK_VALUE, 'effectiveDate_error')) {
			return false;
		}
	}
	setFocusLast('referenceNo');
	return true;
}

function referenceNo_val() {
	var value = $('#referenceNo').val();
	clearError('referenceNo_error');
	if (!isEmpty(value)) {
		if (!isValidRefNumber(value)) {
			setError('referenceNo_error', PBS_INVALID_REF_NUMBER);
			return false;
		}
	}
	setFocusOnSubmit();
	return true;
}

function initFileUpload() {
	vault.setAutoStart(true);
	vault.attachEvent("onFileRemove", function(file) {
		vault.setFilesLimit(1);
	});
}

function beforeFileAdd(file) {
	fileExtension = vault.getFileExtension(file.name);
	var temp = $('#fileExtensionList').val().split(',');
	if ($('#fileInventoryNumber').val() != EMPTY_STRING) {
		alert(PBS_REMOVE_EXISTING_FILE);
		return false;
	}
	if (jQuery.inArray(fileExtension.toLowerCase(), temp) == -1) {
		alert(INVALID_FILE_FORMAT);
		return false;
	}
	return true;
}

function getUploadParameters() {
	param = 'FILE_PATH=' + $('#filePath').val() + '&ALLOWED_EXTENSIONS=' + $('#fileExtensionList').val();
	return param;
}

function afterFileUploadFail(file, extra) {
	alert(extra.param);
	vault.clear();
}

function afterFileUpload(file, extra) {
	$('#fileName').val(file.name);
	$('#fileInventoryNumber').val(extra.invNo);
	$('#fileExt').val(extra.format.toLowerCase());
	if ($('#fileInventoryNumber').val() != EMPTY_STRING) {
		$('#imageScreenView').attr('src', "");
		$('#fileInventoryNumberExit').val(EMPTY_STRING);
	}
	setFocusLast('referenceNo');
}

function revalidate() {
	if (isEmpty($('#fileInventoryNumber').val()) && isEmpty($('#fileInventoryNumberExit').val())) {
		alert(FILE_NOT_YET_UPLOADED);
		errors++;
		return false;
	}
	if (!isEmpty($('#fileInventoryNumber').val())) {
		var temp = $('#fileExtensionList').val().split(',');
		if (jQuery.inArray(fileExtension.toLowerCase(), temp) == -1) {
			alert(INVALID_FILE_FORMAT);
			errors++;
			return false;
		}
	} else if (isEmpty($('#fileInventoryNumberExit').val())) {
		alert(FILE_NOT_YET_UPLOADED);
		errors++;
		return false;
	}
}

function viewUploadedImage() {
	if (isEmpty($('#fileInventoryNumber').val())) {
		alert(FILE_NOT_YET_UPLOADED);
		return false;
	} else {
		$('#imageScreen').attr('src', "");
		win = showWindow('imageView', EMPTY_STRING, 'Staff Signature Image', '', true, false, false, 500, 500);
		$('#imageScreen').attr('src', getBasePath() + "servlet/ImageDownloadProcessor?TYPE=M&INVNUM=" + $('#fileInventoryNumber').val() + "&INVSRC=" + MTM_TBA);
	}
}

function closePopup(value) {
	if (value == 0) {
		closeInlinePopUp(win);
	}
	setFocusLast('referenceNo');
	return true;
}
