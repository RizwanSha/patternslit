var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MBRANCH';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}
function clearFields() {
	$('#branchCode').val(EMPTY_STRING);
	$('#branchCode_desc').html(EMPTY_STRING);
	$('#branchName').val(EMPTY_STRING);
	$('#branchAdd').val(EMPTY_STRING);
	$('#pinCode').val(EMPTY_STRING);
	$('#pinCode_desc').html(EMPTY_STRING);
	$('#stateCode').val(EMPTY_STRING);
	$('#stateCode_desc').html(EMPTY_STRING);
	$('#regionCode').val(EMPTY_STRING);
	$('#regionCode_desc').html(EMPTY_STRING);
	$('#areaCode').val(EMPTY_STRING);
	$('#vatTin').val(EMPTY_STRING);
	$('#cstTin').val(EMPTY_STRING);
	setCheckbox('enabled', YES);
	$('#remarks').val(EMPTY_STRING);
}

function loadData() {
	$('#branchCode').val(validator.getValue('BRANCH_CODE'));
	$('#branchName').val(validator.getValue('BRANCH_NAME'));
	$('#branchAdd').val(validator.getValue('ADDRESS'));
	$('#pinCode').val(validator.getValue('PIN_CODE'));
	$('#stateCode').val(validator.getValue('STATE_CODE'));
	$('#stateCode_desc').html(validator.getValue('F1_DESCRIPTION'));
	$('#regionCode').val(validator.getValue('REGION_CODE'));
	$('#regionCode_desc').html(validator.getValue('F2_DESCRIPTION'));
	$('#areaCode').val(validator.getValue('AREA_CODE'));
	$('#areaCode_desc').html(validator.getValue('F3_DESCRIPTION'));
	$('#vatTin').val(validator.getValue('VAT_TIN_NO'));
	$('#cstTin').val(validator.getValue('CST_TIN_NO'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	resetLoading();
	loadAuditFields(validator);
}
