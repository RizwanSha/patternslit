<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<%@taglib prefix="resource" tagdir="/WEB-INF/tags/resource"%>
<web:bundle baseName="patterns.config.web.forms.emp.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/emp/istaffsignature.js" />
	</web:dependencies>
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="istaffsignature.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/emp/istaffsignature" id="istaffsignature" method="POST">
				<web:dividerBlock>
					<type:optionBox add="true" modify="true" view="true" />
				</web:dividerBlock>
				<web:viewContent id="po_view1">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="190px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="istaffsignature.staffCode" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:staffCode property="staffCode" id="staffCode" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="form.effectivedate" var="common" mandatory="true" />
										</web:column>
										<web:column>
											<type:date property="effectiveDate" id="effectiveDate" lookup="true" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>

							<type:tabbarBody id="signature_TAB_0">
								<web:viewContent id="uploadFile">
									<web:section>
										<web:table>
											<web:columnGroup>
												<web:columnStyle width="180px" />
												<web:columnStyle />
											</web:columnGroup>
											<web:rowOdd>
												<web:column>
													<web:legend key="istaffsignature.uploadfile" var="program" mandatory="true" />
												</web:column>
												<web:column>
													<resource:fileUpload id="vault" width="400px" height="100px" initFileUpload="initFileUpload" beforeFileAdd="beforeFileAdd" afterFileUpload="afterFileUpload" afterFileUploadFail="afterFileUploadFail" filelimit="1" />
												</web:column>
											</web:rowOdd>
										</web:table>
									</web:section>
								</web:viewContent>
								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="180px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowEven>
											<web:column>
												<web:legend key="istaffsignature.filename" var="program" />
											</web:column>
											<web:column>
												<type:fileName id="fileName" property="fileName" />
											</web:column>
										</web:rowEven>
										<web:rowOdd>
											<web:column>
												<web:legend key="efileupload.fileextensionlist" var="common" />
											</web:column>
											<web:column>
												<type:fileExtensionList id="fileExtensionList" property="fileExtensionList" />
											</web:column>
										</web:rowOdd>
									</web:table>
								</web:section>
								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="180px" />
											<web:columnStyle width="180px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowEven>
											<web:column>
												<web:legend key="istaffsignature.fileinventorynumber" var="program" />
											</web:column>
											<web:column>
												<type:fileInventory id="fileInventoryNumber" property="fileInventoryNumber" />
											</web:column>
											<web:column>
												<type:button key="istaffsignature.uploadedImage" id="uploadedImage" var="program" onclick="viewUploadedImage();" />
											</web:column>
										</web:rowEven>
									</web:table>
								</web:section>
							</type:tabbarBody>

							<type:tabbarBody id="signature_TAB_1">
								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="180px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowEven>
											<web:column>
												<web:legend key="istaffsignature.fileinventorynumber" var="program" />
											</web:column>
											<web:column>
												<type:fileInventory id="fileInventoryNumberExit" property="fileInventoryNumberExit" />
											</web:column>
										</web:rowEven>
									</web:table>
								</web:section>
								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="180px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowOdd>
											<web:column>
												<web:legend key="istaffsignature.imageview" var="program" />
											</web:column>
											<web:column>
												<img id="imageScreenView" style="width: 240px; height: 180px;" />
											</web:column>
										</web:rowOdd>
									</web:table>
								</web:section>
							</type:tabbarBody>

							<type:tabbar height="height :270px;" name="Add New $$ View Existing " width="width :1065px;" required="2" id="signature" selected="0">
							</type:tabbar>

							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="190px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="istaffsignature.referenceno" var="program" />
										</web:column>
										<web:column>
											<type:otherInformation25 property="referenceNo" id="referenceNo" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column span="2">
											<web:submitReset />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>


							<web:viewContent id="imageView" styleClass="hidden">
								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="80px" />
										</web:columnGroup>
										<web:rowOdd>
											<web:column>
												<img id="imageScreen" style="width: 450px; height: 400px;" />
											</web:column>
										</web:rowOdd>
										<web:rowOdd>
											<web:column span="2">
												<p align="center">
													<type:button key="form.cancel" id="cancel" var="common" onclick="closePopup('0')" />
												</p>
											</web:column>
										</web:rowOdd>
									</web:table>
								</web:section>
							</web:viewContent>

						</web:sectionBlock>
					</web:dividerBlock>
				</web:viewContent>
				<web:element property="command" />
				<web:element property="action" />
				<web:element property="fileExt" />
				<web:element property="redirectLandingPagePath" />
				<web:element property="fileExtensionListError" />
				<web:element property="filePath" />
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
	<web:queryGrid />
	<web:contextSearch />
</web:fragment>
