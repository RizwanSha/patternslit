var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MLAMHIER';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}

function clearFields() {
	$('#hierarchyCode').val(EMPTY_STRING);
	$('#name').val(EMPTY_STRING);
	$('#shortName').val(EMPTY_STRING);
	setCheckbox('enabled', YES);
	$('#remarks').val(EMPTY_STRING);
}

function loadData() {
	$('#hierarchyCode').val(validator.getValue('LAM_HIER_CODE'));
	$('#name').val(validator.getValue('DESCRIPTION'));
	$('#shortName').val(validator.getValue('CONCISE_DESCRIPTION'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadAuditFields(validator);
}
