var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MSTAFFROLE';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}
function clearFields() {
	$('#staffRoleCode').val(EMPTY_STRING);
	$('#staffRoleCode_desc').html(EMPTY_STRING);
	$('#description').val(EMPTY_STRING);
	$('#conciseDescription').val(EMPTY_STRING);
	$('#parentStaffRoleCode').val(EMPTY_STRING);
	$('#parentStaffRoleCode_desc').html(EMPTY_STRING);
	$('#roleCategory').prop('selectedIndex', 0);
	setCheckbox('enabled', NO);
	$('#remarks').val(EMPTY_STRING);
	hide('hierarchy_view');
}
function loadData() {
	$('#staffRoleCode').val(validator.getValue('STAFF_ROLE_CODE'));
	$('#description').val(validator.getValue('DESCRIPTION'));
	$('#conciseDescription').val(validator.getValue('CONCISE_DESCRIPTION'));
	$('#parentStaffRoleCode').val(validator.getValue('PARENT_STAFF_ROLE_CODE'));
	$('#parentStaffRoleCode_desc').html(validator.getValue('F1_DESCRIPTION'));
	$('#roleCategory').val(validator.getValue('ROLE_CATEGORY'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	setCheckbox('roleInBranch', validator.getValue('BRANCH_ROLE'));
	setCheckbox('regionalRole', validator.getValue('REGIONAL_ROLE'));
	setCheckbox('nationalRole', validator.getValue('NATIONAL_ROLE'));
	$('#remarks').val(validator.getValue('REMARKS'));
	if (!isEmpty($('#parentStaffRoleCode').val())) {
		$('#roleCategory').val(validator.getValue("F1_ROLE_CATEGORY"));
		show('hierarchy_view');
	}
	loadAuditFields(validator);

}

function viewHierarchy() {
	var pkValue = getEntityCode() + PK_SEPERATOR + $('#parentStaffRoleCode').val();
	window.scroll(0, 0);
	showWindow('PREVIEW_VIEW', getBasePath() + 'Renderer/emp/qviewrolehier.jsp', 'QVIEWROLEHIER', PK + '=' + pkValue + "&" + SOURCE + "=" + MAIN, true, false, false, 550, 400);
	resetLoading();
}