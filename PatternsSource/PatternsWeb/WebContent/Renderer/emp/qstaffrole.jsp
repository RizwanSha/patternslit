<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.emp.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/emp/qstaffrole.js" />
	</web:dependencies>
	<web:loadPK />
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="mstaffrole.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:viewContent id="po_view1">
				<web:dividerBlock>
					<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="mstaffrole.staffrolecode" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:staffRoleCodeDisplay property="staffRoleCode" id="staffRoleCode" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="form.description" var="common" mandatory="true" />
										</web:column>
										<web:column>
											<type:descriptionDisplay property="description" id="description" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="form.conciseDescription" var="common" mandatory="true" />
										</web:column>
										<web:column>
											<type:conciseDescriptionDisplay property="conciseDescription" id="conciseDescription" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="mstaffrole.parentstaffcode" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:staffRoleCodeDisplay property="parentStaffRoleCode" id="parentStaffRoleCode" />
										</web:column>
									</web:rowOdd>
										</web:table>
							</web:section>
							<web:viewContent id="hierarchy_view" styleClass="hidden">
								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="200px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowOdd>
											<web:column>
											</web:column>
											<web:column>
												<web:anchorMessage var="program" key="mstaffrole.link" onclick="viewHierarchy()" style="text-decoration:underline;cursor:pointer" />
											</web:column>
										</web:rowOdd>
									</web:table>
								</web:section>
							</web:viewContent>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="mstaffrole.rolecategory" var="program" />
										</web:column>
										<web:column>
											<type:comboDisplay property="roleCategory" id="roleCategory" datasourceid="COMMON_ROLE_CATEGORY" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="mstaffrole.roleinbranch" var="program" />
										</web:column>
										<web:column>
											<type:checkboxDisplay property="roleInBranch" id="roleInBranch"  />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="mstaffrole.regionalrole" var="program" />
										</web:column>
										<web:column>
											<type:checkboxDisplay property="regionalRole" id="regionalRole"/>
										</web:column>
									</web:rowEven>
										<web:rowOdd>
										<web:column>
											<web:legend key="mstaffrole.nationalrole" var="program" />
										</web:column>
										<web:column>
											<type:checkboxDisplay property="nationalRole" id="nationalRole"  />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="form.enabled" var="common" />
										</web:column>
										<web:column>
											<type:checkboxDisplay property="enabled" id="enabled" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="form.remarks" var="common" />
										</web:column>
										<web:column>
											<type:remarksDisplay property="remarks" id="remarks" />
										</web:column>
									</web:rowOdd>
							</web:table>
						</web:section>
						<web:auditDisplay />
					</web:sectionBlock>
				</web:dividerBlock>
			</web:viewContent>
		</web:viewSubPart>
	</web:viewPart>
</web:fragment>