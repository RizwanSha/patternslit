var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MSTAFF';
function init() {
	show('details_linkId');
	refreshTBAGrid();
	refreshQueryGrid();

}
function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'MSTAFF_MAIN', queryGrid);
}

function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}

function clearFields() {
	$('#staffEmpCode').val(EMPTY_STRING);
	$('#staffEmpCode_error').html(EMPTY_STRING);
	$('#staffEmpCode_desc').html(EMPTY_STRING);
	clearNonPKFields();
}

function clearNonPKFields() {
	$('#name').val(EMPTY_STRING);
	$('#name_error').html(EMPTY_STRING);
	$('#dateOfJoining').val(EMPTY_STRING);
	$('#dateOfJoining_error').html(EMPTY_STRING);
	$('#emailId').val(EMPTY_STRING);
	$('#emailId_error').html(EMPTY_STRING);
	$('#mobileNumber').val(EMPTY_STRING);
	$('#mobileNumber_error').html(EMPTY_STRING);
	$('#staffRole').val(EMPTY_STRING);
	$('#staffRole_error').html(EMPTY_STRING);
	$('#staffRole_desc').html(EMPTY_STRING);
	$('#branchCode').val(EMPTY_STRING);
	$('#branchCode_error').html(EMPTY_STRING);
	$('#branchCode_desc').html(EMPTY_STRING);
	$('#regionCode').val(EMPTY_STRING);
	$('#regionCode_error').html(EMPTY_STRING);
	$('#regionCode_desc').html(EMPTY_STRING);
	$('#areaCode').val(EMPTY_STRING);
	$('#areaCode_error').html(EMPTY_STRING);
	$('#areaCode_desc').html(EMPTY_STRING);
	$('#reportingToStaffId').val(EMPTY_STRING);
	$('#reportingToStaffId_error').html(EMPTY_STRING);
	$('#reportingToStaffId_desc').html(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
}

function openAddinfo() {
	show('clickheretoviewthedetails', $('#clickheretoviewthedetails_pic')[0]);
	hide('details_linkId');
}

function add() {
	$('#staffEmpCode').focus();
}

function modify() {
	$('#staffEmpCode').focus();
}

function loadData() {
	$('#staffEmpCode').val(validator.getValue('STAFF_CODE'));
	$('#name').val(validator.getValue('NAME'));
	$('#dateOfJoining').val(validator.getValue('DATE_OF_JOINING'));
	$('#emailId').val(validator.getValue('EMAIL_ID'));
	$('#mobileNumber').val(validator.getValue('MOBILE_NO'));
	$('#staffRole').val(validator.getValue('STAFF_ROLE_CODE'));
	$('#staffRole_desc').html(validator.getValue('F4_DESCRIPTION'));
	$('#branchCode').val(validator.getValue('BRANCH_CODE'));
	$('#branchCode_desc').html(validator.getValue('F1_BRANCH_NAME'));
	$('#regionCode').val(validator.getValue('REGION_CODE'));
	$('#regionCode_desc').html(validator.getValue('F2_DESCRIPTION'));
	$('#areaCode').val(validator.getValue('AREA_CODE'));
	$('#areaCode_desc').html(validator.getValue('F3_DESCRIPTION'));
	$('#reportingToStaffId').val(validator.getValue('REPORTING_STAFF_CODE'));
	if (!isEmpty($('#reportingToStaffId').val()))
		$('#reportingToStaffId_desc').html(validator.getValue('F5_NAME'));
	$('#remarks').val(validator.getValue('REMARKS'));

}

function view(source, primaryKey) {
	hideParent('emp/qstaff', source, primaryKey);
	resetLoading();

}

function doHelp(id) {

	switch (id) {
	case 'staffEmpCode':
		help('COMMON', 'HLP_STAFF_CODE', $('#staffEmpCode').val(), EMPTY_STRING, $('#staffEmpCode'));
		break;
	}

}

function doclearfields(id) {
	switch (id) {
	case 'staffEmpCode':
		if (isEmpty($('#staffEmpCode').val())) {
			clearFields();
			break;
		}
	}

}

function validate(id) {
	var valMode = true;
	switch (id) {
	case 'staffEmpCode':
		staffEmpCode_val();
		break;
	case 'name':
		name_val();
		break;
	case 'dateOfJoining':
		dateOfJoining_val();
		break;
	case 'emailId':
		emailId_val(valMode);
		break;
	case 'mobileNumber':
		mobileNumber_val();
		break;
	case 'staffRole':
		staffRole_val();
		break;
	case 'branchCode':
		branchCode_val();
		break;
	case 'regionCode':
		regionCode_val();
		break;
	case 'areaCode':
		areaCode_val();
		break;
	case 'reportingToStaffId':
		reportingToStaffId_val();
		break;
	case 'remarks':
		remarks_val();
		break;
	}

}

function backtrack(id) {
	switch (id) {
	case 'staffEmpCode':
		setFocusLast('staffEmpCode');
		break;
	case 'name':
		setFocusLast('staffEmpCode');
		break;
	case 'dateOfJoining':
		setFocusLast('name');
		break;
	case 'emailId':
		setFocusLast('dateOfJoining');
		break;
	case 'mobileNumber':
		setFocusLast('emailId');
		break;
	case 'staffRole':
		setFocusLast('currentStatus');
		break;
	case 'branchCode':
		setFocusLast('staffRole');
		break;
	case 'regionCode':
		setFocusLast('branchCode');
		break;
	case 'areaCode':
		setFocusLast('regionCode');
		break;
	case 'reportingToStaffId':
		setFocusLast('areaCode');
		break;
	case 'remarks':
		setFocusLast('mobileNumber');
		break;
	}

}

function staffEmpCode_val() {
	var value = $('#staffEmpCode').val();
	clearError('staffEmpCode_error');
	$('#staffEmpCode_desc').html(EMPTY_STRING);
	if (isEmpty(value)) {
		setError('staffEmpCode_error', MANDATORY);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('STAFF_CODE', value);
		validator.setValue(ACTION, $('#action').val());
		validator.setClass('patterns.config.web.forms.emp.mstaffbean');
		validator.setMethod('validateStaffEmpCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('staffEmpCode_error', validator.getValue(ERROR));
			$('#staffEmpCode_desc').html(EMPTY_STRING);
			return false;
		} else {
			$('#staffEmpCode_desc').html(validator.getValue('DESCRIPTION'));
			if ($('#action').val() == MODIFY) {
				PK_VALUE = getEntityCode() + PK_SEPERATOR + $('#staffEmpCode').val();
				if (!loadPKValues(PK_VALUE, 'staffEmpCode_error')) {
					return false;
				}
			}
		}
	}
	setFocusLast('name');
	return true;
}

function name_val() {
	var value = $('#name').val();
	clearError('name_error');
	if (isEmpty(value)) {
		setError('name_error', MANDATORY);
		return false;
	}
	if (!isValidName(value)) {
		setError('name_error', INVALID_NAME);
		return false;
	}
	setFocusLast('dateOfJoining');
	return true;
}

function dateOfJoining_val() {
	var value = $('#dateOfJoining').val();
	clearError('dateOfJoining_error');
	if (isEmpty(value)) {
		setError('dateOfJoining_error', MANDATORY);
		return false;
	}
	if (!isDate(value)) {
		setError('dateOfJoining_error', INVALID_DATE);
		return false;
	}
	if (!isDateLesserEqual(value, getCBD())) {
		setError('dateOfJoining_error', HMS_DATE_LECBD);
		return false;
	}
	setFocusLast('emailId');
	return true;
}

function emailId_val() {
	var value = $('#emailId').val();
	clearError('emailId_error');
	if (isEmpty(value)) {
		setError('emailId_error', MANDATORY);
		return false;
	}
	if (!isEmail(value)) {
		setError('emailId_error', HMS_INVALID_EMAIL_ID);
		return false;
	}
	setFocusLast('mobileNumber');
	return true;
}

function mobileNumber_val() {
	var value = $('#mobileNumber').val();
	clearError('mobileNumber_error');
	if (isEmpty(value)) {
		setError('mobileNumber_error', MANDATORY);
		return false;
	}
	if (!isMobile(value)) {
		setError('mobileNumber_error', INVALID_MOBILE_NUMBER);
		return false;
	}
	setFocusLast('remarks');
	return true;
}

function currentStaffRole_val() {
	setFocusLast('remarks');
	return true;

}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', INVALID_REMARKS);
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', INVALID_REMARKS);
			return false;
		}
	}
	if ($('#action').val() == MODIFY) {
		if (isEmpty(value)) {
			setError('remarks_error', MANDATORY);
			return false;
		}
	}
	setFocusOnSubmit();
	return true;

}

function toggle(id, ref) {
	if ($('#' + id).hasClass('hidden')) {
		$('#' + id).removeClass('hidden');
		if ($(ref).length > 0) {
			$(ref).find('.pic').attr('src', IMAGE_BASE_PATH + 'maximized.png');
		}
		hide('details_linkId');
	} else {
		$('#' + id).addClass('hidden');
		if ($(ref).length > 0) {
			$(ref).find('.pic').attr('src', IMAGE_BASE_PATH + 'minimized.png');
		}
		show('details_linkId');
	}
	return false;
}
