<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.emp.Messages"
	var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/emp/qstaff.js" />
	</web:dependencies>
	<web:loadPK />
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="mstaff.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:dividerBlock>
				<web:viewContent id="po_view1">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="mstaff.staffempcode" var="program"
												mandatory="true" />
										</web:column>
										<web:column>
											<type:staffCodeDisplay property="staffEmpCode"
												id="staffEmpCode" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="mstaff.name" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:descriptionDisplay property="name" id="name" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="mstaff.dateofjoining" var="program"
												mandatory="true" />
										</web:column>
										<web:column>
											<type:dateDisplay property="dateOfJoining" id="dateOfJoining" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="mstaff.emailid" var="program" />
										</web:column>
										<web:column>
											<type:emailDisplay property="emailId" id="emailId" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="mstaff.mobilenumber" var="program" />
										</web:column>
										<web:column>
											<type:mobileDisplay property="mobileNumber" id="mobileNumber" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:subProgramTitle var="program" key="mstaff.section"
								status="close" />
							<web:viewContent id="details_linkId" styleClass="hidden">
							</web:viewContent>
							<web:section>
								<web:table>
									<web:rowOdd>
										<web:column>
											<web:legend key="mstaff.staffrole" var="program" />
										</web:column>
										<web:column>
											<type:staffRoleDisplay property="staffRole" id="staffRole" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="mstaff.branchcode" var="program" />
										</web:column>
										<web:column>
											<type:branchCodeDisplay property="branchCode" id="branchCode" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="mstaff.regioncode" var="program" />
										</web:column>
										<web:column>
											<type:regionCodeDisplay property="regionCode" id="regionCode" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="mstaff.areacode" var="program" />
										</web:column>
										<web:column>
											<type:areaCodeDisplay property="areaCode" id="areaCode" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="mstaff.reportingtostaffid" var="program" />
										</web:column>
										<web:column>
											<type:staffCodeDisplay property="reportingToStaffId"
												id="reportingToStaffId" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="form.remarks" var="common" />
										</web:column>
										<web:column>
											<type:remarksDisplay property="remarks" id="remarks" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:auditDisplay />
						</web:sectionBlock>
					</web:dividerBlock>
				</web:viewContent>
			</web:dividerBlock>
		</web:viewSubPart>
	</web:viewPart>
</web:fragment>