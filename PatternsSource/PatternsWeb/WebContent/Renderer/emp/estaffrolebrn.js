var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'ESTAFFROLEBRN';

function init() {
	refreshTBAGrid();
	refreshQueryGrid();
	if (_redisplay) {
		if ($('#action').val() == MODIFY) {
			$('#enabled').prop('disabled', false);
		} else {
			setCheckbox('enabled', YES);
			$('#enabled').prop('disabled', true);
		}
	}
}

function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'ESTAFFROLEBRN_MAIN', queryGrid);
}

function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}
function doclearfields(id) {
	switch (id) {
	case 'staffCode':
		if (isEmpty($('#staffCode').val())) {
			$('#staffCode').val(EMPTY_STRING);
			$('#staffCode_error').html(EMPTY_STRING);
			$('#staffCode_desc').html(EMPTY_STRING);
			$('#effectiveDate').val(getCBD());
			$('#effectiveDate_error').html(EMPTY_STRING);
			break;
		}
	case 'effectiveDate':
		if (isEmpty($('#effectiveDate').val())) {
			$('#effectiveDate').val(EMPTY_STRING);
			$('#effectiveDate_error').html(EMPTY_STRING);
			clearNonPKFields();
			break;
		}
	}
}
function clearFields() {
	$('#staffCode').val(EMPTY_STRING);
	$('#staffCode_error').html(EMPTY_STRING);
	$('#staffCode_desc').html(EMPTY_STRING);
	$('#effectiveDate').val(EMPTY_STRING);
	$('#effectiveDate_error').html(EMPTY_STRING);
	clearNonPKFields();
}

function clearNonPKFields() {
	$('#staffRole').val(EMPTY_STRING);
	$('#staffRole_error').html(EMPTY_STRING);
	$('#staffRole_desc').html(EMPTY_STRING);
	$('#branchCode').val(EMPTY_STRING);
	$('#branchCode_error').html(EMPTY_STRING);
	$('#branchCode_desc').html(EMPTY_STRING);
	setCheckbox('enabled', YES);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
}

function loadData() {
	$('#staffCode').val(validator.getValue('STAFF_CODE'));
	$('#staffCode_desc').html(validator.getValue('F1_NAME'));
	$('#effectiveDate').val(validator.getValue('EFF_DATE'));
	$('#staffRole').val(validator.getValue('STAFF_ROLE_CODE'));
	$('#staffRole_desc').html(validator.getValue('F2_DESCRIPTION'));
	$('#branchCode').val(validator.getValue('BRANCH_CODE'));
	$('#branchCode_desc').html(validator.getValue('F3_BRANCH_NAME'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	resetLoading();
}

function view(source, primaryKey) {
	hideParent('emp/qstaffrolebrn', source, primaryKey);
	resetLoading();
}

function doHelp(id) {
	switch (id) {
	case 'staffCode':
		help('COMMON', 'HLP_STAFF_CODE', $('#staffCode').val(), EMPTY_STRING, $('#staffCode'));
		break;
	case 'effectiveDate':
		if ($('#action').val() == MODIFY) {
			if (!isEmpty($('#staffCode').val())) {
				help(CURRENT_PROGRAM_ID, 'HLP_EFF_DATE', $('#effectiveDate').val(), $('#staffCode').val(), $('#effectiveDate'), null, function(gridObj, rowID) {
					$('#effectiveDate').val(gridObj.cells(rowID, 0).getValue());
				});
				break;
			} else {
				setError('staffCode_error', MANDATORY);
			}
		}
		break;
	case 'staffRole':
		help('COMMON', 'HLP_STAFF_ROLE_CODE', $('#staffRole').val(), EMPTY_STRING, $('#staffRole'));
		break;
	case 'branchCode':
		help('COMMON', 'HLP_BRANCH_CODE', $('#branchCode').val(), EMPTY_STRING, $('#branchCode'));
		break;
	}
}

function add() {
	$('#staffCode').focus();
	$('#effectiveDate').val(getCBD());
	setCheckbox('enabled', YES);
	$('#effectiveDate_look').prop('disabled', true);
	$('#enabled').prop('disabled', true);
}

function modify() {
	$('#staffCode').focus();
	$('#effectiveDate_look').prop('disabled', false);
	if ($('#action').val() == MODIFY) {
		$('#branchCode').val(EMPTY_STRING);
		$('#effectiveDate').val(EMPTY_STRING);
		$('#enabled').prop('disabled', false);
	} else {
		$('#enabled').prop('disabled', true);
	}
}

function validate(id) {
	switch (id) {
	case 'staffCode':
		staffCode_val();
		break;
	case 'effectiveDate':
		effectiveDate_val();
		break;
	case 'staffRole':
		staffRole_val();
		break;
	case 'branchCode':
		branchCode_val();
		break;
	case 'enabled':
		enabled_val();
		break;
	case 'remarks':
		remarks_val();
		break;
	}
}

function backtrack(id) {
	switch (id) {
	case 'staffCode':
		setFocusLast('staffCode');
		break;
	case 'effectiveDate':
		setFocusLast('staffCode');
		break;
	case 'staffRole':
		setFocusLast('effectiveDate');
		break;
	case 'branchCode':
		setFocusLast('staffRole');
		break;
	case 'enabled':
		setFocusLast('branchCode');
		break;
	case 'remarks':
		if ($('#action').val() == MODIFY) {
			setFocusLast('enabled');
		} else {
			setFocusLast('branchCode');
		}
		break;
	}
}

function staffCode_val() {
	var value = $('#staffCode').val();
	clearError('staffCode_error');
	if (isEmpty(value)) {
		setError('staffCode_error', MANDATORY);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('STAFF_CODE', value);
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.emp.estaffrolebrnbean');
		validator.setMethod('validateStaffCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('staffCode_error', validator.getValue(ERROR));
			$('#staffCode_desc').html(EMPTY_STRING);
			return false;
		} else {
			$('#staffCode_desc').html(validator.getValue('NAME'));
		}
	}
	setFocusLast('effectiveDate');
	return true;
}

function effectiveDate_val() {
	var value = $('#effectiveDate').val();
	clearError('effectiveDate_error');
	if (isEmpty(value)) {
		$('#effectiveDate').val(getCBD());
		value = $('#effectiveDate').val();
	}
	if (!isValidEffectiveDate($('#effectiveDate').val(), 'effectiveDate_error')) {
		setError('effectiveDate_error', INVALID_EFFECTIVE_DATE);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('STAFF_CODE', $('#staffCode').val());
		validator.setValue('EFF_DATE', value);
		validator.setValue(ACTION, $('#action').val());
		validator.setClass('patterns.config.web.forms.emp.estaffrolebrnbean');
		validator.setMethod('validateEffectiveDate');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('effectiveDate_error', validator.getValue(ERROR));
			return false;
		}
		if ($('#action').val() == MODIFY) {
			PK_VALUE = getEntityCode() + PK_SEPERATOR + $('#staffCode').val() + PK_SEPERATOR + $('#effectiveDate').val();
			if (!loadPKValues(PK_VALUE, 'effectiveDate_error')) {
				return false;
			}
		}
	}
	setFocusLast('staffRole');
	return true;
}

function staffRole_val() {
	if (isEmpty(value)) {
		var value = $('#staffRole').val();
		clearError('staffRole_error');
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('STAFF_ROLE_CODE', value);
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.emp.estaffrolebrnbean');
		validator.setMethod('validateStaffRoleCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('staffRole_error', validator.getValue(ERROR));
			$('#staffRole_desc').html(EMPTY_STRING);
			return false;
		}
		$('#staffRole_desc').html(validator.getValue('DESCRIPTION'));
	}
	setFocusLast('branchCode');
	return true;
}
function branchCode_val() {
	var value = $('#branchCode').val();
	clearError('branchCode_error');
	if (isEmpty(value) && isEmpty($('#staffRole').val())) {
		setError('branchCode_error', STAFF_ROLE_CODE_BRANCH_CODE_EMPTY);
		return false;
	}
	if (isEmpty(value)) {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('ENTITY_CODE', getEntityCode());
		validator.setValue('BRANCH_CODE', value);
		validator.setValue('STAFF_ROLE_CODE', $('#staffRole').val());
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.emp.estaffrolebrnbean');
		validator.setMethod('validatebranchCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('branchCode_error', validator.getValue(ERROR));
			$('#branchCode_desc').html(EMPTY_STRING);
			return false;
		} else {
			$('#branchCode_desc').html(validator.getValue('BRANCH_NAME'));
		}
		
	}
	if ($('#action').val() == MODIFY) {
		setFocusLast('enabled');
	} else {
		setFocusLast('remarks');
	}
	return true;
}

function enabled_val() {
	setFocusLast('remarks');
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', INVALID_REMARKS);
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', INVALID_REMARKS);
			return false;
		}
	}
	if ($('#action').val() == MODIFY) {
		if (isEmpty(value)) {
			setError('remarks_error', MANDATORY);
			return false;
		}
	}
	setFocusOnSubmit();
	return true;
}
