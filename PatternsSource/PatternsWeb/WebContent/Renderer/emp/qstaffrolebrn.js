var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'ESTAFFROLEBRN';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}
function clearFields() {
	$('#staffCode').val(EMPTY_STRING);
	$('#effectiveDate').val(EMPTY_STRING);
	$('#staffRole').val(EMPTY_STRING);
	$('#branchCode').val(EMPTY_STRING);
	setCheckbox('enabled', YES);
	$('#remarks').val(EMPTY_STRING);
}
function loadData() {
	$('#staffCode').val(validator.getValue('STAFF_CODE'));
	$('#staffCode_desc').html(validator.getValue('F1_NAME'));
	$('#effectiveDate').val(validator.getValue('EFF_DATE'));
	$('#staffRole').val(validator.getValue('STAFF_ROLE_CODE'));
	$('#staffRole_desc').html(validator.getValue('F2_DESCRIPTION'));
	$('#branchCode').val(validator.getValue('BRANCH_CODE'));
	$('#branchCode_desc').html(validator.getValue('F3_BRANCH_NAME'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadAuditFields(validator);
}
