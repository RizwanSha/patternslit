var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'ISTAFFLAMHIER';

function init() {
	refreshTBAGrid();
	refreshQueryGrid();
	if (_redisplay) {
		if ($('#action').val() == MODIFY) {
			$('#enabled').prop('disabled', false);
		} else {
			setCheckbox('enabled', YES);
			$('#enabled').prop('disabled', true);
		}
	}
}

function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'ISTAFFLAMHIER_MAIN', queryGrid);
}

function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}
function doclearfields(id) {
	switch (id) {
	case 'staffCode':
		if (isEmpty($('#staffCode').val())) {
			$('#effectiveDate').val(EMPTY_STRING);
			$('#effectiveDate_error').html(EMPTY_STRING);
			$('#effectiveDate').val(getCBD());
			clearNonPKFields();
			break;
		}
	case 'effectiveDate':
		if (isEmpty($('#effectiveDate').val())) {
			clearNonPKFields();
			break;
		}
	}
}
function clearFields() {
	$('#staffCode').val(EMPTY_STRING);
	$('#staffCode_error').html(EMPTY_STRING);
	$('#staffCode_desc').html(EMPTY_STRING);
	$('#effectiveDate').val(EMPTY_STRING);
	$('#effectiveDate_error').html(EMPTY_STRING);
	$('#effectiveDate').val(getCBD());
	clearNonPKFields();
}
function clearNonPKFields() {
	$('#hierarchyCode').val(EMPTY_STRING);
	$('#hierarchyCode_error').html(EMPTY_STRING);
	$('#hierarchyCode_desc').html(EMPTY_STRING);
	$('#allocationRef').val(EMPTY_STRING);
	$('#allocationRef_error').html(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
}

function loadData() {
	$('#staffCode').val(validator.getValue('STAFF_CODE'));
	$('#staffCode_desc').html(validator.getValue('F1_NAME'));
	$('#effectiveDate').val(validator.getValue('EFF_DATE'));
	$('#hierarchyCode').val(validator.getValue('LAM_HIER_CODE'));
	$('#hierarchyCode_desc').html(validator.getValue('F2_DESCRIPTION'));
	$('#allocationRef').val(validator.getValue('ALLOCATION_REF'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	resetLoading();
}

function view(source, primaryKey) {
	hideParent('emp/qstafflamhier', source, primaryKey);
	resetLoading();
}

function doHelp(id) {
	switch (id) {
	case 'staffCode':
		help('COMMON', 'HLP_STAFF_CODE', $('#staffCode').val(), EMPTY_STRING, $('#staffCode'));
		break;
	case 'effectiveDate':
		if (!isEmpty($('#staffCode').val())) {
			help(CURRENT_PROGRAM_ID, 'HLP_EFF_DATE', $('#effectiveDate').val(), $('#staffCode').val(), $('#effectiveDate'));
		} else {
			setError('staffCode_error', MANDATORY);
		}
		break;
	case 'hierarchyCode':
		help('COMMON', 'HLP_LAM_HIER_CODE', $('#hierarchyCode').val(), EMPTY_STRING, $('#hierarchyCode'));
		break;
	}
}
function add() {
	$('#staffCode').focus();
	$('#effectiveDate').val(getCBD());
	setCheckbox('enabled', YES);
	$('#enabled').prop('disabled', true);
}

function modify() {
	$('#staffCode').focus();
	if ($('#action').val() == MODIFY) {
		$('#effectiveDate').val(EMPTY_STRING);
		$('#enabled').prop('disabled', false);
	} else {
		$('#enabled').prop('disabled', true);
	}
}

function validate(id) {
	switch (id) {
	case 'staffCode':
		staffCode_val();
		break;
	case 'effectiveDate':
		effectiveDate_val();
		break;
	case 'hierarchyCode':
		hierarchyCode_val();
		break;
	case 'allocationRef':
		allocationRef_val();
		break;
	case 'enabled':
		enabled_val();
		break;
	case 'remarks':
		remarks_val();
		break;
	}
}

function backtrack(id) {
	switch (id) {
	case 'staffCode':
		setFocusLast('staffCode');
		break;
	case 'effectiveDate':
		setFocusLast('staffCode');
		break;
	case 'hierarchyCode':
		setFocusLast('effectiveDate');
		break;
	case 'allocationRef':
		setFocusLast('hierarchyCode');
		break;
	case 'enabled':
		setFocusLast('allocationRef');
		break;
	case 'remarks':
		if ($('#action').val() == MODIFY) {
			setFocusLast('enabled');
		} else {
			setFocusLast('allocationRef');
		}
		break;
	}
}
function staffCode_val() {
	var value = $('#staffCode').val();
	clearError('staffCode_error');
	if (isEmpty(value)) {
		setError('staffCode_error', MANDATORY);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('ENTITY_CODE', getEntityCode());
		validator.setValue('STAFF_CODE', value);
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.emp.istafflamhierbean');
		validator.setMethod('validateStaffCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('staffCode_error', validator.getValue(ERROR));
			$('#staffCode_desc').html(EMPTY_STRING);
			return false;
		}		
		$('#staffCode_desc').html(validator.getValue('NAME'));
	}
	setFocusLast('effectiveDate');
	return true;
}
function effectiveDate_val() {
	var value = $('#effectiveDate').val();
	clearError('effectiveDate_error');
	if (isEmpty(value)) {
		$('#effectiveDate').val(getCBD());
		setError('effectiveDate_error', MANDATORY);
		return false;
	}
	if (!isValidEffectiveDate(value, 'effectiveDate_error')) {
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('ENTITY_CODE', getEntityCode());
		validator.setValue('STAFF_CODE', $('#staffCode').val());
		validator.setValue('EFF_DATE', value);
		validator.setValue(ACTION, $('#action').val());
		validator.setClass('patterns.config.web.forms.emp.istafflamhierbean');
		validator.setMethod('validateEffectiveDate');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('effectiveDate_error', validator.getValue(ERROR));
			return false;
		} else {
			if ($('#action').val() == MODIFY) {
				PK_VALUE = getEntityCode() + PK_SEPERATOR + $('#staffCode').val() + PK_SEPERATOR + $('#effectiveDate').val();
				if (!loadPKValues(PK_VALUE, 'effectiveDate_error')) {
					return false;
				}
			}
		}
		setFocusLast('hierarchyCode');
		return true;
	}
}
function hierarchyCode_val() {
	var value = $('#hierarchyCode').val();
	clearError('hierarchyCode_error');
	if (isEmpty(value)) {
		setError('hierarchyCode_error', MANDATORY);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('ENTITY_CODE', getEntityCode());
		validator.setValue('LAM_HIER_CODE', value);
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.emp.istafflamhierbean');
		validator.setMethod('validateHierarchyCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('hierarchyCode_error', validator.getValue(ERROR));
			$('#hierarchyCode_desc').html(EMPTY_STRING);
			return false;
		} else {
			$('#hierarchyCode_desc').html(validator.getValue("DESCRIPTION"));
		}
	}
	setFocusLast('allocationRef');
	return true;

}

function allocationRef_val() {
	var value = $('#allocationRef').val();
	clearError('allocationRef_error');
	if (!isEmpty(value)) {
		if (!isValidDescription(value)) {
			setError('allocationRef_error', INVALID_DESCRIPTION);
			return false;
		}
	}
	if ($('#action').val() == MODIFY) {
		setFocusLast('enabled');
	} else {
		setFocusLast('remarks');
	}
	return true;
}

function enabled_val() {
	if ($('#action').val() == MODIFY) {
		setFocusLast('remarks');
	}
	return true;
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', INVALID_REMARKS);
			setFocusLast('remarks');
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', INVALID_REMARKS);
			setFocusLast('remarks');
			return false;
		}
	}
	if ($('#action').val() == MODIFY) {
		if (isEmpty(value)) {
			setError('remarks_error', MANDATORY);
			setFocusLast('remarks');
			return false;
		}
	}
	setFocusOnSubmit();
	return true;
}
