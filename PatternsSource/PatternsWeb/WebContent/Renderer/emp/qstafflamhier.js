var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'ISTAFFLAMHIER';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}
function clearFields() {
	$('#staffCode').val(EMPTY_STRING);
	$('#staffCode_desc').html(EMPTY_STRING);
	$('#effectiveDate').val(EMPTY_STRING);
	$('#hierarchyCode').val(EMPTY_STRING);
	$('#hierarchyCode_desc').html(EMPTY_STRING);
	$('#allocationRef').val(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
}

function loadData() {
	$('#staffCode').val(validator.getValue('STAFF_CODE'));
	$('#staffCode_desc').html(validator.getValue('F1_NAME'));
	$('#effectiveDate').val(validator.getValue('EFF_DATE'));
	$('#hierarchyCode').val(validator.getValue('LAM_HIER_CODE'));
	$('#hierarchyCode_desc').html(validator.getValue('F2_DESCRIPTION'));
	$('#allocationRef').val(validator.getValue('ALLOCATION_REF'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadAuditFields(validator);
}
