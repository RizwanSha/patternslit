<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<web:bundle baseName="patterns.config.web.forms.emp.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/emp/qstaffsignature.js" />
	</web:dependencies>
	<web:loadPK />
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="istaffsignature.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:viewContent id="po_view1">
				<web:dividerBlock>
					<web:sectionBlock width="width-100p" align="left">
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="180px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowOdd>
									<web:column>
										<web:legend key="istaffsignature.staffCode" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:staffCodeDisplay property="staffCode" id="staffCode" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="form.effectivedate" var="common" mandatory="true" />
									</web:column>
									<web:column>
										<type:dateDisplay property="effectiveDate" id="effectiveDate" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="180px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowOdd>
									<web:column>
										<web:legend key="istaffsignature.fileinventorynumber" var="program" />
									</web:column>
									<web:column>
										<type:fileInventoryDisplay id="fileInventoryNumber" property="fileInventoryNumber" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="istaffsignature.imageview" var="program" />
									</web:column>
									<web:column>
										<img id="imageScreenView" style="width: 250px; height: 200px;" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="istaffsignature.referenceno" var="program" />
									</web:column>
									<web:column>
										<type:otherInformation25Display property="referenceNo" id="referenceNo" />
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
					</web:sectionBlock>
					<web:auditDisplay />
				</web:dividerBlock>
			</web:viewContent>
		</web:viewSubPart>
	</web:viewPart>
</web:fragment>
