var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MSTAFFROLE';
function init() {
	refreshTBAGrid();
	refreshQueryGrid();
	if (_redisplay) {
		if ($('#action').val() == ADD) {
			$('#enabled').prop('disabled', true);
			setCheckbox('enabled', YES);
		}
		if (!isEmpty($('#parentStaffRoleCode').val())) {
			$('#roleCategory').val($('#roleType').val());
			$('#roleCategory').prop('disabled', true);
			show('hierarchy_view');
		} else
			hide('hierarchy_view');
	}
}

function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'MSTAFFROLE_MAIN', queryGrid);
}

function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}

function doclearfields(id) {
	switch (id) {
	case 'staffRoleCode':
		if (isEmpty($('#staffRoleCode').val())) {
			clearFields();
			break;
		}
	}
}

function clearFields() {
	$('#staffRoleCode').val(EMPTY_STRING);
	$('#staffRoleCode_error').html(EMPTY_STRING);
	$('#staffRoleCode_desc').html(EMPTY_STRING);
	clearNonPKFields();
}
function clearNonPKFields() {
	$('#description').val(EMPTY_STRING);
	$('#description_error').html(EMPTY_STRING);
	$('#conciseDescription').val(EMPTY_STRING);
	$('#conciseDescription_error').html(EMPTY_STRING);
	$('#parentStaffRoleCode').val(EMPTY_STRING);
	$('#parentStaffRoleCode_error').html(EMPTY_STRING);
	$('#parentStaffRoleCode_desc').html(EMPTY_STRING);
	$('#roleCategory').prop('selectedIndex', 0);
	$('#roleCategory_error').html(EMPTY_STRING);
	setCheckbox('roleInBranch', NO);
	setCheckbox('regionalRole', NO);
	setCheckbox('nationalRole', NO);
	$('#nationalRole_error').html(EMPTY_STRING);
	setCheckbox('enabled', YES);
	$('#enabled_error').html(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
	$('#roleType').val(EMPTY_STRING);
	hide('hierarchy_view');
}

function doHelp(id) {
	switch (id) {
	case 'staffRoleCode':
		help('COMMON', 'HLP_STAFF_ROLE_CODE', $('#staffRoleCode').val(), EMPTY_STRING, $('#staffRoleCode'));
		break;
	case 'parentStaffRoleCode':
		help('COMMON', 'HLP_STAFF_ROLE_CODE', $('#parentStaffRoleCode').val(), EMPTY_STRING, $('#parentStaffRoleCode'));
		break;
	}
}

function viewHierarchy() {
	var pkValue = getEntityCode() + PK_SEPERATOR + $('#parentStaffRoleCode').val();
	window.scroll(0, 0);
	showWindow('PREVIEW_VIEW', getBasePath() + 'Renderer/emp/qviewrolehier.jsp', 'QVIEWROLEHIER', PK + '=' + pkValue + "&" + SOURCE + "=" + MAIN, true, false, false, 750, 400);
	resetLoading();
}

function add() {
	$('#staffRoleCode').focus();
	setCheckbox('enabled', YES);
	$('#enabled').prop('disabled', true);

}
function modify() {
	$('#staffRoleCode').focus();
	if ($('#action').val() == MODIFY)
		$('#enabled').prop('disabled', false);
	else
		$('#enabled').prop('disabled', true);

}
function loadData() {
	$('#staffRoleCode').val(validator.getValue('STAFF_ROLE_CODE'));
	$('#description').val(validator.getValue('DESCRIPTION'));
	$('#conciseDescription').val(validator.getValue('CONCISE_DESCRIPTION'));
	$('#parentStaffRoleCode').val(validator.getValue('PARENT_STAFF_ROLE_CODE'));
	$('#parentStaffRoleCode_desc').html(validator.getValue('F1_DESCRIPTION'));
	$('#roleCategory').val(validator.getValue('ROLE_CATEGORY'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	setCheckbox('roleInBranch', validator.getValue('BRANCH_ROLE'));
	setCheckbox('regionalRole', validator.getValue('REGIONAL_ROLE'));
	setCheckbox('nationalRole', validator.getValue('NATIONAL_ROLE'));
	$('#remarks').val(validator.getValue('REMARKS'));
	if (!isEmpty($('#parentStaffRoleCode').val())) {
		$('#roleType').val(validator.getValue("F1_ROLE_CATEGORY"));
		$('#roleCategory').val($('#roleType').val());
		$('#roleCategory').prop('disabled', true);
		show('hierarchy_view');
	} else
		$('#roleCategory').prop('disabled', false);
	resetLoading();
}
function view(source, primaryKey) {
	hideParent('emp/qstaffrole', source, primaryKey);
	resetLoading();
}

function validate(id) {
	switch (id) {
	case 'staffRoleCode':
		staffRoleCode_val();
		break;
	case 'description':
		description_val();
		break;
	case 'conciseDescription':
		conciseDescription_val();
		break;
	case 'parentStaffRoleCode':
		parentStaffRoleCode_val();
		break;
	case 'roleCategory':
		roleCategory_val();
		break;
	case 'roleInBranch':
		checkAtleastOne_val(true, 'roleInBranch', 'regionalRole', 'nationalRole');
		break;
	case 'regionalRole':
		checkAtleastOne_val(true, 'regionalRole', 'nationalRole', 'roleInBranch');
		break;
	case 'nationalRole':
		checkAtleastOne_val(true, 'nationalRole', 'regionalRole', 'roleInBranch');
		check();
		break;
	case 'enabled':
		enabled_val();
		break;
	case 'remarks':
		remarks_val();
		break;
	}
}

function checkclick(id) {
	switch (id) {
	case 'roleInBranch':
		checkAtleastOne_val(true, 'roleInBranch', 'regionalRole', 'nationalRole');
		break;
	case 'regionalRole':
		checkAtleastOne_val(true, 'regionalRole', 'nationalRole', 'roleInBranch');
		break;
	case 'nationalRole':
		checkAtleastOne_val(true, 'nationalRole', 'regionalRole', 'roleInBranch');
		check();
		break;
	}
}

function backtrack(id) {
	switch (id) {
	case 'staffRoleCode':
		setFocusLast('staffRoleCode');
		break;
	case 'description':
		setFocusLast('staffRoleCode');
		break;
	case 'conciseDescription':
		setFocusLast('description');
		break;
	case 'parentStaffRoleCode':
		setFocusLast('conciseDescription');
		break;
	case 'roleCategory':
		setFocusLast('parentStaffRoleCode');
		break;
	case 'roleInBranch':
		
		if (isEmpty($('#parentStaffRoleCode').val()))
			setFocusLast('roleCategory');
		else
			setFocusLast('parentStaffRoleCode');
		break;
	case 'regionalRole':
		if (!$('#roleInBranch').prop('disabled'))
			setFocusLast('roleInBranch');
		else if (isEmpty($('#parentStaffRoleCode').val()))
			setFocusLast('roleCategory');
		else
			setFocusLast('parentStaffRoleCode');
		break;
	case 'nationalRole':
		if (!$('#regionalRole').prop('disabled'))
			setFocusLast('regionalRole');
		else if (isEmpty($('#parentStaffRoleCode').val()))
			setFocusLast('roleCategory');
		else
			setFocusLast('parentStaffRoleCode');
		break;
	case 'enabled':
		setFocusOnEnabled();
		break;
	case 'remarks':
		if ($('#action').val() == MODIFY) {
			setFocusLast('enabled');
		} else {
			setFocusOnEnabled();
		}
		break;
	}
}

function setFocusOnEnabled() {
	if (!$('#nationalRole').prop('disabled'))
		setFocusLast('nationalRole');
	else if (!$('#regionalRole').prop('disabled'))
		setFocusLast('regionalRole');
	else if (!$('#roleInBranch').prop('disabled'))
		setFocusLast('roleInBranch');
	else if (!$('#roleCategory').prop('disabled'))
		setFocusLast('parentStaffRoleCode');
	return true;
}

function staffRoleCode_val() {
	var value = $('#staffRoleCode').val();
	clearError('staffRoleCode_error');
	if (isEmpty(value)) {
		setError('staffRoleCode_error', MANDATORY);
		return false;
	} else if ($('#action').val() == ADD && !isValidCode(value)) {
		setError('staffRoleCode_error', INVALID_FORMAT);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('STAFF_ROLE_CODE', value);
	validator.setValue(ACTION, $('#action').val());
	validator.setClass('patterns.config.web.forms.emp.mstaffrolebean');
	validator.setMethod('validateStaffRoleCode');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('staffRoleCode_error', validator.getValue(ERROR));
		$('#staffRoleCode_desc').html(EMPTY_STRING);
		return false;
	} else {
		if ($('#action').val() == MODIFY) {
			PK_VALUE = getEntityCode() + PK_SEPERATOR + $('#staffRoleCode').val();
			if (!loadPKValues(PK_VALUE, 'staffRoleCode_error')) {
				return false;
			}
		}
	}
	setFocusLast('description');
	return true;
}

function description_val() {
	var value = $('#description').val();
	clearError('description_error');
	if (isEmpty(value)) {
		setError('description_error', MANDATORY);
		return false;
	}
	if (!isValidDescription(value)) {
		setError('description_error', INVALID_DESCRIPTION);
		return false;
	}
	setFocusLast('conciseDescription');
	return true;
}

function conciseDescription_val() {
	var value = $('#conciseDescription').val();
	clearError('conciseDescription_error');
	if (isEmpty(value)) {
		setError('conciseDescription_error', MANDATORY);
		return false;
	}
	if (!isValidConciseDescription(value)) {
		setError('conciseDescription_error', INVALID_CONCISE_DESCRIPTION);
		return false;
	}
	setFocusLast('parentStaffRoleCode');
	return true;
}

function parentStaffRoleCode_val() {
	var value = $('#parentStaffRoleCode').val();
	clearError('parentStaffRoleCode_error');
	if (!isEmpty(value)) {
		if (value == $('#staffRoleCode').val()) {
			setError('parentStaffRoleCode_error', PBS_PARENT_CANT_SAME);
			return false;
		}
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('ROLE_CODE', $('#staffRoleCode').val());
		validator.setValue('STAFF_ROLE_CODE', value);
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.emp.mstaffrolebean');
		validator.setMethod('validateParentStaffRoleCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('parentStaffRoleCode_error', validator.getValue(ERROR));
			$('#parentStaffRoleCode_desc').html(EMPTY_STRING);
			return false;
		}
		$('#parentStaffRoleCode_desc').html(validator.getValue("DESCRIPTION"));
		$('#roleType').val(validator.getValue("ROLE_CATEGORY"));
		$('#roleCategory').val($('#roleType').val());
		$('#roleCategory').prop('disabled', true);
		show('hierarchy_view');
		if ($('#action').val() == ADD) {
			setFocusLast('roleInBranch');
		} else if (!$('#roleInBranch').prop('disabled'))
			setFocusLast('roleInBranch');
		else if (!$('#regionalRole').prop('disabled'))
			setFocusLast('nationaleRole');
		else if (!$('#regionalRole').prop('disabled'))
			setFocusLast('enabled');
	} else {
		hide('hierarchy_view');
		$('#roleCategory').prop('disabled', false);
		$('#roleCategory').prop('selectedIndex', 0);
		setFocusLast('roleCategory');
	}
	return true;
}

function roleCategory_val() {
	var value = $('#roleCategory').val();
	clearError('roleCategory_error');
	if (isEmpty(value)) {
		setError('roleCategory_error', MANDATORY);
		return false;
	}
	if (!$('#roleCategory').prop('disabled'))
		setFocusLast('roleInBranch');
	else if (!$('#roleInBranch').prop('disabled'))
		setFocusLast('regionalRole');
	else if (!$('#regionalRole').prop('disabled'))
		setFocusLast('nationaleRole');
	else if ($('#action').val() == MODIFY)
		setFocusLast('enabled');
	else
		setFocusLast('remarks');
	return true;
}

function checkAtleastOne_val(valMode, id, nextVal, lastVal) {
	clearError(id + '_error');
	clearError(nextVal + '_error');
	clearError(lastVal + '_error');
	if ($('#' + id).is(':checked')) {
		$('#' + nextVal).prop('disabled', true);
		setCheckbox(nextVal, NO);
		$('#' + lastVal).prop('disabled', true);
		setCheckbox(lastVal, NO);
		if (valMode)
			setFocusLast('remarks');
	} else {
		$('#' + nextVal).prop('disabled', false);
		$('#' + lastVal).prop('disabled', false);
		if (valMode)
			setFocusLast(nextVal);
	}
	return true;
}

function enabled_val() {
	clearError('enabled_error');
	if ($('#action').val() == MODIFY && !$('#enabled').is(':checked')) {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('STAFF_ROLE_CODE', $('#staffRoleCode').val());
		validator.setClass('patterns.config.web.forms.emp.mstaffrolebean');
		validator.setMethod('validateRoleDisabled');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('enabled_error', validator.getValue(ERROR));
			return false;
		}
	}
	setFocusLast('remarks');
	return true;
}

function check() {
	var CheckBoxCount = 0;
	if ($('#roleInBranch').is(':checked'))
		CheckBoxCount++;
	if ($('#regionalRole').is('	:checked'))
		CheckBoxCount++;
	if ($('#nationalRole').is(':checked'))
		CheckBoxCount++;
	if (CheckBoxCount > 1) {
		setError('nationalRole_error', ONLY_ONE_SHOULD_BE_CHECKED);
		return false;
	}
	if (CheckBoxCount == 0) {
		setError('nationalRole_error', ATLEAST_ONE_SHOULD_BE_CHECKED);
		return false;
	}
	return true;
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', INVALID_REMARKS);
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', INVALID_REMARKS);
			return false;
		}
	}
	if ($('#action').val() == MODIFY) {
		if (isEmpty(value)) {
			setError('remarks_error', MANDATORY);
			return false;
		}
	}
	setFocusOnSubmit();
}
