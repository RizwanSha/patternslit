var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MLAMHIER';
function init() {

	refreshTBAGrid();
	refreshQueryGrid();
	if (_redisplay) {
		if ($('#action').val() == ADD) {
			$('#enabled').prop('disabled', true);
			setCheckbox('enabled', YES);
		}
	}
}
function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'MLAMHIER_MAIN', queryGrid);
}

function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}

function clearFields() {
	$('#hierarchyCode').val(EMPTY_STRING);
	$('#hierarchyCode_error').html(EMPTY_STRING);
	$('#hierarchyCode_desc').html(EMPTY_STRING);
	clearNonPKFields();
}

function clearNonPKFields() {
	$('#name').val(EMPTY_STRING);
	$('#name_error').html(EMPTY_STRING);
	$('#name_desc').html(EMPTY_STRING);
	$('#shortName').val(EMPTY_STRING);
	$('#shortName_error').html(EMPTY_STRING);
	$('#shortName_desc').html(EMPTY_STRING);
	setCheckbox('enabled', YES);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
}
function add() {
	$('#hierarchyCode').focus();
	setCheckbox('enabled', YES);
	$('#enabled').prop('disabled', true);
}
function modify() {
	$('#hierarchyCode').focus();
	if ($('#action').val() == MODIFY) {
		$('#enabled').prop('disabled', false);
	} else {
		$('#enabled').prop('disabled', true);
	}
}

function change(id) {
}

function loadData() {
	$('#hierarchyCode').val(validator.getValue('LAM_HIER_CODE'));
	$('#name').val(validator.getValue('DESCRIPTION'));
	$('#shortName').val(validator.getValue('CONCISE_DESCRIPTION'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
}
function view(source, primaryKey) {
	hideParent('emp/qlamhier', source, primaryKey);
	resetLoading();

}

function doHelp(id) {
	switch (id) {
	case 'hierarchyCode':
		help('COMMON', 'HLP_LAM_HIER_CODE', $('#hierarchyCode').val(), EMPTY_STRING, $('#hierarchyCode'));
		break;
	}

}

function doclearfields(id) {
	switch (id) {
	case 'hierarchyCode':
		if (isEmpty($('#hierarchyCode').val())) {
			clearFields();
			break;
		}
	}

}

function validate(id) {
	switch (id) {
	case 'hierarchyCode':
		hierarchyCode_val();
		break;
	case 'name':
		name_val();
		break;
	case 'shortName':
		shortName_val();
		break;
	case 'enabled':
		enabled_val();
		break;
	case 'remarks':
		remarks_val();
		break;
	}

}

function backtrack(id) {
	switch (id) {
	case 'hierarchyCode':
		setFocusLast('hierarchyCode');
		break;
	case 'name':
		setFocusLast('hierarchyCode');
		break;
	case 'shortName':
		setFocusLast('name');
		break;
	case 'enabled':
		setFocusLast('shortName');
		break;
	case 'remarks':
		if ($('#action').val() == MODIFY) {
			setFocusLast('enabled');
		} else {
			setFocusLast('shortName');
		}
		break;
	}

}

function hierarchyCode_val() {
	var value = $('#hierarchyCode').val();
	clearError('hierarchyCode_error');
	$('#hierarchyCode_desc').html(EMPTY_STRING);
	if (isEmpty(value)) {
		setError('hierarchyCode_error', MANDATORY);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('LAM_HIER_CODE', $('#hierarchyCode').val());
		validator.setValue(ACTION, $('#action').val());
		validator.setClass('patterns.config.web.forms.emp.mlamhierbean');
		validator.setMethod('validateLAMHierarchyCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('hierarchyCode_error', validator.getValue(ERROR));
			$('#hierarchyCode_desc').html(EMPTY_STRING);
			return false;
		} else {
			$('#hierarchyCode_desc').html(validator.getValue('DESCRIPTION'));
		}
		if ($('#action').val() == MODIFY) {
			PK_VALUE = getEntityCode() + PK_SEPERATOR + $('#hierarchyCode').val();
			if (!loadPKValues(PK_VALUE, 'hierarchyCode_error')) {
				return false;
			}
		}
	}
	setFocusLast('name');
	return true;
}

function name_val() {
	var value = $('#name').val();
	clearError('name_error');
	if (isEmpty(value)) {
		setError('name_error', MANDATORY);
		return false;
	}
	if (!isValidDescription(value)) {
		setError('name_error', INVALID_DESCRIPTION);
		return false;
	}
	setFocusLast('shortName');
	return true;
}

function shortName_val() {
	var value = $('#shortName').val();
	clearError('shortName_error');
	if (isEmpty(value)) {
		setError('shortName_error', MANDATORY);
		return false;
	}
	if (!isValidConciseDescription(value)) {
		setError('shortName_error', INVALID_CONCISE_DESCRIPTION);
		return false;
	}
	if ($('#action').val() == MODIFY) {
		setFocusLast('enabled');
	} else {
		setFocusLast('remarks');
	}
	return true;
}

function enabled_val() {
	if ($('#action').val() == MODIFY) {
		setFocusLast('remarks');
	}
}
function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', INVALID_REMARKS);
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', INVALID_REMARKS);
			return false;
		}
	}
	if ($('#action').val() == MODIFY) {
		if (isEmpty(value)) {
			setError('remarks_error', MANDATORY);
			return false;
		}
	}
	setFocusOnSubmit();
	return true;

}
