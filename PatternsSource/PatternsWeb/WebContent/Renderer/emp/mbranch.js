var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MBRANCH';
function init() {
	refreshTBAGrid();
	refreshQueryGrid();
	if (_redisplay) {
		if ($('#action').val() == ADD) {
			$('#enabled').prop('disabled', true);
			$('#enabled').prop('checked', true);
		}
		stateCode_val();
		regionCode_val();
		setFocusLast('branchCode');
	}
}
function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'MBRANCH_MAIN', queryGrid);
}
function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}
function clearFields() {
	$('#branchCode').val(EMPTY_STRING);
	$('#branchCode_error').html(EMPTY_STRING);
	$('#branchCode_desc').html(EMPTY_STRING);
	$('#branchCode').focus();
	clearNonPKFields();
}

function clearNonPKFields() {
	$('#branchName').val(EMPTY_STRING);
	$('#branchName_error').html(EMPTY_STRING);
	$('#branchAdd').val(EMPTY_STRING);
	$('#branchAdd_error').html(EMPTY_STRING);
	$('#pinCode').val(EMPTY_STRING);
	$('#pinCode_error').html(EMPTY_STRING);
	$('#pinCode_desc').html(EMPTY_STRING);
	$('#stateCode').val(EMPTY_STRING);
	$('#stateCode_error').html(EMPTY_STRING);
	$('#stateCode_desc').html(EMPTY_STRING);
	$('#regionCode').val(EMPTY_STRING);
	$('#regionCode_error').html(EMPTY_STRING);
	$('#regionCode_desc').html(EMPTY_STRING);
	$('#areaCode').val(EMPTY_STRING);
	$('#areaCode_error').html(EMPTY_STRING);
	$('#areaCode_desc').html(EMPTY_STRING);	
	$('#vatTin').val(EMPTY_STRING);
	$('#vatTin_error').html(EMPTY_STRING);
	$('#cstTin').val(EMPTY_STRING);
	$('#cstTin_error').html(EMPTY_STRING);
	setCheckbox('enabled', YES);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
}
function add() {
	setCheckbox('enabled', YES);
	$('#enabled').prop('disabled', true);
}
function modify() {
	$('#branchCode').focus();
	$('#enabled').prop('disabled', false);
}
function loadData() {
	$('#branchCode').val(validator.getValue('BRANCH_CODE'));
	$('#branchName').val(validator.getValue('BRANCH_NAME'));
	$('#branchAdd').val(validator.getValue('ADDRESS'));
	$('#pinCode').val(validator.getValue('PIN_CODE'));
	$('#stateCode').val(validator.getValue('STATE_CODE'));
	$('#stateCode_desc').html(validator.getValue('F1_DESCRIPTION'));
	$('#regionCode').val(validator.getValue('REGION_CODE'));
	$('#regionCode_desc').html(validator.getValue('F2_DESCRIPTION'));
	$('#areaCode').val(validator.getValue('AREA_CODE'));
	$('#areaCode_desc').html(validator.getValue('F3_DESCRIPTION'));
	$('#vatTin').val(validator.getValue('VAT_TIN_NO'));
	$('#cstTin').val(validator.getValue('CST_TIN_NO'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	setFocusLast('costCode');
	resetLoading();
}
function view(source, primaryKey) {
	hideParent('emp/qbranch', source, primaryKey);
	resetLoading();
}

function doHelp(id) {
	switch (id) {
	case 'branchCode':
		help('COMMON', 'HLP_BRANCH_CODE', $('#branchCode').val(), EMPTY_STRING, $('#branchCode'));
		break;
	case 'stateCode':
		help('COMMON', 'HLP_STATE_CODE', $('#stateCode').val(), EMPTY_STRING, $('#stateCode'));
		break;
	case 'regionCode':
		help('COMMON', 'HLP_REGION_CODE', $('#regionCode').val(), EMPTY_STRING, $('#regionCode'));
		break;
	case 'areaCode':
		help('COMMON', 'HLP_AREA_CODE', $('#areaCode').val(), $('#regionCode').val(), $('#areaCode'));
		break;
	}
}

function doclearfields(id) {
	switch (id) {
	case 'branchCode':
		if (isEmpty($('#branchCode').val())) {
			clearNonPKFields();
			break;
		}
	}
}

function validate(id) {
	switch (id) {
	case 'branchCode':
		branchCode_val();
		break;
	case 'branchName':
		branchName_val();
		break;
	case 'branchAdd':
		branchAdd_val();
		break;
	case 'pinCode':
		pinCode_val();
		break;
	case 'stateCode':
		stateCode_val();
		break;
	case 'regionCode':
		regionCode_val();
		break;
	case 'areaCode':
		areaCode_val();
		break;
	case 'vatTin':
		vatTin_val();
		break;
	case 'cstTin':
		cstTin_val();
		break;
	case 'enabled':
		enabled_val();
		break;
	case 'remarks':
		remarks_val();
		break;

	}
}

function backtrack(id) {
	switch (id) {
	case 'branchCode':
		setFocusLast('branchCode');
		break;
	case 'branchName':
		setFocusLast('branchCode');
		break;
	case 'branchAdd':
		setFocusLast('branchName');
		break;
	case 'pinCode':
		setFocusLast('branchAdd');
		break;
	case 'stateCode':
		setFocusLast('pinCode');
		break;
	case 'regionCode':
		setFocusLast('stateCode');
		break;
	case 'areaCode':
		setFocusLast('regionCode');
		break;
	case 'vatTin':
		setFocusLast('areaCode');
		break;
	case 'cstTin':
		setFocusLast('vatTin');
		break;
	case 'enabled':
		setFocusLast('cstTin');
		break;
	case 'remarks':
		if ($('#action').val() == MODIFY) {
			setFocusLast('enabled');
		} else {
			setFocusLast('cstTin');
		}
		break;
	}
}

function branchCode_val() {
	var value = $('#branchCode').val();
	clearError('branchCode_error');
	if (isEmpty(value)) {
		setError('branchCode_error', HMS_MANDATORY);
		return false;
	} else if ($('#action').val() == ADD && !isValidCode(value)) {
		setError('branchCode_error', HMS_INVALID_FORMAT);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('BRANCH_CODE', $('#branchCode').val());
		validator.setValue(ACTION, $('#action').val());
		validator.setClass('patterns.config.web.forms.emp.mbranchbean');
		validator.setMethod('validateBranchCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('branchCode_error', validator.getValue(ERROR));
			return false;
		} else {
			if ($('#action').val() == MODIFY) {
				PK_VALUE = getEntityCode() + PK_SEPERATOR + $('#branchCode').val();
				if (!loadPKValues(PK_VALUE, 'branchCode_error')) {
					return false;
				}
			}
		}
	}
	setFocusLast('branchName');
	return true;
}

function branchName_val() {
	var value = $('#branchName').val();
	clearError('branchName_error');
	if (!isValidDescription(value)) {
		setError('branchName_error', INVALID_DESCRIPTION);
		return false;
	} else {
		setFocusLast('branchAdd');
	}
	return true;
}

function branchAdd_val() {
	var value = $('#branchName').val();
	clearError('branchName_error');
	if (isEmpty(value)) {
		setError('branchName_error', HMS_MANDATORY);
		return false;
	}
	if (!isValidRemarks(value)) {
		setError('branchName_error', HMS_INVALID_REMARKS);
		return false;
	}
	setFocusLast('pinCode');
	return true;
}

function pinCode_val() {
	var value = $('#pinCode').val();
	clearError('pinCode_error');
	if (isEmpty(value)) {
		setError('pinCode_error', HMS_MANDATORY);
		return false;
	}
	if (!isNumeric(value)) {
		setError('pinCode_error', INVALID_NUMBER);
		return false;
	}
	if (!isLength(value, 6)) {
		setError('pinCode_error', PINCODE_LENGTH);
		return false;
	}

	setFocusLast('stateCode');
	return true;
}

function stateCode_val() {
	var value = $('#stateCode').val();
	clearError('stateCode_error');
	if (isEmpty(value)) {
		setError('stateCode_error', HMS_MANDATORY);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('STATE_CODE', value); //
	validator.setValue(ACTION, USAGE);
	validator.setClass('patterns.config.web.forms.emp.mbranchbean');
	validator.setMethod('validateStateCode');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('stateCode_error', validator.getValue(ERROR));
		$('#stateCode_desc').html(EMPTY_STRING);
		return false;
	} else {
		$('#stateCode_desc').html(validator.getValue("DESCRIPTION"));
	}

	setFocusLast('regionCode');
	return true;
}

function regionCode_val() {
	var value = $('#regionCode').val();
	clearError('regionCode_error');
	if (isEmpty(value)) {
		setError('regionCode_error', HMS_MANDATORY);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('REGION_CODE', value);
	validator.setValue(ACTION, USAGE);
	validator.setClass('patterns.config.web.forms.emp.mbranchbean');
	validator.setMethod('validateRegionCode');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('regionCode_error', validator.getValue(ERROR));
		$('#regionCode_desc').html(EMPTY_STRING);
		return false;
	} else {
		$('#regionCode_desc').html(validator.getValue("DESCRIPTION"));
	}
	setFocusLast('areaCode');
	return true;
}

function areaCode_val() {
	var value = $('#areaCode').val();
	clearError('areaCode_error');
	if (isEmpty(value)) {
		setError('areaCode_error', HMS_MANDATORY);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('REGION_CODE', $('#regionCode').val());
	validator.setValue('AREA_CODE', value);
	validator.setValue(ACTION, USAGE);
	validator.setClass('patterns.config.web.forms.emp.mbranchbean');
	validator.setMethod('validateAreaCode');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('areaCode_error', validator.getValue(ERROR));
		$('#areaCode_desc').html(EMPTY_STRING);
		return false;
	} else {
		$('#areaCode_desc').html(validator.getValue("DESCRIPTION"));
	}

	setFocusLast('vatTin');
	return true;
}

function vatTin_val() {
	var value = $('#vatTin').val();
	clearError('vatTin_error');
	if (isEmpty(value)) {
		setError('vatTin_error', HMS_MANDATORY);
		return false;
	} else {
		setFocusLast('cstTin');
	}
	return true;
}

function cstTin_val() {
	var value = $('#cstTin').val();
	clearError('cstTin_error');
	if (isEmpty(value)) {
		setError('cstTin_error', HMS_MANDATORY);
		return false;
	} else {
		if ($('#action').val() == ADD) {
			setFocusLast('remarks');
		} else {
			setFocusLast('enabled');
		}
	}
	return true;
}

function enabled_val() {
	if ($('#action').val() == MODIFY) {
		setFocusLast('remarks');
	}
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', HMS_INVALID_REMARKS);
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', HMS_INVALID_REMARKS);
			return false;
		}
	}
	if ($('#action').val() == MODIFY) {
		if (isEmpty(value)) {
			setError('remarks_error', HMS_MANDATORY);
			return false;
		}

	}
	setFocusOnSubmit();
	return true;
}
