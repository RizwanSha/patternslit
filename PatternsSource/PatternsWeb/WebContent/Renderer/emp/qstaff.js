var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MSTAFF';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}

function clearFields() {
	$('#staffEmpCode').val(EMPTY_STRING);
	$('#staffEmpCode_error').html(EMPTY_STRING);
	$('#staffEmpCode_desc').html(EMPTY_STRING);
}

function loadData() {
	$('#staffEmpCode').val(validator.getValue('STAFF_CODE'));
	$('#name').val(validator.getValue('NAME'));
	$('#dateOfJoining').val(validator.getValue('DATE_OF_JOINING'));
	$('#emailId').val(validator.getValue('EMAIL_ID'));
	$('#mobileNumber').val(validator.getValue('MOBILE_NO'));
	$('#staffRole').val(validator.getValue('STAFF_ROLE_CODE'));
	$('#staffRole_desc').html(validator.getValue('F4_DESCRIPTION'));
	$('#branchCode').val(validator.getValue('BRANCH_CODE'));
	$('#branchCode_desc').html(validator.getValue('F1_BRANCH_NAME'));
	$('#regionCode').val(validator.getValue('REGION_CODE'));
	$('#regionCode_desc').html(validator.getValue('F2_DESCRIPTION'));
	$('#areaCode').val(validator.getValue('AREA_CODE'));
	$('#areaCode_desc').html(validator.getValue('F3_DESCRIPTION'));
	$('#reportingToStaffId').val(validator.getValue('REPORTING_STAFF_CODE'));
	if (!isEmpty($('#reportingToStaffId').val()))
		$('#reportingToStaffId_desc').html(validator.getValue('F5_NAME'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadAuditFields(validator);
}
