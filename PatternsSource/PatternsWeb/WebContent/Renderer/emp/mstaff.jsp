<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.emp.Messages"
	var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/emp/mstaff.js" />
	</web:dependencies>
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="mstaff.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/emp/mstaff" id="mstaff" method="POST">
				<web:dividerBlock>
					<type:optionBox add="true" modify="true" view="true" />
				</web:dividerBlock>
				<web:viewContent id="po_view1" styleClass="hidden">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="mstaff.staffempcode" var="program"
												mandatory="true" />
										</web:column>
										<web:column>
											<type:staffCode property="staffEmpCode" id="staffEmpCode" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="mstaff.name" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:description property="name" id="name" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="mstaff.dateofjoining" var="program"
												mandatory="true" />
										</web:column>
										<web:column>
											<type:date property="dateOfJoining" id="dateOfJoining" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="mstaff.emailid" var="program"
												mandatory="true" />
										</web:column>
										<web:column>
											<type:email property="emailId" id="emailId" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="mstaff.mobilenumber" var="program"
												mandatory="true" />
										</web:column>
										<web:column>
											<type:mobile property="mobileNumber" id="mobileNumber" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:subProgramTitle var="program" key="mstaff.section"
								collapsable="clickheretoviewthedetails" status="close" />
							<web:viewContent id="details_linkId" styleClass="hidden">
								<web:section>
									<web:table>
										<web:rowOdd>
											<web:column>
												<web:anchorMessage var="program" key="mstaff.clickhere"
													onclick="openAddinfo()"
													style="text-decoration:underline;cursor:pointer" />
											</web:column>
										</web:rowOdd>
									</web:table>
								</web:section>
							</web:viewContent>
							<web:viewContent id="clickheretoviewthedetails"
								styleClass="hidden">
								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="200px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowEven>
											<web:column>
												<web:legend key="mstaff.staffrole" var="program" />
											</web:column>
											<web:column>
												<type:staffRoleDisplay property="staffRole" id="staffRole" />
											</web:column>
										</web:rowEven>
										<web:rowOdd>
											<web:column>
												<web:legend key="mstaff.branchcode" var="program" />
											</web:column>
											<web:column>
												<type:branchCodeDisplay property="branchCode"
													id="branchCode" />
											</web:column>
										</web:rowOdd>
										<web:rowEven>
											<web:column>
												<web:legend key="mstaff.regioncode" var="program" />
											</web:column>
											<web:column>
												<type:regionCodeDisplay property="regionCode"
													id="regionCode" />
											</web:column>
										</web:rowEven>
										<web:rowOdd>
											<web:column>
												<web:legend key="mstaff.areacode" var="program" />
											</web:column>
											<web:column>
												<type:areaCodeDisplay property="areaCode" id="areaCode" />
											</web:column>
										</web:rowOdd>
										<web:rowEven>
											<web:column>
												<web:legend key="mstaff.reportingtostaffid" var="program" />
											</web:column>
											<web:column>
												<type:staffCodeDisplay property="reportingToStaffId"
													id="reportingToStaffId" />
											</web:column>
										</web:rowEven>
									</web:table>
								</web:section>
							</web:viewContent>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="form.remarks" var="common" />
										</web:column>
										<web:column>
											<type:remarks property="remarks" id="remarks" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column span="2">
											<web:submitReset />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
						</web:sectionBlock>
					</web:dividerBlock>
				</web:viewContent>
				<web:element property="command" />
				<web:element property="action" />
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
	<web:queryGrid />
	<web:TBAGrid />
	<web:contextSearch />
</web:fragment>
