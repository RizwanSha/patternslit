var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'ECUSTOMERADDRESS';
function init() {
	refreshTBAGrid();
	refreshQueryGrid();
	if (!isEmpty($('#addresspk').val())) {
		var pkValue = $('#addresspk').val();
		var pk = pkValue.split('|');
		hide('calledQueryGrid');
		$('#customerID').val(pk[1]);
		if (!isEmpty(pk[2])) {
			$('#addressSerial').val(pk[2]);
			domodify();
			$('#officeAddrIdentifier').focus();
			$('#back').remove();
			return true;
		} else {
			doadd();
			$('#officeAddrIdentifier').focus();
			$('#back').remove();
			return true;
		}
	}
	if (_redisplay) {
		if ($('#action').val() == ADD) {
			$('#removeAddress').prop('disabled', true);
			setCheckbox('removeAddress', NO);
			$('#addressSerial').prop('readonly', true);
			$('#addressSerial_look').prop('disabled', true);
		}
	}
}

function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'ECUSTOMERADDRESS_MAIN', queryGrid);
}

function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}

function doclearfields(id) {
	switch (id) {
	case 'customerID':
		if (isEmpty($('#customerID').val())) {
			clearFields();
			break;
		}
	case 'addressSerial':
		if (isEmpty($('#addressSerial').val())) {
			$('#addressSerial_error').html(EMPTY_STRING);
			clearNonPKFields();
			break;
		}
	}
}

function clearFields() {
	$('#customerID').val(EMPTY_STRING);
	$('#customerID_error').html(EMPTY_STRING);
	$('#customerID_desc').html(EMPTY_STRING);
	$('#addressSerial').val(EMPTY_STRING);
	$('#addressSerial_error').html(EMPTY_STRING);
	clearNonPKFields();
}
function clearNonPKFields() {
	$('#officeAddrIdentifier').val(EMPTY_STRING);
	$('#officeAddrIdentifier_error').html(EMPTY_STRING);
	$('#address').val(EMPTY_STRING);
	$('#address_error').html(EMPTY_STRING);
	$('#pinCode').val(EMPTY_STRING);
	$('#pinCode_error').html(EMPTY_STRING);
	$('#state').val(EMPTY_STRING);
	$('#state_desc').html(EMPTY_STRING);
	$('#state_error').html(EMPTY_STRING);
	$('#tanNo').val(EMPTY_STRING);
	$('#tanNo_error').html(EMPTY_STRING);
	setCheckbox('removeAddress', NO);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
	if (!isEmpty($('#addresspk').val())) {
		$('#customerID').val($('#addresspk').val().split(PK_SEPERATOR)[1]);
		$('#back').add();
	} else
		$('#back').remove();
	if (!isEmpty($('#redirectPageId').val())) {
		show('redirect_view');
	} else {
		hide('redirect_view');
	}
}

function doHelp(id) {
	switch (id) {
	case 'customerID':
		help('COMMON', 'HLP_CUSTOMER_ID', $('#customerID').val(), EMPTY_STRING,
				$('#customerID'));
		break;
	case 'addressSerial':
		if ($('#action').val() == MODIFY
				|| $('#rectify').val() == COLUMN_ENABLE)
			help('COMMON', 'HLP_CUST_ADDR_SL', $('#addressSerial').val(), $(
					'#customerID').val(), $('#addressSerial'));
		break;
	case 'state':
		help('COMMON', 'HLP_STATE_CODE', $('#state').val(), EMPTY_STRING,
				$('#state'));
		break;
	}
}

function add() {
	$('#customerID').focus();
	$('#addressSerial').prop('readonly', true);
	$('#addressSerial_pic').prop('disabled', true);
	$('#removeAddress').prop('disabled', true);
	setCheckbox('removeAddress', NO);
	if ($('#addresspk').val() != EMPTY_STRING
			&& $('#addresspk').val().split(PK_SEPERATOR).length == 3) {
		domodify();
		PK_VALUE = $('#addresspk').val();
		if (!loadPKValues(PK_VALUE, 'addressSerial_error')) {
			return false;
		}
	}

}
function modify() {
	$('#customerID').focus();
	$('#addressSerial').prop('readonly', false);
	$('#addressSerial_pic').prop('disabled', false);
	if ($('#action').val() == MODIFY)
		$('#removeAddress').prop('disabled', false);
	else
		$('#removeAddress').prop('disabled', true);

}
function loadData() {
	$('#customerID').val(validator.getValue('CUSTOMER_ID'));
	$('#customerID_desc').html(validator.getValue('F1_CUSTOMER_NAME'));
	$('#addressSerial').val(validator.getValue('ADDR_SL'));
	$('#officeAddrIdentifier').val(validator.getValue('OFFICE_ADDRESS_ID'));
	$('#address').val(validator.getValue('ADDRESS'));
	$('#pinCode').val(validator.getValue('PIN_CODE'));
	$('#state').val(validator.getValue('STATE_CODE'));
	$('#state_desc').html(validator.getValue('F2_DESCRIPTION'));
	$('#tanNo').val(validator.getValue('TAN_NO'));
	setCheckbox('removeAddress', validator.getValue('DETELED_FLAG'));
	$('#remarks').val(validator.getValue('REMARKS'));
	resetLoading();
}
function view(source, primaryKey) {
	hideParent('reg/qcustomeraddress', source, primaryKey);
	resetLoading();
}

function validate(id) {
	switch (id) {
	case 'customerID':
		customerID_val();
		break;
	case 'addressSerial':
		addressSerial_val();
		break;
	case 'officeAddrIdentifier':
		officeAddrIdentifier_val();
		break;
	case 'address':
		address_val();
		break;
	case 'pinCode':
		pinCode_val();
		break;
	case 'state':
		state_val();
		break;
	case 'tanNo':
		tanNo_val();
		break;
	case 'removeAddress':
		removeAddress_val();
		break;
	case 'remarks':
		remarks_val();
		break;
	}
}

function backtrack(id) {
	switch (id) {
	case 'customerID':
		setFocusLast('customerID');
		break;
	case 'addressSerial':
		setFocusLast('customerID');
		break;
	case 'officeAddrIdentifier':
		if ($('#action').val() == ADD)
			setFocusLast('customerID');
		else
			setFocusLast('addressSerial');
		break;
	case 'address':
		setFocusLast('officeAddrIdentifier');
		break;
	case 'pinCode':
		setFocusLast('address');
		break;
	case 'state':
		setFocusLast('pinCode');
		break;
	case 'tanNo':
		setFocusLast('state');
		break;
	case 'removeAddress':
		setFocusLast('tanNo');
		break;
	case 'remarks':
		if ($('#action').val() == ADD)
			setFocusLast('tanNo');
		else
			setFocusLast('removeAddress');
		break;
	}
}

function customerID_val() {
	var value = $('#customerID').val();
	clearError('customerID_error');
	if (isEmpty(value)) {
		setError('customerID_error', MANDATORY);
		return false;
	}
	if (!isNumeric(value)) {
		setError('customerID_error', NUMERIC_CHECK);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('CUSTOMER_ID', value);
		validator.setValue(ACTION, USAGE);
		validator
				.setClass('patterns.config.web.forms.reg.ecustomeraddressbean');
		validator.setMethod('validateCustomerID');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('customerID_error', validator.getValue(ERROR));
			return false;
		} else {
			$('#customerID_desc').html(validator.getValue('CUSTOMER_NAME'));
		}
	}
	if ($('#action').val() != ADD)
		setFocusLast('addressSerial');
	else
		setFocusLast('officeAddrIdentifier');
	return true;
}

function addressSerial_val() {
	var value = $('#addressSerial').val();
	clearError('addressSerial_error');
	if ($('#action').val() == MODIFY || $('#rectify').val() == COLUMN_ENABLE) {
		if (isEmpty(value)) {
			setError('addressSerial_error', MANDATORY);
			return false;
		}
		if (!isNumeric(value)) {
			setError('addressSerial_error', NUMERIC_CHECK);
			return false;
		} else {
			validator.clearMap();
			validator.setMtm(false);
			validator.setValue('CUSTOMER_ID', $('#customerID').val());
			validator.setValue('ADDR_SL', value);
			validator.setValue(ACTION, $('#action').val());
			validator
					.setClass('patterns.config.web.forms.reg.ecustomeraddressbean');
			validator.setMethod('validateAddressSerial');
			validator.sendAndReceive();
			if (validator.getValue(ERROR) != EMPTY_STRING) {
				setError('addressSerial_error', validator.getValue(ERROR));
				return false;
			} else {
				PK_VALUE = getEntityCode() + PK_SEPERATOR
						+ $('#customerID').val() + PK_SEPERATOR
						+ $('#addressSerial').val();
				if (!loadPKValues(PK_VALUE, 'addressSerial_error')) {
					return false;
				}
			}
		}
	}
	setFocusLast('officeAddrIdentifier');
	return true;
}

function officeAddrIdentifier_val() {
	var value = $('#officeAddrIdentifier').val();
	clearError('officeAddrIdentifier_error');
	if (!isEmpty(value)) {
		if (!isValidDescription(value)) {
			setError('officeAddrIdentifier_error', INVALID_CONCISE_DESCRIPTION);
			return false;
		}
	}
	setFocusLast('address');
	return true;
}

function address_val() {
	var value = $('#address').val();
	clearError('address_error');
	if (isEmpty(value)) {
		setError('address_error', MANDATORY);
		return false;
	}
	if (!isValidAddress(value)) {
		setError('address_error', INVALID_ADDRESS);
		return false;
	}
	setFocusLast('pinCode');
	return true;

}

function pinCode_val() {
	var value = $('#pinCode').val();
	clearError('pinCode_error');
	if (isEmpty(value)) {
		setError('pinCode_error', MANDATORY);
		return false;
	}
	if (!isNumeric(value)) {
		setError('pinCode_error', NUMERIC_CHECK);
		return false;
	}
	if (isNegative(value)) {
		setError('pinCode_error', POSITIVE_CHECK);
		return false;
	}
	if (!isLength(value, 6)) {
		setError('pinCode_error', PINCODE_LENGTH);
		return false;
	}
	setFocusLast('state');
	return true;
}

function state_val() {
	var value = $('#state').val();
	clearError('state_error');
	if (isEmpty(value)) {
		setError('state_error', MANDATORY);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('STATE_CODE', value);
		validator.setValue(ACTION, USAGE);
		validator
				.setClass('patterns.config.web.forms.reg.ecustomeraddressbean');
		validator.setMethod('validateStateCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('state_error', validator.getValue(ERROR));
			$('#state_desc').html(EMPTY_STRING);
			return false;
		}
		$('#state_desc').html(validator.getValue('DESCRIPTION'));
	}
	setFocusLast('tanNo');
	return true;
}

function tanNo_val() {
	var value = $('#tanNo').val();
	clearError('tanNo_error');
	if (isEmpty(value)) {
		setError('tanNo_error', MANDATORY);
		return false;
	}
	if (!isValidDescription(value)) {
		setError('tanNo_error', INVALID_TAN_NO);
		return false;
	}
	if ($('#action').val() == ADD)
		setFocusLast('remarks');
	else
		setFocusLast('remarks');
	return true;

}

function removeAddress_val() {
	setFocusLast('remarks');
	return true;
}
function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', INVALID_REMARKS);
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', INVALID_REMARKS);
			return false;
		}
	}
	if ($('#action').val() == MODIFY) {
		if (isEmpty(value)) {
			setError('remarks_error', MANDATORY);
			return false;
		}
	}
	setFocusOnSubmit();

}

function redirect() {
	var url = getBasePath() + $('#redirectPagePath').val();
	url = url + "?addresspk=" + getEntityCode() + PK_SEPERATOR
			+ $('#customerID').val();
	window.location.href = url;
}
