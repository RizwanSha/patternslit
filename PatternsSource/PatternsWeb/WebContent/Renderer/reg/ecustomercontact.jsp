<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.reg.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/reg/ecustomercontact.js" />
	</web:dependencies>
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="ecustomercontact.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/reg/ecustomercontact" id="ecustomercontact" method="POST">
				<web:dividerBlock>
					<type:optionBox add="true" modify="true" view="true" template="true" />
				</web:dividerBlock>
				<web:viewContent id="po_view1" styleClass="hidden">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="ecustomercontact.customerid" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:customerID property="customerID" id="customerID" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
						</web:sectionBlock>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowOdd>
									<web:column>
										<web:legend key="ecustomercontact.addrsl" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:addressSerial property="addressSerial" id="addressSerial" lookup="true" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="form.effectivedate" var="common" mandatory="true" />
									</web:column>
									<web:column>
										<type:date property="effectiveDate" id="effectiveDate" lookup="true" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:sectionTitle var="program" key="ecustomercontact.contactdetail" />
						<web:section>
							<web:table>
								<web:rowOdd>
									<web:column>
										<web:button id="contactDetails" key="ecustomercontact.adddocument" var="program" onclick="addCustomerContactDetails()" />
										<web:button id="clearContactDetailGrid" key="ecustomercontact.clear" var="program" onclick="clearContactDetails()" />
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
						<type:error property="gridError" />
						<web:viewContent id="contactDetails_Fields" styleClass="form-horizontal hidden">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="270px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="ecustomercontact.title" var="program" />
										</web:column>
										<web:column>
											<type:combo property="title" id="title" datasourceid="COMMON_TITLE1" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="ecustomercontact.contactperson" var="program" />
										</web:column>
										<web:column>
											<type:description property="contactPerson" id="contactPerson" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="ecustomercontact.contactdtl1" var="program" />
										</web:column>
										<web:column>
											<type:otherInformation20 property="contactDetails1" id="contactDetails1" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="ecustomercontact.contactdtl2" var="program" />
										</web:column>
										<web:column>
											<type:otherInformation20 property="contactDetails2" id="contactDetails2" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="ecustomercontact.emailid" var="program" />
										</web:column>
										<web:column>
											<type:email property="emailID" id="emailID" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="ecustomercontact.alternateemail" var="program" />
										</web:column>
										<web:column>
											<type:checkboxCenterAlign property="alternateEmail" id="alternateEmail" description="true"/>
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:rowEven>
										<web:column>
											<web:button id="contactDtl" key="ecustomercontact.update" var="program" onclick="updateContactDetails()" />
											<web:button id="clearcontactGrid" key="ecustomercontact.clearfields" var="program" onclick="clearContactDetailsGrid()" />
											<web:button key="form.close" id="close" var="common" onclick="closePopup('0')" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
						</web:viewContent>
						<web:section>
							<web:table>
								<web:rowEven>
									<web:column>
										<web:grid height="210px" width="1031px" id="ecustomercontact_innerGrid" src="reg/ecustomercontact_innerGrid.xml">
										</web:grid>
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowOdd>
									<web:column>
										<web:legend key="form.enabled" var="common" />
									</web:column>
									<web:column>
										<type:checkbox property="enabled" id="enabled" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="form.remarks" var="common" />
									</web:column>
									<web:column>
										<type:remarks property="remarks" id="remarks" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column span="2">
										<web:submitReset />
										<web:button var="common" key="form.operationBack" onclick="redirect()" id="back" />
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
					</web:dividerBlock>
				</web:viewContent>
				<web:element property="contactpk" />
				<web:element property="redirectPageId" />
				<web:element property="redirectPagePath" />
				<web:element property="xmlcontactDetailGrid" />
				<web:element property="command" />
				<web:element property="action" />
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
	<web:queryGrid />
	<web:TBAGrid />
	<web:contextSearch />
</web:fragment>
