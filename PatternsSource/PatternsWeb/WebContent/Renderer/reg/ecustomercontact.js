var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'ECUSTOMERCONTACT';
function init() {
	hide('contactDetails_Fields');
	refreshTBAGrid();
	refreshQueryGrid();
	if (_redisplay) {
		if ($('#action').val() == ADD) {
			$('#enabled').prop('disabled', true);
			setCheckbox('enabled', YES);
			$('#effectiveDate_look').prop('disabled', true);
		}
		ecustomercontact_innerGrid.clearAll();
		if (!isEmpty($('#xmlcontactDetailGrid').val())) {
			ecustomercontact_innerGrid.loadXMLString($('#xmlcontactDetailGrid').val());
		}
	}
}

function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}

function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'ECUSTOMERCONTACT_MAIN');
}

function add() {
	$('#customerID').focus();
	$('#enabled').prop('disabled', true);
	setCheckbox('enabled', YES);
	$('#effectiveDate').val(getCBD());
	$('#effectiveDate_look').prop('disabled', true);
	if ($('#contactpk').val() != EMPTY_STRING && $('#contactpk').val().split(PK_SEPERATOR).length == 4) {
		domodify();
		PK_VALUE = $('#contactpk').val();
		if (!loadPKValues(PK_VALUE, 'addressSerial_error')) {
			return false;
		}
	}
}
function modify() {
	$('#customerID').focus();
	$('#effectiveDate_look').prop('disabled', false);
	if ($('#action').val() == MODIFY)
		$('#enabled').prop('disabled', false);
	else
		$('#enabled').prop('disabled', true);
}

function addCustomerContactDetails() {
	win = showWindow('contactDetails_Fields', EMPTY_STRING, CUSTOMER_CONTACT_DETAILS, EMPTY_STRING, true, false, false, 950, 340);
}

function loadData() {
	$('#customerID').val(validator.getValue('CUSTOMER_ID'));
	$('#customerID_desc').html(validator.getValue('F1_CUSTOMER_NAME'));
	$('#addressSerial').val(validator.getValue('ADDR_SL'));
	$('#effectiveDate').val(validator.getValue('EFF_DATE'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadContactDetailGrid();
	resetLoading();
}

function loadContactDetailGrid() {
	loadGridQuery(CURRENT_PROGRAM_ID, 'ECUSTOMERCONTACT_GRID', contactDetailsPostProcessing);
}

function contactDetailsPostProcessing(result) {
	ecustomercontact_innerGrid.parse(result);
	ecustomercontact_innerGrid.setColumnHidden(0, true);
	var noofrows = ecustomercontact_innerGrid.getRowsNum();
	if (noofrows > 0) {
		for (var i = 0; i < noofrows; i++) {
			var linkValue = "<a href='javascript:void(0);' onclick=ecustomercontactDetails_editGrid('" + i + "')>Edit</a><span>  </span> <a href='javascript:void(0);' onclick=contactDetailsdeleteGridRow('" + i + "');>Delete</a>";
			ecustomercontact_innerGrid.cells2(i, 1).setValue(linkValue);
			ecustomercontact_innerGrid.changeRowId(ecustomercontact_innerGrid.getRowId(i), i);
		}
	}
}
function view(source, primaryKey) {
	hideParent('reg/qcustomercontact', source, primaryKey, 1200);
	resetLoading();
}

function clearFields() {
	$('#customerID').val(EMPTY_STRING);
	$('#customerID_error').html(EMPTY_STRING);
	$('#customerID_desc').html(EMPTY_STRING);
	$('#addressSerial').val(EMPTY_STRING);
	$('#addressSerial_error').html(EMPTY_STRING);
	$('#effectiveDate').val(EMPTY_STRING);
	$('#effectiveDate_error').html(EMPTY_STRING);
	clearNonPKFields();
}

function clearNonPKFields() {
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
	ecustomercontact_innerGrid.clearAll();
	clearContactDetailsGrid();
	if (!isEmpty($('#contactpk').val())) {
		$('#customerID').val($('#contactpk').val().split(PK_SEPERATOR)[1]);
		$('#back').add();
	} else
		$('#back').remove();
	if (!isEmpty($('#redirectPageId').val())) {
		show('redirect_view');
	} else {
		hide('redirect_view');
	}
}
function clearContactDetailsGrid() {
	$('#title').prop('selectedIndex', 0);
	$('#title_error').html(EMPTY_STRING);
	$('#contactPerson').val(EMPTY_STRING);
	$('#contactPerson_error').html(EMPTY_STRING);
	$('#contactDetails1').val(EMPTY_STRING);
	$('#contactDetails1_error').html(EMPTY_STRING);
	$('#contactDetails2').val(EMPTY_STRING);
	$('#contactDetails2_error').html(EMPTY_STRING);
	$('#emailID').val(EMPTY_STRING);
	$('#emailID_error').html(EMPTY_STRING);
	setCheckbox('alternateEmail', NO);
	$('#gridError_error').html(EMPTY_STRING);
}

function doclearfields(id) {
	switch (id) {
	case 'customerID':
		if (isEmpty($('#customerID').val())) {
			clearFields();
		}
		break;
	case 'addressSerial':
		if (isEmpty($('#addressSerial').val())) {
			$('#addressSerial_error').html(EMPTY_STRING);
			$('#effectiveDate').val(EMPTY_STRING);
			$('#effectiveDate_error').html(EMPTY_STRING);
			clearNonPKFields();
		}
		break;
	case 'effectiveDate':
		if (isEmpty($('#effectiveDate').val())) {
			$('#effectiveDate_error').html(EMPTY_STRING);
			clearNonPKFields();
		}
		break;
	}
}

function doHelp(id) {
	switch (id) {
	case 'customerID':
		help('COMMON', 'HLP_CUSTOMER_ID', $('#customerID').val(), EMPTY_STRING, $('#customerID'));
		break;
	case 'addressSerial':
		help('COMMON', 'HLP_CUST_ADDR_SL', $('#addressSerial').val(), $('#customerID').val(), $('#addressSerial'));
		break;
	case 'effectiveDate':
		help('ECUSTOMERCONTACT', 'HLP_EFF_DATE', $('#effectiveDate').val(), $('#customerID').val() + '|' + $('#addressSerial').val(), $('#effectiveDate'));
		break;
	}
}

function validate(id) {
	var valMode = true;
	switch (id) {
	case 'customerID':
		customerID_val();
		break;
	case 'addressSerial':
		addressSerial_val();
		break;
	case 'effectiveDate':
		effectiveDate_val();
		break;
	case 'title':
		title_val(valMode);
		break;
	case 'contactPerson':
		contactPerson_val(valMode);
		break;
	case 'contactDetails1':
		contactDetails1_val(valMode);
		break;
	case 'contactDetails2':
		contactDetails2_val(valMode);
		break;
	case 'emailID':
		emailID_val(valMode);
		break;
	case 'alternateEmail':
		alternateEmail_val();
		break;
	case 'enabled':
		enabled_val();
		break;
	case 'remarks':
		remarks_val();
		break;
	}
}

function backtrack(id) {
	switch (id) {
	case 'customerID':
		setFocusLast('customerID');
		break;
	case 'addressSerial':
		setFocusLast('customerID');
		break;
	case 'effectiveDate':
		setFocusLast('addressSerial');
		break;
	case 'title':
		closeInlinePopUp(win);
		setFocusLast('contactDetails');
		break;
	case 'contactPerson':
		setFocusLast('title');
		break;
	case 'contactDetails1':
		setFocusLast('contactPerson');
		break;
	case 'contactDetails2':
		setFocusLast('contactDetails1');
		break;
	case 'emailID':
		setFocusLast('contactDetails2');
		break;
	case 'alternateEmail':
		setFocusLast('emailID');
		break;
	case 'contactDtl':
		setFocusLast('alternateEmail');
		break;
	case 'contactDetails':
		setFocusLast('effectiveDate');
		break;
	case 'enabled':
		setFocusLast('contactDetails');
		break;
	case 'remarks':
		if ($('#action').val() == ADD)
			setFocusLast('contactDetails');
		else
			setFocusLast('enabled');
		break;
	}
}

function customerID_val() {
	var value = $('#customerID').val();
	clearError('customerID_error');
	if (isEmpty(value)) {
		setError('customerID_error', MANDATORY);
		return false;
	}
	if (!isNumeric(value)) {
		setError('customerID_error', NUMERIC_CHECK);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('CUSTOMER_ID', value);
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.reg.ecustomercontactbean');
		validator.setMethod('validateCustomerID');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('customerID_error', validator.getValue(ERROR));
			$('#customerID_desc').html(EMPTY_STRING);
			return false;
		}
		$('#customerID_desc').html(validator.getValue('CUSTOMER_NAME'));
	}
	setFocusLast('addressSerial');
	return true;
}

function addressSerial_val() {
	var value = $('#addressSerial').val();
	clearError('addressSerial_error');
	if (isEmpty(value)) {
		setError('addressSerial_error', MANDATORY);
		return false;
	}
	if (!isNumeric(value)) {
		setError('addressSerial_error', NUMERIC_CHECK);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('CUSTOMER_ID', $('#customerID').val());
		validator.setValue('ADDR_SL', value);
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.reg.ecustomercontactbean');
		validator.setMethod('validateAddressSerial');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('addressSerial_error', validator.getValue(ERROR));
			return false;
		}
	}
	setFocusLast('effectiveDate');
	return true;
}

function effectiveDate_val() {
	var value = $('#effectiveDate').val();
	clearError('effectiveDate_error');
	if (isEmpty(value))
		value = getCBD();
	$('#effectiveDate').val(value);
	if (!isValidEffectiveDate(value, 'effectiveDate_error')) {
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('CUSTOMER_ID', $('#customerID').val());
		validator.setValue('ADDR_SL', $('#addressSerial').val());
		validator.setValue('EFF_DATE', value);
		validator.setValue(ACTION, $('#action').val());
		validator.setClass('patterns.config.web.forms.reg.ecustomercontactbean');
		validator.setMethod('validateEffectiveDate');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('effectiveDate_error', validator.getValue(ERROR));
			return false;
		} else {
			if ($('#action').val() == MODIFY) {
				PK_VALUE = getEntityCode() + PK_SEPERATOR + $('#customerID').val() + PK_SEPERATOR + $('#addressSerial').val() + PK_SEPERATOR + $('#effectiveDate').val();
				if (!loadPKValues(PK_VALUE, 'effectiveDate_error')) {
					return false;
				}
			}
		}
	}
	setFocusLast('contactDetails');
	return true;
}

function title_val(valMode) {
	var value = $('#title').val();
	clearError('title_error');
	if (valMode)
		setFocusLast('contactPerson');
	return true;
}

function contactPerson_val(valMode) {
	var value = $('#contactPerson').val();
	clearError('contactPerson_error');
	if (!isEmpty($('#title').val())) {
		if (isEmpty(value)) {
			setError('contactPerson_error', MANDATORY);
			return false;
		}
	}
	if (isEmpty($('#title').val())) {
		if (!isEmpty(value)) {
			setError('title_error', MANDATORY);
			return false;
		}
	}
	if (!isEmpty(value)) {
		if (!isValidDescription(value)) {
			setError('contactPerson_error', TC_INVALID_NAME);
			return false;
		}
	}
	if (valMode)
		setFocusLast('contactDetails1');
	return true;

}

function contactDetails1_val(valMode) {
	var value = $('#contactDetails1').val();
	clearError('contactDetails1_error');

	if (!isEmpty(value)) {
		if (!isValidDescription(value)) {
			setError('contactDetails1_error', INVALID_CONTACT_DETAIL);
			return false;
		}
	}
	if (valMode)
		setFocusLast('contactDetails2');
	return true;
}

function contactDetails2_val(valMode) {
	var value = $('#contactDetails2').val();
	clearError('contactDetails2_error');
	if (!isEmpty(value)) {
		if (!isValidDescription(value)) {
			setError('contactDetails2_error', INVALID_CONTACT_DETAIL);
			return false;
		}
	}
	if (valMode)
		setFocusLast('emailID');
	return true;
}

function emailID_val(valMode) {
	var value = $('#emailID').val();
	clearError('emailID_error');
	if (isEmpty(value) && isEmpty($('#contactDetails2').val()) && isEmpty($('#contactDetails1').val()) && isEmpty($('#contactPerson').val())) {
		setError('emailID_error', CONTACT_BLANK);
		return false;
	}
	if (!isEmpty(value)) {
		if (!isEmail(value)) {
			setError('emailID_error', INVALID_EMAIL_ID);
			return false;
		}
	}
	if (valMode)
		if (isEmpty($('#emailID').val())) {
			$('#alternateEmail').prop('disabled',true);
			setFocusLast('contactDtl');	
		} else {
			$('#alternateEmail').prop('disabled',false);
			setFocusLast('alternateEmail');
		}
	return true;
}
function alternateEmail_val() {
	setFocusLast('contactDtl');
	return true;
}
function enabled_val() {
	setFocusLast('remarks');
	return true;
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', INVALID_REMARKS);
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', INVALID_REMARKS);
			return false;
		}
	}
	if ($('#action').val() == MODIFY) {
		if (isEmpty(value)) {
			setError('remarks_error', MANDATORY);
			return false;
		}
	}
	setFocusOnSubmit();

}

function updateContactDetails() {
	var rowIndex;
	if (contactGridrevalidate()) {
		rowid = ecustomercontact_innerGrid.getUserData("", "grid_edit_id");
		if (rowid != EMPTY_STRING || rowid != null)
			rowIndex = ecustomercontact_innerGrid.getRowIndex(rowid);
		else
			rowIndex = ecustomercontact_innerGrid.getRowsNum();
		if ($('#alternateEmail').is(':checked'))
			alternateEmail = ONE;
		else
			alternateEmail = ZERO;
		var linkValue = "<a href='javascript:void(0);' onclick=ecustomercontactDetails_editGrid('" + rowIndex + "')>Edit</a><span>  </span> <a href='javascript:void(0);' onclick=contactDetailsdeleteGridRow('" + rowIndex + "');>Delete</a>";
		var field_values = [ '', linkValue, $('#title').val(), $('#contactPerson').val(), $('#contactDetails1').val(), $('#contactDetails2').val(), $('#emailID').val(), alternateEmail ];
		var uid = _grid_updateRow(ecustomercontact_innerGrid, field_values);
		ecustomercontact_innerGrid.changeRowId(uid, rowIndex);
		clearContactDetailsGrid();
		closeInlinePopUp(win);
		setFocusLast('contactDetails');
		return true;
	}
}

function contactDetailsdeleteGridRow(row) {
	if (confirm(DELETE_CONFIRM)) {
		ecustomercontact_innerGrid.setUserData("", "grid_edit_id", EMPTY_STRING);
		ecustomercontact_innerGrid.deleteRow(row);
		clearContactDetailsGrid();
		setFocusLast('state');
	}
}

function ecustomercontactDetails_editGrid(index) {
	rowId = ecustomercontact_innerGrid.getRowId(index);
	clearContactDetailsGrid();
	addCustomerContactDetails();
	ecustomercontact_innerGrid.setUserData("", "grid_edit_id", rowId);
	$('#title').val(ecustomercontact_innerGrid.cells(rowId, 2).getValue());
	$('#contactPerson').val(ecustomercontact_innerGrid.cells(rowId, 3).getValue());
	$('#contactDetails1').val(ecustomercontact_innerGrid.cells(rowId, 4).getValue());
	$('#contactDetails2').val(ecustomercontact_innerGrid.cells(rowId, 5).getValue());
	$('#emailID').val(ecustomercontact_innerGrid.cells(rowId, 6).getValue());
	setCheckbox('alternateEmail', ecustomercontact_innerGrid.cells(rowId, 7).getValue());
	setFocusLast('title');
}

function clearContactDetails() {
	if (confirm(CLEAR_CONFIRM)) {
		ecustomercontact_innerGrid.clearAll();
		clearContactDetailsGrid();
		ecustomercontact_innerGrid.setUserData("", "grid_edit_id", EMPTY_STRING);
	}
}

function onloadInlinePopUp(param, id) {
	switch (id) {
	case 'contactDetails_Fields':
		clearContactDetailsGrid();
		show('contactDetails_Fields');
		setFocusLast('title');
		break;
	}

}

function gridExit(id) {
	switch (id) {
	case 'title':
	case 'contactPerson':
	case 'contactDetails1':
	case 'contactDetails2':
	case 'emailID':
		if (ecustomercontact_innerGrid.getRowsNum() > 0) {
			$('#xmlcontactDetailGrid').val(ecustomercontact_innerGrid.serialize());
			$('#title_error').html(EMPTY_STRING);
			$('#gridError_error').html(EMPTY_STRING);
			closeInlinePopUp(win);
			if ($('#action').val() == ADD)
				setFocusLast('remarks');
			else
				setFocusLast('enabled');
		} else {
			ecustomercontact_innerGrid.clearAll();
			$('#xmlcontactDetailGrid').val(ecustomercontact_innerGrid.serialize());
			setError('title_error', ADD_ATLEAST_ONE_ROW);
			setError('gridError_error', ADD_ATLEAST_ONE_ROW);
			if ($('#action').val() == ADD)
				setFocusLast('remarks');
			else
				setFocusLast('enabled');
		}
		break;
	}
}

function oncloseInlinePopUp(win) {
	switch (win._idd) {
	case 'contactDetails_Fields':
		$('#xmlcontactDetailGrid').val(ecustomercontact_innerGrid.serialize());
		ecustomercontact_innerGrid.setUserData("", "grid_edit_id", EMPTY_STRING);
		clearContactDetailsGrid();
		if ($('#action').val() == ADD)
			setFocusLast('remarks');
		else
			setFocusLast('enabled');
		break;
	}
	return true;
}

function contactGridrevalidate() {
	var errors = 0;
	if (!title_val(false)) {
		errors++;
	}
	if (!contactPerson_val(false)) {
		errors++;
	}
	if (!contactDetails1_val(false)) {
		errors++;
	}
	if (!contactDetails2_val(false)) {
		errors++;
	}
	if (!emailID_val(false)) {
		errors++;
	}
	if (errors > 0) {
		if (win)
			win.showInnerScroll();
		return false;
	} else
		return true;
}

function revalidate() {
	revaildateGrid();
}
function revaildateGrid() {
	if (ecustomercontact_innerGrid.getRowsNum() > 0) {
		ecustomercontact_innerGrid.setSerializationLevel(false, false, false, false, false, true);
		$('#xmlcontactDetailGrid').val(ecustomercontact_innerGrid.serialize());
		$('#title_error').html(EMPTY_STRING);
		$('#gridError_error').html(EMPTY_STRING);
	} else {
		ecustomercontact_innerGrid.clearAll();
		$('#xmlcontactDetailGrid').val(ecustomercontact_innerGrid.serialize());
		setError('title_error', ADD_ATLEAST_ONE_ROW);
		setError('gridError_error', ADD_ATLEAST_ONE_ROW);
		errors++;
	}
}

function closePopup(value) {
	if (value == 0) {
		closeInlinePopUp(win);
		ecustomercontact_innerGrid.setUserData("", "grid_edit_id", EMPTY_STRING);
		setFocusLast('title');
	}
	return true;
}

function redirect() {
	var url = getBasePath() + $('#redirectPagePath').val();
	url = url + "?addresspk=" + getEntityCode() + PK_SEPERATOR + $('#customerID').val();
	window.location.href = url;
}
