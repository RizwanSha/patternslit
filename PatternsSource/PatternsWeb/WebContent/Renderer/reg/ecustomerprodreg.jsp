<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.reg.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/reg/ecustomerprodreg.js" />
	</web:dependencies>
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="ecustomerprodreg.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/reg/ecustomerprodreg" id="ecustomerprodreg" method="POST">
				<web:dividerBlock>
					<type:optionBox add="true" modify="false" view="true" />
				</web:dividerBlock>
				<web:viewContent id="po_view1" styleClass="hidden">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="ecustomerprodreg.customerid" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:customerID property="customerID" id="customerID" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="ecustomerprodreg.entrysl" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:serial property="entrySl" id="entrySl" lookup="true"/>
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
						</web:sectionBlock>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="ecustomerprodreg.name" var="program" />
									</web:column>
									<web:column>
										<type:description property="customerName" id="customerName" readOnly="true" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="ecustomerprodreg.dateofreg" var="program" />
									</web:column>
									<web:column>
										<type:date property="dateOfReg" id="dateOfReg" readOnly="true" />
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
						<web:sectionTitle var="program" key="ecustomerprodreg.custprodsection" />
						<web:section>
							<web:table>
								<web:rowOdd>
									<web:column>
										<web:button id="productDetails" key="ecustomerprodreg.adddocument" var="program" onclick="addCustomerProductDetails()" />
										<web:button id="clearproductDetailGrid" key="ecustomerprodreg.clear" var="program" onclick="clearCustomerProductDetails()" />
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
						<type:error property="gridError" />
						<web:viewContent id="productDetails_Fields" styleClass="form-horizontal hidden">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="ecustomerprodreg.product" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:leaseProductCode property="product" id="product" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="ecustomerprodreg.portfoliocode" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:portfolioCode property="portfolioCode" id="portfolioCode" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="ecustomerprodreg.sundrydebtoraccount" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:otherInformation10 property="debtorAccount" id="debtorAccount" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="ecustomerprodreg.accidprincipal" var="program" />
										</web:column>
										<web:column>
											<type:otherInformation10 property="accountIDPrincipal" id="accountIDPrincipal" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="ecustomerprodreg.accidinterest" var="program" />
										</web:column>
										<web:column>
											<type:otherInformation10 property="accountIDInterest" id="accountIDInterest" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="ecustomerprodreg.accidunearned" var="program" />
										</web:column>
										<web:column>
											<type:otherInformation10 property="accountIDUnearned" id="accountIDUnearned" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:rowEven>
										<web:column>
											<web:button id="productDtl" key="ecustomerprodreg.update" var="program" onclick="updateCustomerProductDetails()" />
											<web:button id="clearProductGrid" key="ecustomerprodreg.clearfields" var="program" onclick="clearProductDetails()" />
											<web:button key="form.close" id="close" var="common" onclick="closePopup('0')" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
						</web:viewContent>
						<web:section>
							<web:table>
								<web:rowEven>
									<web:column>
										<web:grid height="210px" width="921px" id="ecustomerprodreg_productGrid" src="reg/ecustomerprodreg_productGrid.xml">
										</web:grid>
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowOdd>
									<web:column>
										<web:legend key="form.remarks" var="common" />
									</web:column>
									<web:column>
										<type:remarks property="remarks" id="remarks" />
									</web:column>
								</web:rowOdd>
								<web:rowOdd>
									<web:column span="2">
										<web:submitReset />
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
					</web:dividerBlock>
				</web:viewContent>
				<web:element property="xmlproductDetailGrid" />
				<web:element property="command" />
				<web:element property="action" />
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
	<web:queryGrid />
	<web:TBAGrid />
	<web:contextSearch />
</web:fragment>
