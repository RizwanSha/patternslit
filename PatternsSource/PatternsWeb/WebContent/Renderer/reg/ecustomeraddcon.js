var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'ECUSTOMERADDCON';
function init() {
	clearFields();
	ecustomeraddcon_innerGrid.attachEvent("onRowDblClicked", qcustomeraddress);
	ecustomercon_innerGrid.attachEvent("onRowDblClicked", qcustomercontact);
	if (_redisplay) {
		ecustomeraddcon_innerGrid.clearAll();
		ecustomercon_innerGrid.clearAll();
	}
}

function loadData() {
	$('#lesseeCode').val(validator.getValue('LESSEE_CODE'));
	$('#lesseeCode_desc').html(validator.getValue('F1_DESCRIPTION'));
	$('#customerID').val(validator.getValue('CUSTOMER_ID'));
	$('#customerID_desc').html(validator.getValue('F2_CUSTOMER_NAME'));
	resetLoading();
}

function view(source, primaryKey) {
	hideParent('reg/qcustomeraddcon', source, primaryKey, 1200);
	resetLoading();
}

function clearFields() {
	$('#lesseeCode').val(EMPTY_STRING);
	$('#lesseeCode_error').html(EMPTY_STRING);
	$('#customerID').val(EMPTY_STRING);
	$('#customerID_error').html(EMPTY_STRING);
	$('#customerID_desc').html(EMPTY_STRING);
	clearNonPKFields();
	if (!isEmpty($('#addresspk').val())) {
		$('#customerID').val($('#addresspk').val().split(PK_SEPERATOR)[1]);
		customerID_val();
	}
	if (!isEmpty($('#contactpk').val())) {
		$('#customerID').val($('#contactpk').val().split(PK_SEPERATOR)[1]);
		customerID_val();
	}
}

function clearNonPKFields() {
	ecustomeraddcon_innerGrid.clearAll();
	ecustomercon_innerGrid.clearAll();
}

function doHelp(id) {
	switch (id) {
	case 'lesseeCode':
		help('COMMON', 'HLP_DEBTOR_CODE', $('#lesseeCode').val(), EMPTY_STRING, $('#lesseeCode'), null, function(gridObj, rowID) {
			$('#lesseeCode').val(gridObj.cells(rowID, 0).getValue());
			$('#lesseeCode_desc').html(EMPTY_STRING);
		});
		break;
	case 'customerID':
		help('COMMON', 'HLP_CUSTOMER_ID', $('#customerID').val(), EMPTY_STRING, $('#customerID'), null, function(gridObj, rowID) {
			$('#customerID').val(gridObj.cells(rowID, 0).getValue());
			$('#customerID_desc').html(EMPTY_STRING);
		});
		break;
	}
}

function backtrack(id) {
	switch (id) {
	case 'customerID':
		setFocusLast('lesseeCode');
		break;
	}
}
function doclearfields(id) {
	switch (id) {
	case 'lesseeCode':
		if (isEmpty($('#lesseeCode').val())) {
			$('#lesseeCode_error').html(EMPTY_STRING);
			$('#customerID').val(EMPTY_STRING);
			$('#customerID_error').html(EMPTY_STRING);
			$('#customerID_desc').html(EMPTY_STRING);
			$('#customerName').val(EMPTY_STRING);
			clearNonPKFields();
		}
		break;
	case 'customerID':
		if (isEmpty($('#customerID').val())) {
			$('#lesseeCode').val(EMPTY_STRING);
			$('#lesseeCode_error').html(EMPTY_STRING);
			$('#customerID_error').html(EMPTY_STRING);
			$('#customerID_desc').html(EMPTY_STRING);
			$('#customerName').val(EMPTY_STRING);
			clearNonPKFields();
		}
		break;
	}
}

function validate(id) {
	switch (id) {
	case 'lesseeCode':
		lesseeCode_val();
		break;
	case 'customerID':
		customerID_val();
		break;
	}
}

function lesseeCode_val() {
	clearGridFields(true);
	var value = $('#lesseeCode').val();
	clearError('lesseeCode_error');
	if (!isEmpty(value)) {
		if ($('#action').val() == ADD && !isValidCode(value)) {
			setError('lesseeCode_error', INVALID_FORMAT);
			return false;
		} else {
			validator.clearMap();
			validator.setMtm(false);
			validator.setValue('SUNDRY_DB_AC', $('#lesseeCode').val());
			validator.setValue(ACTION, $('#action').val());
			validator.setClass('patterns.config.web.forms.reg.ecustomeraddconbean');
			validator.setMethod('validateLesseeCode');
			validator.sendAndReceive();
			if (validator.getValue(ERROR) != EMPTY_STRING) {
				setError('lesseeCode_error', validator.getValue(ERROR));
				return false;
			}
		}
		$('#customerID').val(validator.getValue('CUSTOMER_ID'));
		$('#customerName').val(validator.getValue('CUSTOMER_NAME'));
		xmlString = validator.getValue(RESULT_XML);
		ecustomeraddcon_innerGrid.loadXMLString(xmlString);
	}

	setFocusLast('customerID');
	return true;
}

function customerID_val() {
	clearGridFields(false);
	var value = $('#customerID').val();
	clearError('customerID_error');
	if (isEmpty(value)) {
		setError('customerID_error', MANDATORY);
		return false;
	}
	if (!isNumeric(value)) {
		setError('customerID_error', NUMERIC_CHECK);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('CUSTOMER_ID', value);
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.reg.ecustomeraddconbean');
		validator.setMethod('validateCustomerID');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('customerID_error', validator.getValue(ERROR));
			return false;
		}
		$('#lesseeCode').val(EMPTY_STRING);
		$('#customerName').val(validator.getValue('CUSTOMER_NAME'));
		xmlString = validator.getValue(RESULT_XML);
		ecustomeraddcon_innerGrid.loadXMLString(xmlString);
	}
	$('#customerID').blur();
	return true;
}

function clearGridFields(valMode) {
	$('#customerID_desc').html(EMPTY_STRING);
	$('#customerName').val(EMPTY_STRING);
	ecustomeraddcon_innerGrid.clearAll();
	ecustomercon_innerGrid.clearAll();
	if (valMode) {
		$('#customerID').val(EMPTY_STRING);
	} else {
		$('#lesseeCode').val(EMPTY_STRING);
		$('#lesseeCode_error').html(EMPTY_STRING);
	}
}
function qcustomeraddress(rowId) {
	var pkValue = ecustomeraddcon_innerGrid.cells(rowId, 10).getValue();
	window.scroll(0, 0);
	showWindow('PREVIEW_VIEW', getBasePath() + 'Renderer/reg/qcustomeraddress.jsp', 'QCUSTOMERADDRESS', PK + '=' + pkValue + "&" + SOURCE + "=" + MAIN, true, false, false, 940, 575);
	resetLoading();
}

function qcustomercontact(rowId) {
	var pkValue = getEntityCode() + PK_SEPERATOR + $('#customerID').val() + PK_SEPERATOR + $('#forAddressSerial').val() + PK_SEPERATOR + $('#effectiveSince').val();
	window.scroll(0, 0);
	showWindow('PREVIEW_VIEW', getBasePath() + 'Renderer/reg/qcustomercontact.jsp', 'QCUSTOMERCONTACT', PK + '=' + pkValue + "&" + SOURCE + "=" + MAIN, true, false, false, 1175, 575);
	resetLoading();
}

function viewCustomerContact(pk) {
	win = showWindow('contact', EMPTY_STRING, CONTACT_DETAILS, '', true, false, false, 1000, 400);
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('CUSTOMER_ID', $('#customerID').val());
	validator.setValue('ADDR_SL', pk.split(PK_SEPERATOR)[2]);
	validator.setValue(ACTION, USAGE);
	validator.setClass('patterns.config.web.forms.reg.ecustomeraddconbean');
	validator.setMethod('validateViewCustomerContact');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('customerID_error', validator.getValue(ERROR));
		return false;
	}
	$('#forAddressSerial').val(validator.getValue('ADDR_SL'));
	$('#effectiveSince').val(validator.getValue('EFF_DATE'));
	xmlString = validator.getValue(RESULT_XML);
	ecustomercon_innerGrid.loadXMLString(xmlString);

}

function addCustomerAddressDetails() {
	var url = getBasePath() + 'Renderer/reg/ecustomeraddress.jsp';
	location.href = url + "?addresspk=" + getEntityCode() + PK_SEPERATOR + $('#customerID').val() + "&_RL=ecustomeraddcon";
	resetLoading();
}

function addCustomerContactDetails() {

	var url = getBasePath() + 'Renderer/reg/ecustomercontact.jsp';
	location.href = url + "?contactpk=" + getEntityCode() + PK_SEPERATOR + $('#customerID').val() + "&_RL=ecustomeraddcon";
	resetLoading();
}

function editCustomerAddress(pkValue) {
	var url = getBasePath() + 'Renderer/reg/ecustomeraddress.jsp';
	location.href = url + "?addresspk=" + pkValue + "&_RL=ecustomeraddcon";
	resetLoading();
}

function editCustomerContactDetails(pkValue) {
	var pkValue = getEntityCode() + PK_SEPERATOR + $('#customerID').val() + PK_SEPERATOR + $('#forAddressSerial').val() + PK_SEPERATOR + $('#effectiveSince').val();
	var url = getBasePath() + 'Renderer/reg/ecustomercontact.jsp';
	location.href = url + "?contactpk=" + pkValue + "&_RL=ecustomeraddcon";
	resetLoading();
}
