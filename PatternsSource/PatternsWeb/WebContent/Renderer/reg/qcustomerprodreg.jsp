<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.reg.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/reg/qcustomerprodreg.js" />
	</web:dependencies>
	<web:loadPK />
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="qcustomerprodreg.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:viewContent id="po_view1">
				<web:dividerBlock>
					<web:sectionBlock width="width-100p" align="left">
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="190px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="ecustomerprodreg.customerid" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:customerIDDisplay property="customerID" id="customerID" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="ecustomerprodreg.entrysl" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:serialDisplay property="entrySl" id="entrySl" />
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="190px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="ecustomerprodreg.name" var="program" />
									</web:column>
									<web:column>
										<type:descriptionDisplay property="customerName" id="customerName" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="ecustomerprodreg.dateofreg" var="program" />
									</web:column>
									<web:column>
										<type:dateDisplay property="dateOfReg" id="dateOfReg" />
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
						<web:sectionTitle var="program" key="ecustomerprodreg.custprodsection" />
						<web:viewContent id="productDetails_Fields" styleClass="form-horizontal hidden">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="ecustomerprodreg.product" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:leaseProductCodeDisplay property="product" id="product" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="ecustomerprodreg.portfoliocode" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:portfolioCodeDisplay property="portfolioCode" id="portfolioCode" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="ecustomerprodreg.sundrydebtoraccount" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:otherInformation10Display property="debtorAccount" id="debtorAccount" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="ecustomerprodreg.accidprincipal" var="program" />
										</web:column>
										<web:column>
											<type:otherInformation10Display property="accountIDPrincipal" id="accountIDPrincipal" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="ecustomerprodreg.accidinterest" var="program" />
										</web:column>
										<web:column>
											<type:otherInformation10Display property="accountIDInterest" id="accountIDInterest" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="ecustomerprodreg.accidunearned" var="program" />
										</web:column>
										<web:column>
											<type:otherInformation10Display property="accountIDUnearned" id="accountIDUnearned" />
										</web:column>
									</web:rowEven>
									<web:column span="2">
										<p align="left">
											<web:button key="form.close" id="close" var="common" onclick="closePopup('0')" />
										</p>
									</web:column>
								</web:table>
							</web:section>
						</web:viewContent>
						<web:section>
							<web:table>
								<web:rowOdd>
									<web:column>
										<web:grid height="240px" width="921px" id="ecustomerprodreg_productGrid" src="reg/ecustomerprodreg_productGrid.xml">
										</web:grid>
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="190px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="form.remarks" var="common"  />
									</web:column>
									<web:column>
										<type:remarksDisplay property="remarks" id="remarks" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:auditDisplay />
					</web:sectionBlock>
				</web:dividerBlock>
			</web:viewContent>
		</web:viewSubPart>
	</web:viewPart>
</web:fragment>
