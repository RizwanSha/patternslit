<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.reg.Messages"
	var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/reg/ecustomeraddress.js" />
	</web:dependencies>
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="ecustomeraddress.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/reg/ecustomeraddress"
				id="ecustomeraddress" method="POST">
				<web:dividerBlock>
					<type:optionBox add="true" modify="true" view="true" />
				</web:dividerBlock>
				<web:viewContent id="po_view1" styleClass="hidden">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="ecustomeraddress.customerid" var="program"
												mandatory="true" />
										</web:column>
										<web:column>
											<type:customerID property="customerID" id="customerID" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
						</web:sectionBlock>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowOdd>
									<web:column>
										<web:legend key="ecustomeraddress.addrserial" var="program"
											mandatory="true" />
									</web:column>
									<web:column>
										<type:addressSerial property="addressSerial"
											id="addressSerial" lookup="true" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="ecustomeraddress.officeaddr" var="program" />
									</web:column>
									<web:column>
										<type:conciseDescription property="officeAddrIdentifier"
											id="officeAddrIdentifier" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="ecustomeraddress.addr" var="program"
											mandatory="true" />
									</web:column>
									<web:column>
										<type:remarks property="address" id="address" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="ecustomeraddress.pin" var="program"
											mandatory="true" />
									</web:column>
									<web:column>
										<type:pinCode property="pinCode" id="pinCode" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="ecustomeraddress.state" var="program"
											mandatory="true" />
									</web:column>
									<web:column>
										<type:stateCode property="state" id="state" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="ecustomeraddress.tan" var="program"
											mandatory="true" />
									</web:column>
									<web:column>
										<type:otherInformation25 property="tanNo" id="tanNo" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="ecustomeraddress.removeaddr" var="program" />
									</web:column>
									<web:column>
										<type:checkbox property="removeAddress" id="removeAddress" />
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="form.remarks" var="common" />
									</web:column>
									<web:column>
										<type:remarks property="remarks" id="remarks" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column span="3">
										<web:submitReset />
										<web:button var="common" key="form.operationBack"
											onclick="redirect()" id="back" />
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
					</web:dividerBlock>
				</web:viewContent>
				<web:element property="addresspk" />
				<web:element property="redirectPageId" />
				<web:element property="redirectPagePath" />
				<web:element property="command" />
				<web:element property="action" />
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
	<div id="calledQueryGrid">
		<web:queryGrid />
		<web:TBAGrid />
	</div>
	<web:contextSearch />
</web:fragment>
