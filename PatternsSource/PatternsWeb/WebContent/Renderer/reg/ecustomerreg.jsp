<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.reg.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/reg/ecustomerreg.js" />
	</web:dependencies>
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="ecustomerreg.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/reg/ecustomerreg" id="ecustomerreg" method="POST">
				<web:dividerBlock>
					<type:optionBox add="true" modify="false" view="true" />
				</web:dividerBlock>
				<web:viewContent id="po_view1" styleClass="hidden">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="ecustomerreg.customerid" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:customerID property="customerID" id="customerID" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
						</web:sectionBlock>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowOdd>
									<web:column>
										<web:legend key="ecustomerreg.name" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:description property="customerName" id="customerName" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="ecustomerreg.dateofreg" var="program" />
									</web:column>
									<web:column>
										<type:date property="dateOfReg" id="dateOfReg" readOnly="true" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:sectionTitle var="program" key="ecustomerreg.custprodsection" />
						<web:section>
							<web:table>
								<web:rowOdd>
									<web:column>
										<web:button id="productDetails" key="ecustomerreg.adddocument" var="program" onclick="addCustomerProductDetails()" />
										<web:button id="clearproductDetailGrid" key="ecustomerreg.clear" var="program" onclick="clearCustomerProductDetails()" />
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
						<type:error property="gridError" />
						<web:viewContent id="productDetails_Fields" styleClass="form-horizontal hidden">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="ecustomerreg.product" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:leaseProductCode property="product" id="product" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="ecustomerreg.portfoliocode" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:portfolioCode property="portfolioCode" id="portfolioCode" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="ecustomerreg.sundrydebtoraccount" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:otherInformation10 property="debtorAccount" id="debtorAccount" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="ecustomerreg.accidprincipal" var="program" />
										</web:column>
										<web:column>
											<type:otherInformation10 property="accountIDPrincipal" id="accountIDPrincipal" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="ecustomerreg.accidinterest" var="program" />
										</web:column>
										<web:column>
											<type:otherInformation10 property="accountIDInterest" id="accountIDInterest" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="ecustomerreg.accidunearned" var="program" />
										</web:column>
										<web:column>
											<type:otherInformation10 property="accountIDUnearned" id="accountIDUnearned" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:rowEven>
										<web:column>
											<web:button id="productDtl" key="ecustomerreg.update" var="program" onclick="updateCustomerProductDetails()" />
											<web:button id="clearProductGrid" key="ecustomerreg.clearfields" var="program" onclick="clearProductDetails()" />
											<web:button key="form.close" id="close" var="common" onclick="closePopup('0')" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
						</web:viewContent>
						<web:section>
							<web:table>
								<web:rowEven>
									<web:column>
										<web:grid height="210px" width="921px" id="ecustomerregproductDetails_innerGrid" src="reg/ecustomerregproductDetails_innerGrid.xml">
										</web:grid>
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowOdd>
									<web:column>
										<web:legend key="ecustomerreg.pan" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:otherInformation25 property="panNo" id="panNo" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="ecustomerreg.regioncode" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:regionCode property="regionCode" id="regionCode" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="ecustomerreg.address" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:remarks property="regOfficeAddress" id="regOfficeAddress" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="ecustomerreg.pin" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:pinCode property="pinCode" id="pinCode" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="ecustomerreg.state" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:stateCode property="state" id="state" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="ecustomerreg.managerstaffid" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:staffRoleCode property="managerStaffID" id="managerStaffID" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column span="2">
										<web:submitReset />
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
					</web:dividerBlock>
				</web:viewContent>
				<web:element property="xmlproductDetailGrid" />
				<web:element property="command" />
				<web:element property="action" />
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
	<web:queryGrid />
	<web:TBAGrid />
	<web:contextSearch />
</web:fragment>
