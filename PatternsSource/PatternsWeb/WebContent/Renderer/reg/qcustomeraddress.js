var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'ECUSTOMERADDRESS';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}
function clearFields() {
	$('#customerID').val(EMPTY_STRING);
	$('#customerID_desc').html(EMPTY_STRING);
	$('#addressSerial').val(EMPTY_STRING);
	$('#officeAddrIdentifier').val(EMPTY_STRING);
	$('#address').val(EMPTY_STRING);
	$('#pinCode').val(EMPTY_STRING);
	$('#state').val(EMPTY_STRING);
	$('#state_desc').html(EMPTY_STRING);
	$('#tanNo').val(EMPTY_STRING);
	setCheckbox('removeAddress', NO);
	$('#remarks').val(EMPTY_STRING);
}
function loadData() {
	$('#customerID').val(validator.getValue('CUSTOMER_ID'));
	$('#customerID_desc').html(validator.getValue('F1_CUSTOMER_NAME'));
	$('#addressSerial').val(validator.getValue('ADDR_SL'));
	$('#officeAddrIdentifier').val(validator.getValue('OFFICE_ADDRESS_ID'));
	$('#address').val(validator.getValue('ADDRESS'));
	$('#pinCode').val(validator.getValue('PIN_CODE'));
	$('#state').val(validator.getValue('STATE_CODE'));
	$('#state_desc').html(validator.getValue('F2_DESCRIPTION'));
	$('#tanNo').val(validator.getValue('TAN_NO'));
	setCheckbox('removeAddress', validator.getValue('DETELED_FLAG'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadAuditFields(validator);
}
