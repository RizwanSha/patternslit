var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'ECUSTOMERPRODREG';
var leaseType = null;
function init() {
	hide('productDetails_Fields');
	refreshTBAGrid();
	refreshQueryGrid();
	if (_redisplay) {
		if ($('#action').val() == ADD && $('#rectify').val() == COLUMN_DISABLE) {
			$('#entrySl').prop('readonly', true);
			$('#entrySl_pic').prop('disabled', true);
		}
		ecustomerprodreg_productGrid.clearAll();
		if (!isEmpty($('#xmlproductDetailGrid').val())) {
			ecustomerprodreg_productGrid.loadXMLString($('#xmlproductDetailGrid').val());
		}
	}
}

function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}

function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'ECUSTOMERPRODREG_MAIN');
}

function add() {
	$('#customerID').focus();
	$('#entrySl').prop('readonly', true);
	$('#entrySl_pic').prop('disabled', true);
}

function modify() {
}

function loadData() {
	if ($('#rectify').val() == COLUMN_ENABLE) {
		$('#entrySl').prop('readonly', false);
		$('#entrySl_pic').prop('disabled', false);
		$('#customerID').val(validator.getValue('CUSTOMER_ID'));
		$('#entrySl').val(validator.getValue('ENTRY_SL'));
		$('#customerName').val(validator.getValue('F1_CUSTOMER_NAME'));
		$('#dateOfReg').val(validator.getValue('F1_DATE_OF_REG'));
		$('#remarks').val(validator.getValue('REMARKS'));
		loadProductDetailGrid();
		resetLoading();
		$('#entrySl').focus();
	}
}

function addCustomerProductDetails() {
	win = showWindow('productDetails_Fields', EMPTY_STRING, 'CUSTOMER_PRODUCT_DETAILS', EMPTY_STRING, true, false, false, 600, 360);
}

function loadProductDetailGrid() {
	loadGridQuery(CURRENT_PROGRAM_ID, 'ECUSTOMERPRODREG_PRODUCT_GRID', productDetailPostProcessing);
}

function productDetailPostProcessing(result) {
	ecustomerprodreg_productGrid.parse(result);
	ecustomerprodreg_productGrid.setColumnHidden(0, true);
	if ($('#rectify').val() == COLUMN_ENABLE) {
		var noofrows = ecustomerprodreg_productGrid.getRowsNum();
		if (noofrows > 0) {
			for (var i = 0; i < noofrows; i++) {
				var productCode = ecustomerprodreg_productGrid.cells2(i, 4).getValue();
				var linkValue = "<a href='javascript:void(0);' onclick=ecustomerprodreg_editGrid('" + productCode + "')>Edit</a><span>  </span> <a href='javascript:void(0);' onclick=deleteGridRow('" + productCode + "');>Delete</a>";
				ecustomerprodreg_productGrid.cells2(i, 1).setValue(linkValue);
				ecustomerprodreg_productGrid.changeRowId(ecustomerprodreg_productGrid.getRowId(i), productCode);
			}
		}

	}
}

function view(source, primaryKey) {
	hideParent('reg/qcustomerprodreg', source, primaryKey, 1200);
	resetLoading();
}

function clearFields() {
	$('#customerID').val(EMPTY_STRING);
	$('#customerID_error').html(EMPTY_STRING);
	$('#customerID_desc').html(EMPTY_STRING);
	$('#entrySl').val(EMPTY_STRING);
	$('#entrySl_error').html(EMPTY_STRING);
	clearNonPKFields();
}

function clearNonPKFields() {
	$('#customerName').val(EMPTY_STRING);
	$('#customerName_error').html(EMPTY_STRING);
	$('#dateOfReg').val(EMPTY_STRING);
	$('#dateOfReg_error').html(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
	ecustomerprodreg_productGrid.clearAll();
	clearProductDetailsGrid();
}
function clearProductDetailsGrid() {
	$('#product').val(EMPTY_STRING);
	$('#product_error').html(EMPTY_STRING);
	$('#product_desc').html(EMPTY_STRING);
	$('#debtorAccount').val(EMPTY_STRING);
	$('#debtorAccount_error').html(EMPTY_STRING);
	$('#accountIDPrincipal').val(EMPTY_STRING);
	$('#accountIDPrincipal_error').html(EMPTY_STRING);
	$('#accountIDInterest').val(EMPTY_STRING);
	$('#accountIDInterest_error').html(EMPTY_STRING);
	$('#portfolioCode').val(EMPTY_STRING);
	$('#portfolioCode_error').html(EMPTY_STRING);
	$('#portfolioCode_desc').html(EMPTY_STRING);
	$('#accountIDUnearned').val(EMPTY_STRING);
	$('#accountIDUnearned_error').html(EMPTY_STRING);
	$('#product_error').html(EMPTY_STRING);
	$('#gridError_error').html(EMPTY_STRING);
	leaseType = EMPTY_STRING;
	$('#accountIDPrincipal').prop('readonly', false);
	$('#accountIDInterest').prop('readonly', false);
	$('#accountIDUnearned').prop('readonly', false);
}

function doclearfields(id) {
	switch (id) {
	case 'customerID':
		if (isEmpty($('#customerID').val())) {
			$('#customerID').val(EMPTY_STRING);
			clearFields();
		}
		break;
	case 'entrySl':
		if (isEmpty($('#entrySl').val())) {
			$('#entrySl_error').html(EMPTY_STRING);
			clearNonPKFields();
			
		}
		break;
	}
}

function doHelp(id) {
	switch (id) {
	case 'customerID':
		help('COMMON', 'HLP_CUSTOMER_ID', $('#customerID').val(), EMPTY_STRING, $('#customerID'));
		break;
	case 'product':
		help('COMMON', 'HLP_LEASE_PROD_CODE', $('#product').val(), EMPTY_STRING, $('#product'));
		break;
	case 'portfolioCode':
		help('COMMON', 'HLP_PORTFOLIO_CODE', $('#portfolioCode').val(), EMPTY_STRING, $('#portfolioCode'));
		break;
	case 'entrySl':
		help('COMMON', 'HLP_CUSTOMER_ENTRY_SL', $('#entrySl').val(), $('#customerID').val(), $('#entrySl'));
		break;
	}
}

function validate(id) {
	var valMode = true;
	switch (id) {
	case 'customerID':
		customerID_val();
		break;
	case 'entrySl':
		entrySl_val();
		break;
	case 'product':
		product_val(valMode);
		break;
	case 'portfolioCode':
		portfolioCode_val(valMode);
		break;
	case 'debtorAccount':
		debtorAccount_val(valMode);
		break;
	case 'accountIDPrincipal':
		accountIDPrincipal_val(valMode);
		break;
	case 'accountIDInterest':
		accountIDInterest_val(valMode);
		break;
	case 'accountIDUnearned':
		accountIDUnearned_val(valMode);
		break;
	case 'remarks':
		remarks_val();
		break;

	}
}

function backtrack(id) {
	switch (id) {
	case 'customerID':
		setFocusLast('customerID');
		break;
	case 'entrySl':
		setFocusLast('customerID');
		break;
	case 'productDetails':
		if ($('#entrySl').prop('readOnly'))
			setFocusLast('customerID');
		else
			setFocusLast('entrySl');
		break;
	case 'product':
		closeInlinePopUp(win);
		setFocusLast('productDetails');
		break;
	case 'portfolioCode':
		setFocusLast('product');
		break;
	case 'debtorAccount':
		setFocusLast('portfolioCode');
		break;
	case 'accountIDPrincipal':
		setFocusLast('debtorAccount');
		break;
	case 'accountIDInterest':
		setFocusLast('accountIDPrincipal');
		break;
	case 'accountIDUnearned':
		setFocusLast('accountIDInterest');
		break;
	case 'productDtl':
		if (leaseType != 'O')
			setFocusLast('accountIDUnearned');
		else
			setFocusLast('debtorAccount');
		break;
	case 'remarks':
		setFocusLast('productDetails');
		break;
	}
}

function customerID_val() {
	var value = $('#customerID').val();
	clearError('customerID_error');
	if (isEmpty(value)) {
		setError('customerID_error', MANDATORY);
		return false;
	}
	if (!isNumeric(value)) {
		setError('customerID_error', NUMERIC_CHECK);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('CUSTOMER_ID', value);
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.reg.ecustomerprodregbean');
		validator.setMethod('validateCustomerID');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('customerID_error', validator.getValue(ERROR));
			return false;
		} else {
			$('#customerName').val(validator.getValue('CUSTOMER_NAME'));
			$('#dateOfReg').val(validator.getValue('DATE_OF_REG'));
		}
	}
	if ($('#rectify').val() == ZERO)
		setFocusLast('productDetails');
	else
		setFocusLast('entrySl');
	return true;
}
function entrySl_val() {
	var value = $('#entrySl').val();
	clearError('entrySl_error');
	if ($('#rectify').val() == COLUMN_ENABLE) {
		if (isEmpty(value)) {
			setError('entrySl_error', MANDATORY);
			return false;
		}
		if (!isNumeric(value)) {
			setError('entrySl_error', NUMERIC_CHECK);
			return false;
		} else {
			validator.clearMap();
			validator.setMtm(false);
			validator.setValue('CUSTOMER_ID', $('#customerID').val());
			validator.setValue('ENTRY_SL', value);
			validator.setValue(ACTION, $('#action').val());
			validator.setClass('patterns.config.web.forms.reg.ecustomerprodregbean');
			validator.setMethod('validateEntrySerial');
			validator.sendAndReceive();
			if (validator.getValue(ERROR) != EMPTY_STRING) {
				setError('entrySl_error', validator.getValue(ERROR));
				return false;
			} else {
				PK_VALUE = getEntityCode() + PK_SEPERATOR + $('#customerID').val() + PK_SEPERATOR + $('#entrySl').val() ;
				if (!loadPKValues(PK_VALUE, 'entrySl_error')) {
					return false;
				}
			}
		}
	}
	setFocusLast('productDetails');
	return true;
}
function product_val(valMode) {
	var value = $('#product').val();
	clearError('product_error');
	if (isEmpty(value)) {
		setError('product_error', MANDATORY);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('LEASE_PRODUCT_CODE', value);
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.reg.ecustomerprodregbean');
		validator.setMethod('validateProductCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('product_error', validator.getValue(ERROR));
			$('#product_desc').html(EMPTY_STRING);
			return false;
		}
		$('#product_desc').html(validator.getValue('DESCRIPTION'));
		leaseType = validator.getValue('LEASE_TYPE');
		checkProductType();
	}
	if (valMode)
		setFocusLast('portfolioCode');
	return true;
}
function checkProductType() {
	if (leaseType == 'O') {
		$('#accountIDPrincipal').prop('readonly', true);
		$('#accountIDInterest').prop('readonly', true);
		$('#accountIDUnearned').prop('readonly', true);
	} else {
		$('#accountIDPrincipal').prop('readonly', false);
		$('#accountIDInterest').prop('readonly', false);
		$('#accountIDUnearned').prop('readonly', false);
	}
}

function portfolioCode_val(valMode) {
	var value = $('#portfolioCode').val();
	clearError('portfolioCode_error');
	if (isEmpty(value)) {
		setError('portfolioCode_error', MANDATORY);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('PORTFOLIO_CODE', value);
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.reg.ecustomerprodregbean');
		validator.setMethod('validatePortfolioCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('portfolioCode_error', validator.getValue(ERROR));
			$('#portfolioCode_desc').html(EMPTY_STRING);
			return false;
		}
		$('#portfolioCode_desc').html(validator.getValue('DESCRIPTION'));
	}
	if (valMode)
		setFocusLast('debtorAccount');
	return true;
}

function debtorAccount_val(valMode) {
	var value = $('#debtorAccount').val();
	clearError('debtorAccount_error');
	if (isEmpty(value)) {
		setError('debtorAccount_error', MANDATORY);
		return false;
	}
	if (!isValidOtherInformation10(value)) {
		setError('debtorAccount_error', LENGTH_EXCEED_10);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('CUSTOMER_ID', $('#customerID').val());
	validator.setValue('ENTRY_SL', $('#entrySl').val());
	validator.setValue('PROGRAM_ID', CURRENT_PROGRAM_ID);
	validator.setValue('SUNDRY_DB_AC', value);
	validator.setValue(ACTION, USAGE);
	validator.setClass('patterns.config.web.forms.reg.ecustomerprodregbean');
	validator.setMethod('validateDebtorAccount');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('debtorAccount_error', validator.getValue(ERROR));
		return false;
	}
	if (valMode) {
		if (leaseType != 'O')
			setFocusLast('accountIDPrincipal');
		else
			setFocusLast('productDtl');
	}
	return true;
}

function accountIDPrincipal_val(valMode) {
	if (leaseType != 'O') {
		var value = $('#accountIDPrincipal').val();
		clearError('accountIDPrincipal_error');
		if (isEmpty(value)) {
			setError('accountIDPrincipal_error', MANDATORY);
			return false;
		}
		if (!isValidOtherInformation10(value)) {
			setError('accountIDPrincipal_error', LENGTH_EXCEED_10);
			return false;
		}
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('CUSTOMER_ID', $('#customerID').val());
		validator.setValue('ENTRY_SL', $('#entrySl').val());
		validator.setValue('PROGRAM_ID', CURRENT_PROGRAM_ID);
		validator.setValue('PRINCIPAL_AC', value);
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.reg.ecustomerprodregbean');
		validator.setMethod('validatePrincipalAccount');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('accountIDPrincipal_error', validator.getValue(ERROR));
			return false;
		}
	}
	if (valMode)
		setFocusLast('accountIDInterest');
	return true;
}

function accountIDInterest_val(valMode) {
	if (leaseType != 'O') {
		var value = $('#accountIDInterest').val();
		clearError('accountIDInterest_error');
		if (isEmpty(value)) {
			setError('accountIDInterest_error', MANDATORY);
			return false;
		}
		if (!isValidOtherInformation10(value)) {
			setError('accountIDInterest_error', LENGTH_EXCEED_10);
			return false;
		}
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('CUSTOMER_ID', $('#customerID').val());
		validator.setValue('ENTRY_SL', $('#entrySl').val());
		validator.setValue('PROGRAM_ID', CURRENT_PROGRAM_ID);
		validator.setValue('INTEREST_AC', value);
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.reg.ecustomerprodregbean');
		validator.setMethod('validateAccountIDInterest');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('accountIDInterest_error', validator.getValue(ERROR));
			return false;
		}
	}
	if (valMode)
		setFocusLast('accountIDUnearned');
	return true;
}

function accountIDUnearned_val(valMode) {
	if (leaseType != 'O') {
		var value = $('#accountIDUnearned').val();
		clearError('accountIDUnearned_error');
		if (isEmpty(value)) {
			setError('accountIDUnearned_error', MANDATORY);
			return false;
		}
		if (!isValidOtherInformation10(value)) {
			setError('accountIDUnearned_error', LENGTH_EXCEED_10);
			return false;
		}
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('CUSTOMER_ID', $('#customerID').val());
		validator.setValue('ENTRY_SL', $('#entrySl').val());
		validator.setValue('PROGRAM_ID', CURRENT_PROGRAM_ID);
		validator.setValue('UNEARNED_INTEREST_AC', value);
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.reg.ecustomerprodregbean');
		validator.setMethod('validateUnearnedInterestAccount');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('accountIDUnearned_error', validator.getValue(ERROR));
			return false;
		}
	}
	if (valMode)
		setFocusLast('productDtl');
	return true;
}

function updateCustomerProductDetails() {
	if (ecustomerprodregproductDetails_gridDuplicateCheck() && productGridrevalidate()) {
		var linkValue = "<a href='javascript:void(0);' onclick=ecustomerprodreg_editGrid('" + $('#debtorAccount').val() + "')>Edit</a><span>  </span> <a href='javascript:void(0);' onclick=deleteGridRow('" + $('#debtorAccount').val() + "');>Delete</a>";
		var field_values = [ '', linkValue, $('#product').val(), $('#portfolioCode').val(), $('#debtorAccount').val(), $('#accountIDPrincipal').val(), $('#accountIDInterest').val(), $('#accountIDUnearned').val(), leaseType, $('#product_desc').html(), $('#portfolioCode_desc').html() ];
		var uid = _grid_updateRow(ecustomerprodreg_productGrid, field_values);
		ecustomerprodreg_productGrid.changeRowId(uid, $('#debtorAccount').val());
		clearProductDetailsGrid();
		closeInlinePopUp(win);
		setFocusLast('productDetails');
		return true;
	}
}

function ecustomerprodregproductDetails_gridDuplicateCheck() {

	var currentValue = [ $('#debtorAccount').val() ];
	if (!_grid_duplicate_check(ecustomerprodreg_productGrid, currentValue, [ 4 ])) {
		setError('debtorAccount_error', DUPLICATE_DATA);
		return false;
	}
	if (!isEmpty($('#accountIDPrincipal').val())) {
		var currentValue = [ $('#accountIDPrincipal').val() ];
		if (!_grid_duplicate_check(ecustomerprodreg_productGrid, currentValue, [ 5 ])) {
			setError('accountIDPrincipal_error', DUPLICATE_DATA);
			return false;
		}
	}
	if (!isEmpty($('#accountIDInterest').val())) {
		var currentValue = [ $('#accountIDInterest').val() ];
		if (!_grid_duplicate_check(ecustomerprodreg_productGrid, currentValue, [ 6 ])) {
			setError('accountIDInterest_error', DUPLICATE_DATA);
			return false;
		}
	}
	if (!isEmpty($('#accountIDUnearned').val())) {
		var currentValue = [ $('#accountIDUnearned').val() ];
		if (!_grid_duplicate_check(ecustomerprodreg_productGrid, currentValue, [ 7 ])) {
			setError('accountIDUnearned_error', DUPLICATE_DATA);
			return false;
		}
	}
	return true;
}

function deleteGridRow(row) {
	if (confirm(DELETE_CONFIRM)) {
		ecustomerprodreg_productGrid.setUserData("", "grid_edit_id", EMPTY_STRING);
		ecustomerprodreg_productGrid.deleteRow(row);
		clearProductDetailsGrid();
		setFocusLast('product');
	}
}

function ecustomerprodreg_editGrid(rowId) {
	clearProductDetailsGrid();
	addCustomerProductDetails();
	ecustomerprodreg_productGrid.setUserData("", "grid_edit_id", rowId);
	$('#product').val(ecustomerprodreg_productGrid.cells(rowId, 2).getValue());
	$('#portfolioCode').val(ecustomerprodreg_productGrid.cells(rowId, 3).getValue());
	$('#debtorAccount').val(ecustomerprodreg_productGrid.cells(rowId, 4).getValue());
	$('#accountIDPrincipal').val(ecustomerprodreg_productGrid.cells(rowId, 5).getValue());
	$('#accountIDInterest').val(ecustomerprodreg_productGrid.cells(rowId, 6).getValue());
	$('#accountIDUnearned').val(ecustomerprodreg_productGrid.cells(rowId, 7).getValue());
	leaseType = ecustomerprodreg_productGrid.cells(rowId, 8).getValue();
	$('#product_desc').html(ecustomerprodreg_productGrid.cells(rowId, 9).getValue());
	$('#portfolioCode_desc').html(ecustomerprodreg_productGrid.cells(rowId, 11).getValue());
	checkProductType();
	setFocusLast('product');
}

function clearCustomerProductDetails() {
	if (confirm(CLEAR_CONFIRM)) {
		ecustomerprodreg_productGrid.setUserData("", "grid_edit_id", EMPTY_STRING);
		ecustomerprodreg_productGrid.clearAll();
		clearProductDetailsGrid();
		setFocusLast('productDetails');
	}
}

function clearProductDetails() {
	ecustomerprodreg_productGrid.setUserData("", "grid_edit_id", EMPTY_STRING);
	clearProductDetailsGrid();
	setFocusLast('product');
}

function onloadInlinePopUp(param, id) {
	switch (id) {
	case 'productDetails_Fields':
		clearProductDetailsGrid();
		show('productDetails_Fields');
		setFocusLast('product');
		break;
	}

}

function oncloseInlinePopUp(win) {
	switch (win._idd) {
	case 'productDetails_Fields':
		$('#xmlproductDetailGrid').val(ecustomerprodreg_productGrid.serialize());
		ecustomerprodreg_productGrid.setUserData("", "grid_edit_id", EMPTY_STRING);
		clearProductDetailsGrid();
		setFocusLast('panNo');
		break;
	}
	return true;
}

function gridExit(id) {
	switch (id) {
	case 'product':
	case 'portfolioCode':
	case 'debtorAccount':
	case 'accountIDPrincipal':
	case 'accountIDInterest':
	case 'accountIDUnearned':
	case 'productDetails':
		if (ecustomerprodreg_productGrid.getRowsNum() > 0) {
			$('#xmlproductDetailGrid').val(ecustomerprodreg_productGrid.serialize());
			$('#product_error').html(EMPTY_STRING);
			$('#gridError_error').html(EMPTY_STRING);
			closeInlinePopUp(win);
			setFocusLast('remarks');
		} else {
			ecustomerprodreg_productGrid.clearAll();
			$('#xmlproductDetailGrid').val(ecustomerprodreg_productGrid.serialize());
			setError('product_error', ADD_ATLEAST_ONE_ROW);
			setError('gridError_error', ADD_ATLEAST_ONE_ROW);
			setFocusLast('product');
		}
		break;
	}
}

function productGridrevalidate() {
	var errors = 0;
	if (!product_val(false)) {
		errors++;
	}
	if (!portfolioCode_val(false)) {
		errors++;
	}
	if (!debtorAccount_val(false)) {
		errors++;
	}
	if (!accountIDPrincipal_val(false)) {
		errors++;
	}
	if (!accountIDInterest_val(false)) {
		errors++;
	}
	if (!accountIDUnearned_val(false)) {
		errors++;
	}
	if (errors > 0) {
		if (win)
			win.showInnerScroll();
		return false;
	} else
		return true;
}

function revalidate() {
	revaildateGrid();
}
function revaildateGrid() {
	if (ecustomerprodreg_productGrid.getRowsNum() > 0) {
		ecustomerprodreg_productGrid.setSerializationLevel(false, false, false, false, false, true);
		$('#xmlproductDetailGrid').val(ecustomerprodreg_productGrid.serialize());
		$('#product_error').html(EMPTY_STRING);
		$('#gridError_error').html(EMPTY_STRING);
	} else {
		ecustomerprodreg_productGrid.clearAll();
		$('#xmlproductDetailGrid').val(ecustomerprodreg_productGrid.serialize());
		setError('product_error', ADD_ATLEAST_ONE_ROW);
		setError('gridError_error', ADD_ATLEAST_ONE_ROW);
		errors++;
	}
}

function closePopup(value) {
	if (value == 0) {
		closeInlinePopUp(win);
		ecustomerprodreg_productGrid.setUserData("", "grid_edit_id", EMPTY_STRING);
		setFocusLast('productDetails');
	}
	return true;
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', INVALID_REMARKS);
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', INVALID_REMARKS);
			return false;
		}
	}
	if ($('#action').val() == MODIFY) {
		if (isEmpty(value)) {
			setError('remarks_error', MANDATORY);
			return false;
		}
	}
	setFocusOnSubmit();
	return true;
}
