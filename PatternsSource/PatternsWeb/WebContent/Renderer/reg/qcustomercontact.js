var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'ECUSTOMERCONTACT';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}
function clearFields() {
	$('#customerID').val(EMPTY_STRING);
	$('#customerID_desc').html(EMPTY_STRING);
	$('#addressSerial').val(EMPTY_STRING);
	$('#effectiveDate').val(EMPTY_STRING);
	setCheckbox('enabled', NO);
	$('#remarks').val(EMPTY_STRING);
	ecustomercontact_innerGrid.clearAll();
	clearContactDetailsGrid();
}
function clearContactDetailsGrid() {
	$('#title').prop('selectedIndex', 0);
	$('#contactPerson').val(EMPTY_STRING);
	$('#contactDetails1').val(EMPTY_STRING);
	$('#contactDetails2').val(EMPTY_STRING);
	$('#emailID').val(EMPTY_STRING);
	setCheckbox('alternateEmail', NO);
}

function loadData() {
	$('#customerID').val(validator.getValue('CUSTOMER_ID'));
	$('#customerID_desc').html(validator.getValue('F1_CUSTOMER_NAME'));
	$('#addressSerial').val(validator.getValue('ADDR_SL'));
	$('#effectiveDate').val(validator.getValue('EFF_DATE'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadAuditFields(validator);
	loadContactDetailGrid();
}

function loadContactDetailGrid() {
	loadGridQuery(CURRENT_PROGRAM_ID, 'ECUSTOMERCONTACT_GRID', contactDetailsPostProcessing);
}
function contactDetailsPostProcessing(result) {
	ecustomercontact_innerGrid.parse(result);
	ecustomercontact_innerGrid.setColumnHidden(0, true);
	var noofrows = ecustomercontact_innerGrid.getRowsNum();
	if (noofrows > 0) {
		for (var i = 0; i < noofrows; i++) {
			var linkValue = "<a href='javascript:void(0);' onclick=ecustomercontactDetails_editGrid('" + i + "')>View</a>";
			ecustomercontact_innerGrid.cells2(i, 1).setValue(linkValue);
			ecustomercontact_innerGrid.changeRowId(ecustomercontact_innerGrid.getRowId(i), i);
		}
	}
}
function ecustomercontactDetails_editGrid(index) {
	rowId = ecustomercontact_innerGrid.getRowId(index);
	clearContactDetailsGrid();
	addCustomerContactDetails();
	ecustomercontact_innerGrid.setUserData("", "grid_edit_id", rowId);
	$('#title').append(new Option(ecustomercontact_innerGrid.cells(rowId, 2).getValue()));
	$('#contactPerson').val(ecustomercontact_innerGrid.cells(rowId, 3).getValue());
	$('#contactDetails1').val(ecustomercontact_innerGrid.cells(rowId, 4).getValue());
	$('#contactDetails2').val(ecustomercontact_innerGrid.cells(rowId, 5).getValue());
	$('#emailID').val(ecustomercontact_innerGrid.cells(rowId, 6).getValue());
	setCheckbox('alternateEmail', ecustomercontact_innerGrid.cells(rowId, 7).getValue());
}

function addCustomerContactDetails() {
	win = showWindow('contactDetails_Fields', EMPTY_STRING, CUSTOMER_CONTACT_DETAILS, EMPTY_STRING, true, false, false, 930, 310);
}
function closePopup(value) {
	if (value == 0) {
		closeInlinePopUp(win);
	}
	return true;
}
