var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'ECUSTOMERREG';
var leaseType = null;
function init() {
	hide('productDetails_Fields');
	refreshTBAGrid();
	refreshQueryGrid();
	if (_redisplay) {
		if ($('#action').val() == ADD) {
			$('#customerID').prop('readonly', true);
			$('#customerID_pic').prop('disabled', true);
		}
		ecustomerregproductDetails_innerGrid.clearAll();
		if (!isEmpty($('#xmlproductDetailGrid').val())) {
			ecustomerregproductDetails_innerGrid.loadXMLString($('#xmlproductDetailGrid').val());
		}
	}
}

function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}

function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'ECUSTOMERREG_MAIN');
}

function add() {
	$('#customerName').focus();
	$('#customerID').prop('readonly', true);
	$('#customerID_pic').prop('disabled', true);
	$('#dateOfReg').val(getCBD());
}

function modify() {
}

function loadData() {
	if ($('#rectify').val() == COLUMN_ENABLE) {
		$('#customerID').prop('readonly', false);
		$('#customerID_pic').prop('disabled', false);
		$('#customerID').val(validator.getValue('CUSTOMER_ID'));
		$('#customerName').val(validator.getValue('CUSTOMER_NAME'));
		$('#dateOfReg').val(validator.getValue('DATE_OF_REG'));
		$('#panNo').val(validator.getValue('PAN_NO'));
		$('#regionCode').val(validator.getValue('REGION_CODE'));
		$('#regionCode_desc').html(validator.getValue('F1_DESCRIPTION'));
		$('#regOfficeAddress').val(validator.getValue('REG_ADDRESS'));
		$('#pinCode').val(validator.getValue('PIN_CODE'));
		$('#state').val(validator.getValue('STATE_CODE'));
		$('#state_desc').html(validator.getValue('F2_DESCRIPTION'));
		$('#managerStaffID').val(validator.getValue('STAFF_CODE'));
		$('#managerStaffID_desc').html(validator.getValue('F3_NAME'));
		loadProductDetailGrid();
		resetLoading();
		$('#customerID').focus();
	}
}

function addCustomerProductDetails() {
	win = showWindow('productDetails_Fields', EMPTY_STRING, 'CUSTOMER_PRODUCT_DETAILS', EMPTY_STRING, true, false, false, 600, 360);
}

function loadProductDetailGrid() {
	loadGridQuery(CURRENT_PROGRAM_ID, 'ECUSTOMERREG_PRODUCT_GRID', productDetailPostProcessing);
}

function productDetailPostProcessing(result) {
	ecustomerregproductDetails_innerGrid.parse(result);
	ecustomerregproductDetails_innerGrid.setColumnHidden(0, true);
	if ($('#rectify').val() == COLUMN_ENABLE) {
		var noofrows = ecustomerregproductDetails_innerGrid.getRowsNum();
		if (noofrows > 0) {
			for (var i = 0; i < noofrows; i++) {
				var productCode = ecustomerregproductDetails_innerGrid.cells2(i, 4).getValue();
				var linkValue = "<a href='javascript:void(0);' onclick=ecustomerreg_editGrid('" + productCode + "')>Edit</a><span>  </span> <a href='javascript:void(0);' onclick=deleteGridRow('" + productCode + "');>Delete</a>";
				ecustomerregproductDetails_innerGrid.cells2(i, 1).setValue(linkValue);
				ecustomerregproductDetails_innerGrid.changeRowId(ecustomerregproductDetails_innerGrid.getRowId(i), productCode);
			}
		}

	}
}

function view(source, primaryKey) {
	hideParent('reg/qcustomerreg', source, primaryKey, 1200);
	resetLoading();
}

function clearFields() {
	$('#customerID').val(EMPTY_STRING);
	$('#customerID_error').html(EMPTY_STRING);
	$('#customerID_desc').html(EMPTY_STRING);
	clearNonPKFields();
}

function clearNonPKFields() {
	$('#customerName').val(EMPTY_STRING);
	$('#customerName_error').html(EMPTY_STRING);
	$('#panNo').val(EMPTY_STRING);
	$('#panNo_error').html(EMPTY_STRING);
	$('#regOfficeAddress').val(EMPTY_STRING);
	$('#regOfficeAddress_error').html(EMPTY_STRING);
	$('#pinCode').val(EMPTY_STRING);
	$('#pinCode_error').html(EMPTY_STRING);
	$('#state').val(EMPTY_STRING);
	$('#state_desc').html(EMPTY_STRING);
	$('#state_error').html(EMPTY_STRING);
	$('#managerStaffID').val(EMPTY_STRING);
	$('#managerStaffID_error').html(EMPTY_STRING);
	$('#managerStaffID_desc').html(EMPTY_STRING);
	$('#regionCode').val(EMPTY_STRING);
	$('#regionCode_error').html(EMPTY_STRING);
	$('#regionCode_desc').html(EMPTY_STRING);
	ecustomerregproductDetails_innerGrid.clearAll();
	clearProductDetailsGrid();
}
function clearProductDetailsGrid() {
	$('#product').val(EMPTY_STRING);
	$('#product_error').html(EMPTY_STRING);
	$('#product_desc').html(EMPTY_STRING);
	$('#debtorAccount').val(EMPTY_STRING);
	$('#debtorAccount_error').html(EMPTY_STRING);
	$('#accountIDPrincipal').val(EMPTY_STRING);
	$('#accountIDPrincipal_error').html(EMPTY_STRING);
	$('#accountIDInterest').val(EMPTY_STRING);
	$('#accountIDInterest_error').html(EMPTY_STRING);
	$('#portfolioCode').val(EMPTY_STRING);
	$('#portfolioCode_error').html(EMPTY_STRING);
	$('#portfolioCode_desc').html(EMPTY_STRING);
	$('#accountIDUnearned').val(EMPTY_STRING);
	$('#accountIDUnearned_error').html(EMPTY_STRING);
	$('#product_error').html(EMPTY_STRING);
	$('#gridError_error').html(EMPTY_STRING);
	leaseType = EMPTY_STRING;
	$('#accountIDPrincipal').prop('readonly', false);
	$('#accountIDInterest').prop('readonly', false);
	$('#accountIDUnearned').prop('readonly', false);
}

function doclearfields(id) {
	switch (id) {
	case 'customerID':
		if (isEmpty($('#customerID').val())) {
			$('#customerID').val(EMPTY_STRING);
			clearFields();
		}
		break;
	}
}

function doHelp(id) {
	switch (id) {
	case 'customerID':
		help('COMMON', 'HLP_CUSTOMER_ID', $('#customerID').val(), EMPTY_STRING, $('#customerID'));
		break;
	case 'product':
		help('COMMON', 'HLP_LEASE_PROD_CODE', $('#product').val(), EMPTY_STRING, $('#product'));
		break;
	case 'portfolioCode':
		help('COMMON', 'HLP_PORTFOLIO_CODE', $('#portfolioCode').val(), EMPTY_STRING, $('#portfolioCode'));
		break;
	case 'regionCode':
		help('COMMON', 'HLP_REGION_CODE', $('#regionCode').val(), EMPTY_STRING, $('#regionCode'));
		break;
	case 'state':
		help('COMMON', 'HLP_STATE_CODE', $('#state').val(), EMPTY_STRING, $('#state'));
		break;
	case 'managerStaffID':
		help('COMMON', 'HLP_STAFF_CODE', $('#managerStaffID').val(), EMPTY_STRING, $('#managerStaffID'));
		break;
	}
}

function validate(id) {
	var valMode = true;
	switch (id) {
	case 'customerID':
		customerID_val();
		break;
	case 'customerName':
		customerName_val();
		break;
	case 'product':
		product_val(valMode);
		break;
	case 'portfolioCode':
		portfolioCode_val(valMode);
		break;
	case 'debtorAccount':
		debtorAccount_val(valMode);
		break;
	case 'accountIDPrincipal':
		accountIDPrincipal_val(valMode);
		break;
	case 'accountIDInterest':
		accountIDInterest_val(valMode);
		break;
	case 'accountIDUnearned':
		accountIDUnearned_val(valMode);
		break;
	case 'panNo':
		panNo_val();
		break;
	case 'regionCode':
		regionCode_val();
		break;
	case 'regOfficeAddress':
		regOfficeAddress_val();
		break;
	case 'pinCode':
		pinCode_val();
		break;
	case 'state':
		state_val();
		break;
	case 'managerStaffID':
		managerStaffID_val();
		break;
	}
}

function backtrack(id) {
	switch (id) {
	case 'customerID':
		setFocusLast('customerID');
		break;
	case 'customerName':
		if ($('#rectify').val() == COLUMN_ENABLE)
			setFocusLast('customerID');
		else
			setFocusLast('customerName');
		break;
	case 'productDetails':
		setFocusLast('customerName');
		break;
	case 'product':
		closeInlinePopUp(win);
		setFocusLast('productDetails');
		break;
	case 'portfolioCode':
		setFocusLast('product');
		break;
	case 'debtorAccount':
		setFocusLast('portfolioCode');
		break;
	case 'accountIDPrincipal':
		setFocusLast('debtorAccount');
		break;
	case 'accountIDInterest':
		setFocusLast('accountIDPrincipal');
		break;
	case 'accountIDUnearned':
		setFocusLast('accountIDInterest');
		break;
	case 'productDtl':
		if (leaseType != 'O')
			setFocusLast('accountIDUnearned');
		else
			setFocusLast('debtorAccount');
		break;
	case 'panNo':
		setFocusLast('productDetails');
		break;
	case 'regionCode':
		setFocusLast('panNo');
		break;
	case 'regOfficeAddress':
		setFocusLast('regionCode');
		break;
	case 'pinCode':
		setFocusLast('regOfficeAddress');
		break;
	case 'state':
		setFocusLast('pinCode');
		break;
	case 'managerStaffID':
		setFocusLast('state');
		break;
	}
}

function customerID_val() {
	var value = $('#customerID').val();
	clearError('customerID_error');
	if (isEmpty(value)) {
		setError('customerID_error', MANDATORY);
		return false;
	}
	if (!isNumeric(value)) {
		setError('customerID_error', NUMERIC_CHECK);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('CUSTOMER_ID', value);
		validator.setValue(ACTION, $('#action').val());
		validator.setClass('patterns.config.web.forms.reg.ecustomerregbean');
		validator.setMethod('validateCustomerID');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('customerID_error', validator.getValue(ERROR));
			return false;
		} else {
			PK_VALUE = getEntityCode() + PK_SEPERATOR + $('#customerID').val();
			if (!loadPKValues(PK_VALUE, 'customerID_error')) {
				return false;
			}
		}
	}
	setFocusLast('customerName');
	return true;
}

function customerName_val() {
	var value = $('#customerName').val();
	clearError('customerName_error');
	if (isEmpty(value)) {
		setError('customerName_error', MANDATORY);
		return false;
	}
	if (!isValidName(value)) {
		setError('customerName_error', INVALID_NAME);
		return false;
	}
	setFocusLast('productDetails');
	return true;
}

function product_val(valMode) {
	var value = $('#product').val();
	clearError('product_error');
	if (isEmpty(value)) {
		setError('product_error', MANDATORY);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('LEASE_PRODUCT_CODE', value);
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.reg.ecustomerregbean');
		validator.setMethod('validateProductCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('product_error', validator.getValue(ERROR));
			$('#product_desc').html(EMPTY_STRING);
			return false;
		}
		$('#product_desc').html(validator.getValue('DESCRIPTION'));
		leaseType = validator.getValue('LEASE_TYPE');
		checkProductType();
	}
	if (valMode)
		setFocusLast('portfolioCode');
	return true;
}
function checkProductType() {
	if (leaseType == 'O') {
		$('#accountIDPrincipal').prop('readonly', true);
		$('#accountIDInterest').prop('readonly', true);
		$('#accountIDUnearned').prop('readonly', true);
	} else {
		$('#accountIDPrincipal').prop('readonly', false);
		$('#accountIDInterest').prop('readonly', false);
		$('#accountIDUnearned').prop('readonly', false);
	}
}

function portfolioCode_val(valMode) {
	var value = $('#portfolioCode').val();
	clearError('portfolioCode_error');
	if (isEmpty(value)) {
		setError('portfolioCode_error', MANDATORY);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('PORTFOLIO_CODE', value);
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.reg.ecustomerregbean');
		validator.setMethod('validatePortfolioCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('portfolioCode_error', validator.getValue(ERROR));
			$('#portfolioCode_desc').html(EMPTY_STRING);
			return false;
		}
		$('#portfolioCode_desc').html(validator.getValue('DESCRIPTION'));
	}
	if (valMode)
		setFocusLast('debtorAccount');
	return true;
}

function debtorAccount_val(valMode) {
	var value = $('#debtorAccount').val();
	clearError('debtorAccount_error');
	if (isEmpty(value)) {
		setError('debtorAccount_error', MANDATORY);
		return false;
	}
	if (!isValidOtherInformation10(value)) {
		setError('debtorAccount_error', LENGTH_EXCEED_10);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('CUSTOMER_ID', $('#customerID').val());
	validator.setValue('PROGRAM_ID', CURRENT_PROGRAM_ID);
	validator.setValue('SUNDRY_DB_AC', value);
	validator.setValue(ACTION, USAGE);
	validator.setClass('patterns.config.web.forms.reg.ecustomerregbean');
	validator.setMethod('validateDebtorAccount');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('debtorAccount_error', validator.getValue(ERROR));
		return false;
	}
	if (valMode) {
		if (leaseType != 'O')
			setFocusLast('accountIDPrincipal');
		else
			setFocusLast('productDtl');
	}
	return true;
}

function accountIDPrincipal_val(valMode) {
	if (leaseType != 'O') {
		var value = $('#accountIDPrincipal').val();
		clearError('accountIDPrincipal_error');
		if (isEmpty(value)) {
			setError('accountIDPrincipal_error', MANDATORY);
			return false;
		}
		if (!isValidOtherInformation10(value)) {
			setError('accountIDPrincipal_error', LENGTH_EXCEED_10);
			return false;
		}
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('CUSTOMER_ID', $('#customerID').val());
		validator.setValue('PROGRAM_ID', CURRENT_PROGRAM_ID);
		validator.setValue('PRINCIPAL_AC', value);
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.reg.ecustomerregbean');
		validator.setMethod('validatePrincipalAccount');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('accountIDPrincipal_error', validator.getValue(ERROR));
			return false;
		}
	}
	if (valMode)
		setFocusLast('accountIDInterest');
	return true;
}

function accountIDInterest_val(valMode) {
	if (leaseType != 'O') {
		var value = $('#accountIDInterest').val();
		clearError('accountIDInterest_error');
		if (isEmpty(value)) {
			setError('accountIDInterest_error', MANDATORY);
			return false;
		}
		if (!isValidOtherInformation10(value)) {
			setError('accountIDInterest_error', LENGTH_EXCEED_10);
			return false;
		}
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('CUSTOMER_ID', $('#customerID').val());
		validator.setValue('PROGRAM_ID', CURRENT_PROGRAM_ID);
		validator.setValue('INTEREST_AC', value);
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.reg.ecustomerregbean');
		validator.setMethod('validateAccountIDInterest');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('accountIDInterest_error', validator.getValue(ERROR));
			return false;
		}
	}
	if (valMode)
		setFocusLast('accountIDUnearned');
	return true;
}

function accountIDUnearned_val(valMode) {
	if (leaseType != 'O') {
		var value = $('#accountIDUnearned').val();
		clearError('accountIDUnearned_error');
		if (isEmpty(value)) {
			setError('accountIDUnearned_error', MANDATORY);
			return false;
		}
		if (!isValidOtherInformation10(value)) {
			setError('accountIDUnearned_error', LENGTH_EXCEED_10);
			return false;
		}
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('CUSTOMER_ID', $('#customerID').val());
		validator.setValue('PROGRAM_ID', CURRENT_PROGRAM_ID);
		validator.setValue('UNEARNED_INTEREST_AC', value);
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.reg.ecustomerregbean');
		validator.setMethod('validateUnearnedInterestAccount');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('accountIDUnearned_error', validator.getValue(ERROR));
			return false;
		}
	}
	if (valMode)
		setFocusLast('productDtl');
	return true;
}

function panNo_val() {
	var value = $('#panNo').val();
	clearError('panNo_error');
	if (isEmpty(value)) {
		setError('panNo_error', MANDATORY);
		return false;
	}
	if (!isValidPanNumber(value)) {
		setError('panNo_error', INVALID_PAN_NO);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('CUSTOMER_ID', $('#customerID').val());
	validator.setValue('PROGRAM_ID', CURRENT_PROGRAM_ID);
	validator.setValue('PAN_NO', value);
	validator.setValue(ACTION, USAGE);
	validator.setClass('patterns.config.web.forms.reg.ecustomerregbean');
	validator.setMethod('validatePanNo');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('panNo_error', validator.getValue(ERROR));
		return false;
	}
	setFocusLast('regionCode');
	return true;

}
function regionCode_val() {
	var value = $('#regionCode').val();
	clearError('regionCode_error');
	if (isEmpty(value)) {
		setError('regionCode_error', MANDATORY);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('REGION_CODE', value);
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.reg.ecustomerregbean');
		validator.setMethod('validateRegionCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('regionCode_error', validator.getValue(ERROR));
			$('#regionCode_desc').html(EMPTY_STRING);
			return false;
		}
		$('#regionCode_desc').html(validator.getValue('DESCRIPTION'));
	}
	setFocusLast('regOfficeAddress');
	return true;
}

function regOfficeAddress_val() {
	var value = $('#regOfficeAddress').val();
	clearError('regOfficeAddress_error');
	if (isEmpty(value)) {
		setError('regOfficeAddress_error', MANDATORY);
		return false;
	}
	if (!isValidAddress(value)) {
		setError('regOfficeAddress_error', INVALID_ADDRESS);
		return false;
	}
	setFocusLast('pinCode');
	return true;

}

function pinCode_val() {
	var value = $('#pinCode').val();
	clearError('pinCode_error');
	if (isEmpty(value)) {
		setError('pinCode_error', MANDATORY);
		return false;
	}
	if (!isNumeric(value)) {
		setError('pinCode_error', NUMERIC_CHECK);
		return false;
	}
	if (isNegative(value)) {
		setError('pinCode_error', POSITIVE_CHECK);
		return false;
	}
	if (!isLength(value, 6)) {
		setError('pinCode_error', PINCODE_LENGTH);
		return false;
	}
	setFocusLast('state');
	return true;

}

function state_val() {
	var value = $('#state').val();
	clearError('state_error');
	if (isEmpty(value)) {
		setError('state_error', MANDATORY);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('STATE_CODE', value);
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.reg.ecustomerregbean');
		validator.setMethod('validateStateCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('state_error', validator.getValue(ERROR));
			$('#state_desc').html(EMPTY_STRING);
			return false;
		}
		$('#state_desc').html(validator.getValue('DESCRIPTION'));
	}
	setFocusLast('managerStaffID');
	return true;
}

function managerStaffID_val() {
	var value = $('#managerStaffID').val();
	clearError('managerStaffID_error');
	if (isEmpty(value)) {
		setError('managerStaffID_error', MANDATORY);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('STAFF_CODE', value);
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.reg.ecustomerregbean');
		validator.setMethod('validateManagerStaffID');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('managerStaffID_error', validator.getValue(ERROR));
			$('#managerStaffID_desc').html(EMPTY_STRING);
			return false;
		}
		$('#managerStaffID_desc').html(validator.getValue('NAME'));
	}
	setFocusOnSubmit();
	return true;
}

function updateCustomerProductDetails() {
	if (ecustomerregproductDetails_gridDuplicateCheck() && productGridrevalidate()) {
		var linkValue = "<a href='javascript:void(0);' onclick=ecustomerreg_editGrid('" + $('#debtorAccount').val() + "')>Edit</a><span>  </span> <a href='javascript:void(0);' onclick=deleteGridRow('" + $('#product').val() + "');>Delete</a>";
		var field_values = [ '', linkValue, $('#product').val(), $('#portfolioCode').val(), $('#debtorAccount').val(), $('#accountIDPrincipal').val(), $('#accountIDInterest').val(), $('#accountIDUnearned').val(), leaseType, $('#product_desc').html(), $('#portfolioCode_desc').html() ];
		var uid = _grid_updateRow(ecustomerregproductDetails_innerGrid, field_values);
		ecustomerregproductDetails_innerGrid.changeRowId(uid, $('#debtorAccount').val());
		clearProductDetailsGrid();
		closeInlinePopUp(win);
		setFocusLast('productDetails');
		return true;
	}
}

function ecustomerregproductDetails_gridDuplicateCheck() {
	
	var currentValue = [ $('#debtorAccount').val() ];
	if (!_grid_duplicate_check(ecustomerregproductDetails_innerGrid, currentValue, [ 4 ])) {
		setError('debtorAccount_error', DUPLICATE_DATA);
		return false;
	}
	if (!isEmpty($('#accountIDPrincipal').val())) {
		var currentValue = [ $('#accountIDPrincipal').val() ];
		if (!_grid_duplicate_check(ecustomerregproductDetails_innerGrid, currentValue, [ 5 ])) {
			setError('accountIDPrincipal_error', DUPLICATE_DATA);
			return false;
		}
	}
	if (!isEmpty($('#accountIDInterest').val())) {
		var currentValue = [ $('#accountIDInterest').val() ];
		if (!_grid_duplicate_check(ecustomerregproductDetails_innerGrid, currentValue, [ 6 ])) {
			setError('accountIDInterest_error', DUPLICATE_DATA);
			return false;
		}
	}
	if (!isEmpty($('#accountIDUnearned').val())) {
		var currentValue = [ $('#accountIDUnearned').val() ];
		if (!_grid_duplicate_check(ecustomerregproductDetails_innerGrid, currentValue, [ 7 ])) {
			setError('accountIDUnearned_error', DUPLICATE_DATA);
			return false;
		}
	}
	return true;
}

function deleteGridRow(row) {
	if (confirm(DELETE_CONFIRM)) {
		ecustomerregproductDetails_innerGrid.setUserData("", "grid_edit_id", EMPTY_STRING);
		ecustomerregproductDetails_innerGrid.deleteRow(row);
		clearProductDetailsGrid();
		setFocusLast('product');
	}
}

function ecustomerreg_editGrid(rowId) {
	clearProductDetailsGrid();
	addCustomerProductDetails();
	ecustomerregproductDetails_innerGrid.setUserData("", "grid_edit_id", rowId);
	$('#product').val(ecustomerregproductDetails_innerGrid.cells(rowId, 2).getValue());
	$('#portfolioCode').val(ecustomerregproductDetails_innerGrid.cells(rowId, 3).getValue());
	$('#debtorAccount').val(ecustomerregproductDetails_innerGrid.cells(rowId, 4).getValue());
	$('#accountIDPrincipal').val(ecustomerregproductDetails_innerGrid.cells(rowId, 5).getValue());
	$('#accountIDInterest').val(ecustomerregproductDetails_innerGrid.cells(rowId, 6).getValue());
	$('#accountIDUnearned').val(ecustomerregproductDetails_innerGrid.cells(rowId, 7).getValue());
	leaseType = ecustomerregproductDetails_innerGrid.cells(rowId, 8).getValue();
	$('#product_desc').html(ecustomerregproductDetails_innerGrid.cells(rowId, 9).getValue());
	$('#portfolioCode_desc').html(ecustomerregproductDetails_innerGrid.cells(rowId, 11).getValue());
	checkProductType();
	setFocusLast('product');
}

function clearCustomerProductDetails() {
	if (confirm(CLEAR_CONFIRM)) {
		ecustomerregproductDetails_innerGrid.setUserData("", "grid_edit_id", EMPTY_STRING);
		ecustomerregproductDetails_innerGrid.clearAll();
		clearProductDetailsGrid();
		setFocusLast('productDetails');
	}
}

function clearProductDetails() {
	ecustomerregproductDetails_innerGrid.setUserData("", "grid_edit_id", EMPTY_STRING);
	clearProductDetailsGrid();
	setFocusLast('product');
}

function onloadInlinePopUp(param, id) {
	switch (id) {
	case 'productDetails_Fields':
		clearProductDetailsGrid();
		show('productDetails_Fields');
		setFocusLast('product');
		break;
	}

}

function oncloseInlinePopUp(win) {
	switch (win._idd) {
	case 'productDetails_Fields':
		$('#xmlproductDetailGrid').val(ecustomerregproductDetails_innerGrid.serialize());
		ecustomerregproductDetails_innerGrid.setUserData("", "grid_edit_id", EMPTY_STRING);
		clearProductDetailsGrid();
		setFocusLast('panNo');
		break;
	}
	return true;
}

function gridExit(id) {
	switch (id) {
	case 'product':
	case 'portfolioCode':
	case 'debtorAccount':
	case 'accountIDPrincipal':
	case 'accountIDInterest':
	case 'accountIDUnearned':
		if (ecustomerregproductDetails_innerGrid.getRowsNum() > 0) {
			$('#xmlproductDetailGrid').val(ecustomerregproductDetails_innerGrid.serialize());
			$('#product_error').html(EMPTY_STRING);
			$('#gridError_error').html(EMPTY_STRING);
			closeInlinePopUp(win);
			setFocusLast('panNo');
		} else {
			ecustomerregproductDetails_innerGrid.clearAll();
			$('#xmlproductDetailGrid').val(ecustomerregproductDetails_innerGrid.serialize());
			setError('product_error', ADD_ATLEAST_ONE_ROW);
			setError('gridError_error', ADD_ATLEAST_ONE_ROW);
			setFocusLast('product');
		}
		break;
	}
}

function productGridrevalidate() {
	var errors = 0;
	if (!product_val(false)) {
		errors++;
	}
	if (!portfolioCode_val(false)) {
		errors++;
	}
	if (!debtorAccount_val(false)) {
		errors++;
	}
	if (!accountIDPrincipal_val(false)) {
		errors++;
	}
	if (!accountIDInterest_val(false)) {
		errors++;
	}
	if (!accountIDUnearned_val(false)) {
		errors++;
	}
	if (errors > 0) {
		if (win)
			win.showInnerScroll();
		return false;
	} else
		return true;
}

function revalidate() {
	revaildateGrid();
}
function revaildateGrid() {
	if (ecustomerregproductDetails_innerGrid.getRowsNum() > 0) {
		ecustomerregproductDetails_innerGrid.setSerializationLevel(false, false, false, false, false, true);
		$('#xmlproductDetailGrid').val(ecustomerregproductDetails_innerGrid.serialize());
		$('#product_error').html(EMPTY_STRING);
		$('#gridError_error').html(EMPTY_STRING);
	} else {
		ecustomerregproductDetails_innerGrid.clearAll();
		$('#xmlproductDetailGrid').val(ecustomerregproductDetails_innerGrid.serialize());
		setError('product_error', ADD_ATLEAST_ONE_ROW);
		setError('gridError_error', ADD_ATLEAST_ONE_ROW);
		errors++;
	}
}

function closePopup(value) {
	if (value == 0) {
		closeInlinePopUp(win);
		ecustomerregproductDetails_innerGrid.setUserData("", "grid_edit_id", EMPTY_STRING);
		setFocusLast('productDetails');
	}
	return true;
}
