<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.reg.Messages"
	var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/reg/ecustomeraddcon.js" />
	</web:dependencies>
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="ecustomeraddcon.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/reg/ecustomeraddcon" id="ecustomeraddcon"
				method="POST">
				<web:dividerBlock>
				</web:dividerBlock>
				<web:viewContent id="po_view1">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle width="350px" />
										<web:columnStyle width="100px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="ecustomeraddcon.lesseecode" var="program"
												mandatory="false" />
										</web:column>
										<web:column>
											<type:lesseeCode property="lesseeCode" id="lesseeCode" />
										</web:column>
										<web:column>
											<web:legend key="ecustomeraddcon.customerid" var="program"
												mandatory="false" />
										</web:column>
										<web:column>
											<type:customerID property="customerID" id="customerID" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
						</web:sectionBlock>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowOdd>
									<web:column>
										<web:legend key="ecustomeraddcon.customername" var="program"
											mandatory="false" />
									</web:column>
									<web:column>
										<type:descriptionDisplay property="customerName"
											id="customerName" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<type:reset id="reset" key="ecustomeraddcon.reset"
											var="program" />
									</web:column>
									<web:column>
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:section>
							<web:viewTitle var="program" key="ecustomeraddcon.address" />
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="180px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowOdd>
									<web:column>
										<web:button id="addressnew" key="ecustomeraddcon.addnew"
											var="program" onclick="addCustomerAddressDetails()" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column span="2">
										<web:grid height="250px" width="1104px"
											id="ecustomeraddcon_innerGrid"
											src="reg/ecustomeraddcon_innerGrid.xml">
										</web:grid>
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:viewContent id="contact" styleClass="hidden">
							<web:section>
								<web:viewTitle var="program" key="ecustomeraddcon.contact" />
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="120px" />
										<web:columnStyle width="120px" />
										<web:columnStyle width="120px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="ecustomeraddcon.foraddressserial"
												var="program" mandatory="false" />
										</web:column>
										<web:column>
											<type:addressSerialDisplay property="forAddressSerial"
												id="forAddressSerial" />
										</web:column>
										<web:column>
											<web:legend key="ecustomeraddcon.effectivesince"
												var="program" mandatory="false" />
										</web:column>
										<web:column>
											<type:dateDisplay property="effectiveSince"
												id="effectiveSince" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>

							<web:section>
								<web:table>

									<web:rowOdd>
										<web:column span="2">
										<web:button id="contactnew" key="ecustomeraddcon.contactNew"
												var="program" onclick="addCustomerContactDetails()" />
											<web:button id="editcontact"
												key="ecustomeraddcon.editContact" var="program"
												onclick="editCustomerContactDetails()" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:grid height="210px" width="722px"
												id="ecustomercon_innerGrid"
												src="reg/ecustomercon_innerGrid.xml">
											</web:grid>
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
						</web:viewContent>
					</web:dividerBlock>
				</web:viewContent>
				<web:element property="command" />
				<web:element property="action" />
				<web:element property="addresspk" />
				<web:element property="contactpk" />
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
	<web:contextSearch />
</web:fragment>
