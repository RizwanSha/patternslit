var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'ECUSTOMERREG';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}
function clearFields() {
	$('#customerID').val(EMPTY_STRING);
	$('#customerID_desc').html(EMPTY_STRING);
	$('#customerName').val(EMPTY_STRING);
	$('#dateOfReg').val(EMPTY_STRING);
	$('#panNo').val(EMPTY_STRING);
	$('#regOfficeAddress').val(EMPTY_STRING);
	$('#pinCode').val(EMPTY_STRING);
	$('#state').val(EMPTY_STRING);
	$('#state_desc').html(EMPTY_STRING);
	$('#managerStaffID').val(EMPTY_STRING);
	$('#regionCode').val(EMPTY_STRING);
	$('#regionCode_desc').html(EMPTY_STRING);
	ecustomerregproductDetails_innerGrid.clearAll();
}

function loadData() {
	$('#customerID').val(validator.getValue('CUSTOMER_ID'));
	$('#customerName').val(validator.getValue('CUSTOMER_NAME'));
	$('#dateOfReg').val(validator.getValue('DATE_OF_REG'));
	$('#panNo').val(validator.getValue('PAN_NO'));
	$('#regOfficeAddress').val(validator.getValue('REG_ADDRESS'));
	$('#pinCode').val(validator.getValue('PIN_CODE'));
	$('#regionCode').val(validator.getValue('REGION_CODE'));
	$('#regionCode_desc').html(validator.getValue('F1_DESCRIPTION'));
	$('#state').val(validator.getValue('STATE_CODE'));
	$('#state_desc').html(validator.getValue('F2_DESCRIPTION'));
	$('#managerStaffID').val(validator.getValue('STAFF_CODE'));
	$('#managerStaffID_desc').html(validator.getValue('F3_NAME'));
	loadAuditFields(validator);
	loadProductDetailGrid();
}
function clearProductDetailsGrid() {
	$('#product').val(EMPTY_STRING);
	$('#product_desc').html(EMPTY_STRING);
	$('#portfolioCode').val(EMPTY_STRING);
	$('#portfolioCode_desc').html(EMPTY_STRING);
	$('#debtorAccount').val(EMPTY_STRING);
	$('#accountIDPrincipal').val(EMPTY_STRING);
	$('#accountIDInterest').val(EMPTY_STRING);
	$('#accountIDUnearned').val(EMPTY_STRING);
}
function loadProductDetailGrid() {
	loadGridQuery(CURRENT_PROGRAM_ID, 'ECUSTOMERREG_PRODUCT_GRID', productDetailPostProcessing);
}

function productDetailPostProcessing(result) {
	ecustomerregproductDetails_innerGrid.parse(result);
	ecustomerregproductDetails_innerGrid.setColumnHidden(0, true);
	var noofrows = ecustomerregproductDetails_innerGrid.getRowsNum();
	if (noofrows > 0) {
		for (var i = 0; i < noofrows; i++) {
			var productCode = ecustomerregproductDetails_innerGrid.cells2(i, 2).getValue();
			var linkValue = "<a href='javascript:void(0);' onclick=ecustomerreg_editGrid('" + productCode + "')>VIEW</a>";
			ecustomerregproductDetails_innerGrid.cells2(i, 1).setValue(linkValue);
			ecustomerregproductDetails_innerGrid.changeRowId(ecustomerregproductDetails_innerGrid.getRowId(i), productCode);
		}
	}
}


function ecustomerreg_editGrid(rowId) {
	clearProductDetailsGrid();
	addCustomerProductDetails();
	$('#product').val(ecustomerregproductDetails_innerGrid.cells(rowId, 2).getValue());
	$('#portfolioCode').val(ecustomerregproductDetails_innerGrid.cells(rowId, 3).getValue());
	$('#debtorAccount').val(ecustomerregproductDetails_innerGrid.cells(rowId, 4).getValue());
	$('#accountIDPrincipal').val(ecustomerregproductDetails_innerGrid.cells(rowId, 5).getValue());
	$('#accountIDInterest').val(ecustomerregproductDetails_innerGrid.cells(rowId, 6).getValue());
	$('#accountIDUnearned').val(ecustomerregproductDetails_innerGrid.cells(rowId, 7).getValue());
	$('#product_desc').html(ecustomerregproductDetails_innerGrid.cells(rowId, 9).getValue());
	$('#portfolioCode_desc').html(ecustomerregproductDetails_innerGrid.cells(rowId, 10).getValue());
}

function addCustomerProductDetails() {
	win = showWindow('productDetails_Fields', EMPTY_STRING, CUSTOMER_PRODUCT_DETAILS, EMPTY_STRING, true, false, false, 550, 340);
}

function closePopup(value) {
	if (value == 0) {
		closeInlinePopUp(win);
	}
	return true;
}
