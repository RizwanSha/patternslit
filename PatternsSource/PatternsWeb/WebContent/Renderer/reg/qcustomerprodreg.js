var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'ECUSTOMERPRODREG';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}
function clearFields() {
	$('#customerID').val(EMPTY_STRING);
	$('#customerID_desc').html(EMPTY_STRING);
	$('#customerName').val(EMPTY_STRING);
	$('#dateOfReg').val(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	ecustomerprodreg_productGrid.clearAll();
}

function loadData() {
	$('#customerID').val(validator.getValue('CUSTOMER_ID'));
	$('#entrySl').val(validator.getValue('ENTRY_SL'));
	$('#customerName').val(validator.getValue('F1_CUSTOMER_NAME'));
	$('#dateOfReg').val(validator.getValue('F1_DATE_OF_REG'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadAuditFields(validator);
	loadProductDetailGrid();
}
function clearProductDetailsGrid() {
	$('#product').val(EMPTY_STRING);
	$('#product_desc').html(EMPTY_STRING);
	$('#portfolioCode').val(EMPTY_STRING);
	$('#portfolioCode_desc').html(EMPTY_STRING);
	$('#debtorAccount').val(EMPTY_STRING);
	$('#accountIDPrincipal').val(EMPTY_STRING);
	$('#accountIDInterest').val(EMPTY_STRING);
	$('#accountIDUnearned').val(EMPTY_STRING);
}
function loadProductDetailGrid() {
	loadGridQuery(CURRENT_PROGRAM_ID, 'ECUSTOMERPRODREG_PRODUCT_GRID', productDetailPostProcessing);
}

function productDetailPostProcessing(result) {
	ecustomerprodreg_productGrid.parse(result);
	ecustomerprodreg_productGrid.setColumnHidden(0, true);
	var noofrows = ecustomerprodreg_productGrid.getRowsNum();
	if (noofrows > 0) {
		for (var i = 0; i < noofrows; i++) {
			var productCode = ecustomerprodreg_productGrid.cells2(i, 2).getValue();
			var linkValue = "<a href='javascript:void(0);' onclick=viewProductFields('" + productCode + "')>VIEW</a>";
			ecustomerprodreg_productGrid.cells2(i, 1).setValue(linkValue);
			ecustomerprodreg_productGrid.changeRowId(ecustomerprodreg_productGrid.getRowId(i), productCode);
		}
	}
}


function viewProductFields(rowId) {
	clearProductDetailsGrid();
	viewProductDetails();
	$('#product').val(ecustomerprodreg_productGrid.cells(rowId, 2).getValue());
	$('#portfolioCode').val(ecustomerprodreg_productGrid.cells(rowId, 3).getValue());
	$('#debtorAccount').val(ecustomerprodreg_productGrid.cells(rowId, 4).getValue());
	$('#accountIDPrincipal').val(ecustomerprodreg_productGrid.cells(rowId, 5).getValue());
	$('#accountIDInterest').val(ecustomerprodreg_productGrid.cells(rowId, 6).getValue());
	$('#accountIDUnearned').val(ecustomerprodreg_productGrid.cells(rowId, 7).getValue());
	$('#product_desc').html(ecustomerprodreg_productGrid.cells(rowId, 9).getValue());
	$('#portfolioCode_desc').html(ecustomerprodreg_productGrid.cells(rowId, 10).getValue());
}

function viewProductDetails() {
	win = showWindow('productDetails_Fields', EMPTY_STRING, CUSTOMER_PRODUCT_DETAILS, EMPTY_STRING, true, false, false, 550, 340);
}

function closePopup(value) {
	if (value == 0) {
		closeInlinePopUp(win);
	}
	return true;
}
