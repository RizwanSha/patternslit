<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.reg.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/reg/qcustomerreg.js" />
	</web:dependencies>
	<web:loadPK />
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="qcustomerreg.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:viewContent id="po_view1">
				<web:dividerBlock>
					<web:sectionBlock width="width-100p" align="left">
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="ecustomerreg.customerid" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:customerIDDisplay property="customerID" id="customerID" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowOdd>
									<web:column>
										<web:legend key="ecustomerreg.name" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:descriptionDisplay property="customerName" id="customerName" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="ecustomerreg.dateofreg" var="program" />
									</web:column>
									<web:column>
										<type:dateDisplay property="dateOfReg" id="dateOfReg" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:sectionTitle var="program" key="ecustomerreg.custprodsection" />
						<web:viewContent id="productDetails_Fields" styleClass="form-horizontal hidden">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="ecustomerreg.product" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:leaseProductCodeDisplay property="product" id="product" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="ecustomerreg.portfoliocode" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:portfolioCodeDisplay property="portfolioCode" id="portfolioCode" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="ecustomerreg.sundrydebtoraccount" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:otherInformation10Display property="debtorAccount" id="debtorAccount" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="ecustomerreg.accidprincipal" var="program" />
										</web:column>
										<web:column>
											<type:otherInformation10Display property="accountIDPrincipal" id="accountIDPrincipal" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="ecustomerreg.accidinterest" var="program" />
										</web:column>
										<web:column>
											<type:otherInformation10Display property="accountIDInterest" id="accountIDInterest" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="ecustomerreg.accidunearned" var="program" />
										</web:column>
										<web:column>
											<type:otherInformation10Display property="accountIDUnearned" id="accountIDUnearned" />
										</web:column>
									</web:rowEven>
									<web:column span="2">
										<p align="left">
											<web:button key="form.close" id="close" var="common" onclick="closePopup('0')" />
										</p>
									</web:column>
								</web:table>
							</web:section>
						</web:viewContent>
						<web:section>
							<web:table>
								<web:rowOdd>
									<web:column>
										<web:grid height="240px" width="921px" id="ecustomerregproductDetails_innerGrid" src="reg/ecustomerregproductDetails_innerGrid.xml">
										</web:grid>
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="ecustomerreg.pan" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:otherInformation25Display property="panNo" id="panNo" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="ecustomerreg.regioncode" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:regionCodeDisplay property="regionCode" id="regionCode" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="ecustomerreg.address" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:remarksDisplay property="regOfficeAddress" id="regOfficeAddress" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="ecustomerreg.pin" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:pinCodeDisplay property="pinCode" id="pinCode" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="ecustomerreg.state" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:stateCodeDisplay property="state" id="state" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="ecustomerreg.managerstaffid" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:staffRoleCodeDisplay property="managerStaffID" id="managerStaffID" />
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
						<web:auditDisplay />
					</web:sectionBlock>
				</web:dividerBlock>
			</web:viewContent>
		</web:viewSubPart>
	</web:viewPart>
</web:fragment>
