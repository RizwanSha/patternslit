<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.reg.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/reg/qcustomeraddress.js" />
	</web:dependencies>
	<web:loadPK />
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="qcustomeraddress.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:viewContent id="po_view1">
				<web:dividerBlock>
					<web:sectionBlock width="width-100p" align="left">
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="ecustomeraddress.customerid" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:customerIDDisplay property="customerID" id="customerID" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowOdd>
									<web:column>
										<web:legend key="ecustomeraddress.addrserial" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:addressSerialDisplay property="addressSerial" id="addressSerial" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="ecustomeraddress.officeaddr" var="program" />
									</web:column>
									<web:column>
										<type:conciseDescriptionDisplay property="officeAddrIdentifier" id="officeAddrIdentifier" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="ecustomeraddress.addr" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:remarksDisplay property="address" id="address" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="ecustomeraddress.pin" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:pinCodeDisplay property="pinCode" id="pinCode" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="ecustomeraddress.state" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:stateCodeDisplay property="state" id="state" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="ecustomeraddress.tan" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:otherInformation25Display property="tanNo" id="tanNo" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="ecustomeraddress.removeaddr" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:checkboxDisplay property="removeAddress" id="removeAddress" />
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="form.remarks" var="common" />
									</web:column>
									<web:column>
										<type:remarksDisplay property="remarks" id="remarks" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:auditDisplay />
					</web:sectionBlock>
				</web:dividerBlock>
			</web:viewContent>
		</web:viewSubPart>
	</web:viewPart>
</web:fragment>
