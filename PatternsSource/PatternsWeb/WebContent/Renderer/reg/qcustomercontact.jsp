<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.reg.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/reg/qcustomercontact.js" />
	</web:dependencies>
	<web:loadPK />
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="ecustomercontact.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:viewContent id="po_view1">
				<web:dividerBlock>
					<web:sectionBlock width="width-100p" align="left">
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="ecustomercontact.customerid" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:customerIDDisplay property="customerID" id="customerID" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowOdd>
									<web:column>
										<web:legend key="ecustomercontact.addrsl" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:addressSerialDisplay property="addressSerial" id="addressSerial" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="form.effectivedate" var="common" mandatory="true" />
									</web:column>
									<web:column>
										<type:dateDisplay property="effectiveDate" id="effectiveDate" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:viewContent id="contactDetails_Fields" styleClass="form-horizontal hidden">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="ecustomercontact.title" var="program" />
										</web:column>
										<web:column>
											<type:comboDisplay property="title" id="title" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="ecustomercontact.contactperson" var="program" />
										</web:column>
										<web:column>
											<type:descriptionDisplay property="contactPerson" id="contactPerson" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="ecustomercontact.contactdtl1" var="program" />
										</web:column>
										<web:column>
											<type:otherInformation20Display property="contactDetails1" id="contactDetails1" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="ecustomercontact.contactdtl2" var="program" />
										</web:column>
										<web:column>
											<type:otherInformation20Display property="contactDetails2" id="contactDetails2" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="ecustomercontact.emailid" var="program" />
										</web:column>
										<web:column>
											<type:descriptionDisplay property="emailID" id="emailID" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="ecustomercontact.alternateemail" var="program"  />
										</web:column>
										<web:column>
											<type:checkboxCenterAlignDisplay property="alternateEmail" id="alternateEmail" description="true"/>
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column span="2">
											<p align="left">
												<web:button key="form.close" id="close" var="common" onclick="closePopup('0')" />
											</p>
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
						</web:viewContent>
						<web:sectionTitle var="program" key="ecustomercontact.contactdetail" />
						<web:section>
							<web:table>
								<web:rowOdd>
									<web:column>
										<web:grid height="210px" width="1031px" id="ecustomercontact_innerGrid" src="reg/ecustomercontact_innerGrid.xml">
										</web:grid>
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="form.enabled" var="common" />
									</web:column>
									<web:column>
										<type:checkboxDisplay property="enabled" id="enabled" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="form.remarks" var="common" />
									</web:column>
									<web:column>
										<type:remarksDisplay property="remarks" id="remarks" />
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
						<web:auditDisplay />
					</web:sectionBlock>
				</web:dividerBlock>
			</web:viewContent>
		</web:viewSubPart>
	</web:viewPart>
</web:fragment>
