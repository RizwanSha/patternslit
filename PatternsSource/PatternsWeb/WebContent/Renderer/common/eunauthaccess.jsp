<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.common.Messages" var="program" />
<web:fragment>
	<web:scriptlet>
		function init(){
			if((self.parent && !(self.parent===self)) &&(self.parent.frames.length!=0)){
			    self.parent.location=document.location;
			}		
		}
	</web:scriptlet>
	<web:viewPartInline>
		<div class="navbar navbar-inverse navbar-fixed-top drop-shadow">
			<a class="navbar-brand"></a>
		</div>
		<web:viewSubPart>
			<web:form action="/Renderer/common/eunauthaccess" id="eunauthaccess" method="POST">
				<web:section>
					<web:table>

						<web:row>

							<table width="100%">
								<tr>
									<td style="padding: 5px"><input type="image" src="public/styles/images/failureImage.png" style="width: 110px; hight: 100px; border-collapse: collapse;"></td>

									<td><span style="font-size: 20px; color: lightcoral; font-weight: bold;"><web:infoMessage key="eunauthaccess.warning" var="program" /></span><br> <br> <span style="font-size: 14px; color: #2f7698;"><web:infoMessage var="program" key="eunauthaccess.warning1" /></span> <br>
										<span> <web:printRequestAdditionalInfo styleClass="font-size: 12px; " />
									</span> <span style="text-align: center;"> <a href="/LIT/Renderer/common/elogin.jsp"> <input type="button" id="relogin" name="relogin"
												style="align: center; font-size: 13px; background: #dce6f2; margin-top: 20px; text-align: center; width: 200px; font-weight: 600; font-weight: inherit;" class="btn btn-xs btn-default" value="CLICK HERE TO RE-LOGIN" onclick="" />
										</a></span> <script>
											$('#relogin').focus();
										</script>
								</tr>

							</table>
						</web:row>
					</web:table>
				</web:section>

				<!-- <web:section>
					<web:table>
						<web:row>
							<web:column>
								<web:infoMessage var="program" key="eunauthaccess.programtitle" styleClass="error" />
							</web:column>
						</web:row>
					</web:table>
				</web:section>
				<web:section>
					<web:table>
						<web:row>
							<web:column>
								<web:infoMessage var="program" key="eunauthaccess.message1" />
							</web:column>
						</web:row>
						<web:row>
							<web:column>
								<web:printRequestAdditionalInfo styleClass="error" />
							</web:column>
						</web:row>
						<web:row>
							<web:column>
								<web:infoMessage var="program" key="eunauthaccess.message2" />
							</web:column>
						</web:row>
						<web:row>
							<web:column>
								<web:infoMessage var="program" key="eunauthaccess.message3" />
							</web:column>
						</web:row>
						<web:row>
							<web:column>
								<web:infoMessage var="program" key="eunauthaccess.message4" />
							</web:column>
						</web:row>
						<web:row>
							<web:column>
								<web:infoMessage var="program" key="eunauthaccess.message5" />
							</web:column>
						</web:row>
						<web:row>
							<web:column>
							</web:column>
						</web:row>
						<web:row>
							<web:column>
								<web:infoMessage var="program" key="eunauthaccess.message6" />
							</web:column>
						</web:row>
						<web:row>
							<web:column>
								<web:infoMessage var="program" key="eunauthaccess.message7" />
							</web:column>
						</web:row>
						<web:row>
							<web:column>
								<web:infoMessage var="program" key="eunauthaccess.message8" />
							</web:column>
						</web:row>
					  <web:row>
							<web:column>
								<web:infoMessage var="program" key="eunauthaccess.message9" />
							</web:column>
						</web:row>
						<web:row>
							<web:column>
								<web:infoMessage var="program" key="eunauthaccess.message10" />
							</web:column>
						</web:row>	
						<web:row>
							<web:column>
								<web:linkMessage key="elogout.message4" var="program" forward="elogin" />
							</web:column>
						</web:row>
					</web:table>
				</web:section> -->
			</web:form>
			<web:footer />
		</web:viewSubPart>
	</web:viewPartInline>
</web:fragment>