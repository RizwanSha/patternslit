var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MCONTACT';
var rowId, recordType = EMPTY_STRING;
var returnType = EMPTY_STRING;
var sKey = EMPTY_STRING;
var sValue = EMPTY_STRING;
var parentAction = EMPTY_STRING;
var tabbarObject;
var addedOn = EMPTY_STRING, modifiedOn = EMPTY_STRING;
var currentSessionReq = true;
function init() {
	CGQM_CONV_ID = $('#conversationID').val();
	PARENT_PROGRAM_ID = $('#parentProgramId').val();
	LINK_PROGRAM_ID = CURRENT_PROGRAM_ID;
	PARENT_PRIMARY_KEY = $('#parentPrimaryKey').val();
	LINK_PRIMARY_KEY = EMPTY_STRING;
	SOURCE_VALUE = $('#requestType').val();
	returnType = $('#returnType').val();
	sKey = $('#sourceKey').val();
	sValue = $('#sourceValue').val();
	parentAction = $('#parentAction').val();
	detailGrid.clearAll();
	innerGrid.clearAll();
	if ($('#parentAction').val() == MODIFY) {
		currentSessionReq = false;
	}
	if (sKey == 'PERSON_ID') {
		contact.tabs('contact_1').enable();
		$('#person').removeClass('hidden');
		$('#offUse').removeClass('hidden');
		$('#personId').val(PARENT_PRIMARY_KEY);
		$('#currentTab').val('contact_0');
		$('#personSearch').prop('disabled', false);
		$('#official').prop('disabled', false);
	} else {
		LINK_PRIMARY_KEY = getEntityCode() + '|' + sValue;
		$('#person').addClass('hidden');
		$('#offUse').addClass('hidden');
		$('#personSearch').prop('disabled', true);
		$('#official').prop('disabled', true);
		contact.tabs('contact_1').hide(true);
		contact.tabs('contact_1').disable();
		innerGrid.setColumnHidden(4, true);

	}
	if (returnType == 'M') {
		$('#gridDetails').removeClass('hidden');
		loadGrid();
	} else if (returnType == 'S') {
		$('#gridDetails').addClass('hidden');
		loadRecord(sValue);
	}
	$('#action').val(ADD);
	$('#currentTab').val('contact_0');
	tabbarObject = contact;
	innerGrid.attachEvent("onMouseOver", function(rid, ind) {
		if ((ind == 7) || (ind == 8))
			rowId = rid;
	});
}

function loadGrid() {
	innerGrid.clearAll();
	LINK_PROGRAM_ID = CURRENT_PROGRAM_ID;
	if (sKey == 'PERSON_ID')
		PK_VALUE = getPartitionNo() + PK_SEPERATOR + PARENT_PRIMARY_KEY;
	else
		PK_VALUE = PARENT_PRIMARY_KEY;
	var TOKEN_ID = EMPTY_STRING;
	if (SOURCE_VALUE == MAIN) {
		TOKEN_ID = 'MCONTACT_GRID_M';
	} else if (SOURCE_VALUE == TBA) {
		TOKEN_ID = 'MCONTACT_GRID_T';
	} else {
		TOKEN_ID = 'MCONTACT_GRID_M';
	}
	if (currentSessionReq && PARENT_PRIMARY_KEY == EMPTY_STRING) {
		SOURCE_VALUE = 'S';
		var detailxml = window.parent.$("#xmlContactDetailList").val();
		if (detailxml != EMPTY_STRING && detailxml != null) {
			loadComponentGridQuery(PARENT_PROGRAM_ID, 'MCONTACT_GRID_P', latestdataProcessing);
		}
	} else {
		if (((SOURCE_VALUE == TBA) && (parentAction == 'M')) || SOURCE_VALUE == PROGRAM) {
			gridMerging(true);
		} else {
			loadComponentGridQuery(PARENT_PROGRAM_ID, TOKEN_ID, postProcessing);
		}
	}
}
function loadRecord(value) {
	if (value != EMPTY_STRING) {
		$('#action').val(MODIFY);
		LINK_PROGRAM_ID = CURRENT_PROGRAM_ID;
		args = getPartitionNo() + PK_SEPERATOR + value;
		currentSessionReq = true;
		loadLinkedRecordData(loadContactDetail, PARENT_PROGRAM_ID, 'MCONTACT_M', args);
	}
}
function loadContactDetail() {
	if (validator.getValue("CONTACT_TYPE") != EMPTY_STRING) {
		$('#contactInvNum').val(validator.getValue("CONTACT_INV_NO"));
		$('#contactType').val(validator.getValue("CONTACT_TYPE"));
		if (validator.getValue("CONTACT_TYPE") == 'L') {
			$('#landline').removeClass('hidden');
			$('#mobile').addClass('hidden');
			$('#email').addClass('hidden');
			smartCombo('COMMON', 'PHONECODES', '', $('#landlineCountry'));
			landlineCountry.setComboValue(validator.getValue("LL_COUNTRY_CODE"));
			$('#stdCode').val(validator.getValue("LL_LOC_STD_CODE"));
			$('#landlineNumber').val(validator.getValue("LL_CONTACT_NUMBER"));
		} else if (validator.getValue("CONTACT_TYPE") == 'M') {
			$('#landline').addClass('hidden');
			$('#mobile').removeClass('hidden');
			$('#email').addClass('hidden');
			smartCombo('COMMON', 'PHONECODES', '', $('#mobileCountry'));
			mobileCountry.setComboValue(validator.getValue("MOB_COUNTRY_CODE"));
			$('#mobileNumber').val(validator.getValue("MOB_CONTACT_NUMBER"));
		} else {
			$('#landline').addClass('hidden');
			$('#mobile').addClass('hidden');
			$('#email').removeClass('hidden');
			$('#emailId').val(validator.getValue("EMAIL_ID"));
		}
	}
	if (currentSessionReq) {
		SOURCE_VALUE = TBA;
		currentSessionReq = false;
		loadLinkedRecordData(loadContactDetail, CURRENT_PROGRAM_ID, 'MCONTACT_T', args);
	}
}
function gridMerging(isPersonRequest) {
	if (isPersonRequest) {
		loadComponentGridQuery(PARENT_PROGRAM_ID, 'MCONTACT_GRID_M', personGridProcessing);
	}
}

function personGridProcessing(result) {
	currentSessionReq = true;
	postProcessing(result);
	currentSessionReq = false;
	loadComponentGridQuery(PARENT_PROGRAM_ID, 'MCONTACT_GRID_T', dataProcessing);
}

function postProcessing(result) {
	$xml = $($.parseXML(result));
	($xml).find("row").each(function() {
		try {
			var conTypeH = $(this).find("cell")[1].textContent;
			var conType = $(this).find("cell")[2].textContent;
			var details = $(this).find("cell")[3].textContent;
			var official = $(this).find("cell")[4].textContent;
			var rowId = $(this).find("cell")[5].textContent;
			var addOn = $(this).find("cell")[6].textContent;
			var editLink = $(this).find("cell")[7].textContent;
			var removeLink = $(this).find("cell")[8].textContent;
			var recordType = $(this).find("cell")[9].textContent;
			var crOn = $(this).find("cell")[10].textContent;
			var modOn = $(this).find("cell")[11].textContent;

			var gridValues = [ '', conTypeH, conType, details, official, rowId, addOn, editLink, removeLink, recordType, crOn, modOn ];

			var values = [ rowId, decodeBTS(decodeOTB(official)), '', crOn, modOn ];
			innerGrid.addRow(rowId, gridValues);
			detailGrid.addRow(invNumber, values);
		} catch (e) {
		}
	});
	if (!currentSessionReq) {
		SOURCE_VALUE = 'S';
		var detailxml = window.parent.$("#xmlContactDetailList").val();
		if (detailxml != EMPTY_STRING && detailxml != null) {
			loadComponentGridQuery(PARENT_PROGRAM_ID, 'MCONTACT_GRID_P', latestdataProcessing);
		}
	}
}

function dataProcessing(result) {
	$xml = $($.parseXML(result));
	($xml).find("row").each(function() {
		try {
			var conTypeH = $(this).find("cell")[1].textContent;
			var conType = $(this).find("cell")[2].textContent;
			var details = $(this).find("cell")[3].textContent;
			var official = $(this).find("cell")[4].textContent;
			var rowId = $(this).find("cell")[5].textContent;
			var addOn = $(this).find("cell")[6].textContent;
			var editLink = $(this).find("cell")[7].textContent;
			var removeLink = $(this).find("cell")[8].textContent;
			var recordType = $(this).find("cell")[9].textContent;
			var crOn = $(this).find("cell")[10].textContent;
			var modOn = $(this).find("cell")[11].textContent;

			var gridValues = [ '', conTypeH, conType, details, official, rowId, addOn, editLink, removeLink, recordType, crOn, modOn ];
			var values = [ rowId, decodeBTS(decodeOTB(official)), '', crOn, modOn ];
			if (innerGrid.doesRowExist(rowId)) {
				innerGrid.deleteRow(rowId);
				innerGrid.addRow(rowId, gridValues);
				detailGrid.deleteRow(rowId);
				detailGrid.addRow(rowId, values);
			} else {
				innerGrid.addRow(rowId, gridValues);
				detailGrid.addRow(rowId, values);
			}
		} catch (e) {
		}
	});
	if (SOURCE_VALUE != MAIN) {
		SOURCE_VALUE = TBA;
		loadComponentGridQuery(PARENT_PROGRAM_ID, 'MCONTACT_RMVD_T', removedContactProcessing);
	}
	if (!currentSessionReq) {
		SOURCE_VALUE = 'S';
		var detailxml = window.parent.$("#xmlContactDetailList").val();
		if (detailxml != EMPTY_STRING && detailxml != null) {
			loadComponentGridQuery(PARENT_PROGRAM_ID, 'MCONTACT_GRID_P', latestdataProcessing);
		}
	}
}
function removedContactProcessing(result) {
	$xml = $($.parseXML(result));
	$xml.find("row").each(function() {
		try {
			var invNumber = $(this).find("cell")[1].textContent;
			if (innerGrid.doesRowExist(invNumber)) {
				innerGrid.deleteRow(invNumber);
			}
			if (detailGrid.doesRowExist(invNumber)) {
				detailGrid.cells(invNumber, 5).setValue('R');
			}
		} catch (e) {
		}
	});
	return;
}
function latestdataProcessing(result) {
	currentSessionReq = true;
	var detailxml = window.parent.$("#xmlContactDetailList").val();
	if (detailxml != EMPTY_STRING && detailxml != null) {
		dataProcessing(result);
		detailGrid.parse(detailxml);
		$xml = $($.parseXML(detailxml));
		$xml.find("row").each(function() {
			try {
				var invNumber = $(this).find("cell")[0].textContent;
				var offUse = $(this).find("cell")[1].textContent;
				var oprType = $(this).find("cell")[2].textContent;
				var crOn = $(this).find("cell")[3].textContent;
				var moOn = $(this).find("cell")[4].textContent;
				if (innerGrid.doesRowExist(invNumber)) {
					if (oprType == 'R') {
						innerGrid.deleteRow(invNumber);
					} else {
						if (offUse == '1') {
							innerGrid.cells(invNumber, 4).setValue('Yes');
						} else if (offUse == '0') {
							innerGrid.cells(invNumber, 4).setValue('No');
						}
						innerGrid.cells(invNumber, 11).setValue(moOn);
						innerGrid.cells(invNumber, 6).setValue(crOn);
					}
				}
			} catch (e) {
			}
		});
	}
	return;
}
function clearFields() {
	clearDependentFields(EMPTY_STRING);
	$('#contactType').val(EMPTY_STRING);
	searchGrid.clearAll();
}

function clearFormFields() {
	clearDependentFields(EMPTY_STRING);
	$('#contactType').val(EMPTY_STRING);
}

function backtrack(id) {
	switch (id) {
	case 'contactType':
		setFocusLast('contactType');
		break;
	case 'official':
		if ($('#contactType').val() == 'L') {
			setFocusLast('landlineNumber');
		} else if ($('#contactType').val() == 'M') {
			setFocusLast('mobileNumber');
		} else if ($('#contactType').val() == 'E') {
			setFocusLast('emailId');
		} else {
			setFocusLast('contactType');
		}
		break;
	case 'landlineNumber':
		setFocusLast('stdCode');
		break;
	case 'stdCode':
		setFocusLast('landlineCountry');
		break;
	case 'landlineCountry':
		setFocusLast('contactType');
		break;
	case 'mobileNumber':
		setFocusLast('mobileCountry');
		break;
	case 'mobileCountry':
		setFocusLast('contactType');
		break;
	case 'emailId':
		setFocusLast('contactType');
		break;
	case 'numberSearch':
		setFocusLast('numberSearch');
		break;
	case 'emailSearch':
		setFocusLast('numberSearch');
		break;
	case 'personSearch':
		setFocusLast('emailSearch');
		break;
	}
}

function validate(id) {
	switch (id) {
	case 'contactType':
		contactType_val();
		break;
	case 'landlineCountry':
		landlineCountry_val();
		break;
	case 'stdCode':
		stdCode_val();
		break;
	case 'landlineNumber':
		landlineNumber_val();
		break;
	case 'mobileCountry':
		mobileCountry_val();
		break;
	case 'mobileNumber':
		mobileNumber_val();
		break;
	case 'emailId':
		emailId_val();
		break;
	case 'official':
		setFocusLast('add');
		break;
	case 'numberSearch':
		numberSearch_val();
		break;
	case 'emailSearch':
		emailSearch_val();
		break;
	case 'personSearch':
		personSearch_val();
		break;
	}
}

function contactType_val() {
	var value = $('#contactType').val();
	if ($('#currentTab').val() == 'contact_1') {
		clearError('contactType_error');
		return true;
	}
	clearError('contactType_error');
	if (isEmpty(value)) {
		setError('contactType_error', HMS_MANDATORY);
		setFocusLast('contactType');
		return false;
	}

	if (value == 'L') {
		setFocusLast('landlineCountry_div');
	} else if (value == 'M') {
		setFocusLast('mobileCountry');
	} else if (value == 'E') {
		setFocusLast('emailId');
	} else {
		setFocusLast('contactType');
	}
	return true;
}

function landlineCountry_val() {
	if (contactType_val()) {
		if ($('#contactType').val() == 'L') {
			var value = $('#landlineCountry').val();
			clearError('landlineCountry_error');
			if (isEmpty(value)) {
				setError('landlineCountry_error', HMS_MANDATORY);
				return false;
			}
		}
		setFocusLast('stdCode');
		return true;
	} else {
		return false;
	}
}

function stdCode_val() {
	if (contactType_val()) {
		if ($('#contactType').val() == 'L') {
			var value = $('#stdCode').val();
			clearError('stdCode_error');
			if (isEmpty(value)) {
				setError('stdCode_error', HMS_MANDATORY);
				return false;
			}
		}
		setFocusLast('landlineNumber');
		return true;
	} else {
		return false;
	}
}

function landlineNumber_val() {
	if (contactType_val()) {
		if ($('#contactType').val() == 'L') {
			var value = $('#landlineNumber').val();
			clearError('landlineNumber_error');
			if (isEmpty(value)) {
				setError('landlineNumber_error', HMS_MANDATORY);
				return false;
			}
			if (!isValidTelePhoneNumber(value, 15)) {
				setError('landlineNumber_error', INVALID_TELEPHONE);
				return false;
			}
		}
		if (sKey == 'PERSON_ID')
			setFocusLast('official');
		else
			setFocusLast('add');
		return true;
	} else {
		return false;
	}
}

function mobileCountry_val() {
	if (contactType_val()) {
		if ($('#contactType').val() == 'M') {
			var value = $('#mobileCountry').val();
			clearError('mobileCountry_error');
			if (isEmpty(value)) {
				setError('mobileCountry_error', HMS_MANDATORY);
				return false;
			}
		}
		setFocusLast('mobileNumber');
		return true;
	} else {
		return false;
	}
}

function mobileNumber_val() {
	if (contactType_val()) {
		if ($('#contactType').val() == 'M') {
			var value = $('#mobileNumber').val();
			clearError('mobileNumber_error');
			if (isEmpty(value)) {
				setError('mobileNumber_error', HMS_MANDATORY);
				return false;
			}
			if (!isPhoneNumber(value)) {
				setError('mobileNumber_error', HMS_INVALID_MOBILE);
				return false;
			}
		}
		if (sKey == 'PERSON_ID')
			setFocusLast('official');
		else
			setFocusLast('add');
		return true;
	} else {
		return false;
	}
}

function emailId_val() {
	var value = $('#emailId').val();
	clearError('emailId_error');
	if ($('#contactType').val() != 'E') {
		return true;
	}
	if (isEmpty(value)) {
		setError('emailId_error', HMS_MANDATORY);
		return false;
	}
	if (!isEmail(value)) {
		setError('emailId_error', HMS_INVALID_EMAIL_ID);
		return false;
	}
	if (sKey == 'PERSON_ID')
		setFocusLast('official');
	else
		setFocusLast('add');
	return true;
}

function numberSearch_val() {
	var value = $('#numberSearch').val();
	clearError('numberSearch_error');
	if (!isEmpty(value)) {
		loadSearchGrid('T');
		setFocusLast('use');
		return true;
	} else {
		searchGrid.clearAll();
		setFocusLast('emailSearch');
		return true;
	}
}

function emailSearch_val() {
	var value = $('#emailSearch').val();
	clearError('emailSearch_error');
	if (!isEmpty(value)) {
		loadSearchGrid('E');
		setFocusLast('use');
		return true;
	} else {
		searchGrid.clearAll();
		setFocusLast('personSearch');
		return true;
	}
}

function personSearch_val() {
	var value = $('#personSearch').val();
	clearError('personSearch_error');
	if (!isEmpty(value)) {
		loadSearchGrid('P');
		setFocusLast('use');
		return true;
	} else {
		searchGrid.clearAll();
		setFocusLast('personSearch');
		return true;
	}
}

function editContact(invNumber) {
	recordType = EMPTY_STRING;
	if ($('#currentTab').val() == 'contact_1') {
		tabbarObject.goToPrevTab();
		$('#currentTab').val('contact_0');
	}
	$('#operationStatus').val(EMPTY_STRING);

	$('#contactType').val(innerGrid.cells(invNumber, 1).getValue());
	recordType = innerGrid.cells(invNumber, 9).getValue();
	addedOn = innerGrid.cells(invNumber, 10).getValue();
	modifiedOn = innerGrid.cells(invNumber, 11).getValue();
	clearDependentFields($('#contactType').val());
	var contactDetail = innerGrid.cells(invNumber, 3).getValue();
	var contact;
	if ($('#contactType').val() == 'L') {
		contact = contactDetail.substring(0, contactDetail.length);
		$('#landlineCountry').val((contact.split('-')[0]).substring(1));
		landlineCountry.setComboValue($('#landlineCountry').val());
		$('#stdCode').val(contact.split('-')[1]);

		$('#landlineNumber').val(contact.split('-')[2]);
	} else if ($('#contactType').val() == 'M') {
		contact = contactDetail.substring(0, contactDetail.length);
		$('#mobileCountry').val((contact.split('-')[0]).substring(1));
		mobileCountry.setComboValue($('#mobileCountry').val());
		$('#mobileNumber').val(contact.split('-')[1]);
	} else {
		$('#emailId').val(contactDetail);
	}
	$('#official').prop('checked', decodeOTB(innerGrid.cells(invNumber, 4).getValue()));
	$('#inventoryNumber').val(innerGrid.cells(invNumber, 5).getValue());
	$('#action').val(MODIFY);

	searchGrid.clearAll();
	$('#numberSearch').val(EMPTY_STRING);
	clearError('numberSearch_error');
	$('#emailSearch').val(EMPTY_STRING);
	clearError('emailSearch_error');
	$('#personSearch').val(EMPTY_STRING);
	clearError('personSearch_error');
}

function decodeOTB(official) {
	if (official == 'Yes') {
		return true;
	} else {
		return false;
	}
}

function decodeBTO(official) {
	if (official) {
		return 'Yes';
	} else {
		return 'No';
	}
}

function removeContact(invNumber) {
	$('#operationStatus').val('R');
	recordType = EMPTY_STRING;
	if (confirm(DELETE_CONFIRM)) {
		$('#action').val(MODIFY);
		$('#inventoryNumber').val(innerGrid.cells(invNumber, 5).getValue());
		recordType = innerGrid.cells(invNumber, 9).getValue();
		addedOn = innerGrid.cells(invNumber, 10).getValue();
		modifiedOn = innerGrid.cells(invNumber, 11).getValue();
		updateInventory();
	}
}

function viewContact(inventoryNumber) {
	openPopup('common/qinvcontact', inventoryNumber, 'contactView');
	resetLoading();
}

function doHelp(id) {
	switch (id) {
	case 'personSearch':
		help('COMMON', 'HLP_PERSON_DB', $('#personSearch').val(), '', $('#personSearch'));
		break;
	}
}

function change(id) {
	switch (id) {
	case 'contactType':
		contactType_change();
		break;
	}
}

function contactType_change() {
	var contactType = $('#contactType').val();
	clearDependentFields(contactType);
}

function clearDependentFields(contactType) {
	$('#enabled').prop('checked', false);
	if (contactType == EMPTY_STRING) {
		clearError('landlineCountry_error');
		clearError('stdCode_error');
		clearError('landlineNumber_error');
		$('#landlineCountry').val(EMPTY_STRING);
		$('#stdCode').val(EMPTY_STRING);
		$('#landlineNumber').val(EMPTY_STRING);
		clearError('mobileCountry_error');
		clearError('mobileNumber_error');
		$('#mobileCountry').val(EMPTY_STRING);
		$('#mobileNumber').val(EMPTY_STRING);
		clearError('emailId_error');
		$('#emailId').val(EMPTY_STRING);

		landlineCountry.clearAll(true);
		mobileCountry.clearAll(true);
		$('#landline').addClass('hidden');
		$('#mobile').addClass('hidden');
		$('#email').addClass('hidden');

		$('#numberSearch').val(EMPTY_STRING);
		clearError('numberSearch_error');
		$('#emailSearch').val(EMPTY_STRING);
		clearError('emailSearch_error');
		$('#personSearch').val(EMPTY_STRING);
		clearError('personSearch_error');
		searchGrid.clearAll();

		$('#action').val(ADD);
		$('#operationStatus').val(EMPTY_STRING);

	} else if (contactType == 'L') {
		clearError('landlineCountry_error');
		clearError('stdCode_error');
		clearError('landlineNumber_error');
		$('#landlineCountry').val(EMPTY_STRING);
		$('#stdCode').val(EMPTY_STRING);
		$('#landlineNumber').val(EMPTY_STRING);
		clearError('mobileCountry_error');
		clearError('mobileNumber_error');
		$('#mobileCountry').val(EMPTY_STRING);
		$('#mobileNumber').val(EMPTY_STRING);
		clearError('emailId_error');
		$('#emailId').val(EMPTY_STRING);

		landlineCountry.clearAll(true);
		mobileCountry.clearAll(true);

		$('#landline').removeClass('hidden');
		$('#mobile').addClass('hidden');
		$('#email').addClass('hidden');

		smartCombo('COMMON', 'PHONECODES', '', $('#landlineCountry'));
	} else if (contactType == 'M') {
		clearError('landlineCountry_error');
		clearError('stdCode_error');
		clearError('landlineNumber_error');
		$('#landlineCountry').val(EMPTY_STRING);
		$('#stdCode').val(EMPTY_STRING);
		$('#landlineNumber').val(EMPTY_STRING);
		clearError('mobileCountry_error');
		clearError('mobileNumber_error');
		$('#mobileCountry').val(EMPTY_STRING);
		$('#mobileNumber').val(EMPTY_STRING);
		clearError('emailId_error');
		$('#emailId').val(EMPTY_STRING);

		landlineCountry.clearAll(true);
		mobileCountry.clearAll(true);

		$('#landline').addClass('hidden');
		$('#mobile').removeClass('hidden');
		$('#email').addClass('hidden');
		smartCombo('COMMON', 'PHONECODES', '', $('#mobileCountry'));
	} else if (contactType == 'E') {
		clearError('landlineCountry_error');
		clearError('stdCode_error');
		clearError('landlineNumber_error');
		$('#landlineCountry').val(EMPTY_STRING);
		$('#stdCode').val(EMPTY_STRING);
		$('#landlineNumber').val(EMPTY_STRING);
		clearError('mobileCountry_error');
		clearError('mobileNumber_error');
		$('#mobileCountry').val(EMPTY_STRING);
		$('#mobileNumber').val(EMPTY_STRING);
		clearError('emailId_error');
		$('#emailId').val(EMPTY_STRING);

		landlineCountry.clearAll(true);
		mobileCountry.clearAll(true);
		$('#landline').addClass('hidden');
		$('#mobile').addClass('hidden');
		$('#email').removeClass('hidden');
	}
}

function formLink(type, invNo) {
	var link;
	if (type == 'E') {
		link = "EDIT^javascript:editContact(\"" + invNo + "\")";
	} else {
		link = "REMOVE^javascript:removeContact(\"" + invNo + "\")";
	}
	return link;
}

function getContactType(type) {
	if (type == 'L') {
		return 'Landline';
	} else if (type == 'M') {
		return 'Mobile';
	} else {
		return 'E-Mail';
	}
}

function getContactDetails(type) {
	var detail;
	if (type == 'L') {
		detail = '+' + $('#landlineCountry').val() + '-' + $('#stdCode').val() + '-' + $('#landlineNumber').val();
	} else if (type == 'M') {
		detail = '+' + $('#mobileCountry').val() + '-' + $('#mobileNumber').val();
	} else {
		detail = $('#emailId').val();
	}
	return detail;
}

function duplicateCheck() {
	if ($('#action').val() == MODIFY) {
		return true;
	}
	var currentValue;
	if ($('#contactType').val() == 'L') {
		currentValue = [ '+' + $('#landlineCountry').val() + '-' + $('#stdCode').val() + '-' + $('#landlineNumber').val() ];
	} else if ($('#contactType').val() == 'M') {
		currentValue = [ '+' + $('#mobileCountry').val() + '-' + $('#mobileNumber').val() ];
	} else {
		currentValue = [ $('#emailId').val() ];
	}
	return _grid_duplicate_check(innerGrid, currentValue, [ 2 ]);
}

function loadSearchGrid(type) {
	searchGrid.clearAll();
	validator.clearMap();
	validator.setMtm(false);
	if (type == 'T') {
		validator.setValue('SEARCH_CONTENT', $('#numberSearch').val());
	} else if (type == 'E') {
		validator.setValue('SEARCH_CONTENT', $('#emailSearch').val());
	} else {
		validator.setValue('SEARCH_CONTENT', $('#personSearch').val());
	}
	validator.setValue('SEARCH_TYPE', type);
	validator.setClass('patterns.config.web.forms.common.mcontactbean');
	validator.setMethod('loadSearchGrid');
	validator.sendAndReceive();
	if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		xmlString = validator.getValue(RESULT_XML);
		searchGrid.loadXMLString(xmlString);
	} else {
		setError('search_error', validator.getValue(ERROR));
	}
}

function doTabbarClick(id) {
	$('#currentTab').val(id);
	clearDependentFields(EMPTY_STRING);
	$('#contactType').val(EMPTY_STRING);
	if ($('#currentTab').val() == 'contact_1') {
		clearDependentFields(EMPTY_STRING);
	} else {
		searchGrid.clearAll();
		$('#numberSearch').val(EMPTY_STRING);
		clearError('numberSearch_error');
		$('#emailSearch').val(EMPTY_STRING);
		clearError('emailSearch_error');
		$('#personSearch').val(EMPTY_STRING);
		clearError('personSearch_error');
	}
}

function smartchange(id, value, text) {
	switch (id.DOMParent) {
	case 'landlineCountry_div':
		$('#landlineCountry').val(value);
		landlineCountry_val();
		break;
	case 'mobileCountry_div':
		$('#mobileCountry').val(value);
		mobileCountry_val();
		break;
	}
}

function updateResponse(formResponse) {
	if ($('#operationStatus').val() == 'U') {
		if ($('#currentTab').val() == 'contact_1') {
			var ros = searchGrid.getCheckedRows(0);
			var rowIDs = ros.split(',');
			for (var i = 0; i < rowIDs.length; i++) {
				var enabled = searchGrid.cells(rowIDs[i], 0).getValue();
				if (enabled) {
					var type = searchGrid.cells(rowIDs[i], 1).getValue();
					var details = searchGrid.cells(rowIDs[i], 2).getValue();
					var official = searchGrid.cells(rowIDs[i], 3).getValue();
					var inventoryNumber = searchGrid.cells(rowIDs[i], 4).getValue();
					var field_values = [ '', type, getContactType(type), details, decodeBTO(decodeSTB(official)), inventoryNumber, getCBD(), formLink('E', inventoryNumber), formLink('R', inventoryNumber), '', '', '' ];
					if (innerGrid.doesRowExist(inventoryNumber)) {
						if (confirm("Contact Already Exist")) {
							innerGrid.deleteRow(inventoryNumber);
							detailGrid.deleteRow(inventoryNumber);
						}
					}
					innerGrid.addRow(inventoryNumber, field_values);
					var values = [ inventoryNumber, decodeBTS(decodeOTB(official)), 'A', getCBD(), '' ];
					detailGrid.addRow(inventoryNumber, values);
				}
			}
		}
		searchGrid.clearAll();
		$('#numberSearch').val(EMPTY_STRING);
		clearError('numberSearch_error');
		$('#emailSearch').val(EMPTY_STRING);
		clearError('emailSearch_error');
		$('#personSearch').val(EMPTY_STRING);
		clearError('personSearch_error');
	} else {
		if ($('#action').val() == ADD) {
			var contactType = $('#contactType').val();
			var field_values = [ '', contactType, getContactType(contactType), getContactDetails(contactType), decodeBTO($('#official').is(':checked')), formResponse, getCBD(), formLink('E', formResponse), formLink('R', formResponse), '', '', '' ];
			innerGrid.addRow(formResponse, field_values);
			$('#contactType').val(EMPTY_STRING);
			clearDependentFields('');
			$('#contactType').focus();

			var values = [ formResponse, decodeBTS($('#official').is(':checked')), 'A', getCBD(), '' ];

			detailGrid.addRow(formResponse, values);

		} else if ($('#action').val() == MODIFY) {
			if (($('#operationStatus').val() == EMPTY_STRING) && ($('#inventoryNumber').val() != EMPTY_STRING)) {
				var contactType = $('#contactType').val();
				var field_values = [ '', contactType, getContactType(contactType), getContactDetails(contactType), decodeBTO($('#official').is(':checked')), $('#inventoryNumber').val(), getCBD(), formLink('E', $('#inventoryNumber').val()), formLink('R', $('#inventoryNumber').val()), recordType,
						addedOn, getCBD() ];
				innerGrid.deleteRow($('#inventoryNumber').val());
				innerGrid.addRow($('#inventoryNumber').val(), field_values);
				$('#contactType').val(EMPTY_STRING);
				if (recordType == 'M' || (recordType == 'T' && (parentAction == 'M'))) {
					var values = [ $('#inventoryNumber').val(), decodeBTS($('#official').is(':checked')), 'M', addedOn, getCBD() ];
					detailGrid.deleteRow($('#inventoryNumber').val());
					detailGrid.addRow($('#inventoryNumber').val(), values);
				}
				clearDependentFields('');
				$('#contactType').focus();
			} else if (($('#operationStatus').val() == EMPTY_STRING) && ($('#inventoryNumber').val() == EMPTY_STRING)) {
				var contactType = $('#contactType').val();
				var field_values = [ '', contactType, getContactType(contactType), getContactDetails(contactType), decodeBTO($('#official').is(':checked')), formResponse, getCBD(), formLink('E', formResponse), formLink('R', formResponse), '', '', '' ];
				innerGrid.addRow(formResponse, field_values);
				$('#contactType').val(EMPTY_STRING);
				clearDependentFields('');
				$('#contactType').focus();
				var values = [ formResponse, decodeBTS($('#official').is(':checked')), 'A', getCBD(), '' ];
				detailGrid.addRow(formResponse, values);
			} else {
				var official = innerGrid.cells(rowId, 4).getValue();
				var inventryNumber = innerGrid.cells(rowId, 5).getValue();
				var values = [ inventryNumber, decodeBTS(decodeOTB(official)), 'R', addedOn, modifiedOn ];
				if (recordType == 'M' || recordType == 'T') {
					detailGrid.deleteRow(inventryNumber);
					detailGrid.addRow(inventryNumber, values);
				} else {
					detailGrid.deleteRow(inventryNumber);
				}
				innerGrid.deleteRow(inventryNumber);
				$('#contactType').val(EMPTY_STRING);
				clearDependentFields('');
			}
		}
	}
	$('#action').val(ADD);
	$('#operationStatus').val(EMPTY_STRING);
	addedOn = EMPTY_STRING;
	modifiedOn = EMPTY_STRING;
}

function validateExistingContacts() {
	if ($('#currentTab').val() == 'contact_1') {
		var rowID = searchGrid.getCheckedRows(0);
		if (rowID == EMPTY_STRING) {
			alert(SELECT_ATLEAST_ONE);
			return false;
		} else {
			$('#inventoryNumber').val(rowID);
		}
	}
	return true;
}

function updateInventory() {
	//if (revalidate()) {
		validator.clearMap();
		validator.setMtm(false);
		validator.setClass('patterns.config.web.forms.common.mcontactbean');
		validator.setMethod('updateInventory');
		validator.setValue('PARENT_PGM_ID', PARENT_PROGRAM_ID);
		validator.setValue('PARENT_PGM_PK', EMPTY_STRING);
		validator.setValue('LINKED_PGM_ID', CURRENT_PROGRAM_ID);
		validator.setValue('CONVERSATION_ID', $('#conversationID').val());
		if ($('#operationStatus').val() == 'U') {
			validator.setValue('OPERATION_TYPE', 'U');
			validator.setValue('OPERATION_STATUS', "U");
			validator.setValue('INVENTORY_NO', $('#inventoryNumber').val());
		} else if ($('#operationStatus').val() == 'R') {
			validator.setValue('OPERATION_TYPE', 'R');
			validator.setValue('OPERATION_STATUS', "R");
			validator.setValue('INVENTORY_NO', $('#inventoryNumber').val());
		} else if ($('#action').val() == 'A') {
			validator.setValue('OPERATION_TYPE', 'A');
			validator.setValue('OPERATION_STATUS', "");
			validator.setValue('INVENTORY_NO', EMPTY_STRING);
		} else {
			if ((innerGrid.cells(rowId, 9).getValue() == EMPTY_STRING) && (returnType == 'M')) {
				validator.setValue('OPERATION_TYPE', 'A');
			} else {
				validator.setValue('OPERATION_TYPE', 'M');
			}
			validator.setValue('OPERATION_STATUS', $('#operationStatus').val());
			validator.setValue('INVENTORY_NO', $('#inventoryNumber').val());
		}
		validator.setValue('ACTIVE_TAB', $('#currentTab').val());
		validator.setValue('PERSON_ID', $('#personId').val());
		validator.setValue('CURRENT_TAB', $('#currentTab').val());

		var contactType = $('#contactType').val();
		validator.setValue('CONTACT_TYPE', contactType);
		if ($('#official').is(':checked')) {
			validator.setValue('OFFICIAL', '1');
		} else {
			validator.setValue('OFFICIAL', '0');
		}

		if (contactType == 'L') {
			validator.setValue('LL_COUNTRY', $('#landlineCountry').val());
			validator.setValue('LL_STD_CODE', $('#stdCode').val());
			validator.setValue('LL_NUMBER', $('#landlineNumber').val());
		} else if (contactType == 'M') {
			validator.setValue('MOB_COUNTRY', $('#mobileCountry').val());
			validator.setValue('MOB_NUMBER', $('#mobileNumber').val());
		} else {
			validator.setValue('EMAIL_ID', $('#emailId').val());
		}
		validator.sendAndReceive();
		if (validator.getValue(RESULT) == DATA_AVAILABLE) {
			if ($('#operationStatus').val() == 'R') {
				updateResponse($('#inventoryNumber').val());
			} else if ($('#action').val() == 'A') {
				updateResponse(validator.getValue("INVENTORY_NO"));
			} else if ($('#action').val() == 'M') {
				updateResponse($('#inventoryNumber').val());
			}
			if (returnType == 'S') {
				if (isEmpty($('#inventoryNumber').val())){
					window.parent.updateContact(validator.getValue("INVENTORY_NO"));
				}
				else{
					window.parent.updateContact($('#inventoryNumber').val());
				}
				doWindowExit();
			} else {
				window.parent.updateContact(detailGrid.serialize(),innerGrid.getRowsNum());
			}
			alert('INVENTORY UPDATED SUCCESSFULLY');
			$('#inventoryNumber').val(EMPTY_STRING);
		} else {
			alert('INVENTORY UPDATION ERROR');
		}
	//}
}
function revalidate() {
	var error = 0;
	if ($('#currentTab').val() == 'contact_0') {
		if ($('#operationStatus').val() != 'R') {
			if (!contactType_val()) {
				error++;
			}
		}
		if ($('#contactType').val() == 'L') {
			if (!landlineCountry_val()) {
				error++;
			}
			if (!stdCode_val()) {
				error++;
			}
			if (!landlineNumber_val()) {
				error++;
			}
		} else if ($('#contactType').val() == 'M') {
			if (!mobileCountry_val()) {
				error++;
			}
			if (!mobileNumber_val()) {
				error++;
			}
		} else if ($('#contactType').val() == 'E') {
			if (!emailId_val()) {
				error++;
			}
		}
	} else if ($('#operationStatus').val() != 'R') {
		if (!validateExistingContacts()) {
			error++;
		}
		$('#operationStatus').val('U');
	}
	if (error == 0) {
		return true;
	} else {
		return false;
	}
}
function popupResponse(callback) {
	callback(detailGrid.serialize());
}