<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<%@taglib prefix="message" tagdir="/WEB-INF/tags/message"%>
<web:bundle baseName="patterns.config.web.forms.common.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/common/qcontact.js" />
		<web:script src="public/scripts/component.js" />
	</web:dependencies>
	<web:loadPK />
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="mcontact.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:dividerBlock>
				<web:sectionBlock width="width-100p" align="left">
				<web:viewContent id="person" styleClass="hidden">
				<web:section>
						<web:table>
					<web:columnGroup>
						<web:columnStyle width="180px" />
						<web:columnStyle />
					</web:columnGroup>
					<web:rowOdd>
						<web:column>
							<web:legend key="maddress.personId" var="program" mandatory="true" />
						</web:column>
						<web:column>
							<type:personIdDisplay property="personId" id="personId" />
						</web:column>
					</web:rowOdd>
					</web:table>
					</web:section>
					</web:viewContent>
					<web:section>
						<web:table>
							<web:columnGroup>
								<web:columnStyle width="180px" />
								<web:columnStyle />
							</web:columnGroup>
							<web:rowEven>
								<web:column span="2">
									<web:grid height="150px" width="860px" id="innerGrid" src="common/qcontact_innerGrid.xml">
									</web:grid>
								</web:column>
							</web:rowEven>
						</web:table>
					</web:section>
				</web:sectionBlock>
			</web:dividerBlock>
		</web:viewSubPart>
	</web:viewPart>
</web:fragment>