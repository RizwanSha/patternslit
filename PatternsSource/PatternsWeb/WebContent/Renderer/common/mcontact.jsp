<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.common.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/common/mcontact.js" />
		<web:script src="public/scripts/component.js" />
	</web:dependencies>
	<web:loadPK />
	<web:viewPart>
		<web:viewSubPart>
			<web:form action="/Renderer/common/mcontact" id="mcontact" method="POST">
				<web:viewContent id="po_view1">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:viewContent id="person" styleClass="hidden">
							<web:section>
									<web:table>
								<web:rowOdd>
									<web:column>
										<web:legend key="mcontact.personId" var="program" />
									</web:column>
									<web:column>
										<type:personIdDisplay property="personId" id="personId" />
									</web:column>
								</web:rowOdd>
								</web:table>
								</web:section>
							</web:viewContent>
							<web:viewContent id="gridDetails">
								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="180px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowEven>
											<web:column span="2">
												<web:grid height="150px" width="860px" id="innerGrid" src="common/mcontact_innerGrid.xml">
												</web:grid>
											</web:column>
										</web:rowEven>
									</web:table>
								</web:section>
							</web:viewContent>
							<br>
							<type:tabbarBody id="contact_TAB_0">
								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="180px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowOdd>
											<web:column>
												<web:legend key="mcontact.contacttype" var="program" mandatory="true" />
											</web:column>
											<web:column>
												<type:combo property="contactType" id="contactType" datasourceid="COMMON_CONTACTTYPE" />
											</web:column>
										</web:rowOdd>
									</web:table>
								</web:section>
								<web:viewContent id="landline" styleClass="hidden">
									<web:section>
										<type:landlineContact />
									</web:section>
								</web:viewContent>
								<web:viewContent id="mobile" styleClass="hidden">
									<web:section>
										<type:mobileContact />
									</web:section>
								</web:viewContent>
								<web:viewContent id="email" styleClass="hidden">
									<web:section>
										<web:table>
											<web:columnGroup>
												<web:columnStyle width="180px" />
												<web:columnStyle />
											</web:columnGroup>
											<web:rowEven>
												<web:column>
													<web:legend key="mcontact.emailcontact" var="program" />
												</web:column>
												<web:column>
													<type:email property="emailId" id="emailId" />
												</web:column>
											</web:rowEven>
										</web:table>
									</web:section>
								</web:viewContent>
								<web:viewContent id="offUse" styleClass="hidden">
								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="180px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowOdd>
											<web:column>
												<web:legend key="mcontact.officialuse" var="program" />
											</web:column>
											<web:column>
												<type:checkbox property="official" id="official" />
											</web:column>
										</web:rowOdd>
										</web:table>
									</web:section>
									</web:viewContent>
								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="180px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowEven>
											<web:column span="2">
												<type:button key="form.add" id="add" var="common" onclick="updateInventory()" />
												<type:button key="form.cancel" id="clear" var="common" onclick="clearFormFields()" />
											</web:column>
										</web:rowEven>
									</web:table>
								</web:section>
							</type:tabbarBody>
							<type:tabbarBody id="contact_TAB_1">
								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="180px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowOdd>
											<web:column>
												<web:legend key="mcontact.numbersearch" var="program" />
											</web:column>
											<web:column>
												<type:number length="15" size="15" property="numberSearch" id="numberSearch" />
											</web:column>
										</web:rowOdd>
										<web:rowEven>
											<web:column>
												<web:legend key="mcontact.emailsearch" var="program" />
											</web:column>
											<web:column>
												<type:email  property="emailSearch" id="emailSearch" />
											</web:column>
										</web:rowEven>
										<web:rowOdd>
											<web:column>
												<web:legend key="mcontact.personsearch" var="program" />
											</web:column>
											<web:column>
												<type:personId  property="personSearch" id="personSearch" />
											</web:column>
										</web:rowOdd>
									</web:table>
								</web:section>
								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="180px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowEven>
											<web:column span="2">
												<web:grid height="250px" width="630px" id="searchGrid" src="common/mcontact_searchGrid.xml">
												</web:grid>
											</web:column>
										</web:rowEven>
										<web:rowOdd>
											<web:column span="2">
												<type:button key="form.usethiscontact" id="use" var="common" onclick="updateInventory()" />
												<type:button key="form.cancel" id="clear" var="common" onclick="clearFormFields()" />
											</web:column>
										</web:rowOdd>
									</web:table>
								</web:section>
							</type:tabbarBody>
							<type:tabbar height="height :600px;" name="Create New Contacts$$Use Existing Contacts" width="width : 900px;" required="2" id="contact" selected="0"></type:tabbar>
						</web:sectionBlock>
					</web:dividerBlock>
				</web:viewContent>
				<web:viewContent id="detailGrid" styleClass="hidden">
					<web:section>
						<web:table>
							<web:columnGroup>
								<web:columnStyle width="180px" />
								<web:columnStyle />
							</web:columnGroup>
							<web:rowEven>
								<web:column span="2">
									<web:grid height="200px" width="700px" id="detailGrid" src="common/mcontact_detailGrid.xml">
									</web:grid>
								</web:column>
							</web:rowEven>
						</web:table>
					</web:section>
				</web:viewContent>
				<web:element property="command" />
				<web:element property="action" />
				<web:element property="inventoryNumber" />
				<web:element property="conversationID" />
				<web:element property="operationStatus" />
				<web:element property="currentTab" />
				<web:element property="inventoryNumberList" />
				<web:popupKeys />
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
	<web:contextSearch />
</web:fragment>