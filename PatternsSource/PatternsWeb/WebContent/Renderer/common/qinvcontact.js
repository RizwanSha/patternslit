var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'QINVCONTACT';
var sourceKey = EMPTY_STRING;
function init() {
	clearFields();
	loadContactValues(_primaryKey);
}
function clearFields() {
	$('#landlineCountry').val(EMPTY_STRING);
	$('#stdCode').val(EMPTY_STRING);
	$('#landlineNumber').val(EMPTY_STRING);
	$('#mobileCountry').val(EMPTY_STRING);
	$('#mobileNumber').val(EMPTY_STRING);
	$('#emailId').val(EMPTY_STRING);
	$('#landline').addClass('hidden');
	$('#mobile').addClass('hidden');
	$('#email').addClass('hidden');
	setCheckbox('official', NO);
}
function loadData() {
}

function loadContactValues(pk) {
	var key = pk.split(',');
	if (key.length == 5) {
		$('#po_view6').removeClass('hidden');
		SOURCE_VALUE = key[0];
		pk = key[1];
		PARENT_PROGRAM_ID = key[3];
		sourceKey = key[4];
		loadRecord(pk);
		if (sourceKey == 'PERSON_ID') {
			$('#officialUse').removeClass('hidden');
			if (key[2] == '1') {
				setCheckbox('official', YES);
			}
		}
	} else if (key.length == 1) {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('INVENTORY_NUMBER', pk);
		validator.setClass('patterns.config.web.forms.common.mcontactbean');
		validator.setMethod('loadContactDetails');
		validator.sendAndReceive();
		if (validator.getValue(RESULT) == DATA_AVAILABLE) {
			$('#officialUse').addClass('hidden');
			$('#contactInvNum').val(pk);
			$('#contactType').val(validator.getValue("CONTACT_TYPE"));
			if (validator.getValue("CONTACT_TYPE") == 'L') {
				$('#landline').removeClass('hidden');
				$('#mobile').addClass('hidden');
				$('#email').addClass('hidden');
				smartCombo('COMMON', 'PHONECODES', '', $('#landlineCountry'));
				landlineCountry.setComboValue(validator.getValue("LL_CNTRY_CODE"));
				$('#stdCode').val(validator.getValue("LL_STD_CODE"));
				$('#landlineNumber').val(validator.getValue("LL_NUMBER"));
			} else if (validator.getValue("CONTACT_TYPE") == 'M') {
				$('#landline').addClass('hidden');
				$('#mobile').removeClass('hidden');
				$('#email').addClass('hidden');
				smartCombo('COMMON', 'PHONECODES', '', $('#mobileCountry'));
				mobileCountry.setComboValue(validator.getValue("MOB_CNTRY_CODE"));
				$('#mobileNumber').val(validator.getValue("MOB_NUMBER"));
			} else {
				$('#landline').addClass('hidden');
				$('#mobile').addClass('hidden');
				$('#email').removeClass('hidden');
				$('#emailId').val(validator.getValue("EMAIL_ID"));
			}
		}
	}
}
function loadRecord(value) {
	LINK_PROGRAM_ID = 'MCONTACT';
	if (sourceKey == 'PERSON_ID') {
		var parent_pk = value.split(PK_SEPERATOR);
		PARENT_PRIMARY_KEY = parent_pk[0] + PK_SEPERATOR + parent_pk[1];
		args = parent_pk[0] + PK_SEPERATOR + parent_pk[2];
	} else {
		var parent_pk = value.split('||');
		PARENT_PRIMARY_KEY = parent_pk[0];
		args = parent_pk[1];
	}
	if (SOURCE_VALUE == MAIN) {
		isMergeReqd = false;
		loadLinkedRecordData(loadContactDetail, PARENT_PROGRAM_ID, 'MCONTACT_M', args);
	} else {
		isMergeReqd = true;
		loadLinkedRecordData(loadContactDetail, PARENT_PROGRAM_ID, 'MCONTACT_M', args);
	}
}

function loadContactDetail() {
	if (validator.getValue("CONTACT_TYPE") != EMPTY_STRING) {
		$('#contactInvNum').val(validator.getValue("CONTACT_INV_NO"));
		$('#contactType').val(validator.getValue("CONTACT_TYPE"));
		if (validator.getValue("CONTACT_TYPE") == 'L') {
			$('#landline').removeClass('hidden');
			$('#mobile').addClass('hidden');
			$('#email').addClass('hidden');
			smartCombo('COMMON', 'PHONECODES', '', $('#landlineCountry'));
			landlineCountry.setComboValue(validator.getValue("LL_COUNTRY_CODE"));
			$('#stdCode').val(validator.getValue("LL_LOC_STD_CODE"));
			$('#landlineNumber').val(validator.getValue("LL_CONTACT_NUMBER"));
		} else if (validator.getValue("CONTACT_TYPE") == 'M') {
			$('#landline').addClass('hidden');
			$('#mobile').removeClass('hidden');
			$('#email').addClass('hidden');
			smartCombo('COMMON', 'PHONECODES', '', $('#mobileCountry'));
			mobileCountry.setComboValue(validator.getValue("MOB_COUNTRY_CODE"));
			$('#mobileNumber').val(validator.getValue("MOB_CONTACT_NUMBER"));
		} else {
			$('#landline').addClass('hidden');
			$('#mobile').addClass('hidden');
			$('#email').removeClass('hidden');
			$('#emailId').val(validator.getValue("EMAIL_ID"));
		}
	}
	if (isMergeReqd) {
		isMergeReqd = false;
		loadLinkedRecordData(loadContactDetail, PARENT_PROGRAM_ID, 'MCONTACT_T', args);
	}
}