<%@page import="java.util.ArrayList"%>
<%@page import="patterns.config.framework.web.configuration.menu.MenuUtils"%>
<%@page import="patterns.config.framework.web.GenericOption"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%
	String value = request.getParameter("userValue");
	String objID = request.getParameter("objID"); 
	String rowID = null;
	ArrayList<GenericOption> programList = null;
	MenuUtils menu = new MenuUtils();
	programList = menu.filterSmartNavigation(value);
	int rowCount = 0;
%>
<rows session-expired='0'>
	<%
	for (patterns.config.framework.web.GenericOption option : programList) {
		try {
			
			String id = option.getId();
			String label = option.getLabel();
			rowID =objID +  "_navigateLink_" + rowCount;
			System.out.println(id+"  "+label);
		%>
		<row>
			<cell>
				<web:linkrenderer forward="<%=id.toLowerCase()%>" label="<%=label%>" styleClass="navigation-link" id="<%=rowID%>"/>
			</cell>
		</row>
		<%
		System.out.println(id+"  "+label+" --After");
		} catch (Exception e) {
			rowCount--;
		%>
			</cell>
		</row>
		<%
			System.out.println("Exception from here---"+e.getLocalizedMessage());
			e.printStackTrace();
		}
		rowCount++;
	}
	%>
</rows>