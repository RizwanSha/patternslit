<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<%@taglib prefix="message" tagdir="/WEB-INF/tags/message"%>
<web:bundle baseName="patterns.config.web.forms.common.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/common/qinvcontact.js" />
		<web:script src="public/scripts/component.js" />
	</web:dependencies>
	<web:loadPK />
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="mcontact.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:viewContent id="po_view1">
				<web:dividerBlock>
					<web:sectionBlock width="width-100p" align="left">
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="180px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowOdd>
									<web:column>
										<web:legend key="mcontact.invnumber" var="program" />
									</web:column>
									<web:column>
										<type:numberDisplay property="contactInvNum" id="contactInvNum" length="6" size="6" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="mcontact.contacttype" var="program" />
									</web:column>
									<web:column>
										<type:comboDisplay property="contactType" id="contactType" datasourceid="COMMON_CONTACTTYPE" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:viewContent id="landline" styleClass="hidden">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle width="180px" />
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="form.landlinecontact" var="common" />
										</web:column>
										<web:column>
											<type:smartComboDisplay property="landlineCountry" id="landlineCountry" width="180" />
										</web:column>
										<web:column>
											<type:numberDisplay size="6" property="stdCode" id="stdCode" length="6" />
										</web:column>
										<web:column>
											<type:numberDisplay size="10" length="10" property="landlineNumber" id="landlineNumber" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
						</web:viewContent>
						<web:viewContent id="mobile" styleClass="hidden">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="form.mobilecontact" var="common" />
										</web:column>
										<web:column>
											<type:smartComboDisplay property="mobileCountry" id="mobileCountry" width="180" />
										</web:column>
										<web:column>
											<type:mobileDisplay property="mobileNumber" id="mobileNumber" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
						</web:viewContent>
						<web:viewContent id="email" styleClass="hidden">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="mcontact.emailcontact" var="program" />
										</web:column>
										<web:column>
											<type:emailDisplay property="emailId" id="emailId" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
						</web:viewContent>
						<web:viewContent id="officialUse" styleClass="hidden">
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="180px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="mcontact.officialuse" var="program" />
									</web:column>
									<web:column>
										<type:checkboxDisplay property="official" id="official" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						</web:viewContent>
					</web:sectionBlock>
				</web:dividerBlock>
			</web:viewContent>
		</web:viewSubPart>
	</web:viewPart>
</web:fragment>