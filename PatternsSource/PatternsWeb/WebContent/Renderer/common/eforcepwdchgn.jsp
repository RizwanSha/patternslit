<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<web:bundle baseName="patterns.config.web.forms.common.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/common/eforcepwdchgn.js" />
		<web:script src="public/scripts/sha256.js" />
	</web:dependencies>
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="eforcepwdchgn.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/common/eforcepwdchgn" id="eforcepwdchgn" method="POST">
				<web:viewContent id="po_view1">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<div class="legend info-message" id="spanPasswordPolicy"></div>
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="form.currentpwd" var="common" />
										</web:column>
										<web:column>
											<type:password property="loginCurrentPassword" id="loginCurrentPassword" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="form.newpwd" var="common" />
										</web:column>
										<web:column>
											<type:password property="loginNewPassword" id="loginNewPassword" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="form.renewpwd" var="common" />
										</web:column>
										<web:column>
											<type:password property="loginRenewPassword" id="loginRenewPassword" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column span="2">
											<web:submitReset />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column span="2">
											<web:printRequestAdditionalInfo styleClass="error" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
						</web:sectionBlock>
					</web:dividerBlock>
				</web:viewContent>
				<web:element property="minLength" />
				<web:element property="minAlpha" />
				<web:element property="minNumeric" />
				<web:element property="minSpecial" />
				<web:element property="passwordPolicy" />
				<web:element property="randomSalt" />
				<web:element property="loginCurrentPasswordSalt" />
				<web:element property="loginCurrentPasswordHashed" />
				<web:element property="loginNewPasswordHashed" />
				<web:element property="command" />
				<web:element property="action" />
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
</web:fragment>