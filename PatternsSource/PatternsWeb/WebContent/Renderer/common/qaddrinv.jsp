<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.common.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/common/qaddrinv.js" />
		<web:script src="public/scripts/component.js" />
	</web:dependencies>
	<web:loadPK />
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="qaddrinv.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:viewContent id="po_view1">
				<web:dividerBlock>
					<web:sectionBlock width="width-100p" align="left">
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="180px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="maddress.addrinvno" var="program" />
									</web:column>
									<web:column>
										<type:numberDisplay property="addrInvNo" id="addrInvNo" size="10" length="10" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="maddress.addresstype" var="program" />
									</web:column>
									<web:column>
										<type:addressTypeCodeDisplay property="addressType" id="addressType" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="maddress.countrycode" var="program" />
									</web:column>
									<web:column>
										<type:countryCodeDisplay property="countryCodeDisp" id="countryCodeDisp" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<div class="control-label textalign-left">
											<label id="addressLabel1"> <web:legend key="maddress.addressline1" var="program" />
											</label>
										</div>
									</web:column>
									<web:column>
										<type:addressLineDisplay property="addressLine1" id="addressLine1" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<div class="control-label textalign-left">
											<label id="addressLabel2"> <web:legend key="maddress.addressline2" var="program" />
											</label>
										</div>
									</web:column>
									<web:column>
										<type:addressLineDisplay property="addressLine2" id="addressLine2" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<div class="control-label textalign-left">
											<label id="addressLabel3"> <web:legend key="maddress.addressline3" var="program" />
											</label>
										</div>
									</web:column>
									<web:column>
										<type:addressLineDisplay property="addressLine3" id="addressLine3" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<div class="control-label textalign-left">
											<label id="addressLabel4"> <web:legend key="maddress.addressline4" var="program" />
											</label>
										</div>
									</web:column>
									<web:column>
										<type:addressLineDisplay property="addressLine4" id="addressLine4" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<div class="control-label textalign-left">
											<label id="labelForCountryCode"><web:legend key="maddress.geostructure" var="program" /> </label>
										</div>
									</web:column>
									<web:column>
										<type:dynamicDescriptionDisplay length="30" size="25" property="geoStructure" id="geoStructure" />
									</web:column>
								</web:rowOdd>
								<web:rowOdd>
									<web:column>
									</web:column>
									<web:column>
										<type:remarksDisplay property="lastLableField" id="lastLableField" />
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
						<web:viewContent id="po_view6" styleClass="hidden">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="maddress.localaddress" var="program" />
										</web:column>
										<web:column>
											<type:checkboxDisplay property="localAddress" id="localAddress" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="maddress.permanentaddress" var="program" />
										</web:column>
										<web:column>
											<type:checkboxDisplay property="permanentAddress" id="permanentAddress" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="maddress.officialuse" var="program" />
										</web:column>
										<web:column>
											<type:checkboxDisplay property="officialUse" id="officialUse" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="maddress.addrForComm" var="program" />
										</web:column>
										<web:column>
											<type:checkboxDisplay property="addrForComm" id="addrForComm" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
						</web:viewContent>
					</web:sectionBlock>
				</web:dividerBlock>
			</web:viewContent>
		</web:viewSubPart>
	</web:viewPart>
</web:fragment>