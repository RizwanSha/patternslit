<%@page import="java.util.Optional"%>
<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<div class="table_no_wrap">
	<%
		try {
			Object consoleCode=request.getParameter("consoleCode");
			Optional<Object> opconsoleCode= Optional.ofNullable(consoleCode);
			if(opconsoleCode.isPresent()){
				String code = opconsoleCode.get().toString();
				if (code != patterns.config.framework.database.RegularConstants.EMPTY_STRING) {
					session.setAttribute( patterns.config.framework.web.SessionConstants.CONSOLE_CODE, code);
				}	
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	%>
</div>