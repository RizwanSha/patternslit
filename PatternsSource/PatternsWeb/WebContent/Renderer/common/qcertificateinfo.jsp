<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<web:bundle baseName="patterns.config.web.forms.common.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/common/qcertificateinfo.js" />
	</web:dependencies>
	<web:loadPK />
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="qcertificateinfo.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/common/qcertificateinfo" id="qcertificateinfo" method="POST">
				<web:viewContent id="po_view1">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="220px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="qcertificateinfo.certinvnum" var="program" />
										</web:column>
										<web:column>
											<web:infoMessageID id="fileInventoryNum" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="qcertificateinfo.status" var="program" />
										</web:column>
										<web:column>
											<web:infoMessageID id="status" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
						</web:sectionBlock>
					</web:dividerBlock>
				</web:viewContent>
				<web:viewContent id="po_view2">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="220px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="qcertificateinfo.certversion" var="program" />
										</web:column>
										<web:column>
											<web:infoMessageID id="certVersion" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="qcertificateinfo.certserial" var="program" />
										</web:column>
										<web:column>
											<web:infoMessageID id="certSerial" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="qcertificateinfo.certdn" var="program" />
										</web:column>
										<web:column>
											<web:infoMessageID id="certDn" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="qcertificateinfo.certissdn" var="program" />
										</web:column>
										<web:column>
											<web:infoMessageID id="certIssDn" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="qcertificateinfo.signalg" var="program" />
										</web:column>
										<web:column>
											<web:infoMessageID id="certSigAlg" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="qcertificateinfo.certvalid" var="program" />
										</web:column>
										<web:column>
											<web:infoMessageID id="certNotBefore" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="qcertificateinfo.certexpires" var="program" />
										</web:column>
										<web:column>
											<web:infoMessageID id="certNotAfter" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="qcertificateinfo.filename" var="program" />
										</web:column>
										<web:column>
											<web:infoMessageID id="fileName" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
						</web:sectionBlock>
					</web:dividerBlock>
				</web:viewContent>
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
</web:fragment>