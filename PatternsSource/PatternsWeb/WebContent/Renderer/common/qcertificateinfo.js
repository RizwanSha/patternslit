var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'QCERTIFICATEINFO';
function init() {
	clearFields();
	var rosaerr = _primaryKey.split('|');
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('CERT_INV_NUM', rosaerr[1]);
	validator.setClass('patterns.config.web.forms.common.qcertificateinfobean');
	validator.setMethod('getCertificateInfo');
	validator.sendAndReceive();
	if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		$('#fileInventoryNum').html(validator.getValue('CERT_INV_NUM'));
		$('#certVersion').html(validator.getValue('CERT_VERSION'));
		$('#certSerial').html(validator.getValue('CERT_SERIAL'));
		$('#certDn').html(validator.getValue('CERT_SUBJ_DN'));
		$('#certIssDn').html(validator.getValue('CERT_ISS_DN'));
		$('#certSigAlg').html(validator.getValue('CERT_SIG_ALG'));
		$('#certNotBefore').html(validator.getValue('CERT_NOT_BEFORE'));
		$('#certNotAfter').html(validator.getValue('CERT_NOT_AFTER'));
		$('#fileName').html(validator.getValue('CERT_FILE_NAME'));
		$('#status').html(validator.getValue('CERT_IN_USE'));
	}
}
function clearFields() {
	$('#certVersion').html(EMPTY_STRING);
	$('#certSerial').html(EMPTY_STRING);
	$('#certDn').html(EMPTY_STRING);
	$('#certIssDn').html(EMPTY_STRING);
	$('#certSigAlg').html(EMPTY_STRING);
	$('#certNotBefore').html(EMPTY_STRING);
	$('#certNotAfter').html(EMPTY_STRING);
	$('#fileName').html(EMPTY_STRING);
	$('#status').html(EMPTY_STRING);
}
function loadData() {

}