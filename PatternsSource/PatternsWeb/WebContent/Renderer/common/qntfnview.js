var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'QNTFNVIEW';

var contentBackup = EMPTY_STRING;
var inlineGridNotifications = null;
var defaultView = 'U';

function init() {
	viewUnread();
}

function doDelete() {
	var rowID = getOneSelectedRecord(inlineGridNotifications);
	var PK_VALUE = EMPTY_STRING;
	if (rowID) {
		PK_VALUE = inlineGridNotifications.cells(rowID, 1).getValue() + '|' + inlineGridNotifications.cells(rowID, 4).getValue();
		validator.reset();
		validator.setClass('patterns.config.web.forms.common.qntfnviewbean');
		validator.setValue('PK', PK_VALUE);
		validator.setMethod('deleteNotification');
		validator.sendAndReceive();
		if (validator.getValue(RESULT) == DATA_AVAILABLE) {
			alert(NOTIFICATION_DELETED);
			doRefresh();
		} else {
			alert(validator.getValue(ERROR));
		}
	}
}

function doRefresh() {
	if (defaultView == 'U') {
		viewUnread();
	} else {
		viewAll();
	}
}

function loadAllNotifications(_args) {
	inlineGridNotifications = loadInlineQueryGrid(CURRENT_PROGRAM_ID, 'QNTFNVIEW_MAIN', 'inlineGridNotifications', _args);
	inlineGridNotifications.attachEvent("onRowCreated", highlightRow);
	inlineGridNotifications.attachEvent("onRowDblClicked", doRowDblClicked);
}

function doRowDblClicked(rowID) {
	inlineGridNotifications.cells(rowID, 0).setValue(true);
	viewNotifications();
}

function highlightRow(rowID, rowObject) {
	if (this.cells(rowID, 5).getValue() == 'R') {
		this.setRowTextStyle(rowID, EMPTY_STRING);
	} else {
		this.setRowTextStyle(rowID, 'color:red');
	}
}

function viewNotifications() {
	clearNotificationDetails();
	var rowID = getOneSelectedRecord(inlineGridNotifications);
	var PK_VALUE = EMPTY_STRING;
	if (rowID) {
		inlineGridNotifications.setRowTextStyle(rowID, EMPTY_STRING);
		PK_VALUE = inlineGridNotifications.cells(rowID, 1).getValue() + '|' + inlineGridNotifications.cells(rowID, 4).getValue();
		if (!document.getElementById('notificationContainer')) {
			var notificationContainer = document.createElement('DIV');
			notificationContainer.id = 'notificationContainer';
			document.body.appendChild(notificationContainer);
		}
		contentBackup = $('#notificationDivision').html();
		$('#notificationContainer').html($('#notificationDivision').html());
		$('#notificationDivision').html(EMPTY_STRING);
		showContent('alertViewer', 'notificationContainer', WidgetConstants.MODAL_WIDTH, WidgetConstants.MODAL_HEIGHT, ALERT_NOTIFICATION_VIEWER);
		validator.reset();
		validator.setClass('patterns.config.web.forms.common.qntfnviewbean');
		validator.setValue('PK', PK_VALUE);
		validator.setMethod('fetchNotficationDetails');
		validator.sendAndReceiveAsync(processNotificationDetails);
	}
}

function processNotificationDetails() {
	if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		$('#referenceNumber').val(validator.getValue('NTFNDLVRYLIST_INV_NUM'));
		$('#dateTime').val(validator.getValue('NTFNINVENTORY_DATETIME'));
		$('#subject').val(validator.getValue('TEMPLATE_SUB'));
		$('#body').val(validator.getValue('TEMPLATE_BODY'));
	}
}

function clearFields() {
	clearNotificationDetails();
}

function clearNotificationDetails() {
	if (!isEmpty(contentBackup)) {
		$('#notificationDivision').html(contentBackup);
	}
	hideElement('notificationDivision');
	$('#referenceNumber').val(EMPTY_STRING);
	$('#dateTime').val(EMPTY_STRING);
	$('#subject').val(EMPTY_STRING);
	$('#body').val(EMPTY_STRING);
}

function viewUnread() {
	clearFields();
	disableElement('cmdUnread');
	enableElement('cmdAll');
	defaultView = 'U';
	loadAllNotifications('U');
}

function viewAll() {
	clearFields();
	enableElement('cmdUnread');
	disableElement('cmdAll');
	defaultView = 'R';
	loadAllNotifications('R');
}