var validator = new xmlHTTPValidator();
function init() {
	$('.main-content').addClass('login-bg');
	$('#password').focus();
	if ($('#multiSessionCheck').val() == COLUMN_ENABLE) {
		var result = confirm(MULTIPLE_SESSION_LOGIN);
		if (!result) {
			window.location = getBasePath() + EUNAUTHACCESS_JSP;
		}
	}
}

function clearFields() {
	$('#password').val(EMPTY_STRING);
	$('#password_error').html(EMPTY_STRING);

}

function validate(id) {
	switch (id) {
	case 'password':
		password_val();
		setFocusOnSubmit();
		break;
	}
}

function password_val() {
	var value = $('#password').val();
	clearError('password_error');
	if (isEmpty(value)) {
		setError('password_error', MANDATORY);
		return false;
	}
	return true;
}

function revalidate() {
	if (!password_val()) {
		errors++;
	}
	if (errors == 0) {
		$('#userID').val($('#userID').val().toUpperCase());
		var hashedPassword = sha256_digest($('#password').val());
		var hashedUserID = sha256_digest(hashedPassword + $('#userID').val());
		var hashedSalt = sha256_digest(hashedUserID + $('#passwordSalt').val());
		var hashedDetail = sha256_digest(hashedSalt + $('#randomSalt').val());
		$('#password').val(EMPTY_STRING);
		$('#action').val(ADD);
		$('#hashedPassword').val(hashedDetail);
	}
}

function getIgnoreElements() {
	return {
		'password' : '1',
		'passwordSalt' : '1'
	};
}