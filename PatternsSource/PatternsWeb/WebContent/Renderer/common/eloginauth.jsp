<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.common.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/common/eloginauth.js" />
		<web:script src="public/scripts/sha256.js" />
	</web:dependencies>
	<web:viewPartInline>
		<web:viewSubPart>
			<web:form action="/Renderer/common/eloginauth" id="elogin" method="POST">
				<table width="100%" align="center" cellpadding="0">
					<tr>
						<td align="center">
							<table width="100%" align="right" cellpadding="0">
								<tr>
									<td width="40%" height="460px">&nbsp;</td>
									<td width=""></td>
									<td style="vertical-align: top; padding: 50px;" width="35%" height="100%"><web:viewPart>
											<web:viewSubPart>
												<web:viewContent id="po_view1">
													<web:dividerBlock>
														<web:sectionBlock width="width-100p" align="left">
															<web:section>
																<web:table>
																	<web:columnGroup>
																		<web:columnStyle width="140px" />
																		<web:columnStyle />
																	</web:columnGroup>
																	<web:rowHeading>
																		<web:column span="2">
																			<web:heading key="elogin.programtitle" var="program" />
																		</web:column>
																	</web:rowHeading>

																	<web:rowOdd>
																		<web:column>
																			<web:legend key="form.userid" var="common"  mandatory="true" />
																		</web:column>
																		<web:column></web:column>
																	</web:rowOdd>
																	<web:rowEven>
																		<web:column>
																			<type:loginuser property="userID" id="userID" readOnly="true" />
																		</web:column>
																		<web:column></web:column>
																	</web:rowEven>
																	<web:rowOdd>
																		<web:column>
																			<web:legend key="form.password" var="common" mandatory="true" />
																		</web:column>
																		<web:column></web:column>
																	</web:rowOdd>
																	<web:rowEven>
																		<web:column>
																			<type:password property="password" id="password" />
																		</web:column>
																		<web:column></web:column>
																	</web:rowEven>
																	<web:rowOdd>
																		<web:column span="2">
																			<web:loginReset />
																		</web:column>
																	</web:rowOdd>
																</web:table>
															</web:section>
														</web:sectionBlock>
													</web:dividerBlock>
												</web:viewContent>
											</web:viewSubPart>
										</web:viewPart></td>
									<td width="10%"></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<web:element property="passwordSalt" />
				<web:element property="randomSalt" />
				<web:element property="tmpOrgId" />
				<web:element property="hashedPassword" />
				<web:element property="multiSessionCheck" />
				<web:element property="action" />
				<web:element property="command" />
			</web:form>
			<web:footer/>
		</web:viewSubPart>
	</web:viewPartInline>
</web:fragment>