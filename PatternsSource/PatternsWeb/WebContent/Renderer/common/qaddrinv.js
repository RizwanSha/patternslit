var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MADDRESS';
var key = EMPTY_STRING;
var PARENT_PROGRAM_ID = EMPTY_STRING;
var personDetails, isMergeReqd, sourceKey, returnType;
function init() {
	clearFields();
	loadAddressValues(_primaryKey);

}
function clearFields() {
	$('#addrInvNo').val(EMPTY_STRING);
	$('#addressType').val(EMPTY_STRING);
	$('#addressType_desc').html(EMPTY_STRING);
	$('#countryCodeDisp').val(EMPTY_STRING);
	$('#countryCodeDisp_desc').html(EMPTY_STRING);
	$('#addressLine1').val(EMPTY_STRING);
	$('#addressLine2').val(EMPTY_STRING);
	$('#addressLine3').val(EMPTY_STRING);
	$('#addressLine4').val(EMPTY_STRING);
	$('#geoStructure').val(EMPTY_STRING);
	$('#lastLableField').val(EMPTY_STRING);
	setCheckbox('localAddress', NO);
	setCheckbox('permanentAddress', NO);
	setCheckbox('addrForComm', NO);
	setCheckbox('officialUSe', NO);
	$('#po_view6').addClass('hidden');
	clearLabelFields();
}

function loadRecord(value) {
	LINK_PROGRAM_ID = 'MADDRESS';
	var parent_pk = value.split('||');
	PARENT_PRIMARY_KEY = parent_pk[0];
	args = parent_pk[1];
	if (SOURCE_VALUE == MAIN) {
		isMergeReqd = false;
		loadLinkedRecordData(loadAddressDetail, PARENT_PROGRAM_ID, 'MADDRESS_M', args);
	} else {
		isMergeReqd = true;
		loadLinkedRecordData(loadAddressDetail, PARENT_PROGRAM_ID, 'MADDRESS_M', args);
	}
}

function loadAddressDetail(person) {
	if (validator.getValue('ADDR_TYPE') != EMPTY_STRING) {
		$('#addrInvNo').val(validator.getValue('ADDR_INV_NO'));
		$('#addressType').val(validator.getValue('ADDR_TYPE'));
		$('#countryCodeDisp').val(validator.getValue('COUNTRY_CODE'));
		$('#addressLine1').val(validator.getValue('ADDRESS_LINE1'));
		$('#addressLine2').val(validator.getValue('ADDRESS_LINE2'));
		$('#addressLine3').val(validator.getValue('ADDRESS_LINE3'));
		$('#addressLine4').val(validator.getValue('ADDRESS_LINE4'));
		var perdetails = personDetails.split('|');
		setCheckbox('localAddress', perdetails[0]);
		setCheckbox('permanentAddress', perdetails[1]);
		setCheckbox('officialUse', perdetails[2]);
		setCheckbox('addrForComm', perdetails[3]);
		addressType_val();
	}
	if (isMergeReqd) {
		isMergeReqd = false;
		loadLinkedRecordData(loadAddressDetail, PARENT_PROGRAM_ID, 'MADDRESS_T', args);
	}
}

function addressType_val() {
	clearLabelFields();
	var addressType = $('#addressType').val();
	if (isEmpty(addressType)) {
		$('#addressType_desc').html(EMPTY_STRING);
		return false;
	}
	validator.clearMap();
	validator.setValue('ADDR_TYPE', addressType);
	validator.setValue('ACTION', USAGE);
	validator.setClass('patterns.config.web.forms.common.maddressbean');
	validator.setMethod('validateAddressTypeCode');
	validator.sendAndReceive();
	if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		$('#addressType_desc').html(validator.getValue("DESCRIPTION"));
		getAddressLabels();
	}
	return true;
}

function getAddressLabels() {
	var addressType = $('#addressType').val();
	validator.clearMap();
	validator.setValue('ADDR_TYPE', addressType);
	validator.setClass('patterns.config.web.forms.common.maddressbean');
	validator.setMethod('getAddressLabels');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('addressType_error', validator.getValue(ERROR));
	} else if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		$('#countryCodeDisp').val(validator.getValue('ADDR_COU_CODE'));
		$('#countryCodeDisp_desc').html(validator.getValue("COUNT_DESC"));
		$('#countryCode').val(validator.getValue('ADDR_COU_CODE'));
		$('#addressLabel1').html(validator.getValue('ADDR_LABEL_1'));
		$('#addressLabel2').html(validator.getValue('ADDR_LABEL_2'));
		$('#addressLabel3').html(validator.getValue('ADDR_LABEL_3'));
		$('#addressLabel4').html(validator.getValue('ADDR_LABEL_4'));
		$('#labelForCountryCode').html(validator.getValue("V_FIRST_GUS"));
		var datavalue = validator.getValue("V_GUS_STRUCTURE").split(";");
		$('#geoStructure').val(datavalue[0]);
		$('#geoUnitStructure').val(datavalue[0]);
		for (var count = 1; count < datavalue.length; count++) {
			$('#lastLableField').val($('#lastLableField').val() + datavalue[count] + "\n");
		}
	}
	setFocusLast('addressLine1');
	return true;
}
function clearLabelFields() {
	$('#addressType_error').html(EMPTY_STRING);
	$('#countryCodeDisp').val(EMPTY_STRING);
	$('#countryCodeDisp_desc').html(EMPTY_STRING);
	$('#lastLableField').val(EMPTY_STRING);
	$('#countryCode').val(EMPTY_STRING);
	$('#addressLabel1').html(EMPTY_STRING);
	$('#addressLabel2').html(EMPTY_STRING);
	$('#addressLabel3').html(EMPTY_STRING);
	$('#addressLabel4').html(EMPTY_STRING);
	$('#labelForCountryCode').html(EMPTY_STRING);
	$('#addressLabel1').html('Address #1');
	$('#addressLabel2').html('Address #2');
	$('#addressLabel3').html('Address #3');
	$('#addressLabel4').html('Address #4');
	$('#labelForCountryCode').html('Geo-Structure');
	$('#geoStructure').val(EMPTY_STRING);
}

function loadAddressValues(pk) {
	var key = pk.split(',');
	if (key.length == 5) {
		$('#po_view6').removeClass('hidden');
		SOURCE_VALUE = key[0];
		pk = key[1];
		personDetails = key[2];
		PARENT_PROGRAM_ID = key[3];
		sourceKey = key[4];
		if (sourceKey == 'PERSON_ID') {
			$('#po_view6').removeClass('hidden');
		} else {
			$('#po_view6').addClass('hidden');
		}
		loadRecord(pk);
	} else if (key.length == 1) {
		$('#po_view6').addClass('hidden');
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('INVENTORY_NUMBER', pk);
		validator.setClass('patterns.config.web.forms.common.maddressbean');
		validator.setMethod('loadAddressDetails');
		validator.sendAndReceive();
		if (validator.getValue(RESULT) == DATA_AVAILABLE) {
			$('#addrInvNo').val(validator.getValue("ADDR_INV_NO"));
			$('#addressType').val(validator.getValue("ADDR_TYPE"));
			$('#addressType_desc').html(validator.getValue("ADDR_DESC"));
			$('#countryCodeDisp').val(validator.getValue("COUNTRY_CODE"));
			$('#countryCodeDisp_desc').html(validator.getValue("COUNT_DESC"));
			$('#addressLine1').val(validator.getValue("ADDRESS_LINE1"));
			$('#addressLine2').val(validator.getValue("ADDRESS_LINE2"));
			$('#addressLine3').val(validator.getValue("ADDRESS_LINE3"));
			$('#addressLine4').val(validator.getValue("ADDRESS_LINE4"));
			$('#geoStructure').val(validator.getValue("GEO_UNIT_ID"));
			addressType_val();
		}
	}
}
