<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<web:bundle baseName="patterns.config.web.forms.common.Messages" var="program" />
<web:fragment>
<web:dependencies>
		<web:script src="Renderer/common/eresult.js" />
	</web:dependencies>
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="eresult.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:viewContent id="po_view1">
				<web:dividerBlock>
					<web:sectionBlock width="width-100p" align="left">
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="100%" align="center" />
								</web:columnGroup>
								<web:rowOdd>
									<web:column>
										<web:displayStatus />
									</web:column>
								</web:rowOdd>
								<web:rowOdd>
									<web:column>
										<web:displayRedirectLink />
									</web:column>
								</web:rowOdd>
								<web:rowOdd>
									<web:column>
										<web:printRequestAdditionalInfo styleClass="note" />
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
					</web:sectionBlock>
				</web:dividerBlock>
			</web:viewContent>
		</web:viewSubPart>
	</web:viewPart>
</web:fragment>
