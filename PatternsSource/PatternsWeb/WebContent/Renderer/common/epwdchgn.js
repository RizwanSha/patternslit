var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'EPWDCHGN';
function init() {
	$('#spanPasswordPolicy').html($('#passwordPolicy').val());
	$('#loginCurrentPassword').focus();
}
function loginCurrentPassword_val(valMode) {
	var value = $('#loginCurrentPassword').val();
	clearError('loginCurrentPassword_error');
	if (isEmpty(value)) {
		setError('loginCurrentPassword_error', MANDATORY);
		setFocusLast('loginCurrentPassword');
		return false;
	}
	if (valMode) {
		setFocusLast('loginNewPassword');
	}
	return true;
}

function loginNewPassword_val(valMode) {
	var value = $('#loginNewPassword').val();
	clearError('loginNewPassword_error');
	if (isEmpty(value)) {
		setError('loginNewPassword_error', MANDATORY);
		setFocusLast('loginNewPassword');
		return false;
	}
	if ($('#loginCurrentPassword').val() == $('#loginNewPassword').val()) {
		setError('loginNewPassword_error', NEW_OLD_PASSWORD);
		setFocusLast('loginNewPassword');
		return false;
	}
	if (!checkPasswordCriteria(value, $('#minLength').val(), $('#minAlpha').val(), $('#minNumeric').val(), $('#minSpecial').val())) {
		setError('loginNewPassword_error', PASSWORD_POLICY);
		setFocusLast('loginNewPassword');
		return false;
	}
	if (valMode) {
		setFocusLast('loginRenewPassword');
	}
	return true;
}

function loginRenewPassword_val(valMode) {
	var value = $('#loginRenewPassword').val();
	clearError('loginRenewPassword_error');
	if (isEmpty(value)) {
		setError('loginRenewPassword_error', MANDATORY);
		setFocusLast('loginRenewPassword');
		return false;
	}

	if ($('#loginRenewPassword').val() != $('#loginNewPassword').val()) {
		setError('loginRenewPassword_error', PWD_SAME);
		setFocusLast('loginRenewPassword');
		return false;
	}
	if (valMode) {
		setFocusOnSubmit();
	}
	return true;
}

function validate(id) {
	valMode = true;
	switch (id) {
	case 'loginCurrentPassword':
		loginCurrentPassword_val(valMode);
		break;
	case 'loginNewPassword':
		loginNewPassword_val(valMode);
		break;
	case 'loginRenewPassword':
		loginRenewPassword_val(valMode);
		break;
	}
}

function backtrack(id) {
	switch (id) {
	case 'loginCurrentPassword':
		setFocusLast('loginCurrentPassword');
		break;
	case 'loginNewPassword':
		setFocusLast('loginCurrentPassword');
		break;
	case 'loginRenewPassword':
		setFocusLast('loginNewPassword');
		break;

	}
}

function revalidate() {
	if (!loginCurrentPassword_val(false)) {
		errors++;
	}
	if (!loginNewPassword_val(false)) {
		errors++;
	}
	if (!loginRenewPassword_val(false)) {
		errors++;
	}
	if (errors == 0) {
		var userID = getUserID().toUpperCase();
		var hashedCurrentPassword = sha256_digest($('#loginCurrentPassword').val());
		var hashedCurrentUserID = sha256_digest(hashedCurrentPassword + userID);
		var hashedCurrentSalt = sha256_digest(hashedCurrentUserID + $('#loginCurrentPasswordSalt').val());
		var hashedCurrentDetail = sha256_digest(hashedCurrentSalt + $('#randomSalt').val());
		$('#loginCurrentPassword').val(EMPTY_STRING);
		$('#loginCurrentPasswordHashed').val(hashedCurrentDetail);

		var hashedNewPassword = sha256_digest($('#loginNewPassword').val());
		var hashedNewUserID = sha256_digest(hashedNewPassword + userID);
		$('#loginNewPassword').val(EMPTY_STRING);
		$('#loginRenewPassword').val(EMPTY_STRING);
		$('#loginNewPasswordHashed').val(hashedNewUserID);

		$('#action').val(ADD);
	}
}