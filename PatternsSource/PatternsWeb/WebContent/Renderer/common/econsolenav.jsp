<%@page import="patterns.config.framework.web.configuration.menu.MenuUtils"%>

<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.common.Messages" var="program" />
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<div class="table_no_wrap">
<table class="form_table_inner" width="100%">

	<%
		try {
			String consoleCode = request.getParameter("consoleCode").toString();
			java.util.ArrayList<patterns.config.framework.web.GenericOption> programList = null;
			MenuUtils menu = new MenuUtils();
			if (consoleCode != patterns.config.framework.database.RegularConstants.EMPTY_STRING) {
				session.setAttribute( patterns.config.framework.web.SessionConstants.CONSOLE_CODE, consoleCode);
				programList = menu.getProgramList(consoleCode);
				int rowCount = 1;
				for (patterns.config.framework.web.GenericOption option : programList) {
					String id = option.getId();
					String label = option.getLabel();
					try{
					if (rowCount % 2 == 0) {
	%>
	<tr  height="26">
		<td align="left">
			<web:linkrenderer title="<%=label %>" forward="<%=id.toLowerCase()%>" label="<%=label%>" styleClass="program-link"/>
		</td>
	</tr>
	<%
		} else {
	%>
	<tr  height="26">
		<td align="left">
			<web:linkrenderer title="<%=label %>" forward="<%=id.toLowerCase()%>" label="<%=label%>" styleClass="program-link" />
		</td>
	</tr>
	<%
		}}catch(Exception e){
			
		}
					rowCount++;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	%>
</table>
</div>