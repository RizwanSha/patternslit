<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.common.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/common/qntfnview.js" />
	</web:dependencies>
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="qntfnview.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/common/qntfnview" id="qntfnview" method="POST">
				<web:viewContent id="po_view1">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle width="180px" />
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column span="4">
											<type:button id="cmdNotification" key="qntfnview.viewnotification" var="program" onclick="viewNotifications()" />
											<type:button id="cmdUnread" key="qntfnview.viewunread" var="program" onclick="viewUnread()" />
											<type:button id="cmdAll" key="qntfnview.viewread" var="program" onclick="viewAll()" />
											<type:button id="cmdDelete" key="qntfnview.delete" var="program" onclick="doDelete()" />
											<type:button id="cmdRefresh" key="qntfnview.refresh" var="program" onclick="doRefresh()" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:rowOdd>
										<web:column>
											<web:viewContent id="po_view4">
												<web:inlineQueryGrid height="250" width="450" id="inlineGridNotifications" />
											</web:viewContent>
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
						</web:sectionBlock>
					</web:dividerBlock>
				</web:viewContent>
				<web:viewContent id="notificationDivision" styleClass="hidden">
					<web:section>
						<web:table>
							<web:columnGroup>
								<web:columnStyle width="220px" />
								<web:columnStyle />
							</web:columnGroup>
							<web:rowOdd>
								<web:column>
									<web:legend key="qntfnview.referencenumber" var="program" />
								</web:column>
								<web:column>
									<type:inputCodeDisplay property="referenceNumber" id="referenceNumber" />
								</web:column>
							</web:rowOdd>
							<web:rowEven>
								<web:column>
									<web:legend key="qntfnview.datetime" var="program" />
								</web:column>
								<web:column>
									<type:dateTimeDisplay property="dateTime" id="dateTime" />
								</web:column>
							</web:rowEven>
							<web:rowOdd>
								<web:column>
									<web:legend key="qntfnview.subject" var="program" />
								</web:column>
								<web:column>
									<type:templateSubjectDisplay property="subject" id="subject" />
								</web:column>
							</web:rowOdd>
							<web:rowEven>
								<web:column>
									<web:legend key="qntfnview.body" var="program" />
								</web:column>
								<web:column>
									<type:templateBodyDisplay property="body" id="body" />
								</web:column>
							</web:rowEven>
						</web:table>
					</web:section>
				</web:viewContent>
				<web:element property="command" />
				<web:element property="action" />
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
	<web:contextSearch />
</web:fragment>