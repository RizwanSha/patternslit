<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.common.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/common/maddress.js" />
		<web:script src="public/scripts/component.js" />
	</web:dependencies>
	<web:loadPK />
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="maddress.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/common/maddress" id="maddress" method="POST">
				<web:viewContent id="po_view1">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:viewContent id="person" styleClass="hidden">
								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="180px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowOdd>
											<web:column>
												<web:legend key="maddress.personId" var="program"/>
											</web:column>
											<web:column>
												<type:personIdDisplay property="personId" id="personId" />
											</web:column>
										</web:rowOdd>
									</web:table>
								</web:section>
								</web:viewContent>
								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="360px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowEven>
											<web:column>
												<web:viewContent id="gridDetails">
													<web:grid height="150px" width="920px" id="innerGrid" src="common/maddress_innerGrid.xml">
													</web:grid>
												</web:viewContent>
											</web:column>
										</web:rowEven>
									</web:table>
								</web:section>
							<br>
							<type:tabbarBody id="address_TAB_0">
								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="360px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowOdd>
											<web:column>
												<web:legend key="maddress.oldAddress" var="program" />
											</web:column>
											<web:column>
												<type:addressLine property="oldAddressLine1" id="oldAddressLine1" />
											</web:column>
										</web:rowOdd>
										<web:rowEven>
											<web:column>
											</web:column>
											<web:column>
												<type:addressLine property="oldAddressLine2" id="oldAddressLine2" />
											</web:column>
										</web:rowEven>
										<web:rowOdd>
											<web:column>
											</web:column>
											<web:column>
												<type:addressLine property="oldAddressLine3" id="oldAddressLine3" />
											</web:column>
										</web:rowOdd>
										<web:rowEven>
											<web:column>
											</web:column>
											<web:column>
												<type:addressLine property="oldAddressLine4" id="oldAddressLine4" />
											</web:column>
										</web:rowEven>
										<web:rowOdd>
											<web:column>
											</web:column>
											<web:column>
												<type:addressLine property="oldAddressLine5" id="oldAddressLine5" />
											</web:column>
										</web:rowOdd>
										<web:rowEven>
											<web:column span="2">
												<type:button key="form.updateaddress" id="updateAddress" var="common" onclick="updateOldAddress()" />
												<type:button key="form.cancel" id="cancel" var="common" onclick="clearDependentFields()" />
											</web:column>
										</web:rowEven>
									</web:table>
								</web:section>
							</type:tabbarBody>
							<type:tabbarBody id="address_TAB_1">
								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="360px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowEven>
											<web:column>
												<web:legend key="maddress.addrinvno" var="program" mandatory="true" />
											</web:column>
											<web:column>
												<type:numberDisplay property="addrInvNo" id="addrInvNo" size="10" length="10" />
											</web:column>
										</web:rowEven>
										<web:rowOdd>
											<web:column>
												<web:legend key="maddress.addresstype" var="program" mandatory="true" />
											</web:column>
											<web:column>
												<type:addressTypeCode property="addressType" id="addressType" />
											</web:column>
										</web:rowOdd>
										<web:rowEven>
											<web:column>
												<web:legend key="maddress.countrycode" var="program" />
											</web:column>
											<web:column>
												<type:countryCodeDisplay property="countryCodeDisp" id="countryCodeDisp" />
											</web:column>
										</web:rowEven>
										<web:rowOdd>
											<web:column>
												<div class="control-label textalign-left">
													<label id="addressLabel1"> <web:legend key="maddress.addressline1" var="program" />
													</label> <span style="color: red">*</span>
												</div>
											</web:column>
											<web:column>
												<type:addressLine property="addressLine1" id="addressLine1" />
											</web:column>
										</web:rowOdd>
										<web:rowEven>
											<web:column>
												<div class="control-label textalign-left">
													<label id="addressLabel2"> <web:legend key="maddress.addressline2" var="program" />
													</label> <span style="color: red">*</span>
												</div>
											</web:column>
											<web:column>
												<type:addressLine property="addressLine2" id="addressLine2" />
											</web:column>
										</web:rowEven>
										<web:rowOdd>
											<web:column>
												<div class="control-label textalign-left">
													<label id="addressLabel3"> <web:legend key="maddress.addressline3" var="program" />
													</label> <span style="color: red">*</span>
												</div>
											</web:column>
											<web:column>
												<type:addressLine property="addressLine3" id="addressLine3" />
											</web:column>
										</web:rowOdd>
										<web:rowEven>
											<web:column>
												<div class="control-label textalign-left">
													<label id="addressLabel4"> <web:legend key="maddress.addressline4" var="program" />
													</label> <span style="color: red">*</span>
												</div>
											</web:column>
											<web:column>
												<type:addressLine property="addressLine4" id="addressLine4" />
											</web:column>
										</web:rowEven>
										<web:rowOdd>
											<web:column>
												<div class="control-label textalign-left">
													<label id="labelForCountryCode"><web:legend key="maddress.geostructure" var="program" /> </label>
												</div>
											</web:column>
											<web:column>
												<type:dynamicDescriptionDisplay length="30" size="25" property="geoStructure" id="geoStructure" />
											</web:column>
										</web:rowOdd>
										<web:rowOdd>
											<web:column>
											</web:column>
											<web:column>
												<type:remarksDisplay property="lastLableField" id="lastLableField" />
											</web:column>
										</web:rowOdd>
									</web:table>
								</web:section>
								<web:viewContent id="personAddrDetails" styleClass="hidden">
									<web:section>
										<web:table>
										<web:columnGroup>
											<web:columnStyle width="360px" />
											<web:columnStyle />
										</web:columnGroup>
											<web:rowEven>
												<web:column>
													<web:legend key="maddress.localaddress" var="program" />
												</web:column>
												<web:column>
													<type:checkbox property="localAddress" id="localAddress" />
												</web:column>
											</web:rowEven>
											<web:rowOdd>
												<web:column>
													<web:legend key="maddress.permanentaddress" var="program" />
												</web:column>
												<web:column>
													<type:checkbox property="permanentAddress" id="permanentAddress" />
												</web:column>
											</web:rowOdd>
											<web:rowEven>
												<web:column>
													<web:legend key="maddress.officialuse" var="program" />
												</web:column>
												<web:column>
													<type:checkbox property="officialUse" id="officialUse" />
												</web:column>
											</web:rowEven>
											<web:rowOdd>
												<web:column>
													<web:legend key="maddress.addrForComm" var="program" />
												</web:column>
												<web:column>
													<type:checkbox property="addrForComm" id="addrForComm" />
												</web:column>
											</web:rowOdd>
										</web:table>
									</web:section>
								</web:viewContent>
								<web:section>
									<web:table>
										<web:rowEven>
											<web:column span="2">
												<type:button key="form.addtoaddress" id="addToAddress" var="common" onclick="updateInventory()" />
												<type:button key="form.cancel" id="cancel" var="common" onclick="cancelCreatedAddress()"/>
											</web:column>
										</web:rowEven>
									</web:table>
								</web:section>
							</type:tabbarBody>
							<type:tabbarBody id="address_TAB_2">
								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="360px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowOdd>
											<web:column>
												<web:legend key="maddress.personId" var="program"  />
											</web:column>
											<web:column>
												<type:personId property="personSearch" id="personSearch" />
											</web:column>
										</web:rowOdd>
										<web:rowEven>
											<web:column>
												<web:legend key="maddress.addressportion" var="program" />
											</web:column>
											<web:column>
												<type:dynamicDescription length="30" size="25" property="addressPortion" id="addressPortion" />
											</web:column>
										</web:rowEven>
										<web:rowOdd>
											<web:column>
												<web:legend key="maddress.pincode" var="program" />
											</web:column>
											<web:column>
												<type:dynamicDescription length="30" size="25" property="pinCode" id="pinCode" />
											</web:column>
										</web:rowOdd>
									</web:table>
								</web:section>
								<web:section>
									<web:table>
										<web:rowEven>
											<web:column span="2">
												<web:grid height="250px" width="890px" id="searchGrid" src="common/maddress_searchGrid.xml">
												</web:grid>
											</web:column>
										</web:rowEven>
									</web:table>
								</web:section>
								<web:viewContent id="personExistAddrDetails" styleClass="hidden">
									<web:section>
										<web:table>
										<web:columnGroup>
											<web:columnStyle width="360px" />
											<web:columnStyle />
										</web:columnGroup>
											<web:rowOdd>
												<web:column>
													<web:legend key="maddress.localaddress" var="program" />
												</web:column>
												<web:column>
													<type:checkbox property="localAddressExist" id="localAddressExist" />
												</web:column>
											</web:rowOdd>
											<web:rowEven>
												<web:column>
													<web:legend key="maddress.permanentaddress" var="program" />
												</web:column>
												<web:column>
													<type:checkbox property="permanentAddressExist" id="permanentAddressExist" />
												</web:column>
											</web:rowEven>
											<web:rowOdd>
												<web:column>
													<web:legend key="maddress.officialuse" var="program" />
												</web:column>
												<web:column>
													<type:checkbox property="officialUseExist" id="officialUseExist" />
												</web:column>
											</web:rowOdd>
											<web:rowEven>
												<web:column>
													<web:legend key="maddress.addrForComm" var="program" />
												</web:column>
												<web:column>
													<type:checkbox property="addrForCommExist" id="addrForCommExist" />
												</web:column>
											</web:rowEven>
										</web:table>
									</web:section>
								</web:viewContent>
								<web:section>
									<web:table>
										<web:rowOdd>
											<web:column span="2">
												<type:button key="form.usethisaddress" id="useThisAddress" var="common" onclick="useExistingAddress()" />
												<type:button key="form.cancel" id="cancel" var="common" onclick="cancelExistingAddress()"/>
											</web:column>
										</web:rowOdd>
									</web:table>
								</web:section>
							</type:tabbarBody>
							<type:tabbar height="height :790px;" name="Old Address$$Create/Edit New Address$$Use Existing Address" width="width : 1050px;" required="3" id="address" selected="1"></type:tabbar>
							<web:viewContent id="po_view5" styleClass="hidden">
								<web:section>
									<web:table>
										<web:rowOdd>
											<web:column>
												<web:grid height="150px" width="760px" id="detailGrid" src="common/maddress_detailGrid.xml">
												</web:grid>
											</web:column>
										</web:rowOdd>
									</web:table>
								</web:section>
							</web:viewContent>
						</web:sectionBlock>
					</web:dividerBlock>
				</web:viewContent>
				<web:element property="command" />
				<web:element property="action" />
				<web:element property="xmladdress" />
				<web:element property="conversationID" />
				<web:element property="inventoryNumber" />
				<web:element property="geoUnitStructure" />
				<web:element property="countryCode" />
				<web:element property="countryCode_desc" />
				<web:element property="operationStatus" />
				<web:element property="currentTab" />
				<web:element property="parentProgramId" />
				<web:popupKeys />
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
	<web:contextSearch />
</web:fragment>