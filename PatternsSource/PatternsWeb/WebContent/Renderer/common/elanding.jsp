<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="resource" tagdir="/WEB-INF/tags/resource"%>
<web:bundle baseName="patterns.config.web.forms.common.Messages" var="program" />
<web:fragment>
	<web:viewPart>
		<web:dependencies>
		</web:dependencies>


		<web:viewSubPart>
			<web:sectionBlock width="width-100p" align="left">
			<div id="customerReg" style="width: 100%; height: 100%; align: left">
						<table style="width: 100%; height: 100%; table-layout: fixed">
							<tr style="height:100px;">
								<td width="13%" style="padding-left:25px">
									<i class="fa fa-group" style=" color: #375b6d;font-size: 60px;" ></i>
								</td>
								<td>
									<Span class="h4"> <web:legend key="dashboard.staff" var="program" /></Span><br>
									<span class="console-description"> <web:message key="dashboard.staffDesc" var="program" /> </span>
								</td>
								<td width="13%" style="padding-left:25px">
									<i class="fa fa-money" style=" color: #375b6d;font-size: 60px;" ></i>
								</td>
								<td>
									<Span class="h4"> <web:legend key="dashboard.lease" var="program" /></Span><br>
									<span class="console-description"> <web:message key="dashboard.leaseDesc" var="program" /> </span>
								</td>
							</tr>
							
							<tr style="height:100px;">
								<td width="13%" style="padding-left:25px">
									<i class="fa fa-gears" style=" color: #375b6d;font-size: 60px;" ></i>
								</td>
								<td>
									<Span class="h4"> <web:legend key="dashboard.access" var="program" /></Span><br>
									<span class="console-description"> <web:message key="dashboard.accessDesc" var="program" /> </span>
								</td>
								<td width="13%" style="padding-left:25px">
									<i class="fa fa-newspaper-o" style=" color: #375b6d;font-size: 60px;" ></i>
								</td>
								<td>
									<Span class="h4"> <web:legend key="dashboard.invoice" var="program" /></Span><br>
									<span class="console-description"> <web:message key="dashboard.invoiceDesc" var="program" /> </span>
								</td>
							</tr>
							
							<tr style="height:100px;">
								<td width="13%" style="padding-left:25px">
									<i class="fa fa-list" style=" color: #375b6d;font-size: 60px;" ></i>
								</td>
								<td>
									<Span class="h4"> <web:legend key="dashboard.commonCodes" var="program" /></Span><br>
									<span class="console-description"> <web:message key="dashboard.commonCodesDesc" var="program" /> </span>
								</td>
								<td width="13%" style="padding-left:25px">
									<i class="fa fa-pencil-square-o" style=" color: #375b6d;font-size: 60px;" ></i>
								</td>
								<td>
									<Span class="h4"> <web:legend key="dashboard.reg" var="program" /></Span><br>
									<span class="console-description"> <web:message key="dashboard.regDesc" var="program" /> </span>
								</td>
							</tr>
							
						</table>
					</div>
							</web:sectionBlock>
		</web:viewSubPart>
	</web:viewPart>
</web:fragment>
