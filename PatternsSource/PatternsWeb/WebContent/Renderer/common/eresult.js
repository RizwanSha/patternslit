var validator = new xmlHTTPValidator();
var printValidator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'ERESULT';

function init(){
	if($('#printRequired').val()==1){
		$('#cmdPrint').focus();
	}
	else if($('#backToTemplate').val()==1){
		$('#cmdBackToTemplate').focus();
	}
	else{
		$('#cmdForwardPath').focus();
	}
	
	if($('#additionalDetailInfo').val()!=null){
		$('#additionalDetailInfoDiv').append($('#additionalDetailInfo').val());
	}
}

function redirectToEntry() {
	if($('#callSource').val()=="C"){
		//window.location = getBasePath() + $('#forwardPath').val() + "?callSource="+$('#callSource').val();
		closeChildWindow();
	}	
	else
		window.location = getBasePath() + $('#forwardPath').val();
}

function backToTemplateEntry(){
	if($('#callSource').val()=="C")
		//window.location = getBasePath() + $('#forwardPath').val() + "?callSource="+$('#callSource').val()+ "&primaryKey="+$('#primaryKey').val()+"&sourceKey="+$('#sourceKey').val();
		closeChildWindow();
	else 
		window.location = getBasePath() + $('#forwardPath').val() + "?primaryKey="+$('#primaryKey').val()+"&sourceKey="+$('#sourceKey').val()+"&backToTemplate="+$('#backToTemplate').val();
}

function print(){
	disableElement('cmdPrint');
	showMessage('eresult', Message.PROGRESS);
	printValidator.clearMap();
	printValidator.setMtm(false);
	printValidator.setValue('primaryKey', $('#primaryKey').val());
	printValidator.setClass($('#beanPath').val());
	printValidator.setMethod('processPrintRequest');
	printValidator.sendAndReceiveAsync(processDownloadResult);
}

function processDownloadResult() {
	enableElement('cmdPrint');
	if (printValidator.getValue(ERROR) != null && printValidator.getValue(ERROR) != EMPTY_STRING) {
		showMessage('eresult', Message.ERROR, printValidator.getValue(ERROR));
		return false;
	} else {
		showReportLink(printValidator);
		return true;
	}
}

function closeChildWindow(){
	parent.dhxWindows.window($('#forwardName').val()).close();
	//parent.closeInlinePopUp(parent.dhxWindows.window($('#forwardName').val()));
}

function showBatch(batchDate,batchNo){
	//alert(batchDate);
	//alert(batchNo);
	var paramValue=EMPTY_STRING;
	if(!isEmpty(batchDate) && !isEmpty(batchNo))
		paramValue = PK + '=' + getEntityCode()+PK_SEPERATOR+getBranchCode()+PK_SEPERATOR+batchDate+PK_SEPERATOR+batchNo;
	else
		paramValue=EMPTY_STRING;
	win=showWindow('QCTRAN', getBasePath() + '/Renderer/tran/qctran.jsp', PBS_QCTRAN_VIEW, paramValue, true, false, false,1230,640);
}
