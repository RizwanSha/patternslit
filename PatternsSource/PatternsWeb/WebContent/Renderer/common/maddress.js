var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MADDRESS';
var selectedRowID;
var rowId, recordType = EMPTY_STRING;
var requestType = EMPTY_STRING;
var sourceKey = EMPTY_STRING;
var returnType = EMPTY_STRING;
var sourceValue = EMPTY_STRING;
var parentAction = EMPTY_STRING;
var tabbarObject;
var currentSessionReq = true;
var addedOn, modOn = EMPTY_STRING;
function init() {
	PARENT_PROGRAM_ID = $('#parentProgramId').val();
	CGQM_CONV_ID = $('#conversationID').val();
	PARENT_PRIMARY_KEY = $('#parentPrimaryKey').val();
	LINK_PROGRAM_ID = CURRENT_PROGRAM_ID;
	LINK_PRIMARY_KEY = EMPTY_STRING;
	SOURCE_VALUE = $('#requestType').val();
	returnType = $('#returnType').val();
	sourceKey = $('#sourceKey').val();
	sourceValue = $('#sourceValue').val();
	parentAction = $('#parentAction').val();
	clearFields();
	if ($('#parentAction').val() == MODIFY) {
		currentSessionReq = false;
	}
	if (sourceKey == 'PERSON_ID') {
		address.tabs('address_1').enable();
		$('#person').removeClass('hidden');
		$('#personAddrDetails').removeClass('hidden');
		$('#personExistAddrDetails').removeClass('hidden');
		$('#personId').val(sourceValue);
		personId_val();
	} else {
		$('#person').addClass('hidden');
		$('#personAddrDetails').addClass('hidden');
		$('#personExistAddrDetails').addClass('hidden');
		$('#inventoryNumber').val(sourceValue);
		address.tabs('address_0').hide(true);
		address.tabs('address_0').disable();
		address.tabs('address_2').hide(true);
		address.tabs('address_2').disable();
	}
	$('#action').val(ADD);
	if (returnType == 'M') {
		$('#gridDetails').removeClass('hidden');
		loadGrid();
	} else if (returnType == 'S') {
		$('#gridDetails').addClass('hidden');
		loadRecord(sourceValue);
	}
	innerGrid.attachEvent("onMouseOver", function(rid, ind) {
		if ((ind == 10) || (ind == 11))
			rowId = rid;
	});
	tabbarObject = address;
	$('#addressType').focus();
}

function doHelp(id) {
	switch (id) {
	case 'addressType':
		help('COMMON', 'HLP_ADDR_TYPE_M', $('#addressType').val(), '', $('#addressType'));
		break;
	case 'personSeach':
		help('COMMON', 'HLP_PERSON_DB', $('#personSeach').val(), '', $('#personSeach'));
		break;
	}
}
function clearFields() {
	clearDependentFields(true);
	searchGrid.clearAll();
	innerGrid.clearAll();
	detailGrid.clearAll();
}

function validate(id) {
	switch (id) {
	case 'personId':
		personId_val();
		break;
	case 'addressType':
		addressType_val(true);
		break;
	case 'addressLine1':
		addressLine1_val();
		break;
	case 'addressLine2':
		addressLine2_val();
		break;
	case 'addressLine3':
		addressLine3_val();
		break;
	case 'addressLine4':
		addressLine4_val();
		break;
	case 'oldAddressLine1':
		oldAddressLine1_val();
		break;
	case 'oldAddressLine2':
		oldAddressLine2_val();
		break;
	case 'oldAddressLine3':
		oldAddressLine3_val();
		break;
	case 'oldAddressLine4':
		oldAddressLine4_val();
		break;
	case 'oldAddressLine5':
		oldAddressLine5_val();
		break;
	case 'addressPortion':
		addressPortion_val();
		break;
	case 'pinCode':
		pinCode_val();
		break;
	case 'personSearch':
		personSearch_val();
		break;
	case 'localAddress':
		validateCheckbox();
		setFocusLast('permanentAddress');
		break;
	case 'permanentAddress':
		validateCheckbox();
		setFocusLast('officialUse');
		break;
	case 'officialUse':
		validateCheckbox();
		setFocusLast('addrForComm');
		break;
	case 'addrForComm':
		validateCheckbox();
		setFocusLast('addToAddress');
		break;
	case 'localAddressExist':
		validateCheckboxForExist();
		setFocusLast('permanentAddressExist');
		break;
	case 'permanentAddressExist':
		validateCheckboxForExist();
		setFocusLast('officialUseExist');
		break;
	case 'officialUseExist':
		validateCheckboxForExist();
		setFocusLast('addrForCommExist');
		break;
	case 'addrForCommExist':
		validateCheckboxForExist();
		setFocusLast('useThisAddress');
		break;
	}
}
function backtrack(id) {
	switch (id) {
	case 'addressType':
		setFocusLast('addressType');
		break;
	case 'addressLine1':
		setFocusLast('addressType');
		break;
	case 'addressLine2':
		setFocusLast('addressLine1');
		break;
	case 'addressLine3':
		setFocusLast('addressLine2');
		break;
	case 'addressLine4':
		setFocusLast('addressLine3');
		break;
	case 'localAddress':
		setFocusLast('addressLine4');
		break;
	case 'permanentAddress':
		setFocusLast('localAddress');
		break;
	case 'officialUse':
		setFocusLast('permanentAddress');
		break;
	case 'addrForComm':
		setFocusLast('officialUse');
		break;
	case 'addToAddress':
		setFocusLast('addrForComm');
		break;
	case 'cancel':
		setFocusLast('addToAddress');
		break;
	case 'oldAddressLine1':
		setFocusLast('oldAddressLine1');
		break;
	case 'oldAddressLine2':
		setFocusLast('oldAddressLine1');
		break;
	case 'oldAddressLine3':
		setFocusLast('oldAddressLine2');
		break;
	case 'oldAddressLine4':
		setFocusLast('oldAddressLine3');
		break;
	case 'oldAddressLine5':
		setFocusLast('oldAddressLine4');
		break;
	case 'updateAddress':
		setFocusLast('oldAddressLine5');
		break;
	case 'personSearch':
		setFocusLast('personSearch');
		break;
	case 'addressPortion':
		setFocusLast('personSearch');
		break;
	case 'pinCode':
		setFocusLast('addressPortion');
		break;
	case 'localAddressExist':
		setFocusLast('pinCode');
		break;
	case 'permanentAddressExist':
		setFocusLast('localAddressExist');
		break;
	case 'officialUseExist':
		setFocusLast('permanentAddressExist');
		break;
	case 'addrForCommExist':
		setFocusLast('officialUseExist');
		break;
	case 'useThisAddress':
		setFocusLast('addrForCommExist');
		break;
	}
}
function loadGrid() {
	innerGrid.clearAll();
	LINK_PROGRAM_ID = CURRENT_PROGRAM_ID;
	if (sourceKey == 'PERSON_ID')
		PK_VALUE = getPartitionNo() + PK_SEPERATOR + PARENT_PRIMARY_KEY;
	else
		PK_VALUE = PARENT_PRIMARY_KEY;
	var TOKEN_ID = EMPTY_STRING;
	if (SOURCE_VALUE == MAIN) {
		TOKEN_ID = 'MADDRESS_GRID_M';
	} else if (SOURCE_VALUE == TBA) {
		TOKEN_ID = 'MADDRESS_GRID_T';
	} else {
		TOKEN_ID = 'MADDRESS_GRID_M';
	}
	if (currentSessionReq && PARENT_PRIMARY_KEY == EMPTY_STRING) {
		SOURCE_VALUE = 'S';
		var detailxml = window.parent.$("#xmlAddressDetailList").val();
		if (detailxml != EMPTY_STRING && detailxml != null) {
			loadComponentGridQuery(PARENT_PROGRAM_ID, 'MADDRESS_GRID_P', latestdataProcessing);
		}
	} else {
		if (((SOURCE_VALUE == TBA) && (parentAction == 'M')) || SOURCE_VALUE == PROGRAM) {
			gridMerging(true);
		} else {
			loadComponentGridQuery(PARENT_PROGRAM_ID, TOKEN_ID, postProcessing);
		}
	}
}
function loadRecord(value) {
	if (value != EMPTY_STRING) {
		$('#action').val(parentAction);
		LINK_PROGRAM_ID = CURRENT_PROGRAM_ID;
		args = getPartitionNo() + PK_SEPERATOR + value;
		currentSessionReq = true;
		loadLinkedRecordData(loadAddressDetail, PARENT_PROGRAM_ID, 'MADDRESS_M', args);
	}
}
function loadAddressDetail() {
	if (validator.getValue('ADDR_TYPE') != EMPTY_STRING) {
		$('#addrInvNo').val(validator.getValue('ADDR_INV_NO'));
		$('#addressType').val(validator.getValue('ADDR_TYPE'));
		$('#countryCodeDisp').val(validator.getValue('COUNTRY_CODE'));
		$('#addressLine1').val(validator.getValue('ADDRESS_LINE1'));
		$('#addressLine2').val(validator.getValue('ADDRESS_LINE2'));
		$('#addressLine3').val(validator.getValue('ADDRESS_LINE3'));
		$('#addressLine4').val(validator.getValue('ADDRESS_LINE4'));
		addressType_val(false);
	}
	if (currentSessionReq) {
		SOURCE_VALUE = TBA;
		currentSessionReq = false;
		loadLinkedRecordData(loadAddressDetail, CURRENT_PROGRAM_ID, 'MADDRESS_T', args);
	}
}

function postProcessing(result) {
	detailGrid.clearAll();
	$xml = $($.parseXML(result));
	($xml).find("row").each(function() {
		try {
			var invNumber = $(this).find("cell")[1].textContent;
			var addrType = $(this).find("cell")[2].textContent;
			var countryCode = $(this).find("cell")[3].textContent;
			var details = $(this).find("cell")[4].textContent;
			var location = getLocation($(this).find("cell")[3].textContent);
			var editLink = $(this).find("cell")[6].textContent;
			var removeLink = $(this).find("cell")[7].textContent;
			var recordType = $(this).find("cell")[8].textContent;
			var crOn = $(this).find("cell")[9].textContent;
			var modOn = $(this).find("cell")[10].textContent;
			var locAddr = $(this).find("cell")[11].textContent;
			var permAddr = $(this).find("cell")[12].textContent;
			var offAddr = $(this).find("cell")[13].textContent;
			var commAddr = $(this).find("cell")[14].textContent;
			var addrTypeDesc = $(this).find("cell")[15].textContent;
			var countryCodeDesc = $(this).find("cell")[16].textContent;
			var gridValues = [ '', invNumber, addrType, addrTypeDesc, countryCode, countryCodeDesc, details, location, crOn, modOn, editLink, removeLink, recordType, locAddr, permAddr, offAddr, commAddr ];
			var values = [ invNumber, locAddr, permAddr, offAddr, commAddr, EMPTY_STRING, crOn, modOn ];
			innerGrid.addRow(invNumber, gridValues);
			detailGrid.addRow(invNumber, values);
		} catch (e) {
		}
	});
	if (!currentSessionReq) {
		SOURCE_VALUE = 'S';
		var detailxml = window.parent.$("#xmlAddressDetailList").val();
		if (detailxml != EMPTY_STRING && detailxml != null) {
			loadComponentGridQuery(PARENT_PROGRAM_ID, 'MADDRESS_GRID_P', latestdataProcessing);
		}
	}
}
function gridMerging(isPersonRequest) {
	if (isPersonRequest) {
		loadComponentGridQuery(PARENT_PROGRAM_ID, 'MADDRESS_GRID_M', personGridProcessing);
	}
}
function personGridProcessing(result) {
	currentSessionReq = true;
	postProcessing(result);
	currentSessionReq = false;
	loadComponentGridQuery(PARENT_PROGRAM_ID, 'MADDRESS_GRID_T', dataProcessing);
}

function latestdataProcessing(result) {
	currentSessionReq = true;
	var detailxml = window.parent.$("#xmlAddressDetailList").val();
	if (detailxml != EMPTY_STRING && detailxml != null) {
		dataProcessing(result);
		var oldaddress = detailxml.substring(0, 2);
		if (oldaddress == 'O$') {
			return;
		} else {
			detailGrid.parse(detailxml);
			$xml = $($.parseXML(detailxml));
			$xml.find("row").each(function() {
				try {
					var invNumber = $(this).find("cell")[0].textContent;
					var localAddr = $(this).find("cell")[1].textContent;
					var permAddr = $(this).find("cell")[2].textContent;
					var offAddr = $(this).find("cell")[3].textContent;
					var commAddr = $(this).find("cell")[4].textContent;
					var oprType = $(this).find("cell")[5].textContent;
					var crOn = $(this).find("cell")[6].textContent;
					var moOn = $(this).find("cell")[7].textContent;
					if (innerGrid.doesRowExist(invNumber)) {
						if (oprType == 'R') {
							innerGrid.deleteRow(invNumber);
						} else {
							innerGrid.cells(invNumber, 8).setValue(crOn);
							innerGrid.cells(invNumber, 9).setValue(moOn);
							innerGrid.cells(invNumber, 12).setValue(oprType);
							innerGrid.cells(invNumber, 13).setValue(localAddr);
							innerGrid.cells(invNumber, 14).setValue(permAddr);
							innerGrid.cells(invNumber, 15).setValue(offAddr);
							innerGrid.cells(invNumber, 16).setValue(commAddr);
						}
					}
				} catch (e) {
				}
			});
		}
	}
	return;
}

function removedAddressProcessing(result) {
	$xml = $($.parseXML(result));
	$xml.find("row").each(function() {
		try {
			var invNumber = $(this).find("cell")[1].textContent;
			if (innerGrid.doesRowExist(invNumber)) {
				innerGrid.deleteRow(invNumber);
			}
			if (detailGrid.doesRowExist(invNumber)) {
				detailGrid.cells(invNumber, 5).setValue('R');
			}
		} catch (e) {
		}
	});
	return;
}
function dataProcessing(result) {
	$xml = $($.parseXML(result));
	($xml).find("row").each(function() {
		try {
			var invNumber = $(this).find("cell")[1].textContent;
			var addrType = $(this).find("cell")[2].textContent;
			var countryCode = $(this).find("cell")[3].textContent;
			var details = $(this).find("cell")[4].textContent;
			var location = getLocation($(this).find("cell")[3].textContent);
			var editLink = $(this).find("cell")[6].textContent;
			var removeLink = $(this).find("cell")[7].textContent;
			var recordType = $(this).find("cell")[8].textContent;
			var crOn = $(this).find("cell")[9].textContent;
			var modOn = $(this).find("cell")[10].textContent;
			var locAddr = $(this).find("cell")[11].textContent;
			var permAddr = $(this).find("cell")[12].textContent;
			var offAddr = $(this).find("cell")[13].textContent;
			var commAddr = $(this).find("cell")[14].textContent;
			var addrTypeDesc = $(this).find("cell")[15].textContent;
			var countryCodeDesc = $(this).find("cell")[16].textContent;
			var gridValues = [ '', invNumber, addrType, addrTypeDesc, countryCode, countryCodeDesc, details, location, crOn, modOn, editLink, removeLink, recordType, locAddr, permAddr, offAddr, commAddr ];
			var values = [ invNumber, locAddr, permAddr, offAddr, commAddr, EMPTY_STRING, crOn, modOn ];
			if (innerGrid.doesRowExist(invNumber)) {
				innerGrid.deleteRow(invNumber);
				innerGrid.addRow(invNumber, gridValues);
				detailGrid.deleteRow(invNumber);
				detailGrid.addRow(invNumber, values);
			} else {
				innerGrid.addRow(invNumber, gridValues);
				detailGrid.addRow(invNumber, values);
			}
		} catch (e) {
		}
	});
	if (SOURCE_VALUE != MAIN) {
		SOURCE_VALUE = TBA;
		loadComponentGridQuery(CURRENT_PROGRAM_ID, 'MADDRESS_RMVD_T', removedAddressProcessing);
	}
	if (!currentSessionReq) {
		SOURCE_VALUE = 'S';
		var detailxml = window.parent.$("#xmlAddressDetailList").val();
		if (detailxml != EMPTY_STRING && detailxml != null) {
			loadComponentGridQuery(CURRENT_PROGRAM_ID, 'MADDRESS_GRID_P', latestdataProcessing);
		}
	}

}
function getLocation(value) {
	validator.clearMap();
	validator.setValue('COUNTRY_CODE', value);
	validator.setClass('patterns.config.web.forms.common.maddressbean');
	validator.setMethod('getLocation');
	validator.sendAndReceive();
	var location = EMPTY_STRING;
	if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		var datavalue = validator.getValue("V_GUS_STRUCTURE").split(";");
		for (var count = 0; count < datavalue.length; count++) {
			location = location + datavalue[count] + "\n";
		}
		return location;
	}
	return value;
}

function personId_val() {
	var value = $('#personId').val();
	if (!isEmpty(value)) {
		validator.clearMap();
		validator.setValue('PERSON_ID', value);
		validator.setValue('ACTION', USAGE);
		validator.setClass('patterns.config.web.forms.common.maddressbean');
		validator.setMethod('validatePersonId');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('addressType_error', validator.getValue(ERROR));
			return false;
		}
		if (validator.getValue(RESULT) == DATA_UNAVAILABLE) {
			address.tabs('address_0').disable();
			$('#action').val(ADD);
			$('#currentTab').val('address_1');
			return true;
		} else {
			loadOldAddress();
		}
	} else {
		address.tabs('address_0').disable();
		$('#action').val(ADD);
		$('#currentTab').val('address_1');
	}
	return true;
}
function loadOldAddress() {
	validator.clearMap();
	validator.setValue('PERSON_ID', $('#personId').val());
	validator.setValue('ACTION', USAGE);
	validator.setClass('patterns.config.web.forms.common.maddressbean');
	validator.setMethod('loadOldAddress');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('oldAddressLine1_error', validator.getValue(ERROR));
		return false;
	}
	if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		$('#oldAddressLine1').val(validator.getValue('OLD_ADDRESS1'));
		$('#oldAddressLine2').val(validator.getValue('OLD_ADDRESS2'));
		$('#oldAddressLine3').val(validator.getValue('OLD_ADDRESS3'));
		$('#oldAddressLine4').val(validator.getValue('OLD_ADDRESS4'));
		$('#oldAddressLine5').val(validator.getValue('OLD_ADDRESS5'));
		address.tabs('address_0').show(true);
		address.tabs('address_0').enable();
	} else {
		address.tabs('address_0').hide(true);
		address.tabs('address_0').disable();
	}
	return true;
}

function updateOldAddress() {
	if (revalidate) {
		address.tabs('address_1').disable();
		address.tabs('address_2').disable();
		var oldAddress = $('#oldAddressLine1').val() + '###' + $('#oldAddressLine2').val() + '###' + $('#oldAddressLine3').val() + '###' + $('#oldAddressLine4').val() + '###' + $('#oldAddressLine5').val();
		var xml = 'O' + '$' + oldAddress;
		window.parent.updateAddress(xml);
		alert("Old Address Updated");
	}
	// close this window
}
function gridDuplicateCheck(value) {
	return _grid_duplicate_check(innerGrid, value, [ 1 ]);
}
function addressType_val(clear) {
	clearLabelFields();
	if (clear)
		clearDependentFields(false);
	var addressType = $('#addressType').val();
	if (isEmpty(addressType)) {
		$('#addressType_desc').html(EMPTY_STRING);
		setError('addressType_error', HMS_MANDATORY);
		return false;
	}
	validator.clearMap();
	validator.setValue('ADDR_TYPE', addressType);
	validator.setValue('ACTION', USAGE);
	validator.setClass('patterns.config.web.forms.common.maddressbean');
	validator.setMethod('validateAddressTypeCode');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('addressType_error', validator.getValue(ERROR));
	} else if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		$('#addressType_desc').html(validator.getValue("DESCRIPTION"));
		getAddressLabels();
	}
	return true;
}

function getAddressLabels() {
	var addressType = $('#addressType').val();
	validator.clearMap();
	validator.setValue('ADDR_TYPE', addressType);
	validator.setClass('patterns.config.web.forms.common.maddressbean');
	validator.setMethod('getAddressLabels');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('addressType_error', validator.getValue(ERROR));
	} else if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		$('#countryCodeDisp').val(validator.getValue('ADDR_COU_CODE'));
		$('#countryCodeDisp_desc').html(validator.getValue("COUNT_DESC"));
		$('#countryCode').val(validator.getValue('ADDR_COU_CODE'));
		$('#countryCode_desc').val(validator.getValue('COUNT_DESC'));
		$('#addressLabel1').html(validator.getValue('ADDR_LABEL_1'));
		$('#addressLabel2').html(validator.getValue('ADDR_LABEL_2'));
		$('#addressLabel3').html(validator.getValue('ADDR_LABEL_3'));
		$('#addressLabel4').html(validator.getValue('ADDR_LABEL_4'));
		$('#labelForCountryCode').html(validator.getValue("V_FIRST_GUS"));
		var datavalue = validator.getValue("V_GUS_STRUCTURE").split(";");
		$('#geoStructure').val(datavalue[0]);
		$('#geoUnitStructure').val(validator.getValue('GEO_UNIT_STRUC_CODE'));
		for (var count = 1; count < datavalue.length; count++) {
			$('#lastLableField').val($('#lastLableField').val() + datavalue[count] + "\n");
		}
	}
	setFocusLast('addressLine1');
	return true;
}
function clearLabelFields() {
	$('#addressType_error').html(EMPTY_STRING);
	$('#countryCodeDisp').val(EMPTY_STRING);
	$('#countryCodeDisp_desc').html(EMPTY_STRING);
	$('#lastLableField').val(EMPTY_STRING);
	$('#countryCode').val(EMPTY_STRING);
	$('#countryCode_desc').val(EMPTY_STRING);
	$('#addressLabel1').html(EMPTY_STRING);
	$('#addressLabel2').html(EMPTY_STRING);
	$('#addressLabel3').html(EMPTY_STRING);
	$('#addressLabel4').html(EMPTY_STRING);
	$('#labelForCountryCode').html(EMPTY_STRING);
	$('#addressLabel1').html('Address #1');
	$('#addressLabel2').html('Address #2');
	$('#addressLabel3').html('Address #3');
	$('#addressLabel4').html('Address #4');
	$('#labelForCountryCode').html('Geo-Structure');
	$('#geoStructure').val(EMPTY_STRING);
}
function pinCode_val() {
	var value = $('#pinCode').val();
	clearError('pinCode_error');
	if (!isEmpty(value)) {
		loadSearchGrid('PC');
		setFocusLast('localAddressExist');
		return true;
	} else {
		searchGrid.clearAll();
		setFocusLast('pinCode');
		return true;
	}
}

function addressPortion_val() {
	var value = $('#addressPortion').val();
	clearError('addressPortion_error');
	if (!isEmpty(value)) {
		loadSearchGrid('AP');
		setFocusLast('localAddressExist');
		return true;
	} else {
		searchGrid.clearAll();
		setFocusLast('pinCode');
		return true;
	}
}

function personSearch_val() {
	var value = $('#personSearch').val();
	clearError('personSearch_error');
	if (!isEmpty(value)) {
		loadSearchGrid('PS');
		setFocusLast('localAddressExist');
		return true;
	} else {
		searchGrid.clearAll();
		setFocusLast('addressPortion');
		return true;
	}
}

function addressLine1_val() {
	var value = $('#addressLine1').val();
	clearError('addressLine1_error');
	if (isEmpty(value)) {
		setError('addressLine1_error', HMS_MANDATORY);
		return false;
	}
	if (!isValidDescription(value)) {
		setError('addressLine1_error', HMS_INVALID_DESCRIPTION);
		return false;
	}
	setFocusLast('addressLine2');
	return true;
}
function addressLine2_val() {
	var addr1 = $('#addressLine1').val();
	var value = $('#addressLine2').val();
	clearError('addressLine2_error');
	if (isEmpty(value)) {
		setError('addressLine2_error', HMS_MANDATORY);
		return false;
	}
	if (isEmpty(addr1) && (!isEmpty(value))) {
		setError('addressLine2_error', HMS_ABOVELINE_BLANK);
		return false;
	}
	if (!isEmpty(value)) {
		if (!isValidDescription(value)) {
			setError('addressLine2_error', HMS_INVALID_DESCRIPTION);
			return false;
		}
	}
	setFocusLast('addressLine3');
	return true;
}
function addressLine3_val() {
	var addr2 = $('#addressLine2').val();
	var value = $('#addressLine3').val();
	clearError('addressLine3_error');
	if (isEmpty(addr2) && (!isEmpty(value))) {
		setError('addressLine3_error', HMS_ABOVELINE_BLANK);
		return false;
	}
	if (!isEmpty(value)) {
		if (!isValidDescription(value)) {
			setError('addressLine3_error', HMS_INVALID_DESCRIPTION);
			return false;
		}
	}
	setFocusLast('addressLine4');
	return true;
}
function addressLine4_val() {
	var addr3 = $('#addressLine3').val();
	var value = $('#addressLine4').val();
	clearError('addressLine4_error');
	if (isEmpty(addr3) && (!isEmpty(value))) {
		setError('addressLine4_error', HMS_ABOVELINE_BLANK);
		return false;
	}
	if (!isEmpty(value)) {
		if (!isValidDescription(value)) {
			setError('addressLine4_error', HMS_INVALID_DESCRIPTION);
			return false;
		}
	}
	if (sourceKey == 'PERSON_ID')
		setFocusLast('localAddress');
	else
		setFocusLast('addToAddress');
	return true;
}

function oldAddressLine1_val() {
	var value = $('#oldAddressLine1').val();
	clearError('addressLine1_error');
	if (isEmpty(value)) {
		setError('oldAddressLine1_error', HMS_MANDATORY);
		return false;
	}
	if (!isValidDescription(value)) {
		setError('oldAddressLine1_error', HMS_INVALID_DESCRIPTION);
		return false;
	}
	setFocusLast('oldAddressLine2');
	return true;
}
function oldAddressLine2_val() {
	var addr1 = $('#oldAddressLine1').val();
	var value = $('#oldAddressLine2').val();
	clearError('oldAddressLine2_error');
	if (isEmpty(addr1) && (!isEmpty(value))) {
		setError('oldAddressLine2_error', HMS_MANDATORY);
		return false;
	}
	if (!isEmpty(value)) {
		if (!isValidDescription(value)) {
			setError('oldAddressLine2_error', HMS_INVALID_DESCRIPTION);
			return false;
		}
	}
	setFocusLast('oldAddressLine3');
	return true;
}
function oldAddressLine3_val() {
	var addr2 = $('#oldAddressLine2').val();
	var value = $('#oldAddressLine3').val();
	clearError('oldAddressLine3_error');
	if (isEmpty(addr2) && (!isEmpty(value))) {
		setError('oldAddressLine3_error', HMS_MANDATORY);
		return false;
	}
	if (!isEmpty(value)) {
		if (!isValidDescription(value)) {
			setError('oldAddressLine3_error', HMS_INVALID_DESCRIPTION);
			return false;
		}
	}
	setFocusLast('oldAddressLine4');
	return true;
}
function oldAddressLine4_val() {
	var addr3 = $('#oldAddressLine3').val();
	var value = $('#oldAddressLine4').val();
	clearError('oldAddressLine4_error');
	if (isEmpty(addr3) && (!isEmpty(value))) {
		setError('oldAddressLine4_error', HMS_MANDATORY);
		return false;
	}
	if (!isEmpty(value)) {
		if (!isValidDescription(value)) {
			setError('oldAddressLine4_error', HMS_INVALID_DESCRIPTION);
			return false;
		}
	}
	setFocusLast('oldAddressLine5');
	return true;
}
function oldAddressLine5_val() {
	var addr3 = $('#oldAddressLine4').val();
	var value = $('#oldAddressLine5').val();
	clearError('oldAddressLine5_error');
	if (isEmpty(addr3) && (!isEmpty(value))) {
		setError('oldAddressLine5_error', HMS_MANDATORY);
		return false;
	}
	if (!isEmpty(value)) {
		if (!isValidDescription(value)) {
			setError('oldAddressLine5_error', HMS_INVALID_DESCRIPTION);
			return false;
		}
	}
	setFocusLast('updateAddress');
	return true;
}

function removeAddress(invNumber) {
	$('#operationStatus').val('R');
	if (confirm(DELETE_CONFIRM)) {
		$('#action').val(MODIFY);
		$('#inventoryNumber').val(invNumber);
		recordType = innerGrid.cells(invNumber, 12).getValue();
		innerGrid.cells(invNumber, 12).setValue('R');
	}
	updateInventory();
}

function viewAddress(inventoryNumber) {
	openPopup('common/qaddrinv', inventoryNumber, 'Address');
	resetLoading();
}

function formLink(type, invNo) {
	var link;
	if (type == 'E') {
		link = "EDIT^javascript:editAddress(\"" + invNo + "\")";
	} else {
		link = "REMOVE^javascript:removeAddress(\"" + invNo + "\")";
	}
	return link;
}
function validateCheckbox() {
	clearError('addrForComm_error');
	if (!($('#localAddress').is(':checked') || $('#permanentAddress').is(':checked') || $('#officialUse').is(':checked') || $('#addrForComm').is(':checked'))) {
		setError('addrForComm_error', INVALID_CHECK);
		return false;
	}
	return true;
}
function validateCheckboxForExist() {
	clearError('addrForCommExist_error');
	if (!($('#localAddressExist').is(':checked') || $('#permanentAddressExist').is(':checked') || $('#officialUseExist').is(':checked') || $('#addrForCommExist').is(':checked'))) {
		setError('addrForCommExist_error', INVALID_CHECK);
		return false;
	}
	return true;
}
function useExistingAddress() {
	if (validateCheckboxForExist()) {
		var rowID = searchGrid.getCheckedRows(0);
		if (rowID == EMPTY_STRING) {
			alert(SELECT_ATLEAST_ONE);
			return false;
		} else {
			if (revalidate()) {
				var field_values = [ '', searchGrid.cells(rowID, 1).getValue(), searchGrid.cells(rowID, 2).getValue(), searchGrid.cells(rowID, 3).getValue(), searchGrid.cells(rowID, 4).getValue(), searchGrid.cells(rowID, 5).getValue(), searchGrid.cells(rowID, 6).getValue(),
						searchGrid.cells(rowID, 7).getValue(), getCBD(), '', formLink('E', searchGrid.cells(rowID, 1).getValue()), formLink('R', searchGrid.cells(rowID, 1).getValue()), '', decodeBTS($('#localAddressExist').is(':checked')), decodeBTS($('#permanentAddressExist').is(':checked')),
						decodeBTS($('#officialUseExist').is(':checked')), decodeBTS($('#addrForCommExist').is(':checked')) ];
				if (innerGrid.doesRowExist(searchGrid.cells(rowID, 1).getValue())) {
					if (confirm("Address Already Exist")) {
						innerGrid.deleteRow(searchGrid.cells(rowID, 1).getValue());
						detailGrid.deleteRow(searchGrid.cells(rowID, 1).getValue());
					}
				}
				innerGrid.addRow(searchGrid.cells(rowID, 1).getValue(), field_values);
				var values = [ searchGrid.cells(rowID, 1).getValue(), decodeBTS($('#localAddressExist').is(':checked')), decodeBTS($('#permanentAddressExist').is(':checked')), decodeBTS($('#officialUseExist').is(':checked')), decodeBTS($('#addrForCommExist').is(':checked')), 'A', getCBD(), '' ];
				detailGrid.addRow(searchGrid.cells(rowID, 1).getValue(), values);
				$('#operationStatus').val("U");
				$('#inventoryNumber').val(searchGrid.cells(rowID, 1).getValue());
				updateInventory();
				clearSearchGridFields();
			}
		}
	}
}

function loadSearchGrid(type) {
	searchGrid.clearAll();
	validator.clearMap();
	validator.setMtm(false);
	if (type == 'AP') {
		validator.setValue('SEARCH_CONTENT', $('#addressPortion').val());
	} else if (type == 'PC') {
		validator.setValue('SEARCH_CONTENT', $('#pinCode').val());
	} else {
		+validator.setValue('SEARCH_CONTENT', $('#personSearch').val());
	}
	validator.setValue('SEARCH_TYPE', type);
	validator.setClass('patterns.config.web.forms.common.maddressbean');
	validator.setMethod('loadSearchGrid');
	validator.sendAndReceive();
	if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		xmlString = validator.getValue(RESULT_XML);
		searchGrid.loadXMLString(xmlString);
	} else {
		setError('search_error', validator.getValue(ERROR));
		return false;
	}
	setFocusLast('localAddressExist');
	return true;
}

function clearSearchGridFields() {
	searchGrid.clearAll();
	$('#personSearch').val(EMPTY_STRING);
	$('#addressPortion').val(EMPTY_STRING);
	$('#pinCode').val(EMPTY_STRING);
	setCheckbox('localAddressExist', NO);
	setCheckbox('permanentAddressExist', NO);
	setCheckbox('officialUseExist', NO);
	setCheckbox('addrForCommExist', NO);
}
function doTabbarClick(id) {
	clearLabelFields();
	clearDependentFields(true);
	clearSearchGridFields();
	$('#currentTab').val(id);
	if (id == 'address_0') {
		var rows = innerGrid.getRowsNum();
		if (rows > 0) {
			address.tabs('address_0').disable();
		}
	}
	if (id == 'address_1') {
		$('#action').val(ADD);
	}
}

function editAddress(invNumber) {
	recordType = EMPTY_STRING;
	if ($('#currentTab').val() == 'address_2') {
		tabbarObject.goToPrevTab();
		$('#currentTab').val('address_1');
	}
	$('#operationStatus').val(EMPTY_STRING);
	clearDependentFields(true);
	$('#addrInvNo').val(innerGrid.cells(invNumber, 1).getValue());
	$('#inventoryNumber').val(innerGrid.cells(invNumber, 1).getValue());
	$('#addressType').val(innerGrid.cells(invNumber, 2).getValue());
	recordType = innerGrid.cells(invNumber, 12).getValue();
	addressType_val(true);
	var addressDetail = innerGrid.cells(invNumber, 6).getValue();
	var address;
	address = addressDetail.split(' , ');
	$('#addressLine1').val(address[0]);
	$('#addressLine2').val(address[1]);
	$('#addressLine3').val(address[2]);
	$('#addressLine4').val(address[3]);
	addedOn = innerGrid.cells(invNumber, 8).getValue();
	modOn = innerGrid.cells(invNumber, 9).getValue();
	setCheckbox('localAddress', innerGrid.cells(invNumber, 13).getValue());
	setCheckbox('permanentAddress', innerGrid.cells(invNumber, 14).getValue());
	setCheckbox('officialUse', innerGrid.cells(invNumber, 15).getValue());
	setCheckbox('addrForComm', innerGrid.cells(invNumber, 16).getValue());
	$('#inventoryNumber').val(innerGrid.cells(invNumber, 1).getValue());
	$('#action').val(MODIFY);
}

function updateInventory() {
	if (revalidate()) {
		validator.clearMap();
		validator.setMtm(false);
		validator.setClass('patterns.config.web.forms.common.maddressbean');
		validator.setMethod('updateInventory');
		validator.setValue('PARENT_PGM_ID', PARENT_PROGRAM_ID);
		validator.setValue('PARENT_PGM_PK', EMPTY_STRING);
		validator.setValue('LINKED_PGM_ID', CURRENT_PROGRAM_ID);
		validator.setValue('CONVERSATION_ID', $('#conversationID').val());
		if ($('#operationStatus').val() == 'U') {
			validator.setValue('OPERATION_TYPE', 'U');
			validator.setValue('OPERATION_STATUS', "U");
			validator.setValue('INVENTORY_NO', $('#inventoryNumber').val());
		} else if ($('#operationStatus').val() == 'R') {
			validator.setValue('OPERATION_TYPE', 'R');
			validator.setValue('OPERATION_STATUS', "R");
			validator.setValue('INVENTORY_NO', $('#inventoryNumber').val());
		} else if ($('#action').val() == 'A') {
			validator.setValue('OPERATION_TYPE', 'A');
			validator.setValue('OPERATION_STATUS', "");
			validator.setValue('INVENTORY_NO', EMPTY_STRING);
		} else {
			if ((recordType == EMPTY_STRING) && (returnType == 'M')) {
				validator.setValue('OPERATION_TYPE', 'A');
			} else {
				validator.setValue('OPERATION_TYPE', 'M');
			}
			validator.setValue('OPERATION_STATUS', $('#operationStatus').val());
			validator.setValue('INVENTORY_NO', $('#inventoryNumber').val());
		}
		validator.setValue('ACTIVE_TAB', $('#currentTab').val());
		validator.setValue('PERSON_ID', $('#personId').val());
		validator.setValue('ADDR_TYPE', $('#addressType').val());
		validator.setValue('COUNTRY_CODE', $('#countryCode').val());
		validator.setValue('ADDR_LINE1', $('#addressLine1').val());
		validator.setValue('ADDR_LINE2', $('#addressLine2').val());
		validator.setValue('ADDR_LINE3', $('#addressLine3').val());
		validator.setValue('ADDR_LINE4', $('#addressLine4').val());
		validator.setValue('GEO_UNIT_ID', $('#geoUnitStructure').val());
		validator.sendAndReceive();
		if (validator.getValue(RESULT) == DATA_AVAILABLE) {
			$('#inventoryNumber').val(validator.getValue("INVENTORY_NO"));
			if ($('#action').val() == 'A') {
				updateResponse($('#inventoryNumber').val());
			} else if ($('#action').val() == 'M') {
				updateResponse($('#inventoryNumber').val());
			}
			if (returnType == 'M') {
				window.parent.updateAddress(detailGrid.serialize(),innerGrid.getRowsNum());
				
			} else if (returnType == 'S') {
				window.parent.updateAddress($('#inventoryNumber').val());
			}
			alert('INVENTORY UPDATED SUCCESSFULLY');
			$('#inventoryNumber').val(EMPTY_STRING);
		} else {
			alert('INVENTORY UPDATION ERROR');
		}
	}
}
function updateResponse(formResponse) {
	if ($('#operationStatus').val() != 'U') {
		if ($('#action').val() == ADD) {
			var address = $('#addressLine1').val() + ' , ' + $('#addressLine2').val() + ' , ' + $('#addressLine3').val() + ' , ' + $('#addressLine4').val();
			var location = $('#geoStructure').val() + $('#lastLableField').val();
			var field_values = [ '', formResponse, $('#addressType').val(), $('#addressType_desc').html(), $('#countryCode').val(), $('#countryCode_desc').val(), address, location, getCBD(), '', formLink('E', formResponse), formLink('R', formResponse), '',
					decodeBTS($('#localAddress').is(':checked')), decodeBTS($('#permanentAddress').is(':checked')), decodeBTS($('#officialUse').is(':checked')), decodeBTS($('#addrForComm').is(':checked')) ];
			innerGrid.addRow(formResponse, field_values);
			$('#addressType').focus();
			var values = [ formResponse, decodeBTS($('#localAddress').is(':checked')), decodeBTS($('#permanentAddress').is(':checked')), decodeBTS($('#officialUse').is(':checked')), decodeBTS($('#addrForComm').is(':checked')), 'A', getCBD(), '' ];
			detailGrid.addRow(formResponse, values);
			clearDependentFields(true);
		} else if ($('#action').val() == MODIFY) {
			if ($('#operationStatus').val() == EMPTY_STRING) {
				var address = $('#addressLine1').val() + ' , ' + $('#addressLine2').val() + ' , ' + $('#addressLine3').val() + ' , ' + $('#addressLine4').val();
				var location = $('#geoStructure').val() + $('#lastLableField').val();
				var field_values = [ '', formResponse, $('#addressType').val(), $('#addressType_desc').html(), $('#countryCode').val(), $('#countryCode_desc').val(), address, location, addedOn, getCBD(), formLink('E', formResponse), formLink('R', formResponse), recordType,
						decodeBTS($('#localAddress').is(':checked')), decodeBTS($('#permanentAddress').is(':checked')), decodeBTS($('#officialUse').is(':checked')), decodeBTS($('#addrForComm').is(':checked')) ];
				innerGrid.deleteRow($('#inventoryNumber').val());
				innerGrid.addRow($('#inventoryNumber').val(), field_values);
				if (recordType == 'M' || recordType == 'T') {
					var values = [ formResponse, decodeBTS($('#localAddress').is(':checked')), decodeBTS($('#permanentAddress').is(':checked')), decodeBTS($('#officialUse').is(':checked')), decodeBTS($('#addrForComm').is(':checked')), 'M', addedOn, getCBD() ];
					detailGrid.deleteRow($('#inventoryNumber').val());
					detailGrid.addRow($('#inventoryNumber').val(), values);
				} else if (recordType == '') {
					var values = [ formResponse, decodeBTS($('#localAddress').is(':checked')), decodeBTS($('#permanentAddress').is(':checked')), decodeBTS($('#officialUse').is(':checked')), decodeBTS($('#addrForComm').is(':checked')), 'A', getCBD(), '' ];
					detailGrid.deleteRow($('#inventoryNumber').val());
					detailGrid.addRow($('#inventoryNumber').val(), values);
				}
				clearDependentFields(true);
				$('#addressType').focus();
			} else if ($('#operationStatus').val() == 'R') {
				detailGrid.cells(formResponse, 5).setValue('R');
				if (recordType == EMPTY_STRING) {
					detailGrid.deleteRow(formResponse);
				}
				innerGrid.deleteRow(formResponse);
			}
		}
	}
	$('#action').val(ADD);
	$('#operationStatus').val(EMPTY_STRING);
}

function clearDependentFields(addrType) {
	$('#addrInvNo').val(EMPTY_STRING);
	if (addrType) {
		$('#addressType').val(EMPTY_STRING);
		$('#addressType_desc').html(EMPTY_STRING);
		clearError('addressType_error');
	}
	$('#countryCodeDisp').val(EMPTY_STRING);
	$('#countryCodeDisp_desc').html(EMPTY_STRING);
	$('#addressLine1').val(EMPTY_STRING);
	clearError('addressLine1_error');
	$('#addressLine2').val(EMPTY_STRING);
	clearError('addressLine2_error');
	$('#addressLine3').val(EMPTY_STRING);
	clearError('addressLine3_error');
	$('#addressLine4').val(EMPTY_STRING);
	clearError('addressLine4_error');
	$('#geoStructure').val(EMPTY_STRING);
	clearError('geoStructure_error');
	setCheckbox('localAddress', NO);
	setCheckbox('permanentAddress', NO);
	setCheckbox('officialUse', NO);
	setCheckbox('addrForComm', NO);
	clearLabelFields();
}
function revalidate() {
	var error = 0;
	if ($('#currentTab').val() == 'address_0') {
		if (!oldAddressLine1_val()) {
			error++;
		}
		if (!oldAddressLine2_val()) {
			error++;
		}
		if (!oldAddressLine3_val()) {
			error++;
		}
		if (!oldAddressLine4_val()) {
			error++;
		}
		if (!oldAddressLine5_val()) {
			error++;
		}
	} else if ($('#currentTab').val() == 'address_1') {
		if ($('#operationStatus').val() != 'R') {
			if (!addressLine1_val()) {
				error++;
			}
			if (!addressLine2_val()) {
				error++;
			}
			if (!addressLine3_val()) {
				error++;
			}
			if (!addressLine4_val()) {
				error++;
			}
			if (!validateCheckbox()) {
				error++;
			}
		}
	} else if ($('#currentTab').val() == 'address_0') {
		if ($('#operationStatus').val() != 'R') {
			var rowID = searchGrid.getCheckedRows(0);
			if (rowID == EMPTY_STRING) {
				errors++;
			}
			if (!validateCheckboxForExist()) {
				error++;
			}
		}
	}
	if (error == 0) {
		return true;
	} else {
		return false;
	}
}