<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.common.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/common/qaddress.js" />
		<web:script src="public/scripts/component.js" />
	</web:dependencies>
	<web:loadPK />
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="qaddress.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:viewContent id="po_view1">
				<web:dividerBlock>
					<web:sectionBlock width="width-100p" align="left">
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="180px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowOdd>
									<web:column>
										<web:legend key="maddress.personId" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:personIdDisplay property="personId" id="personId" />
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="180px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:viewContent id="po_view4">
											<web:grid height="250px" width="810px" id="innerGrid" src="common/qaddress_innerGrid.xml">
											</web:grid>
										</web:viewContent>
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:viewContent id="po_view5" styleClass="hidden">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="maddress.oldAddress" var="program" />
										</web:column>
										<web:column>
											<type:addressLineDisplay property="oldAddressLine1" id="oldAddressLine1" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
										</web:column>
										<web:column>
											<type:addressLineDisplay property="oldAddressLine2" id="oldAddressLine2" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
										</web:column>
										<web:column>
											<type:addressLineDisplay property="oldAddressLine3" id="oldAddressLine3" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
										</web:column>
										<web:column>
											<type:addressLineDisplay property="oldAddressLine4" id="oldAddressLine4" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
										</web:column>
										<web:column>
											<type:addressLineDisplay property="oldAddressLine5" id="oldAddressLine5" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
						</web:viewContent>
					</web:sectionBlock>
				</web:dividerBlock>
			</web:viewContent>
		</web:viewSubPart>
	</web:viewPart>
	<web:contextSearch />
</web:fragment>