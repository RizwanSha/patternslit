var validator = new xmlHTTPValidator();
function init() {
	$('#userID').focus();
	$('.main-content').addClass('login-bg');
}

function validate(id) {
	switch (id) {
	case 'userID':
		userID_val();
		setFocusOnSubmit();
		break;
	}
}

function clearFields() {
	$('#userID').val(EMPTY_STRING);
	$('#userID_error').html(EMPTY_STRING);
	$('#userID').focus();

}

function userID_val() {
	var value = $('#userID').val();
	clearError('userID_error');
	if (isEmpty(value)) {
		setError('userID_error', MANDATORY);
		return false;
	}
	return true;
}

function revalidate() {
	if (!userID_val()) {
		errors++;
	}

	$('#userID').val($('#userID').val().toUpperCase());
	$('#action').val(ADD);
}