<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<web:bundle baseName="patterns.config.web.forms.common.Messages" var="program" />
<web:fragment>
	<web:viewPartInline>
		<div class="navbar navbar-inverse navbar-fixed-top drop-shadow">
			<a class="navbar-brand"></a>
		</div>
		<web:viewSubPart>
			<web:form action="/Renderer/common/elogout" id="elogout" method="POST">
				<web:viewContent id="po_view1">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:row>
										<web:column>
											<web:printRequestAdditionalInfo styleClass="note" />
										</web:column>
									</web:row>
									<web:row>
										<table>
											<tr>
												<td style="padding: 5px"><input type="image" src="public/styles/images/sucessImage.png" style="width: 90px; hight: 85px"></td>
												<td style="vertical-align: top; padding-top: 20px; font-size: 20px; color: #2f7698; font-weight: bold;"><web:infoMessage key="elogout.message1" var="program" /> <br> <a href="/LIT/Renderer/common/elogin.jsp"> <input type="button" id="relogin" name="relogin"
														style="width: 200px; font-weight: 600; background: #dce6f2; margin-top: 20px;" class="btn btn-xs btn-default" value="CLICK HERE TO RE-LOGIN" onclick="" />
												</a> <script>
													$('#relogin').focus();
												</script>
											</tr>
										</table>
									</web:row>
								</web:table>
							</web:section>
							<type:logoutDetails />
						</web:sectionBlock>
					</web:dividerBlock>
				</web:viewContent>
			</web:form>
			<web:footer />
		</web:viewSubPart>
	</web:viewPartInline>
</web:fragment>

