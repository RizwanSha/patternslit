var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MCONTACT';
var sourceKey = EMPTY_STRING;
var parentKey = EMPTY_STRING;
var isMergeReqd;
function init() {
	clearFields();
	loadValues();
	innerGrid.attachEvent("onRowDblClicked", function(rid, ind) {
		if (sourceKey == 'PERSON_ID')
			value = getPartitionNo() + PK_SEPERATOR + $('#personId').val() + PK_SEPERATOR + rid;
		else
			value = parentKey + '||' + (getPartitionNo() + PK_SEPERATOR + rid);
		viewContact(value);
	});
}
function clearFields() {
	$('#personId').val(EMPTY_STRING);
	innerGrid.clearAll();
}
function loadData() {
}
function loadValues() {
	var source = _primaryKey.split(',');
	SOURCE_VALUE = source[0];
	pk = source[1];
	PARENT_PROGRAM_ID = source[2];
	sourceKey = source[3];
	if (sourceKey == 'PERSON_ID') {
		show('person');
		$('#personId').val(pk.split(PK_SEPERATOR)[1]);
	} else {
		hide('person');
		parentKey = pk;
		innerGrid.setColumnHidden(3, true);
	}
	loadGrid();
}
function loadGrid() {
	innerGrid.clearAll();
	PARENT_PRIMARY_KEY = pk;
	LINK_PRIMARY_KEY = EMPTY_STRING;
	PK_VALUE = pk;
	LINK_PROGRAM_ID = CURRENT_PROGRAM_ID;
	if (SOURCE_VALUE == MAIN) {
		isMergeReqd = false;
		loadComponentGridQuery(PARENT_PROGRAM_ID, 'MCONTACT_GRID_M', postProcessing);
	} else {
		isMergeReqd = true;
		loadComponentGridQuery(PARENT_PROGRAM_ID, 'MCONTACT_GRID_M', postProcessing);
	}

}

function postProcessing(result) {
	$xml = $($.parseXML(result));
	($xml).find("row").each(function() {
		try {
			var conType = $(this).find("cell")[2].textContent;
			var details = $(this).find("cell")[3].textContent;
			var official = $(this).find("cell")[4].textContent;
			var invNumber = $(this).find("cell")[5].textContent;
			var addOn = $(this).find("cell")[6].textContent;
			var modOn = $(this).find("cell")[11].textContent;
			var gridValues = [ '', conType, details, official, invNumber, addOn, modOn ];
			if (innerGrid.doesRowExist(invNumber)) {
				innerGrid.deleteRow(invNumber);
				innerGrid.addRow(invNumber, gridValues);
			} else {
				innerGrid.addRow(invNumber, gridValues);
			}
		} catch (e) {
		}
	});

	if (isMergeReqd) {
		isMergeReqd = false;
		loadComponentGridQuery(PARENT_PROGRAM_ID, 'MCONTACT_GRID_T', postProcessing);
	}
	if (SOURCE_VALUE != MAIN) {
		loadComponentGridQuery(PARENT_PROGRAM_ID, 'MCONTACT_RMVD_T', removedContactProcessing);
	}
}
function removedContactProcessing(result) {
	$xml = $($.parseXML(result));
	$xml.find("row").each(function() {
		try {
			var invNumber = $(this).find("cell")[1].textContent;
			if (innerGrid.doesRowExist(invNumber)) {
				innerGrid.deleteRow(invNumber);
			}
		} catch (e) {
		}
	});
	return;
}
function viewContact(value) {
	var pk;
	if (sourceKey == 'PERSON_ID') {
		var offDetails;
		if (innerGrid.cells(value.split(PK_SEPERATOR)[2], 3).getValue() == 'Yes') {
			offDetails = '1';
		} else {
			offDetails = '0';
		}
		pk = SOURCE_VALUE + ',' + value + ',' + offDetails + ',' + PARENT_PROGRAM_ID + ',' + sourceKey;
	} else {
		pk = SOURCE_VALUE + ',' + value + ',' + ZERO + ',' + PARENT_PROGRAM_ID + ',' + sourceKey;
	}
	openPopup('common/qinvcontact', pk, 'Contact', 960, 500);
	resetLoading();
}