var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'QADDRESS';
var sourceKey = EMPTY_STRING;
var pk, tabletype;
var isMergeReqd;
var parentKey = EMPTY_STRING;

function init() {
	clearFields();
	loadValues();
	innerGrid.attachEvent("onRowDblClicked", function(rid, ind) {
		// if (sourceKey == 'PERSON_ID') {
		// value = parentKey + PK_SEPERATOR + rid;
		// } else {
		value = parentKey + '||' + (getPartitionNo() + PK_SEPERATOR + rid);
		// }
		viewAddress(value);
	});
}
function clearFields() {
	$('#personId').val(EMPTY_STRING);
	innerGrid.clearAll();
}
function loadData() {
}
function loadValues() {
	var source = _primaryKey.split(',');
	SOURCE_VALUE = source[0];
	pk = source[1];
	PARENT_PROGRAM_ID = source[2];
	sourceKey = source[3];
	if (sourceKey == 'PERSON_ID') {
		$('#personId').val(pk.split(PK_SEPERATOR)[1]);
		parentKey = pk;
		loadOldAddress();
	} else {
		parentKey = pk;
		loadGrid();
	}

}
function loadGrid() {
	innerGrid.clearAll();
	PARENT_PRIMARY_KEY = pk;
	LINK_PRIMARY_KEY = EMPTY_STRING;
	PK_VALUE = pk;
	LINK_PROGRAM_ID = 'MADDRESS';
	if (SOURCE_VALUE == MAIN) {
		isMergeReqd = false;
		loadComponentGridQuery(CURRENT_PROGRAM_ID, 'MADDRESS_GRID_M', postProcessing);
	} else {
		isMergeReqd = true;
		loadComponentGridQuery(CURRENT_PROGRAM_ID, 'MADDRESS_GRID_M', postProcessing);
	}

}
function postProcessing(result) {
	$xml = $($.parseXML(result));
	($xml).find("row").each(function() {
		try {
			var invNumber = $(this).find("cell")[1].textContent;
			var addrType = $(this).find("cell")[2].textContent;
			var countryCode = $(this).find("cell")[3].textContent;
			var details = $(this).find("cell")[4].textContent;
			var location = getLocation($(this).find("cell")[3].textContent);
			var crOn = $(this).find("cell")[9].textContent;
			var modOn = $(this).find("cell")[10].textContent;
			var locAddr = $(this).find("cell")[11].textContent;
			var permAddr = $(this).find("cell")[12].textContent;
			var offAddr = $(this).find("cell")[13].textContent;
			var commAddr = $(this).find("cell")[14].textContent;
			var addrTypeDesc = $(this).find("cell")[15].textContent;
			var countryCodeDesc = $(this).find("cell")[16].textContent;
			var gridValues = [ '', invNumber, addrType, addrTypeDesc, countryCode, countryCodeDesc, details, location, crOn, modOn, locAddr, permAddr, offAddr, commAddr ];
			if (innerGrid.doesRowExist(invNumber)) {
				innerGrid.deleteRow(invNumber);
				innerGrid.addRow(invNumber, gridValues);
			} else {
				innerGrid.addRow(invNumber, gridValues);
			}
		} catch (e) {
		}
	});
	if (isMergeReqd) {
		isMergeReqd = false;
		loadComponentGridQuery(CURRENT_PROGRAM_ID, 'MADDRESS_GRID_T', postProcessing);
	}
	if (SOURCE_VALUE != MAIN) {
		loadComponentGridQuery(CURRENT_PROGRAM_ID, 'MADDRESS_RMVD_T', removedAddressProcessing);
	}
}

function removedAddressProcessing(result) {
	$xml = $($.parseXML(result));
	$xml.find("row").each(function() {
		try {
			var invNumber = $(this).find("cell")[1].textContent;
			if (innerGrid.doesRowExist(invNumber)) {
				innerGrid.deleteRow(invNumber);
			}
		} catch (e) {
		}
	});
	return;
}
function loadOldAddress() {
	PARENT_PRIMARY_KEY = pk;
	LINK_PROGRAM_ID = 'MADDRESS';
	args = pk;
	if (SOURCE_VALUE == MAIN) {
		isMergeReqd = false;
		loadLinkedRecordData(loadOldAddressDetail, CURRENT_PROGRAM_ID, 'MADDRESS_OLD_M', args);
	} else {
		isMergeReqd = true;
		loadLinkedRecordData(loadOldAddressDetail, CURRENT_PROGRAM_ID, 'MADDRESS_OLD_M', args);
	}
	return true;
}
function loadOldAddressDetail() {
	if (validator.getValue('OMG_ADDRESS1') != EMPTY_STRING) {
		$('#po_view5').removeClass('hidden');
		$('#oldAddressLine1').val(validator.getValue('OMG_ADDRESS1'));
		$('#oldAddressLine2').val(validator.getValue('OMG_ADDRESS2'));
		$('#oldAddressLine3').val(validator.getValue('OMG_ADDRESS3'));
		$('#oldAddressLine4').val(validator.getValue('OMG_ADDRESS4'));
		$('#oldAddressLine5').val(validator.getValue('OMG_ADDRESS5'));
	} else {
		$('#po_view5').addClass('hidden');
	}
	if (isMergeReqd) {
		isMergeReqd = false;
		loadLinkedRecordData(loadOldAddressDetail, CURRENT_PROGRAM_ID, 'MADDRESS_OLD_T', args);
	}
	loadGrid();
	return true;
}

function viewAddress(value) {
	var personDetails = EMPTY_STRING;
	if (sourceKey == 'PERSON_ID') {
		var key = value.split('||');
		var invnum = key[1].split(PK_SEPERATOR);
		var rowID = invnum[1];
		personDetails = innerGrid.cells(rowID, 10).getValue() + PK_SEPERATOR + innerGrid.cells(rowID, 11).getValue() + PK_SEPERATOR + innerGrid.cells(rowID, 12).getValue() + PK_SEPERATOR + innerGrid.cells(rowID, 13).getValue();
	}
	var pk = SOURCE_VALUE + ',' + value + ',' + personDetails + ',' + PARENT_PROGRAM_ID + ',' + sourceKey;
	openPopup('common/qaddrinv', pk, 'Address', 960, 500);
	resetLoading();
}
function getLocation(value) {
	validator.clearMap();
	validator.setValue('COUNTRY_CODE', value);
	validator.setClass('patterns.config.web.forms.common.maddressbean');
	validator.setMethod('getLocation');
	validator.sendAndReceive();
	var location = EMPTY_STRING;
	if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		var datavalue = validator.getValue("V_GUS_STRUCTURE").split(";");
		for (var count = 0; count < datavalue.length; count++) {
			location = location + datavalue[count] + "\n";
		}
		return location;
	}
	return value;
}