<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<%@taglib prefix="message" tagdir="/WEB-INF/tags/message"%>
<web:bundle baseName="patterns.config.web.forms.bil.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/bil/rbillfilegen.js" />
	</web:dependencies>
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="rbillfilegen.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/bil/rbillfilegen" id="rbillfilegen" method="POST">
				<web:dividerBlock>
					<web:sectionBlock width="width-100p" align="left">
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="rbillfilegen.invoicedate" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:date property="invoiceDate" id="invoiceDate" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="rbillfilegen.leaseproductcode" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:productCode property="leaseProductCode" id="leaseProductCode" />
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:rowEven>
									<web:column>
										<type:button id="print" key="form.print" var="common" onclick="doPrint()" />
										<type:reset id="reset" key="form.reset" var="common" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:section>
							<message:messageBox id="rbillfilegen" />
						</web:section>
					</web:sectionBlock>
				</web:dividerBlock>
				<web:element property="command" />
				<web:element property="action" />
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
	<web:contextSearch />
</web:fragment>
