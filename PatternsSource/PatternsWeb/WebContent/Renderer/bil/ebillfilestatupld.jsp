<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<%@taglib prefix="resource" tagdir="/WEB-INF/tags/resource"%>
<%@taglib prefix="message" tagdir="/WEB-INF/tags/message"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<web:bundle baseName="patterns.config.web.forms.bil.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/bil/ebillfilestatupld.js" />
	</web:dependencies>
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="ebillfilestatupld.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/bil/ebillfilestatupld" id="ebillfilestatupld" method="POST">
				<web:dividerBlock>
					<type:optionBox add="true" modify="false" view="true" />
				</web:dividerBlock>
				<web:viewContent id="po_view1">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="ebillfilestatupld.uploaddatesl" var="program" />
										</web:column>
										<web:column>
											<type:dateDaySL property="upload" id="upload" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="ebillfilestatupld.uploadfile" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<resource:fileUpload id="vault" width="400px" height="100px" initFileUpload="initFileUpload" beforeFileAdd="beforeFileAdd" afterFileUpload="afterFileUpload" afterFileUploadFail="afterFileUploadFail" filelimit="1" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="ebillfilestatupld.filename" var="program" />
										</web:column>
										<web:column>
											<type:fileName id="fileName" property="fileName" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="ebillfilestatupld.fileextensionlist" var="program" />
										</web:column>
										<web:column>
											<type:fileExtensionList id="fileExtensionList" property="fileExtensionList" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="ebillfilestatupld.fileinventorynumber" var="program" />
										</web:column>
										<web:column>
											<type:fileInventory id="fileInventoryNumber" property="fileInventoryNumber" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:button key="ebillfilestatupld.verifyuploadfile" id="uploadFile" var="program" onclick="verifyUploadFile();" />
										</web:column>
										<web:column>
											<span id="uploadSuccess" class="fa fa-check-circle-o" style="font-size: 17px; color: green"> </span>
											<span id="uploadFail" class="fa fa-exclamation-triangle" style="font-size: 17px; color: red"> </span>
											<span id="fileUploadError" style="font-size: 17px; color: red" ><html:errors property="fileUploadError" /></span>
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="form.remarks" var="common" />
										</web:column>
										<web:column>
											<type:remarks property="remarks" id="remarks" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:viewContent id="view_dtl" styleClass="hidden">
								<web:section>
									<message:messageBox id="ebillfilestatupld" />
								</web:section>
								<web:section>
									<web:table>
										<web:rowOdd>
											<web:column>
												<web:grid height="240px" width="1051px" id="ebillfilestatupld_innerGrid" src="bil/ebillfilestatupld_innerGrid.xml">
												</web:grid>
											</web:column>
										</web:rowOdd>
									</web:table>
								</web:section>
							</web:viewContent>
							<web:section>
								<web:table>
									<web:rowOdd>
										<web:column span="2">
											<web:submitReset />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
						</web:sectionBlock>
					</web:dividerBlock>
				</web:viewContent>
				<web:element property="tempSerial" />
				<web:element property="command" />
				<web:element property="action" />
				<web:element property="fileExt" />
				<web:element property="filePath" />
				<web:element property="redirectLandingPagePath" />
				<web:element property="fileExtensionListError" />
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
	<web:queryGrid />
	<web:TBAGrid />
	<web:contextSearch />
</web:fragment>
