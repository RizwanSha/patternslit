var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'EBILLFILESTATUPLD';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}

function clearFields() {
	$('#uploadDate').val(EMPTY_STRING);
	$('#uploadSerial').val(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	$('#referenceNo').val(EMPTY_STRING);
	$('#fileInventoryNumber').val(EMPTY_STRING);
	$('#fileName').val(EMPTY_STRING);
	$('#fileInventoryNumber').val(EMPTY_STRING);
	$('#fileExt').val(EMPTY_STRING);
}

function loadData() {
	$('#uploadDate').val(validator.getValue('ENTRY_DATE'));
	$('#uploadSerial').val(validator.getValue('ENTRY_SL'));
	$('#fileName').val(validator.getValue('F1_FILE_NAME'));
	$('#fileInventoryNumber').val(validator.getValue('FILE_INV_NUM'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadAuditFields(validator);
}
function loadGrid() {
	SOURCE_VALUE = _tableType;
	PK_VALUE = _primaryKey;
	if (_tableType == MAIN) {
		viewDetails();

	} else if (_tableType == TBA) {
		viewDetails();
	}
}
function viewDetails() {
	win = showWindow('view_details', EMPTY_STRING, 'COMPONENT_DETAILS', EMPTY_STRING, true, false, false, 1000, 500);
	ebillfilestatupld_detailGrid.setColumnHidden(0, true);
	loadGridQuery(CURRENT_PROGRAM_ID, 'EBILLFILESTATUPLD_GRID', ebillfilestatupld_detailGrid);
}
