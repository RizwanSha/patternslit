var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'EBILLFILESTATUPLD';
function init() {
	if ($('#fileExtensionListError').val() != null && $('#fileExtensionListError').val() != EMPTY_STRING) {
		alert($('#fileExtensionListError').val());
		window.location.href = getBasePath() + $('#redirectLandingPagePath').val();
		return false;
	}
	refreshQueryGrid();
	refreshTBAGrid(); 
	if (_redisplay) {
		if ($('#action').val() == ADD) {
			$('#uploadDate').prop('readonly', true);
			$('#uploadSerial').prop('readonly', true);
		}
		clearExistingFile();
		hide('uploadSuccess');
		hide('uploadFail');
		hide('view_dtl');
	}
	vault.attachEvent("onFileRemove", function(file) {
		clearExistingFile();
	});
}

function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'EBILLFILESTATUPLD_MAIN');
}

function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}

function view(source, primaryKey) {
	hideParent('bil/qbillfilestatupld', source, primaryKey);
	resetLoading();
}

function clearFields() {
	$('#uploadDate').val(EMPTY_STRING);
	$('#uploadSerial').val(EMPTY_STRING);
	$('#upload_error').html(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
	clearNonPKFields();
}

function clearNonPKFields() {
	$('#referenceNo').val(EMPTY_STRING);
	$('#referenceNo_error').html(EMPTY_STRING);
	$('#fileInventoryNumberExit').val(EMPTY_STRING);
	vault.clear();
	clearExistingFile();
}

function clearExistingFile() {
	$('#fileName').val(EMPTY_STRING);
	$('#fileInventoryNumber').val(EMPTY_STRING);
	$('#fileExt').val(EMPTY_STRING);
	$('#uploadFail').html(EMPTY_STRING);
	$('#uploadSuccess').html(EMPTY_STRING);
	$('#fileUploadError').html(EMPTY_STRING);
	show('uploadFile');
	hide('uploadSuccess');
	hide('uploadFail');
}

function doclearfields(id) {
	switch (id) {
	case 'uploadDate':
		if (isEmpty($('#uploadDate').val())) {
			$('#upload_error').html(EMPTY_STRING);
			clearFields();
			break;
		}
	case 'uploadSerial':
		if (isEmpty($('#uploadSerial').val())) {
			$('#upload_error').html(EMPTY_STRING);
			clearNonPKFields();
			break;
		}
	}
}

function loadData() {
	$('#uploadDate').prop('readonly', false);
	$('#uploadSerial').prop('readonly', false);
	$('#upload_pic').prop('disabled', false);
	$('#uploadDate').val(validator.getValue('ENTRY_DATE'));
	$('#uploadSerial').val(validator.getValue('ENTRY_SL'));
	$('#fileName').val(validator.getValue('F1_FILE_NAME'));
	$('#fileInventoryNumber').val(validator.getValue('FILE_INV_NUM'));
	$('#remarks').val(validator.getValue('REMARKS'));
	resetLoading();
}

function add() {
	$('#uploadDate').prop('readonly', true);
	$('#uploadSerial').prop('readonly', true);
	$('#upload_pic').prop('disabled', true);
	$('#uploadDate').val(getCBD());
}

function modify() {
}

function doHelp(id) {
	switch (id) {
	case 'upload':
		help('EBILLFILESTATUPLD', 'HLP_UPLOAD_SERIAL', $('#uploadSerial').val(), $('#uploadDate').val(), $('#uploadSerial'));
		break;
	}
}

function validate(id) {
	switch (id) {
	case 'uploadDate':
		uploadDate_val();
		break;
	case 'uploadSerial':
		uploadSerial_val();
		break;
	case 'remarks':
		remarks_val();
		break;
	}
}

function backtrack(id) {
	switch (id) {
	case 'uploadDate':
		setFocusLast('uploadDate');
		break;
	case 'uploadSerial':
		setFocusLast('uploadDate');
		break;
	case 'remarks':
		if ($('#rectify').val() == COLUMN_ENABLE)
			setFocusLast('uploadSerial');
		break;
	}
}

function uploadDate_val() {
	var value = $('#uploadDate').val();
	clearError('upload_error');
	if ($('#rectify').val() == COLUMN_ENABLE) {
		if (isEmpty(value)) {
			setError('upload_error', MANDATORY);
			return false;
		}
		if (!isDate(value)) {
			setError('upload_error', INVALID_DATE);
			return false;
		}
		if (isDateGreater(value, getCBD())) {
			setError('upload_error', DATE_LECBD);
			return false;
		}
	}
	setFocusLast('uploadSerial');
	return true;
}

function uploadSerial_val() {
	var value = $('#uploadSerial').val();
	clearError('upload_error');
	if ($('#rectify').val() == COLUMN_ENABLE) {
		if (isEmpty(value)) {
			setError('upload_error', MANDATORY);
			return false;
		}
		if (!isNumeric(value)) {
			setError('upload_error', NUMERIC_CHECK);
			return false;
		} else {
			validator.clearMap();
			validator.setMtm(false);
			validator.setValue('ENTRY_DATE', $('#uploadDate').val());
			validator.setValue('ENTRY_SL', value);
			validator.setValue(ACTION, $('#action').val());
			validator.setClass('patterns.config.web.forms.bil.ebillfilestatupldbean');
			validator.setMethod('validateUploadSerial');
			validator.sendAndReceive();
			if (validator.getValue(ERROR) != EMPTY_STRING) {
				setError('uploadSerial_error', validator.getValue(ERROR));
				return false;
			} else {
				PK_VALUE = getEntityCode() + PK_SEPERATOR + $('#uploadDate').val() + PK_SEPERATOR + $('#uploadSerial').val();
				if (!loadPKValues(PK_VALUE, 'upload_error')) {
					return false;
				}
			}
		}
	}
	setFocusLast('remarks');
	return true;
}

function initFileUpload() {
	clearExistingFile();
	vault.setAutoStart(true);
	vault.attachEvent("onFileRemove", function(file) {
		vault.setFilesLimit(1);
	});
}

function beforeFileAdd(file) {
	if ($('#rectify').val() == COLUMN_ENABLE)
		clearExistingFile();
	$('#fileUploadError').html(EMPTY_STRING);
	fileExtension = vault.getFileExtension(file.name);
	var temp = $('#fileExtensionList').val().split(',');
	if ($('#fileInventoryNumber').val() != EMPTY_STRING) {
		alert(PBS_REMOVE_EXISTING_FILE);
		return false;
	}
	if (jQuery.inArray(fileExtension.toLowerCase(), temp) == -1) {
		alert(INVALID_FILE_FORMAT);
		return false;
	}
	return true;
}

function getUploadParameters() {
	param = 'FILE_PATH=' + $('#filePath').val() + '&ALLOWED_EXTENSIONS=' + $('#fileExtensionList').val();
	return param;
}

function afterFileUploadFail(file, extra) {
	alert(extra.param);
	vault.clear();
}

function afterFileUpload(file, extra) {
	$('#fileName').val(file.name);
	$('#fileInventoryNumber').val(extra.invNo);
	$('#fileExt').val(extra.format.toLowerCase());
	if ($('#fileInventoryNumber').val() != EMPTY_STRING) {
		$('#fileInventoryNumberExit').val(EMPTY_STRING);
	}
	setFocusLast('remarks');
}

function verifyUploadFile() {
	var value = $('#fileName').val();
	var format = $('#fileName').val().split('.');
	clearError('fileUploadError');
	if (isEmpty(value)) {
		setError('fileUploadError', UPLOAD_FILE_MANDATORY);
		return false;
	}
	hide('uploadFile');
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('FILE_NAME', value);
	validator.setValue('FILE_FORMAT', format[1]);
	validator.setValue('SOURCE_KEY', $('#uploadDate').val() + PK_SEPERATOR + $('#uploadSerial').val());
	validator.setValue('PGM_ID', CURRENT_PROGRAM_ID);
	validator.setValue(ACTION, USAGE);
	validator.setClass('patterns.config.web.forms.bil.ebillfilestatupldbean');
	validator.setMethod('validateUploadXLSXFile');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('fileUploadError', validator.getValue(ERROR));
		return false;
	}
	if (validator.getValue(RESULT) == DATA_UNAVAILABLE) {
		hide('uploadSuccess');
		show('uploadFail');
		$('#tempSerial').val(validator.getValue("TEMP_SL"));
		$('#uploadFail').html(FAILURE_MESSAGE);
		return false;
	} else if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		$('#tempSerial').val(validator.getValue("TEMP_SL"));
		$('#uploadSuccess').html(NO_ERROR);
		hide('uploadFail');
		show('uploadSuccess');
	}

}

function viewErrorDetails() {
	ebillfilestatupld_innerGrid.clearAll();
	win = showWindow('view_dtl', EMPTY_STRING, 'COMPONENT_DETAILS', EMPTY_STRING, true, false, false, 1100, 500);
	showMessage(getProgramID(), Message.PROGRESS);
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('TEMP_SL', $('#tempSerial').val());
	validator.setClass('patterns.config.web.forms.bil.ebillfilestatupldbean');
	validator.setMethod('loadTempDetails');
	validator.sendAndReceiveAsync(showInnerGrid);
}

function showInnerGrid() {
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		showMessage(getProgramID(), Message.ERROR, validator.getValue(ERROR));
		return false;
	}
	if (validator.getValue(RESULT) == DATA_UNAVAILABLE) {
		showMessage(getProgramID(), Message.ERROR, NO_RECORD);
		return false;
	}
	if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		ebillfilestatupld_innerGrid.clearAll();
		ebillfilestatupld_innerGrid.loadXMLString(validator.getValue(RESULT_XML));
		hideMessage(getProgramID());
	}
}

function revalidate() {
	if (isEmpty($('#tempSerial').val())) {
		setError('fileUploadError', VERIFY_MANDATORY);
		errors++;
		return false;
	}
	if (isEmpty($('#fileInventoryNumber').val()) && isEmpty($('#fileInventoryNumberExit').val())) {
		alert(FILE_NOT_YET_UPLOADED);
		errors++;
		return false;
	}
	if (!isEmpty($('#fileInventoryNumber').val())) {
		var temp = $('#fileExtensionList').val().split(',');
		if (jQuery.inArray(fileExtension.toLowerCase(), temp) == -1) {
			alert(INVALID_FILE_FORMAT);
			errors++;
			return false;
		}
	} else if (isEmpty($('#fileInventoryNumberExit').val())) {
		alert(FILE_NOT_YET_UPLOADED);
		errors++;
		return false;
	}
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', INVALID_REMARKS);
			setFocusLast('remarks');
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', INVALID_REMARKS);
			setFocusLast('remarks');
			return false;
		}
	}
	if ($('#rectify').val() == COLUMN_ENABLE) {
		if (isEmpty(value)) {
			setError('remarks_error', MANDATORY);
			setFocusLast('remarks');
			return false;
		}
	}
	setFocusOnSubmit();
	return true;
}
