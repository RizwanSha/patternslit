var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'RBILLFILEGEN';
function init() {
	setFocusLast('invoiceDate');
}

function clearFields() {
	$('#invoiceDate').val(EMPTY_STRING);
	$('#invoiceDate_error').html(EMPTY_STRING);
	$('#leaseProductCode').val(EMPTY_STRING);
	$('#leaseProductCode_error').html(EMPTY_STRING);
	$('#leaseProductCode_desc').html(EMPTY_STRING);
	hideMessage(getProgramID());
}
function doHelp(id) {
	switch (id) {
	case 'leaseProductCode':
		help('COMMON', 'HLP_LEASE_PROD_CODE', $('#leaseProductCode').val(), EMPTY_STRING, $('#leaseProductCode'));
		break;
	}
}
function validate(id) {
	var valMode = true;
	switch (id) {
	case 'invoiceDate':
		invoiceDate_val(valMode);
		break;
	case 'leaseProductCode':
		leaseProductCode_val(valMode);
		break;
	}
}

function backtrack(id) {
	switch (id) {
	case 'invoiceDate':
		setFocusLast('invoiceDate');
		break;
	case 'leaseProductCode':
		setFocusLast('invoiceDate');
		break;
	case 'print':
		setFocusLast('leaseProductCode');
		break;
	}
}

function invoiceDate_val(valMode) {
	var value = $('#invoiceDate').val();
	clearError('invoiceDate_error');
	if (isEmpty(value)) {
		$('#invoiceDate').val(getCBD());
		value = $('#invoiceDate').val();
	}
	if (!isDate(value)) {
		setError('invoiceDate_error', INVALID_DATE);
		return false;
	}
	 if (!isDateLesserEqual(value, getCBD())) {
		setError('invoiceDate_error', DATE_LECBD);
		return false;
	}
	if (valMode)
		setFocusLast('leaseProductCode');
	return true;
}

function leaseProductCode_val(valMode) {
	var value = $('#leaseProductCode').val();
	clearError('leaseProductCode_error');
	if (isEmpty(value)) {
		setError('leaseProductCode_error', MANDATORY);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('LEASE_PRODUCT_CODE', value);
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.bil.rbillfilegenbean');
		validator.setMethod('validateLeaseProductCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('leaseProductCode_error', validator.getValue(ERROR));
			$('#leaseProductCode_desc').html(EMPTY_STRING);
			return false;
		}
		$('#leaseProductCode_desc').html(validator.getValue('DESCRIPTION'));
	}
	if (valMode)
		setFocusLast('print');
	return true;
}

function doPrint() {
	hideMessage(getProgramID());
	if (invoiceDate_val(false) && leaseProductCode_val(false)) {
		disableElement('print');
		showMessage(getProgramID(), Message.PROGRESS);
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('ENTITY_CODE', getEntityCode());
		validator.setValue('INVOICE_DATE', $('#invoiceDate').val());
		validator.setValue('LEASE_PRODUCT_CODE', $('#leaseProductCode').val());
		validator.setValue(ACTION, USAGE);
		validator.setValue('REPORT_TYPE', $('#leaseProductCode').val());
		validator.setValue('REPORT_FORMAT', '2');
		validator.setClass('patterns.config.web.forms.bil.rbillfilegenbean');
		validator.setMethod('doPrint');
		validator.sendAndReceiveAsync(processDownloadResult);
	}
}

function processDownloadResult() {
	enableElement('print');
	if (validator.getValue(ERROR) != null && validator.getValue(ERROR) != EMPTY_STRING) {
		if (validator.getValue(ERROR_FIELD) != EMPTY_STRING) {
			hideAllMessages(getProgramID());
			setError(validator.getValue(ERROR_FIELD) + '_error', validator.getValue(ERROR));
		} else {
			showMessage(getProgramID(), Message.ERROR, validator.getValue(ERROR));
		}
	} else if (!isEmpty(validator.getValue('SP_ERROR'))) {
		showMessage(getProgramID(), Message.ERROR, validator.getValue('SP_ERROR'));
	} else {
		showReportLink(validator);
		return true;
	}
}
