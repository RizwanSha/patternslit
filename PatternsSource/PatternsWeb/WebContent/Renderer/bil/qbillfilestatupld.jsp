<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<web:bundle baseName="patterns.config.web.forms.bil.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/bil/qbillfilestatupld.js" />
	</web:dependencies>
	<web:loadPK />
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="ebillfilestatupld.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:viewContent id="po_view1">
				<web:dividerBlock>
					<web:sectionBlock width="width-100p" align="left">
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="180px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowOdd>
									<web:column>
										<web:legend key="ebillfilestatupld.uploaddatesl" var="program" />
									</web:column>
									<web:column>
										<type:dateDaySLDisplay property="upload" id="upload" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="ebillfilestatupld.filename" var="program" />
									</web:column>
									<web:column>
										<type:fileNameDisplay id="fileName" property="fileName" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="ebillfilestatupld.fileinventorynumber" var="program" />
									</web:column>
									<web:column>
										<type:fileInventoryDisplay id="fileInventoryNumber" property="fileInventoryNumber" />
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
						<web:viewContent id="detail_view">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
										</web:column>
										<web:column>
											<web:anchorMessage var="program" key="ebillfilestatupld.link" onclick="loadGrid()" style="text-decoration:underline;cursor:pointer;font-size:17px" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
						</web:viewContent>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="180px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowOdd>
									<web:column>
										<web:legend key="form.remarks" var="common" />
									</web:column>
									<web:column>
										<type:remarksDisplay property="remarks" id="remarks" />
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
						<web:viewContent id="view_details" styleClass="hidden">
							<web:section>
								<web:table>
									<web:rowEven>
										<web:column>
											<web:grid height="280px" width="941px" id="ebillfilestatupld_detailGrid" src="bil/ebillfilestatupld_detailGrid.xml">
											</web:grid>
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
						</web:viewContent>
					</web:sectionBlock>
					<web:auditDisplay />
				</web:dividerBlock>
			</web:viewContent>
		</web:viewSubPart>
	</web:viewPart>
</web:fragment>
