<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.lss.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/lss/eleaseuserattr.js" />
	</web:dependencies>
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="eleaseuserattr.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/lss/eleaseuserattr" id="eleaseuserattr" method="POST">
				<web:dividerBlock>
					<type:optionBox add="true" modify="true" view="true" />
				</web:dividerBlock>
				<web:viewContent id="po_view1" styleClass="hidden">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle width="380px" />
										<web:columnStyle width="120px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="eleaseuserattr.lessecode" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:lesseeCode property="leaseCode" id="leaseCode" />
										</web:column>
										<web:column>
											<web:legend key="eleaseuserattr.customerid" var="program" />
										</web:column>
										<web:column>
											<type:customerID property="customerID" id="customerID" readOnly="true" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="eleaseuserattr.custname" var="program" />
										</web:column>
										<web:column>
											<type:descriptionDisplay property="customerName" id="customerName" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="eleaseuserattr.aggrementserial" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:agreementNo property="aggrementSerial" id="aggrementSerial" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="eleaseuserattr.scheduleid" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:scheduleID property="scheduleID" id="scheduleID" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:viewTitle var="program" key="eleaseuserattr.details" />
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="eleaseuserattr.fieldid" var="program" />
										</web:column>
										<web:column>
											<type:fieldId property="fieldId" id="fieldId" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:viewContent id="fieldvalue" styleClass="hidden">
								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="180px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowEven>
											<web:column>
												<web:legend key="eleaseuserattr.fieldvalue" var="program" />
											</web:column>
											<web:column>
												<type:dynamicIntegerAndDecimal property="fieldValue" id="fieldValue" />
											</web:column>
										</web:rowEven>
									</web:table>
								</web:section>
							</web:viewContent>
							<web:viewContent id="fieldremarks" styleClass="hidden">
								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="180px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowOdd>
											<web:column>
												<web:legend key="eleaseuserattr.fieldvalue" var="program" />
											</web:column>
											<web:column>
												<type:remarks property="fieldRemarks" id="fieldRemarks" />
											</web:column>
										</web:rowOdd>
									</web:table>
								</web:section>
							</web:viewContent>
							<web:section>
								<web:table>
									<web:rowOdd>
										<web:column>
											<web:gridToolbar gridName="eleaseuserattrGrid" cancelFunction="eleaseuserattr_cancelGrid" editFunction="eleaseuserattr_editGrid" addFunction="eleaseuserattr_addGrid" insertAtFirsstNotReq="true" afterDeleteFunction="gridDeleteRow" />
											<web:element property="xmleleaseuserattrGrid" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:grid height="250px" width="962px" id="eleaseuserattrGrid" src="lss/eleaseuserattr_innerGrid.xml">
											</web:grid>
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="form.enabled" var="common" />
										</web:column>
										<web:column>
											<type:checkbox property="enabled" id="enabled" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="form.remarks" var="common" />
										</web:column>
										<web:column>
											<type:remarks property="remarks" id="remarks" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column span="2">
											<web:submitReset />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
						</web:sectionBlock>
					</web:dividerBlock>
				</web:viewContent>
				<web:element property="command" />
				<web:element property="action" />
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
	<web:queryGrid />
	<web:TBAGrid />
	<web:contextSearch />
</web:fragment>