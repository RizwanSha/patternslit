<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<%@taglib prefix="message" tagdir="/WEB-INF/tags/message"%>
<web:bundle baseName="patterns.config.web.forms.lss.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/lss/qlease.js" />
	</web:dependencies>
	<web:loadPK />
	<web:viewPart>
		<style>
.fa-arrow-right:hover:before {
	content: "\f061";
	color: #3da0e3;
	font-size: 20px;
}

.fa-arrow-left:hover:before {
	content: "\f060";
	color: #3da0e3;
	font-size: 20px;
}

.rowCountCaption {
	color: #666d73;
	font-size: 12px;
	font-weight: bold;
}
</style>
		<web:viewSubPart>
			<web:programTitle var="program" key="elease.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:viewContent id="po_view1">
				<web:dividerBlock>
					<web:sectionBlock width="width-100p" align="left">
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="150px" />
									<web:columnStyle width="150px" />
									<web:columnStyle width="90px" />
									<web:columnStyle width="10px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="elease.custLeaseCode" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:leaseProductCodeDisplay property="custLeaseCode" id="custLeaseCode" />
									</web:column>
									<web:column>
										<web:legend key="elease.custID" var="program" />
									</web:column>
									<web:column>
										<type:customerIDDisplay property="custID" id="custID" />
									</web:column>
									<web:column>
										<type:otherInformation50Display property="custName" id="custName" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="150px" />
									<web:columnStyle width="150px" />
									<web:columnStyle width="90px" />
									<web:columnStyle width="100px" />
									<web:columnStyle width="250px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowOdd>
									<web:column>
										<web:legend key="elease.agreementNo" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:agreementSerialDisplay property="agreementNo" id="agreementNo" />
									</web:column>
									<web:column>
										<web:legend key="elease.scheduleID" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:scheduleIDDisplay property="scheduleID" id="scheduleID" />
									</web:column>
									<web:column>
										<web:legend key="elease.preLeaseRef" var="program" />
									</web:column>
									<web:column>
										<type:dateDaySLDisplay property="preLeaseRef" id="preLeaseRef" />
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>

						<type:tabbarBody id="lease_TAB_0">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle width="350px" />
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="elease.createDate" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:dateDisplay property="createDate" id="createDate" />
										</web:column>
										<web:column>
											<web:legend key="elease.leaseComDate" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:dateDisplay property="leaseComDate" id="leaseComDate" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="elease.leaseProduct" var="program" />
										</web:column>
										<web:column>
											<type:leaseProductCodeDisplay property="leaseProduct" id="leaseProduct" />
										</web:column>
										<web:column>
											<web:legend key="elease.assetCurrency" var="program" />
										</web:column>
										<web:column>
											<type:currencyDisplay property="assetCurrency" id="assetCurrency" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle width="10px" />
										<web:columnStyle width="50px" />
										<web:columnStyle width="220px" />
										<web:columnStyle width="200px" />
										<web:columnStyle width="10px" />
										<web:columnStyle width="50px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="elease.billAddress" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:serialDisplay property="billAddress" id="billAddress" />
										</web:column>
										<web:column>
											<a onclick="viewAddress();"> <i title='View Customer Billing Address' id='viewAddress' data-toggle='tooltip' class="fa fa-eye fa-2x" style="margin-left: 1px; color: #3da0e3; cursor: pointer"></i></a>
										</web:column>
										<web:column>
											<div class="rowCountCaption" id="billAddressState"></div>
										</web:column>
										<web:column>
											<web:legend key="elease.shipAddress" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:serialDisplay property="shipAddress" id="shipAddress" />
										</web:column>
										<web:column>
											<a onclick="viewShipAddress();"> <i title='View Customer Shipping Address' id='viewShipAddress' data-toggle='tooltip' class="fa fa-eye fa-2x" style="margin-left: 1px; color: #3da0e3; cursor: pointer"></i></a>
										</web:column>
										<web:column>
											<div class="rowCountCaption" id="shipAddressState"></div>
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle width="350px" />
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="elease.billBranch" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:branchCodeDisplay property="billBranch" id="billBranch" />
										</web:column>
										<web:column>
											<web:legend key="elease.billState" var="program" />
										</web:column>
										<web:column>
											<type:stateCodeDisplay property="billState" id="billState" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle width="500px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="elease.invType" var="program" />
										</web:column>
										<web:column>
											<type:comboDisplay property="invType" id="invType" datasourceid="COMMON_INVOICE_TYPE" />
										</web:column>
										<web:column align="right">
											<button type="button" class="fa fa-arrow-right " title='Go to Asset Details Section ' data-toggle='tooltip' id="nextTab" style="font-size: 15px; margin-right: 5px; color: #3da0e3; cursor: pointer" onclick="clickNextTab();"></button>
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
						</type:tabbarBody>

						<type:tabbarBody id="lease_TAB_1">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="150px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="elease.noOfAssets" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:numberDisplay id="noOfAssets" property="noOfAssets" length="6" size="6" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:grid height="220px" width="990px" id="leaseNoOfAssets_Grid" src="lss/qleaseNoOfAssets_Grid.xml">
											</web:grid>
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="60px" />
										<web:columnStyle width="500px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column align="left">
											<button type="button" onclick="clickPreviousTab();" title='Go to  Address Details Section ' data-toggle='tooltip' id="previousTab1" class="fa fa-arrow-left " style="font-size: 15px; margin-right: 5px; color: #3da0e3; cursor: pointer"></button>
										</web:column>
										<web:column align="right">
											<button type="button" onclick="clickNextTab();" id="nextTab1" class="fa fa-arrow-right " title='Go to Tenor Details Section ' data-toggle='tooltip' style="font-size: 15px; margin-right: 5px; color: #3da0e3; cursor: pointer"></button>
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>

							<web:viewContent id="noOfAssetReq" styleClass="form-horizontal">
								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="150px" />
											<web:columnStyle width="150px" />
											<web:columnStyle width="200px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowEven>
											<web:column>
												<web:legend key="elease.assetSl" var="program" />
											</web:column>
											<web:column>
												<type:serialDisplay property="assetSl" id="assetSl" />
											</web:column>
											<web:column>
												<web:legend key="elease.assetCategory" var="program" mandatory="true" />
											</web:column>
											<web:column>
												<type:schemeCodeDisplay property="assetCategory" id="assetCategory" />
											</web:column>
										</web:rowEven>
										<web:rowOdd>
											<web:column>
												<web:legend key="elease.autoNonAuto" var="program" />
											</web:column>
											<web:column>
												<type:comboDisplay property="autoNonAuto" id="autoNonAuto" datasourceid="COMMON_AUTO_NON_AUTO" />
											</web:column>
											<web:column>
												<web:legend key="elease.assetComponent" var="program" mandatory="true" />
											</web:column>
											<web:column>
												<type:numberDisplay property="assetComponent" id="assetComponent" length="6" size="6" />
											</web:column>
										</web:rowOdd>
									</web:table>
								</web:section>
								<web:viewTitle var="program" key="elease.assetDtl" />
								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="200px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowEven>
											<web:column>
												<web:grid height="200px" width="1054px" id="leaseAssetComponent_Grid" src="lss/leaseAssetComponent_Grid.xml">
												</web:grid>
											</web:column>
										</web:rowEven>
									</web:table>
								</web:section>

								<web:section>
									<web:table>
										<web:rowOdd>
											<web:column>
												<p align="center">
													<web:button key="form.close" id="close" var="common" onclick="closePopup('0')" />
												</p>
											</web:column>
										</web:rowOdd>
									</web:table>
								</web:section>
							</web:viewContent>
						</type:tabbarBody>

						<type:tabbarBody id="lease_TAB_2">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="220px" />
										<web:columnStyle width="200px" />
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend id="tenor" key="elease.tenor" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:threeDigitMonthDisplay property="tenor" id="tenor" />
										</web:column>
										<web:column>
											<web:legend key="elease.cancelPeriod" var="program" />
										</web:column>
										<web:column>
											<type:threeDigitMonthDisplay property="cancelPeriod" id="cancelPeriod" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="elease.repayFreq" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:comboDisplay property="repayFreq" id="repayFreq" datasourceid="COMMON_REPAY_FREQUENCY" />
										</web:column>
										<web:column>
											<web:legend key="elease.advArrers" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:comboDisplay property="advArrers" id="advArrers" datasourceid="COMMON_ADVANCE_ARREARS" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>

							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="220px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="elease.firstPaymet" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:dateDisplay property="firstPaymet" id="firstPaymet" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>

							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="220px" />
										<web:columnStyle width="200px" />
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="elease.intFromDate" var="program" />
										</web:column>
										<web:column>
											<type:dateDisplay property="intFromDate" id="intFromDate" />
										</web:column>
										<web:column>
											<web:legend key="elease.intUptoDate" var="program" />
										</web:column>
										<web:column>
											<type:dateDisplay property="intUptoDate" id="intUptoDate" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="elease.finalFromDate" var="program" />
										</web:column>
										<web:column>
											<type:dateDisplay property="finalFromDate" id="finalFromDate" />
										</web:column>
										<web:column>
											<web:legend key="elease.finalUptoDate" var="program" />
										</web:column>
										<web:column>
											<type:dateDisplay property="finalUptoDate" id="finalUptoDate" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>

							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="220px" />
										<web:columnStyle width="200px" />
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend id="rentDate" key="elease.rentDate" var="program" />
										</web:column>
										<web:column>
											<type:dateDisplay property="rentDate" id="rentDate" />
										</web:column>
										<web:column>
											<web:legend id="lastDate" key="elease.lastDate" var="program" />
										</web:column>
										<web:column>
											<type:dateDisplay property="lastDate" id="lastDate" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="60px" />
										<web:columnStyle width="500px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column align="left">
											<button type="button" onclick="clickPreviousTab();" id="previousTab2" title='Go to Asset Details Section ' data-toggle='tooltip' class="fa fa-arrow-left " style="font-size: 15px; margin-right: 5px; color: #3da0e3; cursor: pointer"></button>
										</web:column>
										<web:column align="right">
											<button type="button" onclick="clickNextTab();" id="nextTab2" class="fa fa-arrow-right " title='Go to Rent Details Section ' data-toggle='tooltip' style="font-size: 15px; margin-right: 5px; color: #3da0e3; cursor: pointer"></button>
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
						</type:tabbarBody>

						<type:tabbarBody id="lease_TAB_3">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="190px" />
										<web:columnStyle width="250px" />
										<web:columnStyle width="170px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend id="grossAssetCost" key="elease.grossAssetCost" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:currencySmallAmountDisplay property="grossAssetCost" id="grossAssetCost" />
										</web:column>
										<web:column>
											<web:legend id="commencementValue" key="elease.commencementValue" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:currencySmallAmountDisplay property="commencementValue" id="commencementValue" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="190px" />
										<web:columnStyle width="250px" />
										<web:columnStyle width="170px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="elease.leaseRentOption" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:comboDisplay property="leaseRentOption" id="leaseRentOption" datasourceid="COMMON_RENT_PERIOD_OPTION" />
										</web:column>
										<web:column>
											<web:legend key="elease.assureRVType" var="program" />
										</web:column>
										<web:column>
											<type:comboDisplay property="assureRVType" id="assureRVType" datasourceid="ELEASE_RV_TYPE" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="elease.assureRVPer" var="program" />
										</web:column>
										<web:column>
											<type:threeDigitTwoDecimalPercentageDisplay property="assureRVPer" id="assureRVPer" />
										</web:column>
										<web:column>
											<web:legend key="elease.assureRVAmount" var="program" />
										</web:column>
										<web:column>
											<type:currencySmallAmountDisplay property="assureRVAmount" id="assureRVAmount" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>

							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="190px" />
										<web:columnStyle width="250px" />
										<web:columnStyle width="170px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="elease.internalRateReturn" var="program" />
										</web:column>
										<web:column>
											<type:threeDigitTwoDecimalPercentageDisplay property="internalRateReturn" id="internalRateReturn" />
										</web:column>
										<web:column>
											<web:legend key="elease.dealType" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:comboDisplay property="dealType" id="dealType" datasourceid="ELEASE_DEAL_TYPE" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:viewTitle var="program" key="elease.rentTaxSection" />
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="190px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:grid height="200px" width="585px" id="leaseRentalCharges_Grid" src="lss/leaseRentalCharges_Grid.xml">
											</web:grid>
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>

							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle width="250px" />
										<web:columnStyle width="220px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="elease.intBrokenRent" var="program" />
										</web:column>
										<web:column>
											<type:currencySmallAmountDisplay property="intBrokenRent" id="intBrokenRent" />
										</web:column>
										<web:column>
											<web:legend key="elease.intBrokenExec" var="program" />
										</web:column>
										<web:column>
											<type:currencySmallAmountDisplay property="intBrokenExec" id="intBrokenExec" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="elease.finalBrokenRent" var="program" />
										</web:column>
										<web:column>
											<type:currencySmallAmountDisplay property="finalBrokenRent" id="finalBrokenRent" />
										</web:column>
										<web:column>
											<web:legend key="elease.finaBrokenExec" var="program" />
										</web:column>
										<web:column>
											<type:currencySmallAmountDisplay property="finaBrokenExec" id="finaBrokenExec" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="60px" />
										<web:columnStyle width="500px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column align="left">
											<button type="button" onclick="clickPreviousTab();" id="previousTab3" class="fa fa-arrow-left " title='Go to Tenor Details Section ' data-toggle='tooltip' style="font-size: 15px; margin-right: 5px; color: #3da0e3; cursor: pointer"></button>
										</web:column>
										<web:column align="right">
											<button type="button" onclick="clickNextTab();" id="nextTab3" class="fa fa-arrow-right " title='Go to Financial Details Section ' data-toggle='tooltip' style="font-size: 15px; margin-right: 5px; color: #3da0e3; cursor: pointer"></button>
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
						</type:tabbarBody>


						<type:tabbarBody id="lease_TAB_4">
							<web:section>
								<message:messageBox id="elease" />
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="240px" />
										<web:columnStyle width="700px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column align="left">
											<a class="fa fa-download  xlsColor" id="xls" style="position: relative; margin-left: 10px; cursor: pointer" onclick="doPrint()"></a>
											<web:anchorMessage var="program" key="elease.download" onclick="doPrint()" style="text-decoration:underline;cursor:pointer" id="xlsFile" />
											<!-- 											<a class="fa fa-file-excel-o  xlsColor" style="cursor: pointer" id="xls" onclick="doPrint()"></a> -->
										</web:column>
										<web:column align="right">
											<div id="noOfslab" style="color: #337ab7; font-size: 19px; padding: 0px 15px; font: normal 14px/1 FontAwesome;"></div>
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:viewContent id="leaseFinanceRentalSchedule">
								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="200px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowOdd>
											<web:column>
												<web:grid height="380px" width="1000px" id="financialScheduleOLGrid" src="lss/qleaseFinanceRentalSchedule_Grid.xml">
												</web:grid>
											</web:column>
										</web:rowOdd>
									</web:table>
								</web:section>
								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="550px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowOdd>
											<web:column align="left">
												<i class="fa fa-info-circle" style="color: #337ab7; font-size: 16px;" aria-hidden="true"> <span> <web:message key="elease.linkTaxInfo" var="program" /></span>
												</i>
											</web:column>
										</web:rowOdd>
									</web:table>
								</web:section>
							</web:viewContent>
							<web:viewContent id="leaseFinanceSchedule">
								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="200px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowOdd>
											<web:column>
												<web:grid height="380px" width="1000px" id="financialScheduleNonOLGrid" src="lss/qleaseFinanceSchedule_Grid.xml">
												</web:grid>
											</web:column>
										</web:rowOdd>
									</web:table>
								</web:section>
								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="550px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowOdd>
											<web:column align="left">
												<i class="fa fa-info-circle" style="color: #337ab7; font-size: 16px;" aria-hidden="true"> <span> <web:message key="elease.linkTaxInfo" var="program" /></span>
												</i>
											</web:column>
										</web:rowOdd>
									</web:table>
								</web:section>
							</web:viewContent>

							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="60px" />
										<web:columnStyle width="500px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column align="left">
											<button type="button" onclick="clickPreviousTab();" id="previousTab4" class="fa fa-arrow-left " title='Go to Rent Details Section ' data-toggle='tooltip' style="font-size: 15px; margin-right: 5px; color: #3da0e3; cursor: pointer"></button>
										</web:column>
										<web:column align="right">
											<button type="button" onclick="clickNextTab();" id="nextTab4" class="fa fa-arrow-right " title='Go to View Applied Tax Section ' data-toggle='tooltip' style="font-size: 15px; margin-right: 5px; color: #3da0e3; cursor: pointer"></button>
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>

							<web:viewContent id="taxDetails">
								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="130px" />
											<web:columnStyle width="280px" />
											<web:columnStyle width="150px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowOdd>
											<web:column>
												<web:legend key="elease.monthSl" var="program" />
											</web:column>
											<web:column>
												<type:serialDisplay property="monthSl" id="monthSl" />
											</web:column>
											<web:column>
												<web:legend key="elease.stateCode" var="program" />
											</web:column>
											<web:column>
												<type:stateCodeDisplay property="stateCode" id="stateCode" />
											</web:column>
										</web:rowOdd>
										<web:rowEven>
											<web:column>
												<web:legend key="elease.startDate" var="program" />
											</web:column>
											<web:column>
												<type:dateDisplay property="startTaxDate" id="startTaxDate" />
											</web:column>
											<web:column>
												<web:legend key="elease.endDate" var="program" />
											</web:column>
											<web:column>
												<type:dateDisplay property="endTaxDate" id="endTaxDate" />
											</web:column>
										</web:rowEven>
									</web:table>
								</web:section>
								<web:viewContent id="prinsInterestWise">
									<web:section>
										<web:table>
											<web:columnGroup>
												<web:columnStyle width="130px" />
												<web:columnStyle width="280px" />
												<web:columnStyle width="150px" />
												<web:columnStyle />
											</web:columnGroup>
											<web:rowOdd>
												<web:column>
													<web:legend key="elease.principalAmt" var="program" />
												</web:column>
												<web:column>
													<type:currencySmallAmountDisplay property="principalTaxAmt" id="principalTaxAmt" />
												</web:column>
												<web:column>
													<web:legend key="elease.interestAmt" var="program" />
												</web:column>
												<web:column>
													<type:currencySmallAmountDisplay property="interestTaxAmt" id="interestTaxAmt" />
												</web:column>
											</web:rowOdd>
										</web:table>
									</web:section>
								</web:viewContent>
								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="130px" />
											<web:columnStyle width="280px" />
											<web:columnStyle width="150px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowEven>
											<web:column>
												<web:legend key="elease.execTaxAmt" var="program" />
											</web:column>
											<web:column>
												<type:currencySmallAmountDisplay property="execTaxAmt" id="execTaxAmt" />
											</web:column>
											<web:column>
												<web:legend key="elease.rentalTaxAmt" var="program" />
											</web:column>
											<web:column>
												<type:currencySmallAmountDisplay property="rentalTaxAmt" id="rentalTaxAmt" />
											</web:column>
										</web:rowEven>
									</web:table>
								</web:section>
								<web:section>
									<web:viewTitle var="program" key="elease.taxWiseDetails" />
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="180px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowOdd>
											<web:column>
												<web:grid height="240px" width="881px" id="leaseTaxWiseDetails_Grid" src="lss/leaseTaxWiseDetails_Grid.xml">
												</web:grid>
											</web:column>
										</web:rowOdd>
									</web:table>
								</web:section>
								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="550px" />
											<web:columnStyle />
										</web:columnGroup>
										<tr>
											<web:rowOdd>
												<web:column>
													<i class="fa fa-info-circle" style="color: #337ab7; font-size: 16px;" aria-hidden="true"> <span> <web:message key="elease.linkInfo" var="program" /></span>
													</i>
												</web:column>
												<web:column></web:column>
											</web:rowOdd>
										</tr>
									</web:table>
								</web:section>
								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="180px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowEven>
											<web:column span="2">
												<p align="center">
													<type:button key="form.close" id="closeTax" var="common" onclick="closePopup('0')" />
												</p>
											</web:column>
										</web:rowEven>
									</web:table>
								</web:section>
							</web:viewContent>

						</type:tabbarBody>

						<type:tabbarBody id="lease_TAB_5">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="elease.taxScheduleCode" var="program" />
										</web:column>
										<web:column>
											<type:codeDisplay property="taxScheduleCode" id="taxScheduleCode" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:grid height="240px" width="938px" id="leaseApplicableTax_Grid" src="lss/leaseApplicableTax_Grid.xml">
											</web:grid>
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="550px" />
										<web:columnStyle />
									</web:columnGroup>
									<tr>
										<web:rowOdd>
											<web:column>
												<i class="fa fa-info-circle" style="color: red; font-size: 16px; padding-bottom: 10px;" aria-hidden="true"> <span> <web:message key="elease.taxDenoteInfo" var="program" /></span>
												</i>
												<span> </span>
												<i class="fa fa-info-circle" style="color: #337ab7; font-size: 16px;" aria-hidden="true"> <span> <web:message key="elease.linkInfo" var="program" /></span>
												</i>
											</web:column>
											<web:column></web:column>
										</web:rowOdd>
									</tr>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="60px" />
										<web:columnStyle width="500px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column align="left">
											<button type="button" onclick="clickPreviousTab();" id="previousTab5" class="fa fa-arrow-left " title='Go to Financial Details Section ' data-toggle='tooltip' style="font-size: 15px; margin-right: 5px; color: #3da0e3; cursor: pointer"></button>
										</web:column>
										<web:column align="right">
											<button type="button" onclick="clickNextTab();" id="nextTab5" class="fa fa-arrow-right " title='Go to Payment Section ' data-toggle='tooltip' style="font-size: 15px; margin-right: 5px; color: #3da0e3; cursor: pointer"></button>
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
						</type:tabbarBody>

						<type:tabbarBody id="lease_TAB_6">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="elease.grossNetTDS" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:comboDisplay id="grossNetTDS" property="grossNetTDS" datasourceid="ELEASE_TDS_TYPE" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>

							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:grid height="180px" width="1000px" id="leasePaymentDetails_Grid" src="lss/leasePaymentDetails_Grid.xml">
											</web:grid>
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>


							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="230px" />
										<web:columnStyle width="250px" />
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="elease.processCode" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:productCodeDisplay id="processCode" property="processCode" />
										</web:column>
										<web:column>
											<web:legend key="elease.invTempCode" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:productCodeDisplay id="invTempCode" property="invTempCode" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="elease.famDesc" var="program" />
										</web:column>
										<web:column>
											<type:otherInformation25Display id="famDesc" property="famDesc" />
										</web:column>
										<web:column>
											<web:legend key="elease.hierarCode" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:staffCodeDisplay id="hierarCode" property="hierarCode" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="elease.invCycleNo" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:twoDigitDisplay id="invCycleNo" property="invCycleNo" />
										</web:column>
										<web:column>
											<web:legend key="elease.initCycleNo" var="program" />
										</web:column>
										<web:column>
											<type:twoDigitDisplay id="initCycleNo" property="initCycleNo" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="230px" />
										<web:columnStyle width="250px" />
										<web:columnStyle width="150px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="elease.finalCycleNo" var="program" />
										</web:column>
										<web:column>
											<type:twoDigitDisplay id="finalCycleNo" property="finalCycleNo" />
										</web:column>
										<web:column>
											<web:legend key="form.remarks" var="common" />
										</web:column>
										<web:column>
											<type:remarksDisplay property="remarks" id="remarks" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>

							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="60px" />
										<web:columnStyle width="500px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column align="left">
											<button type="button" onclick="clickPreviousTab();" id="previousTab6" class="fa fa-arrow-left " title='Go to View Applier Tax Section ' data-toggle='tooltip' style="font-size: 15px; margin-right: 5px; color: #3da0e3; cursor: pointer"></button>
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
						</type:tabbarBody>

						<type:tabbar height="height :270px;" name="Address Details $$ Asset Details $$ Tenor Details $$ Rent Details $$ Financial Details $$ View Applied Tax $$ Payment Details " width="width :1030px;" required="7" id="lease" selected="0">
						</type:tabbar>

						<web:auditDisplay />
					</web:sectionBlock>
				</web:dividerBlock>
			</web:viewContent>
		</web:viewSubPart>
	</web:viewPart>
</web:fragment>



