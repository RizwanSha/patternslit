var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'ELEASEPOINVPAY';
var currencySubUnits = getCurrencyUnit(getBaseCurrency());
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
	pgmGrid.setColumnHidden(0, true);
	pgmGrid.setColTypes("ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro");
}
function clearFields() {
	$('#preLeaseRef').val(EMPTY_STRING);
	$('#preLeaseRefDate').val(EMPTY_STRING);
	$('#preLeaseRefSerial').val(EMPTY_STRING);
	clearNonPKFields();
}

function clearNonPKFields() {
	$('#leaseCode').val(EMPTY_STRING);
	$('#leaseCode_desc').html(EMPTY_STRING);
	$('#custId').val(EMPTY_STRING);
	$('#custName').val(EMPTY_STRING);
	$('#agreeNo').val(EMPTY_STRING);
	$('#agreeNo_desc').html(EMPTY_STRING);
	pgmGrid.clearAll();
	$('#selectedPoSl').val(EMPTY_STRING);
	$('#selectedInvSl').val(EMPTY_STRING);
	$('selectedInvSl_pic').prop('disabled', true);
	$('#selectedInvSlDisplay').val(EMPTY_STRING);
	$('#selectedInvSl').prop('readOnly', true);
	if ($('#action').val() == ADD) {
		$('#paymentSl').prop('readOnly', true);
	} else {
		$('#paymentSl').prop('readOnly', false);
	}
	$('#paymentSl').val(EMPTY_STRING);
	$('#paymentSl_desc').html(EMPTY_STRING);
	$('#payFor').prop('selectedIndex', ZERO);
	$('#payFavour').val(EMPTY_STRING);
	$('#payFavour_desc').html(EMPTY_STRING);
	$('#scheduleId').val(EMPTY_STRING);
	$('#scheduleId_desc').html(EMPTY_STRING);
	$('#date').val(EMPTY_STRING);
	$('#amtPaid').val(EMPTY_STRING);
	$('#amtPaidFormat').val(EMPTY_STRING);
	$('#amtPaid_curr').val(EMPTY_STRING);
	$('#amtPaidFormat').prop('readOnly', true);
	$('#eqLocalFormat').prop('readOnly', true);
	$('#amtPaid_curr').prop('readOnly', true);
	$('#eqLocal_curr').prop('readOnly', true);
	$('#eqLocal').val(EMPTY_STRING);
	$('#eqLocalFormat').val(EMPTY_STRING);
	$('#eqLocal_curr').val(EMPTY_STRING);
	$('#payRef').val(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	$('#amtPaidHidden').val(EMPTY_STRING);
	$('#eqLocalHidden').val(EMPTY_STRING);
	$('#currHidden').val(EMPTY_STRING);
}

function loadData() {
	loadAuditFields(validator);
	$('#preLeaseRefDate').val(validator.getValue('CONTRACT_DATE'));
	$('#preLeaseRefSerial').val(validator.getValue('CONTRACT_SL'));
	$('#action').val(RECTIFY);
	$('#paymentSl').val(validator.getValue('PAYMENT_SL'));
	$('#paymentSl').prop('readOnly', false);
	$('#selectedPoSl').val(validator.getValue('PO_SL'));
	$('#selectedPoSlDisplay').val($('#selectedPoSl').val());
	$('#selectedInvSl').val(validator.getValue('INVOICE_SL'));
	$('#selectedPoSlDisplay').val($('#selectedPoSl').val());
	$('#selectedInvSlDisplay').val($('#selectedInvSl').val());
	$('#payFor').val(validator.getValue('PAY_FOR'));
	$('#date').val(validator.getValue('PAYMENT_DATE'));
	$('#amtPaid_curr').val(validator.getValue('PAYMENT_CCY'));
	$('#amtPaidFormat').val(formatAmount(validator.getValue('PAYMENT_AMOUNT').toString(), $('#amtPaid_curr').val(), currencySubUnits));
	$('#amtPaid').val(unformatAmount($('#amtPaidFormat').val()));
	$('#eqLocal_curr').val(validator.getValue('PAYMENT_CCY'));
	$('#eqLocalFormat').val(formatAmount(validator.getValue('PAYMENT_AMOUNT_BC').toString(), $('#eqLocal_curr').val(), currencySubUnits));
	$('#eqLocal').val(unformatAmount($('#eqLocalFormat').val()));
	$('#payRef').val(validator.getValue('PAYMENT_REFERENCE'));
	$('#remarks').val(validator.getValue('REMARKS'));
	$('#action').val(RECTIFY);
	$('#paymentSl').prop('readOnly', false);
	$('#paymentSl_pic').prop('disabled', false);
	preLeaseRefSerial_val();
	$('#payFavour').val(pgmGrid.cells2(0, 9).getValue());
	$('#payFavour_desc').html(pgmGrid.cells2(0, 10).getValue());
	var posl = pgmGrid.cells2(0, 1).getValue().split("^");
	pgmGrid.cells2(0, 1).setValue(posl[0]);
	var invsl = pgmGrid.cells2(0, 7).getValue().split("^");
	pgmGrid.cells2(0, 7).setValue(invsl[0]);
}

function preLeaseRefSerial_val() {
	pgmGrid.clearAll();
	validator.clearMap();
	validator.setMtm(false);
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('CONTRACT_DATE', $('#preLeaseRefDate').val());
	validator.setValue('CONTRACT_SL', $('#preLeaseRefSerial').val());
	validator.setValue('PO_SL', $('#selectedPoSl').val());
	validator.setValue('INV_SL', $('#selectedInvSl').val());
	validator.setClass('patterns.config.web.forms.lss.eleasepoinvpaybean');
	validator.setMethod('validatePreLeaseContractRefSerial');
	validator.setValue(ACTION, 'RECTIFY');
	validator.setClass('patterns.config.web.forms.lss.eleasepoinvpaybean');
	validator.setMethod('validateAgreementNo');
	validator.sendAndReceive();
	if (validator.getValue("SCHEDULE_ID") == EMPTY_STRING || validator.getValue("SCHEDULE_ID") == NULL_VALUE) {
		$('#agreeNo').val(EMPTY_STRING);
		$('#scheduleId').val(EMPTY_STRING);
		$('#scheduleIDHidden').val(EMPTY_STRING);
		$('#custId').val(validator.getValue("CUSTOMER_ID"));
		$('#custName').val(validator.getValue("CUSTOMER_NAME"));
		$('#leaseCode').val(validator.getValue("SUNDRY_DB_AC"));
	} else {
		$('#agreeNo').val(validator.getValue("AGREEMENT_NO"));
		$('#scheduleId').val(validator.getValue("SCHEDULE_ID"));
		$('#scheduleIDHidden').val($('#scheduleId').val());
		$('#custId').val(validator.getValue("CUSTOMER_ID"));
		$('#custName').val(validator.getValue("CUSTOMER_NAME"));
		$('#leaseCode').val(validator.getValue("SUNDRY_DB_AC"));
	}
	if (isEmpty(validator.getValue("SCHEDULE_ID"))) {
		hide('scheduleDetail');
	} else {
		show('scheduleDetail');
	}
	$('#payFavour').val(validator.getValue("NAME"));
	var result = validator.getValue(RESULT_XML);
	pgmGrid.loadXMLString(result);
	return true;
}

function qsupplierdetails() {
	var pkValue = getEntityCode() + PK_SEPERATOR + $('#payFavour').val(pgmGrid.cells2(0, 9).getValue());
	window.scroll(0, 0);
	showWindow('qsupplier', getBasePath() + 'Renderer/cmn/qsupplier.jsp', QUERY_SUPPLIER_DETAILS, PK + '=' + pkValue + "&" + SOURCE + "=" + MAIN, true, false, false, 950, 550);
	resetLoading();
}