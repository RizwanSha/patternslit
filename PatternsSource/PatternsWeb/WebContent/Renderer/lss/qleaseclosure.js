var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'ELEASECLOSURE';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}
function clearFields() {
	$('#lesseCode').val(EMPTY_STRING);
	$('#customerID').val(EMPTY_STRING);
	$('#customerName').val(EMPTY_STRING);
	$('#agreementNumber').val(EMPTY_STRING);
	$('#scheduleID').val(EMPTY_STRING);
	setCheckbox('verifiedLeaseDetails', YES);
	$('#remarks').val(EMPTY_STRING);
	hide('hierarchy_view');
}
function qLease() {
	var pkValue = getEntityCode() + PK_SEPERATOR + $('#lesseCode').val() + PK_SEPERATOR + $('#agreementNumber').val() + PK_SEPERATOR + $('#scheduleID').val();
	window.scroll(0, 0);
	showWindow('QLease', getBasePath() + 'Renderer/lss/qlease.jsp', QUERY_LEASE_DETAILS, PK + '=' + pkValue + "&" + SOURCE + "=" + MAIN, true, false, false, 1162, 450);
	resetLoading();
}

function loadData() {
	$('#entryDate').val(validator.getValue('ENTRY_DATE'));
	$('#entrySerial').val(validator.getValue('ENTRY_SL'));
	$('#lesseCode').val(validator.getValue('LESSEE_CODE'));
	$('#customerID').val(validator.getValue('F1_CUSTOMER_ID'));
	$('#customerName').val(validator.getValue('F2_CUSTOMER_NAME'));
	$('#agreementNumber').val(validator.getValue('AGREEMENT_NO'));
	$('#scheduleID').val(validator.getValue('SCHEDULE_ID'));
	setCheckbox('verifiedLeaseDetails',YES);
	$('#referenceNumber').val(validator.getValue('REFERENCE_NO'));
	$('#remarks').val(validator.getValue('REMARKS'));
	if (!isEmpty($('#scheduleID').val())) {
		show('hierarchy_view');
	}
	loadAuditFields(validator);
}
