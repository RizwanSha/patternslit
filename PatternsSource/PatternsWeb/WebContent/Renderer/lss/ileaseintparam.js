var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'ILEASEINTPARAM';

function init() {
	refreshTBAGrid();
	refreshQueryGrid();
	if (_redisplay) {
		if ($('#action').val() == MODIFY) {
			$('#enabled').prop('disabled', false);
		} else {
			setCheckbox('enabled', YES);
			$('#enabled').prop('disabled', true);
		}
		if (!isEmpty($('#remittanceIFSC').val())) {
			remittanceIFSC_val();
		}
	}
}

function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'ILEASEINTPARAM_MAIN', queryGrid);
}

function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}
function doclearfields(id) {
	switch (id) {
	case 'productCode':
		if (isEmpty($('#productCode').val())) {
			$('#portfolioCode').val(EMPTY_STRING);
			$('#portfolioCode_error').html(EMPTY_STRING);
			$('#portfolioCode_desc').html(EMPTY_STRING);
			$('#effectiveDate').val(EMPTY_STRING);
			$('#effectiveDate_error').html(EMPTY_STRING);
			$('#effectiveDate').val(getCBD());
			clearNonPKFields();
			break;
		}
	case 'portfolioCode':
		if (isEmpty($('#portfolioCode').val())) {
			$('#effectiveDate').val(EMPTY_STRING);
			$('#effectiveDate_error').html(EMPTY_STRING);
			$('#effectiveDate').val(getCBD());
			clearNonPKFields();
			break;
		}
	case 'effectiveDate':
		if (isEmpty($('#effectiveDate').val())) {
			clearNonPKFields();
			break;
		}
	case 'remittanceIFSC':
		if (isEmpty($('#remittanceIFSC').val())) {
			$('#bankName').val(EMPTY_STRING);
			$('#branchName').val(EMPTY_STRING);
			$('#branchCity').val(EMPTY_STRING);
			$('#stateCode').val(EMPTY_STRING);
			$('#stateCode_desc').html(EMPTY_STRING);
			break;
		}
	}
}
function clearFields() {
	$('#productCode').val(EMPTY_STRING);
	$('#productCode_error').html(EMPTY_STRING);
	$('#productCode_desc').html(EMPTY_STRING);
	$('#portfolioCode').val(EMPTY_STRING);
	$('#portfolioCode_error').html(EMPTY_STRING);
	$('#portfolioCode_desc').html(EMPTY_STRING);
	$('#effectiveDate').val(EMPTY_STRING);
	$('#effectiveDate_error').html(EMPTY_STRING);
	$('#effectiveDate').val(getCBD());
	clearNonPKFields();
}
function clearNonPKFields() {
	$('#remittanceAccNumber').val(EMPTY_STRING);
	$('#remittanceAccNumber_error').html(EMPTY_STRING);
	$('#remittanceIFSC').val(EMPTY_STRING);
	$('#remittanceIFSC_error').html(EMPTY_STRING);
	$('#remittanceIFSC_desc').html(EMPTY_STRING);
	$('#bankName').val(EMPTY_STRING);
	$('#branchName').val(EMPTY_STRING);
	$('#branchCity').val(EMPTY_STRING);
	$('#stateCode').val(EMPTY_STRING);
	$('#stateCode_desc').html(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
}

function loadData() {
	$('#productCode').val(validator.getValue('LEASE_PRODUCT_CODE'));
	$('#productCode_desc').html(validator.getValue('F1_DESCRIPTION'));
	$('#portfolioCode').val(validator.getValue('PORTFOLIO_CODE'));
	$('#portfolioCode_desc').html(validator.getValue('F2_DESCRIPTION'));
	$('#effectiveDate').val(validator.getValue('EFF_DATE'));
	$('#remittanceAccNumber').val(validator.getValue('REMIT_TO_ACNO'));
	$('#remittanceIFSC').val(validator.getValue('IFSC_CODE'));
	$('#bankName').val(validator.getValue('F3_BANK_NAME'));
	$('#branchName').val(validator.getValue('F3_BRANCH_NAME'));
	$('#branchCity').val(validator.getValue('F3_CITY'));
	$('#stateCode').val(validator.getValue('F3_STATE_CODE'));
	$('#stateCode_desc').html(validator.getValue('F4_DESCRIPTION'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	resetLoading();
}

function view(source, primaryKey) {
	hideParent('lss/qleaseintparam', source, primaryKey);
	resetLoading();
}

function doHelp(id) {
	switch (id) {
	case 'productCode':
		help('COMMON', 'HLP_LEASE_PROD_CODE', $('#productCode').val(), EMPTY_STRING, $('#productCode'));
		break;
	case 'portfolioCode':
		help('COMMON', 'HLP_PORTFOLIO_CODE', $('#portfolioCode').val(), EMPTY_STRING, $('#portfolioCode'));
		break;
	case 'effectiveDate':
		if (!isEmpty($('#productCode').val())) {
			if (!isEmpty($('#portfolioCode').val())) {
				help(CURRENT_PROGRAM_ID, 'HLP_EFF_DATE', $('#effectiveDate').val(), $('#productCode').val() + PK_SEPERATOR + $('#portfolioCode').val(), $('#effectiveDate'));
			} else {
				setError('portfolioCode_error', MANDATORY);
			}
		} else {
			setError('productCode_error', MANDATORY);
		}
		break;
	case 'remittanceIFSC':
		help('COMMON', 'HLP_IFSC_CODE', $('#remittanceIFSC').val(), EMPTY_STRING,  $('#remittanceIFSC'),null, function(gridObj, rowID) {
			$('#remittanceIFSC').val(gridObj.cells(rowID, 0).getValue());
			$('#remittanceIFSC_desc').html(EMPTY_STRING);

		});
		break;
	}
}
function add() {
	$('#productCode').focus();
	$('#effectiveDate').val(getCBD());
	setCheckbox('enabled', YES);
	$('#enabled').prop('disabled', true);
}

function modify() {
	$('#productCode').focus();
	if ($('#action').val() == MODIFY) {
		$('#effectiveDate').val(EMPTY_STRING);
		$('#enabled').prop('disabled', false);
	} else {
		$('#enabled').prop('disabled', true);
	}
}

function validate(id) {
	switch (id) {
	case 'productCode':
		productCode_val();
		break;
	case 'portfolioCode':
		portfolioCode_val();
		break;
	case 'effectiveDate':
		effectiveDate_val();
		break;
	case 'remittanceAccNumber':
		remittanceAccNumber_val();
		break;
	case 'remittanceIFSC':
		remittanceIFSC_val();
		break;
	case 'enabled':
		enabled_val();
		break;
	case 'remarks':
		remarks_val();
		break;
	}
}

function backtrack(id) {
	switch (id) {
	case 'productCode':
		setFocusLast('productCode');
		break;
	case 'portfolioCode':
		setFocusLast('productCode');
		break;
	case 'effectiveDate':
		setFocusLast('portfolioCode');
		break;
	case 'remittanceAccNumber':
		setFocusLast('effectiveDate');
		break;
	case 'remittanceIFSC':
		setFocusLast('remittanceAccNumber');
		break;
	case 'enabled':
		setFocusLast('remittanceIFSC');
		break;
	case 'remarks':
		if ($('#action').val() == MODIFY) {
			setFocusLast('enabled');
		} else {
			setFocusLast('remittanceIFSC');
		}
		break;
	}
}
function productCode_val() {
	var value = $('#productCode').val();
	clearError('productCode_error');
	if (isEmpty(value)) {
		setError('productCode_error', MANDATORY);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('ENTITY_CODE', getEntityCode());
		validator.setValue('LEASE_PRODUCT_CODE', value);
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.lss.ileaseintparambean');
		validator.setMethod('validateProductCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('productCode_error', validator.getValue(ERROR));
			$('#productCode_desc').html(EMPTY_STRING);
			return false;
		}
		$('#productCode_desc').html(validator.getValue('DESCRIPTION'));
	}
	setFocusLast('portfolioCode');
	return true;
}
function portfolioCode_val() {
	var value = $('#portfolioCode').val();
	clearError('portfolioCode_error');
	if (isEmpty(value)) {
		setError('portfolioCode_error', MANDATORY);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('ENTITY_CODE', getEntityCode());
		validator.setValue('LEASE_PRODUCT_CODE', value);
		validator.setValue('PORTFOLIO_CODE', value);
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.lss.ileaseintparambean');
		validator.setMethod('validatePortfolioCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('portfolioCode_error', validator.getValue(ERROR));
			$('#portfolioCode_desc').html(EMPTY_STRING);
			return false;
		}
		$('#portfolioCode_desc').html(validator.getValue('DESCRIPTION'));
	}
	setFocusLast('effectiveDate');
	return true;
}
function effectiveDate_val() {
	var value = $('#effectiveDate').val();
	clearError('effectiveDate_error');
	if (isEmpty(value)) {
		$('#effectiveDate').val(getCBD());
		return true;
	}if (!isValidEffectiveDate(value, 'effectiveDate_error')) {
		return false;
	} 
	else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('ENTITY_CODE', getEntityCode());
		validator.setValue('LEASE_PRODUCT_CODE', $('#productCode').val());
		validator.setValue('PORTFOLIO_CODE', $('#portfolioCode').val());
		validator.setValue('EFF_DATE', value);
		validator.setValue(ACTION, $('#action').val());
		validator.setClass('patterns.config.web.forms.lss.ileaseintparambean');
		validator.setMethod('validateEffectiveDate');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('effectiveDate_error', validator.getValue(ERROR));
			return false;
		} else {
			if ($('#action').val() == MODIFY) {
				PK_VALUE = getEntityCode() + PK_SEPERATOR + $('#productCode').val() + PK_SEPERATOR + $('#portfolioCode').val() + PK_SEPERATOR + $('#effectiveDate').val();
				if (!loadPKValues(PK_VALUE, 'effectiveDate_error')) {
					return false;
				}
			}
		}
		setFocusLast('remittanceAccNumber');
		return true;
	}
}
function remittanceAccNumber_val() {
	var value = $('#remittanceAccNumber').val();
	clearError('remittanceAccNumber_error');
	if (isEmpty(value)) {
		setError('remittanceAccNumber_error', MANDATORY);
		return false;
	}
	if (!isValidAccNumber(value)) {
		setError('remittanceAccNumber_error', INVALID_ACNT_NUMBER);
		return false;
	}
	setFocusLast('remittanceIFSC');
	return true;
}
function remittanceIFSC_val() {
	var value = $('#remittanceIFSC').val();
	clearError('remittanceIFSC_error');
	if (isEmpty(value)) {
		setError('remittanceIFSC_error', MANDATORY);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('ENTITY_CODE', getEntityCode());
		validator.setValue('IFSC_CODE', value);
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.lss.ileaseintparambean');
		validator.setMethod('validateRemittanceIFSC');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('remittanceIFSC_error', validator.getValue(ERROR));
			$('#bankName').val(EMPTY_STRING);
			$('#branchName').val(EMPTY_STRING);
			$('#branchCity').val(EMPTY_STRING);
			$('#stateCode').val(EMPTY_STRING);
			$('#stateCode_desc').html(EMPTY_STRING);
			return false;
		} else {
			$('#bankName').val(validator.getValue("BANK_NAME"));
			$('#branchName').val(validator.getValue("BRANCH_NAME"));
			$('#branchCity').val(validator.getValue("CITY"));
			$('#stateCode').val(validator.getValue("STATE_CODE"));
			$('#stateCode_desc').html(validator.getValue("DESCRIPTION"));
		}
	}
	if ($('#action').val() == MODIFY) {
		setFocusLast('enabled');
	} else {
		setFocusLast('remarks');
	}
	return true;
}

function enabled_val() {
	if ($('#action').val() == MODIFY) {
		setFocusLast('remarks');
	}
	return true;
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', INVALID_REMARKS);
			setFocusLast('remarks');
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', INVALID_REMARKS);
			setFocusLast('remarks');
			return false;
		}
	}
	if ($('#action').val() == MODIFY) {
		if (isEmpty(value)) {
			setError('remarks_error', MANDATORY);
			setFocusLast('remarks');
			return false;
		}
	}
	setFocusOnSubmit();
	return true;
}
