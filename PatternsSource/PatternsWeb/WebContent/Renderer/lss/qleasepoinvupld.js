var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'ELEASEPOINVUPLD';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}
function clearFields() {
	$('#uploadDate').val(EMPTY_STRING);
	$('#uploadSerial').val(EMPTY_STRING);
	$('#preLeaseContRefDate').val(EMPTY_STRING);
	$('#preLeaseContRefSerial').val(EMPTY_STRING);
	$('#uploadSerial').val(EMPTY_STRING);
	$('#lesseCode').val(EMPTY_STRING);
	$('#customerID').val(EMPTY_STRING);
	$('#customerName').val(EMPTY_STRING);
	$('#poSerial').val(EMPTY_STRING);
	$('#referenceNo').val(EMPTY_STRING);
	$('#fileInventoryNumberExit').val(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
}

function loadData() {
	$('#uploadDate').val(validator.getValue('ENTRY_DATE'));
	$('#uploadSerial').val(validator.getValue('ENTRY_SL'));
	$('#preLeaseContRefDate').val(validator.getValue('CONTRACT_DATE'));
	$('#preLeaseContRefSerial').val(validator.getValue('CONTRACT_SL'));
	$('#lesseCode').val(validator.getValue('F1_SUNDRY_DB_AC'));
	$('#customerID').val(validator.getValue('F1_CUSTOMER_ID'));
	$('#customerName').val(validator.getValue('F2_CUSTOMER_NAME'));
	$('#poSerial').val(validator.getValue('PO_SL'));
	$('#fileName').val(validator.getValue('F3_FILE_NAME'));
	$('#fileInventoryNumber').val(validator.getValue('FILE_INV_NUM'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadAuditFields(validator);
}
function loadGrid() {
	SOURCE_VALUE = _tableType;
	PK_VALUE = _primaryKey;
	if (_tableType == MAIN) {
		viewDetails();

	} else if (_tableType == TBA) {
		viewDetails();
	}
}
function viewDetails() {
	win = showWindow('view_details', EMPTY_STRING, LEASE_INVOICE_DTL, EMPTY_STRING, true, false, false, 1250, 450);
	eleasepoinvupld_detailGrid.setColumnHidden(0, true);
	loadGridQuery(CURRENT_PROGRAM_ID, 'ELEASEPOINVUPLD_GRID', eleasepoinvupld_detailGrid);
}