var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'ELEASEPO';
var aggrementDate = null;
var w_model_req = null;
var w_conf_req = null;
function init() {
	refreshTBAGrid();
	refreshQueryGrid();
	if (_redisplay) {
		if ($('#action').val() == ADD && $('#rectify').val() == COLUMN_DISABLE) {
			$('#poSerial').prop('readonly', true);
			$('#poSerial_pic').prop('disabled', true);
		}
		preLeaseContractRefSerial_val(false);
		if (validator.getValue('SCHEDULE_ID') != null && !isEmpty(validator.getValue('SCHEDULE_ID')))
			show('scheduleDtl');
		else
			hide('scheduleDtl');
		eleasepo_innerGrid.clearAll();
		if (!isEmpty($('#xmleleasepoGrid').val())) {
			eleasepo_innerGrid.loadXMLString($('#xmleleasepoGrid').val());
		}
	}
}

function loadGrid() {
	loadGridQuery(CURRENT_PROGRAM_ID, 'ELEASEPO_GRID', eleasepo_innerGrid);
}

function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'ELEASEPO_MAIN');
}

function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}

function add() {
	$('#preLeaseContractRefDate').focus();
	$('#poSerial').prop('readonly', true);
	$('#poSerial_pic').prop('disabled', true);
	$('#poDate').val(getCBD());
}

function modify() {

}

function doHelp(id) {
	switch (id) {
	case 'preLeaseContractRef':
	case 'preLeaseContractRefDate':
	case 'preLeaseContractRefSerial':
		help('COMMON', 'HLP_PRE_LEASE_CONT_REF', $('#preLeaseContractRefSerial').val(), $('#preLeaseContractRefDate').val(), $('#preLeaseContractRefSerial'), null, function(gridObj, rowID) {
			$('#preLeaseContractRefDate').val(gridObj.cells(rowID, 0).getValue());
			$('#preLeaseContractRefSerial').val(gridObj.cells(rowID, 1).getValue());
		});
		break;
	case 'poSerial':
		help('COMMON', 'HLP_LEASE_PO_SL', $('#poSerial').val(), $('#preLeaseContractRefDate').val() + PK_SEPERATOR + $('#preLeaseContractRefSerial').val(), $('#poSerial'));
		break;
	case 'assetClassifi':
		help('COMMON', 'HLP_ASSET_TYPE', $('#assetClassifi').val(), EMPTY_STRING, $('#assetClassifi'));
		break;
	case 'supplierId':
		help('COMMON', 'HLP_SUPPLIER_ID', $('#supplierId').val(), EMPTY_STRING, $('#supplierId'));
		break;
	}
}

function view(source, primaryKey) {
	hideParent('lss/qleasepo', source, primaryKey);
	resetLoading();
}

function doclearfields(id) {
	switch (id) {
	case 'preLeaseContractRefDate':
		if (isEmpty($('#preLeaseContractRefDate').val())) {
			clearFields();
			break;
		}
	case 'preLeaseContractRefSerial':
		if (isEmpty($('#preLeaseContractRefSerial').val())) {
			$('#preLeaseContractRef_error').html(EMPTY_STRING);
			$('#lesseCode').val(EMPTY_STRING);
			$('#customerID').val(EMPTY_STRING);
			show('scheduleDtl');
			$('#aggrementNumber').val(EMPTY_STRING);
			$('#scheduleID').val(EMPTY_STRING);
			$('#customerName').val(EMPTY_STRING);
			$('#poSerial').val(EMPTY_STRING);
			$('#poSerial_error').html(EMPTY_STRING);
			clearNonPKFields();
			break;
		}
	case 'poSerial':
		if (isEmpty($('#poSerial').val())) {
			$('#poSerial_error').html(EMPTY_STRING);
			clearNonPKFields();
			break;
		}
	}
}

function clearFields() {
	$('#preLeaseContractRefDate').val(EMPTY_STRING);
	$('#preLeaseContractRef_error').html(EMPTY_STRING);
	$('#preLeaseContractRefSerial').val(EMPTY_STRING);
	$('#lesseCode').val(EMPTY_STRING);
	$('#customerID').val(EMPTY_STRING);
	$('#customerName').val(EMPTY_STRING);
	$('#aggrementNumber').val(EMPTY_STRING);
	$('#scheduleID').val(EMPTY_STRING);
	$('#poSerial').val(EMPTY_STRING);
	$('#poSerial_error').html(EMPTY_STRING);
	show('scheduleDtl');
	clearNonPKFields();
}

function clearNonPKFields() {
	$('#poDate').val(EMPTY_STRING);
	$('#poDate_error').html(EMPTY_STRING);
	$('#poDate').val(getCBD());
	$('#poNumber').val(EMPTY_STRING);
	$('#poNumber_error').html(EMPTY_STRING);
	$('#supplierId').val(EMPTY_STRING);
	$('#supplierId_desc').html(EMPTY_STRING);
	$('#supplierId_error').html(EMPTY_STRING);
	$('#poAmount').val(EMPTY_STRING);
	$('#poAmountFormat').val(EMPTY_STRING);
	$('#poAmount_error').html(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
	eleasepo_innerGrid.clearAll();
	eleasepo_clearGridFields();
}

function eleasepo_clearGridFields() {
	$('#assetClassifi').val(EMPTY_STRING);
	$('#assetClassifi_desc').html(EMPTY_STRING);
	$('#assetClassifi_error').html(EMPTY_STRING);
	$('#quantity').val(EMPTY_STRING);
	$('#quantity_error').html(EMPTY_STRING);
	$('#assetDescription').val(EMPTY_STRING);
	$('#assetDescription_error').html(EMPTY_STRING);
	$('#assetModel').prop('readOnly', false);
	$('#assetModel').val(EMPTY_STRING);
	$('#assetModel_error').html(EMPTY_STRING);
	$('#configuration').prop('readOnly', false);
	$('#configuration').val(EMPTY_STRING);
	$('#configuration_error').html(EMPTY_STRING);
	w_model_req = EMPTY_STRING;
	w_conf_req = EMPTY_STRING;
}

function loadData() {
	$('#poSerial').prop('readonly', false);
	$('#poSerial_pic').prop('disabled', false);
	$('#preLeaseContractRefDate').val(validator.getValue('CONTRACT_DATE'));
	$('#preLeaseContractRefSerial').val(validator.getValue('CONTRACT_SL'));
	$('#lesseCode').val(validator.getValue('F1_SUNDRY_DB_AC'));
	$('#customerID').val(validator.getValue('F1_CUSTOMER_ID'));
	$('#customerName').val(validator.getValue('F2_CUSTOMER_NAME'));
	$('#aggrementNumber').val(validator.getValue('F1_AGREEMENT_NO'));
	$('#scheduleID').val(validator.getValue('F1_SCHEDULE_ID'));
	$('#poSerial').val(validator.getValue('PO_SL'));
	$('#poDate').val(validator.getValue('PO_DATE'));
	$('#poNumber').val(validator.getValue('PO_NUMBER'));
	$('#supplierId').val(validator.getValue('SUPPLIER_ID'));
	$('#supplierId_desc').html(validator.getValue('F3_NAME'));
	$('#poAmount').val(validator.getValue('TOTAL_PO_AMOUNT'));
	$('#remarks').val(validator.getValue('REMARKS'));
	if (isEmpty($('#aggrementNumber').val()) || isEmpty($('#scheduleID').val()))
		hide('scheduleDtl');
	else
		show('scheduleDtl');
	loadGrid();
	$('#poAmountFormat').val(formatAmount($('#poAmount').val(), getBaseCurrency()));
	$('#preLeaseContractRefDate').focus();
	resetLoading();
}

function validate(id) {
	switch (id) {
	case 'preLeaseContractRefDate':
		preLeaseContractRefDate_val();
		break;
	case 'preLeaseContractRefSerial':
		preLeaseContractRefSerial_val(true);
		break;
	case 'poSerial':
		poSerial_val();
		break;
	case 'poDate':
		poDate_val();
		break;
	case 'poNumber':
		poNumber_val();
		break;
	case 'supplierId':
		supplierId_val();
		break;
	case 'poAmountFormat':
		poAmountFormat_val();
		break;
	case 'assetClassifi':
		assetClassifi_val(true);
		break;
	case 'assetDescription':
		assetDescription_val(true);
		break;
	case 'quantity':
		quantity_val(true);
		break;
	case 'assetModel':
		assetModel_val(true);
		break;
	case 'configuration':
		configuration_val(true);
		break;
	case 'remarks':
		remarks_val();
		break;
	}
}

function backtrack(id) {
	switch (id) {
	case 'preLeaseContractRefDate':
		setFocusLast('preLeaseContractRefDate');
		break;
	case 'preLeaseContractRefSerial':
		setFocusLast('preLeaseContractRefDate');
		break;
	case 'poSerial':
		setFocusLast('preLeaseContractRefSerial');
		break;
	case 'poDate':
		if ($('#poSerial').prop('readOnly'))
			setFocusLast('preLeaseContractRefSerial');
		else
			setFocusLast('poSerial');
		break;
	case 'poNumber':
		setFocusLast('poDate');
		break;
	case 'supplierId':
		setFocusLast('poNumber');
		break;
	case 'poAmountFormat':
		setFocusLast('supplierId');
		break;
	case 'assetClassifi':
		setFocusLast('poAmountFormat');
		break;
	case 'assetDescription':
		setFocusLast('assetClassifi');
		break;
	case 'quantity':
		setFocusLast('assetDescription');
		break;
	case 'assetModel':
		setFocusLast('quantity');
		break;
	case 'configuration':
		if ($('#assetModel').prop('readOnly'))
			setFocusLast('quantity');
		else
			setFocusLast('assetModel');
		break;
	case 'eleasepo_innerGrid_add':
		setFocusLast('configuration');
		break;
	case 'remarks':
		setFocusLast('assetClassifi');
		break;
	}
}

function preLeaseContractRefDate_val() {
	var value = $('#preLeaseContractRefDate').val();
	clearError('preLeaseContractRef_error');
	if (isEmpty(value)) {
		setError('preLeaseContractRef_error', MANDATORY);
		return false;
	}
	if (!isDate(value)) {
		setError('preLeaseContractRef_error', INVALID_DATE);
		return false;
	}
	if (isDateGreater(value, getCBD())) {
		setError('preLeaseContractRef_error', DATE_LECBD);
		return false;
	}
	setFocusLast('preLeaseContractRefSerial');
	return true;
}

function preLeaseContractRefSerial_val(valMode) {
	var value = $('#preLeaseContractRefSerial').val();
	clearError('preLeaseContractRef_error');
	if (isEmpty(value)) {
		setError('preLeaseContractRef_error', MANDATORY);
		return false;
	}
	if (!isNumeric(value)) {
		setError('preLeaseContractRef_error', INVALID_NUMBER);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('CONTRACT_DATE', $('#preLeaseContractRefDate').val());
	validator.setValue('CONTRACT_SL', value);
	validator.setValue(ACTION, USAGE);
	validator.setClass('patterns.config.web.forms.lss.eleasepobean');
	validator.setMethod('validatePreLeaseContractRefSerial');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('preLeaseContractRef_error', validator.getValue(ERROR));
		return false;
	}
	$('#lesseCode').val(validator.getValue('SUNDRY_DB_AC'));
	$('#customerID').val(validator.getValue('CUSTOMER_ID'));
	$('#customerName').val(validator.getValue('CUSTOMER_NAME'));
	if (validator.getValue('SCHEDULE_ID') != null && !isEmpty(validator.getValue('SCHEDULE_ID'))) {
		show('scheduleDtl');
		$('#aggrementNumber').val(validator.getValue('AGREEMENT_NO'));
		$('#scheduleID').val(validator.getValue('SCHEDULE_ID'));
	} else
		hide('scheduleDtl');
	aggrementDate = validator.getValue('AGREEMENT_DATE');
	if (valMode) {
		if ($('#rectify').val() == ZERO)
			setFocusLast('poDate');
		else
			setFocusLast('poSerial');
	}
	return true;
}

function poSerial_val() {
	var value = $('#poSerial').val();
	clearError('poSerial_error');
	if ($('#rectify').val() == COLUMN_ENABLE) {
		if (isEmpty(value)) {
			setError('poSerial_error', MANDATORY);
			return false;
		}
		if (!isNumeric(value)) {
			setError('poSerial_error', NUMERIC_CHECK);
			return false;
		} else {
			validator.clearMap();
			validator.setMtm(false);
			validator.setValue('CONTRACT_DATE', $('#preLeaseContractRefDate').val());
			validator.setValue('CONTRACT_SL', $('#preLeaseContractRefSerial').val());
			validator.setValue('PO_SL', value);
			validator.setValue(ACTION, $('#action').val());
			validator.setClass('patterns.config.web.forms.lss.eleasepobean');
			validator.setMethod('validatePOSerial');
			validator.sendAndReceive();
			if (validator.getValue(ERROR) != EMPTY_STRING) {
				setError('poSerial_error', validator.getValue(ERROR));
				return false;
			} else {
				PK_VALUE = getEntityCode() + PK_SEPERATOR + $('#preLeaseContractRefDate').val() + PK_SEPERATOR + $('#preLeaseContractRefSerial').val() + PK_SEPERATOR + $('#poSerial').val();
				if (!loadPKValues(PK_VALUE, 'poSerial_error')) {
					return false;
				}
			}
		}
	}
	setFocusLast('poDate');
	return true;
}

function poDate_val() {
	var value = $('#poDate').val();
	clearError('poDate_error');
	if (isEmpty(value)) {
		setError('poDate_error', MANDATORY);
		return false;
	}
	if (!isDate(value)) {
		setError('poDate_error', INVALID_DATE);
		return false;
	}
	if (isDateGreater(value, getCBD())) {
		setError('poDate_error', DATE_LCBD);
		return false;
	}
	if (isDateLesser(value, aggrementDate)) {
		setError('poDate_error', AGGREMENT_DATE_LESSER);
		return false;
	}
	if (isDateLesser(value, $('#preLeaseContractRefDate').val())) {
		setError('poDate_error', PO_DATE_GREATER);
		return false;
	}
	setFocusLast('poNumber');
	return true;
}

function poNumber_val() {
	var value = $('#poNumber').val();
	clearError('poNumber_error');
	if (isEmpty(value)) {
		setError('poNumber_error', MANDATORY);
		return false;
	}
	if (!isValidDescription(value)) {
		setError('poNumber_error', INVALID_DESCRIPTION);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('CONTRACT_DATE', $('#preLeaseContractRefDate').val());
	validator.setValue('CONTRACT_SL', $('#preLeaseContractRefSerial').val());
	validator.setValue('PO_NUMBER', $('#poNumber').val());
	validator.setValue('PO_SL', $('#poSerial').val());
	validator.setValue('PROGRAM_ID', CURRENT_PROGRAM_ID);
	validator.setValue('RECTIFY', $('#rectify').val());
	validator.setValue(ACTION, USAGE);
	validator.setClass('patterns.config.web.forms.lss.eleasepobean');
	validator.setMethod('validatePoNumber');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('poNumber_error', validator.getValue(ERROR));
		return false;
	}
	setFocusLast('supplierId');
	return true;
}

function supplierId_val() {
	var value = $('#supplierId').val();
	clearError('supplierId_error');
	if (isEmpty(value)) {
		setError('supplierId_error', MANDATORY);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('SUPPLIER_ID', $('#supplierId').val());
	validator.setValue(ACTION, USAGE);
	validator.setClass('patterns.config.web.forms.lss.eleasepobean');
	validator.setMethod('validateSupplierID');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('supplierId_error', validator.getValue(ERROR));
		$('#supplierId_desc').html(EMPTY_STRING);
		return false;
	}
	$('#supplierId_desc').html(validator.getValue('NAME'));
	setFocusLast('poAmountFormat');
	return true;
}

function poAmountFormat_val() {
	var value = unformatAmount($('#poAmountFormat').val());
	clearError('poAmount_error');
	if (isEmpty(value)) {
		setError('poAmount_error', MANDATORY);
		return false;
	}
	$('#poAmount').val(value);
	if (isZero(value)) {
		setError('poAmount_error', ZERO_CHECK);
		return false;
	}
	if (isNegativeAmount(value)) {
		setError('poAmount_error', POSITIVE_AMOUNT);
		return false;
	}
	if (!isCurrencySmallAmount(getBaseCurrency(), value)) {
		setError('poAmount_error', INVALID_SMALL_AMOUNT);
		return false;
	}
	setFocusLast('assetClassifi');
	return true;
}

function assetClassifi_val(valMode) {
	var value = $('#assetClassifi').val();
	clearError('assetClassifi_error');
	if (isEmpty(value)) {
		setError('assetClassifi_error', MANDATORY);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('ASSET_TYPE_CODE', value);
	validator.setValue(ACTION, USAGE);
	validator.setClass('patterns.config.web.forms.lss.eleasepobean');
	validator.setMethod('validateAssetClassification');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('assetClassifi_error', validator.getValue(ERROR));
		$('#assetClassifi_desc').html(EMPTY_STRING);
		return false;
	} else {
		$('#assetClassifi_desc').html(validator.getValue("DESCRIPTION"));
		w_model_req = validator.getValue("MODEL_NO");
		w_conf_req = validator.getValue("CONFIG_DTLS");
		if (w_model_req == 'N') {
			$('#assetModel').val(EMPTY_STRING);
			$('#assetModel_error').html(EMPTY_STRING);
			$('#assetModel').prop('readOnly', true);
		} else
			$('#assetModel').prop('readOnly', false);
		if (w_conf_req == 'N') {
			$('#configuration').val(EMPTY_STRING);
			$('#configuration_error').html(EMPTY_STRING);
			$('#configuration').prop('readOnly', true);
		} else
			$('#configuration').prop('readOnly', false);
	}
	if (valMode)
		setFocusLast('assetDescription');
	return true;
}

function assetDescription_val(valMode) {
	var value = $('#assetDescription').val();
	clearError('assetDescription_error');
	if (isEmpty(value)) {
		setError('assetDescription_error', MANDATORY);
		return false;
	}
	if (!isValidRemarks(value)) {
		setError('assetDescription_error', INVALID_DESCRIPTION);
		return false;
	}
	if (valMode)
		setFocusLast('quantity');
	return true;
}

function quantity_val(valMode) {
	var value = $('#quantity').val();
	clearError('quantity_error');
	if (isEmpty(value)) {
		$('#quantity').val(ONE);
		value = $('#quantity').val();
	}
	if (isZero(value)) {
		setError('quantity_error', ZERO_CHECK);
		return false;
	}
	if (!isNumeric(value)) {
		setError('quantity_error', INVALID_NUMBER);
		return false;
	}
	if (isNegative(value)) {
		setError('quantity_error', POSITIVE_CHECK);
		return false;
	}
	if (isNumberGreater(value, '99999')) {
		setError('quantity_error', INVALID_NUMBER);
		return false;
	}
	if (valMode)
		if (!$('#assetModel').prop('readonly'))
			setFocusLast('assetModel');
		else if (!$('#configuration').prop('readonly'))
			setFocusLast('configuration');
		else
			setFocusLastOnGridSubmit('eleasepo_innerGrid');
	return true;
}

function assetModel_val(valMode) {
	var value = $('#assetModel').val();
	clearError('assetModel_error');
	if (w_model_req != 'N') {
		if (w_model_req == 'M') {
			if (isEmpty(value)) {
				setError('assetModel_error', MANDATORY);
				return false;
			}
		}
		if (!isEmpty(value)) {
			if (!isValidOtherInformation25(value)) {
				setError('assetModel_error', INVALID_DESCRIPTION);
				return false;
			}
		}
	}
	if (valMode) {
		if (!$('#configuration').prop('readonly'))
			setFocusLast('configuration');
		else
			setFocusLastOnGridSubmit('eleasepo_innerGrid');
	}
	return true;
}

function configuration_val(valMode) {
	var value = $('#configuration').val();
	clearError('configuration_error');
	if (w_conf_req != 'N') {
		if (w_conf_req == 'M') {
			if (isEmpty(value)) {
				setError('configuration_error', MANDATORY);
				return false;
			}
		}
		if (!isEmpty(value)) {
			if (!isValidOtherInformation25(value)) {
				setError('configuration_error', INVALID_DESCRIPTION);
				return false;
			}
		}
	}
	if (valMode)
		setFocusLastOnGridSubmit('eleasepo_innerGrid');
	return true;
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', INVALID_REMARKS);
			setFocusLast('remarks');
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', INVALID_REMARKS);
			setFocusLast('remarks');
			return false;
		}
	}
	if ($('#rectify').val() == COLUMN_ENABLE) {
		if (isEmpty(value)) {
			setError('remarks_error', MANDATORY);
			setFocusLast('remarks');
			return false;
		}
	}
	setFocusOnSubmit();
	return true;
}

function eleasepo_addGrid(editMode, editRowId) {
	if (!validateGridFields()) {
		return false;
	}
	var field_values = [ 'false', $('#assetClassifi').val(), $('#assetDescription').val(), $('#quantity').val(), $('#assetModel').val(), $('#configuration').val(), w_model_req, w_conf_req ];
	_grid_updateRow(eleasepo_innerGrid, field_values);
	eleasepo_clearGridFields();
	$('#assetClassifi').focus();
	return true;
}

function eleasepo_editGrid(rowId) {
	eleasepo_clearGridFields();
	$('#assetClassifi').val(eleasepo_innerGrid.cells(rowId, 1).getValue());
	$('#assetDescription').val(eleasepo_innerGrid.cells(rowId, 2).getValue());
	$('#quantity').val(eleasepo_innerGrid.cells(rowId, 3).getValue());
	$('#assetModel').val(eleasepo_innerGrid.cells(rowId, 4).getValue());
	$('#configuration').val(eleasepo_innerGrid.cells(rowId, 5).getValue());
	w_model_req = eleasepo_innerGrid.cells(rowId, 6).getValue();
	w_conf_req = eleasepo_innerGrid.cells(rowId, 7).getValue();
	if (w_model_req == 'N')
		$('#assetModel').prop('readOnly', true);
	if (w_conf_req == 'N')
		$('#configuration').prop('readOnly', true);
	$('#assetClassifi').focus();
}


function eleasepo_cancelGrid(rowId) {
	eleasepo_clearGridFields();
}

function gridExit(id) {
	switch (id) {
	case 'assetClassifi':
	case 'assetDescription':
	case 'quantity':
	case 'assetModel':
	case 'configuration':
		if (eleasepo_innerGrid.getRowsNum() > 0) {
			$('#xmleleasepoGrid').val(eleasepo_innerGrid.serialize());
			eleasepo_clearGridFields();
			setFocusLast('remarks');
		} else {
			$('#xmleleasepoGrid').val(eleasepo_innerGrid.serialize());
			setError('assetClassifi_error', ADD_ATLEAST_ONE_ROW);
			setFocusLast('assetClassifi');
		}
		break;

	}
}

function gridDeleteRow(id) {
	switch (id) {
	case 'assetClassifi':
		deleteGridRecord(eleasepo_innerGrid);
		$('#xmleleasepoGrid').val(eleasepo_innerGrid.serialize());
		break;
	}
}

function validateGridFields() {
	var errors = 0;
	if (!assetClassifi_val(false)) {
		errors++;
	}
	if (!assetDescription_val(false)) {
		errors++;
	}
	if (!quantity_val(false)) {
		errors++;
	}
	if (!assetModel_val(false)) {
		errors++;
	}
	if (!configuration_val(false)) {
		errors++;
	}
	if (errors > 0)
		return false;
	else
		return true;
}

function revalidate() {
	eleasepo_revaildateGrid();
	$('#poAmount').val(unformatAmount($('#poAmountFormat').val()));
}

function eleasepo_revaildateGrid() {
	if (eleasepo_innerGrid.getRowsNum() > 0) {
		$('#xmleleasepoGrid').val(eleasepo_innerGrid.serialize());
	} else {
		eleasepo_innerGrid.clearAll();
		$('#xmleleasepoGrid').val(eleasepo_innerGrid.serialize());
		setError('assetClassifi_error', ADD_ATLEAST_ONE_ROW);
		errors++;
	}
}
