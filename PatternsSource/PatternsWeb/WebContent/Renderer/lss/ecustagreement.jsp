<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.lss.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/lss/ecustagreement.js" />
	</web:dependencies>
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="ecustagreement.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/lss/ecustagreement" id="ecustagreement" method="POST">
				<web:dividerBlock>
					<type:optionBox add="true" modify="true" view="true" />
				</web:dividerBlock>
				<web:viewContent id="po_view1" styleClass="hidden">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="ecustagreement.customerid" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:customerID property="customerID" id="customerID" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="ecustagreement.agreementserial" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:agreementSerial property="agreementSerial" id="agreementSerial" lookup="true"/>
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="ecustagreement.agreementno" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:description property="agreementNo" id="agreementNo" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
						</web:sectionBlock>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="ecustagreement.agreementdate" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:date property="agreementDate" id="agreementDate" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="ecustagreement.sancref" var="program" />
									</web:column>
									<web:column>
										<type:otherInformation25 property="sancRef" id="sancRef" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="ecustagreement.sancamount" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:currencySmallAmount property="sancAmount" id="sancAmount" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:sectionTitle var="program" key="ecustagreement.section" />
						<web:section>
							<web:table>
								<web:rowOdd>
									<web:column>
										<web:button id="productAgreement" key="ecustagreement.adddocument" var="program" onclick="addProductAgreement()" />
										<web:button id="clearProductAgreementGrid" key="ecustagreement.clear" var="program" onclick="clearProductAgreement()" />
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
						<type:error property="gridError" />
						<web:viewContent id="productAgreement_Fields" styleClass="form-horizontal hidden">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="ecustagreement.productcode" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:leaseProductCode property="productCode" id="productCode" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:rowOdd>
										<web:column>
											<web:button id="productDtl" key="ecustagreement.update" var="program" onclick="updateProductAgreement()" />
											<web:button id="clearProductAgreementGrid" key="ecustagreement.clearfields" var="program" onclick="clearProductAgreement()" />
											<web:button key="form.close" id="close" var="common" onclick="closePopup('0')" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
						</web:viewContent>
						<web:section>
							<web:table>
								<web:rowEven>
									<web:column>
										<web:grid height="210px" width="381px" id="ecustagreement_innerGrid" src="lss/ecustagreement_innerGrid.xml">
										</web:grid>
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowOdd>
									<web:column>
										<web:legend key="form.remarks" var="common" />
									</web:column>
									<web:column>
										<type:remarks property="remarks" id="remarks" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column span="2">
										<web:submitReset />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
					</web:dividerBlock>
				</web:viewContent>
				<web:element property="xmlProductAgreementGrid" />
				<web:element property="command" />
				<web:element property="action" />
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
	<web:queryGrid />
	<web:TBAGrid />
	<web:contextSearch />
</web:fragment>
