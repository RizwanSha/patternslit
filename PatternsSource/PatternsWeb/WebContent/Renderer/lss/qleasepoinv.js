var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'ELEASEPOINV';
var currencySubUnits = getCurrencyUnit(getBaseCurrency());
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
	pgmGrid.setColumnHidden(0, true);
}
function clearFields() {
	$('#preLeaseRef').val(EMPTY_STRING);
	$('#preLeaseRefDate').val(EMPTY_STRING);
	$('#preLeaseRefSerial').val(EMPTY_STRING);
	clearNonPKFields();
}

function clearNonPKFields() {
	$('#leaseCode').val(EMPTY_STRING);
	$('#leaseCode_desc').html(EMPTY_STRING);
	$('#custId').val(EMPTY_STRING);
	$('#custName').val(EMPTY_STRING);
	$('#agreeNo').val(EMPTY_STRING);
	$('#agreeNo_desc').html(EMPTY_STRING);
	pgmGrid.clearAll();
	pgmGrid2.clearAll();
	$('#purchOrdSl').val(EMPTY_STRING);
	$('#purchOrdSlDisplay').val(EMPTY_STRING);
	$('#invSl').val(EMPTY_STRING);
	$('#invSl_desc').html(EMPTY_STRING);
	$('#invFor').prop('selectedIndex', ZERO);
	$('#totalValHidden').val(ZERO);
	$('#scheduleId').val(EMPTY_STRING);
	$('#scheduleId_desc').html(EMPTY_STRING);
	$('#invDate').val(EMPTY_STRING);
	$('#invNum').val(EMPTY_STRING);
	$('#invAmt').val(EMPTY_STRING);
	$('#invAmtFormat').val(EMPTY_STRING);
	$('#invAmt_curr').val(EMPTY_STRING);
	$('#paySl').val(EMPTY_STRING);
	$('#paySl_desc').html(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	invValue = EMPTY_STRING;
	outStandingAmt = EMPTY_STRING;
	poAmt = EMPTY_STRING;
	paidAmtFormat = EMPTY_STRING;
	dateOdPayment = EMPTY_STRING;
	eleasepoinvasset_innerGrid.clearAll();
}

function loadGrid() {
	pgmGrid2.setColumnHidden(0, true);
	loadGridQuery(CURRENT_PROGRAM_ID, 'ELEASEPOINV_GRID', postProcessing);
}
function loadAssetGrid() {
	eleasepoinvasset_innerGrid.setColumnHidden(0, true);
	loadGridQuery(CURRENT_PROGRAM_ID, 'ELEASEPOINVASSET_GRID', assetpostProcessing);
}
function postProcessing(result) {
	pgmGrid2.loadXMLString(result);
	var totalVal = 0;
	for (var i = 0; i < pgmGrid2.getRowsNum(); i++) {
		totalVal = parseFloat(totalVal) + parseFloat(pgmGrid2.cells2(i, 4).getValue());
	}
	$('#totalVal').html(formatAmount(totalVal.toString(), getBaseCurrency()));
}

function assetpostProcessing(result) {
	eleasepoinvasset_innerGrid.loadXMLString(result);
	var totalVal = 0;
	for (var i = 0; i < eleasepoinvasset_innerGrid.getRowsNum(); i++) {
		totalVal = parseFloat(totalVal) + parseFloat(eleasepoinvasset_innerGrid.cells2(i, 7).getValue());
	}
	$('#totalValue').html(formatAmount(totalVal.toString(), getBaseCurrency()));
}


function loadData() {
	$('#preLeaseRefDate').val(validator.getValue('CONTRACT_DATE'));
	$('#preLeaseRefSerial').val(validator.getValue('CONTRACT_SL'));
	$('#purchOrdSl').val(validator.getValue('PO_SL'));
	$('#purchOrdSlDisplay').val($('#purchOrdSl').val());
	$('#invSl').val(validator.getValue('INVOICE_SL'));
	$('#invFor').val(validator.getValue('INV_FOR'));
	$('#invDate').val(validator.getValue('INVOICE_DATE'));
	$('#invNum').val(validator.getValue('INVOICE_NUMBER'));
	$('#invAmt_curr').val(validator.getValue('INVOICE_CCY'));
	$('#invAmtFormat').val(formatAmount(validator.getValue('INVOICE_AMOUNT').toString(), $('#invAmt_curr').val(), currencySubUnits));
	$('#invAmt').val(unformatAmount($('#invAmtFormat').val()));
	$('#remarks').val(validator.getValue('REMARKS'));
	$('#action').val(RECTIFY);
	loadAuditFields(validator);
	if ($('#invFor').val() == 'P') {
		$('#paydet').removeClass('hidden');
		loadGrid();
	} else {
		$('#paydet').addClass('hidden');
		pgmGrid2.clearAll();
		$('#paySl').val(EMPTY_STRING);
		$('#paySl_error').html(EMPTY_STRING);
		$('#paySl_desc').html(EMPTY_STRING);
	}
	loadAssetGrid();
	preLeaseRefSerial_val();
}

function preLeaseRefSerial_val() {
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('CONTRACT_DATE', $('#preLeaseRefDate').val());
	validator.setValue('CONTRACT_SL', $('#preLeaseRefSerial').val());
	validator.setValue('PO_SL', $('#purchOrdSl').val());
	validator.setValue(ACTION, RECTIFY);
	validator.setClass('patterns.config.web.forms.lss.eleasepoinvbean');
	validator.setMethod('validatePreLeaseContractRefSerial');
	validator.sendAndReceive();
	if (validator.getValue("SCHEDULE_ID") == EMPTY_STRING || validator.getValue("SCHEDULE_ID") == NULL_VALUE) {
		$('#agreeNo').val(EMPTY_STRING);
		$('#scheduleId').val(EMPTY_STRING);
		hide('scheduleDtl');
		$('#custId').val(validator.getValue("CUSTOMER_ID"));
		$('#custName').val(validator.getValue("CUSTOMER_NAME"));
		$('#leaseCode').val(validator.getValue("SUNDRY_DB_AC"));
	} else {
		show('scheduleDtl');
		$('#agreeNo').val(validator.getValue("AGREEMENT_NO"));
		$('#scheduleId').val(validator.getValue("SCHEDULE_ID"));
		$('#custId').val(validator.getValue("CUSTOMER_ID"));
		$('#custName').val(validator.getValue("CUSTOMER_NAME"));
		$('#leaseCode').val(validator.getValue("SUNDRY_DB_AC"));
	}
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		$('#custId').val(EMPTY_STRING);
		$('#custName').val(EMPTY_STRING);
		$('#leaseCode').val(EMPTY_STRING);
		$('#agreeNo').val(EMPTY_STRING);
		$('#scheduleId').val(EMPTY_STRING);
		pgmGrid.clearAll();
		return false;
	} else {
		var result = validator.getValue(RESULT_XML);
		pgmGrid.loadXMLString(result);
	}
	return true;
}