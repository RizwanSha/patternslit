var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'ELEASEPOINV';
var invValue = EMPTY_STRING;
var poDate = EMPTY_STRING;
var outStandingAmt = EMPTY_STRING;
var poAmt = EMPTY_STRING;
var currencySubUnits = getCurrencyUnit(getBaseCurrency());
var cbd = getCBD();
var dateOdPayment = EMPTY_STRING;
var paidAmt = EMPTY_STRING;
var paidAmtFormat = EMPTY_STRING;
var w_model_req = null;
var w_conf_req = null;
function init() {
	pgmGrid.attachEvent("onRowSelect", doRowSelect);
	refreshTBAGrid();
	refreshQueryGrid();
	if (_redisplay) {
		if ($('#invFor').val() == 'N')
			$('#paydet').addClass('hidden');
		if (!isEmpty($('#xmlGrid').val())) {
			pgmGrid.loadXMLString($('#xmlGrid').val());
		}
		if (!isEmpty($('#xmlGrid2').val())) {
			pgmGrid2.loadXMLString($('#xmlGrid2').val());
			$('#totalVal').html(formatAmount($('#totalValHidden').val().toString(), getBaseCurrency()));
		}
		if ($('#action').val() == ADD) {
			$('#invSl').prop('readOnly', true);
		} else {
			$('#invSl').prop('readOnly', false);
		}
		preLeaseRefSerial_val();
		if (validator.getValue('SCHEDULE_ID') != null && !isEmpty(validator.getValue('SCHEDULE_ID')))
			show('scheduleDtl');
		else
			hide('scheduleDtl');
		leaseCode_val();
		$('#purchOrdSl').val($('#purchOrdSlDisplay').val());
		// pgmGrid.attachEvent("onRowSelect", doRowSelect);
		$('#scheduleId').val($('#scheduleIDHidden').val());
		if ($('#action').val() == RECTIFY) {
			pgmGrid.cells2(0, 0).setValue(ONE);
		}
		eleasepoinvasset_innerGrid.clearAll();
		if (!isEmpty($('#xmleleasepoinvassetGrid').val())) {
			eleasepoinvasset_innerGrid.loadXMLString($('#xmleleasepoinvassetGrid').val());
			$('#totalValue').html(formatAmount($('#totalValueAmount').val().toString(), getBaseCurrency()));
		}

	}
	$('#paydet').removeClass('hidden');
	setFocusLast('preLeaseRefDate');
}
function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'ELEASEPOINV_MAIN', queryGrid);
}
function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}
function loadGrid() {
	loadGridQuery(CURRENT_PROGRAM_ID, 'ELEASEPOINV_GRID', postProcessing);
}
function loadAssetGrid() {
	loadGridQuery(CURRENT_PROGRAM_ID, 'ELEASEPOINVASSET_GRID', assetpostProcessing);
}
function postProcessing(result) {
	pgmGrid2.loadXMLString(result);
	var totalVal = 0;
	for (var i = 0; i < pgmGrid2.getRowsNum(); i++) {
		totalVal = parseFloat(totalVal) + parseFloat(pgmGrid2.cells2(i, 4).getValue());
	}
	$('#totalValHidden').val(totalVal);
	$('#totalVal').html(formatAmount($('#totalValHidden').val().toString(), getBaseCurrency()));
}

function assetpostProcessing(result) {
	eleasepoinvasset_innerGrid.loadXMLString(result);
	var totalVal = 0;
	for (var i = 0; i < eleasepoinvasset_innerGrid.getRowsNum(); i++) {
		totalVal = parseFloat(totalVal) + parseFloat(eleasepoinvasset_innerGrid.cells2(i, 7).getValue());
	}
	$('#totalValueAmount').val(totalVal);
	$('#totalValue').html(formatAmount($('#totalValueAmount').val().toString(), getBaseCurrency()));
}

function view(source, primaryKey) {
	hideParent('lss/qleasepoinv', source, primaryKey, 1100);
	resetLoading();
}
function doHelp(id) {
	switch (id) {
	case 'preLeaseRef':
	case 'preLeaseRefDate':
	case 'preLeaseRefSerial':
		help('COMMON', 'HLP_PRE_LEASE_CONT_REF', $('#preLeaseRefSerial').val(), $('#preLeaseRefSerial').val(), $('#preLeaseRefSerial'), null, function(gridObj, rowID) {
			$('#preLeaseRefDate').val(gridObj.cells(rowID, 0).getValue());
			$('#preLeaseRefSerial').val(gridObj.cells(rowID, 1).getValue());
		});
		break;
	case 'agreeNo':
		if (!isEmpty($('#leaseCode').val())) {
			help('COMMON', 'HLP_LEASE_AGRMNT', $('#agreeNo').val(), $('#custId').val(), $('#agreeNo'), function() {
			});
		} else {
			setError('leaseCode_error', MANDATORY);
		}
		break;
	case 'invSl':
		if (!isEmpty($('#agreeNo').val())) {
			help('COMMON', 'HLP_LEASE_PO_INV_SL', $('#invSl').val(), $('#preLeaseRefDate').val() + PK_SEPERATOR + $('#preLeaseRefSerial').val() + PK_SEPERATOR + $('#purchOrdSl').val(), $('#invSl'));
		} else {
			setError('agreeNo_error', MANDATORY);
		}
		break;

	case 'paySl':
		help('ELEASEPOINV', 'HLP_PAYMENT_SERIAL', $('#paySl').val(), $('#preLeaseRefDate').val() + PK_SEPERATOR + $('#preLeaseRefSerial').val() + PK_SEPERATOR + $('#purchOrdSl').val(), $('#paySl'));
		break;
	case 'assetClassifi':
		help('COMMON', 'HLP_ASSET_TYPE', $('#assetClassifi').val(), EMPTY_STRING, $('#assetClassifi'));
		break;
	}
}

function clearFields() {
	$('#preLeaseRef').val(EMPTY_STRING);
	$('#preLeaseRefDate').val(EMPTY_STRING);
	clearPKFields();
}
function clearPKFields() {
	$('#preLeaseRefSerial').val(EMPTY_STRING);
	$('#preLeaseRef_error').html(EMPTY_STRING);
	$('#leaseCode').val(EMPTY_STRING);
	$('#leaseCode_error').html(EMPTY_STRING);
	$('#leaseCode_desc').html(EMPTY_STRING);
	$('#custId').val(EMPTY_STRING);
	$('#custName').val(EMPTY_STRING);
	$('#agreeNo').val(EMPTY_STRING);
	$('#agreeNo_error').html(EMPTY_STRING);
	$('#agreeNo_desc').html(EMPTY_STRING);
	$('#purchOrdSl').val(EMPTY_STRING);
	$('#purchOrdSlDisplay').val(EMPTY_STRING);
	pgmGrid.clearAll();
	clearNonPKFields(true);
	show('scheduleDtl');
}

function clearNonPKFields(valmode) {
	pgmGrid2.clearAll();

	if ($('#action').val() == ADD) {
		$('#invSl').prop('readOnly', true);
	} else {
		$('#invSl').prop('readOnly', false);
	}
	$('#invSl').val(EMPTY_STRING);
	$('#invSl_error').html(EMPTY_STRING);
	$('#invSl_desc').html(EMPTY_STRING);
	$('#invFor').prop('selectedIndex', ZERO);
	$('#invFor_error').html(EMPTY_STRING);
	$('#totalValHidden').val(ZERO);
	if (valmode) {
		$('#scheduleId').val(EMPTY_STRING);
		$('#scheduleId_error').html(EMPTY_STRING);
		$('#scheduleId_desc').html(EMPTY_STRING);
	}
	$('#invDate').val(EMPTY_STRING);
	$('#invDate_error').html(EMPTY_STRING);
	$('#invNum').val(EMPTY_STRING);
	$('#invNum_error').html(EMPTY_STRING);
	$('#invAmt').val(EMPTY_STRING);
	$('#invAmtFormat').val(EMPTY_STRING);
	$('#invAmt_error').html(EMPTY_STRING);
	$('#invAmt_curr').val(EMPTY_STRING);
	$('#paySl').val(EMPTY_STRING);
	$('#paySl_error').html(EMPTY_STRING);
	$('#paySl_desc').html(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
	$('#totalValHidden').html(EMPTY_STRING);
	$('#totalValueAmount').val(ZERO);
	$('#totalVal').html(EMPTY_STRING);
	$('#totalValue').html(EMPTY_STRING);
	invValue = EMPTY_STRING;
	outStandingAmt = EMPTY_STRING;
	poAmt = EMPTY_STRING;
	paidAmtFormat = EMPTY_STRING;
	dateOdPayment = EMPTY_STRING;
	eleasepoinv_clearAssetGridFields();
	eleasepoinvasset_innerGrid.clearAll();
}

function eleasepoinv_clearAssetGridFields() {
	$('#assetClassifi').val(EMPTY_STRING);
	$('#assetClassifi_desc').html(EMPTY_STRING);
	$('#assetClassifi_error').html(EMPTY_STRING);
	$('#quantity').val(EMPTY_STRING);
	$('#quantity_error').html(EMPTY_STRING);
	$('#assetDescription').val(EMPTY_STRING);
	$('#assetDescription_error').html(EMPTY_STRING);
	$('#assetModel').val(EMPTY_STRING);
	$('#assetModel_error').html(EMPTY_STRING);
	$('#configuration').val(EMPTY_STRING);
	$('#configuration_error').html(EMPTY_STRING);
	$('#valueFormat').val(EMPTY_STRING);
	$('#value').val(EMPTY_STRING);
	$('#value_error').html(EMPTY_STRING);
	w_model_req = EMPTY_STRING;
	w_conf_req = EMPTY_STRING;
	// $('#cstFormat').val(EMPTY_STRING);
	// $('#cst').val(EMPTY_STRING);
	// $('#cst_error').html(EMPTY_STRING);
	// $('#vatFormat').val(EMPTY_STRING);
	// $('#vat').val(EMPTY_STRING);
	// $('#vat_error').html(EMPTY_STRING);
	// $('#stFormat').val(EMPTY_STRING);
	// $('#st').val(EMPTY_STRING);
	// $('#st_error').html(EMPTY_STRING);
	// $('#tcsFormat').val(EMPTY_STRING);
	// $('#tcs').val(EMPTY_STRING);
	// $('#tcs_error').html(EMPTY_STRING);
	// $('#vatOnAmountFormat').val(EMPTY_STRING);
	// $('#vatOnAmount').val(EMPTY_STRING);
	// $('#vatOnAmount_error').html(EMPTY_STRING);
	// $('#totalFormat').val(EMPTY_STRING);
	// $('#total_curr').val(getBaseCurrency());
}

function eleasepoinv_clearGridFields() {
	$('#paySl').val(EMPTY_STRING);
	$('#paySl_error').html(EMPTY_STRING);
	$('#paySl_desc').html(EMPTY_STRING);
}

function add() {
	$('#invSl').prop('readOnly', true);
	$('#invSl_pic').prop('disabled', true);
	$('#preLeaseRefDate').focus();
}
function modify() {

}

function change(id) {
	switch (id) {
	case 'invFor':
		invFor_val();
		break;
	}
}

function loadData() {
	$('#preLeaseRefDate').val(validator.getValue('CONTRACT_DATE'));
	$('#preLeaseRefSerial').val(validator.getValue('CONTRACT_SL'));
	$('#action').val(RECTIFY);
	$('#purchOrdSl').val(validator.getValue('PO_SL'));
	$('#purchOrdSlDisplay').val($('#purchOrdSl').val());
	$('#invSl').val(validator.getValue('INVOICE_SL'));
	$('#invSl').prop('readOnly', false);
	$('#invFor').val(validator.getValue('INV_FOR'));
	$('#invDate').val(validator.getValue('INVOICE_DATE'));
	$('#invNum').val(validator.getValue('INVOICE_NUMBER'));
	$('#invAmt_curr').val(validator.getValue('INVOICE_CCY'));
	$('#invAmtFormat').val(formatAmount(validator.getValue('INVOICE_AMOUNT').toString(), $('#invAmt_curr').val(), currencySubUnits));
	$('#invAmt').val(unformatAmount($('#invAmtFormat').val()));
	$('#remarks').val(validator.getValue('REMARKS'));
	$('#invSl_pic').prop('disabled', false);
	if ($('#invFor').val() == 'P') {
		$('#paydet').removeClass('hidden');
		loadGrid();
	} else {
		$('#paydet').addClass('hidden');
		pgmGrid2.clearAll();
		$('#paySl').val(EMPTY_STRING);
		$('#paySl_error').html(EMPTY_STRING);
		$('#paySl_desc').html(EMPTY_STRING);
	}
	loadAssetGrid();
	preLeaseRefSerial_val();
	leaseCode_val();
	setFocusLast('preLeaseRefDate');
	pgmGrid.cells2(0, 0).setValue(ONE);
}

function backtrack(id) {
	switch (id) {
	case 'preLeaseRefDate':
		setFocusLast('preLeaseRefDate');
		break;
	case 'preLeaseRefSerial':
		setFocusLast('preLeaseRefDate');
		break;
	case 'invSl':
		setFocusLast('preLeaseRefSerial');
		break;
	case 'invFor':
		if ($('#action').val() == RECTIFY) {
			setFocusLast('invSl');
		} else {
			setFocusLast('preLeaseRefSerial');
		}
		break;
	case 'invDate':
		setFocusLast('invFor');
		break;
	case 'invNum':
		setFocusLast('invDate');
		break;
	case 'invAmtFormat':
		setFocusLast('invNum');
		break;
	case 'paySl':
		setFocusLast('invAmtFormat');
		break;
	case 'pgmGrid2_add':
		setFocusLast('paySl');
		break;
	case 'assetClassifi':
		if ($('#invFor').val() == 'P')
			setFocusLast('paySl');
		else
			setFocusLast('invAmtFormat');

		break;
	case 'assetDescription':
		setFocusLast('assetClassifi');
		break;
	case 'quantity':
		setFocusLast('assetDescription');
		break;
	case 'assetModel':
		setFocusLast('quantity');
		break;
	case 'configuration':
		if ($('#assetModel').prop('readOnly'))
			setFocusLast('quantity');
		else
			setFocusLast('assetModel');
		break;
	case 'valueFormat':
		if (!$('#configuration').prop('readOnly'))
			setFocusLast('configuration');
		else if (!$('#assetModel').prop('readOnly'))
			setFocusLast('assetModel');
		else
			setFocusLast('quantity');
		break;
	// case 'cstFormat':
	// setFocusLast('valueFormat');
	// break;
	// case 'vatFormat':
	// setFocusLast('cstFormat');
	// break;
	// case 'stFormat':
	// setFocusLast('vatFormat');
	// break;
	// case 'tcsFormat':
	// setFocusLast('stFormat');
	// break;
	// case 'vatOnAmountFormat':
	// setFocusLast('tcsFormat');
	// break;
	case 'eleasepoinvasset_innerGrid_add':
		// if ($('#vatOnAmountFormat').prop('readOnly'))
		// setFocusLast('tcsFormat');
		// else
		setFocusLast('valueFormat');
		break;
	case 'remarks':
		// if ($('#vatOnAmountFormat').prop('readOnly'))
		// setFocusLast('tcsFormat');
		// else
		setFocusLast('assetClassifi');
		break;
	}
}

function doclearfields(id) {
	switch (id) {
	case 'preLeaseRefDate':
		if (isEmpty($('#preLeaseRefDate').val())) {
			$('#preLeaseRefSerial').val(EMPTY_STRING);
			$('#preLeaseRef_error').html(EMPTY_STRING);
			clearFields();
			break;
		}
	case 'preLeaseRefSerial':
		if (isEmpty($('#preLeaseRefSerial').val())) {
			$('#preLeaseRef_error').html(EMPTY_STRING);
			if ($('#action').val() == ADD) {
				$('#invSl').prop('readOnly', true);
			} else {
				$('#invSl').prop('readOnly', false);
			}
			clearPKFields();
			break;
		}
	case 'invSl':
		if (isEmpty($('#invSl').val())) {
			clearNonPKFields(true);
			break;
		}
	}
}

function validate(id) {
	switch (id) {
	case 'preLeaseRefDate':
		preLeaseRefDate_val();
		break;
	case 'preLeaseRefSerial':
		preLeaseRefSerial_val();
		break;
	case 'leaseCode':
		leaseCode_val(true);
		break;
	case 'agreeNo':
		agreeNo_val(true);
		break;
	case 'invSl':
		invSl_val();
		break;
	case 'invFor':
		invFor_val();
		break;
	case 'scheduleId':
		scheduleId_val(true);
		break;
	case 'invDate':
		invDate_val();
		break;
	case 'invNum':
		invNum_val();
		break;
	case 'invAmtFormat':
		invAmtFormat_val();
		break;
	case 'paySl':
		paySl_val();
		break;

	case 'assetClassifi':
		assetClassifi_val(true);
		break;
	case 'assetDescription':
		assetDescription_val(true);
		break;
	case 'quantity':
		quantity_val(true);
		break;
	case 'assetModel':
		assetModel_val(true);
		break;
	case 'configuration':
		configuration_val(true);
		break;
	case 'valueFormat':
		valueFormat_val(true);
		break;
	// case 'cstFormat':
	// cstFormat_val(true);
	// break;
	// case 'vatFormat':
	// vatFormat_val(true);
	// break;
	// case 'stFormat':
	// stFormat_val(true);
	// break;
	// case 'tcsFormat':
	// tcsFormat_val(true);
	// break;
	// case 'vatOnAmountFormat':
	// vatOnAmountFormat_val(true);
	// break;
	case 'remarks':
		remarks_val();
		break;
	}
}

function preLeaseRefDate_val() {
	var value = $('#preLeaseRefDate').val();
	clearError('preLeaseRef_error');
	if (isEmpty(value)) {
		setError('preLeaseRef_error', MANDATORY);
		return false;
	}
	if (!isDate(value)) {
		setError('preLeaseRef_error', INVALID_DATE);
		return false;
	}
	setFocusLast('preLeaseRefSerial');
	return true;
}

function preLeaseRefSerial_val() {
	var value = $('#preLeaseRefSerial').val();
	clearError('preLeaseRef_error');
	if (isEmpty(value)) {
		setError('preLeaseRef_error', MANDATORY);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('CONTRACT_DATE', $('#preLeaseRefDate').val());
		validator.setValue('CONTRACT_SL', $('#preLeaseRefSerial').val());
		validator.setValue(ACTION, $('#action').val());
		validator.setValue('PO_SL', $('#purchOrdSlDisplay').val());
		validator.setClass('patterns.config.web.forms.lss.eleasepoinvbean');
		validator.setMethod('validatePreLeaseContractRefSerial');
		validator.sendAndReceive();
		if (validator.getValue("SCHEDULE_ID") == EMPTY_STRING || validator.getValue("SCHEDULE_ID") == NULL_VALUE) {
			$('#agreeNo').val(EMPTY_STRING);
			$('#scheduleId').val(EMPTY_STRING);
			hide('scheduleDtl');
			$('#custId').val(validator.getValue("CUSTOMER_ID"));
			$('#custName').val(validator.getValue("CUSTOMER_NAME"));
			$('#leaseCode').val(validator.getValue("SUNDRY_DB_AC"));
		} else {
			show('scheduleDtl');
			$('#agreeNo').val(validator.getValue("AGREEMENT_NO"));
			$('#scheduleId').val(validator.getValue("SCHEDULE_ID"));
			$('#custId').val(validator.getValue("CUSTOMER_ID"));
			$('#custName').val(validator.getValue("CUSTOMER_NAME"));
			$('#leaseCode').val(validator.getValue("SUNDRY_DB_AC"));
		}
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('preLeaseRef_error', validator.getValue(ERROR));
			$('#custId').val(EMPTY_STRING);
			$('#custName').val(EMPTY_STRING);
			$('#leaseCode').val(EMPTY_STRING);
			$('#agreeNo').val(EMPTY_STRING);
			$('#scheduleId').val(EMPTY_STRING);
			pgmGrid.clearAll();
			return false;
		} else {
			var result = validator.getValue(RESULT_XML);
			leaseCode_val(false);
			agreeNo_val(false);
			pgmGrid.loadXMLString(result);

		}
	}
	setFocusLast('invFor');
	return true;
}
function doRowSelect(rowID) {
	pgmGrid.clearSelection();
	pgmGrid.selectRowById(rowID, true);
	pgmGrid.cells(rowID, 0).setValue(true);
	clearError('invFor_error');
	$('#invAmt_curr').val(pgmGrid.cells2(rowID, 4).getValue());
	$('#purchOrdSl').val(pgmGrid.cells2(rowID, 1).getValue());
	$('#purchOrdSlDisplay').val($('#purchOrdSl').val());
	invValue = parseInt(pgmGrid.cells2(rowID, 7).getValue());
	outStandingAmt = pgmGrid.cells2(rowID, 7).getValue();
	poDate = pgmGrid.cells2(rowID, 2).getValue();
	if (isZero(invValue)) {
		setError('purchOrdSl_error', INV_RAISED_ALREADY); //
		return false;
	}
	var checkedrows = rowID.split(',');
	if (checkedrows.length > 1) {
		alert(SELECT_ONE);
		return;
	}
}

function leaseCode_val(valmode) {
	var value = $('#leaseCode').val();
	clearError('leaseCode_error');
	if (!isEmpty(value)) {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('SUNDRY_DB_AC', $('#leaseCode').val());
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.lss.eleasepoinvbean');
		validator.setMethod('validateLeaseCode');
		validator.sendAndReceive();
		$('#custName').val(validator.getValue("CUSTOMER_NAME"));
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('leaseCode_error', validator.getValue(ERROR));
			$('#custId').val(EMPTY_STRING);
			$('#custName').val(EMPTY_STRING);
			$('#agreeNo').val(EMPTY_STRING);
			return false;
		}
	}
	return true;
}

function agreeNo_val(valmode) {
	var value = $('#agreeNo').val();
	clearError('agreeNo_error');
	if (!isEmpty(value)) {
		pgmGrid.clearAll();
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('LESSEE_CODE', $('#leaseCode').val());
		validator.setValue('AGREEMENT_NO', $('#agreeNo').val());
		// validator.setValue('SCHEDULE_ID', $('#scheduleId').val());
		validator.setValue('CUSTOMER_ID', $('#custId').val());
		validator.setValue(ACTION, $('#action').val());
		validator.setValue('PO_SL', $('#purchOrdSlDisplay').val());
		validator.setClass('patterns.config.web.forms.lss.eleasepoinvbean');
		validator.setMethod('validateAgreementNo');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('agreeNo_error', validator.getValue(ERROR));
			return false;
		}
	}
	return true;
}

function scheduleId_val(valmode) {
	var value = $('#scheduleId').val();
	clearError('scheduleId_error');
	pgmGrid.clearAll();
	if (!isEmpty(value)) {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('LESSEE_CODE', $('#leaseCode').val());
		validator.setValue('AGREEMENT_NO', $('#agreeNo').val());
		validator.setValue('SCHEDULE_ID', $('#scheduleId').val());
		validator.setValue(ACTION, $('#action').val());
		validator.setValue('PO_SL', $('#purchOrdSl').val());
		validator.setClass('patterns.config.web.forms.lss.eleasepoinvbean');
		validator.setMethod('validateScheduleID');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('scheduleId_error', validator.getValue(ERROR));
			pgmGrid.clearAll();
			return false;
		}
	}
	if (valmode) {
		setFocusLast('invDate');
	}
	return true;
}

function invSl_val() {
	var value = $('#invSl').val();
	clearError('invSl_error');
	if ($('#action').val() == RECTIFY) {
		var rowID = pgmGrid.getCheckedRows(0);
		if (rowID == EMPTY_STRING) {
			setError('agreeNo_error', SELECT_ATLEAST_ONE);
			errors++;
			return false;
		}
		if (isEmpty(value)) {
			setError('invSl_error', MANDATORY);
			return false;
		}
		if (!isNumeric(value)) {
			setError('invSl_error', INVALID_NUMBER);
			return false;
		}
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('ENTITY_CODE', getEntityCode());
		validator.setValue('CONTRACT_DATE', $('#preLeaseRefDate').val());
		validator.setValue('CONTRACT_SL', $('#preLeaseRefSerial').val());
		validator.setValue('PO_SL', $('#purchOrdSl').val());
		validator.setValue('INVOICE_SL', $('#invSl').val());
		validator.setValue('ACTION', $('#action').val());
		validator.setClass('patterns.config.web.forms.lss.eleasepoinvbean');
		validator.setMethod('validateInvSl');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('invSl_error', validator.getValue(ERROR));
			return false;
		} else {
			// clearNonPKFields(false);
			PK_VALUE = getEntityCode() + PK_SEPERATOR + $('#preLeaseRefDate').val() + PK_SEPERATOR + $('#preLeaseRefSerial').val() + PK_SEPERATOR + $('#purchOrdSl').val() + PK_SEPERATOR + $('#invSl').val(); // 
			loadPKValues(PK_VALUE, 'invSl_error');
		}
	}
	setFocusLast('invDate');
	return true;
}

function invFor_val() {
	var value = $('#invFor').val();
	clearError('invFor_error');
	var rowID = pgmGrid.getCheckedRows(0);
	if (pgmGrid.getRowsNum() > 0) {
		if (rowID == EMPTY_STRING) {
			setError('invFor_error', SELECT_ATLEAST_ONE_POGRID);
			return false;
		}
	}
	if (value == 'P') {
		$('#paydet').removeClass('hidden');
	} else if (value == 'N') {
		$('#paydet').addClass('hidden');
		pgmGrid2.clearAll();
		$('#paySl').val(EMPTY_STRING);
		$('#paySl_error').html(EMPTY_STRING);
		$('#paySl_desc').html(EMPTY_STRING);
		$('#totalVal').html(ZERO);
	}
	setFocusLast('invDate');
	return true;
}

function invDate_val() {
	var value = $('#invDate').val();
	clearError('invDate_error');
	if (isEmpty(value)) {
		$('#invDate').val(cbd);
		return true;
	}
	if (!isDate(value)) {
		setError('invDate_error', INVALID_DATE);
		return false;
	}
	if (isDateGreater(value, cbd)) {
		setError('invDate_error', DATE_LECBD);
		return false;
	}
	if (isDateLesser(value, poDate)) {
		setError('invDate_error', DATE_LESS_PO_DATE); //
		return false;
	}
	setFocusLast('invNum');
	return true;
}

function invNum_val() {
	var value = $('#invNum').val();
	clearError('invNum_error');
	if (isEmpty(value)) {
		setError('invNum_error', MANDATORY);
		return false;
	}
	if (!isValidDescription(value)) {
		setError('invNum_error', INVALID_DESCRIPTION);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('ENTITY_CODE', getEntityCode());
		validator.setValue('CONTRACT_DATE', $('#preLeaseRefDate').val());
		validator.setValue('CONTRACT_SL', $('#preLeaseRefSerial').val());
		validator.setValue('PURCH_ORDER_SL', $('#purchOrdSlDisplay').val());
		validator.setValue('INVOICE_SL', $('#invSl').val());
		validator.setValue('INVOICE_NUMBER', $('#invNum').val());
		validator.setClass('patterns.config.web.forms.lss.eleasepoinvbean');
		validator.setMethod('validateinvNumber');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('invNum_error', validator.getValue(ERROR));
			return false;
		}
	}
	setFocusLast('invAmtFormat');
	return true;
}

function invAmtFormat_val() {
	var value = unformatAmount($('#invAmtFormat').val());
	clearError('invAmt_error');
	var rowID = pgmGrid.getCheckedRows(0);
	if (rowID == EMPTY_STRING) {
		alert(SELECT_ATLEAST_ONE);
		errors++;
		return false;
	}
	if (isEmpty(value)) {
		setError('invAmt_error', MANDATORY);
		return false;
	}
	$('#invAmt').val(value);
	if (!isPositiveAmount(value)) {
		setError('invAmt_error', HMS_POSITIVE_AMOUNT);
		return false;
	}
	if (isZero(value)) {
		setError('invAmt_error', ZERO_AMOUNT);
		return false;
	}

	if (!isCurrencySmallAmount($('#invAmt_curr').val(), value)) {
		setError('invAmt_error', HMS_INVALID_SMALL_AMOUNT);
		return false;
	}
	outStandingAmt = pgmGrid.cells2(rowID, 7).getValue();
	if (isNumberGreater(value, outStandingAmt)) {
		setError('invAmt_error', INVOICE_AMOUNT_LEESER); //
		return false;
	}
	if ($('#invFor').val() == 'P') {
		setFocusLast('paySl');
	} else {
		setFocusLast('assetClassifi');
	}

	return true;
}

function paySl_val() {
	var value = $('#paySl').val();
	clearError('paySl_error');
	if ($('#invFor').val() == 'P') {
		if (isEmpty(value)) {
			setError('paySl_error', MANDATORY);
			return false;
		}
		if (!isNumeric(value)) {
			setError('paySl_error', NUMERIC_CHECK);
			return false;
		}
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('ENTITY_CODE', getEntityCode());
		validator.setValue('CONTRACT_DATE', $('#preLeaseRefDate').val());
		validator.setValue('CONTRACT_SL', $('#preLeaseRefSerial').val());
		validator.setValue('PAYMENT_SL', $('#paySl').val());
		validator.setValue('PURCH_SL', $('#purchOrdSl').val());
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.lss.eleasepoinvbean');
		validator.setMethod('validatePaymentSerial');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('paySl_error', validator.getValue(ERROR));
			return false;
		} else {
			dateOdPayment = validator.getValue("PAYMENT_DATE");
			paidAmt = validator.getValue("PAYMENT_AMOUNT_BC");
			paidAmtFormat = formatAmount(validator.getValue('PAYMENT_AMOUNT_BC').toString(), getBaseCurrency(), currencySubUnits);
		}

	}
	setFocusLast('pgmGrid2_add');
	return true;
}
function eleasepoinv_addGrid(editMode, editRowId) {
	var previousAmount = ZERO;
	if (isEmpty($('#totalValHidden').html()))
		$('#totalVal').html(ZERO);
	if (paySl_val(false)) {
		if (eleasepoinv_gridDuplicateCheck(editMode, editRowId)) {
			if (editMode)
				previousAmount = pgmGrid2.cells(editRowId, 4).getValue();
			var field_values = [ 'false', $('#paySl').val(), dateOdPayment, paidAmtFormat, paidAmt ];
			_grid_updateRow(pgmGrid2, field_values);
			$('#totalValHidden').val(parseFloat($('#totalValHidden').val()) + parseFloat(paidAmt));
			$('#totalVal').html(parseFloat($('#totalValHidden').val()) - parseFloat(previousAmount));
			$('#totalVal').html(formatAmount($('#totalValHidden').val().toString(), getBaseCurrency()));
			eleasepoinv_clearGridFields();
			$('#paySl').focus();
			return true;
		} else {
			setError('paySl_error', DUPLICATE_DATA);
			$('#paySl_desc').html(EMPTY_STRING);
			return false;
		}
	} else {
		return false;
	}
}

function eleasepoinv_editGrid(rowId) {
	$('#paySl').val(pgmGrid2.cells(rowId, 1).getValue());
	dateOdPayment = pgmGrid2.cells(rowId, 2).getValue();
	paidAmtFormat = pgmGrid2.cells(rowId, 3).getValue();
	paidAmt = pgmGrid2.cells(rowId, 4).getValue();
}
function eleasepoinv_gridDuplicateCheck(editMode, editRowId) {
	var currentValue = [ $('#paySl').val() ];
	return _grid_duplicate_check(pgmGrid2, currentValue, [ 1 ]);
}

function gridDeleteRow(id) {
	switch (id) {
	case 'paySl':
		deleteGridRecord(pgmGrid2);
		$('#xmlGrid2').val(pgmGrid2.serialize());
		gridAfterDeleteRow();
		break;
	}

}
function gridAfterDeleteRow() {
	var total = 0;
	for (var i = 0; i < pgmGrid2.getRowsNum(); i++) {
		total = total + parseFloat(pgmGrid2.cells2(i, 4).getValue());
	}
	var amt = formatAmount(total.toString(), getBaseCurrency());
	$('#totalValHidden').val(amt);
	$('#totalVal').html(amt);
	return true;
}

// asset grid

function assetClassifi_val(valMode) {
	var value = $('#assetClassifi').val();
	clearError('assetClassifi_error');
	if (isEmpty(value)) {
		setError('assetClassifi_error', MANDATORY);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('ASSET_TYPE_CODE', value);
	validator.setValue(ACTION, USAGE);
	validator.setClass('patterns.config.web.forms.lss.eleasepobean');
	validator.setMethod('validateAssetClassification');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('assetClassifi_error', validator.getValue(ERROR));
		$('#assetClassifi_desc').html(EMPTY_STRING);
		return false;
	} else {
		$('#assetClassifi_desc').html(validator.getValue("DESCRIPTION"));
		w_model_req = validator.getValue("MODEL_NO");
		w_conf_req = validator.getValue("CONFIG_DTLS");
		if (w_model_req == 'N') {
			$('#assetModel').val(EMPTY_STRING);
			$('#assetModel_error').html(EMPTY_STRING);
			$('#assetModel').prop('readOnly', true);
		} else
			$('#assetModel').prop('readOnly', false);
		if (w_conf_req == 'N') {
			$('#configuration').val(EMPTY_STRING);
			$('#configuration_error').html(EMPTY_STRING);
			$('#configuration').prop('readOnly', true);
		} else
			$('#configuration').prop('readOnly', false);
	}
	if (valMode)
		setFocusLast('assetDescription');
	return true;
}

function assetDescription_val(valMode) {
	var value = $('#assetDescription').val();
	clearError('assetDescription_error');
	if (isEmpty(value)) {
		setError('assetDescription_error', MANDATORY);
		return false;
	}
	if (!isValidRemarks(value)) {
		setError('assetDescription_error', INVALID_DESCRIPTION);
		return false;
	}
	if (valMode)
		setFocusLast('quantity');
	return true;
}

function quantity_val(valMode) {
	var value = $('#quantity').val();
	clearError('quantity_error');
	if (isEmpty(value)) {
		$('#quantity').val(ONE);
		value = $('#quantity').val();
	}
	if (isZero(value)) {
		setError('quantity_error', ZERO_CHECK);
		return false;
	}
	if (!isNumeric(value)) {
		setError('quantity_error', INVALID_NUMBER);
		return false;
	}
	if (isNegative(value)) {
		setError('quantity_error', POSITIVE_CHECK);
		return false;
	}
	if (isNumberGreater(value, '99999')) {
		setError('quantity_error', INVALID_NUMBER);
		return false;
	}
	if (valMode)
		if (!$('#assetModel').prop('readonly'))
			setFocusLast('assetModel');
		else if (!$('#configuration').prop('readonly'))
			setFocusLast('configuration');
		else
			setFocusLast('valueFormat');
	return true;
}

function assetModel_val(valMode) {
	var value = $('#assetModel').val();
	clearError('assetModel_error');
	if (w_model_req != 'N') {
		if (w_model_req == 'M') {
			if (isEmpty(value)) {
				setError('assetModel_error', MANDATORY);
				return false;
			}
		}
		if (!isEmpty(value)) {
			if (!isValidOtherInformation25(value)) {
				setError('assetModel_error', INVALID_DESCRIPTION);
				return false;
			}
		}
	}
	if (valMode)
		if (!$('#configuration').prop('readonly'))
			setFocusLast('configuration');
		else
			setFocusLast('valueFormat');
	return true;
}

function configuration_val(valMode) {
	var value = $('#configuration').val();
	clearError('configuration_error');
	if (w_conf_req != 'N') {
		if (w_conf_req == 'M') {
			if (isEmpty(value)) {
				setError('configuration_error', MANDATORY);
				return false;
			}
		}
		if (!isEmpty(value)) {
			if (!isValidOtherInformation25(value)) {
				setError('configuration_error', INVALID_DESCRIPTION);
				return false;
			}
		}
	}
	if (valMode)
		setFocusLast('valueFormat');
	return true;
}

function valueFormat_val(valMode) {
	var value = unformatAmount($('#valueFormat').val());
	clearError('value_error');
	if (isEmpty(value)) {
		setError('value_error', MANDATORY);
		return false;
	}
	$('#value').val(value);
	if (isZero(value)) {
		setError('value_error', ZERO_CHECK);
		return false;
	}
	if (isNegativeAmount(value)) {
		setError('value_error', POSITIVE_AMOUNT);
		return false;
	}
	if (!isCurrencySmallAmount($('#value_curr').val(), value)) {
		setError('value_error', INVALID_SMALL_AMOUNT);
		return false;
	}
	if (isNumberGreater(value, $('#invAmt').val())) {
		setError('value_error', INVOICE_AMOUNT_EQUAL);
		return false;
	}
	$('#valueFormat').val(formatAmount(value, getBaseCurrency()));
	if (valMode)
		setFocusLastOnGridSubmit('eleasepoinvasset_innerGrid');
	return true;
}
//
// function cstFormat_val(valMode) {
// var value = unformatAmount($('#cstFormat').val());
// clearError('cst_error');
// if (!isEmpty(value)) {
// $('#cst').val(value);
// if (isNegativeAmount(value)) {
// setError('cst_error', POSITIVE_AMOUNT);
// return false;
// }
// if (!isCurrencySmallAmount($('#cst_curr').val(), value)) {
// setError('cst_error', INVALID_SMALL_AMOUNT);
// return false;
// }
// $('#cstFormat').val(formatAmount(value, $('#cst_curr').val()));
// } else {
// $('#cst').val(ZERO);
// $('#cstFormat').val(formatAmount($('#cst').val(), $('#cst_curr').val()));
// }
// if (valMode)
// setFocusLast('vatFormat');
// return true;
// }
//
// function vatFormat_val(valMode) {
// var value = unformatAmount($('#vatFormat').val());
// clearError('vat_error');
// if (!isEmpty(value) && !isZero(value)) {
// $('#vatOnAmountFormat').prop('readonly', false);
// $('#vat').val(value);
// if (isNegativeAmount(value)) {
// setError('vat_error', POSITIVE_AMOUNT);
// return false;
// }
// if (!isCurrencySmallAmount($('#vat_curr').val(), value)) {
// setError('vat_error', INVALID_SMALL_AMOUNT);
// return false;
// }
// $('#vatFormat').val(formatAmount(value, $('#vat_curr').val()));
// } else {
// $('#vat').val(ZERO);
// $('#vatFormat').val(formatAmount($('#vat').val(), $('#vat_curr').val()));
// $('#vatOnAmount').val(ZERO);
// $('#vatOnAmountFormat').val(EMPTY_STRING);
// $('#vatOnAmountFormat_error').html(EMPTY_STRING);
// $('#vatOnAmountFormat').prop('readonly', true);
// }
// if (valMode)
// setFocusLast('stFormat');
// return true;
// }
//
// function stFormat_val(valMode) {
// var value = unformatAmount($('#stFormat').val());
// clearError('st_error');
// if (!isEmpty(value)) {
// $('#st').val(value);
// if (isNegativeAmount(value)) {
// setError('st_error', POSITIVE_AMOUNT);
// return false;
// }
// if (!isCurrencySmallAmount($('#st_curr').val(), value)) {
// setError('st_error', INVALID_SMALL_AMOUNT);
// return false;
// }
// $('#stFormat').val(formatAmount(value, $('#st_curr').val()));
// } else {
// $('#st').val(ZERO);
// $('#stFormat').val(formatAmount($('#st').val(), $('#st_curr').val()));
// }
// if (valMode)
// setFocusLast('tcsFormat');
// return true;
// }
//
// function tcsFormat_val(valMode) {
// var value = unformatAmount($('#tcsFormat').val());
// clearError('tcs_error');
// if (!isEmpty(value)) {
// $('#tcs').val(value);
// if (isNegativeAmount(value)) {
// setError('tcs_error', POSITIVE_AMOUNT);
// return false;
// }
// if (!isCurrencySmallAmount($('#tcs_curr').val(), value)) {
// setError('tcs_error', INVALID_SMALL_AMOUNT);
// return false;
// }
// $('#tcsFormat').val(formatAmount(value, $('#tcs_curr').val()));
// } else
// $('#tcs').val(ZERO);
// $('#tcsFormat').val(formatAmount($('#tcs').val(), $('#tcs_curr').val()));
// if (valMode) {
// if ($('#vatOnAmountFormat').prop('readOnly'))
// setFocusLastOnGridSubmit('eleasepo_innerGrid');
// else
// setFocusLast('vatOnAmountFormat');
// }
// return true;
// }
//
// function vatOnAmountFormat_val(valMode) {
// var value = unformatAmount($('#vatOnAmountFormat').val());
// clearError('vatOnAmount_error');
// if ($('#vat').val() > 0) {
// if (isEmpty(value)) {
// setError('vatOnAmount_error', MANDATORY);
// return false;
// }
// $('#vatOnAmount').val(value);
// if (isZero(value)) {
// setError('vatOnAmount_error', ZERO_CHECK);
// return false;
// }
// if (isNegativeAmount(value)) {
// setError('vatOnAmount_error', POSITIVE_AMOUNT);
// return false;
// }
// if (!isCurrencySmallAmount($('#vatOnAmount_curr').val(), value)) {
// setError('vatOnAmount_error', INVALID_SMALL_AMOUNT);
// return false;
// }
// $('#vatOnAmountFormat').val(formatAmount(value,
// $('#vatOnAmount_curr').val()));
// }
// var totalAmt = parseFloat($('#value').val()) + parseFloat($('#cst').val()) +
// parseFloat($('#vat').val()) + parseFloat($('#st').val()) +
// parseFloat($('#tcs').val());
// $('#totalFormat').val(formatAmount(totalAmt.toString(),
// $('#total_curr').val()));
// if (valMode)
// setFocusLastOnGridSubmit('eleasepo_innerGrid');
// return true;
// }

// asset grid end

function eleasepoinvasset_addGrid(editMode, editRowId) {
	var previousAmount = ZERO;
	// var previousTotAmount = ZERO;
	if (!validateGridFields()) {
		return false;
	}
	if (editMode) {
		previousAmount = eleasepoinvasset_innerGrid.cells(editRowId, 7).getValue();
		// previousTotAmount =
		// unformatAmount(eleasepo_innerGrid.cells(editRowId, 13).getValue());
	}
	// var linkValue = EMPTY_STRING;
	// if (editRowId == EMPTY_STRING || editRowId == null) {
	// linkValue = "<a href='javascript:void(0);' onclick=editAssetGrid('" +
	// eleasepo_innerGrid.getRowsNum() + "');>" + $('#assetClassifi').val() +
	// "</a>";
	// } else {
	// linkValue = "<a href='javascript:void(0);' onclick=editAssetGrid('" +
	// eleasepo_innerGrid.getRowIndex(editRowId) + "')>" +
	// $('#assetClassifi').val() + "</a>";
	// }
	// var field_values = [ 'false', linkValue, $('#assetClassifi').val(),
	// $('#assetDescription').val(), $('#quantity').val(),
	// $('#assetModel').val(), $('#configuration').val(),
	// $('#valueFormat').val(), $('#cstFormat').val(), $('#vatFormat').val(),
	// $('#stFormat').val(), $('#tcsFormat').val(),
	// $('#vatOnAmountFormat').val(), $('#totalFormat').val(),
	// $('#value').val(), $('#cst').val(), $('#vat').val(), $('#st').val(),
	// $('#tcs').val(), $('#vatOnAmount').val(), w_model_req, w_conf_req ];

	var field_values = [ 'false', $('#assetClassifi').val(), $('#assetDescription').val(), $('#quantity').val(), $('#assetModel').val(), $('#configuration').val(), $('#valueFormat').val(), $('#value').val(), w_model_req, w_conf_req ];
	_grid_updateRow(eleasepoinvasset_innerGrid, field_values);
	$('#totalValueAmount').val(parseFloat($('#totalValueAmount').val()) + parseFloat($('#value').val()));
	$('#totalValueAmount').val(parseFloat($('#totalValueAmount').val()) - parseFloat(previousAmount));
	$('#totalValue').html(formatAmount($('#totalValueAmount').val().toString(), getBaseCurrency()));

	// var amount = unformatAmount($('#totalFormat').val());
	// $('#totalAmount').val(parseFloat($('#totalAmount').val()) +
	// parseFloat(amount));
	// $('#totalAmount').val(parseFloat($('#totalAmount').val()) -
	// parseFloat(previousTotAmount));
	// $('#totAmount').html(formatAmount($('#totalAmount').val().toString(),
	// getBaseCurrency()));
	// $('#totalFormat').val(formatAmount($('#totalFormat').val().toString(),
	// $('#total_curr').val()));
	eleasepoinv_clearAssetGridFields();
	$('#assetClassifi').focus();
	return true;
}

// function editAssetGrid(rowId) {
// eleasepo_assetGrid.clearAll();
// var field_values = [ eleasepo_innerGrid.cells2(rowId, 3).getValue(),
// xmleleasepoinvassetGrid.cells2(rowId, 4).getValue(),
// xmleleasepoinvassetGrid.cells2(rowId, 5).getValue(),
// xmleleasepoinvassetGrid.cells2(rowId, 6).getValue() ];
// _grid_updateRow(xmleleasepoinvassetGrid, field_values);
// win = showWindow('view_assetDtl', EMPTY_STRING, LASEPO_ASSET_DTL,
// EMPTY_STRING, true, false, false, 750, 170);
// }
function eleasepoinvasset_editGrid(rowId) {
	eleasepoinv_clearAssetGridFields();
	$('#assetClassifi').val(eleasepoinvasset_innerGrid.cells(rowId, 1).getValue());
	$('#assetDescription').val(eleasepoinvasset_innerGrid.cells(rowId, 2).getValue());
	$('#quantity').val(eleasepoinvasset_innerGrid.cells(rowId, 3).getValue());
	$('#assetModel').val(eleasepoinvasset_innerGrid.cells(rowId, 4).getValue());
	$('#configuration').val(eleasepoinvasset_innerGrid.cells(rowId, 5).getValue());
	$('#valueFormat').val(eleasepoinvasset_innerGrid.cells(rowId, 6).getValue());
	$('#value').val(eleasepoinvasset_innerGrid.cells(rowId, 7).getValue());
	w_model_req = eleasepoinvasset_innerGrid.cells(rowId, 8).getValue();
	w_conf_req = eleasepoinvasset_innerGrid.cells(rowId, 9).getValue();

	// $('#cstFormat').val(eleasepoinvasset_innerGrid.cells(rowId,
	// 8).getValue());
	// $('#vatFormat').val(eleasepoinvasset_innerGrid.cells(rowId,
	// 9).getValue());
	// $('#stFormat').val(eleasepoinvasset_innerGrid.cells(rowId,
	// 10).getValue());
	// $('#tcsFormat').val(eleasepoinvasset_innerGrid.cells(rowId,
	// 11).getValue());
	// $('#vatOnAmountFormat').val(eleasepoinvasset_innerGrid.cells(rowId,
	// 12).getValue());
	// $('#totalFormat').val(eleasepoinvasset_innerGrid.cells(rowId,
	// 13).getValue());
	$('#assetClassifi').focus();
}

function eleasepoinvasset_cancelGrid(rowId) {
	eleasepoinv_clearAssetGridFields();
}

function gridExit(id) {
	switch (id) {
	case 'paySl':
		if (pgmGrid2.getRowsNum() > 0) {
			if (!isNumberEqual($('#totalValHidden').val(), $('#invAmt').val())) {
				setError('paySl_error', PAYMENT_AMOUNT_INVOICE_AMOUNT_EQUAL);
				return false;
			}
			$('#xmlGrid2').val(pgmGrid2.serialize());
			eleasepoinv_clearGridFields();
			setFocusLast('assetClassifi');
		} else if ($('#invFor').val() == 'P') {
			$('#xmlGrid2').val(pgmGrid2.serialize());
			setError('paySl_error', ADD_ATLEAST_ONE_ROW);
			setFocusLast('paySl');
		}
		break;

	case 'assetClassifi':
	case 'assetDescription':
	case 'quantity':
	case 'assetModel':
	case 'configuration':
	case 'valueFormat':
		// case 'cstFormat':
		// case 'vatFormat':
		// case 'stFormat':
		// case 'tcsFormat':
		// case 'vatOnAmountFormat':
		if (eleasepoinvasset_innerGrid.getRowsNum() > 0) {
			if (!isNumberEqual($('#totalValueAmount').val(), $('#invAmt').val())) {
				setError('assetClassifi_error', VALUE_AMOUNT_EQUAL);
				return false;
			}
			$('#xmleleasepoinvassetGrid').val(eleasepoinvasset_innerGrid.serialize());
			eleasepoinv_clearAssetGridFields();
			setFocusLast('remarks');
		} else {
			$('#xmleleasepoinvassetGrid').val(eleasepoinvasset_innerGrid.serialize());
			setError('assetClassifi_error', ADD_ATLEAST_ONE_ROW);
			setFocusLast('assetClassifi');
		}
		break;

	}
}

function gridDeleteRow(id) {
	switch (id) {
	case 'assetClassifi':
		deleteGridRecord(eleasepoinvasset_innerGrid);
		$('#xmleleasepoinvassetGrid').val(eleasepoinvasset_innerGrid.serialize());
		gridAfterDeleteAssetRow();
		break;
	}

}
function gridAfterDeleteAssetRow() {
	var total = 0;
	// var totAmount = 0;
	for (var i = 0; i < eleasepoinvasset_innerGrid.getRowsNum(); i++) {
		total = total + parseFloat(eleasepoinvasset_innerGrid.cells2(i, 7).getValue());
		// totAmount = totAmount +
		// parseFloat(unformatAmount(eleasepoinvasset_innerGrid.cells2(i,
		// 12).getValue()));
	}
	var amt = formatAmount(total.toString(), getBaseCurrency());
	// var totAmt = formatAmount(totAmount.toString(), getBaseCurrency());
	$('#totalValue').html(amt);
	$('#totalValueAmount').val(amt);
	// $('#totAmount').html(totAmt);
	return true;
}

function validateGridFields() {
	var errors = 0;
	if (!assetClassifi_val(false)) {
		errors++;
	}
	if (!assetDescription_val(false)) {
		errors++;
	}
	if (!quantity_val(false)) {
		errors++;
	}
	if (!assetModel_val(false)) {
		errors++;
	}
	if (!configuration_val(false)) {
		errors++;
	}
	if (!valueFormat_val(false)) {
		errors++;
	}
	// if (!cstFormat_val(false)) {
	// errors++;
	// }
	// if (!vatFormat_val(false)) {
	// errors++;
	// }
	// if (!stFormat_val(false)) {
	// errors++;
	// }
	// if (!tcsFormat_val(false)) {
	// errors++;
	// }
	// if (!vatOnAmountFormat_val(false)) {
	// errors++;
	// }
	if (errors > 0)
		return false;
	else
		return true;
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (isEmpty(value)) {
		setError('remarks_error', MANDATORY);
		setFocusLast('remarks');
		return false;
	}
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', HMS_INVALID_REMARKS);
			setFocusLast('remarks');
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', HMS_INVALID_REMARKS);
			setFocusLast('remarks');
			return false;
		}
	}
	if (valMode) {
		setFocusOnSubmit();
	}
	return true;
}

function revalidate() {
	revaildateGrid();
}

function revaildateGrid() {
	$('#invAmt').val(unformatAmount($('#invAmtFormat').val()));
	var rowID = pgmGrid.getCheckedRows(0);
	if (rowID == EMPTY_STRING) {
		setError('invFor_error', ADD_ATLEAST_ONE_ROW);
		errors++;
		return false;
	} else {
		$('#scheduleIDHidden').val($('#scheduleId').val());
		$('#purchOrdSl').val(pgmGrid.cells2(rowID, 1).getValue());
		$('#purchOrdSlDisplay').val($('#purchOrdSl').val());
		invValue = parseInt(pgmGrid.cells2(rowID, 6).getValue());
		outStandingAmt = pgmGrid.cells2(rowID, 7).getValue();
		if (isZero(invValue)) {
			setError('purchOrdSl_error', INV_RAISED_ALREADY); //
			return false;
		}
		poDate = pgmGrid.cells2(rowID, 2).getValue();
		if (isNumberGreater(unformatAmount($('#invAmtFormat').val()), outStandingAmt)) {
			setError('invAmt_error', INV_RAISED_ALREADY); //
			errors++;
			return false;
		}
		var checkedrows = rowID.split(',');
		if (checkedrows.length > 1) {
			setError('agreeNo_error', SELECT_ONE); //
			errors++;
			return false;
		} else {
			$('#xmlGrid').val(pgmGrid.serialize());
			if ($('#invFor').val() == 'P') {
				if (pgmGrid2.getRowsNum() > 0) {
					$('#xmlGrid2').val(pgmGrid2.serialize());
					if (!isNumberEqual($('#totalValHidden').val(), $('#invAmt').val())) {
						setError('paySl_error', PAYMENT_AMOUNT_INVOICE_AMOUNT_EQUAL);
						errors++;
						return false;
					}
				} else {
					pgmGrid2.clearAll();
					$('#xmlGrid2').val(pgmGrid2.serialize());
					setError('paySl_error', ADD_ATLEAST_ONE_ROW);
					errors++;
					return false;
				}
			}
		}
	}
	if (eleasepoinvasset_innerGrid.getRowsNum() > 0) {
		$('#xmleleasepoinvassetGrid').val(eleasepoinvasset_innerGrid.serialize());
		if (!isNumberEqual($('#totalValueAmount').val(), $('#invAmt').val())) {
			setError('assetClassifi_error', VALUE_AMOUNT_EQUAL);
			errors++;
			return false;
		}
	} else {
		eleasepoinvasset_innerGrid.clearAll();
		$('#xmleleasepoinvassetGrid').val(eleasepoinvasset_innerGrid.serialize());
		setError('assetClassifi_error', ADD_ATLEAST_ONE_ROW);
		errors++;
	}
}
