<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<%@taglib prefix="resource" tagdir="/WEB-INF/tags/resource"%>
<%@taglib prefix="message" tagdir="/WEB-INF/tags/message"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<web:bundle baseName="patterns.config.web.forms.lss.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/lss/eleasepoinvupld.js" />
	</web:dependencies>
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="eleasepoinvupld.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/lss/eleasepoinvupld" id="eleasepoinvupld" method="POST">
				<web:dividerBlock>
					<type:optionBox add="true" modify="false" view="true" />
				</web:dividerBlock>
				<web:viewContent id="po_view1">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="eleasepoinvupld.uploaddatesl" var="program" />
										</web:column>
										<web:column>
											<type:dateDaySL property="upload" id="upload" readOnly="true" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="eleasepoinvupld.preleasecontref" var="program" />
										</web:column>
										<web:column>
											<type:dateDaySL property="preLeaseContRef" id="preLeaseContRef" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle width="210px" />
										<web:columnStyle width="120px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="eleasepoinvupld.lessecode" var="program" />
										</web:column>
										<web:column>
											<type:lesseeCodeDisplay property="lesseCode" id="lesseCode" />
										</web:column>
										<web:column>
											<web:legend key="eleasepoinvupld.customerid" var="program" />
										</web:column>
										<web:column>
											<type:customerIDDisplay property="customerID" id="customerID" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="eleasepoinvupld.custname" var="program" />
										</web:column>
										<web:column>
											<type:nameDisplay property="customerName" id="customerName" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="eleasepoinvupld.poserial" var="program" />
										</web:column>
										<web:column>
											<type:POSerial property="poSerial" id="poSerial" lookup="true" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="eleasepoinvupld.uploadfile" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<resource:fileUpload id="vault" width="400px" height="100px" initFileUpload="initFileUpload" beforeFileAdd="beforeFileAdd" afterFileUpload="afterFileUpload" afterFileUploadFail="afterFileUploadFail" filelimit="1" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="eleasepoinvupld.filename" var="program" />
										</web:column>
										<web:column>
											<type:fileName id="fileName" property="fileName" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="eleasepoinvupld.fileextensionlist" var="program" />
										</web:column>
										<web:column>
											<type:fileExtensionList id="fileExtensionList" property="fileExtensionList" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="eleasepoinvupld.fileinventorynumber" var="program" />
										</web:column>
										<web:column>
											<type:fileInventory id="fileInventoryNumber" property="fileInventoryNumber" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:button key="eleasepoinvupld.verifyuploadfile" id="uploadFile" var="program" onclick="verifyUploadFile();" />
										</web:column>
										<web:column>
											<span id="uploadSuccess" class="fa fa-check-circle-o" style="font-size: 17px; color: green"> </span>
											<span id="uploadFail" class="fa fa-exclamation-triangle" style="font-size: 17px; color: red"> </span>
											<span id="fileUploadError" style="font-size: 17px; color: red"><html:errors property="fileUploadError" /></span>
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="form.remarks" var="common" />
										</web:column>
										<web:column>
											<type:remarks property="remarks" id="remarks" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:viewContent id="view_dtl" styleClass="hidden">
								<web:section>
									<message:messageBox id="eleasepoinvupld" />
								</web:section>
								<web:section>
									<web:table>
										<web:rowOdd>
											<web:column>
												<web:grid height="300px" width="1500px" id="eleasepoinvupld_innerGrid" src="lss/eleasepoinvupld_innerGrid.xml">
												</web:grid>
											</web:column>
										</web:rowOdd>
									</web:table>
								</web:section>
							</web:viewContent>
							<web:section>
								<web:table>
									<web:rowOdd>
										<web:column span="2">
											<web:submitReset />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
						</web:sectionBlock>
					</web:dividerBlock>
				</web:viewContent>
				<web:element property="tempSerial" />
				<web:element property="command" />
				<web:element property="action" />
				<web:element property="fileExt" />
				<web:element property="filePath" />
				<web:element property="redirectLandingPagePath" />
				<web:element property="fileExtensionListError" />
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
	<web:queryGrid />
	<web:TBAGrid />
	<web:contextSearch />
</web:fragment>
