var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'ILEASEINTPARAM';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}
function clearFields() {
	$('#productCode').val(EMPTY_STRING);
	$('#productCode_desc').html(EMPTY_STRING);
	$('#portfolioCode').val(EMPTY_STRING);
	$('#portfolioCode_desc').html(EMPTY_STRING);
	$('#effectiveDate').val(EMPTY_STRING);
	$('#remittanceAccNumber').val(EMPTY_STRING);
	$('#remittanceIFSC').val(EMPTY_STRING);
	$('#bankName').val(EMPTY_STRING);
	$('#branchName').val(EMPTY_STRING);
	$('#branchCity').val(EMPTY_STRING);
	$('#stateCode').val(EMPTY_STRING);
	$('#stateCode_desc').html(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
}

function loadData() {
	$('#productCode').val(validator.getValue('LEASE_PRODUCT_CODE'));
	$('#productCode_desc').html(validator.getValue('F1_DESCRIPTION'));
	$('#portfolioCode').val(validator.getValue('PORTFOLIO_CODE'));
	$('#portfolioCode_desc').html(validator.getValue('F2_DESCRIPTION'));
	$('#effectiveDate').val(validator.getValue('EFF_DATE'));
	$('#remittanceAccNumber').val(validator.getValue('REMIT_TO_ACNO'));
	$('#remittanceIFSC').val(validator.getValue('IFSC_CODE'));
	$('#bankName').val(validator.getValue('F3_BANK_NAME'));
	$('#branchName').val(validator.getValue('F3_BRANCH_NAME'));
	$('#branchCity').val(validator.getValue('F3_CITY'));
	$('#stateCode').val(validator.getValue('F3_STATE_CODE'));
	$('#stateCode_desc').html(validator.getValue('F4_DESCRIPTION'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadAuditFields(validator);
}
