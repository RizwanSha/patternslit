var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'ELEASECLOSURE';

function init() {
	refreshTBAGrid();
	refreshQueryGrid();
	if (_redisplay) {
		$('#entryDate').val(getCBD());
		lesseCode_val();
		setCheckbox('verifiedLeaseDetails', NO);
		show('hierarchy_view');
	}
}

function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'ELEASECLOSURE_MAIN', queryGrid);
}

function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}
function doclearfields(id) {
	switch (id) {
	case 'entryDate':
		if (isEmpty($('#entryDate').val())) {
			$('#entryDate').val(EMPTY_STRING);
			$('#entryDate_error').html(EMPTY_STRING);
			$('#entryDate_desc').html(EMPTY_STRING);
			clearNonPKFields();
			break;
		}
	case 'entrySerial':
		if (isEmpty($('#entrySerial').val())) {
			$('#entrySerial').val(EMPTY_STRING);
			$('#entrySerial_error').html(EMPTY_STRING);
			$('#entrySerial_desc').html(EMPTY_STRING);
			hide('hierarchy_view');
			break;
		}
	case 'lesseCode':
		if (isEmpty($('#lesseCode').val())) {
			$('#customerID').val(EMPTY_STRING);
			$('#customerName').val(EMPTY_STRING);
			clearNonPKFields();
			break;
		}
	}
}
function clearFields() {
	$('#entryDate').val(getCBD());
	$('#entrySerial').val(EMPTY_STRING);
	$('#entrySerial_error').html(EMPTY_STRING);
	$('#entrySerial_desc').html(EMPTY_STRING);
	clearNonPKFields();
}
function clearNonPKFields() {
	$('#lesseCode').val(EMPTY_STRING);
	$('#lesseCode_error').html(EMPTY_STRING);
	$('#entrySerial').val(EMPTY_STRING);
	$('#customerID').val(EMPTY_STRING);
	$('#customerName').val(EMPTY_STRING);
	$('#agreementNumber').val(EMPTY_STRING);
	$('#agreementNumber_error').html(EMPTY_STRING);
	$('#agreementNumber_desc').html(EMPTY_STRING);
	$('#scheduleID').val(EMPTY_STRING);
	$('#scheduleID_error').html(EMPTY_STRING);
	$('#scheduleID_desc').html(EMPTY_STRING);
	setCheckbox('verifiedLeaseDetails', NO);
	$('#verifiedLeaseDetails_error').html(EMPTY_STRING);
	$('#referenceNumber').val(EMPTY_STRING);
	$('#referenceNumber_error').html(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
	hide('hierarchy_view');
}

function loadData() {
	$('#entryDate').val(validator.getValue('ENTRY_DATE'));
	$('#entrySerial').val(validator.getValue('ENTRY_SL'));
	$('#lesseCode').val(validator.getValue('LESSEE_CODE'));
	$('#customerID').val(validator.getValue('F1_CUSTOMER_ID'));
	$('#customerName').val(validator.getValue('F2_CUSTOMER_NAME'));
	$('#agreementNumber').val(validator.getValue('AGREEMENT_NO'));
	$('#scheduleID').val(validator.getValue('SCHEDULE_ID'));
	show('hierarchy_view');
	setCheckbox('verifiedLeaseDetails', YES);
	$('#referenceNumber').val(validator.getValue('REFERENCE_NO'));
	$('#remarks').val(validator.getValue('REMARKS'));
	if (!isEmpty($('#scheduleID').val())) {
		show('hierarchy_view');
	}
	$('#lesseCode').focus();
	resetLoading();
}

function view(source, primaryKey) {
	hideParent('lss/qleaseclosure', source, primaryKey, 1000);
	resetLoading();
}

function doHelp(id) {
	switch (id) {
	case 'lesseCode':
		help('COMMON', 'HLP_DEBTOR_CODE', $('#lesseCode').val(), EMPTY_STRING, $('#lesseCode'), function() {
		});
		break;
	case 'agreementNumber':
		help('COMMON', 'HLP_CUST_AGGREEMENT', $('#agreementNumber').val(), $('#customerID').val(), $('#agreementNumber'), null, function(gridObj, rowID) {
			$('#agreementNumber').val(gridObj.cells(rowID, 1).getValue());
		});
		break;

	case 'scheduleID':
		help('COMMON', 'HLP_LEASE_SCH_ID', $('#scheduleID').val(), $('#lesseCode').val() + PK_SEPERATOR + $('#agreementNumber').val(), $('#scheduleID'), function() {
		});
		break;
	}
}
function qLease() {
	var pkValue = getEntityCode() + PK_SEPERATOR + $('#lesseCode').val() + PK_SEPERATOR + $('#agreementNumber').val() + PK_SEPERATOR + $('#scheduleID').val();
	window.scroll(0, 0);
	showWindow('QLease', getBasePath() + 'Renderer/lss/qlease.jsp',QUERY_LEASE_DETAILS, PK + '=' + pkValue + "&" + SOURCE + "=" + MAIN, true, false, false, 1042, 550);
	resetLoading();
}
function add() {
	setCheckbox('verifiedLeaseDetails', NO);
	$('#lesseCode').focus();
	$('#entryDate').val(getCBD());
}



function modify() {

}

function validate(id) {
	switch (id) {
	case 'entryDate':
		entryDate_val();
		break;
	case 'entrySerial':
		entrySerial_val();
		break;
	case 'lesseCode':
		lesseCode_val();
		break;
	case 'agreementNumber':
		agreementNumber_val();
		break;
	case 'scheduleID':
		scheduleID_val();
		break;
	case 'verifiedLeaseDetails':
		verifiedLeaseDetails_val();
		break;
	case 'referenceNumber':
		referenceNumber_val();
		break;
	case 'remarks':
		remarks_val();
		break;
	}
}

function backtrack(id) {
	switch (id) {
		case 'lesseCode':
		setFocusLast('lesseCode');
		break;
	case 'agreementNumber':
		setFocusLast('lesseCode');
		break;
	case 'scheduleID':
		setFocusLast('agreementNumber');
		break;
	case 'verifiedLeaseDetails':
		setFocusLast('scheduleID');
		break;
	case 'referenceNumber':
		setFocusLast('verifiedLeaseDetails');
		break;
	case 'remarks':
		setFocusLast('referenceNumber');
		break;

	}
}
function entryDate_val() {
	var value = $('#entryDate').val();
	clearError('entryDate_error');
	if (isEmpty(value)) {
		$('#entryDate').val(getCBD());
		value = $('#entryDate').val();
	}
	if (!isDate(value)) {
		setError('entryDate_error', INVALID_DATE);
		return false;
	}
	setFocusLast('entrySerial');
	return true;
}
function entrySerial_val(valMode) {
	var value = $('#entrySerial').val();
	clearError('entrySerial_error');
	if (isEmpty(value)) {
		setError('entrySerial_error', MANDATORY);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('ENTRY_DATE', $('#entryDate').val());
		validator.setValue('ENTRY_SL', value);
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.lss.eleaseclosurebean');
		validator.setMethod('validateEntrySerial');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('entrySerial_error', validator.getValue(ERROR));
			return false;
		}
	}
	setFocusLast('lesseCode');
	return true;
}
function lesseCode_val() {
	var value = $('#lesseCode').val();
	clearError('lesseCode_error');
	if (isEmpty(value)) {
		setError('lesseCode_error', MANDATORY);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('LESSEE_CODE', value);
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.lss.eleaseclosurebean');
		validator.setMethod('validateLesseCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('lesseCode_error', validator.getValue(ERROR));
			$('#customerID').val(EMPTY_STRING);
			$('#customerName').val(EMPTY_STRING);
			return false;
		}
		$('#customerID').val(validator.getValue('CUSTOMER_ID'));
		$('#customerName').val(validator.getValue('CUSTOMER_NAME'));
		hide('hierarchy_view');
	}
	setFocusLast('agreementNumber');
	return true;
}
function agreementNumber_val() {
	var value = $('#agreementNumber').val();
	clearError('agreementNumber_error');
	if (isEmpty(value)) {
		setError('agreementNumber_error', MANDATORY);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('ENTITY_CODE', getEntityCode());
		validator.setValue('CUSTOMER_ID', $('#customerID').val());
		validator.setValue('AGREEMENT_NO', value);
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.lss.eleaseclosurebean');
		validator.setMethod('validateagreementNumber');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('agreementNumber_error', validator.getValue(ERROR));
			return false;
		}
	}
	setFocusLast('scheduleID');
	return true;
}
function scheduleID_val() {
	var value = $('#scheduleID').val();
	clearError('scheduleID_error');
	if (isEmpty(value)) {
		setError('scheduleID_error', MANDATORY);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('ENTRY_DATE_PK', $('#entryDate').val());
		validator.setValue('ENTRY_SL_PK', $('#entrySerial').val());
		validator.setValue('LESSEE_CODE', $('#lesseCode').val());
		validator.setValue('AGREEMENT_NO', $('#agreementNumber').val());
		validator.setValue('SCHEDULE_ID', value);
		validator.setValue(ACTION, $('#action').val());
		validator.setClass('patterns.config.web.forms.lss.eleaseclosurebean');
		validator.setMethod('validateScheduleID');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('scheduleID_error', validator.getValue(ERROR));
			return false;
		}
		show('hierarchy_view');
	}
	setFocusLast('verifiedLeaseDetails');
	return true;
}

function verifiedLeaseDetails_val() {
	clearError('verifiedLease_error');
	clearError('verifiedLeaseDetails_error');
	if ($('#verifiedLeaseDetails').is(':checked')) {
		$('#referenceNumber_error').html(EMPTY_STRING);
		$('#referenceNumber').val(EMPTY_STRING);
		$('#remarks_error').html(EMPTY_STRING);
		$('#remarks').val(EMPTY_STRING);
		$('#referenceNumber').attr('readOnly', false);
		$('#remarks').attr('readOnly', false);
		setFocusLast('referenceNumber');
	} else {
		$('#referenceNumber_error').html(EMPTY_STRING);
		$('#referenceNumber').val(EMPTY_STRING);
		$('#referenceNumber').attr('readOnly', true);
		$('#remarks_error').html(EMPTY_STRING);
		$('#remarks').val(EMPTY_STRING);
		$('#remarks').attr('readOnly', true);
		setFocusOnSubmit();
	}
	return true;
}
function referenceNumber_val() {
	var value = $('#referenceNumber').val();
	clearError('referenceNumber_error');
	if (!isEmpty(value)) {
		if (!isValidDescription(value, 50)) {
			setError('refNumber_error', INVALID_REF_NUMBER);
			return false;
		}
	}
	setFocusLast('remarks');
	return true;
}
function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', INVALID_REMARKS);
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', INVALID_REMARKS);
			return false;
		}
	}
	if (isEmpty(value)) {
		setError('remarks_error', MANDATORY);
		return false;
	}
	setFocusOnSubmit();
	return true;
}
