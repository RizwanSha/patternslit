var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'ELEASEINT';
function init() {
	refreshTBAGrid();
	refreshQueryGrid();
	if (_redisplay) {
		if ($('#action').val() == ADD && $('#rectify').val() == COLUMN_DISABLE) {
			$('#entrySerial').prop('readonly', true);
			$('#entrySerial_pic').prop('disabled', true);
		}
		$('#interestRate').prop('readonly', true);
		lesseCode_val(false);
		scheduleID_val(false);
	}
}

function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'ELEASEINT_MAIN');
}

function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}

function add() {
	$('#lesseCode').focus();
	$('#entrySerial').prop('readonly', true);
	$('#entrySerial_pic').prop('disabled', true);
	$('#dateOfEntry').val(getCBD());
}

function modify() {
}

function doHelp(id) {
	switch (id) {
	case 'lesseCode':
		help('COMMON', 'HLP_DEBTOR_CODE', $('#lesseCode').val(), EMPTY_STRING, $('#lesseCode'), function() {

		});
		break;
	case 'aggrementNumber':
		help('COMMON', 'HLP_LEASE_AGRMNT', $('#aggrementNumber').val(), $('#customerID').val(), $('#aggrementNumber'), function() {
		});
		break;
	case 'scheduleID':
		if (isEmpty($('#lesseCode').val()))
			setError('lesseCode_error', MANDATORY);
		else if (isEmpty($('#aggrementNumber').val()))
			setError('aggrementNumber_error', MANDATORY);
		else
			help('COMMON', 'HLP_LEASE_SCH_ID', $('#scheduleID').val(), $('#lesseCode').val() + PK_SEPERATOR + $('#aggrementNumber').val(), $('#scheduleID'), function() {
			});
		break;
	case 'entrySerial':
		if (isEmpty($('#lesseCode').val()))
			setError('lesseCode_error', MANDATORY);
		else if (isEmpty($('#aggrementNumber').val()))
			setError('aggrementNumber_error', MANDATORY);
		else if (isEmpty($('#scheduleID').val()))
			setError('scheduleID_error', MANDATORY);
		else
			help('COMMON', 'HLP_LEASE_INT_SL', $('#entrySerial').val(), $('#lesseCode').val() + PK_SEPERATOR + $('#aggrementNumber').val() + PK_SEPERATOR + $('#scheduleID').val(), $('#entrySerial'), function() {
			});
		break;
	}
}

function view(source, primaryKey) {
	hideParent('lss/qleaseint', source, primaryKey, 1200);
	resetLoading();
}

function doclearfields(id) {
	switch (id) {
	case 'lesseCode':
		if (isEmpty($('#lesseCode').val())) {
			clearFields();
			break;
		}
	case 'aggrementNumber':
		if (isEmpty($('#aggrementNumber').val())) {
			$('#aggrementNumber_desc').html(EMPTY_STRING);
			$('#aggrementNumber_error').html(EMPTY_STRING);
			$('#scheduleID').val(EMPTY_STRING);
			$('#scheduleID_desc').html(EMPTY_STRING);
			$('#scheduleID_error').html(EMPTY_STRING);
			$('#entrySerial').val(EMPTY_STRING);
			$('#entrySerial_error').html(EMPTY_STRING);
			clearNonPKFields();
			break;
		}
	case 'scheduleID':
		if (isEmpty($('#scheduleID').val())) {
			$('#scheduleID_desc').html(EMPTY_STRING);
			$('#scheduleID_error').html(EMPTY_STRING);
			$('#entrySerial').val(EMPTY_STRING);
			$('#entrySerial_error').html(EMPTY_STRING);
			clearNonPKFields();
			break;
		}
	case 'entrySerial':
		if (isEmpty($('#entrySerial').val())) {
			$('#entrySerial_error').html(EMPTY_STRING);
			clearNonPKFields();
			break;
		}
	case 'interestOnAmountFormat':
		if (isEmpty($('#interestOnAmountFormat').val())) {
			$('#amountToBeBilled').val(EMPTY_STRING);
			$('#amountToBeBilledFormat').val(EMPTY_STRING);
			break;
		}
	case 'fromDate':
		if (isEmpty($('#fromDate').val())) {
			$('#noOfDays').val(EMPTY_STRING);
			break;
		}
	case 'uptoDate':
		if (isEmpty($('#uptoDate').val())) {
			$('#noOfDays').val(EMPTY_STRING);
			break;
		}
	}
}

function clearFields() {
	$('#lesseCode').val(EMPTY_STRING);
	$('#lesseCode_desc').html(EMPTY_STRING);
	$('#lesseCode_error').html(EMPTY_STRING);
	$('#customerID').val(EMPTY_STRING);
	$('#customerID_desc').html(EMPTY_STRING);
	$('#customerID_error').html(EMPTY_STRING);
	$('#customerName').val(EMPTY_STRING);
	$('#aggrementNumber').val(EMPTY_STRING);
	$('#aggrementNumber_desc').html(EMPTY_STRING);
	$('#aggrementNumber_error').html(EMPTY_STRING);
	$('#scheduleID').val(EMPTY_STRING);
	$('#scheduleID_desc').html(EMPTY_STRING);
	$('#scheduleID_error').html(EMPTY_STRING);
	$('#entrySerial').val(EMPTY_STRING);
	$('#entrySerial_error').html(EMPTY_STRING);
	clearNonPKFields();
}

function clearScheduleIDDependency(){
	$('#interestRate').val(EMPTY_STRING);
	$('#interestRate').prop('readonly', true);
	$('#interestOnAmount').val(EMPTY_STRING);
	$('#interestOnAmountFormat').val(EMPTY_STRING);
	$('#interestOnAmount_curr').val(EMPTY_STRING);
	$('#interestOnAmount_error').html(EMPTY_STRING);
	$('#amountToBeBilled').val(EMPTY_STRING);
	$('#amountToBeBilledFormat').val(EMPTY_STRING);
	$('#amountToBeBilled_curr').val(EMPTY_STRING);
	$('#amountToBeBilled_error').html(EMPTY_STRING);
}

function clearNonPKFields() {
	$('#dateOfEntry').val(getCBD());
	$('#fromDate').val(EMPTY_STRING);
	$('#fromDate_error').html(EMPTY_STRING);
	$('#uptoDate').val(EMPTY_STRING);
	$('#uptoDate_error').html(EMPTY_STRING);
	$('#noOfDays').val(EMPTY_STRING);
	clearScheduleIDDependency();
	$('#dateOfInvoicingBilling').val(EMPTY_STRING);
	$('#dateOfInvoicingBilling_error').html(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
}

function loadData() {
	$('#entrySerial').prop('readonly', false);
	$('#entrySerial_pic').prop('disabled', false);
	$('#lesseCode').val(validator.getValue('LESSEE_CODE'));
	$('#customerID').val(validator.getValue('F2_CUSTOMER_ID'));
	$('#customerName').val(validator.getValue('F2_CUSTOMER_NAME'));
	$('#aggrementNumber').val(validator.getValue('AGREEMENT_NO'));
	$('#scheduleID').val(validator.getValue('SCHEDULE_ID'));
	$('#entrySerial').val(validator.getValue('ENTRY_SL'));
	$('#dateOfEntry').val(validator.getValue('DATE_OF_ENTRY'));
	$('#fromDate').val(validator.getValue('INT_FROM_DATE'));
	$('#uptoDate').val(validator.getValue('INT_UPTO_DATE'));
	$('#noOfDays').val(validator.getValue('INT_FOR_DAYS'));
	$('#interestRate').val(validator.getValue('APPLIED_INT_RATE'));
	$('#dateOfInvoicingBilling').val(validator.getValue('INV_DATE'));
	$('#remarks').val(validator.getValue('REMARKS'));
	var intersentOnAmount = validator.getValue('INT_ON_AMOUNT');
	var intersentOnAmountCurrency = validator.getValue('INT_ON_AMOUNT_CCY');
	var amountToBeBilled = validator.getValue('INV_AMOUNT');
	var amountToBeBilledCurrency = validator.getValue('INV_AMOUNT_CCY');
	$('#interestOnAmountFormat').val(formatAmount(intersentOnAmount, intersentOnAmountCurrency));
	$('#interestOnAmount').val(unformatAmount($('#interestOnAmountFormat').val()));
	$('#interestOnAmount_curr').val(intersentOnAmountCurrency);
	$('#amountToBeBilledFormat').val(formatAmount(amountToBeBilled, amountToBeBilledCurrency));
	$('#amountToBeBilled').val(unformatAmount($('#amountToBeBilledFormat').val()));
	$('#amountToBeBilled_curr').val(amountToBeBilledCurrency);
	scheduleID_val(false);
	resetLoading();
}

function validate(id) {
	switch (id) {
	case 'lesseCode':
		lesseCode_val(true);
		break;
	case 'aggrementNumber':
		aggrementNumber_val();
		break;
	case 'scheduleID':
		scheduleID_val(true);
		break;
	case 'entrySerial':
		entrySerial_val();
		break;
	case 'fromDate':
		fromDate_val();
		break;
	case 'uptoDate':
		uptoDate_val();
		break;
	case 'interestOnAmountFormat':
		interestOnAmountFormat_val();
		break;
	case 'dateOfInvoicingBilling':
		dateOfInvoicingBilling_val();
		break;
	case 'remarks':
		remarks_val();
		break;
	}
}

function backtrack(id) {
	switch (id) {
	case 'lesseCode':
		setFocusLast('lesseCode');
		break;
	case 'aggrementNumber':
		setFocusLast('lesseCode');
		break;
	case 'scheduleID':
		setFocusLast('aggrementNumber');
		break;
	case 'entrySerial':
		setFocusLast('scheduleID');
		break;
	case 'fromDate':
		if (!$('#entrySerial').prop('readOnly'))
			setFocusLast('entrySerial');
		else
			setFocusLast('scheduleID');
		break;
	case 'uptoDate':
		setFocusLast('fromDate');
		break;
	case 'interestOnAmountFormat':
		setFocusLast('uptoDate');
		break;
	case 'dateOfInvoicingBilling':
		setFocusLast('interestOnAmountFormat');
		break;
	case 'remarks':
		setFocusLast('dateOfInvoicingBilling');
		break;
	}
}

function lesseCode_val(valMode) {
	var value = $('#lesseCode').val();
	clearError('lesseCode_error');
	if (isEmpty(value)) {
		setError('lesseCode_error', MANDATORY);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('SUNDRY_DB_AC', value);
	validator.setValue(ACTION, USAGE);
	validator.setClass('patterns.config.web.forms.lss.eleaseintbean');
	validator.setMethod('validateLesseCode');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('lesseCode_error', validator.getValue(ERROR));
		$('#lesseCode_desc').html(EMPTY_STRING);
		$('#customerID').val(EMPTY_STRING);
		$('#customerName').val(EMPTY_STRING);
		return false;
	}
	$('#lesseCode_desc').html(validator.getValue("DESCRIPTION"));
	$('#customerID').val(validator.getValue("CUSTOMER_ID"));
	$('#customerName').val(validator.getValue("CUSTOMER_NAME"));
	if (valMode)
		setFocusLast('aggrementNumber');
	return true;
}

function aggrementNumber_val() {
	var value = $('#aggrementNumber').val();
	clearError('aggrementNumber_error');
	if (isEmpty(value)) {
		setError('aggrementNumber_error', MANDATORY);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('CUSTOMER_ID', $('#customerID').val());
	validator.setValue('AGREEMENT_NO', value);
	validator.setValue(ACTION, USAGE);
	validator.setClass('patterns.config.web.forms.lss.eleaseintbean');
	validator.setMethod('validateAggrementNumber');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('aggrementNumber_error', validator.getValue(ERROR));
		return false;
	}
	setFocusLast('scheduleID');
	return true;
}

function scheduleID_val(valMode) {
	var value = $('#scheduleID').val();
	clearError('scheduleID_error');
	$('#interestOnAmount_curr').val(EMPTY_STRING);
	$('#amountToBeBilled_curr').val(EMPTY_STRING);
	if (isEmpty(value)) {
		setError('scheduleID_error', MANDATORY);
		clearScheduleIDDependency();
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('LESSEE_CODE', $('#lesseCode').val());
	validator.setValue('AGREEMENT_NO', $('#aggrementNumber').val());
	validator.setValue('SCHEDULE_ID', value);
	validator.setValue(ACTION, USAGE);
	validator.setClass('patterns.config.web.forms.lss.eleaseintbean');
	validator.setMethod('validateScheduleID');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('scheduleID_error', validator.getValue(ERROR));
		clearScheduleIDDependency();
		return false;
	}
	var currency = validator.getValue("ASSET_CCY");
	$('#interestOnAmount_curr').val(currency);
	$('#amountToBeBilled_curr').val(currency);
	if(validator.getValue("INT_RATE_FOR_PRIOR_INVEST") != EMPTY_STRING)
		$('#interestRate').val(validator.getValue("INT_RATE_FOR_PRIOR_INVEST"));
	else
		$('#interestRate').val(ZERO);
	$('#w_basis').val(validator.getValue("DAY_BASIS"));
	$('#w_ro_choice').val(validator.getValue("ROUND_OFF_CHOICE"));
	$('#w_ro_factor').val(validator.getValue("ROUND_OFF_FACTOR"));
	if(valMode){
		if ($('#action').val() == ADD && $('#rectify').val() == COLUMN_DISABLE)
			setFocusLast('fromDate');
		else
			setFocusLast('entrySerial');
	}
	return true;
}

function entrySerial_val() {
	var value = $('#entrySerial').val();
	clearError('entrySerial_error');
	if ($('#rectify').val() == COLUMN_ENABLE) {
		if (isEmpty(value)) {
			setError('entrySerial_error', MANDATORY);
			return false;
		}
		if (!isNumeric(value)) {
			setError('entrySerial_error', NUMERIC_CHECK);
			return false;
		} else {
			validator.clearMap();
			validator.setMtm(false);
			validator.setValue('LESSEE_CODE', $('#lesseCode').val());
			validator.setValue('AGREEMENT_NO', $('#aggrementNumber').val());
			validator.setValue('SCHEDULE_ID', $('#scheduleID').val());
			validator.setValue('ENTRY_SL', value);
			validator.setValue(ACTION, $('#action').val());
			validator.setClass('patterns.config.web.forms.lss.eleaseintbean');
			validator.setMethod('validateEntrySerial');
			validator.sendAndReceive();
			if (validator.getValue(ERROR) != EMPTY_STRING) {
				setError('entrySerial_error', validator.getValue(ERROR));
				return false;
			} else {
				PK_VALUE = getEntityCode() + PK_SEPERATOR + $('#lesseCode').val() + PK_SEPERATOR + $('#aggrementNumber').val() + PK_SEPERATOR + $('#scheduleID').val() + PK_SEPERATOR + $('#entrySerial').val();
				if (!loadPKValues(PK_VALUE, 'entrySerial_error')) {
					return false;
				}
			}
		}
	}
	setFocusLast('fromDate');
	return true;
}

function fromDate_val() {
	var value = $('#fromDate').val();
	clearError('fromDate_error');
	if (isEmpty(value)) {
		setError('fromDate_error', MANDATORY);
		return false;
	}
	if (!isDate(value)) {
		setError('fromDate_error', INVALID_DATE);
		return false;
	}
	if (!isDateLesserEqual(value, getCBD())) {
		setError('fromDate_error', DATE_LECBD);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('LESSEE_CODE', $('#lesseCode').val());
	validator.setValue('AGREEMENT_NO', $('#aggrementNumber').val());
	validator.setValue('SCHEDULE_ID', $('#scheduleID').val());
	validator.setValue('FROM_DATE', $('#fromDate').val());
	validator.setValue(ACTION, USAGE);
	validator.setClass('patterns.config.web.forms.lss.eleaseintbean');
	validator.setMethod('validateFromDate');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('fromDate_error', validator.getValue(ERROR));
		$('#noOfDays').val(EMPTY_STRING);
		return false;
	}
	setFocusLast('uptoDate');
	return true;
}

function uptoDate_val() {
	var fromDate = $('#fromDate').val();
	var uptoDate = $('#uptoDate').val();
	clearError('uptoDate_error');
	if (isEmpty(uptoDate)) {
		setError('uptoDate_error', MANDATORY);
		return false;
	}
	if (!isDate(uptoDate)) {
		setError('uptoDate_error', INVALID_DATE);
		return false;
	}
	if (!isDateLesserEqual(uptoDate, getCBD())) {
		setError('uptoDate_error', DATE_LECBD);
		return false;
	}
	if (!isDateGreaterEqual(uptoDate, fromDate)) {
		setError('uptoDate_error', DATE_GEFD);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('LESSEE_CODE', $('#lesseCode').val());
	validator.setValue('AGREEMENT_NO', $('#aggrementNumber').val());
	validator.setValue('SCHEDULE_ID', $('#scheduleID').val());
	validator.setValue('UPTO_DATE', uptoDate);
	validator.setValue(ACTION, USAGE);
	validator.setClass('patterns.config.web.forms.lss.eleaseintbean');
	validator.setMethod('validateUptoDate');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		$('#noOfDays').val(EMPTY_STRING);
		setError('uptoDate_error', validator.getValue(ERROR));
		return false;
	}
	var nof_days = getNoOfDays(uptoDate, fromDate) + 1;
	$('#noOfDays').val(nof_days);
	setFocusLast('interestOnAmountFormat');
	return true;
}

function interestOnAmountFormat_val() {
	var value = unformatAmount($('#interestOnAmountFormat').val());
	var interestRate =  $('#interestRate').val() == null ? 0 : $('#interestRate').val();
	clearError('interestOnAmount_error');
	$('#amountToBeBilled').val(EMPTY_STRING);
	$('#amountToBeBilledFormat').val(EMPTY_STRING);
	if (isEmpty(value)) {
		setError('interestOnAmount_error', MANDATORY);
		return false;
	}
	if (isZero(value)) {
		setError('interestOnAmount_error', ZERO_CHECK);
		return false;
	}
	if (isNegativeAmount(value)) {
		setError('interestOnAmount_error', POSITIVE_AMOUNT);
		return false;
	}
	if (!isCurrencySmallAmount($('#interestOnAmount_curr').val(), value)) {
		setError('interestOnAmount_error', INVALID_SMALL_AMOUNT);
		return false;
	}
	$('#interestOnAmount').val(value);
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('INTEREST_AMOUNT',$('#interestOnAmount').val());
	validator.setValue('INTEREST_RATE', interestRate);
	validator.setValue('NO_OF_DAYS', $('#noOfDays').val());
	validator.setValue('W_BASIS', $('#w_basis').val());
	validator.setValue('RND_FACT', $('#w_ro_factor').val());
	validator.setValue('RND_CHOICE', $('#w_ro_choice').val());
	validator.setValue(ACTION, USAGE);
	validator.setClass('patterns.config.web.forms.lss.eleaseintbean');
	validator.setMethod('validateAmountToBeBilled');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('interestOnAmount_error', validator.getValue(ERROR));
		return false;
	}
	$('#amountToBeBilled').val(validator.getValue("RND_AMT"));
	$('#amountToBeBilledFormat').val(formatAmount($('#amountToBeBilled').val(), $('#amountToBeBilled_curr').val()));
	setFocusLast('dateOfInvoicingBilling');
	return true;
}


function dateOfInvoicingBilling_val() {
	var value = $('#dateOfInvoicingBilling').val();
	clearError('dateOfInvoicingBilling_error');
	if (isEmpty(value)) {
		setError('dateOfInvoicingBilling_error', MANDATORY);
		return false;
	}
	if (!isDate(value)) {
		setError('dateOfInvoicingBilling_error', INVALID_DATE);
		return false;
	}
	if (isDateLesser(value, getCBD())) {
		setError('dateOfInvoicingBilling_error', DATE_GECBD);
		return false;
	}
	setFocusLast('remarks');
	return true;
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', INVALID_REMARKS);
			setFocusLast('remarks');
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', INVALID_REMARKS);
			setFocusLast('remarks');
			return false;
		}
	}
	if ($('#rectify').val() == COLUMN_ENABLE) {
		if (isEmpty(value)) {
			setError('remarks_error', MANDATORY);
			setFocusLast('remarks');
			return false;
		}
	}
	setFocusOnSubmit();
	return true;
}

function revalidate() {
	var amount = unformatAmount($('#interestOnAmountFormat').val());
	$('#interestOnAmount').val(amount);
}