var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'IINVTERMCON';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}

function clearFields() {
	$('#stateCode').val(EMPTY_STRING);
	$('#effectiveDate').val(EMPTY_STRING);
	$('#termsAndConditions').val(EMPTY_STRING);
}

function loadData() {
	$('#stateCode').val(validator.getValue('STATE_CODE'));
	$('#stateCode_desc').html(validator.getValue('F1_DESCRIPTION'));
	$('#effectiveDate').val(validator.getValue('EFF_DATE'));
	$('#termsAndConditions').val(validator.getValue('TERMS_CONDITION'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	loadAuditFields(validator);
}