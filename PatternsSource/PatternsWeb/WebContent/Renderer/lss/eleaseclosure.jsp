<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.lss.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/lss/eleaseclosure.js" />
	</web:dependencies>
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="eleaseclosure.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/lss/eleaseclosure" id="eleaseclosure" method="POST">
				<web:dividerBlock>
					<type:optionBox add="true" modify="false" view="true" />
				</web:dividerBlock>
				<web:viewContent id="po_view1" styleClass="hidden">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="eleaseclosure.entrydate" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:dateDaySL property="entry" id="entry" readOnly="true" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="eleaseclosure.leaseCode" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:lesseeCode property="lesseCode" id="lesseCode" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="eleaseclosure.customerid" var="program" />
										</web:column>
										<web:column>
											<type:customerCode property="customerID" id="customerID" readOnly="true" />
										</web:column>
										<web:column>
											<web:legend key="eleaseclosure.customername" var="program" />
										</web:column>
										<web:column>
											<type:nameDisplay property="customerName" id="customerName" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="eleaseclosure.agreementnumber" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:agreementNo property="agreementNumber" id="agreementNumber" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="eleaseclosure.scheduleid" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:scheduleID property="scheduleID" id="scheduleID" lookup="true" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:viewContent id="hierarchy_view" styleClass="hidden">
								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="200px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowEven>
											<web:column>
											</web:column>
											<web:column>
												<web:anchorMessage var="program" key="qlease.link" onclick="qLease()" style="text-decoration:underline;cursor:pointer" />
											</web:column>
										</web:rowEven>
									</web:table>
								</web:section>
							</web:viewContent>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="20px" />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<type:checkboxCenterAlign property="verifiedLeaseDetails" id="verifiedLeaseDetails" />
										</web:column>
										<web:column>
											<web:legend key="eleaseclosure.verifiedleasedetails" var="program" />
											<type:error property="verifiedLease"/>
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="eleaseclosure.referencenumber" var="program" />
										</web:column>
										<web:column>
											<type:otherInformation20 property="referenceNumber" id="referenceNumber" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="form.remarks" var="common" mandatory="true" />
										</web:column>
										<web:column>
											<type:remarks property="remarks" id="remarks" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column span="2">
											<web:submitReset />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
						</web:sectionBlock>
					</web:dividerBlock>
				</web:viewContent>
				<web:element property="command" />
				<web:element property="action" />
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
	<web:queryGrid />
	<web:TBAGrid />
	<web:contextSearch />
</web:fragment>
