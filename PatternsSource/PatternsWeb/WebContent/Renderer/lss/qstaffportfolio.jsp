<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<%@taglib prefix="resource" tagdir="/WEB-INF/tags/resource"%>
<web:bundle baseName="patterns.config.web.forms.lss.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<resource:css href="public/cdn/vendor/dhtmlxGrid/codebase/skins/dhtmlxgrid_dhx_skyblue.css" media="screen" />
		<resource:css href="public/cdn/vendor/dhtmlxTree/codebase/skins/roboto.css" media="screen" />
		<resource:css href="public/cdn/vendor/dhtmlxTreeView/codebase/dhtmlxtreeview.css" media="screen" />
		<resource:script src="public/cdn/vendor/dhtmlxCommon/codebase/dhtmlxcore.js" />
		<resource:script src="public/cdn/vendor/dhtmlxTree/codebase/dhtmlxtree.js" />
		<resource:script src="public/cdn/vendor/dhtmlxTreeView/codebase/dhtmlxtreeview.js" />
		<resource:script src="public/cdn/vendor/dhtmlxTreeGrid/codebase/dhtmlxtreegrid.js" />
		<web:script src="Renderer/lss/qstaffportfolio.js" />
	</web:dependencies>
	<style>
		.odd {
			background-color: #ebf3ff;
		}
	</style>
	<web:loadPK />
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="istaffportfolio.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:viewContent id="po_view1">
				<web:dividerBlock>
					<web:sectionBlock width="width-100p" align="left">
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="istaffportfolio.branchcode" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:branchCodeDisplay property="branchCode" id="branchCode" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="form.effectivedate" var="common" mandatory="true" />
									</web:column>
									<web:column>
										<type:dateDisplay property="effectiveDate" id="effectiveDate" />
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
						<web:section>
							<web:viewTitle var="program" key="istaffportfolio.roleheirarchysection"></web:viewTitle>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="180px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:grid height="210px" width="820px" id="roleHierarchyGrid" src="lss/istaffportfolio_rolehierarchy.xml">
										</web:grid>
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowOdd>
									<web:column>
										<web:legend key="form.IsUseEnabled" var="common" />
									</web:column>
									<web:column>
										<type:checkboxDisplay property="enabled" id="enabled" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="form.remarks" var="common" />
									</web:column>
									<web:column>
										<type:remarksDisplay property="remarks" id="remarks" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:viewContent id="viewStaffDetail" styleClass="hidden">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="istaffportfolio.rolecode" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:userDisplay property="roleCode" id="roleCode" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="istaffportfolio.category" var="program" />
										</web:column>
										<web:column>
											<type:comboDisplay property="category" id="category" datasourceid="COMMON_ROLE_CATEGORY" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="istaffportfolio.noofstaff" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:numberDisplay property="noOfStaff" id="noOfStaff" size="3" length="2" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:viewTitle var="program" key="istaffportfolio.staffdetailsection"></web:viewTitle>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:grid height="210px" width="670px" id="viewStaffDetailGrid" src="lss/istaffportfolio_staffDetail.xml">
											</web:grid>
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:button id="close" key="form.close" var="common" onclick="closeStaffWindow()" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
						</web:viewContent>
						<web:auditDisplay />
					</web:sectionBlock>
				</web:dividerBlock>
			</web:viewContent>
		</web:viewSubPart>
	</web:viewPart>
</web:fragment>
