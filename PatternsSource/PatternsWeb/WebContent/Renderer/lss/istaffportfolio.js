var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'ISTAFFPORTFOLIO';
var portfolioList = {};
var staffRoleCodeList = {};
var staffGridPortfolioList = {};
var previousPortfolio;
var previousStaffRole;
var deletePortfolioList = {};
var deletePortfolios;
var deleteStaffRoleCodes;
var gridEditMode = false;
function init() {
	refreshTBAGrid();
	refreshQueryGrid();
	hide('staffDetails');
	hide('viewStaffDetail');
	if (_redisplay) {
		if ($('#action').val() == ADD) {
			$('#enabled').prop('disabled', true);
			setCheckbox('enabled', YES);
		}
		if (!isEmpty($('#xmlRoleHierarchyGrid').val()))
			roleHierarchyGrid.loadXMLString($('#xmlRoleHierarchyGrid').val());
	} else {
		roleHierarchyGrid.enableAlterCss("even", "odd");
		roleHierarchyGrid.loadXMLString($('#xmlRoleHierarchyGrid').val());
	}

}

function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'ISTAFFPORTFOLIO_MAIN', queryGrid);
}

function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}

function doclearfields(id) {
	switch (id) {
	case 'branchCode':
		if (isEmpty($('#branchCode').val())) {
			clearFields();
			roleHierarchyGrid.clearAll();
			break;
		}
	case 'effectiveDate':
		if (isEmpty($('#effectiveDate').val())) {
			$('#effectiveDate_error').html(EMPTY_STRING);
			clearNonPKFields();
			roleHierarchyGrid.clearAll();
			break;
		}
	}
}

function clearFields() {
	$('#branchCode').val(EMPTY_STRING);
	$('#branchCode_error').html(EMPTY_STRING);
	$('#branchCode_desc').html(EMPTY_STRING);
	$('#effectiveDate').val(getCBD());
	$('#effectiveDate_error').html(EMPTY_STRING);
	clearNonPKFields();
}

function clearNonPKFields() {
	clearStaffWindowHeaderDtls();
	setCheckbox('enabled', YES);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
	$('#staffGridInfo_error').html(EMPTY_STRING);
	clearStaffDetailFields();
	staffDetailGrid.clearAll();
	viewStaffDetailGrid.clearAll();
}

function clearStaffWindowHeaderDtls() {
	$('#roleCode').val(EMPTY_STRING);
	$('#roleCode_desc').html(EMPTY_STRING);
	$('#roleCode_error').html(EMPTY_STRING);
	$('#category').val(EMPTY_STRING);
	$('#category_error').html(EMPTY_STRING);
	$('#noOfStaff').val(EMPTY_STRING);
	$('#noOfStaff_error').html(EMPTY_STRING);
	gridEditMode = false;
}

function clearStaffDetailFields() {
	$('#staffID').val(EMPTY_STRING);
	$('#staffID_desc').html(EMPTY_STRING);
	$('#staffID_error').html(EMPTY_STRING);
	$('#portfolioCode').val(EMPTY_STRING);
	$('#portfolioCode_desc').html(EMPTY_STRING);
	$('#portfolioCode_error').html(EMPTY_STRING);
	$('#portfolioCode').prop('readonly', false);
	$('#portfolioCode_desc').prop('readonly', false);
	$('#portfolioCode_pic').prop('disabled', false);
	previousPortfolio = EMPTY_STRING;
	previousStaffRole = EMPTY_STRING;
	gridEditMode = false;
}

function doHelp(id) {
	switch (id) {
	case 'branchCode':
		help('COMMON', 'HLP_BRANCH_CODE', $('#branchCode').val(), EMPTY_STRING, $('#branchCode'));
		break;
	case 'effectiveDate':
		help(CURRENT_PROGRAM_ID, 'HLP_EFF_DATE', $('#effectiveDate').val(), $('#branchCode').val(), $('#effectiveDate'));
		break;
	case 'staffID':
		help('COMMON', 'HLP_STAFF_CODE_FOR_ROLE', $('#staffID').val(), $('#roleCode').val() + PK_SEPERATOR + $('#branchCode').val(), $('#staffID'));
		break;
	case 'portfolioCode':
		help('COMMON', 'HLP_PORTFOLIO_CODE', $('#portfolioCode').val(), EMPTY_STRING, $('#portfolioCode'));
		break;
	}
}

function add() {
	$('#effectiveDate').val(getCBD());
	setCheckbox('enabled', YES);
	$('#enabled').prop('disabled', true);
	$('#branchCode').focus();
}

function modify() {
	if ($('#action').val() == MODIFY) {
		$('#effectiveDate').val(EMPTY_STRING);
		$('#enabled').prop('disabled', false);
	} else {
		$('#enabled').prop('disabled', true);
	}
	$('#branchCode').focus();
}

function loadData() {
	$('#branchCode').val(validator.getValue('BRANCH_CODE'));
	$('#branchCode_desc').html(validator.getValue('F1_BRANCH_NAME'));
	$('#effectiveDate').val(validator.getValue('EFF_DATE'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadStaffRoleHierarchyGrid();
	resetLoading();
}

function loadStaffRoleHierarchyGrid() {
	$('#staffGridInfo_error').html(EMPTY_STRING);
	hideMessage(getProgramID());
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('INVENTORY_SL', $('#mainSerial').val());
	validator.setValue('FROM_QUERY_VIEW', NO);
	validator.setValue('BRANCH_CODE', $('#branchCode').val());
	validator.setValue('EFF_DATE', $('#effectiveDate').val());
	validator.setClass('patterns.config.web.forms.lss.istaffportfoliobean');
	validator.setMethod('getRoleCodeHierarchy');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('staffGridInfo_error', validator.getValue(ERROR));
		return false;
	} else {
		roleHierarchyGrid.clearAll();
		var result_xml = validator.getValue(RESULT_XML);
		roleHierarchyGrid.setImagePath(WidgetConstants.GRID_IMAGE_PATH);
		roleHierarchyGrid.setEditable(false);
		roleHierarchyGrid.enableAlterCss("even", "odd");
		roleHierarchyGrid.loadXMLString(result_xml);
		portList = [];
		staffList = [];
		portfolioList = {};
		staffGridPortfolioList = {};
		deletePortfolioList = {};
		if (!isEmpty(validator.getValue('PORTFOLIO_LIST'))) {
			portList = validator.getValue('PORTFOLIO_LIST').split(PK_SEPERATOR);
			staffList = validator.getValue('STAFF_ROLE_LIST').split(PK_SEPERATOR);
		}
		for (var i = 0; i < portList.length; i++) {
			portfolioList[portList[i]] = staffList[i];
		}
	}
	setFocusLast('effectiveDate');
	return true;
}

function view(source, primaryKey) {
	hideParent('lss/qstaffportfolio', source, primaryKey);
	resetLoading();
}

function validate(id) {
	valMode = true;
	switch (id) {
	case 'branchCode':
		branchCode_val();
		break;
	case 'effectiveDate':
		effectiveDate_val();
		break;
	case 'category':
		category_val();
		break;
	case 'noOfStaff':
		noOfStaff_val();
		break;
	case 'staffID':
		staffID_val(valMode);
		break;
	case 'portfolioCode':
		portfolioCode_val(valMode);
		break;
	case 'enabled':
		enabled_val();
		break;
	case 'remarks':
		remarks_val();
		break;
	}
}

function backtrack(id) {
	switch (id) {
	case 'branchCode':
		setFocusLast('branchCode');
		break;
	case 'effectiveDate':
		setFocusLast('branchCode');
		break;
	case 'noOfStaff':
		setFocusLast('noOfStaff');
		break;
	case 'staffID':
		setFocusLast('noOfStaff');
		break;
	case 'portfolioCode':
		setFocusLast('staffID');
		break;
	case 'staffDetailGrid_add':
		setFocusLast('portfolioCode');
		break;
	case 'addStaffDetail':
		setFocusLast('staffID');
		break;
	case 'enabled':
		setFocusLast('effectiveDate');
		break;
	case 'remarks':
		if ($('#action').val() == MODIFY)
			setFocusLast('enabled');
		else
			setFocusLast('effectiveDate');
		break;
	}
}

function branchCode_val() {
	var value = $('#branchCode').val();
	clearError('branchCode_error');
	$('#staffGridInfo_error').html(EMPTY_STRING);
	if (isEmpty(value)) {
		setError('branchCode_error', MANDATORY);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('BRANCH_CODE', value);
	validator.setValue(ACTION, USAGE);
	validator.setClass('patterns.config.web.forms.lss.istaffportfoliobean');
	validator.setMethod('validateBranchCode');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('branchCode_error', validator.getValue(ERROR));
		return false;
	}
	$('#branchCode_desc').html(validator.getValue("BRANCH_NAME"));
	setFocusLast('effectiveDate');
	return true;
}

function effectiveDate_val() {
	var value = $('#effectiveDate').val();
	clearError('effectiveDate_error');
	$('#staffGridInfo_error').html(EMPTY_STRING);
	roleHierarchyGrid.clearAll();
	clearStaffDetailFields();
	staffDetailGrid.clearAll();
	viewStaffDetailGrid.clearAll();
	if (isEmpty(value)) {
		$('#effectiveDate').val(getCBD());
		value = $('#effectiveDate').val();
	}
	if (!isValidEffectiveDate(value, 'effectiveDate_error')) {
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('BRANCH_CODE', $('#branchCode').val());
		validator.setValue('EFF_DATE', value);
		validator.setValue(ACTION, $('#action').val());
		validator.setClass('patterns.config.web.forms.lss.istaffportfoliobean');
		validator.setMethod('validateEffectiveDate');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('effectiveDate_error', validator.getValue(ERROR));
			return false;
		} else {
			if ($('#action').val() == MODIFY) {
				PK_VALUE = getEntityCode() + PK_SEPERATOR + $('#branchCode').val() + PK_SEPERATOR + $('#effectiveDate').val();
				if (!loadPKValues(PK_VALUE, 'effectiveDate_error')) {
					return false;
				}
			}
			loadStaffRoleHierarchyGrid();
		}
	}
	setFocusLast('remarks');
	return true;
}

function category_val() {
	return true;
}

function noOfStaff_val() {
	var value = $('#noOfStaff').val();
	clearError('noOfStaff_error');
	if (isEmpty(value)) {
		value = 0;
		$('#noOfStaff').val(value);
	}
	if (!isNumeric(value)) {
		setError('noOfStaff_error', INVALID_NUMBER);
		return false;
	}
	if (!isPositive(value)) {
		setError('noOfStaff_error', POSITIVE_CHECK);
		return false;
	}
	$('#noOfStaffHide').val(value);
	var id = roleHierarchyGrid.getAllSubItems(0).split(",");
	for (var i = 0; i < id.length; i++) {
		if (roleHierarchyGrid.cells(id[i], 3).getValue() == 1) {
			if (id[i] != $('#roleCode').val()) {
				if ($('#noOfStaff').val() == 1) {
					setError('noOfStaff_error', TC_STAFF_FOR_ALL_PORTFOLIO_CONFIGURED);
					return false;
				}
			}
		}
		if (roleHierarchyGrid.cells(id[i], 3).getValue() > 1) {
			if (id[i] != $('#roleCode').val()) {
				if ($('#noOfStaff').val() == 1) {
					setError('noOfStaff_error', TC_STAFF_FOR_ALL_PORTFOLIO_CANT_CONFIGURED);
					return false;
				}
			}
		}
	}
	if (value > 1) {
		$('#portfolioCode').prop('readonly', false);
		$('#portfolioCode_desc').prop('readonly', false);
		$('#portfolioCode_pic').prop('disabled', false);
	} else {
		$('#portfolioCode').prop('readonly', true);
		$('#portfolioCode_desc').prop('readonly', true);
		$('#portfolioCode_pic').prop('disabled', true);
		$('#portfolioCode').val(EMPTY_STRING);
		$('#portfolioCode_desc').html(EMPTY_STRING);
		$('#portfolioCode_error').html(EMPTY_STRING);
	}
	setFocusLast('staffID');
	return true;
}

function staffID_val(valMode) {
	var value = $('#staffID').val();
	clearError('staffID_error');
	if (isEmpty(value)) {
		setError('staffID_error', MANDATORY);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('BRANCH_CODE', $('#branchCode').val());
	validator.setValue('STAFF_ROLE_CODE', $('#roleCode').val());
	validator.setValue('STAFF_CODE', value);
	validator.setValue(ACTION, USAGE);
	validator.setClass('patterns.config.web.forms.lss.istaffportfoliobean');
	validator.setMethod('validateStaffCode');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('staffID_error', validator.getValue(ERROR));
		return false;
	}
	$('#staffID_desc').html(validator.getValue("NAME"));
	if ($('#noOfStaff').val() > 1) {
		$('#portfolioCode').prop('readonly', false);
		$('#portfolioCode_desc').prop('readonly', false);
		$('#portfolioCode_pic').prop('disabled', false);
	} else {
		$('#portfolioCode').prop('readonly', true);
		$('#portfolioCode_desc').prop('readonly', true);
		$('#portfolioCode_pic').prop('disabled', true);
		$('#portfolioCode').val(EMPTY_STRING);
		$('#portfolioCode_desc').html(EMPTY_STRING);
		$('#portfolioCode_error').html(EMPTY_STRING);
	}
	if (valMode)
		if ($('#noOfStaff').val() > 1)
			setFocusLast('portfolioCode');
		else
			setFocusLastOnGridSubmit('staffDetailGrid');
	return true;
}

function portfolioCode_val(valMode) {
	var value = $('#portfolioCode').val();
	clearError('portfolioCode_error');
	if ($('#noOfStaff').val() > 1) {
		if (isEmpty(value)) {
			setError('portfolioCode_error', MANDATORY);
			return false;
		}
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('PORTFOLIO_CODE', value);
		validator.setValue('NO_OF_STAFF', $('#noOfStaff').val());
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.lss.istaffportfoliobean');
		validator.setMethod('validatePortfolioCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('portfolioCode_error', validator.getValue(ERROR));
			return false;
		}
		$('#portfolioCode_desc').html(validator.getValue("DESCRIPTION"));
	} else if ($('#noOfStaff').val() == 1) {
		if (!isEmpty(value)) {
			setError('portfolioCode_error', TC_CANT_CNFGR_PORTFOLIO_FOR_ONE_STAFF);
			return false;
		}
	}
	if (valMode)
		setFocusLastOnGridSubmit('staffDetailGrid');
	return true;
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', INVALID_REMARKS);
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', INVALID_REMARKS);
			return false;
		}
	}
	if ($('#action').val() == MODIFY) {
		if (isEmpty(value)) {
			setError('remarks_error', MANDATORY);
			return false;
		}
	}
	setFocusOnSubmit();
	return true;
}

function getRoleHierarchyDetails() {
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('BRANCH_CODE', $('#branchCode').val());
	validator.setClass('patterns.config.web.forms.lss.istaffportfoliobean');
	validator.setMethod('getRoleCodeHierarchy');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('branchCode_error', validator.getValue(ERROR));
		return false;
	} else {
		var result_xml = validator.getValue(RESULT_XML);
		roleHierarchyGrid.setImagePath(WidgetConstants.GRID_IMAGE_PATH);
		roleHierarchyGrid.loadXMLString(result_xml);
	}
	return true;
}

function allocateStaffDetails(staffRoleCode, staffRoleDescription, category, noOfStaff) {
	clearStaffDetailFields();
	staffDetailGrid.clearAll();
	staffGridPortfolioList = {};
	deletePortfolioList = {};
	clearStaffWindowHeaderDtls();
	if (isEmpty($('#branchCode').val())) {
		setError('branchCode_error', MANDATORY);
		return false;
	}
	if (roleHierarchyGrid.cells(staffRoleCode, 3).getValue() > 0) {
		$('#roleCode').val(roleHierarchyGrid.cells(staffRoleCode, 6).getValue());
		$('#roleCode_desc').html(roleHierarchyGrid.cells(staffRoleCode, 1).getValue());
		$('#category').val(category);
		$('#noOfStaff').val(roleHierarchyGrid.cells(staffRoleCode, 3).getValue());
		staffDetailGrid.loadXMLString(roleHierarchyGrid.cells(staffRoleCode, 5).getValue());
	} else {
		$('#roleCode').val(staffRoleCode);
		$('#roleCodeHide').val(staffRoleCode);
		$('#roleCode_desc').html(staffRoleDescription);
		$('#category').val(category);
		$('#categoryHide').val(category);
		$('#noOfStaff').val(noOfStaff);
	}
	if ($('#noOfStaff').val() > 1) {
		$('#portfolioCode').prop('readonly', false);
		$('#portfolioCode_desc').prop('readonly', false);
		$('#portfolioCode_pic').prop('disabled', false);
	} else {
		$('#portfolioCode').prop('readonly', true);
		$('#portfolioCode_desc').prop('readonly', true);
		$('#portfolioCode_pic').prop('disabled', true);
		$('#portfolioCode').val(EMPTY_STRING);
		$('#portfolioCode_desc').html(EMPTY_STRING);
		$('#portfolioCode_error').html(EMPTY_STRING);
	}
	win = showWindow('staffDetails', '', TC_STAFF_ALLOCATION_DETAILS, 'staffDetails', true, false, false, '750', '580');
	return true;
}

function viewStaffDetails(staffRoleCode) {
	if (!isEmpty(roleHierarchyGrid.cells(staffRoleCode, 5).getValue())) {
		viewStaffDetailGrid.clearAll();
		viewStaffDetailGrid.loadXMLString(roleHierarchyGrid.cells(staffRoleCode, 5).getValue());
		win = showWindow('viewStaffDetail', '', TC_VIEW_ALLOC_STAFF_DETAILS, 'viewStaffDetail', true, false, false, '750', '380');
	}
	return true;
}

function onloadInlinePopUp(popUPId) {
	if (popUPId == 'staffDetails') {
		show('staffDetails');
		setFocusLast('noOfStaff');
	} else if (popUPId == 'viewStaffDetail') {
		show('viewStaffDetail');
		viewStaffDetailGrid.setColumnHidden(0, true);
		setFocusLast('close');
	}
	return true;
}

function closeStaffDetailWindow() {
	var rowCount = staffDetailGrid.getRowsNum();
	if (rowCount > 0)
		$('#xmlStaffDetailGrid').val(staffDetailGrid.serialize());
	if ($('#noOfStaff').val() > rowCount) {
		setError('staffID_error', formatMessage(TC_ALL_STAFF_NOT_CONFIGURED, [ $('#noOfStaff').val(), staffDetailGrid.getRowsNum() ]));
		return false;
	}
	if ($('#noOfStaff').val() < rowCount) {
		setError('staffID_error', TC_CANT_ADD_GT_STAFF_COUNT);
		return false;
	}
	if ($('#noOfStaff').val() == 1) {
		if (!isEmpty(staffDetailGrid.cells2(0, 4).getValue())) {
			setError('staffID_error', TC_CANT_CNFGR_PORTFOLIO_FOR_ONE_STAFF);
			return false;
		}
	} else if ($('#noOfStaff').val() > 1) {
		if (isEmpty(staffDetailGrid.cells2(0, 4).getValue())) {
			setError('staffID_error', formatMessage(TC_PORTFOLIO_NT_CNFGRD_FOR_STAFF, [ staffDetailGrid.cells2(0, 2).getValue() ]));
			return false;
		}
	}
	updateStaffDetail();
	$.each(deletePortfolioList, function(key, value) {
		delete portfolioList[key];
	});
	$.each(staffGridPortfolioList, function(key, value) {
		portfolioList[key] = value;
	});
	closeInlinePopUp(win);
	setFocusLast('remarks');
	return true;
}

function updateStaffDetail() {
	roleHierarchyGrid.cells($('#roleCode').val(), 3).setValue($('#noOfStaff').val());
	if ($('#noOfStaff').val() == 1)
		roleHierarchyGrid.cells($('#roleCode').val(), 4).setValue(staffDetailGrid.cells2(0, 3).getValue());
	else if ($('#noOfStaff').val() > 1)
		roleHierarchyGrid.cells($('#roleCode').val(), 4).setValue("<a href=\"javascript:void(0)\" onClick=\"viewStaffDetails('" + $('#roleCode').val() + "')\">View</a>");
	else
		roleHierarchyGrid.cells($('#roleCode').val(), 4).setValue(EMPTY_STRING);
	roleHierarchyGrid.cells($('#roleCode').val(), 5).setValue($('#xmlStaffDetailGrid').val());
}

function closeStaffWindow() {
	closeInlinePopUp(win);
	setFocusLast('remarks');
	return true;
}

function enabled_val() {
	setFocusOnSubmit();
	return true;
}

// grid start
function staff_addGrid(editMode, editRowId) {
	if ($('#noOfStaff').val() == 0) {
		setError('staffID_error', TC_STAFF_CANT_ZERO);
		return false;
	}
	if (staffID_val(false) && portfolioCode_val(false)) {
		var rowCount = staffDetailGrid.getRowsNum();
		if (!editMode) {
			if ($('#noOfStaff').val() <= rowCount) {
				setError('staffID_error', TC_CANT_ADD_GT_STAFF_COUNT);
				return false;
			}
		}
		if (staff_gridDuplicateCheck(editMode, editRowId)) {
			if (isEmpty($('#portfolioCode').val()) && !isEmpty(previousPortfolio)) {
				deletePortfolioList[previousPortfolio] = previousStaffRole;
			}
			if (!isEmpty($('#portfolioCode').val())) {
				if ($('#portfolioCode').val() in portfolioList && !($('#portfolioCode').val() in deletePortfolioList)) {
					if (previousPortfolio != $('#portfolioCode').val()) {
						setError('staffID_error', formatMessage(TC_PORTFOLIO_ALREADY_CONFIGURED, [ portfolioList[$('#portfolioCode').val()] ]));
						return false;
					}
				} else {
					if (!isEmpty(previousPortfolio)) {
						if (previousPortfolio != $('#portfolioCode').val()) {
							staffGridPortfolioList[$('#portfolioCode').val()] = $('#roleCode').val();
							deletePortfolioList[previousPortfolio] = previousStaffRole;
						}
					} else
						staffGridPortfolioList[$('#portfolioCode').val()] = $('#roleCode').val();
				}
			}
			var field_values = [ 'false', $('#roleCode').val(), $('#staffID').val(), $('#staffID_desc').html(), $('#portfolioCode').val(), $('#portfolioCode_desc').html() ];
			_grid_updateRow(staffDetailGrid, field_values);
			clearStaffDetailFields();
			$('#staffID').focus();
			return true;
		} else
			return false;
	} else {
		return false;
	}
}

function staff_afterAdd(rowId) {
	if (gridEditMode)
		staffDetailGrid.setUserData("", "grid_edit_id", rowId);
}

function staff_gridDuplicateCheck(editMode, editRowId) {
	var currentValue = [ $('#staffID').val(), $('#portfolioCode').val() ];
	if (!_grid_duplicate_check(staffDetailGrid, currentValue, [ 2, 4 ])) {
		setError('staffID_error', DUPLICATE_DATA);
		$('#staffID_desc').html(EMPTY_STRING);
		return false;
	}
	var currentValue = [ $('#portfolioCode').val() ];
	if (!_grid_duplicate_check(staffDetailGrid, currentValue, [ 4 ])) {
		setError('staffID_error', TC_CANT_ASIGN_SAME_PORTFOLIO);
		$('#staffID_desc').html(EMPTY_STRING);
		return false;
	}
	return true;
}

function staff_editGrid(rowId) {
	previousPortfolio = EMPTY_STRING;
	previousStaffRole = EMPTY_STRING;
	$('#staffID').val(staffDetailGrid.cells(rowId, 2).getValue());
	$('#staffID_desc').html(staffDetailGrid.cells(rowId, 3).getValue());
	$('#portfolioCode').val(staffDetailGrid.cells(rowId, 4).getValue());
	$('#portfolioCode_desc').html(staffDetailGrid.cells(rowId, 5).getValue());
	previousPortfolio = staffDetailGrid.cells(rowId, 4).getValue();
	previousStaffRole = staffDetailGrid.cells(rowId, 1).getValue();
	$('#portfolioCode').prop('readonly', false);
	$('#portfolioCode_desc').prop('readonly', false);
	$('#portfolioCode_pic').prop('disabled', false);
	gridEditMode = true;
	$('#staffID').focus();
}

function staff_cancelGrid(rowId) {
	clearStaffDetailFields();
}

function gridExit(id) {
	switch (id) {
	case 'staffID':
		$('#xmlStaffDetailGrid').val(staffDetailGrid.serialize());
		clearStaffDetailFields();
		setFocusLast('addStaffDetail');
		break;
	}
}

function gridDeleteRow(id) {
	switch (id) {
	case 'staffID':
		deleteGridRecord(staffDetailGrid);
		break;
	}
}

function staff_beforeDelete() {
	var ros = staffDetailGrid.getCheckedRows(0);
	var rosaerr = ros.split(',');
	deletePortfolios = [];
	deleteStaffRoleCodes = [];
	if (ros == EMPTY_STRING) {
		alert(SELECT_ATLEAST_ONE);
		return;
	} else {
		for (var i = 0; i < rosaerr.length; i++) {
			if (!isEmpty(staffDetailGrid.cells(rosaerr[i], 4).getValue())) {
				deletePortfolios.push(staffDetailGrid.cells(rosaerr[i], 4).getValue());
				deleteStaffRoleCodes.push(staffDetailGrid.cells(rosaerr[i], 1).getValue());
			}
		}
	}
	return true;
}

function staff_afterDelete() {
	for (var i = 0; i < deletePortfolios.length; i++) {
		if (!isEmpty(deletePortfolios[i])) {
			deletePortfolioList[deletePortfolios[i]] = deleteStaffRoleCodes[i];
		}
	}
}

function revalidate() {
	roleHierarchy_revaildateGrid();
}

function roleHierarchy_revaildateGrid() {
	if (roleHierarchyGrid.getRowsNum() > 0) {
		var count = 0;
		var id = roleHierarchyGrid.getAllSubItems(0).split(",");
		for (var i = 0; i < id.length; i++) {
			if (roleHierarchyGrid.cells(id[i], 3).getValue() > 0) {
				count++;
				break;
			}
		}
		if (count == 0) {
			setError('staffGridInfo_error', TC_STAFF_ALLOC_ATLEAST_ONE_ROLE);
			errors++;
		} else {
			roleHierarchyGrid.setSerializationLevel(false, false, false, false, false, true);
			$('#xmlRoleHierarchyGrid').val(roleHierarchyGrid.serialize());
			$('#staffGridInfo_error').html(EMPTY_STRING);
		}
	} else {
		roleHierarchyGrid.clearAll();
		$('#xmlRoleHierarchyGrid').val(roleHierarchyGrid.serialize());
		setError('staffGridInfo_error', ADD_ATLEAST_ONE_ROW);
		errors++;
	}
}