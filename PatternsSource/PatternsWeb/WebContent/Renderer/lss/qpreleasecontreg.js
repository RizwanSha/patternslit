var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'EPRELEASECONTREG';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}
function clearFields() {
	$('#preLeaseContractRefDate').val(EMPTY_STRING);
	$('#preLeaseContractRefSerial').val(EMPTY_STRING);
	$('#customerID').val(EMPTY_STRING);
	$('#customerID_desc').html(EMPTY_STRING);
	$('#lesseeCode').val(EMPTY_STRING);
	$('#lesseeCode_desc').html(EMPTY_STRING);
	$('#productCode').val(EMPTY_STRING);
	$('#productCode_desc').html(EMPTY_STRING);
	$('#branchCode').val(EMPTY_STRING);
	$('#branchCode_desc').html(EMPTY_STRING);
	$('#addressSerial').val(EMPTY_STRING);
	$('#contactSerial').val(EMPTY_STRING);
	setCheckbox('interestApplicable', NO);
	$('#interestRateInvestLeaseCommit').val(EMPTY_STRING);
	$('#interestBillingChoice').prop('selectedIndex', 0);
	$('#interestDebitNotePrinting').prop('selectedIndex', 1);
	$('#remarks').val(EMPTY_STRING);
}
function loadData() {
	$('#preLeaseContractRefDate').val(validator.getValue('CONTRACT_DATE'));
	$('#preLeaseContractRefSerial').val(validator.getValue('CONTRACT_SL'));
	$('#customerID').val(validator.getValue('CUSTOMER_ID'));
	$('#customerID_desc').html(validator.getValue('F1_CUSTOMER_NAME'));
	$('#lesseeCode').val(validator.getValue('SUNDRY_DB_AC'));
	$('#productCode').val(validator.getValue('LEASE_PRODUCT_CODE'));
	$('#productCode_desc').html(validator.getValue('F2_DESCRIPTION'));
	$('#branchCode').val(validator.getValue('BRANCH_CODE'));
	$('#branchCode_desc').html(validator.getValue('F3_BRANCH_NAME'));
	$('#addressSerial').val(validator.getValue('ADDR_SL'));
	$('#contactSerial').val(validator.getValue('CONTACT_SL'));
	setCheckbox('interestApplicable', validator.getValue('INTERIM_INT_APPL'));
	$('#interestRateInvestLeaseCommit').val(validator.getValue('INT_RATE_FOR_PRIOR_INVEST'));
	$('#interestBillingChoice').val(validator.getValue('INTERIM_INT_BILLING'));
	$('#interestDebitNotePrinting').val(validator.getValue('INT_DEBIT_NOTE_PRINT_CHOICE'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadAuditFields(validator);
}
