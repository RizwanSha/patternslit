<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.lss.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/lss/iinvpolicy.js" />
	</web:dependencies>
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="iinvpolicy.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/lss/iinvpolicy" id="ipgmcharges" method="POST">
				<web:dividerBlock>
					<type:optionBox add="true" modify="true" view="true" />
				</web:dividerBlock>
				<web:viewContent id="po_view1" styleClass="hidden">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="iinvpolicy.invoicecurrency" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:currencyCode property="invoiceCurrency" id="invoiceCurrency" readOnly="true" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="form.effectivedate" var="common" mandatory="true" />
										</web:column>
										<web:column>
											<type:date property="effectiveDate" id="effectiveDate" lookup="true" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle width="260px" />
										<web:columnStyle />
									</web:columnGroup>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
								</web:table>
							</web:section>
							<web:viewContent id="iinvpolicy_GridView">
								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="200px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:viewTitle var="program" key="iinvpolicy.section1" />
										<web:rowEven>
											<web:column>
												<web:legend key="iinvpolicy.uptoinvoiceamount" var="program" />
											</web:column>
											<web:column>
												<type:currencySmallAmount property="uptoInvoiceAmount" id="uptoInvoiceAmount" />
												<br>
												<web:legend key="iinvpolicy.leaveblank" var="program" />
											</web:column>
										</web:rowEven>
										<web:rowOdd>
											<web:column>
												<web:legend key="iinvpolicy.numberofsignatures" var="program" />
											</web:column>
											<web:column>
												<type:combo property="numberOfSignatures" id="numberOfSignatures" datasourceid="IINVPOLICY_NOF_SIGN" />
											</web:column>
										</web:rowOdd>
										<web:rowEven>
											<web:column>
												<web:legend key="iinvpolicy.signaturetype" var="program" />
											</web:column>
											<web:column>
												<type:combo property="signatureType" id="signatureType" datasourceid="IINVPOLICY_SIGN_TYPE" />
											</web:column>
										</web:rowEven>
									</web:table>
								</web:section>
								<web:section>
									<web:table>
										<web:rowOdd>
											<web:column>
												<web:gridToolbar gridName="iinvpolicy_innerGrid" addFunction="iinvpolicy_addGrid" editFunction="iinvpolicy_editGrid" cancelFunction="iinvpolicy_cancelGrid" insertAtFirsstNotReq="true" />
												<web:element property="xmliinvpolicyGrid" />
											</web:column>
										</web:rowOdd>
										<web:rowEven>
											<web:column>
												<web:grid height="240px" width="600px" id="iinvpolicy_innerGrid" src="lss/iinvpolicy_innerGrid.xml">
												</web:grid>
											</web:column>
										</web:rowEven>
									</web:table>
								</web:section>
							</web:viewContent>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="form.enabled" var="common" />
										</web:column>
										<web:column>
											<type:checkbox property="enabled" id="enabled" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="form.remarks" var="common" />
										</web:column>
										<web:column>
											<type:remarks property="remarks" id="remarks" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column span="2">
											<web:submitReset />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
						</web:sectionBlock>
					</web:dividerBlock>
				</web:viewContent>
				<web:element property="command" />
				<web:element property="action" />
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
	<web:queryGrid />
	<web:TBAGrid />
	<web:contextSearch />
</web:fragment>
