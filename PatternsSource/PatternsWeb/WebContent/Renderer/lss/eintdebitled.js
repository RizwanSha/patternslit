var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'EINTDEBITLED';
function init() {
	clearFields();
	setFocusLast('intMonth');
	if (_redisplay) {

	}
}

function clearFields() {
	$('#intMonth').prop('selectedIndex', 0);
	// $('#intMonthYear').val(EMPTY_STRING);
	$('#intMonth_error').html(EMPTY_STRING);
	$('#leaseProdCode').val(EMPTY_STRING);
	$('#leaseProdCode_error').html(EMPTY_STRING);
	$('#lesseeCode').val(EMPTY_STRING);
	$('#lesseeCode_error').html(EMPTY_STRING);
	eintdebitled_innerGrid.clearAll();
	hideAllMessages(getProgramID());
	hideAllMessages('component');
}

function clearError() {
	$('#intMonth_error').html(EMPTY_STRING);
	$('#leaseProdCode_error').html(EMPTY_STRING);
	$('#lesseeCode_error').html(EMPTY_STRING);
	hideAllMessages(getProgramID());
}

function clearNonPKFields() {

}

function doclearfields(id) {

}

function doHelp(id) {
	switch (id) {

	case 'leaseProdCode':
		help('COMMON', 'HLP_LEASE_PROD_CODE', $('#leaseProdCode').val(), EMPTY_STRING, $('#leaseProdCode'));
		break;
	case 'lesseeCode':
		help('COMMON', 'HLP_DEBTOR_CODE', $('#lesseeCode').val(), EMPTY_STRING, $('#lesseeCode'));
		break;

	}
}

function validate(id) {
	switch (id) {

	case 'intMonth':
		intMonth_val();
		break;

	case 'intMonthYear':
		intMonthYear_val();
		break;

	case 'leaseProdCode':
		leaseProdCode_val();
		break;

	case 'lesseeCode':
		lesseeCode_val();
		break;

	}
}

function backtrack(id) {
	switch (id) {

	case 'intMonthYear':
		setFocusLast('intMonth');
		break;
	case 'leaseProdCode':
		setFocusLast('intMonthYear');
		break;
	case 'lesseeCode':
		setFocusLast('leaseProdCode');
		break;
	case 'submit':
		setFocusLast('lesseeCode');
		break;
	}
}

function intMonth_val() {
	var value = $('#intMonth').val();
	clearError('intMonth_error');
	if (isEmpty(value)) {
		setError('intMonth_error', MANDATORY);
		return false;
	}
	setFocusLast('intMonthYear');
	return true;
}

function intMonthYear_val() {
	var value = $('#intMonthYear').val();
	clearError('intMonth_error');
	if (isEmpty(value)) {
		$('#intMonthYear').val(getCalendarYear(getCBD()));
		value = $('#intMonthYear').val();
	}
	if (!isValidYear(value)) {
		setError('intMonth_error', INVALID_YEAR);
		return false;
	}
	if (value>getCalendarYear(getCBD())) {
		setError('intMonth_error', YEAR_LE_CALYEAR);
		return false;
	}
	
	setFocusLast('leaseProdCode');
	return true;
}

function leaseProdCode_val() {
	var value = $('#leaseProdCode').val();
	clearError('leaseProdCode_error');
	if (!isEmpty(value)) {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('LEASE_PRODUCT_CODE', value);
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.lss.eintdebitledbean');
		validator.setMethod('validateLeaseProductCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('leaseProdCode_error', validator.getValue(ERROR));
			return false;
		}
		if (validator.getValue(RESULT) == DATA_AVAILABLE) {
			$('#leaseProdCode_desc').html(validator.getValue('DESCRIPTION'));
		}
	}
	setFocusLast('lesseeCode');
	return true;
}

function lesseeCode_val() {
	var value = $('#lesseeCode').val();
	clearError('lesseeCode_error');
	if (!isEmpty(value)) {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('SUNDRY_DB_AC', value);
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.lss.eintdebitledbean');
		validator.setMethod('validateLesseeCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('lesseeCode_error', validator.getValue(ERROR));
			return false;
		}
		if (validator.getValue(RESULT) == DATA_AVAILABLE) {
			$('#lesseeCode_desc').html(validator.getValue('CUSTOMER_NAME'));
		}
	}
	setFocusLast('submit');
	return true;
}

function processAction() {
	clearError();
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('intMonth', $('#intMonth').val());
	validator.setValue('intMonthYear', $('#intMonthYear').val());
	validator.setValue('leaseProdCode', $('#leaseProdCode').val());
	validator.setValue('lesseeCode', $('#lesseeCode').val());
	validator.setValue(ACTION, USAGE);
	validator.setClass('patterns.config.web.forms.lss.eintdebitledbean');
	validator.setMethod('processAction');
	validator.sendAndReceiveAsync(checkResult);
	$('#po_viewSubmit').addClass('hidden');
	showMessage(getProgramID(), Message.PROGRESS);
}

function checkResult() {
	/*
	 * alert(validator.getValue(ERROR)); alert(validator.getValue(ERROR_FIELD));
	 * alert(validator.getValue(RESULT)); alert(validator.getValue('_AI'));
	 */
	if (validator.getValue(ERROR) != EMPTY_STRING) {

		if (validator.getValue(ERROR_FIELD) != EMPTY_STRING) {
			hideAllMessages(getProgramID());
			setError(validator.getValue(ERROR_FIELD) + '_error', validator.getValue(ERROR));
		} else {
			showMessage(getProgramID(), Message.ERROR, validator.getValue(ERROR));
		}

	} else {
		var msg = validator.getValue(RESULT);
		if (validator.getValue(ADDITIONAL_INFO) != EMPTY_STRING) {
			msg = msg + ' - ' + validator.getValue(ADDITIONAL_INFO);
		}
		if (validator.getValue(STATUS) == SUCCESS) {
			showMessage(getProgramID(), Message.SUCCESS, msg);
		} else {
			showMessage(getProgramID(), Message.ERROR, msg);
		}

	}
	$('#po_viewSubmit').removeClass('hidden');

}

function showResultGrid(tmpSerial) {
	eintdebitled_innerGrid.clearAll();
	win = showWindow('view_dtl', EMPTY_STRING, LEDGER_DETAILS, EMPTY_STRING, true, false, false, 1360, 360);
	showMessage('component', Message.PROGRESS);
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('TMP_SERIAL', tmpSerial);
	validator.setClass('patterns.config.web.forms.lss.eintdebitledbean');
	validator.setMethod('loadLedgerDetails');
	validator.sendAndReceiveAsync(showLedgerGrid);
}

function showLedgerGrid() {
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		showMessage('component', Message.ERROR, validator.getValue(ERROR));
		return false;
	}
	if (validator.getValue(RESULT) == DATA_UNAVAILABLE) {
		showMessage('component', Message.ERROR, NO_RECORD);
		return false;
	}
	if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		eintdebitled_innerGrid.clearAll();
		eintdebitled_innerGrid.loadXMLString(validator.getValue(RESULT_XML));
		hideMessage('component');
	}
}
