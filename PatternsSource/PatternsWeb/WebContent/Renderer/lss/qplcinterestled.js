validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'QPLCINTERESTLED';
function init() {
	clearFields();
	qplcinterestled_innerGrid.clearAll();
	setFocusLast('preLeaseRefDate');
	if (!isEmpty(_primaryKey)) {
		pkvalues = _primaryKey.split(PK_SEPERATOR);
		$('#preLeaseRefDate').val(pkvalues[1]);
		$('#preLeaseRefSerial').val(pkvalues[2]);
		$('#fromMonth').val(pkvalues[3]);
		$('#fromMonthYear').val(pkvalues[4]);
		$('#toMonth').val(pkvalues[5]);
		$('#toMonthYear').val(pkvalues[6]);
		preLeaseRefSerial_val();
		loadGridValues(pkvalues);
		$('#preLeaseRefDate').prop('disabled', true);
		$('#preLeaseRefSerial').prop('disabled', true);
		$('#fromMonth').prop('disabled', true);
		$('#fromMonthYear').prop('disabled', true);
		$('#toMonth').prop('disabled', true);
		$('#toMonthYear').prop('disabled', true);
		hide('hideSubmitt');
		
	} else {
		$('#preLeaseRefDate').prop('disabled', false);
		$('#preLeaseRefSerial').prop('disabled', false);
		$('#fromMonth').prop('disabled', false);
		$('#fromMonthYear').prop('disabled', false);
		$('#toMonth').prop('disabled', false);
		$('#toMonthYear').prop('disabled', false);
		show('hideSubmitt');
	}
}
function clearFields() {
	setFocusLast('preLeaseRefDate');
	$('#preLeaseRefDate').val(EMPTY_STRING);
	$('#preLeaseRefSerial').val(EMPTY_STRING);
	$('#preLeaseRef_error').html(EMPTY_STRING);
	$('#customerName').val(EMPTY_STRING);
	$('#fromMonth').val($("#fromMonth option:first-child").val());
	$('#fromMonthYear').val(EMPTY_STRING);
	$('#toMonth').val($("#toMonth option:first-child").val());
	$('#toMonthYear').val(EMPTY_STRING);
	$('#fromMonth_error').html(EMPTY_STRING);
	$('#toMonth_error').html(EMPTY_STRING);
	hideMessage(CURRENT_PROGRAM_ID);
	qplcinterestled_innerGrid.clearAll();
}

function clearNonPKFields() {
}

function doclearfields(id) {
	switch (id) {
	case 'preLeaseRefDate':
		if (isEmpty($('#preLeaseRefDate').val())
				&& isEmpty($('#preLeaseRefSerial').val())) {
			clearFields();
		}
		break;
	case 'preLeaseRefSerial':
		if (isEmpty($('#preLeaseRefDate').val())
				&& isEmpty($('#preLeaseRefSerial').val())) {
			clearFields();
		}
		break;
	}
}

function doHelp(id) {
	switch (id) {

	case 'preLeaseRef':
	case 'preLeaseRefDate':
		help('COMMON', 'HLP_PRE_LEASE_CONT_REF', $('#preLeaseRefDate').val(),
				$('#preLeaseRefDate').val(), $('#preLeaseRefDate'), null,
				function(gridObj, rowID) {
					$('#preLeaseRefDate').val(
							gridObj.cells(rowID, 0).getValue());
				});
		break;
	case 'preLeaseRefSerial':
		help('COMMON', 'HLP_PRE_LEASE_CONT_REF_SERIAL', $('#preLeaseRefSerial')
				.val(), $('#preLeaseRefDate').val(), $('#preLeaseRefSerial'),
				null, function(gridObj, rowID) {
					$('#preLeaseRefSerial').val(
							gridObj.cells(rowID, 1).getValue());
				});
		break;

	}
}

function backtrack(id) {
	switch (id) {
	case 'preLeaseRefSerial':
		setFocusLast('preLeaseRefDate');
		break;
	case 'fromMonth':
		setFocusLast('preLeaseRefSerial');
		break;
	case 'fromMonthYear':
		setFocusLast('fromMonth');
		break;
	case 'toMonth':
		setFocusLast('fromMonthYear');
		break;
	case 'toMonthYear':
		setFocusLast('toMonth');
		break;
	case 'submit':
		setFocusLast('toMonthYear');
		break;
	}
}

function validate(id) {
	switch (id) {
	case 'preLeaseRefDate':
		preLeaseRefDate_val();
		break;
	case 'preLeaseRefSerial':
		preLeaseRefSerial_val();
		break;
	case 'fromMonth':
		fromMonth_val();
		break;
	case 'fromMonthYear':
		fromMonthYear_val();
		break;
	case 'toMonth':
		toMonth_val();
		break;
	case 'toMonthYear':
		toMonthYear_val();
		break;
	}
}

function preLeaseRefDate_val() {
	var value = $('#preLeaseRefDate').val();
	clearError('preLeaseRef_error');
	if (isEmpty(value)) {
		setError('preLeaseRef_error', MANDATORY);
		return false;
	}
	if (!isDate(value)) {
		setError('preLeaseRef_error', INVALID_DATE);
		return false;
	}
	if (isDateGreater(value, getCBD())) {
		setError('preLeaseRef_error', DATE_LECBD);
		return false;
	}
	setFocusLast('preLeaseRefSerial');
	return true;
}

function preLeaseRefSerial_val() {
	var value = $('#preLeaseRefSerial').val();
	clearError('preLeaseRef_error');
	if (isEmpty(value)) {
		setError('preLeaseRef_error', MANDATORY);
		return false;
	}
	if (!isNumeric(value)) {
		setError('preLeaseRef_error', INVALID_NUMBER);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('CONTRACT_DATE', $('#preLeaseRefDate').val());
	validator.setValue('CONTRACT_SL', value);
	validator.setValue(ACTION, USAGE);
	validator.setClass('patterns.config.web.forms.lss.qplcinterestledbean');
	validator.setMethod('validatePreLeaseRefSerial');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('preLeaseRef_error', validator.getValue(ERROR));
		return false;
	}
	$('#customerID').val(validator.getValue('CUSTOMER_ID'));
	$('#customerName').val(validator.getValue('CUSTOMER_NAME'));

	setFocusLast('fromMonth');
	return true;
}

function fromMonth_val() {
	var value = $('#fromMonth').val();
	clearError('fromMonth_error');
	if (isEmpty(value)) {
		setError('fromMonth_error', MANDATORY);
		return false;
	}
	setFocusLast('fromMonthYear');
	return true;
}

function fromMonthYear_val() {
	var month = getCalendarMonth(getCBD());
	var year = getCalendarYear(getCBD());
	var value = $('#fromMonthYear').val();
	clearError('fromMonth_error');
	if (isEmpty(value)) {
		setError('fromMonth_error', MANDATORY);
		return false;
	}
	if (!isValidYear(value)) {
		setError('fromMonth_error', INVALID_YEAR);
		return false;
	}
	if (parseInt($('#fromMonthYear').val()) > parseInt(year)) {
		setError('toMonth_error', YEAR_GREATER_CBY);
		return false;
	}

	if (parseInt($('#fromMonthYear').val()) == parseInt(year)) {
		if (parseInt($('#fromMonth').val()) < parseInt(month)) {
			setError('toMonth_error', INVALID_MONTH);
			return false;
		}
	}
	setFocusLast('toMonth');
	return true;
}

function toMonth_val() {
	var toMonth = $('#toMonth').val();
	var fromMonth = $('#fromMonth').val();
	clearError('toMonth_error');
	if (isEmpty(toMonth)) {
		setError('toMonth_error', MANDATORY);
		return false;
	}
	if (parseInt($('#fromMonthYear').val()) == parseInt($('#toMonthYear').val())) {
		if (parseInt(toMonth) < parseInt(fromMonth)) {
			setError('toMonth_error', INVALID_MONTH);
			return false;
		}
	}
	setFocusLast('toMonthYear');
	return true;
}

function toMonthYear_val() {
	var month = getCalendarMonth(getCBD());
	var year = getCalendarYear(getCBD());
	var value = $('#toMonthYear').val();
	clearError('toMonth_error');
	if (isEmpty(value)) {
		setError('toMonth_error', MANDATORY);
		return false;
	}
	if (!isValidYear(value)) {
		setError('toMonth_error', INVALID_YEAR);
		return false;
	}
	if (parseInt($('#fromMonthYear').val()) > parseInt(value)) {
		setError('toMonth_error', YEAR_LE_FROMYEAR);
		return false;
	}
	if (parseInt($('#toMonthYear').val()) > parseInt(year)) {
		setError('toMonth_error', YEAR_GREATER_CBY);
		return false;
	}
	if (parseInt($('#fromMonthYear').val()) == parseInt($('#toMonthYear').val())) {
		if (parseInt($('#toMonth').val()) < parseInt($('#fromMonth').val())) {
			setError('toMonth_error', BOTH_MONTH_YEAR_SAME);
			return false;
		}
	}

	if (parseInt($('#toMonthYear').val()) == parseInt(year)) {
		if (parseInt(toMonth) < parseInt(month)) {
			setError('toMonth_error', INVALID_MONTH);
			return false;
		}
	}
	setFocusLast('submit');
	return true;
}

function loadGridValues() {
	showMessage(getProgramID(), Message.PROGRESS);
	clearError('toMonth_error');
	clearError('fromMonth_error');
	clearError('preLeaseRef_error');
	qplcinterestled_innerGrid.clearAll();
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('CONTRACT_DATE', $('#preLeaseRefDate').val());
	validator.setValue('CONTRACT_SL', $('#preLeaseRefSerial').val());
	validator.setValue('FROM_YEAR', $('#fromMonthYear').val());
	validator.setValue('FROM_MONTH', $('#fromMonth').val());
	validator.setValue('UPTO_YEAR', $('#toMonthYear').val());
	validator.setValue('UPTO_MONTH', $('#toMonth').val());

	validator.setValue(ACTION, USAGE);
	validator.setClass('patterns.config.web.forms.lss.qplcinterestledbean');
	validator.setMethod('loadGridValues');
	validator.sendAndReceive();
	if (validator.getValue('ERROR_PRE_DATE') != EMPTY_STRING) {
		setError('preLeaseRef_error', validator.getValue('ERROR_PRE_DATE'));
		hideMessage(getProgramID());
		$('#po_view4').addClass('hidden');
		return false;
	}
	if (validator.getValue('ERROR_PRE_SERIAL') != EMPTY_STRING) {
		setError('preLeaseRef_error', validator.getValue('ERROR_PRE_SERIAL'));
		hideMessage(getProgramID());
		$('#po_view4').addClass('hidden');
		return false;
	}
	if (validator.getValue('ERROR_FROM_MONTH') != EMPTY_STRING) {
		setError('fromMonth_error', validator.getValue('ERROR_FROM_MONTH'));
		hideMessage(getProgramID());
		$('#po_view4').addClass('hidden');
		return false;
	}

	if (validator.getValue('ERROR_FROM_YEAR') != EMPTY_STRING) {
		setError('fromMonth_error', validator.getValue('ERROR_FROM_YEAR'));
		hideMessage(getProgramID());
		$('#po_view4').addClass('hidden');
		return false;
	}
	if (validator.getValue('ERROR_UPTO_MONTH') != EMPTY_STRING) {
		setError('toMonth_error', validator.getValue('ERROR_UPTO_MONTH'));
		hideMessage(getProgramID());
		$('#po_view4').addClass('hidden');
		return false;
	}
	if (validator.getValue('ERROR_UPTO_YEAR') != EMPTY_STRING) {
		setError('toMonth_error', validator.getValue('ERROR_UPTO_YEAR'));
		hideMessage(getProgramID());
		$('#po_view4').addClass('hidden');
		return false;
	}
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('preLeaseRef_error', validator.getValue(ERROR));
		return false;
	}
	if (validator.getValue(RESULT) == DATA_UNAVAILABLE) {
		showMessage(getProgramID(), Message.WARN, NO_RECORD);
		$('#po_view4').addClass('hidden');
		return false;
	}
	hideMessage(CURRENT_PROGRAM_ID);
	xmlString = validator.getValue(RESULT_XML);
	qplcinterestled_innerGrid.loadXMLString(xmlString);
	return true;
}

function qpreleasecontreg() {
	var pkValue = getEntityCode() + PK_SEPERATOR + $('#preLeaseRefDate').val()
			+ PK_SEPERATOR + $('#preLeaseRefSerial').val();
	window.scroll(0, 0);
	showWindow('qpreleasecontreg', getBasePath()
			+ 'Renderer/lss/qpreleasecontreg.jsp', QUERY_CONTRACT_DETAILS, PK
			+ '=' + pkValue + "&" + SOURCE + "=" + MAIN, true, false, false,
			950, 550);
	resetLoading();
}
