var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'QPLCDETAILVIEW';

function init() {
	hide('viewInvoiceDetail');
	hide('viewInterimInterestDetail');
	hide('viewPaymentDetail');
	clearFields();
	setFocusLast('preLeaseReferenceDate');
}

function doclearfields(id) {
	switch (id) {
	case 'preLeaseReferenceDate':
		if (isEmpty($('#preLeaseReferenceDate').val())) {
			$('#preLeaseReference_error').html(EMPTY_STRING);
			$('#preLeaseReferenceSerial').val(EMPTY_STRING);
			clearNonPKFields();
			break;
		}
	case 'preLeaseReferenceSerial':
		if (isEmpty($('#preLeaseReferenceSerial').val())) {
			$('#preLeaseReference_error').html(EMPTY_STRING);
			clearNonPKFields();
			break;
		}
	}
}

function clearFields() {
	$('#preLeaseReferenceDate').val(EMPTY_STRING);
	$('#preLeaseReferenceSerial').val(EMPTY_STRING);
	$('#preLeaseReference_error').html(EMPTY_STRING);
	clearNonPKFields();
	if (!isEmpty($('#preLeaseContractRefParam').val())) {
		var refKey = $('#preLeaseContractRefParam').val().split(PK_SEPERATOR);
		$('#preLeaseReferenceDate').val(refKey[1]);
		$('#preLeaseReferenceSerial').val(refKey[2]);
		$('#preLeaseReferenceDate').prop('readonly',true);
		$('#preLeaseReferenceSerial').prop('readonly',true);
		$('#preLeaseReferenceSerial_pic').prop('disabled',true);
		$('#submit').remove();
		$('#reset').remove();
		getPurchaseOrderDetails();
	}
}

function clearNonPKFields() {
	$('#lesseCode').val(EMPTY_STRING);
	$('#customerID').val(EMPTY_STRING);
	$('#customerName').val(EMPTY_STRING);
	$('#leaseAgreementSl').val(EMPTY_STRING);
	$('#scheduleID').val(EMPTY_STRING);
	$('#xmlPODetailGrid').val(EMPTY_STRING);
	$('#xmlInvoiceDetailGrid').val(EMPTY_STRING);
	$('#xmlInterimDetailGrid').val(EMPTY_STRING);
	$('#xmlPaymentDetailGrid').val(EMPTY_STRING);
	$('#totalPOAmount').html(ZERO);
	$('#totalPaidSofar').html(ZERO);
	$('#totalInvoicesNo').html(ZERO);
	$('#preLeaseReferenceDate').prop('readonly',false);
	$('#preLeaseReferenceSerial').prop('readonly',false);
	$('#preLeaseReferenceSerial_pic').prop('disabled',false);
	purchaseOrderDetailGrid.clearAll();
	showInvoiceDetailGrid.clearAll();
	showInterimInterestDetailGrid.clearAll();
	showPaymentDetailGrid.clearAll();
}

function doHelp(id) {
	switch (id) {
	case 'preLeaseReferenceDate':
		help(CURRENT_PROGRAM_ID, 'HLP_PRE_LEASE_REF_NO', EMPTY_STRING, $('#preLeaseReferenceDate').val(), $('#preLeaseReferenceDate'), null, function(gridObj, rowID) {
			$('#preLeaseReferenceDate').val(gridObj.cells(rowID, 0).getValue());
			$('#preLeaseReferenceSerial').val(gridObj.cells(rowID, 1).getValue());
			$('#preLeaseReference_desc').html(gridObj.cells(rowID, 2).getValue());
		});
		break;
	case 'preLeaseReferenceSerial':
		help(CURRENT_PROGRAM_ID, 'HLP_PRE_LEASE_REF_NO', EMPTY_STRING, $('#preLeaseReferenceDate').val(), $('#preLeaseReferenceSerial'), null, function(gridObj, rowID) {
			$('#preLeaseReferenceDate').val(gridObj.cells(rowID, 0).getValue());
			$('#preLeaseReferenceSerial').val(gridObj.cells(rowID, 1).getValue());
			$('#preLeaseReference_desc').html(gridObj.cells(rowID, 2).getValue());
		});
		break;
	}
}

function add() {
}

function modify() {
}

function loadData() {
}

function view(source, primaryKey) {
}

function validate(id) {
	switch (id) {
	case 'preLeaseReferenceDate':
		preLeaseReferenceDate_val();
		break;
	case 'preLeaseReferenceSerial':
		preLeaseReferenceSerial_val();
		break;
	case 'submit':
		getPODetails();
		break;
	}
}

function backtrack(id) {
	switch (id) {
	case 'preLeaseReferenceDate':
		setFocusLast('preLeaseReferenceDate');
		break;
	case 'preLeaseReferenceSerial':
		setFocusLast('preLeaseReferenceDate');
		break;
	case 'submit':
		setFocusLast('preLeaseReferenceSerial');
		break;
	}
}

function getPurchaseOrderDetails(){
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('CONTRACT_DATE', $('#preLeaseReferenceDate').val());
	validator.setValue('CONTRACT_SL', $('#preLeaseReferenceSerial').val());
	validator.setValue(ACTION, USAGE);
	validator.setClass('patterns.config.web.forms.lss.qplcdetailviewbean');
	validator.setMethod('getPreLeaseContractDetails');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('preLeaseReference_error', validator.getValue(ERROR));
		return false;
	} else {
		$('#lesseCode').val(validator.getValue('SUNDRY_DB_AC'));
		$('#customerID').val(validator.getValue('CUSTOMER_ID'));
		$('#customerName').val(validator.getValue('CUSTOMER_NAME'));
		$('#leaseAgreementSl').val(validator.getValue('AGREEMENT_NO'));
		$('#scheduleID').val(validator.getValue('SCHEDULE_ID'));
		getPODetails();
	}
}

function preLeaseReferenceDate_val() {
	var value = $('#preLeaseReferenceDate').val();
	clearError('preLeaseReference_error');
	if (isEmpty(value)) {
		setError('preLeaseReference_error', MANDATORY);
		return false;
	}
	if (!isDate(value)) {
		setError('preLeaseReference_error', HMS_INVALID_DATE);
		return false;
	}
	 if (isDateGreater(value, getCBD())) {
	 setError('preLeaseReference_error', DATE_LECBD);
	 return false;
	 }
	setFocusLast('preLeaseReferenceSerial');
	return true;
}

function preLeaseReferenceSerial_val() {
	var value = $('#preLeaseReferenceSerial').val();
	clearError('preLeaseReference_error');
	if (isEmpty(value)) {
		setError('inPatientYear_error', MANDATORY);
		return false;
	}
	if (isZero(value)) {
		setError('preLeaseReference_error', ZERO_CHECK);
		return false;
	}
	if (!isNumeric(value)) {
		setError('preLeaseReference_error', INVALID_NUMBER);
		return false;
	}
	if (isNegative(value)) {
		setError('preLeaseReference_error', INVALID_NUMBER);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('CONTRACT_DATE', $('#preLeaseReferenceDate').val());
		validator.setValue('CONTRACT_SL', $('#preLeaseReferenceSerial').val());
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.lss.qplcdetailviewbean');
		validator.setMethod('getPreLeaseContractDetails');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('preLeaseReference_error', validator.getValue(ERROR));
			return false;
		} else {
			$('#lesseCode').val(validator.getValue('SUNDRY_DB_AC'));
			$('#customerID').val(validator.getValue('CUSTOMER_ID'));
			$('#customerName').val(validator.getValue('CUSTOMER_NAME'));
			$('#leaseAgreementSl').val(validator.getValue('AGREEMENT_NO'));
			$('#scheduleID').val(validator.getValue('SCHEDULE_ID'));
		}
	}
	setFocusLast('submit');
	return true;
}

function getPODetails() {
	purchaseOrderDetailGrid.clearAll();
	showMessage(getProgramID(), Message.PROGRESS);
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('CONTRACT_DATE', $('#preLeaseReferenceDate').val());
	validator.setValue('CONTRACT_SL', $('#preLeaseReferenceSerial').val());
	validator.setClass('patterns.config.web.forms.lss.qplcdetailviewbean');
	validator.setMethod('getPurchaseOrderDetails');
	validator.sendAndReceiveAsync(showPurchaseOrderDetails);
	return true;
}

function showPurchaseOrderDetails() {
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		showMessage(getProgramID(), Message.ERROR, validator.getValue(ERROR));
	} else {
		var xmlString = EMPTY_STRING;
		if (validator.getValue(RESULT) == DATA_AVAILABLE) {
			xmlString = validator.getValue(RESULT_XML);
			purchaseOrderDetailGrid.loadXMLString(xmlString);
			$('#totalPOAmount').html(validator.getValue('TOTAL_AMOUNT'));
			$('#totalPaidSofar').html(validator.getValue('TOTAL_AMOUNT_PAID_SO_FAR'));
			$('#totalInvoicesNo').html(validator.getValue('TOTAL_INVOICE_COUNT'));
			hideMessage(getProgramID());
		} else {
			showMessage(getProgramID(), Message.WARN, NO_RECORD);
		}
	}

}

function showInvoicesDetails(poSerial) {
	showInvoiceDetailGrid.clearAll();
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('CONTRACT_DATE', $('#preLeaseReferenceDate').val());
	validator.setValue('CONTRACT_SL', $('#preLeaseReferenceSerial').val());
	validator.setValue('PO_SL', poSerial);
	validator.setClass('patterns.config.web.forms.lss.qplcdetailviewbean');
	validator.setMethod('getInvoiceDetails');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		alert(validator.getValue(ERROR));
		return false;
	} else {
		var xmlString = EMPTY_STRING;
		if (validator.getValue(RESULT) == DATA_AVAILABLE) {
			xmlString = validator.getValue(RESULT_XML);
			showInvoiceDetailGrid.loadXMLString(xmlString);
			win = showWindow('showInvoiceDetail', '', TC_INVOICE_DETAILS, 'showInvoiceDetail', true, false, false, '680', '380');
		}
	}
	return true;
}

function showPaymentDetails(poSerial, invoiceSerial) {
	showPaymentDetailGrid.clearAll();
	$('#totalPaymentAmount').html(ZERO);
	$('#totalInterestChargedAmount').html(ZERO);
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('CONTRACT_DATE', $('#preLeaseReferenceDate').val());
	validator.setValue('CONTRACT_SL', $('#preLeaseReferenceSerial').val());
	validator.setValue('PO_SL', poSerial);
	if (invoiceSerial != undefined)
		validator.setValue('INVOICE_SL', invoiceSerial);
	validator.setClass('patterns.config.web.forms.lss.qplcdetailviewbean');
	validator.setMethod('getPaymentDetails');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		alert(validator.getValue(ERROR));
		return false;
	} else {
		var xmlString = EMPTY_STRING;
		if (validator.getValue(RESULT) == DATA_AVAILABLE) {
			xmlString = validator.getValue(RESULT_XML);
			showPaymentDetailGrid.loadXMLString(xmlString);
			$('#totalPaymentAmount').html(validator.getValue('TOTAL_AMOUNT'));
			$('#totalInterestChargedAmount').html(validator.getValue('TOTAL_INTEREST_AMOUNT'));
			win = showWindow('showPaymentDetail', '', TC_PAYMENT_DETAILS, 'showPaymentDetail', true, false, false, '800', '380');
		}
	}
	return true;
}

function showInterimInterestDetails(paymentSerial) {
	showInterimInterestDetailGrid.clearAll();
	$('#totalNoOfDays').html(ZERO);
	$('#totalInterestAmount').html(ZERO);
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('CONTRACT_DATE', $('#preLeaseReferenceDate').val());
	validator.setValue('CONTRACT_SL', $('#preLeaseReferenceSerial').val());
	validator.setValue('PAYMENT_SL', paymentSerial);
	validator.setClass('patterns.config.web.forms.lss.qplcdetailviewbean');
	validator.setMethod('getInterimInterestDetails');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		alert(validator.getValue(ERROR));
		return false;
	} else {
		var xmlString = EMPTY_STRING;
		if (validator.getValue(RESULT) == DATA_AVAILABLE) {
			xmlString = validator.getValue(RESULT_XML);
			showInterimInterestDetailGrid.loadXMLString(xmlString);
			$('#totalNoOfDays').html(validator.getValue('TOTAL_NO_OF_DAYS'));
			$('#totalInterestAmount').html(validator.getValue('TOTAL_INTEREST_AMOUNT'));
			win = showWindow('showInterimInterestDetail', '', TC_INTERIM_INTEREST_DETAILS, 'showInterimInterestDetail', true, false, false, '800', '380');
		}
	}
	return true;
}

function onloadInlinePopUp(popUPId) {
	if (popUPId == 'showInvoiceDetail') {
		show('showInvoiceDetail');
		setFocusLast('closeInvoice');
	} else if (popUPId == 'showInterimInterestDetail') {
		show('showInterimInterestDetail');
		setFocusLast('closeInerim');
	} else if (popUPId == 'showPaymentDetail') {
		show('showPaymentDetail');
		setFocusLast('closePaymentDetail');
	}
	return true;
}

function afterClosePopUp(win) {
	switch (win._idd) {
	case 'showInterimInterestDetail':
		dhxWindows = null;
		win = showWindow('showPaymentDetail', '', TC_PAYMENT_DETAILS, 'showPaymentDetail', true, false, false, '800', '380');
		break;
	case 'showPaymentDetail':
		invSerial = showPaymentDetailGrid.cells2(0, 6).getValue();
		if (!isEmpty(invSerial)) {
			dhxWindows = null;
			win = showWindow('showInvoiceDetail', '', TC_INVOICE_DETAILS, 'showInvoiceDetail', true, false, false, '650', '380');
		}
		break;
	case 'qleasepoinv':
		dhxWindows = null;
		win = showWindow('showInvoiceDetail', '', TC_INVOICE_DETAILS, 'showInvoiceDetail', true, false, false, '650', '380');
		break;
	case 'qleasepoinvpay':
		dhxWindows = null;
		win = showWindow('showPaymentDetail', '', TC_PAYMENT_DETAILS, 'showPaymentDetail', true, false, false, '800', '380');
		break;
	}
}

function viewCustomerDetails() {
	if (!isEmpty($('#customerID').val())) {
		var primaryKey = getEntityCode() + PK_SEPERATOR + $('#customerID').val();
		var params = PK + '=' + primaryKey + '&' + SOURCE + '=' + MAIN;
		showWindow('qcustomerreg', getBasePath() + 'Renderer/reg/qcustomerreg.jsp', VIEW_CUSTOMER_DETAILS, params, true, false, false,'1100');
	} else {
		alert(TC_CUSTOMER_ID_MANDATORY);
		return false;
	}
}

function viewInterimInterestLedgerDetails() {
	if (!isEmpty($('#preLeaseReferenceDate').val()) && !isEmpty($('#preLeaseReferenceSerial').val())) {
		var fromDateRef = new Date(moment($('#preLeaseReferenceDate').val()).format(SCRIPT_DATE_FORMAT));
		var fromMonth = fromDateRef.getMonth() + 1;
		var fromYear = fromDateRef.getFullYear();
		var uptoDateRef = new Date(moment(getCBD()).format(SCRIPT_DATE_FORMAT));
		var uptoMonth = uptoDateRef.getMonth() + 1;
		var uptoYear = uptoDateRef.getFullYear();
		var primaryKey = getEntityCode() + PK_SEPERATOR + $('#preLeaseReferenceDate').val() + PK_SEPERATOR + $('#preLeaseReferenceSerial').val() + PK_SEPERATOR + fromMonth + PK_SEPERATOR + fromYear + PK_SEPERATOR + uptoMonth + PK_SEPERATOR + uptoYear;
		var params = PK + '=' + primaryKey + '&' + SOURCE + '=' + MAIN;
		showWindow('qplcinterestled', getBasePath() + 'Renderer/lss/qplcinterestled.jsp', VIEW_INETERIM_INTERIST_LEDGER_DETAILS, params, true, false, false,'1100');
	} else {
		alert(TC_PRELEASE_REF_MANDATORY);
		return false;
	}
}

function viewLeaseDetails() {
	if (!isEmpty($('#leaseAgreementSl').val())) {
		var primaryKey = getEntityCode() + PK_SEPERATOR + $('#lesseCode').val() + PK_SEPERATOR + $('#leaseAgreementSl').val() + PK_SEPERATOR + $('#scheduleID').val();
		var params = PK + '=' + primaryKey + '&' + SOURCE + '=' + MAIN;
		showWindow('qlease', getBasePath() + 'Renderer/lss/qlease.jsp', VIEW_LEASE_DETAILS, params, true, false, false,'1020');
	} else {
		alert(TC_SCHEDULE_ID_MANDATORY);
		return false;
	}
}

function viewLeasePODetails(poSerial) {
	if (!isEmpty($('#preLeaseReferenceDate').val()) && !isEmpty($('#preLeaseReferenceSerial').val())) {
		var primaryKey = getEntityCode() + PK_SEPERATOR + $('#preLeaseReferenceDate').val() + PK_SEPERATOR + $('#preLeaseReferenceSerial').val() + PK_SEPERATOR + poSerial;
		var params = PK + '=' + primaryKey + '&' + SOURCE + '=' + MAIN;
		showWindow('qleasepo', getBasePath() + 'Renderer/lss/qleasepo.jsp', VIEW_LEASE_PO_DETAILS, params, true, false, false,'1100');
	}
}

function viewLeasePOInvDetails(poSerial, invoiceSerial) {
	if (!isEmpty($('#preLeaseReferenceDate').val()) && !isEmpty($('#preLeaseReferenceSerial').val())) {
		var primaryKey = getEntityCode() + PK_SEPERATOR + $('#preLeaseReferenceDate').val() + PK_SEPERATOR + $('#preLeaseReferenceSerial').val() + PK_SEPERATOR + poSerial + PK_SEPERATOR + invoiceSerial;
		var params = PK + '=' + primaryKey + '&' + SOURCE + '=' + MAIN;
		showWindow('qleasepoinv', getBasePath() + 'Renderer/lss/qleasepoinv.jsp', VEW_LEASE_INVOICE_DETAILS, params, true, false, false,'1100');
	}
}

function viewLeasePOInvPayDetails(paymentSerial) {
	if (!isEmpty($('#preLeaseReferenceDate').val()) && !isEmpty($('#preLeaseReferenceSerial').val())) {
		var primaryKey = getEntityCode() + PK_SEPERATOR + $('#preLeaseReferenceDate').val() + PK_SEPERATOR + $('#preLeaseReferenceSerial').val() + PK_SEPERATOR + paymentSerial;
		var params = PK + '=' + primaryKey + '&' + SOURCE + '=' + MAIN;
		showWindow('qleasepoinvpay', getBasePath() + 'Renderer/lss/qleasepoinvpay.jsp', VEW_LEASE_INVOICE_PAY_DETAILS, params, true, false, false,'1200');
	}
}