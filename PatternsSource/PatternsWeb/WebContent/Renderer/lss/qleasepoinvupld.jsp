<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<web:bundle baseName="patterns.config.web.forms.lss.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/lss/qleasepoinvupld.js" />
	</web:dependencies>
	<web:loadPK />
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="eleasepoinvupld.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:viewContent id="po_view1">
				<web:dividerBlock>
					<web:sectionBlock width="width-100p" align="left">
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="eleasepoinvupld.uploaddatesl" var="program" />
									</web:column>
									<web:column>
										<type:dateDaySLDisplay property="upload" id="upload" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="eleasepoinvupld.preleasecontref" var="program" />
									</web:column>
									<web:column>
										<type:dateDaySLDisplay property="preLeaseContRef" id="preLeaseContRef" />
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle width="210px" />
									<web:columnStyle width="120px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="eleasepoinvupld.lessecode" var="program" />
									</web:column>
									<web:column>
										<type:lesseeCodeDisplay property="lesseCode" id="lesseCode" />
									</web:column>
									<web:column>
										<web:legend key="eleasepoinvupld.customerid" var="program" />
									</web:column>
									<web:column>
										<type:customerIDDisplay property="customerID" id="customerID" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowOdd>
									<web:column>
										<web:legend key="eleasepoinvupld.custname" var="program" />
									</web:column>
									<web:column>
										<type:nameDisplay property="customerName" id="customerName" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="eleasepoinvupld.poserial" var="program" />
									</web:column>
									<web:column>
										<type:POSerialDisplay property="poSerial" id="poSerial" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="eleasepoinvupld.filename" var="program" />
									</web:column>
									<web:column>
										<type:fileNameDisplay id="fileName" property="fileName" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="eleasepoinvupld.fileinventorynumber" var="program" />
									</web:column>
									<web:column>
										<type:fileInventoryDisplay id="fileInventoryNumber" property="fileInventoryNumber" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:viewContent id="detail_view">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
										</web:column>
										<web:column>
											<web:anchorMessage var="program" key="eleasepoinvupld.link" onclick="loadGrid()" style="text-decoration:underline;cursor:pointer;font-size:17px" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
						</web:viewContent>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="form.remarks" var="common" />
									</web:column>
									<web:column>
										<type:remarksDisplay property="remarks" id="remarks" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:viewContent id="view_details" styleClass="hidden">
							<web:section>
								<web:table>
									<web:rowOdd>
										<web:column>
											<web:grid height="280px" width="1150px" id="eleasepoinvupld_detailGrid" src="lss/eleasepoinvupld_detailGrid.xml">
											</web:grid>
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
						</web:viewContent>
					</web:sectionBlock>
					<web:auditDisplay />
				</web:dividerBlock>
			</web:viewContent>
		</web:viewSubPart>
	</web:viewPart>
</web:fragment>
