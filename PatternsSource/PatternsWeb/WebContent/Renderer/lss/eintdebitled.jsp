<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<%@taglib prefix="message" tagdir="/WEB-INF/tags/message"%>
<web:bundle baseName="patterns.config.web.forms.lss.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/lss/eintdebitled.js" />
	</web:dependencies>
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="eintdebitled.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/lss/eintdebitled" id="eintdebitled" method="POST">
				<web:viewContent id="po_view1">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="eintdebitled.intMonthYear" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:yearMonth property="intMonth" id="intMonth" datasourceid="COMMON_MONTHS" />
										</web:column>
									</web:rowEven>

									<web:rowOdd>
										<web:column>
											<web:legend key="eintdebitled.leaseProdCode" var="program" />
										</web:column>
										<web:column>
											<type:leaseProductCode property="leaseProdCode" id="leaseProdCode" />
										</web:column>
									</web:rowOdd>

									<web:rowEven>
										<web:column>
											<web:legend key="eintdebitled.lesseeCode" var="program" />
										</web:column>
										<web:column>
											<type:invoiceTemplateCode property="lesseeCode" id="lesseeCode" />
										</web:column>
									</web:rowEven>

								</web:table>
							</web:section>
							<web:viewContent id="po_viewSubmit">
								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="180px" />
											<web:columnStyle />
										</web:columnGroup>

										<web:rowOdd>
											<web:column>
												<type:button id="submit" key="form.submit" var="common" onclick="processAction()" />
												<type:button id="clear" key="form.clear" var="common" onclick="clearFields()" />
											</web:column>
										</web:rowOdd>
									</web:table>
								</web:section>
							</web:viewContent>
							<web:section>
								<message:messageBox id="eintdebitled" />
							</web:section>
							<web:viewContent id="view_dtl" styleClass="hidden">
								<web:section>
									<message:messageBox id="component" />
								</web:section>
								<web:section>
									<web:table>
										<web:rowOdd>
											<web:column>
												<web:grid height="280px" width="1318px" id="eintdebitled_innerGrid" src="lss/eintdebitled_innerGrid.xml">
												</web:grid>
											</web:column>
										</web:rowOdd>
									</web:table>
								</web:section>
							</web:viewContent>
						</web:sectionBlock>
					</web:dividerBlock>
				</web:viewContent>
				<web:element property="command" />
				<web:element property="action" />
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
	<web:contextSearch />
</web:fragment>
