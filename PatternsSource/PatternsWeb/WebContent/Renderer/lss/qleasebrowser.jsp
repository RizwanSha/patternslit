<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<%@taglib prefix="message" tagdir="/WEB-INF/tags/message"%>
<web:bundle baseName="patterns.config.web.forms.lss.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/lss/qleasebrowser.js" />
	</web:dependencies>
	<web:loadPK />
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="qleasebrowser.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/lss/qleasebrowser" id="qleasebrowser" method="POST">
				<web:viewContent id="po_view1">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:viewContent id="leaseFilters">
								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="200px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowOdd>
											<web:column>
												<web:legend key="qleasebrowser.leaseCode" var="program" />
											</web:column>
											<web:column>
												<type:lesseeCode property="lesseCode" id="lesseCode" />
											</web:column>
										</web:rowOdd>
										<web:rowEven>
											<web:column>
												<web:legend key="qleasebrowser.leaseproductcode" var="program" />
											</web:column>
											<web:column>
												<type:leaseProductCode property="leaseProductCode" id="leaseProductCode" />
											</web:column>
										</web:rowEven>
										<web:rowOdd>
											<web:column>
												<web:legend key="qleasebrowser.shippingstate" var="program" />
											</web:column>
											<web:column>
												<type:stateCode property="shippingState" id="shippingState" />
											</web:column>
										</web:rowOdd>
										<web:rowEven>
											<web:column>
												<web:legend key="qleasebrowser.lessorstate" var="program" />
											</web:column>
											<web:column>
												<type:stateCode property="lessorState" id="lessorState" />
											</web:column>
										</web:rowEven>
										<web:rowOdd>
											<web:column>
												<web:legend key="qleasebrowser.dateofcommencement" var="program" mandatory="true"/>
											</web:column>
											<web:column>
												<type:date property="dateOfCommencement" id="dateOfCommencement" />
											</web:column>
										</web:rowOdd>
										<web:rowEven>
											<web:column>
												<web:legend key="qleasebrowser.invoicecyclenumber" var="program" />
											</web:column>
											<web:column>
												<type:twoDigit property="invoiceCycleNumber" id="invoiceCycleNumber" lookup="true"/>
											</web:column>
										</web:rowEven>
										<web:rowOdd>
											<web:column span="2">
												<type:button var="common" onclick="getLeaseDetails()" key="form.submit" id="submit"/>
												<type:button var="common" key="form.reset" id="reset" onclick="clearFields()"/>
											</web:column>
										</web:rowOdd>
									</web:table>
								</web:section>
							</web:viewContent>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="750px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
										</web:column>
										<web:column>
											<type:button var="program" onclick="viewHideFilters()" key="qleasebrowser.hidefilterlink" id="filter"/>
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<message:messageBox id="qleasebrowser" />
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column span="2">
											<web:viewContent id="po_view4">
												<web:grid paging="true" pagingSize="15" height="520px" width="1182px" id="leaseBrowserGrid" src="lss/qleasebrowser_innerGrid.xml">
												</web:grid>
											</web:viewContent>
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
						</web:sectionBlock>
					</web:dividerBlock>
				</web:viewContent>
				
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
	<web:TBAGrid />
	<web:contextSearch />
</web:fragment>
