<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<%@taglib prefix="resource" tagdir="/WEB-INF/tags/resource"%>
<%@taglib prefix="message" tagdir="/WEB-INF/tags/message"%>
<web:bundle baseName="patterns.config.web.forms.lss.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/lss/elease.js" />
	</web:dependencies>
	<web:viewPart>
		<style>
.fa-arrow-right:hover:before,.fa-arrow-right:focus:before {
	content: "\f061";
	color: #3da0e3;
	font-size: 20px;
}

.fa-arrow-left:hover:before,.fa-arrow-left:focus:before {
	content: "\f060";
	color: #3da0e3;
	font-size: 20px;
}

.rowCountCaption {
	color: #666d73;
	font-size: 12px;
	font-weight: bold;
}
</style>
		<web:viewSubPart>
			<web:programTitle var="program" key="elease.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/lss/elease" id="elease" method="POST">
				<web:dividerBlock>
					<type:optionBox add="true" modify="true" view="true" />
				</web:dividerBlock>
				<web:viewContent id="po_view1">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle width="200px" />
										<web:columnStyle width="100px" />
										<web:columnStyle width="150px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="elease.custLeaseCode" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:lesseeCode property="custLeaseCode" id="custLeaseCode" />
										</web:column>
										<web:column>
											<web:legend key="elease.custID" var="program" />
										</web:column>
										<web:column>
											<type:customerID property="custID" id="custID" readOnly="true" />
										</web:column>
										<web:column>
											<type:otherInformation50Display property="custName" id="custName" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle width="200px" />
										<web:columnStyle width="100px" />
										<web:columnStyle width="150px" />
										<web:columnStyle width="250px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="elease.agreementNo" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:agreementSerial property="agreementNo" id="agreementNo" lookup="true" />
										</web:column>
										<web:column>
											<web:legend key="elease.scheduleID" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:scheduleID property="scheduleID" id="scheduleID" />
										</web:column>
										<web:column>
											<web:legend key="elease.preLeaseRef" var="program" />
										</web:column>
										<web:column>
											<type:dateDaySL property="preLeaseRef" id="preLeaseRef" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<message:messageBox id="elease" />
							</web:section>
							<type:error property="leaseErrors" />
							<type:tabbarBody id="lease_TAB_0">
								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="200px" />
											<web:columnStyle width="440px" />
											<web:columnStyle width="200px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowOdd>
											<web:column>
												<web:legend key="elease.createDate" var="program" mandatory="true" />
											</web:column>
											<web:column>
												<type:date property="createDate" id="createDate" readOnly="true" />
											</web:column>
											<web:column>
												<web:legend key="elease.leaseComDate" var="program" mandatory="true" />
											</web:column>
											<web:column>
												<type:date property="leaseComDate" id="leaseComDate" />
											</web:column>
										</web:rowOdd>
										<web:rowEven>
											<web:column>
												<web:legend key="elease.leaseProduct" var="program" />
											</web:column>
											<web:column>
												<type:leaseProductCode property="leaseProduct" id="leaseProduct" readOnly="true" />
											</web:column>
											<web:column>
												<web:legend key="elease.assetCurrency" var="program" />
											</web:column>
											<web:column>
												<type:currency property="assetCurrency" id="assetCurrency" readOnly="true" />
											</web:column>
										</web:rowEven>
									</web:table>
								</web:section>
								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="200px" />
											<web:columnStyle width="110px" />
											<web:columnStyle width="30px" />
											<web:columnStyle width="30px" />
											<web:columnStyle width="50px" />
											<web:columnStyle width="215px" />
											<web:columnStyle width="200px" />
											<web:columnStyle width="110px" />
											<web:columnStyle width="30px" />
											<web:columnStyle width="30px" />
											<web:columnStyle width="40px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowOdd>
											<web:column>
												<web:legend key="elease.billAddress" var="program" mandatory="true" />
											</web:column>
											<web:column>
												<type:serial property="billAddress" id="billAddress" lookup="true" />
											</web:column>
											<web:column>
												<a onclick="addCustomerAddressDetails('billAddress');"> <i title='Add New Customer Billing Address' id='editaddress' data-toggle='tooltip' class="fa fa-user" style="position: relative; margin-left: -1px; font-size: 21px; color: #3da0e3; cursor: pointer"></i><i class="fa fa-plus" style="font-size: 9px; position: absolute; color: #3da0e3; cursor: pointer"></i></a>
											</web:column>
											<web:column>
												<a onclick="editCustomerAddress('billAddress');"> <i title='Edit Exiting Customer Billing Address' id='address' data-toggle='tooltip' class="fa fa-edit" style="margin-right: 5px; font-size: 21px; margin-left: -1px; color: #3da0e3; cursor: pointer"></i></a>
											</web:column>
											<web:column>
												<a onclick="viewAddress();"> <i title='View Customer Billing Address' id='viewCusAddress' data-toggle='tooltip' class="fa fa-eye" style="margin-left: 1px; font-size: 21px; margin-left: -1px; color: #3da0e3; cursor: pointer"></i></a>
											</web:column>
											<web:column>
												<div class="rowCountCaption" id="billAddressState"></div>
											</web:column>

											<web:column>
												<web:legend key="elease.shipAddress" var="program" mandatory="true" />
											</web:column>
											<web:column>
												<type:serial property="shipAddress" id="shipAddress" lookup="true" />
											</web:column>
											<web:column>
												<a onclick="addCustomerShipAddressDetails('shipAddress');"> <i title='Add New Customer Shipping Address' id='editaddress' data-toggle='tooltip' class="fa fa-user" style="position: relative; margin-left: -1px; font-size: 21px; color: #3da0e3; cursor: pointer"></i><i class="fa fa-plus" style="font-size: 9px; position: absolute; color: #3da0e3; cursor: pointer"></i></a>
											</web:column>
											<web:column>
												<a onclick="editCustomerShipAddress('shipAddress');"> <i title='Edit Exiting Customer Shipping Address' id='address' data-toggle='tooltip' class="fa fa-edit" style="margin-right: 5px; font-size: 21px; margin-left: -1px; color: #3da0e3; cursor: pointer"></i></a>
											</web:column>
											<web:column>
												<a onclick="viewShipAddress();"> <i title='View Customer Shipping Address' id='viewCusShipAddress' data-toggle='tooltip' class="fa fa-eye" style="margin-left: 1px; font-size: 21px; margin-left: -1px; color: #3da0e3; cursor: pointer"></i></a>
											</web:column>
											<web:column>
												<div class="rowCountCaption" id="shipAddressState"></div>
											</web:column>

										</web:rowOdd>
									</web:table>
								</web:section>
								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="200px" />
											<web:columnStyle width="440px" />
											<web:columnStyle width="200px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowEven>
											<web:column>
												<web:legend key="elease.billBranch" var="program" mandatory="true" />
											</web:column>
											<web:column>
												<type:branchCode property="billBranch" id="billBranch" />
											</web:column>
											<web:column>
												<web:legend key="elease.billState" var="program" />
											</web:column>
											<web:column>
												<type:stateCode property="billState" id="billState" readOnly="true" />
											</web:column>
										</web:rowEven>
									</web:table>
								</web:section>
								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="200px" />
											<web:columnStyle width="500px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowOdd>
											<web:column>
												<web:legend key="elease.invType" var="program" />
											</web:column>
											<web:column>
												<type:comboDisplay property="invType" id="invType" datasourceid="COMMON_INVOICE_TYPE" />
											</web:column>
											<web:column align="right">
												<button type="button" class="fa fa-arrow-right " id="nextTab" title='Go to Asset Details Section ' data-toggle='tooltip' style="font-size: 15px; margin-right: 5px; color: #3da0e3; cursor: pointer" onclick="clickNextTab();"></button>
											</web:column>
										</web:rowOdd>
									</web:table>
								</web:section>
							</type:tabbarBody>

							<type:tabbarBody id="lease_TAB_1">
								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="190px" />
											<web:columnStyle width="300px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowEven>
											<web:column>
												<web:legend key="elease.noOfAssets" var="program" mandatory="true" />
											</web:column>
											<web:column>
												<type:number id="noOfAssets" property="noOfAssets" length="6" size="6" />
											</web:column>
											<web:column>
												<web:button id="addAsset" key="elease.addAsset" var="program" onclick="addAssetDetails()" />
												<web:button id="clear" key="elease.clear" var="program" onclick="clearAssetDetails()" />
											</web:column>
										</web:rowEven>
									</web:table>
								</web:section>
								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="200px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowOdd>
											<web:column>
												<web:grid height="240px" width="871px" id="leaseNoOfAssets_Grid" src="lss/leaseNoOfAssets_Grid.xml">
												</web:grid>
												<web:element property="xmlleaseNoOfAssetsGrid" />
											</web:column>
										</web:rowOdd>
									</web:table>
								</web:section>

								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="60px" />
											<web:columnStyle width="500px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowOdd>
											<web:column align="left">
												<button type="button" onclick="clickPreviousTab();" id="previousTab1" title='Go to  Address Details Section ' data-toggle='tooltip' class="fa fa-arrow-left " style="font-size: 15px; margin-right: 5px; color: #3da0e3; cursor: pointer"></button>
											</web:column>
											<web:column align="right">
												<button type="button" onclick="clickNextTab();" id="nextTab1" class="fa fa-arrow-right " title='Go to Tenor Details Section ' data-toggle='tooltip' style="font-size: 15px; margin-right: 5px; color: #3da0e3; cursor: pointer"></button>
											</web:column>
										</web:rowOdd>
									</web:table>
								</web:section>


								<web:viewContent id="noOfAssetReq" styleClass="form-horizontal">
									<web:section>
										<web:table>
											<web:columnGroup>
												<web:columnStyle width="150px" />
												<web:columnStyle width="150px" />
												<web:columnStyle width="200px" />
												<web:columnStyle />
											</web:columnGroup>
											<web:rowEven>
												<web:column>
													<web:legend key="elease.assetSl" var="program" />
												</web:column>
												<web:column>
													<type:serial property="assetSl" id="assetSl" readOnly="true" />
												</web:column>
												<web:column>
													<web:legend key="elease.assetCategory" var="program" mandatory="true" />
												</web:column>
												<web:column>
													<type:schemeCode property="assetCategory" id="assetCategory" />
												</web:column>
											</web:rowEven>
											<web:rowOdd>
												<web:column>
													<web:legend key="elease.autoNonAuto" var="program" />
												</web:column>
												<web:column>
													<type:comboDisplay property="autoNonAuto" id="autoNonAuto" datasourceid="COMMON_AUTO_NON_AUTO" />
												</web:column>
												<web:column>
													<web:legend key="elease.assetComponent" var="program" mandatory="true" />
												</web:column>
												<web:column>
													<type:number property="assetComponent" id="assetComponent" length="6" size="6" />
												</web:column>
											</web:rowOdd>
										</web:table>
									</web:section>
									<web:viewTitle var="program" key="elease.assetDtl" />
									<web:section>
										<web:table>
											<web:columnGroup>
												<web:columnStyle width="150px" />
												<web:columnStyle width="230px" />
												<web:columnStyle width="210px" />
												<web:columnStyle />
											</web:columnGroup>
											<web:rowOdd>
												<web:column>
													<web:legend key="elease.componentSerial" var="program" />
												</web:column>
												<web:column>
													<type:serial property="componentSerial" id="componentSerial" readOnly="true" />
												</web:column>
												<web:column>
													<web:legend key="elease.comAssetID" var="program" />
												</web:column>
												<web:column>
													<type:dynamicDescription property="comAssetID" id="comAssetID" length="35" size="35" />
												</web:column>
											</web:rowOdd>
										</web:table>
									</web:section>
									<web:section>
										<web:table>
											<web:columnGroup>
												<web:columnStyle width="150px" />
												<web:columnStyle />
											</web:columnGroup>
											<web:rowEven>
												<web:column>
													<web:legend key="elease.comAssetDesc" var="program" mandatory="true" />
												</web:column>
												<web:column>
													<type:assetDescription2lines property="comAssetDesc" id="comAssetDesc" />
												</web:column>
											</web:rowEven>
										</web:table>
									</web:section>
									<web:section>
										<web:table>
											<web:columnGroup>
												<web:columnStyle width="150px" />
												<web:columnStyle width="230px" />
												<web:columnStyle width="210px" />
												<web:columnStyle />
											</web:columnGroup>
											<web:rowOdd>
												<web:column>
													<web:legend key="elease.comQty" var="program" mandatory="true" />
												</web:column>
												<web:column>
													<type:number property="comQty" id="comQty" length="3" size="3" />
												</web:column>
												<web:column>
													<web:legend key="elease.comIndidualReq" var="program" />
												</web:column>
												<web:column>
													<type:checkbox property="comIndidualReq" id="comIndidualReq" />
												</web:column>
											</web:rowOdd>
											<web:rowEven>
												<web:column>
													<web:legend key="elease.assetModel" var="program" />
												</web:column>
												<web:column>
													<type:otherInformation25 property="assetModel" id="assetModel" />
												</web:column>
												<web:column>
													<web:legend key="elease.assetConfig" var="program" />
												</web:column>
												<web:column>
													<type:otherInformation25 property="assetConfig" id="assetConfig" />
												</web:column>
											</web:rowEven>
										</web:table>
									</web:section>
									<web:section>
										<web:table>
											<web:columnGroup>
												<web:columnStyle width="200px" />
												<web:columnStyle />
											</web:columnGroup>
											<web:rowOdd>
												<web:column>
													<web:gridToolbar gridName="leaseAssetComponent_Grid" addFunction="leaseAssetComponent_addGrid" editFunction="leaseAssetComponent_editGrid" cancelFunction="leaseAssetComponent_cancelGrid" insertAtFirsstNotReq="true" />
													<web:element property="xmlleaseAssetComponentGrid" />
												</web:column>
											</web:rowOdd>
											<web:rowOdd>
												<web:column>
													<web:grid height="190px" width="1101px" id="leaseAssetComponent_Grid" src="lss/leaseAssetComponent_Grid.xml">
													</web:grid>
												</web:column>
											</web:rowOdd>
										</web:table>
									</web:section>

									<web:section>
										<web:table>
											<web:rowOdd>
												<web:column>
													<p align="center">
														<web:button id="componentNew" key="form.update" var="common" onclick="updateNewComponentDetails()" />
														<web:button id="componentaddNew" key="elease.updateAdd" var="program" onclick="updateAndNewDetails()" />
														<web:button key="form.close" id="close" var="common" onclick="closePopup('0')" />
													</p>
												</web:column>
											</web:rowOdd>
										</web:table>
									</web:section>
								</web:viewContent>
							</type:tabbarBody>

							<type:tabbarBody id="lease_TAB_2">
								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="240px" />
											<web:columnStyle width="250px" />
											<web:columnStyle width="220px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowEven>
											<web:column>
												<web:legend id="tenor" key="elease.tenor" var="program" mandatory="true" />
											</web:column>
											<web:column>
												<type:threeDigitMonth property="tenor" id="tenor" />
											</web:column>
											<web:column>
												<web:legend key="elease.cancelPeriod" var="program" />
											</web:column>
											<web:column>
												<type:threeDigitMonth property="cancelPeriod" id="cancelPeriod" />
											</web:column>
										</web:rowEven>
										<web:rowOdd>
											<web:column>
												<web:legend key="elease.repayFreq" var="program" mandatory="true" />
											</web:column>
											<web:column>
												<type:combo property="repayFreq" id="repayFreq" datasourceid="COMMON_REPAY_FREQUENCY" />
											</web:column>
											<web:column>
												<web:legend key="elease.advArrers" var="program" mandatory="true" />
											</web:column>
											<web:column>
												<type:combo property="advArrers" id="advArrers" datasourceid="COMMON_ADVANCE_ARREARS" />
											</web:column>
										</web:rowOdd>
									</web:table>
								</web:section>

								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="240px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowEven>
											<web:column>
												<web:legend key="elease.firstPaymet" var="program" mandatory="true" />
											</web:column>
											<web:column>
												<type:date property="firstPaymet" id="firstPaymet" />
											</web:column>
										</web:rowEven>
									</web:table>
								</web:section>

								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="240px" />
											<web:columnStyle width="250px" />
											<web:columnStyle width="220px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowOdd>
											<web:column>
												<web:legend key="elease.intFromDate" var="program" />
											</web:column>
											<web:column>
												<type:date property="intFromDate" id="intFromDate" />
											</web:column>
											<web:column>
												<web:legend key="elease.intUptoDate" var="program" />
											</web:column>
											<web:column>
												<type:date property="intUptoDate" id="intUptoDate" />
											</web:column>
										</web:rowOdd>
										<web:rowEven>
											<web:column>
												<web:legend key="elease.finalFromDate" var="program" />
											</web:column>
											<web:column>
												<type:date property="finalFromDate" id="finalFromDate" />
											</web:column>
											<web:column>
												<web:legend key="elease.finalUptoDate" var="program" />
											</web:column>
											<web:column>
												<type:date property="finalUptoDate" id="finalUptoDate" />
											</web:column>
										</web:rowEven>
									</web:table>
								</web:section>

								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="240px" />
											<web:columnStyle width="250px" />
											<web:columnStyle width="220px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowOdd>
											<web:column>
												<web:legend id="rentDate" key="elease.rentDate" var="program" />
											</web:column>
											<web:column>
												<type:date property="rentDate" id="rentDate" readOnly="true" />
											</web:column>
											<web:column>
												<web:legend id="lastDate" key="elease.lastDate" var="program" />
											</web:column>
											<web:column>
												<type:date property="lastDate" id="lastDate" readOnly="true" />
											</web:column>
										</web:rowOdd>
									</web:table>
								</web:section>
								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="60px" />
											<web:columnStyle width="500px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowOdd>
											<web:column align="left">
												<button type="button" onclick="clickPreviousTab();" id="previousTab2" class="fa fa-arrow-left " title='Go to Asset Details Section ' data-toggle='tooltip' style="font-size: 15px; margin-right: 5px; color: #3da0e3; cursor: pointer"></button>
											</web:column>
											<web:column align="right">
												<button type="button" onclick="clickNextTab();" id="nextTab2" class="fa fa-arrow-right " title='Go to Rent Details Section ' data-toggle='tooltip' style="font-size: 15px; margin-right: 5px; color: #3da0e3; cursor: pointer"></button>
											</web:column>
										</web:rowOdd>
									</web:table>
								</web:section>
							</type:tabbarBody>

							<type:tabbarBody id="lease_TAB_3">
								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="200px" />
											<web:columnStyle width="350px" />
											<web:columnStyle width="260px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowOdd>
											<web:column>
												<web:legend id="grossAssetCost" key="elease.grossAssetCost" var="program" mandatory="true" />
											</web:column>
											<web:column>
												<type:currencySmallAmount property="grossAssetCost" id="grossAssetCost" />
											</web:column>
											<web:column>
												<web:legend id="commencementValue" key="elease.commencementValue" var="program" mandatory="true" />
											</web:column>
											<web:column>
												<type:currencySmallAmount property="commencementValue" id="commencementValue" />
											</web:column>
										</web:rowOdd>
									</web:table>
								</web:section>
								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="200px" />
											<web:columnStyle width="350px" />
											<web:columnStyle width="260px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowEven>
											<web:column>
												<web:legend key="elease.leaseRentOption" var="program" mandatory="true" />
											</web:column>
											<web:column>
												<type:combo property="leaseRentOption" id="leaseRentOption" datasourceid="COMMON_RENT_PERIOD_OPTION" />
											</web:column>
											<web:column>
												<web:legend key="elease.assureRVType" var="program" />
											</web:column>
											<web:column>
												<type:comboDisplay property="assureRVType" id="assureRVType" datasourceid="ELEASE_RV_TYPE" />
											</web:column>
										</web:rowEven>
										<web:rowOdd>
											<web:column>
												<web:legend key="elease.assureRVPer" var="program" />
											</web:column>
											<web:column>
												<type:threeDigitTwoDecimalPercentage property="assureRVPer" id="assureRVPer" />
											</web:column>
											<web:column>
												<web:legend key="elease.assureRVAmount" var="program" />
											</web:column>
											<web:column>
												<type:currencySmallAmount property="assureRVAmount" id="assureRVAmount" />
											</web:column>
										</web:rowOdd>
									</web:table>
								</web:section>

								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="200px" />
											<web:columnStyle width="350px" />
											<web:columnStyle width="260px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowEven>
											<web:column>
												<web:legend key="elease.internalRateReturn" var="program" />
											</web:column>
											<web:column>
												<type:threeDigitTwoDecimalPercentage property="internalRateReturn" id="internalRateReturn" />
											</web:column>
											<web:column>
												<web:legend key="elease.dealType" var="program" mandatory="true" />
											</web:column>
											<web:column>
												<type:combo property="dealType" id="dealType" datasourceid="ELEASE_DEAL_TYPE" />
											</web:column>
										</web:rowEven>
									</web:table>
								</web:section>
								<web:section>
									<web:viewTitle var="program" key="elease.rentTaxSection" />
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="200px" />
											<web:columnStyle width="350px" />
											<web:columnStyle width="260px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowOdd>
											<web:column>
												<web:legend key="elease.uptoToner" var="program" />
											</web:column>
											<web:column>
												<type:threeDigitMonth property="uptoToner" id="uptoToner" />
											</web:column>
											<web:column>
												<web:legend key="elease.leaseRentRate" var="program" mandatory="true" />
											</web:column>
											<web:column>
												<type:sixDigitAmount property="leaseRentRate" id="leaseRentRate" />
											</web:column>
										</web:rowOdd>
										<web:rowEven>
											<web:column>
												<web:legend key="elease.rental" var="program" mandatory="true" />
											</web:column>
											<web:column>
												<type:currencySmallAmount property="rental" id="rental" />
											</web:column>
											<web:column>
												<web:legend key="elease.execChags" var="program" />
											</web:column>
											<web:column>
												<type:currencySmallAmount property="execChags" id="execChags" />
											</web:column>
										</web:rowEven>
									</web:table>
								</web:section>
								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="200px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowOdd>
											<web:column>
												<web:gridToolbar gridName="leaseRentalCharges_Grid" addFunction="leaseRentalCharges_addGrid" editFunction="leaseRentalCharges_editGrid" cancelFunction="leaseRentalCharges_cancelGrid" insertAtFirsstNotReq="true" />
												<web:element property="xmlleaseRentalChargesGrid" />
											</web:column>
										</web:rowOdd>
									</web:table>
								</web:section>
								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="600px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowOdd>
											<web:column>
												<web:grid height="150px" width="631px" id="leaseRentalCharges_Grid" src="lss/leaseRentalCharges_Grid.xml">
												</web:grid>
											</web:column>
											<web:column>
												<web:section>
													<web:table>
														<web:columnGroup>
															<web:columnStyle width="210px" />
															<web:columnStyle />
														</web:columnGroup>
														<web:rowEven>
															<web:column>
																<web:legend key="elease.intBrokenRent" var="program" />
															</web:column>
															<web:column>
																<type:currencySmallAmount property="intBrokenRent" id="intBrokenRent" />
															</web:column>
														</web:rowEven>
														<web:rowOdd>
															<web:column>
																<web:legend key="elease.intBrokenExec" var="program" />
															</web:column>
															<web:column>
																<type:currencySmallAmount property="intBrokenExec" id="intBrokenExec" />
															</web:column>
														</web:rowOdd>
														<web:rowEven>
															<web:column>
																<web:legend key="elease.finalBrokenRent" var="program" />
															</web:column>
															<web:column>
																<type:currencySmallAmount property="finalBrokenRent" id="finalBrokenRent" />
															</web:column>
														</web:rowEven>
														<web:rowOdd>
															<web:column>
																<web:legend key="elease.finaBrokenExec" var="program" />
															</web:column>
															<web:column>
																<type:currencySmallAmount property="finaBrokenExec" id="finaBrokenExec" />
															</web:column>
														</web:rowOdd>
													</web:table>
												</web:section>
											</web:column>
										</web:rowOdd>
									</web:table>
								</web:section>



								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="60px" />
											<web:columnStyle width="500px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowOdd>
											<web:column align="left">
												<button type="button" onclick="clickPreviousTab();" id="previousTab3" class="fa fa-arrow-left " title='Go to Tenor Details Section ' data-toggle='tooltip' style="font-size: 15px; margin-right: 5px; color: #3da0e3; cursor: pointer"></button>
											</web:column>
											<web:column align="right">
												<button type="button" onclick="clickNextTab();" id="nextTab3" class="fa fa-arrow-right " title='Go to Financial Details Section ' data-toggle='tooltip' style="font-size: 15px; margin-right: 5px; color: #3da0e3; cursor: pointer"></button>
											</web:column>
										</web:rowOdd>
									</web:table>
								</web:section>
							</type:tabbarBody>



							<type:tabbarBody id="lease_TAB_4">
								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="200px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowOdd>
											<web:column>
												<web:legend key="elease.grossNetTDS" var="program" mandatory="true" />
											</web:column>
											<web:column>
												<type:combo id="grossNetTDS" property="grossNetTDS" datasourceid="ELEASE_TDS_TYPE" />
											</web:column>
											<web:column>
												<web:anchorMessage var="program" key="elease.viewFinancialDtl" onclick="generateScheduleData()" style="text-decoration:underline;cursor:pointer" id="viewFinan" />
											</web:column>
										</web:rowOdd>
									</web:table>
								</web:section>

								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="20px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowEven>
											<web:column>
												<web:button id="addPayment" key="elease.addAsset" var="program" onclick="addPaymentDetails()" />
												<web:button id="clearPayment" key="elease.clear" var="program" onclick="clearPaymentDetails()" />
												<type:error property="paymentErrors" />
											</web:column>
										</web:rowEven>
									</web:table>
								</web:section>
								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="200px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowOdd>
											<web:column>
												<web:grid height="180px" width="1272px" id="leasePaymentDetails_Grid" src="lss/leasePaymentDetails_Grid.xml">
												</web:grid>
												<web:element property="xmlleasePaymentGrid" />
											</web:column>
										</web:rowOdd>
									</web:table>
								</web:section>


								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="200px" />
											<web:columnStyle width="333px" />
											<web:columnStyle width="200px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowEven>
											<web:column>
												<web:legend key="elease.processCode" var="program" mandatory="true" />
											</web:column>
											<web:column>
												<type:productCode id="processCode" property="processCode" />
											</web:column>
											<web:column>
												<web:legend key="elease.invTempCode" var="program" mandatory="true" />
											</web:column>
											<web:column>
												<type:productCode id="invTempCode" property="invTempCode" />
											</web:column>
										</web:rowEven>
										<web:rowOdd>
											<web:column>
												<web:legend key="elease.famDesc" var="program" />
											</web:column>
											<web:column>
												<type:otherInformation25 id="famDesc" property="famDesc" />
											</web:column>
											<web:column>
												<web:legend key="elease.hierarCode" var="program" mandatory="true" />
											</web:column>
											<web:column>
												<type:staffCode id="hierarCode" property="hierarCode" />
											</web:column>
										</web:rowOdd>
										<web:rowEven>
											<web:column>
												<web:legend key="elease.invCycleNo" var="program" mandatory="true" />
											</web:column>
											<web:column>
												<type:twoDigit id="invCycleNo" property="invCycleNo" lookup="true" />
											</web:column>
											<web:column>
												<web:legend key="elease.initCycleNo" var="program" />
											</web:column>
											<web:column>
												<type:twoDigit id="initCycleNo" property="initCycleNo" lookup="true" />
											</web:column>
										</web:rowEven>
										<web:rowOdd>
											<web:column>
												<web:legend key="elease.finalCycleNo" var="program" />
											</web:column>
											<web:column>
												<type:twoDigit id="finalCycleNo" property="finalCycleNo" lookup="true" />
											</web:column>
											<web:column>
												<web:legend key="form.remarks" var="common" />
											</web:column>
											<web:column>
												<type:remarks property="remarks" id="remarks" />
											</web:column>
										</web:rowOdd>
									</web:table>
								</web:section>
								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="60px" />
											<web:columnStyle width="500px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowOdd>
											<web:column align="left">
												<button type="button" onclick="clickPreviousTab();" id="previousTab6" class="fa fa-arrow-left" title='Go to View Applier Tax Section ' data-toggle='tooltip' style="margin-right: 5px; font-size: 15px; color: #3da0e3; cursor: pointer"></button>
											</web:column>
										</web:rowOdd>
									</web:table>
								</web:section>

								<web:viewContent id="paymentDetails" styleClass="form-horizontal">
									<web:section>
										<web:table>
											<web:columnGroup>
												<web:columnStyle width="160px" />
												<web:columnStyle width="230px" />
												<web:columnStyle width="160px" />
												<web:columnStyle />
											</web:columnGroup>
											<web:rowEven>
												<web:column>
													<web:legend key="elease.paymentSerial" var="program" />
												</web:column>
												<web:column>
													<type:serialDisplay property="paymentSerial" id="paymentSerial" />
												</web:column>
												<web:column>
													<web:legend key="elease.paymentUptoTenor" var="program" />
												</web:column>
												<web:column>
													<type:serial id="paymentUptoTenor" property="paymentUptoTenor" />
													<span id="totalTenor_desc" class="level4_description"> </span>
												</web:column>
											</web:rowEven>
											<web:rowOdd>
												<web:column>
													<web:legend key="elease.repaymentMode" var="program" mandatory="true" />
												</web:column>
												<web:column>
													<type:combo property="repaymentMode" id="repaymentMode" datasourceid="ELEASE_REPAY_MODE" />
												</web:column>
												<web:column>
													<web:legend key="elease.ecsIFSC" var="program" />
												</web:column>
												<web:column>
													<type:ifscCode id="ecsIFSC" property="ecsIFSC" />
												</web:column>
											</web:rowOdd>
										</web:table>
									</web:section>
									<web:section>
										<web:table>
											<web:columnGroup>
												<web:columnStyle width="160px" />
												<web:columnStyle />
											</web:columnGroup>
											<web:rowEven>
												<web:column>
													<web:legend key="elease.ecsBank" var="program" />
												</web:column>
												<web:column>
													<type:otherInformation50 id="ecsBank" property="ecsBank" />
												</web:column>
											</web:rowEven>
											<web:rowOdd>
												<web:column>
													<web:legend key="elease.ecsBranch" var="program" />
												</web:column>
												<web:column>
													<type:otherInformation100 id="ecsBranch" property="ecsBranch" />
												</web:column>
											</web:rowOdd>
											<web:rowEven>
												<web:column>
													<web:legend key="elease.cusAccNumber" var="program" />
												</web:column>
												<web:column>
													<type:dynamicDescription id="cusAccNumber" property="cusAccNumber" length="35" size="35" />
												</web:column>
											</web:rowEven>
											<web:rowOdd>
												<web:column>
													<web:legend key="elease.peakAmount" var="program" />
												</web:column>
												<web:column>
													<type:currencySmallAmount id="peakAmount" property="peakAmount" />
												</web:column>
											</web:rowOdd>
										</web:table>
									</web:section>
									<web:section>
										<web:table>
											<web:columnGroup>
												<web:columnStyle width="160px" />
												<web:columnStyle width="230px" />
												<web:columnStyle width="160px" />
												<web:columnStyle />
											</web:columnGroup>
											<web:rowEven>
												<web:column>
													<web:legend key="elease.ecsRefNo" var="program" />
												</web:column>
												<web:column>
													<type:otherInformation25 id="ecsRefNo" property="ecsRefNo" />
												</web:column>
												<web:column>
													<web:legend key="elease.ecsRefDate" var="program" />
												</web:column>
												<web:column>
													<type:date id="ecsRefDate" property="ecsRefDate" />
												</web:column>
											</web:rowEven>
										</web:table>
									</web:section>
									<web:section>
										<web:table>
											<web:rowOdd>
												<web:column>
													<p align="center">
														<web:button id="newPayment" key="form.update" var="common" onclick="updateNewPaymentDetails()" />
														<web:button id="addNewPAyment" key="elease.updateAdd" var="program" onclick="updateAndNewPaymentDetails()" />
														<web:button key="form.close" id="closePayment" var="common" onclick="closePopup('0')" />
													</p>
												</web:column>
											</web:rowOdd>
										</web:table>
									</web:section>
								</web:viewContent>

								<web:viewContent id="showFinancialGrid">

									<type:tabbarBody id="leaseFinance_TAB_0">
										<web:section>
											<web:table>
												<web:columnGroup>
													<web:columnStyle width="240px" />
													<web:columnStyle width="800px" />
													<web:columnStyle />
												</web:columnGroup>
												<web:rowOdd>
													<web:column align="left">
														<a class="fa fa-download  xlsColor" id="xls" style="position: relative; margin-left: 10px; cursor: pointer" onclick="doPrint()"></a>
														<web:anchorMessage var="program" key="elease.download" onclick="doPrint()" style="text-decoration:underline;cursor:pointer" id="xlsFile" />
														<!-- 														<a class="fa fa-file-excel-o  xlsColor" style="cursor: pointer" id="xls" onclick="doPrint()"></a> -->
													</web:column>
													<web:column align="center">
														<div id="valueNegative" style="color: red; align: center; font-size: 22px; font: normal 14px/1 FontAwesome;"></div>
													</web:column>
													<web:column align="right">
														<div id="noOfslab" style="color: #337ab7; font-size: 19px; padding: 0px 15px; font: normal 14px/1 FontAwesome;"></div>
													</web:column>
												</web:rowOdd>
											</web:table>
										</web:section>

										<web:viewContent id="leaseFinanceRentalSchedule">
											<web:section>
												<web:table>
													<web:columnGroup>
														<web:columnStyle width="200px" />
														<web:columnStyle />
													</web:columnGroup>
													<web:rowOdd>
														<web:column>
															<web:grid height="380px" width="1244px" id="financialScheduleOLGrid" src="lss/leaseFinanceRentalSchedule_Grid.xml">
															</web:grid>
														</web:column>
													</web:rowOdd>
												</web:table>
											</web:section>
											<web:section>
												<web:table>
													<web:columnGroup>
														<web:columnStyle width="550px" />
														<web:columnStyle />
													</web:columnGroup>
													<web:rowOdd>
														<web:column align="left">
															<i class="fa fa-info-circle" style="color: #337ab7; font-size: 16px;" aria-hidden="true"> <span> <web:message key="elease.linkTaxInfo" var="program" /></span>
															</i>
														</web:column>
													</web:rowOdd>
												</web:table>
											</web:section>
										</web:viewContent>

										<web:viewContent id="leaseFinanceSchedule">
											<web:section>
												<web:table>
													<web:columnGroup>
														<web:columnStyle width="200px" />
														<web:columnStyle />
													</web:columnGroup>
													<web:rowOdd>
														<web:column>
															<web:grid height="380px" width="1244px" id="financialScheduleNonOLGrid" src="lss/leaseFinanceSchedule_Grid.xml">
															</web:grid>
														</web:column>
													</web:rowOdd>
												</web:table>
											</web:section>
											<web:section>
												<web:table>
													<web:columnGroup>
														<web:columnStyle width="550px" />
														<web:columnStyle />
													</web:columnGroup>
													<web:rowOdd>
														<web:column align="left">
															<i class="fa fa-info-circle" style="color: #337ab7; font-size: 16px;" aria-hidden="true"> <span> <web:message key="elease.linkTaxInfo" var="program" /></span>
															</i>
														</web:column>
													</web:rowOdd>
												</web:table>
											</web:section>
										</web:viewContent>

										<web:viewContent id="editFinancialAmount">
											<web:section>
												<web:table>
													<web:columnGroup>
														<web:columnStyle width="160px" />
														<web:columnStyle width="180px" />
														<web:columnStyle width="130px" />
														<web:columnStyle />
													</web:columnGroup>
													<web:rowOdd>
														<web:column>
															<web:legend key="elease.startDate" var="program" />
														</web:column>
														<web:column>
															<type:dateDisplay property="startDate" id="startDate" />
														</web:column>
														<web:column>
															<web:legend key="elease.endDate" var="program" />
														</web:column>
														<web:column>
															<type:dateDisplay property="endDate" id="endDate" />
														</web:column>
													</web:rowOdd>
												</web:table>
											</web:section>
											<web:section>
												<web:table>
													<web:columnGroup>
														<web:columnStyle width="160px" />
														<web:columnStyle />
													</web:columnGroup>
													<web:rowEven>
														<web:column>
															<web:legend id="totalRentAmt" key="elease.totalRentAmt" var="program" />
														</web:column>
														<web:column>
															<type:currencySmallAmountDisplay property="totalRentAmt" id="totalRentAmt" />
														</web:column>
													</web:rowEven>
													<web:rowOdd>
														<web:column>
															<web:legend id="principalAmt" key="elease.principalAmt" var="program" mandatory="true" />
														</web:column>
														<web:column>
															<type:currencySmallAmount property="principalAmt" id="principalAmt" />
														</web:column>
													</web:rowOdd>
													<web:rowEven>
														<web:column>
															<web:legend id="interestAmt" key="elease.interestAmt" var="program" mandatory="true" />
														</web:column>
														<web:column>
															<type:currencySmallAmount property="interestAmt" id="interestAmt" />
														</web:column>
													</web:rowEven>
												</web:table>
											</web:section>
											<web:section>
												<web:table>
													<web:columnGroup>
														<web:columnStyle width="180px" />
														<web:columnStyle />
													</web:columnGroup>
													<web:rowOdd>
														<web:column>
															<p align="center">
																<web:button id="finAmountEditPrev" key="elease.updateEdit" var="program" onclick="updateAndEditPreviousAmountDetails()" />
																<web:button id="finAmount" key="form.update" var="common" onclick="updateAmountDetails()" />
																<web:button id="finAmountContinue" key="elease.updateAddEdit" var="program" onclick="updateAndAddNewAmountDetails()" />
																<web:button key="form.close" id="closeWin1" var="common" onclick="closePopup('0')" />
															</p>
														</web:column>
													</web:rowOdd>
												</web:table>
											</web:section>
										</web:viewContent>


										<web:viewContent id="taxDetails">
											<web:section>
												<web:table>
													<web:columnGroup>
														<web:columnStyle width="130px" />
														<web:columnStyle width="280px" />
														<web:columnStyle width="150px" />
														<web:columnStyle />
													</web:columnGroup>
													<web:rowOdd>
														<web:column>
															<web:legend key="elease.monthSl" var="program" />
														</web:column>
														<web:column>
															<type:serialDisplay property="monthSl" id="monthSl" />
														</web:column>
														<web:column>
															<web:legend key="elease.stateCode" var="program" />
														</web:column>
														<web:column>
															<type:stateCodeDisplay property="stateCode" id="stateCode" />
														</web:column>
													</web:rowOdd>
													<web:rowEven>
														<web:column>
															<web:legend key="elease.startDate" var="program" />
														</web:column>
														<web:column>
															<type:dateDisplay property="startTaxDate" id="startTaxDate" />
														</web:column>
														<web:column>
															<web:legend key="elease.endDate" var="program" />
														</web:column>
														<web:column>
															<type:dateDisplay property="endTaxDate" id="endTaxDate" />
														</web:column>
													</web:rowEven>
												</web:table>
											</web:section>
											<web:viewContent id="prinsInterestWise">
												<web:section>
													<web:table>
														<web:columnGroup>
															<web:columnStyle width="130px" />
															<web:columnStyle width="280px" />
															<web:columnStyle width="150px" />
															<web:columnStyle />
														</web:columnGroup>
														<web:rowOdd>
															<web:column>
																<web:legend key="elease.principalAmt" var="program" />
															</web:column>
															<web:column>
																<type:currencySmallAmountDisplay property="principalTaxAmt" id="principalTaxAmt" />
															</web:column>
															<web:column>
																<web:legend key="elease.interestAmt" var="program" />
															</web:column>
															<web:column>
																<type:currencySmallAmountDisplay property="interestTaxAmt" id="interestTaxAmt" />
															</web:column>
														</web:rowOdd>
													</web:table>
												</web:section>
											</web:viewContent>
											<web:section>
												<web:table>
													<web:columnGroup>
														<web:columnStyle width="130px" />
														<web:columnStyle width="280px" />
														<web:columnStyle width="150px" />
														<web:columnStyle />
													</web:columnGroup>
													<web:rowEven>
														<web:column>
															<web:legend key="elease.execTaxAmt" var="program" />
														</web:column>
														<web:column>
															<type:currencySmallAmountDisplay property="execTaxAmt" id="execTaxAmt" />
														</web:column>
														<web:column>
															<web:legend key="elease.rentalTaxAmt" var="program" />
														</web:column>
														<web:column>
															<type:currencySmallAmountDisplay property="rentalTaxAmt" id="rentalTaxAmt" />
														</web:column>
													</web:rowEven>
												</web:table>
											</web:section>
											<web:section>
												<web:viewTitle var="program" key="elease.taxWiseDetails" />
												<web:table>
													<web:columnGroup>
														<web:columnStyle width="180px" />
														<web:columnStyle />
													</web:columnGroup>
													<web:rowOdd>
														<web:column>
															<web:grid height="240px" width="971px" id="leaseTaxWiseDetails_Grid" src="lss/leaseTaxWiseDetails_Grid.xml">
															</web:grid>
														</web:column>
													</web:rowOdd>
												</web:table>
											</web:section>
											<web:section>
												<web:table>
													<web:columnGroup>
														<web:columnStyle width="550px" />
														<web:columnStyle />
													</web:columnGroup>
													<tr>
														<web:rowOdd>
															<web:column>
																<i class="fa fa-info-circle" style="color: #337ab7; font-size: 16px;" aria-hidden="true"> <span> <web:message key="elease.linkInfo" var="program" /></span>
																</i>
															</web:column>
															<web:column></web:column>
														</web:rowOdd>
													</tr>
												</web:table>
											</web:section>
											<web:section>
												<web:table>
													<web:columnGroup>
														<web:columnStyle width="180px" />
														<web:columnStyle />
													</web:columnGroup>
													<web:rowOdd>
														<web:column span="2">
															<p align="center">
																<type:button key="form.close" id="closeTax" var="common" onclick="closePopup('0')" />
															</p>
														</web:column>
													</web:rowOdd>
												</web:table>
											</web:section>
										</web:viewContent>
									</type:tabbarBody>

									<type:tabbarBody id="leaseFinance_TAB_1">
										<web:section>
											<web:table>
												<web:columnGroup>
													<web:columnStyle width="200px" />
													<web:columnStyle />
												</web:columnGroup>
												<web:rowEven>
													<web:column>
														<web:legend key="elease.taxScheduleCode" var="program" />
													</web:column>
													<web:column>
														<type:code property="taxScheduleCode" id="taxScheduleCode" readOnly="true" />
													</web:column>
												</web:rowEven>
											</web:table>
										</web:section>
										<web:section>
											<web:table>
												<web:columnGroup>
													<web:columnStyle width="200px" />
													<web:columnStyle />
												</web:columnGroup>
												<web:rowOdd>
													<web:column>
														<web:grid height="350px" width="931px" id="leaseApplicableTax_Grid" src="lss/leaseApplicableTax_Grid.xml">
														</web:grid>
														<web:element property="xmlleaseApplicableTaxGrid" />
													</web:column>
												</web:rowOdd>
											</web:table>
										</web:section>
										<web:section>
											<web:table>
												<web:columnGroup>
													<web:columnStyle width="550px" />
													<web:columnStyle />
												</web:columnGroup>
												<tr>
													<web:rowOdd>
														<web:column>
															<i class="fa fa-info-circle" style="color: red; font-size: 16px; padding-bottom: 10px;" aria-hidden="true"> <span> <web:message key="elease.taxDenoteInfo" var="program" /></span>
															</i>
															<span> </span>
															<i class="fa fa-info-circle" style="color: #337ab7; font-size: 16px;" aria-hidden="true"> <span> <web:message key="elease.linkInfo" var="program" /></span>
															</i>
														</web:column>
														<web:column></web:column>
													</web:rowOdd>
												</tr>
											</web:table>
										</web:section>
									</type:tabbarBody>


									<type:tabbar height="height :500px;" name=" Financial Details$$ View Applied Tax " width="width :1282px;" required="2" id="leaseFinance" selected="0">
									</type:tabbar>

									<web:viewContent id="closeFinGrid">
										<web:section>
											<web:table>
												<web:columnGroup>
													<web:columnStyle width="180px" />
													<web:columnStyle />
												</web:columnGroup>
												<web:rowOdd>
													<web:column span="2">
														<p align="center">
															<type:button key="form.close" id="closeFinanceTax" var="common" onclick="closePopup('0')" />
														</p>
													</web:column>
												</web:rowOdd>
											</web:table>
										</web:section>
									</web:viewContent>

									<web:viewContent id="agreeAndContinue">
										<web:section>
											<web:table>
												<web:columnGroup>
													<web:columnStyle width="50px" />
													<web:columnStyle width="300px" />
													<web:columnStyle width="480px" />
													<web:columnStyle />
												</web:columnGroup>
												<web:row>
													<web:column></web:column>
													<web:column>
														<web:legend key="elease.iAgree" var="program" />
													</web:column>
													<web:column>
														<type:checkbox property="iAgree" id="iAgree" />
													</web:column>
												</web:row>
												<web:row>
													<web:column align="center" span="3">
														<web:button id="iAgreeContinue" key="form.confirm" var="common" onclick="agreeContinue()" />
														<web:button key="form.close" id="closeFinanClose" var="common" onclick="closePopup('0')" />
													</web:column>
												</web:row>
											</web:table>
										</web:section>

									</web:viewContent>

								</web:viewContent>
							</type:tabbarBody>



							<type:tabbar height="height :275px;" name="Address Details $$ Asset Details $$ Tenor Details $$ Rent Details $$ Payment Details " width="width :1300px;" required="7" id="lease" selected="0">
							</type:tabbar>

							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column span="2">
											<web:submitReset />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
						</web:sectionBlock>
					</web:dividerBlock>
				</web:viewContent>
				<web:element property="xmlleaseFinanceScheduleGrid" />
				<web:element property="command" />
				<web:element property="action" />
				<web:element property="shippingState" />
				<web:element property="leaseProductType" />
				<web:element property="assureRVTypeValue" />
				<web:element property="finSerial" />
				<web:element property="spSerial" />
				<web:element property="agreementDate" />
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
	<web:queryGrid />
	<web:TBAGrid />
	<web:contextSearch />
</web:fragment>
