var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'ELEASEPOINVUPLD';
function init() {
	if ($('#fileExtensionListError').val() != null && $('#fileExtensionListError').val() != EMPTY_STRING) {
		alert($('#fileExtensionListError').val());
		window.location.href = getBasePath() + $('#redirectLandingPagePath').val();
		return false;
	}
	refreshQueryGrid();
	refreshTBAGrid();
	if (_redisplay) {
		clearExistingFile();
		hide('uploadSuccess');
		hide('uploadFail');
		hide('view_dtl');
		preLeaseContRefSerial_val(false);
	}
	vault.attachEvent("onFileRemove", function(file) {
		clearExistingFile();
	});
}

function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'ELEASEPOINVUPLD_MAIN');
}

function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}

function view(source, primaryKey) {
	hideParent('lss/qleasepoinvupld', source, primaryKey, 1300);
	resetLoading();
}

function clearFields() {
	$('#uploadDate').val(EMPTY_STRING);
	$('#uploadSerial').val(EMPTY_STRING);
	$('#preLeaseContRefDate').val(EMPTY_STRING);
	$('#preLeaseContRefSerial').val(EMPTY_STRING);
	$('#preLeaseContRef_error').html(EMPTY_STRING);
	$('#uploadSerial').val(EMPTY_STRING);
	$('#upload_error').html(EMPTY_STRING);
	$('#lesseCode').val(EMPTY_STRING);
	$('#customerID').val(EMPTY_STRING);
	$('#customerName').val(EMPTY_STRING);
	$('#poSerial').val(EMPTY_STRING);
	$('#poSerial_error').html(EMPTY_STRING);
	clearNonPKFields();
}

function clearNonPKFields() {
	$('#referenceNo').val(EMPTY_STRING);
	$('#referenceNo_error').html(EMPTY_STRING);
	$('#fileInventoryNumberExit').val(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
	vault.clear();
	clearExistingFile();
}

function clearExistingFile() {
	$('#fileName').val(EMPTY_STRING);
	$('#fileInventoryNumber').val(EMPTY_STRING);
	$('#fileExt').val(EMPTY_STRING);
	$('#uploadFail').html(EMPTY_STRING);
	$('#uploadSuccess').html(EMPTY_STRING);
	$('#fileUploadError').html(EMPTY_STRING);
	show('uploadFile');
	hide('uploadSuccess');
	hide('uploadFail');
}

function doclearfields(id) {
	switch (id) {
	case 'uploadDate':
		if (isEmpty($('#uploadDate').val())) {
			$('#upload_error').html(EMPTY_STRING);
			clearFields();
			break;
		}
	case 'uploadSerial':
		if (isEmpty($('#uploadSerial').val())) {
			$('#uploadSerial').val(EMPTY_STRING);
			$('#upload_error').html(EMPTY_STRING);
			$('#lesseCode').val(EMPTY_STRING);
			$('#customerID').val(EMPTY_STRING);
			$('#customerName').val(EMPTY_STRING);
			$('#poSerial').val(EMPTY_STRING);
			$('#poSerial_error').html(EMPTY_STRING);
			break;
		}
	}
}

function loadData() {
	$('#uploadDate').val(validator.getValue('ENTRY_DATE'));
	$('#uploadSerial').val(validator.getValue('ENTRY_SL'));
	$('#preLeaseContRefDate').val(validator.getValue('CONTRACT_DATE'));
	$('#preLeaseContRefSerial').val(validator.getValue('CONTRACT_SL'));
	$('#lesseCode').val(validator.getValue('F1_SUNDRY_DB_AC'));
	$('#customerID').val(validator.getValue('F1_CUSTOMER_ID'));
	$('#customerName').val(validator.getValue('F2_CUSTOMER_NAME'));
	$('#poSerial').val(validator.getValue('PO_SL'));
	$('#fileName').val(validator.getValue('F3_FILE_NAME'));
	$('#fileInventoryNumber').val(validator.getValue('FILE_INV_NUM'));
	$('#remarks').val(validator.getValue('REMARKS'));
	resetLoading();
}

function add() {
	$('#uploadDate').val(getCBD());
}

function modify() {
}

function doHelp(id) {
	switch (id) {
	case 'preLeaseContRef':
	case 'preLeaseContRefDate':
	case 'preLeaseContRefSerial':
		help('COMMON', 'HLP_PRE_LEASE_CONT_REF', $('#preLeaseContRefSerial').val(), $('#preLeaseContRefDate').val(), $('#preLeaseContRefSerial'), null, function(gridObj, rowID) {
			$('#preLeaseContRefDate').val(gridObj.cells(rowID, 0).getValue());
			$('#preLeaseContRefSerial').val(gridObj.cells(rowID, 1).getValue());
		});
		break;
	case 'poSerial':
		help('ELEASEPOINVUPLD', 'HLP_LEASE_PO_SL', $('#poSerial').val(), $('#preLeaseContRefDate').val() + PK_SEPERATOR + $('#preLeaseContRefSerial').val(), $('#poSerial'));
		break;
	}
}

function validate(id) {
	switch (id) {
	case 'uploadDate':
		uploadDate_val();
		break;
	case 'uploadSerial':
		uploadSerial_val();
		break;
	case 'preLeaseContRefDate':
		preLeaseContRefDate_val();
		break;
	case 'preLeaseContRefSerial':
		preLeaseContRefSerial_val(true);
		break;
	case 'poSerial':
		poSerial_val();
		break;
	case 'remarks':
		remarks_val();
		break;
	}
}

function backtrack(id) {
	switch (id) {
	case 'uploadDate':
		setFocusLast('uploadDate');
		break;
	case 'preLeaseContRefDate':
		setFocusLast('uploadDate');
		break;
	case 'preLeaseContRefSerial':
		setFocusLast('preLeaseContRefDate');
		break;
	case 'poSerial':
		setFocusLast('preLeaseContRefSerial');
		break;
	case 'remarks':
		setFocusLast('poSerial');
		break;
	}
}

function preLeaseContRefDate_val() {
	var value = $('#preLeaseContRefDate').val();
	clearError('preLeaseContRef_error');
	if (isEmpty(value)) {
		setError('preLeaseContRef_error', MANDATORY);
		return false;
	}
	if (!isDate(value)) {
		setError('preLeaseContRef_error', INVALID_DATE);
		return false;
	}
	if (isDateGreater(value, getCBD())) {
		setError('preLeaseContRef_error', DATE_LECBD);
		return false;
	}
	setFocusLast('preLeaseContRefSerial');
	return true;
}

function preLeaseContRefSerial_val(valMode) {
	var value = $('#preLeaseContRefSerial').val();
	clearError('preLeaseContRef_error');
	if (isEmpty(value)) {
		setError('preLeaseContRef_error', MANDATORY);
		return false;
	}
	if (!isNumeric(value)) {
		setError('preLeaseContRef_error', INVALID_NUMBER);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('CONTRACT_DATE', $('#preLeaseContRefDate').val());
	validator.setValue('CONTRACT_SL', value);
	validator.setValue(ACTION, USAGE);
	validator.setClass('patterns.config.web.forms.lss.eleasepoinvupldbean');
	validator.setMethod('validatePreLeaseContRefSerial');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('preLeaseContRef_error', validator.getValue(ERROR));
		return false;
	}
	$('#lesseCode').val(validator.getValue('SUNDRY_DB_AC'));
	$('#customerID').val(validator.getValue('CUSTOMER_ID'));
	$('#customerName').val(validator.getValue('CUSTOMER_NAME'));
	setFocusLast('poSerial');
	return true;
}

function poSerial_val() {
	var value = $('#poSerial').val();
	clearError('poSerial_error');
	if (isEmpty(value)) {
		setError('poSerial_error', MANDATORY);
		return false;
	}
	if (isZero(value)) {
		setError('poSerial_error', ZERO_CHECK);
		return false;
	}
	if (!isNumeric(value)) {
		setError('poSerial_error', NUMERIC_CHECK);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('CONTRACT_DATE', $('#preLeaseContRefDate').val());
		validator.setValue('CONTRACT_SL', $('#preLeaseContRefSerial').val());
		validator.setValue('PO_SL', value);
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.lss.eleasepoinvupldbean');
		validator.setMethod('validatePOSerial');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('poSerial_error', validator.getValue(ERROR));
			return false;
		}
	}
	setFocusLast('remarks');
	return true;
}

function initFileUpload() {
	clearExistingFile();
	vault.setAutoStart(true);
	vault.attachEvent("onFileRemove", function(file) {
		vault.setFilesLimit(1);
	});
}

function beforeFileAdd(file) {
	if ($('#rectify').val() == COLUMN_ENABLE)
		clearExistingFile();
	$('#fileUploadError').html(EMPTY_STRING);
	fileExtension = vault.getFileExtension(file.name);
	var temp = $('#fileExtensionList').val().split(',');
	if ($('#fileInventoryNumber').val() != EMPTY_STRING) {
		alert(PBS_REMOVE_EXISTING_FILE);
		return false;
	}
	if (jQuery.inArray(fileExtension.toLowerCase(), temp) == -1) {
		alert(INVALID_FILE_FORMAT);
		return false;
	}
	return true;
}

function getUploadParameters() {
	param = 'FILE_PATH=' + $('#filePath').val() + '&ALLOWED_EXTENSIONS=' + $('#fileExtensionList').val();
	return param;
}

function afterFileUploadFail(file, extra) {
	alert(extra.param);
	vault.clear();
}

function afterFileUpload(file, extra) {
	$('#fileName').val(file.name);
	$('#fileInventoryNumber').val(extra.invNo);
	$('#fileExt').val(extra.format.toLowerCase());
	if ($('#fileInventoryNumber').val() != EMPTY_STRING) {
		$('#fileInventoryNumberExit').val(EMPTY_STRING);
	}
	setFocusLast('remarks');
}

function verifyUploadFile() {
	var value = $('#fileName').val();
	var format = $('#fileName').val().split('.');
	clearError('fileUploadError');
	if (isEmpty(value)) {
		setError('fileUploadError', UPLOAD_FILE_MANDATORY);
		return false;
	}
	hide('uploadFile');
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('FILE_NAME', value);
	validator.setValue('FILE_FORMAT', format[1]);
	validator.setValue('CONTRACT_DATE', $('#preLeaseContRefDate').val());
	validator.setValue('CONTRACT_SL', $('#preLeaseContRefSerial').val());
	validator.setValue('PO_SL', $('#poSerial').val());
	validator.setValue('SOURCE_KEY', $('#uploadDate').val() + PK_SEPERATOR + $('#uploadSerial').val());
	validator.setValue('PGM_ID', CURRENT_PROGRAM_ID);
	validator.setValue(ACTION, USAGE);
	validator.setClass('patterns.config.web.forms.lss.eleasepoinvupldbean');
	if (format[1] == 'xls')
		validator.setMethod('validateUploadXLSFile');
	else
		validator.setMethod('validateUploadXLSXFile');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('fileUploadError', validator.getValue(ERROR));
		return false;
	}
	if (validator.getValue(RESULT) == DATA_UNAVAILABLE) {
		hide('uploadSuccess');
		show('uploadFail');
		$('#tempSerial').val(validator.getValue("TMP_SERIAL"));
		$('#uploadFail').html(FAILURE_MESSAGE);
		return false;
	} else if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		$('#tempSerial').val(validator.getValue("TMP_SERIAL"));
		$('#uploadSuccess').html(NO_ERROR);
		hide('uploadFail');
		show('uploadSuccess');
	}

}
function viewErrorDetails() {
	eleasepoinvupld_innerGrid.clearAll();
	win = showWindow('view_dtl', EMPTY_STRING, LEASE_INVOICE_UPLOAD_ERROR, EMPTY_STRING, true, false, false, 1300, 500);
	showMessage(getProgramID(), Message.PROGRESS);
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('TMP_SERIAL', $('#tempSerial').val());
	validator.setClass('patterns.config.web.forms.lss.eleasepoinvupldbean');
	validator.setMethod('loadTempDetails');
	validator.sendAndReceiveAsync(showInnerGrid);
}

function showInnerGrid() {
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		showMessage(getProgramID(), Message.ERROR, validator.getValue(ERROR));
		return false;
	}
	if (validator.getValue(RESULT) == DATA_UNAVAILABLE) {
		showMessage(getProgramID(), Message.ERROR, NO_RECORD);
		return false;
	}
	if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		eleasepoinvupld_innerGrid.clearAll();
		eleasepoinvupld_innerGrid.loadXMLString(validator.getValue(RESULT_XML));
		hideMessage(getProgramID());
	}
}

function revalidate() {
	if (isEmpty($('#tempSerial').val())) {
		setError('fileUploadError', VERIFY_MANDATORY);
		errors++;
		return false;
	}
	if (isEmpty($('#fileInventoryNumber').val()) && isEmpty($('#fileInventoryNumberExit').val())) {
		alert(FILE_NOT_YET_UPLOADED);
		errors++;
		return false;
	}
	if (!isEmpty($('#fileInventoryNumber').val())) {
		var temp = $('#fileExtensionList').val().split(',');
		if (jQuery.inArray(fileExtension.toLowerCase(), temp) == -1) {
			alert(INVALID_FILE_FORMAT);
			errors++;
			return false;
		}
	} else if (isEmpty($('#fileInventoryNumberExit').val())) {
		alert(FILE_NOT_YET_UPLOADED);
		errors++;
		return false;
	}
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', INVALID_REMARKS);
			setFocusLast('remarks');
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', INVALID_REMARKS);
			setFocusLast('remarks');
			return false;
		}
	}
	if ($('#rectify').val() == COLUMN_ENABLE) {
		if (isEmpty(value)) {
			setError('remarks_error', MANDATORY);
			setFocusLast('remarks');
			return false;
		}
	}
	setFocusOnSubmit();
	return true;
}
