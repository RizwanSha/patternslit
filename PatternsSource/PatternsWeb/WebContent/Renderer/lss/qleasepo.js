var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'ELEASEPO';
var poCurrency = null;
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}

function loads() {
	eleasepo_innerGrid.setColumnHidden(0, true);
	loadGridQuery(CURRENT_PROGRAM_ID, 'ELEASEPO_GRID', eleasepo_innerGrid);
}

function clearFields() {
	$('#preLeaseContractRefDate').val(EMPTY_STRING);
	$('#preLeaseContractRefSerial').val(EMPTY_STRING);
	$('#lesseCode').val(EMPTY_STRING);
	$('#customerID').val(EMPTY_STRING);
	$('#customerName').val(EMPTY_STRING);
	$('#aggrementNumber').val(EMPTY_STRING);
	$('#scheduleID').val(EMPTY_STRING);
	$('#poSerial').val(EMPTY_STRING);
	$('#poDate').val(EMPTY_STRING);
	$('#poNumber').val(EMPTY_STRING);
	$('#supplierId').val(EMPTY_STRING);
	$('#supplierId_desc').html(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	$('#poAmount').val(EMPTY_STRING);
	$('#poAmountFormat').val(EMPTY_STRING);
	eleasepo_innerGrid.clearAll();
}

function loadData() {
	$('#preLeaseContractRefDate').val(validator.getValue('CONTRACT_DATE'));
	$('#preLeaseContractRefSerial').val(validator.getValue('CONTRACT_SL'));
	$('#lesseCode').val(validator.getValue('F1_SUNDRY_DB_AC'));
	$('#customerID').val(validator.getValue('F1_CUSTOMER_ID'));
	$('#customerName').val(validator.getValue('F2_CUSTOMER_NAME'));
	$('#aggrementNumber').val(validator.getValue('F1_AGREEMENT_NO'));
	$('#scheduleID').val(validator.getValue('F1_SCHEDULE_ID'));
	$('#poSerial').val(validator.getValue('PO_SL'));
	$('#poDate').val(validator.getValue('PO_DATE'));
	$('#poNumber').val(validator.getValue('PO_NUMBER'));
	$('#supplierId').val(validator.getValue('SUPPLIER_ID'));
	$('#supplierId_desc').html(validator.getValue('F3_NAME'));
	$('#poAmount').val(validator.getValue('TOTAL_PO_AMOUNT'));
	$('#poAmount_curr').val(getBaseCurrency());
	$('#remarks').val(validator.getValue('REMARKS'));
	if (isEmpty($('#aggrementNumber').val()) || isEmpty($('#scheduleID').val()))
		hide('scheduleDtl');
	else
		show('scheduleDtl');
	loadAuditFields(validator);
	$('#poAmountFormat').val(formatAmount($('#poAmount').val(), getBaseCurrency()));
	loadGrid();
}

function loadGrid() {
	SOURCE_VALUE = _tableType;
	PK_VALUE = _primaryKey;
	if (_tableType == MAIN) {
		loads();
	} else if (_tableType == TBA) {
		loads();
	}
}
