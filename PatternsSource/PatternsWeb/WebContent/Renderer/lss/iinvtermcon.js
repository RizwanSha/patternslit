var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'IINVTERMCON';
function init() {
	refreshTBAGrid();
	refreshQueryGrid();
	if (_redisplay) {
		if ($('#action').val() == ADD) {
			$('#enabled').prop('disabled', true);
			setCheckbox('enabled', YES);
		}
	}
	setFocusLast('stateCode');
}

function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'IINVTERMCON_MAIN', queryGrid);
}

function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}

function doclearfields(id) {
	switch (id) {
	case 'stateCode':
		if (isEmpty($('#stateCode').val())) {
			clearFields();
			break;
		}
	case 'effectiveDate':
		if (isEmpty($('#effectiveDate').val())) {
			$('#effectiveDate_error').html(EMPTY_STRING);
			clearNonPKFields();
			break;
		}
	}
}

function clearFields() {
	$('#stateCode').val(EMPTY_STRING);
	$('#stateCode_error').html(EMPTY_STRING);
	$('#stateCode_desc').html(EMPTY_STRING);
	$('#effectiveDate').val(EMPTY_STRING);
	$('#effectiveDate_error').html(EMPTY_STRING);
	clearNonPKFields();
}

function clearNonPKFields() {
	$('#termsAndConditions').val(EMPTY_STRING);
	$('#termsAndConditions_error').html(EMPTY_STRING);
	setCheckbox('enabled', YES);
}

function doHelp(id) {
	switch (id) {
	case 'stateCode':
		help('COMMON', 'HLP_STATE_CODE', $('#stateCode').val(), EMPTY_STRING, $('#stateCode'));
		break;
	case 'effectiveDate':
		help('IINVTERMCON', 'HLP_EFF_DATE', $('#effectiveDate').val(), $('#stateCode').val(), $('#effectiveDate'));
		break;
	}
}

function add() {
	$('#effectiveDate').val(getCBD());
	setCheckbox('enabled', YES);
	$('#enabled').prop('disabled', true);
	$('#stateCode').focus();
}

function modify() {
	if ($('#action').val() == MODIFY) {
		$('#effectiveDate').val(EMPTY_STRING);
		$('#enabled').prop('disabled', false);
	} else {
		$('#enabled').prop('disabled', true);
	}
	$('#stateCode').focus();
}

function loadData() {
	$('#stateCode').val(validator.getValue('STATE_CODE'));
	$('#stateCode_desc').html(validator.getValue('F1_DESCRIPTION'));
	$('#effectiveDate').val(validator.getValue('EFF_DATE'));
	$('#termsAndConditions').val(validator.getValue('TERMS_CONDITION'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	resetLoading();
}

function view(source, primaryKey) {
	hideParent('lss/qinvtermcon', source, primaryKey);
	resetLoading();
}

function validate(id) {
	switch (id) {
	case 'stateCode':
		stateCode_val();
		break;
	case 'effectiveDate':
		effectiveDate_val();
		break;
	case 'termsAndConditions':
		termsAndConditions_val();
		break;
	case 'enabled':
		enabled_val();
		break;
	}
}

function backtrack(id) {
	switch (id) {
	case 'stateCode':
		setFocusLast('stateCode');
		break;
	case 'effectiveDate':
		setFocusLast('stateCode');
		break;
	case 'termsAndConditions':
		setFocusLast('effectiveDate');
		break;
	case 'enabled':
		setFocusLast('termsAndConditions');
		break;
	}
}

function stateCode_val() {
	var value = $('#stateCode').val();
	clearError('stateCode_error');
	if (isEmpty(value)) {
		setError('stateCode_error', MANDATORY);
		return false;
	}
	if ($('#action').val() == ADD && !isValidCode(value)) {
		setError('stateCode_error', INVALID_FORMAT);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('STATE_CODE', value);
	validator.setValue(ACTION, $('#action').val());
	validator.setClass('patterns.config.web.forms.lss.iinvtermconbean');
	validator.setMethod('validateStateCode');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('stateCode_error', validator.getValue(ERROR));
		return false;
	}
	$('#stateCode_desc').html(validator.getValue("DESCRIPTION"));
	setFocusLast('effectiveDate');
	return true;
}

function effectiveDate_val() {
	var value = $('#effectiveDate').val();
	clearError('effectiveDate_error');
	if (isEmpty(value)) {
		$('#effectiveDate').val(getCBD());
		value = $('#effectiveDate').val();
	}
	if (!isValidEffectiveDate(value, 'effectiveDate_error')) {
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('STATE_CODE', $('#stateCode').val());
		validator.setValue('EFF_DATE', value);
		validator.setValue(ACTION, $('#action').val());
		validator.setClass('patterns.config.web.forms.lss.iinvtermconbean');
		validator.setMethod('validateEffectiveDate');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('effectiveDate_error', validator.getValue(ERROR));
			return false;
		} else {
			if ($('#action').val() == MODIFY) {
				PK_VALUE = getEntityCode() + PK_SEPERATOR + $('#stateCode').val() + PK_SEPERATOR + $('#effectiveDate').val();
				if (!loadPKValues(PK_VALUE, 'effectiveDate_error')) {
					return false;
				}
			}
		}
	}
	setFocusLast('termsAndConditions');
	return true;
}

function termsAndConditions_val() {
	var value = $('#termsAndConditions').val();
	clearError('termsAndConditions_error');
	if (isEmpty(value)) {
		setError('termsAndConditions_error', MANDATORY);
		return false;
	}
	if ($('#action').val() == ADD) {
		setFocusOnSubmit();
	} else {
		setFocusLast('enabled');
	}
	return true;
}

function enabled_val() {
	setFocusOnSubmit();
	return true;
}
