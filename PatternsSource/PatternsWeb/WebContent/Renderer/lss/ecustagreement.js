var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'ECUSTAGREEMENT';
var cbdval = getCBD();
var dateOfReg = null;
function init() {
	hide('productAgreement_Fields');
	refreshTBAGrid();
	refreshQueryGrid();
	if (_redisplay) {
		ecustagreement_innerGrid.clearAll();
		if (!isEmpty($('#xmlProductAgreementGrid').val())) {
			ecustagreement_innerGrid
					.loadXMLString($('#xmlProductAgreementGrid').val());
		}
		if ($('#action').val() == ADD) {
			$('#agreementSerial').attr('readonly', true);
			$('#agreementSerial_pic').attr("disabled", true);
		} else {
			$('#agreementSerial').attr('readonly', false);
			$('#agreementSerial_pic').attr("disabled", false);
		}
	}
}

function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}

function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'ECUSTAGREEMENT_MAIN');
}

function add() {
	$('#customerID').focus();
	$('#agreementSerial').attr('readonly', true);
	$('#agreementSerial_pic').attr("disabled", true);
}

function modify() {
	$('#customerID').focus();
	$('#agreementSerial').attr('readonly', false);
	$('#agreementSerial_pic').attr("disabled", false);
}

function addProductAgreement() {
	win = showWindow('productAgreement_Fields', EMPTY_STRING,
			PRODUCTS_COVERED_UNDER_THE_AGREEMENT, EMPTY_STRING, true, false,
			false, 550, 150);
}

// function clearProductAgreement() {
// ecustagreement_innerGrid.clearAll();
// clearProductAgreementGrid();
// setFocusLast('');
// }

function loadData() {
	$('#customerID').val(validator.getValue('CUSTOMER_ID'));
	$('#customerID_desc').html(validator.getValue('F1_CUSTOMER_NAME'));
	$('#agreementNo').val(validator.getValue('AGREEMENT_REF_NO'));
	$('#agreementSerial').val(validator.getValue("AGREEMENT_NO"));
	$('#agreementDate').val(validator.getValue('AGREEMENT_DATE'));
	$('#sancRef').val(validator.getValue('SANCTION_REF_NO'));
	$('#sancAmount_curr').val(validator.getValue('SANCTION_CCY'));
	$('#remarks').val(validator.getValue('REMARKS'));
	$('#sancAmount').val(validator.getValue('SANCTION_AMOUNT'));
	$('#sancAmountFormat').val(
			formatAmount($('#sancAmount').val(), $('#sancAmount_curr').val()));
	$('#sancAmount').val(unformatAmount($('#sancAmountFormat').val()));
	loadProductAgreementGrid();
	agreementSerial_val(true);
	resetLoading();
}

function loadProductAgreementGrid() {
	loadGridQuery(CURRENT_PROGRAM_ID, 'ECUSTAGREEMENT_PRODUCT_GRID',
			productAgreementPostProcessing);
}

function productAgreementPostProcessing(result) {
	ecustagreement_innerGrid.parse(result);
	ecustagreement_innerGrid.setColumnHidden(0, true);
	var noofrows = ecustagreement_innerGrid.getRowsNum();
	if (noofrows > 0) {
		for (var i = 0; i < noofrows; i++) {
			var product = ecustagreement_innerGrid.cells2(i, 2).getValue();
			var linkValue = "<a href='javascript:void(0);' onclick=ecustagreement_editGrid('"
					+ product
					+ "')>Edit</a><span>  </span> <a href='javascript:void(0);' onclick=deleteGridRow('"
					+ product + "');>Delete</a>";
			ecustagreement_innerGrid.cells2(i, 1).setValue(linkValue);
			ecustagreement_innerGrid.changeRowId(ecustagreement_innerGrid
					.getRowId(i), product);
		}
	}
}

function view(source, primaryKey) {
	hideParent('lss/qcustagreement', source, primaryKey);
	resetLoading();
}

function clearFields() {
	$('#customerID').val(EMPTY_STRING);
	$('#customerID_desc').html(EMPTY_STRING);
	$('#customerID_error').html(EMPTY_STRING);
	$('#agreementSerial').val(EMPTY_STRING);
	$('#agreementSerial_error').html(EMPTY_STRING);
	$('#agreementSerial_desc').html(EMPTY_STRING);
	clearNonPKFields();
}

function clearNonPKFields() {
	$('#agreementNo').val(EMPTY_STRING);
	$('#agreementNo_error').html(EMPTY_STRING);
	$('#agreementNo_desc').html(EMPTY_STRING);
	$('#agreementDate').val(EMPTY_STRING);
	$('#agreementDate_error').html(EMPTY_STRING);
	$('#sancRef').val(EMPTY_STRING);
	$('#sancRef_error').html(EMPTY_STRING);
	$('#sancAmountFormat').val(EMPTY_STRING);
	$('#sancAmount_error').html(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
	ecustagreement_innerGrid.clearAll();
	clearGridFields();
}

function clearGridFields() {
	$('#productCode').val(EMPTY_STRING);
	$('#productCode_desc').html(EMPTY_STRING);
	$('#productCode_error').html(EMPTY_STRING);
}
// function clearProductAggr() {
// if (confirm(CLEAR_VALUES_CONFIRMATION)) {
// clearGridFields();
// }
// }

function doclearfields(id) {
	switch (id) {
	case 'customerID':
		if (isEmpty($('#customerID').val())) {
			clearFields();
		}
		break;
	case 'agreementSerial':
		if (isEmpty($('#agreementSerial').val())) {
			$('#agreementSerial').val(EMPTY_STRING);
			$('#agreementSerial_error').html(EMPTY_STRING);
			clearNonPKFields();
		}
		break;
	}
}

function doHelp(id) {
	switch (id) {
	case 'customerID':
		help('COMMON', 'HLP_CUSTOMER_ID', $('#customerID').val(), EMPTY_STRING,
				$('#customerID'));
		break;
	case 'agreementSerial':
		if ($('#action').val() == MODIFY) {
			help('COMMON', 'HLP_CUST_AGRMNT_SL', $('#agreementSerial').val(),
					$('#customerID').val(), $('#agreementSerial'));
		}
		break;
	case 'agreementNo':
		help('COMMON', 'HLP_CUST_AGGREEMENT', $('#agreementNo').val(), $(
				'#customerID').val(), $('#agreementNo'), null, function(
				gridObj, rowID) {
			$('#agreementNo').val(gridObj.cells(rowID, 1).getValue());
		});
		break;
	case 'productCode':
		help('COMMON', 'HLP_LEASE_PROD_CODE', $('#productCode').val(),
				EMPTY_STRING, $('#productCode'));
		break;
	}
}

function backtrack(id) {
	switch (id) {
	case 'customerID':
		setFocusLast('customerID');
		break;
	case 'agreementSerial':
		setFocusLast('customerID');
		break;
	case 'agreementNo':
		setFocusLast('customerID');
		break;
	case 'agreementDate':
		setFocusLast('agreementNo');
		break;
	case 'sancRef':
		setFocusLast('agreementDate');
		break;
	case 'sancAmountFormat':
		setFocusLast('sancRef');
		break;
	case 'productAgreement':
		setFocusLast('sancAmountFormat');
		break;
	case 'productCode':
		closeInlinePopUp(win);
		setFocusLast('productAgreement');
		break;
	case 'productDtl':
		setFocusLast('productCode');
		break;
	case 'remarks':
		setFocusLast('productAgreement');
		break;
	}
}

function validate(id) {
	switch (id) {
	case 'customerID':
		customerID_val();
		break;
	case 'agreementSerial':
		agreementSerial_val(false);
		break;
	case 'agreementNo':
		agreementNo_val();
		break;
	case 'agreementDate':
		agreementDate_val();
		break;
	case 'sancRef':
		sancRef_val();
		break;
	case 'sancAmountFormat':
		sancAmountFormat_val();
		break;
	case 'productCode':
		productCode_val();
		break;
	case 'remarks':
		remarks_val();
		break;
	}
}

function customerID_val() {
	var value = $('#customerID').val();
	clearError('customerID_error');
	$('#customerID_desc').html(EMPTY_STRING);
	if (isEmpty(value)) {
		setError('customerID_error', MANDATORY);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('CUSTOMER_ID', $('#customerID').val());
	validator.setValue('ACTION', USAGE);
	validator.setClass('patterns.config.web.forms.lss.ecustagreementbean');
	validator.setMethod('validateCustomerID');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('customerID_error', validator.getValue(ERROR));
		return false;
	} else {
		$('#customerID_desc').html(validator.getValue('CUSTOMER_NAME'));
	}
	if ($('#action').val() == MODIFY)
		setFocusLast('agreementSerial');
	else
		setFocusLast('agreementNo');
	return true;
}

function agreementSerial_val(isEditorView) {
	if ($('#action').val() == MODIFY) {
		var value = $('#agreementSerial').val();
		clearError('agreementSerial_val_error');
		$('#agreementSerial_desc').html(EMPTY_STRING);
		if (isEmpty(value)) {
			setError('agreementSerial_error', MANDATORY);
			return false;
		}
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('CUSTOMER_ID', $('#customerID').val());
		validator.setValue('AGREEMENT_NO', $('#agreementSerial').val());
		validator.setValue('ACTION', USAGE);
		validator.setClass('patterns.config.web.forms.lss.ecustagreementbean');
		validator.setMethod('validateAgreementSerial');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('agreementSerial_error', validator.getValue(ERROR));
			return false;
		} else {
			if (!isEditorView) {
				PK_VALUE = getEntityCode() + PK_SEPERATOR
						+ $('#customerID').val() + PK_SEPERATOR
						+ $('#agreementSerial').val();
				if (!loadPKValues(PK_VALUE, 'agreementSerial_error')) {
					return false;
				}
			}
		}
	}
	setFocusLast('agreementNo');
	return true;
}

function agreementNo_val() {
	var value = $('#agreementNo').val();
	clearError('agreementNo_error');
	if (isEmpty(value)) {
		setError('agreementNo_error', MANDATORY);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('CUSTOMER_ID', $('#customerID').val());
	validator.setValue('AGREEMENT_REF_NO', $('#agreementNo').val());
	validator.setValue('PROGRAM_ID', CURRENT_PROGRAM_ID);
	validator.setValue(ACTION, $('#action').val());
	validator.setClass('patterns.config.web.forms.lss.ecustagreementbean');
	validator.setMethod('validateAgreementNo');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('agreementNo_error', validator.getValue(ERROR));
		return false;
	}
	setFocusLast('agreementDate');
	return true;
}

function agreementDate_val() {
	var value = $('#agreementDate').val();
	clearError('agreementDate_error');
	if (isEmpty(value)) {
		$('#agreementDate').val(cbdval);
		value = $('#agreementDate').val();
	}
	if (!isDate(value)) {
		setError('agreementDate_error', INVALID_DATE);
		return false;
	}
	if (isDateGreater(value, getCBD())) {
		setError('agreementDate_error', DATE_LECBD);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('CUSTOMER_ID', $('#customerID').val());
	validator.setValue('AGREEMENT_DATE', $('#agreementDate').val());
	validator.setValue(ACTION, USAGE);
	validator.setClass('patterns.config.web.forms.lss.ecustagreementbean');
	validator.setMethod('validateAgreementDate');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('agreementDate_error', validator.getValue(ERROR));
		return false;
	}
	dateOfReg = validator.getValue('DATE_OF_REG');
	setFocusLast('sancRef');
	return true;
}

function sancRef_val() {
	var value = $('#sancRef').val();
	clearError('sancRef_error');
	if (!isEmpty(value)) {
		if (!isValidDescription(value)) {
			setError('description_error', INVALID_DESCRIPTION);
			return false;
		}
		if (!isValidOtherInformation25(value)) {
			setError('sancRef_error', INVALID_VALUE);
			return false;
		}
	}
	setFocusLast('sancAmountFormat');
	return true;
}

function sancAmountFormat_val() {
	var value = unformatAmount($('#sancAmountFormat').val());
	clearError('sancAmount_error');
	if (isEmpty(value)) {
		setError('sancAmount_error', MANDATORY);
		return false;
	}
	$('#sancAmount').val(value);
	if (isZero(value)) {
		setError('sancAmount_error', ZERO_CHECK);
		return false;
	}
	if (isNegativeAmount(value)) {
		setError('sancAmount_error', POSITIVE_AMOUNT);
		return false;
	}
	if (!isCurrencySmallAmount($('#sancAmount_curr').val(), value)) {
		setError('sancAmount_error', INVALID_SMALL_AMOUNT);
		return false;
	}
	$('#sancAmountFormat')
			.val(formatAmount(value, $('#sancAmount_curr').val()));
	setFocusLast('productAgreement');
	return true;
}

function productCode_val() {
	var value = $('#productCode').val();
	clearError('productCode_error');
	if (isEmpty(value)) {
		setError('productCode_error', MANDATORY);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('LEASE_PRODUCT_CODE', value);
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.lss.ecustagreementbean');
		validator.setMethod('validateProductCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('productCode_error', validator.getValue(ERROR));
			$('#productCode_desc').html(EMPTY_STRING);
			return false;
		}
		$('#productCode_desc').html(validator.getValue('DESCRIPTION'));
	}
	setFocusLast('productDtl');
	return true;
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', INVALID_REMARKS);
			setFocusLast('remarks');
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', INVALID_REMARKS);
			setFocusLast('remarks');
			return false;
		}
	}
	if ($('#action').val() == MODIFY) {
		if (isEmpty(value)) {
			setError('remarks_error', MANDATORY);
			setFocusLast('remarks');
			return false;
		}
	}
	setFocusOnSubmit();
	return true;
}

function updateProductAgreement() {
	if (ecustagreement_gridDuplicateCheck() && productAgreementGridrevalidate()) {
		var linkValue = "<a href='javascript:void(0);' onclick=ecustagreement_editGrid('"
				+ $('#productCode').val()
				+ "')>Edit</a><span>  </span> <a href='javascript:void(0);' onclick=deleteGridRow('"
				+ $('#productCode').val() + "');>Delete</a>";
		var field_values = [ '', linkValue, $('#productCode').val(),
				$('#productCode_desc').html() ];
		var uid = _grid_updateRow(ecustagreement_innerGrid, field_values);
		ecustagreement_innerGrid.changeRowId(uid, $('#productCode').val());
		clearGridFields();
		closeInlinePopUp(win);
		setFocusLast('productAgreement');
		return true;
	}
}

function ecustagreement_gridDuplicateCheck() {
	var currentValue = [ $('#productCode').val() ];
	if (!_grid_duplicate_check(ecustagreement_innerGrid, currentValue, [ 2 ])) {
		setError('productCode_error', DUPLICATE_DATA);
		return false;
	}
	return true;
}

function deleteGridRow(row) {
	if (confirm(DELETE_CONFIRM)) {
		ecustagreement_innerGrid.setUserData("", "grid_edit_id", EMPTY_STRING);
		ecustagreement_innerGrid.deleteRow(row);
		clearGridFields();
		setFocusLast('productCode');
	}
}

function ecustagreement_editGrid(rowId) {
	clearGridFields();
	addProductAgreement();
	ecustagreement_innerGrid.setUserData("", "grid_edit_id", rowId);
	$('#productCode').val(ecustagreement_innerGrid.cells(rowId, 2).getValue());
	$('#productCode_desc').html(
			ecustagreement_innerGrid.cells(rowId, 3).getValue());
	setFocusLast('productCode');
}

function clearProductAgreement() {
	if (ecustagreement_innerGrid.getRowsNum() > 0) {
		if (confirm(CLEAR_VALUES_CONFIRMATION)) {
			clearGridFields();
			ecustagreement_innerGrid.clearAll();
		}
	}

	// ecustagreement_innerGrid.clearAll();
	// clearGridFields();
	// setFocusLast('productAgreement');
}

function onloadInlinePopUp(param, id) {
	switch (id) {
	case 'productAgreement_Fields':
		clearGridFields();
		show('productAgreement_Fields');
		setFocusLast('productCode');
		break;
	}
}

function gridExit(id) {
	switch (id) {
	case 'productCode':
		if (ecustagreement_innerGrid.getRowsNum() > 0) {
			$('#xmlProductAgreementGrid').val(
					ecustagreement_innerGrid.serialize());
			$('#productCode_error').html(EMPTY_STRING);
			$('#gridError_error').html(EMPTY_STRING);
			closeInlinePopUp(win);
			setFocusLast('remarks');
		} else {
			ecustagreement_innerGrid.clearAll();
			$('#xmlProductAgreementGrid').val(
					ecustagreement_innerGrid.serialize());
			setError('productCode_error', ADD_ATLEAST_ONE_ROW);
			setError('gridError_error', ADD_ATLEAST_ONE_ROW);
			setFocusLast('productCode');
		}
		break;
	case 'productAgreement':
		if (ecustagreement_innerGrid.getRowsNum() > 0) {
			$('#xmlProductAgreementGrid').val(
					ecustagreement_innerGrid.serialize());
			$('#gridError_error').html(EMPTY_STRING);
			setFocusLast('remarks');
		} else {
			ecustagreement_innerGrid.clearAll();
			$('#xmlProductAgreementGrid').val(
					ecustagreement_innerGrid.serialize());
			setError('gridError_error', ADD_ATLEAST_ONE_ROW);
		}
		break;
	case 'addProductAgreement':
		setFocusLast('remarks');
		break;
	}
}

function productAgreementGridrevalidate() {
	var errors = 0;
	if (!productCode_val(false)) {
		errors++;
	}
	if (errors > 0) {
		if (win)
			win.showInnerScroll();
		return false;
	} else
		return true;
}

function revalidate() {
	revaildateGrid();
}
function revaildateGrid() {
	if (ecustagreement_innerGrid.getRowsNum() > 0) {
		ecustagreement_innerGrid.setSerializationLevel(false, false, false,
				false, false, true);
		$('#xmlProductAgreementGrid').val(ecustagreement_innerGrid.serialize());
		$('#productCode_error').html(EMPTY_STRING);
		$('#gridError_error').html(EMPTY_STRING);
	} else {
		ecustagreement_innerGrid.clearAll();
		$('#xmlProductAgreementGrid').val(ecustagreement_innerGrid.serialize());
		setError('productCode_error', ADD_ATLEAST_ONE_ROW);
		setError('gridError_error', ADD_ATLEAST_ONE_ROW);
		errors++;
	}
}

function closePopup(value) {
	if (value == 0) {
		closeInlinePopUp(win);
		setFocusLast('productAgreement');
	}
	return true;
}
