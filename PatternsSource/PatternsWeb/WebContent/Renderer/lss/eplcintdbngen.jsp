<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<%@taglib prefix="message" tagdir="/WEB-INF/tags/message"%>
<web:bundle baseName="patterns.config.web.forms.lss.Messages" var="program" />
<web:fragment>
	<style>
.xlsColor {
	color: #008000;
}

.xlsColor:hover {
	color: #008000;
}
</style>
	<web:dependencies>
		<web:script src="Renderer/lss/eplcintdbngen.js" />
	</web:dependencies>
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="eplcintdbngen.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/lss/eplcintdbngen" id="eplcintdbngen" method="POST">
				<web:viewContent id="po_view1">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="eplcintdbngen.monthyear" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:yearMonth property="leaseMonth" id="leaseMonth" datasourceid="COMMON_MONTHS" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:rowOdd>
										<web:column>
											<type:button id="submit" key="form.submit" var="common" onclick="loadPreLeaseContractInterest();" />
											<type:button key="form.reset" id="reset" var="common" onclick="clearFields();" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<message:messageBox id="eplcintdbngen" />
							</web:section>
							<web:section>
								<web:viewTitle var="program" key="eplcintdbngen.leasecontract" />
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="160px" />
										<web:columnStyle width="750px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<type:button key="form.approve" id="authorize" var="common" onclick="doAuthorize()" />
											<type:button key="form.view" id="view" var="common" onclick="doView()" />
										</web:column>
										<web:column>
										</web:column>
										<web:column>
											<div id="xlsDiv" onclick="doPrint()">
												<a id="xlsFile" class="xlsColor"> <web:legend id="xls" key="eplcintdbngen.xls" var="program" /></a> <a class="fa fa-file-excel-o fa-2x xlsColor" id="xls"></a>
											</div>
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:rowEven>
										<web:section>
											<message:messageBox id="eplcintdbngen" />
										</web:section>
										<web:column>
											<web:grid height="290px" width="1040px" id="eplcintdbngen_innerGrid" src="lss/eplcintdbngen_innerGrid.xml" paging="true">
											</web:grid>
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:viewContent id="ledger_dtl" styleClass="hidden">
								<web:section>
									<message:messageBox id="ledgerDetails" />
								</web:section>
								<web:section>
									<web:table>
										<web:rowOdd>
											<web:column>
												<web:grid height="280px" width="1155px" id="eplcintdbngen_ledgerGrid" src="lss/eplcintdbngen_ledgerGrid.xml">
												</web:grid>
											</web:column>
										</web:rowOdd>
									</web:table>
								</web:section>
							</web:viewContent>
						</web:sectionBlock>
					</web:dividerBlock>
				</web:viewContent>
				<web:element property="command" />
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
	<web:contextSearch />
</web:fragment>