
<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.lss.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/lss/eleasepoinv.js" />
	</web:dependencies>
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="eleasepoinv.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/lss/eleasepoinv" id="eleasepoinv" method="POST">
				<web:dividerBlock>
					<type:optionBox add="true" modify="false" view="true" />
				</web:dividerBlock>
				<web:viewContent id="po_view1" styleClass="hidden">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="eleasepoinv.preleaseref" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:dateDaySL property="preLeaseRef" id="preLeaseRef" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle width="180px" />
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="eleasepoinv.leasecode" var="program" />
										</web:column>
										<web:column>
											<type:lesseeCode property="leaseCode" id="leaseCode" readOnly="true" />
										</web:column>
										<web:column>
											<web:legend key="eleasepoinv.custid" var="program" />
										</web:column>
										<web:column>
											<type:customerIDDisplay property="custId" id="custId" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:viewContent id="scheduleDtl">
								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="200px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowEven>
											<web:column>
												<web:legend key="eleasepoinv.agreeno" var="program" />
											</web:column>
											<web:column>
												<type:agreementNo property="agreeNo" id="agreeNo" readOnly="true" />
											</web:column>
										</web:rowEven>
										<web:rowOdd>
											<web:column>
												<web:legend key="eleasepoinv.scheduleid" var="program" />
											</web:column>
											<web:column>
												<type:scheduleIDDisplay property="scheduleId" id="scheduleId" />
											</web:column>
										</web:rowOdd>
									</web:table>
								</web:section>
							</web:viewContent>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="eleasepoinv.custname" var="program" />
										</web:column>
										<web:column>
											<type:descriptionDisplay property="custName" id="custName" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:viewTitle var="program" key="eleasepoinv.details" />
							<web:section>
								<web:table>
									<web:rowOdd>
										<web:column>
											<web:grid height="230px" width="981px" id="pgmGrid" src="lss/eleasepoinv_Grid.xml">
											</web:grid>
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle width="180px" />
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="eleasepoinv.purchordsl" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:POSerialDisplay property="purchOrdSl" id="purchOrdSl" />
										</web:column>
										<web:column>
											<web:legend key="eleasepoinv.invsl" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:invoiceSerial property="invSl" id="invSl" lookup="true" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:viewTitle var="program" key="eleasepoinv.invdetails" />
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="eleasepoinv.invfor" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:combo property="invFor" id="invFor" datasourceid="ELEASEPOINV_INV_FOR" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="eleasepoinv.invdate" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:date property="invDate" id="invDate" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="eleasepoinv.invnum" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:description property="invNum" id="invNum" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="eleasepoinv.invamt" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:currencySmallAmount property="invAmt" id="invAmt" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:viewContent id="paydet" styleClass="hidden">
								<web:viewTitle var="program" key="eleasepoinv.paydet" />
								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="200px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowOdd>
											<web:column>
												<web:legend key="eleasepoinv.paysl" var="program" />
											</web:column>
											<web:column>
												<type:serial property="paySl" id="paySl" lookup="true" />
											</web:column>
										</web:rowOdd>
									</web:table>
								</web:section>
								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="200px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowEven>
											<web:column span="2">
												<web:gridToolbar gridName="pgmGrid2" cancelFunction="eleasepoinv_cancelGrid" editFunction="eleasepoinv_editGrid" addFunction="eleasepoinv_addGrid" continuousEdit="true" afterDeleteFunction="gridAfterDeleteRow" />
												<web:element property="xmlGrid2" />
											</web:column>
										</web:rowEven>
										<web:rowOdd>
											<web:column>
												<web:grid height="200px" width="601px" id="pgmGrid2" src="lss/eleasepoinv_Grid1.xml">
												</web:grid>
											</web:column>
										</web:rowOdd>
									</web:table>
								</web:section>
							</web:viewContent>
							<web:sectionTitle var="program" key="eleasepoinv.section1" />
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="eleasepoinv.assetclassifi" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:assetTypeCode property="assetClassifi" id="assetClassifi" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle width="380px" />
										<web:columnStyle width="120px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="eleasepoinv.assetdesc" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:remarks property="assetDescription" id="assetDescription" />
										</web:column>
										<web:column>
											<web:legend key="eleasepoinv.quantity" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:fiveDigit property="quantity" id="quantity" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="eleasepoinv.assetmodel" var="program" />
										</web:column>
										<web:column>
											<type:otherInformation25 property="assetModel" id="assetModel" />
										</web:column>
										<web:column>
											<web:legend key="eleasepoinv.configuration" var="program" />
										</web:column>
										<web:column>
											<type:otherInformation25 property="configuration" id="configuration" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="eleasepoinv.value" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:currencySmallAmount property="value" id="value" />
										</web:column>
										<%-- <web:column>
											<web:legend key="eleasepoinv.cst" var="program" />
										</web:column>
										<web:column>
											<type:currencySmallAmount property="cst" id="cst" />
										</web:column> --%>
									</web:rowOdd>
									<%-- <web:rowEven>
										<web:column>
											<web:legend key="eleasepoinv.vat" var="program" />
										</web:column>
										<web:column>
											<type:currencySmallAmount property="vat" id="vat" />
										</web:column>
										<web:column>
											<web:legend key="eleasepoinv.st" var="program" />
										</web:column>
										<web:column>
											<type:currencySmallAmount property="st" id="st" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="eleasepoinv.tcs" var="program" />
										</web:column>
										<web:column>
											<type:currencySmallAmount property="tcs" id="tcs" />
										</web:column>
										<web:column>
											<web:legend key="eleasepoinv.vatonamount" var="program" />
										</web:column>
										<web:column>
											<type:currencySmallAmount property="vatOnAmount" id="vatOnAmount" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="eleasepoinv.total" var="program" />
										</web:column>
										<web:column>
											<type:currencySmallAmountDisplay property="total" id="total" />
										</web:column>
									</web:rowEven> --%>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:rowEven>
										<web:column>
											<web:gridToolbar gridName="eleasepoinvasset_innerGrid" cancelFunction="eleasepoinvasset_cancelGrid" editFunction="eleasepoinvasset_editGrid" addFunction="eleasepoinvasset_addGrid" insertAtFirsstNotReq="true" afterDeleteFunction="gridAfterDeleteAssetRow" />
											<web:element property="xmleleasepoinvassetGrid" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:grid height="220px" width="991px" id="eleasepoinvasset_innerGrid" src="lss/eleasepoinvasset_innerGrid.xml">
											</web:grid>
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<%-- <web:viewContent id="view_assetDtl" styleClass="hidden">
								<web:section>
									<web:table>
										<web:rowOdd>
											<web:column>
												<web:grid height="250px" width="700px" id="eleasepoinv_assetDtlGrid" src="lss/eleasepoinv_assetDtlGrid.xml">
												</web:grid>
											</web:column>
										</web:rowOdd>
									</web:table>
								</web:section>
							</web:viewContent> --%>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="form.remarks" var="common" />
										</web:column>
										<web:column>
											<type:remarks property="remarks" id="remarks" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column span="2">
											<web:submitReset />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
						</web:sectionBlock>
					</web:dividerBlock>
				</web:viewContent>
				<web:element property="command" />
				<web:element property="action" />
				<web:element property="xmlGrid" />
				<web:element property="purchOrdSlDisplay" />
				<web:element property="scheduleIDHidden" />
				<web:element property="totalValHidden" />
				<web:element property="totalPoAmt" />
				<%-- 				<web:element property="totalAmount" />  --%>
				<web:element property="totalValueAmount" />
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
	<web:queryGrid />
	<web:TBAGrid />
	<web:contextSearch />
</web:fragment>
