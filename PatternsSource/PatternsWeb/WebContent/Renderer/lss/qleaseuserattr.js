var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'ELEASEUSERATTR';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
	eleaseuserattrGrid.setColumnHidden(0, true);
}
function clearFields() {
	$('#leaseCode').val(EMPTY_STRING);
	$('#customerID').val(EMPTY_STRING);
	$('#customerName').val(EMPTY_STRING);
	$('#leaseCode_desc').html(EMPTY_STRING);
	$('#aggrementSerial').val(EMPTY_STRING);
	$('#scheduleID').val(EMPTY_STRING);
	setCheckbox('enabled', YES);
	$('#remarks').val(EMPTY_STRING);
}

function loadGrid() {
	loadGridQuery(CURRENT_PROGRAM_ID, 'ELEASEUSERATTR_GRID', eleaseuserattrGrid);
}
function loadData() {
	$('#leaseCode').val(validator.getValue('LESSEE_CODE'));
	$('#customerID').val(validator.getValue('F1_CUSTOMER_ID'));
	$('#customerName').val(validator.getValue('F2_CUSTOMER_NAME'));
	$('#aggrementSerial').val(validator.getValue('AGREEMENT_NO'));
	$('#scheduleID').val(validator.getValue('SCHEDULE_ID'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadGrid();
	loadAuditFields(validator);
}
