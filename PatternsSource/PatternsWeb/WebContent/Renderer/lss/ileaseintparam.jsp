<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.lss.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/lss/ileaseintparam.js" />
	</web:dependencies>
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="ileaseintparam.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/lss/ileaseintparam" id="ileaseintparam" method="POST">
				<web:dividerBlock>
					<type:optionBox add="true" modify="true" view="true" />
				</web:dividerBlock>
				<web:viewContent id="po_view1" styleClass="hidden">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="ileaseintparam.productcode" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:productCode property="productCode" id="productCode" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="ileaseintparam.portfoliocode" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:lesseeCode property="portfolioCode" id="portfolioCode" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="form.effectivedate" var="common" mandatory="true" />
										</web:column>
										<web:column>
											<type:date property="effectiveDate" id="effectiveDate" lookup="true" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="ileaseintparam.remittanceaccnumber" var="program" mandatory="true"/>
										</web:column>
										<web:column>
											<type:otherInformation50 property="remittanceAccNumber" id="remittanceAccNumber" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="ileaseintparam.remittanceIFSC" var="program" mandatory="true"/>
										</web:column>
										<web:column>
											<type:ifscCode property="remittanceIFSC" id="remittanceIFSC" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="ileaseintparam.bankname" var="program"/>
										</web:column>
										<web:column>
											<type:otherInformation50Display property="bankName" id="bankName"/>
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="ileaseintparam.branchname" var="program" />
										</web:column>
										<web:column>
											<type:descriptionDisplay property="branchName" id="branchName" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="ileaseintparam.city" var="program" />
										</web:column>
										<web:column>
											<type:descriptionDisplay property="branchCity" id="branchCity" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="ileaseintparam.statecode" var="program" />
										</web:column>
										<web:column>
											<type:stateCodeDisplay property="stateCode" id="stateCode" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="form.IsUseEnabled" var="common" />
										</web:column>
										<web:column>
											<type:checkbox property="enabled" id="enabled" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="form.remarks" var="common" />
										</web:column>
										<web:column>
											<type:remarks property="remarks" id="remarks" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column span="2">
											<web:submitReset />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
						</web:sectionBlock>
					</web:dividerBlock>
				</web:viewContent>
				<web:element property="command" />
				<web:element property="action" />
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
	<web:queryGrid />
	<web:TBAGrid />
	<web:contextSearch />
</web:fragment>
