var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'IINVPOLICY';
function init() {
	refreshTBAGrid();
	refreshQueryGrid();
	if (_redisplay) {
		if ($('#action').val() == ADD) {
			$('#enabled').prop('disabled', true);
			setCheckbox('enabled', YES);
		}
		iinvpolicy_innerGrid.clearAll();
		iinvpolicy_innerGrid.loadXMLString($('#xmliinvpolicyGrid').val());
	}
}

function loadGrid() {
	loadGridQuery(CURRENT_PROGRAM_ID, 'IINVPOLICY_GRID', iinvpolicy_innerGrid);
}

function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'IINVPOLICY_MAIN', queryGrid);
}

function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}
function clearFields() {
	$('#invoiceCurrency').val(EMPTY_STRING);
	$('#invoiceCurrency_error').html(EMPTY_STRING);
	$('#invoiceCurrency_desc').html(EMPTY_STRING);
	$('#effectiveDate').val(EMPTY_STRING);
	$('#effectiveDate_error').html(EMPTY_STRING);
	clearNonPKFields();
}
function clearNonPKFields() {
	$('#uptoInvoiceAmount').val(EMPTY_STRING);
	$('#uptoInvoiceAmountFormat').val(EMPTY_STRING);
	$('#uptoInvoiceAmount_error').html(EMPTY_STRING);
	$('#numberOfSignatures_error').html(EMPTY_STRING);
	$('#signatureType_error').html(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
	iinvpolicy_innerGrid.clearAll();
}
function add() {
	$('#effectiveDate').focus();
	show('iinvpolicy_GridView');
	setCheckbox('enabled', YES);
	$('#enabled').prop('disabled', true);
	$('#invoiceCurrency').val(getBaseCurrency());
}
function modify() {
	$('#effectiveDate').focus();
	if ($('#action').val() == MODIFY) {
		$('#enabled').prop('disabled', false);
	} else {
		$('#enabled').prop('disabled', true);
	}
}
function loadData() {
	$('#invoiceCurrency').val(validator.getValue('CCY_CODE'));
	$('#effectiveDate').val(validator.getValue('EFF_DATE'));
	$('#uptoInvoiceAmountFormat').val(validator.getValue('UPTO_INV_AMOUNT'));
	$('#numberOfSignatures').val(validator.getValue('NOF_SIGNS'));
	$('#signatureType').val(validator.getValue('SIGN_TYPE'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadGrid();
	resetLoading();
}
function view(source, primaryKey) {
	hideParent('lss/qinvpolicy', source, primaryKey);
	resetLoading();
}
function doclearfields(id) {
	switch (id) {
	case 'invoiceCurrency':
		if (isEmpty($('#invoiceCurrency').val())) {
			clearFields();
			break;
		}
	case 'uptoInvoiceAmount':
		if (isEmpty($('#uptoInvoiceAmount').val())) {
			iinvpolicy_innerGrid.clearAll();
			break;
		}
	}
}

function doHelp(id) {
	switch (id) {
	case 'effectiveDate':
		help('IINVPOLICY', 'HLP_EFF_DATE', $('#effectiveDate').val(), EMPTY_STRING, $('#effectiveDate'));
		break;
	}
}

function validate(id) {
	switch (id) {
	case 'invoiceCurrency':
		invoiceCurrency_val();
		break;
	case 'effectiveDate':
		effectiveDate_val();
		break;
	case 'uptoInvoiceAmountFormat':
		uptoInvoiceAmount_val();
		break;
	case 'numberOfSignatures':
		numberOfSignatures_val();
		break;
	case 'signatureType':
		signatureType_val();
		break;
	case 'enabled':
		enabled_val();
		break;
	case 'remarks':
		remarks_val();
		break;
	}
}
function backtrack(id) {
	switch (id) {
	case 'invoiceCurrency':
		setFocusLast('invoiceCurrency');
		break;
	case 'effectiveDate':
		setFocusLast('invoiceCurrency');
		break;
	case 'uptoInvoiceAmountFormat':
		setFocusLast('effectiveDate');
		break;
	case 'numberOfSignatures':
		setFocusLast('uptoInvoiceAmountFormat');
		break;
	case 'signatureType':
		setFocusLast('numberOfSignatures');
		break;
	case 'enabled':
		setFocusLast('signatureType');
		break;
	case 'remarks':
		if ($('#action').val() == MODIFY) {
			setFocusLast('enabled');
		} else {
			setFocusLast('signatureType');
		}
		break;
	}

}

function invoiceCurrency_val() {
	var value = $('#invoiceCurrency').val();
	clearError('invoiceCurrency_error');
	$('#invoiceCurrency_desc').html(EMPTY_STRING);
	if (isEmpty(value)) {
		setError('invoiceCurrency_error', MANDATORY);
		return false;
	}
	setFocusLast('effectiveDate');
	return true;
}

function effectiveDate_val() {
	var value = $('#effectiveDate').val();
	clearError('effectiveDate_error');
	if (isEmpty(value)) {
		$('#effectiveDate').val(getCBD());
	}
	if (!isValidEffectiveDate($('#effectiveDate').val(), 'effectiveDate_error')) {
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('CCY_CODE', $('#invoiceCurrency').val());
		validator.setValue('EFF_DATE', $('#effectiveDate').val());
		validator.setValue(ACTION, $('#action').val());
		validator.setClass('patterns.config.web.forms.lss.iinvpolicybean');
		validator.setMethod('validateEffectiveDate');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('effectiveDate_error', validator.getValue(ERROR));
			return false;
		}
		if ($('#action').val() == MODIFY) {
			PK_VALUE = getEntityCode() + PK_SEPERATOR + $('#invoiceCurrency').val() + PK_SEPERATOR + $('#effectiveDate').val();
			if (!loadPKValues(PK_VALUE, 'effectiveDate_error')) {
				return false;
			}
		}
	}
	setFocusLast('uptoInvoiceAmountFormat');
	return true;
}

function uptoInvoiceAmount_val() {
	var value = unformatAmount($('#uptoInvoiceAmountFormat').val());
	clearError('uptoInvoiceAmount_error');
	if (isEmpty(value)) {
		value = '999999999999999.999';
	$('#uptoInvoiceAmount').val(value);
	}else{
	if (isZero(value)) {
		setError('uptoInvoiceAmount_error', ZERO_AMOUNT);
		return false;
	}
	$('#uptoInvoiceAmount').val(value);
	if (!isValidAmount(value)) {
		setError('uptoInvoiceAmount_error', INVALID_VALUE);
		return false;
	}
	if (!isPositiveAmount(value)) {
		setError('uptoInvoiceAmount_error', POSITIVE_AMOUNT);
		return false;
	}
	if (!isCurrencySmallAmount($('#uptoInvoiceAmount_curr').val(), value)) {
		setError('uptoInvoiceAmount_error', INVALID_SMALL_AMOUNT);
		return false;
	}}
	$('#uptoInvoiceAmountFormat').val(formatAmount(value, $('#invoiceCurrency').val()));
	setFocusLast('numberOfSignatures');
	return true;
}

function numberOfSignatures_val() {
	setFocusLast('signatureType');
}
function signatureType_val() {
	setFocusLast('iinvpolicy_innerGrid_add');
}
function enabled_val() {
	if ($('#action').val() == MODIFY) {
		setFocusLast('remarks');
	}
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', INVALID_REMARKS);
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', INVALID_REMARKS);
			return false;
		}
	}
	if ($('#action').val() == MODIFY) {
		if (isEmpty(value)) {
			setError('remarks_error', MANDATORY);
			return false;
		}
	}
	setFocusOnSubmit();
	return true;

}

function uptoInvoiceAmount_gridDuplicateCheck(editMode, editRowId) {
	var currentValue = [ $('#uptoInvoiceAmountFormat').val() ];
	return _grid_duplicate_check(iinvpolicy_innerGrid, currentValue, [ 1 ]);
}

function iinvpolicy_addGrid(editMode, editRowId) {
	if (uptoInvoiceAmount_val()) {
		if (iinvpolicy_innerGrid.getRowsNum() > 0) {
			if (parseFloat(iinvpolicy_innerGrid.cells2(iinvpolicy_innerGrid.getRowsNum() - 1, 6).getValue()) > parseFloat($('#uptoInvoiceAmount').val())) {
				setError('uptoInvoiceAmount_error', UPTO_INVOICE_GREATER);
				$('#uptoInvoiceAmountFormat').focus();
				return false;
			}
		}
		if (uptoInvoiceAmount_gridDuplicateCheck(editMode, editRowId)) {
			var numberOfSigns, signType = null;
			if ($("#numberOfSignatures").val() == '1')
				numberOfSigns = 'One';
			else
				numberOfSigns = 'Two';
			if ($("#signatureType").val() == 'I')
				signType = 'Image';
			else
				signType = 'Physical';
			var field_values = [ 'false', $('#uptoInvoiceAmountFormat').val(), numberOfSigns, signType, $('#numberOfSignatures').val(), $('#signatureType').val(), $('#uptoInvoiceAmount').val() ];
			_grid_updateRow(iinvpolicy_innerGrid, field_values);
			iinvpolicy_clearGridFields();
			$('#uptoInvoiceAmountFormat').focus();
			return true;
		} else {
			setError('uptoInvoiceAmount_error', DUPLICATE_DATA);
			$('#uptoInvoiceAmountFormat').focus();
			return false;
		}
	} else {
		$('#uptoInvoiceAmountFormat').focus();
		return false;
	}

}

function iinvpolicy_editGrid(rowId) {
	$('#uptoInvoiceAmountFormat').val(iinvpolicy_innerGrid.cells(rowId, 1).getValue());
	$('#uptoInvoiceAmount_desc').val(iinvpolicy_innerGrid.cells(rowId, 2).getValue());
	$('#numberOfSignatures').val(iinvpolicy_innerGrid.cells(rowId, 4).getValue());
	$('#signatureType').val(iinvpolicy_innerGrid.cells(rowId, 5).getValue());
	$('#uptoInvoiceAmount').focus();
}

function iinvpolicy_cancelGrid(rowId) {
	iinvpolicy_clearGridFields();
}

function gridExit(id) {
	switch (id) {
	case 'uptoInvoiceAmount':
		$('#xmliinvpolicyGrid').val(iinvpolicy_innerGrid.serialize());
		iinvpolicy_clearGridFields();
		break;
	}
	setFocusLast('remarks');
}

function iinvpolicy_clearGridFields() {
	$('#uptoInvoiceAmount').val(EMPTY_STRING);
	$('#uptoInvoiceAmount_desc').html(EMPTY_STRING);
	$('#uptoInvoiceAmount_error').html(EMPTY_STRING);
	clearError('uptoInvoiceAmount_error');
	$('#uptoInvoiceAmountFormat').val(EMPTY_STRING);
	$('#uptoInvoiceAmountFormat_desc').html(EMPTY_STRING);
	$('#uptoInvoiceAmountFormat_error').html(EMPTY_STRING);
	clearError('uptoInvoiceAmountFormat_error');
}

function revalidate() {
	iinvpolicy_revaildateGrid();
}

function iinvpolicy_revaildateGrid() {
	if (iinvpolicy_innerGrid.getRowsNum() > 0) {
		$('#xmliinvpolicyGrid').val(iinvpolicy_innerGrid.serialize());
		$('#uptoInvoiceAmount_error').html(EMPTY_STRING);
	} else {
		iinvpolicy_innerGrid.clearAll();
		setError('uptoInvoiceAmount_error', ADD_ATLEAST_ONE_ROW);
		errors++;
	}
}
