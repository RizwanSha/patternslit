var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'ELEASEINT';
var poCurrency = EMPTY_STRING;
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}

function clearFields() {
	$('#lesseCode').val(EMPTY_STRING);
	$('#lesseCode_desc').html(EMPTY_STRING);
	$('#lesseCode_error').html(EMPTY_STRING);
	$('#customerID').val(EMPTY_STRING);
	$('#customerID_desc').html(EMPTY_STRING);
	$('#customerID_error').html(EMPTY_STRING);
	$('#customerName').val(EMPTY_STRING);
	$('#aggrementNumber').val(EMPTY_STRING);
	$('#aggrementNumber_desc').html(EMPTY_STRING);
	$('#aggrementNumber_error').html(EMPTY_STRING);
	$('#scheduleID').val(EMPTY_STRING);
	$('#scheduleID_desc').html(EMPTY_STRING);
	$('#scheduleID_error').html(EMPTY_STRING);
	$('#entrySerial').val(EMPTY_STRING);
	$('#entrySerial_error').html(EMPTY_STRING);
	clearNonPKFields();
}

function clearNonPKFields() {
	$('#dateOfEntry').val(getCBD());
	$('#fromDate').val(EMPTY_STRING);
	$('#fromDate_error').html(EMPTY_STRING);
	$('#uptoDate').val(EMPTY_STRING);
	$('#uptoDate_error').html(EMPTY_STRING);
	$('#noOfDays').val(EMPTY_STRING);
	$('#interestRate').val(EMPTY_STRING);
	$('#interestRate').prop('readonly', true);
	$('#interestOnAmount').val(EMPTY_STRING);
	$('#interestOnAmountFormat').val(EMPTY_STRING);
	$('#interestOnAmount_curr').val(EMPTY_STRING);
	$('#interestOnAmount_error').html(EMPTY_STRING);
	$('#amountToBeBilled').val(EMPTY_STRING);
	$('#amountToBeBilledFormat').val(EMPTY_STRING);
	$('#amountToBeBilled_curr').val(EMPTY_STRING);
	$('#amountToBeBilled_error').html(EMPTY_STRING);
	$('#dateOfInvoicingBilling').val(EMPTY_STRING);
	$('#dateOfInvoicingBilling_error').html(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
}

function loadData() {
	$('#lesseCode').val(validator.getValue('LESSEE_CODE'));
	$('#customerID').val(validator.getValue('F2_CUSTOMER_ID'));
	$('#customerName').val(validator.getValue('F2_CUSTOMER_NAME'));
	$('#aggrementNumber').val(validator.getValue('AGREEMENT_NO'));
	$('#scheduleID').val(validator.getValue('SCHEDULE_ID'));
	$('#entrySerial').val(validator.getValue('ENTRY_SL'));
	$('#dateOfEntry').val(validator.getValue('DATE_OF_ENTRY'));
	$('#fromDate').val(validator.getValue('INT_FROM_DATE'));
	$('#uptoDate').val(validator.getValue('INT_UPTO_DATE'));
	$('#noOfDays').val(validator.getValue('INT_FOR_DAYS'));
	$('#interestRate').val(validator.getValue('APPLIED_INT_RATE'));
	$('#dateOfInvoicingBilling').val(validator.getValue('INV_DATE'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadAuditFields(validator);
	var intersentOnAmount = validator.getValue('INT_ON_AMOUNT');
	var intersentOnAmountCurrency = validator.getValue('INT_ON_AMOUNT_CCY');
	var amountToBeBilled = validator.getValue('INV_AMOUNT');
	var amountToBeBilledCurrency = validator.getValue('INV_AMOUNT_CCY');
	$('#interestOnAmountFormat').val(formatAmount(intersentOnAmount, intersentOnAmountCurrency));
	$('#interestOnAmount').val(unformatAmount($('#interestOnAmountFormat').val()));
	$('#interestOnAmount_curr').val(intersentOnAmountCurrency);
	$('#amountToBeBilledFormat').val(formatAmount(amountToBeBilled, amountToBeBilledCurrency));
	$('#amountToBeBilled').val(unformatAmount($('#amountToBeBilledFormat').val()));
	$('#amountToBeBilled_curr').val(amountToBeBilledCurrency);
}

