var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'ELEASEPDC';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}

function loadQuery() {
	eleasepdc_innerGrid.setColumnHidden(0, true);
	loadGridQuery(CURRENT_PROGRAM_ID, 'ELEASEPDC_GRID', eleasepdc_innerGrid);
}

function clearFields() {
	$('#lesseCode').val(EMPTY_STRING);
	$('#lesseCode_desc').html(EMPTY_STRING);
	$('#customerID').val(EMPTY_STRING);
	$('#customerName').val(EMPTY_STRING);
	$('#aggrementNumber').val(EMPTY_STRING);
	$('#aggrementNumber_desc').html(EMPTY_STRING);
	$('#scheduleID').val(EMPTY_STRING);
	$('#scheduleID_desc').html(EMPTY_STRING);
	$('#pdcLotSerial').val(EMPTY_STRING);
	$('#bankName').val(EMPTY_STRING);
	$('#branchName').val(EMPTY_STRING);
	$('#ifscCode').val(EMPTY_STRING);
	$('#accountNumber').val(EMPTY_STRING);
	$('#accountName').val(EMPTY_STRING);
	$('#bankingProcessCode').val(EMPTY_STRING);
	$('#bankingProcessCode_desc').html(EMPTY_STRING);
	$('#noOfChque').val(EMPTY_STRING);
	eleasepdc_innerGrid.clearAll();
	$('#remarks').val(EMPTY_STRING);
}
function loadData() {
	$('#lesseCode').val(validator.getValue('LESSEE_CODE'));
	$('#customerID').val(validator.getValue('F2_CUSTOMER_ID'));
	$('#customerName').val(validator.getValue('F2_CUSTOMER_NAME'));
	$('#aggrementNumber').val(validator.getValue('AGREEMENT_NO'));
	$('#scheduleID').val(validator.getValue('SCHEDULE_ID'));
	$('#pdcLotSerial').val(validator.getValue('PDC_LOT_SL'));
	$('#pdclotDate').val(validator.getValue('DATE_OF_ENTRY'));
	setCheckbox('disablePdcLot', validator.getValue('LOT_DISABLED'));
	$('#bankName').val(validator.getValue('CHQ_BANK'));
	$('#branchName').val(validator.getValue('CHQ_BRANCH'));
	$('#ifscCode').val(validator.getValue('CHQ_BRANCH_IFSC'));
	$('#accountNumber').val(validator.getValue('CHQ_AC_NO'));
	$('#accountName').val(validator.getValue('CHQ_AC_NAME'));
	$('#bankingProcessCode').val(validator.getValue('BANK_PROCESS_CODE'));
	$('#bankingProcessCode_desc').html(validator.getValue('F3_NAME'));
	$('#noOfChque').val(validator.getValue('NOF_PDC'));
	$('#remarks').val(validator.getValue('REMARKS'));
	if ($('#disablePdcLot').is(':checked'))
		hide('disablePdc');
	else
		show('disablePdc');
	loadAuditFields(validator);
	loadGrid();
}

function loadGrid() {
	SOURCE_VALUE = _tableType;
	PK_VALUE = _primaryKey;
	if (_tableType == MAIN) {
		loadQuery();
	} else if (_tableType == TBA) {
		loadQuery();
	}
}
function doTabbarClick(id) {
	if (id == 'bankpdcDetails_0') {
		$('#bankpdcDetails').height('340px');
	} else {
		$('#bankpdcDetails').height('270px');
	}
}
