var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'IINVPOLICY';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}

function loadQuery() {
	loadGridQuery(CURRENT_PROGRAM_ID, 'IINVPOLICY_GRID', iinvpolicy_innerGrid);
	iinvpolicy_innerGrid.setColumnsVisibility("true,false,false,flase,false,false,false");
}

function clearFields() {
	$('#invoiceCurrency').val(EMPTY_STRING);
	$('#invoiceCurrency_desc').html(EMPTY_STRING);
	$('#effectiveDate').val(EMPTY_STRING);
	iinvpolicy_innerGrid.clearAll();
}

function loadData() {
	$('#invoiceCurrency').val(validator.getValue('CCY_CODE'));
	$('#effectiveDate').val(validator.getValue('EFF_DATE'));
	$('#uptoInvoiceAmountFormat').val(validator.getValue('UPTO_INV_AMOUNT'));
	$('#numberOfSignatures').val(validator.getValue('NOF_SIGNS'));
	$('#signatureType').val(validator.getValue('SIGN_TYPE'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	iinvpolicy_innerGrid.clearAll();
	loadAuditFields(validator);
	loadGrid();
}

function loadGrid() {
	SOURCE_VALUE = _tableType;
	PK_VALUE = _primaryKey;
	if (_tableType == MAIN) {
		loadQuery();

	} else if (_tableType == TBA) {
		loadQuery();
	}
}
