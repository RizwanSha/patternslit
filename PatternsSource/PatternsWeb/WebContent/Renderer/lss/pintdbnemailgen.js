var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'PINTDBNEMAILGEN';
function init() {
	if ($('.offcanvas-menu').length > 0) {
		var offcanvasVisible = $('.offcanvas-menu').is(':visible');
		if (offcanvasVisible) {
			slideMenu();
		}
	}
	pintdbnemailgen_innerGrid.attachEvent("onRowDblClicked", loadComponentDetails);
	showMessage("emailInfo", Message.INFO, PINVEMAILGEN_EMAIL_INFO);
	pintdbnemailgen_innerGrid.attachEvent("onCheckbox", function(id, ind, value) {
		if (!value)
			$("#masterCheckBox").removeClass("fa fa-check-square-o").addClass("fa fa-square-o");
		if (pintdbnemailgen_innerGrid.getCheckedRows(0).split(',').length == pintdbnemailgen_innerGrid.getRowsNum()) {
			$("#masterCheckBox").removeClass("fa fa-square-o").addClass("fa fa-check-square-o");
			$("#masterCheckBox").attr('title', 'Unselect all');
		} else
			$("#masterCheckBox").attr('title', 'Select all');
		return true;
	});
	clearFields();
}
function masterCheckBox() {
	if (pintdbnemailgen_innerGrid.getRowsNum() >= 1) {
		if ($("#masterCheckBox").hasClass("fa-square-o")) {
			$("#masterCheckBox").removeClass("fa fa-square-o").addClass("fa fa-check-square-o");
			pintdbnemailgen_innerGrid.checkAll(true);
			$("#masterCheckBox").attr('title', 'Unselect all');
		} else if ($("#masterCheckBox").hasClass("fa-check-square-o")) {
			$("#masterCheckBox").removeClass("fa fa-check-square-o").addClass("fa fa-square-o");
			pintdbnemailgen_innerGrid.checkAll(false);
			$("#masterCheckBox").attr('title', 'Select all');
		}
	} else {
		$("#masterCheckBox").attr('title', '');
	}
}
function clearFields() {
	$('#intMonth').focus();
	//$('#intMonth').val(EMPTY_STRING);
	$('#intMonthYear').val(EMPTY_STRING);
	clearPkFieldsError();
	clearNonPkFields();
}
function clearPkFieldsError() {
	$('#intMonth_error').html(EMPTY_STRING);
}
function clearNonPkFields() {
	hideMessage(CURRENT_PROGRAM_ID);
	pintdbnemailgen_innerGrid.clearAll();
	mailGrid.clearAll();
	$("#masterCheckBox").removeClass("fa fa-check-square-o");
	$("#masterCheckBox").addClass("fa fa-square-o");
	$("#masterCheckBox").attr('title', '');
}
function doHelp(id) {
	switch (id) {
	}
}
function backtrack(id) {
	switch (id) {
	case 'intMonthYear':
		setFocusLast('intMonth');
		break;
	case 'submit':
		setFocusLast('intMonthYear');
		break;
	}
}
function doclearfields(id) {
	switch (id) {
	case 'intMonth':
		if (isEmpty($('#intMonth').val())) {
			clearFields();
		}
		break;
	case 'intMonthYear':
		if (isEmpty($('#intMonthYear').val())) {
			clearPkFieldsError();
			clearNonPkFields();
		}
		break;
	}
}
function validate(id) {
	valMode = true;
	switch (id) {
	case 'intMonth':
		intMonth_val(valMode);
		break;
	case 'intMonthYear':
		intMonthYear_val(valMode);
		break;
	case 'submit':
		loadGrid();
		break;
	case 'sendMailId':
		sendMail();
		break;
	case 'reset':
		clearFields();
		break;
	}
}
function intMonth_val(valMode) {
	var value = $('#intMonth').val();
	clearError('intMonth_error');
	if (isEmpty(value)) {
		setError('intMonth_error', MANDATORY);
		return false;
	}
	if (valMode)
		setFocusLast('intMonthYear');
	return true;
}
function intMonthYear_val(valMode) {
	var value = $('#intMonthYear').val();
	clearError('intMonth_error');
	if (isEmpty(value)) {
		$('#intMonthYear').val(getCalendarYear(getCBD()));
		value = $('#intMonthYear').val();
	}
	if (!isValidYear(value)) {
		setError('intMonth_error', INVALID_YEAR);
		return false;
	}
	if (value>getCalendarYear(getCBD())) {
		setError('intMonth_error', YEAR_LE_CALYEAR);
		return false;
	}
	if (valMode)
		setFocusLast('submit');
	return true;
}

function loadGrid() {
	clearNonPkFields();
	clearPkFieldsError();
	showMessage(getProgramID(), Message.PROGRESS);
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('INT_YEAR', $('#intMonthYear').val());
	validator.setValue('INT_MONTH', $('#intMonth').val());
	validator.setValue('REVALIDATE', "1");
	validator.setClass('patterns.config.web.forms.lss.pintdbnemailgenbean');
	validator.setMethod('loadGrid');
	validator.sendAndReceiveAsync(loadGridValues);
}
function loadGridValues() {
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		showMessage(getProgramID(), Message.ERROR, validator.getValue(ERROR));
		return false;
	} else if (validator.getValue(RESULT) == DATA_UNAVAILABLE) {
		showMessage(getProgramID(), Message.WARN, NO_RECORD);
	} else if (validator.getValue('ERROR_PRESENT') == ONE) {
		if (validator.getValue('MONTH_ERROR').trim() != EMPTY_STRING)
			$('#intMonth_error').html(validator.getValue('MONTH_ERROR'));
		if (validator.getValue('YEAR_ERROR').trim() != EMPTY_STRING)
			$('#intMonth_error').html(validator.getValue('YEAR_ERROR'));
		hideMessage(CURRENT_PROGRAM_ID);
	} else if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		xmlString = validator.getValue(RESULT_XML);
		pintdbnemailgen_innerGrid.loadXMLString(xmlString);
		fromDate = validator.getValue('FROM_DATE');
		uptoDate = validator.getValue('UPTO_DATE');
		hideMessage(CURRENT_PROGRAM_ID);
		$("#masterCheckBox").attr('title', 'Select all');
		$('#submit').blur();
	}
	return true;

}
function sendMail() {
	mailGrid.clearAll();
	if (pintdbnemailgen_innerGrid.getCheckedRows(0) != EMPTY_STRING) {
		var rowId = pintdbnemailgen_innerGrid.getCheckedRows(0).split(',');
		for (var i = 0; i < rowId.length; i++) {
			var field_values = [ 'false', pintdbnemailgen_innerGrid.cells(rowId[i], 1).getValue(), pintdbnemailgen_innerGrid.cells(rowId[i], 2).getValue(), pintdbnemailgen_innerGrid.cells(rowId[i], 3).getValue(), pintdbnemailgen_innerGrid.cells(rowId[i], 4).getValue(),
					pintdbnemailgen_innerGrid.cells(rowId[i], 5).getValue(), pintdbnemailgen_innerGrid.cells(rowId[i], 6).getValue(), pintdbnemailgen_innerGrid.cells(rowId[i], 7).getValue(), pintdbnemailgen_innerGrid.cells(rowId[i], 8).getValue(),
					pintdbnemailgen_innerGrid.cells(rowId[i], 9).getValue(), pintdbnemailgen_innerGrid.cells(rowId[i], 10).getValue(), pintdbnemailgen_innerGrid.cells(rowId[i], 11).getValue(), pintdbnemailgen_innerGrid.cells(rowId[i], 12).getValue(),
					pintdbnemailgen_innerGrid.cells(rowId[i], 13).getValue(), pintdbnemailgen_innerGrid.cells(rowId[i], 14).getValue() ];
			_grid_updateRow(mailGrid, field_values);
		}
	} else {
		alert(SELECT_ATLEAST_ONE);
		return false;
	}
	showMessage(getProgramID(), Message.PROGRESS);
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue(ACTION, USAGE);
	validator.setValue('INT_YEAR', $('#intMonthYear').val());
	validator.setValue('INT_MONTH', $('#intMonth').val());
	validator.setValue('REVALIDATE', "0");
	validator.setValue('XML', mailGrid.serialize());
	validator.setClass('patterns.config.web.forms.lss.pintdbnemailgenbean');
	validator.setMethod('processAction');
	validator.sendAndReceiveAsync(sendMailAsync);
}
function sendMailAsync() {
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		showMessage(getProgramID(), Message.ERROR, validator.getValue(ERROR));
		return false;
	} else if (validator.getValue(RESULT) == DATA_UNAVAILABLE) {
		showMessage(getProgramID(), validator.getValue(ERROR));
		return false;
	} else if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		showMessage(getProgramID(), Message.SUCCESS, EMAIL_HAS_SENT_FOR_PROCESS);
		pintdbnemailgen_innerGrid.clearAll();
		xmlString = validator.getValue(RESULT_XML);
		pintdbnemailgen_innerGrid.loadXMLString(xmlString);
		$("#masterCheckBox").removeClass("fa fa-check-square-o");
		$("#masterCheckBox").addClass("fa fa-square-o");
		$("#masterCheckBox").attr('title', 'Select all');
	}
}

function loadComponentDetails(rowId) {
	pintdbnemailgen_ledgerGrid.clearAll();
	win = showWindow('componentdtl', EMPTY_STRING, LEDGER_DETAILS_EMAIL_GEN, EMPTY_STRING, true, false, false, 1150, 430);
	showMessage('ledgerDetails', Message.PROGRESS);
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('DEBIT_NOTE_INVENTORY', rowId);
	validator.setClass('patterns.config.web.forms.lss.pintdbnemailgenbean');
	validator.setMethod('loadComponentDetails');
	validator.sendAndReceiveAsync(showLedgerDetails);
}
function showLedgerDetails() {
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		showMessage('component', Message.ERROR, validator.getValue(ERROR));
		return false;
	}
	if (validator.getValue(RESULT) == DATA_UNAVAILABLE) {
		showMessage('component', Message.ERROR, NO_RECORD);
		return false;
	}
	if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		pintdbnemailgen_ledgerGrid.clearAll();
		pintdbnemailgen_ledgerGrid.loadXMLString(validator.getValue(RESULT_XML));
		hideMessage('component');
	}
}



// Mail
function viewSentLogDetailsFields() {
	mailSentLog_innerGrid.clearAll();
	$('#preLeaseContractRefDate').val(EMPTY_STRING);
	$('#preLeaseContractRefSerial').val(EMPTY_STRING);
	$('#customerName').val(EMPTY_STRING);
	$('#mailSentLogError_error').html(EMPTY_STRING);
}
function viewSentLogDetails(rowId) {
	win = showWindow('mailSentLog', EMPTY_STRING, EMAIL_SENT_DETAILS, '', true, false, false, 835, 390);
	viewSentLogDetailsFields();
	$('#preLeaseContractRefDate').val(pintdbnemailgen_innerGrid.cells(rowId, 1).getValue());
	$('#preLeaseContractRefSerial').val(pintdbnemailgen_innerGrid.cells(rowId, 2).getValue());
	$('#customerName').val(pintdbnemailgen_innerGrid.cells(rowId, 4).getValue());
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('DEBIT_NOTE_INVENTORY', pintdbnemailgen_innerGrid.cells(rowId, 10).getValue());
	validator.setClass('patterns.config.web.forms.lss.pintdbnemailgenbean');
	validator.setMethod('viewSentLogDetails');
	validator.sendAndReceiveAsync(viewSentLogDetailsAsync);
}
function viewSentLogDetailsAsync() {
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('mailSentLogError_error', validator.getValue(ERROR));
		return false;
	} else if (validator.getValue(RESULT) == DATA_UNAVAILABLE) {
		setError('mailSentLogError_error', NO_ROWS_AVAILABLE);
		return false;
	} else if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		$('#mailSentLogError_error').html(EMPTY_STRING);
		xmlString = validator.getValue(RESULT_XML);
		mailSentLog_innerGrid.loadXMLString(xmlString);
	}
}

function clearMailDeatilsFields() {
	mailSentRcp_innerGrid.clearAll();
	$('#mailLogDetailsError_error').html(EMPTY_STRING);
	$('#attachment').removeClass('hidden');
	$('#subject').val(EMPTY_STRING);
	$('#emailContent').val(EMPTY_STRING);
}

function viewMailDetails(EmailLogInvNo) {
	clearMailDeatilsFields();
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('EMAIL_LOG_INVENTORY_NO', EmailLogInvNo);
	validator.setClass('patterns.config.web.forms.lss.pintdbnemailgenbean');
	validator.setMethod('viewMailDetails');
	win = showWindow('mailLogDetails', EMPTY_STRING, EMAIL_DETAILS, '', true, false, false, 900, 605);
	validator.sendAndReceiveAsync(viewMailDetailsAsync);
}
function viewMailDetailsAsync() {
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('mailLogDetailsError_error', validator.getValue(ERROR));
		return false;
	} else if (validator.getValue(RESULT) == DATA_UNAVAILABLE) {
		setError('mailLogDetailsError_error', NO_ROWS_AVAILABLE);
		return false;
	} else if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		$('#mailLogDetailsError_error').html(EMPTY_STRING);
		$('#subject').html(validator.getValue('EMAIL_SUBJECT'));
		$('#emailContent').html(validator.getValue('EMAIL_TEXT'));
		$('#sentOn').val(validator.getValue('SENT_DATE'));
		xmlString = validator.getValue(RESULT_XML);
		mailSentRcp_innerGrid.loadXMLString(xmlString);
		if (validator.getValue("PDF_REPORT_IDENTIFIER").trim() == EMPTY_STRING || validator.getValue("PDF_REPORT_FILE_NAME").trim() == EMPTY_STRING) {
			$('#pdfDiv').hide();
			$('#attachment').addClass('hidden');
		} else {
			$('#pdfDiv').show();
			$('#pdfCaption').html(validator.getValue("PDF_REPORT_FILE_NAME"));
			$('#pdf').attr('href', getBasePath() + 'servlet/ReportDownloadProcessor?' + REPORT_ID + '=' + validator.getValue("PDF_REPORT_IDENTIFIER"));
			$('#pdfFile').attr('href', getBasePath() + 'servlet/ReportDownloadProcessor?' + REPORT_ID + '=' + validator.getValue("PDF_REPORT_IDENTIFIER"));
			attachment = true;
		}
	}
}
function afterClosePopUp(win) {
	switch (win._idd) {
	case 'mailLogDetails':
		dhxWindows = null;
		win = showWindow('mailSentLog', EMPTY_STRING, EMAIL_SENT_DETAILS, '', true, false, false, 810, 390);
		break;

	}
}
