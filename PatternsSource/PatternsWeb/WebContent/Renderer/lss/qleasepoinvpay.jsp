<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.lss.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/lss/qleasepoinvpay.js" />
	</web:dependencies>
	<web:loadPK />
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="qleasepoinvpay.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:viewContent id="po_view1">
				<web:dividerBlock>
					<web:sectionBlock width="width-100p" align="left">
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="eleasepoinvpay.preleaseref" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:dateDaySLDisplay property="preLeaseRef" id="preLeaseRef" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle width="320px" />
									<web:columnStyle width="250px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowOdd>
									<web:column>
										<web:legend key="eleasepoinvpay.leasecode" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:lesseeCodeDisplay property="leaseCode" id="leaseCode"  />
									</web:column>
									<web:column>
										<web:legend key="eleasepoinvpay.custid" var="program" />
									</web:column>
									<web:column>
										<type:customerIDDisplay property="custId" id="custId" />
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>

						<web:viewContent id="scheduleDetail">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="eleasepoinvpay.agreeno" var="program" />
										</web:column>
										<web:column>
											<type:agreementNoDisplay property="agreeNo" id="agreeNo" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="eleasepoinvpay.scheduleid" var="program" />
										</web:column>
										<web:column>
											<type:scheduleIDDisplay property="scheduleId" id="scheduleId" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
						</web:viewContent>


						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="eleasepoinvpay.custname" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:descriptionDisplay property="custName" id="custName" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>

						<web:viewTitle var="program" key="eleasepoinvpay.details" />
						<web:section>
							<web:table>
								<web:rowOdd>
									<web:column>
										<web:grid height="210px" width="785px" id="pgmGrid" src="lss/eleasepoinvpay_Grid.xml">
										</web:grid>
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle width="320px" />
									<web:columnStyle width="250px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="eleasepoinvpay.selectedposl" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:POSerialDisplay property="selectedPoSl" id="selectedPoSl" />
									</web:column>
									<web:column>
										<web:legend key="eleasepoinvpay.paymentsl" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:serialDisplay property="paymentSl" id="paymentSl" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="eleasepoinvpay.payfor" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:comboDisplay property="payFor" id="payFor" datasourceid="COMMON_PAY_FOR" />
									</web:column>
									<web:column>
										<web:legend key="eleasepoinvpay.selectedinvsl" var="program" />
									</web:column>
									<web:column>
										<type:serialDisplay property="selectedInvSl" id="selectedInvSl" />
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
						<web:viewTitle var="program" key="eleasepoinvpay.detailspay" />
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="eleasepoinvpay.date" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:dateDisplay property="date" id="date" />
									</web:column>
								</web:rowEven>

							</web:table>
						</web:section>

						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle width="330px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowOdd>
									<web:column>
										<web:legend key="eleasepoinvpay.payfavour" var="program" />
									</web:column>
									<web:column>
										<type:codeDisplay property="payFavour" id="payFavour" />
									</web:column>
									<web:column>
										<web:anchorMessage var="program" key="eleasepoinvpay.link" onclick="qsupplierdetails()" style="text-decoration:underline;cursor:pointer" />
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle width="320px" />
									<web:columnStyle width="250px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="eleasepoinvpay.amtpaid" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:currencySmallAmountDisplay property="amtPaid" id="amtPaid" />
									</web:column>
									<web:column>
										<web:legend key="eleasepoinvpay.eqlocal" var="program" />
									</web:column>
									<web:column>
										<type:currencySmallAmountDisplay property="eqLocal" id="eqLocal" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowOdd>
									<web:column>
										<web:legend key="eleasepoinvpay.payref" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:descriptionDisplay property="payRef" id="payRef" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="form.remarks" var="common" />
									</web:column>
									<web:column>
										<type:remarksDisplay property="remarks" id="remarks" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
					</web:sectionBlock>
				</web:dividerBlock>
			</web:viewContent>
		</web:viewSubPart>
	</web:viewPart>
</web:fragment>
