var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'EPRELEASECONTREG';
function init() {
	refreshTBAGrid();
	refreshQueryGrid();
	if (_redisplay) {
		interestApplicable_val(false);
	}
}

function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'EPRELEASECONTREG_MAIN', queryGrid);
}

function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}

function doclearfields(id) {
	switch (id) {

	}
}

function clearFields() {
	$('#preLeaseContractRefDate').val(EMPTY_STRING);
	$('#preLeaseContractRefSerial').val(EMPTY_STRING);
	$('#preLeaseContractRefDate').val(getCBD());
	clearNonPKFields();
}
function clearNonPKFields() {
	$('#customerID').val(EMPTY_STRING);
	$('#customerID_error').html(EMPTY_STRING);
	$('#customerID_desc').html(EMPTY_STRING);
	$('#lesseeCode').val(EMPTY_STRING);
	$('#lesseeCode_error').html(EMPTY_STRING);
	$('#lesseeCode_desc').html(EMPTY_STRING);
	$('#productCode').val(EMPTY_STRING);
	$('#productCode_desc').html(EMPTY_STRING);
	$('#branchCode').val(EMPTY_STRING);
	$('#branchCode_desc').html(EMPTY_STRING);
	$('#branchCode_error').html(EMPTY_STRING);
	$('#addressSerial').val(EMPTY_STRING);
	$('#addressSerial_error').html(EMPTY_STRING);
	$('#contactSerial').val(EMPTY_STRING);
	$('#contactSerial_error').html(EMPTY_STRING);
	setCheckbox('interestApplicable', NO);
	$('#interestRateInvestLeaseCommit').val(EMPTY_STRING);
	$('#interestRateInvestLeaseCommit_error').html(EMPTY_STRING);
	$('#interestBillingChoice').prop('selectedIndex', 0);
	$('#interestBillingChoice_error').html(EMPTY_STRING);
	$('#interestDebitNotePrinting').prop('selectedIndex', 1);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
	interestApplicable_val(false);
}

function doHelp(id) {
	switch (id) {
	case 'customerID':
		help('COMMON', 'HLP_CUSTOMER_ID', $('#customerID').val(), EMPTY_STRING, $('#customerID'));
		break;
	case 'lesseeCode':
		help(CURRENT_PROGRAM_ID, 'HLP_DEBTOR_CODE', $('#lesseeCode').val(), $('#customerID').val(), $('#lesseeCode'), function() {
		});
		break;
	case 'addressSerial':
		help('COMMON', 'HLP_CUST_ADDR_SL', $('#addressSerial').val(), $('#customerID').val(), $('#addressSerial'), function() {
		});
		break;
	case 'contactSerial':
		help('COMMON', 'HLP_CONTACT_SL', $('#contactSerial').val(), $('#customerID').val() + '|' + $('#addressSerial').val(), $('#contactSerial'), function() {
		});
		break;

	case 'branchCode':
		help('COMMON', 'HLP_BRANCH_CODE', $('#branchCode').val(), EMPTY_STRING, $('#branchCode'));
		break;
	}
}

function add() {
	$('#customerID').focus();

}
function modify() {
	$('#customerID').focus();

}
function loadData() {
	$('#preLeaseContractRefDate').val(validator.getValue('CONTRACT_DATE'));
	$('#preLeaseContractRefSerial').val(validator.getValue('CONTRACT_SL'));
	$('#customerID').val(validator.getValue('CUSTOMER_ID'));
	$('#customerID_desc').html(validator.getValue('F1_CUSTOMER_NAME'));
	$('#lesseeCode').val(validator.getValue('SUNDRY_DB_AC'));
	$('#productCode').val(validator.getValue('LEASE_PRODUCT_CODE'));
	$('#productCode_desc').html(validator.getValue('F2_DESCRIPTION'));
	$('#branchCode').val(validator.getValue('BRANCH_CODE'));
	$('#branchCode_desc').html(validator.getValue('F3_BRANCH_NAME'));
	$('#addressSerial').val(validator.getValue('ADDR_SL'));
	$('#contactSerial').val(validator.getValue('CONTACT_SL'));
	setCheckbox('interestApplicable', validator.getValue('INTERIM_INT_APPL'));
	interestApplicable_val(false);
	$('#interestRateInvestLeaseCommit').val(validator.getValue('INT_RATE_FOR_PRIOR_INVEST'));
	$('#interestBillingChoice').val(validator.getValue('INTERIM_INT_BILLING'));
	$('#interestDebitNotePrinting').val(validator.getValue('INT_DEBIT_NOTE_PRINT_CHOICE'));
	$('#remarks').val(validator.getValue('REMARKS'));
	resetLoading();
}
function view(source, primaryKey) {
	hideParent('lss/qpreleasecontreg', source, primaryKey);
	resetLoading();
}

function validate(id) {
	switch (id) {
	case 'customerID':
		customerID_val();
		break;
	case 'lesseeCode':
		lesseeCode_val();
		break;
	case 'branchCode':
		branchCode_val();
		break;
	case 'addressSerial':
		addressSerial_val();
		break;
	case 'contactSerial':
		contactSerial_val();
		break;
	case 'interestApplicable':
		interestApplicable_val(true);
		break;
	case 'interestRateInvestLeaseCommit':
		interestRateInvestLeaseCommit_val();
		break;
	case 'interestBillingChoice':
		interestBillingChoice_val();
		break;
	case 'interestDebitNotePrinting':
		interestDebitNotePrinting_val();
		break;
	case 'remarks':
		remarks_val();
		break;
	}
}

function checkclick(id) {
	switch (id) {
	case 'interestApplicable':
		interestApplicable_val(false);
		break;
	}
}

function backtrack(id) {
	switch (id) {
	case 'customerID':
		setFocusLast('customerID');
		break;
	case 'lesseeCode':
		setFocusLast('customerID');
		break;
	case 'branchCode':
		setFocusLast('lesseeCode');
		break;
	case 'addressSerial':
		setFocusLast('branchCode');
		break;
	case 'contactSerial':
		setFocusLast('addressSerial');
		break;
	case 'interestApplicable':
		setFocusLast('contactSerial');
		break;
	case 'interestRateInvestLeaseCommit':
		setFocusLast('interestApplicable');
		break;
	case 'interestBillingChoice':
		setFocusLast('interestRateInvestLeaseCommit');
		break;
	case 'interestDebitNotePrinting':
		setFocusLast('interestBillingChoice');
		break;
	case 'remarks':
		if ($('#interestApplicable').is(':checked'))
			setFocusLast('interestDebitNotePrinting');
		else
			setFocusLast('interestApplicable');
		break;
	}
}

function customerID_val() {
	var value = $('#customerID').val();
	clearError('customerID_error');
	if (isEmpty(value)) {
		setError('customerID_error', MANDATORY);
		return false;
	}
	if (!isNumeric(value)) {
		setError('customerID_error', NUMERIC_CHECK);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('CUSTOMER_ID', value);
	validator.setValue(ACTION, USAGE);
	validator.setClass('patterns.config.web.forms.lss.epreleasecontregbean');
	validator.setMethod('validateCustomerID');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('customerID_error', validator.getValue(ERROR));
		$('#customerID_desc').html(EMPTY_STRING);
		return false;
	}
	$('#customerID_desc').html(validator.getValue('CUSTOMER_NAME'));
	setFocusLast('lesseeCode');
	return true;
}

function lesseeCode_val() {
	var value = $('#lesseeCode').val();
	clearError('lesseeCode_error');
	if (isEmpty(value)) {
		setError('lesseeCode_error', MANDATORY);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('CONTRACT_DATE', $('#preLeaseContractRefDate').val());
	validator.setValue('CONTRACT_SL', $('#preLeaseContractRefSerial').val());
	validator.setValue('CUSTOMER_ID', $('#customerID').val());
	validator.setValue('SUNDRY_DB_AC', value);
	validator.setValue(ACTION, USAGE);
	if ($('#rectify').val() == ZERO)
		validator.setValue("LEASE_CONTRACT", ZERO);
	else
		validator.setValue("LEASE_CONTRACT", ONE);
	validator.setClass('patterns.config.web.forms.lss.epreleasecontregbean');
	validator.setMethod('validateLesseeCode');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('lesseeCode_error', validator.getValue(ERROR));
		$('#lesseeCode_desc').html(EMPTY_STRING);
		$('#productCode').val(EMPTY_STRING);
		$('#productCode_desc').html(EMPTY_STRING);
		return false;
	}
	$('#productCode').val(validator.getValue('LEASE_PRODUCT_CODE'));
	$('#productCode_desc').html(validator.getValue('LEASE_PRODUCT_CODE_DESC'));
	setFocusLast('branchCode');
	return true;
}

function branchCode_val() {
	var value = $('#branchCode').val();
	clearError('branchCode_error');
	if (isEmpty(value)) {
		setError('branchCode_error', MANDATORY);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('BRANCH_CODE', value);
	validator.setValue(ACTION, USAGE);
	validator.setClass('patterns.config.web.forms.lss.epreleasecontregbean');
	validator.setMethod('validateBranchCode');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('branchCode_error', validator.getValue(ERROR));
		$('#branchCode_desc').html(EMPTY_STRING);
		return false;
	}
	$('#branchCode_desc').html(validator.getValue('BRANCH_NAME'));
	setFocusLast('addressSerial');
	return true;
}

function addressSerial_val() {
	var value = $('#addressSerial').val();
	clearError('addressSerial_error');
	clearError('customerID_error');
	if (isEmpty($('#customerID').val())) {
		setError('customerID_error', MANDATORY);
		return false;
	}
	if (isEmpty(value)) {
		setError('addressSerial_error', MANDATORY);
		return false;
	}
	if (!isNumeric(value)) {
		setError('addressSerial_error', NUMERIC_CHECK);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('CUSTOMER_ID', $('#customerID').val());
	validator.setValue('ADDR_SL', value);
	validator.setValue(ACTION, USAGE);
	validator.setClass('patterns.config.web.forms.lss.epreleasecontregbean');
	validator.setMethod('validateAddressSerial');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('addressSerial_error', validator.getValue(ERROR));
		return false;
	}
	setFocusLast('contactSerial');
	return true;
}

function contactSerial_val() {
	var value = $('#contactSerial').val();
	clearError('contactSerial_error');
	clearError('addressSerial_error');
	if (isEmpty($('#addressSerial').val()) || isEmpty($('#customerID').val())) {
		setError('contactSerial_error', ADDR_CUSOMER_MANDATORY);
		return false;
	}
	if (isEmpty(value)) {
		setError('contactSerial_error', MANDATORY);
		return false;
	}
	if (!isNumeric(value)) {
		setError('contactSerial_error', NUMERIC_CHECK);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('CUSTOMER_ID', $('#customerID').val());
	validator.setValue('ADDR_SL', $('#addressSerial').val());
	validator.setValue('DTL_SL', value);
	validator.setValue(ACTION, USAGE);
	validator.setClass('patterns.config.web.forms.lss.epreleasecontregbean');
	validator.setMethod('validateContactSerial');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('contactSerial_error', validator.getValue(ERROR));
		return false;
	}
	setFocusLast('interestApplicable');
	return true;
}

function interestApplicable_val(valMode) {
	if ($('#interestApplicable').is(':checked')) {
		$('#interestRateInvestLeaseCommit').prop('readonly', false);
		$('#interestBillingChoice').prop('disabled', false);
		$('#interestDebitNotePrinting').prop('disabled', false);
		if (valMode)
			setFocusLast('interestRateInvestLeaseCommit');
	} else {
		$('#interestRateInvestLeaseCommit').val(EMPTY_STRING);
		$('#interestRateInvestLeaseCommit_error').html(EMPTY_STRING);
		$('#interestRateInvestLeaseCommit').prop('readonly', true);
		$('#interestBillingChoice').prop('selectedIndex', 0);
		$('#interestBillingChoice').prop('disabled', true);
		$('#interestDebitNotePrinting').prop('selectedIndex', 1);
		$('#interestDebitNotePrinting').prop('disabled', true);
		if (valMode)
			setFocusLast('remarks');
	}
	return true;
}

function interestRateInvestLeaseCommit_val() {
	var value = $('#interestRateInvestLeaseCommit').val();
	clearError('interestRateInvestLeaseCommit_error');
	if ($('#interestApplicable').is(':checked')) {
		if (isEmpty(value)) {
			setError('interestRateInvestLeaseCommit_error', MANDATORY);
			return false;
		}
		if (isZero(value)) {
			setError('interestRateInvestLeaseCommit_error', ZERO_CHECK);
			return false;
		}
		if (!isNumericWithDecimal(value)) {
			setError('interestRateInvestLeaseCommit_error', INVALID_PERCENTAGE);
			return false;
		}
		if (!isValidPercentage(value)) {
			setError('interestRateInvestLeaseCommit_error', INVALID_PERCENTAGE);
			return false;
		}
		$('#interestRateInvestLeaseCommit').val(parseFloat(value).toFixed(2));
	}
	setFocusLast('interestBillingChoice');
	return true;
}
function interestBillingChoice_val() {
	var value = $('#interestBillingChoice').val();
	clearError('interestBillingChoice_error');
	if ($('#interestApplicable').is(':checked')) {
		if (isEmpty(value)) {
			setError('interestBillingChoice_error', MANDATORY);
			return false;
		}
	}
	setFocusLast('interestDebitNotePrinting');
	return true;
}

function interestDebitNotePrinting_val() {
	var value = $('#interestDebitNotePrinting').val();
	clearError('interestDebitNotePrinting_error');
	if ($('#interestApplicable').is(':checked')) {
		if (isEmpty(value)) {
			setError('interestDebitNotePrinting_error', MANDATORY);
			return false;
		}
	}
	setFocusLast('remarks');
	return true;
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', INVALID_REMARKS);
			setFocusLast('remarks');
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', INVALID_REMARKS);
			setFocusLast('remarks');
			return false;
		}
	}
	if ($('#rectify').val() == COLUMN_ENABLE) {
		if (isEmpty(value)) {
			setError('remarks_error', MANDATORY);
			setFocusLast('remarks');
			return false;
		}
	}
	setFocusOnSubmit();
	return true;
}