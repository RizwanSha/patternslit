<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<%@taglib prefix="message" tagdir="/WEB-INF/tags/message"%>
<web:bundle baseName="patterns.config.web.forms.lss.Messages"
	var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/lss/qplcinterestled.js" />
	</web:dependencies>
	<web:loadPK />
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="qplcinterestled.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/lss/qplcinterestled" id="qplcinterestled"
				method="POST">
				<web:dividerBlock>
				</web:dividerBlock>
				<web:viewContent id="po_view1">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="qplcinterestled.preleaseref" var="program"
												mandatory="false" />
										</web:column>
										<web:column>
											<type:dateDaySL property="preLeaseRef" id="preLeaseRef" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="qplcinterestled.customername" var="program"
												mandatory="false" />
										</web:column>
										<web:column>
											<type:descriptionDisplay property="customerName"
												id="customerName" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
						</web:sectionBlock>

						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle width="180px" />
									<web:columnStyle width="60px" />
									<web:columnStyle width="220px" />
									<web:columnStyle />
								</web:columnGroup>

								<web:rowEven>
									<web:column>
										<web:legend key="qplcinterestled.betmonths" var="program" />
									</web:column>
									<web:column>
										<type:yearMonth property="fromMonth" id="fromMonth"
											datasourceid="COMMON_MONTH" />
									</web:column>
									<web:column>
										<web:legend key="qplcinterestled.and" var="program" />
									</web:column>
									<web:column>
										<type:yearMonth property="toMonth" id="toMonth"
											datasourceid="COMMON_MONTH" />
									</web:column>

									<web:column>
										<web:anchorMessage var="program" key="qplcinterestled.link"
											onclick="qpreleasecontreg()"
											style="text-decoration:underline;cursor:pointer" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>

						<web:viewContent id="hideSubmitt">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="40px" />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:button id="submit" key="qplcinterestled.submit"
												var="program" onclick="loadGridValues()" />
										</web:column>
										<web:column>
											<web:button id="reset" key="qplcinterestled.reset"
												var="program" onclick="clearFields()" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
						</web:viewContent>

						<web:section>
							<message:messageBox id="qplcinterestled" />
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column span="2">
										<web:grid height="320px" width="1146px"
											id="qplcinterestled_innerGrid"
											src="lss/qplcinterestled_innerGrid.xml" paging="true">
										</web:grid>
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
					</web:dividerBlock>
				</web:viewContent>
				<web:element property="command" />
				<web:element property="action" />
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
	<web:contextSearch />
</web:fragment>
