<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.lss.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/lss/qleasepdc.js" />
	</web:dependencies>
	<web:loadPK />
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="eleasepdc.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:viewContent id="po_view1">
				<web:dividerBlock>
					<web:sectionBlock width="width-100p" align="left">
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="190px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="eleasepdc.lessecode" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:lesseeCodeDisplay property="lesseCode" id="lesseCode" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="190px" />
									<web:columnStyle width="220px" />
									<web:columnStyle width="120px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowOdd>
									<web:column>
										<web:legend key="eleasepdc.customerid" var="program" />
									</web:column>
									<web:column>
										<type:customerIDDisplay property="customerID" id="customerID" />
									</web:column>
									<web:column>
										<web:legend key="eleasepdc.custname" var="program" />
									</web:column>
									<web:column>
										<type:dynamicDescriptionDisplay property="customerName" id="customerName" length="50" size="50" />
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="190px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="eleasepdc.aggrementno" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:agreementNoDisplay property="aggrementNumber" id="aggrementNumber" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="eleasepdc.scheduleid" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:scheduleIDDisplay property="scheduleID" id="scheduleID" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="eleasepdc.pdclotserial" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:pdcLotSerialDisplay property="pdcLotSerial" id="pdcLotSerial"  />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="eleasepdc.disablepdc" var="program" />
									</web:column>
									<web:column>
										<type:checkboxDisplay property="disablePdcLot" id="disablePdcLot" />
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
						<type:error property="pdcDetailsError" />
						<web:viewContent id="disablePdc">
							<web:viewContent id="bankDtl_view">
								<type:tabbarBody id="bankpdcDetails_TAB_0">
									<web:section>
										<web:table>
											<web:columnGroup>
												<web:columnStyle width="180px" />
												<web:columnStyle />
											</web:columnGroup>
											<web:rowEven>
												<web:column>
													<web:legend key="eleasepdc.pdclotdate" var="program"  />
												</web:column>
												<web:column>
													<type:dateDisplay property="pdclotDate" id="pdclotDate"/>
												</web:column>
											</web:rowEven>
											<web:rowOdd>
												<web:column>
													<web:legend key="eleasepdc.ifsc" var="program" />
												</web:column>
												<web:column>
													<type:ifscCodeDisplay property="ifscCode" id="ifscCode" />
												</web:column>
											</web:rowOdd>
											<web:rowEven>
												<web:column>
													<web:legend key="eleasepdc.bankame" var="program" mandatory="true" />
												</web:column>
												<web:column>
													<type:otherInformation50Display property="bankName" id="bankName" />
												</web:column>
											</web:rowEven>
											<web:rowOdd>
												<web:column>
													<web:legend key="eleasepdc.brnname" var="program" mandatory="true" />
												</web:column>
												<web:column>
													<type:descriptionDisplay property="branchName" id="branchName" />
												</web:column>
											</web:rowOdd>
											<web:rowEven>
												<web:column>
													<web:legend key="form.accountnumber" var="common" mandatory="true" />
												</web:column>
												<web:column>
													<type:otherInformation50Display property="accountNumber" id="accountNumber" />
												</web:column>
											</web:rowEven>
											<web:rowOdd>
												<web:column>
													<web:legend key="eleasepdc.accname" var="program" mandatory="true" />
												</web:column>
												<web:column>
													<type:descriptionDisplay property="accountName" id="accountName" />
												</web:column>
											</web:rowOdd>
											<web:rowEven>
												<web:column>
													<web:legend key="eleasepdc.bankingprocesscode" var="program" mandatory="true" />
												</web:column>
												<web:column>
													<type:processCodeDisplay property="bankingProcessCode" id="bankingProcessCode" />
												</web:column>
											</web:rowEven>
											<web:rowOdd>
												<web:column>
													<web:legend key="eleasepdc.noofchque" var="program" mandatory="true" />
												</web:column>
												<web:column>
													<type:threeDigitDisplay property="noOfChque" id="noOfChque" />
												</web:column>
											</web:rowOdd>
										</web:table>
									</web:section>
								</type:tabbarBody>
							</web:viewContent>
							<web:viewContent id="pdcDtl_view">
								<type:tabbarBody id="bankpdcDetails_TAB_1">
									<web:section>
										<web:table>
											<web:rowOdd>
												<web:column>
													<web:grid height="200px" width="622px" id="eleasepdc_innerGrid" src="lss/eleasepdc_innerGrid.xml">
													</web:grid>
												</web:column>
											</web:rowOdd>
										</web:table>
									</web:section>
								</type:tabbarBody>
							</web:viewContent>
							<type:tabbar height="height:350px;" name="Bank Details $$ PDC Details" width="width:1350px;" required="2" id="bankpdcDetails" selected="0">
							</type:tabbar>
						</web:viewContent>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="190px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="form.remarks" var="common" />
									</web:column>
									<web:column>
										<type:remarksDisplay property="remarks" id="remarks" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:auditDisplay />
					</web:sectionBlock>
				</web:dividerBlock>
			</web:viewContent>
		</web:viewSubPart>
	</web:viewPart>
</web:fragment>