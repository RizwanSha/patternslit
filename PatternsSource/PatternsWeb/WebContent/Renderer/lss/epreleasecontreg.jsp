<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.lss.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/lss/epreleasecontreg.js" />
	</web:dependencies>
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="epreleasecontreg.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/lss/epreleasecontreg" id="epreleasecontreg" method="POST">
				<web:dividerBlock>
					<type:optionBox add="true" modify="false" view="true" />
				</web:dividerBlock>
				<web:viewContent id="po_view1" styleClass="hidden">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="epreleasecontreg.preleasecon" var="program" />
										</web:column>
										<web:column>
											<type:dateDaySL property="preLeaseContractRef" id="preLeaseContractRef" readOnly="true" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
						</web:sectionBlock>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowOdd>
									<web:column>
										<web:legend key="epreleasecontreg.customerid" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:customerID property="customerID" id="customerID" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="epreleasecontreg.lesseecode" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:lesseeCode property="lesseeCode" id="lesseeCode" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="epreleasecontreg.productcode" var="program" />
									</web:column>
									<web:column>
										<type:leaseProductCode property="productCode" id="productCode" readOnly="true"/>
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="epreleasecontreg.branchcode" var="program" mandatory="true"/>
									</web:column>
									<web:column>
										<type:branch property="branchCode" id="branchCode"/>
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="epreleasecontreg.addrserial" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:addressSerial property="addressSerial" id="addressSerial" lookup="true" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="epreleasecontreg.contactserial" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:serial property="contactSerial" id="contactSerial" lookup="true"/>
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="epreleasecontreg.interestapplicable" var="program" />
									</web:column>
									<web:column>
										<type:checkbox property="interestApplicable" id="interestApplicable" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="epreleasecontreg.interestrateinvest" var="program" />
									</web:column>
									<web:column>
										<type:threeDigitTwoDecimalPercentage property="interestRateInvestLeaseCommit" id="interestRateInvestLeaseCommit" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="epreleasecontreg.interestbillingchoice" var="program" />
									</web:column>
									<web:column>
										<type:combo property="interestBillingChoice" id="interestBillingChoice" datasourceid="EPRELEASECONTREG_INT_BILL_OPTION" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="epreleasecontreg.debitnoteprinting" var="program" />
									</web:column>
									<web:column>
										<type:combo property="interestDebitNotePrinting" id="interestDebitNotePrinting" datasourceid="EPRELEASECONTREG_INT_DEBIT_PRINTING" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowOdd>
									<web:column>
										<web:legend key="form.remarks" var="common" />
									</web:column>
									<web:column>
										<type:remarks property="remarks" id="remarks" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column span="2">
										<web:submitReset />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
					</web:dividerBlock>
				</web:viewContent>
				<web:element property="command" />
				<web:element property="action" />
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
	<web:queryGrid />
	<web:TBAGrid />
	<web:contextSearch />
</web:fragment>
