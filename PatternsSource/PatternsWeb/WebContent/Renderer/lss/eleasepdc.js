var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'ELEASEPDC';
function init() {
	refreshTBAGrid();
	refreshQueryGrid();
	if (_redisplay) {
		if ($('#action').val() == ADD && $('#rectify').val() == COLUMN_DISABLE) {
			$('#disablePdcLot').prop('disabled', true);
			setCheckbox('disablePdcLot', NO);
			$('#pdcLotSerial').prop('readonly', true);
			$('#pdcLotSerial_pic').prop('disabled', true);
		}
		$('#chqAmount_curr').prop('readonly', true);
		if (!isEmpty($('#ifscCode').val())) {
			$('#bankName').prop('readonly', true);
			$('#branchName').prop('readonly', true);
		}
		eleasepdc_innerGrid.clearAll();
		if (!isEmpty($('#xmleleasepdcGrid').val())) {
			eleasepdc_innerGrid.loadXMLString($('#xmleleasepdcGrid').val());
		}
		lesseCode_val(false);
		disablePdcLot_val(false);
	}
}

function loadGrid() {
	loadGridQuery(CURRENT_PROGRAM_ID, 'ELEASEPDC_GRID', eleasepdc_innerGrid);
}

function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'ELEASEPDC_MAIN');
}

function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}

function add() {
	$('#lesseCode').focus();
	$('#pdcLotSerial').prop('readonly', true);
	$('#pdcLotSerial_pic').prop('disabled', true);
	$('#pdclotDate').val(getCBD());
	$('#disablePdcLot').prop('disabled', true);
	setCheckbox('disablePdcLot', NO);
}

function modify() {

}

function view(source, primaryKey) {
	hideParent('lss/qleasepdc', source, primaryKey);
	resetLoading();
}

function clearFields() {
	$('#lesseCode').val(EMPTY_STRING);
	$('#lesseCode_error').html(EMPTY_STRING);
	$('#customerID').val(EMPTY_STRING);
	$('#customerName').val(EMPTY_STRING);
	$('#aggrementNumber').val(EMPTY_STRING);
	$('#aggrementNumber_error').html(EMPTY_STRING);
	$('#scheduleID').val(EMPTY_STRING);
	$('#scheduleID_error').html(EMPTY_STRING);
	$('#pdcLotSerial').val(EMPTY_STRING);
	$('#pdcLotSerial_error').html(EMPTY_STRING);
	clearNonPKFields();

}

function clearNonPKFields() {
	if ($('#action').val() == ADD && $('#rectify').val() == COLUMN_DISABLE)
		$('#pdclotDate').val(getCBD());
	else
		$('#pdclotDate').val(EMPTY_STRING);
	show('disablePdc');
	$('#bankName').val(EMPTY_STRING);
	$('#bankName_error').html(EMPTY_STRING);
	setCheckbox('disablePdcLot', NO);
	eleasepdc_clearGridFields();
	$('#branchName').val(EMPTY_STRING);
	$('#branchName_error').html(EMPTY_STRING);
	$('#ifscCode').val(EMPTY_STRING);
	$('#ifscCode_desc').html(EMPTY_STRING);
	$('#ifscCode_error').html(EMPTY_STRING);
	$('#accountNumber').val(EMPTY_STRING);
	$('#accountNumber_error').html(EMPTY_STRING);
	$('#accountName').val(EMPTY_STRING);
	$('#accountName_error').html(EMPTY_STRING);
	$('#bankingProcessCode').val(EMPTY_STRING);
	$('#bankingProcessCode_error').html(EMPTY_STRING);
	$('#bankingProcessCode_desc').html(EMPTY_STRING);
	$('#noOfChque').val(EMPTY_STRING);
	$('#noOfChque_error').html(EMPTY_STRING);
	eleasepdc_innerGrid.clearAll();
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
	$('#chqAmount_curr').val(EMPTY_STRING);
	doTabbarClick('bankpdcDetails_0');
	bankpdcDetails._setTabActive('bankpdcDetails_0', true);
	$('#bankName').prop('readonly', false);
	$('#branchName').prop('readonly', false);
}

function eleasepdc_clearGridFields() {
	$('#chqInstrumentNo').val(EMPTY_STRING);
	$('#chqInstrumentNo_error').html(EMPTY_STRING);
	$('#chqDate').val(EMPTY_STRING);
	$('#chqDate_error').html(EMPTY_STRING);
	$('#aggregateLimit').val(EMPTY_STRING);
	$('#chqAmount').val(EMPTY_STRING);
	$('#chqAmount_curr').prop('readonly', true);
	$('#chqAmountFormat').val(EMPTY_STRING);
	$('#chqAmount_error').html(EMPTY_STRING);
}
function doclearfields(id) {
	switch (id) {
	case 'lesseCode':
		if (isEmpty($('#lesseCode').val())) {
			clearFields();
			break;
		}
	case 'aggrementNumber':
		if (isEmpty($('#aggrementNumber').val())) {
			$('#aggrementNumber_error').html(EMPTY_STRING);
			$('#aggrementNumber_desc').html(EMPTY_STRING);
			$('#scheduleID').val(EMPTY_STRING);
			$('#scheduleID_error').html(EMPTY_STRING);
			$('#scheduleID_desc').html(EMPTY_STRING);
			$('#pdcLotSerial').val(EMPTY_STRING);
			$('#pdcLotSerial_error').html(EMPTY_STRING);
			clearNonPKFields();
			break;
		}
	case 'scheduleID':
		if (isEmpty($('#scheduleID').val())) {
			$('#scheduleID_error').html(EMPTY_STRING);
			$('#scheduleID_desc').html(EMPTY_STRING);
			$('#pdcLotSerial').val(EMPTY_STRING);
			$('#pdcLotSerial_error').html(EMPTY_STRING);
			clearNonPKFields();
			break;
		}
	case 'pdcLotSerial':
		if (isEmpty($('#pdcLotSerial').val())) {
			$('#pdcLotSerial_error').html(EMPTY_STRING);
			clearNonPKFields();
			break;
		}
	}
}

function doHelp(id) {
	switch (id) {
	case 'lesseCode':
		help('COMMON', 'HLP_DEBTOR_CODE', $('#lesseCode').val(), EMPTY_STRING, $('#lesseCode'), function() {
		});
		break;
	case 'aggrementNumber':
		help('COMMON', 'HLP_LEASE_AGRMNT', $('#aggrementNumber').val(), $('#customerID').val(), $('#aggrementNumber'), function() {
		});
		break;
	case 'scheduleID':
		help('COMMON', 'HLP_LEASE_SCH_ID', $('#scheduleID').val(), $('#lesseCode').val() + '|' + $('#aggrementNumber').val(), $('#scheduleID'), function() {
		});
		break;
	case 'pdcLotSerial':
		help('COMMON', 'HLP_PDC_LOT_SL', $('#pdcLotSerial').val(), $('#lesseCode').val() + '|' + $('#aggrementNumber').val() + '|' + $('#scheduleID').val(), $('#pdcLotSerial'));
		break;
	case 'ifscCode':
		help('COMMON', 'HLP_IFSC_CODE', $('#ifscCode').val(), EMPTY_STRING, $('#ifscCode'));
		break;
	case 'bankingProcessCode':
		help('COMMON', 'HLP_BANK_PROC_CODE', $('#bankingProcessCode').val(), EMPTY_STRING, $('#bankingProcessCode'));
		break;
	}
}

function loadData() {
	$('#pdcLotSerial').prop('readonly', false);
	$('#pdcLotSerial_pic').prop('disabled', false);
	$('#disablePdcLot').prop('disabled', false);
	$('#lesseCode').val(validator.getValue('LESSEE_CODE'));
	$('#customerID').val(validator.getValue('F2_CUSTOMER_ID'));
	$('#customerName').val(validator.getValue('F2_CUSTOMER_NAME'));
	$('#aggrementNumber').val(validator.getValue('AGREEMENT_NO'));
	$('#scheduleID').val(validator.getValue('SCHEDULE_ID'));
	$('#pdcLotSerial').val(validator.getValue('PDC_LOT_SL'));
	setCheckbox('disablePdcLot', validator.getValue('LOT_DISABLED'));
	$('#pdclotDate').val(validator.getValue('DATE_OF_ENTRY'));
	$('#bankName').val(validator.getValue('CHQ_BANK'));
	$('#branchName').val(validator.getValue('CHQ_BRANCH'));
	$('#ifscCode').val(validator.getValue('CHQ_BRANCH_IFSC'));
	$('#accountNumber').val(validator.getValue('CHQ_AC_NO'));
	$('#accountName').val(validator.getValue('CHQ_AC_NAME'));
	$('#bankingProcessCode').val(validator.getValue('BANK_PROCESS_CODE'));
	$('#bankingProcessCode_desc').html(validator.getValue('F3_NAME'));
	$('#noOfChequeOld').val(validator.getValue('NOF_PDC'));
	$('#noOfChque').val(validator.getValue('NOF_PDC'));
	$('#chqAmount_curr').val(validator.getValue('F4_ASSET_CCY'));
	$('#remarks').val(validator.getValue('REMARKS'));
	if (!isEmpty($('#ifscCode').val())) {
		$('#bankName').prop('readonly', true);
		$('#branchName').prop('readonly', true);
	}
	disablePdcLot_val(false);
	loadGrid();
	getPDCCount();
	$('#lesseCode').focus();
	resetLoading();
}

function getPDCCount() {
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('LESSEE_CODE', $('#lesseCode').val());
	validator.setValue('AGREEMENT_NO', $('#aggrementNumber').val());
	validator.setValue('SCHEDULE_ID', $('#scheduleID').val());
	validator.setValue(ACTION, USAGE);
	validator.setClass('patterns.config.web.forms.lss.eleasepdcbean');
	validator.setMethod('getPaymentPDCCount');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('scheduleID_error', validator.getValue(ERROR));
		return false;
	}
	$('#pdcCount').val(validator.getValue('REPAY_COUNT'));
	return true;
}

function checkclick(id) {
	switch (id) {
	case 'disablePdcLot':
		disablePdcLot_val(false);
		break;
	}
}

function validate(id) {
	var valMode = true;
	switch (id) {
	case 'lesseCode':
		lesseCode_val(valMode);
		break;
	case 'aggrementNumber':
		aggrementNumber_val();
		break;
	case 'scheduleID':
		scheduleID_val();
		break;
	case 'pdcLotSerial':
		pdcLotSerial_val();
		break;
	case 'disablePdcLot':
		disablePdcLot_val(valMode);
		break;
	case 'ifscCode':
		ifscCode_val();
		break;
	case 'bankName':
		bankName_val();
		break;
	case 'branchName':
		branchName_val();
		break;
	case 'accountNumber':
		accountNumber_val();
		break;
	case 'accountName':
		accountName_val();
		break;
	case 'bankingProcessCode':
		bankingProcessCode_val();
		break;
	case 'noOfChque':
		noOfChque_val();
		break;
	case 'chqInstrumentNo':
		chqInstrumentNo_val(valMode);
		break;
	case 'chqDate':
		chqDate_val(valMode);
		break;
	case 'chqAmountFormat':
		chqAmountFormat_val(valMode);
		break;
	case 'remarks':
		remarks_val();
		break;
	}
}

function backtrack(id) {
	switch (id) {
	case 'lesseCode':
		setFocusLast('lesseCode');
		break;
	case 'aggrementNumber':
		setFocusLast('lesseCode');
		break;
	case 'scheduleID':
		setFocusLast('aggrementNumber');
		break;
	case 'pdcLotSerial':
		setFocusLast('scheduleID');
		break;
	case 'disablePdcLot':
		setFocusLast('pdcLotSerial');
		break;
	case 'ifscCode':
		if ($('#rectify').val() == COLUMN_ENABLE)
			setFocusLast('disablePdcLot');
		else
			setFocusLast('scheduleID');
		break;
	case 'bankName':
		setFocusLast('ifscCode');
		break;
	case 'branchName':
		setFocusLast('bankName');
		break;
	case 'accountNumber':
		if (isEmpty($('#ifscCode').val()))
			setFocusLast('branchName');
		else
			setFocusLast('ifscCode');
		break;
	case 'accountName':
		setFocusLast('accountNumber');
		break;
	case 'bankingProcessCode':
		setFocusLast('accountName');
		break;
	case 'noOfChque':
		setFocusLast('bankingProcessCode');
		break;
	case 'chqInstrumentNo':
		doTabbarClick('bankpdcDetails_0');
		bankpdcDetails._setTabActive('bankpdcDetails_0', true);
		setFocusLast('noOfChque');
		break;
	case 'chqDate':
		setFocusLast('chqInstrumentNo');
		break;
	case 'chqAmountFormat':
		setFocusLast('chqDate');
		break;
	case 'eleasepdc_innerGrid_add':
		setFocusLast('chqAmountFormat');
		break;
	case 'remarks':
		doTabbarClick('bankpdcDetails_1');
		bankpdcDetails._setTabActive('bankpdcDetails_1', true);
		setFocusLast('chqAmountFormat');
		break;
	}
}

function lesseCode_val(valMode) {
	var value = $('#lesseCode').val();
	clearError('lesseCode_error');
	if (isEmpty(value)) {
		setError('lesseCode_error', MANDATORY);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('SUNDRY_DB_AC', value);
	validator.setValue(ACTION, USAGE);
	validator.setClass('patterns.config.web.forms.lss.eleasepdcbean');
	validator.setMethod('validateLesseCode');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('lesseCode_error', validator.getValue(ERROR));
		$('#lesseCode_desc').html(EMPTY_STRING);
		$('#customerID').val(EMPTY_STRING);
		$('#customerName').val(EMPTY_STRING);
		return false;
	}
	$('#lesseCode_desc').html(validator.getValue("DESCRIPTION"));
	$('#customerID').val(validator.getValue("CUSTOMER_ID"));
	$('#customerName').val(validator.getValue("CUSTOMER_NAME"));
	if (valMode)
		setFocusLast('aggrementNumber');
	return true;
}

function aggrementNumber_val() {
	var value = $('#aggrementNumber').val();
	clearError('aggrementNumber_error');
	if (isEmpty(value)) {
		setError('aggrementNumber_error', MANDATORY);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('CUSTOMER_ID', $('#customerID').val());
	validator.setValue('AGREEMENT_NO', value);
	validator.setValue(ACTION, USAGE);
	validator.setClass('patterns.config.web.forms.lss.eleasepdcbean');
	validator.setMethod('validateAggrementNumber');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('aggrementNumber_error', validator.getValue(ERROR));
		return false;
	}
	setFocusLast('scheduleID');
	return true;
}

function scheduleID_val() {
	var value = $('#scheduleID').val();
	clearError('scheduleID_error');
	if (isEmpty(value)) {
		setError('scheduleID_error', MANDATORY);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('LESSEE_CODE', $('#lesseCode').val());
	validator.setValue('AGREEMENT_NO', $('#aggrementNumber').val());
	validator.setValue('SCHEDULE_ID', value);
	validator.setValue(ACTION, USAGE);
	validator.setClass('patterns.config.web.forms.lss.eleasepdcbean');
	validator.setMethod('validateScheduleID');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('scheduleID_error', validator.getValue(ERROR));
		return false;
	}
	$('#chqAmount_curr').val(validator.getValue("ASSET_CCY"));
	$('#pdcCount').val(validator.getValue('REPAY_COUNT'));
	doTabbarClick('bankpdcDetails_0');
	bankpdcDetails._setTabActive('bankpdcDetails_0', true);
	if ($('#action').val() == ADD && $('#rectify').val() == COLUMN_DISABLE)
		setFocusLast('ifscCode');
	else
		setFocusLast('pdcLotSerial');
	return true;
}

function pdcLotSerial_val() {
	var value = $('#pdcLotSerial').val();
	clearError('pdcLotSerial_error');
	if ($('#rectify').val() == COLUMN_ENABLE) {
		if (isEmpty(value)) {
			setError('pdcLotSerial_error', MANDATORY);
			return false;
		}
		if (!isNumeric(value)) {
			setError('pdcLotSerial_error', NUMERIC_CHECK);
			return false;
		} else {
			validator.clearMap();
			validator.setMtm(false);
			validator.setValue('LESSEE_CODE', $('#lesseCode').val());
			validator.setValue('AGREEMENT_NO', $('#aggrementNumber').val());
			validator.setValue('SCHEDULE_ID', $('#scheduleID').val());
			validator.setValue('PDC_LOT_SL', value);
			validator.setValue(ACTION, $('#action').val());
			validator.setClass('patterns.config.web.forms.lss.eleasepdcbean');
			validator.setMethod('validatePDCLotSerial');
			validator.sendAndReceive();
			if (validator.getValue(ERROR) != EMPTY_STRING) {
				setError('pdcLotSerial_error', validator.getValue(ERROR));
				return false;
			} else {
				PK_VALUE = getEntityCode() + PK_SEPERATOR + $('#lesseCode').val() + PK_SEPERATOR + $('#aggrementNumber').val() + PK_SEPERATOR + $('#scheduleID').val() + PK_SEPERATOR + $('#pdcLotSerial').val();
				if (!loadPKValues(PK_VALUE, 'pdcLotSerial_error')) {
					return false;
				}
			}
		}
	}
	doTabbarClick('bankpdcDetails_0');
	bankpdcDetails._setTabActive('bankpdcDetails_0', true);
	if ($('#action').val() == ADD && !$('#rectify').val() == COLUMN_ENABLE)
		setFocusLast('ifscCode');
	else
		setFocusLast('disablePdcLot');
	return true;
}
function disablePdcLot_val(valMode) {
	if ($('#disablePdcLot').is(':checked')) {
		hide('disablePdc');
		if (valMode)
			setFocusLast('remarks');
	} else {
		show('disablePdc');
		if (valMode)
			setFocusLast('ifscCode');
	}
}

function ifscCode_val() {
	var value = $('#ifscCode').val();
	clearError('ifscCode_error');
	if (!isEmpty(value)) {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('IFSC_CODE', value);
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.lss.eleasepdcbean');
		validator.setMethod('validateIFSCCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('ifscCode_error', validator.getValue(ERROR));
			$('#ifscCode_desc').html(EMPTY_STRING);
			$('#bankName').val(EMPTY_STRING);
			$('#branchName').val(EMPTY_STRING);
			return false;
		}
		$('#bankName').val(validator.getValue("BANK_NAME"));
		$('#branchName').val(validator.getValue("BRANCH_NAME"));
	}
	readOnlyFields($('#ifscCode').val());
	if (isEmpty(value))
		setFocusLast('bankName');
	else
		setFocusLast('accountNumber');
	return true;
}
function readOnlyFields(value) {
	if (isEmpty(value)) {
		$('#bankName').val(EMPTY_STRING);
		$('#bankName_error').html(EMPTY_STRING);
		$('#branchName').val(EMPTY_STRING);
		$('#branchName_error').html(EMPTY_STRING);
		$('#bankName').prop('readonly', false);
		$('#branchName').prop('readonly', false);
	} else {
		$('#bankName').prop('readonly', true);
		$('#branchName').prop('readonly', true);
	}

}

function bankName_val() {
	var value = $('#bankName').val();
	clearError('bankName_error');
	if (isEmpty(value)) {
		setError('bankName_error', MANDATORY);
		return false;
	}
	if (!isValidName(value)) {
		setError('bankName_error', INVALID_NAME);
		return false;
	}
	setFocusLast('branchName');
	return true;
}

function branchName_val() {
	var value = $('#branchName').val();
	clearError('branchName_error');
	if (isEmpty(value)) {
		setError('branchName_error', MANDATORY);
		return false;
	}
	if (!isValidName(value)) {
		setError('branchName_error', INVALID_NAME);
		return false;
	}
	setFocusLast('accountNumber');
	return true;
}

function accountNumber_val() {
	var value = $('#accountNumber').val();
	clearError('accountNumber_error');
	if (isEmpty(value)) {
		setError('accountNumber_error', MANDATORY);
		return false;
	}
	if (!isValidAccountNumber(value)) {
		setError('accountNumber_error', INVALID_ACNT_NUMBER);
		return false;
	}
	setFocusLast('accountName');
	return true;
}

function accountName_val() {
	var value = $('#accountName').val();
	clearError('accountName_error');
	if (isEmpty(value)) {
		setError('accountName_error', MANDATORY);
		return false;
	}
	if (!isValidName(value)) {
		setError('accountName_error', INVALID_NAME);
		return false;
	}
	setFocusLast('bankingProcessCode');
	return true;
}

function bankingProcessCode_val() {
	var value = $('#bankingProcessCode').val();
	clearError('bankingProcessCode_error');
	if (isEmpty(value)) {
		setError('bankingProcessCode_error', MANDATORY);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('BANK_PROCESS_CODE', value);
	validator.setValue(ACTION, USAGE);
	validator.setClass('patterns.config.web.forms.lss.eleasepdcbean');
	validator.setMethod('validateBankingProcessCode');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('bankingProcessCode_error', validator.getValue(ERROR));
		$('#bankingProcessCode_desc').html(EMPTY_STRING);
		return false;
	}
	$('#bankingProcessCode_desc').html(validator.getValue("NAME"));
	setFocusLast('noOfChque');
	return true;
}

function noOfChque_val() {
	var value = $('#noOfChque').val();
	var chequeCount = 0;
	clearError('noOfChque_error');
	if (isEmpty(value)) {
		setError('noOfChque_error', MANDATORY);
		return false;
	}
	if (isZero(value)) {
		setError('noOfChque_error', ZERO_CHECK);
		return false;
	}
	if (!isValidThreeDigit(value)) {
		setError('noOfChque_error', LENGTH_EXCEED_3);
		return false;
	}
	if (isNegative(value)) {
		setError('noOfChque_error', POSITIVE_CHECK);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('LESSEE_CODE', $('#lesseCode').val());
	validator.setValue('AGREEMENT_NO', $('#aggrementNumber').val());
	validator.setValue('SCHEDULE_ID', $('#scheduleID').val());
	validator.setValue(ACTION, USAGE);
	validator.setClass('patterns.config.web.forms.lss.eleasepdcbean');
	validator.setMethod('getChequeCount');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('noOfChque_error', validator.getValue(ERROR));
		return false;
	} else {
		if ($('#rectify').val() == COLUMN_DISABLE)
			$('#noOfChequeOld').val('0');
		chequeCount = parseInt(validator.getValue('CHEQUE_COUNT')) + parseInt(value) - parseInt($('#noOfChequeOld').val());
		if (chequeCount > $('#pdcCount').val()) {
			setError('noOfChque_error', formatMessage(TC_CANT_BE_GT_PDC_COUNT, [ $('#pdcCount').val() ]));
			return false;
		}
	}
	doTabbarClick('bankpdcDetails_1');
	bankpdcDetails._setTabActive('bankpdcDetails_1', true);
	setFocusLast('chqInstrumentNo');
	return true;
}

function chqInstrumentNo_val(valMode) {
	var value = $('#chqInstrumentNo').val();
	clearError('chqInstrumentNo_error');
	if (isEmpty(value)) {
		setError('chqInstrumentNo_error', MANDATORY);
		return false;
	}
	if (isZero(value)) {
		setError('chqInstrumentNo_error', ZERO_CHECK);
		return false;
	}
	if (!isNumeric(value)) {
		setError('chqInstrumentNo_error', NUMERIC_CHECK);
		return false;
	}
	if (isNegative(value)) {
		setError('chqInstrumentNo_error', POSITIVE_CHECK);
		return false;
	}
	if (valMode)
		setFocusLast('chqDate');
	return true;
}

function chqDate_val(valMode) {
	var value = $('#chqDate').val();
	clearError('chqDate_error');
	if (isEmpty(value)) {
		setError('chqDate_error', MANDATORY);
		return false;
	}
	if (!isDate(value)) {
		setError('chqDate_error', INVALID_DATE);
		return false;
	}
	if (isDateLesserEqual(value, getCBD())) {
		setError('chqDate_error', DATE_GCBD);
		return false;
	}
	setFocusLast('chqAmountFormat');
	return true;
}

function chqAmountFormat_val(valMode) {
	var value = unformatAmount($('#chqAmountFormat').val());
	clearError('chqAmount_error');
	if (isEmpty(value)) {
		setError('chqAmount_error', MANDATORY);
		return false;
	}
	$('#chqAmount').val(value);
	if (isZero(value)) {
		setError('chqAmount_error', ZERO_CHECK);
		return false;
	}
	if (isNegativeAmount(value)) {
		setError('chqAmount_error', POSITIVE_AMOUNT);
		return false;
	}
	if (!isCurrencySmallAmount($('#chqAmount_curr').val(), value)) {
		setError('chqAmount_error', INVALID_SMALL_AMOUNT);
		return false;
	}
	$('#chqAmountFormat').val(formatAmount(value, $('#chqAmount_curr').val()));
	if (valMode)
		setFocusLastOnGridSubmit('eleasepdc_innerGrid');
	return true;
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', INVALID_REMARKS);
			setFocusLast('remarks');
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', INVALID_REMARKS);
			setFocusLast('remarks');
			return false;
		}
	}
	if ($('#rectify').val() == COLUMN_ENABLE) {
		if (isEmpty(value)) {
			setError('remarks_error', MANDATORY);
			setFocusLast('remarks');
			return false;
		}
	}
	setFocusOnSubmit();
	return true;
}

function eleasepdc_addGrid(editMode, editRowId) {
	if (noOfChque_val(false)) {
		if (chqInstrumentNo_val(false) && chqDate_val(false) && chqAmountFormat_val(false)) {
			if (eleasepdc_gridDuplicateCheck(editMode, editRowId)) {
				if (eleasepdc_innerGrid.getRowsNum() > $('#noOfChque').val() - 1 && !editMode) {
					setError('chqInstrumentNo_error', NO_OF_SLAB_VALUE);
					return false;
				}
				var field_values = [ 'false', $('#chqInstrumentNo').val(), $('#chqDate').val(), $('#chqAmountFormat').val(), $('#chqAmount').val() ];
				_grid_updateRow(eleasepdc_innerGrid, field_values);
				eleasepdc_clearGridFields();
				$('#chqInstrumentNo').focus();
				return true;
			} else {
				setError('chqInstrumentNo_error', AGGR_COMB_EXISTS);
				$('#chqInstrumentNo').focus();
				return false;
			}
		} else {
			return false;
		}
	} else {
		setError('chqInstrumentNo_error', NO_OF_CHQUE);
		$('#chqInstrumentNo').focus();
		return false;
	}
}

function eleasepdc_gridDuplicateCheck(editMode, editRowId) {
	var currentValue = [ $('#chqInstrumentNo').val() ];
	return _grid_duplicate_check(eleasepdc_innerGrid, currentValue, [ 1 ]);
}

function eleasepdc_editGrid(rowId) {
	$('#chqInstrumentNo').val(eleasepdc_innerGrid.cells(rowId, 1).getValue());
	$('#chqDate').val(eleasepdc_innerGrid.cells(rowId, 2).getValue());
	$('#chqAmountFormat').val(eleasepdc_innerGrid.cells(rowId, 3).getValue());
	$('#chqInstrumentNo').focus();

}
function gridExit(id) {
	switch (id) {
	case 'chqInstrumentNo':
	case 'chqDate':
	case 'chqAmountFormat':
		$('#xmleleasepdcGrid').val(eleasepdc_innerGrid.serialize());
		eleasepdc_clearGridFields();
		setFocusLast('remarks');
		break;
	}
}

function eleasepdc_cancelGrid(rowId) {
	eleasepdc_clearGridFields();
}

function revalidate() {
	eleasepdc_revaildateGrid();
}

function eleasepdc_revaildateGrid() {
	if (eleasepdc_innerGrid.getRowsNum() > 0) {
		$('#xmleleasepdcGrid').val(eleasepdc_innerGrid.serialize());
		$('#chqInstrumentNo_error').html(EMPTY_STRING);
		$('#pdcDetailsError_error').html(EMPTY_STRING);
	} else {
		eleasepdc_innerGrid.clearAll();
		$('#xmleleasepdcGrid').val(eleasepdc_innerGrid.serialize());
		setError('chqInstrumentNo_error', ADD_ATLEAST_ONE_ROW);
		setError('pdcDetailsError_error', ADD_ATLEAST_ONE_ROW);
		errors++;
	}
	if (eleasepdc_innerGrid.getRowsNum() != $('#noOfChque').val()) {
		setError('chqInstrumentNo_error', CHEQUE_GRID_VALUE_SAME);
		errors++;
	}
}

function doTabbarClick(id) {
	if (id == 'bankpdcDetails_0') {
		$('#bankpdcDetails').height('350px');
	} else {
		$('#pdcDetailsError_error').html(EMPTY_STRING);
		$('#bankpdcDetails').height('380px');
	}
}
