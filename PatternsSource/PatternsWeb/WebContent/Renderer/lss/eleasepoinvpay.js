var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'ELEASEPOINVPAY';
var currencySubUnits = getCurrencyUnit(getBaseCurrency());
var cbd = getCBD();
var poDate = EMPTY_STRING;
var eqLocal = EMPTY_STRING;
var amtPaid = EMPTY_STRING;
function init() {
	pgmGrid.attachEvent("onRowSelect", doRowSelect);
	pgmGrid.attachEvent("onCheck", doRowSelect);
	refreshTBAGrid();
	refreshQueryGrid();
	if (_redisplay) {
		errorDisplay();
		if (!isEmpty($('#xmlGrid').val())) {
			pgmGrid.loadXMLString($('#xmlGrid').val());
		}
		if ($('#action').val() == ADD) {
			$('#paymentSl').prop('readOnly', true);
		} else {
			$('#paymentSl').prop('readOnly', false);
		}
		$('#eqLocalFormat').prop('readOnly', true);
		$('#amtPaid_curr').prop('readOnly', true);
		$('#eqLocal_curr').prop('readOnly', true);
		if ($('#action').val() == RECTIFY) {
			pgmGrid.cells2(0, 0).setValue(ONE);
		}
		preLeaseRefSerial_val();
	}
	setFocusLast('preLeaseRefDate');
}
function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'ELEASEPOINVPAY_MAIN', queryGrid);
}
function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}
function view(source, primaryKey) {
	hideParent('lss/qleasepoinvpay', source, primaryKey, '1300');
	resetLoading();
}
function doHelp(id) {
	switch (id) {

	case 'preLeaseRef':
	case 'preLeaseRefDate':
	case 'preLeaseRefSerial':
		help('COMMON', 'HLP_PRE_LEASE_CONT_REF', $('#preLeaseRefDate').val(), $('#preLeaseRefDate').val(), $('#preLeaseRefDate'), null, function(gridObj, rowID) {
			$('#preLeaseRefDate').val(gridObj.cells(rowID, 0).getValue());
			$('#preLeaseRefSerial').val(gridObj.cells(rowID, 1).getValue());
		});
		break;
	case 'paymentSl':
		if (!isEmpty($('#preLeaseRefDate').val())) {
			help('COMMON', 'HLP_LEASE_PO_INV_PAY_SL', $('#paymentSl').val(), $('#preLeaseRefDate').val() + PK_SEPERATOR + $('#preLeaseRefSerial').val(), $('#paymentSl'), function() {
			});
		} else {
			setError('preLeaseRef_error', MANDATORY);
		}
		break;
	case 'selectedInvSl':
		if ($('#payFor').val() == 'I') {
			if (pgmGrid.getCheckedRows(0) != EMPTY_STRING) {
				help('ELEASEPOINVPAY', 'HLP_INVOICE_SERIAL', $('#selectedInvSl').val(), $('#preLeaseRefDate').val() + PK_SEPERATOR + $('#preLeaseRefSerial').val() + PK_SEPERATOR + $('#selectedPoSl').val(), $('#selectedInvSl'), function() {
				});
			} else {
				setError('payFor_error', SELECT_ATLEAST_ONE);
				errors++;
			}
		}
		break;
	}
}

function change(id) {
	switch (id) {
	case 'payFor':
		payFor_val();
		break;
	}
}

function viewpoSerial(pk) {
	var poSerial = pk;
	var primarykey = getEntityCode() + PK_SEPERATOR + $('#preLeaseRefDate').val() + PK_SEPERATOR + $('#preLeaseRefSerial').val() + PK_SEPERATOR + poSerial;
	var params = PK + '=' + primarykey + '&' + SOURCE + '=' + MAIN;
	showWindow('PREVIEW_VIEW', getBasePath() + 'Renderer/lss/qleasepo.jsp', 'Lease PO Details', params, true, false, false, 980, 650);
	resetLoading();
}

function viewinvoiceSerial(pk) {
	var primaryKey = pk.split("|");
	var poSerial = primaryKey[0];
	var invSerial = primaryKey[1];
	var primarykey = getEntityCode() + PK_SEPERATOR + $('#preLeaseRefDate').val() + PK_SEPERATOR + $('#preLeaseRefSerial').val() + PK_SEPERATOR + poSerial + PK_SEPERATOR + invSerial;
	var params = PK + '=' + primarykey + '&' + SOURCE + '=' + MAIN;
	showWindow('PREVIEW_VIEW', getBasePath() + 'Renderer/lss/qleasepoinv.jsp', 'Lease PO - Invoice Details', params, true, false, false, 980, 650);
	resetLoading();
}

function clearFields() {
	$('#preLeaseRef').val(EMPTY_STRING);
	$('#preLeaseRefDate').val(EMPTY_STRING);
	$('#preLeaseRefSerial').val(EMPTY_STRING);
	$('#preLeaseRef_error').html(EMPTY_STRING);
	$('#paymentSl').val(EMPTY_STRING);
	$('#paymentSl_desc').html(EMPTY_STRING);
	$('#paymentSl_error').html(EMPTY_STRING);
	clearNonPKFields(true);
}

function clearNonPKFields(valmode) {
	$('#leaseCode').val(EMPTY_STRING);
	$('#leaseCode_error').html(EMPTY_STRING);
	$('#custId').val(EMPTY_STRING);
	$('#custName').val(EMPTY_STRING);
	$('#agreeNo').val(EMPTY_STRING);
	$('#agreeNo_error').html(EMPTY_STRING);
	pgmGrid.clearAll();
	$('#selectedPoSl').val(EMPTY_STRING);
	$('#selectedInvSl').val(EMPTY_STRING);
	$('selectedInvSl_pic').prop('disabled', true);
	$('#selectedInvSlDisplay').val(EMPTY_STRING);
	$('#selectedInvSl').prop('readOnly', true);
	if ($('#action').val() == ADD) {
		$('#paymentSl').prop('readOnly', true);
	} else {
		$('#paymentSl').prop('readOnly', false);
	}
	$('#payFor').prop('selectedIndex', ZERO);
	$('#payFor_error').html(EMPTY_STRING);
	$('#payFavour').val(EMPTY_STRING);
	$('#payFavour_desc').html(EMPTY_STRING);
	$('#scheduleId').val(EMPTY_STRING);
	$('#scheduleId_error').html(EMPTY_STRING);
	$('#scheduleId_desc').html(EMPTY_STRING);
	$('#date').val(EMPTY_STRING);
	$('#date_error').html(EMPTY_STRING);
	$('#amtPaid').val(EMPTY_STRING);
	$('#amtPaidFormat').val(EMPTY_STRING);
	$('#amtPaid_curr').val(EMPTY_STRING);
	$('#amtPaid_error').html(EMPTY_STRING);
	$('#amtPaidFormat').prop('readOnly', false);
	$('#eqLocalFormat').prop('readOnly', true);
	$('#amtPaid_curr').prop('readOnly', true);
	$('#eqLocal_curr').prop('readOnly', true);
	$('#eqLocal').val(EMPTY_STRING);
	$('#eqLocalFormat').val(EMPTY_STRING);
	$('#eqLocal_curr').val(EMPTY_STRING);
	$('#eqLocal_error').html(EMPTY_STRING);
	$('#payRef').val(EMPTY_STRING);
	$('#payRef_error').html(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
	$('#amtPaidHidden').val(EMPTY_STRING);
	$('#eqLocalHidden').val(EMPTY_STRING);
	$('#currHidden').val(EMPTY_STRING);
	$('#agreeNo').val(EMPTY_STRING);
	$('#agreeNo_error').html(EMPTY_STRING);
}

function add() {
	show('scheduleDetail');
	$('#paymentSl').prop('readOnly', true);
	$('#paymentSl_pic').prop('disabled', true);
	$('#preLeaseRefDate').focus();

}

function modify() {

}
function loadData() {
	$('#preLeaseRefDate').val(validator.getValue('CONTRACT_DATE'));
	$('#preLeaseRefSerial').val(validator.getValue('CONTRACT_SL'));
	$('#action').val(RECTIFY);
	$('#paymentSl').val(validator.getValue('PAYMENT_SL'));
	$('#paymentSl').prop('readOnly', false);
	$('#selectedPoSl').val(validator.getValue('PO_SL'));
	$('#selectedPoSlDisplay').val($('#selectedPoSl').val());
	$('#selectedInvSl').val(validator.getValue('INVOICE_SL'));
	$('#selectedPoSlDisplay').val($('#selectedPoSl').val());
	$('#selectedInvSlDisplay').val($('#selectedInvSl').val());
	$('#payFor').val(validator.getValue('PAY_FOR'));
	$('#date').val(validator.getValue('PAYMENT_DATE'));
	$('#amtPaid_curr').val(validator.getValue('PAYMENT_CCY'));
	$('#amtPaidFormat').val(formatAmount(validator.getValue('PAYMENT_AMOUNT').toString(), $('#amtPaid_curr').val(), currencySubUnits));
	$('#amtPaid').val(unformatAmount($('#amtPaidFormat').val()));
	$('#eqLocal_curr').val(validator.getValue('PAYMENT_CCY'));
	$('#eqLocalFormat').val(formatAmount(validator.getValue('PAYMENT_AMOUNT_BC').toString(), $('#eqLocal_curr').val(), currencySubUnits));
	$('#eqLocal').val(unformatAmount($('#eqLocalFormat').val()));
	$('#payRef').val(validator.getValue('PAYMENT_REFERENCE'));
	$('#remarks').val(validator.getValue('REMARKS'));
	$('#action').val(RECTIFY);
	$('#paymentSl').prop('readOnly', false);
	$('#paymentSl_pic').prop('disabled', false);
	preLeaseRefSerial_val();
	pgmGrid.cells2(0, 0).setValue(ONE);
	$('#payFavour').val(pgmGrid.cells2(0, 9).getValue());
	$('#payFavour_desc').html(pgmGrid.cells2(0, 10).getValue());
	setFocusLast('paymentSl');
}

function backtrack(id) {
	switch (id) {
	case 'preLeaseRefDate':
		setFocusLast('preLeaseRefDate');
		break;
	case 'preLeaseRefSerial':
		setFocusLast('preLeaseRefDate');
		break;
	case 'paymentSl':
		setFocusLast('preLeaseRefSerial');
		break;
	case 'payFor':
		if ($('#action').val() == RECTIFY) {
			setFocusLast('paymentSl');
		} else {
			setFocusLast('preLeaseRefSerial');
		}
		break;
	case 'selectedInvSl':
		setFocusLast('payFor');
		break;
	case 'date':
		if ($('#payFor').val() == 'A' || $('#payFor').val() == 'L')
			setFocusLast('payFor');
		else
			setFocusLast('selectedInvSl');
		break;

	case 'amtPaidFormat':
		setFocusLast('date');
		break;
	case 'payRef':
		setFocusLast('amtPaidFormat');
		break;
	case 'remarks':
		setFocusLast('payRef');
		break;
	}
}

function doclearfields(id) {
	switch (id) {
	case 'preLeaseRefDate':
		if (isEmpty($('#preLeaseRefDate').val())) {
			$('#preLeaseRefSerial').val(EMPTY_STRING);
			$('#preLeaseRef_error').html(EMPTY_STRING);
			$('#paymentSl').val(EMPTY_STRING);
			$('#paymentSl_desc').html(EMPTY_STRING);
			$('#paymentSl_error').html(EMPTY_STRING);
			clearNonPKFields(true);
			break;
		}
	case 'preLeaseRefSerial':
		if (isEmpty($('#preLeaseRefSerial').val())) {
			$('#preLeaseRef_error').html(EMPTY_STRING);
			$('#paymentSl').val(EMPTY_STRING);
			$('#paymentSl_desc').html(EMPTY_STRING);
			$('#paymentSl_error').html(EMPTY_STRING);
			clearNonPKFields(true);
			break;
		}
	case 'paymentSl':
		if (isEmpty($('#paymentSl').val())) {
			$('#payFor').prop('selectedIndex', ZERO);
			$('#payFavour').val(EMPTY_STRING);
			$('#payFavour_desc').html(EMPTY_STRING);
			$('#scheduleId').val(EMPTY_STRING);
			$('#scheduleId_error').html(EMPTY_STRING);
			$('#scheduleId_desc').html(EMPTY_STRING);
			$('#date').val(EMPTY_STRING);
			$('#date_error').html(EMPTY_STRING);
			$('#amtPaid').val(EMPTY_STRING);
			$('#amtPaidFormat').val(EMPTY_STRING);
			$('#amtPaid_curr').val(EMPTY_STRING);
			$('#amtPaid_error').html(EMPTY_STRING);
			$('#eqLocalFormat').prop('readOnly', true);
			$('#eqLocal_curr').prop('readOnly', true);
			$('#eqLocal').val(EMPTY_STRING);
			$('#eqLocalFormat').val(EMPTY_STRING);
			$('#eqLocal_curr').val(EMPTY_STRING);
			$('#eqLocal_error').html(EMPTY_STRING);
			$('#payRef').val(EMPTY_STRING);
			$('#payRef_error').html(EMPTY_STRING);
			$('#remarks').val(EMPTY_STRING);
			$('#remarks_error').html(EMPTY_STRING);
			$('#amtPaidHidden').val(EMPTY_STRING);
			$('#eqLocalHidden').val(EMPTY_STRING);
			$('#currHidden').val(EMPTY_STRING);
			break;
		}
	}
}

function validate(id) {
	switch (id) {
	case 'preLeaseRefDate':
		preLeaseRefDate_val();
		break;
	case 'preLeaseRefSerial':
		preLeaseRefSerial_val();
		break;
	case 'payFor':
		payFor_val();
		break;
	case 'paymentSl':
		paymentSl_val();
		break;
	case 'selectedInvSl':
		selectedInvSl_val();
		break;
	case 'date':
		date_val();
		break;
	case 'amtPaidFormat':
		amtPaidFormat_val();
		break;
	case 'eqLocalFormat':
		eqLocalFormat_val();
		break;
	case 'payRef':
		payRef_val();
		break;
	case 'remarks':
		remarks_val();
		break;
	}
}

function preLeaseRefDate_val() {
	var value = $('#preLeaseRefDate').val();
	clearError('preLeaseRef_error');
	if (isEmpty(value)) {
		setError('preLeaseRef_error', MANDATORY);
		return false;
	}
	if (!isDate(value)) {
		setError('preLeaseRef_error', INVALID_DATE);
		return false;
	}
	setFocusLast('preLeaseRefSerial');
	return true;
}

function preLeaseRefSerial_val() {
	var value = $('#preLeaseRefSerial').val();
	clearError('preLeaseRef_error');
	clearError('paymentSl_error');
	if (isEmpty(value)) {
		setError('preLeaseRef_error', MANDATORY);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('CONTRACT_DATE', $('#preLeaseRefDate').val());
		validator.setValue('CONTRACT_SL', $('#preLeaseRefSerial').val());
		validator.setValue(ACTION, $('#action').val());
		validator.setValue('PO_SL', $('#selectedPoSl').val());
		validator.setValue('INV_SL', $('#selectedInvSl').val());
		validator.setClass('patterns.config.web.forms.lss.eleasepoinvpaybean');
		validator.setMethod('validatePreLeaseContractRefSerial');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('preLeaseRef_error', validator.getValue(ERROR));
			$('#custId').val(EMPTY_STRING);
			$('#custName').val(EMPTY_STRING);
			$('#leaseCode').val(EMPTY_STRING);
			$('#agreeNo').val(EMPTY_STRING);
			$('#scheduleId').val(EMPTY_STRING);
			$('#payFavour').val(EMPTY_STRING);
			pgmGrid.clearAll();
			return false;
		} else {
			if (isEmpty(validator.getValue("SCHEDULE_ID"))) {
				$('#agreeNo').val(EMPTY_STRING);
				$('#scheduleId').val(EMPTY_STRING);
				hide('scheduleDetail');
			} else {
				show('scheduleDetail');
				$('#agreeNo').val(validator.getValue("AGREEMENT_NO"));
				$('#scheduleId').val(validator.getValue("SCHEDULE_ID"));
			}
			$('#custId').val(validator.getValue("CUSTOMER_ID"));
			$('#custName').val(validator.getValue("CUSTOMER_NAME"));
			$('#leaseCode').val(validator.getValue("SUNDRY_DB_AC"));
			var result = validator.getValue(RESULT_XML);
			pgmGrid.loadXMLString(result);
		}
	}
	if ($('#action').val() == RECTIFY) {
		setFocusLast('paymentSl');
	} else {
		setFocusLast('payFor');
	}
	return true;
}
function payFor_val() {
	var rowID = pgmGrid.getCheckedRows(0);
	if (rowID == EMPTY_STRING) {
		alert(SELECT_ATLEAST_ONE);
		errors++;
		setError('gridError_error', ATLEAST_ONE_RECORD);
		return false;
	} else {
		clearError('gridError_error');
	}
	var value = $('#payFor').val();
	clearError('payFor_error');
	clearError('selectedInvSl_error');
	if (value == 'A') {
		$('#selectedInvSl').val(EMPTY_STRING);
		$('#selectedInvSlDisplay').val(EMPTY_STRING);
		$('selectedInvSl_pic').prop('disabled', true);
		$('#selectedInvSl').prop('readOnly', true);
		setFocusLast('date');
	} else if (value == 'I') {
		$('#selectedInvSl').val(EMPTY_STRING);
		$('#selectedInvSlDisplay').val(EMPTY_STRING);
		$('#selectedInvSl').prop('readOnly', false);
		$('selectedInvSl_pic').prop('disabled', false);
		setFocusLast('selectedInvSl');
	} else if (value == 'L') {
		$('selectedInvSl_pic').prop('disabled', true);
		$('#selectedInvSl').prop('readOnly', true);
		setError('payFor_error', CHOICE_NOT_ALLOW);
		return false;
	}
	return true;
}

function selectedInvSl_val() {
	var value = $('#selectedInvSl').val();
	clearError('selectedInvSl_error');
	if ($('#payFor').val() == 'I') {
		if (isEmpty(value)) {
			setError('selectedInvSl_error', MANDATORY);
			return false;
		}
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('CONTRACT_DATE', $('#preLeaseRefDate').val());
		validator.setValue('CONTRACT_SL', $('#preLeaseRefSerial').val());
		validator.setValue('PO_SL', $('#selectedPoSlDisplay').val());
		validator.setValue('INVOICE_SL', $('#selectedInvSl').val());
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.lss.eleasepoinvpaybean');
		validator.setMethod('validateInvSerial');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('selectedInvSl_error', validator.getValue(ERROR));
			return false;
		}
	}
	setFocusLast('date');
	return true;
}

function doRowSelect(rowID) {
	
	var checkedrows = pgmGrid.getCheckedRows(0).split(',');
	pgmGrid.clearSelection();
	pgmGrid.selectRowById(rowID, true);
	pgmGrid.cells(rowID, 0).setValue(true);
	if (checkedrows.length > 1) {
		alert(SELECT_ONE);
		return false;
	}
	$('#gridError_error').html(EMPTY_STRING);
	$('#amtPaid_curr').val(pgmGrid.cells2(rowID, 4).getValue());
	$('#eqLocal_curr').val(pgmGrid.cells2(rowID, 4).getValue());
	var selectedSl = pgmGrid.cells2(rowID, 1).getValue().split("^");
	$('#selectedPoSl').val(selectedSl[0]);
	$('#selectedPoSlDisplay').val($('#selectedPoSl').val());
	$('#payFavour').val(pgmGrid.cells2(rowID, 9).getValue());
	$('#payFavour_desc').html(pgmGrid.cells2(rowID, 10).getValue());
	poDate = pgmGrid.cells2(rowID, 2).getValue();

}

function paymentSl_val() {
	var value = $('#paymentSl').val();
	clearError('paymentSl_error');
	if ($('#action').val() == RECTIFY) {
		var rowID = pgmGrid.getCheckedRows(0);
		if (rowID == EMPTY_STRING) {
			setError('paymentSl_error', SELECT_ATLEAST_ONE);
			errors++;
			return false;
		}
		if (isEmpty(value)) {
			setError('paymentSl_error', MANDATORY);
			return false;
		}
		if (!isNumeric(value)) {
			setError('paymentSl_error', INVALID_NUMBER);
			return false;
		}
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('ENTITY_CODE', getEntityCode());
		validator.setValue('CONTRACT_DATE', $('#preLeaseRefDate').val());
		validator.setValue('CONTRACT_SL', $('#preLeaseRefSerial').val());
		validator.setValue('PO_SL', $('#selectedPoSlDisplay').val());
		validator.setValue('INV_SL', $('#selectedInvSlDisplay').val());
		validator.setValue('PAYMENT_SL', $('#paymentSl').val());
		validator.setValue('ACTION', $('#action').val());
		validator.setClass('patterns.config.web.forms.lss.eleasepoinvpaybean');
		validator.setMethod('validatePaymentSl');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('paymentSl_error', validator.getValue(ERROR));
			return false;
		} else {
			clearNonPKFields();
			PK_VALUE = getEntityCode() + PK_SEPERATOR + $('#preLeaseRefDate').val() + PK_SEPERATOR + $('#preLeaseRefSerial').val() + PK_SEPERATOR + $('#paymentSl').val(); // 
			loadPKValues(PK_VALUE, 'paymentSl_error');
		}
	}
	setFocusLast('date');
	return true;
}

function date_val() {
	var value = $('#date').val();
	var rowID = pgmGrid.getCheckedRows(0);
	clearError('date_error');
	if (isEmpty(value)) {
		$('#date').val(cbd);
		return true;
	}
	if (!isDate(value)) {
		setError('date_error', INVALID_DATE);
		return false;
	}
	if (isDateGreater(value, cbd)) {
		setError('date_error', DATE_LECBD);
		return false;
	}
	if (isDateLesser(value, pgmGrid.cells2(rowID, 2).getValue())) {
		setError('date_error', DATE_LESS_PO_DATE);
		return false;
	}
	setFocusLast('amtPaidFormat');
	return true;
}

function amtPaidFormat_val() {
	var value = unformatAmount($('#amtPaidFormat').val());
	clearError('amtPaid_error');
	var rowID = pgmGrid.getCheckedRows(0);
	if (rowID == EMPTY_STRING) {
		alert(SELECT_ATLEAST_ONE);
		errors++;
		return false;
	}
	if (isEmpty(value)) {
		setError('amtPaid_error', MANDATORY);
		return false;
	}
	if (parseInt(value) + parseInt(pgmGrid.cells2(rowID, 8).getValue()) > parseInt(pgmGrid.cells2(rowID, 6).getValue())) {
		setError('amtPaid_error', TOTAL_AMT_EXCEEDS);
		return false;
	}
	$('#amtPaid').val(value);
	eqLocal = $('#amtPaid').val();
	eqLocal = formatAmount(eqLocal.toString(), $('#eqLocal_curr').val(), currencySubUnits);
	$('#eqLocalFormat').val(eqLocal);
	$('#eqLocal').val(unformatAmount($('#eqLocalFormat').val()));
	$('#eqLocal').val($('#amtPaid').val());
	setFocusLast('payRef');
	return true;
}

function eqLocalFormat_val() {
	var value = unformatAmount($('#eqLocalFormat').val());
	clearError('eqLocal_error');
	var rowID = pgmGrid.getCheckedRows(0);
	if (rowID == EMPTY_STRING) {
		alert(SELECT_ATLEAST_ONE);
		errors++;
		return false;
	}
	if (isEmpty(value)) {
		setError('eqLocal_error', MANDATORY);
		return false;
	}
	eqLocal = $('#amtPaid').val();
	eqLocal = formatAmount(eqLocal.toString(), $('#eqLocal_curr').val(), currencySubUnits);
	$('#eqLocalFormat').val(eqLocal);
	$('#eqLocal').val(unformatAmount($('#eqLocalFormat').val()));
}
function payRef_val() {
	var value = $('#payRef').val();
	clearError('payRef_error');
	if (isEmpty(value)) {
		setError('payRef_error', MANDATORY);
		return false;
	}
	if (!isValidDescription(value)) {
		setError('payRef_error', INVALID_DESCRIPTION);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('ENTITY_CODE', getEntityCode());
		validator.setValue('CONTRACT_DATE', $('#preLeaseRefDate').val());
		validator.setValue('CONTRACT_SL', $('#preLeaseRefSerial').val());
		validator.setValue('PAYMENT_SL', $('#paymentSl').val());
		validator.setValue('PAYMENT_REF', $('#payRef').val());
		validator.setClass('patterns.config.web.forms.lss.eleasepoinvpaybean');
		validator.setMethod('validatePayRef');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('payRef_error', validator.getValue(ERROR));
			return false;
		}
	}
	setFocusLast('remarks');
	return true;
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', HMS_INVALID_REMARKS);
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', HMS_INVALID_REMARKS);
			return false;
		}
	}
	if ($('#action').val() == RECTIFY) {
		if (isEmpty(value)) {
			setError('remarks_error', HMS_MANDATORY);
			return false;
		}
	}
	setFocusOnSubmit();
	return true;

}
function revalidate() {
	revaildateGrid();
}
function revaildateGrid() {
	clearError('agreeNo_error');
	var rowID = pgmGrid.getCheckedRows(0);
	if (rowID == EMPTY_STRING) {
		setError('gridError_error', SELECT_ATLEAST_ONE);
		errors++;
		return false;
	} else {
		var checkedrows = rowID.split(',');
		if (checkedrows.length > 1) {
			setError('gridError_error', SELECT_ONE);
			errors++;
			return false;
		} else {
			$('#scheduleIDHidden').val($('#scheduleId').val());
			$('#xmlGrid').val(pgmGrid.serialize());
			$('#amtPaid').val(unformatAmount($('#amtPaidFormat').val()));
			$('#eqLocal').val(unformatAmount($('#eqLocalFormat').val()));
			$('#amtPaidHidden').val($('#amtPaid').val());
			$('#eqLocalHidden').val($('#eqLocal').val());
			$('#currHidden').val($('#eqLocal_curr').val());
			$('#currencyCodeHidden').val(pgmGrid.cells2(rowID, 5).getValue());
			$('#poDateHidden').val(pgmGrid.cells2(rowID, 2).getValue());
			$('#poAmtHidden').val(pgmGrid.cells2(rowID, 6).getValue());
			$('#paidAmtHidden').val(pgmGrid.cells2(rowID, 8).getValue());
		}
	}
}

function qsupplierdetails() {
	var pkValue = getEntityCode() + PK_SEPERATOR + $('#payFavour').val(pgmGrid.cells2(0, 9).getValue());
	window.scroll(0, 0);
	showWindow('qsupplier', getBasePath() + 'Renderer/cmn/qsupplier.jsp', QUERY_SUPPLIER_DETAILS, PK + '=' + pkValue + "&" + SOURCE + "=" + MAIN, true, false, false, 950, 550);
	resetLoading();
}

function errorDisplay() {
	clearError('gridError');
	if (($('#gridError_error').html().trim()) != EMPTY_STRING) {
		setError('gridError_error', SELECT_ATLEAST_ONE);
		errors++;
	}
}