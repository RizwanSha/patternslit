var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'ISTAFFPORTFOLIO';
var mainSerial;
function init() {
	clearFields();
	hide('viewStaffDetail');
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}

function clearFields() {
	$('#branchCode').val(EMPTY_STRING);
	$('#branchCode_desc').html(EMPTY_STRING);
	$('#effectiveDate').val(EMPTY_STRING);
	$('#roleCode').val(EMPTY_STRING);
	$('#roleCode_desc').html(EMPTY_STRING);
	$('#category').val(EMPTY_STRING);
	$('#noOfStaff').val(EMPTY_STRING);
	setCheckbox('enabled', YES);
	$('#remarks').val(EMPTY_STRING);
	viewStaffDetailGrid.clearAll();
	roleHierarchyGrid.clearAll();
}

function loadData() {
	$('#branchCode').val(validator.getValue('BRANCH_CODE'));
	$('#branchCode_desc').html(validator.getValue('F1_BRANCH_NAME'));
	$('#effectiveDate').val(validator.getValue('EFF_DATE'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadAuditFields(validator);
	loadRoleHierarchyGrid();
}

function loadRoleHierarchyGrid() {
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('BRANCH_CODE', $('#branchCode').val());
	validator.setValue('FROM_QUERY_VIEW', YES);
	validator.setValue('EFF_DATE', $('#effectiveDate').val());
	validator.setClass('patterns.config.web.forms.lss.istaffportfoliobean');
	validator.setMethod('getRoleHierarchyDetails');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		return false;
	} else {
		roleHierarchyGrid.clearAll();
		var result_xml = validator.getValue(RESULT_XML);
		roleHierarchyGrid.setImagePath(WidgetConstants.GRID_IMAGE_PATH);
		roleHierarchyGrid.enableAlterCss("even", "odd");
		roleHierarchyGrid.setEditable(false);
		roleHierarchyGrid.loadXMLString(result_xml);
	}
}

function loadStaffRoleHierarchyGrid() {
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('INVENTORY_SL', mainSerial);
	validator.setValue('BRANCH_CODE', $('#branchCode').val());
	validator.setValue('EFF_DATE', $('#effectiveDate').val());
	validator.setClass('patterns.config.web.forms.lss.istaffportfoliobean');
	validator.setMethod('getRoleCodeHierarchy');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('effectiveDate_error', validator.getValue(ERROR));
		return false;
	} else {
		roleHierarchyGrid.clearAll();
		var result_xml = validator.getValue(RESULT_XML);
		roleHierarchyGrid.setImagePath(WidgetConstants.GRID_IMAGE_PATH);
		roleHierarchyGrid.loadXMLString(result_xml);
	}
	return true;
}

function viewStaffDetails(staffRoleCode, staffRoleDescription, category, noOfStaff) {
	viewStaffDetailGrid.clearAll();
	if (roleHierarchyGrid.cells(staffRoleCode, 3).getValue() > 0) {
		$('#roleCode').val(roleHierarchyGrid.cells(staffRoleCode, 6).getValue());
		$('#roleCode_desc').html(roleHierarchyGrid.cells(staffRoleCode, 1).getValue());
		$('#category').val(category);
		$('#noOfStaff').val(roleHierarchyGrid.cells(staffRoleCode, 3).getValue());
		viewStaffDetailGrid.loadXMLString(roleHierarchyGrid.cells(staffRoleCode, 5).getValue());
	} else {
		$('#roleCode').val(staffRoleCode);
		$('#roleCodeHide').val(staffRoleCode);
		$('#roleCode_desc').html(staffRoleDescription);
		$('#category').val(category);
		$('#categoryHide').val(category);
		$('#noOfStaff').val(noOfStaff);
	}
	win = showWindow('viewStaffDetail', '', TC_VIEW_ALLOC_STAFF_DETAILS, 'viewStaffDetail', true, false, false, '750', '450');
	return true;
}

function onloadInlinePopUp(popUPId) {
	show('viewStaffDetail');
	viewStaffDetailGrid.setColumnHidden(0, true);
	setFocusLast('close');
	return true;
}

function closeStaffWindow() {
	closeInlinePopUp(win);
	return true;
}