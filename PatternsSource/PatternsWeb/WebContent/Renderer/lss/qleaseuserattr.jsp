<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.lss.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/lss/qleaseuserattr.js" />
	</web:dependencies>
	<web:loadPK />
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="eleaseuserattr.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:viewContent id="po_view1">
				<web:dividerBlock>
					<web:sectionBlock width="width-100p" align="left">
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="180px" />
									<web:columnStyle width="380px" />
									<web:columnStyle width="120px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="eleaseuserattr.lessecode" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:lesseeCodeDisplay property="leaseCode" id="leaseCode" />
									</web:column>
									<web:column>
										<web:legend key="eleaseuserattr.customerid" var="program" />
									</web:column>
									<web:column>
										<type:customerIDDisplay property="customerID" id="customerID" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="180px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowOdd>
									<web:column>
										<web:legend key="eleaseuserattr.custname" var="program" />
									</web:column>
									<web:column>
										<type:descriptionDisplay property="customerName" id="customerName" />
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="180px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="eleaseuserattr.aggrementserial" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:agreementNoDisplay property="aggrementSerial" id="aggrementSerial" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="eleaseuserattr.scheduleid" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:scheduleIDDisplay property="scheduleID" id="scheduleID" />
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
						<web:viewTitle var="program" key="eleaseuserattr.details" />
						<web:section>
							<web:table>
								<web:rowOdd>
									<web:column>
										<web:grid height="250px" width="962px" id="eleaseuserattrGrid" src="lss/eleaseuserattr_innerGrid.xml">
										</web:grid>
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="180px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="form.IsUseEnabled" var="common" />
									</web:column>
									<web:column>
										<type:checkboxDisplay property="enabled" id="enabled" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="form.remarks" var="common" />
									</web:column>
									<web:column>
										<type:remarksDisplay property="remarks" id="remarks" />
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
						<web:auditDisplay />
					</web:sectionBlock>
				</web:dividerBlock>
			</web:viewContent>
		</web:viewSubPart>
	</web:viewPart>
</web:fragment>
