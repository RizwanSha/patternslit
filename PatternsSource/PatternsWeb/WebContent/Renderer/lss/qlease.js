var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'ELEASE';
var win;
var taxSerial;
var leaseProductType;
var shippingState;
var spSerial;
var checkTax = false;
var hideWin;
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}

function loadGrid() {
	loadAssetComponentGrid();
	loadGridQuery(CURRENT_PROGRAM_ID, 'RENT_GRID', leaseRentalCharges_Grid);
	leaseRentalCharges_Grid.setColumnHidden(0, true);
	loadGridQuery(CURRENT_PROGRAM_ID, 'PAYMENT_GRID', leasePaymentDetails_Grid);
	leasePaymentDetails_Grid.setColumnHidden(1, true);
}

function loadAssetComponentGrid() {
	loadGridQuery(CURRENT_PROGRAM_ID, 'ASSETS_VIEW_GRID', postProcessingAssets);
}

function postProcessingAssets(result) {
	leaseNoOfAssets_Grid.parse(result, function() {
		loadGridQuery(CURRENT_PROGRAM_ID, 'COMPONENT_GRID', postProcessingComponent);
	});
}

function postProcessingComponent(result) {
	$xml = $($.parseXML(result));
	var rowDiscSl = 1;
	var rowdata = new DhtmlxXmlBuilder();
	var assetSl = 0;
	($xml).find("row").each(
			function() {
				assetSl = $(this).find("cell")[1].textContent;
				if (rowDiscSl != assetSl) {
					var rowxml = rowdata.serialize();
					leaseNoOfAssets_Grid.cells2((rowDiscSl - 1), 7).setValue(rowxml);
					rowDiscSl = assetSl;
					rowdata = new DhtmlxXmlBuilder();
				}
				rowdata.addRow([ false, $(this).find("cell")[2].textContent, $(this).find("cell")[3].textContent, $(this).find("cell")[4].textContent, $(this).find("cell")[5].textContent, $(this).find("cell")[6].textContent, $(this).find("cell")[7].textContent, $(this).find("cell")[8].textContent,
						$(this).find("cell")[9].textContent ]);
			});
	if (assetSl != 0) {
		var rowxml = rowdata.serialize();
		leaseNoOfAssets_Grid.cells2((rowDiscSl - 1), 7).setValue(rowxml);
	}
}

function doTabbarClick(id) {
	if (id == 'lease_0') {
		$('#lease').height('270px');
	} else if (id == 'lease_1') {
		$('#lease').height('360px');
	} else if (id == 'lease_2') {
		$('#lease').height('300px');
	} else if (id == 'lease_3') {
		$('#lease').height('560px');
	} else if (id == 'lease_4') {
		$('#lease').height('530px');
	} else if (id == 'lease_5') {
		$('#lease').height('450px');
	} else if (id == 'lease_6') {
		$('#lease').height('550px');
	}
}

function clearFields() {
	$('#custLeaseCode').val(EMPTY_STRING);
	$('#custLeaseCode_desc').html(EMPTY_STRING);
	$('#custID').val(EMPTY_STRING);
	$('#custName').val(EMPTY_STRING);
	$('#agreementNo').val(EMPTY_STRING);
	$('#agreementNo_desc').html(EMPTY_STRING);
	$('#scheduleID').val(EMPTY_STRING);
	$('#scheduleID_desc').html(EMPTY_STRING);
	checkTax = false;
	$('#preLeaseRefDate').val(EMPTY_STRING);
	$('#preLeaseRefSerial').val(EMPTY_STRING);

	// TAB 1
	$('#leaseComDate').val(EMPTY_STRING);
	$('#leaseProduct').val(EMPTY_STRING);
	$('#leaseProduct_desc').html(EMPTY_STRING);
	$('#assetCurrency').val(EMPTY_STRING);
	$('#assetCurrency_desc').html(EMPTY_STRING);
	$('#billAddress').val(EMPTY_STRING);
	$('#billAddress_desc').html(EMPTY_STRING);
	$('#shipAddress').val(EMPTY_STRING);
	$('#shipAddress_desc').html(EMPTY_STRING);
	$('#shipAddressState').html(EMPTY_STRING);
	$('#billAddressState').html(EMPTY_STRING);
	$('#billBranch').val(EMPTY_STRING);
	$('#billBranch_desc').html(EMPTY_STRING);
	$('#billState').val(EMPTY_STRING);
	$('#invType').val(EMPTY_STRING);

	// TAB 2
	$('#noOfAssets').val(EMPTY_STRING);
	$('#taxScheduleCode').val(EMPTY_STRING);
	$('#grossNetTDS').val(EMPTY_STRING);

	// tTAB pop
	$('#assetSl').val(EMPTY_STRING);
	$('#assetCategory').val(EMPTY_STRING);
	$('#assetCategory_desc').html(EMPTY_STRING);
	$('#assetCategory_error').html(EMPTY_STRING);
	$('#autoNonAuto').val(EMPTY_STRING);
	$('#assetComponent').val(EMPTY_STRING);
	$('#assetComponent_error').html(EMPTY_STRING);

	// TAB 3
	$('#tenor').val(EMPTY_STRING);
	$('#cancelPeriod').val(EMPTY_STRING);
	$('#repayFreq').val(EMPTY_STRING);
	$('#advArrers').val(EMPTY_STRING);
	$('#firstPaymet').val(EMPTY_STRING);
	$('#intFromDate').val(EMPTY_STRING);
	$('#firstPaymet').val(EMPTY_STRING);
	$('#finalFromDate').val(EMPTY_STRING);
	$('#finalUptoDate').val(EMPTY_STRING);
	$('#rentDate').val(EMPTY_STRING);
	$('#lastDate').val(EMPTY_STRING);

	// TAB 4

	$('#leaseRentOption').val(EMPTY_STRING);
	$('#commencementValue').val(EMPTY_STRING);
	$('#commencementValueFormat').val(EMPTY_STRING);
	$('#assureRVPer').val(EMPTY_STRING);
	$('#assureRVType').val(EMPTY_STRING);
	$('#assureRVAmount').val(EMPTY_STRING);
	$('#assureRVAmountFormat').val(EMPTY_STRING);
	$('#grossAssetCost').val(EMPTY_STRING);
	$('#grossAssetCostFormat').val(EMPTY_STRING);
	$('#intBrokenRent').val(EMPTY_STRING);
	$('#intBrokenRentFormat').val(EMPTY_STRING);
	$('#intBrokenExec').val(EMPTY_STRING);
	$('#intBrokenExecFormat').val(EMPTY_STRING);
	$('#finalBrokenRent').val(EMPTY_STRING);
	$('#finalBrokenRentFormat').val(EMPTY_STRING);
	$('#finaBrokenExec').val(EMPTY_STRING);
	$('#finaBrokenExecFormat').val(EMPTY_STRING);
	$('#internalRateReturn').val(EMPTY_STRING);
	$('#dealType').val(EMPTY_STRING);

	// TAB 6
	$('#processCode').val(EMPTY_STRING);
	$('#processCode_desc').html(EMPTY_STRING);
	$('#invTempCode').val(EMPTY_STRING);
	$('#invTempCode_desc').html(EMPTY_STRING);
	$('#famDesc').val(EMPTY_STRING);
	setCheckbox('wavedOff', NO);
	$('#invCycleNo').val(EMPTY_STRING);
	$('#hierarCode').val(EMPTY_STRING);
	$('#hierarCode_desc').html(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);

	$('#commencementValue_curr').val(EMPTY_STRING);
	$('#grossAssetCost_curr').val(EMPTY_STRING);
	$('#intBrokenRent_curr').val(EMPTY_STRING);
	$('#intBrokenExec_curr').val(EMPTY_STRING);
	$('#finalBrokenRent_curr').val(EMPTY_STRING);
	$('#finaBrokenExec_curr').val(EMPTY_STRING);
	$('#peakAmount_curr').val(EMPTY_STRING);
	$('#rental_curr').val(EMPTY_STRING);
	$('#execChags_curr').val(EMPTY_STRING);
	$('#principalAmt_curr').val(EMPTY_STRING);
	$('#interestAmt_curr').val(EMPTY_STRING);
	$('#assureRVAmount_curr').val(EMPTY_STRING);

	leaseNoOfAssets_Grid.clearAll();
	leaseAssetComponent_Grid.clearAll();
	leaseRentalCharges_Grid.clearAll();
	leaseApplicableTax_Grid.clearAll();
	financialScheduleNonOLGrid.clearAll();
	leasePaymentDetails_Grid.clearAll();
	leaseTaxWiseDetails_Grid.clearAll();
	hide('noOfAssetReq');
	hide('prinsInterestWise');
	hide('taxDetails');
}

function loadData() {

	$('#custLeaseCode').val(validator.getValue('LESSEE_CODE'));
	$('#agreementNo').val(validator.getValue('AGREEMENT_NO'));
	$('#scheduleID').val(validator.getValue('SCHEDULE_ID'));
	$('#custID').val(validator.getValue('CUSTOMER_ID'));
	$('#custName').val(validator.getValue('F1_CUSTOMER_NAME'));

	$('#preLeaseRefDate').val(validator.getValue('CONTRACT_DATE'));
	$('#preLeaseRefSerial').val(validator.getValue('CONTRACT_SL'));

	// TAB 1
	$('#createDate').val(validator.getValue('DATE_OF_CREATION'));
	$('#leaseComDate').val(validator.getValue('DATE_OF_COMMENCEMENT'));
	$('#leaseProduct').val(validator.getValue('LEASE_PRODUCT_CODE'));
	$('#leaseProduct_desc').html(validator.getValue('F2_DESCRIPTION'));
	leaseProductType = validator.getValue('F2_LEASE_TYPE');
	$('#assetCurrency').val(validator.getValue('ASSET_CCY'));
	$('#assetCurrency_desc').html(validator.getValue('F3_DESCRIPTION'));
	$('#billAddress').val(validator.getValue('BILLING_ADDR_SL'));
	$('#shipAddress').val(validator.getValue('SHIP_ADDR_SL'));
	$('#billAddressState').html(DISPLAY_STATE + validator.getValue("F9_STATE_CODE"));
	$('#shipAddressState').html(DISPLAY_STATE + validator.getValue("F10_STATE_CODE"));
	
	$('#billBranch').val(validator.getValue('LESSOR_BRANCH_CODE'));
	$('#billBranch_desc').html(validator.getValue('F4_BRANCH_NAME'));
	$('#billState').val(validator.getValue('LESSOR_STATE_CODE'));
	$('#billState_desc').html(validator.getValue('F5_DESCRIPTION'));
	$('#invType').val(validator.getValue('F5_INVOICE_TYPE'));

	$('#taxScheduleCode').val(validator.getValue('TAX_SCHEDULE_CODE'));

	// currency load
	var curCode = $('#assetCurrency').val();
	$('#commencementValue_curr').val(curCode);
	$('#grossAssetCost_curr').val(curCode);
	$('#intBrokenRent_curr').val(curCode);
	$('#intBrokenExec_curr').val(curCode);
	$('#finalBrokenRent_curr').val(curCode);
	$('#finaBrokenExec_curr').val(curCode);
	$('#peakAmount_curr').val(curCode);
	$('#rental_curr').val(curCode);
	$('#execChags_curr').val(curCode);
	$('#interestTaxAmt_curr').val(curCode);
	$('#principalTaxAmt_curr').val(curCode);
	$('#execTaxAmt_curr').val(curCode);
	$('#rentalTaxAmt_curr').val(curCode);
	$('#assureRVAmount_curr').val(curCode);

	// TAB 2
	$('#taxScheduleCode').val(validator.getValue('TAX_SCHEDULE_CODE'));
	$('#noOfAssets').val(validator.getValue('NOF_ASSETS'));

	// TAB 3
	$('#tenor').val(validator.getValue('TENOR'));
	$('#cancelPeriod').val(validator.getValue('NON_CANCEL_PERIOD'));
	$('#repayFreq').val(validator.getValue('REPAY_FREQ'));
	$('#advArrers').val(validator.getValue('ADVANCE_ARREARS_FLAG'));
	$('#firstPaymet').val(validator.getValue('FIRST_REPAY_PERIOD_START_DATE'));
	$('#intFromDate').val(validator.getValue('INIT_INTERIM_PERIOD_FROM'));
	$('#intUptoDate').val(validator.getValue('INIT_INTERIM_PERIOD_UPTO'));
	$('#finalFromDate').val(validator.getValue('FINAL_INTERIM_PERIOD_FROM'));
	$('#finalUptoDate').val(validator.getValue('FINAL_INTERIM_PERIOD_UPTO'));
	$('#rentDate').val(validator.getValue('BILLING_FIRST_RENTAL_DATE'));
	$('#lastDate').val(validator.getValue('BILLING_LAST_RENTAL_DATE'));

	// TAB 4
	var grossAssetVal = validator.getValue('GROSS_ASSET_COST');
	var commencementVal = validator.getValue('COMMENCEMENT_VALUE');
	var initRentVal = validator.getValue('INIT_BROKEN_PERIOD_RENT');
	var initExexVal = validator.getValue('INIT_BROKEN_PERIOD_EXEC_CHGS');
	var finalRentVal = validator.getValue('FINAL_BROKEN_PERIOD_RENT');
	var finalExecVal = validator.getValue('FINAL_BROKEN_PERIOD_EXEC_CHGS');
	var rvAmount = validator.getValue('RV_AMOUNT');
	$('#leaseRentOption').val(validator.getValue('LEASE_RENTAL_PERIOD_OPTION'));
	$('#assureRVPer').val(validator.getValue('RV_PER'));
	$('#assureRVType').val(validator.getValue('RV_TYPE'));
	$('#internalRateReturn').val(validator.getValue('IRR_VALUE'));
	$('#dealType').val(validator.getValue('DEAL_TYPE'));

	// TAB 6
	$('#grossNetTDS').val(validator.getValue('TDS_GROSS_NET_INDICATOR'));
	$('#processCode').val(validator.getValue('BANK_PROCESS_CODE'));
	$('#processCode_desc').html(validator.getValue('F6_NAME'));
	$('#invTempCode').val(validator.getValue('INVOICE_TEMPLATE'));
	$('#invTempCode_desc').html(validator.getValue('F7_DESCRIPTION'));
	$('#famDesc').val(validator.getValue('FAM_CODE'));
	setCheckbox('wavedOff', validator.getValue('ST_WAVED_OFF'));
	$('#hierarCode').val(validator.getValue('LAM_HIER_CODE'));
	$('#hierarCode_desc').html(validator.getValue('F8_DESCRIPTION'));
	$('#invCycleNo').val(validator.getValue('INV_CYCLE_NUMBER'));
	$('#initCycleNo').val(validator.getValue('INIT_INTERIM_CYCLE_NUMBER'));
	$('#finalCycleNo').val(validator.getValue('FINAL_INTERIM_CYCLE_NUMBER'));
	$('#remarks').val(validator.getValue('REMARKS'));

	loadAuditFields(validator);
	// amount Formated
	$('#grossAssetCostFormat').val(formatAmount(grossAssetVal, $('#grossAssetCost_curr').val()));
	$('#grossAssetCost').val(unformatAmount($('#grossAssetCostFormat').val()));
	$('#commencementValueFormat').val(formatAmount(commencementVal, $('#commencementValue_curr').val()));
	$('#commencementValue').val(unformatAmount($('#commencementValueFormat').val()));
	if (initRentVal != null && initRentVal != EMPTY_STRING) {
		$('#intBrokenRentFormat').val(formatAmount(initRentVal, $('#intBrokenRent_curr').val()));
		$('#intBrokenRent').val(unformatAmount($('#intBrokenRentFormat').val()));
	}
	if (initExexVal != null && initExexVal != EMPTY_STRING) {
		$('#intBrokenExecFormat').val(formatAmount(initExexVal, $('#intBrokenExec_curr').val()));
		$('#intBrokenExec').val(unformatAmount($('#intBrokenExecFormat').val()));
	}
	if (finalRentVal != null && finalRentVal != EMPTY_STRING) {
		$('#finalBrokenRentFormat').val(formatAmount(finalRentVal, $('#finalBrokenRent_curr').val()));
		$('#finalBrokenRent').val(unformatAmount($('#finalBrokenRentFormat').val()));
	}
	if (finalExecVal != null && finalExecVal != EMPTY_STRING) {
		$('#finaBrokenExecFormat').val(formatAmount(finalExecVal, $('#finaBrokenExec_curr').val()));
		$('#finaBrokenExec').val(unformatAmount($('#finaBrokenExecFormat').val()));
	}
	if (rvAmount != null && rvAmount != EMPTY_STRING) {
		$('#assureRVAmountFormat').val(formatAmount(rvAmount, $('#assureRVAmount_curr').val()));
		$('#assureRVAmount').val(unformatAmount($('#assureRVAmountFormat').val()));
	}
	getSchedule();
	loadGrid();
	loadGridFinance();
	// loadTaxDetails();
	resetLoading();
	// lease._setTabActive('lease_0', true);
}

function eleaseAssets_ViewGrid(rowId) {
	clearComponentsGrid();
	win = showWindow('noOfAssetReq', EMPTY_STRING, 'Asset Details', EMPTY_STRING, true, false, false, 1100, 420);
	$('#assetSl').val(leaseNoOfAssets_Grid.cells(rowId, 2).getValue());
	$('#assetCategory').val(leaseNoOfAssets_Grid.cells(rowId, 3).getValue());
	$('#assetCategory_desc').html(leaseNoOfAssets_Grid.cells(rowId, 4).getValue());
	$('#autoNonAuto').val(leaseNoOfAssets_Grid.cells(rowId, 8).getValue());
	$('#assetComponent').val(leaseNoOfAssets_Grid.cells(rowId, 6).getValue());
	leaseAssetComponent_Grid.loadXMLString(leaseNoOfAssets_Grid.cells(rowId, 7).getValue());
	leaseAssetComponent_Grid.setColumnHidden(0, true);
}

function clearComponentsGrid() {
	$('#assetSl').val(EMPTY_STRING);
	$('#assetCategory').val(EMPTY_STRING);
	$('#assetCategory_desc').html(EMPTY_STRING);
	$('#autoNonAuto').val(EMPTY_STRING);
	$('#assetComponent').val(EMPTY_STRING);
	leaseAssetComponent_Grid.clearAll();
}

function closePopup(value) {
	if (value == 0) {
		if (win._idd == 'undefined' || win._idd == null)
			win = hideWin;
		closeInlinePopUp(win);
	}
	return true;
}

function viewAddress() {
	var pkValue = getEntityCode() + PK_SEPERATOR + $('#custID').val() + PK_SEPERATOR + $('#billAddress').val();
	window.scroll(0, 0);
	showWindow('PREVIEW_VIEW', getBasePath() + 'Renderer/reg/qcustomeraddress.jsp', 'QCUSTOMERADDRESS', PK + '=' + pkValue + "&" + SOURCE + "=" + MAIN, true, false, false, 980, 545);
	resetLoading();
}

function viewShipAddress() {
	var pkValue = getEntityCode() + PK_SEPERATOR + $('#custID').val() + PK_SEPERATOR + $('#shipAddress').val();
	window.scroll(0, 0);
	showWindow('PREVIEW_VIEW', getBasePath() + 'Renderer/reg/qcustomeraddress.jsp', 'QCUSTOMERADDRESS', PK + '=' + pkValue + "&" + SOURCE + "=" + MAIN, true, false, false, 980, 545);
	resetLoading();
}

function getSchedule() {
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('ENTITY_CODE', getEntityCode());
	// validator.setValue('LESSEE_CODE', $('#custLeaseCode').val());
	// validator.setValue('AGREEMENT_NO', $('#agreementNo').val());
	// validator.setValue('SCHEDULE_ID', $('#scheduleID').val());
	// validator.setValue('ADDR_SL', $('#shipAddress').val());
	// validator.setValue('CUSTOMER_ID', $('#custID').val());
	validator.setValue('SP_SERIAL_REQ', "1");
	validator.setValue(ACTION, USAGE);
	validator.setClass('patterns.config.web.forms.lss.eleasebean');
	validator.setMethod('getScheduleCode');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		alert(validator.getValue(ERROR));
		return false;
	} else {
		spSerial = validator.getValue("SP_TEMP_SL");
		// shippingState = validator.getValue("SHIP_STATE_CODE");
	}
	return true;
}

function loadGridFinance() {
	financialScheduleNonOLGrid.clearAll();
	financialScheduleOLGrid.clearAll();
	validator.clearMap();
	validator.setMtm(false);
	showMessage(getProgramID(), Message.PROGRESS);
	validator.setValue('ENTITY_CODE', getEntityCode());
	validator.setValue('LESSEE_CODE', $('#custLeaseCode').val());
	validator.setValue('AGREEMENT_NO', $('#agreementNo').val());
	validator.setValue('SCHEDULE_ID', $('#scheduleID').val());
	validator.setValue('LEASE_PRODUCT_CODE', $('#leaseProduct').val());
	validator.setValue('CUSTOMER_ID', $('#custID').val());
	validator.setValue('STATE_CODE', $('#billState').val());
	validator.setValue('ADDR_SL', $('#shipAddress').val());
	// validator.setValue('SHIP_STATE_CODE', shippingState);
	validator.setValue('TAX_SCHEDULE_CODE', $('#taxScheduleCode').val());
	validator.setValue('TAX_CCY', $('#assetCurrency').val());
	validator.setValue('TAX_ON_DATE', getCBD());
	validator.setValue('EDIT_REQ', "O");
	validator.setValue('MAIN_SL', spSerial);
	validator.setValue(ACTION, USAGE);
	validator.setClass('patterns.config.web.forms.lss.eleasebean');
	validator.setMethod('loadFinanceWiseGrid');
	validator.sendAndReceiveAsync(loadingFinanceGridDetails);
}

function loadingFinanceGridDetails() {
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		alert(validator.getValue(ERROR));
		financialScheduleNonOLGrid.clearAll();
		financialScheduleOLGrid.clearAll();
		leaseApplicableTax_Grid.clearAll();
		hideMessage(getProgramID());
		hide('leaseFinanceSchedule');
		hide('leaseFinanceRentalSchedule');
		return false;
	} else {
		$('#noOfslab').html(EMPTY_STRING);
		$('#noOfslab').append("<div>No. of Slabs    :<span id ='totalValues' x-large;'></span></div>");
		if (leaseProductType == 'O') {
			hide('leaseFinanceSchedule');
			show('leaseFinanceRentalSchedule');
			leaseApplicableTax_Grid.loadXMLString(validator.getValue("TAX_WISE_XML"));
			financialScheduleOLGrid.loadXMLString(validator.getValue("LOAD_FINANCIAL_XML"));
			financialScheduleOLGrid.setColumnHidden(1, true);
			financialScheduleOLGrid.setColumnHidden(4, true);
			financialScheduleOLGrid.setColumnHidden(5, true);
			$('#executoryAmt1').html(validator.getValue("TOTAL_EXEC_TAX"));
			$('#rentalAmt1').html(validator.getValue("TOTAL_RENTAL_TAX"));
			$('#stateAmtFormat1').html(validator.getValue("TOTAL_STATE_TAX_FORMAT"));
			$('#centralAmtFormat1').html(validator.getValue("TOTAL_CENTRAL_TAX_FORMAT"));
			$('#serviceAmtFormat1').html(validator.getValue("TOTAL_SERVICE_TAX_FORMAT"));
			$('#cessesAmtFormat1').html(validator.getValue("TOTAL_CESS_TAX_FORMAT"));
			$('#totalAmtFormat1').html(validator.getValue("TOTAL_VALUE_FORMAT"));
			$("#totalValues").html(financialScheduleOLGrid.getRowsNum());
			hideMessage(getProgramID());
		} else {
			show('leaseFinanceSchedule');
			hide('leaseFinanceRentalSchedule');
			leaseApplicableTax_Grid.loadXMLString(validator.getValue("TAX_WISE_XML"));
			financialScheduleNonOLGrid.loadXMLString(validator.getValue("LOAD_FINANCIAL_XML"));
			financialScheduleNonOLGrid.setColumnHidden(1, true);
			$('#executoryAmt').html(validator.getValue("TOTAL_EXEC_TAX"));
			$('#rentalAmt').html(validator.getValue("TOTAL_RENTAL_TAX"));
			$('#stateAmt').html(validator.getValue("TOTAL_STATE_TAX_FORMAT"));
			$('#centralAmt').html(validator.getValue("TOTAL_CENTRAL_TAX_FORMAT"));
			$('#serviceAmt').html(validator.getValue("TOTAL_SERVICE_TAX_FORMAT"));
			$('#cessesAmt').html(validator.getValue("TOTAL_CESS_TAX_FORMAT"));
			$('#totalAmt').html(validator.getValue("TOTAL_VALUE_FORMAT"));
			$("#totalValues").html(financialScheduleNonOLGrid.getRowsNum());
			hideMessage(getProgramID());
		}
	}
}

function loadTaxDetails() {
	var allSl = [];
	if (financialScheduleNonOLGrid.getRowsNum() > 0) {
		for (var i = 0; i < financialScheduleNonOLGrid.getRowsNum(); i++) {
			lastValue = parseInt(financialScheduleNonOLGrid.cells2(i, 23).getValue());
			if (jQuery.inArray(lastValue, allSl) == -1)
				allSl.push(lastValue);
		}
	}
	if (financialScheduleOLGrid.getRowsNum() > 0) {
		for (var i = 0; i < financialScheduleOLGrid.getRowsNum(); i++) {
			lastValue = parseInt(financialScheduleOLGrid.cells2(i, 23).getValue());
			if (jQuery.inArray(lastValue, allSl) == -1)
				allSl.push(lastValue);
		}
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('ENTITY_CODE', getEntityCode());
	if (allSl.length != 0)
		validator.setValue("TOTAL_SL", allSl);
	validator.setValue(ACTION, USAGE);
	validator.setClass('patterns.config.web.forms.lss.eleasebean');
	validator.setMethod('getTaxDetails');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		alert(validator.getValue(ERROR));
	} else {
		leaseApplicableTax_Grid.clearAll();
		leaseApplicableTax_Grid.loadXMLString(validator.getValue("TAX_XML"));
	}

}

function clearTaxDetails() {
	$('#startTaxDate').val(EMPTY_STRING);
	$('#endTaxDate').val(EMPTY_STRING);
	$('#monthSl').val(EMPTY_STRING);
	$('#stateCode').val(EMPTY_STRING);
	$('#stateCode_desc').html(EMPTY_STRING);
	$('#interestTaxAmt').val(EMPTY_STRING);
	$('#principalTaxAmt').val(EMPTY_STRING);
	$('#execTaxAmt').val(EMPTY_STRING);
	$('#rentalTaxAmt').val(EMPTY_STRING);
	$('#interestTaxAmtFormat').val(EMPTY_STRING);
	$('#principalTaxAmtFormat').val(EMPTY_STRING);
	$('#execTaxAmtFormat').val(EMPTY_STRING);
	$('#rentalTaxAmtFormat').val(EMPTY_STRING);
	hide('prinsInterestWise');
	hide('taxDetails');
	leaseTaxWiseDetails_Grid.clearAll();
}

function viewStateTaxDetails(rowId) {
	clearTaxDetails();
	var values = rowId.split("|");
	if (values[2] == '0.000' || values[2] == '0') {
		alert(NO_TAX_COMPUTED);
		return false;
	}
	taxSerial = values[0];
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('SCHEDULE_SL', values[0]);
	validator.setValue('MAIN_SL', values[1]);
	validator.setValue('TAX_TYPE', "1"); // Regular Tax
	validator.setValue('STATE_WISE', "STATE");
	validator.setClass('patterns.config.web.forms.lss.eleasebean');
	validator.setMethod('getTaxStateCentralWiseDetails');
	validator.sendAndReceiveAsync(loadTaxWiseDetails);
}

function viewCentralTaxDetails(rowId) {
	clearTaxDetails();
	var values = rowId.split("|");
	if (values[2] == '0.000' || values[2] == '0') {
		alert(NO_TAX_COMPUTED);
		return false;
	}
	taxSerial = values[0];
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('SCHEDULE_SL', values[0]);
	validator.setValue('MAIN_SL', values[1]);
	validator.setValue('TAX_TYPE', "1"); // Regular Tax
	validator.setValue('CENTRAL_WISE', "CENTRAL");
	validator.setClass('patterns.config.web.forms.lss.eleasebean');
	validator.setMethod('getTaxStateCentralWiseDetails');
	validator.sendAndReceiveAsync(loadTaxWiseDetails);
}

function viewServiceTaxDetails(rowId) {
	clearTaxDetails();
	var values = rowId.split("|");
	if (values[2] == '0.000' || values[2] == '0') {
		alert(NO_TAX_COMPUTED);
		return false;
	}
	taxSerial = values[0];
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('SCHEDULE_SL', values[0]);
	validator.setValue('MAIN_SL', values[1]);
	validator.setValue('TAX_TYPE', "2"); // Service Tax
	validator.setClass('patterns.config.web.forms.lss.eleasebean');
	validator.setMethod('getTaxStateCentralWiseDetails');
	validator.sendAndReceiveAsync(loadTaxWiseDetails);
}

function viewCessTaxDetails(rowId) {
	clearTaxDetails();
	var values = rowId.split("|");
	if (values[2] == '0.000' || values[2] == '0') {
		alert(NO_TAX_COMPUTED);
		return false;
	}
	taxSerial = values[0];
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('SCHEDULE_SL', values[0]);
	validator.setValue('MAIN_SL', values[1]);
	validator.setValue('TAX_TYPE', "3"); // Cess
	validator.setClass('patterns.config.web.forms.lss.eleasebean');
	validator.setMethod('getTaxStateCentralWiseDetails');
	validator.sendAndReceiveAsync(loadTaxWiseDetails);
}

function loadTaxWiseDetails() {
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		alert(validator.getValue(ERROR));
		clearTaxDetails();
		return false;
	} else if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			alert(validator.getValue(ERROR));
			clearTaxDetails();
			return false;
		} else if (validator.getValue(RESULT) == DATA_AVAILABLE) {
			checkTax = true;
			win = showWindow('taxDetails', EMPTY_STRING, TAX_BREAKUP_DTL, '', true, false, false, 950, 550);
			leaseTaxWiseDetails_Grid.loadXMLString(validator.getValue("TAX_GRID_DETAILS"));
			$('#totalTaxAmount').html(validator.getValue("TOTAL_TAX"));
			$('#stateCode').val(validator.getValue("STATE_CODE"));
			$('#stateCode_desc').html(validator.getValue("STATE_DESC"));

			if (leaseProductType != 'O') {
				show('prinsInterestWise');
				$('#monthSl').val(financialScheduleNonOLGrid.cells2(taxSerial - 1, 0).getValue());
				$('#startTaxDate').val(financialScheduleNonOLGrid.cells2(taxSerial - 1, 2).getValue());
				$('#endTaxDate').val(financialScheduleNonOLGrid.cells2(taxSerial - 1, 3).getValue());

				$('#principalTaxAmtFormat').val(financialScheduleNonOLGrid.cells2(taxSerial - 1, 4).getValue());
				$('#interestTaxAmtFormat').val(financialScheduleNonOLGrid.cells2(taxSerial - 1, 5).getValue());
				$('#execTaxAmtFormat').val(financialScheduleNonOLGrid.cells2(taxSerial - 1, 6).getValue());
				$('#rentalTaxAmtFormat').val(financialScheduleNonOLGrid.cells2(taxSerial - 1, 7).getValue());
			} else {
				hide('prinsInterestWise');
				$('#monthSl').val(financialScheduleOLGrid.cells2(taxSerial - 1, 0).getValue());
				$('#startTaxDate').val(financialScheduleOLGrid.cells2(taxSerial - 1, 2).getValue());
				$('#endTaxDate').val(financialScheduleOLGrid.cells2(taxSerial - 1, 3).getValue());

				$('#principalTaxAmtFormat').val(financialScheduleOLGrid.cells2(taxSerial - 1, 4).getValue());
				$('#interestTaxAmtFormat').val(financialScheduleOLGrid.cells2(taxSerial - 1, 5).getValue());
				$('#execTaxAmtFormat').val(financialScheduleOLGrid.cells2(taxSerial - 1, 6).getValue());
				$('#rentalTaxAmtFormat').val(financialScheduleOLGrid.cells2(taxSerial - 1, 7).getValue());
			}
		}
	}
}

function clickNextTab() {
	var actvId = lease.getActiveTab();
	if (actvId == 'lease_0') {
		doTabbarClick('lease_1');
		lease._setTabActive('lease_1', true);
	} else if (actvId == 'lease_1') {
		doTabbarClick('lease_2');
		lease._setTabActive('lease_2', true);
	} else if (actvId == 'lease_2') {
		doTabbarClick('lease_3');
		lease._setTabActive('lease_3', true);
	} else if (actvId == 'lease_3') {
		doTabbarClick('lease_4');
		lease._setTabActive('lease_4', true);
	} else if (actvId == 'lease_4') {
		doTabbarClick('lease_5');
		lease._setTabActive('lease_5', true);
	} else if (actvId == 'lease_5') {
		doTabbarClick('lease_6');
		lease._setTabActive('lease_6', true);
	}
}

function clickPreviousTab() {
	var actvId = lease.getActiveTab();
	if (actvId == 'lease_1') {
		doTabbarClick('lease_0');
		lease._setTabActive('lease_0', true);
	} else if (actvId == 'lease_2') {
		doTabbarClick('lease_1');
		lease._setTabActive('lease_1', true);
	} else if (actvId == 'lease_3') {
		doTabbarClick('lease_2');
		lease._setTabActive('lease_2', true);
	} else if (actvId == 'lease_4') {
		doTabbarClick('lease_3');
		lease._setTabActive('lease_3', true);
	} else if (actvId == 'lease_5') {
		doTabbarClick('lease_4');
		lease._setTabActive('lease_4', true);
	} else if (actvId == 'lease_6') {
		doTabbarClick('lease_5');
		lease._setTabActive('lease_5', true);
	}
}

function viewTaxMasterDetails(rowId) {
	var pkValue = rowId;
	window.scroll(0, 0);
	showWindow('PREVIEW_VIEW', getBasePath() + 'Renderer/cmn/qtax.jsp', 'Tax Maintenance Finder', PK + '=' + pkValue + "&" + SOURCE + "=" + MAIN, true, false, false, 980, 575);
	resetLoading();
}

function viewTaxMasterWiseDetails(rowId) {
	var pkValue = rowId;
	window.scroll(0, 0);
	win = showWindow('qtaxDetails', getBasePath() + 'Renderer/cmn/qtax.jsp', 'Tax Maintenance Finder', PK + '=' + pkValue + "&" + SOURCE + "=" + MAIN, true, false, false, 980, 575);
	resetLoading();
}

function afterClosePopUp(win) {
	if (checkTax) {
		if (win.getId() == 'qtaxDetails') {
			dhxWindows = null;
			win = EMPTY_STRING;
			win = showWindow('taxDetails', EMPTY_STRING, TAX_BREAKUP_DTL, '', true, false, false, 950, 550);
			if (win)
				hideWin = win;
			return true;
		}
	}
}

function doPrint() {
	if (leaseProductType != 'O') {
		if (financialScheduleNonOLGrid.getRowsNum() == 0) {
			alert('Financial schedule details not available , cant download the excel');
			return false;
		}
	} else {
		if (financialScheduleOLGrid.getRowsNum() == 0) {
			alert('Financial schedule details not available , cant download the excel');
			return false;
		}
	}
	showMessage(getProgramID(), Message.PROGRESS);
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('ENTITY_CODE', getEntityCode());
	validator.setValue('MAIN_SL', spSerial);
	validator.setValue('LEASE_TYPE', leaseProductType);
	validator.setValue(ACTION, USAGE);
	validator.setClass('patterns.config.web.forms.lss.eleasebean');
	validator.setMethod('downloadFinancialWiseGrid');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		hideMessage(getProgramID());
		alert(validator.getValue(ERROR));
		return false;
	} else {
		hideMessage(getProgramID());
		window.location = getBasePath() + 'servlet/ReportDownloadProcessor?' + REPORT_ID + '=' + validator.getValue(REPORT_ID);
		return true;
	}
	return true;
}