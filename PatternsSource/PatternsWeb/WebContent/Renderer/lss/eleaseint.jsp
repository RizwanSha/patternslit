<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.lss.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/lss/eleaseint.js" />
	</web:dependencies>
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="eleaseint.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/lss/eleaseint" id="eleaseint" method="POST">
				<web:dividerBlock>
					<type:optionBox add="true" modify="false" view="true" />
				</web:dividerBlock>
				<web:viewContent id="po_view1" styleClass="hidden">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle width="380px" />
										<web:columnStyle width="120px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="eleaseint.lessecode" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:lesseeCode property="lesseCode" id="lesseCode" />
										</web:column>
										<web:column>
											<web:legend key="eleaseint.customerid" var="program" />
										</web:column>
										<web:column>
											<type:customerID property="customerID" id="customerID" readOnly="true" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="eleaseint.custname" var="program" />
										</web:column>
										<web:column>
											<type:descriptionDisplay property="customerName" id="customerName" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="eleaseint.aggrementno" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:agreementNo property="aggrementNumber" id="aggrementNumber" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="eleaseint.scheduleid" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:scheduleID property="scheduleID" id="scheduleID" lookup="true" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="eleaseint.entrySerial" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:fiveDigit property="entrySerial" id="entrySerial" lookup="true" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:sectionTitle var="program" key="eleaseint.section" />
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="eleaseint.dateOfEntry" var="program" />
										</web:column>
										<web:column>
											<type:date property="dateOfEntry" id="dateOfEntry" readOnly="true" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="eleaseint.fromDate" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:date property="fromDate" id="fromDate" />
										</web:column>
										<web:column>
											<web:legend key="eleaseint.uptoDate" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:date property="uptoDate" id="uptoDate" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="eleaseint.noOfDays" var="program" />
										</web:column>
										<web:column>
											<type:fiveDigit property="noOfDays" id="noOfDays" readOnly="true" />
										</web:column>
										<web:column>
											<web:legend key="eleaseint.interestRate" var="program" />
										</web:column>
										<web:column>
											<type:threeDigitTwoDecimal property="interestRate" id="interestRate" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="eleaseint.interestOnAmount" var="program"  mandatory="true"/>
										</web:column>
										<web:column>
											<type:currencySmallAmount property="interestOnAmount" id="interestOnAmount" />
										</web:column>
										<web:column>
											<web:legend key="eleaseint.amountToBeBilled" var="program"  />
										</web:column>
										<web:column>
											<type:currencySmallAmount property="amountToBeBilled" id="amountToBeBilled"  readOnly="true"/>
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="eleaseint.dateOfInvoicingBilling" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:date property="dateOfInvoicingBilling" id="dateOfInvoicingBilling" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="form.remarks" var="common" />
										</web:column>
										<web:column>
											<type:remarks property="remarks" id="remarks" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column span="2">
											<web:submitReset />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
						</web:sectionBlock>
					</web:dividerBlock>
				</web:viewContent>
				<web:element property="command"/>
				<web:element property="action"/>
				<web:element property="w_basis"/>
				<web:element property="w_ro_choice"/>
				<web:element property="w_ro_factor"/>
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
	<web:queryGrid />
	<web:TBAGrid />
	<web:contextSearch />
</web:fragment>