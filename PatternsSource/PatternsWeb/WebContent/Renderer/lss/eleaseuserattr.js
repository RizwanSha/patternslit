var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'ELEASEUSERATTR';
var fieldType = EMPTY_STRING;
var fieldDecimals = EMPTY_STRING;
var field = EMPTY_STRING;
var fieldSize = EMPTY_STRING;
function init() {
	refreshTBAGrid();
	refreshQueryGrid();
	if (_redisplay) {
		if ($('#action').val() == ADD) {
			$('#enabled').prop('disabled', true);
			$('#enabled').prop('checked', true);
		}
		if (!isEmpty($('#xmleleaseuserattrGrid').val())) {
			eleaseuserattrGrid.loadXMLString($('#xmleleaseuserattrGrid').val());
		}
		if (!isEmpty($('#leaseCode').val())) {
			leaseCode_val();
		}
		$('#fieldvalue').removeClass('hidden');
		$('#fieldremarks').addClass('hidden');
	}
	$('#fieldvalue').removeClass('hidden');
	$('#fieldremarks').addClass('hidden');
	setFocusLast('leaseCode');
}
function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'ELEASEUSERATTR_MAIN', queryGrid);
}
function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}
function loadGrid() {
	loadGridQuery(CURRENT_PROGRAM_ID, 'ELEASEUSERATTR_GRID', eleaseuserattrGrid);
}
function view(source, primaryKey) {
	hideParent('lss/qleaseuserattr', source, primaryKey);
	resetLoading();
}
function doHelp(id) {
	switch (id) {
	case 'leaseCode':
		help('COMMON', 'HLP_DEBTOR_CODE', $('#leaseCode').val(), EMPTY_STRING, $('#leaseCode'), null, function(gridObj, rowID) {
			$('#leaseCode_desc').html(EMPTY_STRING);
		});
		break;
	case 'aggrementSerial':
		if (!isEmpty($('#customerID').val())) {
			help('COMMON', 'HLP_CUST_AGGREEMENT', $('#aggrementSerial').val(), $('#customerID').val(), $('#aggrementSerial'), null, function(gridObj, rowID) {
				$('#aggrementSerial').val(gridObj.cells(rowID, 1).getValue());
				$('#aggrementSerial_desc').html(EMPTY_STRING);
			});
		} else {
			setError('leaseCode_error', MANDATORY);
		}
		break;
	case 'scheduleID':
		if (!isEmpty($('#aggrementSerial').val())) {
			help('COMMON', 'HLP_LEASE_SCH_ID', $('#scheduleID').val(), $('#leaseCode').val() + PK_SEPERATOR + $('#aggrementSerial').val(), $('#scheduleID'), null, function(gridObj, rowID) {
				$('#scheduleID_desc').html(EMPTY_STRING);
			});
		} else {
			setError('aggrementSerial_error', MANDATORY);
		}
		break;
	case 'fieldId':
		help('ELEASEUSERATTR', 'HLP_LEASE_FIELD_SPEC', $('#fieldId').val(), $('#customerID').val(), $('#fieldId'), null, function(gridObj, rowID) {
			$('#fieldId_desc').html(EMPTY_STRING);
		});
		break;
	}
}

function clearFields() {
	$('#leaseCode').val(EMPTY_STRING);
	$('#leaseCode_error').html(EMPTY_STRING);
	$('#customerID').val(EMPTY_STRING);
	$('#customerName').val(EMPTY_STRING);
	$('#leaseCode_desc').html(EMPTY_STRING);
	$('#aggrementSerial').val(EMPTY_STRING);
	$('#aggrementSerial_error').html(EMPTY_STRING);
	$('#aggrementSerial_desc').html(EMPTY_STRING);
	$('#scheduleID').val(EMPTY_STRING);
	$('#scheduleID_error').html(EMPTY_STRING);
	$('#scheduleID_desc').html(EMPTY_STRING);
	clearNonPKFields(true);
}

function clearNonPKFields(valmode) {
	eleaseuserattrGrid.clearAll();
	eleaseuserattr_clearGridFields();
	setCheckbox('enabled', YES);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
}

function add() {
	setCheckbox('enabled', YES);
	$('#enabled').prop('disabled', true);
	$('#leaseCode').focus();
}
function modify() {
	$('leaseCode').focus();
	$('#enabled').prop('disabled', false);
}
function loadData() {
	$('#leaseCode').val(validator.getValue('LESSEE_CODE'));
	$('#customerID').val(validator.getValue('F1_CUSTOMER_ID'));
	$('#customerName').val(validator.getValue('F2_CUSTOMER_NAME'));
	$('#aggrementSerial').val(validator.getValue('AGREEMENT_NO'));
	$('#scheduleID').val(validator.getValue('SCHEDULE_ID'));
	loadGrid();
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
}
function eleaseuserattr_clearGridFields() {
	$('#fieldId').val(EMPTY_STRING);
	$('#fieldId_error').html(EMPTY_STRING);
	$('#fieldId_desc').html(EMPTY_STRING);
	$('#fieldvalue').removeClass('hidden');
	$('#fieldremarks').addClass('hidden');
	$('#fieldValue').val(EMPTY_STRING);
	$('#fieldValue_error').html(EMPTY_STRING);
	$('#fieldRemarks').val(EMPTY_STRING);
	$('#fieldRemarks_error').html(EMPTY_STRING);
	$('#fieldValue').prop('readOnly', false);
	$('#fieldRemarks').prop('readOnly', false);
	field = EMPTY_STRING;
	fieldType = EMPTY_STRING;
	fieldDecimals = EMPTY_STRING;
	fieldSize = EMPTY_STRING;
}

function validate(id) {
	switch (id) {
	case 'leaseCode':
		leaseCode_val();
		break;
	case 'aggrementSerial':
		aggrementSerial_val();
		break;
	case 'scheduleID':
		scheduleID_val();
		break;
	case 'fieldId':
		fieldId_val(true);
		break;
	case 'fieldValue':
		fieldValue_val(true);
		break;
	case 'fieldRemarks':
		fieldRemarks_val(true);
		break;
	case 'enabled':
		enabled_val();
		break;
	case 'remarks':
		remarks_val();
		break;
	}
}

function doclearfields(id) {
	switch (id) {
	case 'leaseCode':
		if (isEmpty($('#leaseCode').val())) {
			$('#aggrementSerial').val(EMPTY_STRING);
			$('#aggrementSerial_error').html(EMPTY_STRING);
			$('#aggrementSerial_desc').html(EMPTY_STRING);
			$('#scheduleID').val(EMPTY_STRING);
			$('#scheduleID_error').html(EMPTY_STRING);
			$('#scheduleID_desc').html(EMPTY_STRING);
			$('#customerID').val(EMPTY_STRING);
			$('#customerName').val(EMPTY_STRING);
			clearNonPKFields(false);
			break;
		}
	case 'aggrementSerial':
		if (isEmpty($('#aggrementSerial').val())) {
			$('#scheduleID').val(EMPTY_STRING);
			$('#scheduleID_error').html(EMPTY_STRING);
			$('#scheduleID_desc').html(EMPTY_STRING);
			clearNonPKFields(false);
			break;
		}
	case 'scheduleID':
		if (isEmpty($('#scheduleID').val())) {
			clearNonPKFields(false);
			break;
		}
	case 'fieldId':
		if (isEmpty($('#fieldId').val())) {
			eleaseuserattr_clearGridFields();
			break;
		}
	}
}

function backtrack(id) {
	switch (id) {
	case 'leaseCode':
		setFocusLast('leaseCode');
		break;
	case 'aggrementSerial':
		setFocusLast('leaseCode');
		break;
	case 'scheduleID':
		setFocusLast('aggrementSerial');
		break;
	case 'fieldId':
		setFocusLast('indAssetId');
		break;
	case 'eleaseuserattrGrid_add':
		setFocusLast('fieldId');
		break;
	case 'fieldValue':
		setFocusLast('fieldId');
		break;
	case 'fieldRemarks':
		setFocusLast('fieldId');
		break;
	case 'remarks':
		setFocusLast('fieldId');
		break;
	}
}

function leaseCode_val() {
	var value = $('#leaseCode').val();
	clearError('leaseCode_error');
	if (isEmpty(value)) {
		setError('leaseCode_error', MANDATORY);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('SUNDRY_DB_AC', $('#leaseCode').val());
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.lss.eleaseuserattrbean');
		validator.setMethod('validateLeaseCode');
		validator.sendAndReceive();
		if (validator.getValue(RESULT) == DATA_AVAILABLE) {
			$('#customerID').val(validator.getValue("CUSTOMER_ID"));
			$('#customerName').val(validator.getValue("CUSTOMER_NAME"));
		} else if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('leaseCode_error', validator.getValue(ERROR));
			$('#leaseCode_desc').html(EMPTY_STRING);
			$('#customerID').val(EMPTY_STRING);
			$('#customerName').val(EMPTY_STRING);
			return false;
		}
	}
	setFocusLast('aggrementSerial');
	return true;
}

function aggrementSerial_val() {
	var value = $('#aggrementSerial').val();
	clearError('aggrementSerial_error');
	if (isEmpty(value)) {
		setError('aggrementSerial_error', MANDATORY);
		return false;
	}
	if (isEmpty($('#customerID').val())) {
		setError('customerID_error', MANDATORY);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('CUSTOMER_ID', $('#customerID').val());
		validator.setValue('AGREEMENT_NO', $('#aggrementSerial').val());
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.lss.eleaseuserattrbean');
		validator.setMethod('validateAggrementSerial');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('aggrementSerial_error', validator.getValue(ERROR));
			return false;
		}
	}
	setFocusLast('scheduleID');
	return true;
}

function scheduleID_val() {
	var value = $('#scheduleID').val();
	clearError('scheduleID_error');
	if (isEmpty(value)) {
		setError('scheduleID_error', MANDATORY);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('LESSEE_CODE', $('#leaseCode').val());
		validator.setValue('AGREEMENT_NO', $('#aggrementSerial').val());
		validator.setValue('SCHEDULE_ID', $('#scheduleID').val());
		validator.setValue(ACTION, $('#action').val());
		validator.setClass('patterns.config.web.forms.lss.eleaseuserattrbean');
		validator.setMethod('validateScheduleID');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('scheduleID_error', validator.getValue(ERROR));
			return false;
		} else {
			if ($('#action').val() == MODIFY) {
				PK_VALUE = getEntityCode() + PK_SEPERATOR + $('#leaseCode').val() + PK_SEPERATOR + $('#aggrementSerial').val() + PK_SEPERATOR + $('#scheduleID').val();
				if (!loadPKValues(PK_VALUE, 'scheduleID_error')) {
					return false;
				}
			}
		}
	}
	setFocusLast('fieldId');
	return true;
}

function fieldId_val(valmode) {
	var value = $('#fieldId').val();
	clearError('fieldId_error');
	if (valmode) {
		$('#fieldValue_error').html(EMPTY_STRING);
		$('#fieldRemarks_error').html(EMPTY_STRING);
	}
	if (isEmpty(value)) {
		setError('fieldId_error', MANDATORY);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('CUSTOMER_ID_HIDDEN', $('#customerID').val());
		validator.setValue('FIELD_ID', $('#fieldId').val());
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.lss.eleaseuserattrbean');
		validator.setMethod('validateFieldID');
		validator.sendAndReceive();
		if (validator.getValue(RESULT) == DATA_AVAILABLE) {
			$('#fieldId_desc').html(validator.getValue("DESCRIPTION"));
			fieldType = validator.getValue("FIELD_VALUE_TYPE");
			fieldDecimals = validator.getValue("FIELD_DECIMALS");
			fieldSize = validator.getValue("FIELD_SIZE");
			if (isEmpty(fieldDecimals)) {
				fieldDecimals = ZERO;
			}
			if (fieldType == 'T') {
				$('#fieldValue').attr('intDigits', fieldSize);
				$('#fieldValue').attr('fracDigits', fieldDecimals);
				$('#fieldValue').prop('readOnly', true);
				$('#fieldRemarks').prop('readOnly', false);
				$('#fieldvalue').addClass('hidden');
				$('#fieldremarks').removeClass('hidden');
				if (valmode)
					setFocusLast('fieldRemarks');
			} else if (fieldType != 'T') {
				$('#fieldValue').attr('intDigits', parseInt(fieldSize - fieldDecimals));
				$('#fieldValue').prop('readOnly', false);
				$('#fieldRemarks').prop('readOnly', true);
				$('#fieldvalue').removeClass('hidden');
				$('#fieldremarks').addClass('hidden');
				if ((fieldType == 'A') || (fieldType == 'F')) {
					$('#fieldValue').attr('fracDigits', fieldDecimals);
				} else if (fieldType == 'N') {
					$('#fieldValue').attr('fracDigits', fieldDecimals);
				}
				if (valmode)
					setFocusLast('fieldValue');
			}
		} else if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('fieldId_error', validator.getValue(ERROR));
			$('#fieldId_desc').html(EMPTY_STRING);
			return false;
		}
	}
	return true;
}

function fieldValue_val(valmode) {
	var value = $('#fieldValue').val();
	clearError('fieldValue_error');
	if (isEmpty(value)) {
		setError('fieldValue_error', MANDATORY);
		return false;
	}
	if (valmode)
		setFocusLastOnGridSubmit('eleaseuserattrGrid');
	return true;
}

function fieldRemarks_val(valmode) {
	var value = $('#fieldRemarks').val();
	clearError('fieldRemarks_error');
	if (isEmpty(value)) {
		setError('fieldRemarks_error', MANDATORY);
		return false;
	}
	var lastChar = value.substring(value.length, value.length - 1);
	if (lastChar == "\n")
		value = value.substring(0, value.length - 1);
	var intLength = parseInt(value.length);
	if (intLength > 0) {
		if (intLength > fieldSize) {
			setError('fieldRemarks_error', HMS_INVALID_MAXINT_RANGE + fieldSize);
			return false;
		}
	}
	if (valmode) {
		setFocusLastOnGridSubmit('eleaseuserattrGrid');
	}
	return true;
}

function eleaseuserattr_addGrid(editMode, editRowId) {
	if (fieldId_val(false)) {
		if (fieldType != 'T') {
			if (!fieldValue_val(false)) {
				return false;
			}
		} else {
			if (!fieldRemarks_val(false)) {
				return false;
			}
		}
		if (eleaseuserattr_gridDuplicateCheck(editMode, editRowId)) {
			if (fieldType == 'T') {
				field = $('#fieldRemarks').val();
			} else if (fieldType != 'T') {
				field = $('#fieldValue').val();
			}
			var field_values = [ 'false', $('#fieldId').val(), $('#fieldId_desc').html(), field, fieldType, fieldDecimals, fieldSize ];
			_grid_updateRow(eleaseuserattrGrid, field_values);
			eleaseuserattr_clearGridFields();
			$('#fieldId').focus();
			return true;
		} else {
			setError('fieldId_error', DUPLICATE_DATA);
			$('#fieldId_desc').html(EMPTY_STRING);
			return false;
		}
	} else {
		return false;
	}
}

function eleaseuserattr_gridDuplicateCheck(editMode, editRowId) {
	var currentValue = [ $('#fieldId').val() ];
	return _grid_duplicate_check(eleaseuserattrGrid, currentValue, [ 1 ]);
}

function eleaseuserattr_editGrid(rowId) {
	$('#fieldId').val(eleaseuserattrGrid.cells(rowId, 1).getValue());
	$('#fieldId_desc').html(eleaseuserattrGrid.cells(rowId, 2).getValue());
	fieldType = eleaseuserattrGrid.cells(rowId, 4).getValue();
	fieldSize = eleaseuserattrGrid.cells(rowId, 6).getValue();
	fieldDecimals = eleaseuserattrGrid.cells(rowId, 5).getValue();
	if (isEmpty(fieldDecimals)) {
		fieldDecimals = ZERO;
	}
	if (fieldType == 'T') {
		$('#fieldValue').prop('readOnly', true);
		$('#fieldRemarks').prop('readOnly', false);
		$('#fieldvalue').addClass('hidden');
		$('#fieldremarks').removeClass('hidden');
		$('#fieldRemarks').val(eleaseuserattrGrid.cells(rowId, 3).getValue());
		$('#fieldValue').attr('intDigits', parseInt(eleaseuserattrGrid.cells(rowId, 6).getValue()));
		$('#fieldValue').attr('fracDigits', fieldDecimals);
	} else if (fieldType != 'T') {
		$('#fieldValue').val(eleaseuserattrGrid.cells(rowId, 3).getValue());
		$('#fieldValue').attr('intDigits', parseInt(fieldSize) - parseInt(fieldDecimals));
		$('#fieldValue').prop('readOnly', false);
		$('#fieldRemarks').prop('readOnly', true);
		$('#fieldvalue').removeClass('hidden');
		$('#fieldremarks').addClass('hidden');
		if ((fieldType == 'A') || (fieldType == 'F')) {
			$('#fieldValue').attr('fracDigits', fieldDecimals);
		} else if (fieldType == 'N') {
			$('#fieldValue').attr('fracDigits', fieldDecimals);
		}
	}
	$('#fieldId').focus();
}

function eleaseuserattr_cancelGrid(rowId) {
	eleaseuserattr_clearGridFields();
}

function gridExit(id) {
	switch (id) {
	case 'fieldId':
		$('#xmleleaseuserattrGrid').val(eleaseuserattrGrid.serialize());
		if ($('#action').val() == MODIFY) {
			setFocusLast('enabled');
		} else {
			setFocusLast('remarks');
		}
		break;
	case 'fieldValue':
		$('#xmleleaseuserattrGrid').val(eleaseuserattrGrid.serialize());
		if ($('#action').val() == MODIFY) {
			setFocusLast('enabled');
		} else {
			setFocusLast('remarks');
		}
		break;
	case 'fieldRemarks':
		$('#xmleleaseuserattrGrid').val(eleaseuserattrGrid.serialize());
		if ($('#action').val() == MODIFY) {
			setFocusLast('enabled');
		} else {
			setFocusLast('remarks');
		}
		break;
	}
}
function gridDeleteRow(id) {
	switch (id) {
	case 'fieldId':
		deleteGridRecord(eleaseuserattrGrid);
		break;
	}
}
function revalidate() {
	eleaseuserattr_revaildateGrid();
}

function enabled_val() {
	if ($('#action').val() == MODIFY) {
		setFocusLast('remarks');
	}
	return true;
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', INVALID_REMARKS);
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', INVALID_REMARKS);
			return false;
		}
	}
	if ($('#action').val() == MODIFY) {
		if (isEmpty(value)) {
			setError('remarks_error', MANDATORY);
			return false;
		}

	}
	setFocusOnSubmit();
	return true;

}

function eleaseuserattr_revaildateGrid() {
	if (eleaseuserattrGrid.getRowsNum() > 0) {
		$('#xmleleaseuserattrGrid').val(eleaseuserattrGrid.serialize());
		$('#fieldId_error').html(EMPTY_STRING);
	} else {
		eleaseuserattrGrid.clearAll();
		$('#xmleleaseuserattrGrid').val(eleaseuserattrGrid.serialize());
		setError('fieldId_error', ADD_ATLEAST_ONE_ROW);
		errors++;
	}
}
