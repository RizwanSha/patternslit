var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'RINTDEBITNOTE';
var month=EMPTY_STRING;
function init() {
	clearFields();
	setFocusLast('month');
}

function clearFields() {
	$('#month').val(getCBD().substring(4, 5));
	$('#monthYear').val(getFinYear(getCBD()));
	$('#month_error').html(EMPTY_STRING);
	rintdebitnote_innerGrid.clearAll();
	$('#generateReport').prop('disabled', true);
	month=EMPTY_STRING;
}

function validate(id) {
	switch (id) {
	case 'month':
		month_val();
		break;
	case 'monthYear':
		monthYear_val();
		break;
	case 'submit':
		loadGrid();
		break;
	}
}

function backtrack(id) {
	switch (id) {
	case 'month':
		setFocusLast('month');
		break;
	case 'monthYear':
		setFocusLast('month');
		break;
	case 'submit':
		setFocusLast('monthYear');
		break;
	case 'generateReport':
		setFocusLast('submit');
		break;
	}
}

function month_val() {
	var value = $('#month').val();
	clearError('month_error');
	if (isEmpty(value)) {
		setError('month_error', MANDATORY);
		return false;
	}
	setFocusLast('monthYear');
	return true;
}

function monthYear_val() {
	var value = $('#monthYear').val();
	clearError('month_error');
	if (isEmpty(value)) {
		$('#monthYear').val(getFinYear(getCBD()));
	}
	value = $('#monthYear').val();
	if (!isValidYear(value)) {
		setError('month_error', INVALID_YEAR);
		return false;
	}
	setFocusLast('submit');
	return true;
}

function loadGrid() {
	$('#submit').prop('disabled', true);
	$('#generateReport').prop('disabled', true);
	rintdebitnote_innerGrid.clearAll();
	showMessage(getProgramID(), Message.PROGRESS);
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('MONTH', $('#month').val());
	validator.setValue('YEAR', $('#monthYear').val());
	validator.setClass('patterns.config.web.forms.lss.rintdebitnotebean');
	validator.setMethod('doLoadGrid');
	validator.sendAndReceiveAsync(postProcessing);
}

function postProcessing() {
	$('#submit').prop('disabled', false);
	$('#month_error').html(EMPTY_STRING);
	if (validator.getValue(ERROR) != null && validator.getValue(ERROR) != EMPTY_STRING) {
		showMessage(getProgramID(), Message.ERROR, validator.getValue(ERROR));
		return false;
	} else if (validator.getValue('ERROR_PRESENT') == ONE) {
		hideMessage(getProgramID());
		if (validator.getValue('YEAR_ERROR').trim() != EMPTY_STRING) {
			$('#month_error').html(validator.getValue('YEAR_ERROR'));
			setFocusLast('monthYear');
		} else if (validator.getValue('MONTH_ERROR').trim() != EMPTY_STRING) {
			$('#month_error').html(validator.getValue('MONTH_ERROR'));
			setFocusLast('month');
		}
	} else if (validator.getValue(RESULT) == DATA_UNAVAILABLE) {
		showMessage(getProgramID(), Message.WARN, NO_RECORD);
	} else {
		rintdebitnote_innerGrid.clearAll();
		xmlString = validator.getValue(RESULT_XML);
		rintdebitnote_innerGrid.loadXMLString(xmlString);
		hideMessage(getProgramID());
		$('#generateReport').prop('disabled', false);
		return true;
	}
}

function doGenerateReport() {
	showMessage(getProgramID(), Message.PROGRESS);
	if (rintdebitnote_innerGrid.getCheckedRows(0) != EMPTY_STRING) {
		rowId = rintdebitnote_innerGrid.getCheckedRows(0);
		var debitNoteInv = rintdebitnote_innerGrid.cells(rowId, 9).getValue();
		var reportGen = rintdebitnote_innerGrid.cells(rowId, 7).getValue();
		if (reportGen == ONE) {
			if (!confirm(OVERRIDE_REPORT)) {
				return;
			}
		}
		var currency = rintdebitnote_innerGrid.cells(rowId, 5).getValue();
		var amount = rintdebitnote_innerGrid.cells(rowId, 6).getValue();
		var debitNoteDate = rintdebitnote_innerGrid.cells(rowId, 10).getValue();
		var branchCode = rintdebitnote_innerGrid.cells(rowId, 11).getValue();
		var nofIntEntries = rintdebitnote_innerGrid.cells(rowId, 12).getValue();
		var lessecode=rintdebitnote_innerGrid.cells(rowId, 2).getValue();
		var serial=rintdebitnote_innerGrid.cells(rowId, 4).getValue();
		serial=serial.substring(serial.lastIndexOf("-")+1,serial.length);
		showMessage(getProgramID(), Message.PROGRESS);
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('DEBIT_NOTE_INVENTORY', debitNoteInv);
		validator.setValue('INTEREST_CCY', currency);
		validator.setValue('DEBIT_NOTE_DATE', debitNoteDate);
		validator.setValue('DEBIT_NOTE_DATE_FIN_YEAR', getFinYear(debitNoteDate));
		validator.setValue('BRANCH_CODE', branchCode);
		validator.setValue('AMOUNT', unformatAmount(amount));
		validator.setValue('NOF_INT_ENTRIES', nofIntEntries);
		if($('#month').val().length=='1'){
			month=ZERO+$('#month').val();
		}
		else{
			month=$('#month').val();
		}
		validator.setValue('REPORT_TYPE', 'DBN'+$('#monthYear').val()+month+'_'+lessecode+serial);
		validator.setClass('patterns.config.web.forms.lss.rintdebitnotebean');
		validator.setMethod('doPrint');
		validator.sendAndReceiveAsync(processDownloadResult);
	} else {
		alert(SELECT_ATLEAST_ONE);
		$('#generateReport').blur();
		hideMessage(getProgramID());
		return;
	}
}

function processDownloadResult() {
	if (validator.getValue(ERROR) != null && validator.getValue(ERROR) != EMPTY_STRING) {
		showMessage(getProgramID(), Message.ERROR, validator.getValue(ERROR));
		return false;
	} else {
		hideMessage(getProgramID());
		rintdebitnote_innerGrid.cells(rowId, 8).setValue("Download^javascript:downloadReport(\"" + validator.getValue(REPORT_ID) + "\")");
		rintdebitnote_innerGrid.cells(rowId, 7).setValue(ONE);
		return true;
	}
}

function downloadReport(pdfIdentifier) {
	window.location = getBasePath() + 'servlet/ReportDownloadProcessor?' + REPORT_ID + '=' + pdfIdentifier;
}