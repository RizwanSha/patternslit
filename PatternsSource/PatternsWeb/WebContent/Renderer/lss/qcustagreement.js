var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'ECUSTAGREEMENT';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}
function clearFields() {
	$('#customerID').val(EMPTY_STRING);
	$('#customerID_desc').html(EMPTY_STRING);
	$('#agreementNo').val(EMPTY_STRING);
	$('#agreementDate').val(EMPTY_STRING);
	$('#sancRef').val(EMPTY_STRING);
	$('#sancAmountFormat').val(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	ecustagreement_innerGrid.clearAll();
	ecustagreement_innerGrid.setColumnHidden(1,true);
}

function loadData() {
	$('#customerID').val(validator.getValue('CUSTOMER_ID'));
	$('#customerID_desc').html(validator.getValue('F1_CUSTOMER_NAME'));
	$('#agreementSerial').val(validator.getValue('AGREEMENT_NO'));
	$('#agreementNo').val(validator.getValue('AGREEMENT_REF_NO'));
	$('#agreementDate').val(validator.getValue('AGREEMENT_DATE'));
	$('#sancRef').val(validator.getValue('SANCTION_REF_NO'));
	$('#sancAmount_curr').val(validator.getValue('SANCTION_CCY'));
	$('#remarks').val(validator.getValue('REMARKS'));
	$('#sancAmount').val(validator.getValue('SANCTION_AMOUNT'));
	loadAuditFields(validator);
	loadProductAgreementGrid();
	$('#sancAmountFormat').val(formatAmount($('#sancAmount').val(), $('#sancAmount_curr').val()));
	$('#sancAmount').val(unformatAmount($('#sancAmountFormat').val()));
}

function loadProductAgreementGrid() {
	loadGridQuery(CURRENT_PROGRAM_ID, 'ECUSTAGREEMENT_PRODUCT_GRID', productAgreementPostProcessing);
}

function productAgreementPostProcessing(result) {
	ecustagreement_innerGrid.parse(result);
	ecustagreement_innerGrid.setColumnHidden(0, true);
	var noofrows = ecustagreement_innerGrid.getRowsNum();
	if (noofrows > 0) {
		for (var i = 0; i < noofrows; i++) {
			var product = ecustagreement_innerGrid.cells2(i, 2).getValue();
			var linkValue = "<a href='javascript:void(0);' onclick=ecustagreement_editGrid('" + product + "')>Edit</a><span>  </span> <a href='javascript:void(0);' onclick=deleteGridRow('" + product + "');>Delete</a>";
			ecustagreement_innerGrid.cells2(i, 1).setValue(linkValue);
			ecustagreement_innerGrid.changeRowId(ecustagreement_innerGrid.getRowId(i), product);
		}
	}
}

function clearProductAgreementGrid() {
	$('#productCode').val(EMPTY_STRING);
	$('#productCode_desc').html(EMPTY_STRING);
	$('#productCode_error').html(EMPTY_STRING);
}

function loadProductDetailGrid() {
	loadGridQuery(CURRENT_PROGRAM_ID, 'ECUSTOMERREG_PRODUCT_GRID', productDetailPostProcessing);
}

function productDetailPostProcessing(result) {
	ecustomerregproductDetails_innerGrid.parse(result);
	ecustomerregproductDetails_innerGrid.setColumnHidden(0, true);
	var noofrows = ecustomerregproductDetails_innerGrid.getRowsNum();
	if (noofrows > 0) {
		for (var i = 0; i < noofrows; i++) {
			var productCode = ecustomerregproductDetails_innerGrid.cells2(i, 2).getValue();
			var linkValue = "<a href='javascript:void(0);' onclick=ecustomerreg_editGrid('" + productCode + "')>Edit</a>";
			ecustomerregproductDetails_innerGrid.cells2(i, 1).setValue(linkValue);
			ecustomerregproductDetails_innerGrid.changeRowId(ecustomerregproductDetails_innerGrid.getRowId(i), productCode);
		}
	}
}

function ecustagreement_editGrid(rowId) {
	clearProductAgreementGrid();
	addProductAgreement();
	ecustagreement_innerGrid.setUserData("", "grid_edit_id", rowId);
	$('#productCode').val(ecustagreement_innerGrid.cells(rowId, 2).getValue());
	$('#productCode_desc').html(ecustagreement_innerGrid.cells(rowId, 3).getValue());
	setFocusLast('productCode');
}

function addProductAgreement() {
	win = showWindow('productAgreement_Fields', EMPTY_STRING, PRODUCTS_COVERED_UNDER_THE_AGREEMENT, EMPTY_STRING, true, false, false, 550, 150);
}

function closePopup(value) {
	if (value == 0) {
		closeInlinePopUp(win);
	}
	return true;
}
