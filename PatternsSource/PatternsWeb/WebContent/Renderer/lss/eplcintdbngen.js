var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'EPLCINTDBNGEN';
function init() {
	clearFields();
	setFocusLast('leaseMonth');
}

function clearFields() {
	$('#leaseMonth').prop('selectedIndex', 0);
	$('#leaseMonth_error').html(EMPTY_STRING);
	$('#leaseMonthYear').val(EMPTY_STRING);
	eplcintdbngen_innerGrid.clearAll();
	eplcintdbngen_ledgerGrid.clearAll();
	hideMessage(getProgramID());
	hideMessage('authorizeStatus');
	hide('ledger_dtl');
	show('authorize');
}

function validate(id) {
	switch (id) {

	case 'leaseMonth':
		leaseMonth_val(true);
		break;
	case 'leaseMonthYear':
		leaseMonthYear_val(true);
		break;

	}
}

function backtrack(id) {
	switch (id) {
	case 'leaseMonth':
		setFocusLast('leaseMonth');
		break;
	case 'leaseMonthYear':
		setFocusLast('leaseMonth');
		break;
	case 'submit':
		setFocusLast('leaseMonthYear');
		break;
	}
}

function leaseMonth_val(valMode) {
	var value = $('#leaseMonth').val();
	clearError('leaseMonth_error');
	if (isEmpty(value)) {
		setError('leaseMonth_error', MANDATORY);
		return false;
	}
	if (valMode)
		setFocusLast('leaseMonthYear');
	return true;
}

function leaseMonthYear_val(valMode) {
	var year = getCalendarYear(getCBD());
	var value = $('#leaseMonthYear').val();
	clearError('leaseMonth_error');
	if (isEmpty(value)) {
		value = year;
		$('#leaseMonthYear').val(value);
	}
	if (!isValidYear(value)) {
		setError('leaseMonth_error', PBS_INVALID_YEAR);
		return false;
	}
	if (value > year) {
		setError('leaseMonth_error', PBS_YEAR_GR_CALYEAR);
		return false;
	}
	if (valMode)
		setFocusLast('submit');
	return true;
}

function loadPreLeaseContractInterest() {
	eplcintdbngen_innerGrid.clearAll();
	if (leaseMonth_val(false) && leaseMonthYear_val(false)) {
		$('#submit').prop('disabled', true);
		showMessage(getProgramID(), Message.PROGRESS);
		eplcintdbngen_innerGrid.clearAll();
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('YEAR', $('#leaseMonthYear').val());
		validator.setValue('MONTH', $('#leaseMonth').val());
		validator.setClass('patterns.config.web.forms.lss.eplcintdbngenbean');
		validator.setMethod('loadPreLeaseContractInterestDetails');
		validator.sendAndReceiveAsync(showStatus);
	}
}

function showStatus() {
	$('#submit').prop('disabled', false);
	hideMessage(getProgramID());
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		if (validator.getValue(ERROR_FIELD) != EMPTY_STRING) {
			hideAllMessages(getProgramID());
			setError(validator.getValue(ERROR_FIELD) + '_error', validator.getValue(ERROR));
		} else {
			showMessage(getProgramID(), Message.ERROR, validator.getValue(ERROR));
		}
	}
	if (!isEmpty(validator.getValue('SP_ERROR'))) {
		showMessage(getProgramID(), Message.ERROR, validator.getValue('SP_ERROR'));
		return false;
	}
	if (validator.getValue(RESULT) == DATA_UNAVAILABLE) {
		showMessage(getProgramID(), Message.ERROR, NO_RECORD);
		return false;
	}
	if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		eplcintdbngen_innerGrid.clearAll();
		eplcintdbngen_innerGrid.attachEvent("onRowSelect", doRowSelect);
		eplcintdbngen_innerGrid.attachEvent("onRowDblClicked", loadLedgerDetails);
		eplcintdbngen_innerGrid.loadXMLString(validator.getValue(RESULT_XML));
	}
}

function doAuthorize() {
	var rowID = getOneSelectedRecord(eplcintdbngen_innerGrid);
	if (rowID < ZERO) {
		alert(ATLEAST_ONE_CHECK);
		return false;
	}
	if (eplcintdbngen_innerGrid.cells(rowID, 12).getValue() != COLUMN_ENABLE) {
		alert(ALERT_VIEW_RECORD);
		return false;
	}
	showMessage('authorizeStatus', Message.PROGRESS);
	hide('submit');
	hide('reset');
	hide('authorize');
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('TMP_SERIAL', eplcintdbngen_innerGrid.cells(rowID, 10).getValue());
	validator.setValue('GRP_SL', eplcintdbngen_innerGrid.cells(rowID, 11).getValue());
	validator.setClass('patterns.config.web.forms.lss.eplcintdbngenbean');
	validator.setMethod('processAction');
	validator.sendAndReceiveAsync(authorizeStatus);
}

function authorizeStatus() {
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		showContent();
		showMessage('authorizeStatus', Message.ERROR, validator.getValue(ERROR));
		return false;
	} else {
		var msg = validator.getValue(RESULT);
		if (validator.getValue(ADDITIONAL_INFO) != EMPTY_STRING) {
			msg = msg + ' - ' + validator.getValue(ADDITIONAL_INFO);
		}
		if (validator.getValue(STATUS) == SUCCESS) {
			alert(RECORD_AUTHORIZED);
		} else {
			showMessage('authorizeStatus', Message.ERROR, msg);
			return false;
		}
	}
	showContent();
	var rowID = getOneSelectedRecord(eplcintdbngen_innerGrid);
	eplcintdbngen_innerGrid.cells(rowID, 0).setValue(false);
	eplcintdbngen_innerGrid.setRowColor(rowID, '#F34E04');
	hideMessage('authorizeStatus');
	eplcintdbngen_innerGrid.clearAll();
	loadPreLeaseContractInterest();
	return true;
}

function showContent() {
	show('submit');
	show('reset');
	show('authorize');
}

function revalidate() {
	errors = 0;
	if (!leaseMonth_val(false)) {
		++errors;
	}
	if (!leaseMonthYear_val(false)) {
		++errors;
	}
	if (errors > 0)
		return false;
	return true;
}

function doRowSelect(rowID) {
	eplcintdbngen_innerGrid.clearSelection();
	eplcintdbngen_innerGrid.selectRowById(rowID, true);
	eplcintdbngen_innerGrid.cells(rowID, 0).setValue(true);
}

function loadLedgerDetails(rowID) {
	eplcintdbngen_innerGrid.clearSelection();
	eplcintdbngen_innerGrid.selectRowById(rowID, true);
	eplcintdbngen_innerGrid.cells(rowID, 0).setValue(true);
	eplcintdbngen_innerGrid.cells(rowID, 12).setValue(COLUMN_ENABLE);
	eplcintdbngen_ledgerGrid.clearAll();
	win = showWindow('ledger_dtl', EMPTY_STRING, LEDGER_DETAILS, EMPTY_STRING, true, false, false, 1200, 400);
	showMessage('ledgerDetails', Message.PROGRESS);
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('CONTRACT_DATE', eplcintdbngen_innerGrid.cells(rowID, 1).getValue());
	validator.setValue('CONTRACT_SL', eplcintdbngen_innerGrid.cells(rowID, 2).getValue());
	validator.setValue('TMP_SERIAL', eplcintdbngen_innerGrid.cells(rowID, 10).getValue());
	validator.setValue('GRP_SL', eplcintdbngen_innerGrid.cells(rowID, 11).getValue());
	validator.setClass('patterns.config.web.forms.lss.eplcintdbngenbean');
	validator.setMethod('loadLedgerDetails');
	validator.sendAndReceiveAsync(showLedgerDetails);
}

function showLedgerDetails() {
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		showMessage('ledgerDetails', Message.ERROR, validator.getValue(ERROR));
		return false;
	}
	if (validator.getValue(RESULT) == DATA_UNAVAILABLE) {
		showMessage('ledgerDetails', Message.ERROR, NO_RECORD);
		return false;
	}
	if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		eplcintdbngen_ledgerGrid.clearAll();
		eplcintdbngen_ledgerGrid.loadXMLString(validator.getValue(RESULT_XML));
		hideMessage('ledgerDetails');
	}
}

function doView() {
	var rowID = getOneSelectedRecord(eplcintdbngen_innerGrid);
	if (rowID < ZERO) {
		alert(ATLEAST_ONE_CHECK);
		return false;
	}
	loadLedgerDetails(rowID);
	eplcintdbngen_innerGrid.cells(rowID, 12).setValue(COLUMN_ENABLE);
}

function doPrint() {
	var month = null;
	if (eplcintdbngen_innerGrid.getRowsNum() == 0) {
		showMessage(getProgramID(), Message.ERROR, ATLEAST_ONE_ROW_IN_GRID);
		return false;
	}
	disableElement('print');
	showMessage(getProgramID(), Message.PROGRESS);
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('TMP_SERIAL', eplcintdbngen_innerGrid.cells2(0, 10).getValue());
	validator.setValue(ACTION, USAGE);
	if ($('#leaseMonth').val().length == 1)
		month = ZERO + $('#leaseMonth').val();
	else
		month = $('#leaseMonth').val();
	validator.setValue('REPORT_TYPE', 'DBN' + $('#leaseMonthYear').val() + month);
	validator.setValue('REPORT_FORMAT', '2');
	validator.setClass('patterns.config.web.forms.lss.eplcintdbngenbean');
	validator.setMethod('doPrint');
	validator.sendAndReceiveAsync(processDownloadResult);
}

function processDownloadResult() {
	enableElement('print');
	if (validator.getValue(ERROR) != null && validator.getValue(ERROR) != EMPTY_STRING) {
		showMessage(getProgramID(), Message.ERROR, validator.getValue(ERROR));
		return false;
	} else {
		hideMessage(getProgramID());
		window.location = getBasePath() + 'servlet/ReportDownloadProcessor?' + REPORT_ID + '=' + validator.getValue(REPORT_ID);
		return true;
	}
}
