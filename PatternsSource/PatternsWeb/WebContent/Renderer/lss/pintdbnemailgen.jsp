<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<%@taglib prefix="message" tagdir="/WEB-INF/tags/message"%>
<web:bundle baseName="patterns.config.web.forms.lss.Messages" var="program" />
<web:fragment>
	<style>
p {
	border: 1px solid;
	padding: 5px;
	font-size: small;
	font-weight: initial;
}

.mailContent {
	height: 240px;
	overflow: auto;
}

.pdfColor {
	color: #ff0000;
}

.pdfColor:hover {
	color: #ff0000;
}
</style>
	<web:dependencies>
		<web:script src="Renderer/lss/pintdbnemailgen.js" />
	</web:dependencies>
	<web:loadPK />
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="lss.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/lss/pintdbnemailgen" id="pintdbnemailgen" method="POST">
				<web:viewContent id="po_view1">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="pintdbnemailgen.intyearmonth" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:yearMonth property="intMonth" id="intMonth" datasourceid="COMMON_MONTHS" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:rowOdd>
										<web:column>
											<web:button id="submit" key="form.submit" var="common" onclick="loadGrid();" />
											<web:button id="reset" key="form.reset" var="common" onclick="clearFields()" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>

							<web:section>
								<message:messageBox id="pintdbnemailgen" />
							</web:section>

							<web:section>
								<web:table>
									<web:rowEven>
										<web:column>
											<web:viewContent id="po_view4">
												<web:grid height="300px" width="1119px" id="pintdbnemailgen_innerGrid" src="lss/pintdbnemailgen_innerGrid.xml">
												</web:grid>
											</web:viewContent>
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="80px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:button id="sendMailId" key="pintdbnemailgen.sendmail" var="program" onclick="sendMail();" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>

							<web:viewContent id="mailSentLog" styleClass="hidden">
								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="140px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowEven>
											<web:column>
												<web:legend key="pintdbnemailgen.preleasecontractref" var="program" />
											</web:column>
											<web:column>
												<type:dateDaySLDisplay property="preLeaseContractRef" id="preLeaseContractRef" />
											</web:column>
										</web:rowEven>
										<web:rowOdd>
											<web:column>
												<web:legend key="pintdbnemailgen.customername" var="program" />
											</web:column>
											<web:column>
												<type:nameDisplay property="customerName" id="customerName" />
											</web:column>
										</web:rowOdd>
										<type:error property="mailSentLogError" />
									</web:table>
								</web:section>
								<web:section>
									<web:table>
										<web:rowEven>
											<web:column>
												<web:viewContent id="po_view4">
													<web:grid height="230px" width="757px" id="mailSentLog_innerGrid" src="lss/pintdbnemailgen_mailSentLog_innerGrid.xml">
													</web:grid>
												</web:viewContent>
											</web:column>
										</web:rowEven>
									</web:table>
								</web:section>
							</web:viewContent>

							<web:viewContent id="mail" styleClass="hidden">
								<web:grid height="0px" width="0px" id="mailGrid" src="lss/pintdbnemailgen_mailGrid.xml">
								</web:grid>
							</web:viewContent>

							<web:viewContent id="mailLogDetails" styleClass="hidden">
								<type:error property="mailLogDetailsError" />
								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="140px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowEven>
											<web:column>
												<web:legend key="pintdbnemailgen.recipient" var="program" />
											</web:column>
											<web:column>
												<web:viewContent id="po_view4">
													<web:grid height="160px" width="416px" id="mailSentRcp_innerGrid" src="lss/pintdbnemailgen_RecpGrid.xml">
													</web:grid>
												</web:viewContent>
											</web:column>
										</web:rowEven>
										<web:rowOdd>
											<web:column>
												<web:legend key="pintdbnemailgen.subject" var="program" />
											</web:column>
											<web:column>
												<p id="subject" />
											</web:column>
										</web:rowOdd>
										<web:rowEven>
											<web:column>
												<web:legend key="pintdbnemailgen.senton" var="program" />
											</web:column>
											<web:column>
												<type:dateTimeDisplay property="sentOn" id="sentOn" />
											</web:column>
										</web:rowEven>
										<web:rowOdd>
											<web:column>
												<web:legend key="pintdbnemailgen.content" var="program" />
											</web:column>
											<web:column>
												<p class="mailContent" id="emailContent" />
											</web:column>
										</web:rowOdd>
									</web:table>
								</web:section>
								<web:viewContent id="attachment">
									<web:section>
										<web:table>
											<web:columnGroup>
												<web:columnStyle width="140px" />
												<web:columnStyle />
											</web:columnGroup>
											<web:rowEven>
												<web:column>
													<web:legend key="pintdbnemailgen.attachment" var="program" />
													<i class="fa fa-paperclip fa-2x" style="color: #000000"></i>
												</web:column>
												<web:column>
													<div id="pdfDiv">
														<a id="pdfFile" href="#" target="_blank" class="pdfColor"> <web:legend id="pdf" key="pintdbnemailgen.pdf" var="program" /></a> <a class="fa fa-file-pdf-o fa-2x pdfColor" id="pdf" href="#" target="_blank"></a>
													</div>
												</web:column>
											</web:rowEven>
										</web:table>
									</web:section>
								</web:viewContent>
							</web:viewContent>
							<web:viewContent id="componentdtl" styleClass="hidden">
								<web:section>
									<type:error property="component" />
								</web:section>
								<web:section>
									<web:table>
										<web:rowOdd>
											<web:column>
												<web:grid height="350px" width="1087px" id="pintdbnemailgen_ledgerGrid" src="lss/pintdbnemailgen_ledgerGrid.xml">
												</web:grid>
											</web:column>
										</web:rowOdd>
									</web:table>
								</web:section>
							</web:viewContent>
						</web:sectionBlock>
					</web:dividerBlock>
				</web:viewContent>
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
	<web:contextSearch />
</web:fragment>

