<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.lss.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/lss/qpreleasecontreg.js" />
	</web:dependencies>
	<web:loadPK />
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="epreleasecontreg.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:viewContent id="po_view1">
				<web:dividerBlock>
					<web:sectionBlock width="width-100p" align="left">
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="epreleasecontreg.preleasecon" var="program" />
									</web:column>
									<web:column>
										<type:dateDaySLDisplay property="preLeaseContractRef" id="preLeaseContractRef" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowOdd>
									<web:column>
										<web:legend key="epreleasecontreg.customerid" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:customerIDDisplay property="customerID" id="customerID" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="epreleasecontreg.lesseecode" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:lesseeCodeDisplay property="lesseeCode" id="lesseeCode" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="epreleasecontreg.productcode" var="program" />
									</web:column>
									<web:column>
										<type:leaseProductCodeDisplay property="productCode" id="productCode" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="epreleasecontreg.branchcode" var="program" mandatory="true"/>
									</web:column>
									<web:column>
										<type:branchDisplay property="branchCode" id="branchCode"/>
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="epreleasecontreg.addrserial" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:addressSerialDisplay property="addressSerial" id="addressSerial" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="epreleasecontreg.contactserial" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:serialDisplay property="contactSerial" id="contactSerial" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="epreleasecontreg.interestapplicable" var="program" />
									</web:column>
									<web:column>
										<type:checkboxDisplay property="interestApplicable" id="interestApplicable" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="epreleasecontreg.interestrateinvest" var="program" />
									</web:column>
									<web:column>
										<type:threeDigitTwoDecimalPercentageDisplay property="interestRateInvestLeaseCommit" id="interestRateInvestLeaseCommit" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="epreleasecontreg.interestbillingchoice" var="program" />
									</web:column>
									<web:column>
										<type:comboDisplay property="interestBillingChoice" id="interestBillingChoice" datasourceid="EPRELEASECONTREG_INT_BILL_OPTION" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="epreleasecontreg.debitnoteprinting" var="program" />
									</web:column>
									<web:column>
										<type:comboDisplay property="interestDebitNotePrinting" id="interestDebitNotePrinting" datasourceid="EPRELEASECONTREG_INT_DEBIT_PRINTING" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowOdd>
									<web:column>
										<web:legend key="form.remarks" var="common" />
									</web:column>
									<web:column>
										<type:remarksDisplay property="remarks" id="remarks" />
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
						<web:auditDisplay />
					</web:sectionBlock>
				</web:dividerBlock>
			</web:viewContent>
		</web:viewSubPart>
	</web:viewPart>
</web:fragment>