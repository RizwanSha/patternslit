var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'QLEASEBROWSER';

function init() {
	clearFields();
	setFocusLast('lesseCode');
}

function clearFields() {
	$('#lesseCode').val(EMPTY_STRING);
	$('#lesseCode_desc').html(EMPTY_STRING);
	$('#leaseProductCode').val(EMPTY_STRING);
	$('#leaseProductCode_desc').html(EMPTY_STRING);
	$('#shippingState').val(EMPTY_STRING);
	$('#shippingState_desc').html(EMPTY_STRING);
	$('#lessorState').val(EMPTY_STRING);
	$('#lessorState_desc').html(EMPTY_STRING);
	$('#dateOfCommencement').val(getCBD());
	$('#invoiceCycleNumber').val(EMPTY_STRING);
	leaseBrowserGrid.clearAll();
	clearErrorFields();
}

function clearErrorFields(){
	$('#lesseCode_error').html(EMPTY_STRING);
	$('#leaseProductCode_error').html(EMPTY_STRING);
	$('#lessorState_error').html(EMPTY_STRING);
	$('#shippingState_error').html(EMPTY_STRING);
	$('#dateOfCommencement_error').html(EMPTY_STRING);
	$('#invoiceCycleNumber_error').html(EMPTY_STRING);
}

function doclearfields(id) {
	switch (id) {
	case 'lesseCode':
		if (isEmpty($('#lesseCode').val())) {
			$('#lesseCode').val(EMPTY_STRING);
			$('#lesseCode_desc').html(EMPTY_STRING);
			$('#lesseCode_error').html(EMPTY_STRING);
		}
		break;
	case 'leaseProductCode':
		if (isEmpty($('#leaseProductCode').val())) {
			$('#leaseProductCode').val(EMPTY_STRING);
			$('#leaseProductCode_desc').html(EMPTY_STRING);
			$('#leaseProductCode_error').html(EMPTY_STRING);
		}
		break;
	case 'lessorState':
		if (isEmpty($('#lessorState').val())) {
			$('#lessorState').val(EMPTY_STRING);
			$('#lessorState_desc').html(EMPTY_STRING);
			$('#lessorState_error').html(EMPTY_STRING);
		}
		break;
	case 'shippingState':
		if (isEmpty($('#shippingState').val())) {
			$('#shippingState').val(EMPTY_STRING);
			$('#shippingState_desc').html(EMPTY_STRING);
			$('#shippingState_error').html(EMPTY_STRING);
		}
		break;
	case 'dateOfCommencement':
		if (isEmpty($('#dateOfCommencement').val())) {
			$('#dateOfCommencement').val(EMPTY_STRING);
			$('#dateOfCommencement_error').html(EMPTY_STRING);
		}
		break;
	case 'invoiceCycleNumber':
		if (isEmpty($('#invoiceCycleNumber').val())) {
			$('#invoiceCycleNumber').val(EMPTY_STRING);
			$('#invoiceCycleNumber_error').html(EMPTY_STRING);
		}
		break;
	}

}
function viewHideFilters() {
	if ($('#leaseFilters').hasClass('hidden')) {
		$('#leaseFilters').removeClass('hidden');
		$("#filter").prop('value', HIDE_FILTER_DETAILS);
		leaseBrowserGrid.enablePaging(true, 15, 9, "leaseBrowserGrid_PA", true);
	} else {
		$('#leaseFilters').addClass('hidden');
		$("#filter").prop('value', SHOW_FILTER_DETAILS);
	}
}

function doHelp(id) {
	switch (id) {
	case 'lesseCode':
		help('COMMON', 'HLP_DEBTOR_CODE', $('#lesseCode').val(), EMPTY_STRING, $('#lesseCode'), function() {
		});
		break;
	case 'leaseProductCode':
		help('COMMON', 'HLP_LEASE_PROD_CODE', $('#leaseProductCode').val(), EMPTY_STRING, $('#leaseProductCode'));
		break;
	case 'shippingState':
		help('COMMON', 'HLP_STATE_CODE', $('#shippingState').val(), EMPTY_STRING, $('#shippingState'));
		break;
	case 'lessorState':
		help('COMMON', 'HLP_STATE_CODE', $('#lessorState').val(), EMPTY_STRING, $('#lessorState'));
		break;
	case 'invoiceCycleNumber':
		help('COMMON', 'HLP_INVOICE_CYCLE_NUMBER', $('#invoiceCycleNumber').val(), EMPTY_STRING, $('#invoiceCycleNumber'));
		break;
	}
}

function validate(id) {
	switch (id) {
	case 'lesseCode':
		lesseCode_val();
		break;
	case 'leaseProductCode':
		leaseProductcode_val();
		break;
	case 'shippingState':
		shippingState_val();
		break;
	case 'lessorState':
		lessorState_val();
		break;
	case 'dateOfCommencement':
		dateOfCommencement_val();
		break;
	case 'invoiceCycleNumber':
		invoiceCycleNumber_val();
		break;
	}
}

function backtrack(id) {
	switch (id) {
	case 'lesseCode':
		setFocusLast('lesseCode');
		break;
	case 'leaseProductCode':
		setFocusLast('lesseCode');
		break;
	case 'shippingState':
		setFocusLast('leaseProductCode');
		break;
	case 'lessorState':
		setFocusLast('shippingState');
		break;
	case 'dateOfCommencement':
		setFocusLast('lessorState');
		break;
	case 'invoiceCycleNumber':
		setFocusLast('dateOfCommencement');
		break;
	case 'submit':
		setFocusLast('invoiceCycleNumber');
		break;
	}
}

function lesseCode_val() {
	var value = $('#lesseCode').val();
	clearError('lesseCode_error');
	if (!isEmpty(value)) {
		if (!isValidCode(value)) {
			setError('lesseCode_error', INVALID_FORMAT);
			return false;
		}
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('SUNDRY_DB_AC', value);
	validator.setValue(ACTION, USAGE);
	validator.setClass('patterns.config.web.forms.lss.qleasebrowserbean');
	validator.setMethod('validateLesseCode');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('lesseCode_error', validator.getValue(ERROR));
		return false;
	} else {
		$('#lesseCode_desc').val(validator.getValue('CUSTOMER_ID'));
	}
	setFocusLast('leaseProductCode');
	return true;
}

function leaseProductcode_val() {
	var value = $('#leaseProductCode').val();
	clearError('leaseProductCode_error');
	if (!isEmpty(value)) {
		if (!isValidCode(value)) {
			setError('leaseProductCode_error', INVALID_FORMAT);
			return false;
		}
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('LEASE_PRODUCT_CODE', $('#leaseProductCode').val());
	validator.setValue(ACTION, $('#action').val());
	validator.setClass('patterns.config.web.forms.lss.qleasebrowserbean');
	validator.setMethod('validateLeaseProductCode');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('leaseProductCode_error', validator.getValue(ERROR));
		$('#leaseProductCode_desc').html(EMPTY_STRING);
		return false;
	} else {
		$('#leaseProductCode_desc').html(validator.getValue('DESCRIPTION'));
	}
	setFocusLast('shippingState');
	return true;
}

function shippingState_val() {
	var value = $('#shippingState').val();
	clearError('shippingState_error');
	if (!isEmpty(value)) {
		if (!isValidCode(value)) {
			setError('shippingState_error', INVALID_FORMAT);
			return false;
		}
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('STATE_CODE', $('#shippingState').val());
	validator.setValue(ACTION, USAGE);
	validator.setClass('patterns.config.web.forms.lss.qleasebrowserbean');
	validator.setMethod('validateShippingState');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('shippingState_error', validator.getValue(ERROR));
		$('#shippingState_desc').html(EMPTY_STRING);
		return false;
	} else {
		$('#shippingState_desc').html(validator.getValue('DESCRIPTION'));
	}
	setFocusLast('lessorState');
	return true;
}

function lessorState_val() {
	var value = $('#lessorState').val();
	clearError('lessorState_error');
	if (!isEmpty(value)) {
		if (!isValidCode(value)) {
			setError('lessorState_error', INVALID_FORMAT);
			return false;
		}
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('STATE_CODE', $('#lessorState').val());
	validator.setValue(ACTION, USAGE);
	validator.setClass('patterns.config.web.forms.lss.qleasebrowserbean');
	validator.setMethod('validateLessorState');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('lessorState_error', validator.getValue(ERROR));
		$('#lessorState_desc').html(EMPTY_STRING);
		return false;
	} else {
		$('#lessorState_desc').html(validator.getValue('DESCRIPTION'));
	}
	setFocusLast('dateOfCommencement');
	return true;
}
function dateOfCommencement_val() {
	var value = $('#dateOfCommencement').val();
	clearError('dateOfCommencement_error');
	if (isEmpty(value)) {
		$('#dateOfCommencement').val(getCBD());
		value = $('#dateOfCommencement').val();
	}
	if (!isDate(value)) {
		setError('dateOfCommencement_error', INVALID_DATE);
		return false;
	}
	if (isDateGreater(value, getCBD())) {
		setError('dateOfCommencement_error', DATE_LECBD);
		return false;
	}
	setFocusLast('invoiceCycleNumber');
	return true;
}
function invoiceCycleNumber_val() {
	var value = $('#invoiceCycleNumber').val();
	clearError('invoiceCycleNumber_error');
	if (!isEmpty(value)) {
		if (!isValidCode(value)) {
			setError('invoiceCycleNumber_error', INVALID_FORMAT);
			return false;
		}
	}
	clearError('invoiceCycleNumber_error');
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('INV_CYCLE_NUMBER', $('#invoiceCycleNumber').val());
	validator.setValue(ACTION, USAGE);
	validator.setClass('patterns.config.web.forms.lss.qleasebrowserbean');
	validator.setMethod('validateInvoiceCycleNumber');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('invoiceCycleNumber_error', validator.getValue(ERROR));
		$('#invoiceCycleNumber_desc').html(EMPTY_STRING);
		return false;
	}
	setFocusLast('submit');
	return true;
}
function qcustomerreg(rowId) {
	var pkValue = rowId;
	window.scroll(0, 0);
	showWindow('QCUSTOMERREG', getBasePath() + 'Renderer/reg/qcustomerreg.jsp', QUERY_CUSTOMER_REG, PK + '=' + pkValue + "&" + SOURCE + "=" + MAIN, true, false, false, 1066, 550);
	resetLoading();
}
function qcustagreement(rowId) {
	var pkValue = rowId;
	window.scroll(0, 0);
	showWindow('QCUSTAGREEMENT', getBasePath() + 'Renderer/lss/qcustagreement.jsp', QUERY_CUSTOMER_AGREEMENT, PK + '=' + pkValue + "&" + SOURCE + "=" + MAIN, true, false, false, 1066, 550);
	resetLoading();
}
function qlease(rowId) {
	var pkValue = rowId;
	window.scroll(0, 0);
	showWindow('QLEASE', getBasePath() + 'Renderer/lss/qlease.jsp', QUERY_LEASE_DETAILS, PK + '=' + pkValue + "&" + SOURCE + "=" + MAIN, true, false, false, 1160, 550);
	resetLoading();
}
function qplcdetailsview(rowId) {
	var pkValue = rowId;
	window.scroll(0, 0);
	showWindow('QPLCDETAILSVIEW', getBasePath() + 'Renderer/lss/qplcdetailsview.jsp', QUERY_POLICE_DETAILS, PK + '=' + pkValue + "&" + SOURCE + "=" + MAIN, true, false, false, 1066, 550);
	resetLoading();
}
function revalidate() {
	getLeaseDetails();
}
function getLeaseDetails() {
	clearErrorFields();
	leaseBrowserGrid.clearAll();
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('LESSEE_CODE', $('#lesseCode').val());
	validator.setValue('LEASE_PRODUCT_CODE', $('#leaseProductCode').val());
	validator.setValue('LESSOR_STATE_CODE', $('#lessorState').val());
	validator.setValue('SHIPPING_STATE_CODE', $('#shippingState').val());
	validator.setValue('DATE_OF_COMMENCEMENT', $('#dateOfCommencement').val());
	validator.setValue('INV_CYCLE_NUMBER', $('#invoiceCycleNumber').val());
	validator.setValue('ACTION', USAGE);
	validator.setClass('patterns.config.web.forms.lss.qleasebrowserbean');
	validator.setMethod('getLeaseBrowserDetails');
	validator.sendAndReceiveAsync(showGrid);
	showMessage(getProgramID(), Message.PROGRESS);
}

function showGrid(){
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		if (validator.getValue(ERROR_FIELD) != EMPTY_STRING) {
			hideAllMessages(getProgramID());
			setError(validator.getValue(ERROR_FIELD) + '_error', validator.getValue(ERROR));
			setFocusLast(validator.getValue(ERROR_FIELD));
		} else {
			showMessage(getProgramID(), Message.ERROR, validator.getValue(ERROR));
		}
		return;
	}else{
		if (validator.getValue(RESULT) == DATA_UNAVAILABLE) {
			showMessage(getProgramID(), Message.WARN, NO_RECORD);
			return;
		}else{
			leaseBrowserGrid.loadXMLString(validator.getValue(RESULT_XML));
		}	
	} 
	hideAllMessages(getProgramID());
	
}