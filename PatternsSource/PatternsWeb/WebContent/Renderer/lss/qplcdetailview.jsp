<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<%@taglib prefix="message" tagdir="/WEB-INF/tags/message"%>
<web:bundle baseName="patterns.config.web.forms.lss.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/lss/qplcdetailview.js" />
	</web:dependencies>
	<style>
			.pre-lease-info { 
				text-align: center;
			 }
			.pre-lease-info i {
				font-size:20px;
			 }
			 .pre-lease-info a:hover, a:focus {
			    text-decoration: none;
			}
			.pre-lease-info .control-label{
				font-size:11px;
				padding:0px;
			}
	</style>
	<web:loadPK />
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="qplcdetailview.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/lss/qplcdetailview" id="qplcdetailview" method="POST">
				<web:viewContent id="po_view1">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="qplcdetailview.preleasereference" var="program" />
										</web:column>
										<web:column>
											<type:dateDaySL property="preLeaseReference" id="preLeaseReference" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle width="200px" />
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="qplcdetailview.lessecode" var="program" />
										</web:column>
										<web:column>
											<type:lesseeCode property="lesseCode" id="lesseCode" readOnly="true" />
										</web:column>
										<web:column>
											<web:legend key="qplcdetailview.customerid" var="program" />
										</web:column>
										<web:column>
											<type:customerID property="customerID" id="customerID" readOnly="true" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="qplcdetailview.customername" var="program" />
										</web:column>
										<web:column>
											<type:description property="customerName" id="customerName" readOnly="true" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle width="200px" />
										<web:columnStyle width="70px" />
										<web:columnStyle width="150px"/>	
										<web:columnStyle width="100px" />
										<web:columnStyle width="130px" />
										<web:columnStyle width="130px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="qplcdetailview.leaseagreementsl" var="program" />
										</web:column>
										<web:column>
											<type:serial property="leaseAgreementSl" id="leaseAgreementSl" readOnly="true" />
										</web:column>
										<web:column>
											<web:legend key="qplcdetailview.scheduleid" var="program" />
										</web:column>
										<web:column>
											<type:scheduleID property="scheduleID" id="scheduleID" readOnly="true" />
										</web:column>
										<web:column>
											<div class="pre-lease-info">
												<a href="javascript:void(0)" onClick="viewCustomerDetails()"> <i class="fa fa-user"></i> <br> <web:legend key="qplcdetailview.customerdetails" var="program" />
												</a>
											</div>
										</web:column>
										<web:column>
											<div class="pre-lease-info">
												<a href="javascript:void(0)" onClick="viewInterimInterestLedgerDetails()"><i class="fa fa-user"></i> <br> <web:legend key="qplcdetailview.intermintrstledger" var="program" /> </a>
											</div>
										</web:column>
										<web:column>
											<div class="pre-lease-info">
												<a href="javascript:void(0)" onClick="viewLeaseDetails()"><i class="fa fa-user"></i> <br> <web:legend key="qplcdetailview.leasedetails" var="program" /></a>
											</div>
										</web:column>
										<web:column>
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
							<web:table>
								<web:rowEven>
									<web:column>
										<type:button id="submit" key="form.submit" var="common" onclick="getPODetails()" />
										<type:reset id="reset" key="form.reset" var="common" />
									</web:column>
								</web:rowEven>
							</web:table>
							</web:section>
							<message:messageBox id="qplcdetailview" />
							<web:section>
								<web:viewTitle var="program" key="qplcdetailview.purchaseOrderDetails"></web:viewTitle>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column span="2">
											<web:viewContent id="po_view4">
												<web:grid paging="true" height="300px" width="1040px" id="purchaseOrderDetailGrid" src="lss/qplcdetailview_poGrid.xml">
												</web:grid>
											</web:viewContent>
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:viewContent id="showInvoiceDetail" styleClass="hidden">
								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="180px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowEven>
											<web:column>
												<web:grid height="300px" width="630px" id="showInvoiceDetailGrid" src="lss/qplcdetailview_invoiceGrid.xml">
												</web:grid>
											</web:column>
										</web:rowEven>
									</web:table>
								</web:section>
							</web:viewContent>
							<web:viewContent id="showInterimInterestDetail" styleClass="hidden">
								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="180px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowEven>
											<web:column>
												<web:grid height="300px" width="760px" id="showInterimInterestDetailGrid" src="lss/qplcdetailview_intermGrid.xml">
												</web:grid>
											</web:column>
										</web:rowEven>
									</web:table>
								</web:section>
							</web:viewContent>
							<web:viewContent id="showPaymentDetail" styleClass="hidden">
								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="180px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowEven>
											<web:column>
												<web:grid height="300px" width="760px" id="showPaymentDetailGrid" src="lss/qplcdetailview_paymentDetailGrid.xml">
												</web:grid>
											</web:column>
										</web:rowEven>
									</web:table>
								</web:section>
							</web:viewContent>
						</web:sectionBlock>
					</web:dividerBlock>
				</web:viewContent>
				<web:element property="preLeaseContractRefParam" />
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
	<web:contextSearch />
</web:fragment>
