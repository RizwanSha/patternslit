<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.lss.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/lss/eleasepo.js" />
	</web:dependencies>
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="eleasepo.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/lss/eleasepo" id="eleasepo" method="POST">
				<web:dividerBlock>
					<type:optionBox add="true" modify="false" view="true" />
				</web:dividerBlock>
				<web:viewContent id="po_view1" styleClass="hidden">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="eleasepo.preleasecontract" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:dateDaySL property="preLeaseContractRef" id="preLeaseContractRef" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle width="380px" />
										<web:columnStyle width="120px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="eleasepo.lessecode" var="program" />
										</web:column>
										<web:column>
											<type:lesseeCodeDisplay property="lesseCode" id="lesseCode" />
										</web:column>
										<web:column>
											<web:legend key="eleasepo.customerid" var="program" />
										</web:column>
										<web:column>
											<type:customerIDDisplay property="customerID" id="customerID" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:viewContent id="scheduleDtl">
								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="200px" />
											<web:columnStyle width="380px" />
											<web:columnStyle width="120px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowEven>
											<web:column>
												<web:legend key="eleasepo.aggrementno" var="program"  />
											</web:column>
											<web:column>
												<type:agreementNoDisplay property="aggrementNumber" id="aggrementNumber" />
											</web:column>
											<web:column>
												<web:legend key="eleasepo.scheduleid" var="program" />
											</web:column>
											<web:column>
												<type:scheduleIDDisplay property="scheduleID" id="scheduleID" />
											</web:column>
										</web:rowEven>
									</web:table>
								</web:section>
							</web:viewContent>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="eleasepo.custname" var="program" />
										</web:column>
										<web:column>
											<type:descriptionDisplay property="customerName" id="customerName" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="eleasepo.poserial" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:serial property="poSerial" id="poSerial" lookup="true" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:sectionTitle var="program" key="eleasepo.section" />
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="eleasepo.podate" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:date property="poDate" id="poDate" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="eleasepo.ponumber" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:description property="poNumber" id="poNumber" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="eleasepo.supplierid" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:code property="supplierId" id="supplierId" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="eleasepo.poamount" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:currencySmallAmount property="poAmount" id="poAmount" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:sectionTitle var="program" key="eleasepo.section1" />
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="eleasepo.assetclassifi" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:assetTypeCode property="assetClassifi" id="assetClassifi" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle width="380px" />
										<web:columnStyle width="120px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="eleasepo.assetdesc" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:remarks property="assetDescription" id="assetDescription" />
										</web:column>
										<web:column>
											<web:legend key="eleasepo.quantity" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:fiveDigit property="quantity" id="quantity" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="eleasepo.assetmodel" var="program" />
										</web:column>
										<web:column>
											<type:otherInformation25 property="assetModel" id="assetModel" />
										</web:column>
										<web:column>
											<web:legend key="eleasepo.configuration" var="program" />
										</web:column>
										<web:column>
											<type:otherInformation25 property="configuration" id="configuration" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:rowEven>
										<web:column>
											<web:gridToolbar gridName="eleasepo_innerGrid" cancelFunction="eleasepo_cancelGrid" editFunction="eleasepo_editGrid" addFunction="eleasepo_addGrid" insertAtFirsstNotReq="true"  />
											<web:element property="xmleleasepoGrid" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:grid height="240px" width="951px" id="eleasepo_innerGrid" src="lss/eleasepo_innerGrid.xml">
											</web:grid>
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="form.remarks" var="common" />
										</web:column>
										<web:column>
											<type:remarks property="remarks" id="remarks" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column span="2">
											<web:submitReset />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
						</web:sectionBlock>
					</web:dividerBlock>
				</web:viewContent>
				<web:element property="command" />
				<web:element property="action" />
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
	<web:queryGrid />
	<web:TBAGrid />
	<web:contextSearch />
</web:fragment>