<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<web:bundle baseName="patterns.config.web.forms.lss.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/lss/qleaseint.js" />
	</web:dependencies>
	<web:loadPK />
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="qleaseint.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:viewContent id="po_view1">
				<web:dividerBlock>
					<web:sectionBlock width="width-100p" align="left">
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="180px" />
									<web:columnStyle width="380px" />
									<web:columnStyle width="120px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="eleaseint.lessecode" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:lesseeCodeDisplay property="lesseCode" id="lesseCode" />
									</web:column>
									<web:column>
										<web:legend key="eleaseint.customerid" var="program" />
									</web:column>
									<web:column>
										<type:customerIDDisplay property="customerID" id="customerID" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="180px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowOdd>
									<web:column>
										<web:legend key="eleaseint.custname" var="program" />
									</web:column>
									<web:column>
										<type:descriptionDisplay property="customerName" id="customerName" />
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="180px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="eleaseint.aggrementno" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:agreementNoDisplay property="aggrementNumber" id="aggrementNumber" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="eleaseint.scheduleid" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:scheduleIDDisplay property="scheduleID" id="scheduleID" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
										<web:column>
											<web:legend key="eleaseint.entrySerial" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:fiveDigitDisplay property="entrySerial" id="entrySerial"  />
										</web:column>
									</web:rowEven>
							</web:table>
						</web:section>
						<web:sectionTitle var="program" key="eleaseint.section" />
						<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="eleaseint.dateOfEntry" var="program" />
										</web:column>
										<web:column>
											<type:dateDisplay property="dateOfEntry" id="dateOfEntry" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="eleaseint.fromDate" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:dateDisplay property="fromDate" id="fromDate" />
										</web:column>
										<web:column>
											<web:legend key="eleaseint.uptoDate" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:dateDisplay property="uptoDate" id="uptoDate" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="eleaseint.noOfDays" var="program" />
										</web:column>
										<web:column>
											<type:fiveDigitDisplay property="noOfDays" id="noOfDays"  />
										</web:column>
										<web:column>
											<web:legend key="eleaseint.interestRate" var="program" />
										</web:column>
										<web:column>
											<type:threeDigitTwoDecimalDisplay property="interestRate" id="interestRate" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="eleaseint.interestOnAmount" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:currencySmallAmountDisplay property="interestOnAmount" id="interestOnAmount" />
										</web:column>
										<web:column>
											<web:legend key="eleaseint.amountToBeBilled" var="program" />
										</web:column>
										<web:column>
											<type:currencySmallAmountDisplay property="amountToBeBilled" id="amountToBeBilled" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="eleaseint.dateOfInvoicingBilling" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:dateDisplay property="dateOfInvoicingBilling" id="dateOfInvoicingBilling" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="180px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="form.remarks" var="common" />
									</web:column>
									<web:column>
										<type:remarksDisplay property="remarks" id="remarks" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:auditDisplay />
					</web:sectionBlock>
				</web:dividerBlock>
			</web:viewContent>
		</web:viewSubPart>
	</web:viewPart>
</web:fragment>
