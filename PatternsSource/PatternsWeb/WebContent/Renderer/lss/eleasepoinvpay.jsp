
<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.lss.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/lss/eleasepoinvpay.js" />
	</web:dependencies>
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="eleasepoinvpay.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/lss/eleasepoinvpay" id="eleasepoinvpay" method="POST">
				<web:dividerBlock>
					<type:optionBox add="true" modify="false" view="true" />
				</web:dividerBlock>
				<web:viewContent id="po_view1" styleClass="hidden">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="eleasepoinvpay.preleaseref" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:dateDaySL property="preLeaseRef" id="preLeaseRef" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle width="320px" />
										<web:columnStyle width="250px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="eleasepoinvpay.leasecode" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:lesseeCode property="leaseCode" id="leaseCode" readOnly="true" />
										</web:column>
										<web:column>
											<web:legend key="eleasepoinvpay.custid" var="program" />
										</web:column>
										<web:column>
											<type:customerID property="custId" id="custId" readOnly="true" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>

							<web:viewContent id="scheduleDetail">
								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="200px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowEven>
											<web:column>
												<web:legend key="eleasepoinvpay.agreeno" var="program" />
											</web:column>
											<web:column>
												<type:agreementNo property="agreeNo" id="agreeNo" readOnly="true" />
											</web:column>
										</web:rowEven>
										<web:rowOdd>
											<web:column>
												<web:legend key="eleasepoinvpay.scheduleid" var="program" />
											</web:column>
											<web:column>
												<type:scheduleID property="scheduleId" id="scheduleId" readOnly="true" />
											</web:column>
										</web:rowOdd>
									</web:table>
								</web:section>
							</web:viewContent>


							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="eleasepoinvpay.custname" var="program" />
										</web:column>
										<web:column>
											<type:descriptionDisplay property="custName" id="custName" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:viewTitle var="program" key="eleasepoinvpay.details" />
							<type:error property="gridError" />
							<web:section>
								<web:table>
									<web:rowOdd>
										<web:column>
											<web:grid height="210px" width="856px" id="pgmGrid" src="lss/eleasepoinvpay_Grid.xml">
											</web:grid>
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
											<web:columnStyle width="200px" />
										<web:columnStyle width="320px" />
										<web:columnStyle width="250px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="eleasepoinvpay.selectedposl" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:POSerialDisplay property="selectedPoSl" id="selectedPoSl" />
										</web:column>
											<web:column>
											<web:legend key="eleasepoinvpay.paymentsl" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:serial property="paymentSl" id="paymentSl" lookup="true" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="eleasepoinvpay.payfor" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:combo property="payFor" id="payFor" datasourceid="COMMON_PAY_FOR" />
										</web:column>
										<web:column>
											<web:legend key="eleasepoinvpay.selectedinvsl" var="program" />
										</web:column>
										<web:column>
											<type:serial property="selectedInvSl" id="selectedInvSl" lookup="true" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:viewTitle var="program" key="eleasepoinvpay.detailspay" />
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="eleasepoinvpay.date" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:date property="date" id="date" />
										</web:column>
									</web:rowEven>

								</web:table>
							</web:section>

							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle width="330px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="eleasepoinvpay.payfavour" var="program" />
										</web:column>
										<web:column>
											<type:codeDisplay property="payFavour" id="payFavour" />
										</web:column>
									<web:column>
												<web:anchorMessage var="program" key="eleasepoinvpay.link" onclick="qsupplierdetails()" style="text-decoration:underline;cursor:pointer" />
											</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle width="320px" />
										<web:columnStyle width="250px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="eleasepoinvpay.amtpaid" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:currencySmallAmount property="amtPaid" id="amtPaid" />
										</web:column>
										<web:column>
											<web:legend key="eleasepoinvpay.eqlocal" var="program" />
										</web:column>
										<web:column>
											<type:currencySmallAmount property="eqLocal" id="eqLocal" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="eleasepoinvpay.payref" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:description property="payRef" id="payRef" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="form.remarks" var="common" />
										</web:column>
										<web:column>
											<type:remarks property="remarks" id="remarks" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column span="2">
											<web:submitReset />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
						</web:sectionBlock>
					</web:dividerBlock>
				</web:viewContent>
				<web:element property="command" />
				<web:element property="action" />
				<web:element property="xmlGrid" />
				<web:element property="selectedPoSlDisplay" />
				<web:element property="selectedInvSlDisplay" />
				<web:element property="amtPaidHidden" />
				<web:element property="eqLocalHidden" />
				<web:element property="currHidden" />
		
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
	<web:queryGrid />
	<web:TBAGrid />
	<web:contextSearch />
</web:fragment>
