<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.lss.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/lss/eleasepdc.js" />
	</web:dependencies>
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="eleasepdc.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/lss/eleasepdc" id="eleasepdc" method="POST">
				<web:dividerBlock>
					<type:optionBox add="true" modify="false" view="true" />
				</web:dividerBlock>
				<web:viewContent id="po_view1">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="eleasepdc.lessecode" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:lesseeCode property="lesseCode" id="lesseCode" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
						</web:sectionBlock>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle width="220px" />
									<web:columnStyle width="120px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowOdd>
									<web:column>
										<web:legend key="eleasepdc.customerid" var="program" />
									</web:column>
									<web:column>
										<type:customerID property="customerID" id="customerID" readOnly="true" />
									</web:column>
									<web:column>
										<web:legend key="eleasepdc.custname" var="program" />
									</web:column>
									<web:column>
										<type:dynamicDescriptionDisplay property="customerName" id="customerName" length="50" size="50" />
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="eleasepdc.aggrementno" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:agreementNo property="aggrementNumber" id="aggrementNumber" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="eleasepdc.scheduleid" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:scheduleID property="scheduleID" id="scheduleID" lookup="true"/>
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="eleasepdc.pdclotserial" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:pdcLotSerial property="pdcLotSerial" id="pdcLotSerial" lookup="true" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="eleasepdc.disablepdc" var="program" />
									</web:column>
									<web:column>
										<type:checkbox property="disablePdcLot" id="disablePdcLot" />
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
						<type:error property="pdcDetailsError" />
						<web:viewContent id="disablePdc">
							<web:viewContent id="bankDtl_view">
								<type:tabbarBody id="bankpdcDetails_TAB_0">
									<web:section>
										<web:table>
											<web:columnGroup>
												<web:columnStyle width="190px" />
												<web:columnStyle />
											</web:columnGroup>
											<web:rowEven>
												<web:column>
													<web:legend key="eleasepdc.pdclotdate" var="program"  />
												</web:column>
												<web:column>
													<type:date property="pdclotDate" id="pdclotDate" readOnly="true" />
												</web:column>
											</web:rowEven>
											<web:rowOdd>
												<web:column>
													<web:legend key="eleasepdc.ifsc" var="program"  />
												</web:column>
												<web:column>
													<type:ifscCode property="ifscCode" id="ifscCode" />
												</web:column>
											</web:rowOdd>
											<web:rowEven>
												<web:column>
													<web:legend key="eleasepdc.bankame" var="program" mandatory="true" />
												</web:column>
												<web:column>
													<type:otherInformation50 property="bankName" id="bankName" />
												</web:column>
											</web:rowEven>
											<web:rowOdd>
												<web:column>
													<web:legend key="eleasepdc.brnname" var="program" mandatory="true" />
												</web:column>
												<web:column>
													<type:description property="branchName" id="branchName" />
												</web:column>
											</web:rowOdd>
											<web:rowEven>
												<web:column>
													<web:legend key="form.accountnumber" var="common" mandatory="true" />
												</web:column>
												<web:column>
													<type:otherInformation50 property="accountNumber" id="accountNumber" />
												</web:column>
											</web:rowEven>
											<web:rowOdd>
												<web:column>
													<web:legend key="eleasepdc.accname" var="program" mandatory="true" />
												</web:column>
												<web:column>
													<type:description property="accountName" id="accountName" />
												</web:column>
											</web:rowOdd>
											<web:rowEven>
												<web:column>
													<web:legend key="eleasepdc.bankingprocesscode" var="program" mandatory="true" />
												</web:column>
												<web:column>
													<type:processCode property="bankingProcessCode" id="bankingProcessCode" />
												</web:column>
											</web:rowEven>
											<web:rowOdd>
												<web:column>
													<web:legend key="eleasepdc.noofchque" var="program" mandatory="true" />
												</web:column>
												<web:column>
													<type:threeDigit property="noOfChque" id="noOfChque" />
												</web:column>
											</web:rowOdd>
										</web:table>
									</web:section>
								</type:tabbarBody>
							</web:viewContent>
							<web:viewContent id="pdcDtl_view">
								<type:tabbarBody id="bankpdcDetails_TAB_1">
									<web:section>
										<web:table>
											<web:columnGroup>
												<web:columnStyle width="190px" />
												<web:columnStyle />
											</web:columnGroup>
											<web:rowOdd>
												<web:column>
													<web:legend key="eleasepdc.chqinstrumentno" var="program" mandatory="true" />
												</web:column>
												<web:column>
													<type:number property="chqInstrumentNo" id="chqInstrumentNo" length="12" size="12" />
												</web:column>
											</web:rowOdd>
											<web:rowEven>
												<web:column>
													<web:legend key="eleasepdc.chqdate" var="program" mandatory="true" />
												</web:column>
												<web:column>
													<type:date property="chqDate" id="chqDate" />
												</web:column>
											</web:rowEven>
											<web:rowOdd>
												<web:column>
													<web:legend key="eleasepdc.chqamt" var="program" mandatory="true" />
												</web:column>
												<web:column>
													<type:currencySmallAmount property="chqAmount" id="chqAmount" currEditable="true" />
												</web:column>
											</web:rowOdd>
										</web:table>
									</web:section>
									<web:section>
										<web:table>
											<web:rowEven>
												<web:column>
													<web:gridToolbar gridName="eleasepdc_innerGrid" cancelFunction="eleasepdc_cancelGrid" editFunction="eleasepdc_editGrid" addFunction="eleasepdc_addGrid" />
													<web:element property="xmleleasepdcGrid" />
												</web:column>
											</web:rowEven>
											<web:rowOdd>
												<web:column>
													<web:grid height="180px" width="673px" id="eleasepdc_innerGrid" src="lss/eleasepdc_innerGrid.xml">
													</web:grid>
												</web:column>
											</web:rowOdd>
										</web:table>
									</web:section>
								</type:tabbarBody>
							</web:viewContent>
							<type:tabbar height="height:350px;" name="Bank Details $$ PDC Details" width="width:1350px;" required="2" id="bankpdcDetails" selected="0">
							</type:tabbar>
						</web:viewContent>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="form.remarks" var="common" />
									</web:column>
									<web:column>
										<type:remarks property="remarks" id="remarks" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column span="2">
										<web:submitReset />
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
					</web:dividerBlock>
				</web:viewContent>
				<web:element property="command" />
				<web:element property="action" />
				<web:element property="pdcCount" />
				<web:element property="noOfChequeOld" />
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
	<web:queryGrid />
	<web:TBAGrid />
	<web:contextSearch />
</web:fragment>
