<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<web:bundle baseName="patterns.config.web.forms.lss.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/lss/qleasepo.js" />
	</web:dependencies>
	<web:loadPK />
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="qleasepo.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:viewContent id="po_view1">
				<web:dividerBlock>
					<web:sectionBlock width="width-100p" align="left">
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="eleasepo.preleasecontract" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:dateDaySLDisplay property="preLeaseContractRef" id="preLeaseContractRef" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle width="380px" />
									<web:columnStyle width="120px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowOdd>
									<web:column>
										<web:legend key="eleasepo.lessecode" var="program" />
									</web:column>
									<web:column>
										<type:lesseeCodeDisplay property="lesseCode" id="lesseCode" />
									</web:column>
									<web:column>
										<web:legend key="eleasepo.customerid" var="program" />
									</web:column>
									<web:column>
										<type:customerIDDisplay property="customerID" id="customerID" />
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
						<web:viewContent id="scheduleDtl">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle width="380px" />
										<web:columnStyle width="120px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="eleasepo.aggrementno" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:agreementNoDisplay property="aggrementNumber" id="aggrementNumber" />
										</web:column>
										<web:column>
											<web:legend key="eleasepo.scheduleid" var="program" />
										</web:column>
										<web:column>
											<type:scheduleIDDisplay property="scheduleID" id="scheduleID" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
						</web:viewContent>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowOdd>
									<web:column>
										<web:legend key="eleasepo.custname" var="program" />
									</web:column>
									<web:column>
										<type:descriptionDisplay property="customerName" id="customerName" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="eleasepo.poserial" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:serialDisplay property="poSerial" id="poSerial" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:sectionTitle var="program" key="eleasepo.section" />
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowOdd>
									<web:column>
										<web:legend key="eleasepo.podate" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:dateDisplay property="poDate" id="poDate" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="eleasepo.ponumber" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:descriptionDisplay property="poNumber" id="poNumber" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="eleasepo.supplierid" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:supplierIDDisplay property="supplierId" id="supplierId" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="eleasepo.poamount" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:currencySmallAmountDisplay property="poAmount" id="poAmount" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:sectionTitle var="program" key="eleasepo.section1" />
						<web:section>
							<web:table>
								<web:rowOdd>
									<web:column>
										<web:grid height="240px" width="901px" id="eleasepo_innerGrid" src="lss/eleasepo_innerGrid.xml">
										</web:grid>
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="form.remarks" var="common" />
									</web:column>
									<web:column>
										<type:remarksDisplay property="remarks" id="remarks" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:auditDisplay />
					</web:sectionBlock>
				</web:dividerBlock>
			</web:viewContent>
		</web:viewSubPart>
	</web:viewPart>
</web:fragment>
