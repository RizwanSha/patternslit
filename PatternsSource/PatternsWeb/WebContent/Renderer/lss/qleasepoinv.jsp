<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.lss.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/lss/qleasepoinv.js" />
	</web:dependencies>
	<web:loadPK />
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="qleasepoinv.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:viewContent id="po_view1">
				<web:dividerBlock>
					<web:sectionBlock width="width-100p" align="left">
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="eleasepoinv.preleaseref" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:dateDaySLDisplay property="preLeaseRef" id="preLeaseRef" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle width="180px" />
									<web:columnStyle width="180px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowOdd>
									<web:column>
										<web:legend key="eleasepoinv.leasecode" var="program" />
									</web:column>
									<web:column>
										<type:lesseeCodeDisplay property="leaseCode" id="leaseCode" />
									</web:column>
									<web:column>
										<web:legend key="eleasepoinv.custid" var="program" />
									</web:column>
									<web:column>
										<type:customerIDDisplay property="custId" id="custId" />
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
						<web:viewContent id="scheduleDtl">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="eleasepoinv.agreeno" var="program" />
										</web:column>
										<web:column>
											<type:agreementNoDisplay property="agreeNo" id="agreeNo" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="eleasepoinv.scheduleid" var="program" />
										</web:column>
										<web:column>
											<type:scheduleIDDisplay property="scheduleId" id="scheduleId" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
						</web:viewContent>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="eleasepoinv.custname" var="program" />
									</web:column>
									<web:column>
										<type:descriptionDisplay property="custName" id="custName" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:viewTitle var="program" key="eleasepoinv.details" />
						<web:section>
							<web:table>
								<web:rowOdd>
									<web:column>
										<web:grid height="110px" width="921px" id="pgmGrid" src="lss/eleasepoinv_Grid.xml">
										</web:grid>
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle width="180px" />
									<web:columnStyle width="180px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="eleasepoinv.purchordsl" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:scheduleIDDisplay property="purchOrdSl" id="purchOrdSl" />
									</web:column>
									<web:column>
										<web:legend key="eleasepoinv.invsl" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:invoiceSerialDisplay property="invSl" id="invSl" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:viewTitle var="program" key="eleasepoinv.invdetails" />
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowOdd>
									<web:column>
										<web:legend key="eleasepoinv.invfor" var="program" />
									</web:column>
									<web:column>
										<type:comboDisplay property="invFor" id="invFor" datasourceid="ELEASEPOINV_INV_FOR" />
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="eleasepoinv.invdate" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:dateDisplay property="invDate" id="invDate" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowOdd>
									<web:column>
										<web:legend key="eleasepoinv.invnum" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:descriptionDisplay property="invNum" id="invNum" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="eleasepoinv.invamt" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:currencySmallAmountDisplay property="invAmt" id="invAmt" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:viewContent id="paydet" styleClass="hidden">
							<web:viewTitle var="program" key="eleasepoinv.paydet" />
							<web:section>
								<web:table>
									<web:rowOdd>
										<web:column>
											<web:grid height="200px" width="541px" id="pgmGrid2" src="lss/eleasepoinv_Grid1.xml">
											</web:grid>
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
						</web:viewContent>
						<web:sectionTitle var="program" key="eleasepoinv.section1" />
						<web:section>
							<web:table>
								<web:rowEven>
									<web:column>
										<web:grid height="220px" width="941px" id="eleasepoinvasset_innerGrid" src="lss/eleasepoinvasset_innerGrid.xml">
										</web:grid>
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowOdd>
									<web:column>
										<web:legend key="form.remarks" var="common" />
									</web:column>
									<web:column>
										<type:remarksDisplay property="remarks" id="remarks" />
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
						<web:auditDisplay />
					</web:sectionBlock>
				</web:dividerBlock>
			</web:viewContent>
		</web:viewSubPart>
	</web:viewPart>
</web:fragment>
