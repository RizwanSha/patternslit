<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.lss.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/lss/qleaseclosure.js" />
	</web:dependencies>
	<web:loadPK />
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="eleaseclosure.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:viewContent id="po_view1">
				<web:dividerBlock>
					<web:sectionBlock width="width-100p" align="left">
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="eleaseclosure.entrydate" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:dateDaySLDisplay property="entry" id="entry" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowOdd>
									<web:column>
										<web:legend key="eleaseclosure.leaseCode" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:lesseeCodeDisplay property="lesseCode" id="lesseCode" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="eleaseclosure.customerid" var="program" />
									</web:column>
									<web:column>
										<type:customerCodeDisplay property="customerID" id="customerID" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="eleaseclosure.customername" var="program" />
									</web:column>
									<web:column>
										<type:nameDisplay property="customerName" id="customerName" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="eleaseclosure.agreementnumber" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:agreementNoDisplay property="agreementNumber" id="agreementNumber" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="eleaseclosure.scheduleid" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:scheduleCodeDisplay property="scheduleID" id="scheduleID" />
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
						<web:viewContent id="hierarchy_view" styleClass="hidden">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
										</web:column>
										<web:column>
											<web:anchorMessage var="program" key="qlease.link" onclick="qLease()" style="text-decoration:underline;cursor:pointer" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
						</web:viewContent>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowOdd>
									<web:column>
										<web:legend key="eleaseclosure.referencenumber" var="program" />
									</web:column>
									<web:column>
										<type:otherInformation20Display property="referenceNumber" id="referenceNumber" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="form.remarks" var="common" mandatory="true" />
									</web:column>
									<web:column>
										<type:remarksDisplay property="remarks" id="remarks" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:auditDisplay />
					</web:sectionBlock>
				</web:dividerBlock>
			</web:viewContent>
		</web:viewSubPart>
	</web:viewPart>
</web:fragment>
