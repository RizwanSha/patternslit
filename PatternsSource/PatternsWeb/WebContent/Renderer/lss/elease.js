var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'ELEASE';
var curCode;
var win;
var gridRentAmt;
var taxTotal;
var checkRental;
var taxSerial;
var assetRequired;
var configRequired;
var prevCommencementValue = null;
var reValNotReq = false;
var hideUnhide = false;
var hideWin = null;
var qtaxCheck = true;
function init() {
	refreshTBAGrid();
	refreshQueryGrid();
	if ($('.offcanvas-menu').length > 0) {
		var offcanvasVisible = $('.offcanvas-menu').is(':visible');
		if (offcanvasVisible) {
			slideMenu();
		}
	}
	hide('noOfAssetReq');
	hide('editFinancialAmount');
	hide('paymentDetails');
	hide('prinsInterestWise');
	hide('taxDetails');
	hide('showFinancialGrid');
	hide('closeFinGrid');
	hide('agreeAndContinue');
	hide('copyText');
	if (_redisplay) {
		billBranch_val();
		if ($('#dealType').val() == 'U') {
			$('#uptoToner').prop('readonly', true);
		} else {
			$('#uptoToner').prop('readonly', false);
		}
		$('#iAgreeContinue').prop('disabled', true);
		$('#iAgree').prop('disabled', true);
		if ($('#leaseProductType').val() == 'O') {
			$('#internalRateReturn').prop('readonly', true);
			$('#internalRateReturn').val(EMPTY_STRING);
			$('#internalRateReturn_error').html(EMPTY_STRING);
			$('#assureRVType').val('A');
			$('#assureRVTypeValue').val('A');
		} else {
			$('#internalRateReturn').prop('readonly', false);
			$('#internalRateReturn_error').html(EMPTY_STRING);
			$('#assureRVType').val('T');
			$('#assureRVTypeValue').val('T');
		}
		leaseNoOfAssets_Grid.clearAll();
		if (!isEmpty($('#xmlleaseNoOfAssetsGrid').val())) {
			leaseNoOfAssets_Grid.setSerializationLevel(false, false, false, false, false, true);
			leaseNoOfAssets_Grid.loadXMLString($('#xmlleaseNoOfAssetsGrid').val());
		}

		leaseRentalCharges_Grid.clearAll();
		if (!isEmpty($('#xmlleaseRentalChargesGrid').val()))
			leaseRentalCharges_Grid.loadXMLString($('#xmlleaseRentalChargesGrid').val());

		leaseApplicableTax_Grid.clearAll();
		if (!isEmpty($('#xmlleaseApplicableTaxGrid').val())) {
			leaseApplicableTax_Grid.loadXMLString($('#xmlleaseApplicableTaxGrid').val());
			$('#grossTaxAmount').html(taxTotal);
		}

		leasePaymentDetails_Grid.clearAll();
		if (!isEmpty($('#xmlleasePaymentGrid').val())) {
			leasePaymentDetails_Grid.setSerializationLevel(false, false, false, false, false, true);
			leasePaymentDetails_Grid.loadXMLString($('#xmlleasePaymentGrid').val());
		}

		if (!isEmpty($('#xmlleaseFinanceScheduleGrid').val())) {
			$('#noOfslab').html(EMPTY_STRING);
			$('#valueNegative').html(EMPTY_STRING);
			$('#noOfslab').append("<div>No. of Slabs    :<span id ='totalValues' x-large;'></span></div>");
			if ($('#leaseProductType').val() != 'O') {
				financialScheduleNonOLGrid.clearAll();
				financialScheduleNonOLGrid.setSerializationLevel(false, false, false, false, false, true);
				financialScheduleNonOLGrid.loadXMLString($('#xmlleaseFinanceScheduleGrid').val());
				hide('leaseFinanceRentalSchedule');
				show('leaseFinanceSchedule');
				$("#totalValues").html(financialScheduleNonOLGrid.getRowsNum());
			} else {
				financialScheduleOLGrid.clearAll();
				financialScheduleOLGrid.setSerializationLevel(false, false, false, false, false, true);
				financialScheduleOLGrid.loadXMLString($('#xmlleaseFinanceScheduleGrid').val());
				financialScheduleOLGrid.setColumnHidden(1, true);
				financialScheduleOLGrid.setColumnHidden(4, true);
				financialScheduleOLGrid.setColumnHidden(5, true);
				show('leaseFinanceRentalSchedule');
				hide('leaseFinanceSchedule');
				$("#totalValues").html(financialScheduleOLGrid.getRowsNum());
			}
		}
		setCheckbox('iAgree', NO);
		lease._setTabActive('lease_0', true);
		$('#lease').height('275px');
		$('#custLeaseCode').focus();
	}
}

function loadGrid() {
	loadAssetComponentGrid();
	loadGridQuery(CURRENT_PROGRAM_ID, 'RENT_GRID', leaseRentalCharges_Grid);
	loadGridQuery(CURRENT_PROGRAM_ID, 'PAYMENT_GRID', leasePaymentDetails_Grid);
}

function loadAssetComponentGrid() {
	loadGridQuery(CURRENT_PROGRAM_ID, 'ASSETS_GRID', postProcessingAssets);

}

function postProcessingAssets(result) {
	leaseNoOfAssets_Grid.parse(result, function() {
		$xml = $($.parseXML(result));
		($xml).find("row").each(function() {
			$('#taxScheduleCode').val($(this).find("cell")[9].textContent);
		});
		loadGridQuery(CURRENT_PROGRAM_ID, 'COMPONENT_GRID', postProcessingComponent);
	});
}

function postProcessingComponent(result) {
	$xml = $($.parseXML(result));
	var rowDiscSl = 1;
	var rowdata = new DhtmlxXmlBuilder();
	var assetSl = 0;
	($xml).find("row").each(function() {
		assetSl = $(this).find("cell")[1].textContent;
		if (rowDiscSl != assetSl) {
			var rowxml = rowdata.serialize();
			leaseNoOfAssets_Grid.cells2((rowDiscSl - 1), 7).setValue(rowxml);
			rowDiscSl = assetSl;
			rowdata = new DhtmlxXmlBuilder();
		}
		rowdata.addRow([ false, $(this).find("cell")[2].textContent, $(this).find("cell")[3].textContent, $(this).find("cell")[4].textContent, $(this).find("cell")[5].textContent, $(this).find("cell")[6].textContent, $(this).find("cell")[7].textContent, $(this).find("cell")[8].textContent, $(this).find("cell")[9].textContent ]);
	});
	if (assetSl != 0) {
		var rowxml = rowdata.serialize();
		leaseNoOfAssets_Grid.cells2((rowDiscSl - 1), 7).setValue(rowxml);
	}
}

function doTabbarClick(id) {
	if (id == 'lease_0') {
		$('#lease').height('275px');
		setFocusLast('leaseComDate');
		$('#leaseErrors').html(EMPTY_STRING);
	} else if (id == 'lease_1') {
		$('#lease').height('385px');
		setFocusLast('noOfAssets');
		$('#leaseErrors').html(EMPTY_STRING);
	} else if (id == 'lease_2') {
		$('#lease').height('330px');
		setFocusLast('tenor');
		$('#leaseErrors').html(EMPTY_STRING);
	} else if (id == 'lease_3') {
		$('#lease').height('570px');
		setFocusLast('grossAssetCost');
		$('#leaseErrors').html(EMPTY_STRING);
	} else if (id == 'leaseFinance_0') {
		$('#lease').height('500px');
		$('#leaseErrors').html(EMPTY_STRING);
	} else if (id == 'leaseFinance_1') {
		$('#lease').height('440px');
		$('#leaseErrors').html(EMPTY_STRING);
	} else if (id == 'lease_4') {
		$('#leaseErrors').html(EMPTY_STRING);
		setFocusLast('repaymentMode');
		$('#lease').height('610px');
	}
}

function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'ELEASE_MAIN');
}
function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}

function view(source, primaryKey) {
	hideParent('lss/qlease', source, primaryKey, 1150);
	resetLoading();
}

function clearFields() {
	$('#custLeaseCode').val(EMPTY_STRING);
	$('#custLeaseCode_desc').html(EMPTY_STRING);
	$('#custLeaseCode_error').html(EMPTY_STRING);
	$('#custID').val(EMPTY_STRING);
	$('#custName').val(EMPTY_STRING);
	$('#agreementNo').val(EMPTY_STRING);
	$('#agreementNo_error').html(EMPTY_STRING);
	$('#agreementDate').val(EMPTY_STRING);
	$('#agreementNo_desc').html(EMPTY_STRING);
	$('#scheduleID').val(EMPTY_STRING);
	$('#scheduleID_error').html(EMPTY_STRING);
	$('#scheduleID_desc').html(EMPTY_STRING);
	$('#leaseErrors_error').html(EMPTY_STRING);
	$('#assetCurrency').val(EMPTY_STRING);
	$('#assetCurrency_desc').html(EMPTY_STRING);
	$('#leaseProductType').val(EMPTY_STRING);
	$('#leaseProduct').val(EMPTY_STRING);
	$('#leaseProduct_desc').html(EMPTY_STRING);
	$('#assureRVType').val(EMPTY_STRING);
	$('#internalRateReturn').val(EMPTY_STRING);
	$('#internalRateReturn_error').html(EMPTY_STRING);
	$('#internalRateReturn').prop('readonly', true);
	clearCurrencyFields();
	clearNonPKFields();
}

function clearNonPKFields() {
	$('#preLeaseRefDate').val(EMPTY_STRING);
	$('#preLeaseRefSerial').val(EMPTY_STRING);
	$('#preLeaseRef_error').html(EMPTY_STRING);
	$('#iAgreeContinue').prop('disabled', true);
	reValNotReq = false;
	qtaxCheck = true;
	// TAB 1
	$('#leaseComDate_error').html(EMPTY_STRING);
	$('#leaseProduct_error').html(EMPTY_STRING);
	$('#billAddress').val(EMPTY_STRING);
	$('#billAddress_error').html(EMPTY_STRING);
	$('#billAddress_desc').html(EMPTY_STRING);
	$('#shipAddress').val(EMPTY_STRING);
	$('#shippingState').val(EMPTY_STRING);
	$('#shipAddress_error').html(EMPTY_STRING);
	$('#shipAddress_desc').html(EMPTY_STRING);
	$('#shipAddressState').html(EMPTY_STRING);
	$('#billAddressState').html(EMPTY_STRING);
	$('#billBranch').val(EMPTY_STRING);
	$('#billBranch_error').html(EMPTY_STRING);
	$('#billBranch_desc').html(EMPTY_STRING);
	$('#billState').val(EMPTY_STRING);
	$('#billState_error').html(EMPTY_STRING);
	$('#billState_desc').html(EMPTY_STRING);
	$('#invType').val(EMPTY_STRING);

	// TAB 2
	$('#noOfAssets').val(EMPTY_STRING);
	$('#noOfAssets_error').html(EMPTY_STRING);
	$('#taxScheduleCode').val(EMPTY_STRING);
	$('#taxScheduleCode_error').html(EMPTY_STRING);
	$('#grossNetTDS').val(EMPTY_STRING);
	$('#grossNetTDS_error').html(EMPTY_STRING);
	$('#grossNetTDS').prop('selectedIndex', 0);

	// TAB 3
	$('#tenor').val(EMPTY_STRING);
	$('#tenor_error').html(EMPTY_STRING);
	$('#cancelPeriod').val(EMPTY_STRING);
	$('#cancelPeriod_error').html(EMPTY_STRING);
	$('#repayFreq').val(EMPTY_STRING);
	$('#repayFreq_error').html(EMPTY_STRING);
	$('#repayFreq').prop('selectedIndex', 0);
	$('#advArrers').val(EMPTY_STRING);
	$('#advArrers_error').html(EMPTY_STRING);
	$('#advArrers').prop('selectedIndex', 0);
	$('#firstPaymet').val(EMPTY_STRING);
	$('#firstPaymet_error').html(EMPTY_STRING);
	$('#intFromDate').val(EMPTY_STRING);
	$('#intFromDate_error').html(EMPTY_STRING);
	$('#intUptoDate').val(EMPTY_STRING);
	$('#intUptoDate_error').html(EMPTY_STRING);
	$('#finalFromDate').val(EMPTY_STRING);
	$('#finalFromDate_error').html(EMPTY_STRING);
	$('#finalUptoDate').val(EMPTY_STRING);
	$('#finalUptoDate_error').html(EMPTY_STRING);
	$('#rentDate').val(EMPTY_STRING);
	$('#rentDate_error').html(EMPTY_STRING);
	$('#lastDate').val(EMPTY_STRING);
	$('#lastDate_error').html(EMPTY_STRING);

	// TAB 4

	$('#leaseRentOption').val(EMPTY_STRING);
	$('#leaseRentOption_error').html(EMPTY_STRING);
	$('#leaseRentOption').prop('selectedIndex', 0);
	$('#commencementValue').val(EMPTY_STRING);
	$('#commencementValueFormat').val(EMPTY_STRING);
	$('#commencementValue_error').html(EMPTY_STRING);
	$('#assureRVPer').val(EMPTY_STRING);
	$('#assureRVPer_error').html(EMPTY_STRING);
	$('#assureRVAmount').val(EMPTY_STRING);
	$('#assureRVAmountFormat').val(EMPTY_STRING);
	$('#assureRVAmount_error').html(EMPTY_STRING);
	$('#assureRVType_error').html(EMPTY_STRING);
	// $('#assureRVTypeValue').val(EMPTY_STRING);
	// $('#assureRVType').val(EMPTY_STRING);
	$('#grossAssetCost').val(EMPTY_STRING);
	$('#grossAssetCostFormat').val(EMPTY_STRING);
	$('#grossAssetCost_error').html(EMPTY_STRING);
	$('#intBrokenRent').val(EMPTY_STRING);
	$('#intBrokenRentFormat').val(EMPTY_STRING);
	$('#intBrokenRent_error').html(EMPTY_STRING);
	$('#intBrokenExec').val(EMPTY_STRING);
	$('#intBrokenExecFormat').val(EMPTY_STRING);
	$('#intBrokenExec_error').html(EMPTY_STRING);
	$('#finalBrokenRent').val(EMPTY_STRING);
	$('#finalBrokenRentFormat').val(EMPTY_STRING);
	$('#finalBrokenRent_error').html(EMPTY_STRING);
	$('#finaBrokenExec').val(EMPTY_STRING);
	$('#finaBrokenExecFormat').val(EMPTY_STRING);
	$('#finaBrokenExec_error').html(EMPTY_STRING);
	$('#dealType').val(EMPTY_STRING);
	$('#dealType_error').html(EMPTY_STRING);
	$('#dealType').prop('selectedIndex', 0);
	rentalCharges_clearGridFields();

	// TAB 6
	$('#processCode').val(EMPTY_STRING);
	$('#processCode_desc').html(EMPTY_STRING);
	$('#processCode_error').html(EMPTY_STRING);
	$('#invTempCode').val(EMPTY_STRING);
	$('#invTempCode_desc').html(EMPTY_STRING);
	$('#invTempCode_error').html(EMPTY_STRING);
	$('#famDesc').val(EMPTY_STRING);
	$('#famDesc_error').html(EMPTY_STRING);
	setCheckbox('iAgree', NO);
	$('#invCycleNo').val(EMPTY_STRING);
	$('#invCycleNo_error').html(EMPTY_STRING);
	$('#initCycleNo').val(EMPTY_STRING);
	$('#initCycleNo_error').html(EMPTY_STRING);
	$('#finalCycleNo').val(EMPTY_STRING);
	$('#finalCycleNo_error').html(EMPTY_STRING);
	$('#hierarCode').val(EMPTY_STRING);
	$('#hierarCode_error').html(EMPTY_STRING);
	$('#hierarCode_desc').html(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
	hide('paymentDetails');
	$('#repaymentMode').prop('selectedIndex', 2);
	$('#paymentUptoTenor').val(EMPTY_STRING);
	clearError('paymentErrors_error');
	clearRepaymentGrid();
	financialAmount();
	financialClearTotalValues();
	leaseNoOfAssets_Grid.clearAll();
	leaseAssetComponent_Grid.clearAll();
	leaseRentalCharges_Grid.clearAll();
	leaseApplicableTax_Grid.clearAll();
	financialScheduleNonOLGrid.clearAll();
	leasePaymentDetails_Grid.clearAll();
	financialScheduleOLGrid.clearAll();
	hide('leaseFinanceSchedule');
	hide('leaseFinanceRentalSchedule');
	hide('editFinancialAmount');
}

function clearTaxDetails() {
	$('#startTaxDate').val(EMPTY_STRING);
	$('#endTaxDate').val(EMPTY_STRING);
	$('#monthSl').val(EMPTY_STRING);
	$('#stateCode').val(EMPTY_STRING);
	$('#stateCode_desc').html(EMPTY_STRING);
	$('#interestTaxAmt').val(EMPTY_STRING);
	$('#principalTaxAmt').val(EMPTY_STRING);
	$('#execTaxAmt').val(EMPTY_STRING);
	$('#rentalTaxAmt').val(EMPTY_STRING);
	$('#interestTaxAmtFormat').val(EMPTY_STRING);
	$('#principalTaxAmtFormat').val(EMPTY_STRING);
	$('#execTaxAmtFormat').val(EMPTY_STRING);
	$('#rentalTaxAmtFormat').val(EMPTY_STRING);
	hide('prinsInterestWise');
	hide('taxDetails');
	hide('showFinancialGrid');
	hide('closeFinGrid');
	hide('agreeAndContinue');
	hide('copyText');
	leaseTaxWiseDetails_Grid.clearAll();
}
function clearRepaymentGrid() {
	clearError('paymentErrors_error');
	$('#repaymentMode_error').html(EMPTY_STRING);
	$('#paymentUptoTenor_error').html(EMPTY_STRING);
	$('#ecsBank').val(EMPTY_STRING);
	$('#ecsBank_error').html(EMPTY_STRING);
	$('#ecsBank_desc').html(EMPTY_STRING);
	$('#ecsBranch').val(EMPTY_STRING);
	$('#ecsBranch_error').html(EMPTY_STRING);
	$('#ecsBranch_desc').html(EMPTY_STRING);
	$('#ecsIFSC').val(EMPTY_STRING);
	$('#ecsIFSC_error').html(EMPTY_STRING);
	$('#ecsIFSC_desc').html(EMPTY_STRING);
	$('#cusAccNumber').val(EMPTY_STRING);
	$('#cusAccNumber_error').html(EMPTY_STRING);
	$('#peakAmount').val(EMPTY_STRING);
	$('#peakAmount_error').html(EMPTY_STRING);
	$('#peakAmountFormat').val(EMPTY_STRING);
	$('#ecsRefNo').val(EMPTY_STRING);
	$('#ecsRefNo_error').html(EMPTY_STRING);
	$('#ecsRefDate').val(EMPTY_STRING);
	$('#ecsRefDate_error').html(EMPTY_STRING);
	$('#ecsIFSC').prop('readonly', false);
	$('#ecsBank').prop('readonly', false);
	$('#ecsIFSC_pic').prop('disabled', false);
	$('#ecsBranch').prop('readonly', false);
	$('#cusAccNumber').prop('readonly', false);
	$('#peakAmountFormat').prop('readonly', false);
	$('#ecsRefNo').prop('readonly', false);
	$('#ecsRefDate').prop('readonly', false);
	$('#ecsRefDate_pic').prop('disabled', false);
}

function clearCurrencyFields() {
	$('#commencementValue_curr').val(EMPTY_STRING);
	$('#grossAssetCost_curr').val(EMPTY_STRING);
	$('#assureRVAmount_curr').val(EMPTY_STRING);
	$('#intBrokenRent_curr').val(EMPTY_STRING);
	$('#intBrokenExec_curr').val(EMPTY_STRING);
	$('#finalBrokenRent_curr').val(EMPTY_STRING);
	$('#finaBrokenExec_curr').val(EMPTY_STRING);
	$('#peakAmount_curr').val(EMPTY_STRING);
	$('#rental_curr').val(EMPTY_STRING);
	$('#execChags_curr').val(EMPTY_STRING);
	$('#interestAmt_curr').val(EMPTY_STRING);
	$('#principalAmt_curr').val(EMPTY_STRING);
	$('#totalRentAmt_curr').val(EMPTY_STRING);
	$('#interestTaxAmt_curr').val(EMPTY_STRING);
	$('#principalTaxAmt_curr').val(EMPTY_STRING);
	$('#execTaxAmt_curr').val(EMPTY_STRING);
	$('#rentalTaxAmt_curr').val(EMPTY_STRING);
}

function doclearfields(id) {
	switch (id) {
	case 'custLeaseCode':
		if (isEmpty($('#custLeaseCode').val())) {
			$('#custLeaseCode_error').html(EMPTY_STRING);
			clearFields();
		}
		break;
	case 'agreementNo':
		if (isEmpty($('#agreementNo').val())) {
			$('#agreementNo_error').html(EMPTY_STRING);
			$('#agreementNo_desc').html(EMPTY_STRING);
			$('#agreementDate').val(EMPTY_STRING);
			$('#scheduleID').val(EMPTY_STRING);
			$('#scheduleID_error').html(EMPTY_STRING);
			clearNonPKFields();
		}
		break;
	case 'scheduleID':
		if (isEmpty($('#scheduleID').val())) {
			$('#scheduleID_error').html(EMPTY_STRING);
			$('#scheduleID_desc').html(EMPTY_STRING);
			clearNonPKFields();
		}
		break;
	case 'billBranch':
		if (isEmpty($('#billBranch').val())) {
			$('#billBranch_error').html(EMPTY_STRING);
			$('#billBranch_desc').html(EMPTY_STRING);
			$('#billState').val(EMPTY_STRING);
			$('#billState_desc').html(EMPTY_STRING);
			$('#invType').val(EMPTY_STRING);
		}
		break;
	}
}

function loadData() {
	$('#custLeaseCode').val(validator.getValue('LESSEE_CODE'));
	$('#agreementNo').val(validator.getValue('AGREEMENT_NO'));
	$('#scheduleID').val(validator.getValue('SCHEDULE_ID'));
	$('#custID').val(validator.getValue('CUSTOMER_ID'));
	$('#custName').val(validator.getValue('F1_CUSTOMER_NAME'));

	$('#preLeaseRefDate').val(validator.getValue('CONTRACT_DATE'));
	$('#preLeaseRefSerial').val(validator.getValue('CONTRACT_SL'));
	// TAB 1
	$('#createDate').val(validator.getValue('DATE_OF_CREATION'));
	$('#leaseComDate').val(validator.getValue('DATE_OF_COMMENCEMENT'));
	$('#leaseProduct').val(validator.getValue('LEASE_PRODUCT_CODE'));
	$('#leaseProduct_desc').html(validator.getValue('F2_DESCRIPTION'));
	$('#leaseProductType').val(validator.getValue('F2_LEASE_TYPE'));
	$('#assetCurrency').val(validator.getValue('ASSET_CCY'));
	$('#assetCurrency_desc').html(validator.getValue('F3_DESCRIPTION'));
	$('#billAddress').val(validator.getValue('BILLING_ADDR_SL'));
	$('#shipAddress').val(validator.getValue('SHIP_ADDR_SL'));
	$('#billAddressState').html(DISPLAY_STATE + validator.getValue("F9_STATE_CODE"));
	$('#shipAddressState').html(DISPLAY_STATE + validator.getValue("F10_STATE_CODE"));

	$('#billBranch').val(validator.getValue('LESSOR_BRANCH_CODE'));
	$('#billBranch_desc').html(validator.getValue('F4_BRANCH_NAME'));
	$('#billState').val(validator.getValue('LESSOR_STATE_CODE'));
	$('#billState_desc').html(validator.getValue('F5_DESCRIPTION'));
	$('#invType').val(validator.getValue('F5_INVOICE_TYPE'));

	$('#taxScheduleCode').val(validator.getValue('TAX_SCHEDULE_CODE'));

	// currency load
	var curCode = $('#assetCurrency').val();
	$('#commencementValue_curr').val(curCode);
	$('#grossAssetCost_curr').val(curCode);
	$('#assureRVAmount_curr').val(curCode);
	$('#intBrokenRent_curr').val(curCode);
	$('#intBrokenExec_curr').val(curCode);
	$('#finalBrokenRent_curr').val(curCode);
	$('#finaBrokenExec_curr').val(curCode);
	$('#peakAmount_curr').val(curCode);
	$('#rental_curr').val(curCode);
	$('#execChags_curr').val(curCode);
	$('#principalAmt_curr').val(curCode);
	$('#interestAmt_curr').val(curCode);
	$('#totalRentAmt_curr').val(curCode);
	$('#interestTaxAmt_curr').val(curCode);
	$('#principalTaxAmt_curr').val(curCode);
	$('#execTaxAmt_curr').val(curCode);
	$('#rentalTaxAmt_curr').val(curCode);
	$('#assureRVAmount_curr').val(curCode);

	// TAB 2
	$('#taxScheduleCode').val(validator.getValue('TAX_SCHEDULE_CODE'));
	$('#noOfAssets').val(validator.getValue('NOF_ASSETS'));

	// TAB 3
	$('#tenor').val(validator.getValue('TENOR'));
	$('#cancelPeriod').val(validator.getValue('NON_CANCEL_PERIOD'));
	$('#repayFreq').val(validator.getValue('REPAY_FREQ'));
	$('#advArrers').val(validator.getValue('ADVANCE_ARREARS_FLAG'));
	$('#firstPaymet').val(validator.getValue('FIRST_REPAY_PERIOD_START_DATE'));
	$('#intFromDate').val(validator.getValue('INIT_INTERIM_PERIOD_FROM'));
	$('#intUptoDate').val(validator.getValue('INIT_INTERIM_PERIOD_UPTO'));
	$('#finalFromDate').val(validator.getValue('FINAL_INTERIM_PERIOD_FROM'));
	$('#finalUptoDate').val(validator.getValue('FINAL_INTERIM_PERIOD_UPTO'));
	$('#rentDate').val(validator.getValue('BILLING_FIRST_RENTAL_DATE'));
	$('#lastDate').val(validator.getValue('BILLING_LAST_RENTAL_DATE'));

	// TAB 4
	var grossAssetVal = validator.getValue('GROSS_ASSET_COST');
	var commencementVal = validator.getValue('COMMENCEMENT_VALUE');
	var initRentVal = validator.getValue('INIT_BROKEN_PERIOD_RENT');
	var initExexVal = validator.getValue('INIT_BROKEN_PERIOD_EXEC_CHGS');
	var finalRentVal = validator.getValue('FINAL_BROKEN_PERIOD_RENT');
	var finalExecVal = validator.getValue('FINAL_BROKEN_PERIOD_EXEC_CHGS');
	var rvAmount = validator.getValue('RV_AMOUNT');

	$('#leaseRentOption').val(validator.getValue('LEASE_RENTAL_PERIOD_OPTION'));
	$('#assureRVPer').val(validator.getValue('RV_PER'));
	$('#assureRVType').val(validator.getValue('RV_TYPE'));
	$('#assureRVTypeValue').val(validator.getValue('RV_TYPE'));

	$('#dealType').val(validator.getValue('DEAL_TYPE'));

	$('#internalRateReturn').val(validator.getValue('IRR_VALUE'));
	if ($('#leaseProductType').val() == 'O')
		$('#internalRateReturn').prop('readonly', true);
	else
		$('#internalRateReturn').prop('readonly', false);

	// TAB 6
	$('#grossNetTDS').val(validator.getValue('TDS_GROSS_NET_INDICATOR'));
	$('#processCode').val(validator.getValue('BANK_PROCESS_CODE'));
	$('#processCode_desc').html(validator.getValue('F6_NAME'));
	$('#invTempCode').val(validator.getValue('INVOICE_TEMPLATE'));
	$('#invTempCode_desc').html(validator.getValue('F7_DESCRIPTION'));
	$('#famDesc').val(validator.getValue('FAM_CODE'));
	$('#hierarCode').val(validator.getValue('LAM_HIER_CODE'));
	$('#hierarCode_desc').html(validator.getValue('F8_DESCRIPTION'));
	$('#invCycleNo').val(validator.getValue('INV_CYCLE_NUMBER'));
	$('#initCycleNo').val(validator.getValue('INIT_INTERIM_CYCLE_NUMBER'));
	$('#finalCycleNo').val(validator.getValue('FINAL_INTERIM_CYCLE_NUMBER'));
	$('#remarks').val(validator.getValue('REMARKS'));

	// amount Formated
	$('#grossAssetCostFormat').val(formatAmount(grossAssetVal, $('#grossAssetCost_curr').val()));
	$('#grossAssetCost').val(unformatAmount($('#grossAssetCostFormat').val()));
	$('#commencementValueFormat').val(formatAmount(commencementVal, $('#commencementValue_curr').val()));
	$('#commencementValue').val(unformatAmount($('#commencementValueFormat').val()));
	if (initRentVal != null && initRentVal != EMPTY_STRING) {
		$('#intBrokenRentFormat').val(formatAmount(initRentVal, $('#intBrokenRent_curr').val()));
		$('#intBrokenRent').val(unformatAmount($('#intBrokenRentFormat').val()));
	}
	if (initExexVal != null && initExexVal != EMPTY_STRING) {
		$('#intBrokenExecFormat').val(formatAmount(initExexVal, $('#intBrokenExec_curr').val()));
		$('#intBrokenExec').val(unformatAmount($('#intBrokenExecFormat').val()));
	}
	if (finalRentVal != null && finalRentVal != EMPTY_STRING) {
		$('#finalBrokenRentFormat').val(formatAmount(finalRentVal, $('#finalBrokenRent_curr').val()));
		$('#finalBrokenRent').val(unformatAmount($('#finalBrokenRentFormat').val()));
	}
	if (finalExecVal != null && finalExecVal != EMPTY_STRING) {
		$('#finaBrokenExecFormat').val(formatAmount(finalExecVal, $('#finaBrokenExec_curr').val()));
		$('#finaBrokenExec').val(unformatAmount($('#finaBrokenExecFormat').val()));
	}
	if (rvAmount != null && rvAmount != EMPTY_STRING) {
		$('#assureRVAmountFormat').val(formatAmount(rvAmount, $('#assureRVAmount_curr').val()));
		$('#assureRVAmount').val(unformatAmount($('#assureRVAmountFormat').val()));
	}
	if (validateProcessStart()) {
		getSchedule();
		loadGrid();
		// loadGridFinance();
		// loadTaxDetails();
	}
	resetLoading();
	lease._setTabActive('lease_0', true);
}

function validateProcessStart() {
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('ENTITY_CODE', getEntityCode());
	validator.setValue('LESSEE_CODE', $('#custLeaseCode').val());
	validator.setValue('AGREEMENT_NO', $('#agreementNo').val());
	validator.setValue('SCHEDULE_ID', $('#scheduleID').val());
	validator.setValue(ACTION, USAGE);
	validator.setClass('patterns.config.web.forms.lss.eleasebean');
	validator.setMethod('validateFinancialPeriodProcessStart');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('scheduleID_error', validator.getValue(ERROR));
		clearNonPKFields();
		return false;
	}
	return true;
}

function getSchedule() {
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('ENTITY_CODE', getEntityCode());
	validator.setValue('LESSEE_CODE', $('#custLeaseCode').val());
	validator.setValue('AGREEMENT_NO', $('#agreementNo').val());
	validator.setValue('SCHEDULE_ID', $('#scheduleID').val());
	validator.setValue('ADDR_SL', $('#shipAddress').val());
	validator.setValue('CUSTOMER_ID', $('#custID').val());
	validator.setValue(ACTION, USAGE);
	validator.setClass('patterns.config.web.forms.lss.eleasebean');
	validator.setMethod('getScheduleCode');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		alert(validator.getValue(ERROR));
		return false;
	} else {
		$('#shippingState').val(validator.getValue("SHIP_STATE_CODE"));
	}
	return true;
}

function loadGridFinance() {
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('ENTITY_CODE', getEntityCode());
	validator.setValue('LESSEE_CODE', $('#custLeaseCode').val());
	validator.setValue('AGREEMENT_NO', $('#agreementNo').val());
	validator.setValue('SCHEDULE_ID', $('#scheduleID').val());
	validator.setValue('LEASE_PRODUCT_CODE', $('#leaseProduct').val());
	validator.setValue('CUSTOMER_ID', $('#custID').val());
	validator.setValue('STATE_CODE', $('#billState').val());
	validator.setValue('SHIP_STATE_CODE', $('#shippingState').val());
	validator.setValue('TAX_SCHEDULE_CODE', $('#taxScheduleCode').val());
	validator.setValue('TAX_CCY', $('#assetCurrency').val());
	validator.setValue('TAX_ON_DATE', getCBD());
	validator.setValue('EDIT_REQ', $('#leaseProductType').val());
	validator.setValue('MAIN_SL', $('#spSerial').val());
	validator.setValue(ACTION, USAGE);
	validator.setClass('patterns.config.web.forms.lss.eleasebean');
	validator.setMethod('loadFinanceWiseGrid');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		alert(validator.getValue(ERROR));
		return false;
	} else {
		$('#noOfslab').html(EMPTY_STRING);
		$('#valueNegative').html(EMPTY_STRING);
		$('#noOfslab').append("<div>No. of Slabs    :<span id ='totalValues' x-large;'></span></div>");
		if ($('#leaseProductType').val() == 'O') {
			hide('leaseFinanceSchedule');
			show('leaseFinanceRentalSchedule');
			financialScheduleOLGrid.loadXMLString(validator.getValue("LOAD_FINANCIAL_XML"));
			$("#totalValues").html(financialScheduleOLGrid.getRowsNum());
			financialScheduleOLGrid.setColumnHidden(1, true);
			financialScheduleOLGrid.setColumnHidden(4, true);
			financialScheduleOLGrid.setColumnHidden(5, true);
			$('#executoryAmt1').html(validator.getValue("TOTAL_EXEC_TAX"));
			$('#rentalAmt1').html(validator.getValue("TOTAL_RENTAL_TAX"));
			$('#stateAmtFormat1').html(validator.getValue("TOTAL_STATE_TAX_FORMAT"));
			$('#centralAmtFormat1').html(validator.getValue("TOTAL_CENTRAL_TAX_FORMAT"));
			$('#serviceAmtFormat1').html(validator.getValue("TOTAL_SERVICE_TAX_FORMAT"));
			$('#cessesAmtFormat1').html(validator.getValue("TOTAL_CESS_TAX_FORMAT"));
			$('#totalAmtFormat1').html(validator.getValue("TOTAL_VALUE_FORMAT"));

			$('#stateAmt1').html(validator.getValue("TOTAL_STATE_TAX"));
			$('#centralAmt1').html(validator.getValue("TOTAL_CENTRAL_TAX"));
			$('#serviceAmt1').html(validator.getValue("TOTAL_SERVICE_TAX"));
			$('#cessesAmt1').html(validator.getValue("TOTAL_CESS_TAX"));
			$('#totalAmt1').html(validator.getValue("TOTAL_VALUE"));
		} else {
			show('leaseFinanceSchedule');
			hide('leaseFinanceRentalSchedule');
			financialScheduleNonOLGrid.loadXMLString(validator.getValue("LOAD_FINANCIAL_XML"));
			$("#totalValues").html(financialScheduleNonOLGrid.getRowsNum());
			$('#executoryAmt').html(validator.getValue("TOTAL_EXEC_TAX"));
			$('#rentalAmt').html(validator.getValue("TOTAL_RENTAL_TAX"));
			$('#stateAmtFormat').html(validator.getValue("TOTAL_STATE_TAX_FORMAT"));
			$('#centralAmtFormat').html(validator.getValue("TOTAL_CENTRAL_TAX_FORMAT"));
			$('#serviceAmtFormat').html(validator.getValue("TOTAL_SERVICE_TAX_FORMAT"));
			$('#cessesAmtFormat').html(validator.getValue("TOTAL_CESS_TAX_FORMAT"));
			$('#totalAmtFormat').html(validator.getValue("TOTAL_VALUE_FORMAT"));

			$('#stateAmt').html(validator.getValue("TOTAL_STATE_TAX"));
			$('#centralAmt').html(validator.getValue("TOTAL_CENTRAL_TAX"));
			$('#serviceAmt').html(validator.getValue("TOTAL_SERVICE_TAX"));
			$('#cessesAmt').html(validator.getValue("TOTAL_CESS_TAX"));
			$('#totalAmt').html(validator.getValue("TOTAL_VALUE"));
		}
	}
}

function add() {
	$('#custLeaseCode').focus();
	$('#createDate').val(getCBD());
	$('#leaseComDate').val(getCBD());
	lease._setTabActive('lease_0', true);
	$('#lease').height('275px');
	$('#uptoToner').prop('readonly', true);
	$('#billBranch_desc').html(getBranchName());
}

function modify() {
	$('#custLeaseCode').focus();
	lease._setTabActive('lease_0', true);
	$('#lease').height('275px');
	$('#uptoToner').prop('readonly', true);
}

function doHelp(id) {
	switch (id) {
	case 'custLeaseCode':
		help('COMMON', 'HLP_DEBTOR_CODE', $('#custLeaseCode').val(), EMPTY_STRING, $('#custLeaseCode'), function() {
		});
		break;
	case 'agreementNo':
		if (!isEmpty($('#custID').val())) {
			help('ELEASE', 'HLP_CUST_AGGREEMENT', $('#agreementNo').val(), $('#custID').val(), $('#agreementNo'), function() {
			});
		} else {
			setError('agreementNo_error', CUSTOMER_ID_REQUIRED);
			return false;
		}
		break;
	case 'scheduleID':
		if (!isEmpty($('#custLeaseCode').val()) && !isEmpty($('#agreementNo').val())) {
			help('COMMON', 'HLP_LEASE_SCH_ID', $('#scheduleID').val(), $('#custLeaseCode').val() + PK_SEPERATOR + $('#agreementNo').val(), $('#scheduleID'));
		} else {
			setError('scheduleID_error', CUST_ID_AGREE_NO_REQUIRED);
			return false;
		}
		break;
	case 'preLeaseRef':
	case 'preLeaseRefDate':
	case 'preLeaseRefSerial':
		if (!isEmpty($('#custLeaseCode').val()) && !isEmpty($('#custID').val())) {
			if (!isEmpty($('#preLeaseRefDate').val())) {
				help('ELEASE', 'HLP_PRE_LEASE_DATE', $('#preLeaseRefSerial').val(), $('#preLeaseRefDate').val() + PK_SEPERATOR + $('#custID').val() + PK_SEPERATOR + $('#custLeaseCode').val() + PK_SEPERATOR + $('#leaseProduct').val(), $('#preLeaseRefSerial'), null, function(gridObj, rowID) {
					$('#preLeaseRefSerial').val(gridObj.cells(rowID, 1).getValue());
				});
			} else {
				help('COMMON', 'HLP_PRE_LEASE_DATE_SL', $('#preLeaseRefSerial').val(), $('#custID').val() + PK_SEPERATOR + $('#custLeaseCode').val() + PK_SEPERATOR + $('#leaseProduct').val(), $('#preLeaseRefSerial'), null, function(gridObj, rowID) {
					$('#preLeaseRefDate').val(gridObj.cells(rowID, 0).getValue());
					$('#preLeaseRefSerial').val(gridObj.cells(rowID, 1).getValue());
				});
			}
		} else {
			setError('preLeaseRef_error', CUSTOMER_ID_REQUIRED);
			return false;
		}
		break;
	case 'billAddress':
		if (!isEmpty($('#custID').val())) {
			help('ELEASE', 'HLP_CUST_ADDRESS', $('#billAddress').val(), $('#custID').val(), $('#billAddress'));
		} else {
			setError('billAddress_error', CUSTOMER_ID_REQUIRED);
			return false;
		}
		break;
	case 'shipAddress':
		if (!isEmpty($('#custID').val())) {
			help('ELEASE', 'HLP_CUST_ADDRESS', $('#shipAddress').val(), $('#custID').val(), $('#shipAddress'));
		} else {
			setError('shipAddress_error', CUSTOMER_ID_REQUIRED);
			return false;
		}
		break;
	case 'billBranch':
		help('COMMON', 'HLP_BRANCH_CODE', $('#billBranch').val(), EMPTY_STRING, $('#billBranch'));
		break;
	case 'assetCategory':
		help('COMMON', 'HLP_ASSET_TYPE', $('#assetCategory').val(), EMPTY_STRING, $('#assetCategory'));
		break;
	case 'ecsIFSC':
		help('COMMON', 'HLP_IFSC_CODE', $('#ecsIFSC').val(), EMPTY_STRING, $('#ecsIFSC'));
		break;
	case 'processCode':
		help('COMMON', 'HLP_BANK_PROC_CODE', $('#processCode').val(), EMPTY_STRING, $('#processCode'));
		break;
	case 'invTempCode':
		help('COMMON', 'HLP_INV_TEMPLATE_CODE', $('#invTempCode').val(), EMPTY_STRING, $('#invTempCode'));
		break;
	case 'hierarCode':
		help('COMMON', 'HLP_LAM_HIER_CODE', $('#hierarCode').val(), EMPTY_STRING, $('#hierarCode'));
		break;
	case 'invCycleNo':
		help('COMMON', 'HLP_INVOICE_CYCLE_NUMBER', $('#invCycleNo').val(), EMPTY_STRING, $('#invCycleNo'));
		break;
	case 'initCycleNo':
		help('COMMON', 'HLP_INVOICE_CYCLE_NUMBER', $('#initCycleNo').val(), EMPTY_STRING, $('#initCycleNo'));
		break;
	case 'finalCycleNo':
		help('COMMON', 'HLP_INVOICE_CYCLE_NUMBER', $('#finalCycleNo').val(), EMPTY_STRING, $('#finalCycleNo'));
		break;
	}
}

function validate(id) {
	valMode = true;
	switch (id) {
	case 'custLeaseCode':
		custLeaseCode_val();
		break;
	case 'agreementNo':
		agreementNo_val();
		break;
	case 'scheduleID':
		scheduleID_val(true);
		break;
	case 'preLeaseRefDate':
		preLeaseRefDate_val();
		break;
	case 'preLeaseRefSerial':
		preLeaseRefSerial_val();
		break;
	case 'leaseComDate':
		leaseComDate_val();
		break;
	case 'billAddress':
		billAddress_val();
		break;
	case 'shipAddress':
		shipAddress_val();
		break;
	case 'billBranch':
		billBranch_val();
		break;
	// TAB 2
	case 'noOfAssets':
		noOfAssets_val(true);
		break;

	// TAB 3
	case 'tenor':
		tenor_val();
		break;
	case 'cancelPeriod':
		cancelPeriod_val();
		break;
	case 'repayFreq':
		repayFreq_val(true);
		break;
	case 'advArrers':
		advArrers_val();
		break;
	case 'firstPaymet':
		firstPaymet_val(true);
		break;
	case 'intFromDate':
		intFromDate_val();
		break;
	case 'intUptoDate':
		intUptoDate_val();
		break;
	case 'finalFromDate':
		finalFromDate_val();
		break;
	case 'finalUptoDate':
		finalUptoDate_val();
		break;

	// TAB 5
	case 'leaseRentOption':
		leaseRentOption_val();
		break;
	case 'commencementValueFormat':
		commencementValueFormat_val(true);
		break;
	case 'assureRVPer':
		assureRVPer_val();
		break;
	case 'assureRVAmountFormat':
		assureRVAmountFormat_val();
		break;
	case 'grossAssetCostFormat':
		grossAssetCostFormat_val();
		break;
	case 'internalRateReturn':
		internalRateReturn_val();
		break;
	case 'dealType':
		dealType_val();
		break;
	case 'uptoToner':
		uptoToner_val();
		break;
	case 'rentalFormat':
		rentalFormat_val();
		break;
	case 'leaseRentRate':
		leaseRentRate_val();
		break;
	case 'execChagsFormat':
		execChagsFormat_val();
		break;
	case 'intBrokenRentFormat':
		intBrokenRentFormat_val();
		break;
	case 'intBrokenExecFormat':
		intBrokenExecFormat_val();
		break;
	case 'finalBrokenRentFormat':
		finalBrokenRentFormat_val();
		break;
	case 'finaBrokenExecFormat':
		finaBrokenExecFormat_val();
		break;

	// TAB 6
	case 'grossNetTDS':
		grossNetTDS_val();
		break;
	case 'repaymentMode':
		repaymentMode_val(true);
		break;
	case 'paymentUptoTenor':
		paymentUptoTenor_val(true);
		break;
	case 'ecsIFSC':
		ecsIFSC_val();
		break;
	case 'ecsBank':
		ecsBank_val();
		break;
	case 'ecsBranch':
		ecsBranch_val();
		break;
	case 'cusAccNumber':
		cusAccNumber_val();
		break;
	case 'peakAmountFormat':
		peakAmountFormat_val();
		break;
	case 'ecsRefNo':
		ecsRefNo_val();
		break;
	case 'ecsRefDate':
		ecsRefDate_val();
		break;
	case 'processCode':
		processCode_val();
		break;
	case 'invTempCode':
		invTempCode_val();
		break;
	case 'famDesc':
		famDesc_val();
		break;
	case 'hierarCode':
		hierarCode_val();
		break;
	case 'invCycleNo':
		invCycleNo_val();
		break;
	case 'initCycleNo':
		initCycleNo_val();
		break;
	case 'finalCycleNo':
		finalCycleNo_val();
		break;
	case 'remarks':
		remarks_val();
		break;

	// POPUP grid validation
	case 'assetCategory':
		assetCategory_val();
		break;
	case 'assetComponent':
		assetComponent_val();
		break;
	case 'componentSerial':
		componentSerial_val();
		break;
	case 'comAssetID':
		comAssetID_val();
		break;
	case 'comAssetDesc':
		comAssetDesc_val();
		break;
	case 'assetModel':
		assetModel_val();
		break;
	case 'assetConfig':
		assetConfig_val();
		break;
	case 'comQty':
		comQty_val();
		break;
	case 'comIndidualReq':
		comIndidualReq_val();
		break;
	case 'principalAmtFormat':
		principalAmtFormat_val(true);
		break;
	case 'interestAmtFormat':
		interestAmtFormat_val();
		break;
	case 'iAgree':
		iAgree_val();
		break;
	}
}

function checkclick(id) {
	switch (id) {
	case 'iAgree':
		iAgree_val();
		break;
	}
}

function backtrack(id) {
	switch (id) {
	case 'custLeaseCode':
		setFocusLast('custLeaseCode');
		break;
	case 'agreementNo':
		setFocusLast('custLeaseCode');
		break;
	case 'scheduleID':
		setFocusLast('agreementNo');
		break;
	case 'preLeaseRefDate':
		setFocusLast('scheduleID');
		break;
	case 'preLeaseRefSerial':
		setFocusLast('preLeaseRefDate');
		break;
	case 'leaseComDate':
		setFocusLast('preLeaseRefSerial');
		break;
	case 'billAddress':
		setFocusLast('leaseComDate');
		break;
	case 'shipAddress':
		setFocusLast('billAddress');
		break;
	case 'billBranch':
		setFocusLast('shipAddress');
		break;
	case 'noOfAssets':
		doTabbarClick('lease_0');
		lease._setTabActive('lease_0', true);
		setFocusLast('billBranch');
		break;
	case 'addAsset':
		setFocusLast('noOfAssets');
		break;

	// TAB 3
	case 'tenor':
		doTabbarClick('lease_1');
		lease._setTabActive('lease_1', true);
		setFocusLast('noOfAssets');
		break;
	case 'cancelPeriod':
		setFocusLast('tenor');
		break;
	case 'repayFreq':
		setFocusLast('cancelPeriod');
		break;
	case 'advArrers':
		setFocusLast('repayFreq');
		break;
	case 'firstPaymet':
		setFocusLast('advArrers');
		break;
	case 'intFromDate':
		setFocusLast('firstPaymet');
		break;
	case 'intUptoDate':
		setFocusLast('intFromDate');
		break;
	case 'finalFromDate':
		setFocusLast('intUptoDate');
		break;
	case 'finalUptoDate':
		setFocusLast('finalFromDate');
		break;
	// TAB 4
	case 'grossAssetCostFormat':
		doTabbarClick('lease_2');
		lease._setTabActive('lease_2', true);
		setFocusLast('finalUptoDate');
		break;
	case 'commencementValueFormat':
		setFocusLast('grossAssetCostFormat');
		break;
	case 'leaseRentOption':
		setFocusLast('commencementValueFormat');
		break;
	case 'assureRVPer':
		setFocusLast('leaseRentOption');
		break;
	case 'assureRVAmountFormat':
		setFocusLast('assureRVPer');
		break;
	case 'internalRateReturn':
		setFocusLast('assureRVAmountFormat');
		break;
	case 'dealType':
		if ($('#internalRateReturn').prop('readonly'))
			setFocusLast('assureRVAmountFormat');
		else
			setFocusLast('internalRateReturn');
		break;
	case 'uptoToner':
		setFocusLast('dealType');
		break;
	case 'leaseRentRate':
		if (!$('#uptoToner').prop('readonly'))
			setFocusLast('uptoToner');
		else
			setFocusLast('dealType');
		break;
	case 'rentalFormat':
		setFocusLast('leaseRentRate');
		break;
	case 'execChagsFormat':
		setFocusLast('rentalFormat');
		break;
	case 'leaseRentalCharges_Grid_add':
		setFocusLast('execChagsFormat');
		break;
	case 'intBrokenRentFormat':
		setFocusLast('dealType');
		break;
	case 'intBrokenExecFormat':
		setFocusLast('intBrokenRentFormat');
		break;
	case 'finalBrokenRentFormat':
		setFocusLast('intBrokenExecFormat');
		break;
	case 'finaBrokenExecFormat':
		setFocusLast('finalBrokenRentFormat');
		break;

	// TAB 6 END
	case 'grossNetTDS':
		doTabbarClick('lease_3');
		lease._setTabActive('lease_3', true);
		setFocusLast('finaBrokenExecFormat');
		break;
	case 'paymentUptoTenor':
		setFocusLast('paymentUptoTenor');
		break;
	case 'repaymentMode':
		setFocusLast('paymentUptoTenor');
		break;
	case 'ecsIFSC':
		setFocusLast('repaymentMode');
		break;
	case 'ecsBank':
		setFocusLast('ecsIFSC');
		break;
	case 'ecsBranch':
		setFocusLast('ecsBank');
		break;
	case 'cusAccNumber':
		setFocusLast('ecsBranch');
		break;
	case 'peakAmountFormat':
		setFocusLast('cusAccNumber');
		break;
	case 'ecsRefNo':
		setFocusLast('peakAmountFormat');
		break;
	case 'ecsRefDate':
		setFocusLast('ecsRefNo');
		break;
	case 'addPayment':
		if ($('#repaymentMode').val() == 'ECS')
			setFocusLast('ecsRefDate');
		else if ($('#repaymentMode').val() == 'AD')
			setFocusLast('cusAccNumber');
		else
			setFocusLast('repaymentMode');
		break;
	case 'processCode':
		setFocusLast('grossNetTDS');
		break;
	case 'invTempCode':
		setFocusLast('processCode');
		break;
	case 'famDesc':
		setFocusLast('invTempCode');
		break;
	case 'hierarCode':
		setFocusLast('famDesc');
		break;
	case 'invCycleNo':
		setFocusLast('hierarCode');
		break;
	case 'initCycleNo':
		setFocusLast('invCycleNo');
		break;
	case 'finalCycleNo':
		setFocusLast('initCycleNo');
		break;
	case 'remarks':
		setFocusLast('finalCycleNo');
		break;

	// POPUP GRID
	case 'assetCategory':
		setFocusLast('assetCategory');
		break;
	case 'assetComponent':
		setFocusLast('assetCategory');
		break;
	case 'comAssetID':
		setFocusLast('assetComponent');
		break;
	case 'comAssetDesc':
		setFocusLast('comAssetID');
		break;
	case 'comQty':
		setFocusLast('comAssetDesc');
		break;
	case 'comIndidualReq':
		setFocusLast('comQty');
		break;
	case 'assetModel':
		setFocusLast('comIndidualReq');
		break;
	case 'assetConfig':
		if (!$('#assetModel').prop('readonly'))
			setFocusLast('assetModel');
		else
			setFocusLast('comIndidualReq');
		break;
	case 'leaseAssetComponent_Grid_add':
		if (!$('#assetConfig').prop('readonly'))
			setFocusLast('assetConfig');
		else if (!$('#assetModel').prop('readonly'))
			setFocusLast('assetModel');
		else
			setFocusLast('comIndidualReq');
		break;
	// TAB 6 POP POP
	case 'principalAmtFormat':
		setFocusLast('principalAmtFormat');
		break;
	case 'interestAmtFormat':
		setFocusLast('principalAmtFormat');
		break;
	case 'finAmount':
		setFocusLast('interestAmtFormat');
		break;

	// POP up backtrack
	case 'finAmountEditPrev':
		setFocusLast('interestAmtFormat');
		break;
	case 'finAmountContinue':
		setFocusLast('interestAmtFormat');
		break;
	case 'closeWin1':
		setFocusLast('interestAmtFormat');
		break;
	}
}

// Header details
function custLeaseCode_val() {
	var value = $('#custLeaseCode').val();
	clearError('custLeaseCode_error');
	lesseeClearFileds();
	if (isEmpty(value)) {
		setError('custLeaseCode_error', MANDATORY);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('ENTITY_CODE', getEntityCode());
		validator.setValue('SUNDRY_DB_AC', value);
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.lss.eleasebean');
		validator.setMethod('validateCustLeaseCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			lesseeClearFileds();
			setError('custLeaseCode_error', validator.getValue(ERROR));
			return false;
		} else {
			$('#custLeaseCode_desc').html(validator.getValue("DESCRIPTION"));
			$('#custID').val(validator.getValue("CUSTOMER_ID"));
			$('#custName').val(validator.getValue("CUSTOMER_NAME"));
			$('#leaseProduct').val(validator.getValue("LEASE_PRODUCT_CODE"));
			$('#leaseProduct_desc').html(validator.getValue("LEASE_DESC"));
			$('#leaseProductType').val(validator.getValue("LEASE_TYPE"));
			$('#assetCurrency').val(validator.getValue("LEASE_ASSET_IN_CCY"));
			$('#assetCurrency_desc').html(validator.getValue("CUR_DESC"));
			curCode = $('#assetCurrency').val();
			$('#commencementValue_curr').val(curCode);
			$('#grossAssetCost_curr').val(curCode);
			$('#intBrokenRent_curr').val(curCode);
			$('#intBrokenExec_curr').val(curCode);
			$('#finalBrokenRent_curr').val(curCode);
			$('#finaBrokenExec_curr').val(curCode);
			$('#peakAmount_curr').val(curCode);
			$('#rental_curr').val(curCode);
			$('#execChags_curr').val(curCode);
			$('#interestAmt_curr').val(curCode);
			$('#principalAmt_curr').val(curCode);
			$('#totalRentAmt_curr').val(curCode);
			$('#interestTaxAmt_curr').val(curCode);
			$('#principalTaxAmt_curr').val(curCode);
			$('#execTaxAmt_curr').val(curCode);
			$('#rentalTaxAmt_curr').val(curCode);
			$('#assureRVAmount_curr').val(curCode);
			var typeValue;
			if ($('#leaseProductType').val() == 'O') {
				$('#internalRateReturn').prop('readonly', true);
				$('#internalRateReturn').val(EMPTY_STRING);
				$('#internalRateReturn_error').html(EMPTY_STRING);
				typeValue = 'A';
				$('#assureRVType').val(typeValue);
				$('#assureRVTypeValue').val(typeValue);
			} else {
				$('#internalRateReturn').prop('readonly', false);
				$('#internalRateReturn_error').html(EMPTY_STRING);
				typeValue = 'T';
				$('#assureRVType').val(typeValue);
				$('#assureRVTypeValue').val(typeValue);
			}
		}
	}
	setFocusLast('agreementNo');
	return true;
}

function lesseeClearFileds() {
	$('#custLeaseCode_desc').html(EMPTY_STRING);
	$('#custID').val(EMPTY_STRING);
	$('#custName').val(EMPTY_STRING);
	$('#leaseProduct').val(EMPTY_STRING);
	$('#leaseProduct_desc').html(EMPTY_STRING);
	$('#assetCurrency').val(EMPTY_STRING);
	$('#assetCurrency_desc').html(EMPTY_STRING);
	$('#internalRateReturn').val(EMPTY_STRING);
	$('#internalRateReturn_error').html(EMPTY_STRING);
}

function agreementNo_val() {
	var value = $('#agreementNo').val();
	clearError('agreementNo_error');
	$('#agreementNo_desc').html(EMPTY_STRING);
	$('#agreementDate').val(EMPTY_STRING);
	if (isEmpty(value)) {
		setError('agreementNo_error', MANDATORY);
		return false;
	}
	if (!isNumeric(value)) {
		setError('agreementNo_error', INVALID_NUMBER);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('ENTITY_CODE', getEntityCode());
		validator.setValue('CUSTOMER_ID', $('#custID').val());
		validator.setValue('AGREEMENT_NO', value);
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.lss.eleasebean');
		validator.setMethod('validateAgreementNo');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			$('#agreementNo_desc').html(EMPTY_STRING);
			$('#agreementDate').val(EMPTY_STRING);
			setError('agreementNo_error', validator.getValue(ERROR));
			return false;
		} else {
			$('#agreementDate').val(validator.getValue("AGREEMENT_DATE"));
		}
	}
	setFocusLast('scheduleID');
	return true;
}

function scheduleID_val(valMode) {
	var value = $('#scheduleID').val();
	clearError('scheduleID_error');
	$('#scheduleID_desc').html(EMPTY_STRING);
	if (isEmpty(value)) {
		setError('scheduleID_error', MANDATORY);
		return false;
	}
	if (!isValidCode(value)) {
		setError('scheduleID_error', INVALID_VALUE);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('ENTITY_CODE', getEntityCode());
		validator.setValue('LESSEE_CODE', $('#custLeaseCode').val());
		validator.setValue('AGREEMENT_NO', $('#agreementNo').val());
		validator.setValue('SCHEDULE_ID', value);
		validator.setValue(ACTION, $('#action').val());
		validator.setValue(RECTIFY, $('#rectify').val());
		validator.setClass('patterns.config.web.forms.lss.eleasebean');
		validator.setMethod('validateScheduleID');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			$('#scheduleID_desc').html(EMPTY_STRING);
			setError('scheduleID_error', validator.getValue(ERROR));
			return false;
		}
	}
	if (valMode) {
		if ($('#action').val() == MODIFY) {
			PK_VALUE = getEntityCode() + PK_SEPERATOR + $('#custLeaseCode').val() + PK_SEPERATOR + $('#agreementNo').val() + PK_SEPERATOR + value;
			if (!loadPKValues(PK_VALUE, 'scheduleID_error')) {
				return false;
			}
		}
	}
	setFocusLast('preLeaseRefDate');
	return true;
}

function preLeaseRefDate_val() {
	var value = $('#preLeaseRefDate').val();
	clearError('preLeaseRef_error');
	if (!isEmpty(value)) {
		if (!isDate(value)) {
			setError('preLeaseRef_error', INVALID_DATE);
			return false;
		}
	}
	if (isEmpty(value)) {
		$('#preLeaseRefSerial').val(EMPTY_STRING);
		clearError('preLeaseRef_error');
		doTabbarClick('lease_0');
		lease._setTabActive('lease_0', true);
		setFocusLast('leaseComDate');
	} else
		setFocusLast('preLeaseRefSerial');
	return true;
}

function preLeaseRefSerial_val() {
	var value = $('#preLeaseRefSerial').val();
	clearError('preLeaseRef_error');
	if (!isEmpty($('#preLeaseRefDate').val())) {
		if (isEmpty(value)) {
			setError('preLeaseRef_error', MANDATORY);
			return false;
		}
	}
	if (!isEmpty(value) && !isEmpty($('#preLeaseRefDate').val())) {
		if (!isDate($('#preLeaseRefDate').val())) {
			setError('preLeaseRef_error', INVALID_DATE);
			return false;
		}
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('ENTITY_CODE', getEntityCode());
		validator.setValue('CONTRACT_DATE', $('#preLeaseRefDate').val());
		validator.setValue('CONTRACT_SL', $('#preLeaseRefSerial').val());
		validator.setValue('LESSEE_CODE', $('#custLeaseCode').val());
		validator.setValue('LEASE_PRODUCT_CODE', $('#leaseProduct').val());
		validator.setValue('CUSTOMER_ID', $('#custID').val());
		validator.setValue('GRID_LOAD_REQ', 1);
		validator.setValue(ACTION, USAGE);
		validator.setValue('ACTIONS', $('#action').val());
		validator.setClass('patterns.config.web.forms.lss.eleasebean');
		validator.setMethod('validatePreLeaseRefSerial');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('preLeaseRef_error', validator.getValue(ERROR));
			$('#billBranch').val(EMPTY_STRING);
			$('#taxScheduleCode').val(EMPTY_STRING);
			leaseNoOfAssets_Grid.clearAll();
			$('#noOfAssets').val(EMPTY_STRING);
			return false;
		} else {
			if (leaseNoOfAssets_Grid.getRowsNum() != 0) {
				if (!confirm(CLEAR_NO_OF_GRID)) {
					doTabbarClick('lease_0');
					lease._setTabActive('lease_0', true);
					setFocusLast('leaseComDate');
					return true;
				}
			}
			$('#billBranch').val(EMPTY_STRING);
			$('#billBranch_desc').html(EMPTY_STRING);
			$('#taxScheduleCode').val(EMPTY_STRING);
			leaseNoOfAssets_Grid.clearAll();
			$('#noOfAssets').val(EMPTY_STRING);
			$('#billBranch').val(validator.getValue("BRANCH_CODE"));
			$('#billBranch_desc').html(validator.getValue("BRANCH_DESC"));
			if (validator.getValue("TAX_SCHEDULE_CODE") != EMPTY_STRING)
				$('#taxScheduleCode').val(validator.getValue("TAX_SCHEDULE_CODE"));
			if (validator.getValue("LOAD_COMPONENT_XML") != EMPTY_STRING)
				leaseNoOfAssets_Grid.loadXMLString(validator.getValue("LOAD_COMPONENT_XML"));
			$('#noOfAssets').val(leaseNoOfAssets_Grid.getRowsNum());
		}
	}
	doTabbarClick('lease_0');
	lease._setTabActive('lease_0', true);
	setFocusLast('leaseComDate');
	return true;
}

// Header details Ends

// Address Detail Tab Begins
function leaseComDate_val() {
	var value = $('#leaseComDate').val();
	clearError('leaseComDate_error');
	if (isEmpty(value)) {
		$('#leaseComDate').val(getCBD());
		value = $('#leaseComDate').val();
	}
	agreementNo_val();
	if (isEmpty($('#agreementDate').val())) {
		setError('leaseComDate_error', AGREEMENT_ID_REQUIRED);
		return false;
	}
	if (!isDate(value)) {
		setError('leaseComDate_error', INVALID_DATE);
		return false;
	}
	if (isDateLesser(value, $('#agreementDate').val())) {
		setError('leaseComDate_error', AGGREMENT_DATE_LESSER);
		return false;
	}
	if (isDateGreater(value, getCBD())) {
		setError('leaseComDate_error', DATE_LECBD);
		return false;
	}
	setFocusLast('billAddress');
	return true;
}

function billAddress_val() {
	var value = $('#billAddress').val();
	clearError('billAddress_error');
	$('#billAddressState').html(EMPTY_STRING);
	if (isEmpty(value)) {
		setError('billAddress_error', MANDATORY);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('ENTITY_CODE', getEntityCode());
		validator.setValue('CUSTOMER_ID', $('#custID').val());
		validator.setValue('ADDR_SL', value);
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.lss.eleasebean');
		validator.setMethod('validateBillAddress');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			$('#billAddressState').html(EMPTY_STRING);
			setError('billAddress_error', validator.getValue(ERROR));
			return false;
		} else {
			$('#billAddressState').html(DISPLAY_STATE + validator.getValue("STATE_CODE"));
		}
	}
	if (isEmpty($('#shipAddress').val())) {
		$('#shipAddress').val(value);
		$('#shipAddressState').html($('#billAddressState').html());
	}
	setFocusLast('shipAddress');
	return true;
}

function shipAddress_val() {
	var value = $('#shipAddress').val();
	clearError('shipAddress_error');
	$('#shipAddressState').html(EMPTY_STRING);
	if (isEmpty(value)) {
		setError('shipAddress_error', MANDATORY);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('ENTITY_CODE', getEntityCode());
		validator.setValue('CUSTOMER_ID', $('#custID').val());
		validator.setValue('ADDR_SL', value);
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.lss.eleasebean');
		validator.setMethod('validateBillAddress');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			$('#shipAddressState').html(EMPTY_STRING);
			setError('shipAddress_error', validator.getValue(ERROR));
			return false;
		} else {
			$('#shippingState').val(validator.getValue("STATE_CODE"));
			$('#shipAddressState').html(DISPLAY_STATE + validator.getValue("STATE_CODE"));
		}
	}
	setFocusLast('billBranch');
	return true;
}

function billBranch_val() {
	var value = $('#billBranch').val();
	clearError('billBranch_error');
	$('#invType').val(EMPTY_STRING);
	$('#billState').val(EMPTY_STRING);
	$('#billState_desc').html(EMPTY_STRING);
	$('#billBranch_desc').html(EMPTY_STRING);
	if (isEmpty(value)) {
		setError('billBranch_error', MANDATORY);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('ENTITY_CODE', getEntityCode());
		validator.setValue('BRANCH_CODE', value);
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.lss.eleasebean');
		validator.setMethod('validateBillBranch');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			$('#billBranch_desc').html(EMPTY_STRING);
			$('#billState').val(EMPTY_STRING);
			$('#billState_desc').html(EMPTY_STRING);
			setError('billBranch_error', validator.getValue(ERROR));
			return false;
		} else {
			$('#billBranch_desc').html(validator.getValue("BRANCH_NAME"));
			$('#billState').val(validator.getValue("STATE_CODE"));
			$('#billState_desc').html(validator.getValue("DESCRIPTION"));
			$('#invType').val(validator.getValue("INVOICE_TYPE"));
		}
	}
	setFocusLast('nextTab');
	return true;
}

// Address Detail Tab Ends

// Payment Detail Section BEGIN

function grossNetTDS_val() {
	var value = $('#grossNetTDS').val();
	clearError('grossNetTDS_error');
	if (isEmpty(value)) {
		setError('grossNetTDS_error', MANDATORY);
		return false;
	}
	setFocusLast('addPayment');
	return true;
}

// Grid 6 payment details
function repaymentMode_val(valMode) {
	var value = $('#repaymentMode').val();
	clearError('repaymentMode_error');
	if (isEmpty(value)) {
		setError('repaymentModee_error', MANDATORY);
		return false;
	}
	if (value == 'AD') {
		clearRepaymentGrid();
		$('#peakAmountFormat').prop('readonly', true);
		$('#ecsRefNo').prop('readonly', true);
		$('#ecsRefDate').prop('readonly', true);
		$('#ecsRefDate_pic').prop('disabled', true);
		setFocusLast('ecsIFSC');
	} else if (value == 'ECS') {
		clearRepaymentGrid();
		$('#peakAmountFormat').prop('readonly', false);
		$('#ecsRefNo').prop('readonly', false);
		$('#ecsRefDate').prop('readonly', false);
		$('#ecsRefDate_pic').prop('disabled', false);
		setFocusLast('ecsIFSC');
	} else {
		clearRepaymentGrid();
		$('#ecsIFSC').prop('readonly', true);
		$('#ecsBank').prop('readonly', true);
		$('#ecsIFSC_pic').prop('disabled', true);
		$('#ecsBranch').prop('readonly', true);
		$('#cusAccNumber').prop('readonly', true);
		$('#peakAmountFormat').prop('readonly', true);
		$('#ecsRefNo').prop('readonly', true);
		$('#ecsRefDate').prop('readonly', true);
		$('#ecsRefDate_pic').prop('disabled', true);
		setFocusLast('newPayment');
	}
	return true;
}

function paymentUptoTenor_val() {
	var value = $('#paymentUptoTenor').val();
	clearError('paymentUptoTenor_error');
	if (isEmpty(value)) {
		setError('paymentUptoTenor_error', MANDATORY);
		return false;
	}
	if (isZero(value)) {
		setError('paymentUptoTenor_error', ZERO_CHECK);
		return false;
	}
	if (!isNumeric(value)) {
		setError('paymentUptoTenor_error', INVALID_NUMBER);
		return false;
	}
	if (!isPositive(value)) {
		setError('paymentUptoTenor_error', POSITIVE_CHECK);
		return false;
	}
	if (isEmpty($('#tenor').val())) {
		setError('paymentUptoTenor_error', TENOR_MANDATORY);
		return false;
	}
	if (parseInt(value) > parseInt($('#tenor').val())) {
		setError('paymentUptoTenor_error', UPTO_TENOR_GREATER_THAN_MONTH + $('#tenor').val());
		return false;
	}
	if (leasePaymentDetails_Grid.getRowsNum() > 0) {
		var totalTenor = 0;
		for (var i = 0; i < leasePaymentDetails_Grid.getRowsNum(); i++) {
			totalTenor = totalTenor + parseInt(leasePaymentDetails_Grid.cells2(i, 3).getValue());
		}
		var editRowValue = 0;
		var editrowId = leasePaymentDetails_Grid.getUserData("", "grid_edit_id");
		if (!isEmpty(editrowId)) {
			editRowValue = parseInt(leasePaymentDetails_Grid.cells(editrowId, 1).getValue());
		}
		totalTenor = totalTenor + parseInt($('#paymentUptoTenor').val()) - editRowValue;
		if ($('#tenor').val() < totalTenor) {
			setError('paymentUptoTenor_error', TOTAL_TENOR_GREATER_THAN_MONTH + $('#tenor').val());
			return false;
		}
	}
	setFocusLast('repaymentMode');
	return true;
}

function ecsIFSC_val() {
	var value = $('#ecsIFSC').val();
	clearError('ecsIFSC_error');
	clearError('ecsBank_error');
	$('#ecsBank').prop('readonly', false);
	$('#ecsBranch').prop('readonly', false);
	clearError('ecsBranch_error');
	if ($('#repaymentMode').val() == 'ECS' || $('#repaymentMode').val() == 'AD') {
		if (!isEmpty(value)) {
			validator.clearMap();
			validator.setMtm(false);
			validator.setValue('ENTITY_CODE', getEntityCode());
			validator.setValue('REPAY_MODE', $('#repaymentMode').val());
			validator.setValue('IFSC_CODE', $('#ecsIFSC').val());
			validator.setValue(ACTION, USAGE);
			validator.setClass('patterns.config.web.forms.lss.eleasebean');
			validator.setMethod('validateIFSCCode');
			validator.sendAndReceive();
			if (validator.getValue(ERROR) != EMPTY_STRING) {
				$('#ecsBank').val(EMPTY_STRING);
				$('#ecsBranch').val(EMPTY_STRING);
				$('#ecsBank').prop('readonly', false);
				$('#ecsBranch').prop('readonly', false);
				setError('ecsIFSC_error', validator.getValue(ERROR));
				return false;
			} else {
				$('#ecsBank').prop('readonly', true);
				$('#ecsBranch').prop('readonly', true);
				$('#ecsBank').val(validator.getValue("BANK_NAME"));
				$('#ecsBranch').val(validator.getValue("BRANCH_NAME"));
				setFocusLast('cusAccNumber');
				return true;
			}
		}
	}
	setFocusLast('ecsBank');
	return true;
}

function ecsBank_val() {
	var value = $('#ecsBank').val();
	clearError('ecsBank_error');
	if ($('#repaymentMode').val() == 'ECS' || $('#repaymentMode').val() == 'AD') {
		if (isEmpty(value)) {
			setError('ecsBank_error', MANDATORY);
			return false;
		}
		if (!isValidName(value)) {
			setError('ecsBank_error', INVALID_VALUE);
			return false;
		}
	}
	setFocusLast('ecsBranch');
	return true;
}

function ecsBranch_val() {
	var value = $('#ecsBranch').val();
	clearError('ecsBranch_error');
	if ($('#repaymentMode').val() == 'ECS' || $('#repaymentMode').val() == 'AD') {
		if (isEmpty(value)) {
			setError('ecsBranch_error', MANDATORY);
			return false;
		}
		if (!isValidName(value)) {
			setError('ecsBranch_error', INVALID_VALUE);
			return false;
		}
	}
	setFocusLast('cusAccNumber');
	return true;
}

function cusAccNumber_val() {
	var value = $('#cusAccNumber').val();
	clearError('cusAccNumber_error');
	if ($('#repaymentMode').val() == 'ECS' || $('#repaymentMode').val() == 'AD') {
		if (isEmpty(value)) {
			setError('cusAccNumber_error', MANDATORY);
			return false;
		}
		if (!isValidAccountNumber(value)) {
			setError('cusAccNumber_error', INVALID_ACNT_NUMBER);
			return false;
		}
	}
	if ($('#repaymentMode').val() == 'AD')
		setFocusLast('newPayment');
	else
		setFocusLast('peakAmountFormat');
	return true;
}

function peakAmountFormat_val() {
	var value = unformatAmount($('#peakAmountFormat').val());
	clearError('peakAmount_error');
	var maxPeakAmount = 0;
	if ($('#repaymentMode').val() == 'ECS') {
		if (isEmpty(value)) {
			setError('peakAmount_error', MANDATORY);
			return false;
		}
		$('#peakAmount').val(value);
		if (isZero(value)) {
			setError('peakAmount_error', ZERO_AMOUNT);
			return false;
		}
		if (!isCurrencySmallAmount($('#peakAmount_curr').val(), value)) {
			setError('peakAmount_error', INVALID_SMALL_AMOUNT);
			return false;
		}
		if (!isPositiveAmount(value)) {
			setError('peakAmount_error', POSITIVE_AMOUNT);
			return false;
		}
		if (leaseRentalCharges_Grid.getRowsNum() > 0) {
			for (var i = 0; i < leaseRentalCharges_Grid.getRowsNum(); i++) {
				if (parseFloat(maxPeakAmount) < parseFloat(leaseRentalCharges_Grid.cells2(i, 5).getValue()))
					maxPeakAmount = leaseRentalCharges_Grid.cells2(i, 5).getValue();
			}
		}
		if (parseFloat(value) < parseFloat(maxPeakAmount)) {
			setError('peakAmount_error', PEAK_AMOUNT_GREATER);
			return false;
		}
		$('#peakAmountFormat').val(formatAmount(value, $('#peakAmount_curr').val()));
	}
	setFocusLast('ecsRefNo');
	return true;
}

function ecsRefNo_val() {
	var value = $('#ecsRefNo').val();
	clearError('ecsRefNo_error');
	if ($('#repaymentMode').val() == 'ECS') {
		if (isEmpty(value)) {
			setError('ecsRefNo_error', MANDATORY);
			return false;
		}
		if (!isValidOtherInformation25(value)) {
			setError('ecsRefNo_error', INVALID_REF_NUMBER);
			return false;
		}
	}
	setFocusLast('ecsRefDate');
	return true;
}

function ecsRefDate_val() {
	var value = $('#ecsRefDate').val();
	clearError('ecsRefDate_error');
	if ($('#repaymentMode').val() == 'ECS') {
		if (isEmpty(value)) {
			$('#ecsRefDate').val(getCBD());
			value = $('#ecsRefDate').val();
		}
		if (!isDate(value)) {
			setError('ecsRefDate_error', INVALID_DATE);
			return false;
		}
		if (isDateLesser(value, $('#firstPaymet').val())) {
			setError('ecsRefDate_error', BILL_START_DATE_LESSER);
			return false;
		}
		if (!isDateGreaterEqual(value, getCBD())) {
			setError('ecsRefDate_error', DATE_GECBD);
			return false;
		}
	}
	setFocusLast('newPayment');
	return true;
}

// add or clear payment grid
function addPaymentDetails() {
	clearError('paymentErrors_error');
	if (!isEmpty($('#tenor').val())) {
		if (isNumeric($('#tenor').val())) {
			if (leasePaymentDetails_Grid.getRowsNum() > 0) {
				var totalTenor = 0;
				for (var i = 0; i < leasePaymentDetails_Grid.getRowsNum(); i++) {
					totalTenor = totalTenor + parseInt(leasePaymentDetails_Grid.cells2(i, 3).getValue());
				}
				if (parseInt($('#tenor').val()) < parseInt(totalTenor)) {
					setError('paymentErrors_error', TENOR_CANT_ADD_NEW);
					return false;
				}
			}
			clearRepaymentGrid();
			$('#paymentUptoTenor').val(EMPTY_STRING);
			if (paymentSlSeqence()) {
				win = EMPTY_STRING;
				win = showWindow('paymentDetails', EMPTY_STRING, PAYMENT_DETAILS, EMPTY_STRING, true, false, false, 950, 400);
				$('#paymentUptoTenor').focus();
			}
		} else {
			setError('paymentErrors_error', TENOR_SHOULDBE_NUMERIC);
			return false;
		}
	} else {
		setError('paymentErrors_error', TENOR_MANDATORY);
		return false;
	}
}

function clearPaymentDetails() {
	clearError('paymentErrors_error');
	leasePaymentDetails_Grid.clearAll();
	leasePaymentDetails_Grid.setUserData("", "grid_edit_id", EMPTY_STRING);
	clearRepaymentGrid();
	$('#paymentUptoTenor').val(EMPTY_STRING);
	setFocusLast('processCode');
}

function paymentSlSeqence() {
	clearError('paymentErrors_error');
	leaseNoOfAssets_Grid.setUserData("", "grid_edit_id", EMPTY_STRING);
	var assetSl;
	ros = leasePaymentDetails_Grid.getRowsNum();
	var noOfVal = $('#tenor').val();
	if (noOfVal < ros) {
		setError('paymentUptoTenor_error', NO_OF_TENOR_GREATER_MONTH + noOfVal);
		return false;
	} else if (noOfVal >= ros) {
		assetSl = parseInt(ros) + 1;
		$('#paymentSerial').val(assetSl);
		return true;
	}
}

function deletePaymentGridRow(row) {
	clearError('paymentErrors_error');
	if (confirm(DELETE_CONFIRM)) {
		leasePaymentDetails_Grid.setUserData("", "grid_edit_id", EMPTY_STRING);
		leasePaymentDetails_Grid.deleteRow(row);
		clearRepaymentGrid();
		$('#paymentUptoTenor').val(EMPTY_STRING);
		setFocusLast('addPayment');
	}
}

function eleasePayment_editGrid(rowId) {
	clearError('paymentErrors_error');
	$('#paymentUptoTenor').val(EMPTY_STRING);
	clearRepaymentGrid();
	win = EMPTY_STRING;
	win = showWindow('paymentDetails', EMPTY_STRING, 'Payment Details', EMPTY_STRING, true, false, false, 950, 400);
	leasePaymentDetails_Grid.setUserData("", "grid_edit_id", rowId);
	$('#paymentSerial').val(leasePaymentDetails_Grid.cells(rowId, 2).getValue());
	$('#paymentUptoTenor').val(leasePaymentDetails_Grid.cells(rowId, 3).getValue());
	$('#repaymentMode').val(leasePaymentDetails_Grid.cells(rowId, 12).getValue());
	repaymentMode_val();
	$('#ecsIFSC').val(leasePaymentDetails_Grid.cells(rowId, 5).getValue());
	$('#ecsBank').val(leasePaymentDetails_Grid.cells(rowId, 6).getValue());
	$('#ecsBranch').val(leasePaymentDetails_Grid.cells(rowId, 7).getValue());
	$('#cusAccNumber').val(leasePaymentDetails_Grid.cells(rowId, 8).getValue());
	$('#peakAmountFormat').val(leasePaymentDetails_Grid.cells(rowId, 13).getValue());
	$('#ecsRefNo').val(leasePaymentDetails_Grid.cells(rowId, 10).getValue());
	$('#ecsRefDate').val(leasePaymentDetails_Grid.cells(rowId, 11).getValue());
	setFocusLast('paymentUptoTenor');
}

function paymentDetails_gridDuplicateCheck() {
	var currentValue = [ $('#paymentSerial').val() ];
	if (!_grid_duplicate_check(leasePaymentDetails_Grid, currentValue, [ 1 ])) {
		setError('paymentErrors_error', DUPLICATE_DATA);
		return false;
	}
	return true;
}
function updateNewPaymentDetails() {
	clearError('paymentErrors_error');
	if (leasePaymentDetails_Grid.getRowsNum() > $('#tenor').val()) {
		setError('paymentErrors_error', NO_OF_TENOR_GREATER_MONTH + $('#tenor').val());
		return false;
	}
	if (paymentDetails_gridDuplicateCheck() && paymentGridrevalidate()) {
		var linkValue = "<a href='javascript:void(0);' onclick=eleasePayment_editGrid('" + $('#paymentSerial').val() + "')><i title='Edit Payment Details' id='editPayment' data-toggle='tooltip' class='fa fa-edit' style='margin-right: 5px; color: #3da0e3;font-size: 21px; cursor: pointer'></i></a><span> </span> <a href='javascript:void(0);' onclick=deletePaymentGridRow('"
				+ $('#paymentSerial').val() + "');><i title='Remove from Grid' id='removePayment' data-toggle='tooltip' class='fa fa-remove' style='margin-left: -3px;font-size: 21px; color: #3da0e3; cursor: pointer'></i></a>";
		var field_values = [ '', linkValue, $('#paymentSerial').val(), $('#paymentUptoTenor').val(), $('#repaymentMode option:selected').text(), $('#ecsIFSC').val(), $('#ecsBank').val(), $('#ecsBranch').val(), $('#cusAccNumber').val(), $('#peakAmountFormat').val(), $('#ecsRefNo').val(), $('#ecsRefDate').val(), $('#repaymentMode').val(), $('#peakAmount').val() ];
		var uid = _grid_updateRow(leasePaymentDetails_Grid, field_values);
		leasePaymentDetails_Grid.changeRowId(uid, $('#paymentSerial').val());
		$('#paymentUptoTenor').val(EMPTY_STRING);
		clearRepaymentGrid();
		closeInlinePopUp(win);
		setFocusLast('processCode');
		return true;
	}
}

function updateAndNewPaymentDetails() {
	clearError('paymentErrors_error');
	if (leasePaymentDetails_Grid.getRowsNum() > $('#tenor').val()) {
		setError('paymentErrors_error', NO_OF_TENOR_GREATER_MONTH + $('#tenor').val());
		return false;
	}
	if (paymentDetails_gridDuplicateCheck() && paymentGridrevalidate()) {
		var linkValue = "<a href='javascript:void(0);' onclick=eleasePayment_editGrid('" + $('#paymentSerial').val() + "')><i title='Edit Payment Details' id='editPayment' data-toggle='tooltip' class='fa fa-edit' style='margin-right: 5px;font-size: 21px; color: #3da0e3; cursor: pointer'></i></a><span> </span> <a href='javascript:void(0);' onclick=deletePaymentGridRow('"
				+ $('#paymentSerial').val() + "');><i title='Remove from Grid' id='removePayment' data-toggle='tooltip' class='fa fa-remove' style='margin-left: -3px;font-size: 21px; color: #3da0e3; cursor: pointer'></i></a>";
		var field_values = [ '', linkValue, $('#paymentSerial').val(), $('#paymentUptoTenor').val(), $('#repaymentMode option:selected').text(), $('#ecsIFSC').val(), $('#ecsBank').val(), $('#ecsBranch').val(), $('#cusAccNumber').val(), $('#peakAmountFormat').val(), $('#ecsRefNo').val(), $('#ecsRefDate').val(), $('#repaymentMode').val(), $('#peakAmount').val() ];
		var uid = _grid_updateRow(leasePaymentDetails_Grid, field_values);
		leasePaymentDetails_Grid.changeRowId(uid, $('#paymentSerial').val());
		$('#paymentUptoTenor').val(EMPTY_STRING);
		clearRepaymentGrid();
		var totalTenor = 0;
		for (var i = 0; i < leasePaymentDetails_Grid.getRowsNum(); i++) {
			totalTenor = totalTenor + parseInt(leasePaymentDetails_Grid.cells2(i, 3).getValue());
		}
		if (totalTenor != parseInt($('#tenor').val())) {
			paymentSlSeqence();
			setFocusLast('paymentUptoTenor');
		} else if (totalTenor == parseInt($('#tenor').val())) {
			alert(TENOR_SUM_GRID_COUNT_SAME);
			setFocusLast('processCode');
			closeInlinePopUp(win);
		}
		return true;
	}
}

function clearPaymentDetails() {
	clearError('paymentErrors_error');
	leasePaymentDetails_Grid.clearAll();
	leasePaymentDetails_Grid.setUserData("", "grid_edit_id", EMPTY_STRING);
	setFocusLast('noOfAssets');
}

function paymentGridrevalidate() {
	var errors = 0;
	if (!paymentUptoTenor_val(false)) {
		errors++;
	}
	if (!ecsIFSC_val(false)) {
		errors++;
	}
	if (!ecsBranch_val(false)) {
		errors++;
	}
	if (!cusAccNumber_val(false)) {
		errors++;
	}
	if (!peakAmountFormat_val(false)) {
		errors++;
	}
	if (!ecsRefNo_val(false)) {
		errors++;
	}
	if (!ecsRefDate_val(false)) {
		errors++;
	}
	if (errors > 0) {
		if (win)
			win.showInnerScroll();
		return false;
	} else
		return true;
}

// end popup payment details

function processCode_val() {
	var value = $('#processCode').val();
	clearError('processCode_error');
	$('#paymentErrors_error').html(EMPTY_STRING);
	$('#processCode_desc').html(EMPTY_STRING);
	if (isEmpty(value)) {
		setError('processCode_error', MANDATORY);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('ENTITY_CODE', getEntityCode());
		validator.setValue('BANK_PROCESS_CODE', value);
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.lss.eleasebean');
		validator.setMethod('validateProcessCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			$('#processCode_desc').html(EMPTY_STRING);
			setError('processCode_error', validator.getValue(ERROR));
			return false;
		} else {
			$('#processCode_desc').html(validator.getValue("NAME"));
		}
	}
	var totalTenor = 0;
	for (var i = 0; i < leasePaymentDetails_Grid.getRowsNum(); i++) {
		totalTenor = totalTenor + parseInt(leasePaymentDetails_Grid.cells2(i, 3).getValue());
	}
	if (totalTenor != parseInt($('#tenor').val())) {
		setError('paymentErrors_error', PAYMENT_TENOR_EQUAL_GRID_COUNT);
		return false;
	}
	setFocusLast('invTempCode');
	return true;
}

function invTempCode_val() {
	var value = $('#invTempCode').val();
	clearError('invTempCode_error');
	$('#invTempCode_desc').html(EMPTY_STRING);
	if (isEmpty(value)) {
		setError('invTempCode_error', MANDATORY);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('ENTITY_CODE', getEntityCode());
		validator.setValue('INVTEMPLATE_CODE', $('#invTempCode').val());
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.lss.eleasebean');
		validator.setMethod('validateInvTempCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			$('#invTempCode_desc').html(EMPTY_STRING);
			setError('invTempCode_error', validator.getValue(ERROR));
			return false;
		} else {
			$('#invTempCode_desc').html(validator.getValue("DESCRIPTION"));
		}
	}
	setFocusLast('famDesc');
	return true;
}

function famDesc_val() {
	var value = $('#famDesc').val();
	clearError('famDesc_error');
	if (!isEmpty(value)) {
		if (!isValidOtherInformation25(value)) {
			setError('famDesc_error', HMS_INVALID_VALUE);
			return false;
		}
	}
	setFocusLast('hierarCode');
	return true;
}

function hierarCode_val() {
	var value = $('#hierarCode').val();
	clearError('hierarCode_error');
	$('#hierarCode_desc').html(EMPTY_STRING);
	if (isEmpty(value)) {
		setError('hierarCode_error', MANDATORY);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('LAM_HIER_CODE', $('#hierarCode').val());
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.lss.eleasebean');
		validator.setMethod('validatehierarCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('hierarCode_error', validator.getValue(ERROR));
			$('#hierarCode_desc').html(EMPTY_STRING);
			return false;
		} else {
			$('#hierarCode_desc').html(validator.getValue('DESCRIPTION'));
		}
	}
	setFocusLast('invCycleNo');
	return true;
}

function invCycleNo_val() {
	var value = $('#invCycleNo').val();
	clearError('invCycleNo_error');
	if (isEmpty(value)) {
		setError('invCycleNo_error', MANDATORY);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('INV_CYCLE_NUMBER', $('#invCycleNo').val());
	validator.setValue(ACTION, USAGE);
	validator.setClass('patterns.config.web.forms.lss.eleasebean');
	validator.setMethod('validateInvoiceCycleNumber');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('invCycleNo_error', validator.getValue(ERROR));
		return false;
	}
	setFocusLast('initCycleNo');
	return true;
}

function initCycleNo_val() {
	var value = $('#initCycleNo').val();
	clearError('initCycleNo_error');
	if (!isEmpty($('#intFromDate').val()) && !isEmpty($('#intUptoDate').val())) {
		if (isEmpty(value)) {
			setError('initCycleNo_error', MANDATORY);
			return false;
		}
	}
	if (!isEmpty(value)) {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('INV_CYCLE_NUMBER', $('#initCycleNo').val());
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.lss.eleasebean');
		validator.setMethod('validateInvoiceCycleNumber');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('initCycleNo_error', validator.getValue(ERROR));
			return false;
		}
	}
	setFocusLast('finalCycleNo');
	return true;
}

function finalCycleNo_val() {
	var value = $('#finalCycleNo').val();
	clearError('finalCycleNo_error');
	if (!isEmpty($('#finalFromDate').val()) && !isEmpty($('#finalUptoDate').val())) {
		if (isEmpty(value)) {
			setError('finalCycleNo_error', MANDATORY);
			return false;
		}
	}
	if (!isEmpty(value)) {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('INV_CYCLE_NUMBER', $('#finalCycleNo').val());
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.lss.eleasebean');
		validator.setMethod('validateInvoiceCycleNumber');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('finalCycleNo_error', validator.getValue(ERROR));
			return false;
		}
	}
	setFocusLast('remarks');
	return true;
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', INVALID_REMARKS);
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', INVALID_REMARKS);
			return false;
		}
	}
	if ($('#action').val() == MODIFY) {
		if (isEmpty(value)) {
			setError('remarks_error', MANDATORY);
			return false;
		}
	}
	setFocusOnSubmit();
	return true;
}
// Payment Detail Section Ends

// Tenor Detail Section Begins

function tenor_val() {
	var value = $('#tenor').val();
	clearError('tenor_error');
	$('#totalTenor_desc').html(EMPTY_STRING);
	if (isEmpty(value)) {
		setError('tenor_error', MANDATORY);
		$('#totalTenor_desc').html(EMPTY_STRING);
		return false;
	}
	if (isZero(value)) {
		setError('tenor_error', ZERO_CHECK);
		$('#totalTenor_desc').html(EMPTY_STRING);
		return false;
	}
	if (!isNumeric(value)) {
		setError('tenor_error', INVALID_NUMBER);
		$('#totalTenor_desc').html(EMPTY_STRING);
		return false;
	}
	if (!isPositive(value)) {
		setError('tenor_error', POSITIVE_CHECK);
		$('#totalTenor_desc').html(EMPTY_STRING);
		return false;
	}
	$('#tenor').val(parseInt(value));
	$('#totalTenor_desc').html('   <  ' + value + '    Total Tenor');
	setFocusLast('cancelPeriod');
	return true;
}

function cancelPeriod_val() {
	var value = $('#cancelPeriod').val();
	clearError('cancelPeriod_error');
	if (!isEmpty(value)) {
		if (isZero(value)) {
			setError('cancelPeriod_error', ZERO_CHECK);
			return false;
		}
		if (!isNumeric(value)) {
			setError('cancelPeriod_error', HMS_INVALID_NUMBER);
			return false;
		}
		if (!isPositive(value)) {
			setError('cancelPeriod_error', POSITIVE_CHECK);
			return false;
		}
		$('#cancelPeriod').val(parseInt(value));
		if (parseInt(value) > parseInt($('#tenor').val())) {
			setError('cancelPeriod_error', CANCEL_TENOR_CANT_GREATER_TENOR + $('#tenor').val());
			return false;
		}
	}
	setFocusLast('repayFreq');
	return true;
}

function repayFreq_val(valMode) {
	var value = $('#repayFreq').val();
	clearError('repayFreq_error');
	if (isEmpty(value)) {
		setError('repayFreq_error', MANDATORY);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('REPAY_FREQUENCY', $('#repayFreq').val());
	validator.setValue('TENOR', $('#tenor').val());
	validator.setClass('patterns.config.web.forms.lss.eleasebean');
	validator.setMethod('validateTenorFrequency');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('repayFreq_error', validator.getValue(ERROR));
		return false;
	}
	if (valMode) {
		if (value == 'M')
			$('#leaseRentOption').prop('selectedIndex', 0);
		else if (value == 'Q')
			$('#leaseRentOption').prop('selectedIndex', 1);
		else if (value == 'H')
			$('#leaseRentOption').prop('selectedIndex', 2);
		else
			$('#leaseRentOption').prop('selectedIndex', 0);
	}
	setFocusLast('advArrers');
	return true;
}

function advArrers_val() {
	var value = $('#advArrers').val();
	clearError('advArrers_error');
	if (isEmpty(value)) {
		setError('advArrers_error', MANDATORY);
		return false;
	}
	setFocusLast('firstPaymet');
	return true;
}

function firstPaymet_val(valMode) {
	var value = $('#firstPaymet').val();
	clearError('firstPaymet_error');
	if (isEmpty(value)) {
		setError('firstPaymet_error', MANDATORY);
		$('#rentDate').val(EMPTY_STRING);
		$('#lastDate').val(EMPTY_STRING);
		return false;
	}
	if (!isDate(value)) {
		setError('firstPaymet_error', INVALID_DATE);
		$('#rentDate').val(EMPTY_STRING);
		$('#lastDate').val(EMPTY_STRING);
		return false;
	}
	if (!isDateGreaterEqual(value, $('#leaseComDate').val())) {
		$('#rentDate').val(EMPTY_STRING);
		$('#lastDate').val(EMPTY_STRING);
		setError('firstPaymet_error', LEASE_COMMENCEMENT_DATE_GREATER + $('#leaseComDate').val());
		return false;
	}
	if (isDateLesser(value, getCBD())) {
		$('#rentDate').val(EMPTY_STRING);
		$('#lastDate').val(EMPTY_STRING);
		setError('firstPaymet_error', DATE_GECBD);
		return false;
	}
	$('#rentDate').val($('#firstPaymet').val());
	var lastuptoDate = moment($('#firstPaymet').val(), SCRIPT_DATE_FORMAT).add($('#tenor').val(), "month").subtract(1, "days").format(SCRIPT_DATE_FORMAT);
	$('#lastDate').val(lastuptoDate);
	if (valMode) {
		if (!isDateLesserEqual($('#firstPaymet').val(), $('#leaseComDate').val())) {
			var diffDays = getNoOfDays($('#firstPaymet').val(), $('#leaseComDate').val());
			var diffValues = parseInt(diffDays) + parseInt(1);
			if (diffValues > 0) {
				var initialFrom = moment($('#leaseComDate').val(), SCRIPT_DATE_FORMAT).add(1, "days").format(SCRIPT_DATE_FORMAT);
				var initialUpto = moment($('#firstPaymet').val(), SCRIPT_DATE_FORMAT).add(-1, "days").format(SCRIPT_DATE_FORMAT);
				if (isEmpty($('#intFromDate').val()))
					$('#intFromDate').val(initialFrom);
				if (isEmpty($('#intUptoDate').val()))
					$('#intUptoDate').val(initialUpto);
			}
		}
	}
	setFocusLast('intFromDate');
	return true;
}

function intFromDate_val() {
	var value = $('#intFromDate').val();
	clearError('intFromDate_error');
	if (!isEmpty(value)) {
		if (!isDate(value)) {
			setError('intFromDate_error', INVALID_DATE);
			return false;
		}
		if (!isDateGreaterEqual(value, $('#leaseComDate').val())) {
			setError('intFromDate_error', LEASE_COMMENCEMENT_DATE_GREATER);
			return false;
		}
		if (isDateGreaterEqual(value, $('#firstPaymet').val())) {
			setError('intFromDate_error', PAYMENT_DATE_LESS_THAN);
			return false;
		}
		setFocusLast('intUptoDate');
	} else {
		$('#intUptoDate').val(EMPTY_STRING);
		clearError('intUptoDate_error');
		setFocusLast('finalFromDate');
	}
	return true;
}

function intUptoDate_val() {
	var value = $('#intUptoDate').val();
	var intFromDate = $('#intFromDate').val();
	clearError('intUptoDate_error');
	if (!isEmpty(intFromDate)) {
		if (isEmpty(value)) {
			setError('intUptoDate_error', MANDATORY);
			return false;
		}
		if (!isDate(value)) {
			setError('intUptoDate_error', INVALID_DATE);
			return false;
		}
		if (!isDateGreaterEqual(value, intFromDate)) {
			setError('intUptoDate_error', DATE_GREATER_THAN_INITIAL_DATE);
			return false;
		}
		if (isDateGreaterEqual(value, $('#firstPaymet').val())) {
			setError('intUptoDate_error', PAYMENT_DATE_LESS_THAN);
			return false;
		}
	} else if (!isEmpty(value)) {
		setError('intFromDate_error', INITIAL_DATE_MANDATORY);
		return false;
	}
	setFocusLast('finalFromDate');
	return true;
}

function finalFromDate_val() {
	var value = $('#finalFromDate').val();
	clearError('finalFromDate_error');
	if (!isEmpty(value)) {
		if (!isDate(value)) {
			setError('finalFromDate_error', INVALID_DATE);
			return false;
		}
		if (isDateLesserEqual(value, $('#lastDate').val())) {
			setError('finalFromDate_error', DATE_GREATER_THAN_BILLING_DATE);
			return false;
		}
		setFocusLast('finalUptoDate');
	} else {
		$('#finalUptoDate').val(EMPTY_STRING);
		clearError('finalUptoDate_error');
		setFocusLast('nextTab2');
	}
	return true;
}

function finalUptoDate_val() {
	var value = $('#finalUptoDate').val();
	var finalFromDate = $('#finalFromDate').val();
	clearError('finalUptoDate_error');
	if (!isEmpty(finalFromDate)) {
		if (isEmpty(value)) {
			setError('finalUptoDate_error', MANDATORY);
			return false;
		}
		if (!isDate(value)) {
			setError('finalUptoDate_error', INVALID_DATE);
			return false;
		}
		if (isDateLesserEqual(value, finalFromDate)) {
			setError('finalUptoDate_error', DATE_GREATER_THAN_FINAL_DATE);
			return false;
		}
	} else if (!isEmpty(value)) {
		setError('finalFromDate_error', FINAL_DATE_MANDATORY);
		return false;
	}
	setFocusLast('nextTab2');
	return true;
}

// Tenor Detail Section Ends

// Rent Detail Seciton Begins

function grossAssetCostFormat_val() {
	var value = unformatAmount($('#grossAssetCostFormat').val());
	clearError('grossAssetCost_error');
	if (isEmpty(value)) {
		setError('grossAssetCost_error', MANDATORY);
		return false;
	}
	$('#grossAssetCost').val(value);
	if (isZero(value)) {
		setError('grossAssetCost_error', ZERO_AMOUNT);
		return false;
	}
	if (!isCurrencySmallAmount($('#grossAssetCost_curr').val(), value)) {
		setError('grossAssetCost_error', INVALID_SMALL_AMOUNT);
		return false;
	}
	if (!isPositiveAmount(value)) {
		setError('grossAssetCost_error', POSITIVE_AMOUNT);
		return false;
	}
	$('#grossAssetCostFormat').val(formatAmount(value, $('#grossAssetCost_curr').val()));
	if (!isEmpty($('#preLeaseRefDate').val()) && !isEmpty($('#preLeaseRefSerial').val())) {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('ENTITY_CODE', getEntityCode());
		validator.setValue('CONTRACT_DATE', $('#preLeaseRefDate').val());
		validator.setValue('CONTRACT_SL', $('#preLeaseRefSerial').val());
		validator.setValue('GROSS_ASSET_COST', value);
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.lss.eleasebean');
		validator.setMethod('validateGrossAssetCost');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('grossAssetCost_error', validator.getValue(ERROR));
			return false;
		}
	}
	setFocusLast('commencementValueFormat');
	return true;
}

function commencementValueFormat_val(valMode) {
	var value = unformatAmount($('#commencementValueFormat').val());
	clearError('commencementValue_error');
	if (isEmpty(value)) {
		setError('commencementValue_error', MANDATORY);
		return false;
	}
	if (valMode) {
		if (prevCommencementValue == null) {
			prevCommencementValue = value;
		}
		if (prevCommencementValue != value) {
			leaseRentalCharges_Grid.clearAll();
			leaseApplicableTax_Grid.clearAll();
			financialScheduleNonOLGrid.clearAll();
			financialScheduleOLGrid.clearAll();
			prevCommencementValue = value;
		}
	}
	$('#commencementValue').val(value);
	if (isZero(value)) {
		setError('commencementValue_error', ZERO_AMOUNT);
		return false;
	}
	if (!isCurrencySmallAmount($('#commencementValue_curr').val(), value)) {
		setError('commencementValue_error', INVALID_SMALL_AMOUNT);
		return false;
	}
	if (!isPositiveAmount(value)) {
		setError('commencementValue_error', POSITIVE_AMOUNT);
		return false;
	}
	if (parseFloat(value) > parseFloat($('#grossAssetCost').val())) {
		setError('commencementValue_error', COMMENVALUE_LE_GROASSETCOST);
		return false;
	}
	$('#commencementValueFormat').val(formatAmount(value, $('#commencementValue_curr').val()));
	setFocusLast('leaseRentOption');
	return true;
}

function leaseRentOption_val() {
	var value = $('#leaseRentOption').val();
	clearError('leaseRentOption_error');
	if (isEmpty(value)) {
		setError('leaseRentOption_error', MANDATORY);
		return false;
	}
	setFocusLast('assureRVPer');
	return true;
}

function assureRVPer_val() {
	var value = $('#assureRVPer').val();
	clearError('assureRVPer_error');
	if (!isEmpty(value)) {
		if (isZero(value)) {
			setError('assureRVPer_error', ZERO_CHECK);
			return false;
		}
		if (isNegativeAmount($('#assureRVPer'))) {
			setError('assureRVPer_error', POSITIVE_CHECK);
			return false;
		}
		if (!isValidPercentage($('#assureRVPer').val())) {
			setError('assureRVPer_error', INVALID_PERCENTAGE);
			return false;
		}
		$('#assureRVPer').val(parseFloat(value));
	}
	setFocusLast('assureRVAmountFormat');
	return true;
}

function assureRVAmountFormat_val() {
	var value = unformatAmount($('#assureRVAmountFormat').val());
	clearError('assureRVAmount_error');
	if (isEmpty(value) && isEmpty($('#assureRVPer').val())) {
		setError('assureRVAmount_error', RVPER_OR_RVAMOUNT);
		return false;
	}
	if (!isEmpty(value)) {
		$('#assureRVAmount').val(value);
		if (isZero(value)) {
			setError('assureRVAmount_error', ZERO_AMOUNT);
			return false;
		}
		if (!isCurrencySmallAmount($('#assureRVAmount_curr').val(), value)) {
			setError('assureRVAmount_error', INVALID_SMALL_AMOUNT);
			return false;
		}
		if (!isPositiveAmount(value)) {
			setError('assureRVAmount_error', POSITIVE_AMOUNT);
			return false;
		}
		$('#assureRVAmountFormat').val(formatAmount(value, $('#assureRVAmount_curr').val()));
	}
	if ($('#internalRateReturn').prop('readonly'))
		setFocusLast('dealType');
	else
		setFocusLast('internalRateReturn');
	return true;
}

function internalRateReturn_val() {
	var value = $('#internalRateReturn').val();
	clearError('internalRateReturn_error');
	if ($('#leaseProductType').val() != 'O') {
		if (isEmpty(value)) {
			setError('internalRateReturn_error', MANDATORY);
			return false;
		}
	}
	if (!isEmpty(value)) {
		if (isZero(value)) {
			setError('internalRateReturn_error', ZERO_CHECK);
			return false;
		}
		if (isNegativeAmount($('#internalRateReturn'))) {
			setError('internalRateReturn_error', POSITIVE_CHECK);
			return false;
		}
		if (!isValidPercentage($('#internalRateReturn').val())) {
			setError('internalRateReturn_error', INVALID_PERCENTAGE);
			return false;
		}
		$('#internalRateReturn').val(parseFloat(value));
	}
	setFocusLast('dealType');
	return true;
}

function intBrokenRentFormat_val() {
	var value = unformatAmount($('#intBrokenRentFormat').val());
	$('#intBrokenRent').val(value);
	clearError('intBrokenRent_error');
	if (!isEmpty($('#intFromDate').val()) && !isEmpty($('#intUptoDate').val())) {
		if (isEmpty(value)) {
			value = ZERO;
		}
		if (!isEmpty(value)) {
			$('#intBrokenRent').val(value);
			if (!isCurrencySmallAmount($('#intBrokenRent_curr').val(), value)) {
				setError('intBrokenRent_error', INVALID_SMALL_AMOUNT);
				return false;
			}
			if (!isPositiveAmount(value)) {
				setError('intBrokenRent_error', POSITIVE_AMOUNT);
				return false;
			}
			$('#intBrokenRentFormat').val(formatAmount(value, $('#intBrokenRent_curr').val()));
		}
	} else if (!isEmpty(value)) {
		setError('intBrokenRent_error', MANDATORY_INITIAL_FROM_DATE);
		return false;
	}
	setFocusLast('intBrokenExecFormat');
	return true;
}

function intBrokenExecFormat_val() {
	var value = unformatAmount($('#intBrokenExecFormat').val());
	$('#intBrokenExec').val(value);
	clearError('intBrokenExec_error');
	if (!isEmpty($('#intFromDate').val()) && !isEmpty($('#intUptoDate').val())) {
		if (isEmpty(value)) {
			value = ZERO;
		}
		$('#intBrokenExec').val(value);
		if (!isCurrencySmallAmount($('#intBrokenExec_curr').val(), value)) {
			setError('intBrokenExec_error', INVALID_SMALL_AMOUNT);
			return false;
		}
		if (!isPositiveAmount(value)) {
			setError('intBrokenExec_error', POSITIVE_AMOUNT);
			return false;
		}
		$('#intBrokenExecFormat').val(formatAmount(value, $('#intBrokenExec_curr').val()));

	} else if (!isEmpty(value)) {
		setError('intBrokenExec_error', MANDATORY_INITIAL_FROM_DATE);
		return false;
	}
	if (!isEmpty($('#finalFromDate').val()) && !isEmpty($('#finalUptoDate').val()))
		setFocusLast('finalBrokenRentFormat');
	else {
		// lease._setTabActive('lease_6', true);
		setFocusLast('nextTab3');
	}
	return true;
}

function finalBrokenRentFormat_val() {
	var value = unformatAmount($('#finalBrokenRentFormat').val());
	$('#finalBrokenRent').val(value);
	clearError('finalBrokenRent_error');
	if (!isEmpty($('#finalFromDate').val()) && !isEmpty($('#finalUptoDate').val())) {
		if (isEmpty(value)) {
			value = ZERO;
		}
		if (!isEmpty(value)) {
			$('#finalBrokenRent').val(value);
			if (!isCurrencySmallAmount($('#finalBrokenRent_curr').val(), value)) {
				setError('finalBrokenRent_error', INVALID_SMALL_AMOUNT);
				return false;
			}
			if (!isPositiveAmount(value)) {
				setError('finalBrokenRent_error', POSITIVE_AMOUNT);
				return false;
			}
			$('#finalBrokenRentFormat').val(formatAmount(value, $('#finalBrokenRent_curr').val()));
		}
	} else if (!isEmpty(value)) {
		setError('finalBrokenRent_error', MANDATORY_FINAL_FROM_DATE);
		return false;
	}
	setFocusLast('finaBrokenExecFormat');
	return true;
}

function finaBrokenExecFormat_val() {
	var value = unformatAmount($('#finaBrokenExecFormat').val());
	$('#finaBrokenExec').val(value);
	clearError('finaBrokenExec_error');
	if (!isEmpty($('#finalFromDate').val()) && !isEmpty($('#finalUptoDate').val())) {
		if (isEmpty(value)) {
			value = ZERO;
		}
		$('#finaBrokenExec').val(value);
		if (!isCurrencySmallAmount($('#finaBrokenExec_curr').val(), value)) {
			setError('finaBrokenExec_error', INVALID_SMALL_AMOUNT);
			return false;
		}
		if (!isPositiveAmount(value)) {
			setError('finaBrokenExec_error', POSITIVE_AMOUNT);
			return false;
		}
		$('#finaBrokenExecFormat').val(formatAmount(value, $('#finaBrokenExec_curr').val()));
	} else if (!isEmpty(value)) {
		setError('finaBrokenExec_error', MANDATORY_FINAL_FROM_DATE);
		return false;
	}
	// lease._setTabActive('lease_6', true);
	setFocusLast('nextTab3');
	return true;
}

function dealType_val() {
	$('#uptoToner').prop('readonly', false);
	var value = $('#dealType').val();
	clearError('dealType_error');
	clearError('uptoToner_error');
	if (isEmpty(value)) {
		setError('dealType_error', MANDATORY);
		return false;
	}
	if ($('#dealType').val() == 'U') {
		$('#uptoToner').prop('readonly', true);
		$('#uptoToner').val($('#tenor').val());
		setFocusLast('leaseRentRate');
	} else {
		$('#uptoToner').val(EMPTY_STRING);
		$('#uptoToner').prop('readonly', false);
		setFocusLast('uptoToner');
	}
	return true;
}

// RENT STRUCTURE GRID 2

function uptoToner_val() {
	var value = $('#uptoToner').val();
	clearError('uptoToner_error');
	if (isEmpty(value)) {
		setError('uptoToner_error', MANDATORY);
		return false;
	}
	if (isZero(value)) {
		setError('uptoToner_error', ZERO_CHECK);
		return false;
	}
	if (!isNumeric(value)) {
		setError('uptoToner_error', INVALID_NUMBER);
		return false;
	}
	if (!isPositive(value)) {
		setError('uptoToner_error', POSITIVE_CHECK);
		return false;
	}
	if (isEmpty($('#tenor').val())) {
		setError('uptoToner_error', TENOR_MONTH_ADDING_GRID);
		return false;
	}
	if (parseInt(value) > parseInt($('#tenor').val())) {
		setError('uptoToner_error', UPTO_TENOR_GREATER_THAN_MONTH + $('#tenor').val());
		return false;
	}
	var monthVal = 0;
	if ($('#repayFreq').val() == 'M')
		monthVal = 1;
	else if ($('#repayFreq').val() == 'Q')
		monthVal = 3;
	else if ($('#repayFreq').val() == 'H')
		monthVal = 6;
	else if ($('#repayFreq').val() == 'Y')
		monthVal = 12;
	if (value % monthVal != 0) {
		setError('uptoToner_error', PAY_FREQ_SHOULD_BE_DIVIDE + monthVal);
		return false;
	}
	if (leaseRentalCharges_Grid.getRowsNum() > 0) {
		var totalTenor = 0;
		for (var i = 0; i < leaseRentalCharges_Grid.getRowsNum(); i++) {
			totalTenor = totalTenor + parseInt(leaseRentalCharges_Grid.cells2(i, 1).getValue());
		}
		var editRowValue = 0;
		var editrowId = leaseRentalCharges_Grid.getUserData("", "grid_edit_id");
		if (!isEmpty(editrowId)) {
			editRowValue = parseInt(leaseRentalCharges_Grid.cells(editrowId, 1).getValue());
		}
		totalTenor = totalTenor + parseInt($('#uptoToner').val()) - editRowValue;
		if ($('#tenor').val() < totalTenor) {
			if (!$('#uptoToner').prop('readonly')) {
				setError('uptoToner_error', TOTAL_TENOR_GREATER_THAN_MONTH + $('#tenor').val());
				return false;
			}
		}
	}
	$('#uptoToner').val(parseInt(value));
	setFocusLast('leaseRentRate');
	return true;
}

function leaseRentRate_val() {
	var value = $('#leaseRentRate').val();
	clearError('leaseRentRate_error');
	if (isEmpty(value)) {
		setError('leaseRentRate_error', MANDATORY);
		return false;
	}
	if (!isValidThreeDigitTwoDecimal($('#leaseRentRate'))) {
		setError('leaseRentRate_error', HMS_INVALID_THREEDIGIT_TWODEC);
		return false;
	}
	if (isNegativeAmount($('#leaseRentRate'))) {
		setError('leaseRentRate_error', HMS_POSITIVE_CHECK);
		return false;
	}
	if (!isEmpty($('#commencementValue').val())) {
		if (checkRental != '1') {
			var rentPeriodValue = 0;
			if ($('#leaseRentOption').val() == 'M')
				rentPeriodValue = parseFloat($('#commencementValue').val()) / parseFloat(1000);
			else if ($('#leaseRentOption').val() == 'Q')
				rentPeriodValue = parseFloat($('#commencementValue').val()) / parseFloat(3000);
			else if ($('#leaseRentOption').val() == 'H')
				rentPeriodValue = parseFloat($('#commencementValue').val()) / parseFloat(6000);

			var rentalAmt = parseFloat($('#leaseRentRate').val()) * parseFloat(rentPeriodValue);
			$('#rentalFormat').val(formatAmount(rentalAmt.toString(), $('#rental_curr').val()));
			$('#rental').val(rentalAmt.toString());
		}
		checkRental = EMPTY_STRING;
	}
	$('#leaseRentRate').val(parseFloat(value));
	setFocusLast('rentalFormat');
	return true;
}

function rentalFormat_val() {
	var value = unformatAmount($('#rentalFormat').val());
	clearError('rental_error');
	clearError('uptoToner_error');
	if (isEmpty(value)) {
		setError('rental_error', MANDATORY);
		return false;
	}
	$('#rental').val(value);
	if (isZero(value)) {
		setError('rental_error', ZERO_AMOUNT);
		return false;
	}
	if (!isCurrencySmallAmount($('#rental_curr').val(), value)) {
		setError('rental_error', INVALID_SMALL_AMOUNT);
		return false;
	}
	if (!isPositiveAmount(value)) {
		setError('rental_error', POSITIVE_AMOUNT);
		return false;
	}
	$('#rentalFormat').val(formatAmount(value, $('#rental_curr').val()));
	setFocusLast('execChagsFormat');
	return true;
}

function execChagsFormat_val() {
	var value = unformatAmount($('#execChagsFormat').val());
	clearError('execChags_error');
	if (isEmpty(value)) {
		value = ZERO;
		$('#execChags').val(value);
		$('#execChagsFormat').val(formatAmount(value, $('#execChags_curr').val()));
	} else {
		$('#execChags').val(value);
		if (!isCurrencySmallAmount($('#execChags_curr').val(), value)) {
			setError('execChags_error', INVALID_SMALL_AMOUNT);
			return false;
		}
		if (!isPositiveAmount(value)) {
			setError('execChags_error', POSITIVE_AMOUNT);
			return false;
		}
		$('#execChagsFormat').val(formatAmount(value, $('#execChags_curr').val()));
	}
	setFocusLastOnGridSubmit('leaseRentalCharges_Grid');
	return true;
}

function initialFinalRentAmount() {
	if (leaseRentalCharges_Grid.getRowsNum() == 1) {
		var gridRent = $('#leaseRentRate').val();

		if (!isEmpty($('#commencementValue').val())) {
			if (!isEmpty($('#intFromDate').val()) && !isEmpty($('#intUptoDate').val())) {
				var perInitialThousandValue = 0;
				if ($('#leaseRentOption').val() == 'M') {
					perInitialThousandValue = parseFloat($('#commencementValue').val()) / parseFloat(1000);
				} else if ($('#leaseRentOption').val() == 'Q') {
					perInitialThousandValue = parseFloat($('#commencementValue').val()) / parseFloat(3000);
				} else if ($('#leaseRentOption').val() == 'H') {
					perInitialThousandValue = parseFloat($('#commencementValue').val()) / parseFloat(6000);
				}
				var rentalAmt = (parseFloat(gridRent) * parseFloat(perInitialThousandValue)) / 30;
				var days = getNoOfDays($('#intUptoDate').val(), $('#intFromDate').val());
				var initialRent = parseFloat(rentalAmt) * (parseFloat(days) + 1);
				$('#intBrokenRentFormat').val(formatAmount(initialRent.toString(), $('#intBrokenRent_curr').val()));
				$('#intBrokenRent').val(initialRent.toString());
			}

			if (!isEmpty($('#finalFromDate').val()) && !isEmpty($('#finalUptoDate').val())) {
				var perFinalThousandValue = 0;
				if ($('#leaseRentOption').val() == 'M') {
					perFinalThousandValue = parseFloat($('#commencementValue').val()) / parseFloat(1000);
				} else if ($('#leaseRentOption').val() == 'Q') {
					perFinalThousandValue = parseFloat($('#commencementValue').val()) / parseFloat(3000);
				} else if ($('#leaseRentOption').val() == 'H') {
					perFinalThousandValue = parseFloat($('#commencementValue').val()) / parseFloat(6000);
				}
				var rentalAmt = (parseFloat(gridRent) * parseFloat(perFinalThousandValue)) / 30;
				var days = getNoOfDays($('#finalUptoDate').val(), $('#finalFromDate').val());
				var initialRent = parseFloat(rentalAmt) * (parseFloat(days) + 1);
				$('#finalBrokenRentFormat').val(formatAmount(initialRent.toString(), $('#finalBrokenRent_curr').val()));
				$('#finalBrokenRent').val(initialRent.toString());
			}
		}
	}
}

// GRID 1 Rent

function leaseRentalCharges_addGrid(editMode, editRowId) {
	if (validateTenorSum(editMode, editRowId)) {
		checkRental = '1';
		if (uptoToner_val() && rentalFormat_val() && leaseRentRate_val() && execChagsFormat_val()) {
			var field_values = [ 'false', $('#uptoToner').val(), $('#leaseRentRate').val(), $('#rentalFormat').val(), $('#execChagsFormat').val(), $('#rental').val(), $('#execChags').val() ];
			_grid_updateRow(leaseRentalCharges_Grid, field_values);
			initialFinalRentAmount();
			// validateFinancialGrid();
			rentalCharges_clearGridFields();
			var totalTenor = 0;
			for (var i = 0; i < leaseRentalCharges_Grid.getRowsNum(); i++) {
				totalTenor = totalTenor + parseInt(leaseRentalCharges_Grid.cells2(i, 1).getValue());
			}
			if (parseInt($('#tenor').val()) == totalTenor) {
				if (!isEmpty($('#intFromDate').val()) && !isEmpty($('#intUptoDate').val()))
					$('#intBrokenRentFormat').focus();
				else if (!isEmpty($('#finalFromDate').val()) && !isEmpty($('#finalUptoDate').val()))
					$('#finalBrokenRentFormat').focus();
				else {
					lease._setTabActive('lease_4', true);
					setFocusLast('grossNetTDS');
				}
				return true;
			} else if (!$('#uptoToner').prop('readonly'))
				$('#uptoToner').focus();
			else
				$('#leaseRentRate').focus();
			return true;
		} else {
			if (!$('#uptoToner').prop('readonly'))
				$('#uptoToner').focus();
			else
				$('#leaseRentRate').focus();
			return false;
		}
	}
}

function validateTenorSum(editMode, editRowId) {
	// var monthFreq = 0;
	// if ($('#repayFreq').val() == 'M')
	// monthFreq = 12;
	// else if ($('#repayFreq').val() == 'Q')
	// monthFreq = 4;
	// else if ($('#repayFreq').val() == 'H')
	// monthFreq = 6;
	// else if ($('#repayFreq').val() == 'Y')
	// monthFreq = 1;

	// if (leaseRentalCharges_Grid.getRowsNum() == 0) {
	// var interestNegative = (parseFloat($('#commencementValue').val()) *
	// parseFloat($('#internalRateReturn').val()) / 100) / parseInt(monthFreq);
	// var negCheck = parseFloat($('#rental').val()) -
	// parseFloat(interestNegative);
	// if (isNegativeAmount(negCheck)) {
	// setError('leaseRentRate_error', 'Cant generate the Financial Schedule
	// since Interest Amount exceeding Rental Amount');
	// return false;
	// }
	// }
	if (leaseRentalCharges_Grid.getRowsNum() > 0) {
		var totalTenor = 0;
		for (var i = 0; i < leaseRentalCharges_Grid.getRowsNum(); i++) {
			totalTenor = totalTenor + parseInt(leaseRentalCharges_Grid.cells2(i, 1).getValue());
		}
		var editRowValue = 0;
		if (editMode) {
			editRowValue = parseInt(leaseRentalCharges_Grid.cells(editRowId, 1).getValue());
		}
		totalTenor = totalTenor + parseInt($('#uptoToner').val()) - editRowValue;
		if (parseInt($('#tenor').val()) < totalTenor) {
			if (!$('#uptoToner').prop('readonly')) {
				setError('uptoToner_error', TOTAL_TENOR_GREATER_THAN_MONTH + $('#tenor').val());
				$('#uptoToner').focus();
				return false;
			} else {
				setError('leaseRentRate_error', TOTAL_TENOR_GREATER_THAN_MONTH + $('#tenor').val());
				$('#rentalFormat').focus();
				return false;
			}
		}
	}
	return true;
}

function leaseRentalCharges_editGrid(rowId) {
	rentalCharges_clearGridFields();
	$('#uptoToner').val(leaseRentalCharges_Grid.cells(rowId, 1).getValue());
	$('#leaseRentRate').val(leaseRentalCharges_Grid.cells(rowId, 2).getValue());
	$('#rentalFormat').val(leaseRentalCharges_Grid.cells(rowId, 3).getValue());
	$('#execChagsFormat').val(leaseRentalCharges_Grid.cells(rowId, 4).getValue());
	$('#rental').val(leaseRentalCharges_Grid.cells(rowId, 5).getValue());
	$('#execChags').val(leaseRentalCharges_Grid.cells(rowId, 6).getValue());
	if (!$('#uptoToner').prop('readonly'))
		$('#uptoToner').focus();
	else
		$('#leaseRentRate').focus();
}

function rentalCharges_clearGridFields() {
	$('#uptoToner').val(EMPTY_STRING);
	$('#uptoToner_error').html(EMPTY_STRING);
	$('#rental').val(EMPTY_STRING);
	$('#rentalFormat').val(EMPTY_STRING);
	$('#rental_error').html(EMPTY_STRING);
	$('#leaseRentRate').val(EMPTY_STRING);
	$('#leaseRentRate_error').html(EMPTY_STRING);
	$('#execChags').val(EMPTY_STRING);
	$('#execChagsFormat').val(EMPTY_STRING);
	$('#execChags_error').html(EMPTY_STRING);
}

function leaseRentalCharges_cancelGrid() {
	rentalCharges_clearGridFields();
}

function gridExit(id) {
	switch (id) {
	// TAB 2
	case 'noOfAssets':
		if (leaseNoOfAssets_Grid.getRowsNum() > 0) {
			if ($('#noOfAssets').val() != leaseNoOfAssets_Grid.getRowsNum()) {
				setError('noOfAssets_error', NO_OF_ASSET_SAME_OF_GRID_VALUES);
				lease._setTabActive('lease_1', true);
				$('#noOfAssets').focus();
				return false;
			}
			$('#xmlleaseNoOfAssetsGrid').val(leaseNoOfAssets_Grid.serialize());
			$('#noOfAssets_error').html(EMPTY_STRING);
			lease._setTabActive('lease_2', true);
			setFocusLast('tenor');
		} else {
			leaseNoOfAssets_Grid.clearAll();
			$('#xmlleaseNoOfAssetsGrid').val(leaseNoOfAssets_Grid.serialize());
			setError('noOfAssets_error', ADD_ATLEAST_ONE_ROW);
			setFocusLast('noOfAssets');
		}
		break;
	case 'assetCategory':
		$('#xmlleaseNoOfAssetsGrid').val(leaseNoOfAssets_Grid.serialize());
		$('#noOfAssets_error').html(EMPTY_STRING);
		setFocusLast('componentNew');
		break;
	case 'assetComponent':
		$('#xmlleaseNoOfAssetsGrid').val(leaseNoOfAssets_Grid.serialize());
		$('#noOfAssets_error').html(EMPTY_STRING);
		setFocusLast('componentNew');
		break;
	// TAB 2 POP UP
	case 'comAssetID':
		if (leaseAssetComponent_Grid.getRowsNum() > 0) {
			clearAssetComponentGridFields();
			setFocusLast('componentNew');
		} else {
			leaseAssetComponent_Grid.clearAll();
			setError('comAssetID_error', ADD_ATLEAST_ONE_ROW);
			setFocusLast('comAssetID');
		}
		break;
	case 'comAssetDesc':
		if (leaseAssetComponent_Grid.getRowsNum() > 0) {
			clearAssetComponentGridFields();
			setFocusLast('componentNew');
		} else {
			leaseAssetComponent_Grid.clearAll();
			setError('comAssetDesc_error', ADD_ATLEAST_ONE_ROW);
			setFocusLast('comAssetDesc');
		}
		break;
	case 'assetModel':
		if (leaseAssetComponent_Grid.getRowsNum() > 0) {
			clearAssetComponentGridFields();
			setFocusLast('componentNew');
		} else {
			leaseAssetComponent_Grid.clearAll();
			setError('assetModel_error', ADD_ATLEAST_ONE_ROW);
			setFocusLast('assetModel');
		}
		break;
	case 'assetConfig':
		if (leaseAssetComponent_Grid.getRowsNum() > 0) {
			clearAssetComponentGridFields();
			setFocusLast('componentNew');
		} else {
			leaseAssetComponent_Grid.clearAll();
			setError('assetConfig_error', ADD_ATLEAST_ONE_ROW);
			setFocusLast('assetConfig');
		}
		break;
	case 'comQty':
		if (leaseAssetComponent_Grid.getRowsNum() > 0) {
			clearAssetComponentGridFields();
			setFocusLast('componentNew');
		} else {
			leaseAssetComponent_Grid.clearAll();
			setError('comQty_error', ADD_ATLEAST_ONE_ROW);
			setFocusLast('comQty');
		}
		break;
	case 'comIndidualReq':
		if (leaseAssetComponent_Grid.getRowsNum() > 0) {
			clearAssetComponentGridFields();
			setFocusLast('componentNew');
		} else {
			leaseAssetComponent_Grid.clearAll();
			setError('comAssetID_error', ADD_ATLEAST_ONE_ROW);
			setFocusLast('comAssetID');
		}
		break;

	// TAB 4 RENTAL GRID
	case 'uptoToner':
		if (leaseRentalCharges_Grid.getRowsNum() > 0) {
			var totalTenor = 0;
			for (var i = 0; i < leaseRentalCharges_Grid.getRowsNum(); i++) {
				totalTenor = totalTenor + parseInt(leaseRentalCharges_Grid.cells2(i, 1).getValue());
			}
			if ($('#tenor').val() < totalTenor) {
				if (!$('#uptoToner').prop('readonly')) {
					setError('uptoToner_error', TOTAL_TENOR_CANT_LESS_TENOR_MONTH);
					$('#uptoToner').focus();
					return false;
				}
			}
			$('#xmlleaseRentalChargesGrid').val(leaseRentalCharges_Grid.serialize());
			rentalCharges_clearGridFields();
			validateTenorSum();
			setFocusLast('intBrokenRentFormat');
		} else {
			leaseRentalCharges_Grid.clearAll();
			setError('uptoToner_error', ADD_ATLEAST_ONE_ROW);
			setFocusLast('uptoToner');
		}
		break;
	case 'rentalFormat':
		if (leaseRentalCharges_Grid.getRowsNum() > 0) {
			var totalTenor = 0;
			for (var i = 0; i < leaseRentalCharges_Grid.getRowsNum(); i++) {
				totalTenor = totalTenor + parseInt(leaseRentalCharges_Grid.cells2(i, 1).getValue());
			}
			if ($('#tenor').val() < totalTenor) {
				if (!$('#uptoToner').prop('readonly')) {
					setError('rental_error', TOTAL_TENOR_CANT_LESS_TENOR_MONTH);
					$('#rentalFormat').focus();
					return false;
				}
			}
			$('#xmlleaseRentalChargesGrid').val(leaseRentalCharges_Grid.serialize());
			rentalCharges_clearGridFields();
			validateTenorSum();
			setFocusLast('intBrokenRentFormat');
		} else {
			leaseRentalCharges_Grid.clearAll();
			setError('uptoToner_error', ADD_ATLEAST_ONE_ROW);
			setFocusLast('uptoToner');
		}
		break;
	case 'leaseRentRate':
		if (leaseRentalCharges_Grid.getRowsNum() > 0) {
			var totalTenor = 0;
			for (var i = 0; i < leaseRentalCharges_Grid.getRowsNum(); i++) {
				totalTenor = totalTenor + parseInt(leaseRentalCharges_Grid.cells2(i, 1).getValue());
			}
			if ($('#tenor').val() < totalTenor) {
				if (!$('#uptoToner').prop('readonly')) {
					setError('leaseRentRate_error', TOTAL_TENOR_CANT_LESS_TENOR_MONTH);
					$('#leaseRentRate').focus();
					return false;
				}
			}
			$('#xmlleaseRentalChargesGrid').val(leaseRentalCharges_Grid.serialize());
			rentalCharges_clearGridFields();
			validateTenorSum();
			setFocusLast('intBrokenRentFormat');
		} else {
			leaseRentalCharges_Grid.clearAll();
			setError('uptoToner_error', ADD_ATLEAST_ONE_ROW);
			setFocusLast('uptoToner');
		}
		break;
	case 'execChagsFormat':
		if (leaseRentalCharges_Grid.getRowsNum() > 0) {
			var totalTenor = 0;
			for (var i = 0; i < leaseRentalCharges_Grid.getRowsNum(); i++) {
				totalTenor = totalTenor + parseInt(leaseRentalCharges_Grid.cells2(i, 1).getValue());
			}
			if ($('#tenor').val() < totalTenor) {
				if (!$('#uptoToner').prop('readonly')) {
					setError('execChagsFormat_error', TOTAL_TENOR_CANT_LESS_TENOR_MONTH);
					$('#execChags').focus();
					return false;
				}
			}
			$('#xmlleaseRentalChargesGrid').val(leaseRentalCharges_Grid.serialize());
			rentalCharges_clearGridFields();
			validateTenorSum();
			setFocusLast('intBrokenRentFormat');
		} else {
			leaseRentalCharges_Grid.clearAll();
			setError('uptoToner_error', ADD_ATLEAST_ONE_ROW);
			setFocusLast('uptoToner');
		}
		break;

	// TAB 5 FINANCIAL GRID
	case 'principalAmtFormat':
		$('#xmlleaseRentalChargesGrid').val(leaseRentalCharges_Grid.serialize());
		financialAmount();
		closePopup('0');
		break;
	case 'interestAmtFormat':
		$('#xmlleaseRentalChargesGrid').val(leaseRentalCharges_Grid.serialize());
		financialAmount();
		closePopup('0');
		break;

	// TAB 6 PAYMENT GRID

	case 'paymentUptoTenor':
		$('#xmlleasePaymentGrid').val(leasePaymentDetails_Grid.serialize());
		$('#paymentUptoTenor').val(EMPTY_STRING);
		clearRepaymentGrid();
		closePopup('0');
		setFocusLast('processCode');
		break;
	case 'repaymentMode':
		$('#xmlleasePaymentGrid').val(leasePaymentDetails_Grid.serialize());
		$('#paymentUptoTenor').val(EMPTY_STRING);
		clearRepaymentGrid();
		closePopup('0');
		setFocusLast('processCode');
		break;
	case 'ecsIFSC':
		$('#xmlleasePaymentGrid').val(leasePaymentDetails_Grid.serialize());
		$('#paymentUptoTenor').val(EMPTY_STRING);
		clearRepaymentGrid();
		closePopup('0');
		setFocusLast('processCode');
		break;
	case 'ecsBank':
		$('#xmlleasePaymentGrid').val(leasePaymentDetails_Grid.serialize());
		$('#paymentUptoTenor').val(EMPTY_STRING);
		clearRepaymentGrid();
		closePopup('0');
		setFocusLast('processCode');
		break;
	case 'ecsBranch':
		$('#xmlleasePaymentGrid').val(leasePaymentDetails_Grid.serialize());
		$('#paymentUptoTenor').val(EMPTY_STRING);
		clearRepaymentGrid();
		closePopup('0');
		setFocusLast('processCode');
		break;
	case 'cusAccNumber':
		$('#xmlleasePaymentGrid').val(leasePaymentDetails_Grid.serialize());
		$('#paymentUptoTenor').val(EMPTY_STRING);
		clearRepaymentGrid();
		closePopup('0');
		setFocusLast('processCode');
		break;
	case 'peakAmountFormat':
		$('#xmlleasePaymentGrid').val(leasePaymentDetails_Grid.serialize());
		$('#paymentUptoTenor').val(EMPTY_STRING);
		clearRepaymentGrid();
		closePopup('0');
		setFocusLast('processCode');
		break;
	case 'ecsRefNo':
		$('#xmlleasePaymentGrid').val(leasePaymentDetails_Grid.serialize());
		$('#paymentUptoTenor').val(EMPTY_STRING);
		clearRepaymentGrid();
		closePopup('0');
		setFocusLast('processCode');
		break;
	case 'ecsRefDate':
		$('#xmlleasePaymentGrid').val(leasePaymentDetails_Grid.serialize());
		$('#paymentUptoTenor').val(EMPTY_STRING);
		clearRepaymentGrid();
		closePopup('0');
		setFocusLast('processCode');
		break;
	}
}

function generateScheduleData() {
	if (!validateScheduleDependFields()) {
		alert(SCHEDULE_GENERATE_NA);
		return false;
	}
	hideUnhide = false;
	showMessage('eleaseFinanceGrid', Message.PROGRESS);
	loadFinancialGrid(true);
	lease._setTabActive('lease_4', true);
}

// TAB 6 GRID FINANCIAL
function loadFinancialGrid(valMode) {
	financialScheduleNonOLGrid.clearAll();
	financialScheduleOLGrid.clearAll();
	leaseApplicableTax_Grid.clearAll();
	$('#xmlleaseRentalChargesGrid').val(leaseRentalCharges_Grid.serialize());
	$('#noOfslab').html(EMPTY_STRING);
	$('#valueNegative').html(EMPTY_STRING);
	showMessage(getProgramID(), Message.PROGRESS);
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('MAIN_SL', $('#spSerial').val());
	if (!isEmpty($('#internalRateReturn').val()))
		validator.setValue('IRR_VALUE', $('#internalRateReturn').val());
	else
		validator.setValue('IRR_VALUE', '0');
	validator.setValue('TENOR_MONTH', $('#tenor').val());
	validator.setValue('ADVANCE_ARREARS_FLAG', $('#advArrers').val());
	validator.setValue('COMMENCEMENT_VALUE', $('#commencementValue').val());
	validator.setValue('LEAVE_TYPE', $('#leaseProductType').val());

	validator.setValue('BILL_FROM_DATE', $('#rentDate').val());
	validator.setValue('BILL_UPTO_DATE', $('#lastDate').val());
	validator.setValue('MONTH_FREQ', $('#repayFreq').val());
	validator.setValue('TENOR_MONTH', $('#tenor').val());
	validator.setValue('XML_RENT_AMT', $('#xmlleaseRentalChargesGrid').val());
	validator.setValue('EDIT_REQ', $('#leaseProductType').val());

	validator.setValue('ENTITY_CODE', getEntityCode());
	validator.setValue('STATE_CODE', $('#billState').val());
	validator.setValue('SHIP_STATE_CODE', $('#shippingState').val());
	validator.setValue('TAX_SCHEDULE_CODE', $('#taxScheduleCode').val());
	validator.setValue('TAX_CCY', $('#assetCurrency').val());
	validator.setValue('TAX_ON_DATE', getCBD());

	if ($('#action').val() == MODIFY) {
		validator.setValue('LESSEE_CODE', $('#custLeaseCode').val());
		validator.setValue('AGREEMENT_NO', $('#agreementNo').val());
		validator.setValue('SCHEDULE_ID', $('#scheduleID').val());
		validator.setValue('LEASE_PRODUCT_CODE', $('#leaseProduct').val());
		validator.setValue('CUSTOMER_ID', $('#custID').val());
	} else {
		validator.setValue('LESSEE_CODE', ZERO);
		validator.setValue('AGREEMENT_NO', ZERO);
		validator.setValue('SCHEDULE_ID', ZERO);
		validator.setValue('LEASE_PRODUCT_CODE', $('#leaseProduct').val());
		validator.setValue('CUSTOMER_ID', ZERO);
	}
	validator.setValue(ACTION, $('#action').val());
	validator.setClass('patterns.config.web.forms.lss.eleasebean');
	validator.setMethod('getFinancialScheduleDetails');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		hide('showFinancialGrid');
		hide('closeFinGrid');
		hide('agreeAndContinue');
		// setError('leaseRentRate_error', validator.getValue(ERROR));
		alert(validator.getValue(ERROR));
		hideMessage(getProgramID());
		return false;
	} else {
		win = showWindow('showFinancialGrid', EMPTY_STRING, 'Financial and Tax Details', EMPTY_STRING, true, false, false, 1330, 635);
		if (win)
			win.showInnerScroll();
		leaseFinance._setTabActive('leaseFinance_0', true);
		if (valMode) {
			show('closeFinGrid');
			hide('agreeAndContinue');
		} else {
			hide('closeFinGrid');
			show('agreeAndContinue');
		}
		$('#noOfslab').append("<div>No. of Slabs    :<span id ='totalValues' x-large;'></span></div>");
		if ('1' == validator.getValue("NEGATIVE_CHECK")) {
			$('#valueNegative').append(PRINCIPAL_SHOULD_NOT_NEGATIVE);
			$('#iAgree').prop('disabled', true);
		} else {
			$('#valueNegative').append(EMPTY_STRING);
			$('#iAgree').prop('disabled', false);
		}

		if ($('#leaseProductType').val() == 'O') {
			hide('leaseFinanceSchedule');
			show('leaseFinanceRentalSchedule');
			financialScheduleOLGrid.loadXMLString(validator.getValue("FINANCIAL_XML"));
			leaseApplicableTax_Grid.loadXMLString(validator.getValue("TAX_WISE_XML"));
			$("#totalValues").html(financialScheduleOLGrid.getRowsNum());
			financialScheduleOLGrid.setColumnHidden(1, true);
			financialScheduleOLGrid.setColumnHidden(4, true);
			financialScheduleOLGrid.setColumnHidden(5, true);
			$('#executoryAmt1').html(validator.getValue("TOTAL_EXEC_TAX"));
			$('#rentalAmt1').html(validator.getValue("TOTAL_RENTAL_TAX"));
			$('#stateAmtFormat1').html(validator.getValue("TOTAL_STATE_TAX_FORMAT"));
			$('#centralAmtFormat1').html(validator.getValue("TOTAL_CENTRAL_TAX_FORMAT"));
			$('#serviceAmtFormat1').html(validator.getValue("TOTAL_SERVICE_TAX_FORMAT"));
			$('#cessesAmtFormat1').html(validator.getValue("TOTAL_CESS_TAX_FORMAT"));
			$('#totalAmtFormat1').html(validator.getValue("TOTAL_VALUE_FORMAT"));

			$('#stateAmt1').html(validator.getValue("TOTAL_STATE_TAX"));
			$('#centralAmt1').html(validator.getValue("TOTAL_CENTRAL_TAX"));
			$('#serviceAmt1').html(validator.getValue("TOTAL_SERVICE_TAX"));
			$('#cessesAmt1').html(validator.getValue("TOTAL_CESS_TAX"));
			$('#totalAmt1').html(validator.getValue("TOTAL_VALUE"));

			$('#pincipalAmt1').html('0');
			$('#interAmt1').html('0');
		} else {
			show('leaseFinanceSchedule');
			hide('leaseFinanceRentalSchedule');
			// financialScheduleNonOLGrid.enableBlockSelection(true);
			financialScheduleNonOLGrid.loadXMLString(validator.getValue("FINANCIAL_XML"));
			leaseApplicableTax_Grid.loadXMLString(validator.getValue("TAX_WISE_XML"));
			$("#totalValues").html(financialScheduleNonOLGrid.getRowsNum());
			$('#executoryAmt').html(validator.getValue("TOTAL_EXEC_TAX"));
			$('#rentalAmt').html(validator.getValue("TOTAL_RENTAL_TAX"));
			$('#stateAmtFormat').html(validator.getValue("TOTAL_STATE_TAX_FORMAT"));
			$('#centralAmtFormat').html(validator.getValue("TOTAL_CENTRAL_TAX_FORMAT"));
			$('#serviceAmtFormat').html(validator.getValue("TOTAL_SERVICE_TAX_FORMAT"));
			$('#cessesAmtFormat').html(validator.getValue("TOTAL_CESS_TAX_FORMAT"));
			$('#totalAmtFormat').html(validator.getValue("TOTAL_VALUE_FORMAT"));

			$('#stateAmt').html(validator.getValue("TOTAL_STATE_TAX"));
			$('#centralAmt').html(validator.getValue("TOTAL_CENTRAL_TAX"));
			$('#serviceAmt').html(validator.getValue("TOTAL_SERVICE_TAX"));
			$('#cessesAmt').html(validator.getValue("TOTAL_CESS_TAX"));
			$('#totalAmt').html(validator.getValue("TOTAL_VALUE"));

			// $('#pincipalAmt').html('0');
			// $('#interAmt').html('0');
		}

		// if (financialScheduleNonOLGrid.getRowsNum() > 0 ||
		// financialScheduleOLGrid.getRowsNum() > 0) {
		// // var allSl = [];
		// // if ($('#leaseProductType').val() == 'O') {
		// // for (var i = 0; i < financialScheduleOLGrid.getRowsNum();
		// // i++) {
		// // lastValue = parseInt(financialScheduleOLGrid.cells2(i,
		// // 23).getValue());
		// // if (jQuery.inArray(lastValue, allSl) == -1)
		// // allSl.push(lastValue);
		// // }
		// // } else {
		// // for (var i = 0; i < financialScheduleNonOLGrid.getRowsNum(); i++)
		// // {
		// // lastValue = parseInt(financialScheduleNonOLGrid.cells2(i,
		// // 23).getValue());
		// // if (jQuery.inArray(lastValue, allSl) == -1)
		// // allSl.push(lastValue);
		// // }
		// // }
		// // $('#grossTaxAmount').html(EMPTY_STRING);
		// clearError('taxScheduleCode_error');
		// // leaseApplicableTax_Grid.clearAll();
		// validator.clearMap();
		// validator.setMtm(false);
		// validator.setValue('ENTITY_CODE', getEntityCode());
		// validator.setValue("TOTAL_SL", $('#spSerial').val());
		// // if (allSl.length != 0)
		// // validator.setValue("TOTAL_SL", allSl);
		// validator.setValue(ACTION, USAGE);
		// validator.setClass('patterns.config.web.forms.lss.eleasebean');
		// validator.setMethod('getTaxDetails');
		// validator.sendAndReceive();
		// if (validator.getValue(ERROR) != EMPTY_STRING) {
		// // leaseApplicableTax_Grid.clearAll();
		// // $('#grossTaxAmount').html(EMPTY_STRING);
		// alert(validator.getValue(ERROR));
		// hideMessage(getProgramID());
		// // return false;
		// } else {
		// leaseApplicableTax_Grid.clearAll();
		// leaseApplicableTax_Grid.loadXMLString(validator.getValue("TAX_XML"));
		// // $('#grossTaxAmount').html(validator.getValue("GROSS_TAX_AMOUNT"));
		// // taxTotal = validator.getValue("GROSS_TAX_AMOUNT");
		// }
		// }
		hideMessage(getProgramID());
	}
	return true;
}

// TAB 2 BEGIN

function noOfAssets_val(valMode) {
	var value = $('#noOfAssets').val();
	clearError('noOfAssets_error');
	if (isEmpty(value)) {
		setError('noOfAssets_error', MANDATORY);
		return false;
	}
	if (!isNumeric(value)) {
		setError('noOfAssets_error', HMS_INVALID_NUMBER);
		return false;
	}
	if (isZero(value)) {
		setError('noOfAssets_error', ZERO_CHECK);
		return false;
	}
	if (!isPositive(value)) {
		setError('noOfAssets_error', POSITIVE_CHECK);
		return false;
	}
	$('#noOfAssets').val(parseInt(value));
	if (valMode) {
		if (leaseNoOfAssets_Grid.getRowsNum() == parseInt(value)) {
			doTabbarClick('lease_2');
			lease._setTabActive('lease_2', true);
			setFocusLast('tenor');
		} else {
			setFocusLast('addAsset');
		}
	}
	return true;
}

// POP UP asset component
function clearAssetComponentPopUpFields() {
	$('#assetSl').val(EMPTY_STRING);
	$('#assetCategory').val(EMPTY_STRING);
	$('#assetCategory_desc').html(EMPTY_STRING);
	$('#assetCategory_error').html(EMPTY_STRING);
	$('#autoNonAuto').val(EMPTY_STRING);
	$('#assetComponent').val(EMPTY_STRING);
	$('#assetComponent_error').html(EMPTY_STRING);
	leaseAssetComponent_Grid.clearAll();
	clearAssetComponentGridFields();
}

function clearAssetComponentGridFields() {
	$('#componentSerial').val(EMPTY_STRING);
	$('#comAssetID').val(EMPTY_STRING);
	$('#comAssetID_error').html(EMPTY_STRING);
	$('#comAssetDesc').val(EMPTY_STRING);
	$('#comAssetDesc_error').html(EMPTY_STRING);
	$('#comQty').val(EMPTY_STRING);
	$('#comQty_error').html(EMPTY_STRING);
	$('#assetModel').val(EMPTY_STRING);
	$('#assetModel_error').html(EMPTY_STRING);
	$('#assetConfig').val(EMPTY_STRING);
	$('#assetConfig_error').html(EMPTY_STRING);
	setCheckbox('comIndidualReq', NO);
}

function assetCategory_val() {
	var value = $('#assetCategory').val();
	clearError('assetCategory_error');
	$('#assetCategory_desc').html(EMPTY_STRING);
	if (isEmpty(value)) {
		setError('assetCategory_error', MANDATORY);
		return false;
	}
	if (isEmpty($('#billState').val())) {
		setError('assetCategory_error', STATE_MANDATORY);
		return false;
	}
	if (!noOfAssetDetails_gridDuplicateCheck()) {
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('ASSET_TYPE_CODE', $('#assetCategory').val());
	validator.setValue('STATE_CODE', $('#billState').val());
	validator.setValue(ACTION, USAGE);
	validator.setClass('patterns.config.web.forms.lss.eleasebean');
	validator.setMethod('validateAssetCategory');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('assetCategory_error', validator.getValue(ERROR));
		$('#assetCategory_desc').html(EMPTY_STRING);
		return false;
	} else {
		if (leaseNoOfAssets_Grid.getRowsNum() > 0) {
			var prevScheduleCode = leaseNoOfAssets_Grid.cells2(0, 9).getValue();
			if (!isEmpty(prevScheduleCode)) {
				if (prevScheduleCode != validator.getValue('TAX_SCHEDULE_CODE')) {
					setError('assetCategory_error', ASSET_SCHEDULE_SHOULD_SAME);
					return false;
				}
			}
		}
		$('#assetCategory_desc').html(validator.getValue('DESCRIPTION'));
		$('#autoNonAuto').val(validator.getValue('AUTO_NON_AUTO'));
		$('#taxScheduleCode').val(validator.getValue('TAX_SCHEDULE_CODE'));
		assetRequired = validator.getValue('MODEL_NO');
		configRequired = validator.getValue('CONFIG_DTLS');
		if (assetRequired == 'N')
			$('#assetModel').prop('readonly', true);
		else
			$('#assetModel').prop('readonly', false);

		if (configRequired == 'N')
			$('#assetConfig').prop('readonly', true);
		else
			$('#assetConfig').prop('readonly', false);
	}
	setFocusLast('assetComponent');
	return true;
}

function assetComponent_val() {
	var value = $('#assetComponent').val();
	clearError('assetComponent_error');
	if (isEmpty(value)) {
		setError('assetComponent_error', MANDATORY);
		$('#componentSerial').val(EMPTY_STRING);
		return false;
	}
	if (!isNumeric(value)) {
		setError('assetComponent_error', HMS_INVALID_NUMBER);
		$('#componentSerial').val(EMPTY_STRING);
		return false;
	}
	if (!isPositive(value)) {
		setError('assetComponent_error', POSITIVE_CHECK);
		return false;
	}
	$('#assetComponent').val(parseInt(value));
	if (leaseAssetComponent_Grid.getRowsNum() > 0) {
		var count = leaseAssetComponent_Grid.getRowsNum() + 1;
		$('#componentSerial').val(count);
	} else
		$('#componentSerial').val('1');
	setFocusLast('comAssetID');
	return true;
}

function componentSerial_val() {
	var value = $('#componentSerial').val();
	clearError('componentSerial_error');
	if (isEmpty(value)) {
		setError('componentSerial_error', MANDATORY);
		return false;
	}
	if (!isNumeric(value)) {
		setError('componentSerial_error', HMS_INVALID_NUMBER);
		return false;
	}
	if (!isPositive(value)) {
		setError('componentSerial_error', POSITIVE_CHECK);
		return false;
	}
	$('#componentSerial').val(parseInt(value));
	setFocusLast('comAssetID');
	return true;
}

function comAssetID_val() {
	var value = $('#comAssetID').val();
	clearError('comAssetID_error');
	if (isEmpty(leaseAssetComponent_Grid.getUserData("", "grid_edit_id")) && $('#assetComponent').val() == leaseAssetComponent_Grid.getRowsNum()) {
		setError('comAssetID_error', ASSET_COMPONENT_QTY_SAME);
		return false;
	}
	if (!isEmpty(value)) {
		if (isZero(value) || isWhitespace(value)) {
			setError('comAssetID_error', ZERO_BLANK_NOT_ALLOWED);
			return false;
		}
		if (!isValidAccountNumber(value)) {
			setError('comAssetID_error', HMS_INVALID_NUMBER);
			return false;
		}
	}
	setFocusLast('comAssetDesc');
	return true;
}

function comAssetDesc_val() {
	var value = $('#comAssetDesc').val();
	clearError('comAssetDesc_error');
	if (isEmpty(value)) {
		setError('comAssetDesc_error', MANDATORY);
		return false;
	}
	if (!isValidRemarks(value)) {
		setError('comAssetDesc_error', INVALID_DESCRIPTION);
		return false;
	}
	setFocusLast('comQty');
	return true;
}

function assetModel_val() {
	var value = $('#assetModel').val();
	clearError('assetModel_error');
	if (assetRequired == 'M') {
		if (isEmpty(value)) {
			setError('assetModel_error', MANDATORY);
			return false;
		}
	}
	if (!isEmpty(value)) {
		if (!isValidOtherInformation25(value)) {
			setError('assetModel_error', HMS_INVALID_VALUE);
			return false;
		}
	}
	if (!$('#assetConfig').prop('readonly'))
		setFocusLast('assetConfig');
	else
		setFocusLastOnGridSubmit('leaseAssetComponent_Grid');
	return true;
}

function assetConfig_val() {
	var value = $('#assetConfig').val();
	clearError('assetConfig_error');
	if (configRequired == 'M') {
		if (isEmpty(value)) {
			setError('assetConfig_error', MANDATORY);
			return false;
		}
	}
	if (!isEmpty(value)) {
		if (!isValidOtherInformation25(value)) {
			setError('assetConfig_error', HMS_INVALID_VALUE);
			return false;
		}
	}
	setFocusLastOnGridSubmit('leaseAssetComponent_Grid');
	return true;
}

function comIndidualReq_val() {
	if (!$('#assetModel').prop('readonly'))
		setFocusLast('assetModel');
	else if (!$('#assetConfig').prop('readonly'))
		setFocusLast('assetConfig');
	else
		setFocusLastOnGridSubmit('leaseAssetComponent_Grid');
}

function comQty_val() {
	var value = $('#comQty').val();
	clearError('comQty_error');
	if (isEmpty(value)) {
		$('#comQty').val(ONE);
		value = $('#comQty').val();
	}
	if (isZero(value)) {
		setError('comQty_error', ZERO_CHECK);
		return false;
	}
	if (!isNumeric(value)) {
		setError('comQty_error', HMS_INVALID_NUMBER);
		return false;
	}
	if (!isPositive(value)) {
		setError('comQty_error', POSITIVE_CHECK);
		return false;
	}
	$('#comQty').val(parseInt(value));
	setFocusLast('comIndidualReq');
	return true;
}

function assetSl_gridDuplicateCheck(editMode, editRowId) {
	if (!isEmpty($('#comAssetID').val())) {
		var componentSerial = [ $('#comAssetID').val() ];
		return _grid_duplicate_check(leaseAssetComponent_Grid, componentSerial, [ 2 ]);
	} else
		return true;
}

function leaseAssetComponent_addGrid(editMode, editRowId) {
	if (!editMode && leaseAssetComponent_Grid.getRowsNum() > $('#assetComponent').val()) {
		setError('comAssetID_error', NO_OF_COMPONENT_CANT_GREATER_THAN);
		return false;
	} else if (componentSerial_val() && comAssetID_val() && comAssetDesc_val() && comQty_val() && assetConfig_val() && assetModel_val()) {
		if (assetSl_gridDuplicateCheck(editMode, editRowId)) {
			var checkVal;
			if ($('#comIndidualReq').is(':checked'))
				checkVal = 'Yes';
			else
				checkVal = 'No';
			var field_values = [ 'false', $('#componentSerial').val(), $('#comAssetID').val(), $('#comAssetDesc').val(), $('#comQty').val(), checkVal, decodeBTS($('#comIndidualReq').is(':checked')), $('#assetModel').val(), $('#assetConfig').val() ];
			_grid_updateRow(leaseAssetComponent_Grid, field_values);
			clearAssetComponentGridFields();
			comoonentSlSeqence();
			return true;
		} else {
			setError('comAssetID_error', HMS_DUPLICATE_DATA);
			$('#comAssetID').focus();
			return false;
		}
	} else {
		$('#comAssetID').focus();
		return false;
	}
}

function comoonentSlSeqence() {
	var componentSl;
	ros = leaseAssetComponent_Grid.getRowsNum();
	var noOfVal = $('#assetComponent').val();
	if (noOfVal < ros) {
		setError('comAssetID_error', NO_OF_COMPONENT_CANT_GREATER_THAN);
		return false;
	} else if (noOfVal >= ros) {
		componentSl = parseInt(ros) + 1;
		$('#componentSerial').val(componentSl);
		$('#comAssetID').focus();
		if (noOfVal == ros) {
			$('#componentSerial').val(EMPTY_STRING);
			$('#componentNew').focus();
		}
		return true;
	}
}

function leaseAssetComponent_editGrid(rowId) {
	$('#componentSerial').val(leaseAssetComponent_Grid.cells(rowId, 1).getValue());
	$('#comAssetID').val(leaseAssetComponent_Grid.cells(rowId, 2).getValue());
	$('#comAssetDesc').val(leaseAssetComponent_Grid.cells(rowId, 3).getValue());
	$('#comQty').val(leaseAssetComponent_Grid.cells(rowId, 4).getValue());
	if (leaseAssetComponent_Grid.cells(rowId, 5).getValue() == 'Yes')
		setCheckbox('comIndidualReq', YES);
	else
		setCheckbox('comIndidualReq', NO);
	$('#assetModel').val(leaseAssetComponent_Grid.cells(rowId, 6).getValue());
	$('#assetConfig').val(leaseAssetComponent_Grid.cells(rowId, 7).getValue());
	$('#comAssetID').focus();
}

function leaseAssetComponent_cancelGrid() {
	clearAssetComponentGridFields();
	comoonentSlSeqence();
}

function gridDeleteRow(id) {
	switch (id) {
	case 'componentSerial':
		deleteGridRecord(leaseAssetComponent_Grid);
		break;
	case 'uptoToner':
		deleteGridRecord(leaseRentalCharges_Grid);
		if ($('#dealType').val() == 'U') {
			if (leaseRentalCharges_Grid.getRowsNum() == 0) {
				$('#uptoToner').val($('#tenor').val());
				setFocusLast('leaseRentRate');
			}
		} else
			setFocusLast('leaseRentRate');
		break;
	}
}

function addAssetDetails() {
	if (noOfAssets_val(false)) {
		clearAssetComponentPopUpFields();
		if (leaseNoOfAssets_Grid.getRowsNum() == parseInt($('#noOfAssets').val())) {
			alert(ASSET_SUM_GRID_COUNT_SAME);
		} else if (assetSlSeqence()) {
			win = EMPTY_STRING;
			$('#assetComponent').val('1');
			win = showWindow('noOfAssetReq', EMPTY_STRING, ASSET_DETAILS, EMPTY_STRING, true, false, false, 1150, 610);
			$('#assetCategory').focus();
		}
	}
}

function clearAssetDetails() {
	leaseNoOfAssets_Grid.clearAll();
	leaseNoOfAssets_Grid.setUserData("", "grid_edit_id", EMPTY_STRING);
	setFocusLast('noOfAssets');
}

function assetSlSeqence() {
	leaseNoOfAssets_Grid.setUserData("", "grid_edit_id", EMPTY_STRING);
	var assetSl;
	ros = leaseNoOfAssets_Grid.getRowsNum();
	var noOfVal = $('#noOfAssets').val();
	if (noOfVal < ros) {
		setError('assetCategory_error', NO_OF_ASSET_GREATER_GRID_VALUE);
		return false;
	} else if (noOfVal >= ros) {
		assetSl = parseInt(ros) + 1;
		$('#assetSl').val(assetSl);
		return true;
	}
}

// NO of asset pop component based on asset grid
function clearPopupDetailsGrid() {
	clearAssetComponentPopUpFields();
	assetSlSeqence();
	leaseNoOfAssets_Grid.setUserData("", "grid_edit_id", EMPTY_STRING);
	setFocusLast('assetCategory');
}

function updateNewComponentDetails() {
	if (leaseAssetComponent_Grid.getRowsNum() != $('#assetComponent').val()) {
		setError('assetComponent_error', NO_OF_ASSET_COMPONENT_SAME);
		return false;
	}
	if (noOfAssetDetails_gridDuplicateCheck() && assetGridrevalidate()) {
		var linkValue = "<a href='javascript:void(0);' onclick=eleaseAssets_editGrid('" + $('#assetSl').val() + "')><i title='Remove from Grid' id='removeAsset' data-toggle='tooltip' class='fa fa-edit' style='margin-right: 5px;font-size: 21px; color: #3da0e3; cursor: pointer'></i></a><span> </span> <a href='javascript:void(0);' onclick=deleteGridRow('" + $('#assetSl').val()
				+ "');><i class='fa fa-remove' style='margin-left: -3px;font-size: 21px; color: #3da0e3; cursor: pointer'></i></a>";
		var xmlFiles = leaseAssetComponent_Grid.serialize();
		$('#xmlleaseAssetComponentGrid').val(leaseAssetComponent_Grid.serialize());
		var field_values = [ '', linkValue, $('#assetSl').val(), $('#assetCategory').val(), $('#assetCategory_desc').html(), $('#autoNonAuto option:selected').text(), $('#assetComponent').val(), xmlFiles, $('#autoNonAuto').val(), $('#taxScheduleCode').val() ];
		var uid = _grid_updateRow(leaseNoOfAssets_Grid, field_values);
		leaseNoOfAssets_Grid.changeRowId(uid, $('#assetSl').val());
		clearAssetComponentPopUpFields();
		assetSlSeqence();
		closeInlinePopUp(win);
		setFocusLast('noOfAssets');
		return true;
	}
}

function updateAndNewDetails() {
	if (leaseAssetComponent_Grid.getRowsNum() != $('#assetComponent').val()) {
		setError('assetComponent_error', NO_OF_ASSET_COMPONENT_SAME);
		return false;
	}
	if (noOfAssetDetails_gridDuplicateCheck() && assetGridrevalidate()) {
		var linkValue = "<a href='javascript:void(0);' onclick=eleaseAssets_editGrid('" + $('#assetSl').val() + "')><i title='Remove from Grid' id='removeAsset' data-toggle='tooltip' class='fa fa-edit' style='margin-right: 5px; color: #3da0e3;font-size: 21px; cursor: pointer'></i></a><span> </span> <a href='javascript:void(0);' onclick=deleteGridRow('" + $('#assetSl').val()
				+ "');><i class='fa fa-remove' style='margin-left: -3px;font-size: 21px; color: #3da0e3; cursor: pointer'></i></a>";
		var xmlFiles = leaseAssetComponent_Grid.serialize();
		$('#xmlleaseAssetComponentGrid').val(leaseAssetComponent_Grid.serialize());
		var field_values = [ '', linkValue, $('#assetSl').val(), $('#assetCategory').val(), $('#assetCategory_desc').html(), $('#autoNonAuto option:selected').text(), $('#assetComponent').val(), xmlFiles, $('#autoNonAuto').val(), $('#taxScheduleCode').val() ];
		var uid = _grid_updateRow(leaseNoOfAssets_Grid, field_values);
		leaseNoOfAssets_Grid.changeRowId(uid, $('#assetSl').val());
		clearAssetComponentPopUpFields();
		if (leaseNoOfAssets_Grid.getRowsNum() == $('#noOfAssets').val()) {
			alert(ASSET_SUM_GRID_COUNT_SAME);
			setFocusLast('noOfAssets');
			closeInlinePopUp(win);
		}
		assetSlSeqence();
		setFocusLast('assetCategory');
		return true;
	}
}

function noOfAssetDetails_gridDuplicateCheck() {
	var currentValue = [ $('#assetCategory').val() ];
	if (!_grid_duplicate_check(leaseNoOfAssets_Grid, currentValue, [ 3 ])) {
		setError('assetCategory_error', DUPLICATE_DATA);
		return false;
	}
	return true;
}

function deleteGridRow(row) {
	if (confirm(DELETE_CONFIRM)) {
		leaseNoOfAssets_Grid.setUserData("", "grid_edit_id", EMPTY_STRING);
		leaseNoOfAssets_Grid.deleteRow(row);
		// clearAssetComponentPopUpFields();
		setFocusLast('noOfAssets');
	}
}

function eleaseAssets_editGrid(rowId) {
	clearAssetComponentPopUpFields();
	win = EMPTY_STRING;
	win = showWindow('noOfAssetReq', EMPTY_STRING, ASSET_DETAILS, EMPTY_STRING, true, false, false, 1150, 610);
	leaseNoOfAssets_Grid.setUserData("", "grid_edit_id", rowId);
	$('#assetSl').val(leaseNoOfAssets_Grid.cells(rowId, 2).getValue());
	$('#assetCategory').val(leaseNoOfAssets_Grid.cells(rowId, 3).getValue());
	$('#assetCategory_desc').html(leaseNoOfAssets_Grid.cells(rowId, 4).getValue());
	$('#autoNonAuto').val(leaseNoOfAssets_Grid.cells(rowId, 8).getValue());
	$('#assetComponent').val(leaseNoOfAssets_Grid.cells(rowId, 6).getValue());
	leaseAssetComponent_Grid.loadXMLString(leaseNoOfAssets_Grid.cells(rowId, 7).getValue());
	setFocusLast('assetCategory');
}

function onloadInlinePopUp(param, id) {
	switch (id) {
	case 'assetSl':
		clearAssetComponentPopUpFields();
		win = EMPTY_STRING;
		win = showWindow('noOfAssetReq', EMPTY_STRING, ASSET_DETAILS, EMPTY_STRING, true, false, false, 1150, 610);
		setFocusLast('assetCategory');
		break;
	}
}

function assetGridrevalidate() {
	var errors = 0;
	if (!assetCategory_val(false)) {
		errors++;
	}
	if (!assetComponent_val(false)) {
		errors++;
	}
	if (leaseAssetComponent_Grid.getRowsNum() > 0) {
		$('#xmlleaseAssetComponentGrid').val(leaseAssetComponent_Grid.serialize());
		$('#comAssetID_error').html(EMPTY_STRING);
	} else {
		leaseAssetComponent_Grid.clearAll();
		$('#xmlleaseAssetComponentGrid').val(leaseAssetComponent_Grid.serialize());
		setError('comAssetID_error', HMS_ADD_ATLEAST_ONE_ROW);
		errors++;
	}
	if (errors > 0) {
		if (win)
			win.showInnerScroll();
		return false;
	} else
		return true;
}

function closePopup(value) {
	if (value == 0) {
		// afterClosePopUp(win);
		if (win._idd == 'undefined' || win._idd == null)
			win = hideWin;
		closeInlinePopUp(win);
		leaseNoOfAssets_Grid.setUserData("", "grid_edit_id", EMPTY_STRING);
		reValNotReq = false;
	}
	return true;
}

function agreeContinue() {
	if (!$('#iAgree').is(':checked')) {
		alert("You must check the acknowledgement");
		return false;
	} else {
		if (financialScheduleNonOLGrid.getRowsNum() == 0 && financialScheduleOLGrid.getRowsNum() == 0) {
			financialScheduleNonOLGrid.clearAll();
			financialScheduleOLGrid.clearAll();
			$('#xmlleaseFinanceScheduleGrid').val(financialScheduleNonOLGrid.serialize());
			$('#xmlleaseFinanceScheduleGrid').val(financialScheduleOLGrid.serialize());
			alert(HMS_ADD_ATLEAST_ONE_ROW);
			errors++;
		} else {
			// generateScheduleData();
			if (!$('#leaseFinanceRentalSchedule').hasClass('hidden')) {
				financialScheduleOLGrid.setSerializationLevel(false, false, false, false, false, true);
				$('#xmlleaseFinanceScheduleGrid').val(financialScheduleOLGrid.serialize());
			} else if (!$('#leaseFinanceSchedule').hasClass('hidden')) {
				financialScheduleNonOLGrid.setSerializationLevel(false, false, false, false, false, true);
				$('#xmlleaseFinanceScheduleGrid').val(financialScheduleNonOLGrid.serialize());
			}
			$('#leaseErrors_error').html(EMPTY_STRING);
			hide('showFinancialGrid');
			hide('closeFinGrid');
			hide('agreeAndContinue');
		}
		reValNotReq = true;
		$('#cmdSubmit').click();
	}
}

function iAgree_val() {
	if ($('#iAgree').is(':checked')) {
		$('#iAgreeContinue').prop('disabled', false);
		setFocusLast('iAgreeContinue');
	} else {
		$('#iAgreeContinue').prop('disabled', true);
	}
	return true;
}

function revalidateSchedule() {
	if (leaseNoOfAssets_Grid.getRowsNum() == 0) {
		setError('noOfAssets_error', ADD_ATLEAST_ONE_ROW);
		$('#lease').height('385px');
		lease._setTabActive('lease_1', true);
		return false;
	}

	var noofAssets = [];
	for (var i = 0; i < leaseNoOfAssets_Grid.getRowsNum(); i++) {
		assetValue = leaseNoOfAssets_Grid.cells2(i, 3).getValue();
		noofAssets.push(assetValue);
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('ENTITY_CODE', getEntityCode());
	validator.setValue('TOTAL_ASSETS', noofAssets);
	validator.setValue('STATE_CODE', $('#billState').val());
	validator.setValue(ACTION, USAGE);
	validator.setClass('patterns.config.web.forms.lss.eleasebean');
	validator.setMethod('validateScheduleDuplicateCheck');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('noOfAssets_error', validator.getValue(ERROR));
		$('#lease').height('385px');
		lease._setTabActive('lease_1', true);
		return false;
	}
	return true;
}

function validateScheduleDependFields() {
	// Financial Schedule arrive validation
	errors = 0;
	if (!tenor_val(false)) {
		errors++;
	}
	if (!repayFreq_val(false)) {
		errors++;
	}
	if (!firstPaymet_val(false)) {
		errors++;
	}
	if (!internalRateReturn_val(false)) {
		errors++;
	}
	if (!shipAddress_val(false)) {
		errors++;
	}
	if (!billBranch_val(false)) {
		errors++;
	}
	if (!revalidateSchedule()) {
		errors++;
	}

	if (leaseRentalCharges_Grid.getRowsNum() > 0) {
		var totalTenor = 0;
		for (var i = 0; i < leaseRentalCharges_Grid.getRowsNum(); i++) {
			totalTenor = totalTenor + parseInt(leaseRentalCharges_Grid.cells2(i, 1).getValue());
		}
		if (totalTenor != parseInt($('#tenor').val())) {
			setError('uptoToner_error', NO_OF_TENOR_UPTO_TENOR_EQUAL);
			errors++;
			return false;
		}
	}
	$('#iAgreeContinue').prop('disabled', true);
	$('#iAgree').prop('disabled', true);
	setCheckbox('iAgree', NO);
	if (errors > 0) {
		return false;
	} else {
		return true;
	}
}

function revalidate() {
	if (reValNotReq) {
		if (win._idd == 'undefined' || win._idd == null)
			win = hideWin;
		closeInlinePopUp(win);
		reValNotReq = false;
		return true;
	}

	if (!scheduleID_val(false)) {
		errors++;
		return false;
	}
	if (!validateScheduleDependFields()) {
		errors++;
		return false;
	}

	// Amount Validation

	if (!commencementValueFormat_val(false)) {
		errors++;
	}
	if (!grossAssetCostFormat_val(false)) {
		errors++;
	}
	if (!intBrokenRentFormat_val(false)) {
		errors++;
	}
	if (!intBrokenExecFormat_val(false)) {
		errors++;
	}
	if (!finalBrokenRentFormat_val(false)) {
		errors++;
	}
	if (!finaBrokenExecFormat_val(false)) {
		errors++;
	}
	if (!assureRVAmountFormat_val(false)) {
		errors++;
	}

	// TAB 1
	if (leaseNoOfAssets_Grid.getRowsNum() > 0) {
		leaseNoOfAssets_Grid.setSerializationLevel(false, false, false, false, false, true);
		$('#xmlleaseNoOfAssetsGrid').val(leaseNoOfAssets_Grid.serialize());
		if (leaseNoOfAssets_Grid.getRowsNum() != parseInt($('#noOfAssets').val())) {
			setError('noOfAssets_error', NO_OF_ASSET_SAME_OF_GRID_VALUES);
			$('#lease').height('385px');
			lease._setTabActive('lease_1', true);
			setFocusLast('noOfAssets');
			errors++;
			return false;
		}
		$('#noOfAssets_error').html(EMPTY_STRING);
	} else {
		leaseNoOfAssets_Grid.clearAll();
		$('#xmlleaseNoOfAssetsGrid').val(leaseNoOfAssets_Grid.serialize());
		$('#lease').height('385px');
		lease._setTabActive('lease_1', true);
		setError('noOfAssets_error', ADD_ATLEAST_ONE_ROW);
		errors++;
	}

	// TAB 4
	if (leaseRentalCharges_Grid.getRowsNum() > 0) {
		var totalTenor = 0;
		for (var i = 0; i < leaseRentalCharges_Grid.getRowsNum(); i++) {
			totalTenor = totalTenor + parseInt(leaseRentalCharges_Grid.cells2(i, 1).getValue());
		}
		if (totalTenor != parseInt($('#tenor').val())) {
			setError('uptoToner_error', NO_OF_TENOR_UPTO_TENOR_EQUAL);
			$('#lease').height('570px');
			lease._setTabActive('lease_3', true);
			setFocusLast('uptoToner');
			errors++;
			return false;
		}
		$('#xmlleaseRentalChargesGrid').val(leaseRentalCharges_Grid.serialize());
		$('#uptoToner_error').html(EMPTY_STRING);
	} else {
		leaseRentalCharges_Grid.clearAll();
		$('#xmlleaseRentalChargesGrid').val(leaseRentalCharges_Grid.serialize());
		$('#lease').height('570px');
		lease._setTabActive('lease_3', true);
		setError('uptoToner_error', HMS_ADD_ATLEAST_ONE_ROW);
		errors++;
	}

	// TAB 5
	if (leaseApplicableTax_Grid.getRowsNum() > 0) {
		$('#xmlleaseApplicableTaxGrid').val(leaseApplicableTax_Grid.serialize());
		$('#taxScheduleCode_error').html(EMPTY_STRING);
	} else {
		// leaseApplicableTax_Grid.clearAll();
		// $('#xmlleaseApplicableTaxGrid').val(leaseApplicableTax_Grid.serialize());
		// lease._setTabActive('lease_5', true);
		// setError('taxScheduleCode_error', HMS_ADD_ATLEAST_ONE_ROW);
		// errors++;
	}

	// TAB 7
	if (leasePaymentDetails_Grid.getRowsNum() > 0) {
		leasePaymentDetails_Grid.setSerializationLevel(false, false, false, false, false, true);
		$('#xmlleasePaymentGrid').val(leasePaymentDetails_Grid.serialize());
		var totalTenor = 0;
		for (var i = 0; i < leasePaymentDetails_Grid.getRowsNum(); i++) {
			totalTenor = totalTenor + parseInt(leasePaymentDetails_Grid.cells2(i, 3).getValue());
		}
		if (totalTenor != parseInt($('#tenor').val())) {
			setError('paymentErrors_error', PAYMENT_TENOR_EQUAL_GRID_COUNT);
			$('#lease').height('610px');
			lease._setTabActive('lease_4', true);
			setFocusLast('grossNetTDS');
			errors++;
			return false;
		}
		$('#paymentErrors_error').html(EMPTY_STRING);
	} else {
		leasePaymentDetails_Grid.clearAll();
		$('#xmlleasePaymentGrid').val(leasePaymentDetails_Grid.serialize());
		$('#lease').height('610px');
		lease._setTabActive('lease_4', true);
		setError('paymentErrors_error', HMS_ADD_ATLEAST_ONE_ROW);
		errors++;
	}

	// TAB 6
	if (errors > 0)
		return false;

	if (!$('#iAgree').is(':checked')) {
		hideUnhide = true;
		if (loadFinancialGrid(false)) {
			if (financialScheduleNonOLGrid.getRowsNum() == 0 && financialScheduleOLGrid.getRowsNum() == 0) {
				financialScheduleNonOLGrid.clearAll();
				financialScheduleOLGrid.clearAll();
				$('#xmlleaseFinanceScheduleGrid').val(financialScheduleNonOLGrid.serialize());
				$('#xmlleaseFinanceScheduleGrid').val(financialScheduleOLGrid.serialize());
				$('#lease').height('610px');
				lease._setTabActive('lease_4', true);
				alert(HMS_ADD_ATLEAST_ONE_ROW);
				errors++;
			} else {
				// generateScheduleData();
				if (!$('#leaseFinanceRentalSchedule').hasClass('hidden')) {
					financialScheduleOLGrid.setSerializationLevel(false, false, false, false, false, true);
					$('#xmlleaseFinanceScheduleGrid').val(financialScheduleOLGrid.serialize());
				} else if (!$('#leaseFinanceSchedule').hasClass('hidden')) {
					financialScheduleNonOLGrid.setSerializationLevel(false, false, false, false, false, true);
					$('#xmlleaseFinanceScheduleGrid').val(financialScheduleNonOLGrid.serialize());
				}
				$('#leaseErrors_error').html(EMPTY_STRING);
			}
			errors++;
			return false;
		} else {
			$('#showFinancialGrid').addClass('hidden');
			hide('closeFinGrid');
			hide('agreeAndContinue');
			errors++;
			$('#lease').height('610px');
			lease._setTabActive('lease_4', true);
			setFocusLast('grossNetTDS');
			return false;
		}
	}

	// if (confirm(FINANCE_GRID_LOADED)) {
	if (financialScheduleNonOLGrid.getRowsNum() == 0 && financialScheduleOLGrid.getRowsNum() == 0) {
		financialScheduleNonOLGrid.clearAll();
		financialScheduleOLGrid.clearAll();
		$('#xmlleaseFinanceScheduleGrid').val(financialScheduleNonOLGrid.serialize());
		$('#xmlleaseFinanceScheduleGrid').val(financialScheduleOLGrid.serialize());
		alert(HMS_ADD_ATLEAST_ONE_ROW);
		errors++;
	} else {
		if (!$('#leaseFinanceRentalSchedule').hasClass('hidden')) {
			financialScheduleOLGrid.setSerializationLevel(false, false, false, false, false, true);
			$('#xmlleaseFinanceScheduleGrid').val(financialScheduleOLGrid.serialize());
		} else if (!$('#leaseFinanceSchedule').hasClass('hidden')) {
			financialScheduleNonOLGrid.setSerializationLevel(false, false, false, false, false, true);
			$('#xmlleaseFinanceScheduleGrid').val(financialScheduleNonOLGrid.serialize());
		}
		$('#leaseErrors_error').html(EMPTY_STRING);

		// if ($('#leaseProductType').val() != 'O') {
		// var totalprinc = 0.00;
		// var totalint = 0.00;
		// var totalRent = 0.00;
		// for (var i = 0; i < financialScheduleNonOLGrid.getRowsNum();
		// i++)
		// {
		// totalprinc = totalprinc +
		// parseFloat(financialScheduleNonOLGrid.cells2(i,
		// 13).getValue());
		// totalint = totalint +
		// parseFloat(financialScheduleNonOLGrid.cells2(i,
		// 14).getValue());
		// totalRent = totalRent +
		// parseFloat(financialScheduleNonOLGrid.cells2(i,
		// 16).getValue());
		// }
		// var totalPrinInter = 0;
		// totalPrinInter = parseFloat(totalprinc) +
		// parseFloat(totalint);
		// if (parseFloat(totalRent).toFixed(2) != totalPrinInter) {
		// setError('leaseErrors_error', PRINC_INTER_COUNT_EQUAL);
		// errors++;
		// lease._setTabActive('lease_4', true);
		// return false;
		// }
		// }
	}

	// } else {
	// $('#showFinancialGrid').addClass('hidden');
	// hide('closeFinGrid');
	// hide('agreeAndContinue');
	// errors++;
	// lease._setTabActive('lease_4', true);
	// setFocusLast('grossNetTDS');
	// return false;
	// }

	if (errors > 0) {
		return false;
	} else {
		return true;
	}
}

// TAB 6 Financial POP up
function financialAmount() {
	$('#startDate').val(EMPTY_STRING);
	$('#endDate').val(EMPTY_STRING);
	$('#totalRentAmt').val(EMPTY_STRING);
	$('#totalRentAmtFormat').val(EMPTY_STRING);
	$('#principalAmt').val(EMPTY_STRING);
	$('#principalAmt_error').html(EMPTY_STRING);
	$('#principalAmtFormat').val(EMPTY_STRING);
	$('#interestAmt').val(EMPTY_STRING);
	$('#interestAmt_error').html(EMPTY_STRING);
	$('#interestAmtFormat').val(EMPTY_STRING);
	$('#finSerial').val(EMPTY_STRING);
}

function financialClearTotalValues() {
	$('#principalAmt').html(EMPTY_STRING);
	$('#interestAmt').html(EMPTY_STRING);
	$('#executoryAmt').html(EMPTY_STRING);
	$('#rentalAmt').html(EMPTY_STRING);
	$('#stateAmt').html(EMPTY_STRING);
	$('#centralAmt').html(EMPTY_STRING);
	$('#serviceAmt').html(EMPTY_STRING);
	$('#cessesAmt').html(EMPTY_STRING);
	$('#totalAmt').html(EMPTY_STRING);

	$('#executoryAmtFormat').html(EMPTY_STRING);
	$('#rentalAmtFormat').html(EMPTY_STRING);
	$('#stateAmtFormat').html(EMPTY_STRING);
	$('#centralAmtFormat').html(EMPTY_STRING);
	$('#serviceAmtFormat').html(EMPTY_STRING);
	$('#cessesAmtFormat').html(EMPTY_STRING);
	$('#totalAmtFormat').html(EMPTY_STRING);
}

function principalAmtFormat_val(valMode) {
	var value = unformatAmount($('#principalAmtFormat').val());
	clearError('principalAmt_error');
	clearError('interestAmt_error');
	var rowId = $('#finSerial').val();
	var rentalAmt = parseFloat(financialScheduleNonOLGrid.cells2(rowId - 1, 16).getValue()).toFixed(2);
	if (isEmpty(value)) {
		setError('principalAmt_error', MANDATORY);
		return false;
	}
	$('#principalAmt').val(value);
	if (isZero(value)) {
		setError('principalAmt_error', ZERO_AMOUNT);
		return false;
	}
	if (!isCurrencySmallAmount($('#principalAmt_curr').val(), value)) {
		setError('principalAmt_error', INVALID_SMALL_AMOUNT);
		return false;
	}
	if (!isPositiveAmount(value)) {
		setError('principalAmt_error', POSITIVE_AMOUNT);
		return false;
	}
	// if (value > rentalAmt) {
	// setError('principalAmt_error', PRINCIPAL_CANT_GREATER);
	// return false;
	// }
	$('#principalAmtFormat').val(formatAmount(value, $('#principalAmt_curr').val()));
	if (valMode) {
		var interAmt = parseFloat(rentalAmt) - parseFloat($('#principalAmt').val());
		var balAmt = parseFloat(interAmt).toFixed(2);
		$('#interestAmtFormat').val(formatAmount(balAmt.toString(), $('#principalAmt_curr').val()));
		$('#interestAmt').val(balAmt);
	}
	setFocusLast('interestAmtFormat');
	return true;
}

function interestAmtFormat_val() {
	var value = unformatAmount($('#interestAmtFormat').val());
	clearError('interestAmt_error');
	clearError('principalAmt_error');
	if (isEmpty(value)) {
		setError('interestAmt_error', MANDATORY);
		return false;
	}
	$('#interestAmt').val(value);
	if (!isCurrencySmallAmount($('#interestAmt_curr').val(), value)) {
		setError('interestAmt_error', INVALID_SMALL_AMOUNT);
		return false;
	}
	if (!isPositiveAmount(value)) {
		setError('interestAmt_error', POSITIVE_AMOUNT);
		return false;
	}
	$('#interestAmtFormat').val(formatAmount(value, $('#interestAmt_curr').val()));
	setFocusLast('finAmount');
	return true;
}

function updateAmountDetails(rowId) {
	var val = $('#finSerial').val();
	var execAmt = financialScheduleNonOLGrid.cells2(val - 1, 15).getValue();
	var rentalAmt = financialScheduleNonOLGrid.cells2(val - 1, 16).getValue();
	var editSl = financialScheduleNonOLGrid.cells2(val - 1, 23).getValue();

	var oldStateTax = financialScheduleNonOLGrid.cells2(val - 1, 17).getValue();
	var oldCentralTax = financialScheduleNonOLGrid.cells2(val - 1, 18).getValue();
	var oldSerTax = financialScheduleNonOLGrid.cells2(val - 1, 19).getValue();
	var oldCessTax = financialScheduleNonOLGrid.cells2(val - 1, 20).getValue();
	var oldToatlTax = financialScheduleNonOLGrid.cells2(val - 1, 21).getValue();

	var oldSl = financialScheduleNonOLGrid.cells2(val - 1, 23).getValue();

	var maxSl = ZERO;
	$('#leaseErrors_error').html(EMPTY_STRING);
	if (financialScheduleNonOLGrid.getRowsNum() > 0) {
		totalSl = financialScheduleNonOLGrid.getRowsNum();
		maxSl = financialScheduleNonOLGrid.cells2(totalSl - 1, 23).getValue();
	}
	if (principalAmtFormat_val(false) && interestAmtFormat_val()) {
		var rentTotal = parseFloat($('#interestAmt').val()) + parseFloat($('#principalAmt').val());
		if (rentTotal != parseFloat(rentalAmt)) {
			setError('principalAmt_error', SUM_OF_PRINCIPAL_INTEREST_EQUAL_RENTAL);
			return false;
		} else if (rentTotal == rentalAmt) {
			validator.clearMap();
			validator.setMtm(false);
			validator.setValue('ENTITY_CODE', getEntityCode());
			validator.setValue('STATE_CODE', $('#billState').val());
			validator.setValue('SHIP_STATE_CODE', $('#shippingState').val());
			validator.setValue('TAX_SCHEDULE_CODE', $('#taxScheduleCode').val());
			validator.setValue('TAX_CCY', $('#assetCurrency').val());
			validator.setValue('TAX_ON_DATE', getCBD());
			validator.setValue('PRINCIPAL_AMOUNT', $('#principalAmt').val());
			validator.setValue('INTEREST_AMOUNT', $('#interestAmt').val());
			validator.setValue('EXECUTORT_AMOUNT', execAmt);
			// if ($('#principalAmt').val() > 0 || $('#interestAmt').val() > 0)
			// validator.setValue('RENTAL_AMOUNT', "0");
			// else
			validator.setValue('RENTAL_AMOUNT', rentalAmt);
			validator.setValue('MAX_MAIN_SL', oldSl);
			validator.setValue(ACTION, USAGE);
			validator.setClass('patterns.config.web.forms.lss.eleasebean');
			validator.setMethod('getFinancialMonthWiseDetails');
			validator.sendAndReceive();
			if (validator.getValue(ERROR) != EMPTY_STRING) {
				alert(validator.getValue(ERROR));
				return false;
			} else {
				financialScheduleNonOLGrid.cells(val, 4).setValue($('#principalAmtFormat').val());
				financialScheduleNonOLGrid.cells(val, 5).setValue($('#interestAmtFormat').val());

				var stateTaxLink = validator.getValue("STATE_TAX_FORMAT") + '^javascript:viewStateTaxDetails("' + val + PK_SEPERATOR + validator.getValue("SP_SERIAL") + PK_SEPERATOR + validator.getValue("EDIT_NEW_SL") + PK_SEPERATOR + validator.getValue("STATE_TAX") + '")';
				var centralTaxLink = validator.getValue("CENTRAL_TAX_FORMAT") + '^javascript:viewCentralTaxDetails("' + val + PK_SEPERATOR + validator.getValue("SP_SERIAL") + PK_SEPERATOR + validator.getValue("EDIT_NEW_SL") + PK_SEPERATOR + validator.getValue("CENTRAL_TAX") + '")';

				var serviceTaxLink = validator.getValue("SERVICE_TAX_FORMAT") + '^javascript:viewServiceTaxDetails("' + val + PK_SEPERATOR + validator.getValue("SP_SERIAL") + PK_SEPERATOR + validator.getValue("EDIT_NEW_SL") + PK_SEPERATOR + validator.getValue("SERVICE_TAX") + '")';
				var cessTaxLink = validator.getValue("CESS_TAX_FORMAT") + '^javascript:viewCessTaxDetails("' + val + PK_SEPERATOR + validator.getValue("SP_SERIAL") + PK_SEPERATOR + validator.getValue("EDIT_NEW_SL") + PK_SEPERATOR + validator.getValue("CESS_TAX") + '")';

				financialScheduleNonOLGrid.cells(val, 8).setValue(stateTaxLink);
				financialScheduleNonOLGrid.cells(val, 9).setValue(centralTaxLink);

				financialScheduleNonOLGrid.cells(val, 10).setValue(serviceTaxLink);
				financialScheduleNonOLGrid.cells(val, 11).setValue(cessTaxLink);
				financialScheduleNonOLGrid.cells(val, 12).setValue(validator.getValue("TOTAL_TAX_FORMAT"));

				financialScheduleNonOLGrid.cells(val, 13).setValue($('#principalAmt').val());
				financialScheduleNonOLGrid.cells(val, 14).setValue($('#interestAmt').val());

				financialScheduleNonOLGrid.cells(val, 17).setValue(validator.getValue("STATE_TAX"));
				financialScheduleNonOLGrid.cells(val, 18).setValue(validator.getValue("CENTRAL_TAX"));
				financialScheduleNonOLGrid.cells(val, 19).setValue(validator.getValue("SERVICE_TAX"));
				financialScheduleNonOLGrid.cells(val, 20).setValue(validator.getValue("CESS_TAX"));
				financialScheduleNonOLGrid.cells(val, 21).setValue(validator.getValue("TOTAL_TAX"));
				financialScheduleNonOLGrid.cells(val, 23).setValue(validator.getValue("EDIT_NEW_SL"));
				var newEditSl = validator.getValue("EDIT_NEW_SL");

				$('#stateAmt').html(parseFloat($('#stateAmt').html().toString()) - parseFloat(oldStateTax));
				$('#centralAmt').html(parseFloat($('#centralAmt').html().toString()) - parseFloat(oldCentralTax));
				$('#serviceAmt').html(parseFloat($('#serviceAmt').html().toString()) - parseFloat(oldSerTax));
				$('#cessesAmt').html(parseFloat($('#cessesAmt').html().toString()) - parseFloat(oldCessTax));
				$('#totalAmt').html(parseFloat($('#totalAmt').html().toString()) - parseFloat(oldToatlTax));

				var singleStateTax = parseFloat($('#stateAmt').html().toString()) + parseFloat(validator.getValue("STATE_TAX"));
				var singleCentralTax = parseFloat($('#centralAmt').html().toString()) + parseFloat(validator.getValue("CENTRAL_TAX"));
				var singleServiceTax = parseFloat($('#serviceAmt').html().toString()) + parseFloat(validator.getValue("SERVICE_TAX"));
				var singleCessTax = parseFloat($('#cessesAmt').html().toString()) + parseFloat(validator.getValue("CESS_TAX"));
				var singleTotalTax = parseFloat($('#totalAmt').html().toString()) + parseFloat(validator.getValue("TOTAL_TAX"));

				var totalprincipal = 0;
				var totalInterest = 0;
				totalprincipal = parseFloat($('#pincipalAmt').html()) + parseFloat($('#principalAmt').val());
				totalInterest = parseFloat($('#pincipalAmt').html()) + parseFloat($('#principalAmt').val());
				$('#pincipalAmt').html(totalprincipal);
				$('#interAmt').html(totalInterest);

				$('#stateAmtFormat').html(formatAmount(singleStateTax.toString(), $('#assetCurrency').val()));
				$('#centralAmtFormat').html(formatAmount(singleCentralTax.toString(), $('#assetCurrency').val()));
				$('#serviceAmtFormat').html(formatAmount(singleServiceTax.toString(), $('#assetCurrency').val()));
				$('#cessesAmtFormat').html(formatAmount(singleCessTax.toString(), $('#assetCurrency').val()));
				$('#totalAmtFormat').html(formatAmount(singleTotalTax.toString(), $('#assetCurrency').val()));

				$('#stateAmt').html(singleStateTax);
				$('#centralAmt').html(singleCentralTax);
				$('#serviceAmt').html(singleServiceTax);
				$('#cessesAmt').html(singleCessTax);
				$('#totalAmt').html(singleTotalTax);

				if (!isEmpty(editSl) || !isEmpty(newEditSl)) {
					var allSl = [];
					for (var i = 0; i < financialScheduleNonOLGrid.getRowsNum(); i++) {
						lastValue = parseInt(financialScheduleNonOLGrid.cells2(i, 23).getValue());
						if (jQuery.inArray(lastValue, allSl) == -1)
							allSl.push(lastValue);
					}
					// $('#grossTaxAmount').html(EMPTY_STRING);
					clearError('taxScheduleCode_error');
					// leaseApplicableTax_Grid.clearAll();
					validator.clearMap();
					validator.setMtm(false);
					validator.setValue('ENTITY_CODE', getEntityCode());
					validator.setValue('EDIT_OLD_SL', editSl);
					validator.setValue('EDIT_NEW_SL', newEditSl);
					if (allSl.length != 0)
						validator.setValue("TOTAL_SL", allSl);
					validator.setValue(ACTION, USAGE);
					validator.setClass('patterns.config.web.forms.lss.eleasebean');
					validator.setMethod('getTaxDetails');
					validator.sendAndReceive();
					if (validator.getValue(ERROR) != EMPTY_STRING) {
						// leaseApplicableTax_Grid.clearAll();
						// $('#grossTaxAmount').html(EMPTY_STRING);
						alert(validator.getValue(ERROR));
						// return false;
					} else {
						leaseApplicableTax_Grid.clearAll();
						leaseApplicableTax_Grid.loadXMLString(validator.getValue("TAX_XML"));
						// $('#grossTaxAmount').html(validator.getValue("GROSS_TAX_AMOUNT"));
						// taxTotal = validator.getValue("GROSS_TAX_AMOUNT");
					}
				}

				// lease._setTabActive('lease_5', true);
				closeInlinePopUp(win);
			}
		}
	}
}

function eleaseFinancial_editGrid(rowId) {
	win = EMPTY_STRING;
	financialAmount();
	// financialClearTotalValues();
	$('#finSerial').val(rowId);
	$('#startDate').val(financialScheduleNonOLGrid.cells2(rowId - 1, 2).getValue());
	$('#endDate').val(financialScheduleNonOLGrid.cells2(rowId - 1, 3).getValue());

	if (!isZero(financialScheduleNonOLGrid.cells2(rowId - 1, 14).getValue())) {
		$('#interestAmt').val(financialScheduleNonOLGrid.cells2(rowId - 1, 14).getValue());
		$('#interestAmtFormat').val(financialScheduleNonOLGrid.cells2(rowId - 1, 5).getValue());
	}
	if (!isZero(financialScheduleNonOLGrid.cells2(rowId - 1, 13).getValue())) {
		$('#principalAmt').val(financialScheduleNonOLGrid.cells2(rowId - 1, 13).getValue());
		$('#principalAmtFormat').val(financialScheduleNonOLGrid.cells2(rowId - 1, 4).getValue());
	}
	$('#totalRentAmtFormat').val(financialScheduleNonOLGrid.cells2(rowId - 1, 7).getValue());
	win = showWindow('editFinancialAmount', EMPTY_STRING, 'Financial Wise Rent Calculation Details', EMPTY_STRING, true, false, false, 670, 250);
	$('#principalAmtFormat').focus();
}

function updateAndAddNewAmountDetails(rowId) {
	var val = $('#finSerial').val();
	var execAmt = financialScheduleNonOLGrid.cells2(val - 1, 15).getValue();
	var rentalAmt = financialScheduleNonOLGrid.cells2(val - 1, 16).getValue();
	var editSl = financialScheduleNonOLGrid.cells2(val - 1, 23).getValue();
	var maxSl = ZERO;

	var oldStateTax = financialScheduleNonOLGrid.cells2(val - 1, 17).getValue();
	var oldCentralTax = financialScheduleNonOLGrid.cells2(val - 1, 18).getValue();
	var oldSerTax = financialScheduleNonOLGrid.cells2(val - 1, 19).getValue();
	var oldCessTax = financialScheduleNonOLGrid.cells2(val - 1, 20).getValue();
	var oldToatlTax = financialScheduleNonOLGrid.cells2(val - 1, 21).getValue();
	var oldSl = financialScheduleNonOLGrid.cells2(val - 1, 23).getValue();

	$('#leaseErrors_error').html(EMPTY_STRING);
	if (financialScheduleNonOLGrid.getRowsNum() > 0) {
		totalSl = financialScheduleNonOLGrid.getRowsNum();
		maxSl = financialScheduleNonOLGrid.cells2(totalSl - 1, 23).getValue();
	}
	if (principalAmtFormat_val(false) && interestAmtFormat_val()) {
		var rentTotal = parseFloat($('#interestAmt').val()) + parseFloat($('#principalAmt').val());
		if (rentTotal != parseFloat(rentalAmt)) {
			setError('principalAmt_error', SUM_OF_PRINCIPAL_INTEREST_EQUAL_RENTAL);
			return false;
		} else if (rentTotal == rentalAmt) {
			validator.clearMap();
			validator.setMtm(false);
			validator.setValue('ENTITY_CODE', getEntityCode());
			validator.setValue('STATE_CODE', $('#billState').val());
			validator.setValue('SHIP_STATE_CODE', $('#shippingState').val());
			validator.setValue('TAX_SCHEDULE_CODE', $('#taxScheduleCode').val());
			validator.setValue('TAX_CCY', $('#assetCurrency').val());
			validator.setValue('TAX_ON_DATE', getCBD());
			validator.setValue('PRINCIPAL_AMOUNT', $('#principalAmt').val());
			validator.setValue('INTEREST_AMOUNT', $('#interestAmt').val());
			validator.setValue('EXECUTORT_AMOUNT', execAmt);
			// if ($('#principalAmt').val() > 0 || $('#interestAmt').val() > 0)
			// validator.setValue('RENTAL_AMOUNT', "0");
			// else
			validator.setValue('RENTAL_AMOUNT', rentalAmt);
			validator.setValue('MAX_MAIN_SL', oldSl);
			validator.setValue(ACTION, USAGE);
			validator.setClass('patterns.config.web.forms.lss.eleasebean');
			validator.setMethod('getFinancialMonthWiseDetails');
			validator.sendAndReceive();
			if (validator.getValue(ERROR) != EMPTY_STRING) {
				alert(validator.getValue(ERROR));
				return false;
			} else {
				financialScheduleNonOLGrid.cells(val, 4).setValue($('#principalAmtFormat').val());
				financialScheduleNonOLGrid.cells(val, 5).setValue($('#interestAmtFormat').val());

				var stateTaxLink = validator.getValue("STATE_TAX_FORMAT") + '^javascript:viewStateTaxDetails("' + val + PK_SEPERATOR + validator.getValue("SP_SERIAL") + PK_SEPERATOR + validator.getValue("EDIT_NEW_SL") + PK_SEPERATOR + validator.getValue("STATE_TAX") + '")';
				var centralTaxLink = validator.getValue("CENTRAL_TAX_FORMAT") + '^javascript:viewCentralTaxDetails("' + val + PK_SEPERATOR + validator.getValue("SP_SERIAL") + PK_SEPERATOR + validator.getValue("EDIT_NEW_SL") + PK_SEPERATOR + validator.getValue("CENTRAL_TAX") + '")';

				var serviceTaxLink = validator.getValue("SERVICE_TAX_FORMAT") + '^javascript:viewServiceTaxDetails("' + val + PK_SEPERATOR + validator.getValue("SP_SERIAL") + PK_SEPERATOR + validator.getValue("EDIT_NEW_SL") + PK_SEPERATOR + validator.getValue("SERVICE_TAX") + '")';
				var cessTaxLink = validator.getValue("CESS_TAX_FORMAT") + '^javascript:viewCessTaxDetails("' + val + PK_SEPERATOR + validator.getValue("SP_SERIAL") + PK_SEPERATOR + validator.getValue("EDIT_NEW_SL") + PK_SEPERATOR + validator.getValue("CESS_TAX") + '")';

				financialScheduleNonOLGrid.cells(val, 8).setValue(stateTaxLink);
				financialScheduleNonOLGrid.cells(val, 9).setValue(centralTaxLink);

				// financialScheduleNonOLGrid.cells(val,
				// 8).setValue(validator.getValue("STATE_TAX_FORMAT"));
				// financialScheduleNonOLGrid.cells(val,
				// 9).setValue(validator.getValue("CENTRAL_TAX_FORMAT"));
				financialScheduleNonOLGrid.cells(val, 10).setValue(serviceTaxLink);
				financialScheduleNonOLGrid.cells(val, 11).setValue(cessTaxLink);
				financialScheduleNonOLGrid.cells(val, 12).setValue(validator.getValue("TOTAL_TAX_FORMAT"));

				financialScheduleNonOLGrid.cells(val, 13).setValue($('#principalAmt').val());
				financialScheduleNonOLGrid.cells(val, 14).setValue($('#interestAmt').val());
				financialScheduleNonOLGrid.cells(val, 17).setValue(validator.getValue("STATE_TAX"));
				financialScheduleNonOLGrid.cells(val, 18).setValue(validator.getValue("CENTRAL_TAX"));
				financialScheduleNonOLGrid.cells(val, 19).setValue(validator.getValue("SERVICE_TAX"));
				financialScheduleNonOLGrid.cells(val, 20).setValue(validator.getValue("CESS_TAX"));
				financialScheduleNonOLGrid.cells(val, 21).setValue(validator.getValue("TOTAL_TAX"));
				financialScheduleNonOLGrid.cells(val, 23).setValue(validator.getValue("EDIT_NEW_SL"));
				var newEditSl = validator.getValue("EDIT_NEW_SL");

				$('#stateAmt').html(parseFloat($('#stateAmt').html().toString()) - parseFloat(oldStateTax));
				$('#centralAmt').html(parseFloat($('#centralAmt').html().toString()) - parseFloat(oldCentralTax));
				$('#serviceAmt').html(parseFloat($('#serviceAmt').html().toString()) - parseFloat(oldSerTax));
				$('#cessesAmt').html(parseFloat($('#cessesAmt').html().toString()) - parseFloat(oldCessTax));
				$('#totalAmt').html(parseFloat($('#totalAmt').html().toString()) - parseFloat(oldToatlTax));

				var singleStateTax = parseFloat($('#stateAmt').html().toString()) + parseFloat(validator.getValue("STATE_TAX"));
				var singleCentralTax = parseFloat($('#centralAmt').html().toString()) + parseFloat(validator.getValue("CENTRAL_TAX"));
				var singleServiceTax = parseFloat($('#serviceAmt').html().toString()) + parseFloat(validator.getValue("SERVICE_TAX"));
				var singleCessTax = parseFloat($('#cessesAmt').html().toString()) + parseFloat(validator.getValue("CESS_TAX"));
				var singleTotalTax = parseFloat($('#totalAmt').html().toString()) + parseFloat(validator.getValue("TOTAL_TAX"));

				$('#stateAmt').html(formatAmount(singleStateTax.toString(), $('#assetCurrency').val()));
				$('#centralAmt').html(formatAmount(singleCentralTax.toString(), $('#assetCurrency').val()));
				$('#serviceAmt').html(formatAmount(singleServiceTax.toString(), $('#assetCurrency').val()));
				$('#cessesAmt').html(formatAmount(singleCessTax.toString(), $('#assetCurrency').val()));
				$('#totalAmt').html(formatAmount(singleTotalTax.toString(), $('#assetCurrency').val()));

				var totalprincipal = 0;
				var totalInterest = 0;
				totalprincipal = parseFloat($('#pincipalAmt').html()) + parseFloat($('#principalAmt').val());
				totalInterest = parseFloat($('#pincipalAmt').html()) + parseFloat($('#principalAmt').val());
				$('#pincipalAmt').html(totalprincipal);
				$('#interAmt').html(totalInterest);

				$('#stateAmtFormat').html(formatAmount(singleStateTax.toString(), $('#assetCurrency').val()));
				$('#centralAmtFormat').html(formatAmount(singleCentralTax.toString(), $('#assetCurrency').val()));
				$('#serviceAmtFormat').html(formatAmount(singleServiceTax.toString(), $('#assetCurrency').val()));
				$('#cessesAmtFormat').html(formatAmount(singleCessTax.toString(), $('#assetCurrency').val()));
				$('#totalAmtFormat').html(formatAmount(singleTotalTax.toString(), $('#assetCurrency').val()));

				$('#stateAmt').html(singleStateTax);
				$('#centralAmt').html(singleCentralTax);
				$('#serviceAmt').html(singleServiceTax);
				$('#cessesAmt').html(singleCessTax);
				$('#totalAmt').html(singleTotalTax);

				if (!isEmpty(editSl) || !isEmpty(newEditSl)) {
					var allSl = [];
					for (var i = 0; i < financialScheduleNonOLGrid.getRowsNum(); i++) {
						lastValue = parseInt(financialScheduleNonOLGrid.cells2(i, 23).getValue());
						if (jQuery.inArray(lastValue, allSl) == -1)
							allSl.push(lastValue);
					}
					// $('#grossTaxAmount').html(EMPTY_STRING);
					clearError('taxScheduleCode_error');
					// leaseApplicableTax_Grid.clearAll();
					validator.clearMap();
					validator.setMtm(false);
					validator.setValue('ENTITY_CODE', getEntityCode());
					validator.setValue('EDIT_OLD_SL', editSl);
					validator.setValue('EDIT_NEW_SL', newEditSl);
					if (allSl.length != 0)
						validator.setValue("TOTAL_SL", allSl);
					validator.setValue(ACTION, USAGE);
					validator.setClass('patterns.config.web.forms.lss.eleasebean');
					validator.setMethod('getTaxDetails');
					validator.sendAndReceive();
					if (validator.getValue(ERROR) != EMPTY_STRING) {
						// leaseApplicableTax_Grid.clearAll();
						// $('#grossTaxAmount').html(EMPTY_STRING);
						alert(validator.getValue(ERROR));
						// return false;
					} else {
						leaseApplicableTax_Grid.clearAll();
						leaseApplicableTax_Grid.loadXMLString(validator.getValue("TAX_XML"));
						// $('#grossTaxAmount').html(validator.getValue("GROSS_TAX_AMOUNT"));
						// taxTotal =
						// validator.getValue("GROSS_TAX_AMOUNT");
					}
				}

				if (financialScheduleNonOLGrid.getRowsNum() == parseInt(val)) {
					alert('Last Row recoed updated, No Next Row records');
					closeInlinePopUp(win);
				} else {
					financialAmount();
					rowId = parseInt(val) + 1;
					$('#startDate').val(financialScheduleNonOLGrid.cells2(rowId - 1, 2).getValue());
					$('#endDate').val(financialScheduleNonOLGrid.cells2(rowId - 1, 3).getValue());
					$('#totalRentAmtFormat').val(financialScheduleNonOLGrid.cells2(rowId - 1, 7).getValue());

					if (!isZero(financialScheduleNonOLGrid.cells2(rowId - 1, 13).getValue())) {
						$('#principalAmtFormat').val(financialScheduleNonOLGrid.cells2(rowId - 1, 4).getValue());
						$('#principalAmt').val(financialScheduleNonOLGrid.cells2(rowId - 1, 13).getValue());
					}
					if (!isZero(financialScheduleNonOLGrid.cells2(rowId - 1, 14).getValue())) {
						$('#interestAmt').val(financialScheduleNonOLGrid.cells2(rowId - 1, 14).getValue());
						$('#interestAmtFormat').val(financialScheduleNonOLGrid.cells2(rowId - 1, 5).getValue());
					}

					$('#finSerial').val(rowId);
					$('#principalAmtFormat').focus();
				}
			}
		}
	}
}

function updateAndEditPreviousAmountDetails(rowId) {
	if ($('#finSerial').val() != '1') {
		var val = $('#finSerial').val();
		var execAmt = financialScheduleNonOLGrid.cells2(val - 1, 15).getValue();
		var rentalAmt = financialScheduleNonOLGrid.cells2(val - 1, 16).getValue();
		var editSl = financialScheduleNonOLGrid.cells2(val - 1, 23).getValue();
		var maxSl = ZERO;

		var oldStateTax = financialScheduleNonOLGrid.cells2(val - 1, 17).getValue();
		var oldCentralTax = financialScheduleNonOLGrid.cells2(val - 1, 18).getValue();
		var oldSerTax = financialScheduleNonOLGrid.cells2(val - 1, 19).getValue();
		var oldCessTax = financialScheduleNonOLGrid.cells2(val - 1, 20).getValue();
		var oldToatlTax = financialScheduleNonOLGrid.cells2(val - 1, 21).getValue();
		var oldSl = financialScheduleNonOLGrid.cells2(val - 1, 23).getValue();

		$('#leaseErrors_error').html(EMPTY_STRING);
		if (financialScheduleNonOLGrid.getRowsNum() > 0) {
			totalSl = financialScheduleNonOLGrid.getRowsNum();
			maxSl = financialScheduleNonOLGrid.cells2(totalSl - 1, 23).getValue();
		}
		if (principalAmtFormat_val(false) && interestAmtFormat_val()) {
			var rentTotal = parseFloat($('#interestAmt').val()) + parseFloat($('#principalAmt').val());
			if (rentTotal != parseFloat(rentalAmt)) {
				setError('principalAmt_error', SUM_OF_PRINCIPAL_INTEREST_EQUAL_RENTAL);
				return false;
			} else if (rentTotal == rentalAmt) {
				validator.clearMap();
				validator.setMtm(false);
				validator.setValue('ENTITY_CODE', getEntityCode());
				validator.setValue('STATE_CODE', $('#billState').val());
				validator.setValue('SHIP_STATE_CODE', $('#shippingState').val());
				validator.setValue('TAX_SCHEDULE_CODE', $('#taxScheduleCode').val());
				validator.setValue('TAX_CCY', $('#assetCurrency').val());
				validator.setValue('TAX_ON_DATE', getCBD());
				validator.setValue('PRINCIPAL_AMOUNT', $('#principalAmt').val());
				validator.setValue('INTEREST_AMOUNT', $('#interestAmt').val());
				validator.setValue('EXECUTORT_AMOUNT', execAmt);
				// if ($('#principalAmt').val() > 0 || $('#interestAmt').val() >
				// 0)
				// validator.setValue('RENTAL_AMOUNT', "0");
				// else
				validator.setValue('RENTAL_AMOUNT', rentalAmt);
				validator.setValue('MAX_MAIN_SL', oldSl);
				validator.setValue(ACTION, USAGE);
				validator.setClass('patterns.config.web.forms.lss.eleasebean');
				validator.setMethod('getFinancialMonthWiseDetails');
				validator.sendAndReceive();
				if (validator.getValue(ERROR) != EMPTY_STRING) {
					alert(validator.getValue(ERROR));
					return false;
				} else {
					financialScheduleNonOLGrid.cells(val, 4).setValue($('#principalAmtFormat').val());
					financialScheduleNonOLGrid.cells(val, 5).setValue($('#interestAmtFormat').val());
					var stateTaxLink = validator.getValue("STATE_TAX_FORMAT") + '^javascript:viewStateTaxDetails("' + val + PK_SEPERATOR + validator.getValue("SP_SERIAL") + PK_SEPERATOR + validator.getValue("EDIT_NEW_SL") + PK_SEPERATOR + validator.getValue("STATE_TAX") + '")';
					var centralTaxLink = validator.getValue("CENTRAL_TAX_FORMAT") + '^javascript:viewCentralTaxDetails("' + val + PK_SEPERATOR + validator.getValue("SP_SERIAL") + PK_SEPERATOR + validator.getValue("EDIT_NEW_SL") + PK_SEPERATOR + validator.getValue("CENTRAL_TAX") + '")';

					var serviceTaxLink = validator.getValue("SERVICE_TAX_FORMAT") + '^javascript:viewServiceTaxDetails("' + val + PK_SEPERATOR + validator.getValue("SP_SERIAL") + PK_SEPERATOR + validator.getValue("EDIT_NEW_SL") + PK_SEPERATOR + validator.getValue("SERVICE_TAX") + '")';
					var cessTaxLink = validator.getValue("CESS_TAX_FORMAT") + '^javascript:viewCessTaxDetails("' + val + PK_SEPERATOR + validator.getValue("SP_SERIAL") + PK_SEPERATOR + validator.getValue("EDIT_NEW_SL") + PK_SEPERATOR + validator.getValue("CESS_TAX") + '")';

					financialScheduleNonOLGrid.cells(val, 8).setValue(stateTaxLink);
					financialScheduleNonOLGrid.cells(val, 9).setValue(centralTaxLink);

					// financialScheduleNonOLGrid.cells(val,
					// 8).setValue(validator.getValue("STATE_TAX_FORMAT"));
					// financialScheduleNonOLGrid.cells(val,
					// 9).setValue(validator.getValue("CENTRAL_TAX_FORMAT"));
					financialScheduleNonOLGrid.cells(val, 10).setValue(serviceTaxLink);
					financialScheduleNonOLGrid.cells(val, 11).setValue(cessTaxLink);
					financialScheduleNonOLGrid.cells(val, 12).setValue(validator.getValue("TOTAL_TAX_FORMAT"));

					financialScheduleNonOLGrid.cells(val, 13).setValue($('#principalAmt').val());
					financialScheduleNonOLGrid.cells(val, 14).setValue($('#interestAmt').val());
					financialScheduleNonOLGrid.cells(val, 17).setValue(validator.getValue("STATE_TAX"));
					financialScheduleNonOLGrid.cells(val, 18).setValue(validator.getValue("CENTRAL_TAX"));
					financialScheduleNonOLGrid.cells(val, 19).setValue(validator.getValue("SERVICE_TAX"));
					financialScheduleNonOLGrid.cells(val, 20).setValue(validator.getValue("CESS_TAX"));
					financialScheduleNonOLGrid.cells(val, 21).setValue(validator.getValue("TOTAL_TAX"));
					financialScheduleNonOLGrid.cells(val, 23).setValue(validator.getValue("EDIT_NEW_SL"));
					var newEditSl = validator.getValue("EDIT_NEW_SL");

					$('#stateAmt').html(parseFloat($('#stateAmt').html().toString()) - parseFloat(oldStateTax));
					$('#centralAmt').html(parseFloat($('#centralAmt').html().toString()) - parseFloat(oldCentralTax));
					$('#serviceAmt').html(parseFloat($('#serviceAmt').html().toString()) - parseFloat(oldSerTax));
					$('#cessesAmt').html(parseFloat($('#cessesAmt').html().toString()) - parseFloat(oldCessTax));
					$('#totalAmt').html(parseFloat($('#totalAmt').html().toString()) - parseFloat(oldToatlTax));

					var singleStateTax = parseFloat($('#stateAmt').html().toString()) + parseFloat(validator.getValue("STATE_TAX"));
					var singleCentralTax = parseFloat($('#centralAmt').html().toString()) + parseFloat(validator.getValue("CENTRAL_TAX"));
					var singleServiceTax = parseFloat($('#serviceAmt').html().toString()) + parseFloat(validator.getValue("SERVICE_TAX"));
					var singleCessTax = parseFloat($('#cessesAmt').html().toString()) + parseFloat(validator.getValue("CESS_TAX"));
					var singleTotalTax = parseFloat($('#totalAmt').html().toString()) + parseFloat(validator.getValue("TOTAL_TAX"));

					$('#stateAmt').html(formatAmount(singleStateTax.toString(), $('#assetCurrency').val()));
					$('#centralAmt').html(formatAmount(singleCentralTax.toString(), $('#assetCurrency').val()));
					$('#serviceAmt').html(formatAmount(singleServiceTax.toString(), $('#assetCurrency').val()));
					$('#cessesAmt').html(formatAmount(singleCessTax.toString(), $('#assetCurrency').val()));
					$('#totalAmt').html(formatAmount(singleTotalTax.toString(), $('#assetCurrency').val()));

					var totalprincipal = 0;
					var totalInterest = 0;
					totalprincipal = parseFloat($('#pincipalAmt').html()) + parseFloat($('#principalAmt').val());
					totalInterest = parseFloat($('#pincipalAmt').html()) + parseFloat($('#principalAmt').val());
					$('#pincipalAmt').html(totalprincipal);
					$('#interAmt').html(totalInterest);

					$('#stateAmtFormat').html(formatAmount(singleStateTax.toString(), $('#assetCurrency').val()));
					$('#centralAmtFormat').html(formatAmount(singleCentralTax.toString(), $('#assetCurrency').val()));
					$('#serviceAmtFormat').html(formatAmount(singleServiceTax.toString(), $('#assetCurrency').val()));
					$('#cessesAmtFormat').html(formatAmount(singleCessTax.toString(), $('#assetCurrency').val()));
					$('#totalAmtFormat').html(formatAmount(singleTotalTax.toString(), $('#assetCurrency').val()));

					$('#stateAmt').html(singleStateTax);
					$('#centralAmt').html(singleCentralTax);
					$('#serviceAmt').html(singleServiceTax);
					$('#cessesAmt').html(singleCessTax);
					$('#totalAmt').html(singleTotalTax);

					if (!isEmpty(editSl) || !isEmpty(newEditSl)) {
						var allSl = [];
						for (var i = 0; i < financialScheduleNonOLGrid.getRowsNum(); i++) {
							lastValue = parseInt(financialScheduleNonOLGrid.cells2(i, 23).getValue());
							if (jQuery.inArray(lastValue, allSl) == -1)
								allSl.push(lastValue);
						}
						// $('#grossTaxAmount').html(EMPTY_STRING);
						clearError('taxScheduleCode_error');
						validator.clearMap();
						validator.setMtm(false);
						validator.setValue('ENTITY_CODE', getEntityCode());
						validator.setValue('EDIT_OLD_SL', editSl);
						validator.setValue('EDIT_NEW_SL', newEditSl);
						if (allSl.length != 0)
							validator.setValue("TOTAL_SL", allSl);
						validator.setValue(ACTION, USAGE);
						validator.setClass('patterns.config.web.forms.lss.eleasebean');
						validator.setMethod('getTaxDetails');
						validator.sendAndReceive();
						if (validator.getValue(ERROR) != EMPTY_STRING) {
							leaseApplicableTax_Grid.clearAll();
							// $('#grossTaxAmount').html(EMPTY_STRING);
							alert(validator.getValue(ERROR));
							// return false;
						} else {
							leaseApplicableTax_Grid.clearAll();
							leaseApplicableTax_Grid.loadXMLString(validator.getValue("TAX_XML"));
							// $('#grossTaxAmount').html(validator.getValue("GROSS_TAX_AMOUNT"));
							// taxTotal =
							// validator.getValue("GROSS_TAX_AMOUNT");
						}
					}

					financialAmount();

					rowId = parseInt(val) - 1;
					$('#startDate').val(financialScheduleNonOLGrid.cells2(rowId - 1, 2).getValue());
					$('#endDate').val(financialScheduleNonOLGrid.cells2(rowId - 1, 3).getValue());

					if (!isZero(financialScheduleNonOLGrid.cells2(rowId - 1, 13).getValue())) {
						$('#principalAmtFormat').val(financialScheduleNonOLGrid.cells2(rowId - 1, 4).getValue());
						$('#principalAmt').val(financialScheduleNonOLGrid.cells2(rowId - 1, 13).getValue());
					}
					if (!isZero(financialScheduleNonOLGrid.cells2(rowId - 1, 14).getValue())) {
						$('#interestAmt').val(financialScheduleNonOLGrid.cells2(rowId - 1, 14).getValue());
						$('#interestAmtFormat').val(financialScheduleNonOLGrid.cells2(rowId - 1, 5).getValue());
					}
					$('#totalRentAmtFormat').val(financialScheduleNonOLGrid.cells2(rowId - 1, 7).getValue());
					$('#finSerial').val(rowId);
					$('#principalAmtFormat').focus();

				}
			}
		}
	} else {
		alert(NO_PREVIOUS_RECORD);
		return false;
	}
}

function editCustomerAddress(returnValue) {
	if (!isEmpty($('#custID').val()) && !isEmpty($('#billAddress').val())) {
		var primaryKey = getEntityCode() + PK_SEPERATOR + $('#custID').val() + PK_SEPERATOR + $('#billAddress').val();
		var params = PK + '=' + primaryKey + '&' + SOURCE + '=' + MAIN;
		window.scroll(0, 0);
		win = showReturnWindow('ECUSTOMERADDRESS', getBasePath() + 'Renderer/reg/ecustomeraddress.jsp', 'Customer Address Maintenance', params, true, false, false, returnValue, 1170, 520);
		$('#customerID').val($('#custID').val());
		$('#addressSerial').val($('#billAddress').val());
		resetLoading();
	} else {
		setError('billAddress_error', CUST_ID_BILL_SERIAL_REQUIRED);
		$('#billAddress').focus();
	}
}

function editCustomerShipAddress(returnValue) {
	if (!isEmpty($('#custID').val()) && !isEmpty($('#shipAddress').val())) {
		var primaryKey = getEntityCode() + PK_SEPERATOR + $('#custID').val() + PK_SEPERATOR + $('#shipAddress').val();
		var params = PK + '=' + primaryKey + '&' + SOURCE + '=' + MAIN;
		window.scroll(0, 0);
		win = showReturnWindow('ECUSTOMERADDRESS', getBasePath() + 'Renderer/reg/ecustomeraddress.jsp', 'Customer Address Maintenance', params, true, false, false, returnValue, 1170, 520);
		$('#customerID').val($('#custID').val());
		$('#addressSerial').val($('#shipAddress').val());
		resetLoading();
	} else {
		setError('shipAddress_error', CUST_ID_BILL_SERIAL_REQUIRED);
		$('#shipAddress').focus();
	}
}

function addCustomerAddressDetails(returnValue) {
	if (!isEmpty($('#custID').val())) {
		var primaryKey = getEntityCode() + PK_SEPERATOR + $('#custID').val();
		var params = PK + '=' + primaryKey + '&' + SOURCE + '=' + MAIN;
		window.scroll(0, 0);
		win = showReturnWindow('ECUSTOMERADDRESS', getBasePath() + 'Renderer/reg/ecustomeraddress.jsp', 'Customer Address Maintenance', params, true, false, false, returnValue, 1170, 520);
		$('#customerID').val($('#custID').val());
		resetLoading();
	} else {
		setError('billAddress_error', CUSTOMER_ID_REQUIRED);
		$('#billAddress').focus();
	}
}

function addCustomerShipAddressDetails(returnValue) {
	if (!isEmpty($('#custID').val())) {
		var primaryKey = getEntityCode() + PK_SEPERATOR + $('#custID').val();
		var params = PK + '=' + primaryKey + '&' + SOURCE + '=' + MAIN;
		window.scroll(0, 0);
		win = showReturnWindow('ECUSTOMERADDRESS', getBasePath() + 'Renderer/reg/ecustomeraddress.jsp', 'Customer Address Maintenance', params, true, false, false, returnValue, 1170, 520);
		$('#customerID').val($('#custID').val());
		resetLoading();
	} else {
		setError('shipAddress_error', CUSTOMER_ID_REQUIRED);
		$('#shipAddress').focus();
	}
}

function oncloseReturnPopUp(returnValue, billAddress, win) {
	var serial = returnValue.split("|");
	if (win.getId() == 'ECUSTOMERADDRESS') {
		$('#' + billAddress).val(serial[2]);
	}
	return true;
}

function viewAddress() {
	if (!isEmpty($('#custID').val()) && !isEmpty($('#billAddress').val())) {
		var pkValue = getEntityCode() + PK_SEPERATOR + $('#custID').val() + PK_SEPERATOR + $('#billAddress').val();
		window.scroll(0, 0);
		showWindow('PREVIEW_VIEW', getBasePath() + 'Renderer/reg/qcustomeraddress.jsp', 'Customer Address Maintenance Finder', PK + '=' + pkValue + "&" + SOURCE + "=" + MAIN, true, false, false, 980, 575);
		resetLoading();
	} else {
		setError('billAddress_error', CUST_ID_BILL_SERIAL_REQUIRED);
		$('#billAddress').focus();
	}
}

function viewShipAddress() {
	if (!isEmpty($('#custID').val()) && !isEmpty($('#shipAddress').val())) {
		var pkValue = getEntityCode() + PK_SEPERATOR + $('#custID').val() + PK_SEPERATOR + $('#shipAddress').val();
		window.scroll(0, 0);
		showWindow('PREVIEW_VIEW', getBasePath() + 'Renderer/reg/qcustomeraddress.jsp', 'Customer Address Maintenance Finder', PK + '=' + pkValue + "&" + SOURCE + "=" + MAIN, true, false, false, 980, 575);
		resetLoading();
	} else {
		setError('shipAddress_error', CUST_ID_BILL_SERIAL_REQUIRED);
		$('#shipAddress').focus();
	}
}

function viewTaxMasterDetails(rowId) {
	var pkValue = rowId;
	qtaxCheck = false;
	window.scroll(0, 0);
	win = showWindow('qtax', getBasePath() + 'Renderer/cmn/qtax.jsp', 'Tax Maintenance Finder', PK + '=' + pkValue + "&" + SOURCE + "=" + MAIN, true, false, false, 980, 575);
	resetLoading();
}

function viewTaxMasterWiseDetails(rowId) {
	var pkValue = rowId;
	qtaxCheck = false;
	window.scroll(0, 0);
	win = showWindow('qtaxDetails', getBasePath() + 'Renderer/cmn/qtax.jsp', 'Tax Maintenance Finder', PK + '=' + pkValue + "&" + SOURCE + "=" + MAIN, true, false, false, 980, 575);
	resetLoading();
}

function loadTaxDetails() {
	if (financialScheduleNonOLGrid.getRowsNum() > 0 || financialScheduleOLGrid.getRowsNum() > 0) {
		var allSl = [];
		if ($('#leaseProductType').val() == 'O') {
			for (var i = 0; i < financialScheduleOLGrid.getRowsNum(); i++) {
				lastValue = parseInt(financialScheduleOLGrid.cells2(i, 23).getValue());
				if (jQuery.inArray(lastValue, allSl) == -1)
					allSl.push(lastValue);
			}
		} else {
			for (var i = 0; i < financialScheduleNonOLGrid.getRowsNum(); i++) {
				lastValue = parseInt(financialScheduleNonOLGrid.cells2(i, 23).getValue());
				if (jQuery.inArray(lastValue, allSl) == -1)
					allSl.push(lastValue);
			}
		}
		// $('#grossTaxAmount').html(EMPTY_STRING);
		clearError('taxScheduleCode_error');
		// leaseApplicableTax_Grid.clearAll();
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('ENTITY_CODE', getEntityCode());
		if (allSl.length != 0)
			validator.setValue("TOTAL_SL", allSl);
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.lss.eleasebean');
		validator.setMethod('getTaxDetails');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			// leaseApplicableTax_Grid.clearAll();
			// $('#grossTaxAmount').html(EMPTY_STRING);
			alert(validator.getValue(ERROR));
			// return false;
		} else {
			leaseApplicableTax_Grid.clearAll();
			leaseApplicableTax_Grid.loadXMLString(validator.getValue("TAX_XML"));
			// $('#grossTaxAmount').html(validator.getValue("GROSS_TAX_AMOUNT"));
			// taxTotal = validator.getValue("GROSS_TAX_AMOUNT");
		}
	}
}

function viewStateTaxDetails(rowId) {
	clearTaxDetails();
	var values = rowId.split("|");
	if (values[2] == '0.000' || values[2] == '0') {
		alert(NO_TAX_COMPUTED);
		return false;
	}
	taxSerial = values[0];
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('SCHEDULE_SL', values[0]);
	validator.setValue('MAIN_SL', values[1]);
	validator.setValue('TAX_TYPE', "1"); // Regular Tax
	validator.setValue('STATE_WISE', "STATE");
	validator.setClass('patterns.config.web.forms.lss.eleasebean');
	validator.setMethod('getTaxStateCentralWiseDetails');
	validator.sendAndReceiveAsync(loadTaxWiseDetails);
}

function viewCentralTaxDetails(rowId) {
	clearTaxDetails();
	var values = rowId.split("|");
	if (values[2] == '0.000' || values[2] == '0') {
		alert(NO_TAX_COMPUTED);
		return false;
	}
	taxSerial = values[0];
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('SCHEDULE_SL', values[0]);
	validator.setValue('MAIN_SL', values[1]);
	validator.setValue('TAX_TYPE', "1"); // Regular Tax
	validator.setValue('CENTRAL_WISE', "CENTRAL");
	validator.setClass('patterns.config.web.forms.lss.eleasebean');
	validator.setMethod('getTaxStateCentralWiseDetails');
	validator.sendAndReceiveAsync(loadTaxWiseDetails);
}

function viewServiceTaxDetails(rowId) {
	clearTaxDetails();
	var values = rowId.split("|");
	if (values[2] == '0.000' || values[2] == '0') {
		alert(NO_TAX_COMPUTED);
		return false;
	}
	taxSerial = values[0];
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('SCHEDULE_SL', values[0]);
	validator.setValue('MAIN_SL', values[1]);
	validator.setValue('TAX_TYPE', "2"); // Service Tax
	validator.setClass('patterns.config.web.forms.lss.eleasebean');
	validator.setMethod('getTaxStateCentralWiseDetails');
	validator.sendAndReceiveAsync(loadTaxWiseDetails);
}

function viewCessTaxDetails(rowId) {
	clearTaxDetails();
	var values = rowId.split("|");
	if (values[2] == '0.000' || values[2] == '0') {
		alert(NO_TAX_COMPUTED);
		return false;
	}
	taxSerial = values[0];
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('SCHEDULE_SL', values[0]);
	validator.setValue('MAIN_SL', values[1]);
	validator.setValue('TAX_TYPE', "3"); // Cess
	validator.setClass('patterns.config.web.forms.lss.eleasebean');
	validator.setMethod('getTaxStateCentralWiseDetails');
	validator.sendAndReceiveAsync(loadTaxWiseDetails);
}

function loadTaxWiseDetails() {
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		alert(validator.getValue(ERROR));
		clearTaxDetails();
		return false;
	} else if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		win = showWindow('taxDetails', EMPTY_STRING, TAX_BREAKUP_DTL, EMPTY_STRING, true, false, false, 1050, 550);
		leaseTaxWiseDetails_Grid.loadXMLString(validator.getValue("TAX_GRID_DETAILS"));
		$('#totalTaxAmount').html(validator.getValue("TOTAL_TAX"));
		$('#stateCode').val(validator.getValue("STATE_CODE"));
		$('#stateCode_desc').html(validator.getValue("STATE_DESC"));

		if ($('#leaseProductType').val() != 'O') {
			show('prinsInterestWise');
			$('#monthSl').val(financialScheduleNonOLGrid.cells2(taxSerial - 1, 0).getValue());
			$('#startTaxDate').val(financialScheduleNonOLGrid.cells2(taxSerial - 1, 2).getValue());
			$('#endTaxDate').val(financialScheduleNonOLGrid.cells2(taxSerial - 1, 3).getValue());

			$('#principalTaxAmtFormat').val(financialScheduleNonOLGrid.cells2(taxSerial - 1, 4).getValue());
			$('#interestTaxAmtFormat').val(financialScheduleNonOLGrid.cells2(taxSerial - 1, 5).getValue());
			$('#execTaxAmtFormat').val(financialScheduleNonOLGrid.cells2(taxSerial - 1, 6).getValue());
			$('#rentalTaxAmtFormat').val(financialScheduleNonOLGrid.cells2(taxSerial - 1, 7).getValue());
		} else {
			hide('prinsInterestWise');
			$('#monthSl').val(financialScheduleOLGrid.cells2(taxSerial - 1, 0).getValue());
			$('#startTaxDate').val(financialScheduleOLGrid.cells2(taxSerial - 1, 2).getValue());
			$('#endTaxDate').val(financialScheduleOLGrid.cells2(taxSerial - 1, 3).getValue());

			$('#principalTaxAmtFormat').val(financialScheduleOLGrid.cells2(taxSerial - 1, 4).getValue());
			$('#interestTaxAmtFormat').val(financialScheduleOLGrid.cells2(taxSerial - 1, 5).getValue());
			$('#execTaxAmtFormat').val(financialScheduleOLGrid.cells2(taxSerial - 1, 6).getValue());
			$('#rentalTaxAmtFormat').val(financialScheduleOLGrid.cells2(taxSerial - 1, 7).getValue());
		}
	}
}

function clickNextTab() {
	var actvId = lease.getActiveTab();
	if (actvId == 'lease_0') {
		doTabbarClick('lease_1');
		lease._setTabActive('lease_1', true);
		setFocusLast('noOfAssets');
	} else if (actvId == 'lease_1') {
		doTabbarClick('lease_2');
		lease._setTabActive('lease_2', true);
		setFocusLast('tenor');
	} else if (actvId == 'lease_2') {
		doTabbarClick('lease_3');
		lease._setTabActive('lease_3', true);
		setFocusLast('grossAssetCostFormat');
	} else if (actvId == 'lease_3') {
		doTabbarClick('lease_4');
		lease._setTabActive('lease_4', true);
		setFocusLast('grossNetTDS');
	}
}

function clickPreviousTab() {
	var actvId = lease.getActiveTab();
	if (actvId == 'lease_1') {
		doTabbarClick('lease_0');
		lease._setTabActive('lease_0', true);
		setFocusLast('leaseComDate');
	} else if (actvId == 'lease_2') {
		doTabbarClick('lease_1');
		lease._setTabActive('lease_1', true);
		setFocusLast('noOfAssets');
	} else if (actvId == 'lease_3') {
		doTabbarClick('lease_2');
		lease._setTabActive('lease_2', true);
		setFocusLast('tenor');
	} else if (actvId == 'lease_4') {
		doTabbarClick('lease_3');
		lease._setTabActive('lease_3', true);
		setFocusLast('grossAssetCostFormat');
	}
}

function afterClosePopUp(win) {
	if (qtaxCheck) {
		if ('taxDetails' == win.getId()) {
			dhxWindows = null;
			win = null;
			win = showWindow('showFinancialGrid', EMPTY_STRING, 'Financial and Tax Details', EMPTY_STRING, true, false, false, 1330, 635);
			if (win)
				win.showInnerScroll();
			leaseFinance._setTabActive('leaseFinance_0', true);
			if (hideUnhide) {
				show('agreeAndContinue');
				hide('closeFinGrid');
			} else {
				show('closeFinGrid');
				hide('agreeAndContinue');
			}
			hideWin = win;
			qtaxCheck = true;
			return true;
		} else if ('qtax' == win.getId()) {
			dhxWindows = null;
			win = null;
			win = showWindow('showFinancialGrid', EMPTY_STRING, 'Financial and Tax Details', EMPTY_STRING, true, false, false, 1330, 635);
			if (win)
				win.showInnerScroll();
			leaseFinance._setTabActive('leaseFinance_0', true);
			if (hideUnhide) {
				show('agreeAndContinue');
				hide('closeFinGrid');
			} else {
				show('closeFinGrid');
				hide('agreeAndContinue');
			}
			hideWin = win;
			qtaxCheck = true;
			return true;
		} else if ('qtaxDetails' == win.getId()) {
			dhxWindows = null;
			win = null;
			win = showWindow('taxDetails', EMPTY_STRING, TAX_BREAKUP_DTL, EMPTY_STRING, true, false, false, 1050, 550);
			hideWin = win;
			qtaxCheck = true;
			return true;
		}
	}
	qtaxCheck = true;
	return true;
}

function validateFinancialGrid(valMode) {
	if (leaseRentalCharges_Grid.getRowsNum() == 0) {
		alert(NO_OF_TENOR_UPTO_TENOR_EQUAL);
		return true;
	}
	var totalTenor = 0;
	for (var i = 0; i < leaseRentalCharges_Grid.getRowsNum(); i++) {
		totalTenor = totalTenor + parseInt(leaseRentalCharges_Grid.cells2(i, 1).getValue());
	}
	if (parseInt($('#tenor').val()) == totalTenor) {
		if (tenor_val() && repayFreq_val(false) && firstPaymet_val(false) && internalRateReturn_val() && commencementValueFormat_val(false) && shipAddress_val() && billBranch_val()) {
			$('#xmlleaseRentalChargesGrid').val(leaseRentalCharges_Grid.serialize());
			$('#noOfslab').html(EMPTY_STRING);
			$('#valueNegative').html(EMPTY_STRING);
			showMessage(getProgramID(), Message.PROGRESS);
			validator.clearMap();
			validator.setMtm(false);

			validator.setValue('MAIN_SL', $('#spSerial').val());
			if (!isEmpty($('#internalRateReturn').val()))
				validator.setValue('IRR_VALUE', $('#internalRateReturn').val());
			else
				validator.setValue('IRR_VALUE', '0');
			validator.setValue('TENOR_MONTH', $('#tenor').val());
			validator.setValue('ADVANCE_ARREARS_FLAG', $('#advArrers').val());
			validator.setValue('COMMENCEMENT_VALUE', $('#commencementValue').val());
			validator.setValue('LEAVE_TYPE', $('#leaseProductType').val());

			validator.setValue('BILL_FROM_DATE', $('#rentDate').val());
			validator.setValue('BILL_UPTO_DATE', $('#lastDate').val());
			validator.setValue('MONTH_FREQ', $('#repayFreq').val());
			validator.setValue('TENOR_MONTH', $('#tenor').val());
			validator.setValue('XML_RENT_AMT', $('#xmlleaseRentalChargesGrid').val());
			validator.setValue('EDIT_REQ', $('#leaseProductType').val());

			validator.setValue('ENTITY_CODE', getEntityCode());
			validator.setValue('STATE_CODE', $('#billState').val());
			validator.setValue('SHIP_STATE_CODE', $('#shippingState').val());
			validator.setValue('TAX_SCHEDULE_CODE', $('#taxScheduleCode').val());
			validator.setValue('TAX_CCY', $('#assetCurrency').val());
			validator.setValue('TAX_ON_DATE', getCBD());

			if ($('#custLeaseCode').val() == MODIFY) {
				validator.setValue('LESSEE_CODE', $('#custLeaseCode').val());
				validator.setValue('AGREEMENT_NO', $('#agreementNo').val());
				validator.setValue('SCHEDULE_ID', $('#scheduleID').val());
				validator.setValue('LEASE_PRODUCT_CODE', $('#leaseProduct').val());
				validator.setValue('CUSTOMER_ID', $('#custID').val());
			} else {
				validator.setValue('LESSEE_CODE', ZERO);
				validator.setValue('AGREEMENT_NO', ZERO);
				validator.setValue('SCHEDULE_ID', ZERO);
				validator.setValue('LEASE_PRODUCT_CODE', $('#leaseProduct').val());
				validator.setValue('CUSTOMER_ID', ZERO);
			}
			validator.setValue(ACTION, $('#action').val());
			validator.setClass('patterns.config.web.forms.lss.eleasebean');
			validator.setMethod('getFinancialScheduleDetails');
			validator.sendAndReceive();
			if (validator.getValue(ERROR) != EMPTY_STRING) {
				financialScheduleNonOLGrid.clearAll();
				financialScheduleOLGrid.clearAll();
				hide('showFinancialGrid');
				hide('closeFinGrid');
				hide('agreeAndContinue');
				setError('leaseRentRate_error', validator.getValue(ERROR));
				// var ros =
				// gridvar.getCheckedRows(leaseRentalCharges_Grid.getRowsNum());
				// leaseRentalCharges_Grid.deleteRow(ros);
				// leaseRentalCharges_Grid.setUserData("", "grid_edit_id",
				// EMPTY_STRING);
				// alert(validator.getValue(ERROR));
				hideMessage(getProgramID());
				return false;
			}
			hideMessage(getProgramID());
			return true;
		} else {
			if (!$('#uptoToner').prop('readonly')) {
				setError('uptoToner_error', FIRSR_PAY_DATE_INCORRECT);
				// alert(FIRSR_PAY_DATE_INCORRECT);
				$('#uptoToner').focus();
				return false;
			} else {
				setError('uptoToner_error', FIRSR_PAY_DATE_INCORRECT);
				$('#rentalFormat').focus();
				// alert(FIRSR_PAY_DATE_INCORRECT);
				return false;
			}
		}
	}
}

function doPrint() {
	if ($('#leaseProductType').val() != 'O') {
		if (financialScheduleNonOLGrid.getRowsNum() == 0) {
			alert('Financial schedule details not available , cant download the excel');
			return false;
		}
	} else {
		if (financialScheduleOLGrid.getRowsNum() == 0) {
			alert('Financial schedule details not available , cant download the excel');
			return false;
		}
	}
	showMessage(getProgramID(), Message.PROGRESS);
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('ENTITY_CODE', getEntityCode());
	validator.setValue('MAIN_SL', $('#spSerial').val());
	validator.setValue('LEASE_TYPE', $('#leaseProductType').val());
	validator.setValue(ACTION, USAGE);
	validator.setClass('patterns.config.web.forms.lss.eleasebean');
	validator.setMethod('downloadFinancialWiseGrid');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		alert(validator.getValue(ERROR));
		return false;
	} else {
		hideMessage(getProgramID());
		window.location = getBasePath() + 'servlet/ReportDownloadProcessor?' + REPORT_ID + '=' + validator.getValue(REPORT_ID);
		return true;
	}
	return true;
}