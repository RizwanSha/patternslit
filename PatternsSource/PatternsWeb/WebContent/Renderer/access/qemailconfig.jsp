<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.access.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/access/qemailconfig.js" />
	</web:dependencies>
	<web:loadPK />
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="qemailconfig.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:viewContent id="po_view1">
				<web:dividerBlock>
					<web:sectionBlock width="width-100p" align="left">
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="180px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="iemailconfig.emailcode" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:codeDisplay property="emailCode" id="emailCode" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="form.effectivedate" var="common" mandatory="true" />
									</web:column>
									<web:column>
										<type:dateDisplay property="effectiveDate" id="effectiveDate" />
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="180px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="iemailconfig.sendprotocol" var="program" />
									</web:column>
									<web:column>
										<type:comboDisplay property="sendProtocol" id="sendProtocol" datasourceid="COMMON_SENDPROTOCOL" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="iemailconfig.sslrequired" var="program" />
									</web:column>
									<web:column>
										<type:checkboxDisplay property="sslRequired" id="sslRequired" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="iemailconfig.receiveprotocol" var="program" />
									</web:column>
									<web:column>
										<type:comboDisplay property="receiveProtocol" id="receiveProtocol" datasourceid="COMMON_REVDPROTOCOL" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:section>
							<web:viewTitle var="program" key="iemailconfig.section1" />
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="180px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowOdd>
									<web:column>
										<web:legend key="iemailconfig.sendername" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:nameDisplay property="senderName" id="senderName" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="iemailconfig.senderemail" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:emailDisplay property="senderEmail" id="senderEmail" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:section>
							<web:viewTitle var="program" key="iemailconfig.section2" />
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="180px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowOdd>
									<web:column>
										<web:legend key="iemailconfig.proxyrequired" var="program" />
									</web:column>
									<web:column>
										<type:checkboxDisplay property="proxyRequired" id="proxyRequired" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="iemailconfig.proxyserver" var="program" />
									</web:column>
									<web:column>
										<type:serverAddressDisplay property="proxyServer" id="proxyServer" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="iemailconfig.proxyserverport" var="program" />
									</web:column>
									<web:column>
										<type:portDisplay property="proxyServerPort" id="proxyServerPort" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="iemailconfig.proxyserverauthentication" var="program" />
									</web:column>
									<web:column>
										<type:checkboxDisplay property="proxyServerAuthentication" id="proxyServerAuthentication" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="iemailconfig.proxyaccountname" var="program" />
									</web:column>
									<web:column>
										<type:nameDisplay property="proxyAccountName" id="proxyAccountName" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="iemailconfig.proxyaccountpassword" var="program" />
									</web:column>
									<web:column>
										<type:passwordDisplay property="proxyAccountPassword" id="proxyAccountPassword" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:section>
							<web:viewTitle var="program" key="iemailconfig.section3" />
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="180px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowOdd>
									<web:column>
										<web:legend key="iemailconfig.outgoingmailserver" var="program" />
									</web:column>
									<web:column>
										<type:serverAddressDisplay property="outgoingMailServer" id="outgoingMailServer" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="iemailconfig.outgoingmailserverport" var="program" />
									</web:column>
									<web:column>
										<type:portDisplay property="outgoingMailServerPort" id="outgoingMailServerPort" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="iemailconfig.outgoingmailserverauthentication" var="program" />
									</web:column>
									<web:column>
										<type:checkboxDisplay property="outgoingMailServerAuthentication" id="outgoingMailServerAuthentication" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="iemailconfig.outgoingaccountname" var="program" />
									</web:column>
									<web:column>
										<type:nameDisplay property="outgoingAccountName" id="outgoingAccountName" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="iemailconfig.outgoingaccountpassword" var="program" />
									</web:column>
									<web:column>
										<type:passwordDisplay property="outgoingAccountPassword" id="outgoingAccountPassword" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="form.remarks" var="common" />
									</web:column>
									<web:column>
										<type:remarksDisplay property="remarks" id="remarks" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:auditDisplay />
					</web:sectionBlock>
				</web:dividerBlock>
			</web:viewContent>
		</web:viewSubPart>
	</web:viewPart>
</web:fragment>