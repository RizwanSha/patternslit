var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MPKICERTAUTH';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}
function clearFields() {
	$('#code').val(EMPTY_STRING);
	$('#description').val(EMPTY_STRING);
	$('#certAuthHttpUrl').val(EMPTY_STRING);
	$('#certAuthLdapUrl').val(EMPTY_STRING);
	$('#ocspHttpUrl').val(EMPTY_STRING);
	$('#crlHttpUrl').val(EMPTY_STRING);
	$('#heirarchyLevel').val(EMPTY_STRING);
	$('#parentCode').val(EMPTY_STRING);
	$('#httpRefreshTime').val(EMPTY_STRING);
	$('#crlLdapUrl').val(EMPTY_STRING);
	$('#ldapRefreshTime').val(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
}
function loadData() {
	$('#code').val(validator.getValue('CODE'));
	$('#description').val(validator.getValue('DESCRIPTION'));
	$('#certAuthHttpUrl').val(validator.getValue('CERT_AUTH_HTTP_URL'));
	$('#certAuthLdapUrl').val(validator.getValue('CERT_AUTH_LDAP_URL'));
	$('#ocspAvail').prop('checked',decodeSTB(validator.getValue('OCSP_VERIFY_AVLBL')));
	$('#ocspHttpUrl').val(validator.getValue('OCSP_HTTP_URL'));
	$('#crlHttpUrl').val(validator.getValue('CRL_HTTP_URL'));
	$('#httpRefreshTime').val(validator.getValue('HTTP_REFRESH_INTERVAL'));
	$('#crlLdapUrl').val(validator.getValue('CRL_LDAP_URL'));
	$('#ldapRefreshTime').val(validator.getValue('LDAP_REFRESH_INTERVAL'));
	$('#enabled').prop('checked',decodeSTB(validator.getValue('ENABLED')));
	$('#remarks').val(validator.getValue('REMARKS'));
	$('#heirarchyLevel').val(validator.getValue('HIERARCHY_LEVEL'));
	if ($('#heirarchyLevel').val() != '1') {
		$('#parentCode').val(validator.getValue('PARENT_CODE'));
		$('#parentCode_desc').html(validator.getValue('PKICERTAUTH_DESCRIPTION'));
	}
	loadAuditFields(validator);
}