<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.access.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/access/epkicertcrl.js" />
	</web:dependencies>
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="epkicertcrl.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/access/epkicertcrl" id="epkicertcrl" method="POST">
				<web:dividerBlock>
					<type:optionBox add="true" modify="true" view="true" />
				</web:dividerBlock>
				<web:viewContent id="po_view1" styleClass="hidden">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="220px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="epkicertcrl.rootcacode" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:code property="rootCaCode" id="rootCaCode" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="epkicertcrl.changedatetime" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:dateDisplay property="changeDateTime" id="changeDateTime" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="220px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="epkicertcrl.enabled" var="program" />
										</web:column>
										<web:column>
											<type:checkbox property="enabled" id="enabled" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<type:fileDetails />
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="220px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="form.remarks" var="common" mandatory="true" />
										</web:column>
										<web:column>
											<type:remarks property="remarks" id="remarks" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column span="2">
											<web:submitReset />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
						</web:sectionBlock>
					</web:dividerBlock>
				</web:viewContent>
				<web:element property="command" />
				<web:element property="action" />
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
	<web:contextSearch />
	<web:queryGrid />
	<web:TBAGrid />
</web:fragment>