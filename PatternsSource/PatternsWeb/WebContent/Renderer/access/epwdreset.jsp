<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<%@taglib prefix="message" tagdir="/WEB-INF/tags/message"%>
<web:bundle baseName="patterns.config.web.forms.access.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/access/epwdreset.js" />
	</web:dependencies>
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="epwdreset.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/access/epwdreset" id="epwdreset" method="POST">
				<web:dividerBlock>
					<type:optionBox add="false" modify="true" view="true" />
				</web:dividerBlock>
				<web:viewContent id="po_view1" styleClass="hidden">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="form.userid" var="common" mandatory="true" />
										</web:column>
										<web:column>
											<type:code property="userID" id="userID" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="epwdreset.dateofreset" var="program" />
										</web:column>
										<web:column>
											<type:dateDisplay property="dateOfReset" id="dateOfReset" />
										</web:column>
									</web:rowOdd>
											<web:rowEven>
										<web:column span="2">
											<div class="legend info-message" id="spanPasswordPolicy"></div>
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="epwdreset.createpassword" var="program"  mandatory="true" />
										</web:column>
										<web:column>
											<type:password property="createPassword" id="createPassword" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="epwdreset.confirmpassword" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:password property="confirmPassword" id="confirmPassword" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="form.remarks" var="common" mandatory="true" />
										</web:column>
										<web:column>
											<type:remarks property="remarks" id="remarks" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column span="2">
											<web:submitReset />
										</web:column>
									</web:rowEven>
									
								</web:table>
							</web:section>
						</web:sectionBlock>
					</web:dividerBlock>
				</web:viewContent>
				<web:section>
					<message:messageBox id="epwdreset" />
				</web:section>
				<web:element property="command" />
				<web:element property="action" />
				<web:element property="pwdDelivery" />
				<web:element property="pwdDeliveryType" />
				<web:element property="minLength" />
				<web:element property="minAlpha" />
				<web:element property="minNumeric" />
				<web:element property="minSpecial" />
				<web:element property="passwordPolicy" />
				<web:element property="randomSalt" />
				<web:element property="loginNewPasswordHashed" />
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
	<web:queryGrid />
	<web:TBAGrid />
	<web:contextSearch/>
</web:fragment>