var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'EBKANNOUNCEDTLS';
function init() {
	refreshQueryGrid();
	refreshTBAGrid();
	if (_redisplay) {
		$('#announceID').attr('readonly', true);
		if ($('#action').val() == ADD) {
			$('#announceDisabled').attr("disabled", "disabled"); 
			userID_val();
			roleType_val();
		} else {
			$('#startDate').attr('readonly', true);
			$('#startDate_pic').attr("disabled", "disabled"); 
			$('#visibility').attr("disabled", "disabled"); 
			$('#userID').attr('readonly', true);
			$('#userID_pic').attr("disabled", "disabled"); 
			$('#roleType').attr("disabled", "disabled"); 
			$('#customerCode').attr('readonly', true);
			$('#customerCode_pic').attr("disabled", "disabled"); 
			$('#branchCode').attr('readonly', true);
			$('#branchCode_pic').attr("disabled", "disabled"); 
			$('#visibility').val($('#temVisibility').val());
			$('#roleType').val($('#tempRoleType').val());
		}
		title();
	}
}
function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'EBKANNOUNCEDTLS_MAIN', queryGrid);
}
function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}
function clearFields() {
	
	$('#announceID').val(EMPTY_STRING);
	$('#announceID_error').html(EMPTY_STRING);
	$('#tempAnnounceId').val(EMPTY_STRING);
	$('#startDate').val(EMPTY_STRING);
	$('#startDate_error').html(EMPTY_STRING);
	$('#expiryDate').val(EMPTY_STRING);
	$('#expiryDate_error').html(EMPTY_STRING);
	$('#displayTitle').val(EMPTY_STRING);
	$('#displayTitle_error').html(EMPTY_STRING);
	$('#url').val(EMPTY_STRING);
	$('#url_error').html(EMPTY_STRING);
	$('#urlTitle').val(EMPTY_STRING);
	$('#urlTitle_error').html(EMPTY_STRING);
	$('#visibility').val(EMPTY_STRING);
	$('#visibility_error').html(EMPTY_STRING);
	$('#userID').val(EMPTY_STRING);
	$('#userID_error').html(EMPTY_STRING);
	$('#userID_desc').html(EMPTY_STRING);
	$('#roleType').val(EMPTY_STRING);
	$('#roleType_error').html(EMPTY_STRING);
	$('#customerCode').val(EMPTY_STRING);
	$('#customerCode_error').html(EMPTY_STRING);
	$('#customerCode_desc').html(EMPTY_STRING);
	$('#branchCode').val(EMPTY_STRING);
	$('#branchCode_error').html(EMPTY_STRING);
	$('#branchCode_desc').html(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
}
function title() {
	if ($('#externalLink').is(':checked')) {
		$('#url').removeAttr("disabled"); 
		$('#urlTitle').removeAttr("disabled"); 
	} else {
		$('#url').attr("disabled", "disabled"); 
		$('#urlTitle').attr("disabled", "disabled"); 
	}
}
function add() {
	
	$('#startDate').focus();
	$('#announceID').attr('readonly', true);
	$('#announceDisabled').attr("disabled", "disabled"); 
	$('#externalLink').prop('checked',false);
	$('#visibility').removeAttr("disabled"); 
	$('#startDate').attr('readonly', false);
	$('#startDate_pic').removeAttr("disabled"); 
	$('#userID').attr('readonly', false);
	$('#customerCode').attr('readonly', false);
	$('#branchCode').attr('readonly', false);
	$('#userID').attr("disabled", "disabled"); 
	$('#userID_pic').attr("disabled", "disabled"); 
	$('#roleType').attr("disabled", "disabled"); 
	$('#customerCode').attr("disabled", "disabled"); 
	$('#customerCode_pic').attr("disabled", "disabled"); 
	$('#branchCode').attr("disabled", "disabled"); 
	$('#branchCode_pic').attr("disabled", "disabled"); 
	title();
}
function modify() {
	
	$('#displayTitle').focus();
	$('#announceID').attr('readonly', true);
	enableFields();
	if ($('#action').val() == MODIFY) {
		$('#startDate').attr('readonly', true);
		$('#startDate_pic').attr("disabled", "disabled"); 
		$('#visibility').attr("disabled", "disabled"); 
		$('#userID').attr('readonly', true);
		$('#roleType').attr("disabled", "disabled"); 
		$('#customerCode').attr('readonly', true);
		$('#branchCode').attr('readonly', true);
	}
}
function loadData() {
	$('#announceID').val(validator.getValue('ANNOUNCE_ID'));
	$('#tempAnnounceId').val(validator.getValue('ANNOUNCE_ID'));
	$('#startDate').val(validator.getValue('ANNOUNCE_START_DT'));
	$('#expiryDate').val(validator.getValue('ANNOUNCE_END_DT'));
	$('#displayTitle').val(validator.getValue('ANNOUNCE_TITLE'));
	$('#externalLink').prop('checked',decodeSTB(validator.getValue('EXTERNAL_LINK')));
	$('#url').val(validator.getValue('ANNOUNCE_URL'));
	$('#urlTitle').val(validator.getValue('ANNOUNCE_URL_TITLE'));
	$('#visibility').val(validator.getValue('ANNOUNCE_VISIBLITY'));
	$('#announceDisabled').prop('checked',decodeSTB(validator.getValue('ANNOUNCE_DISABLED')));
	$('#userID').val(validator.getValue('ANNOUNCE_USER_ID'));
	$('#roleType').val(validator.getValue('ANNOUNCE_ROLE_TYPE'));
	$('#customerCode').val(validator.getValue('ANNOUNCE_CUSTOMER_CODE'));
	$('#branchCode').val(validator.getValue('ANNOUNCE_BRANCH_CODE'));
	$('#temVisibility').val(validator.getValue('ANNOUNCE_VISIBLITY'));
	$('#tempRoleType').val(validator.getValue('ANNOUNCE_ROLE_TYPE'));
	$('#remarks').val(validator.getValue('REMARKS'));
	title();
	if ($('#action').val() == MODIFY) {
		if ($('#announceDisabled').is(':checked')) {
			$('#announceDisabled').attr("disabled", "disabled"); 
		} else {
			$('#announceDisabled').removeAttr("disabled"); 
		}
	} else {
		if ($('#userID').val() != EMPTY_STRING) {
			userID_val();
		} else if ($('#roleType').val() != EMPTY_STRING) {
			roleType_val();
		}
	}
	resetLoading();
	if ($('#userID').val() != EMPTY_STRING) {
		validator.reset();
		validator.setMtm(false);
		validator.setValue('USER_ID', $('#userID').val());
		validator.setClass('patterns.config.web.forms.access.ebkannouncedtlsbean');
		validator.setMethod('fetchUserName');
		validator.sendAndReceive();
		$('#userID_desc').html(validator.getValue('BANKUSERS_USER_NAME'));
	}
	if ($('#customerCode').val() != EMPTY_STRING) {
		validator.reset();
		validator.setMtm(false);
		validator.setValue('CUSTOMER_CODE', $('#customerCode').val());
		validator.setClass('patterns.config.web.forms.access.ebkannouncedtlsbean');
		validator.setMethod('fetchCustomerName');
		validator.sendAndReceive();
		$('#customerCode_desc').html(validator.getValue('CUSTOMER_CUSTOMER_NAME'));
	}
	if ($('#branchCode').val() != EMPTY_STRING) {
		validator.reset();
		validator.setMtm(false);
		validator.setValue('MBRN_CODE', $('#branchCode').val());
		validator.setClass('patterns.config.web.forms.access.ebkannouncedtlsbean');
		validator.setMethod('fetchBranchName');
		validator.sendAndReceive();
		$('#branchCode_desc').html(validator.getValue('MBRN_MBRN_NAME'));
	}
}
function view(source, primaryKey) {
	hideParent('access/qbkannouncedtls', source, primaryKey);
	resetLoading();
}
function doHelp(id) {
	switch (id) {
	case 'userID':
		help(CURRENT_PROGRAM_ID, 'USERS', $('#userID').val(), EMPTY_STRING, $('#userID'));
		break;
	case 'branchCode':
		help(CURRENT_PROGRAM_ID, 'BRANCHES', $('#branchCode').val(), EMPTY_STRING, $('#branchCode'));
		break;
	case 'customerCode':
		help(CURRENT_PROGRAM_ID, 'CUSTOMERS', $('#customerCode').val(), EMPTY_STRING, $('#customerCode'));
		break;
	}
}
function validate(id) {
	switch (id) {
	case 'startDate':
		startDate_val();
		break;
	case 'expiryDate':
		expiryDate_val();
		break;
	case 'displayTitle':
		displayTitle_val();
		break;
	case 'url':
		url_val();
		break;
	case 'urlTitle':
		urlTitle_val();
		break;
	case 'visibility':
		visibility_val();
		break;
	case 'userID':
		userID_val();
		break;
	case 'roleType':
		roleType_val();
		break;
	case 'customerCode':
		combinationCode_val();
		break;
	case 'branchCode':
		combinationCode_val();
		break;
	case 'remarks':
		remarks_val();
		break;
	}
}
function startDate_val() {
	if ($('#action').val() != MODIFY) {
		var value = $('#startDate').val();
		var cbd = getCBD();

		if (isEmpty(value)) {
			setError('startDate_error', MANDATORY);
			return false;
		}
		if (!isDate(value)) {
			setError('startDate_error', INVALID_DATE);
			return false;
		}
		if (!isDateGreaterEqual(value, cbd)) {
			setError('startDate_error', DATE_GECBD);
			return false;
		} else {
			clearError('startDate_error');
			return true;
		}
	}

	return true;
}
function expiryDate_val() {
	var value = $('#expiryDate').val();
	var startdate = $('#startDate').val();
	clearError('expiryDate_error');
	var cbd = getCBD();
	if (isEmpty(value)) {
		setError('expiryDate_error', MANDATORY);
		return false;
	}
	if (!isDate(value)) {
		setError('expiryDate_error', INVALID_DATE);
		return false;
	}
	if (!isDateGreaterEqual(value, startdate)) {
		setError('expiryDate_error', DATE_GESD);
		return false;
	} else {
		clearError('expiryDate_error');
		return true;
	}
}
function displayTitle_val() {
	var value = $('#displayTitle').val();
	clearError('displayTitle_error');
	if (isEmpty(value)) {
		setError('displayTitle_error', MANDATORY);
		return false;
	}
	if (!isValidRemarks(value)) {
		setError('displayTitle_error', INVALID_DISPLAY_TITLE);
		return false;
	}
	return true;
}
function url_val() {
	var value = $('#url').val();
	clearError('url_error');
	if (isEmpty(value)) {
		setError('url_error', MANDATORY);
		return false;
	} else if (!isValidUrl(value)) {
		setError('url_error', INVALID_URL);
		return false;
	}
	return true;
}
function urlTitle_val() {
	var value = $('#urlTitle').val();
	clearError('urlTitle_error');
	if (isEmpty(value)) {
		setError('urlTitle_error', MANDATORY);
		return false;
	}
	if (!isValidRemarks(value)) {
		setError('displayTitle_error', INVALID_URL_TITLE);
		return false;
	}
	return true;
}
function visibility_val() {
	var value = $('#visibility').val();
	clearError('visibility_error');
	if (isEmpty(value)) {
		setError('visibility_error', MANDATORY);
		return false;
	}
	return true;
}
function userID_val() {
	var value = $('#userID').val();
	clearError('userID_error');
	if (!$('#userID').val() == EMPTY_STRING) {
		$('#userID').removeAttr("disabled"); 
		$('#roleType').attr("disabled", "disabled"); 
		$('#customerCode').attr("disabled", "disabled"); 
		$('#branchCode').attr("disabled", "disabled"); 
		$('#customerCode_pic').attr("disabled", "disabled"); 
		$('#branchCode_pic').attr("disabled", "disabled"); 
		$('#roleType_error').html(EMPTY_STRING);
		$('#branchCode_error').html(EMPTY_STRING);
		$('#branchCode_desc').html(EMPTY_STRING);
		$('#customerCode_error').html(EMPTY_STRING);
		$('#customerCode_desc').html(EMPTY_STRING);
		$('#roleType').val(EMPTY_STRING);
		$('#customerCode').val(EMPTY_STRING);
		$('#branchCode').val(EMPTY_STRING);
	} else {
		$('#roleType').removeAttr("disabled"); 
	}
	return true;
}
function change(id) {
	switch (id) {
	case 'roleType':
		roleType_val();
		break;
	case 'visibility':
		enableFields();
		break;
	}
}
function roleType_val() {
	var value = $('#userID').val();
	var roletype = $('#roleType').val();
	if ($('#roleType').val() == EMPTY_STRING) {
		$('#userID').removeAttr("disabled"); 
		$('#userID_pic').removeAttr("disabled"); 
		$('#customerCode').attr("disabled", "disabled"); 
		$('#customerCode_pic').attr("disabled", "disabled"); 
		$('#customerCode_error').html(EMPTY_STRING);
		$('#customerCode_desc').html(EMPTY_STRING);
		$('#customerCode').val(EMPTY_STRING);
		$('#branchCode').attr("disabled", "disabled"); 
		$('#branchCode_pic').attr("disabled", "disabled"); 
		$('#branchCode_error').html(EMPTY_STRING);
		$('#branchCode_desc').html(EMPTY_STRING);
		$('#branchCode').val(EMPTY_STRING);
	} else {
		$('#userID').attr("disabled", "disabled"); 
		$('#userID_pic').attr("disabled", "disabled"); 
		$('#userID_desc').html(EMPTY_STRING);
		$('#userID_error').html(EMPTY_STRING);
		$('#userID').val(EMPTY_STRING);
		if (roletype == 5 || roletype == 6) {
			$('#customerCode').removeAttr("disabled"); 
			$('#customerCode_pic').removeAttr("disabled"); 
			$('#branchCode').attr("disabled", "disabled"); 
			$('#branchCode_pic').attr("disabled", "disabled"); 
			$('#branchCode_error').html(EMPTY_STRING);
			$('#branchCode_desc').html(EMPTY_STRING);
			$('#branchCode').val(EMPTY_STRING);
		} else {
			$('#customerCode').attr("disabled", "disabled"); 
			$('#customerCode_pic').attr("disabled", "disabled"); 
			$('#customerCode_error').html(EMPTY_STRING);
			$('#customerCode_desc').html(EMPTY_STRING);
			$('#customerCode').val(EMPTY_STRING);
			$('#branchCode').removeAttr("disabled"); 
			$('#branchCode_pic').removeAttr("disabled"); 
		}
	}
	return true;
}
function combinationCode_val() {
	var roletype = $('#roleType').val();
	var customercode = $('#customerCode').val();
	var branchcode = $('#branchCode').val();
	clearError('customerCode_error');
	clearError('branchCode_error');
	if (roletype != EMPTY_STRING) {
		if (roletype == 5 || roletype == 6) {
			if (customercode == EMPTY_STRING) {
				setError('customerCode_error', MANDATORY);
				return false;
			}
		} else if (branchcode == EMPTY_STRING) {
			setError('branchCode_error', MANDATORY);
			return false;
		}
	}
	return true;
}
function checkclick(id) {
	switch (id) {
	case 'externalLink':
		if ($('#externalLink').is(':checked')) {
			$('#urlTitle').removeAttr("disabled"); 
			$('#url').removeAttr("disabled"); 
			$('#urlTitle_error').html(EMPTY_STRING);
			$('#url_error').html(EMPTY_STRING);
		} else {
			$('#url').val(EMPTY_STRING);
			$('#urlTitle').val(EMPTY_STRING);
			$('#urlTitle').attr("disabled", "disabled"); 
			$('#url').attr("disabled", "disabled"); 
			$('#urlTitle_error').html(EMPTY_STRING);
			$('#url_error').html(EMPTY_STRING);
		}
		break;
	}
}

function enableFields() {
	var value = $('#visibility').val();
	if (value == COMMON_VISIBILITY_A) {
		$('#userID').removeAttr("disabled"); 
		$('#userID_pic').removeAttr("disabled"); 
		$('#roleType').removeAttr("disabled"); 
	} else {
		$('#userID').attr("disabled", "disabled"); 
		$('#userID_pic').attr("disabled", "disabled"); 
		$('#roleType').attr("disabled", "disabled"); 
		$('#customerCode').attr("disabled", "disabled"); 
		$('#customerCode_pic').attr("disabled", "disabled"); 
		$('#branchCode').attr("disabled", "disabled"); 
		$('#branchCode_pic').attr("disabled", "disabled"); 
		$('#userID').val(EMPTY_STRING);
		$('#userID_error').html(EMPTY_STRING);
		$('#userID_desc').html(EMPTY_STRING);
		$('#roleType').val(EMPTY_STRING);
		$('#roleType_error').html(EMPTY_STRING);
		$('#customerCode').val(EMPTY_STRING);
		$('#customerCode_error').html(EMPTY_STRING);
		$('#customerCode_desc').html(EMPTY_STRING);
		$('#branchCode').val(EMPTY_STRING);
		$('#branchCode_error').html(EMPTY_STRING);
		$('#branchCode_desc').html(EMPTY_STRING);
	}
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (isEmpty(value)) {
		setError('remarks_error', MANDATORY);
		return false;
	}
	if (!isValidRemarks(value)) {
		setError('remarks_error', INVALID_REMARKS);
		return false;
	}
	return true;
}

function revalidate() {
	if (!startDate_val()) {
		errors++;
	}
	if (!expiryDate_val()) {
		errors++;
	}
	if (!displayTitle_val()) {
		errors++;
	}
	if (!remarks_val()) {
		errors++;
	}
	if ($('#externalLink').is(':checked')) {
		if (!url_val()) {
			errors++;
		}
		if (!urlTitle_val()) {
			errors++;
		}
	}
	if (!visibility_val()) {
		errors++;
	}
	var value = $('#visibility').val();
	if (value == COMMON_VISIBILITY_A) {
		if (!($('#userID').val() == EMPTY_STRING && $('#roleType').val() == EMPTY_STRING)) {
			if (!userID_val()) {
				errors++;
			}
			if (!roleType_val()) {
				errors++;
			}
			if (!combinationCode_val()) {
				errors++;
			}
		} else {
			setError('userID_error', MANDATORY);
			setError('roleType_error', MANDATORY);
			errors++;
		}
	}
}