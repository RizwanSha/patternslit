var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'EPKICERTCRL';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}
function clearFields() {
	$('#enabled').prop('checked',false);
	$('#remarks').val(EMPTY_STRING);
}
function loadData() {
	$('#changeDateTime').val(getCBD());
	$('#remarks').val(validator.getValue('REMARKS'));
	$('#rootCaCode').val(validator.getValue('ROOT_CA_CODE'));
	$('#rootCaCode_desc').html(validator.getValue('PKICERTAUTH_DESCRIPTION'));
	$('#fileInventoryNumber').val(validator.getValue('INV_NUM'));
	$('#enabled').prop('checked',decodeSTB(validator.getValue('ENABLED')));
	if ($('#enabled').is(':checked')) {
		$('#cmdPreview').removeAttr("disabled"); 
	} else {
		$('#cmdPreview').attr("disabled", "disabled"); 
	}
	loadAuditFields(validator);
	$('#fileExtensionList').val(CRL_FILE_EXTENSION);
	loadFileName();
	resetLoading();
}
function loadFileName() {
	var value = $('#fileInventoryNumber').val();
	validator.reset();
	validator.setClass('patterns.config.web.forms.operations.ecustusertfaregbean');
	validator.setValue('CERT_INV_NUM', value);
	validator.setMethod('fetchFileName');
	validator.sendAndReceiveAsync(loadAdditionalData);
}

function loadAdditionalData() {
	if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		$('#fileName').val(validator.getValue('CERT_FILE_NAME'));
	}
}
function doFilePreview() {
	var primaryKey = getEntityCode() + '|' + $('#fileInventoryNumber').val();
	showWindow('PREVIEW_VIEW', getBasePath() + 'Renderer/common/qcertificateinfo.jsp', CERTIFICATE_VIEW_TITLE, PK + '=' + primaryKey, true, false, false, 900, 400);
	resetLoading();
}
