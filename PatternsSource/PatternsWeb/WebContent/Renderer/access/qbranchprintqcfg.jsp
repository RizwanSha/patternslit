<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<web:bundle baseName="patterns.config.web.forms.access.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/access/qbranchprintqcfg.js" />
	</web:dependencies>
	<web:loadPK />
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="qbranchprintqcfg.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:viewContent id="po_view1">
				<web:dividerBlock>
					<web:sectionBlock width="width-100p" align="left">
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="180px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
										<web:column>
											<web:legend key="ibranchprintqcfg.branchcode" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:entityBranchListDisplay property="branchCode" id="branchCode" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="form.effectivedate" var="common" mandatory="true" />
										</web:column>
										<web:column>
											<type:dateDisplay property="effectiveDate" id="effectiveDate" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:viewTitle var="program" key="ibranchprintqcfg.section" />
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="ibranchprintqcfg.printtype" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:printTypeDisplay property="printType" id="printType" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="ibranchprintqcfg.printqueue" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:printQueueDisplay property="printQueue" id="printQueue" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:rowOdd>
										<web:column span="2">
											<web:viewContent id="po_view4">
												<web:grid height="240px" width="740px" id="branchPrintqGrid" src="access/qbranchprintqcfg_branchPrintqGrid.xml" paging="false">
												</web:grid>
											</web:viewContent>
										</web:column>
									</web:rowOdd>
							</web:table>
						</web:section>
						<web:auditDisplay />
					</web:sectionBlock>
				</web:dividerBlock>
			</web:viewContent>
		</web:viewSubPart>
	</web:viewPart>
</web:fragment>