var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'ECURRLOGGEDUSERS';
function init() {
}

function loadLoginUsersGrid() {
	clearFields();
	validator.clearMap();
	validator.setMtm(false);
	validator.setClass('patterns.config.web.forms.access.ecurrloggedusersbean');
	validator.setMethod('loadGridValues');
	validator.sendAndReceive();
	if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		loggedusersGrid.loadXMLString(validator.getValue(RESULT_XML));
		$('#currentLoggedUserCount').val(loggedusersGrid.getRowsNum());
	} else {
		setError('userIDError_error', validator.getValue(ERROR));
	}
}

function validateSelectedUsers() {
	var ros = EMPTY_STRING;
	ros = loggedusersGrid.getCheckedRows(0);
	var rosaerr = ros.split(',');
	if (loggedusersGrid.getRowsNum() < 1) {
		alert(NO_ROWS_AVAILABLE);
		return false;
	} else if (ros == EMPTY_STRING) {
		alert(SELECT_ATLEAST_ONE);
		return false;
	}
	return true;
}

function clearFields() {
	$('#currentLoggedUserCount').val(EMPTY_STRING);
	loggedusersGrid.clearAll();
}

function doLogoutProcess() {
	var xmlCurrLoggedUsers = loggedusersGrid.serialize();
	validator.clearMap();
	validator.setMtm(false);
	validator.setClass('patterns.config.web.forms.access.ecurrloggedusersbean');
	validator.setMethod('processLogout');
	validator.setValue('XML_DATA', xmlCurrLoggedUsers);
	validator.sendAndReceive();
	if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		alert(REQUEST_PROCESSED);
		loadLoginUsersGrid();
		return true;
	} else {
		alert(REQUEST_NOT_PROCESSED + ' :: ' + validator.getValue(ERROR));
		return false;
	}
}

function revalidate() {
	if (!validateSelectedUsers()) {
		errors++;
	}
	if (errors == 0) {
		doLogoutProcess();
	}
}