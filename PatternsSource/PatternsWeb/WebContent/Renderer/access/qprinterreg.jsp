<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.access.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/access/qprinterreg.js" />
	</web:dependencies>
	<web:loadPK />
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="qprinterreg.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:viewContent id="po_view1">
				<web:dividerBlock>
					<web:sectionBlock width="width-100p" align="left">
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="180px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
										<web:column>
											<web:legend key="mprinterreg.printerId" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:printerIdDisplay property="printerId" id="printerId" />
										</web:column>
									</web:rowEven>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="180px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowOdd>
										<web:column>
											<web:legend key="mprinterreg.printerDescription" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:descriptionDisplay property="printerDescription" id="printerDescription" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="mprinterreg.printerName" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:descriptionDisplay property="printerName" id="printerName" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="mprinterreg.printAddress" var="program" />
										</web:column>
										<web:column>
											<type:descriptionDisplay property="printAddress" id="printAddress" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="mprinterreg.printerType" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:descriptionDisplay property="printerType" id="printerType" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="mprinterreg.branchCode" var="program" />
										</web:column>
										<web:column>
											<type:entityBranchListDisplay property="branchCode" id="branchCode" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="mprinterreg.branchListCode" var="program" />
										</web:column>
										<web:column>
											<type:entityBranchListDisplay property="branchListCode" id="branchListCode" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="mprinterreg.status" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:comboDisplay property="status" id="status" datasourceid="MPRINTERREG_STATUS" />
										</web:column>
									</web:rowOdd>
									<web:rowOdd>
										<web:column>
											<web:legend key="form.enabled" var="common" />
										</web:column>
										<web:column>
											<type:checkboxDisplay property="enabled" id="enabled" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="form.remarks" var="common" />
										</web:column>
										<web:column>
											<type:remarksDisplay property="remarks" id="remarks" />
										</web:column>
									</web:rowEven>
							</web:table>
						</web:section>
						<web:section>
							<web:viewTitle var="program" key="mprinterreg.section" />
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="190px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowOdd>
									<web:column span="2">
										<web:viewContent id="po_view4">
											<web:grid height="240px" width="250px" id="printer_innerGrid" src="access/qprinterreg_printer_innerGrid.xml">
											</web:grid>
										</web:viewContent>
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
						<web:auditDisplay />
					</web:sectionBlock>
				</web:dividerBlock>
			</web:viewContent>
		</web:viewSubPart>
	</web:viewPart>
</web:fragment>