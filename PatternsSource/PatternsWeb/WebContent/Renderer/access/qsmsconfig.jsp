<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.access.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/access/qsmsconfig.js" />
	</web:dependencies>
	<web:loadPK />
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="qsmsconfig.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:viewContent id="po_view1">
				<web:dividerBlock>
					<web:sectionBlock width="width-100p" align="left">
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="180px" />
									<web:columnStyle />
								</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="ismsconfig.smscode" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:codeDisplay property="smsCode" id="smsCode" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="form.effectivedate" var="common" mandatory="true" />
										</web:column>
										<web:column>
											<type:dateDisplay property="effectiveDate" id="effectiveDate" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle width="210px" />
										<web:columnStyle width="210px" />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="ismsconfig.httpallowed" var="program" />
										</web:column>
										<web:column>
											<type:checkboxDisplay property="httpAllowed" id="httpAllowed" />
										</web:column>
										<web:column>
											<web:legend key="ismsconfig.httpsslrequired" var="program" />
										</web:column>
										<web:column>
											<type:checkboxDisplay property="sslRequired" id="sslRequired" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:viewTitle var="program" key="ismsconfig.section2" />
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
									</web:columnGroup>
									<web:rowOdd>
									<web:column>
											<web:legend key="ismsconfig.serverurl" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:serverAddressDisplay property="serverUrl" id="serverUrl" />
										</web:column>								
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:viewTitle var="program" key="ismsconfig.section3" />
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="ismsconfig.proxyrequired" var="program" />
										</web:column>
										<web:column>
											<type:checkboxDisplay property="proxyRequired" id="proxyRequired" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="ismsconfig.proxyserver" var="program" />
										</web:column>
										<web:column>
											<type:serverAddressDisplay property="proxyServer" id="proxyServer" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="ismsconfig.proxyserverport" var="program" />
										</web:column>
										<web:column>
											<type:portDisplay property="proxyServerPort" id="proxyServerPort" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="ismsconfig.proxyserverauthentication" var="program" />
										</web:column>
										<web:column>
											<type:checkboxDisplay property="proxyServerAuthentication" id="proxyServerAuthentication" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="ismsconfig.proxyaccountname" var="program" />
										</web:column>
										<web:column>
											<type:nameDisplay property="proxyAccountName" id="proxyAccountName" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="ismsconfig.proxyaccountpassword" var="program" />
										</web:column>
										<web:column>
											<type:passwordDisplay property="proxyAccountPassword" id="proxyAccountPassword" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:viewTitle var="program" key="ismsconfig.section4" />
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="ismsconfig.authenticationreqd" var="program" />
										</web:column>
										<web:column>
											<type:checkboxDisplay property="authenticationRequired" id="authenticationRequired" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
									</web:columnGroup>
									<web:rowOdd>
											<web:column>
											<web:legend key="ismsconfig.usernamekey" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:externalDescriptionDisplay property="userNameKey" id="userNameKey" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="ismsconfig.accountname" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:externalDescriptionDisplay property="accountName" id="accountName" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="ismsconfig.accountpasswordkey" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:externalDescriptionDisplay property="accountPasswordKey" id="accountPasswordKey" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="ismsconfig.accountPassword" var="program" />
										</web:column>
										<web:column>
											<type:passwordDisplay property="accountPassword" id="accountPassword" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="ismsconfig.mobilenumberkey" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:externalDescriptionDisplay property="mobileNumberKey" id="mobileNumberKey"  />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="ismsconfig.messagekey" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:externalDescriptionDisplay property="messageKey" id="messageKey"  />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:viewTitle var="program" key="ismsconfig.section5" />
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="ismsconfig.smppallowed" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:checkboxDisplay property="smppAllowed" id="smppAllowed" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="ismsconfig.sourcenumber" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:receiptNumberDisplay property="senderNumber" id="senderNumber"  />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="ismsconfig.systemtype" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:descriptionDisplay property="systemType" id="systemType"  />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="ismsconfig.addressrange" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:dynamicDescriptionDisplay property="addressRange" id="addressRange" length="100" size="100" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
									</web:columnGroup>
									<web:rowOdd>
											<web:column>
											<web:legend key="ismsconfig.transceiverenabled" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:checkboxDisplay property="transceiverEnabled" id="transceiverEnabled" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="ismsconfig.mainserver" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:serverAddressDisplay property="mainServer" id="mainServer" />
										</web:column>
										</web:rowEven>
										<web:rowOdd>
										<web:column>
											<web:legend key="ismsconfig.mainserverport" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:portDisplay property="mainServerPort" id="mainServerPort" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="ismsconfig.failoverserver" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:serverAddressDisplay property="failOverServer" id="failOverServer" />
										</web:column>
										</web:rowEven>
										<web:rowOdd>
										<web:column>
											<web:legend key="ismsconfig.failoverserverport" var="program" />
										</web:column>
										<web:column>
											<type:portDisplay property="failOverServerPort" id="failOverServerPort" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
									</web:columnGroup>
									<web:rowEven>
											<web:column>
											<web:legend key="ismsconfig.dispatchtype" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:comboDisplay property="dispatchType" id="dispatchType" datasourceid="COMMON_SMSTYPE" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
									<web:column>
											<web:legend key="ismsconfig.retrytimeout" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:fourDnumberDisplay property="retryTimeout" id="retryTimeout"  />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="form.remarks" var="common" mandatory="true" />
										</web:column>
										<web:column>
											<type:remarksDisplay property="remarks" id="remarks" />
										</web:column>
									</web:rowEven>
							</web:table>
						</web:section>
						<web:auditDisplay />
					</web:sectionBlock>
				</web:dividerBlock>
			</web:viewContent>
		</web:viewSubPart>
	</web:viewPart>
</web:fragment>