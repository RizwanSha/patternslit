<%--
	JSP         	 	    :       quseractrolealloc	
	Purpose            		:       Query on Acting Role allocation to a Internal User
    Module              	:       access
    Created By           	:       Shankara Subramanian (66979)
    Created On         		:       25-05-2011
   	Modification History 	:	
    Modified By | Modified On | Given By | Remarks	
    		    |			  |			 |		
--%>
<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.access.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/access/quseractrolealloc.js" />
	</web:dependencies>
	<web:loadPK />
	<web:viewPart>
		<web:programTitle var="program" key="quseractrolealloc.programtitle" />
		<web:viewContent id="po_view1">
			<web:dividerBlock>
				<web:sectionBlock width="width-100p" align="left">
					<web:section>
						<web:table>
							<web:columnGroup>
								<web:columnStyle width="180px" />
								<web:columnStyle />
							</web:columnGroup>
							<web:rowOdd>
								<web:column>
									<web:legend key="form.userid" var="common" />
								</web:column>
								<web:column>
									<type:userDisplay property="userID" id="userID" />
								</web:column>
							</web:rowOdd>
							<web:rowEven>
								<web:column>
									<web:legend key="form.rolecode" var="common" />
								</web:column>
								<web:column>
									<type:codeDisplay property="roleCode" id="roleCode" />
								</web:column>
							</web:rowEven>
							<web:rowOdd>
								<web:column>
									<web:legend key="form.enabled" var="common" />
								</web:column>
								<web:column>
									<type:checkboxDisplay property="enabled" id="enabled" />
								</web:column>
							</web:rowOdd>
							<web:rowEven>
								<web:column>
									<web:legend key="form.fromdate" var="common" />
								</web:column>
								<web:column>
									<type:dateDisplay property="fromDate" id="fromDate" />
								</web:column>
							</web:rowEven>
							<web:rowOdd>
								<web:column>
									<web:legend key="form.uptodate" var="common" />
								</web:column>
								<web:column>
									<type:dateDisplay property="toDate" id="toDate" />
								</web:column>
							</web:rowOdd>
						</web:table>
					</web:section>
					<web:auditDisplay />
				</web:sectionBlock>
			</web:dividerBlock>
		</web:viewContent>
	</web:viewPart>
</web:fragment>
