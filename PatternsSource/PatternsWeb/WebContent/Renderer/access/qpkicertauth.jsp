<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.access.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/access/qpkicertauth.js" />
	</web:dependencies>
	<web:loadPK />
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="qpkicertauth.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:viewContent id="po_view1">
				<web:dividerBlock>
					<web:sectionBlock width="width-100p" align="left">
						<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="250px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="mpkicertauth.code" var="program"  />
										</web:column>
										<web:column>
											<type:inputCodeDisplay property="code" id="code" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="250px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="mpkicertauth.description" var="program"  />
										</web:column>
										<web:column>
											<type:descriptionDisplay property="description" id="description" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="mpkicertauth.hierlevel" var="program" />
										</web:column>
										<web:column>
											<type:comboDisplay property="heirarchyLevel" id="heirarchyLevel" datasourceid="COMMON_HEIRLEVEL"/>
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="mpkicertauth.parentcode" var="program" />
										</web:column>
										<web:column>
											<type:codeDisplay property="parentCode" id="parentCode" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="mpkicertauth.cahttpurl" var="program"  />
										</web:column>
										<web:column>
											<type:urlDisplay property="certAuthHttpUrl" id="certAuthHttpUrl" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="mpkicertauth.caldapurl" var="program"  />
										</web:column>
										<web:column>
											<type:urlDisplay property="certAuthLdapUrl" id="certAuthLdapUrl" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="mpkicertauth.ocspverify" var="program" />
										</web:column>
										<web:column>
											<type:checkboxDisplay property="ocspAvail" id="ocspAvail" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="mpkicertauth.ocsphttpurl" var="program" />
										</web:column>
										<web:column>
											<type:urlDisplay property="ocspHttpUrl" id="ocspHttpUrl" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="mpkicertauth.crlhttpurl" var="program"  />
										</web:column>
										<web:column>
											<type:urlDisplay property="crlHttpUrl" id="crlHttpUrl" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="mpkicertauth.httpreftime" var="program"  />
										</web:column>
										<web:column>
											<type:refTimeDisplay property="httpRefreshTime" id="httpRefreshTime" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="mpkicertauth.crlldapurl" var="program"  />
										</web:column>
										<web:column>
											<type:urlDisplay property="crlLdapUrl" id="crlLdapUrl" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="mpkicertauth.ldapreftime" var="program"  />
										</web:column>
										<web:column>
											<type:refTimeDisplay property="ldapRefreshTime" id="ldapRefreshTime" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="form.enabled" var="common" />
										</web:column>
										<web:column>
											<type:checkboxDisplay property="enabled" id="enabled" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="form.remarks" var="common"  />
										</web:column>
										<web:column>
											<type:remarksDisplay property="remarks" id="remarks" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
						<web:auditDisplay />
					</web:sectionBlock>
				</web:dividerBlock>
			</web:viewContent>
		</web:viewSubPart>
	</web:viewPart>
</web:fragment>