var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MPRINTERREG';
var allergy_desc;
var bothDisabled = false;
function init() {
	valMode = true;
	if (_redisplay) {
		if ($('#action').val() == ADD) {
			$('#enabled').prop('disabled', true);
			setCheckbox('enabled', YES);
		}
		printer_innerGrid.clearAll();
		if (!isEmpty($('#xmlPrinterRegGrid').val())) {
			printer_innerGrid.loadXMLString($('#xmlPrinterRegGrid').val());
		}
		entitiesCode_val();
		if (!bothDisabled) {
			branchCode_val(valMode);
			branchListCode_val(valMode);
		}
	}
	refreshTBAGrid();
	refreshQueryGrid();
}

function loadGrid() {
	loadGridQuery(CURRENT_PROGRAM_ID, 'MPRINTERREG_PRINTER_GRID',
			printer_innerGrid);
}

function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'MPRINTERREG_MAIN');
}

function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}

function add() {
	$('#po_view1').removeClass('hidden');
	$('#printerId').focus();
	$('#status').selectedIndex = 0;
	setCheckbox('enabled', YES);
	$('#enabled').prop('disabled', true);
	entitiesCode_val();
}

function modify() {
	$('#po_view1').removeClass('hidden');
	$('#printerId').focus();
	if ($('#action').val() == MODIFY) {
		$('#enabled').prop('disabled', false);
	} else {
		$('#enabled').prop('disabled', true);
	}
	entitiesCode_val();
}

function view(source, primaryKey) {
	hideParent('access/qprinterreg', source, primaryKey);
	resetLoading();
}

function clearFields() {
	$('#printerId').val(EMPTY_STRING);
	$('#printerId_error').html(EMPTY_STRING);
	$('#printerId_desc').html(EMPTY_STRING);
	clearNonPKFields();
}

function clearNonPKFields() {
	$('#printerDescription').val(EMPTY_STRING);
	$('#printerDescription_error').html(EMPTY_STRING);
	$('#printerName').val(EMPTY_STRING);
	$('#printerName_error').html(EMPTY_STRING);
	$('#printAddress').val(EMPTY_STRING);
	$('#printAddress_error').html(EMPTY_STRING);
	$('#printerType').val(EMPTY_STRING);
	$('#printerType_error').html(EMPTY_STRING);
	$('#branchCode').val(EMPTY_STRING);
	$('#branchCode_error').html(EMPTY_STRING);
	$('#branchCode_desc').html(EMPTY_STRING);
	$('#branchListCode').val(EMPTY_STRING);
	$('#branchListCode_error').html(EMPTY_STRING);
	$('#branchListCode_desc').html(EMPTY_STRING);
	$('#status').selectedIndex = 0;
	$('#status_error').html(EMPTY_STRING);
	$('#status_desc').html(EMPTY_STRING);
	setCheckbox('enabled', YES);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
	$('#printTypeId').val(EMPTY_STRING);
	$('#printTypeId_desc').html(EMPTY_STRING);
	$('#printTypeId_error').html(EMPTY_STRING);
	printer_clearGridFields();
	printer_innerGrid.clearAll();

}
function doclearfields(id) {
	switch (id) {
	case 'printerId':
		if (isEmpty($('#printerId').val())) {
			$('#printerId_desc').html(EMPTY_STRING);
			$('#printerId_error').html(EMPTY_STRING);
			clearNonPKFields();
			break;
		}
	}
}
function loadData() {
	$('#printerId').val(validator.getValue('PRINTER_ID'));
	$('#printerDescription').val(validator.getValue('PRINTER_DESC'));
	$('#printerName').val(validator.getValue('PRINTER_NAME'));
	$('#printAddress').val(validator.getValue('PRINTER_ADDRESS'));
	$('#printerType').val(validator.getValue('PRINTER_TYPE'));
	$('#branchCode').val(validator.getValue('BRN_CODE'));
	$('#branchCode_desc').html(validator.getValue('F1_DESCRIPTION'));
	$('#branchListCode').val(validator.getValue('BRNLIST_CODE'));
	$('#branchListCode_desc').html(validator.getValue('F2_DESCRIPTION'));
	$('#status').val(validator.getValue('STATUS'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadGrid();
	resetLoading();
}

function doHelp(id) {
	switch (id) {
	case 'printerId':
		help('COMMON', 'HLP_PRNTYPE_ID', $('#printerId').val(), EMPTY_STRING,
				$('#printerId'));
		break;
	case 'branchCode':
		help('COMMON', 'HLP_ENTITYBRN_M', $('#branchCode').val(), EMPTY_STRING,
				$('#branchCode'));
		break;
	case 'branchListCode':
		help('COMMON', 'HLP_ENTITYBRNLIST_M', $('#branchListCode').val(),
				EMPTY_STRING, $('#branchListCode'));
		break;
	case 'printTypeId':
		help('COMMON', 'HLP_PRNTYPE_M', $('#printTypeId').val(), EMPTY_STRING,
				$('#printTypeId'));
		break;
	}
}

function validate(id) {
	switch (id) {
	case 'printerId':
		printerId_val(valMode);
		break;
	case 'printerDescription':
		printerDescription_val(valMode);
		break;
	case 'printerName':
		printerName_val(valMode);
		break;
	case 'printAddress':
		printAddress_val(valMode);
		break;
	case 'printerType':
		printerType_val(valMode);
		break;
	case 'branchCode':
		branchCode_val(valMode);
		break;
	case 'branchListCode':
		branchListCode_val(valMode);
		break;
	case 'status':
		status_val(valMode);
		break;
	case 'enabled':
		enabled_val(valMode);
		break;
	case 'remarks':
		remarks_val(valMode);
		break;
	case 'printTypeId':
		printTypeId_val(valMode);
		break;
	}
}

function backtrack(id) {
	switch (id) {
	case 'printerId':
		setFocusLast('printerId');
		break;
	case 'printerDescription':
		setFocusLast('printerId');
		break;
	case 'printerName':
		setFocusLast('printerDescription');
		break;
	case 'printAddress':
		setFocusLast('printerName');
		break;
	case 'printerType':
		setFocusLast('printAddress');
		break;
	case 'branchCode':
		setFocusLast('printerType');
		break;
	case 'branchListCode':
		if ($('#branchCode').is('[readonly]')) {
			setFocusLast('printerType');
		} else {
			setFocusLast('branchCode');
		}
		break;
	case 'status':
		if ($('#branchListCode').is('[readonly]')) {
			if ($('#branchCode').is('[readonly]')) {
				setFocusLast('printerType');
			} else {
				setFocusLast('branchCode');
			}
		} else {
			setFocusLast('branchListCode');
		}
		break;
	case 'enabled':
		setFocusLast('status');
		break;
	case 'remarks':
		if ($('#action').val() == MODIFY) {
			setFocusLast('enabled');
		} else {
			setFocusLast('status');
		}
		break;
	case 'printTypeId':
		setFocusLast('remarks');
		break;
	}
}

function printerId_val(valMode) {
	var value = $('#printerId').val();
	clearError('printerId_error');
	if (isEmpty(value)) {
		setError('printerId_error', HMS_MANDATORY);
		return false;
	} else if ($('#action').val() == ADD && !isValidCode(value)) {
		setError('printerId_error', HMS_INVALID_FORMAT);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('PRINTER_ID', $('#printerId').val());
		validator.setValue('ACTION', $('#action').val());
		validator.setClass('patterns.config.web.forms.access.mprinterregbean');
		validator.setMethod('validatePrinterId');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('printerId_error', validator.getValue(ERROR));
			return false;
		} else {
			if (validator.getValue(RESULT) == DATA_AVAILABLE) {
				$('#printerId_desc').html(validator.getValue('PRINTER_DESC'));
			}
		}
		if ($('#action').val() == MODIFY) {
			clearNonPKFields();
			PK_VALUE = getEntityCode() + '|' + $('#printerId').val();
			loadPKValues(PK_VALUE, 'printerId_error');
			loadGrid();
			setFocusLast('printerId');
			return true;
		}

	}
	setFocusLast('printerDescription');
	return true;
}

function printerDescription_val(valMode) {
	var value = $('#printerDescription').val();
	clearError('printerDescription_error');
	if (isEmpty(value)) {
		setError('printerDescription_error', HMS_MANDATORY);
		setFocusLast('printerDescription');
		return false;
	}
	if (!isValidDescription(value)) {
		setError('printerDescription_error', HMS_INVALID_DESCRIPTION);
		setFocusLast('printerDescription');
		return false;
	}
	setFocusLast('printerName');
	return true;
}

function printerName_val(valMode) {
	var value = $('#printerName').val();
	clearError('printerName_error');
	if (isEmpty(value)) {
		setError('printerName_error', HMS_MANDATORY);
		setFocusLast('printerName');
		return false;
	}
	if (!isValidDescription(value)) {
		setError('printerName_error', HMS_INVALID_DESCRIPTION);
		setFocusLast('printerName');
		return false;
	}
	setFocusLast('printAddress');
	return true;
}

function printAddress_val(valMode) {
	var value = $('#printAddress').val();
	clearError('printAddress_error');
	if (!isEmpty(value)) {
		if (!isValidDescription(value)) {
			setError('printAddress_error', HMS_INVALID_DESCRIPTION);
			setFocusLast('printAddress');
			return false;
		}
	}
	setFocusLast('printerType');
	return true;
}

function printerType_val(valMode) {
	var value = $('#printerType').val();
	clearError('printerType_error');
	if (isEmpty(value)) {
		setError('printerType_error', HMS_MANDATORY);
		setFocusLast('printerType');
		return false;
	}
	if (!isValidDescription(value)) {
		setError('printerType_error', HMS_INVALID_DESCRIPTION);
		setFocusLast('printerType');
		return false;
	}
	if ($('#branchCode').is('[readonly]')) {
		if ($('#branchListCode').is('[readonly]')) {
			setFocusLast('status');
			return true;
		} else {
			setFocusLast('branchListCode');
			return true;
		}
	} else {
		setFocusLast('branchCode');
		return true;
	}
	return true;
}

function branchCode_val() {
	var value = $('#branchCode').val();
	clearError('branchCode_error');
	clearError('branchListCode_error');
	if (!isEmpty(value)) {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('BRANCH_CODE', $('#branchCode').val());
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.access.mprinterregbean');
		validator.setMethod('validateBranchCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('branchCode_error', validator.getValue(ERROR));
			return false;
		} else if (validator.getValue(RESULT) == DATA_AVAILABLE) {
			$('#branchCode_desc').html(validator.getValue('DESCRIPTION'));
		}
		$('#branchListCode').prop("readonly", true);
		$('#branchListCode_pic').prop("disabled", true);
		setFocusLast('status');

	} else {
		$('#branchListCode').prop("readonly", false);
		$('#branchListCode_pic').prop("disabled", false);
		setFocusLast('branchListCode');

	}

	return true;

}

function branchListCode_val() {
	var value = $('#branchCode').val();
	clearError('branchListCode_error');
	if (isEmpty(value)) {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('BRNLIST_CODE', $('#branchListCode').val());
		validator.setValue('BRANCH_CODE', $('#branchCode').val());
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.access.mprinterregbean');
		validator.setMethod('validateBranchListCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('branchListCode_error', validator.getValue(ERROR));
			$('#branchCode').prop("readonly", false);
			$('#branchCode_pic').prop("disabled", false);
			return false;
		} else if (validator.getValue(RESULT) == DATA_AVAILABLE) {
			$('#branchListCode_desc').html(validator.getValue('DESCRIPTION'));

		}
		$('#branchCode').prop("readonly", true);
		$('#branchCode_pic').prop("disabled", true);

	}
	setFocusLast('status');

	return true;
}

function status_val(valMode) {
	var value = $('#status').val();
	clearError('status_error');
	if (isEmpty(value)) {
		setError('status_error', HMS_MANDATORY);
		return false;
	}
	if ($('#action').val() == ADD) {
		setFocusLast('remarks');
	} else {
		setFocusLast('enabled');
	}
	return true;
}

function printTypeId_val(valMode) {
	var value = $('#printTypeId').val();
	clearError('printTypeId_error');
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('PRNTYPE_ID', value);
	validator.setValue(ACTION, USAGE);
	validator.setClass('patterns.config.web.forms.access.mprinterregbean');
	validator.setMethod('validateprintTypeId');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('printTypeId_error', validator.getValue(ERROR));
		return false;
	} else {
		$('#printTypeId_desc').html(validator.getValue('INT_DESCRIPTION'));

	}
	if (valMode) {
		setFocusLastOnGridSubmit('printer_innerGrid');
	}
	return true;
}

function entitiesCode_val() {
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue(ACTION, USAGE);
	validator.setClass('patterns.config.web.forms.access.mprinterregbean');
	validator.setMethod('validateEntitiesCode');
	validator.sendAndReceive();
	if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		var branchReg = validator.getValue('BRANCH_REQD');
		if (branchReg == COLUMN_DISABLE) {
			$('#branchCode').val(EMPTY_STRING);
			$('#branchCode').prop("readonly", true);
			$('#branchCode_pic').prop("disabled", true);
			$('#branchListCode').val(EMPTY_STRING);
			$('#branchListCode').prop("readonly", true);
			$('#branchListCode_pic').prop("disabled", true);
			bothDisabled = true;
			return true;
		} else if (branchReg == COLUMN_ENABLE) {
			if (!_redisplay) {
				$('#branchCode').val(EMPTY_STRING);
				$('#branchListCode').val(EMPTY_STRING);
			}
			$('#branchCode').prop("readonly", false);
			$('#branchCode_pic').prop("disabled", false);
			$('#branchListCode').prop("readonly", false);
			$('#branchListCode_pic').prop("disabled", false);
		}
	} else {
		setError('branchCode_error', validator.getValue(ERROR));
		return false;
	}
	return true;
}

function remarks_val(valMode) {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', HMS_INVALID_REMARKS);
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', HMS_INVALID_REMARKS);
			return false;
		}
	}
	if ($('#action').val() == MODIFY) {
		if (isEmpty(value)) {
			setError('remarks_error', HMS_MANDATORY);
			return false;
		}
	}
	setFocusLast('printTypeId');
	return true;
}

function printer_cancelGrid(rowId) {
	printer_clearGridFields();
}
function printer_clearGridFields() {
	$('#printTypeId').val(EMPTY_STRING);
	$('#printTypeIdr_desc').html(EMPTY_STRING);
	clearError('printTypeId_error');
}

function printer_gridDuplicateCheck(editMode, editRowId) {
	var currentValue = [ $('#printTypeId').val() ];
	return _grid_duplicate_check(printer_innerGrid, currentValue, [ 1 ]);
}

function printer_addGrid(editMode, editRowId) {
	if (printTypeId_val(false)) {
		if (printer_gridDuplicateCheck(editMode, editRowId)) {
			var field_values = [ 'false', $('#printTypeId').val() ];
			_grid_updateRow(printer_innerGrid, field_values);
			printer_clearGridFields();
			$('#printTypeId').focus();
			return true;
		} else {
			setError('printTypeId_error', HMS_DUPLICATE_DATA);
			return false;
		}
	} else {
		return false;
	}
}

function printer_editGrid(rowId) {
	$('#printTypeId').val(printer_innerGrid.cells(rowId, 1).getValue());
	$('#printTypeId').focus();

}

function gridExit(id) {
	switch (id) {
	case 'printTypeId':
		$('#xmlPrinterRegGrid').val(printer_innerGrid.serialize());
		setFocusOnSubmit();
		break;
	}
}

function gridDeleteRow(id) {
	switch (id) {
	case 'printTypeId':
		deleteGridRecord(printer_innerGrid);
		break;
	}
}

function revalidate() {
	printer_revaildateGrid();
}

function printer_revaildateGrid() {
	if (printer_innerGrid.getRowsNum() > 0) {
		$('#xmlPrinterRegGrid').val(printer_innerGrid.serialize());
		$('#printTypeId_error').html(EMPTY_STRING);
	} else {
		$('#xmlPrinterRegGrid').val(printer_innerGrid.serialize());
		setError('printTypeId_error', HMS_ADD_ATLEAST_ONE_ROW);
		errors++;
	}
}
