var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MPRINTTYPE';
function init() {
	refreshTBAGrid();
	refreshQueryGrid();
	if ($('#action').val() == ADD) {
		$('#printTypeId').focus();
		$('#enabled').prop('disabled', true);
		setCheckbox('enabled', YES);		
	}
}

function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'MPRINTTYPE_MAIN', queryGrid);
}

function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}

function doclearfields(id) {
	switch (id) {
	case 'printTypeId':
		if (isEmpty($('#printTypeId').val())) {
			$('#printTypeId_error').html(EMPTY_STRING);
			$('#printTypeId_desc').html(EMPTY_STRING);
			clearNonPKFields();
			break;
		}
	}
}

function clearFields() {
	$('#printTypeId').val(EMPTY_STRING);
	$('#printTypeId_error').html(EMPTY_STRING);
	$('#printTypeId_desc').html(EMPTY_STRING);
	clearNonPKFields();
}

function clearNonPKFields() {
	$('#printTypeDescription').val(EMPTY_STRING);
	$('#printTypeDescription_error').html(EMPTY_STRING);
	$('#printingType_error').html(EMPTY_STRING);
	if ($('#action').val() == ADD) {
		$('#enabled').prop('disabled', true);
	} else {
		$('#enabled').prop('disabled', false);
	}
	setCheckbox('enabled', YES);
	$('#enabled_error').html(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
}

function doHelp(id) {
	switch (id) {
	case 'printTypeId':
		help('COMMON', 'HLP_PRNTYPE_ID', $('#printTypeId').val(), EMPTY_STRING,$('#printTypeId'));
		break;
	}
}

function add() {
	$('#printTypeId').focus();
	setCheckbox('enabled', YES);
	$('#enabled').prop('disabled', true);
}

function modify() {
	$('#printTypeId').focus();
	if ($('#action').val() == MODIFY) {
		$('#enabled').prop('disabled', false);
	} else {
		$('#enabled').prop('disabled', true);
	}
}

function loadData() {
	$('#printTypeId').val(validator.getValue('PRNTYPE_ID'));
	$('#printTypeDescription').val(validator.getValue('DESCRIPTION'));
	$('#printingType').val(validator.getValue('PRNT_TYPE'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	resetLoading();
}

function view(source, primaryKey) {
	hideParent('access/qprinttype', source, primaryKey);
	resetLoading();
}

function backtrack(id) {
	switch (id) {
	case 'printTypeId':
		setFocusLast('printTypeId');
		break;
	case 'printTypeDescription':
		setFocusLast('printTypeId');
		break;
	case 'printingType':
		setFocusLast('printTypeDescription');
		break;
	case 'enabled':
		setFocusLast('printingType');
		break;
	case 'remarks':
		if ($('#action').val() == MODIFY) {
			setFocusLast('enabled');
		} else {
			setFocusLast('printingType');
		}
		break;
	}
}

function validate(id) {
	switch (id) {
	case 'printTypeId':
		printTypeId_val();
		break;
	case 'printTypeDescription':
		printTypeDescription_val();
		break;
	case 'printingType':
		printingType_val();
		break;
	case 'enabled':
		enabled_val();
		break;
	case 'remarks':
		remarks_val();
		break;
	}
}

function printTypeId_val() {
	var value = $('#printTypeId').val();
	clearError('printTypeId_error');
	if (isEmpty(value)) {
		setError('printTypeId_error', HMS_MANDATORY);
		return false;
	} else if ($('#action').val() == ADD && !isValidCode(value)) {
		setError('printTypeId_error', HMS_INVALID_FORMAT);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('PRNTYPE_ID', $('#printTypeId').val());
		validator.setValue(ACTION, $('#action').val());
		validator.setClass('patterns.config.web.forms.access.mprinttypebean');
		validator.setMethod('validatePrintType');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('printTypeId_error', validator.getValue(ERROR));
			return false;
		} else {
			if ($('#action').val() == MODIFY) {
				PK_VALUE = getEntityCode() + PK_SEPERATOR
						+ $('#printTypeId').val();
				if (!loadPKValues(PK_VALUE, 'printTypeId_error')) {
					return false;
				}
			}
		}
	}
	setFocusLast('printTypeDescription');
	return true;
}

function printTypeDescription_val() {
	var value = $('#printTypeDescription').val();
	clearError('printTypeDescription_error');
	if (isEmpty(value)) {
		setError('printTypeDescription_error', HMS_MANDATORY);
		return false;
	}
	if (!isValidDescription(value)) {
		setError('printTypeDescription_error', HMS_INVALID_DESCRIPTION);
		return false;
	}
	setFocusLast('printingType');
	return true;
}

function printingType_val() {
	var value = $('#printingType').val();
	clearError('printingType_error');
	if (isEmpty(value)) {
		setError('printingType_error', HMS_MANDATORY);
		setFocusLast('printingType');
		return false;
	}
	if ($('#action').val() == ADD) {
		setFocusLast('remarks');
	} else {
		setFocusLast('enabled');
	}
	return true;
}

function enabled_val() {
	if ($('#action').val() == MODIFY) {
		setFocusLast('remarks');
	}
	return true;
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', HMS_INVALID_REMARKS);
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', HMS_INVALID_REMARKS);
			return false;
		}
	}
	if ($('#action').val() == MODIFY) {
		if (isEmpty(value)) {
			setError('remarks_error', HMS_MANDATORY);
			return false;
		}

	}
	setFocusOnSubmit();
	return true;
}