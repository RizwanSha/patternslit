<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.access.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/access/ismsconfig.js" />
	</web:dependencies>
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="ismsconfig.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/access/ismsconfig" id="ismsconfig" method="POST">
				<web:dividerBlock>
					<type:optionBox add="true" modify="true" view="true" template="true" />
				</web:dividerBlock>
				<web:viewContent id="po_view1" styleClass="hidden">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="ismsconfig.smscode" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:code property="smsCode" id="smsCode" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="form.effectivedate" var="common" mandatory="true" />
										</web:column>
										<web:column>
											<type:date property="effectiveDate" id="effectiveDate" lookup="true" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle width="210px" />
										<web:columnStyle width="210px" />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="ismsconfig.httpallowed" var="program" />
										</web:column>
										<web:column>
											<type:checkbox property="httpAllowed" id="httpAllowed" />
										</web:column>
										<web:column>
											<web:legend key="ismsconfig.httpsslrequired" var="program" />
										</web:column>
										<web:column>
											<type:checkbox property="sslRequired" id="sslRequired" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:viewTitle var="program" key="ismsconfig.section2" />
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
									</web:columnGroup>
									<web:rowOdd>
									<web:column>
											<web:legend key="ismsconfig.serverurl" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:serverAddress property="serverUrl" id="serverUrl" />
										</web:column>								
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:viewTitle var="program" key="ismsconfig.section3" />
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="ismsconfig.proxyrequired" var="program" />
										</web:column>
										<web:column>
											<type:checkbox property="proxyRequired" id="proxyRequired" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="ismsconfig.proxyserver" var="program" />
										</web:column>
										<web:column>
											<type:serverAddress property="proxyServer" id="proxyServer" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="ismsconfig.proxyserverport" var="program" />
										</web:column>
										<web:column>
											<type:port property="proxyServerPort" id="proxyServerPort" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="ismsconfig.proxyserverauthentication" var="program" />
										</web:column>
										<web:column>
											<type:checkbox property="proxyServerAuthentication" id="proxyServerAuthentication" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="ismsconfig.proxyaccountname" var="program" />
										</web:column>
										<web:column>
											<type:name property="proxyAccountName" id="proxyAccountName" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="ismsconfig.proxyaccountpassword" var="program" />
										</web:column>
										<web:column>
											<type:password property="proxyAccountPassword" id="proxyAccountPassword" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:viewTitle var="program" key="ismsconfig.section4" />
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="ismsconfig.authenticationreqd" var="program" />
										</web:column>
										<web:column>
											<type:checkbox property="authenticationRequired" id="authenticationRequired" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
									</web:columnGroup>
									<web:rowOdd>
											<web:column>
											<web:legend key="ismsconfig.usernamekey" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:externalDescription property="userNameKey" id="userNameKey" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="ismsconfig.accountname" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:externalDescription property="accountName" id="accountName" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="ismsconfig.accountpasswordkey" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:externalDescription property="accountPasswordKey" id="accountPasswordKey" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="ismsconfig.accountPassword" var="program" />
										</web:column>
										<web:column>
											<type:password property="accountPassword" id="accountPassword" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="ismsconfig.mobilenumberkey" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:externalDescription property="mobileNumberKey" id="mobileNumberKey"  />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="ismsconfig.messagekey" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:externalDescription property="messageKey" id="messageKey"  />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:viewTitle var="program" key="ismsconfig.section5" />
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="ismsconfig.smppallowed" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:checkbox property="smppAllowed" id="smppAllowed" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="ismsconfig.sourcenumber" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:receiptNumber property="senderNumber" id="senderNumber"  />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="ismsconfig.systemtype" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:description property="systemType" id="systemType"  />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="ismsconfig.addressrange" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:dynamicDescription property="addressRange" id="addressRange" length="100" size="100" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
									</web:columnGroup>
									<web:rowOdd>
											<web:column>
											<web:legend key="ismsconfig.transceiverenabled" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:checkbox property="transceiverEnabled" id="transceiverEnabled" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="ismsconfig.mainserver" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:serverAddress property="mainServer" id="mainServer" />
										</web:column>
										</web:rowEven>
										<web:rowOdd>
										<web:column>
											<web:legend key="ismsconfig.mainserverport" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:port property="mainServerPort" id="mainServerPort" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="ismsconfig.failoverserver" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:serverAddress property="failOverServer" id="failOverServer" />
										</web:column>
										</web:rowEven>
										<web:rowOdd>
										<web:column>
											<web:legend key="ismsconfig.failoverserverport" var="program" />
										</web:column>
										<web:column>
											<type:port property="failOverServerPort" id="failOverServerPort" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
									</web:columnGroup>
									<web:rowEven>
											<web:column>
											<web:legend key="ismsconfig.dispatchtype" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:combo property="dispatchType" id="dispatchType" datasourceid="COMMON_SMSTYPE" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
									<web:column>
											<web:legend key="ismsconfig.retrytimeout" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:fourDnumber property="retryTimeout" id="retryTimeout"  />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="form.remarks" var="common" mandatory="true" />
										</web:column>
										<web:column>
											<type:remarks property="remarks" id="remarks" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column span="2">
											<web:submitReset />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
						</web:sectionBlock>
					</web:dividerBlock>
				</web:viewContent>
				<web:element property="sendAllowed" />
				<web:element property="command" />
				<web:element property="action" />
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
	<web:queryGrid />
	<web:TBAGrid />
	<web:contextSearch />
</web:fragment>