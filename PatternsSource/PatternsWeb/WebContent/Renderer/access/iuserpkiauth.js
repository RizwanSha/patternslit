var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'IUSERPKIAUTH';

function init() {
	refreshQueryGrid();
	refreshTBAGrid();
	if (_redisplay) {
		enableFields();
	}
}

function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'IUSERPKIAUTH_MAIN', queryGrid);
}

function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}

function clearFields() {
	
	$('#roleCode').val(EMPTY_STRING);
	$('#roleCode_error').html(EMPTY_STRING);
	$('#roleCode_desc').html(EMPTY_STRING);
	$('#effectiveDate').val(EMPTY_STRING);
	$('#effectiveDate_error').html(EMPTY_STRING);
	$('#tfaMandatory').prop('checked',false);
	$('#tfaMandatory_error').html(EMPTY_STRING);
	$('#loginAuthReqd').prop('checked',false);
	$('#loginAuthReqd_error').html(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
	enableFields();
}

function checkclick(id) {
	switch (id) {
	case 'tfaMandatory':
		enableFields();
		break;
	}
}

function enableFields(){
	if ($('#tfaMandatory').is(':checked')) {
		$('#loginAuthReqd').removeAttr("disabled"); 
	} else {
		$('#loginAuthReqd').attr("disabled", "disabled"); 
	}
	$('#loginAuthReqd').prop('checked',false);
	$('#loginAuthReqd_error').html(EMPTY_STRING);
}

function add() {
	
	$('#roleCode').focus();
}

function modify() {
	
	$('#roleCode').focus();
}

function doHelp(id) {
	switch (id) {
	case 'roleCode':
		help(CURRENT_PROGRAM_ID, 'ROLES', $('#roleCode').val(), EMPTY_STRING, $('#roleCode'));
		break;
	}
}

function loadData() {
	$('#roleCode').val(validator.getValue('ROLE_CODE'));
	$('#roleCode_desc').html(validator.getValue('BANKROLES_DESCRIPTION'));
	$('#effectiveDate').val(validator.getValue('EFFT_DATE'));
	$('#tfaMandatory').prop('checked',decodeSTB(validator.getValue('TFA_MAND')));
	if($('#tfaMandatory').is(':checked')){
		$('#loginAuthReqd').removeAttr("disabled"); 
		$('#loginAuthReqd').prop('checked',decodeSTB(validator.getValue('LOGIN_AUTH_REQD')));
	}else{
		$('#loginAuthReqd').prop('checked',false);
		$('#loginAuthReqd').attr("disabled", "disabled"); 
	}
	$('#remarks').val(validator.getValue('REMARKS'));
	resetLoading();
}

function view(source, primaryKey) {
	hideParent('access/quserpkiauth', source, primaryKey);
	resetLoading();
}

function validate(id) {
	switch (id) {
	case 'roleCode':
		roleCode_val();
		break;
	case 'effectiveDate':
		effectiveDate_val();
		break;
	case 'remarks':
		remarks_val();
		break;
	}
}

function roleCode_val() {
	var value = $('#roleCode').val();
	clearError('roleCode_error');
	if (isEmpty(value)) {
		setError('roleCode_error', MANDATORY);
		return false;
	}
	return true;
}

function effectiveDate_val() {
	var value = $('#effectiveDate').val();
	clearError('effectiveDate_error');
	if(!isValidEffectiveDate(value,'effectiveDate_error')){
		return false;
	}
	return true;
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (isEmpty(value)) {
		setError('remarks_error', MANDATORY);
		return false;
	}
	if (!isValidRemarks(value)) {
		setError('remarks_error', INVALID_REMARKS);
		return false;
	}
	return true;
}

function revalidate() {
	if (!roleCode_val()) {
		errors++;
	}
	if (!effectiveDate_val()) {
		errors++;
	}
	if (!remarks_val()) {
		errors++;
	}
}