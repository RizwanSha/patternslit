var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MSMSINTF';
function init() {
	refreshTBAGrid();
	refreshQueryGrid();
	if ($('#action').val() == ADD) {
		$('#enabled').prop('disabled', true);
		setCheckbox('enabled', YES);
	}
}

function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'MSMSINTF_MAIN', queryGrid);
}

function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}

function doclearfields(id) {
	switch (id) {
	case 'smsCode':
		if (isEmpty($('#smsCode').val())) {
			$('#smsCode_error').html(EMPTY_STRING);
			$('#smsCode_desc').html(EMPTY_STRING);
			clearNonPKFields();
			break;
		}
	}
}

function clearFields() {
	$('#smsCode').val(EMPTY_STRING);
	$('#smsCode_error').html(EMPTY_STRING);
	$('#smsCode_desc').html(EMPTY_STRING);
	clearNonPKFields();
}

function clearNonPKFields() {
	$('#description').val(EMPTY_STRING);
	$('#description_error').html(EMPTY_STRING);
	setCheckbox('sendAllowed', NO);
	setCheckbox('receiveAllowed', NO);
	clearError('sendAllowed_error');
	clearError('receiveAllowed_error');
	setCheckbox('enabled', YES);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
}

function doHelp(id) {
	switch (id) {
	case 'smsCode':
		if ($('#action').val() == MODIFY) {
			help('MSMSINTF', 'HLP_SMS_INTF', $('#smsCode').val(), EMPTY_STRING, $('#smsCode'));
		}
		break;
	}
}

function add() {
	$('#smsCode').focus();
	setCheckbox('enabled', YES);
	$('#enabled').prop('disabled', true);

}

function modify() {
	$('#smsCode').focus();
	if ($('#action').val() == MODIFY) {
		$('#enabled').prop('disabled', false);
	} else {
		$('#enabled').prop('disabled', true);
	}

}

function loadData() {
	$('#smsCode').val(validator.getValue('SMS_CODE'));
	$('#description').val(validator.getValue('SMS_DESCN'));
	setCheckbox('sendAllowed', validator.getValue('SMS_SEND_ALLOWED'));
	setCheckbox('receiveAllowed', validator.getValue('SMS_RECV_ALLOWED'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	resetLoading();
}

function view(source, primaryKey) {
	hideParent('access/qsmsintf', source, primaryKey);
	resetLoading();
}

function validate(id) {
	switch (id) {
	case 'smsCode':
		smsCode_val();
		break;
	case 'description':
		description_val();
		break;
	case 'sendAllowed':
		sendAllowed_val();
		break;
	case 'receiveAllowed':
		receiveAllowed_val();
		break;
	case 'enabled':
		enabled_val();
		break;
	case 'remarks':
		remarks_val();
		break;
	}
}
function checkclick(id) {
	switch (id) {
	case 'sendAllowed':
		if ($('#sendAllowed').is(':checked')) {
			clearError('sendAllowed_error');
			clearError('receiveAllowed_error');

		}
		break;
	case 'receiveAllowed':
		if ($('#receiveAllowed').is(':checked')) {
			clearError('sendAllowed_error');
			clearError('receiveAllowed_error');

		} else {
			if (!($('#sendAllowed').is(':checked') || $('#receiveAllowed').is(':checked'))) {
				setError('receiveAllowed_error', HMS_SMS_CHECK);
				return false;
			}

		}
		break;
	}
}
function backtrack(id) {
	switch (id) {
	case 'smsCode':
		setFocusLast('smsCode');
		break;
	case 'description':
		setFocusLast('smsCode');
		break;
	case 'sendAllowed':
		setFocusLast('description');
		break;
	case 'receiveAllowed':
		setFocusLast('sendAllowed');
		break;
	case 'enabled':
		setFocusLast('receiveAllowed');
		break;
	case 'remarks':
		if ($('#action').val() == MODIFY) {
			setFocusLast('enabled');
		} else {
			setFocusLast('receiveAllowed');
		}
		break;
	}
}

function smsCode_val() {
	var value = $('#smsCode').val();
	clearError('smsCode_error');
	if (isEmpty(value)) {
		setError('smsCode_error', HMS_MANDATORY);
		return false;
	} else if (!isValidCode(value)) {
		setError('smsCode_error', HMS_INVALID_FORMAT);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('SMS_CODE', $('#smsCode').val());
		validator.setValue(ACTION, $('#action').val());
		validator.setClass('patterns.config.web.forms.access.msmsintfbean');
		validator.setMethod('validateSMSCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('smsCode_error', validator.getValue(ERROR));
			$('#smsCode_desc').html(EMPTY_STRING);
			return false;
		} else {
			if ($('#action').val() == MODIFY) {
				PK_VALUE = $('#smsCode').val();
				if (!loadPKValues(PK_VALUE, 'smsCode_error')) {
					return false;
				}
			}
		}
	}
	setFocusLast('description');
	return true;
}

function description_val() {
	var value = $('#description').val();
	clearError('description_error');
	if (isEmpty(value)) {
		setError('description_error', HMS_MANDATORY);
		return false;
	}
	if (!isValidDescription(value)) {
		setError('description_error', HMS_INVALID_DESCRIPTION);
		return false;
	}
	setFocusLast('sendAllowed');
	return true;
}

function sendAllowed_val() {
	setFocusLast('receiveAllowed');
	return true;
}

function receiveAllowed_val() {
	clearError('receiveAllowed_error');
	if (!($('#sendAllowed').is(':checked') || $('#receiveAllowed').is(':checked'))) {
		setError('receiveAllowed_error', HMS_SMS_CHECK);
		return false;
	}
	if ($('#action').val() == ADD) {
		setFocusLast('remarks');
	} else {
		setFocusLast('enabled');
	}
	return true;
}

function enabled_val() {
	setFocusLast('remarks');
	return true;
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', HMS_INVALID_REMARKS);
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', HMS_INVALID_REMARKS);
			return false;
		}
	}
	if ($('#action').val() == MODIFY) {
		if (isEmpty(value)) {
			setError('remarks_error', HMS_MANDATORY);
			return false;
		}
	}
	setFocusOnSubmit();
	return true;
}
