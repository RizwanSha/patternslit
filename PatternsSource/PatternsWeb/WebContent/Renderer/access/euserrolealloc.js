var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'EUSERROLEALLOC';
var currentRoleDescription = EMPTY_STRING;
function init() {
	refreshTBAGrid();
	refreshQueryGrid();
}
function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'EUSERROLEALLOC_MAIN', queryGrid);
}
function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}

function doclearfields(id) {
	switch (id) {
	case 'userID':
		if (isEmpty($('#userID').val())) {
			$('#userID_error').html(EMPTY_STRING);
			$('#userID_desc').html(EMPTY_STRING);
			break;
		}
	case 'effectiveDate':
		if (isEmpty($('#effectiveDate').val())) {
			$$('#effectiveDate_error').html(EMPTY_STRING);
			clearNonPKFields();
			break;
		}
	}
}

function clearFields() {
	$('#userID').val(EMPTY_STRING);
	$('#userID_desc').html(EMPTY_STRING);
	$('#userID_error').html(EMPTY_STRING);
	$('#effectiveDate').val(EMPTY_STRING);
	$('#effectiveDate_error').html(EMPTY_STRING);
	clearNonPKFields();
}

function clearNonPKFields() {

	$('#roleCode').val(EMPTY_STRING);
	$('#roleCode_desc').html(EMPTY_STRING);
	$('#roleCode_error').html(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
}
function add() {

	$('#userID').focus();
}
function modify() {

	$('#userID').focus();
}

function loadData() {
	$('#userID').val(validator.getValue('USER_ID'));
	$('#userID_desc').html(validator.getValue('F1_USER_NAME'));
	$('#effectiveDate').val(validator.getValue('EFFT_DATE'));
	$('#roleCode').val(validator.getValue('ROLE_CODE'));
	$('#roleCode_desc').html(validator.getValue('F2_DESCRIPTION'));
	$('#remarks').val(validator.getValue('REMARKS'));
	resetLoading();
}
function view(source, primaryKey) {
	hideParent('access/quserrolealloc', source, primaryKey);
	resetLoading();
}

function doHelp(id, shiftmode) {
	switch (id) {
	case 'userID':
		if (!shiftmode) {
			help(CURRENT_PROGRAM_ID, 'HLP_USERS_M', $('#userID').val(), getUserID(), $('#userID'));
		} else {
			help(CURRENT_PROGRAM_ID, 'USERS', $('#userID').val(), EMPTY_STRING, $('#userID'));
		}
		break;

	case 'effectiveDate':
		help(CURRENT_PROGRAM_ID, 'EFFT_DATE', $('#effectiveDate').val(), $('#userID').val(), $('#effectiveDate'));
		break;
	case 'roleCode':
		help(CURRENT_PROGRAM_ID, 'ROLES', $('#roleCode').val(), EMPTY_STRING, $('#roleCode'));
		break;

	}
}

function validate(id) {
	switch (id) {
	case 'userID':
		userID_val();
		break;
	case 'effectiveDate':
		effectiveDate_val();
		break;
	case 'roleCode':
		roleCode_val();
		break;
	case 'remarks':
		remarks_val();
		break;
	}
}

function backtrack(id) {
	switch (id) {
	case 'userID':
		setFocusLast('userID');
		break;
	case 'effectiveDate':
		setFocusLast('userID');
		break;
	case 'roleCode':
		setFocusLast('effectiveDate');
		break;
	case 'remarks':
		setFocusLast('roleCode');
		break;
	}
}
function userID_val() {
	var value = $('#userID').val();
	clearError('userID_error');
	if (isEmpty(value)) {
		setError('userID_error', MANDATORY);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('USER_ID', $('#userID').val());
	validator.setValue(ACTION, USAGE);
	validator.setClass('patterns.config.web.forms.access.euserroleallocbean');
	validator.setMethod('validateCurrentRole');
	validator.sendAndReceive();
	if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		$('#spanCurrentRole').html(EMPTY_STRING);
		$('#userID_desc').html(validator.getValue("USER_NAME"));
		$('#currentRoleCode').val(validator.getValue('CODE'));
		currentRoleDescription = validator.getValue('DESCRIPTION');
		$('#spanCurrentRole').html($('#currentRoleCode').val() + ' ( ' + currentRoleDescription + ' ) ');
		setFocusLast('effectiveDate');
		return true;
	} else {
		setError('userID_error', validator.getValue(ERROR));
		return false;
	}
	setFocusLast('effectiveDate');
	return true;
}

function effectiveDate_val() {
	var value = $('#effectiveDate').val();
	clearError('effectiveDate_error');
	if(value==EMPTY_STRING){
		$('#effectiveDate').val(getCBD());
		value=getCBD();
	}
	if (!isValidEffectiveDate(value, 'effectiveDate_error')) {
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('USER_ID', $('#userID').val());
		validator.setValue('EFFT_DATE', $('#effectiveDate').val());
		validator.setValue(ACTION, $('#action').val());
		validator.setClass('patterns.config.web.forms.access.euserroleallocbean');
		validator.setMethod('validateEffectiveDate');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('effectiveDate_error', validator.getValue(ERROR));
			return false;
		} else {
			if ($('#action').val() == MODIFY) {
				PK_VALUE = getEntityCode() + PK_SEPERATOR + $('#userID').val() + PK_SEPERATOR + $('#effectiveDate').val();
				if (!loadPKValues(PK_VALUE, 'effectiveDate_error')) {
					return false;
				}
				setFocusLast('roleCode');
				return true;
			}
		}
	}
	setFocusLast('roleCode');
	return true;
}

function roleCode_val() {
	var value = $('#roleCode').val();
	clearError('roleCode_error');
	if (isEmpty(value)) {
		setError('roleCode_error', MANDATORY);
		return false;
	}
	if (value == $('#currentRoleCode').val()) {
		setError('roleCode_error', ROLE_NOT_SAME);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('CODE', $('#roleCode').val());
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.access.euserroleallocbean');
		validator.setMethod('validateRoleCode');
		validator.sendAndReceive();
		if (validator.getValue(RESULT) == DATA_AVAILABLE) {
			$('#roleCode_desc').html(validator.getValue('DESCRIPTION'));
		} else {
			setError('roleCode_error', validator.getValue(ERROR));
			return false;
		}
	}
	setFocusLast('remarks');
	return true;
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', HMS_INVALID_REMARKS);
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', HMS_INVALID_REMARKS);
			return false;
		}
	}
	if ($('#action').val() == MODIFY) {
		if (isEmpty(value)) {
			setError('remarks_error', HMS_MANDATORY);
			return false;
		}

	}
	setFocusOnSubmit();
	return true;

}
