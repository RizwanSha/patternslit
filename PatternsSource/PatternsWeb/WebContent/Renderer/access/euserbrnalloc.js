var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'EUSERBRNALLOC';
var currentBranchDescription = EMPTY_STRING;
function init() {
	refreshQueryGrid();
	refreshTBAGrid();
}
function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'EUSERBRNALLOC_MAIN', queryGrid);
}
function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}
function clearFields() {
	
	$('#userID').val(EMPTY_STRING);
	$('#userID_desc').html(EMPTY_STRING);
	$('#userID_error').html(EMPTY_STRING);
	$('#spanCurrentBranch').html(EMPTY_STRING);
	$('#effectiveDate').val(EMPTY_STRING);
	$('#effectiveDate_error').html(EMPTY_STRING);
	$('#branchCode').val(EMPTY_STRING);
	$('#branchCode_desc').html(EMPTY_STRING);
	$('#branchCode_error').html(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
}
function add() {
	
	$('#userID').focus();
}
function modify() {
	
	$('#userID').focus();
}

function loadData() {
	$('#userID').val(validator.getValue('USER_ID'));
	$('#userID_desc').html(validator.getValue('BANKUSERS_USER_NAME'));
	$('#effectiveDate').val(validator.getValue('EFFT_DATE'));
	$('#branchCode').val(validator.getValue('BRANCH_CODE'));
	$('#branchCode_desc').html(validator.getValue('MBRN_MBRN_NAME'));
	$('#remarks').val(validator.getValue('REMARKS'));
	resetLoading();
}
function view(source, primaryKey) {
	hideParent('access/quserbrnalloc', source, primaryKey);
	resetLoading();
}

function doHelp(id) {
	switch (id) {
	case 'userID':
		help(CURRENT_PROGRAM_ID, 'USERS', $('#userID').val(), EMPTY_STRING, $('#userID'));
		break;
	case 'branchCode':
		help(CURRENT_PROGRAM_ID, 'BRANCHES', $('#branchCode').val(), EMPTY_STRING, $('#branchCode'));
		break;

	}
}

function validate(id) {
	switch (id) {
	case 'userID':
		userID_val();
		break;
	case 'effectiveDate':
		effectiveDate_val();
		break;
	case 'branchCode':
		branchCode_val();
		break;
	case 'remarks':
		remarks_val();
		break;
	}
}
function userID_val() {
	var value = $('#userID').val();
	clearError('userID_error');
	if (isEmpty(value)) {
		setError('userID_error', MANDATORY);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('USER_ID', $('#userID').val());
	validator.setClass('patterns.config.web.forms.access.euserbrnallocbean');
	validator.setMethod('validateCurrentBranch');
	validator.sendAndReceive();
	if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		$('#currentBranchCode').val(validator.getValue('MBRN_CODE'));
		currentBranchDescription = validator.getValue('MBRN_NAME');
		$('#spanCurrentBranch').html($('#currentBranchCode').val() + ' ( ' + currentBranchDescription + ' ) ');
		return true;
	} else {
		setError('userID_error', validator.getValue(ERROR));
		return false;
	}

	return true;
}

function effectiveDate_val() {
	var value = $('#effectiveDate').val();
	clearError('effectiveDate_error');
	if (!isValidEffectiveDate(value, 'effectiveDate_error')) {
		return false;
	}
	return true;
}

function branchCode_val() {
	var value = $('#branchCode').val();
	clearError('branchCode_error');
	if (isEmpty(value)) {
		setError('branchCode_error', MANDATORY);
		return false;
	}
	if (value == $('#currentBranchCode').val()) {
		setError('branchCode_error', BRANCH_NOT_SAME);
		return false;
	}

	return true;
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (isEmpty(value)) {
		setError('remarks_error', MANDATORY);
		return false;
	}
	if (!isValidRemarks(value)) {
		setError('remarks_error', INVALID_REMARKS);
		return false;
	}
	return true;
}

function revalidate() {
	if (!userID_val()) {
		errors++;
	}
	if (!effectiveDate_val()) {
		errors++;
	}
	if (!branchCode_val()) {
		errors++;
	}
	if (!remarks_val()) {
		errors++;
	}
}