var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MMOBILE';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}
function clearFields() {
	$('#mobileNumber').val(EMPTY_STRING);
	$('#userid').val(EMPTY_STRING);
}
function loadData() {
	$('#mobileNumber').val(validator.getValue('MOBILENUMBER'));
	$('#userid').val(validator.getValue('USERID'));
	loadAuditFields(validator);
}