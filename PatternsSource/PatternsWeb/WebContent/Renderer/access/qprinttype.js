
var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MPRINTTYPE';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}
function clearFields() {
	$('#printTypeId').val(EMPTY_STRING);
	$('#printTypeDescription').val(EMPTY_STRING);
	$('#printingType').val(EMPTY_STRING);
	setCheckbox('enabled', "0");
	$('#Remarks').val(EMPTY_STRING);

}
function loadData() {
	$('#printTypeId').val(validator.getValue('PRNTYPE_ID'));
	$('#printTypeDescription').val(validator.getValue('DESCRIPTION'));
	$('#printingType').val(validator.getValue('PRNT_TYPE'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadAuditFields(validator);
}