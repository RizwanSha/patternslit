<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="message" tagdir="/WEB-INF/tags/message"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.access.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/access/qoprlog.js" />
	</web:dependencies>
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="qoprlog.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/access/qoprlog" id="qoprlog" method="POST">
				<web:viewContent id="po_view1">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle width="210px" />
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="form.userid" var="common" mandatory="true" />
										</web:column>
										<web:column span="3">
											<type:userHelp property="userID" id="userID" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="form.optionid" var="common" />
										</web:column>
										<web:column span="3">
											<type:code property="optionID" id="optionID" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="form.fromdate" var="common" mandatory="true" />
										</web:column>
										<web:column>
											<type:date property="fromDate" id="fromDate" />
										</web:column>
										<web:column>
											<web:legend key="form.uptodate" var="common" mandatory="true" />
										</web:column>
										<web:column>
											<type:date property="uptoDate" id="uptoDate" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column span="4">
											<type:button id="cmdDownload" key="form.download" var="common" onclick="doDownload();" />
											<type:reset key="form.reset" id="reset" var="common" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<message:messageBox id="qoprlog" />
							</web:section>
						</web:sectionBlock>
					</web:dividerBlock>
				</web:viewContent>
				<web:element property="command" />
				<web:element property="action" />
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
	<web:contextSearch />
</web:fragment>