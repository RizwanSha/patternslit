var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'EUSERBRNALLOC';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}
function clearFields() {
	$('#userID').val(EMPTY_STRING);
	$('#effectiveDate').val(EMPTY_STRING);
	$('#branchCode').val(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
}
function loadData() {
	$('#userID').val(validator.getValue('USER_ID'));
	$('#userID_desc').html(validator.getValue('BANKUSERS_USER_NAME'));
	$('#effectiveDate').val(validator.getValue('EFFT_DATE'));
	$('#branchCode').val(validator.getValue('BRANCH_CODE'));
	$('#branchCode_desc').html(validator.getValue('MBRN_MBRN_NAME'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadAuditFields(validator);
}