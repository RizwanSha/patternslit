var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'EUSERTFAREG';
function init() {
	doPurpose(FILE_PURPOSE_USER_CERTIFICATE);
	refreshQueryGrid();
	refreshTBAGrid();
	if (_redisplay) {
			if ($('#revoked').is(':checked')) 
				enablingFields();
	}
}
function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'EUSERTFAREG_MAIN', queryGrid);
}
function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}
function clearFields() {
	
	$('#userID').val(EMPTY_STRING);
	$('#userID_desc').html(EMPTY_STRING);
	$('#userID_error').html(EMPTY_STRING);
	$('#rootCaCode').val(EMPTY_STRING);
	$('#rootCaCode_desc').html(EMPTY_STRING);
	$('#rootCaCode_error').html(EMPTY_STRING);
	$('#effectiveDate').val(EMPTY_STRING);
	$('#effectiveDate_error').html(EMPTY_STRING);
	$('#revoked').prop('checked',false);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
	$('#fileInventoryNumber').val(EMPTY_STRING);
	$('#fileName').val(EMPTY_STRING);
}
function add() {
	
	$('#userID').focus();
	$('#fileExtensionList').val(CER_FILE_EXTENSION);
	enablingFields();
	
}
function modify() {
	
	$('#userID').focus();
	$('#fileExtensionList').val(CER_FILE_EXTENSION);
	
}
function enablingFields(){
	if ($('#revoked').is(':checked')){
		clearError('fileInventoryNumber_error');
		clearError('rootCaCode_error');
		$('#rootCaCode_desc').html(EMPTY_STRING);
		$('#rootCaCode_pic').attr("disabled", "disabled"); 
		$('#fileName').val(EMPTY_STRING);
		$('#fileInventoryNumber').val(EMPTY_STRING);
		$('#rootCaCode').val(EMPTY_STRING);
		$('#rootCaCode').attr("disabled", "disabled"); 
		$('#cmdFileSelector').attr("disabled", "disabled"); 
		$('#cmdPreview').attr("disabled", "disabled"); 
		$('#remarks').focus();
	}
	else{
		$('#rootCaCode').removeAttr("disabled"); 
		$('#rootCaCode_pic').removeAttr("disabled"); 
		$('#cmdFileSelector').removeAttr("disabled"); 
		$('#cmdPreview').removeAttr("disabled"); 
	  }
}
function loadData() {
	$('#userID').val(validator.getValue('USER_ID'));
	$('#effectiveDate').val(validator.getValue('EFFT_DATE'));
	$('#remarks').val(validator.getValue('REMARKS'));
	$('#rootCaCode').val(validator.getValue('ROOT_CA_CODE'));
	$('#fileInventoryNumber').val(validator.getValue('INV_NUM'));
	$('#userID_desc').html(validator.getValue('BANKUSERS_USER_NAME'));
    $('#rootCaCode_desc').html(validator.getValue('PKICERTAUTH_DESCRIPTION'));
	$('#revoked').prop('checked',decodeSTB(validator.getValue('REVOKED')));
	if($('#revoked').is(':checked')){
		$('#rootCaCode').attr("disabled", "disabled"); 
		$('#cmdFileSelector').attr("disabled", "disabled"); 
		$('#cmdPreview').attr("disabled", "disabled"); 
	}
	else{
		$('#rootCaCode').removeAttr("disabled"); 
		$('#cmdFileSelector').removeAttr("disabled"); 
		$('#cmdPreview').removeAttr("disabled"); 
	 }
	loadFileName();
	resetLoading();
}
function loadFileName(){
	var value = $('#fileInventoryNumber').val();
	validator.reset();
	validator.setClass('patterns.config.web.forms.access.eusertfaregbean');
	validator.setValue('CERT_INV_NUM', value);
	validator.setMethod('fetchFileName');
	validator.sendAndReceiveAsync(loadAdditionalData);
}

function loadAdditionalData() {
	if (validator.getValue(RESULT) == DATA_AVAILABLE) {
	$('#fileName').val(validator.getValue('CERT_FILE_NAME'));
	}
}
function view(source, primaryKey) {
	hideParent('access/qusertfareg', source, primaryKey);
	resetLoading();
}
function doHelp(id) {
	switch (id) {
	case 'userID':
		help(CURRENT_PROGRAM_ID, 'USERS', $('#userID').val(), EMPTY_STRING, $('#userID'));
		break;
	case 'rootCaCode':
		help(CURRENT_PROGRAM_ID, 'CACODES', $('#rootCaCode').val(), EMPTY_STRING, $('#rootCaCode'));
		break;	
	}
}
function validate(id) {
	switch (id) {
	case 'userID':
		userID_val();
		break;
	case 'effectiveDate':
		effectiveDate_val();
		break;
	case 'rootCaCode':
		rootCaCode_val();
		break;
	case 'remarks':
		remarks_val();
		break;
	case 'fileInventoryNumber':
		fileInventoryNumber_val();
		break;
    		
	}
}
function checkclick(id) {
	switch (id) {
	  case 'revoked':
		  enablingFields();
		  break;
	}
}
function userID_val() {
	var value = $('#userID').val();
	clearError('userID_error');
	if (isEmpty(value)) {
		setError('userID_error', MANDATORY);
		return false;
	}
	return true;
}
function effectiveDate_val() {
	var value = $('#effectiveDate').val();
	clearError('effectiveDate_error');
	if(!isValidEffectiveDate(value,'effectiveDate_error')){
		return false;
	}
	return true;
}
function rootCaCode_val() {
	if(!$('#revoked').is(':checked')){
	var value = $('#rootCaCode').val();
	clearError('rootCaCode_error');
	if (isEmpty(value)) {
		setError('rootCaCode_error', MANDATORY);
		return false;
	 }
	}
	return true;
}
function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (isEmpty(value)) {
		setError('remarks_error', MANDATORY);
		return false;
	}
	if (!isValidRemarks(value)) {
		setError('remarks_error', INVALID_REMARKS);
		return false;
	}
	return true;
}
function doFilePreview(){
	if(fileInventoryNumber_val()){
	var primaryKey = getEntityCode() + '|' + $('#fileInventoryNumber').val() ;
	showWindow('PREVIEW_VIEW',getBasePath()+ 'Renderer/common/qcertificateinfo.jsp',CERTIFICATE_VIEW_TITLE,PK+'='+primaryKey,true,false,false);
	resetLoading();
	}

}
function fileInventoryNumber_val() {
	if(!$('#revoked').is(':checked')){
	var value = $('#fileInventoryNumber').val();
	clearError('fileInventoryNumber_error');
	if (isEmpty(value)) {
		setError('fileInventoryNumber_error', MANDATORY);
		return false;
	   }
	}
	return true;
}
function revalidate() {
	
		if (!userID_val()) {
			errors++;
		}
		if (!fileInventoryNumber_val()) {
			errors++;
		}
		if (!effectiveDate_val()) {
			errors++;
		}
		if (!remarks_val()) {
 			errors++;
 		}
		
}