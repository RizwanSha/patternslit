var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'EALERTSTATUS';
function init() {
	refreshQueryGrid();
	refreshTBAGrid();
	if (_redisplay) {
		$('#jobCategory').val($('#tempJobCategory').val());
		$('#jobNature').val($('#tempJobNature').val());
		if ($('#enabled').is(':checked')) {
			showFields();
		} else {
			hideFields();
		}
	}
}

function showFields() {
	$('#cronExpression').removeAttr("disabled"); 
	$('#recordPerRequest').removeAttr("disabled"); 
}
function hideFields() {
	$('#cronExpression').attr("disabled", "disabled"); 
	$('#cronExpression').val(EMPTY_STRING);
	$('#recordPerRequest').attr("disabled", "disabled"); 
	$('#recordPerRequest').val(EMPTY_STRING);
}

function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'EALERTSTATUS_MAIN');
}
function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}
function clearFields() {
	
	$('#processorCode').val(EMPTY_STRING);
	$('#processorCode_error').html(EMPTY_STRING);
	$('#enabled').prop('checked',false);
	$('#cronExpression').val(EMPTY_STRING);
	$('#cronExpression_error').html(EMPTY_STRING);
	$('#recordPerRequest').val(EMPTY_STRING);
	$('#recordPerRequest_error').html(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
	$('#jobCategory').val(EMPTY_STRING);
	$('#jobNature').val(EMPTY_STRING);
}
function modify() {
	
	$('#processorCode').focus();
}
function loadData() {
	$('#processorCode').val(validator.getValue('CODE'));
	$('#processorCode_desc').html(validator.getValue('JOBMAST_JOB_NAME'));
	$('#enabled').prop('checked',decodeSTB(validator.getValue('STATUS')));
	$('#jobCategory').val(validator.getValue('JOBMAST_JOB_CATEGORY'));
	$('#jobNature').val(validator.getValue('JOBMAST_JOB_NATURE'));
	$('#tempJobCategory').val(validator.getValue('JOBMAST_JOB_CATEGORY'));
	$('#tempJobNature').val(validator.getValue('JOBMAST_JOB_NATURE'));
	$('#cronExpression').val(validator.getValue('CRON_EXPRESSION'));
	$('#recordPerRequest').val(validator.getValue('RECORD_PER_REQUEST'));
	$('#remarks').val(validator.getValue('REMARKS'));
	resetLoading();
}
function view(source, primaryKey) {
	hideParent('access/qalertstatus', source, primaryKey);
	resetLoading();
}
function validate(id) {
	switch (id) {
	case 'processorCode':
		processorCode_val();
		break;
	case 'cronExpression':
		cronExpression_val();
		break;
	case 'recordPerRequest':
		recordPerRequest_val();
		break;
	case 'remarks':
		remarks_val();
		break;
	}
}
function processorCode_val() {
	var value = $('#processorCode').val();
	clearError('processorCode_error');
	if (isEmpty(value)) {
		setError('processorCode_error', MANDATORY);
		return false;
	}
	if (!isValidCode(value)) {
		setError('processorCode_error', INVALID_CODE);
		return false;
	}
	return true;
}
function cronExpression_val() {
	var value = $('#cronExpression').val();
	clearError('cronExpression_error');
	if (isEmpty(value)) {
		setError('cronExpression_error', MANDATORY);
		return false;
	}
	if (!isValidCronExp(value)) {
		setError('cronExpression_error', CRON_LENGTH);
		return false;
	}
	return true;
}

function recordPerRequest_val() {
	var value = $('#recordPerRequest').val();
	clearError('recordPerRequest_error');
	if (isEmpty(value)) {
		setError('recordPerRequest_error', MANDATORY);
		return false;
	}
	if (!isNumeric(maxReqMonth)) {
		setError('recordPerRequest_error', NUMERIC_CHECK);
		return false;
	}
	if (isZero(maxReqMonth)) {
		setError('recordPerRequest_error', ZERO_CHECK);
		return false;
	}
	if (!isPositive(maxReqMonth)) {
		setError('recordPerRequest_error', POSITIVE_CHECK);
		return false;
	}
	return true;
}
function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (isEmpty(value)) {
		setError('remarks_error', MANDATORY);
		return false;
	}
	if (!isValidRemarks(value)) {
		setError('remarks_error', INVALID_REMARKS);
		return false;
	}
	return true;
}

function revalidate() {
	if (!processorCode_val()) {
		errors++;
	}
	if ($('#enabled').is(':checked')) {
		if (!cronExpression_val()) {
			errors++;
		}
		if (!recordPerRequest_val()) {
			errors++;
		}
	}
	if (!remarks_val()) {
		errors++;
	}
}

function checkclick(id) {
	switch (id) {
	case 'enabled':
		if ($('#enabled').is(':checked')) {
			showFields();
		} else {
			hideFields();
		}
		break;
	}
}