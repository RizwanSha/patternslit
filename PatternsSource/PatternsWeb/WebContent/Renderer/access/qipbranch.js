var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'EIPBRANCH';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}
function clearFields() {
	$('#brnCode').val(EMPTY_STRING);
	$('#effectiveDate').val(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	$('#enabled').prop('checked',false);

}
function loadData() {
	$('#brnCode').val(validator.getValue('BRANCH_CODE'));
	$('#brnCode_desc').html(validator.getValue('BANKBRANCHES_DESCRIPTION'));
	$('#effectiveDate').val(validator.getValue('EFFT_DATE'));
	$('#remarks').val(validator.getValue('REMARKS'));
	$('#enabled').prop('checked',decodeSTB(validator.getValue('ENABLED')));
	if ($('#enabled').is(':checked')) {
		loadBrnGrid();
	}
	loadAuditFields(validator);
}
function loadBrnGrid() {
	SOURCE_VALUE = _tableType;
	PK_VALUE = _primaryKey;
	loadGridQuery(CURRENT_PROGRAM_ID, 'EIPBRANCH_GRID', innerGrid);
}