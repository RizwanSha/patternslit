var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MUSER';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}
function clearFields() {
	$('#userID').val(EMPTY_STRING);
	$('#userID_error').html(EMPTY_STRING);
	$('#userName').val(EMPTY_STRING);
	$('#roleCode').val(EMPTY_STRING);
	$('#dateOfBirth').val(EMPTY_STRING);
	$('#emailID').val(EMPTY_STRING);
	$('#mobileNumber').val(EMPTY_STRING);
	$('#roleCode_desc').html(EMPTY_STRING);
	$('#empCode').val(EMPTY_STRING);
	$('#empCode_desc').html(EMPTY_STRING);
	$('#doctorCode').val(EMPTY_STRING);
	$('#doctorCode_desc').html(EMPTY_STRING);
	$('#designation').val(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
	$('#personIDDetails_id').val(EMPTY_STRING);
	$('#personIDDetails_name').val(EMPTY_STRING);
	$('#personIDDetails_title1').val(EMPTY_STRING);
	$('#personIDDetails_title2').val(EMPTY_STRING);
	$('#personIDDetails_doj').val(EMPTY_STRING);
	$('#createPassword').val(EMPTY_STRING);
	$('#confirmPassword').val(EMPTY_STRING);
}

function loadData() {
	$('#userID').val(validator.getValue('USER_ID'));
	$('#userName').val(validator.getValue('USER_NAME'));
	$('#empCode').val(validator.getValue('EMP_CODE'));
	$('#roleCode').val(validator.getValue('ROLE_CODE'));
	$('#roleCode_desc').html(validator.getValue('F1_DESCRIPTION'));
	$('#remarks').val(validator.getValue('REMARKS'));
	$('#branchCode').val(validator.getValue('BRANCH_CODE'));
	$('#branchCode_desc').html(validator.getValue('F3_BRANCH_NAME'));
	if (isEmpty($('#empCode').val())) {
		$('#titleName').val(validator.getValue("USER_NAME"));
		$('#dateOfJoin').val(validator.getValue("DATE_OF_JOINING"));
		$('#mobileNumber').val(validator.getValue("MOBILE_NUMBER"));
		$('#emailID').val(validator.getValue("EMAIL_ID"));
		$('#titleName').prop('readonly', false);
		$('#dateOfJoin').prop('readonly', false);
		$('#dateOfJoin_pic').prop('disabled', false);
		$('#mobileNumber').prop('readonly', false);
		$('#emailID').prop('readonly', false);
	} else {
		$('#titleName').val(validator.getValue("F2_NAME"));
		$('#dateOfJoin').val(validator.getValue("F2_DATE_OF_JOINING"));
		$('#mobileNumber').val(validator.getValue("F2_MOBILE_NO"));
		$('#emailID').val(validator.getValue("F2_EMAIL_ID"));
	}
	loadAuditFields(validator);
}
