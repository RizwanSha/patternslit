var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'EALERTSTATUS';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}
function clearFields() {
	$('#processorCode').val(EMPTY_STRING);
	$('#enabled').val(EMPTY_STRING);
	$('#cronExpression').val(EMPTY_STRING);
	$('#recordPerRequest').val(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	$('#jobCategory').val(EMPTY_STRING);
	$('#jobNature').val(EMPTY_STRING);
}
function loadData() {
	$('#processorCode').val(validator.getValue('CODE'));
	$('#processorCode_desc').html(validator.getValue('JOBMAST_JOB_NAME'));
	$('#enabled').prop('checked',decodeSTB(validator.getValue('STATUS')));
	$('#jobCategory').val(validator.getValue('JOBMAST_JOB_CATEGORY'));
	$('#jobNature').val(validator.getValue('JOBMAST_JOB_NATURE'));
	$('#cronExpression').val(validator.getValue('CRON_EXPRESSION'));
	$('#recordPerRequest').val(validator.getValue('RECORD_PER_REQUEST'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadAuditFields(validator);
}