var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'IEMAILCONFIG';
var sendAllowed = null;
var receiveAlloewd = null;

function init() {
	refreshTBAGrid();
	refreshQueryGrid();
	if (_redisplay) {
		enableFields();
		outgoingMailServerAuthenticationEnable();
		$('#emailCode').focus();
	}
}

function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'IEMAILCONFIG_MAIN', queryGrid);
}

function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}

function doclearfields(id) {
	switch (id) {
	case 'emailCode':
		if (isEmpty($('#emailCode').val())) {
			$('#emailCode_error').html(EMPTY_STRING);
			$('#emailCode_desc').html(EMPTY_STRING);
			$('#effectiveDate').val(EMPTY_STRING);
			$('#effectiveDate_error').html(EMPTY_STRING);
			clearNonPKFields();
		}
		break;
	case 'effectiveDate':
		if ($('#action').val() == MODIFY) {
			if (isEmpty($('#effectiveDate').val())) {
				$('#effectiveDate_error').html(EMPTY_STRING);
				clearNonPKFields();
			}
		}
		break;
	}
}

function clearFields() {
	$('#emailCode').val(EMPTY_STRING);
	$('#emailCode_error').html(EMPTY_STRING);
	$('#emailCode_desc').html(EMPTY_STRING);
	$('#effectiveDate').val(EMPTY_STRING);
	$('#effectiveDate_error').html(EMPTY_STRING);
	clearNonPKFields();
}

function clearNonPKFields() {
	$('#sendProtocol').val(EMPTY_STRING);
	$('#sendProtocol_error').html(EMPTY_STRING);
	$('#sendProtocol').prop('disabled', false);
	setCheckbox('sslRequired', NO);
	$('#sslRequired_error').html(EMPTY_STRING);
	$('#receiveProtocol').val(EMPTY_STRING);
	$('#receiveProtocol_error').html(EMPTY_STRING);
	$('#receiveProtocol').prop('disabled', false);
	$('#senderName').val(EMPTY_STRING);
	$('#senderName_error').html(EMPTY_STRING);
	$('#senderName').attr('readOnly', false);
	$('#senderEmail').val(EMPTY_STRING);
	$('#senderEmail_error').html(EMPTY_STRING);
	$('#senderEmail').attr('readOnly', false);
	setCheckbox('proxyRequired', NO);
	$('#proxyRequired_error').html(EMPTY_STRING);
	$('#proxyRequired').prop('disabled', false);
	$('#proxyServer').val(EMPTY_STRING);
	$('#proxyServer_error').html(EMPTY_STRING);
	$('#proxyServer').prop('readOnly', false);
	$('#proxyServerPort').val(EMPTY_STRING);
	$('#proxyServerPort_error').html(EMPTY_STRING);
	$('#proxyServerPort').prop('readOnly', false);
	setCheckbox('proxyServerAuthentication', NO);
	$('#proxyServerAuthentication_error').html(EMPTY_STRING);
	$('#proxyServerAuthentication').prop('disabled', false);
	$('#proxyAccountName').val(EMPTY_STRING);
	$('#proxyAccountName_error').html(EMPTY_STRING);
	$('#proxyAccountName').prop('readOnly', false);
	$('#proxyAccountPassword').val(EMPTY_STRING);
	$('#proxyAccountPassword_error').html(EMPTY_STRING);
	$('#proxyAccountPassword').prop('readOnly', false);
	$('#outgoingMailServer').val(EMPTY_STRING);
	$('#outgoingMailServer_error').html(EMPTY_STRING);
	$('#outgoingMailServer').prop('readOnly', false);
	$('#outgoingMailServerPort').val(EMPTY_STRING);
	$('#outgoingMailServerPort_error').html(EMPTY_STRING);
	$('#outgoingMailServerPort').prop('readOnly', false);
	setCheckbox('outgoingMailServerAuthentication', NO);
	outgoingMailServerAuthenticationEnable();
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);

}

function doHelp(id) {
	switch (id) {
	case 'emailCode':
		if ($('#action').val() == ADD) {
			help('COMMON', 'EMAILINTFLIST', $('#emailCode').val(), EMPTY_STRING, $('#emailCode'));
		} else
			help('IEMAILCONFIG', 'EMAILINTFLIST', $('#emailCode').val(), EMPTY_STRING, $('#emailCode'), null, function(gridObj, rowID) {
				$('#emailCode').val(gridObj.cells(rowID, 0).getValue());
				$('#emailCode_desc').html(gridObj.cells(rowID, 1).getValue());
				$('#effectiveDate').val(gridObj.cells(rowID, 2).getValue());
				effectiveDate_val();
				setFocusLast('effectiveDate');
			});
		break;
	case 'effectiveDate':
		if ($('#action').val() == MODIFY) {
			if (isEmpty($('#emailCode').val())) {
				setError('emailCode_error', HMS_MANDATORY);
				break;
			}
			help('IEMAILCONFIG', 'HLP_EFFT_DATE', $('#effectiveDate').val(), $('#emailCode').val(), $('#effectiveDate'));
			setFocusLast('effectiveDate');
		}
		break;
	}
}

function add() {
	$('#emailCode').focus();
	outgoingMailServerAuthenticationEnable();

}

function modify() {
	$('#emailCode').focus();
}

function loadData() {
	$('#emailCode').val(validator.getValue('EMAIL_CODE'));
	$('#emailCode_desc').html(validator.getValue('F1_EMAIL_DESCN'));
	$('#effectiveDate').val(validator.getValue('EFFT_DATE'));
	setCheckbox('sslRequired', validator.getValue('SSL_REQUIRED'));
	setCheckbox('outgoingMailServerAuthentication', validator.getValue('OUTGOING_AUTH_REQD'));
	if (validator.getValue('OUTGOING_AUTH_REQD') == COLUMN_ENABLE) {
		$('#outgoingAccountName').prop('readOnly', false);
		$('#outgoingAccountName').val(validator.getValue('OUTGOING_USER_NAME'));
		$('#outgoingAccountPassword').prop('readOnly', false);
		$('#outgoingAccountPassword').val(validator.getValue('OUTGOING_PASSWORD'));
	} else {
		$('#outgoingAccountName').val(EMPTY_STRING);
		$('#outgoingAccountName').prop('readOnly', true);
		$('#outgoingAccountPassword').prop('readOnly', true);
		$('#outgoingAccountPassword').val(EMPTY_STRING);
	}
	$('#remarks').val(validator.getValue('REMARKS'));
	var sendProtocol = validator.getValue('SEND_PROTO_TYPE');
	var receiveProtocol = (validator.getValue('RECEIVE_PROTO_TYPE'));
	var senderName = validator.getValue('COMMUNICATION_NAME');
	var senderEmail = validator.getValue('COMMUNICATION_EMAIL');
	var proxyRequired = validator.getValue('PROXY_REQUIRED');
	var proxyServer = validator.getValue('PROXY_SERVER');
	var proxyServerPort = validator.getValue('PROXY_SERVER_PORT');
	var proxyServerAuthentication = validator.getValue('PROXY_AUTH_REQD');
	var proxyAccountName = validator.getValue('PROXY_USER_NAME');
	var proxyAccountPassword = validator.getValue('PROXY_PASSWORD');
	var outgoingMailServer = validator.getValue('OUTGOING_MAIL_SERVER');
	var outgoingMailServerPort = validator.getValue('OUTGOING_MAIL_SERVER_PORT');
	if (emailCode_val(false)) {
		sendalloewd = (validator.getValue('EMAIL_SEND_ALLOWED'));
		receiveAlloewd = (validator.getValue('EMAIL_RECV_ALLOWED'));
		if (sendalloewd == COLUMN_ENABLE) {
			$('#sendProtocol').val(sendProtocol);
			$('#senderName').val(senderName);
			$('#senderEmail').val(senderEmail);
			setCheckbox('proxyRequired', proxyRequired);
			if (proxyRequired == COLUMN_ENABLE) {
				$('#proxyServer').prop('readOnly', false);
				$('#proxyServer').val(proxyServer);
				$('#proxyServerPort').prop('readOnly', false);
				$('#proxyServerPort').val(proxyServerPort);
			} else {
				$('#proxyServer').val(EMPTY_STRING);
				$('#proxyServer_error').html(EMPTY_STRING);
				$('#proxyServer').prop('readOnly', true);
				$('#proxyServerPort').val(EMPTY_STRING);
				$('#proxyServerPort_error').html(EMPTY_STRING);
				$('#proxyServerPort').prop('readOnly', true);
			}
			setCheckbox('proxyServerAuthentication', proxyServerAuthentication);
			if (proxyServerAuthentication == COLUMN_ENABLE) {
				$('#proxyAccountName').prop('readOnly', false);
				$('#proxyAccountName').val(proxyAccountName);
				$('#proxyAccountPassword').prop('readOnly', false);
				$('#proxyAccountPassword').val(proxyAccountPassword);
			} else {
				$('#proxyAccountName').val(EMPTY_STRING);
				$('#proxyAccountName_error').html(EMPTY_STRING);
				$('#proxyAccountName').prop('readOnly', true);
				$('#proxyAccountPassword').val(EMPTY_STRING);
				$('#proxyAccountPassword_error').html(EMPTY_STRING);
				$('#proxyAccountPassword').prop('readOnly', true);
			}
			$('#outgoingMailServer').val(outgoingMailServer);
			$('#outgoingMailServerPort').val(outgoingMailServerPort);
		} else {
			$('#sendProtocol').val(EMPTY_STRING);
			$('#sendProtocol_error').html(EMPTY_STRING);
			$('#sendProtocol').prop('disabled', true);
			$('#senderName').val(EMPTY_STRING);
			$('#senderName_error').html(EMPTY_STRING);
			$('#senderName').prop('readOnly', true);
			$('#senderEmail').val(EMPTY_STRING);
			$('#senderEmail_error').html(EMPTY_STRING);
			$('#senderEmail').prop('readOnly', true);
			setCheckbox('proxyRequired', NO);
			$('#proxyRequired').prop('disabled', true);
			$('#proxyServer').val(EMPTY_STRING);
			$('#proxyServer_error').html(EMPTY_STRING);
			$('#proxyServer').prop('readOnly', true);
			$('#proxyServerPort').val(EMPTY_STRING);
			$('#proxyServerPort_error').html(EMPTY_STRING);
			$('#proxyServerPort').prop('readOnly', true);
			setCheckbox('proxyServerAuthentication', NO);
			$('#proxyServerAuthentication').prop('disabled', true);
			$('#proxyAccountName').val(EMPTY_STRING);
			$('#proxyAccountName_error').html(EMPTY_STRING);
			$('#proxyAccountName').prop('readOnly', true);
			$('#proxyAccountPassword').val(EMPTY_STRING);
			$('#proxyAccountPassword_error').html(EMPTY_STRING);
			$('#proxyAccountPassword').prop('readOnly', true);
			$('#outgoingMailServer').val(EMPTY_STRING);
			$('#outgoingMailServer_error').html(EMPTY_STRING);
			$('#outgoingMailServer').prop('readOnly', true);
			$('#outgoingMailServerPort').val(EMPTY_STRING);
			$('#outgoingMailServerPort_error').html(EMPTY_STRING);
			$('#outgoingMailServerPort').prop('readOnly', true);
		}
		if (receiveAlloewd == COLUMN_ENABLE) {
			$('#receiveProtocol').val(receiveProtocol);
		} else {
			$('#receiveProtocol').val(EMPTY_STRING);
			$('#receiveProtocol_error').html(EMPTY_STRING);
			$('#receiveProtocol').prop('disabled', true);
		}

	}
	resetLoading();
}

function view(source, primaryKey) {
	hideParent('access/qemailconfig', source, primaryKey);
	resetLoading();
}

function enableFields() {
	if($('#sendAllowed').val()==COLUMN_ENABLE)
		 {
			$('#sendProtocol').prop('disabled', false);
			$('#senderName').prop('readOnly', false);
			$('#senderEmail').prop('readOnly', false);
			$('#proxyRequired').prop('disabled', false);
			if ($('#proxyRequired').is(':checked')) {
				$('#proxyServer').prop('readOnly', false);
				$('#proxyServerPort').prop('readOnly', false);

			} else {
				$('#proxyServer').val(EMPTY_STRING);
				$('#proxyServer_error').html(EMPTY_STRING);
				$('#proxyServer').prop('readOnly', true);
				$('#proxyServerPort').val(EMPTY_STRING);
				$('#proxyServerPort_error').html(EMPTY_STRING);
				$('#proxyServerPort').prop('readOnly', true);

			}
			$('#proxyServerAuthentication').prop('disabled', false);
			if ($('#proxyServerAuthentication').is(':checked')) {
				$('#proxyAccountName').prop('readOnly', false);
				$('#proxyAccountPassword').prop('readOnly', false);

			} else {
				$('#proxyAccountName').val(EMPTY_STRING);
				$('#proxyAccountName_error').html(EMPTY_STRING);
				$('#proxyAccountName').prop('readOnly', true);
				$('#proxyAccountPassword').val(EMPTY_STRING);
				$('#proxyAccountPassword_error').html(EMPTY_STRING);
				$('#proxyAccountPassword').prop('readOnly', true);
			}
			$('#outgoingMailServer').prop('readOnly', false);
			$('#outgoingMailServerPort').prop('readOnly', false);
		} else {
			$('#sendProtocol').val(EMPTY_STRING);
			$('#sendProtocol_error').html(EMPTY_STRING);
			$('#sendProtocol').prop('disabled', true);
			$('#senderName').val(EMPTY_STRING);
			$('#senderName_error').html(EMPTY_STRING);
			$('#senderName').prop('readOnly', true);
			$('#senderEmail').val(EMPTY_STRING);
			$('#senderEmail_error').html(EMPTY_STRING);
			$('#senderEmail').prop('readOnly', true);
			setCheckbox('proxyRequired', NO);
			$('#proxyRequired').prop('disabled', true);
			$('#proxyServer').val(EMPTY_STRING);
			$('#proxyServer_error').html(EMPTY_STRING);
			$('#proxyServer').prop('readOnly', true);
			$('#proxyServerPort').val(EMPTY_STRING);
			$('#proxyServerPort_error').html(EMPTY_STRING);
			$('#proxyServerPort').prop('readOnly', true);
			setCheckbox('proxyServerAuthentication', NO);
			$('#proxyServerAuthentication').prop('disabled', true);
			$('#proxyAccountName').val(EMPTY_STRING);
			$('#proxyAccountName_error').html(EMPTY_STRING);
			$('#proxyAccountName').prop('readOnly', true);
			$('#proxyAccountPassword').val(EMPTY_STRING);
			$('#proxyAccountPassword_error').html(EMPTY_STRING);
			$('#proxyAccountPassword').prop('readOnly', true);
			$('#outgoingMailServer').val(EMPTY_STRING);
			$('#outgoingMailServer_error').html(EMPTY_STRING);
			$('#outgoingMailServer').prop('readOnly', true);
			$('#outgoingMailServerPort').val(EMPTY_STRING);
			$('#outgoingMailServerPort_error').html(EMPTY_STRING);
			$('#outgoingMailServerPort').prop('readOnly', true);
		}
		if($('#receiveAlloewd').val()==COLUMN_ENABLE) {
			$('#receiveProtocol').prop('disabled', false);
		} else {
			$('#receiveProtocol').val(EMPTY_STRING);
			$('#receiveProtocol_error').html(EMPTY_STRING);
			$('#receiveProtocol').prop('disabled', true);

		}
}

function outgoingMailServerAuthenticationEnable() {
	if ($('#outgoingMailServerAuthentication').is(':checked')) {
		$('#outgoingAccountName').prop('readOnly', false);
		$('#outgoingAccountPassword').prop('readOnly', false);
	} else {
		$('#outgoingAccountName').val(EMPTY_STRING);
		$('#outgoingAccountName_error').html(EMPTY_STRING);
		$('#outgoingAccountName').prop('readOnly', true);
		$('#outgoingAccountPassword').val(EMPTY_STRING);
		$('#outgoingAccountPassword_error').html(EMPTY_STRING);
		$('#outgoingAccountPassword').prop('readOnly', true);

	}
}

function checkclick(id) {
	switch (id) {
	case 'proxyRequired':
		if ($('#proxyRequired').is(':checked')) {
			$('#proxyServer').prop('readOnly', false);
			$('#proxyServerPort').prop('readOnly', false);

		} else {
			$('#proxyServer').val(EMPTY_STRING);
			$('#proxyServer_error').html(EMPTY_STRING);
			$('#proxyServer').prop('readOnly', true);
			$('#proxyServerPort').val(EMPTY_STRING);
			$('#proxyServerPort_error').html(EMPTY_STRING);
			$('#proxyServerPort').prop('readOnly', true);

		}
		break;
	case 'proxyServerAuthentication':
		if ($('#proxyServerAuthentication').is(':checked')) {
			$('#proxyAccountName').prop('readOnly', false);
			$('#proxyAccountPassword').prop('readOnly', false);

		} else {
			$('#proxyAccountName').val(EMPTY_STRING);
			$('#proxyAccountName_error').html(EMPTY_STRING);
			$('#proxyAccountName').prop('readOnly', true);
			$('#proxyAccountPassword').val(EMPTY_STRING);
			$('#proxyAccountPassword_error').html(EMPTY_STRING);
			$('#proxyAccountPassword').prop('readOnly', true);
		}
		break;
	case 'outgoingMailServerAuthentication':
		if ($('#outgoingMailServerAuthentication').is(':checked')) {
			$('#outgoingAccountName').prop('readOnly', false);
			$('#outgoingAccountPassword').prop('readOnly', false);
		} else {
			$('#outgoingAccountName').val(EMPTY_STRING);
			$('#outgoingAccountName_error').html(EMPTY_STRING);
			$('#outgoingAccountName').prop('readOnly', true);
			$('#outgoingAccountPassword').val(EMPTY_STRING);
			$('#outgoingAccountPassword_error').html(EMPTY_STRING);
			$('#outgoingAccountPassword').prop('readOnly', true);

		}
		break;
	}
}

function validate(id) {
	switch (id) {
	case 'emailCode':
		emailCode_val(true);
		break;
	case 'effectiveDate':
		effectiveDate_val();
		break;
	case 'sendProtocol':
		sendProtocol_val();
		break;
	case 'sslRequired':
		sslRequired_val();
		break;
	case 'receiveProtocol':
		receiveProtocol_val();
		break;
	case 'senderName':
		senderName_val();
		break;
	case 'senderEmail':
		senderEmail_val();
		break;
	case 'proxyRequired':
		proxyRequired_val();
		break;
	case 'proxyServer':
		proxyServer_val();
		break;
	case 'proxyServerPort':
		proxyServerPort_val();
		break;
	case 'proxyServerAuthentication':
		proxyServerAuthentication_val();
		break;
	case 'proxyAccountName':
		proxyAccountName_val();
		break;
	case 'proxyAccountPassword':
		proxyAccountPassword_val();
		break;
	case 'outgoingMailServer':
		outgoingMailServer_val();
		break;
	case 'outgoingMailServerPort':
		outgoingMailServerPort_val();
		break;
	case 'outgoingMailServerAuthentication':
		outgoingMailServerAuthentication_val();
		break;
	case 'outgoingAccountName':
		outgoingAccountName_val();
		break;
	case 'outgoingAccountPassword':
		outgoingAccountPassword_val();
		break;
	case 'remarks':
		remarks_val();
		break;
	}
}

function backtrack(id) {
	switch (id) {
	case 'emailCode':
		setFocusLast('emailCode');
		break;
	case 'effectiveDate':
		setFocusLast('emailCode');
		break;
	case 'sendProtocol':
		setFocusLast('effectiveDate');
		break;
	case 'sslRequired':
		if ($('#sendProtocol').prop('disabled')) {
			setFocusLast('effectiveDate');
		} else {
			setFocusLast('sendProtocol');
		}
		break;
	case 'receiveProtocol':
		setFocusLast('sslRequired');
		break;
	case 'senderName':
		if ($('#receiveProtocol').prop('disabled')) {
			setFocusLast('sslRequired');
		} else {
			setFocusLast('receiveProtocol');
		}
		break;
	case 'senderEmail':
		setFocusLast('senderName');
		break;
	case 'proxyRequired':
		setFocusLast('senderEmail');
		break;
	case 'proxyServer':
		setFocusLast('proxyRequired');
		break;
	case 'proxyServerPort':
		setFocusLast('proxyServer');
		break;
	case 'proxyServerAuthentication':
		if ($('#proxyServerPort').prop('readonly')) {
			setFocusLast('proxyRequired');
		} else {
			setFocusLast('proxyServerPort');
		}
		break;
	case 'proxyAccountName':
		setFocusLast('proxyServerAuthentication');
		break;
	case 'proxyAccountPassword':
		setFocusLast('proxyAccountName');
		break;
	case 'outgoingMailServer':
		if ($('#proxyAccountPassword').prop('readonly')) {
			setFocusLast('proxyServerAuthentication');
		} else {
			setFocusLast('proxyAccountPassword');
		}
		break;
	case 'outgoingMailServerPort':
		setFocusLast('outgoingMailServer');
		break;
	case 'outgoingMailServerAuthentication':
		if ($('#outgoingMailServerPort').prop('readonly')) {
			setFocusLast('receiveProtocol');
			if ($('#receiveProtocol').prop('disabled')) {
				setFocusLast('sslRequired');
			}
		} else {
			setFocusLast('outgoingMailServerPort');
		}
		break;
	case 'outgoingAccountName':
		setFocusLast('outgoingMailServerAuthentication');
		break;
	case 'outgoingAccountPassword':
		if ($('#outgoingAccountName').prop('readonly')) {
			setFocusLast('outgoingMailServerAuthentication');
		} else {
			setFocusLast('outgoingAccountName');
		}
		break;
	case 'remarks':
		if ($('#outgoingAccountPassword').prop('readonly')) {
			setFocusLast('outgoingMailServerAuthentication');
		} else {
			setFocusLast('outgoingAccountPassword');
		}
		break;
	}
}

function enabled_val() {
	if ($('#action').val() == MODIFY) {
		setFocusLast('remarks');
	}
	return true;
}

function emailCode_val(valMode) {
	var value = $('#emailCode').val();
	clearError('emailCode_error');
	if (isEmpty(value)) {
		setError('emailCode_error', HMS_MANDATORY);
		return false;
	} else if (!isValidCode(value)) {
		setError('emailCode_error', HMS_INVALID_FORMAT);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('EMAIL_CODE', $('#emailCode').val());
		validator.setValue(ACTION, $('#action').val());
		validator.setClass('patterns.config.web.forms.access.iemailconfigbean');
		validator.setMethod('validateEmailCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('emailCode_error', validator.getValue(ERROR));
			$('#emailCode_desc').html(EMPTY_STRING);
			clearNonPKFields();
			return false;
		} else
			$('#emailCode_desc').html(validator.getValue('EMAIL_DESCN'));
		sendAllowed=validator.getValue('EMAIL_SEND_ALLOWED');
         receiveAlloewd=validator.getValue('EMAIL_RECV_ALLOWED');
		if (sendAllowed == COLUMN_ENABLE) {
			$('#sendProtocol').prop('disabled', false);
			$('#senderName').prop('readOnly', false);
			$('#senderEmail').prop('readOnly', false);
			$('#proxyRequired').prop('disabled', false);
			if ($('#proxyRequired').is(':checked')) {
				$('#proxyServer').prop('readOnly', false);
				$('#proxyServerPort').prop('readOnly', false);

			} else {
				$('#proxyServer').val(EMPTY_STRING);
				$('#proxyServer_error').html(EMPTY_STRING);
				$('#proxyServer').prop('readOnly', true);
				$('#proxyServerPort').val(EMPTY_STRING);
				$('#proxyServerPort_error').html(EMPTY_STRING);
				$('#proxyServerPort').prop('readOnly', true);

			}
			$('#proxyServerAuthentication').prop('disabled', false);
			if ($('#proxyServerAuthentication').is(':checked')) {
				$('#proxyAccountName').prop('readOnly', false);
				$('#proxyAccountPassword').prop('readOnly', false);

			} else {
				$('#proxyAccountName').val(EMPTY_STRING);
				$('#proxyAccountName_error').html(EMPTY_STRING);
				$('#proxyAccountName').prop('readOnly', true);
				$('#proxyAccountPassword').val(EMPTY_STRING);
				$('#proxyAccountPassword_error').html(EMPTY_STRING);
				$('#proxyAccountPassword').prop('readOnly', true);
			}
			$('#outgoingMailServer').prop('readOnly', false);
			$('#outgoingMailServerPort').prop('readOnly', false);
		} else {
			$('#sendProtocol').val(EMPTY_STRING);
			$('#sendProtocol_error').html(EMPTY_STRING);
			$('#sendProtocol').prop('disabled', true);
			$('#senderName').val(EMPTY_STRING);
			$('#senderName_error').html(EMPTY_STRING);
			$('#senderName').prop('readOnly', true);
			$('#senderEmail').val(EMPTY_STRING);
			$('#senderEmail_error').html(EMPTY_STRING);
			$('#senderEmail').prop('readOnly', true);
			setCheckbox('proxyRequired', NO);
			$('#proxyRequired').prop('disabled', true);
			$('#proxyServer').val(EMPTY_STRING);
			$('#proxyServer_error').html(EMPTY_STRING);
			$('#proxyServer').prop('readOnly', true);
			$('#proxyServerPort').val(EMPTY_STRING);
			$('#proxyServerPort_error').html(EMPTY_STRING);
			$('#proxyServerPort').prop('readOnly', true);
			setCheckbox('proxyServerAuthentication', NO);
			$('#proxyServerAuthentication').prop('disabled', true);
			$('#proxyAccountName').val(EMPTY_STRING);
			$('#proxyAccountName_error').html(EMPTY_STRING);
			$('#proxyAccountName').prop('readOnly', true);
			$('#proxyAccountPassword').val(EMPTY_STRING);
			$('#proxyAccountPassword_error').html(EMPTY_STRING);
			$('#proxyAccountPassword').prop('readOnly', true);
			$('#outgoingMailServer').val(EMPTY_STRING);
			$('#outgoingMailServer_error').html(EMPTY_STRING);
			$('#outgoingMailServer').prop('readOnly', true);
			$('#outgoingMailServerPort').val(EMPTY_STRING);
			$('#outgoingMailServerPort_error').html(EMPTY_STRING);
			$('#outgoingMailServerPort').prop('readOnly', true);
		}
		receiveAlloewd = (validator.getValue('EMAIL_RECV_ALLOWED'));
		if (validator.getValue('EMAIL_RECV_ALLOWED') == COLUMN_ENABLE) {
			$('#receiveProtocol').prop('disabled', false);
		} else {
			$('#receiveProtocol').val(EMPTY_STRING);
			$('#receiveProtocol_error').html(EMPTY_STRING);
			$('#receiveProtocol').prop('disabled', true);

		}
		outgoingMailServerAuthenticationEnable();
	}
	if (valMode)
		setFocusLast('effectiveDate');
	return true;
}

function effectiveDate_val() {
	var value = $('#effectiveDate').val();
	clearError('effectiveDate_error');
	if (isEmpty(value)) {
		$('#effectiveDate').val(getCBD());
		value = $(effectiveDate).val();
	}
	if (!isValidEffectiveDate(value, 'effectiveDate_error')) {
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('ENTITY_CODE', getEntityCode());
		validator.setValue('EMAIL_CODE', $('#emailCode').val());
		validator.setValue('EFFT_DATE', $('#effectiveDate').val());
		validator.setValue(ACTION, $('#action').val());
		validator.setClass('patterns.config.web.forms.access.iemailconfigbean');
		validator.setMethod('validateEffectiveDate');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('effectiveDate_error', validator.getValue(ERROR));
			setFocusLast('effectiveDate');
			return false;
		} else {
			if ($('#action').val() == MODIFY) {
				PK_VALUE = getEntityCode() + PK_SEPERATOR + $('#emailCode').val() + PK_SEPERATOR + $('#effectiveDate').val();
				if (!loadPKValues(PK_VALUE, 'effectiveDate_error')) {
					return false;
				}
			}
		}
	}
	if (sendAllowed == COLUMN_ENABLE) {
		$('#sendProtocol').prop('disabled', false);
		setFocusLast('sendProtocol');
	} else {
		setFocusLast('sslRequired');
	}
	return true;
}

function sendProtocol_val() {
	if (sendAllowed == COLUMN_ENABLE) {
		$('#sendProtocol').prop('disabled', false);
		var value = $('#sendProtocol').val();
		clearError('sendProtocol_error');
		if (isEmpty(value)) {
			setError('sendProtocol_error', HMS_MANDATORY);
			return false;
		}
	} else if (sendAllowed == COLUMN_DISABLE) {
		$('#sendProtocol').val(EMPTY_STRING);
		$('#sendProtocol_error').html(EMPTY_STRING);
		$('#sendProtocol').prop('disabled', true);
	}
	setFocusLast('sslRequired');
	return true;
}

function sslRequired_val() {
	if (receiveAlloewd == COLUMN_ENABLE) {
		$('#receiveProtocol').prop('disabled', false);
		setFocusLast('receiveProtocol');
	} else if (receiveAlloewd == COLUMN_DISABLE) {
		$('#receiveProtocol').val(EMPTY_STRING);
		$('#receiveProtocol_error').html(EMPTY_STRING);
		$('#receiveProtocol').prop('disabled', true);
		if (sendAllowed == COLUMN_ENABLE) {
			$('#senderName').prop('readOnly', false);
		}
		setFocusLast('senderName');
	}
	if ((sendAllowed == COLUMN_DISABLE) && (receiveAlloewd == COLUMN_DISABLE)) {
		setFocusLast('outgoingMailServerAuthentication');
	}
	return true;

}

function receiveProtocol_val() {
	if (receiveAlloewd == COLUMN_ENABLE) {
		$('#receiveProtocol').prop('disabled', false);
		var value = $('#receiveProtocol').val();
		clearError('receiveProtocol_error');
		if (isEmpty(value)) {
			setError('receiveProtocol_error', HMS_MANDATORY);
			return false;
		}
	} else if (receiveAlloewd == COLUMN_DISABLE) {
		$('#receiveProtocol').val(EMPTY_STRING);
		$('#receiveProtocol_error').html(EMPTY_STRING);
		$('#receiveProtocol').prop('disabled', true);
	}
	setFocusLast('senderName');
	if (sendAllowed == COLUMN_DISABLE) {
		setFocusLast('outgoingMailServerAuthentication');
	}
	return true;
}

function senderName_val() {
	if (sendAllowed == COLUMN_ENABLE) {
		$('#senderName').prop('readOnly', false);
		var value = $('#senderName').val();
		clearError('senderName_error');
		if (isEmpty(value)) {
			setError('senderName_error', MANDATORY);
			return false;
		}
		if (!isValidName(value)) {
			setError('senderName_error', INVALID_NAME);
			return false;
		}
	} else if (sendAllowed == COLUMN_DISABLE) {
		$('#senderName').val(EMPTY_STRING);
		$('#senderName_error').html(EMPTY_STRING);
		$('#senderName').prop('readOnly', true);
	}
	setFocusLast('senderEmail');
	return true;
}

function senderEmail_val() {
	if (sendAllowed == COLUMN_ENABLE) {
		$('#senderEmail').prop('readOnly', false);
		var value = $('#senderEmail').val();
		clearError('senderEmail_error');
		if (isEmpty(value)) {
			setError('senderEmail_error', MANDATORY);
			return false;
		}
		if (!isEmail(value)) {
			setError('senderEmail_error', INVALID_EMAIL_ID);
			return false;
		}
	} else if (sendAllowed == COLUMN_DISABLE) {
		$('#senderEmail').val(EMPTY_STRING);
		$('#senderEmail_error').html(EMPTY_STRING);
		$('#senderEmail').prop('readOnly', true);
	}
	setFocusLast('proxyRequired');
	return true;
}

function proxyRequired_val() {
	if (sendAllowed == COLUMN_ENABLE) {
		$('#proxyRequired').prop('disabled', false);
	} else if (sendAllowed == COLUMN_DISABLE) {
		setCheckbox('proxyRequired', NO);
		$('#proxyRequired').prop('disabled', true);
	}
	if ($('#proxyRequired').is(':checked')) {
		setFocusLast('proxyServer');
	} else {
		setFocusLast('proxyServerAuthentication');
	}
	return true;
}

function proxyServer_val() {
	if ($('#proxyRequired').is(':checked')) {
		$('#proxyServer').prop('readOnly', false);
		var value = $('#proxyServer').val();
		clearError('proxyServer_error');
		if (isEmpty(value)) {
			setError('proxyServer_error', MANDATORY);
			return false;
		}
		if (!isValidServerAddress(value)) {
			setError('proxyServer_error', INVALID_SERVER_ADDRESS);
			return false;
		}

	} else {
		$('#proxyServer').val(EMPTY_STRING);
		$('#proxyServer_error').html(EMPTY_STRING);
		$('#proxyServer').prop('readOnly', true);
	}
	setFocusLast('proxyServerPort');
	return true;
}

function proxyServerPort_val() {
	if ($('#proxyRequired').is(':checked')) {
		$('#proxyServerPort').prop('readOnly', false);
		var value = $('#proxyServerPort').val();
		clearError('proxyServerPort_error');
		if (isEmpty(value)) {
			setError('proxyServerPort_error', MANDATORY);
			return false;
		}
		if (!isNumeric(value)) {
			setError('proxyServerPort_error', NUMERIC_CHECK);
			return false;
		}
		if (!isValidPort(value)) {
			setError('proxyServerPort_error', INVALID_PORT);
			return false;
		}
	} else {
		$('#proxyServerPort').val(EMPTY_STRING);
		$('#proxyServerPort_error').html(EMPTY_STRING);
		$('#proxyServerPort').prop('readOnly', true);
	}
	setFocusLast('proxyServerAuthentication');
	return true;
}

function proxyServerAuthentication_val() {
	if (sendAllowed == COLUMN_ENABLE) {
		$('#proxyServerAuthentication').prop('disabled', false);
	} else if (sendAllowed == COLUMN_DISABLE) {
		setCheckbox('proxyServerAuthentication', NO);
		$('#proxyServerAuthentication').prop('disabled', true);
	}
	if ($('#proxyServerAuthentication').is(':checked')) {
		setFocusLast('proxyAccountName');
	} else {
		setFocusLast('outgoingMailServer');
	}
	return true;
}

function proxyAccountName_val() {
	if ($('#proxyServerAuthentication').is(':checked')) {
		$('#proxyAccountName').prop('readOnly', false);
		var value = $('#proxyAccountName').val();
		clearError('proxyAccountName_error');
		if (isEmpty(value)) {
			setError('proxyAccountName_error', MANDATORY);
			return false;
		}
		if (!isValidName(value)) {
			setError('proxyAccountName_error', INVALID_NAME);
			return false;
		}
	} else {
		$('#proxyAccountName').val(EMPTY_STRING);
		$('#proxyAccountName_error').html(EMPTY_STRING);
		$('#proxyAccountName').prop('readOnly', true);
	}
	setFocusLast('proxyAccountPassword');
	return true;
}

function proxyAccountPassword_val() {
	if ($('#proxyServerAuthentication').is(':checked')) {
		$('#proxyAccountPassword').prop('readOnly', false);
		var value = $('#proxyAccountPassword').val();
		clearError('proxyAccountPassword_error');
		if (isEmpty(value)) {
			setError('proxyAccountPassword_error', MANDATORY);
			return false;
		}
		if (!isValidCredential(value)) {
			setError('proxyAccountPassword_error', INVALID_CREDENTIALS);
			return false;
		}
	} else {
		$('#proxyAccountPassword').val(EMPTY_STRING);
		$('#proxyAccountPassword_error').html(EMPTY_STRING);
		$('#proxyAccountPassword').prop('readOnly', true);
	}
	setFocusLast('outgoingMailServer');
	return true;
}

function outgoingMailServer_val() {
	if (sendAllowed == COLUMN_ENABLE) {
		$('#outgoingMailServer').prop('readOnly', false);
		var value = $('#outgoingMailServer').val();
		clearError('outgoingMailServer_error');
		if (isEmpty(value)) {
			setError('outgoingMailServer_error', MANDATORY);
			return false;
		}
		if (!isValidServerAddress(value)) {
			setError('outgoingMailServer_error', INVALID_SERVER_ADDRESS);
			return false;
		}
	} else if (sendAllowed == COLUMN_DISABLE) {
		$('#outgoingMailServer').val(EMPTY_STRING);
		$('#outgoingMailServer_error').html(EMPTY_STRING);
		$('#outgoingMailServer').prop('readOnly', true);
	}
	setFocusLast('outgoingMailServerPort');
	return true;
}

function outgoingMailServerPort_val() {
	if (sendAllowed == COLUMN_ENABLE) {
		$('#outgoingMailServerPort').prop('readOnly', false);
		var value = $('#outgoingMailServerPort').val();
		clearError('outgoingMailServerPort_error');
		if (isEmpty(value)) {
			setError('outgoingMailServerPort_error', MANDATORY);
			return false;
		}
		if (!isNumeric(value)) {
			setError('outgoingMailServerPort_error', NUMERIC_CHECK);
			return false;
		}
		if (!isValidPort(value)) {
			setError('outgoingMailServerPort_error', INVALID_PORT);
			return false;
		}
	} else if (sendAllowed == COLUMN_DISABLE) {
		$('#outgoingMailServerPort').val(EMPTY_STRING);
		$('#outgoingMailServerPort_error').html(EMPTY_STRING);
		$('#outgoingMailServerPort').prop('readOnly', true);
	}
	setFocusLast('outgoingMailServerAuthentication');
	return true;

}

function outgoingMailServerAuthentication_val() {
	if ($('#outgoingMailServerAuthentication').is(':checked')) {
		setFocusLast('outgoingAccountName');
	} else {
		outgoingMailServerAuthenticationEnable();
		setFocusLast('remarks');
	}

}

function outgoingAccountName_val() {
	if ($('#outgoingMailServerAuthentication').is(':checked')) {
		$('#outgoingAccountName').prop('readOnly', false);
		var value = $('#outgoingAccountName').val();
		clearError('outgoingAccountName_error');
		if (isEmpty(value)) {
			setError('outgoingAccountName_error', MANDATORY);
			return false;
		}
		if (!isValidName(value)) {
			setError('outgoingAccountName_error', INVALID_NAME);
			return false;
		}
	} else {
		$('#outgoingAccountName').val(EMPTY_STRING);
		$('#outgoingAccountName_error').html(EMPTY_STRING);
		$('#outgoingAccountName').prop('readOnly', true);
	}
	setFocusLast('outgoingAccountPassword');
	return true;
}

function outgoingAccountPassword_val() {
	if ($('#outgoingMailServerAuthentication').is(':checked')) {
		$('#outgoingAccountPassword').prop('readOnly', false);
		var value = $('#outgoingAccountPassword').val();
		clearError('outgoingAccountPassword_error');
		if (isEmpty(value)) {
			setError('outgoingAccountPassword_error', MANDATORY);
			return false;
		}
		if (!isValidCredential(value)) {
			setError('outgoingAccountPassword_error', INVALID_CREDENTIALS);
			return false;
		}
	} else {
		$('#outgoingAccountPassword').val(EMPTY_STRING);
		$('#outgoingAccountPassword_error').html(EMPTY_STRING);
		$('#outgoingAccountPassword').prop('readOnly', true);
	}
	setFocusLast('remarks');
	return true;
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', HMS_INVALID_REMARKS);
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', HMS_INVALID_REMARKS);
			return false;
		}
	}
	if ($('#action').val() == MODIFY) {
		if (isEmpty(value)) {
			setError('remarks_error', HMS_MANDATORY);
			return false;
		}

	}
	setFocusOnSubmit();
	return true;

}
function revalidate()
{
	$('#sendAllowed').val(sendAllowed);

	$('#receiveAlloewd').val(receiveAlloewd);
	}