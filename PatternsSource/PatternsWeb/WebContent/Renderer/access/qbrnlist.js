var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MBRNLIST';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}
function clearFields() {
	$('#brnListCode').val(EMPTY_STRING);
	$('#brnListDescription').val(EMPTY_STRING);
	$('#enabled').prop('checked',false);
	$('#remarks').val(EMPTY_STRING);
}
function loadProgramsGrid() {
	SOURCE_VALUE = _tableType;
	PK_VALUE = _primaryKey;
	loadGridQuery(CURRENT_PROGRAM_ID, 'BRN_GRID', brnGrid);
}
function loadData() {
	$('#brnListCode').val(validator.getValue('CODE'));
	$('#brnListDescription').val(validator.getValue('DESCRIPTION'));
	$('#enabled').prop('checked',decodeSTB(validator.getValue('ENABLED')));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadProgramsGrid();
	loadAuditFields(validator);
}