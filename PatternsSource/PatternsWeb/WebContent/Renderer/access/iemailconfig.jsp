<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.access.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/access/iemailconfig.js" />
	</web:dependencies>
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="iemailconfig.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/access/iemailconfig" id="iemailconfig" method="POST">
				<web:dividerBlock>
					<type:optionBox add="true" modify="true" view="true" template="true" />
				</web:dividerBlock>
				<web:viewContent id="po_view1" styleClass="hidden">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="280px" />
										<web:columnStyle />
									</web:columnGroup>
											<web:rowEven>
										<web:column>
											<web:legend key="iemailconfig.emailcode" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:code property="emailCode" id="emailCode" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="form.effectivedate" var="common" mandatory="true" />
										</web:column>
										<web:column>
											<type:date property="effectiveDate" id="effectiveDate" lookup="true" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="280px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="iemailconfig.sendprotocol" var="program" />
										</web:column>
										<web:column>
											<type:combo property="sendProtocol" id="sendProtocol" datasourceid="COMMON_SENDPROTOCOL" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="iemailconfig.sslrequired" var="program" />
										</web:column>
										<web:column>
											<type:checkbox property="sslRequired" id="sslRequired" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="iemailconfig.receiveprotocol" var="program" />
										</web:column>
										<web:column>
											<type:combo property="receiveProtocol" id="receiveProtocol" datasourceid="COMMON_REVDPROTOCOL" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:viewTitle var="program" key="iemailconfig.section1" />
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="280px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="iemailconfig.sendername" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:name property="senderName" id="senderName" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="iemailconfig.senderemail" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:email property="senderEmail" id="senderEmail" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:viewTitle var="program" key="iemailconfig.section2" />
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="280px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="iemailconfig.proxyrequired" var="program" />
										</web:column>
										<web:column>
											<type:checkbox property="proxyRequired" id="proxyRequired" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="iemailconfig.proxyserver" var="program" />
										</web:column>
										<web:column>
											<type:serverAddress property="proxyServer" id="proxyServer" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="iemailconfig.proxyserverport" var="program" />
										</web:column>
										<web:column>
											<type:port property="proxyServerPort" id="proxyServerPort" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="iemailconfig.proxyserverauthentication" var="program" />
										</web:column>
										<web:column>
											<type:checkbox property="proxyServerAuthentication" id="proxyServerAuthentication" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="iemailconfig.proxyaccountname" var="program" />
										</web:column>
										<web:column>
											<type:name property="proxyAccountName" id="proxyAccountName" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="iemailconfig.proxyaccountpassword" var="program" />
										</web:column>
										<web:column>
											<type:password property="proxyAccountPassword" id="proxyAccountPassword" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:viewTitle var="program" key="iemailconfig.section3" />
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="280px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="iemailconfig.outgoingmailserver" var="program" />
										</web:column>
										<web:column>
											<type:serverAddress property="outgoingMailServer" id="outgoingMailServer" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="iemailconfig.outgoingmailserverport" var="program" />
										</web:column>
										<web:column>
											<type:port property="outgoingMailServerPort" id="outgoingMailServerPort" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="iemailconfig.outgoingmailserverauthentication" var="program" />
										</web:column>
										<web:column>
											<type:checkbox property="outgoingMailServerAuthentication" id="outgoingMailServerAuthentication" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="iemailconfig.outgoingaccountname" var="program" />
										</web:column>
										<web:column>
											<type:name property="outgoingAccountName" id="outgoingAccountName" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="iemailconfig.outgoingaccountpassword" var="program" />
										</web:column>
										<web:column>
											<type:password property="outgoingAccountPassword" id="outgoingAccountPassword" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="form.remarks" var="common" />
										</web:column>
										<web:column>
											<type:remarks property="remarks" id="remarks" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column span="2">
											<web:submitReset />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
						</web:sectionBlock>
					</web:dividerBlock>
				</web:viewContent>
				<web:element property="command" />
				<web:element property="action" />
				<web:element property="sendAllowed" />
				<web:element property="receiveAlloewd" />
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
	<web:queryGrid />
	<web:TBAGrid />
	<web:contextSearch />
</web:fragment>