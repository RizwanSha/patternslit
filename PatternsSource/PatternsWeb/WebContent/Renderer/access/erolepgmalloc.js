var validator = new xmlHTTPValidator();
var ROLE_TYPE = EMPTY_STRING;
var CURRENT_PROGRAM_ID = 'EROLEPGMALLOC';
function init() {
	refreshTBAGrid();
	refreshQueryGrid();
	if (_redisplay) {
		loadProgramsGrid();
		pgmGrid.clearAll();
		if (!isEmpty($('#xmlPgmList').val())) {
			pgmGrid.loadXMLString($('#xmlPgmList').val());
		}
		loadProgramsGrid();
	}
	pgmGrid.attachEvent('onCheckbox', doOnCheck);
}
function loadProgramsGrid() {
	pgmGrid.clearAll();
	validator.clearMap();
	validator.setValue('ROLE_CODE', $('#roleCode').val());
	validator.setValue('MODULE_ID', $('#moduleCode').val());
	validator.setValue(ACTION, $('#action').val());
	validator.setClass('patterns.config.web.forms.access.erolepgmallocbean');
	validator.setMethod('getRolePrograms');
	validator.sendAndReceive();
	if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		ROLE_TYPE = validator.getValue("ROLE_TYPE");
		xmlString = validator.getValue(RESULT_XML);
		pgmGrid.loadXMLString(xmlString);
		if ($('#action').val() == ADD) {
			disableGrid();
		} else {
			gridDisable();
		}
		setFocusLast('effectiveDate');
		return true;
	} else {
		setError('roleCode_error', validator.getValue(ERROR));
		setFocusLast('roleCode');
		return false;

	}
}

function disableGrid() {
	var noofrows = pgmGrid.getRowsNum();
	for (i = 0; i < noofrows; i++) {
		pgmGrid.cells2(i, 4).setValue(false);
		pgmGrid.cells2(i, 6).setValue(false);
		pgmGrid.cells2(i, 8).setValue(false);
		pgmGrid.cells2(i, 10).setValue(false);
		pgmGrid.cells2(i, 12).setValue(false);
		pgmGrid.cells2(i, 14).setValue(false);
		pgmGrid.cells2(i, 4).setDisabled(true);
		pgmGrid.cells2(i, 6).setDisabled(true);
		pgmGrid.cells2(i, 8).setDisabled(true);
		pgmGrid.cells2(i, 10).setDisabled(true);
		pgmGrid.cells2(i, 12).setDisabled(true);
		pgmGrid.cells2(i, 14).setDisabled(true);
	}
}
function doOnCheck(rowID, cellInd, state) {
	if (cellInd == 3) {
		var a = pgmGrid.cells(rowID, 3).getValue();
		if (state) {
			if (ROLE_TYPE == "1" || ROLE_TYPE == "3") {
				if (pgmGrid.cells(rowID, 5).getValue() == "1") {
					pgmGrid.cells(rowID, 4).setDisabled(false);
					pgmGrid.cells(rowID, 4).setValue(true);
				} else {
					pgmGrid.cells(rowID, 4).setDisabled(true);
				}
				if (pgmGrid.cells(rowID, 7).getValue() == "1") {
					pgmGrid.cells(rowID, 6).setDisabled(false);
					pgmGrid.cells(rowID, 6).setValue(true);
				} else {
					pgmGrid.cells(rowID, 6).setDisabled(true);
				}
				/*if (pgmGrid.cells(rowID, 15).getValue() == "1") {
					pgmGrid.cells(rowID, 14).setDisabled(false);
					pgmGrid.cells(rowID, 14).setValue(true);
				} else {
					pgmGrid.cells(rowID, 14).setDisabled(true);
				}*/
				pgmGrid.cells(rowID, 10).setDisabled(true);
				pgmGrid.cells(rowID, 12).setDisabled(true);
			}
			if (pgmGrid.cells(rowID, 9).getValue() == "1") {
				pgmGrid.cells(rowID, 8).setDisabled(false);
				pgmGrid.cells(rowID, 8).setValue(true);
			} else {
				pgmGrid.cells(rowID, 8).setDisabled(true);
			}
			if (ROLE_TYPE == "2" || ROLE_TYPE == "4") {
				if (pgmGrid.cells(rowID, 11).getValue() == "1") {
					pgmGrid.cells(rowID, 10).setDisabled(false);
					pgmGrid.cells(rowID, 10).setValue(true);
				} else {
					pgmGrid.cells(rowID, 10).setDisabled(true);
				}
				if (pgmGrid.cells(rowID, 13).getValue() == "1") {
					pgmGrid.cells(rowID, 12).setDisabled(false);
					pgmGrid.cells(rowID, 12).setValue(true);
				} else {
					pgmGrid.cells(rowID, 12).setDisabled(true);
				}
				pgmGrid.cells(rowID, 4).setDisabled(true);
				pgmGrid.cells(rowID, 6).setDisabled(true);
				pgmGrid.cells(rowID, 14).setDisabled(true);
			}

		} else {
			pgmGrid.cells(rowID, 4).setValue(false);
			pgmGrid.cells(rowID, 6).setValue(false);
			pgmGrid.cells(rowID, 8).setValue(false);
			pgmGrid.cells(rowID, 10).setValue(false);
			pgmGrid.cells(rowID, 12).setValue(false);
			pgmGrid.cells(rowID, 14).setValue(false);
			pgmGrid.cells(rowID, 4).setDisabled(true);
			pgmGrid.cells(rowID, 6).setDisabled(true);
			pgmGrid.cells(rowID, 8).setDisabled(true);
			pgmGrid.cells(rowID, 10).setDisabled(true);
			pgmGrid.cells(rowID, 12).setDisabled(true);
			pgmGrid.cells(rowID, 14).setDisabled(true);

		}
	}
}
function gridDisable() {
	total_rows = pgmGrid.getRowsNum();
	for (row = 0; row < total_rows; row++) {
		if (pgmGrid.cells2(row, 3).getValue() == "1") {
			if (ROLE_TYPE == "1" || ROLE_TYPE == "3") {
				if (pgmGrid.cells2(row, 5).getValue() == "1") {
					pgmGrid.cells2(row, 4).setDisabled(false);
				} else {
					pgmGrid.cells2(row, 4).setDisabled(true);
				}
				if (pgmGrid.cells2(row, 7).getValue() == "1") {
					pgmGrid.cells2(row, 6).setDisabled(false);
				} else {
					pgmGrid.cells2(row, 6).setDisabled(true);
				}
			/*	if (pgmGrid.cells2(row, 15).getValue() == "1") {
					pgmGrid.cells2(row, 14).setDisabled(false);
				} else {
					pgmGrid.cells2(row, 14).setDisabled(true);
				}*/
				pgmGrid.cells(rowID, 10).setDisabled(true);
				pgmGrid.cells(rowID, 12).setDisabled(true);
			}
			if (pgmGrid.cells2(row, 9).getValue() == "1") {
				pgmGrid.cells2(row, 8).setDisabled(false);
			} else {
				pgmGrid.cells2(row, 8).setDisabled(true);
			}
			if (ROLE_TYPE == "2" || ROLE_TYPE == "4") {
				if (pgmGrid.cells2(row, 11).getValue() == "1") {
					pgmGrid.cells2(row, 10).setDisabled(false);
				} else {
					pgmGrid.cells2(row, 10).setDisabled(true);
				}
				if (pgmGrid.cells2(row, 13).getValue() == "1") {
					pgmGrid.cells2(row, 12).setDisabled(false);
				} else {
					pgmGrid.cells2(row, 12).setDisabled(true);
				}
				pgmGrid.cells(rowID, 4).setDisabled(true);
				pgmGrid.cells(rowID, 6).setDisabled(true);
				pgmGrid.cells(rowID, 14).setDisabled(true);
			}

		} else {
			pgmGrid.cells2(row, 4).setValue(false);
			pgmGrid.cells2(row, 6).setValue(false);
			pgmGrid.cells2(row, 8).setValue(false);
			pgmGrid.cells2(row, 10).setValue(false);
			pgmGrid.cells2(row, 12).setValue(false);
			pgmGrid.cells2(row, 14).setValue(false);
			pgmGrid.cells2(row, 4).setDisabled(true);
			pgmGrid.cells2(row, 6).setDisabled(true);
			pgmGrid.cells2(row, 8).setDisabled(true);
			pgmGrid.cells2(row, 10).setDisabled(true);
			pgmGrid.cells2(row, 12).setDisabled(true);
			pgmGrid.cells2(row, 14).setDisabled(true);

		}
	}

}
function loadProgramsData() {
	loadGridQuery(CURRENT_PROGRAM_ID, 'PGM_GRID', postProcessing);
}
var resultData = null;
function postProcessing(result) {
	$xml = $($.parseXML(result));
	($xml).find("row").each(function() {
		try {
			var rowID = $(this).find("cell")[1].textContent;
			var aa = $(this).find("cell")[2].textContent;
			var ma = $(this).find("cell")[3].textContent;
			var va = $(this).find("cell")[4].textContent;
			var autha = $(this).find("cell")[5].textContent;
			var reja = $(this).find("cell")[6].textContent;
			var proa = $(this).find("cell")[7].textContent;
			if (rowID == (pgmGrid.cells(rowID, 1).getValue())) {
				pgmGrid.cells(rowID, 3).setValue(true);
				pgmGrid.cells(rowID, 4).setValue(stringToBoolean(aa));
				pgmGrid.cells(rowID, 6).setValue(stringToBoolean(ma));
				pgmGrid.cells(rowID, 8).setValue(stringToBoolean(va));
				pgmGrid.cells(rowID, 10).setValue(stringToBoolean(autha));
				pgmGrid.cells(rowID, 12).setValue(stringToBoolean(reja));
				pgmGrid.cells(rowID, 14).setValue(stringToBoolean(proa));
			} else {
				pgmGrid.deleteRow(rowID);
			}
		} catch (e) {
		}
	});
	gridDisable();
}

function stringToBoolean(val) {
	if (val == COLUMN_ENABLE)
		return true;
	return false;
}

function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'EROLEPGMALLOC_MAIN', queryGrid);
}

function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}

function doHelp(id, shiftmode) {
	switch (id) {
	case 'roleCode':
		if (!shiftmode) {
			help('COMMON', 'ROLES', $('#roleCode').val(), '', $('#roleCode'));
		} else {
			help('EROLEPGMALLOC', 'ROLES', $('#roleCode').val(), '',
					$('#roleCode'));
		}
		break;
	case 'moduleCode':
		help('EROLEPGMALLOC', 'MODULES', $('#moduleCode').val(), '',
				$('#moduleCode'));
		break;
	case 'effectiveDate':
		help('EROLEPGMALLOC', 'EFFECTIVEDATE', $('#effectiveDate').val(), $(
				'#roleCode').val()
				+ '|' + $('#moduleCode').val(), $('#effectiveDate'));
		break;
	}
}

function doclearfields(id) {
	switch (id) {
	case 'roleCode':
		if (isEmpty($('#roleCode').val())) {
			$('#roleCode_error').html(EMPTY_STRING);
			$('#roleCode_desc').html(EMPTY_STRING);
			clearNonPKFields();
			break;
		}
	case 'moduleCode':
		if (isEmpty($('#moduleCode').val())) {
			$('#moduleCode_error').html(EMPTY_STRING);
			$('#moduleCode_desc').html(EMPTY_STRING);
			pgmGrid.clearAll();
			break;
		}
	case 'effectiveDate':
		if (isEmpty($('#effectiveDate').val())) {
			$('#effectiveDate_error').html(EMPTY_STRING);
			clearNonPKFields();
			loadProgramsGrid();
			break;
		}
	}
}

function clearFields() {
	// $('#po_view1').addClass('hidden');
	$('#roleCode').val('');
	$('#roleCode_error').html('');
	$('#roleCode_desc').html('');
	$('#moduleCode').val('');
	$('#moduleCode_error').html('');
	$('#moduleCode_desc').html('');
	$('#effectiveDate').val('');
	$('#effectiveDate_error').html('');

	clearNonPKFields();
}
function clearNonPKFields() {
	$('#remarks').val('');
	$('#remarks_error').html('');
	pgmGrid.clearAll();

}
function add() {

	$('#roleCode').focus();
}
function modify() {

	$('#roleCode').focus();

}
function loadData() {
	$('#roleCode').val(validator.getValue('ROLE_CODE'));
	$('#roleCode_desc').html(validator.getValue('F1_DESCRIPTION'));
	$('#moduleCode').val(validator.getValue('MODULE_ID'));
	$('#moduleCode_desc').html(validator.getValue('F2_MODULE_NAME'));
	$('#effectiveDate').val(validator.getValue('EFFT_DATE'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadProgramsGrid();
	loadProgramsData();
	resetLoading();
}

function view(source, primaryKey) {
	hideParent('access/qrolepgmalloc', source, primaryKey);
	resetLoading();
}
function validate(id) {
	switch (id) {
	case 'roleCode':
		roleCode_val();
		break;
	case 'moduleCode':
		moduleCode_val();
		break;
	case 'fetch':
		loadProgramsGrid();
		break;
	case 'effectiveDate':
		effectiveDate_val();
		break;
	case 'remarks':
		remarks_val();
		break;
	}
}
function backtrack(id) {
	switch (id) {
	case 'roleCode':
		setFocusLast('roleCode');
		break;
	case 'moduleCode':
		setFocusLast('roleCode');
		break;
	case 'fetch':
		setFocusLast('moduleCode');
		break;
	case 'effectiveDate':
		setFocusLast('fetch');
		break;
	case 'remarks':
		setFocusLast('effectiveDate');
		break;
	}
}
function roleCode_val() {
	var value = $('#roleCode').val();
	clearError('roleCode_error');
	if (isEmpty(value)) {
		setError('roleCode_error', HMS_MANDATORY);
		setFocusLast('roleCode');
		return false;
	}
	if (!isValidCode(value)) {
		setError('roleCode_error', HMS_INVALID_FORMAT);
		setFocusLast('roleCode');
		return false;
	}
	if (!isEmpty(value)) {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('ROLE_CODE', $('#roleCode').val());
		validator.setValue(ACTION, $('#action').val());
		validator
				.setClass('patterns.config.web.forms.access.erolepgmallocbean');
		validator.setMethod('roleCodeValidate');
		validator.sendAndReceive();
		if (validator.getValue(RESULT) == DATA_AVAILABLE) {
			$('#roleCode_desc').html(validator.getValue('DESCRIPTION'));
		} else {
			setError('roleCode_error', validator.getValue(ERROR));
			setFocusLast('roleCode');
			return false;
		}
	}
	setFocusLast('moduleCode');
	return true;
}
function moduleCode_val() {
	var value = $('#moduleCode').val();
	clearError('moduleCode_error');
	pgmGrid.clearAll();
	if (isEmpty(value)) {
		setError('moduleCode_error', HMS_MANDATORY);
		setFocusLast('moduleCode');
		return false;
	}
	if (!isValidCode(value)) {
		setError('moduleCode_error', HMS_INVALID_FORMAT);
		setFocusLast('moduleCode');
		return false;
	}
	if (!isEmpty(value)) {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('MODULE_ID', $('#moduleCode').val());
		validator.setValue(ACTION, $('#action').val());
		validator
				.setClass('patterns.config.web.forms.access.erolepgmallocbean');
		validator.setMethod('moduleValidate');
		validator.sendAndReceive();
		if (validator.getValue(RESULT) == DATA_AVAILABLE) {
			$('#moduleCode_desc').html(validator.getValue('MODULE_NAME'));
		} else {
			setError('moduleCode_error', validator.getValue(ERROR));
			setFocusLast('moduleCode');
			return false;
		}
	}
	setFocusLast('fetch');
	return true;
}
function effectiveDate_val() {
	var value = $('#effectiveDate').val();
	clearError('effectiveDate_error');
	if (isEmpty(value)) {
		setError('effectiveDate_error', HMS_MANDATORY);
		setFocusLast('effectiveDate');
		return false;
	}
	if (!isValidEffectiveDate(value, 'effectiveDate_error')) {
		setFocusLast('effectiveDate');
		return false;
	}
	if (!isEmpty(value)) {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('ROLE_CODE', $('#roleCode').val());
		validator.setValue('MODULE_ID', $('#moduleCode').val());
		validator.setValue('EFFT_DATE', $('#effectiveDate').val());
		validator.setValue(ACTION, $('#action').val());
		validator
				.setClass('patterns.config.web.forms.access.erolepgmallocbean');
		validator.setMethod('validateEffectiveDate');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('effectiveDate_error', validator.getValue(ERROR));
			setFocusLast('effectiveDate');
			return false;
		}
	} else if ($('#action').val() == MODIFY) {
		PK_VALUE = getEntityCode() + PK_SEPERATOR + $('#roleCode').val()
				+ PK_SEPERATOR + $('#moduleCode').val() + PK_SEPERATOR
				+ getTBADateFormat($('#effectiveDate').val());
		if (!loadPKValues(PK_VALUE, 'effectiveDate_error')) {
			return false;
		}
		loadProgramsData();
		setFocusLast('remarks');
		return true;
	}
	setFocusLast('remarks');
	return true;
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', HMS_INVALID_REMARKS);
			setFocusLast('remarks');
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', HMS_INVALID_REMARKS);
			setFocusLast('remarks');
			return false;
		}
	}
	if ($('#action').val() == MODIFY) {
		if (isEmpty(value)) {
			setError('remarks_error', HMS_MANDATORY);
			setFocusLast('remarks');
			return false;
		}

	}
	setFocusOnSubmit();
	return true;

}

function revalidate() {
	$('#xmlPgmList').val(pgmGrid.serialize());
}