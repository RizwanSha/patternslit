<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.access.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/access/isyscpm.js" />
	</web:dependencies>
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="isyscpm.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/access/isyscpm" id="isyscpm" method="POST">
				<web:dividerBlock>
					<type:optionBox add="true" modify="true" view="true"  template="true"/>
				</web:dividerBlock>
				<web:viewContent id="po_view1" styleClass="hidden">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="280px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="form.effectivedate" var="common" mandatory="true" />
										</web:column>
										<web:column>
											<type:date property="effectiveDate" id="effectiveDate" lookup="true" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:viewTitle var="program" key="isyscpm.section1" />
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="280px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="isyscpm.minlengthuid" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:twoDnumber property="minLengthUID" id="minLengthUID" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="isyscpm.minlengthpwd" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:twoDnumber property="minLengthPwd" id="minLengthPwd" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="isyscpm.minnumpwd" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:twoDnumber property="minNumPwd" id="minNumPwd" />
										</web:column>
									</web:rowOdd>
									<web:rowOdd>
										<web:column span="2">
											<web:usermessage key="isyscpm.info2" var="program" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="isyscpm.minalphapwd" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:twoDnumber property="minAlphaPwd" id="minAlphaPwd" />
										</web:column>
									</web:rowEven>
									<web:rowEven>
										<web:column span="2">
											<web:usermessage key="isyscpm.info3" var="program" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="isyscpm.minscpwd" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:twoDnumber property="minSCPwd" id="minSCPwd" />
										</web:column>
									</web:rowOdd>
									<web:rowOdd>
										<web:column span="2">
											<web:usermessage key="isyscpm.info4" var="program" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:viewTitle var="program" key="isyscpm.section2" />
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="280px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="isyscpm.forcepwdchgn" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:fourDnumber property="forcePwdChgn" id="forcePwdChgn" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="isyscpm.prevpwdcnt" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:twoDnumber property="prevPwdCnt" id="prevPwdCnt" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="isyscpm.uwcnt" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:twoDnumber property="uwCnt" id="uwCnt" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="isyscpm.sawcnt" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:twoDnumber property="sawCnt" id="sawCnt" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="isyscpm.lockuiduacnt" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:twoDnumber property="lockUIDUACnt" id="lockUIDUACnt" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="isyscpm.lockuidnucnt" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:fourDnumber property="lockUIDNUCnt" id="lockUIDNUCnt" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="isyscpm.lockuiduocnt" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:fourDnumber property="lockUIDUOCnt" id="lockUIDUOCnt" />
										</web:column>
									</web:rowEven>
									<%--
									<web:rowOdd>
										<web:column>
											<web:legend key="isyscpm.uidexpperiod" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:fourDnumber property="UIDExpPeriod" id="UIDExpPeriod" />
										</web:column>
									</web:rowOdd>
									 --%>
									<web:rowOdd>
										<web:column>
											<web:legend key="isyscpm.pwdexpperiod" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:fourDnumber property="pwdExpPeriod" id="pwdExpPeriod" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:viewTitle var="program" key="isyscpm.section3" />
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="280px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="isyscpm.totalleaseperiod" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:fourDnumber property="totalLeasePeriod" id="totalLeasePeriod" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="isyscpm.autologoutperiod" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:fourDnumber property="autoLogoutPeriod" id="autoLogoutPeriod" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="isyscpm.multiplesessionallowed" var="program" />
										</web:column>
										<web:column>
											<type:checkbox property="multipleSessionAllowed" id="multipleSessionAllowed" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="isyscpm.iprestrictusers" var="program" />
										</web:column>
										<web:column>
											<type:checkbox property="ipRestrictUsers" id="ipRestrictUsers" />
										</web:column>
									</web:rowOdd>

									<web:rowEven>
										<web:column>
											<web:legend key="isyscpm.actroleallowed" var="program" />
										</web:column>
										<web:column>
											<type:checkbox property="actRoleAllowed" id="actRoleAllowed" />
										</web:column>
									</web:rowEven>

									<web:rowOdd>
										<web:column>
											<web:legend key="isyscpm.adminactivation" var="program" />
										</web:column>
										<web:column>
											<type:checkbox property="adminActivation" id="adminActivation" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="isyscpm.pwddelmechanism" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:combo property="pwdDelMechanism" id="pwdDelMechanism" datasourceid="COMMON_PWDDELIVERY" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="isyscpm.pwdmanualtype" var="program" />
										</web:column>
										<web:column>
											<type:combo property="pwdManualType" id="pwdManualType" datasourceid="COMMON_PWDMANUALTYPE" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="form.remarks" var="common" />
										</web:column>
										<web:column>
											<type:remarks property="remarks" id="remarks" />
										</web:column>
									</web:rowEven>
									<web:rowEven>
										<web:column span="2">
											<web:submitReset />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
						</web:sectionBlock>
					</web:dividerBlock>
				</web:viewContent>
				<web:element property="command" />
				<web:element property="action" />
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
	<web:queryGrid />
	<web:TBAGrid />
	<web:contextSearch/>
</web:fragment>

