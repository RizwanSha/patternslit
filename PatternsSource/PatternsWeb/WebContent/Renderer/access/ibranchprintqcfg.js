var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'IBRANCHPRINTQCFG';
function init() {
	refreshQueryGrid();
	refreshTBAGrid();
	if (_redisplay) {
		branchPrintqGrid.clearAll();
		branchPrintqGrid.loadXMLString($('#xmlBrnPrntGrid').val());
	}
}

function loadGrid() {
	loadGridQuery(CURRENT_PROGRAM_ID, 'IBRANCHPRINTQCFG_GRID', branchPrintqGrid);

}
function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'IBRANCHPRINTQCFG_MAIN', queryGrid);
}
function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}

function doHelp(id) {
	switch (id) {
	case 'branchCode':
		help('COMMON', 'HLP_ENTITYBRN_M', $('#branchCode').val(), EMPTY_STRING, $('#branchCode'));
		break;
	case 'effectiveDate':
		help(CURRENT_PROGRAM_ID, 'HLP_EFFT_DATE', $('#effectiveDate').val(), $('#branchCode').val(), $('#effectiveDate'));
		break;
	case 'printType':
		help('COMMON', 'HLP_PRNTYPE_M', $('#printType').val(), EMPTY_STRING, $('#printType'));
		break;
	case 'printQueue':
		help('COMMON', 'HLP_PRINTQUEUE_M', $('#printQueue').val(), $('#branchCode').val(), $('#printQueue'));
		break;
	}
}

function doclearfields(id) {
	switch (id) {
	case 'branchCode':
		if (isEmpty($('#branchCode').val())) {
			$('#branchCode_error').html(EMPTY_STRING);
			$('#branchCode_desc').html(EMPTY_STRING);
			break;
		}
		
	case 'effectiveDate':
		if (isEmpty($('#effectiveDate').val())) {
			$('#effectiveDate_error').html(EMPTY_STRING);
			clearNonPKFields();
			break;
		}
	}
}

function clearFields() {
	$('#branchCode').val(EMPTY_STRING);
	$('#branchCode_error').html(EMPTY_STRING);
	$('#branchCode_desc').html(EMPTY_STRING);
	$('#effectiveDate').val(EMPTY_STRING);
	$('#effectiveDate_error').html(EMPTY_STRING);
	clearNonPKFields();
}

function clearNonPKFields() {
	$('#printType').val(EMPTY_STRING);
	$('#printType_error').html(EMPTY_STRING);
	$('#printType_desc').html(EMPTY_STRING);
	$('#printQueue').val(EMPTY_STRING);
	$('#printQueue_error').html(EMPTY_STRING);
	$('#printQueue_desc').html(EMPTY_STRING);
	branchPrintqGrid.clearAll();
}
function add() {
	$('#branchCode').focus();
}
function modify() {
	$('#branchCode').focus();
}

function loadData() {
	$('#branchCode').val(validator.getValue('BRN_CODE'));
	$('#branchCode_desc').html(validator.getValue('F1_DESCRIPTION'));
	$('#effectiveDate').val(validator.getValue('EFFT_DATE'));
	loadGrid();
	resetLoading();
}
function view(source, primaryKey) {
	hideParent('access/qbranchprintqcfg', source, primaryKey);
	resetLoading();
}

function validate(id) {
	valMode = true;
	switch (id) {
	case 'branchCode':
		branchCode_val(valMode);
		break;
	case 'effectiveDate':
		effectiveDate_val(valMode);
		break;
	case 'printType':
		printType_val(valMode);
		break;
	case 'printQueue':
		printQueue_val(valMode);
		break;
	}
}

function backtrack(id) {
	switch (id) {
	case 'branchCode':
		setFocusLast('branchCode');
		break;
	case 'effectiveDate':
		setFocusLast('branchCode');
		break;
	case 'printType':
		setFocusLast('effectiveDate');
		break;
	case 'printQueue':
		setFocusLast('printType');
		break;
	}
}

function gridExit(id) {
	switch (id) {
	case 'printType':
		$('#xmlBrnPrntGrid').val(branchPrintqGrid.serialize());
		setFocusOnSubmit();
		break;
	}
}

function gridDeleteRow(id) {
	switch (id) {
	case 'printType':
		deleteGridRecord(branchPrintqGrid);
		break;
	}
}

function branchCode_val(valMode) {
	var value = $('#branchCode').val();
	clearError('branchCode_error');
	if (isEmpty(value)) {
		setError('branchCode_error', MANDATORY);
		setFocusLast('branchCode');
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('BRANCH_CODE', $('#branchCode').val());
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.access.ibranchprintqcfgbean');
		validator.setMethod('validateBranchCode');
		validator.sendAndReceive();
		if (validator.getValue(RESULT) == DATA_AVAILABLE) {
			$('#branchCode_desc').html(validator.getValue('DESCRIPTION'));
		} else {
			setError('branchCode_error', validator.getValue(ERROR));
			setFocusLast('branchCode');
			return false;
		}
	}
	setFocusLast('effectiveDate');
	return true;
}

function effectiveDate_val(valMode) {
	var value = $('#effectiveDate').val();
	clearError('effectiveDate_error');
	if (!isValidEffectiveDate(value, 'effectiveDate_error')) {
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('BRN_CODE', $('#branchCode').val());
		validator.setValue('EFFT_DATE', $('#effectiveDate').val());
		validator.setValue(ACTION, $('#action').val());
		validator.setClass('patterns.config.web.forms.access.ibranchprintqcfgbean');
		validator.setMethod('validateEffectiveDate');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('effectiveDate_error', validator.getValue(ERROR));
			setFocusLast('effectiveDate');
			return false;
		} else {
			if ($('#action').val() == MODIFY) {
				PK_VALUE = getEntityCode() + PK_SEPERATOR + $('#branchCode').val() + PK_SEPERATOR + $('#effectiveDate').val();
				if(!loadPKValues(PK_VALUE, 'effectiveDate_error')){
					return false;
				}
				setFocusLast('printType');
				return true;
			}
		}
	}
	setFocusLast('printType');
	return true;
}

function printType_val(valMode) {
	var value = $('#printType').val();
	clearError('printType_error');
	if (isEmpty(value)) {
		setError('printType_error', MANDATORY);
		setFocusLast('printType');
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('PRNTYPE_ID', value);
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.access.ibranchprintqcfgbean');
		validator.setMethod('validatePrintTypeID');
		validator.sendAndReceive();
		if (validator.getValue(RESULT) == DATA_AVAILABLE) {
			$('#printType_desc').html(validator.getValue('DESCRIPTION'));
		} else {
			setError('printType_error', validator.getValue(ERROR));
			setFocusLast('printType');
			return false;
		}
	}
	if (valMode) {
		setFocusLast('printQueue');
	}
	return true;
}

function printQueue_val(valMode) {
	var value = $('#printQueue').val();
	clearError('printQueue_error');
	if (isEmpty(value)) {
		setError('printQueue_error', MANDATORY);
		setFocusLast('printQueue');
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('BRANCH_CODE', $('#branchCode').val());
		validator.setValue('PRNQ_ID', value);
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.access.ibranchprintqcfgbean');
		validator.setMethod('validatePrintQueue');
		validator.sendAndReceive();
		if (validator.getValue(RESULT) == DATA_AVAILABLE) {
			$('#printQueue_desc').html(validator.getValue('DESCRIPTION'));
		} else {
			setError('printQueue_error', validator.getValue(ERROR));
			$('#printQueue_desc').html(EMPTY_STRING);
			setFocusLast('printQueue');
			return false;
		}
	}
	if (valMode) {
		setFocusLastOnGridSubmit('branchPrintqGrid');
	}
	return true;
}

function  branchPrintq_gridDuplicateCheck(editMode, editRowId) {
	var currentValue = [ $('#printType').val() ];
	return _grid_duplicate_check(branchPrintqGrid, currentValue, [ 1 ]);
}

function branchPrintq_addGrid(editMode, editRowId) {
	if ((printType_val(false)) && (printQueue_val(false))) {
		if (branchPrintq_gridDuplicateCheck(editMode, editRowId)) {
			var field_values = [ 'false', $('#printType').val(), $('#printType_desc').html(), $('#printQueue').val(), $('#printQueue_desc').html() ];
			_grid_updateRow(branchPrintqGrid, field_values);
			branchPrintq_clearGridFields();
			$('#printType').focus();
			return true;
		} else {
			setError('printType_error', HMS_DUPLICATE_DATA);
			$('#printType').focus();
			return false;
		}
	} else {
		$('#printType').focus();
		return false;
	}
}

function branchPrintq_editGrid(rowId) {

	$('#printType').val(branchPrintqGrid.cells(rowId, 1).getValue());
	$('#printType_desc').html(branchPrintqGrid.cells(rowId, 2).getValue());
	$('#printQueue').val(branchPrintqGrid.cells(rowId, 3).getValue());
	$('#printQueue_desc').html(branchPrintqGrid.cells(rowId, 4).getValue());
	$('#printType').focus();
}

function branchPrintq_cancelGrid(rowId) {
	branchPrintq_clearGridFields();
}

function branchPrintq_clearGridFields() {
	$('#printType').val(EMPTY_STRING);
	clearError('printType_error');
	$('#printType_desc').html(EMPTY_STRING);
	$('#printQueue').val(EMPTY_STRING);
	clearError('printQueue_error');
	$('#printQueue_desc').html(EMPTY_STRING);
}

function revalidate() {

	branchPrintqGrid_revaildateGrid();
}

function branchPrintqGrid_revaildateGrid() {
	if (branchPrintqGrid.getRowsNum() > 0) {
		$('#xmlBrnPrntGrid').val(branchPrintqGrid.serialize());
		$('#printType_error').html(EMPTY_STRING);
	} else {
		branchPrintqGrid.clearAll();
		$('#xmlBrnPrntGrid').val(branchPrintqGrid.serialize());
		setError('printType_error', HMS_ADD_ATLEAST_ONE_ROW);
		errors++;
	}
}
