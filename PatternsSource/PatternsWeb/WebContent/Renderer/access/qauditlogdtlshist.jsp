<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<%@taglib prefix="message" tagdir="/WEB-INF/tags/message"%>
<web:bundle baseName="patterns.config.web.forms.access.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/access/qauditlogdtlshist.js" />
	</web:dependencies>
	<web:loadPK />
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="qauditlogdtlshist.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/access/qauditlogdtlshist" id="qauditlogdtlshist" method="POST">
				<web:viewContent id="po_view1">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="form.optionid" var="common" />
										</web:column>
										<web:column>
											<type:optionIDDisplay property="optionID" id="optionID" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle width="10px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="qauditlogdtlshist.primarykey" var="program" />
										</web:column>
										<web:column>
											<type:optionIDDisplay property="primaryKey" id="primaryKey" />
										</web:column>
										<web:column>
											<type:button id="viewProgram" key="form.viewprogram" var="common" onclick="viewProgramdtl();" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle width="180px" />
										<web:columnStyle width="130px" />
										<web:columnStyle width="210px" />
										<web:columnStyle width="150px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="qauditlogdtlshist.actiontype" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:combo property="actionType" id="actionType" datasourceid="COMMON_QAUDITLOG" />
										</web:column>
										<web:column>
											<web:legend key="qauditlogdtlshist.viewType" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:combo property="viewType" id="viewType" datasourceid="COMMON_VIEW_TYPE" />
										</web:column>
										<web:column>
											<web:legend key="qauditlogdtlshist.fromTable" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:combo property="fromTable" id="fromTable" datasourceid="QAUDITLOGDTLSHIST_SERIAL" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:viewContent id="po_view7" styleClass="hidden">
								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="180px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowOdd>
											<web:column>
												<web:legend key="qauditlogdtlshist.detailTableSerial" var="program" mandatory="true" />
											</web:column>
											<web:column>
												<type:combo property="detailTableSerial" id="detailTableSerial" datasourceid="QAUDITLOGDTLSHIST_DTL_SERIAL" />
											</web:column>
										</web:rowOdd>
									</web:table>
								</web:section>
							</web:viewContent>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<tr id="buttonId" class="row_odd" height="26">
										<web:column span="3">
											<type:button id="submit" key="form.submit" var="common" onclick="onSubmit();" />
											<type:button id="reset" key="form.reset" var="common" onclick="clearFields();" />
											<type:button key="form.cancel" id="cancel" var="common" onclick="closePopup('0')" />
											<span id="totalrows" style="color: blue; font-weight: bold;"></span>
										</web:column>
									</tr>
								</web:table>
							</web:section>
							<web:section>
								<message:messageBox id="qauditlogdtlshist" />
							</web:section>
							<web:viewContent id="po_view8" styleClass="hidden">
								<web:viewTitle var="program" key="qauditlogdtlshist.oldimagetitle" />
								<table>
									<tr>
										<td style="padding: 4px">
											<div id="auditTable_div" style="width: 1230px; height: 250px"></div>
										</td>
									</tr>
								</table>
							</web:viewContent>
							<web:viewTitle var="program" key="qauditlogdtlshist.newimagetitle" />
							<table>
								<tr>
									<td style="padding: 4px">
										<div id="auditTableDetails_div" style="width: 1230px; height: 280px"></div>
									</td>
								</tr>
							</table>
						</web:sectionBlock>
					</web:dividerBlock>
				</web:viewContent>
				<web:element property="hidPrimaryKey" />
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
	<web:contextSearch />
</web:fragment>