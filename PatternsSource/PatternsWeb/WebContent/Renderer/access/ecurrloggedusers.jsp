<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.access.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/access/ecurrloggedusers.js" />
	</web:dependencies>
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="ecurrloggedusers.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/access/ecurrloggedusers" id="ecurrloggedusers" method="POST">
				<web:viewContent id="po_view1">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column span="2">
											<type:button id="fetch" key="form.fetch" var="common" onclick="loadLoginUsersGrid();" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="form.currentloggedusercount" var="common" />
										</web:column>
										<web:column>
											<type:inputCodeDisplay property="currentLoggedUserCount" id="currentLoggedUserCount" />
											<type:error property="userIDError" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:viewTitle var="program" key="ecurrloggedusers.section1" />
								<web:table>
									<web:rowEven>
										<web:column span="2">
											<web:viewContent id="po_view4">
												<web:grid height="240px" width="850px" id="loggedusersGrid" src="access/ecurrloggedusers_Grid.xml">
												</web:grid>
											</web:viewContent>
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column span="2">
											<type:button id="submit" key="form.logoutuser" var="common" onclick="revalidate();" />
											<type:reset key="form.reset" id="reset" var="common" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
						</web:sectionBlock>
					</web:dividerBlock>
				</web:viewContent>
				<web:element property="command" />
				<web:element property="action" />
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
</web:fragment>