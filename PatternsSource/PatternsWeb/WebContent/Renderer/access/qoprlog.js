var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'QOPRLOG'; 
function init() {
	$('#userID').focus();
}
/*function loadOprLogGrid(){
	$('#po_view4').removeClass('hidden');
	var fromDate = $('#fromDate').val();
	var uptoDate = $('#uptoDate').val();
	if(isEmpty($('#optionID').val())){
		var _args=$('#userID').val()+'|'+fromDate+'|'+ uptoDate ;
		inlineGridSuccessful = loadInlineQueryGrid(CURRENT_PROGRAM_ID,'QOPRLOG_O_MAIN','inlineGridSucc',_args);
	}else{
		var _args=$('#userID').val()+'|'+$('#optionID').val()+'|'+fromDate+'|'+ uptoDate ;
		inlineGridSuccessful = loadInlineQueryGrid(CURRENT_PROGRAM_ID,'QOPRLOG_OU_MAIN','inlineGridSucc',_args);
	}
}*/
function clearFields(){
	$('#userID').val(EMPTY_STRING);
	$('#userID_desc').html(EMPTY_STRING);
	$('#userID_error').html(EMPTY_STRING);
	$('#optionID').val(EMPTY_STRING);
	$('#optionID_desc').html(EMPTY_STRING);
	$('#optionID_error').html(EMPTY_STRING);
	$('#fromDate').val(EMPTY_STRING);
	$('#fromDate_error').html(EMPTY_STRING);
	$('#uptoDate').val(EMPTY_STRING);
	$('#uptoDate_error').html(EMPTY_STRING);

}
function doHelp(id){
	switch(id){
	case 'userID':
		help('QOPRLOG', 'USERS', $('#userID').val(), EMPTY_STRING, $('#userID'));
		break;
	case 'optionID':
		help('QOPRLOG', 'OPTIONS', $('#optionID').val(), EMPTY_STRING, $('#optionID'));
	break;
	}
}
function validate(id){
	switch(id){
	case 'userID':
		userID_val();
		break;
	case 'optionID':
		optionID_val();
		break;
	case 'fromDate':
		fromDate_val();
		break;
	case 'uptoDate':
		uptoDate_val();
		break;
	}
}
function userID_val(){
	var value = $('#userID').val();
	clearError('userID_error');
	if (isEmpty(value)) {
		setError('userID_error', MANDATORY);
		return false;
	}	else{
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('USER_ID', $('#userID').val());
		validator.setClass('patterns.config.web.forms.access.qoprlogbean');
		validator.setMethod('validateUserID');
		validator.sendAndReceive();
		if (validator.getValue(RESULT) == DATA_AVAILABLE) {
			return true;
		} else {
			setError('userID_error', validator.getValue(ERROR));
			return false;
		} 
	}
	return true;
}
function optionID_val(){
	var value = $('#optionID').val();
	var userid = $('#userID').val();
	clearError('optionID_error');
	if(userid!=EMPTY_STRING){
	    if(value!=EMPTY_STRING){
	       if (isEmpty(value)) {
		   setError('optionID_error', MANDATORY);
		   return false;
	       }
	    	validator.clearMap();
	    	validator.setMtm(false);
	    	validator.setValue('MPGM_ID', $('#optionID').val());
	    	validator.setClass('patterns.config.web.forms.access.qoprlogbean');
	    	validator.setMethod('validateOptionID');
	    	validator.sendAndReceive();
	    	if (validator.getValue(RESULT) == DATA_AVAILABLE) {	
	    		return true;
	    	} else {
	    		setError('optionID_error', validator.getValue(ERROR));
	    		return false;
	    	} 
		}
		return true;
	}
	setError('userID_error', MANDATORY);
	return false;
}

function fromDate_val(){
	var value = $('#fromDate').val();
	clearError('fromDate_error');
	if (isEmpty(value)) {
		setError('fromDate_error', MANDATORY);
		return false;
	}
	if (!isDate(value)) {
		setError('fromDate_error', INVALID_DATE);
		return false;
	}
	if(!isDateLesserEqual(value,getCBD())){
		setError('fromDate_error',DATE_LECBD);
		return false;
	}
	return true;
}
function uptoDate_val(){
	var value = $('#uptoDate').val();
	var fromDate = $('#fromDate').val();
	clearError('uptoDate_error');
	if (isEmpty(value)) {
		setError('uptoDate_error', MANDATORY);
		return false;
	}
	if (!isDate(value)) {
		setError('uptoDate_error', INVALID_DATE);
		return false;
	}
	if(!isDateLesserEqual(value,getCBD())){
		setError('uptoDate_error',DATE_LECBD);
		return false;
	}
	if(!isDateGreaterEqual(value,fromDate)){
		setError('uptoDate_error',DATE_GEFD);
		return false;
	}
	return true;
}
function doDownload() {
	if(revalidate()){
		disableElement('cmdDownload');
		showMessage(getProgramID(), Message.PROGRESS);
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('USER_ID', $('#userID').val());
		validator.setValue('OPTION_ID', $('#optionID').val());
		validator.setValue('FROM_DATE', $('#fromDate').val());
		validator.setValue('UPTO_DATE', $('#uptoDate').val());
		validator.setValue('REPORT_TYPE',"ROPRLOG");
		validator.setClass('patterns.config.web.forms.access.qoprlogbean');
		validator.setMethod('getOperationLog');
		validator.sendAndReceiveAsync(processDownloadResult);
	}
}

function processDownloadResult(){
	enableElement('cmdDownload');
	if(validator.getValue(RESULT) == DATA_AVAILABLE){
		showReportLink(validator);
	}else{
		showMessage(getProgramID(), Message.ERROR, validator.getValue(ERROR));
	}
}
function revalidate(){
	if(!userID_val()){
		return false;
	}
	if(!optionID_val()){
		return false;
	}
	if(!fromDate_val()){
		return false;
	}
	if(!uptoDate_val()){
		return false;
	}	
	return true;
}