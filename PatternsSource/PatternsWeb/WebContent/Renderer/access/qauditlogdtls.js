var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'QAUDITLOGDTLS';

function init() {
	validator.reset();
	validator.setValue("PK", $('#hidPrimaryKey').val());
	validator.setClass("patterns.config.web.forms.access.qauditlogdtlsbean");
	validator.setMethod("getAuditLogDetails");
	validator.sendAndReceive();
	if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		$('#optionID').val(validator.getValue('OPTION_ID'));
		$('#optionID_desc').html(validator.getValue('OPTION_DESCN'));
		$('#primaryKey').val(validator.getValue('LOG_PK'));
		$('#logDateTime').val(validator.getValue('LOG_DATETIME'));
		$('#logSerial').val(validator.getValue('LOG_SL'));
		$('#auction').val(validator.getValue('ACTION'));
		var newImageXml = validator.getValue('NEW_IMAGE_XML');
		var oldImageXml = validator.getValue('OLD_IMAGE_XML');
		$('#newImageGrid').html(validator.getValue('NEW_IMAGE_XML'));
		$('#oldImageGrid').html(validator.getValue('OLD_IMAGE_XML'));
		$('#rectifyImageGrid').html(validator.getValue('RECTIFY_IMAGE_XML'));
	}
}

function clearFields() {
	$('#hidPrimaryKey').val(EMPTY_STRING);
	$('#optionID').val(EMPTY_STRING);
	$('#optionID_desc').html(EMPTY_STRING);
	$('#primaryKey').val(EMPTY_STRING);
	$('#logDateTime').val(EMPTY_STRING);
	$('#logSerial').val(EMPTY_STRING);
	$('#auction').val(EMPTY_STRING);
	$('#office').val(EMPTY_STRING);
	$('#newImageGrid').html(EMPTY_STRING);
	$('#oldImageGrid').html(EMPTY_STRING);
	$('#rectifyImageGrid').html(EMPTY_STRING);
}
