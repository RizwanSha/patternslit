var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MSMSINTF';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}
function clearFields() {
	$('#smsCode').val(EMPTY_STRING);
	$('#description').val(EMPTY_STRING);
	$('#sendAllowed').prop('checked',false);
	$('#receiveAllowed').prop('checked',false);
	$('#enabled').prop('checked',false);
	$('#remarks').val(EMPTY_STRING);
}

function loadData() {
	$('#smsCode').val(validator.getValue('SMS_CODE'));
	$('#description').val(validator.getValue('SMS_DESCN'));
	setCheckbox('sendAllowed', validator.getValue('SMS_SEND_ALLOWED'));
	setCheckbox('receiveAllowed', validator.getValue('SMS_RECV_ALLOWED'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadAuditFields(validator);
}