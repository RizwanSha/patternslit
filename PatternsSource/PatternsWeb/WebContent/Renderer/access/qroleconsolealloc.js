var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'EROLECONSOLEALLOC';
function init() {
	clearFields();
	loadProgramsGrid();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}
function loadProgramsGrid() {
	innerGrid.clearAll();
	validator.clearMap();
	validator.setMtm(false);
	validator
			.setClass('patterns.config.web.forms.access.eroleconsoleallocbean');
	validator.setMethod('loadConsoleGrid');
	validator.sendAndReceive();
	if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		xmlString = validator.getValue(RESULT_XML);
		innerGrid.loadXMLString(xmlString);
	}
}
function loadProgramsGridInMod() {
	loadGridQuery(CURRENT_PROGRAM_ID, 'EROLECONSOLEALLOC_GRID', postProcessing);
}

function postProcessing(result) {
	$xml = $($.parseXML(result));
	($xml).find("row").each(function() {
		var rowID = $(this).find("cell")[2].textContent;
		innerGrid.cells(rowID, 3).setValue(true);
	});
}

function loadData() {
	$('#roleType').val(validator.getValue('ROLE_TYPE'));
	$('#effectiveDate').val(validator.getValue('EFFT_DATE'));
	$('#remarks').val(validator.getValue('REMARKS'));
	SOURCE_VALUE = _tableType;
	PK_VALUE = _primaryKey;
	if (_tableType == MAIN) {
		loadProgramsGridInMod();
	} else if (_tableType == TBA) {
		loadProgramsGridInMod();
	}
	loadAuditFields(validator);
}
function clearFields() {
	$('#roleType').val(EMPTY_STRING);
	$('#effectiveDate').val(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
}