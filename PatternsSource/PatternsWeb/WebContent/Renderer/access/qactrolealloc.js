var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'EACTROLEALLOC';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}
function clearFields() {
	$('#userID').val(EMPTY_STRING);
	$('#roleCode').val(EMPTY_STRING);
	$('#fromDate').val(EMPTY_STRING);
	$('#toDate').val(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	
}
function loadData() {
	$('#userID').val(validator.getValue('USER_ID'));
	$('#userID_desc').html(validator.getValue('F1_USER_NAME'));
	$('#roleCode').val(validator.getValue('ROLE_CODE'));
	$('#roleCode_desc').html(validator.getValue('F2_DESCRIPTION'));
	$('#enabled').prop('checked',decodeSTB(validator.getValue('ENABLED')));
	if ($('#enabled').is(':checked')) {
		$('#fromDate').val(validator.getValue('FROM_DATE'));
		$('#toDate').val(validator.getValue('UPTO_DATE'));
	}
	$('#remarks').val(validator.getValue('REMARKS'));
	loadAuditFields(validator);
}
