var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'EROLECONSOLEALLOC';
var resultData = null;
function init() {
	refreshQueryGrid();
	refreshTBAGrid();
	if (_redisplay) {
		innerGrid.loadXMLString($('#xmlConsoleAllocGrid').val());
	}
}

function loadProgramsGrid() {
	innerGrid.clearAll();
	validator.reset();
	validator.clearMap();
	validator.setMtm(false);
	validator.setClass('patterns.config.web.forms.access.eroleconsoleallocbean');
	validator.setMethod('loadConsoleGrid');
	validator.sendAndReceive();
	if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		xmlString = validator.getValue(RESULT_XML);
		innerGrid.loadXMLString(xmlString);
	}
}

function loadProgramsGridInMod() {
	loadGridQuery(CURRENT_PROGRAM_ID, 'EROLECONSOLEALLOC_GRID', postProcessing);
}

function postProcessing(result) {
	$xml = $($.parseXML(result));
	($xml).find("row").each(function() {
		var rowID = $(this).find("cell")[2].textContent;
		innerGrid.cells(rowID, 3).setValue(true);
	});

}
function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'EROLECONSOLEALLOC_MAIN', queryGrid);
}
function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}

function doclearfields(id) {
	switch (id) {
	case 'roleType':
		if (isEmpty($('#roleType').val())) {
			$('#roleType_error').html(EMPTY_STRING);
			break;
		}
	case 'effectiveDate':
		if (isEmpty($('#effectiveDate').val())) {
			$('#effectiveDate_error').html(EMPTY_STRING);
			clearNonPKFields();
			innerGrid.clearAll();
			loadProgramsGrid();
			break;
		}
	}
}
function clearFields() {

	$('#roleType').val(EMPTY_STRING);
	$('#roleType_error').html(EMPTY_STRING);
	$('#effectiveDate').val(EMPTY_STRING);
	$('#effectiveDate_error').html(EMPTY_STRING);

	clearNonPKFields();
}

function clearNonPKFields() {
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
}
function add() {
	$('#roleType').focus();
	loadProgramsGrid();
}
function modify() {
	$('#roleType').focus();
	loadProgramsGrid();
}
function loadData() {
	$('#roleType').val(validator.getValue('ROLE_TYPE'));
	$('#effectiveDate').val(validator.getValue('EFFT_DATE'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadProgramsGrid();
	loadProgramsGridInMod();
	resetLoading();
}
function view(source, primaryKey) {
	hideParent('access/qroleconsolealloc', source, primaryKey);
	resetLoading();
}

function doHelp(id) {
	switch (id) {
	case 'effectiveDate':
		help(CURRENT_PROGRAM_ID, 'ROLE_EFF_DATE', $('#effectiveDate').val(), $('#roleType').val(), $('#effectiveDate'));
		break;
	}
}
function validate(id) {
	switch (id) {
	case 'roleType':
		roleType_val();
		break;
	case 'effectiveDate':
		effectiveDate_val();
		break;
	case 'remarks':
		remarks_val();
		break;
	}
}

function backtrack(id) {
	switch (id) {
	case 'roleType':
		setFocusLast('roleType');
		break;
	case 'effectiveDate':
		setFocusLast('roleType');
		break;
	case 'remarks':
		setFocusLast('effectiveDate');
		break;
	}
}
function roleType_val() {
	var value = $('#roleType').val();
	clearError('roleType_error');
	if (isEmpty(value)) {
		setError('roleType_error', MANDATORY);
		setFocusLast('roleType');
		return false;
	}
	setFocusLast('effectiveDate');
	return true;
}
function effectiveDate_val() {
	var value = $('#effectiveDate').val();
	clearError('effectiveDate_error');
	if(value==EMPTY_STRING){
		value=getCBD();
		$('#effectiveDate').val(value);
	}
	if (!isValidEffectiveDate(value, 'effectiveDate_error')) {
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('ROLE_TYPE', $('#roleType').val());
		validator.setValue('EFFT_DATE', $('#effectiveDate').val());
		validator.setValue(ACTION, $('#action').val());
		validator.setClass('patterns.config.web.forms.access.eroleconsoleallocbean');
		validator.setMethod('validateEffectiveDate');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('effectiveDate_error', validator.getValue(ERROR));
			setFocusLast('effectiveDate');
			return false;
		} else {
			if ($('#action').val() == MODIFY) {
				PK_VALUE = getEntityCode() + PK_SEPERATOR + $('#roleType').val() + PK_SEPERATOR + $('#effectiveDate').val();
				if (!loadPKValues(PK_VALUE, 'effectiveDate_error')) {
					return false;
				}
				setFocusLast('remarks');
				return true;
			}
		}
	}
	setFocusLast('remarks');
	return true;
}

function EnbChk() {
	var row;
	var gridvalue;
	var flag = 0;
	var total_rows = innerGrid.getRowsNum();
	for (row = 0; row < total_rows; row++) {
		gridvalue = innerGrid.cells2(row, 3).getValue();
		if (gridvalue != 1) {
			flag++;
		} else {
			flag = 0;
			break;
		}
	}
	if (flag != 0) {
		setError('effectiveDate_error', CHECK_ATLEAST_ONE);
		return false;
	}
	return true;
}
function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', HMS_INVALID_REMARKS);
			setFocusLast('remarks');
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', HMS_INVALID_REMARKS);
			setFocusLast('remarks');
			return false;
		}
	}
	if ($('#action').val() == MODIFY) {
		if (isEmpty(value)) {
			setError('remarks_error', HMS_MANDATORY);
			setFocusLast('remarks');
			return false;
		}

	}
	setFocusOnSubmit();
	return true;
}

function revalidate() {
	if (innerGrid.getRowsNum() > 0) {
		$('#xmlConsoleAllocGrid').val(innerGrid.serialize());
		setError('effectiveDate_error', EMPTY_STRING);
	} else {
		setError('effectiveDate_error', ADD_ATLEAST_ONE_ROW);
	}
}