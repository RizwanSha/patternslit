var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'EUSERACTIVATION';
function init() {
	refreshQueryGrid();
	refreshTBAGrid();
	if (_redisplay) {
		//$('#userID').attr('readonly', true);
		$('#userActivationStatus').val($('#tmpActivationStatus').val());
	}
	$('#' + "spanActivationPolicy").html($('#' + "activationPolicy").val());
}
function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'EUSERACTIVATION_MAIN', queryGrid);
}
function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}
function doclearfields(id) {
	switch (id) {
	case 'userID':
		if (isEmpty($('#userID').val())) {
			$('#userID_error').html(EMPTY_STRING);
			$('#userID_desc').html(EMPTY_STRING);
			clearNonPKFields();
			break;
		}

	}
}
function clearFields() {

	$('#userID').val(EMPTY_STRING);
	$('#userID_desc').html(EMPTY_STRING);
	$('#userID_error').html(EMPTY_STRING);
	clearNonPKFields();
}

function clearNonPKFields() {
	$('#intimationMode').val(EMPTY_STRING);
	$('#intimationMode_error').html(EMPTY_STRING);
	$('#intimationReferenceNo').val(EMPTY_STRING);
	$('#intimationReferenceNo_error').html(EMPTY_STRING);
	$('#intimationDate').val(EMPTY_STRING);
	$('#intimationDate_error').html(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
}

function modify() {

	$('#intimationMode').focus();
}
function loadData() {
	$('#userID').val(validator.getValue('USER_ID'));
	$('#userID_desc').html(validator.getValue('USER_NAME'));
	$('#intimationMode').val(validator.getValue('INTIMATION_MODE'));
	$('#intimationReferenceNo').val(validator.getValue('INTIMATION_REF_NO'));
	$('#intimationDate').val(validator.getValue('INTIMATION_DATE'));
	$('#remarks').val(validator.getValue('REMARKS'));
	var args = getEntityCode() + '|' + $('#userID').val();
	loadRecordData(loadAdditionalData, CURRENT_PROGRAM_ID, 'EUSERACTIVATION', args);
	resetLoading();
}
function loadAdditionalData() {
	$('#userActivationStatus').val(validator.getValue('STATUS'));
	$('#tmpActivationStatus').val(validator.getValue('STATUS'));
	$('#userActivation').val(validator.getValue('STATUS'));
}
function view(source, primaryKey) {
	hideParent('access/quseractivation', source, primaryKey);
	resetLoading();
}
function doHelp(id) {
	switch (id) {
	case 'userID':
		help('EUSERROLEALLOC', 'USERS', $('#userID').val(), EMPTY_STRING, $('#userID'));
		break;
	}
}

function validate(id) {
	switch (id) {
	case 'userID':
		userID_val();
		break;
	case 'intimationMode':
		intimationMode_val();
		break;
	case 'intimationReferenceNo':
		intimationReferenceNo_val();
		break;
	case 'intimationDate':
		intimationDate_val();
		break;
	case 'remarks':
		remarks_val();
		break;
	}
}

function backtrack(id) {
	switch (id) {
	case 'userID':
		setFocusLast('userID');
	case 'intimationMode':
		setFocusLast('userID');
		break;
	case 'intimationReferenceNo':
		setFocusLast('intimationMode');
		break;
	case 'intimationDate':
		setFocusLast('intimationReferenceNo');
		break;
	case 'remarks':
		setFocusLast('intimationDate');
		break;
	}
}
function userID_val() {
	var value = $('#userID').val();
	clearError('userID_error');
	if (isEmpty(value)) {
		setError('userID_error', MANDATORY);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('USER_ID', $('#userID').val());
		validator.setValue('ACTIVATION', $('#userActivation').html());
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.access.euseractivationbean');
		validator.setMethod('validateUserId');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('userID_error', validator.getValue(ERROR));
			setFocusLast('userID');

			return false;
		} else {
			$('#userID_desc').html(validator.getValue('USER_NAME'));
			setFocusLast('intimationMode');
			return true;
		}
	}
	setFocusLast('intimationMode');
	return true;
}
function intimationMode_val() {
	var value = $('#intimationMode').val();
	clearError('intimationMode_error');
	if (isEmpty(value)) {
		setError('intimationMode_error', MANDATORY);
		return false;
	}
	setFocusLast('intimationReferenceNo');
	return true;
}
function intimationReferenceNo_val() {
	var value = $('#intimationReferenceNo').val();
	clearError('intimationReferenceNo_error');
	if (value != EMPTY_STRING) {
		if (!isValidRefNumber(value)) {
			setError('intimationReferenceNo_error', MANDATORY);
			return false;
		}
	}
	setFocusLast('intimationDate');
	return true;
}
function intimationDate_val() {
	var value = $('#intimationDate').val();
	clearError('intimationDate_error');
	if (isEmpty(value)) {
		setError('intimationDate_error', MANDATORY);
		return false;
	}
	if (!isDate(value)) {
		setError('intimationDate_error', INVALID_DATE);
		return false;
	}
	if (isDateGreater(value, getCBD())) {
		setError('intimationDate_error', DATE_LECBD);
		return false;
	}
	setFocusLast('remarks');
	return true;
}
function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (isEmpty(value)) {
		setError('remarks_error', MANDATORY);
		return false;
	}
	if (!isValidRemarks(value)) {
		setError('remarks_error', INVALID_REMARKS);
		return false;
	}
	setFocusOnSubmit();
	return true;
}
