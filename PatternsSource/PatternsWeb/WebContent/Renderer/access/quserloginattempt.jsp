<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.access.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/access/quserloginattempt.js" />
	</web:dependencies>
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="quserloginattempt.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/access/quserloginattempt" id="quserloginattempt" method="POST">
				<web:viewContent id="po_view1">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle width="210px" />
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="form.userid" var="common" mandatory="true" />
										</web:column>
										<web:column span="3">
											<type:userHelp property="userID" id="userID" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="form.fromdate" var="common" mandatory="true" />
										</web:column>
										<web:column>
											<type:date property="fromDate" id="fromDate" />
										</web:column>
										<web:column>
											<web:legend key="form.todate" var="common" mandatory="true" />
										</web:column>
										<web:column>
											<type:date property="toDate" id="toDate" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="quserloginattempt.attempttype" var="program" mandatory="true" />
										</web:column>
										<web:column span="3">
											<type:combo property="attemptType" id="attemptType" datasourceid="COMMON_LOGINATTEMPT" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column span="4">
											<type:button id="submit" key="form.submit" var="common" onclick="revalidate();" />
											<type:button id="reset" key="form.reset" var="common" onclick="clearFields();" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:viewContent id="po_view4" styleClass="hidden">
								<web:section>
									<web:viewTitle var="program" key="quserloginattempt.section1" />
									<web:table>
										<web:rowOdd>
											<web:column>
												<web:grid height="550px" width="1045px" id="innerGridSucc" src="access/quserloginattempt_Succ.xml">
												</web:grid>
											</web:column>
										</web:rowOdd>
									</web:table>
								</web:section>
							</web:viewContent>
							<web:viewContent id="po_view5" styleClass="hidden">
								<web:section>
									<web:viewTitle var="program" key="quserloginattempt.section2" />
									<web:table>
										<web:rowEven>
											<web:column>
												<web:grid height="550px" width="702px" id="innerGridUnsucc" src="access/quserloginattempt_Unsucc.xml">
												</web:grid>
											</web:column>
										</web:rowEven>
									</web:table>
								</web:section>
							</web:viewContent>
						</web:sectionBlock>
					</web:dividerBlock>
				</web:viewContent>
				<web:element property="command" />
				<web:element property="action" />
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
	<web:contextSearch />
</web:fragment>