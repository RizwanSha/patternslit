var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'EACTROLEALLOC';
function init() {
	refreshTBAGrid();
	refreshQueryGrid();
	if (_redisplay) {
		if ($('#enabled').is(':checked')) {
			showFields();
		} else {
			hideFields();
		}
	}
}
function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'EACTROLEALLOC_MAIN');
}

function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}

function showFields(clear) {
	$('#fromDate').removeAttr("disabled");
	$('#fromDate_pic').removeAttr("disabled");
	$('#toDate').removeAttr("disabled");
	$('#toDate_pic').removeAttr("disabled");
}
function hideFields() {
	setCheckbox('enabled', NO);
	$('#fromDate').attr("disabled", "disabled");
	$('#fromDate').val(EMPTY_STRING);
	$('#fromDate_pic').attr("disabled", "disabled");
	$('#toDate').attr("disabled", "disabled");
	$('#toDate').val(EMPTY_STRING);
	$('#toDate_pic').attr("disabled", "disabled");
}
function clearFields() {

	$('#userID').val(EMPTY_STRING);
	$('#userID_error').html(EMPTY_STRING);
	$('#userID_desc').html(EMPTY_STRING);
	$('#roleCode').val(EMPTY_STRING);
	$('#roleCode_error').html(EMPTY_STRING);
	$('#roleCode_desc').html(EMPTY_STRING);

	clearNonPKFields();
}

function clearNonPKFields() {
	$('#fromDate').val(EMPTY_STRING);
	$('#fromDate_error').html(EMPTY_STRING);
	$('#toDate').val(EMPTY_STRING);
	$('#toDate_error').html(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
}

function add() {

	$('#userID').focus();
	hideFields();
}

function modify() {

	$('#userID').focus();
}

function loadData() {
	$('#userID').val(validator.getValue('USER_ID'));
	$('#userID_desc').html(validator.getValue('F1_USER_NAME'));
	$('#roleCode').val(validator.getValue('ROLE_CODE'));
	$('#roleCode_desc').html(validator.getValue('F2_DESCRIPTION'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	if ($('#enabled').is(':checked')) {
		showFields();
		$('#fromDate').val(validator.getValue('FROM_DATE'));
		$('#toDate').val(validator.getValue('UPTO_DATE'));
	} else {
		hideFields();
	}
	$('#remarks').val(validator.getValue('REMARKS'));
	resetLoading();
}
function doHelp(id) {
	switch (id) {
	case 'userID':
		help(CURRENT_PROGRAM_ID, 'USERS', $('#userID').val(), getUserID(),
				$('#userID'));
		break;
	case 'roleCode':
		help(CURRENT_PROGRAM_ID, 'ROLES', $('#roleCode').val(), $('#userID')
				.val(), $('#roleCode'));
		break;
	}
}
function checkclick(id) {
	switch (id) {
	case 'enabled':
		enabled_val();
		break;
	}
}
function enabled_val() {
	if ($('#enabled').is(':checked')) {
		showFields();
		setFocusLast('fromDate');
	} else {
		hideFields();
		setFocusLast('remarks');
	}
	return true;
}
function view(source, primaryKey) {
	hideParent('access/qactrolealloc', source, primaryKey);
	resetLoading();
}

function validate(id) {
	switch (id) {
	case 'userID':
		userID_val();
		break;
	case 'roleCode':
		roleCode_val();
		break;
	case 'enabled':
		enabled_val();
		break;
	case 'fromDate':
		fromDate_val();
		break;
	case 'toDate':
		toDate_val();
		break;

	case 'remarks':
		remarks_val();
		break;
	}
}

function backtrack(id) {
	switch (id) {
	case 'userID':
		setFocusLast('userID');
		break;
	case 'roleCode':
		setFocusLast('userID');
		break;
	case 'enabled':
		setFocusLast('roleCode');
		break;
	case 'fromDate':
		setFocusLast('enabled');
		break;
	case 'toDate':
		setFocusLast('fromDate');
		break;

	case 'remarks':
		if ($('#enabled').is(':checked')) {
			setFocusLast('toDate');
		} else {
			setFocusLast('enabled');
		}

		break;
	}
}
function userID_val() {
	var value = $('#userID').val();
	clearError('userID_error');
	if (isEmpty(value)) {
		setError('userID_error', MANDATORY);
		return false;
	}
	if (value == getUserID()) {
		setError('userID_error', CANNOT_ALLOCATE_TO_SELF);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('USER_ID', $('#userID').val());
		validator.setValue(ACTION, USAGE);
		validator
				.setClass('patterns.config.web.forms.access.eactroleallocbean');
		validator.setMethod('validateUserId');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('userID_error', validator.getValue(ERROR));
			return false;
		} else {
			$('#userID_desc').html(validator.getValue('USER_NAME'));
		}
	}
	setFocusLast('roleCode');
	return true;
}

function roleCode_val() {
	var value = $('#roleCode').val();
	clearError('roleCode_error');
	if (isEmpty(value)) {
		setError('roleCode_error', MANDATORY);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('CODE', $('#roleCode').val());
		validator.setValue(ACTION, USAGE);
		validator
				.setClass('patterns.config.web.forms.access.eactroleallocbean');
		validator.setMethod('validateRoleCode');
		validator.sendAndReceive();
		if (validator.getValue(RESULT) == DATA_AVAILABLE) {
			$('#roleCode_desc').html(validator.getValue('DESCRIPTION'));
			if ($('#action').val() == MODIFY) {
				PK_VALUE = getEntityCode() + PK_SEPERATOR + $('#userID').val()
						+ PK_SEPERATOR + $('#roleCode').val();
				if (!loadPKValues(PK_VALUE, 'roleCode_error')) {
					return false;
				}
			}
		} else {
			setError('roleCode_error', validator.getValue(ERROR));
			setFocusLast('roleCode');
			return false;
		}
	}
	setFocusLast('enabled');
	return true;
}

function fromDate_val() {
	var value = $('#fromDate').val();
	var cbd = getCBD();
	if (isEmpty(value)) {
		setError('fromDate_error', MANDATORY);
		return false;
	}
	if (!isDate(value)) {
		setError('fromDate_error', INVALID_DATE);
		return false;
	}
	if (!isDateGreaterEqual(value, cbd)) {
		setError('fromDate_error', DATE_GECBD);
		return false;
	}
	clearError('fromDate_error');
	setFocusLast('toDate');
	return true;
}

function toDate_val() {
	var value = $('#toDate').val();
	var frmdate = $('#fromDate').val();
	clearError('toDate_error');
	var cbd = getCBD();
	if (isEmpty(value)) {
		setError('toDate_error', MANDATORY);
		return false;
	}
	if (!isDate(value)) {
		setError('toDate_error', INVALID_DATE);
		return false;
	}
	if (!isDateGreaterEqual(value, frmdate)) {
		setError('toDate_error', DATE_GEFD);
		return false;
	}
	clearError('toDate_error');
	setFocusLast('remarks');
	return true;
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', HMS_INVALID_REMARKS);
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', HMS_INVALID_REMARKS);
			return false;
		}
	}
	if ($('#action').val() == MODIFY) {
		if (isEmpty(value)) {
			setError('remarks_error', HMS_MANDATORY);
			return false;
		}

	}
	setFocusOnSubmit();
	return true;

}
