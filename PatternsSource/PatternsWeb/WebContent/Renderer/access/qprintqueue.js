/*
 *
 Author : Aswaq A
 Created Date : 18-Feb-2015
 Spec Reference : HMS-PSD-ACC-034-qprintqueue
 Modification History
 ----------------------------------------------------------------------
 Sl.No		Modified Date	Author			Modified Changes	Version
 ----------------------------------------------------------------------
 
 */

var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MPRINTQUEUE';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}
function clearFields() {
	$('#printQueueId').val(EMPTY_STRING);
	$('#printQueueDescription').val(EMPTY_STRING);
	$('#printTypeId').val(EMPTY_STRING);
	$('#branchCode').val(EMPTY_STRING);
	$('#branchListCode').val(EMPTY_STRING);
	$('#sourceType').val(EMPTY_STRING);
	$('#sourcePath').val(EMPTY_STRING);
	
	setCheckbox('enabled', "0");
	$('#remarks').val(EMPTY_STRING);
}
function loadData() {
	$('#printQueueId').val(validator.getValue('PRNQ_ID'));
	$('#printQueueId').html(validator.getValue('DESCRIPTION'));

	
	$('#printQueueDescription').val(validator.getValue('DESCRIPTION'));
	
	$('#printTypeId').val(validator.getValue('PRNTYPE_ID'));
	$('#printTypeId_desc').html(validator.getValue('F1_DESCRIPTION'));

	$('#branchCode').val(validator.getValue('BRN_CODE'));
	$('#branchCode_desc').html(validator.getValue('F2_DESCRIPTION'));

	$('#branchListCode').val(validator.getValue('BRNLIST_CODE'));
	$('#branchListCode_desc').html(validator.getValue('F3_DESCRIPTION'));

	$('#sourceType').val(validator.getValue('SRC_TYPE'));
	
	$('#sourcePath').val(validator.getValue('SRC_PATH'));
	
	setCheckbox('enabled', validator.getValue('ENABLED'));
	
	$('#remarks').val(validator.getValue('REMARKS'));
	loadAuditFields(validator);
}