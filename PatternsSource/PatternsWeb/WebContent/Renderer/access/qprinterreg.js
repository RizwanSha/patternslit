var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MPRINTERREG';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}

function clearFields() {
	$('#printerId').val(EMPTY_STRING);
	$('#printerDescription').val(EMPTY_STRING);
	$('#printerName').val(EMPTY_STRING);
	$('#printAddress').val(EMPTY_STRING);
	$('#printerType').val(EMPTY_STRING);
	$('#branchCode').val(EMPTY_STRING);
	$('#branchListCode').val(EMPTY_STRING);
	$('#status').val(EMPTY_STRING);
	setCheckbox('enabled', NO);
	$('#remarks').val(EMPTY_STRING);
	printer_innerGrid.clearAll();
}


function loadData() {
	$('#printerId').val(validator.getValue('PRINTER_ID'));
	$('#printerDescription').val(validator.getValue('PRINTER_DESC'));
	$('#printerName').val(validator.getValue('PRINTER_NAME'));
	$('#printAddress').val(validator.getValue('PRINTER_ADDRESS'));
	$('#printerType').val(validator.getValue('PRINTER_TYPE'));
	$('#branchCode').val(validator.getValue('BRN_CODE'));
	$('#branchCode_desc').html(validator.getValue('F1_DESCRIPTION'));
	$('#branchListCode').val(validator.getValue('BRNLIST_CODE'));
	$('#branchListCode_desc').html(validator.getValue('F2_DESCRIPTION'));
	$('#status').val(validator.getValue('STATUS'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadGrid();
	loadAuditFields(validator);
}


function loadGrid() {
	SOURCE_VALUE = _tableType;
	PK_VALUE = _primaryKey;
	if (_tableType == MAIN) {
		loads();
	} else if (_tableType == TBA) {
		loads();
	}
}

function loads() {
	loadGridQuery(CURRENT_PROGRAM_ID, 'MPRINTERREG_PRINTER_GRID', printer_innerGrid);
}