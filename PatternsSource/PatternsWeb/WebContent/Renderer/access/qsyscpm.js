var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'ISYSCPM';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}
function clearFields() {
	$('#effectiveDate').val(EMPTY_STRING);
	$('#minLengthUID').val(EMPTY_STRING);
	$('#minLengthPwd').val(EMPTY_STRING);
	$('#minNumPwd').val(EMPTY_STRING);
	$('#minAlphaPwd').val(EMPTY_STRING);
	$('#minSCPwd').val(EMPTY_STRING);
	$('#forcePwdChgn').val(EMPTY_STRING);
	$('#prevPwdCnt').val(EMPTY_STRING);
	$('#uwCnt').val(EMPTY_STRING);
	$('#sawCnt').val(EMPTY_STRING);
	$('#lockUIDUACnt').val(EMPTY_STRING);
	$('#lockUIDNUCnt').val(EMPTY_STRING);
	$('#lockUIDUOCnt').val(EMPTY_STRING);
	// $('#UIDExpPeriod').val(EMPTY_STRING);
	$('#pwdExpPeriod').val(EMPTY_STRING);
	$('#totalLeasePeriod').val(EMPTY_STRING);
	$('#autoLogoutPeriod').val(EMPTY_STRING);
	$('#multipleSessionAllowed').prop('checked', false);
	$('#ipRestrictUsers').prop('checked', false);
	$('#actRoleAllowed').prop('checked', false);
	$('#adminActivation').prop('checked', false);
	$('#pwdDelMechanism').val(EMPTY_STRING);
	$('#pwdManualType').val(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
}
function loadData() {
	$('#effectiveDate').val(validator.getValue('EFFT_DATE'));
	$('#minLengthUID').val(validator.getValue('MIN_UID_LEN'));
	$('#minLengthPwd').val(validator.getValue('MIN_PWD_LEN'));
	$('#minNumPwd').val(validator.getValue('MIN_PWD_NUM'));
	$('#minAlphaPwd').val(validator.getValue('MIN_PWD_ALPHA'));
	$('#minSCPwd').val(validator.getValue('MIN_PWD_SPECIAL'));
	$('#forcePwdChgn').val(validator.getValue('FORCE_PWD_CHGN'));
	$('#prevPwdCnt').val(validator.getValue('PREVENT_PREV_PWD'));
	$('#uwCnt').val(validator.getValue('UNSUCC_ATTMPT_UW'));
	$('#sawCnt').val(validator.getValue('UNSUCC_ATTMPT_SW'));
	$('#lockUIDUACnt').val(validator.getValue('LOCK_UID'));
	$('#totalLeasePeriod').val(validator.getValue('TOTAL_LEASE_PERIOD'));
	$('#autoLogoutPeriod').val(validator.getValue('AUTO_LOGOUT_SECS'));
	$('#lockUIDNUCnt').val(validator.getValue('INACT_NOT_USED'));
	$('#lockUIDUOCnt').val(validator.getValue('INACT_ONCE_USED'));
	// $('#UIDExpPeriod').val(validator.getValue('INACT_UID_EXPIRY'));
	$('#pwdExpPeriod').val(validator.getValue('INACT_PWD_EXPIRY'));
	$('#multipleSessionAllowed').prop('checked', decodeSTB(validator.getValue('MULTIPLE_SESSION_ALLOWED')));
	$('#ipRestrictUsers').prop('checked', decodeSTB(validator.getValue('ADMIN_USER_IP_RESTRICT')));
	$('#actRoleAllowed').prop('checked', decodeSTB(validator.getValue('MAX_ACTING_ROLE')));
	$('#adminActivation').prop('checked', decodeSTB(validator.getValue('ADMIN_ACTIVATION_REQD')));
	$('#pwdDelMechanism').val(validator.getValue('PWD_DELIVERY_MECHANISM'));
	$('#pwdManualType').val(validator.getValue('PWD_MANUAL_TYPE'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadAuditFields(validator);
}