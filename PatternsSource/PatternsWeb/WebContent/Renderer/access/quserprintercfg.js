var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'IUSERPRINTERCFG';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}
function clearFields() {
	$('#userId').val(EMPTY_STRING);
	$('#effectiveDate').val(EMPTY_STRING);
	iuserprintercfg_clearGridFields();
	iuserprintercfgpgm_clearGridFields();
}

function iuserprintercfg_clearGridFields() {
	$('#printTypeId').val(EMPTY_STRING);
	$('#printTypeId_desc').html(EMPTY_STRING);
	$('#printerId').val(EMPTY_STRING);
	$('#printerId_desc').html(EMPTY_STRING);
	setCheckbox('defaultPrinter', NO);
}

function iuserprintercfgpgm_clearGridFields() {
	$('#pgmId').val(EMPTY_STRING);
	$('#pgmId_desc').html(EMPTY_STRING);
	$('#progPrinterId').val(EMPTY_STRING);
	$('#progPrinterId_desc').html(EMPTY_STRING);
}

function loadPrntCfgGrid() {
	loadGridQuery(CURRENT_PROGRAM_ID, 'IUSERPRINTCFG_GRID',
			iuserprintercfg_innerGrid);
}
function loadPgmPrntCfgGrid() {
	loadGridQuery(CURRENT_PROGRAM_ID, 'IUSERPRINTCFGPGM_GRID',
			iuserprintercfgpgm_innerGrid);
}

function loadData() {
	$('#userId').val(validator.getValue('USER_ID'));
	$('#userId_desc').html(validator.getValue('F1_USER_NAME'));
	$('#effectiveDate').val(validator.getValue('EFFT_DATE'));
	loadAuditFields(validator);
	SOURCE_VALUE = _tableType;
	PK_VALUE = _primaryKey;
	loadPrntCfgGrid();
	loadPgmPrntCfgGrid();
}