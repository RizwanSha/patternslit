package patterns.config.web.forms.access;
/*

*
Author : Pavan kumar.R
Created Date : 20-FEB-2015
Spec Reference: PSD-ACC-38
Modification History
----------------------------------------------------------------------
Sl.No		Modified Date	Author			Modified Changes	Version
----------------------------------------------------------------------

*/


import java.sql.ResultSet;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.process.TBAActionType;
import patterns.config.framework.service.DTDObject;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.AccessValidator;
import patterns.config.validations.CommonValidator;
import patterns.config.web.forms.GenericFormBean;

public class ibranchlistprintqcfgbean extends GenericFormBean {
	private static final long serialVersionUID = -6219657363150466160L;
	private String branchListCode;
	private String effectiveDate;
	private String printTypeId;
	private String printQueueId;
	private String xmlIbranchlistprintqcfgGrid;

	public String getPrintTypeId() {
		return printTypeId;
	}

	public void setPrintTypeId(String printTypeId) {
		this.printTypeId = printTypeId;
	}

	public String getBranchListCode() {
		return branchListCode;
	}

	public void setBranchListCode(String branchListCode) {
		this.branchListCode = branchListCode;
	}

	public String getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public String getPrintQueueId() {
		return printQueueId;
	}

	public void setPrintQueueId(String printQueueId) {
		this.printQueueId = printQueueId;
	}

	public String getXmlIbranchlistprintqcfgGrid() {
		return xmlIbranchlistprintqcfgGrid;
	}

	public void setXmlIbranchlistprintqcfgGrid(
			String xmlIbranchlistprintqcfgGrid) {
		this.xmlIbranchlistprintqcfgGrid = xmlIbranchlistprintqcfgGrid;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public void reset() {
		branchListCode = RegularConstants.EMPTY_STRING;
		effectiveDate = RegularConstants.EMPTY_STRING;
		printTypeId = RegularConstants.EMPTY_STRING;
		printQueueId = RegularConstants.EMPTY_STRING;
		xmlIbranchlistprintqcfgGrid = RegularConstants.EMPTY_STRING;
		setProcessBO("patterns.config.framework.bo.access.ibranchlistprintqcfgBO");
	}

	@Override
	public void validate() {
		if (validateBranchListCode() && validateEffectiveDate()) {
			validateGrid();
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.set("BRNLIST_CODE", branchListCode);
				formDTO.set("EFFT_DATE", effectiveDate);
				DTDObject dtdObject = new DTDObject();
				dtdObject.addColumn(0, "SL");
				dtdObject.addColumn(1, "PRNTYPE_ID");
				dtdObject.addColumn(2, "PRNTYPE_ID_DESC");
				dtdObject.addColumn(3, "PRNQ_ID");
				dtdObject.addColumn(4, "PRNQ_ID_DESC");
				dtdObject.setXML(xmlIbranchlistprintqcfgGrid);
				formDTO.setDTDObject("BRNLISTPRNQCFGHISTDTL", dtdObject);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("branchListCode",BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		}
	}

	private boolean validateBranchListCode() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("BRNLIST_CODE", branchListCode);
			formDTO.set(ContentManager.USER_ACTION, USAGE);
			formDTO = validateBranchListCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("branchListCode",
						formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("branchListCode",
					BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateBranchListCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, RegularConstants.EMPTY_STRING);
		AccessValidator validation = new AccessValidator();
		branchListCode = inputDTO.get("BRNLIST_CODE");
		try {
			if (validation.isEmpty(branchListCode)) {
				resultDTO.set(ContentManager.ERROR,
						BackOfficeErrorCodes.HMS_FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS,"DESCRIPTION");
			resultDTO = validation.validateEntityBranchList(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR,
						resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(
					ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR,
						BackOfficeErrorCodes.HMS_INVALID_CODE);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR,
					BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	private boolean validateEffectiveDate() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("ACTION", getAction());
			formDTO.set("BRNLIST_CODE", branchListCode);
			formDTO.set("EFFT_DATE", effectiveDate);
			formDTO = validateEffectiveDate(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("effectiveDate",
						formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("effectiveDate",
					BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);

		}
		return false;
	}

	public DTObject validateEffectiveDate(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		CommonValidator commonvalidation = new CommonValidator();
		String branchListCode = inputDTO.get("BRNLIST_CODE");
		String effectiveDate = inputDTO.get("EFFT_DATE");
		String action = inputDTO.get(ContentManager.USER_ACTION);
		try {
			if (commonvalidation.isEmpty(effectiveDate)) {
				resultDTO.set(ContentManager.ERROR,BackOfficeErrorCodes.HMS_FIELD_BLANK);
				return resultDTO;
			}
			TBAActionType AT;
			if (action.equals(ADD))
				AT = TBAActionType.ADD;
			else
				AT = TBAActionType.MODIFY;
			if (!commonvalidation.isEffectiveDateValid(effectiveDate, AT)) {
				resultDTO.set(ContentManager.ERROR,BackOfficeErrorCodes.HMS_INVALID_EFFECTIVE_DATE);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "EFFT_DATE");
			inputDTO.set(ContentManager.TABLE, "BRNLISTPRNQCFGHIST");
			String columns[] = new String[] { context.getEntityCode(),
					branchListCode, effectiveDate };
			inputDTO.setObject(ContentManager.PROGRAM_KEY, columns);
			resultDTO = commonvalidation.validatePK(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR,
						resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (action.equals(ADD)) {
				if (ContentManager.DATA_AVAILABLE.equals(resultDTO
						.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR,
							BackOfficeErrorCodes.HMS_RECORD_EXISTS);
					return resultDTO;
				}
			} else if (action.equals(MODIFY)) {
				if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO
						.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR,
							BackOfficeErrorCodes.HMS_RECORD_NOT_EXISTS);
					return resultDTO;
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR,
					BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			commonvalidation.close();
		}
		return resultDTO;
	}

	private boolean validateGrid() {
		AccessValidator validation = new AccessValidator();
		try {
			DTDObject dtdObject = new DTDObject();
			dtdObject.addColumn(0, "SL");
			dtdObject.addColumn(1, "PRNTYPE_ID");
			dtdObject.addColumn(2, "PRNTYPE_ID_DESC");
			dtdObject.addColumn(3, "PRNQ_ID");
			dtdObject.addColumn(4, "PRNQ_ID_DESC");
			dtdObject.setXML(xmlIbranchlistprintqcfgGrid);
			if (dtdObject.getRowCount() <= 0) {
				getErrorMap().setError("printTypeId",BackOfficeErrorCodes.HMS_ATLEAST_ONE_ROW);
				return false;
			}
			if (!validation.checkDuplicateRows(dtdObject, 1)) {
				getErrorMap().setError("printTypeId",
						
						
						BackOfficeErrorCodes.HMS_DUPLICATE_DATA);
				return false;
			}
			for (int i = 0; i < dtdObject.getRowCount(); i++) {
				if (!validatePrintTypeId(dtdObject.getValue(i, 1)))
				{
					getErrorMap().setError("printTypeId",BackOfficeErrorCodes.HMS_INVALID_ACTION);
					return false;
				}
				if (!validatePrintQueueId(dtdObject.getValue(i, 3), branchListCode)){
					getErrorMap().setError("printQueueId",BackOfficeErrorCodes.HMS_INVALID_ACTION);
					return false;
					
				}
				
			}

			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("printTypeId",
					BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	private boolean validatePrintTypeId(String printTypeId) {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("PRNTYPE_ID", printTypeId);
			formDTO.set("ACTION", USAGE);
			formDTO = validatePrintTypeId(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("printTypeId",
						formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("printTypeId",
					BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validatePrintTypeId(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, RegularConstants.EMPTY_STRING);
		AccessValidator validation = new AccessValidator();
		printTypeId = inputDTO.get("PRNTYPE_ID");
		try {
			if (validation.isEmpty(printTypeId)) {
				resultDTO.set(ContentManager.ERROR,BackOfficeErrorCodes.HMS_FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
			resultDTO = validation.validatePrintType(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR,
						resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(
					ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR,BackOfficeErrorCodes.HMS_INVALID_CODE);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR,
					BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	private boolean validatePrintQueueId(String printQueueId,String branchListCode) {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("BRNLIST_CODE", branchListCode);
			formDTO.set("PRNQ_ID", printQueueId);
			formDTO.set("ACTION", USAGE);
			formDTO = validatePrintQueueId(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("printQueueId",
						formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("printQueueId",
					BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validatePrintQueueId(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		DTObject resultDTOBranch = new DTObject();
		resultDTOBranch.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		resultDTO.set(ContentManager.RESULT, RegularConstants.EMPTY_STRING);
		AccessValidator validation = new AccessValidator();
		branchListCode = inputDTO.get("BRNLIST_CODE");
		printQueueId = inputDTO.get("PRNQ_ID");
		try {
			if (validation.isEmpty(printQueueId)) {
				resultDTO.set(ContentManager.ERROR,BackOfficeErrorCodes.HMS_FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
			resultDTO = validation.validatePrintQueueId(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR,
						resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR,BackOfficeErrorCodes.HMS_INVALID_CODE);
				return resultDTO;
			}
				resultDTOBranch = validateBranchListPrintQueueId(inputDTO);
				if (ContentManager.DATA_UNAVAILABLE.equals(resultDTOBranch.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.PRNT_BRNLIST_CODE_UNAVAILABLE);
					return resultDTO;
				}
} 

			catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR,
					BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public DTObject validateBranchListPrintQueueId(DTObject inputDTO) {
		DTObject resultDTOBranch = new DTObject();
		resultDTOBranch.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		try {
			DBContext dbContext = new DBContext();
			DBUtil dbutil = dbContext.createUtilInstance();
			String sqlQuery = null;
			try {
				dbutil.reset();
				dbutil.setMode(DBUtil.PREPARED);
				sqlQuery = "SELECT COUNT(1) FROM  PRNQ  WHERE ENTITY_CODE=? AND BRNLIST_CODE=? AND PRNQ_ID=? ";
				dbutil.reset();
				dbutil.setSql(sqlQuery);
				dbutil.setString(1, context.getEntityCode());
				dbutil.setString(2, inputDTO.get("BRNLIST_CODE"));
				dbutil.setString(3, inputDTO.get("PRNQ_ID"));
				ResultSet rset = dbutil.executeQuery();
				if (rset.next()) {
					if (rset.getInt(1) == 0) {
						resultDTOBranch.set(ContentManager.RESULT,
								ContentManager.DATA_UNAVAILABLE);
						return resultDTOBranch;
					}
				} else {
					resultDTOBranch.set(ContentManager.RESULT,
							ContentManager.DATA_AVAILABLE);
					return resultDTOBranch;
				}
			} catch (Exception e) {
				e.printStackTrace();
				resultDTOBranch.set(ContentManager.ERROR,
						BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
			} finally {
				dbContext.close();
			}

		} catch (Exception e) {
			e.printStackTrace();
			resultDTOBranch.set(ContentManager.ERROR,
					BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		}
		return resultDTOBranch;
	}

}
