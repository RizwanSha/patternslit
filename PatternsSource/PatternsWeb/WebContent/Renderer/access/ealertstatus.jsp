<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.access.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/access/ealertstatus.js" />
	</web:dependencies>
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="ealertstatus.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/access/ealertstatus" id="ealertstatus" method="POST">
				<web:dividerBlock>
					<type:optionBox add="false" modify="true" view="true" />
				</web:dividerBlock>
				<web:viewContent id="po_view1" styleClass="hidden">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="ealertstatus.processorcode" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:code property="processorCode" id="processorCode" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="ealertstatus.jobcategory" var="program" />
										</web:column>
										<web:column>
											<type:comboDisplay property="jobCategory" id="jobCategory" datasourceid="COMMON_JOBCATEGORY" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="ealertstatus.jobnature" var="program" />
										</web:column>
										<web:column>
											<type:comboDisplay property="jobNature" id="jobNature" datasourceid="COMMON_JOBNATURE" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="form.enabled" var="common" />
										</web:column>
										<web:column>
											<type:checkbox property="enabled" id="enabled" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="ealertstatus.cronexpression" var="program" />
										</web:column>
										<web:column>
											<type:cron property="cronExpression" id="cronExpression" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="ealertstatus.recordperrequest" var="program" />
										</web:column>
										<web:column>
											<type:sixDnumber property="recordPerRequest" id="recordPerRequest" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="form.remarks" var="common" mandatory="true" />
										</web:column>
										<web:column>
											<type:remarks property="remarks" id="remarks" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column span="2">
											<web:submitReset />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
						</web:sectionBlock>
					</web:dividerBlock>
				</web:viewContent>
				<web:element property="command" />
				<web:element property="action" />
				<web:element property="tempJobCategory" />
				<web:element property="tempJobNature" />
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
	<web:queryGrid />
	<web:TBAGrid />
	<web:contextSearch />
</web:fragment>