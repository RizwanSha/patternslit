var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'EBKANNOUNCEDTLS';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}
function clearFields() {
	$('#announceID').val(EMPTY_STRING);
	$('#startDate').val(EMPTY_STRING);
	$('#expiryDate').val(EMPTY_STRING);
	$('#displayTitle').val(EMPTY_STRING);
	$('#url').val(EMPTY_STRING);
	$('#urlTitle').val(EMPTY_STRING);
	$('#visibility').val(EMPTY_STRING);
	$('#announceDisabled').val(EMPTY_STRING);
	$('#userID').val(EMPTY_STRING);
	$('#roleType').val(EMPTY_STRING);
	$('#customerCode').val(EMPTY_STRING);
	$('#branchCode').val(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
}
function loadData() {
	$('#announceID').val(validator.getValue('ANNOUNCE_ID'));
	$('#startDate').val(validator.getValue('ANNOUNCE_START_DT'));
	$('#expiryDate').val(validator.getValue('ANNOUNCE_END_DT'));
	$('#displayTitle').val(validator.getValue('ANNOUNCE_TITLE'));
	$('#externalLink').prop('checked',decodeSTB(validator.getValue('EXTERNAL_LINK')));
	$('#url').val(validator.getValue('ANNOUNCE_URL'));
	$('#urlTitle').val(validator.getValue('ANNOUNCE_URL_TITLE'));
	$('#visibility').val(validator.getValue('ANNOUNCE_VISIBLITY'));
	$('#announceDisabled').prop('checked',decodeSTB(validator.getValue('ANNOUNCE_DISABLED')));
	$('#userID').val(validator.getValue('ANNOUNCE_USER_ID'));
	$('#roleType').val(validator.getValue('ANNOUNCE_ROLE_TYPE'));
	$('#customerCode').val(validator.getValue('ANNOUNCE_CUSTOMER_CODE'));
	$('#branchCode').val(validator.getValue('ANNOUNCE_BRANCH_CODE'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadAuditFields(validator);
	if ($('#userID').val() != EMPTY_STRING) {
		validator.reset();
		validator.setMtm(false);
		validator.setValue('USER_ID', $('#userID').val());
		validator.setClass('patterns.config.web.forms.access.ebkannouncedtlsbean');
		validator.setMethod('fetchUserName');
		validator.sendAndReceive();
		$('#userID_desc').html(validator.getValue('USER_NAME'));
	}
	if ($('#customerCode').val() != EMPTY_STRING) {
		validator.reset();
		validator.setMtm(false);
		validator.setValue('CUSTOMER_CODE', $('#customerCode').val());
		validator.setClass('patterns.config.web.forms.access.ebkannouncedtlsbean');
		validator.setMethod('fetchCustomerName');
		validator.sendAndReceive();
		$('#customerCode_desc').html(validator.getValue('CUSTOMER_CUSTOMER_NAME'));
	}
	if ($('#branchCode').val() != EMPTY_STRING) {
		validator.reset();
		validator.setMtm(false);
		validator.setValue('BRANCH_CODE', $('#branchCode').val());
		validator.setClass('patterns.config.web.forms.access.ebkannouncedtlsbean');
		validator.setMethod('fetchBranchName');
		validator.sendAndReceive();
		$('#branchCode_desc').html(validator.getValue('BANKBRANCHES_DESCRIPTION'));
	}
}