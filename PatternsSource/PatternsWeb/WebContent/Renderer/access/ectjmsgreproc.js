var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'ECTJMSGREPROC';
var request = EMPTY_STRING;
function init() {
	$('#action').val(ADD);
	$('#programIdentifier').focus();
	eipregreqqueue_innerGrid.attachEvent("onRowDblClicked", doRowDblClicked);
}
function doHelp(id) {
	switch (id) {
	case 'programIdentifier':
		help('ECTJMSGREPROC', 'HLP_ECTJMSGREPROC', $('#programIdentifier').val(), EMPTY_STRING, $('#programIdentifier'));
		break;
	}

}

function doclearfields(id) {
	switch (id) {
	case 'programIdentifier':
		if (isEmpty($('#programIdentifier').val())) {
			$('#programIdentifier_error').html(EMPTY_STRING);
			eipregreqqueue_innerGrid.clearAll();
			break;
		}
	}
}

function clearFields() {
	$('#programIdentifier').val(EMPTY_STRING);
	$('#programIdentifier_error').html(EMPTY_STRING);
	$('#programIdentifier_desc').html(EMPTY_STRING);
	eipregreqqueue_innerGrid.clearAll();
	$('#programIdentifier').focus();
}
function validate(id) {
	valMode = true;
	switch (id) {
	case 'programIdentifier':
		programIdentifier_val(valMode);
		break;
	}
}

function programIdentifier_val(valMode) {
	var value = $('#programIdentifier').val();
	clearError('programIdentifier_error');
	if (isEmpty(value)) {
		setError('programIdentifier_error', HMS_MANDATORY);
		setFocusLast('programIdentifier');
		return false;
	} else if ($('#action').val() == ADD && !isValidCode(value)) {
		setError('programIdentifier_error', HMS_INVALID_FORMAT);
		setFocusLast('programIdentifier');
		return false;
	} else {
		if ($('#action').val() == ADD) {
			validator.clearMap();
			validator.setMtm(false);
			validator.setValue('PROGRAM_IDENTIFIER', $('#programIdentifier').val());
			validator.setValue(ACTION, $('#action').val());
			validator.setClass('patterns.config.web.forms.access.ectjmsgreprocbean');
			validator.setMethod('programIdentifierValidate');
			validator.sendAndReceive();
			if (validator.getValue(ERROR) != EMPTY_STRING) {
				setError('programIdentifier_error', validator.getValue(ERROR));
				setFocusLast('programIdentifier');
				return false;
			} else {
				$('#programIdentifier_desc').val(validator.getValue('MPGM_ID'));
			}
		}
	}
	setFocusOnsub();
	return true;
}
function setFocusOnsub() {
	$('#submit').focus();
}
function Submit() {
	eipregreqqueue_innerGrid.clearAll();
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('PROGRAM_IDENTIFIER', $('#programIdentifier').val());
	validator.setValue('ACTION', $('#action').val());
	validator.setClass('patterns.config.web.forms.access.ectjmsgreprocbean');
	validator.setMethod('loadGrid');
	validator.sendAndReceive();
	if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		xmlString = validator.getValue(RESULT_XML);
		eipregreqqueue_innerGrid.loadXMLString(xmlString);
		return true;
	}

}

function Refresh() {
	eipregreqqueue_innerGrid.clearAll();
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('PROGRAM_IDENTIFIER', $('#programIdentifier').val());
	validator.setValue('ACTION', $('#action').val());
	validator.setClass('patterns.config.web.forms.access.ectjmsgreprocbean');
	validator.setMethod('loadGrid');
	validator.sendAndReceive();
	if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		xmlString = validator.getValue(RESULT_XML);
		eipregreqqueue_innerGrid.loadXMLString(xmlString);
		return true;
	}
}
function doRowDblClicked(rowID) {
	eipregreqqueue_innerGrid.clearSelection();
	eipregreqqueue_innerGrid.selectRowById(rowID, true);
	var refId = eipregreqqueue_innerGrid.cells(rowID, 0).getValue();
	var processValue = eipregreqqueue_innerGrid.cells(rowID, 5).getValue();
	if (processValue == 0) {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('MESSAGE_REF_ID', refId);
		validator.setValue('ACTION', $('#action').val());
		validator.setClass('patterns.config.web.forms.access.ectjmsgreprocbean');
		validator.setMethod('updateCtjProcInqPe');
		validator.sendAndReceive();
		if (validator.getValue(RESULT) == DATA_AVAILABLE) {
			eipregreqqueue_innerGrid.setRowColor(rowID, '#F34E04');
			eipregreqqueue_innerGrid.clearSelection();
			alert("Selected Record added for Re-Process ");
			eipregreqqueue_innerGrid.cells(rowID, 5).setValue(true);
		} else {
			alert("Duplicate entry-Record Alredy Exists In ctjprocinqpe");
		}
	}else{
		alert("Selected Record is Al-Ready Present ctjprocinqpe ");
		eipregreqqueue_innerGrid.clearSelection();
	}
}
