var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MCONSOLE';

function init() {
	refreshTBAGrid();
	refreshQueryGrid();
	if (_redisplay) {
		if ($('#action').val() == ADD) {
			$('#enabled').prop('disabled', true);
			setCheckbox('enabled', YES);
		}
	}
}

function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'MCONSOLE_MAIN');
}

function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}

function doclearfields(id) {
	switch (id) {
	case 'consoleCode':
		if (isEmpty($('#consoleCode').val())) {
			$('#consoleCode_error').html(EMPTY_STRING);
			clearNonPKFields();
			break;
		}
	}
}
function clearFields() {

	$('#consoleCode').val(EMPTY_STRING);
	$('#consoleCode_error').html(EMPTY_STRING);
	$('#consoleCode').focus();
	clearNonPKFields();
}

function clearNonPKFields() {
	$('#description').val(EMPTY_STRING);
	$('#description_error').html(EMPTY_STRING);
	//$('#enabled').prop('checked', false);
	$('#enabled_error').html(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
}
function add() {

	$('#consoleCode').focus();
	setCheckbox('enabled', YES);
	$('#enabled').prop('disabled', true);
}

function modify() {

	$('#consoleCode').focus();
	if ($('#action').val() == MODIFY) {
		$('#enabled').prop('disabled', false);
	} else {
		$('#enabled').prop('disabled', true);
	}
}

function loadData() {
	$('#consoleCode').val(validator.getValue('CODE'));
	$('#description').val(validator.getValue('DESCRIPTION'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	resetLoading();
}

function view(source, primaryKey) {
	hideParent('access/qconsole', source, primaryKey);
	resetLoading();
}
function doHelp(id) {
	switch (id) {
	case 'consoleCode':
		help('MCONSOLE', 'HLP_CONSOLE_CODE', $('#consoleCode').val(), EMPTY_STRING, $('#consoleCode'));
		break;
	}
}

function validate(id) {
	switch (id) {
	case 'consoleCode':
		consoleCode_val();
		break;
	case 'description':
		description_val();
		break;
	case 'enabled':
		enabled_val();
		break;
	case 'remarks':
		remarks_val();
		break;
	}
}

function enabled_val() {
	if ($('#action').val() == MODIFY) {
		setFocusLast('remarks');
	}
}

function backtrack(id) {
	switch (id) {
	case 'consoleCode':
		setFocusLast('consoleCode');
		break;
	case 'description':
		setFocusLast('consoleCode');
		break;
	case 'enabled':
		setFocusLast('description');
		break;
	case 'remarks':
		if ($('#action').val() == MODIFY) {
			setFocusLast('enabled');
		} else {
			setFocusLast('description');
		}
		break;
	}
}

function consoleCode_val() {
	var value = $('#consoleCode').val();
	clearError('consoleCode_error');
	if (isEmpty(value)) {
		setError('consoleCode_error', MANDATORY);
		setFocusLast('consoleCode');
		return false;
	} else if ($('#action').val() == ADD && !isValidCode(value)) {
		setError('consoleCode_error', HMS_INVALID_FORMAT);
		setFocusLast('roleCode');
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('CODE', $('#consoleCode').val());
		validator.setValue(ACTION, $('#action').val());
		validator.setClass('patterns.config.web.forms.access.mconsolebean');
		validator.setMethod('validateConsoleCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('consoleCode_error', validator.getValue(ERROR));
			setFocusLast('consoleCode');
			return false;
		} else {
			if ($('#action').val() == MODIFY) {
				PK_VALUE = getEntityCode() + PK_SEPERATOR + $('#consoleCode').val();
				if(!loadPKValues(PK_VALUE, 'consoleCode_error')){
					return false;
				}
				setFocusLast('description');
				return true;
			}
		}
	}
	setFocusLast('description');
	return true;

}

function description_val() {
	var value = $('#description').val();
	clearError('description_error');
	if (!isValidDescription(value)) {
		setError('description_error', INVALID_DESCRIPTION);
		setFocusLast('description');
		return false;
	}
	if ($('#action').val() == ADD) {
		setFocusLast('remarks');
	} else {
		setFocusLast('enabled');
	}
	return true;
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', HMS_INVALID_REMARKS);
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', HMS_INVALID_REMARKS);
			return false;
		}
	}
	if ($('#action').val() == MODIFY) {
		if (isEmpty(value)) {
			setError('remarks_error', HMS_MANDATORY);
			return false;
		}
		
	}
	setFocusOnSubmit();
	return true;

}