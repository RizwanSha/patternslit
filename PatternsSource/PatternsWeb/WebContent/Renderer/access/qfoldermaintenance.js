var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'IFOLDERMAINTENANCE';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}
function clearFields() {
	$('#effectiveDate').val(EMPTY_STRING);
	$('#folderReportsGeneration').val(EMPTY_STRING);
	$('#folderPortalUpload').val(EMPTY_STRING);
	$('#folderTempUpload').val(EMPTY_STRING);
	$('#folderCBSFileUpload').val(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
}
function loadData() {
	$('#effectiveDate').val(validator.getValue('EFFT_DATE'));
	$('#folderReportsGeneration').val(validator.getValue('REPORT_GENERATION_PATH'));
	$('#folderPortalUpload').val(validator.getValue('PORTAL_FILE_UPLOAD_PATH'));
	$('#folderTempUpload').val(validator.getValue('TEMP_FILE_UPLOAD_PATH'));
	$('#folderCBSFileUpload').val(validator.getValue('CBS_FILE_TRANSFER_PATH'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadAuditFields(validator);
}