var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MCOURSE';
var _oldRoleType = EMPTY_STRING;
function init() {
	refreshTBAGrid();
	refreshQueryGrid();
	if (_redisplay) {
		if ($('#action').val() == MODIFY) {
			
		} else {
			$('#enabled').prop('disabled', true);
			$('#enabled').prop('checked',true);
		}
	}
}
function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'MCOURSE_MAIN', queryGrid);
}
function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}
function clearFields() {
	
	$('#courseId').val(EMPTY_STRING);
	$('#courseId_error').html(EMPTY_STRING);
	$('#courseTitle').val(EMPTY_STRING);
	$('#courseTitle_error').html(EMPTY_STRING);
	$('#courseDescn').val(EMPTY_STRING);
	$('#courseDescn_error').html(EMPTY_STRING);
	$('#enabled').prop('checked',false);
	$('#enabled_error').html(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
	$('#position').val(EMPTY_STRING);
	$('#position_error').html(EMPTY_STRING);
}
function add() {
	
	$('#courseId').focus();
	$('#enabled').prop('checked',true);
	$('#enabled').prop('disabled', true);
}
function modify() {
	
	$('#courseId').focus();
	if ($('#action').val() == MODIFY) {
		$('#enabled').prop('disabled', false);
	} else {
		$('#enabled').prop('disabled', true);
	}
}
function loadData() {
	$('#courseId').val(validator.getValue('COURSE_ID'));
	$('#courseTitle').val(validator.getValue('COURSE_TITLE'));
	$('#courseDescn').val(validator.getValue('COURSE_DESCN'));
	$('#enabled').prop('checked',decodeSTB(validator.getValue('ACTIVE')));
	$('#position').val(validator.getValue('COURSE_POSITION'));
	$('#remarks').val(validator.getValue('REMARKS'));
	resetLoading();
}
function view(source, primaryKey) {
	hideParent('access/qcourse', source, primaryKey);
	resetLoading();
}
function validate(id) {
	switch (id) {
	
	}
}


function revalidate() {
	
	if (!remarks_val()) {
		errors++;
	}
}