<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.access.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/access/mpkicertauth.js" />
	</web:dependencies>
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="mpkicertauth.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/access/mpkicertauth" id="mpkicertauth" method="POST">
				<web:dividerBlock>
					<type:optionBox add="true" modify="true" view="true" />
				</web:dividerBlock>
				<web:viewContent id="po_view1" styleClass="hidden">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="250px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="mpkicertauth.code" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:inputCode property="code" id="code" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="250px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="mpkicertauth.description" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:description property="description" id="description" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="mpkicertauth.hierlevel" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:combo property="heirarchyLevel" id="heirarchyLevel" datasourceid="COMMON_HEIRLEVEL"/>
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="mpkicertauth.parentcode" var="program"/>
										</web:column>
										<web:column>
											<type:code property="parentCode" id="parentCode" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="mpkicertauth.cahttpurl" var="program" />
										</web:column>
										<web:column>
											<type:url property="certAuthHttpUrl" id="certAuthHttpUrl" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="mpkicertauth.caldapurl" var="program" />
										</web:column>
										<web:column>
											<type:url property="certAuthLdapUrl" id="certAuthLdapUrl" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="mpkicertauth.ocspverify" var="program" />
										</web:column>
										<web:column>
											<type:checkbox property="ocspAvail" id="ocspAvail" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="mpkicertauth.ocsphttpurl" var="program" />
										</web:column>
										<web:column>
											<type:url property="ocspHttpUrl" id="ocspHttpUrl" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="mpkicertauth.crlhttpurl" var="program" />
										</web:column>
										<web:column>
											<type:url property="crlHttpUrl" id="crlHttpUrl" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="mpkicertauth.httpreftime" var="program" />
										</web:column>
										<web:column>
											<type:refTime property="httpRefreshTime" id="httpRefreshTime" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="mpkicertauth.crlldapurl" var="program" />
										</web:column>
										<web:column>
											<type:url property="crlLdapUrl" id="crlLdapUrl" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="mpkicertauth.ldapreftime" var="program" />
										</web:column>
										<web:column>
											<type:refTime property="ldapRefreshTime" id="ldapRefreshTime" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="form.enabled" var="common" />
										</web:column>
										<web:column>
											<type:checkbox property="enabled" id="enabled" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="form.remarks" var="common" mandatory="true" />
										</web:column>
										<web:column>
											<type:remarks property="remarks" id="remarks" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column span="2">
											<web:submitReset />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
						</web:sectionBlock>
					</web:dividerBlock>
				</web:viewContent>
				<web:element property="command" />
				<web:element property="action" />
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
	<web:contextSearch />
	<web:queryGrid />
	<web:TBAGrid />
</web:fragment>