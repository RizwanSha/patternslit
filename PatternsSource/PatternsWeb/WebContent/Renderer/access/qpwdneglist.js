var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'IPWDNEGLIST';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}
function clearFields() {
	$('#effectiveDate').val(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	innerGrid.clearAll();
}

function loadPasswordGrid() {
	SOURCE_VALUE = _tableType;
	PK_VALUE = _primaryKey;
	loadGridQuery(CURRENT_PROGRAM_ID, 'IPWDNEGLIST_GRID', innerGrid);
}
function loadData() {
	$('#effectiveDate').val(validator.getValue('EFFT_DATE'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadPasswordGrid();
	loadAuditFields(validator);
}