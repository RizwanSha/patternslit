var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'QUSERLOGINATTEMPT';
var inlineGridSuccessful = null;
var inlineGridUnsuccessful = null;

function init() {
	$('#userID').focus();
}
function loadUserLoginGrid() {
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('USER_ID', $('#userID').val());
	validator.setValue('FROM_DATE', $('#fromDate').val());
	validator.setValue('TO_DATE', $('#toDate').val());
	validator.setValue('ATTEMPT_TYPE', $('#attemptType').val());
	validator.setClass('patterns.config.web.forms.access.quserloginattemptbean');
	validator.setMethod('loadUserLoginGrid');
	validator.sendAndReceive();
	if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		if ($('#attemptType').val() == 'S') {
			$('#po_view4').removeClass('hidden');
			innerGridSucc.clearAll();
			innerGridSucc.loadXMLString(validator.getValue("XML"));
		} else if ($('#attemptType').val() == 'U') {
			$('#po_view5').removeClass('hidden');
			innerGridUnsucc.clearAll();
			innerGridUnsucc.loadXMLString(validator.getValue("XML"));
		} else if ($('#attemptType').val() == 'X') {
			$('#po_view4').removeClass('hidden');
			$('#po_view5').removeClass('hidden');
			innerGridSucc.clearAll();
			innerGridUnsucc.clearAll();
			innerGridSucc.loadXMLString(validator.getValue("XML1"));
			innerGridUnsucc.loadXMLString(validator.getValue("XML2"));
		}
	}
}
function change(id) {
	$('#po_view4').addClass('hidden');
	$('#po_view5').addClass('hidden');
	innerGridSucc.clearAll();
	innerGridUnsucc.clearAll();
}

function doclearfields(id) {
	switch (id) {
	case 'userID':
		if (isEmpty($('#userID').val())) {
			$('#userID_error').html(EMPTY_STRING);
			$('#userID_desc').html(EMPTY_STRING);
			clearNonPKFields();
			break;
		}
	}
}

function clearFields() {
	$('#userID').val(EMPTY_STRING);
	$('#userID_desc').html(EMPTY_STRING);
	$('#userID_error').html(EMPTY_STRING);
	clearNonPKFields();
}

function clearNonPKFields() {
	$('#fromDate').val(EMPTY_STRING);
	$('#fromDate_error').html(EMPTY_STRING);
	$('#toDate').val(EMPTY_STRING);
	$('#toDate_error').html(EMPTY_STRING);
	$('#attemptType').val(EMPTY_STRING);
	$('#attemptType_error').html(EMPTY_STRING);
	innerGridSucc.clearAll();
	innerGridUnsucc.clearAll();
	$('#po_view4').addClass('hidden');
	$('#po_view5').addClass('hidden');
}
function doHelp(id) {
	switch (id) {
	case 'userID':
		help('COMMON', 'USERS_INTERNAL', $('#userID').val(), EMPTY_STRING, $('#userID'));
		break;
	}
}
function validate(id) {
	valMode = true;
	switch (id) {
	case 'userID':
		userID_val(valMode);
		break;
	case 'fromDate':
		fromDate_val(valMode);
		break;
	case 'toDate':
		toDate_val(valMode);
		break;
	case 'attemptType':
		attemptType_val(valMode);
		break;
	}
}

function backtrack(id) {
	switch (id) {
	case 'userID':
		setFocusLast('userID');
		break;
	case 'fromDate':
		setFocusLast('userID');
		break;
	case 'toDate':
		setFocusLast('fromDate');
		break;
	case 'attemptType':
		setFocusLast('toDate');
		break;
	case 'submit':
		setFocusLast('attemptType');
		break;
	}
}

function userID_val(valMode) {
	var value = $('#userID').val();
	clearError('userID_error');
	if (isEmpty(value)) {
		setError('userID_error', MANDATORY);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('USER_ID', $('#userID').val());
	validator.setClass('patterns.config.web.forms.access.quserloginattemptbean');
	validator.setMethod('validateUserID');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('userID_error', validator.getValue(ERROR));
		return false;
	} else if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		$('#userID_desc').html(validator.getValue("USER_NAME"));
	}
	setFocusLast('fromDate');
	return true;
}
function fromDate_val(valMode) {
	var value = $('#fromDate').val();
	clearError('fromDate_error');
	if (isEmpty(value)) {
		setError('fromDate_error', MANDATORY);
		return false;
	}
	if (!isDate(value)) {
		setError('fromDate_error', INVALID_DATE);
		return false;
	}
	if (!isDateLesserEqual(value, getCBD())) {
		setError('fromDate_error', DATE_LECBD);
		return false;
	}
	setFocusLast('toDate');
	return true;
}
function toDate_val(valMode) {
	var value = $('#toDate').val();
	var fromDate = $('#fromDate').val();
	clearError('toDate_error');
	if (isEmpty(value)) {
		setError('toDate_error', MANDATORY);
		return false;
	}
	if (!isDate(value)) {
		setError('toDate_error', INVALID_DATE);
		return false;
	}
	if (!isDateLesserEqual(value, getCBD())) {
		setError('toDate_error', DATE_LECBD);
		return false;
	}
	if (!isDateGreaterEqual(value, fromDate)) {
		setError('toDate_error', DATE_GEFD);
		return false;
	}
	setFocusLast('attemptType');
	return true;
}
function attemptType_val(valMode) {
	var value = $('#attemptType').val();
	clearError('attemptType_error');
	if (isEmpty(value)) {
		setError('attemptType_error', MANDATORY);
		return false;
	}
	setFocusLast('submit');
	return true;
}
function revalidate() {
	errors = 0;
	if (!userID_val(false)) {
		errors++;
	}
	if (!fromDate_val(false)) {
		errors++;
	}
	if (!toDate_val(false)) {
		errors++;
	}
	if (!attemptType_val(false)) {
		errors++;
	}
	if (errors == 0) {
		loadUserLoginGrid();
	} else {
		innerGridSucc.clearAll();
		innerGridUnsucc.clearAll();
	}
}