var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'ISMSCONFIG';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}
function clearFields() {
	$('#smsCode').val(EMPTY_STRING);
	$('#smsCode_desc').html(EMPTY_STRING);
	$('#effectiveDate').val(EMPTY_STRING);
	setCheckbox('httpAllowed', NO);
	setCheckbox('sslRequired', NO);
	$('#serverUrl').val(EMPTY_STRING);
	setCheckbox('proxyRequired', NO);
	$('#proxyServer').val(EMPTY_STRING);
	$('#proxyServerPort').val(EMPTY_STRING);
	setCheckbox('proxyServerAuthentication', NO);
	$('#proxyAccountName').val(EMPTY_STRING);
	$('#proxyAccountPassword').val(EMPTY_STRING);
	setCheckbox('authenticationRequired', NO);
	$('#userNameKey').val(EMPTY_STRING);
	$('#accountName').val(EMPTY_STRING);
	$('#accountPasswordKey').val(EMPTY_STRING);
	$('#accountPassword').val(EMPTY_STRING);
	$('#mobileNumberKey').val(EMPTY_STRING);
	$('#messageKey').val(EMPTY_STRING);
	setCheckbox('smppAllowed', NO);
	$('#senderNumber').val(EMPTY_STRING);
	$('#systemType').val(EMPTY_STRING);
	$('#addressRange').val(EMPTY_STRING);
	setCheckbox('transceiverEnabled', NO);
	$('#mainServer').val(EMPTY_STRING);
	$('#mainServerPort').val(EMPTY_STRING);
	$('#failOverServer').val(EMPTY_STRING);
	$('#failOverServerPort').val(EMPTY_STRING);
	$('#dispatchType').val(EMPTY_STRING);
	$('#retryTimeout').val(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
}

function loadData() {
	$('#smsCode').val(validator.getValue('SMS_CODE'));
	$('#smsCode_desc').html(validator.getValue('F1_SMS_DESCN'));
	$('#effectiveDate').val(validator.getValue('EFFT_DATE'));
	setCheckbox('httpAllowed', validator.getValue('SMSC_HTTP_ALLOWED'));
	setCheckbox('sslRequired', validator.getValue('SSL_REQUIRED'));
	$('#serverUrl').val(validator.getValue('SERVER_URL'));
	setCheckbox('proxyRequired', validator.getValue('PROXY_REQUIRED'));
	$('#proxyServer').val(validator.getValue('PROXY_SERVER'));
	$('#proxyServerPort').val(validator.getValue('PROXY_SERVER_PORT'));
	setCheckbox('proxyServerAuthentication', validator.getValue('PROXY_AUTH_REQD'));
	$('#proxyAccountName').val(validator.getValue('PROXY_USER_NAME'));
	$('#proxyAccountPassword').val(validator.getValue('PROXY_PASSWORD'));
	setCheckbox('authenticationRequired', validator.getValue('AUTH_REQD'));
	$('#userNameKey').val(validator.getValue('USER_NAME_KEY'));
	$('#accountName').val(validator.getValue('USER_NAME'));
	$('#accountPasswordKey').val(validator.getValue('PASSWORD_KEY'));
	$('#accountPassword').val(validator.getValue('PASSWORD'));
	$('#mobileNumberKey').val(validator.getValue('MOBILE_NUMBER_KEY'));
	$('#messageKey').val(validator.getValue('MESSAGE_KEY'));
	setCheckbox('smppAllowed', validator.getValue('SMSC_SMPP_ALLOWED'));
	$('#senderNumber').val(validator.getValue('SOURCE_NUMBER'));
	$('#systemType').val(validator.getValue('SYSTEM_TYPE'));
	$('#addressRange').val(validator.getValue('ADDRESS_RANGE'));
	setCheckbox('transceiverEnabled', validator.getValue('TRANSCEIVER_MODE'));
	$('#mainServer').val(validator.getValue('MAIN_SMPP_SERVER'));
	$('#mainServerPort').val(validator.getValue('MAIN_SMPP_PORT'));
	$('#failOverServer').val(validator.getValue('FAILOVER_SMPP_SERVER'));
	$('#failOverServerPort').val(validator.getValue('FAILOVER_SMPP_PORT'));
	$('#dispatchType').val(validator.getValue('SMS_TYPE'));
	$('#retryTimeout').val(validator.getValue('RETRY_TIMEOUT'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadAuditFields(validator);
}