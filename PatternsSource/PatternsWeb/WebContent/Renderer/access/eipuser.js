var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'EIPUSER';
function init() {
	refreshQueryGrid();
	refreshTBAGrid();
	if (_redisplay) {
		if ($('#enabled').is(':checked')) {
			innerGrid.clearAll();
			if (!isEmpty($('#xmlIPList').val())) {
				innerGrid.loadXMLString($('#xmlIPList').val());
			}
		}
		showFields();
	}
}

function loadGrid() {
	loadGridQuery(CURRENT_PROGRAM_ID, 'EIPUSER_GRID', innerGrid);
}

function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'EIPUSER_MAIN', queryGrid);
}

function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}

function doHelp(id) {
	switch (id) {
	case 'userID':
		help('COMMON', 'HLP_USERS_M', $('#userID').val(), EMPTY_STRING, $('#userID'));
		break;
	}
}

function doclearfields(id) {
	switch (id) {
	case 'userID':
		if (isEmpty($('#userID').val())) {
			$('#userID_error').html(EMPTY_STRING);
			$('#userID_desc').html(EMPTY_STRING);
			break;
		}
	case 'effectiveDate':
		if (isEmpty($('#effectiveDate').val())) {
			$$('#effectiveDate_error').html(EMPTY_STRING);
			clearNonPKFields();
			break;
		}
	}
}

function clearFields() {

	$('#userID').val(EMPTY_STRING);
	$('#userID_error').html(EMPTY_STRING);
	$('#userID_desc').html(EMPTY_STRING);
	$('#effectiveDate').val(EMPTY_STRING);
	$('#effectiveDate_error').html(EMPTY_STRING);

}

function clearNonPKFields() {
	$('#enabled').prop('checked', false);
	$('#enabled_error').html(EMPTY_STRING);
	$('#IPAddress').val(EMPTY_STRING);
	$('#IPAddress_error').html(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
	innerGrid.clearAll();
}

function showFields() {
	if ($('#enabled').is(':checked')) {
		$('#IPAddress').removeAttr("disabled");
		$('#IPAddress').val(EMPTY_STRING);
		$('#IPAddress_error').html(EMPTY_STRING);
	} else {
		$('#IPAddress').attr("disabled", "disabled");
		$('#IPAddress').val(EMPTY_STRING);
		$('#IPAddress_error').html(EMPTY_STRING);
	}
}

function checkclick(id) {
	switch (id) {
	case 'enabled':
		enabled_val(valMode);
		break;
	}
}

function enabled_val(valMode) {
	if ($('#enabled').is(':checked')) {
		$('#IPAddress').removeAttr("disabled");
		$('#IPAddress').val(EMPTY_STRING);
		$('#IPAddress_error').html(EMPTY_STRING);
		setFocusLast('IPAddress');
	} else {
		$('#IPAddress').attr("disabled", "disabled");
		$('#IPAddress').val(EMPTY_STRING);
		$('#IPAddress_error').html(EMPTY_STRING);
		innerGrid.clearAll();
		setFocusLast('remarks');
	}
}

function add() {

	showFields();
	$('#userID').focus();
}

function modify() {

}

function loadData() {
	$('#userID').val(validator.getValue('USER_ID'));
	$('#userID_desc').html(validator.getValue('BANKUSERS_USER_NAME'));
	$('#effectiveDate').val(validator.getValue('EFFT_DATE'));
	$('#enabled').prop('checked', decodeSTB(validator.getValue('ENABLED')));
	$('#remarks').val(validator.getValue('REMARKS'));
	showFields();
	if ($('#enabled').is(':checked')) {
		loadGrid();
	} else {
		innerGrid.clearAll();
	}
	resetLoading();
}

function view(source, primaryKey) {
	hideParent('access/qipuser', source, primaryKey);
	resetLoading();
}

function validate(id) {
	var valMode = true;
	switch (id) {
	case 'userID':
		userID_val(valMode);
		break;
	case 'effectiveDate':
		effectiveDate_val(valMode);
		break;
	case 'enabled':
		enabled_val(valMode);
		break;
	case 'IPAddress':
		IPAddress_val(valMode);
		break;
	case 'remarks':
		remarks_val(valMode);
		break;

	}
}

function backtrack(id) {
	switch (id) {
	case 'userID':
		setFocusLast('userID');
		break;
	case 'effectiveDate':
		setFocusLast('userID');
		break;
	case 'enabled':
		setFocusLast('effectiveDate');
		break;
	case 'IPAddress':
		setFocusLast('enabled');
		break;
	case 'remarks':
		if ($('#enabled').is(':checked')) {
			setFocusLast('IPAddress');
		} else {
			setFocusLast('enabled');
		}
		break;

	}
}

function gridExit(id) {
	switch (id) {
	case 'IPAddress':
		$('#xmlIPList').val(innerGrid.serialize());
		setFocusLast('remarks');
		break;
	}
}

function gridDeleteRow(id) {
	switch (id) {
	case 'IPAddress':
		deleteGridRecord(innerGrid);
		break;
	}
}

function userID_val(valMode) {
	var value = $('#userID').val();
	clearError('userID_error');
	if (isEmpty(value)) {
		setError('userID_error', MANDATORY);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('USER_ID', $('#userID').val());
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.access.muserbean');
		validator.setMethod('validateUserId');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('effectiveDate_error', validator.getValue(ERROR));
			return false;
		} else {
			$('#userID_desc').html(validator.getValue('USER_NAME'));
		}

	}
	setFocusLast('effectiveDate');
	return true;
}

function effectiveDate_val(valMode) {
	var value = $('#effectiveDate').val();
	clearError('effectiveDate_error');
	if (!isValidEffectiveDate(value, 'effectiveDate_error')) {
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('USER_ID', $('#userID').val());
		validator.setValue('EFFT_DATE', $('#effectiveDate').val());
		validator.setValue(ACTION, $('#action').val());
		validator.setClass('patterns.config.web.forms.access.eipuserbean');
		validator.setMethod('validateEffectiveDate');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('effectiveDate_error', validator.getValue(ERROR));
			return false;
		} else {
			if ($('#action').val() == MODIFY) {
				PK_VALUE = getEntityCode() + PK_SEPERATOR + $('#user_ID').val() + PK_SEPERATOR + $('#effectiveDate').val();
				if (!loadPKValues(PK_VALUE, 'effectiveDate_error')) {
					return false;
				}
				setFocusLast('enabled');
				return true;
			}

		}

	}
	setFocusLast('enabled');
	return true;
}

function IPAddress_val(valMode) {
	var value = $('#IPAddress').val();
	clearError('IPAddress_error');
	if (isEmpty(value)) {
		setError('IPAddress_error', MANDATORY);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('IP_ADDRESS', value);
		validator.setClass('patterns.config.web.forms.access.eipuserbean');
		validator.setMethod('validateIP');
		validator.sendAndReceive();
		if (validator.getValue(RESULT) == DATA_AVAILABLE) {
			if (valMode) {
				setFocusLastOnGridSubmit('innerGrid');
			}
			return true;
		} else {
			setError('IPAddress_error', validator.getValue(ERROR));
			return false;
		}
	}
}

function ipAddressDuplicateCheck(editMode, editRowId) {
	var currentValue = [ $('#IPAddress').val() ];
	return _grid_duplicate_check(innerGrid, currentValue, [ 1 ]);
}

function addFunction(editMode, editRowId) {
	if ($('#enabled').is(':checked')) {
		if (IPAddress_val(false)) {
			var IPAddress = $('#IPAddress').val();
			if (ipAddressDuplicateCheck(editMode, editRowId)) {
				var field_values = [ 'false', IPAddress ];
				_grid_updateRow(innerGrid, field_values);
				clearGridFields();
				$('#IPAddress').focus();
				return true;
			} else {
				setError('IPAddress_error', DUPLICATE_DATA);
				$('#IPAddress').focus();
				return false;

			}
		} else {
			$('#IPAddress').focus();
			return false;
		}
	}
}

function editFunction(rowId) {
	if ($('#enabled').is(':checked')) {
		$('#IPAddress').val(innerGrid.cells(rowId, 1).getValue());
		$('#IPAddress').focus();

	}
}

function cancelFunction(rowId) {
	clearGridFields();
}

function clearGridFields() {
	$('#IPAddress').val(EMPTY_STRING);
	clearError('IPAddress_error');
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (isEmpty(value)) {
		setError('remarks_error', MANDATORY);
		return false;
	}
	if (!isValidRemarks(value)) {
		setError('remarks_error', INVALID_REMARKS);
		return false;
	}
	setFocusOnSubmit();
	return true;
}

function revalidate() {
	if ($('#enabled').is(':checked') == true) {
		if (innerGrid.getRowsNum() > 0) {
			$('#xmlIPList').val(innerGrid.serialize());
			$('#IPAddress_error').html(EMPTY_STRING);
		} else {
			innerGrid.clearAll();
			$('#xmlIPList').val(innerGrid.serialize());
			setError('IPAddress_error', ADD_ATLEAST_ONE_ROW);
			errors++;
		}
	}
}