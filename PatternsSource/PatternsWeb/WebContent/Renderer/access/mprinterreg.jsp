<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.access.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/access/mprinterreg.js" />
	</web:dependencies>
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="mprinterreg.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/access/mprinterreg" id="mprinterreg" method="POST">
				<web:dividerBlock>
					<type:optionBox add="true" modify="true" view="true" />
				</web:dividerBlock>
				<web:viewContent id="po_view1" styleClass="hidden">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="200px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="mprinterreg.printerId" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:printerId property="printerId" id="printerId" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="mprinterreg.printerDescription" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:description property="printerDescription" id="printerDescription" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="mprinterreg.printerName" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:description property="printerName" id="printerName" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="mprinterreg.printAddress" var="program" />
										</web:column>
										<web:column>
											<type:description property="printAddress" id="printAddress" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="mprinterreg.printerType" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:description property="printerType" id="printerType" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="mprinterreg.branchCode" var="program" />
										</web:column>
										<web:column>
											<type:entityBranchList property="branchCode" id="branchCode" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="mprinterreg.branchListCode" var="program" />
										</web:column>
										<web:column>
											<type:entityBranchList property="branchListCode" id="branchListCode" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="mprinterreg.status" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:combo property="status" id="status" datasourceid="MPRINTERREG_STATUS" />
										</web:column>
									</web:rowOdd>
									<web:rowOdd>
										<web:column>
											<web:legend key="form.enabled" var="common" />
										</web:column>
										<web:column>
											<type:checkbox property="enabled" id="enabled" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="form.remarks" var="common" />
										</web:column>
										<web:column>
											<type:remarks property="remarks" id="remarks" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:viewTitle var="program" key="mprinterreg.section" />
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="120px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="mprinterreg.printTypeId" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:printType property="printTypeId" id="printTypeId" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column span="2">
											<web:gridToolbar gridName="printer_innerGrid" cancelFunction="printer_cancelGrid" editFunction="printer_editGrid" addFunction="printer_addGrid" continuousEdit="true" />
											<web:element property="xmlPrinterRegGrid" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column span="2">
											<web:viewContent id="po_view4">
												<web:grid height="240px" width="250px" id="printer_innerGrid" src="access/mprinterreg_printer_innerGrid.xml">
												</web:grid>
											</web:viewContent>
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:submitReset />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
						</web:sectionBlock>
					</web:dividerBlock>
				</web:viewContent>
				<web:element property="command" />
				<web:element property="action" />
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
	<web:queryGrid />
	<web:TBAGrid />
	<web:contextSearch />
</web:fragment>
