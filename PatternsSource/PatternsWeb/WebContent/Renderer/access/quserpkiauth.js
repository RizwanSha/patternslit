var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'IUSERPKIAUTH';

function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}

function clearFields() {
	$('#roleCode').val(EMPTY_STRING);
	$('#roleCode_desc').html(EMPTY_STRING);
	$('#effectiveDate').val(EMPTY_STRING);
	$('#tfaMandatory').prop('checked',false);
	$('#loginAuthReqd').prop('checked',false);
	$('#remarks').val(EMPTY_STRING);
}

function loadData() {
	$('#roleCode').val(validator.getValue('ROLE_CODE'));
	$('#roleCode_desc').html(validator.getValue('BANKROLES_DESCRIPTION'));
	$('#effectiveDate').val(validator.getValue('EFFT_DATE'));
	$('#tfaMandatory').prop('checked',decodeSTB(validator.getValue('TFA_MAND')));
	if ($('#tfaMandatory').is(':checked')) {
		$('#loginAuthReqd').prop('checked',decodeSTB(validator.getValue('LOGIN_AUTH_REQD')));
	}
	$('#remarks').val(validator.getValue('REMARKS'));
	loadAuditFields(validator);
}