
var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MBRNLIST'; 
var desc = EMPTY_STRING;
function init() {
	refreshQueryGrid();
	refreshTBAGrid();
	if (_redisplay) {
		brnGrid.clearAll();
		if(!isEmpty($('#xmlBrnList').val())){			
			brnGrid.loadXMLString($('#xmlBrnList').val());
		}
		if ($('#action').val() == ADD) {
			$('#enabled').prop('disabled', true);		
			$('#enabled').prop('checked',true);
		}
			
	}
}
function refreshQueryGrid(){
	loadQueryGrid(CURRENT_PROGRAM_ID, 'MBRNLIST_MAIN', queryGrid);
}

function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}
function loadProgramsGrid() {
	loadGridQuery(CURRENT_PROGRAM_ID,'BRN_GRID', brnGrid);
}

function clearFields(){
	
	$('#brnListCode').val(EMPTY_STRING);
	$('#brnListCode_error').html(EMPTY_STRING);
	$('#brnListDescription').val(EMPTY_STRING);
	$('#brnListDescription_error').html(EMPTY_STRING);
	$('#brnCode').val(EMPTY_STRING);
	$('#brnCode_error').html(EMPTY_STRING);
	$('#brnCode_desc').html(EMPTY_STRING);
	$('#enabled').prop('checked',false);
	$('#enabled_error').html(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
	brnGrid.clearAll();
}
function add() {
	
	$('#brnListCode').focus();
	$('#enabled').prop('checked',true);	
	$('#enabled').prop('disabled', true);
}
function modify() {
	
	//$('#brnCode').focus();
	if ($('#action').val() == MODIFY) {
		$('#enabled').prop('disabled', false);	
	}else{
		$('#enabled').prop('disabled', true);
	}
}
function loadData(){
	$('#brnListCode').val(validator.getValue('CODE'));
	$('#brnListDescription').val(validator.getValue('DESCRIPTION'));
	$('#brnCode_desc').html(validator.getValue('BRANCHES_DESCRIPTION'));
	$('#enabled').prop('checked',decodeSTB(validator.getValue('ENABLED')));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadProgramsGrid();
	resetLoading();	
}
function view(source,primaryKey) {
	hideParent('access/qbrnlist',source,primaryKey);
	resetLoading();
}
function validate(id){
	switch(id){
		case 'brnListCode':
			brnListCode_val();
		break;
		case 'brnCode':
			brnCode_val();
		break;
		case 'brnListDescription':
			brnListDescription_val();
		break;
		case 'remarks':
			remarks_val();
		break;
	}
}
function brnListCode_val(){
	var value = $('#brnListCode').val();
	clearError('brnListCode_error');
	if (isEmpty(value)) {
		setError('brnListCode_error', MANDATORY);
		return false;
	}
	if (!isValidCode(value)) {
		setError('brnListCode_error', INVALID_FORMAT);
		return false;
	}
	return true;
}

function brnListDescription_val(){
	var value = $('#brnListDescription').val();
	clearError('brnListDescription_error');	
	if (!isValidDescription(value)) {
		setError('brnListDescription_error', INVALID_DESCRIPTION);
		return false;
	}
	return true;
}

function brnCode_val(){
	var value = $('#brnCode').val();
	clearError('brnCode_error');
	if (isEmpty(value)) {
		setError('brnCode_error', MANDATORY);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('BRANCH_CODE',value);
	validator.setClass('patterns.config.web.forms.access.mbrnlistbean');
	validator.setMethod('validateBranchCode');
	validator.sendAndReceive();
	if(validator.getValue(RESULT)==DATA_AVAILABLE ){
		desc = validator.getValue('MBRN_NAME');
		return true;
	}	else{
		setError('brnCode_error', validator.getValue(ERROR));
		return false;
	}
	return true;
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (isEmpty(value)) {
		setError('remarks_error', MANDATORY);
		return false;
	}
	if (!isValidRemarks(value)) {
		setError('remarks_error', INVALID_REMARKS);
		return false;
	}
	return true;
}

function myaddfunction(){
	clearError('brnCode_error');
	if(brnCode_val()){
	    var rowid = brnGrid.uid();
	   	var branchCode = $('#brnCode').val();
	   	if(branchCodeDuplicateCheck()){
	   		var sl = brnGrid.getRowsNum()+1;
			var field_values=['false', branchCode, desc];
			brnGrid.addRow(rowid,field_values,brnGrid.getRowsNum());
			rowid = EMPTY_STRING;
			clearGridFields();
			$('#brnCode_desc').html(EMPTY_STRING);
			return true;
	   	} else{
	   		setError('brnCode_error', DUPLICATE_DATA);
	   		return false;
	   	}
	}else{
		return false;
	}
}

function myeditfunction(rowID){
  	$('#brnCode').val(brnGrid.cells(rowID,1).getValue());
	$('#brnCode_desc').html(validator.getValue('DESCRIPTION'));
  	brnGrid.deleteRow(rowID);
   	$('#brnCode').focus();
}

function clearGridFields(){
   	$('#brnCode').val(EMPTY_STRING);
   	clearError('brnCode_error');
}

function doHelp(id){
	switch(id){
		case 'brnCode':
			help('MBRNLIST', 'BRANCHES', $('#brnCode').val(), EMPTY_STRING, $('#brnCode'));
		break;
	}
}
function branchCodeDuplicateCheck()	{
	var row ;
	var gridvalue;
	var currvalue ;
	total_rows = brnGrid.getRowsNum();
	currvalue = $('#brnCode').val();
	for (row=0;row<total_rows;row++)	{
	    gridvalue = brnGrid.cells2(row,1).getValue();
	    if ( gridvalue == currvalue){
	       return false;
	    }
	}
	return true;
}

function revalidate(){
	if(!brnListCode_val()){
		errors++;
	}
	if(!brnListDescription_val()){
		errors++;
	}
	if (!remarks_val()) {
		errors++;
	}	
	if(brnGrid.getRowsNum() > 0){
		$('#xmlBrnList').val(brnGrid.serialize());
	}else{
		setError('brnCode_error', ADD_ATLEAST_ONE_ROW);
		errors++;
	}
}