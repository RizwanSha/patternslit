var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = "EUSERACTROLEALLOC";
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}
function clearFields() {
	$('#userID').val("");
	$('#roleCode').val("");
	$('#fromDate').val("");
	$('#toDate').val("");
}
function loadData() {
	$('#userID').val(validator.getValue("USER_ID"));
	$('#userID_desc').html(validator.getValue("USER_NAME"));
	$('#roleCode').val(validator.getValue("ROLE_CODE"));
	$('#roleCode_desc').html(validator.getValue("DESCRIPTION"));
	$('#enabled').prop('checked',decodeSTB(validator.getValue("ENABLED")));
	if ($('#enabled').is(':checked')) {
		$('#fromDate').val(validator.getValue("FROM_DATE"));
		$('#toDate').val(validator.getValue("UPTO_DATE"));
	}
	loadAuditFields(validator);
}
