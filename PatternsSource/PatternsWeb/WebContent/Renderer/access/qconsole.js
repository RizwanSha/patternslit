var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MCONSOLE';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}
function clearFields() {
	$('#consoleCode').val(EMPTY_STRING);
	$('#description').val(EMPTY_STRING);
	$('#enabled').prop('checked',false);
	$('#remarks').val(EMPTY_STRING);
}
function loadData() {
	$('#consoleCode').val(validator.getValue('CODE'));
	$('#description').val(validator.getValue('DESCRIPTION'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadAuditFields(validator);
}