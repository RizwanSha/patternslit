var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'EROLEBRNLIST';
function init() {
	refreshQueryGrid();
	refreshTBAGrid();
	if (_redisplay) {
		if ($('#enabled').is(':checked')) {
			showFields();
		} else {
			hideFields();
		}
	}
}
function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'EROLEBRNLIST_MAIN', queryGrid);
}
function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}
function showFields() {
	$('#brnListCode').removeAttr("disabled"); 
	$('#brnListCode_pic').removeAttr("disabled"); 
}
function hideFields() {
	$('#brnListCode').attr("disabled", "disabled"); 
	$('#brnListCode').val(EMPTY_STRING);
	$('#brnListCode_desc').html(EMPTY_STRING);
	$('#brnListCode_error').html(EMPTY_STRING);
	$('#brnListCode_pic').attr("disabled", "disabled"); 
}
function clearFields() {
	
	$('#roleCode').val(EMPTY_STRING);
	$('#roleCode_error').html(EMPTY_STRING);
	$('#roleCode_desc').html(EMPTY_STRING);
	$('#effectiveDate').val(EMPTY_STRING);
	$('#effectiveDate_error').html(EMPTY_STRING);
	$('#enabled').prop('checked',false);
	$('#brnListCode').val(EMPTY_STRING);
	$('#brnListCode_error').html(EMPTY_STRING);
	$('#brnListCode_desc').html(EMPTY_STRING);
	$('#brnListCode').attr("disabled", "disabled"); 
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
}
function add() {
	
	hideFields();
	$('#roleCode').focus();
}
function modify() {
	
	$('#enabled').focus();
}
function loadData() {
	$('#roleCode').val(validator.getValue('ROLE_CODE'));
	$('#roleCode_desc').html(validator.getValue('BANKROLES_DESCRIPTION'));
	$('#effectiveDate').val(validator.getValue('EFFT_DATE'));
	$('#enabled').prop('checked',decodeSTB(validator.getValue('ENABLED')));
	if ($('#enabled').is(':checked')) {
		showFields();
		$('#brnListCode').val(validator.getValue('BRANCH_LIST_CODE'));
		$('#brnListCode_desc').html(validator.getValue('BRANCHLIST_DESCRIPTION'));
	} else {
		hideFields();
		$('#brnListCode').attr("disabled", "disabled"); 
	}
	$('#remarks').val(validator.getValue('REMARKS'));
	resetLoading();
}
function view(source, primaryKey) {
	hideParent('access/qrolebrnlist', source, primaryKey);
	resetLoading();
}
function validate(id) {
	switch (id) {
	case 'roleCode':
		roleCode_val();
		break;
	case 'brnListCode':
		brnListCode_val();
		break;
	case 'effectiveDate':
		effectiveDate_val();
		break;
	case 'remarks':
		remarks_val();
		break;
	}
}
function roleCode_val() {
	var value = $('#roleCode').val();
	clearError('roleCode_error');
	if (isEmpty(value)) {
		setError('roleCode_error', MANDATORY);
		return false;
	}
	return true;
}
function brnListCode_val() {
	var value = $('#brnListCode').val();
	clearError('brnListCode_error');
	if (isEmpty(value)) {
		setError('brnListCode_error', MANDATORY);
		return false;
	}
	return true;
}
function effectiveDate_val() {
	var value = $('#effectiveDate').val();
	clearError('effectiveDate_error');
	if(!isValidEffectiveDate(value,'effectiveDate_error')){
		return false;
	}
	return true;
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (isEmpty(value)) {
		setError('remarks_error', MANDATORY);
		return false;
	}
	if (!isValidRemarks(value)) {
		setError('remarks_error', INVALID_REMARKS);
		return false;
	}
	return true;
}

function revalidate() {
	if (!roleCode_val()) {
		errors++;
	}

	if (!effectiveDate_val()) {
		errors++;
	}
	if ($('#enabled').is(':checked') == true) {
		if (!brnListCode_val()) {
			errors++;
		}
	}
	if (!remarks_val()) {
		errors++;
	}
}
function doHelp(id) {
	switch (id) {
	case 'roleCode':
		help('EROLEBRNLIST', 'ROLES', $('#roleCode').val(), EMPTY_STRING, $('#roleCode'));
		break;
	case 'brnListCode':
		help('EROLEBRNLIST', 'BRANCHLIST', $('#brnListCode').val(), EMPTY_STRING, $('#brnListCode'));
		break;
	}
}

function checkclick(id) {
	switch (id) {
	case 'enabled':
		if ($('#enabled').is(':checked')) {
			showFields();
		} else {
			hideFields();
		}
		break;
	}
}