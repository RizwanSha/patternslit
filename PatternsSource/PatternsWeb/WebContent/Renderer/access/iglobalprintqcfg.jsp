<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.access.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/access/iglobalprintqcfg.js" />
	</web:dependencies>
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="iglobalprintqcfg.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/access/iglobalprintqcfg" id="iglobalprintqcfg" method="POST">
				<web:dividerBlock>
					<type:optionBox add="true" modify="true" view="true" template="true" />
				</web:dividerBlock>
				<web:viewContent id="po_view1">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="form.effectivedate" var="common" mandatory="true" />
										</web:column>
										<web:column>
											<type:date property="effectiveDate" id="effectiveDate" lookup="true" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:viewTitle var="program" key="iglobalprintqcfg.section" />
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="iglobalprintqcfg.printTypeId" var="program"  />
										</web:column>
										<web:column>
											<type:printerId property="printTypeId" id="printTypeId"    />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="iglobalprintqcfg.printQueueId" var="program" />
										</web:column>
										<web:column>
											<type:printQueue property="printQueueId" id="printQueueId"   />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:rowOdd>
										<web:column span="2">
											<web:gridToolbar gridName="innerGrid" cancelFunction="iglobalprintqcfg_cancelGrid" editFunction="iglobalprintqcfg_editGrid" addFunction="iglobalprintqcfg_addGrid" continuousEdit="true" />
											<web:element property="xmlPrinterConfigGrid" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column span="2">
												<web:grid height="240px" width="560px" id="innerGrid" src="access/iglobalprintqcfg_innerGrid.xml" paging="false">
												</web:grid>
										</web:column>
									</web:rowEven>
									<web:rowEven>
										<web:column span="2">
											<web:submitReset />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
						</web:sectionBlock>
					</web:dividerBlock>
				</web:viewContent>
				<web:element property="command" />
				<web:element property="action" />
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
	<web:queryGrid />
	<web:TBAGrid />
	<web:contextSearch />
</web:fragment>