var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'IBRANCHPRINTQCFG';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}

function loads() {
	loadGridQuery(CURRENT_PROGRAM_ID, 'IBRANCHPRINTQCFG_GRID', branchPrintqGrid);
}

function clearFields() {
	$('#branchCode').val(EMPTY_STRING);
	$('#branchCode_desc').html(EMPTY_STRING);
	$('#effectiveDate').val(EMPTY_STRING);
	branchPrintqGrid.clearAll();
}
function loadData() {
	$('#branchCode').val(validator.getValue('BRN_CODE'));
	$('#branchCode_desc').html(validator.getValue('F1_DESCRIPTION'));
	$('#effectiveDate').val(validator.getValue('EFFT_DATE'));
	loadGrid();
	loadAuditFields(validator);
}
function loadGrid() {
	SOURCE_VALUE = _tableType;
	PK_VALUE = _primaryKey;
	if (_tableType == MAIN) {
		loads();
	} else if (_tableType == TBA) {
		loads();
	}
}