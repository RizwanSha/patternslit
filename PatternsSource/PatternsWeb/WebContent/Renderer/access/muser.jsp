<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.access.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/access/muser.js" />
	</web:dependencies>
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="muser.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/access/muser" id="muser" method="POST">
				<web:dividerBlock>
					<type:optionBox add="true" modify="true" view="true" />
				</web:dividerBlock>
				<web:viewContent id="po_view1" styleClass="hidden">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column span="4">
											<div class="legend info-message" id="spanUserPolicy"></div>
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="form.userid" var="common" mandatory="true" />
										</web:column>
										<web:column>
											<type:user property="userID" id="userID" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="muser.empcode" var="program" />
										</web:column>
										<web:column>
											<type:employeeCode property="empCode" id="empCode" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:viewContent id="personDetails" styleClass="hidden">
								<web:section>
									<web:viewTitle var="program" key="muser.empdtls" />
								</web:section>
							</web:viewContent>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="muser.titlename" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:name property="titleName" id="titleName" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="muser.dateofjoin" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:date property="dateOfJoin" id="dateOfJoin" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="muser.mobilenumber" var="program" />
										</web:column>
										<web:column>
											<type:mobile property="mobileNumber" id="mobileNumber" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="muser.emailid" var="program" />
										</web:column>
										<web:column>
											<type:email property="emailID" id="emailID" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="muser.branchcode" var="program"  mandatory="true"  />
										</web:column>
										<web:column>
											<type:code property="branchCode" id="branchCode" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:viewTitle var="program" key="muser.logindtls" />
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column span="2">
											<div class="legend info-message" id="spanPasswordPolicy"></div>
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="epwdreset.createpassword" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:password property="createPassword" id="createPassword" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="epwdreset.confirmpassword" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:password property="confirmPassword" id="confirmPassword" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="form.rolecode" var="common" mandatory="true" />
										</web:column>
										<web:column>
											<type:code property="roleCode" id="roleCode" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="form.remarks" var="common" />
										</web:column>
										<web:column>
											<type:remarks property="remarks" id="remarks" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column span="2">
											<web:submitReset />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
						</web:sectionBlock>
					</web:dividerBlock>
				</web:viewContent>
				<web:element property="userPolicy" />
				<web:element property="hidminUserLength" />
				<web:element property="command" />
				<web:element property="action" />
				<web:element property="pwdDelivery" />
				<web:element property="pwdDeliveryType" />
				<web:element property="minLength" />
				<web:element property="minAlpha" />
				<web:element property="minNumeric" />
				<web:element property="minSpecial" />
				<web:element property="passwordPolicy" />
				<web:element property="randomSalt" />
				<web:element property="loginNewPasswordHashed" />
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
	<web:queryGrid />
	<web:TBAGrid />
	<web:contextSearch />
</web:fragment>