var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'EPGMCTL';
function init() {
	refreshTBAGrid();
	refreshQueryGrid();
	loadViewQuery();
	//hide('po_view1');
	show('po_view2', $('#po_view2_pic'));
}
function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'EPGMCTL_MAIN', queryGrid);
}
function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}
function clearFields() {
	$('#optionID').val(EMPTY_STRING);
	$('#optionID_error').html(EMPTY_STRING);
	$('#optionID_desc').html(EMPTY_STRING);
	clearNonPKFields();
}
function clearNonPKFields() {
	setCheckbox('authReqd', NO);
	setCheckbox('doubleAuthReqd', NO);

}
function modify() {

	$('#optionID').focus();
}
function loadData() {
	$('#optionID').val(validator.getValue('MPGM_ID'));
	$('#optionID_desc').html(validator.getValue('F1_MPGM_DESCN'));
	setCheckbox('authReqd', validator.getValue('MPGM_AUTH_REQD'));
	if ($('#authReqd').is(':checked')) {
		$('#doubleAuthReqd').removeAttr("disabled");
		setCheckbox('doubleAuthReqd', validator.getValue('MPGM_DBL_AUTH_REQD'));
	} else {
		$('#doubleAuthReqd').attr("disabled", "disabled");
		$('#doubleAuthReqd').prop('checked', false);
	}
	resetLoading();
}

function doHelp(id) {
	switch (id) {
	case 'optionID':
		help(CURRENT_PROGRAM_ID, 'HLP_OPTIONS', $('#optionID').val(), EMPTY_STRING, $('#optionID'));

	}
}

function view(source, primaryKey) {
	hideParent('access/qpgmctl', source, primaryKey);
	resetLoading();
}
function validate(id) {
	switch (id) {
	case 'optionID':
		optionID_val();
		break;
	case 'authReqd':
		authReqd_val();
		break;
	case 'doubleAuthReqd':
		doubleAuthReqd_val();
		break;
	}
}

function backtrack(id) {
	switch (id) {
	case 'optionID':
		setFocusLast('optionID');
		break;
	case 'authReqd':
		setFocusLast('optionID');
		break;
	case 'doubleAuthReqd':
		setFocusLast('authReqd');
		break;
	}
}
function authReqd_val() {
	if ($('#authReqd').is(':checked')) {
		$('#doubleAuthReqd').prop("disabled", false);
		$('#doubleAuthReqd_desc').html(EMPTY_STRING);
		setFocusLast('doubleAuthReqd');
	} else {
		$('#doubleAuthReqd').prop("disabled", true);
		setCheckbox('doubleAuthReqd',NO);
		$('#doubleAuthReqd_desc').html(EMPTY_STRING);
		setFocusOnSubmit();
	}

	return true;
}
function doubleAuthReqd_val() {
	setFocusOnSubmit();
	return true;
}
function optionID_val() {
	var value = $('#optionID').val();
	clearError('optionID_error');
	if (isEmpty(value)) {
		setError('optionID_error', MANDATORY);
		setFocusLast('optionID');
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('PGM_ID', value);
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.access.epgmctlbean');
		validator.setMethod('validateOptionID');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('optionID_error', validator.getValue(ERROR));
			setFocusLast('optionID');
			return false;
		} else {

			$('#optionID_desc').html(validator.getValue("MPGM_DESCN"));
			if ($('#action').val() == MODIFY) {
				PK_VALUE = $('#optionID').val();
				if (!loadPKValues(PK_VALUE, 'optionID_error')) {
					return false;
				}
				setFocusLast('authReqd');
				return true;
			}
		}
	}
	setFocusLast('authReqd');
	return true;
}

function checkclick(id) {
	switch (id) {
	case 'authReqd':
		authReqd_val();
		break;
	}
}
function change(id) {
	switch (id) {
	case 'doubleAuthReqd':
		doubleAuthReqd_val();
		break;
	}
}
