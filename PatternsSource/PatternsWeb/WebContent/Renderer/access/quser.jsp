<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.access.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/access/quser.js" />
	</web:dependencies>
	<web:loadPK />
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="quser.querytitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:viewContent id="po_view1">
				<web:dividerBlock>
					<web:sectionBlock width="width-100p" align="left">
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="180px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowOdd>
									<web:column>
										<web:legend key="form.userid" var="common" mandatory="true" />
									</web:column>
									<web:column>
										<type:userDisplay property="userID" id="userID" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="muser.empcode" var="program" />
									</web:column>
									<web:column>
										<type:employeeCodeDisplay property="empCode" id="empCode" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:viewContent id="personDetails" styleClass="hidden">
							<web:section>
								<web:viewTitle var="program" key="muser.empdtls" />
							</web:section>
						</web:viewContent>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="180px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowOdd>
									<web:column>
										<web:legend key="muser.titlename" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:nameDisplay property="titleName" id="titleName" />
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="180px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="muser.dateofjoin" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:dateDisplay property="dateOfJoin" id="dateOfJoin" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="180px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowOdd>
									<web:column>
										<web:legend key="muser.mobilenumber" var="program" />
									</web:column>
									<web:column>
										<type:mobileDisplay property="mobileNumber" id="mobileNumber" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="muser.emailid" var="program" />
									</web:column>
									<web:column>
										<type:emailDisplay property="emailID" id="emailID" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="muser.branchcode" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:codeDisplay property="branchCode" id="branchCode" />
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
						<web:section>
							<web:viewTitle var="program" key="muser.logindtls" />
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="180px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column span="2">
										<div class="legend info-message" id="spanPasswordPolicy"></div>
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="epwdreset.createpassword" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:passwordDisplay property="createPassword" id="createPassword" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="epwdreset.confirmpassword" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:passwordDisplay property="confirmPassword" id="confirmPassword" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="form.rolecode" var="common" mandatory="true" />
									</web:column>
									<web:column>
										<type:codeDisplay property="roleCode" id="roleCode" />
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="180px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="form.remarks" var="common" />
									</web:column>
									<web:column>
										<type:remarksDisplay property="remarks" id="remarks" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:auditDisplay />
					</web:sectionBlock>
				</web:dividerBlock>
			</web:viewContent>
		</web:viewSubPart>
	</web:viewPart>
</web:fragment>
