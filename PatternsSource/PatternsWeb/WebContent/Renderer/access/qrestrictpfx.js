var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'IRESTRICTPFX';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}
function clearFields() {
	$('#effectiveDate').val(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	$('#pfxRestrictReq').prop('checked',false);

}
function loadData() {
	$('#effectiveDate').val(validator.getValue('EFFT_DATE'));
	$('#pfxRestrictReq').prop('checked',decodeSTB(validator.getValue('PFX_RESTRICT_REQD')));
	$('#remarks').val(validator.getValue('REMARKS'));
	if ($('#pfxRestrictReq').is(':checked')) {
		loadGrid();
	}
	loadAuditFields(validator);
}

function loadGrid() {
	SOURCE_VALUE = _tableType;
	PK_VALUE = _primaryKey;
	loadGridQuery(CURRENT_PROGRAM_ID, 'IRESTRICTPFX_GRID', innerGrid);
}