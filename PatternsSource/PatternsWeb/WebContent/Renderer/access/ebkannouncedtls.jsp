<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.access.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/access/ebkannouncedtls.js" />
	</web:dependencies>
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="ebkannouncedtls.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/access/ebkannouncedtls" id="ebkannouncedtls" method="POST">
				<web:dividerBlock>
					<type:optionBox add="true" modify="true" view="true" />
				</web:dividerBlock>
				<web:viewContent id="po_view1" styleClass="hidden">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="ebkannouncedtls.announcementid" var="program" />
										</web:column>
										<web:column>
											<type:announcementID property="announceID" id="announceID" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="form.startdate" var="common" mandatory="true" />
										</web:column>
										<web:column>
											<type:date property="startDate" id="startDate" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="form.expirydate" var="common" mandatory="true" />
										</web:column>
										<web:column>
											<type:date property="expiryDate" id="expiryDate" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="form.displaytitle" var="common" mandatory="true" />
										</web:column>
										<web:column>
											<type:remarks property="displayTitle" id="displayTitle" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="form.externallink" var="common" />
										</web:column>
										<web:column>
											<type:checkbox property="externalLink" id="externalLink" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="form.url" var="common" />
										</web:column>
										<web:column>
											<type:url property="url" id="url" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="form.urltitle" var="common" />
										</web:column>
										<web:column>
											<type:remarks property="urlTitle" id="urlTitle" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="form.visibility" var="common" mandatory="true" />
										</web:column>
										<web:column>
											<type:combo property="visibility" id="visibility" datasourceid="COMMON_VISIBILITY" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="form.disabled" var="common" />
										</web:column>
										<web:column>
											<type:checkbox property="announceDisabled" id="announceDisabled" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="form.userid" var="common" />
										</web:column>
										<web:column>
											<type:userHelp property="userID" id="userID" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="form.roletype" var="common" />
										</web:column>
										<web:column>
											<type:combo property="roleType" id="roleType" datasourceid="COMMON_ROLES" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="form.customercode" var="common" />
										</web:column>
										<web:column>
											<type:customerCodeHelp property="customerCode" id="customerCode" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="form.branchcode" var="common" />
										</web:column>
										<web:column>
											<type:code property="branchCode" id="branchCode" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="form.remarks" var="common" mandatory="true" />
										</web:column>
										<web:column>
											<type:remarks property="remarks" id="remarks" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column span="2">
											<web:submitReset />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
						</web:sectionBlock>
					</web:dividerBlock>
				</web:viewContent>
				<web:element property="command" />
				<web:element property="action" />
				<web:element property="tempAnnounceId" />
				<web:element property="temVisibility" />
				<web:element property="tempRoleType" />
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
	<web:queryGrid />
	<web:contextSearch />
	<web:TBAGrid />
</web:fragment>
