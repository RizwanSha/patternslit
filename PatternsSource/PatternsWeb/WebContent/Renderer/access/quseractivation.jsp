<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.access.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/access/quseractivation.js" />
	</web:dependencies>
	<web:loadPK />
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="quseractivation.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:viewContent id="po_view1">
				<web:dividerBlock>
					<web:sectionBlock width="width-100p" align="left">
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="180px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowOdd>
									<web:column>
										<web:legend key="form.userid" var="common" />
									</web:column>
									<web:column>
										<type:userDisplay property="userID" id="userID" />
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="180px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="euseractivation.useractivationstatus" var="program" />
									</web:column>
									<web:column>
										<type:comboDisplay property="userActivationStatus" id="userActivationStatus" datasourceid="COMMON_ACTIVATIONSTATUS" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="euseractivation.intimationmode" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:comboDisplay property="intimationMode" id="intimationMode" datasourceid="COMMON_INTIMATIONMODE" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="euseractivation.referenceNo" var="program" mandatory="false" />
									</web:column>
									<web:column>
										<type:twentyFiveDnumberDisplay property="intimationReferenceNo" id="intimationReferenceNo" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="euseractivation.intimationdate" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:dateDisplay property="intimationDate" id="intimationDate" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="form.remarks" var="common" mandatory="true" />
									</web:column>
									<web:column>
										<type:remarksDisplay property="remarks" id="remarks" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:auditDisplay />
					</web:sectionBlock>
				</web:dividerBlock>
			</web:viewContent>
		</web:viewSubPart>
	</web:viewPart>
</web:fragment>