var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'EUSERSTATUS';
function init() {
	refreshTBAGrid();
	refreshQueryGrid();
	loadViewQuery();
	show('po_view2', $('#po_view2_pic'));
	$('#currentUserStatus').prop('disabled', true);
}
function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'EUSERSTATUS_MAIN', getUserID());
}
function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}
function clearFields() {

	$('#userID').val(EMPTY_STRING);
	$('#userID_desc').html(EMPTY_STRING);
	$('#userID_error').html(EMPTY_STRING);
	$('#currentUserStatus').val(EMPTY_STRING);
	$('#currentUserStatus_error').html(EMPTY_STRING);
	$('#status').val(EMPTY_STRING);
	$('#status_error').html(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
}

function doclearfields(id) {
	switch (id) {
	case 'userID':
		if (isEmpty($('#userID').val())) {
			clearFields();

		}
		break;
	}
}

function modify() {

	$('#userID').focus();
}

function doHelp(id) {
	switch (id) {
	case 'userID':
		help('EUSERROLEALLOC', 'HLP_USERS_M', $('#userID').val(), getUserID(),
				$('#userID'));
		break;
	}
}

function loadData() {
	$('#userID').val(validator.getValue('USER_ID'));
	$('#userID_desc').html(validator.getValue('USER_NAME'));
	// $('#status').val(validator.getValue('STATUS'));
	$('#currentUserStatus').val(validator.getValue('STATUS'));
	$('#currentUserStatus').prop("disabled", true);
	$('#remarks').val(validator.getValue('REMARKS'));
	//$('#tmpCurrentUserStatus').val(validator.getValue('STATUS'));
	resetLoading();
}
function view(source, primaryKey) {
	hideParent('access/quserstatus', source, primaryKey);
	resetLoading();
}
function validate(id) {
	switch (id) {
	case 'userID':
		userID_val();
		break;
	case 'currentUserStatus':
		currentUserStatus_val();
		break;
	case 'status':
		status_val();
		break;
	case 'remarks':
		remarks_val();
		break;
	}
}

function backtrack(id) {
	switch (id) {
	case 'userID':
		setFocusLast('userID');
		break;
	case 'currentUserStatus':
		setFocusLast('userID');
		break;
	case 'status':
		if ($('#currentUserStatus').prop("disabled")) {
			setFocusLast('userID');
		} else {
			setFocusLast('currentUserStatus');
		}

		break;
	case 'remarks':
		setFocusLast('status');
		break;
	}
}

function userID_val() {
	var value = $('#userID').val();
	clearError('userID_error');
	if (isEmpty(value)) {
		setError('userID_error', MANDATORY);
		return false;
	}
	if (value == getUserID()) {
		setError('userID_error', CANNOT_ALLOCATE_TO_SELF);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('USER_ID', $('#userID').val());
		validator.setValue(ACTION, $('#action').val());
		validator.setClass('patterns.config.web.forms.access.euserstatusbean');
		validator.setMethod('validateUserId');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('userID_error', validator.getValue(ERROR));
			setFocusLast('userID');
			return false;
		} else {
			$('#userID_desc').html(validator.getValue('USER_NAME'));
			$('#currentUserStatus').val(validator.getValue('STATUS'));
			setFocusLast('status');
			return true;
		}
	}
	return true;
}

function currentUserStatus_val() {
	var value = $('#currentUserStatus').val();
	clearError('currentUserStatus_error');
	if (isEmpty(value)) {
		setError('currentUserStatus_error', MANDATORY);
		setFocusLast('currentUserStatus');
		return false;
	}
	setFocusLast('status');
	return true;
}
function status_val() {
	var value = $('#status').val();
	clearError('status_error');
	if (isEmpty(value)) {
		setError('status_error', MANDATORY);
		setFocusLast('status');
		return false;
	}
	if (value== $('#currentUserStatus').val()) {
		setError('status_error', CURR_STATUS_NOT_ALLOWED);
		return false;
	}
	setFocusLast('remarks');
	return true;
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (isEmpty(value)) {
		setError('remarks_error', HMS_MANDATORY);
		setFocusLast('remarks');
		return false;
	}

	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', HMS_INVALID_REMARKS);
			setFocusLast('remarks');
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', HMS_INVALID_REMARKS);
			setFocusLast('remarks');
			return false;
		}
	}

	setFocusOnSubmit();
	return true;

}

function revalidate(){
	$('#currentUserStatus').prop('disabled', false);
}