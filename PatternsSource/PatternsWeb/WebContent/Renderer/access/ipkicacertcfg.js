var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'IPKICACERTCFG';
function init() {
	doPurpose(FILE_PURPOSE_ROOT_CERTIFICATE);
	refreshQueryGrid();
	refreshTBAGrid();	
}
function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'IPKICACERTCFG_MAIN', queryGrid);
}
function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}
function clearFields() {
	
	$('#rootCaCode').val(EMPTY_STRING);
	$('#rootCaCode_desc').html(EMPTY_STRING);	
	$('#rootCaCode_error').html(EMPTY_STRING);	
	$('#effectiveDate').val(EMPTY_STRING);
	$('#effectiveDate_error').html(EMPTY_STRING);
	$('#revoked').prop('checked',false);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
	clearFileDetails();
}
function clearFileDetails() {
	$('#fileInventoryNumber').val(EMPTY_STRING);
	$('#fileName').val(EMPTY_STRING);
}
function add() {
	
	$('#rootCaCode').focus();
	enablingFields();
	$('#fileExtensionList').val(CER_FILE_EXTENSION);
}
function modify() {
	
	$('#rootCaCode').focus();
	enablingFields();
	$('#fileExtensionList').val(CER_FILE_EXTENSION);
}
function loadData() {
	$('#rootCaCode').val(validator.getValue('CA_CODE'));
	$('#rootCaCode_desc').html(validator.getValue('PKICERTAUTH_DESCRIPTION'));
	$('#effectiveDate').val(validator.getValue('EFFT_DATE'));
	$('#revoked').prop('checked',decodeSTB(validator.getValue('REVOKED')));
	if($('#revoked').is(':checked')){
		$('#cmdFileSelector').attr("disabled", "disabled"); 
		$('#cmdPreview').attr("disabled", "disabled"); 
	}
	else{
		$('#cmdFileSelector').removeAttr("disabled"); 
		$('#cmdPreview').removeAttr("disabled"); 
	}
	$('#remarks').val(validator.getValue('REMARKS'));
	$('#fileInventoryNumber').val(validator.getValue('INV_NUM'));
	loadFileName();
	resetLoading();
}
function loadFileName(){
	var value = $('#fileInventoryNumber').val();
	validator.reset();
	validator.setClass('patterns.config.web.forms.access.eusertfaregbean');
	validator.setValue('CERT_INV_NUM', value);
	validator.setMethod('fetchFileName');
	validator.sendAndReceiveAsync(loadAdditionalData);
}
function doFilePreview(){
	if(fileInventoryNumber_val()){
	var primaryKey = getEntityCode() + '|' + $('#fileInventoryNumber').val() ;
	showWindow('PREVIEW_VIEW',getBasePath()+ 'Renderer/common/qcertificateinfo.jsp',CERTIFICATE_VIEW_TITLE,PK+'='+primaryKey,true,false,false);
	resetLoading();
	}

}
function loadAdditionalData() {
	if (validator.getValue(RESULT) == DATA_AVAILABLE) {
	$('#fileName').val(validator.getValue('CERT_FILE_NAME'));
	}
}
function view(source, primaryKey) {
	hideParent('access/qpkicacertcfg', source, primaryKey);
	resetLoading();
}
function doHelp(id) {
	switch (id) {
	case 'rootCaCode':
		help(CURRENT_PROGRAM_ID, 'CACODES', $('#rootCaCode').val(), EMPTY_STRING, $('#rootCaCode'));
		break;
	}
}
function validate(id) {
	switch (id) {
	case 'rootCaCode':
		rootCaCode_val();
		break;
	case 'effectiveDate':
		effectiveDate_val();
		break;	
	case 'remarks':
		remarks_val();
		break;
	case 'fileInventoryNumber':
		fileInventoryNumber_val();
		break;
	}
}
function checkclick(id) {
	switch (id) {
	case 'revoked':
		enablingFields();
		break;
	}
}
function enablingFields(){
	if ($('#revoked').is(':checked')){
		clearError('fileInventoryNumber_error');
		clearError('rootCaCode_error');
		$('#rootCaCode_desc').html(EMPTY_STRING);
		$('#rootCaCode_pic').attr("disabled", "disabled"); 
		$('#fileName').val(EMPTY_STRING);
		$('#fileInventoryNumber').val(EMPTY_STRING);
		$('#rootCaCode').val(EMPTY_STRING);
		$('#rootCaCode').attr("disabled", "disabled"); 
		$('#cmdFileSelector').attr("disabled", "disabled"); 
		$('#cmdPreview').attr("disabled", "disabled"); 
		$('#remarks').focus();
	}
	else{
		$('#rootCaCode').removeAttr("disabled"); 
		$('#rootCaCode_pic').removeAttr("disabled"); 
		$('#cmdFileSelector').removeAttr("disabled"); 
		$('#cmdPreview').removeAttr("disabled"); 
	  }
}
function rootCaCode_val() {
	var value = $('#rootCaCode').val();
	clearError('rootCaCode_error');
	if (isEmpty(value)) {
		setError('rootCaCode_error', MANDATORY);
		return false;
	}
	return true;
}
function effectiveDate_val() {
	var value = $('#effectiveDate').val();
	clearError('effectiveDate_error');
	if(!isValidEffectiveDate(value,'effectiveDate_error')){
		return false;
	}
	return true;
}	
function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (isEmpty(value)) {
		setError('remarks_error', MANDATORY);
		return false;
	}
	if (!isValidRemarks(value)) {
		setError('remarks_error', INVALID_REMARKS);
		return false;
	}
	return true;
}
function fileInventoryNumber_val() {
	if(!$('#revoked').is(':checked')){
	var value = $('#fileInventoryNumber').val();
	clearError('fileInventoryNumber_error');
	if (isEmpty(value)) {
		setError('fileInventoryNumber_error', MANDATORY);
		return false;
	   }
	}
	return true;
}
function revalidate() {
	if (!rootCaCode_val()) {
		errors++;
	}
	if (!fileInventoryNumber_val()) {
		errors++;
	}
	if (!effectiveDate_val()) {
		errors++;
	}	
	if (!remarks_val()) {
		errors++;
	}
}