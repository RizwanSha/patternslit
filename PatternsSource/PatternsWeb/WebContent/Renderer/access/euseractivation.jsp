<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.access.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/access/euseractivation.js" />
	</web:dependencies>
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="euseractivation.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/access/euseractivation" id="euseractivation" method="POST">
				<web:dividerBlock>
					<type:optionBox add="false" modify="true" view="true" />
				</web:dividerBlock>
				<web:viewContent id="po_view1" styleClass="hidden">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column span="2">
											<div class="legend info-message" id="spanActivationPolicy"></div>
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="form.userid" var="common" />
										</web:column>
										<web:column>
											<type:user property="userID" id="userID" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="euseractivation.useractivationstatus" var="program" />
										</web:column>
										<web:column>
											<type:comboDisplay property="userActivationStatus" id="userActivationStatus" datasourceid="COMMON_ACTIVATIONSTATUS" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="euseractivation.intimationmode" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:combo property="intimationMode" id="intimationMode" datasourceid="COMMON_INTIMATIONMODE" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="euseractivation.referenceNo" var="program" mandatory="false" />
										</web:column>
										<web:column>
											<type:twentyFiveDnumber property="intimationReferenceNo" id="intimationReferenceNo" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="euseractivation.intimationdate" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:date property="intimationDate" id="intimationDate" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="form.remarks" var="common" mandatory="true" />
										</web:column>
										<web:column>
											<type:remarks property="remarks" id="remarks" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column span="2">
											<web:submitReset />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
						</web:sectionBlock>
					</web:dividerBlock>
				</web:viewContent>
				<web:element property="command" />
				<web:element property="action" />
				<web:element property="userActivation" />
				<web:element property="tmpActivationStatus" />
				<web:element property="activationPolicy" />
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
	<web:contextSearch />
	<web:queryGrid />
	<web:TBAGrid />
</web:fragment>