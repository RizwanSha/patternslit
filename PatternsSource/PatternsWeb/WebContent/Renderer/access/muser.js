var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MUSER';
var roleType = getRoleType();
var hidePrefix = EMPTY_STRING;
var pwdDeliveryChoice = EMPTY_STRING;
var isUserAlreadyLoggedIn = false;
function init() {
	$('#spanUserPolicy').html($('#userPolicy').val());
	refreshQueryGrid();
	refreshTBAGrid();
	if (_redisplay) {
		if ($('#action').val() == MODIFY) {
			$('#roleCode').prop("readonly", true);
			$('#roleCode_pic').prop("disabled", true);
			$('#createPassword').val(EMPTY_STRING);
			$('#createPassword_desc').html(EMPTY_STRING);
			$('#createPassword_error').html(EMPTY_STRING);
			$('#createPassword').attr('readonly', true);
			$('#confirmPassword').val(EMPTY_STRING);
			$('#confirmPassword_error').html(EMPTY_STRING);
			$('#confirmPassword').attr('readonly', true);
			empCode_val(false);
			userLoginCheck();
			if (isUserAlreadyLoggedIn) {
				$('#empCode').prop("readonly", true);
				$('#empCode_pic').prop("disabled", true);
			} else {
				$('#empCode').prop("readonly", false);
				$('#empCode_pic').prop("disabled", false);
			}
		} else {
			if ($('#pwdDelivery').val() == 4 && $('#pwdDeliveryType').val() == 2) {
				$('#spanPasswordPolicy').html($('#passwordPolicy').val());
				$('#createPassword').val(EMPTY_STRING);
				$('#createPassword_error').html(EMPTY_STRING);
				$('#confirmPassword').val(EMPTY_STRING);
				$('#confirmPassword_error').html(EMPTY_STRING);
			}
		}
		setFocusLast('userID');
	}
}

function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'MUSER_ADMIN', queryGrid);
}

function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}

function doHelp(id) {
	switch (id) {
	case 'userID':
		help('COMMON', 'HLP_USERS_M', $('#userID').val(), EMPTY_STRING, $('#userID'));
		break;
	case 'empCode':
		help('COMMON', 'HLP_STAFF_CODE', $('#empCode').val(), EMPTY_STRING, $('#empCode'));
		break;
	case 'branchCode':
		help('COMMON', 'HLP_BRANCH_CODE', $('#branchCode').val(), EMPTY_STRING, $('#branchCode'));
		break;
	case 'roleCode':
		if ($('#action').val() == ADD) {
			help('COMMON', 'ROLES_INTERNAL', $('#roleCode').val(), EMPTY_STRING, $('#roleCode'));
		}
		break;
	}
}

function doclearfields(id) {
	switch (id) {
	case 'userID':
		if (isEmpty($('#userID').val())) {
			$('#userID_desc').html(EMPTY_STRING);
			$('#userID_error').html(EMPTY_STRING);
			clearNonPKFields();
		}
		break;
	case 'userID':
		if (isEmpty($('#empCode').val())) {
			clearEmployeeRelateFields()
		}
		break;
	}
}

function clearFields() {
	$('#userID').val(EMPTY_STRING);
	$('#userID_error').html(EMPTY_STRING);
	$('#userID_desc').html(EMPTY_STRING);
	clearNonPKFields();
}

function clearNonPKFields() {
	$('#empCode').val(EMPTY_STRING);
	$('#empCode_desc').html(EMPTY_STRING);
	$('#empCode_error').html(EMPTY_STRING);
	$('#titleName').val(EMPTY_STRING);
	$('#titleName_error').html(EMPTY_STRING);
	$('#roleCode').val(EMPTY_STRING);
	$('#roleCode_desc').html(EMPTY_STRING);
	$('#roleCode_error').html(EMPTY_STRING);
	$('#dateOfJoin').val(EMPTY_STRING);
	$('#dateOfJoin_error').html(EMPTY_STRING);
	$('#emailID').val(EMPTY_STRING);
	$('#emailID_error').html(EMPTY_STRING);
	$('#branchCode').val(EMPTY_STRING);
	$('#branchCode_desc').html(EMPTY_STRING);
	$('#branchCode_error').html(EMPTY_STRING);
	$('#mobileNumber').val(EMPTY_STRING);
	$('#mobileNumber_error').html(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
	$('#createPassword').val(EMPTY_STRING);
	$('#createPassword_error').html(EMPTY_STRING);
	$('#confirmPassword').val(EMPTY_STRING);
	$('#confirmPassword_error').html(EMPTY_STRING);
	employeeNotReadOnlyFields();
	clearEmployeeRelateFields();
}

function add() {
	setFocusLast('userID');
	// show('personDetails');
	$('#roleCode').prop("readonly", false);
	$('#roleCode_pic').prop("disabled", false);
	if ($('#pwdDelivery').val() == 4 && $('#pwdDeliveryType').val() == 2) {
		$('#spanPasswordPolicy').html($('#passwordPolicy').val());
		$('#createPassword').val(EMPTY_STRING);
		$('#createPassword_error').html(EMPTY_STRING);
		$('#createPassword').attr('readonly', false);
		$('#confirmPassword').val(EMPTY_STRING);
		$('#confirmPassword_error').html(EMPTY_STRING);
		$('#confirmPassword').attr('readonly', false);
	} else {
		$('#spanPasswordPolicy').html(HMS_PASSWORD_SYS_GEN);
		$('#createPassword').val(EMPTY_STRING);
		$('#createPassword_desc').html(EMPTY_STRING);
		$('#createPassword_error').html(EMPTY_STRING);
		$('#createPassword').attr('readonly', true);
		$('#confirmPassword').val(EMPTY_STRING);
		$('#confirmPassword_error').html(EMPTY_STRING);
		$('#confirmPassword').attr('readonly', true);
	}
}

function modify() {
	setFocusLast('userID');
	// show('personDetails');
	if ($('#action').val() == MODIFY) {
		$('#roleCode').prop("readonly", true);
		$('#roleCode_pic').prop("disabled", true);
		$('#createPassword').val(EMPTY_STRING);
		$('#createPassword_desc').html(EMPTY_STRING);
		$('#createPassword_error').html(EMPTY_STRING);
		$('#createPassword').attr('readonly', true);
		$('#confirmPassword').val(EMPTY_STRING);
		$('#confirmPassword_error').html(EMPTY_STRING);
		$('#confirmPassword').attr('readonly', true);
		$('#spanPasswordPolicy').html(EMPTY_STRING);
	}
}

function loadData() {
	$('#userID').val(validator.getValue('USER_ID'));
	$('#userName').val(validator.getValue('USER_NAME'));
	$('#empCode').val(validator.getValue('EMP_CODE'));
	$('#roleCode').val(validator.getValue('ROLE_CODE'));
	$('#roleCode_desc').html(validator.getValue('F1_DESCRIPTION'));
	$('#remarks').val(validator.getValue('REMARKS'));
	if (isEmpty($('#empCode').val())) {
		$('#titleName').val(validator.getValue("USER_NAME"));
		$('#dateOfJoin').val(validator.getValue("DATE_OF_JOINING"));
		$('#mobileNumber').val(validator.getValue("MOBILE_NUMBER"));
		$('#emailID').val(validator.getValue("EMAIL_ID"));
		$('#branchCode').val(validator.getValue('BRANCH_CODE'));
		$('#branchCode_desc').html(validator.getValue('F3_BRANCH_NAME'));
		employeeNotReadOnlyFields();

	} else {
		$('#titleName').val(validator.getValue("F2_NAME"));
		$('#dateOfJoin').val(validator.getValue("F2_DATE_OF_JOINING"));
		$('#mobileNumber').val(validator.getValue("F2_MOBILE_NO"));
		$('#emailID').val(validator.getValue("F2_EMAIL_ID"));
		employeeReadOnlyFields();
	}

	// if (isEmpty($('#empCode').val())) {
	// hide('personDetails');
	// $('#dateOfJoin').prop('readonly', false);
	// $('#dateOfJoin_pic').prop('disabled', false);
	// $('#emailID').prop('readonly', false);
	// $('#mobileNumber').prop('readonly', false);
	// } else {
	// show('personDetails');
	// $('#dateOfJoin').prop('readonly', true);
	// $('#dateOfJoin_pic').prop('disabled', true);
	// $('#emailID').prop('readonly', true);
	// $('#mobileNumber').prop('readonly', true);
	// }
	// userLoginCheck();
	// if (isUserAlreadyLoggedIn) {
	// $('#empCode').prop("readonly", true);
	// $('#empCode_pic').prop("disabled", true);
	// } else {
	// $('#empCode').prop("readonly", false);
	// $('#empCode_pic').prop("disabled", false);
	// }
	// if (isEmpty($('#empCode').val())) {
	// $('#empCode').prop("readonly", true);
	// $('#empCode_pic').prop("disabled", true);
	// } else {
	// $('#empCode').prop("readonly", false);
	// $('#empCode_pic').prop("disabled", false);
	// }
	resetLoading();
}

function view(source, primaryKey) {
	hideParent('access/quser', source, primaryKey);
	resetLoading();
}

function validate(id) {
	valMode = true;
	switch (id) {
	case 'userID':
		userID_val();
		break;
	case 'createPassword':
		createPassword_val(true);
		break;
	case 'confirmPassword':
		confirmPassword_val(true);
		break;
	case 'titleName':
		titleName_val();
		break;
	case 'dateOfJoin':
		dateOfJoin_val();
		break;
	case 'empCode':
		empCode_val(valMode);
		break;
	case 'roleCode':
		roleCode_val();
		break;
	case 'emailID':
		emailID_val();
		break;
	case 'branchCode':
		branchCode_val();
		break;
	case 'mobileNumber':
		mobileNumber_val();
		break;
	case 'remarks':
		remarks_val();
		break;
	}
}

function backtrack(id) {
	switch (id) {
	case 'userID':
		setFocusLast('userID');
		break;
	case 'empCode':
		setFocusLast('userID');
		break;
	case 'titleName':
		setFocusLast('empCode');
		break;
	case 'dateOfJoin':
		setFocusLast('titleName');
		break;
	case 'mobileNumber':
		setFocusLast('dateOfJoin');
		break;
	case 'emailID':
		setFocusLast('mobileNumber');
		break;
	// case 'createPassword':
	// if (!$('#mobileNumber').prop('readonly'))
	// setFocusLast('mobileNumber');
	// else if (!$('#emailID').prop('readonly'))
	// setFocusLast('emailID');
	// else if (!$('#dateOfJoin').prop('readonly'))
	// setFocusLast('dateOfJoin');
	// else
	// setFocusLast('userName');
	// break;
	// case 'confirmPassword':
	// setFocusLast('createPassword');
	// break;
	case 'roleCode':
		if (!isEmpty($('#empCode').val())) {
			setFocusLast('empCode');
		} else {
			setFocusLast('branchCode');
		}
		break;
	case 'remarks':
		if (isEmpty($('#empCode').val())) {
			setFocusLast('emailID');
		} else {
			setFocusLast('empCode');
		}
		break;
	}
}

function userID_val() {
	var minUserLength = $('#hidminUserLength').val();
	var value = $('#userID').val();
	var userIDLen = value.length;
	clearError('userID_error');
	$('#userID_desc').html(EMPTY_STRING);
	if (isEmpty(value)) {
		setError('userID_error', HMS_MANDATORY);
		setFocusLast('userID');
		return false;
	}
	if ($('#action').val() == ADD) {
		if (userIDLen < minUserLength) {
			setError('userID_error', INVALID_USERID_LENGTH);
			return false;
		} else {
			if (!isValidCode(value, LENGTH_50)) {
				setError('userID_error', HMS_INVALID_FORMAT);
				return false;
			}
		}
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('USER_ID', $('#userID').val());
	validator.setValue('MIN_LENGTH', $('#hidminUserLength').val());
	validator.setValue(ACTION, $('#action').val());
	validator.setClass('patterns.config.web.forms.access.muserbean');
	validator.setMethod('validateUser');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('userID_error', validator.getValue(ERROR));
		return false;
	} else {
		if ($('#action').val() == MODIFY) {
			PK_VALUE = getPartitionNo() + PK_SEPERATOR + $('#userID').val();
			if (!loadPKValues(PK_VALUE, 'userID_error')) {
				return false;
			}
		}
	}
	// if ($('#action').val() == MODIFY && isUserAlreadyLoggedIn)
	// setFocusLast('userName');
	setFocusLast('empCode');
	return true;
}

function userLoginCheck() {
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('USER_ID', $('#userID').val());
	validator.setClass('patterns.config.web.forms.access.muserbean');
	validator.setMethod('validateUserLoginCheck');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('userID_error', validator.getValue(ERROR));
		return false;
	} else {
		isUserAlreadyLoggedIn = decodeSTB(validator.getValue('ALREADY_LOGIN'));
	}
	return true;
}

function createPassword_val(valMode) {
	var value = $('#createPassword').val();
	clearError('createPassword_error');
	if ($('#pwdDelivery').val() == 4 && $('#pwdDeliveryType').val() == 2) {
		if (isEmpty(value)) {
			setError('createPassword_error', MANDATORY);
			return false;
		}
		if (!checkPasswordCriteria(value, $('#minLength').val(), $('#minAlpha').val(), $('#minNumeric').val(), $('#minSpecial').val())) {
			setError('createPassword_error', PASSWORD_POLICY);
			return false;
		}
	}
	if (valMode)
		setFocusLast('confirmPassword');
	return true;
}

function confirmPassword_val(valMode) {
	var value = $('#confirmPassword').val();
	clearError('confirmPassword_error');
	if ($('#pwdDelivery').val() == 4 && $('#pwdDeliveryType').val() == 2) {
		if (isEmpty(value)) {
			setError('confirmPassword_error', MANDATORY);
			return false;
		}
		if ($('#createPassword').val() != $('#confirmPassword').val()) {
			setError('confirmPassword_error', PWD_SAME);
			return false;
		}
	}
	if (valMode) {
		setFocusLast('roleCode');
	}
	return true;
}

function empCode_val(valMode) {
	var value = $('#empCode').val();
	clearError('empCode_error');
	if (!isEmpty(value)) {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('STAFF_CODE', $('#empCode').val());
		validator.setValue('USER_ID', $('#userID').val());
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.access.muserbean');
		validator.setMethod('validateEmployeeCode');
		validator.sendAndReceive();
		if (validator.getValue(RESULT) == DATA_AVAILABLE) {
			$('#titleName').val(validator.getValue("NAME"));
			$('#dateOfJoin').val(validator.getValue("DATE_OF_JOINING"));
			$('#mobileNumber').val(validator.getValue("MOBILE_NO"));
			$('#emailID').val(validator.getValue("EMAIL_ID"));
			$('#branchCode').val(validator.getValue('BRANCH_CODE'));
			employeeReadOnlyFields();
			if (valMode)
				setFocusLast('roleCode');
			if ($('#action').val() == MODIFY) {
				setFocusLast('remarks');
			}
		} else if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('empCode_error', validator.getValue(ERROR));
			return false;
		}
	} else {
		employeeNotReadOnlyFields();
		setFocusLast('titleName');
		return true;
	}
}

function employeeReadOnlyFields() {
	$('#titleName').prop('readonly', true);
	$('#dateOfJoin').prop('readonly', true);
	$('#dateOfJoin_pic').prop('disabled', true);
	$('#mobileNumber').prop('readonly', true);
	$('#emailID').prop('readonly', true);
	$('#branchCode').prop('readonly', true);
}

function employeeNotReadOnlyFields() {
	$('#titleName').prop('readonly', false);
	$('#dateOfJoin').prop('readonly', false);
	$('#dateOfJoin_pic').prop('disabled', false);
	$('#mobileNumber').prop('readonly', false);
	$('#emailID').prop('readonly', false);
	$('#branchCode').prop('readonly', false);
}

function clearEmployeeRelateFields() {
	$('#titleName').val(EMPTY_STRING);
	$('#dateOfJoin').val(EMPTY_STRING);
	$('#mobileNumber').val(EMPTY_STRING);
	$('#emailID').val(EMPTY_STRING);
	$('#branchCode').val(EMPTY_STRING);
}

// function getPersonRelatedFields() {
// show('personDetails');
// $('#personIDDetails_id').val(validator.getValue('PERSON_ID'));
// $('#personIDDetails_title1').val(validator.getValue('TITLE_CODE1').trim());
// $('#personIDDetails_title2').val(validator.getValue('TITLE_CODE2').trim());
// $('#personIDDetails_name').val(validator.getValue('PERSON_NAME').trim());
// if ($('#action').val() == ADD) {
// if ($('#userName').val() == EMPTY_STRING)
// $('#userName').val(validator.getValue('PERSON_NAME'));
// if (!isEmpty(validator.getValue('DATE_OF_BIRTH'))) {
// $('#dateOfJoin').val(validator.getValue('DATE_OF_BIRTH').trim());
// $('#dateOfJoin').prop('readonly', true);
// $('#dateOfJoin_pic').prop('disabled', true);
// $('#dateOfJoin_error').html(EMPTY_STRING);
// } else {
// $('#dateOfJoin_error').html(EMPTY_STRING);
// $('#dateOfJoin').prop('readonly', false);
// $('#dateOfJoin_pic').prop('disabled', false);
// }
// if (!isEmpty(validator.getValue('EMAIL_ID'))) {
// $('#emailID').val(validator.getValue('EMAIL_ID').trim());
// $('#emailID').prop('readonly', true);
// $('#emailID_error').html(EMPTY_STRING);
// } else {
// $('#emailID').val(EMPTY_STRING);
// $('#emailID_error').html(EMPTY_STRING);
// $('#emailID').prop('readonly', false);
// }
// if (!isEmpty(validator.getValue('MOBILE_NO'))) {
// $('#mobileNumber').val(validator.getValue('MOBILE_NO').trim());
// $('#mobileNumber').prop('readonly', true);
// $('#mobileNumber_error').html(EMPTY_STRING);
// } else {
// $('#mobileNumber').val(EMPTY_STRING);
// $('#mobileNumber_error').html(EMPTY_STRING);
// $('#mobileNumber').prop('readonly', false);
// }
// }
// }

// function clearingPersonFieldRelated() {
// $('#dateOfJoin_error').html(EMPTY_STRING);
// $('#dateOfJoin').prop('readonly', false);
// $('#dateOfJoin_pic').prop('disabled', false);
// $('#emailID_error').html(EMPTY_STRING);
// $('#emailID').prop('readonly', false);
// $('#mobileNumber_error').html(EMPTY_STRING);
// $('#mobileNumber').prop('readonly', false);
// }

function titleName_val() {
	var value = $('#titleName').val();
	clearError('titleName_error');
	if (isEmpty(value)) {
		setError('titleName_error', MANDATORY);
		return false;
	}
	if (!isValidName(value)) {
		setError('titleName_error', INVALID_NAME);
		return false;
	}
	setFocusLast('dateOfJoin');
	return true;
}

function dateOfJoin_val() {
	var value = $('#dateOfJoin').val();
	clearError('dateOfJoin_error');
	if (isEmpty(value)) {
		setError('dateOfJoin_error', MANDATORY);
		return false;
	}
	if (!isValidEffectiveDate(value, 'dateOfJoin_error')) {
		return false;
	}
	setFocusLast('mobileNumber');
	return true;
}

function mobileNumber_val() {
	var value = $('#mobileNumber').val();
	clearError('mobileNumber_error');
	if (!isEmpty(value)) {
		if (!isPhoneNumber(value)) {
			setError('mobileNumber_error', INVALID_MOBILE_NUMBER);
			return false;
		}
	}
	// if ($('#action').val() == ADD) {
	// if ($('#pwdDelivery').val() == 4 && $('#pwdDeliveryType').val() == 2)
	// setFocusLast('createPassword');
	// else
	// setFocusLast('roleCode');
	// }
	setFocusLast('emailID');
	return true;
}

function emailID_val() {
	var value = $('#emailID').val();
	clearError('emailID_error');
	if (!isEmpty(value)) {
		if (!isEmail(value)) {
			setError('emailID_error', INVALID_EMAIL_ID);
			return false;
		}
	}
	// if (!$('#mobileNumber').prop('readonly')) {
	// setFocusLast('mobileNumber');
	// return true;
	// } else if ($('#action').val() == ADD) {
	// if ($('#pwdDelivery').val() == 4 && $('#pwdDeliveryType').val() == 2)
	// setFocusLast('createPassword');
	// else
	// setFocusLast('roleCode');
	// } else
	setFocusLast('branchCode');
	return true;
}

function branchCode_val() {
	var value = $('#branchCode').val();
	clearError('branchCode_error');
	if (isEmpty(value)) {
		setError('branchCode_error', MANDATORY);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('BRANCH_CODE', $('#branchCode').val());
	validator.setValue(ACTION, USAGE);
	validator.setClass('patterns.config.web.forms.access.muserbean');
	validator.setMethod('validateBranchCode');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('branchCode_error', validator.getValue(ERROR));
		$('#branchCode_desc').html(EMPTY_STRING);
		return false;
	}
	$('#branchCode_desc').html(validator.getValue("BRANCH_NAME"));
	if ($('#action').val() == MODIFY) {
		setFocusLast('remarks');
	} else {
		setFocusLast('roleCode');
	}
	return true;
}

function roleCode_val() {
	var value = $('#roleCode').val();
	clearError('roleCode_error');
	if (isEmpty(value)) {
		setError('roleCode_error', MANDATORY);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('ROLE_CODE', $('#roleCode').val());
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.access.muserbean');
		validator.setMethod('validateRoleCode');
		validator.sendAndReceive();
		if (validator.getValue(RESULT) == DATA_AVAILABLE) {
			$('#roleCode_desc').html(validator.getValue('DESCRIPTION'));
		} else {
			setError('roleCode_error', validator.getValue(ERROR));
			return false;
		}
	}
	setFocusLast('remarks');
	return true;
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', HMS_INVALID_REMARKS);
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', HMS_INVALID_REMARKS);
			return false;
		}
	}
	if ($('#action').val() == MODIFY) {
		if (isEmpty(value)) {
			setError('remarks_error', HMS_MANDATORY);
			return false;
		}

	}
	setFocusOnSubmit();
	return true;

}

function revalidate() {
	if ($('#action').val() == ADD) {
		if ($('#pwdDelivery').val() == 4 && $('#pwdDeliveryType').val() == 2) {
			if (!createPassword_val(false)) {
				errors++;
			}
			if (!confirmPassword_val(false)) {
				errors++;
			}
			if (errors == 0) {
				var userID = $('#userID').val();
				var hashedNewPassword = sha256_digest($('#createPassword').val());
				var hashedNewUserID = sha256_digest(hashedNewPassword + userID);
				$('#loginNewPasswordHashed').val(hashedNewUserID);
			}
		}
	}
}
