var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'EROLEPGMALLOC';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}
function clearFields() {
	$('#roleCode').val('');
	$('#moduleCode').val('');
	$('#effectiveDate').val('');
	$('#remarks').val('');
}

function loadProgramsGrid() {
	pgmGrid.clearAll();
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('ROLE_CODE', $('#roleCode').val());
	validator.setValue('MODULE_ID', $('#moduleCode').val());
	validator.setValue(ACTION, $('#action').val());
	validator.setClass('patterns.config.web.forms.access.erolepgmallocbean');
	validator.setMethod('getRolePrograms');
	validator.sendAndReceive();
	if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		xmlString = validator.getValue(RESULT_XML);
		pgmGrid.loadXMLString(xmlString);
	}
}
function loadProgramsData() {
	loadGridQuery(CURRENT_PROGRAM_ID, 'PGM_GRID', postProcessing);
}

var resultData = null;
function postProcessing(result) {
	$xml = $($.parseXML(result));
	($xml).find("row").each(function() {
		try {
			var rowID = $(this).find("cell")[1].textContent;
			var add_allowed = $(this).find("cell")[2].textContent;
			var modify_allowed = $(this).find("cell")[3].textContent;
			var view_allowed = $(this).find("cell")[4].textContent;
			var auth_allowed = $(this).find("cell")[5].textContent;
			var rej_allowed = $(this).find("cell")[6].textContent;
			var process_allowed = $(this).find("cell")[7].textContent;
			if (rowID == (pgmGrid.cells(rowID, 1).getValue())) {
				pgmGrid.cells(rowID, 3).setValue(true);
				if(add_allowed==ONE){
					pgmGrid.cells(rowID, 4).setValue(true);
				}else{
					pgmGrid.cells(rowID, 4).setValue(false);
				}
				if(modify_allowed==ONE){
					pgmGrid.cells(rowID, 5).setValue(true);
				}else{
					pgmGrid.cells(rowID, 5).setValue(false);
				}
				if(view_allowed==ONE){
					pgmGrid.cells(rowID, 6).setValue(true);
				}else{
					pgmGrid.cells(rowID, 6).setValue(false);
				}
				
				if(auth_allowed==ONE){
					pgmGrid.cells(rowID, 7).setValue(true);
				}else{
					pgmGrid.cells(rowID, 7).setValue(false);
				}
				if(rej_allowed==ONE){
					pgmGrid.cells(rowID, 8).setValue(true);
				}else{
					pgmGrid.cells(rowID, 8).setValue(false);
				}
				if(process_allowed==ONE){
					pgmGrid.cells(rowID, 9).setValue(true);
				}else{
					pgmGrid.cells(rowID, 9).setValue(false);
				}
			} else {
				pgmGrid.deleteRow(rowID);
			}
		} catch (e) {
		}
	});
}

function stringToBoolean(val) {
	if (val == COLUMN_ENABLE)
		return true;
	return false;
}
function loadData() {
	$('#roleCode').val(validator.getValue('ROLE_CODE'));
	$('#roleCode_desc').html(validator.getValue('F1_DESCRIPTION'));
	$('#moduleCode').val(validator.getValue('MODULE_ID'));
	$('#moduleCode_desc').html(validator.getValue('F2_MODULE_NAME'));
	$('#effectiveDate').val(validator.getValue('EFFT_DATE'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadAuditFields(validator);
	loadProgramsGrid();
	SOURCE_VALUE = _tableType;
	PK_VALUE = _primaryKey;
	loadProgramsData();
}