var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'QUSERPWDCHGN';
var inlineGridSuccessful = null;
function init() {
	$('#userID').focus();
}

function loadUserPwdGrid() {
	$('#po_view4').removeClass('hidden');
	showMessage(getProgramID(), Message.PROGRESS);
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('USER_ID', $('#userID').val());
	validator.setValue('FROM_DATE', $('#fromDate').val());
	validator.setValue('TO_DATE', $('#toDate').val());
	validator.setClass('patterns.config.web.forms.access.quserpwdchgnbean');
	validator.setMethod('loadUserPwdGrid');
	validator.sendAndReceiveAsync(showStatus);
}

function showStatus() {
	if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		inlineGridSucc.clearAll();
		inlineGridSucc.loadXMLString(validator.getValue("XML"));
		window.scrollTo(0, 210);
		hideMessage(getProgramID());
	} else {
		setError('userID_error', validator.getValue(ERROR));
		hideMessage(getProgramID());
	}
}

function clearFields() {
	$('#userID').val(EMPTY_STRING);
	$('#userID_desc').html(EMPTY_STRING);
	$('#userID_error').html(EMPTY_STRING);
	clearNonPKFields();
}

function clearNonPKFields() {
	$('#fromDate').val(EMPTY_STRING);
	$('#fromDate_error').html(EMPTY_STRING);
	$('#toDate').val(EMPTY_STRING);
	$('#toDate_error').html(EMPTY_STRING);
	inlineGridSucc.clearAll();
	$('#po_view4').addClass('hidden');
}

function doHelp(id) {
	switch (id) {
	case 'userID':
		help('COMMON', 'USERS_INTERNAL', $('#userID').val(), EMPTY_STRING, $('#userID'));
		break;

	}
}
function validate(id) {
	valMode = true;
	switch (id) {
	case 'userID':
		userID_val(valMode);
		break;
	case 'fromDate':
		fromDate_val(valMode);
		break;
	case 'toDate':
		toDate_val(valMode);
		break;
	}
}

function backtrack(id) {
	switch (id) {
	case 'userID':
		setFocusLast('userID');
		break;
	case 'fromDate':
		setFocusLast('userID');
		break;
	case 'toDate':
		setFocusLast('fromDate');
		break;
	case 'submit':
		setFocusLast('toDate');
		break;
	}
}

function userID_val(valMode) {
	var value = $('#userID').val();
	clearError('userID_error');
	if (!isEmpty(value)) {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('USER_ID', $('#userID').val());
		validator.setClass('patterns.config.web.forms.access.quserpwdchgnbean');
		validator.setMethod('validateUserID');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('userID_error', validator.getValue(ERROR));
			return false;
		} else if (validator.getValue(RESULT) == DATA_AVAILABLE) {
			$('#userID_desc').html(validator.getValue("USER_NAME"));
		}
	}
	setFocusLast('fromDate');
	return true;
}
function fromDate_val(valMode) {
	var value = $('#fromDate').val();
	clearError('fromDate_error');
	if (isEmpty(value)) {
		setError('fromDate_error', MANDATORY);
		return false;
	}
	if (!isDate(value)) {
		setError('fromDate_error', INVALID_DATE);
		return false;
	}
	if (!isDateLesserEqual(value, getCBD())) {
		setError('fromDate_error', DATE_LECBD);
		return false;
	}
	setFocusLast('toDate');
	return true;
}
function toDate_val(valMode) {
	var value = $('#toDate').val();
	var fromDate = $('#fromDate').val();
	clearError('toDate_error');
	if (isEmpty(value)) {
		setError('toDate_error', MANDATORY);
		return false;
	}
	if (!isDate(value)) {
		setError('toDate_error', INVALID_DATE);
		return false;
	}
	if (!isDateLesserEqual(value, getCBD())) {
		setError('toDate_error', DATE_LECBD);
		return false;
	}
	if (!isDateGreaterEqual(value, fromDate)) {
		setError('toDate_error', DATE_GEFD);
		return false;
	}
	setFocusLast('submit');
	return true;
}

function revalidate() {
	if (!userID_val(false)) {
		errors++;
	}
	if (!fromDate_val(false)) {
		errors++;
	}
	if (!toDate_val(false)) {
		errors++;
	}
	if (errors == 0) {
		loadUserPwdGrid();
	} else {
		errors = 0;
		innerGrid.clearAll();
		$('#po_view4').addClass('hidden');
	}
}