<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.access.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/access/qbkannouncedtls.js" />
	</web:dependencies>
	<web:loadPK />
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="qbkannouncedtls.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:viewContent id="po_view1">
				<web:dividerBlock>
					<web:sectionBlock width="width-100p" align="left">
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="180px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="ebkannouncedtls.announcementid" var="program" />
									</web:column>
									<web:column>
										<type:announcementIDDisplay property="announceID" id="announceID" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="180px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowOdd>
									<web:column>
										<web:legend key="form.startdate" var="common" mandatory="true" />
									</web:column>
									<web:column>
										<type:dateDisplay property="startDate" id="startDate" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="form.expirydate" var="common" mandatory="true" />
									</web:column>
									<web:column>
										<type:dateDisplay property="expiryDate" id="expiryDate" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="form.displaytitle" var="common" mandatory="true" />
									</web:column>
									<web:column>
										<type:remarksDisplay property="displayTitle" id="displayTitle" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="form.externallink" var="common" />
									</web:column>
									<web:column>
										<type:checkboxDisplay property="externalLink" id="externalLink" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="form.url" var="common" />
									</web:column>
									<web:column>
										<type:urlDisplay property="url" id="url" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="form.urltitle" var="common" />
									</web:column>
									<web:column>
										<type:remarksDisplay property="urlTitle" id="urlTitle" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="form.visibility" var="common" mandatory="true" />
									</web:column>
									<web:column>
										<type:comboDisplay property="visibility" id="visibility" datasourceid="COMMON_VISIBILITY" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="form.disabled" var="common" />
									</web:column>
									<web:column>
										<type:checkboxDisplay property="announceDisabled" id="announceDisabled" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="form.userid" var="common" />
									</web:column>
									<web:column>
										<type:userDisplay property="userID" id="userID" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="form.roletype" var="common" />
									</web:column>
									<web:column>
										<type:comboDisplay property="roleType" id="roleType" datasourceid="COMMON_ROLES" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="form.customercode" var="common" />
									</web:column>
									<web:column>
										<type:customerCodeDisplay property="customerCode" id="customerCode" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="form.branchcode" var="common" />
									</web:column>
									<web:column>
										<type:codeDisplay property="branchCode" id="branchCode" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="form.remarks" var="common" mandatory="true" />
									</web:column>
									<web:column>
										<type:remarksDisplay property="remarks" id="remarks" />
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
						<web:auditDisplay />
					</web:sectionBlock>
				</web:dividerBlock>
			</web:viewContent>
		</web:viewSubPart>
	</web:viewPart>
</web:fragment>