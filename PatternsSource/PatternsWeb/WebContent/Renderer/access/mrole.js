var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MROLE';
var _oldRoleType = EMPTY_STRING;
function init() {
	
	refreshTBAGrid();
	refreshQueryGrid();
	if (_redisplay) {
		if ($('#action').val() == MODIFY) {
			$('#roleType').prop('disabled', true);
			$('#roleType').val($('#tempRoleType').val());
		} else {
			$('#enabled').prop('disabled', true);
			$('#enabled').prop('checked', true);
		}
		cashierRole_val();
		setFocusLast('roleCode');
	}
}
function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'MROLE_MAIN', queryGrid);
}
function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}
function clearFields() {
	$('#roleCode').val(EMPTY_STRING);
	$('#roleCode_error').html(EMPTY_STRING);
	$('#roleCode_desc').html(EMPTY_STRING);
	$('#roleCode').focus();
	clearNonPKFields();

}

function clearNonPKFields() {
	$('#roleDescription').val(EMPTY_STRING);
	$('#roleDescription_error').html(EMPTY_STRING);
	$('#roleType').val(EMPTY_STRING);
	$('#roleType_error').html(EMPTY_STRING);
	setCheckbox('enabled', YES);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
}
function add() {
	setCheckbox('enabled', YES);
	$('#enabled').prop('disabled', true);
	$('#roleType').prop('disabled', false);
}
function modify() {
	$('#roleCode').focus();
	if ($('#action').val() == MODIFY) {
		$('#enabled').prop('disabled', false);
		$('#roleType').prop('disabled', true);
	} else {
		$('#enabled').prop('disabled', true);
		$('#roleType').prop('disabled', false);
	}
}
function loadData() {
	$('#roleCode').val(validator.getValue('CODE'));
	$('#roleDescription').val(validator.getValue('DESCRIPTION'));
	$('#roleType').val(validator.getValue('ROLE_TYPE'));
	$('#tempRoleType').val(validator.getValue('ROLE_TYPE'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	setFocusLast('roleCode');
	resetLoading();
}
function view(source, primaryKey) {
	hideParent('access/qrole', source, primaryKey);
	resetLoading();
}

function doHelp(id) {
	switch (id) {
	case 'roleCode':
		help('MROLE', 'HLP_ROLE_CODE', $('#roleCode').val(), EMPTY_STRING, $('#roleCode'));
		break;
	}
}

function doclearfields(id) {
	switch (id) {
	case 'roleCode':
		if (isEmpty($('#roleCode').val())) {
			clearNonPKFields();
			break;
		}
	}
}


function validate(id) {
	switch (id) {
	case 'roleCode':
		roleCode_val();
		break;
	case 'roleDescription':
		roleDescription_val();
		break;
	case 'roleType':
		roleType_val();
		break;
	case 'enabled':
		enabled_val();
		break;
	case 'remarks':
		remarks_val();
		break;

	}
}

function backtrack(id) {
	switch (id) {
	case 'roleCode':
		setFocusLast('roleCode');
		break;
	case 'roleDescription':
		setFocusLast('roleCode');
		break;
	case 'roleType':
		setFocusLast('roleDescription');
		break;
	case 'enabled':
		if ($('#action').val() == MODIFY) {
			setFocusLast('roleDescription');
		} else {
			setFocusLast('roleType');
		}
		break;
	case 'remarks':
		if ($('#action').val() == MODIFY) {
			setFocusLast('enabled');
		} else {
			setFocusLast('roleType');
		}
		break;
	}
}

function roleCode_val() {
	var value = $('#roleCode').val();
	clearError('roleCode_error');
	if (isEmpty(value)) {
		setError('roleCode_error', HMS_MANDATORY);
		return false;
	} else if ($('#action').val() == ADD && !isValidCode(value)) {
		setError('roleCode_error', HMS_INVALID_FORMAT);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('ROLE_CODE', $('#roleCode').val());
		validator.setValue(ACTION, $('#action').val());
		validator.setClass('patterns.config.web.forms.access.mrolebean');
		validator.setMethod('validateRoleCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('roleCode_error', validator.getValue(ERROR));
			return false;
		} else {
			if ($('#action').val() == MODIFY) {
				PK_VALUE = getEntityCode() + PK_SEPERATOR + $('#roleCode').val();
				if (!loadPKValues(PK_VALUE, 'roleCode_error')) {
					return false;
				}
				setFocusLast('roleDescription');
				return true;
			}
		}
	}
	setFocusLast('roleDescription');
	return true;
}
function roleDescription_val() {
	var value = $('#roleDescription').val();
	clearError('roleDescription_error');
	if (!isValidDescription(value)) {
		setError('roleDescription_error', INVALID_DESCRIPTION);
		return false;
	}
	if ($('#action').val() == ADD) {
		setFocusLast('roleType');
	} else {
		setFocusLast('enabled');
	}
	return true;
}

function roleType_val() {
	var value = $('#roleType').val();
	clearError('roleType_error');
	if (isEmpty(value)) {
		setError('roleType_error', MANDATORY);
		setFocusLast('roleType');
		return false;
	}
	if ($('#action').val() == ADD) {
		setFocusLast('remarks');
	} else {
		setFocusLast('enabled');
	}
	return true;
}


function enabled_val() {
	if ($('#action').val() == MODIFY) {
		setFocusLast('remarks');
	}
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', HMS_INVALID_REMARKS);
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', HMS_INVALID_REMARKS);
			return false;
		}
	}
	if ($('#action').val() == MODIFY) {
		if (isEmpty(value)) {
			setError('remarks_error', HMS_MANDATORY);
			return false;
		}

	}
	setFocusOnSubmit();
	return true;

}

function revalidate() {
	$('#tempRoleType').val($('#roleType').val());
}
