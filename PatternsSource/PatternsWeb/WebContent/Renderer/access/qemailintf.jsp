<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.access.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/access/qemailintf.js" />
	</web:dependencies>
	<web:loadPK />
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="qemailintf.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:viewContent id="po_view1">
				<web:dividerBlock>
					<web:sectionBlock width="width-100p" align="left">
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="180px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
										<web:column>
											<web:legend key="memailintf.emailcode" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:codeDisplay property="emailCode" id="emailCode"/>
										</web:column>
									</web:rowEven>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="180px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowOdd>
										<web:column>
											<web:legend key="form.description" var="common" mandatory="true" />
										</web:column>
										<web:column>
											<type:descriptionDisplay property="description" id="description" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="memailintf.sendallowed" var="program" />
										</web:column>
										<web:column>
											<type:checkboxDisplay property="sendAllowed" id="sendAllowed" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="memailintf.receiveallowed" var="program" />
										</web:column>
										<web:column>
											<type:checkboxDisplay property="receiveAllowed" id="receiveAllowed" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="form.enabled" var="common" />
										</web:column>
										<web:column>
											<type:checkboxDisplay property="enabled" id="enabled" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="form.remarks" var="common" />
										</web:column>
										<web:column>
											<type:remarksDisplay property="remarks" id="remarks" />
										</web:column>
									</web:rowOdd>
							</web:table>
						</web:section>
						<web:auditDisplay />
					</web:sectionBlock>
				</web:dividerBlock>
			</web:viewContent>
		</web:viewSubPart>
	</web:viewPart>
</web:fragment>