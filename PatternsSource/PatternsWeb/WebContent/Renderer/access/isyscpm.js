var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'ISYSCPM';
function init() {
	refreshQueryGrid();
	refreshTBAGrid();
}
function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'ISYSCPM_MAIN', queryGrid);
}
function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}

function doclearfields(id) {
	switch (id) {
	case 'effectiveDate':
		if (isEmpty($('#effectiveDate').val())) {
			$('#effectiveDate_error').html(EMPTY_STRING);
			clearNonPKFields();
			break;
		}
	}
}

function clearFields() {
	$('#effectiveDate').val(EMPTY_STRING);
	$('#effectiveDate_error').html(EMPTY_STRING);
	$('#effectiveDate').focus();
	clearNonPKFields();
}
function clearNonPKFields() {
	$('#minLengthUID').val(EMPTY_STRING);
	$('#minLengthUID_error').html(EMPTY_STRING);
	$('#minLengthPwd').val(EMPTY_STRING);
	$('#minLengthPwd_error').html(EMPTY_STRING);
	$('#minNumPwd').val(EMPTY_STRING);
	$('#minNumPwd_error').html(EMPTY_STRING);
	$('#minAlphaPwd').val(EMPTY_STRING);
	$('#minAlphaPwd_error').html(EMPTY_STRING);
	$('#minSCPwd').val(EMPTY_STRING);
	$('#minSCPwd_error').html(EMPTY_STRING);
	$('#forcePwdChgn').val(EMPTY_STRING);
	$('#forcePwdChgn_error').html(EMPTY_STRING);
	$('#prevPwdCnt').val(EMPTY_STRING);
	$('#prevPwdCnt_error').html(EMPTY_STRING);
	$('#uwCnt').val(EMPTY_STRING);
	$('#uwCnt_error').html(EMPTY_STRING);
	$('#sawCnt').val(EMPTY_STRING);
	$('#sawCnt_error').html(EMPTY_STRING);
	$('#lockUIDUACnt').val(EMPTY_STRING);
	$('#lockUIDUACnt_error').html(EMPTY_STRING);
	$('#lockUIDNUCnt').val(EMPTY_STRING);
	$('#lockUIDNUCnt_error').html(EMPTY_STRING);
	$('#lockUIDUOCnt').val(EMPTY_STRING);
	$('#lockUIDUOCnt_error').html(EMPTY_STRING);
	$('#pwdExpPeriod').val(EMPTY_STRING);
	$('#pwdExpPeriod_error').html(EMPTY_STRING);
	$('#totalLeasePeriod').val(EMPTY_STRING);
	$('#totalLeasePeriod_error').html(EMPTY_STRING);
	$('#autoLogoutPeriod').val(EMPTY_STRING);
	$('#autoLogoutPeriod_error').html(EMPTY_STRING);
	$('#multipleSessionAllowed').prop('checked', false);
	$('#multipleSessionAllowed_error').html(EMPTY_STRING);
	$('#ipRestrictUsers').prop('checked', false);
	$('#ipRestrictUsers_error').html(EMPTY_STRING);
	$('#actRoleAllowed').prop('checked', false);
	$('#actRoleAllowed_error').html(EMPTY_STRING);
	$('#adminActivation').prop('checked', false);
	$('#adminActivation_error').html(EMPTY_STRING);
	$('#pwdDelMechanism').val(EMPTY_STRING);
	$('#pwdDelMechanism_error').html(EMPTY_STRING);
	$('#pwdManualType').val(EMPTY_STRING);
	$('#pwdManualType_error').html(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
}
function add() {
	$('#effectiveDate').focus();
}
function modify() {
	$('#effectiveDate').focus();
}
function loadData() {
	$('#effectiveDate').val(validator.getValue('EFFT_DATE'));
	$('#minLengthUID').val(validator.getValue('MIN_UID_LEN'));
	$('#minLengthPwd').val(validator.getValue('MIN_PWD_LEN'));
	$('#minNumPwd').val(validator.getValue('MIN_PWD_NUM'));
	$('#minAlphaPwd').val(validator.getValue('MIN_PWD_ALPHA'));
	$('#minSCPwd').val(validator.getValue('MIN_PWD_SPECIAL'));
	$('#forcePwdChgn').val(validator.getValue('FORCE_PWD_CHGN'));
	$('#prevPwdCnt').val(validator.getValue('PREVENT_PREV_PWD'));
	$('#uwCnt').val(validator.getValue('UNSUCC_ATTMPT_UW'));
	$('#sawCnt').val(validator.getValue('UNSUCC_ATTMPT_SW'));
	$('#lockUIDUACnt').val(validator.getValue('LOCK_UID'));
	$('#lockUIDNUCnt').val(validator.getValue('INACT_NOT_USED'));
	$('#lockUIDUOCnt').val(validator.getValue('INACT_ONCE_USED'));
	// $('#UIDExpPeriod').val(validator.getValue('INACT_UID_EXPIRY'));
	$('#pwdExpPeriod').val(validator.getValue('INACT_PWD_EXPIRY'));
	$('#totalLeasePeriod').val(validator.getValue('TOTAL_LEASE_PERIOD'));
	$('#autoLogoutPeriod').val(validator.getValue('AUTO_LOGOUT_SECS'));
	$('#multipleSessionAllowed').prop('checked',
			decodeSTB(validator.getValue('MULTIPLE_SESSION_ALLOWED')));
	$('#ipRestrictUsers').prop('checked',
			decodeSTB(validator.getValue('ADMIN_USER_IP_RESTRICT')));
	$('#actRoleAllowed').prop('checked',
			decodeSTB(validator.getValue('MAX_ACTING_ROLE')));
	$('#adminActivation').prop('checked',
			decodeSTB(validator.getValue('ADMIN_ACTIVATION_REQD')));
	$('#remarks').val(validator.getValue('REMARKS'));
	$('#pwdDelMechanism').val(validator.getValue('PWD_DELIVERY_MECHANISM'));
	if ($('#pwdDelMechanism').val() == "4") {
		$('#pwdManualType').prop('disabled', false);
		$('#pwdManualType').val(validator.getValue('PWD_MANUAL_TYPE'));
	} else {
		$('#pwdManualType').prop('disabled', false);
	}
	resetLoading();
}

function doHelp(id) {
	switch (id) {
	case 'effectiveDate':
		help('COMMON', 'HLP_EFFT_DATE', $('#effectiveDate').val(),
				EMPTY_STRING, $('#effectiveDate'));
		break;
	}
}

function view(source, primaryKey) {
	hideParent('access/qsyscpm', source, primaryKey);
	resetLoading();
}
function validate(id) {
	switch (id) {
	case 'effectiveDate':
		effectiveDate_val();
		break;
	case 'minLengthUID':
		minLengthUID_val();
		break;
	case 'minLengthPwd':
		minLengthPwd_val();
		break;
	case 'minNumPwd':
		minNumPwd_val();
		break;
	case 'minAlphaPwd':
		minAlphaPwd_val();
		break;
	case 'minSCPwd':
		minSCPwd_val();
		break;
	case 'forcePwdChgn':
		forcePwdChgn_val();
		break;
	case 'prevPwdCnt':
		prevPwdCnt_val();
		break;
	case 'uwCnt':
		uwCnt_val();
		break;
	case 'sawCnt':
		sawCnt_val();
		break;
	case 'lockUIDUACnt':
		lockUIDUACnt_val();
		break;
	case 'lockUIDNUCnt':
		lockUIDNUCnt_val();
		break;
	case 'lockUIDUOCnt':
		lockUIDUOCnt_val();
		break;
	/*
	 * case 'UIDExpPeriod': UIDExpPeriod_val(); break;
	 */
	case 'pwdExpPeriod':
		pwdExpPeriod_val();
		break;
	case 'totalLeasePeriod':
		totalLeasePeriod_val();
		break;
	case 'autoLogoutPeriod':
		autoLogoutPeriod_val();
		break;
	case 'multipleSessionAllowed':
		multipleSessionAllowed_val();
		break;
	case 'ipRestrictUsers':
		ipRestrictUsers_val();
		break;
	case 'actRoleAllowed':
		actRoleAllowed_val();
		break;
	case 'adminActivation':
		adminActivation_val();
		break;
	case 'pwdDelMechanism':
		pwdDelMechanism_val();
		break;
	case 'pwdManualType':
		pwdManualType_val();
		break;
	case 'remarks':
		remarks_val();
		break;
	}
}

function multipleSessionAllowed_val() {
	setFocusLast('ipRestrictUsers');
	return true;
}

function ipRestrictUsers_val() {
	setFocusLast('actRoleAllowed');
	return true;
}

function actRoleAllowed_val() {
	setFocusLast('adminActivation');
	return true;
}

function pwdDelMechanism_val() {
	var value = $('#pwdDelMechanism').val();
	clearError('pwdDelMechanism_error');
	if (isEmpty(value)) {
		setError('pwdDelMechanism_error', MANDATORY);
		setFocusLast('pwdDelMechanism');
		return false;
	}
	if (value == '4') {
		$('#pwdManualType').prop('disabled', false);
		setFocusLast('pwdManualType');
	} else {
		$('#pwdManualType').prop('disabled', true);
		setFocusLast('remarks');
	}

	return true;
}

function pwdManualType_val() {
	var value = $('#pwdManualType').val();
	clearError('pwdManualType_error');
	if ($('#pwdDelMechanism').val() == '4') {
		if (isEmpty(value)) {
			setError('pwdDelMechanism_error', MANDATORY);
			setFocusLast('pwdDelMechanism');
			return false;
		}
	}
	setFocusLast('remarks');
	return true;
}

function adminActivation_val() {
	setFocusLast('pwdDelMechanism');
	return true;
}

function backtrack(id) {
	switch (id) {
	case 'effectiveDate':
		setFocusLast('effectiveDate');
		break;
	case 'minLengthUID':
		setFocusLast('effectiveDate');
		break;
	case 'minLengthPwd':
		setFocusLast('minLengthUID');
		break;
	case 'minNumPwd':
		setFocusLast('minLengthPwd');
		break;
	case 'minAlphaPwd':
		setFocusLast('minNumPwd');
		break;
	case 'minSCPwd':
		setFocusLast('minAlphaPwd');
		break;
	case 'forcePwdChgn':
		setFocusLast('minSCPwd');
		break;
	case 'prevPwdCnt':
		setFocusLast('forcePwdChgn');
		break;
	case 'uwCnt':
		setFocusLast('prevPwdCnt');
		break;
	case 'sawCnt':
		setFocusLast('uwCnt');
		break;
	case 'lockUIDUACnt':
		setFocusLast('sawCnt');
		break;
	case 'lockUIDNUCnt':
		setFocusLast('lockUIDUACnt');
		break;
	case 'lockUIDUOCnt':
		setFocusLast('lockUIDNUCnt');
		break;
	case 'pwdExpPeriod':
		setFocusLast('lockUIDUOCnt');
		break;
	case 'totalLeasePeriod':
		setFocusLast('pwdExpPeriod');
		break;
	case 'autoLogoutPeriod':
		setFocusLast('totalLeasePeriod');
		break;
	case 'multipleSessionAllowed':
		setFocusLast('autoLogoutPeriod');
		break;
	case 'ipRestrictUsers':
		setFocusLast('multipleSessionAllowed');
		break;
	case 'actRoleAllowed':
		setFocusLast('ipRestrictUsers');
		break;
	case 'adminActivation':
		setFocusLast('actRoleAllowed');
		break;
	case 'pwdDelMechanism':
		setFocusLast('adminActivation');
		break;
	case 'pwdManualType':
		setFocusLast('pwdDelMechanism');
		break;
	case 'remarks':
		if ($('#pwdDelMechanism').val() == '4') {
			setFocusLast('pwdManualType');
		} else {
			setFocusLast('pwdDelMechanism');
		}
		break;
	}
}

function effectiveDate_val() {
	var value = $('#effectiveDate').val();
	clearError('effectiveDate_error');
	if (!isValidEffectiveDate(value, 'effectiveDate_error')) {
		setFocusLast('effectiveDate');
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('EFFT_DATE', $('#effectiveDate').val());
		validator.setValue(ACTION, $('#action').val());
		validator.setClass('patterns.config.web.forms.access.isyscpmbean');
		validator.setMethod('validateEffectiveDate');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('effectiveDate_error', validator.getValue(ERROR));
			setFocusLast('effectiveDate');
			return false;
		} else {
			if ($('#action').val() == MODIFY) {
				PK_VALUE = getEntityCode() + PK_SEPERATOR
						+ $('#effectiveDate').val();
				if (!loadPKValues(PK_VALUE, 'effectiveDate_error')) {
					return false;
				}
				setFocusLast('minLengthUID');
				return true;
			}
		}
	}
	setFocusLast('minLengthUID');
	return true;
}

function minLengthUID_val() {
	var value = $('#minLengthUID').val();
	clearError('minLengthUID_error');
	if (isEmpty(value)) {
		setError('minLengthUID_error', MANDATORY);
		setFocusLast('minLengthUID');
		return false;
	}
	if (!isNumeric(value)) {
		setError('minLengthUID_error', NUMERIC_CHECK);
		setFocusLast('minLengthUID');
		return false;
	}
	if (isZero(value)) {
		setError('minLengthUID_error', ZERO_CHECK);
		setFocusLast('minLengthUID');
		return false;
	}
	if (!isPositive(value)) {
		setError('minLengthUID_error', POSITIVE_CHECK);
		setFocusLast('minLengthUID');
		return false;
	}
	if (parseInt(value) > 50) {
		setError('minLengthUID_error', MAXVAL_50);
		setFocusLast('minLengthUID');
		return false;
	}
	setFocusLast('minLengthPwd');
	return true;
}
function minLengthPwd_val() {
	var value = $('#minLengthPwd').val();
	clearError('minLengthPwd_error');
	if (isEmpty(value)) {
		setError('minLengthPwd_error', MANDATORY);
		setFocusLast('minLengthPwd');
		return false;
	}
	if (!isNumeric(value)) {
		setError('minLengthPwd_error', NUMERIC_CHECK);
		setFocusLast('minLengthPwd');
		return false;
	}
	if (isZero(value)) {
		setError('minLengthPwd_error', ZERO_CHECK);
		setFocusLast('minLengthPwd');
		return false;
	}
	if (!isPositive(value)) {
		setError('minLengthPwd_error', POSITIVE_CHECK);
		setFocusLast('minLengthPwd');
		return false;
	}
	if (parseInt(value) > 99) {
		setError('minLengthPwd_error', MAXVAL_99);
		setFocusLast('minLengthPwd');
		return false;
	}
	setFocusLast('minNumPwd');
	return true;
}
function minNumPwd_val() {
	var value = $('#minNumPwd').val();
	clearError('minNumPwd_error');
	if (isEmpty(value)) {
		setError('minNumPwd_error', MANDATORY);
		setFocusLast('minNumPwd');
		return false;
	}
	if (!isNumeric(value)) {
		setError('minNumPwd_error', NUMERIC_CHECK);
		setFocusLast('minNumPwd');
		return false;
	}
	if (!isPositive(value)) {
		setError('minNumPwd_error', POSITIVE_CHECK);
		setFocusLast('minNumPwd');
		return false;
	}
	if (parseInt(value) > 99) {
		setError('minNumPwd_error', MAXVAL_99);
		setFocusLast('minNumPwd');
		return false;
	}
	setFocusLast('minAlphaPwd');
	return true;
}
function minAlphaPwd_val() {
	var value = $('#minAlphaPwd').val();
	clearError('minAlphaPwd_error');
	if (isEmpty(value)) {
		setError('minAlphaPwd_error', MANDATORY);
		setFocusLast('minAlphaPwd');
		return false;
	}
	if (!isNumeric(value)) {
		setError('minAlphaPwd_error', NUMERIC_CHECK);
		setFocusLast('minAlphaPwd');
		return false;
	}
	if (!isPositive(value)) {
		setError('minAlphaPwd_error', POSITIVE_CHECK);
		setFocusLast('minAlphaPwd');
		return false;
	}
	if (parseInt(value) > 99) {
		setError('minAlphaPwd_error', MAXVAL_99);
		setFocusLast('minAlphaPwd');
		return false;
	}
	setFocusLast('minSCPwd');
	return true;
}
function minSCPwd_val() {
	var value = $('#minSCPwd').val();
	clearError('minSCPwd_error');
	if (isEmpty(value)) {
		setError('minSCPwd_error', MANDATORY);
		setFocusLast('minSCPwd');
		return false;
	}
	if (!isNumeric(value)) {
		setError('minSCPwd_error', NUMERIC_CHECK);
		setFocusLast('minSCPwd');
		return false;
	}
	if (!isPositive(value)) {
		setError('minSCPwd_error', POSITIVE_CHECK);
		setFocusLast('minSCPwd');
		return false;
	}
	if (parseInt(value) > 99) {
		setError('minSCPwd_error', MAXVAL_99);
		setFocusLast('minSCPwd');
		return false;

	}
	var total_length = parseInt($('#minNumPwd').val())
			+ parseInt($('#minAlphaPwd').val())
			+ parseInt($('#minSCPwd').val());
	if (total_length != parseInt($('#minLengthPwd').val())) {
		setError('minSCPwd_error', PASSWORD_LEN_CHECK);
		setFocusLast('minSCPwd');
		return false;
	}
	setFocusLast('forcePwdChgn');
	return true;
}
function forcePwdChgn_val() {
	var value = $('#forcePwdChgn').val();
	clearError('forcePwdChgn_error');
	if (isEmpty(value)) {
		setError('forcePwdChgn_error', MANDATORY);
		setFocusLast('forcePwdChgn');
		return false;
	}
	if (!isNumeric(value)) {
		setError('forcePwdChgn_error', NUMERIC_CHECK);
		setFocusLast('forcePwdChgn');
		return false;
	}
	if (!isPositive(value)) {
		setError('forcePwdChgn_error', POSITIVE_CHECK);
		setFocusLast('forcePwdChgn');
		return false;
	}
	if (parseInt(value) > 9999) {
		setError('forcePwdChgn_error', MAXVAL_9999);
		setFocusLast('forcePwdChgn');
		return false;
	}
	setFocusLast('prevPwdCnt');
	return true;
}
function prevPwdCnt_val() {
	var value = $('#prevPwdCnt').val();
	clearError('prevPwdCnt_error');
	if (isEmpty(value)) {
		setError('prevPwdCnt_error', MANDATORY);
		setFocusLast('prevPwdCnt');
		return false;
	}
	if (!isNumeric(value)) {
		setError('prevPwdCnt_error', NUMERIC_CHECK);
		setFocusLast('prevPwdCnt');
		return false;
	}
	if (!isPositive(value)) {
		setError('prevPwdCnt_error', POSITIVE_CHECK);
		setFocusLast('prevPwdCnt');
		return false;
	}
	if (parseInt(value) > 99) {
		setError('prevPwdCnt_error', MAXVAL_99);
		setFocusLast('prevPwdCnt');
		return false;
	}
	setFocusLast('uwCnt');
	return true;
}
function uwCnt_val() {
	var value = $('#uwCnt').val();
	clearError('uwCnt_error');
	if (isEmpty(value)) {
		setError('uwCnt_error', MANDATORY);
		setFocusLast('uwCnt');
		return false;
	}
	if (!isNumeric(value)) {
		setError('uwCnt_error', NUMERIC_CHECK);
		setFocusLast('uwCnt');
		return false;
	}
	if (!isPositive(value)) {
		setError('uwCnt_error', POSITIVE_CHECK);
		setFocusLast('uwCnt');
		return false;
	}
	if (parseInt(value) > 99) {
		setError('uwCnt_error', MAXVAL_99);
		setFocusLast('uwCnt');
		return false;
	}
	setFocusLast('sawCnt');
	return true;
}
function sawCnt_val() {
	var value = $('#sawCnt').val();
	clearError('sawCnt_error');
	if (isEmpty(value)) {
		setError('sawCnt_error', MANDATORY);
		setFocusLast('sawCnt');
		return false;
	}
	if (!isNumeric(value)) {
		setError('sawCnt_error', NUMERIC_CHECK);
		setFocusLast('sawCnt');
		return false;
	}
	if (!isPositive(value)) {
		setError('sawCnt_error', POSITIVE_CHECK);
		setFocusLast('sawCnt');
		return false;
	}
	if (parseInt(value) > 99) {
		setError('sawCnt_error', MAXVAL_99);
		setFocusLast('sawCnt');
		return false;
	}
	setFocusLast('lockUIDUACnt');
	return true;
}
function lockUIDUACnt_val() {
	var value = $('#lockUIDUACnt').val();
	clearError('lockUIDUACnt_error');
	if (isEmpty(value)) {
		setError('lockUIDUACnt_error', MANDATORY);
		setFocusLast('lockUIDUACnt');
		return false;
	}
	if (!isNumeric(value)) {
		setError('lockUIDUACnt_error', NUMERIC_CHECK);
		setFocusLast('lockUIDUACnt');
		return false;
	}
	if (!isPositive(value)) {
		setError('lockUIDUACnt_error', POSITIVE_CHECK);
		setFocusLast('lockUIDUACnt');
		return false;
	}
	if (parseInt(value) > 99) {
		setError('lockUIDUACnt_error', MAXVAL_99);
		setFocusLast('lockUIDUACnt');
		return false;
	}
	setFocusLast('lockUIDNUCnt');
	return true;
}
function autoLogoutPeriod_val() {
	var value = $('#autoLogoutPeriod').val();
	clearError('autoLogoutPeriod_error');
	if (isEmpty(value)) {
		setError('autoLogoutPeriod_error', MANDATORY);
		setFocusLast('autoLogoutPeriod');
		return false;
	}
	if (!isNumeric(value)) {
		setError('autoLogoutPeriod_error', NUMERIC_CHECK);
		setFocusLast('autoLogoutPeriod');
		return false;
	}
	if (isZero(value)) {
		setError('autoLogoutPeriod_error', ZERO_CHECK);
		setFocusLast('autoLogoutPeriod');
		return false;
	}
	if (!isPositive(value)) {
		setError('autoLogoutPeriod_error', POSITIVE_CHECK);
		setFocusLast('autoLogoutPeriod');
		return false;
	}
	if (parseInt(value) > 9999) {
		setError('autoLogoutPeriod_error', MAXVAL_9999);
		setFocusLast('autoLogoutPeriod');
		return false;
	}
	var leasePeriodValue = parseInt($('#totalLeasePeriod').val());
	if (parseInt(value) > leasePeriodValue) {
		setError('totalLeasePeriod_error', LEASE_PERIOD_GT_AUTO_LOGOUT);
		setFocusLast('autoLogoutPeriod');
		return false;
	}
	setFocusLast('multipleSessionAllowed');
	return true;
}

function totalLeasePeriod_val() {
	var value = $('#totalLeasePeriod').val();
	clearError('totalLeasePeriod_error');
	if (isEmpty(value)) {
		setError('totalLeasePeriod_error', MANDATORY);
		setFocusLast('totalLeasePeriod');
		return false;
	}
	if (!isNumeric(value)) {
		setError('totalLeasePeriod_error', NUMERIC_CHECK);
		setFocusLast('totalLeasePeriod');
		return false;
	}
	if (isZero(value)) {
		setError('totalLeasePeriod_error', ZERO_CHECK);
		setFocusLast('totalLeasePeriod');
		return false;
	}
	if (!isPositive(value)) {
		setError('totalLeasePeriod_error', POSITIVE_CHECK);
		setFocusLast('totalLeasePeriod');
		return false;
	}
	if (parseInt(value) > 9999) {
		setError('totalLeasePeriod_error', MAXVAL_9999);
		setFocusLast('totalLeasePeriod');
		return false;
	}
	setFocusLast('autoLogoutPeriod');
	return true;
}

function lockUIDNUCnt_val() {
	var value = $('#lockUIDNUCnt').val();
	clearError('lockUIDNUCnt_error');
	if (isEmpty(value)) {
		setError('lockUIDNUCnt_error', MANDATORY);
		setFocusLast('lockUIDNUCnt');
		return false;
	}
	if (!isNumeric(value)) {
		setError('lockUIDNUCnt_error', NUMERIC_CHECK);
		setFocusLast('lockUIDNUCnt');
		return false;
	}
	if (!isPositive(value)) {
		setError('lockUIDNUCnt_error', POSITIVE_CHECK);
		setFocusLast('lockUIDNUCnt');
		return false;
	}
	if (parseInt(value) > 9999) {
		setError('lockUIDNUCnt_error', MAXVAL_9999);
		setFocusLast('lockUIDNUCnt');
		return false;
	}
	setFocusLast('lockUIDUOCnt');
	return true;
}
function lockUIDUOCnt_val() {
	var value = $('#lockUIDUOCnt').val();
	clearError('lockUIDUOCnt_error');
	if (isEmpty(value)) {
		setError('lockUIDUOCnt_error', MANDATORY);
		setFocusLast('lockUIDUOCnt');
		return false;
	}
	if (!isNumeric(value)) {
		setError('lockUIDUOCnt_error', NUMERIC_CHECK);
		setFocusLast('lockUIDUOCnt');
		return false;
	}
	if (!isPositive(value)) {
		setError('lockUIDUOCnt_error', POSITIVE_CHECK);
		setFocusLast('lockUIDUOCnt');
		return false;
	}
	if (parseInt(value) > 9999) {
		setError('lockUIDUOCnt_error', MAXVAL_9999);
		setFocusLast('lockUIDUOCnt');
		return false;
	}
	setFocusLast('pwdExpPeriod');
	return true;
}
/*
 * function UIDExpPeriod_val() { var value = $('#UIDExpPeriod').val();
 * clearError('UIDExpPeriod_error'); if (isEmpty(value)) {
 * setError('UIDExpPeriod_error', MANDATORY); return false; } if
 * (!isNumeric(value)) { setError('UIDExpPeriod_error', NUMERIC_CHECK); return
 * false; } if (!isPositive(value)) { setError('UIDExpPeriod_error',
 * POSITIVE_CHECK); return false; } if (parseInt(value) > 9999) {
 * setError('UIDExpPeriod_error', MAXVAL_9999); return false; } return true; }
 */
function pwdExpPeriod_val() {
	var value = $('#pwdExpPeriod').val();
	clearError('pwdExpPeriod_error');
	if (isEmpty(value)) {
		setError('pwdExpPeriod_error', MANDATORY);
		setFocusLast('pwdExpPeriod');
		return false;
	}
	if (!isNumeric(value)) {
		setError('pwdExpPeriod_error', NUMERIC_CHECK);
		setFocusLast('pwdExpPeriod');
		return false;
	}
	if (!isPositive(value)) {
		setError('pwdExpPeriod_error', POSITIVE_CHECK);
		setFocusLast('pwdExpPeriod');
		return false;
	}
	if (parseInt(value) > 9999) {
		setError('pwdExpPeriod_error', MAXVAL_9999);
		setFocusLast('pwdExpPeriod');
		return false;
	}
	setFocusLast('totalLeasePeriod');
	return true;
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', HMS_INVALID_REMARKS);
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', HMS_INVALID_REMARKS);
			return false;
		}
	}
	if ($('#action').val() == MODIFY) {
		if (isEmpty(value)) {
			setError('remarks_error', HMS_MANDATORY);
			return false;
		}

	}
	setFocusOnSubmit();
	return true;

}

function revalidate(){
	if (!effectiveDate_val()) {
		errors++;
	}
	if (!minLengthUID_val()) {
		errors++;
	}
	if (!minLengthPwd_val()) {
		errors++;
	}
	if (!minNumPwd_val()) {
		errors++;
	}
	if (!minAlphaPwd_val()) {
		errors++;
	}
	if (!minSCPwd_val()) {
		errors++;
	}
	if (!forcePwdChgn_val()) {
		errors++;
	}
	if (!prevPwdCnt_val()) {
		errors++;
	}
	if (!uwCnt_val()) {
		errors++;
	}
	if (!sawCnt_val()) {
		errors++;
	}
	if (!lockUIDUACnt_val()) {
		errors++;
	}
	if (!lockUIDNUCnt_val()) {
		errors++;
	}
	if (!lockUIDUOCnt_val()) {
		errors++;
	}
	/*
	if (!UIDExpPeriod_val()) {
		errors++;
	}
	*/
	if (!pwdExpPeriod_val()) {
		errors++;
	}
	if (!totalLeasePeriod_val()) {
		errors++;
	}
	if (!autoLogoutPeriod_val()) {
		errors++;
	}
	if (!pwdDelMechanism_val()) {
		errors++;
	}
	if (!pwdManualType_val()) {
		errors++;
	}
	if (!remarks_val()) {
		errors++;
	}
}