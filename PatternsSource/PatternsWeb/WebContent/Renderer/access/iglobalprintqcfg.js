var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'IGLOBALPRINTQCFG';
function init() {
	refreshQueryGrid();
	refreshTBAGrid();
	if (_redisplay) {
		innerGrid.clearAll();
		innerGrid.loadXMLString($('#xmlPrinterConfigGrid').val());
	}
}

function loadGrid() {
	loadGridQuery(CURRENT_PROGRAM_ID, 'IGLOBALPRINTQCFG_GRID', innerGrid);
}

function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'IGLOBALPRINTQCFG_MAIN', queryGrid);
}

function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}

function doHelp(id) {
	switch (id) {
	case 'effectiveDate':
		help(CURRENT_PROGRAM_ID, 'HLP_EFFT_DATE', $('#effectiveDate').val(),
				EMPTY_STRING, $('#effectiveDate'));
		break;
	case 'printTypeId':
		help('COMMON', 'HLP_PRNTYPE_M', $('#printTypeId').val(), EMPTY_STRING,
				$('#printTypeId'));
		break;
	case 'printQueueId':
		help('COMMON', 'HLP_PRINTQUEUE_M', $('#printQueueId').val(),
				EMPTY_STRING, $('#printQueueId'));
		break;
	}
}

function clearFields() {
	$('#effectiveDate').val(EMPTY_STRING);
	$('#effectiveDate_error').html(EMPTY_STRING);
	clearNonPKFields();
}

function clearNonPKFields() {
	$('#printTypeId').val(EMPTY_STRING);
	$('#printTypeId_error').html(EMPTY_STRING);
	$('#printTypeId_desc').html(EMPTY_STRING);
	$('#printQueueId').val(EMPTY_STRING);
	$('#printQueueId_error').html(EMPTY_STRING);
	$('#printQueueId_desc').html(EMPTY_STRING);
	innerGrid.clearAll();
}

function add() {
	$('#po_view1').removeClass('hidden');
	$('#effectiveDate').focus();
}

function modify() {
	$('#po_view1').removeClass('hidden');
	$('#printTypeId').focus();
}

function loadData() {
	$('#effectiveDate').val(validator.getValue('EFFT_DATE'));
	loadGrid();
	resetLoading();
}

function view(source, primaryKey) {
	hideParent('access/qglobalprintqcfg', source, primaryKey);
	resetLoading();
}

function validate(id) {
	valMode = true;
	switch (id) {
	case 'effectiveDate':
		effectiveDate_val(valMode);
		break;
	case 'printTypeId':
		printTypeId_val(valMode);
		break;
	case 'printQueueId':
		printQueueId_val(valMode);
		break;
	}
}

function doclearfields(id) {
	switch (id) {
	case 'effectiveDate':
		if (isEmpty($('#effectiveDate').val())) {
			$('#effectiveDate_error').html(EMPTY_STRING);
			clearNonPKFields();
			break;
		}
	}
}

function backtrack(id) {
	switch (id) {
	case 'effectiveDate':
		setFocusLast('effectiveDate');
		break;
	case 'printTypeId':
		setFocusLast('effectiveDate');
		break;
	case 'printQueueId':
		setFocusLast('printTypeId');
		break;
	}
}

function gridExit(id) {
	switch (id) {
	case 'printQueueId':
		$('#xmlPrinterConfigGrid').val(innerGrid.serialize());
		setFocusOnSubmit();
		break;
	}
}

function gridDeleteRow(id) {
	switch (id) {
	case 'printTypeId':
		deleteGridRecord(innerGrid);
		break;
	}
}

function effectiveDate_val(valMode) {
	var value = $('#effectiveDate').val();
	clearError('effectiveDate_error');
	if (!isValidEffectiveDate(value, 'effectiveDate_error')) {
		setFocusLast('effectiveDate');
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('EFFT_DATE', $('#effectiveDate').val());
		validator.setValue(ACTION, $('#action').val());
		validator
				.setClass('patterns.config.web.forms.access.iglobalprintqcfgbean');
		validator.setMethod('validateEffectiveDate');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('effectiveDate_error', validator.getValue(ERROR));
			setFocusLast('effectiveDate');
			return false;
		} else {
			if ($('#action').val() == MODIFY) {
				PK_VALUE = getEntityCode() + PK_SEPERATOR
						+ $('#effectiveDate').val();
				if (!loadPKValues(PK_VALUE, 'effectiveDate_error')) {
					return false;
				}
				setFocusLast('printTypeId');
				return true;
			}
		}
	}
	setFocusLast('printTypeId');
	return true;
}

function printTypeId_val(valMode) {
	var value = $('#printTypeId').val();
	clearError('printTypeId_error');
	if (isEmpty(value)) {
		setError('printTypeId_error', MANDATORY);
		setFocusLast('printTypeId');
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('PRNTYPE_ID', value);
		validator.setValue(ACTION, USAGE);
		validator
				.setClass('patterns.config.web.forms.access.iglobalprintqcfgbean');
		validator.setMethod('validatePrintTypeId');
		validator.sendAndReceive();
		if (validator.getValue(RESULT) == DATA_AVAILABLE) {
			$('#printTypeId_desc').html(validator.getValue('DESCRIPTION'));
		} else {
			setError('printTypeId_error', validator.getValue(ERROR));
			setFocusLast('printTypeId');
			return false;
		}
	}
	setFocusLast('printQueueId');
	return true;
}

function printQueueId_val(valMode) {
	var value = $('#printQueueId').val();
	clearError('printQueueId_error');
	if (isEmpty(value)) {
		setError('printQueueId_error', MANDATORY);
		setFocusLast('printQueueId');
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('PRNQ_ID', value);
		validator.setValue(ACTION, USAGE);
		validator
				.setClass('patterns.config.web.forms.access.iglobalprintqcfgbean');
		validator.setMethod('validatePrintQueueId');
		validator.sendAndReceive();
		if (validator.getValue(RESULT) == DATA_AVAILABLE) {
			$('#printQueueId_desc').html(validator.getValue('DESCRIPTION'));

		} else {
			setError('printQueueId_error', validator.getValue(ERROR));
			setFocusLast('printQueueId');
			return false;
		}
	}
	if (valMode) {
		setFocusLastOnGridSubmit('innerGrid');
	}
	return true;
}

function iglobalprintqcfg_gridDuplicateCheck(editMode, editRowId) {
	var currentValue = [ $('#printTypeId').val() ];
	return _grid_duplicate_check(innerGrid, currentValue, [ 1 ]);
}

function iglobalprintqcfg_addGrid(editMode, editRowId) {
	if (printTypeId_val(false) && printQueueId_val(false)) {
		if (iglobalprintqcfg_gridDuplicateCheck(editMode, editRowId)) {
			var field_values = [ 'false', $('#printTypeId').val(),
					$('#printTypeId_desc').html(), $('#printQueueId').val(),
					$('#printQueueId_desc').html() ];
			_grid_updateRow(innerGrid, field_values);
			iglobalprintqcfg_clearGridFields();
			$('#printTypeId').focus();
			return true;
		} else {
			setError('printTypeId_error', HMS_DUPLICATE_DATA);
			$('#printTypeId').focus();
			return false;
		}
	} else {
		$('#printTypeId').focus();
		return false;
	}
}

function iglobalprintqcfg_editGrid(rowId) {
	$('#printTypeId').val(innerGrid.cells(rowId, 1).getValue());
	$('#printTypeId_desc').html(innerGrid.cells(rowId, 2).getValue());
	$('#printQueueId').val(innerGrid.cells(rowId, 3).getValue());
	$('#printQueueId_desc').html(innerGrid.cells(rowId, 4).getValue());
	$('#printTypeId').focus();
}

function iglobalprintqcfg_cancelGrid(rowId) {
	iglobalprintqcfg_clearGridFields();
}

function iglobalprintqcfg_clearGridFields() {
	$('#printTypeId').val(EMPTY_STRING);
	$('#printTypeId_desc').html(EMPTY_STRING);
	$('#printQueueId').val(EMPTY_STRING);
	$('#printQueueId_desc').html(EMPTY_STRING);
	clearError('#printTypeId_error');
}

function revalidate() {
	if (innerGrid.getRowsNum() > 0) {
		$('#xmlPrinterConfigGrid').val(innerGrid.serialize());
		$('#printTypeId_error').html(EMPTY_STRING);
	} else {
		setError('#printTypeId_error', ADD_ATLEAST_ONE_ROW);
	}
}
