var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MEMAILINTF';
function init() {
	refreshTBAGrid();
	refreshQueryGrid();
	if ($('#action').val() == ADD) {
		$('#enabled').prop('disabled', true);
		setCheckbox('enabled', YES);
	}
}

function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID,'MEMAILINTF_MAIN', queryGrid);
}

function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}

function doclearfields(id) {
	switch (id) {
	case 'emailCode':
		if (isEmpty($('#emailCode').val())) {
			$('#emailCode_error').html(EMPTY_STRING);
			$('#emailCode_desc').html(EMPTY_STRING);
			clearNonPKFields();
			break;
		}
	}
}

function clearFields() {
	$('#emailCode').val(EMPTY_STRING);
	$('#emailCode_error').html(EMPTY_STRING);
	$('#emailCode_desc').html(EMPTY_STRING);
	clearNonPKFields();
}

function clearNonPKFields() {
	$('#description').val(EMPTY_STRING);
	$('#description_error').html(EMPTY_STRING);
	setCheckbox('sendAllowed', NO);
    setCheckbox('receiveAllowed', NO);
	setCheckbox('enabled', YES);
    $('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
}

function doHelp(id) {
	switch (id) {
	case 'emailCode':
		help('MEMAILINTF', 'HLP_EMAIL_INTF', $('#emailCode').val(), EMPTY_STRING, $('#emailCode'));
		break;
	}
}

function add() {
	$('#emailCode').focus();
	setCheckbox('enabled', YES);
	$('#enabled').prop('disabled', true);

}

function modify() {
	$('#emailCode').focus();
	if ($('#action').val() == MODIFY) {
		$('#enabled').prop('disabled', false);
	} else {
		$('#enabled').prop('disabled', true);
	}

}

function loadData() {
	$('#emailCode').val(validator.getValue('EMAIL_CODE'));
	$('#description').val(validator.getValue('EMAIL_DESCN'));
	setCheckbox('sendAllowed', validator.getValue('EMAIL_SEND_ALLOWED'));
	setCheckbox('receiveAllowed', validator.getValue('EMAIL_RECV_ALLOWED'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	resetLoading();
}

function view(source, primaryKey) {
	hideParent('access/qemailintf', source, primaryKey);
	resetLoading();
}

function validate(id) {
	switch (id) {
	case 'emailCode':
		emailCode_val();
		break;
	case 'description':
		description_val();
		break;
	case 'sendAllowed':
		sendAllowed_val();
		break;
	case 'receiveAllowed':
		receiveAllowed_val();
		break;
	case 'enabled':
		enabled_val();
		break;
	case 'remarks':
		remarks_val();
		break;
	}
}

function backtrack(id) {
	switch (id) {
	case 'emailCode':
		setFocusLast('emailCode');
		break;
	case 'description':
		setFocusLast('emailCode');
		break;
	case 'sendAllowed':
		setFocusLast('description');
		break;
	case 'receiveAllowed':
		setFocusLast('sendAllowed');
		break;
	case 'enabled':
		setFocusLast('receiveAllowed');
		break;
	case 'remarks':
		if ($('#action').val() == MODIFY) {
			setFocusLast('enabled');
		} else {
			setFocusLast('receiveAllowed');
		}
		break;
	}
}


function emailCode_val() {
	var value = $('#emailCode').val();
	clearError('emailCode_error');
	if (isEmpty(value)) {
		setError('emailCode_error', HMS_MANDATORY);
		return false;
	} else if (!isValidCode(value)) {
		setError('emailCode_error', HMS_INVALID_FORMAT);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('EMAIL_CODE', $('#emailCode').val());
		validator.setValue(ACTION, $('#action').val());
		validator.setClass('patterns.config.web.forms.access.memailintfbean');
		validator.setMethod('validateEmailCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('emailCode_error', validator.getValue(ERROR));
			$('#emailCode_desc').html(EMPTY_STRING);
			return false;
		} else {
			if ($('#action').val() == MODIFY) {
				PK_VALUE = $('#emailCode').val();
				if(!loadPKValues(PK_VALUE, 'emailCode_error')){
					return false;
				}
			}
		}
	}
	setFocusLast('description');
	return true;
}

function description_val() {
	var value = $('#description').val();
	clearError('description_error');
	if (isEmpty(value)) {
		setError('description_error', HMS_MANDATORY);
		return false;
	}
	if (!isValidDescription(value)) {
		setError('description_error', HMS_INVALID_DESCRIPTION);
		return false;
	}
	setFocusLast('sendAllowed');
	return true;
}

function sendAllowed_val() {
	setFocusLast('receiveAllowed');
return true;
}

function receiveAllowed_val() {
	if ($('#action').val() == ADD) {
		setFocusLast('remarks');
	} else {
		setFocusLast('enabled');
	}
	return true;
}

function enabled_val() {
		setFocusLast('remarks');
	return true;
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', HMS_INVALID_REMARKS);
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', HMS_INVALID_REMARKS);
			return false;
		}
	}
	if ($('#action').val() == MODIFY) {
		if (isEmpty(value)) {
			setError('remarks_error', HMS_MANDATORY);
			return false;
		}
	}
	setFocusOnSubmit();
	return true;
}
