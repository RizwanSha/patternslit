var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'IRESTRICTPFX';
function init() {
	refreshQueryGrid();
	refreshTBAGrid();
	if (_redisplay) {
		if($('#pfxRestrictReq').is(':checked')){
			innerGrid.clearAll();
			if(!isEmpty($('#xmlPrefixDetails').val())){
				innerGrid.loadXMLString($('#xmlPrefixDetails').val());
			}
		}
		showFields();
	}
}

function loadGrid() {
	loadGridQuery(CURRENT_PROGRAM_ID, 'IRESTRICTPFX_GRID',innerGrid);
}

function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'IRESTRICTPFX_MAIN');
}

function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}

function clearFields() {
	
	$('#effectiveDate').val(EMPTY_STRING);
	$('#effectiveDate_error').html(EMPTY_STRING);
	$('#pfxRestrictReq').prop('checked',false);
	$('#pfxRestrictReq_error').html(EMPTY_STRING);
	$('#prefixCode').val(EMPTY_STRING);
	$('#prefixCode_error').html(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
	innerGrid.clearAll();
}

function showFields() {
	if ($('#pfxRestrictReq').is(':checked')) {
		$('#prefixCode').removeAttr("disabled"); 
		$('#prefixCode').val(EMPTY_STRING);
		$('#prefixCode_error').html(EMPTY_STRING);	
	} else {		
		$('#prefixCode').attr("disabled", "disabled"); 
		$('#prefixCode').val(EMPTY_STRING);
		$('#prefixCode_error').html(EMPTY_STRING);			
	}
}

function checkclick(id) {
	switch (id) {
	case 'pfxRestrictReq':
		if ($('#pfxRestrictReq').is(':checked')) {
			$('#prefixCode').removeAttr("disabled"); 
			$('#prefixCode').val(EMPTY_STRING);
			$('#prefixCode_error').html(EMPTY_STRING);	
		} else {		
			$('#prefixCode').attr("disabled", "disabled"); 
			$('#prefixCode').val(EMPTY_STRING);
			$('#prefixCode_error').html(EMPTY_STRING);	
			innerGrid.clearAll();
		}
		break;
	}
}

function add() {
	
	showFields();
	$('#effectiveDate').focus();
}

function modify() {
	
}

function loadData() {
	$('#effectiveDate').val(validator.getValue('EFFT_DATE'));
	$('#pfxRestrictReq').prop('checked',decodeSTB(validator.getValue('PFX_RESTRICT_REQD')));
	$('#remarks').val(validator.getValue('REMARKS'));
	showFields();
	if($('#pfxRestrictReq').is(':checked'))	{
		loadGrid();
	}	else	{
		innerGrid.clearAll();
	}
	resetLoading();
}

function view(source, primaryKey) {
	hideParent('access/qrestrictpfx', source, primaryKey);
	resetLoading();
}

function validate(id) {
	switch (id) {
	case 'effectiveDate':
		effectiveDate_val();
		break;	
	case 'prefixCode':
		prefixCode_val();
		break;
	case 'remarks':
		remarks_val();
		break;
	}
}


function prefixCode_val(){
	var value = $('#prefixCode').val();
	clearError('prefixCode_error');
	if (isEmpty(value)) {
		setError('prefixCode_error', MANDATORY);
		return false;
	}else{
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('PFX',value);
		validator.setClass('patterns.config.web.forms.access.irestrictpfxbean');
		validator.setMethod('validatePrefix');
		validator.sendAndReceive();
		if(validator.getValue(RESULT)==DATA_AVAILABLE ){
			return true;
		}	else{
			setError('prefixCode_error', validator.getValue(ERROR));
			return false;
		}
	} 
}


function prefixCodeDuplicateCheck()	{
	var row ;
	var gridvalue;
	var currvalue ;
	total_rows = innerGrid.getRowsNum();
	currvalue = $('#prefixCode').val();
	for (row=0;row<total_rows;row++)	{
	    gridvalue = innerGrid.cells2(row,1).getValue();
	    if ( gridvalue == currvalue){
	       return false;}
	}
	return true;
}

function myeditfunction(rowID){
	if($('#pfxRestrictReq').is(':checked'))	{	
		$('#prefixCode').val( innerGrid.cells(rowID,1).getValue());
		innerGrid.deleteRow(rowID);
		$('#prefixCode').focus();
	}
}

function myaddfunction(){
	if($('#pfxRestrictReq').is(':checked'))	{
	    if(prefixCode_val()){
		    $('#gridbox_rowid').val(innerGrid.uid());
		   	var prefixCode = $('#prefixCode').val();
		    if(prefixCodeDuplicateCheck()){
		   		var sl = innerGrid.getRowsNum()+1;
				var field_values=['false' ,prefixCode];
				innerGrid.addRow($('#gridbox_rowid').val(),field_values,innerGrid.getRowsNum());
				$('#gridbox_rowid').val(EMPTY_STRING);
				clearGridFields();
				$('#prefixCode').focus();
				return true;
		    }
		   	else{
		   		setError('prefixCode_error', DUPLICATE_DATA);
		   		return false;
		   	}
		} else {
			return false;
		}
	}
}

function clearGridFields() {
	$('#prefixCode').val(EMPTY_STRING);
	clearError('prefixCode_error');
}

function effectiveDate_val() {
	var value = $('#effectiveDate').val();
	clearError('effectiveDate_error');
	if(!isValidEffectiveDate(value,'effectiveDate_error')){
		return false;
	}
	return true;
}
function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (isEmpty(value)) {
		setError('remarks_error', MANDATORY);
		return false;
	}
	if (!isValidRemarks(value)) {
		setError('remarks_error', INVALID_REMARKS);
		return false;
	}
	return true;
}

function revalidate() {
	if (!effectiveDate_val()) {
		errors++;
	}
	if (!remarks_val()) {
		errors++;
	}
	if ($('#pfxRestrictReq').is(':checked')==true) {
		if(innerGrid.getRowsNum()>0){
			$('#xmlPrefixDetails').val(innerGrid.serialize());
			$('#prefixCode_error').html(EMPTY_STRING);
		}else{
			innerGrid.clearAll();
			$('#xmlPrefixDetails').val(innerGrid.serialize());
			setError('prefixCode_error', ADD_ATLEAST_ONE_ROW);
			errors++;
		}
	}
}