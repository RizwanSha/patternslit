var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'EPWDRESET';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}
function clearFields() {
	$('#userID').val(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
}
function loadData() {
	$('#userID').val(validator.getValue('USER_ID'));
	$('#userID_desc').html(validator.getValue('USERS_USER_NAME'));
	$('#dateOfReset').val(getCBD());
	$('#remarks').val(validator.getValue('REMARKS'));
	loadAuditFields(validator);
}