var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MPRINTQUEUE';
var bothDisable=false;

function init() {
	refreshTBAGrid();
	refreshQueryGrid();
	if (_redisplay) {	
		if ($('#action').val() == ADD) {
			$('#enabled').prop('disabled', true);
			setCheckbox('enabled', YES);
		}
		if (!bothDisable) {	
		branchCode_val();
		branchListCode_val();
		}
	}
}
function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'MPRINTQUEUE_MAIN', queryGrid);
}

function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}

function doclearfields(id) {
	switch (id) {
	case 'printQueueId':
		if (isEmpty($('#printQueueId').val())) {
			$('#printQueueId_error').html(EMPTY_STRING);
			$('#printQueueId_desc').html(EMPTY_STRING);
			clearNonPKFields();
			break;
		}
	}
}

function clearFields() {
	$('#printQueueId').val(EMPTY_STRING);
	$('#printQueueId_error').html(EMPTY_STRING);
	$('#printQueueId_desc').html(EMPTY_STRING);
	clearNonPKFields();
}
function clearNonPKFields() {
	$('#printQueueDescription').val(EMPTY_STRING);
	$('#printQueueDescription_error').html(EMPTY_STRING);

	$('#printTypeId').val(EMPTY_STRING);
	$('#printTypeId_desc').html(EMPTY_STRING);
	$('#printTypeId_error').html(EMPTY_STRING);

	$('#branchCode').val(EMPTY_STRING);
	$('#branchCode_desc').html(EMPTY_STRING);
	$('#branchCode_error').html(EMPTY_STRING);

	$('#branchListCode').val(EMPTY_STRING);
	$('#branchListCode_error').html(EMPTY_STRING);
	$('#branchListCode_desc').html(EMPTY_STRING);

	$('#sourceType_error').html(EMPTY_STRING);

	$('#sourcePath').val(EMPTY_STRING);
	$('#sourcePath_error').html(EMPTY_STRING);

	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);

}

function doHelp(id) {
	switch (id) {
	case 'printQueueId':
		help('COMMON', 'HLP_PRNQUEUE', $('#printQueueId').val(), EMPTY_STRING,
				$('#printQueueId'));
		break;
	case 'printTypeId':
		help('COMMON', 'HLP_PRNTYPE_M', $('#printTypeId').val(), EMPTY_STRING,
				$('#printTypeId'));
		break;
	case 'branchCode':
		help('COMMON', 'HLP_ENTITYBRN_M', $('#branchCode').val(), EMPTY_STRING,
				$('#branchCode'));
		break;
	case 'branchListCode':
		help('COMMON', 'HLP_ENTITYBRNLIST_M', $('#branchListCode').val(),
				EMPTY_STRING, $('#branchListCode'));
		break;
	}
}
function add() {
	$('#printQueueId').focus();
	setCheckbox('enabled', YES);
	$('#enabled').prop('disabled', true);
	branchRequired();
}
function modify() {
	$('#printQueueId').focus();
	if ($('#action').val() == MODIFY) {
		$('#enabled').prop('disabled', false);
	} else {
		$('#enabled').prop('disabled', true);
	}
	branchRequired();
}

function loadData() {
	$('#printQueueId').val(validator.getValue('PRNQ_ID'));
	$('#printQueueId').html(validator.getValue('DESCRIPTION'));
	$('#printQueueDescription').val(validator.getValue('DESCRIPTION'));
	$('#printTypeId').val(validator.getValue('PRNTYPE_ID'));
	$('#printTypeId_desc').html(validator.getValue('F1_DESCRIPTION'));
	$('#branchCode').val(validator.getValue('BRN_CODE'));
	$('#branchCode_desc').html(validator.getValue('F2_DESCRIPTION'));
	$('#branchListCode').val(validator.getValue('BRNLIST_CODE'));
	$('#branchListCode_desc').html(validator.getValue('F3_DESCRIPTION'));
	$('#sourceType').val(validator.getValue('SRC_TYPE'));
	$('#sourcePath').val(validator.getValue('SRC_PATH'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	
	if (!bothDisable) {	
	branchCode_val();
	branchListCode_val();
	}
	resetLoading();
	
}
function view(source, primaryKey) {
	hideParent('access/qprintqueue', source, primaryKey);
	resetLoading();
}
function validate(id) {
	switch (id) {
	case 'printQueueId':
		printQueueId_val();
		break;
	case 'printQueueDescription':
		description_val();
		break;
	case 'printTypeId':
		printTypeId_val();
		break;
	case 'branchCode':
		branchCode_val();
		break;
	case 'branchListCode':
		branchListCode_val();
		break;
	case 'sourceType':
		sourceType_val();
		break;
	case 'sourcePath':
		sourcePath_val();
		break;
	case 'enabled':
		enabled_val();
		break;
	case 'remarks':
		remarks_val();
		break;

	}
}

function backtrack(id) {
	switch (id) {
	case 'printQueueId':
		setFocusLast('printQueueId');
		break;
	case 'printQueueDescription':
		setFocusLast('printQueueId');
		break;
	case 'printTypeId':
		setFocusLast('printQueueDescription');
		break;
	case 'branchCode':
		setFocusLast('printTypeId');
		break;
	case 'branchListCode':
		if ($('#branchCode').is('[readonly]')) {
			setFocusLast('printTypeId');
		} else {
			setFocusLast('branchCode');
		}
		break;
	case 'sourceType':
		if ($('#branchListCode').is('[readonly]')) {
			if ($('#branchCode').is('[readonly]')) {
				setFocusLast('printTypeId');
			} else {
				setFocusLast('branchCode');
			}
		} else {
			setFocusLast('branchListCode');
		}
		break;
	case 'sourcePath':
		setFocusLast('sourceType');
		break;
	case 'enabled':
		setFocusLast('sourcePath');
		break;
	case 'remarks':
		if ($('#action').val() == MODIFY) {
			setFocusLast('enabled');
		} else {
			setFocusLast('sourcePath');
		}
		break;
	}
}

function printQueueId_val() {
	var value = $('#printQueueId').val();
	clearError('printQueueId_error');
	if (isEmpty(value)) {
		setError('printQueueId_error', HMS_MANDATORY);
		return false;
	} else if ($('#action').val() == ADD && !isValidCode(value)) {
		setError('printQueueId_error', HMS_INVALID_FORMAT);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('PRNQ_ID', $('#printQueueId').val());
		validator.setValue(ACTION, $('#action').val());
		validator.setClass('patterns.config.web.forms.access.mprintqueuebean');
		validator.setMethod('validatePrintQueueId');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('printQueueId_error', validator.getValue(ERROR));
			return false;
		} else {
			if ($('#action').val() == MODIFY) {
				PK_VALUE = getEntityCode() + PK_SEPERATOR
						+ $('#printQueueId').val();
				if (!loadPKValues(PK_VALUE, 'printQueueId_error')) {
					return false;
				}
			}
		}
	}
	setFocusLast('printQueueDescription');
	return true;
}

function description_val() {
	var value = $('#printQueueDescription').val();
	clearError('printQueueDescription_error');
	if (isEmpty(value)) {
		setError('printQueueDescription_error', HMS_MANDATORY);
		return false;
	}
	if (!isValidDescription(value)) {
		setError('printQueueDescription_error', HMS_INVALID_DESCRIPTION);
		return false;
	}
	setFocusLast('printTypeId');
	return true;
}

function printTypeId_val() {
	var value = $('#printTypeId').val();
	clearError('printTypeId_error');
	if (isEmpty(value)) {
		setError('printTypeId_error', HMS_MANDATORY);
		return false;
	} else if ($('#action').val() == ADD && !isValidCode(value)) {
		setError('printTypeId_error', HMS_INVALID_FORMAT);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('PRNTYPE_ID', $('#printTypeId').val());
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.access.mprintqueuebean');
		validator.setMethod('validatePrintType');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('printTypeId_error', validator.getValue(ERROR));
			return false;
		} else {
			if (validator.getValue(RESULT) == DATA_AVAILABLE) {
				$('#printTypeId_desc').html(
						validator.getValue('DESCRIPTION'));
			}
		}
	}
	if ($('#branchCode').is('[readonly]')) {
		if ($('#branchListCode').is('[readonly]')) {
			setFocusLast('sourceType');
		} else {
			setFocusLast('branchListCode');
		}
	} else {
		setFocusLast('branchCode');
	}
	return true;

}

function branchCode_val() {
	var value = $('#branchCode').val();
	clearError('branchCode_error');
	clearError('branchListCode_error');
	if (!isEmpty(value)) {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('BRANCH_CODE', $('#branchCode').val());
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.access.mprintqueuebean');
		validator.setMethod('validateEntityBranch');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('branchCode_error', validator.getValue(ERROR));
			return false;
		} else if (validator.getValue(RESULT) == DATA_AVAILABLE) {
			$('#branchCode_desc').html(validator.getValue('DESCRIPTION'));
		}
		$('#branchListCode').val(EMPTY_STRING);
		$('#branchListCode').prop("readonly", true);
		$('#branchListCode_desc').html(EMPTY_STRING);
		
		$('#branchListCode_pic').prop("disabled", true);
		setFocusLast('sourceType');

	} else {
		$('#branchListCode').prop("readonly", false);
		$('#branchListCode_pic').prop("disabled", false);
		setFocusLast('branchListCode');

	}

	return true;

}

function branchListCode_val() {
	var value = $('#branchCode').val();
	clearError('branchListCode_error');
	if (isEmpty(value)) {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('BRNLIST_CODE', $('#branchListCode').val());
		validator.setValue('BRANCH_CODE', $('#branchCode').val());
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.access.mprintqueuebean');
		validator.setMethod('validateEntityBranchList');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('branchListCode_error', validator.getValue(ERROR));
			$('#branchCode').prop("readonly", false);
			$('#branchCode_pic').prop("disabled", false);
			return false;
		} else if (validator.getValue(RESULT) == DATA_AVAILABLE) {
			$('#branchListCode_desc').html(validator.getValue('DESCRIPTION'));

		}
		$('#branchCode').val(EMPTY_STRING);
		$('#branchCode').prop("readonly", true);
		$('#branchCode_desc').html(EMPTY_STRING);

     	$('#branchCode_pic').prop("disabled", true);
	}
	
	setFocusLast('sourceType');

	return true;
}

function sourceType_val() {
	var value = $('#sourceType').val();
	clearError('sourceType_error');
	if (isEmpty(value)) {
		setError('sourceType_error', HMS_MANDATORY);
		return false;
	}
	setFocusLast('sourcePath');
	return true;

}

function sourcePath_val() {
	var value = $('#sourcePath').val();
	clearError('sourcePath_error');
	if ($('#sourceType').val() == 'F') {
		if (isEmpty(value)) {
			setError('sourcePath_error', HMS_MANDATORY);
			return false;
		}
	}
	if (!isValidRemarks(value)) {
		setError('sourcePath_error', PRNT_SOURCEPATH_LENGHT);
		return false;
	}

	setFocusLast('remarks');

	return true;
}

function enabled_val() {
	setFocusLast('remarks');
	return true;
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', HMS_INVALID_REMARKS);
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', HMS_INVALID_REMARKS);
			return false;
		}
	}
	if ($('#action').val() == MODIFY) {
		if (isEmpty(value)) {
			setError('remarks_error', HMS_MANDATORY);
			return false;
		}

	}
	setFocusOnSubmit();
	return true;

}

function branchRequired() {
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue(ACTION, USAGE);
	validator.setClass('patterns.config.web.forms.access.mprintqueuebean');
	validator.setMethod('validateEntitiesCode');
	validator.sendAndReceive();
	if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		var branchReg = validator.getValue('BRANCH_REQD');
		if (branchReg == COLUMN_DISABLE) {
			$('#branchCode').val(EMPTY_STRING);
			$('#branchCode').prop("readonly", true);
			$('#branchCode_pic').prop("disabled", true);

			$('#branchListCode').val(EMPTY_STRING);
			$('#branchListCode').prop("readonly", true);
			$('#branchListCode_pic').prop("disabled", true);
			bothDisable=true;
			return true;
		} else if (branchReg == COLUMN_ENABLE) {
			$('#branchCode').val(EMPTY_STRING);
			$('#branchCode').prop("readonly", false);
			$('#branchCode_pic').prop("disabled", false);

			$('#branchListCode').val(EMPTY_STRING);
			$('#branchListCode').prop("readonly", false);
			$('#branchListCode_pic').prop("disabled", false);
			bothDisable=false;
		}
	} else {
		setError('branchCode_error', validator.getValue(ERROR));
		return false;
	}
	return true;

}


