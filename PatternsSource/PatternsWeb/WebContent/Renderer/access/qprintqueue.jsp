<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.access.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/access/qprintqueue.js" />
	</web:dependencies>
	<web:loadPK />
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="qprintqueue.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:viewContent id="po_view1">
				<web:dividerBlock>
					<web:sectionBlock width="width-100p" align="left">
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="180px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="mprintqueue.printQueueId" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:printTypeDisplay property="printQueueId" id="printQueueId" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="180px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowOdd>
									<web:column>
										<web:legend key="mprintqueue.printQueueDescription" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:descriptionDisplay property="printQueueDescription" id="printQueueDescription" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="mprintqueue.printTypeId" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:printTypeDisplay property="printTypeId" id="printTypeId" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="mprintqueue.branchCode" var="program" />
									</web:column>
									<web:column>
										<type:entityBranchListDisplay property="branchCode" id="branchCode" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="mprintqueue.branchListCode" var="program" />
									</web:column>
									<web:column>
										<type:entityBranchListDisplay property="branchListCode" id="branchListCode" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="mprintqueue.sourceType" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:comboDisplay property="sourceType" id="sourceType" datasourceid="MPRINTQUEUE_SOURCETYPE" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="mprintqueue.sourcePath" var="program" />
									</web:column>
									<web:column>
										<type:remarksDisplay property="sourcePath" id="sourcePath" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="mprintqueue.enabled" var="program" />
									</web:column>
									<web:column>
										<type:checkboxDisplay property="enabled" id="enabled" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="form.remarks" var="common" />
									</web:column>
									<web:column>
										<type:remarksDisplay property="remarks" id="remarks" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column span="2">
										<web:submitReset />
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
						<web:auditDisplay />
					</web:sectionBlock>
				</web:dividerBlock>
			</web:viewContent>
		</web:viewSubPart>
	</web:viewPart>
</web:fragment>