var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'ECONSOLEPGMALLOC';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}

function loads() {
	loadGridQuery(CURRENT_PROGRAM_ID, 'ECONSOLEPGMALLOC_GRID', innerGrid);
}

function clearFields() {
	$('#consoleCode').val(EMPTY_STRING);
	$('#effectiveDate').val(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
}
function loadData() {
	$('#consoleCode').val(validator.getValue('CONSOLE_CODE'));
	$('#consoleCode_desc').html(validator.getValue('F1_DESCRIPTION'));
	$('#effectiveDate').val(validator.getValue('EFFT_DATE'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadGrid();
	loadAuditFields(validator);
}
function loadGrid() {
	SOURCE_VALUE = _tableType;
	PK_VALUE = _primaryKey;
	if (_tableType == MAIN) {
		loads();
	} else if (_tableType == TBA) {
		loads();
	}
}