var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'EUSERCLOSURE';
function init() {
	$('#daySerial').attr('readonly', true);
	$('#entryDate').attr('readonly', true);
	$('#entryDate_pic').attr("disabled", "disabled"); 
	refreshTBAGrid();
	refreshQueryGrid();
	if (_redisplay) {
	}
}
function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'EUSERCLOSURE_MAIN', queryGrid);
}
function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}
function doHelp(id) {
	switch (id) {
	case 'userId':
		help('COMMON', 'USERS_INTERNAL', $('#userId').val(), EMPTY_STRING, $('#userId'));
		break;
	}
}
function clearFields() {
	
	$('#entryDate').val(EMPTY_STRING);
	$('#entryDate_error').html(EMPTY_STRING);
	$('#daySerial').val(EMPTY_STRING);
	$('#userId').val(EMPTY_STRING);
	$('#userId_error').html(EMPTY_STRING);
	$('#userId_desc').html(EMPTY_STRING);
	$('#joinDate').val(EMPTY_STRING);
	$('#addrLine1').val(EMPTY_STRING);
	$('#addr_error').html(EMPTY_STRING);
	$('#addrLine3').val(EMPTY_STRING);
	$('#addrLine4').val(EMPTY_STRING);
	$('#addrLine5').val(EMPTY_STRING);
	$('#addrLocation').val(EMPTY_STRING);
	$('#addrPinCode').val(EMPTY_STRING);
	$('#addrLocation_desc').html(EMPTY_STRING);
	$('#addrCountry').val(EMPTY_STRING);
	$('#addrCountry_desc').html(EMPTY_STRING);
	$('#closureDate').val(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
}
function add() {
	
	$('#entryDate').val(getCBD());
}
function modify() {
	if ($('#action').val() == MODIFY) {
		alert(OPR_NOT_ALLOWED);
	} else {
		
		$('#userId').focus();
	}
}
function loadData() {
	$('#entryDate').val(validator.getValue("ENTRY_DATE"));
	$('#daySerial').attr('readonly', false);
	$('#daySerial').val(validator.getValue("DAY_SL"));
	$('#daySerial').attr('readonly', true);
	$('#userId').val(validator.getValue('USER_ID'));
	$('#userId_desc').html(validator.getValue('USER_NAME'));
	$('#joinDate').val(validator.getValue('USERS_DATE_OF_JOINING'));
	$('#remarks').val(validator.getValue('REMARKS'));
	resetLoading();
	$('#closureDate').val(validator.getValue('DATE_OF_RELIVING'));
}
function view(source, primaryKey) {
	hideParent('access/quserclosure', source, primaryKey);
	resetLoading();
}

function validate(id) {
	switch (id) {
	case 'userId':
		userID_val();
		break;
	case 'remarks':
		remarks_val();
		break;
	case 'closureDate':
		closureDate_val();
		break;
	}
}

function closureDate_val() {
	var value = $('#closureDate').val();
	clearError('closureDate_error');
	if (!isValidEffectiveDate(value, 'closureDate_error')) {
		return false;
	}
	return true;
}
function userID_val() {
	var value = $('#userId').val();
	clearError('userId_error');
	if (isEmpty(value)) {
		setError('userId_error', MANDATORY);
		return false;
	}
	validator.clearMap();
	validator.setValue('USER_ID', value);
	validator.setClass('patterns.config.web.forms.access.euserclosurebean');
	validator.setMethod('getUserDetails');
	validator.sendAndReceive();
	if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		$('#userId_desc').html(validator.getValue('USER_NAME'));
		$('#joinDate').val(validator.getValue('DATE_OF_JOINING'));
		$('#addrLine1').val(validator.getValue('AI_ADDR1'));
		$('#addrLine2').val(validator.getValue('AI_ADDR2'));
		$('#addrLine3').val(validator.getValue('AI_ADDR3'));
		$('#addrLine4').val(validator.getValue('AI_ADDR4'));
		$('#addrLine5').val(validator.getValue('AI_ADDR5'));
		$('#addrLocation').val(validator.getValue('AI_LOC'));
		$('#addrPinCode').val(validator.getValue('AI_PINCODE'));
		$('#addrCountry').val(validator.getValue('AI_COUNTRY'));
	} else {
		setError('userId_error', validator.getValue(ERROR));
	}
	return true;
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (isEmpty(value)) {
		setError('remarks_error', MANDATORY);
		return false;
	}
	if (!isValidRemarks(value)) {
		setError('remarks_error', INVALID_REMARKS);
		return false;
	}
	return true;
}
function revalidate() {
	if (!userID_val()) {
		errors++;
	}
	if (!remarks_val()) {
		errors++;
	}
	if (!closureDate_val()) {
		errors++;
	}
}