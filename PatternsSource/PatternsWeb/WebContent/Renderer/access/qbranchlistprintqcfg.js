var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'IBRANCHLISTPRINTQCFG';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}
function loads() {
	loadGridQuery(CURRENT_PROGRAM_ID, 'IBRANCHLISTPRINTQCFG',ibranchlistprintqcfg_innerGrid);
}

function clearFields() {
	$('#branchListCode').val(EMPTY_STRING);
	$('#effectiveDate').val(EMPTY_STRING);
}
function loadData() {
	$('#branchListCode').val(validator.getValue('BRNLIST_CODE'));
	$('#branchListCode_desc').html(validator.getValue('F1_DESCRIPTION'));
	$('#effectiveDate').val(validator.getValue('EFFT_DATE'));	
	loadAuditFields(validator);
	loadGrid();
	
}
function loadGrid() {
	SOURCE_VALUE = _tableType;
	PK_VALUE = _primaryKey;
	if (_tableType == MAIN) {
		loads();
	} else if (_tableType == TBA) {
		loads();
	}
}