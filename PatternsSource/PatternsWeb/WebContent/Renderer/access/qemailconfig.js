var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'IEMAILCONFIG';
var sendAllowed;
var receiveAlloewd;
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}
function clearFields() {
	$('#emailCode').val(EMPTY_STRING);
	$('#effectiveDate').val(EMPTY_STRING);
	$('#sendProtocol').val(EMPTY_STRING);
	setCheckbox('sslRequired', NO);
	$('#receiveProtocol').val(EMPTY_STRING);
	$('#senderName').val(EMPTY_STRING);
	$('#senderEmail').val(EMPTY_STRING);
	setCheckbox('proxyRequired', NO);
	$('#proxyServer').val(EMPTY_STRING);
	$('#proxyServerPort').val(EMPTY_STRING);
	setCheckbox('proxyServerAuthentication', NO);
	$('#proxyAccountName').val(EMPTY_STRING);
	$('#proxyAccountPassword').val(EMPTY_STRING);
	$('#outgoingMailServer').val(EMPTY_STRING);
	$('#outgoingMailServerPort').val(EMPTY_STRING);
	setCheckbox('outgoingMailServerAuthentication', NO);
	$('#outgoingAccountName').val(EMPTY_STRING);
	$('#outgoingAccountPassword').val(EMPTY_STRING);
}

function loadData() {
	$('#emailCode').val(validator.getValue('EMAIL_CODE'));
	$('#emailCode_desc').html(validator.getValue('F1_EMAIL_DESCN'));
	$('#effectiveDate').val(validator.getValue('EFFT_DATE'));
	$('#sendProtocol').val(validator.getValue('SEND_PROTO_TYPE'));
	setCheckbox('sslRequired', validator.getValue('SSL_REQUIRED'));
	$('#receiveProtocol').val(validator.getValue('RECEIVE_PROTO_TYPE'));
	$('#senderName').val(validator.getValue('COMMUNICATION_NAME'));
	$('#senderEmail').val(validator.getValue('COMMUNICATION_EMAIL'));
	setCheckbox('proxyRequired', validator.getValue('PROXY_REQUIRED'));
	$('#proxyServer').val(validator.getValue('PROXY_SERVER'));
	$('#proxyServerPort').val(validator.getValue('PROXY_SERVER_PORT'));
	setCheckbox('proxyServerAuthentication', validator.getValue('PROXY_AUTH_REQD'));
	$('#proxyAccountName').val(validator.getValue('PROXY_USER_NAME'));
	$('#proxyAccountPassword').val(validator.getValue('PROXY_PASSWORD'));
	$('#outgoingMailServer').val(validator.getValue('OUTGOING_MAIL_SERVER'));
	$('#outgoingMailServerPort').val(validator.getValue('OUTGOING_MAIL_SERVER_PORT'));
	setCheckbox('outgoingMailServerAuthentication', validator.getValue('OUTGOING_AUTH_REQD'));
	$('#outgoingAccountName').val(validator.getValue('OUTGOING_USER_NAME'));
	$('#outgoingAccountPassword').val(validator.getValue('OUTGOING_PASSWORD'));
	$('#remarks').val(validator.getValue('REMARKS'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadAuditFields(validator);
}
