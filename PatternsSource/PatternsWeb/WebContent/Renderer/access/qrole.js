var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MROLE';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}
function clearFields() {
	$('#roleCode').val(EMPTY_STRING);
	$('#roleDescription').val(EMPTY_STRING);
	$('#roleType').val(EMPTY_STRING);
	$('#attributesReq').prop('checked',false);
	setCheckbox('fieldRole', NO);
	setCheckbox('cashierRole', NO);
	setCheckbox('paymentsAllow', NO);
	setCheckbox('receiptsAllow', NO);
	setCheckbox('headCashRole', NO);
	setCheckbox('enabled', NO);
	$('#remarks').val(EMPTY_STRING);
}
function loadData() {
	$('#roleCode').val(validator.getValue('CODE'));
	$('#roleDescription').val(validator.getValue('DESCRIPTION'));
	$('#roleType').val(validator.getValue('ROLE_TYPE'));
	setCheckbox('attributesReq', validator.getValue('SPL_ATTR_REQ'));
	setCheckbox('fieldRole', validator.getValue('FIELD_ROLE'));
	setCheckbox('cashierRole', validator.getValue('CASHIER_ROLE'));
	setCheckbox('paymentsAllow', validator.getValue('PAYMENTS_ALLOWED'));
	setCheckbox('receiptsAllow', validator.getValue('RECEIPTS_ALLOWED'));
	setCheckbox('headCashRole', validator.getValue('HEAD_CASHIER_ROLE'));
	setCheckbox('enabled', validator.getValue('ENABLED'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadAuditFields(validator);
}
