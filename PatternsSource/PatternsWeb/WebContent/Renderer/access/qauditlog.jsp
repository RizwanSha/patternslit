<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<%@taglib prefix="message" tagdir="/WEB-INF/tags/message"%>
<web:bundle baseName="patterns.config.web.forms.access.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/access/qauditlog.js" />
	</web:dependencies>
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="qauditlog.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/access/qauditlog" id="qauditlog" method="POST">
				<web:viewContent id="po_view1">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle width="210px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="form.optionid" var="common" />
										</web:column>
										<web:column span="3">
											<type:optionID property="optionID" id="optionID" helpRequired="true" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="form.userid" var="common" />
										</web:column>
										<web:column span="3">
											<type:userHelp property="userID" id="userID" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle width="250px" />
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="form.fromdate" var="common" mandatory="true" />
										</web:column>
										<web:column>
											<type:date property="fromDate" id="fromDate" />
										</web:column>
										<web:column>
											<web:legend key="form.uptodate" var="common" mandatory="true" />
										</web:column>
										<web:column>
											<type:date property="toDate" id="toDate" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="qauditlog.actiontype" var="program" mandatory="true" />
										</web:column>
										<web:column >
											<type:combo property="actionType" id="actionType" datasourceid="COMMON_QAUDITLOG" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<type:button id="submit" key="form.submit" var="common" onclick="revalidate();" />
											<type:button id="reset" key="form.reset" var="common" onclick="clearFields();" />
										</web:column>
										<web:column align="center">
											<type:button id="cmdView" key="qauditlog.viewdetails" var="program" onclick="doView()" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<message:messageBox id="qauditlog" />
							</web:section>
							<web:viewContent id="po_view4" styleClass="hidden">
								<web:section>
									<web:table>
										<web:columnGroup>
											<web:columnStyle width="200px" />
											<web:columnStyle />
										</web:columnGroup>
										<web:rowOdd>
											<web:column span="2">
												<web:viewContent id="po_view5">
													<web:inlineQueryGrid height="550" width="500" id="qauditlog_innerGrid" />
												</web:viewContent>
											</web:column>
										</web:rowOdd>
									</web:table>
								</web:section>
							</web:viewContent>
						</web:sectionBlock>
					</web:dividerBlock>
				</web:viewContent>
				<web:element property="xmlInnerGrid" />
				<web:element property="gridbox_rowid" />
				<web:element property="command" />
				<web:element property="action" />
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
	<web:contextSearch />
</web:fragment>