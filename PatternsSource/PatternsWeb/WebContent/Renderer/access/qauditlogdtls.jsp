<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.access.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/access/qauditlogdtls.js" />
	</web:dependencies>
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="qauditlogdtls.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/access/qauditlogdtls" id="qauditlogdtls" method="POST">
				<web:viewContent id="po_view1">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle width="220px" />
										<web:columnStyle width="220px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="form.optionid" var="common" />
										</web:column>
										<web:column span="3">
											<type:optionIDDisplay property="optionID" id="optionID" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="qauditlogdtls.primarykey" var="program" />
										</web:column>
										<web:column span="3">
											<type:nameDisplay property="primaryKey" id="primaryKey" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle width="290px" />
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="qauditlogdtls.logdatetime" var="program" />
										</web:column>
										<web:column>
											<type:dateTimeDisplay property="logDateTime" id="logDateTime" />
										</web:column>
										<web:column>
											<web:legend key="qauditlogdtls.logserial" var="program" />
										</web:column>
										<web:column>
											<type:inputCodeDisplay property="logSerial" id="logSerial" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="qauditlogdtls.auction" var="program" />
										</web:column>
										<web:column span="3">
											<type:inputCodeDisplay property="auction" id="auction" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:row>
										<web:column style="vertical-align: top;">
											<web:section>
												<web:viewTitle var="program" key="qauditlogdtls.newimagetitle" />
												<web:table>
													<web:row>
														<web:column style="vertical-align: top;">
															<div id="newImageGrid" class="newImageGrid_div" align="center" style="vertical-align: top;"></div>
														</web:column>
													</web:row>
												</web:table>
												<web:viewTitle var="program" key="qauditlogdtls.oldimagetitle" />
												<web:table>
													<web:row>
														<web:column style="vertical-align: top;">
															<div id="oldImageGrid" class="oldImageGrid_div" align="center" style="vertical-align: top;"></div>
														</web:column>
													</web:row>
												</web:table>
												<web:viewTitle var="program" key="qauditlogdtls.rectifyimagetitle" />
												<web:table>
													<web:row>
														<web:column style="vertical-align: top;">
															<div id="rectifyImageGrid" class="oldImageGrid_div" align="center" style="vertical-align: top;"></div>
														</web:column>
													</web:row>
												</web:table>
											</web:section>
										</web:column>
									</web:row>
								</web:table>
							</web:section>
							<web:element property="hidPrimaryKey" />
						</web:sectionBlock>
					</web:dividerBlock>
				</web:viewContent>
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
	<web:contextSearch />
</web:fragment>