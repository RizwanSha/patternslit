var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = "EUSERACTROLEALLOC";
function init() {
	refreshTBAGrid();
	refreshQueryGrid();
	if (_redisplay) {
		if ($('#enabled').is(':checked')) {
			showFields();
		} else {
			hideFields();
		}

	}
}
function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, "EUSERACTROLEALLOC_MAIN");
}

function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}

function showFields() {
	$('#fromDate').removeAttr("disabled"); 
	$('#fromDate_pic').removeAttr("disabled"); 
	$('#toDate').removeAttr("disabled"); 
	$('#toDate_pic').removeAttr("disabled"); 
}
function hideFields() {
	$('#enabled').prop('checked',false);
	$('#fromDate').attr("disabled", "disabled"); 
	$('#fromDate').val("");
	$('#fromDate_pic').attr("disabled", "disabled"); 
	$('#toDate').attr("disabled", "disabled"); 
	$('#toDate').val("");
	$('#toDate_pic').attr("disabled", "disabled"); 
}
function clearFields() {
	
	$('#userID').val("");
	$('#userID_error').html("");
	$('#roleCode').val("");
	$('#roleCode_error').html("");
	$('#fromDate').val("");
	$('#fromDate_error').html("");
	$('#toDate').val("");
	$('#toDate_error').html("");
}

function add() {
	
	$('#userID').focus();
	hideFields();
}

function modify() {
	
	$('#enabled').focus();

}

function loadData() {
	$('#userID').val(validator.getValue("USER_ID"));
	$('#userID_desc').html(validator.getValue("BANKUSERS_USER_NAME"));
	$('#roleCode').val(validator.getValue("ROLE_CODE"));
	$('#roleCode_desc').html(validator.getValue("BANKROLES_DESCRIPTION"));
	$('#enabled').prop('checked',decodeSTB(validator.getValue("ENABLED")));
	if ($('#enabled').is(':checked')) {
		showFields();
		$('#fromDate').val(validator.getValue("FROM_DATE"));
		$('#toDate').val(validator.getValue("UPTO_DATE"));
	} else {
		hideFields();
	}
	resetLoading();
}

function doHelp(id) {
	switch (id) {
	case "userID":
		help(CURRENT_PROGRAM_ID, "USERIDLIST1", $('#'+"userID").val(), "", $('#'+"userID"));
		break;
	case "roleCode":
		help(CURRENT_PROGRAM_ID, "ROLECODELIST1", $('#'+"roleCode").val(), "", $('#'+"roleCode"));
		break;
	}
}
function checkclick(id) {
	switch (id) {
	case "enabled":
		if ($('#enabled').is(':checked')) {
			showFields();
		} else {
			hideFields();
		}
		break;
	}
}

function view(source, primaryKey) {
	hideParent("access/quseractrolealloc", source, primaryKey);
	resetLoading();
}

function validate(id) {
	switch (id) {
	case "userID":
		userID_val();
		break;
	case "roleCode":
		roleCode_val();
		break;
	case "fromDate":
		fromDate_val();
		break;
	case "toDate":
		toDate_val();
		break;
	}
}

function userID_val() {
	var value = $('#'+"userID").val();
	clearError("userID_error");
	if (isEmpty(value)) {
		setError("userID_error", MANDATORY);
		return false;
	} else if (!isValidUserID(value)) {
		setError("userID_error", INVALID_USERID);
		return false;
	}
	return true;
}

function roleCode_val() {
	var value = $('#'+"roleCode").val();
	clearError("roleCode_error");
	if (isEmpty(value)) {
		setError("roleCode_error", MANDATORY);
		return false;
	} else if (!isValidCode(value)) {
		setError("roleCode_error", INVALID_CODE);
		return false;
	}
	return true;
}

function fromDate_val() {
	var value = $('#'+"fromDate").val();
	var cbd = getCBD();
	if ($('#action').val() == "A") {
		if (isEmpty(value)) {
			setError("fromDate_error", MANDATORY);
			return false;
		}
		if (!isDate(value)) {
			setError("fromDate_error", INVALID_DATE);
			return false;
		}
		if (!isDateGreaterEqual(value, cbd)) {
			setError("fromDate_error", DATE_GECBD);
			return false;
		} else {
			clearError("fromDate_error");
			return true;
		}
	}
	if ($('#action').val() == "M") {
		if (isEmpty(value)) {
			setError("fromDate_error", MANDATORY);
			return false;
		}
		if (!isDate(value)) {
			setError("fromDate_error", INVALID_DATE);
			return false;
		}
		if (!isDateGreaterEqual(value, cbd)) {
			setError("fromDate_error", DATE_GECBD);
			return false;
		} else {
			clearError("fromDate_error");
			return true;
		}
	}
}

function toDate_val() {
	var value = $('#'+"toDate").val();
	var frmdate = $('#'+"fromDate").val();
	clearError("toDate_error");
	var cbd = getCBD();
	if ($('#action').val() == "A") {
		if (isEmpty(value)) {
			setError("toDate_error", MANDATORY);
			return false;
		}
		if (!isDate(value)) {
			setError("toDate_error", INVALID_DATE);
			return false;
		}
		if (!isDateGreaterEqual(value, frmdate)) {
			setError("toDate_error", DATE_GEFD);
			return false;
		} else {
			clearError("toDate_error");
			return true;
		}
	}
	if ($('#action').val() == "M") {
		if (isEmpty(value)) {
			setError("toDate_error", MANDATORY);
			return false;
		}
		if (!isDate(value)) {
			setError("toDate_error", INVALID_DATE);
			return false;
		}
		if (!isDateGreaterEqual(value, frmdate)) {
			setError("toDate_error", DATE_GEFD);
			return false;
		} else {
			clearError("toDate_error");
			return true;
		}
	}
}
function revalidate() {
	if (!userID_val()) {
		errors++;
	}
	if (!roleCode_val()) {
		errors++;
	}
	if ($('#enabled').is(':checked')) {
		if (!fromDate_val()) {
			errors++;
		}
		if (!toDate_val()) {
			errors++;
		}
	}

}