var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'IBRANCHLISTPRINTQCFG';

function init() {
	refreshTBAGrid();
	refreshQueryGrid();
	if (_redisplay) {
		ibranchlistprintqcfg_innerGrid.clearAll();
		if (!isEmpty($('#xmlIbranchlistprintqcfgGrid').val())) {
			ibranchlistprintqcfg_innerGrid.loadXMLString($(
					'#xmlIbranchlistprintqcfgGrid').val());
		}
	}
}

function loadGrid() {
	loadGridQuery(CURRENT_PROGRAM_ID, 'IBRANCHLISTPRINTQCFG',ibranchlistprintqcfg_innerGrid);
}
function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'IBRANCHLISTPRINTQCFG_MAIN');
}
function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}

function doHelp(id) {
	switch (id) {
	case 'branchListCode':
		help('COMMON', 'HLP_ENTITYBRNLIST_M', $('#branchListCode').val(),EMPTY_STRING, $('#branchListCode'));
		break;
	case 'effectiveDate':
		help('IGLOBALPRINTQCFG', 'HLP_EFFT_DATE', $('#effectiveDate').val(),$('#branchListCode').val(), $('#effectiveDate'));
		break;

	case 'printTypeId':
		help('COMMON', 'HLP_PRNTYPE_M', $('#printTypeId').val(), EMPTY_STRING,$('#printTypeId'));
		break;
	case 'printQueueId':
		help('COMMON', 'HLP_PRINTQUEUE_M', $('#printQueueId').val(),$('#branchListCode').val(), $('#printQueueId'));
		break;
	}
}
function clearFields() {
	$('#branchListCode').val(EMPTY_STRING);
	$('#branchListCode_error').html(EMPTY_STRING);
	$('#branchListCode_desc').html(EMPTY_STRING);
	$('#effectiveDate').val(EMPTY_STRING);
	$('#effectiveDate_error').html(EMPTY_STRING);
	clearNonPKFields();
}

function clearNonPKFields() {
	ibranchlistprintqcfg_clearGridFields();	
	ibranchlistprintqcfg_innerGrid.clearAll();
}

function doclearfields(id) {
	switch (id) {
	case 'branchListCode':
		if (isEmpty($('#branchListCode').val())) {
			$('#branchListCode_error').html(EMPTY_STRING);
			$('#branchListCode_desc').html(EMPTY_STRING);
			break;
		}
	case 'effectiveDate':
		if (isEmpty($('#effectiveDate').val())) {
			$('#effectiveDate_error').html(EMPTY_STRING);
			$('#effectiveDate_desc').html(EMPTY_STRING);
			clearNonPKFields();
			break;
		}
	}
}
function add() {
	$('#branchListCode').focus();
	$('#effectiveDate_look').prop('disabled', true);
}
function modify() {
	$('#branchListCode').focus();
	if ($('#action').val() == MODIFY) {
		$('#effectiveDate_look').prop('disabled', false);
	}
}
function loadData() {
	$('#branchListCode').val(validator.getValue('BRNLIST_CODE'));
	$('#branchListCode_desc').html(validator.getValue('F1_DESCRIPTION'));
	$('#effectiveDate').val(validator.getValue('EFFT_DATE'));
	loadGrid();
	resetLoading();
}
function view(source, primaryKey) {
	hideParent('access/qbranchlistprintqcfg', source, primaryKey);
	resetLoading();
}

function validate(id) {
	valMode = true;
	switch (id) {
	case 'branchListCode':
		branchListCode_val(valMode);
		break;
	case 'effectiveDate':
		effectiveDate_val(valMode);
		break;
	case 'printTypeId':
		printTypeId_val(valMode);
		break;
	case 'printQueueId':
		printQueueId_val(valMode);
		break;

	}
}
function backtrack(id) {
	switch (id) {
	case 'branchListCode':
		setFocusLast('branchListCode');
		break;
	case 'effectiveDate':
		setFocusLast('branchListCode');
		break;
	case 'printTypeId':
		setFocusLast('effectiveDate');
		break;
	case 'printQueueId':
		setFocusLast('printTypeId');
		break;
	}
}
function gridExit(id) {
	switch (id) {
	case 'printTypeId':
		$('#xmlIbranchlistprintqcfgGrid').val(ibranchlistprintqcfg_innerGrid.serialize());
		setFocusOnSubmit();
		break;
	}
}
function gridDeleteRow(id){
	switch (id) {
	case 'printTypeId':
		deleteGridRecord(ibranchlistprintqcfg_innerGrid);
		break;
	}
}

function branchListCode_val(valMode) {
	var value = $('#branchListCode').val();
	clearError('branchListCode_error');
	if (isEmpty(value)) {
		setError('branchListCode_error', HMS_MANDATORY);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('BRNLIST_CODE', $('#branchListCode').val());
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.access.ibranchlistprintqcfgbean');
		validator.setMethod('validateBranchListCode');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('branchListCode_error', validator.getValue(ERROR));
			return false;
		} else {
			$('#branchListCode_desc').html(validator.getValue('DESCRIPTION'));

		}

	}
	setFocusLast('effectiveDate');
	return true;

}

function effectiveDate_val(valMode) {
	var value = $('#effectiveDate').val();
	clearError('effectiveDate_error');
	if (!isValidEffectiveDate(value, 'effectiveDate_error')) {
		return false;
	} else {
		if ($('#action').val() == ADD) {
			validator.clearMap();
			validator.setMtm(false);
			validator.setValue('BRNLIST_CODE', $('#branchListCode').val());
			validator.setValue('EFFT_DATE', $('#effectiveDate').val());
			validator.setValue('ACTION', $('#action').val());
			validator.setClass('patterns.config.web.forms.access.ibranchlistprintqcfgbean');
			validator.setMethod('validateEffectiveDate');
			validator.sendAndReceive();
			if (validator.getValue(ERROR) != EMPTY_STRING) {
				setError('effectiveDate_error', validator.getValue(ERROR));
				return false;
			}
		} else {
			PK_VALUE = getEntityCode() + PK_SEPERATOR + $('#branchListCode').val() + PK_SEPERATOR + getTBADateFormat($('#effectiveDate').val());
			loadPKValues(PK_VALUE, 'effectiveDate_error');

		}
	}
	setFocusLast('printTypeId');
	return true;
}

function printTypeId_val(valMode) {
	var value = $('#printTypeId').val();
	clearError('printTypeId_error');
	if (isEmpty(value)) {
		setError('printTypeId_error', HMS_MANDATORY);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('PRNTYPE_ID', $('#printTypeId').val());
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.access.ibranchlistprintqcfgbean');
		validator.setMethod('validatePrintTypeId');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('printTypeId_error', validator.getValue(ERROR));
			return false;
		} else {
			$('#printTypeId_desc').html(validator.getValue('DESCRIPTION'));

		}

	}
	setFocusLast('printQueueId');
	return true;

}

function printQueueId_val(valMode) {
	var value = $('#printQueueId').val();
	clearError('printQueueId_error');
	if (isEmpty(value)) {
		setError('printQueueId_error', HMS_MANDATORY);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('BRNLIST_CODE', $('#branchListCode').val());
		validator.setValue('PRNQ_ID', $('#printQueueId').val());
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.access.ibranchlistprintqcfgbean');
		validator.setMethod('validatePrintQueueId');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('printQueueId_error', validator.getValue(ERROR));
			return false;
		} else {
			$('#printQueueId_desc').html(validator.getValue('DESCRIPTION'));

		}

	}
	if (valMode) {
		setFocusLastOnGridSubmit('ibranchlistprintqcfg_innerGrid');
	}
	return true;

}

function ibranchlistprintqcfg_gridDuplicateCheck(editMode, editRowId) {
	var currentValue = [ $('#printTypeId').val() ];
	return _grid_duplicate_check(ibranchlistprintqcfg_innerGrid, currentValue, [ 1 ]);
}

function ibranchlistprintqcfg_clearGridFields() {
	$('#printTypeId').val(EMPTY_STRING);
	$('#printTypeId_desc').html(EMPTY_STRING);
	$('#printTypeId_error').html(EMPTY_STRING);
	$('#printQueueId').val(EMPTY_STRING);
	$('#printQueueId_desc').html(EMPTY_STRING);
	$('#printQueueId_error').html(EMPTY_STRING);
}
function ibranchlistprintqcfg_editGrid(rowId) {
	$('#printTypeId').val(ibranchlistprintqcfg_innerGrid.cells(rowId, 1).getValue());
	$('#printTypeId_desc').html(ibranchlistprintqcfg_innerGrid.cells(rowId, 2).getValue());
	$('#printQueueId').val(ibranchlistprintqcfg_innerGrid.cells(rowId, 3).getValue());
	$('#printQueueId_desc').html(ibranchlistprintqcfg_innerGrid.cells(rowId, 4).getValue());
	$('#printTypeId').focus();
}
function revalidate() {

	ibranchlistprintqcfg_revaildateGrid();

}
function ibranchlistprintqcfg_revaildateGrid() {
	if (ibranchlistprintqcfg_innerGrid.getRowsNum() > 0) {
		$('#xmlIbranchlistprintqcfgGrid').val(ibranchlistprintqcfg_innerGrid.serialize());
		$('#printTypeId_error').html(EMPTY_STRING);
	} else {
		ibranchlistprintqcfg_innerGrid.clearAll();
		$('#xmlIbranchlistprintqcfgGrid').val(ibranchlistprintqcfg_innerGrid.serialize());
		setError('printTypeId_error', HMS_ADD_ATLEAST_ONE_ROW);
		errors++;
	}

}
function ibranchlistprintqcfg_addGrid(editMode, editRowId) {

	if (printTypeId_val(false) && printQueueId_val(false)) {
		if (ibranchlistprintqcfg_gridDuplicateCheck(editMode, editRowId)) {
			var field_values = [ 'false', $('#printTypeId').val(), $('#printTypeId_desc').html(), $('#printQueueId').val(), $('#printQueueId_desc').html() ];
			_grid_updateRow(ibranchlistprintqcfg_innerGrid, field_values);
			ibranchlistprintqcfg_clearGridFields();
			$('#printTypeId').focus();
			return true;
		} else {
			setError('printTypeId_error', HMS_DUPLICATE_DATA);
			$('#printTypeId').focus();
			return false;
		}
	} else {
		
		$('#printTypeId').focus();
		return false;
	}
}

function ibranchlistprintqcfg_cancelGrid(rowId) {
	ibranchlistprintqcfg_clearGridFields();
}
