var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MPKICERTAUTH';
function init() {
	refreshQueryGrid();
	refreshTBAGrid();
	if (_redisplay) {
		if ($('#action').val() == ADD) {
			$('#enabled').prop('disabled', true);
			$('#enabled').prop('checked',true);
		}
		if (!$('#ocspAvail').is(':checked')) {
			$('#ocspHttpUrl').attr("disabled", "disabled"); 
			$('#ocspHttpUrl').val(EMPTY_STRING);
		}
		enableParentCode();
	}
}
function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'MPKICERTAUTH_MAIN', queryGrid);
}
function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}
function clearFields() {
	
	$('#code').val(EMPTY_STRING);
	$('#code_error').html(EMPTY_STRING);
	$('#description').val(EMPTY_STRING);
	$('#description_error').html(EMPTY_STRING);
	$('#heirarchyLevel').val(EMPTY_STRING);
	$('#heirarchyLevel_error').html(EMPTY_STRING);
	$('#parentCode').val(EMPTY_STRING);
	$('#parentCode_error').html(EMPTY_STRING);
	$('#parentCode_desc').html(EMPTY_STRING);
	$('#certAuthHttpUrl').val(EMPTY_STRING);
	$('#certAuthHttpUrl_error').html(EMPTY_STRING);
	$('#certAuthLdapUrl').val(EMPTY_STRING);
	$('#certAuthLdapUrl_error').html(EMPTY_STRING);
	$('#ocspAvail').prop('checked',false);
	$('#ocspHttpUrl').val(EMPTY_STRING);
	$('#ocspHttpUrl_error').html(EMPTY_STRING);
	$('#crlHttpUrl').val(EMPTY_STRING);
	$('#crlHttpUrl_error').html(EMPTY_STRING);
	$('#httpRefreshTime').val(EMPTY_STRING);
	$('#httpRefreshTime_error').html(EMPTY_STRING);
	$('#crlLdapUrl').val(EMPTY_STRING);
	$('#crlLdapUrl_error').html(EMPTY_STRING);
	$('#ldapRefreshTime').val(EMPTY_STRING);
	$('#ldapRefreshTime_error').html(EMPTY_STRING);
	$('#enabled').prop('checked',false);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
}

function add() {
	
	$('#code').focus();
	$('#enabled').prop('checked',true);
	$('#enabled').prop('disabled', true);
	$('#ocspAvail').prop('checked',false);
	$('#ocspHttpUrl').attr("disabled", "disabled"); 
	$('#parentCode').attr("disabled", "disabled"); 
	$('#parentCode_pic').attr("disabled", "disabled"); 
}
function modify() {
	
	$('#code').focus();
	if ($('#action').val() == ADD) {
		$('#enabled').prop('disabled', true);
		$('#heirarchyLevel').removeAttr("disabled"); 
		$('#parentCode').removeAttr("disabled"); 
		$('#enabled').prop('checked',true);
	} else {
		$('#enabled').prop('disabled', true);
		$('#heirarchyLevel').attr("disabled", "disabled"); 
		$('#parentCode').attr("disabled", "disabled"); 
	}
}
function change(id) {
	switch (id) {
	case 'heirarchyLevel':
		enableParentCode();
		break;
	}
}

function doHelp(id) {
	switch (id) {
	case 'parentCode':
		if (heirarchyLevel_val()) {
			help(CURRENT_PROGRAM_ID, 'PARENTCODE', $('#parentCode').val(),parseInt($('#heirarchyLevel').val())-1, $('#parentCode'));
		}
		break;
	}
}
function enableParentCode() {
	$('#heirarchyLevel_error').html(EMPTY_STRING);
	var value = $('#heirarchyLevel').val();
	if (value == '1' || value == EMPTY_STRING) {
		$('#parentCode').attr("disabled", "disabled"); 
		$('#parentCode_pic').attr("disabled", "disabled"); 
		$('#parentCode').val(EMPTY_STRING);
		$('#parentCode_error').html(EMPTY_STRING);
	} else {
		$('#parentCode').removeAttr("disabled"); 
		$('#parentCode_pic').removeAttr("disabled"); 
		$('#parentCode').focus();
	}
}
function view(source, primaryKey) {
	hideParent('access/qpkicertauth', source, primaryKey);
	resetLoading();
}

function loadData() {
	$('#code').val(validator.getValue('CODE'));
	$('#description').val(validator.getValue('DESCRIPTION'));
	$('#certAuthHttpUrl').val(validator.getValue('CERT_AUTH_HTTP_URL'));
	$('#certAuthLdapUrl').val(validator.getValue('CERT_AUTH_LDAP_URL'));
	$('#ocspAvail').prop('checked',decodeSTB(validator.getValue('OCSP_VERIFY_AVLBL')));
	if (!$('#ocspAvail').is(':checked')) {
		$('#ocspHttpUrl').attr("disabled", "disabled"); 
		$('#ocspHttpUrl').val(EMPTY_STRING);
	} else {
		$('#ocspHttpUrl').removeAttr("disabled"); 
		$('#ocspHttpUrl').val(validator.getValue('OCSP_HTTP_URL'));
	}
	$('#heirarchyLevel').val(validator.getValue('HIERARCHY_LEVEL'));
	if ($('#heirarchyLevel').val() != '1') {
		$('#parentCode').val(validator.getValue('PARENT_CODE'));
		$('#parentCode_desc').html(validator.getValue('PKICERTAUTH_DESCRIPTION'));
	} else {
		$('#parentCode').attr("disabled", "disabled"); 
		$('#parentCode_pic').attr("disabled", "disabled"); 
	}
	$('#crlHttpUrl').val(validator.getValue('CRL_HTTP_URL'));
	$('#httpRefreshTime').val(validator.getValue('HTTP_REFRESH_INTERVAL'));
	$('#crlLdapUrl').val(validator.getValue('CRL_LDAP_URL'));
	$('#ldapRefreshTime').val(validator.getValue('LDAP_REFRESH_INTERVAL'));
	if ($('#action').val() == MODIFY) {
		$('#enabled').prop('checked',decodeSTB(validator.getValue('ENABLED')));
	} else {
		$('#enabled').prop('checked',false);
	}
	$('#remarks').val(validator.getValue('REMARKS'));
	resetLoading();
}

function validate(id) {
	switch (id) {
	case 'code':
		code_val();
		break;
	case 'description':
		description_val();
		break;
	case 'heirarchyLevel':
		heirarchyLevel_val();
		break;
	case 'parentCode':
		parentCode_val();
		break;
	case 'certAuthHttpUrl':
		certAuthHttpUrl_val();
		break;
	case 'certAuthLdapUrl':
		certAuthLdapUrl_val();
		break;
	case 'ocspHttpUrl':
		ocspHttpUrl_val();
		break;
	case 'crlHttpUrl':
		crlHttpUrl_val();
		break;
	case 'httpRefreshTime':
		httpRefreshTime_val();
		break;
	case 'crlLdapUrl':
		crlLdapUrl_val();
		break;
	case 'ldapRefreshTime':
		ldapRefreshTime_val();
		break;
	case 'remarks':
		remarks_val();
		break;
	}
}
function checkclick(id) {
	switch (id) {
	case 'ocspAvail':
		if ($('#ocspAvail').is(':checked')) {
			$('#ocspHttpUrl').removeAttr("disabled"); 
			$('#ocspHttpUrl').val(EMPTY_STRING);
			$('#ocspHttpUrl_error').html(EMPTY_STRING);
		} else {
			$('#ocspHttpUrl').attr("disabled", "disabled"); 
			$('#ocspHttpUrl').val(EMPTY_STRING);
			$('#ocspHttpUrl_error').html(EMPTY_STRING);
		}
		break;
	}
}
function code_val() {
	var value = $('#code').val();
	clearError('code_error');
	if (isEmpty(value)) {
		setError('code_error', MANDATORY);
		return false;
	}
	if (!isValidCode(value)) {
		setError('code_error', INVALID_CODE);
		return false;
	}
	return true;
}
function description_val() {
	var value = $('#description').val();
	clearError('description_error');
	if (isEmpty(value)) {
		setError('description_error', MANDATORY);
		return false;
	}
	if (!isValidDescription) {
		setError('description_error', INVALID_DESCRIPTION);
		return false;
	}
	return true;
}
function heirarchyLevel_val() {
	var value = $('#heirarchyLevel').val();
	clearError('heirarchyLevel_error');
	if (isEmpty(value)) {
		setError('heirarchyLevel_error', MANDATORY);
		return false;
	}
	return true;
}
function parentCode_val() {
	if ($('#heirarchyLevel').val() != '1' && $('#heirarchyLevel').val() != EMPTY_STRING) {
		var value = $('#parentCode').val();
		clearError('parentCode_error');
		if (isEmpty(value)) {
			setError('parentCode_error', MANDATORY);
			return false;
		}
	}
	return true;
}
function certAuthHttpUrl_val() {
	var value = $('#certAuthHttpUrl').val();
	clearError('certAuthHttpUrl_error');
	if (!isValidUrl(value)) {
		setError('certAuthHttpUrl_error', INVALID_URL);
		return false;
	}
	return true;
}
function certAuthLdapUrl_val() {
	var value = $('#certAuthLdapUrl').val();
	clearError('certAuthLdapUrl_error');
	if (!isValidUrl(value)) {
		setError('certAuthLdapUrl_error', INVALID_URL);
		return false;
	}
	return true;
}
function ocspHttpUrl_val() {
	if ($('#ocspAvail').is(':checked') == true) {
		var value = $('#ocspHttpUrl').val();
		clearError('ocspHttpUrl_error');
		if (isEmpty(value)) {
			setError('ocspHttpUrl_error', MANDATORY);
			return false;
		}
		if (!isValidUrl(value)) {
			setError('ocspHttpUrl_error', INVALID_URL);
			return false;
		}
	}
	return true;
}
function crlHttpUrl_val() {
	var value = $('#crlHttpUrl').val();
	clearError('crlHttpUrl_error');
	if (!isValidUrl(value)) {
		setError('crlHttpUrl_error', INVALID_URL);
		return false;
	}
	return true;
}
function httpRefreshTime_val() {
	clearError('httpRefreshTime_error');
	if (!isEmpty($('#crlHttpUrl').val())) {
		var value = $('#httpRefreshTime').val();
		if (isEmpty(value)) {
			setError('httpRefreshTime_error', MANDATORY);
			return false;
		}
		if (!isNumeric(value)) {
			setError('httpRefreshTime_error', INVALID_NUMBER);
			return false;
		}
		if (!isValidRefreshTime(value)) {
			setError('httpRefreshTime_error', INVALID_REFERESH_TIME);
			return false;
		}
	}
	return true;
}
function crlLdapUrl_val() {

	var value = $('#crlLdapUrl').val();
	clearError('crlLdapUrl_error');
	if (!isValidUrl(value)) {
		setError('crlLdapUrl_error', INVALID_URL);
		return false;
	}
	return true;
}

function ldapRefreshTime_val() {
	clearError('ldapRefreshTime_error');
	if (!isEmpty($('#crlHttpUrl').val())) {
		var value = $('#ldapRefreshTime').val();
		if (isEmpty(value)) {
			setError('ldapRefreshTime_error', MANDATORY);
			return false;
		}
		if (!isNumeric(value)) {
			setError('ldapRefreshTime_error', INVALID_NUMBER);
			return false;
		}
		if (!isValidRefreshTime(value)) {
			setError('ldapRefreshTime_error', INVALID_REFERESH_TIME);
			return false;
		}
	}
	return true;
}
function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (isEmpty(value)) {
		setError('remarks_error', MANDATORY);
		return false;
	}
	if (!isValidRemarks(value)) {
		setError('remarks_error', INVALID_REMARKS);
		return false;
	}
	return true;
}
function revalidate() {
	if (!code_val()) {
		errors++;
	}
	if (!description_val()) {
		errors++;
	}
	if (!certAuthHttpUrl_val()) {
		errors++;
	}
	if (!certAuthLdapUrl_val()) {
		errors++;
	}
	if (!ocspHttpUrl_val()) {
		errors++;
	}
	if (!crlHttpUrl_val()) {
		errors++;
	}
	if (!httpRefreshTime_val()) {
		errors++;
	}
	if (!crlLdapUrl_val()) {
		errors++;
	}
	if (!ldapRefreshTime_val()) {
		errors++;
	}
	if (!remarks_val()) {
		errors++;
	}
	if (!heirarchyLevel_val()) {
		errors++;
	}
	if (!parentCode_val()) {
		errors++;
	}
}