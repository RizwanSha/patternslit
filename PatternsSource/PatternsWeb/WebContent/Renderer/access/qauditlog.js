var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'QAUDITLOG';
var inlineGridSuccessful = null;

function init() {
	$('#optionID').focus();
	clearFields();
	$('#cmdView').attr("disabled", "disabled");
}

function loadAuditLogGrid() {
	$('#po_view4').removeClass('hidden');
	showMessage(getProgramID(), Message.PROGRESS);
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('OPTION_ID', $('#optionID').val());
	validator.setValue('USER_ID', $('#userID').val());
	validator.setValue('FROM_DATE', $('#fromDate').val());
	validator.setValue('TO_DATE', $('#toDate').val());
	validator.setValue('ACTION_TYPE', $('#actionType').val());
	validator.setClass('patterns.config.web.forms.access.qauditlogbean');
	validator.setMethod('loadAuditLogGrid');
	validator.sendAndReceiveAsync(showStatus);
}

function showStatus() {
	if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		var args=validator.getValue("INV_NO");
		inlineGridSuccessful = loadInlineQueryGrid(CURRENT_PROGRAM_ID, 'QAUDITLOG_MAIN', 'qauditlog_innerGrid',args);
		inlineGridSuccessful.attachEvent("onRowSelect", doRowSelect);
		inlineGridSuccessful.attachEvent("onRowDblClicked", doRowDblClicked);
		//window.scrollTo(0, 210);
		$('#cmdView').removeAttr("disabled");
		hideMessage(getProgramID());
	} else {
		setError('optionID_error', validator.getValue(ERROR));
		hideMessage(getProgramID());
	}
}

function doRowSelect(rowID) {
	inlineGridSuccessful.clearSelection();
	inlineGridSuccessful.uncheckAll();
	inlineGridSuccessful.selectRowById(rowID, true);
	inlineGridSuccessful.cells(rowID, 0).setValue(true);
}

function doRowDblClicked(rowID) {
	inlineGridSuccessful.clearSelection();
	inlineGridSuccessful.uncheckAll();
	inlineGridSuccessful.selectRowById(rowID, true);
	inlineGridSuccessful.cells(rowID, 0).setValue(true);
	doView();
}

function doView() {
	var rowID = inlineGridSuccessful.getCheckedRows(0);
	if (rowID == EMPTY_STRING) {
		alert(SELECT_ATLEAST_ONE);
		return;
	} else {
		var checkedrows = rowID.split(',');
		if (checkedrows.length > 1) {
			alert(SELECT_ONE);
			return;
		}
		var optionID = inlineGridSuccessful.cells(rowID, 4).getValue();
		var pk = inlineGridSuccessful.cells(rowID, 6).getValue();
		var dateTime = inlineGridSuccessful.cells(rowID, 2).getValue();
		var sl = inlineGridSuccessful.cells(rowID, 3).getValue();

		var primaryKey = getEntityCode() + '/' + optionID + '/' + pk + '/' + dateTime + '/' + sl;
		window.scroll(0, 0);
		showWindow('PREVIEW_VIEW', getBasePath() + 'Renderer/access/qauditlogdtls.jsp', QAUDITLOG_VIEW_TITLE, PK + '=' + primaryKey, true, false, false, 980, 620);
		resetLoading();
	}
}

function clearFields() {
	$('#optionID').val(EMPTY_STRING);
	$('#optionID_desc').html(EMPTY_STRING);	
	$('#optionID_error').html(EMPTY_STRING);
	$('#userID').val(EMPTY_STRING);
	$('#userID_desc').html(EMPTY_STRING);
	$('#userID_error').html(EMPTY_STRING);
	$('#fromDate').val(EMPTY_STRING);
	$('#fromDate_error').html(EMPTY_STRING);
	$('#toDate').val(EMPTY_STRING);
	$('#toDate_error').html(EMPTY_STRING);
	$('#actionType').val(EMPTY_STRING);
	$('#actionType_error').html(EMPTY_STRING);
	$('#fromDate').val(getCBD());
	$('#toDate').val(getCBD());
	$('#po_view4').addClass('hidden');
}

function change(id) {
	$('#po_view4').addClass('hidden');
	$('#cmdView').attr("disabled", "disabled");
}

function doHelp(id) {
	switch (id) {
	case 'optionID':
		help(CURRENT_PROGRAM_ID, 'OPTIONS', $('#optionID').val(), EMPTY_STRING, $('#optionID'));
		break;
	case 'userID':
		help('COMMON', 'USERS_INTERNAL', $('#userID').val(), EMPTY_STRING, $('#userID'));
		break;
	}
}

function validate(id) {
	valMode=true;
	switch (id) {
	case 'optionID':
		optionID_val(valMode);
		break;
	case 'userID':
		userID_val(valMode);
		break;
	case 'fromDate':
		fromDate_val(valMode);
		break;
	case 'toDate':
		toDate_val(valMode);
		break;
	case 'actionType':
		actionType_val(valMode);
		break;
	}
}

function backtrack(id) {
	switch (id) {
	case 'optionID':
		setFocusLast('optionID');
		break;
	case 'userID':
		setFocusLast('optionID');
		break;
	case 'fromDate':
		setFocusLast('userID');
		break;
	case 'toDate':
		setFocusLast('fromDate');
		break;
	case 'actionType':
		setFocusLast('toDate');
		break;
	case 'submit':
		setFocusLast('actionType');
		break;
	}
}

function optionID_val(valMode) {
	var value = $('#optionID').val();
	clearError('optionID_error');
	if (!isEmpty(value)) {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('MPGM_ID', $('#optionID').val());
		validator.setClass('patterns.config.web.forms.access.qauditlogbean');
		validator.setMethod('validateOptionID');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('optionID_error', validator.getValue(ERROR));
			return false;
		} else if (validator.getValue(RESULT) == DATA_AVAILABLE) {
			$('#optionID_desc').html(validator.getValue("MPGM_DESCN"));
		}
	}
	setFocusLast('userID');
	return true;
}

function userID_val(valMode) {
	var value = $('#userID').val();
	clearError('userID_error');
	if (!isEmpty(value)) {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('USER_ID', $('#userID').val());
		validator.setClass('patterns.config.web.forms.access.qauditlogbean');
		validator.setMethod('validateUserID');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('userID_error', validator.getValue(ERROR));
			return false;
		} else if (validator.getValue(RESULT) == DATA_AVAILABLE) {
			$('#userID_desc').html(validator.getValue("USER_NAME"));
		}
	}
	setFocusLast('fromDate');
	return true;
}

function fromDate_val(valMode) {
	var value = $('#fromDate').val();
	clearError('fromDate_error');
	if (isEmpty(value)) {
		setError('fromDate_error', MANDATORY);
		return false;
	}
	if (!isDate(value)) {
		setError('fromDate_error', INVALID_DATE);
		return false;
	}
	if (!isDateLesserEqual(value, getCBD())) {
		setError('fromDate_error', DATE_LECBD);
		return false;
	}
	setFocusLast('toDate');
	return true;
}

function toDate_val(valMode) {
	var value = $('#toDate').val();
	var fromDate = $('#fromDate').val();
	clearError('toDate_error');
	if (isEmpty(value)) {
		setError('toDate_error', MANDATORY);
		return false;
	}
	if (!isDate(value)) {
		setError('toDate_error', INVALID_DATE);
		return false;
	}
	if (!isDateLesserEqual(value, getCBD())) {
		setError('toDate_error', DATE_LECBD);
		return false;
	}
	if (!isEmpty(fromDate)) {
		if (!isDateGreaterEqual(value, fromDate)) {
			setError('toDate_error', DATE_GEFD);
			return false;
		}
	}
	setFocusLast('actionType');
	return true;
}

function actionType_val(valMode) {
	var value = $('#actionType').val();
	clearError('actionType_error');
	if (isEmpty(value)) {
		setError('actionType_error', MANDATORY);
		return false;
	}
	setFocusLast('submit');
	return true;
}

function revalidate() {
	if (!optionID_val(false)) {
		errors++;
	}
	if (!userID_val(false)) {
		errors++;
	}
	if (!fromDate_val(false)) {
		errors++;
	}
	if (!toDate_val(false)) {
		errors++;
	}
	if (!actionType_val(false)) {
		errors++;
	}
	if (errors == 0) {
		loadAuditLogGrid();
	} else {
		errors = 0;
		$('#po_view4').addClass('hidden');
	}
}