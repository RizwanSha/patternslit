<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.access.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/access/qbrnlist.js" />
	</web:dependencies>
	<web:loadPK />
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="qbrnlist.querytitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:viewContent id="po_view1">
				<web:dividerBlock>
					<web:sectionBlock width="width-100p" align="left">
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="180px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="form.brnlistcode" var="common" mandatory="true" />
									</web:column>
									<web:column>
										<type:inputCodeDisplay property="brnListCode" id="brnListCode" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="180px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowOdd>
									<web:column>
										<web:legend key="form.description" var="common" mandatory="true" />
									</web:column>
									<web:column>
										<type:descriptionDisplay property="brnListDescription" id="brnListDescription" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="form.enabled" var="common" />
									</web:column>
									<web:column>
										<type:checkboxDisplay property="enabled" id="enabled" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="form.remarks" var="common" mandatory="true" />
									</web:column>
									<web:column>
										<type:remarksDisplay property="remarks" id="remarks" />
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
						<web:section>
							<web:viewTitle var="program" key="mbrnlist.dividertitle" />
							<web:table>
								<web:rowEven>
									<web:column>
										<web:viewContent id="po_view4">
											<web:grid height="200" width="350" id="brnGrid" src="access/qbrnlist_Grid.xml">
											</web:grid>
										</web:viewContent>
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
					</web:sectionBlock>
				</web:dividerBlock>
				<web:dividerBlock>
					<web:sectionBlock width="width-100p" align="left">
						<web:auditDisplay />
					</web:sectionBlock>
				</web:dividerBlock>
			</web:viewContent>
		</web:viewSubPart>
	</web:viewPart>
</web:fragment>