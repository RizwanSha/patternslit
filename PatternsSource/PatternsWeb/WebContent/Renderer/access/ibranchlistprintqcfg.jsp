<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.access.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/access/ibranchlistprintqcfg.js" />
	</web:dependencies>
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="ibranchlistprintqcfg.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/access/ibranchlistprintqcfg" id="ibranchlistprintqcfg" method="POST">
				<web:dividerBlock>
					<type:optionBox add="true" modify="true" view="true" />
				</web:dividerBlock>
				<web:viewContent id="po_view1" styleClass="hidden">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="ibranchlistprintqcfg.branchlistcode" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:entityBranchList property="branchListCode" id="branchListCode" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="ibranchlistprintqcfg.effectivedate" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:date property="effectiveDate" id="effectiveDate" lookup="true" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:viewTitle var="program" key="ibranchlistprintqcfg.printerdetailsconfiguration" />
									<web:rowEven>
										<web:column>
											<web:legend key="ibranchlistprintqcfg.printtypeid" var="program"/>
										</web:column>
										<web:column>
											<type:printerId property="printTypeId" id="printTypeId" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="ibranchlistprintqcfg.printqueueid" var="program"/>
										</web:column>
										<web:column>
											<type:printQueue property="printQueueId" id="printQueueId" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:rowEven>
										<web:column>
											<web:gridToolbar gridName="ibranchlistprintqcfg_innerGrid" cancelFunction="ibranchlistprintqcfg_cancelGrid" editFunction="ibranchlistprintqcfg_editGrid" addFunction="ibranchlistprintqcfg_addGrid" continuousEdit="true" />
											<web:element property="xmlIbranchlistprintqcfgGrid" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:viewContent id="po_view4">
												<web:grid height="240px" width="480px" id="ibranchlistprintqcfg_innerGrid" src="access/ibranchlistprintqcfg_innerGrid.xml">
												</web:grid>
											</web:viewContent>
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column span="2">
											<web:submitReset />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
						</web:sectionBlock>
					</web:dividerBlock>
				</web:viewContent>
				<web:element property="command" />
				<web:element property="action" />
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
	<web:queryGrid />
	<web:TBAGrid />
	<web:contextSearch />
</web:fragment>