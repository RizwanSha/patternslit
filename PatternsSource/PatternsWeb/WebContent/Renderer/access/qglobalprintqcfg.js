var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'IGLOBALPRINTQCFG';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}

function loads() {
	loadGridQuery(CURRENT_PROGRAM_ID, 'IGLOBALPRINTQCFG_GRID', innerGrid);
}

function clearFields() {
	$('#effectiveDate').val(EMPTY_STRING);
}
function loadData() {
	$('#effectiveDate').val(validator.getValue('EFFT_DATE'));
	loadGrid();
	loadAuditFields(validator);
}
function loadGrid() {
	SOURCE_VALUE = _tableType;
	PK_VALUE = _primaryKey;
	if (_tableType == MAIN) {
		loads();
	} else if (_tableType == TBA) {
		loads();
	}
}