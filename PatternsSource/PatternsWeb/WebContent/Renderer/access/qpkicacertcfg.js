var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'IPKICACERTCFG';
var inventoryNum = EMPTY_STRING;
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}
function clearFields() {
	$('#rootCaCode').val(EMPTY_STRING);
	$('#effectiveDate').val(EMPTY_STRING);
	$('#revoked').prop('checked',false);
	$('#remarks').val(EMPTY_STRING);
}
function loadData() {
	$('#rootCaCode').val(validator.getValue('CA_CODE'));
	$('#rootCaCode_desc').html(validator.getValue('PKICERTAUTH_DESCRIPTION'));
	$('#effectiveDate').val(validator.getValue('EFFT_DATE'));
	$('#revoked').prop('checked',decodeSTB(validator.getValue('REVOKED')));
	if ($('#revoked').is(':checked')) {
		$('#cmdPreview').attr("disabled", "disabled"); 
	} else {
		$('#cmdPreview').removeAttr("disabled"); 
	}
	$('#remarks').val(validator.getValue('REMARKS'));
	$('#fileInventoryNumber').val(validator.getValue('INV_NUM'));
	$('#fileExtensionList').val(CER_FILE_EXTENSION);
	loadAuditFields(validator);
	loadFileName();
}
function loadFileName() {
	var value = $('#fileInventoryNumber').val();
	validator.reset();
	validator.setClass('patterns.config.web.forms.access.ipkicacertcfgbean');
	validator.setValue('CERT_INV_NUM', value);
	validator.setMethod('fetchFileName');
	validator.sendAndReceiveAsync(loadAdditionalData);
}

function loadAdditionalData() {
	if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		$('#fileName').val(validator.getValue('CERT_FILE_NAME'));
	}
}
function doFilePreview(){
	var primaryKey = getEntityCode() + '|' + $('#fileInventoryNumber').val() ;
	showWindow('PREVIEW_VIEW',getBasePath()+ 'Renderer/common/qcertificateinfo.jsp',CERTIFICATE_VIEW_TITLE,PK+'='+primaryKey,true,false,false,900,450);
	resetLoading();
}