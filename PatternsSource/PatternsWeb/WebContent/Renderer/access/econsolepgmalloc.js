var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'ECONSOLEPGMALLOC';
function init() {
	refreshQueryGrid();
	refreshTBAGrid();
	if (_redisplay) {
		innerGrid.clearAll();
		innerGrid.loadXMLString($('#xmlPgmGrid').val());
	}
}

function loadGrid() {
	loadGridQuery(CURRENT_PROGRAM_ID, 'ECONSOLEPGMALLOC_GRID', innerGrid);

}
function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'ECONSOLEPGMALLOC_MAIN', queryGrid);
}
function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}

function doHelp(id) {
	switch (id) {
	case 'consoleCode':
		help(CURRENT_PROGRAM_ID, 'CONSOLES', $('#consoleCode').val(), EMPTY_STRING, $('#consoleCode'));
		break;
	case 'effectiveDate':
		help(CURRENT_PROGRAM_ID, 'CONSOLE_EFF_DATE', $('#effectiveDate').val(), $('#consoleCode').val(), $('#effectiveDate'));
		break;
	case 'optionID':
		help(CURRENT_PROGRAM_ID, 'OPTIONS', $('#optionID').val(), EMPTY_STRING, $('#optionID'));
		break;
	}
}

function clearFields() {
	$('#consoleCode').val(EMPTY_STRING);
	$('#consoleCode_error').html(EMPTY_STRING);
	$('#consoleCode_desc').html(EMPTY_STRING);
	$('#effectiveDate').val(EMPTY_STRING);
	$('#effectiveDate_error').html(EMPTY_STRING);
	clearNonPKFields();
}

function clearNonPKFields() {
	$('#optionID').val(EMPTY_STRING);
	$('#optionID_error').html(EMPTY_STRING);
	$('#optionID_desc').html(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
	innerGrid.clearAll();
}
function add() {
	$('#consoleCode').focus();
}
function modify() {
	$('#consoleCode').focus();
}

function loadData() {
	$('#consoleCode').val(validator.getValue('CONSOLE_CODE'));
	$('#consoleCode_desc').html(validator.getValue('F1_DESCRIPTION'));
	$('#effectiveDate').val(validator.getValue('EFFT_DATE'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadGrid();
	resetLoading();
}
function view(source, primaryKey) {
	hideParent('access/qconsolepgmalloc', source, primaryKey);
	resetLoading();
}

function validate(id) {
	valMode = true;
	switch (id) {
	case 'consoleCode':
		consoleCode_val(valMode);
		break;
	case 'effectiveDate':
		effectiveDate_val(valMode);
		break;
	case 'optionID':
		optionID_val(valMode);
		break;
	case 'remarks':
		remarks_val(valMode);
		break;
	}
}

function doclearfields(id) {
	switch (id) {
	case 'consoleCode':
		if (isEmpty($('#consoleCode').val())) {
			$('#consoleCode_error').html(EMPTY_STRING);
			$('#consoleCode_desc').html(EMPTY_STRING);
			break;
		}

	case 'effectiveDate':
		if (isEmpty($('#effectiveDate').val())) {
			$('#effectiveDate_error').html(EMPTY_STRING);
			clearNonPKFields();
			break;
		}
	}
}
function backtrack(id) {
	switch (id) {
	case 'consoleCode':
		setFocusLast('consoleCode');
		break;
	case 'effectiveDate':
		setFocusLast('consoleCode');
		break;
	case 'remarks':
		setFocusLast('optionID');
		break;
	case 'optionID':
		setFocusLast('effectiveDate');
		break;
	}
}
function gridExit(id) {
	switch (id) {
	case 'optionID':
		$('#xmlPgmGrid').val(innerGrid.serialize());
		option_clearGridFields();
		setFocusLast('remarks');
		break;
	}
}

function gridDeleteRow(id) {
	switch (id) {
	case 'optionID':
		deleteGridRecord(innerGrid);
		break;
	}
}

function consoleCode_val(valMode) {
	var value = $('#consoleCode').val();
	clearError('consoleCode_error');
	if (isEmpty(value)) {
		setError('consoleCode_error', MANDATORY);
		setFocusLast('consoleCode');
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('CODE', $('#consoleCode').val());
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.access.econsolepgmallocbean');
		validator.setMethod('validateConsoleCode');
		validator.sendAndReceive();
		if (validator.getValue(RESULT) == DATA_AVAILABLE) {
			$('#consoleCode_desc').html(validator.getValue('DESCRIPTION'));
		} else {
			setError('consoleCode_error', validator.getValue(ERROR));
			setFocusLast('consoleCode');
			return false;
		}
	}
	setFocusLast('effectiveDate');
	return true;
}

function effectiveDate_val(valMode) {
	var value = $('#effectiveDate').val();
	clearError('effectiveDate_error');
	if (!isValidEffectiveDate(value, 'effectiveDate_error')) {
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('CONSOLE_CODE', $('#consoleCode').val());
		validator.setValue('EFFT_DATE', $('#effectiveDate').val());
		validator.setValue(ACTION, $('#action').val());
		validator.setClass('patterns.config.web.forms.access.econsolepgmallocbean');
		validator.setMethod('validateEffectiveDate');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('effectiveDate_error', validator.getValue(ERROR));
			setFocusLast('effectiveDate');
			return false;
		} else {
			if ($('#action').val() == MODIFY) {
				PK_VALUE = getEntityCode() + PK_SEPERATOR + $('#consoleCode').val() + PK_SEPERATOR + $('#effectiveDate').val();
				if (!loadPKValues(PK_VALUE, 'effectiveDate_error')) {
					return false;
				}
				setFocusLast('optionID');
				return true;
			}
		}
	}
	setFocusLast('optionID');
	return true;
}

function optionID_val(valMode) {
	var value = $('#optionID').val();
	clearError('optionID_error');
	if (isEmpty(value)) {
		setError('optionID_error', MANDATORY);
		setFocusLast('optionID');
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('PGM_ID', value);
		validator.setClass('patterns.config.web.forms.access.econsolepgmallocbean');
		validator.setMethod('validateOptionID');
		validator.sendAndReceive();
		if (validator.getValue(RESULT) == DATA_AVAILABLE) {
			$('#optionID_desc').html(validator.getValue('MPGM_DESCN'));

		} else {
			setError('optionID_error', validator.getValue(ERROR));
			setFocusLast('optionID');
			return false;
		}
	}
	if (valMode) {
		setFocusLastOnGridSubmit('innerGrid');
	}
	return true;
}

function option_gridDuplicateCheck(editMode, editRowId) {
	var currentValue = [ $('#optionID').val() ];
	return _grid_duplicate_check(innerGrid, currentValue, [ 1 ]);
}

function option_addGrid(editMode, editRowId) {
	if (optionID_val(false)) {
		if (option_gridDuplicateCheck(editMode, editRowId)) {
			var field_values = [ 'false', $('#optionID').val(), $('#optionID_desc').html() ];
		_grid_updateRow(innerGrid, field_values);
			option_clearGridFields();
			$('#optionID').focus();
			return true;
		} else {
			setError('optionID_error', HMS_DUPLICATE_DATA);
			$('#optionID').focus();
			return false;
		}
	} else {
		$('#optionID').focus();
		return false;
	}

}

function option_editGrid(rowId) {

	$('#optionID').val(innerGrid.cells(rowId, 1).getValue());
	$('#optionID_desc').html(innerGrid.cells(rowId, 2).getValue());
	$('#optionID').focus();
}

function option_cancelGrid(rowId) {
	option_clearGridFields();
}

function option_clearGridFields() {
	$('#optionID').val(EMPTY_STRING);
	$('#optionID_desc').html(EMPTY_STRING);
	$('#optionID_error').html(EMPTY_STRING);
	clearError('optionID_error');
}

function remarks_val(valMode) {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', HMS_INVALID_REMARKS);
			setFocusLast('remarks');
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', HMS_INVALID_REMARKS);
			setFocusLast('remarks');
			return false;
		}
	}
	if ($('#action').val() == MODIFY) {
		if (isEmpty(value)) {
			setError('remarks_error', HMS_MANDATORY);
			setFocusLast('remarks');
			return false;
		}

	}
	setFocusOnSubmit();
	return true;
}

function revalidate() {
	if (innerGrid.getRowsNum() > 0) {
		$('#xmlPgmGrid').val(innerGrid.serialize());
		$('#optionID_error').html(EMPTY_STRING);
	} else {
		setError('optionID_error', ADD_ATLEAST_ONE_ROW);
	}
}
