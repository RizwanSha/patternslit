<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<web:bundle baseName="patterns.config.web.forms.access.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/access/irestrictpfx.js" />
	</web:dependencies>
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="irestrictpfx.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/access/irestrictpfx" id="irestrictpfx" method="POST">
				<web:dividerBlock>
					<type:optionBox add="true" modify="true" view="true" template="true" />
				</web:dividerBlock>
				<web:viewContent id="po_view1" styleClass="hidden">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="form.effectivedate" var="common" mandatory="true" />
										</web:column>
										<web:column>
											<type:date property="effectiveDate" id="effectiveDate" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="irestrictpfx.pfxrestrictreq" var="program" />
										</web:column>
										<web:column>
											<type:checkbox property="pfxRestrictReq" id="pfxRestrictReq" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="form.remarks" var="common" mandatory="true" />
										</web:column>
										<web:column>
											<type:remarks property="remarks" id="remarks" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:viewTitle var="program" key="irestrictpfx.section1" />
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="irestrictpfx.prefixcode" var="program" />
										</web:column>
										<web:column>
											<type:prefixCode property="prefixCode" id="prefixCode" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:rowOdd>
										<web:column>
											<type:button id="update" key="form.add" var="common" onclick="_grid_addFunction(myaddfunction,innerGrid)" />
											<type:button id="edit" key="form.edit" var="common" onclick="_grid_modifyFunction(myeditfunction,innerGrid)" />
											<type:button id="remove" key="form.delete" var="common" onclick="_grid_deleteFunction(innerGrid,gridbox_rowid)" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:viewContent id="po_view4">
												<web:grid height="150" width="230" id="innerGrid" src="access/irestrictpfx_innerGrid.xml">
												</web:grid>
											</web:viewContent>
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:submitReset />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
						</web:sectionBlock>
					</web:dividerBlock>
				</web:viewContent>
				<web:element property="command" />
				<web:element property="action" />
				<web:element property="xmlPrefixDetails" />
				<web:element property="gridbox_rowid" />
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
	<web:queryGrid />
	<web:TBAGrid />
	<web:contextSearch />
</web:fragment>