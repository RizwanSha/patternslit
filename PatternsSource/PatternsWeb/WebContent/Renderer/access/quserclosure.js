var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'EUSERCLOSURE';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}
function clearFields() {
	$('#userId').val(EMPTY_STRING);
	$('#userId_desc').html(EMPTY_STRING);
	$('#joinDate').val(EMPTY_STRING);
	$('#addrLine1').val(EMPTY_STRING);
	$('#addrLine2').val(EMPTY_STRING);
	$('#addrLine3').val(EMPTY_STRING);
	$('#addrLine4').val(EMPTY_STRING);
	$('#addrLine5').val(EMPTY_STRING);
	$('#addrLocation').val(EMPTY_STRING);
	$('#addrCountry').val(EMPTY_STRING);
	$('#closureDate').val(EMPTY_STRING);
	$('#addrPinCode').val(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
}
function loadData() {
	$('#userId').val(validator.getValue('USER_ID'));
	$('#userId_desc').html(validator.getValue('USER_NAME'));
	$('#joinDate').val(validator.getValue('USERS_DATE_OF_JOINING'));
	$('#remarks').val(validator.getValue('REMARKS'));
	$('#closureDate').val(validator.getValue('DATE_OF_RELIVING'));
	loadAuditFields(validator);
	loadaddrDetails();
}
function loadaddrDetails() {
	var args = getEntityCode() + '|' + $('#userId').val();
	SOURCE_VALUE = _tableType;
	PK_VALUE = _primaryKey;
	loadRecordData(loadAdditionalData, 'MUSER', 'ADDRESS_DTL', args);
}
function loadAdditionalData() {
	$('#addrLine1').val(validator.getValue('AI_ADDR1'));
	$('#addrLine2').val(validator.getValue('AI_ADDR2'));
	$('#addrLine3').val(validator.getValue('AI_ADDR3'));
	$('#addrLine4').val(validator.getValue('AI_ADDR4'));
	$('#addrLine5').val(validator.getValue('AI_ADDR5'));
	$('#addrLocation').val(validator.getValue('AI_LOC'));
	$('#addrCountry').val(validator.getValue('AI_COUNTRY'));
	$('#addrPinCode').val(validator.getValue('AI_PINCODE'));
}