var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'IPWDNEGLIST'; 

function init() {
	refreshQueryGrid();
	refreshTBAGrid();
}
function refreshQueryGrid(){
	loadQueryGrid(CURRENT_PROGRAM_ID, 'IPWDNEGLIST_MAIN', queryGrid);
}
function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}
function loadPasswordGrid() {
	loadGridQuery(CURRENT_PROGRAM_ID,'IPWDNEGLIST_GRID',innerGrid);
}
function clearFields(){
	
	$('#effectiveDate').val(EMPTY_STRING);
	$('#effectiveDate_error').html(EMPTY_STRING);
	$('#password').val(EMPTY_STRING);
	$('#password_error').html(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
	innerGrid.clearAll();
}
function add() {
	
	$('#effectiveDate').focus();
}
function modify() {
	
	$('#effectiveDate').focus();
}
function loadData(){
	$('#effectiveDate').val(validator.getValue('EFFT_DATE'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadPasswordGrid();
	resetLoading();	
}
function view(source,primaryKey) {
	hideParent('access/qpwdneglist',source,primaryKey);
	resetLoading();
}
function validate(id){
	switch(id){
		case 'effectiveDate':
			effectiveDate_val();
		break;
		case 'password':
			password_val();
		break;
		case 'remarks':
			remarks_val();
			break;
	}
}
function effectiveDate_val() {
	var value = $('#effectiveDate').val();
	clearError('effectiveDate_error');
	if(!isValidEffectiveDate(value,'effectiveDate_error')){
		return false;
	}
	return true;
}
function password_val(){
	var value = $('#password').val();
	clearError('password_error');
	if (isEmpty(value)) {
		setError('password_error', MANDATORY);
		return false;
	}
	return true;
}
function myaddfunction(){
	clearError('password_error');
	var res=false;
	var rowid = innerGrid.uid();
	var password=$('#password').val();
	if(password_val())	{
		var serial = innerGrid.getRowsNum()+1;
     	var field_values=['false', password];
     	if(passWordDupeChk())	{
     		innerGrid.addRow(rowid,field_values,innerGrid.getRowsNum());
     	}else	{
 			setError('password_error', DUPLICATE_DATA);
 			return false;
 		}
		rowid = EMPTY_STRING;
		clearGridFields();
		return true;
	}
}

function passWordDupeChk()	{
	var row ;
	var gridvalue;
	var currvalue ;
	total_rows = innerGrid.getRowsNum();
	currvalue = $('#password').val();
	for (row=0;row<total_rows;row++)	{
	    gridvalue = innerGrid.cells2(row,1).getValue();
	    if ( gridvalue == currvalue){
	       return false;
	    }
	}
	return true;
}

function myeditfunction(){
	var rowID = getOneSelectedRecord(innerGrid);
	var total_rows = innerGrid.getRowsNum();
	var row,gridvalue ;
	for (row=0;row<total_rows;row++)	{
	    gridvalue = innerGrid.cells2(row,0).getValue();
	    if(gridvalue == true){
	    	$('#password').val( innerGrid.cells2(row,1).getValue());
	    	innerGrid.deleteRow(rowID);
	    }
	}			
}

function clearGridFields(){
   	$('#password').val(EMPTY_STRING);
   	clearError('password_error');
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (isEmpty(value)) {
		setError('remarks_error', MANDATORY);
		return false;
	}
	if (!isValidRemarks(value)) {
		setError('remarks_error', INVALID_REMARKS);
		return false;
	}
	return true;
}

function revalidate(){
	if(!effectiveDate_val()){
		errors++;
	}
	if (!remarks_val()) {
		errors++;
	}
	if(innerGrid.getRowsNum()>0){
		$('#xmlPwdNeglist').val(innerGrid.serialize());
	}else{
		setError('password_error', ADD_ATLEAST_ONE_ROW);
		errors++;
	}
}