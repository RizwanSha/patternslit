<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.access.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/access/erolepgmalloc.js" />
	</web:dependencies>
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="erolepgmalloc.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/access/erolepgmalloc" id="erolepgmalloc" method="POST">
				<web:dividerBlock>
					<type:optionBox add="true" modify="true" view="true" template="true" />
				</web:dividerBlock>
				<web:viewContent id="po_view1" styleClass="hidden">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column align="left">
											<web:legend key="form.rolecode" var="common" mandatory="true" />
										</web:column>
										<web:column>
											<type:code property="roleCode" id="roleCode" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column align="left">
											<web:legend key="form.modulecode" var="common" mandatory="true" />
										</web:column>
										<web:column>
											<type:code property="moduleCode" id="moduleCode" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column span="2">
											<type:button id="fetch" key="erolepgmalloc.fetch" var="program" onclick="loadProgramsGrid();" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column align="left">
											<web:legend key="form.effectivedate" var="common" mandatory="true"/>
										</web:column>
										<web:column>
											<type:date property="effectiveDate" id="effectiveDate" lookup="true"/>
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:viewTitle var="program" key="erolepgmalloc.section1" />
								<web:table>
									<web:rowOdd>
										<web:column>
											<web:grid height="440px" width="1010px" id="pgmGrid" src="access/erolepgmalloc_Grid.xml">
											</web:grid>
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="form.remarks" var="common"/>
										</web:column>
										<web:column>
											<type:remarks property="remarks" id="remarks" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:submitReset />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
						</web:sectionBlock>
					</web:dividerBlock>
				</web:viewContent>
				<web:element property="xmlPgmList" />
				<web:element property="command" />
				<web:element property="action" />
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
	<web:queryGrid />
	<web:TBAGrid />
	<web:contextSearch />
</web:fragment>