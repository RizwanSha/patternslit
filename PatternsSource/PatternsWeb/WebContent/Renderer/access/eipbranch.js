var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'EIPBRANCH';
function init() {
	refreshQueryGrid();
	refreshTBAGrid();
	if (_redisplay) {
		if($('#enabled').is(':checked') ){
			innerGrid.clearAll();
			if(!isEmpty($('#xmlIPList').val())){
				innerGrid.loadXMLString($('#xmlIPList').val());
			}
		}
		showFields();
	}
}

function loadGrid() {
	loadGridQuery(CURRENT_PROGRAM_ID,'EIPBRANCH_GRID', innerGrid);
}

function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'EIPBRANCH_MAIN', queryGrid);
}

function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}

function doHelp(id){
	switch(id){
		case 'brnCode':
			help('EIPBRANCH', 'BRANCHES', $('#brnCode').val(), EMPTY_STRING, $('#brnCode'));
			break;
	}
}

function clearFields(){
	
	$('#brnCode').val(EMPTY_STRING);
	$('#brnCode_error').html(EMPTY_STRING);
	$('#brnCode_desc').html(EMPTY_STRING);
	$('#effectiveDate').val(EMPTY_STRING);
	$('#effectiveDate_error').html(EMPTY_STRING);
	$('#enabled').prop('checked',false);
	$('#enabled_error').html(EMPTY_STRING);
	$('#IPAddress').val(EMPTY_STRING);
	$('#IPAddress_error').html(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
	innerGrid.clearAll();
}

function add() {
	
	$('#brnCode').focus();
	showFields();
}

function modify() {
	
}

function loadData(){
	$('#brnCode').val(validator.getValue('BRANCH_CODE'));
	$('#brnCode_desc').html(validator.getValue('MBRN_MBRN_NAME'));
	$('#effectiveDate').val(validator.getValue('EFFT_DATE'));
	$('#remarks').val(validator.getValue('REMARKS'));
	$('#enabled').prop('checked',decodeSTB(validator.getValue('ENABLED')));
	showFields();
	if($('#enabled').is(':checked'))	{
		loadGrid();
	}	else	{
		innerGrid.clearAll();
	}
	resetLoading();
}

function showFields() {
	if ($('#enabled').is(':checked')) {
		$('#IPAddress').removeAttr("disabled"); 
		$('#IPAddress').val(EMPTY_STRING);
		$('#IPAddress_error').html(EMPTY_STRING);	
	} else {		
		$('#IPAddress').attr("disabled", "disabled"); 
		$('#IPAddress').val(EMPTY_STRING);
		$('#IPAddress_error').html(EMPTY_STRING);			
	}
}

function checkclick(id) {
	switch (id) {
	case 'enabled':
		if ($('#enabled').is(':checked')) {
			$('#IPAddress').removeAttr("disabled"); 
			$('#IPAddress').val(EMPTY_STRING);
			$('#IPAddress_error').html(EMPTY_STRING);	
		} else {		
			$('#IPAddress').attr("disabled", "disabled"); 
			$('#IPAddress').val(EMPTY_STRING);
			$('#IPAddress_error').html(EMPTY_STRING);
			innerGrid.clearAll();
		}
		break;
	}
}

function view(source,primaryKey) {
	hideParent('access/qipbranch',source,primaryKey);
	resetLoading();
}

function validate(id) {
	switch (id) {
	case 'brnCode':
		brnCode_val();
	break;
	case 'effectiveDate':
		effectiveDate_val();
	break;
	case 'IPAddress':
		IPAddress_val();
		break;
	case 'remarks':
		remarks_val();
		break;
	}
}

function brnCode_val() {
	var value = $('#brnCode').val();
	clearError('brnCode_error');
	if (isEmpty(value)) {
		setError('brnCode_error', MANDATORY);
		return false;
	} 
	return true;
}

function effectiveDate_val() {
	var value = $('#effectiveDate').val();
	clearError('effectiveDate_error');
	if(!isValidEffectiveDate(value,'effectiveDate_error')){
		return false;
	}
	return true;
}

function IPAddress_val(){
	var value = $('#IPAddress').val();
	clearError('IPAddress_error');
	if (isEmpty(value)) {
		setError('IPAddress_error', MANDATORY);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('IP_ADDRESS',value);
	validator.setClass('patterns.config.web.forms.access.eipuserbean');
	validator.setMethod('validateIP');
	validator.sendAndReceive();
	if(validator.getValue(RESULT)==DATA_AVAILABLE ){
		return true;
	}	else{
		setError('IPAddress_error', validator.getValue(ERROR));
		return false;
	}
}

function myaddfunction(){
	if($('#enabled').is(':checked'))	{
	    if(IPAddress_val()){
		    $('#gridbox_rowid').val(innerGrid.uid());
		   	var IPAddress = $('#IPAddress').val();
		    if(ipAddressDuplicateCheck()){
		   		var sl = innerGrid.getRowsNum()+1;
				var field_values='0,'+IPAddress;
				innerGrid.addRow($('#gridbox_rowid').val(),field_values,innerGrid.getRowsNum());
				$('#gridbox_rowid').val(EMPTY_STRING);
				clearGridFields();
				$('#IPAddress').focus();
				return true;
		    }	else{
		   		setError('IPAddress_error', DUPLICATE_DATA);
		   		return false;
		   	}
		} else {
			return false;
		}
	}
}

function ipAddressDuplicateCheck()	{
	var row ;
	var gridvalue;
	var currvalue ;
	total_rows = innerGrid.getRowsNum();
	currvalue = $('#IPAddress').val();
	for (row=0;row<total_rows;row++)	{
	    gridvalue = innerGrid.cells2(row,1).getValue();
	    if ( gridvalue == currvalue){
	       return false;}
	}
	return true;
}	

function clearGridFields(){
	$('#IPAddress').val(EMPTY_STRING);
	clearError('IPAddress_error');
}

function myeditfunction(rowID){
	if($('#enabled').is(':checked'))	{	
		$('#IPAddress').val( innerGrid.cells(rowID,1).getValue());
		innerGrid.deleteRow(rowID);
		$('#IPAddress').focus();
	}
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (isEmpty(value)) {
		setError('remarks_error', MANDATORY);
		return false;
	}
	if (!isValidRemarks(value)) {
		setError('remarks_error', INVALID_REMARKS);
		return false;
	}
	return true;
}

function revalidate() {
	if (!brnCode_val()) {
		errors++;
	}
	if (!effectiveDate_val()) {
		errors++;
	}
	if (!remarks_val()) {
		errors++;
	}	
	if ($('#enabled').is(':checked')==true) {
		if(innerGrid.getRowsNum()>0){
			$('#xmlIPList').val(innerGrid.serialize());
			$('#IPAddress_error').html(EMPTY_STRING);
		}else{
			innerGrid.clearAll();
			$('#xmlIPList').val(innerGrid.serialize());
			 setError('IPAddress_error', ADD_ATLEAST_ONE_ROW);
			 errors++;
		}
	}
}