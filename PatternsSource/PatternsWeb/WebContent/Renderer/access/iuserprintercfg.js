var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'IUSERPRINTERCFG';
var printTypeVal = new Array();
function init() {
	valMode = true;
	refreshTBAGrid();
	refreshQueryGrid();
	if (_redisplay) {
		iuserprintercfg_innerGrid.clearAll();
		iuserprintercfgpgm_innerGrid.clearAll();
		if (!isEmpty($('#xmlUserPrinterConfigGrid').val())) {
			iuserprintercfg_innerGrid.loadXMLString($(
					'#xmlUserPrinterConfigGrid').val());
		}
		if (!isEmpty($('#xmlUserProgramPrinterConfigGrid').val())) {
			iuserprintercfgpgm_innerGrid.loadXMLString($(
					'#xmlUserProgramPrinterConfigGrid').val());
		}
	}
}

function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'IUSERPRINTERCFG_MAIN');
}

function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}

function add() {
	$('#userId').focus();
}

function modify() {
	$('#userId').focus();
}

function view(source, primaryKey) {
	hideParent('access/quserprintercfg', source, primaryKey);
	resetLoading();
}

function clearFields() {
	$('#userId').val(EMPTY_STRING);
	$('#userId_desc').html(EMPTY_STRING);
	$('#userId_error').html(EMPTY_STRING);
	$('#effectiveDate').val(EMPTY_STRING);
	$('#effectiveDate_error').html(EMPTY_STRING);
	clearNonPKFields();
}

function clearNonPKFields() {
	iuserprintercfg_clearGridFields();
	iuserprintercfg_innerGrid.clearAll();
	iuserprintercfgpgm_clearGridFields();
	iuserprintercfgpgm_innerGrid.clearAll();
}
function iuserprintercfg_clearGridFields() {
	$('#printTypeId').val(EMPTY_STRING);
	$('#printTypeId_desc').html(EMPTY_STRING);
	$('#printTypeId_error').html(EMPTY_STRING);
	$('#printerId').val(EMPTY_STRING);
	$('#printerId_error').html(EMPTY_STRING);
	$('#printerId_desc').html(EMPTY_STRING);
	setCheckbox('defaultPrinter', NO);
	$('#defaultPrinter_error').html(EMPTY_STRING);
}

function iuserprintercfgpgm_clearGridFields() {
	$('#pgmId').val(EMPTY_STRING);
	$('#pgmId_error').html(EMPTY_STRING);
	$('#pgmId_desc').html(EMPTY_STRING);
	$('#progPrinterId').val(EMPTY_STRING);
	$('#progPrinterId_error').html(EMPTY_STRING);
	$('#progPrinterId_desc').html(EMPTY_STRING);
}

function doclearfields(id) {
	switch (id) {
	case 'userId':
		if (isEmpty($('#userId').val())) {
			$('#userId_error').html(EMPTY_STRING);
			$('#userId_desc').html(EMPTY_STRING);
			break;
		}
	case 'effectiveDate':
		if (isEmpty($('#effectiveDate').val())) {
			$('#effectiveDate_error').html(EMPTY_STRING);
			$('#effectiveDate_desc').html(EMPTY_STRING);
			clearNonPKFields();
			break;
		}
	}
}
function loadData() {
	$('#userId').val(validator.getValue('USER_ID'));
	$('#userId_desc').html(validator.getValue('F1_USER_NAME'));
	$('#effectiveDate').val(validator.getValue('EFFT_DATE'));
	loadPrntCfgGrid();
	loadPgmPrntCfgGrid();
}
function loadPrntCfgGrid() {
	loadGridQuery(CURRENT_PROGRAM_ID, 'IUSERPRINTCFG_GRID',
			iuserprintercfg_innerGrid);
}
function loadPgmPrntCfgGrid() {
	loadGridQuery(CURRENT_PROGRAM_ID, 'IUSERPRINTCFGPGM_GRID',
			iuserprintercfgpgm_innerGrid);
}

function doHelp(id) {
	switch (id) {
	case 'userId':
		help('COMMON', 'HLP_USERS_M', $('#userId').val(), EMPTY_STRING,
				$('#userId'));
		break;
	case 'effectiveDate':
		help('IUSERPRINTERCFG', 'HLP_EFFT_DATE', $('#effectiveDate').val(), $(
				'#userId').val(), $('#effectiveDate'));
		break;
	case 'printTypeId':
		help('COMMON', 'HLP_PRNTYPE_M', $('#printTypeId').val(), EMPTY_STRING,
				$('#printTypeId'));
		break;
	case 'printerId':
		if (!(isEmpty($('#userId').val()))
				&& !(isEmpty($('#printTypeId').val()))) {
			help('IUSERPRINTERCFG', 'HLP_PRINTER_M', $('#printerId').val(), $(
					'#printTypeId').val()
					+ '|' + $('#userId').val() + '|' + $('#userId').val(),
					$('#printerId'));
		}
		break;
	case 'pgmId':
		help('COMMON', 'OPTIONS', $('#pgmId').val(), EMPTY_STRING, $('#pgmId'));
		break;
	case 'progPrinterId':
		if (!(isEmpty($('#userId').val())) && !(isEmpty($('#pgmId').val()))) {
			help('IUSERPRINTERCFG', 'HLP_PRINTER_P', $('#progPrinterId').val(),
					$('#pgmId').val() + '|' + $('#userId').val() + '|'
							+ $('#userId').val(), $('#progPrinterId'));
		}
		break;
	}
}

function validate(id) {
	valMode = true;
	switch (id) {
	case 'userId':
		userId_val(valMode);
		break;
	case 'effectiveDate':
		effectiveDate_val(valMode);
		break;
	case 'printTypeId':
		printTypeId_val(valMode);
		break;
	case 'printerId':
		printerId_val(valMode);
		break;
	case 'defaultPrinter':
		defaultPrinter_val(valMode);
		break;
	case 'pgmId':
		pgmId_val(valMode);
		break;
	case 'progPrinterId':
		progPrinterId_val(valMode);
		break;
	}
}

function backtrack(id) {
	switch (id) {
	case 'userId':
		setFocusLast('userId');
		break;
	case 'effectiveDate':
		setFocusLast('userId');
		break;
	case 'printTypeId':
		setFocusLast('effectiveDate');
		break;
	case 'printerId':
		setFocusLast('printTypeId');
		break;
	case 'defaultPrinter':
		setFocusLast('printerId');
		break;
	case 'pgmId':
		setFocusLast('defaultPrinter');
		break;
	case 'progPrinterId':
		setFocusLast('pgmId');
		break;
	}
}

function gridExit(id) {
	switch (id) {
	case 'printTypeId':
		$('#xmlUserPrinterConfigGrid').val(
				iuserprintercfg_innerGrid.serialize());
		if (!iuserprintercfgOneDefaultPrinterCheck()) {
			setFocusLast('printTypeId');
		} else {
			clearError('printTypeId_error');
			iuserprintercfg_clearGridFields();
			setFocusLast('pgmId');
		}
		break;
	case 'pgmId':
		$('#xmlUserProgramPrinterConfigGrid').val(
				iuserprintercfgpgm_innerGrid.serialize());
		iuserprintercfgpgm_clearGridFields();
		setFocusOnSubmit();
		break;
	}
}

function gridDeleteRow(id) {
	switch (id) {
	case 'printTypeId':
		deleteGridRecord(iuserprintercfg_innerGrid);
		break;
	case 'pgmId':
		deleteGridRecord(iuserprintercfgpgm_innerGrid);
		break;
	}
}

function userId_val(valMode) {
	var value = $('#userId').val();
	clearError('userId_error');
	if (isEmpty(value)) {
		setError('userId_error', HMS_MANDATORY);
		return false;
	} else if (!isValidCode(value)) {
		setError('userId_error', HMS_INVALID_FORMAT);
		return false;
	}
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('USER_ID', $('#userId').val());
	validator.setValue(ACTION, $('#action').val());
	validator.setClass('patterns.config.web.forms.access.iuserprintercfgbean');
	validator.setMethod('validateUserId');
	validator.sendAndReceive();
	if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		$('#userId_desc').html(validator.getValue('USER_NAME'));
	} else {
		setError('userId_error', validator.getValue(ERROR));
		return false;
	}
	setFocusLast('effectiveDate');
	return true;
}
function effectiveDate_val(valMode) {
	var value = $('#effectiveDate').val();
	clearError('effectiveDate_error');
	if (isEmpty(value)) {
		setError('effectiveDate_error', HMS_MANDATORY);
		return false;
	}
	if (!isValidEffectiveDate(value, 'effectiveDate_error')) {
		return false;
	}
	if (!isEmpty(value)) {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('USER_ID', $('#userId').val());
		validator.setValue('EFFT_DATE', $('#effectiveDate').val());
		validator.setValue(ACTION, $('#action').val());
		validator
				.setClass('patterns.config.web.forms.access.iuserprintercfgbean');
		validator.setMethod('validateEffectiveDate');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('effectiveDate_error', validator.getValue(ERROR));
			setFocusLast('effectiveDate');
			return false;
		} else if ($('#action').val() == MODIFY) {
			PK_VALUE = getEntityCode() + PK_SEPERATOR + $('#userId').val()
					+ PK_SEPERATOR + $('#effectiveDate').val();
			if (!loadPKValues(PK_VALUE, 'effectiveDate_error')) {
				return false;
			}
		}
	}
	setFocusLast('printTypeId');
	return true;
}
function printTypeId_val(valMode) {
	var value = $('#printTypeId').val();
	clearError('printTypeId_error');
	if (isEmpty(value)) {
		setError('printTypeId_error', HMS_MANDATORY);
		return false;
	} else if (!isValidCode(value)) {
		setError('printTypeId_error', HMS_INVALID_FORMAT);
		return false;
	}
	if (!isEmpty(value)) {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('PRNTYPE_ID', value);
		validator.setValue(ACTION, USAGE);
		validator
				.setClass('patterns.config.web.forms.access.iuserprintercfgbean');
		validator.setMethod('validatePrintTypeId');
		validator.sendAndReceive();
		if (validator.getValue(RESULT) == DATA_AVAILABLE) {
			$('#printTypeId_desc').html(validator.getValue('DESCRIPTION'));

		} else {
			setError('printTypeId_error', validator.getValue(ERROR));
			return false;
		}
	}
	setFocusLast('printerId');
	return true;
}

function printerId_val(valMode) {
	var value = $('#printerId').val();
	clearError('printerId_error');
	if (isEmpty(value)) {
		setError('printerId_error', HMS_MANDATORY);
		return false;
	} else if (!isValidCode(value)) {
		setError('printerId_error', HMS_INVALID_FORMAT);
		return false;
	}
	if (!isEmpty(value)) {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('PRINTER_ID', value);
		validator.setValue('PRNTYPE_ID', $('#printTypeId').val());
		validator.setValue('USER_ID', $('#userId').val());
		validator.setValue(ACTION, USAGE);
		validator
				.setClass('patterns.config.web.forms.access.iuserprintercfgbean');
		validator.setMethod('validatePrinterId');
		validator.sendAndReceive();
		if (validator.getValue(RESULT) == DATA_AVAILABLE) {
			$('#printerId_desc').html(validator.getValue('PRINTER_DESC'));

		} else {
			setError('printerId_error', validator.getValue(ERROR));
			return false;
		}
	}
	setFocusLast('defaultPrinter');
	return true;
}

function defaultPrinter_val(valMode) {
	if (valMode) {
		setFocusLastOnGridSubmit('iuserprintercfg_innerGrid');
	}
	return true;
}

function pgmId_val(valMode) {
	var value = $('#pgmId').val();
	clearError('pgmId_error');
	if (isEmpty(value)) {
		setError('pgmId_error', HMS_MANDATORY);
		return false;
	} else if (!isValidCode(value)) {
		setError('pgmId_error', HMS_INVALID_FORMAT);
		return false;
	}
	if (!isEmpty(value)) {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('MPGM_ID', value);
		validator.setValue(ACTION, USAGE);
		validator
				.setClass('patterns.config.web.forms.access.iuserprintercfgbean');
		validator.setMethod('validatePgmId');
		validator.sendAndReceive();
		if (validator.getValue(RESULT) == DATA_AVAILABLE) {
			$('#pgmId_desc').html(validator.getValue('MPGM_DESCN'));
		} else {
			setError('pgmId_error', validator.getValue(ERROR));
			return false;
		}
	}
	setFocusLast('progPrinterId');
	return true;
}

function progPrinterId_val(valMode) {
	var value = $('#progPrinterId').val();
	clearError('progPrinterId_error');
	if (isEmpty(value)) {
		setError('progPrinterId_error', HMS_MANDATORY);
		return false;
	} else if (!isValidCode(value)) {
		setError('progPrinterId_error', HMS_INVALID_FORMAT);
		return false;
	}
	if (!isEmpty(value)) {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('PRINTER_ID', value);
		validator.setValue('PGM_ID', $('#pgmId').val());
		validator.setValue('USER_ID', $('#userId').val());
		validator.setValue(ACTION, USAGE);
		validator
				.setClass('patterns.config.web.forms.access.iuserprintercfgbean');
		validator.setMethod('validatePrinterIdpgm');
		validator.sendAndReceive();
		if (validator.getValue(RESULT) == DATA_AVAILABLE) {
			$('#progPrinterId_desc').html(validator.getValue('PRINTER_DESC'));

		} else {
			setError('progPrinterId_error', validator.getValue(ERROR));
			return false;
		}
	}
	if (valMode) {
		setFocusLastOnGridSubmit('iuserprintercfgpgm_innerGrid');
	}
	return true;
}

function iuserprintercfg_addGrid(editMode, editRowId) {
	if (printTypeId_val(false) && printerId_val(false)) {
		if (iuserprintercfg_gridDuplicateCheck(editMode, editRowId)) {
			if ($('#defaultPrinter').is(':checked')) {
				if (!iuserprintercfg_gridDuplicateCheckDefaultPrinter(editMode, editRowId)) {
					setError('printTypeId_error',PRNT_EACH_PRINTTYPE_ONE_DEFAULT_PRINTER );
					return false;
				}
			}
			var field_values = [ 'false', $('#printTypeId').val(),
					$('#printTypeId_desc').html(), $('#printerId').val(),
					$('#printerId_desc').html(),
					$('#defaultPrinter_desc').html() ];
			_grid_updateRow(iuserprintercfg_innerGrid, field_values);
			iuserprintercfg_clearGridFields();
			$('#printTypeId').focus();
			return true;
		} else {
			setError('printTypeId_error', HMS_DUPLICATE_DATA);
			$('#printTypeId').focus();
			return false;
		}
	} else {
		$('#printTypeId').focus();
		return false;
	}
}

function iuserprintercfg_gridDuplicateCheck(editMode, editRowId) {
	var currentValue = [ $('#printTypeId').val(), $('#printerId').val() ];
	return _grid_duplicate_check(iuserprintercfg_innerGrid, currentValue, [ 1,
			3 ]);
}
function iuserprintercfg_gridDuplicateCheckDefaultPrinter(editMode, editRowId) {
	var currentValue = [ $('#printTypeId').val(), $('#defaultPrinter_desc').html()  ];
	return _grid_duplicate_check(iuserprintercfg_innerGrid, currentValue, [ 1,5 ]);
}


function iuserprintercfg_editGrid(rowId) {
	$('#printTypeId').val(iuserprintercfg_innerGrid.cells(rowId, 1).getValue());
	$('#printTypeId_desc').html(
			iuserprintercfg_innerGrid.cells(rowId, 2).getValue());
	$('#printerId').val(iuserprintercfg_innerGrid.cells(rowId, 3).getValue());
	$('#printerId_desc').html(
			iuserprintercfg_innerGrid.cells(rowId, 4).getValue());
	if (iuserprintercfg_innerGrid.cells(rowId, 5).getValue() == "Yes") {
		setCheckbox('defaultPrinter', '1');
	} else {
		setCheckbox('defaultPrinter', '0');
	}
	$('#printTypeId').focus();
}

function iuserprintercfg_cancelGrid(rowId) {
	iuserprintercfg_clearGridFields();
}

function iuserprintercfgpgm_addGrid(editMode, editRowId) {
	if (pgmId_val(false) && progPrinterId_val(false)) {
		if (iuserprintercfgpgm_gridDuplicateCheck(editMode, editRowId)) {
			var field_values = [ 'false', $('#pgmId').val(),
					$('#pgmId_desc').html(), $('#progPrinterId').val(),
					$('#progPrinterId_desc').html() ];
			_grid_updateRow(iuserprintercfgpgm_innerGrid, field_values);
			iuserprintercfgpgm_clearGridFields();
			$('#pgmId').focus();
			return true;
		} else {
			setError('pgmId_error', HMS_DUPLICATE_DATA);
			$('#pgmId').focus();
			return false;
		}
	} else {
		$('#pgmId').focus();
		return false;
	}
}

function iuserprintercfgpgm_gridDuplicateCheck(editMode, editRowId) {
	var currentValue = [ $('#pgmId').val() ];
	return _grid_duplicate_check(iuserprintercfgpgm_innerGrid, currentValue,
			[ 1 ]);
}

function iuserprintercfgpgm_editGrid(rowId) {
	$('#pgmId').val(iuserprintercfgpgm_innerGrid.cells(rowId, 1).getValue());
	$('#pgmId_desc').html(
			iuserprintercfgpgm_innerGrid.cells(rowId, 2).getValue());
	$('#progPrinterId').val(
			iuserprintercfgpgm_innerGrid.cells(rowId, 3).getValue());
	$('#progPrinterId_desc').html(
			iuserprintercfgpgm_innerGrid.cells(rowId, 4).getValue());
	$('#pgmId').focus();
}

function iuserprintercfgpgm_cancelGrid(rowId) {
	iuserprintercfgpgm_clearGridFields();
}

function iuserprintercfg_revaildateGrid() {
	if (iuserprintercfg_innerGrid.getRowsNum() > 0) {
		$('#xmlUserPrinterConfigGrid').val(
				iuserprintercfg_innerGrid.serialize());
		$('#printTypeId_error').html(EMPTY_STRING);
	} else {
		iuserprintercfg_innerGrid.clearAll();
		$('#xmlUserPrinterConfigGrid').val(
				iuserprintercfg_innerGrid.serialize());
		setError('printTypeId_error', HMS_ADD_ATLEAST_ONE_ROW);
		errors++;
	}
	if (iuserprintercfgpgm_innerGrid.getRowsNum() > 0) {
		$('#xmlUserProgramPrinterConfigGrid').val(
				iuserprintercfgpgm_innerGrid.serialize());
		$('#pgmId_error').html(EMPTY_STRING);
	} else {
		iuserprintercfgpgm_innerGrid.clearAll();
		$('#xmlUserProgramPrinterConfigGrid').val(
				iuserprintercfgpgm_innerGrid.serialize());
		setError('pgmId_error', HMS_ADD_ATLEAST_ONE_ROW);
		errors++;
	}
}

function iuserprintercfgOneDefaultPrinterCheck() {
	var count = 0;
	printTypeVal = distinctarray();
	var rowCount = iuserprintercfg_innerGrid.getRowsNum();
	for (var i = 0; i < printTypeVal.length; i++) {
		for (var row = 0; row < rowCount; row++) {
			if (printTypeVal[i] == iuserprintercfg_innerGrid.cells2(row, 1)
					.getValue()) {
				if (iuserprintercfg_innerGrid.cells2(row, 5).getValue() == "Yes") {
					count++;
					if (count > 1) {
						setError('printTypeId_error',
								PRNT_EACH_PRINTTYPE_ONE_DEFAULT_PRINTER);
						return false;
					}
				}
			}
		}
		if (count == 0) {
			setError('printTypeId_error', PRNT_ATLEAST_ONE_DEFAULT_PRINTER);
			return false;
		}
		count = 0;
	}
	return true;
}

function distinctarray() {
	var output = new Array();
	var found = false;
	var rowCount = iuserprintercfg_innerGrid.getRowsNum();
	for (var row = 0; row < rowCount; row++) {
		for (var j = 0; j < output.length; j++) {
			if (output[j] == iuserprintercfg_innerGrid.cells2(row, 1)
					.getValue()) {
				found = true;
			}
		}
		if (!found) {
			output[output.length] = iuserprintercfg_innerGrid.cells2(row, 1)
					.getValue();
		}
		found = false;
	}
	return output;
}

function revalidate() {
	iuserprintercfg_revaildateGrid();
}
