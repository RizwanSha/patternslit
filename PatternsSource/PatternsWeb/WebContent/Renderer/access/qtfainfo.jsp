<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.access.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/access/qtfainfo.js" />
	</web:dependencies>
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="qtfainfo.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/access/qtfainfo" id="qtfainfo" method="POST">
				<web:viewContent id="po_view1">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="form.userid" var="common" />
										</web:column>
										<web:column>
											<type:userDisplay property="userID" id="userID" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="qtfainfo.username" var="program" />
										</web:column>
										<web:column>
											<type:nameDisplay property="userName" id="userName" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column span="2">
											<type:button id="cmdCertificateInfo" key="qtfainfo.viewcertificateinfo" var="program" onclick="viewCertificateInfo();" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="qtfainfo.auctionip" var="program" />
										</web:column>
										<web:column>
											<type:ipaddressDisplay property="auctionIP" id="auctionIP" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="qtfainfo.auctionvalue" var="program" />
										</web:column>
										<web:column>
											<type:remarksDisplay property="actualValue" id="actualValue" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="qtfainfo.encodedvalue" var="program" />
										</web:column>
										<web:column>
											<type:remarksDisplay property="encodedValue" id="encodedValue" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:element property="hidPrimaryKey" />
							<web:element property="hidInventoryNumber" />
						</web:sectionBlock>
					</web:dividerBlock>
				</web:viewContent>
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
	<web:contextSearch />
</web:fragment>