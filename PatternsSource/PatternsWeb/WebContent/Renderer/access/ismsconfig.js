var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'ISMSCONFIG';
var cbdval = getCBD();

function init() {
	refreshQueryGrid();
	refreshTBAGrid();
	if (_redisplay) {
		enableFields();
		if ($('#dispatchType').val() == 'P') {
			$('#retryTimeout').prop('readOnly', false);
		} else {
			$('#retryTimeout').val(EMPTY_STRING);
			$('#retryTimeout').prop('readOnly', true);
			clearError('retryTimeout_error');
		}
	}
}

function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'ISMSCONFIG_MAIN', queryGrid);
}

function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}

function doclearfields(id) {
	switch (id) {
	case 'smsCode':
		if (isEmpty($('#smsCode').val())) {
			$('#smsCode_error').html(EMPTY_STRING);
			$('#smsCode_desc').html(EMPTY_STRING);
			$('#effectiveDate').val(EMPTY_STRING);
			$('#effectiveDate_error').html(EMPTY_STRING);
			clearNonPKFields();
		}
		break;
	case 'effectiveDate':
		if ($('#action').val() == MODIFY) {
			if (isEmpty($('#effectiveDate').val())) {
				$('#effectiveDate_error').html(EMPTY_STRING);
				clearNonPKFields();
			}
		}
		break;
	}
}

function clearFields() {
	$('#smsCode').val(EMPTY_STRING);
	$('#smsCode_error').html(EMPTY_STRING);
	$('#smsCode_desc').html(EMPTY_STRING);
	$('#effectiveDate').val(EMPTY_STRING);
	$('#effectiveDate_error').html(EMPTY_STRING);
	clearNonPKFields();
}

function clearNonPKFields() {
	setCheckbox('httpAllowed', NO);
	$('#httpAllowed').prop('disabled', false);
	$('#httpAllowed_error').html(EMPTY_STRING);
	setCheckbox('sslRequired', NO);
	$('#sslRequired').prop('disabled', false);
	$('#sslRequired_error').html(EMPTY_STRING);
	$('#serverUrl').val(EMPTY_STRING);
	$('#serverUrl_error').html(EMPTY_STRING);
	$('#serverUrl').prop('readOnly', false);
	setCheckbox('proxyRequired', NO);
	$('#proxyRequired').prop('disabled', false);
	$('#proxyRequired_error').html(EMPTY_STRING);
	$('#proxyServer').val(EMPTY_STRING);
	$('#proxyServer_error').html(EMPTY_STRING);
	$('#proxyServer').prop('readOnly', false);
	$('#proxyServerPort').val(EMPTY_STRING);
	$('#proxyServerPort_error').html(EMPTY_STRING);
	$('#proxyServerPort').prop('readOnly', false);
	setCheckbox('proxyServerAuthentication', NO);
	$('#proxyServerAuthentication').prop('disabled', false);
	$('#proxyServerAuthentication_error').html(EMPTY_STRING);
	$('#proxyAccountName').val(EMPTY_STRING);
	$('#proxyAccountName_error').html(EMPTY_STRING);
	$('#proxyAccountName').prop('readOnly', false);
	$('#proxyAccountPassword').val(EMPTY_STRING);
	$('#proxyAccountPassword_error').html(EMPTY_STRING);
	$('#proxyAccountPassword').prop('readOnly', false);
	setCheckbox('authenticationRequired', NO);
	$('#authenticationRequired').prop('disabled', false);
	$('#authenticationRequired_error').html(EMPTY_STRING);
	$('#userNameKey').val(EMPTY_STRING);
	$('#userNameKey_error').html(EMPTY_STRING);
	$('#userNameKey').prop('readOnly', false);
	$('#accountName').val(EMPTY_STRING);
	$('#accountName_error').html(EMPTY_STRING);
	$('#accountName').prop('readOnly', false);
	$('#accountPasswordKey').val(EMPTY_STRING);
	$('#accountPasswordKey_error').html(EMPTY_STRING);
	$('#accountPasswordKey').prop('readOnly', false);
	$('#accountPassword').val(EMPTY_STRING);
	$('#accountPassword_error').html(EMPTY_STRING);
	$('#accountPassword_desc').html(EMPTY_STRING);
	$('#accountPassword').prop('readOnly', false);
	$('#mobileNumberKey').val(EMPTY_STRING);
	$('#mobileNumberKey_error').html(EMPTY_STRING);
	$('#mobileNumberKey').prop('readOnly', false);
	$('#messageKey').val(EMPTY_STRING);
	$('#messageKey_error').html(EMPTY_STRING);
	$('#messageKey').prop('readOnly', false);
	setCheckbox('smppAllowed', NO);
	$('#smppAllowed').prop('disabled', false);
	$('#smppAllowed_error').html(EMPTY_STRING);
	$('#senderNumber').val(EMPTY_STRING);
	$('#senderNumber_error').html(EMPTY_STRING);
	$('#senderNumber').prop('readOnly', false);
	$('#systemType').val(EMPTY_STRING);
	$('#systemType_error').html(EMPTY_STRING);
	$('#systemType').prop('readOnly', false);
	$('#addressRange').val(EMPTY_STRING);
	$('#addressRange_error').html(EMPTY_STRING);
	$('#addressRange').prop('readOnly', false);
	setCheckbox('transceiverEnabled', NO);
	$('#transceiverEnabled').prop('disabled', false);
	$('#transceiverEnabled_error').html(EMPTY_STRING);
	$('#mainServer').val(EMPTY_STRING);
	$('#mainServer_error').html(EMPTY_STRING);
	$('#mainServer').prop('readOnly', false);
	$('#mainServerPort').val(EMPTY_STRING);
	$('#mainServerPort_error').html(EMPTY_STRING);
	$('#mainServerPort').prop('readOnly', false);
	$('#failOverServer').val(EMPTY_STRING);
	$('#failOverServer_error').html(EMPTY_STRING);
	$('#failOverServer').prop('readOnly', false);
	$('#failOverServerPort').val(EMPTY_STRING);
	$('#failOverServerPort_error').html(EMPTY_STRING);
	$('#failOverServerPort_desc').html(EMPTY_STRING);
	$('#failOverServerPort').prop('readOnly', false);
	$('#dispatchType').val(EMPTY_STRING);
	$('#dispatchType_error').html(EMPTY_STRING);
	$('#dispatchType').prop('disabled', false);
	$('#retryTimeout').val(EMPTY_STRING);
	$('#retryTimeout_error').html(EMPTY_STRING);
	$('#retryTimeout').prop('readOnly', false);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
}
function add() {
	$('#smsCode').focus();
	$('#effectiveDate_look').prop('disabled', true);
}
function modify() {
	$('#smsCode').focus();
	$('#effectiveDate_look').prop('disabled', false);
}

function doHelp(id) {
	switch (id) {
	case 'smsCode':
		help('COMMON', 'SMSINTFLIST', $('#smsCode').val(), EMPTY_STRING, $('#smsCode'));
		break;
	case 'effectiveDate':
		if (isEmpty($('#smsCode').val())) {
			setError('smsCode_error', HMS_MANDATORY);
		}else{
			help('CURRENT_PROGRAM_ID', 'HLP_EFFT_DATE', $('#effectiveDate').val(), $('#smsCode').val(), $('#effectiveDate'));
		}
		break;
	}
}

function enableFields() {
	if ($('#sendAllowed').val() == COLUMN_ENABLE) {
		$('#httpAllowed').prop('disabled', false);
		if ($('#httpAllowed').is(':checked')) {
			$('#sslRequired').prop('disabled', false);
			$('#serverUrl').prop('readOnly', false);

			$('#proxyRequired').prop('disabled', false);

			if ($('#proxyRequired').is(':checked')) {
				$('#proxyServer').prop('readOnly', false);
				$('#proxyServerPort').prop('readOnly', false);
			} else {
				$('#proxyServer').val(EMPTY_STRING);
				$('#proxyServer').prop('readOnly', true);
				clearError('proxyServer_error');
				$('#proxyServerPort').val(EMPTY_STRING);
				$('#proxyServerPort').prop('readOnly', true);
				clearError('proxyServerPort_error');
			}
			$('#proxyServerAuthentication').prop('disabled', false);

			if ($('#proxyServerAuthentication').is(':checked')) {
				$('#proxyAccountName').prop('readOnly', false);
				$('#proxyAccountPassword').prop('readOnly', false);
			} else {
				$('#proxyAccountName').val(EMPTY_STRING);
				$('#proxyAccountName').prop('readOnly', true);
				$('#proxyAccountPassword').val(EMPTY_STRING);
				$('#proxyAccountPassword').prop('readOnly', true);
			}

			$('#authenticationRequired').prop('disabled', false);

			if ($('#authenticationRequired').is(':checked')) {
				$('#userNameKey').prop('readOnly', false);
				$('#accountName').prop('readOnly', false);
				$('#accountPasswordKey').prop('readOnly', false);
				$('#accountPassword').prop('readOnly', false);
				$('#mobileNumberKey').prop('readOnly', false);
				$('#messageKey').prop('readOnly', false);
			} else {
				$('#userNameKey').val(EMPTY_STRING);
				$('#userNameKey').prop('readOnly', true);
				$('#accountName').val(EMPTY_STRING);
				$('#accountPasswordKey').val(EMPTY_STRING);
				$('#accountPasswordKey').prop('readOnly', true);
				$('#accountName').prop('readOnly', true);
				$('#accountPassword').val(EMPTY_STRING);
				$('#accountPassword').prop('readOnly', true);
				$('#mobileNumberKey').val(EMPTY_STRING);
				$('#mobileNumberKey').prop('readOnly', true);
				$('#messageKey').val(EMPTY_STRING);
				$('#messageKey').prop('readOnly', true);
				clearError('userNameKey_error');
				clearError('accountName_error');
				clearError('accountPasswordKey_error');
				clearError('accountPassword_error');
				clearError('mobileNumberKey_error');
				clearError('messageKey_error');
			}
		} else {
			setCheckbox('sslRequired', NO);
			$('#sslRequired').prop('disabled', true);

			$('#serverUrl').val(EMPTY_STRING);
			$('#serverUrl').prop('readOnly', true);
			clearError('serverUrl_error');

			setCheckbox('proxyRequired', NO);
			$('#proxyRequired').prop('disabled', true);

			$('#proxyServer').val(EMPTY_STRING);
			$('#proxyServer').prop('readOnly', true);
			clearError('proxyServer_error');
			$('#proxyServerPort').val(EMPTY_STRING);
			$('#proxyServerPort').prop('readOnly', true);
			clearError('proxyServerPort_error');

			setCheckbox('proxyServerAuthentication', NO);
			$('#proxyServerAuthentication').prop('disabled', true);

			$('#proxyAccountName').val(EMPTY_STRING);
			$('#proxyAccountName').prop('readOnly', true);
			$('#proxyAccountPassword').val(EMPTY_STRING);
			$('#proxyAccountPassword').prop('readOnly', true);
			clearError('proxyAccountName_error');
			clearError('proxyAccountPassword_error');

			setCheckbox('authenticationRequired', NO);
			$('#authenticationRequired').prop('disabled', true);

			$('#userNameKey').val(EMPTY_STRING);
			$('#userNameKey').prop('readOnly', true);
			$('#accountName').val(EMPTY_STRING);
			$('#accountPasswordKey').val(EMPTY_STRING);
			$('#accountPasswordKey').prop('readOnly', true);
			$('#accountName').prop('readOnly', true);
			$('#accountPassword').val(EMPTY_STRING);
			$('#accountPassword').prop('readOnly', true);
			$('#mobileNumberKey').val(EMPTY_STRING);
			$('#mobileNumberKey').prop('readOnly', true);
			$('#messageKey').val(EMPTY_STRING);
			$('#messageKey').prop('readOnly', true);
			clearError('userNameKey_error');
			clearError('accountName_error');
			clearError('accountPasswordKey_error');
			clearError('accountPassword_error');
			clearError('mobileNumberKey_error');
			clearError('messageKey_error');
		}
		$('#smppAllowed').prop('disabled', false);
		if ($('#smppAllowed').is(':checked')) {
			$('#senderNumber').prop('readOnly', false);
			$('#systemType').prop('readOnly', false);
			$('#addressRange').prop('readOnly', false);
			$('#transceiverEnabled').prop('disabled', false);
			$('#mainServer').prop('readOnly', false);
			$('#mainServerPort').prop('readOnly', false);
			$('#failOverServer').prop('readOnly', false);
			$('#failOverServerPort').prop('readOnly', false);
			$('#dispatchType').prop('disabled', false);

			if ($('#dispatchType').val() == 'P') {
				$('#retryTimeout').prop('readOnly', false);
			} else {
				$('#retryTimeout').val(EMPTY_STRING);
				$('#retryTimeout').prop('readOnly', true);
				clearError('retryTimeout_error');
			}
		} else {
			$('#senderNumber').val(EMPTY_STRING);
			$('#senderNumber').prop('readOnly', true);

			$('#systemType').val(EMPTY_STRING);
			$('#systemType').prop('readOnly', true);

			$('#addressRange').val(EMPTY_STRING);
			$('#addressRange').prop('readOnly', true);

			setCheckbox('transceiverEnabled', NO);
			$('#transceiverEnabled_error').html(EMPTY_STRING);
			$('#transceiverEnabled').prop('disabled', true);

			$('#mainServer').val(EMPTY_STRING);
			$('#mainServer').prop('readOnly', true);

			$('#mainServerPort').val(EMPTY_STRING);
			$('#mainServerPort').prop('readOnly', true);

			$('#failOverServer').val(EMPTY_STRING);
			$('#failOverServer').prop('readOnly', true);

			$('#failOverServerPort').val(EMPTY_STRING);
			$('#failOverServerPort').prop('readOnly', true);

			$('#dispatchType').val(EMPTY_STRING);
			$('#dispatchType').prop('disabled', true);

			$('#retryTimeout').val(EMPTY_STRING);
			$('#retryTimeout').prop('readOnly', true);
			clearError('senderNumber_error');
			clearError('systemType_error');
			clearError('addressRange_error');
			clearError('mainServer_error');
			clearError('mainServerPort_error');
			clearError('failOverServer_error');
			clearError('failOverServerPort_error');
			clearError('dispatchType_error');
			clearError('retryTimeout_error');
		}
	} else {
		setCheckbox('httpAllowed', NO);
		$('#httpAllowed_error').html(EMPTY_STRING);
		$('#httpAllowed').prop('disabled', true);

		setCheckbox('sslRequired', NO);
		$('#sslRequired').prop('disabled', true);

		$('#serverUrl').val(EMPTY_STRING);
		$('#serverUrl').prop('readOnly', true);
		clearError('serverUrl_error');

		setCheckbox('proxyRequired', NO);
		$('#proxyRequired').prop('disabled', true);

		$('#proxyServer').val(EMPTY_STRING);
		$('#proxyServer').prop('readOnly', true);
		clearError('proxyServer_error');
		$('#proxyServerPort').val(EMPTY_STRING);
		$('#proxyServerPort').prop('readOnly', true);
		clearError('proxyServerPort_error');

		setCheckbox('proxyServerAuthentication', NO);
		$('#proxyServerAuthentication').prop('disabled', true);

		$('#proxyAccountName').val(EMPTY_STRING);
		$('#proxyAccountName').prop('readOnly', true);
		$('#proxyAccountPassword').val(EMPTY_STRING);
		$('#proxyAccountPassword').prop('readOnly', true);
		clearError('proxyAccountName_error');
		clearError('proxyAccountPassword_error');

		setCheckbox('authenticationRequired', NO);
		$('#authenticationRequired').prop('disabled', true);

		$('#userNameKey').val(EMPTY_STRING);
		$('#userNameKey').prop('readOnly', true);
		$('#accountName').val(EMPTY_STRING);
		$('#accountPasswordKey').val(EMPTY_STRING);
		$('#accountPasswordKey').prop('readOnly', true);
		$('#accountName').prop('readOnly', true);
		$('#accountPassword').val(EMPTY_STRING);
		$('#accountPassword').prop('readOnly', true);
		$('#mobileNumberKey').val(EMPTY_STRING);
		$('#mobileNumberKey').prop('readOnly', true);
		$('#messageKey').val(EMPTY_STRING);
		$('#messageKey').prop('readOnly', true);
		clearError('userNameKey_error');
		clearError('accountName_error');
		clearError('accountPasswordKey_error');
		clearError('accountPassword_error');
		clearError('mobileNumberKey_error');
		clearError('messageKey_error');

		setCheckbox('smppAllowed', NO);
		$('#smppAllowed_error').html(EMPTY_STRING);
		$('#smppAllowed').prop('disabled', true);

		$('#senderNumber').val(EMPTY_STRING);
		$('#senderNumber').prop('readOnly', true);

		$('#systemType').val(EMPTY_STRING);
		$('#systemType').prop('readOnly', true);

		$('#addressRange').val(EMPTY_STRING);
		$('#addressRange').prop('readOnly', true);

		setCheckbox('transceiverEnabled', NO);
		$('#transceiverEnabled_error').html(EMPTY_STRING);
		$('#transceiverEnabled').prop('disabled', true);

		$('#mainServer').val(EMPTY_STRING);
		$('#mainServer').prop('readOnly', true);

		$('#mainServerPort').val(EMPTY_STRING);
		$('#mainServerPort').prop('readOnly', true);

		$('#failOverServer').val(EMPTY_STRING);
		$('#failOverServer').prop('readOnly', true);

		$('#failOverServerPort').val(EMPTY_STRING);
		$('#failOverServerPort').prop('readOnly', true);

		$('#dispatchType').val(EMPTY_STRING);
		$('#dispatchType').prop('disabled', true);

		$('#retryTimeout').val(EMPTY_STRING);
		$('#retryTimeout').prop('readOnly', true);
		clearError('senderNumber_error');
		clearError('systemType_error');
		clearError('addressRange_error');
		clearError('mainServer_error');
		clearError('mainServerPort_error');
		clearError('failOverServer_error');
		clearError('failOverServerPort_error');
		clearError('dispatchType_error');
		clearError('retryTimeout_error');
	}
}

function checkclick(id) {
	switch (id) {
	case 'httpAllowed':
		if ($('#httpAllowed').is(':checked')) {
			$('#sslRequired').prop('disabled', false);
			$('#serverUrl').prop('readOnly', false);
			$('#proxyRequired').prop('disabled', false);
			if ($('#proxyRequired').is(':checked')) {
				$('#proxyServer').prop('readOnly', false);
				$('#proxyServerPort').prop('readOnly', false);
			} else {
				$('#proxyServer').val(EMPTY_STRING);
				$('#proxyServer').prop('readOnly', true);
				clearError('proxyServer_error');
				$('#proxyServerPort').val(EMPTY_STRING);
				$('#proxyServerPort').prop('readOnly', true);
				clearError('proxyServerPort_error');
			}
			$('#proxyServerAuthentication').prop('disabled', false);
			if ($('#proxyServerAuthentication').is(':checked')) {
				$('#proxyAccountName').prop('readOnly', false);
				$('#proxyAccountPassword').prop('readOnly', false);
			} else {
				$('#proxyAccountName').val(EMPTY_STRING);
				$('#proxyAccountName').prop('readOnly', true);
				$('#proxyAccountPassword').val(EMPTY_STRING);
				$('#proxyAccountPassword').prop('readOnly', true);
				clearError('proxyAccountName_error');
				clearError('proxyAccountPassword_error');
			}
			$('#authenticationRequired').prop('disabled', false);
			if ($('#authenticationRequired').is(':checked')) {
				$('#userNameKey').prop('readOnly', false);
				$('#accountName').prop('readOnly', false);
				$('#accountPasswordKey').prop('readOnly', false);
				$('#accountPassword').prop('readOnly', false);
				$('#mobileNumberKey').prop('readOnly', false);
				$('#messageKey').prop('readOnly', false);
			} else {
				$('#userNameKey').val(EMPTY_STRING);
				$('#userNameKey').prop('readOnly', true);
				$('#accountName').val(EMPTY_STRING);
				$('#accountPasswordKey').val(EMPTY_STRING);
				$('#accountPasswordKey').prop('readOnly', true);
				$('#accountName').prop('readOnly', true);
				$('#accountPassword').val(EMPTY_STRING);
				$('#accountPassword').prop('readOnly', true);
				$('#mobileNumberKey').val(EMPTY_STRING);
				$('#mobileNumberKey').prop('readOnly', true);
				$('#messageKey').val(EMPTY_STRING);
				$('#messageKey').prop('readOnly', true);
				clearError('userNameKey_error');
				clearError('accountName_error');
				clearError('accountPasswordKey_error');
				clearError('accountPassword_error');
				clearError('mobileNumberKey_error');
				clearError('messageKey_error');
			}
		} else {
			setCheckbox('sslRequired', NO);
			$('#sslRequired').prop('disabled', true);

			$('#serverUrl').val(EMPTY_STRING);
			$('#serverUrl').prop('readOnly', true);
			clearError('serverUrl_error');

			setCheckbox('proxyRequired', NO);
			$('#proxyRequired').prop('disabled', true);

			$('#proxyServer').val(EMPTY_STRING);
			$('#proxyServer').prop('readOnly', true);
			clearError('proxyServer_error');
			$('#proxyServerPort').val(EMPTY_STRING);
			$('#proxyServerPort').prop('readOnly', true);
			clearError('proxyServerPort_error');

			setCheckbox('proxyServerAuthentication', NO);
			$('#proxyServerAuthentication').prop('disabled', true);

			$('#proxyAccountName').val(EMPTY_STRING);
			$('#proxyAccountName').prop('readOnly', true);
			$('#proxyAccountPassword').val(EMPTY_STRING);
			$('#proxyAccountPassword').prop('readOnly', true);
			clearError('proxyAccountName_error');
			clearError('proxyAccountPassword_error');

			setCheckbox('authenticationRequired', NO);
			$('#authenticationRequired').prop('disabled', true);

			$('#userNameKey').val(EMPTY_STRING);
			$('#userNameKey').prop('readOnly', true);
			$('#accountName').val(EMPTY_STRING);
			$('#accountPasswordKey').val(EMPTY_STRING);
			$('#accountPasswordKey').prop('readOnly', true);
			$('#accountName').prop('readOnly', true);
			$('#accountPassword').val(EMPTY_STRING);
			$('#accountPassword').prop('readOnly', true);
			$('#mobileNumberKey').val(EMPTY_STRING);
			$('#mobileNumberKey').prop('readOnly', true);
			$('#messageKey').val(EMPTY_STRING);
			$('#messageKey').prop('readOnly', true);
			clearError('userNameKey_error');
			clearError('accountName_error');
			clearError('accountPasswordKey_error');
			clearError('accountPassword_error');
			clearError('mobileNumberKey_error');
			clearError('messageKey_error');
		}
		break;
	case 'proxyRequired':
		if ($('#proxyRequired').is(':checked')) {
			$('#proxyServer').prop('readOnly', false);
			$('#proxyServerPort').prop('readOnly', false);
		} else {
			$('#proxyServer').val(EMPTY_STRING);
			$('#proxyServer').prop('readOnly', true);
			clearError('proxyServer_error');
			clearError('proxyServerPort_error');
			$('#proxyServerPort').val(EMPTY_STRING);
			$('#proxyServerPort').prop('readOnly', true);
		}
		break;
	case 'proxyServerAuthentication':
		if ($('#proxyServerAuthentication').is(':checked')) {
			$('#proxyAccountName').prop('readOnly', false);
			$('#proxyAccountPassword').prop('readOnly', false);
		} else {
			$('#proxyAccountName').val(EMPTY_STRING);
			$('#proxyAccountName').prop('readOnly', true);
			$('#proxyAccountPassword').val(EMPTY_STRING);
			$('#proxyAccountPassword').prop('readOnly', true);
			clearError('proxyAccountName_error');
			clearError('proxyAccountPassword_error');
		}
		break;
	case 'authenticationRequired':
		if ($('#authenticationRequired').is(':checked')) {
			$('#userNameKey').prop('readOnly', false);
			$('#accountName').prop('readOnly', false);
			$('#accountPasswordKey').prop('readOnly', false);
			$('#accountPassword').prop('readOnly', false);
			$('#mobileNumberKey').prop('readOnly', false);
			$('#messageKey').prop('readOnly', false);
		} else {
			$('#userNameKey').val(EMPTY_STRING);
			$('#userNameKey').prop('readOnly', true);
			$('#accountName').val(EMPTY_STRING);
			$('#accountPasswordKey').val(EMPTY_STRING);
			$('#accountPasswordKey').prop('readOnly', true);
			$('#accountName').prop('readOnly', true);
			$('#accountPassword').val(EMPTY_STRING);
			$('#accountPassword').prop('readOnly', true);
			$('#mobileNumberKey').val(EMPTY_STRING);
			$('#mobileNumberKey').prop('readOnly', true);
			$('#messageKey').val(EMPTY_STRING);
			$('#messageKey').prop('readOnly', true);
			clearError('userNameKey_error');
			clearError('accountName_error');
			clearError('accountPasswordKey_error');
			clearError('accountPassword_error');
			clearError('mobileNumberKey_error');
			clearError('messageKey_error');
		}
		break;
	case 'smppAllowed':
		if ($('#smppAllowed').is(':checked')) {
			$('#senderNumber').prop('readOnly', false);
			$('#systemType').prop('readOnly', false);
			$('#addressRange').prop('readOnly', false);
			$('#transceiverEnabled').prop('disabled', false);
			$('#mainServer').prop('readOnly', false);
			$('#mainServerPort').prop('readOnly', false);
			$('#failOverServer').prop('readOnly', false);
			$('#failOverServerPort').prop('readOnly', false);
			$('#dispatchType').prop('disabled', false);
			$('#retryTimeout').prop('readOnly', false);
		} else {
			$('#senderNumber').val(EMPTY_STRING);
			$('#senderNumber').prop('readOnly', true);

			$('#systemType').val(EMPTY_STRING);
			$('#systemType').prop('readOnly', true);

			$('#addressRange').val(EMPTY_STRING);
			$('#addressRange').prop('readOnly', true);

			setCheckbox('transceiverEnabled', NO);
			$('#transceiverEnabled_error').html(EMPTY_STRING);
			$('#transceiverEnabled').prop('disabled', true);

			$('#mainServer').val(EMPTY_STRING);
			$('#mainServer').prop('readOnly', true);

			$('#mainServerPort').val(EMPTY_STRING);
			$('#mainServerPort').prop('readOnly', true);

			$('#failOverServer').val(EMPTY_STRING);
			$('#failOverServer').prop('readOnly', true);

			$('#failOverServerPort').val(EMPTY_STRING);
			$('#failOverServerPort').prop('readOnly', true);

			$('#dispatchType').val(EMPTY_STRING);
			$('#dispatchType').prop('disabled', true);

			$('#retryTimeout').val(EMPTY_STRING);
			$('#retryTimeout').prop('readOnly', true);
			clearError('senderNumber_error');
			clearError('systemType_error');
			clearError('addressRange_error');
			clearError('mainServer_error');
			clearError('mainServerPort_error');
			clearError('failOverServer_error');
			clearError('failOverServerPort_error');
			clearError('dispatchType_error');
			clearError('retryTimeout_error');
		}
		break;
	}
}

function loadData() {
	$('#smsCode').val(validator.getValue('SMS_CODE'));
	$('#smsCode_desc').html(validator.getValue('F1_SMS_DESCN'));
	$('#effectiveDate').val(validator.getValue('EFFT_DATE'));
	var httpAllowed = validator.getValue('SMSC_HTTP_ALLOWED');
	var sslRequired = validator.getValue('SSL_REQUIRED');
	var serverUrl = validator.getValue('SERVER_URL');
	var proxyRequired = validator.getValue('PROXY_REQUIRED');
	var proxyServer = validator.getValue('PROXY_SERVER');
	var proxyServerPort = validator.getValue('PROXY_SERVER_PORT');
	var proxyServerAuthentication = validator.getValue('PROXY_AUTH_REQD');
	var proxyAccountName = validator.getValue('PROXY_USER_NAME');
	var proxyAccountPassword = validator.getValue('PROXY_PASSWORD');
	var authenticationRequired = validator.getValue('AUTH_REQD');
	var userNameKey = validator.getValue('USER_NAME_KEY');
	var accountName = validator.getValue('USER_NAME');
	var accountPasswordKey = validator.getValue('PASSWORD_KEY');
	var accountPassword = validator.getValue('PASSWORD');
	var mobileNumberKey = validator.getValue('MOBILE_NUMBER_KEY');
	var messageKey = validator.getValue('MESSAGE_KEY');
	var smppAllowed = validator.getValue('SMSC_SMPP_ALLOWED');
	var senderNumber = validator.getValue('SOURCE_NUMBER');
	var systemType = validator.getValue('SYSTEM_TYPE');
	var addressRange = validator.getValue('ADDRESS_RANGE');
	var transceiverEnabled = validator.getValue('TRANSCEIVER_MODE');
	var mainServer = validator.getValue('MAIN_SMPP_SERVER');
	var mainServerPort = validator.getValue('MAIN_SMPP_PORT');
	var failOverServer = validator.getValue('FAILOVER_SMPP_SERVER');
	var failOverServerPort = validator.getValue('FAILOVER_SMPP_PORT');
	var dispatchType = validator.getValue('SMS_TYPE');
	var retryTimeout = validator.getValue('RETRY_TIMEOUT');
	$('#remarks').val(validator.getValue('REMARKS'));
	if (smsCode_val(false)) {
		sendAllowed = (validator.getValue('SMS_SEND_ALLOWED'));
		if (sendAllowed == COLUMN_ENABLE) {
			$('#httpAllowed').prop('disabled', false);
			setCheckbox('httpAllowed', httpAllowed);
			if ($('#httpAllowed').is(':checked')) {
				$('#sslRequired').prop('disabled', false);
				setCheckbox('sslRequired', sslRequired);
				$('#serverUrl').prop('readOnly', false);
				$('#serverUrl').val(serverUrl);
				$('#proxyRequired').prop('disabled', false);
				setCheckbox('proxyRequired', proxyRequired);
				if ($('#proxyRequired').is(':checked')) {
					$('#proxyServer').prop('readOnly', false);
					$('#proxyServer').val(proxyServer);
					$('#proxyServerPort').prop('readOnly', false);
					$('#proxyServerPort').val(proxyServerPort);
				} else {
					clearError('proxyServer_error');
					clearError('proxyServerPort_error');
					$('#proxyServer').val(EMPTY_STRING);
					$('#proxyServer').prop('readOnly', true);
					$('#proxyServerPort').val(EMPTY_STRING);
					$('#proxyServerPort').prop('readOnly', true);
				}
				$('#proxyServerAuthentication').prop('disabled', false);
				setCheckbox('proxyServerAuthentication', proxyServerAuthentication);
				if ($('#proxyServerAuthentication').is(':checked')) {
					$('#proxyAccountName').prop('readOnly', false);
					$('#proxyAccountName').val(proxyAccountName);
					$('#proxyAccountPassword').prop('readOnly', false);
					$('#proxyAccountPassword').val(proxyAccountPassword);
				} else {
					$('#proxyAccountName').val(EMPTY_STRING);
					$('#proxyAccountName').prop('readOnly', true);
					$('#proxyAccountPassword').val(EMPTY_STRING);
					$('#proxyAccountPassword').prop('readOnly', true);
					clearError('proxyAccountName_error');
					clearError('proxyAccountPassword_error');
				}
				$('#authenticationRequired').prop('disabled', false);
				setCheckbox('authenticationRequired', authenticationRequired);
				if ($('#authenticationRequired').is(':checked')) {
					$('#userNameKey').prop('readOnly', false);
					$('#userNameKey').val(userNameKey);
					$('#accountName').prop('readOnly', false);
					$('#accountName').val(accountName);
					$('#accountPasswordKey').prop('readOnly', false);
					$('#accountPasswordKey').val(accountPasswordKey);
					$('#accountPassword').prop('readOnly', false);
					$('#accountPassword').val(accountPassword);
					$('#mobileNumberKey').prop('readOnly', false);
					$('#mobileNumberKey').val(mobileNumberKey);
					$('#messageKey').prop('readOnly', false);
					$('#messageKey').val(messageKey);
				} else {
					$('#userNameKey').val(EMPTY_STRING);
					$('#userNameKey').prop('readOnly', true);
					$('#accountName').val(EMPTY_STRING);
					$('#accountPasswordKey').val(EMPTY_STRING);
					$('#accountPasswordKey').prop('readOnly', true);
					$('#accountName').prop('readOnly', true);
					$('#accountPassword').val(EMPTY_STRING);
					$('#accountPassword').prop('readOnly', true);
					$('#mobileNumberKey').val(EMPTY_STRING);
					$('#mobileNumberKey').prop('readOnly', true);
					$('#messageKey').val(EMPTY_STRING);
					$('#messageKey').prop('readOnly', true);
				}
			} else {
				setCheckbox('sslRequired', NO);
				$('#sslRequired').prop('disabled', true);

				$('#serverUrl').val(EMPTY_STRING);
				$('#serverUrl').prop('readOnly', true);
				clearError('serverUrl_error');

				setCheckbox('proxyRequired', NO);
				$('#proxyRequired').prop('disabled', true);

				$('#proxyServer').val(EMPTY_STRING);
				$('#proxyServer').prop('readOnly', true);
				$('#proxyServerPort').val(EMPTY_STRING);
				$('#proxyServerPort').prop('readOnly', true);
				clearError('proxyServer_error');
				clearError('proxyServerPort_error');

				setCheckbox('proxyServerAuthentication', NO);
				$('#proxyServerAuthentication').prop('disabled', true);

				$('#proxyAccountName').val(EMPTY_STRING);
				$('#proxyAccountName').prop('readOnly', true);
				$('#proxyAccountPassword').val(EMPTY_STRING);
				$('#proxyAccountPassword').prop('readOnly', true);
				clearError('proxyAccountName_error');
				clearError('proxyAccountPassword_error');

				setCheckbox('authenticationRequired', NO);
				$('#authenticationRequired').prop('disabled', true);

				$('#userNameKey').val(EMPTY_STRING);
				$('#userNameKey').prop('readOnly', true);
				$('#accountName').val(EMPTY_STRING);
				$('#accountPasswordKey').val(EMPTY_STRING);
				$('#accountPasswordKey').prop('readOnly', true);
				$('#accountName').prop('readOnly', true);
				$('#accountPassword').val(EMPTY_STRING);
				$('#accountPassword').prop('readOnly', true);
				$('#mobileNumberKey').val(EMPTY_STRING);
				$('#mobileNumberKey').prop('readOnly', true);
				$('#messageKey').val(EMPTY_STRING);
				$('#messageKey').prop('readOnly', true);
				clearError('userNameKey_error');
				clearError('accountName_error');
				clearError('accountPasswordKey_error');
				clearError('accountPassword_error');
				clearError('mobileNumberKey_error');
				clearError('messageKey_error');
			}
			$('#smppAllowed').prop('disabled', false);
			setCheckbox('smppAllowed', smppAllowed);
			if ($('#smppAllowed').is(':checked')) {
				$('#senderNumber').prop('readOnly', false);
				$('#senderNumber').val(senderNumber);
				$('#systemType').prop('readOnly', false);
				$('#systemType').val(systemType);
				$('#addressRange').prop('readOnly', false);
				$('#addressRange').val(addressRange);
				$('#transceiverEnabled').prop('disabled', false);
				setCheckbox('transceiverEnabled', transceiverEnabled);
				$('#mainServer').prop('readOnly', false);
				$('#mainServer').val(mainServer);
				$('#mainServerPort').prop('readOnly', false);
				$('#mainServerPort').val(mainServerPort);
				$('#failOverServer').prop('readOnly', false);
				$('#failOverServer').val(failOverServer);
				$('#failOverServerPort').prop('readOnly', false);
				$('#failOverServerPort').val(failOverServerPort);
				$('#dispatchType').prop('disabled', false);
				$('#dispatchType').val(dispatchType);
				if (dispatchType == 'P') {
					$('#retryTimeout').prop('readOnly', false);
					$('#retryTimeout').val(retryTimeout);
				} else {
					$('#retryTimeout').val(EMPTY_STRING);
					$('#retryTimeout').prop('readOnly', true);
					clearError('retryTimeout_error');
				}
			} else {
				$('#senderNumber').val(EMPTY_STRING);
				$('#senderNumber').prop('readOnly', true);

				$('#systemType').val(EMPTY_STRING);
				$('#systemType').prop('readOnly', true);

				$('#addressRange').val(EMPTY_STRING);
				$('#addressRange').prop('readOnly', true);

				setCheckbox('transceiverEnabled', NO);
				$('#transceiverEnabled_error').html(EMPTY_STRING);
				$('#transceiverEnabled').prop('disabled', true);

				$('#mainServer').val(EMPTY_STRING);
				$('#mainServer').prop('readOnly', true);

				$('#mainServerPort').val(EMPTY_STRING);
				$('#mainServerPort').prop('readOnly', true);

				$('#failOverServer').val(EMPTY_STRING);
				$('#failOverServer').prop('readOnly', true);

				$('#failOverServerPort').val(EMPTY_STRING);
				$('#failOverServerPort').prop('readOnly', true);

				$('#dispatchType').val(EMPTY_STRING);
				$('#dispatchType').prop('disabled', true);

				$('#retryTimeout').val(EMPTY_STRING);
				$('#retryTimeout').prop('readOnly', true);
				clearError('senderNumber_error');
				clearError('systemType_error');
				clearError('addressRange_error');
				clearError('mainServer_error');
				clearError('mainServerPort_error');
				clearError('failOverServer_error');
				clearError('failOverServerPort_error');
				clearError('dispatchType_error');
				clearError('retryTimeout_error');
			}
		} else {
			setCheckbox('httpAllowed', NO);
			$('#httpAllowed_error').html(EMPTY_STRING);
			$('#httpAllowed').prop('disabled', true);

			setCheckbox('sslRequired', NO);
			$('#sslRequired').prop('disabled', true);

			$('#serverUrl').val(EMPTY_STRING);
			$('#serverUrl').prop('readOnly', true);
			clearError('serverUrl_error');

			setCheckbox('proxyRequired', NO);
			$('#proxyRequired').prop('disabled', true);

			$('#proxyServer').val(EMPTY_STRING);
			$('#proxyServer').prop('readOnly', true);
			clearError('proxyServer_error');
			$('#proxyServerPort').val(EMPTY_STRING);
			$('#proxyServerPort').prop('readOnly', true);
			clearError('proxyServerPort_error');

			setCheckbox('proxyServerAuthentication', NO);
			$('#proxyServerAuthentication').prop('disabled', true);

			$('#proxyAccountName').val(EMPTY_STRING);
			$('#proxyAccountName').prop('readOnly', true);
			$('#proxyAccountPassword').val(EMPTY_STRING);
			$('#proxyAccountPassword').prop('readOnly', true);
			clearError('proxyAccountName_error');
			clearError('proxyAccountPassword_error');

			setCheckbox('authenticationRequired', NO);
			$('#authenticationRequired').prop('disabled', true);

			$('#userNameKey').val(EMPTY_STRING);
			$('#userNameKey').prop('readOnly', true);
			$('#accountName').val(EMPTY_STRING);
			$('#accountPasswordKey').val(EMPTY_STRING);
			$('#accountPasswordKey').prop('readOnly', true);
			$('#accountName').prop('readOnly', true);
			$('#accountPassword').val(EMPTY_STRING);
			$('#accountPassword').prop('readOnly', true);
			$('#mobileNumberKey').val(EMPTY_STRING);
			$('#mobileNumberKey').prop('readOnly', true);
			$('#messageKey').val(EMPTY_STRING);
			$('#messageKey').prop('readOnly', true);
			clearError('userNameKey_error');
			clearError('accountName_error');
			clearError('accountPasswordKey_error');
			clearError('accountPassword_error');
			clearError('mobileNumberKey_error');
			clearError('messageKey_error');

			setCheckbox('smppAllowed', NO);
			$('#smppAllowed_error').html(EMPTY_STRING);
			$('#smppAllowed').prop('disabled', true);

			$('#senderNumber').val(EMPTY_STRING);
			$('#senderNumber').prop('readOnly', true);

			$('#systemType').val(EMPTY_STRING);
			$('#systemType').prop('readOnly', true);

			$('#addressRange').val(EMPTY_STRING);
			$('#addressRange').prop('readOnly', true);

			setCheckbox('transceiverEnabled', NO);
			$('#transceiverEnabled_error').html(EMPTY_STRING);
			$('#transceiverEnabled').prop('disabled', true);

			$('#mainServer').val(EMPTY_STRING);
			$('#mainServer').prop('readOnly', true);

			$('#mainServerPort').val(EMPTY_STRING);
			$('#mainServerPort').prop('readOnly', true);

			$('#failOverServer').val(EMPTY_STRING);
			$('#failOverServer').prop('readOnly', true);

			$('#failOverServerPort').val(EMPTY_STRING);
			$('#failOverServerPort').prop('readOnly', true);

			$('#dispatchType').val(EMPTY_STRING);
			$('#dispatchType').prop('disabled', true);

			$('#retryTimeout').val(EMPTY_STRING);
			$('#retryTimeout').prop('readOnly', true);
			clearError('senderNumber_error');
			clearError('systemType_error');
			clearError('addressRange_error');
			clearError('mainServer_error');
			clearError('mainServerPort_error');
			clearError('failOverServer_error');
			clearError('failOverServerPort_error');
			clearError('dispatchType_error');
			clearError('retryTimeout_error');
		}
	}
	resetLoading();
}
function view(source, primaryKey) {
	hideParent('access/qsmsconfig', source, primaryKey);
	resetLoading();
}
function validate(id) {
	switch (id) {
	case 'smsCode':
		smsCode_val(true);
		break;
	case 'effectiveDate':
		effectiveDate_val();
		break;
	case 'httpAllowed':
		httpAllowed_val();
		break;
	case 'sslRequired':
		sslRequired_val();
		break;
	case 'senderNumber':
		senderNumber_val();
		break;
	case 'serverUrl':
		serverUrl_val();
		break;
	case 'proxyRequired':
		proxyRequired_val();
		break;
	case 'proxyServer':
		proxyServer_val();
		break;
	case 'proxyServerPort':
		proxyServerPort_val();
		break;
	case 'proxyServerAuthentication':
		proxyServerAuthentication_val();
		break;
	case 'proxyAccountName':
		proxyAccountName_val();
		break;
	case 'proxyAccountPassword':
		proxyAccountPassword_val();
		break;
	case 'authenticationRequired':
		authenticationRequired_val();
		break;
	case 'userNameKey':
		userNameKey_val();
		break;
	case 'accountName':
		accountName_val();
		break;
	case 'accountPasswordKey':
		accountPasswordKey_val();
		break;
	case 'accountPassword':
		accountPassword_val();
		break;
	case 'mobileNumberKey':
		mobileNumberKey_val();
		break;
	case 'messageKey':
		messageKey_val();
		break;
	case 'smppAllowed':
		smppAllowed_val();
		break;
	case 'systemType':
		systemType_val();
		break;
	case 'addressRange':
		addressRange_val();
		break;
	case 'transceiverEnabled':
		transceiverEnabled_val();
		break;
	case 'mainServer':
		mainServer_val();
		break;
	case 'mainServerPort':
		mainServerPort_val();
		break;
	case 'failOverServer':
		failOverServer_val();
		break;
	case 'failOverServerPort':
		failOverServerPort_val();
		break;
	case 'dispatchType':
		dispatchType_val();
		break;
	case 'retryTimeout':
		retryTimeout_val();
		break;
	case 'remarks':
		remarks_val();
		break;
	}
}

function backtrack(id) {
	switch (id) {
	case 'smsCode':
		setFocusLast('smsCode');
		break;
	case 'effectiveDate':
		setFocusLast('smsCode');
		break;
	case 'httpAllowed':
		setFocusLast('effectiveDate');
		break;
	case 'sslRequired':
		if ($('#httpAllowed').prop('disabled')) {
			setFocusLast('effectiveDate');
		} else {
			setFocusLast('httpAllowed');
		}
		break;
	case 'serverUrl':
		if ($('#sslRequired').prop('disabled')) {
			setFocusLast('httpAllowed');
			if ($('#httpAllowed').prop('disabled')) {
				setFocusLast('effectiveDate');
			} else {
				setFocusLast('httpAllowed');
			}
		} else {
			setFocusLast('sslRequired');
		}
		break;
	case 'proxyRequired':
		if ($('#serverUrl').prop('readonly')) {
			setFocusLast('sslRequired');
			if ($('#sslRequired').prop('disabled')) {
				setFocusLast('httpAllowed');
			} else {
				setFocusLast('sslRequired');
			}
		} else {
			setFocusLast('serverUrl');
		}
		break;
	case 'proxyServer':
		if ($('#proxyRequired').prop('disabled')) {
			setFocusLast('serverUrl');
			if ($('#serverUrl').prop('readonly')) {
				setFocusLast('sslRequired');
			} else {
				setFocusLast('serverUrl');
			}
		} else {
			setFocusLast('proxyRequired');
		}
		break;
	case 'proxyServerPort':
		setFocusLast('proxyServer');
		break;
	case 'proxyServerAuthentication':
		if ($('#proxyServerPort').prop('readonly')) {
			setFocusLast('proxyRequired');
		} else {
			setFocusLast('proxyServerPort');
		}
		break;
	case 'proxyAccountName':
		if ($('#proxyServerAuthentication').prop('disabled')) {
			setFocusLast('proxyServerPort');
			if ($('#proxyServerPort').prop('readonly')) {
				setFocusLast('proxyRequired');
			} else {
				setFocusLast('proxyServerPort');
			}
		} else {
			setFocusLast('proxyServerAuthentication');
		}
		break;
	case 'proxyAccountPassword':
		setFocusLast('proxyAccountName');
		break;
	case 'authenticationRequired':
		if ($('#proxyAccountPassword').prop('readonly')) {
			setFocusLast('proxyServerAuthentication');
		} else {
			setFocusLast('proxyAccountPassword');
		}
		break;
	case 'userNameKey':
		if ($('#authenticationRequired').prop('disabled')) {
			setFocusLast('proxyAccountPassword');
			if ($('#proxyAccountPassword').prop('readonly')) {
				setFocusLast('proxyServerAuthentication');
			} else {
				setFocusLast('proxyAccountPassword');
			}
		} else {
			setFocusLast('authenticationRequired');
		}
		break;
	case 'accountName':
		setFocusLast('userNameKey');
		break;
	case 'accountPasswordKey':
		setFocusLast('accountName');
		break;
	case 'accountPassword':
		setFocusLast('accountPasswordKey');
		break;
	case 'mobileNumberKey':
		setFocusLast('accountPassword');
		break;
	case 'messageKey':
		setFocusLast('mobileNumberKey');
		break;
	case 'smppAllowed':
		if ($('#messageKey').prop('readonly')) {
			setFocusLast('authenticationRequired');
			if ($('#authenticationRequired').prop('disabled')) {
				setFocusLast('proxyServerAuthentication');
				if ($('#proxyServerAuthentication').prop('disabled')) {
					setFocusLast('proxyRequired');
					if ($('#proxyRequired').prop('disabled')) {
						setFocusLast('httpAllowed');
					} else {
						setFocusLast('proxyRequired');
					}
				} else {
					setFocusLast('proxyServerAuthentication');
				}
			} else {
				setFocusLast('authenticationRequired');
			}
		} else {
			setFocusLast('messageKey');
		}
		break;
	case 'senderNumber':
		if ($('#smppAllowed').prop('disabled')) {
			setFocusLast('authenticationRequired');
		} else {
			setFocusLast('smppAllowed');
		}
		break;
	case 'systemType':
		setFocusLast('senderNumber');
		break;
	case 'addressRange':
		setFocusLast('systemType');
		break;
	case 'transceiverEnabled':
		setFocusLast('addressRange');
		break;
	case 'mainServer':
		setFocusLast('transceiverEnabled');
		break;
	case 'mainServerPort':
		setFocusLast('mainServer');
		break;
	case 'failOverServer':
		setFocusLast('mainServerPort');
		break;
	case 'failOverServerPort':
		setFocusLast('failOverServer');
		break;
	case 'dispatchType':
		if ($('#failOverServerPort').prop('readonly')) {
			setFocusLast('smppAllowed');
		} else {
			setFocusLast('failOverServerPort');
		}
		break;
	case 'retryTimeout':
		if ($('#dispatchType').prop('disabled')) {
			setFocusLast('smppAllowed');
			if ($('#smppAllowed').prop('disabled')) {
				setFocusLast('effectiveDate');
			} else {
				setFocusLast('smppAllowed');
			}
		} else {
			setFocusLast('dispatchType');
		}
		break;
	case 'remarks':
		if ($('#retryTimeout').prop('readonly')) {
			setFocusLast('dispatchType');
			if ($('dispatchType').val() == 'P') {
				setFocusLast('retryTimeout');
			} else {
				setFocusLast('dispatchType');
			}
			if ($('#dispatchType').prop('disabled')) {
				setFocusLast('smppAllowed');
				if ($('#smppAllowed').prop('disabled')) {
					setFocusLast('authenticationRequired');
					if ($('#authenticationRequired').prop('disabled')) {
						setFocusLast('proxyServerAuthentication');
						if ($('#proxyServerAuthentication').prop('disabled')) {
							setFocusLast('proxyRequired');
							if ($('#proxyRequired').prop('disabled')) {
								setFocusLast('httpAllowed');
								if ($('#httpAllowed').prop('disabled')) {
									setFocusLast('effectiveDate');
								} else {
									setFocusLast('httpAllowed');
								}
							} else {
								setFocusLast('proxyRequired');
							}
						} else {
							setFocusLast('proxyServerAuthentication');
						}
					} else {
						setFocusLast('authenticationRequired');
					}
				} else {
					setFocusLast('smppAllowed');
				}
			} else {
				setFocusLast('dispatchType');
			}
		} else {
			setFocusLast('retryTimeout');
		}
		break;
	break;
}
}

function smsCode_val(valMode) {
var value = $('#smsCode').val();
clearError('smsCode_error');
if (isEmpty(value)) {
	setError('smsCode_error', HMS_MANDATORY);
	return false;
} else if (!isValidCode(value)) {
	setError('smsCode_error', HMS_INVALID_FORMAT);
	return false;
} else {
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('SMS_CODE', $('#smsCode').val());
	validator.setValue(ACTION, USAGE);
	validator.setClass('patterns.config.web.forms.access.ismsconfigbean');
	validator.setMethod('validateSMSCode');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('smsCode_error', validator.getValue(ERROR));
		$('#smsCode_desc').html(EMPTY_STRING);
		clearNonPKFields();
		return false;
	} else {
		$('#smsCode_desc').html(validator.getValue('SMS_DESCN'));
		sendAllowed = (validator.getValue('SMS_SEND_ALLOWED'));
		if (sendAllowed == COLUMN_ENABLE) {
			$('#httpAllowed').prop('disabled', false);
			if ($('#httpAllowed').is(':checked')) {
				$('#sslRequired').prop('disabled', false);
				$('#serverUrl').prop('readOnly', false);
				$('#proxyRequired').prop('disabled', false);
				if ($('#proxyRequired').is(':checked')) {
					$('#proxyServer').prop('readOnly', false);
					$('#proxyServerPort').prop('readOnly', false);
				} else {
					$('#proxyServer').val(EMPTY_STRING);
					$('#proxyServer').prop('readOnly', true);
					clearError('proxyServer_error');
					$('#proxyServerPort').val(EMPTY_STRING);
					$('#proxyServerPort').prop('readOnly', true);
					clearError('proxyServerPort_error');
				}
				$('#proxyServerAuthentication').prop('disabled', false);
				if ($('#proxyServerAuthentication').is(':checked')) {
					$('#proxyAccountName').prop('readOnly', false);
					$('#proxyAccountPassword').prop('readOnly', false);
				} else {
					$('#proxyAccountName').val(EMPTY_STRING);
					$('#proxyAccountName').prop('readOnly', true);
					$('#proxyAccountPassword').val(EMPTY_STRING);
					$('#proxyAccountPassword').prop('readOnly', true);
					clearError('proxyAccountName_error');
					clearError('proxyAccountPassword_error');
				}
				$('#authenticationRequired').prop('disabled', false);
				if ($('#authenticationRequired').is(':checked')) {
					$('#userNameKey').prop('readOnly', false);
					$('#accountName').prop('readOnly', false);
					$('#accountPasswordKey').prop('readOnly', false);
					$('#accountPassword').prop('readOnly', false);
					$('#mobileNumberKey').prop('readOnly', false);
					$('#messageKey').prop('readOnly', false);
				} else {
					$('#userNameKey').val(EMPTY_STRING);
					$('#userNameKey').prop('readOnly', true);
					$('#accountName').val(EMPTY_STRING);
					$('#accountPasswordKey').val(EMPTY_STRING);
					$('#accountPasswordKey').prop('readOnly', true);
					$('#accountName').prop('readOnly', true);
					$('#accountPassword').val(EMPTY_STRING);
					$('#accountPassword').prop('readOnly', true);
					$('#mobileNumberKey').val(EMPTY_STRING);
					$('#mobileNumberKey').prop('readOnly', true);
					$('#messageKey').val(EMPTY_STRING);
					$('#messageKey').prop('readOnly', true);
					clearError('userNameKey_error');
					clearError('accountName_error');
					clearError('accountPasswordKey_error');
					clearError('accountPassword_error');
					clearError('mobileNumberKey_error');
					clearError('messageKey_error');
				}
			} else {
				setCheckbox('sslRequired', NO);
				$('#sslRequired').prop('disabled', true);

				$('#serverUrl').val(EMPTY_STRING);
				$('#serverUrl').prop('readOnly', true);
				clearError('serverUrl_error');

				setCheckbox('proxyRequired', NO);
				$('#proxyRequired').prop('disabled', true);

				$('#proxyServer').val(EMPTY_STRING);
				$('#proxyServer').prop('readOnly', true);
				clearError('proxyServer_error');
				$('#proxyServerPort').val(EMPTY_STRING);
				$('#proxyServerPort').prop('readOnly', true);
				clearError('proxyServerPort_error');

				setCheckbox('proxyServerAuthentication', NO);
				$('#proxyServerAuthentication').prop('disabled', true);

				$('#proxyAccountName').val(EMPTY_STRING);
				$('#proxyAccountName').prop('readOnly', true);
				$('#proxyAccountPassword').val(EMPTY_STRING);
				$('#proxyAccountPassword').prop('readOnly', true);

				setCheckbox('authenticationRequired', NO);
				$('#authenticationRequired').prop('disabled', true);

				$('#userNameKey').val(EMPTY_STRING);
				$('#userNameKey').prop('readOnly', true);
				$('#accountName').val(EMPTY_STRING);
				$('#accountPasswordKey').val(EMPTY_STRING);
				$('#accountPasswordKey').prop('readOnly', true);
				$('#accountName').prop('readOnly', true);
				$('#accountPassword').val(EMPTY_STRING);
				$('#accountPassword').prop('readOnly', true);
				$('#mobileNumberKey').val(EMPTY_STRING);
				$('#mobileNumberKey').prop('readOnly', true);
				$('#messageKey').val(EMPTY_STRING);
				$('#messageKey').prop('readOnly', true);
				clearError('userNameKey_error');
				clearError('accountName_error');
				clearError('accountPasswordKey_error');
				clearError('accountPassword_error');
				clearError('mobileNumberKey_error');
				clearError('messageKey_error');
			}
			$('#smppAllowed').prop('disabled', false);
			if ($('#smppAllowed').is(':checked')) {
				$('#senderNumber').prop('readOnly', false);
				$('#systemType').prop('readOnly', false);
				$('#addressRange').prop('readOnly', false);
				$('#transceiverEnabled').prop('disabled', false);
				$('#mainServer').prop('readOnly', false);
				$('#mainServerPort').prop('readOnly', false);
				$('#failOverServer').prop('readOnly', false);
				$('#failOverServerPort').prop('readOnly', false);
				$('#dispatchType').prop('disabled', false);
				if ($('#dispatchType').val() == 'P') {
					$('#retryTimeout').prop('readOnly', false);
				} else {
					$('#retryTimeout').val(EMPTY_STRING);
					$('#retryTimeout').prop('readOnly', true);
					clearError('retryTimeout_error');
				}
			} else {
				$('#senderNumber').val(EMPTY_STRING);
				$('#senderNumber').prop('readOnly', true);

				$('#systemType').val(EMPTY_STRING);
				$('#systemType').prop('readOnly', true);

				$('#addressRange').val(EMPTY_STRING);
				$('#addressRange').prop('readOnly', true);

				setCheckbox('transceiverEnabled', NO);
				$('#transceiverEnabled_error').html(EMPTY_STRING);
				$('#transceiverEnabled').prop('disabled', true);

				$('#mainServer').val(EMPTY_STRING);
				$('#mainServer').prop('readOnly', true);

				$('#mainServerPort').val(EMPTY_STRING);
				$('#mainServerPort').prop('readOnly', true);

				$('#failOverServer').val(EMPTY_STRING);
				$('#failOverServer').prop('readOnly', true);

				$('#failOverServerPort').val(EMPTY_STRING);
				$('#failOverServerPort').prop('readOnly', true);

				$('#dispatchType').val(EMPTY_STRING);
				$('#dispatchType').prop('disabled', true);

				$('#retryTimeout').val(EMPTY_STRING);
				$('#retryTimeout').prop('readOnly', true);

				clearError('senderNumber_error');
				clearError('systemType_error');
				clearError('addressRange_error');
				clearError('mainServer_error');
				clearError('mainServerPort_error');
				clearError('failOverServer_error');
				clearError('failOverServerPort_error');
				clearError('dispatchType_error');
				clearError('retryTimeout_error');
			}
		} else {
			setCheckbox('httpAllowed', NO);
			$('#httpAllowed_error').html(EMPTY_STRING);
			$('#httpAllowed').prop('disabled', true);

			setCheckbox('sslRequired', NO);
			$('#sslRequired').prop('disabled', true);

			$('#serverUrl').val(EMPTY_STRING);
			$('#serverUrl').prop('readOnly', true);
			clearError('serverUrl_error');

			setCheckbox('proxyRequired', NO);
			$('#proxyRequired').prop('disabled', true);

			$('#proxyServer').val(EMPTY_STRING);
			$('#proxyServer').prop('readOnly', true);
			clearError('proxyServer_error');
			$('#proxyServerPort').val(EMPTY_STRING);
			$('#proxyServerPort').prop('readOnly', true);
			clearError('proxyServerPort_error');

			setCheckbox('proxyServerAuthentication', NO);
			$('#proxyServerAuthentication').prop('disabled', true);

			$('#proxyAccountName').val(EMPTY_STRING);
			$('#proxyAccountName').prop('readOnly', true);
			$('#proxyAccountPassword').val(EMPTY_STRING);
			$('#proxyAccountPassword').prop('readOnly', true);
			clearError('proxyAccountName_error');
			clearError('proxyAccountPassword_error');

			setCheckbox('authenticationRequired', NO);
			$('#authenticationRequired').prop('disabled', true);

			$('#userNameKey').val(EMPTY_STRING);
			$('#userNameKey').prop('readOnly', true);
			$('#accountName').val(EMPTY_STRING);
			$('#accountPasswordKey').val(EMPTY_STRING);
			$('#accountPasswordKey').prop('readOnly', true);
			$('#accountName').prop('readOnly', true);
			$('#accountPassword').val(EMPTY_STRING);
			$('#accountPassword').prop('readOnly', true);
			$('#mobileNumberKey').val(EMPTY_STRING);
			$('#mobileNumberKey').prop('readOnly', true);
			$('#messageKey').val(EMPTY_STRING);
			$('#messageKey').prop('readOnly', true);
			clearError('userNameKey_error');
			clearError('accountName_error');
			clearError('accountPasswordKey_error');
			clearError('accountPassword_error');
			clearError('mobileNumberKey_error');
			clearError('messageKey_error');

			setCheckbox('smppAllowed', NO);
			$('#smppAllowed_error').html(EMPTY_STRING);
			$('#smppAllowed').prop('disabled', true);

			$('#senderNumber').val(EMPTY_STRING);
			$('#senderNumber').prop('readOnly', true);

			$('#systemType').val(EMPTY_STRING);
			$('#systemType').prop('readOnly', true);

			$('#addressRange').val(EMPTY_STRING);
			$('#addressRange').prop('readOnly', true);

			setCheckbox('transceiverEnabled', NO);
			$('#transceiverEnabled_error').html(EMPTY_STRING);
			$('#transceiverEnabled').prop('disabled', true);

			$('#mainServer').val(EMPTY_STRING);
			$('#mainServer').prop('readOnly', true);

			$('#mainServerPort').val(EMPTY_STRING);
			$('#mainServerPort').prop('readOnly', true);

			$('#failOverServer').val(EMPTY_STRING);
			$('#failOverServer').prop('readOnly', true);

			$('#failOverServerPort').val(EMPTY_STRING);
			$('#failOverServerPort').prop('readOnly', true);

			$('#dispatchType').val(EMPTY_STRING);
			$('#dispatchType').prop('disabled', true);

			$('#retryTimeout').val(EMPTY_STRING);
			$('#retryTimeout').prop('readOnly', true);
			clearError('senderNumber_error');
			clearError('systemType_error');
			clearError('addressRange_error');
			clearError('mainServer_error');
			clearError('mainServerPort_error');
			clearError('failOverServer_error');
			clearError('failOverServerPort_error');
			clearError('dispatchType_error');
			clearError('retryTimeout_error');
		}
	}
}
if (valMode)
	setFocusLast('effectiveDate');
return true;
}

function effectiveDate_val() {
var value = $('#effectiveDate').val();
clearError('effectiveDate_error');
if (isEmpty(value)) {
	$('#effectiveDate').val(cbdval);
	value = $('#effectiveDate').val();
}
if (!isValidEffectiveDate(value, 'effectiveDate_error')) {
	return false;
} else {
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue('SMS_CODE', $('#smsCode').val());
	validator.setValue('EFFT_DATE', $('#effectiveDate').val());
	validator.setValue(ACTION, $('#action').val());
	validator.setClass('patterns.config.web.forms.access.ismsconfigbean');
	validator.setMethod('validateEffectiveDate');
	validator.sendAndReceive();
	if (validator.getValue(ERROR) != EMPTY_STRING) {
		setError('effectiveDate_error', validator.getValue(ERROR));
		setFocusLast('effectiveDate');
		return false;
	} else {
		if ($('#action').val() == MODIFY) {
			PK_VALUE = getEntityCode() + PK_SEPERATOR + $('#smsCode').val() + PK_SEPERATOR + $('#effectiveDate').val();
			if (!loadPKValues(PK_VALUE, 'effectiveDate_error')) {
				return false;
			}
		}
	}
	if (sendAllowed == COLUMN_ENABLE) {
		setFocusLast('httpAllowed');
	} else {
		setFocusLast('remarks');
	}
	return true;
}
}

function httpAllowed_val() {
if (sendAllowed == COLUMN_ENABLE) {
	$('#httpAllowed').prop('disabled', false);
	if ($('#httpAllowed').is(':checked')) {
		$('#sslRequired').prop('disabled', false);
		$('#serverUrl').prop('readOnly', false);
		$('#proxyRequired').prop('disabled', false);
		if ($('#proxyRequired').is(':checked')) {
			$('#proxyServer').prop('readOnly', false);
			$('#proxyServerPort').prop('readOnly', false);
		} else {
			$('#proxyServer').val(EMPTY_STRING);
			$('#proxyServer').prop('readOnly', true);
			clearError('proxyServer_error');
			$('#proxyServerPort').val(EMPTY_STRING);
			$('#proxyServerPort').prop('readOnly', true);
			clearError('proxyServerPort_error');
		}
		$('#proxyServerAuthentication').prop('disabled', false);
		if ($('#proxyServerAuthentication').is(':checked')) {
			$('#proxyAccountName').prop('readOnly', false);
			$('#proxyAccountPassword').prop('readOnly', false);
		} else {
			$('#proxyAccountName').val(EMPTY_STRING);
			$('#proxyAccountName').prop('readOnly', true);
			clearError('proxyAccountName_error');
			$('#proxyAccountPassword').val(EMPTY_STRING);
			$('#proxyAccountPassword').prop('readOnly', true);
			clearError('proxyAccountPassword_error');
		}
		$('#authenticationRequired').prop('disabled', false);
		if ($('#authenticationRequired').is(':checked')) {
			$('#userNameKey').prop('readOnly', false);
			$('#accountName').prop('readOnly', false);
			$('#accountPasswordKey').prop('readOnly', false);
			$('#accountPassword').prop('readOnly', false);
			$('#mobileNumberKey').prop('readOnly', false);
			$('#messageKey').prop('readOnly', false);
		} else {
			$('#userNameKey').val(EMPTY_STRING);
			$('#userNameKey').prop('readOnly', true);
			$('#accountName').val(EMPTY_STRING);
			$('#accountName').prop('readOnly', true);
			$('#accountPasswordKey').val(EMPTY_STRING);
			$('#accountPasswordKey').prop('readOnly', true);
			$('#accountPassword').val(EMPTY_STRING);
			$('#accountPassword').prop('readOnly', true);
			$('#mobileNumberKey').val(EMPTY_STRING);
			$('#mobileNumberKey').prop('readOnly', true);
			$('#messageKey').val(EMPTY_STRING);
			$('#messageKey').prop('readOnly', true);
			clearError('userNameKey_error');
			clearError('accountName_error');
			clearError('accountPasswordKey_error');
			clearError('accountPassword_error');
			clearError('mobileNumberKey_error');
			clearError('messageKey_error');
		}
	} else {
		setCheckbox('sslRequired', NO);
		$('#sslRequired').prop('disabled', true);

		$('#serverUrl').val(EMPTY_STRING);
		$('#serverUrl').prop('readOnly', true);
		clearError('serverUrl_error');
		setCheckbox('proxyRequired', NO);
		$('#proxyRequired').prop('disabled', true);
		$('#proxyServer').val(EMPTY_STRING);
		$('#proxyServer').prop('readOnly', true);
		clearError('proxyServer_error');
		$('#proxyServerPort').val(EMPTY_STRING);
		$('#proxyServerPort').prop('readOnly', true);
		clearError('proxyServerPort_error');
		setCheckbox('proxyServerAuthentication', NO);
		$('#proxyServerAuthentication').prop('disabled', true);
		$('#proxyAccountName').val(EMPTY_STRING);
		$('#proxyAccountName').prop('readOnly', true);
		$('#proxyAccountPassword').val(EMPTY_STRING);
		$('#proxyAccountPassword').prop('readOnly', true);
		clearError('proxyAccountName_error');
		clearError('proxyAccountPassword_error');
		setCheckbox('authenticationRequired', NO);
		$('#authenticationRequired').prop('disabled', true);
		$('#userNameKey').val(EMPTY_STRING);
		$('#userNameKey').prop('readOnly', true);
		$('#accountName').val(EMPTY_STRING);
		$('#accountPasswordKey').val(EMPTY_STRING);
		$('#accountPasswordKey').prop('readOnly', true);
		$('#accountName').prop('readOnly', true);
		$('#accountPassword').val(EMPTY_STRING);
		$('#accountPassword').prop('readOnly', true);
		$('#mobileNumberKey').val(EMPTY_STRING);
		$('#mobileNumberKey').prop('readOnly', true);
		$('#messageKey').val(EMPTY_STRING);
		$('#messageKey').prop('readOnly', true);
		clearError('userNameKey_error');
		clearError('accountName_error');
		clearError('accountPasswordKey_error');
		clearError('accountPassword_error');
		clearError('mobileNumberKey_error');
		clearError('messageKey_error');
	}
	if ($('#httpAllowed').is(':checked')) {
		setFocusLast('sslRequired');
	} else {
		setFocusLast('smppAllowed');
	}
}
setFocusLast('sslRequired');
return true;
}

function sslRequired_val() {
if ($('#httpAllowed').is(':checked')) {
	$('#sslRequired').prop('disabled', false);
} else {
	setCheckbox('sslRequired', NO);
	$('#sslRequired').prop('disabled', true);
}
setFocusLast('serverUrl');
return true;
}

function serverUrl_val() {
if ($('#httpAllowed').is(':checked')) {
	$('#serverUrl').prop('readOnly', false);
	var value = $('#serverUrl').val();
	clearError('serverUrl_error');
	if (isEmpty(value)) {
		setError('serverUrl_error', HMS_MANDATORY);
		return false;
	}
	if (!isValidUrl(value)) {
		setError('serverUrl_error', HMS_INVALID_URL);
		return false;
	}
} else {
	$('#serverUrl').val(EMPTY_STRING);
	$('#serverUrl').prop('readOnly', true);
	clearError('serverUrl_error');
}
setFocusLast('proxyRequired');
return true;
}

function proxyRequired_val() {
if ($('#httpAllowed').is(':checked')) {
	$('#proxyRequired').prop('disabled', false);
	setFocusLast('proxyRequired');
	if ($('#proxyRequired').is(':checked')) {
		setFocusLast('proxyServer');
	} else {
		setFocusLast('proxyServerAuthentication');
	}
} else {
	setCheckbox('proxyRequired', NO);
	$('#proxyRequired').prop('disabled', true);
}
return true;
}

function proxyServer_val() {
if ($('#proxyRequired').is(':checked')) {
	$('#proxyServer').prop('readOnly', false);
	var value = $('#proxyServer').val();
	clearError('proxyServer_error');
	if (isEmpty(value)) {
		setError('proxyServer_error', HMS_MANDATORY);
		return false;
	}
	if (!isValidServerAddress(value)) {
		setError('proxyServer_error', HMS_INVALID_SERVER_ADDRESS);
		return false;
	}
} else {
	$('#proxyServer').val(EMPTY_STRING);
	$('#proxyServer').prop('readOnly', true);
	clearError('proxyServer_error');
}
setFocusLast('proxyServerPort');
return true;
}

function proxyServerPort_val() {
clearError('messageKey_error');
if ($('#proxyRequired').is(':checked')) {
	$('#proxyServerPort').prop('readOnly', false);
	var value = $('#proxyServerPort').val();
	clearError('proxyServerPort_error');
	if (isEmpty(value)) {
		setError('proxyServerPort_error', HMS_MANDATORY);
		return false;
	}
	if (!isNumeric(value)) {
		setError('proxyServerPort_error', HMS_INVALID_NUMBER);
		return false;
	}
	if (!isValidPort(value)) {
		setError('proxyServerPort_error', HMS_INVALID_PORT);
		return false;
	}
} else {
	$('#proxyServerPort').val(EMPTY_STRING);
	$('#proxyServerPort').prop('readOnly', true);
	clearError('proxyServerPort_error');
}
setFocusLast('proxyServerAuthentication');
return true;
}

function proxyServerAuthentication_val() {
if ($('#httpAllowed').is(':checked')) {
	$('#proxyServerAuthentication').prop('disabled', false);
	setFocusLast('proxyServerAuthentication');
	if ($('#proxyServerAuthentication').is(':checked')) {
		setFocusLast('proxyAccountName');
	} else {
		setFocusLast('authenticationRequired');
	}
} else {
	setCheckbox('proxyServerAuthentication', NO);
	$('#proxyServerAuthentication').prop('disabled', true);
}
return true;
}

function proxyAccountName_val() {
if ($('#proxyServerAuthentication').is(':checked')) {
	$('#proxyAccountName').prop('readOnly', false);
	var value = $('#proxyAccountName').val();
	clearError('proxyAccountName_error');
	if (isEmpty(value)) {
		setError('proxyAccountName_error', HMS_MANDATORY);
		return false;
	}
	if (!isValidName(value)) {
		setError('proxyAccountName_error', HMS_INVALID_NAME);
		return false;
	}
} else {
	$('#proxyAccountName').val(EMPTY_STRING);
	$('#proxyAccountName').prop('readOnly', true);
	clearError('proxyAccountName_error');
}
setFocusLast('proxyAccountPassword');
return true;
}

function proxyAccountPassword_val() {
if ($('#proxyServerAuthentication').is(':checked')) {
	$('#proxyAccountPassword').prop('readOnly', false);
	var value = $('#proxyAccountPassword').val();
	clearError('proxyAccountPassword_error');
	if (isEmpty(value)) {
		setError('proxyAccountPassword_error', HMS_MANDATORY);
		return false;
	}
	if (!isValidCredential(value)) {
		setError('proxyAccountPassword_error', HMS_INVALID_CREDENTIALS);
		return false;
	}
} else {
	$('#proxyAccountPassword').val(EMPTY_STRING);
	$('#proxyAccountPassword').prop('readOnly', true);
}
setFocusLast('authenticationRequired');
return true;
}

function authenticationRequired_val() {
if ($('#httpAllowed').is(':checked')) {
	$('#authenticationRequired').prop('disabled', false);
	setFocusLast('authenticationRequired');
	if ($('#authenticationRequired').is(':checked')) {
		setFocusLast('userNameKey');
	} else {
		setFocusLast('smppAllowed');
	}
} else {
	setCheckbox('authenticationRequired', NO);
	$('#authenticationRequired').prop('disabled', true);
}
return true;
}

function userNameKey_val() {
if ($('#authenticationRequired').is(':checked')) {
	$('#userNameKey').prop('readOnly', false);
	var value = $('#userNameKey').val();
	clearError('userNameKey_error');
	if (isEmpty(value)) {
		setError('userNameKey_error', HMS_MANDATORY);
		return false;
	}
	if (!isValidOtherInformation50(value)) {
		setError('userNameKey_error', HMS_INVALID_KEY);
		return false;
	}
} else {
	$('#userNameKey').val(EMPTY_STRING);
	$('#userNameKey').prop('readOnly', true);
	clearError('userNameKey_error');
}
setFocusLast('accountName');
return true;
}

function accountName_val() {
if ($('#authenticationRequired').is(':checked')) {
	$('#accountName').prop('readOnly', false);
	var value = $('#accountName').val();
	clearError('accountName_error');
	if (isEmpty(value)) {
		setError('accountName_error', HMS_MANDATORY);
		return false;
	}
	if (!isValidOtherInformation50(value)) {
		setError('accountName_error', HMS_INVALID_VALUE);
		return false;
	}
} else {
	$('#accountName').val(EMPTY_STRING);
	$('#accountName').prop('readOnly', true);
	clearError('accountName_error');
}
setFocusLast('accountPasswordKey');
return true;
}

function accountPasswordKey_val() {
if ($('#authenticationRequired').is(':checked')) {
	$('#accountPasswordKey').prop('readOnly', false);
	var value = $('#accountPasswordKey').val();
	clearError('accountPasswordKey_error');
	if (isEmpty(value)) {
		setError('accountPasswordKey_error', HMS_MANDATORY);
		return false;
	}
	if (!isValidOtherInformation50(value)) {
		setError('accountPasswordKey_error', HMS_INVALID_KEY);
		return false;
	}
} else {
	$('#accountPasswordKey').val(EMPTY_STRING);
	$('#accountPasswordKey').prop('readOnly', true);
	clearError('accountPasswordKey_error');
}
setFocusLast('accountPassword');
return true;
}

function accountPassword_val() {
if ($('#authenticationRequired').is(':checked')) {
	$('#accountPassword').prop('readOnly', false);
	var value = $('#accountPassword').val();
	clearError('accountPassword_error');
	if (isEmpty(value)) {
		setError('accountPassword_error', HMS_MANDATORY);
		return false;
	}
	if (!isValidCredential(value)) {
		setError('accountPassword_error', HMS_INVALID_CREDENTIALS);
		return false;
	}
} else {
	$('#accountPassword').val(EMPTY_STRING);
	$('#accountPassword').prop('readOnly', true);
	clearError('accountPassword_error');
}
setFocusLast('mobileNumberKey');
return true;
}

function mobileNumberKey_val() {
if ($('#authenticationRequired').is(':checked')) {
	$('#mobileNumberKey').prop('readOnly', false);
	var value = $('#mobileNumberKey').val();
	clearError('mobileNumberKey_error');
	if (isEmpty(value)) {
		setError('mobileNumberKey_error', HMS_MANDATORY);
		return false;
	}
	if (!isValidOtherInformation50(value)) {
		setError('mobileNumberKey_error', HMS_INVALID_VALUE);
		return false;
	}
} else {
	$('#mobileNumberKey').val(EMPTY_STRING);
	$('#mobileNumberKey').prop('readOnly', true);
	clearError('mobileNumberKey_error');
}
setFocusLast('messageKey');
return true;
}

function messageKey_val() {
if ($('#authenticationRequired').is(':checked')) {
	$('#messageKey').prop('readOnly', false);
	var value = $('#messageKey').val();
	clearError('messageKey_error');
	if (isEmpty(value)) {
		setError('messageKey_error', HMS_MANDATORY);
		return false;
	}
	if (!isValidOtherInformation50(value)) {
		setError('messageKey_error', HMS_INVALID_VALUE);
		return false;
	}
} else {
	$('#messageKey').val(EMPTY_STRING);
	$('#messageKey').prop('readOnly', true);
	clearError('messageKey_error');
}
setFocusLast('smppAllowed');
return true;
}

function smppAllowed_val() {
if (sendAllowed == COLUMN_ENABLE) {
	$('#smppAllowed').prop('disabled', false);
	if ($('#smppAllowed').is(':checked')) {
		$('#senderNumber').prop('readOnly', false);
		$('#systemType').prop('readOnly', false);
		$('#addressRange').prop('readOnly', false);
		$('#transceiverEnabled').prop('disabled', false);
		$('#mainServer').prop('readOnly', false);
		$('#mainServerPort').prop('readOnly', false);
		$('#failOverServer').prop('readOnly', false);
		$('#failOverServerPort').prop('readOnly', false);
		$('#dispatchType').prop('disabled', false);
		$('#retryTimeout').prop('readOnly', false);
	} else {
		$('#senderNumber').val(EMPTY_STRING);
		$('#senderNumber').prop('readOnly', true);

		$('#systemType').val(EMPTY_STRING);
		$('#systemType').prop('readOnly', true);

		$('#addressRange').val(EMPTY_STRING);
		$('#addressRange').prop('readOnly', true);

		setCheckbox('transceiverEnabled', NO);
		$('#transceiverEnabled_error').html(EMPTY_STRING);
		$('#transceiverEnabled').prop('disabled', true);

		$('#mainServer').val(EMPTY_STRING);
		$('#mainServer').prop('readOnly', true);

		$('#mainServerPort').val(EMPTY_STRING);
		$('#mainServerPort').prop('readOnly', true);

		$('#failOverServer').val(EMPTY_STRING);
		$('#failOverServer').prop('readOnly', true);

		$('#failOverServerPort').val(EMPTY_STRING);
		$('#failOverServerPort').prop('readOnly', true);

		$('#dispatchType').val(EMPTY_STRING);
		$('#dispatchType').prop('disabled', true);

		$('#retryTimeout').val(EMPTY_STRING);
		$('#retryTimeout').prop('readOnly', true);
		clearError('senderNumber_error');
		clearError('systemType_error');
		clearError('addressRange_error');
		clearError('mainServer_error');
		clearError('mainServerPort_error');
		clearError('failOverServer_error');
		clearError('failOverServerPort_error');
		clearError('dispatchType_error');
		clearError('retryTimeout_error');
	}
} else {
	setCheckbox('smppAllowed', NO);
	$('#smppAllowed').prop('disabled', true);
}
if ($('#smppAllowed').is(':checked')) {
	setFocusLast('senderNumber');
} else {
	if ($('#dispatchType').val() == 'p') {
		$('#retryTimeout').prop('readOnly', false);
		setFocusLast('retryTimeout');
	} else {
		setFocusLast('remarks');
	}
}
return true;
}

function senderNumber_val() {
if ($('#smppAllowed').is(':checked')) {
	$('#senderNumber').prop('readOnly', false);
	var value = $('#senderNumber').val();
	clearError('senderNumber_error');
	if (isEmpty(value)) {
		setError('senderNumber_error', HMS_MANDATORY);
		return false;
	}
	if (!isNumeric(value)) {
		setError('senderNumber_error', HMS_INVALID_NUMBER);
		return false;
	}
	if (isZero(value)) {
		setError('senderNumber_error', HMS_ZERO_CHECK);
		return false;
	}
} else {
	$('#senderNumber').val(EMPTY_STRING);
	$('#senderNumber').prop('readOnly', true);
	clearError('senderNumber_error');
}
setFocusLast('systemType');
return true;
}

function systemType_val() {
if ($('#smppAllowed').is(':checked')) {
	$('#systemType').prop('readOnly', false);
	var value = $('#systemType').val();
	clearError('systemType_error');
	if (isEmpty(value)) {
		setError('systemType_error', HMS_MANDATORY);
		return false;
	}
	if (!isValidDescription(value)) {
		setError('systemType_error', HMS_INVALID_VALUE);
		return false;
	}
} else {
	$('#systemType').val(EMPTY_STRING);
	$('#systemType').prop('readOnly', true);
	clearError('systemType_error');
}
setFocusLast('addressRange');
return true;
}

function addressRange_val() {
if ($('#smppAllowed').is(':checked')) {
	$('#addressRange').prop('readOnly', false);
	var value = $('#addressRange').val();
	clearError('addressRange_error');
	if (isEmpty(value)) {
		setError('addressRange_error', HMS_MANDATORY);
		return false;
	}
	if (!isValidKey(value)) {
		setError('addressRange_error', HMS_INVALID_VALUE);
		return false;
	}
} else {
	$('#addressRange').val(EMPTY_STRING);
	$('#addressRange').prop('readOnly', true);
	clearError('addressRange_error');
}
setFocusLast('transceiverEnabled');
return true;
}

function transceiverEnabled_val() {
if ($('#smppAllowed').is(':checked')) {
	$('#transceiverEnabled').prop('disabled', false);
} else {
	setCheckbox('transceiverEnabled', NO);
	$('#transceiverEnabled').prop('disabled', true);
}
setFocusLast('mainServer');
return true;
}

function mainServer_val() {
if ($('#smppAllowed').is(':checked')) {
	$('#mainServer').prop('readOnly', false);
	var value = $('#mainServer').val();
	clearError('mainServer_error');
	if (isEmpty(value)) {
		setError('mainServer_error', HMS_MANDATORY);
		return false;
	}
	if (!isValidServerAddress(value)) {
		setError('mainServer_error', INVALID_SERVER_ADDRESS);
		return false;
	}
} else {
	$('#mainServer').val(EMPTY_STRING);
	$('#mainServer').prop('readOnly', true);
	clearError('mainServer_error');
}
setFocusLast('mainServerPort');

}

function mainServerPort_val() {
if ($('#smppAllowed').is(':checked')) {
	$('#mainServerPort').prop('readOnly', false);
	var value = $('#mainServerPort').val();
	clearError('mainServerPort_error');
	if (isEmpty(value)) {
		setError('mainServerPort_error', HMS_MANDATORY);
		return false;
	}
	if (!isNumeric(value)) {
		setError('mainServerPort_error', HMS_INVALID_NUMBER);
		return false;
	}
	if (!isValidPort(value)) {
		setError('mainServerPort_error', HMS_INVALID_PORT);
		return false;
	}
} else {
	$('#mainServerPort').val(EMPTY_STRING);
	$('#mainServerPort').prop('readOnly', true);
	clearError('mainServerPort_error');
}
setFocusLast('failOverServer');
}

function failOverServer_val() {
if ($('#smppAllowed').is(':checked')) {
	$('#failOverServer').prop('readOnly', false);
	var value = $('#failOverServer').val();
	clearError('failOverServer_error');
	if (isEmpty(value)) {
		setError('failOverServer_error', HMS_MANDATORY);
		return false;
	}
	if (!isValidServerAddress(value)) {
		setError('failOverServer_error', INVALID_SERVER_ADDRESS);
		return false;
	}
} else {
	$('#failOverServer').val(EMPTY_STRING);
	$('#failOverServer').prop('readOnly', true);
	clearError('failOverServer_error');
}
setFocusLast('failOverServerPort');

}

function failOverServerPort_val() {
if ($('#smppAllowed').is(':checked')) {
	$('#failOverServerPort').prop('readOnly', false);
	var value = $('#failOverServerPort').val();
	clearError('failOverServerPort_error');
	if (isEmpty(value)) {
		setError('failOverServerPort_error', HMS_MANDATORY);
		return false;
	}
	if (!isNumeric(value)) {
		setError('failOverServerPort_error', HMS_INVALID_NUMBER);
		return false;
	}
	if (!isValidPort(value)) {
		setError('failOverServerPort_error', HMS_INVALID_PORT);
		return false;
	}
} else {
	$('#failOverServerPort').val(EMPTY_STRING);
	$('#failOverServerPort').prop('readOnly', true);
	clearError('failOverServerPort_error');
}
setFocusLast('dispatchType');
}

function dispatchType_val() {
if ($('#smppAllowed').is(':checked')) {
	$('#dispatchType').prop('disabled', false);
	var value = $('#dispatchType').val();
	clearError('dispatchType_error');
	if (isEmpty(value)) {
		setError('dispatchType_error', HMS_MANDATORY);
		return false;
	}
} else {
	$('#dispatchType').val(EMPTY_STRING);
	$('#dispatchType').prop('disabled', true);
	clearError('dispatchType_error');
}
if ($('#dispatchType').val() == 'P') {
	$('#retryTimeout').prop('readOnly', false);
	setFocusLast('retryTimeout');
} else {
	$('#retryTimeout').val(EMPTY_STRING);
	$('#retryTimeout').prop('readOnly', true);
	clearError('retryTimeout_error');
	setFocusLast('remarks');
}
return true;
}

function retryTimeout_val() {
if ($('#dispatchType').val() == 'P') {
	$('#retryTimeout').prop('readOnly', false);
	var value = $('#retryTimeout').val();
	clearError('retryTimeout_error');
	if (isEmpty(value)) {
		setError('retryTimeout_error', HMS_MANDATORY);
		return false;
	}
	if (!isNumeric(value)) {
		setError('retryTimeout_error', HMS_INVALID_NUMBER);
		return false;
	}
	if (isZero(value)) {
		setError('retryTimeout_error', HMS_ZERO_CHECK);
		return false;
	}
	if (!isNumberLesserThanEqual(value, 9999)) {
		setError('retryTimeout_error', MAXVAL_9999);
		return false;
	}
} else {
	$('#retryTimeout').val(EMPTY_STRING);
	$('#retryTimeout').prop('readOnly', true);
	clearError('retryTimeout_error');
	setFocusLast('remarks');
}
setFocusLast('remarks');
return true;
}

function remarks_val() {
var value = $('#remarks').val();
clearError('remarks_error');
if (value != null && value.length > 0) {
	if (isWhitespace(value)) {
		setError('remarks_error', HMS_INVALID_REMARKS);
		return false;
	}
	if (!isValidRemarks(value)) {
		setError('remarks_error', HMS_INVALID_REMARKS);
		return false;
	}
}
if ($('#action').val() == MODIFY) {
	if (isEmpty(value)) {
		setError('remarks_error', HMS_MANDATORY);
		return false;
	}
}
setFocusOnSubmit();
return true;
}

function revalidate() {
$('#sendAllowed').val(sendAllowed);
}