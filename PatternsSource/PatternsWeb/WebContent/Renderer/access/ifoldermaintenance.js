var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'IFOLDERMAINTENANCE';
function init() {
	refreshTBAGrid();
	refreshQueryGrid();
}
function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'IFOLDERMAINTENANCE_MAIN');
}
function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}
function clearFields() {
	
	$('#effectiveDate').val(EMPTY_STRING);
	$('#effectiveDate_error').html(EMPTY_STRING);
	$('#folderReportsGeneration').val(EMPTY_STRING);
	$('#folderReportsGeneration_error').html(EMPTY_STRING);
	$('#folderPortalUpload').val(EMPTY_STRING);
	$('#folderPortalUpload_error').html(EMPTY_STRING);
	$('#folderTempUpload').val(EMPTY_STRING);
	$('#folderTempUpload_error').html(EMPTY_STRING);
	$('#folderCBSFileUpload').val(EMPTY_STRING);
	$('#folderCBSFileUpload_error').html(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
}
function add() {
	
	$('#effectiveDate').focus();
}
function modify() {
	
	$('#effectiveDate').focus();

}
function view(source, primaryKey) {
	hideParent('access/qfoldermaintenance', source, primaryKey);
	resetLoading();
}
function loadData() {
	$('#effectiveDate').val(validator.getValue('EFFT_DATE'));
	$('#folderReportsGeneration').val(validator.getValue('REPORT_GENERATION_PATH'));
	$('#folderPortalUpload').val(validator.getValue('PORTAL_FILE_UPLOAD_PATH'));
	$('#folderTempUpload').val(validator.getValue('TEMP_FILE_UPLOAD_PATH'));
	$('#folderCBSFileUpload').val(validator.getValue('CBS_FILE_TRANSFER_PATH'));
	$('#remarks').val(validator.getValue('REMARKS'));
	resetLoading();
}
function validate(id) {
	switch (id) {
	case 'effectiveDate':
		effectiveDate_val();
		break;
	case 'folderReportsGeneration':
		folderReportsGeneration_val();
		break;
	case 'folderPortalUpload':
		folderPortalUpload_val();
		break;
	case 'folderCBSFileUpload':
		folderCBSFileUpload_val();
		break;
	case 'folderTempUpload':
		folderTempUpload_val();
		break;
	case 'remarks':
		remarks_val();
		break;
	}
}
function effectiveDate_val() {
	var value = $('#effectiveDate').val();
	clearError('effectiveDate_error');
	if (isEmpty(value)) {
		setError('effectiveDate_error', MANDATORY);
		return false;
	}
	if (!isDate(value)) {
		setError('effectiveDate_error', INVALID_DATE);
		return false;
	}
	if (!isDateGreaterEqual(value, getCBD())) {
		setError('effectiveDate_error', DATE_GCBD);
		return false;
	}
	return true;
}
function folderPortalUpload_val() {
	var value = $('#folderPortalUpload').val();
	clearError('folderPortalUpload_error');
	if (isEmpty(value)) {
		setError('folderPortalUpload_error', MANDATORY);
		return false;
	}
	return true;
}
function folderReportsGeneration_val() {
	var value = $('#folderReportsGeneration').val();
	clearError('folderReportsGeneration_error');
	if (isEmpty(value)) {
		setError('folderReportsGeneration_error', MANDATORY);
		return false;
	}
	return true;
}
function folderTempUpload_val() {
	var value = $('#folderTempUpload').val();
	clearError('folderTempUpload_error');
	if (isEmpty(value)) {
		setError('folderTempUpload_error', MANDATORY);
		return false;
	}
	return true;
}
function folderCBSFileUpload_val() {
	var value = $('#folderCBSFileUpload').val();
	clearError('folderCBSFileUpload_error');
	if (isEmpty(value)) {
		setError('folderCBSFileUpload_error', MANDATORY);
		return false;
	}
	return true;
}

function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (isEmpty(value)) {
		setError('remarks_error', MANDATORY);
		return false;
	}
	if (!isValidRemarks(value)) {
		setError('remarks_error', INVALID_REMARKS);
		return false;
	}
	return true;
}

function revalidate() {
	if (!effectiveDate_val()) {
		errors++;
	}
	if (!folderReportsGeneration_val()) {
		errors++;
	}
	if (!folderTempUpload_val()) {
		errors++;
	}
	if (!folderPortalUpload_val()) {
		errors++;
	}
	if (!folderCBSFileUpload_val()) {
		errors++;
	}
	if (!remarks_val()) {
		errors++;
	}
}