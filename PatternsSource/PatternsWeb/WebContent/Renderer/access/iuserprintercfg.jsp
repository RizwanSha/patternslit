<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.access.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/access/iuserprintercfg.js" />
	</web:dependencies>
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="iuserprintercfg.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/access/iuserprintercfg" id="iuserprintercfg" method="POST">
				<web:dividerBlock>
					<type:optionBox add="true" modify="true" view="true" />
				</web:dividerBlock>
				<web:viewContent id="po_view1" styleClass="hidden">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="form.userid" var="common" mandatory="true" />
										</web:column>
										<web:column>
											<type:user property="userId" id="userId" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="form.effectivedate" var="common" mandatory="true" />
										</web:column>
										<web:column>
											<type:date property="effectiveDate" id="effectiveDate" lookup="true" />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
							<web:section>
							<web:viewTitle var="program" key="iuserprintercfg.gridSection" />
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowEven>
										<web:column>
											<web:legend key="iuserprintercfg.printtypeid" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:printType property="printTypeId" id="printTypeId" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="iuserprintercfg.printerid" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:printerId property="printerId" id="printerId" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="iuserprintercfg.defaultprinter" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:checkbox property="defaultPrinter" id="defaultPrinter" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:rowOdd>
										<web:column>
											<web:gridToolbar gridName="iuserprintercfg_innerGrid" addFunction="iuserprintercfg_addGrid" deleteNotReq="false" editFunction="iuserprintercfg_editGrid" insertAtFirsstNotReq="false" cancelFunction="iuserprintercfg_cancelGrid" continuousEdit="true" insertAtFirsstNotReq="false" />
											<web:element property="xmlUserPrinterConfigGrid" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:viewContent id="po_view4">
												<web:grid height="240px" width="860px" id="iuserprintercfg_innerGrid" src="access/iuserprintercfg_innerGrid.xml">
												</web:grid>
											</web:viewContent>
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
							<web:viewTitle var="program" key="iuserprintercfg.pgmgridSection" />
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="iuserprintercfg.pgmId" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:optionID property="pgmId" id="pgmId" helpRequired="true"/>
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="iuserprintercfg.progPrinterId" var="program" mandatory="true" />
										</web:column>
										<web:column>
											<type:printerId property="progPrinterId" id="progPrinterId" />
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:rowOdd>
										<web:column>
											<web:gridToolbar gridName="iuserprintercfgpgm_innerGrid" addFunction="iuserprintercfgpgm_addGrid" deleteNotReq="false" editFunction="iuserprintercfgpgm_editGrid" insertAtFirsstNotReq="false" cancelFunction="iuserprintercfgpgm_cancelGrid" continuousEdit="true" insertAtFirsstNotReq="false" />
											<web:element property="xmlUserProgramPrinterConfigGrid" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:viewContent id="po_view5">
												<web:grid height="240px" width="710px" id="iuserprintercfgpgm_innerGrid" src="access/iuserprintercfgpgm_innerGrid.xml">
												</web:grid>
											</web:viewContent>
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column span="2">
											<web:submitReset />
										</web:column>
									</web:rowOdd>
								</web:table>
							</web:section>
						</web:sectionBlock>
					</web:dividerBlock>
				</web:viewContent>
				<web:element property="command" />
				<web:element property="action" />
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
	<web:queryGrid />
	<web:TBAGrid />
	<web:contextSearch />
</web:fragment>