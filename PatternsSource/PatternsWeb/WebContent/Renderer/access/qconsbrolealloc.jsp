<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.access.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/access/qconsbrolealloc.js" />
	</web:dependencies>
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="qconsbrolealloc.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:form action="/Renderer/access/qconsbrolealloc" id="qconsbrolealloc" method="POST">
				<web:viewContent id="po_view1">
					<web:dividerBlock>
						<web:sectionBlock width="width-100p" align="left">
							<web:section>
								<web:table>
									<web:columnGroup>
										<web:columnStyle width="180px" />
										<web:columnStyle />
									</web:columnGroup>
									<web:rowOdd>
										<web:column>
											<web:legend key="form.userid" var="common" mandatory="true" />
										</web:column>
										<web:column>
											<type:userHelp property="userID" id="userID" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column span="2">
											<type:button id="fetch" key="form.fetch" var="common" onclick="revalidate();" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="qconsbrolealloc.primaryrolecode" var="program" />
										</web:column>
										<web:column>
											<type:inputCodeDisplay property="primaryRoleCode" id="primaryRoleCode" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column span="2">
											<web:legend key="qconsbrolealloc.actingrolecode" var="program" />
										</web:column>
									</web:rowEven>
									<web:rowEven>
										<web:column span="2">
											<web:viewContent id="po_view5" styleClass="hidden">
												<web:inlineQueryGrid height="250" width="670" id="inlineGridSucc" />
											</web:viewContent>
										</web:column>
									</web:rowEven>
								</web:table>
							</web:section>
						</web:sectionBlock>
					</web:dividerBlock>
				</web:viewContent>
				<web:element property="command" />
				<web:element property="action" />
			</web:form>
		</web:viewSubPart>
	</web:viewPart>
	<web:contextSearch />
</web:fragment>