var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'QTFAINFO';

function init() {
	validator.reset();
	validator.setValue("INV_NUM_PK", $('#hidPrimaryKey').val());
	validator.setClass("patterns.config.web.forms.access.qtfainfobean");
	validator.setMethod("getAuditPKIDetails");
	validator.sendAndReceive();
	if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		$('#userID').val(validator.getValue('USER_ID'));
		$('#userName').val(validator.getValue('USER_NAME'));
		$('#auctionIP').val(validator.getValue('ACTION_IP'));
		$('#actualValue').val(validator.getValue('ACTUAL_VALUE'));
		$('#encodedValue').val(validator.getValue('ENCODED_VALUE'));
		$('#hidInventoryNumber').val(validator.getValue('CERT_INV_NUM'));
	}
}

function clearFields() {
	$('#hidPrimaryKey').val(EMPTY_STRING);
	$('#hidInventoryNumber').val(EMPTY_STRING);
	$('#userID').val(EMPTY_STRING);
	$('#userName').val(EMPTY_STRING);
	$('#auctionIP').val(EMPTY_STRING);
	$('#actualValue').val(EMPTY_STRING);
	$('#encodedValue').val(EMPTY_STRING);
}

function viewCertificateInfo() {
	var pk = getEntityCode() + '|' + $('#hidInventoryNumber').val();
	showWindow('PREVIEW_VIEW', getBasePath() + 'Renderer/common/qcertificateinfo.jsp', CERTIFICATE_VIEW_TITLE, PK + '=' + pk, true, false, false, 920, 450);
	resetLoading();
}