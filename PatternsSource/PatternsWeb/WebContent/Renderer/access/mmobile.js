var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'MMOBILE';
var _oldRoleType = EMPTY_STRING;
function init() {
	refreshTBAGrid();
	refreshQueryGrid();
	if (_redisplay) {
		if ($('#action').val() == MODIFY) {
			$('#mobileNumber').attr("disabled", "disabled"); 
			$('#mobileNumber').val("");
		} else {
		}
	}
}
function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'MMOBILE_CMN', queryGrid);
}
function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}
function clearFields() {
	
	$('#mobileNumber').val(EMPTY_STRING);
	$('#mobileNumber_error').html(EMPTY_STRING);
	$('#userid').val(EMPTY_STRING);
	$('#userid_error').html(EMPTY_STRING);
}
function add() {
	
	$('#mobileNumber').focus();
}
function modify() {
	
	$('#mobileNumber').focus();
}
function loadData() {
	$('#mobileNumber').val(validator.getValue('MOBILENUMBER'));
	$('#userid').val(validator.getValue('USERID'));
	resetLoading();
}
function view(source, primaryKey) {
	hideParent('access/qmobile', source, primaryKey);
	resetLoading();
}
function validate(id) {
	switch (id) {
	case 'mobileNumber':
		mobnum_valid();
		break;
	case 'userid':
		userid_valid();
		break;
	}
}
function mobnum_valid() {
	var value = $('#mobileNumber').val();
	clearError('mobileNumber_error');
	if (isEmpty(value)) {
		setError('mobileNumber_error', MANDATORY);
		return false;
	}
	return true;
}
function userid_valid() {
	var value = $('#userid').val();
	clearError('userid_error');
	return true;
}

function revalidate() {
	if (!mobnum_valid()) {
		errors++;
	}
	if (!userid_valid()) {
		errors++;
	}
}