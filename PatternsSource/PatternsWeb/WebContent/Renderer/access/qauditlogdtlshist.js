var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'QAUDITLOGDTLSHIST';
var mpgmTable = EMPTY_STRING;
var programModule, programName, pk;
function init() {
	$('#actionType').prop('selectedIndex', 3);
	$('#viewType').prop('selectedIndex', 1);
	pk = _primaryKey.split(',');
	$('#optionID').val(pk[0]);
	$('#optionID_desc').html(pk[1]);
	$('#primaryKey').val(pk[2]);
	$('#po_view7').addClass('hidden');
	$('#po_view4').addClass('hidden');
	validator.clearMap();
	validator.setMtm(false);
	validator.setValue("PK", $('#hidPrimaryKey').val());
	validator.setClass("patterns.config.web.forms.access.qauditlogdtlshistbean");
	validator.setMethod("getTableDetails");
	validator.sendAndReceive();
	if (validator.getValue(RESULT) == DATA_AVAILABLE) {
		programModule = validator.getValue('MODULE_NAME').toLowerCase();
		var tableSl = validator.getValue('TABLE_SL').split(PK_SEPERATOR);
		var tableName = validator.getValue('TABLE_NAME').split(PK_SEPERATOR);
		$('#fromTable').empty();
		for (var i = 0; i < tableName.length; i++) {
			if (tableSl[i] != EMPTY_STRING)
				$('#fromTable').append(new Option(tableName[i], tableSl[i]));
		}
		onSubmit();
	}
	clearFields();
	setFocusLast('actionType');
}

function clearFields() {
	$('#actionType').prop('selectedIndex', 3);
	$('#viewType').prop('selectedIndex', 1);
	$('#buttonId').removeClass('row_even');
	$('#buttonId').addClass('row_odd');
	$('#po_view7').addClass('hidden');
	$('#po_view4').addClass('hidden');
	$('#totalrows').html(EMPTY_STRING);
	$('#auditTable_div').empty();
	$('#auditTableDetails_div').empty();
	hideMessage(getProgramID());
	setFocusLast('actionType');
}

function validate(id) {
	switch (id) {
	case 'actionType':
		actionType_val(true);
		break;
	case 'viewType':
		viewType_val(true);
		break;
	case 'fromTable':
		fromTable_val(true);
		break;
	case 'detailTableSerial':
		detailTableSerial_val(true);
		break;
	case 'submit':
		onSubmit();
		break;
	}
}

function change(id) {
	$('#totalrows').html(EMPTY_STRING);
	switch (id) {
	// case 'actionType':
	// actionType_val(true);
	// break;
	// case 'viewType':
	// viewType_val(true);
	// break;
	case 'fromTable':
		fromTable_val(true);
		break;
	case 'detailTableSerial':
		detailTableSerial_val(true);
		break;
	}
}

function backtrack(id) {
	switch (id) {
	case 'actionType':
		setFocusLast('actionType');
		break;
	case 'viewType':
		setFocusLast('actionType');
		break;
	case 'fromTable':
		setFocusLast('viewType');
		break;
	case 'detailTableSerial':
		setFocusLast('fromTable');
		break;
	case 'submit':
		if ($('#po_view7').addClass('hidden'))
			setFocusLast('fromTable');
		else if ($('#po_view7').removeClass('hidden'))
			setFocusLast('actionType');
		break;
	}
}

function actionType_val(valmode) {
	$('#auditTable_div').empty();
	$('#auditTableDetails_div').empty();
	var value = $('#actionType').val();
	clearError('actionType_error');
	if (isEmpty(value)) {
		setError('actionType_error', MANDATORY);
		return false;
	}
	if (valmode)
		setFocusLast('viewType');
	return true;
}

function viewType_val(valmode) {
	$('#auditTable_div').empty();
	$('#auditTableDetails_div').empty();
	var value = $('#viewType').val();
	clearError('viewType_error');
	if (isEmpty(value)) {
		setError('viewType_error', MANDATORY);
		return false;
	}
	if (valmode)
		setFocusLast('fromTable');
	return true;
}

function fromTable_val(valmode) {
	$('#auditTable_div').empty();
	$('#auditTableDetails_div').empty();
	var value = $('#fromTable').val();
	clearError('fromTable_error');
	if (isEmpty(value)) {
		setError('fromTable_error', MANDATORY);
		return false;
	} else {
		$('#detailTableSerial').empty();
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue("PK", $('#hidPrimaryKey').val());
		validator.setValue("AUCTION_TYPE", $('#actionType').val());
		validator.setValue("FROM_TABLE", $('#fromTable').val());
		validator.setClass("patterns.config.web.forms.access.qauditlogdtlshistbean");
		validator.setMethod("getDtlTableDetails");
		validator.sendAndReceive();
		if (validator.getValue(RESULT) == DATA_AVAILABLE) {
			mpgmTable = validator.getValue('MPGM_TABLE_NAME');
			if (mpgmTable != value) {
				var tableSl = validator.getValue('DTL_TABLE_SL').split(PK_SEPERATOR);
				var tableName = validator.getValue('DTL_TABLE_NAME').split(PK_SEPERATOR);
				$('#detailTableSerial').empty();
				for (var i = 0; i < tableName.length; i++) {
					if (tableSl[i] != EMPTY_STRING)
						$('#detailTableSerial').append(new Option(tableName[i], tableSl[i]));
				}
				$('#po_view7').removeClass('hidden');
				$('#buttonId').removeClass('row_odd');
				$('#buttonId').addClass('row_even');
				if (valmode)
					setFocusLast('detailTableSerial');
			} else {
				$('#buttonId').removeClass('row_even');
				$('#buttonId').addClass('row_odd');
				$('#po_view7').addClass('hidden');
				if (valmode)
					setFocusLast('submit');
			}
		}
	}
	return true;
}

function detailTableSerial_val(valmode) {
	$('#auditTable_div').empty();
	$('#auditTableDetails_div').empty();
	var value = $('#detailTableSerial').val();
	clearError('detailTableSerial_error');
	if (mpgmTable != $('#fromTable').val()) {
		if (isEmpty(value)) {
			setError('detailTableSerial_error', MANDATORY);
			return false;
		}
	}
	if (valmode)
		setFocusLast('submit');
	return true;
}

function onSubmit() {
	$('#totalrows').html(EMPTY_STRING);
	if (revalidateValue()) {
		$('#po_view4').removeClass('hidden');
		$('#po_view8').removeClass('hidden');
		clearError('fromTable_error');
		$('#auditTable_div').empty();
		$('#auditTableDetails_div').empty();
		showMessage(getProgramID(), Message.PROGRESS);
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue("PK", $('#hidPrimaryKey').val());
		if ($('#actionType').val() == 'X')
			validator.setValue("AUCTION_TYPE", EMPTY_STRING);
		else
			validator.setValue("AUCTION_TYPE", $('#actionType').val());
		if (isEmpty($('#detailTableSerial').val()) || $('#detailTableSerial').val() == null)
			validator.setValue("VIEW_TYPE", $('#viewType').val());
		else
			validator.setValue("VIEW_TYPE", "A");
		if (!isEmpty($('#fromTable').val())) {
			validator.setValue("FROM_TABLE", $('#fromTable').val());
		} else {
			setError('fromTable_error', HMS_MANDATORY);
			setFocusLast('fromTable');
			return false;
		}
		if (isEmpty($('#detailTableSerial').val()) || $('#detailTableSerial').val() == null)
			validator.setValue("DTL_SERIAL", EMPTY_STRING);
		else
			validator.setValue("DTL_SERIAL", $('#detailTableSerial').val());
		validator.setValue(ACTION, USAGE);
		validator.setClass("patterns.config.web.forms.access.qauditlogdtlshistbean");
		validator.setMethod('loadAuditLogDetails');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) == EMPTY_STRING) {
			$('#auditTable_div').empty();
			$('#auditTableDetails_div').empty();
			if (isEmpty($('#detailTableSerial').val()) || $('#detailTableSerial').val() == null)
				$('#po_view8').addClass('hidden');
			hideMessage(getProgramID());
			if (validator.getValue('GRID') != EMPTY_STRING) {
				var grid = new dhtmlXGridObject('auditTable_div');
				grid.setImagePath(WidgetConstants.GRID_IMAGE_PATH);
				grid.init();
				grid.loadXMLString(validator.getValue('GRID'));
				grid.setUserData("", "grid_id", EMPTY_STRING);
				grid.setSortImgState(false);
				grid.attachEvent("onCellUnMarked", function(rid, ind) {
					grid.setUserData("", "grid_edit_id", EMPTY_STRING);
					grid.setRowColor(rid, 'WHITE');
				});
			}
			if (validator.getValue('GRID_DTL') != EMPTY_STRING) {
				var grid1 = new dhtmlXGridObject('auditTableDetails_div');
				grid1.setImagePath(WidgetConstants.GRID_IMAGE_PATH);
				grid1.init();
				grid1.loadXMLString(validator.getValue('GRID_DTL'));
				grid1.setUserData("", "grid_id", EMPTY_STRING);
				grid1.setSortImgState(false);
				grid1.attachEvent("onCellUnMarked", function(rid, ind) {
					grid1.setUserData("", "grid_edit_id", EMPTY_STRING);
					grid1.setRowColor(rid, 'WHITE');
				});
			}
			if (!isEmpty($('#detailTableSerial').val())) {
				if (parseInt(validator.getValue('ROWS_DIFF')) == 0) {
					$('#totalrows').html('No Record Changed in Detail Table ');
				}
				if (parseInt(validator.getValue('ROWS_DIFF')) < 0)
					$('#totalrows').html('No of Records Deleted in Detail Table : ' + ' ' + parseInt(validator.getValue('ROWS_DIFF') * -1));
				else
					$('#totalrows').html('No of Records Added in Detail Table : ' + ' ' + validator.getValue('ROWS_DIFF'));
			}
		} else if (validator.getValue(ERROR) != null && validator.getValue(ERROR) != EMPTY_STRING) {
			showMessage(getProgramID(), Message.ERROR, validator.getValue(ERROR));
			$('#auditTable_div').empty();
			$('#auditTableDetails_div').empty();
			return false;
		}
	}
}

function revalidateValue() {
	hideMessage(getProgramID());
	var errors = 0;
	// if (!actionType_val(false)) {
	// errors++;
	// }
	// if (!fromTable_val(true)) {
	// errors++;
	// }
	// if (!detailTableSerial_val(false)) {
	// errors++;
	// }
	if (errors > 0) {
		return false;
	} else {
		return true;
	}
}

function closePopup(value) {
	parent.dhxWindows.window('QAUDITLOGDTLSHIST').close();
	return true;
}

function viewProgramdtl() {
	programName = pk[0].toLowerCase().trim();
	var pkey = pk[2];
	var params = PK + '=' + pkey + '&' + SOURCE + '=' + MAIN;
	var programNameDtl = programName.replace(programName.charAt(0), 'q');
	var qpgmName = programNameDtl + '.jsp';
	showWindow(programName, getBasePath() + 'Renderer/' + programModule + '/' + qpgmName, programName.toUpperCase().trim(), params, true, false, false, 1000, 550);
	resetLoading();
}
