var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'QCONSBROLEALLOC';
var inlineGridSuccessful = null;
function init() {
}

function loadActingRolesGrid() {
	$('#po_view5').removeClass('hidden');
	var _args = $('#userID').val();
	inlineGridSuccessful = loadInlineQueryGrid(CURRENT_PROGRAM_ID, 'QCONSBROLEALLOC_MAIN', 'inlineGridSucc', _args);
}

function clearFields() {
	$('#po_view5').addClass('hidden');
	$('#userID').val(EMPTY_STRING);
	$('#primaryRoleCode').val(EMPTY_STRING);
	$('#userID_desc').html(EMPTY_STRING);
	$('#userID_error').html(EMPTY_STRING);
	if (inlineGridSuccessful)
		inlineGridSuccessful.clearAll(true);
}

function doHelp(id) {
	switch (id) {
	case 'userID':
		help(CURRENT_PROGRAM_ID, 'USERS', $('#userID').val(), EMPTY_STRING, $('#userID'));
		break;
	}
}

function validate(id) {
	switch (id) {
	case 'userID':
		userID_val();
		break;
	}
}

function userID_val() {
	var value = $('#userID').val();
	clearError('userID_error');

	if (isEmpty(value)) {
		setError('userID_error', MANDATORY);
		return false;
	}
	if (!isValidCode(value)) {
		setError('userID_error', INVALID_USERID);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('USER_ID', $('#userID').val());
		validator.setClass('patterns.config.web.forms.access.qconsbroleallocbean');
		validator.setMethod('validateUserID');
		validator.sendAndReceive();
		if (validator.getValue(RESULT) == DATA_AVAILABLE) {
			$('#primaryRoleCode').val(validator.getValue('ROLE_CODE'));
		} else {
			setError('userID_error', validator.getValue(ERROR));
			return false;
		}
	}
	return true;
}

function revalidate() {
	if (!userID_val()) {
		errors++;
	}
	if (errors == 0) {
		loadActingRolesGrid();
	} else {
		errors = 0;
		if (inlineGridSuccessful)
			inlineGridSuccessful.clearAll(true);
	}
}