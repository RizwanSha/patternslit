var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'EROLEBRNLIST';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}
function clearFields() {
	$('#roleCode').val(EMPTY_STRING);
	$('#brnListCode').val(EMPTY_STRING);
	$('#effectiveDate').val(EMPTY_STRING);
	$('#enabled').prop('checked',false);
	$('#remarks').val(EMPTY_STRING);
}
function loadData() {
	$('#roleCode').val(validator.getValue('ROLE_CODE'));
	$('#roleCode_desc').html(validator.getValue('BANKROLES_DESCRIPTION'));
	$('#effectiveDate').val(validator.getValue('EFFT_DATE'));
	$('#enabled').prop('checked',decodeSTB(validator.getValue('ENABLED')));
	$('#brnListCode').val(validator.getValue('BRANCH_LIST_CODE'));
	$('#brnListCode_desc').html(validator.getValue('BRANCHLIST_DESCRIPTION'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadAuditFields(validator);
}