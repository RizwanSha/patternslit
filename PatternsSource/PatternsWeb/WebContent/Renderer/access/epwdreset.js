var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'EPWDRESET';
var pwdDeliveryChoice = EMPTY_STRING;
var passwordType = EMPTY_STRING;
function init() {
	refreshTBAGrid();
	refreshQueryGrid();
	loadViewQuery();
	show('po_view2', $('#po_view2_pic'));
	// $('#userID').attr('readonly', true);
	if (_redisplay) {
		$('#dateOfReset').val(getCBD());
	}
	clearFields();
	$('#action').val(MODIFY);
	$('#actionDescription').val(MODIFY_DESC);
	$('#spanMode').html(MODIFY_DESC);
	modify();
}

function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'EPWDRESET_MAIN', getUserID());
}

function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}

function doHelp(id) {
	switch (id) {

	case 'userID':
		help('EACTROLEALLOC', 'USERS', $('#userID').val(),  getUserID(), $('#userID'));
		break;

	}
}

function clearFields() {
	$('#userID').val(EMPTY_STRING);
	$('#userID_error').html(EMPTY_STRING);
	$('#userID_desc').html(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
	$('#createPassword').val(EMPTY_STRING);
	$('#createPassword_error').html(EMPTY_STRING);
	$('#confirmPassword').val(EMPTY_STRING);
	$('#confirmPassword_error').html(EMPTY_STRING);
	$('#createPassword').attr('readonly', false);
	$('#confirmPassword').attr('readonly', false);
}

function modify() {
	$('#userID').focus();
	$('#dateOfReset').val(getCBD());
	setPasswordCriteria();
	// hideMessage('epwdreset');
}

function loadData() {
	$('#userID').val(validator.getValue('USER_ID'));
	$('#userID_desc').html(validator.getValue("USERS_USER_NAME"));
	$('#dateOfReset').val(getCBD());
	$('#remarks').val(validator.getValue('REMARKS'));
	setPasswordCriteria();
	resetLoading();
}

function setPasswordCriteria() {
	if ($('#pwdDelivery').val() == 4 && $('#pwdDeliveryType').val() == 2) {
		$('#createPassword').val(EMPTY_STRING);
		$('#createPassword_error').html(EMPTY_STRING);
		$('#createPassword').attr('readonly', false);
		$('#spanPasswordPolicy').html($('#passwordPolicy').val());
		$('#confirmPassword').val(EMPTY_STRING);
		$('#confirmPassword_error').html(EMPTY_STRING);
		$('#confirmPassword').attr('readonly', false);
	} else {
		$('#spanPasswordPolicy').html(HMS_PASSWORD_SYS_GEN);
		$('#createPassword').val(EMPTY_STRING);
		$('#createPassword_desc').html(EMPTY_STRING);
		$('#createPassword_error').html(EMPTY_STRING);
		$('#createPassword').attr('readonly', true);

		$('#confirmPassword').val(EMPTY_STRING);
		$('#confirmPassword_error').html(EMPTY_STRING);
		$('#confirmPassword').attr('readonly', true);
	}

}

function view(source, primaryKey) {
	hideParent('access/qpwdreset', source, primaryKey);
	resetLoading();
}

function validate(id) {
	valMode = true;
	switch (id) {
	case 'userID':
		userID_val(valMode);
		break;
	case 'createPassword':
		createPassword_val(valMode);
		break;
	case 'confirmPassword':
		confirmPassword_val(valMode);
		break;
	case 'remarks':
		remarks_val(valMode);
		break;
	}
}

function backtrack(id) {
	switch (id) {
	case 'userID':
		setFocusLast('userID');
		break;
	case 'createPassword':
		setFocusLast('userID');
		break;
	case 'confirmPassword':
		setFocusLast('createPassword');
		break;
	case 'remarks':
		if ($('#createPassword').prop('readonly')) {
			setFocusLast('userID');
		} else {
			setFocusLast('confirmPassword');
		}

		break;

	}
}
function userID_val(valMode) {
	var minUserLength = $('#hidminUserLength').val();
	var value = $('#userID').val();
	clearError('userID_error');
	if (isEmpty(value)) {
		setError('userID_error', HMS_MANDATORY);
		setFocusLast('userID');
		return false;
	}
	if (value == getUserID()) {
		setError('userID_error', CANNOT_ALLOCATE_TO_SELF);
		return false;
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('USER_ID', $('#userID').val());
		validator.setValue(ACTION, USAGE);
		validator.setClass('patterns.config.web.forms.access.muserbean');
		validator.setMethod('validateUserId');
		validator.sendAndReceive();
		if (validator.getValue(ERROR) != EMPTY_STRING) {
			setError('userID_error', validator.getValue(ERROR));
			return false;
		} else {
			$('#userID_desc').html(validator.getValue('USER_NAME'));
			$('#dateOfReset').val(getCBD());
			if (valMode) {
				if ($('#pwdDelivery').val() == 4
						&& $('#pwdDeliveryType').val() == 2) {
					setFocusLast('createPassword');
				} else {
					setFocusLast('remarks');
				}
			}
		}

	}
	return true;

}

function createPassword_val(valMode) {
	var value = $('#createPassword').val();
	clearError('createPassword_error');
	if ($('#pwdDelivery').val() == 4 && $('#pwdDeliveryType').val() == 2) {
		if (isEmpty(value)) {
			setError('createPassword_error', MANDATORY);
			setFocusLast('createPassword');
			return false;
		}
		if (!checkPasswordCriteria(value, $('#minLength').val(), $('#minAlpha')
				.val(), $('#minNumeric').val(), $('#minSpecial').val())) {
			setError('createPassword_error', PASSWORD_POLICY);
			setFocusLast('createPassword');
			return false;
		}
	}
	if (valMode) {
		setFocusLast('confirmPassword');
	}
	return true;
}

function confirmPassword_val(valMode) {
	var value = $('#confirmPassword').val();
	clearError('confirmPassword_error');
	if ($('#pwdDelivery').val() == 4 && $('#pwdDeliveryType').val() == 2) {
		if (isEmpty(value)) {
			setError('confirmPassword_error', MANDATORY);
			setFocusLast('confirmPassword');
			return false;
		}
		if ($('#createPassword').val() != $('#confirmPassword').val()) {
			setError('confirmPassword_error', PWD_SAME);
			setFocusLast('confirmPassword');
			return false;
		}

	}
	if (valMode) {
		setFocusLast('remarks');
	}

	return true;
}

function remarks_val(valMode) {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (isEmpty(value)) {
		setError('remarks_error', MANDATORY);
		setFocusLast('remarks');
		return false;
	}
	if (value != null && value.length > 0) {
		if (isWhitespace(value)) {
			setError('remarks_error', HMS_INVALID_REMARKS);
			setFocusLast('remarks');
			return false;
		}
		if (!isValidRemarks(value)) {
			setError('remarks_error', HMS_INVALID_REMARKS);
			setFocusLast('remarks');
			return false;
		}
	}
	if (valMode) {
		setFocusOnSubmit();
	}
	return true;
}

function revalidate() {

	if (!createPassword_val(false)) {
		errors++;
	}
	if (!confirmPassword_val(false)) {
		errors++;
	}
	
	if (errors == 0) {
		if ($('#pwdDelivery').val() == 4 && $('#pwdDeliveryType').val() == 2) {
			var userID = $('#userID').val();
			var hashedNewPassword = sha256_digest($('#createPassword').val());
			var hashedNewUserID = sha256_digest(hashedNewPassword + userID);
			$('#loginNewPasswordHashed').val(hashedNewUserID);
		}
		return true;
	} else {

		return false;
	}
}
