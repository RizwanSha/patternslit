
var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'EIPUSER';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}
function clearFields() {
	$('#userID').val(EMPTY_STRING);
	$('#effectiveDate').val(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
	$('#enabled').prop('checked',false);
}
function loadData() {
	$('#userID').val(validator.getValue('USER_ID'));
	$('#userID_desc').html(validator.getValue('BANKUSERS_USER_NAME'));
	$('#effectiveDate').val(validator.getValue('EFFT_DATE'));
	$('#remarks').val(validator.getValue('REMARKS'));
	$('#enabled').prop('checked',decodeSTB(validator.getValue('ENABLED')));
	if ($('#enabled').is(':checked')) {
		loadGrid();
	}
	loadAuditFields(validator);
}

function loadGrid() {
	SOURCE_VALUE = _tableType;
	PK_VALUE = _primaryKey;
	loadGridQuery(CURRENT_PROGRAM_ID, 'EIPUSER_GRID', innerGrid);
}