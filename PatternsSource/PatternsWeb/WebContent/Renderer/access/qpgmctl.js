var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'EPGMCTL';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}
function clearFields() {
	$('#optionID').val(EMPTY_STRING);
	$('#authReqd').prop('checked',false);
	$('#doubleAuthReqd').prop('checked',false);
}
function loadData() {
	$('#optionID').val(validator.getValue('MPGM_ID'));
	$('#optionID_desc').html(validator.getValue('F1_MPGM_DESCN'));
	setCheckbox('authReqd', validator.getValue('MPGM_AUTH_REQD'));
	setCheckbox('doubleAuthReqd', validator.getValue('MPGM_DBL_AUTH_REQD'));
	loadAuditFields(validator);
}