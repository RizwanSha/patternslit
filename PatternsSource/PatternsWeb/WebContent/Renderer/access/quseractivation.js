var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'EUSERACTIVATION';
function init() {
	clearFields();
	if (_tableType == MAIN) {
		loadMainValues(_primaryKey);
	} else if (_tableType == TBA) {
		loadTBAValues(_primaryKey);
	}
}
function clearFields() {
	$('#userID').val(EMPTY_STRING);
	$('#intimationMode').val(EMPTY_STRING);
	$('#intimationReferenceNo').val(EMPTY_STRING);
	$('#intimationDate').val(EMPTY_STRING);
	$('#remarks').val(EMPTY_STRING);
}
function loadData() {
	$('#userID').val(validator.getValue('USER_ID'));
	$('#userID_desc').html(validator.getValue('BANKUSERS_USER_NAME'));
	$('#intimationMode').val(validator.getValue('INTIMATION_MODE'));
	$('#intimationReferenceNo').val(validator.getValue('INTIMATION_REF_NO'));
	$('#intimationDate').val(validator.getValue('INTIMATION_DATE'));
	$('#remarks').val(validator.getValue('REMARKS'));
	loadAuditFields(validator);
	var args = getEntityCode() + '|' + $('#userID').val();
	loadRecordData(loadAdditionalData, CURRENT_PROGRAM_ID, 'EUSERACTIVATION', args);
}
function loadAdditionalData() {
	$('#userActivationStatus').val(validator.getValue('STATUS'));
}