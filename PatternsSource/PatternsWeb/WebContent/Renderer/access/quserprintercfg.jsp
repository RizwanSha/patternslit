<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.access.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/access/quserprintercfg.js" />
	</web:dependencies>
	<web:loadPK />
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="quserprintercfg.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:viewContent id="po_view">
				<web:dividerBlock>
					<web:sectionBlock width="width-100p" align="left">
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="180px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="form.userid" var="common" mandatory="true" />
									</web:column>
									<web:column>
										<type:userDisplay property="userId" id="userId" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="form.effectivedate" var="common" mandatory="true" />
									</web:column>
									<web:column>
										<type:dateDisplay property="effectiveDate" id="effectiveDate" />
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
						<web:section>
							<web:viewTitle var="program" key="iuserprintercfg.gridSection" />
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="180px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="iuserprintercfg.printtypeid" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:printTypeDisplay property="printTypeId" id="printTypeId" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="iuserprintercfg.printerid" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:printerIdDisplay property="printerId" id="printerId" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="iuserprintercfg.defaultprinter" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:checkboxDisplay property="defaultPrinter" id="defaultPrinter" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:rowOdd>
									<web:column>
										<web:grid height="240px" width="860px" id="iuserprintercfg_innerGrid" src="access/iuserprintercfg_innerGrid.xml">
										</web:grid>
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
						<web:section>
							<web:viewTitle var="program" key="iuserprintercfg.pgmgridSection" />
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="180px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="iuserprintercfg.pgmId" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:optionIDDisplay property="pgmId" id="pgmId" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="iuserprintercfg.progPrinterId" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:printerIdDisplay property="progPrinterId" id="progPrinterId" />
									</web:column>
								</web:rowOdd>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:rowEven>
									<web:column>
										<web:grid height="240px" width="710px" id="iuserprintercfgpgm_innerGrid" src="access/iuserprintercfgpgm_innerGrid.xml">
										</web:grid>
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:auditDisplay />
					</web:sectionBlock>
				</web:dividerBlock>
			</web:viewContent>
		</web:viewSubPart>
	</web:viewPart>
</web:fragment>