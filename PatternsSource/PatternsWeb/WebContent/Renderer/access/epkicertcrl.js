var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = 'EPKICERTCRL';
function init() {
	doPurpose(FILE_PURPOSE_CRL_LIST);
	refreshQueryGrid();
	refreshTBAGrid();
	if (_redisplay) {
		enablingFields();
	} 
}

function refreshQueryGrid() {
	loadQueryGrid(CURRENT_PROGRAM_ID, 'EPKICERTCRL_MAIN', queryGrid);
}
function refreshTBAGrid() {
	loadTBAGrid(CURRENT_PROGRAM_ID, tbaGrid);
}
function clearFields() {
	
	$('#rootCaCode').val(EMPTY_STRING);
	$('#rootCaCode_desc').html(EMPTY_STRING);
	$('#rootCaCode_error').html(EMPTY_STRING);
	$('#enabled').prop('checked',false);
	$('#remarks').val(EMPTY_STRING);
	$('#remarks_error').html(EMPTY_STRING);
	$('#fileInventoryNumber').val(EMPTY_STRING);
	$('#fileName').val(EMPTY_STRING);
}
function add() {
	
	$('#changeDateTime').val(getCBD());
	$('#fileExtensionList').val(CRL_FILE_EXTENSION);
	$('#cmdFileSelector').attr("disabled", "disabled"); 
	$('#cmdPreview').attr("disabled", "disabled"); 
	
}
function modify() {
	
	$('#rootCaCode').focus();
	$('#fileExtensionList').val(CRL_FILE_EXTENSION);
}
function loadFileName(){
	var value = $('#fileInventoryNumber').val();
	validator.reset();
	validator.setClass('patterns.config.web.forms.access.epkicertcrlbean');
	validator.setValue('CERT_INV_NUM', value);
	validator.setMethod('fetchFileName');
	validator.sendAndReceiveAsync(loadAdditionalData);
}

function loadAdditionalData() {
	if (validator.getValue(RESULT) == DATA_AVAILABLE) {
	$('#fileName').val(validator.getValue('CERT_FILE_NAME'));
	}
}
function doFilePreview(){
	if(fileInventoryNumber_val()){
	var primaryKey = getEntityCode() + '|' + $('#fileInventoryNumber').val() ;
	showWindow('PREVIEW_VIEW',getBasePath()+ 'Renderer/common/qcertificateinfo.jsp',CERTIFICATE_VIEW_TITLE,PK+'='+primaryKey,true,false,false);
	resetLoading();
	}

}
function loadData() {
	$('#changeDateTime').val(getCBD());
	$('#remarks').val(validator.getValue('REMARKS'));
	$('#rootCaCode').val(validator.getValue('ROOT_CA_CODE'));
	$('#rootCaCode_desc').html(validator.getValue('PKICERTAUTH_DESCRIPTION'));
	$('#fileInventoryNumber').val(validator.getValue('INV_NUM'));
	$('#enabled').prop('checked',decodeSTB(validator.getValue('ENABLED')));
	if($('#enabled').is(':checked')){
		$('#cmdFileSelector').removeAttr("disabled"); 
		$('#cmdPreview').removeAttr("disabled"); 
	}
else{
	$('#cmdFileSelector').attr("disabled", "disabled"); 
	$('#cmdPreview').attr("disabled", "disabled"); 
 }
	loadFileName();
	resetLoading();
}
function loadAdditionalData() {
	$('#fileName').val(validator.getValue('CERT_FILE_NAME'));
	}
function view(source, primaryKey) {
	hideParent('access/qpkicertcrl', source, primaryKey);
	resetLoading();
}
function doHelp(id) {
	switch (id) {
	case 'rootCaCode':
		help(CURRENT_PROGRAM_ID, 'CACODES', $('#rootCaCode').val(), EMPTY_STRING, $('#rootCaCode'));
		break;	
	}
}
function validate(id) {
	switch (id) {
	case 'remarks':
		remarks_val();
		break;
	case 'rootCaCode':
		rootCaCode_val();
		break;	
	case 'fileInventoryNumber':
		fileInventoryNumber_val();
		break;	
	}
}
function checkclick(id) {
	switch (id) {
	case 'enabled':
		enablingFields();
		break;
	}
}
function enablingFields(){
	if ($('#enabled').is(':checked')){
		$('#cmdFileSelector').removeAttr("disabled"); 
		$('#cmdPreview').removeAttr("disabled"); 
	}
	else{
		$('#cmdFileSelector').attr("disabled", "disabled"); 
		$('#cmdPreview').attr("disabled", "disabled"); 
	  }
}
function effectiveDate_val() {
	var value = $('#effectiveDate').val();
	clearError('effectiveDate_error');
	if (!isValidEffectiveDate(value, 'effectiveDate_error')) {
		return false;
	}
	return true;
}
function rootCaCode_val() {
	var value = $('#rootCaCode').val();
	clearError('rootCaCode_error');
	if (isEmpty(value)) {
		setError('rootCaCode_error', MANDATORY);
		return false;
	 }
	return true;
}
function remarks_val() {
	var value = $('#remarks').val();
	clearError('remarks_error');
	if (isEmpty(value)) {
		setError('remarks_error', MANDATORY);
		return false;
	}
	if (!isValidRemarks(value)) {
		setError('remarks_error', INVALID_REMARKS);
		return false;
	}
	return true;
}
function fileInventoryNumber_val() {
	if(!$('#enabled').is(':checked')){
	var value = $('#fileInventoryNumber').val();
	clearError('fileInventoryNumber_error');
	if (isEmpty(value)) {
		setError('fileInventoryNumber_error', MANDATORY);
		return false;
	   }
	}
	return true;
}
function revalidate() {
		if (!effectiveDate_val()) {
			errors++;
		}
		if (!fileInventoryNumber_val()) {
			errors++;
		}
		if (!remarks_val()) {
			errors++;
		}
}