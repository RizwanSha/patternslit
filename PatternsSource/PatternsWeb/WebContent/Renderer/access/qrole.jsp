<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:bundle baseName="patterns.config.web.forms.access.Messages" var="program" />
<web:fragment>
	<web:dependencies>
		<web:script src="Renderer/access/qrole.js" />
	</web:dependencies>
	<web:loadPK />
	<web:viewPart>
		<web:viewSubPart>
			<web:programTitle var="program" key="qrole.programtitle" />
		</web:viewSubPart>
		<web:viewSubPart>
			<web:viewContent id="po_view1">
				<web:dividerBlock>
					<web:sectionBlock width="width-100p" align="left">
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowEven>
									<web:column>
										<web:legend key="form.rolecode" var="common" mandatory="true" />
									</web:column>
									<web:column>
										<type:inputCodeDisplay property="roleCode" id="roleCode" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:section>
							<web:table>
								<web:columnGroup>
									<web:columnStyle width="200px" />
									<web:columnStyle />
								</web:columnGroup>
								<web:rowOdd>
									<web:column>
										<web:legend key="form.description" var="common" mandatory="true" />
									</web:column>
									<web:column>
										<type:descriptionDisplay property="roleDescription" id="roleDescription" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="mrole.roletype" var="program" mandatory="true" />
									</web:column>
									<web:column>
										<type:comboDisplay property="roleType" id="roleType" datasourceid="COMMON_ROLES" />
									</web:column>
								</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="mrole.attributesReq" var="program" />
									</web:column>
									<web:column>
										<type:checkboxDisplay property="attributesReq" id="attributesReq" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
										<web:column>
											<web:legend key="mrole.fieldrole" var="program" />
										</web:column>
										<web:column>
											<type:checkboxDisplay property="fieldRole" id="fieldRole" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="mrole.cashierrole" var="program" />
										</web:column>
										<web:column>
											<type:checkboxDisplay property="cashierRole" id="cashierRole" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="mrole.paymentsallow" var="program" />
										</web:column>
										<web:column>
											<type:checkboxDisplay property="paymentsAllow" id="paymentsAllow" />
										</web:column>
									</web:rowEven>
									<web:rowOdd>
										<web:column>
											<web:legend key="mrole.receiptsallow" var="program" />
										</web:column>
										<web:column>
											<type:checkboxDisplay property="receiptsAllow" id="receiptsAllow" />
										</web:column>
									</web:rowOdd>
									<web:rowEven>
										<web:column>
											<web:legend key="mrole.headcashrole" var="program" />
										</web:column>
										<web:column>
											<type:checkboxDisplay property="headCashRole" id="headCashRole" />
										</web:column>
									</web:rowEven>
								<web:rowOdd>
									<web:column>
										<web:legend key="form.enabled" var="common" />
									</web:column>
									<web:column>
										<type:checkboxDisplay property="enabled" id="enabled" />
									</web:column>
								</web:rowOdd>
								<web:rowEven>
									<web:column>
										<web:legend key="form.remarks" var="common" mandatory="true" />
									</web:column>
									<web:column>
										<type:remarksDisplay property="remarks" id="remarks" />
									</web:column>
								</web:rowEven>
							</web:table>
						</web:section>
						<web:auditDisplay />
					</web:sectionBlock>
				</web:dividerBlock>
			</web:viewContent>
		</web:viewSubPart>
	</web:viewPart>
</web:fragment>
