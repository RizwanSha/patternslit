<%@tag body-content="empty" %>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@attribute name="styleClass" required="true" rtexprvalue="false"%>

	<%
		String action = request.getParameter("_AI");
		if (action == null) {
			action = (String) request.getAttribute("_AI");
		}
		String actionCode = request.getParameter("_RC");
		if (actionCode == null) {
			actionCode = (String) request.getAttribute("_RC");
		}
		String additionalDetailInfo = request.getParameter("XML_ADI");
	%>
	
	<%
		if(additionalDetailInfo==null){
	%>
		<div class="textalign-center">
		<%
			if (action == null || action.trim().equals("")) {
		%>
			<div class="level5_description"></div>
		<%
			} else {
		%>
			<div class="level4_message ${styleClass}"><%=((actionCode != null) ?  (actionCode + " : ") : "") + action%></div>
			
		<%
			}
		%>
		</div>
	<%
		}else{
		
	%>
		<div id='additionalDetailInfoDiv' align='center'>
			<input type="hidden" id="additionalDetailInfo" value="<%=additionalDetailInfo%>" />
		</div>
	<%
		
		}
	%>		
