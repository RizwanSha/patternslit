<%@tag body-content="empty" %>
<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@attribute name="key" required="true"%>
<%@attribute name="var" required="true" rtexprvalue="true"%>
<%@attribute name="styleClass" required="false" rtexprvalue="true"%>
<span class="frm_fld_lgnd ${styleClass}">
	<web:message var="${var}" key="${key}" />
</span>