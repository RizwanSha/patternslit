<%@tag body-content="empty"  %>
<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:section>
	<web:viewTitle var="common" key="form.auditdetails" />
	<web:table>
		<web:rowEven>
			<web:column span="2">
				<web:table borderNotRequired="true">
					<web:columnGroup>
						<web:columnStyle width="180px" />
						<web:columnStyle width="180px" />
						<web:columnStyle width="180px" />
						<web:columnStyle />
					</web:columnGroup>
					<web:rowEven>
						<web:column>
							<web:legend key="form.createdby" var="common" />
						</web:column>
						<web:column>
							<type:userDisplay property="createdBy" id="createdBy" />
						</web:column>
						<web:column>
							<web:legend key="form.createdon" var="common" />
						</web:column>
						<web:column>
							<type:dateTimeDisplay property="createdOn" id="createdOn" />
						</web:column>
					</web:rowEven>
				</web:table>
			</web:column>
		</web:rowEven>
		<web:rowOdd>
			<web:column span="2">
				<web:table borderNotRequired="true">
					<web:columnGroup>
						<web:columnStyle width="180px" />
						<web:columnStyle width="180px" />
						<web:columnStyle width="180px" />
						<web:columnStyle />
					</web:columnGroup>
					<web:rowOdd>
						<web:column>
							<web:legend key="form.modifiedby" var="common" />
						</web:column>
						<web:column>
							<type:userDisplay property="modifiedBy" id="modifiedBy" />
						</web:column>
						<web:column>
							<web:legend key="form.modifiedon" var="common" />
						</web:column>
						<web:column>
							<type:dateTimeDisplay property="modifiedOn" id="modifiedOn" />
						</web:column>
					</web:rowOdd>
				</web:table>
			</web:column>
		</web:rowOdd>
		<web:rowEven>
			<web:column span="2">
				<web:table borderNotRequired="true">
					<web:columnGroup>
						<web:columnStyle width="180px" />
						<web:columnStyle width="180px" />
						<web:columnStyle width="180px" />
						<web:columnStyle />
					</web:columnGroup>
					<web:rowEven>
						<web:column>
							<web:legend key="form.authorizedby" var="common" />
						</web:column>
						<web:column>
							<type:userDisplay property="authBy" id="authBy" />
						</web:column>
						<web:column>
							<web:legend key="form.authorizedon" var="common" />
						</web:column>
						<web:column>
							<type:dateTimeDisplay property="authOn" id="authOn" />
						</web:column>
					</web:rowEven>
				</web:table>
			</web:column>
		</web:rowEven>
	</web:table>
</web:section>