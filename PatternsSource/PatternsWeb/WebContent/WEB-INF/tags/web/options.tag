<%@tag body-content="empty"  %>
<%@attribute name="datasourceid" required="false" rtexprvalue="true"%>
<%
	String[] values = datasourceid.split("_");
	String tokenID="",programID="";
	for(int i=1;i<values.length;i++){
		tokenID = tokenID + "_" + values[i];
	}
	tokenID = tokenID.substring(1);
patterns.config.framework.database.utils.DBContext dbContext = new patterns.config.framework.database.utils.DBContext();
	java.util.ArrayList<patterns.config.framework.web.GenericOption> roleTypeList = null;
	roleTypeList = patterns.config.web.forms.GenericFormBean.getGenericOptions(values[0], tokenID, dbContext, "--");
	dbContext.close();
	for(patterns.config.framework.web.GenericOption option : roleTypeList){
		String id = option.getId();
		String label = option.getLabel();
%>
	<option value="<%=id%>"><%=label%></option>
<%	}
%>
