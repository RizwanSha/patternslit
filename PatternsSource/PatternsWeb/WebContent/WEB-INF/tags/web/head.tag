<%@tag import="patterns.config.framework.thread.ApplicationContext"%>
<%@tag body-content="scriptless"%>
<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="resource" tagdir="/WEB-INF/tags/resource"%>

<head>
<%
	java.util.Locale locale = null;
	try {
		locale = ApplicationContext.getInstance().getLocale();
	} catch (Exception e) {
		locale = java.util.Locale.US;
	}
%>
<title><web:message key="header.applicationtitle" var="common" /></title>

<%
	javax.servlet.http.HttpServletRequest _request = (javax.servlet.http.HttpServletRequest) request;
	String _path = _request.getContextPath();
	String _BasePath = _request.getScheme() + "://" + _request.getServerName() + ":" + _request.getServerPort() + _path + "/";
%>
<base href="<%=_BasePath%>" />
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="PRAGMA" content="no-cache">
<meta http-equiv="CACHE-CONTROL" content="private">
<meta http-equiv="CACHE-CONTROL" content="no-store">
<meta http-equiv="CACHE-CONTROL" content="must-revalidate">
<meta http-equiv="CACHE-CONTROL" content="post-check=0,pre-check=0">
<meta http-equiv="Expires" content="-1">

<%--Styles --%>
<resource:css href="public/cdn/vendor/bootstrap/css/bootstrap.css" media="screen" />
<%-- <resource:css href="public/cdn/vendor/bootstrap/css/bootstrap_ippb.css" media="screen" /> --%>
<resource:css href="public/cdn/vendor/dhtmlxCalendar/codebase/skins/dhtmlxcalendar_dhx_web.css" media="screen" />
<resource:css href="public/cdn/vendor/dhtmlxCombo/codebase/skins/dhtmlxcombo_dhx_web.css" media="screen" />
<resource:css href="public/cdn/vendor/dhtmlxGrid/codebase/skins/dhtmlxgrid_dhx_web.css" media="screen" />
<resource:css href="public/cdn/vendor/dhtmlxWindows/codebase/skins/dhtmlxwindows_dhx_web.css" media="screen" />
<resource:css href="public/cdn/vendor/dhtmlxToolbar/codebase/skins/dhtmlxtoolbar_dhx_web.css" media="screen" />
<resource:css href="public/cdn/vendor/dhtmlxTabbar/codebase/skins/dhtmlxtabbar_dhx_web.css" media="screen" />
<resource:css href="public/styles/index.css" media="screen" />
<%-- <resource:css href="public/styles/index_ippb.css" media="screen" /> --%>
<resource:css href="public/styles/css/font-awesome.min.css" media="screen" />

<%--Scripts --%>

<resource:script src="public/cdn/vendor/jquery/jquery.js" />
<resource:script src="public/cdn/vendor/jquery/jquery.autocomplete.js" />
<resource:script src="public/cdn/vendor/jquery/jquery.browser.js" />
<resource:script src="public/cdn/vendor/jquery/jquery.shortcuts.js" />

<resource:script src="public/cdn/vendor/moment.js" />


<resource:script src="public/cdn/vendor/bootstrap/js/bootstrap.js" />

<resource:script src="public/scripts/common.js" />
<resource:script src="public/scripts/sha256.js" />
<resource:script src="public/scripts/i18n/messages.js" />
<resource:script src="public/cdn/vendor/dhtmlxCommon/codebase/dhtmlxcommon.js" />
<resource:script src="public/cdn/vendor/dhtmlxCommon/codebase/dhtmlxcontainer.js" />


<resource:script src="public/cdn/vendor/dhtmlxCalendar/codebase/dhtmlxcalendar.js" />


<resource:script src="public/cdn/vendor/dhtmlxCombo/codebase/dhtmlxcombo.js" />


<resource:script src="public/cdn/vendor/dhtmlxGrid/codebase/dhtmlxgrid.js" />

<resource:script src="public/cdn/vendor/dhtmlxGrid/codebase/ext/dhtmlxgrid_splt.js" />
<resource:script src="public/cdn/vendor/dhtmlxGrid/codebase/ext/dhtmlxgrid_pgn.js" />
<resource:script src="public/cdn/vendor/dhtmlxGrid/codebase/ext/dhtmlxgrid_group.js" />
<resource:script src="public/cdn/vendor/dhtmlxGrid/codebase/excells/dhtmlxgrid_excell_cntr.js" />
<resource:script src="public/cdn/vendor/dhtmlxGrid/codebase/excells/dhtmlxgrid_excell_link.js" />


<resource:script src="public/cdn/vendor/dhtmlxGrid/codebase/ext/dhtmlxgrid_filter.js" />


<resource:script src="public/cdn/vendor/dhtmlxWindows/codebase/dhtmlxwindows.js" />


<resource:script src="public/cdn/vendor/dhtmlxToolbar/codebase/dhtmlxtoolbar.js" />


<resource:script src="public/cdn/vendor/dhtmlxTabbar/codebase/dhtmlxtabbar.js" />

<resource:script src="public/scripts/querygrid.js" />
<resource:script src="public/scripts/queryview.js" />
<resource:script src="public/cdn/vendor/jquery/jquery.cookie.js" />

<resource:script src="public/cdn/vendor/jquery/jquery.cookie.js" />

<jsp:doBody />
</head>