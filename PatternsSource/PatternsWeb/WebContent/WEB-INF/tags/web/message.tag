<%@tag body-content="empty"  %>
<%@attribute name="key" required="true" rtexprvalue="true"%>
<%@attribute name="var" required="true" rtexprvalue="true"%>
<%@attribute name="draw" required="false"%>
<%
	String message = "&nbsp;";
	if(key !=null && !key.trim().equals("")){
		java.util.ResourceBundle bundle = (java.util.ResourceBundle)request.getAttribute(var);
		try{
		message = bundle.getString(key);
		}catch(Exception e){
			message = "#{" + key + "}";
		}
		if(draw == null){
%>
		<%=message%>
<%
		}else{
			request.setAttribute("message",message);	
		}
	}else{
%>

		<%=message%>
<%
	}
%>