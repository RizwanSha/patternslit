<%@tag import="patterns.config.framework.charsets.UTF8Control"%>
<%@tag import="java.util.Locale"%>
<%@tag body-content="empty"  %>
<%@attribute name="baseName" required="true"%>
<%@attribute name="var" required="true"%>
<%
	java.util.Locale locale = null;
	try{
		locale =patterns.config.framework.thread.ApplicationContext.getInstance().getLocale();
	}catch(Exception e){
		locale = Locale.US;
	}
	java.util.ResourceBundle bundle = null;
	try{
		bundle = java.util.ResourceBundle.getBundle(baseName.replaceAll("\\.","/"),locale,new UTF8Control());
		request.setAttribute(var,bundle);
	}catch(java.lang.Exception e){
		e.printStackTrace();
	}
	
	java.util.Locale additionalLocale= null;
	
	if(request.getAttribute("ADDITIONAL_LOCALE") != null){
		String additionalLocaleName = (String)request.getAttribute("ADDITIONAL_LOCALE");
		try{
			additionalLocale= new java.util.Locale(additionalLocaleName);
		}catch(Exception e){
			additionalLocale = Locale.US;
		}	
		java.util.ResourceBundle additionalBundle = null;
		try{
			additionalBundle = java.util.ResourceBundle.getBundle(baseName.replaceAll("\\.","/"),additionalLocale,new UTF8Control());
			request.setAttribute(var + '_'+ additionalLocaleName,additionalBundle);
		}catch(java.lang.Exception e){
			e.printStackTrace();
		}
	}
	
%>