<%@tag body-content="scriptless"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%--Dependencies--%>
	<c:choose>
		<c:when test="${requestScope['show_errors'] eq 'show_errors'}">
			<script type="text/javascript">
				_redisplay = true;
			</script>	
		</c:when>
	</c:choose>
	<jsp:doBody />
<%--/Dependencies--%>