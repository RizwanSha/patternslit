<%@tag body-content="empty"  %>
<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@attribute name="key" required="true" rtexprvalue="true"%>
<c:out value="${requestScope[key]}" />