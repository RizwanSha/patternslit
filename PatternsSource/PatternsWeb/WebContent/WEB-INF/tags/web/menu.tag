<%@tag body-content="empty"%>
<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<c:if test="${sessionScope['_PRS'] eq '0'}">
	<div class="offcanvas-menu">
	<!-- 	Search Div removed by Dileep on 29-09-2016 !-->
		<%-- <div class="menu-content-search">
			<table>
				<web:viewSubPart>
					<web:table>
						<web:rowOdd>
							<web:column align="center">
								<type:smartNavigation />
							</web:column>
						</web:rowOdd>
					</web:table>
				</web:viewSubPart>
			</table>
		</div> --%>
		<!-- <div class="menu-content">
			<web:viewPart>
				<web:viewSubPart>
					<web:subProgramTitle var="common" key="form.console" collapsable="po_menu" descReqd="true" />
					<web:viewContent id="po_menu">
						<web:table>
							<web:rowOdd>
								<web:column align="center">
									<type:consoleCombo />
								</web:column>
							</web:rowOdd>
							<web:rowEven>
								<web:column align="center">
									<div id="program_div" style="height: 176px; overflow: auto;"></div>
								</web:column>
							</web:rowEven>
						</web:table>
					</web:viewContent>
				</web:viewSubPart>
			</web:viewPart>
		</div>
		 -->
		 <div class="menu-content">
			<web:viewPart>
				<web:viewSubPart>
					
					<web:viewContent id="po_menu">
						<web:table>
							<web:rowOdd>
								<web:column align="left">
									<div id="accordian" style="height:580px; overflow-y:auto;border-radius:5px">
										<ul>
											<type:accordionMenu/>
										</ul>
										<ul>
											<type:freqUsedOptionAccordion/>
										</ul>
									</div>
								</web:column>
							</web:rowOdd>
						</web:table>
					</web:viewContent>
				</web:viewSubPart>
			</web:viewPart>
		</div>
		 
		
		
	</div>
</c:if>
<script type="text/javascript">
	if($('#_rop').val()!=EMPTY_STRING && parseInt($('#_rop').val())==0)
		hide('ropdiv');
	
	$(document).ready(function(){
		$("#accordian h3").click(function(){
			var id=$(this).attr('id');
			$(this).removeClass('fa fa-chevron-down');
			$(this).addClass('fa fa-chevron-right');
			if(id=="menuh3"){
				$("#menuUL").slideUp();
			}
			else if(id=="recentUsedh3"){
				$("#recentUsedUL").slideUp();
			}
			if(!$(this).next().is(":visible"))
			{
				$(this).addClass('fa fa-chevron-down');
				$(this).next().slideDown();
			}
		});
		$("#accordian h2").click(function(){
			$("#accordian ul ul ul").slideUp();
			$("h2").removeClass('fa fa-chevron-down');
			$("h2").addClass('fa fa-chevron-right');
			
			if(!$(this).next().is(":visible"))
			{
				$(this).next().slideDown();
				$(this).addClass('fa fa-chevron-down');
			}
		})
	})
</script>