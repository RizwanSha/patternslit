<%@tag import="patterns.config.framework.web.DHTMLXGridUtility"%>
<%@tag body-content="scriptless"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@attribute name="width" required="true"%>
<%@attribute name="height" required="true"%>
<%@attribute name="pagingSize" required="false"%>
<%@attribute name="id" required="true"%>
<%@attribute name="src" required="true"%>
<%@attribute name="paging" required="false" type="java.lang.Boolean"%>
<table width="${width}">
	<tr>
		<td style="padding: 4px">
			<div id="${id}_div" style="width:${width};height:${height}"></div>
		</td>
	</tr>
	<c:if test="${paging eq true}">
		<tr>
			<td id="${id}_PA"></td>
		</tr>
	</c:if>
</table>
<c:set var="xmlDefinition" scope="page" value="" />
<%
	jspContext.setAttribute("xmlDefinition", DHTMLXGridUtility.getXMLDefinition("/Renderer/" + src));
%>
<input type="hidden" id="${id}_xmldef"
	value='<c:out value="${pageScope['xmlDefinition']}" />' />
<script type="text/javascript">
	${id} = new dhtmlXGridObject('${id}_div');
	${id}.setImagePath(WidgetConstants.GRID_IMAGE_PATH);
	${id}.init();
	${id}.loadXMLString(document.getElementById('${id}_xmldef').value);
	${id}.setUserData("","grid_id",EMPTY_STRING);
	${id}.setSortImgState(false);
	${id}.attachEvent("onCellUnMarked", function(rid,ind){
		${id}.setUserData("","grid_edit_id",EMPTY_STRING);
		${id}.setRowColor(rid, 'WHITE');
	});
	{
		/*
	var colCount = ${id}.getColumnsNum();
	var colSort = Array.apply(null, new Array(colCount)).map(String.prototype.valueOf,"na");
	${id}.setColSorting(colSort);
	*/
	${id}.attachEvent("onBeforeSorting", function(ind,type,direction){
		    //your code here
		    return false;
		});
	${id}.attachEvent("onPageChanged", function(ind,type,direction){
	    //your code here
	    var start=(${id}.currentPage - 1) * ${id}.rowsBufferOutSize;
		${id}.selectRow(start, true);
	});
	}
	/*
	${id}.attachEvent('onRowSelect', function(id, ind) {
		if(this.getColType(0) == "ch" || this.getColType(0) == "ra"){
			this.cells(id,0).setValue(true);
		}
	});
	*/
</script>

<c:if test="${(paging eq true) and (empty pagingSize)}">
	<script type="text/javascript">	
	${id}.enablePaging(true, 10, 9, "${id}_PA", true);
	${id}.setPagingSkin('toolbar', 'dhx_web');
	</script>
</c:if>

<c:if test="${(paging eq true) and (not empty pagingSize)}">
	<script type="text/javascript">
	${id}.enablePaging(true, ${pagingSize}, 9, "${id}_PA", true);
	${id}.setPagingSkin('toolbar', 'dhx_web');
	</script>
</c:if>





