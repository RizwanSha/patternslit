<%@tag body-content="empty"  %>
<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@attribute name="key" required="true" rtexprvalue="true"%>
<%@attribute name="var" required="true" rtexprvalue="true"%>
<div class="level3_title highlight-title">
	<web:message var="${var}" key="${key }" />
</div>