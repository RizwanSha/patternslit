<%@tag body-content="empty"%>
<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@attribute name="key" required="true"%>
<%@attribute name="id" required="true" rtexprvalue="true"%>
<%@attribute name="onclick" required="true" rtexprvalue="true"%>
<%@attribute name="var" required="true" rtexprvalue="true"%>

<web:message var="${var}" key="${key}" draw="draw" />
<%
	String value = (String) request.getAttribute("message");
%>
<input type="button" id="${id}" class="btn btn-xs btn-default" value='<%=value %>' onclick="${onclick}" />
