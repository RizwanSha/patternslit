<%@tag body-content="empty"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<html:hidden styleId="parentProgramId" property="parentProgramId"/>
<html:hidden styleId="parentPrimaryKey" property="parentPrimaryKey"/>
<html:hidden styleId="linkProgramId" property="linkProgramId"/>
<html:hidden styleId="linkPrimaryKey" property="linkPrimaryKey"/>
<html:hidden styleId="requestType" property="requestType"/>
<html:hidden styleId="parentAction" property="parentAction"/>
<html:hidden styleId="returnType" property="returnType"/>
<html:hidden styleId="sourceKey" property="sourceKey"/>
<html:hidden styleId="sourceValue" property="sourceValue"/>