<%@tag body-content="empty"  %>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@attribute name="key" required="true"%>
<%@attribute name="forward" required="true"%>
<%@attribute name="var" required="true" rtexprvalue="true"%>
<div class="textalign-center">
	<html:link forward="${forward}" styleClass="action-link textalign-center" >
		<web:message var="${var}" key="${key}" />
	</html:link>
</div>