<%@tag body-content="scriptless"  %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@attribute name="id" required="true" rtexprvalue="true"%>
<%@attribute name="styleClass" required="false" rtexprvalue="true"%>
<c:choose>
	<c:when test="${requestScope['show_errors'] eq 'show_errors'}">
		<div id="${id}" style="width: 100%" class="ie_fix textalign-left">
			<jsp:doBody />
		</div>	
	</c:when>
	<c:otherwise>
		<div id="${id}" class="${styleClass} ie_fix textalign-left" style="width: 100%">
			<jsp:doBody />
		</div>	
	</c:otherwise>
</c:choose>