<%@tag body-content="empty"%>
<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:if test="${(requestScope['RENDERER_TYPE'] eq '1' or requestScope['RENDERER_TYPE'] eq '5') and param.callSource ne 'C'}">
	<div class="role-popup hide-content">
		<label style="font-size:14px;font-weight:500;margin-bottom:2em;">SELECT TO SWITCH ROLE</label>
		<type:roleCombo />
		<input type="button" class="btn btn-primary" value="Switch" onclick="updateRole();" style="float: left;margin-left: 15px;padding: 8px 25px;">
		<input type="button" class="btn btn-danger" value="Cancel" onclick="rolePopup();" style="float: left;margin-left: 15px;padding: 8px 25px;">
	</div>
	<div class="stationery-popup hide-content">
		<label style="font-size:14px;font-weight:500;margin-bottom:2em;">SELECT TO SWITCH STATIONERY</label>
		<type:stationerySetting />
		<input type="button" class="btn btn-primary" value="Switch" onclick="updateStationery();" style="float: left;margin-left: 15px;padding: 8px 25px;">
		<input type="button" class="btn btn-danger" value="Cancel" onclick="stationeryPopup();" style="float: left;margin-left: 15px;padding: 8px 25px;">
	</div>
	<div class="logindetails-popup hide-content">
		<label style="font-size: 14px;"><b>LOGIN DETAILS</b></label>
		<type:loginDetails />
	</div>
</c:if>