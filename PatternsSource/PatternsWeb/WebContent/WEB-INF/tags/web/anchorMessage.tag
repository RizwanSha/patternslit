<%@tag body-content="empty" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@attribute name="var" required="true" rtexprvalue="true"%>
<%@attribute name="key" required="true"%>
<%@attribute name="href" required="false"%>
<%@attribute name="onclick" required="true" rtexprvalue="true"%>
<%@attribute name="forward" required="false" type="java.lang.Boolean"%>
<%@attribute name="style" required="true"%>
<%@attribute name="id" required="false"%>
<c:choose>
	<c:when test="${forward eq true}">
		<a href="${href}" onclick="${onclick}" style="${style}" id="${id}">
			<web:message var="${var}" key="${key}" />
		</a>
	</c:when>
	<c:otherwise>
		<a  onclick="${onclick}" style="${style}" id="${id}">
			<web:message var="${var}" key="${key}"/>
		</a>
	</c:otherwise>
</c:choose>