<%@tag body-content="scriptless"%>
<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@attribute name="action" required="true"%>
<%@attribute name="id" required="true"%>
<%@attribute name="method" required="true"%>
<html:form method="${method}" action="${action}" styleId="${id}" >
	<web:dividerBlock>
		<span class="level4_error"> <html:errors property="genericError" />
		</span>
	</web:dividerBlock>
	<jsp:doBody />
	<web:element property="actionDescription" />
	<web:element property="tfaRequired" />
	<web:element property="tfaNonce" />
	<web:element property="tfaValue" />
	<web:element property="tfaEncodedValue" />
	<web:element property="csrfValue" />
</html:form>
<c:if test="${requestScope['_TFA'] eq '1' }">
	<applet codebase="<%=request.getContextPath()%>/applets/" code="SignerApplet.class" name="SIGNAPPLET" width="1" height="1" id="SIGNAPPLET"></applet>
</c:if>
<script type="text/javascript">
	try {
		$('#${id}').setAttributes({
			"autocomplete" : "off"
		});
	} catch (e) {
	}
</script>