<%@tag body-content="empty"%>
<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@attribute name="key" required="true"%>
<%@attribute name="var" required="true" rtexprvalue="true"%>
<%@attribute name="mandatory" required="false" type="java.lang.Boolean"%>
<%@attribute name="breakreq" required="false" type="java.lang.Boolean"%>

<label class="control-label ">
	<web:message var="${var}" key="${key}" />
	<c:if test="${breakreq eq true}">
		<br>
	</c:if>
	(<web:message var="${var}_${requestScope['ADDITIONAL_LOCALE']}" key="${key}" />)
	<c:if test="${mandatory eq true}">
		<span style="color: red">*</span>
	</c:if>
</label>