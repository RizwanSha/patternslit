<%@tag body-content="empty"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<div id="helpGrid_handle" style="z-index: 999; width: 520px; height: 320px;" class="hidden">
	<table cellpadding="4" cellspacing="0" class="help-table" width="540px" height="320px">
		<tbody>
			<tr>
				<td height="28px" align="center" valign="middle">
					<table cellpadding="0" cellspacing="0" width="98%" align="center">
						<tr>
							<td width="180px"><select class="help-combo-box" id="help_combo"></select></td>
							<td width="180px"><input type="text" class="help-text-box" id="help_text" onKeydown="Javascript:if(event.keyCode==13) doHelpGridReload();"/></td>
							<td width="60px"><type:button key="form.go" id="help_go" var="common" onclick="doHelpGridReload()" /></td>
							<td>
								<div class="help-close-link" onclick="hideHelp()">
									<web:message var="common" key="form.close" />
								</div>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr class="help-table-row">
				<td align="center" valign="top">
					<table width="540px" cellspacing="0" cellpadding="0">
						<tr>
							<td>
								<div id="helpGrid_div" style="height: 340px; width: 100%;"></div>
							</td>
						</tr>
						<tr>
							<td id="helpGrid_PA"></td>
						</tr>
						<tr>
							<td>
								<div id="helpGrid_ST" style="display: none; font-size: 12px; color: red; overflow: hidden"></div>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</tbody>
	</table>
</div>