<%@tag body-content="empty"  %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@attribute name="width" required="false"%>
<%@attribute name="align" required="false"%>
<%
	if(align == null)
		align = "left";
%>
<c:choose>
	<c:when test="${width != null}">
		<col width="${width}" align="${align}"/>
	</c:when>
	<c:otherwise>
		<col />
	</c:otherwise>
</c:choose>

