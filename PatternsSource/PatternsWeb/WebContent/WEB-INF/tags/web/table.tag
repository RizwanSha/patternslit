<%@tag body-content="scriptless"  %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@attribute name="borderNotRequired" required="false" type="java.lang.Boolean" %>
<div class="div_wrap">
<c:choose>
	<c:when test="${borderNotRequired}">
		<div class="table_no_wrap">
			<table class="form_table_inner" width="100%">
				<jsp:doBody/>
			</table>
		</div>
	</c:when>
	<c:otherwise>
		<div class="table_wrap">
			<table class="form_table">
				<jsp:doBody/>
			</table>
		</div>
	</c:otherwise>
</c:choose>
</div>
	