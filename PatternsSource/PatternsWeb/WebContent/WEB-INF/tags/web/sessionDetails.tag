<%@tag body-content="empty"%>
<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:if test="${requestScope['RENDERER_TYPE'] eq '1'}">
	<div class="bs-docs-header">
		<div class="container">
			<ul class="nav navbar-nav navbar-right date-time">
				<li><b> <web:message var="common" key="header.currentlogin" />
				</b></li>
				<li style="margin-right:20px;"><b class="value"> &nbsp;<%=(java.util.Date) session.getAttribute("_LDT") == null ? "" : patterns.config.framework.web.FormatUtils.getDate((java.util.Date) session.getAttribute("_LDT"), "dd/MM/yyyy HH:mm:ss")%>
				</b></li>
				<li><b> <web:message var="common" key="header.cbd" />
				</b></li>
				<li><b class="value"> &nbsp;<%=(java.util.Date) session.getAttribute("_SDT") == null ? "" : patterns.config.framework.web.FormatUtils.getDate((java.util.Date) session.getAttribute("_SDT"), "dd/MM/yyyy HH:mm:ss")%>
				</b></li>
			</ul>
		</div>
	</div>
</c:if>