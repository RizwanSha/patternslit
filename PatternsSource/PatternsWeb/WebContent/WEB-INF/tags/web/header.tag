<%@tag body-content="scriptless"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:if test="${(requestScope['RENDERER_TYPE'] eq '1' or requestScope['RENDERER_TYPE'] eq '3' or  requestScope['RENDERER_TYPE'] eq '5') and param.callSource ne 'C'}">
	<div class="navbar navbar-inverse navbar-fixed-top drop-shadow">
		<jsp:doBody />
	</div>
</c:if>
