<%@tag body-content="empty"%>
<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:if test="${requestScope['RENDERER_TYPE'] eq '1'}">
	<span class="user-name"><c:out value="${sessionScope['_UN']}" /></span>
	<br />
	<span class="label label-info user-role"><c:out value="${sessionScope['_RC']}" /></span>
</c:if>