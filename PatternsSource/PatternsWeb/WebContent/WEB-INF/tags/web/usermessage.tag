<%@tag body-content="empty"  %>
<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@attribute name="key" required="true" %>
<%@attribute name="var" required="true" rtexprvalue="true"%>
<div class="legend user-message">
	<web:message var="${var}" key="${key}"/>
</div>
