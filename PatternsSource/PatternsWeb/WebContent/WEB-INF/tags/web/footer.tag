<%@tag body-content="empty"%>
<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<div class="navbar-fixed-bottom">
	<div class="footer clearfix">
		<ul id="footer-list">
			<li><a href="#" title="" onclick="goTo('sysreq')"> <web:message var="common" key="footer.sysreq" />
			</a></li>
			<li><a href="#" title="" onclick="goTo('security')"> <web:message var="common" key="footer.security" />
			</a></li>
			<li><a href="#" title="" onclick="goTo('disclaimer')"> <web:message var="common" key="footer.disclaimer" />
			</a></li>
		</ul>
		<ul class="pull-right" id="footer-list">
			<li><a> <label><web:message var="common" key="footer.organizationname" /></label>
			</a></li>
			<li><a> <img src="public/styles/images/branding/patterns.jpg" style="width: 25px" />
			</a></li>
		</ul>
	</div>
</div>

