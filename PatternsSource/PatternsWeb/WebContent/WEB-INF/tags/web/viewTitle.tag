<%@tag body-content="empty"%>
<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@attribute name="collapsable" required="false"%>
<%@attribute name="status" required="false"%>
<%@attribute name="key" required="true" rtexprvalue="true"%>
<%@attribute name="var" required="true" rtexprvalue="true"%>
<%@attribute name="styleClass" required="false"%>
<%@attribute name="addLocale" required="false"%>
<%@attribute name="refreshReq" required="false"%>
<%@attribute name="refreshFunction" required="false"%>
<c:choose>
	<c:when test="${styleClass != null}">
		<div class="${styleClass} highlight-title">
			<c:choose>
				<c:when test="${collapsable != null}">
					<c:if test="${status eq 'close'}">
						<b class="toggle right" title="click to toggle" onclick="toggle('${collapsable}',this)"><img src="public/styles/images/minimized.png.png" class="pic" /></b>
					</c:if>
					<c:if test="${(status eq 'open') or (status eq null)}">
						<b class="toggle right" title="click to toggle" onclick="toggle('${collapsable}',this)"><img src="public/styles/images/maximized.png" class="pic" /></b>
					</c:if>
				</c:when>
				<c:otherwise>
				</c:otherwise>
			</c:choose>
			<img src="public/styles/images/program.png" alt="Icon" /><span style="text-align: center"><web:message var="${var}" key="${key}" /></span>
			<c:if test="${refreshReq != null}">
				<span style="float: right;"><a  href="javascript:void(0);" onclick=${refreshFunction }><i id="refresh_icon" class="fa fa-refresh" style="font-size: 20px; color: #f4f4f4;"></i>
				</a>&nbsp;&nbsp; </span>
			</c:if>

			<c:if test="${addLocale eq true}">
				<span style="float: right;">(<web:message var="${var}_${requestScope['ADDITIONAL_LOCALE']}" key="${key}" />)
				</span>
			</c:if>
		</div>
	</c:when>
	<c:otherwise>
		<div class="level3_title highlight-title">
			<c:choose>
				<c:when test="${collapsable != null}">
					<c:if test="${status eq 'close'}">
						<b class="toggle right" title="click to toggle" onclick="toggle('${collapsable}',this)"> <img src="public/styles/images/minimized.png" class="pic" /></b>
					</c:if>
					<c:if test="${(status eq 'open') or (status eq null)}">
						<b class="toggle right" title="click to toggle" onclick="toggle('${collapsable}',this)"> <img src="public/styles/images/maximized.png" class="pic" /></b>
					</c:if>
				</c:when>
				<c:otherwise>
				</c:otherwise>
			</c:choose>
			<span style="text-align: center"><web:message var="${var}" key="${key}" /></span>
			<c:if test="${refreshReq != null}">
				<span style="float: right;"><a  href="javascript:void(0);" onclick=${refreshFunction }><i id="refresh_icon" class="fa fa-refresh" style="font-size: 20px; color: #f4f4f4;"></i>
				</a>&nbsp;&nbsp; </span>
			</c:if>
			<c:if test="${addLocale eq true}">
				<span style="float: right;">(<web:message var="${var}_${requestScope['ADDITIONAL_LOCALE']}" key="${key}" />)
				</span>
			</c:if>
		</div>
	</c:otherwise>
</c:choose>