<%@tag body-content="empty"%>
<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@attribute name="id" required="false"%>
<%@attribute name="key" required="true"%>
<%@attribute name="var" required="true" rtexprvalue="true"%>
<%@attribute name="mandatory" required="false" type="java.lang.Boolean"%>
<c:choose>
	<c:when test="${id ne null}">
		<label id="${id}Caption" class="control-label "><web:message var="${var}" key="${key}" />
			<c:if test="${mandatory eq true}">
				<span style="color: red">*</span>
			</c:if>
		</label>
	</c:when>
	<c:otherwise>
		<label class="control-label "><web:message var="${var}" key="${key}" />
			<c:if test="${mandatory eq true}">
				<span style="color: red">*</span>
			</c:if>
		</label>
	</c:otherwise>
</c:choose>
