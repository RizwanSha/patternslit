<%@tag body-content="empty"%>
<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@attribute name="collapsable" required="false"%>
<%@attribute name="status" required="false"%>
<%@attribute name="key" required="true" rtexprvalue="true"%>
<%@attribute name="var" required="true" rtexprvalue="true"%>
<%@attribute name="src" required="false" rtexprvalue="true"%>
<%@attribute name="descReqd" required="false" rtexprvalue="false"%>
<%@attribute name="statusRequired" required="false" rtexprvalue="false"%>
<%@attribute name="statusId" required="false" rtexprvalue="false"%>

<div class="level2_title textalign-left">
	<c:if test="${collapsable ne null}">
		<c:if test="${status eq 'close'}">
			<b id="${collapsable}_pic" class="toggle right" title="click to toggle" onclick="toggle('${collapsable}',this)"><img src="public/styles/images/minimized.png" class="pic" /></b>
		</c:if>
		<c:if test="${(status eq 'open') or (status eq null)}">
			<b id="${collapsable}_pic" class="toggle right" title="click to toggle" onclick="toggle('${collapsable}',this)"><img src="public/styles/images/maximized.png" class="pic" /></b>
		</c:if>
	</c:if>
	<span><web:message var="${var}" key="${key}" /></span>
	<c:if test="${statusRequired ne null}">
		<span id='${statusId}'></span>
	</c:if>
</div>