<%@tag body-content="scriptless"%>
<tr>
	<td align="center" height="100%" width="100%" valign="top">
		<table width="100%" cellpadding="0" cellspacing="0" border="0" class="content-view">
			<tr>
				<td width="12"></td>
				<td style="vertical-align: top">
					<div class="form-horizontal form-group-sm">
						<jsp:doBody />
					</div>
				</td>
				<td width="12"></td>
			</tr>
		</table>
	</td>
</tr>
