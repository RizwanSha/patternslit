<%@tag body-content="empty" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@attribute name="width" required="true"%>
<%@attribute name="height" required="true"%>
<%@attribute name="id" required="true" rtexprvalue="true"%>
<div style="margin: 0.5em">
<div id="${id}_div" style="width:${width};height:${height}px;" style="background-color:white;"></div>
<div id="${id}_PA" style="background-color: transparent; z-index: 202; overflow: hidden"></div>
<div id="${id}_ST" style="display: none; font-size: 12px; color: red; overflow: hidden"></div>
</div>