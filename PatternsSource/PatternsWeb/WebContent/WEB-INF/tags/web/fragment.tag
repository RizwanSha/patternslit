
<%@tag body-content="scriptless"%>
<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:choose>
	<c:when test="${requestScope['RENDERER_TYPE'] eq '4'}">
		<jsp:doBody />
	</c:when>
	<c:otherwise>
		<web:bundle baseName="patterns.config.web.forms.Messages" var="common" />
		<web:html>
		<web:head></web:head>
		<web:body>
			<form id="redirectionForm" action="" method="POST">
				<%--<input typ	e="hidden" name="<csrf:token-name />" value="<csrf:token-value />" /> --%>
			</form>
			<web:session />
			<web:navigationDetails />
			<web:sidenavbar />
			<web:header>
				<web:brandingInfo />
			</web:header>
			<web:wrapper>
				<c:if
					test="${requestScope['RENDERER_TYPE'] eq '1' or requestScope['RENDERER_TYPE'] eq '5'}">
					<web:contentForm>
						<jsp:doBody />
					</web:contentForm>
				</c:if>
				<c:if test="${requestScope['RENDERER_TYPE'] eq '2'}">
					<web:contentView>
						<jsp:doBody />
					</web:contentView>
				</c:if>
				<c:if test="${requestScope['RENDERER_TYPE'] eq '3'}">
					<web:contentCustom>
						<jsp:doBody />
					</web:contentCustom>
				</c:if>
			</web:wrapper>
			<a href="#" id="reland" class="back-to-top">Back to Top</a>
		</web:body>
		</web:html>
	</c:otherwise>
</c:choose>