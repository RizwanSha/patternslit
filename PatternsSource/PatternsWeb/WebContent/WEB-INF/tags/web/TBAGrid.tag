<%@tag import="patterns.config.framework.web.ajax.ContentManager"%>
<%@tag import="patterns.config.framework.service.DTObject"%>
<%@tag import="patterns.config.framework.database.RegularConstants"%>
<%@tag body-content="scriptless" isELIgnored="false"%>
<%@tag import="patterns.config.validations.AccessValidator"%>
<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%
	String isAuthRequired = RegularConstants.ZERO;
	AccessValidator validation = new AccessValidator();
	try {
		DTObject resultDTO = new DTObject();
		resultDTO.set("MPGM_ID",(String) request.getAttribute(patterns.config.framework.web.RequestConstants.PROCESS_ID));
		resultDTO.set(ContentManager.FETCH_COLUMNS, "MPGM_AUTH_REQD");
		resultDTO = validation.validateOption(resultDTO);
		isAuthRequired = resultDTO.get("MPGM_AUTH_REQD");
	} catch (Exception e) {
		e.printStackTrace();
	}finally{
		validation.close();
	}
	if (RegularConstants.ONE.equals(isAuthRequired)) {
%>
<web:viewPart>
	<web:viewSubPart>
		<web:subProgramTitle var="common" key="form.rectification"
			collapsable="po_view3" descReqd="true" statusRequired="true"
			statusId="tbaRowCount" />
	</web:viewSubPart>
	<web:viewSubPart>
		<web:viewContent id="po_view3">
			<div id="tbaGrid_div"
				style="margin-left: 0.2em; margin-right: 0.2em; width: 700px; height: 240px; background-color: white;"></div>
			<div id="tbaGrid_PA"
				style="margin-left: 0.2em; background-color: transparent; z-index: 202; overflow: hidden"></div>
			<div id="tbaGrid_ST"
				style="margin-left: 0.2em; display: block; font-size: 12px; color: red; overflow: hidden; height: 1em;"></div>
			<web:message var="common" key="form.tbagrid" draw="draw" />
			<script type="text/javascript">
				tbaGrid = new dhtmlXGridObject('tbaGrid_div');
				tbaGrid.setImagePath(WidgetConstants.GRID_IMAGE_PATH);
				tbaGrid.setHeader("${requestScope['message']}");
				tbaGrid.setInitWidths("50,0,0,0,150,80,150,100,0,150");
				tbaGrid.setColAlign("center,left,left,left,left,center,left,center,left,left");
				tbaGrid.setColTypes("link,ro,ro,ro,ro,ro,ro,ro,ro,ro,");
				tbaGrid.init();
				tbaGrid.enableColumnAutoSize(false);
				//tbaGrid.setSkin("light");
				tbaGrid.setColumnHidden(1, true);
				tbaGrid.setColumnHidden(2, true);
				tbaGrid.setColumnHidden(3, true);
			</script>
		</web:viewContent>
	</web:viewSubPart>
</web:viewPart>
<%
	} else {
%>
<script type="text/javascript">
	tbaGrid = new dhtmlXGridObject('');
</script>
<%
	}
%>