<%@tag body-content="empty"%>
<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!--  
<c:if test="${requestScope['RENDERER_TYPE'] eq '3'}">
	<div class="container">
		<div class="navbar-header">
			<a class="navbar-brand" href="javascrip:void(0);"></a>
			
			<div class="navbar-info">
				<div class="entity-name">PSDI HOSPITAL</div>
				<div class="entity-location">Chennai</div>
			</div>
			
		</div>
	</div>
</c:if>
-->

<c:if test="${requestScope['RENDERER_TYPE'] eq '1'}">
	<div class="container">
		<div class="navbar-header">
			<!--  <a class="navbar-brand" href="Renderer/common/elanding.jsp"></a>-->
			<a class="navbar-brand" href="javascript:void(0)" onclick="loadLandingPage()"></a>
			<ul class="nav navbar-nav navbar-left">
				<li><a href="javascript:void(0)" class="nav-icon" style="line-height:2.3" title="Menu" onclick="slideMenu()"> <i class="fa fa-lg fa-bars"></i></a></li>
<!-- 				<li><a href="Renderer/common/elanding.jsp" style="line-height:3" class="nav-icon" title="Home"> <i class="fa fa-lg fa-home"></i></a></li> -->
			</ul>
		</div>
		<nav class="collapse navbar-collapse bs-navbar-collapse" role="navigation">
			<ul class="nav navbar-nav navbar-right">
			<!-- 			Added by Dileep on 29-09-2016 starts -->
				<li><a href="javascript:void(0)" class="navbar-icon" title="Program Search" onclick="showProgramSearch()" style="float: right;"><i class="fa fa-search"></i></a>
					<div class="smartSearchBox" style="width: 240px">
						<input type='text' class="smartSearchInput" placeholder="Search" id="smartNavigate" maxlength="40" />
					</div> <script type="text/javascript">
						var smartNavigate_ref = new AutoComplete("smartNavigate");
					</script></li>
<!-- 					Added by Dileep on 29-09-2016 ends -->
				
				<li><span class="cbd"><%=(java.util.Date) session.getAttribute("_SDT") == null ? "" : patterns.config.framework.web.FormatUtils.getDate((java.util.Date) session.getAttribute("_SDT"), "EEEE dd , MMMM YYYY")%>
					</span>
				<br>
				<span class="user-office"> User Office:	</span>
				<span class="user-office"> <%= session.getAttribute("_BRC") %></span>
				</li>
<!-- 				<li><a class="line"><span class="line-seperator"></span></a></li> -->
				<li><web:userDetails /></li>
<!-- 				<li><a class="line"><span class="line-seperator"></span></a></li> -->
<!--                  <li><a class="navbar-icon" style="background-color: white;padding: 7px;border-radius: 37px; border: solid 1px #2f7698;" href="#" data-toggle="dropdown"><i class="fa fa-lg fa-user" style=" color: #439dc9;font-size: 47px;" id="userSettings" alt="UserSettings"></i></a> -->

				<li><a class="navbar-icon"  href="#" data-toggle="dropdown"><i class="fa fa-cog" style=" color: #ffffff;font-size: 24px;" id="userSettings" alt="UserSettings"></i></a>
					<ul class="dropdown-menu" style="margin-top: 10px;" role="menu" aria-labelledby="dropdownMenu1">
						<li role="presentation"><a role="menuitem" tabindex="-1" onclick="loginInfoPopup();" href="javascript:void(0);"> <i class="fa fa-info-circle" style="margin-right: 5px"></i> <label>Login Details</label></a></li>
						<li role="presentation"><a role="menuitem" tabindex="-1" href="Renderer/common/epwdchgn.jsp"><i class="glyphicon glyphicon-cog" style="margin-right: 5px"></i> <label>Password Change</label></a></li>
						<!--  <li role="presentation"><a role="menuitem" tabindex="-1" onclick="rolePopup();" href="javascript:void(0);"> <i class="fa fa-exchange" style="margin-right: 4px"></i> <label>Switch Role</label></a></li> 
						<li role="presentation"><a role="menuitem" tabindex="-1" onclick="stationeryPopup();" href="javascript:void(0);"> <i class="fa fa-exchange" style="margin-right: 4px"></i> <label>Switch Stationery</label></a></li> -->
						<li role="presentation" class="divider"></li>
					</ul></li>
					
				<li><a class="navbar-icon" href="#" data-toggle="dropdown"><i class="fa fa-power-off" style=" color: #ffffff;font-size: 24px;" id="userSettings" alt="UserSettings"></i></a>
					<ul class="dropdown-menu" style="margin-top: 10px;" role="menu" aria-labelledby="dropdownMenu1">
						<li role="presentation"><a role="menuitem" tabindex="-1" href="Renderer/common/elogout.jsp"> <i class="fa fa-sign-out" style="margin-right: 5px"></i> <label>Logout</label></a></li>
					</ul></li>
			</ul>
		</nav>
	</div>
<!-- 
	<c:if test="${requestScope['RENDERER_TYPE'] eq '1'}">
		<div class="container">
			<nav class="collapse navbar-collapse bs-navbar-collapse" style="position:fixed;right:10px;top:60px;"  role="navigation">
								<ul class="nav navbar-nav navbar-right date-time">
					<li><b> <web:message var="common" key="header.currentlogin" />
					</b></li>
					<li style="margin-right: 20px;"><b class="value"> &nbsp;<%=(java.util.Date) session.getAttribute("_LDT") == null ? "" : patterns.config.framework.web.FormatUtils.getDate((java.util.Date) session.getAttribute("_LDT"), "dd/MM/yyyy HH:mm:ss")%>
					</b></li>
					<li><b> <web:message var="common" key="header.cbd" />
					</b></li>
					<li><b class="value"> &nbsp;<%=(java.util.Date) session.getAttribute("_SDT") == null ? "" : patterns.config.framework.web.FormatUtils.getDate((java.util.Date) session.getAttribute("_SDT"), "dd/MM/yyyy HH:mm:ss")%>
					</b></li>
				</ul>
			</nav>
		</div>
	</c:if>
	 -->
</c:if>