<%@tag body-content="scriptless"  %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@attribute name="styleClass" required="false"%>
<%@attribute name="markboundary" required="false"%>
<div class="category_block block_wrap ${styleClass} ${markboundary}">
	<jsp:doBody />
</div>
	