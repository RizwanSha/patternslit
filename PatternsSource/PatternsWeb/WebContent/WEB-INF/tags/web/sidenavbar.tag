<%@tag body-content="scriptless"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:if test="${(requestScope['RENDERER_TYPE'] eq '1' or requestScope['RENDERER_TYPE'] eq '3' or requestScope['RENDERER_TYPE'] eq '5') and param.callSource ne 'C'}">
	<div class="overlay" id="overlay" onclick="overlayPage();"></div>
	<div class="loader hide-content">
		<img class="animate-loader" src="public/styles/images/loader.png" alt="loader">
	</div>
	<div class="side-navbar" role="navigation">
		<ul>
			<li><a href="">
					<div class="side-navbar-left-content">
						<i class="fa fa-tasks"></i> <label>Workbench</label>
					</div>
			</a></li>
			<li><a href="">
					<div class="side-navbar-left-content">
						<i class="fa fa-envelope"></i> <label>Notifications</label>
					</div>
					<div class="side-navbar-right-content">
						<span class="bubble">5</span>
					</div>
			</a></li>
			<li><a id="profile">
					<div class="side-navbar-left-content">
						<i class="glyphicon glyphicon-user"></i> <label style="margin-right: 0;">Profile</label>
					</div> <!-- <div class="side-navbar-right-content">
						<span><i class="fa fa-sort-down"></i></span>
					</div> -->
			</a>
				<ul class="list-unstyled">
					<li><a href="Renderer/common/epwdchgn.jsp"> <i class="glyphicon glyphicon-cog"></i> <label>Password Change</label>
					</a></li>
					<li><a href=""> <i class="fa fa-user"></i> <label>User Profile</label>
					</a></li>
					<li><a href=""> <i class="fa fa-exchange"></i> <label>Switch Role</label>
					</a></li>
					<li><a href=""> <i class="fa fa-list-ul"></i> <label>Activity Log</label>
					</a></li>
				</ul></li>
		</ul>
		<div class="logout-div">
			<a href="Renderer/common/elogout.jsp">
				<div>
					<i class="fa fa-sign-out"></i> <label>Logout</label>
				</div>
			</a>
		</div>
	</div>
</c:if>