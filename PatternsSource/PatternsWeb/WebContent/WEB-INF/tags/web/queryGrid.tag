<%@tag body-content="empty"%>
<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web"%>
<web:viewPart>
	<web:viewSubPart>
		<web:subProgramTitle var="program"
			key="${requestScope['_PIDL']}.querytitle" collapsable="po_view2"
			status="close" descReqd="true" />
	</web:viewSubPart>
	<web:viewSubPart>
		<web:viewContent id="po_view2" styleClass="hidden">
			<web:dividerBlock>
				<web:sectionBlock width="width-100p" align="left">
					<web:section>
						<div id="filters" class="div_wrap form-horizontal form-group-sm"></div>
					</web:section>
					<web:section>
						<table width="100%" cellspacing="0" cellpadding="0">
							<tr>
								<td>
									<div id="queryGrid_div"
										style="height: 370px; width: 100%; background-color: transparent; z-index: 202; overflow: hidden"></div>
								</td>
							</tr>
							<tr>
								<td id="queryGrid_PA"></td>
							</tr>
							<tr>
								<td>
									<div id="queryGrid_ST"
										style="display: none; font-size: 12px; color: red; overflow: hidden"></div>
								</td>
							</tr>
						</table>
					</web:section>
				</web:sectionBlock>
			</web:dividerBlock>
		</web:viewContent>
	</web:viewSubPart>
</web:viewPart>