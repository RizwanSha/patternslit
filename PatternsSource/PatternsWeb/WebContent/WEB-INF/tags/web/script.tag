<%@tag body-content="empty"%>
<%@taglib prefix="resource" tagdir="/WEB-INF/tags/resource"%>
<%@attribute name="src" required="true"%>
<resource:script src="${src}" />
