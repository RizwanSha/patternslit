<%@tag body-content="scriptless"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:if test="${requestScope['RENDERER_TYPE'] eq '2'}">
	<div class="main-content container" style="*overflow-y: hidden;">
		<jsp:doBody />
	</div>
</c:if>

<c:if test="${requestScope['RENDERER_TYPE'] ne '2'}">
	<div class="main-content container">
		<jsp:doBody />
	</div>
</c:if>