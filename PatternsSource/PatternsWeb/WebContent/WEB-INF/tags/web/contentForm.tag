<%@tag body-content="scriptless"%>
<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="content-holder">
	<c:if test="${requestScope['RENDERER_TYPE'] eq '1' and param.callSource ne 'C'}">
		<web:menu />
		<div class="form-container"><jsp:doBody /></div>
	</c:if>
	
	<c:if test="${requestScope['RENDERER_TYPE'] ne '1' or param.callSource eq 'C'}">
		<div class="form-container"><jsp:doBody />
		</div>
	</c:if>
</div>