<%@tag body-content="empty"  %>
<%@attribute name="property" required="true"%>
<%@attribute name="id" required="true" rtexprvalue="true"%>
<input type='text' id="${id}_curr" maxlength="3" size="1" readonly="readonly" class="text" />
<input type='text' id="${id}Format" maxlength="25" size="18" readonly="readonly" class="text textalign-right" />
<input type='hidden' id="${id}"/>