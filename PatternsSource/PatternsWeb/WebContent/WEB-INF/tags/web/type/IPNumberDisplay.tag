<%@tag body-content="empty"  %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@attribute name="property" required="true"%>
<%@attribute name="id" required="true" rtexprvalue="true"%>
<%@attribute name="descRequired" required="false" type="java.lang.Boolean"%>
<input type='text' id="${id}" maxlength="4" size="2" readonly="readonly" class="text" />
<input type='text' id="${id}Serial" maxlength="8" size="6" readonly="readonly" class="text" />
<c:choose>
	<c:when test="${descRequired eq true}">
		<span id="${id}_desc" class="level4_description"> </span>
	</c:when>
</c:choose>