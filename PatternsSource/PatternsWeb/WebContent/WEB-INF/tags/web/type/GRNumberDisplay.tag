<%@tag body-content="empty"  %>
<%@attribute name="property" required="true"%>
<%@attribute name="id" required="true" rtexprvalue="true"%>
<input type='text' id="${id}" maxlength="2" size="2" readonly="readonly" class="text" />
<input type='text' id="${id}Year" maxlength="4" size="2" readonly="readonly" class="text" />
<input type='text' id="${id}Serial" maxlength="8" size="6" readonly="readonly" class="text" />
<span id="${id}_desc" class="level4_description"> </span>