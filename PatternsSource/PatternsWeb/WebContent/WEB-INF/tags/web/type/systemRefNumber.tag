<%@tag body-content="empty"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@attribute name="property" required="true"%>
<%@attribute name="id" required="true" rtexprvalue="true"%>
<%@attribute name="readOnly" required="false" type="java.lang.Boolean"%>
<%@attribute name="descRequired" required="false" type="java.lang.Boolean"%>
<c:choose>
	<c:when test="${readOnly eq true}">
		<html:text styleClass="date" styleId="${id}Date" property="${property}Date" maxlength="10" size="14" readonly="true" />
		<html:text styleClass="text" styleId="${id}BatchNo" property="${property}BatchNo" maxlength="4" size="4" readonly="true" />
		<html:text styleClass="text" styleId="${id}Serial" property="${property}Serial" maxlength="12" size="12" readonly="true" />
	</c:when>
	<c:otherwise>
		<html:text styleClass="date" styleId="${id}Date" property="${property}Date" maxlength="10" size="14" onblur="doblur(event,this)" onfocus="dofocus(event,this)" />
		<input type="image" class="calendar pic" src="public/styles/images/date.png" id="${id}_pic" onclick="showCalendar(this,event,false);return false" />
		<html:text styleClass="text" styleId="${id}BatchNo" property="${property}BatchNo" maxlength="4" size="4" onblur="doblur(event,this)" onfocus="dofocus(event,this)" onkeyup="dokeyup(event,this)" />
		<html:text styleClass="text" styleId="${id}Serial" property="${property}Serial" maxlength="12" size="12" onblur="doblur(event,this)" onfocus="dofocus(event,this)" onkeyup="dokeyup(event,this)" />
	</c:otherwise>
</c:choose>
<span id="${id}_error" class="level4_error"><html:errors property="${property}" /></span>
<script type="text/javascript">
$('#${id}Date').attr('placeholder', SCRIPT_DATE_FORMAT);
</script>