
<%@tag body-content="empty"%>
<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div style="font-size: 16px">
	<div class="container nav navbar-nav navbar-right">
		<b><web:message var="common" key="form.logintime" /></b>
		<span style="color: #2f7698; font-weight: bold;" id="loginTime"><%=request.getAttribute("loginDate") %></span><br> <br>
		<b><web:message var="common" key="form.logouttime" /></b>
		<span style="color: #2f7698; font-weight: bold;" id="logoutTime"><%=request.getAttribute("logoutDate") %></span><br> <br>
	</div>
</div>
