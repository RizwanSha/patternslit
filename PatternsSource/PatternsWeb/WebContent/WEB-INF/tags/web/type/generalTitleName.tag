<%@tag body-content="empty"  %>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@attribute name="property" required="true"%>
<%@attribute name="id" required="true"%>
<%@attribute name="datasourceid_title" required="true"%>
<%@attribute name="readOnly" required="false" type="java.lang.Boolean"%>
<html:select styleClass="text" styleId="${property}_title" property="${property}_title" onfocus="dofocus(event,this)" onblur="doblur(event,this)" onchange="dochange(event,this)">
	<html:optionsCollection name="${datasourceid_title}" value="id" label="label" />
</html:select>
<html:text styleId="${property}" property="${property}" maxlength="100" size="60" onblur="doblur(event,this)" onfocus="dofocus(event,this)" styleClass="text" />
<span id="${id}_error" class="level4_error"> <html:errors property="${property}" /></span>