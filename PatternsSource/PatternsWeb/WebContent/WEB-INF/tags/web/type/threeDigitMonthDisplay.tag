<%@tag body-content="empty"%>
<%@attribute name="property" required="true"%>
<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@attribute name="id" required="true" rtexprvalue="true"%>
<input type='text' class="text" id="${id}" maxlength="3" readonly="readonly" size="3" />
<span class="level4_description"> <web:legend key="form.month" var="common" />
</span>
