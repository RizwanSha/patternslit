<%@tag body-content="empty"%>
<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:section>
	<web:viewTitle var="common" key="form.membersection" />
	<web:table>
		<web:columnGroup>
			<web:columnStyle width="180px" />
			<web:columnStyle />
		</web:columnGroup>
		<web:rowOdd>
			<web:column>
				<web:legend key="form.membername" var="common" />
			</web:column>
			<web:column>
				<type:memberDisplay property="member" id="member" />
			</web:column>
		</web:rowOdd>
		<web:rowEven>
			<web:column>
				<web:legend key="form.dateofregistration" var="common" />
			</web:column>
			<web:column>
				<type:dateDisplay property="dateOfReg" id="dateOfReg" />
			</web:column>
		</web:rowEven>
		<web:rowOdd>
			<web:column>
				<web:legend key="form.membertype" var="common" />
			</web:column>
			<web:column>
				<type:codeDisplay property="memberType" id="memberType" />
			</web:column>
		</web:rowOdd>
		<web:rowEven>
			<web:column>
				<web:legend key="form.gender" var="common" />
			</web:column>
			<web:column>
				<type:comboDisplay property="gender" id="gender" datasourceid="COMMON_SEX" />
			</web:column>
		</web:rowEven>
		<web:rowOdd>
			<web:column>
				<web:legend key="form.dob" var="common" />
			</web:column>
			<web:column>
				<type:dateDisplay property="dateOfBirth" id="dateOfBirth" />
			</web:column>
		</web:rowOdd>
	</web:table>
</web:section>