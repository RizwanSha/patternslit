<%@tag body-content="empty"  %>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@attribute name="property" required="true"%>
<%@attribute name="id" required="true" rtexprvalue="true"%>
<%@attribute name="readOnly" required="false" type="java.lang.Boolean"%>
<c:choose>
	<c:when test="${readOnly eq true}">
		<html:textarea rows="24" cols="71" style="font-family:Courier New" styleId="${id}" readonly="true" property="${property}" onblur="doblur(event,this)" onfocus="dofocus(event,this)" styleClass="text" />
	</c:when>
	<c:otherwise>
		<html:textarea rows="24" cols="71"  style="font-family:Courier New" styleId="${id}" property="${property}" onblur="doblur(event,this)" onfocus="dofocus(event,this)" styleClass="text" />
	</c:otherwise>
</c:choose>
<span id="${id}_error" class="level4_error"> <html:errors property="${property}" /> </span>