<%@tag body-content="empty"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@attribute name="id" required="true"%>
<div class="cash"><span style="float: left;color:  #4682B4;font-size: 13px">Cash in Hand</span><br> <span style="float: right; font-size: 22px; font-weight: bold"> <span id="${id}_cashAmnt"></span>&nbsp;<span id="${id}_cashCurr" style="float: bottom; font-size: 12px; font-weight: normal"></span></span></div>
<div class="cash"><span style="float: left;color:  #4682B4;font-size: 13px">Payments in Pipeline</span><br> <span style="float: right; font-size: 22px; font-weight: bold"> <span id="${id}_paymAmnt"></span>&nbsp;<span id="${id}_paymCurr" style="float: bottom; font-size: 12px; font-weight: normal"></span></span></div>
<div class="cash"><span style="float: left;color:  #4682B4;font-size: 13px">Receipts in Pipeline</span><br> <span style="float: right; font-size: 22px; font-weight: bold"> <span id="${id}_recptAmnt"></span>&nbsp;<span id="${id}_recptCurr" style="float: bottom; font-size: 12px; font-weight: normal"></span></span></div>
<div class="cash"><span style="float: left;color:  #4682B4;font-size: 13px">Effective Cash in Hand</span><br> <span style="float: right; font-size: 22px; font-weight: bold"> <span id="${id}_effCashAmnt"></span>&nbsp;<span id="${id}_effCashCurr" style="float: bottom; font-size: 12px; font-weight: normal"></span></span></div>
