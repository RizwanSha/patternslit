<%@tag body-content="empty"  %>
<%@attribute name="property" required="true"%>
<%@attribute name="id" required="true" rtexprvalue="true"%>
<%@attribute name="cols" required="true" rtexprvalue="true"%>
<%@attribute name="rows" required="true" rtexprvalue="true"%>
<textarea rows="${rows}" cols="${cols}" id="${id}" readonly="readonly" class="text"></textarea>
