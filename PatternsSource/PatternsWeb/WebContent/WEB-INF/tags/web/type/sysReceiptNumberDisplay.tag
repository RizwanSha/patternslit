<%@tag body-content="empty"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<%@attribute name="property" required="true"%>
<%@attribute name="id" required="true" rtexprvalue="true"%>
<input type='text' id="${id}Type" maxlength="12" size="15" readonly="readonly" class="code" />
<input type='text' id="${id}Year" maxlength="4" size="2" readonly="readonly" class="text" />
<input type='text' id="${id}" maxlength="8" size="6" readonly="readonly" class="text" />
