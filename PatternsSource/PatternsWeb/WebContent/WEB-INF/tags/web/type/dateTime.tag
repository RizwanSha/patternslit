<%@tag body-content="empty"  %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@attribute name="property" required="true"%>
<%@attribute name="id" required="true" rtexprvalue="true"%>
<%@attribute name="readOnly" required="false" type="java.lang.Boolean"%>
<%@attribute name="lookup" required="false" type="java.lang.Boolean"%>
<c:choose>
	<c:when test="${readOnly eq true}">
		<html:text styleId="${id}" property="${property}" maxlength="16" size="24" readonly="true" styleClass="date" />
	</c:when>
	<c:otherwise>
		<html:text styleId="${id}" property="${property}" maxlength="16" size="24" onblur="doblur(event,this)" onfocus="dofocus(event,this)" styleClass="date" />
		<input type="image" class="calendar pic" src="public/styles/images/datetime.png" id="${id}_pic" onclick="showCalendar(this,event,true);return false" />
		<c:choose>
			<c:when test="${lookup eq true}">
				<input type="image" class="help pic" src="public/styles/images/search.png" id="${id}_look" onclick="showHelp('${id}',event);return false" />
			</c:when>
		</c:choose>	
	</c:otherwise>
</c:choose>
<span id="${id}_error" class="level4_error"> <html:errors property="${property}" /> </span>