<%@tag body-content="empty" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<%@attribute name="property" required="true"%>
<%@attribute name="id" required="true" rtexprvalue="true"%>
<%@attribute name="readOnly" required="false" type="java.lang.Boolean"%>
<%@attribute name="datasourceid" required="false"%>
<type:comboDisplay property="${id}Ward" id="${id}Ward"  errorNotRequired="true"/>
<type:comboDisplay property="${id}Room" id="${id}Room" errorNotRequired="true"/>
<type:comboDisplay property="${id}Bed" id="${id}Bed" errorNotRequired="true"/>
<span id="${id}_desc" class="level4_description"> </span>