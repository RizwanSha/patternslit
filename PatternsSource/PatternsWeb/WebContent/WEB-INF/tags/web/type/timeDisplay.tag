<%@tag body-content="empty"  %>
<%@attribute name="property" required="true"%>
<%@attribute name="id" required="true" rtexprvalue="true"%>
<input type="text" id="${id}" maxlength="5" size="6" readonly="readonly" class="time" />
<!-- Time picker -->
<!-- 
	<input type="image" class="calendar pic" src="public/styles/images/calendar.gif" id="${id}_pic" style="cursor: default" onclick="return false" disabled="disabled" />
-->
<script type="text/javascript">
$('#${id}').attr('placeholder', SCRIPT_TIME_FORMAT);
</script>