<%@tag body-content="empty"%>
<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<div>
	<table border="1" style="border-color:black;background-color: #ffffcc" >
		<tr>
			<td style="padding-top: 0px; padding-bottom: 0px;"><web:legend key="form.cashinhand" var="common" /></td>
			<td class="text-center" style="padding-bottom: 0px;"><span id="cashonhandCurr" style="font-size: 12px;" class="level4_description"></span></td>
			<td class="text-right" style="padding-bottom: 0px;"><span id="cashonhand" style="font-size: 12px;" class="level4_description"></span></td>
		</tr>
		<tr>
			<td style="padding-top: 0px; padding-bottom: 0px;"><web:legend key="form.receiveamount" var="common" /></td>
			<td class="text-center" style="padding-bottom: 0px;"><span id="receiveamountCurr" style="font-size: 12px;" class="level4_description"></span></td>
			<td class="text-right" style="padding-bottom: 0px;"><span id="receiveamount" style="font-size: 12px;" class="level4_description"></span></td>
		</tr>
		<tr>
			<td style="padding-top: 0px; padding-bottom: 0px;"><web:legend key="form.netamount" var="common" /></td>
			<td class="text-center" style="padding-bottom: 0px;"><span id="netamountCurr" style="font-size: 12px;" class="level4_description"></span></td>
			<td class="text-right" style="padding-bottom: 0px;"><span id="netamount" style="font-size: 12px;" class="level4_description"></span></td>
		</tr>
													
	</table>
</div>