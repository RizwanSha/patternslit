<%@tag body-content="empty"  %>
<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@attribute name="id" required="true" rtexprvalue="true"%>
<%@attribute name="key" required="true" %>
<%@attribute name="var" required="true" rtexprvalue="true"%>
<web:message var="${var}" key="${key}" draw="draw"/>
<%
	String value=(String)request.getAttribute("message");
%>
<input type="button" id="${id}" name="${id}" class="action-button" value='<%=value %>'/>
