<%@tag body-content="empty"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@attribute name="property" required="true"%>
<%@attribute name="id" required="true" rtexprvalue="true"%>
<%@attribute name="readOnly" required="false" type="java.lang.Boolean"%>
<web:section>
	<web:table>
		<web:columnGroup>
			<web:columnStyle width="180px" />
			<web:columnStyle />
		</web:columnGroup>
		<web:rowOdd>
			<web:column>
				<web:legend key="form.${id}" var="common" mandatory="true" />
			</web:column>
			<web:column>
				<c:choose>
					<c:when test="${readOnly eq true}">
						<type:dynamicDescriptionDisplay size="50" length="50" property="${id}Line1" id="${id}Line1" />
					</c:when>
					<c:otherwise>
						<type:dynamicDescription size="50" length="50" property="${id}Line1" id="${id}Line1" />
					</c:otherwise>
				</c:choose>
			</web:column>
		</web:rowOdd>
		<web:rowEven>
			<web:column />
			<web:column>
				<c:choose>
					<c:when test="${readOnly eq true}">
						<type:dynamicDescriptionDisplay size="50" length="50" property="${id}Line2" id="${id}Line2" />
					</c:when>
					<c:otherwise>
						<type:dynamicDescription size="50" length="50" property="${id}Line2" id="${id}Line2" />
					</c:otherwise>
				</c:choose>
			</web:column>
		</web:rowEven>
		<web:rowOdd>
			<web:column />
			<web:column>
				<c:choose>
					<c:when test="${readOnly eq true}">
						<type:dynamicDescriptionDisplay size="50" length="50" property="${id}Line3" id="${id}Line3" />
					</c:when>
					<c:otherwise>
						<type:dynamicDescription size="50" length="50" property="${id}Line3" id="${id}Line3" />
					</c:otherwise>
				</c:choose>
			</web:column>
		</web:rowOdd>
		<web:rowEven>
			<web:column />
			<web:column>
				<c:choose>
					<c:when test="${readOnly eq true}">
						<type:dynamicDescriptionDisplay size="50" length="50" property="${id}Line4" id="${id}Line4" />
					</c:when>
					<c:otherwise>
						<type:dynamicDescription size="50" length="50" property="${id}Line4" id="${id}Line4" />
					</c:otherwise>
				</c:choose>
			</web:column>
		</web:rowEven>
		<web:rowOdd>
			<web:column />
			<web:column>
				<c:choose>
					<c:when test="${readOnly eq true}">
						<type:dynamicDescriptionDisplay size="50" length="50" property="${id}Line5" id="${id}Line5" />
					</c:when>
					<c:otherwise>
						<type:dynamicDescription size="50" length="50" property="${id}Line5" id="${id}Line5" />
					</c:otherwise>
				</c:choose>
			</web:column>
		</web:rowOdd>
		<web:rowOdd>
			<web:column>
				<web:legend key="form.pincode" var="common" mandatory="true" />
			</web:column>
			<web:column>
				<c:choose>
					<c:when test="${readOnly eq true}">
						<type:dynamicDescriptionDisplay property="${id}PinCode" id="${id}PinCode" length="10" size="10" />
					</c:when>
					<c:otherwise>
						<type:dynamicDescription property="${id}PinCode" id="${id}PinCode" length="10" size="10" />
					</c:otherwise>
				</c:choose>
			</web:column>
		</web:rowOdd>
		<web:rowEven>
			<web:column>
				<web:legend key="form.location" var="common" mandatory="true" />
			</web:column>
			<web:column>
				<c:choose>
					<c:when test="${readOnly eq true}">
						<type:codeDisplay property="${id}Location" id="${id}Location" />
					</c:when>
					<c:otherwise>
						<type:code property="${id}Location" id="${id}Location" />
					</c:otherwise>
				</c:choose>
			</web:column>
		</web:rowEven>
		<web:rowOdd>
			<web:column>
				<web:legend key="form.country" var="common" mandatory="true" />
			</web:column>
			<web:column>
				<c:choose>
					<c:when test="${readOnly eq true}">
						<type:codeDisplay property="${id}Country" id="${id}Country" />
					</c:when>
					<c:otherwise>
						<type:code property="${id}Country" id="${id}Country" />
					</c:otherwise>
				</c:choose>
			</web:column>
		</web:rowOdd>
	</web:table>
</web:section>