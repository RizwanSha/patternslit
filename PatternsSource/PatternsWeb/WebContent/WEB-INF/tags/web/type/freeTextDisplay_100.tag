<%@tag body-content="empty" description="Encapsulates the body content"%>
<%@attribute name="property" required="true"%>
<%@attribute name="id" required="true" rtexprvalue="true"%>
<input type='text' class="code" id="${id}" maxlength="100" size="100" readonly="readonly" />
<span id="${id}_desc" class="level4_description"> </span>
