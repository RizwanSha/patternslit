<%@tag body-content="empty"  %>
<%@attribute name="property" required="true"%>
<%@attribute name="id" required="true" rtexprvalue="true"%>
<textarea rows="10" cols="80" id="${id}" readonly="readonly" class="text"></textarea>
