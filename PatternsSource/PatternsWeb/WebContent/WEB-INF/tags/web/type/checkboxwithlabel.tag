<%@tag body-content="empty"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@attribute name="property" required="true"%>
<%@attribute name="id" required="true" rtexprvalue="true"%>
<%@attribute name="key" required="true"%>
<%@attribute name="var" required="true" rtexprvalue="true"%>
<html:checkbox styleId="${id}" onblur="doblur(event,this)" onfocus="dofocus(event,this)" property="${property}" onclick="docheckclick(event,this)" styleClass="checkbox" style="min-height:12px;height:12px;"/>
<label class="control-label "><web:message var="${var}" key="${key}" /></label>
<span id="${id}_error" class="level4_error"> <html:errors property="${property}" /></span>