<%@tag body-content="empty"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="resource" tagdir="/WEB-INF/tags/resource"%>
<web:dependencies>
	<web:script src="Renderer/cmn/epersonimagecapture.js" />
	<web:script src="public/cdn/vendor/webcam/webcam.js" />
</web:dependencies>
<web:viewContent id="imageCaptureView">
	<web:dividerBlock>
		<web:sectionBlock width="width-100p" align="left">
			<web:viewContent id="fileUpload">
				<web:section>
					<web:table>
						<web:columnGroup>
							<web:columnStyle width="80px" />
							<web:columnStyle />
						</web:columnGroup>
						<web:rowEven>
							<web:column>
								<web:legend key="form.choosefile" var="common" />
							</web:column>
							<web:column>
								<resource:fileUploadPhoto id="evault" width="400px" height="100px"
									initFileUpload="initFileUpload" beforeFileAdd="beforeFileAdd"
									afterFileUpload="afterFileUpload"
									afterFileUploadFail="afterFileUploadFail" filelimit="1" />
							</web:column>
						</web:rowEven>
					</web:table>
				</web:section>

				<web:section>
					<web:table>
						<web:rowOdd>
							<web:column>
								<web:legend key="form.image" var="common" />
							</web:column>
							<web:column>
								<img id="image_preview" style="width: 200px; height: 150px;" />
							</web:column>
						</web:rowOdd>
					</web:table>
				</web:section>
			</web:viewContent>
			<web:viewContent id="takeasnapshot" styleClass="hidden">
				<web:section>
					<web:table>
						<web:columnGroup>
							<web:columnStyle width="160px" />
							<web:columnStyle />
						</web:columnGroup>
						<web:rowEven>
							<web:column>
							</web:column>
							<web:column>
								<div id="my_camera" style="width: 200px; height: 150px;"></div>
								<img id="result_of_snap" style="width: 200px; height: 150px;" />
							</web:column>
						</web:rowEven>
					</web:table>
				</web:section>
				<web:section>
					<web:table>
						<web:columnGroup>
							<web:columnStyle width="200px" />
							<web:columnStyle width="25px" />
							<web:columnStyle />
						</web:columnGroup>
						<web:rowOdd>
							<web:column>
							</web:column>
							<web:column>
								<input type="image" id="cp" onclick="take_snapshot()"
									src="public/styles/images/slr-camera-xxl-2.png"
									style="width: 30px; height: 30px;" />
							</web:column>
							<web:column>
								<a href="javascript:void(take_snapshot())" id="linkId"></a>
							</web:column>
						</web:rowOdd>
					</web:table>
				</web:section>
			</web:viewContent>
			<web:section>
				<web:table>
					<web:rowEven>
						<web:column span="3">
							<web:button id="selected" key="form.ok" var="common"
								onclick="ResultBack()" />
							<web:button id="Cancelbutton" key="form.cancel" var="common"
								onclick="CancelFuction()" />
							<web:button id="capturefromcamera" key="form.capturefromcampera"
								var="common" onclick="changeCaptureMode() " />
						</web:column>
					</web:rowEven>
				</web:table>
			</web:section>
		</web:sectionBlock>
	</web:dividerBlock>
</web:viewContent>