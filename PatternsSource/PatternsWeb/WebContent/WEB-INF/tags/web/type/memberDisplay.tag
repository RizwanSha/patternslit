<%@tag body-content="empty"  %>
<%@attribute name="property" required="true"%>
<%@attribute name="id" required="true" rtexprvalue="true"%>
<input type='text' id="${id}Salutation" maxlength="2" size="2" readonly="readonly" class="text" />
<input type='text' id="${id}Name" maxlength="80" size="80" readonly="readonly" class="text" />
