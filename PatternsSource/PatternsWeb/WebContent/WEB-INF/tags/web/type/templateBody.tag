<%@tag body-content="empty" description="Encapsulates the body content" %>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@attribute name="property" required="true"%>
<%@attribute name="id" required="true" rtexprvalue="true"%>
<html:textarea rows="20" cols="50" styleId="${id}" property="${property}" onblur="doblur(event,this)" onfocus="dofocus(event,this)" styleClass="text" />
<span id="${id}_error" class="level4_error"> <html:errors property="${property}" /> </span>