<%@tag body-content="empty"%>
<%@attribute name="property" required="true"%>
<%@attribute name="id" required="true" rtexprvalue="true"%>
<input type='text' id="${id}" maxlength="128" readonly="readonly" size="60" class="text" />
