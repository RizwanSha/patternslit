<%@tag body-content="empty"  %>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@attribute name="property" required="true"%>
<%@attribute name="id" required="true" rtexprvalue="true"%>
<html:checkbox styleId="${id}" onblur="doblur(event,this)" onfocus="dofocus(event,this)" property="${property}" onclick="docheckclick(event,this)" styleClass="checkbox" />
<span id="${id}_desc" class="level4_description" style="bottom:10px;padding-left:5px;"> </span>
<span id="${id}_error" class="level4_error"> <html:errors property="${property}" /> </span>