<%@tag body-content="empty"  %>
<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@attribute name="property" required="true"%>
<%@attribute name="id" required="true" rtexprvalue="true"%>
<%@attribute name="key" required="true"%>
<%@attribute name="var" required="true" rtexprvalue="true"%>
<%@attribute name="mandatory" required="false" type="java.lang.Boolean"%>
<input type='checkbox' id="${id}" onblur="doblur(event,this)" onfocus="dofocus(event,this)" disabled="disabled" class="checkbox" style="min-height:12px;height:12px;"/>
<label class="control-label "><web:message var="${var}" key="${key}" />
<c:if test="${mandatory eq true}">
	<span style="color: red">*</span>
</c:if>
</label>