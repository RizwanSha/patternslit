<%@tag body-content="empty"  %>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@attribute name="property" required="true"%>
<%@attribute name="id" required="true" rtexprvalue="true"%>
<html:text styleId="${id}" property="${property}" maxlength="7" size="7" onblur="doblur(event,this)" onfocus="dofocus(event,this)" styleClass="text textalign-right"/>
<span id="${id}_error" class="level4_error"> <html:errors property="${property}" /> </span>
<script type="text/javascript">
$('#${id}').attr('placeholder', FOURDIGIT_TWODECIMAL);
</script>