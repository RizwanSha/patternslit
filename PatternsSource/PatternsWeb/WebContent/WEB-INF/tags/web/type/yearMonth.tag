<%@tag body-content="empty" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<%@attribute name="property" required="true"%>
<%@attribute name="id" required="true" rtexprvalue="true"%>
<%@attribute name="datasourceid" required="true"%>
<type:comboMonthList property="${id}" id="${id}" datasourceid="${datasourceid }"/>
<html:text styleClass="text" styleId="${id}Year" property="${property}Year" maxlength="4" size="2" onblur="doblur(event,this)" onfocus="dofocus(event,this)"  />
<span id="${id}_error" class="level4_error"> <html:errors property="${property}" /> </span>