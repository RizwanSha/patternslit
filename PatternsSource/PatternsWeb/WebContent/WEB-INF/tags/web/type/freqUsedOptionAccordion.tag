<%@tag import="patterns.config.framework.web.GenericOption"%>
<%@tag body-content="empty"%>
<%@tag
	import="patterns.config.framework.web.configuration.menu.MenuUtils"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<li>
	<h3 id="recentUsedh3" class="fa fa-chevron-right" style="cursor: pointer;">&nbsp;<span class="fa fa-clock-o"></span>Recently Used Programs</h3>
		<ul id="recentUsedUL">
		<%
			java.util.ArrayList<GenericOption> fuo = null;
			MenuUtils menu = new MenuUtils();
			fuo = menu.getFrequentlyUsedPrograms();
			int rowCount = 1;
			for (GenericOption option : fuo) {
				String id = option.getId();
				String label = option.getLabel();
				try {
					
		%>
		<li style="text-indent:5px"><web:linkrenderer forward="<%=id.toLowerCase()%>" label="<%=label%>" styleClass="" /></li>
		
		<%
				} catch (Exception e) {

				}
			}
		%>

	</ul>
	
	</li>

