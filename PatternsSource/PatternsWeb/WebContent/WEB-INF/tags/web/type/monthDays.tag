<%@tag body-content="empty" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@attribute name="property" required="true"%>
<%@attribute name="id" required="true" rtexprvalue="true"%>
<%@attribute name="readOnly" required="false" type="java.lang.Boolean"%>
<c:choose>
	<c:when test="${readOnly eq true}">
		<html:text styleClass="text" styleId="${id}_Months" property="${property}_Months" maxlength="2" size="2" onblur="doblur(event,this)" onfocus="dofocus(event,this)"  readonly="true"/>
		<span  class="level4_description">
			<web:legend key="form.month" var="common"  />
		</span>
		<html:text styleClass="text" styleId="${id}_Days" property="${property}_Days" maxlength="3" size="3" onblur="doblur(event,this)" onfocus="dofocus(event,this)"  readonly="true"/>
		<span  class="level4_description">
			<web:legend key="form.days" var="common"  />
		</span>
	</c:when>
	<c:otherwise>
		<html:text styleClass="text" styleId="${id}_Months" property="${property}_Months" maxlength="2" size="2" onblur="doblur(event,this)" onfocus="dofocus(event,this)"/>
		<span  class="level4_description">
			<web:legend key="form.month" var="common"  />
		</span>
		<html:text styleClass="text" styleId="${id}_Days" property="${property}_Days" maxlength="3" size="3" onblur="doblur(event,this)" onfocus="dofocus(event,this)"/>
		<span  class="level4_description">
			<web:legend key="form.days" var="common"  />
		</span>
	</c:otherwise>
</c:choose>
<span id="${id}_error" class="level4_error"> <html:errors property="${property}" /> </span>