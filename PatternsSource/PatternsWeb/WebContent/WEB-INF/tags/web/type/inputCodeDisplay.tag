<%@tag body-content="empty" %>
<%@attribute name="property" required="true"%>
<%@attribute name="id" required="true" rtexprvalue="true"%>
<input type='text' class="code" id="${id}" maxlength="12" size="15" readonly="readonly" />
<span id="${id}_desc" class="level4_description"> </span>
<span id="${id}_error" class="level4_error"> </span>