<%@tag body-content="empty"  %>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<%@attribute name="property" required="true"%>
<%@attribute name="id" required="true" rtexprvalue="true"%>
<type:comboBillTypeDisplay property="${id}BillType" id="${id}BillType" />
<input type='text' id="${id}BillYear" maxlength="4" size="2" readonly="readonly" class="text" />
<input type='text' id="${id}" maxlength="8" size="6" readonly="readonly" class="text" />
