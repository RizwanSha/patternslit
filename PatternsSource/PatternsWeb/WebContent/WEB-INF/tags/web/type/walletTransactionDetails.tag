<%@tag body-content="empty"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@attribute name="styleClass" required="true" rtexprvalue="false"%>
<%@attribute name="id" required="true"%>
<div class="textalign-center">
	<table align="center">
		<tr>
			<td colspan="2" style="border: 2px solid black;width: 350px;height: 30px;font-weight: 60px;font-size: large;">Transaction Success</td>
		</tr>
		<tr>
			<td style="border: 2px solid black;width: 80px;font-weight:20px;height: 25px;font-size: large;">Batch Date</td>
			<td id="${id}_1" style="border: 2px solid black; width:80px;font-weight:20px;height: 25px;font-size: large;"></td>
		</tr>
		<tr>
			<td style="border: 2px solid black; width:80px;font-weight:20px;height: 25px;font-size: large;">Batch Number</td>
			<td id="${id}_2" style="border: 2px solid black; width:80px;font-weight:20px;height: 25px;font-size: large;"></td>
		</tr>
	</table>
</div>