<%@tag body-content="empty"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@attribute name="property" required="true"%>
<%@attribute name="id" required="true" rtexprvalue="true"%>
<web:columnGroup>
	<web:columnStyle width="180px" />
	<web:columnStyle />
</web:columnGroup>
<web:column>
	<input type='text' id="${property}_relationship" maxlength="3" size="12" readonly="readonly" class="text" />
</web:column>
<web:column>
	<type:personNameDisplay property="person" id="person"></type:personNameDisplay>
</web:column>