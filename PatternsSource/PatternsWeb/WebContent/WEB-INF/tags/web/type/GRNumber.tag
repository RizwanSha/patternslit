<%@tag body-content="empty"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@attribute name="property" required="true"%>
<%@attribute name="id" required="true" rtexprvalue="true"%>
<%@attribute name="readOnly" required="false" type="java.lang.Boolean"%>
<html:text styleClass="code" styleId="${id}" property="${property}" maxlength="2" size="2" onblur="doblur(event,this)" onfocus="dofocus(event,this)" onkeyup="dokeyup(event,this)" />
<html:text styleClass="text" styleId="${id}Year" property="${property}Year" maxlength="4" size="2" value="<%=(String)session.getAttribute(patterns.config.framework.web.SessionConstants.FIN_YEAR)%>" onblur="doblur(event,this)" onfocus="dofocus(event,this)" onkeyup="dokeyup(event,this)"/>
<html:text styleClass="text" styleId="${id}Serial" property="${property}Serial" maxlength="8" size="6" onblur="doblur(event,this)" onfocus="dofocus(event,this)" onkeyup="dokeyup(event,this)" />
<input type="image" class="help pic" src="public/styles/images/search.png" id="${id}_pic" onclick="showHelp('${id}',event);return false" />
<span id="${id}_desc" class="level4_description"> </span>
<span id="${id}_error" class="level4_error"> <html:errors property="${property}" />
</span>