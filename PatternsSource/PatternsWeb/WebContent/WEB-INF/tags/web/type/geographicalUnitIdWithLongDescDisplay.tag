<%@tag body-content="empty" %>
<%@attribute name="property" required="true"%>
<%@attribute name="id" required="true" rtexprvalue="true"%>
<input type='text' class="code" id="${id}" maxlength="12" size="12" readonly="readonly" />
<textarea rows="4" cols="50" id="${id}_desc" readonly="readonly" class="text"></textarea>
