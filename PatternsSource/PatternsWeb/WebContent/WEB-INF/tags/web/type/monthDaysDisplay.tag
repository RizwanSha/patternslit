<%@tag body-content="empty"  %>
<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@attribute name="property" required="true"%>
<%@attribute name="id" required="true" rtexprvalue="true"%>
<input type='text' class="text" id="${id}_Months" maxlength="3" size="3" readonly="readonly"/>
<span  class="level4_description">
			<web:legend key="form.month" var="common"  />
</span>
<input type='text' class="text" id="${id}_Days" maxlength="2" size="2" readonly="readonly"/>
<span  class="level4_description">
			<web:legend key="form.days" var="common"  />
</span>
