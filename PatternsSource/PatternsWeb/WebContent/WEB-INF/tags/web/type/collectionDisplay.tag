<%@tag body-content="empty"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@attribute name="full" required="false" type="java.lang.Boolean"%>
<web:dependencies>
	<web:script src="Renderer/cmn/collection.js" />
</web:dependencies>
<web:viewContent id="view_collection">
	<web:dividerBlock>
		<web:sectionBlock width="width-100p" align="left">
			<web:section>
				<web:viewTitle key="form.paymentsection" var="common" />
				<web:table>
					<web:columnGroup>
						<web:columnStyle width="240px" />
						<web:columnStyle />
					</web:columnGroup>
					<web:rowEven>
						<web:column span="2">
							<web:grid height="200px" width="704px" id="collectionGrid" src="cmn/collection_innerGrid.xml">
							</web:grid>
						</web:column>
					</web:rowEven>
				</web:table>
			</web:section>
			<web:section>
				<web:table>
					<web:columnGroup>
						<web:columnStyle width="180px" />
						<web:columnStyle />
					</web:columnGroup>
					<web:rowOdd>
						<web:column>
							<web:legend key="form.cashreturned" var="common" />
						</web:column>
						<web:column>
							<type:currencyBigAmountDisplay property="cashReturned" id="cashReturned" />
						</web:column>
					</web:rowOdd>
					<c:choose>
						<c:when test="${full eq true}">
							<web:rowEven>
								<web:column>
									<web:legend key="form.netreceiptamt" var="common" mandatory="true" />
								</web:column>
								<web:column>
									<type:currencyBigAmountDisplay property="netReceiptAmt" id="netReceiptAmt" />
								</web:column>
							</web:rowEven>
						</c:when>
					</c:choose>
				</web:table>
			</web:section>
			<web:section>
				<web:table>
					<web:columnGroup>
						<web:columnStyle width="180px" />
						<web:columnStyle width="180px" />
						<web:columnStyle width="150px" />
						<web:columnStyle />
					</web:columnGroup>
					<web:rowOdd>
						<web:column>
							<web:legend key="form.manualrcomprecpt" var="common" mandatory="true" />
						</web:column>
						<web:column>
							<type:comboDisplay property="manualOrComputerReceipt" id="manualOrComputerReceipt" datasourceid="COMMON_RECEIPTTYPE" />
						</web:column>
						<web:column>
							<web:legend key="form.manualreceipt" var="common" />
						</web:column>
						<web:column>
							<type:receiptNumberDisplay property="manualReceipt" id="manualReceipt" />
						</web:column>
					</web:rowOdd>
				</web:table>
				<web:table>
				</web:table>
			</web:section>
			<web:viewContent id="paymentDetails" styleClass="hidden">
				<web:section>
					<web:table>
						<web:columnGroup>
							<web:columnStyle width="180px" />
							<web:columnStyle />
						</web:columnGroup>
						<web:rowOdd>
							<web:column>
								<web:legend key="form.paymentmode" var="common" mandatory="true" />
							</web:column>
							<web:column>
								<type:paymentModeDisplay property="paymentModeCodePaymentDetails" id="paymentModeCodePaymentDetails" />
							</web:column>
						</web:rowOdd>
						<web:rowEven>
							<web:column>
								<web:legend key="form.description" var="common" />
							</web:column>
							<web:column>
								<type:descriptionDisplay property="paymentModePaymentDetails" id="paymentModePaymentDetails" />
							</web:column>
						</web:rowEven>
						<web:rowOdd>
							<web:column>
								<web:legend key="form.paymentamount" var="common" mandatory="true" />
							</web:column>
							<web:column>
								<type:currencyBigAmountDisplay property="paymentAmountPaymentDetails" id="paymentAmountPaymentDetails" />
							</web:column>
						</web:rowOdd>
					</web:table>
				</web:section>
				<web:viewContent id="view_chequeDisplay" styleClass="hidden">
					<web:section>
						<web:table>
							<web:columnGroup>
								<web:columnStyle width="180px" />
								<web:columnStyle />
							</web:columnGroup>
							<web:rowOdd>
								<web:column>
									<web:legend key="form.chequepoddnumber" var="common" mandatory="true" />
								</web:column>
								<web:column>
									<type:chequeNumberDisplay property="chequePODDNumberPaymentDetails" id="chequePODDNumberPaymentDetails" />
								</web:column>
							</web:rowOdd>
							<web:rowEven>
								<web:column>
									<web:legend key="form.chequedated" var="common" mandatory="true" />
								</web:column>
								<web:column>
									<type:dateDisplay property="chequeDatedPaymentDetails" id="chequeDatedPaymentDetails" />
								</web:column>
							</web:rowEven>
							<web:rowOdd>
								<web:column>
									<web:legend key="form.bankcode" var="common" mandatory="true" />
								</web:column>
								<web:column>
									<type:bankCodeDisplay property="bankCodePaymentDetails" id="bankCodePaymentDetails" />
								</web:column>
							</web:rowOdd>
							<web:rowEven>
								<web:column>
									<web:legend key="form.branchcode" var="common" mandatory="true" />
								</web:column>
								<web:column>
									<type:branchDisplay property="branchCodePaymentDetails" id="branchCodePaymentDetails" />
								</web:column>
							</web:rowEven>
						</web:table>
					</web:section>
				</web:viewContent>
				<web:viewContent id="view_eftDisplay" styleClass="hidden">
					<web:section>
						<web:table>
							<web:columnGroup>
								<web:columnStyle width="180px" />
								<web:columnStyle />
							</web:columnGroup>
							<web:rowOdd>
								<web:column>
									<web:legend key="form.bankaccountcode" var="common" mandatory="true" />
								</web:column>
								<web:column>
									<type:bankAccountCodeDisplay property="bankAccountCodePaymentDetails" id="bankAccountCodePaymentDetails" />
								</web:column>
							</web:rowOdd>
							<web:rowEven>
								<web:column>
									<web:legend key="form.eftref" var="common" mandatory="true" />
								</web:column>
								<web:column>
									<type:otherInformation25Display property="eftRefPaymentDetails" id="eftRefPaymentDetails" />
								</web:column>
							</web:rowEven>
						</web:table>
					</web:section>
				</web:viewContent>
				<web:viewContent id="view_cardDisplay" styleClass="hidden">
					<web:section>
						<web:table>
							<web:columnGroup>
								<web:columnStyle width="180px" />
								<web:columnStyle />
							</web:columnGroup>
							<web:rowEven>
								<web:column>
									<web:legend key="form.authrsettlementref" var="common" mandatory="true" />
								</web:column>
								<web:column>
									<type:otherInformation25Display property="authSettlementRefPaymentDetails" id="authSettlementRefPaymentDetails" />
								</web:column>
							</web:rowEven>
						</web:table>
					</web:section>
				</web:viewContent>
				<web:section>
					<web:table>
						<web:columnGroup>
							<web:columnStyle width="180px" />
						</web:columnGroup>
						<web:rowEven>
							<web:column>
								<type:button key="form.ok" id="ok" var="common" onclick="closePaymentDetails()" />
							</web:column>
						</web:rowEven>
					</web:table>
				</web:section>
			</web:viewContent>
		</web:sectionBlock>
	</web:dividerBlock>
</web:viewContent>

