<%@tag body-content="empty" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@attribute name="property" required="true"%>
<html:hidden styleId="${property}Date"  property="${property}Date"/>
<html:hidden styleId="${property}Serial"  property="${property}Serial"/>

