<%@tag body-content="empty"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@attribute name="property" required="true"%>
<%@attribute name="id" required="true" rtexprvalue="true"%>
<%@attribute name="datasourceid_relationship" required="false"%>
	<html:select styleClass="text" styleId="${id}_relationship" property="${property}_relationship" onfocus="dofocus(event,this)" onblur="doblur(event,this)" onchange="dochange(event,this)">
		<c:choose>
			<c:when test="${datasourceid_relationship !=null}">
				<html:optionsCollection name="${datasourceid_relationship}" value="id" label="label" />
			</c:when>
		</c:choose>
	</html:select>
<web:column>
	<type:personNameDisplay property="person" id="person"></type:personNameDisplay>
</web:column>
