<%@tag body-content="empty"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@attribute name="property" required="true"%>
<%@attribute name="id" required="true" rtexprvalue="true"%>
<%@attribute name="readOnly" required="false" type="java.lang.Boolean"%>
<%@attribute name="alterAccNoSearchReq" required="false" type="java.lang.Boolean"%>
<%@attribute name="alterAccNoSearchFunc" required="false" rtexprvalue="true"%>
<%@attribute name="accBalViewReq" required="false" type="java.lang.Boolean"%>
<%@attribute name="accBalViewFunc" required="false" rtexprvalue="true"%>
<%@attribute name="qacntHolderimageViewReq" required="false" type="java.lang.Boolean"%>
<web:dependencies>
	<web:script src="Renderer/casa/accountNumber.js" />
</web:dependencies>
<c:choose>
	<c:when test="${readOnly eq true}">
		<html:text styleClass="text" styleId="${id}" property="${property}" maxlength="25" size="20" onblur="doblur(event,this)" onfocus="dofocus(event,this)" readonly="true" />
	</c:when>
	<c:otherwise>
		<html:text styleClass="text" styleId="${id}" property="${property}" maxlength="25" size="20" onblur="doblur(event,this)" onfocus="dofocus(event,this)" />
		<input type="image" class="help pic" src="public/styles/images/search.png" id="${id}_pic" onclick="showHelp('${id}',event);return false" />
		<c:choose>
			<c:when test="${alterAccNoSearchReq eq true}">
				<input type="image" class="help pic" src="public/styles/images/alterAccNoSearch.png" id="${id}_searchpic" onclick="doAccountAlterSearch('${id}');" />
			</c:when>
		</c:choose>
		<c:choose>	
			<c:when test="${accBalViewReq eq true}">
				<input type="image" class="help pic" src="public/styles/images/balView.png" id="${id}_balviewpic" onclick="viewAccBalance('${id}');" />
			</c:when>
		</c:choose>	
		<c:choose>	
			<c:when test="${qacntHolderimageViewReq eq true}">
				<input type="image" class="help pic" src="public/styles/images/photoView.png" id="${id}_Acntholderimageviewpic" onclick="viewQacntholderimageview('${id}');" />
			</c:when>
		</c:choose>	
	</c:otherwise>
</c:choose>
<span id="${id}_desc" class="level4_description"> </span>
<span id="${id}_error" class="level4_error"> <html:errors property="${property}" />
</span>