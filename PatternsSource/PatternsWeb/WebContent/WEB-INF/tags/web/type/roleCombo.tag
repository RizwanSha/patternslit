<%@tag body-content="empty"%>
<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<select class="text" id="cboRoleType" style="margin: 0 1em 2em 1em;">
	<%
		java.util.LinkedList<patterns.config.framework.web.GenericOption> roleList = null;
			roleList =(java.util.LinkedList<patterns.config.framework.web.GenericOption>)session.getAttribute(patterns.config.framework.web.SessionConstants.ROLE_LIST);
			for(patterns.config.framework.web.GenericOption option : roleList){
		String id = option.getId();
		String label = option.getLabel();
	%>
	<option value="<%=id%>"><%=label%></option>
	<%
		}
	%>
</select>

<html:form method="POST" action="/Renderer/common/eroleupdation" styleId="eroleupdation">
	<html:hidden styleId="hidRoleCode" property="hidRoleCode" />
	<web:element property="csrfValue" />
	<%--<input type="hidden" name="<csrf:token-name />" value="<csrf:token-value />" /> --%>
</html:form>

<script type="text/javascript">
	function updateRole() {
		$('#hidRoleCode').val($('#cboRoleType').val());
		$('#eroleupdation').submit();
		rolePopup();
	}
	$('#cboRoleType').val("<%=session.getAttribute(patterns.config.framework.web.SessionConstants.ROLE_CODE)%>");
</script>
