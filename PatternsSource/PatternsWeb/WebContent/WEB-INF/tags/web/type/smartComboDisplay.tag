<%@tag body-content="empty"%>
<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@attribute name="property" required="true"%>
<%@attribute name="id" required="true"%>
<%@attribute name="width" required="true"%>

<div id="${id}_div" style="width: ${width}px;"></div>
<web:elementDisplay property="${id}" />
<script type="text/javascript">
	${id} = new dhtmlXCombo('${id}_div', 'combo', ${width});
	${id}.disable(true);
</script>