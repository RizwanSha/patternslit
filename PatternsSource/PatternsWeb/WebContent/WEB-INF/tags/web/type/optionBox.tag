<%@tag body-content="empty"%>
<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="html" uri="http://struts.apache.org/tags-html"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<%@attribute name="add" required="true" type="java.lang.Boolean"%>
<%@attribute name="modify" required="true" type="java.lang.Boolean"%>
<%@attribute name="view" required="true" type="java.lang.Boolean"%>
<%@attribute name="template" required="false" type="java.lang.Boolean"%>
<div class="option-box textalign-left">
	<%
		String value = "";
	%>
	<c:if test="${(add eq true) and (requestScope['ADD_ALLOWED'] eq '1')}">
		<web:message var="common" key="form.add" draw="draw" />
		<%
			value = (String) request.getAttribute("message");
		%>
		<input type="button" id="add" name="add" class="btn btn-xs btn-default" accesskey="A" value='<%=value%>' onclick="doadd()" />
	</c:if>
	<c:if test="${(modify eq true) and (requestScope['MODIFY_ALLOWED'] eq '1')}">
		<web:message var="common" key="form.modify" draw="draw" />
		<%
			value = (String) request.getAttribute("message");
		%>
		<input type="button" id="modify" name="modify" class="btn btn-xs btn-default" accesskey="M" value='<%=value%>' onclick="domodify()" />
	</c:if>

	<c:if test="${(view eq true) and (requestScope['VIEW_ALLOWED'] eq '1')}">
		<web:message var="common" key="form.view" draw="draw" />
		<%
			value = (String) request.getAttribute("message");
		%>
		<input type="button" id="view" name="view" class="btn btn-xs btn-default" accesskey="V" value='<%=value%>' onclick="doqview()" />
	</c:if>
	<c:if test="${(template eq true) and (requestScope['TEMPLATE_ALLOWED'] eq '1')}">
		<web:message var="common" key="form.template" draw="draw" />
		<%
			value = (String) request.getAttribute("message");
		%>
		<input type="button" id="template" name="template" class="btn btn-xs btn-default" accesskey="T" value='<%=value%>' onclick="dotemplate()" />
	</c:if>
	<span id="spanMode" class="frm_mode"></span>
	<c:if test="${ (requestScope['COMMON_SCENARIOLIST'] !=null)}">
	<div class="scenario-combo" style="display: inline-block;float: right;margin: 0px 20px 0px;">
		<type:combo property="scenario" id="scenario" datasourceid="COMMON_SCENARIOLIST" errorNotRequired="true"/>
	</div>
	</c:if>
	 <span id="spanLoad" class="frm_loading"></span> <span id="${id}_error" class="level4_error"><html:errors property="genericError" /></span>
</div>
