<%@tag body-content="empty"%>
<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@attribute name="property" required="true"%>
<%@attribute name="id" required="true"%>
<%@attribute name="datasourceid" required="false"%>
<type:comboDisplay property="${property}_relation" id="${id}_relation" datasourceid="${datasourceid}" errorNotRequired="true" />
<input type='text' id="${property}" maxlength="100" size="75" readonly="readonly" class="text" />



