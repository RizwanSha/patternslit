<%@tag body-content="empty" description="Encapsulates the body content" %>
<%@attribute name="property" required="true"%>
<%@attribute name="id" required="true" rtexprvalue="true"%>
<textarea rows="2" cols="50" id="${id}" readonly="readonly" class="text"></textarea>
