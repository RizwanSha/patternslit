<%@tag body-content="empty"%>
<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:section>
	<web:table>
		<web:columnGroup>
			<web:columnStyle width="80px" />
			<web:columnStyle width="160px" />
			<web:columnStyle width="80px" />
			<web:columnStyle />
		</web:columnGroup>
		<web:rowEven>
			<web:column>
				<web:legend key="eteller.alterAccType" var="program" mandatory="true" />
			</web:column>
			<web:column>
				<type:pidCode property="alterAccCode" id="alterAccCode" />
			</web:column>
			<web:column>
				<web:legend key="eteller.alterAccNo" var="program" mandatory="true" />
			</web:column>
			<web:column>
				<type:pidNumber property="alterAccNo" id="alterAccNo" />
			</web:column>
		</web:rowEven>
	</web:table>
</web:section>
<web:section>
	<web:table>
		<web:columnGroup>
			<web:columnStyle width="180px" />
			<web:columnStyle />
		</web:columnGroup>
		<web:rowOdd>
			<web:column span="3">
				<web:grid height="200px" width="501px" id="accountNo_Grid" src="cash/eteller_accout_lookup.xml">
				</web:grid>
			</web:column>
		</web:rowOdd>
	</web:table>
</web:section>
<web:section>
	<web:table>
		<web:columnGroup>
			<web:columnStyle width="180px" />
			<web:columnStyle />
		</web:columnGroup>
		<web:rowEven>
			<web:column>
				<type:button var="common" onclick="resetAccNo()" key="form.close" id="reset" />
			</web:column>
		</web:rowEven>
	</web:table>
</web:section>
