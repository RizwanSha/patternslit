<%@tag body-content="empty"  %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@attribute name="property" required="true"%>
<%@attribute name="id" required="true" rtexprvalue="true"%>
<%@attribute name="descRequired" required="false" type="java.lang.Boolean"%>
<input type='text' id="${id}BatchNo" maxlength="10" size="10" readonly="readonly" class="text" />
<input type='text' id="${id}Serial" maxlength="10" size="10" readonly="readonly" class="text" />