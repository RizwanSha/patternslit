<%@tag body-content="empty"%>
<div class="smartSearchBox" style="width: 270px">
	<div class="smartSearchIcon">
		<i class="fa fa-search"></i>
	</div>
	<input type='text' class="smartSearchInput" placeholder="Search" id="smartNavigate" maxlength="40" />
</div>
<script type="text/javascript">
	var smartNavigate_ref = new AutoComplete("smartNavigate");
</script>
