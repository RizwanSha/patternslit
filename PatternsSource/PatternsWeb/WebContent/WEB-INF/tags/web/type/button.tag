<%@tag body-content="empty"%>
<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@attribute name="id" required="true" rtexprvalue="true"%>
<%@attribute name="key" required="true"%>
<%@attribute name="onclick" required="true" rtexprvalue="true"%>
<%@attribute name="var" required="true" rtexprvalue="true"%>
<%@attribute name="styleClass" required="false" rtexprvalue="true"%>
<%@attribute name="addLocale" required="false" type="java.lang.Boolean"%>
<web:message var="${var}" key="${key}" draw="draw" />
<%
	String value = (String) request.getAttribute("message");
%>
<c:if test="${addLocale eq true}">
	<web:message var="${var}_${requestScope['ADDITIONAL_LOCALE']}" key="${key}" draw="draw" />
	<%
		value = value + '(' + (String) request.getAttribute("message") + ')';
	%>
</c:if>

<input type="button" id="${id}" name="${id}" class="action-button ${styleClass}" value='<%=value %>' onclick="${onclick}" />
