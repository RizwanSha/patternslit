<%@tag body-content="empty" %>
<%@attribute name="property" required="true"%>
<%@attribute name="id" required="true" rtexprvalue="true"%>
<input type='text' class="code" id="${id}" maxlength="6" size="12" readonly="readonly" />
<span id="${id}_desc" class="level4_description"> </span>