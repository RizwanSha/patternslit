<%@tag body-content="empty"  %>
<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@attribute name="id" required="true" rtexprvalue="true"%>
<%@attribute name="width" required="true"%>
<%@attribute name="height" required="true"%>
<%@attribute name="url" required="true"%>

<input type="image" src="${url}" id="${id}" class="help pic" onclick="showHelp('${id}',event);return false"  style="width:${width};height:${height}"/>
	