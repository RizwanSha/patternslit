<%@tag body-content="empty"  %>
<%@attribute name="property" required="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@attribute name="id" required="true" rtexprvalue="true"%>
<input type='text' id="${id}Year" maxlength="4" size="2" readonly="readonly" class="text" />
<input type='text' id="${id}" maxlength="8" size="8" readonly="readonly" class="text" />