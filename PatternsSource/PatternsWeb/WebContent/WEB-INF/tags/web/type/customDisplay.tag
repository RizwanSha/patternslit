<%@tag body-content="empty"  %>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@attribute name="property" required="true" rtexprvalue="true"%>
<%@attribute name="id" required="true" rtexprvalue="true"%>
<%@attribute name="maxlength" required="true" rtexprvalue="true"%>
<%@attribute name="src" required="true" rtexprvalue="true"%>
<div class="input custom" id="${id}_div">
	<img class="icon" src="${src}"/>
	<input type='text' id="${id}" maxlength="${maxlength}" readonly="readonly"/>
</div>