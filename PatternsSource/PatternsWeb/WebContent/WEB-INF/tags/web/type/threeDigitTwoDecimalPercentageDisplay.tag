<%@tag body-content="empty"%>
<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@attribute name="property" required="true"%>
<%@attribute name="id" required="true" rtexprvalue="true"%>
<input type='text' class="text textalign-right" id="${id}" maxlength="6" size="6" readonly="readonly" />
<span class="level4_description"> <web:legend key="form.percentage" var="common" />
</span>
