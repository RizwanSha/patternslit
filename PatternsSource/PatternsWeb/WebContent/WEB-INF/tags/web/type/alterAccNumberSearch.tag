<%@tag body-content="empty"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:viewContent id="accountNoGrid" styleClass="hidden">
	<web:dividerBlock>
		<web:sectionBlock width="width-100p" align="left">
			<web:section>
				<web:table>
					<web:columnGroup>
						<web:columnStyle width="80px" />
						<web:columnStyle width="160px" />
						<web:columnStyle width="80px" />
						<web:columnStyle />
					</web:columnGroup>
					<web:rowEven>
						<web:column>
							<web:legend key="form.alterAccCode" var="common" mandatory="true" />
						</web:column>
						<web:column>
							<type:pidCode property="alterAccCode" id="alterAccCode" />
						</web:column>
						<web:column>
							<web:legend key="form.alterAccNo" var="common" mandatory="true" />
						</web:column>
						<web:column>
							<type:pidNumber property="alterAccNo" id="alterAccNo" />
						</web:column>
					</web:rowEven>
				</web:table>
			</web:section>
			<web:section>
				<web:table>
					<web:columnGroup>
						<web:columnStyle width="180px" />
						<web:columnStyle />
					</web:columnGroup>
					<web:rowOdd>
						<web:column span="3">
							<web:grid height="200px" width="501px" id="accountNo_Grid" src="reg/alterNumberSearch_lookup.xml">
							</web:grid>
						</web:column>
					</web:rowOdd>
				</web:table>
			</web:section>
			<web:section>
				<web:table>
					<web:columnGroup>
						<web:columnStyle width="180px" />
						<web:columnStyle />
					</web:columnGroup>
					<web:rowEven>
						<web:column align="center">
							<type:button var="common" onclick="resetAccNo()" key="form.close" id="resetAcc" />
						</web:column>
					</web:rowEven>
				</web:table>
			</web:section>
		</web:sectionBlock>
	</web:dividerBlock>
</web:viewContent>