<%@tag body-content="empty"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@attribute name="property" required="true"%>
<%@attribute name="id" required="true" rtexprvalue="true"%>
<%@attribute name="readOnly" required="false" type="java.lang.Boolean"%>
<c:choose>
	<c:when test="${readOnly eq true}">
		<html:text styleId="${id}Prefix" property="${property}Prefix" maxlength="12" size="12" styleClass="code" onblur="doblur(event,this)" onfocus="dofocus(event,this)" readonly="true" />
		<html:text styleId="${id}" property="${property}" maxlength="10" size="10" styleClass="code" onblur="doblur(event,this)" onfocus="dofocus(event,this)" readonly="true" />
	</c:when>
	<c:otherwise>
		<html:text styleId="${id}Prefix" property="${property}Prefix" maxlength="12" size="12" styleClass="code" onblur="doblur(event,this)" onfocus="dofocus(event,this)" />
		<html:text styleId="${id}" property="${property}" maxlength="10" size="10" styleClass="code" onblur="doblur(event,this)" onfocus="dofocus(event,this)" />
	</c:otherwise>
</c:choose>
<span id="${id}_error" class="level4_error"> <html:errors property="${property}" />
</span>
<script type="text/javascript">
$('#${id}Prefix').attr('placeholder', 'Prefix (if any)');
$('#${id}').attr('placeholder', 'Number');
</script>