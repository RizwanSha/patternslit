<%@tag body-content="empty"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<%
	String primaryKey = request.getParameter("pk");
	String pk[] = primaryKey.split("\\|");
	String div = "";
	for (int i = 1; i <= Integer.parseInt(pk[0]); i++) {
		if (i <= 4) {
			if (pk[1].equals("R"))
				div = "resHist" + i;
			else
				div = "ofcHist" + i;
%>

<type:_addressReg id="<%=div%>" counter="<%=String.valueOf(i)%>" />
<%
	}
	}
%>