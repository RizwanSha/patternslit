<%@tag body-content="empty"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@attribute name="property" required="true"%>
<%@attribute name="id" required="true" rtexprvalue="true"%>
<%-- <input type="password" id="${id}" name="${property}" maxlength="32" size="30" onblur="doblur(event,this)" onfocus="dofocus(event,this)" styleClass="password" ondrag="return false" ondrop="return false" oncopy="return false" onpaste="return false" /> --%>
<html:password property="${property}" styleId="${id}"  maxlength="32" size="30" onblur="doblur(event,this)" onfocus="dofocus(event,this)" styleClass="password"></html:password>
<span id="${id}_desc" class="level4_description"> </span>
<span id="${id}_error" class="level4_error"> <html:errors property="${property}" /></span>
