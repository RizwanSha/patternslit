<%@tag body-content="empty"  %>
<%@attribute name="property" required="true"%>
<%@attribute name="id" required="true" rtexprvalue="true"%>
<%@attribute name="length" required="true"%>
<%@attribute name="size" required="true"%>
<input type='text' id="${id}" maxlength="${length}" size="${size}" readonly="readonly" class="text" />
