<%@tag body-content="empty"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@attribute name="property" required="true"%>
<%@attribute name="id" required="true" rtexprvalue="true"%>
<%@attribute name="barchart" required="false"%>
<%@attribute name="piechart" required="false"%>
<%@attribute name="linechart" required="false"%>
<%@attribute name="areachart" required="false"%>
<%@attribute name="radarchart" required="false"%>
<%@attribute name="scatterchart" required="false"%>

<web:column>
	<web:viewContent id="${id}Label">
		<label class="control-label " id="${id}Msg"><web:message key="form.viewAs" var="common" /></label>
	</web:viewContent>
</web:column>
<web:column>
	<web:viewContent id="${id}Combo">
		<select class="text" id="${id}" onfocus="dofocus(event,this)" onblur="doblur(event,this)" onchange="dochange(event,this)">
			<option value="1">PDF</option>
			<c:if test="${barchart eq true}">
				<option value="2"><web:legend key="form.barchart" var="common" /></option>
			</c:if>
			<c:if test="${piechart eq true}">
				<option value="3"><web:legend key="form.piechart" var="common" /></option>
			</c:if>
			<c:if test="${linechart eq true}">
				<option value="4"><web:legend key="form.linechart" var="common" /></option>
			</c:if>
			<c:if test="${areachart eq true}">
				<option value="5"><web:legend key="form.areachart" var="common" /></option>
			</c:if>
			<c:if test="${radarchart eq true}">
				<option value="6"><web:legend key="form.radarchart" var="common" /></option>
			</c:if>
			<c:if test="${scatterchart eq true}">
				<option value="7"><web:legend key="form.scatterchart" var="common" /></option>
			</c:if>
		</select>
		<span id="${id}_error" class="level4_error"> <html:errors property="${property}" />
		</span>
	</web:viewContent>
</web:column>

