<%@tag body-content="empty"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@attribute name="property" required="true"%>
<%@attribute name="id" required="true" rtexprvalue="true"%>
<input type='text' id="${property}_relationship" maxlength="3" size="12" readonly="readonly" class="text" />
<type:personNameDisplay property="person" id="person"></type:personNameDisplay>
