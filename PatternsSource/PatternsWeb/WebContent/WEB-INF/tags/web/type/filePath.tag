<%@tag body-content="empty"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@attribute name="property" required="true"%>
<%@attribute name="id" required="true" rtexprvalue="true"%>
<%@attribute name="readOnly" required="false" type="java.lang.Boolean"%>
<div class="input text" id="${id}_div"><c:choose>
	<c:when test="${readOnly eq true}">
		<html:text styleId="${id}" property="${property}" size="60" maxlength="128" onblur="doblur(event,this)" onfocus="dofocus(event,this)" readonly="true" />
	</c:when>
	<c:otherwise>
		<html:text styleId="${id}" property="${property}" size="60" maxlength="128" onblur="doblur(event,this)" onfocus="dofocus(event,this)" />
	</c:otherwise>
</c:choose></div>
<span id="${id}_error" class="level4_error"><html:errors property="${property}" /></span>