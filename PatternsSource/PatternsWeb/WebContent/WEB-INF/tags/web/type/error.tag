<%@tag body-content="empty"  %>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@attribute name="property" required="true"%>
<span id="${property}_error" class="level4_error"> <html:errors property="${property}" /> </span>