<%@tag body-content="empty"%>
<%@attribute name="property" required="true"%>
<%@attribute name="size" required="true"%>
<%@attribute name="length" required="true"%>
<%@attribute name="id" required="true" rtexprvalue="true"%>
<input type='text' class="text textalign-right" id="${id}" maxlength="${length}" size="${size}" readonly="readonly" />
