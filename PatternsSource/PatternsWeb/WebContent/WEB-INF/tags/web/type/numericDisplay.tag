<%@tag body-content="empty"  %>
<%@attribute name="property" required="true"%>
<%@attribute name="id" required="true" rtexprvalue="true"%>
<div class="input number ${display}" id="${id}_div">
	<input type='text' class="text" id="${id}" maxlength="5" readonly="readonly"/>
</div>