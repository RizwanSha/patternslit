<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<%@tag body-content="empty"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<web:table>
	<web:columnGroup>
		<web:columnStyle width="180px" />
		<web:columnStyle width="180px" />
		<web:columnStyle width="180px" />
		<web:columnStyle />
	</web:columnGroup>
	<web:rowEven>
		<web:column>
			<web:legend key="form.landlinecontact" var="common" />
		</web:column>
		<web:column>
			<type:smartCombo property="landlineCountry" id="landlineCountry"
				width="180" />
		</web:column>
		<web:column>
			<type:number size="6" property="stdCode" id="stdCode" length="6" />
		</web:column>
		<web:column>
			<type:number size="10" length="10" property="landlineNumber"
				id="landlineNumber" />
		</web:column>
	</web:rowEven>
</web:table>