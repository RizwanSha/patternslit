<%@tag body-content="empty"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@attribute name="property" required="true"%>
<%@attribute name="id" required="true" rtexprvalue="true"%>
<%@attribute name="readOnly" required="false" type="java.lang.Boolean"%>
<%@attribute name="currEditable" required="false" type="java.lang.Boolean"%>
<c:choose>
	<c:when test="${currEditable eq true}">
		<html:text styleId="${id}_curr" property="${property}_curr" maxlength="3" size="3"  onblur="doblur(event,this)" onfocus="dofocus(event,this)" styleClass="code" readonly="false"/>
	</c:when>
	<c:otherwise>
		<html:text styleId="${id}_curr" property="${property}_curr" maxlength="3" size="3" value="<%=(String)session.getAttribute(patterns.config.framework.web.SessionConstants.BASE_CURRENCY)%>" onblur="doblur(event,this)" onfocus="dofocus(event,this)" styleClass="text" readonly="true"/>
	</c:otherwise>	
</c:choose>
<c:choose>
	<c:when test="${readOnly eq true}">
		<html:text styleId="${id}Format" property="${property}Format" maxlength="43" size="30" onblur="doblur(event,this)" onfocus="dofocus(event,this)" styleClass="amount" readonly="true"/>
	</c:when>
	<c:otherwise>
		<html:text styleId="${id}Format" property="${property}Format" maxlength="43" size="30" onblur="doblur(event,this)" onfocus="dofocus(event,this)" styleClass="amount" />
	</c:otherwise>
</c:choose>
<html:hidden styleId="${id}" property="${property}" />
<span id="${id}_error" class="level4_error"><html:errors property="${property}" /></span>