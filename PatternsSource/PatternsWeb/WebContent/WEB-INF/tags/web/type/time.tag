<%@tag body-content="empty"  %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@attribute name="property" required="true"%>
<%@attribute name="id" required="true" rtexprvalue="true"%>
<%@attribute name="readOnly" required="false" type="java.lang.Boolean"%>
<%@attribute name="lookup" required="false" type="java.lang.Boolean"%>
<c:choose>
	<c:when test="${readOnly eq true}">
		<html:text styleClass="time" styleId="${id}" property="${property}" maxlength="5" size="6" readonly="true" />
	</c:when>
	<c:otherwise>
		<html:text styleClass="time" styleId="${id}" property="${property}" maxlength="5" size="6" onblur="doblur(event,this)" onfocus="dofocus(event,this)" />
		<!-- Time picker -->
		<!--  
		<input type="image" class="calendar pic" src="public/styles/images/calendar.gif" id="${id}_pic" onclick="showCalendar(this,event);return false" />
		<c:choose>
			<c:when test="${lookup eq true}">
				<input type="image" class="help pic" src="public/styles/images/search.png" id="${id}_look" onclick="showHelp('${id}',event);return false" />
			</c:when>
		</c:choose>
		-->	
	</c:otherwise>
</c:choose>
<span id="${id}_error" class="level4_error"> <html:errors property="${property}" /> </span>
<script type="text/javascript">
$('#${id}').attr('placeholder', SCRIPT_TIME_FORMAT);
</script>
