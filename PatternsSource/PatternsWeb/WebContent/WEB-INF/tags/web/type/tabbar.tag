<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@attribute name="id" required="true"%>
<%@attribute name="width" required="true"%>
<%@attribute name="height" required="true"%>
<%@attribute name="required" required="true"%>
<%@attribute name="name" required="true"%>
<%@attribute name="selected" required="true"%>
<div id="${id}" style="${width} ${height}"></div>
<script type="text/javascript">
	${id} = new dhtmlXTabBar('${id}');
	${id}.attachEvent("onTabClick", function(idClicked){
		tabbarClicked(idClicked);
	});

	var selected='${selected}';

	var tabList='${name}';
	var tabs=tabList.split('$$');
	for(var i=0;i<tabs.length;i++){
		//Naresh added begin 
		hide('${id}_'+i);
		//Naresh added end
		if(i==selected){
			${id}.addTab('${id}_'+i, tabs[i],null, null, true);
		}else{
			${id}.addTab('${id}_'+i, tabs[i]);
		}
		${id}.tabs('${id}_'+i).attachObject('${id}_TAB_'+i);
	}
</script>