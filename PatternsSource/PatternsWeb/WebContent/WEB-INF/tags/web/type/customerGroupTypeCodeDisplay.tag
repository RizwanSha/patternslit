<%@tag body-content="empty"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<%@attribute name="id" required="true"%>
<%@attribute name="property" required="true"%>
<input type='text' id="${id}" maxlength="12" size="12" readonly="readonly" class="code" />
<span id="${id}_desc" class="level4_description"> </span>