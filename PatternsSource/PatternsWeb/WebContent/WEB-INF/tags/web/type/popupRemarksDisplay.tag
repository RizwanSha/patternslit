<%@tag body-content="empty"  %>
<%@attribute name="property" required="true"%>
<%@attribute name="id" required="true" rtexprvalue="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@attribute name="addLocale" required="false" type="java.lang.Boolean"%>
<a href="javascript:showPopUpRemarks();"> <label class="control-label" style="cursor:pointer;"><web:message key="form.viewRemarks" var="common" /></label></a>
<web:viewContent id="${id}_view" styleClass="hidden">
	<web:section>
		<web:table>
			<web:columnGroup>
				<web:columnStyle width="80px" />
				<web:columnStyle />
			</web:columnGroup>
			<web:rowOdd>
				<web:column>
					<c:choose>
						<c:when test="${addLocale eq true}">
							<web:legendB key="form.remarks" var="common" />
						</c:when>
						<c:otherwise>
							<web:legend key="form.remarks" var="common" />
						</c:otherwise>
					</c:choose>
				</web:column>
				<web:column>
					<type:remarksDisplay property="${id}" id="${id}" />
				</web:column>
			</web:rowOdd>
			<web:rowEven>
				<web:column>
					<type:button id="${id}_ok" key="form.ok" var="common" onclick="closePopUpRemarks()" addLocale="${addLocale}"/>
				</web:column>
				<web:column>
				</web:column>
			</web:rowEven>
		</web:table>
	</web:section>
</web:viewContent>