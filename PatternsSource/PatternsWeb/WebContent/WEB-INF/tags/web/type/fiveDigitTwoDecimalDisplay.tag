<%@tag body-content="empty"%>
<%@attribute name="property" required="true"%>
<%@attribute name="id" required="true" rtexprvalue="true"%>
<input type='text' class="text textalign-right" id="${id}" maxlength="8" size="8" readonly="readonly" />
