<%@tag body-content="empty"%>
<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@attribute name="property" required="true"%>
<%@attribute name="id" required="true"%>
<%@attribute name="dojnotreq" required="false" type="java.lang.Boolean"%>
<%@attribute name="statusnotreq" required="false" type="java.lang.Boolean"%>

<web:table>
	<web:columnGroup>
		<web:columnStyle width="180px" />
		<web:columnStyle />
	</web:columnGroup>
	<web:rowEven>
		<web:column>
			<web:legend key="form.personId" var="common" />
		</web:column>
		<web:column>
			<input type='text' class="code" id="${property}_id" maxlength="12" size="12" readonly="readonly" />
		</web:column>
	</web:rowEven>
	<web:rowOdd>
		<web:column>
			<web:legend key="form.name" var="common" />
		</web:column>
		<web:column>
			<input type='text' id="${property}_title1" maxlength="3" size="3" readonly="readonly" class="text" />
			<input type='text' id="${property}_title2" maxlength="3" size="3" readonly="readonly" class="text" />
			<input type='text' id="${property}_name" maxlength="100" size="80" readonly="readonly" class="text" />
		</web:column>
	</web:rowOdd>
	<c:choose>
		<c:when test="${dojnotreq eq false || dojnotreq eq null}">
			<web:rowEven>
				<web:column>
					<web:legend key="form.doj" var="common" />
				</web:column>
				<web:column>
					<input type="text" id="${property}_doj" maxlength="10" size="14" readonly="readonly" class="date" />
				</web:column>
			</web:rowEven>
		</c:when>
	</c:choose>
	<c:choose>
		<c:when test="${statusnotreq eq false || statusnotreq eq null}">
			<web:rowEven>
				<web:column>
					<web:legend key="form.currentStatus" var="common" />
				</web:column>
				<web:column>
					<input type='text' class="code" id="${property}_status" maxlength="25" size="25" readonly="readonly" />
				</web:column>
			</web:rowEven>
		</c:when>
	</c:choose>
</web:table>
