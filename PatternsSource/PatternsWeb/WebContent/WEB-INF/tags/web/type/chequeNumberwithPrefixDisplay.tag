<%@tag body-content="empty"%>
<%@attribute name="property" required="true"%>
<%@attribute name="id" required="true" rtexprvalue="true"%>
<input type='text' class="code" id="${id}Prefix" maxlength="12" size="12" readonly="readonly" />
<input type='text' class="code" id="${id}" maxlength="10" size="20" readonly="readonly" />
