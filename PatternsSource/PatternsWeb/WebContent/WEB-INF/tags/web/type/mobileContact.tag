<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<%@tag body-content="empty"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<web:table>
	<web:columnGroup>
		<web:columnStyle width="180px" />
		<web:columnStyle width="180px" />
		<web:columnStyle />
	</web:columnGroup>
	<web:rowEven>
		<web:column>
			<web:legend key="form.mobilecontact" var="common" />
		</web:column>
		<web:column>
			<type:smartCombo property="mobileCountry" id="mobileCountry"
				width="180" />
		</web:column>
		<web:column>
			<type:mobile property="mobileNumber" id="mobileNumber" />
		</web:column>
	</web:rowEven>
</web:table>