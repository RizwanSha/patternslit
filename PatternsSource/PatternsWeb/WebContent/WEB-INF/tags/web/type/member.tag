<%@tag body-content="empty"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@attribute name="property" required="true"%>
<%@attribute name="id" required="true" rtexprvalue="true"%>
<html:text styleId="${id}Salutation" property="${property}Salutation" maxlength="2" size="2" styleClass="text" onblur="doblur(event,this)" onfocus="dofocus(event,this)" />
<html:text styleId="${id}Name" property="${property}Name" maxlength="80" size="80" styleClass="text" onblur="doblur(event,this)" onfocus="dofocus(event,this)" />
<span id="${id}_error" class="level4_error"> <html:errors property="${property}" />
	</span>