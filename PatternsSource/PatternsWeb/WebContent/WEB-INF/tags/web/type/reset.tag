<%@tag body-content="empty"  %>
<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@attribute name="id" required="true"%>
<%@attribute name="key" required="true" %>
<%@attribute name="var" required="true" rtexprvalue="true"%>
<web:message var="${var}" key="${key}" draw="draw"/>
<%
	String value=(String)request.getAttribute("message");
%>
<input type="submit" id="${id}" name="${id}" class="btn btn-xs " accesskey="R" value='<%=value %>' onclick="doreset(this)" />
