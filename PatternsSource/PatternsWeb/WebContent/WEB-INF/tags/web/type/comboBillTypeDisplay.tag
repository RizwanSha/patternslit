<%@tag body-content="empty"  %>
<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@attribute name="property" required="true"%>
<%@attribute name="id" required="true"%>
<%@attribute name="datasourceid" required="false"%>
<select class="text" id="${id}" disabled="disabled">
<c:choose>
		<c:when test="${datasourceid !=null}">
			<web:options datasourceid="${datasourceid}" />
		</c:when>
</c:choose>
</select>
