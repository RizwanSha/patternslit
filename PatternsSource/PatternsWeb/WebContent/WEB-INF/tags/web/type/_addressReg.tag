<%@tag body-content="empty"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<%@attribute name="id" required="true" rtexprvalue="true"%>
<%@attribute name="counter" required="true" rtexprvalue="true"%>
<web:viewContent id="po_view_${counter}" styleClass="hidden">
	<web:section>
		<web:viewTitle var="program" key="qmemresaddr.section${counter}" />
		<web:table>
			<web:columnGroup>
				<web:columnStyle width="180px" />
				<web:columnStyle />
			</web:columnGroup>
			<web:rowEven>
				<web:column>
					<web:legend var="program" key="qmemresaddr.addressnumber" />
				</web:column>
				<web:column>
					<type:dynamicDescriptionDisplay property="${id}" id="${id}" length="10" size="10" />
				</web:column>
			</web:rowEven>
		</web:table>
	</web:section>
	<type:addressDetails property="${id}" id="${id}" readOnly="true" />
	<web:grid height="180" width="570" id="${id}Grid" src="mem/qmemaddrhist_phoneGrid.xml">
	</web:grid>
</web:viewContent>