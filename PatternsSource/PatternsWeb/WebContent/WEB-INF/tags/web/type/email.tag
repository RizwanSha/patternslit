<%@tag body-content="empty"  %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@attribute name="property" required="true"%>
<%@attribute name="id" required="true" rtexprvalue="true"%>
<%@attribute name="size" required="false" %>
<c:choose>
	<c:when test="${size ne null}">
		<html:text styleId="${id}" property="${property}" maxlength="100" size="${size}" onblur="doblur(event,this)" onfocus="dofocus(event,this)" styleClass="text" />
	</c:when>
	<c:otherwise>
		<html:text styleId="${id}" property="${property}" maxlength="100" size="100" onblur="doblur(event,this)" onfocus="dofocus(event,this)" styleClass="text" />
	</c:otherwise>
</c:choose>
<span id="${id}_error" class="level4_error"> <html:errors property="${property}" /> </span>
<script type="text/javascript">
$('#${id}').attr('placeholder', EMAIL_INFO);
</script>