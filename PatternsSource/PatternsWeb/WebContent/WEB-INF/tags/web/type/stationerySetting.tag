<%@tag body-content="empty"%>
<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="resource" tagdir="/WEB-INF/tags/resource"%>
<resource:script src="/Renderer/common/euserpreference.js" />
<html:form method="POST" action="/Renderer/common/euserpreference" styleId="euserpreference">
	<html:hidden styleId="hidPrintStationeryType" property="hidPrintStationeryType" />
</html:form>

<select class="text" id="printStationeryType" style="margin: 0 1em 2em 1em;">
	<%
		java.util.ArrayList<patterns.config.framework.web.GenericOption> stationeryList = null;
			stationeryList =(java.util.ArrayList<patterns.config.framework.web.GenericOption>)request.getAttribute("COMMON_PRINT_STATIONERY_TYPES");
			for(patterns.config.framework.web.GenericOption option : stationeryList){
		String id = option.getId();
		String label = option.getLabel();
	%>
	<option value="<%=id%>"><%=label%></option>
	<%
		}
	%>
</select>


<script type="text/javascript">
	$('#printStationeryType').val("<%=request.getAttribute("PRINT_STATIONERY_TYPE")%>");
</script>
