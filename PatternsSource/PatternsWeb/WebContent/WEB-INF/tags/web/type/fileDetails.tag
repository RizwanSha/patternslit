<%@tag body-content="empty"%>
<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="resource" tagdir="/WEB-INF/tags/resource"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:section>
	<web:table>
		<web:columnGroup>
			<web:columnStyle width="220px" />
			<web:columnStyle />
		</web:columnGroup>
		<web:rowEven>
			<web:column span="2">
				<type:button id="cmdFileSelector" var="common" key="button.fileselector" onclick="doFileUpload()" />
				<type:button id="cmdPreview" var="common" key="button.preview" onclick="doFilePreview()" />
			</web:column>
		</web:rowEven>
		<web:rowOdd>
			<web:column>
				<web:legend key="efileupload.filename" var="common" />
			</web:column>
			<web:column>
				<type:fileName id="fileName" property="fileName" />
			</web:column>
		</web:rowOdd>
		<web:rowEven>
			<web:column>
				<web:legend key="efileupload.fileinventorynumber" var="common" />
			</web:column>
			<web:column>
				<type:fileInventory id="fileInventoryNumber" property="fileInventoryNumber" />
			</web:column>
		</web:rowEven>
		<web:rowOdd>
			<web:column>
				<web:legend key="efileupload.fileDescn" var="common" mandatory="true" />
			</web:column>
			<web:column>
				<type:description property="fileDescn" id="fileDescn" />
			</web:column>
		</web:rowOdd>
		<web:rowEven>
			<web:column>
				<web:legend key="efileupload.alttext" var="common" />
			</web:column>
			<web:column>
				<type:description property="fileAltText" id="fileAltText" />
			</web:column>
		</web:rowEven>
	</web:table>
</web:section>
<web:element property="filePurpose" />
<web:element property="fileExtensionList" />

<resource:fileSelector />