<%@tag body-content="empty"  %>
<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@attribute name="property" required="true"%>
<%@attribute name="id" required="true"%>
<%@attribute name="datasourceid" required="false"%>
<html:select styleClass="text" styleId="${id}" property="${property}" onfocus="dofocus(event,this)" onblur="doblur(event,this)" onchange="dochange(event,this)">
	<c:choose>
		<c:when test="${datasourceid !=null}">
			<html:optionsCollection name="${datasourceid}" value="id" label="label" />
		</c:when>
	</c:choose>
</html:select>