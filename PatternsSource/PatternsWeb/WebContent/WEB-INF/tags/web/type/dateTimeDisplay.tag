<%@tag body-content="empty"  %>
<%@attribute name="property" required="true"%>
<%@attribute name="id" required="true" rtexprvalue="true"%>
<input type="text" id="${id}" maxlength="16" size="24" readonly="readonly" class="date" />
<input type="image" class="calendar pic" src="public/styles/images/datetime.png" id="${id}_pic" style="cursor: default" onclick="return false" disabled="disabled" />
