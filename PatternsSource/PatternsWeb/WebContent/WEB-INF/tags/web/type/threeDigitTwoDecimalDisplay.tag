<%@tag body-content="empty"%>
<%@attribute name="property" required="true"%>
<%@attribute name="id" required="true" rtexprvalue="true"%>
<input type='text' class="text textalign-right" id="${id}" maxlength="6" size="6" readonly="readonly" />
