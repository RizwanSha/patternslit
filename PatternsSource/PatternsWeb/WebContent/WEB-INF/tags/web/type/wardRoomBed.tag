<%@tag body-content="empty"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<%@attribute name="property" required="true"%>
<%@attribute name="id" required="true" rtexprvalue="true"%>
<%@attribute name="readOnly" required="false" type="java.lang.Boolean"%>
<%@attribute name="datasourceid" required="false"%>
<%@attribute name="holdfunction" required="false"%>
<c:choose>
	<c:when test="${datasourceid ne null}">
		<type:combo property="${id}Ward" id="${id}Ward" datasourceid="${datasourceid}" errorNotRequired="true" />
	</c:when>
	<c:otherwise>
		<type:combo property="${id}Ward" id="${id}Ward" errorNotRequired="true" />
	</c:otherwise>
</c:choose>
<type:combo property="${id}Room" id="${id}Room" errorNotRequired="true" />
<type:combo property="${id}Bed" id="${id}Bed" errorNotRequired="true" />
<c:choose>
	<c:when test="${holdfunction ne null}">
		<type:button id="${id}hold" key="form.hold" var="common" onclick="${holdfunction}()" />
	</c:when>
</c:choose>
<span id="${id}_desc" class="level4_description"> </span>
<span id="${id}_error" class="level4_error"> <html:errors property="${property}" />
</span>