<%@tag body-content="empty"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@attribute name="property" required="true"%>
<%@attribute name="id" required="true" rtexprvalue="true"%>
<%@attribute name="readOnly" required="false" type="java.lang.Boolean"%>

<html:text styleClass="text" styleId="${id}From" property="${property}From" maxlength="4" size="2" onblur="doblur(event,this)" onfocus="dofocus(event,this)" onkeyup="dokeyup(event,this)" />

<c:choose>
	<c:when test="${readOnly eq false}">
		<html:text styleClass="text" styleId="${id}To" property="${property}To" maxlength="4" size="2" onblur="doblur(event,this)" onfocus="dofocus(event,this)" onkeyup="dokeyup(event,this)" />
	</c:when>
	<c:otherwise>
		<html:text styleClass="text" styleId="${id}To" property="${property}To" maxlength="4" size="2" onblur="doblur(event,this)" onfocus="dofocus(event,this)" onkeyup="dokeyup(event,this)" readonly="true" />
	</c:otherwise>
</c:choose>
<span id="${id}_error" class="level4_error"><html:errors property="${property}" /></span>