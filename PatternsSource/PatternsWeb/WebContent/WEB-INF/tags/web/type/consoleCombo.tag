<%@tag import="patterns.config.framework.database.RegularConstants"%>
<%@tag body-content="empty"%>
<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<web:bundle baseName="patterns.config.web.forms.Messages" var="common" />
<select class="smartSearchInput" id="consoleList" onchange="doConsoleChange($(this))">
	<%
		java.util.ArrayList<patterns.config.framework.web.GenericOption> consoleList = null;
		patterns.config.framework.thread.ApplicationContext context = patterns.config.framework.thread.ApplicationContext.getInstance();
		String roleType = (String) session.getAttribute(patterns.config.framework.web.SessionConstants.ROLE_TYPE);
		if(roleType!=RegularConstants.NULL)		{
			consoleList = (java.util.ArrayList<patterns.config.framework.web.GenericOption>)session.getAttribute(patterns.config.framework.web.SessionConstants.CONSOLE_LIST);
			if (consoleList.size()==0)	{
	%>
	<option value="">-
		<web:legend key="menu.selectconsole" var="common" />-
	</option>
	<%
		}	else	{
		for (patterns.config.framework.web.GenericOption option : consoleList) {
			String id = option.getId();
			String label = option.getLabel();
	%>
	<option value="<%=id%>"><%=label%></option>
	<%
		}
			}
		}else	{
	%>
	<option value="" selected="selected">-
		<web:legend key="menu.selectconsole" var="common" />-
	</option>
	<%
		}
	%>
</select>
<script type="text/javascript">
    var _consoleValue = "<%=(String)session.getAttribute(patterns.config.framework.web.SessionConstants.CONSOLE_CODE)%>";
	if (_consoleValue != null)
		$('#consoleList').val(_consoleValue);
</script>