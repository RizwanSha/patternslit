<%@tag body-content="empty"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@attribute name="property" required="true"%>
<%@attribute name="id" required="true" rtexprvalue="true"%>
<html:text styleId="${id}" property="${property}" maxlength="6" size="6" onblur="doblur(event,this)" onfocus="dofocus(event,this)" styleClass="text textalign-right" />
<span><label class="control-label ">%</label></span>
<span id="${id}_error" class="level4_error"> <html:errors property="${property}" />
</span>