<%@tag body-content="empty" description="Encapsulates the body content" %>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<%@attribute name="id" required="true"%>
<%@attribute name="property" required="true"%>
<%@attribute name="readOnly" required="false" type="java.lang.Boolean"%>
<%@attribute name="lookup" required="false" type="java.lang.Boolean"%>
<c:choose>
	<c:when test="${readOnly eq true}">
		<html:text styleClass="code" styleId="${id}" property="${property}" maxlength="35" size="35" onblur="doblur(event,this)" onfocus="dofocus(event,this)" readonly="true" />
	</c:when>
	<c:otherwise>
		<html:text styleClass="code" styleId="${id}" property="${property}" maxlength="35" size="35" onblur="doblur(event,this)" onfocus="dofocus(event,this)" />
		<c:choose>
			<c:when test="${lookup eq true}">
				<input type="image" class="help pic" src="public/styles/images/search.png" id="${id}_look" onclick="showHelp('${id}',event);return false" />
			</c:when>
		</c:choose>
	</c:otherwise>
</c:choose>
<span id="${id}_desc" class="level4_description"> </span>
<span id="${id}_error" class="level4_error"> <html:errors property="${property}" />
</span>