<%@tag body-content="empty"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<%@attribute name="property" required="true"%>
<%@attribute name="id" required="true" rtexprvalue="true"%>
<input type='text' id="${id}From" maxlength="4" size="2" readonly="readonly" class="text" />
<input type='text' id="${id}To" maxlength="4" size="2" readonly="readonly" class="text" />
