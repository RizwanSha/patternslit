<%@tag body-content="empty"%>
<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="resource" tagdir="/WEB-INF/tags/resource"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<web:section>
	<web:table>
		<web:columnGroup>
			<web:columnStyle width="220px" />
			<web:columnStyle />
		</web:columnGroup>
		<web:rowEven>
			<web:column span="2">
				<type:button id="cmdPreview" var="common" key="button.preview" onclick="doFilePreview()" />
			</web:column>
		</web:rowEven>
		<web:rowOdd>
			<web:column>
				<web:legend key="efileupload.filename" var="common" />
			</web:column>
			<web:column>
				<type:fileNameDisplay id="fileName" property="fileName" />
			</web:column>
		</web:rowOdd>
		<web:rowEven>
			<web:column>
				<web:legend key="efileupload.fileinventorynumber" var="common" />
			</web:column>
			<web:column>
				<type:fileInventoryDisplay id="fileInventoryNumber" property="fileInventoryNumber" />
			</web:column>
		</web:rowEven>
		<web:rowOdd>
			<web:column>
				<web:legend key="efileupload.fileextensionlist" var="common" />
			</web:column>
			<web:column>
				<type:fileExtensionListDisplay id="fileExtensionList" property="fileExtensionList" />
			</web:column>
		</web:rowOdd>
	</web:table>
</web:section>
<resource:fileSelector />