<%@tag body-content="empty"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@attribute name="id" required="true"%>
<div class="noofaccount">
	<span style="float: left; color: #4682B4; font-size: 14px">Customers Registered</span><br> <span style="float: right; font-size: 22px; font-weight: bold"> <span id="${id}_regDetails"></span></span>
</div>
<div class="noofaccount">
	<span style="float: left; color: #4682B4; font-size: 14px">Accounts Opened</span><br> <span style="float: right; font-size: 22px; font-weight: bold"> <span id="${id}_accDetails"></span></span>
</div>
<div class="noofaccount">
	<span style="float: left; color: #4682B4; font-size: 14px">Accounts Open Under Government Schemes</span><br> <span style="float: right; font-size: 22px; font-weight: bold"> <span id="${id}_schemasDetails"></span></span>
</div>

