<%@tag import="patterns.config.framework.web.GenericOption"%>
<%@tag body-content="empty"%>
<%@tag import="patterns.config.framework.web.configuration.menu.MenuUtils"%>
<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<div class="table_no_wrap">
	<table class="form_table_inner" width="100%">
		<%
			java.util.ArrayList<GenericOption> rop = null;
			MenuUtils menu = new MenuUtils();
			rop = menu.getRelatedPrograms();
			int rowCount = 1;
			for (GenericOption option : rop) {
				String id = option.getId();
				String label = option.getLabel();
				try {
					if (rowCount % 2 == 0) {
		%>
		<tr  height="26">
			<td align="left"><web:linkrenderer
					forward="<%=id.toLowerCase()%>" label="<%=label%>"
					styleClass="program-link" /></td>
		</tr>
		<%
			} else {
		%>
		<tr  height="26">
			<td align="left"><web:linkrenderer
					forward="<%=id.toLowerCase()%>" label="<%=label%>"
					styleClass="program-link" /></td>
		</tr>
		<%
			}
					rowCount++;
				} catch (Exception e) {

				}
			}
		%>

	</table>
</div>
<input id="_rop" type="hidden" value="<%=rop.size()%>"/>




