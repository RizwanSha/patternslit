<%@tag body-content="empty"%>
<%@attribute name="property" required="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@attribute name="id" required="true" rtexprvalue="true"%>
<input type='text' id="${id}Branch" maxlength="12" size="15" readonly="readonly" class="code" />
<input type='text' id="${id}Year" maxlength="4" size="2" readonly="readonly" class="text" />
<input type='text' id="${id}Sl" maxlength="10" size="12" readonly="readonly" class="text" />
<span id="${id}_desc" class="level4_description"> </span>
