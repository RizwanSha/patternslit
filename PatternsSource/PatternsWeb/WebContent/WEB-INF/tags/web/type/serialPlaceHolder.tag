<%@tag body-content="empty"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@attribute name="property" required="true"%>
<%@attribute name="id" required="true" rtexprvalue="true"%>
<%@attribute name="key" required="true" rtexprvalue="true"%>
<%@attribute name="readOnly" required="false" type="java.lang.Boolean"%>
<%@attribute name="lookup" required="false" type="java.lang.Boolean"%>
<c:set var="placeHoder" scope="page" value="" />


<%
	String message = "&nbsp;";
	if (key != null && !key.trim().equals("")) {
		java.util.ResourceBundle bundle = (java.util.ResourceBundle) request
				.getAttribute("common");
		try {
			message = bundle.getString(key);
			jspContext.setAttribute("placeHoder",message);
					
		} catch (Exception e) {
			message = "#{" + key + "}";
			
		}
	}
%>

<c:choose>
	<c:when test="${readOnly eq true}">
		<html:text styleClass="text" styleId="${id}" property="${property}" maxlength="2" size="2" onblur="doblur(event,this)" onfocus="dofocus(event,this)" readonly="true" />
	</c:when>
	<c:otherwise>
		<html:text styleClass="text" styleId="${id}" property="${property}" maxlength="2" size="2" onblur="doblur(event,this)" onfocus="dofocus(event,this)" />
		<c:choose>
			<c:when test="${lookup eq true}">
				<input type="image" class="help pic" src="public/styles/images/search.png" id="${id}_pic" onclick="showHelp('${id}',event);return false" />
			</c:when>
		</c:choose>
	</c:otherwise>

</c:choose>
<span id="${id}_error" class="level4_error"> <html:errors property="${property}" />
</span>
<script type="text/javascript">
	var temp = '<c:out value="${pageScope['placeHoder']}" />';
	$('#${id}').attr('placeholder',temp);
</script>