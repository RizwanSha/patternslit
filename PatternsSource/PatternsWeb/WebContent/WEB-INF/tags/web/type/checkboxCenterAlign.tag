<%@tag body-content="empty"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@attribute name="property" required="true"%>
<%@attribute name="id" required="true" rtexprvalue="true"%>
<%@attribute name="description" required="false" type="java.lang.Boolean"%>
<html:checkbox styleId="${id}" onblur="doblur(event,this)" style="min-height:0px;height:15px;margin-top:6px;" onfocus="dofocus(event,this)" property="${property}" onclick="docheckclick(event,this)" styleClass="checkbox" />
<c:choose>
	<c:when test="${description eq true}">
	<span id="${id}_desc" class="level4_description" style="bottom:5px;padding-left:5px;"> </span>
	</c:when>
</c:choose>
<span id="${id}_error" class="level4_error"> <html:errors property="${property}" />
</span>