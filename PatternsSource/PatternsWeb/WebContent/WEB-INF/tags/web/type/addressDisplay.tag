<%@tag body-content="empty"  %>
<%@attribute name="property" required="true"%>
<%@attribute name="id" required="true" rtexprvalue="true"%>
<textarea rows="5" cols="50" id="${id}" readonly="readonly" class="text"></textarea>
