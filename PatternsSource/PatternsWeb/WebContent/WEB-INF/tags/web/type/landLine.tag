<%@tag body-content="empty"  %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@attribute name="property" required="true"%>
<%@attribute name="id" required="true" rtexprvalue="true"%>
<%@attribute name="readOnly" required="false" type="java.lang.Boolean"%>
<c:choose>
	<c:when test="${readOnly eq true}">
		<html:text styleId="${id}" property="${property}" maxlength="15" size="14" styleClass="text" onblur="doblur(event,this)" onfocus="dofocus(event,this)" readonly="true" />
	</c:when>
	<c:otherwise>
		<html:text styleId="${id}" property="${property}" maxlength="15" size="14" styleClass="text" onblur="doblur(event,this)" onfocus="dofocus(event,this)" />
	</c:otherwise>
</c:choose>
<span id="${id}_error" class="level4_error"> <html:errors property="${property}" /> </span>