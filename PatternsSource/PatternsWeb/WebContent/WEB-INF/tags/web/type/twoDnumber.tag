<%@tag body-content="empty"  %>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@attribute name="property" required="true"%>
<%@attribute name="id" required="true" rtexprvalue="true"%>
<html:text styleId="${id}" styleClass="text" property="${property}" maxlength="2" size="2" onblur="doblur(event,this)" onfocus="dofocus(event,this)" />
<span id="${id}_error"> <html:errors property="${property}" /> </span>