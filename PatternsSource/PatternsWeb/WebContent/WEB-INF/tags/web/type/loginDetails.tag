<%@tag import="patterns.config.framework.web.ajax.ContentManager"%>
<%@tag import="patterns.config.framework.service.DTObject"%>
<%@tag import="patterns.config.framework.database.RegularConstants"%>

<%@tag import="patterns.config.framework.database.utils.DBUtil"%>
<%@tag import="patterns.config.framework.database.utils.DBContext"%>
<%@tag import="java.sql.ResultSet"%>
<%@tag import="java.util.Date"%>

<%@tag import="patterns.config.validations.AccessValidator"%>
<%@tag import="patterns.config.validations.MasterValidator"%>
<%@tag body-content="empty"%>
<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%
	String branchName = null;
	Date attemptDate = null;
	AccessValidator validation = new AccessValidator();
	MasterValidator validator = new MasterValidator();
	DTObject resultDTO = new DTObject();
	DBContext dbContext = new DBContext();
	DBUtil util = dbContext.createUtilInstance();
	try {

		resultDTO.set("BRANCH_CODE", (String) session.getAttribute(patterns.config.framework.web.SessionConstants.BRANCH_CODE));
		resultDTO.set("ACTION", "U");
		resultDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
		resultDTO = validation.validateEntityBranch(resultDTO);
		branchName = resultDTO.get("DESCRIPTION");

		String sqlQuery = "SELECT MAX(ATTEMPT_DATETIME) FROM USERSLOGINATTEMPTS WHERE ENTITY_CODE=? AND USER_ID=? ";
		util.reset();
		util.setSql(sqlQuery);
		util.setString(1, (String) session.getAttribute(patterns.config.framework.web.SessionConstants.ENTITY_CODE));
		util.setString(2, (String) session.getAttribute(patterns.config.framework.web.SessionConstants.USER_ID));
		ResultSet rset = util.executeQuery();
		if (rset.next()) {
			attemptDate = rset.getDate(1);
		}

	} catch (Exception e) {
		e.printStackTrace();
	} finally {
		validation.close();
		validator.close();
		dbContext.close();
		util.reset();
	}
%>

<c:if test="${requestScope['RENDERER_TYPE'] eq '1'}">
	<div style="font-size: 12px">
		<div class="container nav navbar-nav navbar-right">
			<web:message var="common" key="form.logindtl" />
			<span style="color: lightskyblue;"><b><%=(java.util.Date) session.getAttribute("_LDT") == null ? "N/A" : patterns.config.framework.web.FormatUtils.getDate((java.util.Date) session.getAttribute("_LDT"), "EEEE dd , MMMM YYYY ,HH:mm:ss ")%></b></span>
			<web:message var="common" key="form.logindtl1" />
			<span style="color: lightskyblue;"><b><%=session.getAttribute("_BRC") == null ? "N/A" : session.getAttribute("_BRC")%></b></span><%=" (" + branchName + ")"%>
			<br> <br>
			<web:message var="common" key="form.succlogin" />
			<span style="color: lightskyblue;"><b><%=(java.util.Date) session.getAttribute("_LLDT") == null ? "N/A" : patterns.config.framework.web.FormatUtils.getDate((java.util.Date) session.getAttribute("_LLDT"), "EEEE dd , MMMM YYYY ,HH:mm:ss ")%></b></span><br><br>
			<web:message var="common" key="form.unsucclogin" />
			<span style="color: lightskyblue;"><b><%=(java.util.Date) attemptDate == null ? "N/A" : patterns.config.framework.web.FormatUtils.getDate((java.util.Date) attemptDate, "EEEE dd , MMMM YYYY ,HH:mm:ss ")%></b></span>
		</div>
	</div>
	<div>
		<input type="button" class="btn btn-primary" value="Ok" onclick="loginInfoPopup();" style="margin-top: 15px; margin-left: 1px; padding: 8px 25px;">
	</div>
</c:if>

