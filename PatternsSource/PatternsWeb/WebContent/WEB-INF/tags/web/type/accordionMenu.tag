<%@tag import="patterns.config.framework.database.RegularConstants"%>
<%@tag import="patterns.config.framework.web.configuration.menu.MenuUtils"%>

<%@tag body-content="empty"%>
<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<li class="active">
	<h3 id="menuh3" class="fa fa-chevron-down" style="cursor: pointer;">&nbsp;<span class="fa fa-list-ul"></span>Menu</h3>
	<ul id="menuUL">
		<!-- we will keep this LI open by default -->

	<%
		java.util.ArrayList<patterns.config.framework.web.GenericOption> consoleList = null;
		patterns.config.framework.thread.ApplicationContext context = patterns.config.framework.thread.ApplicationContext.getInstance();
		String roleType = (String) session.getAttribute(patterns.config.framework.web.SessionConstants.ROLE_TYPE);
		String sessionConsole = (String)session.getAttribute(patterns.config.framework.web.SessionConstants.CONSOLE_CODE);
		if(roleType!=RegularConstants.NULL)		{
			consoleList = (java.util.ArrayList<patterns.config.framework.web.GenericOption>)session.getAttribute(patterns.config.framework.web.SessionConstants.CONSOLE_LIST);
			if (consoleList.size()==0)	{
	%>
		</li>
	<%
		}	
		else{
			int rowCount = 1;
			for (patterns.config.framework.web.GenericOption option : consoleList) {
				String id = option.getId();
				String label = option.getLabel();
				if(id.equalsIgnoreCase(sessionConsole)){
					%>
						<li class="active">
							<h2 class="fa fa-chevron-right" style="cursor: pointer;font-size:medium;"  onClick=setAccordMenuConsole('<%= id%>');>&nbsp;<%=label%></h2><ul>
							
					<%
				}
				else{
					%>
					<li>
						<h2 class="fa fa-chevron-right" style="cursor: pointer;font-size:medium;" onClick=setAccordMenuConsole('<%= id%>');>&nbsp;<%=label%></h2><ul>
					<%
				}
				
				String consoleCode = id;
				java.util.ArrayList<patterns.config.framework.web.GenericOption> programList = null;
				MenuUtils menu = new MenuUtils();
				if (consoleCode != patterns.config.framework.database.RegularConstants.EMPTY_STRING) {
					programList = menu.getProgramList(consoleCode);
					
					for (patterns.config.framework.web.GenericOption pgmOption : programList) {
						String pgmId = pgmOption.getId();
						String pgmlabel = pgmOption.getLabel();
			%>
							<li><web:linkrenderer title="<%=pgmlabel %>" forward="<%=pgmId.toLowerCase()%>" label="<%=pgmlabel%>" styleClass=""/></li>
<%
					}
				}
				%>
				</ul>
				</li>
				<%
				rowCount++;
			}
		}
	}		
	%>
	</ul>
	</li>