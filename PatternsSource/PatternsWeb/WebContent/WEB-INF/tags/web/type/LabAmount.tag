<%@tag body-content="empty"%>
<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@attribute name="property" required="true"%>
<%@attribute name="id" required="true"%>
<c:choose>
	<c:when test="${readOnly eq true}">
		<html:text styleId="${id}Format" property="${property}Format" maxlength="25" size="25" onblur="doblur(event,this,'amount')" onfocus="dofocus(event,this)" styleClass="text textalign-right" readonly="true" />
	</c:when>
	<c:otherwise>
		<html:text styleId="${id}Format" property="${property}Format" maxlength="25" size="25" onblur="doblur(event,this,'amount')" onfocus="dofocus(event,this)" styleClass="text textalign-right" />
	</c:otherwise>
</c:choose>
<html:hidden styleId="${id}" property="${property}" />
<span id="${id}_error" class="level4_error"><html:errors property="${property}Format" /></span>