<%@tag body-content="empty"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@attribute name="gridName" required="true"%>
<%@attribute name="addFunction" required="true"%>
<%@attribute name="editFunction" required="true"%>
<%@attribute name="cancelFunction" required="false"%>
<%@attribute name="continuousEdit" required="false"%>
<%@attribute name="afterAddFunction" required="false"%>
<%@attribute name="beforeDeleteFunction" required="false"%>
<%@attribute name="afterDeleteFunction" required="false"%>
<%@attribute name="deleteNotReq" required="false" type="java.lang.Boolean"%>
<%@attribute name="insertAtFirsstNotReq" required="false" type="java.lang.Boolean"%>
<%@attribute name="cancelNotReq" required="false" type="java.lang.Boolean"%>
<%@attribute name="moveUpDownReq" required="false" type="java.lang.Boolean"%>
<type:toolbarbutton id="${gridName}_add" key="form.add" var="common" onclick="_grid_addFunction(${addFunction},${afterAddFunction eq null?'NULL_VALUE':afterAddFunction},${gridName},false)" />
<type:toolbarbutton id="${gridName}_edit" key="form.edit" var="common" onclick="_grid_modifyFunction(${editFunction},${gridName},${continuousEdit eq null? false : continuousEdit})" />
<c:if test="${deleteNotReq != true}">
	<type:toolbarbutton id="${gridName}_del" key="form.delete" var="common" onclick="_grid_deleteFunction(${beforeDeleteFunction eq null?'NULL_VALUE':beforeDeleteFunction},${gridName},${afterDeleteFunction eq null?'NULL_VALUE':afterDeleteFunction})" />
</c:if>
<c:if test="${insertAtFirsstNotReq != true}">
	<type:toolbarbutton id="${gridName}_ins" key="form.insertAtFirst" var="common" onclick="_grid_addFunction(${addFunction},${afterAddFunction eq null?'NULL_VALUE':afterAddFunction},${gridName},true)" />
</c:if>
<c:if test="${cancelNotReq != true}">
	<type:toolbarbutton id="${gridName}_cancel" key="form.cancelEdit" var="common" onclick="_grid_cancelFunction(${cancelFunction},${gridName})" />
</c:if>
<c:if test="${moveUpDownReq == true}">
	<type:toolbarbutton id="${gridName}_MoveUp" key="form.moveUp" var="common" onclick="_grid_MoveUpFunction(${gridName})" />
	<type:toolbarbutton id="${gridName}_MoveDown" key="form.moveDown" var="common" onclick="_grid_MoveDownFunction(${gridName})" />
</c:if>
