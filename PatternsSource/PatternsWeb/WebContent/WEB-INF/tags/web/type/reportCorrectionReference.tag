<%@tag body-content="empty" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib prefix="type" tagdir="/WEB-INF/tags/web/type"%>
<%@attribute name="property" required="true"%>
<%@attribute name="id" required="true" rtexprvalue="true"%>
<%@attribute name="readOnly" required="false" type="java.lang.Boolean"%>
<%@attribute name="viewCorrection" required="false" type="java.lang.Boolean"%>

<c:choose>
	<c:when test="${readOnly eq true}">
		<html:text styleId="${id}Date" property="${property}Date" maxlength="10" size="14" onblur="doblur(event,this)" onfocus="dofocus(event,this)" styleClass="date" readonly="true"/>
		<html:text styleClass="code" styleId="${id}Serial" property="${property}Serial" maxlength="5" size="5" onblur="doblur(event,this)" onfocus="dofocus(event,this)"  />
	</c:when>
	<c:otherwise>
		<html:text styleId="${id}Date" property="${property}Date" maxlength="10" size="14" onblur="doblur(event,this)" onfocus="dofocus(event,this)" styleClass="date" />
		<input type="image" class="calendar pic" src="public/styles/images/date.png" id="${id}Date_pic" onclick="showCalendar(this,event,false);return false" />
		<html:text styleClass="text" styleId="${id}Serial" property="${property}Serial" maxlength="5" size="5" onblur="doblur(event,this)" onfocus="dofocus(event,this)"  />
		<input type="image" class="help pic" src="public/styles/images/search.png" id="${id}Serial_pic" onclick="showHelp('${id}Serial',event);return false" />
	</c:otherwise>
</c:choose>
<c:choose>
	<c:when test="${viewCorrection eq true}">
		<type:button id="viewCorrection" key="form.viewDetail" var="common" onclick="viewCorrectionDetails('${id}')"/>
	</c:when>
</c:choose>
<span id="${id}_desc" class="level4_description"> </span>
<span id="${id}_error" class="level4_error"> <html:errors property="${property}" /> </span>
