<%@tag body-content="empty"%>
<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@attribute name="property" required="true"%>
<%@attribute name="id" required="true" rtexprvalue="true"%>
<html:hidden styleId="${id}CashInHand" property="${property}CashInHand" />
<html:hidden styleId="${id}UnAuthPayments" property="${property}UnAuthPayments" />
<html:hidden styleId="${id}UnAuthReceipts" property="${property}UnAuthReceipts" />
<div>
	<table border="1" style="border-color:black;background-color: #ffffcc" >
		<tr>
			<td style="padding-top: 0px;padding-bottom: 0px;"><web:legend key="form.cashinhand" var="common" /></td>
			<td class="text-center" style="padding-bottom: 0px;"><span id="cashInHandCurr" style="font-size: 12px;" class="level4_description"></span></td>
			<td class="text-right" style="padding-bottom: 0px;"><span id="cashInHand" style="font-size: 12px;" class="level4_description"></span></td>
		</tr>
		<tr>
			<td style="padding-top: 0px;padding-bottom: 0px;"><web:legend key="form.unauthpaymentcash" var="common" /></td>
			<td class="text-center" style="padding-bottom: 0px;"><span id="unAuthPaymentCurr" style="font-size: 12px;" class="level4_description"></span></td>
			<td class="text-right" style="padding-bottom: 0px;"><span id="unAuthPaymentCash" style="font-size: 12px;" class="level4_description"></span></td>
		</tr>
		<tr>
			<td style="padding-top: 0px;padding-bottom: 0px;"><web:legend key="form.unauthreceiptcash" var="common" /></td>
			<td class="text-center" style="padding-bottom: 0px;"><span id="unAuthReceiptCurr" style="font-size: 12px;" class="level4_description"></span></td>
			<td class="text-right" style="padding-bottom: 0px;"><span id="unAuthReceiptCash" style="font-size: 12px;" class="level4_description"></span></td>
		</tr>
		<!--  
		<tr>
			<td style="padding-top: 0px;padding-bottom: 0px;"><web:legend key="form.cashallowed" var="common" /></td>
			<td style="padding-bottom: 0px;"><span id="cashAllowed" style="font-size: 12px;" class="level4_description"></span></td>
		</tr>
		-->
	</table>
</div>