<%@tag body-content="empty"  %>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@attribute name="property" required="true"%>
<%@attribute name="id" required="true" rtexprvalue="true"%>
<html:text styleClass="code" styleId="${id}" property="${property}" maxlength="12" size="15" onblur="doblur(event,this)" onfocus="dofocus(event,this)" />
<span id="${id}_desc" class="level4_description"> </span>
<span id="${id}_error" class="level4_error"> <html:errors property="${property}" /> </span>