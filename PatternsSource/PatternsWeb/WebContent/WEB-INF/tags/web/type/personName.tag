<%@tag body-content="empty"  %>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@attribute name="property" required="true"%>
<%@attribute name="id" required="true"%>
<%@attribute name="datasourceid_title1" required="true"%>
<%@attribute name="datasourceid_title2" required="true"%>

<%@attribute name="readOnly" required="false" type="java.lang.Boolean"%>
<html:select styleClass="text" styleId="${property}_title1" property="${property}_title1" onfocus="dofocus(event,this)" onblur="doblur(event,this)" onchange="dochange(event,this)">
	<html:optionsCollection name="${datasourceid_title1}" value="id" label="label" />
</html:select>
<html:select styleClass="text" styleId="${property}_title2" property="${property}_title2" onfocus="dofocus(event,this)" onblur="doblur(event,this)" onchange="dochange(event,this)">
	<html:optionsCollection name="${datasourceid_title2}" value="id" label="label" />
</html:select>
<html:text styleId="${property}_name" property="${property}_name" maxlength="100" size="75" onblur="doblur(event,this)" onfocus="dofocus(event,this)" styleClass="text" />
<span id="${id}_error" class="level4_error"> <html:errors property="${property}" /> </span>