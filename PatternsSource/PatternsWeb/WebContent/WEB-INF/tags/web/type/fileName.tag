<%@tag body-content="empty"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@attribute name="property" required="true"%>
<%@attribute name="id" required="true" rtexprvalue="true"%>
<html:text styleId="${id}" property="${property}" maxlength="100" size="80" styleClass="text" readonly="true" />