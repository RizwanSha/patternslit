<%@tag body-content="empty"  %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@attribute name="property" required="true"%>
<%@attribute name="id" required="true" rtexprvalue="true"%>
<%@attribute name="description" required="false" type="java.lang.Boolean"%>
<input type='checkbox' id="${id}" onblur="doblur(event,this)" style="min-height:0px;height:15px;margin-top:6px;" onfocus="dofocus(event,this)" disabled="disabled" class="checkbox"/>
<c:choose>
	<c:when test="${description eq true}">
	<span id="${id}_desc" class="level4_description" style="bottom:5px;padding-left:5px;"> </span>
	</c:when>
</c:choose>