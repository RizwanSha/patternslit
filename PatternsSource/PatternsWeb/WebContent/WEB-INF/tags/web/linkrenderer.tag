<%@tag body-content="empty"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@attribute name="label" required="true" rtexprvalue="true"%>
<%@attribute name="forward" required="true" rtexprvalue="true"%>
<%@attribute name="styleClass" required="true" rtexprvalue="true"%>
<%@attribute name="id" required="false" rtexprvalue="true"%>
<%@attribute name="title" required="false" rtexprvalue="true"%>
<c:choose>
	<c:when test="${id eq null}">
		<html:link title="${title}" styleClass="${styleClass}" forward="${forward}">${label}</html:link>
	</c:when>
	<c:otherwise>
		<html:link title="${title}" styleClass="${styleClass}" styleId="${id}" forward="${forward}">${label}</html:link>
	</c:otherwise>
</c:choose>