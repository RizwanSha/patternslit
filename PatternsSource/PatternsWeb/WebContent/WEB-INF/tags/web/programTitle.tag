<%@tag body-content="empty"%>
<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@attribute name="collapsable" required="false"%>
<%@attribute name="status" required="false"%>
<%@attribute name="key" required="true" rtexprvalue="true"%>
<%@attribute name="var" required="true" rtexprvalue="true"%>
<%@attribute name="src" required="false" rtexprvalue="true"%>
<%@attribute name="descReqd" required="false" rtexprvalue="false"%>
<%@attribute name="otherLangreq" required="false" type="java.lang.Boolean"%>
<div class="level2_title textalign-left">
	<c:if test="${collapsable ne null}">
		<c:if test="${status eq 'close'}">
			<b id="${collapsable}_pic" class="toggle right" title="click to toggle" onclick="toggle('${collapsable}',this)"><img src="public/styles/images/minimized.png" class="pic" /> </b>
		</c:if>
		<c:if test="${(status eq 'open') or (status eq null)}">
			<b id="${collapsable}_pic" class="toggle right" title="click to toggle" onclick="toggle('${collapsable}',this)"><img src="public/styles/images/maximized.png" class="pic" /> </b>
		</c:if>
	</c:if>
	<c:choose>
		<c:when test="${descReqd eq true}">
			<web:message var="${var}" key="${key }" />
		</c:when>
		<c:otherwise>
			<%=request.getAttribute("_PDESCN")%>
		</c:otherwise>
	</c:choose>
	<c:if test="${otherLangreq eq true}">
		(<web:message var="${var}_${requestScope['ADDITIONAL_LOCALE']}" key="${key}" />)
	</c:if>
</div>