<%@tag body-content="scriptless"  %>
<%@attribute name="width" required="true" rtexprvalue="true"%>
<%@attribute name="align" required="true" rtexprvalue="true"%>
<div class="clearfix ${align} ${width}" style="overflow: hidden;">
	<jsp:doBody />
</div>