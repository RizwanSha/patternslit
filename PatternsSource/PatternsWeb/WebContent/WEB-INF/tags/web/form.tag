<%@tag body-content="scriptless"%>
<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@attribute name="action" required="true"%>
<%@attribute name="id" required="true"%>
<%@attribute name="method" required="true"%>
<html:form method="${method}" action="${action}" styleId="${id}" styleClass="form-horizontal form-group-sm">
	<web:dividerBlock>
		<span class="level4_error"><html:errors property="genericError" /></span>
	</web:dividerBlock>
	<jsp:doBody />
	<web:element property="actionDescription" />
	<web:element property="tfaRequired" />
	<web:element property="tfaNonce" />
	<web:element property="tfaValue" />
	<web:element property="tfaEncodedValue" />
	<web:element property="csrfValue" />
	<web:element property="callSource" />
	<web:element property="primaryKey" />
	<web:element property="sourceKey" />
	<web:element property="backToTemplate" />
	<web:element property="rectify" />
	
</html:form>
<c:if test="${requestScope['_TFA'] eq '1' }">
	<applet codebase="<%=request.getContextPath()%>/applets/" code="SignerApplet.class" name="SIGNAPPLET" width="1" height="1" id="SIGNAPPLET"></applet>
</c:if>
<script type="text/javascript">
	$('#${id}').on('submit', function(e, element, ajax) {
		if (typeof e.originalEvent !== 'undefined') {
			e.preventDefault();
			return false;
		}
		if (getCommand() == RESET) {
			document.getElementById('${id}').submit();
		} else {
			if (dosubmit(element, ajax)) {
				//document.getElementById('${id}').submit();
			} else {
				e.preventDefault();
				return false;
			}
		}
	});
</script>