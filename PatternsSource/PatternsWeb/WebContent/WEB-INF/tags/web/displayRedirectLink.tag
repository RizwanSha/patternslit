<%@tag import="patterns.config.framework.web.configuration.menu.MenuUtils"%>
<%@tag body-content="empty"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@taglib prefix="message" tagdir="/WEB-INF/tags/message"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%
	String redirectLink = request.getParameter("_RL");
	if (redirectLink == null) {
		redirectLink = (String) request.getAttribute("_RL");
	}
	request.setAttribute("_RL", redirectLink);
	//Rizwan Changes done related to Back to Template on 24 Jun 2015 Begins
	String backToTemp = request.getParameter("backToTemplate");
	request.setAttribute("backToTemplate", backToTemp);
	//Rizwan Changes done related to Back to Template on 24 Jun 2015 Ends
	
	String printRequired = request.getParameter("printRequired");
	request.setAttribute("printRequired", printRequired);
	
%>
<div class="textalign-center">
	<button id="cmdForwardPath" class="btn btn-xs btn-primary" onclick="redirectToEntry()">
		<web:message var="common" key="form.operationBack" />
	</button>
	<!-- Rizwan Changes done related to Back to Template on 24 Jun 2015 Begins -->
	<c:if test="${(requestScope['backToTemplate'] eq '1')}">
		<button id="cmdBackToTemplate" class="btn btn-xs btn-primary" onclick="backToTemplateEntry()">
			<web:message var="common" key="form.operationBackTemplate" />
		</button>
	</c:if>
	<!-- Rizwan Changes done related to Back to Template on 24 Jun 2015 Ends -->
	<c:if test="${(requestScope['printRequired'] eq '1')}">
		<button id="cmdPrint" class="btn btn-xs btn-primary" onclick="print()">
			<web:message var="common" key="form.print" />
		</button>
		<web:section>
			<div class="textalign-center">
				<message:messageBox id="eresult" />
			</div>
	</web:section>
	</c:if>
	<%
		String forwardName = (String) request.getAttribute("_RL");
		String forwardPath = MenuUtils.resolveForward(forwardName, request);
		String callSource = request.getParameter("callSource");
		//Rizwan Changes done related to Back to Template on 24 Jun 2015 Begins
		String primaryKey = request.getParameter("primaryKey");
		String sourceKey = request.getParameter("sourceKey");
		String backToTemplate = request.getParameter("backToTemplate");
		String printRequir = request.getParameter("printRequired");
		String beanPath = request.getParameter("beanPath");
		//Rizwan Changes done related to Back to Template on 24 Jun 2015 Ends
	%>
	<input type="hidden" id="forwardPath" value="<%=forwardPath%>" />
	<input type="hidden" id="callSource" value="<%=callSource%>" />
	<!-- Rizwan Changes done related to Back to Template on 24 Jun 2015 Begins -->
	<input type="hidden" id="primaryKey" value="<%=primaryKey%>" />
	<input type="hidden" id="sourceKey" value="<%=sourceKey%>" />
	<input type="hidden" id="backToTemplate" value="<%=backToTemplate%>" />
	<!-- Rizwan Changes done related to Back to Template on 24 Jun 2015 Ends -->
	<input type="hidden" id="printRequired" value="<%=printRequir%>" />
	<input type="hidden" id="beanPath" value="<%=beanPath%>" />
	<input type="hidden" id="forwardName" value="<%=forwardName%>" />
</div>
