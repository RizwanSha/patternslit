<%@tag body-content="empty"  %>
<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@attribute name="key" required="true" rtexprvalue="true"%>
<%@attribute name="var" required="true" rtexprvalue="true"%>
<h4>
	<web:message var="${var}" key="${key }"/>
</h4>