<%@tag body-content="empty" %>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@attribute name="key" required="true"%>
<%@attribute name="var" required="true" rtexprvalue="true"%>
<%@attribute name="forward" required="true"%>
<%@attribute name="styleClass" required="true"%>
<%@attribute name="id" required="false"%>
<html:link styleClass="${styleClass}" styleId="${id}" forward="${forward}" >
	<web:message var="${var}" key="${key}" />
</html:link>
