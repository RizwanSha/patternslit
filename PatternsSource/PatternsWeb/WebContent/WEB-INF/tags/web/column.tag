<%@tag body-content="scriptless"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@attribute name="span" required="false"%>
<%@attribute name="rowspan" required="false"%>
<%@attribute name="align" required="false"%>
<%@attribute name="style" required="false"%>
<c:choose>
	<c:when test="${span != null and align != null}">
		<td colspan="${span}" align="${align}" style="${style}"><jsp:doBody /></td>
	</c:when>
	<c:when test="${span != null}">
		<td colspan="${span}" style="${style}"><jsp:doBody /></td>
	</c:when>
	<c:when test="${rowspan != null}">
		<td rowspan="${rowspan}" style="${style}"><jsp:doBody /></td>
	</c:when>
	<c:when test="${align != null}">
		<td align="${align}" style="${style}"><jsp:doBody /></td>
	</c:when>
	<c:otherwise>
		<td style="${style}"><jsp:doBody /></td>
	</c:otherwise>
</c:choose>