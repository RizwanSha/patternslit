<%@tag body-content="empty" %>
<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@attribute name="key" required="true"%>
<%@attribute name="var" required="true" rtexprvalue="true"%>
<%@attribute name="id" required="true"%>
<%@attribute name="styleClass" required="false" rtexprvalue="true"%>
<div class="level2_info">
<div class="hidden" id="${id}"><web:message var="${var}" key="${key}" /></div>
</div>