<%@tag body-content="empty"%>
<%@taglib prefix="message" tagdir="/WEB-INF/tags/message"%>
<%@attribute name="id" required="true"%>
<message:info styleClass="hidden" id="${id}_info" />
<message:error styleClass="hidden" id="${id}_error" />
<message:warn styleClass="hidden" id="${id}_warn" />
<message:success styleClass="hidden" id="${id}_success" />
<message:progress styleClass="hidden" id="${id}_progress" />