<%@tag body-content="empty"%>
<%@attribute name="id" required="true" rtexprvalue="true"%>
<%@attribute name="styleClass" required="true"%>
<div class="message-row success-row ${styleClass}" id="${id}"></div>