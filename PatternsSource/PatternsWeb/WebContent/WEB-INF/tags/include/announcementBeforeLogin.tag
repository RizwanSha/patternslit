<%@tag body-content="empty"%>
<%@tag import="patterns.config.framework.web.utils.BankAnnouncementUtils"%>
<%@tag import="java.util.List"%>
<%@tag import="java.util.ArrayList"%>
<%@taglib prefix="web" tagdir="/WEB-INF/tags/web"%>
<%
	List<BankAnnouncementUtils.Announcement> announceList = new ArrayList<BankAnnouncementUtils.Announcement>();
	BankAnnouncementUtils utils = new BankAnnouncementUtils();
	announceList = utils.getAnnounceListBeforeLogin();
%>
<div class='announcement_Detail' style='height: 200px'>
	<marquee class="announcement_mar" direction="up" scrollamount="2" behavior="scroll" onmouseover="this.stop();" onmouseout="this.start();">
		<table width="100%">
				<%
			try {
				int rowcount = 0;
				for (BankAnnouncementUtils.Announcement announce : announceList) {
					rowcount++;
					String announceTitle = announce.getAnnounceTitle();
					String announceUrl = announce.getAnnounceUrl();
					String announceUrlTitle = announce.getAnnounceUrlTitle();
					String externalLink = announce.getExternalLink();
					if (rowcount % 2 == 0) {
		%>
		<tr class="row_even level3-header">
			<td>
			<p><%=announceTitle%></p>
			<%
				if (externalLink != null && externalLink.equals("1")) {
			%>
			<p><a class="announcement_link" target="_blank" href="<%=announceUrl%>"><%=announceUrlTitle%></a></p>
			</td>
			<%
				}
			%>
		</tr>
		<%
			} else {
		%>
		<tr class="row_odd level3-header">
			<td>
			<p><%=announceTitle%></p>
			<%
				if (externalLink != null && externalLink.equals("1")) {
			%>
			<p><a class="announcement_link" target="_blank" href="<%=announceUrl%>"><%=announceUrlTitle%></a></p>
			</td>
			<%
				}
			%>
		</tr>
		<%
			}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		%>
		</table>
	</marquee>
</div>