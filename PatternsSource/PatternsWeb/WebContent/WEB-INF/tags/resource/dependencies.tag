<%--DHTMLX Scripts --%>
<%@tag body-content="empty"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="resource" tagdir="/WEB-INF/tags/resource"%>
<%@attribute name="file" required="false" type="java.lang.Boolean"%>
<c:if test="${file eq true}">
	<resource:css href="public/dhtmlxVault/codebase/dhtmlxvault.css" media="screen" />
	<resource:script src="public/dhtmlxVault/codebase/dhtmlxvault.js" />
	<resource:script src="public/scripts/fileuploader.js" />
	<div id="vaultDiv" class="hidden"></div>
	<script type="text/javascript">
		vault=new dhtmlXVaultObject();
    	vault.setImagePath("public/dhtmlxVault/codebase/imgs/");
    	vault.setServerHandlers("servlet/FileUploadProcessor", "servlet/FileUploadInfoProcessor", "servlet/FileUploadIDProcessor");
    	vault.create("vaultDiv");
    	vault.setFilesLimit(1);
	</script>
</c:if>