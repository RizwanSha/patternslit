<%@tag body-content="empty"%>
<%@attribute name="href" required="true"%>
<%@attribute name="media" required="true"%>
<%
javax.servlet.http.HttpServletRequest _request = (javax.servlet.http.HttpServletRequest) request;
String _path = _request.getContextPath();
%>
<link rel="stylesheet" type="text/css" media="${media}" href="<%=_path%>/${href}" />
