<%@tag body-content="empty"%>
<%@taglib prefix="resource" tagdir="/WEB-INF/tags/resource"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<resource:css href="public/cdn/vendor/dhtmlxVault/codebase/skins/dhtmlxvault_dhx_skyblue.css" media="screen" />
<resource:script src="public/cdn/vendor/dhtmlxVault/codebase/dhtmlxvault.js" />
<resource:script src="public/cdn/vendor/dhtmlxVault/codebase/ext/dhtmlxvault_progress.js" />

<%@attribute name="id" required="true"%>
<%@attribute name="width" required="true"%>
<%@attribute name="height" required="true"%>
<%@attribute name="initFileUpload" required="false"%>
<%@attribute name="beforeFileAdd" required="false"%>
<%@attribute name="afterFileUpload" required="false"%>
<%@attribute name="afterFileUploadFail" required="false"%>
<%@attribute name="filelimit" required="false"%>
<%@attribute name="maxfileSize" required="false"%>
<%@attribute name="allowedFileExtension" required="false"%>

<div id="${id}_div" style="width:${width}; height:${height};"></div>
<span id="${id}_div_desc" class="level4_description"> </span>

<script type="text/javascript">
	$("#${id}_div").html(EMPTY_STRING);
	var uploadUrl = "servlet/FileUploadProcessor";
	var vaultInitObject = {
		parent : "${id}_div",
		uploadUrl : uploadUrl,
		buttonUpload : false,
		buttonClear: false,
		skin:"dhx_skyblue",
	};
	
	${id} = new dhtmlXVaultObject(vaultInitObject);
	
	${id}._doUploadFile2 = ${id}._doUploadFile;
	${id}._doUploadFile = function(a) {
		url = uploadUrl + "?" + getUploadParameters();
		this.setURL(url);
		this._doUploadFile2.apply(this, arguments);
	}
	
</script>

<c:if test="${not empty initFileUpload}">
	<script type="text/javascript">
		${initFileUpload}();
	</script>	
</c:if>

<c:if test="${not empty beforeFileAdd}">
	<script type="text/javascript">
		${id}.attachEvent("onBeforeFileAdd",function(file){
			return ${beforeFileAdd}(file);	
		});
	</script>	
</c:if>

<c:if test="${not empty afterFileUpload}">
	<script type="text/javascript">
		${id}.attachEvent("onUploadFile",function(file,extra){
			${afterFileUpload}(file,extra);	
		});
	</script>	
</c:if>

<c:if test="${not empty afterFileUploadFail}">
	<script type="text/javascript">
		${id}.attachEvent("onUploadFail",function(file,extra){
			${afterFileUploadFail}(file,extra);	
		});
	</script>	
</c:if>


<c:if test="${not empty filelimit}">
	<script type="text/javascript">
		${id}.setFilesLimit(${filelimit});
		${id}.attachEvent("onClear", function(){
			${id}.setFilesLimit(${filelimit});
		});
		${id}.attachEvent("onFileRemove", function(file){
			${id}.setFilesLimit(${filelimit});
		});
	</script>	
</c:if>

<c:if test="${not empty allowedFileExtension}">
	<script type="text/javascript">
		$("#${id}_div_desc").html('${allowedFileExtension}');
	</script>	
</c:if>

<c:if test="${not empty initFileUpload}">
	<script type="text/javascript">
		${initFileUpload}(${id});	
	</script>	
</c:if>