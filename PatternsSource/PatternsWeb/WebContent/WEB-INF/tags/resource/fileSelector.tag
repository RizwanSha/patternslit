<%@tag body-content="empty"%>
<%@taglib prefix="resource" tagdir="/WEB-INF/tags/resource"%>
<resource:css href="public/dhtmlxVault/codebase/dhtmlxvault.css" media="screen" />
<resource:script src="public/dhtmlxVault/codebase/dhtmlxvault.js" />
<div id="vaultDiv" class="hidden"></div>
<script type="text/javascript">
	var FILE_PURPOSE_USER_CERTIFICATE = 'U';
	var FILE_PURPOSE_ROOT_CERTIFICATE = 'R';
	var FILE_PURPOSE_CRL_LIST = 'C';
	var fileWindow = null;
	function doPurpose(filePurpose) {
		$('#filePurpose').val(filePurpose);
	}
	function doFileUpload() {
		clearError('fileInventoryNumber_error');
		if (!document.getElementById('vaultDiv')) {
			var vaultDiv = document.createElement('DIV');
			vaultDiv.id = 'vaultDiv';
			document.body.appendChild(vaultDiv);
		}
		$('#vaultDiv').html(EMPTY_STRING);
		vault = new dhtmlXVaultObject();
		vault.setImagePath(WidgetConstants.FILE_IMAGE_PATH);
		vault.setServerHandlers("servlet/FileUploadProcessor", "servlet/FileUploadInfoProcessor", "servlet/FileUploadIDProcessor");
		vault.onAddFile = function(fileName) {
			var allowed = false;
			var ext = this.getFileExtension(fileName);
			var extensionList = $('#fileExtensionList').val().split(',');
			if (extensionList.length > 0) {
				for (i = 0; i < extensionList.length; ++i) {
					if (extensionList[i].toLowerCase() == ext.toLowerCase()) {
						allowed = true;
						break;
					}
				}
			} else {
				allowed = true;
			}
			if (!allowed) {
				alert("You may upload only files with extensions as " + $('#fileExtensionList').val());
				return false;
			} else {
				return true;
			}
		};
		vault.onFileUploaded = function(file) {
			if (!file.error) {
				$('#fileInventoryNumber').val(this.sessionId);
				clearError('fileInventoryNumber_error');
				$('#fileName').val() = file.name.substring(file.name.lastIndexOf("\\")+1,file.name.length);
				$('#fileDescn').val() = file.name.substring(file.name.lastIndexOf("\\")+1,file.name.length);
				alert(FILE_UPLOAD_SERVER_SUCCESS);
				if (fileWindow) {
					fileWindow.close();
				}
			}
		};
		vault.create("vaultDiv");
		vault.setFormField("filePurpose", $('#filePurpose').val());
		
		vault.setFilesLimit(1);
		fileWindow = showContent('fileViewer', 'vaultDiv', '430', '265', FILE_UPLOADER_TITLE);
	}
</script>