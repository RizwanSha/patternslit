var validator = new xmlHTTPValidator();
var CURRENT_PROGRAM_ID = "ELANDING";
var inlineGridSuccessful = null;
function init() {
	loadDSRGrid();
}

function loadDSRGrid() {
	inlineGridSuccessful = loadInlineQueryGrid(CURRENT_PROGRAM_ID, 'DSR_MAIN', 'inlineGridSucc',getRoleCode()+'|'+getRoleCode());
	inlineGridSuccessful.attachEvent("onRowSelect", doRowSelect);
	inlineGridSuccessful.attachEvent("onRowDblClicked", doRowDblClicked);
}

function doRowDblClicked(rowID) {
	inlineGridSuccessful.clearSelection();
	inlineGridSuccessful.selectRowById(rowID, true);
	inlineGridSuccessful.cells(rowID, 0).setValue(true);
	doView();
}

function doRowSelect(rowID) {
	inlineGridSuccessful.clearSelection();
	inlineGridSuccessful.selectRowById(rowID, true);
	inlineGridSuccessful.cells(rowID, 0).setValue(true);
}

function doView() {
	var row = inlineGridSuccessful.getCheckedRows(0);
	if (row == EMPTY_STRING) {
		alert(SELECT_ATLEAST_ONE);
	} else {
		validator.clearMap();
		validator.setMtm(false);
		validator.setValue('DSR_NO', inlineGridSuccessful.cells(row, 3).getValue());
		validator.setClass('patterns.config.web.forms.dms.qdocbrowserbean');
		validator.setMethod('validateDsrNo');
		validator.sendAndReceive();
		var InvNo = validator.getValue("INV_NO");
		if (validator.getValue(RESULT) == DATA_AVAILABLE) {
			var primaryKey = InvNo;
			showWindow('viewPhotosScanImages', getBasePath() + 'Renderer/dms/qimageslider.jsp', IMAGESLIDER_VIEW_TITLE, PK + '=' + primaryKey, true, false, false, 1125, 600);
			resetLoading();

		}

	}
}