package com.patterns.mobile.services.lookup;

import patterns.config.framework.service.DTObject;
import patterns.config.framework.service.EJBLocator;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.thread.ApplicationContext;
import patterns.config.framework.web.BackOfficeFormatUtils;

public abstract class MobileAJAXContentManager extends ContentManager {

	public abstract DTObject getData(DTObject input);

	private ApplicationContext context = ApplicationContext.getInstance();

	public final ApplicationContext getContext() {
		return context;
	}

	public final String preProcessSQL(String sqlQuery) {
		sqlQuery = sqlQuery.replaceAll(SESSION_USER_ID, context.getUserID());
		sqlQuery = sqlQuery.replaceAll(SESSION_PARTITION_NO, context.getPartitionNo());
		sqlQuery = sqlQuery.replaceAll(SESSION_ENTITY_CODE, context.getEntityCode());
		sqlQuery = sqlQuery.replaceAll(SESSION_CUSTOMER_CODE, context.getCustomerCode());
		sqlQuery = sqlQuery.replaceAll(SESSION_DATE_FORMAT, context.getDateFormat());
		sqlQuery = sqlQuery.replaceAll(SESSION_DATETIME_FORMAT, context.getDateFormat()+" "+RegularConstants.TIME_FORMAT);
//		sqlQuery = sqlQuery.replaceAll(SESSION_CBD, BackOfficeFormatUtils.getDate(context.getCurrentBusinessDate(), context.getDateFormat()));
		sqlQuery = sqlQuery.replaceAll(SESSION_TBA_FORMAT, BackOfficeConstants.DB_TBA_DATE_FORMAT);
		sqlQuery = sqlQuery.replaceAll(SESSION_BRANCH_CODE, context.getBranchCode());
		sqlQuery = sqlQuery.replaceAll(SESSION_ROLE_CODE, context.getRoleCode());
		sqlQuery = sqlQuery.replaceAll(SESSION_ROLE_TYPE, context.getRoleType());
		return sqlQuery;
	}	
}