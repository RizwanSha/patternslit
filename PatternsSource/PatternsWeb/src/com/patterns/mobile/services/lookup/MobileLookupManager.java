package com.patterns.mobile.services.lookup;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.StringTokenizer;

import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.loggers.ApplicationLogger;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.DHTMLXGridUtility;
import patterns.config.framework.web.configuration.lookup.LookupConfiguration;
import patterns.config.framework.web.configuration.lookup.LookupMap;
import patterns.json.JSONArray;
import patterns.json.JSONObject;

public class MobileLookupManager extends MobileAJAXContentManager {

	private String tokenID = EMPTY_STRING;
	private String programID = EMPTY_STRING;
	private String arguments = EMPTY_STRING;
	private String startPosition = EMPTY_STRING;

	private String buffer = EMPTY_STRING;
	private String searchText = EMPTY_STRING;
	private String searchColumn = EMPTY_STRING;
	private String orderColumn = EMPTY_STRING;
	private String sortOrder = EMPTY_STRING;
	private boolean yearWiseParsingRequired = false;
	private Object[] yearParameters;

	private LookupConfiguration lookupConfiguration = null;
	private String sqlQuery = EMPTY_STRING;
	ApplicationLogger logger = null;

	private static final String SELECT_CLAUSE = "#{SELECT_CLAUSE}#";
	private static final String SELECT_CLAUSE_X = "#{SELECT_CLAUSE_X}#";
	private static final String WHERE_CLAUSE = "#{WHERE_CLAUSE}#";
	private static final String WHERE_CLAUSE_X = "#{WHERE_CLAUSE_X}#";
	private static final String ORDER_CLAUSE = "#{ORDER_CLAUSE}#";
	private static final String ORDER_CLAUSE_X = "#{ORDER_CLAUSE_X}#";

	public MobileLookupManager() {
		super();
		logger = ApplicationLogger.getInstance(this.getClass().getSimpleName());
	}

	public DTObject getData(DTObject input) {
		DTObject result = new DTObject();
		result.set(RESULT, DATA_UNAVAILABLE);

		try {
			LookupMap lookUpMap = LookupMap.getInstance();
			programID = input.get(PROGRAM_KEY);
			if (programID == null) {
				logger.logError("LOOKUP_PROGRAM_ID not specified");
				return result;
			}
			// programID =
			// programID.trim().toUpperCase(getContext().getLocale());

			tokenID = input.get(TOKEN_KEY);
			if (tokenID == null) {
				logger.logError("LOOKUP_TOKEN_ID not specified");
				return result;
			}

			// tokenID = tokenID.trim().toUpperCase(getContext().getLocale());

			logger.logDebug("LOOKUP_TOKEN : " + programID + ";" + tokenID);
			lookupConfiguration = lookUpMap.getConfiguration(programID, tokenID);
			if (lookupConfiguration == null) {
				logger.logError("LOOKUP_CONFIGURATION not found : " + programID + ";" + tokenID);
				return result;
			}

			if (!lookupConfiguration.isDirectAccess()) {
				logger.logError("LOOKUP_TOKEN Direct Access : " + programID + ";" + tokenID);
				return result;
			}

			if (!input.containsKey(ARGUMENT_KEY)) {
				arguments = EMPTY_STRING;
				input.set(ARGUMENT_KEY, EMPTY_STRING);
			} else {
				arguments = input.get(ARGUMENT_KEY);
			}

			if (!input.containsKey(START_POSITION_KEY)) {
				startPosition = DEFAULT_POSITION;
			} else {
				startPosition = input.get(START_POSITION_KEY).trim();
				try {
					Integer.parseInt(startPosition);
				} catch (NumberFormatException e) {
					logger.logError("LOOKUP_START_POSITION invalid : " + programID + ";" + tokenID + ";" + startPosition);
					return result;
				}
			}

			if (!input.containsKey(BUFFER_KEY)) {
				buffer = DEFAULT_BUFFER;
				input.set(BUFFER_KEY, DEFAULT_BUFFER);
			} else {
				buffer = input.get(BUFFER_KEY).trim();
				try {
					Integer.parseInt(buffer);
				} catch (NumberFormatException e) {
					logger.logError("LOOKUP_BUFFER_KEY invalid : " + programID + ";" + tokenID + ";" + buffer);
					return result;
				}
				buffer = DEFAULT_BUFFER;
			}

			if (!input.containsKey(SEARCH_TEXT)) {
				searchText = EMPTY_STRING;
			} else {
				searchText = input.get(SEARCH_TEXT).trim();
			}
			if (!input.containsKey(SEARCH_COLUMN)) {
				searchColumn = DEFAULT_POSITION;

			} else {
				searchColumn = input.get(SEARCH_COLUMN).trim();
				try {
					Integer.parseInt(searchColumn);
				} catch (NumberFormatException e) {
					logger.logError("LOOKUP_SEARCH_COLUMN invalid : " + programID + ";" + tokenID + ";" + searchColumn);
					return result;
				}
			}

			if (input.containsKey(ORDER_COLUMN)) {
				orderColumn = input.get(ORDER_COLUMN).trim();
				try {
					Integer.parseInt(orderColumn);
				} catch (NumberFormatException e) {
					logger.logError("LOOKUP_ORDER_COLUMN invalid : " + programID + ";" + tokenID + ";" + orderColumn);
					return result;
				}
			}
			if (input.containsKey(SORT_ORDER)) {
				sortOrder = input.get(SORT_ORDER).trim();
				if (!(sortOrder.equals(SORT_ASC) || sortOrder.equals(SORT_DESC))) {
					logger.logError("LOOKUP_SORT_ORDER invalid : " + programID + ";" + tokenID + ";" + sortOrder);
					return result;
				}
			} else {
				input.set(SORT_ORDER, SORT_ASC);
			}

			generateSQLStatement(result);

			if (result.hasError()) {
				logger.logError("LOOKUP SQL Statement Error");
				return result;
			}

			if (lookupConfiguration.isDefaultLoadingRequired() || !searchText.isEmpty()) {
				executeSQLStatement(result);
			}
			if (result.hasError()) {
				logger.logError("SQL Statement Generation has error");
				return result;
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError("getData(1) : " + e.getLocalizedMessage() + " : " + input);
			result.set(RESULT, DATA_UNAVAILABLE);
		}
		return result;
	}

	protected void generateSQLStatement(DTObject result) throws Exception {
		String basicSQL = lookupConfiguration.getSqlQuery();
		boolean selectClauseAvailable = false;
		StringBuffer completeSQL = new StringBuffer(EMPTY_STRING);
		StringBuffer selectSql = new StringBuffer(EMPTY_STRING);
		if (basicSQL.contains(SELECT_CLAUSE)) {
			selectClauseAvailable = true;
			for (int index = 0; index < lookupConfiguration.getColumnNames().length; index++) {
				selectSql.append(lookupConfiguration.getAliasNames()[index]).append(".").append(lookupConfiguration.getColumnNames()[index]);
				if (index != (lookupConfiguration.getColumnNames().length - 1))
					selectSql.append(",");
			}
			selectSql.append(" ");
			completeSQL.append(basicSQL.replace(SELECT_CLAUSE, selectSql.toString()));
		}
		StringBuffer selectXSql = new StringBuffer(EMPTY_STRING);
		if (!selectClauseAvailable) {
			if (basicSQL.contains(SELECT_CLAUSE_X)) {
				for (int index = 0; index < lookupConfiguration.getPredefinedColumns().length; index++) {
					selectXSql.append(lookupConfiguration.getPredefinedColumns()[index]);
					if (index != (lookupConfiguration.getPredefinedColumns().length - 1))
						selectXSql.append(",");
				}
				selectXSql.append(" ");
				completeSQL.append(basicSQL.replace(SELECT_CLAUSE_X, selectXSql.toString()));
			}
		}
		if (searchText.equals(EMPTY_STRING)) {
			completeSQL = new StringBuffer(completeSQL.toString().replace(WHERE_CLAUSE, EMPTY_STRING));
			completeSQL = new StringBuffer(completeSQL.toString().replace(WHERE_CLAUSE_X, EMPTY_STRING));
		} else {
			String dateFormat = "'#DATE_FORMAT#'";
			if (basicSQL.contains(WHERE_CLAUSE_X) && yearWiseParsingRequired) {
				dateFormat = "''#DATE_FORMAT#''";
			}
			StringBuffer whereQuery = new StringBuffer(EMPTY_STRING);
			switch (lookupConfiguration.getColumnTypes()[Integer.parseInt(searchColumn)]) {
			case VARCHAR:
			default:
				searchText = "%" + searchText + "%";
				whereQuery.append(" LOWER(" + lookupConfiguration.getAliasNames()[Integer.parseInt(searchColumn)]).append(".")
						.append(lookupConfiguration.getColumnNames()[Integer.parseInt(searchColumn)].toUpperCase(getContext().getLocale())).append(") LIKE LOWER(?) ");
				break;
			case DATE:
				whereQuery.append(lookupConfiguration.getAliasNames()[Integer.parseInt(searchColumn)]).append(".")
						.append(lookupConfiguration.getColumnNames()[Integer.parseInt(searchColumn)].toUpperCase(getContext().getLocale())).append(" >= TO_DATE(?," + dateFormat + ") ");
				break;
			case TIMESTAMP:
				whereQuery.append(lookupConfiguration.getAliasNames()[Integer.parseInt(searchColumn)]).append(".")
						.append(lookupConfiguration.getColumnNames()[Integer.parseInt(searchColumn)].toUpperCase(getContext().getLocale())).append(" >= str_to_date(?,''#DATETIME_FORMAT#'') ");
				break;
			case BIGDECIMAL:
				whereQuery.append(lookupConfiguration.getAliasNames()[Integer.parseInt(searchColumn)]).append(".")
						.append(lookupConfiguration.getColumnNames()[Integer.parseInt(searchColumn)].toUpperCase(getContext().getLocale())).append(" = ? ");
				break;
			case INTEGER:
				whereQuery.append(lookupConfiguration.getAliasNames()[Integer.parseInt(searchColumn)]).append(".")
						.append(lookupConfiguration.getColumnNames()[Integer.parseInt(searchColumn)].toUpperCase(getContext().getLocale())).append(" = ? ");
				break;
			case BIGINT:
				whereQuery.append(lookupConfiguration.getAliasNames()[Integer.parseInt(searchColumn)]).append(".")
						.append(lookupConfiguration.getColumnNames()[Integer.parseInt(searchColumn)].toUpperCase(getContext().getLocale())).append(" = ? ");
				break;
			case LONG:
				whereQuery.append(lookupConfiguration.getAliasNames()[Integer.parseInt(searchColumn)]).append(".")
						.append(lookupConfiguration.getColumnNames()[Integer.parseInt(searchColumn)].toUpperCase(getContext().getLocale())).append(" = ? ");
				break;
			}
			StringBuffer whereSql = new StringBuffer(EMPTY_STRING);
			if (basicSQL.contains(WHERE_CLAUSE_X)) {
				whereSql.append(" AND ").append(whereQuery.toString());
				completeSQL = new StringBuffer(completeSQL.toString().replace(WHERE_CLAUSE_X, whereSql.toString()));
			} else if (basicSQL.contains(WHERE_CLAUSE)) {
				whereSql.append(" WHERE ").append(whereQuery.toString());
				completeSQL = new StringBuffer(completeSQL.toString().replace(WHERE_CLAUSE, whereSql.toString()));
			}
		}

		StringBuffer orderQuery = new StringBuffer(EMPTY_STRING);
		if (!orderColumn.equals(EMPTY_STRING) && !sortOrder.equals(EMPTY_STRING)) {
			orderQuery.append(lookupConfiguration.getAliasNames()[(Integer.parseInt(orderColumn) - 1)]).append(".").append(lookupConfiguration.getColumnNames()[(Integer.parseInt(orderColumn) - 1)])
					.append(" ").append(sortOrder);
		} else {
			for (int index = 0; index < lookupConfiguration.getColumnNames().length; index++) {
				String sortingOrder = lookupConfiguration.getDefaultSort()[index];
				if (sortingOrder != null && !sortingOrder.equals(EMPTY_STRING)) {
					sortingOrder = sortingOrder.equals("A") ? "ASC" : "DESC";
					orderQuery.append(" ").append(lookupConfiguration.getAliasNames()[index]).append(".").append(lookupConfiguration.getColumnNames()[index]).append(" ").append(sortingOrder)
							.append(",");
				}
			}
			if (!orderQuery.equals(EMPTY_STRING)) {
				orderQuery = new StringBuffer(orderQuery.toString().substring(0, orderQuery.toString().length() - 1));
			}
		}

		StringBuffer orderSql = new StringBuffer(EMPTY_STRING);
		if (completeSQL.toString().contains(ORDER_CLAUSE_X)) {

			orderSql.append(" ,").append(orderQuery.toString());
		} else if (completeSQL.toString().contains(ORDER_CLAUSE)) {
			orderSql.append(" ORDER BY ").append(orderQuery.toString());
		}

		completeSQL = new StringBuffer(completeSQL.toString().replace(ORDER_CLAUSE_X, orderSql.toString()));
		completeSQL = new StringBuffer(completeSQL.toString().replace(ORDER_CLAUSE, orderSql.toString()));

		if (yearWiseParsingRequired) {
			completeSQL = new StringBuffer(MessageFormat.format(completeSQL.toString(), yearParameters));
		}

		String recordCountSql = EMPTY_STRING;
		if (selectClauseAvailable) {
			recordCountSql = completeSQL.toString().replaceFirst(selectSql.toString().trim(), "COUNT(1)");
		}
		if (!selectClauseAvailable) {
			recordCountSql = completeSQL.toString().replaceFirst(selectXSql.toString().trim(), "COUNT(1)");
		}
		if (!orderSql.equals(EMPTY_STRING) && !orderSql.equals(" ")) {
			recordCountSql = recordCountSql.replace(orderSql, EMPTY_STRING);
		}

		basicSQL = preProcessSQL(completeSQL.toString());
		recordCountSql = preProcessSQL(recordCountSql);

		completeSQL.setLength(0);
		completeSQL.append(basicSQL);

		sqlQuery = completeSQL.toString();
		logger.logInfo(sqlQuery);
	}

	private void executeSQLStatement(DTObject result) {
		DBUtil util = getDbContext().createUtilInstance();
		int bindValuesCount = 0;
		int argumentCount = 0;
		int _index = 1;
		try {
			result.set(RESULT, ROW_NOT_PRESENT);
			util.reset();
			util.setSql(sqlQuery);
			if (sqlQuery.indexOf("?") > -1) {
				bindValuesCount = (new StringTokenizer(sqlQuery, "?").countTokens()) - 1;
				String[] sql_arguments = arguments.split("\\|");
				if (arguments.equals(EMPTY_STRING)) {
					argumentCount = 0;
				} else {
					argumentCount = sql_arguments.length;
				}
				if (argumentCount == 0 && bindValuesCount > 0) {
					util.setString(_index, searchText);
				} else if (bindValuesCount != argumentCount) {
					if (bindValuesCount < argumentCount) {
						for (int k = 1; k <= bindValuesCount; k++) {
							util.setString(_index, sql_arguments[k - 1]);
							_index++;
						}
					} else {
						for (int k = 1; k <= argumentCount; k++) {
							util.setString(_index, sql_arguments[k - 1]);
							_index++;
						}
						util.setString(_index, searchText);
					}
				} else if ((bindValuesCount == argumentCount) && argumentCount > 0) {
					for (int k = 1; k <= argumentCount; k++) {
						util.setString(_index, sql_arguments[k - 1]);
						_index++;
					}
				}
			}

			ResultSet resultSet = util.executeQuery();
			JSONArray jsonArray = getJSON(resultSet);
			if (jsonArray.length() > 0) {
				result.set(RESULT, ROW_PRESENT);
				result.setObject(CONTENT, jsonArray);
				result.set(RESULT, DATA_AVAILABLE);
			}
			util.reset();

		} catch (Exception e) {
			e.printStackTrace();
			logger.logError(" executeSQLStatement(1) - " + e.getLocalizedMessage());
			result.set(RESULT, DATA_UNAVAILABLE);
		} finally {
		}
	}

	private JSONArray getJSON(ResultSet resultSet) {
		JSONArray jsonData = new JSONArray();
		JSONArray jsonRows = new JSONArray();
		JSONArray jsonRow;
		JSONArray jsonColumns = new JSONArray();
		try {
			ResultSetMetaData metaData = resultSet.getMetaData();
			int columnCount = metaData.getColumnCount();
//			for (int index = 0; index < lookupConfiguration.getColumnHeadings_en_US().length; index++) {
//				jsonColumns.put(lookupConfiguration.getColumnHeadings_en_US()[index]);
//			}
			jsonData.put(jsonColumns);
			while (resultSet.next()) {
				jsonRow = new JSONArray();
				for (int i = 1; i <= columnCount; i++) {
					jsonRow.put(resultSet.getString(i));
				}
				jsonRows.put(jsonRow);
			}
			jsonData.put(jsonRows);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return jsonRows;
	}
}