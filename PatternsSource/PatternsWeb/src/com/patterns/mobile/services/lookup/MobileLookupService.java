package com.patterns.mobile.services.lookup;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.json.JSONArray;
import patterns.json.JSONObject;

public class MobileLookupService {

	// public String getPinCodes(){
	//
	// }

	public String getPidCodes() {
		DTObject form = new DTObject();
		form.set("_PROGRAM", "MOBILE");
		form.set("_TOKEN", "HLP_PID_CODES");
		JSONArray content = new JSONArray();
		try {
			content = getHelpData(form);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return content.toString();
	}

	// public String getAccountNumbers() {
	// DTObject form = new DTObject();
	// form.set("_PROGRAM", "MOBILE");
	// form.set("_TOKEN", "HLP_ACC_NO");
	// JSONArray content = new JSONArray();
	// try {
	// content = getHelpData(form);
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// return content.toString();
	// }
	//
	// public String getCifNumbers() {
	// DTObject form = new DTObject();
	// form.set("_PROGRAM", "MOBILE");
	// form.set("_TOKEN", "HLP_CIF_NO");
	// JSONArray content = new JSONArray();
	// try {
	// content = getHelpData(form);
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// return content.toString();
	// }

	public String getProducts() {
		DTObject form = new DTObject();
		form.set("_PROGRAM", "MOBILE");
		form.set("_TOKEN", "HLP_PRODUCTS");
		JSONObject column = new JSONObject();
		JSONArray columns = new JSONArray();
		JSONArray rows = new JSONArray();
		column.put("english", "Product Code");
		column.put("hindi", "प्रवर्ग");
		columns.put(column);
		column = new JSONObject();
		column.put("english", "Description");
		column.put("hindi", "वर्णन");
		columns.put(column);
		JSONArray content = new JSONArray();
		content.put(columns);
		try {
			rows = getHelpData(form);
		} catch (Exception e) {
			e.printStackTrace();
		}
		content.put(rows);
		return content.toString();
	}

	public String getAccountTypes() {
		DTObject form = new DTObject();
		form.set("_PROGRAM", "MOBILE");
		form.set("_TOKEN", "HLP_ACC_TYPES");
		JSONObject column = new JSONObject();
		JSONArray columns = new JSONArray();
		JSONArray rows = new JSONArray();
		column.put("english", "Account Type");
		column.put("hindi", "खाता प्रकार");
		columns.put(column);
		column = new JSONObject();
		column.put("english", "Description");
		column.put("hindi", "वर्णन");
		columns.put(column);
		JSONArray content = new JSONArray();
		content.put(columns);
		try {
			rows = getHelpData(form);
		} catch (Exception e) {
			e.printStackTrace();
		}
		content.put(rows);
		return content.toString();
	}

	public String getAccountSubTypes() {
		DTObject form = new DTObject();
		form.set("_PROGRAM", "MOBILE");
		form.set("_TOKEN", "HLP_ACC_SUB_TYPES");
		JSONArray content = new JSONArray();
		JSONArray totalData = new JSONArray();
		JSONArray columns = new JSONArray();
		JSONArray row = new JSONArray();
		JSONObject accountTypes = new JSONObject();
		// JSONObject accountSubTypes = new JSONObject();
		try {
			JSONObject column = new JSONObject();
			column.put("english", "Account Sub Type");
			column.put("hindi", "खाता उप प्रकार");
			columns.put(column);
			column = new JSONObject();
			column.put("english", "Description");
			column.put("hindi", "वर्णन");
			columns.put(column);
			content.put(columns);
			totalData = getHelpData(form);
			int columnLength = 2;
			int rowLength = ((JSONArray) totalData).length();
			row = new JSONArray();
			for (int i = 0; i < rowLength; i++) {
				String accountType = (String) ((JSONArray) totalData.get(i)).get(0);
				Object accountSubTypes = null;
				try {
					accountSubTypes = accountTypes.get(accountType);
				} catch (Exception e) {
					if (!(accountSubTypes instanceof JSONArray)) {
						accountSubTypes = new JSONArray();
						accountTypes.put(accountType, accountSubTypes);
					}
				}
				row = new JSONArray();
				for (int j = 1; j <= columnLength; j++) {
					row.put(((JSONArray) totalData.get(i)).get(j));
				}
				((JSONArray) accountSubTypes).put(row);
				accountTypes.put(accountType, accountSubTypes);
			}
			content.put(accountTypes);
			// content.put(accountTypes.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return content.toString();
	}

	public String getGovernmentSchemes() {
		DTObject form = new DTObject();
		form.set("_PROGRAM", "MOBILE");
		form.set("_TOKEN", "HLP_GOVT_SCHEMES");
		JSONArray rows = new JSONArray();
		JSONArray columns = new JSONArray();
		JSONArray content = new JSONArray();
		JSONObject column = new JSONObject();
		column.put("english", "Government Scheme");
		column.put("hindi", "सरकारी योजना");
		columns.put(column);
		column = new JSONObject();
		column.put("english", "Description");
		column.put("hindi", "वर्णन");
		columns.put(column);
		content.put(columns);
		try {
			rows = getHelpData(form);
		} catch (Exception e) {
			e.printStackTrace();
		}
		content.put(rows);
		return content.toString();
	}

	public String getOperatingModes() {
		DTObject form = new DTObject();
		form.set("_PROGRAM", "MOBILE");
		form.set("_TOKEN", "HLP_OPR_MODES");
		JSONArray rows = new JSONArray();
		JSONArray columns = new JSONArray();
		JSONArray content = new JSONArray();
		JSONObject column = new JSONObject();
		column.put("english", "Operating Mode");
		column.put("hindi", "कार्य प्रणाल");
		columns.put(column);
		column = new JSONObject();
		column.put("english", "Description");
		column.put("hindi", "वर्णन");
		columns.put(column);
		content.put(columns);
		try {
			rows = getHelpData(form);
		} catch (Exception e) {
			e.printStackTrace();
		}
		content.put(rows);
		return content.toString();
	}

	private JSONArray getHelpData(DTObject form) {
		JSONArray content = new JSONArray();
		if (form.containsKey("_PROGRAM") && form.containsKey("_TOKEN")) {
			MobileLookupManager manager = new MobileLookupManager();
			try {
				manager.init();
				DTObject dataObject = manager.getData(form);
				content = (JSONArray) dataObject.getObject(ContentManager.CONTENT);
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				manager.destroy();
			}
		}
		return content;
	}

	public String getLookupDetails(DTObject fields) {
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		String sqlQuery = null;
		JSONArray lookupDetails = new JSONArray();
		ResultSet rset;
		try {
			util.reset();
			switch (fields.get("FIELD_NAME")) {
			case "accountNumbers":
				sqlQuery = "SELECT CONCAT(C.TITLE_CODE, ' ', C.NAME) , C.CIF_NO FROM CUSTOMERREG C WHERE C.CIF_NO IN (SELECT H.CIF_NO FROM ACCOUNTHOLDERS H WHERE H.ACCOUNT_NO=?)";
				util.setSql(sqlQuery);
				String accountNumber = new JSONObject(fields.get("FIELD_VALUES")).getString("accountNumber");
				util.setString(1, accountNumber);
				rset = util.executeQuery();
				JSONArray dataRow;
				while (rset.next()) {
					dataRow = new JSONArray();
					dataRow.put(rset.getObject(1));
					dataRow.put(rset.getString("C.CIF_NO"));
					lookupDetails.put(dataRow);
				}
				break;
			case "cifNumbers":
				sqlQuery = "SELECT NAME, LEGAL_CONN_NAME, YEAR_OF_BIRTH, DATE_OF_BIRTH, TITLE_CODE FROM CUSTOMERREG WHERE CIF_NO=?";
				util.setSql(sqlQuery);
				String cifNumber = new JSONObject(fields.get("FIELD_VALUES")).getString("cifNumber");
				util.setString(1, cifNumber);
				rset = util.executeQuery();
				if (rset.next()) {
					lookupDetails.put(rset.getString("NAME"));
					lookupDetails.put(rset.getString("LEGAL_CONN_NAME"));
					lookupDetails.put(rset.getString("YEAR_OF_BIRTH"));
					lookupDetails.put(rset.getString("DATE_OF_BIRTH"));
					lookupDetails.put(rset.getString("TITLE_CODE"));
				}
				break;
			case "pidCodes":
				sqlQuery = "SELECT DOC_HAS_ISSUE_DATE, DOC_HAS_EXPIRY_DATE, DOC_USED_FOR_ID_PROOF, DOC_USED_FOR_ADDR_PROOF, DESCRIPTION FROM PIDCODES WHERE PID_CODE=?";
				util.setSql(sqlQuery);
				String pidCode = new JSONObject(fields.get("FIELD_VALUES")).getString("pidCode");
				util.setString(1, pidCode);
				rset = util.executeQuery();
				if (rset.next()) {
					lookupDetails.put(rset.getString("DOC_HAS_ISSUE_DATE"));
					lookupDetails.put(rset.getString("DOC_HAS_EXPIRY_DATE"));
					lookupDetails.put(rset.getString("DOC_USED_FOR_ID_PROOF"));
					lookupDetails.put(rset.getString("DOC_USED_FOR_ADDR_PROOF"));
					lookupDetails.put(rset.getString("DESCRIPTION"));
				}
				break;
			default:
				sqlQuery = "";
				break;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			dbContext.close();
			util.reset();
		}
		return lookupDetails.toString();
	}
}
