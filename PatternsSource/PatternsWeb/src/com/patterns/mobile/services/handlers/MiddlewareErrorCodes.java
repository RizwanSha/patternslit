/*
 * Copyright (c) 2014, 2015, Patterns Software Design Institute. All rights reserved. Unauthorized copying of this file, via any medium is strictly prohibited.
 * PATTERNS SDI PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.patterns.mobile.services.handlers;

/**
 * The Class MiddlewareErrorCodes enumerates possible error codes in Middleware
 */
public class MiddlewareErrorCodes {
	public static final String UNSPECIFIED_ERROR = "0999";
	public static final String FIELD_BLANK = "SRV001";
	public static final String INVALID_EMAIL_ID = "SRV002";
	public static final String EMAIL_ALREADY_REGISTERED = "SRV003";
	public static final String INVALID_MOBILE_NUMBER = "SRV004";
	public static final String INVALID_SUBJECT_TYPE = "SRV005";
	public static final String INVALID_SESSION = "SRV006";
	public static final String INVALID_GENDER = "SRV007";
	public static final String INVALID_DATE = "SRV008";
	public static final String DATE_LCBD = "SRV009";
	public static final String INVALID_NATIONALITY = "SRV010";
	public static final String INVALID_SECURITY_QUESTION = "SRV011";
	public static final String INVALID_PID_TYPE = "SRV012";
	public static final String EMAIL_NOT_EXISTS = "SRV013";
	public static final String INVALID_SECURITY_ANSWER = "SRV014";
	public static final String INVALID_OTP = "SRV015";
	public static final String OTP_EXPIRED = "SRV016";
	public static final String EMAIL_INTF_NOT_CONFIGURED = "SRV017";
	public static final String INVALID_CAPTCHA = "SRV018";
	public static final String INVALID_LENGTH = "SRV019";
	public static final String ATLEAST_ONE_ACNT_REQD = "SRV020";
	public static final String ATLEAST_ONE_PID_REQD = "SRV021";
	public static final String EMAIL_NOT_REGISTERED = "SRV023";
	public static final String EMAIL_NOT_SAME = "SRV024";
	// public static final String INVALID_CREDENTIAL = "SRV025";
	public static final String TO_DATE_GE_FROM_DATE = "SRV026";
	public static final String INVALID_REQ_TYPE = "SRV027";
	public static final String CIR_RUID_REQD = "SRV028";
	public static final String NO_RESULT = "SRV029";
	public static final String EXT_APPLNCFG_UNAVAIL = "SRV030";
	public static final String INVALID_APP_KEY = "SRV031";
	public static final String MULTIPLE_HIT = "SRV032";
	public static final String USER_NOT_ACTIVATED = "SRV033";
	public static final String INVALID_SEC_QUEST_ANS = "SRV034";
	public static final String XMLPARSING_ERROR = "SRV035";
	public static final String CAPTCHA_RETRY_EXCEEDS = "SRV036";
	public static final String REG_RETRY_EXCEEDS = "SRV037";
	public static final String NO_HIT = "SRV038";
	public static final String LOGIN_LOCKED = "SRV039";
	public static final String CATALOG_LOAD_FAILED = "SRV040";
	// Added By Sarat 08-04-2015 -Begin
	public static final String MOBILE_NUMBER_ALREADY_REGISTERED = "SRV041";
	// Added By Sarat 08-04-2015 -End
	// Added By Aditya on 28-04-2015 Begin
	public static final String DATE_LECBD = "SRV042";
	// Added By Aditya on 28-04-2015 End
	// Added By MSPR on 28-04-2015 Begin
	public static final String USER_INACTIVE = "SRV043";
	// Added By MSPR on 28-04-2015 End
	// Added By MSPR on 28-04-2015 Begin
	public static final String INVALID_DATE_RANGE = "SRV044";
	// public static final String INVALID_DATE_RANGE = "SRV008";
	// Added By MSPR on 28-04-2015 Begin

	public static final String FIELD_ROLE_NOT_ENABLED = "SRV100";
	public static final String WALLET_NOT_AVAILABLE = "SRV101";
	public static final String CASHMODPARAM_NOT_AVAILABLE = "SRV102";
}
