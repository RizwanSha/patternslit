/*
 * Copyright (c) 2014, 2015, Patterns Software Design Institute. All
 * rights reserved. Unauthorized copying of this file, via any medium is
 * strictly prohibited. PATTERNS SDI PROPRIETARY/CONFIDENTIAL. Use is
 * subject to license terms.
 */
package com.patterns.mobile.services.handlers;

import patterns.config.framework.database.MiddlewareConstants;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.interfaceref.InterfaceConstants;
import patterns.config.framework.loggers.ApplicationLogger;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.thread.ApplicationContext;
import patterns.config.framework.thread.MobileApplicationContextImpl;
import patterns.config.framework.thread.WebContext;
import patterns.config.framework.web.FormatUtils;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.framework.web.ajax.ErrorMap;
import patterns.config.validations.MiddlewareValidator;

import com.patterns.mobile.services.ServiceBean;
import com.patterns.mobile.services.ServiceLoader;
import com.patterns.mobile.services.pojo.GenericRequest;
import com.patterns.mobile.services.pojo.GenericResponse;

/**
 * The Class GenericInterfaceHandler provides base functionality for a handler
 */
public class GenericInterfaceHandler<T extends GenericRequest, U extends GenericResponse> {
	protected String requestId;
	protected String sessionId;
	private ApplicationLogger logger = null;
	private DBContext dbContext = null;
	private String processBO;
	protected final MobileApplicationContextImpl context = (MobileApplicationContextImpl) ApplicationContext.getInstance();
	protected final WebContext webContext = WebContext.getInstance();
	private ErrorMap errorMap = new ErrorMap();
	protected T request;

	public ErrorMap getErrorMap() {
		return errorMap;
	}

	public void setErrorMap(ErrorMap errorMap) {
		this.errorMap = errorMap;
	}

	public ApplicationLogger getLogger() {
		return logger;
	}

	public GenericInterfaceHandler() {
		logger = ApplicationLogger.getInstance(this.getClass().getSimpleName());
		this.dbContext = new DBContext();
	}

	public GenericInterfaceHandler(DBContext dbContext) {
		logger = ApplicationLogger.getInstance(this.getClass().getSimpleName());
		this.dbContext = dbContext;
	}

	public DBContext getDbContext() {
		return dbContext;
	}

	public void close() {
		getDbContext().close();
	}

	public void setProcessBO(String processBO) {
		this.processBO = processBO;
	}

	public String getProcessBO() {
		return processBO;
	}

	public String decodeBooleanToString(boolean value) {
		return FormatUtils.decodeBooleanToString(value);
	}

	public boolean decodeStringToBoolean(String value) {
		return FormatUtils.decodeStringToBoolean(value);
	}

	public GenericResponse process(T request) throws Exception {
		this.request = request;
		String appId = getApplicationKey();
		// String sessionId = request.getSessionId();
		GenericResponse response = new GenericResponse();
		if (isEmpty(request.getApplicationKey()) || (!request.getApplicationKey().equals(appId))) {
			response.setRequestId(request.getRequestId());
			response.setErrorPresent(true);
			getErrorMap().setError(InterfaceConstants.ERROR, MiddlewareErrorCodes.INVALID_APP_KEY);
			response.setErrorMap(getErrorMap().getMap());
			return response;
		}
		String requestType = request.getRequestType();
		if (requestType.equals(MiddlewareConstants.REQ_TYPE_LOGIN)) {
			response = processLoginRequest(request);
		} else {
			// if (!validSession()) {
			// response.setRequestId(request.getRequestId());
			// response.setErrorPresent(true);
			// getErrorMap().setError(InterfaceConstants.ERROR,
			// MiddlewareErrorCodes.INVALID_SESSION);
			// response.setErrorMap(getErrorMap().getMap());
			// return response;
			// }
			// if (validUserSession()) {
			if (requestType.equals(MiddlewareConstants.REQ_TYPE_QUERY)) {
				response = processQueryRequest(request);
			} else if (requestType.equals(MiddlewareConstants.REQ_TYPE_SUBMIT)) {
				response = getSubmitResponse(request);

			} else if (requestType.equals(MiddlewareConstants.REQ_TYPE_LOOKUP)) {
				response = processQueryRequest(request);
			} else {
				response.setErrorPresent(true);
			}
			// } else {
			// response.setErrorPresent(true);
			// response.setErrorMap(getErrorMap().getMap());
			// return response;
			// }
		}
		response.setRequestId(request.getRequestId());
		if (getErrorMap().getMap().size() > 0) {
			response.setErrorPresent(true);
			response.setErrorMap(getErrorMap().getMap());
		}
		return response;
	}

	public GenericResponse getSubmitResponse(T request) throws Exception{
		GenericResponse response = new GenericResponse();
		return response;
	}

	// GenericResponse response = new GenericResponse();
	// DTObject formDTO = processSubmitRequest(request);
	// if
	// (formDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE))
	// {
	// List<String> responseDataList = new ArrayList<String>();
	// boolean success = false;
	// TBAProcessResult executionResult;
	// if (formDTO.containsKey("SERVICE_BO")) {
	// formDTO.set(RegularConstants.PROCESSBO_CLASS, getProcessBO());
	// formDTO.set("ENTITY_CODE", context.getEntityCode());
	// formDTO.set("USER_ID", context.getUserID());
	// formDTO.set("OFFICE_CODE", context.getBranchCode());
	// // formDTO.set("SESSION_ID", context.getUserID());
	// formDTO.set("FIN_YEAR", context.getFinYear());
	// formDTO.set("CLUSTER_CODE", context.getClusterCode());
	// formDTO.set("PARTITION_NO", context.getPartitionNo());
	// formDTO.set("ROLE_CODE", context.getRoleCode());
	// ServiceExecutor executor = new ServiceExecutor();
	// executionResult = executor.delegate(formDTO);
	// if (executionResult.getProcessStatus().equals(TBAProcessStatus.SUCCESS))
	// {
	// DTObject responseDTO = (DTObject) executionResult.getAdditionalInfo();
	// }
	// } else {
	// BackOfficeProcessManager processManager = new BackOfficeProcessManager();
	// TBAProcessInfo processInfo = new TBAProcessInfo();
	// processInfo.setProcessData(formDTO);
	// processInfo.setProcessBO(formDTO.get("BO_NAME"));
	// TBAProcessAction processAction = new TBAProcessAction();
	// // ApplicationContext context =
	// // ApplicationContext.getInstance();
	// processAction.setProcessID(formDTO.get("PROCESS_ID"));
	// processAction.setPartitionNo(context.getPartitionNo());
	// processAction.setActionEntity(context.getEntityCode());
	// processAction.setActionBranch(context.getBranchCode());
	// processAction.setActionType(TBAActionType.ADD);
	// processAction.setActionUser(context.getUserID());
	// processAction.setActionDate(null);
	// processAction.setActionCustomer("1");
	// processAction.setRectification(false);
	// processInfo.setProcessAction(processAction);
	// try {
	// executionResult = processManager.delegate(processInfo);
	// switch (executionResult.getProcessStatus()) {
	// case FAILURE:
	// break;
	// case SUCCESS:
	// success = true;
	// break;
	// default:
	// break;
	// }
	// MessageParam additionalInfo = (MessageParam)
	// executionResult.getAdditionalInfo();
	// responseDataList = additionalInfo.getParameters();
	// } catch (Exception e) {
	// e.printStackTrace();
	// getErrorMap().setError(InterfaceConstants.ERROR,
	// MiddlewareErrorCodes.UNSPECIFIED_ERROR);
	// }
	// }
	//
	// if (responseDataList.size() == 0) {
	// getErrorMap().setError(InterfaceConstants.ERROR,
	// MiddlewareErrorCodes.UNSPECIFIED_ERROR);
	// return response;
	// }
	// JSONArray responseData = new JSONArray();
	// for (int i = 0; i < responseDataList.size(); i++) {
	// responseData.put(responseDataList.get(i));
	// }
	// if (success) {
	// response.setResponseData(responseData.toString());
	// }
	// }
	// return response;
	// }

	public GenericResponse processQueryRequest(T request) throws Exception {
		return new GenericResponse();
	};

	public DTObject processSubmitRequest(T request) throws Exception {
		return new DTObject();
	};

	public GenericResponse processLoginRequest(T request) throws Exception {
		return new GenericResponse();
	};

	public boolean isEmpty(String input) {
		if (input != null && !input.equals(RegularConstants.EMPTY_STRING))
			return false;
		return true;
	}

	public String getApplicationKey() {
		String applicationId = RegularConstants.EMPTY_STRING;
		MiddlewareValidator validator = new MiddlewareValidator();
		try {
			DTObject formDTO = validator.getApplicationKey();
			if (formDTO.get(ContentManager.ERROR) == null && formDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
				return formDTO.get("EXT_GENERATED_KEY");
			}
		} catch (Exception e) {
			e.printStackTrace();
			getLogger().logError(e.getLocalizedMessage());
		} finally {
			validator.close();
		}
		return applicationId;
	}

	public String getApplicationId() {
		return context.getMobileContextParam().get("APPL_EXT_APP_CODE");
	}

	public boolean captchaRequired(String serviceCode) {
		ServiceBean serviceBean = ServiceLoader.getInstance().getServiceConfiguration(context.getEntityCode(), context.getMobileContextParam().get("APPL_EXT_APP_CODE"), serviceCode);
		return serviceBean.isCaptchaRequired();
	}

	public boolean lockingRequired(String serviceCode) {
		ServiceBean serviceBean = ServiceLoader.getInstance().getServiceConfiguration(context.getEntityCode(), context.getMobileContextParam().get("APPL_EXT_APP_CODE"), serviceCode);
		return serviceBean.isLockingRequired();
	}

	public boolean validSession() {
		MiddlewareValidator validator = new MiddlewareValidator();
		try {
			if (validator.isEmpty(request.getSessionId())) {
				return false;
			}
			DTObject formDTO = new DTObject();
			formDTO.set("SESSION_ID", request.getSessionId());
			formDTO = validator.validateSession(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				return false;
			}
			if (formDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getLogger().logError(e.getLocalizedMessage());
		} finally {
			validator.close();
		}
		return false;
	}

	private boolean validUserSession() {
		MiddlewareValidator validator = new MiddlewareValidator();
		try {
			if (validator.isEmpty(request.getSessionId())) {
				getErrorMap().setError("sessionId", MiddlewareErrorCodes.FIELD_BLANK);
				return false;
			}
			DTObject formDTO = new DTObject();
			formDTO.set("SESSION_ID", request.getSessionId());
			formDTO.set("USER_ID", context.getUserID());
			formDTO = validator.validateUserSessionId(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError(InterfaceConstants.ERROR, formDTO.get(ContentManager.ERROR));
				return false;
			}
			if (formDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				getErrorMap().setError(InterfaceConstants.ERROR, MiddlewareErrorCodes.INVALID_SESSION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError(InterfaceConstants.ERROR, MiddlewareErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return false;
	}
}