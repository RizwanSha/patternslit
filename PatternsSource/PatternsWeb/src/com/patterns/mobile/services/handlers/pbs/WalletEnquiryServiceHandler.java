/*


 * Copyright (c) 2014, 2015, Patterns Software Design Institute. All
 * rights reserved. Unauthorized copying of this file, via any medium is
 * strictly prohibited. PATTERNS SDI PROPRIETARY/CONFIDENTIAL. Use is
 * subject to license terms.
 */
package com.patterns.mobile.services.handlers.pbs;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.Random;

import org.json.JSONObject;

import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.interfaceref.InterfaceConstants;
import patterns.config.framework.service.DTObject;
import patterns.config.validations.MiddlewareValidator;
import patterns.json.JSONArray;

import com.patterns.mobile.services.handlers.GenericInterfaceHandler;
import com.patterns.mobile.services.handlers.MiddlewareErrorCodes;
import com.patterns.mobile.services.lookup.MobileLookupService;
import com.patterns.mobile.services.pojo.GenericRequest;
import com.patterns.mobile.services.pojo.GenericResponse;

/**
 * The Class WalletEnquiryServiceHandler
 */
public class WalletEnquiryServiceHandler extends GenericInterfaceHandler<GenericRequest, GenericResponse> {

	public WalletEnquiryServiceHandler() {
		setProcessBO("");
	}

	@SuppressWarnings("unused")
	private GenericRequest request;
	@SuppressWarnings("unused")
	private int transactionPerPage = 10;

	@Override
	public GenericResponse processQueryRequest(GenericRequest request) throws Exception {
		this.request = request;
		GenericResponse response = new GenericResponse();
		response.setRequestId(request.getRequestId());
		String queryType = request.getQueryType();
		JSONObject responseData = new JSONObject();
		try {
			if (queryType.equals("init")) {
				MobileLookupService lookupService = new MobileLookupService();
				// responseData.put("accountNumbers",
				// lookupService.getAccountNumbers());
				// responseData.put("cifNumbers",
				// lookupService.getCifNumbers());
				response.setResponseData(responseData.toString());
			} else if (queryType.equals("gridQuery")) {
				JSONObject requestData = new JSONObject(request.getRequestData());
				DTObject inputDTO = new DTObject();
				inputDTO.set("ACCOUNT_NO", (String) requestData.get("accountNumber"));
				inputDTO.set("CIF_NO", (String) requestData.get("cifNumber"));
				inputDTO.set("PIN_CODE", (String) requestData.get("pinCode"));
				inputDTO.set("MOBILE_NO", (String) requestData.get("mobileNumber"));
				inputDTO.set("STATUS", (String) requestData.get("status"));
				String responseString = getWallets(inputDTO);
				responseData.put("walletsForDelivery", responseString);
				response.setResponseData(responseData.toString());
			} else if (queryType.equals("update")) {
				JSONObject requestData = new JSONObject(request.getRequestData());
				DTObject inputDTO = new DTObject();
				inputDTO.set("WALLET_NO", (String) requestData.get("walletNumber"));
				inputDTO.set("ACCOUNT_NO", (String) requestData.get("accountNumber"));
				updateStatus(inputDTO);
			}

		} catch (Exception e) {
			e.printStackTrace();
			getLogger().logError(e.getLocalizedMessage());
			getErrorMap().setError(InterfaceConstants.ERROR, MiddlewareErrorCodes.UNSPECIFIED_ERROR);
		}
		response.setErrorMap(getErrorMap().getMap());
		return response;
	}

	private String getWallets(DTObject inputDTO) {
		String result = "";
		String inputAccountNumber = inputDTO.get("ACCOUNT_NO");
		String inputCifNumber = inputDTO.get("CIF_NO");
		String inputPinCode = inputDTO.get("PIN_CODE");
		String inputMobileNumber = inputDTO.get("MOBILE_NO");
		String inputStatus = inputDTO.get("STATUS");
		String sqlQuery = "SELECT W.WALLET_NO,W.ACCOUNT_NO,FN_FORMAT_AMOUNT(B.AC_BAL,W.WALLET_CURR,CR.SUB_CCY_UNITS_IN_CCY) ,B.AC_BAL, B.STATUS, W.ASSIGN_DATE FROM WALLETASSIGNDTL W ,WALLETASSIGN A ,WALLETBALA00 B, CURRENCY CR WHERE W.ENTITY_CODE=? AND A.FIELD_OFFICER =? AND W.ASSIGN_DATE=FN_GETCBD(?) AND W.OFFICE_CODE= '1'  AND W.ENTITY_CODE=A.ENTITY_CODE AND W.OFFICE_CODE=A.OFFICE_CODE AND W.ASSIGN_DATE=A.ASSIGN_DATE AND W.ASSIGN_SL=A.ASSIGN_SL AND W.ENTITY_CODE=B.ENTITY_CODE AND W.WALLET_NO=B.WALLET_NO AND W.ACCOUNT_NO=B.ACCOUNT_NO";
		if (inputAccountNumber != null && !inputAccountNumber.equals("")) {
			sqlQuery = sqlQuery + " AND W.ACCOUNT_NO=?";
		}
		if (inputStatus != null && !inputStatus.equals("")) {
			sqlQuery = sqlQuery + " AND B.STATUS=?";
		}
		String innerQuery = "SELECT C.CIF_NO,CONCAT(C.TITLE_CODE, ' ',C.NAME) NAME,C.ADDRESS,C.GEO_UNIT_ID,C.MOBILE,D.PID_CODE,P.CONCISE_DESCRIPTION ,D.DOCUMENT_NUMBER from CUSTOMERREG C,ACCOUNTHOLDERS H,CIFIDENTIFICATIONDOC D ,PIDCODES P WHERE H.ENTITY_CODE=? and H.ACCOUNT_NO=? and H.DTL_SL=1 AND C.CIF_NO=H.CIF_NO  AND  C.CIF_NO=D.CIF_NO AND D.DOC_SL=1 AND D.PID_CODE=P.PID_CODE AND DOC_USED_FOR_ID_PROOF='1'";
		if (inputCifNumber != null && !inputCifNumber.equals("")) {
			innerQuery = innerQuery + " AND C.CIF_NO=?";
		}
		if (inputPinCode != null && !inputPinCode.equals("")) {
			innerQuery = innerQuery + " AND C.GEO_UNIT_ID=?";
		}
		if (inputMobileNumber != null && !inputMobileNumber.equals("")) {
			innerQuery = innerQuery + " AND C.MOBILE=?";
		}
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		DBUtil utilInner = dbContext.createUtilInstance();
		ResultSet rset;
		ResultSet rsetInner;
		int counter = 0;
		JSONArray walletDetails = new JSONArray();
		JSONArray walletDetailPage = new JSONArray();
		JSONObject walletDetail;
		try {
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, context.getEntityCode());
			// TODO fetch actual user ID
			util.setString(2, "ADMINMAKER");
			util.setString(3, context.getEntityCode());
			int j = 3;
			if (inputAccountNumber != null && !inputAccountNumber.equals("")) {
				util.setString(++j, inputAccountNumber);
			}
			if (inputStatus != null && !inputStatus.equals("")) {
				util.setString(++j, inputStatus);
			}
			rset = util.executeQuery();
			while (rset.next()) {
				String walletNumber = rset.getString(1);
				String accountNumber = rset.getString(2);
				String walletAmount = rset.getString(3);
				String status = rset.getString(5);
				String assignDate = rset.getString(6);
				utilInner.reset();
				utilInner.setSql(innerQuery);
				utilInner.setString(1, context.getEntityCode());
				utilInner.setString(2, accountNumber);
				int k = 2;
				if (inputCifNumber != null && !inputCifNumber.equals("")) {
					utilInner.setString(++k, inputCifNumber);
				}
				if (inputPinCode != null && !inputPinCode.equals("")) {
					utilInner.setString(++k, inputPinCode);
				}
				if (inputMobileNumber != null && !inputMobileNumber.equals("")) {
					utilInner.setString(++k, inputMobileNumber);
				}
				rsetInner = utilInner.executeQuery();
				if (rsetInner.next()) {
					String cifNumber = rsetInner.getString(1);
					String name = rsetInner.getString(2);
					String address = rsetInner.getString(3);
					String pinCode = rsetInner.getString(4);
					String mobileNumber = rsetInner.getString(5);
					String pidCode = rsetInner.getString(6);
					String description = rsetInner.getString(7);
					String documentNumber = rsetInner.getString(8);
					walletDetail = new JSONObject();
					walletDetail.put("accountNumber", accountNumber);
					walletDetail.put("walletNumber", walletNumber);
					walletDetail.put("customerName", name);
					walletDetail.put("mobileNumber", mobileNumber);
					walletDetail.put("cifNumber", cifNumber);
					walletDetail.put("status", getMessageFromStatus(status));
					walletDetail.put("pinCode", pinCode);
					String lines[] = address.split("[\\r\\n]+");
					walletDetail.put("address", lines);
					walletDetail.put("pidCode", pidCode);
					walletDetail.put("description", description);
					walletDetail.put("pidNumber", documentNumber);
					walletDetail.put("walletAmount", walletAmount);
					walletDetail.put("receivedOn", assignDate);
					walletDetailPage.put(walletDetail);
					counter++;
					if (counter % 5 == 0) {
						walletDetails.put(walletDetailPage);
						counter = 0;
						walletDetailPage = new JSONArray();
					}
				}
			}
			if (counter > 0 && counter < 5) {
				walletDetails.put(walletDetailPage);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			dbContext.close();
		}
		return walletDetails.toString();
	}

	private String getMessageFromStatus(String status) {
		String result = "";
		switch (status) {
		case "00":
			result = "Normal";
			break;
		case "01":
			result = "Transferred, but not funded";
			break;
		case "02":
			result = "Pending";
			break;
		case "03":
			result = "Disbursed";
			break;
		}
		return result;
	}

	private void updateStatus(DTObject inputDTO) {
		DBContext dbContext = new DBContext();
		String walletNumber = inputDTO.get("WALLET_NO");
		String accountNumber = inputDTO.get("ACCOUNT_NO");
		DBUtil util = dbContext.createUtilInstance();
		try {
			String sqlQuery = "UPDATE WALLETBALA00 SET STATUS = '03' WHERE ENTITY_CODE = ? AND WALLET_NO=? AND ACCOUNT_NO=?";
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, context.getEntityCode());
			util.setString(2, walletNumber);
			util.setString(3, accountNumber);
			util.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
			try {
				dbContext.rollback();
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		} finally {
			dbContext.close();
		}
	}
}
