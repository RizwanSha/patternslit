/*
 * Copyright (c) 2014, 2015, Patterns Software Design Institute. All
 * rights reserved. Unauthorized copying of this file, via any medium is
 * strictly prohibited. PATTERNS SDI PROPRIETARY/CONFIDENTIAL. Use is
 * subject to license terms.
 */
package com.patterns.mobile.services.handlers.pbs;

import java.sql.ResultSet;

import org.json.JSONObject;

import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.interfaceref.InterfaceConstants;
import patterns.config.framework.service.DTObject;
import patterns.json.JSONArray;

import com.patterns.mobile.services.handlers.GenericInterfaceHandler;
import com.patterns.mobile.services.handlers.MiddlewareErrorCodes;
import com.patterns.mobile.services.pojo.GenericRequest;
import com.patterns.mobile.services.pojo.GenericResponse;

/**
 * The Class WalletEnquiryServiceHandler
 */
public class LookupServiceHandler extends GenericInterfaceHandler<GenericRequest, GenericResponse> {

	private final int limit = 61;

	public LookupServiceHandler() {
		setProcessBO("");
	}

	@SuppressWarnings("unused")
	private int transactionPerPage = 10;

	@Override
	public GenericResponse processQueryRequest(GenericRequest request) throws Exception {
		GenericResponse response = new GenericResponse();
		JSONObject responseData = new JSONObject();
		try {
			JSONObject requestData = new JSONObject(request.getRequestData());
			DTObject inputDTO = new DTObject();
			String helpFilter = requestData.getString("helpFilter");
			inputDTO.set("HELP_FILTER", helpFilter);
			inputDTO.set("HELP_FILTER_COLUMN", requestData.getString("helpFilterColumn"));
			String helpField = requestData.getString("helpField");
			JSONArray jsonRows = new JSONArray();
			JSONArray jsonColumns = new JSONArray();
			JSONArray jsonData = new JSONArray();
			JSONObject column = new JSONObject();
			if (helpField.equals("accountNumber")) {
				column.put("english", "Account Number");
				column.put("hindi", "खाता संख्या");
				jsonColumns.put(column);
				column = new JSONObject();
				column.put("english", "Name");
				column.put("hindi", "नाम");
				jsonColumns.put(column);
				if (!helpFilter.equals("") && helpFilter != null) {
					jsonRows = getAccountNumberRows(inputDTO);
				}
			} else if (helpField.equals("cifNumber")) {
				column.put("english", "CIF Number");
				column.put("hindi", "सीआईएफ संख्या");
				jsonColumns.put(column);
				column = new JSONObject();
				column.put("english", "Name");
				column.put("hindi", "नाम");
				jsonColumns.put(column);
				if (!helpFilter.equals("") && helpFilter != null) {
					jsonRows = getCIFNumberRows(inputDTO);
				}
			}
			jsonData.put(jsonColumns);
			jsonData.put(jsonRows);
			responseData.put("helpData", jsonData);
			response.setResponseData(responseData.toString());
		} catch (Exception e) {
			e.printStackTrace();
			getLogger().logError(e.getLocalizedMessage());
			getErrorMap().setError(InterfaceConstants.ERROR, MiddlewareErrorCodes.UNSPECIFIED_ERROR);
		}
		response.setErrorMap(getErrorMap().getMap());
		return response;
	}

	private JSONArray getAccountNumberRows(DTObject inputDTO) {
		String helpFilter = inputDTO.get("HELP_FILTER");
		String helpFilterColumn = inputDTO.get("HELP_FILTER_COLUMN");
		JSONArray jsonRows = new JSONArray();
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		StringBuffer sqlQuery = new StringBuffer("SELECT ACCOUNT_NO, CONCAT(TITLE_CODE, ' ', NAME) FROM ACCOUNTS WHERE ENTITY_CODE = ? AND ");
		if (helpFilterColumn.equals("1")) {
			sqlQuery.append("ACCOUNT_NO = ? ");
		} else if (helpFilterColumn.equals("2")) {
			sqlQuery.append("NAME LIKE ? ");
			helpFilter = "%" + helpFilter + "%";
		}
		sqlQuery.append(" ORDER BY ACCOUNT_NO LIMIT 60");
		try {
			util.reset();
			util.setSql(sqlQuery.toString());
			util.setString(1, context.getEntityCode());
			util.setString(2, helpFilter);
			ResultSet resultSet = util.executeQuery();
			jsonRows = getResults(resultSet);
		} catch (Exception e) {
			e.printStackTrace();
			getLogger().logError(e.getLocalizedMessage());
		} finally {
			util.reset();
			dbContext.close();
		}
		return jsonRows;
	}

	private JSONArray getCIFNumberRows(DTObject inputDTO) {
		String helpFilter = inputDTO.get("HELP_FILTER");
		String helpFilterColumn = inputDTO.get("HELP_FILTER_COLUMN");
		JSONArray jsonRows = new JSONArray();
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		StringBuffer sqlQuery = new StringBuffer("SELECT CIF_NO, CONCAT(TITLE_CODE, ' ', NAME) FROM CUSTOMERREG WHERE ");
		if (helpFilterColumn.equals("1")) {
			sqlQuery.append("CIF_NO = ? ");
		} else if (helpFilterColumn.equals("2")) {
			sqlQuery.append("NAME LIKE ? ");
			helpFilter = "%" + helpFilter + "%";
		}
		sqlQuery.append(" ORDER BY CIF_NO LIMIT 60");
		try {
			util.reset();
			util.setSql(sqlQuery.toString());
			util.setString(1, helpFilter);
			ResultSet resultSet = util.executeQuery();
			jsonRows = getResults(resultSet);
		} catch (Exception e) {
			e.printStackTrace();
			getLogger().logError(e.getLocalizedMessage());
		} finally {
			util.reset();
			dbContext.close();
		}
		return jsonRows;
	}

	private JSONArray getResults(ResultSet resultSet) throws Exception {
		JSONArray jsonRows = new JSONArray();
		JSONArray jsonRow;
		while (resultSet.next()) {
			jsonRow = new JSONArray();
			jsonRow.put(resultSet.getString(1));
			jsonRow.put(resultSet.getString(2));
			jsonRows.put(jsonRow);
		}
		return jsonRows;
	}
}
