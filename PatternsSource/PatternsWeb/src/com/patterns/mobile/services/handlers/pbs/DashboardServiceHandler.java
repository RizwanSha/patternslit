/*
 * Copyright (c) 2014, 2015, Patterns Software Design Institute. All
 * rights reserved. Unauthorized copying of this file, via any medium is
 * strictly prohibited. PATTERNS SDI PROPRIETARY/CONFIDENTIAL. Use is
 * subject to license terms.
 */
package com.patterns.mobile.services.handlers.pbs;

import java.sql.ResultSet;

import org.json.JSONException;
import org.json.JSONObject;

import patterns.config.framework.database.MiddlewareConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.interfaceref.InterfaceConstants;
import patterns.json.JSONArray;

import com.patterns.mobile.services.handlers.GenericInterfaceHandler;
import com.patterns.mobile.services.handlers.MiddlewareErrorCodes;
import com.patterns.mobile.services.pojo.GenericRequest;
import com.patterns.mobile.services.pojo.GenericResponse;

/**
 * The Class DashboardServiceHandler
 */
public class DashboardServiceHandler extends GenericInterfaceHandler<GenericRequest, GenericResponse> {

	public DashboardServiceHandler() {
		setProcessBO("");
	}

	@Override
	public GenericResponse processQueryRequest(GenericRequest request) throws Exception {
		GenericResponse response = new GenericResponse();
		response.setRequestId(request.getRequestId());
		String queryType = request.getQueryType();
		JSONObject responseData = new JSONObject();
		try {
			if (queryType.equals(MiddlewareConstants.QUERY_TYPE_INIT)) {
				responseData.put("walletDetails", getWalletDetails());
				responseData.put("loginDetails", getLoginDetails());
				responseData.put("mapDetails", getMapDetails());
				response.setResponseData(responseData.toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
			getLogger().logError(e.getLocalizedMessage());
			getErrorMap().setError(InterfaceConstants.ERROR, MiddlewareErrorCodes.UNSPECIFIED_ERROR);
		}
		response.setErrorMap(getErrorMap().getMap());
		return response;
	}

	private String getMapDetails() {
		DBContext dbContext = new DBContext();
		JSONObject mapDetails = new JSONObject();
		try {
			StringBuffer sqlQuery = new StringBuffer("SELECT C.FILE_DATA FROM USERREGIONALLOCDTL UR, GEOREGIONS G LEFT OUTER JOIN CMNFILEINVENTORY C ON C.ENTITY_CODE = G.ENTITY_CODE AND C.FILE_INV_NUM = G.MAP_INV_NO WHERE UR.ENTITY_CODE =?  AND UR.FIELD_USER_ID = ? AND G.ENTITY_CODE = UR.ENTITY_CODE AND G.GEO_REGION_CODE=UR.GEO_REGION_CODE");
			DBUtil util = dbContext.createUtilInstance();
			util.reset();
			util.setSql(sqlQuery.toString());
			util.setString(1, context.getEntityCode());
			util.setString(2, context.getUserID());
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				mapDetails.put("overlayObject", new String(rset.getBytes(1)));
				JSONArray walletDetails = getWallets();
				mapDetails.put("markerDetails", walletDetails);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			dbContext.close();
		}
		return mapDetails.toString();
	}

	private JSONArray getWallets() {
		String clusterCode = context.getClusterCode();
		StringBuffer walletQuery = new StringBuffer("SELECT W.WALLET_NO,W.ACCOUNT_NO,FN_FORMAT_AMOUNT(W.WALLET_AMOUNT,W.WALLET_CURR,CR.SUB_CCY_UNITS_IN_CCY), B.STATUS,DECODE_CMLOVREC('COMMON','WALLET_STATUS',B.STATUS) FROM WALLETASSIGNDTL W ,WALLETASSIGN A ,WALLETBAL").append(clusterCode);
		walletQuery.append(" B, CURRENCY CR WHERE W.ENTITY_CODE=? AND A.FIELD_OFFICER =? AND W.ASSIGN_DATE=FN_GETCBD(?) AND W.OFFICE_CODE= ?  AND W.ENTITY_CODE=A.ENTITY_CODE AND W.OFFICE_CODE=A.OFFICE_CODE AND W.ASSIGN_DATE=A.ASSIGN_DATE AND W.ASSIGN_SL=A.ASSIGN_SL AND W.ENTITY_CODE=B.ENTITY_CODE AND W.WALLET_NO=B.WALLET_NO AND W.ACCOUNT_NO=B.ACCOUNT_NO");
		walletQuery.append(" AND B.STATUS IN ('02','03','05')");

		StringBuffer customerQuery = new StringBuffer("SELECT CONCAT(C.TITLE_CODE, ' ',C.NAME) NAME,C.ADDRESS,C.GEO_LOCATION_LAT,C.GEO_LOCATION_LONG FROM ACCOUNTHOLDERS H,CUSTOMERREG C WHERE H.ENTITY_CODE =?  AND H.ACCOUNT_NO = ? AND H.DTL_SL = 1 AND H.CIF_NO = C.CIF_NO");
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		DBUtil utilInner = dbContext.createUtilInstance();
		JSONArray walletDetailArray = new JSONArray();
		try {
			util.reset();
			util.setSql(walletQuery.toString());
			util.setString(1, context.getEntityCode());
			util.setString(2, context.getUserID());
			util.setString(3, context.getEntityCode());
			util.setString(4, context.getBranchCode());
			ResultSet rset = util.executeQuery();
			while (rset.next()) {
				JSONObject walletDetails = new JSONObject();
				walletDetails.put("walletNumber", rset.getString(1));
				String accountNumber = rset.getString(2);
				walletDetails.put("accountNumber", rset.getString(2));
				walletDetails.put("amount", rset.getString(3));
				walletDetails.put("status", rset.getString(4));
				walletDetails.put("statusDescription", rset.getString(5));
				utilInner.reset();
				utilInner.setSql(customerQuery.toString());
				utilInner.setString(1, context.getEntityCode());
				utilInner.setString(2, accountNumber);
				ResultSet rsetInner = utilInner.executeQuery();
				if (rsetInner.next()) {
					walletDetails.put("name", rsetInner.getString(1));
					walletDetails.put("address", rsetInner.getString(2));
					walletDetails.put("lat", rsetInner.getString(3));
					walletDetails.put("lng", rsetInner.getString(4));
				}
				utilInner.reset();
				walletDetailArray.put(walletDetails);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			dbContext.close();
		}
		return walletDetailArray;
	}

	private String getLoginDetails() {
		JSONObject loginDetails = new JSONObject();
		try {
			loginDetails.put("userId", context.getUserID());
			loginDetails.put("userName", context.getUserName());
			Object lastLoginTime = context.getLastLoginDateTime();
			if (lastLoginTime != null) {
				lastLoginTime = lastLoginTime.toString();
			}
			loginDetails.put("lastLogin", lastLoginTime);
			loginDetails.put("currentDate", context.getCurrentBusinessDate());
			loginDetails.put("roleDescription", context.getRoleDescription());
			loginDetails.put("roleCode", context.getRoleCode());
			loginDetails.put("branchCode", context.getBranchCode());
			loginDetails.put("branchDescription", context.getBranchDescription());
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return loginDetails.toString();
	}

	private String getWalletDetails() {
		DBContext dbContext = new DBContext();
		JSONObject walletDetails = new JSONObject();
		try {
			StringBuffer sqlQuery = new StringBuffer("SELECT B.STATUS, COALESCE(SUM(W.WALLET_AMOUNT),0), COUNT(1) FROM WALLETASSIGNDTL W ,WALLETASSIGN A ,WALLETBAL");
			sqlQuery.append(context.getClusterCode());
			sqlQuery.append(" B WHERE A.ENTITY_CODE=? AND A.OFFICE_CODE= ? AND A.FIELD_OFFICER =? AND A.ASSIGN_DATE=FN_GETCBD(?) AND W.ENTITY_CODE=A.ENTITY_CODE AND W.OFFICE_CODE=A.OFFICE_CODE AND W.ASSIGN_DATE=A.ASSIGN_DATE AND W.ASSIGN_SL=A.ASSIGN_SL AND W.ENTITY_CODE=B.ENTITY_CODE AND W.WALLET_NO=B.WALLET_NO AND W.WALLET_CURR=B.CURR_CODE GROUP BY B.STATUS");
			DBUtil util = dbContext.createUtilInstance();
			JSONObject walletDetail;
			util.reset();
			util.setSql(sqlQuery.toString());
			util.setString(1, context.getEntityCode());
			util.setString(2, context.getBranchCode());
			util.setString(3, context.getUserID());
			util.setString(4, context.getEntityCode());
			ResultSet rset = util.executeQuery();
			while (rset.next()) {
				walletDetail = new JSONObject();
				String status = rset.getString(1);
				walletDetail.put("status", status);
				walletDetail.put("amount", rset.getString(2));
				walletDetail.put("count", rset.getString(3));
				walletDetails.put(status, walletDetail);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			dbContext.close();
		}
		return walletDetails.toString();
	}
}
