/*
 * Copyright (c) 2014, 2015, Patterns Software Design Institute. All
 * rights reserved. Unauthorized copying of this file, via any medium is
 * strictly prohibited. PATTERNS SDI PROPRIETARY/CONFIDENTIAL. Use is
 * subject to license terms.
 */
package com.patterns.mobile.services.handlers.pbs;

import patterns.config.framework.interfaceref.InterfaceConstants;
import patterns.config.framework.service.DTDObject;
import patterns.config.framework.service.DTObject;
import patterns.json.JSONArray;
import patterns.json.JSONObject;

import com.patterns.mobile.services.handlers.GenericInterfaceHandler;
import com.patterns.mobile.services.handlers.MiddlewareErrorCodes;
import com.patterns.mobile.services.lookup.MobileLookupService;
import com.patterns.mobile.services.pojo.GenericRequest;
import com.patterns.mobile.services.pojo.GenericResponse;

/**
 * The Class AccountSummaryServiceHandler
 */
public class CustomerRegistrationServiceHandler extends GenericInterfaceHandler<GenericRequest, GenericResponse> {

	public CustomerRegistrationServiceHandler() {
		setProcessBO("");
	}

	@SuppressWarnings("unused")
	private int transactionPerPage = 10;

	@Override
	public GenericResponse processQueryRequest(GenericRequest request) throws Exception {
		GenericResponse response = new GenericResponse();

		try {
			response.setRequestId(request.getRequestId());
			String appId = getApplicationKey();
			if (isEmpty(request.getApplicationKey()) || (!request.getApplicationKey().equals(appId))) {
				getErrorMap().setError(InterfaceConstants.ERROR, MiddlewareErrorCodes.INVALID_APP_KEY);
				response.setErrorMap(getErrorMap().getMap());
				return response;
			}
			String queryType = request.getQueryType();
			JSONObject responseData = new JSONObject();
			if (queryType.equals("init")) {
				MobileLookupService lookupService = new MobileLookupService();
				responseData.put("pidCodes", lookupService.getPidCodes());
				// responseData.put("pinCodes", lookupService.getPinCodes());
				response.setResponseData(responseData.toString());
			} else if (queryType.equals("lookup")) {
				DTObject fields = new DTObject();
				fields.set("FIELD_NAME", request.getLookupField());
				fields.set("FIELD_VALUES", request.getRequestData());
				MobileLookupService lookupService = new MobileLookupService();
				response.setResponseData(lookupService.getLookupDetails(fields));
			}
		} catch (Exception e) {
			e.printStackTrace();
			getLogger().logError(e.getLocalizedMessage());
			getErrorMap().setError(InterfaceConstants.ERROR, MiddlewareErrorCodes.UNSPECIFIED_ERROR);
		}
		response.setErrorMap(getErrorMap().getMap());
		return response;
	}

	@Override
	public DTObject processSubmitRequest(GenericRequest request) throws Exception {
		JSONObject requestData = new JSONObject(request.getRequestData());
		DTObject formDTO = new DTObject();
		if (validate(requestData)) {
			formDTO.set("ENTITY_CODE", "1");
			formDTO.set("BRANCH_NO", "1");
			formDTO.set("TITLE_CODE", requestData.getString("customerTitle"));
			formDTO.set("NAME", requestData.getString("customerName"));
			formDTO.set("GENDER", requestData.getString("gender"));
			formDTO.set("MARITAL_STATUS", requestData.getString("maritalStatus"));
			formDTO.set("LEGAL_CONN_FLAG", requestData.getString("relationshipType"));
			formDTO.set("LEGAL_CONN_NAME", requestData.getString("relationName"));
			formDTO.set("ADDRESS", requestData.getString("relationName"));
			formDTO.set("GEO_UNIT_ID", requestData.getString("relationName"));
			formDTO.set("YEAR_OF_BIRTH", requestData.getString("relationName"));
			formDTO.set("DATE_OF_BIRTH", requestData.getString("relationName"));
			formDTO.set("MOBILE", requestData.getString("relationName"));
			formDTO.set("EMAIL_ID", requestData.getString("relationName"));
			formDTO.set("REMARKS", "patterns.config.framework.bo.casa.ecustomerregBO");
			formDTO.set("PROCESS_ID", "ECUSTOMERREG");
			formDTO.set("CREATED_BY", "MOBILE_USER");
			DTDObject dtdObject = new DTDObject();
			dtdObject.addColumn(0, "SELECT");
			dtdObject.addColumn(1, "ACTION");
			dtdObject.addColumn(2, "PID_CODE");
			dtdObject.addColumn(3, "DESCRIPTION");
			dtdObject.addColumn(4, "DOCUMENT_NUMBER");
			dtdObject.addColumn(5, "DOCUMENT_ISSUE_DATE");
			dtdObject.addColumn(6, "DOCUMENT_EXPIRY_DATE");
			dtdObject.addColumn(7, "DOC_USED_FOR_ADDR_PROOF");
			dtdObject.addColumn(8, "DOC_USED_FOR_ID_PROOF");
			JSONArray documentsSubmitted;
			documentsSubmitted = new JSONArray(requestData.getString("documentsSubmitted"));
			JSONObject documentSubmitted;
			for (int i = 0; i < documentsSubmitted.length(); i++) {
				documentSubmitted = (JSONObject) documentsSubmitted.get(i);
				dtdObject.addRow();
				dtdObject.setValue(i, 0, "1");
				dtdObject.setValue(i, 1, "A");
				dtdObject.setValue(i, 2, documentSubmitted.getString("documentId"));
				dtdObject.setValue(i, 3, documentSubmitted.getString("description"));
				dtdObject.setValue(i, 4, documentSubmitted.getString("documentNumber"));
				dtdObject.setValue(i, 5, documentSubmitted.getString("dateOfIssue"));
				dtdObject.setValue(i, 6, documentSubmitted.getString("dateOfExpiry"));
				dtdObject.setValue(i, 7, documentSubmitted.getString("addressProof"));
				dtdObject.setValue(i, 8, documentSubmitted.getString("idProof"));
				if (i == 0) {
					formDTO.set("TITLE_CODE", documentSubmitted.getString("title"));
					formDTO.set("NAME", documentSubmitted.getString("name"));
				}
			}
			formDTO.setDTDObject("ECIFIDENTIFICATIONDOC", dtdObject);
		}
		return formDTO;
	}

	private boolean validate(JSONObject requestData) {
		return true;
	};
}
