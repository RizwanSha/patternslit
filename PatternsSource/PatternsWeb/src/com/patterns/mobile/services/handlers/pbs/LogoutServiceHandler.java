/*
 * Copyright (c) 2014, 2015, Patterns Software Design Institute. All
 * rights reserved. Unauthorized copying of this file, via any medium is
 * strictly prohibited. PATTERNS SDI PROPRIETARY/CONFIDENTIAL. Use is
 * subject to license terms.
 */
package com.patterns.mobile.services.handlers.pbs;

import patterns.config.delegate.BackOfficeProcessManager;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.interfaceref.InterfaceConstants;
import patterns.config.framework.process.TBAProcessResult;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTObject;
import patterns.config.validations.MiddlewareValidator;

import com.patterns.mobile.services.handlers.GenericInterfaceHandler;
import com.patterns.mobile.services.pojo.GenericResponse;
import com.patterns.mobile.services.pojo.pbs.LoginRequest;

/**
 * The Class AccountSummaryServiceHandler
 */
public class LogoutServiceHandler extends GenericInterfaceHandler<LoginRequest, GenericResponse> {

	public LogoutServiceHandler() {
		setProcessBO("patterns.config.framework.bo.mobile.LogoutBO");
	}

	@SuppressWarnings("unused")
	private int transactionPerPage = 10;

	@Override
	public GenericResponse processQueryRequest(LoginRequest request) throws Exception {
		GenericResponse response = new GenericResponse();
		return response;
	}

	@Override
	public GenericResponse processLoginRequest(LoginRequest request) throws Exception {
		GenericResponse response = new GenericResponse();
		MiddlewareValidator validator = new MiddlewareValidator();
		try {
			DTObject formDTO = new DTObject();
			String userID = context.getUserID();
			formDTO.set("P_ENTITY_CODE", context.getEntityCode());
			formDTO.set("P_USER_ID", userID);
			formDTO.setObject("P_LOGIN_DATE", context.getLoginDateTime());
			formDTO.set("P_REASON", RegularConstants.MANUAL_LOGOUT);
			formDTO.set("P_SESSION_ID", webContext.getSession().getId());
			formDTO.set(RegularConstants.PROCESSBO_CLASS, "patterns.config.framework.bo.common.elogoutBO");
			BackOfficeProcessManager processManager = new BackOfficeProcessManager();
			TBAProcessResult processResult = processManager.delegate(formDTO);
			if (processResult.getProcessStatus().equals(TBAProcessStatus.FAILURE)) {
				getErrorMap().setError(InterfaceConstants.ERROR, processResult.getErrorCode());
			} else {
				if ((processResult.getErrorCode() != null && !processResult.getErrorCode().isEmpty())) {
					getErrorMap().setError(InterfaceConstants.ERROR, processResult.getErrorCode());
				}
			}
		} catch (Exception e) {
			getErrorMap().setError(InterfaceConstants.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return response;
	}
}