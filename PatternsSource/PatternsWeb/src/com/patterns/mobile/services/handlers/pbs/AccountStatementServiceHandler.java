/*
 * Copyright (c) 2014, 2015, Patterns Software Design Institute. All
 * rights reserved. Unauthorized copying of this file, via any medium is
 * strictly prohibited. PATTERNS SDI PROPRIETARY/CONFIDENTIAL. Use is
 * subject to license terms.
 */
package com.patterns.mobile.services.handlers.pbs;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Types;

import org.json.JSONObject;

import patterns.config.framework.database.MiddlewareConstants;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.interfaceref.InterfaceConstants;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.framework.web.reports.ReportRM;
import patterns.config.validations.MiddlewareValidator;
import patterns.json.JSONArray;

import com.patterns.mobile.services.handlers.GenericInterfaceHandler;
import com.patterns.mobile.services.handlers.MiddlewareErrorCodes;
import com.patterns.mobile.services.lookup.MobileLookupService;
import com.patterns.mobile.services.pojo.GenericRequest;
import com.patterns.mobile.services.pojo.GenericResponse;

/**
 * The Class AccountStatementServiceHandler
 */
public class AccountStatementServiceHandler extends GenericInterfaceHandler<GenericRequest, GenericResponse> {

	public AccountStatementServiceHandler() {
		setProcessBO("");
	}

	@Override
	public GenericResponse processQueryRequest(GenericRequest request) throws Exception {
		GenericResponse response = new GenericResponse();
		try {
			String queryType = request.getQueryType();
			JSONObject responseData = new JSONObject();
			if (queryType.equals(MiddlewareConstants.QUERY_TYPE_INIT)) {
				MobileLookupService lookupService = new MobileLookupService();
				// responseData.put("accountNumbers",
				// lookupService.getAccountNumbers());
				response.setResponseData(responseData.toString());
			} else if (queryType.equals(MiddlewareConstants.QUERY_TYPE_GRIDQUERY)) {
				JSONObject requestData = new JSONObject(request.getRequestData());
				String accountNumber = (String) requestData.get("accountNumber");
				String fromDate = (String) requestData.get("fromDate");
				String toDate = (String) requestData.get("toDate");
				DTObject inputDTO = new DTObject();
				inputDTO.set("ENTITY_CODE", context.getEntityCode());
				inputDTO.set("ACCOUNT_NO", accountNumber);
				inputDTO.set("FROM_DATE", fromDate);
				inputDTO.set("UPTO_DATE", toDate);
				inputDTO.set("CCY_CODE", "INR");
				inputDTO.set("SUB_CCY_UNITS_IN_CCY", "2");
				inputDTO.set("BRANCH_CODE", context.getBranchCode());
				inputDTO.set("CLUSTER_CODE", context.getClusterCode());
				// inputDTO.setObject("CBD_DATE",
				// context.getCurrentBusinessDate());
				inputDTO.set("DATE_FORMAT", "%d-%m-%Y");

				DTObject result = loadAccountStatments(inputDTO);
				if (result == null || result.containsKey(InterfaceConstants.ERROR)) {
					getErrorMap().setError(InterfaceConstants.ERROR, MiddlewareErrorCodes.NO_RESULT);
				}
				responseData.put("statementDetails", result.get("STATMENT_DETAILS"));
				responseData.put("closingTotal", result.get("CLOSE_TOTAL"));
				responseData.put("openingBalance", result.get("OPEN_BAL"));
				responseData.put("closingBalance", result.get("CLOSE_BAL"));
				response.setResponseData(responseData.toString());
			} else if (queryType.equals(MiddlewareConstants.QUERY_TYPE_LOOKUP)) {
				DTObject fields = new DTObject();
				fields.set("FIELD_NAME", request.getLookupField());
				fields.set("FIELD_VALUES", request.getRequestData());
				MobileLookupService lookupService = new MobileLookupService();
				response.setResponseData(lookupService.getLookupDetails(fields));
			} else if (queryType.equals(MiddlewareConstants.QUERY_TYPE_FETCH)) {
				JSONObject requestData = new JSONObject(request.getRequestData());
				String accountNumber = requestData.getString("accountNumber");
				MiddlewareValidator validator = new MiddlewareValidator();
				String fetchType = requestData.getString("fetchType");
				if (fetchType.equals("accountDetails")) {
					responseData.put("accountDetail", validator.getAccountDetails(accountNumber));
				} else if (fetchType.equals("accountBalance")) {
					responseData.put("accountBalance", validator.getAccountBalance(accountNumber));
				} else if(fetchType.equals("accountWallets")){
					responseData.put("accountWallets", validator.getAccountWallets(accountNumber));
				}
				response.setResponseData(responseData.toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
			getLogger().logError(e.getLocalizedMessage());
			getErrorMap().setError(InterfaceConstants.ERROR, MiddlewareErrorCodes.UNSPECIFIED_ERROR);
		}
		response.setErrorMap(getErrorMap().getMap());
		return response;
	}

	private DTObject loadAccountStatments(DTObject inputDTO) {
		DTObject result = new DTObject();
		DBContext dbContext = new DBContext();
		try {
			DBUtil dbUtil = dbContext.createUtilInstance();
			StringBuffer sqlQuery = new StringBuffer(RegularConstants.EMPTY_STRING);
			BigDecimal op_balance = new BigDecimal("0");
			BigDecimal cl_balance = new BigDecimal("0");
			String ccy = "", units = "";
			ccy = inputDTO.get("CCY_CODE");
			units = inputDTO.get("SUB_CCY_UNITS_IN_CCY");

			dbUtil.reset();
			dbUtil.setMode(DBUtil.CALLABLE);
			dbUtil.setSql("CALL SP_GET_ACNT_STMT(?,?,?,?,?,?,?,?,?,?,?,?)");
			dbUtil.setLong(1, Long.parseLong(context.getEntityCode()));
			dbUtil.setLong(2, Long.parseLong(inputDTO.get("ACCOUNT_NO")));
			dbUtil.setString(3, inputDTO.get("CCY_CODE"));
			dbUtil.setString(4, inputDTO.get("BRANCH_CODE"));
			dbUtil.setString(5, inputDTO.get("CLUSTER_CODE"));
			dbUtil.setDate(6, new java.sql.Date(BackOfficeFormatUtils.getDate(inputDTO.get("FROM_DATE"), inputDTO.get("DATE_FORMAT")).getTime()));
			dbUtil.setDate(7, new java.sql.Date(BackOfficeFormatUtils.getDate(inputDTO.get("UPTO_DATE"), inputDTO.get("DATE_FORMAT")).getTime()));
			dbUtil.setDate(8, context.getCurrentBusinessDate());
			dbUtil.registerOutParameter(9, Types.VARCHAR);
			dbUtil.registerOutParameter(10, Types.BIGINT);
			dbUtil.registerOutParameter(11, Types.DECIMAL);
			dbUtil.registerOutParameter(12, Types.DECIMAL);
			dbUtil.execute();
			String status = dbUtil.getString(9);
			if (!status.equals(RegularConstants.SP_SUCCESS)) {
				inputDTO.set("SP_ERROR", status);
				return inputDTO;
			}
			Long serial = dbUtil.getLong(10);
			op_balance = dbUtil.getBigDecimal(11);
			cl_balance = dbUtil.getBigDecimal(12);

			dbUtil.reset();
			sqlQuery.append("SELECT TO_CHAR(TRAN_DATE,?)TRAN_DATE,TRAN_MODE,DB_CR_FLG,");
			sqlQuery.append(" COALESCE(DEPOSITS,0) DEPOSITS,COALESCE(WITHDRAWALS,0) WITHDRAWALS,");
			sqlQuery.append(" DEPOSITS_F,WITHDRAWALS_F,");
			sqlQuery.append(" CHEQUE_NUMBER,PARTICULARS");
			sqlQuery.append(" FROM RTMP_ACNTSTMT");
			sqlQuery.append(" WHERE ENTITY_CODE = ? AND INV_SL = ?");
			dbUtil.setSql(sqlQuery.toString());
			dbUtil.setString(1, inputDTO.get("DATE_FORMAT"));
			dbUtil.setLong(2, Long.parseLong(context.getEntityCode()));
			dbUtil.setLong(3, serial);
			ResultSet rset = dbUtil.executeQuery();
			int rowNo = 1;
			rset.last();
			int rowCount = rset.getRow();
			rset.beforeFirst();
			BigDecimal deposits_page_total = new BigDecimal("0");
			BigDecimal withdrawals_page_total = new BigDecimal("0");
			BigDecimal tmp_op_bal = op_balance;

			JSONArray openingBalance = new JSONArray();
			JSONArray openingBalanceSub;
			JSONArray transactions;
			JSONArray transactionsSub = new JSONArray();
			JSONArray closingTotal = new JSONArray();
			JSONArray closingTotalSub;
			JSONArray allTran = new JSONArray();
			while (rset.next()) {
				/* Opening Balance Begin */
				if (rowNo % 10 == 1) {
					openingBalanceSub = new JSONArray();
					if (tmp_op_bal.compareTo(BigDecimal.ZERO) >= 0) {
						openingBalanceSub.put(ReportRM.fmc(tmp_op_bal, ccy, units));
						openingBalanceSub.put("");
					} else {
						openingBalanceSub.put("");
						openingBalanceSub.put(ReportRM.fmc(tmp_op_bal, ccy, units));
					}
					openingBalance.put(openingBalanceSub);
				}
				/* Opening Balance End */

				transactions = new JSONArray();
				transactions.put(rset.getString("TRAN_DATE"));
				transactions.put(getTranMode(rset.getString("TRAN_MODE"), rset.getString("DB_CR_FLG")));
				transactions.put(rset.getString("CHEQUE_NUMBER"));
				transactions.put(rset.getString("PARTICULARS"));
				transactions.put(rset.getString("DEPOSITS_F"));
				transactions.put(rset.getString("WITHDRAWALS_F"));
				transactionsSub.put(transactions);
				if (rowNo % 8 == 0 || rowCount == rowNo) {
					allTran.put(transactionsSub);
					transactionsSub = new JSONArray();
				}
				withdrawals_page_total = withdrawals_page_total.add(rset.getBigDecimal("WITHDRAWALS"));
				deposits_page_total = deposits_page_total.add(rset.getBigDecimal("DEPOSITS"));

				/* Closing Total begin */
				if (rowNo % 8 == 0 || rowCount == rowNo) {
					if (tmp_op_bal.compareTo(BigDecimal.ZERO) >= 0)
						deposits_page_total = deposits_page_total.add(tmp_op_bal);
					else
						withdrawals_page_total = withdrawals_page_total.add(tmp_op_bal);

					closingTotalSub = new JSONArray();
					closingTotalSub.put(ReportRM.fmc(deposits_page_total, ccy, units));
					closingTotalSub.put(ReportRM.fmc(withdrawals_page_total, ccy, units));
					closingTotal.put(closingTotalSub);
					tmp_op_bal = deposits_page_total.subtract(withdrawals_page_total);
					deposits_page_total = BigDecimal.ZERO;
					withdrawals_page_total = BigDecimal.ZERO;
				}
				/* Closing Total end */
				rowNo++;
			}
			dbUtil.reset();
			/* Closing Balance begin */
			JSONArray closingBalance = new JSONArray();
			if (cl_balance.compareTo(BigDecimal.ZERO) >= 0) {
				closingBalance.put(ReportRM.fmc(cl_balance, ccy, units));
				closingBalance.put("");
			} else {
				closingBalance.put("");
				closingBalance.put(ReportRM.fmc(cl_balance, ccy, units));
			}
			/* Closing Balance end */

			result.set("OPEN_BAL", openingBalance.toString());
			result.set("STATMENT_DETAILS", allTran.toString());
			result.set("CLOSE_TOTAL", closingTotal.toString());
			result.set("CLOSE_BAL", closingBalance.toString());
		} catch (Exception e) {
			e.printStackTrace();
			getLogger().logError(e.getLocalizedMessage());
			getErrorMap().setError(InterfaceConstants.ERROR, MiddlewareErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			dbContext.close();
		}
		return result;
	}

	String getTranMode(String mode, String flag) {
		String value = "";
		if (mode != null) {
			if (mode.equals("1") && flag.equals("D"))
				value = "TO TRF";
			else if (mode.equals("1") && flag.equals("C"))
				value = "BY TRF";
			else if (mode.equals("2") && flag.equals("D"))
				value = "TO CLG";
			else if (mode.equals("2") && flag.equals("C"))
				value = "BY CLG";
			else if (mode.equals("3") && flag.equals("D"))
				value = "TO CASH";
			else if (mode.equals("3") && flag.equals("C"))
				value = "BY CASH";
		}
		return value;
	}
}
