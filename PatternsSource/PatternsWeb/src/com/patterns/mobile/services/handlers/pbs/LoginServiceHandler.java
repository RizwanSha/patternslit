/*
 * Copyright (c) 2014, 2015, Patterns Software Design Institute. All
 * rights reserved. Unauthorized copying of this file, via any medium is
 * strictly prohibited. PATTERNS SDI PROPRIETARY/CONFIDENTIAL. Use is
 * subject to license terms.
 */
package com.patterns.mobile.services.handlers.pbs;

import java.sql.ResultSet;
import java.util.Date;

import patterns.config.delegate.BackOfficeProcessManager;
import patterns.config.delegate.ServiceExecutor;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.MiddlewareConstants;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.interfaceref.InterfaceConstants;
import patterns.config.framework.process.TBAProcessResult;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.PasswordUtils;
import patterns.config.framework.web.SessionConstants;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.AccessValidator;
import patterns.config.validations.MiddlewareValidator;
import patterns.json.JSONObject;

import com.patterns.mobile.services.handlers.GenericInterfaceHandler;
import com.patterns.mobile.services.handlers.MiddlewareErrorCodes;
import com.patterns.mobile.services.pojo.GenericResponse;
import com.patterns.mobile.services.pojo.pbs.LoginRequest;

/**
 * The Class AccountSummaryServiceHandler
 */
public class LoginServiceHandler extends GenericInterfaceHandler<LoginRequest, GenericResponse> {

	public LoginServiceHandler() {
		setProcessBO("patterns.config.framework.bo.mobile.LoginBO");
	}

	@SuppressWarnings("unused")
	private int transactionPerPage = 10;

	@Override
	public GenericResponse processQueryRequest(LoginRequest request) throws Exception {
		GenericResponse response = new GenericResponse();
		return response;
	}

	@Override
	public GenericResponse processLoginRequest(LoginRequest request) throws Exception {
		GenericResponse response = new GenericResponse();
		MiddlewareValidator validator = new MiddlewareValidator();
		try {
			String loginType = request.getLoginType();
			DTObject formDTO = new DTObject();
			String userID = request.getUserId();
			JSONObject responseData = new JSONObject();
			if (loginType.equals(MiddlewareConstants.LOGIN_TYPE_INIT)) {
				formDTO.set("P_PARTITION_NO", context.getPartitionNo());
				formDTO.set("P_USER_ID", userID);
				formDTO.set("P_USER_IP", webContext.getRequest().getRemoteAddr());
				formDTO.set("P_USER_AGENT", webContext.getRequest().getHeader("User-Agent"));
				formDTO.set("P_LOGIN_SESSION_ID", webContext.getSession().getId());
				formDTO.set("P_SERVER_ADDR", webContext.getRequest().getServerName());
				formDTO.set(RegularConstants.PROCESSBO_CLASS, "patterns.config.framework.bo.common.eloginBO");
				BackOfficeProcessManager processManager = new BackOfficeProcessManager();
				TBAProcessResult processResult = processManager.delegate(formDTO);
				if (processResult.getProcessStatus().equals(TBAProcessStatus.FAILURE)) {
					getErrorMap().setError(InterfaceConstants.ERROR, processResult.getErrorCode());
				} else {
					if ((processResult.getErrorCode() != null && !processResult.getErrorCode().isEmpty())) {
						getErrorMap().setError(InterfaceConstants.ERROR, processResult.getErrorCode());
					} else {
						DTObject resultDTO = (DTObject) processResult.getAdditionalInfo();
						formDTO.reset();
						formDTO.set("USER_ID", userID);
						formDTO.set("REQ_ID", request.getRequestId());
						formDTO.set("STEP", "1");
						updateDatabase(formDTO);

						// updateLoginInitSession(userID);
						responseData.put("passwordSalt", resultDTO.get(SessionConstants.PASSWORD_SALT));
						String randomSalt = PasswordUtils.getSalt();
						webContext.getSession().setAttribute(SessionConstants.RANDOM_SALT, randomSalt);
						responseData.put("randomSalt", randomSalt);
						response.setResponseData(responseData.toString());
					}
				}
			} else if (loginType.equals(MiddlewareConstants.LOGIN_TYPE_AUTH)) {
				String passwordValid = decodeBooleanToString(isPasswordValid(userID, request.getHashedPassword()));
				passwordValid = "1";
				formDTO.set("P_ENTITY_CODE", context.getEntityCode());
				formDTO.set("P_PARTITION_NO", context.getPartitionNo());
				formDTO.set("P_USER_ID", userID);
				formDTO.set("P_FIRST_PIN_VALID", passwordValid);
				formDTO.set("P_TFA_VALID", "1");
				formDTO.set("P_USER_IP", webContext.getRequest().getRemoteAddr());
				formDTO.set("P_USER_AGENT", webContext.getRequest().getHeader("User-Agent"));
				formDTO.set("P_LOGIN_SESSION_ID", webContext.getSession().getId());
				formDTO.set("P_SERVER_ADDR", webContext.getRequest().getServerName());
				formDTO.set("P_MULTI_SESSION_CHECK", "0");
				formDTO.set(RegularConstants.PROCESSBO_CLASS, "patterns.config.framework.bo.common.eloginauthBO");
				BackOfficeProcessManager processManager = new BackOfficeProcessManager();
				TBAProcessResult processResult = processManager.delegate(formDTO);
				if (processResult.getProcessStatus().equals(TBAProcessStatus.FAILURE)) {
					getErrorMap().setError(InterfaceConstants.ERROR, processResult.getErrorCode());
				} else {
					if (processResult.getErrorCode() != null && !processResult.getErrorCode().isEmpty()) {
						getErrorMap().setError(InterfaceConstants.ERROR, processResult.getErrorCode());
					} else {
						DTObject resultDTO = (DTObject) processResult.getAdditionalInfo();
						formDTO.reset();
						formDTO.set("ROLE_CODE", resultDTO.get(SessionConstants.ROLE_CODE));
						formDTO = validator.validateUserRole(formDTO);
						if (formDTO.get(ContentManager.ERROR) != null) {
							getErrorMap().setError(InterfaceConstants.ERROR, MiddlewareErrorCodes.FIELD_ROLE_NOT_ENABLED);
							return response;
						}
						if (formDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
							getErrorMap().setError(InterfaceConstants.ERROR, MiddlewareErrorCodes.FIELD_ROLE_NOT_ENABLED);
							return response;
						}
						formDTO.set("BRANCH_CODE", resultDTO.get(SessionConstants.BRANCH_CODE));
						formDTO = validator.fetchAdditionalDetails(formDTO);
						if (formDTO.get(ContentManager.ERROR) != null) {
							getErrorMap().setError(InterfaceConstants.ERROR, MiddlewareErrorCodes.FIELD_ROLE_NOT_ENABLED);
							return response;
						}
						if (formDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
							getErrorMap().setError(InterfaceConstants.ERROR, MiddlewareErrorCodes.FIELD_ROLE_NOT_ENABLED);
							return response;
						}

						validator.close();

						String sessionId = getSessionSequence();
						formDTO.set("SESSION_ID", sessionId);
						formDTO.set("USER_ID", resultDTO.get(SessionConstants.USER_ID));
						formDTO.set("USER_NAME", resultDTO.get(SessionConstants.USER_NAME));

						formDTO.set("BRANCH_DESC", resultDTO.get("BRANCH_DESC"));
						formDTO.set("CLUSTER_CODE", resultDTO.get(SessionConstants.CLUSTER_CODE));
						formDTO.setObject("LAST_LOGIN_DATE_TIME", resultDTO.getObject(SessionConstants.LAST_LOGIN_DATE_TIME));
						formDTO.set("FIN_YEAR", resultDTO.get(SessionConstants.FIN_YEAR));
						formDTO.set("ROLE_CODE", resultDTO.get(SessionConstants.ROLE_CODE));
						formDTO.set("REQ_ID", request.getRequestId());
						formDTO.setObject("LOGIN_DATE_TIME", resultDTO.getObject(SessionConstants.LOGIN_DATE_TIME));
						formDTO.set("APPL_EXT_APP_CODE", context.getMobileContextParam().get("APPL_EXT_APP_CODE"));
						formDTO.set("STEP", "2");
						formDTO.set("DATE_FORMAT", resultDTO.get(SessionConstants.DATE_FORMAT));
						updateDatabase(formDTO);

						responseData.put("sessionId", sessionId);
						response.setResponseData(responseData.toString());
					}
				}
			}
		} catch (Exception e) {
			getErrorMap().setError(InterfaceConstants.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return response;
	}

	private boolean updateDatabase(DTObject inputDTO) {
		try {
			inputDTO.set(RegularConstants.PROCESSBO_CLASS, getProcessBO());
			inputDTO.set("ENTITY_CODE", context.getEntityCode());
			ServiceExecutor executor = new ServiceExecutor();
			TBAProcessResult executionResult = executor.delegate(inputDTO);
			if (executionResult.getProcessStatus().equals(TBAProcessStatus.SUCCESS)) {
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	private boolean isPasswordValid(String userID, String hashedPassword) {
		String randomSalt = (String) webContext.getSession().getAttribute(SessionConstants.RANDOM_SALT);
		String userPassword = RegularConstants.EMPTY_STRING;
		AccessValidator validator = new AccessValidator();
		try {
			userPassword = validator.getInternalUserPassword(userID);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			validator.close();
		}
		String encryptedValue = PasswordUtils.getEncryptedHashSalt(userPassword, randomSalt);
		if (encryptedValue.equals(hashedPassword)) {
			return true;
		} else {
			return false;
		}
	}

	private String getSessionSequence() {
		String session = "";
		DBContext dbContext = new DBContext();
		try {
			DBUtil util = dbContext.createUtilInstance();
			String sqlQuery = "SELECT FN_NEXTVAL('MOBILE_SESSION_SEQ') AS INV_SEQ FROM DUAL";
			util.reset();
			util.setSql(sqlQuery);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				session = context.getMobileContextParam().get("APPL_EXT_APP_CODE") + "." + rset.getString(1) + new Date().getTime();
			}
		} catch (Exception e) {
			e.printStackTrace();
			try {
				dbContext.rollback();
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		} finally {
			dbContext.close();
		}
		return session;
	}
}
