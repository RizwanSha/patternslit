/*
 * Copyright (c) 2014, 2015, Patterns Software Design Institute. All
 * rights reserved. Unauthorized copying of this file, via any medium is
 * strictly prohibited. PATTERNS SDI PROPRIETARY/CONFIDENTIAL. Use is
 * subject to license terms.
 */
package com.patterns.mobile.services.handlers.pbs;

import java.util.List;

import patterns.config.delegate.BackOfficeProcessManager;
import patterns.config.framework.bo.MessageParam;
import patterns.config.framework.database.MiddlewareConstants;
import patterns.config.framework.interfaceref.InterfaceConstants;
import patterns.config.framework.process.TBAActionType;
import patterns.config.framework.process.TBAProcessAction;
import patterns.config.framework.process.TBAProcessInfo;
import patterns.config.framework.process.TBAProcessResult;
import patterns.config.framework.service.DTDObject;
import patterns.config.framework.service.DTObject;
import patterns.json.JSONArray;
import patterns.json.JSONObject;

import com.patterns.mobile.services.handlers.GenericInterfaceHandler;
import com.patterns.mobile.services.handlers.MiddlewareErrorCodes;
import com.patterns.mobile.services.lookup.MobileLookupService;
import com.patterns.mobile.services.pojo.GenericRequest;
import com.patterns.mobile.services.pojo.GenericResponse;

/**
 * The Class AccountOpeningServiceHandler
 */
public class AccountOpeningServiceHandler extends GenericInterfaceHandler<GenericRequest, GenericResponse> {

	public AccountOpeningServiceHandler() {
		setProcessBO("");
	}

	@Override
	public GenericResponse processQueryRequest(GenericRequest request) throws Exception {
		GenericResponse response = new GenericResponse();

		try {
			response.setRequestId(request.getRequestId());
			String appId = getApplicationKey();
			if (isEmpty(request.getApplicationKey()) || (!request.getApplicationKey().equals(appId))) {
				getErrorMap().setError(InterfaceConstants.ERROR, MiddlewareErrorCodes.INVALID_APP_KEY);
				response.setErrorMap(getErrorMap().getMap());
				return response;
			}
			String queryType = request.getQueryType();
			JSONObject responseData = new JSONObject();
			if (queryType.equals(MiddlewareConstants.QUERY_TYPE_INIT)) {

				MobileLookupService lookupService = new MobileLookupService();
				responseData.put("productCodes", lookupService.getProducts());
				responseData.put("accountTypes", lookupService.getAccountTypes());
				responseData.put("accountSubTypes", lookupService.getAccountSubTypes());
				responseData.put("governmentSchemes", lookupService.getGovernmentSchemes());
				responseData.put("operatingModes", lookupService.getOperatingModes());
				// responseData.put("cifNumbers",
				// lookupService.getCifNumbers());
				response.setResponseData(responseData.toString());
			} else if (queryType.equals(MiddlewareConstants.QUERY_TYPE_LOOKUP)) {
				DTObject fields = new DTObject();
				fields.set("FIELD_NAME", request.getLookupField());
				fields.set("FIELD_VALUES", request.getRequestData());
				MobileLookupService lookupService = new MobileLookupService();
				response.setResponseData(lookupService.getLookupDetails(fields));
			}
		} catch (Exception e) {
			e.printStackTrace();
			getLogger().logError(e.getLocalizedMessage());
			getErrorMap().setError(InterfaceConstants.ERROR, MiddlewareErrorCodes.UNSPECIFIED_ERROR);
		}
		response.setErrorMap(getErrorMap().getMap());
		return response;
	}

	@Override
	public GenericResponse getSubmitResponse(GenericRequest request) throws Exception {
		GenericResponse response = new GenericResponse();
		JSONObject requestData = new JSONObject(request.getRequestData());
		DTObject formDTO = new DTObject();
		if (validate(requestData)) {
			int numberOfAccountHolders = requestData.getInt("numberOfAccountHolders");
			formDTO.set("ENTITY_CODE", "1");
			formDTO.set("BRANCH_NO", "1");
			formDTO.set("PRODUCT_CODE", requestData.getString("productCode"));
			formDTO.set("ACCOUNT_TYPE_CODE", requestData.getString("accountType"));
			formDTO.set("ACCOUNT_SUBTYPE_CODE", requestData.getString("accountSubType"));
			formDTO.set("OPERATING_MODE", requestData.getString("operatingMode"));
			formDTO.set("NOOF_ACCOUNT_HOLDERS", String.valueOf(numberOfAccountHolders));
			formDTO.set("REMARKS", requestData.getString("remarks"));
			formDTO.set("BO_NAME", "patterns.config.framework.bo.casa.eaccntopenforsbBO");
			formDTO.set("PROCESS_ID", "EACCNTOPENFORSB");
			formDTO.set("CREATED_BY", "MOBILE_USER");

			String qualifierList = (String) requestData.get("qualifiers");
			JSONArray qualifiers = new JSONArray(qualifierList);
			// formDTO.set("MINOR_ACCOUNT_FLAG", qualifiers.get(0));
			// formDTO.set("LUNATIC_ACCOUNT_FLAG",
			// decodeBooleanToString(lunaticAc));
			// if (guardianCifNumber != null &&
			// !guardianCifNumber.equals(RegularConstants.EMPTY_STRING))
			// formDTO.set("GUARDIAN_CIF_NO", guardianCifNumber);
			// formDTO.set("ILLITERATE_AC_FLAG", illiterateAcThrAgent);
			// formDTO.set("PENSIONER_FLAG", decodeBooleanToString(pensioner));
			// formDTO.set("PPONO_REFNO", ppoNumber);
			// formDTO.set("BPL_AC_FLAG", decodeBooleanToString(bplAc));
			// formDTO.set("BPL_CARD_REFNO", bplCardNo);
			// formDTO.set("PHYSICALLY_HANDICAP_FLAG",
			// decodeBooleanToString(physHandicapd));
			// formDTO.set("BLIND_FLAG", decodeBooleanToString(blind));

			DTDObject dtdObject = new DTDObject();
			dtdObject.addColumn(0, "SL");
			dtdObject.addColumn(1, "CIF");
			dtdObject.addColumn(2, "NAME");
			dtdObject.addColumn(3, "DOB");
			dtdObject.addColumn(4, "NAME");
			dtdObject.addColumn(5, "RELATION");
			dtdObject.addColumn(6, "TITLE");
			JSONArray accountHolders;
			if (numberOfAccountHolders > 1) {
				accountHolders = new JSONArray(requestData.getString("accountHolders"));
			} else {
				accountHolders = new JSONArray().put(new JSONObject(requestData.getString("accountHolder")));
			}
			JSONObject accountHolder;
			for (int i = 0; i < accountHolders.length(); i++) {
				accountHolder = (JSONObject) accountHolders.get(i);
				dtdObject.addRow();
				dtdObject.setValue(i, 0, "1");
				dtdObject.setValue(i, 1, accountHolder.getString("cif"));
				dtdObject.setValue(i, 2, accountHolder.getString("name"));
				dtdObject.setValue(i, 3, accountHolder.getString("yearDob"));
				dtdObject.setValue(i, 4, accountHolder.getString("name"));
				dtdObject.setValue(i, 5, accountHolder.getString("relation"));
				dtdObject.setValue(i, 6, accountHolder.getString("title"));
				if (i == 0) {
					formDTO.set("TITLE_CODE", accountHolder.getString("title"));
					formDTO.set("NAME", accountHolder.getString("name"));
				}
			}
			formDTO.setDTDObject("ACCOUNTHOLDERS", dtdObject);
			dtdObject = new DTDObject();
			dtdObject.addColumn(0, "EDIT_LINK");
			dtdObject.addColumn(1, "GOVT_SCHEME");
			dtdObject.addColumn(2, "GOVT_SCHEME_NAME");
			dtdObject.addColumn(3, "ENRL_ID_NAME");
			dtdObject.addColumn(4, "ENRL_NAME");
			dtdObject.addColumn(5, "REF_NO");
			dtdObject.addColumn(6, "REMOVE_LINK");
			JSONArray governmentSchemes = new JSONArray(requestData.getString("governmentSchemes"));
			JSONObject governmentScheme;
			for (int i = 0; i < governmentSchemes.length(); i++) {
				governmentScheme = (JSONObject) governmentSchemes.get(i);
				dtdObject.addRow();
				dtdObject.setValue(i, 0, "1");
				dtdObject.setValue(i, 1, governmentScheme.getString("schemeCode"));
				dtdObject.setValue(i, 2, governmentScheme.getString("schemeName"));
				dtdObject.setValue(i, 3, governmentScheme.getString("cardName"));
				dtdObject.setValue(i, 4, governmentScheme.getString("cardName"));
				dtdObject.setValue(i, 5, governmentScheme.getString("referenceNumber"));
				dtdObject.setValue(i, 6, "1");
			}
			formDTO.setDTDObject("ACCOUNTSCHEMES", dtdObject);

			BackOfficeProcessManager processManager = new BackOfficeProcessManager();
			TBAProcessInfo processInfo = new TBAProcessInfo();
			processInfo.setProcessData(formDTO);
			processInfo.setProcessBO(formDTO.get("BO_NAME"));
			TBAProcessAction processAction = new TBAProcessAction();
			// ApplicationContext context =
			// ApplicationContext.getInstance();
			processAction.setProcessID(formDTO.get("PROCESS_ID"));
			processAction.setPartitionNo(context.getPartitionNo());
			processAction.setActionEntity(context.getEntityCode());
			processAction.setActionBranch(context.getBranchCode());
			processAction.setActionType(TBAActionType.ADD);
			processAction.setActionUser(context.getUserID());
			processAction.setActionDate(null);
			processAction.setActionCustomer("1");
			processAction.setRectification(false);
			processInfo.setProcessAction(processAction);
			boolean success = false;
			try {
				TBAProcessResult executionResult = processManager.delegate(processInfo);
				switch (executionResult.getProcessStatus()) {
				case FAILURE:
					break;
				case SUCCESS:
					success = true;
					break;
				default:
					break;
				}
				MessageParam additionalInfo = (MessageParam) executionResult.getAdditionalInfo();
				List<String> responseDataList = additionalInfo.getParameters();
				if (responseDataList.size() == 0) {
					getErrorMap().setError(InterfaceConstants.ERROR, MiddlewareErrorCodes.UNSPECIFIED_ERROR);
					return response;
				}
				JSONArray responseData = new JSONArray();
				for (int i = 0; i < responseDataList.size(); i++) {
					responseData.put(responseDataList.get(i));
				}
				if (success) {
					response.setResponseData(responseData.toString());
				}
			} catch (Exception e) {
				e.printStackTrace();
				getErrorMap().setError(InterfaceConstants.ERROR, MiddlewareErrorCodes.UNSPECIFIED_ERROR);
			}
		}
		return response;
	}

	// @Override
	// public DTObject processSubmitRequest(GenericRequest request) throws
	// Exception {
	// JSONObject requestData = new JSONObject(request.getRequestData());
	// DTObject formDTO = new DTObject();
	// if (validate(requestData)) {
	// int numberOfAccountHolders =
	// requestData.getInt("numberOfAccountHolders");
	// formDTO.set("ENTITY_CODE", "1");
	// formDTO.set("BRANCH_NO", "1");
	// formDTO.set("PRODUCT_CODE", requestData.getString("productCode"));
	// formDTO.set("ACCOUNT_TYPE_CODE", requestData.getString("accountType"));
	// formDTO.set("ACCOUNT_SUBTYPE_CODE",
	// requestData.getString("accountSubType"));
	// formDTO.set("OPERATING_MODE", requestData.getString("operatingMode"));
	// formDTO.set("NOOF_ACCOUNT_HOLDERS",
	// String.valueOf(numberOfAccountHolders));
	// formDTO.set("REMARKS", requestData.getString("remarks"));
	// formDTO.set("BO_NAME",
	// "patterns.config.framework.bo.casa.eaccntopenforsbBO");
	// formDTO.set("PROCESS_ID", "EACCNTOPENFORSB");
	// formDTO.set("CREATED_BY", "MOBILE_USER");
	//
	// String qualifierList = (String) requestData.get("qualifiers");
	// JSONArray qualifiers = new JSONArray(qualifierList);
	// // formDTO.set("MINOR_ACCOUNT_FLAG", qualifiers.get(0));
	// // formDTO.set("LUNATIC_ACCOUNT_FLAG",
	// // decodeBooleanToString(lunaticAc));
	// // if (guardianCifNumber != null &&
	// // !guardianCifNumber.equals(RegularConstants.EMPTY_STRING))
	// // formDTO.set("GUARDIAN_CIF_NO", guardianCifNumber);
	// // formDTO.set("ILLITERATE_AC_FLAG", illiterateAcThrAgent);
	// // formDTO.set("PENSIONER_FLAG", decodeBooleanToString(pensioner));
	// // formDTO.set("PPONO_REFNO", ppoNumber);
	// // formDTO.set("BPL_AC_FLAG", decodeBooleanToString(bplAc));
	// // formDTO.set("BPL_CARD_REFNO", bplCardNo);
	// // formDTO.set("PHYSICALLY_HANDICAP_FLAG",
	// // decodeBooleanToString(physHandicapd));
	// // formDTO.set("BLIND_FLAG", decodeBooleanToString(blind));
	//
	// DTDObject dtdObject = new DTDObject();
	// dtdObject.addColumn(0, "SL");
	// dtdObject.addColumn(1, "CIF");
	// dtdObject.addColumn(2, "NAME");
	// dtdObject.addColumn(3, "DOB");
	// dtdObject.addColumn(4, "NAME");
	// dtdObject.addColumn(5, "RELATION");
	// dtdObject.addColumn(6, "TITLE");
	// JSONArray accountHolders;
	// if (numberOfAccountHolders > 1) {
	// accountHolders = new JSONArray(requestData.getString("accountHolders"));
	// } else {
	// accountHolders = new JSONArray().put(new
	// JSONObject(requestData.getString("accountHolder")));
	// }
	// JSONObject accountHolder;
	// for (int i = 0; i < accountHolders.length(); i++) {
	// accountHolder = (JSONObject) accountHolders.get(i);
	// dtdObject.addRow();
	// dtdObject.setValue(i, 0, "1");
	// dtdObject.setValue(i, 1, accountHolder.getString("cif"));
	// dtdObject.setValue(i, 2, accountHolder.getString("name"));
	// dtdObject.setValue(i, 3, accountHolder.getString("yearDob"));
	// dtdObject.setValue(i, 4, accountHolder.getString("name"));
	// dtdObject.setValue(i, 5, accountHolder.getString("relation"));
	// dtdObject.setValue(i, 6, accountHolder.getString("title"));
	// if (i == 0) {
	// formDTO.set("TITLE_CODE", accountHolder.getString("title"));
	// formDTO.set("NAME", accountHolder.getString("name"));
	// }
	// }
	// formDTO.setDTDObject("ACCOUNTHOLDERS", dtdObject);
	// dtdObject = new DTDObject();
	// dtdObject.addColumn(0, "EDIT_LINK");
	// dtdObject.addColumn(1, "GOVT_SCHEME");
	// dtdObject.addColumn(2, "GOVT_SCHEME_NAME");
	// dtdObject.addColumn(3, "ENRL_ID_NAME");
	// dtdObject.addColumn(4, "ENRL_NAME");
	// dtdObject.addColumn(5, "REF_NO");
	// dtdObject.addColumn(6, "REMOVE_LINK");
	// JSONArray governmentSchemes = new
	// JSONArray(requestData.getString("governmentSchemes"));
	// JSONObject governmentScheme;
	// for (int i = 0; i < governmentSchemes.length(); i++) {
	// governmentScheme = (JSONObject) governmentSchemes.get(i);
	// dtdObject.addRow();
	// dtdObject.setValue(i, 0, "1");
	// dtdObject.setValue(i, 1, governmentScheme.getString("schemeCode"));
	// dtdObject.setValue(i, 2, governmentScheme.getString("schemeName"));
	// dtdObject.setValue(i, 3, governmentScheme.getString("cardName"));
	// dtdObject.setValue(i, 4, governmentScheme.getString("cardName"));
	// dtdObject.setValue(i, 5, governmentScheme.getString("referenceNumber"));
	// dtdObject.setValue(i, 6, "1");
	// }
	// formDTO.setDTDObject("ACCOUNTSCHEMES", dtdObject);
	// }
	// return formDTO;
	// }

	private boolean validate(JSONObject requestData) {
		return true;
	};
}
