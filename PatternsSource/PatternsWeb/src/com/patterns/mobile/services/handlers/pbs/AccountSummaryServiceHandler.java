/*
 * Copyright (c) 2014, 2015, Patterns Software Design Institute. All
 * rights reserved. Unauthorized copying of this file, via any medium is
 * strictly prohibited. PATTERNS SDI PROPRIETARY/CONFIDENTIAL. Use is
 * subject to license terms.
 */
package com.patterns.mobile.services.handlers.pbs;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Types;

import org.json.JSONObject;

import patterns.config.framework.database.MiddlewareConstants;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.interfaceref.InterfaceConstants;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.framework.web.reports.ReportRM;
import patterns.config.validations.MiddlewareValidator;
import patterns.json.JSONArray;

import com.patterns.mobile.services.handlers.GenericInterfaceHandler;
import com.patterns.mobile.services.handlers.MiddlewareErrorCodes;
import com.patterns.mobile.services.lookup.MobileLookupService;
import com.patterns.mobile.services.pojo.GenericRequest;
import com.patterns.mobile.services.pojo.GenericResponse;

/**
 * The Class AccountSummaryServiceHandler
 */
public class AccountSummaryServiceHandler extends GenericInterfaceHandler<GenericRequest, GenericResponse> {

	public AccountSummaryServiceHandler() {
		setProcessBO("");
	}

	@Override
	public GenericResponse processQueryRequest(GenericRequest request) throws Exception {
		GenericResponse response = new GenericResponse();

		try {
			String queryType = request.getQueryType();
			JSONObject responseData = new JSONObject();
			if (queryType.equals(MiddlewareConstants.QUERY_TYPE_INIT)) {
				MobileLookupService lookupService = new MobileLookupService();
				// responseData.put("accountNumbers",
				// lookupService.getAccountNumbers());
				response.setResponseData(responseData.toString());
			} else if (queryType.equals(MiddlewareConstants.QUERY_TYPE_GRIDQUERY)) {
				JSONObject requestData = new JSONObject(request.getRequestData());
				String accountNumber = (String) requestData.get("accountNumber");
				String fromDate = (String) requestData.get("fromDate");
				String toDate = (String) requestData.get("toDate");
				DTObject inputDTO = new DTObject();
				inputDTO.set("ENTITY_CODE", context.getEntityCode());
				inputDTO.set("ACCOUNT_NO", accountNumber);
				inputDTO.set("FROM_DATE", fromDate);
				inputDTO.set("UPTO_DATE", toDate);
				inputDTO.set("CCY_CODE", "INR");
				inputDTO.set("SUB_CCY_UNITS_IN_CCY", "2");
				inputDTO.set("BRANCH_CODE", context.getBranchCode());
				inputDTO.set("CLUSTER_CODE", context.getClusterCode());
				// inputDTO.setObject("CBD_DATE",
				// context.getCurrentBusinessDate());
				inputDTO.set("DATE_FORMAT", "%d-%m-%Y");

				DTObject result = loadAccountSummary(inputDTO);
				if (result == null || result.containsKey(InterfaceConstants.ERROR)) {
					getErrorMap().setError(InterfaceConstants.ERROR, MiddlewareErrorCodes.NO_RESULT);
				}
				responseData.put("summaryDetails", result.get("SUMMARY_DETAILS"));
				responseData.put("closingTotal", result.get("CLOSE_TOTAL"));
				responseData.put("openingBalance", result.get("OPEN_BAL"));
				responseData.put("closingBalance", result.get("CLOSE_BAL"));
				response.setResponseData(responseData.toString());
			} else if (queryType.equals(MiddlewareConstants.QUERY_TYPE_LOOKUP)) {
				DTObject fields = new DTObject();
				fields.set("FIELD_NAME", request.getLookupField());
				fields.set("FIELD_VALUES", request.getRequestData());
				MobileLookupService lookupService = new MobileLookupService();
				response.setResponseData(lookupService.getLookupDetails(fields));
			} else if (queryType.equals(MiddlewareConstants.QUERY_TYPE_FETCH)) {
				JSONObject requestData = new JSONObject(request.getRequestData());
				String accountNumber = requestData.getString("accountNumber");
				MiddlewareValidator validator = new MiddlewareValidator();
				String fetchType = requestData.getString("fetchType");
				if (fetchType.equals("accountDetails")) {
					responseData.put("accountDetail", validator.getAccountDetails(accountNumber));
				} else if (fetchType.equals("accountBalance")) {
					responseData.put("accountBalance", validator.getAccountBalance(accountNumber));
				} else if(fetchType.equals("accountWallets")){
					responseData.put("accountWallets", validator.getAccountWallets(accountNumber));
				}
				response.setResponseData(responseData.toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
			getLogger().logError(e.getLocalizedMessage());
			getErrorMap().setError(InterfaceConstants.ERROR, MiddlewareErrorCodes.UNSPECIFIED_ERROR);
		}
		response.setErrorMap(getErrorMap().getMap());
		return response;
	}

	private DTObject loadAccountSummary(DTObject inputDTO) {
		DTObject result = new DTObject();
		DBContext dbContext = new DBContext();
		try {
			StringBuffer sqlQuery = new StringBuffer(RegularConstants.EMPTY_STRING);
			BigDecimal op_balance = new BigDecimal("0");
			BigDecimal cl_balance = new BigDecimal("0");
			String ccy = "", units = "";
			DBUtil dbUtil = dbContext.createUtilInstance();
			ccy = inputDTO.get("CCY_CODE");
			units = inputDTO.get("SUB_CCY_UNITS_IN_CCY");

			dbUtil.reset();
			dbUtil.setMode(DBUtil.CALLABLE);
			dbUtil.setSql("CALL SP_GET_ACNT_SUMSTMT(?,?,?,?,?,?,?,?,?,?,?,?)");
			dbUtil.setLong(1, Long.parseLong(context.getEntityCode()));
			dbUtil.setLong(2, Long.parseLong(inputDTO.get("ACCOUNT_NO")));
			dbUtil.setString(3, inputDTO.get("CCY_CODE"));
			dbUtil.setString(4, inputDTO.get("BRANCH_CODE"));
			dbUtil.setString(5, inputDTO.get("CLUSTER_CODE"));
			dbUtil.setDate(6, new java.sql.Date(BackOfficeFormatUtils.getDate(inputDTO.get("FROM_DATE"), inputDTO.get("DATE_FORMAT")).getTime()));
			dbUtil.setDate(7, new java.sql.Date(BackOfficeFormatUtils.getDate(inputDTO.get("UPTO_DATE"), inputDTO.get("DATE_FORMAT")).getTime()));
			dbUtil.setDate(8, context.getCurrentBusinessDate());
			dbUtil.registerOutParameter(9, Types.VARCHAR);
			dbUtil.registerOutParameter(10, Types.BIGINT);
			dbUtil.registerOutParameter(11, Types.DECIMAL);
			dbUtil.registerOutParameter(12, Types.DECIMAL);
			dbUtil.execute();
			String status = dbUtil.getString(9);
			if (!status.equals(RegularConstants.SP_SUCCESS)) {
				inputDTO.set("SP_ERROR", status);
				return inputDTO;
			}
			Long serial = dbUtil.getLong(10);
			op_balance = dbUtil.getBigDecimal(11);
			cl_balance = dbUtil.getBigDecimal(12);

			dbUtil.reset();
			sqlQuery.append("SELECT TO_CHAR(FROM_DATE,?)FROM_DATE,TO_CHAR(UPTO_DATE,?)UPTO_DATE,");
			sqlQuery.append(" COALESCE(AC_CR_SUM,0) AC_CR_SUM,COALESCE(AC_DB_SUM,0) AC_DB_SUM,AC_CR_SUM_F,AC_DB_SUM_F");
			sqlQuery.append(" FROM RTMP_ACNTSUMSTMT");
			sqlQuery.append(" WHERE ENTITY_CODE = ? AND INV_SL = ?");
			dbUtil.setSql(sqlQuery.toString());
			dbUtil.setString(1, inputDTO.get("DATE_FORMAT"));
			dbUtil.setString(2, inputDTO.get("DATE_FORMAT"));
			dbUtil.setLong(3, Long.parseLong(context.getEntityCode()));
			dbUtil.setLong(4, serial);
			ResultSet rset = dbUtil.executeQuery();
			int rowNo = 1;
			rset.last();
			int rowCount = rset.getRow();
			rset.beforeFirst();
			BigDecimal cr_page_total = new BigDecimal("0");
			BigDecimal db_page_total = new BigDecimal("0");
			BigDecimal tmp_op_bal = op_balance;

			JSONArray openingBalance = new JSONArray();
			JSONArray openingBalanceSub;
			JSONArray transactions;
			JSONArray transactionsSub = new JSONArray();
			JSONArray closingTotal = new JSONArray();
			JSONArray closingTotalSub;
			JSONArray allTran = new JSONArray();
			while (rset.next()) {
				/* Opening Balance Begin */
				if (rowNo % 10 == 1) {
					openingBalanceSub = new JSONArray();
					if (tmp_op_bal.compareTo(BigDecimal.ZERO) >= 0) {
						openingBalanceSub.put(ReportRM.fmc(tmp_op_bal, ccy, units));
						openingBalanceSub.put("");
					} else {
						openingBalanceSub.put("");
						openingBalanceSub.put(ReportRM.fmc(tmp_op_bal, ccy, units));
					}
					openingBalance.put(openingBalanceSub);
				}
				/* Opening Balance End */

				transactions = new JSONArray();
				transactions.put(rset.getString("FROM_DATE"));
				transactions.put(rset.getString("UPTO_DATE"));
				transactions.put(rset.getString("AC_CR_SUM_F"));
				transactions.put(rset.getString("AC_DB_SUM_F"));
				transactionsSub.put(transactions);
				if (rowNo % 8 == 0 || rowCount == rowNo) {
					allTran.put(transactionsSub);
					transactionsSub = new JSONArray();
				}
				cr_page_total = cr_page_total.add(rset.getBigDecimal("AC_CR_SUM"));
				db_page_total = db_page_total.add(rset.getBigDecimal("AC_DB_SUM"));

				/* Closing Total begin */
				if (rowNo % 8 == 0 || rowCount == rowNo) {
					if (tmp_op_bal.compareTo(BigDecimal.ZERO) >= 0)
						cr_page_total = cr_page_total.add(tmp_op_bal);
					else
						db_page_total = db_page_total.add(tmp_op_bal);
					closingTotalSub = new JSONArray();
					closingTotalSub.put(ReportRM.fmc(cr_page_total, ccy, units));
					closingTotalSub.put(ReportRM.fmc(db_page_total, ccy, units));
					closingTotal.put(closingTotalSub);
					tmp_op_bal = cr_page_total.subtract(db_page_total);
					db_page_total = BigDecimal.ZERO;
					cr_page_total = BigDecimal.ZERO;
				}
				/* Closing Total end */
				rowNo++;
			}

			/* Closing Balance begin */
			JSONArray closingBalance = new JSONArray();
			if (cl_balance.compareTo(BigDecimal.ZERO) >= 0) {
				closingBalance.put(ReportRM.fmc(cl_balance, ccy, units));
				closingBalance.put("");
			} else {
				closingBalance.put("");
				closingBalance.put(ReportRM.fmc(cl_balance, ccy, units));
			}
			dbUtil.reset();
			/* Closing Balance end */

			result.set("OPEN_BAL", openingBalance.toString());
			result.set("SUMMARY_DETAILS", allTran.toString());
			result.set("CLOSE_TOTAL", closingTotal.toString());
			result.set("CLOSE_BAL", closingBalance.toString());
		} catch (Exception e) {
			e.printStackTrace();
			getLogger().logError(e.getLocalizedMessage());
			getErrorMap().setError(InterfaceConstants.ERROR, MiddlewareErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			dbContext.close();
		}
		return result;
	}
}