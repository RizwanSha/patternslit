/*
 * Copyright (c) 2014, 2015, Patterns Software Design Institute. All
 * rights reserved. Unauthorized copying of this file, via any medium is
 * strictly prohibited. PATTERNS SDI PROPRIETARY/CONFIDENTIAL. Use is
 * subject to license terms.
 */
package com.patterns.mobile.services.handlers.pbs;

import org.json.JSONObject;

import patterns.config.framework.interfaceref.InterfaceConstants;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.MiddlewareValidator;

import com.patterns.mobile.services.handlers.GenericInterfaceHandler;
import com.patterns.mobile.services.handlers.MiddlewareErrorCodes;
import com.patterns.mobile.services.pojo.GenericRequest;
import com.patterns.mobile.services.pojo.GenericResponse;

/**
 * The Class DocumentDispatchServiceHandler
 */
public class DocumentDispatchServiceHandler extends GenericInterfaceHandler<GenericRequest, GenericResponse> {

	public DocumentDispatchServiceHandler() {
		setProcessBO("");
	}

	@SuppressWarnings("unused")
	private GenericRequest request;

	@Override
	public GenericResponse processQueryRequest(GenericRequest request) throws Exception {
		this.request = request;
		GenericResponse response = new GenericResponse();
		MiddlewareValidator validator = new MiddlewareValidator();
		try {
			response.setRequestId(request.getRequestId());
			String appId = getApplicationKey();
			if (isEmpty(request.getApplicationKey()) || (!request.getApplicationKey().equals(appId))) {
				getErrorMap().setError(InterfaceConstants.ERROR, MiddlewareErrorCodes.INVALID_APP_KEY);
				response.setErrorMap(getErrorMap().getMap());
				return response;
			}
			JSONObject responseData = new JSONObject();
//			JSONObject requestData = new JSONObject(request.getRequestData());
//			String accountNo = (String) requestData.get("ACCOUNT_NO");
//			String customerNo = (String) requestData.get("CIF_NO");
//			String documentType = (String) requestData.get("DOC_TYPE");
//			DTObject result = validator.fetchCustomerDocument(documentType, customerNo, accountNo);
			DTObject result = validator.fetchCustomerDocument("1", "1", "100000100001");
			if (result.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				getErrorMap().setError(InterfaceConstants.ERROR, MiddlewareErrorCodes.UNSPECIFIED_ERROR);
			} else {
				responseData.put("DATA", result.get("DATA"));
				response.setResponseData(responseData.toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
			getLogger().logError(e.getLocalizedMessage());
			getErrorMap().setError(InterfaceConstants.ERROR, MiddlewareErrorCodes.UNSPECIFIED_ERROR);
		}
		response.setErrorMap(getErrorMap().getMap());
		return response;
	}
}
