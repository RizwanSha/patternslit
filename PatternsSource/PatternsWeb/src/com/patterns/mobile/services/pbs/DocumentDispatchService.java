/*
 * Copyright (c) 2014, 2015, Patterns Software Design Institute. All
 * rights reserved. Unauthorized copying of this file, via any medium is
 * strictly prohibited. PATTERNS SDI PROPRIETARY/CONFIDENTIAL. Use is
 * subject to license terms.
 */
package com.patterns.mobile.services.pbs;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.jboss.resteasy.annotations.Form;

import patterns.config.framework.loggers.ApplicationLogger;

import com.patterns.mobile.services.handlers.pbs.DocumentDispatchServiceHandler;
import com.patterns.mobile.services.pojo.GenericRequest;
import com.patterns.mobile.services.pojo.GenericResponse;

/**
 * The Class DocumentDispatchService.
 */
@Path("/MPBS/MOB/DocumentRetrieval")
public class DocumentDispatchService {

	/** The logger. */
	private static ApplicationLogger logger = null;

	/**
	 * Instantiates a new DocumentDispatch service.
	 */
	public DocumentDispatchService() {
		logger = ApplicationLogger.getInstance(this.getClass().getSimpleName());
	}

	/**
	 * Process request.
	 * 
	 * @param formRequest
	 *            the form request
	 * @return the response
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public Response processRequest(@Form GenericRequest formRequest) {
		GenericResponse response = null;
		DocumentDispatchServiceHandler handler = new DocumentDispatchServiceHandler();
		try {
			response = handler.process(formRequest);
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError(e.getLocalizedMessage());
		}
		response.setErrorMap(handler.getErrorMap().getMap());
		return Response.status(200).entity(response).build();
	}
}