/*
 * Copyright (c) 2014, 2015, Patterns Software Design Institute. All
 * rights reserved. Unauthorized copying of this file, via any medium is
 * strictly prohibited. PATTERNS SDI PROPRIETARY/CONFIDENTIAL. Use is
 * subject to license terms.
 */
package com.patterns.mobile.services;

import java.io.Serializable;

/**
 * The Class ServiceBean provides default functioality for a Service
 * related POJO
 */
public class ServiceBean implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 2166761367064873581L;

	/** The entity code. */
	private String entityCode;

	/** The application id. */
	private String applicationId;

	/** The client id. */
	private String clientId;

	/** The servicecode. */
	private String servicecode;

	/** The description. */
	private String description;

	/** The captcha required. */
	private boolean captchaRequired;

	/** The locking required. */
	private boolean lockingRequired;

	/**
	 * Gets the entity code.
	 * 
	 * @return the entity code
	 */
	public String getEntityCode() {
		return entityCode;
	}

	/**
	 * Sets the entity code.
	 * 
	 * @param entityCode
	 *            the new entity code
	 */
	public void setEntityCode(String entityCode) {
		this.entityCode = entityCode;
	}

	/**
	 * Gets the application id.
	 * 
	 * @return the application id
	 */
	public String getApplicationId() {
		return applicationId;
	}

	/**
	 * Sets the application id.
	 * 
	 * @param applicationId
	 *            the new application id
	 */
	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	/**
	 * Gets the client id.
	 * 
	 * @return the client id
	 */
	public String getClientId() {
		return clientId;
	}

	/**
	 * Sets the client id.
	 * 
	 * @param clientId
	 *            the new client id
	 */
	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	/**
	 * Gets the servicecode.
	 * 
	 * @return the servicecode
	 */
	public String getServicecode() {
		return servicecode;
	}

	/**
	 * Sets the servicecode.
	 * 
	 * @param servicecode
	 *            the new servicecode
	 */
	public void setServicecode(String servicecode) {
		this.servicecode = servicecode;
	}

	/**
	 * Gets the description.
	 * 
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 * 
	 * @param description
	 *            the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Checks if is captcha required.
	 * 
	 * @return true, if is captcha required
	 */
	public boolean isCaptchaRequired() {
		return captchaRequired;
	}

	/**
	 * Sets the captcha required.
	 * 
	 * @param captchaRequired
	 *            the new captcha required
	 */
	public void setCaptchaRequired(boolean captchaRequired) {
		this.captchaRequired = captchaRequired;
	}

	/**
	 * Checks if is locking required.
	 * 
	 * @return true, if is locking required
	 */
	public boolean isLockingRequired() {
		return lockingRequired;
	}

	/**
	 * Sets the locking required.
	 * 
	 * @param lockingRequired
	 *            the new locking required
	 */
	public void setLockingRequired(boolean lockingRequired) {
		this.lockingRequired = lockingRequired;
	}
}
