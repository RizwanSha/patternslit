/*
 * Copyright (c) 2014, 2015, Patterns Software Design Institute. All
 * rights reserved. Unauthorized copying of this file, via any medium is
 * strictly prohibited. PATTERNS SDI PROPRIETARY/CONFIDENTIAL. Use is
 * subject to license terms.
 */
package com.patterns.mobile.services;

/**
 * The Class ServiceCodes provides constants for service codes
 */
public class ServiceCodes {

	public static final String REGISTRATION_INIT = "REGINIT";
	public static final String REGISTRATION_COMP = "REGCOMP";
	public static final String CIRGENERATION = "CIRGEN";
	public static final String CIRREPORT = "CIRREPORT";
	public static final String EMAILCHANGE = "EMAILCHANGE";
	public static final String LOGIN_INIT = "LOGININIT";
	public static final String LOGIN_COMP = "LOGINCOMP";
	public static final String LOGOUT = "LOGOUT";
	public static final String MOBILECHNG_INIT = "MOBCHNGINIT";
	public static final String MOBILECHNG_COMP = "MOBCHNGCOMP";
	public static final String OTP_RESEND = "OTPRESEND";

	public static final String BALOGIN = "BALOGIN";
	public static final String BALOGOUT = "BALOGOUT";
	public static final String BASUBSRCH = "SUBSRCH";
	public static final String BACIRREPORT = "BACIRREPORT";
	public static final String BACIRGENERATION = "BACIRGEN";
	public static final String BAARCSRCH = "BAARCSRCH";
}
