/*
 * Copyright (c) 2014, 2015, Patterns Software Design Institute. All
 * rights reserved. Unauthorized copying of this file, via any medium is
 * strictly prohibited. PATTERNS SDI PROPRIETARY/CONFIDENTIAL. Use is
 * subject to license terms.
 */
package com.patterns.mobile.services;

import java.sql.ResultSet;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.loggers.ApplicationLogger;
import patterns.config.framework.web.FormatUtils;


/**
 * The Class ServiceLoader maintains the service configuration in-memory
 */
public class ServiceLoader {

	private static final ReentrantReadWriteLock _READ_WRITE_LOCK_GLOBAL = new ReentrantReadWriteLock();

	private static final Lock _WRITE_GLOBAL = _READ_WRITE_LOCK_GLOBAL.writeLock();

	private final ApplicationLogger logger;

	private ServiceLoader() {
		logger = ApplicationLogger.getInstance(this.getClass().getSimpleName());
	}

	private Map<String, Map<String, Map<String, ServiceBean>>> internalMap;
	private Map<String, Map<String, Map<String, ServiceBean>>> internalLocalMap;

	private static class ServiceTemplatesMapHelper {
		private static final ServiceLoader _INSTANCE = new ServiceLoader();
	}

	public static ServiceLoader getInstance() {
		return ServiceTemplatesMapHelper._INSTANCE;
	}

	public final void unloadConfiguration() {
		try {
			_WRITE_GLOBAL.lock();
			resetConfiguration();
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError(e.getLocalizedMessage());
		} finally {
			_WRITE_GLOBAL.unlock();
		}
	}

	public final void reloadConfiguration() {
		try {
			_WRITE_GLOBAL.lock();
			loadConfiguration();
			resetConfiguration();
			initConfiguration();
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError(e.getLocalizedMessage());
		} finally {
			_WRITE_GLOBAL.unlock();
		}
	}

	private final void initConfiguration() {
		internalMap.putAll(internalLocalMap);
		internalLocalMap.clear();
	}

	private final void resetConfiguration() {
		logger.logDebug("resetConfiguration()");
		if (internalMap == null) {
			internalMap = new HashMap<String, Map<String, Map<String, ServiceBean>>>();
		}
		internalMap.clear();
	}

	public final ServiceBean getServiceConfiguration(String entityCode, String applicationId, String serviceCode) {
		if (internalMap == null)
			reloadConfiguration();
		return internalMap.get(entityCode).get(applicationId).get(serviceCode);
	}

	public final Collection<ServiceBean> getServiceConfigurations(String entityCode, String applicationId) {
		if (internalMap == null)
			reloadConfiguration();
		return internalMap.get(entityCode).get(applicationId).values();
	}

	private final void loadConfiguration() {
		logger.logDebug("loadConfiguration()");
		if (internalLocalMap == null) {
			internalLocalMap = new HashMap<String, Map<String, Map<String, ServiceBean>>>();
		}
		internalLocalMap.clear();

		DBContext dbContext = new DBContext();
		try {
			String entitySql = "SELECT ENTITY_CODE FROM INSTALL";
			DBUtil entityUtil = dbContext.createUtilInstance();
			entityUtil.setSql(entitySql);
			ResultSet entityRSet = entityUtil.executeQuery();
			while (entityRSet.next()) {
				String entityCode = entityRSet.getString(1);
				String applicationSql = "SELECT EXT_APPL_CODE FROM EXTAPPL WHERE ENABLED='1'";
				DBUtil applicationUtil = dbContext.createUtilInstance();
				applicationUtil.setSql(applicationSql);
				ResultSet applicationRSet = applicationUtil.executeQuery();
				Map<String, Map<String, ServiceBean>> applicationServiceMap = new HashMap<String, Map<String, ServiceBean>>();
				while (applicationRSet.next()) {
					String applicationId = applicationRSet.getString(1);
					Map<String, ServiceBean> serviceMap = new HashMap<String, ServiceBean>();
					String serviceSql = "SELECT SERVICE_ID,CLIENT_ID,DESCRIPTION,CAPTCHA_REQUIRED,LOCKING_REQUIRED FROM SRVSERVICES WHERE ENTITY_CODE=? AND APP_ID=? AND ENABLED='1'";
					DBUtil serviceUtil = dbContext.createUtilInstance();
					serviceUtil.setSql(serviceSql);
					serviceUtil.setString(1, entityCode);
					serviceUtil.setString(2, applicationId);
					ResultSet serviceRSet = serviceUtil.executeQuery();
					while (serviceRSet.next()) {
						ServiceBean service = new ServiceBean();
						service.setEntityCode(entityCode);
						service.setApplicationId(applicationId);
						service.setServicecode(serviceRSet.getString(1));
						service.setClientId(serviceRSet.getString(2));
						service.setDescription(serviceRSet.getString(3));
						service.setCaptchaRequired(FormatUtils.decodeStringToBoolean(serviceRSet.getString(4)));
						service.setLockingRequired(FormatUtils.decodeStringToBoolean(serviceRSet.getString(5)));
						serviceMap.put(serviceRSet.getString(1), service);
					}
					serviceUtil.reset();
					applicationServiceMap.put(applicationId, serviceMap);
				}
				applicationUtil.reset();
				internalLocalMap.put(entityCode, applicationServiceMap);
			}
			entityUtil.reset();
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError(e.getLocalizedMessage());
			logger.logError(" loadConfiguration() Exception");
		} finally {
			dbContext.close();
		}
	}
}