/*
 * Copyright (c) 2014, 2015, Patterns Software Design Institute. All rights reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * PATTERNS SDI PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
package com.patterns.mobile.services.pojo.pbs;

import com.patterns.mobile.services.pojo.GenericResponse;

/**
 * The Class OtpResendResponse.
 */
public class AccountStatementResponse extends GenericResponse {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -4085446986456430369L;

	private String openingBalance;
	private String statementDetails;
	private String closingTotal;
	private String closingBalance;

	public String getOpeningBalance() {
		return openingBalance;
	}

	public void setOpeningBalance(String openingBalance) {
		this.openingBalance = openingBalance;
	}

	public String getStatementDetails() {
		return statementDetails;
	}

	public void setStatementDetails(String statementDetails) {
		this.statementDetails = statementDetails;
	}

	public String getClosingTotal() {
		return closingTotal;
	}

	public void setClosingTotal(String closingTotal) {
		this.closingTotal = closingTotal;
	}

	public String getClosingBalance() {
		return closingBalance;
	}

	public void setClosingBalance(String closingBalance) {
		this.closingBalance = closingBalance;
	}
}
