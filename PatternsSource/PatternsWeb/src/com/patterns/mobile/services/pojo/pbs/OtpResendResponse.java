/*
 * Copyright (c) 2014, 2015, Patterns Software Design Institute. All rights reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * PATTERNS SDI PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
package com.patterns.mobile.services.pojo.pbs;

import com.patterns.mobile.services.pojo.GenericResponse;


/**
 * The Class OtpResendResponse.
 */
public class OtpResendResponse extends GenericResponse {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -4085446986456430369L;
	
	/** The resend disable. */
	private boolean resendDisable;

	/**
	 * Checks if is resend disable.
	 * 
	 * @return true, if is resend disable
	 */
	public boolean isResendDisable() {
		return resendDisable;
	}

	/**
	 * Sets the resend disable.
	 * 
	 * @param resendDisable
	 *            the new resend disable
	 */
	public void setResendDisable(boolean resendDisable) {
		this.resendDisable = resendDisable;
	}
}
