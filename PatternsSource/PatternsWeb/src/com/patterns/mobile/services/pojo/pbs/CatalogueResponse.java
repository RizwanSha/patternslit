/*
 * Copyright (c) 2014, 2015, Patterns Software Design Institute. All rights reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * PATTERNS SDI PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
package com.patterns.mobile.services.pojo.pbs;

import com.patterns.mobile.services.pojo.GenericResponse;

/**
 * The Class CatalogueResponse.
 */
public class CatalogueResponse extends GenericResponse {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1768430368469095064L;

	/** The catalogue. */
	private String catalogue;

	/**
	 * Gets the catalogue.
	 * 
	 * @return the catalogue
	 */
	public String getCatalogue() {
		return catalogue;
	}

	/**
	 * Sets the catalogue.
	 * 
	 * @param catalogue
	 *            the new catalogue
	 */
	public void setCatalogue(String catalogue) {
		this.catalogue = catalogue;
	}
}
