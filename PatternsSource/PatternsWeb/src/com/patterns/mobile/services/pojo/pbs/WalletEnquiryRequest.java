/*
 * Copyright (c) 2014, 2015, Patterns Software Design Institute. All rights reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * PATTERNS SDI PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
package com.patterns.mobile.services.pojo.pbs;

import javax.ws.rs.FormParam;

import com.patterns.mobile.services.pojo.GenericRequest;

/**
 * The Class AccountStatementRequest.
 */
public class WalletEnquiryRequest extends GenericRequest {

	private static final long serialVersionUID = 5382429346297635893L;
	@FormParam("sessionId")
	private String sessionId;
	@FormParam("accountNumber")
	private String accountNumber;
	@FormParam("cif")
	private String cif;

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
}
