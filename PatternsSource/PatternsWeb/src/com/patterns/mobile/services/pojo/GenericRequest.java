/*
 * Copyright (c) 2014, 2015, Patterns Software Design Institute. All rights reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * PATTERNS SDI PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
package com.patterns.mobile.services.pojo;

import java.io.Serializable;

import javax.ws.rs.FormParam;


/**
 * The Class GenericRequest.
 */
public class GenericRequest implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -1332761630109443516L;

	/** The request id. */
	@FormParam("requestId")
	private String requestId;

	/** The session id. */
	@FormParam("sessionId")
	private String sessionId;

	/** The application key. */
	@FormParam("applicationKey")
	public String applicationKey;
	
	/** The request type. */
	@FormParam("requestType")
	private String requestType;
	
	/** The query type. */
	@FormParam("queryType")
	private String queryType;
	
	@FormParam("requestData")
	private String requestData;	
	
	@FormParam("lookupField")
	private String lookupField;	
	
	public String getLookupField() {
		return lookupField;
	}

	public void setLookupField(String lookupField) {
		this.lookupField = lookupField;
	}

	public String getRequestData() {
		return requestData;
	}

	public void setRequestData(String requestData) {
		this.requestData = requestData;
	}

	public String getQueryType() {
		return queryType;
	}

	public void setQueryType(String queryType) {
		this.queryType = queryType;
	}

	public String getRequestType() {
		return requestType;
	}

	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}

	/**
	 * Gets the application key.
	 * 
	 * @return the application key
	 */
	public String getApplicationKey() {
		return applicationKey;
	}

	/**
	 * Sets the application key.
	 * 
	 * @param applicationKey
	 *            the new application key
	 */
	public void setApplicationKey(String applicationKey) {
		this.applicationKey = applicationKey;
	}
	
	/**
	 * Gets the request id.
	 * 
	 * @return the request id
	 */
	public String getRequestId() {
		return requestId;
	}

	/**
	 * Sets the request id.
	 * 
	 * @param requestId
	 *            the new request id
	 */
	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	/**
	 * Gets the session id.
	 * 
	 * @return the session id
	 */
	public String getSessionId() {
		return sessionId;
	}

	/**
	 * Sets the session id.
	 * 
	 * @param sessionId
	 *            the new session id
	 */
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

}
