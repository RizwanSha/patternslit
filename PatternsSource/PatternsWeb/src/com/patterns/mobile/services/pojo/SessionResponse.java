/*
 * Copyright (c) 2014, 2015, Patterns Software Design Institute. All rights reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * PATTERNS SDI PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
package com.patterns.mobile.services.pojo;

import java.util.LinkedList;
import java.util.List;


/**
 * The Class SessionResponse.
 */
public class SessionResponse extends GenericResponse {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1768430368469095064L;
	
	/** The session id. */
	private String sessionId;
	
	/** The captcha required. */
	private boolean captchaRequired;
	
	/** The captcha list. */
	private List<String> captchaList = new LinkedList<String>();
	
	/** The first login tc required. */
	private boolean firstLoginTCRequired;
	
	/** The register tc required. */
	private boolean registerTCRequired;

	/**
	 * Gets the session id.
	 * 
	 * @return the session id
	 */
	public String getSessionId() {
		return sessionId;
	}

	/**
	 * Sets the session id.
	 * 
	 * @param sessionId
	 *            the new session id
	 */
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	/**
	 * Checks if is captcha required.
	 * 
	 * @return true, if is captcha required
	 */
	public boolean isCaptchaRequired() {
		return captchaRequired;
	}

	/**
	 * Sets the captcha required.
	 * 
	 * @param captchaRequired
	 *            the new captcha required
	 */
	public void setCaptchaRequired(boolean captchaRequired) {
		this.captchaRequired = captchaRequired;
	}

	/**
	 * Gets the captcha list.
	 * 
	 * @return the captcha list
	 */
	public List<String> getCaptchaList() {
		return captchaList;
	}

	/**
	 * Sets the captcha list.
	 * 
	 * @param captchaList
	 *            the new captcha list
	 */
	public void setCaptchaList(List<String> captchaList) {
		this.captchaList = captchaList;
	}

	/**
	 * Checks if is first login tc required.
	 * 
	 * @return true, if is first login tc required
	 */
	public boolean isFirstLoginTCRequired() {
		return firstLoginTCRequired;
	}

	/**
	 * Sets the first login tc required.
	 * 
	 * @param firstLoginTCRequired
	 *            the new first login tc required
	 */
	public void setFirstLoginTCRequired(boolean firstLoginTCRequired) {
		this.firstLoginTCRequired = firstLoginTCRequired;
	}

	/**
	 * Checks if is register tc required.
	 * 
	 * @return true, if is register tc required
	 */
	public boolean isRegisterTCRequired() {
		return registerTCRequired;
	}

	/**
	 * Sets the register tc required.
	 * 
	 * @param registerTCRequired
	 *            the new register tc required
	 */
	public void setRegisterTCRequired(boolean registerTCRequired) {
		this.registerTCRequired = registerTCRequired;
	}

}
