/*
 * Copyright (c) 2014, 2015, Patterns Software Design Institute. All rights reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * PATTERNS SDI PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
package com.patterns.mobile.services.pojo.pbs;

import com.patterns.mobile.services.pojo.GenericResponse;

/**
 * The Class SchemeResponse.
 */
public class SchemeResponse extends GenericResponse {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1768430368469095064L;

	/** The scheme. */
	private String scheme;

	/**
	 * Gets the scheme.
	 * 
	 * @return the scheme
	 */
	public String getScheme() {
		return scheme;
	}

	/**
	 * Sets the scheme.
	 * 
	 * @param catalogue
	 *            the new scheme
	 */
	public void setScheme(String scheme) {
		this.scheme = scheme;
	}
}
