/*
 * Copyright (c) 2014, 2015, Patterns Software Design Institute. All rights reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * PATTERNS SDI PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
package com.patterns.mobile.services.pojo.pbs;

import javax.ws.rs.FormParam;

import com.patterns.mobile.services.pojo.GenericRequest;

/**
 * The Class AccountStatementRequest.
 */
public class LoginRequest extends GenericRequest {

	private static final long serialVersionUID = 5382429346297635893L;
	@FormParam("sessionId")
	private String sessionId;
	@FormParam("userId")
	private String userId;
	@FormParam("hashedPassword")
	private String hashedPassword;
	@FormParam("loginType")
	private String loginType;
	
	public String getLoginType() {
		return loginType;
	}

	public void setLoginType(String loginType) {
		this.loginType = loginType;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getHashedPassword() {
		return hashedPassword;
	}

	public void setHashedPassword(String hashedPassword) {
		this.hashedPassword = hashedPassword;
	}
}
