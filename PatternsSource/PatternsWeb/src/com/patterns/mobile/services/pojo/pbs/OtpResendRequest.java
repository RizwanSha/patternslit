/*
 * Copyright (c) 2014, 2015, Patterns Software Design Institute. All rights reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * PATTERNS SDI PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
package com.patterns.mobile.services.pojo.pbs;

import com.patterns.mobile.services.pojo.GenericRequest;


/**
 * The Class OtpResendRequest.
 */
public class OtpResendRequest extends GenericRequest {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1353617726932267211L;
}
