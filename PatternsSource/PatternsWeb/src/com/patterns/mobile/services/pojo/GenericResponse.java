/*
 * Copyright (c) 2014, 2015, Patterns Software Design Institute. All rights reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * PATTERNS SDI PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
package com.patterns.mobile.services.pojo;

import java.io.Serializable;
import java.util.Map;


/**
 * The Class GenericResponse.
 */
public class GenericResponse implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -1332761630109443516L;

	/** The request id. */
	private String requestId;

	/** The error map. */
	private Map<String, String> errorMap;

	private String responseData;	
	
	public String getResponseData() {
		return responseData;
	}

	public void setResponseData(String responseData) {
		this.responseData = responseData;
	}

	/**
	 * Gets the request id.
	 * 
	 * @return the request id
	 */
	public String getRequestId() {
		return requestId;
	}

	/**
	 * Sets the request id.
	 * 
	 * @param requestId
	 *            the new request id
	 */
	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	/**
	 * Gets the error map.
	 * 
	 * @return the error map
	 */
	public Map<String, String> getErrorMap() {
		return errorMap;
	}

	/**
	 * Sets the error map.
	 * 
	 * @param errorMap
	 *            the error map
	 */
	public void setErrorMap(Map<String, String> errorMap) {
		this.errorMap = errorMap;
	}

	/** The error present. */
	private boolean errorPresent;

	/**
	 * Checks if is error present.
	 * 
	 * @return true, if is error present
	 */
	public boolean isErrorPresent() {
		return errorPresent;
	}

	/**
	 * Sets the error present.
	 * 
	 * @param errorPresent
	 *            the new error present
	 */
	public void setErrorPresent(boolean errorPresent) {
		this.errorPresent = errorPresent;
	}

}
