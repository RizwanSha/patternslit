package patterns.config.web.reports.inv;


import java.io.ByteArrayInputStream;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

import net.sf.jasperreports.engine.JRResultSetDataSource;
import net.sf.jasperreports.engine.util.JRXmlUtils;

import org.w3c.dom.Document;

import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.web.reports.ReportConstants;
import patterns.config.framework.web.reports.fw.ReportAdapter;
import patterns.config.web.reports.resources.ReportResourceBundles;


public class rinvoiceauthreport extends ReportAdapter {
	private String gridXml = RegularConstants.EMPTY_STRING;

	@Override
	public Map<String, Object> getReportParameters() {
		HashMap<String, Object> params = new HashMap<String, Object>();
		return params;
	}

	public Document processXMLData() {
		Document document = null;
		try {
			document = JRXmlUtils.parse(new ByteArrayInputStream(gridXml.getBytes()));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return document;
	}

	@Override
	public int getReportProcessorType() {
		return ReportConstants.REPORT_PROCESSOR_RESULT_SET;
	}
	
	public JRResultSetDataSource processResultSet() {
		return new JRResultSetDataSource((ResultSet)context.getFilters().getObject("RESULT_SET"));
	}

	@Override
	public String getReportPath() {
		String reportPath = RegularConstants.NULL;
		reportPath = "/inv/reports/rinvoiceauth.jasper";
		return reportPath;
	}

	@Override
	public void initResource() {

	}

	@Override
	public void releaseResource() {

	}

	@Override
	public String getResourceBundle() {
		return ReportResourceBundles.INV_REPORTS;
	}

}
