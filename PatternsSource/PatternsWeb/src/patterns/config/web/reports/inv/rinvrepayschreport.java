package patterns.config.web.reports.inv;

import java.util.HashMap;
import java.util.Map;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.reports.ReportConstants;
import patterns.config.framework.web.reports.fw.ReportAdapter;
import patterns.config.web.reports.resources.ReportResourceBundles;

public class rinvrepayschreport extends ReportAdapter {
	public Map<String, Object> getReportParameters() {
		HashMap<String, Object> params = new HashMap<String, Object>();
		DTObject inputDTO = context.getFilters();
		params.put("INPUTDTO", inputDTO);
		params.put("ENTITY_CODE", Integer.parseInt(inputDTO.get("ENTITY_CODE")));
		params.put("LESSEE_CODE", inputDTO.get("LESSEE_CODE"));
		params.put("AGREEMENT_NO", inputDTO.get("AGREEMENT_NO"));
		params.put("SCHEDULE_ID", inputDTO.get("SCHEDULE_ID"));
		params.put("SUBREPORT_PATH", "/inv/reports/");
		return params;
	}

	@Override
	public int getReportProcessorType() {
		return ReportConstants.REPORT_PROCESSOR_CONNECTION;
	}

	@Override
	public String getReportPath() {
		String reportPath = RegularConstants.NULL;
		reportPath = "/inv/reports/rinvrepaysch_main.jasper";
		return reportPath;
	}

	

	@Override
	public void initResource() {

	}

	@Override
	public void releaseResource() {

	}

	@Override
	public String getResourceBundle() {
		return ReportResourceBundles.INV_REPORTS;
	}



}