package patterns.config.web.reports.inv;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.framework.web.reports.ReportConstants;
import patterns.config.framework.web.reports.fw.ReportAdapter;
import patterns.config.web.reports.resources.ReportResourceBundles;
import ar.com.fdvs.dj.domain.DJCalculation;
import ar.com.fdvs.dj.domain.DJValueFormatter;
import ar.com.fdvs.dj.domain.Style;
import ar.com.fdvs.dj.domain.builders.ColumnBuilder;
import ar.com.fdvs.dj.domain.builders.FastReportBuilder;
import ar.com.fdvs.dj.domain.constants.Border;
import ar.com.fdvs.dj.domain.constants.HorizontalAlign;
import ar.com.fdvs.dj.domain.constants.Page;
import ar.com.fdvs.dj.domain.constants.VerticalAlign;
import ar.com.fdvs.dj.domain.entities.columns.AbstractColumn;

public class rinvoicereport extends ReportAdapter {
	boolean isDynamic = false;
	String fmtType = RegularConstants.EMPTY_STRING;
	private int grid_columnCount = 0;
	private ArrayList<String> grid_columnFields = new ArrayList<String>();
	private ArrayList<String> grid_columnDesc = new ArrayList<String>();
	private ArrayList<String> grid_columnTypes = new ArrayList<String>();
	private String annex_Query;
	private String grid_Query;
	private int annex_columnCount = 0;
	private ArrayList<String> annex_columnFields = new ArrayList<String>();
	private ArrayList<String> annex_columnDesc = new ArrayList<String>();
	private ArrayList<String> annex_columnTypes = new ArrayList<String>();
	private String annex_mode;
	private String report_Type;

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> getReportParameters() {
		HashMap<String, Object> params = new HashMap<String, Object>();
		DTObject inputDTO = context.getFilters();
		params.put("INPUTDTO", inputDTO);
		params.put("ENTITY_CODE", Integer.parseInt(inputDTO.get("ENTITY_CODE")));
		params.put("LEASE_PROD_CODE", inputDTO.get("LEASE_PROD_CODE"));
		params.put("INVOICE_YEAR", Integer.parseInt(inputDTO.get("INV_YEAR")));
		params.put("INVOICE_MONTH", Integer.parseInt(inputDTO.get("INV_MONTH")));
		params.put("INVOICE_SERIAL", Integer.parseInt(inputDTO.get("INV_SERIAL")));
		params.put("P_TEMP_SL", Integer.parseInt(inputDTO.get("TEMP_SL")));
		params.put("P_CONSOLIDATED", inputDTO.get("CONSOLIDATED"));
		params.put("TOTAL_INV_AMT", inputDTO.get("TOTAL_INV_AMT"));
		params.put("INV_CCY", inputDTO.get("INV_CCY"));
		params.put("INV_CCY_UNITS", inputDTO.get("INV_CCY_UNITS"));
		params.put("CCY_DESC", inputDTO.get("CONCISE_DESCRIPTION"));
		params.put("SUB_CCY_DESC", inputDTO.get("SUB_CCY_CONCISE_DESCRIPTION"));
		params.put("SUBREPORT_PATH", "/inv/reports/");
		fmtType = inputDTO.get("REPORT_FMT_TYPE");
		annex_Query = inputDTO.get("ANNEX_QUERY");
		grid_Query = inputDTO.get("GRID_QUERY");
		grid_columnFields = (ArrayList<String>) inputDTO.getObject("GRID_COLUMN_FIELDS");
		grid_columnDesc = (ArrayList<String>) inputDTO.getObject("GRID_COLUMN_DESC");
		grid_columnTypes = (ArrayList<String>) inputDTO.getObject("GRID_COLUMN_TYPES");
		annex_columnFields = (ArrayList<String>) inputDTO.getObject("ANNEX_COLUMN_FIELDS");
		annex_columnDesc = (ArrayList<String>) inputDTO.getObject("ANNEX_COLUMN_DESC");
		annex_columnTypes = (ArrayList<String>) inputDTO.getObject("ANNEX_COLUMN_TYPES");
		annex_mode = inputDTO.get("ANNEX_MODE");
		report_Type = String.valueOf(inputDTO.getObject(ContentManager.REPORT_FORMAT));
		if (!fmtType.equals(RegularConstants.ONE))
			isDynamic = true;
		params.put("REPORT_TYPE", report_Type);
//		if (annex_columnCount < 5) {
//			params.put("ANNEXURE_HEADER", "rinvoice_headerP.jasper");
//			params.put("ANNEXURE_FOOTER", "rinvoice_FooterP.jasper");
//		} else {
//			params.put("ANNEXURE_HEADER", "rinvoice_headerL.jasper");
//			params.put("ANNEXURE_FOOTER", "rinvoice_FooterL.jasper");
//		}
		return params;
	}

	@Override
	public int getReportProcessorType() {
		return ReportConstants.REPORT_PROCESSOR_CONNECTION;
	}

	@Override
	public String getReportPath() {
		String reportPath = RegularConstants.NULL;
		reportPath = "/inv/reports/rinvoice_main.jasper";
		return reportPath;
	}

	@Override
	public boolean isDynamic() {
		return isDynamic;
	}

	@Override
	public void initResource() {

	}

	@Override
	public void releaseResource() {

	}

	@Override
	public String getResourceBundle() {
		return ReportResourceBundles.INV_REPORTS;
	}

	private String getAmountPattern() {
		String pattern = "##,##,##,##,##,##,###.00";
		return pattern;
	}

	public List<Integer> getDyanmicReportProcessorType() {
		List<Integer> processorTypeList = new ArrayList<Integer>();
		if (fmtType.equals("2") && report_Type.equals("1"))
			processorTypeList.add(ReportConstants.REPORT_PROCESSOR_CONNECTION);
		if (fmtType.equals("3") || fmtType.equals("4") && report_Type.equals("1"))
			processorTypeList.add(ReportConstants.REPORT_PROCESSOR_CONNECTION);
		if ((fmtType.equals("2") || fmtType.equals("4")) && ((annex_mode.equals("P") || annex_mode.equals("B")) || (annex_mode.equals("E") && report_Type.equals("2"))))
			processorTypeList.add(ReportConstants.REPORT_PROCESSOR_CONNECTION);
		return processorTypeList;
	}

	public DTObject getDyanmicReportProcessData() {
		DTObject processData = new DTObject();
		int index = 0;
		if (fmtType.equals("2") && report_Type.equals("1"))
			processData.setObject(String.valueOf(index++), null);
		if (fmtType.equals("3") || fmtType.equals("4") && report_Type.equals("1"))
			processData.setObject(String.valueOf(index++), null);
		if ((fmtType.equals("2") || fmtType.equals("4")) && ((annex_mode.equals("P") || annex_mode.equals("B")) || (annex_mode.equals("E") && report_Type.equals("2"))))
			processData.setObject(String.valueOf(index++), null);
		return processData;
	}

	@Override
	public List<FastReportBuilder> getDyanmicReportContent(String reportBasePath) {
		List<FastReportBuilder> fastReportList = new ArrayList<FastReportBuilder>();
		try {
			Style fieldStyleL = new Style();
			fieldStyleL.setBorder(new Border(1));
			fieldStyleL.setHorizontalAlign(HorizontalAlign.LEFT);
			fieldStyleL.setVerticalAlign(VerticalAlign.MIDDLE);

			Style fieldStyleR = new Style();
			fieldStyleR.setBorder(new Border(1));
			fieldStyleR.setHorizontalAlign(HorizontalAlign.RIGHT);
			fieldStyleR.setVerticalAlign(VerticalAlign.MIDDLE);
			fieldStyleR.setPattern(getAmountPattern());
			fieldStyleR.setStretchWithOverflow(true);

			Style middleCell = new Style();
			middleCell.setBorderBottom(new Border(1));
			middleCell.setHorizontalAlign(HorizontalAlign.LEFT);
			middleCell.setVerticalAlign(VerticalAlign.MIDDLE);
			middleCell.setStretchWithOverflow(true);

			Style firstCell = new Style();
			firstCell.setBorderLeft(new Border(1));
			firstCell.setBorderBottom(new Border(1));
			firstCell.setHorizontalAlign(HorizontalAlign.LEFT);
			firstCell.setVerticalAlign(VerticalAlign.MIDDLE);
			firstCell.setStretchWithOverflow(true);

			Style lastCell = new Style();
			lastCell.setBorderRight(new Border(1));
			lastCell.setBorderBottom(new Border(1));
			lastCell.setHorizontalAlign(HorizontalAlign.LEFT);
			lastCell.setVerticalAlign(VerticalAlign.MIDDLE);
			lastCell.setStretchWithOverflow(true);

			Style columnHStyleC = new Style();
			columnHStyleC.setBorder(new Border(1));
			columnHStyleC.setHorizontalAlign(HorizontalAlign.CENTER);
			columnHStyleC.setVerticalAlign(VerticalAlign.MIDDLE);
			
			
			Style fieldStyleFraction = new Style();
			fieldStyleFraction.setBorder(new Border(1));
			fieldStyleFraction.setHorizontalAlign(HorizontalAlign.RIGHT);
			fieldStyleFraction.setVerticalAlign(VerticalAlign.MIDDLE);
			fieldStyleFraction.setPattern("###############.###");
			fieldStyleFraction.setStretchWithOverflow(true);
			
			Style fieldStyleNumber = new Style();
			fieldStyleNumber.setBorder(new Border(1));
			fieldStyleNumber.setHorizontalAlign(HorizontalAlign.RIGHT);
			fieldStyleNumber.setVerticalAlign(VerticalAlign.MIDDLE);
			fieldStyleNumber.setPattern("###############");
			fieldStyleNumber.setStretchWithOverflow(true);
			
			Style fieldStyleDate = new Style();
			fieldStyleDate.setBorder(new Border(1));
			fieldStyleDate.setHorizontalAlign(HorizontalAlign.CENTER);
			fieldStyleDate.setVerticalAlign(VerticalAlign.MIDDLE);
			fieldStyleDate.setPattern("dd-MM-yyyy");
			fieldStyleDate.setStretchWithOverflow(true);

			if (fmtType.equals("2") && report_Type.equals("1")) {
				FastReportBuilder rinvAnnexReport = new FastReportBuilder();
				rinvAnnexReport.setTemplateFile(reportBasePath + "/inv/reports/rinvoice_main.jrxml", true, true, true, true);
				fastReportList.add(rinvAnnexReport);

			}
			if ((fmtType.equals("3") || fmtType.equals("4")) && report_Type.equals("1")) {
				FastReportBuilder rinvGridReport = new FastReportBuilder();
				rinvGridReport.setTemplateFile(reportBasePath + "/inv/reports/rinvoice_grid.jrxml", true, true, true, false);
				rinvGridReport.setQuery(grid_Query, "SQL");
				int totalWidth = 555-90;
				grid_columnCount = grid_columnFields.size();
				int width = totalWidth / grid_columnCount;
				boolean footerReq=true;
				if(!grid_columnTypes.contains("A"))
					footerReq=false;
				for (int i = 0; i < grid_columnCount; i++) {
					String columnName = grid_columnFields.get(i);
					String columnTitle = grid_columnDesc.get(i);
					String columnType = grid_columnTypes.get(i);
					if (i == grid_columnCount - 2)
						width = totalWidth;
					if (i == grid_columnCount - 1)
						width = 90;
					if ("A".equals(columnType)) {
						AbstractColumn amountCol = ColumnBuilder.getNew().setColumnProperty(columnName, BigDecimal.class.getName()).setFixedWidth(true).setTitle(columnTitle).setWidth(new Integer(width)).setHeaderStyle(columnHStyleC).setStyle(fieldStyleR).build();
						rinvGridReport.addColumn(amountCol);
						totalWidth = totalWidth - width;
						rinvGridReport.addGlobalFooterVariable(amountCol, DJCalculation.SUM, fieldStyleR, null);

					} else if ("F".equals(columnType)) {
						AbstractColumn fractionCol = ColumnBuilder.getNew().setColumnProperty(columnName, BigDecimal.class.getName()).setFixedWidth(true).setTitle(columnTitle).setWidth(new Integer(width)).setHeaderStyle(columnHStyleC).setStyle(fieldStyleFraction).build();
						rinvGridReport.addColumn(fractionCol);
						totalWidth = totalWidth - width;
						if(footerReq){
							Style cellStyle = middleCell;
							if (i == 0)
								cellStyle = firstCell;
							else if (i == grid_columnCount - 1)
								cellStyle = lastCell;
							if (i > 0)
								rinvGridReport.addGlobalFooterVariable(fractionCol, DJCalculation.NOTHING, cellStyle, getValueFormatter());
						}

					}else if ("N".equals(columnType)) {
						AbstractColumn numberCol = ColumnBuilder.getNew().setColumnProperty(columnName, BigDecimal.class.getName()).setFixedWidth(true).setTitle(columnTitle).setWidth(new Integer(width)).setHeaderStyle(columnHStyleC).setStyle(fieldStyleNumber).build();
						rinvGridReport.addColumn(numberCol);
						totalWidth = totalWidth - width;
						if(footerReq){
							Style cellStyle = middleCell;
							if (i == 0)
								cellStyle = firstCell;
							else if (i == grid_columnCount - 1)
								cellStyle = lastCell;
							if (i > 0)
								rinvGridReport.addGlobalFooterVariable(numberCol, DJCalculation.NOTHING, cellStyle, getValueFormatter());
						}

					}else if ("D".equals(columnType)) {
						AbstractColumn dateCol = ColumnBuilder.getNew().setColumnProperty(columnName, Date.class.getName()).setFixedWidth(true).setTitle(columnTitle).setWidth(new Integer(width)).setHeaderStyle(columnHStyleC).setStyle(fieldStyleDate).build();
						rinvGridReport.addColumn(dateCol);
						totalWidth = totalWidth - width;
						if(footerReq){
							Style cellStyle = middleCell;
							if (i == 0)
								cellStyle = firstCell;
							else if (i == grid_columnCount - 1)
								cellStyle = lastCell;
							if (i > 0)
								rinvGridReport.addGlobalFooterVariable(dateCol, DJCalculation.NOTHING, cellStyle, getValueFormatter());
						}

					}else {
						AbstractColumn textCol = ColumnBuilder.getNew().setColumnProperty(columnName, String.class.getName()).setFixedWidth(true).setTitle(columnTitle).setWidth(new Integer(width)).setHeaderStyle(columnHStyleC).setStyle(fieldStyleL).build();
						rinvGridReport.addColumn(textCol);
						totalWidth = totalWidth - width;
						Style cellStyle = middleCell;
						if(footerReq){
							if (i == 0)
								cellStyle = firstCell;
							else if (i == grid_columnCount - 1)
								cellStyle = lastCell;
							if (i > 0)
								rinvGridReport.addGlobalFooterVariable(textCol, DJCalculation.NOTHING, cellStyle, getValueFormatter());
						}
					}
				}
				if(footerReq)
					rinvGridReport.setGrandTotalLegend("Total");
				rinvGridReport.setGrandTotalLegendStyle(fieldStyleL);
				rinvGridReport.setGlobalFooterVariableHeight(17);
				rinvGridReport.setWhenNoDataAllSectionNoDetail();
				fastReportList.add(rinvGridReport);

			}
			if ((fmtType.equals("2") || fmtType.equals("4")) && ((annex_mode.equals("P") || annex_mode.equals("B")) || (annex_mode.equals("E") && report_Type.equals("2")))) {
				FastReportBuilder rinvConsAnnex = new FastReportBuilder();
				annex_columnCount = annex_columnFields.size();
				int totalWidth2 = 802;
				if (annex_columnCount < 5){
					totalWidth2 = 555;
					rinvConsAnnex.setTemplateFile(reportBasePath + "/inv/reports/rinvoice_consAnnexP.jrxml", true, true, true, false);
				}else{
					rinvConsAnnex.setTemplateFile(reportBasePath + "/inv/reports/rinvoice_consAnnexL.jrxml", true, true, true, false);
				}
				rinvConsAnnex.setQuery(annex_Query, "SQL");
				annex_columnCount = annex_columnFields.size();
				
				int width2 = totalWidth2 / annex_columnCount;
				
				boolean footerReq=true;
				if(!annex_columnTypes.contains("A"))
					footerReq=false;
				for (int i = 0; i < annex_columnCount; i++) {
					String columnName1 = annex_columnFields.get(i);
					String columnTitle1 = annex_columnDesc.get(i);
					String columnType1 = annex_columnTypes.get(i);
					if (i == annex_columnCount - 1)
						width2 = totalWidth2;
					if ("A".equals(columnType1)) {
						AbstractColumn amountCol1 = ColumnBuilder.getNew().setColumnProperty(columnName1, BigDecimal.class.getName()).setFixedWidth(true).setTitle(columnTitle1).setWidth(new Integer(width2)).setHeaderStyle(columnHStyleC).setStyle(fieldStyleR).build();
						rinvConsAnnex.addColumn(amountCol1);
						totalWidth2 = totalWidth2 - width2;
						rinvConsAnnex.addGlobalFooterVariable(amountCol1, DJCalculation.SUM, fieldStyleR, null);
					}else if ("F".equals(columnType1)) {
						AbstractColumn fractionCol = ColumnBuilder.getNew().setColumnProperty(columnName1, BigDecimal.class.getName()).setFixedWidth(true).setTitle(columnTitle1).setWidth(new Integer(width2)).setHeaderStyle(columnHStyleC).setStyle(fieldStyleFraction).build();
						rinvConsAnnex.addColumn(fractionCol);
						totalWidth2 = totalWidth2 - width2;
						
						if(footerReq){
							Style cellStyle = middleCell;
							if (i == 0)
								cellStyle = firstCell;
							else if (i == annex_columnCount - 1)
								cellStyle = lastCell;
							if (i > 0)
								rinvConsAnnex.addGlobalFooterVariable(fractionCol, DJCalculation.NOTHING, cellStyle, getValueFormatter());
						}
					}else if ("N".equals(columnType1)) {
						AbstractColumn numberCol = ColumnBuilder.getNew().setColumnProperty(columnName1, BigDecimal.class.getName()).setFixedWidth(true).setTitle(columnTitle1).setWidth(new Integer(width2)).setHeaderStyle(columnHStyleC).setStyle(fieldStyleNumber).build();
						rinvConsAnnex.addColumn(numberCol);
						totalWidth2 = totalWidth2 - width2;
						
						if(footerReq){
							Style cellStyle = middleCell;
							if (i == 0)
								cellStyle = firstCell;
							else if (i == annex_columnCount - 1)
								cellStyle = lastCell;
							if (i > 0)
								rinvConsAnnex.addGlobalFooterVariable(numberCol, DJCalculation.NOTHING, cellStyle, getValueFormatter());
						}
					}else if ("D".equals(columnType1)) {
						AbstractColumn dateCol = ColumnBuilder.getNew().setColumnProperty(columnName1, Date.class.getName()).setFixedWidth(true).setTitle(columnTitle1).setWidth(new Integer(width2)).setHeaderStyle(columnHStyleC).setStyle(fieldStyleDate).build();
						rinvConsAnnex.addColumn(dateCol);
						totalWidth2 = totalWidth2 - width2;
						
						if(footerReq){
							Style cellStyle = middleCell;
							if (i == 0)
								cellStyle = firstCell;
							else if (i == annex_columnCount - 1)
								cellStyle = lastCell;
							if (i > 0)
								rinvConsAnnex.addGlobalFooterVariable(dateCol, DJCalculation.NOTHING, cellStyle, getValueFormatter());
						}
					} else {
						AbstractColumn textCol1 = ColumnBuilder.getNew().setColumnProperty(columnName1, String.class.getName()).setFixedWidth(true).setTitle(columnTitle1).setWidth(new Integer(width2)).setHeaderStyle(columnHStyleC).setStyle(fieldStyleL).build();
						rinvConsAnnex.addColumn(textCol1);
						totalWidth2 = totalWidth2 - width2;
						if(footerReq){
							Style cellStyle = middleCell;
							if (i == 0)
								cellStyle = firstCell;
							else if (i == annex_columnCount - 1)
								cellStyle = lastCell;
							if (i > 0)
								rinvConsAnnex.addGlobalFooterVariable(textCol1, DJCalculation.NOTHING, cellStyle, getValueFormatter());
						}
					}
				}
				if(footerReq)
					rinvConsAnnex.setGrandTotalLegend("Total");
				rinvConsAnnex.setGrandTotalLegendStyle(fieldStyleL);
				rinvConsAnnex.setGlobalFooterVariableHeight(17);
				rinvConsAnnex.setWhenNoDataAllSectionNoDetail();
				if (annex_columnCount < 5)
					rinvConsAnnex.setPageSizeAndOrientation(Page.Page_A4_Portrait());
				fastReportList.add(rinvConsAnnex);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
		}
		return fastReportList;
	}

	private DJValueFormatter getValueFormatter() {
		return new DJValueFormatter() {
			@SuppressWarnings("rawtypes")
			public Object evaluate(Object value, Map fields, Map variables, Map parameters) {
				return "";
			}

			public String getClassName() {
				return String.class.getName();
			}
		};
	}

}