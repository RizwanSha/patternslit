package patterns.config.web.reports.bil;

import java.io.ByteArrayInputStream;
import java.util.HashMap;
import java.util.Map;

import net.sf.jasperreports.engine.util.JRXmlUtils;

import org.w3c.dom.Document;

import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.reports.ReportConstants;
import patterns.config.framework.web.reports.fw.ReportAdapter;
import patterns.config.web.reports.resources.ReportResourceBundles;

public class rbillfilegenreport extends ReportAdapter {
	private String gridXml = RegularConstants.EMPTY_STRING;

	@Override
	public Map<String, Object> getReportParameters() {
		HashMap<String, Object> params = new HashMap<String, Object>();
		DTObject inputDTO = context.getFilters();
		params.put("ENTITY_CODE", inputDTO.get("ENTITY_CODE"));
		params.put("TEMP_SL", inputDTO.get("TEMP_SL"));
		params.put("SUBREPORT_PATH", "/bil/reports/");
		return params;
	}

	public Document processXMLData() {
		Document document = null;
		try {
			document = JRXmlUtils.parse(new ByteArrayInputStream(gridXml.getBytes()));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return document;
	}

	@Override
	public int getReportProcessorType() {
		return ReportConstants.REPORT_PROCESSOR_CONNECTION;
	}

	@Override
	public String getReportPath() {
		String reportPath = RegularConstants.NULL;
		reportPath = "/bil/reports/rbillfilegenreport.jasper";
		return reportPath;
	}

	@Override
	public void initResource() {

	}

	@Override
	public void releaseResource() {

	}

	@Override
	public String getResourceBundle() {
		return ReportResourceBundles.BIL_REPORTS;
	}

}
