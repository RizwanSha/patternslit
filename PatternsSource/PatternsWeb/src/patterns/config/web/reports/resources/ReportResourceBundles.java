package patterns.config.web.reports.resources;

public class ReportResourceBundles {

	public static final String LAB_REPORTS = "lab";
	public static final String OPT_REPORTS = "opt";
	public static final String REG_REPORTS = "reg";
	public static final String IPT_REPORTS = "ipt";
	public static final String CMN_REPORTS = "cmn";
	public static final String INV_REPORTS = "inv";
	public static final String PAYROLL_REPORTS = "payroll";
	public static final String TRAN_REPORTS = "tran";
	public static final String GL_REPORTS = "gl";
	public static final String BIL_REPORTS = "bil";
	public static final String LSS_REPORTS = "lss";
}
