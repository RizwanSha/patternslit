package patterns.config.web.reports.lss;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ar.com.fdvs.dj.domain.builders.FastReportBuilder;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.framework.web.reports.ReportConstants;
import patterns.config.framework.web.reports.fw.ReportAdapter;
import patterns.config.web.reports.resources.ReportResourceBundles;

public class rintdebitnotereport extends ReportAdapter {

	boolean isDynamic = false;

	@Override
	public Map<String, Object> getReportParameters() {
		HashMap<String, Object> params = new HashMap<String, Object>();
		DTObject inputDTO = context.getFilters();
		params.put("INPUTDTO", inputDTO);
		params.put("ENTITY_CODE", Long.parseLong(inputDTO.get("ENTITY_CODE")));
		params.put("DEBIT_NOTE_INVENTORY", Long.parseLong(inputDTO.get("DEBIT_NOTE_INVENTORY")));
		params.put("INTEREST_CCY", inputDTO.get("INTEREST_CCY"));
		params.put("DEBIT_NOTE_DATE", new java.util.Date(BackOfficeFormatUtils.getDate(inputDTO.get("DEBIT_NOTE_DATE"), inputDTO.get("DATE_FORMAT")).getTime()));
		params.put("BRANCH_CODE", inputDTO.get("BRANCH_CODE"));
		params.put("AMOUNT", new BigDecimal(inputDTO.get("AMOUNT")));
		params.put("NOF_INT_ENTRIES", Integer.parseInt(inputDTO.get("NOF_INT_ENTRIES")));
		params.put("SUBREPORT_PATH", "/lss/reports/");
		if (Integer.parseInt(inputDTO.get("NOF_INT_ENTRIES")) > 1)
			isDynamic = true;
		return params;
	}

	@Override
	public boolean isDynamic() {
		return isDynamic;
	}

	@Override
	public void initResource() {

	}

	@Override
	public void releaseResource() {

	}

	@Override
	public int getReportProcessorType() {
		return ReportConstants.REPORT_PROCESSOR_CONNECTION;
	}

	public List<Integer> getDyanmicReportProcessorType() {
		List<Integer> processorTypeList = new ArrayList<Integer>();
		processorTypeList.add(ReportConstants.REPORT_PROCESSOR_CONNECTION);
		processorTypeList.add(ReportConstants.REPORT_PROCESSOR_CONNECTION);
		return processorTypeList;
	}

	public DTObject getDyanmicReportProcessData() {
		DTObject processData = new DTObject();
		processData.setObject("0", null);
		processData.setObject("1", null);
		return processData;
	}

	public List<FastReportBuilder> getDyanmicReportContent(String reportBasePath) {
		List<FastReportBuilder> fastReportList = new ArrayList<FastReportBuilder>();
		try {
			FastReportBuilder rintdebitnote = new FastReportBuilder();
			rintdebitnote.setTemplateFile(reportBasePath + "/lss/reports/rintdebitnote.jrxml", true, true, true, true);
			fastReportList.add(rintdebitnote);

			FastReportBuilder rintdebitnote_Annexure = new FastReportBuilder();
			rintdebitnote_Annexure.setTemplateFile(reportBasePath + "/lss/reports/rintdebitnote_Annexure.jrxml", true, true, true, true);
			fastReportList.add(rintdebitnote_Annexure);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
		}
		return fastReportList;
	}

	@Override
	public String getReportPath() {
		String reportPath = RegularConstants.NULL;
		reportPath = "/lss/reports/rintdebitnote.jasper";
		return reportPath;
	}

	@Override
	public String getResourceBundle() {
		return ReportResourceBundles.LSS_REPORTS;
	}

}