package patterns.config.web.actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class UnauthorizedAction extends GenericAction {

	public UnauthorizedAction() {
		getLogger().logDebug("#()");
	}

	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		getLogger().logError("execute()");
		return mapping.findForward(UNAUTH_ACTION);
	}
}