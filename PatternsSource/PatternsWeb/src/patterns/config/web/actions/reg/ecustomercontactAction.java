package patterns.config.web.actions.reg;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionRedirect;

import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.loggers.ApplicationLogger;
import patterns.config.framework.process.TBAProcessResult;
import patterns.config.framework.web.RequestConstants;
import patterns.config.web.actions.GenericAction;

public class ecustomercontactAction extends GenericAction {

	private ApplicationLogger logger = null;

	public ecustomercontactAction() {
		logger = ApplicationLogger.getInstance(this.getClass().getSimpleName());
		logger.logDebug("#()");
	}

	public ApplicationLogger getLogger() {
		return logger;
	}

	public boolean doConditionalRedirect(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response, TBAProcessResult result, ActionRedirect redirect) {
		String redirectPageId = request.getParameter("redirectPageId");
		if (redirectPageId != null && !redirectPageId.equals(RegularConstants.EMPTY_STRING)) {
			redirect.addParameter(RequestConstants.REDIRECT_LINK, redirectPageId);
			return true;
		} else
			return false;

	}
}