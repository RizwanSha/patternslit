package patterns.config.web.actions;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionRedirect;

import patterns.config.delegate.BackOfficeProcessManager;
import patterns.config.framework.bo.AdditionalDetailInfo;
import patterns.config.framework.bo.MessageParam;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.loggers.ApplicationLogger;
import patterns.config.framework.pki.ProcessTFAInfo;
import patterns.config.framework.process.TBAActionType;
import patterns.config.framework.process.TBAProcessAction;
import patterns.config.framework.process.TBAProcessInfo;
import patterns.config.framework.process.TBAProcessResult;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.thread.ApplicationContext;
import patterns.config.framework.web.HTMLGridUtility;
import patterns.config.framework.web.RequestConstants;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.framework.web.configuration.program.ProgramConfiguration;
import patterns.config.framework.web.configuration.program.ProgramMap;
import patterns.config.web.forms.GenericFormBean;

public class GenericAction extends Action {
	private ApplicationLogger logger = null;

	public static final String SUCCESS_ACTION = "success";
	public static final String CHANGE_ACTION = "change";
	public static final String RESET_ACTION = "reset";
	public static final String FAILURE_ACTION = "failure";
	public static final String UNAUTH_ACTION = "unauth";

	public GenericAction() {
		logger = ApplicationLogger.getInstance(this.getClass().getSimpleName());
		logger.logDebug("#()");
	}

	public ApplicationLogger getLogger() {
		return logger;
	}

	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		String forwardName = null;
		if (!(form instanceof GenericFormBean)) {
			forwardName = GenericAction.UNAUTH_ACTION;
		} else {
			GenericFormBean formBean = (GenericFormBean) form;
			ApplicationContext context = ApplicationContext.getInstance();
			if (formBean.getCommand().equals(GenericFormBean.RESET_ACTION)) {
				ActionRedirect redirect = new ActionRedirect(mapping.findForward(RESET_ACTION));
				redirect.addParameter("callSource", context.getCallSource());
				return redirect;
			}
			if (formBean.getTfaSuccess().equals(RegularConstants.COLUMN_DISABLE)) {
				forwardName = GenericAction.UNAUTH_ACTION;
				ActionRedirect redirect = new ActionRedirect(mapping.findForward(forwardName));
				redirect.addParameter(RequestConstants.SUCCESS_FLAG, RequestConstants.SUCCESS_FLAG_FAILURE);
				redirect.addParameter(RequestConstants.ADDITIONAL_INFO, formBean.getCommonResource(formBean.getFormDTO().get(ContentManager.ERROR), null));
				formBean.getFormDTO().reset();
				return redirect;
			} else {
				BackOfficeProcessManager processManager = new BackOfficeProcessManager();
				TBAProcessInfo processInfo = new TBAProcessInfo();
				processInfo.setProcessData(formBean.getFormDTO());
				processInfo.setProcessBO(formBean.getProcessBO());
				TBAProcessAction processAction = new TBAProcessAction();
				//ApplicationContext context = ApplicationContext.getInstance();
				processAction.setPartitionNo(context.getPartitionNo());
				processAction.setActionEntity(context.getEntityCode());
				processAction.setActionBranch(context.getBranchCode());
				// added by swaroopa as on 27/07/2016 begins
				processAction.setClusterCode(context.getClusterCode());
				processAction.setFinYear(context.getFinYear());
				// added by swaroopa as on 27/07/2016 ends
				
				// added by swaroopa as on 26-04-2012 begins
				ProcessTFAInfo processTFAInfo = new ProcessTFAInfo();
				processTFAInfo.setDeploymentContext(context.getDeploymentContext());
				processTFAInfo.setUserId(context.getUserID());
				processTFAInfo.setDigitalCertificateInventoryNumber(context.getDigitalCertificateInventoryNumber());
				processTFAInfo.setTfaRequired(formBean.getTfaRequired());
				processTFAInfo.setTfaSuccess(formBean.getTfaSuccess());
				processTFAInfo.setTfaValue(formBean.getTfaValue());
				processTFAInfo.setTfaEncodedValue(formBean.getTfaEncodedValue());
				processInfo.setProcessTFAInfo(processTFAInfo);
				// added by swaroopa as on 26-04-2012 ends
				TBAActionType actionType = null;
				String action = formBean.getAction();
				if (action != null) {
					if (action.equals("A"))
						actionType = TBAActionType.ADD;
					else if (action.equals("M"))
						actionType = TBAActionType.MODIFY;
				}
				processAction.setActionType(actionType);

				processAction.setActionUser(context.getUserID());
				processAction.setActionDate(context.getCurrentBusinessDate());
				processAction.setActionCustomer(context.getCustomerCode());
				//ARIBALA ADDED ON 09102015 BEGIN
				if(formBean.getRectify().equals(RegularConstants.COLUMN_ENABLE)){
					processAction.setRectification(true);
				}else{
					processAction.setRectification(false);
				}
				//ARIBALA ADDED ON 09102015 END
				
				if (formBean.getParentProcessId() == null) {
					processAction.setProcessID(context.getProcessID().toUpperCase(context.getLocale()));
				} else {
					processAction.setProcessID(formBean.getParentProcessId().toUpperCase(context.getLocale()));
				}
				// processAction.setProcessID(context.getProcessID().toUpperCase(context.getLocale()));
				processAction.setActionIP(context.getClientIP());
				processAction.setFinYear(context.getFinYear());
				processAction.setCurrentYear(context.getCurrentYear());
				processAction.setDateFormat(context.getDateFormat());
				processAction.setActionRole(context.getRoleCode());
				processInfo.setProcessAction(processAction);
				//System.out.println("Start Time:" + new java.util.Date(System.currentTimeMillis()).toString());
				TBAProcessResult processResult = processManager.delegate(processInfo);
				ActionRedirect redirect = new ActionRedirect();
				switch (processResult.getProcessStatus()) {
				case FAILURE:
					forwardName = GenericAction.FAILURE_ACTION;
					redirect.addParameter(RequestConstants.SUCCESS_FLAG, RequestConstants.SUCCESS_FLAG_FAILURE);
					break;
				case SUCCESS:
					forwardName = GenericAction.SUCCESS_ACTION;
					redirect.addParameter(RequestConstants.SUCCESS_FLAG, RequestConstants.SUCCESS_FLAG_SUCCESS);
					break;
				default:
					break;
				}
				//System.out.println("End Time:" + new java.util.Date(System.currentTimeMillis()).toString());
				// Jainudeen Changes 28/NOV/2014 Begin
				Object additionalInfo = processResult.getAdditionalInfo();
				// Jainudeen Changes 28/NOV/2014 End
				ActionForward actionForward = mapping.findForward(GenericAction.SUCCESS_ACTION);
				redirect.setPath(actionForward.getPath());
				redirect.addParameter("callSource", context.getCallSource());
				redirect.addParameter(RequestConstants.RESULT_CODE, processResult.getErrorCode());
				// Jainudeen Changes 28/NOV/2014 Begin
				if (additionalInfo != null && additionalInfo instanceof MessageParam) {
					MessageParam messageParam = (MessageParam) additionalInfo;
					redirect.addParameter(RequestConstants.ADDITIONAL_INFO, formBean.getErrorResource(messageParam.getCode(), messageParam.toParameterArray()));
				} else if (additionalInfo != null && additionalInfo instanceof AdditionalDetailInfo) {
					redirect.addParameter(RequestConstants.ADDITIONAL_DETAIL_INFO, getAdditionalDetailInfoHtml(formBean,(AdditionalDetailInfo)additionalInfo));
				} else if (additionalInfo != null && additionalInfo instanceof Object) {
					redirect.addParameter(RequestConstants.ADDITIONAL_INFO, additionalInfo);
				} else
					redirect.addParameter(RequestConstants.ADDITIONAL_INFO, formBean.getErrorResource(processResult.getErrorCode(), null));
				// Jainudeen Changes 28/NOV/2014 End
				
				//Rizwan Changes done related to Back to Template on 24 Jun 2015 begins
				ProgramConfiguration programConfiguration = ProgramMap.getInstance().getConfiguration(context.getProcessID());
				redirect.addParameter("primaryKey", processResult.getPrimaryKey());
				boolean backToTmplt = programConfiguration.isBackToTemplate();
				if(backToTmplt && TBAProcessStatus.SUCCESS.equals(processResult.getProcessStatus())){
					//redirect.addParameter("primaryKey", processResult.getPrimaryKey());
					redirect.addParameter("backToTemplate", RegularConstants.COLUMN_ENABLE);
					redirect.addParameter("sourceKey", processResult.getSourceKey());
				}
				//Rizwan Changes done related to Back to Template on 24 Jun 2015 end
				boolean printRequired = programConfiguration.isPrintRequired();
				if(printRequired && doConditionalPrint(mapping,form,request,response,processResult,redirect) && TBAProcessStatus.SUCCESS.equals(processResult.getProcessStatus())){
					redirect.addParameter("printRequired", RegularConstants.COLUMN_ENABLE);
					redirect.addParameter("beanPath", form.getClass().getName());
					//redirect.addParameter("primaryKey", processResult.getPrimaryKey());
				}
				if (processResult.getResponseDTO() != null) {
					if (processResult.getResponseDTO().get(RegularConstants.ERROR) != null) {
						redirect.addParameter(RequestConstants.ADDITIONAL_DETAILS, processResult.getResponseDTO().get(RegularConstants.ERROR));
					} else if (processResult.getResponseDTO().get(RegularConstants.ADDITIONAL_INFO) != null) {
						redirect.addParameter(RequestConstants.ADDITIONAL_DETAILS, processResult.getResponseDTO().get(RegularConstants.ADDITIONAL_INFO));
					}
				}
				if(!doConditionalRedirect(mapping,form,request,response,processResult,redirect))
					redirect.addParameter(RequestConstants.REDIRECT_LINK, context.getProcessID().toLowerCase(context.getLocale()));
				redirect.addParameter(RequestConstants.CURRENT_OPERATION, formBean.getAction());
				formBean.getFormDTO().reset();
				return redirect;
			}
		}
		return mapping.findForward(forwardName);
	}
	protected boolean doConditionalRedirect(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response, TBAProcessResult result,ActionRedirect redirect) {
        return false;
	}
	protected boolean doConditionalPrint(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response, TBAProcessResult result,ActionRedirect redirect) {
        return true;
	}
	private String getAdditionalDetailInfoHtml(GenericFormBean form,AdditionalDetailInfo additionalInfo){
		HTMLGridUtility gridXML = new HTMLGridUtility();
		gridXML.init("stage-grid");
		gridXML.startHead();
		
		/*String headerAttributeKey[]={"colspan","class","style"};
		String headerAttributeValue[]={"2","level2_title","text-align: center;"};
		
		String rowEvenAttributeKey[]={"class","style"};
		String rowEvenAttributeValue[]={"row_even","text-align: left;"};
		String rowOddAttributeKey[]={"class","style"};
		String rowOddAttributeValue[]={"row_odd","text-align: left;"};
		
		String cellCaptionAttributeKey[]={"class"};
		String cellCaptionAttributeValue[]={"level5_description"};
		String cellValueAttributeKey[]={"style"};
		String cellValueAttributeValue[]={"font-size:12px"};
		*/
		
		String headerAttributeKey[]={"colspan","class"};
		String headerAttributeValue[]={"2","additionalInfo_title"};
		
		String rowEvenAttributeKey[]={"class","style"};
		String rowEvenAttributeValue[]={"row_even","text-align: left;"};
		String rowOddAttributeKey[]={"class","style"};
		String rowOddAttributeValue[]={"row_odd","text-align: left;"};
		
		String cellCaptionAttributeKey[]={"class"};
		String cellCaptionAttributeValue[]={"additionalInfo_key",};
		String cellValueAttributeKey[]={"class","style"};
		String cellValueAttributeValue[]={"additionalInfo_value","min-width:75px"};
		
		gridXML.setCell(form.getErrorResource(additionalInfo.getHeading().getCode(),additionalInfo.getHeading().toParameterArray()),headerAttributeKey,headerAttributeValue,true);
		gridXML.endHead();
		int i=0;
		
		for(MessageParam messageParam:additionalInfo.getKey()){
			if(i%2==0)
				gridXML.startRow(rowEvenAttributeKey,rowEvenAttributeValue);
			else
				gridXML.startRow(rowOddAttributeKey,rowOddAttributeValue);
			gridXML.setCell(form.getErrorResource(messageParam.getCode(),messageParam.toParameterArray()),cellCaptionAttributeKey,cellCaptionAttributeValue,true);
			gridXML.setCell(additionalInfo.getValue().get(i),cellValueAttributeKey,cellValueAttributeValue,true);
			gridXML.endRow();
			i++;
		}
		gridXML.finish();
		return gridXML.getHTML();
	}
}