package patterns.config.web.actions.common;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionRedirect;

import patterns.config.delegate.BackOfficeProcessManager;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.process.TBAProcessResult;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.thread.WebContext;
import patterns.config.framework.web.RequestConstants;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.web.actions.GenericAction;
import patterns.config.web.forms.GenericFormBean;

public class euserpreferenceaction extends GenericAction {

	public euserpreferenceaction() {
		getLogger().logInfo("#()");
	}

	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		String forwardName = null;
		if (!(form instanceof GenericFormBean)) {
			forwardName = GenericAction.UNAUTH_ACTION;
		} else {
			GenericFormBean formBean = (GenericFormBean) form;
			BackOfficeProcessManager processManager = new BackOfficeProcessManager();
			DTObject inputDTO = formBean.getFormDTO();
			
			if (!(inputDTO.get(ContentManager.ERROR).equals(RegularConstants.EMPTY_STRING))) {
				ActionRedirect redirect = new ActionRedirect(mapping.findForward(GenericAction.FAILURE_ACTION));
				redirect.addParameter(RequestConstants.SUCCESS_FLAG, RequestConstants.SUCCESS_FLAG_FAILURE);
				redirect.addParameter(RequestConstants.ADDITIONAL_INFO, formBean.getCommonResource(formBean.getFormDTO().get(ContentManager.ERROR), null));
				formBean.getFormDTO().reset();
				return redirect;
			}
			
			inputDTO.set(RegularConstants.PROCESSBO_CLASS, formBean.getProcessBO());
			TBAProcessResult processResult = processManager.delegate(inputDTO);
			if (processResult.getProcessStatus().equals(TBAProcessStatus.FAILURE)) {
				WebContext webContext = WebContext.getInstance();
				webContext.getRequest().setAttribute(BackOfficeConstants.INFO_MESSAGE, formBean.getCommonResource(processResult.getErrorCode(), null));
				return mapping.findForward(GenericAction.FAILURE_ACTION);
			} else {
				if (formBean.getCommand().equals(GenericAction.SUCCESS_ACTION)) {
					forwardName = GenericAction.SUCCESS_ACTION;
				} else {
					return mapping.findForward(GenericAction.FAILURE_ACTION);
				}
			}
			
		}
		return mapping.findForward(forwardName);
	}
}
