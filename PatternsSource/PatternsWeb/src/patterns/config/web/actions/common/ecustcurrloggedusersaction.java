package patterns.config.web.actions.common;

import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionRedirect;

import patterns.config.delegate.BackOfficeProcessManager;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.process.TBAProcessResult;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.thread.ApplicationContext;
import patterns.config.framework.thread.WebContext;
import patterns.config.framework.web.RequestConstants;
import patterns.config.web.actions.GenericAction;
import patterns.config.web.forms.GenericFormBean;

public class ecustcurrloggedusersaction extends GenericAction {

	public ecustcurrloggedusersaction() {
		getLogger().logInfo("#()");
	}

	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		String forwardName = null;
		if (!(form instanceof GenericFormBean)) {
			forwardName = GenericAction.UNAUTH_ACTION;
		} else {
			GenericFormBean formBean = (GenericFormBean) form;
			if (formBean.getCommand().equals(GenericFormBean.RESET_ACTION)) {
				return mapping.findForward(RESET_ACTION);
			} else {
				ApplicationContext context = ApplicationContext.getInstance();
				ResourceBundle bundle = ResourceBundle.getBundle("panacea/portal/web/forms/Messages", context.getLocale());
				BackOfficeProcessManager processManager = new BackOfficeProcessManager();
				DTObject inputDTO = formBean.getFormDTO();
				inputDTO.set(RegularConstants.PROCESSBO_CLASS, formBean.getProcessBO());
				TBAProcessResult processResult = processManager.delegate(inputDTO);
				if (processResult.getProcessStatus().equals(TBAProcessStatus.FAILURE)) {
					WebContext webContext = WebContext.getInstance();
					webContext.getRequest().setAttribute(BackOfficeConstants.INFO_MESSAGE, bundle.getString(processResult.getErrorCode()));
					forwardName = GenericAction.SUCCESS_ACTION;
				} else {
					forwardName = GenericAction.SUCCESS_ACTION;
				}
				ActionRedirect redirect = new ActionRedirect(mapping.findForward(forwardName));
				switch (processResult.getProcessStatus()) {
				case FAILURE:
					redirect.addParameter(RequestConstants.SUCCESS_FLAG, RequestConstants.SUCCESS_FLAG_FAILURE);
					redirect.addParameter(RequestConstants.ADDITIONAL_INFO, processResult.getErrorCode());
					break;
				case SUCCESS:
				default:
					redirect.addParameter(RequestConstants.SUCCESS_FLAG, RequestConstants.SUCCESS_FLAG_SUCCESS);
					redirect.addParameter(RequestConstants.ADDITIONAL_INFO, processResult.getAdditionalInfo());
					break;
				}
				redirect.addParameter(RequestConstants.REDIRECT_LINK, context.getProcessID().toLowerCase(context.getLocale()));
				redirect.addParameter(RequestConstants.CURRENT_OPERATION, formBean.getAction());
				return redirect;
			}
		}
		return mapping.findForward(forwardName);
	}
}
