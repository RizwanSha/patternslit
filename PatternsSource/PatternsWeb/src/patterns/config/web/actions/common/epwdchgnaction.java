package patterns.config.web.actions.common;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import patterns.config.delegate.BackOfficeProcessManager;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.process.TBAProcessResult;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.thread.ApplicationContext;
import patterns.config.framework.thread.WebContext;
import patterns.config.framework.web.RequestConstants;
import patterns.config.framework.web.SessionConstants;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.web.actions.GenericAction;
import patterns.config.web.forms.GenericFormBean;

public class epwdchgnaction extends GenericAction {
	public epwdchgnaction() {
		getLogger().logInfo("#()");
	}

	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		String forwardName = null;
		if (!(form instanceof GenericFormBean)) {
			forwardName = GenericAction.UNAUTH_ACTION;
		} else {
			GenericFormBean formBean = (GenericFormBean) form;
			if (formBean.getCommand().equals(GenericFormBean.RESET_ACTION)) {
				return mapping.findForward(RESET_ACTION);
			}
			if (formBean.getTfaSuccess().equals(RegularConstants.COLUMN_DISABLE)) { // added by swaroopa as on 04-06-2012 begins
				forwardName = UNAUTH_ACTION;
				WebContext.getInstance().getRequest().getSession().setAttribute(RequestConstants.ADDITIONAL_INFO, formBean.getCommonResource(formBean.getFormDTO().get(ContentManager.ERROR), null));
				formBean.getFormDTO().reset();
				return mapping.findForward(forwardName); // added by swaroopa as on 04-06-2012 ends
			} else {
				BackOfficeProcessManager processManager = new BackOfficeProcessManager();
				DTObject inputDTO = formBean.getFormDTO();
				ApplicationContext context = ApplicationContext.getInstance();
				HttpSession session = WebContext.getInstance().getRequest().getSession();
				// added by swaroopa as on 04-06-2012 begins
				inputDTO.set("P_PROCESS_ID", context.getProcessID());
				inputDTO.set("P_ACTION_CONTEXT", context.getDeploymentContext());
				inputDTO.set("P_CERT_INV_NUM", (String) session.getAttribute(SessionConstants.CERT_INV_NUM));
				inputDTO.set("P_TFA_VALUE", formBean.getTfaValue());
				inputDTO.set("P_ENC_VALUE", formBean.getTfaEncodedValue());
				inputDTO.set("P_BRANCH_CODE", context.getBranchCode());
				inputDTO.set("P_CUSTOMER_CODE", context.getCustomerCode());
				inputDTO.set("P_USER_IP", context.getClientIP());
				// added by swaroopa as on 04-06-2012 ends
				inputDTO.set(RegularConstants.PROCESSBO_CLASS, formBean.getProcessBO());
				TBAProcessResult processResult = processManager.delegate(inputDTO);
				WebContext webContext = WebContext.getInstance();
				if (processResult.getProcessStatus().equals(TBAProcessStatus.FAILURE)) {
					webContext.getSession().setAttribute(RequestConstants.ADDITIONAL_INFO, formBean.getCommonResource(processResult.getErrorCode(), null));
					forwardName = GenericAction.FAILURE_ACTION;
				} else {
					webContext.getSession().setAttribute(RequestConstants.ADDITIONAL_INFO, formBean.getCommonResource("password.changesuccess", null));
					forwardName = GenericAction.SUCCESS_ACTION;
				}
			}
		}
		return mapping.findForward(forwardName);
	}
}
