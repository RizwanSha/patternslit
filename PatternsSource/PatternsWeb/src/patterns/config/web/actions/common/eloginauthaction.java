package patterns.config.web.actions.common;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;

import javax.servlet.ServletContext;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import patterns.config.delegate.BackOfficeProcessManager;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.monitor.ContextReference;
import patterns.config.framework.process.TBAProcessResult;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.thread.ApplicationContext;
import patterns.config.framework.thread.WebContext;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.framework.web.GenericOption;
import patterns.config.framework.web.PasswordUtils;
import patterns.config.framework.web.RequestConstants;
import patterns.config.framework.web.SessionConstants;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.framework.web.configuration.menu.MenuUtils;
import patterns.config.framework.web.utils.ContextUtility;
import patterns.config.validations.AccessValidator;
import patterns.config.web.actions.GenericAction;
import patterns.config.web.forms.GenericFormBean;

public class eloginauthaction extends GenericAction {

	public eloginauthaction() {
		getLogger().logInfo("#()");
	}

	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		String forwardName = null;
		if (!(form instanceof GenericFormBean)) {
			forwardName = GenericAction.UNAUTH_ACTION;
		} else {

			GenericFormBean formBean = (GenericFormBean) form;
			if (formBean.getCommand().equals(GenericFormBean.RESET_ACTION)) {
				return mapping.findForward(RESET_ACTION);
			} // added by swaroopa as on 04-06-2012 begins
			if (!formBean.isTFAHandlingSelfManaged() && formBean.getTfaSuccess().equals(RegularConstants.COLUMN_DISABLE)) {
				forwardName = UNAUTH_ACTION;
				WebContext.getInstance().getRequest().getSession().setAttribute(RequestConstants.ADDITIONAL_INFO, formBean.getCommonResource(formBean.getFormDTO().get(ContentManager.ERROR), null));
				formBean.getFormDTO().reset();
				return mapping.findForward(forwardName);
				// added by swaroopa as on 04-06-2012 ends
			} else {
				ApplicationContext context = ApplicationContext.getInstance();
				WebContext webContext = WebContext.getInstance();
				HttpSession session = webContext.getRequest().getSession();
				BackOfficeProcessManager processManager = new BackOfficeProcessManager();
				DTObject inputDTO = formBean.getFormDTO();
				// added by swaroopa as on 01-06-2012 begins
				inputDTO.set("P_PROCESS_ID", context.getProcessID());
				inputDTO.set("P_ACTION_CONTEXT", context.getDeploymentContext());
				inputDTO.set("P_CERT_INV_NUM", (String) session.getAttribute(SessionConstants.CERT_INV_NUM));
				inputDTO.set("P_TFA_VALUE", formBean.getTfaValue());
				inputDTO.set("P_ENC_VALUE", formBean.getTfaEncodedValue());
				// added by swaroopa as on 01-06-2012 ends
				inputDTO.set(RegularConstants.PROCESSBO_CLASS, formBean.getProcessBO());
				TBAProcessResult processResult = processManager.delegate(inputDTO);
				getLogger().logDebug("Login Status : " + processResult.getProcessStatus());
				if (processResult.getProcessStatus().equals(TBAProcessStatus.FAILURE)) {
					getLogger().logDebug("Login Failure : " + processResult.getErrorCode());
					session.setAttribute(RequestConstants.ADDITIONAL_INFO, formBean.getCommonResource(processResult.getErrorCode(), null));
					forwardName = GenericAction.FAILURE_ACTION;
				} else {
					if (processResult.getErrorCode() != null  && !processResult.getErrorCode().isEmpty()) {
						getLogger().logDebug("Login Failure : " + processResult.getErrorCode());
						session.setAttribute(RequestConstants.ADDITIONAL_INFO, formBean.getCommonResource(processResult.getErrorCode(), null));
						forwardName = GenericAction.FAILURE_ACTION;
					} else {
						String oldSessionID = session.getId();
						String tfaRequired = (String) session.getAttribute(SessionConstants.TFA_REQ);
						String loginTfaRequired = (String) session.getAttribute(SessionConstants.LOGIN_TFA_REQ);
						String certificateInventoryNumber = (String) session.getAttribute(SessionConstants.CERT_INV_NUM);
						String organizationName = (String) session.getAttribute(SessionConstants.ORG_NAME);
						boolean secureSite = (Boolean) session.getAttribute(SessionConstants.SECURE_SITE);

						session.invalidate();
						session = webContext.getSession(true);
						String newSessionID = session.getId();
						if (oldSessionID.equals(newSessionID)) {
							getLogger().logDebug("Login Failure : BLV999");
							session.setAttribute(RequestConstants.ADDITIONAL_INFO, formBean.getCommonResource("BLV997", null));
							forwardName = GenericAction.FAILURE_ACTION;
						} else {
							DTObject resultDTO = (DTObject) processResult.getAdditionalInfo();
							// swaroopa as on 16-05-2012 begins
							ServletContext servletContext = ContextReference.getContext();
							String httpsMode = servletContext.getInitParameter(RegularConstants.INIT_HTTPS);
							boolean httpsEnabled = httpsMode != null && httpsMode.equals(RegularConstants.COLUMN_ENABLE);
							
							Cookie sessionCookie = new Cookie(RegularConstants.SESSIONID_COOKIE, session.getId());
							sessionCookie.setPath("/");
							if (httpsEnabled) {
								sessionCookie.setSecure(true);
							}
							response.addCookie(sessionCookie);
							String randomNonce = PasswordUtils.generateRandomNonce(resultDTO.get(SessionConstants.USER_ID), resultDTO.get(SessionConstants.CLIENT_IP));
							Cookie nonceCookie = new Cookie(RegularConstants.NONCE_COOKIE, randomNonce);
							nonceCookie.setPath("/");
							if (httpsEnabled) {
								nonceCookie.setSecure(true);
							}
							response.addCookie(nonceCookie);
							
							session.setAttribute(SessionConstants.TFA_NONCE, randomNonce);
							session.setAttribute(SessionConstants.SECURE_SITE, secureSite);
							session.setAttribute(SessionConstants.CLIENT_IP, resultDTO.get(SessionConstants.CLIENT_IP));
							String deploymentContext = servletContext.getInitParameter(RegularConstants.INIT_ACCESS_TYPE);
							session.setAttribute(SessionConstants.DEPLOYMENT_CONTEXT, deploymentContext);
							session.setAttribute(SessionConstants.LOCALE, context.getLocale());
							session.setMaxInactiveInterval((Integer) resultDTO.getObject("P_AUTO_LOGOUT_SECS"));
							session.setAttribute(SessionConstants.PARTITION_NO, context.getPartitionNo());
							session.setAttribute(SessionConstants.ENTITY_CODE, resultDTO.get(SessionConstants.ENTITY_CODE));
							session.setAttribute(SessionConstants.BRANCH_CODE, resultDTO.get(SessionConstants.BRANCH_CODE));
							session.setAttribute(SessionConstants.ORG_NAME, organizationName);
							session.setAttribute(SessionConstants.USER_ID, resultDTO.get(SessionConstants.USER_ID));
							session.setAttribute(SessionConstants.USER_AGENT, resultDTO.get(SessionConstants.USER_AGENT));
							session.setAttribute(SessionConstants.BASE_CURRENCY, resultDTO.get(SessionConstants.BASE_CURRENCY));
							session.setAttribute(SessionConstants.CBD, resultDTO.getObject(SessionConstants.CBD));
							session.setAttribute(SessionConstants.CBD_STRING, BackOfficeFormatUtils.getDate((Date) resultDTO.getObject(SessionConstants.CBD), resultDTO.get(SessionConstants.DATE_FORMAT)));
							session.setAttribute(SessionConstants.USER_NAME, resultDTO.get(SessionConstants.USER_NAME));
							session.setAttribute(SessionConstants.ROLE_TYPE, resultDTO.get(SessionConstants.ROLE_TYPE));
							session.setAttribute(SessionConstants.ROLE_CODE, resultDTO.get(SessionConstants.ROLE_CODE));
							session.setAttribute(SessionConstants.LAST_LOGIN_DATE_TIME, resultDTO.getObject(SessionConstants.LAST_LOGIN_DATE_TIME));
							session.setAttribute(SessionConstants.PASSWORD_RESET, resultDTO.get(SessionConstants.PASSWORD_RESET));
							session.setAttribute(SessionConstants.LOGIN_DATE_TIME, resultDTO.getObject(SessionConstants.LOGIN_DATE_TIME));
							session.setAttribute(SessionConstants.DATE_FORMAT, resultDTO.get(SessionConstants.DATE_FORMAT));
							session.setAttribute(SessionConstants.CURRENCY_UNITS, resultDTO.get(SessionConstants.CURRENCY_UNITS));
							session.setAttribute(SessionConstants.EMPCODE_TYPE, resultDTO.get(SessionConstants.EMPCODE_TYPE));
							session.setAttribute(SessionConstants.EMPCODE_SIZE, resultDTO.get(SessionConstants.EMPCODE_SIZE));
							session.setAttribute(SessionConstants.REG_NO_YR_PREFIX_REQD, resultDTO.get(SessionConstants.REG_NO_YR_PREFIX_REQD));
							session.setAttribute(SessionConstants.CURR_YEAR, resultDTO.get(SessionConstants.CURR_YEAR));
							session.setAttribute(SessionConstants.FIN_YEAR, resultDTO.get(SessionConstants.FIN_YEAR));
							session.setAttribute(SessionConstants.START_FIN_MONTH, resultDTO.get(SessionConstants.START_FIN_MONTH));
							// added by swaroopa as on 27/07/2017 begins
							session.setAttribute(SessionConstants.CLUSTER_CODE, resultDTO.get(SessionConstants.CLUSTER_CODE));
							// added by swaroopa as on 27/07/2017 ends
							
							session.setAttribute(SessionConstants.TFA_REQ, tfaRequired);
							session.setAttribute(SessionConstants.LOGIN_TFA_REQ, loginTfaRequired);
							session.setAttribute(SessionConstants.CERT_INV_NUM, certificateInventoryNumber);

							ApplicationContext applicationContext = new ContextUtility().loadApplicationContextOnLogin(session, request);
							ApplicationContext.setInstance(applicationContext);
							session.setAttribute(SessionConstants.APPLICATION_CONTEXT, applicationContext);

							String userID = resultDTO.get(SessionConstants.USER_ID);
							MenuUtils menu = new MenuUtils();
							LinkedList<GenericOption> roleList = menu.getRoles(userID);
							session.setAttribute(SessionConstants.ROLE_LIST, roleList);
							String roleType = resultDTO.get(SessionConstants.ROLE_TYPE);
							ArrayList<GenericOption> consoleList = menu.getConsoleList(roleType,resultDTO.get(SessionConstants.ROLE_CODE));
							session.setAttribute(SessionConstants.CONSOLE_LIST, consoleList);
							if (consoleList.size() > 0) {
								session.setAttribute(SessionConstants.CONSOLE_CODE, consoleList.get(0).getId());
							} else {
								session.setAttribute(SessionConstants.CONSOLE_CODE, RegularConstants.EMPTY_STRING);
							}
							if (session.getAttribute(SessionConstants.PASSWORD_RESET).toString().trim().equals(RegularConstants.COLUMN_ENABLE)) {
								forwardName = GenericAction.CHANGE_ACTION;
							} /*else if (isDashboardReq()) {
								if (isCashier(resultDTO.get(SessionConstants.ROLE_CODE))) {
									forwardName = GenericAction.CASHIER_ROLE;
								}else{
									forwardName = GenericAction.ACC_STATISTICS;
								}
							}*/
							else{
								forwardName = GenericAction.SUCCESS_ACTION;
							}
						}
					}
				}
			}
		}
		return mapping.findForward(forwardName);
	}
	public boolean isCashier(String roleCode) {
		DTObject resultDTO = new DTObject();
		AccessValidator validator = new AccessValidator();
		try {
			resultDTO.set(ContentManager.CODE, roleCode);
			resultDTO.set(ContentManager.FETCH_COLUMNS, "CASHIER_ROLE,HEAD_CASHIER_ROLE");
			resultDTO.set(ContentManager.USER_ACTION, "U");
			resultDTO = validator.validateInternalRole(resultDTO);
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
				if (resultDTO.get("CASHIER_ROLE").equals(RegularConstants.COLUMN_ENABLE) || resultDTO.get("HEAD_CASHIER_ROLE").equals(RegularConstants.COLUMN_ENABLE))
					return true;
				else
					return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			validator.close();
		}
		return false;
	}
	public boolean isDashboardReq() {
		DTObject resultDTO = new DTObject();
		AccessValidator validator = new AccessValidator();
		try {
			resultDTO = validator.getINSTALL(resultDTO);
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
				if (RegularConstants.COLUMN_ENABLE.equals(resultDTO.get("DASHBOARD_REQ")))
					return true;
				else
					return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			validator.close();
		}
		return false;
	}
}