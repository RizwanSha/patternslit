package patterns.config.web.actions.common;

import javax.servlet.ServletContext;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import patterns.config.delegate.BackOfficeProcessManager;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.loggers.ApplicationLogger;
import patterns.config.framework.monitor.ContextReference;
import patterns.config.framework.pki.PKIErrorCodes;
import patterns.config.framework.process.TBAProcessResult;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.thread.ApplicationContext;
import patterns.config.framework.thread.WebContext;
import patterns.config.framework.web.PasswordUtils;
import patterns.config.framework.web.RequestConstants;
import patterns.config.framework.web.SessionConstants;
import patterns.config.web.actions.GenericAction;
import patterns.config.web.forms.GenericFormBean;

public class eloginaction extends Action {
	protected ApplicationLogger logger = null;

	public eloginaction() {
		logger = ApplicationLogger.getInstance(this.getClass().getSimpleName());
		logger.logInfo("#()");
	}

	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		String forwardName = null;
		if (!(form instanceof GenericFormBean)) {
			forwardName = GenericAction.UNAUTH_ACTION;
		} else {
			GenericFormBean formBean = (GenericFormBean) form;
			if (formBean.getCommand().equals(GenericFormBean.RESET_ACTION)) {
				forwardName = GenericAction.RESET_ACTION;
			} else {
				ApplicationContext context = ApplicationContext.getInstance();
				WebContext webContext = WebContext.getInstance();
				HttpSession session = webContext.getSession();
				if (session.getAttribute(SessionConstants.TEMP_SESSION_START) == null) {
					session.setAttribute(RequestConstants.ADDITIONAL_INFO, formBean.getCommonResource("BLI999", null));
					forwardName = GenericAction.FAILURE_ACTION;
				} else {
					BackOfficeProcessManager processManager = new BackOfficeProcessManager();
					DTObject inputDTO = formBean.getFormDTO();
					inputDTO.set(RegularConstants.PROCESSBO_CLASS, formBean.getProcessBO());
					TBAProcessResult processResult = processManager.delegate(inputDTO);
					if (processResult.getProcessStatus().equals(TBAProcessStatus.FAILURE)) {
						session.setAttribute(RequestConstants.ADDITIONAL_INFO, formBean.getCommonResource(processResult.getErrorCode(), null));
						forwardName = GenericAction.FAILURE_ACTION;
					} else {
						if (processResult.getErrorCode() != null && !processResult.getErrorCode().isEmpty()) {
							session.setAttribute(RequestConstants.ADDITIONAL_INFO, formBean.getCommonResource(processResult.getErrorCode(), null));
							forwardName = GenericAction.FAILURE_ACTION;
						} else {
							
							DTObject resultDTO = (DTObject) processResult.getAdditionalInfo();
							// added by swaroopa as on 04-06-2012 begins
							if (resultDTO.get(SessionConstants.TFA_REQ).equals(RegularConstants.COLUMN_ENABLE)) {
								DTObject input = new DTObject();
								input.set("ENTITY_CODE", resultDTO.get(SessionConstants.ENTITY_CODE));
								input.set("USER_ID", resultDTO.get(SessionConstants.USER_ID));
								input.set("CUSTOMER_CODE", resultDTO.get(SessionConstants.CUSTOMER_CODE));
								input.set("CERT_INV_NUM", resultDTO.get(SessionConstants.CERT_INV_NUM));
								boolean isCRLRevoked = checkUserCRL(input);
								if (isCRLRevoked) {
									forwardName = GenericFormBean.UNAUTH_ACTION;
									WebContext.getInstance().getRequest().getSession().setAttribute(RequestConstants.ADDITIONAL_INFO, formBean.getCommonResource(PKIErrorCodes.ERR_RR_OTHER, null));
									formBean.getFormDTO().reset();
									return mapping.findForward(forwardName);
								}
							}
							// added by swaroopa as on 04-06-2012 ends
							// swaroopa as on 16-05-2012 begins
							ServletContext servletContext = ContextReference.getContext();
							String httpsMode = servletContext.getInitParameter(RegularConstants.INIT_HTTPS);
							boolean httpsEnabled = httpsMode != null && httpsMode.equals(RegularConstants.COLUMN_ENABLE);
							Cookie sessionCookie = new Cookie(RegularConstants.SESSIONID_COOKIE, session.getId());
							sessionCookie.setPath("/");
							if (httpsEnabled) {
								sessionCookie.setSecure(true);
							}
							response.addCookie(sessionCookie);
							String randomNonce = PasswordUtils.generateRandomNonce(resultDTO.get(SessionConstants.USER_ID), resultDTO.get(SessionConstants.CLIENT_IP));
							Cookie nonceCookie = new Cookie(RegularConstants.NONCE_COOKIE, randomNonce);
							nonceCookie.setPath("/");
							if (httpsEnabled) {
								nonceCookie.setSecure(true);
							}
							response.addCookie(nonceCookie);
							session.setAttribute(SessionConstants.TFA_NONCE, randomNonce);
							session.setAttribute(SessionConstants.CLIENT_IP, resultDTO.get(SessionConstants.CLIENT_IP));
							String deploymentContext = servletContext.getInitParameter(RegularConstants.INIT_ACCESS_TYPE);
							session.setAttribute(SessionConstants.DEPLOYMENT_CONTEXT, deploymentContext);
							// swaroopa as on 16-05-2012 ends

							session.setAttribute(SessionConstants.ORG_NAME, resultDTO.get(SessionConstants.ORG_NAME));

							session.setAttribute(SessionConstants.ENTITY_CODE, resultDTO.get(SessionConstants.ENTITY_CODE));
							session.setAttribute(SessionConstants.USER_ID, resultDTO.get(SessionConstants.USER_ID));
							session.setAttribute(SessionConstants.USER_AGENT, resultDTO.get(SessionConstants.USER_AGENT));
							session.setAttribute(SessionConstants.PASSWORD_SALT, resultDTO.get(SessionConstants.PASSWORD_SALT));
							session.setAttribute(SessionConstants.LOCALE, context.getLocale());
							session.setAttribute(SessionConstants.CUSTOMER_CODE, resultDTO.get(SessionConstants.CUSTOMER_CODE));
							session.setAttribute(SessionConstants.TFA_REQ, resultDTO.get(SessionConstants.TFA_REQ));
							session.setAttribute(SessionConstants.LOGIN_TFA_REQ, resultDTO.get(SessionConstants.LOGIN_TFA_REQ));
							session.setAttribute(SessionConstants.CERT_INV_NUM, resultDTO.get(SessionConstants.CERT_INV_NUM));
							session.setAttribute(SessionConstants.MULTI_SESSION_CHECK, resultDTO.get(SessionConstants.MULTI_SESSION_CHECK));
							session.setAttribute(SessionConstants.LOGIN_AUTHENTICATION_CONTEXT, RegularConstants.COLUMN_ENABLE);

							forwardName = GenericAction.SUCCESS_ACTION;
						}
					}
				}
			}
		}
		return mapping.findForward(forwardName);
	}

	// added by swaroopa as on 04-06-2012 begins
	private boolean checkUserCRL(DTObject input) {
		boolean isUserCRLRevoked = false;
		// PKIValidator validator = new PKIValidator();
		// try {
		// String entityCode = input.get("ENTITY_CODE");
		// String certificateInventoryNumber = input.get("CERT_INV_NUM");
		// DTObject result = validator.readUserCRL(input);
		// if (result.get(ContentManager.ERROR) != null) {
		// isUserCRLRevoked = true;
		// return isUserCRLRevoked;
		// }
		// if
		// (result.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE))
		// {
		// isUserCRLRevoked = true;
		// return isUserCRLRevoked;
		// }
		// X509CRL caCRL = (X509CRL) result.getObject("CA_CRL_CERTIFICATE");
		// X509CRL customerCRL = (X509CRL)
		// result.getObject("CUSTOMER_CRL_CERTIFICATE");
		// isUserCRLRevoked = validator.isCertificateRevoked(entityCode,
		// certificateInventoryNumber, caCRL, customerCRL);
		// } catch (Exception e) {
		// e.printStackTrace();
		// } finally {
		// validator.close();
		// }
		return isUserCRLRevoked;
	}
	// added by swaroopa as on 04-06-2012 ends
}