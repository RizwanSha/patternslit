package patterns.config.web.forms.auth;

import java.sql.Date;
import java.sql.ResultSet;

import patterns.config.delegate.BackOfficeProcessManager;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.pki.ProcessTFAInfo;
import patterns.config.framework.process.TBAProcessResult;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.thread.WebContext;
import patterns.config.framework.web.BackOfficeDateUtils;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.framework.web.DHTMLXGridUtility;
import patterns.config.framework.web.RequestConstants;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.AccessValidator;
import patterns.config.validations.AuthorizationValidator;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.EodSodValidator;
import patterns.config.web.forms.GenericFormBean;

public class acmnauthbean extends GenericFormBean {
	private static final long serialVersionUID = -6219657363150466160L;

	public acmnauthbean() {
		setProcessBO("patterns.config.framework.bo.auth.acmnauthBO");
	}

	private String optionID;
	private String fromDate;
	private String toDate;
	private String branchCode;
	private String userID;
	private String gridbox_rowid;

	public String getGridbox_rowid() {
		return gridbox_rowid;
	}

	public void setGridbox_rowid(String gridbox_rowid) {
		this.gridbox_rowid = gridbox_rowid;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getOptionID() {
		return optionID;
	}

	public void setOptionID(String optionID) {
		this.optionID = optionID;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public String getUserID() {
		return userID;
	}

	public void validate() {
	}

	public void reset() {
		optionID = RegularConstants.EMPTY_STRING;
		fromDate = RegularConstants.EMPTY_STRING;
		toDate = RegularConstants.EMPTY_STRING;
		branchCode = RegularConstants.EMPTY_STRING;
		userID = RegularConstants.EMPTY_STRING;
		gridbox_rowid = RegularConstants.EMPTY_STRING;
	}

	public DTObject validateOptionID(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		DTObject formDTO = getFormDTO();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		AccessValidator validation = new AccessValidator();
		try {
			String programID = inputDTO.get("MPGM_ID");
			if (!validation.isEmpty(programID)) {

				formDTO.set("MPGM_ID", programID);
				formDTO = validation.validateProgramOption(formDTO);
				if (formDTO.get(ContentManager.ERROR) != null) {
					resultDTO.set(ContentManager.ERROR, formDTO.get(ContentManager.ERROR));
					return resultDTO;
				}
				if (!formDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.OPTION_NOT_ALLOTED);
					return resultDTO;
				}
			}
			resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public DTObject validateUserID(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		DTObject formDTO = getFormDTO();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		AuthorizationValidator validation = new AuthorizationValidator();
		try {
			String userID = inputDTO.get("USER_ID");
			if (validation.isEmpty(userID)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			formDTO.set(ContentManager.USER_ID, userID);
			formDTO = validation.validateAuthorizationUserID(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, formDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (!formDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_USERID);
				return resultDTO;
			}
			resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public DTObject processAction(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		DTObject formDTO = getFormDTO();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		try {
			formDTO.set("MPGM_ID", inputDTO.get("TBAQ_PGM_ID"));
			formDTO = validateOptionID(formDTO);
			if (!formDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
			if (formDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, formDTO.get(ContentManager.ERROR));
				return resultDTO;
			}

			// added by swaroopa as on 16-04-2012 begins
			initializeTFA(inputDTO.get("PROGRAM_ID"));
			setTfaValue(inputDTO.get("tfaValue"));
			setTfaEncodedValue(inputDTO.get("tfaEncodedValue"));
			boolean tfaValid = isTFAValid(inputDTO.get("PROGRAM_ID"));
			if (tfaValid) {
				setTfaSuccess(RegularConstants.COLUMN_ENABLE);
			} else {
				setTfaSuccess(RegularConstants.COLUMN_DISABLE);
			}
			if (tfaValid) {

				// Aribala added for PICKUP BEG
				/*
				 * CommonValidator validator = new CommonValidator(); try {
				 * DTObject result = new DTObject(); result.set("PROGRAM_ID",
				 * inputDTO.get("TBAQ_PGM_ID")); result.set("PICKUP_PGM",
				 * inputDTO.get("PROGRAM_ID")); result.set("SOURCE_TABLE", "");
				 * result.set("SOURCE_KEY", inputDTO.get("TBAQ_MAIN_PK")); if
				 * (!validator.getPickupAccess(result)) {
				 * resultDTO.set(ContentManager.RESULT,
				 * ContentManager.DATA_UNAVAILABLE);
				 * resultDTO.set(ContentManager.ERROR,
				 * BackOfficeErrorCodes.PICKUP_DENIED); return resultDTO; } }
				 * catch (Exception e) { e.printStackTrace();
				 * resultDTO.set(ContentManager.ERROR,
				 * BackOfficeErrorCodes.UNSPECIFIED_ERROR); } finally {
				 * validator.close(); }
				 */
				// Aribala added for PICKUP END

				ProcessTFAInfo processTFAInfo = new ProcessTFAInfo();
				processTFAInfo.setUserId(context.getUserID());
				processTFAInfo.setDeploymentContext(context.getDeploymentContext());
				processTFAInfo.setDigitalCertificateInventoryNumber(context.getDigitalCertificateInventoryNumber());
				processTFAInfo.setTfaRequired(getTfaRequired());
				processTFAInfo.setTfaSuccess(getTfaSuccess());
				processTFAInfo.setTfaValue(getTfaValue());
				processTFAInfo.setTfaEncodedValue(getTfaEncodedValue());
				// added by swaroopa as on 16-04-2012 ends
				BackOfficeProcessManager processManager = new BackOfficeProcessManager();
				inputDTO.set("ENTITY_CODE", context.getEntityCode());
				inputDTO.set("PARTITION_NO", context.getPartitionNo());
				inputDTO.set("USER_ID", context.getUserID());
				inputDTO.set("CUST_ID", context.getCustomerCode());// added by
																	// swaroopa
																	// as on
																	// 26-04-2012
				inputDTO.set("IP_ADDRESS", context.getClientIP());
				inputDTO.setObject("CBD", context.getCurrentBusinessDate());
				inputDTO.setObject("PKI", processTFAInfo);// added by swaroopa
															// as on 26-04-2012
				inputDTO.set(RegularConstants.PROCESSBO_CLASS, getProcessBO());
				TBAProcessResult processResult = processManager.delegate(inputDTO);
				if (processResult.getProcessStatus().equals(TBAProcessStatus.SUCCESS)) {
					resultDTO.set("auth_status", processResult.getAdditionalInfo().toString());
					resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
					if (processResult.getResponseDTO() != null) {
						if (processResult.getResponseDTO().get(RegularConstants.ERROR) != null) {
							resultDTO.set(RequestConstants.ADDITIONAL_DETAILS, processResult.getResponseDTO().get(RegularConstants.ERROR));
						} else if (processResult.getResponseDTO().get(RegularConstants.ADDITIONAL_INFO) != null) {
							resultDTO.set(RequestConstants.ADDITIONAL_DETAILS, processResult.getResponseDTO().get(RegularConstants.ADDITIONAL_INFO));
						}
					}
				} else {
					resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
				}
				resultDTO.set("TFA_ERROR", RegularConstants.COLUMN_DISABLE);
			} else {
				WebContext.getInstance().getSession().setAttribute(RequestConstants.ADDITIONAL_INFO, getCommonResource(BackOfficeErrorCodes.INVALID_TFA, null));
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				resultDTO.set("TFA_ERROR_MSG", getCommonResource(BackOfficeErrorCodes.INVALID_TFA, null));
				resultDTO.set("TFA_ERROR", RegularConstants.COLUMN_ENABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return resultDTO;
	}

	public DTObject validateAuthorize(DTObject inputDTO) {
		inputDTO.set("AUTH_REJ_OPTION", "A");
		DTObject resultDTO = new DTObject();
		boolean isAccessAllowed = checkProgramAccess(inputDTO);
		if (isAccessAllowed) {
			resultDTO = processAction(inputDTO);
		} else {
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.AUTHORIZE_NOT_ALLOWED);
		}
		return resultDTO;
	}

	public DTObject validateReject(DTObject inputDTO) {
		inputDTO.set("AUTH_REJ_OPTION", "R");
		DTObject resultDTO = new DTObject();
		boolean isAccessAllowed = checkProgramAccess(inputDTO);
		if (isAccessAllowed) {
			resultDTO = processAction(inputDTO);
		} else {
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.REJECTION_NOT_ALLOWED);
		}
		return resultDTO;
	}

	private boolean checkProgramAccess(DTObject inputDTO) {
		boolean flag = false;
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		try {
			util.setMode(DBUtil.PREPARED);
			util.setSql("SELECT FN_ISPGMACCESSALLOWED(?,?,?,?) FROM DUAL");
			util.setString(1, context.getEntityCode());
			util.setString(2, context.getRoleCode());
			util.setString(3, inputDTO.get("TBAQ_PGM_ID"));
			util.setString(4, inputDTO.get("AUTH_REJ_OPTION"));
			ResultSet rs = util.executeQuery();
			if (rs.next()) {
				String action = rs.getString(1);
				if (action.equals("1")) {
					flag = true;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			flag = false;
		} finally {
			util.reset();
			dbContext.close();
		}
		return flag;
	}

	private boolean readMainCont() {
		EodSodValidator validator = new EodSodValidator();
		try {
			DTObject formDTO = validator.readMainContDetails();
			if (formDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
				if (formDTO.get("SOD_COMPL").equals(RegularConstants.COLUMN_ENABLE) && formDTO.get("SOD_IN_PROGRESS").equals(RegularConstants.COLUMN_DISABLE)) {
					return true;
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			validator.close();
		}
		return false;
	}

	public DTObject loadPendingRecords(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		

		DBContext dbContext = new DBContext();
		AccessValidator validation = new AccessValidator(dbContext);
		try {
			if (readMainCont()) {
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
				String programID = inputDTO.get("MPGM_ID");
				String userID = inputDTO.get("USER_ID");
				String fromDateString = inputDTO.get("FROM_DATE");
				String uptoDateString = inputDTO.get("TO_DATE");

				if (!validation.isEmpty(programID)) {
					DTObject dtobj = new DTObject();
					dtobj.reset();
					dtobj.set("MPGM_ID", programID);
					resultDTO = validateOptionID(dtobj);
				}
				if (!resultDTO.containsKey(ContentManager.ERROR)) {
					if (!validation.isEmpty(userID)) {
						DTObject dtobj = new DTObject();
						dtobj.reset();
						dtobj.set("USER_ID", userID);
						resultDTO = validateUserID(dtobj);
					}

					if (!validation.isEmpty(fromDateString)) {
						if (!validateFromDate(fromDateString)) {
							resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_DATE);
						}
					}
					if (!validation.isEmpty(fromDateString) && !validation.isEmpty(uptoDateString)) {
						if (!validateToDate(fromDateString, uptoDateString)) {
							resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_DATE);
						}
					}
				}
				if (!resultDTO.containsKey(ContentManager.ERROR)) {

					DBUtil util = dbContext.createUtilInstance();

					util.setMode(DBUtil.CALLABLE);
					util.setSql("{CALL SP_GETACMNAUTH_RESULTS(?,?,?,?,?,?,?,?,?)}");
					util.setString(1, context.getPartitionNo());
					util.setString(2, context.getEntityCode());
					util.setString(3, context.getUserID());
					util.setString(4, context.getRoleCode());
					util.setString(5, context.getRoleType());
					util.setString(6, programID);
					util.setString(7, userID);

					if (!fromDateString.equals("") && !uptoDateString.equals("")) {
						Date fromDate = new java.sql.Date(BackOfficeFormatUtils.getDate(fromDateString, context.getDateFormat()).getTime());
						Date uptoDate = new java.sql.Date(BackOfficeFormatUtils.getDate(uptoDateString, context.getDateFormat()).getTime());
						util.setDate(8, fromDate);
						util.setDate(9, uptoDate);

					} else {

						util.setDate(8, null);
						util.setDate(9, null);

					}

					util.execute();
					util.reset();
					util.setMode(DBUtil.PREPARED);
					util.setSql("select QUERY_PATH,TBAQ_PGM_ID,MPGM_DESCN,TBAQ_MAIN_PK,TBAQ_OPERN_FLG,TBAQ_DONE_BY,TO_CHAR(TBAQ_DONE_ON,'%d/%m/%Y %H:%i:%s') TBAQ_DONE_ON ,TO_CHAR(TBAQ_ENTRY_DATE,'%d-%m-%Y')TBAQ_ENTRY_DATE,TBAQ_ENTRY_SL,DUMMY_1,DUMMY_2,AUTH_ALLOWED,REJECT_ALLOWED FROM TMPACMNAUTHDETAILS ORDER BY TBAQ_ENTRY_DATE");
					ResultSet rset = util.executeQuery();
					int columnCount = rset.getMetaData().getColumnCount();
					int rows = 0;
					DHTMLXGridUtility utility = new DHTMLXGridUtility();
					utility.init();
					resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
					while (rset.next()) {
						rows++;
						utility.startRow();
						utility.setCell(RegularConstants.EMPTY_STRING);
						for (int j = 1; j <= columnCount; ++j) {
							utility.setCell(rset.getString(j));
						}
						utility.endRow();
					}
					util.reset();
					utility.finish();
					resultDTO.set(ContentManager.RESULT_XML, utility.getXML());
					if (rows > 0) {
						resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
					}

				}
			}else{
				resultDTO.set("SOD_COMPL","0" );
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			dbContext.close();
		}
		return resultDTO;
	}

	private boolean validateFromDate(String fromDate) {
		CommonValidator validation = new CommonValidator();
		try {
			if (!validation.isEmpty(fromDate)) {
				java.util.Date dateInstance = validation.isValidDate(fromDate);
				if (dateInstance != null) {
					// DEFECT ID - 20120306-13516 begin
					if (validation.isDateLesserEqualCBD(fromDate)) {
						return true;
					}
					// DEFECT ID - 20120306-13516 end
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			validation.close();
		}
		return false;
	}

	private boolean validateToDate(String fromDate, String toDate) {
		CommonValidator validation = new CommonValidator();
		try {
			if (!validation.isEmpty(toDate) && !validation.isEmpty(fromDate)) {
				java.util.Date dateInstance = validation.isValidDate(toDate);
				if (dateInstance != null) {
					// DEFECT ID - 20120306-13516 begin
					if (validation.isDateLesserEqualCBD(toDate)) {
						if (BackOfficeDateUtils.isDateLesserThanEqual(fromDate, toDate)) {
							return true;
						}
					}
					// DEFECT ID - 20120306-13516 end
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			validation.close();
		}
		return false;
	}
}