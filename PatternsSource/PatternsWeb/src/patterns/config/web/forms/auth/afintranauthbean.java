package patterns.config.web.forms.auth;

/*
 *The program used for Financial Authorization Program 

 Author : AJAY E
 Created Date : 08-AUG-2016
 Spec Reference :AFINTRANAUTH
 Modification History
 ----------------------------------------------------------------------
 Sl.No		Modified Date	Author			Modified Changes	Version
 ----------------------------------------------------------------------

 */
import java.sql.ResultSet;

import javax.swing.text.AbstractDocument.Content;

import patterns.config.delegate.BackOfficeProcessManager;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.pki.ProcessTFAInfo;
import patterns.config.framework.process.TBAProcessResult;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.thread.WebContext;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.framework.web.DHTMLXGridUtility;
import patterns.config.framework.web.RequestConstants;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.AccessValidator;
import patterns.config.validations.AuthorizationValidator;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.EodSodValidator;
import patterns.config.web.forms.GenericFormBean;

public class afintranauthbean extends GenericFormBean {
	private static final long serialVersionUID = -6219657363150466160L;

	public afintranauthbean() {
		setProcessBO("patterns.config.framework.bo.auth.afintranauthBO");
	}

	private String officeCode;
	private String batchDate;
	private String programId;
	private String fromBatchNo;
	private String toBatchNo;
	private String userID;
	private String notes;
	private String gridbox_rowid;

	public String getGridbox_rowid() {
		return gridbox_rowid;
	}

	public void setGridbox_rowid(String gridbox_rowid) {
		this.gridbox_rowid = gridbox_rowid;
	}

	public String getOfficeCode() {
		return officeCode;
	}

	public void setOfficeCode(String officeCode) {
		this.officeCode = officeCode;
	}

	public String getBatchDate() {
		return batchDate;
	}

	public void setBatchDate(String batchDate) {
		this.batchDate = batchDate;
	}

	public String getProgramId() {
		return programId;
	}

	public void setProgramId(String programId) {
		this.programId = programId;
	}

	public String getFromBatchNo() {
		return fromBatchNo;
	}

	public void setFromBatchNo(String fromBatchNo) {
		this.fromBatchNo = fromBatchNo;
	}

	public String getToBatchNo() {
		return toBatchNo;
	}

	public void setToBatchNo(String toBatchNo) {
		this.toBatchNo = toBatchNo;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public String getUserID() {
		return userID;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public void validate() {
	}

	public void reset() {
		officeCode = context.getBranchCode();
		batchDate = RegularConstants.EMPTY_STRING;
		programId = RegularConstants.EMPTY_STRING;
		fromBatchNo = RegularConstants.EMPTY_STRING;
		toBatchNo = RegularConstants.EMPTY_STRING;
		userID = RegularConstants.EMPTY_STRING;
		gridbox_rowid = RegularConstants.EMPTY_STRING;
	}

	public DTObject validateOfficeCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		AccessValidator validation = new AccessValidator();
		try {
			String branchCode = inputDTO.get("BRANCH_CODE");
			if (validation.isEmpty(branchCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
			resultDTO = validation.validateEntityBranch(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (!resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
			resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public DTObject validateProgramId(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		AccessValidator validation = new AccessValidator();
		try {
			String programID = inputDTO.get("MPGM_ID");
			if (!validation.isEmpty(programID)) {
				resultDTO.set("MPGM_ID", programID);
				inputDTO.set(ContentManager.FETCH, ContentManager.FETCH_ALL);
				resultDTO = validation.validateProgramOption(inputDTO);
				if (resultDTO.get(ContentManager.ERROR) != null) {
					resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
					return resultDTO;
				}
				if (!resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.OPTION_NOT_ALLOTED);
					return resultDTO;
				}
			}
			resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public DTObject validateUserID(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		DTObject formDTO = getFormDTO();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		AuthorizationValidator validation = new AuthorizationValidator();
		try {
			String userID = inputDTO.get("USER_ID");
			if (validation.isEmpty(userID)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			formDTO.set(ContentManager.USER_ID, userID);
			inputDTO.set(ContentManager.FETCH_COLUMNS, "MPGM_DESCN");
			formDTO = validation.validateAuthorizationUserID(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, formDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (!formDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_USERID);
				return resultDTO;
			}
			resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	private boolean validateBatchDate(String BatchDate) {
		CommonValidator validation = new CommonValidator();
		try {
			java.util.Date dateInstance = validation.isValidDate(BatchDate);
			if (dateInstance != null) {
				if (validation.isDateLesserEqualCBD(BatchDate)) {
					return true;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			validation.close();
		}
		return false;
	}

	public DTObject validateFromBatchNo(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		AuthorizationValidator validation = new AuthorizationValidator();
		try {
			String batchNo = inputDTO.get("FROM_BATCH_NO");
			if (!validation.isValidNumber(batchNo)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_NUMBER);
				return resultDTO;
			}
			if (!validation.isValidLength(batchNo, validation.LENGTH_1, validation.LENGTH_10)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_NUMBER_LENGTH);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public DTObject validateToBatchNo(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		AuthorizationValidator validation = new AuthorizationValidator();
		try {
			String fromBatchNo = inputDTO.get("FROM_BATCH_NO");
			String ToBatchNo = inputDTO.get("FROM_BATCH_NO");
			if (!validation.isEmpty(fromBatchNo)) {
				if (!validation.isValidNumber(ToBatchNo)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_NUMBER);
					return resultDTO;
				}
				if (!validation.isValidLength(ToBatchNo, validation.LENGTH_1, validation.LENGTH_10)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_NUMBER_LENGTH);
					return resultDTO;
				}
				if (!validation.isNumberGreaterThanEqual(fromBatchNo, ToBatchNo)) {
					//resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.PPBS_FROMBATCH_LESSTHAN_TOBATCH);
					return resultDTO;
				}
			} else {
				if (!validation.isEmpty(ToBatchNo)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_NUMBER);
					return resultDTO;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public DTObject processAction(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		DTObject formDTO = getFormDTO();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		try {
			formDTO.set("MPGM_ID", inputDTO.get("SOURCE_PGM"));
			formDTO = validateProgramId(formDTO);
			if (!formDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
			if (formDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, formDTO.get(ContentManager.ERROR));
				return resultDTO;
			}

			// added by swaroopa as on 16-04-2012 begins
			initializeTFA(inputDTO.get("PROGRAM_ID"));
			setTfaValue(inputDTO.get("tfaValue"));
			setTfaEncodedValue(inputDTO.get("tfaEncodedValue"));
			boolean tfaValid = isTFAValid(inputDTO.get("PROGRAM_ID"));
			if (tfaValid) {
				setTfaSuccess(RegularConstants.COLUMN_ENABLE);
			} else {
				setTfaSuccess(RegularConstants.COLUMN_DISABLE);
			}
			if (tfaValid) {

				// Aribala added for PICKUP BEG
				/*
				 * CommonValidator validator = new CommonValidator(); try {
				 * DTObject result = new DTObject(); result.set("PROGRAM_ID",
				 * inputDTO.get("TBAQ_PGM_ID")); result.set("PICKUP_PGM",
				 * inputDTO.get("PROGRAM_ID")); result.set("SOURCE_TABLE", "");
				 * result.set("SOURCE_KEY", inputDTO.get("TBAQ_MAIN_PK")); if
				 * (!validator.getPickupAccess(result)) {
				 * resultDTO.set(ContentManager.RESULT,
				 * ContentManager.DATA_UNAVAILABLE);
				 * resultDTO.set(ContentManager.ERROR,
				 * BackOfficeErrorCodes.PICKUP_DENIED); return resultDTO; } }
				 * catch (Exception e) { e.printStackTrace();
				 * resultDTO.set(ContentManager.ERROR,
				 * BackOfficeErrorCodes.UNSPECIFIED_ERROR); } finally {
				 * validator.close(); }
				 */
				// Aribala added for PICKUP END

				ProcessTFAInfo processTFAInfo = new ProcessTFAInfo();
				processTFAInfo.setUserId(context.getUserID());
				processTFAInfo.setDeploymentContext(context.getDeploymentContext());
				processTFAInfo.setDigitalCertificateInventoryNumber(context.getDigitalCertificateInventoryNumber());
				processTFAInfo.setTfaRequired(getTfaRequired());
				processTFAInfo.setTfaSuccess(getTfaSuccess());
				processTFAInfo.setTfaValue(getTfaValue());
				processTFAInfo.setTfaEncodedValue(getTfaEncodedValue());
				// added by swaroopa as on 16-04-2012 ends
				BackOfficeProcessManager processManager = new BackOfficeProcessManager();
				inputDTO.set("ENTITY_CODE", context.getEntityCode());
				inputDTO.set("PARTITION_NO", context.getPartitionNo());
				inputDTO.set("USER_ID", context.getUserID());
				inputDTO.set("CUST_ID", context.getCustomerCode());// added by
																	// swaroopa
																	// as on
																	// 26-04-2012
				inputDTO.set("IP_ADDRESS", context.getClientIP());
				inputDTO.setObject("CBD", context.getCurrentBusinessDate());
				inputDTO.setObject("PKI", processTFAInfo);// added by swaroopa
															// as on 26-04-2012
				inputDTO.set(RegularConstants.PROCESSBO_CLASS, getProcessBO());
				TBAProcessResult processResult = processManager.delegate(inputDTO);
				if (processResult.getProcessStatus().equals(TBAProcessStatus.SUCCESS)) {
					resultDTO.set("auth_status", processResult.getAdditionalInfo().toString());
					resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
					if (processResult.getResponseDTO() != null) {
						if (processResult.getResponseDTO().get(RegularConstants.ERROR) != null) {
							resultDTO.set(RequestConstants.ADDITIONAL_DETAILS, processResult.getResponseDTO().get(RegularConstants.ERROR));
						} else if (processResult.getResponseDTO().get(RegularConstants.ADDITIONAL_INFO) != null) {
							resultDTO.set(RequestConstants.ADDITIONAL_DETAILS, processResult.getResponseDTO().get(RegularConstants.ADDITIONAL_INFO));
						}
					}
				} else {
					resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
				}
				resultDTO.set("TFA_ERROR", RegularConstants.COLUMN_DISABLE);
			} else {
				WebContext.getInstance().getSession().setAttribute(RequestConstants.ADDITIONAL_INFO, getCommonResource(BackOfficeErrorCodes.INVALID_TFA, null));
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				resultDTO.set("TFA_ERROR_MSG", getCommonResource(BackOfficeErrorCodes.INVALID_TFA, null));
				resultDTO.set("TFA_ERROR", RegularConstants.COLUMN_ENABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return resultDTO;
	}

	public DTObject validateAuthorize(DTObject inputDTO) {
		inputDTO.set("AUTH_REJ_OPTION", "A");
		DTObject resultDTO = new DTObject();
		boolean isAccessAllowed = checkProgramAccess(inputDTO);
		if (isAccessAllowed) {
			resultDTO = processAction(inputDTO);
		} else {
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.AUTHORIZE_NOT_ALLOWED);
		}
		return resultDTO;
	}

	public DTObject validateReject(DTObject inputDTO) {
		inputDTO.set("AUTH_REJ_OPTION", "R");
		DTObject resultDTO = new DTObject();
		boolean isAccessAllowed = checkProgramAccess(inputDTO);
		if (isAccessAllowed) {
			resultDTO = processAction(inputDTO);
		} else {
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.REJECTION_NOT_ALLOWED);
		}
		return resultDTO;
	}

	private boolean checkProgramAccess(DTObject inputDTO) {
		boolean flag = false;
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		try {
			util.setMode(DBUtil.PREPARED);
			util.setSql("SELECT FN_ISPGMACCESSALLOWED(?,?,?,?) FROM DUAL");
			util.setString(1, context.getEntityCode());
			util.setString(2, context.getRoleCode());
			util.setString(3, inputDTO.get("CTQ_SOURCE_PGM"));
			util.setString(4, inputDTO.get("AUTH_REJ_OPTION"));
			ResultSet rs = util.executeQuery();
			if (rs.next()) {
				String action = rs.getString(1);
				if (action.equals("1")) {
					flag = true;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			flag = false;
		} finally {
			util.reset();
			dbContext.close();
		}
		return flag;
	}

	private boolean readMainCont() {
		EodSodValidator validator = new EodSodValidator();
		try {
			DTObject formDTO = validator.readMainContDetails();
			if (formDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
				if (formDTO.get("SOD_COMPL").equals(RegularConstants.COLUMN_ENABLE) && formDTO.get("SOD_IN_PROGRESS").equals(RegularConstants.COLUMN_DISABLE)) {
					return true;
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			validator.close();
		}
		return false;
	}

	public DTObject loadPendingRecords(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		DBContext dbContext = new DBContext();
		AccessValidator validation = new AccessValidator(dbContext);
		try {
			if (readMainCont()) {
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
				String branchCode = inputDTO.get("BRANCH_CODE");
				String batchDate = inputDTO.get("BATCH_DATE");
				String programID = inputDTO.get("MPGM_ID");
				String fromBatch = inputDTO.get("FROM_BATCH_NO");
				String toBatch = inputDTO.get("TO_BATCH_NO");
				String userID = inputDTO.get("USER_ID");

				DTObject dtobj = new DTObject();
				dtobj.reset();
				dtobj.set("BRANCH_CODE", branchCode);
				dtobj.set(ContentManager.ACTION, USAGE);
				resultDTO = validateOfficeCode(dtobj);
				if (resultDTO.containsKey(ContentManager.ERROR)) {
					return resultDTO;
				}
				if (!validation.isEmpty(programID)) {
					dtobj.reset();
					dtobj.set("MPGM_ID", programID);
					resultDTO = validateProgramId(dtobj);
				}
				if (resultDTO.containsKey(ContentManager.ERROR)) {
					return resultDTO;
				}
				if (!validation.isEmpty(userID)) {
					dtobj.reset();
					dtobj.set("USER_ID", userID);
					resultDTO = validateUserID(dtobj);
				}
				if (resultDTO.containsKey(ContentManager.ERROR)) {
					return resultDTO;
				}
				if (!resultDTO.containsKey(ContentManager.ERROR)) {
					if (validation.isEmpty(batchDate)) {
						resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
					}
					if (!validateBatchDate(batchDate)) {
						resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_DATE);
					}
					if (!validation.isEmpty(fromBatch)) {
						dtobj.reset();
						dtobj.set("FROM_BATCH_NO", fromBatch);
						resultDTO = validateFromBatchNo(dtobj);
					}
					dtobj.reset();
					dtobj.set("FROM_BATCH_NO", fromBatch);
					dtobj.set("TO_BATCH_NO", toBatch);
					resultDTO = validateToBatchNo(dtobj);
				}
				if (resultDTO.containsKey(ContentManager.ERROR)) {
					return resultDTO;
				}

				DBUtil util = dbContext.createUtilInstance();
				util.setMode(DBUtil.CALLABLE);
				util.setSql("{CALL SP_GET_CTRAN_AUTHQ(?,?,?,?,?,?,?,?,?,?,?,?)}");
				util.setString(1, context.getPartitionNo());
				util.setString(2, context.getEntityCode());
				util.setString(3, context.getUserID());
				util.setString(4, context.getRoleCode());
				util.setString(5, context.getRoleType());
				util.setString(6, branchCode.isEmpty() ? null : branchCode);
				util.setString(7, context.getClusterCode());
				util.setDate(8, batchDate.isEmpty() ? null : new java.sql.Date(BackOfficeFormatUtils.getDate(batchDate, context.getDateFormat()).getTime()));
				util.setString(9, programID.isEmpty() ? null : programID);
				util.setString(10, fromBatch.isEmpty() ? null : fromBatch);
				util.setString(11, toBatch.isEmpty() ? null : toBatch);
				util.setString(12, userID.isEmpty() ? null : userID);
				util.execute();
				util.reset();
				util.setMode(DBUtil.PREPARED);
				util.setSql("SELECT QUERY_PATH,SRC_PGM_ID,MPGM_DESCN,SRC_PGM_KEY,TRAN_PK,OFFICE_CODE,TO_CHAR(BATCH_DATE,?),BATCH_NO,OPERN_FLG,DONE_BY,DONE_ON,DUMMY_1,DUMMY_2,AUTH_ALLOWED,REJECT_ALLOWED FROM TMPCTRANQDETAILS ORDER BY BATCH_DATE");
				util.setString(1, context.getDateFormat());
				ResultSet rset = util.executeQuery();
				int columnCount = rset.getMetaData().getColumnCount();
				int rows = 0;
				DHTMLXGridUtility utility = new DHTMLXGridUtility();
				utility.init();
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
				while (rset.next()) {
					rows++;
					utility.startRow();
					utility.setCell(RegularConstants.EMPTY_STRING);
					for (int j = 1; j <= columnCount; ++j) {
						utility.setCell(rset.getString(j));
					}
					utility.endRow();
				}
				util.reset();
				utility.finish();
				resultDTO.set(ContentManager.RESULT_XML, utility.getXML());
				if (rows > 0) {
					resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				}
			} else {
				resultDTO.set("SOD_COMPL", "0");
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			dbContext.close();
		}
		return resultDTO;
	}
}