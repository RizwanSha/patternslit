/*
 *
 Author : Raja E
 Created Date : 10-03-2017
 Spec Reference : EBILLFILESTATUPLD
 Modification History
 ----------------------------------------------------------------------
 Sl.No		Modified Date	Author			Modified Changes	Version
 ----------------------------------------------------------------------

 */

package patterns.config.web.forms.bil;

import java.io.FileInputStream;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Arrays;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.DHTMLXGridUtility;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.AccessValidator;
import patterns.config.validations.BaseDomainValidator;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.RegistrationValidator;
import patterns.config.web.forms.GenericFormBean;
import patterns.config.framework.bo.BackOfficeErrorCodes;

public class ebillfilestatupldbean extends GenericFormBean {

	private static final long serialVersionUID = 1L;

	private String uploadDate;
	private String uploadSerial;
	private String fileName;
	private String fileInventoryNumber;
	private String fileExtensionList;
	private String fileExt;
	private String referenceNo;
	private String filePath;
	private String remarks;
	private String redirectLandingPagePath;
	private String fileExtensionListError;
	private String tempSerial;

	public String getUploadDate() {
		return uploadDate;
	}

	public void setUploadDate(String uploadDate) {
		this.uploadDate = uploadDate;
	}

	public String getUploadSerial() {
		return uploadSerial;
	}

	public void setUploadSerial(String uploadSerial) {
		this.uploadSerial = uploadSerial;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileInventoryNumber() {
		return fileInventoryNumber;
	}

	public void setFileInventoryNumber(String fileInventoryNumber) {
		this.fileInventoryNumber = fileInventoryNumber;
	}

	public String getFileExtensionList() {
		return fileExtensionList;
	}

	public void setFileExtensionList(String fileExtensionList) {
		this.fileExtensionList = fileExtensionList;
	}

	public String getFileExt() {
		return fileExt;
	}

	public void setFileExt(String fileExt) {
		this.fileExt = fileExt;
	}

	public String getReferenceNo() {
		return referenceNo;
	}

	public void setReferenceNo(String referenceNo) {
		this.referenceNo = referenceNo;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getRedirectLandingPagePath() {
		return redirectLandingPagePath;
	}

	public void setRedirectLandingPagePath(String redirectLandingPagePath) {
		this.redirectLandingPagePath = redirectLandingPagePath;
	}

	public String getFileExtensionListError() {
		return fileExtensionListError;
	}

	public void setFileExtensionListError(String fileExtensionListError) {
		this.fileExtensionListError = fileExtensionListError;
	}

	public String getTempSerial() {
		return tempSerial;
	}

	public void setTempSerial(String tempSerial) {
		this.tempSerial = tempSerial;
	}

	public void reset() {
		uploadDate = RegularConstants.EMPTY_STRING;
		uploadSerial = RegularConstants.EMPTY_STRING;
		fileName = RegularConstants.EMPTY_STRING;
		fileInventoryNumber = RegularConstants.EMPTY_STRING;
		fileExtensionList = RegularConstants.EMPTY_STRING;
		fileExt = RegularConstants.EMPTY_STRING;
		referenceNo = RegularConstants.EMPTY_STRING;
		filePath = RegularConstants.EMPTY_STRING;
		remarks = RegularConstants.EMPTY_STRING;
		redirectLandingPagePath = RegularConstants.EMPTY_STRING;
		fileExtensionListError = RegularConstants.EMPTY_STRING;
		tempSerial = RegularConstants.EMPTY_STRING;
		DTObject resultDTO = new DTObject();
		validateFileFormatAllowed(resultDTO);
		setProcessBO("patterns.config.framework.bo.bil.ebillfilestatupldBO");
	}

	@Override
	public void validate() {
		// TODO Auto-generated method stub
		if (validateUploadDate() && validateUploadSerial()) {
			validateVerifyUpload();
			validateRemarks();
			ArrayList<String> aList = new ArrayList<String>(Arrays.asList(fileExtensionList.split("\\,")));
			if (!aList.contains(fileExt)) {
				getErrorMap().setError("upload", BackOfficeErrorCodes.INVALID_FILE_FORMAT);
			}
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.set("ENTRY_DATE", uploadDate);
				formDTO.set("ENTRY_SL", uploadSerial);
				formDTO.set("FILE_INV_NUM", fileInventoryNumber);
				formDTO.set("REMARKS", remarks);
				formDTO.set("UPLOADED_BY", context.getUserID());
				formDTO.set("TEMP_SL", tempSerial);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("upload", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
	}

	public void validateFileFormatAllowed(DTObject inputDTO) {
		AccessValidator validator = new AccessValidator();
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		try {
			inputDTO.set("PGM_ID", context.getProcessID());
			inputDTO.set("PURPOSE", "BIL");
			resultDTO = validator.readFilePurposeParameter(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				fileExtensionListError = getErrorResource(BackOfficeErrorCodes.UNSPECIFIED_ERROR, null);
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				fileExtensionListError = getErrorResource(BackOfficeErrorCodes.FILEPURPOSE_CFG_UNAVAILABLE, null);
			}
			fileExtensionList = resultDTO.get("ALLOWED_EXTENSIONS");
		} catch (Exception e) {
			e.printStackTrace();
			fileExtensionListError = getErrorResource(BackOfficeErrorCodes.UNSPECIFIED_ERROR, null);
		} finally {
			validator.close();
		}
	}

	public boolean validateUploadDate() {
		CommonValidator validator = new CommonValidator();
		try {
			if (getAction().equals(MODIFY)) {
				getErrorMap().setError("upload", BackOfficeErrorCodes.INVALID_ACTION);
				return false;
			}
			if (getRectify().equals(RegularConstants.COLUMN_ENABLE)) {
				java.util.Date dateInstance = validator.isValidDate(uploadDate);
				if (validator.isEmpty(uploadDate)) {
					getErrorMap().setError("upload", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (dateInstance == null) {
					getErrorMap().setError("upload", BackOfficeErrorCodes.INVALID_DATE);
					return false;
				}
				if (validator.isDateGreaterThanCBD(uploadDate)) {
					getErrorMap().setError("upload", BackOfficeErrorCodes.DATE_LECBD);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("upload", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return false;
	}

	public boolean validateUploadSerial() {
		try {
			if (getAction().equals(MODIFY)) {
				getErrorMap().setError("upload", BackOfficeErrorCodes.INVALID_ACTION);
				return false;
			}
			if (getRectify().equals(RegularConstants.COLUMN_ENABLE)) {
				DTObject formDTO = new DTObject();
				formDTO.set("ENTRY_DATE", uploadDate);
				formDTO.set("ENTRY_SL", uploadSerial);
				formDTO.set(ContentManager.ACTION, getAction());
				formDTO = validateUploadSerial(formDTO);
				if (formDTO.get(ContentManager.ERROR) != null) {
					getErrorMap().setError("upload", formDTO.get(ContentManager.ERROR));
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("upload", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateUploadSerial(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		RegistrationValidator validation = new RegistrationValidator();
		String uploadSerial = inputDTO.get("ENTRY_SL");
		try {
			if (validation.isEmpty(uploadSerial)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			if (!validation.isValidNumber(uploadSerial)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_NUMBER);
				return resultDTO;
			}
			inputDTO.set(ContentManager.TABLE, "BILLFILESTATUPLD");
			inputDTO.set(ContentManager.FETCH, ContentManager.FETCH_ALL);
			String columns[] = new String[] { context.getEntityCode(), inputDTO.get("ENTRY_DATE"), uploadSerial };
			inputDTO.setObject(ContentManager.PROGRAM_KEY, columns);
			resultDTO = validation.validatePK(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateVerifyUpload() {
		try {
			if (tempSerial.isEmpty()) {
				getErrorMap().setError("fileUploadError", BackOfficeErrorCodes.INVALID_ACTION);
				return false;
			}
			DTObject formDTO = new DTObject();
			formDTO.set("TEMP_SL", tempSerial);
			formDTO = validateVerifyUpload(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("fileUploadError", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("fileUploadError", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateVerifyUpload(DTObject inputDTO) {
		String sqlQuery = ContentManager.EMPTY_STRING;
		sqlQuery = "SELECT COUNT(1) FROM TMPBILLFILESTAT WHERE ENTITY_CODE=? AND TEMP_SL=?";
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		DTObject resultDTO = new DTObject();
		try {
			util.setMode(DBUtil.PREPARED);
			util.setSql(sqlQuery);
			util.setLong(1, Long.parseLong(context.getEntityCode()));
			util.setString(2, inputDTO.get("TEMP_SL"));
			ResultSet rs = util.executeQuery();
			if (rs.next()) {
				if (rs.getBoolean(1)) {
					resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				} else {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.VERIFY_FILE_UPLOAD);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			util.reset();
			dbContext.close();
		}
		return resultDTO;
	}

	public DTObject validateUploadXLSXFile(DTObject inputDTO) {
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		String sqlQuery = RegularConstants.EMPTY_STRING;
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		BaseDomainValidator validation = new BaseDomainValidator();
		String fileName = inputDTO.get("FILE_NAME");
		XSSFSheet sheet;
		try {
			resultDTO = validation.getSystemFolders(inputDTO);
			long serial = getTBAReferenceSerial(inputDTO);
			if (inputDTO.get("FILE_FORMAT").equals("xlsx")) {
				FileInputStream input = new FileInputStream(resultDTO.get("FILE_UPLOAD_PATH") + fileName);
				@SuppressWarnings("resource")
				XSSFWorkbook workbook = new XSSFWorkbook(input);
				int noOfSheets = workbook.getNumberOfSheets();
				int k = 1;
				for (int j = 0; j < noOfSheets; j++) {
					sheet = workbook.getSheetAt(j);
					XSSFRow row;
					for (int i = 1; i <= sheet.getLastRowNum(); i++) {
						row = sheet.getRow(i);
						util.reset();
						sqlQuery = "INSERT INTO TMPBILLFILESTAT(ENTITY_CODE,TEMP_SL,DTL_SL,REF_DOC_NO,ACCOUNT_NO,STATUS,OTH_SYS_REF_NUMBER)VALUES(?,?,?,?,?,?,?)";
						util.setSql(sqlQuery);
						util.setLong(1, Long.parseLong(context.getEntityCode()));
						util.setLong(2, serial);
						util.setInt(3, k++);
						util.setString(4, row.getCell(0).toString());
						if (row.getCell(1).getCellType() == XSSFCell.CELL_TYPE_STRING)
							util.setString(5, row.getCell(1).toString());
						else
							util.setInt(5, Integer.valueOf((int) Math.round(row.getCell(1).getNumericCellValue())));
						util.setString(6, row.getCell(2).toString());
						if (row.getCell(3).getCellType() == XSSFCell.CELL_TYPE_STRING)
							util.setString(7, row.getCell(3).toString());
						else
							util.setInt(7, Integer.valueOf((int) Math.round(row.getCell(3).getNumericCellValue())));
						util.executeUpdate();
					}
				}
				util.reset();
				util.setMode(DBUtil.CALLABLE);
				util.setSql("{CALL SP_EBILLFILE_VERIFY(?,?,?)}");
				util.setLong(1, Long.parseLong(context.getEntityCode()));
				util.setLong(2, serial);
				util.registerOutParameter(3, Types.VARCHAR);
				util.execute();
				if (!util.getString(3).equals("0")) {
					resultDTO.set("TEMP_SL", String.valueOf(serial));
					resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
					return resultDTO;
				}
				resultDTO.set("TEMP_SL", String.valueOf(serial));
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
			} else {
				inputDTO.set("SERIAL", String.valueOf(serial));
				inputDTO.set("FILE_UPLOAD_PATH", resultDTO.get("FILE_UPLOAD_PATH"));
				resultDTO = UploadXLSFile(inputDTO);
				if (resultDTO.get(ContentManager.ERROR) != null) {
					resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
					return resultDTO;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
			dbContext.close();
		}
		return resultDTO;
	}

	public DTObject UploadXLSFile(DTObject inputDTO) {
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		String sqlQuery = RegularConstants.EMPTY_STRING;
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		long serial = Long.parseLong(inputDTO.get("SERIAL"));
		HSSFSheet sheet;
		try {
			FileInputStream input = new FileInputStream(inputDTO.get("FILE_UPLOAD_PATH") + inputDTO.get("FILE_NAME"));
			HSSFWorkbook wb = new HSSFWorkbook(input);
			int noOfSheets = wb.getNumberOfSheets();
			int k = 1;
			for (int j = 0; j < noOfSheets; j++) {
				sheet = wb.getSheetAt(j);
				Row row;
				for (int i = 1; i <= sheet.getLastRowNum(); i++) {
					row = sheet.getRow(i);
					util.reset();
					sqlQuery = "INSERT INTO TMPBILLFILESTAT(ENTITY_CODE,TEMP_SL,DTL_SL,REF_DOC_NO,ACCOUNT_NO,STATUS,OTH_SYS_REF_NUMBER)VALUES(?,?,?,?,?,?,?)";
					util.setSql(sqlQuery);
					util.setLong(1, Long.parseLong(context.getEntityCode()));
					util.setLong(2, serial);
					util.setInt(3, k++);
					util.setString(4, row.getCell(0).toString());
					if (row.getCell(1).getCellType() == HSSFCell.CELL_TYPE_STRING)
						util.setString(5, row.getCell(1).toString());
					else
						util.setInt(5, Integer.valueOf((int) Math.round(row.getCell(1).getNumericCellValue())));
					util.setString(6, row.getCell(2).toString());
					if (row.getCell(3).getCellType() == HSSFCell.CELL_TYPE_STRING)
						util.setString(7, row.getCell(3).toString());
					else
						util.setInt(7, Integer.valueOf((int) Math.round(row.getCell(3).getNumericCellValue())));
					util.executeUpdate();
				}
			}
			util.reset();
			util.setMode(DBUtil.CALLABLE);
			util.setSql("{CALL SP_EBILLFILE_VERIFY(?,?,?)}");
			util.setLong(1, Long.parseLong(context.getEntityCode()));
			util.setLong(2, serial);
			util.registerOutParameter(3, Types.VARCHAR);
			util.execute();
			if (!util.getString(3).equals("0")) {
				resultDTO.set("TEMP_SL", String.valueOf(serial));
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
				return resultDTO;
			}
			resultDTO.set("TEMP_SL", String.valueOf(serial));
			resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			dbContext.close();
		}
		return resultDTO;
	}

	public long getTBAReferenceSerial(DTObject input) {
		String sourceKey = input.get("SOURCE_KEY");
		String programId = input.get("PGM_ID");
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		long serial = 0;

		try {
			util.reset();
			util.setMode(DBUtil.CALLABLE);
			String sql = "CALL FN_GET_TBA_REFSL(?,?,?)";
			util.setSql(sql);
			util.setString(1, programId);
			util.setString(2, sourceKey);
			util.registerOutParameter(3, Types.INTEGER);
			util.execute();
			serial = util.getInt(3);

		} catch (Exception e) {
			e.printStackTrace();

		} finally {
			util.reset();
			dbContext.close();
		}
		return serial;
	}

	public DTObject loadTempDetails(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		DHTMLXGridUtility gridUtility = new DHTMLXGridUtility();
		String sqlQuery = RegularConstants.EMPTY_STRING;
		boolean recordExist = false;
		try {
			util.reset();
			util.setMode(DBUtil.PREPARED);
			sqlQuery = "SELECT DTL_SL,REF_DOC_NO,ACCOUNT_NO,STATUS,OTH_SYS_REF_NUMBER,ROW_ERROR FROM TMPBILLFILESTAT WHERE ENTITY_CODE=? AND TEMP_SL=?";
			util.setSql(sqlQuery);
			util.setLong(1, Long.parseLong(context.getEntityCode()));
			util.setLong(2, Long.parseLong(inputDTO.get("TEMP_SL")));
			ResultSet rset = util.executeQuery();
			gridUtility.init();
			while (rset.next()) {
				gridUtility.startRow(rset.getString("DTL_SL"));
				gridUtility.setCell(rset.getString("DTL_SL"));
				gridUtility.setCell(rset.getString("REF_DOC_NO"));
				gridUtility.setCell(rset.getString("ACCOUNT_NO"));
				gridUtility.setCell(rset.getString("STATUS"));
				gridUtility.setCell(rset.getString("OTH_SYS_REF_NUMBER"));
				if (rset.getString("ROW_ERROR") != null && !rset.getString("ROW_ERROR").isEmpty())
					gridUtility.setCell("<p style=color:red;>" + rset.getString("ROW_ERROR") + "</p>");
				else
					gridUtility.setCell(rset.getString("ROW_ERROR"));
				gridUtility.endRow();
				recordExist = true;
			}
			if (!recordExist) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_NOT_EXISTS);
				return resultDTO;
			}
			gridUtility.finish();
			String resultXML = gridUtility.getXML();
			inputDTO.set(ContentManager.RESULT_XML, resultXML);
			inputDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);

		} catch (Exception e) {
			e.printStackTrace();
			inputDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			dbContext.close();
		}
		return inputDTO;
	}

	public boolean validateRemarks() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (remarks != null && remarks.length() > 0) {
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			if (getRectify().equals(RegularConstants.COLUMN_ENABLE)) {
				if (commonValidator.isEmpty(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}

}
