/*
 *
 Author : Raja E
 Created Date : 28-02-2017
 Spec Reference : RBILLFILEGEN
 Modification History
 ----------------------------------------------------------------------
 Sl.No		Modified Date	Author			Modified Changes	Version
 ----------------------------------------------------------------------

 */

package patterns.config.web.forms.bil;

import java.sql.Types;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.framework.web.reports.ReportUtils;
import patterns.config.validations.AccessValidator;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.MasterValidator;
import patterns.config.web.forms.GenericFormBean;

public class rbillfilegenbean extends GenericFormBean {

	private static final long serialVersionUID = 1L;
	private String invoiceDate;
	private String leaseProductCode;

	public String getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(String invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public String getLeaseProductCode() {
		return leaseProductCode;
	}

	public void setLeaseProductCode(String leaseProductCode) {
		this.leaseProductCode = leaseProductCode;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public void reset() {
		// TODO Auto-generated method stub
		invoiceDate = RegularConstants.EMPTY_STRING;
		leaseProductCode = RegularConstants.EMPTY_STRING;
	}

	@Override
	public void validate() {
		validateInvoiceDate();
		validateProductCode();
		try {

		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("invoiceDate", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
	}

	public DTObject validate(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		if (!validateInvoiceDate()) {
			resultDTO.set(ContentManager.ERROR, getErrorMap().getErrorKey("invoiceDate"));
			resultDTO.set(ContentManager.ERROR_FIELD, "invoiceDate");
			return resultDTO;
		}
		if (!validateProductCode()) {
			resultDTO.set(ContentManager.ERROR, getErrorMap().getErrorKey("leaseProductCode"));
			resultDTO.set(ContentManager.ERROR_FIELD, "leaseProductCode");
			return resultDTO;
		}
		return resultDTO;
	}

	private boolean validateInvoiceDate() {
		CommonValidator validator = new CommonValidator();
		try {
			if (validator.isEmpty(invoiceDate)) {
				getErrorMap().setError("invoiceDate", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (validator.isValidDate(invoiceDate) == null) {
				getErrorMap().setError("invoiceDate", BackOfficeErrorCodes.INVALID_DATE);
				return false;
			}
			if (validator.isDateGreaterThanCBD(invoiceDate)) {
				getErrorMap().setError("invoiceDate", BackOfficeErrorCodes.DATE_LECBD);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("invoiceDate", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return false;
	}

	public boolean validateProductCode() {
		try {
			DTObject formDTO = getFormDTO();
			formDTO.set("LEASE_PRODUCT_CODE", leaseProductCode);
			formDTO.set(ContentManager.USER_ACTION, USAGE);
			formDTO = validateLeaseProductCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("leaseProductCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("leaseProductCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateLeaseProductCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		MasterValidator validation = new MasterValidator();
		String productCode = inputDTO.get("LEASE_PRODUCT_CODE");
		try {
			if (validation.isEmpty(productCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
			resultDTO = validation.validateLeaseProductCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public DTObject doPrint(DTObject inputDTO) {
		inputDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		AccessValidator validation = new AccessValidator();
		String entityCode = inputDTO.get("ENTITY_CODE");
		String invoiceDate = inputDTO.get("INVOICE_DATE");
		String leaseProductCode = inputDTO.get("LEASE_PRODUCT_CODE");
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		try {
			setInvoiceDate(inputDTO.get("INVOICE_DATE"));
			setLeaseProductCode(inputDTO.get("LEASE_PRODUCT_CODE"));
			DTObject validateDTO = validate(inputDTO);
			if (validateDTO.get(ContentManager.ERROR) != null) {
				return validateDTO;
			}
			long serial = 0;
			String errorCode = null;
			dbContext.setAutoCommit(false);
			util.reset();
			util.setMode(DBUtil.CALLABLE);
			util.setSql("CALL SP_RBILLFILEGEN(?,?,?,?,?,?)");
			util.setLong(1, Long.parseLong(entityCode));
			util.setDate(2, new java.sql.Date(BackOfficeFormatUtils.getDate(invoiceDate, context.getDateFormat()).getTime()));
			util.setString(3, leaseProductCode);
			util.registerOutParameter(4, Types.BIGINT);
			util.registerOutParameter(5, Types.VARCHAR);
			util.registerOutParameter(6, Types.VARCHAR);
			util.execute();
			dbContext.commit();
			errorCode = String.valueOf(util.getString(5));
			if (!errorCode.equals("S")) {
				inputDTO.set("SP_ERROR", util.getString(6));
				return inputDTO;
			}
			serial = Long.parseLong(util.getString(4));
			inputDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
			inputDTO.set(ContentManager.REPORT_HANDLER, "patterns.config.web.reports.bil.rbillfilegenreport");
			inputDTO.set("ENTITY_CODE", inputDTO.get("ENTITY_CODE"));
			inputDTO.set("TEMP_SL", String.valueOf(serial));
			if (inputDTO.get("REPORT_FORMAT").equals("") || inputDTO.get("REPORT_FORMAT") == null)
				inputDTO.set(ContentManager.REPORT_FORMAT, ReportUtils.EXPORT_PDF + "");
			else
				inputDTO.set(ContentManager.REPORT_FORMAT, inputDTO.get("REPORT_FORMAT"));
			processDownload(inputDTO);
		} catch (Exception e) {
			e.printStackTrace();
			inputDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			dbContext.close();
			validation.close();
		}
		return inputDTO;
	}

}
