/*
 *
 Author : Raja E
 Created Date : 28-01-2017
 Spec Reference : MSTAFFROLE
 Modification History
 ----------------------------------------------------------------------
 Sl.No		Modified Date	Author			Modified Changes	Version
 ----------------------------------------------------------------------

 */

package patterns.config.web.forms.emp;

import java.sql.ResultSet;
import java.util.ArrayList;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.GenericOption;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.MasterValidator;
import patterns.config.web.forms.GenericFormBean;

public class mstaffrolebean extends GenericFormBean {

	private static final long serialVersionUID = 1L;

	private String staffRoleCode;
	private String description;
	private String conciseDescription;
	private String parentStaffRoleCode;
	private String roleCategory;
	private boolean enabled;
	private String remarks;
	private String roleType;
	private boolean roleInBranch;
	private boolean regionalRole;
	private boolean nationalRole;


	public boolean isRoleInBranch() {
		return roleInBranch;
	}

	public void setRoleInBranch(boolean roleInBranch) {
		this.roleInBranch = roleInBranch;
	}

	public boolean isRegionalRole() {
		return regionalRole;
	}

	public void setRegionalRole(boolean regionalRole) {
		this.regionalRole = regionalRole;
	}

	public boolean isNationalRole() {
		return nationalRole;
	}

	public void setNationalRole(boolean nationalRole) {
		this.nationalRole = nationalRole;
	}

	public String getStaffRoleCode() {
		return staffRoleCode;
	}

	public void setStaffRoleCode(String staffRoleCode) {
		this.staffRoleCode = staffRoleCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getConciseDescription() {
		return conciseDescription;
	}

	public void setConciseDescription(String conciseDescription) {
		this.conciseDescription = conciseDescription;
	}

	public String getParentStaffRoleCode() {
		return parentStaffRoleCode;
	}

	public void setParentStaffRoleCode(String parentStaffRoleCode) {
		this.parentStaffRoleCode = parentStaffRoleCode;
	}

	public String getRoleCategory() {
		return roleCategory;
	}

	public void setRoleCategory(String roleCategory) {
		this.roleCategory = roleCategory;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getRoleType() {
		return roleType;
	}

	public void setRoleType(String roleType) {
		this.roleType = roleType;
	}

	@Override
	public void reset() {
		// TODO Auto-generated method stub

		DBContext dbContext = new DBContext();
		ArrayList<GenericOption> category = null;
		category = getGenericOptions("COMMON", "ROLE_CATEGORY", dbContext, RegularConstants.NULL);
		webContext.getRequest().setAttribute("COMMON_ROLE_CATEGORY", category);
		dbContext.close();
		staffRoleCode = RegularConstants.EMPTY_STRING;
		description = RegularConstants.EMPTY_STRING;
		conciseDescription = RegularConstants.EMPTY_STRING;
		parentStaffRoleCode = RegularConstants.EMPTY_STRING;
		roleCategory = RegularConstants.EMPTY_STRING;
		roleInBranch =false;
		regionalRole =false;
		nationalRole =false;
		roleType = RegularConstants.EMPTY_STRING;
		enabled = false;
		remarks = RegularConstants.EMPTY_STRING;
		setProcessBO("patterns.config.framework.bo.emp.mstaffroleBO");
	}

	@Override
	public void validate() {
		// TODO Auto-generated method stub
		if (validateStaffRoleCode()) {
			validateDescription();
			validateConciseDescription();
			validateParentStaffRoleCode();
			validateRoleCategory();
			validateRoleDisabled();
			validateCheck();
			validateRemarks();
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.set("STAFF_ROLE_CODE", staffRoleCode);
				formDTO.set("DESCRIPTION", description);
				formDTO.set("CONCISE_DESCRIPTION", conciseDescription);
				formDTO.set("PARENT_STAFF_ROLE_CODE", parentStaffRoleCode);
				formDTO.set("BRANCH_ROLE", decodeBooleanToString(roleInBranch));
				formDTO.set("REGIONAL_ROLE", decodeBooleanToString(regionalRole));
				formDTO.set("NATIONAL_ROLE", decodeBooleanToString(nationalRole));
				if (parentStaffRoleCode.isEmpty())
					formDTO.set("ROLE_CATEGORY", roleCategory);
				else
					formDTO.set("ROLE_CATEGORY", "M");
				if (getAction().equals(ADD)) {
					formDTO.set("ENABLED", decodeBooleanToString(true));
				} else {
					formDTO.set("ENABLED", decodeBooleanToString(enabled));
				}
				formDTO.set("REMARKS", remarks);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("staffRoleCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
	}

	public boolean validateStaffRoleCode() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("STAFF_ROLE_CODE", staffRoleCode);
			formDTO.set(ContentManager.USER_ACTION, getAction());
			formDTO = validateStaffRoleCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("staffRoleCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("staffRoleCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateStaffRoleCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		MasterValidator validation = new MasterValidator();
		String staffRoleCode = inputDTO.get("STAFF_ROLE_CODE");
		String action = inputDTO.get(ContentManager.USER_ACTION);
		try {
			if (validation.isEmpty(staffRoleCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
			resultDTO = validation.ValidateStaffRoleCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (action.equals(ADD)) {
				if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_EXISTS);
					return resultDTO;
				}
			} else {
				if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_NOT_EXISTS);
					return resultDTO;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateDescription() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(description)) {
				getErrorMap().setError("description", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidDescription(description)) {
				getErrorMap().setError("description", BackOfficeErrorCodes.INVALID_DESCRIPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("description", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateConciseDescription() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(conciseDescription)) {
				getErrorMap().setError("conciseDescription", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidConciseDescription(conciseDescription)) {
				getErrorMap().setError("conciseDescription", BackOfficeErrorCodes.INVALID_CONCISE_DESCRIPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("conciseDescription", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateParentStaffRoleCode() {
		try {
			if (!parentStaffRoleCode.isEmpty()) {
				if (parentStaffRoleCode.equals(staffRoleCode)) {
					getErrorMap().setError("parentStaffRoleCode", BackOfficeErrorCodes.FAS_GLCODE_CANT_SAME);
					return false;
				}
				DTObject formDTO = new DTObject();
				formDTO.set("ROLE_CODE", staffRoleCode);
				formDTO.set("STAFF_ROLE_CODE", parentStaffRoleCode);
				formDTO.set(ContentManager.USER_ACTION, USAGE);
				formDTO = validateParentStaffRoleCode(formDTO);
				if (formDTO.get(ContentManager.ERROR) != null) {
					getErrorMap().setError("parentStaffRoleCode", formDTO.get(ContentManager.ERROR));
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("parentStaffRoleCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateParentStaffRoleCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		MasterValidator validation = new MasterValidator();
		String roleCode = inputDTO.get("ROLE_CODE");
		String parentStaffRole = inputDTO.get("STAFF_ROLE_CODE");
		try {
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION,PARENT_STAFF_ROLE_CODE,ROLE_CATEGORY");
			resultDTO = validation.ValidateStaffRoleCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
			if (roleCode.equals(resultDTO.get("PARENT_STAFF_ROLE_CODE"))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.CHILD_OF_PARENT_NA);
				return resultDTO;
			}
			inputDTO.set("STAFF_ROLE_CODE", roleCode);
			inputDTO.set("PARENT_STAFF_ROLE_CODE", parentStaffRole);
			String staffRole = validateStaffRole(inputDTO);
			if (staffRole.equals(RegularConstants.ONE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.ALREADY_EXIST_HIERARCHY);
				return resultDTO;
			}

		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public String validateStaffRole(DTObject inputDTO) {
		String sqlQuery = ContentManager.EMPTY_STRING;
		sqlQuery = "SELECT FN_CHK_STAFFROLE_HIER_OCCUR(?,?,?)";
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		String staffRoleCkeck = null;
		try {
			util.setMode(DBUtil.PREPARED);
			util.setSql(sqlQuery);
			util.setString(1, context.getEntityCode());
			util.setString(2, inputDTO.get("STAFF_ROLE_CODE"));
			util.setString(3, inputDTO.get("PARENT_STAFF_ROLE_CODE"));
			ResultSet rs = util.executeQuery();
			if (rs.next()) {
				staffRoleCkeck = rs.getString(1);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			util.reset();
			dbContext.close();
		}
		return staffRoleCkeck;
	}

	public boolean validateRoleCategory() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(parentStaffRoleCode)) {
				if (validation.isEmpty(roleCategory)) {
					getErrorMap().setError("roleCategory", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!validation.isCMLOVREC("COMMON", "ROLE_CATEGORY", roleCategory)) {
					getErrorMap().setError("roleCategory", BackOfficeErrorCodes.INVALID_OPTION);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("roleCategory", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateRoleDisabled() {
		try {
			if (getAction().equals(MODIFY) && !enabled) {
				DTObject formDTO = new DTObject();
				formDTO.set("STAFF_ROLE_CODE", staffRoleCode);
				formDTO = validateRoleDisabled(formDTO);
				if (formDTO.get(ContentManager.ERROR) != null) {
					getErrorMap().setError("enabled", formDTO.get(ContentManager.ERROR));
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("enabled", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateRoleDisabled(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		try {
			if (checkDisabledCode(inputDTO)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.PARENT_CANNOT_DISABLED);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return resultDTO;
	}

	public boolean checkDisabledCode(DTObject inputDTO) {
		String sqlQuery = ContentManager.EMPTY_STRING;
		sqlQuery = "SELECT COUNT(1) FROM STAFFROLE WHERE ENTITY_CODE=? AND PARENT_STAFF_ROLE_CODE=?";
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		boolean disabledCode = false;
		try {
			util.setMode(DBUtil.PREPARED);
			util.setSql(sqlQuery);
			util.setString(1, context.getEntityCode());
			util.setString(2, inputDTO.get("STAFF_ROLE_CODE"));
			ResultSet rs = util.executeQuery();
			if (rs.next()) {
				disabledCode = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			util.reset();
			dbContext.close();
		}
		return disabledCode;
	}
	
	
	
	public boolean validateCheck() {
		CommonValidator validation = new CommonValidator();
		try {
			int CheckBoxCount = 0;
			if (roleInBranch)
				CheckBoxCount++;
			if (regionalRole)
				CheckBoxCount++;
			if (nationalRole)
				CheckBoxCount++;
			if (CheckBoxCount > 1) {
				getErrorMap().setError("nationalRole", BackOfficeErrorCodes.ONLY_ONE_VALUE_CHECKED);
				return false;
			}
			if (CheckBoxCount == 0) {
				getErrorMap().setError("nationalRole", BackOfficeErrorCodes.ATLEAST_ONLY_ONE_VALUE_CHECKED);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("roleInBranch", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateRemarks() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (remarks != null && remarks.length() > 0) {
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			if (getAction().equals(MODIFY)) {
				if (commonValidator.isEmpty(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}
}
