package patterns.config.web.forms.emp;

/*
 *
 Author : NARESHKUMAR D
 Created Date : 30-JAN-2017
 Spec Reference : TATACAP/EMP/0013-ISTAFFSIGNATURE - Staff Signature Image File Uploads and Maintenance
 Modification History
 ----------------------------------------------------------------------
 Sl.No		Modified Date	Author			Modified Changes	Version
 ----------------------------------------------------------------------

 */
import java.util.ArrayList;
import java.util.Arrays;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.process.TBAActionType;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.framework.web.configuration.menu.MenuUtils;
import patterns.config.validations.AccessValidator;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.EmployeeValidator;
import patterns.config.validations.MasterValidator;
import patterns.config.web.forms.GenericFormBean;

public class istaffsignaturebean extends GenericFormBean {
	private static final long serialVersionUID = 1L;

	private String staffCode;
	private String effectiveDate;
	private String fileName;
	private String fileInventoryNumber;
	private String fileExtensionList;
	private String fileExt;
	private String fileInventoryNumberExit;
	private String redirectLandingPagePath;
	private String fileExtensionListError;

	public String getFileExtensionListError() {
		return fileExtensionListError;
	}

	public void setFileExtensionListError(String fileExtensionListError) {
		this.fileExtensionListError = fileExtensionListError;
	}

	public String getRedirectLandingPagePath() {
		return redirectLandingPagePath;
	}

	public void setRedirectLandingPagePath(String redirectLandingPagePath) {
		this.redirectLandingPagePath = redirectLandingPagePath;
	}

	public String getFileInventoryNumberExit() {
		return fileInventoryNumberExit;
	}

	public void setFileInventoryNumberExit(String fileInventoryNumberExit) {
		this.fileInventoryNumberExit = fileInventoryNumberExit;
	}

	public String getStaffCode() {
		return staffCode;
	}

	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}

	public String getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileInventoryNumber() {
		return fileInventoryNumber;
	}

	public void setFileInventoryNumber(String fileInventoryNumber) {
		this.fileInventoryNumber = fileInventoryNumber;
	}

	public String getFileExtensionList() {
		return fileExtensionList;
	}

	public void setFileExtensionList(String fileExtensionList) {
		this.fileExtensionList = fileExtensionList;
	}

	public String getFileExt() {
		return fileExt;
	}

	public void setFileExt(String fileExt) {
		this.fileExt = fileExt;
	}

	public String getReferenceNo() {
		return referenceNo;
	}

	public void setReferenceNo(String referenceNo) {
		this.referenceNo = referenceNo;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	private String referenceNo;
	private String filePath;

	public void reset() {
		staffCode = RegularConstants.EMPTY_STRING;
		effectiveDate = RegularConstants.EMPTY_STRING;
		fileName = RegularConstants.EMPTY_STRING;
		fileInventoryNumber = RegularConstants.EMPTY_STRING;
		fileInventoryNumberExit = RegularConstants.EMPTY_STRING;
		fileExtensionList = RegularConstants.EMPTY_STRING;
		fileExtensionListError = RegularConstants.EMPTY_STRING;
		fileExt = RegularConstants.EMPTY_STRING;
		referenceNo = RegularConstants.EMPTY_STRING;
		filePath = RegularConstants.EMPTY_STRING;
		redirectLandingPagePath = MenuUtils.resolveForward("elanding", webContext.getRequest());
		DTObject resultDTO = new DTObject();
		validateFileFormatAllowed(resultDTO);
		setProcessBO("patterns.config.framework.bo.emp.istaffsignatureBO");
	}

	public void validate() {
		MasterValidator validation = new MasterValidator();
		if (validateStaffCode() && validateEffectiveDate()) {
			validateReferenceNo();
			ArrayList<String> aList = new ArrayList<String>(Arrays.asList(fileExtensionList.split("\\,")));
			if (!aList.contains(fileExt)) {
				getErrorMap().setError("staffCode", BackOfficeErrorCodes.INVALID_FILE_FORMAT);
			}
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.set("STAFF_CODE", staffCode);
				formDTO.setObject("EFF_DATE", BackOfficeFormatUtils.getDate(effectiveDate, context.getDateFormat()));
				if (!validation.isEmpty(fileInventoryNumber))
					formDTO.set("FILE_INV_NO", fileInventoryNumber);
				else
					formDTO.set("FILE_INV_NO", fileInventoryNumberExit);
				formDTO.set("REFERENCE_NO", referenceNo);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("staffCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
	}

	public boolean validateStaffCode() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("STAFF_CODE", staffCode);
			formDTO.set("ACTION", USAGE);
			formDTO = validateStaffCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("staffCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("staffCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateStaffCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, ContentManager.DATA_UNAVAILABLE);
		EmployeeValidator validation = new EmployeeValidator();
		String staffCode = inputDTO.get("STAFF_CODE");
		try {
			if (validation.isEmpty(staffCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH, ContentManager.FETCH_ALL);
			resultDTO = validation.validateStaffCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateEffectiveDate() {
		try {
			DTObject formDTO = getFormDTO();
			formDTO.set("ENTITY_CODE", context.getEntityCode());
			formDTO.set("STAFF_CODE", staffCode);
			formDTO.set("EFF_DATE", effectiveDate);
			formDTO.set(ContentManager.USER_ACTION, getAction());
			formDTO = validateEffectiveDate(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("effectiveDate", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("effectiveDate", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateEffectiveDate(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, ContentManager.DATA_UNAVAILABLE);
		CommonValidator commonvalidation = new CommonValidator();
		String entityCode = inputDTO.get("ENTITY_CODE");
		String staffCode = inputDTO.get("STAFF_CODE");
		String effectiveDate = inputDTO.get("EFF_DATE");
		String action = inputDTO.get(ContentManager.USER_ACTION);
		try {
			if (commonvalidation.isEmpty(effectiveDate)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_FIELD_BLANK);
				return resultDTO;
			}
			TBAActionType AT;
			if (action.equals(ADD))
				AT = TBAActionType.ADD;
			else
				AT = TBAActionType.MODIFY;
			if (!commonvalidation.isEffectiveDateValid(effectiveDate, AT)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_EFFECTIVE_DATE);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH, ContentManager.FETCH_ALL);
			inputDTO.set(ContentManager.TABLE, "STAFFSIGNATUREHIST");
			String columns[] = new String[] { entityCode, staffCode, effectiveDate };
			inputDTO.setObject(ContentManager.PROGRAM_KEY, columns);
			resultDTO = commonvalidation.validatePK(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (action.equals(ADD)) {
				if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_RECORD_EXISTS);
				}
			} else if (action.equals(MODIFY)) {
				if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_RECORD_NOT_EXISTS);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			commonvalidation.close();
		}
		return resultDTO;
	}

	private boolean validateReferenceNo() {
		CommonValidator validation = new CommonValidator();
		try {
			if (!validation.isEmpty(referenceNo)) {
				if (!validation.isValidRefNumber(referenceNo)) {
					getErrorMap().setError("referenceNo", BackOfficeErrorCodes.PBS_INVALID_REF_NUMBER);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("referenceNo", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public void validateFileFormatAllowed(DTObject inputDTO) {
		AccessValidator validator = new AccessValidator();
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		try {
			inputDTO.set("PGM_ID", context.getProcessID());
			inputDTO.set("PURPOSE", "CMN");
			resultDTO = validator.readFilePurposeParameter(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				fileExtensionListError = getErrorResource(BackOfficeErrorCodes.UNSPECIFIED_ERROR, null);
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				fileExtensionListError = getErrorResource(BackOfficeErrorCodes.FILEPURPOSE_CFG_UNAVAILABLE, null);
			}
			fileExtensionList = resultDTO.get("ALLOWED_EXTENSIONS");
		} catch (Exception e) {
			e.printStackTrace();
			fileExtensionListError = getErrorResource(BackOfficeErrorCodes.UNSPECIFIED_ERROR, null);
		} finally {
			validator.close();
		}
	}
}
