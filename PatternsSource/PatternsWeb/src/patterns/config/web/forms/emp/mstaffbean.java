package patterns.config.web.forms.emp;

/*
 *
 Author : Javeed S
 Created Date : 08-FEB-2016
 Spec Reference : PPBS/EMP/0037-MSTAFF - STAFF DETAILS MAINTENANCE
 Modification History
 ----------------------------------------------------------------------
 Sl.No		Modified Date	Author			Modified Changes	Version
 ----------------------------------------------------------------------

 */
import java.util.ArrayList;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.GenericOption;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.EmployeeValidator;
import patterns.config.web.forms.GenericFormBean;

public class mstaffbean extends GenericFormBean {

	private static final long serialVersionUID = -6219657363150466160L;

	private String staffEmpCode;
	private String name;
	private String dateOfJoining;
	private String emailId;
	private String mobileNumber;
	private String staffRole;
	private String branchCode;
	private String regionCode;
	private String areaCode;
	private String reportingToStaffId;
	private String remarks;

	public String getStaffEmpCode() {
		return staffEmpCode;
	}

	public void setStaffEmpCode(String staffEmpCode) {
		this.staffEmpCode = staffEmpCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDateOfJoining() {
		return dateOfJoining;
	}

	public void setDateOfJoining(String dateOfJoining) {
		this.dateOfJoining = dateOfJoining;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getStaffRole() {
		return staffRole;
	}

	public void setStaffRole(String staffRole) {
		this.staffRole = staffRole;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getRegionCode() {
		return regionCode;
	}

	public void setRegionCode(String regionCode) {
		this.regionCode = regionCode;
	}

	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	public String getReportingToStaffId() {
		return reportingToStaffId;
	}

	public void setReportingToStaffId(String reportingToStaffId) {
		this.reportingToStaffId = reportingToStaffId;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void reset() {
		staffEmpCode = RegularConstants.EMPTY_STRING;
		name = RegularConstants.EMPTY_STRING;
		dateOfJoining = RegularConstants.EMPTY_STRING;
		emailId = RegularConstants.EMPTY_STRING;
		mobileNumber = RegularConstants.EMPTY_STRING;
		staffRole = RegularConstants.EMPTY_STRING;
		branchCode = RegularConstants.EMPTY_STRING;
		regionCode = RegularConstants.EMPTY_STRING;
		areaCode = RegularConstants.EMPTY_STRING;
		reportingToStaffId = RegularConstants.EMPTY_STRING;
		remarks = RegularConstants.EMPTY_STRING;
		DBContext dbContext = new DBContext();

		ArrayList<GenericOption> currentStatus = null;
		currentStatus = getGenericOptions("MSTAFF", "STAFF_STATUS", dbContext, null);
		webContext.getRequest().setAttribute("MSTAFF_STAFF_STATUS", currentStatus);

		dbContext.close();
		setProcessBO("patterns.config.framework.bo.emp.mstaffBO");

	}

	@Override
	public void validate() {
		if (validateStaffEmpCode()) {
			validateName();
			validateDateOfJoining();
			validateEmailId();
			validateMobileNumber();
			validateRemarks();
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.set("STAFF_CODE", staffEmpCode);
				formDTO.set("NAME", name);
				formDTO.set("DATE_OF_JOINING", dateOfJoining);
				formDTO.set("EMAIL_ID", emailId);
				formDTO.set("MOBILE_NO", mobileNumber);
				formDTO.set("REMARKS", remarks);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("staffEmpCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
	}

	public boolean validateStaffEmpCode() {
		try {
			DTObject formDTO = getFormDTO();
			formDTO.set("STAFF_CODE", staffEmpCode);
			formDTO.set(ContentManager.ACTION, getAction());
			formDTO = validateStaffEmpCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("staffEmpCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("staffEmpCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateStaffEmpCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		EmployeeValidator validation = new EmployeeValidator();
		String staffEmpCode = inputDTO.get("STAFF_CODE");
		String action = inputDTO.get(ContentManager.ACTION);
		try {
			if (validation.isEmpty(staffEmpCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "STAFF_STATUS");
			resultDTO = validation.validateStaffCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (action.equals(ADD)) {
				if (ContentManager.DATA_AVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_RECORD_EXISTS);
					return resultDTO;
				}
			} else {
				if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_RECORD_NOT_EXISTS);
					return resultDTO;
				}
			}
			if ("D".equals(resultDTO.get("STAFF_STATUS"))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.STAFFCODE_DELETED);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateName() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(name)) {
				getErrorMap().setError("name", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidName(name)) {
				getErrorMap().setError("name", BackOfficeErrorCodes.INVALID_NAME);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("name", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateDateOfJoining() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(dateOfJoining)) {
				getErrorMap().setError("dateOfJoining", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isDateLesserEqualCBD(dateOfJoining)) {
				getErrorMap().setError("dateOfJoining", BackOfficeErrorCodes.DATE_LECBD);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("dateOfJoining", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateEmailId() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(emailId)) {
				getErrorMap().setError("emailId", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidEmail(emailId)) {
				getErrorMap().setError("emailId", BackOfficeErrorCodes.INVALID_EMAIL_ID);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("emailId", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateMobileNumber() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(mobileNumber)) {
				getErrorMap().setError("mobileNumber", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidPhoneNumber(mobileNumber)) {
				getErrorMap().setError("mobileNumber", BackOfficeErrorCodes.INVALID_MOBILE_NUMBER);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("mobileNumber", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateRemarks() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (remarks != null && remarks.length() > 0) {
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			if (getAction().equals(MODIFY)) {
				if (commonValidator.isEmpty(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}

}
