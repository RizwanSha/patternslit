package patterns.config.web.forms.emp;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.MasterValidator;
import patterns.config.web.forms.GenericFormBean;

public class mbranchbean extends GenericFormBean {

	private static final long serialVersionUID = 1L;
	private String branchCode;
	private String areaCode;
	private String branchName;
	private String branchAdd;
	private String pinCode;
	private String stateCode;
	private String regionCode;
	private String vatTin;
	private String cstTin;
	private boolean enabled;
	private String remarks;

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getBranchAdd() {
		return branchAdd;
	}

	public void setBranchAdd(String branchAdd) {
		this.branchAdd = branchAdd;
	}

	public String getPinCode() {
		return pinCode;
	}

	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}

	public String getStateCode() {
		return stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	public String getRegionCode() {
		return regionCode;
	}

	public void setRegionCode(String regionCode) {
		this.regionCode = regionCode;
	}

	public String getVatTin() {
		return vatTin;
	}

	public void setVatTin(String vatTin) {
		this.vatTin = vatTin;
	}

	public String getCstTin() {
		return cstTin;
	}

	public void setCstTin(String cstTin) {
		this.cstTin = cstTin;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public void reset() {
		// TODO Auto-generated method stub
		branchCode = RegularConstants.EMPTY_STRING;
		areaCode = RegularConstants.EMPTY_STRING;
		branchName = RegularConstants.EMPTY_STRING;
		branchAdd = RegularConstants.EMPTY_STRING;
		pinCode = RegularConstants.EMPTY_STRING;
		stateCode = RegularConstants.EMPTY_STRING;
		regionCode = RegularConstants.EMPTY_STRING;
		vatTin = RegularConstants.EMPTY_STRING;
		cstTin = RegularConstants.EMPTY_STRING;
		enabled = false;
		remarks = RegularConstants.EMPTY_STRING;
		setProcessBO("patterns.config.framework.bo.emp.mbranchBO");
	}

	@Override
	public void validate() {
		if (validateBranchCode()) {
			validateAreaCode();
			validateBranchName();
			validateBranchAdd();
			validatePinCode();
			validateStateCode();
			validateRegionCode();
			validateVatTin();
			validateCstTin();
			validateRemarks();
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.set("BRANCH_CODE", branchCode);
				formDTO.set("BRANCH_NAME", branchName);
				formDTO.set("ADDRESS", branchAdd);
				formDTO.set("PIN_CODE", pinCode);
				formDTO.set("STATE_CODE", stateCode);
				formDTO.set("REGION_CODE", regionCode);
				formDTO.set("VAT_TIN_NO", vatTin);
				formDTO.set("CST_TIN_NO", cstTin);
				if (getAction().equals(ADD)) {
					formDTO.set("ENABLED", decodeBooleanToString(true));
				} else {
					formDTO.set("ENABLED", decodeBooleanToString(enabled));
				}
				formDTO.set("REMARKS", remarks);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("branchCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
	}

	public boolean validateBranchCode() {
		MasterValidator validation = new MasterValidator();
		try {
			if (validation.isEmpty(branchCode)) {
				getErrorMap().setError("branchCode", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			DTObject formDTO = getFormDTO();
			formDTO.set("BRANCH_CODE", branchCode);
			formDTO.set(ContentManager.USER_ACTION, getAction());
			formDTO = validateBranchCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("branchCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("branchCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public DTObject validateBranchCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		MasterValidator validation = new MasterValidator();
		String branchCode = inputDTO.get("BRANCH_CODE");
		String action = inputDTO.get(ContentManager.USER_ACTION);
		try {
			if (validation.isEmpty(branchCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "BRANCH_CODE");
			resultDTO = validation.validateBranchCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				return resultDTO;
			}
			if (action.equals(ADD)) {
				if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_EXISTS);
					return resultDTO;
				}
			}
			if (action.equals(MODIFY)) {
				if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_NOT_EXISTS);
					return resultDTO;
				}
			}
			return resultDTO;
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateBranchName() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(branchName)) {
				getErrorMap().setError("branchName", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidDescription(branchName)) {
				getErrorMap().setError("branchName", BackOfficeErrorCodes.INVALID_DESCRIPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("branchName", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateBranchAdd() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(branchAdd)) {
				getErrorMap().setError("branchAdd", BackOfficeErrorCodes.HMS_FIELD_BLANK);
				return false;
			}
			if (!validation.isValidRemarks(branchAdd)) {
				getErrorMap().setError("branchAdd", BackOfficeErrorCodes.HMS_INVALID_REMARKS);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("branchAdd", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validatePinCode() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(pinCode)) {
				getErrorMap().setError("pinCode", BackOfficeErrorCodes.HMS_FIELD_BLANK);
				return false;
			}
			if (!validation.isValidNumber(pinCode)) {
				getErrorMap().setError("pinCode", BackOfficeErrorCodes.INVALID_NUMBER);
				return false;
			}
			if (!validation.isValidLength(pinCode, validation.LENGTH_6, validation.LENGTH_6)) {
				getErrorMap().setError("pinCode", BackOfficeErrorCodes.PINCODE_LENGTH);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("pinCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateStateCode() {
		MasterValidator validation = new MasterValidator();
		try {
			if (validation.isEmpty(stateCode)) {
				getErrorMap().setError("stateCode", BackOfficeErrorCodes.HMS_FIELD_BLANK);
				return false;
			}
			DTObject formDTO = getFormDTO();
			formDTO.set("STATE_CODE", stateCode);
			formDTO.set(ContentManager.USER_ACTION, USAGE);
			formDTO = validateStateCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("stateCode", formDTO.get(ContentManager.ERROR));
				return false;
			}

			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("stateCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public DTObject validateStateCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		MasterValidator validation = new MasterValidator();
		try {
			inputDTO.set(ContentManager.USER_ACTION, USAGE);
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
			resultDTO = validation.validateStateCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				return resultDTO;
			} else if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
			return resultDTO;
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateRegionCode() {
		MasterValidator validation = new MasterValidator();
		try {
			if (validation.isEmpty(regionCode)) {
				getErrorMap().setError("regionCode", BackOfficeErrorCodes.HMS_FIELD_BLANK);
				return false;
			}
			DTObject formDTO = getFormDTO();
			formDTO.set("REGION_CODE", regionCode);
			formDTO.set(ContentManager.USER_ACTION, USAGE);
			formDTO = validateRegionCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("regionCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("regionCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public DTObject validateRegionCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		MasterValidator validation = new MasterValidator();
		try {
			inputDTO.set(ContentManager.USER_ACTION, USAGE);
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
			resultDTO = validation.validateRegionCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				return resultDTO;
			} else if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
			return resultDTO;
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateAreaCode() {
		MasterValidator validation = new MasterValidator();
		try {
			if (validation.isEmpty(areaCode)) {
				getErrorMap().setError("areaCode", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			DTObject formDTO = getFormDTO();
			formDTO.set("REGION_CODE", regionCode);
			formDTO.set("AREA_CODE", areaCode);
			formDTO.set(ContentManager.USER_ACTION, getAction());
			formDTO = validateAreaCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("areaCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("areaCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public DTObject validateAreaCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		MasterValidator validation = new MasterValidator();
		String areaCode = inputDTO.get("AREA_CODE");
		try {
			if (validation.isEmpty(areaCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "AREA_CODE,DESCRIPTION");
			resultDTO = validation.validateAreaCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateVatTin() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(vatTin)) {
				getErrorMap().setError("vatTin", BackOfficeErrorCodes.HMS_FIELD_BLANK);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("vatTin", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateCstTin() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(cstTin)) {
				getErrorMap().setError("cstTin", BackOfficeErrorCodes.HMS_FIELD_BLANK);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("cstTin", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateRemarks() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (remarks != null && remarks.length() > 0) {
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.HMS_INVALID_REMARKS);
					return false;
				}
			}
			if (getAction().equals(MODIFY)) {
				if (commonValidator.isEmpty(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.HMS_FIELD_BLANK);
					return false;
				}
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.HMS_INVALID_REMARKS);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}
}
