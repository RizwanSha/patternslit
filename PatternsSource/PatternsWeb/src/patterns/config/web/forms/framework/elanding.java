package patterns.config.web.forms.framework;

import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.SessionConstants;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.AccessValidator;
import patterns.config.web.forms.GenericFormBean;

public class elanding extends GenericFormBean {

	private static final long serialVersionUID = -6219657363150466160L;

	private String userID;
	private String tmpOrgId;

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public String getTmpOrgId() {
		return tmpOrgId;
	}

	public void setTmpOrgId(String tmpOrgId) {
		this.tmpOrgId = tmpOrgId;
	}

	@Override
	public void reset() {
		setProcessBO("patterns.config.framework.bo.common.eloginBO");
		userID = ContentManager.EMPTY_STRING;
		if (!context.isFormPosted()) {
			webContext.getSession().setMaxInactiveInterval(SessionConstants.MAX_LOGIN_INACTIVE_INTERVAL_SECONDS);
			webContext.getSession().setAttribute(SessionConstants.TEMP_SESSION_START, RegularConstants.COLUMN_ENABLE);
		}
	}

	


	@Override
	public void validate() {
		if (userID == null)
			userID = ContentManager.EMPTY_STRING;
		userID = userID.toUpperCase(context.getLocale());
		DTObject formDTO = getFormDTO();
		formDTO.set("P_ENTITY_CODE", "1");
		formDTO.set("P_USER_ID", userID);
		formDTO.set("P_USER_IP", webContext.getRequest().getRemoteAddr());
		formDTO.set("P_USER_AGENT", webContext.getRequest().getHeader("User-Agent"));
		formDTO.set("P_LOGIN_SESSION_ID", webContext.getSession().getId());
		formDTO.set("P_SERVER_ADDR", webContext.getRequest().getServerName());
		formDTO.set("P_ORG_DESCN", "HMS");
	}
	
	public DTObject validateCashier(DTObject inputDTO) {
        DTObject resultDTO = new DTObject();
        DTObject installDTO = new DTObject();
        AccessValidator validator = new AccessValidator();
        try {
                    installDTO = validator.getINSTALL(installDTO);
                    if (installDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)){
                          resultDTO.set("DASHBOARD_REQ", RegularConstants.COLUMN_DISABLE);
                          return resultDTO;
                    }else{
                          if (RegularConstants.COLUMN_DISABLE.equals(installDTO.get("DASHBOARD_REQ"))){
                                return installDTO;
                          }
                          resultDTO.set("DASHBOARD_REQ",installDTO.get("DASHBOARD_REQ"));
                    }     
                    
                    installDTO.set(ContentManager.CODE,context.getRoleCode());
                    installDTO.set(ContentManager.FETCH_COLUMNS, "CASHIER_ROLE,HEAD_CASHIER_ROLE");
                    installDTO.set(ContentManager.USER_ACTION, "U");
                    resultDTO = validator.validateInternalRole(installDTO);
                    if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
                          if (resultDTO.get("CASHIER_ROLE").equals(RegularConstants.COLUMN_ENABLE) || resultDTO.get("HEAD_CASHIER_ROLE").equals(RegularConstants.COLUMN_ENABLE))
                                resultDTO.set("CASHIER", RegularConstants.COLUMN_ENABLE);
                          else
                                resultDTO.set("CASHIER", RegularConstants.COLUMN_DISABLE);
                    }else{
                          resultDTO.set("DASHBOARD_REQ",RegularConstants.COLUMN_DISABLE);
                    }
        } catch (Exception e) {
              e.printStackTrace();
        } finally {
              validator.close();
        }
        return resultDTO;
  }

}