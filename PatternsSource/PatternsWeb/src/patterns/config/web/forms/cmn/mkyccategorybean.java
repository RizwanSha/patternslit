 package patterns.config.web.forms.cmn;

import java.util.ArrayList;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.GenericOption;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.MasterValidator;
import patterns.config.web.forms.GenericFormBean;



public class mkyccategorybean extends GenericFormBean {

	private static final long serialVersionUID = 1L;
	private String kycCategoryCode;
	private String description;
	private String conciseDescription;
	private String categoryType;
	private boolean proofOfIdentify;
	private boolean proofOfAddress;
	private boolean enabled;
	private String remarks;

	



	public String getKycCategoryCode() {
		return kycCategoryCode;
	}

	public void setKycCategoryCode(String kycCategoryCode) {
		this.kycCategoryCode = kycCategoryCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getConciseDescription() {
		return conciseDescription;
	}

	public void setConciseDescription(String conciseDescription) {
		this.conciseDescription = conciseDescription;
	}

	public String getCategoryType() {
		return categoryType;
	}

	public void setCategoryType(String categoryType) {
		this.categoryType = categoryType;
	}

	public boolean isProofOfIdentify() {
		return proofOfIdentify;
	}

	public void setProofOfIdentify(boolean proofOfIdentify) {
		this.proofOfIdentify = proofOfIdentify;
	}

	public boolean isProofOfAddress() {
		return proofOfAddress;
	}

	public void setProofOfAddress(boolean proofOfAddress) {
		this.proofOfAddress = proofOfAddress;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}



	public void reset() {
		kycCategoryCode = RegularConstants.EMPTY_STRING;
		description = RegularConstants.EMPTY_STRING;
		conciseDescription = RegularConstants.EMPTY_STRING;
		categoryType = RegularConstants.EMPTY_STRING;
		proofOfIdentify = false;
		proofOfAddress = false;
		enabled = false;
		remarks = RegularConstants.EMPTY_STRING;
		
		DBContext dbContext = new DBContext();
		ArrayList<GenericOption> categoryType = null;
		categoryType = getGenericOptions("MKYCCATEGORY", "CAT_TYPE", dbContext, null);
		webContext.getRequest().setAttribute("MKYCCATEGORY_CAT_TYPE", categoryType);
		dbContext.close();
		setProcessBO("patterns.config.framework.bo.cmn.mkyccategoryBO");
	}

	

	public void validate() {
		if (validateKYCCategoryCode()) {
			validateDescription();
			validateConciseDescription();
			validateCategoryType();
			validateRemarks();
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set(ContentManager.ENTITY_CODE, context.getEntityCode());
				formDTO.set("KYC_CATEGORY", kycCategoryCode);
				formDTO.set("DESCRIPTION", description);
				formDTO.set("CONCISE_DESCRIPTION", conciseDescription);
				formDTO.set("CATEGORY_FOR", categoryType);
				formDTO.set("POI_REQD", decodeBooleanToString(proofOfIdentify));
				formDTO.set("POA_REQD", decodeBooleanToString(proofOfAddress));
				if (getAction().equals(ADD)) {
					formDTO.set("ENABLED", decodeBooleanToString(true));
				} else {
					formDTO.set("ENABLED", decodeBooleanToString(enabled));
				}
				formDTO.set("REMARKS", remarks);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("kycCategoryCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}

	}

	
	public boolean validateKYCCategoryCode() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("KYC_CATEGORY", kycCategoryCode);
			formDTO.set(ContentManager.ACTION, getAction());
			formDTO = validateKYCCategoryCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("kycCategoryCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("kycCategoryCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateKYCCategoryCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		MasterValidator validation = new MasterValidator();
		String kycCategoryCode = inputDTO.get("KYC_CATEGORY");
		String action = inputDTO.get(ContentManager.USER_ACTION);
		try {
			if (validation.isEmpty(kycCategoryCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
			resultDTO = validation.validateKYCCategoryCode(inputDTO);
			if (action.equals(ADD)) {
				if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_EXISTS);
				}
			} else {
				if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_NOT_EXISTS);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	
	public boolean validateDescription() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(description)) {
				getErrorMap().setError("description", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidDescription(description)) {
				getErrorMap().setError("description", BackOfficeErrorCodes.INVALID_DESCRIPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("description", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateConciseDescription() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(conciseDescription)) {
				getErrorMap().setError("conciseDescription", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidConciseDescription(conciseDescription)) {
				getErrorMap().setError("conciseDescription", BackOfficeErrorCodes.INVALID_CONCISE_DESCRIPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("conciseDescription", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateCategoryType() {
		CommonValidator validation = new CommonValidator();
		try {
			if (!validation.isEmpty(categoryType)) {
			if (!validation.isCMLOVREC("MKYCCATEGORY", "CAT_TYPE", categoryType)) {
				getErrorMap().setError("categoryType", BackOfficeErrorCodes.INVALID_OPTION);
				return false;

			}
		}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("categoryType", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateRemarks() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (remarks != null && remarks.length() > 0) {
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			if (getAction().equals(MODIFY)) {
				if (commonValidator.isEmpty(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}

}
