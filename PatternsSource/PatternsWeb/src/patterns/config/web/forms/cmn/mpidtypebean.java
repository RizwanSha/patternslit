package patterns.config.web.forms.cmn;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.MasterValidator;
import patterns.config.web.forms.GenericFormBean;

public class mpidtypebean extends GenericFormBean {

	private static final long serialVersionUID = 8790049416794248092L;

	private String pidTypeCode;
	private String description;
	private String conciseDescription;
	private boolean codeMeantForIncomeTaxPayerId;
	private boolean codemeantfornationalID;
	private boolean codeMeantForPassport;
	private boolean documentCanUseForPersonIdentification;
	private boolean documentCanUseForAddressIdentifaction;
	private boolean officiallyValidDoc;
	private boolean underSimpMeasuresKyc;
	private boolean officiallyValidDocument;
	private boolean underSimplifiedMeasuresKyc;
	private String countryCode;
	private String issueOfDocument;
	private boolean documentHasIssueDate;
	private boolean documenthasExpiryDate;
	private boolean altAccntId;
	private boolean pidForBplCard;
	private boolean enabled;
	private String remarks;

	public boolean isOfficiallyValidDocument() {
		return officiallyValidDocument;
	}

	public void setOfficiallyValidDocument(boolean officiallyValidDocument) {
		this.officiallyValidDocument = officiallyValidDocument;
	}

	public boolean isUnderSimplifiedMeasuresKyc() {
		return underSimplifiedMeasuresKyc;
	}

	public void setUnderSimplifiedMeasuresKyc(boolean underSimplifiedMeasuresKyc) {
		this.underSimplifiedMeasuresKyc = underSimplifiedMeasuresKyc;
	}

	public boolean isOfficiallyValidDoc() {
		return officiallyValidDoc;
	}

	public void setOfficiallyValidDoc(boolean officiallyValidDoc) {
		this.officiallyValidDoc = officiallyValidDoc;
	}

	public boolean isUnderSimpMeasuresKyc() {
		return underSimpMeasuresKyc;
	}

	public void setUnderSimpMeasuresKyc(boolean underSimpMeasuresKyc) {
		this.underSimpMeasuresKyc = underSimpMeasuresKyc;
	}

	public String getPidTypeCode() {
		return pidTypeCode;
	}

	public void setPidTypeCode(String pidTypeCode) {
		this.pidTypeCode = pidTypeCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getConciseDescription() {
		return conciseDescription;
	}

	public void setConciseDescription(String conciseDescription) {
		this.conciseDescription = conciseDescription;
	}

	public boolean isCodeMeantForIncomeTaxPayerId() {
		return codeMeantForIncomeTaxPayerId;
	}

	public void setCodeMeantForIncomeTaxPayerId(boolean codeMeantForIncomeTaxPayerId) {
		this.codeMeantForIncomeTaxPayerId = codeMeantForIncomeTaxPayerId;
	}

	public boolean isCodemeantfornationalID() {
		return codemeantfornationalID;
	}

	public void setCodemeantfornationalID(boolean codemeantfornationalID) {
		this.codemeantfornationalID = codemeantfornationalID;
	}

	public boolean isCodeMeantForPassport() {
		return codeMeantForPassport;
	}

	public void setCodeMeantForPassport(boolean codeMeantForPassport) {
		this.codeMeantForPassport = codeMeantForPassport;
	}

	public boolean isDocumentCanUseForPersonIdentification() {
		return documentCanUseForPersonIdentification;
	}

	public void setDocumentCanUseForPersonIdentification(boolean documentCanUseForPersonIdentification) {
		this.documentCanUseForPersonIdentification = documentCanUseForPersonIdentification;
	}

	public boolean isDocumentCanUseForAddressIdentifaction() {
		return documentCanUseForAddressIdentifaction;
	}

	public void setDocumentCanUseForAddressIdentifaction(boolean documentCanUseForAddressIdentifaction) {
		this.documentCanUseForAddressIdentifaction = documentCanUseForAddressIdentifaction;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getIssueOfDocument() {
		return issueOfDocument;
	}

	public void setIssueOfDocument(String issueOfDocument) {
		this.issueOfDocument = issueOfDocument;
	}

	public boolean isDocumentHasIssueDate() {
		return documentHasIssueDate;
	}

	public void setDocumentHasIssueDate(boolean documentHasIssueDate) {
		this.documentHasIssueDate = documentHasIssueDate;
	}

	public boolean isDocumenthasExpiryDate() {
		return documenthasExpiryDate;
	}

	public void setDocumenthasExpiryDate(boolean documenthasExpiryDate) {
		this.documenthasExpiryDate = documenthasExpiryDate;
	}

	public boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public boolean isAltAccntId() {
		return altAccntId;
	}

	public void setAltAccntId(boolean altAccntId) {
		this.altAccntId = altAccntId;
	}

	public boolean isPidForBplCard() {
		return pidForBplCard;
	}

	public void setPidForBplCard(boolean pidForBplCard) {
		this.pidForBplCard = pidForBplCard;
	}

	@Override
	public void reset() {
		pidTypeCode = RegularConstants.EMPTY_STRING;
		description = RegularConstants.EMPTY_STRING;
		conciseDescription = RegularConstants.EMPTY_STRING;
		codeMeantForIncomeTaxPayerId = false;
		codemeantfornationalID = false;
		codeMeantForPassport = false;
		documentCanUseForPersonIdentification = false;
		documentCanUseForAddressIdentifaction = false;
		officiallyValidDoc=false;
		underSimpMeasuresKyc=false;
		officiallyValidDocument=false;
		underSimplifiedMeasuresKyc=false;
		countryCode = RegularConstants.EMPTY_STRING;
		issueOfDocument = RegularConstants.EMPTY_STRING;
		documentHasIssueDate = false;
		documenthasExpiryDate = false;
		altAccntId = false;
		pidForBplCard = false;
		enabled = false;
		remarks = RegularConstants.EMPTY_STRING;
		setProcessBO("patterns.config.framework.bo.cmn.mpidtypeBO");
	}

	@Override
	public void validate() {
		if (validatePIDCode()) {
			validateDescription();
			validateConciseDescription();
			validateCountryCode();
			validateissueOfDocument();
			validateExpiryDate();
			validateRemarks();
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("PID_CODE", pidTypeCode);
				formDTO.set("DESCRIPTION", description);
				formDTO.set("CONCISE_DESCRIPTION", conciseDescription);
				formDTO.set("INCOME_TAX_PAYER_ID", decodeBooleanToString(codeMeantForIncomeTaxPayerId));
				formDTO.set("NATIONAL_ID", decodeBooleanToString(codemeantfornationalID));
				formDTO.set("PASSPORT_ID", decodeBooleanToString(codeMeantForPassport));
				formDTO.set("DOC_USED_FOR_ID_PROOF", decodeBooleanToString(documentCanUseForPersonIdentification));
				formDTO.set("ID_PROOF_OFFICIALLY_VALID_DOC", decodeBooleanToString(officiallyValidDoc));
				formDTO.set("ID_PROOF_SIMPLIFIED_MEASURE_KYC", decodeBooleanToString(underSimpMeasuresKyc));
				formDTO.set("DOC_USED_FOR_ADDR_PROOF", decodeBooleanToString(documentCanUseForAddressIdentifaction));
				formDTO.set("ADDR_PROOF_OFFICIALLY_VALID_DOC", decodeBooleanToString(officiallyValidDocument));
				formDTO.set("ADDR_PROOF_SIMPLIFIED_MEASURE_KYC", decodeBooleanToString(underSimplifiedMeasuresKyc));
				formDTO.set("DOC_APPL_FOR_COUNTRY", countryCode);
				formDTO.set("DOC_ISSUER_NAME", issueOfDocument);
				formDTO.set("DOC_HAS_ISSUE_DATE", decodeBooleanToString(documentHasIssueDate));
				formDTO.set("DOC_HAS_EXPIRY_DATE", decodeBooleanToString(documenthasExpiryDate));
				formDTO.set("ALT_ACCOUNT_ID", decodeBooleanToString(altAccntId));
				formDTO.set("PID_FOR_BPL_CARD", decodeBooleanToString(pidForBplCard));
				if (getAction().equals(ADD)) {
					formDTO.set("ENABLED", decodeBooleanToString(true));
				} else {
					formDTO.set("ENABLED", decodeBooleanToString(enabled));
				}
				formDTO.set("REMARKS", remarks);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("pidTypeCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
	}

	public boolean validatePIDCode() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("PID_CODE", pidTypeCode);
			formDTO.set(ContentManager.ACTION, getAction());
			formDTO = validatePIDCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("pidTypeCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("pidTypeCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validatePIDCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		MasterValidator validation = new MasterValidator();
		String pidTypeCode = inputDTO.get("PID_CODE");
		String action = inputDTO.get(ContentManager.USER_ACTION);
		try {
			if (validation.isEmpty(pidTypeCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "PID_CODE");
			resultDTO = validation.validatePIDCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				return resultDTO;
			}
			if (action.equals(ADD)) {
				if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_EXISTS);
				}
			} else {
				if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_NOT_EXISTS);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	private boolean validateDescription() {
		CommonValidator validation = new CommonValidator();
		try {
			if (!validation.isValidDescription(description)) {
				getErrorMap().setError("description", BackOfficeErrorCodes.INVALID_DESCRIPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("description", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	private boolean validateConciseDescription() {
		CommonValidator validation = new CommonValidator();
		try {
			if (!validation.isValidConciseDescription(conciseDescription)) {
				getErrorMap().setError("conciseDescription", BackOfficeErrorCodes.INVALID_CONCISE_DESCRIPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("conciseDescription", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	private boolean validateExpiryDate() {
		CommonValidator validation = new CommonValidator();
		try {
			if (decodeBooleanToString(documenthasExpiryDate).equals(RegularConstants.ONE)) {
				if (decodeBooleanToString(documentHasIssueDate).equals(RegularConstants.ZERO)) {
					getErrorMap().setError("documentHasIssueDate", BackOfficeErrorCodes.HMS_DOC_EXP_ISS_NA);
					return false;
				}
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("documenthasExpiryDate", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	private boolean validateissueOfDocument() {
		CommonValidator validation = new CommonValidator();
		try {
			if (!validation.isValidDescription(issueOfDocument)) {
				getErrorMap().setError("issueOfDocument", BackOfficeErrorCodes.INVALID_DESCRIPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("issueOfDocument", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateCountryCode() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("COUNTRY_CODE", countryCode);
			formDTO.set(ContentManager.ACTION, USAGE);
			formDTO = validateCountryCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("countryCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("countryCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;

	}

	public DTObject validateCountryCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		MasterValidator validation = new MasterValidator();
		String countryCode = inputDTO.get("COUNTRY_CODE");
		try {
			if (!validation.isEmpty(countryCode)) {
				inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
				resultDTO = validation.validateCountryCode(inputDTO);
				if (resultDTO.get(ContentManager.ERROR) != null) {
					resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
					return resultDTO;
				}
				if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
					return resultDTO;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateRemarks() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (remarks != null && remarks.length() > 0) {
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			if (getAction().equals(MODIFY)) {
				if (commonValidator.isEmpty(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}
}
