package patterns.config.web.forms.cmn;

/**
 * This program will be used to enter PID Registration for a Person
 * 
 * @version 1.0
 * @author Jayashree
 * @since 04-Dec-2014 <br>
 *        <b>Modified History</b> <BR>
 *        
 *         @author RAJA
 * 		@since 08-JULY-2016 <br>
 *        <U><B> Sl.No Modified Date Author Modified Changes Version </B></U> <br>
 */

import java.sql.ResultSet;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.process.TBAActionType;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.MasterValidator;
import patterns.config.web.forms.GenericFormBean;

public class epidregbean extends GenericFormBean {

	private static final long serialVersionUID = 1L;
	private String personId;
	private String PIDRegSl;
	private boolean deRegister;
	private String dateOfRegistration;
	private String PIDCode;
	private String PIDNumber;
	private String PIDIssueDate;
	private String PIDExpiryDate;
	private String PIDRemarks;
	private String dateOfDereg;
	private String reasonOfDereg;

	public String getPersonId() {
		return personId;
	}

	public void setPersonId(String personId) {
		this.personId = personId;
	}

	public String getPIDRegSl() {
		return PIDRegSl;
	}

	public void setPIDRegSl(String pIDRegSl) {
		PIDRegSl = pIDRegSl;
	}

	public boolean isDeRegister() {
		return deRegister;
	}

	public void setDeRegister(boolean deRegister) {
		this.deRegister = deRegister;
	}

	public String getDateOfRegistration() {
		return dateOfRegistration;
	}

	public void setDateOfRegistration(String dateOfRegistration) {
		this.dateOfRegistration = dateOfRegistration;
	}

	public String getPIDCode() {
		return PIDCode;
	}

	public void setPIDCode(String pIDCode) {
		PIDCode = pIDCode;
	}

	public String getPIDNumber() {
		return PIDNumber;
	}

	public void setPIDNumber(String pIDNumber) {
		PIDNumber = pIDNumber;
	}

	public String getPIDIssueDate() {
		return PIDIssueDate;
	}

	public void setPIDIssueDate(String pIDIssueDate) {
		PIDIssueDate = pIDIssueDate;
	}

	public String getPIDExpiryDate() {
		return PIDExpiryDate;
	}

	public void setPIDExpiryDate(String pIDExpiryDate) {
		PIDExpiryDate = pIDExpiryDate;
	}

	public String getPIDRemarks() {
		return PIDRemarks;
	}

	public void setPIDRemarks(String pIDRemarks) {
		PIDRemarks = pIDRemarks;
	}

	public String getDateOfDereg() {
		return dateOfDereg;
	}

	public void setDateOfDereg(String dateOfDereg) {
		this.dateOfDereg = dateOfDereg;
	}

	public String getReasonOfDereg() {
		return reasonOfDereg;
	}

	public void setReasonOfDereg(String reasonOfDereg) {
		this.reasonOfDereg = reasonOfDereg;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void reset() {
		personId = RegularConstants.EMPTY_STRING;
		PIDRegSl = RegularConstants.EMPTY_STRING;
		deRegister = false;
		dateOfRegistration = RegularConstants.EMPTY_STRING;
		PIDCode = RegularConstants.EMPTY_STRING;
		PIDNumber = RegularConstants.EMPTY_STRING;
		PIDIssueDate = RegularConstants.EMPTY_STRING;
		PIDExpiryDate = RegularConstants.EMPTY_STRING;
		PIDRemarks = RegularConstants.EMPTY_STRING;
		dateOfDereg = RegularConstants.EMPTY_STRING;
		reasonOfDereg = RegularConstants.EMPTY_STRING;
		setProcessBO("patterns.config.framework.bo.cmn.epidregBO");
	}

	public void validate() {
		if (validateCIF() && validatePIDRegSl()) {
			validateDeRegister();
				if(deRegister){
				validateDateOfDereg();
				validatereasonOfDereg();
			} else {
				validateDateOfRegistration();
				validatePIDCode();
				validatePIDNumber();
				validatePIDIssueDate();
				validatePIDExpiryDate();
				validatePIDRemarks();
			}

		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("CIF_NO", personId);
				if (getAction().equals(MODIFY)) {
					formDTO.setObject("DOC_SL", PIDRegSl);
				}
				formDTO.set("PID_CODE", PIDCode);
				formDTO.set("DATE_OF_REG", dateOfRegistration);
				formDTO.set("DOCUMENT_NUMBER", PIDNumber);
				formDTO.set("DOCUMENT_ISSUE_DATE", PIDIssueDate);
				formDTO.set("DOCUMENT_EXPIRY_DATE", PIDExpiryDate);
				formDTO.set("REMARKS", PIDRemarks);
				formDTO.set("DATE_OF_DE_REG", dateOfDereg);
				formDTO.set("REASON_FOR_DE_REG", reasonOfDereg);
				if (getAction().equals(ADD)) {
					formDTO.set("PID_DE_REG", decodeBooleanToString(false));
				} else {
					formDTO.set("PID_DE_REG", decodeBooleanToString(deRegister));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("personId", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
	}

	public boolean validateCIF() {
		CommonValidator validation = new CommonValidator();
		DTObject formDTO = new DTObject();
		try {
			formDTO.set("CIF_NO", personId);
			formDTO.set(ContentManager.USER_ACTION, USAGE);
			formDTO = validateCIFNumber(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("personId", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("personId", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public DTObject validateCIFNumber(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		MasterValidator validator = new MasterValidator();
		String cifNo = inputDTO.get("CIF_NO");
		try {
			if (validator.isEmpty(cifNo)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "NAME");
			resultDTO = validator.validateCIF(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}

		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return resultDTO;
	}

	public boolean validatePIDRegSl() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("CIF_NO", personId);
			formDTO.set("DOC_SL", PIDRegSl);
			formDTO.set(ContentManager.USER_ACTION, getAction());
			if (getAction().equals(MODIFY)) {
				formDTO = validatePIDRegSl(formDTO);
				if (formDTO.get(ContentManager.ERROR) != null) {
					getErrorMap().setError("PIDRegSl", formDTO.get(ContentManager.ERROR));
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("personId", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validatePIDRegSl(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		MasterValidator validation = new MasterValidator();
		String personId = inputDTO.get("CIF_NO");
		String PIDRegSl = inputDTO.get("DOC_SL");
		try {
			if (validation.isEmpty(PIDRegSl)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			if (!validation.isValidNumber(PIDRegSl)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_NUMBER);
				return resultDTO;
			}
			inputDTO.set(ContentManager.TABLE, "CIFIDENTIFICATIONDOC");
			inputDTO.set(ContentManager.FETCH_COLUMNS, "PID_DE_REG");
			String columns[] = new String[] { personId, PIDRegSl };
			inputDTO.setObject(ContentManager.PROGRAM_KEY, columns);
			resultDTO = validation.validatePK(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
				if (RegularConstants.COLUMN_ENABLE.equals(resultDTO.get("PID_DE_REG"))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.PBS_PID_ALREADY_DEREG);
					return resultDTO;
				}
			} else if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_NOT_EXISTS);

			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateDateOfRegistration() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			java.util.Date dateInstance = commonValidator.isValidDate(dateOfRegistration);
			if (commonValidator.isEmpty(dateOfRegistration)) {
				getErrorMap().setError("dateOfRegistration", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (dateInstance == null) {
				getErrorMap().setError("dateOfRegistration", BackOfficeErrorCodes.INVALID_DATE);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("dateOfRegistration", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}

	public boolean validatePIDCode() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("PID_CODE", PIDCode);
			formDTO.set("CIF_NO", personId);
			formDTO.set("DOC_SL", PIDRegSl);
			formDTO.set("ACTION", USAGE);
			formDTO.set("MODE", getAction());
			formDTO = validatePIDCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("PIDCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("PIDCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validatePIDCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		MasterValidator validation = new MasterValidator();
		String PIDCode = inputDTO.get("PID_CODE");
		String mode = inputDTO.get("MODE");
		try {
			if (validation.isEmpty(PIDCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
			resultDTO = validation.validatePIDCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
			if (mode.equals(ADD))
				inputDTO.set("DOC_SL", "0");
			if (!validatePersonPIDExist(inputDTO)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.PBS_PID_EXISTS);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public final boolean validatePersonPIDExist(DTObject input) {
		DTObject result = new DTObject();
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		String sqlQuery = null;
		String personId = input.get("CIF_NO");
		String pid_Code = input.get("PID_CODE");
		String PIDRegSl = input.get("DOC_SL");
		try {
			sqlQuery = "SELECT COUNT(1) FROM CIFIDENTIFICATIONDOC WHERE CIF_NO = ? AND DOC_SL <> ? AND PID_CODE  = ? AND PID_DE_REG ='0' ";
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, personId);
			util.setString(2, PIDRegSl);
			util.setString(3, pid_Code);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				if (Integer.parseInt(rset.getString(1)) >= 1) {
					return false;
				} else
					return true;
			} else
				return false;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			dbContext.close();
		}
	}

	private DTObject isPIDNumberExist(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		CommonValidator validator = new CommonValidator();
		try {
			String primaryKey = inputDTO.get("CIF_NO") + RegularConstants.PK_SEPARATOR + inputDTO.get("DOC_SL");
			String dedupKey = inputDTO.get("PID_CODE") + RegularConstants.PK_SEPARATOR + inputDTO.get("DOCUMENT_NUMBER");
			DTObject formDTO = new DTObject();
			formDTO.set(ContentManager.PROGRAM_ID, "EPIDREG");
			formDTO.set(ContentManager.PURPOSE_ID, "PID_NUMBER");
			formDTO.set(ContentManager.PROGRAM_KEY, primaryKey);
			formDTO.set(ContentManager.DEDUP_KEY, dedupKey);
			resultDTO = validator.isDeDupAlreadyExist(formDTO);
			if (ContentManager.DATA_AVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				Object[] errorParam = { resultDTO.get("LINKED_TO_PK") };
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.PBS_DOCUMENT_ID_ALREADY_EXIST);
				resultDTO.setObject(ContentManager.ERROR_PARAM, errorParam);
			}

		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return resultDTO;
	}

	public boolean validatePIDNumber() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("CIF_NO", personId);
			formDTO.set("DOC_SL", PIDRegSl);
			formDTO.set("PID_CODE", PIDCode);
			formDTO.set("DOCUMENT_NUMBER", PIDNumber);
			formDTO.set("ACTION", USAGE);
			formDTO = validatePIDNumber(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("PIDNumber", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("PIDNumber", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validatePIDNumber(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		MasterValidator validation = new MasterValidator();
		String PIDNumber = inputDTO.get("DOCUMENT_NUMBER");
		try {
			if (validation.isEmpty(PIDNumber)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			if (validation.isZero(PIDNumber)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.ZERO_CHECK);
				return resultDTO;
			}
			resultDTO = isPIDNumberExist(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validatePIDIssueDate() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			boolean PIDIssDate = false;
			DTObject formDTO = new DTObject();
			formDTO.set("PID_CODE", PIDCode);
			formDTO.set("ACTION", USAGE);
			formDTO = validatePIDIssueDate(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("PIDIssueDate", formDTO.get(ContentManager.ERROR));
				return false;
			}
			if (formDTO.get(ContentManager.RESULT) == ContentManager.DATA_AVAILABLE) {
				PIDIssDate = decodeStringToBoolean(formDTO.get("DOC_HAS_ISSUE_DATE"));
			}
			if (PIDIssDate) {
				java.util.Date dateInstance = commonValidator.isValidDate(PIDIssueDate);
				if (commonValidator.isEmpty(PIDIssueDate)) {
					getErrorMap().setError("PIDIssueDate", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (dateInstance == null) {
					getErrorMap().setError("PIDIssueDate", BackOfficeErrorCodes.INVALID_DATE);
					return false;
				}
				if(commonValidator.isDateGreaterThanCBD(PIDIssueDate)){
					getErrorMap().setError("PIDIssueDate", BackOfficeErrorCodes.DATE_LECBD);
					return false;
				}
			} else {
				if (!commonValidator.isEmpty(PIDIssueDate)) {
					getErrorMap().setError("PIDIssueDate", BackOfficeErrorCodes.PBS_ISSUEDT_NR);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("PIDIssueDate", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}

	public DTObject validatePIDIssueDate(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		MasterValidator validation = new MasterValidator();
		try {
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DOC_HAS_ISSUE_DATE");
			resultDTO = validation.validatePIDCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validatePIDExpiryDate() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			boolean PIDExpDate = false;
			DTObject formDTO = new DTObject();
			formDTO.set("PID_CODE", PIDCode);
			formDTO.set("ACTION", USAGE);
			formDTO = validatePIDExpiryDate(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("PIDExpiryDate", formDTO.get(ContentManager.ERROR));
				return false;
			}
			if (formDTO.get(ContentManager.RESULT) == ContentManager.DATA_AVAILABLE) {
				PIDExpDate = decodeStringToBoolean(formDTO.get("DOC_HAS_EXPIRY_DATE"));
			}
			if (PIDExpDate) {
				
				if (commonValidator.isEmpty(PIDExpiryDate)) {
					getErrorMap().setError("PIDExpiryDate", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				java.util.Date expDate = commonValidator.isValidDate(PIDExpiryDate);
				if (commonValidator.isEmpty(PIDExpiryDate)) {
					getErrorMap().setError("PIDExpiryDate", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (expDate == null) {
					getErrorMap().setError("PIDExpiryDate", BackOfficeErrorCodes.INVALID_DATE);
					return false;
				}
				
				if(!commonValidator.isEmpty(PIDIssueDate) && commonValidator.isDateLesser(PIDExpiryDate, PIDIssueDate)){
					getErrorMap().setError("PIDExpiryDate", BackOfficeErrorCodes.PBS_DATE_GE_ISSUEDATE);
					return false;
				}
				
				if(commonValidator.isDateLesserCBD(PIDExpiryDate)){
					getErrorMap().setError("PIDExpiryDate", BackOfficeErrorCodes.DATE_GECBD);
					return false;
				}
				
			} else {
				if (!commonValidator.isEmpty(PIDExpiryDate)) {
					getErrorMap().setError("PIDExpiryDate", BackOfficeErrorCodes.PBS_EXPIRYDT_NR);
					return false;
				}
			}
			commonValidator.close();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("PIDExpiryDate", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}

	public DTObject validatePIDExpiryDate(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		MasterValidator validation = new MasterValidator();
		try {
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DOC_HAS_EXPIRY_DATE");
			resultDTO = validation.validatePIDCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validatePIDRemarks() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (commonValidator.isEmpty(PIDRemarks)) {
				getErrorMap().setError("PIDRemarks", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!commonValidator.isValidRemarks(PIDRemarks)) {
				getErrorMap().setError("PIDRemarks", BackOfficeErrorCodes.INVALID_REMARKS);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("PIDRemarks", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}

	public boolean validateDateOfDereg() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (deRegister) {
				java.util.Date dateInstance = commonValidator.isValidDate(dateOfDereg);
				if (commonValidator.isEmpty(dateOfDereg)) {
					getErrorMap().setError("dateOfDereg", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (dateInstance == null) {
					getErrorMap().setError("dateOfDereg", BackOfficeErrorCodes.INVALID_DATE);
					return false;
				}
			} else {
				if (!commonValidator.isEmpty(dateOfDereg)) {
					getErrorMap().setError("dateOfDereg", BackOfficeErrorCodes.PBS_DODREG_NR);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("dateOfDereg", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}

	public boolean validatereasonOfDereg() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (deRegister) {
				if (commonValidator.isEmpty(reasonOfDereg)) {
					getErrorMap().setError("reasonOfDereg", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!commonValidator.isValidRemarks(reasonOfDereg)) {
					getErrorMap().setError("reasonOfDereg", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			} else {
				if (!commonValidator.isEmpty(reasonOfDereg)) {
					getErrorMap().setError("reasonOfDereg", BackOfficeErrorCodes.HMS_DREG_REASON_NR);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("reasonOfDereg", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}

	public boolean validateDeRegister() {
		try {
			if (deRegister && getAction().equals(ADD)) {
				getErrorMap().setError("deRegister", BackOfficeErrorCodes.INVALID_ACTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("deRegister", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

}
