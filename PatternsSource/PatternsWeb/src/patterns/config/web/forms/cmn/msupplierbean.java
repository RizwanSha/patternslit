package patterns.config.web.forms.cmn;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.MasterValidator;
import patterns.config.web.forms.GenericFormBean;

public class msupplierbean extends GenericFormBean {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String supplierID;
	private String name;
	private String shortName;
	private String pan;
	private String ifsc;
	private boolean enabled;
	private String remarks;

	public String getSupplierID() {
		return supplierID;
	}

	public void setSupplierID(String supplierID) {
		this.supplierID = supplierID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public String getPan() {
		return pan;
	}

	public void setPan(String pan) {
		this.pan = pan;
	}

	public String getIfsc() {
		return ifsc;
	}

	public void setIfsc(String ifsc) {
		this.ifsc = ifsc;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	@Override
	public void reset() {
		supplierID = RegularConstants.EMPTY_STRING;
		name = RegularConstants.EMPTY_STRING;
		shortName = RegularConstants.EMPTY_STRING;
		pan = RegularConstants.EMPTY_STRING;
		ifsc = RegularConstants.EMPTY_STRING;
		enabled = false;
		remarks = RegularConstants.EMPTY_STRING;
		setProcessBO("patterns.config.framework.bo.cmn.msupplierBO");
	}

	@Override
	public void validate() {
		if (validateSupplierID()) {
			validateName();
			validateShortName();
			validatePan();
			validateIfsc();
			validateRemarks();
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.set("SUPPLIER_ID", supplierID);
				formDTO.set("NAME", name);
				formDTO.set("SHORT_NAME", shortName);
				formDTO.set("PAN_NO", pan);
				formDTO.set("IFSC_CODE", ifsc);
				if (getAction().equals(ADD)) {
					formDTO.set("ENABLED", decodeBooleanToString(true));
				} else {
					formDTO.set("ENABLED", decodeBooleanToString(enabled));
				}
				formDTO.set("REMARKS", remarks);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("supplierID", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
	}

	public boolean validateSupplierID() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("SUPPLIER_ID", supplierID);
			formDTO.set(ContentManager.USER_ACTION, getAction());
			formDTO = validateSupplierID(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("supplierID", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("supplierID", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateSupplierID(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		MasterValidator validation = new MasterValidator();
		String supplierID = inputDTO.get("SUPPLIER_ID");
		String action = inputDTO.get(ContentManager.USER_ACTION);
		try {
			if (validation.isEmpty(supplierID)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "NAME");
			resultDTO = validation.validateSupplierID(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (action.equals(ADD)) {
				if (ContentManager.DATA_AVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_EXISTS);
				}
			} else {
				if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_NOT_EXISTS);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateName() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(name)) {
				getErrorMap().setError("name", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidDescription(name)) {
				getErrorMap().setError("name", BackOfficeErrorCodes.INVALID_NAME);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("name", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateShortName() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(shortName)) {
				getErrorMap().setError("shortName", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidConciseDescription(shortName)) {
				getErrorMap().setError("shortName", BackOfficeErrorCodes.HMS_INVALID_SHORT_NAME);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("shortName", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validatePan() {
		CommonValidator validator = new CommonValidator();
		try {
			if (validator.isEmpty(pan)) {
				getErrorMap().setError("pan", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validator.isValidPANNumber(pan)) {
				getErrorMap().setError("pan", BackOfficeErrorCodes.INVALID_PAN);
				return false;
			}
			DTObject formDTO = new DTObject();
			formDTO.set("PROGRAM_ID", context.getProcessID());
			formDTO.set("PAN_NO", pan);
			formDTO.set("SUPPLIER_ID", supplierID);
			formDTO.set(ContentManager.USER_ACTION, USAGE);
			formDTO = validatePanNoExist(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null && !formDTO.get(ContentManager.ERROR).equals(RegularConstants.EMPTY_STRING)) {
				getErrorMap().setError("pan", formDTO.get(ContentManager.ERROR), (Object[]) formDTO.getObject(ContentManager.ERROR_PARAM));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("pan", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validatePanNoExist(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		CommonValidator validator = new CommonValidator();
		String pan = inputDTO.get("PAN_NO");

		try {
			if (validator.isEmpty(pan)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			
			String primaryKey = context.getEntityCode() + RegularConstants.PK_SEPARATOR + inputDTO.get("SUPPLIER_ID");
			String dedupKey = context.getEntityCode() + RegularConstants.PK_SEPARATOR + inputDTO.get("PAN_NO") + RegularConstants.PK_SEPARATOR + RegularConstants.ONE;
			DTObject formDTO = new DTObject();
			formDTO.set(ContentManager.PROGRAM_ID, inputDTO.get("PROGRAM_ID"));
			formDTO.set(ContentManager.PURPOSE_ID, "PANNUM");
			formDTO.set(ContentManager.PROGRAM_KEY, primaryKey);
			formDTO.set(ContentManager.DEDUP_KEY, dedupKey);
			resultDTO = validator.isDeDupAlreadyExist(formDTO);
			if (resultDTO.get(ContentManager.ERROR) != null)
				return resultDTO;
			else if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
				if (!(inputDTO.get(ContentManager.ACTION).equals(MODIFY) && inputDTO.get("SUPPLIER_ID").equals(resultDTO.get("LINKED_TO_PK").split("\\|")[1]))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.PAN_NO_ALREADY_LINKED);
					Object[] param = { resultDTO.get("LINKED_TO_PK").split("\\|")[1]};
					resultDTO.setObject(ContentManager.ERROR_PARAM, param);
					return resultDTO;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return resultDTO;
	}

	public boolean validateIfsc() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("IFSC_CODE", ifsc);
			formDTO.set(ContentManager.USER_ACTION, getAction());
			formDTO = validateIFSCCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("ifsc", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("ifsc", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateIFSCCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		MasterValidator validation = new MasterValidator();
		String ifsc = inputDTO.get("IFSC_CODE");
		try {
			if (validation.isEmpty(ifsc)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "BANK_NAME,BRANCH_NAME,CITY,STATE_CODE");
			resultDTO = validation.validateIFSCCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}

		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateRemarks() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (remarks != null && remarks.length() > 0) {
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			if (getAction().equals(MODIFY)) {
				if (commonValidator.isEmpty(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}
}
