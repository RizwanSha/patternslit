package patterns.config.web.forms.cmn;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.process.TBAActionType;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.MasterValidator;
import patterns.config.web.forms.GenericFormBean;

public class mactypeintratebean extends GenericFormBean {

	private static final long serialVersionUID = 1L;

	private String productCode;
	private String currencyCode;
	private String accountTypeCode;
	private String accountSubType;
	private String effectiveDate;
	private String normalInterest;
	private String overdueInterest;
	private String penalInterest;
	private boolean enabled;
	private String remarks;
	private String w_loans;
	private String w_deposits;
	private String w_casa;
	private String w_int_purpose;
	private String w_loan_int_purpose;

	public String getW_int_purpose() {
		return w_int_purpose;
	}

	public void setW_int_purpose(String w_int_purpose) {
		this.w_int_purpose = w_int_purpose;
	}

	public String getW_loan_int_purpose() {
		return w_loan_int_purpose;
	}

	public void setW_loan_int_purpose(String w_loan_int_purpose) {
		this.w_loan_int_purpose = w_loan_int_purpose;
	}

	public String getW_loans() {
		return w_loans;
	}

	public void setW_loans(String w_loans) {
		this.w_loans = w_loans;
	}

	public String getW_deposits() {
		return w_deposits;
	}

	public void setW_deposits(String w_deposits) {
		this.w_deposits = w_deposits;
	}

	public String getW_casa() {
		return w_casa;
	}

	public void setW_casa(String w_casa) {
		this.w_casa = w_casa;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public String getAccountTypeCode() {
		return accountTypeCode;
	}

	public void setAccountTypeCode(String accountTypeCode) {
		this.accountTypeCode = accountTypeCode;
	}

	public String getAccountSubType() {
		return accountSubType;
	}

	public void setAccountSubType(String accountSubType) {
		this.accountSubType = accountSubType;
	}

	public String getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public String getNormalInterest() {
		return normalInterest;
	}

	public void setNormalInterest(String normalInterest) {
		this.normalInterest = normalInterest;
	}

	public String getOverdueInterest() {
		return overdueInterest;
	}

	public void setOverdueInterest(String overdueInterest) {
		this.overdueInterest = overdueInterest;
	}

	public String getPenalInterest() {
		return penalInterest;
	}

	public void setPenalInterest(String penalInterest) {
		this.penalInterest = penalInterest;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public void reset() {
		productCode = RegularConstants.EMPTY_STRING;
		currencyCode = RegularConstants.EMPTY_STRING;
		accountTypeCode = RegularConstants.EMPTY_STRING;
		accountSubType = RegularConstants.EMPTY_STRING;
		effectiveDate = RegularConstants.EMPTY_STRING;
		normalInterest = RegularConstants.EMPTY_STRING;
		overdueInterest = RegularConstants.EMPTY_STRING;
		penalInterest = RegularConstants.EMPTY_STRING;
		enabled = false;
		remarks = RegularConstants.EMPTY_STRING;
		w_loans = RegularConstants.EMPTY_STRING;
		w_deposits = RegularConstants.EMPTY_STRING;
		w_casa = RegularConstants.EMPTY_STRING;
		w_int_purpose = RegularConstants.EMPTY_STRING;
		w_loan_int_purpose = RegularConstants.EMPTY_STRING;
		setProcessBO("patterns.config.framework.bo.cmn.mactypeintrateBO");
	}

	public void validate() {
		if (validateProductCode() && validateCurrencyCode() && validateAccountTypeCode() && validateAccountSubType() && validateEffectiveDate()) {
			validateNormalInterest();
			validateOverdueInterest();
			validatePenalInterest();
			validateRemarks();
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.set("PRODUCT_CODE", productCode);
				formDTO.set("ACCOUNT_TYPE_CODE", accountTypeCode);
				formDTO.set("ACCOUNT_SUBTYPE_CODE", accountSubType);
				formDTO.set("CCY_CODE", currencyCode);
				formDTO.set("EFF_DATE", effectiveDate);
				formDTO.set("INT_RATE_TYPE_CODE_NORMAL", normalInterest);
				formDTO.set("INT_RATE_TYPE_CODE_OD", overdueInterest);
				formDTO.set("INT_RATE_TYPE_CODE_PENAL", penalInterest);
				if (getAction().equals(ADD)) {
					formDTO.set("ENABLED", decodeBooleanToString(true));
				} else {
					formDTO.set("ENABLED", decodeBooleanToString(enabled));
				}
				formDTO.set("REMARKS", remarks);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("productCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
	}

	public boolean validateProductCode() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("PRODUCT_CODE", productCode);
			formDTO.set(ContentManager.ACTION, USAGE);
			formDTO = validateProductCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("productCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("productCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateProductCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		MasterValidator validation = new MasterValidator();
		String productCode = inputDTO.get("PRODUCT_CODE");
		try {
			if (validation.isEmpty(productCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "PRODUCT_CODE,PRODUCT_GROUP_CODE,DESCRIPTION");
			resultDTO = validation.validateProductCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
			String prdctCode = resultDTO.get("PRODUCT_CODE");
			String desc = resultDTO.get("DESCRIPTION");
			String productgroupCode = inputDTO.get("PRODUCT_GROUP_CODE");
			productgroupCode = resultDTO.get("PRODUCT_GROUP_CODE");
			inputDTO.set(ContentManager.FETCH_COLUMNS, "GROUP_TYPE");
			inputDTO.set("PRODUCT_GROUP_CODE", productgroupCode);
			resultDTO = validation.validateProductGroupCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
			resultDTO.set("W_LOAN", RegularConstants.ZERO);
			resultDTO.set("W_DEPOSIT", RegularConstants.ZERO);
			resultDTO.set("W_CASA", RegularConstants.ZERO);
			if (resultDTO.get("GROUP_TYPE").equals("CS"))
				resultDTO.set("W_CASA", RegularConstants.ONE);
			else if (resultDTO.get("GROUP_TYPE").equals("TD"))
				resultDTO.set("W_DEPOSIT", RegularConstants.ONE);
			else if (resultDTO.get("GROUP_TYPE").equals("LA"))
				resultDTO.set("W_LOAN", RegularConstants.ONE);
			else {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.PBS_INT_PARAM_NR_FOR_PROD);
				return resultDTO;
			}
			resultDTO.set("PRODUCT_CODE", prdctCode);
			resultDTO.set("DESCRIPTION", desc);
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateCurrencyCode() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("CCY_CODE", currencyCode);
			formDTO.set("PRODUCT_CODE", productCode);
			formDTO.set(ContentManager.ACTION, USAGE);
			formDTO = validateCurrencyCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("currencyCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("currencyCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateCurrencyCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		MasterValidator validation = new MasterValidator();
		String productCode = inputDTO.get("PRODUCT_CODE");
		String currencyCode = inputDTO.get("CCY_CODE");
		try {
			if (validation.isEmpty(currencyCode) && validation.isEmpty(productCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
			resultDTO = validation.validateCurrency(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "PRODUCT_CODE");
			inputDTO.set("PRODUCT_CODE", productCode);
			inputDTO = validation.validateProductCurrency(inputDTO);
			if (inputDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, inputDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (inputDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
			if (!currencyCode.equals(inputDTO.get("CCY_CODE"))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.PBS_CURR_NOT_ALLOW_PROD);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateAccountTypeCode() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("ACCOUNT_TYPE_CODE", accountTypeCode);
			formDTO.set(ContentManager.USER_ACTION, USAGE);
			formDTO = validateAccountTypeCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("accountTypeCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("accountTypeCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateAccountTypeCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		MasterValidator validation = new MasterValidator();
		String accountTypeCode = inputDTO.get("ACCOUNT_TYPE_CODE");
		try {
			if (validation.isEmpty(accountTypeCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
			resultDTO = validation.validateAccountTypeCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateAccountSubType() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("ACCOUNT_SUBTYPE_CODE", accountSubType);
			formDTO.set(ContentManager.USER_ACTION, USAGE);
			formDTO = validateAccountSubType(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("accountSubType", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("accountSubType", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateAccountSubType(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		MasterValidator validation = new MasterValidator();
		String accountSubType = inputDTO.get("ACCOUNT_SUBTYPE_CODE");
		try {
			if (validation.isEmpty(accountSubType)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
			resultDTO = validation.validateAccountSubTypeCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateEffectiveDate() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set(ContentManager.ACTION, getAction());
			formDTO.set("PRODUCT_CODE", productCode);
			formDTO.set("ACCOUNT_TYPE_CODE", accountTypeCode);
			formDTO.set("ACCOUNT_SUBTYPE_CODE", accountSubType);
			formDTO.set("CCY_CODE", currencyCode);
			formDTO.set("EFF_DATE", effectiveDate);
			formDTO = validateEffectiveDate(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("effectiveDate", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("effectiveDate", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateEffectiveDate(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		CommonValidator validation = new CommonValidator();
		String productCode = inputDTO.get("PRODUCT_CODE");
		String accountTypeCode = inputDTO.get("ACCOUNT_TYPE_CODE");
		String accountSubType = inputDTO.get("ACCOUNT_SUBTYPE_CODE");
		String currencyCode = inputDTO.get("CCY_CODE");
		String effectiveDate = inputDTO.get("EFF_DATE");
		String action = inputDTO.get(ContentManager.USER_ACTION);
		try {
			if (validation.isEmpty(effectiveDate)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			TBAActionType AT;
			if (action.equals(ADD))
				AT = TBAActionType.ADD;
			else
				AT = TBAActionType.MODIFY;
			if (!validation.isEffectiveDateValid(effectiveDate, AT)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_EFFECTIVE_DATE);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "EFF_DATE");
			inputDTO.set(ContentManager.TABLE, "ACTYPEINTRATEHIST");
			String columns[] = new String[] { context.getEntityCode(), productCode, accountTypeCode, accountSubType, currencyCode, effectiveDate };
			inputDTO.setObject(ContentManager.PROGRAM_KEY, columns);
			resultDTO = validation.validatePK(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (action.equals(ADD)) {
				if (ContentManager.DATA_AVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_EXISTS);
					return resultDTO;
				}
			} else if (action.equals(MODIFY)) {
				if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_NOT_EXISTS);
					return resultDTO;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateNormalInterest() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("INT_RATE_TYPE_CODE", normalInterest);
			formDTO.set("W_CASA", w_casa);
			formDTO.set("W_DEPOSIT", w_deposits);
			formDTO.set("W_LOAN", w_loans);
			formDTO.set(ContentManager.ACTION, USAGE);
			formDTO = validateNormalInterest(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("normalInterest", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("normalInterest", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateNormalInterest(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		MasterValidator validation = new MasterValidator();
		String normalInterest = inputDTO.get("INT_RATE_TYPE_CODE");
		String w_casa = inputDTO.get("W_CASA");
		String w_deposits = inputDTO.get("W_DEPOSIT");
		String w_loans = inputDTO.get("W_LOAN");
		try {
			if (validation.isEmpty(normalInterest)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION,INT_RATE_PURPOSE,LOAN_INT_RATE_PURPOSE");
			resultDTO = validation.validateInterestRateTypeCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
			if (w_loans.equals(RegularConstants.ONE) && resultDTO.get("INT_RATE_PURPOSE").equals("L") && !resultDTO.get("LOAN_INT_RATE_PURPOSE").equals("N")) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.PBS_INT_TYPE_NA_NORMAL);
				return resultDTO;
			}
			if ((w_casa.equals(RegularConstants.ONE) && (resultDTO.get("INT_RATE_PURPOSE").equals("G") || resultDTO.get("INT_RATE_PURPOSE").equals("C")))) {
				return resultDTO;
			} else if ((w_deposits.equals(RegularConstants.ONE) && (resultDTO.get("INT_RATE_PURPOSE").equals("G") || resultDTO.get("INT_RATE_PURPOSE").equals("D")))) {
				return resultDTO;
			} else if ((w_loans.equals(RegularConstants.ONE) && (resultDTO.get("INT_RATE_PURPOSE").equals("G") || resultDTO.get("INT_RATE_PURPOSE").equals("L")))) {
				return resultDTO;
			} else {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.PBS_INT_TYPE_NA_PROD_TYPE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateOverdueInterest() {
		try {
			if (w_loans.equals(RegularConstants.ONE)) {
				DTObject formDTO = new DTObject();
				formDTO.set("INT_RATE_TYPE_CODE", overdueInterest);
				formDTO.set(ContentManager.ACTION, USAGE);
				formDTO = validateOverdueInterest(formDTO);
				if (formDTO.get(ContentManager.ERROR) != null) {
					getErrorMap().setError("overdueInterest", formDTO.get(ContentManager.ERROR));
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("overdueInterest", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateOverdueInterest(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		MasterValidator validation = new MasterValidator();
		String overdueInterest = inputDTO.get("INT_RATE_TYPE_CODE");
		try {
			if (validation.isEmpty(overdueInterest)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION,INT_RATE_PURPOSE,LOAN_INT_RATE_PURPOSE");
			resultDTO = validation.validateInterestRateTypeCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
			if (resultDTO.get("INT_RATE_PURPOSE").equals("L")) {
				if (!resultDTO.get("LOAN_INT_RATE_PURPOSE").equals("O")) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.PBS_INT_TYPE_NA_OVERDUE);
					return resultDTO;
				}
			} else {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.PBS_INT_TYPE_NA_PROD_TYPE);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validatePenalInterest() {
		try {
			if (w_loans.equals(RegularConstants.ONE)) {
				DTObject formDTO = new DTObject();
				formDTO.set("INT_RATE_TYPE_CODE", penalInterest);
				formDTO.set(ContentManager.ACTION, USAGE);
				formDTO = validatePenalInterest(formDTO);
				if (formDTO.get(ContentManager.ERROR) != null) {
					getErrorMap().setError("penalInterest", formDTO.get(ContentManager.ERROR));
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("penalInterest", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validatePenalInterest(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		MasterValidator validation = new MasterValidator();
		String penalInterest = inputDTO.get("INT_RATE_TYPE_CODE");
		try {
			if (validation.isEmpty(penalInterest)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION,INT_RATE_PURPOSE,LOAN_INT_RATE_PURPOSE");
			resultDTO = validation.validateInterestRateTypeCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
			if (resultDTO.get("INT_RATE_PURPOSE").equals("L")) {
				if (!resultDTO.get("LOAN_INT_RATE_PURPOSE").equals("P")) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.PBS_INT_TYPE_NA_OVERDUE);
					return resultDTO;
				}
			} else {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.PBS_INT_TYPE_NA_PROD_TYPE);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateRemarks() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (remarks != null && remarks.length() > 0) {
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			if (getAction().equals(MODIFY)) {
				if (commonValidator.isEmpty(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}
}
