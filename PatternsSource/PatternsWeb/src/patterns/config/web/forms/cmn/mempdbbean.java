package patterns.config.web.forms.cmn;

import java.sql.ResultSet;
/**
 * This program will be used to maintain Employee Details and to test about
 * employee PF number and Esi number eligible for current employee Code. .
 * 
 * 
 * @version 1.0
 * @author TULASI THOTA
 * @since 26-Nov-2014 <br>
 *        <b>Modified History</b> <BR>
 *        <U><B> Sl.No Modified Date Author Modified Changes Version 1.
 *        28-Nov-2014 Jainudeen Added three methods in PayRollValidator 1.o 1.0.
 *        Method names are isPFNumberUsable,isESINumberUsable,isPersonIDUsable
 *        02-DESC -2014 Naveen told me to create two error codes. There are
 *        1)HMS00101-HMS_ESI_NO_REQ=ESI Number needs to be specified
 *        2)HMS00102-HMS_PF_NO_REQ=PF Number needs to be specified 3)03-02-2016
 *        Naresh 3 fields added </B></U> <br>
 * @15-07-2016
 * Person Id Field Removed and Person Daetils capturing in screen..  (@Modified author Pavan Kumar R)
 *        
 *        
 *        
 */
import java.util.ArrayList;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.process.TBAActionType;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.GenericOption;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.MasterValidator;
import patterns.config.web.forms.GenericFormBean;

public class mempdbbean extends GenericFormBean {
	private static final long serialVersionUID = 1L;
	private String employeeCode;
	private String employee;
	private String employee_title;
	private String dateOfBirth;
	private String gender;
	private String relationShip;
	private String relationName;
	private String dateOfJoining;
	private String dateOfRetirement;
	private boolean provideFundApplicable;
	private String pfNumber;
	private boolean personFundRequired;
	private boolean eligibleEsi;
	private String esiNumber;
	private String qualification;
	private String emailId;
	private String mobileNumber;
	private String remarks;
	String titleUsage = RegularConstants.EMPTY_STRING;
	String prevDateOfJoin = RegularConstants.EMPTY_STRING;

	public String getEmployeeCode() {
		return employeeCode;
	}

	public void setEmployeeCode(String employeeCode) {
		this.employeeCode = employeeCode;
	}

	public String getEmployee() {
		return employee;
	}

	public void setEmployee(String employee) {
		this.employee = employee;
	}

	public String getEmployee_title() {
		return employee_title;
	}

	public void setEmployee_title(String employee_title) {
		this.employee_title = employee_title;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getRelationShip() {
		return relationShip;
	}

	public void setRelationShip(String relationShip) {
		this.relationShip = relationShip;
	}

	public String getRelationName() {
		return relationName;
	}

	public void setRelationName(String relationName) {
		this.relationName = relationName;
	}

	public String getDateOfJoining() {
		return dateOfJoining;
	}

	public void setDateOfJoining(String dateOfJoining) {
		this.dateOfJoining = dateOfJoining;
	}

	public String getDateOfRetirement() {
		return dateOfRetirement;
	}

	public void setDateOfRetirement(String dateOfRetirement) {
		this.dateOfRetirement = dateOfRetirement;
	}

	public boolean isProvideFundApplicable() {
		return provideFundApplicable;
	}

	public void setProvideFundApplicable(boolean provideFundApplicable) {
		this.provideFundApplicable = provideFundApplicable;
	}

	public String getPfNumber() {
		return pfNumber;
	}

	public void setPfNumber(String pfNumber) {
		this.pfNumber = pfNumber;
	}

	public boolean isPersonFundRequired() {
		return personFundRequired;
	}

	public void setPersonFundRequired(boolean personFundRequired) {
		this.personFundRequired = personFundRequired;
	}

	public boolean isEligibleEsi() {
		return eligibleEsi;
	}

	public void setEligibleEsi(boolean eligibleEsi) {
		this.eligibleEsi = eligibleEsi;
	}

	public String getEsiNumber() {
		return esiNumber;
	}

	public void setEsiNumber(String esiNumber) {
		this.esiNumber = esiNumber;
	}

	public String getQualification() {
		return qualification;
	}

	public void setQualification(String qualification) {
		this.qualification = qualification;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void reset() {
		employeeCode = RegularConstants.EMPTY_STRING;
		employee_title = RegularConstants.EMPTY_STRING;
		employee = RegularConstants.EMPTY_STRING;
		gender = RegularConstants.EMPTY_STRING;
		dateOfBirth = RegularConstants.EMPTY_STRING;
		relationShip = RegularConstants.EMPTY_STRING;
		relationName = RegularConstants.EMPTY_STRING;
		dateOfJoining = RegularConstants.EMPTY_STRING;
		dateOfRetirement = RegularConstants.EMPTY_STRING;
		provideFundApplicable = false;
		pfNumber = RegularConstants.EMPTY_STRING;
		personFundRequired = false;
		eligibleEsi = false;
		esiNumber = RegularConstants.EMPTY_STRING;
		qualification = RegularConstants.EMPTY_STRING;
		emailId = RegularConstants.EMPTY_STRING;
		mobileNumber = RegularConstants.EMPTY_STRING;
		remarks = RegularConstants.EMPTY_STRING;

		DBContext dbContext = new DBContext();
		ArrayList<GenericOption> gender = null;
		gender = getGenericOptions("COMMON", "GENDER", dbContext, "--");
		webContext.getRequest().setAttribute("COMMON_GENDER", gender);
		ArrayList<GenericOption> relationShip = null;
		relationShip = getGenericOptions("COMMON", "RELATION_SHIP", dbContext, "--");
		webContext.getRequest().setAttribute("COMMON_RELATION_SHIP", relationShip);

		ArrayList<GenericOption> title = null;
		title = getTitle("COMMON", "TITLE", dbContext, "--");
		webContext.getRequest().setAttribute("COMMON_TITLE", title);

		dbContext.close();
		setProcessBO("patterns.config.framework.bo.cmn.mempdbBO");
	}

	public static final ArrayList<GenericOption> getTitle(String programID, String tokenID, DBContext dbContext, String blankString) {
		ArrayList<GenericOption> genericList = new ArrayList<GenericOption>();
		int counter = 0;
		if (blankString != null) {
			genericList.add(counter, new GenericOption(ContentManager.EMPTY_STRING, blankString));
			++counter;
		}
		String sqlQuery = "SELECT TITLE_CODE FROM TITLE WHERE TITLE_CHILDREN_USG='0' AND ISNULL(TITLE_OCCU) AND ENABLED='1' ";
		DBUtil util = dbContext.createUtilInstance();
		try {
			util.setMode(DBUtil.PREPARED);
			util.setSql(sqlQuery);
			ResultSet rs = util.executeQuery();
			while (rs.next()) {
				genericList.add(counter, new GenericOption(rs.getString(1), rs.getString(1)));
				++counter;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			util.reset();
		}
		return genericList;
	}

	@Override
	public void validate() {
		if (validateEmpCode()) {
			validateEmployeeTitle();
			validateEmployeeName();
			validateGender();
			validatedateofBirth();
			validateRelationShip();
			validateRelationName();
			validateDateOfJoining();
			validateDateOfRetirement();
			validatePfNumber();
			validateEsiNumber();
			validateQualification();
			validateEmailId();
			validateMobileNumber();
			validateRemarks();
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.set("EMP_CODE", employeeCode);
				formDTO.set("TITLE_CODE", employee_title);
				formDTO.set("PERSON_NAME", employee);
				formDTO.set("GENDER", gender);
				formDTO.set("DATE_OF_BIRTH", dateOfBirth);
				formDTO.set("FATHER_HUSBAND_FLAG", relationShip);
				formDTO.set("FATHER_OR_HUSBAND_NAME", relationName);
				formDTO.set("DATE_OF_JOIN", dateOfJoining);
				formDTO.set("RETIREMENT_DATE", dateOfRetirement);
				formDTO.set("PF_APPLICABLE", decodeBooleanToString(provideFundApplicable));
				if (decodeBooleanToString(provideFundApplicable).equals("1"))
					formDTO.set("PF_NUMBER", pfNumber);
				else
					formDTO.set("PF_NUMBER", RegularConstants.NULL);
				formDTO.set("PENSION_FUND_REQD", decodeBooleanToString(personFundRequired));
				formDTO.set("ELIGIBLE_FOR_ESI", decodeBooleanToString(eligibleEsi));
				if (decodeBooleanToString(eligibleEsi).equals("1"))
					formDTO.set("EMP_ESI_NO", esiNumber);
				else
					formDTO.set("EMP_ESI_NO", RegularConstants.NULL);
				formDTO.set("EMP_QUALIFICATION", qualification);
				formDTO.set("EMAIL_ID", emailId);
				formDTO.set("MOBILE_NO", mobileNumber);
				formDTO.set("REMARKS", remarks);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("employeeCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
		}
	}

	private boolean validateEmpCode() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("EMP_CODE", employeeCode);
			formDTO.set("ACTION", getAction());
			formDTO = validateEmployeeCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("employeeCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("employeeCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateEmployeeCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		MasterValidator validation = new MasterValidator();
		String empCode = inputDTO.get("EMP_CODE");
		String action = inputDTO.get("ACTION");
		try {
			if (validation.isEmpty(empCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "TO_CHAR(DATE_OF_JOIN,'" + context.getDateFormat() + "') DATE_OF_JOIN");
			resultDTO = validation.validateEmployeeCode(inputDTO);
			if (action.equals(ADD)) {
				if (ContentManager.DATA_AVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_EXISTS);
				}
			} else {
				if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_NOT_EXISTS);
				} else {
					prevDateOfJoin = resultDTO.get("DATE_OF_JOIN");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateEmployeeTitle() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(employee_title)) {
				getErrorMap().setError("employee", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			} else {
				DTObject formDTO = new DTObject();
				formDTO.set("TITLE_CODE", employee_title);
				formDTO = validateEmployeeTitle(formDTO);
				if (formDTO.get(ContentManager.ERROR) != null) {
					getErrorMap().setError("employee", formDTO.get(ContentManager.ERROR));
					return false;
				}
				titleUsage = formDTO.get("TITLE_USAGE");
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("employee", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return true;
	}

	public DTObject validateEmployeeTitle(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		MasterValidator validator = new MasterValidator();
		titleUsage = RegularConstants.EMPTY_STRING;
		try {
			inputDTO.set(ContentManager.FETCH_COLUMNS, "TITLE_USAGE,TITLE_CHILDREN_USG");
			inputDTO.set(ContentManager.ACTION, USAGE);
			inputDTO.set(ContentManager.CODE, inputDTO.get("TITLE_CODE"));
			inputDTO.set(ContentManager.TABLE, "TITLE");
			inputDTO.set(ContentManager.COLUMN, "TITLE_CODE");
			inputDTO.set(ContentManager.ENTITY_CODE, context.getEntityCode());
			resultDTO = validator.validateEntityWiseCodes(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_NOT_EXISTS);
				return resultDTO;
			}
			if (ContentManager.DATA_AVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				if (RegularConstants.ONE.equals(resultDTO.get("TITLE_CHILDREN_USG"))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.PBS_TITLE_CANT_BE_USED);
					return resultDTO;
				}
			}
			return resultDTO;
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return resultDTO;
		} finally {
			validator.close();
		}
	}

	public boolean validateEmployeeName() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(employee)) {
				getErrorMap().setError("employee", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			} else if (!validation.isValidName(employee)) {
				getErrorMap().setError("employee", BackOfficeErrorCodes.INVALID_NAME);
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("employee", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return true;
	}

	public boolean validateGender() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(gender)) {
				getErrorMap().setError("gender", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			} else if (!validation.isCMLOVREC("COMMON", "GENDER", gender)) {
				getErrorMap().setError("gender", BackOfficeErrorCodes.INVALID_OPTION);
				return false;
			} else if (titleUsage.equals("2")) {
				if (gender.equals("F")) {
					getErrorMap().setError("gender", BackOfficeErrorCodes.PBS_GENDER_CANT_BE_USED);
					return false;
				}
			} else if (titleUsage.equals("3")) {
				if (gender.equals("M")) {
					getErrorMap().setError("gender", BackOfficeErrorCodes.PBS_GENDER_CANT_BE_USED);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("gender", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validatedateofBirth() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(dateOfBirth)) {
				getErrorMap().setError("dateOfBirth", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			java.util.Date dateInstance = validation.isValidDate(dateOfBirth);
			if (dateInstance == null) {
				getErrorMap().setError("dateOfBirth", BackOfficeErrorCodes.INVALID_DATE);
				return false;
			} else if (!validation.isDateLesserCBD(dateOfBirth)) {
				getErrorMap().setError("dateOfBirth", BackOfficeErrorCodes.DATE_LCBD);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("dateOfBirth", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;

	}

	public boolean validateRelationShip() {
		CommonValidator validator = new CommonValidator();
		try {
			if (validator.isEmpty(relationShip)) {
				getErrorMap().setError("relationShip", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validator.isCMLOVREC("COMMON", "RELATION_SHIP", relationShip)) {
				getErrorMap().setError("relationShip", BackOfficeErrorCodes.INVALID_OPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("relationShip", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return false;
	}

	public boolean validateRelationName() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(relationName)) {
				getErrorMap().setError("relationName", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			} else if (!validation.isValidName(relationName)) {
				getErrorMap().setError("relationName", BackOfficeErrorCodes.INVALID_NAME);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("relationName", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateDateOfJoining() {
		CommonValidator validation = new CommonValidator();
		try {
			TBAActionType AT;
			if (getAction().equals(ADD))
				AT = TBAActionType.ADD;
			else
				AT = TBAActionType.MODIFY;
			if (validation.isEmpty(dateOfJoining)) {
				getErrorMap().setError("dateOfJoining", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			java.util.Date dateInstance = validation.isValidDate(dateOfJoining);
			if (dateInstance == null) {
				getErrorMap().setError("dateOfJoining", BackOfficeErrorCodes.INVALID_DATE);
				return false;
			}
			if (getAction().equals(ADD) || (getAction().equals(MODIFY) && !dateOfJoining.equals(prevDateOfJoin))) {
				if (!validation.isEffectiveDateValid(dateOfJoining, AT)) {
					getErrorMap().setError("dateOfJoining", BackOfficeErrorCodes.DATE_GECBD);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("dateOfJoining", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateDateOfRetirement() {
		CommonValidator validation = new CommonValidator();
		try {
			if (!validation.isEmpty(dateOfRetirement)) {
				java.util.Date dateInstance = validation.isValidDate(dateOfBirth);
				if (dateInstance == null) {
					getErrorMap().setError("dateOfRetirement", BackOfficeErrorCodes.INVALID_DATE);
					return false;
				} else if (validation.isDateLesser(dateOfRetirement, dateOfRetirement)) {
					getErrorMap().setError("dateOfRetirement", BackOfficeErrorCodes.DATE_LCBD);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("dateOfRetirement", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;

	}

	private boolean validateEsiNumber() {
		DTObject formDTO = new DTObject();
		DTObject resultDTO = new DTObject();
		CommonValidator validation = new CommonValidator();
		try {
			if (!eligibleEsi && validation.isEmpty(esiNumber)) {
				return true;
			}
			if (!eligibleEsi && !validation.isEmpty(esiNumber)) {
				getErrorMap().setError("esiNumber", BackOfficeErrorCodes.PBS_EMP_NO_ESI);
				return false;
			}
			if (eligibleEsi && validation.isEmpty(esiNumber)) {
				getErrorMap().setError("esiNumber", BackOfficeErrorCodes.PBS_ESI_NO_REQ);
				return false;

			}
			formDTO.set("EMP_ESI_NO", esiNumber);
			formDTO.set("EMP_CODE", employeeCode);
			resultDTO = isESINumberUsable(formDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("esiNumber", resultDTO.get(ContentManager.ERROR));
				return false;
			}

			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("esiNumber", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public DTObject isESINumberUsable(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		MasterValidator validator = new MasterValidator();
		try {
			if (!validator.isESINumberUsable(inputDTO)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.PBS_EMP_DUP_ESI);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return resultDTO;
	}

	private boolean validatePfNumber() {
		DTObject formDTO = new DTObject();
		DTObject resultDTO = new DTObject();
		CommonValidator validation = new CommonValidator();
		try {
			if (!provideFundApplicable && validation.isEmpty(pfNumber)) {
				return true;
			}
			if (!provideFundApplicable && !validation.isEmpty(pfNumber)) {
				getErrorMap().setError("pfNumber", BackOfficeErrorCodes.PBS_EMP_NO_PF);
				return false;
			}
			if (provideFundApplicable && validation.isEmpty(pfNumber)) {
				getErrorMap().setError("pfNumber", BackOfficeErrorCodes.PBS_PF_NO_REQ);
				return false;
			}
			formDTO.set("PF_NUMBER", pfNumber);
			formDTO.set("EMP_CODE", employeeCode);
			resultDTO = isPFNumberUsable(formDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("pfNumber", resultDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("pfNumber", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public DTObject isPFNumberUsable(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		MasterValidator validation = new MasterValidator();
		try {
			if (!validation.isPFNumberUsable(inputDTO)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.PBS_EMP_DUP_PF);
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	private boolean validateQualification() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(qualification)) {
				getErrorMap().setError("qualification", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			} else if (!validation.isValidDescription(qualification)) {
				getErrorMap().setError("qualification", BackOfficeErrorCodes.INVALID_VALUE);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("qualification", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateEmailId() {
		CommonValidator validation = new CommonValidator();
		try {
			if (!validation.isEmpty(emailId) && !validation.isValidEmail(emailId)) {
				getErrorMap().setError("emailId", BackOfficeErrorCodes.INVALID_EMAIL_ID);
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("emailId", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return true;
	}

	public boolean validateMobileNumber() {
		CommonValidator validation = new CommonValidator();
		try {
			if (!validation.isEmpty(mobileNumber) && !validation.isValidPhoneNumber(mobileNumber)) {
				getErrorMap().setError("mobileNumber", BackOfficeErrorCodes.INVALID_MOBILE_NUMBER);
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("mobileNumber", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return true;
	}

	private boolean validateRemarks() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (remarks != null && remarks.length() > 0) {
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			if (getAction().equals(MODIFY)) {
				if (commonValidator.isEmpty(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}
}
