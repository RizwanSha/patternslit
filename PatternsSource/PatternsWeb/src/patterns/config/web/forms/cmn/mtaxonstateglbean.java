package patterns.config.web.forms.cmn;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.BaseDomainValidator;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.FASValidator;
import patterns.config.validations.MasterValidator;
import patterns.config.web.forms.GenericFormBean;

public class mtaxonstateglbean extends GenericFormBean {

	/**
	 * 
	 * 
	 * @version 1.0
	 * @author Kalimuthu U 
	 * @since 27-Feb-2017 <br>
	 *        <b>MTAXONSTATEGL</b> <BR>
	 *        <U><B>Tax on State GL Configuration </B></U> <br>
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String stateCode;
	private String taxCode;
	private String taxonStateCode;
	private String taxPayGLAcctHead;
	private boolean enabled;
	private String remarks;

	public String getStateCode() {
		return stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	public String getTaxCode() {
		return taxCode;
	}

	public void setTaxCode(String taxCode) {
		this.taxCode = taxCode;
	}

	public String getTaxonStateCode() {
		return taxonStateCode;
	}

	public void setTaxonStateCode(String taxonStateCode) {
		this.taxonStateCode = taxonStateCode;
	}

	public String getTaxPayGLAcctHead() {
		return taxPayGLAcctHead;
	}

	public void setTaxPayGLAcctHead(String taxPayGLAcctHead) {
		this.taxPayGLAcctHead = taxPayGLAcctHead;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void reset() {
		stateCode = RegularConstants.EMPTY_STRING;
		taxCode = RegularConstants.EMPTY_STRING;
		taxonStateCode = RegularConstants.EMPTY_STRING;
		taxPayGLAcctHead = RegularConstants.EMPTY_STRING;
		enabled = false;
		remarks = RegularConstants.EMPTY_STRING;
		setProcessBO("patterns.config.framework.bo.cmn.mtaxonstateglBO");
	}

	public void validate() {
		if (validateStateCode() && validateTaxCode() && validateTaxOnStateCode()) {
			validateTaxPayGLAcctHead();
			validateRemarks();
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.set("STATE_CODE", stateCode); 
				formDTO.set("TAX_CODE", taxCode);
				formDTO.set("TAX_ON_STATE_CODE", taxonStateCode);
				formDTO.set("TAX_PAY_GL_HEAD_CODE", taxPayGLAcctHead);
				if (getAction().equals(ADD)) {
					formDTO.set("ENABLED", decodeBooleanToString(true));
				} else {
					formDTO.set("ENABLED", decodeBooleanToString(enabled));
				}
				formDTO.set("REMARKS", remarks);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("stateCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
	}

	public boolean validateStateCode() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set(ContentManager.ACTION, USAGE);
			formDTO = validateStateCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("stateCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("stateCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateStateCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		DTObject resultDTO1 = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		BaseDomainValidator validation = new BaseDomainValidator();
		MasterValidator validatior = new MasterValidator();
		try {
			inputDTO.set(ContentManager.FETCH_COLUMNS, "CENTRAL_CODE");
			resultDTO = validation.getINSTALL(inputDTO);
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
			inputDTO.set("STATE_CODE", resultDTO.get("CENTRAL_CODE"));
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
			resultDTO1 = validatior.validateCentralAndStateCode(inputDTO);
			if (resultDTO1.get(ContentManager.ERROR) != null) {
				resultDTO1.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO1;
			}
			if (resultDTO1.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO1.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO1;
			}
			resultDTO.set("CENTRAL_CODE", resultDTO.get("CENTRAL_CODE"));
			resultDTO.set("DESCRIPTION", resultDTO1.get("DESCRIPTION"));
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
			validatior.close();
		}
		return resultDTO;
	}

	public boolean validateTaxCode() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("STATE_CODE", stateCode);
			formDTO.set("TAX_CODE", taxCode);
			formDTO.set(ContentManager.ACTION, USAGE);
			formDTO = validateTaxCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("taxCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("taxCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateTaxCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		MasterValidator validation = new MasterValidator();
		String taxCode = inputDTO.get("TAX_CODE");
		try {
			if (validation.isEmpty(taxCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
			resultDTO = validation.validateTax(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateTaxOnStateCode() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("STATE_CODE", stateCode);
			formDTO.set("TAX_CODE", taxCode);
			formDTO.set("TAX_ON_STATE_CODE", taxonStateCode);
			formDTO.set(ContentManager.ACTION, getAction());
			formDTO = validateTaxOnStateCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("taxonStateCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("taxonStateCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateTaxOnStateCode(DTObject inputDTO) {
		DTObject resultDTO1 = new DTObject();
		DTObject resultDTO = new DTObject();
		DTObject cenAndStateCodeInputDTO = new DTObject();
		MasterValidator validation = new MasterValidator();
		String stateCode = inputDTO.get("STATE_CODE");
		String taxOnStateCode = inputDTO.get("TAX_ON_STATE_CODE");
		String action = inputDTO.get(ContentManager.ACTION);
		try {
			if (validation.isEmpty(taxOnStateCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			if (taxOnStateCode.equals(stateCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.STATE_CODE_NOT_EQUAL);
				return resultDTO;
			} else {
				cenAndStateCodeInputDTO.set("STATE_CODE", taxOnStateCode);
				cenAndStateCodeInputDTO.set(ContentManager.USER_ACTION, USAGE);
				cenAndStateCodeInputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
				resultDTO = validation.validateCentralAndStateCode(cenAndStateCodeInputDTO);
				if (resultDTO.get(ContentManager.ERROR) != null) {
					resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
					return resultDTO;
				}
				if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
					return resultDTO;
				}
				inputDTO.set(ContentManager.FETCH_ALL, ContentManager.FETCH);
				resultDTO1 = validation.validateTaxOnStateCode(inputDTO);
				if (resultDTO1.get(ContentManager.ERROR) != null) {
					return resultDTO1;
				}
				if (action.equals(ADD)) {
					if (resultDTO1.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
						resultDTO1.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_EXISTS);
						return resultDTO1;
					}
				} else {
					if (resultDTO1.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
						resultDTO1.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_NOT_EXISTS);
						return resultDTO1;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateTaxPayGLAcctHead() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("GL_HEAD_CODE", taxPayGLAcctHead);
			formDTO.set(ContentManager.USER_ACTION, USAGE);
			formDTO = validateTaxPayGLAcctHead(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("taxPayGLAcctHead", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("taxPayGLAcctHead", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateTaxPayGLAcctHead(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		FASValidator validation = new FASValidator();
		String taxPayGLAcctHead = inputDTO.get("GL_HEAD_CODE");
		try {
			if (validation.isEmpty(taxPayGLAcctHead)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION,ENABLED");
			resultDTO = validation.validateGLHeadCode(inputDTO);
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
			if (resultDTO.get("ENABLED").equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.CODE_CLOSED);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateRemarks() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (remarks != null && remarks.length() > 0) {
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			if (getAction().equals(MODIFY)) {
				if (commonValidator.isEmpty(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}

}