package patterns.config.web.forms.cmn;

import java.sql.ResultSet;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.service.DTDObject;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.LeaseValidator;
import patterns.config.validations.MasterValidator;
import patterns.config.validations.RegistrationValidator;
import patterns.config.web.forms.GenericFormBean;

public class mindassetbean extends GenericFormBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String leaseCode;
	private String agreeNo;
	private String scheduleId;
	private String assetSl;
	private String compSl;
	private String qtySl;
	private String qtyOfAsset;
	private String indAssetId;
	private String fieldId;
	private String fieldValue;
	private String fieldRemarks;
	private String xmlGrid;
	private boolean enabled;
	private String remarks;
	private String assetQty;
	private String custIDHidden;
	private String assetSerialAssetType;

	public String getAssetSerialAssetType() {
		return assetSerialAssetType;
	}

	public void setAssetSerialAssetType(String assetSerialAssetType) {
		this.assetSerialAssetType = assetSerialAssetType;
	}

	public String getAssetQty() {
		return assetQty;
	}

	public void setAssetQty(String assetQty) {
		this.assetQty = assetQty;
	}

	public String getCustIDHidden() {
		return custIDHidden;
	}

	public void setCustIDHidden(String custIDHidden) {
		this.custIDHidden = custIDHidden;
	}

	public String getLeaseCode() {
		return leaseCode;
	}

	public void setLeaseCode(String leaseCode) {
		this.leaseCode = leaseCode;
	}

	public String getAgreeNo() {
		return agreeNo;
	}

	public void setAgreeNo(String agreeNo) {
		this.agreeNo = agreeNo;
	}

	public String getScheduleId() {
		return scheduleId;
	}

	public void setScheduleId(String scheduleId) {
		this.scheduleId = scheduleId;
	}

	public String getAssetSl() {
		return assetSl;
	}

	public void setAssetSl(String assetSl) {
		this.assetSl = assetSl;
	}

	public String getCompSl() {
		return compSl;
	}

	public void setCompSl(String compSl) {
		this.compSl = compSl;
	}

	public String getQtySl() {
		return qtySl;
	}

	public void setQtySl(String qtySl) {
		this.qtySl = qtySl;
	}

	public String getQtyOfAsset() {
		return qtyOfAsset;
	}

	public void setQtyOfAsset(String qtyOfAsset) {
		this.qtyOfAsset = qtyOfAsset;
	}

	public String getIndAssetId() {
		return indAssetId;
	}

	public void setIndAssetId(String indAssetId) {
		this.indAssetId = indAssetId;
	}

	public String getFieldId() {
		return fieldId;
	}

	public void setFieldId(String fieldId) {
		this.fieldId = fieldId;
	}

	public String getFieldValue() {
		return fieldValue;
	}

	public void setFieldValue(String fieldValue) {
		this.fieldValue = fieldValue;
	}

	public String getFieldRemarks() {
		return fieldRemarks;
	}

	public void setFieldRemarks(String fieldRemarks) {
		this.fieldRemarks = fieldRemarks;
	}

	public String getXmlGrid() {
		return xmlGrid;
	}

	public void setXmlGrid(String xmlGrid) {
		this.xmlGrid = xmlGrid;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public void reset() {
		// TODO Auto-generated method stub
		leaseCode = RegularConstants.EMPTY_STRING;
		agreeNo = RegularConstants.EMPTY_STRING;
		scheduleId = RegularConstants.EMPTY_STRING;
		assetSl = RegularConstants.EMPTY_STRING;
		compSl = RegularConstants.EMPTY_STRING;
		qtySl = RegularConstants.EMPTY_STRING;
		qtyOfAsset = RegularConstants.EMPTY_STRING;
		indAssetId = RegularConstants.EMPTY_STRING;
		fieldId = RegularConstants.EMPTY_STRING;
		fieldValue = RegularConstants.EMPTY_STRING;
		fieldRemarks = RegularConstants.EMPTY_STRING;
		enabled = false;
		remarks = RegularConstants.EMPTY_STRING;
		setProcessBO("patterns.config.framework.bo.cmn.mindassetBO");
	}

	@Override
	public void validate() {
		if (validateLeaseCode() && validateAgreementNo() && validateScheduleID() && validateAssetSerial() && validateCompSerial() && validateQtySerial()) {
			// TODO Auto-generated method stub
			validateIndAssetID();
			validateGrid();
			validateRemarks();
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.set("LESSEE_CODE", leaseCode);
				formDTO.set("AGREEMENT_NO", agreeNo);
				formDTO.set("SCHEDULE_ID", scheduleId);
				formDTO.set("ASSET_SL", assetSl);
				formDTO.set("ASSET_COMP_SL", compSl);
				formDTO.set("ASSET_QTY_SL", qtySl);
				formDTO.set("INDIVIDUAL_ASSET_ID", indAssetId);
				if (getAction().equals(ADD)) {
					formDTO.set("ENABLED", decodeBooleanToString(true));
				} else {
					formDTO.set("ENABLED", decodeBooleanToString(enabled));
				}
				formDTO.set("REMARKS", remarks);
				DTDObject dtdObject = new DTDObject();
				dtdObject.addColumn(0, "SELECT");
				dtdObject.addColumn(1, "FIELD_ID");
				dtdObject.addColumn(2, "DESCRIPTION");
				dtdObject.addColumn(3, "FIELD_VALUE");
				dtdObject.addColumn(4, "FIELD_TYPE");
				dtdObject.addColumn(5, "FIELD_DECIMAL");
				dtdObject.addColumn(6, "FIELD_SIZE");
				dtdObject.setXML(xmlGrid);
				formDTO.setDTDObject("MINDASSETDTL", dtdObject);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("leaseCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
	}

	public boolean validateLeaseCode() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(leaseCode)) {
				getErrorMap().setError("leaseCode", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			DTObject formDTO = getFormDTO();
			formDTO.set("SUNDRY_DB_AC", leaseCode);
			formDTO.set(ContentManager.ACTION, USAGE);
			formDTO = validateLeaseCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("leaseCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("leaseCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public DTObject validateLeaseCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		MasterValidator validation = new MasterValidator();
		RegistrationValidator validatior = new RegistrationValidator();
		DBContext dbContext = new DBContext();
		try {
			inputDTO.set(ContentManager.FETCH_COLUMNS, "SUNDRY_DB_AC");
			resultDTO = getCustomerLesseeCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
			custIDHidden = resultDTO.get("CUSTOMER_ID");
			inputDTO.set("CUSTOMER_ID", custIDHidden);
			inputDTO.set(ContentManager.FETCH_COLUMNS, "CUSTOMER_ID,CUSTOMER_NAME");
			resultDTO = validatior.valildateCustomerID(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			} else if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
				resultDTO.set("CUSTOMER_ID", custIDHidden);
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
			validatior.close();
			dbContext.close();
		}
		return resultDTO;
	}

	public DTObject getCustomerLesseeCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		try {
			String sqlQuery = "SELECT CUSTOMER_ID FROM CUSTOMERACDTL WHERE ENTITY_CODE=? AND SUNDRY_DB_AC=?";
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, context.getEntityCode());
			util.setString(2, inputDTO.get("SUNDRY_DB_AC"));
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				resultDTO.set("CUSTOMER_ID", rset.getString("CUSTOMER_ID"));
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			util.reset();
			dbContext.close();
		}
		return resultDTO;
	}

	public boolean validateAgreementNo() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(agreeNo) && validation.isEmpty(custIDHidden)) {
				getErrorMap().setError("agreeNo", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			DTObject formDTO = getFormDTO();
			formDTO.set("CUSTOMER_ID", custIDHidden);
			formDTO.set("AGREEMENT_NO", agreeNo);
			formDTO.set(ContentManager.ACTION, USAGE);
			formDTO = validateAgreementNo(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("agreeNo", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("agreeNo", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public DTObject validateAgreementNo(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		LeaseValidator validation = new LeaseValidator();
		DBContext dbContext = new DBContext();
		try {
			inputDTO.set(ContentManager.FETCH_COLUMNS, "AGREEMENT_NO");
			resultDTO = validation.validateCustomerAgreement(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
			dbContext.close();
		}
		return resultDTO;
	}

	public boolean validateScheduleID() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(scheduleId)) {
				getErrorMap().setError("scheduleId", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			DTObject formDTO = getFormDTO();
			formDTO.set("LESSEE_CODE", leaseCode);
			formDTO.set("AGREEMENT_NO", agreeNo);
			formDTO.set("SCHEDULE_ID", scheduleId);
			formDTO.set(ContentManager.ACTION, USAGE);
			formDTO = validateScheduleID(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("scheduleId", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("scheduleId", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public DTObject validateScheduleID(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		LeaseValidator validation = new LeaseValidator();
		DBContext dbContext = new DBContext();
		try {
			inputDTO.set(ContentManager.FETCH_COLUMNS, "SCHEDULE_ID,TO_CHAR(AUTH_ON,'" + context.getDateFormat() + "') AUTH_ON,LEASE_CLOSURE_ON");
			resultDTO = validation.validateScheduleID(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != RegularConstants.NULL) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
			if (resultDTO.get("AUTH_ON") == RegularConstants.NULL) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNAUTH_LEASE_DETAILS);//
				return resultDTO;
			}
			if (resultDTO.get("LEASE_CLOSURE_ON") != RegularConstants.NULL) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.LEASE_CLOSED);//
				return resultDTO;
			}

		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
			dbContext.close();
		}
		return resultDTO;
	}

	public boolean validateAssetSerial() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(assetSl)) {
				getErrorMap().setError("assetSl", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidNumber(assetSl)) {
				getErrorMap().setError("assetSl", BackOfficeErrorCodes.INVALID_NUMBER);
				return false;
			}
			DTObject formDTO = getFormDTO();
			formDTO.set("LESSEE_CODE", leaseCode);
			formDTO.set("AGREEMENT_NO", agreeNo);
			formDTO.set("SCHEDULE_ID", scheduleId);
			formDTO.set("ASSET_SL", assetSl);
			formDTO.set(ContentManager.ACTION, USAGE);
			formDTO = validateAssetSerial(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("assetSl", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("assetSl", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public DTObject validateAssetSerial(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		DTObject resultDTOAssetTypeDesc = new DTObject();
		LeaseValidator validation = new LeaseValidator();
		MasterValidator validatior = new MasterValidator();
		DBContext dbContext = new DBContext();
		try {
			inputDTO.set(ContentManager.FETCH, ContentManager.FETCH_ALL); // NOT
																			// ENABLED
			resultDTO = validation.validateLeaseAssetSerial(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
			inputDTO.set("ASSET_TYPE_CODE", resultDTO.get("ASSET_TYPE_CODE"));
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
			resultDTOAssetTypeDesc = validatior.validateAssetType(inputDTO);
			if (resultDTOAssetTypeDesc.get(ContentManager.ERROR) != null) {
				resultDTOAssetTypeDesc.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTOAssetTypeDesc;
			}
			if (resultDTOAssetTypeDesc.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTOAssetTypeDesc.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTOAssetTypeDesc;
			}
			resultDTO.set("DESCRIPTION", resultDTOAssetTypeDesc.get("DESCRIPTION"));
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
			validatior.close();
			dbContext.close();
		}
		return resultDTO;
	}

	public boolean validateCompSerial() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(compSl)) {
				getErrorMap().setError("compSl", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidNumber(compSl)) {
				getErrorMap().setError("compSl", BackOfficeErrorCodes.INVALID_NUMBER);
				return false;
			}
			DTObject formDTO = getFormDTO();
			formDTO.set("LESSEE_CODE", leaseCode);
			formDTO.set("AGREEMENT_NO", agreeNo);
			formDTO.set("SCHEDULE_ID", scheduleId);
			formDTO.set("ASSET_SL", assetSl);
			formDTO.set("ASSET_COMP_SL", compSl);
			formDTO.set(ContentManager.ACTION, USAGE);
			formDTO = validateCompSerial(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("compSl", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("compSl", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public DTObject validateCompSerial(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		LeaseValidator validation = new LeaseValidator();
		DBContext dbContext = new DBContext();
		try {
			inputDTO.set(ContentManager.FETCH_COLUMNS, "ASSET_DESCRIPTION,IND_DTLS_REQD,ASSET_QTY");
			resultDTO = validation.validateLeaseAssetComponentSerial(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
			if (resultDTO.get("IND_DTLS_REQD").equals(RegularConstants.ZERO)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.ASSETID_CANNOT_SPECIFY); //
				return resultDTO;
			}
			assetQty = resultDTO.get("ASSET_QTY");
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
			dbContext.close();
		}
		return resultDTO;
	}

	public boolean validateQtySerial() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(qtySl)) {
				getErrorMap().setError("qtySl", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isEmpty(assetQty)) {
				if (Integer.parseInt(qtySl) > Integer.parseInt(assetQty)) {
					getErrorMap().setError("qtySl", BackOfficeErrorCodes.ASSETID_MAPPED);
					return false;
				}
			}
			DTObject formDTO = new DTObject();
			formDTO.set(ContentManager.USER_ACTION, getAction());
			formDTO.set("LESSEE_CODE", leaseCode);
			formDTO.set("AGREEMENT_NO", agreeNo);
			formDTO.set("SCHEDULE_ID", scheduleId);
			formDTO.set("ASSET_SL", assetSl);
			formDTO.set("ASSET_COMP_SL", compSl);
			formDTO.set("ASSET_QTY_SL", qtySl);
			formDTO = validateQtySerial(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("qtySl", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("qtySl", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public DTObject validateQtySerial(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		CommonValidator commonvalidation = new CommonValidator();
		String leaseCode = inputDTO.get("LESSEE_CODE");
		String agreeNo = inputDTO.get("AGREEMENT_NO");
		String scheduleId = inputDTO.get("SCHEDULE_ID");
		String assetSl = inputDTO.get("ASSET_SL");
		String compSl = inputDTO.get("ASSET_COMP_SL");
		String qtySl = inputDTO.get("ASSET_QTY_SL");
		String action = inputDTO.get(ContentManager.USER_ACTION);
		try {
			inputDTO.set(ContentManager.FETCH, ContentManager.FETCH_ALL);
			inputDTO.set(ContentManager.TABLE, "LEASEASSETDETAIL");
			String columns[] = new String[] { context.getEntityCode(), leaseCode, agreeNo, scheduleId, assetSl, compSl, qtySl };
			inputDTO.setObject(ContentManager.PROGRAM_KEY, columns);
			resultDTO = commonvalidation.validatePK(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (action.equals(ADD)) {
				if (ContentManager.DATA_AVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_RECORD_EXISTS);
					return resultDTO;
				}
			} else if (action.equals(MODIFY)) {
				if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_RECORD_NOT_EXISTS);
					return resultDTO;
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			commonvalidation.close();
		}
		return resultDTO;
	}

	public boolean validateIndAssetID() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(indAssetId)) {
				getErrorMap().setError("indAssetId", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			DTObject formDTO = getFormDTO();
			formDTO.set("ENTITY_CODE", context.getEntityCode());
			formDTO.set("LESSEE_CODE", leaseCode);
			formDTO.set("AGREEMENT_NO", agreeNo);
			formDTO.set("SCHEDULE_ID", scheduleId);
			formDTO.set("ASSET_SL", assetSl);
			formDTO.set("ASSET_COMP_SL", compSl);
			formDTO.set("ASSET_QTY_SL", qtySl);
			formDTO.set("INDIVIDUAL_ASSET_ID", indAssetId);
			formDTO = validateIndAssetID(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("indAssetId", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("indAssetId", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public DTObject validateIndAssetID(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		MasterValidator validation = new MasterValidator();
		try {
			String primaryKey = inputDTO.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + inputDTO.get("LESSEE_CODE") + RegularConstants.PK_SEPARATOR + inputDTO.get("AGREEMENT_NO") + RegularConstants.PK_SEPARATOR + inputDTO.get("SCHEDULE_ID") + RegularConstants.PK_SEPARATOR
					+ inputDTO.get("ASSET_SL") + RegularConstants.PK_SEPARATOR + inputDTO.get("ASSET_COMP_SL") + RegularConstants.PK_SEPARATOR + inputDTO.get("ASSET_QTY_SL");
			String dedupKey = context.getEntityCode() + RegularConstants.PK_SEPARATOR + inputDTO.get("INDIVIDUAL_ASSET_ID") + RegularConstants.PK_SEPARATOR + RegularConstants.ONE;
			DTObject formDTO = new DTObject();
			formDTO.set(ContentManager.PROGRAM_ID, "MINDASSET");
			formDTO.set(ContentManager.PURPOSE_ID, "IND_ASSET_ID");
			formDTO.set(ContentManager.PROGRAM_KEY, primaryKey);
			formDTO.set(ContentManager.DEDUP_KEY, dedupKey);
			resultDTO = validation.isDeDupAlreadyExist(formDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
				Object[] errorParam = { resultDTO.getObject("LINKED_TO_PK") };
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.IND_ASSET_EXISTS);
				resultDTO.setObject(ContentManager.ERROR_PARAM, errorParam);
				return resultDTO;
			}

		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateFieldID(String fieldId) {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(fieldId)) {
				getErrorMap().setError("fieldId", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			DTObject formDTO = getFormDTO();
			formDTO.set("FIELD_ID", fieldId);
			formDTO.set("CUSTOMER_ID_HIDDEN", custIDHidden);
			formDTO.set("ASSET_SERIAL", assetSerialAssetType);
			formDTO.set(ContentManager.ACTION, USAGE);
			formDTO = validateFieldID(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("fieldId", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("fieldId", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public DTObject validateFieldID(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		MasterValidator validation = new MasterValidator();
		DBContext dbContext = new DBContext();
		try {
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION,FIELD_VALUE_TYPE,FIELD_DECIMALS,FIELD_SIZE,CUSTOMER_ID,ASSET_TYPE_CODE,FIELD_FOR");
			resultDTO = validation.validateFieldId(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
			if (!validation.isEmpty(resultDTO.get("CUSTOMER_ID"))) {
				if (!inputDTO.get("CUSTOMER_ID_HIDDEN").equals(resultDTO.get("CUSTOMER_ID"))) {
					Object[] errorParam = { resultDTO.get("CUSTOMER_ID"),inputDTO.get("CUSTOMER_ID_HIDDEN") };
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RESERVED_FOR_CUSTOMER);
					resultDTO.setObject(ContentManager.ERROR_PARAM, errorParam);
					return resultDTO;
				}
			}
			if (!validation.isEmpty(resultDTO.get("ASSET_TYPE_CODE"))) {
				if (!inputDTO.get("ASSET_SERIAL").equals(resultDTO.get("ASSET_TYPE_CODE")) ) {
					Object[] errorParam = { resultDTO.get("ASSET_TYPE_CODE"),inputDTO.get("ASSET_SERIAL") };
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RESERVED_FOR_ASSET_TYPE);
					resultDTO.setObject(ContentManager.ERROR_PARAM, errorParam);
					return resultDTO;
				}
			}
			if (!resultDTO.get("FIELD_FOR").equals("A")) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_NOT_ALLOWED_ASSET_ATTRIBUTES);
				return resultDTO;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
			dbContext.close();
		}
		return resultDTO;
	}

	private boolean validateGrid() {
		CommonValidator validation = new CommonValidator();
		try {
			DTObject formDTO = getFormDTO();
			DTObject inputDTO = new DTObject();

			DTDObject dtdObject = new DTDObject();
			dtdObject.addColumn(0, "SELECT");
			dtdObject.addColumn(1, "FIELD_ID");
			dtdObject.addColumn(2, "DESCRIPTION");
			dtdObject.addColumn(3, "FIELD_VALUE");
			dtdObject.addColumn(4, "FIELD_TYPE");
			dtdObject.addColumn(5, "FIELD_DECIMAL");
			dtdObject.addColumn(6, "FIELD_SIZE");
			dtdObject.setXML(xmlGrid);
			if (dtdObject.getRowCount() <= 0) {
				getErrorMap().setError("fieldId", BackOfficeErrorCodes.ATLEAST_ONE_ROW);
				return false;
			}
			if (!validation.checkDuplicateRows(dtdObject, 1)) {
				getErrorMap().setError("fieldId", BackOfficeErrorCodes.DUPLICATE_DATA);
				return false;
			}
			for (int i = 0; i < dtdObject.getRowCount(); i++) {
				if (!validateFieldID(dtdObject.getValue(i, 1))) {
					getErrorMap().setError("fieldId", BackOfficeErrorCodes.INVALID_ACTION);
					return false;
				}
				if (dtdObject.getValue(i, 4).equals("T")) {
					inputDTO.set("PRECISION", dtdObject.getValue(i, 6));
					if (!validateFieldRemarks(dtdObject.getValue(i, 4))) {
						getErrorMap().setError("fieldId", BackOfficeErrorCodes.INVALID_ACTION);
						return false;
					}
				} else if (!dtdObject.getValue(i, 4).equals("T")) {
					inputDTO.set("FIELD_VALUE", dtdObject.getValue(i, 3));
					String fieldDecimal = dtdObject.getValue(i, 5);
					if (validation.isEmpty(dtdObject.getValue(i, 5))) {
						fieldDecimal = RegularConstants.ZERO;
					}
					inputDTO.set("PRECISION", String.valueOf((Integer.parseInt(dtdObject.getValue(i, 6)) - Integer.parseInt(fieldDecimal))));
					if (dtdObject.getValue(i, 4).equals("A") || dtdObject.getValue(i, 4).equals("F")) {
						inputDTO.set("SCALE", fieldDecimal);
					} else if (dtdObject.getValue(i, 4).equals("N")) {
						inputDTO.set("SCALE", fieldDecimal);
					}
					formDTO = validation.isValidDynamicIntFractions(inputDTO);
					if (formDTO.getObject(ContentManager.ERROR_PARAM) != null) {
						Object[] errorParam = formDTO.getObjectL(ContentManager.ERROR_PARAM);
						getErrorMap().setError("fieldValue", formDTO.get(ContentManager.ERROR), errorParam);
					}
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("fieldId", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return true;
	}

	public boolean validateFieldRemarks(String fieldRemarks) {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(fieldRemarks)) {
				getErrorMap().setError("fieldRemarks", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("fieldRemarks", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateRemarks() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (remarks != null && remarks.length() > 0) {
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			if (getAction().equals(MODIFY)) {
				if (commonValidator.isEmpty(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}
}
