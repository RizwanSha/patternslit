package patterns.config.web.forms.cmn;

import java.sql.ResultSet;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.AccessValidator;
import patterns.config.validations.BaseDomainValidator;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.MasterValidator;
import patterns.config.web.forms.GenericFormBean;

public class mgeoregionsbean extends GenericFormBean {

	private static final long serialVersionUID = 1L;

	private String geoRegionCode;
	private String description;
	private String conciseDescription;
	private String pinCode;
	private String officeCode;
	private boolean enabled;
	private String remarks;
	private String mapData;
	private String mapInvNo;

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getGeoRegionCode() {
		return geoRegionCode;
	}

	public void setGeoRegionCode(String geoRegionCode) {
		this.geoRegionCode = geoRegionCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getConciseDescription() {
		return conciseDescription;
	}

	public void setConciseDescription(String conciseDescription) {
		this.conciseDescription = conciseDescription;
	}

	public String getPinCode() {
		return pinCode;
	}

	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}

	public String getOfficeCode() {
		return officeCode;
	}

	public void setOfficeCode(String officeCode) {
		this.officeCode = officeCode;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getMapData() {
		return mapData;
	}

	public void setMapData(String mapData) {
		this.mapData = mapData;
	}

	public String getMapInvNo() {
		return mapInvNo;
	}

	public void setMapInvNo(String mapInvNo) {
		this.mapInvNo = mapInvNo;
	}

	@Override
	public void reset() {
		geoRegionCode = RegularConstants.EMPTY_STRING;
		description = RegularConstants.EMPTY_STRING;
		conciseDescription = RegularConstants.EMPTY_STRING;
		pinCode = RegularConstants.EMPTY_STRING;
		officeCode = RegularConstants.EMPTY_STRING;
		mapData = RegularConstants.EMPTY_STRING;
		mapInvNo = RegularConstants.EMPTY_STRING;
		enabled = false;
		remarks = RegularConstants.EMPTY_STRING;
		setProcessBO("patterns.config.framework.bo.cmn.mgeoregionsBO");
	}

	@Override
	public void validate() {
		if (validateGeoRegionCode()) {
			validateDescription();
			validateConciseDescription();
			validatePinCode();
			validateOfficeCode();
			validateRemarks();
			// validateGeoRegion();
			uploadMapData();
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.set("GEO_REGION_CODE", geoRegionCode);
				formDTO.set("DESCRIPTION", description);
				formDTO.set("CONCISE_DESCRIPTION", conciseDescription);
				formDTO.set("PINCODE_GEOUNITID", pinCode);
				formDTO.set("OFFICE_CODE", officeCode);
				formDTO.set("MAP_INV_NO", mapInvNo);
				// formDTO.set("MAP_DATA", mapData);
				if (getAction().equals(ADD)) {
					formDTO.set("ENABLED", decodeBooleanToString(true));
				} else {
					formDTO.set("ENABLED", decodeBooleanToString(enabled));
				}
				formDTO.set("REMARKS", remarks);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("geoRegionCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
	}

	public boolean validateGeoRegionCode() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("GEO_REGION_CODE", geoRegionCode);
			formDTO.set(ContentManager.ACTION, getAction());
			formDTO = validateGeoRegionCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("geoRegionCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("geoRegionCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateGeoRegionCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		MasterValidator validation = new MasterValidator();
		String geoRegionCode = inputDTO.get("GEO_REGION_CODE");
		String action = inputDTO.get(ContentManager.USER_ACTION);
		try {
			if (validation.isEmpty(geoRegionCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "GEO_REGION_CODE");
			resultDTO = validation.validateGeoRegionCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (action.equals(ADD)) {
				if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_EXISTS);
				}
			} else {
				if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_NOT_EXISTS);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateDescription() {
		CommonValidator validation = new CommonValidator();
		try {
			if (!validation.isValidDescription(description)) {
				getErrorMap().setError("description", BackOfficeErrorCodes.INVALID_DESCRIPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("description", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateConciseDescription() {
		CommonValidator validation = new CommonValidator();
		try {
			if (!validation.isValidConciseDescription(conciseDescription)) {
				getErrorMap().setError("conciseDescription", BackOfficeErrorCodes.INVALID_CONCISE_DESCRIPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("conciseDescription", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validatePinCode() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("GEO_UNIT_ID", pinCode);
			formDTO.set(ContentManager.ACTION, USAGE);
			formDTO = validatePinCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("pinCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("pinCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validatePinCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		MasterValidator validation = new MasterValidator();
		String pinCode = inputDTO.get("GEO_UNIT_ID");
		try {
			if (validation.isEmpty(pinCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set("ENTITY_CODE", context.getEntityCode());
			inputDTO.set(ContentManager.ACTION, USAGE);
			inputDTO.set(ContentManager.FETCH_COLUMNS, "OUR_COUNTRY_CODE,GEO_UNIT_STRUC_PINCODE");
			resultDTO = validation.validateEntityCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			} else if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
				String entityPinCode = resultDTO.get("GEO_UNIT_STRUC_PINCODE");
				inputDTO.set("GEOC_COU_CODE", resultDTO.get("OUR_COUNTRY_CODE"));
				inputDTO.set("GEOC_UNIT_ID", pinCode);
				inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION,GEO_UNIT_STRUC_CODE");
				resultDTO = validation.validateGeographicalUnitId(inputDTO);
				if (resultDTO.get(ContentManager.ERROR) != null) {
					resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
					return resultDTO;
				}
				if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
					return resultDTO;
				}
				if (!entityPinCode.equals(resultDTO.get("GEO_UNIT_STRUC_CODE"))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.PBS_PINCODE_ONLY_ALLOWED);
					return resultDTO;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateOfficeCode() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("BRANCH_CODE", officeCode);
			formDTO.set(ContentManager.ACTION, USAGE);
			formDTO = validateOfficeCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("officeCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("officeCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateOfficeCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		AccessValidator validation = new AccessValidator();
		String officeCode = inputDTO.get("BRANCH_CODE");
		try {
			if (validation.isEmpty(officeCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
			resultDTO = validation.validateEntityBranch(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}

		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateRemarks() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (remarks != null && remarks.length() > 0) {
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			if (getAction().equals(MODIFY)) {
				if (commonValidator.isEmpty(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}

	public boolean validateGeoRegion() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("PROGRAM_ID", context.getProcessID());
			formDTO.set("GEO_REGION_CODE", geoRegionCode);
			formDTO.set("PINCODE_GEOUNITID", pinCode);
			formDTO.set("OFFICE_CODE", officeCode);
			formDTO = validateGeoRegion(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("geoRegionCode", formDTO.get(ContentManager.ERROR), (Object[]) formDTO.getObject(ContentManager.ERROR_PARAM));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("geoRegionCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateGeoRegion(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		CommonValidator validation = new CommonValidator();
		try {
			String primaryKey = context.getEntityCode() + RegularConstants.PK_SEPARATOR + inputDTO.get("GEO_REGION_CODE");
			String dedupKey = context.getEntityCode() + RegularConstants.PK_SEPARATOR + inputDTO.get("PINCODE_GEOUNITID") + RegularConstants.PK_SEPARATOR + inputDTO.get("OFFICE_CODE") + RegularConstants.PK_SEPARATOR + RegularConstants.ONE;
			DTObject formDTO = new DTObject();
			formDTO.set(ContentManager.PROGRAM_ID, inputDTO.get("PROGRAM_ID"));
			formDTO.set(ContentManager.PROGRAM_KEY, primaryKey);
			formDTO.set(ContentManager.PURPOSE_ID, "BRANCH");
			formDTO.set(ContentManager.DEDUP_KEY, dedupKey);
			resultDTO = validation.isDeDupAlreadyExist(formDTO);
			if (resultDTO.get(ContentManager.ERROR) != null)
				return resultDTO;
			else if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.PBS_PINCODE_OFF_CODE_ALREADY_EXISTS);
				Object[] param = { resultDTO.get("LINKED_TO_PK").split("\\|")[1] };
				resultDTO.setObject(ContentManager.ERROR_PARAM, param);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean uploadMapData() {
		CommonValidator validation = new CommonValidator();
		DTObject formDTO = new DTObject();
		try {
			if (validation.isEmpty(mapData)) {
				getErrorMap().setError("geoRegionCode", BackOfficeErrorCodes.PBS_MAP_REGION_NOT_CREATED);
				return false;
			}
			else if (!validation.isEmpty(mapInvNo)) {
				formDTO.set("MAP_DATA", mapData);
				formDTO.set("MAP_INV_NO", mapInvNo);
				formDTO = validateMapInvNo(formDTO);
				if (formDTO.get(ContentManager.ERROR) != null) {
					getErrorMap().setError("geoRegionCode", formDTO.get(ContentManager.ERROR));
					return false;
				}
				return true;
			}
			else if (validation.isEmpty(mapInvNo)) {
				formDTO.set("MAP_DATA", mapData);
				formDTO = uploadMapData(formDTO);
				if (formDTO.get(ContentManager.ERROR) != null) {
					getErrorMap().setError("geoRegionCode", formDTO.get(ContentManager.ERROR));
					return false;
				}
			}

			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("geoRegionCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject uploadMapData(DTObject inputDTO) {
		BaseDomainValidator validator = new BaseDomainValidator();
		String inventoryNumber = "";
		DTObject resultDTO = new DTObject();
		try {
			byte[] uploadData;
			uploadData = inputDTO.get("MAP_DATA").getBytes();
			resultDTO = validator.generateFileInventorySequence(resultDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FILE_INVENTORY_GENERROR);
				return resultDTO;
			}
			inventoryNumber = resultDTO.get("INV_SEQ");
			resultDTO.set("INV_NUM", inventoryNumber);
			resultDTO.setObject("FILE", uploadData);
			resultDTO.setObject("THUMB_NAIL", RegularConstants.NULL);
			resultDTO.set("FILE_NAME", RegularConstants.NULL);
			resultDTO.set("IN_USE", RegularConstants.COLUMN_DISABLE);
			resultDTO.set("PURPOSE", RegularConstants.NULL);
			resultDTO.set("EXTENSION", RegularConstants.NULL);
			resultDTO.set("GROUP_FILE_INV", RegularConstants.NULL);
			resultDTO.set("CHECKSUM", RegularConstants.NULL);
			try {
				boolean updated = validator.updateFileInventory(resultDTO);
				if (!updated) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FILE_UPLOAD);
					return resultDTO;
				}
				setMapInvNo(resultDTO.get("INV_NUM"));
			} catch (Exception e) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FILE_UPLOAD);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return resultDTO;
	}

	public DTObject validateMapInvNo(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		try {
			DBContext dbContext = new DBContext();
			DBUtil dbutil = dbContext.createUtilInstance();
			StringBuffer sqlQuery = new StringBuffer(RegularConstants.EMPTY_STRING);
			try {
				dbutil.reset();
				dbutil.setMode(DBUtil.PREPARED);
				sqlQuery.append("SELECT C.FILE_NAME FROM CMNFILEINVENTORY C");
				sqlQuery.append(" WHERE C.ENTITY_CODE = ? AND C.FILE_INV_NUM =?");
				dbutil.reset();
				dbutil.setSql(sqlQuery.toString());
				dbutil.setLong(1, Long.parseLong(context.getEntityCode()));
				dbutil.setString(2, inputDTO.get("MAP_INV_NO"));
				ResultSet rset = dbutil.executeQuery();
				if (rset.next()) {
					resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
					resultDTO.set("FILE_NAME", rset.getString(1));
					return resultDTO;
				} else {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.PBS_MAP_INV_NO_UNAVAILABLE);
					return resultDTO;
				}
			} catch (Exception e) {
				e.printStackTrace();
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			} finally {
				dbContext.close();
			}

		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return resultDTO;
	}

}
