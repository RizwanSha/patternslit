package patterns.config.web.forms.cmn;

import java.sql.ResultSet;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.service.DTDObject;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.MasterValidator;
import patterns.config.web.forms.GenericFormBean;

public class mbtrancodesbean extends GenericFormBean {

	private static final long serialVersionUID = 1L;

	private String bankingTranCode;
	private String description;
	private String conciseDescription;
	private String bankingTranAccCode;
	private boolean localCurrTranFlag;
	private String transactionCode;
	private boolean enabled;
	private String remarks;
	private String xmlTranCodesGrid;

	public String getBankingTranCode() {
		return bankingTranCode;
	}

	public void setBankingTranCode(String bankingTranCode) {
		this.bankingTranCode = bankingTranCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getConciseDescription() {
		return conciseDescription;
	}

	public void setConciseDescription(String conciseDescription) {
		this.conciseDescription = conciseDescription;
	}

	public String getBankingTranAccCode() {
		return bankingTranAccCode;
	}

	public void setBankingTranAccCode(String bankingTranAccCode) {
		this.bankingTranAccCode = bankingTranAccCode;
	}

	public boolean isLocalCurrTranFlag() {
		return localCurrTranFlag;
	}

	public void setLocalCurrTranFlag(boolean localCurrTranFlag) {
		this.localCurrTranFlag = localCurrTranFlag;
	}

	public String getTransactionCode() {
		return transactionCode;
	}

	public void setTransactionCode(String transactionCode) {
		this.transactionCode = transactionCode;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getXmlTranCodesGrid() {
		return xmlTranCodesGrid;
	}

	public void setXmlTranCodesGrid(String xmlTranCodesGrid) {
		this.xmlTranCodesGrid = xmlTranCodesGrid;
	}

	@Override
	public void reset() {
		bankingTranCode = RegularConstants.EMPTY_STRING;
		description = RegularConstants.EMPTY_STRING;
		conciseDescription = RegularConstants.EMPTY_STRING;
		bankingTranAccCode = RegularConstants.EMPTY_STRING;
		localCurrTranFlag = false;
		transactionCode = RegularConstants.EMPTY_STRING;
		enabled = false;
		remarks = RegularConstants.EMPTY_STRING;
		setProcessBO("patterns.config.framework.bo.cmn.mbtrancodesBO");
	}

	@Override
	public void validate() {
		CommonValidator validation = new CommonValidator();
		if (validateBankingTranCode()) {
			validateDescription();
			validateConciseDescription();
			validateBankingTranAccCode();
			validateRemarks();
			validateGrid();
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("BTRAN_CODE", bankingTranCode);
				formDTO.set("DESCRIPTION", description);
				formDTO.set("CONCISE_DESCRIPTION", conciseDescription);
				if (!validation.isEmpty(bankingTranAccCode))
					formDTO.set("BTRAN_ACCESS_CODE", bankingTranAccCode);
				formDTO.set("BTRAN_LC_FLAG", decodeBooleanToString(localCurrTranFlag));
				if (getAction().equals(ADD)) {
					formDTO.set("ENABLED", decodeBooleanToString(true));
				} else {
					formDTO.set("ENABLED", decodeBooleanToString(enabled));
				}
				formDTO.set("REMARKS", remarks);

				DTDObject dtdObject = new DTDObject();
				dtdObject.addColumn(0, "SL");
				dtdObject.addColumn(1, "TRANSACTION_CODE");
				dtdObject.addColumn(2, "DESCRIPTION");
				dtdObject.addColumn(3, "TRAN_MODE");
				dtdObject.addColumn(4, "WITHDRAW_DEP");
				dtdObject.setXML(xmlTranCodesGrid);
				formDTO.setDTDObject("BTRANCODESDTL", dtdObject);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("bankingTranCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
	}

	public boolean validateBankingTranCode() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("BTRAN_CODE", bankingTranCode);
			formDTO.set(ContentManager.ACTION, getAction());
			formDTO.set("PROGRAM_ID", context.getProcessID());
			formDTO = validateBankingTranCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("bankingTranCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("bankingTranCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateBankingTranCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		MasterValidator validation = new MasterValidator();
		String bankingTranCode = inputDTO.get("BTRAN_CODE");
		String action = inputDTO.get(ContentManager.USER_ACTION);
		try {
			if (validation.isEmpty(bankingTranCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "BTRAN_CODE");
			resultDTO = validation.validateBankingTranCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (action.equals(ADD)) {
				if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_EXISTS);
					return resultDTO;
				}
			} else {
				if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_NOT_EXISTS);
					return resultDTO;
				}
			}

			DTObject formDTO = new DTObject();
			formDTO.set(ContentManager.PROGRAM_ID, inputDTO.get("PROGRAM_ID"));
			formDTO.set(ContentManager.PURPOSE_ID, "BTRAN_ACCESS_CODE");
			formDTO.set(ContentManager.PROGRAM_KEY, inputDTO.get("BTRAN_CODE"));
			formDTO.set(ContentManager.DEDUP_KEY, inputDTO.get("BTRAN_CODE"));
			resultDTO = validation.isDeDupAlreadyExist(formDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
				Object[] errorParam = { resultDTO.getObject("LINKED_TO_PK") };
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.PBS_ALPHA_ACCESS_CODE_ALREDY_LINKED);
				resultDTO.setObject(ContentManager.ERROR_PARAM, errorParam);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateDescription() {
		CommonValidator validation = new CommonValidator();
		try {
			if (!validation.isValidDescription(description)) {
				getErrorMap().setError("description", BackOfficeErrorCodes.INVALID_DESCRIPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("description", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateConciseDescription() {
		CommonValidator validation = new CommonValidator();
		try {
			if (!validation.isValidConciseDescription(conciseDescription)) {
				getErrorMap().setError("conciseDescription", BackOfficeErrorCodes.INVALID_CONCISE_DESCRIPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("conciseDescription", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateBankingTranAccCode() {
		CommonValidator validation = new CommonValidator();
		try {
			if (!validation.isEmpty(bankingTranAccCode)) {
				if (!validation.isValidCode(bankingTranAccCode)) {
					getErrorMap().setError("bankingTranAccCode", BackOfficeErrorCodes.INVALID_VALUE);
					return false;
				}
				if (!validation.isValidLength(bankingTranAccCode, validation.LENGTH_1, validation.LENGTH_6)) {
					getErrorMap().setError("bankingTranAccCode", BackOfficeErrorCodes.INVALID_LENGTH);
					return false;
				}
				if (bankingTranAccCode.equals(bankingTranCode)) {
					getErrorMap().setError("bankingTranAccCode", BackOfficeErrorCodes.PBS_ALPHA_ACCESS_CODE_NOTEQUAL);
					return false;
				}
				DTObject formDTO = new DTObject();
				formDTO.set("PROGRAM_ID", context.getProcessID());
				formDTO.set("BTRAN_CODE", bankingTranCode);
				formDTO.set("BTRAN_ACCESS_CODE", bankingTranAccCode);
				formDTO.set(ContentManager.ACTION, USAGE);
				formDTO = validateBankingTranAccCode(formDTO);
				if (formDTO.get(ContentManager.ERROR) != null) {
					getErrorMap().setError("bankingTranAccCode", formDTO.get(ContentManager.ERROR), (Object[]) formDTO.getObject(ContentManager.ERROR_PARAM));
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("bankingTranAccCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public DTObject validateBankingTranAccCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		CommonValidator validation = new CommonValidator();
		try {
			String primaryKey = inputDTO.get("BTRAN_CODE");
			String dedupKey = inputDTO.get("BTRAN_ACCESS_CODE");
			DTObject formDTO = new DTObject();
			formDTO.set(ContentManager.PROGRAM_ID, inputDTO.get("PROGRAM_ID"));
			formDTO.set(ContentManager.PURPOSE_ID, "BTRAN_ACCESS_CODE");
			formDTO.set(ContentManager.PROGRAM_KEY, primaryKey);
			formDTO.set(ContentManager.DEDUP_KEY, dedupKey);
			resultDTO = validation.isDeDupAlreadyExist(formDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
				Object[] errorParam = { resultDTO.getObject("LINKED_TO_PK") };
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.PBS_TRAN_ACCESS_CODE_ALREDY_LIKED);
				resultDTO.setObject(ContentManager.ERROR_PARAM, errorParam);
				return resultDTO;
			}
			DTObject formDTOOFTranAccessCode = new DTObject();
			formDTOOFTranAccessCode.set(ContentManager.PROGRAM_ID, inputDTO.get("PROGRAM_ID"));
			formDTOOFTranAccessCode.set(ContentManager.PURPOSE_ID, "BTRAN_CODE");
			formDTOOFTranAccessCode.set(ContentManager.PROGRAM_KEY, primaryKey);
			formDTOOFTranAccessCode.set(ContentManager.DEDUP_KEY, inputDTO.get("BTRAN_ACCESS_CODE"));
			formDTOOFTranAccessCode.set("BTRAN_ACCESS_CODE", inputDTO.get("BTRAN_ACCESS_CODE"));
			resultDTO = validateAlphaAccessCodeDeDup(formDTOOFTranAccessCode);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
				Object[] errorParam = { resultDTO.getObject("LINKED_TO_PK") };
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.PBS_TRAN_CODE_ALREDY_LIKED);
				resultDTO.setObject(ContentManager.ERROR_PARAM, errorParam);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public DTObject validateAlphaAccessCodeDeDup(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBContext dbContext = new DBContext();
		DBUtil dbutil = dbContext.createUtilInstance();
		String sqlQueryMainTable = "SELECT BTRAN_CODE FROM BTRANCODESM WHERE CAST(BTRAN_CODE AS CHAR) = ?";
		String sqlQueryTBADEDUPCHECK = RegularConstants.EMPTY_STRING;
		try {
			dbutil.reset();
			dbutil.setMode(DBUtil.PREPARED);
			dbutil.setSql(sqlQueryMainTable.toString());
			dbutil.setString(1, inputDTO.get("BTRAN_ACCESS_CODE"));
			ResultSet rset = dbutil.executeQuery();
			if (rset.next()) {
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				resultDTO.set("LINKED_TO_PK", rset.getString("BTRAN_CODE"));
				return resultDTO;
			} else {
				sqlQueryTBADEDUPCHECK = "SELECT PGM_PK FROM TBADEDUPCHECK WHERE PARTITION_NO=? AND PGM_ID=? AND PURPOSE_ID=? AND PGM_PK<>? AND COLUMN_VALUES=?";
				dbutil.reset();
				dbutil.setSql(sqlQueryTBADEDUPCHECK);
				dbutil.setInt(1, Integer.parseInt(context.getPartitionNo()));
				dbutil.setString(2, inputDTO.get(ContentManager.PROGRAM_ID));
				dbutil.setString(3, inputDTO.get(ContentManager.PURPOSE_ID));
				dbutil.setString(4, inputDTO.get(ContentManager.PROGRAM_KEY));
				dbutil.setString(5, inputDTO.get(ContentManager.DEDUP_KEY));
				ResultSet rsetTba = dbutil.executeQuery();
				if (rsetTba.next()) {
					resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
					resultDTO.set("LINKED_TO_PK", rsetTba.getString("PGM_PK"));
				}
			}
			return resultDTO;
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			dbutil.reset();
			dbContext.close();
		}
		return resultDTO;

	}

	public boolean validateRemarks() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (remarks != null && remarks.length() > 0) {
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			if (getAction().equals(MODIFY)) {
				if (commonValidator.isEmpty(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}

	public boolean validateTransactionCode(String transactionCode) {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("TRAN_CODE", transactionCode);
			formDTO.set(ContentManager.USER_ACTION, USAGE);
			formDTO = validateTransactionCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("transactionCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("transactionCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateTransactionCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		MasterValidator validation = new MasterValidator();
		String transactionCode = inputDTO.get("TRAN_CODE");
		try {
			if (validation.isEmpty(transactionCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION,TRAN_MODE,TRAN_TYPE");
			resultDTO = validation.validateTranCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			} else if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
				String desc = resultDTO.get("DESCRIPTION");
				String tranType = resultDTO.get("TRAN_TYPE");

				String tranModeLabel = validation.getCMLOVLABEL("COMMON", "TRANMODE", resultDTO.get("TRAN_MODE"));
				tranModeLabel = tranModeLabel.substring(2);
				String tranTypeLabel = validation.getCMLOVLABEL("COMMON", "TRANTYPE", tranType);
				tranTypeLabel = tranTypeLabel.substring(2);

				resultDTO.set("TRAN_TYPE", tranTypeLabel);
				resultDTO.set("TRAN_MODE", tranModeLabel);
				resultDTO.set("DESCRIPTION", desc);
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	private boolean validateGrid() {
		CommonValidator validation = new CommonValidator();
		try {
			DTDObject dtdObject = new DTDObject();
			dtdObject.addColumn(0, "SL");
			dtdObject.addColumn(1, "TRANSACTION_CODE");
			dtdObject.addColumn(2, "DESCRIPTION");
			dtdObject.addColumn(3, "TRAN_MODE");
			dtdObject.addColumn(4, "WITHDRAW_DEP");
			dtdObject.setXML(xmlTranCodesGrid);
			if (dtdObject.getRowCount() <= 0) {
				getErrorMap().setError("transactionCode", BackOfficeErrorCodes.ATLEAST_ONE_ROW);
				return false;
			}
			if (!validation.checkDuplicateRows(dtdObject, 1)) {
				getErrorMap().setError("transactionCode", BackOfficeErrorCodes.DUPLICATE_DATA);
				return false;
			}
			for (int i = 0; i < dtdObject.getRowCount(); i++) {
				if (!validateTransactionCode(dtdObject.getValue(i, 1))) {
					getErrorMap().setError("transactionCode", BackOfficeErrorCodes.INVALID_ACTION);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("transactionCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return true;
	}

}
