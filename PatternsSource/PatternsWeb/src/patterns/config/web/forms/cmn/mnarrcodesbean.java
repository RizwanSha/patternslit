package patterns.config.web.forms.cmn;


import java.util.ArrayList;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.GenericOption;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.MasterValidator;
import patterns.config.web.forms.GenericFormBean;

public class mnarrcodesbean extends GenericFormBean {

	private static final long serialVersionUID = 1L;
	private String narrationCode;
	private String description;
	private String conciseDescription;
	private String useTranCode;
	private boolean enabled;
	private String remarks;
	
	
	
	public String getNarrationCode() {
		return narrationCode;
	}

	public void setNarrationCode(String narrationCode) {
		this.narrationCode = narrationCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getConciseDescription() {
		return conciseDescription;
	}

	public void setConciseDescription(String conciseDescription) {
		this.conciseDescription = conciseDescription;
	}

	public String getUseTranCode() {
		return useTranCode;
	}

	public void setUseTranCode(String useTranCode) {
		this.useTranCode = useTranCode;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void reset() {
		narrationCode = RegularConstants.EMPTY_STRING;
		description = RegularConstants.EMPTY_STRING;
		conciseDescription = RegularConstants.EMPTY_STRING;
		useTranCode = RegularConstants.EMPTY_STRING;
		enabled=false;
		remarks = RegularConstants.EMPTY_STRING;
		DBContext dbContext = new DBContext();
		ArrayList<GenericOption> useTranCode = null;
		useTranCode = getGenericOptions("COMMON", "TRANMODE", dbContext, null);
		webContext.getRequest().setAttribute("COMMON_TRANMODE", useTranCode);
		dbContext.close();
		setProcessBO("patterns.config.framework.bo.cmn.mnarrcodesBO");
	}

	public void validate() {
		if (validateNarrationCode()) {
			validateDescription();
			validateConciseDescription();
			validateUseTranCode();
			validateRemarks();
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.set("NARR_CODE", narrationCode);
				formDTO.set("DESCRIPTION", description);
				formDTO.set("CONCISE_DESCRIPTION", conciseDescription);
				formDTO.set("TRAN_MODE", useTranCode);
				if (getAction().equals(ADD)) {
					formDTO.set("ENABLED", decodeBooleanToString(true));
				} else {
					formDTO.set("ENABLED", decodeBooleanToString(enabled));
				}
				formDTO.set("REMARKS", remarks);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("narrationCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
	}

	
	
	
	public boolean validateNarrationCode() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("NARR_CODE", narrationCode);
			formDTO.set("ACTION", getAction());
			formDTO = validateNarrationCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("narrationCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("narrationCode", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		}
		return false;
	}
	
	
	public DTObject validateNarrationCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		MasterValidator validation = new MasterValidator();
		String narrationCode = inputDTO.get("NARR_CODE");
		String action = inputDTO.get(ContentManager.USER_ACTION);
		try {
			if(validation.isEmpty(narrationCode)){
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
			resultDTO = validation.validateNarrationCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (action.equals(ADD)) {
				if (ContentManager.DATA_AVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_EXISTS);
				}
			} else {
				if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_NOT_EXISTS);

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}
	

	public boolean validateDescription() {
		CommonValidator validation = new CommonValidator();
		try {
			if(validation.isEmpty(description)){
				getErrorMap().setError("description",BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidRemarks(description)) {
				getErrorMap().setError("description", BackOfficeErrorCodes.PBS_INVALID_NARRTION_DESCRIPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("description", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}
	
	public boolean validateConciseDescription() {
		CommonValidator validation = new CommonValidator();
		try {
			if(validation.isEmpty(conciseDescription)){
				getErrorMap().setError("conciseDescription",BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidConciseDescription(conciseDescription)) {
				getErrorMap().setError("conciseDescription", BackOfficeErrorCodes.INVALID_CONCISE_DESCRIPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("conciseDescription", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}
	
	public boolean validateUseTranCode() {
		CommonValidator validation = new CommonValidator();
		try {
			if(validation.isEmpty(useTranCode)){
				getErrorMap().setError("useTranCode",BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isCMLOVREC("COMMON", "TRANMODE", useTranCode)) {
				getErrorMap().setError("useTranCode", BackOfficeErrorCodes.INVALID_OPTION);
				return false;

			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("useTranCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	

	public boolean validateRemarks() {
		CommonValidator commonValidator = new CommonValidator();
        try {

            if (getAction().equals(ADD) && remarks!=null && remarks.length()>0) {
                if (commonValidator.isEmpty(remarks)) {
                    getErrorMap().setError("remarks", BackOfficeErrorCodes.FIELD_BLANK);
                    return false;
                }
                if(!commonValidator.isValidRemarks(remarks)){
                    getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
                    return false;
                }
            }
            else if (getAction().equals(MODIFY)) {
                if (commonValidator.isEmpty(remarks)) {
                    getErrorMap().setError("remarks", BackOfficeErrorCodes.FIELD_BLANK);
                    return false;
                }
                if(!commonValidator.isValidRemarks(remarks)){
                    getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
                    return false;
                }

            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            getErrorMap().setError("remarks", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
        } finally {
            commonValidator.close();
        }
        return false;
	}
}
