

/*
 *
 Author : Raja E
 Created Date : 08-12-2016
 Spec Reference : MACSTATUSCODES
 Modification History
 ----------------------------------------------------------------------
 Sl.No		Modified Date	Author			Modified Changes	Version
 ----------------------------------------------------------------------

 */


package patterns.config.web.forms.cmn;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.FASValidator;
import patterns.config.validations.MasterValidator;
import patterns.config.web.forms.GenericFormBean;

public class macstatuscodesbean extends GenericFormBean {

	private static final long serialVersionUID = 1L;

	private String accountStatusCode;
	private String description;
	private String conciseDescription;
	private boolean preOpeningStatus;
	private boolean normalStatus;
	private boolean overdueStatus;
	private boolean closedStatus;
	private boolean inOperativeStatus;
	private boolean dormantStatus;
	private boolean authorityStatus;
	private String regAuthorityName;
	private boolean statusNoTransactionPeriod;
	private String noTransactionPeriod;
	private boolean balanceTransferAccount;
	private String payableGLHead;
	private boolean GLChangeStatus;
	private boolean enabled;
	private String remarks;

	public String getRegAuthorityName() {
		return regAuthorityName;
	}

	public void setRegAuthorityName(String regAuthorityName) {
		this.regAuthorityName = regAuthorityName;
	}

	public String getNoTransactionPeriod() {
		return noTransactionPeriod;
	}

	public void setNoTransactionPeriod(String noTransactionPeriod) {
		this.noTransactionPeriod = noTransactionPeriod;
	}

	public String getPayableGLHead() {
		return payableGLHead;
	}

	public void setPayableGLHead(String payableGLHead) {
		this.payableGLHead = payableGLHead;
	}

	public boolean isStatusNoTransactionPeriod() {
		return statusNoTransactionPeriod;
	}

	public void setStatusNoTransactionPeriod(boolean statusNoTransactionPeriod) {
		this.statusNoTransactionPeriod = statusNoTransactionPeriod;
	}

	public String getAccountStatusCode() {
		return accountStatusCode;
	}

	public void setAccountStatusCode(String accountStatusCode) {
		this.accountStatusCode = accountStatusCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getConciseDescription() {
		return conciseDescription;
	}

	public void setConciseDescription(String conciseDescription) {
		this.conciseDescription = conciseDescription;
	}

	public boolean isPreOpeningStatus() {
		return preOpeningStatus;
	}

	public void setPreOpeningStatus(boolean preOpeningStatus) {
		this.preOpeningStatus = preOpeningStatus;
	}

	public boolean isNormalStatus() {
		return normalStatus;
	}

	public void setNormalStatus(boolean normalStatus) {
		this.normalStatus = normalStatus;
	}

	public boolean isOverdueStatus() {
		return overdueStatus;
	}

	public void setOverdueStatus(boolean overdueStatus) {
		this.overdueStatus = overdueStatus;
	}

	public boolean isClosedStatus() {
		return closedStatus;
	}

	public void setClosedStatus(boolean closedStatus) {
		this.closedStatus = closedStatus;
	}

	public boolean isInOperativeStatus() {
		return inOperativeStatus;
	}

	public void setInOperativeStatus(boolean inOperativeStatus) {
		this.inOperativeStatus = inOperativeStatus;
	}

	public boolean isDormantStatus() {
		return dormantStatus;
	}

	public void setDormantStatus(boolean dormantStatus) {
		this.dormantStatus = dormantStatus;
	}

	public boolean isAuthorityStatus() {
		return authorityStatus;
	}

	public void setAuthorityStatus(boolean authorityStatus) {
		this.authorityStatus = authorityStatus;
	}

	public boolean isBalanceTransferAccount() {
		return balanceTransferAccount;
	}

	public void setBalanceTransferAccount(boolean balanceTransferAccount) {
		this.balanceTransferAccount = balanceTransferAccount;
	}

	public boolean isGLChangeStatus() {
		return GLChangeStatus;
	}

	public void setGLChangeStatus(boolean gLChangeStatus) {
		GLChangeStatus = gLChangeStatus;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public void reset() {
		// TODO Auto-generated method stub
		accountStatusCode = RegularConstants.EMPTY_STRING;
		description = RegularConstants.EMPTY_STRING;
		conciseDescription = RegularConstants.EMPTY_STRING;
		preOpeningStatus = false;
		normalStatus = false;
		overdueStatus = false;
		closedStatus = false;
		inOperativeStatus = false;
		dormantStatus = false;
		authorityStatus = false;
		regAuthorityName = RegularConstants.EMPTY_STRING;
		statusNoTransactionPeriod = false;
		noTransactionPeriod = RegularConstants.EMPTY_STRING;
		balanceTransferAccount = false;
		payableGLHead = RegularConstants.EMPTY_STRING;
		GLChangeStatus = false;
		enabled = false;
		remarks = RegularConstants.EMPTY_STRING;
		setProcessBO("patterns.config.framework.bo.cmn.macstatuscodesBO");
	}

	@Override
	public void validate() {
		// TODO Auto-generated method stub

		if (validateAccountStatusCode()) {
			validateDescription();
			validateConciseDescription();
			validateAuthorityStatus();
			validateRegAuthorityName();
			validateNoTransactionPeriod();
			validatePayableGLHead();
			validateRemarks();
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.set("ACNT_STATUS_CODE", accountStatusCode);
				formDTO.set("DESCRIPTION", description);
				formDTO.set("CONCISE_DESCRIPTION", conciseDescription);
				formDTO.set("PRE_OPENING_STATUS", decodeBooleanToString(preOpeningStatus));
				formDTO.set("NORMAL_STATUS", decodeBooleanToString(normalStatus));
				formDTO.set("OVERDUE_STATUS", decodeBooleanToString(overdueStatus));
				formDTO.set("CLOSED_STATUS", decodeBooleanToString(closedStatus));
				formDTO.set("INOPERATIVE_STATUS", decodeBooleanToString(inOperativeStatus));
				formDTO.set("DORMANT_STATUS", decodeBooleanToString(dormantStatus));
				formDTO.set("TRAN_REG_AUTH_STATUS", decodeBooleanToString(authorityStatus));
				formDTO.set("REG_AUTH_NAME", regAuthorityName);
				formDTO.set("NO_TRAN_STATUS", decodeBooleanToString(statusNoTransactionPeriod));
				if (noTransactionPeriod.isEmpty())
					formDTO.set("NO_TRAN_PERIOD", RegularConstants.NULL);
				else
					formDTO.set("NO_TRAN_PERIOD", noTransactionPeriod);
				formDTO.set("BAL_TRANSFER_REQ_STATUS", decodeBooleanToString(balanceTransferAccount));
				formDTO.set("PAY_GL_HEAD", payableGLHead);
				formDTO.set("GL_CHG_REQ_STATUS", decodeBooleanToString(GLChangeStatus));
				if (getAction().equals(ADD)) {
					formDTO.set("ENABLED", decodeBooleanToString(true));
				} else {
					formDTO.set("ENABLED", decodeBooleanToString(enabled));
				}
				formDTO.set("REMARKS", remarks);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("accountStatusCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
	}

	public boolean validateAccountStatusCode() {
		try {
			DTObject formDTO = getFormDTO();
			formDTO.set("ACNT_STATUS_CODE", accountStatusCode);
			formDTO.set(ContentManager.USER_ACTION, getAction());
			formDTO = validateAccountStatusCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("accountStatusCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("accountStatusCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateAccountStatusCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		MasterValidator validation = new MasterValidator();
		String accStatusCode = inputDTO.get("ACNT_STATUS_CODE");
		String action = inputDTO.get(ContentManager.USER_ACTION);
		try {
			if (validation.isEmpty(accStatusCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH, ContentManager.FETCH_ALL);
			resultDTO = validation.validateAccountStatusCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (action.equals(ADD)) {
				if (ContentManager.DATA_AVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_EXISTS);
				}
			} else {
				if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_NOT_EXISTS);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateDescription() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(description)) {
				getErrorMap().setError("description", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidDescription(description)) {
				getErrorMap().setError("description", BackOfficeErrorCodes.INVALID_DESCRIPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("description", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateConciseDescription() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(conciseDescription)) {
				getErrorMap().setError("conciseDescription", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidConciseDescription(conciseDescription)) {
				getErrorMap().setError("conciseDescription", BackOfficeErrorCodes.INVALID_CONCISE_DESCRIPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("conciseDescription", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateAuthorityStatus() {
		CommonValidator validation = new CommonValidator();
		try {
			int CheckBoxCount = 0;
			if (preOpeningStatus)
				CheckBoxCount++;
			if (normalStatus)
				CheckBoxCount++;
			if (overdueStatus)
				CheckBoxCount++;
			if (closedStatus)
				CheckBoxCount++;
			if (inOperativeStatus)
				CheckBoxCount++;
			if (dormantStatus)
				CheckBoxCount++;
			if (authorityStatus)
				CheckBoxCount++;
			if (CheckBoxCount > 1) {
				getErrorMap().setError("authorityStatus", BackOfficeErrorCodes.ONLY_ONE_VALUE_CHECKED);
				return false;
			}
			if (CheckBoxCount == 0) {
				getErrorMap().setError("authorityStatus", BackOfficeErrorCodes.ONLY_ONE_VALUE_CHECKED);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("conciseDescription", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateRegAuthorityName() {
		CommonValidator validation = new CommonValidator();
		try {
			if (authorityStatus) {
				if (validation.isEmpty(regAuthorityName)) {
					getErrorMap().setError("regAuthorityName", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!validation.isValidConciseDescription(regAuthorityName)) {
					getErrorMap().setError("regAuthorityName", BackOfficeErrorCodes.INVALID_CONCISE_DESCRIPTION);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("regAuthorityName", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateNoTransactionPeriod() {
		CommonValidator validation = new CommonValidator();
		try {
			if (statusNoTransactionPeriod) {
				if (validation.isEmpty(noTransactionPeriod)) {
					getErrorMap().setError("noTransactionPeriod", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!validation.isValidNumber(noTransactionPeriod)) {
					getErrorMap().setError("noTransactionPeriod", BackOfficeErrorCodes.NUMERIC_CHECK);
					return false;
				}
				if (validation.isZero(noTransactionPeriod)) {
					getErrorMap().setError("noTransactionPeriod", BackOfficeErrorCodes.ZERO_CHECK);
					return false;
				}
				if (validation.isValidNegativeNumber(noTransactionPeriod)) {
					getErrorMap().setError("noTransactionPeriod", BackOfficeErrorCodes.POSITIVE_CHECK);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("noTransactionPeriod", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validatePayableGLHead() {
		try {
			if (overdueStatus) {
				DTObject formDTO = getFormDTO();
				formDTO.set("GL_HEAD_CODE", payableGLHead);
				formDTO.set(ContentManager.USER_ACTION, USAGE);
				formDTO = validateGLHead(formDTO);
				if (formDTO.get(ContentManager.ERROR) != null) {
					getErrorMap().setError("payableGLHead", formDTO.get(ContentManager.ERROR));
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("payableGLHead", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateGLHead(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		FASValidator validation = new FASValidator();
		String glHeadCode = inputDTO.get("GL_HEAD_CODE");
		try {
			if (validation.isEmpty(glHeadCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			if (!validation.isValidNumber(glHeadCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.NUMERIC_CHECK);
				return resultDTO;
			}
			inputDTO.reset();
			inputDTO.set("GL_HEAD_CODE", glHeadCode);
			inputDTO.set(ContentManager.USER_ACTION, USAGE);
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION,POSTING_HEAD");
			resultDTO = validation.validateGLHead(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
			if (!resultDTO.get("POSTING_HEAD").equals(RegularConstants.ONE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.PBS_POST_GL_ALLOWED);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateRemarks() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (remarks != null && remarks.length() > 0) {
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			if (getAction().equals(MODIFY)) {
				if (commonValidator.isEmpty(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}
}
