package patterns.config.web.forms.cmn;

/*
 *
 Author : Javeed S
 Created Date : 28-JAN-2016
 Spec Reference : PPBS/CMN/0001-MPORTFOLIO -Portfolio Maintenance
 Modification History
 ----------------------------------------------------------------------
 Sl.No		Modified Date	Author			Modified Changes	Version
 ----------------------------------------------------------------------

 */

import java.sql.ResultSet;
import java.util.ArrayList;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.GenericOption;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.MasterValidator;
import patterns.config.web.forms.GenericFormBean;

public class mportfoliobean extends GenericFormBean {

	private static final long serialVersionUID = -6219657363150466160L;

	private String portFolioCode;
	private String description;
	private String conciseDescription;
	private String parentPortfolioCode;
	private String status;
	private boolean enabled;
	private String remarks;

	public String getPortFolioCode() {
		return portFolioCode;
	}

	public void setPortFolioCode(String portFolioCode) {
		this.portFolioCode = portFolioCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getConciseDescription() {
		return conciseDescription;
	}

	public void setConciseDescription(String conciseDescription) {
		this.conciseDescription = conciseDescription;
	}

	public String getParentPortfolioCode() {
		return parentPortfolioCode;
	}

	public void setParentPortfolioCode(String parentPortfolioCode) {
		this.parentPortfolioCode = parentPortfolioCode;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void reset() {
		portFolioCode = RegularConstants.EMPTY_STRING;
		description = RegularConstants.EMPTY_STRING;
		conciseDescription = RegularConstants.EMPTY_STRING;
		parentPortfolioCode = RegularConstants.EMPTY_STRING;
		status = RegularConstants.EMPTY_STRING;
		enabled = false;
		remarks = RegularConstants.EMPTY_STRING;
		DBContext dbContext = new DBContext();
		ArrayList<GenericOption> portFolioCode = null;
		portFolioCode = getGenericOptions("COMMON", "PORTFOLIO_STATUS", dbContext, null);
		webContext.getRequest().setAttribute("COMMON_PORTFOLIO_STATUS", portFolioCode);
		dbContext.close();

		setProcessBO("patterns.config.framework.bo.cmn.mportfolioBO");
	}

	public void validate() {
		if (validatePortFolioCode()) {
			validateDescription();
			validateConciseDescription();
			validateParentPortfolioCode();
			validateStatus();
			if (getAction().equals(MODIFY) && status.equals("C"))
				validateCheckParentDisabled("status");
			else if (getAction().equals(MODIFY) && !enabled)
				validateCheckParentDisabled("enabled");
			validateRemarks();
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.set("PORTFOLIO_CODE", portFolioCode);
				formDTO.set("DESCRIPTION", description);
				formDTO.set("CONCISE_DESCRIPTION", conciseDescription);
				formDTO.set("PARENT_PORTFOLIO_CODE", parentPortfolioCode);
				formDTO.set("STATUS", status);
				if (getAction().equals(ADD))
					formDTO.set("STATUS", "O");
				else
					formDTO.set("STATUS", status);
				if (getAction().equals(ADD)) {
					formDTO.set("ENABLED", decodeBooleanToString(true));
				} else {
					formDTO.set("ENABLED", decodeBooleanToString(enabled));
				}
				formDTO.set("REMARKS", remarks);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("portFolioCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
	}

	public boolean validatePortFolioCode() {
		try {
			DTObject formDTO = getFormDTO();
			formDTO.set("PORTFOLIO_CODE", portFolioCode);
			formDTO.set(ContentManager.USER_ACTION, getAction());
			formDTO = validatePortFolioCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("portFolioCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("portFolioCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validatePortFolioCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		MasterValidator validation = new MasterValidator();
		String portFolioCode = inputDTO.get("PORTFOLIO_CODE");
		String action = inputDTO.get(ContentManager.ACTION);
		try {
			if (validation.isEmpty(portFolioCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			resultDTO = validation.validatePortFolioCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (action.equals(ADD)) {
				if (ContentManager.DATA_AVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_EXISTS);
				}
			} else {
				if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_NOT_EXISTS);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateDescription() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(description)) {
				getErrorMap().setError("description", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidDescription(description)) {
				getErrorMap().setError("description", BackOfficeErrorCodes.INVALID_DESCRIPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("description", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateConciseDescription() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(conciseDescription)) {
				getErrorMap().setError("conciseDescription", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidDescription(conciseDescription)) {
				getErrorMap().setError("conciseDescription", BackOfficeErrorCodes.INVALID_DESCRIPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("conciseDescription", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateParentPortfolioCode() {
		try {
			if (!parentPortfolioCode.isEmpty()) {
				if (parentPortfolioCode.equals(portFolioCode)) {
					getErrorMap().setError("parentPortfolioCode", BackOfficeErrorCodes.FAS_GLCODE_CANT_SAME);
					return false;
				}
				DTObject formDTO = new DTObject();
				formDTO.set("PORTFOLIO_CODE", parentPortfolioCode);
				formDTO.set("FOLIO_CODE", portFolioCode);
				formDTO.set(ContentManager.USER_ACTION, USAGE);
				formDTO = validateParentPortfolioCode(formDTO);
				if (formDTO.get(ContentManager.ERROR) != null) {
					getErrorMap().setError("parentPortfolioCode", formDTO.get(ContentManager.ERROR));
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("parentPortfolioCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateParentPortfolioCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		MasterValidator validation = new MasterValidator();
		String parentPortfolioCode = inputDTO.get("PORTFOLIO_CODE");
		String portFolioCode = inputDTO.get("FOLIO_CODE");
		try {
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION,PARENT_PORTFOLIO_CODE");
			resultDTO = validation.validatePortFolioCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
			if (!validation.isEmpty(resultDTO.get("PARENT_PORTFOLIO_CODE"))) {
				if (portFolioCode.equals(resultDTO.get("PARENT_PORTFOLIO_CODE"))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.CHILD_OF_PARENT_NA);
					return resultDTO;
				}
			}
			inputDTO.set("PORTFOLIO_CODE", portFolioCode);
			inputDTO.set("PARENT_PORTFOLIO_CODE", parentPortfolioCode);
			String checkParentPortFolio = validateParentPortFolioCode(inputDTO);
			if (checkParentPortFolio.equals(RegularConstants.ONE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.ALREADY_EXIST_HIERARCHY);
				return resultDTO;
			}

		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public String validateParentPortFolioCode(DTObject inputDTO) {
		String sqlQuery = ContentManager.EMPTY_STRING;
		sqlQuery = "SELECT FN_CHK_PORTFOLIO_HIER_OCCUR(?,?,?)";
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		String checkParentPortFolio = null;
		try {
			util.setMode(DBUtil.PREPARED);
			util.setSql(sqlQuery);
			util.setString(1, context.getEntityCode());
			util.setString(2, inputDTO.get("PORTFOLIO_CODE"));
			util.setString(3, inputDTO.get("PARENT_PORTFOLIO_CODE"));
			ResultSet rs = util.executeQuery();
			if (rs.next()) {
				checkParentPortFolio = rs.getString(1);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			util.reset();
			dbContext.close();
		}
		return checkParentPortFolio;
	}

	public boolean validateCheckParentDisabled(String errorField) {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("PORTFOLIO_CODE", portFolioCode);
			formDTO = validateCheckParentDisabled(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError(errorField, formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError(errorField, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateCheckParentDisabled(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		try {
			if (checkDisabledCode(inputDTO)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.PARENT_CANNOT_DISABLED);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return resultDTO;
	}

	public boolean checkDisabledCode(DTObject inputDTO) {
		String sqlQuery = ContentManager.EMPTY_STRING;
		sqlQuery = "SELECT COUNT(1) FROM PORTFOLIO WHERE ENTITY_CODE=? AND PARENT_PORTFOLIO_CODE=?";
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		boolean disabledCode = false;
		try {
			util.setMode(DBUtil.PREPARED);
			util.setSql(sqlQuery);
			util.setString(1, context.getEntityCode());
			util.setString(2, inputDTO.get("PORTFOLIO_CODE"));
			ResultSet rs = util.executeQuery();
			if (rs.next()) {
				if (rs.getBoolean(1)) {
					disabledCode = true;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			util.reset();
			dbContext.close();
		}
		return disabledCode;
	}

	public boolean validateStatus() {
		CommonValidator validation = new CommonValidator();
		try {
			if (getAction().equals(MODIFY)) {
				if (validation.isEmpty(status)) {
					getErrorMap().setError("status", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!validation.isCMLOVREC("COMMON", "PORTFOLIO_STATUS", status)) {
					getErrorMap().setError("status", BackOfficeErrorCodes.INVALID_OPTION);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("status", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateRemarks() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (remarks != null && remarks.length() > 0) {
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			if (getAction().equals(MODIFY)) {
				if (commonValidator.isEmpty(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}
}
