package patterns.config.web.forms.cmn;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.MasterValidator;
import patterns.config.web.forms.GenericFormBean;

public class mifscbean extends GenericFormBean {

	private static final long serialVersionUID = -6219657363150466160L;
	private String ifscCode;
	private String bankName;
	private String branchName;
	private String address;
	private String micr;
	private String city;
	private String stateCode;
	private boolean enabled;
	private String remarks;
	
	
	
	public String getIfscCode() {
		return ifscCode;
	}
	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getMicr() {
		return micr;
	}
	public void setMicr(String micr) {
		this.micr = micr;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getStateCode() {
		return stateCode;
	}
	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}
	public boolean isEnabled() {
		return enabled;
	}
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	@Override
	public void reset() {
		ifscCode = RegularConstants.EMPTY_STRING;
		bankName = RegularConstants.EMPTY_STRING;
		branchName = RegularConstants.EMPTY_STRING;
		address = RegularConstants.EMPTY_STRING;
		micr = RegularConstants.EMPTY_STRING;
		city = RegularConstants.EMPTY_STRING;
		stateCode = RegularConstants.EMPTY_STRING;
		enabled = false;
		remarks = RegularConstants.EMPTY_STRING;
		setProcessBO("patterns.config.framework.bo.cmn.mifscBO");
	}
	@Override
	public void validate() {
		if (validateIFSCCode()) {
			validateBankName();
			validateBranchName();
			validateAddress();
			validateMicr();
			validateCity();
			validateStateCode();
			validateRemarks();
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.set("IFSC_CODE", ifscCode);
				formDTO.set("BANK_NAME", bankName);
				formDTO.set("BRANCH_NAME", branchName);
				formDTO.set("ADDRESS", address);
				formDTO.set("MICR_CODE", micr);
				formDTO.set("CITY", city);
				formDTO.set("STATE_CODE", stateCode);
				if (getAction().equals(ADD)) {
					formDTO.set("ENABLED", decodeBooleanToString(true));
				} else {
					formDTO.set("ENABLED", decodeBooleanToString(enabled));
				}
				formDTO.set("REMARKS", remarks);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("ifsc", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
	}
	public boolean validateIFSCCode() {
		try {
			
			DTObject formDTO = getFormDTO();
			formDTO.set("IFSC_CODE", ifscCode);
			formDTO.set(ContentManager.USER_ACTION, getAction());
			formDTO = validateIFSCCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("ifscCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("ifscCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} 
		return false;
	}
	public DTObject validateIFSCCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		MasterValidator validation = new MasterValidator();
		String ifscCode = inputDTO.get("IFSC_CODE");
		String action = inputDTO.get(ContentManager.USER_ACTION);
		try {
			if (validation.isEmpty(ifscCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH,ContentManager.FETCH_ALL);
			resultDTO = validation.validateIFSCCode(inputDTO);
			if (action.equals(ADD)) {
				if (ContentManager.DATA_AVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_EXISTS);
					return resultDTO;
				}
			} else {
				if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_NOT_EXISTS);
					return resultDTO;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}
	public boolean validateBankName() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(bankName)) {
				getErrorMap().setError("bankName", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (bankName.length() > 50) {
				getErrorMap().setError("bankName", BackOfficeErrorCodes.INVALID_LENGTH);
				return false;
			}
			
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("bankName", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}
	public boolean validateBranchName() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(branchName)) {
				getErrorMap().setError("branchName", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidDescription(branchName)) {
				getErrorMap().setError("branchName", BackOfficeErrorCodes.INVALID_DESCRIPTION);
				return false;
			}
			
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("branchName", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}
	public boolean validateAddress() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(address)) {
				getErrorMap().setError("address", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidRemarks(address)) {
				getErrorMap().setError("address", BackOfficeErrorCodes.INVALID_REMARKS);
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("address", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return true;
	}
	public boolean validateMicr() {
		CommonValidator validation = new CommonValidator();
		try {
			if (!validation.isEmpty(micr)) {
			if (!validation.isValidConciseDescription(micr)) {
				getErrorMap().setError("micr", BackOfficeErrorCodes.INVALID_DESCRIPTION);
				return false;
			}
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("micr", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return true;
	}
	public boolean validateCity() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(city)) {
				getErrorMap().setError("city", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidDescription(city)) {
				getErrorMap().setError("city", BackOfficeErrorCodes.INVALID_DESCRIPTION);
				return false;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("city", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return true;
	}
	public boolean validateStateCode() {
		try {
			DTObject formDTO = getFormDTO();
			formDTO.set("STATE_CODE", stateCode);
			formDTO.set(ContentManager.USER_ACTION, getAction());
			formDTO = validateStateCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("stateCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("stateCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} 
		return false;
	}
	public DTObject validateStateCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		MasterValidator validation = new MasterValidator();
		String stateCode = inputDTO.get("STATE_CODE");
		try {
			if (validation.isEmpty(stateCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH,ContentManager.FETCH_ALL);
			resultDTO = validation.validateStateCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
			return resultDTO;
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateRemarks() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (remarks != null && remarks.length() > 0) {
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			if (getAction().equals(MODIFY)) {
				if (commonValidator.isEmpty(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}
} 
