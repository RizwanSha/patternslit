/*
 *
 Author : ARULPRASATH A
 Created Date : 17-AUG-2016
 Spec Reference : PPBS/CMN/0023-MEMPBRN - Employee Branch (1-Master, Abstraction Program)
 Modification History
 ----------------------------------------------------------------------
 Sl.No		Modified Date	Author			Modified Changes	Version
 ----------------------------------------------------------------------
 
 */

package patterns.config.web.forms.cmn;

import java.sql.ResultSet;

import patterns.config.framework.bo.BackOfficeErrorCodes;

import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.process.TBAActionType;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.AccessValidator;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.MasterValidator;
import patterns.config.web.forms.GenericFormBean;

public class mempbrnbean extends GenericFormBean {

	private static final long serialVersionUID = 1L;

	private String employeeCode;
	private String currtOfficeCode;
	private String effectiveDate;
	private String newOfficeCode;
	private boolean enabled;
	private String remarks;

	public String getEmployeeCode() {
		return employeeCode;
	}

	public void setEmployeeCode(String employeeCode) {
		this.employeeCode = employeeCode;
	}

	public String getCurrtOfficeCode() {
		return currtOfficeCode;
	}

	public void setCurrtOfficeCode(String currtOfficeCode) {
		this.currtOfficeCode = currtOfficeCode;
	}

	public String getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public String getNewOfficeCode() {
		return newOfficeCode;
	}

	public void setNewOfficeCode(String newOfficeCode) {
		this.newOfficeCode = newOfficeCode;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	@Override
	public void reset() {
		employeeCode = RegularConstants.EMPTY_STRING;
		currtOfficeCode = RegularConstants.EMPTY_STRING;
		effectiveDate = RegularConstants.EMPTY_STRING;
		newOfficeCode = RegularConstants.EMPTY_STRING;
		enabled = false;
		remarks = RegularConstants.EMPTY_STRING;
		setProcessBO("patterns.config.framework.bo.cmn.mempbrnBO");
	}

	@Override
	public void validate() {
		if (validateEmployeeCode() && validateEffectiveDate()) {
			validateNewOfficeCode();
			validateRemarks();
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.set("EMP_CODE", employeeCode);
				formDTO.set("EFF_DATE", effectiveDate);
				formDTO.set("BRANCH_CODE", newOfficeCode);
				if (getAction().equals(ADD)) {
					formDTO.set("ENABLED", decodeBooleanToString(true));
				} else {
					formDTO.set("ENABLED", decodeBooleanToString(enabled));
				}
				formDTO.set("REMARKS", remarks);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("employeeCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
	}

	public boolean validateEmployeeCode() {
		try {
			DTObject formDTO = getFormDTO();
			formDTO.set("EMP_CODE", employeeCode);
			formDTO.set(ContentManager.USER_ACTION, USAGE);
			formDTO = validateEmployeeCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("employeeCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("employeeCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateEmployeeCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		AccessValidator validation = new AccessValidator();
		MasterValidator validator = new MasterValidator();
		String employeeCode = inputDTO.get("EMP_CODE");
		String branchCode = null;
		try {
			if (validation.isEmpty(employeeCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "PERSON_NAME");
			resultDTO = validation.validateEmployeeCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}

			String empName = resultDTO.get("PERSON_NAME");
			inputDTO.set(ContentManager.FETCH_COLUMNS, "RELIEVED_ON_DATE");
			inputDTO.set("EMP_ID", employeeCode);
			resultDTO = validation.validateEmployeeExit(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.PBS_EMP_RELIEVED);
				return resultDTO;
			}
			branchCode = validateCurrentOfficeCode(inputDTO);
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
			inputDTO.set("BRANCH_CODE", branchCode);
			resultDTO = validation.validateEntityBranch(inputDTO);
			resultDTO.set("BRANCH_CODE", branchCode);
			resultDTO.set("DESCRIPTION", resultDTO.get("DESCRIPTION"));
			resultDTO.set("PERSON_NAME", empName);
			resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
			validator.close();
		}
		return resultDTO;
	}

	public String validateCurrentOfficeCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		String branchCode = null;
		try {
			String sqlQuery = "SELECT BRANCH_CODE FROM EMPBRN WHERE ENTITY_CODE=? AND EMP_CODE=? ";
			util.reset();
			util.setSql(sqlQuery);
			util.setLong(1, Long.parseLong(context.getEntityCode()));
			util.setString(2, inputDTO.get("EMP_CODE"));
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				branchCode = rset.getString(1);
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			dbContext.close();
			util.reset();
		}
		return branchCode;
	}

	public boolean validateEffectiveDate() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set(ContentManager.ACTION, getAction());
			formDTO.set("EMP_CODE", employeeCode);
			formDTO.set("EFF_DATE", effectiveDate);
			formDTO = validateEffectiveDate(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("effectiveDate", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("effectiveDate", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateEffectiveDate(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		CommonValidator validator = new CommonValidator();
		String employeeCode = inputDTO.get("EMP_CODE");
		String effectiveDate = inputDTO.get("EFF_DATE");
		String action = inputDTO.get(ContentManager.USER_ACTION);
		try {
			if (validator.isEmpty(effectiveDate)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			TBAActionType AT;
			if (action.equals(ADD))
				AT = TBAActionType.ADD;
			else
				AT = TBAActionType.MODIFY;
			if (!validator.isEffectiveDateValid(effectiveDate, AT)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_EFFECTIVE_DATE);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "EMP_CODE,ENABLED");
			inputDTO.set(ContentManager.TABLE, "EMPBRNHIST");
			String columns[] = new String[] { context.getEntityCode(), employeeCode, effectiveDate };
			inputDTO.setObject(ContentManager.PROGRAM_KEY, columns);
			resultDTO = validator.validatePK(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (action.equals(ADD)) {
				if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_EXISTS);
					return resultDTO;
				}
			} else if (action.equals(MODIFY)) {
				if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_NOT_EXISTS);
					return resultDTO;
				}
				if (RegularConstants.ZERO.equals(resultDTO.get("ENABLED"))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.CODE_CLOSED);
					return resultDTO;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return resultDTO;
	}

	public boolean validateNewOfficeCode() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("CURR_BRANCH_CODE", currtOfficeCode);
			formDTO.set("BRANCH_CODE", newOfficeCode);
			formDTO.set(ContentManager.USER_ACTION, USAGE);
			formDTO = validateNewOfficeCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("newOfficeCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("newOfficeCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateNewOfficeCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		AccessValidator validation = new AccessValidator();
		String currtOfficeCode = inputDTO.get("CURR_BRANCH_CODE");
		String newOfficeCode = inputDTO.get("BRANCH_CODE");
		try {
			if (validation.isEmpty(newOfficeCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			if (newOfficeCode.equals(currtOfficeCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.PBS_CURR_NEW_OFF_CODE_SHOULD_NOT_SAME);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
			resultDTO = validation.validateEntityBranch(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateRemarks() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (remarks != null && remarks.length() > 0) {
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			if (getAction().equals(MODIFY)) {
				if (commonValidator.isEmpty(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}

	public DTObject validateEmployeeCodeDetails(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		AccessValidator validation = new AccessValidator();
		try {
			String branchCode = validateCurrentOfficeCode(inputDTO);
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
			inputDTO.set("BRANCH_CODE", branchCode);
			resultDTO = validation.validateEntityBranch(inputDTO);
			resultDTO.set("BRANCH_CODE", branchCode);
			resultDTO.set("DESCRIPTION", resultDTO.get("DESCRIPTION"));
			resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}
}
