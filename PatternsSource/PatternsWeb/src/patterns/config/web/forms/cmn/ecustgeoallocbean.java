package patterns.config.web.forms.cmn;

import java.sql.ResultSet;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.process.TBAActionType;
import patterns.config.framework.service.DTDObject;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.DHTMLXGridUtility;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.AccessValidator;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.MasterValidator;
import patterns.config.web.forms.GenericFormBean;

public class ecustgeoallocbean extends GenericFormBean {

	private static final long serialVersionUID = 1L;
	private String cusotmerIdFoNo;
	private String effectiveDate;
	private String xmlCustGrid;
	private String address;
	private String pincode;

	
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPincode() {
		return pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public String getXmlCustGrid() {
		return xmlCustGrid;
	}

	public void setXmlCustGrid(String xmlCustGrid) {
		this.xmlCustGrid = xmlCustGrid;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	private boolean enabled;
	private String remarks;

	public String getCusotmerIdFoNo() {
		return cusotmerIdFoNo;
	}

	public void setCusotmerIdFoNo(String cusotmerIdFoNo) {
		this.cusotmerIdFoNo = cusotmerIdFoNo;
	}

	public String getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	private String cifGeoUnitId;

	@Override
	public void reset() {
		// TODO Auto-generated method stub
		cusotmerIdFoNo = RegularConstants.EMPTY_STRING;
		effectiveDate = RegularConstants.EMPTY_STRING;
		enabled = false;
		xmlCustGrid=RegularConstants.EMPTY_STRING;
		remarks = RegularConstants.EMPTY_STRING;
		cifGeoUnitId =RegularConstants.EMPTY_STRING;
		setProcessBO("patterns.config.framework.bo.cmn.ecustgeoallocBO");
	}

	@Override
	public void validate() {
		if (validateCustomerIdNo() && validateEffectiveDate()) {
			validateRemarks();
			validateGrid();
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.set("CIF_NO", cusotmerIdFoNo);
				formDTO.set("EFF_DATE", effectiveDate);
				if (getAction().equals(ADD)) {
					formDTO.set("ENABLED", decodeBooleanToString(true));
				} else {
					formDTO.set("ENABLED", decodeBooleanToString(enabled));
				}
				formDTO.set("REMARKS", remarks);
				DTDObject dtdObject = new DTDObject();
				dtdObject.addColumn(0, "SL");
				dtdObject.addColumn(1, "GEO_REGION_CODE");
				dtdObject.addColumn(2, "DESCRIPTION");
				dtdObject.addColumn(3, "PINCODE_GEOUNITID");
				dtdObject.addColumn(4, "OFFICE_CODE");
				dtdObject.setXML(xmlCustGrid);
				formDTO.setDTDObject("CUSTGEOALLOCHISTDTL", dtdObject);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("cusotmerIdFoNo", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
	}

	public boolean validateCustomerIdNo() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("CIF_NO", cusotmerIdFoNo);
			 formDTO.set("FETCH_REGIONS", RegularConstants.COLUMN_DISABLE);
			formDTO.set(ContentManager.USER_ACTION, USAGE);
			formDTO = validateCustomerIdNo(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("cusotmerIdFoNo", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("cusotmerIdFoNo", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateCustomerIdNo(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		DTObject gridDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		MasterValidator validation = new MasterValidator();
		String cusotmerIdFoNo = inputDTO.get("CIF_NO");
		try {
			if (validation.isEmpty(cusotmerIdFoNo)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "NAME,ADDRESS,GEO_UNIT_ID");
			resultDTO = validation.validateCIF(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
			
			cifGeoUnitId = resultDTO.get("GEO_UNIT_ID"); 
			inputDTO.set("PIN_CODE", resultDTO.get("GEO_UNIT_ID"));
			   if (inputDTO.get("FETCH_REGIONS").equals(RegularConstants.COLUMN_ENABLE)) {
					gridDTO = loadGridValues(inputDTO, validation.getDbContext().createUtilInstance());
					if (gridDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
						//resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.PBS_REGION_DOES_NOT_EXIST_FOR_CIF);
						resultDTO.set("GEOREGION_ERROR", getErrorResource(BackOfficeErrorCodes.PBS_REGION_DOES_NOT_EXIST_FOR_CIF, null));
						return resultDTO;
					}
					resultDTO.set(ContentManager.RESULT_XML, gridDTO.get(ContentManager.RESULT_XML));
			   }
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateEffectiveDate() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set(ContentManager.USER_ACTION, getAction());
			formDTO.set("CIF_NO", cusotmerIdFoNo);
			formDTO.set("EFF_DATE", effectiveDate);
			formDTO = validateEffectiveDate(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("effectiveDate", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("effectiveDate", BackOfficeErrorCodes.UNSPECIFIED_ERROR);

		}
		return false;
	}

	public DTObject validateEffectiveDate(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		CommonValidator commonvalidation = new CommonValidator();
		String cusotmerIdFoNo = inputDTO.get("CIF_NO");
		String effectiveDate = inputDTO.get("EFF_DATE");
		String action = inputDTO.get(ContentManager.USER_ACTION);
		try {
			if (commonvalidation.isEmpty(effectiveDate)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			TBAActionType AT;
			if (action.equals(ADD))
				AT = TBAActionType.ADD;
			else
				AT = TBAActionType.MODIFY;
			if (!commonvalidation.isEffectiveDateValid(effectiveDate, AT)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_EFFECTIVE_DATE);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "CIF_NO,ENABLED");
			inputDTO.set(ContentManager.TABLE, "CUSTGEOALLOCHIST");
			String columns[] = new String[] { context.getEntityCode(), cusotmerIdFoNo, effectiveDate };
			inputDTO.setObject(ContentManager.PROGRAM_KEY, columns);
			resultDTO = commonvalidation.validatePK(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (action.equals(ADD)) {
				if (ContentManager.DATA_AVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_EXISTS);
					return resultDTO;
				}
			} else if (action.equals(MODIFY)) {
				if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_NOT_EXISTS);
					return resultDTO;
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonvalidation.close();
		}
		return resultDTO;
	}


	

	public DTObject loadGridValues(DTObject inputDTO,DBUtil util) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		StringBuffer sqlQuery = new StringBuffer(ContentManager.EMPTY_STRING);
		sqlQuery.append("SELECT G.GEO_REGION_CODE,G.DESCRIPTION,G.PINCODE_GEOUNITID,G.OFFICE_CODE  ");
		sqlQuery.append(" FROM GEOREGIONS G");
		sqlQuery.append(" WHERE G.PINCODE_GEOUNITID=? AND G.ENABLED='1'");
		DHTMLXGridUtility gridUtility = new DHTMLXGridUtility();
		String resultXML = RegularConstants.EMPTY_STRING;
		try {
			util.setMode(DBUtil.PREPARED);
			util.setSql(sqlQuery.toString());
			util.setString(1, inputDTO.get("PIN_CODE"));
			ResultSet rs = util.executeQuery();
			gridUtility.init();
			if(rs.next()){
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				do{
					gridUtility.startRow(rs.getString("GEO_REGION_CODE"));
					gridUtility.setCell(RegularConstants.ZERO);
					gridUtility.setCell(rs.getString("GEO_REGION_CODE"));
					gridUtility.setCell(rs.getString("DESCRIPTION"));
					gridUtility.setCell(rs.getString("PINCODE_GEOUNITID"));
					gridUtility.setCell(rs.getString("OFFICE_CODE"));
					gridUtility.endRow();
				}while(rs.next());
			}	
			gridUtility.finish();
			resultXML = gridUtility.getXML();
			resultDTO.set(ContentManager.RESULT_XML, resultXML);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			util.reset();
		}
		return resultDTO;
	}

	public DTObject loadGridValues(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		CommonValidator validator= new CommonValidator();
		try {
			resultDTO=loadGridValues(inputDTO,validator.getDbContext().createUtilInstance());
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			validator.close();
		}
		return resultDTO;
	}
	
	public boolean validateRemarks() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (remarks != null && remarks.length() > 0) {
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			if (getAction().equals(MODIFY)) {
				if (commonValidator.isEmpty(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}

	private boolean validateGrid() {
		CommonValidator validation = new CommonValidator();
		DTObject resultDTO = new DTObject();
		try {
			DTDObject dtdObject = new DTDObject();
			dtdObject.addColumn(0, "SL");
			dtdObject.addColumn(1, "GEO_REGION_CODE");
			dtdObject.addColumn(2, "DESCRIPTION");
			dtdObject.addColumn(3, "PINCODE_GEOUNITID");
			dtdObject.addColumn(4, "OFFICE_CODE");
			dtdObject.setXML(xmlCustGrid);
			if (dtdObject.getRowCount() <= 0) {
				getErrorMap().setError("gridCheck", BackOfficeErrorCodes.ATLEAST_ONE_ROW);
				return false;
			}
			int checkAtleastOneSel = 0;
			for (int i = 0; i < dtdObject.getRowCount(); i++) {
				if (dtdObject.getValue(i, 0).equals(RegularConstants.ONE)) {
					checkAtleastOneSel++;
					resultDTO=validateGeoRegionCode(dtdObject.getValue(i, 1));
					if(resultDTO.get(ContentManager.ERROR)!=null){
						if(resultDTO.get(ContentManager.ERROR_PARAM)!=null){
							Object[] errorParam = { resultDTO.get(ContentManager.ERROR_PARAM)};
							getErrorMap().setError("gridCheck", resultDTO.get(ContentManager.ERROR),errorParam);
						}else{
							getErrorMap().setError("gridCheck", resultDTO.get(ContentManager.ERROR));
						}
						return false;
					}
				}
				
			}
			
			if (checkAtleastOneSel == 0) {
				getErrorMap().setError("gridCheck", BackOfficeErrorCodes.SELECT_ATLEAST_ONE);
				return false;
			}
				
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("gridCheck", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}

		return true;
		
	}
	
	public DTObject validateGeoRegionCode(String geoRegionCode) {
		DTObject resultDTO = new DTObject();
		DTObject inputDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		MasterValidator validation = new MasterValidator();
		
		try {
			inputDTO.set(ContentManager.FETCH_COLUMNS, "PINCODE_GEOUNITID");
			inputDTO.set("GEO_REGION_CODE",geoRegionCode);
			inputDTO.set(ContentManager.ACTION,USAGE);
			resultDTO = validation.validateGeoRegionCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
			if(!cifGeoUnitId.equals(resultDTO.get("PINCODE_GEOUNITID"))){
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.PBS_REGION_CIFNO_PIN_MATCH);
				resultDTO.set(ContentManager.ERROR_PARAM,geoRegionCode);
				return resultDTO;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}
}
