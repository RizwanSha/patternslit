package patterns.config.web.forms.cmn;

import java.sql.ResultSet;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.MasterValidator;
import patterns.config.web.forms.GenericFormBean;

public class mgeounitconbean extends GenericFormBean {
	private static final long serialVersionUID = 1L;
	private String countryCode;
	private String geoUnitID;
	private String name;
	private String shortName;
	private String parentgeoUnitID;
	private String geoStructure;
	private String parentGeoStructure;
	private String geoUnitType;
	private String remarks;
	private String unitTypeAllowed;
	private boolean enabled;

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getUnitTypeAllowed() {
		return unitTypeAllowed;
	}

	public void setUnitTypeAllowed(String unitTypeAllowed) {
		this.unitTypeAllowed = unitTypeAllowed;
	}

	public String getParentGeoStructure() {
		return parentGeoStructure;
	}

	public void setParentGeoStructure(String parentGeoStructure) {
		this.parentGeoStructure = parentGeoStructure;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getGeoUnitID() {
		return geoUnitID;
	}

	public void setGeoUnitID(String geoUnitID) {
		this.geoUnitID = geoUnitID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public String getParentgeoUnitID() {
		return parentgeoUnitID;
	}

	public void setParentgeoUnitID(String parentgeoUnitID) {
		this.parentgeoUnitID = parentgeoUnitID;
	}

	public String getGeoStructure() {
		return geoStructure;
	}

	public void setGeoStructure(String geoStructure) {
		this.geoStructure = geoStructure;
	}

	public String getGeoUnitType() {
		return geoUnitType;
	}

	public void setGeoUnitType(String geoUnitType) {
		this.geoUnitType = geoUnitType;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public void reset() {
		countryCode = RegularConstants.EMPTY_STRING;
		geoUnitID = RegularConstants.EMPTY_STRING;
		name = RegularConstants.EMPTY_STRING;
		shortName = RegularConstants.EMPTY_STRING;
		parentgeoUnitID = RegularConstants.EMPTY_STRING;
		geoStructure = RegularConstants.EMPTY_STRING;
		geoUnitType = RegularConstants.EMPTY_STRING;
		enabled = false;
		remarks = RegularConstants.EMPTY_STRING;
		setProcessBO("patterns.config.framework.bo.cmn.mgeounitconBO");
	}

	public void validate() {
		if (validateCountryCode() && validateGeoUnitID()) {
			validateName();
			validateShortName();
			validateParentGeoUnitID();
			validateGeoStructure();
			validateGeoUnitType();
			validateRemarks();
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("COUNTRY_CODE", countryCode);
				formDTO.set("GEO_UNIT_ID", geoUnitID);
				formDTO.set("DESCRIPTION", name);
				formDTO.set("CONCISE_DESCRIPTION", shortName);
				formDTO.set("PARENT_GEO_UNIT_ID", parentgeoUnitID);
				formDTO.set("GEO_UNIT_STRUC_CODE", geoStructure);
				if (geoUnitType.equals(RegularConstants.EMPTY_STRING))
					formDTO.set("GEO_UNIT_TYPE", null);
				else
					formDTO.set("GEO_UNIT_TYPE", geoUnitType);
				if (getAction().equals(ADD)) {
					formDTO.set("ENABLED", decodeBooleanToString(true));
				} else {
					formDTO.set("ENABLED", decodeBooleanToString(enabled));
				}
				formDTO.set("REMARKS", remarks);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("countryCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
	}

	public boolean validateCountryCode() {
		CommonValidator validator = new CommonValidator();
		try {
			if (validator.isEmpty(countryCode)) {
				getErrorMap().setError("countryCode", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			DTObject formDTO = new DTObject();
			formDTO.set("COUNTRY_CODE", countryCode);
			formDTO.set(ContentManager.ACTION, USAGE);
			formDTO = validateCountryCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("countryCode", formDTO.get(ContentManager.ERROR));
				return false;
			}

			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("countryCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return false;
	}

	public DTObject validateCountryCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		MasterValidator validation = new MasterValidator();
		try {
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION,GEO_UNIT_STRUC_REQ");
			resultDTO = validation.validateCountryCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
			if (resultDTO.get("GEO_UNIT_STRUC_REQ").equals(RegularConstants.ZERO)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.PBS_CODE_NTR_GEO_ORG);
				return resultDTO;
			}

		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateGeoUnitID() {
		CommonValidator validator = new CommonValidator();
		try {
			if (validator.isEmpty(geoUnitID)) {
				getErrorMap().setError("geoUnitID", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			DTObject formDTO = new DTObject();
			formDTO.set("COUNTRY_CODE", countryCode);
			formDTO.set("GEO_UNIT_ID", geoUnitID);
			formDTO.set(ContentManager.ACTION, getAction());
			formDTO = validateGeoUnitID(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("geoUnitID", formDTO.get(ContentManager.ERROR));
				return false;
			}

			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("geoUnitID", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return false;
	}

	public DTObject validateGeoUnitID(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		MasterValidator validation = new MasterValidator();
		String action = inputDTO.get(ContentManager.ACTION);
		try {
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION,ENABLED");
			resultDTO = validation.validateGeographicalUnitId(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (action.equals(ADD)) {
				if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_EXISTS);
				}
				
			} else {
				if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_NOT_EXISTS);
				}
				if (resultDTO.get("ENABLED").equals(RegularConstants.COLUMN_DISABLE)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.CODE_CLOSED);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateName() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(name)) {
				getErrorMap().setError("name", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidDescription(name)) {
				getErrorMap().setError("name", BackOfficeErrorCodes.INVALID_NAME);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("name", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateShortName() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(shortName)) {
				getErrorMap().setError("shortName", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidConciseDescription(shortName)) {
				getErrorMap().setError("shortName", BackOfficeErrorCodes.PBS_INVALID_SHORT_NAME);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("shortName", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateParentGeoUnitID() {
		CommonValidator validation = new CommonValidator();
		try {
			DTObject formDTO = new DTObject();
			if (!validation.isEmpty(parentgeoUnitID)) {
				if (geoUnitID.equals(parentgeoUnitID)) {
					getErrorMap().setError("parentgeoUnitID", BackOfficeErrorCodes.PBS_GEOUNIT_NT_SAME_PARENT);
					return false;
				}
				formDTO.set("COUNTRY_CODE", countryCode);
				formDTO.set("GEO_UNIT_ID", parentgeoUnitID);
				formDTO.set("UNIT_ID", geoUnitID);
				formDTO.set(ContentManager.ACTION, USAGE);
				formDTO = validateParentGeoUnitID(formDTO);
				if (formDTO.get(ContentManager.ERROR) != null) {
					getErrorMap().setError("parentgeoUnitID", formDTO.get(ContentManager.ERROR));
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("parentgeoUnitID", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public DTObject validateParentGeoUnitID(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		DTObject ckeckDTO = new DTObject();
		MasterValidator validation = new MasterValidator();
		try {
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
			resultDTO = validation.validateGeographicalUnitId(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
			ckeckDTO = isChildCheck(inputDTO);
			if (ckeckDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, ckeckDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (ckeckDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
				if (ckeckDTO.get("PARENT_GEO_UNIT_ID").equals(inputDTO.getObject("UNIT_ID"))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.PBS_CHILD_GEOUNIT_NT_USE_PARENT);
					return resultDTO;
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateGeoStructure() {
		CommonValidator validator = new CommonValidator();
		try {
			if (validator.isEmpty(geoStructure)) {
				getErrorMap().setError("geoStructure", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			DTObject formDTO = new DTObject();
			formDTO.set("COUNTRY_CODE", countryCode);
			formDTO.set("GEO_UNIT_STRUC_CODE", geoStructure);
			formDTO.set(ContentManager.ACTION, USAGE);
			formDTO = validateGeoStructure(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("geoStructure", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("geoStructure", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return false;
	}

	public DTObject validateGeoStructure(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		DTObject result = new DTObject();
		MasterValidator validation = new MasterValidator();
		try {
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION,UNIT_TYPE_REQ,PARENT_GUS_CODE");
			resultDTO = validation.validateGeographicalUnitStructureCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
			unitTypeAllowed = resultDTO.get("UNIT_TYPE_REQ");
			result = checkCodeBelongsCountry(inputDTO);
			if (result.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, result.get(ContentManager.ERROR));
				return resultDTO;
			}

			if (!validation.isEmpty(resultDTO.get("PARENT_GUS_CODE"))) {
				inputDTO.set("GEO_UNIT_STRUC_CODE", resultDTO.get("PARENT_GUS_CODE"));
				inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
				result = validation.validateGeographicalUnitStructureCode(inputDTO);
				if (result.get(ContentManager.ERROR) != null) {
					resultDTO.set(ContentManager.ERROR, result.get(ContentManager.ERROR));
					return resultDTO;
				}
				if (result.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
					return resultDTO;
				}
				resultDTO.set("PARENT_DESCRIPTION", result.get("DESCRIPTION"));
			}

		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateGeoUnitType() {
		MasterValidator validation = new MasterValidator();
		try {
			if (unitTypeAllowed.equals(RegularConstants.ONE)) {
				if (validation.isEmpty(geoUnitType)) {
					getErrorMap().setError("geoUnitType", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				DTObject formDTO = new DTObject();
				formDTO.set("GEO_UNIT_STRUC_CODE", geoStructure);
				formDTO.set("GEO_UNIT_TYPE", geoUnitType);
				formDTO.set(ContentManager.ACTION, USAGE);
				formDTO = validateGeoUnitType(formDTO);
				if (formDTO.get(ContentManager.ERROR) != null) {
					getErrorMap().setError("geoUnitType", formDTO.get(ContentManager.ERROR));
					return false;
				}
			} else {
				if (!validation.isEmpty(geoUnitType)) {
					getErrorMap().setError("geoUnitType", BackOfficeErrorCodes.PBS_VALUE_NOT_ALLOWED);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("geoUnitType", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public DTObject validateGeoUnitType(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		MasterValidator validation = new MasterValidator();
		try {
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION,ENABLED");
			resultDTO = validation.validateGeographicalUnitType(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
			if (resultDTO.get("ENABLED").equals(RegularConstants.COLUMN_DISABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.CODE_CLOSED);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateRemarks() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (remarks != null && remarks.length() > 0) {
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			if (getAction().equals(MODIFY)) {
				if (commonValidator.isEmpty(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}

	public DTObject isChildCheck(DTObject inputDTO) {
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		inputDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		try {
			String sql = "SELECT PARENT_GEO_UNIT_ID FROM GEOUNITCON WHERE GEO_UNIT_ID=?";
			util.reset();
			util.setMode(DBUtil.PREPARED);
			util.setSql(sql);
			util.setString(1, inputDTO.get("GEO_UNIT_ID"));
			ResultSet rs = util.executeQuery();
			if (rs.next()) {
				inputDTO.set("PARENT_GEO_UNIT_ID", rs.getString("PARENT_GEO_UNIT_ID"));
				inputDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			inputDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			dbContext.close();
		}
		return inputDTO;
	}

	public DTObject checkCodeBelongsCountry(DTObject inputDTO) {
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		int count = 0;
		try {
			String sql = "SELECT COUNT(1)  FROM COUNTRY WHERE COUNTRY_CODE=? AND GEO_UNIT_STRUC_CODE=?";
			util.reset();
			util.setMode(DBUtil.PREPARED);
			util.setSql(sql);
			util.setString(1, inputDTO.get("COUNTRY_CODE"));
			util.setString(2, inputDTO.get("GEO_UNIT_STRUC_CODE"));
			ResultSet rs = util.executeQuery();
			if (rs.next()) {
				count = rs.getInt(1);
			}
			if (count == 0) {
				inputDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.PBS_GEOSTRUC_NT_BELONG_BRANCH);
				return inputDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			inputDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			dbContext.close();
		}
		return inputDTO;
	}

}