package patterns.config.web.forms.cmn;

import java.sql.ResultSet;
import java.util.ArrayList;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.GenericOption;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.MasterValidator;
import patterns.config.web.forms.GenericFormBean;

/**
 * 
 * 
 * @version 1.0
 * @author Dileep Kumar Reddy T
 * @since 30-Nov-2016 <br>
 *        <b>Modified History</b> <BR>
 *        <U><B> Sl.No Modified Date Author Modified Changes Version </B></U> <br>
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 */
public class mintratetypesbean extends GenericFormBean {

	private static final long serialVersionUID = 1L;

	private String interestRateTypeCode;
	private String description;
	private String conciseDescription;
	private boolean standardRate;
	private String standardRateTypeCode;
	private String currencyCode;
	private String internalExternal;
	private boolean interestRateByTenors;
	private String interestRatePurpose;
	private String loanInterestRate;
	private boolean enabled;
	private String remarks;

	public String getInterestRateTypeCode() {
		return interestRateTypeCode;
	}

	public void setInterestRateTypeCode(String interestRateTypeCode) {
		this.interestRateTypeCode = interestRateTypeCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getConciseDescription() {
		return conciseDescription;
	}

	public void setConciseDescription(String conciseDescription) {
		this.conciseDescription = conciseDescription;
	}

	public boolean isStandardRate() {
		return standardRate;
	}

	public void setStandardRate(boolean standardRate) {
		this.standardRate = standardRate;
	}

	public String getStandardRateTypeCode() {
		return standardRateTypeCode;
	}

	public void setStandardRateTypeCode(String standardRateTypeCode) {
		this.standardRateTypeCode = standardRateTypeCode;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public String getInternalExternal() {
		return internalExternal;
	}

	public void setInternalExternal(String internalExternal) {
		this.internalExternal = internalExternal;
	}

	public boolean isInterestRateByTenors() {
		return interestRateByTenors;
	}

	public void setInterestRateByTenors(boolean interestRateByTenors) {
		this.interestRateByTenors = interestRateByTenors;
	}

	public String getInterestRatePurpose() {
		return interestRatePurpose;
	}

	public void setInterestRatePurpose(String interestRatePurpose) {
		this.interestRatePurpose = interestRatePurpose;
	}

	public String getLoanInterestRate() {
		return loanInterestRate;
	}

	public void setLoanInterestRate(String loanInterestRate) {
		this.loanInterestRate = loanInterestRate;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void reset() {
		interestRateTypeCode = RegularConstants.EMPTY_STRING;
		description = RegularConstants.EMPTY_STRING;
		conciseDescription = RegularConstants.EMPTY_STRING;
		standardRate = false;
		standardRateTypeCode = RegularConstants.EMPTY_STRING;
		currencyCode = RegularConstants.EMPTY_STRING;
		internalExternal = RegularConstants.EMPTY_STRING;
		interestRateByTenors = false;
		interestRatePurpose = RegularConstants.EMPTY_STRING;
		loanInterestRate = RegularConstants.EMPTY_STRING;
		enabled = false;
		remarks = RegularConstants.EMPTY_STRING;
		DBContext dbContext = new DBContext();
		ArrayList<GenericOption> internalExternalList = getGenericOptions("MINTRATETYPES", "INT_EXT", dbContext, null);
		webContext.getRequest().setAttribute("MINTRATETYPES_INT_EXT", internalExternalList);

		ArrayList<GenericOption> ratePurposeList = getGenericOptions("MINTRATETYPES", "INT_RATE_PURP", dbContext, null);
		webContext.getRequest().setAttribute("MINTRATETYPES_INT_RATE_PURP", ratePurposeList);

		ArrayList<GenericOption> loanRateList = getGenericOptions("MINTRATETYPES", "LOAN_INT_RATE_PURP", dbContext, "--");
		webContext.getRequest().setAttribute("MINTRATETYPES_LOAN_INT_RATE_PURP", loanRateList);

		dbContext.close();
		setProcessBO("patterns.config.framework.bo.cmn.mintratetypesBO");
	}

	public void validate() {
		if (validateInterestRateType()) {
			validateDescription();
			validateCurrencyCode();
			validateStandardRate();
			validateStandardRateTypeCode();
			validateConciseDescription();
			validateInternalExternal();
			validateInterestRatePurpose();
			validateLoanInterestRate();
			validateRemarks();
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.reset();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.set("INT_RATE_TYPE_CODE", interestRateTypeCode);
				formDTO.set("DESCRIPTION", description);
				formDTO.set("CONCISE_DESCRIPTION", conciseDescription);
				formDTO.set("STANDARD_RATE", decodeBooleanToString(standardRate));
				if (!standardRate)
					formDTO.set("STD_INT_RATE_TYPE_CODE", standardRateTypeCode);
				formDTO.set("INT_RATE_CCY_CODE", currencyCode);
				formDTO.set("INTERNAL_EXTERNAL", internalExternal);
				formDTO.set("INT_RATES_BY_TENOR", decodeBooleanToString(interestRateByTenors));
				formDTO.set("INT_RATE_PURPOSE", interestRatePurpose);
				if ("L".equals(interestRatePurpose))
					formDTO.set("LOAN_INT_RATE_PURPOSE", loanInterestRate);
				if (getAction().equals(ADD)) {
					formDTO.set("ENABLED", decodeBooleanToString(true));
				} else {
					formDTO.set("ENABLED", decodeBooleanToString(enabled));
				}
				formDTO.set("REMARKS", remarks);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("interestRateTypeCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
	}

	private boolean validateInterestRateType() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("INT_RATE_TYPE_CODE", interestRateTypeCode);
			formDTO.set("ACTION", getAction());
			formDTO = validateinterestRateTypeCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("interestRateTypeCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("interestRateTypeCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateinterestRateTypeCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		MasterValidator validation = new MasterValidator();
		String interestRateTypeCode = inputDTO.get("INT_RATE_TYPE_CODE");
		String action = inputDTO.get(ContentManager.ACTION);
		try {
			if (validation.isEmpty(interestRateTypeCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
			resultDTO = validation.validateInterestRateTypeCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (action.equals(ADD)) {
				if (ContentManager.DATA_AVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_EXISTS);
					return resultDTO;
				}
			} else {
				if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_NOT_EXISTS);
					return resultDTO;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateDescription() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(description)) {
				getErrorMap().setError("description", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidDescription(description)) {
				getErrorMap().setError("description", BackOfficeErrorCodes.INVALID_DESCRIPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("description", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	private boolean validateConciseDescription() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(conciseDescription)) {
				getErrorMap().setError("conciseDescription", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidConciseDescription(conciseDescription)) {
				getErrorMap().setError("conciseDescription", BackOfficeErrorCodes.INVALID_CONCISE_DESCRIPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("conciseDescription", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	private boolean validateStandardRate() {
		if (!standardRate) {
			DTObject formDTO = new DTObject();
			formDTO.set("INT_RATE_TYPE_CODE", interestRateTypeCode);
			formDTO.set(ContentManager.ACTION, getAction());
			formDTO = validateInterestRateIsStandard(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("standardRate", formDTO.get(ContentManager.ERROR));
				return false;
			}
		}
		return true;
	}

	public DTObject validateInterestRateIsStandard(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.NULL);
		MasterValidator validation = new MasterValidator();
		String standardRateTypeCode = inputDTO.get("INT_RATE_TYPE_CODE");
		String action = inputDTO.get(ContentManager.ACTION);
		DBUtil util = validation.getDbContext().createUtilInstance();
		String sql = RegularConstants.EMPTY_STRING;
		try {
			if (action.equals(MODIFY)) {
				if (validation.isEmpty(standardRateTypeCode)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.PBS_INTEREST_RATE_TYPE_MANDATORY);
					return resultDTO;
				} else {
					sql = "SELECT COUNT(1) FROM INTRATETYPES WHERE ENTITY_CODE=? AND STD_INT_RATE_TYPE_CODE=?";
					util.reset();
					util.setSql(sql);
					util.setLong(1, Long.parseLong(context.getEntityCode()));
					util.setString(2, standardRateTypeCode);
					ResultSet rs = util.executeQuery();
					if (rs.next()) {
						if (rs.getInt(1) != 0) {
							resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.PBS_USED_AS_STD_RATE);
							return resultDTO;
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			util.reset();
			validation.close();
		}
		return resultDTO;
	}

	private boolean validateStandardRateTypeCode() {
		try {
			if (!standardRate) {
				DTObject formDTO = new DTObject();
				formDTO.set(ContentManager.ACTION, USAGE);
				formDTO.set("STD_INT_RATE_TYPE_CODE", standardRateTypeCode);
				formDTO = validatestandardRateTypeCode(formDTO);
				if (formDTO.get(ContentManager.ERROR) != null) {
					getErrorMap().setError("standardRateTypeCode", formDTO.get(ContentManager.ERROR));
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("standardRateTypeCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validatestandardRateTypeCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		DTObject currencyDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		MasterValidator validation = new MasterValidator();
		String standardRateTypeCode = inputDTO.get("STD_INT_RATE_TYPE_CODE");
		try {
			if (!validation.isEmpty(standardRateTypeCode)) {
				inputDTO.set("INT_RATE_TYPE_CODE", standardRateTypeCode);
				inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION,STANDARD_RATE,INT_RATE_CCY_CODE");
				resultDTO = validation.validateInterestRateTypeCode(inputDTO);
				if (resultDTO.get(ContentManager.ERROR) != null) {
					resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
					return resultDTO;
				}
				if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
					return resultDTO;
				}
				if (RegularConstants.COLUMN_DISABLE.equals(resultDTO.get("STANDARD_RATE"))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.PBS_ONLY_STANDARD_INTRST_RATES_ALLOWED);
					return resultDTO;
				} else {
					inputDTO.set("CCY_CODE", resultDTO.get("INT_RATE_CCY_CODE"));
					inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
					currencyDTO = validation.validateCurrency(inputDTO);
					if (ContentManager.DATA_UNAVAILABLE.equals(currencyDTO.get(ContentManager.RESULT))) {
						resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
						return resultDTO;
					}
					resultDTO.set("CURR_DESC", currencyDTO.get("DESCRIPTION"));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	private boolean validateCurrencyCode() {
		try {
			DTObject formDTO = getFormDTO();
			formDTO.set("CCY_CODE", currencyCode);
			formDTO.set("STD_INT_RATE_TYPE_CODE", standardRateTypeCode);
			formDTO.set(ContentManager.ACTION, USAGE);
			formDTO = validateCurrencyCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("currencyCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("currencyCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateCurrencyCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.NULL);
		MasterValidator validation = new MasterValidator();
		String currencyCode = inputDTO.get("CCY_CODE");
		try {
			if (validation.isEmpty(currencyCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
			resultDTO = validation.validateCurrency(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				return resultDTO;
			} else if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	private boolean validateInternalExternal() {
		CommonValidator validation = new CommonValidator();
		try {
			if (!validation.isCMLOVREC("MINTRATETYPES", "INT_EXT", internalExternal)) {
				getErrorMap().setError("internalExternal", BackOfficeErrorCodes.INVALID_OPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("internalExternal", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	private boolean validateInterestRatePurpose() {
		CommonValidator validation = new CommonValidator();
		try {
			if (!validation.isCMLOVREC("MINTRATETYPES", "INT_RATE_PURP", interestRatePurpose)) {
				getErrorMap().setError("interestRatePurpose", BackOfficeErrorCodes.INVALID_OPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("interestRatePurpose", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	private boolean validateLoanInterestRate() {
		CommonValidator validation = new CommonValidator();
		try {
			if ("L".equals(interestRatePurpose)) {
				if (validation.isEmpty(loanInterestRate)) {
					getErrorMap().setError("loanInterestRate", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!validation.isCMLOVREC("MINTRATETYPES", "LOAN_INT_RATE_PURP", loanInterestRate)) {
					getErrorMap().setError("loanInterestRate", BackOfficeErrorCodes.INVALID_OPTION);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("loanInterestRate", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateRemarks() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (remarks != null && remarks.length() > 0) {
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			if (getAction().equals(MODIFY)) {
				if (commonValidator.isEmpty(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}
}
