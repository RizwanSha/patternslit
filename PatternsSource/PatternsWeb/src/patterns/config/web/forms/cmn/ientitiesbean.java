package patterns.config.web.forms.cmn;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.MasterValidator;
import patterns.config.web.forms.GenericFormBean;

public class ientitiesbean extends GenericFormBean {

	private static final long serialVersionUID = -6219657363150466160L;
	private String entityId;
	private String entityName;
	private String regOfficeAdd;
	private String pinCode;
	private String stateCode;
	private String panCard;
	private String remarks;
	
	
	public String getEntityId() {
		return entityId;
	}
	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}
	public String getEntityName() {
		return entityName;
	}
	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}
	public String getRegOfficeAdd() {
		return regOfficeAdd;
	}
	public void setRegOfficeAdd(String regOfficeAdd) {
		this.regOfficeAdd = regOfficeAdd;
	}
	public String getPinCode() {
		return pinCode;
	}
	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}
	public String getStateCode() {
		return stateCode;
	}
	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}
	public String getPanCard() {
		return panCard;
	}
	public void setPanCard(String panCard) {
		this.panCard = panCard;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	@Override
	public void reset() {
		entityId = RegularConstants.EMPTY_STRING;
		entityName = RegularConstants.EMPTY_STRING;
		regOfficeAdd = RegularConstants.EMPTY_STRING;
		pinCode = RegularConstants.EMPTY_STRING;
		stateCode = RegularConstants.EMPTY_STRING;
		panCard = RegularConstants.EMPTY_STRING;
		remarks = RegularConstants.EMPTY_STRING;
		setProcessBO("patterns.config.framework.bo.cmn.ientitiesBO");
	}
	@Override
	public void validate() {
		  {
			 validateEntityId();
			validateEntityName();
			validateRegOfficeAdd();
			validatePinCode();
			validateStateCode();
			validatePanCard();
			validateRemarks();
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.set("ENTITY_ID", entityId);
				formDTO.set("ENTITY_NAME", entityName);
				formDTO.set("REG_ADDRESS", regOfficeAdd);
				formDTO.set("PIN_CODE", pinCode);
				formDTO.set("STATE_CODE", stateCode);
				formDTO.set("PAN_NO", panCard);
				formDTO.set("REMARKS", remarks);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("entityId", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
	}
	public boolean validateEntityId() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(entityId)) {
				getErrorMap().setError("entityId", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("entityId", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}
	public boolean validateEntityName() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(entityName)) {
				getErrorMap().setError("entityName", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("entityName", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}
	public boolean validateRegOfficeAdd() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(regOfficeAdd)) {
				getErrorMap().setError("regOfficeAdd", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidRemarks(regOfficeAdd)) {
				getErrorMap().setError("regOfficeAdd", BackOfficeErrorCodes.INVALID_REMARKS);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("regOfficeAdd", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}
	public boolean validatePinCode() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(pinCode)) {
				getErrorMap().setError("pinCode", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidNumber(pinCode)) {
				getErrorMap().setError("pinCode", BackOfficeErrorCodes.NUMERIC_CHECK);
				return false;
			}
			if (validation.isValidNegativeNumber(pinCode)) {
				getErrorMap().setError("pinCode", BackOfficeErrorCodes.POSITIVE_CHECK);
				return false;
			}
			if (pinCode.length() != 6) {
				getErrorMap().setError("pinCode", BackOfficeErrorCodes.PINCODE_LENGTH);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("pinCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}
	public boolean validateStateCode() {
		try {
			
			DTObject formDTO = getFormDTO();
			formDTO.set("STATE_CODE", stateCode);
			formDTO.set(ContentManager.USER_ACTION, getAction());
			formDTO = validateStateCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("stateCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("stateCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} 
		return false;
	}
	public DTObject validateStateCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		MasterValidator validation = new MasterValidator();
		String stateCode = inputDTO.get("STATE_CODE");
		try {
			if (validation.isEmpty(stateCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
			resultDTO = validation.validateStateCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			else if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
			return resultDTO;
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}
	public boolean validatePanCard() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(panCard)) {
				getErrorMap().setError("panCard", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (panCard.length() > 10) {
				getErrorMap().setError("panCard", BackOfficeErrorCodes.PANCARD_LENGTH);
				return false;
			}
			if (panCard.contains(" ")) {
				getErrorMap().setError("panCard", BackOfficeErrorCodes.SPACE_BETWEEN);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("panCard", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}
	public boolean validateRemarks() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (remarks != null && remarks.length() > 0) {
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			if (getAction().equals(MODIFY)) {
				if (commonValidator.isEmpty(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	} 
	
} 
