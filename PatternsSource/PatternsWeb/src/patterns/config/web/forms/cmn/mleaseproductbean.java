package patterns.config.web.forms.cmn;

import java.util.ArrayList;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.GenericOption;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.FASValidator;
import patterns.config.validations.MasterValidator;
import patterns.config.web.forms.GenericFormBean;

/**
 * 
 * 
 * @version 1.0
 * @author Kalimuthu U
 * @since 01-Jan-2017 <br>
 *        <b>MLEASEPRODUCT</b> <BR>
 *        <U><B> Lease Product Codes </B></U> <br>
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 */
public class mleaseproductbean extends GenericFormBean {

	private static final long serialVersionUID = 1L;
	private String leaseProductCode;
	private String leaseProductDescription;
	private String leaseProductConciseDescription;
	private String leaseType;
	private String sundryDebtorsHead;
	private String rentReceivablePrincipal;
	private String rentReceivableInterest;
	private String unearnedIncomeHead;
	private boolean enabled;
	private String remarks;
	private String baseCurrCode;

	public String getBaseCurrCode() {
		return baseCurrCode;
	}

	public void setBaseCurrCode(String baseCurrCode) {
		this.baseCurrCode = baseCurrCode;
	}

	public String getLeaseProductCode() {
		return leaseProductCode;
	}

	public void setLeaseProductCode(String leaseProductCode) {
		this.leaseProductCode = leaseProductCode;
	}

	public String getLeaseProductDescription() {
		return leaseProductDescription;
	}

	public void setLeaseProductDescription(String leaseProductDescription) {
		this.leaseProductDescription = leaseProductDescription;
	}

	public String getLeaseProductConciseDescription() {
		return leaseProductConciseDescription;
	}

	public void setLeaseProductConciseDescription(String leaseProductConciseDescription) {
		this.leaseProductConciseDescription = leaseProductConciseDescription;
	}

	public String getLeaseType() {
		return leaseType;
	}

	public void setLeaseType(String leaseType) {
		this.leaseType = leaseType;
	}

	public String getSundryDebtorsHead() {
		return sundryDebtorsHead;
	}

	public void setSundryDebtorsHead(String sundryDebtorsHead) {
		this.sundryDebtorsHead = sundryDebtorsHead;
	}

	public String getRentReceivablePrincipal() {
		return rentReceivablePrincipal;
	}

	public void setRentReceivablePrincipal(String rentReceivablePrincipal) {
		this.rentReceivablePrincipal = rentReceivablePrincipal;
	}

	public String getRentReceivableInterest() {
		return rentReceivableInterest;
	}

	public void setRentReceivableInterest(String rentReceivableInterest) {
		this.rentReceivableInterest = rentReceivableInterest;
	}

	public String getUnearnedIncomeHead() {
		return unearnedIncomeHead;
	}

	public void setUnearnedIncomeHead(String unearnedIncomeHead) {
		this.unearnedIncomeHead = unearnedIncomeHead;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void reset() {
		leaseProductCode = RegularConstants.EMPTY_STRING;
		leaseProductDescription = RegularConstants.EMPTY_STRING;
		leaseProductConciseDescription = RegularConstants.EMPTY_STRING;
		leaseType = RegularConstants.EMPTY_STRING;
		baseCurrCode = RegularConstants.EMPTY_STRING;
		sundryDebtorsHead = RegularConstants.EMPTY_STRING;
		rentReceivablePrincipal = RegularConstants.EMPTY_STRING;
		rentReceivableInterest = RegularConstants.EMPTY_STRING;
		unearnedIncomeHead = RegularConstants.EMPTY_STRING;
		enabled = false;
		remarks = RegularConstants.EMPTY_STRING;
		DBContext dbContext = new DBContext();
		ArrayList<GenericOption> leaseType = null;
		leaseType = getGenericOptions("COMMON", "LEASE_TYPE", dbContext, null);
		webContext.getRequest().setAttribute("COMMON_LEASE_TYPE", leaseType);
		dbContext.close();
		setProcessBO("patterns.config.framework.bo.cmn.mleaseproductBO");
	}

	public void validate() {
		if (validateLeaseProductCode()) {
			validateLeaseProductDescription();
			validateLeaseProductConciseDescription();
			validateLeaseType();
			validateBaseCurrCode();
			validateSundryDebtorsHead();
			validateRentReceivablePrincipal();
			validateRentReceivableInterest();
			validateUnearnedIncomeHead();
			validateRemarks();
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.set("LEASE_PRODUCT_CODE", leaseProductCode);
				formDTO.set("DESCRIPTION", leaseProductDescription);
				formDTO.set("CONCISE_DESCRIPTION", leaseProductConciseDescription);
				formDTO.set("LEASE_TYPE", leaseType);
				formDTO.set("LEASE_ASSET_IN_CCY", baseCurrCode);
				formDTO.set("SUNDRY_DEBTOR_GL_HEAD_CODE", sundryDebtorsHead);
				formDTO.set("RENT_RECV_PRIN_GL_HEAD_CODE", rentReceivablePrincipal);
				formDTO.set("RENT_RECV_INT_GL_HEAD_CODE", rentReceivableInterest);
				formDTO.set("UNEARNED_INCOME_GL_HEAD_CODE", unearnedIncomeHead);
				formDTO.set("REMARKS", remarks);
				if (getAction().equals(ADD)) {
					formDTO.set("ENABLED", decodeBooleanToString(true));
				} else {
					formDTO.set("ENABLED", decodeBooleanToString(enabled));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("leaseProductCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
	}

	private boolean validateLeaseProductCode() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("LEASE_PRODUCT_CODE", leaseProductCode);
			formDTO.set(ContentManager.USER_ACTION, getAction());
			formDTO = validateLeaseProductCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("leaseProductCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("leaseProductCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateLeaseProductCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		MasterValidator validation = new MasterValidator();
		String leaseProductCode = inputDTO.get("LEASE_PRODUCT_CODE");
		String action = inputDTO.get(ContentManager.USER_ACTION);
		try {
			if (validation.isEmpty(leaseProductCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			resultDTO = validation.validateLeaseProductCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (action.equals(ADD)) {
				if (ContentManager.DATA_AVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_EXISTS);
					return resultDTO;
				}
			} else {
				if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_NOT_EXISTS);
					return resultDTO;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	private boolean validateLeaseProductDescription() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(leaseProductDescription)) {
				getErrorMap().setError("leaseProductDescription", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidDescription(leaseProductDescription)) {
				getErrorMap().setError("leaseProductDescription", BackOfficeErrorCodes.INVALID_DESCRIPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("leaseProductDescription", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	private boolean validateLeaseProductConciseDescription() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(leaseProductConciseDescription)) {
				getErrorMap().setError("leaseProductConciseDescription", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidConciseDescription(leaseProductConciseDescription)) {
				getErrorMap().setError("leaseProductConciseDescription", BackOfficeErrorCodes.INVALID_CONCISE_DESCRIPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("leaseProductConciseDescription", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	private boolean validateLeaseType() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(leaseType)) {
				getErrorMap().setError("leaseType", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isCMLOVREC("COMMON", "LEASE_TYPE", leaseType)) {
				getErrorMap().setError("leaseType", BackOfficeErrorCodes.INVALID_SELECTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("leaseType", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateBaseCurrCode() {
		try {
			DTObject formDTO = getFormDTO();
			formDTO.set("CCY_CODE", baseCurrCode);
			formDTO.set(ContentManager.USER_ACTION, getAction());
			formDTO = validateBaseCurrCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("baseCurrCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("baseCurrCode", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateBaseCurrCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		MasterValidator validation = new MasterValidator();
		String baseCurrCode = inputDTO.get("CCY_CODE");
		try {
			if (validation.isEmpty(baseCurrCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_MANDATORY);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
			resultDTO = validation.validateCurrency(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_CODE);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	private boolean validateSundryDebtorsHead() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("GL_HEAD_CODE", sundryDebtorsHead);
			formDTO.set(ContentManager.USER_ACTION, getAction());
			formDTO = validateSundryDebtorsHead(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("sundryDebtorsHead", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("sundryDebtorsHead", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	private boolean validateRentReceivablePrincipal() {
		try {
			if (!rentReceivablePrincipal.isEmpty()) {
				DTObject formDTO = new DTObject();
				formDTO.set("GL_HEAD_CODE", rentReceivablePrincipal);
				formDTO.set(ContentManager.USER_ACTION, getAction());
				formDTO = validateGlHeadCode(formDTO);
				if (formDTO.get(ContentManager.ERROR) != null) {
					getErrorMap().setError("rentReceivablePrincipal", formDTO.get(ContentManager.ERROR));
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("rentReceivablePrincipal", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	private boolean validateRentReceivableInterest() {
		try {
			if (!rentReceivableInterest.isEmpty()) {
				DTObject formDTO = new DTObject();
				formDTO.set("GL_HEAD_CODE", rentReceivableInterest);
				formDTO.set(ContentManager.USER_ACTION, getAction());
				formDTO = validateGlHeadCodes(formDTO);
				if (formDTO.get(ContentManager.ERROR) != null) {
					getErrorMap().setError("rentReceivableInterest", formDTO.get(ContentManager.ERROR));
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("rentReceivableInterest", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateGlHeadCodes(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		FASValidator validation = new FASValidator();
		try {
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
			resultDTO = validation.validateGLHeadCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	private boolean validateUnearnedIncomeHead() {
		try {
			if (!unearnedIncomeHead.isEmpty()) {
				DTObject formDTO = new DTObject();
				formDTO.set("GL_HEAD_CODE", unearnedIncomeHead);
				formDTO.set(ContentManager.USER_ACTION, getAction());
				formDTO = validateGlHeadCodes(formDTO);
				if (formDTO.get(ContentManager.ERROR) != null) {
					getErrorMap().setError("unearnedIncomeHead", formDTO.get(ContentManager.ERROR));
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("unearnedIncomeHead", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateSundryDebtorsHead(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		FASValidator validation = new FASValidator();
		String glHeadCode = inputDTO.get("GL_HEAD_CODE");
		try {
			if (validation.isEmpty(glHeadCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
			resultDTO = validation.validateGLHeadCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public DTObject validateGlHeadCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		FASValidator validation = new FASValidator();
		try {
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
			resultDTO = validation.validateGLHeadCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	private boolean validateRemarks() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (remarks != null && remarks.length() > 0) {
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			if (getAction().equals(MODIFY)) {
				if (commonValidator.isEmpty(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}

}
