package patterns.config.web.forms.cmn;

import java.sql.ResultSet;
import java.util.ArrayList;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.service.DTDObject;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.GenericOption;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.BaseDomainValidator;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.FASValidator;
import patterns.config.validations.MasterValidator;
import patterns.config.web.forms.GenericFormBean;

public class mtaxbean extends GenericFormBean {

	private static final long serialVersionUID = 1L;

	private String stateCode;
	private String taxCode;
	private String description;
	private String conciseDescription;
	private String taxType;
	private String taxOn;
	private String taxOnTaxCode;
	private String taxIpGLAcctHead;
	private String taxPayGLAcctHead;
	private String noOfTaxSchedules;
	private String taxScheduleCode;
	private boolean prdctWiseTaxInclusive;
	private boolean cstmerWiseTaxInclusive;
	private boolean leaseWiseTaxInclusive;
	private boolean enabled;
	private String remarks;
	private String xmlTax;
	private String installCenteralCode;
	private String taxScheduleCodeTaxOnTaxCode;

	public String getConciseDescription() {
		return conciseDescription;
	}

	public void setConciseDescription(String conciseDescription) {
		this.conciseDescription = conciseDescription;
	}

	public String getStateCode() {
		return stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	public String getTaxCode() {
		return taxCode;
	}

	public void setTaxCode(String taxCode) {
		this.taxCode = taxCode;
	}

	public String getTaxType() {
		return taxType;
	}

	public void setTaxType(String taxType) {
		this.taxType = taxType;
	}

	public String getTaxOn() {
		return taxOn;
	}

	public void setTaxOn(String taxOn) {
		this.taxOn = taxOn;
	}

	public String getTaxOnTaxCode() {
		return taxOnTaxCode;
	}

	public void setTaxOnTaxCode(String taxOnTaxCode) {
		this.taxOnTaxCode = taxOnTaxCode;
	}

	public String getTaxIpGLAcctHead() {
		return taxIpGLAcctHead;
	}

	public void setTaxIpGLAcctHead(String taxIpGLAcctHead) {
		this.taxIpGLAcctHead = taxIpGLAcctHead;
	}

	public String getTaxPayGLAcctHead() {
		return taxPayGLAcctHead;
	}

	public void setTaxPayGLAcctHead(String taxPayGLAcctHead) {
		this.taxPayGLAcctHead = taxPayGLAcctHead;
	}

	public String getNoOfTaxSchedules() {
		return noOfTaxSchedules;
	}

	public void setNoOfTaxSchedules(String noOfTaxSchedules) {
		this.noOfTaxSchedules = noOfTaxSchedules;
	}

	public String getTaxScheduleCode() {
		return taxScheduleCode;
	}

	public void setTaxScheduleCode(String taxScheduleCode) {
		this.taxScheduleCode = taxScheduleCode;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getXmlTax() {
		return xmlTax;
	}

	public void setXmlTax(String xmlTax) {
		this.xmlTax = xmlTax;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public boolean isPrdctWiseTaxInclusive() {
		return prdctWiseTaxInclusive;
	}

	public void setPrdctWiseTaxInclusive(boolean prdctWiseTaxInclusive) {
		this.prdctWiseTaxInclusive = prdctWiseTaxInclusive;
	}

	public boolean isCstmerWiseTaxInclusive() {
		return cstmerWiseTaxInclusive;
	}

	public void setCstmerWiseTaxInclusive(boolean cstmerWiseTaxInclusive) {
		this.cstmerWiseTaxInclusive = cstmerWiseTaxInclusive;
	}

	public boolean isLeaseWiseTaxInclusive() {
		return leaseWiseTaxInclusive;
	}

	public void setLeaseWiseTaxInclusive(boolean leaseWiseTaxInclusive) {
		this.leaseWiseTaxInclusive = leaseWiseTaxInclusive;
	}

	public String getInstallCenteralCode() {
		return installCenteralCode;
	}

	public void setInstallCenteralCode(String installCenteralCode) {
		this.installCenteralCode = installCenteralCode;
	}

	public String getTaxScheduleCodeTaxOnTaxCode() {
		return taxScheduleCodeTaxOnTaxCode;
	}

	public void setTaxScheduleCodeTaxOnTaxCode(String taxScheduleCodeTaxOnTaxCode) {
		this.taxScheduleCodeTaxOnTaxCode = taxScheduleCodeTaxOnTaxCode;
	}

	@Override
	public void reset() {
		DTObject inputDTO = new DTObject();
		fetchDefaultDetails(inputDTO);
		stateCode = RegularConstants.EMPTY_STRING;
		taxCode = RegularConstants.EMPTY_STRING;
		description = RegularConstants.EMPTY_STRING;
		conciseDescription = RegularConstants.EMPTY_STRING;
		taxType = RegularConstants.EMPTY_STRING;
		taxOn = RegularConstants.EMPTY_STRING;
		taxOnTaxCode = RegularConstants.EMPTY_STRING;
		taxIpGLAcctHead = RegularConstants.EMPTY_STRING;
		taxPayGLAcctHead = RegularConstants.EMPTY_STRING;
		noOfTaxSchedules = RegularConstants.EMPTY_STRING;
		taxScheduleCode = RegularConstants.EMPTY_STRING;
		prdctWiseTaxInclusive = false;
		cstmerWiseTaxInclusive = false;
		leaseWiseTaxInclusive = false;
		enabled = false;
		remarks = RegularConstants.EMPTY_STRING;
		xmlTax = RegularConstants.EMPTY_STRING;
		DBContext dbContext = new DBContext();
		ArrayList<GenericOption> taxType = null;
		taxType = getGenericOptions("MTAX", "TAX_TYPE", dbContext, null);
		webContext.getRequest().setAttribute("MTAX_TAX_TYPE", taxType);
		ArrayList<GenericOption> taxOn = null;
		taxOn = getGenericOptions("MTAX", "TAX_ON", dbContext, null);
		webContext.getRequest().setAttribute("MTAX_TAX_ON", taxOn);
		dbContext.close();
		setProcessBO("patterns.config.framework.bo.cmn.mtaxBO");

	}

	@Override
	public void validate() {
		if (validateStateCode() && validateTaxCode()) {
			validateDescription();
			validateConciseDescription();
			validateTaxType();
			validateTaxOn();
			validateTaxOnTaxCode();
			validateTaxIpGLAcctHead();
			validateTaxPayGLAcctHead();
			validateNoOfTaxSchedules();
			validateRemarks();
			validateGrid();
		}
		CommonValidator validation = new CommonValidator();
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.set("STATE_CODE", stateCode);
				formDTO.set("TAX_CODE", taxCode);
				formDTO.set("DESCRIPTION", description);
				formDTO.set("CONCISE_DESCRIPTION", conciseDescription);
				formDTO.set("TAX_TYPE", taxType);
				formDTO.set("TAX_INPUT_GL_HEAD_CODE", taxIpGLAcctHead);
				formDTO.set("TAX_PAY_GL_HEAD_CODE", taxPayGLAcctHead);
				formDTO.set("TAX_BYUS_PROD_ALLOWED", decodeBooleanToString(prdctWiseTaxInclusive));
				formDTO.set("TAX_BYUS_CUST_ALLOWED", decodeBooleanToString(cstmerWiseTaxInclusive));
				formDTO.set("TAX_BYUS_LEASE_ALLOWED", decodeBooleanToString(leaseWiseTaxInclusive));
				formDTO.set("TAX_ON", taxOn);
				if (validation.isEmpty(taxOnTaxCode)) {
					formDTO.set("TAX_ON_TAX_CODE", RegularConstants.ZERO);
				}
				formDTO.set("TAX_ON_TAX_CODE", taxOnTaxCode);
				if (validation.isEmpty(noOfTaxSchedules)) {
					formDTO.set("NOF_TAX_SCHEDULES", RegularConstants.ZERO);
				} else {
					formDTO.set("NOF_TAX_SCHEDULES", noOfTaxSchedules);
				}
				if (getAction().equals(ADD)) {
					formDTO.set("ENABLED", decodeBooleanToString(true));
				} else {
					formDTO.set("ENABLED", decodeBooleanToString(enabled));
				}
				formDTO.set("REMARKS", remarks);
				DTDObject dtdObject = new DTDObject();
				dtdObject.addColumn(0, "SL");
				dtdObject.addColumn(1, "TAX_SCHEDULE_CODE");
				dtdObject.setXML(xmlTax);
				formDTO.setDTDObject("TAXSCHEDULE", dtdObject);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("stateCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}

	}

	public boolean validateStateCode() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("STATE_CODE", stateCode);
			formDTO.set(ContentManager.USER_ACTION, USAGE);
			formDTO = validateStateCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("stateCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("stateCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateStateCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		MasterValidator validation = new MasterValidator();
		String stateCode = inputDTO.get("STATE_CODE");
		try {
			if (validation.isEmpty(stateCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
			resultDTO = validation.validateCentralAndStateCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateTaxCode() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("STATE_CODE", stateCode);
			formDTO.set("TAX_CODE", taxCode);
			formDTO.set(ContentManager.ACTION, getAction());
			formDTO = validateTaxCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("taxCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("taxCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateTaxCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		MasterValidator validation = new MasterValidator();
		String taxCode = inputDTO.get("TAX_CODE");
		String action = inputDTO.get(ContentManager.USER_ACTION);
		try {
			if (validation.isEmpty(taxCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "TAX_CODE");
			resultDTO = validation.validateTax(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				return resultDTO;
			}
			if (action.equals(ADD)) {
				if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_EXISTS);
				}
			} else {
				if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_NOT_EXISTS);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	private boolean validateDescription() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(description)) {
				getErrorMap().setError("description", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidDescription(description)) {
				getErrorMap().setError("description", BackOfficeErrorCodes.INVALID_DESCRIPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("description", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	private boolean validateConciseDescription() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(conciseDescription)) {
				getErrorMap().setError("conciseDescription", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidConciseDescription(conciseDescription)) {
				getErrorMap().setError("conciseDescription", BackOfficeErrorCodes.INVALID_CONCISE_DESCRIPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("conciseDescription", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateTaxType() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(taxType)) {
				getErrorMap().setError("taxType", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isCMLOVREC("MTAX", "TAX_TYPE", taxType)) {
				getErrorMap().setError("taxType", BackOfficeErrorCodes.INVALID_OPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("taxType", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateTaxOn() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(taxOn)) {
				getErrorMap().setError("taxType", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isCMLOVREC("MTAX", "TAX_ON", taxOn)) {
				getErrorMap().setError("taxOn", BackOfficeErrorCodes.INVALID_OPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("taxOn", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateTaxOnTaxCode() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("STATE_CODE", stateCode);
			formDTO.set("ACTUAL_TAX_CODE", taxCode);
			formDTO.set("TAX_ON_TAX_CODE", taxOnTaxCode);
			formDTO.set(ContentManager.ACTION, getAction());
			formDTO.set("TAX_ON", taxOn);
			formDTO = validateTaxOnTaxCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("taxOnTaxCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("taxOnTaxCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject fetchDefaultDetails(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		BaseDomainValidator validation = new BaseDomainValidator();
		try {
			inputDTO.set(ContentManager.FETCH_COLUMNS, "TAX_SCHEDULE_CODE_FOR_TAXONTAX_CODE,CENTRAL_CODE");
			resultDTO = validation.getINSTALL(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
			setInstallCenteralCode(resultDTO.get("CENTRAL_CODE"));
			setTaxScheduleCodeTaxOnTaxCode(resultDTO.get("TAX_SCHEDULE_CODE_FOR_TAXONTAX_CODE"));
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public DTObject validateTaxOnTaxCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		MasterValidator validation = new MasterValidator();
		String taxOnTaxCode = inputDTO.get("TAX_ON_TAX_CODE");
		String taxCode = inputDTO.get("ACTUAL_TAX_CODE");
		String action = inputDTO.get(ContentManager.USER_ACTION);
		try {
			if (inputDTO.get("TAX_ON").equals("4")) {
				if (validation.isEmpty(taxOnTaxCode)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
					return resultDTO;
				}
				inputDTO.set("TAX_CODE", taxOnTaxCode);
				inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
				resultDTO = validation.validateTax(inputDTO);
				if (resultDTO.get(ContentManager.ERROR) != null) {
					return resultDTO;
				}
				if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
					return resultDTO;
				}
				if (action.equals(MODIFY)) {
					if (resultDTO.get("TAX_CODE").equals(taxCode)) {
						resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.TAX_ON_CODE_VALUE_SAME);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateTaxIpGLAcctHead() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("GL_HEAD_CODE", taxIpGLAcctHead);
			formDTO.set(ContentManager.USER_ACTION, USAGE);
			formDTO = validateTaxIpGLAcctHead(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("taxIpGLAcctHead", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("taxIpGLAcctHead", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateTaxIpGLAcctHead(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		FASValidator validation = new FASValidator();
		String taxIpGLAcctHead = inputDTO.get("GL_HEAD_CODE");
		try {
			if (validation.isEmpty(taxIpGLAcctHead)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION,ENABLED");
			resultDTO = validation.validateGLHeadCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
			if (resultDTO.get("ENABLED").equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.CODE_CLOSED);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateTaxPayGLAcctHead() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("GL_HEAD_CODE", taxPayGLAcctHead);
			formDTO.set(ContentManager.USER_ACTION, USAGE);
			formDTO = validateTaxPayGLAcctHead(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("taxPayGLAcctHead", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("taxPayGLAcctHead", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateTaxPayGLAcctHead(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		FASValidator validation = new FASValidator();
		String taxPayGLAcctHead = inputDTO.get("GL_HEAD_CODE");
		try {
			if (validation.isEmpty(taxPayGLAcctHead)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION,ENABLED");
			resultDTO = validation.validateGLHeadCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
			if (resultDTO.get("ENABLED").equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.CODE_CLOSED);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateNoOfTaxSchedules() {
		CommonValidator validation = new CommonValidator();
		try {
			if (!taxOn.equals("4")) {
				if (validation.isEmpty(noOfTaxSchedules)) {
					getErrorMap().setError("noOfTaxSchedules", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!validation.isValidTwoDigit(noOfTaxSchedules)) {
					getErrorMap().setError("noOfTaxSchedules", BackOfficeErrorCodes.INVALID_NUMBER);
					return false;
				}
				if (validation.isZero(noOfTaxSchedules)) {
					getErrorMap().setError("noOfTaxSchedules", BackOfficeErrorCodes.AMOUNT_NOT_ZERO);
					return false;
				}
				if (validation.isValidNegativeNumber(noOfTaxSchedules)) {
					getErrorMap().setError("noOfTaxSchedules", BackOfficeErrorCodes.PBS_POSITIVE_AMOUNT);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("noOfRdInstall", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateTaxScheduleCode(String taxScheduleCode) {
		MasterValidator validation = new MasterValidator();
		try {
			if (!taxOn.equals("4")) {
				if (validation.isEmpty(taxScheduleCode)) {
					getErrorMap().setError("taxScheduleCode", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!validation.isValidOtherInformation25(taxScheduleCode)) {
					getErrorMap().setError("taxScheduleCode", BackOfficeErrorCodes.INVALID_VALUE);
					return false;
				}
				DTObject formDTO = new DTObject();
				formDTO.set("TAX_ON", taxOn);
				formDTO = validateTaxScheduleCode(formDTO);
				if (formDTO.get(ContentManager.ERROR) != null) {
					getErrorMap().setError("taxScheduleCode", formDTO.get(ContentManager.ERROR));
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("taxScheduleCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public DTObject validateTaxScheduleCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		String CenteralCode = inputDTO.get("TAX_ON");
		if (CenteralCode.equals(taxScheduleCodeTaxOnTaxCode)) {
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_VALUE_ONLY_CENTERAL_CODE);
			return resultDTO;
		}
		resultDTO = validateGetCountTaxSchedule(inputDTO);
		if (!inputDTO.get("TAX_ON").equals("4")) {
			if (resultDTO.get("RECORD_AVAILABLE").equals(RegularConstants.ONE)) {
				Object errorParam[] = { resultDTO.get("STATE_CODE"), resultDTO.get("TAX_CODE") };
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.TAX_SCHEDULE_MAP);
				resultDTO.setObject(ContentManager.ERROR_PARAM, errorParam);
				return resultDTO;
			}
		}
		return resultDTO;
	}

	public DTObject validateGetCountTaxSchedule(DTObject inputDTO) {
		DBContext dbContext = new DBContext();
		DTObject resultDTO = new DTObject();
		resultDTO.set("RECORD_AVAILABLE", RegularConstants.ZERO);
		try {
			DBUtil util = dbContext.createUtilInstance();
			util.reset();
			util.setMode(DBUtil.PREPARED);
			util.setSql("SELECT COUNT(1),STATE_CODE,TAX_CODE FROM TAXSCHEDULE WHERE ENTITY_CODE=? AND TAX_SCHEDULE_CODE=?");
			util.setLong(1, Long.parseLong(context.getEntityCode()));
			util.setString(2, inputDTO.get("TAX_SCHEDULE_CODE"));
			ResultSet rs = util.executeQuery();
			if (rs.next()) {
				if (rs.getInt(1) != 0) {
					resultDTO.set("RECORD_AVAILABLE", RegularConstants.ONE);
					resultDTO.set("STATE_CODE", rs.getString(2));
					resultDTO.set("TAX_CODE", rs.getString(3));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			dbContext.close();
		}
		return resultDTO;
	}

	public boolean validateRemarks() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (remarks != null && remarks.length() > 0) {
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			if (getAction().equals(MODIFY)) {
				if (commonValidator.isEmpty(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}

	private boolean validateGrid() {
		CommonValidator validation = new CommonValidator();
		try {
			DTDObject dtdObject = new DTDObject();
			dtdObject.addColumn(0, "SL");
			dtdObject.addColumn(1, "TAX_SCHEDULE_CODE");
			dtdObject.setXML(xmlTax);
			if (!taxOn.equals("4")) {
				if (dtdObject.getRowCount() <= 0) {
					getErrorMap().setError("taxScheduleCode", BackOfficeErrorCodes.ATLEAST_ONE_ROW);
					return false;
				}
				if (dtdObject.getRowCount() != Integer.parseInt(noOfTaxSchedules)) {
					Object errorParam[] = { noOfTaxSchedules };
					getErrorMap().setError("taxScheduleCode", BackOfficeErrorCodes.TAX_SCHEDULE_GRID_VALUE_SAME, errorParam);
					return false;
				}
				if (!validation.checkDuplicateRows(dtdObject, 1)) {
					getErrorMap().setError("taxScheduleCode", BackOfficeErrorCodes.DUPLICATE_DATA);
					return false;
				}
				for (int i = 0; i < dtdObject.getRowCount(); i++) {
					if (!validateTaxScheduleCode(dtdObject.getValue(i, 1))) {
						getErrorMap().setError("taxScheduleCode", BackOfficeErrorCodes.INVALID_ACTION);
						return false;
					}
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("taxScheduleCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return true;
	}
}
