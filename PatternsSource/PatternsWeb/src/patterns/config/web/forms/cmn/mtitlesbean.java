package patterns.config.web.forms.cmn;

import java.util.ArrayList;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.GenericOption;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.MasterValidator;
import patterns.config.web.forms.GenericFormBean;

public class mtitlesbean extends GenericFormBean {

	private static final long serialVersionUID = 1L;
	private String titleCode;
	private String extDescription;
	private String intDescription;
	private String shortDescription;
	private String titleUsage;
	private boolean titleChildrenUsage;
	private boolean titleDeceasedUsage;
	private String titleOccupation;
	private String otherOccupation;
	private boolean fixMaritalStatus;
	private String defMaritalStatus;
	private boolean enabled;
	private String remarks;

	public boolean isFixMaritalStatus() {
		return fixMaritalStatus;
	}

	public void setFixMaritalStatus(boolean fixMaritalStatus) {
		this.fixMaritalStatus = fixMaritalStatus;
	}

	public String getDefMaritalStatus() {
		return defMaritalStatus;
	}

	public void setDefMaritalStatus(String defMaritalStatus) {
		this.defMaritalStatus = defMaritalStatus;
	}

	public String getTitleCode() {
		return titleCode;
	}

	public void setTitleCode(String titleCode) {
		this.titleCode = titleCode;
	}

	public String getExtDescription() {
		return extDescription;
	}

	public void setExtDescription(String extDescription) {
		this.extDescription = extDescription;
	}

	public String getIntDescription() {
		return intDescription;
	}

	public void setIntDescription(String intDescription) {
		this.intDescription = intDescription;
	}

	public String getShortDescription() {
		return shortDescription;
	}

	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}

	public String getTitleUsage() {
		return titleUsage;
	}

	public void setTitleUsage(String titleUsage) {
		this.titleUsage = titleUsage;
	}

	public boolean isTitleChildrenUsage() {
		return titleChildrenUsage;
	}

	public void setTitleChildrenUsage(boolean titleChildrenUsage) {
		this.titleChildrenUsage = titleChildrenUsage;
	}

	public boolean isTitleDeceasedUsage() {
		return titleDeceasedUsage;
	}

	public void setTitleDeceasedUsage(boolean titleDeceasedUsage) {
		this.titleDeceasedUsage = titleDeceasedUsage;
	}

	public String getTitleOccupation() {
		return titleOccupation;
	}

	public void setTitleOccupation(String titleOccupation) {
		this.titleOccupation = titleOccupation;
	}

	public String getOtherOccupation() {
		return otherOccupation;
	}

	public void setOtherOccupation(String otherOccupation) {
		this.otherOccupation = otherOccupation;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void reset() {
		titleCode = RegularConstants.EMPTY_STRING;
		extDescription = RegularConstants.EMPTY_STRING;
		intDescription = RegularConstants.EMPTY_STRING;
		titleUsage = RegularConstants.EMPTY_STRING;
		titleOccupation = RegularConstants.EMPTY_STRING;
		otherOccupation = RegularConstants.EMPTY_STRING;
		remarks = RegularConstants.EMPTY_STRING;
		defMaritalStatus = RegularConstants.EMPTY_STRING;
		fixMaritalStatus = false;

		DBContext dbContext = new DBContext();
		ArrayList<GenericOption> titleUsage = null;
		titleUsage = getGenericOptions("COMMON", "CMNTITLES", dbContext, null);
		webContext.getRequest().setAttribute("COMMON_CMNTITLES", titleUsage);
		dbContext.close();
		setProcessBO("patterns.config.framework.bo.cmn.mtitlesBO");
	}

	public void validate() {
		if (validateTitleCode()) {
			validateExternalDescription();
			validateInternalDescription();
			validateShortDescription();
			validateTitleUsage();
			validateOccupationCode();
			validateOtherOccupation();
			validateDefMaritialStatus();
			validateFixMaritalStatus();
			validateRemarks();
		}
		CommonValidator validation=new CommonValidator();
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.set("TITLE_CODE", titleCode);
				formDTO.set("EXT_DESCRIPTION", extDescription);
				formDTO.set("INT_DESCRIPTION", intDescription);
				formDTO.set("CONCISE_DESCRIPTION", shortDescription);
				formDTO.set("TITLE_USAGE", titleUsage);
				formDTO.set("TITLE_CHILDREN_USG", decodeBooleanToString(titleChildrenUsage));
				formDTO.set("TITLE_DECEASED_USG", decodeBooleanToString(titleDeceasedUsage));
                if(!validation.isEmpty(titleOccupation))
                      formDTO.set("TITLE_OCCU", titleOccupation);
                else
                      formDTO.set("TITLE_OCCU", RegularConstants.NULL);
                formDTO.set("TITLE_OTH_OCCU", otherOccupation);
                if (getAction().equals(ADD)) {
                      formDTO.set("ENABLED", decodeBooleanToString(true));
                } else {
                      formDTO.set("ENABLED", decodeBooleanToString(enabled));
                }
                if(!validation.isEmpty(defMaritalStatus))
                      formDTO.set("DEFAULT_MARITAL_STATUS", defMaritalStatus);
                else
                      formDTO.set("DEFAULT_MARITAL_STATUS", RegularConstants.NULL);
               
                formDTO.set("MARITAL_STATUS_FIXED", decodeBooleanToString(fixMaritalStatus));
                formDTO.set("REMARKS", remarks);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("titleCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		finally
		{
			validation.close();
		}
	}

	public boolean validateTitleCode() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("TITLE_CODE", titleCode);
			formDTO.set("ACTION", getAction());
			formDTO = validateTitleCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("titleCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("titleCode", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateTitleCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		MasterValidator validation = new MasterValidator();
		String titleCode = inputDTO.get("TITLE_CODE");
		String action = inputDTO.get("ACTION");
		try {
			if (validation.isEmpty(titleCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH, ContentManager.FETCH_ALL);
			resultDTO = validation.validateTitleCode(inputDTO);
			if (action.equals(ADD)) {
				if (ContentManager.DATA_AVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_RECORD_EXISTS);
				}
			} else if (action.equals(MODIFY)) {
				if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_RECORD_NOT_EXISTS);

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateExternalDescription() {
		CommonValidator validation = new CommonValidator();
		try {
			if (!validation.isValidDescription(extDescription)) {
				getErrorMap().setError("extDescription", BackOfficeErrorCodes.HMS_INVALID_DESCRIPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("extDescription", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateInternalDescription() {
		CommonValidator validation = new CommonValidator();
		try {
			if (!validation.isValidDescription(intDescription)) {
				getErrorMap().setError("intDescription", BackOfficeErrorCodes.HMS_INVALID_DESCRIPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("intDescription", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateShortDescription() {
		CommonValidator validation = new CommonValidator();
		try {

			if (!validation.isValidConciseDescription(shortDescription)) {
				getErrorMap().setError("shortDescription", BackOfficeErrorCodes.HMS_INVALID_CONCISE_DESCRIPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("shortDescription", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateTitleUsage() {
		CommonValidator validation = new CommonValidator();
		try {
			if (!validation.isCMLOVREC("COMMON", "CMNTITLES", titleUsage)) {
				getErrorMap().setError("titleUsage", BackOfficeErrorCodes.INVALID_OPTION);
				return false;

			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("titleUsage", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateOccupationCode() {
		MasterValidator validation = new MasterValidator();
		try {
			DTObject formDTO = getFormDTO();
			formDTO.set("OCCU_CODE", titleOccupation);
			formDTO.set("ACTION", getAction());
			if (!validation.isEmpty(titleOccupation)) {
				formDTO = validateOccupationCode(formDTO);
				if (formDTO.get(ContentManager.ERROR) != null) {
					getErrorMap().setError("titleOccupation", formDTO.get(ContentManager.ERROR));
					return false;
				}
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("titleOccupation", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public DTObject validateOccupationCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DTObject formDTO = getFormDTO();
		MasterValidator validation = new MasterValidator();
		try {
			String titleOccupation = inputDTO.get("OCCU_CODE");
			if (!validation.isEmpty(titleOccupation)) {
				if (!validation.isValidDescription(titleOccupation)) {
					getErrorMap().setError("titleOccupation", BackOfficeErrorCodes.HMS_INVALID_DESCRIPTION);
					return resultDTO;
				}
				formDTO.set("OCCU_CODE", titleOccupation);
				formDTO.set("ACTION", "USAGE");
				formDTO.set(ContentManager.FETCH, ContentManager.FETCH_ALL);
				resultDTO = validation.validateOccupationCode(formDTO);
				if (resultDTO.get(ContentManager.ERROR) != null) {
					resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
					return resultDTO;
				}
				if (!resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_RECORD_NOT_EXISTS);
					return resultDTO;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateOtherOccupation() {
		CommonValidator validation = new CommonValidator();
		try {
			if (!validation.isEmpty(otherOccupation)) {
				if (!validation.isValidDescription(otherOccupation)) {
					getErrorMap().setError("otherOccupation", BackOfficeErrorCodes.HMS_INVALID_DESCRIPTION);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("otherOccupation", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateDefMaritialStatus() {
		MasterValidator validation = new MasterValidator();
		try {
			if (!validation.isEmpty(defMaritalStatus)) {
				DTObject formDTO = getFormDTO();
				formDTO.set("MARITAL_STATUS_CODE", defMaritalStatus);
				formDTO.set(ContentManager.USER_ACTION, USAGE);
				formDTO = validateMaritalStatusCode(formDTO);
				if (formDTO.get(ContentManager.ERROR) != null) {
					getErrorMap().setError("defMaritalStatus", formDTO.get(ContentManager.ERROR));
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("defMaritalStatus", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public DTObject validateMaritalStatusCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		MasterValidator validation = new MasterValidator();
		try {
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
			resultDTO = validation.validateMaritalStatusCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateFixMaritalStatus() {
		MasterValidator validation = new MasterValidator();
		try {
			if (validation.isEmpty(defMaritalStatus)) {
				if (fixMaritalStatus) {
					getErrorMap().setError("fixMaritalStatus", BackOfficeErrorCodes.HMS_VALUE_NT_ALLWD);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("fixMaritalStatus", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateRemarks() {
		CommonValidator commonValidator = new CommonValidator();
		try {

			if (getAction().equals(ADD) && remarks != null && remarks.length() > 0) {
				if (commonValidator.isEmpty(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.HMS_MANDATORY);
					return false;
				}
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.HMS_INVALID_REMARKS);
					return false;
				}
			} else if (getAction().equals(MODIFY)) {
				if (commonValidator.isEmpty(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.HMS_MANDATORY);
					return false;
				}
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.HMS_INVALID_REMARKS);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}
}
