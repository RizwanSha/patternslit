package patterns.config.web.forms.cmn;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.FASValidator;
import patterns.config.validations.MasterValidator;
import patterns.config.web.forms.GenericFormBean;

public class msubglheadbean extends GenericFormBean {
	private static final long serialVersionUID = 1L;
	private String glHeadCode;
	private String subGlHeadCode;
	private String description;
	private String conciseDescription;
	private String custSegCode;
	private String portCode;
	private boolean enabled;
	private String remarks;

	public String getPortCode() {
		return portCode;
	}

	public void setPortCode(String portCode) {
		this.portCode = portCode;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getGlHeadCode() {
		return glHeadCode;
	}

	public void setGlHeadCode(String glHeadCode) {
		this.glHeadCode = glHeadCode;
	}

	public String getSubGlHeadCode() {
		return subGlHeadCode;
	}

	public void setSubGlHeadCode(String subGlHeadCode) {
		this.subGlHeadCode = subGlHeadCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getConciseDescription() {
		return conciseDescription;
	}

	public void setConciseDescription(String conciseDescription) {
		this.conciseDescription = conciseDescription;
	}

	public String getCustSegCode() {
		return custSegCode;
	}

	public void setCustSegCode(String custSegCode) {
		this.custSegCode = custSegCode;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void reset() {
		glHeadCode = RegularConstants.EMPTY_STRING;
		subGlHeadCode = RegularConstants.EMPTY_STRING;
		description = RegularConstants.EMPTY_STRING;
		conciseDescription = RegularConstants.EMPTY_STRING;
		custSegCode = RegularConstants.EMPTY_STRING;
		enabled = false;
		portCode = RegularConstants.EMPTY_STRING;
		remarks = RegularConstants.EMPTY_STRING;
		setProcessBO("patterns.config.framework.bo.cmn.msubglheadBO");
	}

	public void validate() {
		if (validateglHeadCode() && validatesubglHeadCode()) {
			validateDescription();
			validateConciseDescription();
			validateCustomerSegmentCode();
			validatePortfoliocode();
			validateRemarks();
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.set("GL_HEAD_CODE", glHeadCode);
				formDTO.set("SUB_GL_HEAD_CODE", subGlHeadCode);
				formDTO.set("DESCRIPTION", description);
				formDTO.set("CONCISE_DESCRIPTION", conciseDescription);
				formDTO.set("CUST_SEGMENT_CODE", custSegCode);
				formDTO.set("PORTFOLIO_CODE", portCode);
				if (getAction().equals(ADD)) {
					formDTO.set("ENABLED", decodeBooleanToString(true));
				} else {
					formDTO.set("ENABLED", decodeBooleanToString(enabled));
				}

				formDTO.set("REMARKS", remarks);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("glHeadCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
	}

	public boolean validateglHeadCode() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("GL_HEAD_CODE", glHeadCode);
			formDTO.set(ContentManager.ACTION, getAction());
			formDTO = validateGlHeadCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("glHeadCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("glHeadCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateGlHeadCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		FASValidator validation = new FASValidator();
		String glHeadCode = inputDTO.get("GL_HEAD_CODE");
		try {
			if (validation.isEmpty(glHeadCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "GL_HEAD_CODE,DESCRIPTION");
			resultDTO = validation.validateGLHeadCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;

			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validatesubglHeadCode() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("GL_HEAD_CODE", glHeadCode);
			formDTO.set("SUB_GL_HEAD_CODE", subGlHeadCode);
			formDTO.set(ContentManager.ACTION, getAction());
			formDTO = validatesubGlCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("subGlHeadCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError(subGlHeadCode, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validatesubGlCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, ContentManager.DATA_UNAVAILABLE);
		MasterValidator validation = new MasterValidator();
		String subGlHeadCode = inputDTO.get("SUB_GL_HEAD_CODE");
		String action = inputDTO.get(ContentManager.ACTION);
		try {
			if (validation.isEmpty(subGlHeadCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION,ENABLED");
			resultDTO = validation.validatesubglHeadCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (action.equals(ADD)) {
				if (ContentManager.DATA_AVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_EXISTS);
					return resultDTO;
				}
			} else {
				if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
					return resultDTO;
				}
				if (RegularConstants.ZERO.equals(resultDTO.get("ENABLED"))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.CODE_CLOSED);
					return resultDTO;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateDescription() {
		CommonValidator validation = new CommonValidator();
		try {

			if (validation.isEmpty(description)) {
				getErrorMap().setError("description", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidDescription(description)) {
				getErrorMap().setError("description", BackOfficeErrorCodes.INVALID_DESCRIPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("description", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateConciseDescription() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(conciseDescription)) {
				getErrorMap().setError("conciseDescription", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidConciseDescription(conciseDescription)) {
				getErrorMap().setError("conciseDescription", BackOfficeErrorCodes.INVALID_CONCISE_DESCRIPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("conciseDescription", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateCustomerSegmentCode() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("CUST_SEGMENT_CODE", custSegCode);
			formDTO.set(ContentManager.ACTION, USAGE);
			formDTO = validateCustomerSegmentCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("custSegCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("custSegCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateCustomerSegmentCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		MasterValidator validation = new MasterValidator();
		String custSegCode = inputDTO.get("CUST_SEGMENT_CODE");
		try {
			if (validation.isEmpty(custSegCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "CUST_SEGMENT_CODE,DESCRIPTION");
			resultDTO = validation.validateCustomerSegmentCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validatePortfoliocode() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("PORTFOLIO_CODE", portCode);
			formDTO.set(ContentManager.ACTION, USAGE);
			formDTO = validatePortfoliocode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("portCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("portCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validatePortfoliocode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		MasterValidator validation = new MasterValidator();
		String portCode = inputDTO.get("PORTFOLIO_CODE");
		try {
			if (validation.isEmpty(portCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "PORTFOLIO_CODE,DESCRIPTION,ENABLED");
			resultDTO = validation.validatePortfoliocode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateRemarks() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (remarks != null && remarks.length() > 0) {
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			if (getAction().equals(MODIFY)) {
				if (commonValidator.isEmpty(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}
}
