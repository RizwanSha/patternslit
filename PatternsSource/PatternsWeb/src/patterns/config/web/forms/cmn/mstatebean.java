package patterns.config.web.forms.cmn;

import java.util.ArrayList;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.GenericOption;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.MasterValidator;
import patterns.config.web.forms.GenericFormBean;

public class mstatebean extends GenericFormBean {

	private static final long serialVersionUID = -6219657363150466160L;
	private String stateCode;
	private String name;
	private String shortName;
	private String stateOrUnionTerritory;
	private String oldStateCodeSplitOnDate;
	private String newStateFormationDate;
	private String invoiceType;
	private boolean enabled;
	private String remarks;

	public String getStateCode() {
		return stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public String getStateOrUnionTerritory() {
		return stateOrUnionTerritory;
	}

	public void setStateOrUnionTerritory(String stateOrUnionTerritory) {
		this.stateOrUnionTerritory = stateOrUnionTerritory;
	}

	public String getOldStateCodeSplitOnDate() {
		return oldStateCodeSplitOnDate;
	}

	public void setOldStateCodeSplitOnDate(String oldStateCodeSplitOnDate) {
		this.oldStateCodeSplitOnDate = oldStateCodeSplitOnDate;
	}

	public String getNewStateFormationDate() {
		return newStateFormationDate;
	}

	public void setNewStateFormationDate(String newStateFormationDate) {
		this.newStateFormationDate = newStateFormationDate;
	}

	public String getInvoiceType() {
		return invoiceType;
	}

	public void setInvoiceType(String invoiceType) {
		this.invoiceType = invoiceType;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public void reset() {
		stateCode = RegularConstants.EMPTY_STRING;
		name = RegularConstants.EMPTY_STRING;
		shortName = RegularConstants.EMPTY_STRING;
		stateOrUnionTerritory = RegularConstants.EMPTY_STRING;
		oldStateCodeSplitOnDate = RegularConstants.EMPTY_STRING;
		newStateFormationDate = RegularConstants.EMPTY_STRING;
		invoiceType = RegularConstants.EMPTY_STRING;
		enabled = false;
		remarks = RegularConstants.EMPTY_STRING;
		DBContext dbContext = new DBContext();
		ArrayList<GenericOption> stateOrUnionTerritory = null;
		stateOrUnionTerritory = getGenericOptions("MSTATE", "STATE_OR_TERR", dbContext, RegularConstants.NULL);
		webContext.getRequest().setAttribute("MSTATE_STATE_OR_TERR", stateOrUnionTerritory);
		ArrayList<GenericOption> invoiceType = null;
		invoiceType = getGenericOptions("COMMON", "INVOICE_TYPE", dbContext, RegularConstants.NULL);
		webContext.getRequest().setAttribute("COMMON_INVOICE_TYPE", invoiceType);
		dbContext.close();
		setProcessBO("patterns.config.framework.bo.cmn.mstateBO");
	}

	@Override
	public void validate() {
		if (validateStateCode()) {
			validateName();
			validateShortName();
			validateStateOrUnionTerritory();
			validateOldStateCodeSplitOnDate();
			validateNewStateFormationDate();
			validateInvoiceTypeForState();
			validateRemarks();
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.set("STATE_CODE", stateCode);
				formDTO.set("DESCRIPTION", name);
				formDTO.set("CONCISE_DESCRIPTION", shortName);
				formDTO.set("STATE_UT", stateOrUnionTerritory);
				formDTO.set("OLD_STATE_SPLIT_ON", oldStateCodeSplitOnDate);
				formDTO.set("NEW_STATE_FORMATION_ON", newStateFormationDate);
				formDTO.set("INVOICE_TYPE", invoiceType);
				if (getAction().equals(ADD)) {
					formDTO.set("ENABLED", decodeBooleanToString(true));
				} else {
					formDTO.set("ENABLED", decodeBooleanToString(enabled));
				}
				formDTO.set("REMARKS", remarks);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("stateCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
	}

	public boolean validateStateCode() {
		try {

			DTObject formDTO = getFormDTO();
			formDTO.set("STATE_CODE", stateCode);
			formDTO.set(ContentManager.USER_ACTION, getAction());
			formDTO = validateStateCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("stateCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("stateCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateStateCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		MasterValidator validation = new MasterValidator();
		String stateCode = inputDTO.get("STATE_CODE");
		String action = inputDTO.get(ContentManager.USER_ACTION);
		try {
			if (validation.isEmpty(stateCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
			resultDTO = validation.validateStateCode(inputDTO);
			if (action.equals(ADD)) {
				if (ContentManager.DATA_AVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_EXISTS);
					return resultDTO;
				}
			} else {
				if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_NOT_EXISTS);
					return resultDTO;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateName() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(name)) {
				getErrorMap().setError("name", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidDescription(name)) {
				getErrorMap().setError("name", BackOfficeErrorCodes.INVALID_DESCRIPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("name", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateShortName() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(shortName)) {
				getErrorMap().setError("shortName", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidConciseDescription(shortName)) {
				getErrorMap().setError("shortName", BackOfficeErrorCodes.INVALID_DESCRIPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("shortName", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateStateOrUnionTerritory() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(stateOrUnionTerritory)) {
				getErrorMap().setError("stateOrUnionTerritory", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isCMLOVREC("MSTATE", "STATE_OR_TERR", stateOrUnionTerritory)) {
				getErrorMap().setError("stateOrUnionTerritory", BackOfficeErrorCodes.INVALID_OPTION);
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("stateOrUnionTerritory", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return true;
	}

	public boolean validateOldStateCodeSplitOnDate() {
		CommonValidator validation = new CommonValidator();
		try {
			if (!validation.isEmpty(oldStateCodeSplitOnDate)) {
				if (validation.isValidDate(oldStateCodeSplitOnDate) == null) {
					getErrorMap().setError("oldStateCodeSplitOnDate", BackOfficeErrorCodes.INVALID_OPTION);
					return false;
				}
				if (validation.isDateGreaterThanCBD(oldStateCodeSplitOnDate)) {
					getErrorMap().setError("oldStateCodeSplitOnDate", BackOfficeErrorCodes.GREATER_CBD_DATE);
					return false;
				}
				if (validation.isDateEqualCBD(oldStateCodeSplitOnDate)) {
					getErrorMap().setError("oldStateCodeSplitOnDate", BackOfficeErrorCodes.EQUAL_TO_CBD);
					return false;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("oldStateCodeSplitOnDate", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return true;
	}

	public boolean validateNewStateFormationDate() {
		CommonValidator validation = new CommonValidator();
		try {
			if (!validation.isEmpty(newStateFormationDate)) {
				if (validation.isValidDate(newStateFormationDate) == null) {
					getErrorMap().setError("newStateFormationDate", BackOfficeErrorCodes.INVALID_OPTION);
					return false;
				}
				if (validation.isDateGreaterThanCBD(newStateFormationDate)) {
					getErrorMap().setError("newStateFormationDate", BackOfficeErrorCodes.GREATER_CBD_DATE);
					return false;
				}
				if (!validation.isDateGreaterThanEqual(newStateFormationDate, oldStateCodeSplitOnDate)) {
					getErrorMap().setError("newStateFormationDate", BackOfficeErrorCodes.GREATER_OLD_SPLIT_DATE);
					return false;
				}
				if (validation.isDateEqualCBD(newStateFormationDate)) {
					getErrorMap().setError("newStateFormationDate", BackOfficeErrorCodes.EQUAL_TO_CBD);
					return false;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("newStateFormationDate", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return true;
	}

	public boolean validateInvoiceTypeForState() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(invoiceType)) {
				getErrorMap().setError("invoiceType", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isCMLOVREC("COMMON", "INVOICE_TYPE", invoiceType)) {
				getErrorMap().setError("invoiceType", BackOfficeErrorCodes.INVALID_OPTION);
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("invoiceType", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return true;
	}

	public boolean validateRemarks() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (remarks != null && remarks.length() > 0) {
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			if (getAction().equals(MODIFY)) {
				if (commonValidator.isEmpty(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}
}
