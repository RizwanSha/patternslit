package patterns.config.web.forms.cmn;

import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;

import patterns.config.constants.ProgramConstants;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.BindParameterType;
import patterns.config.framework.database.DBEntry;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.framework.web.GenericOption;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.FASValidator;
import patterns.config.validations.MasterValidator;
import patterns.config.web.forms.GenericFormBean;

/**
 * 
 * 
 * @version 1.0
 * @author Vivekananda Reddy
 * @since 09-DEC-2016 <br>
 *        <b>Modified History</b> <BR>
 *        <U><B> Sl.No Modified Date Author Modified Changes Version </B></U> <br>
 * 
 * 
 */
public class enomosbean extends GenericFormBean {
	private static final long serialVersionUID = 1L;
	private String glHead;
	private String systemRefNo;
	private String dateOfEntry;
	private String entryType;
	private String interBranchTranType;
	private String originatingDebitsAllowed;
	private String debitOrCredit;
	private String orgEntryAmtActl;
	private String orgEntryAmtActl_curr;
	private String orgEntryAmtActlFormat;
	private String orgEntryAmtBase;
	private String orgEntryAmtBase_curr;
	private String orgEntryAmtBaseFormat;
	private String manualEntryRefNo;
	private String systemEntryRefDate;
	private String systemEntryRefBatchNo;
	private String systemEntryRefSerial;
	private String openBalOfEntryActl;
	private String openBalOfEntryActl_curr;
	private String openBalOfEntryActlFormat;
	private String openBalOfEntryBase;
	private String openBalOfEntryBase_curr;
	private String openBalOfEntryBaseFormat;
	private String openBalActDbOrCr;
	private String openBalBaseDbOrCr;
	private String openingBalbaseDate;
	private String cifNumber;
	private String staffId;
	private String notes;
	private String numberingRequired;
	private String folioRelationship;
	private String manualNumbersAllowed;
	private String tempPrecedingEntry;
	private String tempReversal;

	public String getTempPrecedingEntry() {
		return tempPrecedingEntry;
	}

	public void setTempPrecedingEntry(String tempPrecedingEntry) {
		this.tempPrecedingEntry = tempPrecedingEntry;
	}

	public String getTempReversal() {
		return tempReversal;
	}

	public void setTempReversal(String tempReversal) {
		this.tempReversal = tempReversal;
	}

	public String getGlHead() {
		return glHead;
	}

	public void setGlHead(String glHead) {
		this.glHead = glHead;
	}

	public String getSystemRefNo() {
		return systemRefNo;
	}

	public void setSystemRefNo(String systemRefNo) {
		this.systemRefNo = systemRefNo;
	}

	public String getDateOfEntry() {
		return dateOfEntry;
	}

	public void setDateOfEntry(String dateOfEntry) {
		this.dateOfEntry = dateOfEntry;
	}

	public String getEntryType() {
		return entryType;
	}

	public void setEntryType(String entryType) {
		this.entryType = entryType;
	}

	public String getInterBranchTranType() {
		return interBranchTranType;
	}

	public void setInterBranchTranType(String interBranchTranType) {
		this.interBranchTranType = interBranchTranType;
	}

	public String getDebitOrCredit() {
		return debitOrCredit;
	}

	public void setDebitOrCredit(String debitOrCredit) {
		this.debitOrCredit = debitOrCredit;
	}

	public String getOrgEntryAmtActl() {
		return orgEntryAmtActl;
	}

	public void setOrgEntryAmtActl(String orgEntryAmtActl) {
		this.orgEntryAmtActl = orgEntryAmtActl;
	}

	public String getOrgEntryAmtActl_curr() {
		return orgEntryAmtActl_curr;
	}

	public void setOrgEntryAmtActl_curr(String orgEntryAmtActl_curr) {
		this.orgEntryAmtActl_curr = orgEntryAmtActl_curr;
	}

	public String getOrgEntryAmtActlFormat() {
		return orgEntryAmtActlFormat;
	}

	public void setOrgEntryAmtActlFormat(String orgEntryAmtActlFormat) {
		this.orgEntryAmtActlFormat = orgEntryAmtActlFormat;
	}

	public String getOrgEntryAmtBase() {
		return orgEntryAmtBase;
	}

	public void setOrgEntryAmtBase(String orgEntryAmtBase) {
		this.orgEntryAmtBase = orgEntryAmtBase;
	}

	public String getOrgEntryAmtBase_curr() {
		return orgEntryAmtBase_curr;
	}

	public void setOrgEntryAmtBase_curr(String orgEntryAmtBase_curr) {
		this.orgEntryAmtBase_curr = orgEntryAmtBase_curr;
	}

	public String getOrgEntryAmtBaseFormat() {
		return orgEntryAmtBaseFormat;
	}

	public void setOrgEntryAmtBaseFormat(String orgEntryAmtBaseFormat) {
		this.orgEntryAmtBaseFormat = orgEntryAmtBaseFormat;
	}

	public String getManualEntryRefNo() {
		return manualEntryRefNo;
	}

	public void setManualEntryRefNo(String manualEntryRefNo) {
		this.manualEntryRefNo = manualEntryRefNo;
	}

	public String getSystemEntryRefDate() {
		return systemEntryRefDate;
	}

	public void setSystemEntryRefDate(String systemEntryRefDate) {
		this.systemEntryRefDate = systemEntryRefDate;
	}

	public String getSystemEntryRefBatchNo() {
		return systemEntryRefBatchNo;
	}

	public void setSystemEntryRefBatchNo(String systemEntryRefBatchNo) {
		this.systemEntryRefBatchNo = systemEntryRefBatchNo;
	}

	public String getSystemEntryRefSerial() {
		return systemEntryRefSerial;
	}

	public void setSystemEntryRefSerial(String systemEntryRefSerial) {
		this.systemEntryRefSerial = systemEntryRefSerial;
	}

	public String getOpenBalOfEntryActl() {
		return openBalOfEntryActl;
	}

	public void setOpenBalOfEntryActl(String openBalOfEntryActl) {
		this.openBalOfEntryActl = openBalOfEntryActl;
	}

	public String getOpenBalOfEntryActl_curr() {
		return openBalOfEntryActl_curr;
	}

	public void setOpenBalOfEntryActl_curr(String openBalOfEntryActl_curr) {
		this.openBalOfEntryActl_curr = openBalOfEntryActl_curr;
	}

	public String getOpenBalOfEntryActlFormat() {
		return openBalOfEntryActlFormat;
	}

	public void setOpenBalOfEntryActlFormat(String openBalOfEntryActlFormat) {
		this.openBalOfEntryActlFormat = openBalOfEntryActlFormat;
	}

	public String getOpenBalOfEntryBase() {
		return openBalOfEntryBase;
	}

	public void setOpenBalOfEntryBase(String openBalOfEntryBase) {
		this.openBalOfEntryBase = openBalOfEntryBase;
	}

	public String getOpenBalOfEntryBase_curr() {
		return openBalOfEntryBase_curr;
	}

	public void setOpenBalOfEntryBase_curr(String openBalOfEntryBase_curr) {
		this.openBalOfEntryBase_curr = openBalOfEntryBase_curr;
	}

	public String getOpenBalOfEntryBaseFormat() {
		return openBalOfEntryBaseFormat;
	}

	public void setOpenBalOfEntryBaseFormat(String openBalOfEntryBaseFormat) {
		this.openBalOfEntryBaseFormat = openBalOfEntryBaseFormat;
	}

	public String getOpeningBalbaseDate() {
		return openingBalbaseDate;
	}

	public void setOpeningBalbaseDate(String openingBalbaseDate) {
		this.openingBalbaseDate = openingBalbaseDate;
	}

	public String getCifNumber() {
		return cifNumber;
	}

	public void setCifNumber(String cifNumber) {
		this.cifNumber = cifNumber;
	}

	public String getStaffId() {
		return staffId;
	}

	public void setStaffId(String staffId) {
		this.staffId = staffId;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getOpenBalActDbOrCr() {
		return openBalActDbOrCr;
	}

	public void setOpenBalActDbOrCr(String openBalActDbOrCr) {
		this.openBalActDbOrCr = openBalActDbOrCr;
	}

	public String getOpenBalBaseDbOrCr() {
		return openBalBaseDbOrCr;
	}

	public void setOpenBalBaseDbOrCr(String openBalBaseDbOrCr) {
		this.openBalBaseDbOrCr = openBalBaseDbOrCr;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getOriginatingDebitsAllowed() {
		return originatingDebitsAllowed;
	}

	public void setOriginatingDebitsAllowed(String originatingDebitsAllowed) {
		this.originatingDebitsAllowed = originatingDebitsAllowed;
	}

	public String getNumberingRequired() {
		return numberingRequired;
	}

	public void setNumberingRequired(String numberingRequired) {
		this.numberingRequired = numberingRequired;
	}

	public String getFolioRelationship() {
		return folioRelationship;
	}

	public void setFolioRelationship(String folioRelationship) {
		this.folioRelationship = folioRelationship;
	}

	public String getManualNumbersAllowed() {
		return manualNumbersAllowed;
	}

	public void setManualNumbersAllowed(String manualNumbersAllowed) {
		this.manualNumbersAllowed = manualNumbersAllowed;
	}

	public void reset() {
		glHead = RegularConstants.EMPTY_STRING;
		systemRefNo = RegularConstants.EMPTY_STRING;
		dateOfEntry = RegularConstants.EMPTY_STRING;
		entryType = RegularConstants.EMPTY_STRING;
		interBranchTranType = RegularConstants.EMPTY_STRING;
		originatingDebitsAllowed = RegularConstants.EMPTY_STRING;
		debitOrCredit = RegularConstants.EMPTY_STRING;
		orgEntryAmtActl = RegularConstants.EMPTY_STRING;
		orgEntryAmtActl_curr = RegularConstants.EMPTY_STRING;
		orgEntryAmtActlFormat = RegularConstants.EMPTY_STRING;
		orgEntryAmtBase = RegularConstants.EMPTY_STRING;
		orgEntryAmtBase_curr = RegularConstants.EMPTY_STRING;
		orgEntryAmtBaseFormat = RegularConstants.EMPTY_STRING;
		manualEntryRefNo = RegularConstants.EMPTY_STRING;
		systemEntryRefDate = RegularConstants.EMPTY_STRING;
		systemEntryRefBatchNo = RegularConstants.EMPTY_STRING;
		systemEntryRefSerial = RegularConstants.EMPTY_STRING;
		openBalOfEntryActl = RegularConstants.EMPTY_STRING;
		openBalOfEntryActl_curr = RegularConstants.EMPTY_STRING;
		openBalOfEntryActlFormat = RegularConstants.EMPTY_STRING;
		openBalOfEntryBase = RegularConstants.EMPTY_STRING;
		openBalOfEntryBase_curr = RegularConstants.EMPTY_STRING;
		openBalOfEntryBaseFormat = RegularConstants.EMPTY_STRING;
		openBalActDbOrCr = RegularConstants.EMPTY_STRING;
		openBalBaseDbOrCr = RegularConstants.EMPTY_STRING;
		openingBalbaseDate = RegularConstants.EMPTY_STRING;
		cifNumber = RegularConstants.EMPTY_STRING;
		staffId = RegularConstants.EMPTY_STRING;
		notes = RegularConstants.EMPTY_STRING;
		numberingRequired = RegularConstants.EMPTY_STRING;
		folioRelationship = RegularConstants.EMPTY_STRING;
		manualNumbersAllowed = RegularConstants.EMPTY_STRING;

		tempPrecedingEntry = RegularConstants.EMPTY_STRING;
		tempReversal = RegularConstants.EMPTY_STRING;
		DBContext dbContext = new DBContext();
		ArrayList<GenericOption> debitCredit = null;
		debitCredit = getGenericOptions("COMMON", "DEBIT_CREDIT", dbContext, "--");
		webContext.getRequest().setAttribute("COMMON_DEBIT_CREDIT", debitCredit);

		ArrayList<GenericOption> reconTypeList = null;
		reconTypeList = getGenericOptions("COMMON", "GL_RECON_TYPE", dbContext, "--");
		webContext.getRequest().setAttribute("COMMON_GL_RECON_TYPE", reconTypeList);

		ArrayList<GenericOption> entryTypeList = null;
		entryTypeList = getGenericOptions("ENOMOS", "ENTRY_TYPE", dbContext, "--");
		webContext.getRequest().setAttribute("ENOMOS_ENTRY_TYPE", entryTypeList);

		dbContext.close();
		setProcessBO("patterns.config.framework.bo.cmn.enomosBO");
	}

	public void validate() {
		if (validateGlHead() && validateSystemRefNo()) {
			validateDateOfEntry();
			validateInterBranchTranType();
			validateEntryType();
			validateDebitOrCredit();
			validateOrgEntryAmtActlFormat();
			validateOrgEntryAmtBaseFormat();
			if (getAction().equals(MODIFY)) {
				validateSystemEntryRefDate();
				validateSystemEntryRefBatchNo();
				validateSystemEntryRefSerial();
			}
			if ("N".equals(entryType)) {
				if (RegularConstants.ONE.equals(manualNumbersAllowed))
					validateManualEntryRefNo();
				validateOpenBalOfEntryActlFormat();
				validateOpenBalOfEntryBaseFormat();
				validateOpeningBalbaseDate();
				validateOpenBalActDbOrCr();
				if ("1".equals(folioRelationship) || "3".equals(folioRelationship))
					validateCIFNumber();
				else if ("2".equals(folioRelationship))
					validateStaffId();
			}
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.set("BRANCH_CODE", context.getBranchCode());
				formDTO.set("GL_HEAD_CODE", glHead);
				if (getAction().equals(MODIFY))
					formDTO.set("SYSTEM_REF_NO", systemRefNo);
				formDTO.set("ENTRY_TYPE", entryType);
				formDTO.set("OP_BAL_ENTRY", RegularConstants.ONE);
				formDTO.set("INTER_BRN_TRAN_TYPES", interBranchTranType);
				formDTO.set("ENTRY_DATE", dateOfEntry);
				formDTO.set("TRAN_DB_CR_FLAG", debitOrCredit);
				formDTO.set("ORIG_ENTRY_CCY_AC", orgEntryAmtActl_curr);
				formDTO.set("ORIG_ENTRY_AMT_AC", orgEntryAmtActl);
				formDTO.set("ORIG_ENTRY_CCY_BC", orgEntryAmtBase_curr);
				formDTO.set("ORIG_ENTRY_AMT_BC", orgEntryAmtBase);
				formDTO.set("NEW_REF_REQ", RegularConstants.ZERO);
				if (RegularConstants.ONE.equals(numberingRequired)) {
					if (getAction().equals(MODIFY)) {
						formDTO.set("SYSTEM_TRAN_REF_NO_DATE", systemEntryRefDate);
						formDTO.set("SYSTEM_TRAN_REF_NO_BATCH", systemEntryRefBatchNo);
						formDTO.set("SYSTEM_TRAN_REF_NO_SERIAL", systemEntryRefSerial);
						if (systemEntryRefBatchNo.isEmpty()) {
							formDTO.set("NEW_REF_REQ", RegularConstants.ONE);
							formDTO.set("SYSTEM_TRAN_REF_NO_DATE", BackOfficeFormatUtils.getDate(context.getCurrentBusinessDate(), context.getDateFormat()));
						}
					} else {
						formDTO.set("NEW_REF_REQ", RegularConstants.ONE);
						formDTO.set("SYSTEM_TRAN_REF_NO_DATE", BackOfficeFormatUtils.getDate(context.getCurrentBusinessDate(), context.getDateFormat()));
					}
				}
				if ("N".equals(entryType)) {
					if (RegularConstants.ONE.equals(manualNumbersAllowed))
						formDTO.set("MANUAL_ENTRY_REF_NO", manualEntryRefNo);

					formDTO.set("OPEN_BAL_OF_ENTRY_AC", openBalOfEntryActl);
					formDTO.set("OPEN_BAL_OF_ENTRY_DBCR_AC", openBalActDbOrCr);
					formDTO.set("OPEN_BAL_OF_ENTRY_BC", openBalOfEntryBase);
					formDTO.set("OPEN_BAL_OF_ENTRY_DBCR_BC", openBalBaseDbOrCr);
					formDTO.set("OPEN_BALANCE_BASE_DATE", openingBalbaseDate);
					if ("1".equals(folioRelationship) || "3".equals(folioRelationship))
						formDTO.set("CIF_NO", cifNumber);
					else if ("2".equals(folioRelationship))
						formDTO.set("EMP_ID", staffId);
				}
				formDTO.set("REMARKS", notes);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("glHead", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
		}
	}

	public boolean validateGlHead() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(glHead)) {
				getErrorMap().setError("glHead", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			DTObject formDTO = getFormDTO();
			formDTO.set("GL_HEAD_CODE", glHead);
			formDTO.set(ContentManager.USER_ACTION, USAGE);
			formDTO = validateGlHead(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("glHead", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("glHead", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public DTObject validateGlHead(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		FASValidator validation = new FASValidator();
		String glHeadCode = inputDTO.get("GL_HEAD_CODE");
		try {
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION,ROOT_GL_HEAD,GL_CURR_CODE,POSTING_HEAD");
			inputDTO.set("GL_HEAD_CODE", glHeadCode);
			resultDTO = validation.validateGLHead(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				return resultDTO;
			}
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
			if (!RegularConstants.ONE.equals(resultDTO.get("POSTING_HEAD"))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.PBS_POST_GL_ALLOWED);
				return resultDTO;
			}
			String description = resultDTO.get("DESCRIPTION");
			String glCurrCode = resultDTO.get("GL_CURR_CODE");
			inputDTO.set(ContentManager.FETCH_COLUMNS, "OPERATIONS_ACCOUNT,ENTRY_LEVEL_RECON_REQ,PRECEED_ENTRY_TYPE,RECON_TYPE,FOLIO_RELATIONSHIP");
			inputDTO.set("ROOT_GL_HEAD", resultDTO.get("ROOT_GL_HEAD"));
			resultDTO = validation.validateGLAttributes(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				return resultDTO;
			}
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.PBS_ROOT_GL);
				return resultDTO;
			} else {
				if (RegularConstants.ONE.equals(resultDTO.get("OPERATIONS_ACCOUNT"))) {
					if (!RegularConstants.ONE.equals(resultDTO.get("ENTRY_LEVEL_RECON_REQ"))) {
						resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.PBS_ONLY_ENTRY_LEVEL_RECON_GLS_ALLOWED);
						return resultDTO;
					}
				} else {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.PBS_ONLY_OPR_ACNT_GLS_ALLOWED);
					return resultDTO;
				}
			}
			resultDTO.set("DESCRIPTION", description);
			resultDTO.set("GL_CURR_CODE", glCurrCode);
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateSystemRefNo() {
		CommonValidator validation = new CommonValidator();
		try {
			if (getAction().equals(MODIFY)) {
				if (validation.isEmpty(systemRefNo)) {
					getErrorMap().setError("systemRefNo", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				DTObject formDTO = getFormDTO();
				formDTO.set("GL_HEAD_CODE", glHead);
				formDTO.set("SYSTEM_REF_NO", systemRefNo);
				formDTO.set(ContentManager.USER_ACTION, getAction());
				formDTO = validateSystemRefNo(formDTO);
				if (formDTO.get(ContentManager.ERROR) != null) {
					getErrorMap().setError("systemRefNo", formDTO.get(ContentManager.ERROR));
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("systemRefNo", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public DTObject validateSystemRefNo(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		CommonValidator validation = new CommonValidator();
		String action = inputDTO.get(ContentManager.USER_ACTION);
		try {
			HashMap<Integer, DBEntry> whereClauseFieldsMap = new HashMap<Integer, DBEntry>();
			whereClauseFieldsMap.put(1, new DBEntry("ENTITY_CODE", context.getEntityCode(), BindParameterType.VARCHAR));
			whereClauseFieldsMap.put(2, new DBEntry("GL_HEAD_CODE", inputDTO.get("GL_HEAD_CODE"), BindParameterType.VARCHAR));
			whereClauseFieldsMap.put(3, new DBEntry("BRANCH_CODE", context.getBranchCode(), BindParameterType.VARCHAR));
			whereClauseFieldsMap.put(4, new DBEntry("SYSTEM_REF_NO", Long.parseLong(inputDTO.get("SYSTEM_REF_NO")), BindParameterType.LONG));
			boolean isRecordAvailable = validation.isRecordAvailable("NOMOS", whereClauseFieldsMap);
			if (isRecordAvailable) {
				if (action.equals(ADD)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_EXISTS);
					return resultDTO;
				}
			} else {
				if (action.equals(MODIFY)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_NOT_EXISTS);
					return resultDTO;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateDateOfEntry() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(dateOfEntry)) {
				getErrorMap().setError("dateOfEntry", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (validation.isValidDate(dateOfEntry) == null) {
				getErrorMap().setError("dateOfEntry", BackOfficeErrorCodes.INVALID_DATE);
				return false;
			}
			DTObject formDTO = getFormDTO();
			formDTO.set("GL_HEAD_CODE", glHead);
			formDTO.set("ENTRY_DATE", dateOfEntry);
			formDTO = validateDateOfEntry(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("dateOfEntry", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("dateOfEntry", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public DTObject validateDateOfEntry(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		MasterValidator validation = new MasterValidator();
		FASValidator fasvalidation = new FASValidator();

		try {
			inputDTO.set(ContentManager.FETCH_COLUMNS, "TO_CHAR(DATE_OF_INCORP,'" + context.getDateFormat() + "') DATE_OF_INCORP");
			inputDTO.set("ENTITY_CODE", context.getEntityCode());
			inputDTO.set(ContentManager.ACTION, USAGE);
			resultDTO = validation.validateEntityCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				return resultDTO;
			}
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_DATE);
				return resultDTO;
			} else {
				if (validation.isDateLesser(inputDTO.get("ENTRY_DATE"), resultDTO.get("DATE_OF_INCORP"))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.PBS_DATE_CANNOT_BE_LE_DATE_OF_INCORP);
					return resultDTO;
				}
				inputDTO.set(ContentManager.FETCH_COLUMNS, "TO_CHAR(GL_OPENING_DATE,'" + context.getDateFormat() + "') GL_OPENING_DATE,TO_CHAR(GL_CLOSURE_DATE,'" + context.getDateFormat() + "') GL_CLOSURE_DATE");
				resultDTO = fasvalidation.validateGLHead(inputDTO);
				if (resultDTO.get(ContentManager.ERROR) != null) {
					return resultDTO;
				}
				if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_DATE);
					return resultDTO;
				} else {
					if (validation.isDateLesser(inputDTO.get("ENTRY_DATE"), resultDTO.get("GL_OPENING_DATE"))) {
						resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.PBS_DATE_CANNOT_BE_LE_GL_OPENING_DATE);
						return resultDTO;
					}
					if (!validation.isEmpty(resultDTO.get("GL_CLOSURE_DATE")) && validation.isDateGreaterThanEqual(inputDTO.get("ENTRY_DATE"), resultDTO.get("GL_CLOSURE_DATE"))) {
						resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.PBS_DATE_CANNOT_BE_GR_OR_EQ_GL_OPENING_DATE);
						return resultDTO;
					}
					DBUtil util = validation.getDbContext().createUtilInstance();
					util.reset();
					util.setMode(DBUtil.CALLABLE);
					String sqlQuery = "{CALL SP_ACCOUNTING_BOOK_STATUS_OF_DATE(?,?,?,?)}";
					String status = null;
					String periodSerial = null;
					util.setSql(sqlQuery);
					util.setLong(1, Long.parseLong(context.getEntityCode()));
					util.setDate(2, new java.sql.Date(BackOfficeFormatUtils.getDate(inputDTO.get("ENTRY_DATE"), context.getDateFormat()).getTime()));
					util.registerOutParameter(3, Types.VARCHAR);
					util.registerOutParameter(4, Types.VARCHAR);
					util.execute();
					status = util.getString(3);
					periodSerial = util.getString(4);
					if (status == null) {
						resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.PBS_ACCOUNTING_NOT_YET_OPENED_FOR_YEAR);
						return resultDTO;
					}
					if (!"O".equals(status)) {
						if (periodSerial == null) {
							resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.PBS_ACCOUNTING_NOT_YET_CLOSED_FOR_YEAR);
							return resultDTO;
						} else {
							resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.PBS_ACCOUNTING_NOT_YET_CLOSED_FOR_QUARTER);
							return resultDTO;
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
			fasvalidation.close();
		}
		return resultDTO;
	}

	public boolean validateInterBranchTranType() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(interBranchTranType)) {
				getErrorMap().setError("interBranchTranType", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			DTObject formDTO = getFormDTO();
			formDTO.set("INTER_BRN_TRAN_TYPES", interBranchTranType);
			formDTO.set(ContentManager.USER_ACTION, USAGE);
			formDTO = validateInterBranchTranType(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("interBranchTranType", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("interBranchTranType", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public DTObject validateInterBranchTranType(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		MasterValidator validation = new MasterValidator();
		try {
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION,ORIGINATING_DEBITS_ALLOWED,NUMBERING_REQUIRED,MANUAL_NUMBERS_ALLOWED");
			resultDTO = validation.validateInterBankTranType(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateEntryType() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(entryType)) {
				getErrorMap().setError("entryType", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isCMLOVREC("ENOMOS", "ENTRY_TYPE", entryType)) {
				getErrorMap().setError("entryType", BackOfficeErrorCodes.INVALID_OPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("entryType", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateDebitOrCredit() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(debitOrCredit)) {
				getErrorMap().setError("debitOrCredit", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isCMLOVREC("COMMON", "DEBIT_CREDIT", debitOrCredit)) {
				getErrorMap().setError("debitOrCredit", BackOfficeErrorCodes.INVALID_OPTION);
				return false;
			}
			if (RegularConstants.DEBIT.equals(debitOrCredit)) {
				if ("N".equals(entryType) && RegularConstants.CREDIT.equals(tempPrecedingEntry)) {
					getErrorMap().setError("debitOrCredit", BackOfficeErrorCodes.PBS_ONLY_CREDIT_ALLOWED_FOR_REVERSAL);
					return false;
				}
				if ("R".equals(entryType) && RegularConstants.DEBIT.equals(tempPrecedingEntry)) {
					getErrorMap().setError("debitOrCredit", BackOfficeErrorCodes.PBS_ONLY_CREDIT_ALLOWED_FOR_REVERSAL);
					return false;
				}
				if (RegularConstants.ZERO.equals(originatingDebitsAllowed)) {
					getErrorMap().setError("debitOrCredit", BackOfficeErrorCodes.PBS_ORGINATE_DEBIT_NOT_ALLOWED_FOR_TRAN_TYPE);
					return false;
				}
			} else if (RegularConstants.CREDIT.equals(debitOrCredit)) {
				if ("N".equals(entryType) && RegularConstants.DEBIT.equals(tempPrecedingEntry)) {
					getErrorMap().setError("debitOrCredit", BackOfficeErrorCodes.PBS_ONLY_DEBIT_ALLOWED_FOR_REVERSAL);
					return false;
				}
				if ("R".equals(entryType) && RegularConstants.CREDIT.equals(tempPrecedingEntry)) {
					getErrorMap().setError("debitOrCredit", BackOfficeErrorCodes.PBS_ONLY_DEBIT_ALLOWED_FOR_REVERSAL);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("debitOrCredit", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateOrgEntryAmtActlFormat() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(orgEntryAmtActl)) {
				getErrorMap().setError("orgEntryAmtActl", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (validation.isZeroAmount(orgEntryAmtActl)) {
				getErrorMap().setError("orgEntryAmtActl", BackOfficeErrorCodes.PBS_ZERO_AMOUNT);
				return false;
			}
			DTObject formDTO = new DTObject();
			formDTO.set("CURRENCY_CODE", orgEntryAmtActl_curr);
			formDTO.set(ProgramConstants.AMOUNT, orgEntryAmtActl);
			formDTO = validation.validateCurrencyBigAmount(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("orgEntryAmtActl", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("orgEntryAmtActl", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateOrgEntryAmtBaseFormat() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(orgEntryAmtBase)) {
				getErrorMap().setError("orgEntryAmtBase", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (validation.isZeroAmount(orgEntryAmtBase)) {
				getErrorMap().setError("orgEntryAmtBase", BackOfficeErrorCodes.PBS_ZERO_AMOUNT);
				return false;
			}
			DTObject formDTO = new DTObject();
			formDTO.set("CURRENCY_CODE", orgEntryAmtBase_curr);
			formDTO.set(ProgramConstants.AMOUNT, orgEntryAmtBase);
			formDTO = validation.validateCurrencyBigAmount(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("orgEntryAmtBase", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("orgEntryAmtBase", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateManualEntryRefNo() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(manualEntryRefNo)) {
				getErrorMap().setError("manualEntryRefNo", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("manualEntryRefNo", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateSystemEntryRefDate() {
		CommonValidator validation = new CommonValidator();
		try {
			if (!validation.isEmpty(systemEntryRefDate)) {
				if (validation.isValidDate(systemEntryRefDate) == null) {
					getErrorMap().setError("systemEntryRef", BackOfficeErrorCodes.INVALID_DATE);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("systemEntryRef", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateSystemEntryRefBatchNo() {
		CommonValidator validation = new CommonValidator();
		try {
			if (!validation.isEmpty(systemEntryRefBatchNo)) {
				if (!validation.isValidNumber(systemEntryRefBatchNo)) {
					getErrorMap().setError("systemEntryRef", BackOfficeErrorCodes.INVALID_NUMBER);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("systemEntryRef", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateSystemEntryRefSerial() {
		CommonValidator validation = new CommonValidator();
		try {
			if (!validation.isEmpty(systemEntryRefSerial)) {
				if (!validation.isValidNumber(systemEntryRefSerial)) {
					getErrorMap().setError("systemEntryRef", BackOfficeErrorCodes.INVALID_NUMBER);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("systemEntryRef", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateOpenBalOfEntryActlFormat() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(openBalOfEntryActl)) {
				setOpenBalOfEntryActl(RegularConstants.ZERO);
			}
			DTObject formDTO = new DTObject();
			formDTO.set("CURRENCY_CODE", openBalOfEntryActl_curr);
			formDTO.set(ProgramConstants.AMOUNT, openBalOfEntryActl);
			formDTO = validation.validateCurrencyBigAmount(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("openBalOfEntryActl", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("openBalOfEntryActl", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateOpenBalOfEntryBaseFormat() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(openBalOfEntryBase)) {
				setOpenBalOfEntryBase(RegularConstants.ZERO);
			}
			DTObject formDTO = new DTObject();
			formDTO.set("CURRENCY_CODE", openBalOfEntryBase_curr);
			formDTO.set(ProgramConstants.AMOUNT, openBalOfEntryBase);
			formDTO = validation.validateCurrencyBigAmount(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("openBalOfEntryBase", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("openBalOfEntryBase", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateOpeningBalbaseDate() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(openingBalbaseDate)) {
				getErrorMap().setError("openingBalbaseDate", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (validation.isValidDate(openingBalbaseDate) == null) {
				getErrorMap().setError("openingBalbaseDate", BackOfficeErrorCodes.INVALID_DATE);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("openingBalbaseDate", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateOpenBalActDbOrCr() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(openBalActDbOrCr)) {
				getErrorMap().setError("openBalActDbOrCr", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isCMLOVREC("COMMON", "DEBIT_CREDIT", openBalActDbOrCr)) {
				getErrorMap().setError("openBalActDbOrCr", BackOfficeErrorCodes.INVALID_OPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("openBalActDbOrCr", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateCIFNumber() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(cifNumber)) {
				getErrorMap().setError("cifNumber", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			DTObject formDTO = getFormDTO();
			formDTO.set("CIF_NO", cifNumber);
			formDTO.set("FOLIO_RELATIONSHIP", folioRelationship);
			formDTO.set(ContentManager.USER_ACTION, USAGE);
			formDTO = validateCIFNumber(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("cifNumber", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("cifNumber", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public DTObject validateCIFNumber(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		MasterValidator validation = new MasterValidator();
		try {
			inputDTO.set(ContentManager.FETCH_COLUMNS, "NAME,CORP_CUSTOMER");
			resultDTO = validation.validateCIF(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			} else {
				if (inputDTO.get("FOLIO_RELATIONSHIP").equals("1") && RegularConstants.ONE.equals(resultDTO.get("CORP_CUSTOMER"))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.PBS_CORP_CUST_NOT_ALLOWED);
					return resultDTO;
				}
				if (inputDTO.get("FOLIO_RELATIONSHIP").equals("3") && RegularConstants.ZERO.equals(resultDTO.get("CORP_CUSTOMER"))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.PBS_ONLY_CORP_CUST_ALLOWED);
					return resultDTO;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateStaffId() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(staffId)) {
				getErrorMap().setError("staffId", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			DTObject formDTO = getFormDTO();
			formDTO.set("EMP_CODE", staffId);
			formDTO.set(ContentManager.USER_ACTION, USAGE);
			formDTO = validateStaffId(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("staffId", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("staffId", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public DTObject validateStaffId(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		MasterValidator validation = new MasterValidator();
		try {
			inputDTO.set(ContentManager.FETCH_COLUMNS, "PERSON_NAME");
			resultDTO = validation.validateEmployeeCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateNotes() {
		CommonValidator validation = new CommonValidator();
		try {
			if (notes != null && notes.length() > 0) {
				if (!validation.isValidRemarks(notes)) {
					getErrorMap().setError("notes", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			if (getAction().equals(MODIFY)) {
				if (validation.isEmpty(notes)) {
					getErrorMap().setError("notes", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!validation.isValidRemarks(notes)) {
					getErrorMap().setError("notes", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("notes", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}
}