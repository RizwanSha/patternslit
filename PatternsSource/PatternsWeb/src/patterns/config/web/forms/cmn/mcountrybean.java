package patterns.config.web.forms.cmn;

/**
 * This program will be used to create codes to represent Countries.
 * The program will also configure if the country code needs a Geographical Organization -
 * that would be used while specifying the Address.

 * @version 1.0
 * @author  Prashanth
 * @since   25-Nov-2014
 * <br>
 * <b>Modified History</b>
 * <BR>
 * <U><B>
 * Sl.No		Modified Date	Author			Modified Changes		Version
 * </B></U>
 * <br>

 */
import patterns.config.framework.bo.BackOfficeErrorCodes;

import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.MasterValidator;
import patterns.config.validations.CommonValidator;
import patterns.config.web.forms.GenericFormBean;

public class mcountrybean extends GenericFormBean {

	private static final long serialVersionUID = 1L;
	public String countryCode;
	public String countryName;
	public String shortName;
	public String geoUnitStructureCode;
	public boolean enabled;

	public String remarks;

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	@Override
	public void reset() {
		// TODO Auto-generated method stub
		countryCode = RegularConstants.EMPTY_STRING;
		countryName = RegularConstants.EMPTY_STRING;
		shortName = RegularConstants.EMPTY_STRING;
		geoUnitStructureCode = RegularConstants.EMPTY_STRING;
		enabled = false;
		remarks = RegularConstants.EMPTY_STRING;
		setProcessBO("patterns.config.framework.bo.cmn.mcountryBO");

	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public String getGeoUnitStructureCode() {
		return geoUnitStructureCode;
	}

	public void setGeoUnitStructureCode(String geoUnitStructureCode) {
		this.geoUnitStructureCode = geoUnitStructureCode;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public void validate() {
		MasterValidator validation = new MasterValidator();
		if (validateCountryCode()) {
			validatecountryName();
			validateshortName();
			validateGeographicalUnitStructureCode();
			validateRemarks();

		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("COUNTRY_CODE", countryCode);
				formDTO.set("DESCRIPTION", countryName);
				formDTO.set("CONCISE_DESCRIPTION", shortName);
				formDTO.set("GEO_UNIT_STRUC_CODE", geoUnitStructureCode);
				if (getAction().equals(ADD)) {
					formDTO.set("ENABLED", decodeBooleanToString(true));
				} else {
					formDTO.set("ENABLED", decodeBooleanToString(enabled));
				}
				if (!validation.isEmpty(geoUnitStructureCode)) {
					formDTO.set("GEO_UNIT_STRUC_REQ", decodeBooleanToString(true));
				} else {
					formDTO.set("GEO_UNIT_STRUC_REQ", decodeBooleanToString(false));
				}
				formDTO.set("REMARKS", remarks);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("countryCode", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
	}

	public DTObject validateCountryCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		MasterValidator validation = new MasterValidator();
		String countryCode = inputDTO.get("COUNTRY_CODE");
		String action = inputDTO.get("ACTION");
		try {
			if (validation.isEmpty(countryCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.CODE, countryCode);
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
			resultDTO = validation.validateCountryCode(inputDTO);
			if (action.equals(ADD)) {
				if (ContentManager.DATA_AVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_RECORD_EXISTS);
				}
			} else {
				if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_RECORD_NOT_EXISTS);

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public DTObject validateGeographicalUnitStructureCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		MasterValidator validation = new MasterValidator();
		String geoUnitStructureCode = inputDTO.get("GEO_UNIT_STRUC_CODE");
		try {
			inputDTO.set(ContentManager.CODE, geoUnitStructureCode);
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
			resultDTO = validation.validateGeographicalUnitStructureCode(inputDTO);
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}

		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateGeographicalUnitStructureCode() {
		MasterValidator validation = new MasterValidator();
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("GEO_UNIT_STRUC_CODE", geoUnitStructureCode);
			formDTO.set("ACTION", USAGE);
			if (!validation.isEmpty(geoUnitStructureCode)) {
				formDTO = validateGeographicalUnitStructureCode(formDTO);
				if (formDTO.get(ContentManager.ERROR) != null) {
					getErrorMap().setError("geoUnitStructureCode", formDTO.get(ContentManager.ERROR));
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("geoUnitStructureCode", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateCountryCode() {

		try {
			DTObject formDTO = new DTObject();
			formDTO.set("COUNTRY_CODE", countryCode);
			formDTO.set("ACTION", getAction());
			formDTO = validateCountryCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("countryCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("countryCode", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		}
		return false;
	}

	public boolean validatecountryName() {
		CommonValidator validation = new CommonValidator();
		try {

			if (!validation.isValidDescription(countryName)) {
				getErrorMap().setError("countryName", BackOfficeErrorCodes.HMS_INVALID_NAME);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("countryName", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateshortName() {
		CommonValidator validation = new CommonValidator();
		try {
			if (!validation.isValidConciseDescription(shortName)) {
				getErrorMap().setError("shortName", BackOfficeErrorCodes.HMS_INVALID_SHORT_NAME);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("shortName", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateRemarks() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (remarks != null && remarks.length() > 0) {
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.HMS_INVALID_REMARKS);
					return false;
				}
			}
			if (getAction().equals(MODIFY)) {
				if (commonValidator.isEmpty(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.HMS_FIELD_BLANK);
					return false;
				}
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.HMS_INVALID_REMARKS);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}

}
