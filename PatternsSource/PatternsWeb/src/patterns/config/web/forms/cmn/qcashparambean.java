package patterns.config.web.forms.cmn;

import java.sql.ResultSet;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.web.forms.GenericFormBean;

public class qcashparambean extends GenericFormBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String cashGlOfc;
	private String cashGlHeadcash;
	private String counterCashGl;
	private String recordExist;

	public String getRecordExist() {
		return recordExist;
	}

	public void setRecordExist(String recordExist) {
		this.recordExist = recordExist;
	}

	public String getCashGlOfc() {
		return cashGlOfc;
	}

	public void setCashGlOfc(String cashGlOfc) {
		this.cashGlOfc = cashGlOfc;
	}

	public String getCashGlHeadcash() {
		return cashGlHeadcash;
	}

	public void setCashGlHeadcash(String cashGlHeadcash) {
		this.cashGlHeadcash = cashGlHeadcash;
	}

	public String getCounterCashGl() {
		return counterCashGl;
	}

	public void setCounterCashGl(String counterCashGl) {
		this.counterCashGl = counterCashGl;
	}

	public String getFieldStaffCashGl() {
		return fieldStaffCashGl;
	}

	public void setFieldStaffCashGl(String fieldStaffCashGl) {
		this.fieldStaffCashGl = fieldStaffCashGl;
	}

	public String getFieldStaffCashGlbanking() {
		return fieldStaffCashGlbanking;
	}

	public void setFieldStaffCashGlbanking(String fieldStaffCashGlbanking) {
		this.fieldStaffCashGlbanking = fieldStaffCashGlbanking;
	}

	public String getInterOfcCashTransfer() {
		return interOfcCashTransfer;
	}

	public void setInterOfcCashTransfer(String interOfcCashTransfer) {
		this.interOfcCashTransfer = interOfcCashTransfer;
	}

	public boolean isDominationDtlReg() {
		return dominationDtlReg;
	}

	public void setDominationDtlReg(boolean dominationDtlReg) {
		this.dominationDtlReg = dominationDtlReg;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	private String fieldStaffCashGl;
	private String fieldStaffCashGlbanking;
	private String interOfcCashTransfer;
	private boolean dominationDtlReg;
	private String remarks;

	private String vaultWithTrans;
	private String vaultDepTrans;
	private String cashtoCashTrans;

	public String getVaultWithTrans() {
		return vaultWithTrans;
	}

	public void setVaultWithTrans(String vaultWithTrans) {
		this.vaultWithTrans = vaultWithTrans;
	}

	public String getVaultDepTrans() {
		return vaultDepTrans;
	}

	public void setVaultDepTrans(String vaultDepTrans) {
		this.vaultDepTrans = vaultDepTrans;
	}

	public String getCashtoCashTrans() {
		return cashtoCashTrans;
	}

	public void setCashtoCashTrans(String cashtoCashTrans) {
		this.cashtoCashTrans = cashtoCashTrans;
	}

	@Override
	public void reset() {
		// TODO Auto-generated method stub
		DTObject resultDTO = new DTObject();
		cashGlOfc = RegularConstants.EMPTY_STRING;
		cashGlHeadcash = RegularConstants.EMPTY_STRING;
		counterCashGl = RegularConstants.EMPTY_STRING;
		fieldStaffCashGl = RegularConstants.EMPTY_STRING;
		fieldStaffCashGlbanking = RegularConstants.EMPTY_STRING;
		interOfcCashTransfer = RegularConstants.EMPTY_STRING;
		dominationDtlReg = false;
		vaultWithTrans = RegularConstants.EMPTY_STRING;
		vaultDepTrans = RegularConstants.EMPTY_STRING;
		cashtoCashTrans = RegularConstants.EMPTY_STRING;
		remarks = RegularConstants.EMPTY_STRING;
		resultDTO = validRecordExists(resultDTO);
	}

	@Override
	public void validate() {
	}

	// TODO Auto-generated method stub

	public final DTObject validRecordExists(DTObject input) {
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		DTObject result = new DTObject();
		try {
			util.reset();
			StringBuffer query = new StringBuffer();
			query.append("SELECT S1.OFFICE_GL_HEAD,S1.HEAD_CASHIER_GL_HEAD,S1.COUNTER_CASH_GL_HEAD,S1.FIELD_STAFF_CASH_GL_HEAD,S1.BANK_CORR_CASH_GL_HEAD,S1.INTER_OFF_CASH_TRF_GL_HEAD,S1.DENOMINATION_REQ,S1.VAULT_WITHDRAWAL_FTRAN_CODE ");
			query.append(",S1.VAULT_DEPOSIT_FTRAN_CODE,S1.CTC_FTRAN_CODE,S1.REMARKS,F1.DESCRIPTION F1_DESCRIPTION,F2.DESCRIPTION F2_DESCRIPTION,F3.DESCRIPTION F3_DESCRIPTION,F4.DESCRIPTION F4_DESCRIPTION,F5.DESCRIPTION F5_DESCRIPTION ");
			query.append(",F6.DESCRIPTION F6_DESCRIPTION,F7.DESCRIPTION F7_DESCRIPTION,F8.DESCRIPTION F8_DESCRIPTION,F9.DESCRIPTION F9_DESCRIPTION FROM CASHMODPARAM S1  INNER JOIN GLMAST F1 ");
			query.append("ON S1.OFFICE_GL_HEAD = F1.GL_HEAD_CODE INNER JOIN GLMAST F2 ON S1.HEAD_CASHIER_GL_HEAD = F2.GL_HEAD_CODE LEFT OUTER JOIN GLMAST F3 ON S1.COUNTER_CASH_GL_HEAD = F3.GL_HEAD_CODE ");
			query.append("INNER JOIN GLMAST F4 ON S1.FIELD_STAFF_CASH_GL_HEAD = F4.GL_HEAD_CODE INNER JOIN GLMAST F5 ON S1.BANK_CORR_CASH_GL_HEAD = F5.GL_HEAD_CODE INNER JOIN GLMAST F6 ON S1.INTER_OFF_CASH_TRF_GL_HEAD = F6.GL_HEAD_CODE INNER JOIN BTRANCODESM F7 ");
			query.append("ON S1.VAULT_WITHDRAWAL_FTRAN_CODE = F7.BTRAN_CODE INNER JOIN BTRANCODESM F8 ON S1.VAULT_DEPOSIT_FTRAN_CODE = F8.BTRAN_CODE INNER JOIN BTRANCODESM F9 ON S1.CTC_FTRAN_CODE = F9.BTRAN_CODE WHERE S1.ENTITY_CODE = ? ");
			util.setSql(query.toString());
			util.setString(1, context.getEntityCode());
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				result.set("OFFICE_GL_HEAD", rset.getString("OFFICE_GL_HEAD"));
				result.set("HEAD_CASHIER_GL_HEAD", rset.getString("HEAD_CASHIER_GL_HEAD"));
				result.set("COUNTER_CASH_GL_HEAD", rset.getString("COUNTER_CASH_GL_HEAD"));
				result.set("FIELD_STAFF_CASH_GL_HEAD", rset.getString("FIELD_STAFF_CASH_GL_HEAD"));
				result.set("BANK_CORR_CASH_GL_HEAD", rset.getString("BANK_CORR_CASH_GL_HEAD"));
				result.set("INTER_OFF_CASH_TRF_GL_HEAD", rset.getString("INTER_OFF_CASH_TRF_GL_HEAD"));
				result.set("DENOMINATION_REQ", decodeBooleanToString(rset.getBoolean("DENOMINATION_REQ")));
				result.set("VAULT_WITHDRAWAL_FTRAN_CODE", rset.getString("VAULT_WITHDRAWAL_FTRAN_CODE"));
				result.set("VAULT_DEPOSIT_FTRAN_CODE", rset.getString("VAULT_DEPOSIT_FTRAN_CODE"));
				result.set("CTC_FTRAN_CODE", rset.getString("CTC_FTRAN_CODE"));
				result.set("REMARKS", rset.getString("REMARKS"));
				result.set("RECORDEXIST", RegularConstants.ONE);
				result.set("F1_DESCRIPTION", rset.getString("F1_DESCRIPTION"));
				result.set("F2_DESCRIPTION", rset.getString("F2_DESCRIPTION"));
				result.set("F3_DESCRIPTION", rset.getString("F3_DESCRIPTION"));
				result.set("F4_DESCRIPTION", rset.getString("F4_DESCRIPTION"));
				result.set("F5_DESCRIPTION", rset.getString("F5_DESCRIPTION"));
				result.set("F6_DESCRIPTION", rset.getString("F6_DESCRIPTION"));
				result.set("F7_DESCRIPTION", rset.getString("F7_DESCRIPTION"));
				result.set("F8_DESCRIPTION", rset.getString("F8_DESCRIPTION"));
				result.set("F9_DESCRIPTION", rset.getString("F9_DESCRIPTION"));
			} else {
				result.set("RECORDEXIST", RegularConstants.ZERO);
				result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			dbContext.close();
			util.reset();
		}
		return result;
	}

}
