/**Government Schemes Repository
 * 
 * @version 1.0
 * @author  AJAY E
 * @since   01-JUL-2016
 * <br>
 * <b>Modified History</b>
 * <BR>
 * <U><B>
 * Sl.No		Modified Date	Author			Modified Changes		Version
 * </B></U>
 * <br>
 *  
 */

package patterns.config.web.forms.cmn;

import java.util.ArrayList;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.GenericOption;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.MasterValidator;
import patterns.config.web.forms.GenericFormBean;

public class mschemebean extends GenericFormBean {

	private static final long serialVersionUID = 1L;
	private String schemeCode;
	private String description;
	private String conciseDescription;
	private String schemeFor;
	private boolean schemeHasEnrol;
	private String usingPidType;
	private String nameOfIdEnrolNo;
	private boolean paymentConrolTokenRef;
	private String nameOfTokenRef;
	private boolean enabled;
	private String remarks;
	private String parentSchemeCode;

	public String getParentSchemeCode() {
		return parentSchemeCode;
	}

	public void setParentSchemeCode(String parentSchemeCode) {
		this.parentSchemeCode = parentSchemeCode;
	}

	public String getSchemeCode() {
		return schemeCode;
	}

	public void setSchemeCode(String schemeCode) {
		this.schemeCode = schemeCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getConciseDescription() {
		return conciseDescription;
	}

	public void setConciseDescription(String conciseDescription) {
		this.conciseDescription = conciseDescription;
	}

	public String getSchemeFor() {
		return schemeFor;
	}

	public void setSchemeFor(String schemeFor) {
		this.schemeFor = schemeFor;
	}

	public boolean isSchemeHasEnrol() {
		return schemeHasEnrol;
	}

	public void setSchemeHasEnrol(boolean schemeHasEnrol) {
		this.schemeHasEnrol = schemeHasEnrol;
	}

	public String getUsingPidType() {
		return usingPidType;
	}

	public void setUsingPidType(String usingPidType) {
		this.usingPidType = usingPidType;
	}

	public String getNameOfIdEnrolNo() {
		return nameOfIdEnrolNo;
	}

	public void setNameOfIdEnrolNo(String nameOfIdEnrolNo) {
		this.nameOfIdEnrolNo = nameOfIdEnrolNo;
	}

	public boolean isPaymentConrolTokenRef() {
		return paymentConrolTokenRef;
	}

	public void setPaymentConrolTokenRef(boolean paymentConrolTokenRef) {
		this.paymentConrolTokenRef = paymentConrolTokenRef;
	}

	public String getNameOfTokenRef() {
		return nameOfTokenRef;
	}

	public void setNameOfTokenRef(String nameOfTokenRef) {
		this.nameOfTokenRef = nameOfTokenRef;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void reset() {
		schemeCode = RegularConstants.EMPTY_STRING;
		description = RegularConstants.EMPTY_STRING;
		conciseDescription = RegularConstants.EMPTY_STRING;
		schemeFor = RegularConstants.EMPTY_STRING;
		parentSchemeCode = RegularConstants.EMPTY_STRING;
		schemeHasEnrol = false;
		usingPidType = RegularConstants.EMPTY_STRING;
		nameOfIdEnrolNo = RegularConstants.EMPTY_STRING;
		paymentConrolTokenRef = false;
		nameOfTokenRef = RegularConstants.EMPTY_STRING;
		enabled = false;
		remarks = RegularConstants.EMPTY_STRING;

		DBContext dbContext = new DBContext();
		ArrayList<GenericOption> schemefor = null;
		schemefor = getGenericOptions("COMMON", "SCHEMEFOR", dbContext, null);
		webContext.getRequest().setAttribute("COMMON_SCHEMEFOR", schemefor);
		dbContext.close();

		setProcessBO("patterns.config.framework.bo.cmn.mschemeBO");
	}

	@Override
	public void validate() {
		if (validateSchemeCode()) {
			validateDescription();
			validateConciseDescription();
			validateSchemeFor();
			validateParentSchemeCode();
			validatePidCode();
			validateNameOfIdEnrolNo();
			validatePaymentConrolTokenRef();
			validateNameOfTokenRef();
			validateRemarks();
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.set("SCHEME_CODE", schemeCode);
				formDTO.set("DESCRIPTION", description);
				formDTO.set("CONCISE_DESCRIPTION", conciseDescription);
				formDTO.set("SCHEME_FOR", schemeFor);
				formDTO.set("PARENT_SCH_CODE", parentSchemeCode);
				formDTO.set("PID_CODE", usingPidType);
				formDTO.set("SCHEME_HAS_ENROL", decodeBooleanToString(schemeHasEnrol));
				formDTO.set("ENROL_DESCRIPTION", nameOfIdEnrolNo);
				formDTO.set("PAY_CONTROL_TOKEN_REF", decodeBooleanToString(paymentConrolTokenRef));
				formDTO.set("TOKEN_REF_DESCRIPTION", nameOfTokenRef);
				if (getAction().equals(ADD)) {
					formDTO.set("ENABLED", decodeBooleanToString(true));
				} else {
					formDTO.set("ENABLED", decodeBooleanToString(enabled));
				}
				formDTO.set("REMARKS", remarks);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("schemeCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
	}

	public boolean validateSchemeCode() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("SCHEME_CODE", schemeCode);
			formDTO.set("ACTION", getAction());
			formDTO = validateSchemeCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("schemeCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("schemeCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;

	}

	public DTObject validateSchemeCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		MasterValidator validation = new MasterValidator();
		String schemeCode = inputDTO.get("SCHEME_CODE");
		String action = inputDTO.get(ContentManager.USER_ACTION);
		try {
			if (validation.isEmpty(schemeCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
			resultDTO = validation.validateSchemeCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (action.equals(ADD)) {
				if (ContentManager.DATA_AVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_EXISTS);
				}
			} else {
				if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_NOT_EXISTS);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateDescription() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(description)) {
				getErrorMap().setError("description", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidDescription(description)) {
				getErrorMap().setError("description", BackOfficeErrorCodes.INVALID_DESCRIPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("description", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateConciseDescription() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(conciseDescription)) {
				getErrorMap().setError("conciseDescription", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidConciseDescription(conciseDescription)) {
				getErrorMap().setError("conciseDescription", BackOfficeErrorCodes.INVALID_CONCISE_DESCRIPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("conciseDescription", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateSchemeFor() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(schemeFor)) {
				getErrorMap().setError("schemeFor", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			} else if (!validation.isCMLOVREC("COMMON", "SCHEMEFOR", schemeFor)) {
				getErrorMap().setError("schemeFor", BackOfficeErrorCodes.INVALID_OPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("schemeFor", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;

	}

	public boolean validateParentSchemeCode() {
		CommonValidator validation = new CommonValidator();
		try {
			if (!validation.isEmpty(parentSchemeCode)) {
				DTObject formDTO = new DTObject();
				formDTO.set("SCHEME_CODE", schemeCode);
				formDTO.set("PARENT_SCH_CODE", parentSchemeCode);
				formDTO.set("ACTION", USAGE);
				formDTO = validateParentSchemeCode(formDTO);
				if (formDTO.get(ContentManager.ERROR) != null) {
					getErrorMap().setError("parentSchemeCode", formDTO.get(ContentManager.ERROR));
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("parentSchemeCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public DTObject validateParentSchemeCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		MasterValidator validation = new MasterValidator();
		String schemeCode = inputDTO.get("SCHEME_CODE");
		String parentSchemeCode = inputDTO.get("PARENT_SCH_CODE");
		try {
			if (!validation.isEmpty(schemeCode)) {
				if (schemeCode.equals(parentSchemeCode)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.PBS_SCHEME_CODE_NOT_EQ_PARENT_CODE);
					return resultDTO;
				}
				inputDTO.set("SCHEME_CODE", parentSchemeCode);
				inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
				resultDTO = validation.validateSchemeCode(inputDTO);
				if (resultDTO.get(ContentManager.ERROR) != null) {
					resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
					return resultDTO;
				}
				if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
					return resultDTO;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validatePidCode() {
		CommonValidator validation = new CommonValidator();
		try {
			if (schemeHasEnrol) {
				DTObject formDTO = new DTObject();
				formDTO.set("PID_CODE", usingPidType);
				formDTO.set("ACTION", USAGE);
				formDTO = validatePidCode(formDTO);
				if (formDTO.get(ContentManager.ERROR) != null) {
					getErrorMap().setError("usingPidType", formDTO.get(ContentManager.ERROR));
					return false;
				}
			} else {
				if (!validation.isEmpty(usingPidType)) {
					getErrorMap().setError("usingPidType", BackOfficeErrorCodes.PBS_VALUE_ALLWD_FOR_SCHEME_HAS_ID);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("usingPidType", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;

	}

	public DTObject validatePidCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.NULL);
		MasterValidator validation = new MasterValidator();
		String pidCode = inputDTO.get("PID_CODE");
		try {
			if (!validation.isEmpty(pidCode)) {
				inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
				resultDTO = validation.validatePIDCode(inputDTO);
				if (resultDTO.get(ContentManager.ERROR) != null) {
					resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
					return resultDTO;
				}
				if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
					return resultDTO;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateNameOfIdEnrolNo() {
		CommonValidator validation = new CommonValidator();
		try {
			if (schemeHasEnrol) {
				if (validation.isEmpty(nameOfIdEnrolNo)) {
					getErrorMap().setError("nameOfIdEnrolNo", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!validation.isValidDescription(nameOfIdEnrolNo)) {
					getErrorMap().setError("nameOfIdEnrolNo", BackOfficeErrorCodes.INVALID_DESCRIPTION);
					return false;
				}
			} else {
				if (!validation.isEmpty(nameOfIdEnrolNo)) {
					getErrorMap().setError("nameOfIdEnrolNo", BackOfficeErrorCodes.PBS_VALUE_ALLWD_FOR_SCHEME_HAS_ID);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("nameOfIdEnrolNo", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validatePaymentConrolTokenRef() {
		if (schemeFor.equals("A")) {
			if (paymentConrolTokenRef) {
				getErrorMap().setError("paymentConrolTokenRef", BackOfficeErrorCodes.PBS_VALUE_ALLWD_FOR_SUBSIDY_SCHEME);
				return false;
			}
		}
		return true;
	}

	public boolean validateNameOfTokenRef() {
		CommonValidator validation = new CommonValidator();
		try {
			if (schemeFor.equals("P") && paymentConrolTokenRef) {
				if (validation.isEmpty(nameOfTokenRef)) {
					getErrorMap().setError("nameOfTokenRef", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!validation.isValidDescription(nameOfTokenRef)) {
					getErrorMap().setError("nameOfTokenRef", BackOfficeErrorCodes.INVALID_DESCRIPTION);
					return false;
				}
			} else {
				if (!validation.isEmpty(nameOfTokenRef)) {
					getErrorMap().setError("nameOfTokenRef", BackOfficeErrorCodes.PBS_VALUE_ALLWD_FOR_PREISSUED_TOKEN);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("nameOfTokenRef", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateRemarks() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (remarks != null && remarks.length() > 0) {
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			if (getAction().equals(MODIFY)) {
				if (commonValidator.isEmpty(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}
}