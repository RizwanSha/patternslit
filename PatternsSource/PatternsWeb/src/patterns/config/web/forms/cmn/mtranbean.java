package patterns.config.web.forms.cmn;

import java.util.ArrayList;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.GenericOption;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.MasterValidator;
import patterns.config.web.forms.GenericFormBean;

public class mtranbean extends GenericFormBean {

	private static final long serialVersionUID = -6219657363150466160L;
	private String transactionCode;
	private String description;
	private String conciseDescription;
	private String alphaAccessCode;
	private String transactionMode;
	private String depositWithdrawal;
	private String transactionUsingDocument;
	private boolean slipNumberRequired;
	private String numberOfLegsInTheTransaction;
	private boolean enabled;
	private String remarks;

	public String getTransactionCode() {
		return transactionCode;
	}

	public void setTransactionCode(String transactionCode) {
		this.transactionCode = transactionCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getConciseDescription() {
		return conciseDescription;
	}

	public void setConciseDescription(String conciseDescription) {
		this.conciseDescription = conciseDescription;
	}

	public String getAlphaAccessCode() {
		return alphaAccessCode;
	}

	public void setAlphaAccessCode(String alphaAccessCode) {
		this.alphaAccessCode = alphaAccessCode;
	}

	public String getTransactionMode() {
		return transactionMode;
	}

	public void setTransactionMode(String transactionMode) {
		this.transactionMode = transactionMode;
	}

	public String getDepositWithdrawal() {
		return depositWithdrawal;
	}

	public void setDepositWithdrawal(String depositWithdrawal) {
		this.depositWithdrawal = depositWithdrawal;
	}

	public String getTransactionUsingDocument() {
		return transactionUsingDocument;
	}

	public void setTransactionUsingDocument(String transactionUsingDocument) {
		this.transactionUsingDocument = transactionUsingDocument;
	}

	public boolean isSlipNumberRequired() {
		return slipNumberRequired;
	}

	public void setSlipNumberRequired(boolean slipNumberRequired) {
		this.slipNumberRequired = slipNumberRequired;
	}

	public String getNumberOfLegsInTheTransaction() {
		return numberOfLegsInTheTransaction;
	}

	public void setNumberOfLegsInTheTransaction(String numberOfLegsInTheTransaction) {
		this.numberOfLegsInTheTransaction = numberOfLegsInTheTransaction;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void reset() {
		transactionCode = RegularConstants.EMPTY_STRING;
		description = RegularConstants.EMPTY_STRING;
		conciseDescription = RegularConstants.EMPTY_STRING;
		alphaAccessCode = RegularConstants.EMPTY_STRING;
		transactionMode = RegularConstants.EMPTY_STRING;
		depositWithdrawal = RegularConstants.EMPTY_STRING;
		transactionUsingDocument = RegularConstants.EMPTY_STRING;
		slipNumberRequired = false;
		numberOfLegsInTheTransaction = RegularConstants.EMPTY_STRING;
		enabled = false;
		remarks = RegularConstants.EMPTY_STRING;
		DBContext dbContext = new DBContext();
		ArrayList<GenericOption> tranMode = null;
		tranMode = getGenericOptions("COMMON", "TRANMODE", dbContext, null);
		webContext.getRequest().setAttribute("COMMON_TRANMODE", tranMode);

		ArrayList<GenericOption> tranType = null;
		tranType = getGenericOptions("COMMON", "TRANTYPE", dbContext, null);
		webContext.getRequest().setAttribute("COMMON_TRANTYPE", tranType);

		ArrayList<GenericOption> tranDoc = null;
		tranDoc = getGenericOptions("COMMON", "TRANDOC", dbContext, null);
		webContext.getRequest().setAttribute("COMMON_TRANDOC", tranDoc);

		ArrayList<GenericOption> tranLeg = null;
		tranLeg = getGenericOptions("COMMON", "TRANLEG", dbContext, null);
		webContext.getRequest().setAttribute("COMMON_TRANLEG", tranLeg);

		dbContext.close();
		setProcessBO("patterns.config.framework.bo.cmn.mtranBO");
	}

	@Override
	public void validate() {
		CommonValidator validation = new CommonValidator();
		if (validateTransactionCode()) {
			validateDescription();
			validateConciseDescription();
			validateAlphaAccessCode();
			validateTransactionMode();
			validateDepositWithdrawal();
			validateTransactionUsingDocument();
			validateSlipNumberRequired();
			validateNumberOfLegsInTheTransaction();
			validateRemarks();
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("TRAN_CODE", transactionCode);
				formDTO.set("DESCRIPTION", description);
				formDTO.set("CONCISE_DESCRIPTION", conciseDescription);
				if (validation.isEmpty(alphaAccessCode))
					formDTO.set("TRAN_ACCESS_CODE", RegularConstants.NULL);
				else
					formDTO.set("TRAN_ACCESS_CODE", alphaAccessCode);
				formDTO.set("TRAN_MODE", transactionMode);
				formDTO.set("TRAN_TYPE", depositWithdrawal);
				formDTO.set("TRAN_DOC", transactionUsingDocument);
				formDTO.set("SLIP_NUM_REQUIRED", decodeBooleanToString(slipNumberRequired));
				formDTO.set("TRAN_LEGS", numberOfLegsInTheTransaction);
				if (getAction().equals(ADD)) {
					formDTO.set("ENABLED", decodeBooleanToString(true));
				} else {
					formDTO.set("ENABLED", decodeBooleanToString(enabled));
				}

				formDTO.set("REMARKS", remarks);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("transactionCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
	}

	public boolean validateTransactionCode() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("TRAN_CODE", transactionCode);
			formDTO.set("ACTION", getAction());
			formDTO = validateTransactionCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("transactionCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("transactionCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateTransactionCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		MasterValidator validation = new MasterValidator();
		String transactionCode = inputDTO.get("TRAN_CODE");
		String action = inputDTO.get(ContentManager.USER_ACTION);
		try {
			if (validation.isEmpty(transactionCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			if (!validation.isValidNumber(transactionCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_NUMBER);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
			resultDTO = validation.validateTranCode(inputDTO);
			if (action.equals(ADD)) {
				if (ContentManager.DATA_AVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_EXISTS);
				}
			} else {
				if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_NOT_EXISTS);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateDescription() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(description)) {
				getErrorMap().setError("description", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidDescription(description)) {
				getErrorMap().setError("description", BackOfficeErrorCodes.INVALID_DESCRIPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("description", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;

	}

	public boolean validateConciseDescription() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(conciseDescription)) {
				getErrorMap().setError("conciseDescription", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidConciseDescription(conciseDescription)) {
				getErrorMap().setError("conciseDescription", BackOfficeErrorCodes.INVALID_CONCISE_DESCRIPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("conciseDescription", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateAlphaAccessCode() {
		CommonValidator validation = new CommonValidator();
		try {
			if (!validation.isEmpty(alphaAccessCode)) {
				if (!validation.isValidCode(alphaAccessCode)) {
					getErrorMap().setError("alphaAccessCode", BackOfficeErrorCodes.INVALID_VALUE);
					return false;
				}
				DTObject formDTO = new DTObject();
				formDTO.set("TRAN_CODE", transactionCode);
				formDTO.set("TRAN_ACCESS_CODE", alphaAccessCode);
				formDTO.set("PROGRAM_ID", context.getProcessID());
				formDTO.set(ContentManager.USER_ACTION, USAGE);
				formDTO = validateAlphaAccessCode(formDTO);
				if (formDTO.get(ContentManager.ERROR) != null) {
					getErrorMap().setError("alphaAccessCode", formDTO.get(ContentManager.ERROR), formDTO.getObjectL(ContentManager.ERROR_PARAM));
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("alphaAccessCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public DTObject validateAlphaAccessCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		MasterValidator validation = new MasterValidator();
		try {
			String primaryKey = inputDTO.get("TRAN_CODE");
			String dedupKey = inputDTO.get("TRAN_ACCESS_CODE");
			DTObject formDTO = new DTObject();
			formDTO.set(ContentManager.PROGRAM_ID, inputDTO.get("PROGRAM_ID"));
			formDTO.set(ContentManager.PURPOSE_ID, "TRAN_ACCESS_CODE");
			formDTO.set(ContentManager.PROGRAM_KEY, primaryKey);
			formDTO.set(ContentManager.DEDUP_KEY, dedupKey);
			resultDTO = validation.isDeDupAlreadyExist(formDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
				Object[] errorParam = { resultDTO.getObject("LINKED_TO_PK") };
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.PBS_TRAN_ACCESS_CODE_ALREDY_LIKED);
				resultDTO.setObject(ContentManager.ERROR_PARAM, errorParam);
				return resultDTO;
			}

		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	private boolean validateTransactionMode() {
		CommonValidator validation = new CommonValidator();
		try {
			if (!validation.isCMLOVREC("COMMON", "TRANMODE", transactionMode)) {
				getErrorMap().setError("transactionMode", BackOfficeErrorCodes.INVALID_OPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("transactionMode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	private boolean validateDepositWithdrawal() {
		CommonValidator validation = new CommonValidator();
		try {
			if (!validation.isCMLOVREC("COMMON", "TRANTYPE", depositWithdrawal)) {
				getErrorMap().setError("depositWithdrawal", BackOfficeErrorCodes.INVALID_OPTION);
				return false;
			}

			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("depositWithdrawal", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	private boolean validateTransactionUsingDocument() {
		CommonValidator validation = new CommonValidator();
		try {
			if (!validation.isCMLOVREC("COMMON", "TRANDOC", transactionUsingDocument)) {
				getErrorMap().setError("transactionUsingDocument", BackOfficeErrorCodes.INVALID_OPTION);
				return false;
			}
			if ((depositWithdrawal.equals("1")) && (transactionUsingDocument.equals("3"))) {
				getErrorMap().setError("transactionUsingDocument", BackOfficeErrorCodes.PBS_VALUE_NT_ALLOWED_FOR_WITHDRAWAL);
				return false;
			}
			if ((depositWithdrawal.equals("2")) && (!transactionUsingDocument.equals("3"))) {
				getErrorMap().setError("transactionUsingDocument", BackOfficeErrorCodes.PBS_VALUE_NT_ALLOWED_FOR_DEPOSIT);
				return false;
			}		
			
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("transactionUsingDocument", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	private boolean validateSlipNumberRequired() {
		try {
			if ((!transactionUsingDocument.equals("2")) && (decodeBooleanToString(slipNumberRequired).equals("1"))) {
				getErrorMap().setError("slipNumberRequired", BackOfficeErrorCodes.PBS_TRAN_USING_DOC_IS_NOT_WITHDRAWAL_SLIP);
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("slipNumberRequired", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	private boolean validateNumberOfLegsInTheTransaction() {
		CommonValidator validation = new CommonValidator();
		try {
			if (!validation.isCMLOVREC("COMMON", "TRANLEG", numberOfLegsInTheTransaction)) {
				getErrorMap().setError("numberOfLegsInTheTransaction", BackOfficeErrorCodes.INVALID_OPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("numberOfLegsInTheTransaction", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateRemarks() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (remarks != null && remarks.length() > 0) {
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			if (getAction().equals(MODIFY)) {
				if (commonValidator.isEmpty(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}
}
