package patterns.config.web.forms.cmn;
/**
 * The program will be used to create and maintain various geounitstructure codes
 * 
 * 
 * @version 1.0
 * @author Prashanth
 * @since 2-Dec-2014 <br>
 *        <b>Modified History</b> <BR>
 *        <U><B> Sl.No Modified Date Author Modified Changes Version </B></U> <br>
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 */
import java.sql.ResultSet;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.MasterValidator;
import patterns.config.web.forms.GenericFormBean;


public class mgeounitstrucbean extends GenericFormBean {

	private static final long serialVersionUID = -6219657363150466160L;

	private String geoUnitStructureCode;
	private String description;
	private String conciseDescription;
	private String parentgeoUnitStructureCode;
	private boolean lastHierarchy;
	private boolean dataPatternRequired;
	private String dataInputPattern;
	private boolean enabled;
	private boolean requireaUnitType;
	private String previoushier;

	public String getPrevioushier() {
		return previoushier;
	}

	public void setPrevioushier(String previoushier) {
		this.previoushier = previoushier;
	}

	public boolean isRequireaUnitType() {
		return requireaUnitType;
	}

	public void setRequireaUnitType(boolean requireaUnitType) {
		this.requireaUnitType = requireaUnitType;
	}

	private String remarks;

	public String getGeoUnitStructureCode() {
		return geoUnitStructureCode;
	}

	public void setGeoUnitStructureCode(String geoUnitStructureCode) {
		this.geoUnitStructureCode = geoUnitStructureCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getConciseDescription() {
		return conciseDescription;
	}

	public void setConciseDescription(String conciseDescription) {
		this.conciseDescription = conciseDescription;
	}

	public String getParentgeoUnitStructureCode() {
		return parentgeoUnitStructureCode;
	}

	public void setParentgeoUnitStructureCode(String parentgeoUnitStructureCode) {
		this.parentgeoUnitStructureCode = parentgeoUnitStructureCode;
	}

	public boolean isLastHierarchy() {
		return lastHierarchy;
	}

	public void setLastHierarchy(boolean lastHierarchy) {
		this.lastHierarchy = lastHierarchy;
	}

	public boolean isDataPatternRequired() {
		return dataPatternRequired;
	}

	public void setDataPatternRequired(boolean dataPatternRequired) {
		this.dataPatternRequired = dataPatternRequired;
	}

	public String getDataInputPattern() {
		return dataInputPattern;
	}

	public void setDataInputPattern(String dataInputPattern) {
		this.dataInputPattern = dataInputPattern;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public void reset() {
		geoUnitStructureCode = RegularConstants.EMPTY_STRING;
		description = RegularConstants.EMPTY_STRING;
		conciseDescription = RegularConstants.EMPTY_STRING;
		parentgeoUnitStructureCode = RegularConstants.EMPTY_STRING;
		lastHierarchy = false;
		requireaUnitType = false;
		dataPatternRequired = false;
		dataInputPattern = RegularConstants.EMPTY_STRING;
		enabled = false;
		remarks = RegularConstants.EMPTY_STRING;
		setProcessBO("patterns.config.framework.bo.cmn.mgeounitstrucBO");
	}

	@Override
	public void validate() {
		if (validateGeographicalUnitStructureCode()) {
			validateDescription();
			validateConciseDescription();
			validateParentgeoUnitStructureCode();
			validatelastHierarchy();
			validateDataInputPattern();
			validateRemarks();
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("GEO_UNIT_STRUC_CODE", geoUnitStructureCode);
				formDTO.set("DESCRIPTION", description);
				formDTO.set("CONCISE_DESCRIPTION", conciseDescription);
				formDTO.set("PARENT_GUS_CODE", parentgeoUnitStructureCode);
				formDTO.set("LAST_IN_HIERARCHY", decodeBooleanToString(lastHierarchy));
				formDTO.set("UNIT_TYPE_REQ", decodeBooleanToString(requireaUnitType));
				formDTO.set("DATA_PATTERN_REQ", decodeBooleanToString(dataPatternRequired));
				formDTO.set("DATA_PATTERN", dataInputPattern);
				if (getAction().equals(ADD)) {
					formDTO.set("ENABLED", decodeBooleanToString(true));
				} else {
					formDTO.set("ENABLED", decodeBooleanToString(enabled));
				}
				formDTO.set("REMARKS", remarks);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("geoUnitStructureCode", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		}

	}

	public boolean validatelastHierarchy() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("GEO_UNIT_CODE", geoUnitStructureCode);
			formDTO.set("ACTION", getAction());
			formDTO.set("PREV_LAST_IN_HIERARCHY", previoushier);
			formDTO.set("LAST_IN_HIERARCHY", decodeBooleanToString(lastHierarchy));
			if (lastHierarchy) {
				formDTO = validatelastHierarchy(formDTO);
				if (formDTO.get(ContentManager.ERROR) != null) {
					getErrorMap().setError("lastHierarchy", formDTO.get(ContentManager.ERROR));
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("lastHierarchy", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		}
		return false;
	}

	public boolean validateGeographicalUnitStructureCode() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("GEO_UNIT_STRUC_CODE", geoUnitStructureCode);
			formDTO.set("ACTION", getAction());
			formDTO = validateGeographicalUnitStructureCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("geoUnitStructureCode", formDTO.get(ContentManager.ERROR));
				return false;
			}

			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("geoUnitStructureCode", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateGeographicalUnitStructureCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		MasterValidator validation = new MasterValidator();
		String parentgeoUnitStructureCode = inputDTO.get("PARENT_GUS_CODE");

		String geoUnitStructureCode = inputDTO.get("GEO_UNIT_STRUC_CODE");
		String action = inputDTO.get("ACTION");
		try {
			if (validation.isEmpty(geoUnitStructureCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS,"DESCRIPTION");
			resultDTO = validation.validateGeographicalUnitStructureCode(inputDTO);
			if (action.equals(ADD)) {
				if (ContentManager.DATA_AVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_RECORD_EXISTS);
				}
				if (geoUnitStructureCode.equals(parentgeoUnitStructureCode)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_PGUS_LAST_HIER);
					return resultDTO;
				}

			} else {
				if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_RECORD_NOT_EXISTS);

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateDescription() {
		CommonValidator validation = new CommonValidator();
		try {
			if (!validation.isValidDescription(description)) {
				getErrorMap().setError("description", BackOfficeErrorCodes.HMS_INVALID_DESCRIPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("description", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateConciseDescription() {
		CommonValidator validation = new CommonValidator();
		try {
			if (!validation.isValidConciseDescription(conciseDescription)) {
				getErrorMap().setError("shortDescription", BackOfficeErrorCodes.HMS_INVALID_CONCISE_DESCRIPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("conciseDescription", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateParentgeoUnitStructureCode() {
		MasterValidator validation = new MasterValidator();

		try {
			if (!validation.isEmpty(parentgeoUnitStructureCode)) {
				DTObject formDTO = new DTObject();
				formDTO.set("PARENT_GUS_CODE", parentgeoUnitStructureCode);
				formDTO.set("GEO_UNIT_CODE", geoUnitStructureCode);

				formDTO.set("ACTION", getAction());
				formDTO = validateParentgeoUnitStructureCode(formDTO);
				if (formDTO.get(ContentManager.ERROR) != null) {
					getErrorMap().setError("parentgeoUnitStructureCode", formDTO.get(ContentManager.ERROR));
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("geoUnitStructureCode", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		}finally {
			validation.close();
		}
		return false;
	}

	public DTObject validateParentgeoUnitStructureCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		int count = 0;
		String tempGeoParentCode = null;
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		MasterValidator validation = new MasterValidator();
		String parentgeoUnitStructureCode = inputDTO.get("PARENT_GUS_CODE");
		String geoUnitStructureCode = inputDTO.get("GEO_UNIT_CODE");
		try {
			if (!validation.isEmpty(parentgeoUnitStructureCode)) {
				inputDTO.set("GEO_UNIT_STRUC_CODE", parentgeoUnitStructureCode);
				inputDTO.set(ContentManager.FETCH_COLUMNS, "GEO_UNIT_STRUC_CODE,DESCRIPTION,PARENT_GUS_CODE");
				resultDTO = validation.validateGeographicalUnitStructureCode(inputDTO);
				if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
					if (parentgeoUnitStructureCode.equals(geoUnitStructureCode)) {
						resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_PGUS_SAS_GUS);
						return resultDTO;
					}
					if ("1".equals(resultDTO.get("LAST_IN_HIERARCHY"))) {
						resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_PGUS_LAST_HIER);
						return resultDTO;
					}
					tempGeoParentCode = resultDTO.get("PARENT_GUS_CODE");
					if (tempGeoParentCode.equals(geoUnitStructureCode)) {
						resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_PGUS_SAS_GUSP);
						return resultDTO;
					}
					
					util.reset();
					util.setMode(DBUtil.PREPARED);
					util.setSql("SELECT COUNT(1)  FROM GEOUNITSTRUC WHERE PARENT_GUS_CODE =? AND GEO_UNIT_STRUC_CODE<>?");
					util.setString(1, parentgeoUnitStructureCode);
					util.setString(2, geoUnitStructureCode);
					ResultSet rs1 = util.executeQuery();
					if (rs1.next()) {
						count = rs1.getInt(1);
					}
					if (count == 1) {
						resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_PGUS_NAVL);
						return resultDTO;
					}
					
				} else {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_RECORD_NOT_EXISTS);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			dbContext.close();
			validation.close();
		}
		return resultDTO;
	}

	public DTObject validatelastHierarchy(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		DBContext dbContext = new DBContext();
		String geoUnitStructureCode = inputDTO.get("GEO_UNIT_STRUC_CODE");

		MasterValidator validation = new MasterValidator();

		try {
			if (RegularConstants.COLUMN_DISABLE.equals(inputDTO.get("PREV_LAST_IN_HIERARCHY")) && inputDTO.get("LAST_IN_HIERARCHY").equals(RegularConstants.COLUMN_ENABLE)) {
				inputDTO.set(ContentManager.COLUMN, "PARENT_GUS_CODE");
				inputDTO.set(ContentManager.CODE, geoUnitStructureCode);
				inputDTO.set(ContentManager.TABLE, "GEOUNITSTRUC");
				inputDTO.set(ContentManager.FETCH_COLUMNS, "LAST_IN_HIERARCHY");
				resultDTO = validation.validateGenericCode(inputDTO);
				if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_PGUS_INUSE);
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			dbContext.close();
			validation.close();
		}
		return resultDTO;

	}

	public boolean validateDataInputPattern() {
		CommonValidator validation = new CommonValidator();
		try {
			if (dataPatternRequired) {

				if (validation.isEmpty(dataInputPattern)) {
					getErrorMap().setError("dataInputPattern", BackOfficeErrorCodes.HMS_FIELD_BLANK);
					return false;
				}
				if (!validation.isValidDescription(dataInputPattern)) {
					getErrorMap().setError("dataInputPattern", BackOfficeErrorCodes.HMS_INVALID_DESCRIPTION);
					return false;
				}
			}
			return true;

		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("dataInputPattern", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateRemarks() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (remarks != null && remarks.length() > 0) {
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.HMS_INVALID_REMARKS);
					return false;
				}
			}
			if (getAction().equals(MODIFY)) {
				if (commonValidator.isEmpty(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.HMS_FIELD_BLANK);
					return false;
				}
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.HMS_INVALID_REMARKS);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}

}