package patterns.config.web.forms.cmn;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.process.TBAActionType;
import patterns.config.framework.service.DTDObject;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.AccessValidator;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.MasterValidator;
import patterns.config.web.forms.GenericFormBean;

public class euserregionallocbean extends GenericFormBean {

	private static final long serialVersionUID = 1L;
	private String fieldUser;
	private String effectiveDate;
	private String geoRegionCode;
	private String xmlUserRegAllocGrid;
	private boolean enabled;
	private String remarks;
	private String pincode;	
	private String officecode;
    private String fieldUserType;
    
	public String getFieldUserType() {
		return fieldUserType;
	}

	public void setFieldUserType(String fieldUserType) {
		this.fieldUserType = fieldUserType;
	}

	public String getPincode() {
		return pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public String getOfficecode() {
		return officecode;
	}

	public void setOfficecode(String officecode) {
		this.officecode = officecode;
	}

	public String getXmlUserRegAllocGrid() {
		return xmlUserRegAllocGrid;
	}

	public void setXmlUserRegAllocGrid(String xmlUserRegAllocGrid) {
		this.xmlUserRegAllocGrid = xmlUserRegAllocGrid;
	}

	public String getFieldUser() {
		return fieldUser;
	}

	public void setFieldUser(String fieldUser) {
		this.fieldUser = fieldUser;
	}

	public String getGeoRegionCode() {
		return geoRegionCode;
	}

	public void setGeoRegionCode(String geoRegionCode) {
		this.geoRegionCode = geoRegionCode;
	}

	public String getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public void reset() {
		// TODO Auto-generated method stub
		fieldUser = RegularConstants.EMPTY_STRING;
		effectiveDate = RegularConstants.EMPTY_STRING;
		geoRegionCode = RegularConstants.EMPTY_STRING;
		enabled = false;
		remarks = RegularConstants.EMPTY_STRING;
		xmlUserRegAllocGrid = RegularConstants.EMPTY_STRING;
		pincode = RegularConstants.EMPTY_STRING;
		officecode = RegularConstants.EMPTY_STRING;
		setProcessBO("patterns.config.framework.bo.cmn.euserregionallocBO");
	}

	@Override
	public void validate() {
		// TODO Auto-generated method stub
		if (validateFieldUser() && validateEffectiveDate()) {
			validateGrid();
			validateRemarks();
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.set("FIELD_USER_ID", fieldUser);
				formDTO.setObject("EFF_DATE", BackOfficeFormatUtils.getDate(effectiveDate, context.getDateFormat()));
				if (getAction().equals(ADD))
					formDTO.set("ENABLED", decodeBooleanToString(true));
				else
					formDTO.set("ENABLED", decodeBooleanToString(enabled));
				formDTO.set("REMARKS", remarks);

				DTDObject dtdObject = new DTDObject();
				dtdObject.addColumn(0, "SL");
				dtdObject.addColumn(1, "GEO_REGION_CODE");
				dtdObject.addColumn(2, "DESCRIPTION");
				dtdObject.addColumn(3, "PINCODE");
				dtdObject.addColumn(4, "OFFICE_NUMBER");
				dtdObject.setXML(xmlUserRegAllocGrid);
				formDTO.setDTDObject("USERREGIONALLOCHISTDTL", dtdObject);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("fieldUser", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
	}

	public boolean validateFieldUser() {
		CommonValidator validator = new CommonValidator();
		DTObject formDTO = new DTObject();
		try {
			if (validator.isEmpty(fieldUser)) {
				getErrorMap().setError("fieldUser", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			formDTO.set("USER_ID", fieldUser);
			formDTO.set("ACTION", getAction());
			formDTO = validateFieldUser(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("fieldUser", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("fieldUser", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return false;
	}

	public DTObject validateFieldUser(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);

		AccessValidator validation = new AccessValidator();
		try {
			inputDTO.set(ContentManager.FETCH, ContentManager.FETCH_ALL);
			resultDTO = validation.validateInternalUser(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_USERID);
				return resultDTO;
			}
			String thirdPartyOrgCode = resultDTO.get("THIRD_PARTY_ORG_CODE");
			String empID = resultDTO.get("EMP_CODE");
			String userName = resultDTO.get("USER_NAME");
			inputDTO.set(ContentManager.CODE, resultDTO.get("ROLE_CODE"));
			resultDTO = validation.validateInternalRole(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
			if (resultDTO.get("FIELD_ROLE").equals(RegularConstants.COLUMN_DISABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.PBS_FIELD_ROLE_USER);
				return resultDTO;
			}
			
			if (!thirdPartyOrgCode.equals(RegularConstants.EMPTY_STRING)) {
				resultDTO.set("USER_TYPE", RegularConstants.ONE);
			} else if (!empID.equals(RegularConstants.EMPTY_STRING)) {
				resultDTO.set("USER_TYPE", RegularConstants.ZERO);
			}
			resultDTO.set("USER_NAME", userName);
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateEffectiveDate() {
		DTObject formDTO = new DTObject();
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(effectiveDate)) {
				getErrorMap().setError("effectiveDate", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			formDTO.set("USER_ID", fieldUser);
			formDTO.set("EFF_DATE", effectiveDate);
			formDTO.set("ACTION", getAction());
			formDTO = validateEffectiveDate(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("effectiveDate", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("effectiveDate", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public DTObject validateEffectiveDate(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		CommonValidator commonvalidation = new CommonValidator();
		String filedUserID = inputDTO.get("USER_ID");
		String date = inputDTO.get("EFF_DATE");
		String action = inputDTO.get("ACTION");
		try {
			TBAActionType AT;
			if (action.equals(ADD))
				AT = TBAActionType.ADD;
			else
				AT = TBAActionType.MODIFY;
			if (!commonvalidation.isEffectiveDateValid(date, AT)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_EFFECTIVE_DATE);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "ENABLED");
			inputDTO.set(ContentManager.TABLE, "USERREGIONALLOCHIST");
			String columns[] = new String[] { context.getEntityCode(), filedUserID, date };
			inputDTO.setObject(ContentManager.PROGRAM_KEY, columns);
			resultDTO = commonvalidation.validatePK(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (action.equals(ADD)) {
				if (ContentManager.DATA_AVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_EXISTS);
				}
			} else if (action.equals(MODIFY)) {
				if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_NOT_EXISTS);
				} else if (resultDTO.get("ENABLED").equals(RegularConstants.ZERO)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.CODE_CLOSED);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonvalidation.close();
		}
		return resultDTO;
	}

	private boolean validateGrid() {
		AccessValidator validation = new AccessValidator();
		try {
			DTDObject dtdObject = new DTDObject();
			dtdObject.addColumn(0, "SL");
			dtdObject.addColumn(1, "GEO_REGION_CODE");
			dtdObject.addColumn(2, "DESCRIPTION");
			dtdObject.addColumn(3, "PINCODE");
			dtdObject.addColumn(4, "OFFICE_NUMBER");
			dtdObject.setXML(xmlUserRegAllocGrid);
			if (dtdObject.getRowCount() <= 0) {
				getErrorMap().setError("geoRegionCode", BackOfficeErrorCodes.ATLEAST_ONE_ROW);
				return false;
			}
			if (!validation.checkDuplicateRows(dtdObject, 1)) {
				getErrorMap().setError("geoRegionCode", BackOfficeErrorCodes.DUPLICATE_DATA);
				return false;
			}
			for (int i = 0; i < dtdObject.getRowCount(); i++) {
				if (!validateGeoRegionCode(dtdObject.getValue(i, 1))) {
					getErrorMap().setError("geoRegionCode", BackOfficeErrorCodes.INVALID_ACTION);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("geoRegionCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}

		return true;
	}

	public boolean validateGeoRegionCode(String geoRegionCode) {
		try {
			DTObject formDTO = getFormDTO();
			formDTO.set("GEO_REGION_CODE", geoRegionCode);
			formDTO.set("ACTION", USAGE);
			formDTO = validateGeoRegionCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("geoRegionCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("geoRegionCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateGeoRegionCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		MasterValidator validation = new MasterValidator();
		String geoRegionCode = inputDTO.get("GEO_REGION_CODE");
		try {
			if (validation.isEmpty(geoRegionCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION,PINCODE_GEOUNITID,OFFICE_CODE");
			resultDTO = validation.validateGeoRegionCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}

		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateRemarks() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (remarks != null && remarks.length() > 0) {
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			if (getAction().equals(MODIFY)) {
				if (commonValidator.isEmpty(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}

}