package patterns.config.web.forms.cmn;

import java.util.ArrayList;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.GenericOption;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.MasterValidator;
import patterns.config.web.forms.GenericFormBean;

public class massetfieldsbean extends GenericFormBean {

	private static final long serialVersionUID = -6219657363150466160L;
	private String fieldId;
	private String name;
	private String shortName;
	private String fieldFor;
	private String fieldValueType;
	private String fieldSize;
	private String noOfFractionDigits;
	private String fieldDedicatedToCustomerId;
	private String fieldAssetType;
	private boolean enabled;
	private String remarks;

	public String getFieldId() {
		return fieldId;
	}

	public void setFieldId(String fieldId) {
		this.fieldId = fieldId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getShortName() {
		return shortName;
	}

	public String getFieldFor() {
		return fieldFor;
	}

	public void setFieldFor(String fieldFor) {
		this.fieldFor = fieldFor;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public String getFieldValueType() {
		return fieldValueType;
	}

	public void setFieldValueType(String fieldValueType) {
		this.fieldValueType = fieldValueType;
	}

	public String getFieldSize() {
		return fieldSize;
	}

	public void setFieldSize(String fieldSize) {
		this.fieldSize = fieldSize;
	}

	public String getNoOfFractionDigits() {
		return noOfFractionDigits;
	}

	public void setNoOfFractionDigits(String noOfFractionDigits) {
		this.noOfFractionDigits = noOfFractionDigits;
	}

	public String getFieldDedicatedToCustomerId() {
		return fieldDedicatedToCustomerId;
	}

	public void setFieldDedicatedToCustomerId(String fieldDedicatedToCustomerId) {
		this.fieldDedicatedToCustomerId = fieldDedicatedToCustomerId;
	}

	public String getFieldAssetType() {
		return fieldAssetType;
	}

	public void setFieldAssetType(String fieldAssetType) {
		this.fieldAssetType = fieldAssetType;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public void reset() {
		fieldId = RegularConstants.EMPTY_STRING;
		name = RegularConstants.EMPTY_STRING;
		shortName = RegularConstants.EMPTY_STRING;
		fieldFor = RegularConstants.EMPTY_STRING;
		fieldValueType = RegularConstants.EMPTY_STRING;
		fieldSize = RegularConstants.EMPTY_STRING;
		noOfFractionDigits = RegularConstants.EMPTY_STRING;
		fieldDedicatedToCustomerId = RegularConstants.EMPTY_STRING;
		fieldAssetType = RegularConstants.EMPTY_STRING;
		enabled = false;
		remarks = RegularConstants.EMPTY_STRING;
		DBContext dbContext = new DBContext();
		ArrayList<GenericOption> fieldFor = null;
		fieldFor = getGenericOptions("MASSETFIELDS", "FIELD_FOR", dbContext, RegularConstants.NULL);
		webContext.getRequest().setAttribute("MASSETFIELDS_FIELD_FOR", fieldFor);
		ArrayList<GenericOption> fieldValueType = null;
		fieldValueType = getGenericOptions("MASSETFIELDS", "FIELD_VALUE_TYPE", dbContext, RegularConstants.NULL);
		webContext.getRequest().setAttribute("MASSETFIELDS_FIELD_VALUE_TYPE", fieldValueType);
		dbContext.close();
		setProcessBO("patterns.config.framework.bo.cmn.massetfieldsBO");
	}

	@Override
	public void validate() {
		if (validateFieldId()) {
			validateName();
			validateShortName();
			validateFieldFor();
			validateFieldValueType();
			validateFieldSize();
			validateNoOfFractionDigits();
			validateFieldDedicatedToCustomerId();
			validateFieldAssetType();
			validateRemarks();
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.set("FIELD_ID", fieldId);
				formDTO.set("DESCRIPTION", name);
				formDTO.set("CONCISE_DESCRIPTION", shortName);
				formDTO.set("FIELD_FOR", fieldFor);
				formDTO.set("FIELD_VALUE_TYPE", fieldValueType);
				formDTO.set("FIELD_SIZE", fieldSize);
				if (fieldFor.equals("L"))
					formDTO.set("ASSET_TYPE_CODE", RegularConstants.NULL);
				else
					formDTO.set("ASSET_TYPE_CODE", fieldAssetType);
				if (fieldValueType.equals("A") || fieldValueType.equals("F"))
					formDTO.set("FIELD_DECIMALS", noOfFractionDigits);
				else
					formDTO.set("FIELD_DECIMALS", RegularConstants.NULL);
				formDTO.set("CUSTOMER_ID", fieldDedicatedToCustomerId);
				if (fieldDedicatedToCustomerId.isEmpty())
					formDTO.set("CUSTOMER_ID", RegularConstants.NULL);
				else
					formDTO.set("CUSTOMER_ID", fieldDedicatedToCustomerId);

				if (fieldAssetType.isEmpty())
					formDTO.set("ASSET_TYPE_CODE", RegularConstants.NULL);
				else
					formDTO.set("ASSET_TYPE_CODE", fieldAssetType);

				if (getAction().equals(ADD)) {
					formDTO.set("ENABLED", decodeBooleanToString(true));
				} else {
					formDTO.set("ENABLED", decodeBooleanToString(enabled));
				}
				formDTO.set("REMARKS", remarks);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("fieldId", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
	}

	public boolean validateFieldId() {
		try {

			DTObject formDTO = getFormDTO();
			formDTO.set("FIELD_ID", fieldId);
			formDTO.set(ContentManager.USER_ACTION, getAction());
			formDTO = validateFieldId(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("fieldId", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("fieldId", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateFieldId(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		MasterValidator validation = new MasterValidator();
		String fieldId = inputDTO.get("FIELD_ID");
		String action = inputDTO.get(ContentManager.USER_ACTION);
		try {
			if (validation.isEmpty(fieldId)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
			resultDTO = validation.validateFieldId(inputDTO);
			if (action.equals(ADD)) {
				if (ContentManager.DATA_AVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_EXISTS);
					return resultDTO;
				}
			} else {
				if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_NOT_EXISTS);
					return resultDTO;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateName() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(name)) {
				getErrorMap().setError("name", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidDescription(name)) {
				getErrorMap().setError("name", BackOfficeErrorCodes.INVALID_DESCRIPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("name", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateShortName() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(shortName)) {
				getErrorMap().setError("shortName", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidConciseDescription(shortName)) {
				getErrorMap().setError("shortName", BackOfficeErrorCodes.INVALID_DESCRIPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("shortName", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateFieldFor() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(fieldFor)) {
				getErrorMap().setError("fieldFor", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isCMLOVREC("MASSETFIELDS", "FIELD_FOR", fieldFor)) {
				getErrorMap().setError("fieldFor", BackOfficeErrorCodes.INVALID_OPTION);
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("fieldFor", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return true;
	}
	
	public boolean validateFieldValueType() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(fieldValueType)) {
				getErrorMap().setError("fieldValueType", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isCMLOVREC("MASSETFIELDS", "FIELD_VALUE_TYPE", fieldValueType)) {
				getErrorMap().setError("fieldValueType", BackOfficeErrorCodes.INVALID_OPTION);
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("fieldValueType", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return true;
	}

	public boolean validateFieldSize() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(fieldSize)) {
				getErrorMap().setError("fieldSize", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidDecimal(fieldSize)) {
				getErrorMap().setError("fieldSize", BackOfficeErrorCodes.INVALID_NUMBER);
				return false;
			}
			if (validation.isValidNegativeNumber(fieldSize)) {
				getErrorMap().setError("fieldSize", BackOfficeErrorCodes.POSITIVE_CHECK);
				return false;
			}
			if (fieldValueType.equals("T")) {
				if (validation.isNumberGreater(fieldSize, "250")) {
					getErrorMap().setError("fieldSize", BackOfficeErrorCodes.VALUE_MORE_250);
					return false;
				}
			}
			if (!fieldValueType.equals("T")) {
				if (validation.isNumberGreater(fieldSize, "35")) {
					getErrorMap().setError("fieldSize", BackOfficeErrorCodes.VALUE_MORE_35);
					return false;
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("fieldSize", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return true;
	}

	public boolean validateNoOfFractionDigits() {
		CommonValidator validation = new CommonValidator();
		try {
			if (fieldValueType.equals("A") || fieldValueType.equals("F")) {

				if (validation.isEmpty(noOfFractionDigits)) {
					getErrorMap().setError("noOfFractionDigits", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!validation.isValidDecimal(noOfFractionDigits)) {
					getErrorMap().setError("noOfFractionDigits", BackOfficeErrorCodes.INVALID_NUMBER);
					return false;
				}
				if (validation.isValidNegativeNumber(noOfFractionDigits)) {
					getErrorMap().setError("noOfFractionDigits", BackOfficeErrorCodes.POSITIVE_CHECK);
					return false;
				}
				if (validation.isNumberGreater(noOfFractionDigits, "30")) {
					getErrorMap().setError("noOfFractionDigits", BackOfficeErrorCodes.VALUE_MORE_30);
					return false;
				}

			} else {
				if (!validation.isEmpty(noOfFractionDigits)) {
					getErrorMap().setError("noOfFractionDigits", BackOfficeErrorCodes.VALUE_NOT_ALLOWED);
					return false;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("noOfFractionDigits", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return true;
	}

	public boolean validateFieldDedicatedToCustomerId() {
		try {

			DTObject formDTO = getFormDTO();
			formDTO.set("CUSTOMER_ID", fieldDedicatedToCustomerId);
			formDTO.set(ContentManager.USER_ACTION, getAction());
			formDTO = validateFieldDedicatedToCustomerId(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("fieldDedicatedToCustomerId", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("fieldDedicatedToCustomerId", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateFieldDedicatedToCustomerId(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		MasterValidator validation = new MasterValidator();
		String fieldDedicatedToCustomerId = inputDTO.get("CUSTOMER_ID");
		try {
			if (!validation.isEmpty(fieldDedicatedToCustomerId)) {
				if (validation.isEmpty(fieldDedicatedToCustomerId)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
					return resultDTO;
				}
				inputDTO.set(ContentManager.FETCH_COLUMNS, "CUSTOMER_NAME");
				resultDTO = validation.valildateCustomerID(inputDTO);
				if (resultDTO.get(ContentManager.ERROR) != null) {
					resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
					return resultDTO;
				}
				if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
					return resultDTO;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateFieldAssetType() {
		try {

			DTObject formDTO = getFormDTO();
			formDTO.set("ASSET_TYPE_CODE", fieldAssetType);
			
			formDTO.set("FIELD_FOR", fieldFor);
			formDTO.set(ContentManager.USER_ACTION, USAGE);
			formDTO = validateFieldAssetType(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("fieldAssetType", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("fieldAssetType", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateFieldAssetType(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		MasterValidator validation = new MasterValidator();
		String fieldAssetType = inputDTO.get("ASSET_TYPE_CODE");
		String fieldFor = inputDTO.get("FIELD_FOR");
		try {
			if (fieldFor.equals("A")) {
			if (!validation.isEmpty(fieldAssetType)) {
				if (validation.isEmpty(fieldAssetType)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
					return resultDTO;
				}
				inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
				resultDTO = validation.validateAssetType(inputDTO);
				if (resultDTO.get(ContentManager.ERROR) != null) {
					resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
					return resultDTO;
				}
				if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
					return resultDTO;
				}
			}
		} else if (fieldFor.equals("L") && !validation.isEmpty(fieldAssetType)) {
			getErrorMap().setError("fieldAssetType", BackOfficeErrorCodes.VALUE_NOT_ALLOWED);
			return resultDTO;
		}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateRemarks() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (remarks != null && remarks.length() > 0) {
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			if (getAction().equals(MODIFY)) {
				if (commonValidator.isEmpty(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}
}
