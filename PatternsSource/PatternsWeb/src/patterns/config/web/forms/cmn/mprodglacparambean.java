package patterns.config.web.forms.cmn;

import java.sql.ResultSet;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.service.DTDObject;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.DHTMLXGridUtility;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.FASValidator;
import patterns.config.validations.MasterValidator;
import patterns.config.web.forms.GenericFormBean;

public class mprodglacparambean extends GenericFormBean {

	private static final long serialVersionUID = 1L;
	private String productCode;
	private String currencyCode;
	private String productGlHeadCode;
	private String statusCode;
	private String glHeadCode;
	private String remarks;
	private String w_loans;
	private String w_deposits;
	private String w_casa;

	public String getW_loans() {
		return w_loans;
	}

	public void setW_loans(String w_loans) {
		this.w_loans = w_loans;
	}

	public String getW_deposits() {
		return w_deposits;
	}

	public void setW_deposits(String w_deposits) {
		this.w_deposits = w_deposits;
	}

	public String getW_casa() {
		return w_casa;
	}

	public void setW_casa(String w_casa) {
		this.w_casa = w_casa;
	}

	private String xmlProductGrid;

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public String getProductGlHeadCode() {
		return productGlHeadCode;
	}

	public void setProductGlHeadCode(String productGlHeadCode) {
		this.productGlHeadCode = productGlHeadCode;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getGlHeadCode() {
		return glHeadCode;
	}

	public void setGlHeadCode(String glHeadCode) {
		this.glHeadCode = glHeadCode;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getXmlProductGrid() {
		return xmlProductGrid;
	}

	public void setXmlProductGrid(String xmlProductGrid) {
		this.xmlProductGrid = xmlProductGrid;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public void reset() {
		productCode = RegularConstants.EMPTY_STRING;
		currencyCode = RegularConstants.EMPTY_STRING;
		productGlHeadCode = RegularConstants.EMPTY_STRING;
		statusCode = RegularConstants.EMPTY_STRING;
		glHeadCode = RegularConstants.EMPTY_STRING;
		remarks = RegularConstants.EMPTY_STRING;
		xmlProductGrid = RegularConstants.EMPTY_STRING;
		setProcessBO("patterns.config.framework.bo.cmn.mprodglacparamBO");

	}

	@Override
	public void validate() {
		if (validateProductCode()) {
			validateCurrencyCode();
			validateGlHeadCode();
			validateGrid();
			validateRemarks();
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.set("PRODUCT_CODE", productCode);
				formDTO.set("CCY_CODE", currencyCode);
				formDTO.set("PRODUCT_GL_HEAD_CODE", productGlHeadCode);
				formDTO.set("REMARKS", remarks);

				DTDObject dtdObject = new DTDObject();
				dtdObject.addColumn(0, "SL");
				dtdObject.addColumn(1, "LINK");
				dtdObject.addColumn(2, "ACNT_STATUS_CODE");
				dtdObject.addColumn(3, "GL_HEAD_CODE");
				dtdObject.addColumn(4, "DESCRIPTION");
				dtdObject.setXML(xmlProductGrid);
				formDTO.setDTDObject("PRODGLACPARAMDTL", dtdObject);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("productCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
	}

	public boolean validateProductCode() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("PRODUCT_CODE", productCode);
			formDTO.set(ContentManager.ACTION, USAGE);
			formDTO = validateProductCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("productCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("productCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateProductCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		MasterValidator validation = new MasterValidator();
		String productCode = inputDTO.get("PRODUCT_CODE");
		try {
			if (validation.isEmpty(productCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "PRODUCT_CODE,PRODUCT_GROUP_CODE,DESCRIPTION");
			resultDTO = validation.validateProductCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
			String prdctCode = resultDTO.get("PRODUCT_CODE");
			String desc = resultDTO.get("DESCRIPTION");
			String productgroupCode = inputDTO.get("PRODUCT_GROUP_CODE");
			productgroupCode = resultDTO.get("PRODUCT_GROUP_CODE");
			inputDTO.set(ContentManager.FETCH_COLUMNS, "GROUP_TYPE");
			inputDTO.set("PRODUCT_GROUP_CODE", productgroupCode);
			resultDTO = validation.validateProductGroupCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}

			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
			if (resultDTO.get("GROUP_TYPE").equals("CS") || resultDTO.get("GROUP_TYPE").equals("TD") || resultDTO.get("GROUP_TYPE").equals("LA")) {
				setGroupType(resultDTO.get("GROUP_TYPE"));
				resultDTO.set("W_LOAN", w_loans);
				resultDTO.set("W_DEPOSIT", w_deposits);
				resultDTO.set("W_CASA", w_casa);
			} else {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			}
			resultDTO.set("PRODUCT_CODE", prdctCode);
			resultDTO.set("DESCRIPTION", desc);
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public void setGroupType(String groupType) {
		if (groupType.equals("CS")) {
			w_loans = "0";
			w_deposits = "0";
			w_casa = "1";
		} else if (groupType.equals("LA")) {
			w_loans = "0";
			w_deposits = "1";
			w_casa = "0";
		} else if (groupType.equals("TD")) {
			w_loans = "1";
			w_deposits = "0";
			w_casa = "0";
		}
	}

	public boolean validateCurrencyCode() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("CCY_CODE", currencyCode);
			formDTO.set("PRODUCT_CODE", productCode);
			formDTO.set(ContentManager.ACTION, USAGE);
			formDTO = validatecurrencyCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("currencyCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("currencyCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validatecurrencyCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		MasterValidator validation = new MasterValidator();
		String productCode = inputDTO.get("PRODUCT_CODE");
		String currencyCode = inputDTO.get("CCY_CODE");
		try {
			if (validation.isEmpty(currencyCode) && validation.isEmpty(productCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}

			inputDTO.set(ContentManager.FETCH_COLUMNS, "CCY_CODE,DESCRIPTION");
			resultDTO = validation.validateCurrency(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
			String sqlQuery = "SELECT COUNT(1) FROM PRODCURRDTL WHERE ENTITY_CODE=? AND PRODUCT_CODE=? AND CURR_CODE=?";
			util.reset();
			util.setSql(sqlQuery);
			util.setLong(1, Long.parseLong(context.getEntityCode()));
			util.setString(2, inputDTO.get("PRODUCT_CODE"));
			util.setString(3, inputDTO.get("CCY_CODE"));
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				if (rset.getInt(1) == 0) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.CCY_PRODUCT_NA);
					return resultDTO;
				}
			}

			sqlQuery = "SELECT ACNT_STATUS_CODE,'' AS GLHEAD,'' AS GLDESCRIPTION,DESCRIPTION FROM ACNTSTATUS WHERE ENTITY_CODE=? AND GL_CHG_REQ_STATUS=? AND ENABLED=?";
			util.reset();
			util.setSql(sqlQuery);
			util.setLong(1, Long.parseLong(context.getEntityCode()));
			util.setString(2, RegularConstants.ONE);
			util.setString(3, RegularConstants.ONE);
			rset = util.executeQuery();
			DHTMLXGridUtility dhtmlUtility = new DHTMLXGridUtility();
			dhtmlUtility.init();
			while (rset.next()) {
				dhtmlUtility.startRow(rset.getString("ACNT_STATUS_CODE"));
				dhtmlUtility.setCell("Edit^javascript:EditGrid(\"" + rset.getString("ACNT_STATUS_CODE") + "\")");
				dhtmlUtility.setCell(rset.getString("ACNT_STATUS_CODE"));
				dhtmlUtility.setCell("");
				dhtmlUtility.setCell("");
				dhtmlUtility.setCell(rset.getString("DESCRIPTION"));
				dhtmlUtility.endRow();
			}
			dhtmlUtility.finish();
			resultDTO.set(ContentManager.RESULT_XML, dhtmlUtility.getXML());
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
			dbContext.close();
			util.reset();
		}
		return resultDTO;
	}

	public boolean validateGlHeadCode() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("PRODUCT_CODE", productCode);
			formDTO.set("GL_HEAD_CODE", productGlHeadCode);
			formDTO.set("CCY_CODE", currencyCode);
			formDTO.set("W_CASA", w_casa);
			formDTO.set("W_DEPOSIT", w_deposits);
			formDTO.set("W_LOAN", w_loans);
			formDTO.set(ContentManager.USER_ACTION, USAGE);
			formDTO = validateProductGlHeadCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("productGlHeadCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("productGlHeadCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateProductGlHeadCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		DTObject resultDTO1 = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		FASValidator validation = new FASValidator();
		String productCode = inputDTO.get("PRODUCT_CODE");
		String productGlHeadCode = inputDTO.get("GL_HEAD_CODE");
		String ccyCode = inputDTO.get("CCY_CODE");
		w_casa = inputDTO.get("W_CASA");
		w_deposits = inputDTO.get("W_DEPOSIT");
		w_loans = inputDTO.get("W_LOAN");
		String action = inputDTO.get(ContentManager.ACTION);
		try {
			if (validation.isEmpty(productGlHeadCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "GL_HEAD_CODE,DESCRIPTION,GL_CURR_CODE,POSTING_HEAD");
			inputDTO.set(ContentManager.USER_ACTION, USAGE);
			resultDTO = validation.validateGLHead(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
			if (!resultDTO.get("GL_CURR_CODE").equals(inputDTO.get("CCY_CODE"))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.GL_CCY_NM);
				return resultDTO;
			}
			if (!resultDTO.get("POSTING_HEAD").equals(RegularConstants.ONE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.PBS_POST_GL_ALLOWED);
				return resultDTO;
			}
			inputDTO.set("ROOT_GL_HEAD", inputDTO.get("GL_HEAD_CODE"));
			resultDTO1 = validateGlMastRootAttribute(inputDTO);
			if (resultDTO1.get(ContentManager.ERROR) != null && resultDTO1.get(ContentManager.ERROR) != RegularConstants.EMPTY_STRING) {
				resultDTO1.set(ContentManager.ERROR, resultDTO1.get(ContentManager.ERROR));
				return resultDTO1;
			}
			if (resultDTO1.get("BANK_AC_GL").equals(RegularConstants.ONE)) {
				resultDTO1.set(ContentManager.ERROR, BackOfficeErrorCodes.PBS_BANK_GL_NA);
				return resultDTO1;
			}
			if (resultDTO1.get("CASH_GL").equals(RegularConstants.ONE)) {
				resultDTO1.set(ContentManager.ERROR, BackOfficeErrorCodes.PBS_POST_GL_ALLOWED);
				return resultDTO1;
			}
			if (w_casa.equals(RegularConstants.ONE) && !resultDTO1.get("GL_ACCOUNTING_TYPE").equals("2")) {
				resultDTO1.set(ContentManager.ERROR, BackOfficeErrorCodes.PG_NA_GLTYPE);
				return resultDTO1;
			}
			if (w_deposits.equals(RegularConstants.ONE) && !resultDTO1.get("GL_ACCOUNTING_TYPE").equals("2")) {
				resultDTO1.set(ContentManager.ERROR, BackOfficeErrorCodes.PG_NA_GLTYPE);
				return resultDTO1;
			}
			if (w_loans.equals(RegularConstants.ONE) && !resultDTO1.get("GL_ACCOUNTING_TYPE").equals(RegularConstants.ONE)) {
				resultDTO1.set(ContentManager.ERROR, BackOfficeErrorCodes.PG_NA_GLTYPE);
				return resultDTO1;
			}

			inputDTO.set(ContentManager.FETCH_COLUMNS, "PRODUCT_CODE");
			inputDTO.set(ContentManager.TABLE, "PRODGLACPARAM");
			String columns[] = new String[] { context.getEntityCode(), productCode, ccyCode, productGlHeadCode };
			inputDTO.setObject(ContentManager.PROGRAM_KEY, columns);
			inputDTO = validation.validatePK(inputDTO);
			if (inputDTO.get(ContentManager.ERROR) != null) {
				inputDTO.set(ContentManager.ERROR, inputDTO.get(ContentManager.ERROR));
				return inputDTO;
			}
			if (action.equals(ADD)) {
				if (ContentManager.DATA_AVAILABLE.equals(inputDTO.get(ContentManager.RESULT))) {
					inputDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_EXISTS);
					return inputDTO;
				}
			} else if (action.equals(MODIFY)) {
				if (ContentManager.DATA_UNAVAILABLE.equals(inputDTO.get(ContentManager.RESULT))) {
					inputDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_NOT_EXISTS);
					return inputDTO;
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public DTObject validateGlMastRootAttribute(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		try {
			String sqlQuery = "SELECT BANK_AC_GL,CASH_GL,GL_ACCOUNTING_TYPE FROM GLMASTROOTATTR WHERE ENTITY_CODE=? AND ROOT_GL_HEAD=?";
			util.reset();
			util.setSql(sqlQuery);
			util.setLong(1, Long.parseLong(context.getEntityCode()));
			util.setString(2, inputDTO.get("ROOT_GL_HEAD"));
			ResultSet rset = util.executeQuery();

			if (rset.next()) {
				resultDTO.set("BANK_AC_GL", rset.getString("BANK_AC_GL"));
				resultDTO.set("CASH_GL", rset.getString("CASH_GL"));
				resultDTO.set("GL_ACCOUNTING_TYPE", rset.getString("GL_ACCOUNTING_TYPE"));
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
			} else {
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			dbContext.close();
			util.reset();
		}
		return resultDTO;
	}

	public DTObject validateGlHeadCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		FASValidator validation = new FASValidator();
		String glHeadCode = inputDTO.get("GL_HEAD_CODE");
		try {
			if (validation.isEmpty(glHeadCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH, "GL_HEAD_CODE");
			resultDTO = validation.validateGLHead(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}

		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	private boolean validateGrid() {
		CommonValidator validation = new CommonValidator();
		try {
			DTDObject dtdObject = new DTDObject();
			dtdObject.addColumn(0, "SL");
			dtdObject.addColumn(1, "STATUS_CODE");
			dtdObject.addColumn(2, "GL_HEAD_CODE");
			dtdObject.addColumn(3, "DESCRIPTION");		
			dtdObject.addColumn(4, "DESCRIPTION");
			dtdObject.setXML(xmlProductGrid);
			if (dtdObject.getRowCount() <= 0) {
				getErrorMap().setError("statusCode", BackOfficeErrorCodes.ATLEAST_ONE_ROW);
				return false;
			}
			if (!validation.checkDuplicateRows(dtdObject, 1)) {
				getErrorMap().setError("statusCode", BackOfficeErrorCodes.DUPLICATE_DATA);
				return false;
			}
			for (int i = 0; i < dtdObject.getRowCount(); i++) {
				if (!validateglHeadCode(dtdObject.getValue(i, 2))) {
					getErrorMap().setError("statusCode", BackOfficeErrorCodes.INVALID_ACTION);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("statusCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}

		return false;
	}

	public boolean validateglHeadCode(String glHeadCode) {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("GL_HEAD_CODE", glHeadCode);
			formDTO.set(ContentManager.ACTION, USAGE);
			formDTO = validateGlHeadCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("glHeadCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("glHeadCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public boolean validateRemarks() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (remarks != null && remarks.length() > 0) {
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			if (getAction().equals(MODIFY)) {
				if (commonValidator.isEmpty(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}
}