

/*
 *
 Author : Raja E
 Created Date : 14-02-2017
 Spec Reference : IASSETTYPETAXSCH
 Modification History
 ----------------------------------------------------------------------
 Sl.No		Modified Date	Author			Modified Changes	Version
 ----------------------------------------------------------------------

 */










package patterns.config.web.forms.cmn;

import java.sql.ResultSet;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.process.TBAActionType;
import patterns.config.framework.service.DTDObject;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.DHTMLXGridUtility;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.BaseDomainValidator;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.MasterValidator;
import patterns.config.web.forms.GenericFormBean;

public class iassettypetaxschbean extends GenericFormBean {

	private static final long serialVersionUID = 1L;

	private String assetTypeCode;
	private String effectiveDate;
	private String stateCode;
	private String taxSheduleCode;
	private String xmliassettypetaxschGrid;
	private boolean enabled;
	private String remarks;

	public String getAssetTypeCode() {
		return assetTypeCode;
	}

	public void setAssetTypeCode(String assetTypeCode) {
		this.assetTypeCode = assetTypeCode;
	}

	public String getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public String getStateCode() {
		return stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	public String getTaxSheduleCode() {
		return taxSheduleCode;
	}

	public void setTaxSheduleCode(String taxSheduleCode) {
		this.taxSheduleCode = taxSheduleCode;
	}

	public String getXmliassettypetaxschGrid() {
		return xmliassettypetaxschGrid;
	}

	public void setXmliassettypetaxschGrid(String xmliassettypetaxschGrid) {
		this.xmliassettypetaxschGrid = xmliassettypetaxschGrid;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public void reset() {
		// TODO Auto-generated method stub
		assetTypeCode = RegularConstants.EMPTY_STRING;
		effectiveDate = RegularConstants.EMPTY_STRING;
		stateCode = RegularConstants.EMPTY_STRING;
		taxSheduleCode = RegularConstants.EMPTY_STRING;
		xmliassettypetaxschGrid = RegularConstants.EMPTY_STRING;
		enabled = false;
		remarks = RegularConstants.EMPTY_STRING;
		setProcessBO("patterns.config.framework.bo.cmn.iassettypetaxschBO");
	}

	@Override
	public void validate() {
		// TODO Auto-generated method stub
		if (validateAssetTypeCode() && validateEffectiveDate()) {
			validateGrid();
			validateRemarks();
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.set("ASSET_TYPE_CODE", assetTypeCode);
				formDTO.set("EFF_DATE", effectiveDate);
				if (getAction().equals(ADD)) {
					formDTO.set("ENABLED", decodeBooleanToString(true));
				} else {
					formDTO.set("ENABLED", decodeBooleanToString(enabled));
				}
				formDTO.set("REMARKS", remarks);

				DTDObject dtdObject = new DTDObject();
				dtdObject.addColumn(0, "SL");
				dtdObject.addColumn(1, "ACTION");
				dtdObject.addColumn(2, "STATE_CODE");
				dtdObject.addColumn(3, "STATE_DESCRIPTION");
				dtdObject.addColumn(4, "TAX_SCHEDULE_CODE");
				dtdObject.setXML(xmliassettypetaxschGrid);
				formDTO.setDTDObject("ASSETTYPETAXSCHHISTDTL", dtdObject);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("assetTypeCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
	}

	private boolean validateAssetTypeCode() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("ASSET_TYPE_CODE", assetTypeCode);
			formDTO.set(ContentManager.USER_ACTION, USAGE);
			formDTO = validateAssetTypeCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("assetTypeCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("assetTypeCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateAssetTypeCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		MasterValidator validation = new MasterValidator();
		String assetTypeCode = inputDTO.get("ASSET_TYPE_CODE");
		try {
			if (validation.isEmpty(assetTypeCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
			resultDTO = validation.validateAssetType(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateEffectiveDate() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("ASSET_TYPE_CODE", assetTypeCode);
			formDTO.set("EFF_DATE", effectiveDate);
			formDTO.set(ContentManager.USER_ACTION, getAction());
			formDTO = validateEffectiveDate(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("effectiveDate", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("effectiveDate", BackOfficeErrorCodes.UNSPECIFIED_ERROR);

		}
		return false;
	}

	public DTObject validateEffectiveDate(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		CommonValidator commonvalidation = new CommonValidator();
		String assetTypeCode = inputDTO.get("ASSET_TYPE_CODE");
		String effectiveDate = inputDTO.get("EFF_DATE");
		String action = inputDTO.get(ContentManager.USER_ACTION);
		try {
			if (commonvalidation.isEmpty(effectiveDate)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			TBAActionType AT;
			if (action.equals(ADD))
				AT = TBAActionType.ADD;
			else
				AT = TBAActionType.MODIFY;
			if (!commonvalidation.isEffectiveDateValid(effectiveDate, AT)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_EFFECTIVE_DATE);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "ENABLED");
			inputDTO.set(ContentManager.TABLE, "ASSETTYPETAXSCHHIST");
			String columns[] = new String[] { context.getEntityCode(), assetTypeCode, effectiveDate };
			inputDTO.setObject(ContentManager.PROGRAM_KEY, columns);
			resultDTO = commonvalidation.validatePK(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (action.equals(ADD)) {
				if (ContentManager.DATA_AVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_EXISTS);
					return resultDTO;
				}
				inputDTO.set("ENTITY_CODE", context.getEntityCode());
				resultDTO = getStateCodes(inputDTO);
			} else if (action.equals(MODIFY)) {
				if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_NOT_EXISTS);
					return resultDTO;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonvalidation.close();
		}
		return resultDTO;
	}

	public DTObject getStateCodes(DTObject inputDTO) {
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		BaseDomainValidator validation = new BaseDomainValidator();
		DTObject resultDTO = new DTObject();
		DHTMLXGridUtility gridUtility = new DHTMLXGridUtility();
		String defCentralCode = RegularConstants.EMPTY_STRING;
		StringBuffer sqlQuery = new StringBuffer(RegularConstants.EMPTY_STRING);
		try {
			resultDTO = validation.getINSTALL(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_NOT_EXISTS);
				return resultDTO;
			}else if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
				defCentralCode=resultDTO.get("CENTRAL_CODE");
			}
				
			sqlQuery.append("SELECT STATE_CODE,DESCRIPTION FROM STATE WHERE ENTITY_CODE=? AND STATE_CODE <> ? AND ENABLED='1'");
			sqlQuery.append("");
			util.reset();
			util.setMode(DBUtil.PREPARED);
			util.setSql(sqlQuery.toString());
			util.setString(1, inputDTO.get("ENTITY_CODE"));
			util.setString(2, defCentralCode);
			ResultSet rset = util.executeQuery();
			gridUtility.init();
			while (rset.next()) {
				gridUtility.startRow(rset.getString(1));
				gridUtility.setCell("0");
				gridUtility.setCell("<a href='javascript:void(0);' onclick=iassettypetaxsch_editGrid('" + rset.getString("STATE_CODE") + "')>Edit</a>");
				gridUtility.setCell(rset.getString("STATE_CODE"));
				gridUtility.setCell(rset.getString("DESCRIPTION"));
				gridUtility.setCell("");
				gridUtility.setCell("");
				gridUtility.endRow();
			}
			gridUtility.finish();
			String resultXML = gridUtility.getXML();
			inputDTO.set(ContentManager.RESULT_XML, resultXML);
			inputDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);

		} catch (Exception e) {
			e.printStackTrace();
			inputDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			dbContext.close();
			validation.close();
		}
		return inputDTO;
	}

	public boolean validateStateCode(String stateCode) {
		try {
			DTObject formDTO = getFormDTO();
			formDTO.set("STATE_CODE", stateCode);
			formDTO.set(ContentManager.USER_ACTION, USAGE);
			formDTO = validateStateCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("stateCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("stateCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateStateCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		MasterValidator validation = new MasterValidator();
		String stateCode = inputDTO.get("STATE_CODE");
		try {
			if (validation.isEmpty(stateCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
			resultDTO = validation.validateStateCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateTaxScheduleCode(String taxScheduleCode, String stateCode) {
		try {
			DTObject formDTO = getFormDTO();
			formDTO.set("STATE_CODE", stateCode);
			formDTO.set("TAX_SCHEDULE_CODE", taxScheduleCode);
			formDTO = validateTaxScheduleCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("taxSheduleCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("taxSheduleCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateTaxScheduleCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		MasterValidator validation = new MasterValidator();
		String taxScheduleCode = inputDTO.get("TAX_SCHEDULE_CODE");
		try {
			if (!validation.isEmpty(taxScheduleCode)) {
				if (!validation.isTaxScheduleAllowed(inputDTO)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
					return resultDTO;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	private boolean validateGrid() {
		CommonValidator validation = new CommonValidator();
		try {
			DTDObject dtdObject = new DTDObject();
			dtdObject.addColumn(0, "SL");
			dtdObject.addColumn(1, "ACTION");
			dtdObject.addColumn(2, "STATE_CODE");
			dtdObject.addColumn(3, "STATE_DESCRIPTION");
			dtdObject.addColumn(4, "TAX_SCHEDULE_CODE");
			dtdObject.setXML(xmliassettypetaxschGrid);
			if (dtdObject.getRowCount() <= 0) {
				getErrorMap().setError("stateCode", BackOfficeErrorCodes.ATLEAST_ONE_ROW);
				return false;
			}
			if (!validation.checkDuplicateRows(dtdObject, 2, 4)) {
				getErrorMap().setError("stateCode", BackOfficeErrorCodes.DUPLICATE_DATA);
				return false;
			}
			int checkEmpty = 0;
			for (int i = 0; i < dtdObject.getRowCount(); i++) {
				if (!validateStateCode(dtdObject.getValue(i, 2))) {
					getErrorMap().setError("stateCode", BackOfficeErrorCodes.INVALID_ACTION);
					return false;
				}
				if (!validateTaxScheduleCode(dtdObject.getValue(i, 4), dtdObject.getValue(i, 2))) {
					getErrorMap().setError("taxSheduleCode", BackOfficeErrorCodes.INVALID_ACTION);
					return false;
				}
				if (!validation.isEmpty(dtdObject.getValue(i, 4))) {
					checkEmpty++;
				}
			}
			if (checkEmpty == 0) {
				getErrorMap().setError("gridError", BackOfficeErrorCodes.ATLEAST_ONE_CODE_MANDATORY);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("stateCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateRemarks() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (remarks != null && remarks.length() > 0) {
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			if (getAction().equals(MODIFY)) {
				if (commonValidator.isEmpty(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}

}