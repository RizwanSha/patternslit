package patterns.config.web.forms.cmn;

import java.util.ArrayList;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.process.TBAActionType;
import patterns.config.framework.service.DTDObject;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.GenericOption;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.AccessValidator;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.MasterValidator;
/*
 * The program will be used to create and maintain  Tenor Specification Code. Tenor Configuration Specification. 
 *
 Author : Pavan kumar.R
 Created Date : 01-DEC-2016
 Spec Reference CMN-00042
 Modification History
 ----------------------------------------------------------------------
 Sl.No		Modified Date	Author			Modified Changes	Version
 ----------------------------------------------------------------------

 */
import patterns.config.web.forms.GenericFormBean;

public class mtenorconfigbean extends GenericFormBean {

	private static final long serialVersionUID = -6219657363150466160L;

	private String tenorSpecificationCode;
	private String efftDate;
	private String uptoPeriod;
	private String periodFlag;
	private String tenorDescription;
	private boolean enabled;
	private String remarks;
	private String xmlMtenorconfigGrid;
	public static final int MONTHS_IN_DAYS = 30;

	public String getTenorSpecificationCode() {
		return tenorSpecificationCode;
	}

	public void setTenorSpecificationCode(String tenorSpecificationCode) {
		this.tenorSpecificationCode = tenorSpecificationCode;
	}

	public String getEfftDate() {
		return efftDate;
	}

	public void setEfftDate(String efftDate) {
		this.efftDate = efftDate;
	}

	public String getUptoPeriod() {
		return uptoPeriod;
	}

	public void setUptoPeriod(String uptoPeriod) {
		this.uptoPeriod = uptoPeriod;
	}

	public String getPeriodFlag() {
		return periodFlag;
	}

	public void setPeriodFlag(String periodFlag) {
		this.periodFlag = periodFlag;
	}

	public String getTenorDescription() {
		return tenorDescription;
	}

	public void setTenorDescription(String tenorDescription) {
		this.tenorDescription = tenorDescription;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getXmlMtenorconfigGrid() {
		return xmlMtenorconfigGrid;
	}

	public void setXmlMtenorconfigGrid(String xmlMtenorconfigGrid) {
		this.xmlMtenorconfigGrid = xmlMtenorconfigGrid;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void reset() {
		tenorSpecificationCode = RegularConstants.EMPTY_STRING;
		efftDate = RegularConstants.EMPTY_STRING;
		uptoPeriod = RegularConstants.EMPTY_STRING;
		periodFlag = RegularConstants.EMPTY_STRING;
		tenorDescription = RegularConstants.EMPTY_STRING;
		enabled = false;
		remarks = RegularConstants.EMPTY_STRING;
		xmlMtenorconfigGrid = RegularConstants.EMPTY_STRING;
		DBContext dbContext = new DBContext();
		ArrayList<GenericOption> accountingType = null;
		accountingType = getGenericOptions("MTENORCONFIG", "UPTO_PERIOD", dbContext, RegularConstants.NULL);
		webContext.getRequest().setAttribute("MTENORCONFIG_UPTO_PERIOD", accountingType);
		dbContext.close();
		setProcessBO("patterns.config.framework.bo.cmn.mtenorconfigBO");
	}

	@Override
	public void validate() {
		if (validateTenorSpecificationCode() && validateEffectiveDate()) {
			validateGrid();
			validateRemarks();

		}

		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.set("TENOR_SPEC_CODE", tenorSpecificationCode);
				formDTO.set("EFF_DATE", efftDate);
				DTDObject dtdObject = new DTDObject();
				dtdObject.addColumn(0, "SELECT");
				dtdObject.addColumn(1, "UPTO_PERIOD");
				dtdObject.addColumn(2, "PERIOD_FLAG_LABEL");
				dtdObject.addColumn(3, "TENOR_DESCRIPTION");
				dtdObject.addColumn(4, "PERIOD_FLAG");
				dtdObject.setXML(xmlMtenorconfigGrid);
				formDTO.setDTDObject("TENORCONFIGHISTDTL", dtdObject);
				if (getAction().equals(ADD)) {
					formDTO.set("ENABLED", decodeBooleanToString(true));
				} else {
					formDTO.set("ENABLED", decodeBooleanToString(enabled));
				}
				formDTO.set("REMARKS", remarks);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("tenorSpecificationCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}

	}

	public boolean validateTenorSpecificationCode() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("TENOR_SPEC_CODE", tenorSpecificationCode);
			formDTO.set(ContentManager.ACTION, USAGE);
			formDTO = validateTenorSpecificationCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("tenorSpecificationCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("tenorSpecificationCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateTenorSpecificationCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		MasterValidator validation = new MasterValidator();
		String tenorSpecificationCode = inputDTO.get("TENOR_SPEC_CODE");
		try {
			if (validation.isEmpty(tenorSpecificationCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
			resultDTO = validation.validateTenorSpecificationCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
			return resultDTO;
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateEffectiveDate() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("ACTION", getAction());
			formDTO.set("TENOR_SPEC_CODE", tenorSpecificationCode);
			formDTO.set("EFF_DATE", efftDate);
			formDTO = validateEffectiveDate(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("efftDate", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("efftDate", BackOfficeErrorCodes.UNSPECIFIED_ERROR);

		}
		return false;
	}

	public DTObject validateEffectiveDate(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		CommonValidator commonvalidation = new CommonValidator();
		String tenorSpecificationCode = inputDTO.get("TENOR_SPEC_CODE");
		String effectiveDate = inputDTO.get("EFF_DATE");
		String action = inputDTO.get(ContentManager.USER_ACTION);
		try {
			if (commonvalidation.isEmpty(effectiveDate)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			TBAActionType AT;
			if (action.equals(ADD))
				AT = TBAActionType.ADD;
			else
				AT = TBAActionType.MODIFY;
			if (!commonvalidation.isEffectiveDateValid(effectiveDate, AT)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_EFFECTIVE_DATE);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "TENOR_SPEC_CODE");
			inputDTO.set(ContentManager.TABLE, "TENORCONFIGHIST");
			String columns[] = new String[] { context.getEntityCode(), tenorSpecificationCode, effectiveDate };
			inputDTO.setObject(ContentManager.PROGRAM_KEY, columns);
			resultDTO = commonvalidation.validatePK(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (action.equals(ADD)) {
				if (ContentManager.DATA_AVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_EXISTS);
					return resultDTO;
				}
			} else if (action.equals(MODIFY)) {
				if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_NOT_EXISTS);
					return resultDTO;
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonvalidation.close();
		}
		return resultDTO;
	}

	private boolean validateGrid() {
		AccessValidator validation = new AccessValidator();
		try {
			DTDObject dtdObject = new DTDObject();
			dtdObject.addColumn(0, "SELECT");
			dtdObject.addColumn(1, "UPTO_PERIOD");
			dtdObject.addColumn(2, "PERIOD_FLAG_LABEL");
			dtdObject.addColumn(3, "TENOR_DESCRIPTION");
			dtdObject.addColumn(4, "PERIOD_FLAG");
			dtdObject.setXML(xmlMtenorconfigGrid);
			if (dtdObject.getRowCount() <= 0) {
				getErrorMap().setError("uptoPeriod", BackOfficeErrorCodes.ATLEAST_ONE_ROW);
				return false;
			}
			int uptoPeriodInDaysOfLastRow;
			int uptoPeriodInDaysOfCurrentRow;
			for (int i = 0; i < dtdObject.getRowCount(); i++) {
				if (i > 0) {
					if (dtdObject.getValue(i - 1, 4).equals("M")) {
						if (dtdObject.getValue(i, 4).equals("D")) {
							getErrorMap().setError("uptoPeriod", BackOfficeErrorCodes.PBS_SAME_PERIOD_FLAG);
							return false;
						}
						uptoPeriodInDaysOfLastRow = Integer.parseInt(dtdObject.getValue(i - 1, 1)) * MONTHS_IN_DAYS;
					} else {
						uptoPeriodInDaysOfLastRow = Integer.parseInt(dtdObject.getValue(i - 1, 1));
					}
				} else {
					uptoPeriodInDaysOfLastRow = 0;
				}
				if (dtdObject.getValue(i, 4).equals("M"))
					uptoPeriodInDaysOfCurrentRow = Integer.parseInt(dtdObject.getValue(i, 1)) * MONTHS_IN_DAYS;
				else
					uptoPeriodInDaysOfCurrentRow = Integer.parseInt(dtdObject.getValue(i, 1));

				if (!validateUptoPeriod(uptoPeriodInDaysOfCurrentRow, uptoPeriodInDaysOfLastRow)) {
					getErrorMap().setError("uptoPeriod", BackOfficeErrorCodes.INVALID_ACTION);
					return false;
				}
				if (!validatePeriodFlag(dtdObject.getValue(i, 4))) {
					getErrorMap().setError("uptoPeriod", BackOfficeErrorCodes.INVALID_ACTION);
					return false;
				}

			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("uptoPeriod", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateUptoPeriod(int currentuptoPeriod, int previousUptoPeriod) {
		CommonValidator validation = new CommonValidator();
		try {
			String currentuptoPeriodString = String.valueOf(currentuptoPeriod);
			if (validation.isEmpty(currentuptoPeriodString)) {
				getErrorMap().setError("uptoPeriod", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			} else if (!validation.isValidNumber(currentuptoPeriodString)) {
				getErrorMap().setError("uptoPeriod", BackOfficeErrorCodes.INVALID_NUMBER);
				return false;
			} else if (!validation.isValidPositiveNumber(currentuptoPeriodString)) {
				getErrorMap().setError("uptoPeriod", BackOfficeErrorCodes.POSITIVE_CHECK);
				return false;
			} else if (!validation.isNumberGreater(currentuptoPeriodString, String.valueOf(previousUptoPeriod))) {
				getErrorMap().setError("uptoPeriod", BackOfficeErrorCodes.PBS_TENOR_INCR_ORDER);
				return false;
			}

			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("uptoPeriod", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	private boolean validatePeriodFlag(String periodFlag) {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(periodFlag)) {
				getErrorMap().setError("periodFlag", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}

			if (!validation.isCMLOVREC("MTENORCONFIG", "UPTO_PERIOD", periodFlag)) {
				getErrorMap().setError("periodFlag", BackOfficeErrorCodes.INVALID_OPTION);
				return false;
			}

			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("periodFlag", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateTenorDescription(String tenorDescription) {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (commonValidator.isEmpty(tenorDescription)) {
				getErrorMap().setError("tenorDescription", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!commonValidator.isValidRemarks(tenorDescription)) {
				getErrorMap().setError("tenorDescription", BackOfficeErrorCodes.INVALID_REMARKS);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("tenorDescription", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}

	public boolean validateRemarks() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (remarks != null && remarks.length() > 0) {
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			if (getAction().equals(MODIFY)) {
				if (commonValidator.isEmpty(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}
}
