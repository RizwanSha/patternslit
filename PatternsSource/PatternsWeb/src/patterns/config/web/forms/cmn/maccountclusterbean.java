/*
 *
 Author : NARESHKUMAR D
 Created Date : 28-JUN-2016
 Spec Reference : PPBS/CMN/0005-,MACCOUNTCLUSTER - Account Cluster codes maintenance(1-Master, Abstraction Program)
 Modification History
 ----------------------------------------------------------------------
 Sl.No		Modified Date	Author			Modified Changes	Version
 ----------------------------------------------------------------------
 
 */

package patterns.config.web.forms.cmn;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.MasterValidator;
import patterns.config.web.forms.GenericFormBean;

public class maccountclusterbean extends GenericFormBean {

	private static final long serialVersionUID = 1L;
	private String accountClusterCode;
	private String description;
	private String conciseDescription;
	private String accountClusterAlphaCode;
	private String clusterStateCode;
	private boolean enabled;
	private String remarks;

	public String getAccountClusterCode() {
		return accountClusterCode;
	}

	public void setAccountClusterCode(String accountClusterCode) {
		this.accountClusterCode = accountClusterCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getConciseDescription() {
		return conciseDescription;
	}

	public void setConciseDescription(String conciseDescription) {
		this.conciseDescription = conciseDescription;
	}

	public String getAccountClusterAlphaCode() {
		return accountClusterAlphaCode;
	}

	public void setAccountClusterAlphaCode(String accountClusterAlphaCode) {
		this.accountClusterAlphaCode = accountClusterAlphaCode;
	}

	public String getClusterStateCode() {
		return clusterStateCode;
	}

	public void setClusterStateCode(String clusterStateCode) {
		this.clusterStateCode = clusterStateCode;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void reset() {
		accountClusterCode = RegularConstants.EMPTY_STRING;
		description = RegularConstants.EMPTY_STRING;
		conciseDescription = RegularConstants.EMPTY_STRING;
		accountClusterAlphaCode = RegularConstants.EMPTY_STRING;
		clusterStateCode = RegularConstants.EMPTY_STRING;
		enabled = false;
		remarks = RegularConstants.EMPTY_STRING;
		setProcessBO("patterns.config.framework.bo.cmn.maccountclusterBO");
	}

	public void validate() {
		if (validateAccountClusterCodes()) {
			validateDescription();
			validateConciseDescription();
			validateAccountClusterAlphaCode();
			validateClusterStateCode();
			validateRemarks();
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.set("ACNT_CLUSTER_CODE", accountClusterCode);
				formDTO.set("DESCRIPTION", description);
				formDTO.set("CONCISE_DESCRIPTION", conciseDescription);
				formDTO.set("CLUSTER_ALPHA_CODE", accountClusterAlphaCode);
				formDTO.set("CLUSTER_STATE_CODE", clusterStateCode);
				if (getAction().equals(ADD)) {
					formDTO.set("ENABLED", decodeBooleanToString(true));
				} else {
					formDTO.set("ENABLED", decodeBooleanToString(enabled));
				}
				formDTO.set("REMARKS", remarks);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("accountClusterCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
	}

	public boolean validateAccountClusterCodes() {
		try {

			DTObject formDTO = new DTObject();
			formDTO.set("ACNT_CLUSTER_CODE", accountClusterCode);
			formDTO.set(ContentManager.ACTION, getAction());
			formDTO = validateAccountClusterCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("accountClusterCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("accountClusterCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateAccountClusterCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		MasterValidator validation = new MasterValidator();
		String accountClusterCode = inputDTO.get("ACNT_CLUSTER_CODE");
		String action = inputDTO.get(ContentManager.USER_ACTION);
		try {
			if (validation.isEmpty(accountClusterCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "ACNT_CLUSTER_CODE");
			resultDTO = validation.validateAccountClusterCode(inputDTO);
			if (action.equals(ADD)) {
				if (ContentManager.DATA_AVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
						resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_EXISTS);
					}
				}
			} else {
				if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_NOT_EXISTS);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateDescription() {
		CommonValidator validation = new CommonValidator();
		try {
			if (!validation.isValidDescription(description)) {
				getErrorMap().setError("description", BackOfficeErrorCodes.INVALID_DESCRIPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("description", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateConciseDescription() {
		CommonValidator validation = new CommonValidator();
		try {
			if (!validation.isValidConciseDescription(conciseDescription)) {
				getErrorMap().setError("conciseDescription", BackOfficeErrorCodes.INVALID_CONCISE_DESCRIPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("conciseDescription", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateAccountClusterAlphaCode() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("PROGRAM_ID", context.getProcessID());
			formDTO.set(ContentManager.ACTION, USAGE);
			formDTO.set("ACNT_CLUSTER_CODE", accountClusterCode);
			formDTO.set("CLUSTER_ALPHA_CODE", accountClusterAlphaCode);
			formDTO = validateAccountClusterAlphaCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null && !formDTO.get(ContentManager.ERROR).equals(RegularConstants.EMPTY_STRING)) {
				getErrorMap().setError("accountClusterAlphaCode", formDTO.get(ContentManager.ERROR), (Object[]) formDTO.getObject(ContentManager.ERROR_PARAM));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("accountClusterAlphaCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateAccountClusterAlphaCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		MasterValidator validation = new MasterValidator();
		String accountClusterAlphaCode = inputDTO.get("CLUSTER_ALPHA_CODE");
		String accountClusterCode = inputDTO.get("ACNT_CLUSTER_CODE");
		String action = inputDTO.get(ContentManager.USER_ACTION);
		try {
			if (validation.isEmpty(accountClusterAlphaCode) || validation.isEmpty(accountClusterCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			if (accountClusterAlphaCode.equals((accountClusterCode))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.PBS_ACC_NOT_EQ_ACCALPHA);
				return resultDTO;
			}
			String primaryKey = context.getEntityCode() + RegularConstants.PK_SEPARATOR + inputDTO.get("ACNT_CLUSTER_CODE");
			String dedupKey = context.getEntityCode() + RegularConstants.PK_SEPARATOR + inputDTO.get("CLUSTER_ALPHA_CODE");
			inputDTO.set(ContentManager.PROGRAM_ID, inputDTO.get("PROGRAM_ID"));
			inputDTO.set(ContentManager.PURPOSE_ID, "CONNECTION_CHK");
			inputDTO.set(ContentManager.PROGRAM_KEY, primaryKey);
			inputDTO.set(ContentManager.DEDUP_KEY, dedupKey);
			resultDTO = validation.isDeDupAlreadyExist(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null)
				return resultDTO;
			else if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.PPBS_ALPHA_CODE_ALREADY_EXIT);
				Object[] param = { resultDTO.get("LINKED_TO_PK").split("\\|")[1] };
				resultDTO.setObject(ContentManager.ERROR_PARAM, param);
				return resultDTO;
			}
			inputDTO.reset();
			inputDTO.set(ContentManager.ACTION, action);
			inputDTO.set("ACNT_CLUSTER_CODE", accountClusterAlphaCode);
			inputDTO.set(ContentManager.FETCH_COLUMNS, "ACNT_CLUSTER_CODE,CLUSTER_ALPHA_CODE");
			resultDTO = validation.validateAccountClusterCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
				if (accountClusterAlphaCode.equals(resultDTO.get("ACNT_CLUSTER_CODE"))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.PBS_ACC_CODE_ALREADY_EXIT);
					Object[] param = { resultDTO.get("CLUSTER_ALPHA_CODE") };
					resultDTO.setObject(ContentManager.ERROR_PARAM, param);
					return resultDTO;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateClusterStateCode() {
		CommonValidator validation = new CommonValidator();
		try {
			if (!validation.isEmpty(clusterStateCode)) {
				DTObject formDTO = new DTObject();
				formDTO.set("GEO_UNIT_ID", clusterStateCode);
				formDTO.set(ContentManager.ACTION, USAGE);
				formDTO = validateClusterStateCode(formDTO);
				if (formDTO.get(ContentManager.ERROR) != null) {
					getErrorMap().setError("clusterStateCode", formDTO.get(ContentManager.ERROR));
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("clusterStateCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public DTObject validateClusterStateCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		MasterValidator validation = new MasterValidator();
		String clusterStateCode = inputDTO.get("GEO_UNIT_ID");
		try {
			if (validation.isEmpty(clusterStateCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set("ENTITY_CODE", context.getEntityCode());
			inputDTO.set(ContentManager.ACTION, USAGE);
			inputDTO.set(ContentManager.FETCH_COLUMNS, "OUR_COUNTRY_CODE,GEO_UNIT_STRUC_STATE_CODE");
			resultDTO = validation.validateEntityCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			} else if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
				String entityStateCode = resultDTO.get("GEO_UNIT_STRUC_STATE_CODE");
				inputDTO.set("GEOC_COU_CODE", resultDTO.get("OUR_COUNTRY_CODE"));
				inputDTO.set("GEOC_UNIT_ID", clusterStateCode);
				inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION,GEO_UNIT_STRUC_CODE");
				resultDTO = validation.validateGeographicalUnitId(inputDTO);
				if (resultDTO.get(ContentManager.ERROR) != null) {
					resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
					return resultDTO;
				}
				if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
					return resultDTO;
				}
				if (!entityStateCode.equals(resultDTO.get("GEO_UNIT_STRUC_CODE"))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.PBS_STATECODE_ONLY_ALLOWED);
					return resultDTO;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateRemarks() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (remarks != null && remarks.length() > 0) {
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			if (getAction().equals(MODIFY)) {
				if (commonValidator.isEmpty(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}
}