/*
 *
 Author : NARESHKUMAR D
 Created Date : 01-JULY-2016
 Spec Reference : PPBS/CMN/0015-MENTITYBRANCH - Entity Branch code Master Maintenance (1-Master, Abstraction Program)
 Modification History
 ----------------------------------------------------------------------
 Sl.No		Modified Date	Author			Modified Changes	Version
 ----------------------------------------------------------------------
 
 */

package patterns.config.web.forms.cmn;

import java.sql.ResultSet;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeDateUtils;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.AccessValidator;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.MasterValidator;
import patterns.config.web.forms.GenericFormBean;

public class mentitybranchbean extends GenericFormBean {

	private static final long serialVersionUID = 1L;
	private String entityBranchCode;
	private String description;
	private String conciseDescription;
	private String officeOpenDate;
	private String branchDetails;
	private String geoUnitStateCode;
	private String organizationUnit;
	private String controlBranchOffice;
	private String clusterCode;
	private String officeCloseDate;
	private String parentOrgUnit;
	private boolean enabled;
	private String remarks;

	public String getOfficeOpenDate() {
		return officeOpenDate;
	}

	public void setOfficeOpenDate(String officeOpenDate) {
		this.officeOpenDate = officeOpenDate;
	}

	public String getOfficeCloseDate() {
		return officeCloseDate;
	}

	public void setOfficeCloseDate(String officeCloseDate) {
		this.officeCloseDate = officeCloseDate;
	}

	public String getEntityBranchCode() {
		return entityBranchCode;
	}

	public void setEntityBranchCode(String entityBranchCode) {
		this.entityBranchCode = entityBranchCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getConciseDescription() {
		return conciseDescription;
	}

	public void setConciseDescription(String conciseDescription) {
		this.conciseDescription = conciseDescription;
	}

	public String getBranchDetails() {
		return branchDetails;
	}

	public void setBranchDetails(String branchDetails) {
		this.branchDetails = branchDetails;
	}

	public String getGeoUnitStateCode() {
		return geoUnitStateCode;
	}

	public void setGeoUnitStateCode(String geoUnitStateCode) {
		this.geoUnitStateCode = geoUnitStateCode;
	}

	public String getOrganizationUnit() {
		return organizationUnit;
	}

	public void setOrganizationUnit(String organizationUnit) {
		this.organizationUnit = organizationUnit;
	}

	public String getControlBranchOffice() {
		return controlBranchOffice;
	}

	public void setControlBranchOffice(String controlBranchOffice) {
		this.controlBranchOffice = controlBranchOffice;
	}

	public String getClusterCode() {
		return clusterCode;
	}

	public void setClusterCode(String clusterCode) {
		this.clusterCode = clusterCode;
	}

	public String getParentOrgUnit() {
		return parentOrgUnit;
	}

	public void setParentOrgUnit(String parentOrgUnit) {
		this.parentOrgUnit = parentOrgUnit;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void reset() {
		entityBranchCode = RegularConstants.EMPTY_STRING;
		description = RegularConstants.EMPTY_STRING;
		conciseDescription = RegularConstants.EMPTY_STRING;
		officeOpenDate = RegularConstants.EMPTY_STRING;
		branchDetails = RegularConstants.EMPTY_STRING;
		geoUnitStateCode = RegularConstants.EMPTY_STRING;
		organizationUnit = RegularConstants.EMPTY_STRING;
		controlBranchOffice = RegularConstants.EMPTY_STRING;
		clusterCode = RegularConstants.EMPTY_STRING;
		officeCloseDate = RegularConstants.EMPTY_STRING;
		parentOrgUnit = RegularConstants.EMPTY_STRING;
		enabled = false;
		remarks = RegularConstants.EMPTY_STRING;
		setProcessBO("patterns.config.framework.bo.cmn.mentitybranchBO");
	}

	public void validate() {
		if (validateEntityBranchCodes()) {
			validateDescription();
			validateConciseDescription();
			validateOfficeOpenDate();
			validateBranchDetails();
			validateGeoUnitStateCode();
			validateOrganizationUnit();
			validateControlBranchOffice();
			validateClusterCode();
			validateOfficeCloseDate();
			validateRemarks();
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.set("BRANCH_CODE", entityBranchCode);
				formDTO.set("DESCRIPTION", description);
				formDTO.set("CONCISE_DESCRIPTION", conciseDescription);
				formDTO.set("OPEN_ON_DATE", officeOpenDate);
				formDTO.set("BRANCH_DETAILS", branchDetails);
				formDTO.set("GEO_UNIT_STATE_CODE", geoUnitStateCode);
				formDTO.set("TYPE_OF_ORG_UNIT", organizationUnit);
				formDTO.set("CONTROLLING_OFFICE_CODE", controlBranchOffice);
				formDTO.set("CLUSTER_CODE", clusterCode);
				formDTO.set("CLOSED_ON_DATE", officeCloseDate);
				if (getAction().equals(ADD)) {
					formDTO.set("ENABLED", decodeBooleanToString(true));
				} else {
					formDTO.set("ENABLED", decodeBooleanToString(enabled));
				}
				formDTO.set("REMARKS", remarks);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("entityBranchCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
	}

	public boolean validateEntityBranchCodes() {
		try {

			DTObject formDTO = new DTObject();
			formDTO.set("BRANCH_CODE", entityBranchCode);
			formDTO.set(ContentManager.ACTION, getAction());
			formDTO = validateEntityBranchCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("entityBranchCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("entityBranchCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateEntityBranchCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		AccessValidator validation = new AccessValidator();
		String entityBranchCode = inputDTO.get("BRANCH_CODE");
		String action = inputDTO.get(ContentManager.USER_ACTION);
		try {
			if (validation.isEmpty(entityBranchCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			if (!validation.isValidNumber(entityBranchCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_NUMBER);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH, ContentManager.FETCH_ALL);
			resultDTO = validation.validateEntityBranch(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (action.equals(ADD)) {
				if (ContentManager.DATA_AVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
						resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_EXISTS);
					}
				}
			} else {
				if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_NOT_EXISTS);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateDescription() {
		CommonValidator validation = new CommonValidator();
		try {
			if (!validation.isValidDescription(description)) {
				getErrorMap().setError("description", BackOfficeErrorCodes.INVALID_DESCRIPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("description", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateConciseDescription() {
		CommonValidator validation = new CommonValidator();
		try {
			if (!validation.isValidConciseDescription(conciseDescription)) {
				getErrorMap().setError("conciseDescription", BackOfficeErrorCodes.INVALID_CONCISE_DESCRIPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("conciseDescription", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateOfficeOpenDate() {
		CommonValidator validation = new CommonValidator();
		try {
			java.util.Date dateInstance = validation.isValidDate(officeOpenDate);
			if (validation.isEmpty(officeOpenDate)) {
				getErrorMap().setError("officeOpenDate", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (dateInstance == null) {
				String officeOpenDateParam[] = { context.getDateFormat() };
				getErrorMap().setError("officeOpenDate", BackOfficeErrorCodes.INVALID_DATE, officeOpenDateParam);
				return false;
			}
			if (validation.isDateGreaterThanCBD(officeOpenDate)) {
				getErrorMap().setError("officeOpenDate", BackOfficeErrorCodes.DATE_LCBD);
				return false;
			}
			DTObject formDTO = new DTObject();
			formDTO.set("OPEN_ON_DATE", officeOpenDate);
			formDTO.set(ContentManager.USER_ACTION, getAction());
			formDTO = validateOfficeOpenDate(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("officeOpenDate", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("officeOpenDate", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public DTObject validateOfficeOpenDate(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		MasterValidator validation = new MasterValidator();
		String officeOpenDate = inputDTO.get("OPEN_ON_DATE");
		try {
			inputDTO.set("ENTITY_CODE", context.getEntityCode());
			inputDTO.set(ContentManager.ACTION, USAGE);
			inputDTO.set(ContentManager.FETCH_COLUMNS, "TO_CHAR(DATE_OF_INCORPORATION,'" + BackOfficeConstants.TBA_DATE_FORMAT + "') DATE_OF_INCORPORATION");
			resultDTO = validation.validateEntityCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.getObject("DATE_OF_INCORPORATION") != RegularConstants.NULL && !resultDTO.get("DATE_OF_INCORPORATION").equals(RegularConstants.EMPTY_STRING)) {
				if (!BackOfficeDateUtils.isDateGreaterThanEqual(officeOpenDate, resultDTO.get("DATE_OF_INCORPORATION"))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.PBS_ENT_DATE_OF_INC);
					return resultDTO;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateBranchDetails() {
		CommonValidator Validator = new CommonValidator();
		try {
			if (Validator.isEmpty(branchDetails)) {
				getErrorMap().setError("branchDetails", BackOfficeErrorCodes.PBS_BRANCH_ADDRESS_DTL);
				return false;
			}
			if (!Validator.isValidRemarks(branchDetails)) {
				getErrorMap().setError("branchDetails", BackOfficeErrorCodes.PBS_BRANCH_ADDRESS_DTL);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("branchDetails", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			Validator.close();
		}
		return false;
	}

	public boolean validateGeoUnitStateCode() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("GEO_UNIT_ID", geoUnitStateCode);
			formDTO.set(ContentManager.ACTION, USAGE);
			formDTO = validateGeoUnitStateCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("geoUnitStateCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("geoUnitStateCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateGeoUnitStateCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		MasterValidator validation = new MasterValidator();
		String geoUnitStateCode = inputDTO.get("GEO_UNIT_ID");
		try {
			if (validation.isEmpty(geoUnitStateCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set("ENTITY_CODE", context.getEntityCode());
			inputDTO.set(ContentManager.ACTION, USAGE);
			inputDTO.set(ContentManager.FETCH_COLUMNS, "OUR_COUNTRY_CODE,GEO_UNIT_STRUC_STATE_CODE");
			resultDTO = validation.validateEntityCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			} else if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
				String entityStateCode = resultDTO.get("GEO_UNIT_STRUC_STATE_CODE");
				inputDTO.set("GEOC_COU_CODE", resultDTO.get("OUR_COUNTRY_CODE"));
				inputDTO.set("GEOC_UNIT_ID", geoUnitStateCode);
				inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION,GEO_UNIT_STRUC_CODE");
				resultDTO = validation.validateGeographicalUnitId(inputDTO);
				if (resultDTO.get(ContentManager.ERROR) != null) {
					resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
					return resultDTO;
				}
				if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
					return resultDTO;
				}
				if (!entityStateCode.equals(resultDTO.get("GEO_UNIT_STRUC_CODE"))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.PBS_STATECODE_ONLY_ALLOWED);
					return resultDTO;
				}
			} else {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateOrganizationUnit() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("ORG_UNIT_TYPE", organizationUnit);
			formDTO.set(ContentManager.ACTION, USAGE);
			formDTO = validateOrganizationUnit(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("organizationUnit", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("organizationUnit", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
		}
		return false;
	}

	public DTObject validateOrganizationUnit(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		MasterValidator validation = new MasterValidator();
		String organizationUnit = inputDTO.get("ORG_UNIT_TYPE");
		try {
			if (validation.isEmpty(organizationUnit)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
			resultDTO = validation.validateOrganizationUnitType(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateControlBranchOffice() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("BRANCH_CODE", entityBranchCode);
			formDTO.set("CONTROLLING_OFFICE_CODE", controlBranchOffice);
			formDTO.set(ContentManager.ACTION, USAGE);
			formDTO = validateControlBranchOffice(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("controlBranchOffice", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("controlBranchOffice", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
		}
		return false;
	}

	public DTObject validateControlBranchOffice(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		AccessValidator validation = new AccessValidator();
		String controlBranchOffice = inputDTO.get("CONTROLLING_OFFICE_CODE");
		String branchOffice = inputDTO.get("BRANCH_CODE");
		try {

			if (validation.isEmpty(controlBranchOffice)) {
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				resultDTO.set("DESCRIPTION", RegularConstants.EMPTY_STRING);
				if (isBranchHaveRecord(validation.getDbContext().createUtilInstance())) {
					resultDTO.set("ROOT_BRANCH_EXIST", getErrorResource(BackOfficeErrorCodes.PBS_CONTROL_BRN_MAN, null));
					return resultDTO;
				}
			}
			if (branchOffice.equals(controlBranchOffice)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.PBS_BRN_NOT_EQUAL_CONTROLBRN);
				return resultDTO;
			}
			if (!validation.isEmpty(controlBranchOffice)) {
				inputDTO.set("BRANCH_CODE", controlBranchOffice);
				inputDTO.set(ContentManager.FETCH_COLUMNS, "BRANCH_CODE,DESCRIPTION");
				resultDTO = validation.validateEntityBranch(inputDTO);
				if (resultDTO.get(ContentManager.ERROR) != null) {
					resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
					return resultDTO;
				}
				if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
					return resultDTO;
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateClusterCode() {
		CommonValidator validation = new CommonValidator();
		try {
			if (!validation.isEmpty(clusterCode)) {
				DTObject formDTO = new DTObject();
				formDTO.set("ACNT_CLUSTER_CODE", clusterCode);
				formDTO.set(ContentManager.ACTION, USAGE);
				formDTO = validateClusterCode(formDTO);
				if (formDTO.get(ContentManager.ERROR) != null) {
					getErrorMap().setError("clusterCode", formDTO.get(ContentManager.ERROR));
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("clusterCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public DTObject validateClusterCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		MasterValidator validation = new MasterValidator();
		String clusterCode = inputDTO.get("ACNT_CLUSTER_CODE");
		try {
			if (!validation.isEmpty(clusterCode)) {
				inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
				resultDTO = validation.validateAccountClusterCode(inputDTO);
				if (resultDTO.get(ContentManager.ERROR) != null) {
					resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
					return resultDTO;
				}
				if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
					return resultDTO;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateOfficeCloseDate() {
		CommonValidator validation = new CommonValidator();
		try {
			if (!validation.isEmpty(officeCloseDate)) {
				java.util.Date dateInstance = validation.isValidDate(officeCloseDate);
				if (dateInstance == null) {
					getErrorMap().setError("officeCloseDate", BackOfficeErrorCodes.INVALID_DATE);
					return false;
				}
				if (validation.isDateGreaterThanCBD(officeCloseDate)) {
					getErrorMap().setError("officeCloseDate", BackOfficeErrorCodes.DATE_LCBD);
					return false;
				}
				if (validation.isDateLesser(officeCloseDate, officeOpenDate)) {
					getErrorMap().setError("officeCloseDate", BackOfficeErrorCodes.PBS_DATE_GCOD);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("officeCloseDate", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateRemarks() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (remarks != null && remarks.length() > 0) {
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			if (getAction().equals(MODIFY)) {
				if (commonValidator.isEmpty(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}

	public final boolean isBranchHaveRecord(DBUtil util) {
		String sqlQuery = "SELECT 1 FROM ENTITYBRN WHERE ENTITY_CODE=? ";
		try {
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, context.getEntityCode());
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			util.reset();
		}
		return false;
	}
}
