/**This program will be used to create and maintain codes for each type of allergy that can exist. 
 * One or more allergies can be configured to a Person while registering. 
 * Also the allergies can be appended or altered for a Person. 
 * 
 * @version 1.0
 * @author  Raja E
 * @since   19-Nov-2014
 * <br>
 * <b>Modified History</b>
 * <BR>
 * <U><B>
 * Sl.No		Modified Date	Author			Modified Changes		Version
 * </B></U>
 * <br>
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 */

package patterns.config.web.forms.cmn;


import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.MasterValidator;
import patterns.config.web.forms.GenericFormBean;

public class mbeneficiarybean extends GenericFormBean {

	private static final long serialVersionUID = 1L;
	private String beneficiaryId;
	private String name;
	private String address;
	private String accountNumber;
	private String accountType;
	private String ifscCode;
	private boolean enabled;
	private String remarks;

	public String getBeneficiaryId() {
		return beneficiaryId;
	}

	public void setBeneficiaryId(String beneficiaryId) {
		this.beneficiaryId = beneficiaryId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public String getIfscCode() {
		return ifscCode;
	}

	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void reset() {
		beneficiaryId = RegularConstants.EMPTY_STRING;
		name = RegularConstants.EMPTY_STRING;
		address = RegularConstants.EMPTY_STRING;
		accountNumber = RegularConstants.EMPTY_STRING;
		accountType = RegularConstants.EMPTY_STRING;
		ifscCode = RegularConstants.EMPTY_STRING;
		enabled = false;
		remarks = RegularConstants.EMPTY_STRING;
		setProcessBO("patterns.config.framework.bo.cmn.mbeneficiaryBO");
	}

	public void validate() {
		if (validateBeneficiaryId()) {
			validateName();
			validateAddress();
			validateAccountNumber();
			validateIfscCode();
			validateRemarks();
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.set("BENEFICIARY_ID", beneficiaryId);
				formDTO.set("BENEFICIARY_NAME", name);
				formDTO.set("BENEFICIARY_ADDRESS", address);
				formDTO.set("BENEFICIARY_AC_NO", accountNumber);
				formDTO.set("BENEFICIARY_AC_TYPE", accountType);
				formDTO.set("BENEFICIARY_IFSC_CODE", ifscCode);
				if (getAction().equals(ADD)) {
					formDTO.set("ENABLED", decodeBooleanToString(true));
				} else {
					formDTO.set("ENABLED", decodeBooleanToString(enabled));
				}
				formDTO.set("REMARKS", remarks);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("beneficiaryId", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
	}

	public boolean validateBeneficiaryId() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("BENEFICIARY_ID", beneficiaryId);
			formDTO.set("ACTION", getAction());
			formDTO = validateBeneficiaryId(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("beneficiaryId", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("beneficiaryId", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateBeneficiaryId(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		MasterValidator validation = new MasterValidator();
		String beneficiaryId = inputDTO.get("BENEFICIARY_ID");
		String action = inputDTO.get(ContentManager.USER_ACTION);
		try {
			if (validation.isEmpty(beneficiaryId)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "BENEFICIARY_NAME");
			resultDTO = validation.validateBeneficiaryId(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (action.equals(ADD)) {
				if (ContentManager.DATA_AVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_EXISTS);
				}
			} else {
				if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_NOT_EXISTS);

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateName() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(name)) {
				getErrorMap().setError("name", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidName(name)) {
				getErrorMap().setError("name", BackOfficeErrorCodes.INVALID_NAME);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("name", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateAddress() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(address)) {
				getErrorMap().setError("address", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidAddress(address)) {
				getErrorMap().setError("address", BackOfficeErrorCodes.INVALID_ADDRESS);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("address", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateAccountNumber() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(accountNumber)) {
				getErrorMap().setError("accountNumber", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidBeneficiaryAccountNumber(accountNumber)) {
				getErrorMap().setError("accountNumber", BackOfficeErrorCodes.PBS_INVALID_BENEFICIARY_ACCOUNT_NUMBER);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("accountNumber", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateAccountType() {
		CommonValidator validation = new CommonValidator();
		try {
			if (!validation.isEmpty(accountType)) {
				if (!validation.isValidConciseDescription(accountType)) {
					getErrorMap().setError("accountType", BackOfficeErrorCodes.PBS_INVALID_BENEFICIARY_ACCOUNT_TYPE);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("accountType", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateIfscCode() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(ifscCode)) {
				getErrorMap().setError("ifscCode", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidIFSCCode(ifscCode)) {
				getErrorMap().setError("ifscCode", BackOfficeErrorCodes.INVALID_IFSC_CODE);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("ifscCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateRemarks() {
		CommonValidator commonValidator = new CommonValidator();
		try {

			if (getAction().equals(ADD) && remarks != null && remarks.length() > 0) {
				if (commonValidator.isEmpty(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			} else if (getAction().equals(MODIFY)) {
				if (commonValidator.isEmpty(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}

			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}
}
