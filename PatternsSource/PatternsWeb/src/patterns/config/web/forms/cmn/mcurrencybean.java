package patterns.config.web.forms.cmn;

import java.sql.ResultSet;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.MasterValidator;
import patterns.config.web.forms.GenericFormBean;

/*
 * The program will be used to create and maintain currency codes. Currency codes will be used in financial entries and displays across the application. 
 *
 Author : Pavan kumar.R
 Created Date : 11-Nov-2014
 Spec Reference HMS-PSD-CMN-0034-mcurrency
 Modification History
 ----------------------------------------------------------------------
 Sl.No		Modified Date	Author			Modified Changes	Version
 ----------------------------------------------------------------------

 */

public class mcurrencybean extends GenericFormBean {

	private static final long serialVersionUID = -6219657363150466160L;

	private String currencyCode;
	private String ccyName;
	private String ccyShortName;
	private String ccyNotation;
	private boolean hasSubCcy;

	private String sccyName;
	private String sccyShortName;
	private String sccyNotation;
	private String sccyUnitsForCcy;
	private String isoCcyCode;

	private boolean enabled;
	private String remarks;

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public String getCcyName() {
		return ccyName;
	}

	public void setCcyName(String ccyName) {
		this.ccyName = ccyName;
	}

	public String getCcyShortName() {
		return ccyShortName;
	}

	public void setCcyShortName(String ccyShortName) {
		this.ccyShortName = ccyShortName;
	}

	public String getCcyNotation() {
		return ccyNotation;
	}

	public void setCcyNotation(String ccyNotation) {
		this.ccyNotation = ccyNotation;
	}

	public boolean isHasSubCcy() {
		return hasSubCcy;
	}

	public void setHasSubCcy(boolean hasSubCcy) {
		this.hasSubCcy = hasSubCcy;
	}

	public String getSccyName() {
		return sccyName;
	}

	public void setSccyName(String sccyName) {
		this.sccyName = sccyName;
	}

	public String getSccyShortName() {
		return sccyShortName;
	}

	public void setSccyShortName(String sccyShortName) {
		this.sccyShortName = sccyShortName;
	}

	public String getSccyNotation() {
		return sccyNotation;
	}

	public void setSccyNotation(String sccyNotation) {
		this.sccyNotation = sccyNotation;
	}

	public String getSccyUnitsForCcy() {
		return sccyUnitsForCcy;
	}

	public void setSccyUnitsForCcy(String sccyUnitsForCcy) {
		this.sccyUnitsForCcy = sccyUnitsForCcy;
	}

	public String getIsoCcyCode() {
		return isoCcyCode;
	}

	public void setIsoCcyCode(String isoCcyCode) {
		this.isoCcyCode = isoCcyCode;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void reset() {
		currencyCode = RegularConstants.EMPTY_STRING;
		ccyName = RegularConstants.EMPTY_STRING;
		ccyShortName = RegularConstants.EMPTY_STRING;
		ccyNotation = RegularConstants.EMPTY_STRING;
		hasSubCcy = false;
		sccyName = RegularConstants.EMPTY_STRING;
		sccyShortName = RegularConstants.EMPTY_STRING;
		sccyNotation = RegularConstants.EMPTY_STRING;
		sccyUnitsForCcy = RegularConstants.EMPTY_STRING;
		isoCcyCode = RegularConstants.EMPTY_STRING;
		enabled = false;
		remarks = RegularConstants.EMPTY_STRING;
		setProcessBO("patterns.config.framework.bo.cmn.mcurrencyBO");

	}

	@Override
	public void validate() {
		if (validateCurrencyCode()) {
			validateCcyName();
			validateCcyShortName();
			validateCcyNotation();
			if (hasSubCcy) {
				validateSccyName();
				validateSccyShortName();
				validateSccyNotation();
				validateSccyUnitsForCcy();
			}
			validateIsoCcyCode();
			validateRemarks();
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("CCY_CODE", currencyCode);
				formDTO.set("DESCRIPTION", ccyName);
				formDTO.set("CONCISE_DESCRIPTION", ccyShortName);
				formDTO.set("CCY_NOTATION", ccyNotation);
				formDTO.set("HAS_SUB_CCY", decodeBooleanToString(hasSubCcy));
				if (hasSubCcy) {
					formDTO.set("SUB_CCY_DESCRIPTION", sccyName);
					formDTO.set("SUB_CCY_CONCISE_DESCRIPTION", sccyShortName);
					formDTO.set("SUB_CCY_NOTATION", sccyNotation);
					formDTO.set("SUB_CCY_UNITS_IN_CCY", sccyUnitsForCcy);
				} else {
					formDTO.set("SUB_CCY_DESCRIPTION", RegularConstants.EMPTY_STRING);
					formDTO.set("SUB_CCY_CONCISE_DESCRIPTION", RegularConstants.EMPTY_STRING);
					formDTO.set("SUB_CCY_NOTATION", RegularConstants.EMPTY_STRING);
					formDTO.set("SUB_CCY_UNITS_IN_CCY", RegularConstants.NULL);
				}
				formDTO.set("ISO_CCY_CODE", isoCcyCode);
				if (getAction().equals(ADD)) {
					formDTO.set("ENABLED", decodeBooleanToString(true));
				} else {
					formDTO.set("ENABLED", decodeBooleanToString(enabled));
				}

				formDTO.set("REMARKS", remarks);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("currencyCode", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		}
	}

	private boolean validateIsoCcyCode() {
		int count = 0;
		CommonValidator validation = new CommonValidator();
		DBUtil util = validation.getDbContext().createUtilInstance();

		
		try {
			if (!validation.isValidNumber(isoCcyCode)) {
				getErrorMap().setError("isoCcyCode", BackOfficeErrorCodes.HMS_INVALID_NUMBER);
				return false;
			}
			
			if (!validation.isValidISOCurrency(isoCcyCode)) {
				getErrorMap().setError("isoCcyCode", BackOfficeErrorCodes.HMS_INVALID_VALUE);
				return false;
			}
			
			StringBuffer sqlQuery = new StringBuffer("SELECT COUNT(1) FROM CURRENCY WHERE ISO_CCY_CODE=? AND ENABLED='1' AND CCY_CODE<>?");
			util.reset();
			util.setSql(sqlQuery.toString());
			util.setString(1, isoCcyCode);
			util.setString(2, currencyCode);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				count = rset.getInt(1);
			}
			if (count != 0) {
				getErrorMap().setError("isoCcyCode", BackOfficeErrorCodes.HMS_ISO_CCY_INUSE);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("isoCcyCode", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			util.reset();
			validation.close();
		}
		return false;
	}

	private boolean validateSccyUnitsForCcy() {
		CommonValidator validation = new CommonValidator();
		try {
			if (!validation.isValidFiveDigit(sccyUnitsForCcy)) {
				getErrorMap().setError("sccyUnitsForCcy", BackOfficeErrorCodes.HMS_INVALID_VALUE);
				return false;
			}

			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("sccyUnitsForCcy", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	private boolean validateSccyNotation() {
		CommonValidator validation = new CommonValidator();
		try {
			if (!validation.isValidCurrencyNotation(sccyNotation)) {
				getErrorMap().setError("sccyNotation", BackOfficeErrorCodes.HMS_INVALID_VALUE);
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("sccyNotation", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	private boolean validateSccyShortName() {

		CommonValidator validation = new CommonValidator();
		try {
			if (!validation.isValidConciseDescription(sccyShortName)) {
				getErrorMap().setError("sccyShortName", BackOfficeErrorCodes.HMS_INVALID_SHORT_NAME);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("sccyShortName", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;

	}

	private boolean validateSccyName() {
		CommonValidator validation = new CommonValidator();
		try {
			if (!validation.isValidDescription(sccyName)) {
				getErrorMap().setError("sccyName", BackOfficeErrorCodes.HMS_INVALID_NAME);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("sccyName", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	private boolean validateCcyNotation() {
		CommonValidator validation = new CommonValidator();
		try {
			if (!validation.isValidCurrencyNotation(ccyNotation)) {
				getErrorMap().setError("ccyNotation", BackOfficeErrorCodes.HMS_INVALID_VALUE);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("ccyNotation", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	private boolean validateCcyShortName() {

		CommonValidator validation = new CommonValidator();
		try {
			if (!validation.isValidConciseDescription(ccyShortName)) {
				getErrorMap().setError("ccyShortName", BackOfficeErrorCodes.HMS_INVALID_SHORT_NAME);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("ccyShortName", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	private boolean validateCcyName() {
		CommonValidator validation = new CommonValidator();
		try {
			if (!validation.isValidDescription(ccyName)) {
				getErrorMap().setError("ccyName", BackOfficeErrorCodes.HMS_INVALID_NAME);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("ccyName", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;

	}

	private boolean validateCurrencyCode() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("CCY_CODE", currencyCode);
			formDTO.set("ACTION", getAction());
			formDTO = validateCurrencyCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("currencyCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("currencyCode", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		}
		return false;
	}

	public boolean validateRemarks() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (remarks != null && remarks.length() > 0) {
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.HMS_INVALID_REMARKS);
					return false;
				}
			}
			if (getAction().equals(MODIFY)) {
				if (commonValidator.isEmpty(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.HMS_FIELD_BLANK);
					return false;
				}
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.HMS_INVALID_REMARKS);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}

	public DTObject validateCurrencyCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		MasterValidator validation = new MasterValidator();
		String currencyCode = inputDTO.get("CCY_CODE");
		String action = inputDTO.get("ACTION");
		try {
			if (validation.isEmpty(currencyCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH, ContentManager.FETCH_ALL);
			resultDTO = validation.validateCurrency(inputDTO);
			if (action.equals(ADD)) {
				if (ContentManager.DATA_AVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_RECORD_EXISTS);
				}
			} else {
				if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_RECORD_NOT_EXISTS);

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}
}
