package patterns.config.web.forms.cmn;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.process.TBAActionType;
import patterns.config.framework.service.DTDObject;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.framework.web.configuration.menu.MenuUtils;
import patterns.config.validations.AccessValidator;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.MasterValidator;
import patterns.config.web.forms.GenericFormBean;

/**
 * 
 * 
 * @version 1.0
 * @author Dileep Kumar Reddy T
 * @since 30-Jun-2016 <br>
 *        Third Party Organizations as Business Correspondents <b>Modified
 *        History</b> <BR>
 *        <U><B> Sl.No Modified Date Author Modified Changes Version </B></U> <br>
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 */
public class morgbusinesscorrbean extends GenericFormBean {

	private static final long serialVersionUID = 1L;

	private String organizationCode;
	private String effectiveDate;
	private boolean enableBusinesCorr;
	private String geoStateCode;
	private String geoDistCode;
	private String countryCode;
	private String geoUnitStructStateCodeHid;
	private String geoUnitStructDistCodeHid;
	private String xmlOrgBusinessCorrGrid;
	private boolean enabled;
	private String remarks;
	private String redirectLandingPagePath;

	public String getOrganizationCode() {
		return organizationCode;
	}

	public void setOrganizationCode(String organizationCode) {
		this.organizationCode = organizationCode;
	}

	public String getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public boolean isEnableBusinesCorr() {
		return enableBusinesCorr;
	}

	public void setEnableBusinesCorr(boolean enableBusinesCorr) {
		this.enableBusinesCorr = enableBusinesCorr;
	}

	public String getGeoStateCode() {
		return geoStateCode;
	}

	public void setGeoStateCode(String geoStateCode) {
		this.geoStateCode = geoStateCode;
	}

	public String getGeoDistCode() {
		return geoDistCode;
	}

	public void setGeoDistCode(String geoDistCode) {
		this.geoDistCode = geoDistCode;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getGeoUnitStructStateCodeHid() {
		return geoUnitStructStateCodeHid;
	}

	public void setGeoUnitStructStateCodeHid(String geoUnitStructStateCodeHid) {
		this.geoUnitStructStateCodeHid = geoUnitStructStateCodeHid;
	}

	public String getGeoUnitStructDistCodeHid() {
		return geoUnitStructDistCodeHid;
	}

	public void setGeoUnitStructDistCodeHid(String geoUnitStructDistCodeHid) {
		this.geoUnitStructDistCodeHid = geoUnitStructDistCodeHid;
	}

	public String getXmlOrgBusinessCorrGrid() {
		return xmlOrgBusinessCorrGrid;
	}

	public void setXmlOrgBusinessCorrGrid(String xmlOrgBusinessCorrGrid) {
		this.xmlOrgBusinessCorrGrid = xmlOrgBusinessCorrGrid;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getRedirectLandingPagePath() {
		return redirectLandingPagePath;
	}

	public void setRedirectLandingPagePath(String redirectLandingPagePath) {
		this.redirectLandingPagePath = redirectLandingPagePath;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void reset() {
		organizationCode = RegularConstants.EMPTY_STRING;
		effectiveDate = RegularConstants.EMPTY_STRING;
		enableBusinesCorr = false;
		geoStateCode = RegularConstants.EMPTY_STRING;
		geoDistCode = RegularConstants.EMPTY_STRING;
		geoUnitStructDistCodeHid = RegularConstants.EMPTY_STRING;
		geoUnitStructDistCodeHid = RegularConstants.EMPTY_STRING;
		countryCode = RegularConstants.EMPTY_STRING;
		xmlOrgBusinessCorrGrid = RegularConstants.EMPTY_STRING;
		enabled = false;
		remarks = RegularConstants.EMPTY_STRING;
		setProcessBO("patterns.config.framework.bo.cmn.morgbusinesscorrBO");
		getCountryStructureDetails();
	}

	public void validate() {
		if (validateOrganizationCode()) {
			if (validateEfftDate()) {
				validateGrid();
				validateRemarks();
			}
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.set("ORG_CODE", organizationCode);
				formDTO.set("EFF_DATE", effectiveDate);
				formDTO.set("ENABLEDASCORRDENT", decodeBooleanToString(enableBusinesCorr));
				if (getAction().equals(ADD)) {
					formDTO.set("ENABLED", decodeBooleanToString(true));
				} else {
					formDTO.set("ENABLED", decodeBooleanToString(enabled));
				}
				formDTO.set("REMARKS", remarks);

				DTDObject dtdObject = new DTDObject();
				dtdObject.addColumn(0, "SL");
				dtdObject.addColumn(1, "COUNTRY_CODE");
				dtdObject.addColumn(2, "GEO_UNIT_STATE_ID");
				dtdObject.addColumn(3, "DESCRIPTION");
				dtdObject.addColumn(4, "GEO_UNIT_DISTRICT_ID");
				dtdObject.addColumn(5, "DESCRIPTION");
				dtdObject.setXML(xmlOrgBusinessCorrGrid);
				formDTO.setDTDObject("ORGBUSINESSCORRHISTDTL", dtdObject);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("organizationCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
	}

	public void getCountryStructureDetails() {
		CommonValidator validation = new CommonValidator();
		DTObject resultDTO = new DTObject();
		try {
			resultDTO.set(ContentManager.CODE, context.getEntityCode());
			resultDTO.set(ContentManager.ACTION, USAGE);
			resultDTO.set(ContentManager.COLUMN, "ENTITY_CODE");
			resultDTO.set(ContentManager.FETCH_COLUMNS, "OUR_COUNTRY_CODE,GEO_UNIT_STRUC_STATE_CODE,GEO_UNIT_STRUC_DIST_CODE");
			resultDTO = validation.validEntityCode(resultDTO);
			countryCode = resultDTO.get("OUR_COUNTRY_CODE");
			geoUnitStructStateCodeHid = resultDTO.get("GEO_UNIT_STRUC_STATE_CODE");
			geoUnitStructDistCodeHid = resultDTO.get("GEO_UNIT_STRUC_DIST_CODE");
			redirectLandingPagePath = MenuUtils.resolveForward("elanding", webContext.getRequest());
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			validation.close();
		}
	}

	public boolean validateOrganizationCode() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("ORG_CODE", organizationCode);
			formDTO.set("ACTION", USAGE);
			formDTO = validateOrganizationCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("organizationCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("organizationCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateOrganizationCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.NULL);
		MasterValidator validation = new MasterValidator();
		String orgCode = inputDTO.get("ORG_CODE");
		try {
			if (validation.isEmpty(orgCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
			resultDTO = validation.validateOrganizationCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateEfftDate() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set(ContentManager.USER_ACTION, getAction());
			formDTO.set("EFF_DATE", effectiveDate);
			formDTO.set("ORG_CODE", organizationCode);
			formDTO = validateEffectiveDate(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("effectiveDate", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("effectiveDate", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateEffectiveDate(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		CommonValidator commonvalidation = new CommonValidator();
		String effectiveDate = inputDTO.get("EFF_DATE");
		String orgCode = inputDTO.get("ORG_CODE");
		String action = inputDTO.get(ContentManager.USER_ACTION);
		try {
			TBAActionType AT;
			if (action.equals(ADD))
				AT = TBAActionType.ADD;
			else
				AT = TBAActionType.MODIFY;
			if (!commonvalidation.isEffectiveDateValid(effectiveDate, AT)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_EFFECTIVE_DATE);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "ORG_CODE,ENABLED");
			inputDTO.set(ContentManager.TABLE, "ORGBUSINESSCORRHIST");
			String columns[] = new String[] { context.getEntityCode(), orgCode, effectiveDate };
			inputDTO.setObject(ContentManager.PROGRAM_KEY, columns);
			resultDTO = commonvalidation.validatePK(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;

			}
			if (action.equals(ADD)) {
				if (ContentManager.DATA_AVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_EXISTS);
					return resultDTO;
				}
			} else if (action.equals(MODIFY)) {
				if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_NOT_EXISTS);
					return resultDTO;
				}
				if (RegularConstants.ZERO.equals(resultDTO.get("ENABLED"))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.CODE_CLOSED);
					return resultDTO;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonvalidation.close();
		}
		return resultDTO;
	}

	private boolean validateGrid() {
		AccessValidator validation = new AccessValidator();
		try {
			DTDObject dtdObject = new DTDObject();
			dtdObject.addColumn(0, "SL");
			dtdObject.addColumn(1, "COUNTRY_CODE");
			dtdObject.addColumn(2, "GEO_UNIT_STATE_ID");
			dtdObject.addColumn(3, "DESCRIPTION");
			dtdObject.addColumn(4, "GEO_UNIT_DISTRICT_ID");
			dtdObject.addColumn(5, "DESCRIPTION");
			dtdObject.setXML(xmlOrgBusinessCorrGrid);
			if (dtdObject.getRowCount() <= 0) {
				getErrorMap().setError("geoStateCode", BackOfficeErrorCodes.ATLEAST_ONE_ROW);
				return false;
			}
			if (!validation.checkDuplicateRows(dtdObject, 2, 4)) {
				getErrorMap().setError("geoStateCode", BackOfficeErrorCodes.DUPLICATE_DATA);
				return false;
			}
			for (int i = 0; i < dtdObject.getRowCount(); i++) {
				if ((!validateGeoStateCode(dtdObject.getValue(i, 1), dtdObject.getValue(i, 2))) || (!validateGeoDistCode(dtdObject.getValue(i, 1), dtdObject.getValue(i, 2), dtdObject.getValue(i, 4)))) {
					getErrorMap().setError("geoStateCode", BackOfficeErrorCodes.INVALID_ACTION);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("geoStateCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateGeoStateCode(String countryCode, String geoStateCode) {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("GEOC_COU_CODE", countryCode);
			formDTO.set("GEO_UNIT_STATE_ID", geoStateCode);
			formDTO.set("GEO_UNIT_STRUC_STATE_CODE", geoUnitStructStateCodeHid);
			formDTO.set("ACTION", USAGE);
			formDTO = validateGeoStateCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("geoStateCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("geoStateCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateGeoStateCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.NULL);
		MasterValidator validation = new MasterValidator();
		String geoStateCode = inputDTO.get("GEO_UNIT_STATE_ID");
		String geoUnitStructStateCodeHid = inputDTO.get("GEO_UNIT_STRUC_STATE_CODE");
		try {
			if (validation.isEmpty(geoStateCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set("GEOC_UNIT_ID", geoStateCode);
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION,GEO_UNIT_STRUC_CODE");
			resultDTO = validation.validateGeographicalUnitId(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
			if (!(geoUnitStructStateCodeHid.equals(resultDTO.get("GEO_UNIT_STRUC_CODE")))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.PBS_GEOUNIT_NOT_BELONGS_STATE);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateGeoDistCode(String countryCode, String geoStateCode, String geoDistCode) {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("GEOC_COU_CODE", countryCode);
			formDTO.set("GEO_UNIT_DISTRICT_ID", geoDistCode);
			formDTO.set("GEO_UNIT_STATE_ID", geoStateCode);
			formDTO.set("GEO_UNIT_STRUC_DIST_CODE", geoUnitStructDistCodeHid);
			formDTO.set("ACTION", USAGE);
			formDTO = validateGeoDistCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("geoStateCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("geoStateCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateGeoDistCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.NULL);
		MasterValidator validation = new MasterValidator();
		String geoStateCode = inputDTO.get("GEO_UNIT_STATE_ID");
		String geoDistCode = inputDTO.get("GEO_UNIT_DISTRICT_ID");
		String geoUnitStructDistCodeHid = inputDTO.get("GEO_UNIT_STRUC_DIST_CODE");
		try {
			if (validation.isEmpty(geoDistCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set("GEOC_UNIT_ID", geoDistCode);
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION,PARENT_GEO_UNIT_ID,GEO_UNIT_STRUC_CODE");
			resultDTO = validation.validateGeographicalUnitId(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
			if (!(geoUnitStructDistCodeHid.equals(resultDTO.get("GEO_UNIT_STRUC_CODE")))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.PBS_GEOUNIT_NOT_BELONGS_DIST);
				return resultDTO;
			}
			if (!(geoStateCode.equals(resultDTO.get("PARENT_GEO_UNIT_ID")))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.PBS_GEOUNIT_NOT_BELONGS_STATE);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateRemarks() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (remarks != null && remarks.length() > 0) {
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			if (getAction().equals(MODIFY)) {
				if (commonValidator.isEmpty(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}
}
