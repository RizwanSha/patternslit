package patterns.config.web.forms.cmn;

import java.util.ArrayList;
import java.util.Date;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeDateUtils;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.framework.web.GenericOption;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.MasterValidator;
import patterns.config.web.forms.GenericFormBean;

public class mentitiesbean extends GenericFormBean {

	private static final long serialVersionUID = 1L;
	private String entityCode;
	private String name;
	private String defaultLanCode;
	private String defaultDateFormat;
	private String dateOfIncorporation;
	private String bankLicenseType;
	private String baseCurrCode;
	private String baseAddrType;
	private String countryCode;
	private String finYear;
	private String weekStartsFrom;
	private boolean hasBranches;
	private boolean hiddenhas;
	private String lengthBranchCode;
	private boolean multiCurAccount;
	private boolean multiCur;
	private String geoUnitStateCode;
	private String geoUnitDistrictCode;
	private String geoUnitPinCode;
	private boolean entityDissolved;
	private String dissolvedOn;
	private String remarks;
	private String w_version;
	private String w_add_struc_reqd;
	private String beforeNoOfDigits;
	private String tempGeoParentCode;
	private String w_country_struc;
	private String fin;
	private String weekStarts;

	public String getEntityCode() {
		return entityCode;
	}

	public void setEntityCode(String entityCode) {
		this.entityCode = entityCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDefaultLanCode() {
		return defaultLanCode;
	}

	public void setDefaultLanCode(String defaultLanCode) {
		this.defaultLanCode = defaultLanCode;
	}

	public String getDefaultDateFormat() {
		return defaultDateFormat;
	}

	public void setDefaultDateFormat(String defaultDateFormat) {
		this.defaultDateFormat = defaultDateFormat;
	}

	public String getDateOfIncorporation() {
		return dateOfIncorporation;
	}

	public void setDateOfIncorporation(String dateOfIncorporation) {
		this.dateOfIncorporation = dateOfIncorporation;
	}

	public String getBankLicenseType() {
		return bankLicenseType;
	}

	public void setBankLicenseType(String bankLicenseType) {
		this.bankLicenseType = bankLicenseType;
	}

	public String getBaseCurrCode() {
		return baseCurrCode;
	}

	public void setBaseCurrCode(String baseCurrCode) {
		this.baseCurrCode = baseCurrCode;
	}

	public String getBaseAddrType() {
		return baseAddrType;
	}

	public void setBaseAddrType(String baseAddrType) {
		this.baseAddrType = baseAddrType;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getW_country_struc() {
		return w_country_struc;
	}

	public void setW_country_struc(String w_country_struc) {
		this.w_country_struc = w_country_struc;
	}

	public String getFinYear() {
		return finYear;
	}

	public void setFinYear(String finYear) {
		this.finYear = finYear;
	}

	public String getWeekStartsFrom() {
		return weekStartsFrom;
	}

	public void setWeekStartsFrom(String weekStartsFrom) {
		this.weekStartsFrom = weekStartsFrom;
	}

	public String getFin() {
		return fin;
	}

	public void setFin(String fin) {
		this.fin = fin;
	}

	public String getWeekStarts() {
		return weekStarts;
	}

	public void setWeekStarts(String weekStarts) {
		this.weekStarts = weekStarts;
	}

	public boolean isHasBranches() {
		return hasBranches;
	}

	public void setHasBranches(boolean hasBranches) {
		this.hasBranches = hasBranches;
	}

	public boolean isHiddenhas() {
		return hiddenhas;
	}

	public void setHiddenhas(boolean hiddenhas) {
		this.hiddenhas = hiddenhas;
	}

	public String getLengthBranchCode() {
		return lengthBranchCode;
	}

	public void setLengthBranchCode(String lengthBranchCode) {
		this.lengthBranchCode = lengthBranchCode;
	}

	public String getBeforeNoOfDigits() {
		return beforeNoOfDigits;
	}

	public void setBeforeNoOfDigits(String beforeNoOfDigits) {
		this.beforeNoOfDigits = beforeNoOfDigits;
	}

	public boolean isMultiCurAccount() {
		return multiCurAccount;
	}

	public void setMultiCurAccount(boolean multiCurAccount) {
		this.multiCurAccount = multiCurAccount;
	}

	public boolean isMultiCur() {
		return multiCur;
	}

	public void setMultiCur(boolean multiCur) {
		this.multiCur = multiCur;
	}

	public String getGeoUnitStateCode() {
		return geoUnitStateCode;
	}

	public void setGeoUnitStateCode(String geoUnitStateCode) {
		this.geoUnitStateCode = geoUnitStateCode;
	}

	public String getGeoUnitDistrictCode() {
		return geoUnitDistrictCode;
	}

	public void setGeoUnitDistrictCode(String geoUnitDistrictCode) {
		this.geoUnitDistrictCode = geoUnitDistrictCode;
	}

	public String getGeoUnitPinCode() {
		return geoUnitPinCode;
	}

	public void setGeoUnitPinCode(String geoUnitPinCode) {
		this.geoUnitPinCode = geoUnitPinCode;
	}

	public boolean isEntityDissolved() {
		return entityDissolved;
	}

	public void setEntityDissolved(boolean entityDissolved) {
		this.entityDissolved = entityDissolved;
	}

	public String getDissolvedOn() {
		return dissolvedOn;
	}

	public void setDissolvedOn(String dissolvedOn) {
		this.dissolvedOn = dissolvedOn;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getW_version() {
		return w_version;
	}

	public void setW_version(String w_version) {
		this.w_version = w_version;
	}

	public String getW_add_struc_reqd() {
		return w_add_struc_reqd;
	}

	public void setW_add_struc_reqd(String w_add_struc_reqd) {
		this.w_add_struc_reqd = w_add_struc_reqd;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void reset() {
		entityCode = RegularConstants.EMPTY_STRING;
		name = RegularConstants.EMPTY_STRING;
		defaultLanCode = RegularConstants.EMPTY_STRING;
		defaultDateFormat = RegularConstants.EMPTY_STRING;
		dateOfIncorporation = RegularConstants.EMPTY_STRING;
		bankLicenseType = RegularConstants.EMPTY_STRING;
		baseCurrCode = RegularConstants.EMPTY_STRING;
		baseAddrType = RegularConstants.EMPTY_STRING;
		countryCode = RegularConstants.EMPTY_STRING;
		finYear = RegularConstants.EMPTY_STRING;
		weekStartsFrom = RegularConstants.EMPTY_STRING;
		lengthBranchCode = RegularConstants.EMPTY_STRING;
		geoUnitStateCode = RegularConstants.EMPTY_STRING;
		geoUnitDistrictCode = RegularConstants.EMPTY_STRING;
		geoUnitPinCode = RegularConstants.EMPTY_STRING;
		dissolvedOn = RegularConstants.EMPTY_STRING;
		beforeNoOfDigits = RegularConstants.EMPTY_STRING;
		tempGeoParentCode = RegularConstants.EMPTY_STRING;
		w_country_struc = RegularConstants.EMPTY_STRING;
		fin = RegularConstants.EMPTY_STRING;
		weekStarts = RegularConstants.EMPTY_STRING;
		hasBranches = false;
		hiddenhas = false;
		multiCurAccount = false;
		multiCur = false;
		entityDissolved = false;
		remarks = RegularConstants.EMPTY_STRING;
		w_add_struc_reqd = RegularConstants.EMPTY_STRING;

		DBContext dbContext = new DBContext();
		ArrayList<GenericOption> finYear = null;
		finYear = getGenericOptions("COMMON", "MONTHS", dbContext, "Select");
		webContext.getRequest().setAttribute("COMMON_MONTHS", finYear);

		ArrayList<GenericOption> weekStartsFrom = null;
		weekStartsFrom = getGenericOptions("COMMON", "WEEK_DAY", dbContext, "Select");
		webContext.getRequest().setAttribute("COMMON_WEEK_DAY", weekStartsFrom);
		dbContext.close();
		setProcessBO("patterns.config.framework.bo.cmn.mentitiesBO");
	}

	public void validate() {
		if (validateEntityCode()) {
			validateName();
			validateDateOfIncorporation();
			validateLengthBranchCode();
			validateBankLicenseType();
			validateBaseCurrCode();
			validateBaseAddrType();
			validateCountryCode();
			validateFinYear();
			validateWeekStartsFrom();
			validateGeoUnitStateCode();
			validateGeoUnitDistrictCode();
			validateGeoUnitPinCode();
			validateRemarks();
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set(ContentManager.ENTITY_CODE, context.getEntityCode());
				formDTO.set("ENTITY_CODE", entityCode);
				formDTO.set("ENTITY_NAME", name);
				formDTO.set("BANK_LIC_TYPES", bankLicenseType);
				formDTO.set("REMARKS", remarks);
				if (w_version.equals(RegularConstants.COLUMN_DISABLE)) {
					formDTO.set("DATE_OF_INCORP", dateOfIncorporation);
					formDTO.set("BASE_CCY_CODE", baseCurrCode);
					formDTO.set("BASE_ADDR_TYPE", baseAddrType);
					formDTO.set("OUR_COUNTRY_CODE", countryCode);
					formDTO.set("FIN_YEAR_START_MONTH", finYear);
					formDTO.set("START_DAY_OF_WEEK", weekStartsFrom);
					formDTO.set("GEO_UNIT_STRUC_STATE_CODE", geoUnitStateCode);
					formDTO.set("GEO_UNIT_STRUC_DIST_CODE", geoUnitDistrictCode);
					formDTO.set("GEO_UNIT_STRUC_PINCODE", geoUnitPinCode);
				} else {
					if (!w_version.equals(RegularConstants.COLUMN_DISABLE)) {
						formDTO.set("DATE_OF_INCORP", dateOfIncorporation);
						if (dateOfIncorporation.equals(RegularConstants.EMPTY_STRING)) {
							formDTO.set("DATE_OF_INCORP", RegularConstants.NULL);
						}
						formDTO.set("BASE_CCY_CODE", baseCurrCode);
						if (baseCurrCode.equals(RegularConstants.EMPTY_STRING)) {
							formDTO.set("BASE_CCY_CODE", RegularConstants.NULL);
						}
						formDTO.set("BASE_ADDR_TYPE", baseAddrType);
						if (baseAddrType.equals(RegularConstants.EMPTY_STRING)) {
							formDTO.set("BASE_ADDR_TYPE", RegularConstants.NULL);
						}
						formDTO.set("OUR_COUNTRY_CODE", countryCode);
						if (countryCode.equals(RegularConstants.EMPTY_STRING)) {
							formDTO.set("OUR_COUNTRY_CODE", RegularConstants.NULL);
						}
						formDTO.set("FIN_YEAR_START_MONTH", fin);
						if (fin.equals(RegularConstants.EMPTY_STRING)) {
							formDTO.set("FIN_YEAR_START_MONTH", fin);
						}
						formDTO.set("START_DAY_OF_WEEK", weekStarts);
						if (weekStarts.equals(RegularConstants.EMPTY_STRING)) {
							formDTO.set("START_DAY_OF_WEEK", weekStarts);
						}
						formDTO.set("GEO_UNIT_STRUC_STATE_CODE", geoUnitStateCode);
						if (geoUnitStateCode.equals(RegularConstants.EMPTY_STRING)) {
							formDTO.set("GEO_UNIT_STRUC_STATE_CODE", RegularConstants.NULL);
						}
						formDTO.set("GEO_UNIT_STRUC_DIST_CODE", geoUnitDistrictCode);
						if (geoUnitDistrictCode.equals(RegularConstants.EMPTY_STRING)) {
							formDTO.set("GEO_UNIT_STRUC_DIST_CODE", RegularConstants.NULL);
						}
						formDTO.set("GEO_UNIT_STRUC_PINCODE", geoUnitPinCode);
						if (geoUnitDistrictCode.equals(RegularConstants.EMPTY_STRING)) {
							formDTO.set("GEO_UNIT_STRUC_PINCODE", RegularConstants.NULL);
						}
					}
				}
				if (w_add_struc_reqd.equals(RegularConstants.COLUMN_ENABLE)) {
					formDTO.set("GEO_UNIT_STRUC_DIST_CODE", geoUnitDistrictCode);
					formDTO.set("GEO_UNIT_STRUC_PINCODE", geoUnitPinCode);
				} else {
					formDTO.set("GEO_UNIT_STRUC_DIST_CODE", RegularConstants.NULL);
					formDTO.set("GEO_UNIT_STRUC_PINCODE", RegularConstants.NULL);
				}
				formDTO.set("MULTI_CCY_ALLOWED", decodeBooleanToString(multiCur));
				formDTO.set("ENTITY_DISSOLVED", decodeBooleanToString(entityDissolved));
				formDTO.set("BRANCH_REQD", decodeBooleanToString(hiddenhas));
				if (hiddenhas) {
					formDTO.set("BRN_OFFICE_NO_OF_DIGITS", lengthBranchCode);
				} else {
					formDTO.set("BRN_OFFICE_NO_OF_DIGITS", RegularConstants.ZERO);
				}
				if (entityDissolved) {
					formDTO.set("ENTITY_DISSOLVED_ON", dissolvedOn);
				} else {
					formDTO.set("ENTITY_DISSOLVED_ON", RegularConstants.NULL);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("entityCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}

	}

	public boolean validateEntityCode() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("ENTITY_CODE", entityCode);
			formDTO.set(ContentManager.ACTION, getAction());
			formDTO = validateEntityCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("entityCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("entityCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateEntityCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		MasterValidator validation = new MasterValidator();
		String entityCode = inputDTO.get("ENTITY_CODE");
		String action = inputDTO.get(ContentManager.USER_ACTION);
		try {
			if (validation.isEmpty(entityCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.CODE, entityCode);
			inputDTO.set(ContentManager.COLUMN, "ENTITY_CODE");
			inputDTO.set(ContentManager.FETCH_COLUMNS, "ENTITY_NAME,DATA_VERSION,BRN_OFFICE_NO_OF_DIGITS");
			resultDTO = validation.validEntityCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			w_version = resultDTO.get("DATA_VERSION");
			beforeNoOfDigits = resultDTO.get("BRN_OFFICE_NO_OF_DIGITS");
			if (action.equals(MODIFY)) {
				if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_NOT_EXISTS);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateName() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(name)) {
				getErrorMap().setError("name", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidDescription(name)) {
				getErrorMap().setError("name", BackOfficeErrorCodes.INVALID_DESCRIPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("name", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateDateOfIncorporation() {
		MasterValidator validation = new MasterValidator();
		try {
			if (w_version.equals(RegularConstants.COLUMN_DISABLE)) {
				if (validation.isEmpty(dateOfIncorporation)) {
					getErrorMap().setError("dateOfIncorporation", BackOfficeErrorCodes.HMS_MANDATORY);
					return false;
				}
				java.util.Date cbdDate = context.getCurrentBusinessDate();
				Date date = new java.sql.Date(BackOfficeFormatUtils.getDate(dateOfIncorporation, BackOfficeConstants.JAVA_DATE_FORMAT).getTime());
				if (BackOfficeDateUtils.isDateGreaterThan(date, cbdDate)) {
					getErrorMap().setError("dateOfIncorporation", BackOfficeErrorCodes.DATE_LCBD);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("dateOfIncorporation", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateLengthBranchCode() {
		MasterValidator validation = new MasterValidator();
		try {
			if (hiddenhas) {
				if (w_version.equals(RegularConstants.COLUMN_DISABLE)) {
					if (validation.isEmpty(lengthBranchCode)) {
						getErrorMap().setError("lengthBranchCode", BackOfficeErrorCodes.HMS_MANDATORY);
						return false;
					}
					if (!validation.isValidPositiveNumber(lengthBranchCode)) {
						getErrorMap().setError("lengthBranchCode", BackOfficeErrorCodes.HMS_POSITIVE_AMOUNT);
						return false;
					}
					if (validation.isZero(lengthBranchCode)) {
						getErrorMap().setError("lengthBranchCode", BackOfficeErrorCodes.HMS_ZERO_AMOUNT);
						return false;
					}
					if (Integer.parseInt(lengthBranchCode) > 12) {
						getErrorMap().setError("lengthBranchCode", BackOfficeErrorCodes.BRN_CODE_LS);
						return false;
					}
				} else {
					if (validation.isEmpty(lengthBranchCode)) {
						getErrorMap().setError("lengthBranchCode", BackOfficeErrorCodes.HMS_MANDATORY);
						return false;
					}
					if (!validation.isValidPositiveNumber(lengthBranchCode)) {
						getErrorMap().setError("lengthBranchCode", BackOfficeErrorCodes.HMS_POSITIVE_AMOUNT);
						return false;
					}
					if (validation.isZero(lengthBranchCode)) {
						getErrorMap().setError("lengthBranchCode", BackOfficeErrorCodes.HMS_ZERO_AMOUNT);
						return false;
					}
					if (Integer.parseInt(lengthBranchCode) < Integer.parseInt(beforeNoOfDigits)) {
						getErrorMap().setError("lengthBranchCode", BackOfficeErrorCodes.BRN_CODE_LEN_CANNOT_REDUCE);
						return false;
					}
					if (Integer.parseInt(lengthBranchCode) > 12) {
						getErrorMap().setError("lengthBranchCode", BackOfficeErrorCodes.BRN_CODE_LS);
						return false;
					}
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("lengthBranchCode", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateBankLicenseType() {
		try {
			DTObject formDTO = getFormDTO();
			formDTO.set("BANK_LIC_TYPES", bankLicenseType);
			formDTO.set(ContentManager.USER_ACTION, getAction());
			formDTO = validateBankLicenseType(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("bankLicenseType", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("bankLicenseType", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateBankLicenseType(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		MasterValidator validation = new MasterValidator();
		bankLicenseType = inputDTO.get("BANK_LIC_TYPES");
		try {
			if (validation.isEmpty(bankLicenseType)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_MANDATORY);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
			resultDTO = validation.validateBankLicenseType(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_CODE);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateBaseCurrCode() {
		try {
			if (Integer.parseInt(w_version) == 0)
				if (w_version.equals(RegularConstants.COLUMN_DISABLE)) {
					DTObject formDTO = getFormDTO();
					formDTO.set("CCY_CODE", baseCurrCode);
					formDTO.set("W_VERSION", w_version);
					formDTO.set(ContentManager.USER_ACTION, getAction());
					formDTO = validateBaseCurrCode(formDTO);
					if (formDTO.get(ContentManager.ERROR) != null) {
						getErrorMap().setError("baseCurrCode", formDTO.get(ContentManager.ERROR));
						return false;
					}
					return true;
				}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("baseCurrCode", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateBaseCurrCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		MasterValidator validation = new MasterValidator();
		baseCurrCode = inputDTO.get("CCY_CODE");
		w_version = inputDTO.get("W_VERSION");
		try {
			if (w_version.equals(RegularConstants.COLUMN_DISABLE)) {
				if (validation.isEmpty(baseCurrCode)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_MANDATORY);
					return resultDTO;
				}
				inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
				resultDTO = validation.validateCurrency(inputDTO);
				if (resultDTO.get(ContentManager.ERROR) != null) {
					resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
					return resultDTO;
				}
				if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_CODE);
					return resultDTO;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateBaseAddrType() {
		try {
			if (w_version.equals(RegularConstants.COLUMN_DISABLE)) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ADDR_TYPE", baseAddrType);
				formDTO.set("W_VERSION", w_version);
				formDTO.set(ContentManager.USER_ACTION, getAction());
				formDTO = validateBaseAddrType(formDTO);
				if (formDTO.get(ContentManager.ERROR) != null) {
					getErrorMap().setError("baseAddrType", formDTO.get(ContentManager.ERROR));
					return false;
				}
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("baseAddrType", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateBaseAddrType(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		MasterValidator validation = new MasterValidator();
		baseAddrType = inputDTO.get("ADDR_TYPE");
		w_version = inputDTO.get("W_VERSION");
		try {
			if (w_version.equals(RegularConstants.COLUMN_DISABLE)) {
				if (validation.isEmpty(baseAddrType)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_MANDATORY);
					return resultDTO;
				}
				inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
				resultDTO = validation.validateBaseAddrType(inputDTO);
				if (resultDTO.get(ContentManager.ERROR) != null) {
					resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
					return resultDTO;
				}
				if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_CODE);
					return resultDTO;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateCountryCode() {
		try {
			if (w_version.equals(RegularConstants.COLUMN_DISABLE)) {
				DTObject formDTO = getFormDTO();
				formDTO.set("COUNTRY_CODE", countryCode);
				formDTO.set("W_VERSION", w_version);
				formDTO.set(ContentManager.USER_ACTION, getAction());
				formDTO = validateCountryCode(formDTO);
				if (formDTO.get(ContentManager.ERROR) != null) {
					getErrorMap().setError("countryCode", formDTO.get(ContentManager.ERROR));
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("countryCode", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateCountryCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		MasterValidator validation = new MasterValidator();
		countryCode = inputDTO.get("COUNTRY_CODE");
		w_version = inputDTO.get("W_VERSION");
		try {
			if (w_version.equals(RegularConstants.COLUMN_DISABLE)) {
				if (validation.isEmpty(countryCode)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_MANDATORY);
					return resultDTO;
				}
				inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION,GEO_UNIT_STRUC_REQ,GEO_UNIT_STRUC_CODE");
				resultDTO = validation.validateCountryCode(inputDTO);
				if (resultDTO.get(ContentManager.ERROR) != null) {
					resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
					return resultDTO;
				}
				if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_CODE);
					return resultDTO;
				}
				w_add_struc_reqd = resultDTO.get("GEO_UNIT_STRUC_REQ");
				w_country_struc = resultDTO.get("GEO_UNIT_STRUC_CODE");
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateFinYear() {
		CommonValidator validation = new CommonValidator();
		try {
			if (w_version.equals(RegularConstants.COLUMN_DISABLE)) {
				if (validation.isEmpty(finYear)) {
					getErrorMap().setError("finYear", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!validation.isCMLOVREC("COMMON", "MONTHS", finYear)) {
					getErrorMap().setError("finYear", BackOfficeErrorCodes.INVALID_OPTION);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("finYear", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateWeekStartsFrom() {
		CommonValidator validation = new CommonValidator();
		try {
			if (w_version.equals(RegularConstants.COLUMN_DISABLE)) {
				if (validation.isEmpty(weekStartsFrom)) {
					getErrorMap().setError("weekStartsFrom", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!validation.isCMLOVREC("COMMON", "WEEK_DAY", weekStartsFrom)) {
					getErrorMap().setError("weekStartsFrom", BackOfficeErrorCodes.INVALID_OPTION);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("weekStartsFrom", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateGeoUnitStateCode() {
		try {
			if (w_version.equals(RegularConstants.COLUMN_DISABLE)) {
				DTObject formDTO = getFormDTO();
				formDTO.set("GEO_UNIT_STRUC_CODE", geoUnitStateCode);
				formDTO.set("W_VERSION", w_version);
				formDTO.set("GEO_UNIT_STRUC_REQ", w_add_struc_reqd);
				formDTO.set(ContentManager.USER_ACTION, getAction());
				formDTO = validateGeoUnitStateCode(formDTO);
				if (formDTO.get(ContentManager.ERROR) != null) {
					getErrorMap().setError("geoUnitStateCode", formDTO.get(ContentManager.ERROR));
					return false;
				}
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("geoUnitStateCode", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateGeoUnitStateCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		MasterValidator validation = new MasterValidator();
		geoUnitStateCode = inputDTO.get("GEO_UNIT_STRUC_CODE");
		w_version = inputDTO.get("W_VERSION");
		w_add_struc_reqd = inputDTO.get("GEO_UNIT_STRUC_REQ");
		try {
			if (w_version.equals(RegularConstants.COLUMN_DISABLE)) {
				if (validation.isEmpty(geoUnitStateCode)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_MANDATORY);
					return resultDTO;
				}
				inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
				inputDTO.set("GEO_UNIT_STRUC_CODE", geoUnitStateCode);
				resultDTO = validation.validateGeographicalUnitStructureCode(inputDTO);
				if (resultDTO.get(ContentManager.ERROR) != null) {
					resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
					return resultDTO;
				}
				if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_CODE);
					return resultDTO;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateGeoUnitDistrictCode() {
		try {
			if (w_version.equals(RegularConstants.COLUMN_DISABLE)) {
				if (w_add_struc_reqd.equals(RegularConstants.COLUMN_ENABLE)) {
					DTObject formDTO = getFormDTO();
					formDTO.set("GEO_UNIT", geoUnitDistrictCode);
					formDTO.set("W_CONTRY_STRUC", w_country_struc);
					formDTO.set("W_VERSION", w_version);
					formDTO.set("GEO_UNIT_STRUC_REQ", w_add_struc_reqd);
					formDTO.set(ContentManager.USER_ACTION, getAction());
					formDTO = validateGeoUnitDistrictCode(formDTO);
					if (formDTO.get(ContentManager.ERROR) != null) {
						getErrorMap().setError("geoUnitDistrictCode", formDTO.get(ContentManager.ERROR));
						return false;
					}
					return true;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("geoUnitDistrictCode", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateGeoUnitDistrictCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		MasterValidator validation = new MasterValidator();
		geoUnitDistrictCode = inputDTO.get("GEO_UNIT");
		w_country_struc = inputDTO.get("W_CONTRY_STRUC");
		w_version = inputDTO.get("W_VERSION");
		w_add_struc_reqd = inputDTO.get("GEO_UNIT_STRUC_REQ");
		try {
			if (w_version.equals(RegularConstants.COLUMN_DISABLE)) {
				if (w_add_struc_reqd.equals(RegularConstants.COLUMN_ENABLE)) {
					if (validation.isEmpty(geoUnitDistrictCode)) {
						resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_MANDATORY);
						return resultDTO;
					}
					inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION,PARENT_GUS_CODE");
					inputDTO.set("GEO_UNIT_STRUC_CODE", geoUnitDistrictCode);
					resultDTO = validation.validateGeographicalUnitStructureCode(inputDTO);
					if (resultDTO.get(ContentManager.ERROR) != null) {
						resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
						return resultDTO;
					}
					if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
						resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_CODE);
						return resultDTO;
					}
					tempGeoParentCode = resultDTO.get("PARENT_GUS_CODE");
					if (tempGeoParentCode.equals(w_country_struc)) {
						resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_PGUS_SAS_GUSP);
						return resultDTO;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateGeoUnitPinCode() {
		try {
			if (w_version.equals(RegularConstants.COLUMN_DISABLE)) {
				if (w_add_struc_reqd.equals(RegularConstants.COLUMN_ENABLE)) {
					DTObject formDTO = getFormDTO();
					formDTO.set("GEO_UNIT_STRUC", geoUnitPinCode);
					formDTO.set("W_CONTRY_STRUC", w_country_struc);
					formDTO.set("W_VERSION", w_version);
					formDTO.set("GEO_UNIT_STRUC_REQ", w_add_struc_reqd);
					formDTO.set(ContentManager.USER_ACTION, getAction());
					formDTO = validateGeoUnitPinCode(formDTO);
					if (formDTO.get(ContentManager.ERROR) != null) {
						getErrorMap().setError("geoUnitPinCode", formDTO.get(ContentManager.ERROR));
						return false;
					}
					return true;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("geoUnitPinCode", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateGeoUnitPinCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		MasterValidator validation = new MasterValidator();
		geoUnitPinCode = inputDTO.get("GEO_UNIT_STRUC");
		w_country_struc = inputDTO.get("W_CONTRY_STRUC");
		w_version = inputDTO.get("W_VERSION");
		w_add_struc_reqd = inputDTO.get("GEO_UNIT_STRUC_REQ");
		try {
			if (w_version.equals(RegularConstants.COLUMN_DISABLE)) {
				if (w_add_struc_reqd.equals(RegularConstants.COLUMN_ENABLE)) {
					if (validation.isEmpty(geoUnitPinCode)) {
						resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_MANDATORY);
						return resultDTO;
					}
					inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION,PARENT_GUS_CODE,LAST_IN_HIERARCHY");
					inputDTO.set("GEO_UNIT_STRUC_CODE", geoUnitPinCode);
					resultDTO = validation.validateGeographicalUnitStructureCode(inputDTO);
					if (resultDTO.get(ContentManager.ERROR) != null) {
						resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
						return resultDTO;
					}
					if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
						resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_CODE);
						return resultDTO;
					}
					tempGeoParentCode = resultDTO.get("PARENT_GUS_CODE");
					if (tempGeoParentCode.equals(w_country_struc)) {
						resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.CODE_NOT_CHILD);
						return resultDTO;
					}
					if (!resultDTO.get("LAST_IN_HIERARCHY").equals(RegularConstants.COLUMN_ENABLE)) {
						resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.ONLY_LAST_IN_HIER);
						return resultDTO;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateRemarks() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (w_version.equals(RegularConstants.COLUMN_DISABLE)) {
				if (commonValidator.isEmpty(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}

}
