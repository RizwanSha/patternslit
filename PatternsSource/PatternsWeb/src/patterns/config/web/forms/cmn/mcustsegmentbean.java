package patterns.config.web.forms.cmn;

import java.sql.ResultSet;
import java.util.ArrayList;
/*
 * The program will be used to create and maintain customer segment  . 
 *
 Author : Pavan kumar.R
 Created Date : 28-June-2016
 Spec Reference PPBS/CMN/0003-MCUSTSEGMENT
 Modification History
 ----------------------------------------------------------------------
 Sl.No		Modified Date	Author			Modified Changes	Version
 ----------------------------------------------------------------------		
 */

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.GenericOption;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.MasterValidator;
import patterns.config.web.forms.GenericFormBean;

public class mcustsegmentbean extends GenericFormBean {

	private static final long serialVersionUID = -6219657363150466160L;

	private String customerSegmentCode;
	private String description;
	private String conciseDescription;
	private String customerSegmentAlphaCode;
	private boolean rootLevelSegment;
	private String parentSegmentCode;
	private String individualOrInstitutionalSegment;
	private boolean prioritySectorSegment;
	private boolean seniorCitizenSegment;
	private boolean womenSegment;
	private boolean enabled;
	private String remarks;
	private String checkBoxSatus;
	private boolean forStaff;
	private boolean forForeign;
	private boolean forNonResidents;
	private String nonResidentType;

	public String getNonResidentType() {
		return nonResidentType;
	}

	public void setNonResidentType(String nonResidentType) {
		this.nonResidentType = nonResidentType;
	}

	public boolean isForStaff() {
		return forStaff;
	}

	public void setForStaff(boolean forStaff) {
		this.forStaff = forStaff;
	}

	public boolean isForForeign() {
		return forForeign;
	}

	public void setForForeign(boolean forForeign) {
		this.forForeign = forForeign;
	}

	public boolean isForNonResidents() {
		return forNonResidents;
	}

	public void setForNonResidents(boolean forNonResidents) {
		this.forNonResidents = forNonResidents;
	}

	public String getCheckBoxSatus() {
		return checkBoxSatus;
	}

	public void setCheckBoxSatus(String checkBoxSatus) {
		this.checkBoxSatus = checkBoxSatus;
	}

	public String getCustomerSegmentCode() {
		return customerSegmentCode;
	}

	public void setCustomerSegmentCode(String customerSegmentCode) {
		this.customerSegmentCode = customerSegmentCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getConciseDescription() {
		return conciseDescription;
	}

	public void setConciseDescription(String conciseDescription) {
		this.conciseDescription = conciseDescription;
	}

	public String getCustomerSegmentAlphaCode() {
		return customerSegmentAlphaCode;
	}

	public void setCustomerSegmentAlphaCode(String customerSegmentAlphaCode) {
		this.customerSegmentAlphaCode = customerSegmentAlphaCode;
	}

	public boolean isRootLevelSegment() {
		return rootLevelSegment;
	}

	public void setRootLevelSegment(boolean rootLevelSegment) {
		this.rootLevelSegment = rootLevelSegment;
	}

	public String getParentSegmentCode() {
		return parentSegmentCode;
	}

	public void setParentSegmentCode(String parentSegmentCode) {
		this.parentSegmentCode = parentSegmentCode;
	}

	public String getIndividualOrInstitutionalSegment() {
		return individualOrInstitutionalSegment;
	}

	public void setIndividualOrInstitutionalSegment(String individualOrInstitutionalSegment) {
		this.individualOrInstitutionalSegment = individualOrInstitutionalSegment;
	}

	public boolean isPrioritySectorSegment() {
		return prioritySectorSegment;
	}

	public void setPrioritySectorSegment(boolean prioritySectorSegment) {
		this.prioritySectorSegment = prioritySectorSegment;
	}

	public boolean isSeniorCitizenSegment() {
		return seniorCitizenSegment;
	}

	public void setSeniorCitizenSegment(boolean seniorCitizenSegment) {
		this.seniorCitizenSegment = seniorCitizenSegment;
	}

	public boolean isWomenSegment() {
		return womenSegment;
	}

	public void setWomenSegment(boolean womenSegment) {
		this.womenSegment = womenSegment;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void reset() {
		customerSegmentCode = RegularConstants.EMPTY_STRING;
		description = RegularConstants.EMPTY_STRING;
		conciseDescription = RegularConstants.EMPTY_STRING;
		customerSegmentAlphaCode = RegularConstants.EMPTY_STRING;
		rootLevelSegment = false;
		parentSegmentCode = RegularConstants.EMPTY_STRING;
		individualOrInstitutionalSegment = RegularConstants.EMPTY_STRING;
		prioritySectorSegment = false;
		seniorCitizenSegment = false;
		womenSegment = false;
		forStaff = false;
		forForeign = false;
		forNonResidents = false;
		enabled = false;
		remarks = RegularConstants.EMPTY_STRING;
		DBContext dbContext = new DBContext();
		ArrayList<GenericOption> indivInstitSegment = null;
		indivInstitSegment = getGenericOptions("COMMON", "INDIV_INSTIT_SEGMENT", dbContext, "--");
		webContext.getRequest().setAttribute("COMMON_INDIV_INSTIT_SEGMENT", indivInstitSegment);

		ArrayList<GenericOption> nonResidenType = null;
		nonResidenType = getGenericOptions("COMMON", "NON_RESIDENT_TYPE", dbContext, "--");
		webContext.getRequest().setAttribute("COMMON_NON_RESIDENT_TYPE", nonResidenType);

		dbContext.close();
		setProcessBO("patterns.config.framework.bo.cmn.mcustsegmentBO");
	}

	@Override
	public void validate() {
		CommonValidator validation = new CommonValidator();
		if (validateCustomerSegmentCode()) {
			validateDescription();
			validateConciseDescription();
			validateCustomerSegmentAlphaCode();
			validateParentSegmentCode();
			validateIndividualOrInstitutionalSegment();
			validateprioritySectorSegment();
			validateseniorCitizenSegment();
			validateisWomenSegment();
			validateForStaff();
			validateforForeign();
			// validateCheckBoxValues();
			validateNonResidenType();
			/*
			 * if (!getAction().equals(ADD)) validateEnabled();
			 */
			validateRemarks();

		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.set("CUST_SEGMENT_CODE", customerSegmentCode);
				formDTO.set("DESCRIPTION", description);
				formDTO.set("CONCISE_DESCRIPTION", conciseDescription);
				if (validation.isEmpty(customerSegmentAlphaCode))
					formDTO.set("CUST_SEGEMNT_ALPHA_CODE", RegularConstants.NULL);
				else
					formDTO.set("CUST_SEGEMNT_ALPHA_CODE", customerSegmentAlphaCode);
				formDTO.set("ROOT_LEVEL_SEGMENT", decodeBooleanToString(rootLevelSegment));
				if (parentSegmentCode.isEmpty())
					formDTO.set("PARENT_SEGMENT_CODE", RegularConstants.NULL);
				else
					formDTO.set("PARENT_SEGMENT_CODE", parentSegmentCode);
				formDTO.set("INDIVID_INSTIT_SEGMENT", individualOrInstitutionalSegment);
				formDTO.set("PRIORTY_SECTOR_SEGMENT", decodeBooleanToString(prioritySectorSegment));
				formDTO.set("SENIOR_CITIZEN_SEGMENT", decodeBooleanToString(seniorCitizenSegment));
				formDTO.set("WOMEN_SEGMENT", decodeBooleanToString(womenSegment));
				formDTO.set("FOR_STAFF", decodeBooleanToString(forStaff));
				formDTO.set("FOR_FOREIGN_NATIONAL", decodeBooleanToString(forForeign));
				formDTO.set("FOR_NON_RESI", decodeBooleanToString(forNonResidents));
				formDTO.set("NON_RESI_TYPE", nonResidentType);
				if (getAction().equals(ADD)) {
					formDTO.set("ENABLED", decodeBooleanToString(true));
				} else {
					formDTO.set("ENABLED", decodeBooleanToString(enabled));
				}

				formDTO.set("REMARKS", remarks);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("customerSegmentCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
	}

	public boolean validateCustomerSegmentCode() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("CUST_SEGMENT_CODE", customerSegmentCode);
			formDTO.set("ACTION", getAction());
			formDTO = validateCustomerSegmentCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("customerSegmentCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("customerSegmentCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateCustomerSegmentCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		MasterValidator validation = new MasterValidator();
		String customerSegment = inputDTO.get("CUST_SEGMENT_CODE");
		String action = inputDTO.get(ContentManager.USER_ACTION);
		try {
			if (validation.isEmpty(customerSegment)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			if (!validation.isValidNumber(customerSegment)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_NUMBER);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
			resultDTO = validation.validateCustomerSegmentCode(inputDTO);
			if (action.equals(ADD)) {
				if (ContentManager.DATA_AVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_EXISTS);
				}
			} else {
				if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_NOT_EXISTS);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateDescription() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(description)) {
				getErrorMap().setError("description", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidDescription(description)) {
				getErrorMap().setError("description", BackOfficeErrorCodes.INVALID_DESCRIPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("description", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;

	}

	public boolean validateConciseDescription() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(conciseDescription)) {
				getErrorMap().setError("conciseDescription", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidConciseDescription(conciseDescription)) {
				getErrorMap().setError("conciseDescription", BackOfficeErrorCodes.INVALID_CONCISE_DESCRIPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("conciseDescription", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateCustomerSegmentAlphaCode() {
		CommonValidator validation = new CommonValidator();
		try {
			if (!validation.isEmpty(customerSegmentAlphaCode)) {
				if (!validation.isValidCode(customerSegmentAlphaCode)) {
					getErrorMap().setError("customerSegmentAlphaCode", BackOfficeErrorCodes.INVALID_VALUE);
					return false;
				} else if (customerSegmentAlphaCode.trim().equals(customerSegmentCode.trim())) {
					getErrorMap().setError("customerSegmentAlphaCode", BackOfficeErrorCodes.PBS_ALPHA_CODE_NEQUAL);
					return false;
				}
				DTObject formDTO = new DTObject();
				formDTO.set("CUST_SEGMENT_CODE", customerSegmentCode);
				formDTO.set("CUST_SEGEMNT_ALPHA_CODE", customerSegmentAlphaCode);
				formDTO.set("PROGRAM_ID", context.getProcessID());
				formDTO.set(ContentManager.USER_ACTION, USAGE);
				formDTO = validateCustomerSegmentAlphaCode(formDTO);
				if (formDTO.get(ContentManager.ERROR) != null) {
					getErrorMap().setError("customerSegmentAlphaCode", formDTO.get(ContentManager.ERROR), formDTO.getObjectL(ContentManager.ERROR_PARAM));
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("customerSegmentAlphaCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public DTObject validateCustomerSegmentAlphaCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		MasterValidator validation = new MasterValidator();
		try {
			String primaryKey = context.getEntityCode() + RegularConstants.PK_SEPARATOR + inputDTO.get("CUST_SEGMENT_CODE");
			String dedupKey = context.getEntityCode() + RegularConstants.PK_SEPARATOR + inputDTO.get("CUST_SEGEMNT_ALPHA_CODE");
			DTObject formDTO = new DTObject();
			formDTO.set(ContentManager.PROGRAM_ID, inputDTO.get("PROGRAM_ID"));
			formDTO.set(ContentManager.PURPOSE_ID, "CUST_SEGEMNT_ALPHA_CODE");
			formDTO.set(ContentManager.PROGRAM_KEY, primaryKey);
			formDTO.set(ContentManager.DEDUP_KEY, dedupKey);
			resultDTO = validation.isDeDupAlreadyExist(formDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
				Object[] errorParam = { resultDTO.getObject("LINKED_TO_PK") };
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.PBS_CUST_ALPHA_CODE_ALREADY_EXIST_SEGCODE);
				resultDTO.setObject(ContentManager.ERROR_PARAM, errorParam);
				return resultDTO;
			}
			DTObject formDTOOFCusSegCode = new DTObject();
			String dedupKeyCusSegCode = context.getEntityCode() + RegularConstants.PK_SEPARATOR + inputDTO.get("CUST_SEGEMNT_ALPHA_CODE");
			formDTOOFCusSegCode.set(ContentManager.PROGRAM_ID, inputDTO.get("PROGRAM_ID"));
			formDTOOFCusSegCode.set(ContentManager.PURPOSE_ID, "CUST_SEGMENT_CODE");
			formDTOOFCusSegCode.set(ContentManager.PROGRAM_KEY, primaryKey);
			formDTOOFCusSegCode.set(ContentManager.DEDUP_KEY, dedupKeyCusSegCode);
			formDTOOFCusSegCode.set("CUST_SEGEMNT_ALPHA_CODE", inputDTO.get("CUST_SEGEMNT_ALPHA_CODE"));
			resultDTO = validateCustomerSegmentCodeDeDup(formDTOOFCusSegCode);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
				Object[] errorParam = { resultDTO.getObject("LINKED_TO_PK") };
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.PBS_CUST_ALPHA_CODE_ALREADY_EXIST_SEGCODE);
				resultDTO.setObject(ContentManager.ERROR_PARAM, errorParam);
				return resultDTO;
			}

		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public DTObject validateCustomerSegmentCodeDeDup(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBContext dbContext = new DBContext();
		DBUtil dbutil = dbContext.createUtilInstance();
		String sqlQueryMainTable = "SELECT ENTITY_CODE,CUST_SEGMENT_CODE FROM CUSTSEGMENT WHERE ENTITY_CODE=? AND CAST(CUST_SEGMENT_CODE AS CHAR) = ?";
		String sqlQueryTBADEDUPCHECK = RegularConstants.EMPTY_STRING;
		try {
			dbutil.reset();
			dbutil.setMode(DBUtil.PREPARED);
			dbutil.setSql(sqlQueryMainTable.toString());
			dbutil.setString(1, context.getEntityCode());
			dbutil.setString(2, inputDTO.get("CUST_SEGEMNT_ALPHA_CODE"));
			ResultSet rset = dbutil.executeQuery();
			if (rset.next()) {
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				resultDTO.set("LINKED_TO_PK", rset.getString("ENTITY_CODE") + "|" + rset.getString("CUST_SEGMENT_CODE"));
				return resultDTO;
			} else {
				sqlQueryTBADEDUPCHECK = "SELECT PGM_PK FROM TBADEDUPCHECK WHERE PARTITION_NO=? AND PGM_ID=? AND PURPOSE_ID=? AND PGM_PK<>? AND COLUMN_VALUES=?";
				dbutil.reset();
				dbutil.setSql(sqlQueryTBADEDUPCHECK);
				dbutil.setInt(1, Integer.parseInt(context.getPartitionNo()));
				dbutil.setString(2, inputDTO.get(ContentManager.PROGRAM_ID));
				dbutil.setString(3, inputDTO.get(ContentManager.PURPOSE_ID));
				dbutil.setString(4, inputDTO.get(ContentManager.PROGRAM_KEY));
				dbutil.setString(5, inputDTO.get(ContentManager.DEDUP_KEY));
				ResultSet rsetTba = dbutil.executeQuery();
				if (rsetTba.next()) {
					resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
					resultDTO.set("LINKED_TO_PK", rsetTba.getString("PGM_PK"));
				}
			}
			return resultDTO;
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			dbContext.close();
		}
		return resultDTO;

	}

	public boolean validateParentSegmentCode() {
		MasterValidator validation = new MasterValidator();
		try {
			if (!rootLevelSegment) {
				if (validation.isEmpty(parentSegmentCode)) {
					getErrorMap().setError("parentSegmentCode", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (customerSegmentCode.trim().equals(parentSegmentCode.trim())) {
					getErrorMap().setError("parentSegmentCode", BackOfficeErrorCodes.PBS_SEGMENT_CODE_NEQUAL);
					return false;
				}
				DTObject formDTO = new DTObject();
				formDTO.set("CUST_SEGMENT_CODE", parentSegmentCode);
				formDTO.set("CURRENT_CUST_SEGMENT_CODE", customerSegmentCode);
				formDTO.set(ContentManager.USER_ACTION, USAGE);
				formDTO = validateParentSegmentCode(formDTO);
				if (formDTO.get(ContentManager.ERROR) != null) {
					getErrorMap().setError("parentSegmentCode", formDTO.get(ContentManager.ERROR));
					return false;
				}
			} else {
				if (!validation.isEmpty(parentSegmentCode)) {
					getErrorMap().setError("parentSegmentCode", BackOfficeErrorCodes.PBS_NOT_REQ_FOR_PARENT_SEGMENT);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("parentSegmentCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}

		return false;
	}

	public DTObject validateParentSegmentCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		MasterValidator validation = new MasterValidator();
		String parentSegment = inputDTO.get("CUST_SEGMENT_CODE");
		try {
			if (!validation.isEmpty(parentSegment)) {
				if (!validation.isValidNumber(parentSegment)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_NUMBER);
					return resultDTO;
				}
				inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION,PARENT_SEGMENT_CODE");
				resultDTO = validation.validateCustomerSegmentCode(inputDTO);
				if (resultDTO.get(ContentManager.ERROR) != null) {
					resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
					return resultDTO;
				}
				if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
					return resultDTO;
				}
				if (!validation.isEmpty(resultDTO.get("PARENT_SEGMENT_CODE")) && (resultDTO.get("PARENT_SEGMENT_CODE").equals(inputDTO.get("CURRENT_CUST_SEGMENT_CODE")))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.PBS_CANNOT_SPEC_AS_PARENT);
					return resultDTO;

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	private boolean validateIndividualOrInstitutionalSegment() {
		CommonValidator validation = new CommonValidator();
		try {
			if (rootLevelSegment && !validation.isCMLOVREC("COMMON", "INDIV_INSTIT_SEGMENT", individualOrInstitutionalSegment)) {
				getErrorMap().setError("individualOrInstitutionalSegment", BackOfficeErrorCodes.INVALID_OPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("individualOrInstitutionalSegment", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	private boolean validateprioritySectorSegment() {
		CommonValidator validation = new CommonValidator();
		try {
			if (!rootLevelSegment) {
				if ((prioritySectorSegment)) {
					getErrorMap().setError("prioritySectorSegment", BackOfficeErrorCodes.INVALID_CHECK);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("individualOrInstitutionalSegment", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	private boolean validateseniorCitizenSegment() {
		CommonValidator validation = new CommonValidator();
		try {
			if (!rootLevelSegment) {
				if (seniorCitizenSegment) {
					getErrorMap().setError("seniorCitizenSegment", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("seniorCitizenSegment", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	private boolean validateisWomenSegment() {
		CommonValidator validation = new CommonValidator();
		try {
			if (!rootLevelSegment) {
				if (womenSegment) {
					getErrorMap().setError("womenSegment", BackOfficeErrorCodes.INVALID_CHECK);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("womenSegment", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	private boolean validateForStaff() {
		CommonValidator validation = new CommonValidator();
		try {
			if (!rootLevelSegment) {
				if (forStaff) {
					getErrorMap().setError("forStaff", BackOfficeErrorCodes.INVALID_CHECK);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("forStaff", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	private boolean validateforForeign() {
		CommonValidator validation = new CommonValidator();
		try {
			if (rootLevelSegment && forForeign) {
				if (forNonResidents) {
					getErrorMap().setError("forForeign", BackOfficeErrorCodes.INVALID_CHECK);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("forForeign", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	private boolean validateNonResidenType() {
		CommonValidator validation = new CommonValidator();
		try {
			if (forNonResidents) {
				if (validation.isEmpty(nonResidentType)) {
					getErrorMap().setError("nonResidentType", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!validation.isCMLOVREC("COMMON", "NON_RESIDENT_TYPE", nonResidentType)) {
					getErrorMap().setError("nonResidentType", BackOfficeErrorCodes.INVALID_OPTION);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("nonResidentType", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateRemarks() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (remarks != null && remarks.length() > 0) {
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			if (getAction().equals(MODIFY)) {
				if (commonValidator.isEmpty(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}

	/*
	 * public boolean validateEnabled() { try { DTObject formDTO = new
	 * DTObject(); formDTO.set("CUST_SEGMENT_CODE", customerSegmentCode);
	 * formDTO = validateCheckSegmentisActLikeParentForOther(formDTO); if
	 * (formDTO.get(ContentManager.ERROR) != null) {
	 * getErrorMap().setError("customerSegmentCode",
	 * formDTO.get(ContentManager.ERROR)); return false; } if
	 * (ContentManager.DATA_AVAILABLE.equals(formDTO.get(ContentManager.RESULT))
	 * && !enabled) { getErrorMap().setError("enabled",
	 * BackOfficeErrorCodes.PBS_CANNOT_DISABLED_AS_IT_IS_PARENT); return false;
	 * } return true; } catch (Exception e) { e.printStackTrace();
	 * getErrorMap().setError("customerSegmentCode",
	 * BackOfficeErrorCodes.UNSPECIFIED_ERROR); } return false; }
	 */

	/*
	 * public DTObject validateCheckSegmentisActLikeParentForOther(DTObject
	 * inputDTO) { DTObject resultDTO = new DTObject();
	 * resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
	 * DBContext dbContext = new DBContext(); DBUtil dbutil =
	 * dbContext.createUtilInstance(); String sqlQuery =
	 * "SELECT COUNT(1) FROM CUSTSEGMENT WHERE ENTITY_CODE=? AND PARENT_SEGMENT_CODE=? AND ENABLED='1'"
	 * ; try { dbutil.reset(); dbutil.setMode(DBUtil.PREPARED);
	 * dbutil.setSql(sqlQuery); dbutil.setString(1, context.getEntityCode());
	 * dbutil.setString(2, inputDTO.get("CUST_SEGMENT_CODE")); ResultSet rset =
	 * dbutil.executeQuery(); if (rset.next()) { if (rset.getInt(1) != 0)
	 * resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
	 * return resultDTO; } return resultDTO; } catch (Exception e) {
	 * e.printStackTrace(); resultDTO.set(ContentManager.ERROR,
	 * BackOfficeErrorCodes.UNSPECIFIED_ERROR); } finally { dbContext.close(); }
	 * return resultDTO; }
	 */

}