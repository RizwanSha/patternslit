package patterns.config.web.forms.cmn;

import java.sql.ResultSet;
import java.util.ArrayList;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.GenericOption;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.MasterValidator;
import patterns.config.web.forms.GenericFormBean;

/**
 * This program will be used to create the codes to represent various
 * organizations. outside the hospital. This is not for each entity. When the
 * organization is referred in the application further .
 * 
 * @version 1.0
 * @author P. Chandrakala
 * @since 27-May-2015 <br>
 *        <b>Modified History</b> <BR>
 *        <U><B> Sl.No Modified Date Author Modified Changes Version </B></U> <br>
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 */

public class morganizationbean extends GenericFormBean {

	private static final long serialVersionUID = 1L;
	private String organizationCode;
	private String description;
	private String conciseDescription;
	private String contactName_title;
	private String contactName;
	private String emailID;
	private String address;
	private boolean enabled;
	private String remarks;

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getOrganizationCode() {
		return organizationCode;
	}

	public void setOrganizationCode(String organizationCode) {
		this.organizationCode = organizationCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getconciseDescription() {
		return conciseDescription;
	}

	public void setconciseDescription(String conciseDescription) {
		this.conciseDescription = conciseDescription;
	}

	public String getConciseDescription() {
		return conciseDescription;
	}

	public void setConciseDescription(String conciseDescription) {
		this.conciseDescription = conciseDescription;
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public String getContactName_title() {
		return contactName_title;
	}

	public void setContactName_title(String contactName_title) {
		this.contactName_title = contactName_title;
	}

	public String getContactName_() {
		return contactName;
	}

	public void setContactName_name(String contactName) {
		this.contactName = contactName;
	}

	public String getEmailID() {
		return emailID;
	}

	public void setEmailID(String emailID) {
		this.emailID = emailID;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void reset() {
		organizationCode = RegularConstants.EMPTY_STRING;
		description = RegularConstants.EMPTY_STRING;
		conciseDescription = RegularConstants.EMPTY_STRING;
		contactName = RegularConstants.EMPTY_STRING;
		contactName_title = RegularConstants.EMPTY_STRING;
		address = RegularConstants.EMPTY_STRING;
		emailID = RegularConstants.EMPTY_STRING;
		enabled = false;
		remarks = RegularConstants.EMPTY_STRING;

		DBContext dbContext = new DBContext();
		ArrayList<GenericOption> title1 = null;
		title1 = getTitle(dbContext, "--", "G");
		webContext.getRequest().setAttribute("COMMON_TITLE", title1);
		dbContext.close();
		setProcessBO("patterns.config.framework.bo.cmn.morganizationBO");
	}

	public static final ArrayList<GenericOption> contactName_title(String programID, String tokenID, DBContext dbContext, String blankString) {
		ArrayList<GenericOption> genericList = new ArrayList<GenericOption>();
		int counter = 0;
		if (blankString != null) {
			genericList.add(counter, new GenericOption(ContentManager.EMPTY_STRING, blankString));
			++counter;
		}
		String sqlQuery = "SELECT TITLE_CODE FROM TITLE WHERE TITLE_OCCU <> ''";
		DBUtil util = dbContext.createUtilInstance();
		try {
			util.setMode(DBUtil.PREPARED);
			util.setSql(sqlQuery);
			ResultSet rs = util.executeQuery();
			while (rs.next()) {
				genericList.add(counter, new GenericOption(rs.getString(1), rs.getString(1)));
				++counter;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			util.reset();
		}
		return genericList;
	}

	public void validate() {
		if (validateOrganizationCode()) {
			validateDescription();
			validateConciseDescription();
			validateTitle();
			validatePersonNameOnly();
			validateEmailID();
			validateAddress();
			validateRemarks();
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set(ContentManager.ENTITY_CODE, context.getEntityCode());
				formDTO.set("ORG_CODE", organizationCode);
				formDTO.set("DESCRIPTION", description);
				formDTO.set("CONCISE_DESCRIPTION", conciseDescription);
				formDTO.set("CONTACT_PERSON_TITLE", contactName_title);
				formDTO.set("CONTACT_PERSON_NAME", contactName);
				formDTO.set("CONTACT_EMAIL_ID", emailID);
				formDTO.set("ADDRESS", address);
				if (getAction().equals(ADD)) {
					formDTO.set("ENABLED", decodeBooleanToString(true));
				} else {
					formDTO.set("ENABLED", decodeBooleanToString(enabled));
				}
				formDTO.set("REMARKS", remarks);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("organizationCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}

	}

	public boolean validateOrganizationCode() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("ORG_CODE", organizationCode);
			formDTO.set(ContentManager.USER_ACTION, getAction());
			formDTO = validateOrganizationCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("organizationCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			if (getAction().equals(ADD)) {
				if (formDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
					getErrorMap().setError("organizationCode", BackOfficeErrorCodes.RECORD_EXISTS);
					return false;
				}
			} else if (getAction().equals(MODIFY)) {
				if (formDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
					getErrorMap().setError("organizationCode", BackOfficeErrorCodes.RECORD_NOT_EXISTS);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("organizationCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateOrganizationCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		MasterValidator validation = new MasterValidator();
		String organizationCode = inputDTO.get("ORG_CODE");
		String action = inputDTO.get(ContentManager.USER_ACTION);
		try {
			if (validation.isEmpty(organizationCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
			resultDTO = validation.validateOrganizationCode(inputDTO);
			if (action.equals(ADD)) {
				if (ContentManager.DATA_AVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_EXISTS);
				}
			} else {
				if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_NOT_EXISTS);

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateDescription() {
		CommonValidator validation = new CommonValidator();
		try {
			if (!validation.isValidDescription(description)) {
				getErrorMap().setError("description", BackOfficeErrorCodes.INVALID_DESCRIPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("description", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateConciseDescription() {
		CommonValidator validation = new CommonValidator();
		try {
			if (!validation.isValidConciseDescription(conciseDescription)) {
				getErrorMap().setError("conciseDescription", BackOfficeErrorCodes.INVALID_CONCISE_DESCRIPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("conciseDescription", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateTitle() { // Professional Title
		MasterValidator validation = new MasterValidator();
		try {
			DTObject resultDTO = new DTObject();
			DTObject formDTO = new DTObject();
			formDTO.set("TITLE_CODE", contactName_title);
			formDTO.set(ContentManager.USER_ACTION, USAGE);
			resultDTO = validateTitle(formDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("contactName_name", resultDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("contactName_name", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public DTObject validateTitle(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		MasterValidator masterValidation = new MasterValidator();
		String titleCode = inputDTO.get("TITLE_CODE");
		try {
			if (!masterValidation.isEmpty(titleCode)) {
				inputDTO.set(ContentManager.FETCH_COLUMNS, "TITLE_OCCU");
				resultDTO = masterValidation.validateTitleCode(inputDTO);
				if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			masterValidation.close();
		}
		return resultDTO;
	}

	public boolean validatePersonNameOnly() {
		CommonValidator validation = new CommonValidator();
		try {

			if (validation.isEmpty(contactName)) {
				getErrorMap().setError("contactName", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidName(contactName)) {
				getErrorMap().setError("contactName", BackOfficeErrorCodes.INVALID_NAME);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("contactName", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateEmailID() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(emailID)) {
				getErrorMap().setError("emailID", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidEmail(emailID)) {
				getErrorMap().setError("emailID", BackOfficeErrorCodes.INVALID_EMAIL_ID);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("emailID", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateAddress() {
		CommonValidator validator = new CommonValidator();
		try {
			if (validator.isEmpty(address)) {
				getErrorMap().setError("address", BackOfficeErrorCodes.HMS_MANDATORY);
				return false;
			}
			if (!validator.isValidAddress(address)) {
				getErrorMap().setError("address", BackOfficeErrorCodes.INVALID_ADDRESS);
				return false;
			}

		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("address", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return true;
	}

	public boolean validateRemarks() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (remarks != null && remarks.length() > 0) {
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			if (getAction().equals(MODIFY)) {
				if (commonValidator.isEmpty(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}

}
