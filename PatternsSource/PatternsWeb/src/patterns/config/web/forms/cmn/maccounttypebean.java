package patterns.config.web.forms.cmn;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.FASValidator;
import patterns.config.validations.MasterValidator;
import patterns.config.web.forms.GenericFormBean;

public class maccounttypebean extends GenericFormBean {

	private static final long serialVersionUID = 1L;

	private String accountTypeCode;
	private String description;
	private String conciseDescription;
	private String accountTypeAlphaCode;
	private String prodCodeOfAcntType;
	private boolean subAccntType;
	private String accntsFrGovtScheme;
	private boolean chqBookFacAvail;
	private boolean passBookFacAvail;
	private boolean debCardFacAvail;
	private String glHeadAcntType;
	private boolean enabled;
	private String remarks;

	public String getAccntsFrGovtScheme() {
		return accntsFrGovtScheme;
	}

	public void setAccntsFrGovtScheme(String accntsFrGovtScheme) {
		this.accntsFrGovtScheme = accntsFrGovtScheme;
	}

	public boolean isChqBookFacAvail() {
		return chqBookFacAvail;
	}

	public void setChqBookFacAvail(boolean chqBookFacAvail) {
		this.chqBookFacAvail = chqBookFacAvail;
	}

	public boolean isPassBookFacAvail() {
		return passBookFacAvail;
	}

	public void setPassBookFacAvail(boolean passBookFacAvail) {
		this.passBookFacAvail = passBookFacAvail;
	}

	public boolean isDebCardFacAvail() {
		return debCardFacAvail;
	}

	public void setDebCardFacAvail(boolean debCardFacAvail) {
		this.debCardFacAvail = debCardFacAvail;
	}

	public boolean isSubAccntType() {
		return subAccntType;
	}

	public void setSubAccntType(boolean subAccntType) {
		this.subAccntType = subAccntType;
	}

	public String getAccountTypeCode() {
		return accountTypeCode;
	}

	public void setAccountTypeCode(String accountTypeCode) {
		this.accountTypeCode = accountTypeCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getConciseDescription() {
		return conciseDescription;
	}

	public void setConciseDescription(String conciseDescription) {
		this.conciseDescription = conciseDescription;
	}

	public String getAccountTypeAlphaCode() {
		return accountTypeAlphaCode;
	}

	public void setAccountTypeAlphaCode(String accountTypeAlphaCode) {
		this.accountTypeAlphaCode = accountTypeAlphaCode;
	}

	public String getProdCodeOfAcntType() {
		return prodCodeOfAcntType;
	}

	public void setProdCodeOfAcntType(String prodCodeOfAcntType) {
		this.prodCodeOfAcntType = prodCodeOfAcntType;
	}

	public String getGlHeadAcntType() {
		return glHeadAcntType;
	}

	public void setGlHeadAcntType(String glHeadAcntType) {
		this.glHeadAcntType = glHeadAcntType;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	@Override
	public void reset() {
		accountTypeCode = RegularConstants.EMPTY_STRING;
		description = RegularConstants.EMPTY_STRING;
		conciseDescription = RegularConstants.EMPTY_STRING;
		accountTypeAlphaCode = RegularConstants.EMPTY_STRING;
		prodCodeOfAcntType = RegularConstants.EMPTY_STRING;
		subAccntType = false;
		glHeadAcntType = RegularConstants.EMPTY_STRING;
		enabled = false;
		remarks = RegularConstants.EMPTY_STRING;
		setProcessBO("patterns.config.framework.bo.cmn.maccounttypeBO");
	}

	@Override
	public void validate() {
		CommonValidator validation = new CommonValidator();
		if (validateAccountTypeCode()) {
			validateDescription();
			validateConciseDescription();
			validateAccountTypeAlphaCode();
			validateAccntsFrGovtScheme();
			validateRemarks();
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.set("ACCOUNT_TYPE_CODE", accountTypeCode);
				formDTO.set("DESCRIPTION", description);
				formDTO.set("CONCISE_DESCRIPTION", conciseDescription);
				if (!validation.isEmpty(accountTypeAlphaCode))
					formDTO.set("ALPHA_CODE", accountTypeAlphaCode);
				formDTO.set("SUB_ACNT_TYPE_AVAILABLE", decodeBooleanToString(subAccntType));
				if (!validation.isEmpty(accntsFrGovtScheme))
					formDTO.set("GOVT_SCHEME_CODE", accntsFrGovtScheme);
				formDTO.set("CHEQUE_BOOK_FAC_AVAILABLE", decodeBooleanToString(chqBookFacAvail));
				formDTO.set("PASS_BOOK_FAC_AVAILABLE", decodeBooleanToString(passBookFacAvail));
				formDTO.set("DEBIT_CARD_FAC_AVAILABLE", decodeBooleanToString(debCardFacAvail));
				if (getAction().equals(ADD)) {
					formDTO.set("ENABLED", decodeBooleanToString(true));
				} else {
					formDTO.set("ENABLED", decodeBooleanToString(enabled));
				}
				formDTO.set("REMARKS", remarks);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("accountTypeCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}finally{
			validation.close();
		}
	}

	public boolean validateAccountTypeCode() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("ACCOUNT_TYPE_CODE", accountTypeCode);
			formDTO.set(ContentManager.ACTION, getAction());
			formDTO = validateAccountTypeCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("accountTypeCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("accountTypeCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateAccountTypeCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		MasterValidator validation = new MasterValidator();
		String accountTypeCode = inputDTO.get("ACCOUNT_TYPE_CODE");
		String action = inputDTO.get(ContentManager.USER_ACTION);
		try {
			if (validation.isEmpty(accountTypeCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "ACCOUNT_TYPE_CODE");
			resultDTO = validation.validateAccountTypeCode(inputDTO);
			if (action.equals(ADD)) {
				if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_EXISTS);
				}
			} else {
				if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_NOT_EXISTS);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateDescription() {
		CommonValidator validation = new CommonValidator();
		try {
			if (!validation.isValidDescription(description)) {
				getErrorMap().setError("description", BackOfficeErrorCodes.INVALID_DESCRIPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("description", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateConciseDescription() {
		CommonValidator validation = new CommonValidator();
		try {
			if (!validation.isValidConciseDescription(conciseDescription)) {
				getErrorMap().setError("conciseDescription", BackOfficeErrorCodes.INVALID_CONCISE_DESCRIPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("conciseDescription", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateAccountTypeAlphaCode() {
		CommonValidator validation = new CommonValidator();
		try {
			if (!validation.isEmpty(accountTypeAlphaCode)) {
				DTObject formDTO = new DTObject();
				formDTO.set("PROGRAM_ID", context.getProcessID());
				formDTO.set("ACCOUNT_TYPE_CODE", accountTypeCode);
				formDTO.set("ALPHA_CODE", accountTypeAlphaCode);
				formDTO.set(ContentManager.ACTION, USAGE);
				formDTO = validateAccountTypeAlphaCode(formDTO);
				if (formDTO.get(ContentManager.ERROR) != null) {
					getErrorMap().setError("accountTypeAlphaCode", formDTO.get(ContentManager.ERROR), (Object[]) formDTO.getObject(ContentManager.ERROR_PARAM));
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("accountTypeAlphaCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public DTObject validateAccountTypeAlphaCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		CommonValidator validation = new CommonValidator();
		String accountTypeAlphaCode = inputDTO.get("ALPHA_CODE");
		try {
			if (!validation.isEmpty(accountTypeAlphaCode)) {
				if (!validation.isValidCode(accountTypeAlphaCode)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
					return resultDTO;
				}
				if (!validation.isValidLength(accountTypeAlphaCode, validation.LENGTH_1, validation.LENGTH_6)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_LENGTH);
					return resultDTO;
				}
				String primaryKey = context.getEntityCode() + RegularConstants.PK_SEPARATOR + inputDTO.get("ACCOUNT_TYPE_CODE");
				String dedupKey = context.getEntityCode() + RegularConstants.PK_SEPARATOR + inputDTO.get("ALPHA_CODE") + RegularConstants.PK_SEPARATOR + RegularConstants.ONE;
				DTObject formDTO = new DTObject();
				formDTO.set(ContentManager.PROGRAM_ID, inputDTO.get("PROGRAM_ID"));
				formDTO.set(ContentManager.PROGRAM_KEY, primaryKey);
				formDTO.set(ContentManager.PURPOSE_ID, "ALPHA_CODE");
				formDTO.set(ContentManager.DEDUP_KEY, dedupKey);
				resultDTO = validation.isDeDupAlreadyExist(formDTO);
				if (resultDTO.get(ContentManager.ERROR) != null)
					return resultDTO;
				else if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.PBS_ACC_TYPE_ALPHA_CODE_LINKED);
					Object[] param = { resultDTO.get("LINKED_TO_PK").split("\\|")[1] };
					resultDTO.setObject(ContentManager.ERROR_PARAM, param);
					return resultDTO;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateAccntsFrGovtScheme() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("SCHEME_CODE", accntsFrGovtScheme);
			formDTO.set(ContentManager.USER_ACTION, USAGE);
			formDTO = validateAccntsFrGovtScheme(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("accntsFrGovtScheme", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("accntsFrGovtScheme", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateAccntsFrGovtScheme(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		MasterValidator validation = new MasterValidator();
		String accntsFrGovtScheme = inputDTO.get("SCHEME_CODE");
		try {
			if (!validation.isEmpty(accntsFrGovtScheme)) {
				inputDTO.set(ContentManager.FETCH_COLUMNS, "SCHEME_CODE,DESCRIPTION");
				resultDTO = validation.validateSchemeCode(inputDTO);
				if (resultDTO.get(ContentManager.ERROR) != null) {
					resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
					return resultDTO;
				}
				if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
					return resultDTO;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateRemarks() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (remarks != null && remarks.length() > 0) {
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			if (getAction().equals(MODIFY)) {
				if (commonValidator.isEmpty(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}

}
