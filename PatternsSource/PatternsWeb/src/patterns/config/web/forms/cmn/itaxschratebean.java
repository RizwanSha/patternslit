package patterns.config.web.forms.cmn;

import java.sql.ResultSet;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.process.TBAActionType;
import patterns.config.framework.service.DTDObject;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.MasterValidator;
import patterns.config.web.forms.GenericFormBean;

public class itaxschratebean extends GenericFormBean {

	private static final long serialVersionUID = 1L;

	private String stateCode;
	private String taxCode;
	private String effectiveDate;
	private String scheduleCode;
	private String taxRates;
	private String taxOnAmtPortion;
	private boolean enabled;
	private String remarks;
	private String xmlTaxSchrate;

	public String getStateCode() {
		return stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	public String getTaxCode() {
		return taxCode;
	}

	public void setTaxCode(String taxCode) {
		this.taxCode = taxCode;
	}

	public String getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public String getScheduleCode() {
		return scheduleCode;
	}

	public void setScheduleCode(String scheduleCode) {
		this.scheduleCode = scheduleCode;
	}

	public String getTaxRates() {
		return taxRates;
	}

	public void setTaxRates(String taxRates) {
		this.taxRates = taxRates;
	}

	public String getTaxOnAmtPortion() {
		return taxOnAmtPortion;
	}

	public void setTaxOnAmtPortion(String taxOnAmtPortion) {
		this.taxOnAmtPortion = taxOnAmtPortion;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getXmlTaxSchrate() {
		return xmlTaxSchrate;
	}

	public void setXmlTaxSchrate(String xmlTaxSchrate) {
		this.xmlTaxSchrate = xmlTaxSchrate;
	}

	@Override
	public void reset() {
		stateCode = RegularConstants.EMPTY_STRING;
		taxCode = RegularConstants.EMPTY_STRING;
		effectiveDate = RegularConstants.EMPTY_STRING;
		scheduleCode = RegularConstants.EMPTY_STRING;
		taxRates = RegularConstants.EMPTY_STRING;
		taxOnAmtPortion = RegularConstants.EMPTY_STRING;
		enabled = false;
		remarks = RegularConstants.EMPTY_STRING;
		xmlTaxSchrate = RegularConstants.EMPTY_STRING;
		setProcessBO("patterns.config.framework.bo.cmn.itaxschrateBO");
	}

	@Override
	public void validate() {
		if (validateStateCode() && validateTaxCode() && validateEffectiveDate()) {
			validateTaxOnAmtPortion();
			validateRemarks();
			validateGrid();
		}

		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.set("STATE_CODE", stateCode);
				formDTO.set("TAX_CODE", taxCode);
				formDTO.set("EFF_DATE", effectiveDate);
				formDTO.set("TAX_ON_AMT_PORTION", taxOnAmtPortion);
				if (getAction().equals(ADD)) {
					formDTO.set("ENABLED", decodeBooleanToString(true));
				} else {
					formDTO.set("ENABLED", decodeBooleanToString(enabled));
				}
				formDTO.set("REMARKS", remarks);

				DTDObject dtdObject = new DTDObject();
				dtdObject.addColumn(0, "SL");
				dtdObject.addColumn(1, "TAX_SCHEDULE_CODE");
				dtdObject.addColumn(2, "TAX_RATE");
				dtdObject.setXML(xmlTaxSchrate);
				formDTO.setDTDObject("TAXRATEDTLHIST", dtdObject);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("stateCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
	}

	public boolean validateStateCode() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("STATE_CODE", stateCode);
			formDTO.set(ContentManager.USER_ACTION, USAGE);
			formDTO = validateStateCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("stateCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("stateCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateStateCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		MasterValidator validation = new MasterValidator();
		String stateCode = inputDTO.get("STATE_CODE");
		try {
			if (validation.isEmpty(stateCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
			resultDTO = validation.validateCentralAndStateCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateTaxCode() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("STATE_CODE", stateCode);
			formDTO.set("TAX_CODE", taxCode);
			formDTO.set(ContentManager.USER_ACTION, USAGE);
			formDTO = validateTaxCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("taxCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("taxCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateTaxCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		DTObject resultDTOTaxon = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		MasterValidator validation = new MasterValidator();
		String stateCode = inputDTO.get("STATE_CODE");
		String taxCode = inputDTO.get("TAX_CODE");
		try {
			if (validation.isEmpty(stateCode) && validation.isEmpty(taxCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION,TAX_ON");
			resultDTO = validation.validateTax(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
			inputDTO.set("TAX_ON", resultDTO.get("TAX_ON"));
			resultDTOTaxon = getTaxRelatedInfo(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
			resultDTO.set("DESCRIPTION", resultDTO.get("DESCRIPTION"));
			resultDTO.set("TAX_ON_DESC", resultDTOTaxon.get("TAX_ON_DESC"));
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public DTObject getTaxRelatedInfo(DTObject inputDTO) {
		String sqlQuery = ContentManager.EMPTY_STRING;
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		sqlQuery = "SELECT LOV_LABEL_EN_US FROM cmlovrec WHERE PGM_ID=? AND LOV_ID=? AND LOV_VALUE=?";
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		try {
			util.setMode(DBUtil.PREPARED);
			util.setSql(sqlQuery);
			util.setString(1, "MTAX");
			util.setString(2, "TAX_ON");
			util.setString(3, inputDTO.get("TAX_ON"));
			ResultSet rs = util.executeQuery();
			if (rs.next()) {
				resultDTO.set("TAX_ON_DESC", rs.getString(1));
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
			} else {
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			util.reset();
			dbContext.close();
		}
		return resultDTO;
	}
	

	public boolean validateEffectiveDate() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set(ContentManager.ACTION, getAction());
			formDTO.set("STATE_CODE", stateCode);
			formDTO.set("TAX_CODE", taxCode);
			formDTO.set("EFF_DATE", effectiveDate);
			formDTO = validateEffectiveDate(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("effectiveDate", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("effectiveDate", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateEffectiveDate(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		CommonValidator validation = new CommonValidator();
		String stateCode = inputDTO.get("STATE_CODE");
		String taxCode = inputDTO.get("TAX_CODE");
		String effectiveDate = inputDTO.get("EFF_DATE");
		String action = inputDTO.get(ContentManager.USER_ACTION);
		try {
			if (validation.isEmpty(effectiveDate)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			TBAActionType AT;
			if (action.equals(ADD))
				AT = TBAActionType.ADD;
			else
				AT = TBAActionType.MODIFY;
			if (!validation.isEffectiveDateValid(effectiveDate, AT)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_EFFECTIVE_DATE);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "EFF_DATE");
			inputDTO.set(ContentManager.TABLE, "TAXRATEHIST");
			String columns[] = new String[] { context.getEntityCode(), stateCode, taxCode, effectiveDate };
			inputDTO.setObject(ContentManager.PROGRAM_KEY, columns);
			resultDTO = validation.validatePK(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (action.equals(ADD)) {
				if (ContentManager.DATA_AVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_EXISTS);
					return resultDTO;
				}
			} else if (action.equals(MODIFY)) {
				if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_NOT_EXISTS);
					return resultDTO;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	
	public boolean validateTaxOnAmtPortion() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(taxOnAmtPortion)) {
				getErrorMap().setError("taxOnAmtPortion", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (validation.isZero(taxOnAmtPortion)) {
				getErrorMap().setError("taxOnAmtPortion", BackOfficeErrorCodes.ZERO_CHECK);
				return false;
			}
			if (!validation.isThreeDigitTwoDecimal(taxOnAmtPortion, 2)) {
				getErrorMap().setError("taxOnAmtPortion", BackOfficeErrorCodes.INVALID_PERCENTAGE);
				return false;
			}
			if (!validation.isValidPercentage(taxOnAmtPortion)) {
				getErrorMap().setError("taxOnAmtPortion", BackOfficeErrorCodes.INVALID_PERCENTAGE);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("taxOnAmtPortion", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateRemarks() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (remarks != null && remarks.length() > 0) {
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			if (getAction().equals(MODIFY)) {
				if (commonValidator.isEmpty(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}

	public boolean validateScheduleCode(String scheduleCode) {
		try {
			DTObject formDTO = getFormDTO();
			formDTO.set("TAX_SCHEDULE_CODE", scheduleCode);
			formDTO = validateScheduleCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("scheduleCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("scheduleCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateScheduleCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		MasterValidator validation = new MasterValidator();
		String scheduleCode = inputDTO.get("TAX_SCHEDULE_CODE");
		try {
			if (validation.isEmpty(scheduleCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			if (!validation.isTaxScheduleAllowed(inputDTO)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateTaxRates(String taxRates) {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(taxRates)) {
				getErrorMap().setError("taxRates", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (validation.isZero(taxRates)) {
				getErrorMap().setError("taxRates", BackOfficeErrorCodes.ZERO_CHECK);
				return false;
			}
			if (!validation.isThreeDigitTwoDecimal(taxRates, 2)) {
				getErrorMap().setError("taxRates", BackOfficeErrorCodes.INVALID_PERCENTAGE);
				return false;
			}
			if (!validation.isValidPercentage(taxRates)) {
				getErrorMap().setError("taxRates", BackOfficeErrorCodes.INVALID_PERCENTAGE);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("taxRates", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	private boolean validateGrid() {
		CommonValidator validation = new CommonValidator();
		try {
			DTDObject dtdObject = new DTDObject();
			dtdObject.addColumn(0, "SL");
			dtdObject.addColumn(1, "TAX_SCHEDULE_CODE");
			dtdObject.addColumn(2, "TAX_RATE");
			dtdObject.setXML(xmlTaxSchrate);
			if (dtdObject.getRowCount() <= 0) {
				getErrorMap().setError("scheduleCode", BackOfficeErrorCodes.ATLEAST_ONE_ROW);
				return false;
			}
			if (!validation.checkDuplicateRows(dtdObject, 1)) {
				getErrorMap().setError("scheduleCode", BackOfficeErrorCodes.DUPLICATE_DATA);
				return false;
			}
			for (int i = 0; i < dtdObject.getRowCount(); i++) {
				if (!validateScheduleCode(dtdObject.getValue(i, 1))) {
					getErrorMap().setError("scheduleCode", BackOfficeErrorCodes.INVALID_ACTION);
					return false;
				}
				if (!validateTaxRates(dtdObject.getValue(i, 2))) {
					getErrorMap().setError("taxRates", BackOfficeErrorCodes.INVALID_ACTION);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("scheduleCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return true;
	}
}
