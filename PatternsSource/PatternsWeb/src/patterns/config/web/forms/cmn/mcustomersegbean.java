package patterns.config.web.forms.cmn;
import java.util.ArrayList;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.GenericOption;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.MasterValidator;
import patterns.config.web.forms.GenericFormBean;

public class mcustomersegbean extends GenericFormBean {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String customerSegmentCode;
	public String getCustomerSegmentCode() {
		return customerSegmentCode;
	}
	public void setCustomerSegmentCode(String customerSegmentCode) {
		this.customerSegmentCode = customerSegmentCode;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getConciseDescription() {
		return conciseDescription;
	}
	public void setConciseDescription(String conciseDescription) {
		this.conciseDescription = conciseDescription;
	}
	public String getSegmentFor() {
		return segmentFor;
	}
	public void setSegmentFor(String segmentFor) {
		this.segmentFor = segmentFor;
	}
	public boolean isPrioritySectorSegment() {
		return prioritySectorSegment;
	}
	public void setPrioritySectorSegment(boolean prioritySectorSegment) {
		this.prioritySectorSegment = prioritySectorSegment;
	}
	public boolean isSeniorCitizenSegment() {
		return seniorCitizenSegment;
	}
	public void setSeniorCitizenSegment(boolean seniorCitizenSegment) {
		this.seniorCitizenSegment = seniorCitizenSegment;
	}
	public boolean isWomenSegment() {
		return womenSegment;
	}
	public void setWomenSegment(boolean womenSegment) {
		this.womenSegment = womenSegment;
	}
	public boolean isStaffSegment() {
		return staffSegment;
	}
	public void setStaffSegment(boolean staffSegment) {
		this.staffSegment = staffSegment;
	}
	public boolean isEnabled() {
		return enabled;
	}
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	private String description;
	private String conciseDescription;
	private String segmentFor;
	private boolean prioritySectorSegment;
	private boolean seniorCitizenSegment;
	private boolean womenSegment;
	private boolean staffSegment;
    private boolean enabled;
	private String remarks;
	@Override
	public void reset() {
		customerSegmentCode = RegularConstants.EMPTY_STRING;
		description = RegularConstants.EMPTY_STRING;
		conciseDescription = RegularConstants.EMPTY_STRING;
		segmentFor = RegularConstants.EMPTY_STRING;
		prioritySectorSegment = false;
		seniorCitizenSegment = false;
		womenSegment = false;
		staffSegment = false;
		enabled = false;
		remarks = RegularConstants.EMPTY_STRING;
		DBContext dbContext = new DBContext();
		ArrayList<GenericOption> segmentFor = null;
		segmentFor = getGenericOptions("MCUSTOMERSEG", "SEGMENT_FOR", dbContext, "--");
		webContext.getRequest().setAttribute("MCUSTOMERSEG_SEGMENT_FOR", segmentFor);
		dbContext.close();
        setProcessBO("patterns.config.framework.bo.cmn.mcustomersegBO");
}
	@Override
	public void validate() {
		if (validateCustSegmentCode()) {
			validateSegmentFor();
			validateDescription();
			validateConciseDescription();
			validateRemarks();
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.set("CUST_SEGMENT_CODE", customerSegmentCode);
				formDTO.set("DESCRIPTION", description);
				formDTO.set("CONCISE_DESCRIPTION", conciseDescription);
				formDTO.set("SEGMENT_FOR", segmentFor);
				formDTO.set("PRIORTY_SECTOR_SEG", decodeBooleanToString(prioritySectorSegment));
				formDTO.set("SENIOR_CIT_SEG", decodeBooleanToString(seniorCitizenSegment));
				formDTO.set("WOMEN_SEG", decodeBooleanToString(womenSegment));
				formDTO.set("STAFF_SEG", decodeBooleanToString(staffSegment));
			if (getAction().equals(ADD)) {
				formDTO.set("ENABLED", decodeBooleanToString(true));
			} else {
				formDTO.set("ENABLED", decodeBooleanToString(enabled));
			}
			formDTO.set("REMARKS", remarks);
		}
	} 
		catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("depositProductCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
	}
		
	public boolean validateCustSegmentCode() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("CUST_SEGMENT_CODE", customerSegmentCode);
			formDTO.set(ContentManager.USER_ACTION, getAction());
			formDTO = validateCustSegmentCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("customerSegmentCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("customerSegmentCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;

	}
	   
	public DTObject validateCustSegmentCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		MasterValidator validation = new MasterValidator();
		String customerSegmentCode = inputDTO.get("CUST_SEGMENT_CODE");
		String action = inputDTO.get(ContentManager.USER_ACTION);
		try {
			if (validation.isEmpty(customerSegmentCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
			resultDTO = validation.validateCustomerSegmentCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (action.equals(ADD)) {
				if (ContentManager.DATA_AVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_EXISTS);
				}
			} else {
				if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_NOT_EXISTS);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}
		
	public boolean validateDescription() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(description)) {
				getErrorMap().setError("description", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidDescription(description)) {
				getErrorMap().setError("description", BackOfficeErrorCodes.INVALID_DESCRIPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("description", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateConciseDescription() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(conciseDescription)) {
				getErrorMap().setError("conciseDescription", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidConciseDescription(conciseDescription)) {
				getErrorMap().setError("conciseDescription", BackOfficeErrorCodes.INVALID_CONCISE_DESCRIPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("conciseDescription", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}
	
	public boolean validateSegmentFor() {
		CommonValidator validator = new CommonValidator();
		try {
			if (validator.isEmpty(segmentFor)) {
				getErrorMap().setError("segmentFor", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validator.isCMLOVREC("MCUSTOMERSEG", "SEGMENT_FOR", segmentFor)) {
				getErrorMap().setError("segmentFor", BackOfficeErrorCodes.INVALID_CODE);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("segmentFor", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return false;
	}
	
	public boolean validateRemarks() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (remarks != null && remarks.length() > 0) {
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			if (getAction().equals(MODIFY)) {
				if (commonValidator.isEmpty(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}
}
