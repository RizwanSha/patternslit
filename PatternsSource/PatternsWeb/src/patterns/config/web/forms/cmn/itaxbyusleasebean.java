package patterns.config.web.forms.cmn;

import java.sql.ResultSet;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.process.TBAActionType;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.FASValidator;
import patterns.config.validations.LeaseValidator;
import patterns.config.validations.MasterValidator;
import patterns.config.validations.RegistrationValidator;
import patterns.config.web.forms.GenericFormBean;

public class itaxbyusleasebean extends GenericFormBean {
	private static final long serialVersionUID = 1L;
	private String lesseeCode;
	private String customerId;
	private String customerName;
	private String agreementNumber;
	private String scheduleId;
	private String stateCode;
	private String taxCode;
	private String effectiveDate;
	private boolean taxBorne;
	private String accountingHead;
	private boolean enabled;
	private String remarks;


	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getLesseeCode() {
		return lesseeCode;
	}

	public void setLesseeCode(String lesseeCode) {
		this.lesseeCode = lesseeCode;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getAgreementNumber() {
		return agreementNumber;
	}

	public void setAgreementNumber(String agreementNumber) {
		this.agreementNumber = agreementNumber;
	}

	public String getScheduleId() {
		return scheduleId;
	}

	public void setScheduleId(String scheduleId) {
		this.scheduleId = scheduleId;
	}

	public String getStateCode() {
		return stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	public String getTaxCode() {
		return taxCode;
	}

	public void setTaxCode(String taxCode) {
		this.taxCode = taxCode;
	}

	public String getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public boolean isTaxBorne() {
		return taxBorne;
	}

	public void setTaxBorne(boolean taxBorne) {
		this.taxBorne = taxBorne;
	}

	public String getAccountingHead() {
		return accountingHead;
	}

	public void setAccountingHead(String accountingHead) {
		this.accountingHead = accountingHead;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void reset() {
		lesseeCode = RegularConstants.EMPTY_STRING;
		customerId = RegularConstants.EMPTY_STRING;
		agreementNumber = RegularConstants.EMPTY_STRING;
		scheduleId = RegularConstants.EMPTY_STRING;
		stateCode = RegularConstants.EMPTY_STRING;
		taxCode = RegularConstants.EMPTY_STRING;
		effectiveDate = RegularConstants.EMPTY_STRING;
		taxBorne = false;
		accountingHead = RegularConstants.EMPTY_STRING;
		enabled = false;
		remarks = RegularConstants.EMPTY_STRING;
		setProcessBO("patterns.config.framework.bo.cmn.itaxbyusleaseBO");
	}

	public void validate() {
		if (validateCustLeaseCode() && validateAgreementNo() && validateScheduleID() && validateStateCode() && validateTaxCode() && validateEffectiveDate()) {
			validateAccountHead();
			validateRemarks();
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.set("LESSEE_CODE", lesseeCode);
				formDTO.set("AGREEMENT_NO", agreementNumber);
				formDTO.set("SCHEDULE_ID", scheduleId);
				formDTO.set("STATE_CODE", stateCode);
				formDTO.set("TAX_CODE", taxCode);
				formDTO.set("EFF_DATE", effectiveDate);
				formDTO.set("TAX_BY_US", decodeBooleanToString(taxBorne));
				formDTO.set("TAX_BYUS_GL_HEAD_CODE", accountingHead);
				if (getAction().equals(ADD)) {
					formDTO.set("ENABLED", decodeBooleanToString(true));
				} else {
					formDTO.set("ENABLED", decodeBooleanToString(enabled));
				}
				formDTO.set("REMARKS", remarks);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("lesseeCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
	}

	public boolean validateCustLeaseCode() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("SUNDRY_DB_AC", lesseeCode);
			formDTO.set(ContentManager.ACTION, USAGE);
			formDTO = validateCustLeaseCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("lesseeCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("lesseeCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateCustLeaseCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, ContentManager.DATA_UNAVAILABLE);
		RegistrationValidator validation = new RegistrationValidator();
		String lesseeCode = inputDTO.get("SUNDRY_DB_AC");
		try {
			if (validation.isEmpty(lesseeCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			resultDTO = getCustomerLesseeCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
			inputDTO.set("CUSTOMER_ID", resultDTO.get("CUSTOMER_ID"));
			inputDTO.set(ContentManager.FETCH_COLUMNS, "CUSTOMER_ID,CUSTOMER_NAME");
			resultDTO = validation.valildateCustomerID(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public DTObject getCustomerLesseeCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		try {
			String sqlQuery = "SELECT CUSTOMER_ID,LEASE_PRODUCT_CODE FROM CUSTOMERACDTL WHERE ENTITY_CODE=? AND SUNDRY_DB_AC=?";
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, context.getEntityCode());
			util.setString(2, inputDTO.get("SUNDRY_DB_AC"));
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				resultDTO.set("CUSTOMER_ID", rset.getString("CUSTOMER_ID"));
				resultDTO.set("LEASE_PRODUCT_CODE", rset.getString("LEASE_PRODUCT_CODE"));
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			util.reset();
			dbContext.close();
		}
		return resultDTO;
	}

	public boolean validateAgreementNo() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("SUNDRY_DB_AC", lesseeCode);
			formDTO.set("CUSTOMER_ID", customerId);
			formDTO.set("AGREEMENT_NO", agreementNumber);
			formDTO.set(ContentManager.ACTION, USAGE);
			formDTO = validateAgreementNo(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("agreementNumber", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("agreementNumber", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateAgreementNo(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, ContentManager.DATA_UNAVAILABLE);
		LeaseValidator validation = new LeaseValidator();
		String agreementNumber = inputDTO.get("AGREEMENT_NO");
		try {
			if (validation.isEmpty(agreementNumber)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			if (!validation.isValidNumber(agreementNumber)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_NUMBER);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, ContentManager.FETCH_ALL);
			resultDTO = validation.validateCustomerAgreement(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	private boolean validateScheduleID() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("LESSEE_CODE", lesseeCode);
			formDTO.set("AGREEMENT_NO", agreementNumber);
			formDTO.set("SCHEDULE_ID", scheduleId);
			formDTO.set(ContentManager.USER_ACTION, USAGE);
			formDTO = validateScheduleID(formDTO);
			if (formDTO.get(ContentManager.ERROR) != RegularConstants.NULL) {
				getErrorMap().setError("scheduleId", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("scheduleID", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateScheduleID(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		LeaseValidator validation = new LeaseValidator();
		String scheduleId = inputDTO.get("SCHEDULE_ID");
		try {
			if (validation.isEmpty(scheduleId)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "SCHEDULE_ID,ASSET_CCY,AUTH_ON,LEASE_CLOSURE_ON");
			resultDTO = validation.validateScheduleID(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != RegularConstants.NULL) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
			if (resultDTO.getObject("AUTH_ON") == null) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNAUTHORIZED_LEASE);
				return resultDTO;
			}
			if (resultDTO.getObject("LEASE_CLOSURE_ON") != null) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.LEASE_CLOSED);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateStateCode() {
		try {
			DTObject formDTO = getFormDTO();
			formDTO.set("STATE_CODE", stateCode);
			formDTO.set(ContentManager.USER_ACTION, getAction());
			formDTO = validateStateCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("stateCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("stateCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateStateCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		MasterValidator validation = new MasterValidator();
		String stateCode = inputDTO.get("STATE_CODE");
		try {
			if (validation.isEmpty(stateCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "STATE_CODE,DESCRIPTION");
			resultDTO = validation.validateStateCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateTaxCode() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("STATE_CODE", stateCode);
			formDTO.set("TAX_CODE", taxCode);
			formDTO.set(ContentManager.ACTION, USAGE);
			formDTO = validateTaxCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("taxCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("taxCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateTaxCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		MasterValidator validation = new MasterValidator();
		String taxCode = inputDTO.get("TAX_CODE");
		try {
			if (validation.isEmpty(taxCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "TAX_CODE,DESCRIPTION,TAX_BYUS_PROD_ALLOWED");
			resultDTO = validation.validateTax(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
			if (resultDTO.get("TAX_BYUS_PROD_ALLOWED").equals(RegularConstants.ZERO)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.CONFIGURATION_NOT_ALLOWED);
				return resultDTO;
			}

		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateEffectiveDate() {
		try {
			DTObject formDTO = getFormDTO();
			formDTO.set("SUNDRY_DB_AC", lesseeCode);
			formDTO.set("AGREEMENT_NO", agreementNumber);
			formDTO.set("SCHEDULE_ID", scheduleId);
			formDTO.set("STATE_CODE", stateCode);
			formDTO.set("TAX_CODE", taxCode);
			formDTO.set("EFF_DATE", effectiveDate);
			formDTO.set(ContentManager.USER_ACTION, getAction());
			formDTO = validateEffectiveDate(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("effectiveDate", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("effectiveDate", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
		}
		return false;
	}

	public DTObject validateEffectiveDate(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		MasterValidator validation = new MasterValidator();
		String lesseeCode = inputDTO.get("SUNDRY_DB_AC");
		String agreementNumber = inputDTO.get("AGREEMENT_NO");
		String scheduleId = inputDTO.get("SCHEDULE_ID");
		String stateCode = inputDTO.get("STATE_CODE");
		String taxCode = inputDTO.get("TAX_CODE");
		String effectiveDate = inputDTO.get("EFF_DATE");
		String action = inputDTO.get(ContentManager.USER_ACTION);
		try {
			TBAActionType AT;
			if (action.equals(ADD))
				AT = TBAActionType.ADD;
			else
				AT = TBAActionType.MODIFY;
			if (validation.isEmpty(effectiveDate)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;

			}
			if (!validation.isEffectiveDateValid(effectiveDate, AT)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_EFFECTIVE_DATE);
				return resultDTO;
			}

			inputDTO.set(ContentManager.FETCH_COLUMNS, "TAX_CODE,ENABLED");
			inputDTO.set(ContentManager.TABLE, "TAXBYUSLEASEHIST");
			String columns[] = new String[] { context.getEntityCode(), lesseeCode, agreementNumber, scheduleId, stateCode, taxCode, effectiveDate };
			inputDTO.setObject(ContentManager.PROGRAM_KEY, columns);
			resultDTO = validation.validatePK(inputDTO);
			if (action.equals(ADD)) {
				if (ContentManager.DATA_AVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_EXISTS);
				}
			} else {
				if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_NOT_EXISTS);

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateAccountHead() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (taxBorne) {
				DTObject formDTO = new DTObject();
				formDTO.set("GL_HEAD_CODE", accountingHead);
				formDTO.set(ContentManager.USER_ACTION, USAGE);
				formDTO = validateGlHeadCode(formDTO);
				if (formDTO.get(ContentManager.ERROR) != null) {
					getErrorMap().setError("accountingHead", formDTO.get(ContentManager.ERROR));
					return false;
				}
			} else {
				if (!commonValidator.isEmpty(accountingHead)) {
					getErrorMap().setError("accountingHead", BackOfficeErrorCodes.PBS_VALUE_NOT_ALLOWED);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("accountingHead", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}

	public DTObject validateGlHeadCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		FASValidator validation = new FASValidator();
		String accountingHead = inputDTO.get("GL_HEAD_CODE");
		try {
			if (validation.isEmpty(accountingHead)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "GL_HEAD_CODE,DESCRIPTION,ENABLED");
			resultDTO = validation.validateGLHeadCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateRemarks() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (remarks != null && remarks.length() > 0) {
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			if (getAction().equals(MODIFY)) {
				if (commonValidator.isEmpty(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}
}
