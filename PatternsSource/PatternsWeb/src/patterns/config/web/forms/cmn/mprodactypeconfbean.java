package patterns.config.web.forms.cmn;

import java.sql.ResultSet;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.process.TBAActionType;
import patterns.config.framework.service.DTDObject;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.MasterValidator;
import patterns.config.web.forms.GenericFormBean;

public class mprodactypeconfbean extends GenericFormBean {

	private static final long serialVersionUID = 1L;

	private String productCode;
	private String currencyCode;
	private String effectiveDate;
	private String accountType;
	private String accountSubType;
	private String xmlAccTypeGrid;
	private boolean enabled;
	private String remarks;

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public String getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public String getAccountSubType() {
		return accountSubType;
	}

	public void setAccountSubType(String accountSubType) {
		this.accountSubType = accountSubType;
	}

	public String getXmlAccTypeGrid() {
		return xmlAccTypeGrid;
	}

	public void setXmlAccTypeGrid(String xmlAccTypeGrid) {
		this.xmlAccTypeGrid = xmlAccTypeGrid;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public void reset() {
		// TODO Auto-generated method stub
		productCode = RegularConstants.EMPTY_STRING;
		currencyCode = RegularConstants.EMPTY_STRING;
		effectiveDate = RegularConstants.EMPTY_STRING;
		xmlAccTypeGrid = RegularConstants.EMPTY_STRING;
		enabled = false;
		remarks = RegularConstants.EMPTY_STRING;
		setProcessBO("patterns.config.framework.bo.cmn.mprodactypeconfBO");
	}

	@Override
	public void validate() {
		if (validateProductCode() && validateCurrencyCode() && validateEffectiveDate()) {
			validateGrid();
			validateRemarks();
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.set("PRODUCT_CODE", productCode);
				formDTO.set("CCY_CODE", currencyCode);
				formDTO.set("EFF_DATE", effectiveDate);
				if (getAction().equals(ADD)) {
					formDTO.set("ENABLED", decodeBooleanToString(true));
				} else {
					formDTO.set("ENABLED", decodeBooleanToString(enabled));
				}
				formDTO.set("REMARKS", remarks);

				DTDObject dtdObject = new DTDObject();
				dtdObject.addColumn(0, "SL");
				dtdObject.addColumn(1, "ACCOUNT_TYPE_CODE");
				dtdObject.addColumn(2, "DESCRIPTION");
				dtdObject.addColumn(3, "ACCOUNT_SUBTYPE_CODE");
				dtdObject.addColumn(4, "DESCRIPTION");
				dtdObject.setXML(xmlAccTypeGrid);
				formDTO.setDTDObject("PRODACTYPECONFHISTDTL", dtdObject);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("productCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
	}

	private boolean validateProductCode() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("PRODUCT_CODE", productCode);
			formDTO.set(ContentManager.USER_ACTION, USAGE);
			formDTO = validateProductCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("productCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("productCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateProductCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		MasterValidator validation = new MasterValidator();
		String productCode = inputDTO.get("PRODUCT_CODE");
		try {
			if (validation.isEmpty(productCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "PRODUCT_CODE,DESCRIPTION");
			resultDTO = validation.validateProductCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}

		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateCurrencyCode() {
		try {
			DTObject formDTO = getFormDTO();
			formDTO.set("PRODUCT_CODE", productCode);
			formDTO.set("CCY_CODE", currencyCode);
			formDTO.set(ContentManager.USER_ACTION, USAGE);
			formDTO = validateCurrencyCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("currencyCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("currencyCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateCurrencyCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		DTObject currencyDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		MasterValidator validation = new MasterValidator();
		String ccyCode = inputDTO.get("CCY_CODE");
		try {
			if (validation.isEmpty(ccyCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
			resultDTO = validation.validateCurrency(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CURRENCYCODE);
				return resultDTO;
			}
			inputDTO.set("PRODUCT_CODE", inputDTO.get("PRODUCT_CODE"));
			inputDTO.set("CCY_CODE", ccyCode);
			currencyDTO = validateCurrencyFields(inputDTO);
			if (currencyDTO.get(ContentManager.ERROR) != null) {
				currencyDTO.set(ContentManager.ERROR, currencyDTO.get(ContentManager.ERROR));
				return currencyDTO;
			}
			if (ContentManager.DATA_UNAVAILABLE.equals(currencyDTO.get(ContentManager.RESULT))) {
				currencyDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.CCY_PRODUCT_NA);
				return currencyDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public DTObject validateCurrencyFields(DTObject inputDTO) {
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		DTObject resultDTO = new DTObject();
		try {
			util.setMode(DBUtil.PREPARED);
			util.setSql("SELECT COUNT(1) FROM PRODCURRDTL WHERE ENTITY_CODE=? AND PRODUCT_CODE=? AND CURR_CODE=?");
			util.setString(1, context.getEntityCode());
			util.setString(2, inputDTO.get("PRODUCT_CODE"));
			util.setString(3, inputDTO.get("CCY_CODE"));
			ResultSet rs = util.executeQuery();
			if (rs.next()) {
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
			} else {
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			util.reset();
			dbContext.close();
		}
		return resultDTO;
	}

	public boolean validateEffectiveDate() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("PRODUCT_CODE", productCode);
			formDTO.set("CCY_CODE", currencyCode);
			formDTO.set("EFF_DATE", effectiveDate);
			formDTO.set(ContentManager.USER_ACTION, getAction());
			formDTO = validateEffectiveDate(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("effectiveDate", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("effectiveDate", BackOfficeErrorCodes.UNSPECIFIED_ERROR);

		}
		return false;
	}

	public DTObject validateEffectiveDate(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		CommonValidator commonvalidation = new CommonValidator();
		String productCode = inputDTO.get("PRODUCT_CODE");
		String effectiveDate = inputDTO.get("EFF_DATE");
		String ccyCode = inputDTO.get("CCY_CODE");
		String action = inputDTO.get(ContentManager.USER_ACTION);
		try {
			if (commonvalidation.isEmpty(effectiveDate)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			TBAActionType AT;
			if (action.equals(ADD))
				AT = TBAActionType.ADD;
			else
				AT = TBAActionType.MODIFY;
			if (!commonvalidation.isEffectiveDateValid(effectiveDate, AT)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_EFFECTIVE_DATE);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "PRODUCT_CODE");
			inputDTO.set(ContentManager.TABLE, "PRODACTYPECONFHIST");
			String columns[] = new String[] { context.getEntityCode(), productCode, ccyCode, effectiveDate };
			inputDTO.setObject(ContentManager.PROGRAM_KEY, columns);
			resultDTO = commonvalidation.validatePK(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (action.equals(ADD)) {
				if (ContentManager.DATA_AVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_EXISTS);
					return resultDTO;
				}
			} else if (action.equals(MODIFY)) {
				if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_NOT_EXISTS);
					return resultDTO;
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonvalidation.close();
		}
		return resultDTO;
	}

	private boolean validateAccountType(String accountType) {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("ACCOUNT_TYPE_CODE", accountType);
			formDTO.set(ContentManager.USER_ACTION, USAGE);
			formDTO = validateAccountTypeCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("accountType", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("accountType", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateAccountTypeCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		MasterValidator validation = new MasterValidator();
		String accType = inputDTO.get("ACCOUNT_TYPE_CODE");
		try {
			if (validation.isEmpty(accType)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "ACCOUNT_TYPE_CODE,DESCRIPTION");
			resultDTO = validation.validateAccountTypeCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	private boolean validateAccountSubType(String accountSubType, String accountType) {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("PRODUCT_CODE", productCode);
			formDTO.set("ACCOUNT_TYPE_CODE", accountType);
			formDTO.set("ACCOUNT_SUBTYPE_CODE", accountSubType);
			formDTO.set(ContentManager.USER_ACTION, USAGE);
			formDTO = validateAccountSubTypeCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("accountSubType", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("accountSubType", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateAccountSubTypeCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		DTObject resultDTO1 = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		MasterValidator validation = new MasterValidator();
		String accSubType = inputDTO.get("ACCOUNT_SUBTYPE_CODE");
		String prodCode = inputDTO.get("PRODUCT_CODE");
		try {
			if (validation.isEmpty(accSubType)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "ACCOUNT_SUBTYPE_CODE,DESCRIPTION,CUST_SEGMENT_CODE");
			resultDTO = validation.validateAccountSubTypeCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
			inputDTO.set("PRODUCT_CODE", prodCode);
			inputDTO.set("CUST_SEGMENT_CODE", resultDTO.get("CUST_SEGMENT_CODE"));
			resultDTO1 = validateCustomerSegment(inputDTO);
			if (resultDTO1.get(ContentManager.ERROR) != null) {
				resultDTO1.set(ContentManager.ERROR, resultDTO1.get(ContentManager.ERROR));
				return resultDTO1;
			}
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO1.get(ContentManager.RESULT))) {
				resultDTO1.set(ContentManager.ERROR, BackOfficeErrorCodes.PROD_ACNT_SUBTYPE_CUSTSEG_NC);
				return resultDTO1;
			}

		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public DTObject validateCustomerSegment(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		String productCode = inputDTO.get("PRODUCT_CODE");
		String custSegment = inputDTO.get("CUST_SEGMENT_CODE");
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		try {
			util.setMode(DBUtil.PREPARED);
			util.reset();
			util.setSql("SELECT COUNT(1) FROM PRODAVLSEGDTL WHERE ENTITY_CODE=? AND PRODUCT_CODE=? AND CUST_SEGMENT=?");
			util.setString(1, context.getEntityCode());
			util.setString(2, productCode);
			util.setString(3, custSegment);
			ResultSet rs = util.executeQuery();
			if (rs.next()) {
				if (rs.getBoolean(1)) {
					resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				} else {
					resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			dbContext.close();
		}
		return resultDTO;
	}

	public boolean validateRemarks() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (remarks != null && remarks.length() > 0) {
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			if (getAction().equals(MODIFY)) {
				if (commonValidator.isEmpty(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}

	private boolean validateGrid() {
		CommonValidator validation = new CommonValidator();
		try {
			DTDObject dtdObject = new DTDObject();
			dtdObject.addColumn(0, "SL");
			dtdObject.addColumn(1, "ACCOUNT_TYPE_CODE");
			dtdObject.addColumn(2, "DESCRIPTION");
			dtdObject.addColumn(3, "ACCOUNT_SUBTYPE_CODE");
			dtdObject.addColumn(4, "DESCRIPTION");
			dtdObject.setXML(xmlAccTypeGrid);
			if (dtdObject.getRowCount() <= 0) {
				getErrorMap().setError("accountType", BackOfficeErrorCodes.ATLEAST_ONE_ROW);
				return false;
			}
			if (!validation.checkDuplicateRows(dtdObject, 1, 3)) {
				getErrorMap().setError("accountType", BackOfficeErrorCodes.DUPLICATE_DATA);
				return false;
			}
			for (int i = 0; i < dtdObject.getRowCount(); i++) {
				if (!validateAccountType(dtdObject.getValue(i, 1))) {
					getErrorMap().setError("accountType", BackOfficeErrorCodes.INVALID_ACTION);
					return false;
				}
				if (!validateAccountSubType(dtdObject.getValue(i, 3), dtdObject.getValue(i, 1))) {
					getErrorMap().setError("accountSubType", BackOfficeErrorCodes.INVALID_ACTION);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("accountType", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

}
