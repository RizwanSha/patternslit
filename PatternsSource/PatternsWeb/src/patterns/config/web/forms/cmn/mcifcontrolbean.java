/*
 *The program used for Customer Id Folio(CIF) control 

 Author : Jayashree.P
 Created Date : 28-June-2016
 Spec Reference :mcifcontrol
 Modification History
 ----------------------------------------------------------------------
 Sl.No		Modified Date	Author			Modified Changes	Version
 ----------------------------------------------------------------------

 */

package patterns.config.web.forms.cmn;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.process.TBAActionType;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.MasterValidator;
import patterns.config.web.forms.GenericFormBean;

public class mcifcontrolbean extends GenericFormBean {

	private static final long serialVersionUID = 1L;
	private String customerSegmentCode;
	private String effectiveDate;
	private String cifNumber;
	private boolean enabled;
	private String remarks;

	public String getCustomerSegmentCode() {
		return customerSegmentCode;
	}

	public void setCustomerSegmentCode(String customerSegmentCode) {
		this.customerSegmentCode = customerSegmentCode;
	}

	public String getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public String getCifNumber() {
		return cifNumber;
	}

	public void setCifNumber(String cifNumber) {
		this.cifNumber = cifNumber;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void reset() {
		customerSegmentCode = RegularConstants.EMPTY_STRING;
		effectiveDate = RegularConstants.EMPTY_STRING;
		cifNumber = RegularConstants.EMPTY_STRING;
		enabled = false;
		remarks = RegularConstants.EMPTY_STRING;
		setProcessBO("patterns.config.framework.bo.cmn.mcifcontrolBO");
	}

	@Override
	public void validate() {
		if (validateCustomerSegmentCode() && validateEffectiveDate()) {
			validateStartingCIFNumber();
			validateRemarks();
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.set("CUST_SEGMENT_CODE", customerSegmentCode);
				formDTO.set("EFF_DATE", effectiveDate);
				formDTO.set("STARTING_CIF", cifNumber);
				if (getAction().equals(ADD)) {
					formDTO.set("ENABLED", decodeBooleanToString(true));
				} else {
					formDTO.set("ENABLED", decodeBooleanToString(enabled));
				}
				formDTO.set("REMARKS", remarks);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("customerSegmentCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
	}

	public boolean validateCustomerSegmentCode() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("CUST_SEGMENT_CODE", customerSegmentCode);
			formDTO.set(ContentManager.USER_ACTION, USAGE);
			formDTO = validateCustomerSegmentCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("customerSegmentCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("customerSegmentCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;

	}

	public DTObject validateCustomerSegmentCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		MasterValidator validation = new MasterValidator();
		String customerSegmentCode = inputDTO.get("CUST_SEGMENT_CODE");
		try {
			if (validation.isEmpty(customerSegmentCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
			resultDTO = validation.validateCustomerSegmentCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				return resultDTO;
			}
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateEffectiveDate() {
		CommonValidator validation = new CommonValidator();
		DTObject formDTO = new DTObject();
		try {
			if (validation.isEmpty(effectiveDate)) {
				getErrorMap().setError("effectiveDate", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			formDTO.set(ContentManager.ACTION, getAction());
			formDTO.set("CUST_SEGMENT_CODE", customerSegmentCode);
			formDTO.set("EFF_DATE", effectiveDate);
			formDTO = validateEffectiveDate(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("effectiveDate", formDTO.get(ContentManager.ERROR));
				return false;
			}
			
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("effectiveDate", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public DTObject validateEffectiveDate(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		CommonValidator commonvalidation = new CommonValidator();
		String effDate = inputDTO.get("EFF_DATE");
		String action = inputDTO.get(ContentManager.ACTION);
		try {
			TBAActionType AT;
			if (action.equals(ADD))
				AT = TBAActionType.ADD;
			else
				AT = TBAActionType.MODIFY;
			if (!commonvalidation.isEffectiveDateValid(effDate, AT)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_EFFECTIVE_DATE);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "ENABLED");
			inputDTO.set(ContentManager.TABLE, "CIFCONTROLHIST");
			String columns[] = new String[] { context.getEntityCode(), inputDTO.get("CUST_SEGMENT_CODE"), effDate };
			inputDTO.setObject(ContentManager.PROGRAM_KEY, columns);
			resultDTO = commonvalidation.validatePK(inputDTO);
			if (action.equals(ADD)) {
				if (ContentManager.DATA_AVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_EXISTS);
					return resultDTO;
				}
			} else if (action.equals(MODIFY)) {
				if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_NOT_EXISTS);
					return resultDTO;
				}
				if (resultDTO.get("ENABLED").equals(RegularConstants.COLUMN_DISABLE)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.CODE_CLOSED);
					return resultDTO;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonvalidation.close();
		}
		return resultDTO;
	}

	public boolean validateStartingCIFNumber() {
		MasterValidator validation = new MasterValidator();
		try {
			if (validation.isEmpty(cifNumber)) {
				getErrorMap().setError("cifNumber", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidNumber(cifNumber)){
				getErrorMap().setError("cifNumber", BackOfficeErrorCodes.INVALID_NUMBER_FORMAT);
				return false;
			}
			if (!validation.isValidPositiveNumber(cifNumber)){
				getErrorMap().setError("cifNumber", BackOfficeErrorCodes.POSITIVE_CHECK);
				return false;
			}
			if (!validation.isValidLength(cifNumber, 1, 12)){
				getErrorMap().setError("cifNumber", BackOfficeErrorCodes.INVALID_LENGTH);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("cifNumber", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateRemarks() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (remarks != null && remarks.length() > 0) {
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			if (getAction().equals(MODIFY)) {
				if (commonValidator.isEmpty(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}
}