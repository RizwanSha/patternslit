/*
 *The program used for Lease Purchase Order (PO) Details Entry

 Author : ARULPRASATH A
 Created Date : 21-FEB-2017
 Spec Reference : PPBS/LSS-ELEASEPO
 Modification History
 ----------------------------------------------------------------------
 Sl.No		Modified Date	Author			Modified Changes	  Version
 ----------------------------------------------------------------------
	1		08-03-2017    KALIMUTHU U      SCHEDULE ID based load  2.0
											the grid fields.
											
	2		21-03-2017		RAJA E				version3.0	& 3.1	
------------------------------------------------------------------------
				
 */

package patterns.config.web.forms.lss;

import java.sql.ResultSet;

import patterns.config.constants.ProgramConstants;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.service.DTDObject;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.LeaseValidator;
import patterns.config.validations.MasterValidator;
import patterns.config.validations.RegistrationValidator;
import patterns.config.web.forms.GenericFormBean;

public class eleasepobean extends GenericFormBean {

	private static final long serialVersionUID = 1L;

	private String lesseCode;
	private String preLeaseContractRefDate;
	private String preLeaseContractRefSerial;
	private String poSerial;
	private String poDate;
	private String poNumber;
	private String supplierId;
	private String poAmount;
	private String poAmountFormat;
	private String assetClassifi;
	private String quantity;
	private String assetDescription;
	private String assetModel;
	private String configuration;
	private String remarks;
	private String xmleleasepoGrid;
	private String aggrementDate;

	public String getLesseCode() {
		return lesseCode;
	}

	public void setLesseCode(String lesseCode) {
		this.lesseCode = lesseCode;
	}

	public String getPreLeaseContractRefDate() {
		return preLeaseContractRefDate;
	}

	public void setPreLeaseContractRefDate(String preLeaseContractRefDate) {
		this.preLeaseContractRefDate = preLeaseContractRefDate;
	}

	public String getPreLeaseContractRefSerial() {
		return preLeaseContractRefSerial;
	}

	public void setPreLeaseContractRefSerial(String preLeaseContractRefSerial) {
		this.preLeaseContractRefSerial = preLeaseContractRefSerial;
	}

	public String getPoSerial() {
		return poSerial;
	}

	public void setPoSerial(String poSerial) {
		this.poSerial = poSerial;
	}

	public String getPoDate() {
		return poDate;
	}

	public void setPoDate(String poDate) {
		this.poDate = poDate;
	}

	public String getPoNumber() {
		return poNumber;
	}

	public void setPoNumber(String poNumber) {
		this.poNumber = poNumber;
	}

	public String getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(String supplierId) {
		this.supplierId = supplierId;
	}

	public String getPoAmount() {
		return poAmount;
	}

	public void setPoAmount(String poAmount) {
		this.poAmount = poAmount;
	}

	public String getPoAmountFormat() {
		return poAmountFormat;
	}

	public void setPoAmountFormat(String poAmountFormat) {
		this.poAmountFormat = poAmountFormat;
	}

	public String getAssetClassifi() {
		return assetClassifi;
	}

	public void setAssetClassifi(String assetClassifi) {
		this.assetClassifi = assetClassifi;
	}

	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public String getAssetDescription() {
		return assetDescription;
	}

	public void setAssetDescription(String assetDescription) {
		this.assetDescription = assetDescription;
	}

	public String getAssetModel() {
		return assetModel;
	}

	public void setAssetModel(String assetModel) {
		this.assetModel = assetModel;
	}

	public String getConfiguration() {
		return configuration;
	}

	public void setConfiguration(String configuration) {
		this.configuration = configuration;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getXmleleasepoGrid() {
		return xmleleasepoGrid;
	}

	public void setXmleleasepoGrid(String xmleleasepoGrid) {
		this.xmleleasepoGrid = xmleleasepoGrid;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public void reset() {
		lesseCode = RegularConstants.EMPTY_STRING;
		preLeaseContractRefDate = RegularConstants.EMPTY_STRING;
		preLeaseContractRefSerial = RegularConstants.EMPTY_STRING;
		poSerial = RegularConstants.EMPTY_STRING;
		poDate = RegularConstants.EMPTY_STRING;
		poNumber = RegularConstants.EMPTY_STRING;
		supplierId = RegularConstants.EMPTY_STRING;
		poAmount = RegularConstants.EMPTY_STRING;
		poAmountFormat = RegularConstants.EMPTY_STRING;
		assetClassifi = RegularConstants.EMPTY_STRING;
		assetDescription = RegularConstants.EMPTY_STRING;
		quantity = RegularConstants.EMPTY_STRING;
		assetModel = RegularConstants.EMPTY_STRING;
		configuration = RegularConstants.EMPTY_STRING;
		remarks = RegularConstants.EMPTY_STRING;
		xmleleasepoGrid = RegularConstants.EMPTY_STRING;
		aggrementDate = RegularConstants.EMPTY_STRING;
		setProcessBO("patterns.config.framework.bo.lss.eleasepoBO");
	}

	@Override
	public void validate() {
		if (validatePreLeaseContractRefDate() && validatePreLeaseContractRefSerial() && validatePOSerial()) {
			validatePODate();
			validatePONumber();
			validateSupplierID();
			validatePoAmountFormat();
			validateGrid();
			validateRemarks();
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.set("CONTRACT_DATE", preLeaseContractRefDate);
				formDTO.set("CONTRACT_SL", preLeaseContractRefSerial);
				formDTO.set("PO_SL", poSerial);
				formDTO.set("PO_DATE", poDate);
				formDTO.set("PO_NUMBER", poNumber);
				formDTO.set("SUPPLIER_ID", supplierId);
				formDTO.set("PO_CCY", context.getBaseCurrency());
				formDTO.set("TOTAL_PO_AMOUNT", poAmount);
				if (getRectify() != null && !getRectify().equals(RegularConstants.ONE))
					formDTO.set("PO_AMOUNT_PAID_SO_FAR", RegularConstants.ZERO);
				else
					formDTO.set("PO_AMOUNT_PAID_SO_FAR", RegularConstants.ZERO);
				formDTO.set("REMARKS", remarks);

				DTDObject dtdObject = new DTDObject();
				dtdObject.addColumn(0, "SL");
				dtdObject.addColumn(1, "ASSET_TYPE_CODE");
				dtdObject.addColumn(2, "ASSET_DESCRIPTION");
				dtdObject.addColumn(3, "QUANTITY");
				dtdObject.addColumn(4, "ASSET_MODEL");
				dtdObject.addColumn(5, "ASSET_CONFIG");
				dtdObject.addColumn(6, "MODEL_REQ");
				dtdObject.addColumn(7, "CONFIG_REQ");
				dtdObject.setXML(xmleleasepoGrid);
				formDTO.setDTDObject("LEASEPOASSETS", dtdObject);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("preLeaseContractRefDate", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
	}

	public boolean validatePreLeaseContractRefDate() {
		CommonValidator validator = new CommonValidator();
		try {

			if (validator.isEmpty(preLeaseContractRefDate)) {
				getErrorMap().setError("preLeaseContractRef", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			java.util.Date dateInstance = validator.isValidDate(preLeaseContractRefDate);
			if (dateInstance == null) {
				getErrorMap().setError("preLeaseContractRef", BackOfficeErrorCodes.INVALID_DATE);
				return false;
			}
			if (validator.isDateGreaterThanCBD(preLeaseContractRefDate)) {
				getErrorMap().setError("preLeaseContractRef", BackOfficeErrorCodes.DATE_LECBD);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("preLeaseContractRef", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return false;
	}

	public boolean validatePreLeaseContractRefSerial() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("CONTRACT_DATE", preLeaseContractRefDate);
			formDTO.set("CONTRACT_SL", preLeaseContractRefSerial);
			formDTO.set(ContentManager.ACTION, getAction());
			formDTO = validatePreLeaseContractRefSerial(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("preLeaseContractRef", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("preLeaseContractRef", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validatePreLeaseContractRefSerial(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		DTObject customerDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		LeaseValidator validation = new LeaseValidator();
		RegistrationValidator validator = new RegistrationValidator();
		String contractDate = inputDTO.get("CONTRACT_DATE");
		String contractSl = inputDTO.get("CONTRACT_SL");
		try {
			if (validation.isEmpty(contractDate)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			if (validation.isEmpty(contractSl)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			if (!validation.isValidNumber(contractSl)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_NUMBER);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "CUSTOMER_ID,SUNDRY_DB_AC,AGREEMENT_NO,SCHEDULE_ID,E_STATUS");
			resultDTO = validation.validatePreLeaseContractReferenceSerial(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_NOT_EXISTS);
				return resultDTO;
			}
			if (!resultDTO.get("E_STATUS").equals("A")) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_NOT_AUTORIZED);
				return resultDTO;
			}
			String customerId = resultDTO.get("CUSTOMER_ID");
			inputDTO.set("CUSTOMER_ID", customerId);
			inputDTO.set(ContentManager.FETCH_COLUMNS, "CUSTOMER_NAME");
			customerDTO = validator.valildateCustomerID(inputDTO);
			if (customerDTO.get(ContentManager.ERROR) != null) {
				customerDTO.set(ContentManager.ERROR, customerDTO.get(ContentManager.ERROR));
				return customerDTO;
			}
			resultDTO.set("CUSTOMER_NAME", customerDTO.get("CUSTOMER_NAME"));
			inputDTO.set("CUSTOMER_ID", customerId);
			customerDTO = getAggrementDate(inputDTO);
			if (customerDTO.get(ContentManager.ERROR) != null) {
				customerDTO.set(ContentManager.ERROR, customerDTO.get(ContentManager.ERROR));
				return customerDTO;
			}
			aggrementDate = customerDTO.get("AGREEMENT_DATE");
			resultDTO.set("AGREEMENT_DATE", aggrementDate);
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
			validator.close();
		}
		return resultDTO;
	}

	public DTObject getAggrementDate(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		try {
			String sqlQuery = "SELECT TO_CHAR(AGREEMENT_DATE,'" + context.getDateFormat() + "') FROM CUSTAGREEMENT WHERE ENTITY_CODE=? AND CUSTOMER_ID=?";
			util.reset();
			util.setSql(sqlQuery);
			util.setLong(1, Long.parseLong(context.getEntityCode()));
			util.setLong(2, Long.parseLong(inputDTO.get("CUSTOMER_ID")));
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				resultDTO.set("AGREEMENT_DATE", rset.getString(1));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			util.reset();
			dbContext.close();
		}
		return resultDTO;
	}

	public boolean validatePOSerial() {
		try {
			if (getAction().equals(MODIFY)) {
				getErrorMap().setError("poSerial", BackOfficeErrorCodes.INVALID_ACTION);
				return false;
			}
			if (getRectify().equals(RegularConstants.COLUMN_ENABLE)) {
				DTObject formDTO = new DTObject();
				formDTO.set("CONTRACT_DATE", preLeaseContractRefDate);
				formDTO.set("CONTRACT_SL", preLeaseContractRefSerial);
				formDTO.set("PO_SL", poSerial);
				formDTO.set(ContentManager.ACTION, getAction());
				formDTO = validatePOSerial(formDTO);
				if (formDTO.get(ContentManager.ERROR) != null) {
					getErrorMap().setError("poSerial", formDTO.get(ContentManager.ERROR));
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("poSerial", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validatePOSerial(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		LeaseValidator validation = new LeaseValidator();
		String poSerial = inputDTO.get("PO_SL");
		try {
			if (validation.isEmpty(poSerial)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			if (!validation.isValidNumber(poSerial)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_NUMBER);
				return resultDTO;
			}
			inputDTO.set(ContentManager.TABLE, "LEASEPO");
			inputDTO.set(ContentManager.FETCH_COLUMNS, "E_STATUS");
			String columns[] = new String[] { context.getEntityCode(), inputDTO.get("CONTRACT_DATE"), inputDTO.get("CONTRACT_SL"), poSerial };
			inputDTO.setObject(ContentManager.PROGRAM_KEY, columns);
			resultDTO = validation.validatePK(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.REFERENCE_DOES_NOT_EXIST);
				return resultDTO;
			}

			if (!resultDTO.getObject(ContentManager.E_STATUS).equals(RegularConstants.EMPTY_STRING)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_ALREADY_AUTORIZED);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validatePODate() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(poDate)) {
				getErrorMap().setError("poDate", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			java.util.Date dateInstance = validation.isValidDate(poDate);
			if (dateInstance == null) {
				String poDateParam[] = { context.getDateFormat() };
				getErrorMap().setError("poDate", BackOfficeErrorCodes.INVALID_DATE, poDateParam);
				return false;
			}
			if (validation.isDateGreaterThanCBD(poDate)) {
				getErrorMap().setError("poDate", BackOfficeErrorCodes.DATE_LCBD);
				return false;
			}
			if (validation.isDateLesser(poDate, aggrementDate)) {
				getErrorMap().setError("poDate", BackOfficeErrorCodes.AGGREMENT_DATE_LESSER);
				return false;
			}
			if (validation.isDateLesser(poDate, preLeaseContractRefDate)) {
				getErrorMap().setError("poDate", BackOfficeErrorCodes.PO_DATE_GREATER);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("poDate", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validatePONumber() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(poNumber)) {
				getErrorMap().setError("poNumber", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidDescription(poNumber)) {
				getErrorMap().setError("poNumber", BackOfficeErrorCodes.INVALID_DESCRIPTION);
				return false;
			}
			DTObject formDTO = new DTObject();
			formDTO.set("CONTRACT_DATE", preLeaseContractRefDate);
			formDTO.set("CONTRACT_SL", preLeaseContractRefSerial);
			formDTO.set("PO_SL", poSerial);
			formDTO.set("PROGRAM_ID", context.getProcessID());
			formDTO.set("PO_NUMBER", poNumber);
			formDTO.set("ACTION", USAGE);
			formDTO.set("RECTIFY", getRectify());
			formDTO = validatePoNumber(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null && !formDTO.get(ContentManager.ERROR).equals(RegularConstants.EMPTY_STRING)) {
				getErrorMap().setError("poNumber", formDTO.get(ContentManager.ERROR), (Object[]) formDTO.getObject(ContentManager.ERROR_PARAM));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("poNumber", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public DTObject validatePoNumber(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		MasterValidator validation = new MasterValidator();
		try {
			resultDTO = isPoNumberExist(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	private DTObject isPoNumberExist(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		CommonValidator validator = new CommonValidator();
		try {
			String primaryKey = context.getEntityCode() + RegularConstants.PK_SEPARATOR + inputDTO.get("CONTRACT_DATE") + RegularConstants.PK_SEPARATOR + inputDTO.get("CONTRACT_SL") + RegularConstants.PK_SEPARATOR + inputDTO.get("PO_SL");
			String dedupKey = context.getEntityCode() + RegularConstants.PK_SEPARATOR + inputDTO.get("PO_NUMBER");
			DTObject formDTO = new DTObject();
			formDTO.set(ContentManager.PROGRAM_ID, inputDTO.get("PROGRAM_ID"));
			formDTO.set(ContentManager.PURPOSE_ID, "PO_NUMBER");
			formDTO.set(ContentManager.PROGRAM_KEY, primaryKey);
			formDTO.set(ContentManager.DEDUP_KEY, dedupKey);
			resultDTO = validator.isDeDupAlreadyExist(formDTO);
			if (resultDTO.get(ContentManager.ERROR) != null)
				return resultDTO;
			else if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
				if (!(RegularConstants.ONE.equals(inputDTO.get("RECTIFY")) && primaryKey.equals(resultDTO.get("LINKED_TO_PK")))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.PO_NUMBER_LINKED);
					Object[] param = { resultDTO.get("LINKED_TO_PK") };
					resultDTO.setObject(ContentManager.ERROR_PARAM, param);
					return resultDTO;
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return resultDTO;
	}

	public boolean validateSupplierID() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("SUPPLIER_ID", supplierId);
			formDTO.set(ContentManager.USER_ACTION, USAGE);
			formDTO = validateSupplierID(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("supplierId", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("supplierId", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateSupplierID(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		MasterValidator validation = new MasterValidator();
		String supplierId = inputDTO.get("SUPPLIER_ID");
		try {
			if (validation.isEmpty(supplierId)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "NAME");
			resultDTO = validation.validateSupplierID(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validatePoAmountFormat() {
		CommonValidator validator = new CommonValidator();
		DTObject formDTO = new DTObject();
		try {
			if (validator.isEmpty(poAmount)) {
				getErrorMap().setError("poAmountFormat", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (validator.isZeroAmount(poAmount)) {
				getErrorMap().setError("poAmountFormat", BackOfficeErrorCodes.AMOUNT_NOT_ZERO);
				return false;
			}
			formDTO.set("CURRENCY_CODE", context.getBaseCurrency());
			formDTO.set(ProgramConstants.AMOUNT, poAmount);
			formDTO = validator.validateCurrencySmallAmount(formDTO);
			if (formDTO.get(ContentManager.ERROR) != RegularConstants.NULL) {
				getErrorMap().setError("poAmountFormat", formDTO.get(ContentManager.ERROR));
				return false;
			}
			if (validator.isValidNegativeAmount(poAmount)) {
				getErrorMap().setError("poAmountFormat", BackOfficeErrorCodes.AMOUNT_POSITIVE);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("poAmountFormat", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return false;
	}

	public boolean validateAssetClassification(String assetClassifi) {
		try {
			DTObject formDTO = getFormDTO();
			formDTO.set("ASSET_TYPE_CODE", assetClassifi);
			formDTO.set(ContentManager.ACTION, USAGE);
			formDTO = validateAssetClassification(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("assetClassifi", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("assetClassifi", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateAssetClassification(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		MasterValidator validator = new MasterValidator();
		String assetClassifi = inputDTO.get("ASSET_TYPE_CODE");
		try {
			if (validator.isEmpty(assetClassifi)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION,MODEL_NO,CONFIG_DTLS");
			resultDTO = validator.validateAssetType(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return resultDTO;
	}

	public boolean validateAssetDescription(String assetDesc) {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(assetDesc)) {
				getErrorMap().setError("assetDescription", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidRemarks(assetDesc)) {
				getErrorMap().setError("assetDescription", BackOfficeErrorCodes.INVALID_DESCRIPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("assetDescription", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateQuantity(String quantity) {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(quantity)) {
				getErrorMap().setError("quantity", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (validation.isZero(quantity)) {
				getErrorMap().setError("quantity", BackOfficeErrorCodes.ZERO_CHECK);
				return false;
			}
			if (!validation.isValidNumber(quantity)) {
				getErrorMap().setError("quantity", BackOfficeErrorCodes.INVALID_NUMBER);
				return false;
			}
			if (validation.isValidNegativeNumber(quantity)) {
				getErrorMap().setError("quantity", BackOfficeErrorCodes.POSITIVE_CHECK);
				return false;
			}
			if (validation.isNumberGreater(quantity, "99999")) {
				getErrorMap().setError("quantity", BackOfficeErrorCodes.INVALID_NUMBER);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("quantity", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateAssetModel(String assetModel, String modelNo) {
		CommonValidator validation = new CommonValidator();
		try {
			if (!modelNo.equals("N")) {
				if (modelNo.equals("M")) {
					if (validation.isEmpty(assetModel)) {
						getErrorMap().setError("assetModel", BackOfficeErrorCodes.FIELD_BLANK);
						return false;
					}
				}
				if (!validation.isEmpty(assetModel)) {
					if (!validation.isValidOtherInformation25(assetModel)) {
						getErrorMap().setError("assetModel", BackOfficeErrorCodes.INVALID_DESCRIPTION);
						return false;
					}
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("assetModel", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateConfiguration(String configuration, String configDtl) {
		CommonValidator validation = new CommonValidator();
		try {
			if (!configDtl.equals("N")) {
				if (configDtl.equals("M")) {
					if (validation.isEmpty(configuration)) {
						getErrorMap().setError("configuration", BackOfficeErrorCodes.FIELD_BLANK);
						return false;
					}
				}
				if (!validation.isEmpty(configuration)) {
					if (!validation.isValidOtherInformation25(configuration)) {
						getErrorMap().setError("configuration", BackOfficeErrorCodes.INVALID_DESCRIPTION);
						return false;
					}
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("configuration", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateRemarks() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (remarks != null && remarks.length() > 0) {
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			if (getRectify().equals(RegularConstants.COLUMN_ENABLE)) {
				if (commonValidator.isEmpty(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}

	private boolean validateGrid() {
		CommonValidator validation = new CommonValidator();
		try {
			DTDObject dtdObject = new DTDObject();
			dtdObject.addColumn(0, "SL");
			dtdObject.addColumn(1, "ASSET_TYPE_CODE");
			dtdObject.addColumn(2, "ASSET_DESCRIPTION");
			dtdObject.addColumn(3, "QUANTITY");
			dtdObject.addColumn(4, "ASSET_MODEL");
			dtdObject.addColumn(5, "ASSET_CONFIG");
			dtdObject.addColumn(6, "MODEL_REQ");
			dtdObject.addColumn(7, "CONFIG_REQ");
			dtdObject.setXML(xmleleasepoGrid);
			if (dtdObject.getRowCount() <= 0) {
				getErrorMap().setError("assetClassifi", BackOfficeErrorCodes.ATLEAST_ONE_ROW);
				return false;
			}
			for (int i = 0; i < dtdObject.getRowCount(); i++) {
				if (!validateAssetClassification(dtdObject.getValue(i, 1))) {
					getErrorMap().setError("assetClassifi", BackOfficeErrorCodes.INVALID_ACTION);
					return false;
				}
				if (!validateAssetDescription(dtdObject.getValue(i, 2))) {
					getErrorMap().setError("quantity", BackOfficeErrorCodes.INVALID_ACTION);
					return false;
				}
				if (!validateQuantity(dtdObject.getValue(i, 3))) {
					getErrorMap().setError("quantity", BackOfficeErrorCodes.INVALID_ACTION);
					return false;
				}
				if (!validateAssetModel(dtdObject.getValue(i, 4), dtdObject.getValue(i, 6))) {
					getErrorMap().setError("assetModel", BackOfficeErrorCodes.INVALID_ACTION);
					return false;
				}
				if (!validateConfiguration(dtdObject.getValue(i, 5), dtdObject.getValue(i, 7))) {
					getErrorMap().setError("configuration", BackOfficeErrorCodes.INVALID_ACTION);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("assetClassifi", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}
}
