
/*
 *
 Author : Raja E
 Created Date : 07-04-2017
 Spec Reference : ELEASEPOINVUPLD
 Modification History
 ----------------------------------------------------------------------
 Sl.No		Modified Date	Author			Modified Changes	Version
 ----------------------------------------------------------------------

 */



package patterns.config.web.forms.lss;

import java.io.FileInputStream;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Arrays;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.framework.web.DHTMLXGridUtility;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.AccessValidator;
import patterns.config.validations.BaseDomainValidator;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.LeaseValidator;
import patterns.config.validations.RegistrationValidator;
import patterns.config.web.forms.GenericFormBean;

public class eleasepoinvupldbean extends GenericFormBean {

	private static final long serialVersionUID = 1L;

	private String uploadDate;
	private String uploadSerial;
	private String preLeaseContRefDate;
	private String preLeaseContRefSerial;
	private String poSerial;
	private String fileName;
	private String fileInventoryNumber;
	private String fileExtensionList;
	private String fileExt;
	private String referenceNo;
	private String filePath;
	private String remarks;
	private String redirectLandingPagePath;
	private String fileExtensionListError;
	private String tempSerial;

	public String getUploadDate() {
		return uploadDate;
	}

	public void setUploadDate(String uploadDate) {
		this.uploadDate = uploadDate;
	}

	public String getUploadSerial() {
		return uploadSerial;
	}

	public void setUploadSerial(String uploadSerial) {
		this.uploadSerial = uploadSerial;
	}

	public String getPreLeaseContRefDate() {
		return preLeaseContRefDate;
	}

	public void setPreLeaseContRefDate(String preLeaseContRefDate) {
		this.preLeaseContRefDate = preLeaseContRefDate;
	}

	public String getPreLeaseContRefSerial() {
		return preLeaseContRefSerial;
	}

	public void setPreLeaseContRefSerial(String preLeaseContRefSerial) {
		this.preLeaseContRefSerial = preLeaseContRefSerial;
	}

	public String getPoSerial() {
		return poSerial;
	}

	public void setPoSerial(String poSerial) {
		this.poSerial = poSerial;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileInventoryNumber() {
		return fileInventoryNumber;
	}

	public void setFileInventoryNumber(String fileInventoryNumber) {
		this.fileInventoryNumber = fileInventoryNumber;
	}

	public String getFileExtensionList() {
		return fileExtensionList;
	}

	public void setFileExtensionList(String fileExtensionList) {
		this.fileExtensionList = fileExtensionList;
	}

	public String getFileExt() {
		return fileExt;
	}

	public void setFileExt(String fileExt) {
		this.fileExt = fileExt;
	}

	public String getReferenceNo() {
		return referenceNo;
	}

	public void setReferenceNo(String referenceNo) {
		this.referenceNo = referenceNo;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getRedirectLandingPagePath() {
		return redirectLandingPagePath;
	}

	public void setRedirectLandingPagePath(String redirectLandingPagePath) {
		this.redirectLandingPagePath = redirectLandingPagePath;
	}

	public String getFileExtensionListError() {
		return fileExtensionListError;
	}

	public void setFileExtensionListError(String fileExtensionListError) {
		this.fileExtensionListError = fileExtensionListError;
	}

	public String getTempSerial() {
		return tempSerial;
	}

	public void setTempSerial(String tempSerial) {
		this.tempSerial = tempSerial;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public void reset() {
		// TODO Auto-generated method stub
		uploadDate = RegularConstants.EMPTY_STRING;
		uploadSerial = RegularConstants.EMPTY_STRING;
		preLeaseContRefDate = RegularConstants.EMPTY_STRING;
		preLeaseContRefSerial = RegularConstants.EMPTY_STRING;
		poSerial = RegularConstants.EMPTY_STRING;
		fileName = RegularConstants.EMPTY_STRING;
		fileInventoryNumber = RegularConstants.EMPTY_STRING;
		fileExtensionList = RegularConstants.EMPTY_STRING;
		fileExt = RegularConstants.EMPTY_STRING;
		referenceNo = RegularConstants.EMPTY_STRING;
		filePath = RegularConstants.EMPTY_STRING;
		remarks = RegularConstants.EMPTY_STRING;
		redirectLandingPagePath = RegularConstants.EMPTY_STRING;
		fileExtensionListError = RegularConstants.EMPTY_STRING;
		tempSerial = RegularConstants.EMPTY_STRING;
		DTObject resultDTO = new DTObject();
		validateFileFormatAllowed(resultDTO);
		setProcessBO("patterns.config.framework.bo.lss.eleasepoinvupldBO");
	}

	@Override
	public void validate() {
		// TODO Auto-generated method stub
		if (validateUploadDate() && validateUploadSerial()) {
			validatePreLeaseContRefDate();
			validatePreLeaseContRefSerial();
			validatePOSerial();
			validateRemarks();
			ArrayList<String> aList = new ArrayList<String>(Arrays.asList(fileExtensionList.split("\\,")));
			if (!aList.contains(fileExt)) {
				getErrorMap().setError("upload", BackOfficeErrorCodes.INVALID_FILE_FORMAT);
			}
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.set("ENTRY_DATE", uploadDate);
				formDTO.set("ENTRY_SL", uploadSerial);
				formDTO.set("CONTRACT_DATE", preLeaseContRefDate);
				formDTO.set("CONTRACT_SL", preLeaseContRefSerial);
				formDTO.set("PO_SL", poSerial);
				formDTO.set("FILE_INV_NUM", fileInventoryNumber);
				formDTO.set("REMARKS", remarks);
				formDTO.set("UPLOADED_BY", context.getUserID());
				formDTO.set("TMP_SERIAL", tempSerial);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("upload", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
	}

	public void validateFileFormatAllowed(DTObject inputDTO) {
		AccessValidator validator = new AccessValidator();
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		try {
			inputDTO.set("PGM_ID", context.getProcessID());
			inputDTO.set("PURPOSE", "LSS");
			resultDTO = validator.readFilePurposeParameter(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				fileExtensionListError = getErrorResource(BackOfficeErrorCodes.UNSPECIFIED_ERROR, null);
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				fileExtensionListError = getErrorResource(BackOfficeErrorCodes.FILEPURPOSE_CFG_UNAVAILABLE, null);
			}
			fileExtensionList = resultDTO.get("ALLOWED_EXTENSIONS");
		} catch (Exception e) {
			e.printStackTrace();
			fileExtensionListError = getErrorResource(BackOfficeErrorCodes.UNSPECIFIED_ERROR, null);
		} finally {
			validator.close();
		}
	}

	public boolean validateUploadDate() {
		CommonValidator validator = new CommonValidator();
		try {
			java.util.Date dateInstance = validator.isValidDate(uploadDate);
			if (validator.isEmpty(uploadDate)) {
				getErrorMap().setError("upload", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (dateInstance == null) {
				getErrorMap().setError("upload", BackOfficeErrorCodes.INVALID_DATE);
				return false;
			}
			if (validator.isDateGreaterThanCBD(uploadDate)) {
				getErrorMap().setError("upload", BackOfficeErrorCodes.DATE_LECBD);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("upload", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return false;
	}

	public boolean validateUploadSerial() {
		try {
			if (getAction().equals(MODIFY)) {
				getErrorMap().setError("upload", BackOfficeErrorCodes.INVALID_ACTION);
				return false;
			}
			if (getRectify().equals(RegularConstants.COLUMN_ENABLE)) {
				DTObject formDTO = new DTObject();
				formDTO.set("ENTRY_DATE", uploadDate);
				formDTO.set("ENTRY_SL", uploadSerial);
				formDTO.set(ContentManager.ACTION, getAction());
				formDTO = validateUploadSerial(formDTO);
				if (formDTO.get(ContentManager.ERROR) != null) {
					getErrorMap().setError("upload", formDTO.get(ContentManager.ERROR));
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("upload", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateUploadSerial(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		LeaseValidator validation = new LeaseValidator();
		String entrySl = inputDTO.get("ENTRY_SL");
		try {
			if (validation.isEmpty(entrySl)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			if (!validation.isValidNumber(entrySl)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_NUMBER);
				return resultDTO;
			}
			inputDTO.set(ContentManager.TABLE, "LEASEPOINVUPLD");
			inputDTO.set(ContentManager.FETCH_COLUMNS, "E_STATUS");
			String columns[] = new String[] { context.getEntityCode(), inputDTO.get("ENTRY_DATE"), entrySl };
			inputDTO.setObject(ContentManager.PROGRAM_KEY, columns);
			resultDTO = validation.validatePK(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_NOT_EXISTS);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validatePreLeaseContRefDate() {
		CommonValidator validator = new CommonValidator();
		try {

			if (validator.isEmpty(preLeaseContRefDate)) {
				getErrorMap().setError("preLeaseContRef", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			java.util.Date dateInstance = validator.isValidDate(preLeaseContRefDate);
			if (dateInstance == null) {
				getErrorMap().setError("preLeaseContRef", BackOfficeErrorCodes.INVALID_DATE);
				return false;
			}
			if (validator.isDateGreaterThanCBD(preLeaseContRefDate)) {
				getErrorMap().setError("preLeaseContRef", BackOfficeErrorCodes.DATE_LECBD);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("preLeaseContRef", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return false;
	}

	public boolean validatePreLeaseContRefSerial() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("CONTRACT_DATE", preLeaseContRefDate);
			formDTO.set("CONTRACT_SL", preLeaseContRefSerial);
			formDTO.set(ContentManager.ACTION, getAction());
			formDTO = validatePreLeaseContRefSerial(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("preLeaseContRef", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("preLeaseContRef", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validatePreLeaseContRefSerial(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		DTObject customerDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		LeaseValidator validation = new LeaseValidator();
		RegistrationValidator validator = new RegistrationValidator();
		String contractDate = inputDTO.get("CONTRACT_DATE");
		String contractSl = inputDTO.get("CONTRACT_SL");
		try {
			if (validation.isEmpty(contractDate) || validation.isEmpty(contractSl)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			if (!validation.isValidNumber(contractSl)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_NUMBER);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "CUSTOMER_ID,SUNDRY_DB_AC,AGREEMENT_NO,SCHEDULE_ID,E_STATUS");
			resultDTO = validation.validatePreLeaseContractReferenceSerial(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_SERIAL);
				return resultDTO;
			}
			if (!resultDTO.get("E_STATUS").equals("A")) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_NOT_AUTORIZED);
				return resultDTO;
			}
			String customerId = resultDTO.get("CUSTOMER_ID");
			inputDTO.set("CUSTOMER_ID", customerId);
			inputDTO.set(ContentManager.FETCH_COLUMNS, "CUSTOMER_NAME");
			customerDTO = validator.valildateCustomerID(inputDTO);
			if (customerDTO.get(ContentManager.ERROR) != null) {
				customerDTO.set(ContentManager.ERROR, customerDTO.get(ContentManager.ERROR));
				return customerDTO;
			}
			resultDTO.set("CUSTOMER_NAME", customerDTO.get("CUSTOMER_NAME"));
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
			validator.close();
		}
		return resultDTO;
	}

	public boolean validatePOSerial() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("CONTRACT_DATE", preLeaseContRefDate);
			formDTO.set("CONTRACT_SL", preLeaseContRefSerial);
			formDTO.set("PO_SL", poSerial);
			formDTO.set(ContentManager.USER_ACTION, getAction());
			formDTO = validatePOSerial(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("poSerial", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("poSerial", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validatePOSerial(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		LeaseValidator validation = new LeaseValidator();
		String poSerial = inputDTO.get("PO_SL");
		String action = inputDTO.get(ContentManager.USER_ACTION);
		try {
			if (validation.isEmpty(poSerial)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			if (!validation.isValidNumber(poSerial)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_NUMBER);
				return resultDTO;
			}
			inputDTO.set(ContentManager.TABLE, "LEASEPO");
			inputDTO.set(ContentManager.FETCH_COLUMNS, "E_STATUS");
			String columns[] = new String[] { context.getEntityCode(), inputDTO.get("CONTRACT_DATE"), inputDTO.get("CONTRACT_SL"), poSerial };
			inputDTO.setObject(ContentManager.PROGRAM_KEY, columns);
			resultDTO = validation.validatePK(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_SERIAL);
				return resultDTO;
			}
			if (!resultDTO.get("E_STATUS").equals("A")) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_UNAUTHORIZED);
				return resultDTO;
			}
			inputDTO.set("PGM_ID", "ELEASEPOINV");
			resultDTO = checkLeaseInvoice(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (ContentManager.DATA_AVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				if (resultDTO.get("E_STATUS") == RegularConstants.NULL) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNAUTHORIZED_PLC);
					return resultDTO;
				}
			}
			if (action.equals(ADD) && getRectify() != null && getRectify().equals(RegularConstants.ZERO)) {
				inputDTO.set("PGM_ID", context.getProcessID());
				resultDTO = checkLeaseInvoice(inputDTO);
				if (resultDTO.get(ContentManager.ERROR) != null) {
					resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
					return resultDTO;
				}
				if (ContentManager.DATA_AVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					if (resultDTO.get("E_STATUS") == RegularConstants.NULL) {
						resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNAUTHORIZED_PLC);
						return resultDTO;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public DTObject checkLeaseInvoice(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		String sqlQuery = RegularConstants.EMPTY_STRING;
		try {
			if (inputDTO.get("PGM_ID").equals("ELEASEPOINV"))
				sqlQuery = "SELECT E_STATUS FROM LEASEPOINV WHERE ENTITY_CODE=? AND CONTRACT_DATE=? AND CONTRACT_SL=? AND PO_SL=? AND INVOICE_SL IS NULL";
			else
				sqlQuery = "SELECT E_STATUS FROM LEASEPOINVUPLD WHERE ENTITY_CODE=? AND CONTRACT_DATE=? AND CONTRACT_SL=? AND PO_SL=?";
			util.reset();
			util.setSql(sqlQuery);
			util.setLong(1, Long.parseLong(context.getEntityCode()));
			util.setDate(2, new java.sql.Date(BackOfficeFormatUtils.getDate(inputDTO.get("CONTRACT_DATE"), context.getDateFormat()).getTime()));
			util.setInt(3, Integer.parseInt(inputDTO.get("CONTRACT_SL")));
			util.setInt(4, Integer.parseInt(inputDTO.get("PO_SL")));
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				resultDTO.set("E_STATUS", rset.getString("E_STATUS"));
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
			} else {
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			util.reset();
			dbContext.close();
		}
		return resultDTO;
	}

	public DTObject validateUploadXLSFile(DTObject inputDTO) {
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		String sqlQuery = RegularConstants.EMPTY_STRING;
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		BaseDomainValidator validation = new BaseDomainValidator();
		HSSFSheet sheet;
		try {
			resultDTO = validation.getSystemFolders(inputDTO);
			long serial = getTBAReferenceSerial(inputDTO);
			FileInputStream input = new FileInputStream(resultDTO.get("FILE_UPLOAD_PATH") + inputDTO.get("FILE_NAME"));
			HSSFWorkbook wb = new HSSFWorkbook(input);
			int noOfSheets = wb.getNumberOfSheets();
			int k = 1;
			for (int j = 0; j < noOfSheets; j++) {
				sheet = wb.getSheetAt(j);
				Row row;
				if (sheet.getLastRowNum() == 0) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FILE_CANNOT_EMPTY);
					return resultDTO;
				}
				if (sheet.getLastRowNum() > 1000 || sheet.getRow(0).getLastCellNum() > 20) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FILE_TOO_LARGE);
					return resultDTO;
				}
				for (int i = 1; i <= sheet.getLastRowNum(); i++) {
					row = sheet.getRow(i);
					util.reset();
					sqlQuery = "INSERT INTO TMPPOINVUPLDDTL(ENTITY_CODE,TMP_SERIAL,DTL_SL,CONTRACT_REF,PO_NUMBER,INVOICE_DATE,INVOICE_NUMBER,INVOICE_CCY,INVOICE_AMOUNT,PAY_SL_LIST,ASSET_TYPE_CODE,QUANTITY,ASSET_DESCRIPTION,ASSET_MODEL,ASSET_CONFIG,ASSET_VALUE)VALUES(?,?,?,?,?,TO_DATE(?,'%d-%b-%Y'),?,?,?,?,?,?,?,?,?,?)";
					util.setSql(sqlQuery);
					util.setLong(1, Long.parseLong(context.getEntityCode()));
					util.setLong(2, serial);
					util.setInt(3, k++);
					util.setString(4, row.getCell(0).toString());
					util.setString(5, row.getCell(2).toString());
					util.setString(6, row.getCell(4).toString());
					util.setString(7, row.getCell(3).toString());
					util.setString(8, context.getBaseCurrency());
					util.setString(9, row.getCell(5).toString());
					util.setString(10, row.getCell(6).toString());
					util.setString(11, row.getCell(8).toString());
					util.setString(12, row.getCell(10).toString());
					util.setString(13, row.getCell(9).toString());
					util.setString(14, row.getCell(11).toString());
					util.setString(15, row.getCell(12).toString());
					util.setString(16, row.getCell(13).toString());
					util.executeUpdate();
				}
			}
			util.reset();
			util.setMode(DBUtil.CALLABLE);
			util.setSql("{CALL SP_ELEASEPOINVUPLD_VERIFY(?,?,?,?,?,?)}");
			util.setLong(1, Long.parseLong(context.getEntityCode()));
			util.setDate(2, new java.sql.Date(BackOfficeFormatUtils.getDate(inputDTO.get("CONTRACT_DATE"), context.getDateFormat()).getTime()));
			util.setInt(3, Integer.parseInt(inputDTO.get("CONTRACT_SL")));
			util.setInt(4, Integer.parseInt(inputDTO.get("PO_SL")));
			util.setLong(5, serial);
			util.registerOutParameter(6, Types.VARCHAR);
			util.execute();
			if (util.getString(6).equals("F")) {
				resultDTO.set("TMP_SERIAL", String.valueOf(serial));
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
				return resultDTO;
			}
			resultDTO.set("TMP_SERIAL", String.valueOf(serial));
			resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			dbContext.close();
			validation.close();
		}
		return resultDTO;
	}

	public long getTBAReferenceSerial(DTObject input) {
		String sourceKey = input.get("SOURCE_KEY");
		String programId = input.get("PGM_ID");
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		long serial = 0;

		try {
			util.reset();
			util.setMode(DBUtil.CALLABLE);
			String sql = "CALL FN_GET_TBA_REFSL(?,?,?)";
			util.setSql(sql);
			util.setString(1, programId);
			util.setString(2, sourceKey);
			util.registerOutParameter(3, Types.INTEGER);
			util.execute();
			serial = util.getInt(3);

		} catch (Exception e) {
			e.printStackTrace();

		} finally {
			util.reset();
			dbContext.close();
		}
		return serial;
	}

	public DTObject loadTempDetails(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		DHTMLXGridUtility gridUtility = new DHTMLXGridUtility();
		String sqlQuery = RegularConstants.EMPTY_STRING;
		boolean recordExist = false;
		try {
			util.reset();
			util.setMode(DBUtil.PREPARED);
			sqlQuery = "SELECT DTL_SL,CONTRACT_REF,PO_NUMBER,INVOICE_DATE,INVOICE_NUMBER,INVOICE_AMOUNT,PAY_SL_LIST,ASSET_TYPE_CODE,QUANTITY,ASSET_DESCRIPTION,ASSET_MODEL,ASSET_CONFIG,ASSET_VALUE,IFNULL(ROW_ERROR,'') FROM TMPPOINVUPLDDTL WHERE ENTITY_CODE=? AND TMP_SERIAL=?";
			util.setSql(sqlQuery);
			util.setLong(1, Long.parseLong(context.getEntityCode()));
			util.setLong(2, Long.parseLong(inputDTO.get("TMP_SERIAL")));
			ResultSet rset = util.executeQuery();
			gridUtility.init();
			while (rset.next()) {
				gridUtility.startRow(rset.getString("DTL_SL"));
				gridUtility.setCell(rset.getString("DTL_SL"));
				gridUtility.setCell("<p style=color:red;>" + rset.getString("ROW_ERROR") + "</p>");
				gridUtility.setCell(rset.getString("CONTRACT_REF"));
				gridUtility.setCell(rset.getString("PO_NUMBER"));
				gridUtility.setCell(rset.getString("INVOICE_DATE"));
				gridUtility.setCell(rset.getString("INVOICE_NUMBER"));
				gridUtility.setCell(rset.getString("INVOICE_AMOUNT"));
				gridUtility.setCell(rset.getString("PAY_SL_LIST"));
				gridUtility.setCell(rset.getString("ASSET_TYPE_CODE"));
				gridUtility.setCell(rset.getString("ASSET_DESCRIPTION"));
				gridUtility.setCell(rset.getString("QUANTITY"));
				gridUtility.setCell(rset.getString("ASSET_MODEL"));
				gridUtility.setCell(rset.getString("ASSET_CONFIG"));
				gridUtility.setCell(rset.getString("ASSET_VALUE"));
				gridUtility.endRow();
				recordExist = true;
			}
			if (!recordExist) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_NOT_EXISTS);
				return resultDTO;
			}
			gridUtility.finish();
			String resultXML = gridUtility.getXML();
			inputDTO.set(ContentManager.RESULT_XML, resultXML);
			inputDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);

		} catch (Exception e) {
			e.printStackTrace();
			inputDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			dbContext.close();
		}
		return inputDTO;
	}

	public DTObject validateUploadXLSXFile(DTObject inputDTO) {
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		String sqlQuery = RegularConstants.EMPTY_STRING;
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		BaseDomainValidator validation = new BaseDomainValidator();
		XSSFSheet sheet;
		try {
			resultDTO = validation.getSystemFolders(inputDTO);
			long serial = getTBAReferenceSerial(inputDTO);
			FileInputStream input = new FileInputStream(resultDTO.get("FILE_UPLOAD_PATH") + inputDTO.get("FILE_NAME"));
			@SuppressWarnings("resource")
			XSSFWorkbook workbook = new XSSFWorkbook(input);
			int noOfSheets = workbook.getNumberOfSheets();
			int k = 1;
			for (int j = 0; j < noOfSheets; j++) {
				sheet = workbook.getSheetAt(j);
				XSSFRow row;
				if (sheet.getLastRowNum() == 0) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FILE_CANNOT_EMPTY);
					return resultDTO;
				}
				if (sheet.getLastRowNum() > 1000 || sheet.getRow(0).getLastCellNum() > 20) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FILE_TOO_LARGE);
					return resultDTO;
				}
				for (int i = 1; i <= sheet.getLastRowNum(); i++) {
					row = sheet.getRow(i);
					util.reset();
					sqlQuery = "INSERT INTO TMPPOINVUPLDDTL(ENTITY_CODE,TMP_SERIAL,DTL_SL,CONTRACT_REF,PO_NUMBER,INVOICE_DATE,INVOICE_NUMBER,INVOICE_CCY,INVOICE_AMOUNT,PAY_SL_LIST,ASSET_TYPE_CODE,QUANTITY,ASSET_DESCRIPTION,ASSET_MODEL,ASSET_CONFIG,ASSET_VALUE)VALUES(?,?,?,?,?,TO_DATE(?,'%d-%b-%Y'),?,?,?,?,?,?,?,?,?,?)";
					util.setSql(sqlQuery);
					util.setLong(1, Long.parseLong(context.getEntityCode()));
					util.setLong(2, serial);
					util.setInt(3, k++);
					util.setString(4, row.getCell(0).toString());
					util.setString(5, row.getCell(2).toString());
					util.setString(6, row.getCell(4).toString());
					util.setString(7, row.getCell(3).toString());
					util.setString(8, context.getBaseCurrency());
					util.setString(9, row.getCell(5).toString());
					util.setString(10, row.getCell(6).toString());
					util.setString(11, row.getCell(8).toString());
					util.setString(12, row.getCell(10).toString());
					util.setString(13, row.getCell(9).toString());
					util.setString(14, row.getCell(11).toString());
					util.setString(15, row.getCell(12).toString());
					util.setString(16, row.getCell(13).toString());
					util.executeUpdate();
				}
			}
			util.reset();
			util.setMode(DBUtil.CALLABLE);
			util.setSql("{CALL SP_ELEASEPOINVUPLD_VERIFY(?,?,?,?,?,?)}");
			util.setLong(1, Long.parseLong(context.getEntityCode()));
			util.setDate(2, new java.sql.Date(BackOfficeFormatUtils.getDate(inputDTO.get("CONTRACT_DATE"), context.getDateFormat()).getTime()));
			util.setInt(3, Integer.parseInt(inputDTO.get("CONTRACT_SL")));
			util.setInt(4, Integer.parseInt(inputDTO.get("PO_SL")));
			util.setLong(5, serial);
			util.registerOutParameter(6, Types.VARCHAR);
			util.execute();
			if (util.getString(6).equals("F")) {
				resultDTO.set("TMP_SERIAL", String.valueOf(serial));
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
				return resultDTO;
			}
			resultDTO.set("TMP_SERIAL", String.valueOf(serial));
			resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			dbContext.close();
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateRemarks() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (remarks != null && remarks.length() > 0) {
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			if (getRectify().equals(RegularConstants.COLUMN_ENABLE)) {
				if (commonValidator.isEmpty(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}

}
