/*
 *The program used for Customer Agreement Maintenance

 Author : ARULPRASATH A
 Created Date : 13-FEB-2017
 Spec Reference : PPBS/LSS-ECUSTAGREEMENT
 Modification History
 ----------------------------------------------------------------------
 Sl.No		Modified Date	Author			Modified Changes	Version
 ----------------------------------------------------------------------

 */

package patterns.config.web.forms.lss;

import patterns.config.constants.ProgramConstants;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.service.DTDObject;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.LeaseValidator;
import patterns.config.validations.MasterValidator;
import patterns.config.validations.RegistrationValidator;
import patterns.config.web.forms.GenericFormBean;

public class ecustagreementbean extends GenericFormBean {

	private static final long serialVersionUID = 1L;

	private String customerID;
	private String agreementSerial;
	private String agreementNo;
	private String agreementDate;
	private String sancRef;
	private String sancAmount;
	private String sancAmountFormat;
	private String sancAmount_curr;
	private String productCode;
	private String remarks;
	private String xmlProductAgreementGrid;

	public String getCustomerID() {
		return customerID;
	}

	public void setCustomerID(String customerID) {
		this.customerID = customerID;
	}

	public String getAgreementSerial() {
		return agreementSerial;
	}

	public void setAgreementSerial(String agreementSerial) {
		this.agreementSerial = agreementSerial;
	}

	public String getAgreementNo() {
		return agreementNo;
	}

	public void setAgreementNo(String agreementNo) {
		this.agreementNo = agreementNo;
	}

	public String getAgreementDate() {
		return agreementDate;
	}

	public void setAgreementDate(String agreementDate) {
		this.agreementDate = agreementDate;
	}

	public String getSancRef() {
		return sancRef;
	}

	public void setSancRef(String sancRef) {
		this.sancRef = sancRef;
	}

	public String getSancAmount() {
		return sancAmount;
	}

	public void setSancAmount(String sancAmount) {
		this.sancAmount = sancAmount;
	}

	public String getSancAmountFormat() {
		return sancAmountFormat;
	}

	public void setSancAmountFormat(String sancAmountFormat) {
		this.sancAmountFormat = sancAmountFormat;
	}

	public String getSancAmount_curr() {
		return sancAmount_curr;
	}

	public void setSancAmount_curr(String sancAmount_curr) {
		this.sancAmount_curr = sancAmount_curr;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getXmlProductAgreementGrid() {
		return xmlProductAgreementGrid;
	}

	public void setXmlProductAgreementGrid(String xmlProductAgreementGrid) {
		this.xmlProductAgreementGrid = xmlProductAgreementGrid;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public void reset() {
		customerID = RegularConstants.EMPTY_STRING;
		agreementSerial = RegularConstants.EMPTY_STRING;
		agreementNo = RegularConstants.EMPTY_STRING;
		agreementDate = RegularConstants.EMPTY_STRING;
		sancRef = RegularConstants.EMPTY_STRING;
		sancAmount_curr = RegularConstants.EMPTY_STRING;
		sancAmount = RegularConstants.EMPTY_STRING;
		remarks = RegularConstants.EMPTY_STRING;
		xmlProductAgreementGrid = RegularConstants.EMPTY_STRING;
		setProcessBO("patterns.config.framework.bo.lss.ecustagreementBO");
	}

	@Override
	public void validate() {
		CommonValidator validation = new CommonValidator();
		if (validateCustomerID() && validateAgreementSerial()) {
			validateAgreementNo();
			validateAgreementDate();
			validateSancRef();
			validateSancAmount();
			validateRemarks();
			validateGrid();
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.set("CUSTOMER_ID", customerID);
				if (getAction().equals(MODIFY))
					formDTO.set("AGREEMENT_NO", agreementSerial);
				formDTO.set("AGREEMENT_REF_NO", agreementNo);
				formDTO.set("AGREEMENT_DATE", agreementDate);
				if (!validation.isEmpty(sancRef)) {
					formDTO.set("SANCTION_REF_NO", sancRef);
				}
				formDTO.set("SANCTION_CCY", sancAmount_curr);
				formDTO.set("SANCTION_AMOUNT", sancAmount);
				formDTO.set("REMARKS", remarks);
				DTDObject dtdObject = new DTDObject();
				dtdObject.addColumn(0, "SL");
				dtdObject.addColumn(1, "ACTION");
				dtdObject.addColumn(2, "LEASE_PRODUCT_CODE");
				dtdObject.addColumn(3, "DESCRIPTION");
				dtdObject.setXML(xmlProductAgreementGrid);
				formDTO.setDTDObject("CUSTAGREEMENTPROD", dtdObject);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("customerID", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
	}

	public boolean validateCustomerID() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("CUSTOMER_ID", customerID);
			formDTO.set(ContentManager.USER_ACTION, USAGE);
			formDTO = validateCustomerID(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("customerID", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("customerID", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateCustomerID(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		RegistrationValidator validation = new RegistrationValidator();
		String customerID = inputDTO.get("CUSTOMER_ID");
		try {
			if (validation.isEmpty(customerID)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "CUSTOMER_NAME");

			resultDTO = validation.valildateCustomerID(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateAgreementSerial() {
		CommonValidator validation = new CommonValidator();
		try {
			if (getAction().equals(MODIFY)) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.set("CUSTOMER_ID", customerID);
				formDTO.set("AGREEMENT_NO", agreementSerial);
				formDTO.set(ContentManager.USER_ACTION, getAction());
				formDTO = validateAgreementSerial(formDTO);
				if (formDTO.get(ContentManager.ERROR) != null) {
					getErrorMap().setError("agreementSerial", formDTO.get(ContentManager.ERROR));
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("agreementSerial", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public DTObject validateAgreementSerial(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		LeaseValidator validation = new LeaseValidator();
		String agreementSerial = inputDTO.get("AGREEMENT_NO");
		String action = inputDTO.get(ContentManager.USER_ACTION);
		try {
			if (validation.isEmpty(agreementSerial)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "CODE_IN_USE");
			resultDTO = validation.validateCustomerAgreement(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (action.equals(ADD)) {
				if (ContentManager.DATA_AVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_EXISTS);
					return resultDTO;
				}
			} else {
				if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_NOT_EXISTS);
					return resultDTO;
				}
				if(resultDTO.get("CODE_IN_USE").equals(RegularConstants.ONE)){
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.MOD_NOT_ALLOWED);
					return resultDTO;	
				}					 
				
				
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateAgreementNo() {
		try {
			DTObject formDTO = getFormDTO();
			formDTO.set("PROGRAM_ID", context.getProcessID());
			formDTO.set("CUSTOMER_ID", customerID);
			formDTO.set("AGREEMENT_REF_NO", agreementNo);
			formDTO.set(ContentManager.ACTION, getAction());
			formDTO = validateAgreementNo(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("agreementNo", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("agreementNo", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateAgreementNo(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		DTObject resultDTO1 = new DTObject();
		LeaseValidator validation = new LeaseValidator();
		String agreementNo = inputDTO.get("AGREEMENT_REF_NO");
		try {
			if (validation.isEmpty(agreementNo)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			resultDTO1 = isAgreementNoExist(inputDTO);
			if (resultDTO1.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO1.get(ContentManager.ERROR));
				if (resultDTO1.getObject(ContentManager.ERROR_PARAM) != null)
					resultDTO.setObject(ContentManager.ERROR_PARAM, resultDTO1.getObject(ContentManager.ERROR_PARAM));
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	private DTObject isAgreementNoExist(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		CommonValidator validator = new CommonValidator();
		try {
			String primaryKey = context.getEntityCode() + RegularConstants.PK_SEPARATOR + inputDTO.get("CUSTOMER_ID")
					+ RegularConstants.PK_SEPARATOR + inputDTO.get("AGREEMENT_REF_NO");
			String dedupKey = context.getEntityCode() + RegularConstants.PK_SEPARATOR + inputDTO.get("AGREEMENT_REF_NO")
					+ RegularConstants.PK_SEPARATOR + RegularConstants.ONE;
			DTObject formDTO = new DTObject();
			formDTO.set(ContentManager.PROGRAM_ID, inputDTO.get("PROGRAM_ID"));
			formDTO.set(ContentManager.PURPOSE_ID, "AGREEMENT");
			formDTO.set(ContentManager.PROGRAM_KEY, primaryKey);
			formDTO.set(ContentManager.DEDUP_KEY, dedupKey);
			resultDTO = validator.isDeDupAlreadyExist(formDTO);
			if (resultDTO.get(ContentManager.ERROR) != null)
				return resultDTO;
			else if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
				if (!(inputDTO.get(ContentManager.ACTION).equals(MODIFY)
						&& inputDTO.get("CUSTOMER_ID").equals(resultDTO.get("LINKED_TO_PK").split("\\|")[1]))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.AGREEMENT_NO_LINKED);
					Object[] param = { resultDTO.get("LINKED_TO_PK").split("\\|")[1] };
					resultDTO.setObject(ContentManager.ERROR_PARAM, param);
					return resultDTO;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return resultDTO;
	}

	public boolean validateAgreementDate() {
		CommonValidator validation = new CommonValidator();
		try {
			java.util.Date dateInstance = validation.isValidDate(agreementDate);
			if (validation.isEmpty(agreementDate)) {
				getErrorMap().setError("agreementDate", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (dateInstance == null) {
				String agreementDateParam[] = { context.getDateFormat() };
				getErrorMap().setError("agreementDate", BackOfficeErrorCodes.INVALID_DATE, agreementDateParam);
				return false;
			}
			if (validation.isDateGreaterThanCBD(agreementDate)) {
				getErrorMap().setError("agreementDate", BackOfficeErrorCodes.DATE_LCBD);
				return false;
			}
			DTObject formDTO = new DTObject();
			formDTO.set("CUSTOMER_ID", customerID);
			formDTO.set("AGREEMENT_DATE", agreementDate);
			formDTO.set(ContentManager.USER_ACTION, getAction());
			formDTO = validateAgreementDate(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("agreementDate", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("agreementDate", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public DTObject validateAgreementDate(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		RegistrationValidator validation = new RegistrationValidator();
		String agreementDate = inputDTO.get("AGREEMENT_DATE");
		try {
			inputDTO.set("ENTITY_CODE", context.getEntityCode());
			inputDTO.set(ContentManager.ACTION, USAGE);
			inputDTO.set(ContentManager.FETCH_COLUMNS,
					"TO_CHAR(DATE_OF_REG,'" + context.getDateFormat() + "') DATE_OF_REG");
			resultDTO = validation.valildateCustomerID(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.getObject("DATE_OF_REG") != RegularConstants.NULL
					&& !resultDTO.get("DATE_OF_REG").equals(RegularConstants.EMPTY_STRING)) {
				if (validation.isDateLesser(agreementDate, resultDTO.get("DATE_OF_REG"))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.DATE_SHOULD_NOT_LESS_THAN);
					return resultDTO;
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateSancRef() {
		CommonValidator validation = new CommonValidator();
		try {
			if (!validation.isEmpty(sancRef)) {
				if (!validation.isValidDescription(sancRef)) {
					getErrorMap().setError("sancRef", BackOfficeErrorCodes.INVALID_DESCRIPTION);
					return false;
				}
				if (!validation.isValidOtherInformation25(sancRef)) {
					getErrorMap().setError("sancRef", BackOfficeErrorCodes.INVALID_VALUE);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("agreementNo", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public boolean validateSancAmount() {
		CommonValidator validator = new CommonValidator();
		DTObject formDTO = new DTObject();
		try {
			if (validator.isEmpty(sancAmount)) {
				getErrorMap().setError("fixedChargeAmountFormat", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (validator.isZeroAmount(sancAmount)) {
				getErrorMap().setError("sancAmountFormat", BackOfficeErrorCodes.AMOUNT_NOT_ZERO);
				return false;
			}
			if (validator.isValidNegativeAmount(sancAmount)) {
				getErrorMap().setError("sancAmountFormat", BackOfficeErrorCodes.PBS_POSITIVE_AMOUNT);
				return false;
			}
			formDTO.set("CURRENCY_CODE", sancAmount_curr);
			formDTO.set(ProgramConstants.AMOUNT, sancAmount);
			formDTO = validator.validateCurrencySmallAmount(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("sancAmountFormat", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("sancAmountFormat", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return false;
	}

	public boolean validateProductCode(String productCode) {
		try {
			DTObject formDTO = getFormDTO();
			formDTO.set("LEASE_PRODUCT_CODE", productCode);
			formDTO.set(ContentManager.USER_ACTION, USAGE);
			formDTO = validateProductCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("productCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("productCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateProductCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		MasterValidator validation = new MasterValidator();
		String productCode = inputDTO.get("LEASE_PRODUCT_CODE");
		try {
			if (validation.isEmpty(productCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
			resultDTO = validation.validateLeaseProductCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateRemarks() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (remarks != null && remarks.length() > 0) {
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			if (getAction().equals(MODIFY)) {
				if (commonValidator.isEmpty(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}

	private boolean validateGrid() {
		CommonValidator validation = new CommonValidator();
		try {
			DTDObject dtdObject = new DTDObject();
			dtdObject.addColumn(0, "SL");
			dtdObject.addColumn(1, "ACTION");
			dtdObject.addColumn(2, "LEASE_PRODUCT_CODE");
			dtdObject.addColumn(3, "DESCRIPTION");
			dtdObject.setXML(xmlProductAgreementGrid);
			if (dtdObject.getRowCount() <= 0) {
				getErrorMap().setError("gridError", BackOfficeErrorCodes.ATLEAST_ONE_ROW);
				return false;
			}
			if (!validation.checkDuplicateRows(dtdObject, 2)) {
				getErrorMap().setError("gridError", BackOfficeErrorCodes.DUPLICATE_DATA);
				return false;
			}
			for (int i = 0; i < dtdObject.getRowCount(); i++) {
				if (!validateProductCode(dtdObject.getValue(i, 2))) {
					getErrorMap().setError("gridError", BackOfficeErrorCodes.INVALID_ACTION);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("gridError", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}
}
