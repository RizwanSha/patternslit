package patterns.config.web.forms.lss;

import java.sql.ResultSet;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.LeaseValidator;
import patterns.config.validations.MasterValidator;
import patterns.config.validations.RegistrationValidator;
import patterns.config.web.forms.GenericFormBean;

/**
 * 
 * 
 * @version 1.0
 * @author Kalimuthu U
 * @since 20-Jan-2017 <br>
 *        <b>ELEASECLOSURE</b> <BR>
 *        <U><B> Charge Accounting Parameter </B></U> <br>
 * 
 * 
 * 
 * 
 * 
 * 
 */
public class eleaseclosurebean extends GenericFormBean {

	private static final long serialVersionUID = 1L;
	private String entryDate;
	private String entrySerial;
	private String lesseCode;
	private String agreementNumber;
	private String scheduleID;
	private boolean verifiedLeaseDetails;
	private String referenceNumber;
	private String remarks;
	private String customerID;
	private String customerName;
	private String verifiedLease;

	public String getVerifiedLease() {
		return verifiedLease;
	}

	public void setVerifiedLease(String verifiedLease) {
		this.verifiedLease = verifiedLease;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerID() {
		return customerID;
	}

	public void setCustomerID(String customerID) {
		this.customerID = customerID;
	}

	public String getEntryDate() {
		return entryDate;
	}

	public void setEntryDate(String entryDate) {
		this.entryDate = entryDate;
	}

	public String getEntrySerial() {
		return entrySerial;
	}

	public void setEntrySerial(String entrySerial) {
		this.entrySerial = entrySerial;
	}

	public String getLesseCode() {
		return lesseCode;
	}

	public void setLesseCode(String lesseCode) {
		this.lesseCode = lesseCode;
	}

	public String getAgreementNumber() {
		return agreementNumber;
	}

	public void setAgreementNumber(String agreementNumber) {
		this.agreementNumber = agreementNumber;
	}

	public String getScheduleID() {
		return scheduleID;
	}

	public void setScheduleID(String scheduleID) {
		this.scheduleID = scheduleID;
	}

	public boolean isVerifiedLeaseDetails() {
		return verifiedLeaseDetails;
	}

	public void setVerifiedLeaseDetails(boolean verifiedLeaseDetails) {
		this.verifiedLeaseDetails = verifiedLeaseDetails;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void reset() {
		entryDate = RegularConstants.EMPTY_STRING;
		entrySerial = RegularConstants.EMPTY_STRING;
		lesseCode = RegularConstants.EMPTY_STRING;
		agreementNumber = RegularConstants.EMPTY_STRING;
		scheduleID = RegularConstants.EMPTY_STRING;
		verifiedLeaseDetails = false;
		referenceNumber = RegularConstants.EMPTY_STRING;
		remarks = RegularConstants.EMPTY_STRING;
		customerID = RegularConstants.EMPTY_STRING;
		setProcessBO("patterns.config.framework.bo.lss.eleaseclosureBO");
	}

	public void validate() {
		if (validateEntryDate() && validateEntrySerial()) {
			validateLesseCode();
			validateagreementNumber();
			validateScheduleID();
			validateVerifiedLeaseDetails();
			validateReferenceNumber();
			validateRemarks();
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.setObject("ENTRY_DATE", entryDate);
				formDTO.setObject("ENTRY_SL", entrySerial);
				formDTO.set("LESSEE_CODE", lesseCode);
				formDTO.set("AGREEMENT_NO", agreementNumber);
				formDTO.set("SCHEDULE_ID", scheduleID);
				formDTO.setObject("CLOSURE_ON", context.getCurrentBusinessDate());
				formDTO.set("CLOSURE_BY", context.getUserID());
				formDTO.set("REFERENCE_NO", referenceNumber);
				formDTO.set("REMARKS", remarks);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("entryDate", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
	}

	private boolean validateEntryDate() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (commonValidator.isEmpty(entryDate)) {
				getErrorMap().setError("entryDate", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (commonValidator.isDateGreaterThanCBD(entryDate)) {
				getErrorMap().setError("entryDate", BackOfficeErrorCodes.INVALID_DATE);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("entryDate", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}

	private boolean validateEntrySerial() {
		CommonValidator validation = new CommonValidator();
		try {
			if (getAction().equals(MODIFY)) {
				getErrorMap().setError("entrySerial", BackOfficeErrorCodes.INVALID_ACTION);
				return false;
			}
			if (getRectify().equals(RegularConstants.COLUMN_ENABLE)) {
				DTObject input = new DTObject();
				DTObject result = new DTObject();
				if (validation.isEmpty(entrySerial)) {
					getErrorMap().setError("entrySerial", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}

				if (!validation.isValidNumber(entrySerial)) {
					getErrorMap().setError("receiptRef", BackOfficeErrorCodes.INVALID_NUMBER);
					return false;
				}
				input.set("ENTRY_DATE", entryDate);
				input.set("ENTRY_SL", entrySerial);
				input.set(ContentManager.ACTION, getAction());
				result = validateEntrySerial(input);
				if (result.get(ContentManager.ERROR) != null) {
					getErrorMap().setError("entrySerial", result.get(ContentManager.ERROR));
					return false;
				}
				return true;
			} else {
				if (!validation.isEmpty(entrySerial)) {
					getErrorMap().setError("entrySerial", BackOfficeErrorCodes.REFERENCE_SERIAL_AUTO_GENERATED);
					return false;
				}
			}

			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("entrySerial", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public DTObject validateEntrySerial(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.NULL);
		CommonValidator validator = new CommonValidator();
		try {
			inputDTO.set(ContentManager.TABLE, "LEASECLOSURE");
			inputDTO.set(ContentManager.FETCH_COLUMNS, "E_STATUS");
			String columns[] = new String[] { context.getEntityCode(), inputDTO.get("ENTRY_DATE"), inputDTO.get("ENTRY_SL") };
			inputDTO.setObject(ContentManager.PROGRAM_KEY, columns);
			resultDTO = validator.validatePK(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (getRectify().equals(RegularConstants.COLUMN_ENABLE)) {
				if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.REFERENCE_DOES_NOT_EXIST);
				}
				// AFTER AUTHORIZATION MODIFICATION NOT ALLOWED
				if (!resultDTO.getObject("E_STATUS").equals(RegularConstants.EMPTY_STRING)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.AUTH_REJ_REF_MODIFY_NA);
					return resultDTO;
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return resultDTO;
	}

	private boolean validateLesseCode() {
		MasterValidator validation = new MasterValidator();
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("LESSEE_CODE", lesseCode);
			formDTO.set(ContentManager.USER_ACTION, USAGE);
			formDTO = validateLesseCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("lesseCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("lesseCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public DTObject validateLesseCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		LeaseValidator validation = new LeaseValidator();
		RegistrationValidator validatior = new RegistrationValidator();
		String lesseCode = inputDTO.get("LESSEE_CODE");
		try {
			if (validation.isEmpty(lesseCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set("SUNDRY_DB_AC", lesseCode);
			inputDTO.set(ContentManager.FETCH, ContentManager.FETCH_ALL);
			resultDTO = validation.valildateLesseCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != RegularConstants.NULL) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			inputDTO.set("CUSTOMER_ID", resultDTO.get("CUSTOMER_ID"));
			inputDTO.set(ContentManager.FETCH_COLUMNS, "CUSTOMER_ID,CUSTOMER_NAME");
			resultDTO = validatior.valildateCustomerID(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
			validatior.close();
		}
		return resultDTO;
	}

	public DTObject valildateCustomerID(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		try {
			String sqlQuery = "SELECT CUSTOMER_ID,LEASE_PRODUCT_CODE FROM CUSTOMERACDTL WHERE ENTITY_CODE=? AND SUNDRY_DB_AC=?";
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, context.getEntityCode());
			util.setString(2, inputDTO.get("SUNDRY_DB_AC"));
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				resultDTO.set("CUSTOMER_ID", rset.getString("CUSTOMER_ID"));
				resultDTO.set("LEASE_PRODUCT_CODE", rset.getString("LEASE_PRODUCT_CODE"));
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			util.reset();
			dbContext.close();
		}
		return resultDTO;
	}

	public String getCustomerID(DTObject inputDTO) {
		String sqlQuery = ContentManager.EMPTY_STRING;
		sqlQuery = "SELECT CUSTOMER_ID FROM CUSTOMERACDTL WHERE ENTITY_CODE=? AND SUNDRY_DB_AC=?";
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		String customerId = null;
		try {
			util.setMode(DBUtil.PREPARED);
			util.setSql(sqlQuery);
			util.setString(1, context.getEntityCode());
			util.setString(2, inputDTO.get("SUNDRY_DB_AC"));
			ResultSet rs = util.executeQuery();
			if (rs.next()) {
				customerId = rs.getString(1);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			util.reset();
			dbContext.close();
		}
		return customerId;
	}

	private boolean validateagreementNumber() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("CUSTOMER_ID", customerID);
			formDTO.set("AGREEMENT_NO", agreementNumber);
			formDTO.set(ContentManager.USER_ACTION, USAGE);
			formDTO = validateagreementNumber(formDTO);
			if (formDTO.get(ContentManager.ERROR) != RegularConstants.NULL) {
				getErrorMap().setError("agreementNumber", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("agreementNumber", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateagreementNumber(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		LeaseValidator validation = new LeaseValidator();
		String agreementNo = inputDTO.get("AGREEMENT_NO");
		String customerID = inputDTO.get("CUSTOMER_ID");
		try {
			if (validation.isEmpty(agreementNo)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set("CUSTOMER_ID", customerID);
			inputDTO.set("AGREEMENT_NO", agreementNo);
			resultDTO = validation.validateCustomerAgreement(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != RegularConstants.NULL) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	private boolean validateScheduleID() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("LESSEE_CODE", lesseCode);
			formDTO.set("AGREEMENT_NO", agreementNumber);
			formDTO.set("SCHEDULE_ID", scheduleID);
			formDTO.set("ENTRY_DATE_PK", entryDate);
			formDTO.set("ENTRY_SL_PK", entrySerial);
			formDTO.set(ContentManager.USER_ACTION, USAGE);
			formDTO = validateScheduleID(formDTO);
			if (formDTO.get(ContentManager.ERROR) != RegularConstants.NULL) {
				getErrorMap().setError("scheduleID", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("scheduleID", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateScheduleID(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		LeaseValidator validation = new LeaseValidator();
		String scheduleId = inputDTO.get("SCHEDULE_ID");
		try {
			if (validation.isEmpty(scheduleId)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "E_STATUS,LEASE_CLOSURE_ON");
			resultDTO = validation.validateScheduleID(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != RegularConstants.NULL) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.REFERENCE_DOES_NOT_EXIST);
				return resultDTO;
			}
			if (!resultDTO.get("E_STATUS").equals(RegularConstants.AUTHORIZED)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_REFERENCE);
				return resultDTO;
			}
			if (resultDTO.getObject("LEASE_CLOSURE_ON") != RegularConstants.NULL) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.LEASE_CLOSED);
				return resultDTO;
			}
			resultDTO = validateScheduleIDCombination(inputDTO);

		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public DTObject validateScheduleIDCombination(DTObject inputDTO) {
		String sqlQuery = ContentManager.EMPTY_STRING;
		DTObject resultDTO = new DTObject();
		sqlQuery = "SELECT TO_CHAR(ENTRY_DATE,'" + context.getDateFormat() + "')ENTRY_DATE,ENTRY_SL,E_STATUS FROM LEASECLOSURE WHERE ENTITY_CODE=? AND LESSEE_CODE=? AND AGREEMENT_NO=? AND SCHEDULE_ID=? AND E_STATUS IS NOT NULL";
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		try {
			util.setMode(DBUtil.PREPARED);
			util.setSql(sqlQuery);
			util.setString(1, context.getEntityCode());
			util.setString(2, inputDTO.get("LESSEE_CODE"));
			util.setString(3, inputDTO.get("AGREEMENT_NO"));
			util.setString(4, inputDTO.get("SCHEDULE_ID"));
			ResultSet rs = util.executeQuery();
			if (rs.next()) {
				resultDTO.set("ENTRY_DATE", rs.getString(1));
				resultDTO.set("ENTRY_SL", Integer.toString(rs.getInt(2)));
				resultDTO.set("E_STATUS", rs.getString(3));
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
			} else {
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			util.reset();
			dbContext.close();
		}
		return resultDTO;
	}

	private boolean validateVerifiedLeaseDetails() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (!verifiedLeaseDetails) {
				getErrorMap().setError("verifiedLease", BackOfficeErrorCodes.VERIFIED);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("verifiedLease", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}

	private boolean validateReferenceNumber() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (!commonValidator.isEmpty(referenceNumber)) {
				if (!commonValidator.isValidOtherInformation25(referenceNumber)) {
					getErrorMap().setError("referenceNumber", BackOfficeErrorCodes.INVALID_VALUE);
					return false;
				}
				return true;
			} else if (commonValidator.isEmpty(referenceNumber))
				return true;

		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("referenceNumber", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}

	private boolean validateRemarks() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (remarks != null && remarks.length() > 0) {
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			if (commonValidator.isEmpty(remarks)) {
				getErrorMap().setError("remarks", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!commonValidator.isValidRemarks(remarks)) {
				getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
				return false;
			}

			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}
}
