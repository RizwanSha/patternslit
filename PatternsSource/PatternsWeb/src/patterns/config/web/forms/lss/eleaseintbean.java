/*
 *The program used for Lease Interest Amount Entry

 Author : ASWAQ ASU
 Created Date : 02-MAR-2017
 Spec Reference : PPBS/LSS-ELEASEINT
 Modification History
 ----------------------------------------------------------------------
 Sl.No		Modified Date	Author			Modified Changes	Version
 ----------------------------------------------------------------------

 */

package patterns.config.web.forms.lss;

import java.sql.ResultSet;

import patterns.config.constants.ProgramConstants;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.LeaseValidator;
import patterns.config.validations.RegistrationValidator;
import patterns.config.web.forms.GenericFormBean;

public class eleaseintbean extends GenericFormBean {

	private static final long serialVersionUID = 1L;

	private String lesseCode;
	private String customerID;
	private String aggrementNumber;
	private String scheduleID;
	private String entrySerial;
	private String dateOfEntry;
	private String fromDate;
	private String uptoDate;
	private String noOfDays;
	private String interestRate;
	private String interestOnAmount;
	private String interestOnAmountFormat;
	private String interestOnAmount_curr;
	private String amountToBeBilled;
	private String amountToBeBilledFormat;
	private String amountToBeBilled_curr;
	private String dateOfInvoicingBilling;
	private String remarks;
	private String w_basis;
	private String w_ro_choice;
	private String w_ro_factor;
	
	
	public String getAmountToBeBilledFormat() {
		return amountToBeBilledFormat;
	}

	public void setAmountToBeBilledFormat(String amountToBeBilledFormat) {
		this.amountToBeBilledFormat = amountToBeBilledFormat;
	}


	
	public String getAmountToBeBilled() {
		return amountToBeBilled;
	}

	public void setAmountToBeBilled(String amountToBeBilled) {
		this.amountToBeBilled = amountToBeBilled;
	}

	public String getAmountToBeBilled_curr() {
		return amountToBeBilled_curr;
	}

	public void setAmountToBeBilled_curr(String amountToBeBilled_curr) {
		this.amountToBeBilled_curr = amountToBeBilled_curr;
	}
	
	public String getEntrySerial() {
		return entrySerial;
	}

	public void setEntrySerial(String entrySerial) {
		this.entrySerial = entrySerial;
	}

	public String getW_basis() {
		return w_basis;
	}

	public void setW_basis(String w_basis) {
		this.w_basis = w_basis;
	}

	public String getW_ro_choice() {
		return w_ro_choice;
	}

	public void setW_ro_choice(String w_ro_choice) {
		this.w_ro_choice = w_ro_choice;
	}

	public String getW_ro_factor() {
		return w_ro_factor;
	}

	public void setW_ro_factor(String w_ro_factor) {
		this.w_ro_factor = w_ro_factor;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public String getLesseCode() {
		return lesseCode;
	}

	public void setLesseCode(String lesseCode) {
		this.lesseCode = lesseCode;
	}

	public String getCustomerID() {
		return customerID;
	}

	public void setCustomerID(String customerID) {
		this.customerID = customerID;
	}

	public String getAggrementNumber() {
		return aggrementNumber;
	}

	public void setAggrementNumber(String aggrementNumber) {
		this.aggrementNumber = aggrementNumber;
	}

	public String getScheduleID() {
		return scheduleID;
	}

	public void setScheduleID(String scheduleID) {
		this.scheduleID = scheduleID;
	}

	public String getEntryserial() {
		return entrySerial;
	}

	public void setEntryserial(String entryserial) {
		this.entrySerial = entryserial;
	}

	public String getDateOfEntry() {
		return dateOfEntry;
	}

	public void setDateOfEntry(String dateOfEntry) {
		this.dateOfEntry = dateOfEntry;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getUptoDate() {
		return uptoDate;
	}

	public void setUptoDate(String uptoDate) {
		this.uptoDate = uptoDate;
	}

	public String getNoOfDays() {
		return noOfDays;
	}

	public void setNoOfDays(String noOfDays) {
		this.noOfDays = noOfDays;
	}

	public String getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(String interestRate) {
		this.interestRate = interestRate;
	}

	public String getInterestOnAmount() {
		return interestOnAmount;
	}

	public void setInterestOnAmount(String interestOnAmount) {
		this.interestOnAmount = interestOnAmount;
	}

	public String getInterestOnAmountFormat() {
		return interestOnAmountFormat;
	}

	public void setInterestOnAmountFormat(String interestOnAmountFormat) {
		this.interestOnAmountFormat = interestOnAmountFormat;
	}

	public String getInterestOnAmount_curr() {
		return interestOnAmount_curr;
	}

	public void setInterestOnAmount_curr(String interestOnAmount_curr) {
		this.interestOnAmount_curr = interestOnAmount_curr;
	}

	public String getDateOfInvoicingBilling() {
		return dateOfInvoicingBilling;
	}

	public void setDateOfInvoicingBilling(String dateOfInvoicingBilling) {
		this.dateOfInvoicingBilling = dateOfInvoicingBilling;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	
		
	@Override
	public void reset() {
		lesseCode = RegularConstants.EMPTY_STRING;
		customerID = RegularConstants.EMPTY_STRING;
		aggrementNumber = RegularConstants.EMPTY_STRING;
		scheduleID = RegularConstants.EMPTY_STRING;
		entrySerial = RegularConstants.EMPTY_STRING;
		dateOfEntry = RegularConstants.EMPTY_STRING;
		fromDate = RegularConstants.EMPTY_STRING;
		uptoDate = RegularConstants.EMPTY_STRING;
		noOfDays = RegularConstants.EMPTY_STRING;
		interestRate = RegularConstants.EMPTY_STRING;
		interestOnAmount = RegularConstants.EMPTY_STRING;
		interestOnAmountFormat = RegularConstants.EMPTY_STRING;
		interestOnAmount_curr = RegularConstants.EMPTY_STRING;
		dateOfInvoicingBilling = RegularConstants.EMPTY_STRING;
		remarks = RegularConstants.EMPTY_STRING;
		setProcessBO("patterns.config.framework.bo.lss.eleaseintBO");
	}

	@Override
	public void validate() {
		if (validateLesseCode() && validateAggrementNumber() && validateScheduleID() && validateEntrySerial()) {
			validateFromDate();
			validateUptoDate();
			validateInterestOnAmount();
			validateDateOfInvoicingBilling();
			validateRemarks();
		} 
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.set("LESSEE_CODE", lesseCode);
				formDTO.set("AGREEMENT_NO", aggrementNumber);
				formDTO.set("SCHEDULE_ID", scheduleID);
				formDTO.set("ENTRY_SL", entrySerial);
				formDTO.set("DATE_OF_ENTRY", dateOfEntry);
				formDTO.set("INT_FROM_DATE", fromDate);
				formDTO.set("INT_UPTO_DATE", uptoDate);
				formDTO.set("INT_FOR_DAYS", noOfDays);
				formDTO.set("APPLIED_INT_RATE", interestRate);
				formDTO.set("INT_ON_AMOUNT_CCY", interestOnAmount_curr);
				formDTO.set("INT_ON_AMOUNT", interestOnAmount);
				formDTO.set("INV_AMOUNT_CCY", amountToBeBilled_curr);
				formDTO.set("INV_AMOUNT", amountToBeBilled);
				formDTO.set("INV_DATE", dateOfInvoicingBilling);
				formDTO.set("FINANCE_SCHED_DTL_SL", RegularConstants.NULL);
				formDTO.set("REMARKS", remarks);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("lesseCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
	}

	private boolean validateLesseCode() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("SUNDRY_DB_AC", lesseCode);
			formDTO.set(ContentManager.USER_ACTION, USAGE);
			formDTO = validateLesseCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != RegularConstants.NULL) {
				getErrorMap().setError("lesseCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("lesseCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateLesseCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		LeaseValidator validation = new LeaseValidator();
		RegistrationValidator validator = new RegistrationValidator();
		String lesseCode = inputDTO.get("SUNDRY_DB_AC");
		try {
			if (validation.isEmpty(lesseCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH, ContentManager.FETCH_ALL);
			resultDTO = validation.valildateLesseCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != RegularConstants.NULL) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			inputDTO.set("CUSTOMER_ID", resultDTO.get("CUSTOMER_ID"));
			inputDTO.set(ContentManager.FETCH_COLUMNS, "CUSTOMER_ID,CUSTOMER_NAME");
			resultDTO = validator.valildateCustomerID(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != RegularConstants.NULL) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
			validator.close();
		}
		return resultDTO;
	}
	private boolean validateAggrementNumber() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("CUSTOMER_ID", customerID);
			formDTO.set("AGREEMENT_NO", aggrementNumber);
			formDTO.set(ContentManager.USER_ACTION, USAGE);
			formDTO = validateAggrementNumber(formDTO);
			if (formDTO.get(ContentManager.ERROR) != RegularConstants.NULL) {
				getErrorMap().setError("aggrementNumber", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("aggrementNumber", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateAggrementNumber(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		LeaseValidator validation = new LeaseValidator();
		String aggrementNumber = inputDTO.get("AGREEMENT_NO");
		try {
			if (validation.isEmpty(aggrementNumber)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH, ContentManager.FETCH_ALL);
			resultDTO = validation.validateCustomerAgreement(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != RegularConstants.NULL) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	private boolean validateScheduleID() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("LESSEE_CODE", lesseCode);
			formDTO.set("AGREEMENT_NO", aggrementNumber);
			formDTO.set("SCHEDULE_ID", scheduleID);
			formDTO.set(ContentManager.USER_ACTION, USAGE);
			formDTO = validateScheduleID(formDTO);
			if (formDTO.get(ContentManager.ERROR) != RegularConstants.NULL) {
				getErrorMap().setError("scheduleID", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("scheduleID", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateScheduleID(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		DTObject installDetailsDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		LeaseValidator validation = new LeaseValidator();
		String scheduleId = inputDTO.get("SCHEDULE_ID");
		try {
			if (validation.isEmpty(scheduleId)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "ASSET_CCY,AUTH_ON,LEASE_CLOSURE_ON,INT_RATE_FOR_PRIOR_INVEST");
			resultDTO = validation.validateScheduleID(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != RegularConstants.NULL) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
			if (resultDTO.getObject("AUTH_ON") == null) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNAUTHORIZED_LEASE);
				return resultDTO;
			}
			if (resultDTO.getObject("LEASE_CLOSURE_ON") != null) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.LEASE_CLOSED);
				return resultDTO;
			}
			installDetailsDTO = validateInstallDetails();
			w_basis =  installDetailsDTO.get("DAY_BASIS");
			w_ro_choice = installDetailsDTO.get("ROUND_OFF_CHOICE");
			w_ro_factor = installDetailsDTO.get("ROUND_OFF_FACTOR");
			interestOnAmount_curr = resultDTO.get("ASSET_CCY");
			amountToBeBilled_curr = resultDTO.get("ASSET_CCY");
			interestRate = resultDTO.get("INT_RATE_FOR_PRIOR_INVEST");
			resultDTO.set("DAY_BASIS", w_basis);
			resultDTO.set("ROUND_OFF_CHOICE", w_ro_choice);
			resultDTO.set("ROUND_OFF_FACTOR", w_ro_factor);
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}
	

	public boolean validateEntrySerial() {
		try {
			if (getAction().equals(MODIFY)) {
				getErrorMap().setError("entrySerial", BackOfficeErrorCodes.INVALID_ACTION);
				return false;
			}
			if (getRectify().equals(RegularConstants.COLUMN_ENABLE)) {
				DTObject formDTO = new DTObject();
				formDTO.set("LESSEE_CODE", lesseCode);
				formDTO.set("AGREEMENT_NO", aggrementNumber);
				formDTO.set("SCHEDULE_ID", scheduleID);
				formDTO.set("ENTRY_SL", entrySerial);
				formDTO.set(ContentManager.ACTION, getAction());
				formDTO = validateEntrySerial(formDTO);
				if (formDTO.get(ContentManager.ERROR) != null) {
					getErrorMap().setError("entrySerial", formDTO.get(ContentManager.ERROR));
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("entrySerial", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateEntrySerial(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		RegistrationValidator validation = new RegistrationValidator();
		String entrySerial = inputDTO.get("ENTRY_SL");
		try {
			if (validation.isEmpty(entrySerial)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			if (!validation.isValidNumber(entrySerial)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_NUMBER);
				return resultDTO;
			}
			inputDTO.set(ContentManager.TABLE, "LEASEINT");
			inputDTO.set(ContentManager.FETCH, ContentManager.FETCH_ALL);
			String columns[] = new String[] { context.getEntityCode(), inputDTO.get("LESSEE_CODE"), inputDTO.get("AGREEMENT_NO"), inputDTO.get("SCHEDULE_ID"), entrySerial };
			inputDTO.setObject(ContentManager.PROGRAM_KEY, columns);
			resultDTO = validation.validatePK(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	
	
	public boolean validateFromDate() {
		CommonValidator validation = new CommonValidator();
		try {
			java.util.Date dateInstance = validation.isValidDate(fromDate);
			if (validation.isEmpty(fromDate)) {
				getErrorMap().setError("fromDate", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (dateInstance == null) {
				String agreementDateParam[] = { context.getDateFormat() };
				getErrorMap().setError("fromDate", BackOfficeErrorCodes.INVALID_DATE, agreementDateParam);
				return false;
			}
			if (validation.isDateGreaterThanCBD(fromDate)) {
				getErrorMap().setError("fromDate", BackOfficeErrorCodes.DATE_LECBD);
				return false;
			}
			DTObject formDTO = new DTObject();
			formDTO.set("LESSEE_CODE", lesseCode);
			formDTO.set("AGREEMENT_NO", aggrementNumber);
			formDTO.set("SCHEDULE_ID", scheduleID);
			formDTO.set("FROM_DATE", fromDate);
			formDTO.set(ContentManager.USER_ACTION, getAction());
			formDTO = validateFromDate(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("fromDate", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("fromDate", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public DTObject validateFromDate(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		LeaseValidator validation = new LeaseValidator();
		String fromDate = inputDTO.get("FROM_DATE");
		try {
			inputDTO.set("ENTITY_CODE", context.getEntityCode());
			inputDTO.set(ContentManager.ACTION, USAGE);
			inputDTO.set(ContentManager.FETCH_COLUMNS, "TO_CHAR(DATE_OF_COMMENCEMENT,'" + context.getDateFormat() + "') DATE_OF_COMMENCEMENT,TO_CHAR(DATE_OF_CREATION,'" + context.getDateFormat() + "') DATE_OF_CREATION");
			resultDTO = validation.validateScheduleID(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != RegularConstants.NULL) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.getObject("DATE_OF_COMMENCEMENT") != RegularConstants.NULL && !resultDTO.get("DATE_OF_COMMENCEMENT").equals(RegularConstants.EMPTY_STRING)) {
				if (!validation.isDateLesser(fromDate, resultDTO.get("DATE_OF_COMMENCEMENT"))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.DATE_CANNOT_BE_AFTER_LEASE_COMM);
					return resultDTO;
				}
			}
			if (resultDTO.getObject("DATE_OF_CREATION") != RegularConstants.NULL && !resultDTO.get("DATE_OF_CREATION").equals(RegularConstants.EMPTY_STRING)) {
				if (validation.isDateLesser(fromDate, resultDTO.get("DATE_OF_CREATION"))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.DATE_CANNOT_BE_PRIOR_TO_LEASE_CREATION);
					return resultDTO;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateUptoDate() {
		CommonValidator validation = new CommonValidator();
		try {
			java.util.Date dateInstance = validation.isValidDate(uptoDate);
			if (validation.isEmpty(uptoDate)) {
				getErrorMap().setError("uptoDate", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (dateInstance == null) {
				String agreementDateParam[] = { context.getDateFormat() };
				getErrorMap().setError("uptoDate", BackOfficeErrorCodes.INVALID_DATE, agreementDateParam);
				return false;
			}
			if (validation.isDateLesser(uptoDate,fromDate)) {
				getErrorMap().setError("uptoDate", BackOfficeErrorCodes.DATE_GEFD); 
				return false;
			}
			if (validation.isDateGreaterThanCBD(uptoDate)) {
				getErrorMap().setError("uptoDate", BackOfficeErrorCodes.DATE_LECBD);
				return false;
			}
			DTObject formDTO = new DTObject();
			formDTO.set("LESSEE_CODE", lesseCode);
			formDTO.set("AGREEMENT_NO", aggrementNumber);
			formDTO.set("SCHEDULE_ID", scheduleID);
			formDTO.set("UPTO_DATE", uptoDate);
			formDTO.set(ContentManager.USER_ACTION, getAction());
			formDTO = validateUptoDate(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("uptoDate", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("uptoDate", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public DTObject validateUptoDate(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		LeaseValidator validation = new LeaseValidator();
		String uptoDate = inputDTO.get("UPTO_DATE");

		try {
			inputDTO.set("ENTITY_CODE", context.getEntityCode());
			inputDTO.set(ContentManager.ACTION, USAGE);
			inputDTO.set(ContentManager.FETCH_COLUMNS, "TO_CHAR(DATE_OF_COMMENCEMENT,'" + context.getDateFormat() + "') DATE_OF_COMMENCEMENT");
			resultDTO = validation.validateScheduleID(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != RegularConstants.NULL) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.getObject("DATE_OF_COMMENCEMENT") != RegularConstants.NULL && !resultDTO.get("DATE_OF_COMMENCEMENT").equals(RegularConstants.EMPTY_STRING)) {
				if (!validation.isDateLesser(uptoDate, resultDTO.get("DATE_OF_COMMENCEMENT"))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.DATE_CANNOT_BE_AFTER_LEASE_COMM);
					return resultDTO;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}
	
	public DTObject validateAmountToBeBilled(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		CommonValidator validation = new CommonValidator();
		double interestAmount =0.00;
		double interestRate =0.00;
		double noOfDays =0.00;
		int w_basis =0;
		if(inputDTO.get("INTEREST_AMOUNT") != RegularConstants.NULL)
			interestAmount = Double.parseDouble(inputDTO.get("INTEREST_AMOUNT"));
		if(inputDTO.get("INTEREST_RATE") != RegularConstants.NULL)
			interestRate = Double.parseDouble(inputDTO.get("INTEREST_RATE"));
		if(inputDTO.get("NO_OF_DAYS") != RegularConstants.NULL)
			noOfDays = Double.parseDouble(inputDTO.get("NO_OF_DAYS"));
		if(inputDTO.get("W_BASIS") != RegularConstants.NULL)
			w_basis = Integer.parseInt(inputDTO.get("W_BASIS"));
		try {
			double interestRateAfterDivision = interestRate / 100;
			double noFDaysBasisRatio = noOfDays  / w_basis ;
			double amountToBeFilled = interestAmount * interestRateAfterDivision * noFDaysBasisRatio;
			inputDTO.set("AMOUNT", String.valueOf(amountToBeFilled));
			resultDTO = validation.roundOff(inputDTO);
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateAmountToBeFilled() {
		CommonValidator validator = new CommonValidator();
		DTObject formDTO = new DTObject();
		try {
			if (validator.isEmpty(amountToBeBilled)) {
				getErrorMap().setError("amountToBeBilledFormat", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			formDTO.set("CURRENCY_CODE", amountToBeBilled_curr);
			formDTO.set(ProgramConstants.AMOUNT, amountToBeBilled);
			formDTO = validator.validateCurrencySmallAmount(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("amountToBeBilledFormat", formDTO.get(ContentManager.ERROR));
				return false;
			}
			if (validator.isValidNegativeAmount(amountToBeBilled)) {
				getErrorMap().setError("amountToBeBilledFormat", BackOfficeErrorCodes.PBS_POSITIVE_AMOUNT);
				return false;
			}
			DTObject interestDTO = new DTObject();
			interestDTO.set("INTEREST_AMOUNT", interestOnAmount);
			interestDTO.set("INTEREST_RATE", interestRate);
			interestDTO.set("NO_OF_DAYS", noOfDays);
			interestDTO.set("W_BASIS", w_basis);
			interestDTO.set("RND_FACT",w_ro_factor);
			interestDTO.set("RND_CHOICE", w_ro_choice);
			formDTO = validateAmountToBeBilled(interestDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("amountToBeBilled", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("amountToBeBilledFormat", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return false;
	}

	public boolean validateInterestOnAmount() {
		CommonValidator validator = new CommonValidator();
		DTObject formDTO = new DTObject();
		try {
			if (validator.isEmpty(interestOnAmount)) {
				getErrorMap().setError("interestOnAmountFormat", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			formDTO.set("CURRENCY_CODE", interestOnAmount_curr);
			formDTO.set(ProgramConstants.AMOUNT, interestOnAmount);
			formDTO = validator.validateCurrencySmallAmount(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("interestOnAmountFormat", formDTO.get(ContentManager.ERROR));
				return false;
			}
			if (validator.isZeroAmount(interestOnAmount)) {
				getErrorMap().setError("interestOnAmountFormat", BackOfficeErrorCodes.AMOUNT_NOT_ZERO);
				return false;
			}
			if (validator.isValidNegativeAmount(interestOnAmount)) {
				getErrorMap().setError("interestOnAmountFormat", BackOfficeErrorCodes.PBS_POSITIVE_AMOUNT);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("interestOnAmountFormat", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return false;
	}
	
	public boolean validateDateOfInvoicingBilling() {

		CommonValidator validator = new CommonValidator();
		try {
			java.util.Date dateInstance = validator.isValidDate(dateOfInvoicingBilling);
			if (validator.isEmpty(dateOfInvoicingBilling)) {
				getErrorMap().setError("dateOfInvoicingBilling", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (dateInstance == null) {
				String agreementDateParam[] = { context.getDateFormat() };
				getErrorMap().setError("dateOfInvoicingBilling", BackOfficeErrorCodes.INVALID_DATE, agreementDateParam);
				return false;
			}
			if (validator.isDateLesserCBD(dateOfInvoicingBilling)) {
				getErrorMap().setError("dateOfInvoicingBilling", BackOfficeErrorCodes.DATE_GECBD);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("dateOfInvoicingBilling", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return false;
		
	}
	
	public final DTObject validateInstallDetails() {
		DBContext dbContext = new DBContext();
		DTObject resultDTO = new DTObject();
		try {
			DBUtil util = dbContext.createUtilInstance();
			util.reset();
			util.setMode(DBUtil.PREPARED);
			util.setSql("SELECT * FROM INSTALL WHERE PARTITION_NO=?");
			util.setString(1, context.getPartitionNo());
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				resultDTO.set("DAY_BASIS", rset.getString("DAY_BASIS"));
				resultDTO.set("ROUND_OFF_CHOICE", rset.getString("ROUND_OFF_CHOICE"));
				resultDTO.set("ROUND_OFF_FACTOR", rset.getString("ROUND_OFF_FACTOR"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			dbContext.close();
		}
		return resultDTO;
	}
	
	public boolean validateRemarks() {
		CommonValidator commonValidator = new CommonValidator();
		try {
				if (remarks!=null && remarks.length()>0) {
					if(!commonValidator.isValidRemarks(remarks)){
						getErrorMap().setError("remarks", BackOfficeErrorCodes.HMS_INVALID_REMARKS);
						return false;
					}
				}
				if (getRectify().equals(RegularConstants.COLUMN_ENABLE)) {
					if (commonValidator.isEmpty(remarks)) {
						getErrorMap().setError("remarks", BackOfficeErrorCodes.HMS_FIELD_BLANK);
						return false;
					}
					if(!commonValidator.isValidRemarks(remarks)){
						getErrorMap().setError("remarks", BackOfficeErrorCodes.HMS_INVALID_REMARKS);
						return false;
					}
				}
				return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}
}
