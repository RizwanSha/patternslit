package patterns.config.web.forms.lss;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.process.TBAActionType;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.MasterValidator;
import patterns.config.web.forms.GenericFormBean;

/**
 * 
 * 
 * @version 1.0
 * @author Kalimuthu U
 * @since 15-MAR-2017 <br>
 *        <b>ILEASEINTPARAM</b> <BR>
 *        <U><B> Lease Interest Debit Note Parameters </B></U> <br>
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 */
public class ileaseintparambean extends GenericFormBean {

	private static final long serialVersionUID = 1L;
	private String productCode;
	private String portfolioCode;
	private String effectiveDate;
	private String remittanceAccNumber;
	private String remittanceIFSC;
	private boolean enabled;
	private String remarks;

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getPortfolioCode() {
		return portfolioCode;
	}

	public void setPortfolioCode(String portfolioCode) {
		this.portfolioCode = portfolioCode;
	}

	public String getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public String getRemittanceAccNumber() {
		return remittanceAccNumber;
	}

	public void setRemittanceAccNumber(String remittanceAccNumber) {
		this.remittanceAccNumber = remittanceAccNumber;
	}

	public String getRemittanceIFSC() {
		return remittanceIFSC;
	}

	public void setRemittanceIFSC(String remittanceIFSC) {
		this.remittanceIFSC = remittanceIFSC;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void reset() {
		productCode = RegularConstants.EMPTY_STRING;
		portfolioCode = RegularConstants.EMPTY_STRING;
		effectiveDate = RegularConstants.EMPTY_STRING;
		remittanceAccNumber = RegularConstants.EMPTY_STRING;
		remittanceIFSC = RegularConstants.EMPTY_STRING;
		remarks = RegularConstants.EMPTY_STRING;
		enabled = false;
		setProcessBO("patterns.config.framework.bo.lss.ileaseintparamBO");
	}

	public void validate() {
		if (validateProductCode() && validatePortfolioCode() && validateEffectiveDate()) {
			validateRemittanceAccNumber();
			validateRemittanceIFSC();
			validateRemarks();
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.setObject("LEASE_PRODUCT_CODE", productCode);
				formDTO.setObject("PORTFOLIO_CODE", portfolioCode);
				formDTO.set("EFF_DATE", effectiveDate);
				formDTO.set("REMIT_TO_ACNO", remittanceAccNumber);
				formDTO.set("IFSC_CODE", remittanceIFSC);
				if (getAction().equals(ADD)) {
					formDTO.set("ENABLED", decodeBooleanToString(true));
				} else {
					formDTO.set("ENABLED", decodeBooleanToString(enabled));
				}
				formDTO.set("REMARKS", remarks);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("productCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
	}

	private boolean validateProductCode() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("LEASE_PRODUCT_CODE", productCode);
			formDTO.set("ACTION", getAction());
			formDTO = validateProductCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("productCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("productCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateProductCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		MasterValidator validation = new MasterValidator();
		String productCode = inputDTO.get("LEASE_PRODUCT_CODE");
		try {
			if (validation.isEmpty(productCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
			resultDTO = validation.validateLeaseProductCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	private boolean validatePortfolioCode() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("PORTFOLIO_CODE", portfolioCode);
			formDTO.set("ACTION", USAGE);
			formDTO = validatePortfolioCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("portfolioCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("portfolioCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validatePortfolioCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		MasterValidator validation = new MasterValidator();
		String portfolioCode = inputDTO.get("PORTFOLIO_CODE");
		try {
			if (validation.isEmpty(portfolioCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
			resultDTO = validation.validatePortFolioCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	private boolean validateEffectiveDate() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("LEASE_PRODUCT_CODE", productCode);
			formDTO.set("PORTFOLIO_CODE", portfolioCode);
			formDTO.set("EFF_DATE", effectiveDate);
			formDTO.set(ContentManager.ACTION, getAction());
			formDTO = validateEffectiveDate(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("effectiveDate", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("effectiveDate", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateEffectiveDate(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		CommonValidator commonvalidation = new CommonValidator();
		String productCode = inputDTO.get("LEASE_PRODUCT_CODE");
		String portfolioCode = inputDTO.get("PORTFOLIO_CODE");
		String effectiveDate = inputDTO.get("EFF_DATE");
		String action = inputDTO.get(ContentManager.USER_ACTION);
		try {
			if (commonvalidation.isEmpty(effectiveDate)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			TBAActionType AT;
			if (action.equals(ADD))
				AT = TBAActionType.ADD;
			else
				AT = TBAActionType.MODIFY;
			if (!commonvalidation.isEffectiveDateValid(effectiveDate, AT)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_EFFECTIVE_DATE);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH, ContentManager.FETCH_ALL);
			inputDTO.set(ContentManager.TABLE, "LEASEINTPARAMHIST");
			String columns[] = new String[] { context.getEntityCode(), productCode, portfolioCode, effectiveDate };
			inputDTO.setObject(ContentManager.PROGRAM_KEY, columns);
			resultDTO = commonvalidation.validatePK(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (action.equals(ADD)) {
				if (ContentManager.DATA_AVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_EXISTS);
					return resultDTO;
				}
			} else if (action.equals(MODIFY)) {
				if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_NOT_EXISTS);
					return resultDTO;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonvalidation.close();
		}
		return resultDTO;
	}

	private boolean validateRemittanceAccNumber() {
		CommonValidator validator = new CommonValidator();
		try {
			if (validator.isEmpty(remittanceAccNumber)) {
				getErrorMap().setError("remittanceAccNumber", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validator.isValidCodePattern(remittanceAccNumber)) {
				getErrorMap().setError("remittanceAccNumber", BackOfficeErrorCodes.INVALID_NUMBER);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remittanceAccNumber", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	private boolean validateRemittanceIFSC() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("IFSC_CODE", remittanceIFSC);
			formDTO.set("ACTION", getAction());
			formDTO = validateRemittanceIFSC(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("remittanceIFSC", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remittanceIFSC", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateRemittanceIFSC(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		DTObject resultDTOStateCodeDesc = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		MasterValidator validation = new MasterValidator();
		String remittanceIFSC = inputDTO.get("IFSC_CODE");
		try {
			if (validation.isEmpty(remittanceIFSC)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "BANK_NAME,BRANCH_NAME,CITY,STATE_CODE");
			resultDTO = validation.validateIFSCCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
			inputDTO.set("STATE_CODE", resultDTO.get("STATE_CODE"));
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
			resultDTOStateCodeDesc = validation.validateStateCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
			resultDTO.set("DESCRIPTION", resultDTOStateCodeDesc.get("DESCRIPTION"));
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateRemarks() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (remarks != null && remarks.length() > 0) {
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			if (getAction().equals(MODIFY)) {
				if (commonValidator.isEmpty(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}
}
