package patterns.config.web.forms.lss;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.process.TBAActionType;
import patterns.config.framework.service.DTDObject;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.DHTMLXGridUtility;
import patterns.config.framework.web.GenericOption;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.EmployeeValidator;
import patterns.config.validations.MasterValidator;
import patterns.config.web.forms.GenericFormBean;

import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;

import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;

public class istaffportfoliobean extends GenericFormBean {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String branchCode;
	private String effectiveDate;
	private String roleCode;
	private String category;
	private String noOfStaff;
	private String staffID;
	private String portfolioCode;
	private boolean enabled;
	private String remarks;
	private String xmlStaffDetailGrid;
	private String roleCodeHide;
	private String categoryHide;
	private String noOfStaffHide;
	private String xmlRoleHierarchyGrid;
	private String staffGridInfo;
	private String mainSerial;

	DTDObject roleHierarchyDTDObject = new DTDObject();
	DTDObject staffDetailDTDObject = new DTDObject();

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public String getRoleCode() {
		return roleCode;
	}

	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getNoOfStaff() {
		return noOfStaff;
	}

	public void setNoOfStaff(String noOfStaff) {
		this.noOfStaff = noOfStaff;
	}

	public String getStaffID() {
		return staffID;
	}

	public void setStaffID(String staffID) {
		this.staffID = staffID;
	}

	public String getPortfolioCode() {
		return portfolioCode;
	}

	public void setPortfolioCode(String portfolioCode) {
		this.portfolioCode = portfolioCode;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getXmlStaffDetailGrid() {
		return xmlStaffDetailGrid;
	}

	public void setXmlStaffDetailGrid(String xmlStaffDetailGrid) {
		this.xmlStaffDetailGrid = xmlStaffDetailGrid;
	}

	public String getCategoryHide() {
		return categoryHide;
	}

	public void setCategoryHide(String categoryHide) {
		this.categoryHide = categoryHide;
	}

	public String getRoleCodeHide() {
		return roleCodeHide;
	}

	public void setRoleCodeHide(String roleCodeHide) {
		this.roleCodeHide = roleCodeHide;
	}

	public String getNoOfStaffHide() {
		return noOfStaffHide;
	}

	public void setNoOfStaffHide(String noOfStaffHide) {
		this.noOfStaffHide = noOfStaffHide;
	}

	public String getXmlRoleHierarchyGrid() {
		return xmlRoleHierarchyGrid;
	}

	public void setXmlRoleHierarchyGrid(String xmlRoleHierarchyGrid) {
		this.xmlRoleHierarchyGrid = xmlRoleHierarchyGrid;
	}

	public String getStaffGridInfo() {
		return staffGridInfo;
	}

	public void setStaffGridInfo(String staffGridInfo) {
		this.staffGridInfo = staffGridInfo;
	}

	public String getMainSerial() {
		return mainSerial;
	}

	public void setMainSerial(String mainSerial) {
		this.mainSerial = mainSerial;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public void reset() {
		branchCode = RegularConstants.EMPTY_STRING;
		effectiveDate = RegularConstants.EMPTY_STRING;
		roleCode = RegularConstants.EMPTY_STRING;
		category = RegularConstants.EMPTY_STRING;
		noOfStaff = RegularConstants.EMPTY_STRING;
		staffID = RegularConstants.EMPTY_STRING;
		portfolioCode = RegularConstants.EMPTY_STRING;
		enabled = false;
		remarks = RegularConstants.EMPTY_STRING;
		mainSerial = RegularConstants.EMPTY_STRING;
		xmlRoleHierarchyGrid = RegularConstants.EMPTY_STRING;
		xmlStaffDetailGrid = RegularConstants.EMPTY_STRING;
		getRoleHierarchySl();
		DBContext dbContext = new DBContext();
		ArrayList<GenericOption> severityList = getGenericOptions("COMMON", "ROLE_CATEGORY", dbContext, null);
		webContext.getRequest().setAttribute("COMMON_ROLE_CATEGORY", severityList);
		dbContext.close();
		setProcessBO("patterns.config.framework.bo.lss.istaffportfolioBO");
	}

	@Override
	public void validate() {
		if (validateBranchCode() && validateEffectiveDate()) {
			validateGrid();
			validateRemarks();
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.set("BRANCH_CODE", branchCode);
				formDTO.set("EFF_DATE", effectiveDate);
				if (getAction().equals(ADD)) {
					formDTO.set("ENABLED", decodeBooleanToString(true));
				} else {
					formDTO.set("ENABLED", decodeBooleanToString(enabled));
				}
				formDTO.set("REMARKS", remarks);
				formingStaffDTDObject();
				formDTO.setDTDObject("ROLEHIERARCHYDTL", roleHierarchyDTDObject);
				formDTO.setDTDObject("STAFFDETAIL", staffDetailDTDObject);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("stateCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
	}

	private void formingStaffDTDObject() {
		CommonValidator validator = new CommonValidator();
		try {
			DTDObject roleDTDObjectTemp = new DTDObject();
			DTDObject staffDTDObjectTemp = new DTDObject();

			roleDTDObjectTemp.addColumn(0, "STAFF_ROLE_CODE");
			roleDTDObjectTemp.addColumn(1, "STAFF_DESCRIPTION");
			roleDTDObjectTemp.addColumn(2, "CATEGORY");
			roleDTDObjectTemp.addColumn(3, "NOOFSTAFF");
			roleDTDObjectTemp.addColumn(4, "STAFF_NAME");
			roleDTDObjectTemp.addColumn(5, "STAFF_XML");
			roleDTDObjectTemp.addColumn(6, "STAFF_ROLE");
			roleDTDObjectTemp.addColumn(7, "CATEGORY_HIDE");
			roleDTDObjectTemp.setXML(xmlRoleHierarchyGrid);

			staffDTDObjectTemp.addColumn(0, "SL");
			staffDTDObjectTemp.addColumn(1, "STAFF_ROLE_CODE");
			staffDTDObjectTemp.addColumn(2, "STAFF_CODE");
			staffDTDObjectTemp.addColumn(3, "STAFF_NAME");
			staffDTDObjectTemp.addColumn(4, "PORTFOLIO_CODE");
			staffDTDObjectTemp.addColumn(5, "PORTFOLIO_NAME");

			roleHierarchyDTDObject.addColumn(0, "SL");
			roleHierarchyDTDObject.addColumn(1, "STAFF_ROLE_CODE");
			roleHierarchyDTDObject.addColumn(2, "NOOFSTAFF");

			staffDetailDTDObject.addColumn(0, "SL");
			staffDetailDTDObject.addColumn(1, "STAFF_ROLE_CODE");
			staffDetailDTDObject.addColumn(2, "DTL_SL");
			staffDetailDTDObject.addColumn(3, "STAFF_CODE");
			staffDetailDTDObject.addColumn(4, "PORTFOLIO_CODE");
			int index = 0;
			for (int i = 0; i < roleDTDObjectTemp.getRowSize(); i++) {
				if (!validator.isEmpty(roleDTDObjectTemp.getValue(i, "STAFF_XML"))) {
					roleHierarchyDTDObject.addRow();
					index = roleHierarchyDTDObject.getRowSize() - 1;
					roleHierarchyDTDObject.setValue(index, "SL", String.valueOf(i + 1));
					roleHierarchyDTDObject.setValue(index, "STAFF_ROLE_CODE", roleDTDObjectTemp.getValue(i, "STAFF_ROLE"));
					roleHierarchyDTDObject.setValue(index, "NOOFSTAFF", roleDTDObjectTemp.getValue(i, "NOOFSTAFF"));
					staffDTDObjectTemp.clearRows();
					staffDTDObjectTemp.setXML(roleDTDObjectTemp.getValue(i, "STAFF_XML"));
					int rowIndex = 0;
					for (int j = 0; j < staffDTDObjectTemp.getRowSize(); j++) {
						staffDetailDTDObject.addRow();
						rowIndex = staffDetailDTDObject.getRowSize() - 1;
						staffDetailDTDObject.setValue(rowIndex, "SL", String.valueOf(i + 1));
						staffDetailDTDObject.setValue(rowIndex, "STAFF_ROLE_CODE", staffDTDObjectTemp.getValue(j, "STAFF_ROLE_CODE"));
						staffDetailDTDObject.setValue(rowIndex, "DTL_SL", String.valueOf(j + 1));
						staffDetailDTDObject.setValue(rowIndex, "STAFF_CODE", staffDTDObjectTemp.getValue(j, "STAFF_CODE"));
						staffDetailDTDObject.setValue(rowIndex, "PORTFOLIO_CODE", staffDTDObjectTemp.getValue(j, "PORTFOLIO_CODE"));
					}
				}
			}
		} catch (Exception e) {

		} finally {
			validator.close();
		}

	}

	public void getRoleHierarchySl() {
		DTObject resultDTO = new DTObject();
		try {
			resultDTO = getRoleHierarchyDetails(resultDTO);
			mainSerial = resultDTO.get("INVENTORY_SL");
			xmlRoleHierarchyGrid = resultDTO.get(ContentManager.RESULT_XML);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public DTObject getRoleHierarchyDetails(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		try {
			util.reset();
			util.setMode(DBUtil.CALLABLE);
			util.setSql("CALL SP_ROLE_HIERARCHY(?,?,?,?)");
			util.setString(1, context.getEntityCode());
			util.registerOutParameter(2, Types.BIGINT);
			util.registerOutParameter(3, Types.CHAR);
			util.registerOutParameter(4, Types.VARCHAR);
			util.execute();
			if ("S".equals(util.getString(3))) {
				String mainSl = util.getString(2);
				inputDTO.set("INVENTORY_SL", mainSl);
				resultDTO = getRoleCodeHierarchy(inputDTO);
				resultDTO.set("INVENTORY_SL", mainSl);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			dbContext.close();
			util.reset();
		}
		return resultDTO;
	}

	public DTObject getRoleCodeHierarchy(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		CommonValidator validator = new CommonValidator();
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		DBUtil util1 = dbContext.createUtilInstance();
		String lastValue = RegularConstants.EMPTY_STRING;
		String oldStaffCode = RegularConstants.EMPTY_STRING;
		String newStaffCode = RegularConstants.EMPTY_STRING;
		String oldParentStaffCode = RegularConstants.EMPTY_STRING;
		String newParentStaffCode = RegularConstants.EMPTY_STRING;
		String oldParentStaff = RegularConstants.EMPTY_STRING;

		StringBuffer sqlQuery = new StringBuffer(RegularConstants.EMPTY_STRING);
		StringBuffer sqlStaffQuery = new StringBuffer(RegularConstants.EMPTY_STRING);
		String staffName = RegularConstants.EMPTY_STRING;
		String staffXml = RegularConstants.EMPTY_STRING;
		String fromQueryView = inputDTO.get("FROM_QUERY_VIEW");
		StringBuffer staffList = new StringBuffer(RegularConstants.EMPTY_STRING);
		StringBuffer portfolio = new StringBuffer(RegularConstants.EMPTY_STRING);
		int noOfStaff = 0;
		int count = 0;
		try {
			sqlStaffQuery.append("SELECT SR.STAFF_ROLE_CODE,SR.NOF_STAFF,SRDTL.STAFF_CODE,S.NAME STAFF_NAME,SRDTL.PORTFOLIO_CODE,P.DESCRIPTION PORTFOLIO_NAME");
			sqlStaffQuery.append(" FROM STAFFPFROLESHIST SR");
			sqlStaffQuery.append(" JOIN STAFFPFROLECONFHIST SRDTL ON(SRDTL.ENTITY_CODE=SR.ENTITY_CODE AND SRDTL.BRANCH_CODE=SR.BRANCH_CODE AND SRDTL.EFF_DATE=SR.EFF_DATE AND SRDTL.STAFF_ROLE_CODE=SR.STAFF_ROLE_CODE) ");
			sqlStaffQuery.append(" JOIN STAFF S ON(S.ENTITY_CODE=SR.ENTITY_CODE AND S.STAFF_CODE=SRDTL.STAFF_CODE AND S.STAFF_ROLE_CODE=SR.STAFF_ROLE_CODE)");
			sqlStaffQuery.append(" LEFT OUTER JOIN PORTFOLIO P ON(P.ENTITY_CODE=SR.ENTITY_CODE AND P.PORTFOLIO_CODE=SRDTL.PORTFOLIO_CODE)");
			sqlStaffQuery.append(" WHERE SR.ENTITY_CODE=? AND SR.BRANCH_CODE=? AND SR.EFF_DATE=TO_DATE(?,?) AND SR.STAFF_ROLE_CODE=? ");
			sqlStaffQuery.append(" ORDER BY SRDTL.DTL_SL ");

			util.reset();
			sqlQuery.append("SELECT SH.STAFF_ORDER,SH.PARENT_ORDER,SH.ROOT_STAFF_ROLE_CODE,SH.PARENT_STAFF_ROLE_CODE,");
			sqlQuery.append(" SH.STAFF_ROLE_CODE,SH.STAFF_CONCISE_DESCRIPTION,C.LOV_LABEL_EN_US ROLE_CATEGORY_DESC,SH.ROLE_CATEGORY ");
			sqlQuery.append(" FROM STAFF_ROLE_HIERARCHY SH");
			sqlQuery.append(" LEFT OUTER JOIN CMLOVREC C ON(C.PGM_ID='COMMON' AND C.LOV_ID='ROLE_CATEGORY' AND C.LOV_VALUE=SH.ROLE_CATEGORY) ");
			sqlQuery.append(" WHERE SH.ENTITY_CODE=? AND  SH.INVENTORY_NO=? ");
			sqlQuery.append(" ORDER BY STAFF_ORDER ");
			util.setSql(sqlQuery.toString());
			util.setString(1, context.getEntityCode());
			util.setString(2, inputDTO.get("INVENTORY_SL"));
			ResultSet rset = util.executeQuery();
			int i = 0;
			int j = 0;
			String[] newLength = null;
			String[] oldLength = null;
			DHTMLXGridUtility dhtmlUtility = new DHTMLXGridUtility();
			dhtmlUtility.init();
			while (rset.next()) {
				if (!validator.isEmpty(inputDTO.get("BRANCH_CODE"))) {
					staffXml = RegularConstants.EMPTY_STRING;
					noOfStaff = 0;
					staffName = RegularConstants.EMPTY_STRING;
					util1.reset();
					util1.setSql(sqlStaffQuery.toString());
					util1.setString(1, context.getEntityCode());
					util1.setString(2, inputDTO.get("BRANCH_CODE"));
					util1.setString(3, inputDTO.get("EFF_DATE"));
					util1.setString(4, context.getDateFormat());
					util1.setString(5, rset.getString("STAFF_ROLE_CODE"));
					ResultSet rset1 = util1.executeQuery();
					DHTMLXGridUtility staffDhtmlUtility = new DHTMLXGridUtility();
					staffDhtmlUtility.init();
					while (rset1.next()) {
						staffDhtmlUtility.startRow();
						staffDhtmlUtility.setCell(RegularConstants.EMPTY_STRING);
						staffDhtmlUtility.setCell(rset1.getString("STAFF_ROLE_CODE"));
						staffDhtmlUtility.setCell(rset1.getString("STAFF_CODE"));
						staffDhtmlUtility.setCell(rset1.getString("STAFF_NAME"));
						staffDhtmlUtility.setCell(rset1.getString("PORTFOLIO_CODE"));
						staffDhtmlUtility.setCell(rset1.getString("PORTFOLIO_NAME"));
						staffDhtmlUtility.endRow();
						noOfStaff = rset1.getInt(2);
						staffName = rset1.getString("STAFF_NAME");
						if (!validator.isEmpty(rset1.getString("PORTFOLIO_CODE"))) {
							if (staffList.length() >= 1)
								staffList.append("|").append(rset1.getString("STAFF_ROLE_CODE"));
							else
								staffList.append(rset1.getString("STAFF_ROLE_CODE"));
							if (portfolio.length() >= 1)
								portfolio.append("|").append(rset1.getString("PORTFOLIO_CODE"));
							else
								portfolio.append(rset1.getString("PORTFOLIO_CODE"));
						}
					}
					staffDhtmlUtility.finish();
					staffXml = staffDhtmlUtility.getXML();
				}
				count++;
				newStaffCode = rset.getString("ROOT_STAFF_ROLE_CODE");
				if (!oldStaffCode.equals(newStaffCode)) {
					String oldParentLength = rset.getString("STAFF_ORDER");
					String[] last = lastValue.split("\\.");
					String[] first = oldParentLength.split("\\.");
					if (first.length < last.length) {
						int latestValue = last.length - first.length;
						while (latestValue > 0) {
							dhtmlUtility.endRow();
							latestValue--;
						}
						lastValue = "";
					}
					oldParentStaffCode = "";
					if (i != 0)
						dhtmlUtility.endRow();
					i++;
					dhtmlUtility.startRow(rset.getString("ROOT_STAFF_ROLE_CODE"));
					if (RegularConstants.ONE.equals(fromQueryView))
						dhtmlUtility.setCell(rset.getString("ROOT_STAFF_ROLE_CODE"));
					else
						dhtmlUtility.setCell("<a href='javascript:void(0)' onClick='allocateStaffDetails(\"" + rset.getString("ROOT_STAFF_ROLE_CODE") + "\"," + "\"" + rset.getString("STAFF_CONCISE_DESCRIPTION") + "\"," + "\"" + rset.getString("ROLE_CATEGORY") + "\"," + "\""
								+ String.valueOf(noOfStaff) + "\")'>" + rset.getString("ROOT_STAFF_ROLE_CODE") + "</a>");
					dhtmlUtility.setCell(rset.getString("STAFF_CONCISE_DESCRIPTION"));
					dhtmlUtility.setCell(rset.getString("ROLE_CATEGORY_DESC"));
					dhtmlUtility.setCell(String.valueOf(noOfStaff));
					if (noOfStaff == 1) {
						dhtmlUtility.setCell(staffName);
					} else if (noOfStaff > 1) {
						if (RegularConstants.ONE.equals(fromQueryView)) {
							dhtmlUtility.setCell("<a href='javascript:void(0)' onClick='viewStaffDetails(\"" + rset.getString("ROOT_STAFF_ROLE_CODE") + "\"," + "\"" + rset.getString("STAFF_CONCISE_DESCRIPTION") + "\"," + "\"" + rset.getString("ROLE_CATEGORY") + "\"," + "\""
									+ String.valueOf(noOfStaff) + "\")'>View</a>");
						} else {
							dhtmlUtility.setCell("<a href='javascript:void(0)' onClick='viewStaffDetails(\"" + rset.getString("ROOT_STAFF_ROLE_CODE") + "\")'>View</a>");
						}
					} else {
						dhtmlUtility.setCell("");
					}
					if (noOfStaff > 0)
						dhtmlUtility.setCell(staffXml);
					else
						dhtmlUtility.setCell("");
					dhtmlUtility.setCell(rset.getString("ROOT_STAFF_ROLE_CODE"));
					dhtmlUtility.setCell(rset.getString("ROLE_CATEGORY"));
					j = 0;
				}
				if (oldStaffCode.equals(newStaffCode)) {
					if (j == 0) {
						oldParentStaff = rset.getString("STAFF_ORDER");
					}
					j = 1;
					newParentStaffCode = rset.getString("STAFF_ORDER");
					// newParentGlCat = rset.getString("PARENT_GL_HEAD");
					newLength = newParentStaffCode.split("\\.");
					oldLength = oldParentStaffCode.split("\\.");

					if (newLength.length > oldLength.length) {
						dhtmlUtility.startRow(rset.getString("STAFF_ROLE_CODE"));
						if (RegularConstants.ONE.equals(fromQueryView))
							dhtmlUtility.setCell(rset.getString("STAFF_ROLE_CODE"));
						else
							dhtmlUtility.setCell("<a href='javascript:void(0)' onClick='allocateStaffDetails(\"" + rset.getString("STAFF_ROLE_CODE") + "\"," + "\"" + rset.getString("STAFF_CONCISE_DESCRIPTION") + "\"," + "\"" + rset.getString("ROLE_CATEGORY") + "\"," + "\""
									+ String.valueOf(noOfStaff) + "\")'>" + rset.getString("STAFF_ROLE_CODE") + "</a>");
						dhtmlUtility.setCell(rset.getString("STAFF_CONCISE_DESCRIPTION"));
						dhtmlUtility.setCell(rset.getString("ROLE_CATEGORY_DESC"));
						dhtmlUtility.setCell(String.valueOf(noOfStaff));
						if (noOfStaff == 1) {
							dhtmlUtility.setCell(staffName);
						} else if (noOfStaff > 1) {
							if (RegularConstants.ONE.equals(fromQueryView)) {
								dhtmlUtility.setCell("<a href='javascript:void(0)' onClick='viewStaffDetails(\"" + rset.getString("STAFF_ROLE_CODE") + "\"," + "\"" + rset.getString("STAFF_CONCISE_DESCRIPTION") + "\"," + "\"" + rset.getString("ROLE_CATEGORY") + "\"," + "\""
										+ String.valueOf(noOfStaff) + "\")'>View</a>");
							} else {
								dhtmlUtility.setCell("<a href='javascript:void(0)' onClick='viewStaffDetails(\"" + rset.getString("STAFF_ROLE_CODE") + "\")'>View</a>");
							}
						} else {
							dhtmlUtility.setCell("");
						}
						if (noOfStaff > 0)
							dhtmlUtility.setCell(staffXml);
						else
							dhtmlUtility.setCell("");
						dhtmlUtility.setCell(rset.getString("STAFF_ROLE_CODE"));
						dhtmlUtility.setCell(rset.getString("ROLE_CATEGORY"));
					} else if (newLength.length < oldLength.length) {
						int latestValue = oldLength.length - newLength.length;
						while (latestValue >= 0) {
							dhtmlUtility.endRow();
							latestValue--;
						}
						dhtmlUtility.startRow(rset.getString("STAFF_ROLE_CODE"));
						if (RegularConstants.ONE.equals(fromQueryView))
							dhtmlUtility.setCell(rset.getString("STAFF_ROLE_CODE"));
						else
							dhtmlUtility.setCell("<a href='javascript:void(0)' onClick='allocateStaffDetails(\"" + rset.getString("STAFF_ROLE_CODE") + "\"," + "\"" + rset.getString("STAFF_CONCISE_DESCRIPTION") + "\"," + "\"" + rset.getString("ROLE_CATEGORY") + "\"," + "\""
									+ String.valueOf(noOfStaff) + "\")'>" + rset.getString("STAFF_ROLE_CODE") + "</a>");
						dhtmlUtility.setCell(rset.getString("STAFF_CONCISE_DESCRIPTION"));
						dhtmlUtility.setCell(rset.getString("ROLE_CATEGORY_DESC"));
						dhtmlUtility.setCell(String.valueOf(noOfStaff));
						if (noOfStaff == 1) {
							dhtmlUtility.setCell(staffName);
						} else if (noOfStaff > 1) {
							if (RegularConstants.ONE.equals(fromQueryView)) {
								dhtmlUtility.setCell("<a href='javascript:void(0)' onClick='viewStaffDetails(\"" + rset.getString("STAFF_ROLE_CODE") + "\"," + "\"" + rset.getString("STAFF_CONCISE_DESCRIPTION") + "\"," + "\"" + rset.getString("ROLE_CATEGORY") + "\"," + "\""
										+ String.valueOf(noOfStaff) + "\")'>" + rset.getString("STAFF_ROLE_CODE") + "</a>");
							} else {
								dhtmlUtility.setCell("<a href='javascript:void(0)' onClick='viewStaffDetails(\"" + rset.getString("STAFF_ROLE_CODE") + "\")'>View</a>");
							}
						} else {
							dhtmlUtility.setCell("");
						}
						if (noOfStaff > 0)
							dhtmlUtility.setCell(staffXml);
						else
							dhtmlUtility.setCell("");
						dhtmlUtility.setCell(rset.getString("STAFF_ROLE_CODE"));
						dhtmlUtility.setCell(rset.getString("ROLE_CATEGORY"));
					} else if (newLength.length == oldLength.length) {
						dhtmlUtility.endRow();
						dhtmlUtility.startRow(rset.getString("STAFF_ROLE_CODE"));
						if (RegularConstants.ONE.equals(fromQueryView)) {
							dhtmlUtility.setCell(rset.getString("STAFF_ROLE_CODE"));
						} else {
							dhtmlUtility.setCell("<a href='javascript:void(0)' onClick='allocateStaffDetails(\"" + rset.getString("STAFF_ROLE_CODE") + "\"," + "\"" + rset.getString("STAFF_CONCISE_DESCRIPTION") + "\"," + "\"" + rset.getString("ROLE_CATEGORY") + "\"," + "\""
									+ String.valueOf(noOfStaff) + "\")'>" + rset.getString("STAFF_ROLE_CODE") + "</a>");
						}
						dhtmlUtility.setCell(rset.getString("STAFF_CONCISE_DESCRIPTION"));
						dhtmlUtility.setCell(rset.getString("ROLE_CATEGORY_DESC"));
						dhtmlUtility.setCell(String.valueOf(noOfStaff));
						if (noOfStaff == 1) {
							dhtmlUtility.setCell(staffName);
						} else if (noOfStaff > 1) {
							if (RegularConstants.ONE.equals(fromQueryView)) {
								dhtmlUtility.setCell("<a href='javascript:void(0)' onClick='viewStaffDetails(\"" + rset.getString("STAFF_ROLE_CODE") + "\"," + "\"" + rset.getString("STAFF_CONCISE_DESCRIPTION") + "\"," + "\"" + rset.getString("ROLE_CATEGORY") + "\"," + "\""
										+ String.valueOf(noOfStaff) + "\")'>View</a>");
							} else {
								dhtmlUtility.setCell("<a href='javascript:void(0)' onClick='viewStaffDetails(\"" + rset.getString("STAFF_ROLE_CODE") + "\")'>View</a>");
							}
						} else {
							dhtmlUtility.setCell("");
						}
						if (noOfStaff > 0)
							dhtmlUtility.setCell(staffXml);
						else
							dhtmlUtility.setCell("");
						dhtmlUtility.setCell(rset.getString("STAFF_ROLE_CODE"));
						dhtmlUtility.setCell(rset.getString("ROLE_CATEGORY"));
					}
					oldParentStaffCode = newParentStaffCode;
					lastValue = newParentStaffCode;
				}
				oldStaffCode = newStaffCode;
			}
			String[] last = ((oldParentStaffCode.trim()).equals(RegularConstants.EMPTY_STRING)) ? null : oldParentStaffCode.split("\\.");
			String[] first = ((oldParentStaff.trim()).equals(RegularConstants.EMPTY_STRING)) ? null : oldParentStaff.split("\\.");

			if (last != null && first != null) {
				if (first.length < last.length) {
					dhtmlUtility.endRow();
					int latestValue = last.length - first.length;
					while (latestValue > 0) {
						dhtmlUtility.endRow();
						latestValue--;
					}
				}
				if (last.length == first.length) {
					dhtmlUtility.endRow();
				}
			}
			dhtmlUtility.endRow();
			dhtmlUtility.finish();
			if (count > 0) {
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				resultDTO.set(ContentManager.RESULT_XML, dhtmlUtility.getXML());
				resultDTO.set("STAFF_ROLE_LIST", staffList.toString());
				resultDTO.set("PORTFOLIO_LIST", portfolio.toString());
			} else {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.EMPTY_GRID);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			dbContext.close();
			util.reset();
			validator.close();
		}
		return resultDTO;
	}

	public boolean validateBranchCode() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("BRANCH_CODE", branchCode);
			formDTO.set(ContentManager.USER_ACTION, getAction());
			formDTO = validateBranchCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("branchCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("branchCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateBranchCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		MasterValidator validation = new MasterValidator();
		String stateCode = inputDTO.get("BRANCH_CODE");
		try {
			if (validation.isEmpty(stateCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "BRANCH_NAME");
			resultDTO = validation.validateBranchCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateEffectiveDate() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("BRANCH_CODE", branchCode);
			formDTO.set("EFF_DATE", effectiveDate);
			formDTO.set(ContentManager.USER_ACTION, getAction());
			formDTO = validateEffectiveDate(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("effectiveDate", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("effectiveDate", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateEffectiveDate(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		CommonValidator commonvalidation = new CommonValidator();
		String branchCode = inputDTO.get("BRANCH_CODE");
		String effectiveDate = inputDTO.get("EFF_DATE");
		String action = inputDTO.get(ContentManager.USER_ACTION);
		try {
			if (commonvalidation.isEmpty(effectiveDate)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			TBAActionType AT;
			if (action.equals(ADD))
				AT = TBAActionType.ADD;
			else
				AT = TBAActionType.MODIFY;
			if (!commonvalidation.isEffectiveDateValid(effectiveDate, AT)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_EFFECTIVE_DATE);
				return resultDTO;
			}
			String columns[] = new String[] { context.getEntityCode(), branchCode, effectiveDate };
			inputDTO.setObject(ContentManager.PROGRAM_KEY, columns);
			inputDTO.set(ContentManager.FETCH_COLUMNS, "BRANCH_CODE");
			inputDTO.set(ContentManager.TABLE, "STAFFPORTFOLIOHIST");
			resultDTO = commonvalidation.validatePK(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (action.equals(ADD)) {
				if (ContentManager.DATA_AVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_EXISTS);
					return resultDTO;
				}
			} else if (action.equals(MODIFY)) {
				if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_NOT_EXISTS);
					return resultDTO;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonvalidation.close();
		}
		return resultDTO;
	}

	private boolean validateGrid() {
		CommonValidator validation = new CommonValidator();
		try {
			boolean staffGridRecordExists = false;
			ArrayList<String> staffRoleList = new ArrayList<String>();
			DTDObject roleDTDObjectTemp = new DTDObject();
			DTDObject staffDTDObjectTemp = new DTDObject();
			roleDTDObjectTemp.addColumn(0, "STAFF_ROLE_CODE");
			roleDTDObjectTemp.addColumn(1, "STAFF_DESCRIPTION");
			roleDTDObjectTemp.addColumn(2, "CATEGORY");
			roleDTDObjectTemp.addColumn(3, "NOOFSTAFF");
			roleDTDObjectTemp.addColumn(4, "STAFF_NAME");
			roleDTDObjectTemp.addColumn(5, "STAFF_XML");
			roleDTDObjectTemp.addColumn(6, "STAFF_ROLE");
			roleDTDObjectTemp.addColumn(7, "CATEGORY_HIDE");
			roleDTDObjectTemp.setXML(xmlRoleHierarchyGrid);

			staffDTDObjectTemp.addColumn(0, "SL");
			staffDTDObjectTemp.addColumn(1, "STAFF_ROLE_CODE");
			staffDTDObjectTemp.addColumn(2, "STAFF_CODE");
			staffDTDObjectTemp.addColumn(3, "STAFF_NAME");
			staffDTDObjectTemp.addColumn(4, "PORTFOLIO_CODE");
			staffDTDObjectTemp.addColumn(5, "PORTFOLIO_NAME");

			if (roleDTDObjectTemp.getRowCount() <= 0) {
				getErrorMap().setError("staffGridInfo", BackOfficeErrorCodes.ATLEAST_ONE_ROW);
				return false;
			}
			if (!validation.checkDuplicateRows(roleDTDObjectTemp, 6)) {
				getErrorMap().setError("staffGridInfo", BackOfficeErrorCodes.DUPLICATE_DATA);
				return false;
			}
			int count = 0;
			for (int i = 0; i < roleDTDObjectTemp.getRowSize(); i++) {
				if (!validation.isEmpty(roleDTDObjectTemp.getValue(i, "STAFF_XML"))) {
					Object[] param = { roleDTDObjectTemp.getValue(i, 6) };
					if (roleDTDObjectTemp.getValue(i, 3).equals("1")) {
						count++;
					}
					if (count > 1) {
						getErrorMap().setError("staffGridInfo", BackOfficeErrorCodes.STAFF_CONFIG_APPLY_FR_ALL_PORTFOLIO_AVALBE, param);
						return false;
					}
					staffGridRecordExists = true;
					if (!validateRoleCode(roleDTDObjectTemp.getValue(i, 6), "1") || !validateCategory(roleDTDObjectTemp.getValue(i, 7), "2") || !validateNoOfStaff(roleDTDObjectTemp.getValue(i, 3), "3")) {
						return false;
					}
					staffDTDObjectTemp.clearRows();
					staffDTDObjectTemp.setXML(roleDTDObjectTemp.getValue(i, "STAFF_XML"));
					if (staffDTDObjectTemp.getRowCount() <= 0) {
						getErrorMap().setError("staffGridInfo", BackOfficeErrorCodes.ATLEAST_ONE_ROW);
						return false;
					}
					if (Integer.parseInt(roleDTDObjectTemp.getValue(i, 3)) > staffDTDObjectTemp.getRowCount()) {
						Object[] configParams = { roleDTDObjectTemp.getValue(i, 3), staffDTDObjectTemp.getRowCount(), roleDTDObjectTemp.getValue(i, 6) };
						getErrorMap().setError("staffGridInfo", BackOfficeErrorCodes.ALL_STAFF_NOT_CONFIGURED, configParams);
						return false;
					}
					if (!validation.checkDuplicateRows(staffDTDObjectTemp, 3, 4)) {
						getErrorMap().setError("staffGridInfo", BackOfficeErrorCodes.DUPLICATE_DATA_STAFF_PORTFOLIO_COMBO, param);
						return false;
					}
					if (!validation.checkDuplicateRows(staffDTDObjectTemp, 4)) {
						getErrorMap().setError("staffGridInfo", BackOfficeErrorCodes.PORTFOLIO_CONFIGURED_AGAIN, param);
						return false;
					}
					for (int j = 0; j < staffDTDObjectTemp.getRowCount(); j++) {
						if (roleDTDObjectTemp.getValue(i, 3).equals("1")) {
							if (!validation.isEmpty(staffDTDObjectTemp.getValue(j, 4))) {
								Object[] configParam = { roleDTDObjectTemp.getValue(i, 6) };
								getErrorMap().setError("staffGridInfo", BackOfficeErrorCodes.CANT_CNFGR_PORTFOLIO_FOR_ONE_STAFF, configParam);
								return false;
							}
						}
						if (staffRoleList.contains(staffDTDObjectTemp.getValue(j, 4))) {
							getErrorMap().setError("staffGridInfo", BackOfficeErrorCodes.PORTFOLIO_CONFIGURED_AGAIN, param);
							return false;
						} else {
							staffRoleList.add(staffDTDObjectTemp.getValue(j, 4));
						}
						if (!validateStaffCode(staffDTDObjectTemp.getValue(j, 1), staffDTDObjectTemp.getValue(j, 2)) || !validatePortfolioCode(staffDTDObjectTemp.getValue(j, 4), roleDTDObjectTemp.getValue(i, 3))) {
							return false;
						}
					}
				}
			}
			if (!staffGridRecordExists) {
				getErrorMap().setError("staffGridInfo", BackOfficeErrorCodes.STAFF_ALLOC_ATLEAST_ONE_ROLE);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("staffGridInfo", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return true;
	}

	public boolean validateNoOfStaff(String noOfStaff, String column) {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(noOfStaff)) {
				getErrorMap().setError("staffGridInfo", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
				return false;
			}
			if (validation.isValidNegativeNumber(noOfStaff)) {
				getErrorMap().setError("staffGridInfo", BackOfficeErrorCodes.POSITIVE_CHECK);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("staffGridInfo", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public boolean validateRoleCode(String staffROleCode, String column) {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("STAFF_ROLE_CODE", staffROleCode);
			formDTO.set("ACTION", USAGE);
			formDTO = validateStaffRoleCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("staffGridInfo", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("staffGridInfo", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateStaffRoleCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, ContentManager.DATA_UNAVAILABLE);
		MasterValidator validation = new MasterValidator();
		String staffRoleCode = inputDTO.get("STAFF_ROLE_CODE");
		try {
			if (validation.isEmpty(staffRoleCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
			resultDTO = validation.ValidateStaffRoleCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	private boolean validateCategory(String category, String column) {
		CommonValidator validation = new CommonValidator();
		try {
			if (!validation.isEmpty(category)) {
				if (!validation.isCMLOVREC("COMMON", "ROLE_CATEGORY", category)) {
					getErrorMap().setError("staffGridInfo", BackOfficeErrorCodes.INVALID_OPTION);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("staffGridInfo", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateStaffCode(String staffROleCode, String staffID) {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("STAFF_ROLE_CODE", staffROleCode);
			formDTO.set("BRANCH_CODE", branchCode);
			formDTO.set("STAFF_CODE", staffID);
			formDTO.set("ACTION", USAGE);
			formDTO = validateStaffCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("staffGridInfo", BackOfficeErrorCodes.CONFIGURED_SCENARIO_FAILED);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("staffGridInfo", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateStaffCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, ContentManager.DATA_UNAVAILABLE);
		EmployeeValidator validation = new EmployeeValidator();
		String staffCode = inputDTO.get("STAFF_CODE");
		String branchCode = inputDTO.get("BRANCH_CODE");
		String staffRoleCode = inputDTO.get("STAFF_ROLE_CODE");
		try {
			if (validation.isEmpty(staffCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "NAME,BRANCH_CODE,STAFF_ROLE_CODE");
			resultDTO = validation.validateStaffCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_STAFF_ID);
				return resultDTO;
			}
			if (!branchCode.equals(resultDTO.get("BRANCH_CODE"))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_STAFF_ID);
				return resultDTO;
			}
			if (!staffRoleCode.equals(resultDTO.get("STAFF_ROLE_CODE"))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.STAFF_NT_BELNGS_TO_ROLE_CNFGRD);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validatePortfolioCode(String portfolioCode, String noOfStaff) {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("PORTFOLIO_CODE", portfolioCode);
			formDTO.set("NO_OF_STAFF", noOfStaff);
			formDTO.set("ACTION", USAGE);
			formDTO = validatePortfolioCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("staffGridInfo", BackOfficeErrorCodes.CONFIGURED_SCENARIO_FAILED);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("staffGridInfo", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validatePortfolioCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.NULL);
		MasterValidator validation = new MasterValidator();
		String portfolioCode = inputDTO.get("PORTFOLIO_CODE");
		Integer noOfStaff = Integer.parseInt(inputDTO.get("NO_OF_STAFF"));
		try {
			if (noOfStaff > 1) {
				if (validation.isEmpty(portfolioCode)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
					return resultDTO;
				}
			}
			if (!validation.isEmpty(portfolioCode)) {
				inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
				resultDTO = validation.validatePortfoliocode(inputDTO);
				if (resultDTO.get(ContentManager.ERROR) != null) {
					resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
					return resultDTO;
				}
				if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_PORTFOLIO);
					return resultDTO;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateRemarks() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (remarks != null && remarks.length() > 0) {
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			if (getAction().equals(MODIFY)) {
				if (commonValidator.isEmpty(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}
}
