/*
 *
 Author : Raja E
 Created Date : 16-03-2017
 Spec Reference : EPRELEASECONTREG
 Modification History
 ----------------------------------------------------------------------
 Sl.No		Modified Date	Author			Modified Changes	Version
 ----------------------------------------------------------------------

 */

package patterns.config.web.forms.lss;

import java.sql.ResultSet;
import java.util.ArrayList;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.framework.web.GenericOption;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.EmployeeValidator;
import patterns.config.validations.LeaseValidator;
import patterns.config.validations.MasterValidator;
import patterns.config.validations.RegistrationValidator;
import patterns.config.web.forms.GenericFormBean;

public class epreleasecontregbean extends GenericFormBean {

	private static final long serialVersionUID = 1L;

	private String preLeaseContractRefDate;
	private String preLeaseContractRefSerial;
	private String customerID;
	private String lesseeCode;
	private String productCode;
	private String branchCode;
	private String addressSerial;
	private String contactSerial;
	private boolean interestApplicable;
	private String interestRateInvestLeaseCommit;
	private String interestBillingChoice;
	private String interestDebitNotePrinting;
	private String remarks;

	public String getPreLeaseContractRefDate() {
		return preLeaseContractRefDate;
	}

	public void setPreLeaseContractRefDate(String preLeaseContractRefDate) {
		this.preLeaseContractRefDate = preLeaseContractRefDate;
	}

	public String getPreLeaseContractRefSerial() {
		return preLeaseContractRefSerial;
	}

	public void setPreLeaseContractRefSerial(String preLeaseContractRefSerial) {
		this.preLeaseContractRefSerial = preLeaseContractRefSerial;
	}

	public String getCustomerID() {
		return customerID;
	}

	public void setCustomerID(String customerID) {
		this.customerID = customerID;
	}

	public String getLesseeCode() {
		return lesseeCode;
	}

	public void setLesseeCode(String lesseeCode) {
		this.lesseeCode = lesseeCode;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getAddressSerial() {
		return addressSerial;
	}

	public void setAddressSerial(String addressSerial) {
		this.addressSerial = addressSerial;
	}

	public String getContactSerial() {
		return contactSerial;
	}

	public void setContactSerial(String contactSerial) {
		this.contactSerial = contactSerial;
	}

	public boolean isInterestApplicable() {
		return interestApplicable;
	}

	public void setInterestApplicable(boolean interestApplicable) {
		this.interestApplicable = interestApplicable;
	}

	public String getInterestRateInvestLeaseCommit() {
		return interestRateInvestLeaseCommit;
	}

	public void setInterestRateInvestLeaseCommit(String interestRateInvestLeaseCommit) {
		this.interestRateInvestLeaseCommit = interestRateInvestLeaseCommit;
	}

	public String getInterestBillingChoice() {
		return interestBillingChoice;
	}

	public void setInterestBillingChoice(String interestBillingChoice) {
		this.interestBillingChoice = interestBillingChoice;
	}

	public String getInterestDebitNotePrinting() {
		return interestDebitNotePrinting;
	}

	public void setInterestDebitNotePrinting(String interestDebitNotePrinting) {
		this.interestDebitNotePrinting = interestDebitNotePrinting;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public void reset() {
		// TODO Auto-generated method stub
		preLeaseContractRefDate = RegularConstants.EMPTY_STRING;
		preLeaseContractRefSerial = RegularConstants.EMPTY_STRING;
		customerID = RegularConstants.EMPTY_STRING;
		lesseeCode = RegularConstants.EMPTY_STRING;
		productCode = RegularConstants.EMPTY_STRING;
		branchCode = RegularConstants.EMPTY_STRING;
		addressSerial = RegularConstants.EMPTY_STRING;
		contactSerial = RegularConstants.EMPTY_STRING;
		interestApplicable = false;
		interestRateInvestLeaseCommit = RegularConstants.EMPTY_STRING;
		interestBillingChoice = RegularConstants.EMPTY_STRING;
		interestDebitNotePrinting = RegularConstants.EMPTY_STRING;
		remarks = RegularConstants.EMPTY_STRING;

		setProcessBO("patterns.config.framework.bo.lss.epreleasecontregBO");
		DBContext dbContext = new DBContext();
		ArrayList<GenericOption> interimInterestBillingChoice = null;
		interimInterestBillingChoice = getGenericOptions("EPRELEASECONTREG", "INT_BILL_OPTION", dbContext, "---");
		webContext.getRequest().setAttribute("EPRELEASECONTREG_INT_BILL_OPTION", interimInterestBillingChoice);

		ArrayList<GenericOption> interestDeitNotePrint = null;
		interestDeitNotePrint = getGenericOptions("EPRELEASECONTREG", "INT_DEBIT_PRINTING", dbContext, null);
		webContext.getRequest().setAttribute("EPRELEASECONTREG_INT_DEBIT_PRINTING", interestDeitNotePrint);
		dbContext.close();
	}

	@Override
	public void validate() {
		// TODO Auto-generated method stub
		if (validatePreLeaseContractRefDate() && validatePreLeaseContractRefSerial()) {
			validateCustomerID();
			validateLesseeCode();
			validateBranchCode();
			validateAddressSerial();
			validateContactSerial();
			validateInterestRateInvestLeaseCommit();
			validateInterestBillingChoice();
			validateInterestDebitNotePrinting();
			validateRemarks();
		}

		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.set("CONTRACT_DATE", preLeaseContractRefDate);
				formDTO.set("CONTRACT_SL", preLeaseContractRefSerial);
				formDTO.set("CUSTOMER_ID", customerID);
				formDTO.set("SUNDRY_DB_AC", lesseeCode);
				formDTO.set("LEASE_PRODUCT_CODE", productCode);
				formDTO.set("BRANCH_CODE", branchCode);
				formDTO.set("ADDR_SL", addressSerial);
				formDTO.set("CONTACT_SL", contactSerial);
				formDTO.set("INTERIM_INT_APPL", decodeBooleanToString(interestApplicable));
				if (interestRateInvestLeaseCommit.isEmpty())
					formDTO.set("INT_RATE_FOR_PRIOR_INVEST", RegularConstants.NULL);
				else
					formDTO.set("INT_RATE_FOR_PRIOR_INVEST", interestRateInvestLeaseCommit);
				formDTO.set("INTERIM_INT_BILLING", interestBillingChoice);
				formDTO.set("INT_DEBIT_NOTE_PRINT_CHOICE", interestDebitNotePrinting);
				formDTO.set("REMARKS", remarks);
				formDTO.set("AGREEMENT_NO", RegularConstants.NULL);
				formDTO.set("SCHEDULE_ID", RegularConstants.NULL);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("customerID", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
	}

	public boolean validatePreLeaseContractRefDate() {
		CommonValidator validator = new CommonValidator();
		try {
			java.util.Date dateInstance = validator.isValidDate(preLeaseContractRefDate);
			if (validator.isEmpty(preLeaseContractRefDate)) {
				getErrorMap().setError("preLeaseContractRef", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (dateInstance == null) {
				getErrorMap().setError("preLeaseContractRef", BackOfficeErrorCodes.INVALID_DATE);
				return false;
			}
			if (validator.isDateGreaterThanCBD(preLeaseContractRefDate)) {
				getErrorMap().setError("preLeaseContractRef", BackOfficeErrorCodes.DATE_LECBD);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("preLeaseContractRef", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return false;
	}

	public boolean validatePreLeaseContractRefSerial() {
		try {
			if (getAction().equals(MODIFY)) {
				getErrorMap().setError("preLeaseContractRefSerial", BackOfficeErrorCodes.INVALID_ACTION);
				return false;
			}
			if (getRectify().equals(RegularConstants.COLUMN_ENABLE)) {
				DTObject formDTO = new DTObject();
				formDTO.set("CONTRACT_DATE", preLeaseContractRefDate);
				formDTO.set("CONTRACT_SL", preLeaseContractRefSerial);
				formDTO.set(ContentManager.ACTION, getAction());
				formDTO = validatePreLeaseContractRefSerial(formDTO);
				if (formDTO.get(ContentManager.ERROR) != null) {
					getErrorMap().setError("preLeaseContractRef", formDTO.get(ContentManager.ERROR));
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("preLeaseContractRef", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validatePreLeaseContractRefSerial(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		LeaseValidator validation = new LeaseValidator();
		String contractSl = inputDTO.get("CONTRACT_SL");
		try {
			if (validation.isEmpty(contractSl)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			if (!validation.isValidNumber(contractSl)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_NUMBER);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH, ContentManager.FETCH_ALL);
			resultDTO = validation.validatePreLeaseContractReferenceSerial(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_NOT_EXISTS);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateCustomerID() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("CUSTOMER_ID", customerID);
			formDTO.set(ContentManager.ACTION, USAGE);
			formDTO = validateCustomerID(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("customerID", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("customerID", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateCustomerID(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		RegistrationValidator validation = new RegistrationValidator();
		String customerId = inputDTO.get("CUSTOMER_ID");
		try {
			if (validation.isEmpty(customerId)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			if (!validation.isValidNumber(customerId)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.NUMERIC_CHECK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "CUSTOMER_NAME");
			resultDTO = validation.valildateCustomerID(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	private boolean validateLesseeCode() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("LEASE_PRODUCT_CODE", productCode);
			formDTO.set("CONTRACT_DATE", preLeaseContractRefDate);
			formDTO.set("CONTRACT_SL", preLeaseContractRefSerial);
			formDTO.set("CUSTOMER_ID", customerID);
			formDTO.set("SUNDRY_DB_AC", lesseeCode);
			if (getRectify() != null && getRectify().equals(RegularConstants.COLUMN_ENABLE))
				formDTO.set("LEASE_CONTRACT", RegularConstants.ONE);
			else
				formDTO.set("LEASE_CONTRACT", RegularConstants.ZERO);
			formDTO.set(ContentManager.USER_ACTION, USAGE);
			formDTO = validateLesseeCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null && !formDTO.get(ContentManager.ERROR).equals(RegularConstants.EMPTY_STRING)) {
				getErrorMap().setError("lesseeCode", formDTO.get(ContentManager.ERROR), (Object[]) formDTO.getObject(ContentManager.ERROR_PARAM));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("lesseeCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateLesseeCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		LeaseValidator validation = new LeaseValidator();
		MasterValidator validator = new MasterValidator();
		String lesseCode = inputDTO.get("SUNDRY_DB_AC");
		String customerId = inputDTO.get("CUSTOMER_ID");
		try {
			if (validation.isEmpty(lesseCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH, ContentManager.FETCH_ALL);
			resultDTO = validation.valildateLesseCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != RegularConstants.NULL) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
			if (!resultDTO.get("CUSTOMER_ID").equals(customerId)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.CUSTOMER_ID_NOT_ALLOWED);
				return resultDTO;
			}
			String productCode = resultDTO.get("LEASE_PRODUCT_CODE");
			inputDTO.set("LEASE_PRODUCT_CODE", productCode);
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
			resultDTO = validator.validateLeaseProductCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != RegularConstants.NULL) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			String desc = resultDTO.get("DESCRIPTION");
			resultDTO = PreLeaseContractNumber(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != RegularConstants.NULL) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (ContentManager.DATA_AVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.PRE_LEASE_CONTRACT_EXIST);
				Object[] param = { resultDTO.get("CONTRACT_DATE"), resultDTO.get("CONTRACT_SL") };
				resultDTO.setObject(ContentManager.ERROR_PARAM, param);
				return resultDTO;
			}
			resultDTO.set("LEASE_PRODUCT_CODE", productCode);
			resultDTO.set("LEASE_PRODUCT_CODE_DESC", desc);
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
			validator.close();
		}
		return resultDTO;
	}

	public DTObject PreLeaseContractNumber(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		String sqlQuery = null;
		try {
			if (inputDTO.get("LEASE_CONTRACT").equals(RegularConstants.ZERO))
				sqlQuery = "SELECT TO_CHAR(CONTRACT_DATE,'" + context.getDateFormat() + "')CONTRACT_DATE,CONTRACT_SL,SCHEDULE_ID FROM PRELEASECONTRACT WHERE ENTITY_CODE=? AND CUSTOMER_ID=? AND SUNDRY_DB_AC=? AND LEASE_PRODUCT_CODE=? AND SCHEDULE_ID IS NULL";
			else
				sqlQuery = "SELECT TO_CHAR(CONTRACT_DATE,'" + context.getDateFormat() + "')CONTRACT_DATE,CONTRACT_SL,SCHEDULE_ID FROM PRELEASECONTRACT WHERE ENTITY_CODE=? AND CUSTOMER_ID=? AND SUNDRY_DB_AC=? AND LEASE_PRODUCT_CODE=? AND (CONTRACT_DATE,CONTRACT_SL)<>(?,?) AND SCHEDULE_ID IS NULL";
			util.reset();
			util.setSql(sqlQuery);
			util.setLong(1, Long.parseLong(context.getEntityCode()));
			util.setLong(2, Long.parseLong(inputDTO.get("CUSTOMER_ID")));
			util.setString(3, inputDTO.get("SUNDRY_DB_AC"));
			util.setString(4, inputDTO.get("LEASE_PRODUCT_CODE"));
			if (inputDTO.get("LEASE_CONTRACT").equals(RegularConstants.ONE)) {
				util.setDate(5, new java.sql.Date(BackOfficeFormatUtils.getDate(inputDTO.get("CONTRACT_DATE"), context.getDateFormat()).getTime()));
				util.setInt(6, Integer.parseInt(inputDTO.get("CONTRACT_SL")));
			}
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				resultDTO.set("CONTRACT_DATE", rset.getString(1));
				resultDTO.set("CONTRACT_SL", rset.getString(2));
				resultDTO.set("SCHEDULE_ID", rset.getString(3));
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
			} else {
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			util.reset();
			dbContext.close();
		}
		return resultDTO;
	}

	public boolean validateBranchCode() {
		try {
			DTObject formDTO = getFormDTO();
			formDTO.set("BRANCH_CODE", branchCode);
			formDTO.set(ContentManager.USER_ACTION, USAGE);
			formDTO = validateBranchCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("branchCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("branchCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateBranchCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		EmployeeValidator validation = new EmployeeValidator();
		String channelCode = inputDTO.get("BRANCH_CODE");
		try {
			if (validation.isEmpty(channelCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "BRANCH_NAME");
			resultDTO = validation.validateBranchCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateAddressSerial() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("CUSTOMER_ID", customerID);
			formDTO.set("ADDR_SL", addressSerial);
			formDTO.set(ContentManager.ACTION, USAGE);
			formDTO = validateAddressSerial(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("addressSerial", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("addressSerial", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateAddressSerial(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		RegistrationValidator validation = new RegistrationValidator();
		String addrSl = inputDTO.get("ADDR_SL");
		try {
			if (validation.isEmpty(addrSl)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			if (!validation.isValidNumber(addrSl)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_NUMBER);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH, ContentManager.FETCH_ALL);
			resultDTO = validation.validateCustomerAddressSerial(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateContactSerial() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("CUSTOMER_ID", customerID);
			formDTO.set("ADDR_SL", addressSerial);
			formDTO.set("DTL_SL", contactSerial);
			formDTO.set(ContentManager.ACTION, USAGE);
			formDTO = validateContactSerial(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("contactSerial", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("contactSerial", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateContactSerial(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		RegistrationValidator validation = new RegistrationValidator();
		String contactSl = inputDTO.get("DTL_SL");
		try {
			if (validation.isEmpty(contactSl)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			if (!validation.isValidNumber(contactSl)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_NUMBER);
				return resultDTO;
			}
			resultDTO = checkContactSerial(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public DTObject checkContactSerial(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		try {
			String sqlQuery = "SELECT COUNT(1) FROM CUSTOMERCONTACTDTL WHERE ENTITY_CODE=? AND CUSTOMER_ID=? AND ADDR_SL=? AND DTL_SL=?";
			util.reset();
			util.setSql(sqlQuery);
			util.setLong(1, Long.parseLong(context.getEntityCode()));
			util.setLong(2, Long.parseLong(inputDTO.get("CUSTOMER_ID")));
			util.setInt(3, Integer.parseInt(inputDTO.get("ADDR_SL")));
			util.setInt(4, Integer.parseInt(inputDTO.get("DTL_SL")));
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
			} else {
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			util.reset();
			dbContext.close();
		}
		return resultDTO;
	}

	public boolean validateInterestRateInvestLeaseCommit() {
		CommonValidator validation = new CommonValidator();
		try {
			if (interestApplicable) {
				if (validation.isEmpty(interestRateInvestLeaseCommit)) {
					getErrorMap().setError("interestRateInvestLeaseCommit", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (validation.isZero(interestRateInvestLeaseCommit)) {
					getErrorMap().setError("interestRateInvestLeaseCommit", BackOfficeErrorCodes.ZERO_CHECK);
					return false;
				}
				if (!validation.isValidPercentage(interestRateInvestLeaseCommit)) {
					getErrorMap().setError("interestRateInvestLeaseCommit", BackOfficeErrorCodes.INVALID_PERCENTAGE);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("interestRateInvestLeaseCommit", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateInterestBillingChoice() {
		CommonValidator validation = new CommonValidator();
		try {
			if (interestApplicable) {
				if (validation.isEmpty(interestBillingChoice)) {
					getErrorMap().setError("interestBillingChoice", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!validation.isCMLOVREC("EPRELEASECONTREG", "INT_BILL_OPTION", interestBillingChoice)) {
					getErrorMap().setError("interestBillingChoice", BackOfficeErrorCodes.INVALID_OPTION);
					return false;
				}
			} else {
				if (!validation.isEmpty(interestBillingChoice)) {
					getErrorMap().setError("interestBillingChoice", BackOfficeErrorCodes.APPLICABLE_ONLY_CHECKED);
					return false;
				}
			}

			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("interestBillingChoice", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateInterestDebitNotePrinting() {
		CommonValidator validation = new CommonValidator();
		try {
			if (interestApplicable) {
				if (validation.isEmpty(interestDebitNotePrinting)) {
					getErrorMap().setError("interestDebitNotePrinting", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!validation.isCMLOVREC("EPRELEASECONTREG", "INT_DEBIT_PRINTING", interestDebitNotePrinting)) {
					getErrorMap().setError("interestDebitNotePrinting", BackOfficeErrorCodes.INVALID_OPTION);
					return false;
				}
			} else {
				if (!validation.isEmpty(interestDebitNotePrinting)) {
					getErrorMap().setError("interestDebitNotePrinting", BackOfficeErrorCodes.APPLICABLE_ONLY_CHECKED);
					return false;
				}
			}

			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("interestDebitNotePrinting", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateRemarks() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (remarks != null && remarks.length() > 0) {
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			if (getRectify().equals(RegularConstants.COLUMN_ENABLE)) {
				if (commonValidator.isEmpty(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}

}
