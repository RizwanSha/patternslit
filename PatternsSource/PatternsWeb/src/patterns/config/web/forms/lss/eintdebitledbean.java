package patterns.config.web.forms.lss;

import java.sql.ResultSet;
import java.util.ArrayList;

import patterns.config.delegate.BackOfficeProcessManager;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.MessageParam;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.process.TBAProcessResult;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.DHTMLXGridUtility;
import patterns.config.framework.web.GenericOption;
import patterns.config.framework.web.RequestConstants;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.LeaseValidator;
import patterns.config.validations.MasterValidator;
import patterns.config.validations.RegistrationValidator;
import patterns.config.web.forms.GenericFormBean;

public class eintdebitledbean extends GenericFormBean {

	private static final long serialVersionUID = -6219657363150466160L;

	private String intMonth;
	private String intMonthYear;
	private String leaseProdCode;
	private String lesseeCode;

	public String getIntMonth() {
		return intMonth;
	}

	public void setIntMonth(String intMonth) {
		this.intMonth = intMonth;
	}

	public String getIntMonthYear() {
		return intMonthYear;
	}

	public void setIntMonthYear(String intMonthYear) {
		this.intMonthYear = intMonthYear;
	}

	public String getLeaseProdCode() {
		return leaseProdCode;
	}

	public void setLeaseProdCode(String leaseProdCode) {
		this.leaseProdCode = leaseProdCode;
	}

	public String getLesseeCode() {
		return lesseeCode;
	}

	public void setLesseeCode(String lesseeCode) {
		this.lesseeCode = lesseeCode;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public void reset() {

		intMonth = RegularConstants.EMPTY_STRING;
		intMonthYear = RegularConstants.EMPTY_STRING;
		leaseProdCode = RegularConstants.EMPTY_STRING;
		lesseeCode = RegularConstants.EMPTY_STRING;

		DBContext dbContext = new DBContext();
		ArrayList<GenericOption> monthList = null;
		monthList = getGenericOptions("COMMON", "MONTHS", dbContext, null);
		webContext.getRequest().setAttribute("COMMON_MONTHS", monthList);

	}

	@Override
	public void validate() {

	}

	public DTObject validate(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();

		if (!validateIntMonth()) {
			resultDTO.set(ContentManager.ERROR, getErrorMap().getErrorKey("intMonth"));
			resultDTO.set(ContentManager.ERROR_FIELD, "intMonth");
			return resultDTO;
		}
		if (!validateIntMonthYear()) {
			resultDTO.set(ContentManager.ERROR, getErrorMap().getErrorKey("intMonthYear"));
			resultDTO.set(ContentManager.ERROR_FIELD, "intMonth");
			return resultDTO;
		}
		if (!validateLeaseProductCode()) {
			resultDTO.set(ContentManager.ERROR, getErrorMap().getErrorKey("leaseProdCode"));
			resultDTO.set(ContentManager.ERROR_FIELD, "leaseProdCode");
			return resultDTO;
		}
		if (!validateLesseeCode()) {
			resultDTO.set(ContentManager.ERROR, getErrorMap().getErrorKey("lesseeCode"));
			resultDTO.set(ContentManager.ERROR_FIELD, "lesseeCode");
			return resultDTO;
		}

		return resultDTO;
	}

	public DTObject processAction(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		try {

			setIntMonth(inputDTO.get("intMonth"));
			setIntMonthYear(inputDTO.get("intMonthYear"));
			setLeaseProdCode(inputDTO.get("leaseProdCode"));
			setLesseeCode(inputDTO.get("lesseeCode"));
			setProcessBO("patterns.config.framework.bo.lss.eintdebitledBO");
			DTObject validateDTO = validate(inputDTO);
			if (validateDTO.get(ContentManager.ERROR) != null) {
				return validateDTO;
			}

			DTObject formDTO = getFormDTO();
			formDTO.set("ENTITY_CODE", context.getEntityCode());
			formDTO.set("INT_MONTH", getIntMonth());
			formDTO.set("INT_YEAR", getIntMonthYear());
			formDTO.set("PRODUCT_CODE", !getLeaseProdCode().equals(RegularConstants.EMPTY_STRING) ? getLeaseProdCode() : RegularConstants.NULL);
			formDTO.set("LESSEE_CODE", !getLesseeCode().equals(RegularConstants.EMPTY_STRING) ? getLesseeCode() : RegularConstants.NULL);
			formDTO.set("ENTRY_MODE", "M");
			formDTO.set("USER_ID", context.getUserID());
			formDTO.set("PROCESS_ID", "EINTDEBITLED");
			formDTO.set(RegularConstants.PROCESSBO_CLASS, getProcessBO());

			// TBAProcessInfo processInfo = new TBAProcessInfo();
			// processInfo.setProcessData(formDTO);
			// processInfo.setProcessBO(getProcessBO());
			// processInfo.setProcessTFAInfo(getProcessTFAInfo());
			// processInfo.setProcessAction(getprocessAction(inputDTO));

			BackOfficeProcessManager processManager = new BackOfficeProcessManager();
			TBAProcessResult processResult = processManager.delegate(formDTO);
			if (processResult.getProcessStatus().equals(TBAProcessStatus.SUCCESS)) {
				resultDTO.set(ContentManager.STATUS, RegularConstants.SUCCESS);
				//resultDTO.set("TMPSERIAL", processResult.getGeneratedID());
				resultDTO.set(ContentManager.RESULT, getErrorResource(BackOfficeErrorCodes.PROCESS_SUCCESS, null));
			} else {
				resultDTO.set(ContentManager.STATUS, RegularConstants.FAILURE);
				resultDTO.set(ContentManager.RESULT, getErrorResource(BackOfficeErrorCodes.PROCESS_NOT_SUCCESS, null));
			}
			
			Object additionalInfo = processResult.getAdditionalInfo();
			if (additionalInfo != null && additionalInfo instanceof String) {
				resultDTO.set(RequestConstants.ADDITIONAL_INFO, processResult.getAdditionalInfo().toString());
			}else if (additionalInfo != null && additionalInfo instanceof MessageParam) {
				MessageParam messageParam = (MessageParam) additionalInfo;
				resultDTO.set(RequestConstants.ADDITIONAL_INFO, getErrorResource(messageParam.getCode(), messageParam.toParameterArray()));
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return resultDTO;
	}

	private boolean validateIntMonth() {
		CommonValidator validator = new CommonValidator();
		try {
			if (validator.isEmpty(intMonth)) {
				getErrorMap().setError("intMonth", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validator.isCMLOVREC("COMMON", "MONTHS", intMonth)) {
				getErrorMap().setError("intMonth", BackOfficeErrorCodes.INVALID_OPTION);
				return false;
			}

			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("intMonth", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return false;
	}

	private boolean validateIntMonthYear() {
		CommonValidator validator = new CommonValidator();
		try {
			if (validator.isEmpty(intMonthYear)) {
				getErrorMap().setError("intMonthYear", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validator.isValidYear(intMonthYear)) {
				getErrorMap().setError("intMonthYear", BackOfficeErrorCodes.INVALID_YEAR);
				return false;
			}
			if (!validator.isNumberGreaterThanEqual(context.getCurrentYear(),intMonthYear)) {
				getErrorMap().setError("intMonthYear", BackOfficeErrorCodes.YEAR_LE_CALYEAR);
				return false;
			}

			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("intMonthYear", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return false;
	}

	private boolean validateLeaseProductCode() {
		CommonValidator validator = new CommonValidator();
		try {
			if (validator.isEmpty(leaseProdCode)) {
				return true;
			}
			DTObject formDTO = new DTObject();
			formDTO.set("LEASE_PRODUCT_CODE", leaseProdCode);
			formDTO.set(ContentManager.ACTION, USAGE);
			formDTO = validateLeaseProductCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("leaseProdCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("leaseProdCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return false;
	}

	public DTObject validateLeaseProductCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		MasterValidator validation = new MasterValidator();
		try {
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
			resultDTO = validation.validateLeaseProductCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	private boolean validateLesseeCode() {
		CommonValidator validator = new CommonValidator();
		try {
			if (validator.isEmpty(lesseeCode)) {
				return true;
			}
			DTObject formDTO = new DTObject();
			formDTO.set("SUNDRY_DB_AC", lesseeCode);
			formDTO.set(ContentManager.ACTION, USAGE);
			formDTO = validateLesseeCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("lesseeCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("lesseeCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return false;
	}

	public DTObject validateLesseeCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		DTObject custinputDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		LeaseValidator validation = new LeaseValidator();
		RegistrationValidator regValidator = new RegistrationValidator();
		try {
			inputDTO.set(ContentManager.FETCH, ContentManager.FETCH_ALL);
			resultDTO = validation.valildateLesseCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}

			custinputDTO.set(ContentManager.ACTION, USAGE);
			custinputDTO.set("CUSTOMER_ID", resultDTO.get("CUSTOMER_ID"));
			custinputDTO.set(ContentManager.FETCH_COLUMNS, "CUSTOMER_NAME");
			resultDTO = regValidator.valildateCustomerID(custinputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}

		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
			regValidator.close();
		}
		return resultDTO;
	}

	public DTObject loadLedgerDetails(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		DBContext dbContext = new DBContext();
		DHTMLXGridUtility gridUtility = new DHTMLXGridUtility();
		StringBuffer sqlQuery = new StringBuffer(RegularConstants.EMPTY_STRING);
		boolean recordExist = false;
		try {
			DBUtil util = dbContext.createUtilInstance();
			util.setMode(DBUtil.PREPARED);
			sqlQuery.append("SELECT TO_CHAR(LED.CONTRACT_DATE,'" + context.getDateFormat() + "')CONTRACT_DATE,LED.CONTRACT_SL,LED.FOR_YEAR,CM.LOV_LABEL_EN_US FOR_MONTH,LED.PAYMENT_SL,");
			sqlQuery.append("C.CUSTOMER_NAME,SU.NAME,TO_CHAR(LED.FROM_DATE,'" + context.getDateFormat() + "')FROM_DATE,TO_CHAR(LED.UPTO_DATE,'" + context.getDateFormat() + "')UPTO_DATE,");
			sqlQuery.append(" LED.NOF_DAYS,LED.INTEREST_RATE,");
			sqlQuery.append("TO_CHAR(LPOP.PAYMENT_DATE,'" + context.getDateFormat() + "')PAYMENT_DATE,LPOP.PAYMENT_CCY,FN_FORMAT_AMOUNT(LPOP.PAYMENT_AMOUNT,LPOP.PAYMENT_CCY,C1.SUB_CCY_UNITS_IN_CCY) PAY_AMOUNT,");
			sqlQuery.append("LED.INTEREST_CCY,FN_FORMAT_AMOUNT(LED.INTEREST_AMOUNT,LED.INTEREST_CCY,C2.SUB_CCY_UNITS_IN_CCY) INT_AMOUNT ");
			sqlQuery.append(" FROM TMPPLCINTERESTLED TP ");

			sqlQuery.append(" JOIN PLCINTERESTLED LED ON (LED.ENTITY_CODE =? AND LED.CONTRACT_DATE = TP.CONTRACT_DATE AND LED.CONTRACT_SL = TP.CONTRACT_SL  ");
			sqlQuery.append(" AND LED.FOR_YEAR = TP.FOR_YEAR AND LED.FOR_MONTH = TP.FOR_MONTH AND LED.PAYMENT_SL = TP.PAYMENT_SL) ");

			sqlQuery.append(" JOIN PRELEASECONTRACT PLC ON (PLC.ENTITY_CODE = LED.ENTITY_CODE AND PLC.CONTRACT_DATE = LED.CONTRACT_DATE ");
			sqlQuery.append(" AND PLC.CONTRACT_SL = LED.CONTRACT_SL) ");

			sqlQuery.append(" JOIN LEASEPOINVPAY LPOP ON (LPOP.ENTITY_CODE = LED.ENTITY_CODE AND LPOP.CONTRACT_DATE = LED.CONTRACT_DATE ");
			sqlQuery.append(" AND LPOP.CONTRACT_SL = LED.CONTRACT_SL AND LPOP.PAYMENT_SL = LED.PAYMENT_SL) ");

			sqlQuery.append(" JOIN LEASEPO LPO ON (LPO.ENTITY_CODE = LPOP.ENTITY_CODE AND LPO.CONTRACT_DATE = LPOP.CONTRACT_DATE AND ");
			sqlQuery.append(" LPO.CONTRACT_SL = LPOP.CONTRACT_SL AND LPO.PO_SL = LPOP.PO_SL) ");

			sqlQuery.append(" JOIN CUSTOMER C ON (C.ENTITY_CODE = PLC.ENTITY_CODE AND C.CUSTOMER_ID = PLC.CUSTOMER_ID) ");
			sqlQuery.append(" JOIN SUPPLIER SU ON (SU.ENTITY_CODE = LPO.ENTITY_CODE AND SU.SUPPLIER_ID = LPO.SUPPLIER_ID) ");
			sqlQuery.append(" JOIN CMLOVREC CM ON (CM.PGM_ID = 'COMMON' AND CM.LOV_ID = 'MONTH_SHORT' AND CM.LOV_VALUE = LED.FOR_MONTH) ");
			sqlQuery.append(" JOIN CURRENCY C1 ON (C1.CCY_CODE = LPOP.PAYMENT_CCY) ");
			sqlQuery.append(" JOIN CURRENCY C2 ON (C2.CCY_CODE = LED.INTEREST_CCY) ");

			sqlQuery.append(" WHERE TP.TMP_SERIAL=?");

			util.setSql(sqlQuery.toString());
			util.setLong(1, Long.parseLong(context.getEntityCode()));
			util.setLong(2, Long.parseLong(inputDTO.get("TMP_SERIAL")));
			ResultSet rset = util.executeQuery();
			gridUtility.init();
			while (rset.next()) {
				gridUtility.startRow();
				gridUtility.setCell(rset.getString("CONTRACT_DATE"));
				gridUtility.setCell(rset.getString("CONTRACT_SL"));
				gridUtility.setCell(rset.getString("FOR_YEAR"));
				gridUtility.setCell(rset.getString("FOR_MONTH"));
				gridUtility.setCell(rset.getString("PAYMENT_SL"));
				gridUtility.setCell(rset.getString("CUSTOMER_NAME"));
				gridUtility.setCell(rset.getString("NAME"));
				gridUtility.setCell(rset.getString("PAYMENT_DATE"));
				gridUtility.setCell(rset.getString("FROM_DATE"));
				gridUtility.setCell(rset.getString("UPTO_DATE"));
				gridUtility.setCell(rset.getString("NOF_DAYS"));
				gridUtility.setCell(rset.getString("INTEREST_RATE"));
				gridUtility.setCell(rset.getString("PAYMENT_CCY"));
				gridUtility.setCell(rset.getString("PAY_AMOUNT"));
				gridUtility.setCell(rset.getString("INTEREST_CCY"));
				gridUtility.setCell(rset.getString("INT_AMOUNT"));
				gridUtility.endRow();
				recordExist = true;
			}
			if (!recordExist) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_NOT_EXISTS);
				return resultDTO;
			}
			gridUtility.finish();
			String resultXML = gridUtility.getXML();
			inputDTO.set(ContentManager.RESULT_XML, resultXML);
			inputDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);

		} catch (Exception e) {
			e.printStackTrace();
			inputDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			dbContext.close();
		}
		return inputDTO;
	}

}