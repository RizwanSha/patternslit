package patterns.config.web.forms.lss;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.ResultSet;
import java.util.ArrayList;

import patterns.config.constants.ProgramConstants;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.service.DTDObject;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.framework.web.DHTMLXGridUtility;
import patterns.config.framework.web.GenericOption;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.AccessValidator;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.LeaseValidator;
import patterns.config.validations.MasterValidator;
import patterns.config.validations.RegistrationValidator;
import patterns.config.web.forms.GenericFormBean;

public class eleasepoinvpaybean extends GenericFormBean {

	/**
	 * v
	 */
	private static final long serialVersionUID = 1L;
	private String leaseCode;
	private String custId;
	private String custName;
	private String agreeNo;
	private String scheduleId;
	private String selectedPoSl;
	private String selectedInvSl;
	private String paymentSl;
	private String date;
	private String amtPaid;
	private String amtPaidFormat;
	private String amtPaid_curr;
	private String eqLocal;
	private String eqLocalFormat;
	private String eqLocal_curr;
	private String payRef;
	private String remarks;
	private String selectedPoSlDisplay;
	private String poDate;
	private String amtPaidHidden;
	private String eqLocalHidden;
	private String currHidden;
	private String payFor;
	private String payFavour;
	private String preLeaseRef;
	private String preLeaseRefDate;
	private String preLeaseRefSerial;


	public String getPreLeaseRef() {
		return preLeaseRef;
	}

	public void setPreLeaseRef(String preLeaseRef) {
		this.preLeaseRef = preLeaseRef;
	}

	public String getPreLeaseRefDate() {
		return preLeaseRefDate;
	}

	public void setPreLeaseRefDate(String preLeaseRefDate) {
		this.preLeaseRefDate = preLeaseRefDate;
	}

	public String getPreLeaseRefSerial() {
		return preLeaseRefSerial;
	}

	public void setPreLeaseRefSerial(String preLeaseRefSerial) {
		this.preLeaseRefSerial = preLeaseRefSerial;
	}

	public String getPayFavour() {
		return payFavour;
	}

	public void setPayFavour(String payFavour) {
		this.payFavour = payFavour;
	}

	public String getPayFor() {
		return payFor;
	}

	public void setPayFor(String payFor) {
		this.payFor = payFor;
	}


	public String getCurrHidden() {
		return currHidden;
	}

	public void setCurrHidden(String currHidden) {
		this.currHidden = currHidden;
	}

	public String getAmtPaidHidden() {
		return amtPaidHidden;
	}

	public void setAmtPaidHidden(String amtPaidHidden) {
		this.amtPaidHidden = amtPaidHidden;
	}

	public String getEqLocalHidden() {
		return eqLocalHidden;
	}

	public void setEqLocalHidden(String eqLocalHidden) {
		this.eqLocalHidden = eqLocalHidden;
	}

	public String getPoDate() {
		return poDate;
	}

	public void setPoDate(String poDate) {
		this.poDate = poDate;
	}

	public String getSelectedPoSlDisplay() {
		return selectedPoSlDisplay;
	}

	public void setSelectedPoSlDisplay(String selectedPoSlDisplay) {
		this.selectedPoSlDisplay = selectedPoSlDisplay;
	}

	public String getSelectedInvSlDisplay() {
		return selectedInvSlDisplay;
	}

	public void setSelectedInvSlDisplay(String selectedInvSlDisplay) {
		this.selectedInvSlDisplay = selectedInvSlDisplay;
	}

	private String selectedInvSlDisplay;

	public String getLeaseCode() {
		return leaseCode;
	}

	public void setLeaseCode(String leaseCode) {
		this.leaseCode = leaseCode;
	}

	public String getCustId() {
		return custId;
	}

	public void setCustId(String custId) {
		this.custId = custId;
	}

	public String getCustName() {
		return custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	public String getAgreeNo() {
		return agreeNo;
	}

	public void setAgreeNo(String agreeNo) {
		this.agreeNo = agreeNo;
	}

	public String getScheduleId() {
		return scheduleId;
	}

	public void setScheduleId(String scheduleId) {
		this.scheduleId = scheduleId;
	}

	public String getSelectedPoSl() {
		return selectedPoSl;
	}

	public void setSelectedPoSl(String selectedPoSl) {
		this.selectedPoSl = selectedPoSl;
	}

	public String getSelectedInvSl() {
		return selectedInvSl;
	}

	public void setSelectedInvSl(String selectedInvSl) {
		this.selectedInvSl = selectedInvSl;
	}

	public String getPaymentSl() {
		return paymentSl;
	}

	public void setPaymentSl(String paymentSl) {
		this.paymentSl = paymentSl;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getAmtPaid() {
		return amtPaid;
	}

	public void setAmtPaid(String amtPaid) {
		this.amtPaid = amtPaid;
	}

	public String getAmtPaidFormat() {
		return amtPaidFormat;
	}

	public void setAmtPaidFormat(String amtPaidFormat) {
		this.amtPaidFormat = amtPaidFormat;
	}

	public String getAmtPaid_curr() {
		return amtPaid_curr;
	}

	public void setAmtPaid_curr(String amtPaid_curr) {
		this.amtPaid_curr = amtPaid_curr;
	}

	public String getEqLocal() {
		return eqLocal;
	}

	public void setEqLocal(String eqLocal) {
		this.eqLocal = eqLocal;
	}

	public String getEqLocalFormat() {
		return eqLocalFormat;
	}

	public void setEqLocalFormat(String eqLocalFormat) {
		this.eqLocalFormat = eqLocalFormat;
	}

	public String getEqLocal_curr() {
		return eqLocal_curr;
	}

	public void setEqLocal_curr(String eqLocal_curr) {
		this.eqLocal_curr = eqLocal_curr;
	}

	public String getPayRef() {
		return payRef;
	}

	public void setPayRef(String payRef) {
		this.payRef = payRef;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getXmlGrid() {
		return xmlGrid;
	}

	public void setXmlGrid(String xmlGrid) {
		this.xmlGrid = xmlGrid;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	private String xmlGrid;

	@Override
	public void reset() {
		// TODO Auto-generated method stub
		preLeaseRef = RegularConstants.EMPTY_STRING;
		preLeaseRefDate = RegularConstants.EMPTY_STRING;
		preLeaseRefSerial = RegularConstants.EMPTY_STRING;
		leaseCode = RegularConstants.EMPTY_STRING;
		custId = RegularConstants.EMPTY_STRING;
		custName = RegularConstants.EMPTY_STRING;
		agreeNo = RegularConstants.EMPTY_STRING;
		scheduleId = RegularConstants.EMPTY_STRING;
		selectedPoSl = RegularConstants.EMPTY_STRING;
		selectedInvSl = RegularConstants.EMPTY_STRING;
		paymentSl = RegularConstants.EMPTY_STRING;
		date = RegularConstants.EMPTY_STRING;
		amtPaid = RegularConstants.EMPTY_STRING;
		amtPaidFormat = RegularConstants.EMPTY_STRING;
		amtPaid_curr = RegularConstants.EMPTY_STRING;
		eqLocal = RegularConstants.EMPTY_STRING;
		eqLocalFormat = RegularConstants.EMPTY_STRING;
		eqLocal_curr = RegularConstants.EMPTY_STRING;
		remarks = RegularConstants.EMPTY_STRING;
		payFor = RegularConstants.EMPTY_STRING;
		payFavour = RegularConstants.EMPTY_STRING;
		DBContext dbContext = new DBContext();
		ArrayList<GenericOption> payForList = null;
		payForList = getGenericOptions("COMMON", "PAY_FOR", dbContext, null);
		webContext.getRequest().setAttribute("COMMON_PAY_FOR", payForList);
		dbContext.close();
		setProcessBO("patterns.config.framework.bo.lss.eleasepoinvpayBO");
	}

	@Override
	public void validate() {
		// TODO Auto-generated method stub
		if (validatePreLeaseContractDate() && validatePreLeaseContractRefSerial()) {
			validatePaymentSl();
			validateGrid();
			validatePayFor();
			validateInvSerial();
			validateDate();
			validateEqLocalFormat();
			validatePayRef();
			validateRemarks();
			
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.set("CONTRACT_DATE", preLeaseRefDate); 
				formDTO.set("CONTRACT_SL", preLeaseRefSerial); 
				if (paymentSl != null && !paymentSl.equals(RegularConstants.EMPTY_STRING))
					formDTO.set("PAYMENT_SL", paymentSl);
				else
					formDTO.set("PAYMENT_SL", RegularConstants.EMPTY_STRING);
				formDTO.set("PO_SL", selectedPoSlDisplay);
				formDTO.set("PAY_FOR", payFor);
				if (selectedInvSl == null || selectedInvSl.trim().length() == 0) {
					formDTO.set("INVOICE_SL", RegularConstants.NULL);
				} else {
					formDTO.set("INVOICE_SL", selectedInvSl);
				}
				formDTO.set("PAYMENT_DATE", date);
				formDTO.set("PAYMENT_CCY", currHidden);
				formDTO.set("PAYMENT_AMOUNT", amtPaidHidden);
				formDTO.set("PAYMENT_AMOUNT_BC", eqLocalHidden);
				formDTO.set("PAYMENT_REFERENCE", payRef);
				formDTO.set("REMARKS", remarks);
				DTDObject dtdObject = new DTDObject();
				dtdObject.addColumn(0, "SELECT");
				dtdObject.addColumn(1, "PO_ENTRYSL");
				dtdObject.addColumn(2, "PO_DATE");
				dtdObject.addColumn(3, "PO_NUM");
				dtdObject.addColumn(4, "PO_CURR");
				dtdObject.addColumn(5, "PO_AMTFORMAT");
				dtdObject.addColumn(6, "PO_AMTUNFORMAT");
				dtdObject.addColumn(7, "PAIDSOFAR");
				dtdObject.addColumn(8, "PO_AMOUNT_PAID_SO_FAR");
				dtdObject.addColumn(9, "SUPPLIER_ID");
				dtdObject.addColumn(10, "NAME");
				dtdObject.setXML(xmlGrid);
				formDTO.setDTDObject("ELEASEPOINVPAYDTL", dtdObject);
				setAction("A");
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("leaseCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
	}

	public boolean validatePreLeaseContractRefSerial() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("CONTRACT_DATE", preLeaseRefDate);
			formDTO.set("CONTRACT_SL", preLeaseRefSerial);
			formDTO.set("PO_SL", selectedPoSlDisplay);
			formDTO.set("INV_SL", selectedInvSlDisplay);
			formDTO.set(ContentManager.ACTION, getAction());
			formDTO = validatePreLeaseContractRefSerial(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("preLeaseRef", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("preLeaseRef", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public boolean validatePreLeaseContractDate() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (commonValidator.isEmpty(preLeaseRefDate)) {
				getErrorMap().setError("preLeaseRefDate", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			java.util.Date dateInstance = commonValidator.isValidDate(preLeaseRefDate);
			if (dateInstance == null) {
				getErrorMap().setError("preLeaseRefDate", BackOfficeErrorCodes.INVALID_DATE);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("preLeaseRefDate", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}

	public DTObject validatePreLeaseContractRefSerial(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		DTObject poDTO = new DTObject();
		DTObject custDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		LeaseValidator validation = new LeaseValidator();
		RegistrationValidator validator = new RegistrationValidator();
		String contdate = inputDTO.get("CONTRACT_DATE");
		String contractSl = inputDTO.get("CONTRACT_SL");
		String action = inputDTO.get("ACTION");
		try {
			if (validation.isEmpty(contdate) || validation.isEmpty(contractSl)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			if (!validation.isValidNumber(contractSl)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_NUMBER);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "CUSTOMER_ID,SUNDRY_DB_AC,SCHEDULE_ID,AGREEMENT_NO");
			resultDTO = validation.validatePreLeaseContractReferenceSerial(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_NOT_EXISTS);
				return resultDTO;
			}
			inputDTO.set("CUSTOMER_ID", resultDTO.get("CUSTOMER_ID"));
			inputDTO.set(ContentManager.FETCH_COLUMNS, "CUSTOMER_NAME");
			custDTO = validator.valildateCustomerID(inputDTO);
			if (custDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, custDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
			resultDTO.set("CUSTOMER_NAME", custDTO.get("CUSTOMER_NAME"));
			inputDTO.set(ContentManager.USER_ACTION, action);
			poDTO = loadDetails(inputDTO);
			resultDTO.set(ContentManager.RESULT_XML, poDTO.get(ContentManager.RESULT_XML));
			return resultDTO;
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
			validator.close();
		}
		return resultDTO;
	}

	public DTObject loadDetails(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBContext dbContext = new DBContext();
		Date contractDate = null;
		contractDate = new java.sql.Date(BackOfficeFormatUtils.getDate(inputDTO.get("CONTRACT_DATE"), BackOfficeConstants.JAVA_DATE_FORMAT).getTime());
		String contractSerial = inputDTO.get("CONTRACT_SL");
		String poSL = inputDTO.get("PO_SL");
		StringBuffer sqlQuery = new StringBuffer();
		DBUtil util = dbContext.createUtilInstance();
		boolean recordExist = false;
		String action = inputDTO.get("ACTION");
		try {
			util.reset();
			sqlQuery.append("SELECT M.PO_SL,TO_CHAR(M.PO_DATE,?)PO_DATE,M.PO_NUMBER,M.PO_CCY,M.TOTAL_PO_AMOUNT,FN_FORMAT_AMOUNT(M.TOTAL_PO_AMOUNT,M.PO_CCY,C.SUB_CCY_UNITS_IN_CCY)TOTPOAMTFORMAT,FN_FORMAT_AMOUNT(M.PO_AMOUNT_PAID_SO_FAR,M.PO_CCY,C.SUB_CCY_UNITS_IN_CCY )PAIDSOFAR,M.PO_AMOUNT_PAID_SO_FAR,M.SUPPLIER_ID,S.NAME FROM LEASEPO M ,CURRENCY C,SUPPLIER S WHERE M.E_STATUS='A' AND M.TOTAL_PO_AMOUNT<>M.PO_AMOUNT_PAID_SO_FAR AND M.ENTITY_CODE=? AND M.CONTRACT_DATE=? AND M.CONTRACT_SL=? AND C.CCY_CODE=M.PO_CCY AND M.PO_CCY=C.CCY_CODE AND M.PO_SL=IFNULL(?,M.PO_SL)  AND M.TOTAL_PO_AMOUNT<>M.PO_AMOUNT_PAID_SO_FAR  AND S.ENTITY_CODE=M.ENTITY_CODE AND S.SUPPLIER_ID=M.SUPPLIER_ID");
			util.setSql(sqlQuery.toString());
			util.setString(1, context.getDateFormat());
			util.setInt(2, Integer.parseInt(context.getEntityCode()));
			util.setDate(3, contractDate);
			util.setString(4, contractSerial);
			if (action.equals("RECTIFY")) {
				util.setString(5, poSL);
			} else {
				util.setString(5, RegularConstants.NULL);
			}
			ResultSet rset = util.executeQuery();
			DHTMLXGridUtility gridUtility = new DHTMLXGridUtility();
			gridUtility.init();
			int index = 0;
			while (rset.next()) {
				gridUtility.startRow(String.valueOf(index));
				gridUtility.setCell("");
				gridUtility.setCell(rset.getString("PO_SL") + "^javascript:viewpoSerial(\"" + rset.getString("PO_SL") + "\")");
				gridUtility.setCell(rset.getString("PO_DATE"));
				gridUtility.setCell(rset.getString("PO_NUMBER"));
				gridUtility.setCell(rset.getString("PO_CCY"));
				gridUtility.setCell(rset.getString("TOTPOAMTFORMAT"));
				gridUtility.setCell(rset.getString("TOTAL_PO_AMOUNT"));
				gridUtility.setCell(rset.getString("PAIDSOFAR"));
				gridUtility.setCell(rset.getString("PO_AMOUNT_PAID_SO_FAR"));
				gridUtility.setCell(rset.getString("SUPPLIER_ID"));
				gridUtility.setCell(rset.getString("NAME"));
				gridUtility.endRow();
				index++;
				recordExist = true;
			}
			if (!recordExist) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_NOT_EXISTS);
				return resultDTO;
			}
			gridUtility.finish();
			String resultXML = gridUtility.getXML();
			resultDTO.set(ContentManager.RESULT_XML, resultXML);
			resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			dbContext.close();
			util.reset();
		}
		return resultDTO;
	}

	private boolean validateGrid() {
		CommonValidator validation = new CommonValidator();
		try {
			DTDObject dtdObject = new DTDObject();
			dtdObject.addColumn(0, "SELECT");
			dtdObject.addColumn(1, "PO_ENTRYSL");
			dtdObject.addColumn(2, "PO_DATE");
			dtdObject.addColumn(3, "PO_NUM");
			dtdObject.addColumn(4, "PO_CURR");
			dtdObject.addColumn(5, "PO_AMTFORMAT");
			dtdObject.addColumn(6, "PO_AMTUNFORMAT");
			dtdObject.addColumn(7, "PAIDSOFAR");
			dtdObject.addColumn(8, "PO_AMOUNT_PAID_SO_FAR");
			dtdObject.addColumn(9, "SUPPLIER_ID");
			dtdObject.addColumn(10, "NAME");
			dtdObject.setXML(xmlGrid);
			if (dtdObject.getRowCount() <= 0) {
				getErrorMap().setError("agreeNo", BackOfficeErrorCodes.ATLEAST_ONE_ROW);
				return false;
			}
			if (!validation.checkDuplicateRows(dtdObject, 1, 7)) {
				getErrorMap().setError("agreeNo", BackOfficeErrorCodes.DUPLICATE_DATA);
				return false;
			}

			for (int i = 0; i < dtdObject.getRowCount(); i++) {
				if (dtdObject.getValue(i, 0).equals(RegularConstants.ONE)) {
					poDate = dtdObject.getValue(i, 2);
				}
				if (!validateAmtPaidFormat(dtdObject.getValue(i, 6), dtdObject.getValue(i, 8))) {
					getErrorMap().setError("amtPaidFormat", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("agreeNo", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return true;
	}

	public boolean validatePayFor() {
		AccessValidator validation = new AccessValidator();
		try {
			if (validation.isEmpty(payFor)) {
				getErrorMap().setError("payFor", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isCMLOVREC("COMMON", "PAY_FOR", payFor)) {
				getErrorMap().setError("payFor", BackOfficeErrorCodes.INVALID_OPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("payFor", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateInvSerial() {
		CommonValidator validation = new CommonValidator();
		try {
			if (payFor.equals("I")) {
				DTObject formDTO = getFormDTO();
				formDTO.set("CONTRACT_DATE", preLeaseRefDate);
				formDTO.set("CONTRACT_SL", preLeaseRefSerial);
				formDTO.set("PO_SL", selectedPoSlDisplay);
				formDTO.set("INVOICE_SL", selectedInvSl);
				formDTO.set(ContentManager.ACTION, getAction());
				formDTO = validateInvSerial(formDTO);
				if (formDTO.get(ContentManager.ERROR) != null) {
					getErrorMap().setError("selectedInvSl", formDTO.get(ContentManager.ERROR));
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("selectedInvSl", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public DTObject validateInvSerial(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		LeaseValidator validation = new LeaseValidator();
		DBUtil util = validation.getDbContext().createUtilInstance();
		try {
			inputDTO.set(ContentManager.FETCH_COLUMNS, "INVOICE_SL,AUTH_ON,INV_FOR,INVOICE_AMOUNT_PAID_SOFAR,INVOICE_AMOUNT");
			resultDTO = validation.validateLeasePoInv(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != RegularConstants.NULL) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
			if (resultDTO.getObject("AUTH_ON") == RegularConstants.NULL) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.LEASE_UNAUTH);
				return resultDTO;
			}
			if (!resultDTO.get("INV_FOR").equals("N")) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.PAY_DONE);
				return resultDTO;
			}
			if (resultDTO.get("INVOICE_AMOUNT_PAID_SOFAR") == resultDTO.get("INVOICE_AMOUNT")) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.PAID_ALREADY_INV);
				return resultDTO;
			}

		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
			util.reset();
		}
		return resultDTO;
	}

	public boolean validateDate() {
		CommonValidator commonvalidation = new CommonValidator();
		try {
			if (commonvalidation.isEmpty(date)) {
				getErrorMap().setError("date", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			java.util.Date dateInstance = commonvalidation.isValidDate(date);
			if (dateInstance == null) {
				getErrorMap().setError("date", BackOfficeErrorCodes.INVALID_DATE);
				return false;
			}
			if (commonvalidation.isDateGreaterThanCBD(date)) {
				getErrorMap().setError("date", BackOfficeErrorCodes.DATE_LCBD);
				return false;
			}
			if (commonvalidation.isDateLesser(date, poDate)) {
				getErrorMap().setError("date", BackOfficeErrorCodes.DATE_LESS_PO_DATE);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("date", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonvalidation.close();
		}
		return false;
	}
	
	
	private boolean validateAmtPaidFormat(String totalamt, String paidamt) {
		CommonValidator validation = new CommonValidator();
		DTObject formDTO = getFormDTO();
		try {
			if (validation.isEmpty(amtPaidHidden)) {
				getErrorMap().setError("amtPaidFormat", BackOfficeErrorCodes.HMS_MANDATORY);
				return false;
			}

			if (validation.isValidNegativeAmount(amtPaidHidden)) {
				getErrorMap().setError("amtPaid", BackOfficeErrorCodes.HMS_POSITIVE_AMOUNT);
				return false;
			}
			if (validation.isZeroAmount(amtPaidHidden)) {
				getErrorMap().setError("amtPaid", BackOfficeErrorCodes.HMS_ZERO_AMOUNT);
				return false;
			}
			if (!validation.isValidAmount(amtPaidHidden)) {
				getErrorMap().setError("amtPaid", BackOfficeErrorCodes.INVALID_AMOUNT);
				return false;
			}
			BigDecimal value1;
			BigDecimal value2;
			value1 = new BigDecimal(amtPaidHidden);
			value2 = new BigDecimal(paidamt);
			BigDecimal sum = value2.add(value1);
			String amount = String.valueOf(sum);
			if (validation.isAmountGreater(amount, totalamt)) {
				getErrorMap().setError("amtPaid", BackOfficeErrorCodes.TOTAL_AMT_EXCEEDS);
				return false;
			}

			formDTO.set("CURRENCY_CODE", currHidden);
			formDTO.set(ProgramConstants.AMOUNT, amtPaidHidden);
			formDTO.set(ProgramConstants.CURRENCY_UNITS, context.getBaseCurrencyUnits());
			formDTO.set(ContentManager.USER_ACTION, getAction());
			formDTO = validation.validateCurrencySmallAmount(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("amtPaid", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("amtPaidFormat", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	private boolean validateEqLocalFormat() {
		CommonValidator validation = new CommonValidator();
		DTObject formDTO = getFormDTO();
		try {
			if (validation.isEmpty(eqLocalHidden)) {
				getErrorMap().setError("eqLocal", BackOfficeErrorCodes.HMS_MANDATORY);
				return false;
			}
			if (validation.isValidNegativeAmount(eqLocalHidden)) {
				getErrorMap().setError("eqLocal", BackOfficeErrorCodes.HMS_POSITIVE_AMOUNT);
				return false;
			}
			if (validation.isZeroAmount(eqLocalHidden)) {
				getErrorMap().setError("eqLocal", BackOfficeErrorCodes.HMS_ZERO_AMOUNT);
				return false;
			}
			if (!validation.isValidAmount(eqLocalHidden)) {
				getErrorMap().setError("eqLocal", BackOfficeErrorCodes.INVALID_AMOUNT);
				return false;
			}
			formDTO.set("CURRENCY_CODE", currHidden);
			formDTO.set(ProgramConstants.AMOUNT, eqLocalHidden);
			formDTO.set(ProgramConstants.CURRENCY_UNITS, context.getBaseCurrencyUnits());
			formDTO.set(ContentManager.USER_ACTION, getAction());
			formDTO = validation.validateCurrencySmallAmount(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("eqLocal", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("eqLocal", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	} 

	public boolean validatePaymentSl() {
		DTObject formDTO = new DTObject();
		CommonValidator validation = new CommonValidator();
		try {
			if (getAction().equals("RECTIFY")) {
				if (validation.isEmpty(paymentSl)) {
					getErrorMap().setError("paymentSl", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!validation.isValidNumber(paymentSl)) {
					getErrorMap().setError("paymentSl", BackOfficeErrorCodes.INVALID_NUMBER);
					return false;
				}
				formDTO.set("ENTITY_CODE", context.getBranchCode());
				formDTO.set("CONTRACT_DATE", preLeaseRefDate);
				formDTO.set("CONTRACT_SL", preLeaseRefSerial);
				formDTO.set("PO_SL", selectedPoSlDisplay);
				formDTO.set("INV_SL", selectedInvSlDisplay);
				formDTO.set("PAYMENT_SL", paymentSl);
				formDTO.set(ContentManager.ACTION, getAction());
				formDTO = validatePaymentSl(formDTO);
				if (formDTO.get(ContentManager.ERROR) != null) {
					getErrorMap().setError("paymentSl", formDTO.get(ContentManager.ERROR));
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("paymentSl", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public DTObject validatePaymentSl(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		MasterValidator validator = new MasterValidator();
		try {
			inputDTO.set(ContentManager.TABLE, "LEASEPOINVPAY");
			inputDTO.set(ContentManager.FETCH_COLUMNS, "CONTRACT_DATE");
			String columns[] = new String[] { context.getEntityCode(), inputDTO.get("CONTRACT_DATE"), inputDTO.get("CONTRACT_SL"), inputDTO.get("PAYMENT_SL") };
			inputDTO.setObject(ContentManager.PROGRAM_KEY, columns);
			resultDTO = validator.validatePK(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return resultDTO;
	}

	public boolean validatePayRef() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(payRef)) {
				getErrorMap().setError("payRef", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			DTObject formDTO = getFormDTO();
			formDTO.set("ENTITY_CODE", context.getEntityCode());
			formDTO.set("CONTRACT_DATE", preLeaseRefDate);
			formDTO.set("CONTRACT_SL", preLeaseRefSerial);
			formDTO.set("PAYMENT_SL", paymentSl);
			formDTO.set("PAYMENT_REF", payRef);
			formDTO = validatePayRef(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("payRef", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("payRef", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public DTObject validatePayRef(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		MasterValidator validation = new MasterValidator();
		try {
			String primaryKey = inputDTO.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + inputDTO.get("CONTRACT_DATE") + RegularConstants.PK_SEPARATOR 
					+ inputDTO.get("CONTRACT_SL") + RegularConstants.PK_SEPARATOR + inputDTO.get("PAYMENT_SL") + RegularConstants.PK_SEPARATOR + inputDTO.get("PAYMENT_REF");
			String dedupKey = context.getEntityCode() + RegularConstants.PK_SEPARATOR + inputDTO.get("PAYMENT_REF") + RegularConstants.PK_SEPARATOR + RegularConstants.ONE;
			DTObject formDTO = new DTObject();
			formDTO.set(ContentManager.PROGRAM_ID, "ELEASEPOINVPAY");
			formDTO.set(ContentManager.PURPOSE_ID, "PAYMENT_REF");
			formDTO.set(ContentManager.PROGRAM_KEY, primaryKey);
			formDTO.set(ContentManager.DEDUP_KEY, dedupKey);
			resultDTO = validation.isDeDupAlreadyExist(formDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
				Object[] errorParam = { resultDTO.getObject("LINKED_TO_PK") };
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.PAY_REF_EXISTS); // NEW
				resultDTO.setObject(ContentManager.ERROR_PARAM, errorParam);
				return resultDTO;
			}

		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateRemarks() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (remarks != null && remarks.length() > 0) {
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			if (getAction().equals("RECTIFY")) {
				if (commonValidator.isEmpty(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}
}
