package patterns.config.web.forms.lss;

/*
 *
 Author : NARESHKUMAR D
 Created Date : 08-MAR-2017
 Spec Reference : TATACAP/LSS/0004-ELEASE  - Lease Details Maintenance
 Modification History
 ----------------------------------------------------------------------
 Sl.No		Modified Date	Author			Modified Changes	Version
 ----------------------------------------------------------------------

 */
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Types;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import patterns.config.constants.ProgramConstants;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.service.DTDObject;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeDateUtils;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.framework.web.DHTMLXGridUtility;
import patterns.config.framework.web.GenericOption;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.framework.web.reports.ReportRM;
import patterns.config.framework.web.reports.ReportUtils;
import patterns.config.validations.AccessValidator;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.InventoryValidator;
import patterns.config.validations.InvoiceValidator;
import patterns.config.validations.LeaseValidator;
import patterns.config.validations.MasterValidator;
import patterns.config.validations.RegistrationValidator;
import patterns.config.web.forms.GenericFormBean;

public class eleasebean extends GenericFormBean {
	String mainSl;
	DTDObject component_object = null;
	DTDObject asset_object = null;
	DTObject discamountDTO = null;
	DTDObject dtdObject4 = new DTDObject();

	private static final long serialVersionUID = 1L;
	private String custLeaseCode;
	private String custID;
	private String leaseProductType;
	private String taxScheduleCode;
	private String finSerial;
	private boolean iAgree;

	public boolean isiAgree() {
		return iAgree;
	}

	public void setiAgree(boolean iAgree) {
		this.iAgree = iAgree;
	}

	public String getFinSerial() {
		return finSerial;
	}

	public void setFinSerial(String finSerial) {
		this.finSerial = finSerial;
	}

	public String getTaxScheduleCode() {
		return taxScheduleCode;
	}

	public void setTaxScheduleCode(String taxScheduleCode) {
		this.taxScheduleCode = taxScheduleCode;
	}

	public String getLeaseProductType() {
		return leaseProductType;
	}

	public void setLeaseProductType(String leaseProductType) {
		this.leaseProductType = leaseProductType;
	}

	public String getCustID() {
		return custID;
	}

	public void setCustID(String custID) {
		this.custID = custID;
	}

	private String agreementNo;
	private String scheduleID;
	private String agreementDate;

	public String getAgreementDate() {
		return agreementDate;
	}

	public void setAgreementDate(String agreementDate) {
		this.agreementDate = agreementDate;
	}

	// TAb 1
	private String createDate;
	private String leaseComDate;
	private String billAddress;
	private String billBranch;
	private String billState;
	private String paymentUptoTenor;
	private String shipAddress;
	private String shippingState;

	public String getShippingState() {
		return shippingState;
	}

	public void setShippingState(String shippingState) {
		this.shippingState = shippingState;
	}

	public String getShipAddress() {
		return shipAddress;
	}

	public void setShipAddress(String shipAddress) {
		this.shipAddress = shipAddress;
	}

	public String getPaymentUptoTenor() {
		return paymentUptoTenor;
	}

	public void setPaymentUptoTenor(String paymentUptoTenor) {
		this.paymentUptoTenor = paymentUptoTenor;
	}

	public String getBillState() {
		return billState;
	}

	public void setBillState(String billState) {
		this.billState = billState;
	}

	private String invType;
	private String leaseProduct;
	private String assetCurrency;
	private String copyGridField;

	public String getCopyGridField() {
		return copyGridField;
	}

	public void setCopyGridField(String copyGridField) {
		this.copyGridField = copyGridField;
	}

	public String getAssetCurrency() {
		return assetCurrency;
	}

	public void setAssetCurrency(String assetCurrency) {
		this.assetCurrency = assetCurrency;
	}

	public String getLeaseProduct() {
		return leaseProduct;
	}

	public void setLeaseProduct(String leaseProduct) {
		this.leaseProduct = leaseProduct;
	}

	// TAb 3
	private String repaymentMode;
	private String ecsBank;
	private String ecsBranch;
	private String ecsIFSC;
	private String cusAccNumber;
	private String peakAmount;
	private String peakAmountFormat;
	private String ecsRefNo;
	private String ecsRefDate;
	private String remarks;

	// Tab 2
	private String noOfAssets;
	private String assetSl;
	private String assetCategory;
	private String autoNonAuto;
	private String assetComponent;
	private String componentSerial;
	private String comAssetID;
	private String comAssetDesc;
	private String comQty;

	private boolean comIndidualReq;
	private String xmlleaseNoOfAssetsGrid;
	private String xmlleaseAssetComponentGrid;
	private String xmlleaseRentalChargesGrid;
	private String xmlleaseApplicableTaxGrid;
	private String xmlleaseFinanceScheduleGrid;
	private String xmlleasePaymentGrid;

	public String getXmlleasePaymentGrid() {
		return xmlleasePaymentGrid;
	}

	public void setXmlleasePaymentGrid(String xmlleasePaymentGrid) {
		this.xmlleasePaymentGrid = xmlleasePaymentGrid;
	}

	public String getXmlleaseApplicableTaxGrid() {
		return xmlleaseApplicableTaxGrid;
	}

	public void setXmlleaseApplicableTaxGrid(String xmlleaseApplicableTaxGrid) {
		this.xmlleaseApplicableTaxGrid = xmlleaseApplicableTaxGrid;
	}

	public String getXmlleaseFinanceScheduleGrid() {
		return xmlleaseFinanceScheduleGrid;
	}

	public void setXmlleaseFinanceScheduleGrid(String xmlleaseFinanceScheduleGrid) {
		this.xmlleaseFinanceScheduleGrid = xmlleaseFinanceScheduleGrid;
	}

	public String getXmlleaseNoOfAssetsGrid() {
		return xmlleaseNoOfAssetsGrid;
	}

	public void setXmlleaseNoOfAssetsGrid(String xmlleaseNoOfAssetsGrid) {
		this.xmlleaseNoOfAssetsGrid = xmlleaseNoOfAssetsGrid;
	}

	private String rental;
	private String rentalFormat;

	public String getRentalFormat() {
		return rentalFormat;
	}

	public void setRentalFormat(String rentalFormat) {
		this.rentalFormat = rentalFormat;
	}

	private String execChags;
	private String execChagsFormat;
	private String uptoToner;

	private String tenor;
	private String cancelPeriod;
	private String repayFreq;
	private String advArrers;
	private String firstPaymet;
	private String intFromDate;

	public String getFinalFromDate() {
		return finalFromDate;
	}

	public void setFinalFromDate(String finalFromDate) {
		this.finalFromDate = finalFromDate;
	}

	public String getFinalUptoDate() {
		return finalUptoDate;
	}

	public void setFinalUptoDate(String finalUptoDate) {
		this.finalUptoDate = finalUptoDate;
	}

	private String intUptoDate;
	private String finalFromDate;
	private String finalUptoDate;
	private String rentDate;
	private String lastDate;
	private String grossAssetCost;
	private String grossAssetCostFormat;
	private String commencementValue;
	private String commencementValueFormat;
	private String leaseRentOption;
	private String leaseRentRate;
	private String assureRVPer;
	private String intBrokenRent;
	private String intBrokenExec;
	private String finalBrokenRent;
	private String finaBrokenExec;
	private String intBrokenRentFormat;
	private String intBrokenExecFormat;
	private String finalBrokenRentFormat;
	private String finaBrokenExecFormat;
	private String dealType;
	private String grossRentalAmtWithTax;

	private String grossNetTDS;

	private String processCode;
	private String invTempCode;

	public String getProcessCode() {
		return processCode;
	}

	public void setProcessCode(String processCode) {
		this.processCode = processCode;
	}

	public String getInvTempCode() {
		return invTempCode;
	}

	public void setInvTempCode(String invTempCode) {
		this.invTempCode = invTempCode;
	}

	public String getInvCycleNo() {
		return invCycleNo;
	}

	public void setInvCycleNo(String invCycleNo) {
		this.invCycleNo = invCycleNo;
	}

	public String gethierarCode() {
		return hierarCode;
	}

	public void sethierarCode(String hierarCode) {
		this.hierarCode = hierarCode;
	}

	private String invCycleNo;
	private String initCycleNo;
	private String finalCycleNo;

	public String getInitCycleNo() {
		return initCycleNo;
	}

	public void setInitCycleNo(String initCycleNo) {
		this.initCycleNo = initCycleNo;
	}

	public String getFinalCycleNo() {
		return finalCycleNo;
	}

	public void setFinalCycleNo(String finalCycleNo) {
		this.finalCycleNo = finalCycleNo;
	}

	private String hierarCode;

	public String getRental() {
		return rental;
	}

	public void setRental(String rental) {
		this.rental = rental;
	}

	public String getExecChags() {
		return execChags;
	}

	public void setExecChags(String execChags) {
		this.execChags = execChags;
	}

	public String getExecChagsFormat() {
		return execChagsFormat;
	}

	public void setExecChagsFormat(String execChagsFormat) {
		this.execChagsFormat = execChagsFormat;
	}

	public String getXmlleaseRentalChargesGrid() {
		return xmlleaseRentalChargesGrid;
	}

	public void setXmlleaseRentalChargesGrid(String xmlleaseRentalChargesGrid) {
		this.xmlleaseRentalChargesGrid = xmlleaseRentalChargesGrid;
	}

	public String getUptoToner() {
		return uptoToner;
	}

	public void setUptoToner(String uptoToner) {
		this.uptoToner = uptoToner;
	}

	public String getCustLeaseCode() {
		return custLeaseCode;
	}

	public void setCustLeaseCode(String custLeaseCode) {
		this.custLeaseCode = custLeaseCode;
	}

	public String getAgreementNo() {
		return agreementNo;
	}

	public void setAgreementNo(String agreementNo) {
		this.agreementNo = agreementNo;
	}

	public String getScheduleID() {
		return scheduleID;
	}

	public void setScheduleID(String scheduleID) {
		this.scheduleID = scheduleID;
	}

	public String getXmlleaseAssetComponentGrid() {
		return xmlleaseAssetComponentGrid;
	}

	public void setXmlleaseAssetComponentGrid(String xmlleaseAssetComponentGrid) {
		this.xmlleaseAssetComponentGrid = xmlleaseAssetComponentGrid;
	}

	public boolean isComIndidualReq() {
		return comIndidualReq;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getLeaseComDate() {
		return leaseComDate;
	}

	public void setLeaseComDate(String leaseComDate) {
		this.leaseComDate = leaseComDate;
	}

	public void setComIndidualReq(boolean comIndidualReq) {
		this.comIndidualReq = comIndidualReq;
	}

	public String getComponentSerial() {
		return componentSerial;
	}

	public void setComponentSerial(String componentSerial) {
		this.componentSerial = componentSerial;
	}

	public String getComAssetID() {
		return comAssetID;
	}

	public void setComAssetID(String comAssetID) {
		this.comAssetID = comAssetID;
	}

	public String getComAssetDesc() {
		return comAssetDesc;
	}

	public void setComAssetDesc(String comAssetDesc) {
		this.comAssetDesc = comAssetDesc;
	}

	public String getComQty() {
		return comQty;
	}

	public void setComQty(String comQty) {
		this.comQty = comQty;
	}

	public String getBillAddress() {
		return billAddress;
	}

	public void setBillAddress(String billAddress) {
		this.billAddress = billAddress;
	}

	public String getBillBranch() {
		return billBranch;
	}

	public void setBillBranch(String billBranch) {
		this.billBranch = billBranch;
	}

	public String getInvType() {
		return invType;
	}

	public void setInvType(String invType) {
		this.invType = invType;
	}

	public String getRepaymentMode() {
		return repaymentMode;
	}

	public void setRepaymentMode(String repaymentMode) {
		this.repaymentMode = repaymentMode;
	}

	public String getEcsBank() {
		return ecsBank;
	}

	public void setEcsBank(String ecsBank) {
		this.ecsBank = ecsBank;
	}

	public String getEcsBranch() {
		return ecsBranch;
	}

	public void setEcsBranch(String ecsBranch) {
		this.ecsBranch = ecsBranch;
	}

	public String getEcsIFSC() {
		return ecsIFSC;
	}

	public void setEcsIFSC(String ecsIFSC) {
		this.ecsIFSC = ecsIFSC;
	}

	public String getCusAccNumber() {
		return cusAccNumber;
	}

	public void setCusAccNumber(String cusAccNumber) {
		this.cusAccNumber = cusAccNumber;
	}

	public String getPeakAmount() {
		return peakAmount;
	}

	public void setPeakAmount(String peakAmount) {
		this.peakAmount = peakAmount;
	}

	public String getPeakAmountFormat() {
		return peakAmountFormat;
	}

	public void setPeakAmountFormat(String peakAmountFormat) {
		this.peakAmountFormat = peakAmountFormat;
	}

	public String getEcsRefNo() {
		return ecsRefNo;
	}

	public void setEcsRefNo(String ecsRefNo) {
		this.ecsRefNo = ecsRefNo;
	}

	public String getEcsRefDate() {
		return ecsRefDate;
	}

	public void setEcsRefDate(String ecsRefDate) {
		this.ecsRefDate = ecsRefDate;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getNoOfAssets() {
		return noOfAssets;
	}

	public void setNoOfAssets(String noOfAssets) {
		this.noOfAssets = noOfAssets;
	}

	public String getAssetSl() {
		return assetSl;
	}

	public void setAssetSl(String assetSl) {
		this.assetSl = assetSl;
	}

	public String getAssetCategory() {
		return assetCategory;
	}

	public void setAssetCategory(String assetCategory) {
		this.assetCategory = assetCategory;
	}

	public String getAutoNonAuto() {
		return autoNonAuto;
	}

	public void setAutoNonAuto(String autoNonAuto) {
		this.autoNonAuto = autoNonAuto;
	}

	public String getAssetComponent() {
		return assetComponent;
	}

	public void setAssetComponent(String assetComponent) {
		this.assetComponent = assetComponent;
	}

	public String getTenor() {
		return tenor;
	}

	public void setTenor(String tenor) {
		this.tenor = tenor;
	}

	public String getCancelPeriod() {
		return cancelPeriod;
	}

	public void setCancelPeriod(String cancelPeriod) {
		this.cancelPeriod = cancelPeriod;
	}

	public String getRepayFreq() {
		return repayFreq;
	}

	public void setRepayFreq(String repayFreq) {
		this.repayFreq = repayFreq;
	}

	public String getAdvArrers() {
		return advArrers;
	}

	public void setAdvArrers(String advArrers) {
		this.advArrers = advArrers;
	}

	public String getFirstPaymet() {
		return firstPaymet;
	}

	public void setFirstPaymet(String firstPaymet) {
		this.firstPaymet = firstPaymet;
	}

	public String getIntFromDate() {
		return intFromDate;
	}

	public void setIntFromDate(String intFromDate) {
		this.intFromDate = intFromDate;
	}

	public String getIntUptoDate() {
		return intUptoDate;
	}

	public void setIntUptoDate(String intUptoDate) {
		this.intUptoDate = intUptoDate;
	}

	public String getRentDate() {
		return rentDate;
	}

	public void setRentDate(String rentDate) {
		this.rentDate = rentDate;
	}

	public String getLastDate() {
		return lastDate;
	}

	public void setLastDate(String lastDate) {
		this.lastDate = lastDate;
	}

	public String getGrossAssetCost() {
		return grossAssetCost;
	}

	public void setGrossAssetCost(String grossAssetCost) {
		this.grossAssetCost = grossAssetCost;
	}

	public String getGrossAssetCostFormat() {
		return grossAssetCostFormat;
	}

	public void setGrossAssetCostFormat(String grossAssetCostFormat) {
		this.grossAssetCostFormat = grossAssetCostFormat;
	}

	public String getCommencementValue() {
		return commencementValue;
	}

	public void setCommencementValue(String commencementValue) {
		this.commencementValue = commencementValue;
	}

	public String getCommencementValueFormat() {
		return commencementValueFormat;
	}

	public void setCommencementValueFormat(String commencementValueFormat) {
		this.commencementValueFormat = commencementValueFormat;
	}

	public String getLeaseRentOption() {
		return leaseRentOption;
	}

	public void setLeaseRentOption(String leaseRentOption) {
		this.leaseRentOption = leaseRentOption;
	}

	public String getLeaseRentRate() {
		return leaseRentRate;
	}

	public void setLeaseRentRate(String leaseRentRate) {
		this.leaseRentRate = leaseRentRate;
	}

	public String getAssureRVPer() {
		return assureRVPer;
	}

	public void setAssureRVPer(String assureRVPer) {
		this.assureRVPer = assureRVPer;
	}

	public String getIntBrokenRent() {
		return intBrokenRent;
	}

	public void setIntBrokenRent(String intBrokenRent) {
		this.intBrokenRent = intBrokenRent;
	}

	public String getIntBrokenExec() {
		return intBrokenExec;
	}

	public void setIntBrokenExec(String intBrokenExec) {
		this.intBrokenExec = intBrokenExec;
	}

	public String getFinalBrokenRent() {
		return finalBrokenRent;
	}

	public void setFinalBrokenRent(String finalBrokenRent) {
		this.finalBrokenRent = finalBrokenRent;
	}

	public String getFinaBrokenExec() {
		return finaBrokenExec;
	}

	public void setFinaBrokenExec(String finaBrokenExec) {
		this.finaBrokenExec = finaBrokenExec;
	}

	public String getIntBrokenRentFormat() {
		return intBrokenRentFormat;
	}

	public void setIntBrokenRentFormat(String intBrokenRentFormat) {
		this.intBrokenRentFormat = intBrokenRentFormat;
	}

	public String getIntBrokenExecFormat() {
		return intBrokenExecFormat;
	}

	public void setIntBrokenExecFormat(String intBrokenExecFormat) {
		this.intBrokenExecFormat = intBrokenExecFormat;
	}

	public String getFinalBrokenRentFormat() {
		return finalBrokenRentFormat;
	}

	public void setFinalBrokenRentFormat(String finalBrokenRentFormat) {
		this.finalBrokenRentFormat = finalBrokenRentFormat;
	}

	public String getFinaBrokenExecFormat() {
		return finaBrokenExecFormat;
	}

	public void setFinaBrokenExecFormat(String finaBrokenExecFormat) {
		this.finaBrokenExecFormat = finaBrokenExecFormat;
	}

	public String getDealType() {
		return dealType;
	}

	public void setDealType(String dealType) {
		this.dealType = dealType;
	}

	public String getGrossRentalAmtWithTax() {
		return grossRentalAmtWithTax;
	}

	public void setGrossRentalAmtWithTax(String grossRentalAmtWithTax) {
		this.grossRentalAmtWithTax = grossRentalAmtWithTax;
	}

	public String getGrossNetTDS() {
		return grossNetTDS;
	}

	public void setGrossNetTDS(String grossNetTDS) {
		this.grossNetTDS = grossNetTDS;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	// TAB 6
	private String principalAmt;
	private String principalAmtFormat;
	private String interestAmt;
	private String interestAmtFormat;

	public String getPrincipalAmt() {
		return principalAmt;
	}

	public void setPrincipalAmt(String principalAmt) {
		this.principalAmt = principalAmt;
	}

	public String getPrincipalAmtFormat() {
		return principalAmtFormat;
	}

	public void setPrincipalAmtFormat(String principalAmtFormat) {
		this.principalAmtFormat = principalAmtFormat;
	}

	public String getInterestAmt() {
		return interestAmt;
	}

	public void setInterestAmt(String interestAmt) {
		this.interestAmt = interestAmt;
	}

	public String getInterestAmtFormat() {
		return interestAmtFormat;
	}

	public void setInterestAmtFormat(String interestAmtFormat) {
		this.interestAmtFormat = interestAmtFormat;
	}

	private String famDesc;

	public String getFamDesc() {
		return famDesc;
	}

	public void setFamDesc(String famDesc) {
		this.famDesc = famDesc;
	}

	private String assureRVAmount;
	private String assureRVAmount_curr;
	private String assureRVAmountFormat;
	private String assureRVType;
	private String assureRVTypeValue;
	private String preLeaseRefDate;
	private String internalRateReturn;
	private String assetModel;
	private String assetConfig;

	public String getAssetModel() {
		return assetModel;
	}

	public void setAssetModel(String assetModel) {
		this.assetModel = assetModel;
	}

	public String getAssetConfig() {
		return assetConfig;
	}

	public void setAssetConfig(String assetConfig) {
		this.assetConfig = assetConfig;
	}

	public String getInternalRateReturn() {
		return internalRateReturn;
	}

	public void setInternalRateReturn(String internalRateReturn) {
		this.internalRateReturn = internalRateReturn;
	}

	public String getPreLeaseRefDate() {
		return preLeaseRefDate;
	}

	public void setPreLeaseRefDate(String preLeaseRefDate) {
		this.preLeaseRefDate = preLeaseRefDate;
	}

	public String getPreLeaseRefSerial() {
		return preLeaseRefSerial;
	}

	public void setPreLeaseRefSerial(String preLeaseRefSerial) {
		this.preLeaseRefSerial = preLeaseRefSerial;
	}

	public String getSpSerial() {
		return spSerial;
	}

	public void setSpSerial(String spSerial) {
		this.spSerial = spSerial;
	}

	private String preLeaseRefSerial;
	private String spSerial;

	public String getAssureRVAmount() {
		return assureRVAmount;
	}

	public void setAssureRVAmount(String assureRVAmount) {
		this.assureRVAmount = assureRVAmount;
	}

	public String getAssureRVAmount_curr() {
		return assureRVAmount_curr;
	}

	public void setAssureRVAmount_curr(String assureRVAmount_curr) {
		this.assureRVAmount_curr = assureRVAmount_curr;
	}

	public String getAssureRVAmountFormat() {
		return assureRVAmountFormat;
	}

	public void setAssureRVAmountFormat(String assureRVAmountFormat) {
		this.assureRVAmountFormat = assureRVAmountFormat;
	}

	public String getAssureRVType() {
		return assureRVType;
	}

	public void setAssureRVType(String assureRVType) {
		this.assureRVType = assureRVType;
	}

	public String getAssureRVTypeValue() {
		return assureRVTypeValue;
	}

	public void setAssureRVTypeValue(String assureRVTypeValue) {
		this.assureRVTypeValue = assureRVTypeValue;
	}

	public void reset() {
		custLeaseCode = RegularConstants.EMPTY_STRING;
		custID = RegularConstants.EMPTY_STRING;
		agreementNo = RegularConstants.EMPTY_STRING;
		scheduleID = RegularConstants.EMPTY_STRING;
		preLeaseRefDate = RegularConstants.EMPTY_STRING;
		preLeaseRefSerial = RegularConstants.EMPTY_STRING;
		spSerial = RegularConstants.EMPTY_STRING;
		iAgree = false;
		agreementDate = RegularConstants.EMPTY_STRING;
		// Tab 1
		createDate = RegularConstants.EMPTY_STRING;
		leaseComDate = RegularConstants.EMPTY_STRING;
		billAddress = RegularConstants.EMPTY_STRING;
		shipAddress = RegularConstants.EMPTY_STRING;
		shippingState = RegularConstants.EMPTY_STRING;
		billBranch = RegularConstants.EMPTY_STRING;
		billState = RegularConstants.EMPTY_STRING;
		invType = RegularConstants.EMPTY_STRING;
		leaseProduct = RegularConstants.EMPTY_STRING;
		leaseProductType = RegularConstants.EMPTY_STRING;
		taxScheduleCode = RegularConstants.EMPTY_STRING;
		assetCurrency = RegularConstants.EMPTY_STRING;
		processCode = RegularConstants.EMPTY_STRING;
		invTempCode = RegularConstants.EMPTY_STRING;
		invCycleNo = RegularConstants.EMPTY_STRING;
		hierarCode = RegularConstants.EMPTY_STRING;
		finSerial = RegularConstants.EMPTY_STRING;

		// Tab 2
		noOfAssets = RegularConstants.EMPTY_STRING;
		assetSl = RegularConstants.EMPTY_STRING;
		assetCategory = RegularConstants.EMPTY_STRING;
		autoNonAuto = RegularConstants.EMPTY_STRING;
		assetComponent = RegularConstants.EMPTY_STRING;
		assetModel = RegularConstants.EMPTY_STRING;
		assetConfig = RegularConstants.EMPTY_STRING;
		// tab 1
		componentSerial = RegularConstants.EMPTY_STRING;
		comAssetID = RegularConstants.EMPTY_STRING;
		comAssetDesc = RegularConstants.EMPTY_STRING;
		comQty = RegularConstants.EMPTY_STRING;
		comIndidualReq = false;
		xmlleaseAssetComponentGrid = RegularConstants.EMPTY_STRING;

		// TAB 3
		xmlleaseRentalChargesGrid = RegularConstants.EMPTY_STRING;
		rental = RegularConstants.EMPTY_STRING;
		rentalFormat = RegularConstants.EMPTY_STRING;
		execChags = RegularConstants.EMPTY_STRING;
		execChagsFormat = RegularConstants.EMPTY_STRING;
		uptoToner = RegularConstants.EMPTY_STRING;
		tenor = RegularConstants.EMPTY_STRING;
		cancelPeriod = RegularConstants.EMPTY_STRING;
		repayFreq = RegularConstants.EMPTY_STRING;
		advArrers = RegularConstants.EMPTY_STRING;
		firstPaymet = RegularConstants.EMPTY_STRING;
		intFromDate = RegularConstants.EMPTY_STRING;
		intUptoDate = RegularConstants.EMPTY_STRING;
		finalFromDate = RegularConstants.EMPTY_STRING;
		finalUptoDate = RegularConstants.EMPTY_STRING;
		rentDate = RegularConstants.EMPTY_STRING;
		lastDate = RegularConstants.EMPTY_STRING;
		grossAssetCost = RegularConstants.EMPTY_STRING;
		grossAssetCostFormat = RegularConstants.EMPTY_STRING;
		commencementValue = RegularConstants.EMPTY_STRING;
		commencementValueFormat = RegularConstants.EMPTY_STRING;
		leaseRentOption = RegularConstants.EMPTY_STRING;
		assureRVPer = RegularConstants.EMPTY_STRING;
		assureRVAmount = RegularConstants.EMPTY_STRING;
		assureRVAmount_curr = RegularConstants.EMPTY_STRING;
		assureRVAmountFormat = RegularConstants.EMPTY_STRING;
		assureRVType = RegularConstants.EMPTY_STRING;
		assureRVTypeValue = RegularConstants.EMPTY_STRING;
		internalRateReturn = RegularConstants.EMPTY_STRING;
		// TAB 4
		intBrokenRent = RegularConstants.EMPTY_STRING;
		intBrokenExec = RegularConstants.EMPTY_STRING;
		finalBrokenRent = RegularConstants.EMPTY_STRING;
		finaBrokenExec = RegularConstants.EMPTY_STRING;
		intBrokenRentFormat = RegularConstants.EMPTY_STRING;
		intBrokenExecFormat = RegularConstants.EMPTY_STRING;
		finalBrokenRentFormat = RegularConstants.EMPTY_STRING;
		finaBrokenExecFormat = RegularConstants.EMPTY_STRING;
		dealType = RegularConstants.EMPTY_STRING;
		leaseRentRate = RegularConstants.EMPTY_STRING;
		grossRentalAmtWithTax = RegularConstants.EMPTY_STRING;

		// tab 6 main
		grossNetTDS = RegularConstants.EMPTY_STRING;
		paymentUptoTenor = RegularConstants.EMPTY_STRING;
		repaymentMode = RegularConstants.EMPTY_STRING;
		ecsBank = RegularConstants.EMPTY_STRING;
		ecsBranch = RegularConstants.EMPTY_STRING;
		ecsIFSC = RegularConstants.EMPTY_STRING;
		cusAccNumber = RegularConstants.EMPTY_STRING;
		peakAmount = RegularConstants.EMPTY_STRING;
		peakAmountFormat = RegularConstants.EMPTY_STRING;
		ecsRefNo = RegularConstants.EMPTY_STRING;
		ecsRefDate = RegularConstants.EMPTY_STRING;
		processCode = RegularConstants.EMPTY_STRING;
		invTempCode = RegularConstants.EMPTY_STRING;
		invCycleNo = RegularConstants.EMPTY_STRING;
		initCycleNo = RegularConstants.EMPTY_STRING;
		finalCycleNo = RegularConstants.EMPTY_STRING;
		hierarCode = RegularConstants.EMPTY_STRING;
		remarks = RegularConstants.EMPTY_STRING;

		famDesc = RegularConstants.EMPTY_STRING;

		// TAB 6
		principalAmt = RegularConstants.EMPTY_STRING;
		principalAmtFormat = RegularConstants.EMPTY_STRING;
		interestAmt = RegularConstants.EMPTY_STRING;
		interestAmtFormat = RegularConstants.EMPTY_STRING;

		xmlleaseApplicableTaxGrid = RegularConstants.EMPTY_STRING;
		xmlleaseFinanceScheduleGrid = RegularConstants.EMPTY_STRING;
		xmlleaseNoOfAssetsGrid = RegularConstants.EMPTY_STRING;
		xmlleasePaymentGrid = RegularConstants.EMPTY_STRING;

		DBContext dbContext = new DBContext();
		ArrayList<GenericOption> leaseType = null;
		leaseType = getGenericOptions("COMMON", "LEASE_TYPE", dbContext, null);
		webContext.getRequest().setAttribute("COMMON_LEASE_TYPE", leaseType);

		ArrayList<GenericOption> invType = null;
		invType = getGenericOptions("COMMON", "INVOICE_TYPE", dbContext, null);
		webContext.getRequest().setAttribute("COMMON_INVOICE_TYPE", invType);

		ArrayList<GenericOption> grossNet = null;
		grossNet = getGenericOptions("ELEASE", "TDS_TYPE", dbContext, null);
		webContext.getRequest().setAttribute("ELEASE_TDS_TYPE", grossNet);

		ArrayList<GenericOption> bill = null;
		bill = getGenericOptions("ELEASE", "INT_BILL_OPTION", dbContext, null);
		webContext.getRequest().setAttribute("ELEASE_INT_BILL_OPTION", bill);

		ArrayList<GenericOption> autoNonAuto = null;
		autoNonAuto = getGenericOptions("COMMON", "AUTO_NON_AUTO", dbContext, null);
		webContext.getRequest().setAttribute("COMMON_AUTO_NON_AUTO", autoNonAuto);

		ArrayList<GenericOption> repayMode = null;
		repayMode = getGenericOptions("ELEASE", "REPAY_MODE", dbContext, null);
		webContext.getRequest().setAttribute("ELEASE_REPAY_MODE", repayMode);

		ArrayList<GenericOption> repayFreq = null;
		repayFreq = getGenericOptions("COMMON", "REPAY_FREQUENCY", dbContext, null);
		webContext.getRequest().setAttribute("COMMON_REPAY_FREQUENCY", repayFreq);

		ArrayList<GenericOption> advArrears = null;
		advArrears = getGenericOptions("COMMON", "ADVANCE_ARREARS", dbContext, null);
		webContext.getRequest().setAttribute("COMMON_ADVANCE_ARREARS", advArrears);

		ArrayList<GenericOption> deaType = null;
		deaType = getGenericOptions("ELEASE", "DEAL_TYPE", dbContext, null);
		webContext.getRequest().setAttribute("ELEASE_DEAL_TYPE", deaType);

		ArrayList<GenericOption> leaseRent = null;
		leaseRent = getGenericOptions("COMMON", "RENT_PERIOD_OPTION", dbContext, null);
		webContext.getRequest().setAttribute("COMMON_RENT_PERIOD_OPTION", leaseRent);

		ArrayList<GenericOption> rvType = null;
		rvType = getGenericOptions("ELEASE", "RV_TYPE", dbContext, null);
		webContext.getRequest().setAttribute("ELEASE_RV_TYPE", rvType);

		spSerial = getMainSerial(dbContext);
		dbContext.close();

		component_object = new DTDObject();
		asset_object = new DTDObject();
		discamountDTO = new DTObject();

		setProcessBO("patterns.config.framework.bo.lss.eleaseBO");
	}

	private String getMainSerial(DBContext dbContext) {
		DBUtil util = dbContext.createUtilInstance();
		try {
			util.reset();
			String sqlQuery = "SELECT FN_NEXTVAL(?) AS SEQ_SP_SERIAL FROM DUAL";
			util.setSql(sqlQuery);
			util.setString(1, context.getProcessID());
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				int spSerial = rset.getInt("SEQ_SP_SERIAL");
				return String.valueOf(spSerial);
			}
			return "0";
		} catch (Exception e) {
			e.printStackTrace();
			return "0";
		} finally {
			util.reset();
			dbContext.close();
		}
	}

	public void validate() {
		RegistrationValidator validation = new RegistrationValidator();
		if (validateCustLeaseCode() && validateAgreementNo() && validateScheduleID()) {
			int errors = 0;
			String errMsg = RegularConstants.EMPTY_STRING;

			// TAB 1
			validateCreateDate();
			validateLeaseComDate();
			validateBillAddress();
			validateShipAddress();
			validateBillBranch();
			if (getErrorMap().length() > errors) {
				errMsg = errMsg + "Address Details  ";
				errors = getErrorMap().length();
			}

			// TAB 2
			validateNoOfAssets();
			validateAssetQtyGrid();
			if (getErrorMap().length() > errors) {
				errMsg = errMsg + "Asset Details  ";
				errors = getErrorMap().length();
			}

			// TAB 3
			validateTenor();
			validateCancelPeriod();
			validateRepayFreq();
			validateAdvArrers();
			validateFirstPaymet();
			validateIntFromDate();
			validateIntUptoDate();
			validateFinalFromDate();
			validateFinalUptoDate();
			validateRentDate();
			validateLastDate();
			if (getErrorMap().length() > errors) {
				errMsg = errMsg + "Tenor Details  ";
				errors = getErrorMap().length();
			}

			// TAB 4
			if (validateGrossAssetCostFormat())
				validateCommencementValueFormat();
			validateLeaseRentOption();
			validateAssureRVPer();
			validateAssureRVAmountFormat();
			validateInternalRateReturn();
			validateIntBrokenRentFormat();
			validateIintBrokenExecFormat();
			validateFinalBrokenRentFormat();
			validateFinaBrokenExecFormat();
			validateDealType();
			validateRentalGrid();
			if (getErrorMap().length() > errors) {
				errMsg = errMsg + "Rent Details  ";
				errors = getErrorMap().length();
			}

			// TAB 7
			validateGrossNetTDS();
			validatePaymentGrid();
			validateProcessCode();
			validateInvTempCode();
			validateFamDesc();
			validateInvCycleNo();
			validateInitCycleNo();
			validateFinalCycleNo();
			validatehierarCode();
			validateRemarks();
			if (getErrorMap().length() > errors) {
				errMsg = errMsg + "Payment Details";
				errors = getErrorMap().length();
			}
			if (getErrorMap().length() == 0) {
				try {
					// TAB 5
					setAssetCompToDTObject();

					// TAB 6
					DTObject inputDTO = new DTObject();
					// dtdObject4 = null;
					dtdObject4 = intialFinalAddcolumn(inputDTO);
					if (dtdObject4 == null) {
						Object[] errorParam = { "Finanancial Principal and Interest slabs not specfied in Financial Details " };
						getErrorMap().setError("leaseErrors", BackOfficeErrorCodes.TAB_ERROR_CHECK, errorParam);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (errors > 0) {
				errMsg = errMsg + " Sections having errors";
				Object[] errorParam = { errMsg };
				getErrorMap().setError("leaseErrors", BackOfficeErrorCodes.TAB_ERROR_CHECK, errorParam);
			}

		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.set("LESSEE_CODE", custLeaseCode);
				formDTO.set("AGREEMENT_NO", agreementNo);
				formDTO.set("SCHEDULE_ID", scheduleID);
				formDTO.set("CUSTOMER_ID", custID);

				if (!preLeaseRefDate.equals(RegularConstants.EMPTY_STRING) && !preLeaseRefSerial.equals(RegularConstants.EMPTY_STRING)) {
					formDTO.set("CONTRACT_DATE", preLeaseRefDate);
					formDTO.set("CONTRACT_SL", preLeaseRefSerial);
				} else {
					formDTO.set("CONTRACT_DATE", RegularConstants.NULL);
					formDTO.set("CONTRACT_SL", RegularConstants.NULL);
				}

				// TAB 1

				formDTO.set("DATE_OF_CREATION", createDate);
				formDTO.set("DATE_OF_COMMENCEMENT", leaseComDate);
				formDTO.set("LEASE_PRODUCT_CODE", leaseProduct);
				formDTO.set("ASSET_CCY", assetCurrency);
				formDTO.set("BILLING_ADDR_SL", billAddress);
				formDTO.set("SHIP_ADDR_SL", shipAddress);
				formDTO.set("LESSOR_BRANCH_CODE", billBranch);
				formDTO.set("LESSOR_STATE_CODE", billState);

				// TAB 2

				formDTO.set("NOF_ASSETS", noOfAssets);

				// TAB 2 GRID
				formDTO.setDTDObject("LEASEASSETS_GRID", asset_object);
				formDTO.setDTDObject("LEASEASSETCOMP_GRID", component_object);

				// TAB 3
				formDTO.set("TENOR", tenor);
				if (cancelPeriod.equals(RegularConstants.EMPTY_STRING))
					formDTO.set("NON_CANCEL_PERIOD", RegularConstants.NULL);
				else
					formDTO.set("NON_CANCEL_PERIOD", cancelPeriod);
				formDTO.set("REPAY_FREQ", repayFreq);
				formDTO.set("ADVANCE_ARREARS_FLAG", advArrers);
				formDTO.set("FIRST_REPAY_PERIOD_START_DATE", firstPaymet);
				if (!validation.isEmpty(intFromDate) && !validation.isEmpty(intUptoDate)) {
					formDTO.set("INIT_INTERIM_PERIOD_FROM", intFromDate);
					formDTO.set("INIT_INTERIM_PERIOD_UPTO", intUptoDate);
				}
				if (!validation.isEmpty(finalFromDate) && !validation.isEmpty(finalUptoDate)) {
					formDTO.set("FINAL_INTERIM_PERIOD_FROM", finalFromDate);
					formDTO.set("FINAL_INTERIM_PERIOD_UPTO", finalUptoDate);
				}

				formDTO.set("BILLING_FIRST_RENTAL_DATE", rentDate);
				formDTO.set("BILLING_LAST_RENTAL_DATE", lastDate);

				// TAB 5
				formDTO.set("TAX_SCHEDULE_CODE", taxScheduleCode);

				// TAB Grid 4
				formDTO.set("GROSS_ASSET_COST", grossAssetCost);
				formDTO.set("COMMENCEMENT_VALUE", commencementValue);
				formDTO.set("LEASE_RENTAL_PERIOD_OPTION", leaseRentOption);

				if (assureRVPer.equals(RegularConstants.EMPTY_STRING))
					formDTO.set("RV_PER", RegularConstants.NULL);
				else
					formDTO.set("RV_PER", assureRVPer);

				if (assureRVAmount.equals(RegularConstants.EMPTY_STRING))
					formDTO.set("RV_AMOUNT", RegularConstants.NULL);
				else
					formDTO.set("RV_AMOUNT", assureRVAmount);

				if (!"O".equals(leaseProductType))
					formDTO.set("RV_TYPE", "T");
				else
					formDTO.set("RV_TYPE", "A");

				if (internalRateReturn.equals(RegularConstants.EMPTY_STRING))
					formDTO.set("IRR_VALUE", RegularConstants.NULL);
				else
					formDTO.set("IRR_VALUE", internalRateReturn);

				if (intBrokenRent.equals(RegularConstants.EMPTY_STRING))
					formDTO.set("INIT_BROKEN_PERIOD_RENT", RegularConstants.NULL);
				else
					formDTO.set("INIT_BROKEN_PERIOD_RENT", intBrokenRent);
				if (intBrokenExec.equals(RegularConstants.EMPTY_STRING))
					formDTO.set("INIT_BROKEN_PERIOD_EXEC_CHGS", RegularConstants.NULL);
				else
					formDTO.set("INIT_BROKEN_PERIOD_EXEC_CHGS", intBrokenExec);
				if (finalBrokenRent.equals(RegularConstants.EMPTY_STRING))
					formDTO.set("FINAL_BROKEN_PERIOD_RENT", RegularConstants.NULL);
				else
					formDTO.set("FINAL_BROKEN_PERIOD_RENT", finalBrokenRent);
				if (finaBrokenExec.equals(RegularConstants.EMPTY_STRING))
					formDTO.set("FINAL_BROKEN_PERIOD_EXEC_CHGS", RegularConstants.NULL);
				else
					formDTO.set("FINAL_BROKEN_PERIOD_EXEC_CHGS", finaBrokenExec);
				formDTO.set("DEAL_TYPE", dealType);

				DTDObject dtdObject1 = new DTDObject();
				dtdObject1.addColumn(0, "DTL_SL");
				dtdObject1.addColumn(1, "UPTO_TENOR");
				dtdObject1.addColumn(2, "LEASE_RENTAL_RATE");
				dtdObject1.addColumn(3, "TENOR_RENT_FORMAT");
				dtdObject1.addColumn(4, "TENOR_EXEC_CHGS_FORMAT");
				dtdObject1.addColumn(5, "TENOR_RENT");
				dtdObject1.addColumn(6, "TENOR_EXEC_CHGS");
				dtdObject1.setXML(xmlleaseRentalChargesGrid);
				formDTO.setDTDObject("LEASERENTSTRUC_GRID", dtdObject1);

				formDTO.setDTDObject("LEASEFINANCESCHEDULE_GRID", dtdObject4);

				// Tab 7
				DTDObject leasePayment = new DTDObject();
				leasePayment.addColumn(0, "SL");
				leasePayment.addColumn(1, "LINK");
				leasePayment.addColumn(2, "DTL_SL");
				leasePayment.addColumn(3, "UPTO_TENOR");
				leasePayment.addColumn(4, "REPAY_MODE_DESC");
				leasePayment.addColumn(5, "ECS_DB_IFSC");
				leasePayment.addColumn(6, "ECS_DB_BANK");
				leasePayment.addColumn(7, "ECS_DB_BRANCH");
				leasePayment.addColumn(8, "ECS_DB_AC_NO");
				leasePayment.addColumn(9, "ECS_DB_PEAK_AMOUNT_FORMAT");
				leasePayment.addColumn(10, "ECS_MANDATE_REFNO");
				leasePayment.addColumn(11, "ECS_MANDATE_DATE");
				leasePayment.addColumn(12, "REPAY_MODE");
				leasePayment.addColumn(13, "ECS_DB_PEAK_AMOUNT");
				leasePayment.setXML(xmlleasePaymentGrid);
				formDTO.setDTDObject("LEASEREPAYMENT_GRID", leasePayment);

				formDTO.set("TDS_GROSS_NET_INDICATOR", grossNetTDS);

				formDTO.set("BANK_PROCESS_CODE", processCode);
				formDTO.set("INVOICE_TEMPLATE", invTempCode);
				if (!famDesc.equals(RegularConstants.EMPTY_STRING))
					formDTO.set("FAM_CODE", famDesc);
				else
					formDTO.set("FAM_CODE", RegularConstants.NULL);

				formDTO.set("LAM_HIER_CODE", hierarCode);
				formDTO.set("INV_CYCLE_NUMBER", invCycleNo);
				if (!initCycleNo.equals(RegularConstants.EMPTY_STRING))
					formDTO.set("INIT_INTERIM_CYCLE_NUMBER", initCycleNo);

				if (!finalCycleNo.equals(RegularConstants.EMPTY_STRING))
					formDTO.set("FINAL_INTERIM_CYCLE_NUMBER", finalCycleNo);

				formDTO.set("REMARKS", remarks);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("custLeaseCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
	}

	public boolean validateCustLeaseCode() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("SUNDRY_DB_AC", custLeaseCode);
			formDTO.set(ContentManager.ACTION, USAGE);
			formDTO = validateCustLeaseCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("custLeaseCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("custLeaseCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateCustLeaseCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		DTObject formDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, ContentManager.DATA_UNAVAILABLE);
		RegistrationValidator validation = new RegistrationValidator();
		MasterValidator validator = new MasterValidator();
		LeaseValidator validate = new LeaseValidator();
		String custLeaseCode = inputDTO.get("SUNDRY_DB_AC");
		DBContext dbContext = validation.getDbContext();
		DBUtil util = dbContext.createUtilInstance();
		inputDTO.setObject("DB_UTIL", util);
		try {
			if (validation.isEmpty(custLeaseCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			resultDTO = getCustomerLesseeCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
			leaseProduct = resultDTO.get("LEASE_PRODUCT_CODE");
			inputDTO.set("CUSTOMER_ID", resultDTO.get("CUSTOMER_ID"));
			inputDTO.set(ContentManager.FETCH_COLUMNS, "CUSTOMER_ID,CUSTOMER_NAME");
			resultDTO = validation.valildateCustomerID(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			} else if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
				custID = resultDTO.get("CUSTOMER_ID");
				inputDTO.set("LEASE_PRODUCT_CODE", leaseProduct);
				inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION,LEASE_ASSET_IN_CCY,LEASE_TYPE");
				formDTO = validator.validateLeaseProductCode(inputDTO);
				if (formDTO.get(ContentManager.ERROR) != null) {
					resultDTO.set(ContentManager.ERROR, formDTO.get(ContentManager.ERROR));
					return resultDTO;
				}
				if (formDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
					return resultDTO;
				}
				assetCurrency = formDTO.get("LEASE_ASSET_IN_CCY");
				leaseProductType = formDTO.get("LEASE_TYPE");
				resultDTO.set("LEASE_ASSET_IN_CCY", formDTO.get("LEASE_ASSET_IN_CCY"));
				resultDTO.set("LEASE_TYPE", formDTO.get("LEASE_TYPE"));
				resultDTO.set("LEASE_DESC", formDTO.get("DESCRIPTION"));
				resultDTO.set("LEASE_PRODUCT_CODE", leaseProduct);
				inputDTO.set("CCY_CODE", assetCurrency);
				inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
				formDTO = validator.validateCurrency(inputDTO);
				if (formDTO.get(ContentManager.ERROR) != null) {
					resultDTO.set(ContentManager.ERROR, formDTO.get(ContentManager.ERROR));
					return resultDTO;
				}
				if (formDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
					return resultDTO;
				}
				resultDTO.set("CUR_DESC", formDTO.get("DESCRIPTION"));
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
			validator.close();
			validate.close();
		}
		return resultDTO;
	}

	public DTObject getCustomerLesseeCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil util = (DBUtil) inputDTO.getObject("DB_UTIL");
		try {
			String sqlQuery = "SELECT CUSTOMER_ID,LEASE_PRODUCT_CODE FROM CUSTOMERACDTL WHERE ENTITY_CODE=? AND SUNDRY_DB_AC=?";
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, context.getEntityCode());
			util.setString(2, inputDTO.get("SUNDRY_DB_AC"));
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				resultDTO.set("CUSTOMER_ID", rset.getString("CUSTOMER_ID"));
				resultDTO.set("LEASE_PRODUCT_CODE", rset.getString("LEASE_PRODUCT_CODE"));
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return resultDTO;
	}

	public boolean validateAgreementNo() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("CUSTOMER_ID", custID);
			formDTO.set("AGREEMENT_NO", agreementNo);
			formDTO.set(ContentManager.ACTION, USAGE);
			formDTO = validateAgreementNo(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("agreementNo", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("agreementNo", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateAgreementNo(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, ContentManager.DATA_UNAVAILABLE);
		LeaseValidator validation = new LeaseValidator();
		String custID = inputDTO.get("CUSTOMER_ID");
		String agreementNo = inputDTO.get("AGREEMENT_NO");
		try {
			if (validation.isEmpty(custID) && validation.isEmpty(agreementNo)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			if (!validation.isValidNumber(agreementNo)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_NUMBER);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "AGREEMENT_NO,TO_CHAR(AGREEMENT_DATE,'" + context.getDateFormat() + "') AGREEMENT_DATE");
			resultDTO = validation.validateCustomerAgreement(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
			agreementDate = resultDTO.get("AGREEMENT_DATE");
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateScheduleID() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("ENTITY_CODE", context.getEntityCode());
			formDTO.set("LESSEE_CODE", custLeaseCode);
			formDTO.set("AGREEMENT_NO", agreementNo);
			formDTO.set("SCHEDULE_ID", scheduleID);
			formDTO.set(ContentManager.ACTION, getAction());
			formDTO.set(ContentManager.RECTIFY, getRectify());
			formDTO = validateScheduleID(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("scheduleID", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("scheduleID", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateScheduleID(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, ContentManager.DATA_UNAVAILABLE);
		MasterValidator validation = new MasterValidator();
		String action = inputDTO.get("ACTION");
		String rectify = inputDTO.get("RECTIFY");
		String entityCode = inputDTO.get("ENTITY_CODE");
		String custLeaseCode = inputDTO.get("LESSEE_CODE");
		String agreementNo = inputDTO.get("AGREEMENT_NO");
		String scheduleID = inputDTO.get("SCHEDULE_ID");
		try {
			if (validation.isEmpty(custLeaseCode) && validation.isEmpty(agreementNo) && validation.isEmpty(scheduleID)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			if (!validation.isValidDescription(scheduleID, 10)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_LENGTH);
				return resultDTO;
			}
			if (!validation.isValidCode(scheduleID)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_VALUE);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH, ContentManager.FETCH_ALL);
			inputDTO.set(ContentManager.TABLE, "LEASE");
			String columns[] = new String[] { entityCode, custLeaseCode, agreementNo, scheduleID };
			inputDTO.setObject(ContentManager.PROGRAM_KEY, columns);
			resultDTO = validation.validatePK(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (action.equals(ADD) && rectify.equals(RegularConstants.COLUMN_DISABLE)) {
				if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_EXISTS);
					return resultDTO;
				}
			} else if (rectify.equals(RegularConstants.COLUMN_ENABLE)) {
				if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_NOT_EXISTS);
					return resultDTO;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validatePreLeaseRefDate() {
		CommonValidator validator = new CommonValidator();
		try {
			if (!validator.isEmpty(preLeaseRefDate)) {
				java.util.Date dateInstance = validator.isValidDate(preLeaseRefDate);
				if (dateInstance == null) {
					getErrorMap().setError("preLeaseRef", BackOfficeErrorCodes.INVALID_DATE);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("preLeaseRef", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return false;
	}

	public boolean validatePreLeaseRefSerial() {
		CommonValidator validator = new CommonValidator();
		try {
			if (!validator.isEmpty(preLeaseRefDate) && !validator.isEmpty(preLeaseRefSerial)) {
				if (!validator.isValidNumber(preLeaseRefSerial)) {
					getErrorMap().setError("preLeaseRef", BackOfficeErrorCodes.INVALID_NUMBER);
					return false;
				}
				DTObject formDTO = new DTObject();
				formDTO.set("LESSEE_CODE", custLeaseCode);
				formDTO.set("LEASE_PRODUCT_CODE", leaseProduct);
				formDTO.set("CUSTOMER_ID", custID);
				formDTO.set("CONTRACT_DATE", preLeaseRefDate);
				formDTO.set("CONTRACT_SL", preLeaseRefSerial);
				formDTO.set("GRID_LOAD_REQ", "0");
				formDTO.set(ContentManager.ACTION, USAGE);
				formDTO = validatePreLeaseRefSerial(formDTO);
				if (formDTO.get(ContentManager.ERROR) != null) {
					getErrorMap().setError("preLeaseRef", formDTO.get(ContentManager.ERROR));
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("preLeaseRef", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return false;
	}

	public DTObject validatePreLeaseRefSerial(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		LeaseValidator validation = new LeaseValidator();
		MasterValidator validator = new MasterValidator();
		String contractSl = inputDTO.get("CONTRACT_SL");
		String contractDate = inputDTO.get("CONTRACT_DATE");
		String custLeaseCode = inputDTO.get("LESSEE_CODE");
		String leaseProduct = inputDTO.get("LEASE_PRODUCT_CODE");
		String custID = inputDTO.get("CUSTOMER_ID");
		String gridLoadReq = inputDTO.get("GRID_LOAD_REQ");
		String branchCode = RegularConstants.EMPTY_STRING;
		String branchName = RegularConstants.EMPTY_STRING;
		try {
			if (validation.isEmpty(contractSl) || validation.isEmpty(contractDate)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			if (!validation.isValidNumber(contractSl)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_NUMBER);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "BRANCH_CODE,LEASE_PRODUCT_CODE,SUNDRY_DB_AC,CUSTOMER_ID,E_STATUS");
			resultDTO = validation.validatePreLeaseContractReferenceSerial(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			} else if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
				if (!"A".equals(resultDTO.get("E_STATUS")) && !custLeaseCode.equals(resultDTO.get("SUNDRY_DB_AC")) && !leaseProduct.equals(resultDTO.get("LEASE_PRODUCT_CODE")) && !custID.equals(resultDTO.get("CUSTOMER_ID"))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.PLC_STATUS_NOT_MATCHED);
					return resultDTO;
				}
				branchCode = resultDTO.get("BRANCH_CODE");
				resultDTO = validateContractDateSerial(inputDTO);
				if (resultDTO.get(ContentManager.ERROR) != null) {
					resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
					return resultDTO;
				} else if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
					if ("1".equals(gridLoadReq)) {
						inputDTO.set("BRANCH_CODE", branchCode);
						inputDTO.set(ContentManager.FETCH_COLUMNS, "BRANCH_NAME,STATE_CODE");
						resultDTO = validator.validateBranchCode(inputDTO);
						if (resultDTO.get(ContentManager.ERROR) != null) {
							resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
							return resultDTO;
						}
						if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
							resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
							return resultDTO;
						} else if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
							inputDTO.set("STATE_CODE", resultDTO.get("STATE_CODE"));
							branchName = resultDTO.get("BRANCH_NAME");
							resultDTO = loadAssetComponentGrid(inputDTO);
							if (resultDTO.get(ContentManager.ERROR) != null) {
								resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
								return resultDTO;
							} else {
								resultDTO.set("BRANCH_CODE", branchCode);
								resultDTO.set("BRANCH_DESC", branchName);
								if (!RegularConstants.EMPTY_STRING.equals(resultDTO.get("TAX_SCHEDULE_CODE"))) {
									resultDTO.set("TAX_SCHEDULE_CODE", resultDTO.get("TAX_SCHEDULE_CODE"));
									resultDTO.set("LOAD_COMPONENT_XML", resultDTO.get("XML_GRID"));
								}
							}
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
			validator.close();
		}
		return resultDTO;
	}

	public DTObject loadAssetComponentGrid(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		StringBuffer sqlQuery = new StringBuffer(RegularConstants.EMPTY_STRING);
		StringBuffer sqlQueryXml = new StringBuffer(RegularConstants.EMPTY_STRING);
		DBContext dbContext = new DBContext();
		DBUtil util2 = dbContext.createUtilInstance();
		DBUtil util1 = dbContext.createUtilInstance();
		int rowcnt = 0;
		String assetType = RegularConstants.EMPTY_STRING;
		String autoNnAuto = RegularConstants.EMPTY_STRING;
		Date contractDate = new java.sql.Date(BackOfficeFormatUtils.getDate(inputDTO.get("CONTRACT_DATE"), context.getDateFormat()).getTime());
		try {
			sqlQuery.append("SELECT LD.ASSET_TYPE_CODE,A.DESCRIPTION,DECODE_CMLOVREC('COMMON','AUTO_NON_AUTO',A.AUTO_NON_AUTO) AUTO_NON_AUTO_VAL,A.AUTO_NON_AUTO, (SELECT COUNT(1) FROM LEASEPOINVASSETS LL WHERE LL.ENTITY_CODE= ? AND LL.CONTRACT_DATE=? AND LL.CONTRACT_SL=? AND LL.ASSET_TYPE_CODE=LD.ASSET_TYPE_CODE ) COM_QTY ");
			sqlQuery.append("FROM LEASEPOINV L,LEASEPOINVASSETS LD,ASSETTYPE A WHERE L.ENTITY_CODE=LD.ENTITY_CODE AND L.CONTRACT_DATE=LD.CONTRACT_DATE AND L.CONTRACT_SL=LD.CONTRACT_SL AND LD.ENTITY_CODE=A.ENTITY_CODE AND LD.ASSET_TYPE_CODE=A.ASSET_TYPE_CODE ");
			sqlQuery.append("AND L.ENTITY_CODE= ? AND L.CONTRACT_DATE=? AND L.CONTRACT_SL=? GROUP BY LD.ASSET_TYPE_CODE");
			util2.reset();
			util2.setSql(sqlQuery.toString());
			util2.setString(1, context.getEntityCode());
			util2.setDate(2, (java.sql.Date) contractDate);
			util2.setString(3, inputDTO.get("CONTRACT_SL"));
			util2.setString(4, context.getEntityCode());
			util2.setDate(5, (java.sql.Date) contractDate);
			util2.setString(6, inputDTO.get("CONTRACT_SL"));
			ResultSet rset = util2.executeQuery();
			DHTMLXGridUtility dhtmlUtility = new DHTMLXGridUtility();
			dhtmlUtility.init();
			while (rset.next()) {
				int count = 0;
				rowcnt++;
				dhtmlUtility.startRow(String.valueOf(rowcnt));
				dhtmlUtility.setCell("");
				dhtmlUtility.setCell("<i title='Edit from Grid' id='removeAsset' onclick=eleaseAssets_editGrid(\"" + rowcnt + "\") data-toggle='tooltip' class='fa fa-edit' style='margin-right: 5px;font-size: 18px; color: #3da0e3; cursor: pointer'></i>  <i title='Remove from Grid' id='removeAsset' data-toggle='tooltip' onclick=deleteGridRow(\"" + rowcnt
						+ "\") class='fa fa-remove' style='margin-left: -3px;font-size: 21px; color: #3da0e3; cursor: pointer'></i> ");
				dhtmlUtility.setCell(String.valueOf(rowcnt));
				assetType = rset.getString("ASSET_TYPE_CODE");
				autoNnAuto = rset.getString("AUTO_NON_AUTO");
				dhtmlUtility.setCell(assetType);
				dhtmlUtility.setCell(rset.getString("DESCRIPTION"));
				dhtmlUtility.setCell(rset.getString("AUTO_NON_AUTO_VAL"));
				dhtmlUtility.setCell(rset.getString("COM_QTY"));

				sqlQueryXml = new StringBuffer(RegularConstants.EMPTY_STRING);
				sqlQueryXml.append("SELECT LD.ASSET_DESCRIPTION , LD.QUANTITY , LD.ASSET_MODEL, LD.ASSET_CONFIG ");
				sqlQueryXml.append(" FROM LEASEPOINVASSETS LD WHERE LD.ENTITY_CODE= ? AND LD.CONTRACT_DATE=? AND LD.CONTRACT_SL=? AND LD.ASSET_TYPE_CODE = ? ");
				util1.reset();
				util1.setSql(sqlQueryXml.toString());
				util1.setString(1, context.getEntityCode());
				util1.setDate(2, (java.sql.Date) contractDate);
				util1.setString(3, inputDTO.get("CONTRACT_SL"));
				util1.setString(4, assetType);
				ResultSet rs = util1.executeQuery();
				DHTMLXGridUtility componentXml = new DHTMLXGridUtility();
				componentXml.init();
				while (rs.next()) {
					count++;
					componentXml.startRow(String.valueOf(count));
					componentXml.setCell("");
					componentXml.setCell(String.valueOf(count));
					componentXml.setCell("");
					componentXml.setCell(rs.getString("ASSET_DESCRIPTION"));
					componentXml.setCell(rs.getString("QUANTITY"));
					componentXml.setCell("No");
					componentXml.setCell("0");
					componentXml.setCell(rs.getString("ASSET_MODEL"));
					componentXml.setCell(rs.getString("ASSET_CONFIG"));
					componentXml.endRow();
				}
				componentXml.finish();
				dhtmlUtility.setCell(componentXml.getXML().toString());
				dhtmlUtility.setCell(autoNnAuto);
				if (rowcnt == 1) {
					inputDTO.set("ASSET_TYPE_CODE", assetType);
					resultDTO = getTaxScheduleCode(inputDTO);
					if (resultDTO.get(ContentManager.ERROR) != null) {
						resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
						return resultDTO;
					}
					if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
						resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.SCHEDULE_CODE_NOT_CONFIGURED);
						return resultDTO;
					}
					taxScheduleCode = resultDTO.get("TAX_SCHEDULE_CODE");
				}
				dhtmlUtility.setCell(taxScheduleCode);
				dhtmlUtility.endRow();
			}
			dhtmlUtility.finish();
			if (rowcnt > 0) {
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				resultDTO.set("XML_GRID", dhtmlUtility.getXML());
				resultDTO.set("TAX_SCHEDULE_CODE", taxScheduleCode);
			} else {
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				// resultDTO.set(ContentManager.ERROR,
				// BackOfficeErrorCodes.PURCHASE_ORDER_NOT_RAISED);
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			util2.reset();
			util1.reset();
			dbContext.close();
		}
		return resultDTO;
	}

	public DTObject validateContractDateSerial(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		Date contractDate = new java.sql.Date(BackOfficeFormatUtils.getDate(inputDTO.get("CONTRACT_DATE"), context.getDateFormat()).getTime());
		try {
			String sqlQuery = "SELECT LESSEE_CODE,AGREEMENT_NO,SCHEDULE_ID FROM LEASE WHERE ENTITY_CODE = ? AND E_STATUS <> 'R' AND CONTRACT_DATE=? AND CONTRACT_SL=? ";
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, context.getEntityCode());
			util.setDate(2, (java.sql.Date) contractDate);
			util.setString(3, inputDTO.get("CONTRACT_SL"));
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				Object[] errorParam = { rset.getString("LESSEE_CODE") + RegularConstants.PK_SEPARATOR + rset.getString("AGREEMENT_NO") + RegularConstants.PK_SEPARATOR + rset.getString("SCHEDULE_ID") };
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.SCHEDULE_ALREADY_LINKED);
				resultDTO.setObject(ContentManager.ERROR_PARAM, errorParam);
				return resultDTO;
			}
			resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			util.reset();
			dbContext.close();
		}
		return resultDTO;
	}

	public boolean validateCreateDate() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(createDate)) {
				getErrorMap().setError("createDate", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			Date date = validation.isValidDate(createDate);
			if (date == null) {
				getErrorMap().setError("createDate", BackOfficeErrorCodes.INVALID_DATE);
				return false;
			}
			if (validation.isDateLesserCBD(createDate)) {
				getErrorMap().setError("createDate", BackOfficeErrorCodes.DATE_GECBD);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("createDate", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateLeaseComDate() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(leaseComDate)) {
				getErrorMap().setError("leaseComDate", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			Date date = validation.isValidDate(leaseComDate);
			if (date == null) {
				getErrorMap().setError("leaseComDate", BackOfficeErrorCodes.INVALID_DATE);
				return false;
			}
			if (validation.isDateLesser(leaseComDate, agreementDate)) {
				getErrorMap().setError("leaseComDate", BackOfficeErrorCodes.AGGREMENT_DATE_LESSER);
				return false;
			}
			if (validation.isDateGreaterThanCBD(leaseComDate)) {
				getErrorMap().setError("leaseComDate", BackOfficeErrorCodes.DATE_LECBD);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("leaseComDate", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;

	}

	public boolean validateBillAddress() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("CUSTOMER_ID", custID);
			formDTO.set("ADDR_SL", billAddress);
			formDTO.set(ContentManager.ACTION, USAGE);
			formDTO = validateBillAddress(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("billAddress", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("billAddress", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public boolean validateShipAddress() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("CUSTOMER_ID", custID);
			formDTO.set("ADDR_SL", shipAddress);
			formDTO.set(ContentManager.ACTION, USAGE);
			formDTO = validateBillAddress(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("shipAddress", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("shipAddress", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateBillAddress(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, ContentManager.DATA_UNAVAILABLE);
		RegistrationValidator validation = new RegistrationValidator();
		String custId = inputDTO.get("CUSTOMER_ID");
		String addSl = inputDTO.get("ADDR_SL");
		try {
			if (validation.isEmpty(custId) && validation.isEmpty(addSl)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			if (!validation.isValidNumber(addSl)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_NUMBER);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "STATE_CODE");
			resultDTO = validation.validateCustomerAddressSerial(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateBillBranch() {
		try {
			DTObject formDTO = getFormDTO();
			formDTO.set("BRANCH_CODE", billBranch);
			formDTO.set(ContentManager.USER_ACTION, USAGE);
			formDTO = validateBillBranch(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("billBranch", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("billBranch", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateBillBranch(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		MasterValidator validator = new MasterValidator();
		String billBranch = inputDTO.get("BRANCH_CODE");
		String billBranchName = RegularConstants.EMPTY_STRING;
		try {
			if (validator.isEmpty(billBranch)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "BRANCH_CODE,BRANCH_NAME,STATE_CODE");
			resultDTO = validator.validateBranchCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			} else if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
				billBranchName = resultDTO.get("BRANCH_NAME");
				billState = resultDTO.get("STATE_CODE");
				inputDTO.set("STATE_CODE", billState);
				inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION,INVOICE_TYPE");
				resultDTO = validator.validateStateCode(inputDTO);
				if (resultDTO.get(ContentManager.ERROR) != null) {
					resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
					return resultDTO;
				}
				if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
					return resultDTO;
				}
				resultDTO.set("BRANCH_NAME", billBranchName);
				resultDTO.set("STATE_CODE", billState);
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return resultDTO;
	}

	// TAB 1 END

	// TAB 2
	public boolean validateNoOfAssets() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(noOfAssets)) {
				getErrorMap().setError("noOfAssets", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidNumber(noOfAssets)) {
				getErrorMap().setError("noOfAssets", BackOfficeErrorCodes.INVALID_NUMBER);
				return false;
			}
			if (validation.isZero(noOfAssets)) {
				getErrorMap().setError("noOfAssets", BackOfficeErrorCodes.ZERO_CHECK);
				return false;
			}
			if (!validation.isValidPositiveNumber(noOfAssets)) {
				getErrorMap().setError("noOfAssets", BackOfficeErrorCodes.HMS_POSITIVE_CHECK);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("noOfAssets", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	// TAB 3

	public boolean validateTenor() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(tenor)) {
				getErrorMap().setError("tenor", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (validation.isZero(tenor)) {
				getErrorMap().setError("tenor", BackOfficeErrorCodes.ZERO_CHECK);
				return false;
			}
			if (!validation.isValidNumber(tenor)) {
				getErrorMap().setError("tenor", BackOfficeErrorCodes.INVALID_NUMBER);
				return false;
			}
			if (!validation.isValidPositiveNumber(tenor)) {
				getErrorMap().setError("tenor", BackOfficeErrorCodes.HMS_POSITIVE_CHECK);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("tenor", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateCancelPeriod() {
		CommonValidator validation = new CommonValidator();
		try {
			if (!validation.isEmpty(cancelPeriod)) {
				if (validation.isZero(cancelPeriod)) {
					getErrorMap().setError("cancelPeriod", BackOfficeErrorCodes.ZERO_CHECK);
					return false;
				}
				if (!validation.isValidNumber(cancelPeriod)) {
					getErrorMap().setError("cancelPeriod", BackOfficeErrorCodes.INVALID_NUMBER);
					return false;
				}
				if (!validation.isValidPositiveNumber(cancelPeriod)) {
					getErrorMap().setError("cancelPeriod", BackOfficeErrorCodes.HMS_POSITIVE_CHECK);
					return false;
				}
				if (Integer.parseInt(cancelPeriod) > Integer.parseInt(tenor)) {
					getErrorMap().setError("cancelPeriod", BackOfficeErrorCodes.CANCEL_TENOR_CANT_GREATER_TENOR);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("cancelPeriod", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateRepayFreq() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("TENOR", tenor);
			formDTO.set("REPAY_FREQUENCY", repayFreq);
			formDTO = validateTenorFrequency(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("repayFreq", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("repayFreq", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateTenorFrequency(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		MasterValidator validation = new MasterValidator();
		int tenor = Integer.parseInt(inputDTO.get("TENOR"));
		String repayFreq = inputDTO.get("REPAY_FREQUENCY");
		int monthCheck = 0;
		try {
			if (validation.isEmpty(repayFreq) || validation.isEmpty(String.valueOf(tenor))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			if (!validation.isCMLOVREC("COMMON", "REPAY_FREQUENCY", repayFreq)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_OPTION);
				return resultDTO;
			}

			if ("M".equals(repayFreq))
				monthCheck = 1;
			else if ("Q".equals(repayFreq))
				monthCheck = 3;
			else if ("H".equals(repayFreq))
				monthCheck = 6;
			else if ("Y".equals(repayFreq))
				monthCheck = 12;

			if (tenor % monthCheck != 0) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.MONTH_REQ_SUMOF_ZERO);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateAdvArrers() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(advArrers)) {
				getErrorMap().setError("advArrers", BackOfficeErrorCodes.HMS_MANDATORY);
				return false;
			}
			if (!validation.isCMLOVREC("COMMON", "ADVANCE_ARREARS", advArrers)) {
				getErrorMap().setError("advArrers", BackOfficeErrorCodes.HMS_INVALID_OPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("advArrers", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateFirstPaymet() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(firstPaymet)) {
				getErrorMap().setError("firstPaymet", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isEmpty(firstPaymet)) {
				Date date = validation.isValidDate(firstPaymet);
				if (date == null) {
					getErrorMap().setError("firstPaymet", BackOfficeErrorCodes.INVALID_DATE);
					return false;
				}
			}
			if (validation.isDateLesser(firstPaymet, leaseComDate)) {
				getErrorMap().setError("firstPaymet", BackOfficeErrorCodes.LEASE_COMMENCEMENT_DATE_GREATER);
				return false;
			}
			if (validation.isDateLesserCBD(firstPaymet)) {
				getErrorMap().setError("firstPaymet", BackOfficeErrorCodes.DATE_GECBD);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("firstPaymet", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateIntFromDate() {
		CommonValidator validation = new CommonValidator();
		try {
			if (!validation.isEmpty(intFromDate)) {
				Date date = validation.isValidDate(intFromDate);
				if (date == null) {
					getErrorMap().setError("intFromDate", BackOfficeErrorCodes.INVALID_DATE);
					return false;
				}
				if (validation.isDateLesser(intFromDate, leaseComDate)) {
					getErrorMap().setError("intFromDate", BackOfficeErrorCodes.LEASE_COMMENCEMENT_DATE_GREATER);
					return false;
				}
				if (validation.isDateGreaterThanEqual(intFromDate, firstPaymet)) {
					getErrorMap().setError("intFromDate", BackOfficeErrorCodes.PAYMENT_DATE_LESS_THAN);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			validation.close();
		}
	}

	public boolean validateIntUptoDate() {
		CommonValidator validation = new CommonValidator();
		try {
			if (!validation.isEmpty(intFromDate)) {
				if (validation.isEmpty(intUptoDate)) {
					getErrorMap().setError("intUptoDate", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!validation.isEmpty(intUptoDate)) {
					Date date = validation.isValidDate(intUptoDate);
					if (date == null) {
						getErrorMap().setError("intUptoDate", BackOfficeErrorCodes.INVALID_DATE);
						return false;
					}
				}
				if (validation.isDateLesser(intUptoDate, intFromDate)) {
					getErrorMap().setError("intUptoDate", BackOfficeErrorCodes.DATE_GREATER_THAN_INITIAL_DATE);
					return false;
				}
				if (validation.isDateGreaterThanEqual(intUptoDate, firstPaymet)) {
					getErrorMap().setError("intUptoDate", BackOfficeErrorCodes.PAYMENT_DATE_LESS_THAN);
					return false;
				}
			} else if (!validation.isEmpty(intUptoDate)) {
				if (validation.isEmpty(intFromDate)) {
					getErrorMap().setError("intFromDate", BackOfficeErrorCodes.INITIAL_DATE_MANDATORY);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			validation.close();
		}
	}

	public boolean validateFinalFromDate() {
		CommonValidator validation = new CommonValidator();
		try {
			if (!validation.isEmpty(finalFromDate)) {
				Date date = validation.isValidDate(finalFromDate);
				if (date == null) {
					getErrorMap().setError("finalFromDate", BackOfficeErrorCodes.INVALID_DATE);
					return false;
				}
				if (BackOfficeDateUtils.isDateLesserThanEqual(finalFromDate, lastDate)) {
					getErrorMap().setError("finalFromDate", BackOfficeErrorCodes.DATE_GREATER_THAN_BILLING_DATE);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			validation.close();
		}
	}

	public boolean validateFinalUptoDate() {
		CommonValidator validation = new CommonValidator();
		try {
			if (!validation.isEmpty(finalFromDate)) {
				if (validation.isEmpty(finalUptoDate)) {
					getErrorMap().setError("finalUptoDate", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!validation.isEmpty(finalUptoDate)) {
					Date date = validation.isValidDate(finalUptoDate);
					if (date == null) {
						getErrorMap().setError("finalUptoDate", BackOfficeErrorCodes.INVALID_DATE);
						return false;
					}
				}
				if (validation.isDateLesser(finalUptoDate, finalFromDate)) {
					getErrorMap().setError("finalUptoDate", BackOfficeErrorCodes.DATE_GREATER_THAN_FINAL_DATE);
					return false;
				}
			} else if (!validation.isEmpty(finalUptoDate)) {
				if (validation.isEmpty(finalFromDate)) {
					getErrorMap().setError("finalFromDate", BackOfficeErrorCodes.FINAL_DATE_MANDATORY);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			validation.close();
		}
	}

	public boolean validateRentDate() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(rentDate)) {
				getErrorMap().setError("rentDate", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isEmpty(rentDate)) {
				Date date = validation.isValidDate(rentDate);
				if (date == null) {
					getErrorMap().setError("rentDate", BackOfficeErrorCodes.INVALID_DATE);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			validation.close();
		}
	}

	public boolean validateLastDate() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(lastDate)) {
				getErrorMap().setError("lastDate", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isEmpty(lastDate)) {
				Date date = validation.isValidDate(lastDate);
				if (date == null) {
					getErrorMap().setError("lastDate", BackOfficeErrorCodes.INVALID_DATE);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			validation.close();
		}
	}

	// TAB 7 BEGIN

	public boolean validateGrossNetTDS() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(grossNetTDS)) {
				getErrorMap().setError("grossNetTDS", BackOfficeErrorCodes.HMS_MANDATORY);
				return false;
			}
			if (!validation.isCMLOVREC("ELEASE", "TDS_TYPE", grossNetTDS)) {
				getErrorMap().setError("grossNetTDS", BackOfficeErrorCodes.HMS_INVALID_OPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("grossNetTDS", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	// TAB 7 PAYMENT GRID
	public boolean validatePaymentGrid() {
		try {
			DTDObject leasePayment = new DTDObject();
			leasePayment.addColumn(0, "SL");
			leasePayment.addColumn(1, "LINK");
			leasePayment.addColumn(2, "DTL_SL");
			leasePayment.addColumn(3, "UPTO_TENOR");
			leasePayment.addColumn(4, "REPAY_MODE_DESC");
			leasePayment.addColumn(5, "ECS_DB_IFSC");
			leasePayment.addColumn(6, "ECS_DB_BANK");
			leasePayment.addColumn(7, "ECS_DB_BRANCH");
			leasePayment.addColumn(8, "ECS_DB_AC_NO");
			leasePayment.addColumn(9, "ECS_DB_PEAK_AMOUNT_FORMAT");
			leasePayment.addColumn(10, "ECS_MANDATE_REFNO");
			leasePayment.addColumn(11, "ECS_MANDATE_DATE");
			leasePayment.addColumn(12, "REPAY_MODE");
			leasePayment.addColumn(13, "ECS_DB_PEAK_AMOUNT");
			leasePayment.setXML(xmlleasePaymentGrid);
			if (leasePayment.getRowCount() <= 0) {
				getErrorMap().setError("paymentErrors", BackOfficeErrorCodes.HMS_ATLEAST_ONE_ROW);
				return false;
			}
			for (int i = 0; i < leasePayment.getRowCount(); i++) {
				if (!validatePaymentUptoTenor(leasePayment.getValue(i, 3))) {
					getErrorMap().setError("paymentErrors", BackOfficeErrorCodes.HMS_INVALID_NUMBER);
					return false;
				}
				if (!validateRepaymentMode(leasePayment.getValue(i, 12))) {
					getErrorMap().setError("paymentErrors", BackOfficeErrorCodes.HMS_INVALID_ACTION);
					return false;
				}
				if (!validateEcsIFSC(leasePayment.getValue(i, 5), leasePayment.getValue(i, 12))) {
					getErrorMap().setError("paymentErrors", BackOfficeErrorCodes.HMS_INVALID_IFSC_CODE);
					return false;
				}
				if (!validateEcsBank(leasePayment.getValue(i, 6), leasePayment.getValue(i, 12))) {
					getErrorMap().setError("paymentErrors", BackOfficeErrorCodes.HMS_INVALID_IFSC_CODE);
					return false;
				}
				if (!validateEcsBranch(leasePayment.getValue(i, 7), leasePayment.getValue(i, 12))) {
					getErrorMap().setError("paymentErrors", BackOfficeErrorCodes.HMS_INVALID_ACCOUNT);
					return false;
				}
				if (!validateCusAccNumber(leasePayment.getValue(i, 8), leasePayment.getValue(i, 12))) {
					getErrorMap().setError("paymentErrors", BackOfficeErrorCodes.HMS_INVALID_ACCOUNT);
					return false;
				}
				if (!validatePeakAmountFormat(leasePayment.getValue(i, 13), leasePayment.getValue(i, 12))) {
					getErrorMap().setError("paymentErrors", BackOfficeErrorCodes.HMS_INVALID_AMOUNT);
					return false;
				}
				if (!validateEcsRefNo(leasePayment.getValue(i, 10), leasePayment.getValue(i, 12))) {
					getErrorMap().setError("paymentErrors", BackOfficeErrorCodes.HMS_INVALID_REFERENCE);
					return false;
				}
				if (!validateEcsRefDate(leasePayment.getValue(i, 11), leasePayment.getValue(i, 12))) {
					getErrorMap().setError("paymentErrors", BackOfficeErrorCodes.HMS_INVALID_DATE);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("paymentErrors", BackOfficeErrorCodes.ERROR_IN_GRID);
		}
		return false;
	}

	public boolean validatePaymentUptoTenor(String paymentUptoTenor) {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(paymentUptoTenor)) {
				getErrorMap().setError("paymentUptoTenor", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (validation.isZero(paymentUptoTenor)) {
				getErrorMap().setError("paymentUptoTenor", BackOfficeErrorCodes.ZERO_CHECK);
				return false;
			}
			if (!validation.isValidNumber(paymentUptoTenor)) {
				getErrorMap().setError("paymentUptoTenor", BackOfficeErrorCodes.INVALID_NUMBER);
				return false;
			}
			if (!validation.isValidPositiveNumber(paymentUptoTenor)) {
				getErrorMap().setError("paymentUptoTenor", BackOfficeErrorCodes.HMS_POSITIVE_CHECK);
				return false;
			}
			if (validation.isNumberGreater(paymentUptoTenor, tenor)) {
				getErrorMap().setError("paymentUptoTenor", BackOfficeErrorCodes.TENOR_GREATER_THAN_TOTAL_TENOR);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("paymentUptoTenor", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateRepaymentMode(String repaymentMode) {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(repaymentMode)) {
				getErrorMap().setError("repaymentMode", BackOfficeErrorCodes.HMS_MANDATORY);
				return false;
			}
			if (!validation.isCMLOVREC("ELEASE", "REPAY_MODE", repaymentMode)) {
				getErrorMap().setError("repaymentMode", BackOfficeErrorCodes.HMS_INVALID_OPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("repaymentMode", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateEcsIFSC(String ecsIFSC, String repaymentMode) {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("IFSC_CODE", ecsIFSC);
			formDTO.set("REPAY_MODE", repaymentMode);
			formDTO.set(ContentManager.ACTION, USAGE);
			formDTO = validateIFSCCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("ecsIFSC", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("ecsIFSC", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateIFSCCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		MasterValidator validation = new MasterValidator();
		String ifscCode = inputDTO.get("IFSC_CODE");
		String repaymentMode = inputDTO.get("REPAY_MODE");
		try {
			if (repaymentMode.equals("ECS") || repaymentMode.equals("AD")) {
				if (!validation.isEmpty(ifscCode)) {
					inputDTO.set(ContentManager.FETCH_COLUMNS, "BANK_NAME,BRANCH_NAME");
					resultDTO = validation.validateIFSCCode(inputDTO);
					if (resultDTO.get(ContentManager.ERROR) != null) {
						resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
						return resultDTO;
					}
					if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
						resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
						return resultDTO;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateEcsBank(String ecsBank, String repaymentMode) {
		CommonValidator validation = new CommonValidator();
		try {
			if (repaymentMode.equals("ECS") || repaymentMode.equals("AD")) {
				if (validation.isEmpty(ecsBank)) {
					getErrorMap().setError("ecsBank", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!validation.isValidName(ecsBank)) {
					getErrorMap().setError("ecsBank", BackOfficeErrorCodes.INVALID_NAME);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("ecsBank", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateEcsBranch(String ecsBranch, String repaymentMode) {
		CommonValidator validation = new CommonValidator();
		try {
			if (repaymentMode.equals("ECS") || repaymentMode.equals("AD")) {
				if (validation.isEmpty(ecsBranch)) {
					getErrorMap().setError("ecsBranch", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!validation.isValidName(ecsBranch)) {
					getErrorMap().setError("ecsBranch", BackOfficeErrorCodes.INVALID_NAME);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("ecsBranch", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateCusAccNumber(String cusAccNumber, String repaymentMode) {
		CommonValidator validation = new CommonValidator();
		try {
			if (repaymentMode.equals("ECS") || repaymentMode.equals("AD")) {
				if (validation.isEmpty(cusAccNumber)) {
					getErrorMap().setError("cusAccNumber", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!validation.isValidAccountNumber(cusAccNumber)) {
					getErrorMap().setError("cusAccNumber", BackOfficeErrorCodes.INVALID_IFSC_CODE);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("cusAccNumber", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validatePeakAmountFormat(String peakAmount, String repaymentMode) {
		CommonValidator validation = new CommonValidator();
		DTObject formDTO = new DTObject();
		try {
			if (repaymentMode.equals("ECS")) {
				if (validation.isEmpty(peakAmount)) {
					getErrorMap().setError("peakAmountFormat", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (validation.isZeroAmount(peakAmount)) {
					getErrorMap().setError("peakAmountFormat", BackOfficeErrorCodes.HMS_ZERO_AMOUNT);
					return false;
				}
				formDTO.set("CURRENCY_CODE", assetCurrency);
				formDTO.set(ProgramConstants.AMOUNT, peakAmount);
				formDTO.set(ContentManager.ACTION, USAGE);
				formDTO = validation.validateCurrencySmallAmount(formDTO);
				if (formDTO.get(ContentManager.ERROR) != null) {
					getErrorMap().setError("peakAmountFormat", formDTO.get(ContentManager.ERROR));
					return false;
				}
				if (validation.isValidNegativeAmount(peakAmount)) {
					getErrorMap().setError("peakAmountFormat", BackOfficeErrorCodes.HMS_POSITIVE_AMOUNT);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("peakAmountFormat", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateEcsRefNo(String ecsRefNo, String repaymentMode) {
		CommonValidator Validator = new CommonValidator();
		try {
			if (repaymentMode.equals("ECS")) {
				if (Validator.isEmpty(ecsRefNo)) {
					getErrorMap().setError("ecsRefNo", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!Validator.isValidOtherInformation25(ecsRefNo)) {
					getErrorMap().setError("ecsRefNo", BackOfficeErrorCodes.HMS_INVALID_REFERENCE);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("ecsRefNo", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			Validator.close();
		}
		return false;
	}

	public boolean validateEcsRefDate(String ecsRefDate, String repaymentMode) {
		CommonValidator validation = new CommonValidator();
		try {
			if (repaymentMode.equals("ECS")) {
				if (validation.isEmpty(ecsRefDate)) {
					getErrorMap().setError("ecsRefDate", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!validation.isEmpty(ecsRefDate)) {
					Date date = validation.isValidDate(ecsRefDate);
					if (date == null) {
						getErrorMap().setError("ecsRefDate", BackOfficeErrorCodes.INVALID_DATE);
						return false;
					}
				}
				// if (BackOfficeDateUtils.isDateGreaterThan(ecsRefDate,
				// firstPaymet)) {
				// getErrorMap().setError("ecsRefDate",
				// BackOfficeErrorCodes.BILL_START_DATE_LESSER);
				// return false;
				// }
				if (!validation.isDateGreaterThanEqualCBD(ecsRefDate)) {
					getErrorMap().setError("ecsRefDate", BackOfficeErrorCodes.DATE_GECBD);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("ecsRefDate", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	// TAB 7 PAYMENT GRID FINISHED

	public boolean validateProcessCode() {
		try {
			DTObject formDTO = getFormDTO();
			formDTO.set("BANK_PROCESS_CODE", processCode);
			formDTO.set(ContentManager.ACTION, USAGE);
			formDTO = validateProcessCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("processCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("processCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateProcessCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		MasterValidator validation = new MasterValidator();
		String processCode = inputDTO.get("BANK_PROCESS_CODE");
		try {
			if (validation.isEmpty(processCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "NAME");
			resultDTO = validation.validateBankingProcessCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateInvTempCode() {
		try {
			DTObject formDTO = getFormDTO();
			formDTO.set("INVTEMPLATE_CODE", invTempCode);
			formDTO.set(ContentManager.ACTION, USAGE);
			formDTO = validateInvTempCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("invTempCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("invTempCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateInvTempCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		InventoryValidator validation = new InventoryValidator();
		String invTempCode = inputDTO.get("INVTEMPLATE_CODE");
		try {
			if (validation.isEmpty(invTempCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
			resultDTO = validation.ValidateInvTemplateCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateFamDesc() {
		CommonValidator validation = new CommonValidator();
		try {
			if (!validation.isEmpty(famDesc)) {
				if (!validation.isValidOtherInformation25(famDesc)) {
					getErrorMap().setError("famDesc", BackOfficeErrorCodes.HMS_INVALID_VALUE);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("famDesc", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateInvCycleNo() {
		try {
			DTObject formDTO = getFormDTO();
			formDTO.set("INV_CYCLE_NUMBER", invCycleNo);
			formDTO.set(ContentManager.ACTION, USAGE);
			formDTO = validateInvoiceCycleNumber(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("invCycleNo", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("invCycleNo", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public boolean validateInitCycleNo() {
		CommonValidator validation = new CommonValidator();
		try {
			if (!validation.isEmpty(intFromDate) && !validation.isEmpty(intUptoDate)) {
				if (validation.isEmpty(initCycleNo)) {
					getErrorMap().setError("initCycleNo", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
			}
			if (!validation.isEmpty(initCycleNo)) {
				DTObject formDTO = getFormDTO();
				formDTO.set("INV_CYCLE_NUMBER", initCycleNo);
				formDTO.set(ContentManager.ACTION, USAGE);
				formDTO = validateInvoiceCycleNumber(formDTO);
				if (formDTO.get(ContentManager.ERROR) != null) {
					getErrorMap().setError("initCycleNo", formDTO.get(ContentManager.ERROR));
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("initCycleNo", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateFinalCycleNo() {
		CommonValidator validation = new CommonValidator();
		try {
			if (!validation.isEmpty(finalFromDate) && !validation.isEmpty(finalUptoDate)) {
				if (validation.isEmpty(finalCycleNo)) {
					getErrorMap().setError("finalCycleNo", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
			}
			if (!validation.isEmpty(finalCycleNo)) {
				DTObject formDTO = getFormDTO();
				formDTO.set("INV_CYCLE_NUMBER", finalCycleNo);
				formDTO.set(ContentManager.ACTION, USAGE);
				formDTO = validateInvoiceCycleNumber(formDTO);
				if (formDTO.get(ContentManager.ERROR) != null) {
					getErrorMap().setError("finalCycleNo", formDTO.get(ContentManager.ERROR));
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("finalCycleNo", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public DTObject validateInvoiceCycleNumber(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		InvoiceValidator validation = new InvoiceValidator();
		String invoiceCycleNumber = inputDTO.get("INV_CYCLE_NUMBER");
		try {
			if (validation.isEmpty(invoiceCycleNumber)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			if (!validation.isValidPositiveNumber(invoiceCycleNumber)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_POSITIVE_CHECK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "INV_CYCLE_NUMBER");
			resultDTO = validation.validateInvoiceCycleNumber(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validatehierarCode() {
		try {
			DTObject formDTO = getFormDTO();
			formDTO.set("LAM_HIER_CODE", hierarCode);
			formDTO.set(ContentManager.ACTION, USAGE);
			formDTO = validatehierarCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("hierarCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("hierarCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validatehierarCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		CommonValidator validation = new CommonValidator();
		String hierarCode = inputDTO.get("LAM_HIER_CODE");
		try {
			if (validation.isEmpty(hierarCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
			resultDTO = validation.validateHierarchyCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateRemarks() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (getAction().equals(ADD) && remarks != null && remarks.length() > 0) {
				if (commonValidator.isEmpty(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			} else if (getAction().equals(MODIFY)) {
				if (commonValidator.isEmpty(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}

	// TAB 4

	public boolean validateGrossAssetCostFormat() {
		CommonValidator validation = new CommonValidator();
		DTObject formDTO = new DTObject();
		try {
			if (validation.isEmpty(grossAssetCost)) {
				getErrorMap().setError("grossAssetCostFormat", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (validation.isZeroAmount(grossAssetCost)) {
				getErrorMap().setError("grossAssetCostFormat", BackOfficeErrorCodes.HMS_ZERO_AMOUNT);
				return false;
			}
			if (validation.isValidNegativeAmount(grossAssetCost)) {
				getErrorMap().setError("grossAssetCostFormat", BackOfficeErrorCodes.HMS_POSITIVE_AMOUNT);
				return false;
			}
			formDTO.set("CURRENCY_CODE", assetCurrency);
			formDTO.set(ProgramConstants.AMOUNT, grossAssetCost);
			formDTO.set(ContentManager.ACTION, USAGE);
			formDTO = validation.validateCurrencySmallAmount(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("grossAssetCostFormat", formDTO.get(ContentManager.ERROR));
				return false;
			}
			if (!validation.isEmpty(preLeaseRefSerial) && !validation.isEmpty(preLeaseRefDate)) {
				formDTO.reset();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.set("CONTRACT_SL", preLeaseRefSerial);
				formDTO.set("CONTRACT_DATE", preLeaseRefDate);
				formDTO.set("GROSS_ASSET_COST", grossAssetCost);
				formDTO.set(ContentManager.ACTION, USAGE);
				formDTO = validateGrossAssetCost(formDTO);
				if (formDTO.get(ContentManager.ERROR) != null) {
					getErrorMap().setError("grossAssetCostFormat", formDTO.get(ContentManager.ERROR));
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("grossAssetCostFormat", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public DTObject validateGrossAssetCost(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		LeaseValidator validation = new LeaseValidator();
		String contractSl = inputDTO.get("CONTRACT_SL");
		String contractDate = inputDTO.get("CONTRACT_DATE");
		String grossCode = inputDTO.get("GROSS_ASSET_COST");
		Date date = new java.sql.Date(BackOfficeFormatUtils.getDate(contractDate, context.getDateFormat()).getTime());
		double sumOfLleasePO = 0.00;
		try {
			if (validation.isEmpty(contractSl) || validation.isEmpty(contractDate) || validation.isEmpty(grossCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH, ContentManager.FETCH_ALL);
			resultDTO = validation.validatePreLeaseContractReferenceSerial(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			} else if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
				String sqlQuery = "SELECT SUM(TOTAL_PO_AMOUNT)TOTAL_PO_AMOUNT FROM LEASEPO WHERE ENTITY_CODE=? AND CONTRACT_DATE=? AND CONTRACT_SL=? AND E_STATUS='A' ";
				util.reset();
				util.setSql(sqlQuery);
				util.setString(1, context.getEntityCode());
				util.setDate(2, (java.sql.Date) date);
				util.setString(3, contractSl);
				ResultSet rset = util.executeQuery();
				if (rset.next()) {
					resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
					sumOfLleasePO = Double.parseDouble(rset.getString("TOTAL_PO_AMOUNT"));
				}
				DecimalFormat df = new DecimalFormat("#.##");
				sumOfLleasePO = Double.valueOf(df.format(sumOfLleasePO));
				if (validation.isAmountGreater(grossCode, String.valueOf(sumOfLleasePO))) {
					Object[] errorParam = { sumOfLleasePO };
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.GROSS_AMOUNT_CANT_GREATER_PO_AMT);
					resultDTO.setObject(ContentManager.ERROR_PARAM, errorParam);
					return resultDTO;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
			util.reset();
			dbContext.close();
		}
		return resultDTO;
	}

	public boolean validateCommencementValueFormat() {
		CommonValidator validation = new CommonValidator();
		DTObject formDTO = new DTObject();
		try {
			if (validation.isEmpty(commencementValue)) {
				getErrorMap().setError("commencementValueFormat", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (validation.isZeroAmount(commencementValue)) {
				getErrorMap().setError("commencementValueFormat", BackOfficeErrorCodes.HMS_ZERO_AMOUNT);
				return false;
			}
			formDTO.set("CURRENCY_CODE", assetCurrency);
			formDTO.set(ProgramConstants.AMOUNT, commencementValue);
			formDTO.set(ContentManager.ACTION, USAGE);
			formDTO = validation.validateCurrencySmallAmount(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("commencementValueFormat", formDTO.get(ContentManager.ERROR));
				return false;
			}
			if (validation.isValidNegativeAmount(commencementValue)) {
				getErrorMap().setError("commencementValueFormat", BackOfficeErrorCodes.HMS_POSITIVE_AMOUNT);
				return false;
			}
			if (validation.isAmountGreater(commencementValue, grossAssetCost)) {
				getErrorMap().setError("commencementValueFormat", BackOfficeErrorCodes.COMMENCEMENT_CANT_GREATER_GROSS_AMOUNT);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("commencementValueFormat", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateLeaseRentOption() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(leaseRentOption)) {
				getErrorMap().setError("leaseRentOption", BackOfficeErrorCodes.HMS_MANDATORY);
				return false;
			}
			if (!validation.isCMLOVREC("COMMON", "RENT_PERIOD_OPTION", leaseRentOption)) {
				getErrorMap().setError("leaseRentOption", BackOfficeErrorCodes.HMS_INVALID_OPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("leaseRentOption", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateAssureRVPer() {
		CommonValidator validation = new CommonValidator();
		try {
			if (!validation.isEmpty(assureRVPer)) {
				if (validation.isZero(assureRVPer)) {
					getErrorMap().setError("assureRVPer", BackOfficeErrorCodes.ZERO_CHECK);
					return false;
				}
				if (!validation.isThreeDigitTwoDecimal(assureRVPer, 2)) {
					getErrorMap().setError("assureRVPer", BackOfficeErrorCodes.HMS_INVALID_THREEDIGIT_TWODEC);
					return false;
				}
				if (validation.isValidNegativeAmount(assureRVPer)) {
					getErrorMap().setError("assureRVPer", BackOfficeErrorCodes.HMS_POSITIVE_CHECK);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("assureRVPer", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateAssureRVAmountFormat() {
		CommonValidator validation = new CommonValidator();
		DTObject formDTO = new DTObject();
		try {
			if (validation.isEmpty(assureRVPer) && validation.isEmpty(assureRVAmount)) {
				getErrorMap().setError("assureRVAmountFormat", BackOfficeErrorCodes.RV_PER_AMOUNT_MANDATORY);
				return false;
			}
			if (!validation.isEmpty(assureRVAmount)) {
				if (validation.isZeroAmount(assureRVAmount)) {
					getErrorMap().setError("assureRVAmountFormat", BackOfficeErrorCodes.HMS_ZERO_AMOUNT);
					return false;
				}
				formDTO.set("CURRENCY_CODE", assetCurrency);
				formDTO.set(ProgramConstants.AMOUNT, assureRVAmount);
				formDTO.set(ContentManager.ACTION, USAGE);
				formDTO = validation.validateCurrencySmallAmount(formDTO);
				if (formDTO.get(ContentManager.ERROR) != null) {
					getErrorMap().setError("assureRVAmountFormat", formDTO.get(ContentManager.ERROR));
					return false;
				}
				if (validation.isValidNegativeAmount(assureRVAmount)) {
					getErrorMap().setError("assureRVAmountFormat", BackOfficeErrorCodes.HMS_POSITIVE_AMOUNT);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("assureRVAmountFormat", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateInternalRateReturn() {
		CommonValidator validation = new CommonValidator();
		try {
			if (!"O".equals(leaseProductType)) {
				if (validation.isEmpty(internalRateReturn)) {
					getErrorMap().setError("internalRateReturn", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (validation.isZero(internalRateReturn)) {
					getErrorMap().setError("internalRateReturn", BackOfficeErrorCodes.ZERO_CHECK);
					return false;
				}
				if (!validation.isThreeDigitTwoDecimal(internalRateReturn, 2)) {
					getErrorMap().setError("internalRateReturn", BackOfficeErrorCodes.HMS_INVALID_THREEDIGIT_TWODEC);
					return false;
				}
				if (validation.isValidNegativeAmount(internalRateReturn)) {
					getErrorMap().setError("internalRateReturn", BackOfficeErrorCodes.HMS_POSITIVE_CHECK);
					return false;
				}
			} else {
				if (!validation.isEmpty(internalRateReturn)) {
					getErrorMap().setError("internalRateReturn", BackOfficeErrorCodes.ONLY_ALLOWED_OPERATING_LEASE);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("internalRateReturn", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateIntBrokenRentFormat() {
		CommonValidator validation = new CommonValidator();
		DTObject formDTO = new DTObject();
		try {
			if (!validation.isEmpty(intFromDate) && !validation.isEmpty(intUptoDate)) {
				if (!validation.isEmpty(intBrokenRent)) {
					formDTO.set("CURRENCY_CODE", assetCurrency);
					formDTO.set(ProgramConstants.AMOUNT, intBrokenRent);
					formDTO.set(ContentManager.ACTION, USAGE);
					formDTO = validation.validateCurrencySmallAmount(formDTO);
					if (formDTO.get(ContentManager.ERROR) != null) {
						getErrorMap().setError("intBrokenRentFormat", formDTO.get(ContentManager.ERROR));
						return false;
					}
					if (validation.isValidNegativeAmount(intBrokenRent)) {
						getErrorMap().setError("intBrokenRentFormat", BackOfficeErrorCodes.HMS_POSITIVE_AMOUNT);
						return false;
					}
				}
			} else if (!validation.isEmpty(intBrokenRent)) {
				getErrorMap().setError("intBrokenRentFormat", BackOfficeErrorCodes.MANDATORY_INITIAL_FROM_DATE);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("intBrokenRentFormat", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateIintBrokenExecFormat() {
		CommonValidator validation = new CommonValidator();
		DTObject formDTO = new DTObject();
		try {
			if (!validation.isEmpty(intFromDate) && !validation.isEmpty(intUptoDate)) {
				if (!validation.isEmpty(intBrokenExec)) {
					formDTO.set("CURRENCY_CODE", assetCurrency);
					formDTO.set(ProgramConstants.AMOUNT, intBrokenExec);
					formDTO.set(ContentManager.ACTION, USAGE);
					formDTO = validation.validateCurrencySmallAmount(formDTO);
					if (formDTO.get(ContentManager.ERROR) != null) {
						getErrorMap().setError("intBrokenExecFormat", formDTO.get(ContentManager.ERROR));
						return false;
					}
					if (validation.isValidNegativeAmount(intBrokenExec)) {
						getErrorMap().setError("intBrokenExecFormat", BackOfficeErrorCodes.HMS_POSITIVE_AMOUNT);
						return false;
					}
				}
			} else if (!validation.isEmpty(intBrokenExec)) {
				getErrorMap().setError("intBrokenExecFormat", BackOfficeErrorCodes.MANDATORY_INITIAL_FROM_DATE);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("intBrokenExecFormat", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateFinalBrokenRentFormat() {
		CommonValidator validation = new CommonValidator();
		DTObject formDTO = new DTObject();
		try {
			if (!validation.isEmpty(finalFromDate) && !validation.isEmpty(finalUptoDate)) {
				if (!validation.isEmpty(finalBrokenRent)) {
					formDTO.set("CURRENCY_CODE", assetCurrency);
					formDTO.set(ProgramConstants.AMOUNT, finalBrokenRent);
					formDTO.set(ContentManager.ACTION, USAGE);
					formDTO = validation.validateCurrencySmallAmount(formDTO);
					if (formDTO.get(ContentManager.ERROR) != null) {
						getErrorMap().setError("finalBrokenRentFormat", formDTO.get(ContentManager.ERROR));
						return false;
					}
					if (validation.isValidNegativeAmount(finalBrokenRent)) {
						getErrorMap().setError("finalBrokenRentFormat", BackOfficeErrorCodes.HMS_POSITIVE_AMOUNT);
						return false;
					}
				}
			} else if (!validation.isEmpty(finalBrokenRent)) {
				getErrorMap().setError("finalBrokenRentFormat", BackOfficeErrorCodes.MANDATORY_FINAL_FROM_DATE);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("finalBrokenRentFormat", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateFinaBrokenExecFormat() {
		CommonValidator validation = new CommonValidator();
		DTObject formDTO = new DTObject();
		try {
			if (!validation.isEmpty(finalFromDate) && !validation.isEmpty(finalUptoDate)) {
				if (!validation.isEmpty(finaBrokenExec)) {
					formDTO.set("CURRENCY_CODE", assetCurrency);
					formDTO.set(ProgramConstants.AMOUNT, finaBrokenExec);
					formDTO.set(ContentManager.ACTION, USAGE);
					formDTO = validation.validateCurrencySmallAmount(formDTO);
					if (formDTO.get(ContentManager.ERROR) != null) {
						getErrorMap().setError("finaBrokenExecFormat", formDTO.get(ContentManager.ERROR));
						return false;
					}
					if (validation.isValidNegativeAmount(finaBrokenExec)) {
						getErrorMap().setError("finaBrokenExecFormat", BackOfficeErrorCodes.HMS_POSITIVE_AMOUNT);
						return false;
					}
				}
			} else if (!validation.isEmpty(finaBrokenExec)) {
				getErrorMap().setError("finaBrokenExecFormat", BackOfficeErrorCodes.MANDATORY_FINAL_FROM_DATE);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("finaBrokenExecFormat", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateDealType() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(dealType)) {
				getErrorMap().setError("dealType", BackOfficeErrorCodes.HMS_MANDATORY);
				return false;
			}
			if (!validation.isCMLOVREC("ELEASE", "DEAL_TYPE", dealType)) {
				getErrorMap().setError("dealType", BackOfficeErrorCodes.HMS_INVALID_OPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("dealType", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	// RENT STRUCTURE GRID 3

	public boolean validateUptoToner(String uptoToner) {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(uptoToner)) {
				getErrorMap().setError("uptoToner", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (validation.isZero(uptoToner)) {
				getErrorMap().setError("uptoToner", BackOfficeErrorCodes.ZERO_CHECK);
				return false;
			}
			if (!validation.isValidNumber(uptoToner)) {
				getErrorMap().setError("uptoToner", BackOfficeErrorCodes.INVALID_NUMBER);
				return false;
			}
			if (!validation.isValidPositiveNumber(uptoToner)) {
				getErrorMap().setError("uptoToner", BackOfficeErrorCodes.HMS_POSITIVE_CHECK);
				return false;
			}
			if (Integer.parseInt(uptoToner) > Integer.parseInt(tenor)) {
				getErrorMap().setError("uptoToner", BackOfficeErrorCodes.TENOR_GREATER_THAN_TOTAL_TENOR);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("uptoToner", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateLeaseRentRate(String leaseRentRate) {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(leaseRentRate)) {
				getErrorMap().setError("leaseRentRate", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isThreeDigitTwoDecimal(leaseRentRate, 2)) {
				getErrorMap().setError("leaseRentRate", BackOfficeErrorCodes.INVALID_NUMBER);
				return false;
			}
			if (validation.isValidNegativeAmount(leaseRentRate)) {
				getErrorMap().setError("leaseRentRate", BackOfficeErrorCodes.HMS_POSITIVE_CHECK);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("leaseRentRate", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateRentalFormat(String rental) {
		CommonValidator validation = new CommonValidator();
		DTObject formDTO = new DTObject();
		try {
			if (validation.isEmpty(rental)) {
				getErrorMap().setError("rentalFormat", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (validation.isZeroAmount(rental)) {
				getErrorMap().setError("rentalFormat", BackOfficeErrorCodes.HMS_ZERO_AMOUNT);
				return false;
			}
			formDTO.set("CURRENCY_CODE", assetCurrency);
			formDTO.set(ProgramConstants.AMOUNT, rental);
			formDTO.set(ContentManager.ACTION, USAGE);
			formDTO = validation.validateCurrencySmallAmount(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("rentalFormat", formDTO.get(ContentManager.ERROR));
				return false;
			}
			if (validation.isValidNegativeAmount(rental)) {
				getErrorMap().setError("rentalFormat", BackOfficeErrorCodes.HMS_POSITIVE_AMOUNT);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("rentalFormat", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateExecChagsFormat(String execChags) {
		CommonValidator validation = new CommonValidator();
		DTObject formDTO = new DTObject();
		try {
			if (validation.isEmpty(execChags)) {
				execChags = RegularConstants.ZERO;
			}
			if (!validation.isZeroAmount(execChags)) {
				formDTO.set("CURRENCY_CODE", assetCurrency);
				formDTO.set(ProgramConstants.AMOUNT, execChags);
				formDTO.set(ContentManager.ACTION, USAGE);
				formDTO = validation.validateCurrencySmallAmount(formDTO);
				if (formDTO.get(ContentManager.ERROR) != null) {
					getErrorMap().setError("execChagsFormat", formDTO.get(ContentManager.ERROR));
					return false;
				}
			}
			if (validation.isValidNegativeAmount(execChags)) {
				getErrorMap().setError("execChagsFormat", BackOfficeErrorCodes.HMS_POSITIVE_AMOUNT);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("execChagsFormat", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateRentalGrid() {
		try {
			DTDObject dtdObject = new DTDObject();
			dtdObject.addColumn(0, "DTL_SL");
			dtdObject.addColumn(1, "UPTO_TENOR");
			dtdObject.addColumn(2, "LEASE_RENTAL_RATE");
			dtdObject.addColumn(3, "TENOR_RENT_FORMAT");
			dtdObject.addColumn(4, "TENOR_EXEC_CHGS_FORMAT");
			dtdObject.addColumn(5, "LEASE_RENTAL_RATE");
			dtdObject.addColumn(6, "TENOR_EXEC_CHGS");
			dtdObject.setXML(xmlleaseRentalChargesGrid);
			if (dtdObject.getRowCount() <= 0) {
				getErrorMap().setError("uptoToner", BackOfficeErrorCodes.HMS_ATLEAST_ONE_ROW);
				return false;
			}
			for (int i = 0; i < dtdObject.getRowCount(); i++) {
				if (!validateUptoToner(dtdObject.getValue(i, 1))) {
					getErrorMap().setError("uptoToner", BackOfficeErrorCodes.HMS_INVALID_NUMBER);
					return false;
				}
				if (!validateLeaseRentRate(dtdObject.getValue(i, 2))) {
					getErrorMap().setError("leaseRentRate", BackOfficeErrorCodes.HMS_INVALID_NUMBER);
					return false;
				}
				if (!validateRentalFormat(dtdObject.getValue(i, 5))) {
					getErrorMap().setError("rentalFormat", BackOfficeErrorCodes.HMS_INVALID_AMOUNT);
					return false;
				}
				if (!validateExecChagsFormat(dtdObject.getValue(i, 6))) {
					getErrorMap().setError("execChagsFormat", BackOfficeErrorCodes.HMS_INVALID_AMOUNT);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("uptoToner", BackOfficeErrorCodes.ERROR_IN_GRID);
		}
		return false;
	}

	// POPUP Grid Asset Component validation

	public boolean validateComponentSerial(String componentSerial) {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(componentSerial)) {
				getErrorMap().setError("componentSerial", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (validation.isZero(componentSerial)) {
				getErrorMap().setError("componentSerial", BackOfficeErrorCodes.ZERO_CHECK);
				return false;
			}
			if (!validation.isValidNumber(componentSerial)) {
				getErrorMap().setError("componentSerial", BackOfficeErrorCodes.INVALID_NUMBER);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("comAssetID", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateComAssetID(String comAssetID) {
		CommonValidator validation = new CommonValidator();
		try {
			if (!validation.isEmpty(comAssetID)) {
				if (!validation.isValidOtherInformation50(comAssetID)) {
					getErrorMap().setError("comAssetID", BackOfficeErrorCodes.INVALID_NUMBER);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("comAssetID", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateComAssetDesc(String comAssetDesc) {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(comAssetDesc)) {
				getErrorMap().setError("comAssetDesc", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidRemarks(comAssetDesc)) {
				getErrorMap().setError("comAssetDesc", BackOfficeErrorCodes.INVALID_DESCRIPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("comAssetDesc", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateComQty(String comQty) {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(comQty)) {
				getErrorMap().setError("comQty", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (validation.isZero(comQty)) {
				getErrorMap().setError("comQty", BackOfficeErrorCodes.ZERO_CHECK);
				return false;
			}
			if (!validation.isValidNumber(comQty)) {
				getErrorMap().setError("comQty", BackOfficeErrorCodes.INVALID_NUMBER);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("comQty", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateAssetModel(String assetModel) {
		CommonValidator validation = new CommonValidator();
		try {
			if (!validation.isEmpty(assetModel)) {
				if (!validation.isValidOtherInformation25(assetModel)) {
					getErrorMap().setError("assetModel", BackOfficeErrorCodes.HMS_INVALID_VALUE);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("assetModel", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateAssetConfig(String assetConfig) {
		CommonValidator validation = new CommonValidator();
		try {
			if (!validation.isEmpty(assetConfig)) {
				if (!validation.isValidOtherInformation25(assetConfig)) {
					getErrorMap().setError("assetConfig", BackOfficeErrorCodes.HMS_INVALID_VALUE);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("assetConfig", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateComponentQtyGrid(String xmlleaseAssetComponentGrid) {
		AccessValidator validation = new AccessValidator();
		try {
			DTDObject componentGrid = new DTDObject();
			componentGrid.addColumn(0, "DTL_SL");
			componentGrid.addColumn(1, "ASSET_COMP_SL");
			componentGrid.addColumn(2, "CUSTOMER_ASSET_ID");
			componentGrid.addColumn(3, "ASSET_DESCRIPTION");
			componentGrid.addColumn(4, "ASSET_QTY");
			componentGrid.addColumn(5, "IND_DTLS_REQD_VAL");
			componentGrid.addColumn(6, "IND_DTLS_REQD");
			componentGrid.addColumn(7, "ASSET_MODEL");
			componentGrid.addColumn(8, "ASSET_CONFIG");
			componentGrid.setXML(xmlleaseAssetComponentGrid);
			if (componentGrid.getRowCount() <= 0) {
				getErrorMap().setError("comAssetID", BackOfficeErrorCodes.HMS_ATLEAST_ONE_ROW);
				return false;
			}
			if (!validation.checkDuplicateRows(componentGrid, 1)) {
				getErrorMap().setError("comAssetID", BackOfficeErrorCodes.HMS_DUPLICATE_DATA);
				return false;
			}
			for (int i = 0; i < componentGrid.getRowCount(); i++) {
				if (!validateComponentSerial(componentGrid.getValue(i, 1))) {
					getErrorMap().setError("comAssetID", BackOfficeErrorCodes.HMS_INVALID_NUMBER);
					return false;
				}
				if (!validateComAssetID(componentGrid.getValue(i, 2))) {
					getErrorMap().setError("comAssetID", BackOfficeErrorCodes.HMS_INVALID_NUMBER);
					return false;
				}
				if (!validateComAssetDesc(componentGrid.getValue(i, 3))) {
					getErrorMap().setError("comAssetDesc", BackOfficeErrorCodes.HMS_INVALID_DESCRIPTION);
					return false;
				}
				if (!validateComQty(componentGrid.getValue(i, 4))) {
					getErrorMap().setError("comQty", BackOfficeErrorCodes.HMS_INVALID_NUMBER);
					return false;
				}
				if (!validateAssetModel(componentGrid.getValue(i, 7))) {
					getErrorMap().setError("assetModel", BackOfficeErrorCodes.HMS_INVALID_VALUE);
					return false;
				}
				if (!validateAssetConfig(componentGrid.getValue(i, 8))) {
					getErrorMap().setError("assetConfig", BackOfficeErrorCodes.HMS_INVALID_VALUE);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("comAssetID", BackOfficeErrorCodes.ERROR_IN_GRID);
		} finally {
			validation.close();
		}
		return false;
	}

	// POP UP NO of asset Grid

	public boolean validateAssetSl(String assetSl) {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(assetSl)) {
				getErrorMap().setError("assetSl", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (validation.isZero(assetSl)) {
				getErrorMap().setError("assetSl", BackOfficeErrorCodes.ZERO_CHECK);
				return false;
			}
			if (!validation.isValidNumber(assetSl)) {
				getErrorMap().setError("assetSl", BackOfficeErrorCodes.INVALID_NUMBER);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("assetSl", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateAssetCategory(String assetCategory) {
		try {
			DTObject formDTO = getFormDTO();
			formDTO.set("ASSET_TYPE_CODE", assetCategory);
			formDTO.set("STATE_CODE", billState);
			formDTO.set(ContentManager.ACTION, USAGE);
			formDTO = validateAssetCategory(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("assetCategory", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("assetCategory", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateAssetCategory(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		DTObject formDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		MasterValidator validation = new MasterValidator();
		String assetCategory = inputDTO.get("ASSET_TYPE_CODE");
		String billState = inputDTO.get("STATE_CODE");
		try {
			if (validation.isEmpty(assetCategory) || validation.isEmpty(billState)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION,AUTO_NON_AUTO,MODEL_NO,CONFIG_DTLS");
			resultDTO = validation.validateAssetType(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
			formDTO = getTaxScheduleCode(inputDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, formDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (formDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.SCHEDULE_CODE_NOT_CONFIGURED);
				return resultDTO;
			}
			taxScheduleCode = formDTO.get("TAX_SCHEDULE_CODE");
			resultDTO.set("TAX_SCHEDULE_CODE", formDTO.get("TAX_SCHEDULE_CODE"));
			resultDTO.set("MODEL_NO", resultDTO.get("MODEL_NO"));
			resultDTO.set("CONFIG_DTLS", resultDTO.get("CONFIG_DTLS"));
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public DTObject getTaxScheduleCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		try {
			String sqlQuery = "SELECT TAX_SCHEDULE_CODE FROM ASSETTYPETAXSCHDTL WHERE ENTITY_CODE=? AND ASSET_TYPE_CODE=? AND STATE_CODE=?";
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, context.getEntityCode());
			util.setString(2, inputDTO.get("ASSET_TYPE_CODE"));
			util.setString(3, inputDTO.get("STATE_CODE"));
			ResultSet resultset = util.executeQuery();
			if (resultset.next()) {
				if (!resultset.getString("TAX_SCHEDULE_CODE").isEmpty()) {
					resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
					resultDTO.set("TAX_SCHEDULE_CODE", resultset.getString("TAX_SCHEDULE_CODE"));
					return resultDTO;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			util.reset();
			dbContext.close();
		}
		return resultDTO;
	}

	public DTObject validateScheduleDuplicateCheck(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		String[] noOfassets = inputDTO.get("TOTAL_ASSETS").split(",");
		String prevAssets = RegularConstants.EMPTY_STRING;
		try {
			for (int i = 0; i < noOfassets.length; i++) {
				String sqlQuery = "SELECT TAX_SCHEDULE_CODE FROM ASSETTYPETAXSCHDTL WHERE ENTITY_CODE=? AND ASSET_TYPE_CODE=? AND STATE_CODE=?";
				util.reset();
				util.setSql(sqlQuery);
				util.setString(1, context.getEntityCode());
				util.setString(2, noOfassets[i]);
				util.setString(3, inputDTO.get("STATE_CODE"));
				ResultSet resultset = util.executeQuery();
				if (resultset.next()) {
					if (resultset.getString("TAX_SCHEDULE_CODE").isEmpty()) {
						resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.SCHEDULE_CODE_NOT_CONFIGURED);
						return resultDTO;
					} else {
						if (i == 0) {
							prevAssets = resultset.getString("TAX_SCHEDULE_CODE");
						} else if (!prevAssets.equals(resultset.getString("TAX_SCHEDULE_CODE"))) {
							Object[] errorParam = { "(" + prevAssets + ")" + " != " + "(" + resultset.getString("TAX_SCHEDULE_CODE") + ")" };
							resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.ASSET_CAN_SAME_ALL_SCHEDULE);
							resultDTO.setObject(ContentManager.ERROR_PARAM, errorParam);
							return resultDTO;
						}
					}
				}
			}
			resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			util.reset();
			dbContext.close();
		}
		return resultDTO;
	}

	public boolean validateAssetComponent(String assetComponent) {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(assetComponent)) {
				getErrorMap().setError("assetComponent", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (validation.isZero(assetComponent)) {
				getErrorMap().setError("assetComponent", BackOfficeErrorCodes.ZERO_CHECK);
				return false;
			}
			if (!validation.isValidNumber(assetComponent)) {
				getErrorMap().setError("assetComponent", BackOfficeErrorCodes.INVALID_NUMBER);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("assetComponent", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateAssetQtyGrid() {
		AccessValidator validation = new AccessValidator();
		try {
			DTDObject dtdObject = new DTDObject();
			dtdObject.addColumn(0, "SL");
			dtdObject.addColumn(1, "LINK");
			dtdObject.addColumn(2, "ASSET_SL");
			dtdObject.addColumn(3, "ASSET_TYPE_CODE");
			dtdObject.addColumn(4, "ASSET_DESC");
			dtdObject.addColumn(5, "ASSET_AUTO_TEXT");
			dtdObject.addColumn(6, "NOF_ASSET_COMP");
			dtdObject.addColumn(7, "XML_COMPONENT");
			dtdObject.addColumn(8, "ASSET_AUTO");
			dtdObject.addColumn(9, "SCHEDULE_CODE");
			dtdObject.setXML(xmlleaseNoOfAssetsGrid);
			if (dtdObject.getRowCount() <= 0) {
				getErrorMap().setError("noOfAssets", BackOfficeErrorCodes.HMS_ATLEAST_ONE_ROW);
				return false;
			}
			if (!validation.checkDuplicateRows(dtdObject, 1)) {
				getErrorMap().setError("noOfAssets", BackOfficeErrorCodes.HMS_DUPLICATE_DATA);
				return false;
			}
			for (int i = 0; i < dtdObject.getRowCount(); i++) {
				if (!validateAssetSl(dtdObject.getValue(i, 2))) {
					getErrorMap().setError("noOfAssets", BackOfficeErrorCodes.HMS_INVALID_NUMBER);
					return false;
				}
				if (!validateAssetCategory(dtdObject.getValue(i, 3))) {
					getErrorMap().setError("noOfAssets", BackOfficeErrorCodes.HMS_INVALID_NUMBER);
					return false;
				}
				if (!validateAssetComponent(dtdObject.getValue(i, 6))) {
					getErrorMap().setError("noOfAssets", BackOfficeErrorCodes.HMS_INVALID_DESCRIPTION);
					return false;
				}
				if (!validateComponentQtyGrid(dtdObject.getValue(i, 7))) {
					getErrorMap().setError("noOfAssets", BackOfficeErrorCodes.HMS_INVALID_NUMBER);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("noOfAssets", BackOfficeErrorCodes.ERROR_IN_GRID);
		} finally {
			validation.close();
		}
		return false;
	}

	public void setAssetCompToDTObject() throws Exception {
		CommonValidator validation = new CommonValidator();
		DTObject resultDTO = new DTObject();
		try {
			if (!validation.isEmpty(xmlleaseNoOfAssetsGrid)) {
				DTDObject temp_asset_object = new DTDObject();
				temp_asset_object.addColumn(0, "SL");
				temp_asset_object.addColumn(1, "LINK");
				temp_asset_object.addColumn(2, "ASSET_SL");
				temp_asset_object.addColumn(3, "ASSET_TYPE_CODE");
				temp_asset_object.addColumn(4, "ASSET_DESC");
				temp_asset_object.addColumn(5, "ASSET_AUTO_TEXT");
				temp_asset_object.addColumn(6, "NOF_ASSET_COMP");
				temp_asset_object.addColumn(7, "XML_COMPONENT");
				temp_asset_object.addColumn(8, "ASSET_AUTO");
				temp_asset_object.addColumn(9, "SCHEDULE_CODE");
				temp_asset_object.setXML(xmlleaseNoOfAssetsGrid);

				DTDObject temp_component_object = new DTDObject();
				temp_component_object.addColumn(0, "DTL_SL");
				temp_component_object.addColumn(1, "ASSET_COMP_SL");
				temp_component_object.addColumn(2, "CUSTOMER_ASSET_ID");
				temp_component_object.addColumn(3, "ASSET_DESCRIPTION");
				temp_component_object.addColumn(4, "ASSET_QTY");
				temp_component_object.addColumn(5, "IND_DTLS_REQD_VAL");
				temp_component_object.addColumn(6, "IND_DTLS_REQD");
				temp_component_object.addColumn(7, "ASSET_MODEL");
				temp_component_object.addColumn(8, "ASSET_CONFIG");

				// DTDObject asset_object = new DTDObject();
				asset_object.addColumn(0, "ASSET_SL");
				asset_object.addColumn(1, "ASSET_TYPE_CODE");
				asset_object.addColumn(2, "NOF_ASSET_COMP");

				// DTDObject component_object = new DTDObject();
				component_object.addColumn(0, "ASSET_SL");
				component_object.addColumn(1, "ASSET_COMP_SL");
				component_object.addColumn(2, "ASSET_DESCRIPTION");
				component_object.addColumn(3, "CUSTOMER_ASSET_ID");
				component_object.addColumn(4, "ASSET_QTY");
				component_object.addColumn(5, "IND_DTLS_REQD");
				component_object.addColumn(6, "ASSET_MODEL");
				component_object.addColumn(7, "ASSET_CONFIG");

				for (int i = 0; i < temp_asset_object.getRowSize(); i++) {
					asset_object.addRow();
					asset_object.setValue(i, "ASSET_SL", temp_asset_object.getValue(i, "ASSET_SL"));
					asset_object.setValue(i, "ASSET_TYPE_CODE", temp_asset_object.getValue(i, "ASSET_TYPE_CODE"));
					asset_object.setValue(i, "NOF_ASSET_COMP", temp_asset_object.getValue(i, "NOF_ASSET_COMP"));
					temp_component_object.clearRows();
					if (!validation.isEmpty(temp_asset_object.getValue(i, "XML_COMPONENT"))) {
						temp_component_object.setXML(temp_asset_object.getValue(i, "XML_COMPONENT"));
						int rowIndex = 0;
						for (int j = 0; j < temp_component_object.getRowSize(); j++) {
							component_object.addRow();
							rowIndex = component_object.getRowSize() - 1;
							component_object.setValue(rowIndex, "ASSET_SL", temp_asset_object.getValue(i, "ASSET_SL"));
							component_object.setValue(rowIndex, "ASSET_COMP_SL", temp_component_object.getValue(j, "ASSET_COMP_SL"));
							component_object.setValue(rowIndex, "ASSET_DESCRIPTION", temp_component_object.getValue(j, "ASSET_DESCRIPTION"));
							component_object.setValue(rowIndex, "CUSTOMER_ASSET_ID", temp_component_object.getValue(j, "CUSTOMER_ASSET_ID"));
							component_object.setValue(rowIndex, "ASSET_QTY", temp_component_object.getValue(j, "ASSET_QTY"));
							component_object.setValue(rowIndex, "IND_DTLS_REQD", temp_component_object.getValue(j, "IND_DTLS_REQD"));
							component_object.setValue(rowIndex, "ASSET_MODEL", temp_component_object.getValue(j, "ASSET_MODEL"));
							component_object.setValue(rowIndex, "ASSET_CONFIG", temp_component_object.getValue(j, "ASSET_CONFIG"));
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
	}

	// GRID 6 FINANCIAL GRID

	public DTObject getFinancialDetails(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		MasterValidator validation = new MasterValidator();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		DBContext dbContext = new DBContext();
		String startDate = inputDTO.get("BILL_FROM_DATE");
		String endDate = inputDTO.get("BILL_UPTO_DATE");
		String frequency = inputDTO.get("MONTH_FREQ");
		String editReq = inputDTO.get("EDIT_REQ");
		String taxFromDate = inputDTO.get("TAX_ON_DATE");
		String stateCode = inputDTO.get("STATE_CODE");
		String shipStateCode = inputDTO.get("SHIP_STATE_CODE");
		String scheduleCode = inputDTO.get("TAX_SCHEDULE_CODE");
		String curCode = inputDTO.get("TAX_CCY");
		int monthFreq = 0;
		int monthCount = 0;

		String StateTax = RegularConstants.ZERO;
		String StateServiceTax = RegularConstants.ZERO;
		String StateCess = RegularConstants.ZERO;
		String CentralTax = RegularConstants.ZERO;
		String CentralServiceTax = RegularConstants.ZERO;
		String CentralCess = RegularConstants.ZERO;
		double totalAmount = 0;
		int rowcnt = 1;
		int count = 1;
		int countRow = 1;
		try {
			if (frequency.equals("M"))
				monthFreq = 1;
			else if (frequency.equals("Q"))
				monthFreq = 3;
			else if (frequency.equals("H"))
				monthFreq = 6;
			else if (frequency.equals("Y"))
				monthFreq = 12;

			DTDObject dtdObject = new DTDObject();
			dtdObject.addColumn(0, "DTL_SL");
			dtdObject.addColumn(1, "UPTO_TENOR");
			dtdObject.addColumn(2, "TENOR_RENT_FORMAT");
			dtdObject.addColumn(3, "LEASE_RENTAL_RATE");
			dtdObject.addColumn(4, "TENOR_EXEC_CHGS_FORMAT");
			dtdObject.addColumn(5, "LEASE_RENTAL_RATE");
			dtdObject.addColumn(6, "TENOR_EXEC_CHGS");
			dtdObject.setXML(inputDTO.get("XML_RENT_AMT"));
			if (dtdObject.getRowCount() <= 0) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_ATLEAST_ONE_ROW);
				return resultDTO;
			}

			Map<String, DTObject> rentValues = new HashMap<String, DTObject>();
			DHTMLXGridUtility dhtmlUtility = new DHTMLXGridUtility();
			dhtmlUtility.init();
			Calendar c = Calendar.getInstance();
			Date fromDate = new java.sql.Date(BackOfficeFormatUtils.getDate(startDate, context.getDateFormat()).getTime());
			Date lastDay = new java.sql.Date(BackOfficeFormatUtils.getDate(endDate, context.getDateFormat()).getTime());
			c.setTime(lastDay);
			c.add(Calendar.DATE, -1);
			Date lastDate = c.getTime();

			String[] rentList;
			double totalServiceTax = 0;
			double totalCessTax = 0;

			double totalExecTax = 0;
			double totalRentalTax = 0;
			double totalStateTax = 0;
			double totalCentralTax = 0;
			double sumTotalServiceTax = 0;
			double sumTotalCessTax = 0;
			double sumTotalValue = 0;
			String tableSerial = RegularConstants.ZERO;
			while (fromDate.getTime() < lastDate.getTime()) {

				c.setTime(fromDate);
				c.add(Calendar.MONTH, monthFreq);
				c.add(Calendar.DATE, -1);
				Date uptoDate = c.getTime();
				monthCount = monthCount + monthFreq;
				rentList = getRent(monthCount, dtdObject).split("\\|");
				String ExeAmt = rentList[0];
				String rentAmt = rentList[1];
				if (!rentValues.containsKey(rentAmt + "#" + ExeAmt)) {

					Date date = new java.sql.Date(BackOfficeFormatUtils.getDate(taxFromDate, context.getDateFormat()).getTime());
					inputDTO.set("STATE_CODE", stateCode);
					inputDTO.set("SHIP_STATE_CODE", shipStateCode);
					inputDTO.set("TAX_SCHEDULE_CODE", scheduleCode);
					inputDTO.set("TAX_CCY", curCode);
					inputDTO.set("TAX_ON_DATE", String.valueOf(date));
					inputDTO.set("PRINCIPAL_AMOUNT", "0");
					inputDTO.set("INTEREST_AMOUNT", "0");
					inputDTO.set("EXECUTORY_AMOUNT", String.valueOf(ExeAmt));
					inputDTO.set("RENTAL_AMOUNT", String.valueOf(rentAmt));
					mainSl = getMaxCountForMainSerial(dbContext);
					inputDTO.set("MAIN_SL", String.valueOf(mainSl));
					inputDTO = validation.getFinancialTaxAmount(inputDTO);
					if (inputDTO.get(ContentManager.ERROR) != null) {
						resultDTO.set(ContentManager.ERROR, inputDTO.get(ContentManager.ERROR));
						return resultDTO;
					} else if (inputDTO.get("SP_ERROR") != null) {
						resultDTO.set(ContentManager.ERROR, inputDTO.get("SP_ERROR"));
						return resultDTO;
					} else {
						rentValues.put(rentAmt + "#" + ExeAmt, inputDTO);
						tableSerial = inputDTO.get("O_SERIAL");
						StateTax = inputDTO.get("O_STATE_TAX");
						StateServiceTax = inputDTO.get("O_STATE_SERVICE_TAX");
						StateCess = inputDTO.get("O_STATE_CESS");
						CentralTax = inputDTO.get("O_CENTRAL_TAX");
						CentralServiceTax = inputDTO.get("O_CENTRAL_SERVICE_TAX");
						CentralCess = inputDTO.get("O_CENTRAL_CESS");
						totalServiceTax = Double.parseDouble(StateServiceTax) + Double.parseDouble(CentralServiceTax);
						totalCessTax = Double.parseDouble(StateCess) + Double.parseDouble(CentralCess);
						totalAmount = Double.parseDouble(ExeAmt) + Double.parseDouble(rentAmt) + Double.parseDouble(StateTax) + Double.parseDouble(CentralTax) + totalServiceTax + totalCessTax;
						resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
					}
				} else {
					inputDTO = rentValues.get(rentAmt + "#" + ExeAmt);
					StateTax = inputDTO.get("O_STATE_TAX");
					StateServiceTax = inputDTO.get("O_STATE_SERVICE_TAX");
					StateCess = inputDTO.get("O_STATE_CESS");
					CentralTax = inputDTO.get("O_CENTRAL_TAX");
					CentralServiceTax = inputDTO.get("O_CENTRAL_SERVICE_TAX");
					CentralCess = inputDTO.get("O_CENTRAL_CESS");
					totalServiceTax = Double.parseDouble(StateServiceTax) + Double.parseDouble(CentralServiceTax);
					totalCessTax = Double.parseDouble(StateCess) + Double.parseDouble(CentralCess);
					totalAmount = Double.parseDouble(ExeAmt) + Double.parseDouble(rentAmt) + Double.parseDouble(StateTax) + Double.parseDouble(CentralTax) + totalServiceTax + totalCessTax;
					resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				}

				totalExecTax = Double.parseDouble(ExeAmt) + totalExecTax;
				totalRentalTax = Double.parseDouble(rentAmt) + totalRentalTax;
				totalStateTax = Double.parseDouble(StateTax) + totalStateTax;
				totalCentralTax = Double.parseDouble(CentralTax) + totalCentralTax;
				sumTotalServiceTax = totalServiceTax + sumTotalServiceTax;
				sumTotalCessTax = totalCessTax + sumTotalCessTax;
				sumTotalValue = totalAmount + sumTotalValue;

				dhtmlUtility.startRow();
				dhtmlUtility.setCell(String.valueOf(rowcnt++));
				if ("O".equals(editReq))
					dhtmlUtility.setCell(String.valueOf(count++));
				else
					dhtmlUtility.setCell("<i class='fa fa-edit' style='margin-right: 5px;font-size: 18px; color: #3da0e3; cursor: pointer'></i></a>^javascript:eleaseFinancial_editGrid(\"" + count++ + "\")");

				dhtmlUtility.setCell(BackOfficeFormatUtils.getDate(fromDate, context.getDateFormat()));
				dhtmlUtility.setCell(BackOfficeFormatUtils.getDate(uptoDate, context.getDateFormat()));

				// Format Amount
				dhtmlUtility.setCell("0");
				dhtmlUtility.setCell("0");
				dhtmlUtility.setCell(ReportRM.fmc(new BigDecimal(ExeAmt), context.getBaseCurrency(), context.getBaseCurrencyUnits()));
				dhtmlUtility.setCell(ReportRM.fmc(new BigDecimal(rentAmt), context.getBaseCurrency(), context.getBaseCurrencyUnits()));
				dhtmlUtility.setCell(ReportRM.fmc(new BigDecimal(StateTax), context.getBaseCurrency(), context.getBaseCurrencyUnits()) + "^javascript:viewStateTaxDetails(\"" + countRow + RegularConstants.PK_SEPARATOR + tableSerial + RegularConstants.PK_SEPARATOR + mainSl + RegularConstants.PK_SEPARATOR + StateTax + "\")");
				dhtmlUtility.setCell(ReportRM.fmc(new BigDecimal(CentralTax), context.getBaseCurrency(), context.getBaseCurrencyUnits()) + "^javascript:viewCentralTaxDetails(\"" + countRow + RegularConstants.PK_SEPARATOR + tableSerial + RegularConstants.PK_SEPARATOR + mainSl + RegularConstants.PK_SEPARATOR + CentralTax + "\")");

				dhtmlUtility.setCell(ReportRM.fmc(new BigDecimal(String.valueOf(totalServiceTax)), context.getBaseCurrency(), context.getBaseCurrencyUnits()) + "^javascript:viewServiceTaxDetails(\"" + countRow + RegularConstants.PK_SEPARATOR + tableSerial + RegularConstants.PK_SEPARATOR + mainSl + RegularConstants.PK_SEPARATOR + totalServiceTax + "\")");
				dhtmlUtility.setCell(ReportRM.fmc(new BigDecimal(String.valueOf(totalCessTax)), context.getBaseCurrency(), context.getBaseCurrencyUnits()) + "^javascript:viewCessTaxDetails(\"" + countRow + RegularConstants.PK_SEPARATOR + tableSerial + RegularConstants.PK_SEPARATOR + mainSl + RegularConstants.PK_SEPARATOR + totalCessTax + "\")");
				dhtmlUtility.setCell(ReportRM.fmc(new BigDecimal(totalAmount), context.getBaseCurrency(), context.getBaseCurrencyUnits()));

				// UnFormat Amount
				dhtmlUtility.setCell("0");
				dhtmlUtility.setCell("0");
				dhtmlUtility.setCell(ExeAmt);
				dhtmlUtility.setCell(rentAmt);
				dhtmlUtility.setCell(StateTax);
				dhtmlUtility.setCell(CentralTax);
				dhtmlUtility.setCell(String.valueOf(totalServiceTax));
				dhtmlUtility.setCell(String.valueOf(totalCessTax));
				dhtmlUtility.setCell(String.valueOf(totalAmount));
				dhtmlUtility.setCell("2");
				dhtmlUtility.setCell(String.valueOf(mainSl));
				dhtmlUtility.endRow();
				c.add(Calendar.DATE, 1);
				fromDate = c.getTime();
				countRow++;
			}
			dhtmlUtility.finish();
			if (rowcnt > 0) {
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				resultDTO.set("FINANCIAL_XML", dhtmlUtility.getXML());
				resultDTO.set("TOTAL_EXEC_TAX", ReportRM.fmc(new BigDecimal(totalExecTax), context.getBaseCurrency(), context.getBaseCurrencyUnits()));
				resultDTO.set("TOTAL_RENTAL_TAX", ReportRM.fmc(new BigDecimal(totalRentalTax), context.getBaseCurrency(), context.getBaseCurrencyUnits()));
				resultDTO.set("TOTAL_STATE_TAX_FORMAT", ReportRM.fmc(new BigDecimal(totalStateTax), context.getBaseCurrency(), context.getBaseCurrencyUnits()));
				resultDTO.set("TOTAL_CENTRAL_TAX_FORMAT", ReportRM.fmc(new BigDecimal(totalCentralTax), context.getBaseCurrency(), context.getBaseCurrencyUnits()));
				resultDTO.set("TOTAL_SERVICE_TAX_FORMAT", ReportRM.fmc(new BigDecimal(sumTotalServiceTax), context.getBaseCurrency(), context.getBaseCurrencyUnits()));
				resultDTO.set("TOTAL_CESS_TAX_FORMAT", ReportRM.fmc(new BigDecimal(sumTotalCessTax), context.getBaseCurrency(), context.getBaseCurrencyUnits()));
				resultDTO.set("TOTAL_VALUE_FORMAT", ReportRM.fmc(new BigDecimal(sumTotalValue), context.getBaseCurrency(), context.getBaseCurrencyUnits()));

				resultDTO.set("TOTAL_STATE_TAX", String.valueOf(totalStateTax));
				resultDTO.set("TOTAL_CENTRAL_TAX", String.valueOf(totalCentralTax));
				resultDTO.set("TOTAL_SERVICE_TAX", String.valueOf(sumTotalServiceTax));
				resultDTO.set("TOTAL_CESS_TAX", String.valueOf(sumTotalCessTax));
				resultDTO.set("TOTAL_VALUE", String.valueOf(sumTotalValue));

			} else {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_EMPTY_GRID);
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			dbContext.close();
			validation.close();
		}
		return resultDTO;
	}

	private String getRent(int monthCount, DTDObject dtdObject) {
		String rent = RegularConstants.ZERO + "|" + RegularConstants.ZERO;
		int month = 0;
		for (int i = 0; i < dtdObject.getRowCount(); i++) {
			month = Integer.parseInt(dtdObject.getValue(i, 1)) + month;
			if (month >= monthCount) {
				rent = dtdObject.getValue(i, 6) + "|" + dtdObject.getValue(i, 5);
				break;
			}
		}
		return rent;
	}

	public DTObject getFinancialMonthWiseDetails(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		DBContext dbContext = new DBContext();
		// DBUtil util = dbContext.createUtilInstance();
		MasterValidator validation = new MasterValidator();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		String finDate = inputDTO.get("TAX_ON_DATE");
		String principalAmt = inputDTO.get("PRINCIPAL_AMOUNT");
		String interestAmt = inputDTO.get("INTEREST_AMOUNT");
		double totalAmt = 0;
		double cessTax = 0;
		double serviceTax = 0;
		String mainEditSl = inputDTO.get("MAX_MAIN_SL");
		Date date = new java.sql.Date(BackOfficeFormatUtils.getDate(finDate, context.getDateFormat()).getTime());
		try {
			// mainSl = getMaxCountForMainSerial(dbContext);
			inputDTO.set("MAIN_SL", mainEditSl);
			inputDTO.set("TAX_ON_DATE", String.valueOf(date));
			inputDTO = validation.getFinancialTaxAmount(inputDTO);
			if (inputDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, inputDTO.get(ContentManager.ERROR));
				return resultDTO;
			} else if (inputDTO.get("SP_ERROR") != null) {
				resultDTO.set(ContentManager.ERROR, inputDTO.get("SP_ERROR"));
				return resultDTO;
			} else {

				resultDTO.set("STATE_TAX", inputDTO.get("O_STATE_TAX"));
				resultDTO.set("CENTRAL_TAX", inputDTO.get("O_CENTRAL_TAX"));
				serviceTax = Double.parseDouble(inputDTO.get("O_STATE_SERVICE_TAX")) + Double.parseDouble(inputDTO.get("O_CENTRAL_SERVICE_TAX"));
				resultDTO.set("SERVICE_TAX", String.valueOf(serviceTax));
				cessTax = Double.parseDouble(inputDTO.get("O_CENTRAL_CESS")) + Double.parseDouble(inputDTO.get("O_STATE_CESS"));
				resultDTO.set("CESS_TAX", String.valueOf(cessTax));
				totalAmt = Double.parseDouble(principalAmt) + Double.parseDouble(interestAmt) + Double.parseDouble(inputDTO.get("O_STATE_TAX")) + Double.parseDouble(inputDTO.get("O_CENTRAL_TAX")) + serviceTax + cessTax;
				resultDTO.set("TOTAL_TAX", String.valueOf(totalAmt));

				resultDTO.set("STATE_TAX_FORMAT", ReportRM.fmc(new BigDecimal(inputDTO.get("O_STATE_TAX")), context.getBaseCurrency(), context.getBaseCurrencyUnits()));
				resultDTO.set("CENTRAL_TAX_FORMAT", ReportRM.fmc(new BigDecimal(inputDTO.get("O_CENTRAL_TAX")), context.getBaseCurrency(), context.getBaseCurrencyUnits()));
				resultDTO.set("SERVICE_TAX_FORMAT", ReportRM.fmc(new BigDecimal(String.valueOf(serviceTax)), context.getBaseCurrency(), context.getBaseCurrencyUnits()));
				resultDTO.set("CESS_TAX_FORMAT", ReportRM.fmc(new BigDecimal(String.valueOf(cessTax)), context.getBaseCurrency(), context.getBaseCurrencyUnits()));
				resultDTO.set("TOTAL_TAX_FORMAT", ReportRM.fmc(new BigDecimal(totalAmt), context.getBaseCurrency(), context.getBaseCurrencyUnits()));
				resultDTO.set("EDIT_NEW_SL", mainEditSl);
				resultDTO.set("SP_SERIAL", inputDTO.get("O_SERIAL"));
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
			dbContext.close();
		}
		return resultDTO;
	}

	public DTDObject intialFinalAddcolumn(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		CommonValidator validation = new CommonValidator();

		try {
			dtdObject4.addColumn(0, "DTL_SL");
			dtdObject4.addColumn(1, "LINK");
			dtdObject4.addColumn(2, "RENTAL_START_DATE");
			dtdObject4.addColumn(3, "RENTAL_END_DATE");
			dtdObject4.addColumn(4, "PRINCIPAL_AMOUNT_FORMAT");
			dtdObject4.addColumn(5, "INTEREST_AMOUNT_FORMAT");
			dtdObject4.addColumn(6, "EXECUTORY_AMOUNT_FORMAT");
			dtdObject4.addColumn(7, "RENTAL_AMOUNT_FORMAT");
			dtdObject4.addColumn(8, "VAT_FORMAT");
			dtdObject4.addColumn(9, "CST_FORMAT");
			dtdObject4.addColumn(10, "ST_AMOUNT_FORMAT");
			dtdObject4.addColumn(11, "CESS_FORMAT");
			dtdObject4.addColumn(12, "TOTAL_RENTAL_FORMAT");

			dtdObject4.addColumn(13, "PRINCIPAL_AMOUNT");
			dtdObject4.addColumn(14, "INTEREST_AMOUNT");
			dtdObject4.addColumn(15, "EXECUTORY_AMOUNT");
			dtdObject4.addColumn(16, "RENTAL_AMOUNT");
			dtdObject4.addColumn(17, "VAT");
			dtdObject4.addColumn(18, "CST");
			dtdObject4.addColumn(19, "ST_AMOUNT");
			dtdObject4.addColumn(20, "CESS");
			dtdObject4.addColumn(21, "TOTAL_RENTAL");
			dtdObject4.addColumn(22, "INV_FOR");
			dtdObject4.addColumn(23, "MAIN_SL");
			dtdObject4.setXML(xmlleaseFinanceScheduleGrid);
			if (dtdObject4.getRowCount() <= 0) {
				getErrorMap().setError("leaseErrors", BackOfficeErrorCodes.HMS_ATLEAST_ONE_ROW);
				return null;
			}

			// if (!"O".equals(leaseProductType)) {
			// double totalprinc = 0.00;
			// double totalint = 0.00;
			// double totalRental = 0.00;
			// double sumPrincInter = 0.00;
			// for (int i = 0; i < dtdObject4.getRowCount(); i++) {
			// String totalPrincipal = dtdObject4.getValue(i, 13);
			// String totalInterest = dtdObject4.getValue(i, 14);
			// String totalRent = dtdObject4.getValue(i, 16);
			// totalprinc = totalprinc + Double.parseDouble(totalPrincipal);
			// totalint = totalint + Double.parseDouble(totalInterest);
			// totalRental = totalRental + Double.parseDouble(totalRent);
			// }
			// sumPrincInter = totalprinc + totalint;
			// DecimalFormat df = new DecimalFormat("#.##");
			// totalRental = Double.valueOf(df.format(totalRental));
			// if (sumPrincInter != totalRental) {
			// dtdObject4 = null;
			// return dtdObject4;
			// }
			// }
			int count = dtdObject4.getRowCount();
			// boolean initial = false;

			StringBuffer xmlFinancial = new StringBuffer();
			xmlFinancial.append(xmlleaseFinanceScheduleGrid);

			DHTMLXGridUtility gridUtility = new DHTMLXGridUtility();
			if (!intFromDate.equals(RegularConstants.EMPTY_STRING) && !intUptoDate.equals(RegularConstants.EMPTY_STRING)) {

				double serviceTax = 0;
				double cessTax = 0;
				double totalAmt = 0;

				gridUtility.init();
				gridUtility.startRow("0");
				gridUtility.setCell("");
				gridUtility.setCell("");
				gridUtility.setCell(intFromDate);
				gridUtility.setCell(intUptoDate);
				gridUtility.setCell("");
				gridUtility.setCell("");
				gridUtility.setCell("");
				gridUtility.setCell("");
				gridUtility.setCell("");
				gridUtility.setCell("");
				gridUtility.setCell("");
				gridUtility.setCell("");
				gridUtility.setCell("");
				gridUtility.setCell("0");
				gridUtility.setCell("0");
				gridUtility.setCell(intBrokenExec);
				gridUtility.setCell(intBrokenRent);

				inputDTO.set("STATE_CODE", billState);
				inputDTO.set("SHIP_STATE_CODE", shippingState);
				inputDTO.set("TAX_SCHEDULE_CODE", billState);
				inputDTO.set("TAX_CCY", assetCurrency);
				inputDTO.set("TAX_ON_DATE", String.valueOf(context.getCurrentBusinessDate()));
				inputDTO.set("PRINCIPAL_AMOUNT", "0");
				inputDTO.set("INTEREST_AMOUNT", "0");
				inputDTO.set("RENTAL_AMOUNT", intBrokenRent);
				inputDTO.set("EXECUTORY_AMOUNT", intBrokenExec);
				inputDTO.set("LESSEE_CODE", custLeaseCode);
				inputDTO.set("AGREEMENT_NO", agreementNo);
				inputDTO.set("SCHEDULE_ID", scheduleID);
				inputDTO.set("LEASE_PRODUCT_CODE", leaseProduct);
				inputDTO.set("CUSTOMER_ID", custID);
				inputDTO.set("MAIN_SL", spSerial);
				inputDTO.set("SCHEDULE_SL", "0");
				inputDTO = validation.getFinancialTaxAmount(inputDTO);
				if (inputDTO.get(ContentManager.ERROR) != null) {
					resultDTO.set(ContentManager.ERROR, inputDTO.get(ContentManager.ERROR));
					return null;
				} else {
					gridUtility.setCell(inputDTO.get("O_STATE_TAX"));
					gridUtility.setCell(inputDTO.get("O_CENTRAL_TAX"));
					serviceTax = Double.parseDouble(inputDTO.get("O_STATE_SERVICE_TAX")) + Double.parseDouble(inputDTO.get("O_CENTRAL_SERVICE_TAX"));
					gridUtility.setCell(String.valueOf(serviceTax));
					cessTax = Double.parseDouble(inputDTO.get("O_CENTRAL_CESS")) + Double.parseDouble(inputDTO.get("O_STATE_CESS"));
					gridUtility.setCell(String.valueOf(cessTax));
					totalAmt = Double.parseDouble(intBrokenExec) + Double.parseDouble(intBrokenRent) + Double.parseDouble(inputDTO.get("O_STATE_TAX")) + Double.parseDouble(inputDTO.get("O_CENTRAL_TAX")) + serviceTax + cessTax;
					gridUtility.setCell(String.valueOf(totalAmt));
					gridUtility.setCell("1");
				}
				gridUtility.endRow();
				String resultXML = gridUtility.getXML();

				xmlFinancial.replace(0, 27, resultXML);
				// initial = true;
			}

			if (!finalFromDate.equals(RegularConstants.EMPTY_STRING) && !finalUptoDate.equals(RegularConstants.EMPTY_STRING)) {

				double serviceTax1 = 0;
				double cessTax1 = 0;
				double totalAmt1 = 0;
				DHTMLXGridUtility gridUtility1 = new DHTMLXGridUtility();
				gridUtility1.startRow(String.valueOf(count++));
				gridUtility1.setCell("");
				gridUtility1.setCell("");
				gridUtility1.setCell(finalFromDate);
				gridUtility1.setCell(finalUptoDate);
				gridUtility1.setCell("");
				gridUtility1.setCell("");
				gridUtility1.setCell("");
				gridUtility1.setCell("");
				gridUtility1.setCell("");
				gridUtility1.setCell("");
				gridUtility1.setCell("");
				gridUtility1.setCell("");
				gridUtility1.setCell("");
				gridUtility1.setCell("0");
				gridUtility1.setCell("0");
				gridUtility1.setCell(finaBrokenExec);
				gridUtility1.setCell(finalBrokenRent);

				inputDTO.reset();
				inputDTO.set("STATE_CODE", billState);
				inputDTO.set("SHIP_STATE_CODE", shippingState);
				inputDTO.set("TAX_SCHEDULE_CODE", billState);
				inputDTO.set("TAX_CCY", assetCurrency);
				inputDTO.set("TAX_ON_DATE", String.valueOf(context.getCurrentBusinessDate()));
				inputDTO.set("PRINCIPAL_AMOUNT", "0");
				inputDTO.set("INTEREST_AMOUNT", "0");
				inputDTO.set("LESSEE_CODE", custLeaseCode);
				inputDTO.set("AGREEMENT_NO", agreementNo);
				inputDTO.set("SCHEDULE_ID", scheduleID);
				inputDTO.set("LEASE_PRODUCT_CODE", leaseProduct);
				inputDTO.set("CUSTOMER_ID", custID);
				inputDTO.set("RENTAL_AMOUNT", finalBrokenRent);
				inputDTO.set("EXECUTORY_AMOUNT", finaBrokenExec);
				inputDTO.set("MAIN_SL", spSerial);
				inputDTO.set("SCHEDULE_SL", String.valueOf(count));
				inputDTO = validation.getFinancialTaxAmount(inputDTO);
				if (inputDTO.get(ContentManager.ERROR) != null) {
					resultDTO.set(ContentManager.ERROR, inputDTO.get(ContentManager.ERROR));
					return null;
				} else {
					gridUtility1.setCell(inputDTO.get("O_STATE_TAX"));
					gridUtility1.setCell(inputDTO.get("O_CENTRAL_TAX"));
					serviceTax1 = Double.parseDouble(inputDTO.get("O_STATE_SERVICE_TAX")) + Double.parseDouble(inputDTO.get("O_CENTRAL_SERVICE_TAX"));
					gridUtility1.setCell(String.valueOf(serviceTax1));
					cessTax1 = Double.parseDouble(inputDTO.get("O_CENTRAL_CESS")) + Double.parseDouble(inputDTO.get("O_STATE_CESS"));
					gridUtility1.setCell(String.valueOf(cessTax1));
					totalAmt1 = Double.parseDouble(finaBrokenExec) + Double.parseDouble(finalBrokenRent) + Double.parseDouble(inputDTO.get("O_STATE_TAX")) + Double.parseDouble(inputDTO.get("O_CENTRAL_TAX")) + serviceTax1 + cessTax1;
					gridUtility1.setCell(String.valueOf(totalAmt1));
					gridUtility1.setCell("3");
				}
				gridUtility1.endRow();
				gridUtility1.finish();
				String resultXML1 = gridUtility1.getXML();
				int totalLine = xmlFinancial.length();
				int rowsRemove = totalLine - Integer.parseInt("7");
				xmlFinancial.replace(rowsRemove, totalLine, resultXML1);
			}
			dtdObject4.clearRows();
			dtdObject4.setXML(xmlFinancial.toString());
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			validation.close();
		}
		return dtdObject4;
	}

	// LOAD AND VIEW FINANCE GRID

	public DTObject loadFinanceGrid(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		MasterValidator validation = new MasterValidator();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		String editReq = inputDTO.get("EDIT_REQ");
		int rowcnt = 1;
		int count = 1;
		int serial = 1;
		int countRow = 1;
		String stateCode = inputDTO.get("STATE_CODE");
		String shipStateCode = inputDTO.get("SHIP_STATE_CODE");
		String scheduleCode = inputDTO.get("TAX_SCHEDULE_CODE");
		String currencyCode = inputDTO.get("TAX_CCY");
		String taxDate = inputDTO.get("TAX_ON_DATE");
		String spSerial = inputDTO.get("MAIN_SL");

		double totalServiceTax = 0;
		// BigDecimal mani = new BigDecimal(0);
		double totalCessTax = 0;
		double serviceTax = 0;
		double cessTax = 0;
		double totalAmt = 0;
		double withOutTaxSum = 0;

		double totalExecTax = 0;
		double totalRentalTax = 0;
		double totalStateTax = 0;
		double totalCentralTax = 0;
		double sumTotalServiceTax = 0;
		double sumTotalCessTax = 0;
		double sumTotalValue = 0;
		Date date = new java.sql.Date(BackOfficeFormatUtils.getDate(taxDate, context.getDateFormat()).getTime());
		try {
			inputDTO.set("SERIAL", spSerial);
			removePreviousSpSerialResults(inputDTO);
			String sqlQuery = "SELECT TO_CHAR(RENTAL_START_DATE,'" + context.getDateFormat() + "')RENTAL_START_DATE,TO_CHAR(RENTAL_END_DATE,'" + context.getDateFormat() + "')RENTAL_END_DATE,PRINCIPAL_AMOUNT,INTEREST_AMOUNT,EXECUTORY_AMOUNT,RENTAL_AMOUNT,INV_FOR FROM LEASEFINANCESCHEDULE WHERE ENTITY_CODE= ? AND LESSEE_CODE=? AND AGREEMENT_NO=? AND SCHEDULE_ID=? AND INV_FOR='2' ";
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, context.getEntityCode());
			util.setString(2, inputDTO.get("LESSEE_CODE"));
			util.setString(3, inputDTO.get("AGREEMENT_NO"));
			util.setString(4, inputDTO.get("SCHEDULE_ID"));
			ResultSet rset = util.executeQuery();
			DHTMLXGridUtility dhtmlUtility = new DHTMLXGridUtility();
			dhtmlUtility.init();
			while (rset.next()) {
				dhtmlUtility.startRow(String.valueOf(rowcnt++));
				dhtmlUtility.setCell(String.valueOf(serial++));
				if ("O".equals(editReq))
					dhtmlUtility.setCell(String.valueOf(count++));
				else
					dhtmlUtility.setCell("<i class='fa fa-edit' style='margin-right: 5px;font-size: 18px; color: #3da0e3; cursor: pointer'></i></a>^javascript:eleaseFinancial_editGrid(\"" + count++ + "\")");

				dhtmlUtility.setCell(rset.getString("RENTAL_START_DATE"));
				dhtmlUtility.setCell(rset.getString("RENTAL_END_DATE"));
				dhtmlUtility.setCell(ReportRM.fmc(new BigDecimal(rset.getString("PRINCIPAL_AMOUNT")), context.getBaseCurrency(), context.getBaseCurrencyUnits()));
				dhtmlUtility.setCell(ReportRM.fmc(new BigDecimal(rset.getString("INTEREST_AMOUNT")), context.getBaseCurrency(), context.getBaseCurrencyUnits()));
				dhtmlUtility.setCell(ReportRM.fmc(new BigDecimal(rset.getString("EXECUTORY_AMOUNT")), context.getBaseCurrency(), context.getBaseCurrencyUnits()));
				dhtmlUtility.setCell(ReportRM.fmc(new BigDecimal(rset.getString("RENTAL_AMOUNT")), context.getBaseCurrency(), context.getBaseCurrencyUnits()));

				// mainSl = getMaxCountForMainSerial(dbContext);
				inputDTO.set("MAIN_SL", String.valueOf(spSerial));
				inputDTO.set("SCHEDULE_SL", String.valueOf(countRow));
				inputDTO.set("STATE_CODE", stateCode);
				inputDTO.set("SHIP_STATE_CODE", shipStateCode);
				inputDTO.set("TAX_SCHEDULE_CODE", scheduleCode);
				inputDTO.set("TAX_CCY", currencyCode);
				inputDTO.set("TAX_ON_DATE", String.valueOf(date));
				inputDTO.set("PRINCIPAL_AMOUNT", rset.getString("PRINCIPAL_AMOUNT"));
				inputDTO.set("INTEREST_AMOUNT", rset.getString("INTEREST_AMOUNT"));
				inputDTO.set("EXECUTORY_AMOUNT", rset.getString("EXECUTORY_AMOUNT"));
				inputDTO.set("RENTAL_AMOUNT", rset.getString("RENTAL_AMOUNT"));

				withOutTaxSum = Double.parseDouble(rset.getString("EXECUTORY_AMOUNT")) + Double.parseDouble(rset.getString("RENTAL_AMOUNT"));
				inputDTO = validation.getFinancialTaxAmount(inputDTO);
				if (inputDTO.get(ContentManager.ERROR) != null) {
					resultDTO.set(ContentManager.ERROR, inputDTO.get(ContentManager.ERROR));
					return null;
				} else {
					dhtmlUtility.setCell(ReportRM.fmc(new BigDecimal(inputDTO.get("O_STATE_TAX")), context.getBaseCurrency(), context.getBaseCurrencyUnits()) + "^javascript:viewStateTaxDetails(\"" + countRow + RegularConstants.PK_SEPARATOR + spSerial + RegularConstants.PK_SEPARATOR + inputDTO.get("O_STATE_TAX") + "\")");
					dhtmlUtility.setCell(ReportRM.fmc(new BigDecimal(inputDTO.get("O_CENTRAL_TAX")), context.getBaseCurrency(), context.getBaseCurrencyUnits()) + "^javascript:viewCentralTaxDetails(\"" + countRow + RegularConstants.PK_SEPARATOR + spSerial + RegularConstants.PK_SEPARATOR + inputDTO.get("O_CENTRAL_TAX") + "\")");

					serviceTax = Double.parseDouble(inputDTO.get("O_STATE_SERVICE_TAX")) + Double.parseDouble(inputDTO.get("O_CENTRAL_SERVICE_TAX"));
					dhtmlUtility.setCell(ReportRM.fmc(new BigDecimal(serviceTax), context.getBaseCurrency(), context.getBaseCurrencyUnits()) + "^javascript:viewServiceTaxDetails(\"" + countRow + RegularConstants.PK_SEPARATOR + spSerial + RegularConstants.PK_SEPARATOR + serviceTax + "\")");
					cessTax = Double.parseDouble(inputDTO.get("O_CENTRAL_CESS")) + Double.parseDouble(inputDTO.get("O_STATE_CESS"));
					dhtmlUtility.setCell(ReportRM.fmc(new BigDecimal(cessTax), context.getBaseCurrency(), context.getBaseCurrencyUnits()) + "^javascript:viewCessTaxDetails(\"" + countRow + RegularConstants.PK_SEPARATOR + spSerial + RegularConstants.PK_SEPARATOR + cessTax + "\")");
					totalAmt = withOutTaxSum + Double.parseDouble(inputDTO.get("O_STATE_TAX")) + Double.parseDouble(inputDTO.get("O_CENTRAL_TAX")) + serviceTax + cessTax;
					dhtmlUtility.setCell(ReportRM.fmc(new BigDecimal(totalAmt), context.getBaseCurrency(), context.getBaseCurrencyUnits()));
				}

				totalExecTax = Double.parseDouble(rset.getString("EXECUTORY_AMOUNT")) + totalExecTax;
				totalRentalTax = Double.parseDouble(rset.getString("RENTAL_AMOUNT")) + totalRentalTax;
				totalStateTax = Double.parseDouble(inputDTO.get("O_STATE_TAX")) + totalStateTax;
				totalCentralTax = Double.parseDouble(inputDTO.get("O_CENTRAL_TAX")) + totalCentralTax;
				sumTotalServiceTax = totalServiceTax + sumTotalServiceTax;
				sumTotalCessTax = totalCessTax + sumTotalCessTax;
				sumTotalValue = totalAmt + sumTotalValue;

				dhtmlUtility.setCell(rset.getString("PRINCIPAL_AMOUNT"));
				dhtmlUtility.setCell(rset.getString("INTEREST_AMOUNT"));
				dhtmlUtility.setCell(rset.getString("EXECUTORY_AMOUNT"));
				dhtmlUtility.setCell(rset.getString("RENTAL_AMOUNT"));

				dhtmlUtility.setCell(inputDTO.get("O_STATE_TAX"));
				dhtmlUtility.setCell(inputDTO.get("O_CENTRAL_TAX"));
				dhtmlUtility.setCell(String.valueOf(serviceTax));
				dhtmlUtility.setCell(String.valueOf(cessTax));
				dhtmlUtility.setCell(String.valueOf(totalAmt));
				dhtmlUtility.setCell("2");
				dhtmlUtility.setCell(String.valueOf(spSerial));
				dhtmlUtility.endRow();
				countRow++;
			}
			dhtmlUtility.finish();
			if (rowcnt > 0) {
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				resultDTO.set("LOAD_FINANCIAL_XML", dhtmlUtility.getXML());
				resultDTO.set("TOTAL_EXEC_TAX", ReportRM.fmc(new BigDecimal(totalExecTax), context.getBaseCurrency(), context.getBaseCurrencyUnits()));
				resultDTO.set("TOTAL_RENTAL_TAX", ReportRM.fmc(new BigDecimal(totalRentalTax), context.getBaseCurrency(), context.getBaseCurrencyUnits()));
				resultDTO.set("TOTAL_STATE_TAX_FORMAT", ReportRM.fmc(new BigDecimal(totalStateTax), context.getBaseCurrency(), context.getBaseCurrencyUnits()));
				resultDTO.set("TOTAL_CENTRAL_TAX_FORMAT", ReportRM.fmc(new BigDecimal(totalCentralTax), context.getBaseCurrency(), context.getBaseCurrencyUnits()));
				resultDTO.set("TOTAL_SERVICE_TAX_FORMAT", ReportRM.fmc(new BigDecimal(sumTotalServiceTax), context.getBaseCurrency(), context.getBaseCurrencyUnits()));
				resultDTO.set("TOTAL_CESS_TAX_FORMAT", ReportRM.fmc(new BigDecimal(sumTotalCessTax), context.getBaseCurrency(), context.getBaseCurrencyUnits()));
				resultDTO.set("TOTAL_VALUE_FORMAT", ReportRM.fmc(new BigDecimal(sumTotalValue), context.getBaseCurrency(), context.getBaseCurrencyUnits()));

				resultDTO.set("TOTAL_STATE_TAX", String.valueOf(totalStateTax));
				resultDTO.set("TOTAL_CENTRAL_TAX", String.valueOf(totalCentralTax));
				resultDTO.set("TOTAL_SERVICE_TAX", String.valueOf(sumTotalServiceTax));
				resultDTO.set("TOTAL_CESS_TAX", String.valueOf(sumTotalCessTax));
				resultDTO.set("TOTAL_VALUE", String.valueOf(sumTotalValue));

			} else {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_EMPTY_GRID);
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			dbContext.close();
			validation.close();
		}
		return resultDTO;
	}

	private String getMaxCountForMainSerial(DBContext dbContext) {
		DBUtil util = dbContext.createUtilInstance();
		try {
			util.reset();
			util.setSql("SELECT IFNULL(MAX(MAIN_SL),0) MAIN_SL FROM TMPTAXDETAILS ");
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				int maxSl = rset.getInt(1) + 1;
				return String.valueOf(maxSl);
			}
			return "0";
		} catch (Exception e) {
			e.printStackTrace();
			return "0";
		} finally {
			util.reset();
			dbContext.close();
		}
	}

	public DTObject getScheduleCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		RegistrationValidator validation = new RegistrationValidator();
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		try {
			if ("1".equals(inputDTO.get("SP_SERIAL_REQ"))) {
				util.reset();
				String sqlQuery = "SELECT FN_NEXTVAL(?) AS SEQ_SP_SERIAL FROM DUAL";
				util.setSql(sqlQuery);
				util.setString(1, "ELEASE");
				ResultSet rset = util.executeQuery();
				if (rset.next()) {
					String spTempSerial = rset.getString("SEQ_SP_SERIAL");
					resultDTO.set("SP_TEMP_SL", spTempSerial);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			util.reset();
			dbContext.close();
			validation.close();
		}
		return resultDTO;
	}

	public DTObject getTaxStateCentralWiseDetails(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		StringBuffer sqlQuery = new StringBuffer(RegularConstants.EMPTY_STRING);
		int rowcnt = 0;
		double totalTaxAmount = 0;
		String stateCode = RegularConstants.EMPTY_STRING;
		String stateDesc = RegularConstants.EMPTY_STRING;
		String installCenCode = RegularConstants.EMPTY_STRING;
		try {
			sqlQuery.append("SELECT CENTRAL_CODE FROM INSTALL WHERE PARTITION_NO=? ");
			util.reset();
			util.setSql(sqlQuery.toString());
			util.setString(1, context.getPartitionNo());
			ResultSet rs = util.executeQuery();
			if (rs.next()) {
				installCenCode = rs.getString("CENTRAL_CODE");
			}

			sqlQuery = new StringBuffer(RegularConstants.EMPTY_STRING);
			sqlQuery.append("SELECT TMP.TAX_CODE TAX_CODE,T.DESCRIPTION TAX_DESC,FN_FORMAT_AMOUNT(TMP.TAX_ON_AMT_PORTION ,TMP.TAX_CCY,CY.SUB_CCY_UNITS_IN_CCY)TAX_ON_AMT_PORTION,  ");
			sqlQuery.append("FN_FORMAT_AMOUNT(TMP.TAX_RATE ,TMP.TAX_CCY,CY.SUB_CCY_UNITS_IN_CCY)TAX_RATE, FN_FORMAT_AMOUNT(TMP.TAX_ON_AMT ,TMP.TAX_CCY,CY.SUB_CCY_UNITS_IN_CCY)TAX_ON_AMT,FN_FORMAT_AMOUNT(TMP.TAX_AMT ,TMP.TAX_CCY,CY.SUB_CCY_UNITS_IN_CCY)TAX_AMT,TAX_AMT TAX_AMT_TOTAL ,TMP.STATE_CODE,S.DESCRIPTION STATE_DESC ,DECODE_CMLOVREC('MTAX','TAX_ON',T.TAX_ON) TAX_ON_DESC ");
			sqlQuery.append("FROM TMPTAXDETAILS TMP, TAX T ,CURRENCY CY,STATE S  WHERE TMP.TAX_CODE=T.TAX_CODE  AND TMP.STATE_CODE=T.STATE_CODE AND TMP.TAX_CCY=CY.CCY_CODE AND S.ENTITY_CODE=T.ENTITY_CODE AND S.STATE_CODE=TMP.STATE_CODE ");
			sqlQuery.append("AND TMP.SCHEDULE_SL=? AND TMP.MAIN_SL=?  AND T.TAX_TYPE = ?");

			if ("STATE".equals(inputDTO.get("STATE_WISE"))) {
				sqlQuery.append(" AND TMP.STATE_CODE <> '" + installCenCode + "' ");
			} else if ("CENTRAL".equals(inputDTO.get("CENTRAL_WISE"))) {
				sqlQuery.append(" AND TMP.STATE_CODE ='" + installCenCode + "' ");
			}
			sqlQuery.append("ORDER BY TMP.TAX_ON_TAX_CODE,TMP.TAX_CODE");
			util.reset();
			util.setSql(sqlQuery.toString());
			util.setString(1, inputDTO.get("SCHEDULE_SL"));
			util.setString(2, inputDTO.get("MAIN_SL"));
			util.setString(3, inputDTO.get("TAX_TYPE"));
			ResultSet rset = util.executeQuery();
			DHTMLXGridUtility dhtmlUtility = new DHTMLXGridUtility();
			dhtmlUtility.init();
			while (rset.next()) {
				rowcnt++;
				dhtmlUtility.startRow(String.valueOf(rowcnt));
				dhtmlUtility.setCell("");
				dhtmlUtility.setCell(rset.getString("TAX_CODE") + "^javascript:viewTaxMasterWiseDetails(\"" + context.getEntityCode() + RegularConstants.PK_SEPARATOR + rset.getString("STATE_CODE") + RegularConstants.PK_SEPARATOR + rset.getString("TAX_CODE") + "\")");
				dhtmlUtility.setCell(rset.getString("TAX_DESC"));
				dhtmlUtility.setCell(rset.getString("TAX_RATE"));
				dhtmlUtility.setCell(rset.getString("TAX_ON_AMT_PORTION"));
				dhtmlUtility.setCell(rset.getString("TAX_ON_AMT"));
				dhtmlUtility.setCell(rset.getString("TAX_AMT"));
				dhtmlUtility.setCell(rset.getString("TAX_ON_DESC"));
				stateCode = rset.getString("STATE_CODE");
				stateDesc = rset.getString("STATE_DESC");

				totalTaxAmount = +totalTaxAmount + Float.parseFloat(rset.getString("TAX_AMT_TOTAL"));
				dhtmlUtility.endRow();
			}
			dhtmlUtility.finish();
			if (rowcnt > 0) {
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				resultDTO.set("TAX_GRID_DETAILS", dhtmlUtility.getXML());
				resultDTO.set("TOTAL_TAX", ReportRM.fmc(new BigDecimal(totalTaxAmount), context.getBaseCurrency(), context.getBaseCurrencyUnits()));
				resultDTO.set("STATE_CODE", stateCode);
				resultDTO.set("STATE_DESC", stateDesc);
			} else {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.TAX_CODE_NOT_CONFIGURED);
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			dbContext.close();
		}
		return resultDTO;
	}

	// MASTER SP LOAD
	public DTObject getFinancialScheduleDetails(DTObject inputDTO) {
		System.out.println("begin   " + new Date());
		DTObject resultDTO = new DTObject();
		MasterValidator validation = new MasterValidator();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		DBContext dbContext = validation.getDbContext();
		DBUtil util = dbContext.createUtilInstance();
		StringBuffer sqlQuery = new StringBuffer(RegularConstants.EMPTY_STRING);
		double totalExecTax = 0;
		double totalRentalTax = 0;
		double totalStateTax = 0;
		double totalCentralTax = 0;
		double totalServiceTax = 0;
		double totalCessTax = 0;
		double totalAmount = 0;
		int rowcnt = 0;
		int count = 1;
		int countRow = 1;
		String financialXML;
		try {

			String frequency = inputDTO.get("MONTH_FREQ");
			String valueIRR = inputDTO.get("IRR_VALUE");
			String noOfMonth = inputDTO.get("TENOR_MONTH");
			String arrearAdvance = inputDTO.get("ADVANCE_ARREARS_FLAG");
			String stateCode = inputDTO.get("STATE_CODE");
			String shipStateCode = inputDTO.get("SHIP_STATE_CODE");
			String scheduleCode = inputDTO.get("TAX_SCHEDULE_CODE");
			String curCode = inputDTO.get("TAX_CCY");
			String leaseProductType = inputDTO.get("LEAVE_TYPE");

			// String action = inputDTO.get("ACTION");
			String custLeaseCode = inputDTO.get("LESSEE_CODE");
			String agreementNo = inputDTO.get("AGREEMENT_NO");
			String scheduleID = inputDTO.get("SCHEDULE_ID");
			String leaseProduct = inputDTO.get("LEASE_PRODUCT_CODE");
			String custID = inputDTO.get("CUSTOMER_ID");

			String spSerial = inputDTO.get("MAIN_SL");
			String cbd = String.valueOf(context.getCurrentBusinessDate());
			Date startDate = new java.sql.Date(BackOfficeFormatUtils.getDate(String.valueOf(inputDTO.get("BILL_FROM_DATE")), context.getDateFormat()).getTime());
			String negativeCheck = RegularConstants.EMPTY_STRING;
			int gridCount = 0;
			StringBuffer gridColumns = new StringBuffer(RegularConstants.EMPTY_STRING);
			inputDTO.set("SERIAL", spSerial);
			inputDTO.setObject("DB_UTIL", util);
			removePreviousSpSerialResults(inputDTO);

			DTDObject dtdObject = new DTDObject();
			dtdObject.addColumn(0, "DTL_SL");
			dtdObject.addColumn(1, "UPTO_TENOR");
			dtdObject.addColumn(2, "TENOR_RENT_FORMAT");
			dtdObject.addColumn(3, "LEASE_RENTAL_RATE");
			dtdObject.addColumn(4, "TENOR_EXEC_CHGS_FORMAT");
			dtdObject.addColumn(5, "LEASE_RENTAL_RATE");
			dtdObject.addColumn(6, "TENOR_EXEC_CHGS");
			dtdObject.setXML(inputDTO.get("XML_RENT_AMT"));
			if (dtdObject.getRowCount() <= 0) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_ATLEAST_ONE_ROW);
				return resultDTO;
			} else {
				gridCount = dtdObject.getRowCount();
				int gridTenor = 0;
				for (int i = 0; i < dtdObject.getRowCount(); i++) {
					gridTenor += Integer.parseInt(dtdObject.getValue(i, 1));
					if (i != 0)
						gridColumns.append("$");
					gridColumns.append(dtdObject.getValue(i, 1) + RegularConstants.PK_SEPARATOR).append(dtdObject.getValue(i, 5) + RegularConstants.PK_SEPARATOR).append(dtdObject.getValue(i, 6));
				}
				if (!String.valueOf(noOfMonth).equals(String.valueOf(gridTenor))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.TENOR_GREATER_THAN_TOTAL_TENOR);
					return resultDTO;
				}
			}

			inputDTO.set("MAIN_SL", spSerial);
			inputDTO.set("TOTAL_SL", spSerial);
			inputDTO.set("MONTH_FREQ", String.valueOf(frequency));
			inputDTO.set("IRR", valueIRR);
			inputDTO.set("TOTAL_TENOR", noOfMonth);
			inputDTO.set("ARREAR_ADVANCE", arrearAdvance);
			inputDTO.set("START_DATE", String.valueOf(startDate));
			inputDTO.set("LEASE_TYPE", leaseProductType);
			inputDTO.set("STATE_CODE", stateCode);
			inputDTO.set("SHIP_STATE_CODE", shipStateCode);
			inputDTO.set("TAX_SCHEDULE_CODE", scheduleCode);
			inputDTO.set("TAX_CCY", curCode);
			inputDTO.set("CBD", cbd);
			inputDTO.set("LESSEE_CODE", custLeaseCode);
			inputDTO.set("AGREEMENT_NO", agreementNo);
			inputDTO.set("SCHEDULE_ID", scheduleID);
			inputDTO.set("LEASE_PRODUCT_CODE", leaseProduct);
			inputDTO.set("CUSTOMER_ID", custID);
			inputDTO.set("GRID_VALUES", gridColumns.toString());
			inputDTO.set("GRID_COUNT", String.valueOf(gridCount));

			resultDTO = getFinancialSchedule(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			} else {
				negativeCheck = resultDTO.get("O_NEGATIVE_ERROR");
			}

			sqlQuery.append("SELECT MAIN_SL, SCHEDULE_SL, TO_CHAR(RENTAL_START_DATE,'" + context.getDateFormat() + "')RENTAL_START_DATE,TO_CHAR(RENTAL_END_DATE,'" + context.getDateFormat() + "')RENTAL_END_DATE,");
			sqlQuery.append(" PRINCIPAL_AMOUNT, INTEREST_AMOUNT, EXECUTORY_AMOUNT, RENTAL_AMOUNT, STATE_TAX_AMOUNT, CENTRAL_TAX_AMOUNT, SERVICE_TAX_AMOUNT, CESS_TAX_AMOUNT, TOTAL_AMOUNT , ");
			sqlQuery.append(" PRINCIPAL_AMOUNT_FORMAT, INTEREST_AMOUNT_FORMAT, EXECUTORY_AMOUNT_FORMAT, RENTAL_AMOUNT_FORMAT, STATE_TAX_AMOUNT_FORMAT, CENTRAL_TAX_AMOUNT_FORMAT, SERVICE_TAX_AMOUNT_FORMAT, CESS_TAX_AMOUNT_FORMAT, TOTAL_AMOUNT_FORMAT ");
			sqlQuery.append(" FROM TMPFINANCESCHEDULEWISEDETAILS WHERE MAIN_SL=? ORDER BY SCHEDULE_SL");
			util.reset();
			util.setSql(sqlQuery.toString());
			util.setString(1, spSerial);
			ResultSet rs = util.executeQuery();
			DHTMLXGridUtility dhtmlUtility = new DHTMLXGridUtility();
			dhtmlUtility.init();
			while (rs.next()) {
				rowcnt++;
				if (validation.isValidNegativeAmount(rs.getString("PRINCIPAL_AMOUNT")) || validation.isValidNegativeAmount(rs.getString("INTEREST_AMOUNT"))) {
					dhtmlUtility.startRow(String.valueOf(rowcnt), "color:red");
				} else {
					dhtmlUtility.startRow(String.valueOf(rowcnt));
				}
				dhtmlUtility.setCell(String.valueOf(rowcnt));
				if ("O".equals(leaseProductType))
					dhtmlUtility.setCell(String.valueOf(count++));
				else
					dhtmlUtility.setCell("<i class='fa fa-edit' style='margin-right: 5px;font-size: 18px; color: #3da0e3; cursor: pointer'></i></a>^javascript:eleaseFinancial_editGrid(\"" + rs.getString("SCHEDULE_SL") + "\")");

				dhtmlUtility.setCell(rs.getString("RENTAL_START_DATE"));
				dhtmlUtility.setCell(rs.getString("RENTAL_END_DATE"));

				// Format Amount

				dhtmlUtility.setCell(rs.getString("PRINCIPAL_AMOUNT_FORMAT"));
				dhtmlUtility.setCell(rs.getString("INTEREST_AMOUNT_FORMAT"));
				dhtmlUtility.setCell(rs.getString("EXECUTORY_AMOUNT_FORMAT"));
				dhtmlUtility.setCell(rs.getString("RENTAL_AMOUNT_FORMAT"));
				dhtmlUtility.setCell(rs.getString("STATE_TAX_AMOUNT_FORMAT") + "^javascript:viewStateTaxDetails(\"" + rs.getString("SCHEDULE_SL") + RegularConstants.PK_SEPARATOR + spSerial + RegularConstants.PK_SEPARATOR + rs.getString("STATE_TAX_AMOUNT") + "\")");
				dhtmlUtility.setCell(rs.getString("CENTRAL_TAX_AMOUNT_FORMAT") + "^javascript:viewCentralTaxDetails(\"" + rs.getString("SCHEDULE_SL") + RegularConstants.PK_SEPARATOR + spSerial + RegularConstants.PK_SEPARATOR + rs.getString("CENTRAL_TAX_AMOUNT") + "\")");

				dhtmlUtility.setCell(rs.getString("SERVICE_TAX_AMOUNT_FORMAT") + "^javascript:viewServiceTaxDetails(\"" + countRow + RegularConstants.PK_SEPARATOR + spSerial + RegularConstants.PK_SEPARATOR + rs.getString("SERVICE_TAX_AMOUNT") + "\")");
				dhtmlUtility.setCell(rs.getString("CESS_TAX_AMOUNT_FORMAT") + "^javascript:viewCessTaxDetails(\"" + countRow + RegularConstants.PK_SEPARATOR + spSerial + RegularConstants.PK_SEPARATOR + rs.getString("CESS_TAX_AMOUNT") + "\")");
				dhtmlUtility.setCell(rs.getString("TOTAL_AMOUNT_FORMAT"));

				// UnFormat Amount
				dhtmlUtility.setCell(rs.getString("PRINCIPAL_AMOUNT"));
				dhtmlUtility.setCell(rs.getString("INTEREST_AMOUNT"));
				dhtmlUtility.setCell(rs.getString("EXECUTORY_AMOUNT"));
				dhtmlUtility.setCell(rs.getString("RENTAL_AMOUNT"));
				dhtmlUtility.setCell(rs.getString("STATE_TAX_AMOUNT"));
				dhtmlUtility.setCell(rs.getString("CENTRAL_TAX_AMOUNT"));
				dhtmlUtility.setCell(rs.getString("SERVICE_TAX_AMOUNT"));
				dhtmlUtility.setCell(rs.getString("CESS_TAX_AMOUNT"));
				dhtmlUtility.setCell(rs.getString("TOTAL_AMOUNT"));
				dhtmlUtility.setCell("2");
				dhtmlUtility.setCell(String.valueOf(spSerial));
				dhtmlUtility.endRow();

				totalExecTax = Double.parseDouble(rs.getString("EXECUTORY_AMOUNT")) + totalExecTax;
				totalRentalTax = Double.parseDouble(rs.getString("RENTAL_AMOUNT")) + totalRentalTax;
				totalStateTax = Double.parseDouble(rs.getString("STATE_TAX_AMOUNT")) + totalStateTax;
				totalCentralTax = Double.parseDouble(rs.getString("CENTRAL_TAX_AMOUNT")) + totalCentralTax;
				totalServiceTax = Double.parseDouble(rs.getString("SERVICE_TAX_AMOUNT")) + totalServiceTax;
				totalCessTax = Double.parseDouble(rs.getString("CESS_TAX_AMOUNT")) + totalCessTax;
				totalAmount = Double.parseDouble(rs.getString("TOTAL_AMOUNT")) + totalAmount;
			}
			dhtmlUtility.finish();
			financialXML = dhtmlUtility.getXML();

			if (rowcnt > 0) {
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				resultDTO = getTaxDetails(inputDTO);
				resultDTO.set("TAX_WISE_XML", resultDTO.get("TAX_XML"));

				resultDTO.set("FINANCIAL_XML", financialXML);
				String baseCurrency = context.getBaseCurrency();
				String baseCurrencyUnits = context.getBaseCurrencyUnits();
				resultDTO.set("TOTAL_EXEC_TAX", ReportRM.fmc(new BigDecimal(totalExecTax), baseCurrency, baseCurrencyUnits));
				resultDTO.set("TOTAL_RENTAL_TAX", ReportRM.fmc(new BigDecimal(totalRentalTax), baseCurrency, baseCurrencyUnits));
				resultDTO.set("TOTAL_STATE_TAX_FORMAT", ReportRM.fmc(new BigDecimal(totalStateTax), baseCurrency, baseCurrencyUnits));
				resultDTO.set("TOTAL_CENTRAL_TAX_FORMAT", ReportRM.fmc(new BigDecimal(totalCentralTax), baseCurrency, baseCurrencyUnits));
				resultDTO.set("TOTAL_SERVICE_TAX_FORMAT", ReportRM.fmc(new BigDecimal(totalServiceTax), baseCurrency, baseCurrencyUnits));
				resultDTO.set("TOTAL_CESS_TAX_FORMAT", ReportRM.fmc(new BigDecimal(totalCessTax), baseCurrency, baseCurrencyUnits));
				resultDTO.set("TOTAL_VALUE_FORMAT", ReportRM.fmc(new BigDecimal(totalAmount), baseCurrency, baseCurrencyUnits));

				resultDTO.set("TOTAL_STATE_TAX", String.valueOf(totalStateTax));
				resultDTO.set("TOTAL_CENTRAL_TAX", String.valueOf(totalCentralTax));
				resultDTO.set("TOTAL_SERVICE_TAX", String.valueOf(totalServiceTax));
				resultDTO.set("TOTAL_CESS_TAX", String.valueOf(totalCessTax));
				resultDTO.set("TOTAL_VALUE", String.valueOf(totalAmount));
				resultDTO.set("NEGATIVE_CHECK", negativeCheck);

				System.out.println("end  " + new Date());
			} else {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_EMPTY_GRID);
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			dbContext.close();
			validation.close();
		}
		return resultDTO;
	}

	public final DTObject getFinancialSchedule(DTObject input) {
		DTObject resultDTO = new DTObject();
		DBUtil util = (DBUtil) input.getObject("DB_UTIL");
		resultDTO.set(ContentManager.ERROR, RegularConstants.NULL);
		try {
			util.reset();
			util.setMode(DBUtil.CALLABLE);
			util.setSql("CALL SP_ELEASE_ARRIVE_SCHEDULE(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
			util.setString(1, input.get("MAIN_SL"));
			util.setString(2, input.get("MONTH_FREQ"));
			util.setString(3, input.get("IRR"));
			util.setString(4, input.get("TOTAL_TENOR"));
			util.setString(5, input.get("ARREAR_ADVANCE"));
			util.setString(6, input.get("COMMENCEMENT_VALUE"));
			util.setString(7, input.get("START_DATE"));
			util.setString(8, input.get("LEASE_TYPE"));
			util.setLong(9, Long.parseLong(context.getEntityCode()));
			util.setString(10, input.get("STATE_CODE"));
			util.setString(11, input.get("SHIP_STATE_CODE"));
			util.setString(12, input.get("TAX_SCHEDULE_CODE"));
			util.setString(13, input.get("TAX_CCY"));
			util.setString(14, input.get("CBD"));

			util.setString(15, input.get("LESSEE_CODE"));
			util.setString(16, input.get("AGREEMENT_NO"));
			util.setString(17, input.get("SCHEDULE_ID"));
			util.setString(18, input.get("CUSTOMER_ID"));
			util.setString(19, input.get("LEASE_PRODUCT_CODE"));
			util.setString(20, input.get("GRID_COUNT"));
			util.setString(21, input.get("GRID_VALUES"));

			util.registerOutParameter(22, Types.CHAR);
			util.registerOutParameter(23, Types.VARCHAR);
			util.execute();
			String error = util.getString(23);
			if (error.equals("S")) {
				resultDTO.set("O_NEGATIVE_ERROR", util.getString(22));
				resultDTO.set("O_ERROR_CODE", util.getString(23));
			} else {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_AMOUNT);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return resultDTO;
		} finally {
			util.reset();
		}
		return resultDTO;
	}

	public DTObject getTaxDetails(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		StringBuffer sqlQuery = new StringBuffer(RegularConstants.EMPTY_STRING);
		int rowcnt = 0;
		String totalSl = inputDTO.get("TOTAL_SL");
		// String[] totalSl = inputDTO.get("TOTAL_SL").split(",");
		try {
			sqlQuery.append("SELECT DISTINCT(TMP.TAX_CODE)TAX_CODE,TMP.STATE_CODE STATE_CODE , T.DESCRIPTION TAX_DESC,DECODE_CMLOVREC('MTAX','TAX_TYPE',T.TAX_TYPE) TAX_TYPE,FN_FORMAT_AMOUNT(TMP.TAX_RATE ,TMP.TAX_CCY,CY.SUB_CCY_UNITS_IN_CCY)TAX_RATE,FN_FORMAT_AMOUNT(TMP.TAX_ON_AMT ,TMP.TAX_CCY,CY.SUB_CCY_UNITS_IN_CCY)TAX_ON_AMT,TAX_ON_AMT_PORTION,DECODE_CMLOVREC('MTAX','TAX_ON',T.TAX_ON) TAX_ON_DESC,TMP.TAX_BY_US TAX_BY_US FROM TMPTAXDETAILS TMP, TAX T ,CURRENCY CY ");
			sqlQuery.append(" WHERE TMP.TAX_CODE=T.TAX_CODE  AND TMP.STATE_CODE=T.STATE_CODE AND TMP.TAX_CCY=CY.CCY_CODE ");
			// int totalLen = totalSl.length;
			// String rootValue = "";
			// for (int i = 0; i < totalLen; i++) {
			// if (i != 0 && i < totalLen)
			// rootValue += "," + "'" + totalSl[i] + "'";
			// else
			// rootValue += "'" + totalSl[i] + "'";
			// }
			// sqlQuery.append("AND TMP.MAIN_SL IN (" + rootValue + ") ");
			sqlQuery.append("AND TMP.MAIN_SL = ? ");
			sqlQuery.append(" GROUP BY TMP.TAX_CODE ORDER BY TMP.TAX_ON_TAX_CODE,TMP.TAX_CODE");
			util.reset();
			util.setSql(sqlQuery.toString());
			util.setString(1, totalSl);
			ResultSet rset = util.executeQuery();
			DHTMLXGridUtility dhtmlUtility = new DHTMLXGridUtility();
			dhtmlUtility.init();
			while (rset.next()) {
				rowcnt++;
				if ("1".equals(rset.getString("TAX_BY_US"))) {
					dhtmlUtility.startRow(String.valueOf(rowcnt), "color:red");
				} else
					dhtmlUtility.startRow(String.valueOf(rowcnt));
				dhtmlUtility.setCell("");
				dhtmlUtility.setCell(rset.getString("TAX_CODE") + "^javascript:viewTaxMasterDetails(\"" + context.getEntityCode() + RegularConstants.PK_SEPARATOR + rset.getString("STATE_CODE") + RegularConstants.PK_SEPARATOR + rset.getString("TAX_CODE") + "\")");
				dhtmlUtility.setCell(rset.getString("TAX_DESC"));
				dhtmlUtility.setCell(rset.getString("TAX_TYPE"));
				dhtmlUtility.setCell(rset.getString("TAX_RATE"));
				dhtmlUtility.setCell(rset.getString("TAX_ON_AMT_PORTION"));
				dhtmlUtility.setCell(rset.getString("TAX_ON_DESC"));
				dhtmlUtility.endRow();
			}
			dhtmlUtility.finish();
			if (rowcnt > 0) {
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				resultDTO.set("TAX_XML", dhtmlUtility.getXML());
			}
			// else {
			// resultDTO.set(ContentManager.ERROR,
			// BackOfficeErrorCodes.TAX_CODE_NOT_CONFIGURED);
			// }
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			dbContext.close();
		}
		return resultDTO;
	}

	
	public DTObject removePreviousSpSerialResults(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		// DBContext dbContext = new DBContext();
		// DBUtil util = dbContext.createUtilInstance();
		DBUtil util = (DBUtil) inputDTO.getObject("DB_UTIL");
		try {
			util.reset();
			util.setSql("DELETE FROM TMPTAXDETAILS WHERE MAIN_SL = ? ");
			util.setString(1, inputDTO.get("SERIAL"));
			util.execute();
			util.reset();
			util.setSql("DELETE FROM TMPFINANCESCHEDULEWISEDETAILS WHERE MAIN_SL = ? ");
			util.setString(1, inputDTO.get("SERIAL"));
			util.execute();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			util.reset();
		}
		return resultDTO;
	}

	public DTObject validateFinancialPeriodProcessStart(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		try {
			String sqlQuery = "SELECT COUNT(1) COUNT FROM LEASEFINANCESCHEDULE WHERE ENTITY_CODE=? AND LESSEE_CODE=? AND AGREEMENT_NO = ? AND SCHEDULE_ID=? AND COMP_ENTRY_DATE IS NOT NULL ";
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, context.getEntityCode());
			util.setString(2, inputDTO.get("LESSEE_CODE"));
			util.setString(3, inputDTO.get("AGREEMENT_NO"));
			util.setString(4, inputDTO.get("SCHEDULE_ID"));
			ResultSet rset = util.executeQuery();
			while (rset.next()) {
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				if (!"0".equals(rset.getString("COUNT"))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.CANT_MODIFY_LEASE_REPAY_START);
					return resultDTO;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			util.reset();
			dbContext.close();
		}
		return resultDTO;
	}

	public DTObject loadFinanceWiseGrid(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		MasterValidator validation = new MasterValidator();
		RegistrationValidator validator = new RegistrationValidator();
		DBContext dbContext = validation.getDbContext();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		StringBuffer sqlQuery = new StringBuffer(RegularConstants.EMPTY_STRING);
		// DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		int rowcnt = 1;
		int count = 1;
		int countRow = 1;
		String spSerial = inputDTO.get("MAIN_SL");
		String editReq = inputDTO.get("EDIT_REQ");

		double totalServiceTax = 0;
		double totalCessTax = 0;
		double totalExecTax = 0;
		double totalRentalTax = 0;
		double totalStateTax = 0;
		double totalCentralTax = 0;
		double totalAmount = 0;
		int scheduleSL = 1;

		String financialXML;
		try {
			inputDTO.set(ContentManager.FETCH_COLUMNS, "STATE_CODE");
			resultDTO = validator.validateCustomerAddressSerial(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.SHIPPING_NOT_CONFIG);
				return resultDTO;
			} else {
				inputDTO.set("SHIP_STATE_CODE", resultDTO.get("STATE_CODE"));
			}

			inputDTO.set("SERIAL", spSerial);
			inputDTO.setObject("DB_UTIL", util);
			removePreviousSpSerialResults(inputDTO);
			inputDTO.set("MAIN_SL", spSerial);
			inputDTO.set("TOTAL_SL", spSerial);
			inputDTO.set("SCHEDULE_SL", String.valueOf(scheduleSL));
			resultDTO = getLoadFinancialSchedule(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			} else {
				sqlQuery.append("SELECT MAIN_SL, SCHEDULE_SL, TO_CHAR(RENTAL_START_DATE,'" + context.getDateFormat() + "')RENTAL_START_DATE,TO_CHAR(RENTAL_END_DATE,'" + context.getDateFormat() + "')RENTAL_END_DATE,");
				sqlQuery.append(" PRINCIPAL_AMOUNT, INTEREST_AMOUNT, EXECUTORY_AMOUNT, RENTAL_AMOUNT, STATE_TAX_AMOUNT, CENTRAL_TAX_AMOUNT, SERVICE_TAX_AMOUNT, CESS_TAX_AMOUNT, TOTAL_AMOUNT , ");
				sqlQuery.append(" PRINCIPAL_AMOUNT_FORMAT, INTEREST_AMOUNT_FORMAT, EXECUTORY_AMOUNT_FORMAT, RENTAL_AMOUNT_FORMAT, STATE_TAX_AMOUNT_FORMAT, CENTRAL_TAX_AMOUNT_FORMAT, SERVICE_TAX_AMOUNT_FORMAT, CESS_TAX_AMOUNT_FORMAT, TOTAL_AMOUNT_FORMAT ");
				sqlQuery.append(" FROM TMPFINANCESCHEDULEWISEDETAILS WHERE MAIN_SL=? ORDER BY SCHEDULE_SL");
				util.reset();
				util.setSql(sqlQuery.toString());
				util.setString(1, spSerial);
				ResultSet rs = util.executeQuery();
				DHTMLXGridUtility dhtmlUtility = new DHTMLXGridUtility();
				dhtmlUtility.init();
				while (rs.next()) {
					dhtmlUtility.startRow();
					dhtmlUtility.setCell(String.valueOf(rowcnt++));
					if ("O".equals(editReq))
						dhtmlUtility.setCell(String.valueOf(count++));
					else
						dhtmlUtility.setCell("<i class='fa fa-edit' style='margin-right: 5px;font-size: 18px; color: #3da0e3; cursor: pointer'></i></a>^javascript:eleaseFinancial_editGrid(\"" + rs.getString("SCHEDULE_SL") + "\")");

					dhtmlUtility.setCell(rs.getString("RENTAL_START_DATE"));
					dhtmlUtility.setCell(rs.getString("RENTAL_END_DATE"));

					// Format Amount
					dhtmlUtility.setCell(rs.getString("PRINCIPAL_AMOUNT_FORMAT"));
					dhtmlUtility.setCell(rs.getString("INTEREST_AMOUNT_FORMAT"));
					dhtmlUtility.setCell(rs.getString("EXECUTORY_AMOUNT_FORMAT"));
					dhtmlUtility.setCell(rs.getString("RENTAL_AMOUNT_FORMAT"));
					dhtmlUtility.setCell(rs.getString("STATE_TAX_AMOUNT_FORMAT") + "^javascript:viewStateTaxDetails(\"" + rs.getString("SCHEDULE_SL") + RegularConstants.PK_SEPARATOR + spSerial + RegularConstants.PK_SEPARATOR + rs.getString("STATE_TAX_AMOUNT") + "\")");
					dhtmlUtility.setCell(rs.getString("CENTRAL_TAX_AMOUNT_FORMAT") + "^javascript:viewCentralTaxDetails(\"" + rs.getString("SCHEDULE_SL") + RegularConstants.PK_SEPARATOR + spSerial + RegularConstants.PK_SEPARATOR + rs.getString("CENTRAL_TAX_AMOUNT") + "\")");

					dhtmlUtility.setCell(rs.getString("SERVICE_TAX_AMOUNT_FORMAT") + "^javascript:viewServiceTaxDetails(\"" + countRow + RegularConstants.PK_SEPARATOR + spSerial + RegularConstants.PK_SEPARATOR + rs.getString("SERVICE_TAX_AMOUNT") + "\")");
					dhtmlUtility.setCell(rs.getString("CESS_TAX_AMOUNT_FORMAT") + "^javascript:viewCessTaxDetails(\"" + countRow + RegularConstants.PK_SEPARATOR + spSerial + RegularConstants.PK_SEPARATOR + rs.getString("CESS_TAX_AMOUNT") + "\")");
					dhtmlUtility.setCell(rs.getString("TOTAL_AMOUNT_FORMAT"));

					// UnFormat Amount
					dhtmlUtility.setCell(rs.getString("PRINCIPAL_AMOUNT"));
					dhtmlUtility.setCell(rs.getString("INTEREST_AMOUNT"));
					dhtmlUtility.setCell(rs.getString("EXECUTORY_AMOUNT"));
					dhtmlUtility.setCell(rs.getString("RENTAL_AMOUNT"));
					dhtmlUtility.setCell(rs.getString("STATE_TAX_AMOUNT"));
					dhtmlUtility.setCell(rs.getString("CENTRAL_TAX_AMOUNT"));
					dhtmlUtility.setCell(rs.getString("SERVICE_TAX_AMOUNT"));
					dhtmlUtility.setCell(rs.getString("CESS_TAX_AMOUNT"));
					dhtmlUtility.setCell(rs.getString("TOTAL_AMOUNT"));
					dhtmlUtility.setCell("2");
					dhtmlUtility.setCell(String.valueOf(spSerial));
					dhtmlUtility.endRow();

					totalExecTax = Double.parseDouble(rs.getString("EXECUTORY_AMOUNT")) + totalExecTax;
					totalRentalTax = Double.parseDouble(rs.getString("RENTAL_AMOUNT")) + totalRentalTax;
					totalStateTax = Double.parseDouble(rs.getString("STATE_TAX_AMOUNT")) + totalStateTax;
					totalCentralTax = Double.parseDouble(rs.getString("CENTRAL_TAX_AMOUNT")) + totalCentralTax;
					totalServiceTax = Double.parseDouble(rs.getString("SERVICE_TAX_AMOUNT")) + totalServiceTax;
					totalCessTax = Double.parseDouble(rs.getString("CESS_TAX_AMOUNT")) + totalCessTax;
					totalAmount = Double.parseDouble(rs.getString("TOTAL_AMOUNT")) + totalAmount;
				}
				dhtmlUtility.finish();
				financialXML = dhtmlUtility.getXML();
				if (rowcnt > 0) {
					resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
					resultDTO = getTaxDetails(inputDTO);
					resultDTO.set("TAX_WISE_XML", resultDTO.get("TAX_XML"));

					resultDTO.set("LOAD_FINANCIAL_XML", financialXML);
					String baseCurrency = context.getBaseCurrency();
					String baseCurrencyUnits = context.getBaseCurrencyUnits();
					resultDTO.set("TOTAL_EXEC_TAX", ReportRM.fmc(new BigDecimal(totalExecTax), baseCurrency, baseCurrencyUnits));
					resultDTO.set("TOTAL_RENTAL_TAX", ReportRM.fmc(new BigDecimal(totalRentalTax), baseCurrency, baseCurrencyUnits));
					resultDTO.set("TOTAL_STATE_TAX_FORMAT", ReportRM.fmc(new BigDecimal(totalStateTax), baseCurrency, baseCurrencyUnits));
					resultDTO.set("TOTAL_CENTRAL_TAX_FORMAT", ReportRM.fmc(new BigDecimal(totalCentralTax), baseCurrency, baseCurrencyUnits));
					resultDTO.set("TOTAL_SERVICE_TAX_FORMAT", ReportRM.fmc(new BigDecimal(totalServiceTax), baseCurrency, baseCurrencyUnits));
					resultDTO.set("TOTAL_CESS_TAX_FORMAT", ReportRM.fmc(new BigDecimal(totalCessTax), baseCurrency, baseCurrencyUnits));
					resultDTO.set("TOTAL_VALUE_FORMAT", ReportRM.fmc(new BigDecimal(totalAmount), baseCurrency, baseCurrencyUnits));

					resultDTO.set("TOTAL_STATE_TAX", String.valueOf(totalStateTax));
					resultDTO.set("TOTAL_CENTRAL_TAX", String.valueOf(totalCentralTax));
					resultDTO.set("TOTAL_SERVICE_TAX", String.valueOf(totalServiceTax));
					resultDTO.set("TOTAL_CESS_TAX", String.valueOf(totalCessTax));
					resultDTO.set("TOTAL_VALUE", String.valueOf(totalAmount));

				} else {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_EMPTY_GRID);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			dbContext.close();
			validation.close();
		}
		return resultDTO;
	}

	public final DTObject getLoadFinancialSchedule(DTObject input) {
		DTObject resultDTO = new DTObject();
		// DBUtil util = getDbContext().createUtilInstance();
		DBUtil util = (DBUtil) input.getObject("DB_UTIL");
		resultDTO.set(ContentManager.ERROR, RegularConstants.NULL);
		try {
			util.reset();
			util.setMode(DBUtil.CALLABLE);
			util.setSql("CALL SP_ELEASE_LOAD_FINANCE_SCHEDULE(?,?,?,?,?,?,?,?,?,?,?,?,?)");
			util.setString(1, input.get("MAIN_SL"));
			util.setString(2, input.get("SCHEDULE_SL"));
			util.setLong(3, Long.parseLong(context.getEntityCode()));
			util.setString(4, input.get("LESSEE_CODE"));
			util.setString(5, input.get("AGREEMENT_NO"));
			util.setString(6, input.get("SCHEDULE_ID"));
			util.setString(7, input.get("STATE_CODE"));
			util.setString(8, input.get("SHIP_STATE_CODE"));
			util.setString(9, input.get("TAX_SCHEDULE_CODE"));
			util.setString(10, input.get("TAX_CCY"));
			util.setString(11, input.get("CUSTOMER_ID"));
			util.setString(12, input.get("LEASE_PRODUCT_CODE"));
			util.registerOutParameter(13, Types.VARCHAR);
			util.execute();
			String error = util.getString(13);
			if (!error.equals("S")) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_NOT_EXISTS);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			return resultDTO;
		} finally {
			util.reset();
		}
		return resultDTO;
	}

	@SuppressWarnings("deprecation")
	public DTObject downloadFinancialWiseGrid(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		StringBuffer fileName = new StringBuffer(RegularConstants.EMPTY_STRING);
		HSSFWorkbook hwb = new HSSFWorkbook();
		HSSFSheet sheet = hwb.createSheet("sheet");
		String leaseType = inputDTO.get("LEASE_TYPE");
		StringBuffer sqlQuery = new StringBuffer(RegularConstants.EMPTY_STRING);
		try {
			ReportUtils utils = new ReportUtils();
			String reportIdentifier = utils.getReportSequence();
			sqlQuery.append("SELECT REPLACE(REPORT_GENERATION_PATH,'\\\\','/') REPORT_GENERATION_PATH FROM SYSTEMFOLDERS WHERE ENTITY_CODE = ?  AND EFFT_DATE = (SELECT MAX(EFFT_DATE) FROM SYSTEMFOLDERS WHERE ENTITY_CODE = ? )");
			util.reset();
			util.setSql(sqlQuery.toString());
			util.setString(1, context.getEntityCode());
			util.setString(2, context.getEntityCode());
			ResultSet rs = util.executeQuery();
			if (rs.next()) {
				fileName.append(rs.getString("REPORT_GENERATION_PATH"));
				fileName.append("FinancialSchedule_").append(reportIdentifier).append(".xls");
			}

			// filename.append("C:/PBS/REPORTS/FinancialSchedule.xls");

			sqlQuery = new StringBuffer(RegularConstants.EMPTY_STRING);
			sqlQuery.append("SELECT  SCHEDULE_SL, TO_CHAR(RENTAL_START_DATE,'" + context.getDateFormat() + "')RENTAL_START_DATE,TO_CHAR(RENTAL_END_DATE,'" + context.getDateFormat() + "')RENTAL_END_DATE,");
			if (!"O".equals(leaseType))
				sqlQuery.append(" PRINCIPAL_AMOUNT, INTEREST_AMOUNT, EXECUTORY_AMOUNT, RENTAL_AMOUNT, STATE_TAX_AMOUNT, CENTRAL_TAX_AMOUNT, SERVICE_TAX_AMOUNT, CESS_TAX_AMOUNT, TOTAL_AMOUNT  ");
			else
				sqlQuery.append("  EXECUTORY_AMOUNT, RENTAL_AMOUNT, STATE_TAX_AMOUNT, CENTRAL_TAX_AMOUNT, SERVICE_TAX_AMOUNT, CESS_TAX_AMOUNT, TOTAL_AMOUNT  ");

			sqlQuery.append(" FROM TMPFINANCESCHEDULEWISEDETAILS WHERE MAIN_SL=? ORDER BY SCHEDULE_SL");
			util.reset();
			util.setSql(sqlQuery.toString());
			util.setString(1, inputDTO.get("MAIN_SL"));
			ResultSet rset = util.executeQuery();
			FileOutputStream fileOut = new FileOutputStream(fileName.toString());
			int rowCount = 0;
			HSSFRow row;
			while (rset.next()) {
				if (rowCount == 0) {
					row = sheet.createRow(rowCount);
					if (!"O".equals(leaseType)) {
						row.createCell((short) 0).setCellValue("Serial");
						row.createCell((short) 1).setCellValue("Start Date");
						row.createCell((short) 2).setCellValue("End Date");
						row.createCell((short) 3).setCellValue("Principal Amount");
						row.createCell((short) 4).setCellValue("Interest Amount");
						row.createCell((short) 5).setCellValue("Executory Amount");
						row.createCell((short) 6).setCellValue("Rental Amount");
						row.createCell((short) 7).setCellValue("State Govt. Amount");
						row.createCell((short) 8).setCellValue("Central Govt. Amount");
						row.createCell((short) 9).setCellValue("Service Amount");
						row.createCell((short) 10).setCellValue("Cesses Amount");
						row.createCell((short) 11).setCellValue("Total Rental Amount");
					} else {
						row.createCell((short) 0).setCellValue("Serial");
						row.createCell((short) 1).setCellValue("Start Date");
						row.createCell((short) 2).setCellValue("End Date");
						row.createCell((short) 3).setCellValue("Executory Amount");
						row.createCell((short) 4).setCellValue("Rental Amount");
						row.createCell((short) 5).setCellValue("State Govt. Amount");
						row.createCell((short) 6).setCellValue("Central Govt. Amount");
						row.createCell((short) 7).setCellValue("Service Amount");
						row.createCell((short) 8).setCellValue("Cesses Amount");
						row.createCell((short) 9).setCellValue("Total Rental Amount");
					}
					rowCount++;
				}
				row = sheet.createRow(rowCount);
				if (!"O".equals(leaseType)) {
					row.createCell((short) 0).setCellValue(rset.getString(1));
					row.createCell((short) 1).setCellValue(rset.getString(2));
					row.createCell((short) 2).setCellValue(rset.getString(3));
					row.createCell((short) 3).setCellValue(new Double(rset.getString(4)));
					row.createCell((short) 4).setCellValue(new Double(rset.getString(5)));
					row.createCell((short) 5).setCellValue(new Double(rset.getString(6)));
					row.createCell((short) 6).setCellValue(new Double(rset.getString(7)));
					row.createCell((short) 7).setCellValue(new Double(rset.getString(8)));
					row.createCell((short) 8).setCellValue(new Double(rset.getString(9)));
					row.createCell((short) 9).setCellValue(new Double(rset.getString(10)));
					row.createCell((short) 10).setCellValue(new Double(rset.getString(11)));
					row.createCell((short) 11).setCellValue(new Double(rset.getString(12)));
				} else {
					row.createCell((short) 0).setCellValue(rset.getString(1));
					row.createCell((short) 1).setCellValue(rset.getString(2));
					row.createCell((short) 2).setCellValue(rset.getString(3));
					row.createCell((short) 3).setCellValue(new Double(rset.getString(4)));
					row.createCell((short) 4).setCellValue(new Double(rset.getString(5)));
					row.createCell((short) 5).setCellValue(new Double(rset.getString(6)));
					row.createCell((short) 6).setCellValue(new Double(rset.getString(7)));
					row.createCell((short) 7).setCellValue(new Double(rset.getString(8)));
					row.createCell((short) 8).setCellValue(new Double(rset.getString(9)));
					row.createCell((short) 9).setCellValue(new Double(rset.getString(10)));
				}
				rowCount++;
			}
			hwb.write(fileOut);
			fileOut.close();
			String deploymentContext = utils.getDeploymentContext();
			String userID = context.getUserID();
			String entityCode = context.getEntityCode();
			utils.createReportDownload(entityCode, reportIdentifier, deploymentContext, userID, 2, fileName.toString());
			resultDTO.set(ContentManager.REPORT_ID, reportIdentifier);
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {

			dbContext.close();
		}
		return resultDTO;

	}
}
