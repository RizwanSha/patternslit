package patterns.config.web.forms.lss;

import java.sql.ResultSet;
import java.util.ArrayList;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.framework.web.DHTMLXGridUtility;
import patterns.config.framework.web.GenericOption;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.LeaseValidator;
import patterns.config.validations.MasterValidator;
import patterns.config.web.forms.GenericFormBean;

public class qplcinterestledbean extends GenericFormBean {

	private static final long serialVersionUID = 1L;
	private String preLeaseRefDate;
	private String preLeaseRefSerial;
	private String customerID;
	private String customerName;
	private String xmlInterestGrid;
	private String fromMonth;
	private String fromMonthYear;
	private String toMonth;
	private String toMonthYear;

	public String getPreLeaseRefDate() {
		return preLeaseRefDate;
	}

	public void setPreLeaseRefDate(String preLeaseRefDate) {
		this.preLeaseRefDate = preLeaseRefDate;
	}

	public String getPreLeaseRefSerial() {
		return preLeaseRefSerial;
	}

	public void setPreLeaseRefSerial(String preLeaseRefSerial) {
		this.preLeaseRefSerial = preLeaseRefSerial;
	}

	public String getCustomerID() {
		return customerID;
	}

	public void setCustomerID(String customerID) {
		this.customerID = customerID;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getXmlInterestGrid() {
		return xmlInterestGrid;
	}

	public void setXmlInterestGrid(String xmlInterestGrid) {
		this.xmlInterestGrid = xmlInterestGrid;
	}

	public String getFromMonthYear() {
		return fromMonthYear;
	}

	public void setFromMonthYear(String fromMonthYear) {
		this.fromMonthYear = fromMonthYear;
	}

	public String getFromMonth() {
		return fromMonth;
	}

	public void setFromMonth(String fromMonth) {
		this.fromMonth = fromMonth;
	}

	public String getToMonth() {
		return toMonth;
	}

	public void setToMonth(String toMonth) {
		this.toMonth = toMonth;
	}

	public String getToMonthYear() {
		return toMonthYear;
	}

	public void setToMonthYear(String toMonthYear) {
		this.toMonthYear = toMonthYear;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public void reset() {
		preLeaseRefDate = RegularConstants.EMPTY_STRING;
		preLeaseRefSerial = RegularConstants.EMPTY_STRING;
		customerName = RegularConstants.EMPTY_STRING;
		DBContext dbContext = new DBContext();
		ArrayList<GenericOption> fromMonth = null;
		fromMonth = getGenericOptions("COMMON", "MONTH", dbContext, RegularConstants.NULL);
		webContext.getRequest().setAttribute("COMMON_MONTH", fromMonth);
		ArrayList<GenericOption> toMonth = null;
		toMonth = getGenericOptions("COMMON", "MONTH", dbContext, RegularConstants.NULL);
		webContext.getRequest().setAttribute("COMMON_MONTH", toMonth);
		dbContext.close();
	}

	public DTObject validatePreLeaseRefDate(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		CommonValidator validator = new CommonValidator();
		try {
			if (!validator.isEmpty(preLeaseRefDate)) {
				java.util.Date dateInstance = validator.isValidDate(preLeaseRefDate);
				if (dateInstance == null) {
					getErrorMap().setError("preLease", BackOfficeErrorCodes.INVALID_DATE);
					return resultDTO;
				}
			}
			return resultDTO;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("preLease", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return resultDTO;
	}

	public boolean validatePreLeaseRefSerial() {
		CommonValidator validator = new CommonValidator();
		try {
			if (!validator.isEmpty(preLeaseRefDate) && !validator.isEmpty(preLeaseRefSerial)) {
				if (!validator.isValidNumber(preLeaseRefSerial)) {
					getErrorMap().setError("preLease", BackOfficeErrorCodes.INVALID_NUMBER);
					return false;
				}
				DTObject formDTO = new DTObject();
				formDTO.set("CONTRACT_DATE", preLeaseRefDate);
				formDTO.set("CONTRACT_SL", preLeaseRefSerial);
				formDTO.set(ContentManager.ACTION, USAGE);
				formDTO = validatePreLeaseRefSerial(formDTO);
				if (formDTO.get(ContentManager.ERROR) != null) {
					getErrorMap().setError("preLease", formDTO.get(ContentManager.ERROR));
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("preLease", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validatePreLeaseRefSerial(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		LeaseValidator validation = new LeaseValidator();
		MasterValidator validator = new MasterValidator();
		try {
			if (validation.isEmpty(inputDTO.get("CONTRACT_SL")) || validation.isEmpty(inputDTO.get("CONTRACT_DATE"))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "CUSTOMER_ID");
			resultDTO = validation.validatePreLeaseContractReferenceSerial(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
			inputDTO.set("CUSTOMER_ID", resultDTO.get("CUSTOMER_ID"));
			inputDTO.set(ContentManager.FETCH_COLUMNS, "CUSTOMER_NAME");
			resultDTO = validator.valildateCustomerID(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
			return resultDTO;
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
			validator.close();
		}
		return resultDTO;
	}

	public DTObject validateFromMonth(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		CommonValidator validation = new CommonValidator();

		try {
			if (validation.isEmpty(fromMonth)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			if (!validation.isCMLOVREC("COMMON", "MONTH", fromMonth)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_OPTION);
				return resultDTO;
			}

		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public DTObject validateToMonth(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(toMonth)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			if (!validation.isCMLOVREC("COMMON", "MONTH", toMonth)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_OPTION);
				return resultDTO;
			}
			if (fromMonthYear.equals(toMonthYear)) {
				if (validation.isNumberLesser(toMonth, fromMonth)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.YEAR_EQUAL_THEN_MONTH);
					return resultDTO;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public DTObject validateFromMonthYear(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(fromMonthYear)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			if (!validation.isValidYear(fromMonthYear)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_YEAR);
				return resultDTO;
			}

		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public DTObject validateToMonthYear(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(toMonthYear)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			if (!validation.isValidYear(fromMonthYear)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_YEAR);
				return resultDTO;
			}
			if (validation.isNumberLesser(toMonthYear, fromMonthYear)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_YEAR);
				return resultDTO;
			}

		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public DTObject loadGridValues(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		MasterValidator validator = new MasterValidator();
		try {
			setPreLeaseRefDate(inputDTO.get("CONTRACT_DATE"));
			resultDTO = validatePreLeaseRefDate(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set("ERROR_PRE_DATE", getErrorResource(resultDTO.get(ContentManager.ERROR), null));
				resultDTO.set(ContentManager.ERROR, RegularConstants.NULL);
				return resultDTO;
			}
			setPreLeaseRefSerial(inputDTO.get("CONTRACT_SL"));
			resultDTO = validatePreLeaseRefSerial(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set("ERROR_PRE_SERIAL", getErrorResource(resultDTO.get(ContentManager.ERROR), null));
				resultDTO.set(ContentManager.ERROR, RegularConstants.NULL);
				return resultDTO;
			}
			setFromMonth(inputDTO.get("FROM_MONTH"));
			setFromMonthYear(inputDTO.get("FROM_YEAR"));
			setToMonth(inputDTO.get("UPTO_MONTH"));
			setToMonthYear(inputDTO.get("UPTO_YEAR"));
			resultDTO = validateFromMonth(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set("ERROR_FROM_MONTH", getErrorResource(resultDTO.get(ContentManager.ERROR), null));
				resultDTO.set(ContentManager.ERROR, RegularConstants.NULL);
				return resultDTO;
			}
			resultDTO = validateFromMonthYear(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set("ERROR_FROM_YEAR", getErrorResource(resultDTO.get(ContentManager.ERROR), null));
				resultDTO.set(ContentManager.ERROR, RegularConstants.NULL);
				return resultDTO;
			}
			resultDTO = validateToMonth(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set("ERROR_UPTO_MONTH", getErrorResource(resultDTO.get(ContentManager.ERROR), null));
				resultDTO.set(ContentManager.ERROR, RegularConstants.NULL);
				return resultDTO;
			}
			resultDTO = validateToMonthYear(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set("ERROR_UPTO_YEAR", getErrorResource(resultDTO.get(ContentManager.ERROR), null));
				resultDTO.set(ContentManager.ERROR, RegularConstants.NULL);
				return resultDTO;
			}
			DBUtil dbutil = validator.getDbContext().createUtilInstance();
			StringBuffer sqlQuery = new StringBuffer(RegularConstants.EMPTY_STRING);
			sqlQuery.append("SELECT P.FOR_YEAR,P.PAYMENT_SL,S.NAME,TO_CHAR(P.FROM_DATE, ?) FROM_DATE,TO_CHAR(P.UPTO_DATE, ?) UPTO_DATE,P.NOF_DAYS, ");
			sqlQuery.append("P.INTEREST_RATE,P.INTEREST_CCY,P.INTEREST_AMOUNT,P.PROC_BY,TO_CHAR(P.PROC_ON, ?) PROC_ON, ");
			sqlQuery.append("P.DEBIT_NOTE_INVENTORY,CM1.LOV_LABEL_EN_US AS FOR_MONTH,CM2.LOV_LABEL_EN_US AS ENTRY_MODE ");
			sqlQuery.append("FROM PLCINTERESTLED P ");
			sqlQuery.append("LEFT OUTER JOIN CMLOVREC CM1 ON (CM1.PGM_ID='COMMON' AND CM1.LOV_ID='MONTH' AND CM1.LOV_VALUE=P.FOR_MONTH) ");
			sqlQuery.append("LEFT OUTER JOIN CMLOVREC CM2 ON (CM2.PGM_ID='COMMON' AND CM2.LOV_ID='ENTRY_MODE' AND CM2.LOV_VALUE=P.ENTRY_MODE) ");
			sqlQuery.append("LEFT OUTER JOIN LEASEPOINVPAY L ON (L.ENTITY_CODE=P.ENTITY_CODE AND L.CONTRACT_DATE=P.CONTRACT_DATE AND L.CONTRACT_SL=P.CONTRACT_SL AND L.PAYMENT_SL=P.PAYMENT_SL) ");
			sqlQuery.append("LEFT OUTER JOIN LEASEPO O ON (O.ENTITY_CODE=P.ENTITY_CODE AND O.CONTRACT_DATE=P.CONTRACT_DATE AND O.CONTRACT_SL=P.CONTRACT_SL AND O.PO_SL=L.PO_SL) ");
			sqlQuery.append("LEFT OUTER JOIN SUPPLIER S ON (S.ENTITY_CODE=P.ENTITY_CODE AND S.SUPPLIER_ID=O.SUPPLIER_ID) ");
			sqlQuery.append("WHERE P.ENTITY_CODE=? AND P.CONTRACT_DATE=? AND P.CONTRACT_SL=?  ");
			sqlQuery.append("AND FOR_YEAR>=? AND FOR_YEAR<=? AND FOR_MONTH>=(CASE WHEN FOR_YEAR=? THEN ? ELSE FOR_MONTH END) AND ");
			sqlQuery.append(" FOR_MONTH<=(CASE WHEN FOR_YEAR=? THEN ? ELSE FOR_MONTH END) ");
			dbutil.reset();
			dbutil.setSql(sqlQuery.toString());
			dbutil.setString(1, context.getDateFormat());
			dbutil.setString(2, context.getDateFormat());
			dbutil.setString(3, context.getDateFormat() + " " + RegularConstants.TIME_FORMAT);
			dbutil.setLong(4, Long.parseLong(context.getEntityCode()));
			dbutil.setDate(5, new java.sql.Date(BackOfficeFormatUtils.getDate(inputDTO.get("CONTRACT_DATE"), BackOfficeConstants.JAVA_DATE_FORMAT).getTime()));
			dbutil.setInt(6, Integer.parseInt(inputDTO.get("CONTRACT_SL")));
			dbutil.setString(7, inputDTO.get("FROM_YEAR"));
			dbutil.setString(8, inputDTO.get("UPTO_YEAR"));
			dbutil.setString(9, inputDTO.get("FROM_YEAR"));
			dbutil.setString(10, inputDTO.get("FROM_MONTH"));
			dbutil.setString(11, inputDTO.get("UPTO_YEAR"));
			dbutil.setString(12, inputDTO.get("UPTO_MONTH"));
			ResultSet rset = dbutil.executeQuery();
			DHTMLXGridUtility gridUtility = new DHTMLXGridUtility();
			gridUtility.init();
			int rowId = 0;
			while (rset.next()) {
				rowId = rowId + 1;
				gridUtility.startRow(String.valueOf(rowId));
				gridUtility.setCell(rset.getString("FOR_YEAR"));
				gridUtility.setCell(rset.getString("FOR_MONTH"));
				gridUtility.setCell(rset.getString("PAYMENT_SL"));
				gridUtility.setCell(rset.getString("NAME"));
				gridUtility.setCell(rset.getString("FROM_DATE"));
				gridUtility.setCell(rset.getString("UPTO_DATE"));
				gridUtility.setCell(rset.getString("NOF_DAYS"));
				gridUtility.setCell(rset.getString("INTEREST_RATE"));
				gridUtility.setCell(rset.getString("INTEREST_CCY"));
				gridUtility.setCell(rset.getString("INTEREST_AMOUNT"));
				gridUtility.setCell(rset.getString("ENTRY_MODE"));
				gridUtility.setCell(rset.getString("PROC_BY"));
				gridUtility.setCell(rset.getString("PROC_ON"));
				gridUtility.setCell(rset.getString("DEBIT_NOTE_INVENTORY"));
				gridUtility.endRow();
			}
			gridUtility.finish();
			if (rowId != 0) {
				String resultXML = gridUtility.getXML();
				resultDTO.set(ContentManager.RESULT_XML, resultXML);
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
			} else {
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return resultDTO;
	}

	@Override
	public void validate() {
		// TODO Auto-generated method stub
		
	}

}
