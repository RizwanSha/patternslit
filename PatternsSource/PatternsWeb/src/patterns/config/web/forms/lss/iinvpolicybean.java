package patterns.config.web.forms.lss;

import java.util.ArrayList;

import patterns.config.constants.ProgramConstants;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.process.TBAActionType;
import patterns.config.framework.service.DTDObject;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.GenericOption;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.AccessValidator;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.MasterValidator;
import patterns.config.web.forms.GenericFormBean;

public class iinvpolicybean extends GenericFormBean {

	private static final long serialVersionUID = -6219657363150466160L;

	private String invoiceCurrency;
	private String effectiveDate;
	private String uptoInvoiceAmount;
	private String uptoInvoiceAmountFormat;
	private String xmliinvpolicyGrid;
	private String numberOfSignatures;
	private String signatureType;
	private boolean enabled;
	private String remarks;

	public String getInvoiceCurrency() {
		return invoiceCurrency;
	}

	public void setInvoiceCurrency(String invoiceCurrency) {
		this.invoiceCurrency = invoiceCurrency;
	}

	public String getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public String getUptoInvoiceAmount() {
		return uptoInvoiceAmount;
	}

	public void setUptoInvoiceAmount(String uptoInvoiceAmount) {
		this.uptoInvoiceAmount = uptoInvoiceAmount;
	}

	public String getUptoInvoiceAmountFormat() {
		return uptoInvoiceAmountFormat;
	}

	public void setUptoInvoiceAmountFormat(String uptoInvoiceAmountFormat) {
		this.uptoInvoiceAmountFormat = uptoInvoiceAmountFormat;
	}

	public String getXmliinvpolicyGrid() {
		return xmliinvpolicyGrid;
	}

	public void setXmliinvpolicyGrid(String xmliinvpolicyGrid) {
		this.xmliinvpolicyGrid = xmliinvpolicyGrid;
	}

	public String getNumberOfSignatures() {
		return numberOfSignatures;
	}

	public void setNumberOfSignatures(String numberOfSignatures) {
		this.numberOfSignatures = numberOfSignatures;
	}

	public String getSignatureType() {
		return signatureType;
	}

	public void setSignatureType(String signatureType) {
		this.signatureType = signatureType;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void reset() {
		invoiceCurrency = RegularConstants.EMPTY_STRING;
		effectiveDate = RegularConstants.EMPTY_STRING;
		uptoInvoiceAmount = RegularConstants.EMPTY_STRING;
		numberOfSignatures = RegularConstants.EMPTY_STRING;
		signatureType = RegularConstants.EMPTY_STRING;
		enabled = false;
		remarks = RegularConstants.EMPTY_STRING;

		DBContext dbContext = new DBContext();
		ArrayList<GenericOption> numberOfSignatures = null;
		numberOfSignatures = getGenericOptions("IINVPOLICY", "NOF_SIGN", dbContext, null);
		webContext.getRequest().setAttribute("IINVPOLICY_NOF_SIGN", numberOfSignatures);
		ArrayList<GenericOption> signatureType = null;
		signatureType = getGenericOptions("IINVPOLICY", "SIGN_TYPE", dbContext, null);
		webContext.getRequest().setAttribute("IINVPOLICY_SIGN_TYPE", signatureType);

		dbContext.close();

		setProcessBO("patterns.config.framework.bo.lss.iinvpolicyBO");
	}

	public void validate() {
		if (validateInvoiceCurrency()) {
			validateEffectiveDate();
			validateGrid();
			validateRemarks();
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.set("CCY_CODE", invoiceCurrency);
				formDTO.set("EFF_DATE", effectiveDate);
				formDTO.set("UPTO_INV_AMOUNT", uptoInvoiceAmount);
				formDTO.set("NOF_SIGNS", numberOfSignatures);
				formDTO.set("SIGN_TYPE", signatureType);
				if (getAction().equals(ADD)) {
					formDTO.set("ENABLED", decodeBooleanToString(true));
				} else {
					formDTO.set("ENABLED", decodeBooleanToString(enabled));
				}
				formDTO.set("REMARKS", remarks);
				DTDObject dtdObject = new DTDObject();
				dtdObject.addColumn(0, "SL");
				dtdObject.addColumn(1, "UPTO_INV_AMOUNT");
				dtdObject.addColumn(2, "NOF_SIGNS");
				dtdObject.addColumn(3, "SIGN_TYPE");
				dtdObject.addColumn(4, "NOOF_SIGNS");
				dtdObject.addColumn(5, "SIGNS_TYPE");
				dtdObject.addColumn(6, "INV_AMOUNT");
				dtdObject.setXML(xmliinvpolicyGrid);
				formDTO.setDTDObject("INVPOLICYHISTDTL", dtdObject);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("invoiceCurrency", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
	}

	public boolean validateInvoiceCurrency() {
		try {
			DTObject formDTO = getFormDTO();
			formDTO.set("CCY_CODE", invoiceCurrency);
			formDTO.set(ContentManager.USER_ACTION, USAGE);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("invoiceCurrency", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("invoiceCurrency", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public boolean validateEffectiveDate() {
		try {
			DTObject formDTO = getFormDTO();
			formDTO.set("CCY_CODE", invoiceCurrency);
			formDTO.set("EFF_DATE", effectiveDate);
			formDTO.set(ContentManager.USER_ACTION, getAction());
			formDTO = validateEffectiveDate(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("effectiveDate", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("effectiveDate", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateEffectiveDate(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		MasterValidator validation = new MasterValidator();
		String invoiceCurrency = inputDTO.get("CCY_CODE");
		String effectiveDate = inputDTO.get("EFF_DATE");
		String action = inputDTO.get(ContentManager.USER_ACTION);
		try {
			TBAActionType AT;
			if (action.equals(ADD))
				AT = TBAActionType.ADD;
			else
				AT = TBAActionType.MODIFY;
			if (validation.isEmpty(effectiveDate)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;

			}
			if (!validation.isEffectiveDateValid(effectiveDate, AT)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_EFFECTIVE_DATE);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "CCY_CODE");
			inputDTO.set(ContentManager.TABLE, "INVPOLICYHIST");
			String columns[] = new String[] { context.getEntityCode(), invoiceCurrency, effectiveDate };
			inputDTO.setObject(ContentManager.PROGRAM_KEY, columns);
			resultDTO = validation.validatePK(inputDTO);
			if (action.equals(ADD)) {
				if (ContentManager.DATA_AVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_EXISTS);
					return resultDTO;
				}
			} else {
				if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_NOT_EXISTS);
					return resultDTO;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateUptoInvoiceAmount(String uptoInvoiceAmount) {
		CommonValidator validation = new CommonValidator();
		DTObject formDTO = new DTObject();
		try {
			if (!validation.isEmpty(uptoInvoiceAmount)) {
				if (validation.isZero(uptoInvoiceAmount)) {
					getErrorMap().setError("uptoInvoiceAmount", BackOfficeErrorCodes.ZERO_CHECK);
					return false;
				}
				formDTO.set("CURRENCY_CODE", invoiceCurrency);
				formDTO.set("UPTO_INV_AMOUNT", uptoInvoiceAmount);
				formDTO.set(ProgramConstants.AMOUNT, uptoInvoiceAmount);
				formDTO = validation.validateCurrencySmallAmount(formDTO);
				if (formDTO.get(ContentManager.ERROR) != RegularConstants.NULL) {
					getErrorMap().setError("uptoInvoiceAmount", formDTO.get(ContentManager.ERROR));
					return false;
				}
				if (validation.isValidNegativeNumber(uptoInvoiceAmount)) {
					getErrorMap().setError("uptoInvoiceAmount", BackOfficeErrorCodes.POSITIVE_CHECK);
					return false;
				}
				
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("uptoInvoiceAmount", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	private boolean validateGrid() {
		AccessValidator validation = new AccessValidator();
		try {
			DTDObject dtdObject = new DTDObject();
			dtdObject.addColumn(0, "SL");
			dtdObject.addColumn(1, "UPTO_INV_AMOUNT");
			dtdObject.addColumn(2, "NOF_SIGNS");
			dtdObject.addColumn(3, "SIGN_TYPE");
			dtdObject.addColumn(4, "NOOF_SIGNS");
			dtdObject.addColumn(5, "SIGNS_TYPE");
			dtdObject.addColumn(6, "INV_AMOUNT");
			dtdObject.setXML(xmliinvpolicyGrid);
			if (dtdObject.getRowCount() <= 0) {
				getErrorMap().setError("uptoInvoiceAmount", BackOfficeErrorCodes.ADD_ATLEAST_ONE_ROW);
				return false;
			}
			if (!validation.checkDuplicateRows(dtdObject, 1)) {
				getErrorMap().setError("uptoInvoiceAmount", BackOfficeErrorCodes.DUPLICATE_DATA);
				return false;
			}
			for (int i = 0; i < dtdObject.getRowCount(); i++) {
				if (!validateUptoInvoiceAmount(dtdObject.getValue(i, 6))) {
					getErrorMap().setError("uptoInvoiceAmount", BackOfficeErrorCodes.INVALID_ACTION);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("uptoInvoiceAmount", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateRemarks() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (remarks != null && remarks.length() > 0) {
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			if (getAction().equals(MODIFY)) {
				if (commonValidator.isEmpty(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}
}
