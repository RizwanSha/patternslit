/*
 *
 Author : Raja E
 Created Date : 16-02-2017
 Spec Reference : ELEASEPDC
 Modification History
 ----------------------------------------------------------------------
 Sl.No		Modified Date	Author			Modified Changes	Version
 ----------------------------------------------------------------------

 */

package patterns.config.web.forms.lss;

import java.sql.ResultSet;

import patterns.config.constants.ProgramConstants;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.service.DTDObject;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.LeaseValidator;
import patterns.config.validations.MasterValidator;
import patterns.config.validations.RegistrationValidator;
import patterns.config.web.forms.GenericFormBean;

public class eleasepdcbean extends GenericFormBean {

	private static final long serialVersionUID = 1L;

	private String lesseCode;
	private String aggrementNumber;
	private String scheduleID;
	private String pdcLotSerial;
	private boolean disablePdcLot;
	private String pdclotDate;
	private String bankName;
	private String branchName;
	private String ifscCode;
	private String accountNumber;
	private String accountName;
	private String bankingProcessCode;
	private String noOfChque;
	private String chqInstrumentNo;
	private String chqDate;
	private String chqAmount;
	private String chqAmount_curr;
	private String chqAmountFormat;
	private String xmleleasepdcGrid;
	private String remarks;
	private String customerID;
	private String pdcCount;
	private String noOfChequeOld;

	public String getCustomerID() {
		return customerID;
	}

	public void setCustomerID(String customerID) {
		this.customerID = customerID;
	}

	public String getLesseCode() {
		return lesseCode;
	}

	public void setLesseCode(String lesseCode) {
		this.lesseCode = lesseCode;
	}

	public String getAggrementNumber() {
		return aggrementNumber;
	}

	public void setAggrementNumber(String aggrementNumber) {
		this.aggrementNumber = aggrementNumber;
	}

	public String getScheduleID() {
		return scheduleID;
	}

	public void setScheduleID(String scheduleID) {
		this.scheduleID = scheduleID;
	}

	public String getPdcLotSerial() {
		return pdcLotSerial;
	}

	public void setPdcLotSerial(String pdcLotSerial) {
		this.pdcLotSerial = pdcLotSerial;
	}

	public boolean isDisablePdcLot() {
		return disablePdcLot;
	}

	public void setDisablePdcLot(boolean disablePdcLot) {
		this.disablePdcLot = disablePdcLot;
	}

	public String getPdclotDate() {
		return pdclotDate;
	}

	public void setPdclotDate(String pdclotDate) {
		this.pdclotDate = pdclotDate;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getIfscCode() {
		return ifscCode;
	}

	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public String getBankingProcessCode() {
		return bankingProcessCode;
	}

	public void setBankingProcessCode(String bankingProcessCode) {
		this.bankingProcessCode = bankingProcessCode;
	}

	public String getNoOfChque() {
		return noOfChque;
	}

	public void setNoOfChque(String noOfChque) {
		this.noOfChque = noOfChque;
	}

	public String getChqInstrumentNo() {
		return chqInstrumentNo;
	}

	public void setChqInstrumentNo(String chqInstrumentNo) {
		this.chqInstrumentNo = chqInstrumentNo;
	}

	public String getChqDate() {
		return chqDate;
	}

	public void setChqDate(String chqDate) {
		this.chqDate = chqDate;
	}

	public String getChqAmount() {
		return chqAmount;
	}

	public void setChqAmount(String chqAmount) {
		this.chqAmount = chqAmount;
	}

	public String getChqAmountFormat() {
		return chqAmountFormat;
	}

	public void setChqAmountFormat(String chqAmountFormat) {
		this.chqAmountFormat = chqAmountFormat;
	}

	public String getXmleleasepdcGrid() {
		return xmleleasepdcGrid;
	}

	public void setXmleleasepdcGrid(String xmleleasepdcGrid) {
		this.xmleleasepdcGrid = xmleleasepdcGrid;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getChqAmount_curr() {
		return chqAmount_curr;
	}

	public void setChqAmount_curr(String chqAmount_curr) {
		this.chqAmount_curr = chqAmount_curr;
	}

	public String getPdcCount() {
		return pdcCount;
	}

	public void setPdcCount(String pdcCount) {
		this.pdcCount = pdcCount;
	}

	public String getNoOfChequeOld() {
		return noOfChequeOld;
	}

	public void setNoOfChequeOld(String noOfChequeOld) {
		this.noOfChequeOld = noOfChequeOld;
	}

	@Override
	public void reset() {
		// TODO Auto-generated method stub
		lesseCode = RegularConstants.EMPTY_STRING;
		aggrementNumber = RegularConstants.EMPTY_STRING;
		scheduleID = RegularConstants.EMPTY_STRING;
		pdcLotSerial = RegularConstants.EMPTY_STRING;
		disablePdcLot = false;
		pdclotDate = RegularConstants.EMPTY_STRING;
		bankName = RegularConstants.EMPTY_STRING;
		branchName = RegularConstants.EMPTY_STRING;
		ifscCode = RegularConstants.EMPTY_STRING;
		accountNumber = RegularConstants.EMPTY_STRING;
		accountName = RegularConstants.EMPTY_STRING;
		bankingProcessCode = RegularConstants.EMPTY_STRING;
		noOfChque = RegularConstants.EMPTY_STRING;
		chqInstrumentNo = RegularConstants.EMPTY_STRING;
		chqDate = RegularConstants.EMPTY_STRING;
		chqAmount = RegularConstants.EMPTY_STRING;
		chqAmountFormat = RegularConstants.EMPTY_STRING;
		chqAmount_curr = RegularConstants.EMPTY_STRING;
		xmleleasepdcGrid = RegularConstants.EMPTY_STRING;
		remarks = RegularConstants.EMPTY_STRING;
		customerID = RegularConstants.EMPTY_STRING;
		pdcCount = RegularConstants.EMPTY_STRING;
		noOfChequeOld = RegularConstants.EMPTY_STRING;
		setProcessBO("patterns.config.framework.bo.lss.eleasepdcBO");
	}

	@Override
	public void validate() {
		// TODO Auto-generated method stub
		if (validateLesseCode() && validateAggrementNumber() && validateScheduleID() && validatePDCLotSerial()) {
			if (!disablePdcLot) {
				validateIFSCCode();
				validateBankName();
				validateBranchName();
				validateAccountNumber();
				validateAccountName();
				validateBankingProcessCode();
				validateNoOfChque();
				if (!validateGrid())
					getErrorMap().setError("pdcDetailsError", BackOfficeErrorCodes.PDC_DETAILS_ERROR);
			}
			validateRemarks();
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.set("LESSEE_CODE", lesseCode);
				formDTO.set("AGREEMENT_NO", aggrementNumber);
				formDTO.set("SCHEDULE_ID", scheduleID);
				formDTO.set("PDC_LOT_SL", pdcLotSerial);
				formDTO.set("REMARKS", remarks);
				if (getAction().equals(ADD) && !getRectify().equals(RegularConstants.COLUMN_ENABLE))
					formDTO.set("LOT_DISABLED", decodeBooleanToString(false));
				else
					formDTO.set("LOT_DISABLED", decodeBooleanToString(disablePdcLot));
				if (!disablePdcLot) {
					formDTO.set("DATE_OF_ENTRY", pdclotDate);
					formDTO.set("CHQ_BRANCH_IFSC", ifscCode);
					formDTO.set("CHQ_BANK", bankName);
					formDTO.set("CHQ_BRANCH", branchName);
					formDTO.set("CHQ_AC_NO", accountNumber);
					formDTO.set("CHQ_AC_NAME", accountName);
					formDTO.set("BANK_PROCESS_CODE", bankingProcessCode);
					formDTO.set("NOF_PDC", noOfChque);

					DTDObject dtdObject = new DTDObject();
					dtdObject.addColumn(0, "SL");
					dtdObject.addColumn(1, "CHQ_INSTRUMENT_NO");
					dtdObject.addColumn(2, "CHQ_DATE");
					dtdObject.addColumn(3, "CHQ_AMOUNT");
					dtdObject.addColumn(4, "AMOUNT");
					dtdObject.setXML(xmleleasepdcGrid);
					formDTO.setDTDObject("LEASEPDCDTLS", dtdObject);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("lesseCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
	}

	private boolean validateLesseCode() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("SUNDRY_DB_AC", lesseCode);
			formDTO.set(ContentManager.USER_ACTION, USAGE);
			formDTO = validateLesseCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != RegularConstants.NULL) {
				getErrorMap().setError("lesseCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("lesseCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateLesseCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		LeaseValidator validation = new LeaseValidator();
		RegistrationValidator validator = new RegistrationValidator();
		String lesseCode = inputDTO.get("SUNDRY_DB_AC");
		try {
			if (validation.isEmpty(lesseCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH, ContentManager.FETCH_ALL);
			resultDTO = validation.valildateLesseCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != RegularConstants.NULL) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			inputDTO.set("CUSTOMER_ID", resultDTO.get("CUSTOMER_ID"));
			inputDTO.set(ContentManager.FETCH_COLUMNS, "CUSTOMER_ID,CUSTOMER_NAME");
			resultDTO = validator.valildateCustomerID(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
			validator.close();
		}
		return resultDTO;
	}

	private boolean validateAggrementNumber() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("CUSTOMER_ID", customerID);
			formDTO.set("AGREEMENT_NO", aggrementNumber);
			formDTO.set(ContentManager.USER_ACTION, USAGE);
			formDTO = validateAggrementNumber(formDTO);
			if (formDTO.get(ContentManager.ERROR) != RegularConstants.NULL) {
				getErrorMap().setError("aggrementNumber", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("aggrementNumber", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateAggrementNumber(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		LeaseValidator validation = new LeaseValidator();
		String chargeCode = inputDTO.get("AGREEMENT_NO");
		try {
			if (validation.isEmpty(chargeCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH, ContentManager.FETCH_ALL);
			resultDTO = validation.validateCustomerAgreement(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != RegularConstants.NULL) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	private boolean validateScheduleID() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("LESSEE_CODE", lesseCode);
			formDTO.set("AGREEMENT_NO", aggrementNumber);
			formDTO.set("SCHEDULE_ID", scheduleID);
			formDTO.set(ContentManager.USER_ACTION, USAGE);
			formDTO = validateScheduleID(formDTO);
			if (formDTO.get(ContentManager.ERROR) != RegularConstants.NULL) {
				getErrorMap().setError("scheduleID", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("scheduleID", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateScheduleID(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		LeaseValidator validation = new LeaseValidator();
		String scheduleId = inputDTO.get("SCHEDULE_ID");
		try {
			if (validation.isEmpty(scheduleId)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "SCHEDULE_ID,ASSET_CCY,AUTH_ON,LEASE_CLOSURE_ON");
			resultDTO = validation.validateScheduleID(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != RegularConstants.NULL) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
			if (resultDTO.getObject("AUTH_ON") == null) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNAUTHORIZED_LEASE);
				return resultDTO;
			}
			if (resultDTO.getObject("LEASE_CLOSURE_ON") != null) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.LEASE_CLOSED);
				return resultDTO;
			}
			resultDTO.set("REPAY_COUNT", getPaymentPDCCount(inputDTO).get("REPAY_COUNT"));
			if (Integer.parseInt(resultDTO.get("REPAY_COUNT")) == 0) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.REPAYMENT_MODE);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public DTObject getPaymentPDCCount(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		try {
			String sqlQuery = "SELECT COUNT(REPAY_MODE) FROM LEASEREPAYMENT WHERE ENTITY_CODE=? AND LESSEE_CODE=? AND AGREEMENT_NO=? AND SCHEDULE_ID=? AND REPAY_MODE='PDC'";
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, context.getEntityCode());
			util.setString(2, inputDTO.get("LESSEE_CODE"));
			util.setString(3, inputDTO.get("AGREEMENT_NO"));
			util.setString(4, inputDTO.get("SCHEDULE_ID"));
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				resultDTO.set("REPAY_COUNT", rset.getString(1));
			} else {
				resultDTO.set("REPAY_COUNT", RegularConstants.ZERO);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			util.reset();
			dbContext.close();
		}
		return resultDTO;
	}

	public DTObject getChequeCount(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		try {
			String sqlQuery = "SELECT COUNT(NOF_PDC) FROM LEASEPDC WHERE ENTITY_CODE=? AND LESSEE_CODE=? AND AGREEMENT_NO=? AND SCHEDULE_ID=?";
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, context.getEntityCode());
			util.setString(2, inputDTO.get("LESSEE_CODE"));
			util.setString(3, inputDTO.get("AGREEMENT_NO"));
			util.setString(4, inputDTO.get("SCHEDULE_ID"));
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				resultDTO.set("CHEQUE_COUNT", rset.getString(1));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			util.reset();
			dbContext.close();
		}
		return resultDTO;
	}

	public boolean validatePDCLotSerial() {
		try {
			if (getAction().equals(MODIFY)) {
				getErrorMap().setError("pdcLotSerial", BackOfficeErrorCodes.INVALID_ACTION);
				return false;
			}
			if (getRectify().equals(RegularConstants.COLUMN_ENABLE)) {
				DTObject formDTO = new DTObject();
				formDTO.set("LESSEE_CODE", lesseCode);
				formDTO.set("AGREEMENT_NO", aggrementNumber);
				formDTO.set("SCHEDULE_ID", scheduleID);
				formDTO.set("PDC_LOT_SL", pdcLotSerial);
				formDTO.set(ContentManager.ACTION, getAction());
				formDTO = validatePDCLotSerial(formDTO);
				if (formDTO.get(ContentManager.ERROR) != null) {
					getErrorMap().setError("pdcLotSerial", formDTO.get(ContentManager.ERROR));
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("pdcLotSerial", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validatePDCLotSerial(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		RegistrationValidator validation = new RegistrationValidator();
		String pdcLotSerial = inputDTO.get("PDC_LOT_SL");
		try {
			if (validation.isEmpty(pdcLotSerial)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			if (!validation.isValidNumber(pdcLotSerial)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_NUMBER);
				return resultDTO;
			}
			inputDTO.set(ContentManager.TABLE, "LEASEPDC");
			inputDTO.set(ContentManager.FETCH, ContentManager.FETCH_ALL);
			String columns[] = new String[] { context.getEntityCode(), inputDTO.get("LESSEE_CODE"), inputDTO.get("AGREEMENT_NO"), inputDTO.get("SCHEDULE_ID"), pdcLotSerial };
			inputDTO.setObject(ContentManager.PROGRAM_KEY, columns);
			resultDTO = validation.validatePK(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateIFSCCode() {
		try {
			if (!ifscCode.isEmpty()) {
				DTObject formDTO = getFormDTO();
				formDTO.set("IFSC_CODE", ifscCode);
				formDTO.set(ContentManager.USER_ACTION, USAGE);
				formDTO = validateIFSCCode(formDTO);
				if (formDTO.get(ContentManager.ERROR) != null) {
					getErrorMap().setError("ifscCode", formDTO.get(ContentManager.ERROR));
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("ifscCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateIFSCCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		MasterValidator validation = new MasterValidator();
		String ifscCode = inputDTO.get("IFSC_CODE");
		try {
			if (validation.isEmpty(ifscCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "BANK_NAME,BRANCH_NAME");
			resultDTO = validation.validateIFSCCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateBankName() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(bankName)) {
				getErrorMap().setError("bankName", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidName(bankName)) {
				getErrorMap().setError("bankName", BackOfficeErrorCodes.INVALID_NAME);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("bankName", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateBranchName() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(branchName)) {
				getErrorMap().setError("branchName", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidName(branchName)) {
				getErrorMap().setError("branchName", BackOfficeErrorCodes.INVALID_NAME);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("branchName", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateAccountNumber() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(accountNumber)) {
				getErrorMap().setError("accountNumber", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidAccountNumber(accountNumber)) {
				getErrorMap().setError("accountNumber", BackOfficeErrorCodes.INVALID_IFSC_CODE);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("accountNumber", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateAccountName() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(accountName)) {
				getErrorMap().setError("accountName", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidName(accountName)) {
				getErrorMap().setError("accountName", BackOfficeErrorCodes.INVALID_NAME);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("accountName", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateBankingProcessCode() {
		try {
			DTObject formDTO = getFormDTO();
			formDTO.set("BANK_PROCESS_CODE", bankingProcessCode);
			formDTO.set(ContentManager.ACTION, USAGE);
			formDTO = validateBankingProcessCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("bankingProcessCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("bankingProcessCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateBankingProcessCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		MasterValidator validation = new MasterValidator();
		String bankProcessCode = inputDTO.get("BANK_PROCESS_CODE");
		try {
			if (validation.isEmpty(bankProcessCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "NAME");
			resultDTO = validation.validateBankingProcessCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateNoOfChque() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(noOfChque)) {
				getErrorMap().setError("noOfChque", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (validation.isZero(noOfChque)) {
				getErrorMap().setError("noOfChque", BackOfficeErrorCodes.ZERO_CHECK);
				return false;
			}
			if (!validation.isValidThreeDigit(noOfChque)) {
				getErrorMap().setError("noOfChque", BackOfficeErrorCodes.NUMERIC_CHECK);
				return false;
			}
			if (validation.isValidNegativeNumber(noOfChque)) {
				getErrorMap().setError("noOfChque", BackOfficeErrorCodes.POSITIVE_CHECK);
				return false;
			}
			DTObject formDTO = getFormDTO();
			formDTO.set("LESSEE_CODE", lesseCode);
			formDTO.set("AGREEMENT_NO", aggrementNumber);
			formDTO.set("SCHEDULE_ID", scheduleID);
			formDTO.set(ContentManager.ACTION, USAGE);
			formDTO = getChequeCount(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("noOfChque", formDTO.get(ContentManager.ERROR));
				return false;
			}
			if (getRectify().equals(RegularConstants.COLUMN_DISABLE))
				noOfChequeOld = "0";
			int chequeCount = Integer.parseInt(formDTO.get("CHEQUE_COUNT")) + Integer.parseInt(noOfChque) - Integer.parseInt(noOfChequeOld);
			if (chequeCount > Integer.parseInt(pdcCount)) {
				Object[] param = { pdcCount };
				getErrorMap().setError("noOfChque", BackOfficeErrorCodes.CANT_BE_GT_PDC_COUNT, param);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("noOfChque", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateRemarks() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (remarks != null && remarks.length() > 0) {
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			if (getRectify().equals(RegularConstants.COLUMN_ENABLE)) {
				if (commonValidator.isEmpty(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}

	private boolean validateGrid() {
		CommonValidator validation = new CommonValidator();
		try {
			DTDObject dtdObject = new DTDObject();
			dtdObject.addColumn(0, "SL");
			dtdObject.addColumn(1, "CHQ_INSTRUMENT_NO");
			dtdObject.addColumn(2, "CHQ_DATE");
			dtdObject.addColumn(3, "CHQ_AMOUNT");
			dtdObject.addColumn(4, "AMOUNT");
			dtdObject.setXML(xmleleasepdcGrid);
			if (dtdObject.getRowCount() <= 0) {
				getErrorMap().setError("chqInstrumentNo", BackOfficeErrorCodes.ATLEAST_ONE_ROW);
				return false;
			}
			if (dtdObject.getRowCount() != Integer.parseInt(noOfChque)) {
				getErrorMap().setError("chqInstrumentNo", BackOfficeErrorCodes.CHEQUE_GRID_VALUE_SAME);
				return false;
			}
			if (!validation.checkDuplicateRows(dtdObject, 1)) {
				getErrorMap().setError("chqInstrumentNo", BackOfficeErrorCodes.DUPLICATE_DATA);
				return false;
			}
			for (int i = 0; i < dtdObject.getRowCount(); i++) {
				if (!validateChqInstrumentNo(dtdObject.getValue(i, 1))) {
					getErrorMap().setError("chqInstrumentNo", BackOfficeErrorCodes.INVALID_ACTION);
					return false;
				}
				if (!validateChqDate(dtdObject.getValue(i, 2))) {
					getErrorMap().setError("chqDate", BackOfficeErrorCodes.INVALID_ACTION);
					return false;
				}
				if (!validateChqAmountFormat(dtdObject.getValue(i, 4))) {
					getErrorMap().setError("chqAmountFormat", BackOfficeErrorCodes.INVALID_ACTION);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("chqInstrumentNo", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateChqInstrumentNo(String chqInstrumentNo) {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(chqInstrumentNo)) {
				getErrorMap().setError("chqInstrumentNo", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (validation.isZero(chqInstrumentNo)) {
				getErrorMap().setError("chqInstrumentNo", BackOfficeErrorCodes.ZERO_CHECK);
				return false;
			}
			if (!validation.isValidNumber(chqInstrumentNo)) {
				getErrorMap().setError("chqInstrumentNo", BackOfficeErrorCodes.NUMERIC_CHECK);
				return false;
			}
			if (validation.isValidNegativeNumber(chqInstrumentNo)) {
				getErrorMap().setError("chqInstrumentNo", BackOfficeErrorCodes.POSITIVE_CHECK);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("chqInstrumentNo", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateChqDate(String chqDate) {
		CommonValidator validator = new CommonValidator();
		try {
			java.util.Date dateInstance = validator.isValidDate(chqDate);
			if (validator.isEmpty(chqDate)) {
				getErrorMap().setError("chqDate", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (dateInstance == null) {
				getErrorMap().setError("chqDate", BackOfficeErrorCodes.INVALID_DATE);
				return false;
			}
			if (!validator.isDateGreaterThanCBD(chqDate)) {
				getErrorMap().setError("chqDate", BackOfficeErrorCodes.DATE_LECBD);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("chqDate", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return false;
	}

	public boolean validateChqAmountFormat(String chqAmount) {
		CommonValidator validator = new CommonValidator();
		DTObject formDTO = new DTObject();
		try {
			if (validator.isEmpty(chqAmount)) {
				getErrorMap().setError("chqAmountFormat", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (validator.isZeroAmount(chqAmount)) {
				getErrorMap().setError("chqAmountFormat", BackOfficeErrorCodes.AMOUNT_NOT_ZERO);
				return false;
			}
			formDTO.set("CURRENCY_CODE", chqAmount_curr);
			formDTO.set(ProgramConstants.AMOUNT, chqAmount);
			formDTO = validator.validateCurrencySmallAmount(formDTO);
			if (formDTO.get(ContentManager.ERROR) != RegularConstants.NULL) {
				getErrorMap().setError("chqAmountFormat", formDTO.get(ContentManager.ERROR));
				return false;
			}
			if (validator.isValidNegativeAmount(chqAmount)) {
				getErrorMap().setError("chqAmountFormat", BackOfficeErrorCodes.AMOUNT_POSITIVE);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("chqAmountFormat", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return false;
	}

}