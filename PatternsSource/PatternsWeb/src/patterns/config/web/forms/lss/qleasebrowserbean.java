package patterns.config.web.forms.lss;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.ResultSet;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.framework.web.DHTMLXGridUtility;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.framework.web.reports.ReportRM;
import patterns.config.validations.AccessValidator;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.InvoiceValidator;
import patterns.config.validations.LeaseValidator;
import patterns.config.validations.MasterValidator;
import patterns.config.validations.RegistrationValidator;
import patterns.config.web.forms.GenericFormBean;

/**
 * 
 * 
 * @version 1.0
 * @author Kalimuthu U
 * @since 24-MAR-2017 <br>
 *        <b>QLEASEBROWSER</b> <BR>
 *        <U><B> Query on Lease Browser </B></U> <br>
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 */
public class qleasebrowserbean extends GenericFormBean {

	private static final long serialVersionUID = 1L;
	private String lesseCode;
	private String leaseProductCode;
	private String shippingState;
	private String lessorState;
	private String dateOfCommencement;
	private String invoiceCycleNumber;
	

	public String getLesseCode() {
		return lesseCode;
	}

	public void setLesseCode(String lesseCode) {
		this.lesseCode = lesseCode;
	}

	public String getLeaseProductCode() {
		return leaseProductCode;
	}

	public void setLeaseProductCode(String leaseProductCode) {
		this.leaseProductCode = leaseProductCode;
	}

	public String getShippingState() {
		return shippingState;
	}

	public void setShippingState(String shippingState) {
		this.shippingState = shippingState;
	}

	public String getLessorState() {
		return lessorState;
	}

	public void setLessorState(String lessorState) {
		this.lessorState = lessorState;
	}

	public String getDateOfCommencement() {
		return dateOfCommencement;
	}

	public void setDateOfCommencement(String dateOfCommencement) {
		this.dateOfCommencement = dateOfCommencement;
	}

	public String getInvoiceCycleNumber() {
		return invoiceCycleNumber;
	}

	public void setInvoiceCycleNumber(String invoiceCycleNumber) {
		this.invoiceCycleNumber = invoiceCycleNumber;
	}

	

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void reset() {
		lesseCode = RegularConstants.EMPTY_STRING;
		leaseProductCode = RegularConstants.EMPTY_STRING;
		shippingState = RegularConstants.EMPTY_STRING;
		lessorState = RegularConstants.EMPTY_STRING;
		dateOfCommencement = RegularConstants.EMPTY_STRING;
		invoiceCycleNumber = RegularConstants.EMPTY_STRING;
		
	}

	public void validate() {
	}

	public DTObject validate(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		if (!validateLesseCode()) {
			resultDTO.set(ContentManager.ERROR, getErrorMap().getErrorKey("lesseCode"));
			resultDTO.set(ContentManager.ERROR_FIELD, "lesseCode");
			return resultDTO;
		}
		if (!validateLeaseProductCode()) {
			resultDTO.set(ContentManager.ERROR, getErrorMap().getErrorKey("leaseProductCode"));
			resultDTO.set(ContentManager.ERROR_FIELD, "leaseProductCode");
			return resultDTO;
		}
		if (!validateShippingState()) {
			resultDTO.set(ContentManager.ERROR, getErrorMap().getErrorKey("shippingState"));
			resultDTO.set(ContentManager.ERROR_FIELD, "shippingState");
			return resultDTO;
		}
		if (!validateLessorState()) {
			resultDTO.set(ContentManager.ERROR, getErrorMap().getErrorKey("lessorState"));
			resultDTO.set(ContentManager.ERROR_FIELD, "lessorState");
			return resultDTO;
		}
		if (!validateDateOfCommencement()) {
			resultDTO.set(ContentManager.ERROR, getErrorMap().getErrorKey("dateOfCommencement"));
			resultDTO.set(ContentManager.ERROR_FIELD, "dateOfCommencement");
			return resultDTO;
		}
		if (!validateInvoiceCycleNumber()) {
			resultDTO.set(ContentManager.ERROR, getErrorMap().getErrorKey("invoiceCycleNumber"));
			resultDTO.set(ContentManager.ERROR_FIELD, "invoiceCycleNumber");
			return resultDTO;
		}
		return resultDTO;
	}

	private boolean validateLeaseProductCode() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("LEASE_PRODUCT_CODE", leaseProductCode);
			formDTO.set(ContentManager.USER_ACTION, USAGE);
			formDTO = validateLeaseProductCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("leaseProductCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("leaseProductCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateLeaseProductCode(DTObject inputDTO) {
		String productCode = inputDTO.get("LEASE_PRODUCT_CODE");
		DTObject resultDTO = new DTObject();
		if (!productCode.equals(RegularConstants.EMPTY_STRING)) {
			resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
			MasterValidator validation = new MasterValidator();
			try {
				inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
				resultDTO = validation.validateLeaseProductCode(inputDTO);
				if (resultDTO.get(ContentManager.ERROR) != null) {
					resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
					return resultDTO;
				}
				if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
					return resultDTO;
				}
			} catch (Exception e) {
				e.printStackTrace();
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			} finally {
				validation.close();
			}
		}
		return resultDTO;
	}

	private boolean validateLesseCode() {
		MasterValidator validation = new MasterValidator();
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("SUNDRY_DB_AC", lesseCode);
			formDTO.set(ContentManager.USER_ACTION, USAGE);
			formDTO = validateLesseCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("lesseCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("lesseCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public DTObject validateLesseCode(DTObject inputDTO) {
		String lesseCode = inputDTO.get("SUNDRY_DB_AC");
		DTObject resultDTO = new DTObject();
		if (!lesseCode.equals(RegularConstants.EMPTY_STRING)) {
			resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
			LeaseValidator validation = new LeaseValidator();
			RegistrationValidator validatior = new RegistrationValidator();
			try {
				inputDTO.set(ContentManager.FETCH, ContentManager.FETCH_ALL);
				resultDTO = validation.valildateLesseCode(inputDTO);
				if (resultDTO.get(ContentManager.ERROR) != RegularConstants.NULL) {
					resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
					return resultDTO;
				}
				inputDTO.set("CUSTOMER_ID", resultDTO.get("CUSTOMER_ID"));
				inputDTO.set(ContentManager.FETCH_COLUMNS, "CUSTOMER_ID,CUSTOMER_NAME");
				resultDTO = validatior.valildateCustomerID(inputDTO);
				if (resultDTO.get(ContentManager.ERROR) != null) {
					resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
					return resultDTO;
				}
				if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
					return resultDTO;
				}
			} catch (Exception e) {
				e.printStackTrace();
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			} finally {
				validation.close();
				validatior.close();
			}
		}
		return resultDTO;
	}

	public boolean validateShippingState() {
		try {
			DTObject formDTO = getFormDTO();
			formDTO.set("STATE_CODE", shippingState);
			formDTO.set(ContentManager.USER_ACTION, USAGE);
			formDTO = validateShippingState(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("shippingState", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("shippingState", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateShippingState(DTObject inputDTO) {
		String stateCode = inputDTO.get("STATE_CODE");
		DTObject resultDTO = new DTObject();
		if (!stateCode.equals(RegularConstants.EMPTY_STRING)) {
			resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
			MasterValidator validation = new MasterValidator();
			try {
				inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
				resultDTO = validation.validateStateCode(inputDTO);
				if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
					return resultDTO;
				}
			} catch (Exception e) {
				e.printStackTrace();
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			} finally {
				validation.close();
			}
		}
		return resultDTO;
	}

	public boolean validateLessorState() {
		try {

			DTObject formDTO = getFormDTO();
			formDTO.set("STATE_CODE", lessorState);
			formDTO.set(ContentManager.USER_ACTION, USAGE);
			formDTO = validateLessorState(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("lessorState", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("lessorState", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateLessorState(DTObject inputDTO) {
		String lessorState = inputDTO.get("STATE_CODE");
		DTObject resultDTO = new DTObject();
		if (!lessorState.equals(RegularConstants.EMPTY_STRING)) {
			resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
			MasterValidator validation = new MasterValidator();
			try {
				inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
				resultDTO = validation.validateStateCode(inputDTO);
				if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
					return resultDTO;
				}
			} catch (Exception e) {
				e.printStackTrace();
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			} finally {
				validation.close();
			}
		}
		return resultDTO;
	}

	public boolean validateDateOfCommencement() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(dateOfCommencement)) {
				getErrorMap().setError("dateOfCommencement", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (validation.isValidDate(dateOfCommencement) == null) {
				getErrorMap().setError("dateOfCommencement", BackOfficeErrorCodes.INVALID_DATE);
				return false;
			}
			if (!validation.isDateLesserEqualCBD(dateOfCommencement)) {
				getErrorMap().setError("dateOfCommencement", BackOfficeErrorCodes.GREATER_CBD_DATE);
				return false;
			}

			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("dateOfCommencement", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateInvoiceCycleNumber() {
		try {
			DTObject formDTO = getFormDTO();
			formDTO.set("INV_CYCLE_NUMBER", invoiceCycleNumber);
			formDTO.set(ContentManager.USER_ACTION, USAGE);
			formDTO = validateInvoiceCycleNumber(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("invoiceCycleNumber", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("invoiceCycleNumber", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateInvoiceCycleNumber(DTObject inputDTO) {
		String invoiceCycleNumber = inputDTO.get("INV_CYCLE_NUMBER");
		DTObject resultDTO = new DTObject();
		if (!invoiceCycleNumber.equals(RegularConstants.EMPTY_STRING)) {
			resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
			InvoiceValidator validation = new InvoiceValidator();
			try {
				inputDTO.set(ContentManager.FETCH_ALL, ContentManager.FETCH_COLUMNS);
				resultDTO = validation.validateInvoiceCycleNumber(inputDTO);
				if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
					return resultDTO;
				}
			} catch (Exception e) {
				e.printStackTrace();
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			} finally {
				validation.close();
			}
		}
		return resultDTO;
	}

	public DTObject getLeaseBrowserDetails(DTObject inputDTO) {
		int i = 1;
		int j = 1;
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.NULL);
		setLesseCode(inputDTO.get("LESSEE_CODE"));
		setLeaseProductCode(inputDTO.get("LEASE_PRODUCT_CODE"));
		setLessorState(inputDTO.get("LESSOR_STATE_CODE"));
		setShippingState(inputDTO.get("SHIPPING_STATE_CODE"));
		setDateOfCommencement(inputDTO.get("DATE_OF_COMMENCEMENT"));
		setInvoiceCycleNumber(inputDTO.get("INV_CYCLE_NUMBER"));
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		AccessValidator validator = new AccessValidator();
		StringBuffer sqlQuery = new StringBuffer();
		try {
			DTObject validateDTO=validate(inputDTO);
			if(validateDTO.get(ContentManager.ERROR)!=null){
				return validateDTO;
			}
			
			util.reset();
			sqlQuery.append("SELECT L.ENTITY_CODE,L.LESSEE_CODE,L.AGREEMENT_NO,L.SCHEDULE_ID,C.CUSTOMER_NAME,L.CUSTOMER_ID,L.CONTRACT_SL,TO_CHAR(L.DATE_OF_COMMENCEMENT,'" + context.getDateFormat() + "')DATE_OF_COMMENCEMENT,L.LEASE_PRODUCT_CODE,L.COMMENCEMENT_VALUE,L.ASSET_CCY,TO_CHAR(L.LEASE_CLOSURE_ON,'"
					+ context.getDateFormat() + "')LEASE_CLOSURE_ON,TO_CHAR(L.CONTRACT_DATE,'" + context.getDateFormat() + "')CONTRACT_DATE,");
			sqlQuery.append("CM.LOV_LABEL_EN_US REPAYFREQUENCY,CML.LOV_LABEL_EN_US ADVANCEARREARS,L.INV_CYCLE_NUMBER,CASE WHEN L.CONTRACT_DATE IS NOT NULL THEN 'YES' ELSE '' END CONTRACT_DATE_YES ");
			sqlQuery.append("FROM LEASE L INNER JOIN CUSTOMER C ON C.ENTITY_CODE=L.ENTITY_CODE AND C.CUSTOMER_ID=L.CUSTOMER_ID JOIN CMLOVREC CM ON CM.PGM_ID='COMMON' AND CM.LOV_ID='REPAY_FREQUENCY' AND CM.LOV_VALUE=L.REPAY_FREQ ");
			sqlQuery.append("JOIN CMLOVREC CML ON CML.PGM_ID='COMMON' AND CML.LOV_ID='ADVANCE_ARREARS' AND CML.LOV_VALUE=L.ADVANCE_ARREARS_FLAG ");
			sqlQuery.append("LEFT OUTER JOIN CUSTOMERADDRESS CU ON CU.ENTITY_CODE=L.ENTITY_CODE AND CU.CUSTOMER_ID=L.CUSTOMER_ID AND CU.ADDR_SL=L.SHIP_ADDR_SL ");
			sqlQuery.append("WHERE L.ENTITY_CODE=?");
			if (!lesseCode.equals(RegularConstants.EMPTY_STRING)) {
				sqlQuery.append(" AND L.LESSEE_CODE=?");
			}
			if (!leaseProductCode.equals(RegularConstants.EMPTY_STRING)) {
				sqlQuery.append(" AND L.LEASE_PRODUCT_CODE=?");
			}
			if (!lessorState.equals(RegularConstants.EMPTY_STRING)) {
				sqlQuery.append(" AND L.LESSOR_STATE_CODE=?");
			}
			if (!shippingState.equals(RegularConstants.EMPTY_STRING)) {
				sqlQuery.append(" AND CU.STATE_CODE=?");
			}
			if (!dateOfCommencement.equals(RegularConstants.EMPTY_STRING)) {
				sqlQuery.append(" AND L.DATE_OF_COMMENCEMENT >=?");
			}
			if (!invoiceCycleNumber.equals(RegularConstants.EMPTY_STRING)) {
				sqlQuery.append(" AND L.INV_CYCLE_NUMBER=?");
			}
			util.setSql(sqlQuery.toString());
			util.setLong(i, Long.parseLong(context.getEntityCode()));
			if (!lesseCode.equals(RegularConstants.EMPTY_STRING)) {
				i++;
				util.setString(i, lesseCode);
			}
			if (!leaseProductCode.equals(RegularConstants.EMPTY_STRING)) {
				i++;
				util.setString(i, leaseProductCode);
			}
			if (!lessorState.equals(RegularConstants.EMPTY_STRING)) {
				i++;
				util.setString(i, lessorState);
			}
			if (!shippingState.equals(RegularConstants.EMPTY_STRING)) {
				i++;
				util.setString(i, shippingState);
			}
			if (!dateOfCommencement.equals(RegularConstants.EMPTY_STRING)) {
				i++;
				Date dateOfCommencementDate = new java.sql.Date(BackOfficeFormatUtils.getDate(dateOfCommencement, BackOfficeConstants.TBA_DATE_FORMAT).getTime());
				util.setDate(i, dateOfCommencementDate);
			}
			if (!invoiceCycleNumber.equals(RegularConstants.EMPTY_STRING)) {
				i++;
				util.setString(i, invoiceCycleNumber);
			}
			ResultSet rset = util.executeQuery();
			DHTMLXGridUtility dhtmlUtility = new DHTMLXGridUtility();
			dhtmlUtility.init();
			if(rset.next()){
					do{
						if (rset.getString("LEASE_CLOSURE_ON") != RegularConstants.NULL) {
							dhtmlUtility.startRow(String.valueOf(j), "color:red");
						} else {
							dhtmlUtility.startRow(String.valueOf(j));
						}
						dhtmlUtility.setCell(rset.getString("LESSEE_CODE") + "^javascript:qcustomerreg(\"" + rset.getLong("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + rset.getString("CUSTOMER_ID") + "\")");
						dhtmlUtility.setCell(rset.getString("AGREEMENT_NO") + "^javascript:qcustagreement(\"" + rset.getLong("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + rset.getString("CUSTOMER_ID") + RegularConstants.PK_SEPARATOR + rset.getString("AGREEMENT_NO") + "\")");
						dhtmlUtility.setCell(rset.getString("SCHEDULE_ID") + "^javascript:qlease(\"" + rset.getLong("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + rset.getString("LESSEE_CODE") + RegularConstants.PK_SEPARATOR + rset.getString("AGREEMENT_NO") + RegularConstants.PK_SEPARATOR + rset.getString("SCHEDULE_ID") + "\")");
						dhtmlUtility.setCell(rset.getString("CUSTOMER_NAME"));
						dhtmlUtility.setCell(rset.getString("DATE_OF_COMMENCEMENT"));
						dhtmlUtility.setCell(rset.getString("LEASE_PRODUCT_CODE"));
						dhtmlUtility.setCell(rset.getString("ASSET_CCY"));
						dhtmlUtility.setCell(ReportRM.fmc(new BigDecimal(rset.getString("COMMENCEMENT_VALUE")), context.getBaseCurrency(), context.getBaseCurrencyUnits())); 
						dhtmlUtility.setCell(rset.getString("REPAYFREQUENCY"));
						dhtmlUtility.setCell(rset.getString("ADVANCEARREARS"));
						dhtmlUtility.setCell(rset.getString("INV_CYCLE_NUMBER"));
						dhtmlUtility.setCell(rset.getString("CONTRACT_DATE_YES") + "^javascript:qplcdetailsview(\"" + rset.getLong("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + rset.getString("CONTRACT_DATE") + RegularConstants.PK_SEPARATOR + rset.getString("CONTRACT_SL") + "\")");
						dhtmlUtility.endRow();
						j++;
					}while (rset.next());
					dhtmlUtility.finish();
					String resultXML = dhtmlUtility.getXML();
					resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
					resultDTO.set(ContentManager.RESULT_XML, resultXML);
			}else{
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			util.reset();
			validator.close();
			dbContext.close();
		}
		return resultDTO;
	}
}
