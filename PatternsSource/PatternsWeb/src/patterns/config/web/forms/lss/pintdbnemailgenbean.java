package patterns.config.web.forms.lss;

import java.sql.ResultSet;
import java.util.ArrayList;

import patterns.config.delegate.BackOfficeProcessManager;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.process.TBAProcessResult;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTDObject;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.DHTMLXGridUtility;
import patterns.config.framework.web.GenericOption;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.MasterValidator;
/*
 * E-Mail Generation for Debit Note 
 *
 Author : Pavan kumar.R
 Created Date : 21-March-2017
 Spec Reference PSD-LSS-
 Modification History
 ----------------------------------------------------------------------
 Sl.No		Modified Date	Author			Modified Changes	Version

 ----------------------------------------------------------------------

 */
import patterns.config.web.forms.GenericFormBean;

public class pintdbnemailgenbean extends GenericFormBean {

	private static final long serialVersionUID = -6219657363150466160L;
	private String intMonth;
	private String intMonthYear;

	public String getIntMonth() {
		return intMonth;
	}

	public void setIntMonth(String intMonth) {
		this.intMonth = intMonth;
	}

	public String getIntMonthYear() {
		return intMonthYear;
	}

	public void setIntMonthYear(String intMonthYear) {
		this.intMonthYear = intMonthYear;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void validate() {
	}

	@Override
	public void reset() {
		intMonth = RegularConstants.EMPTY_STRING;
		intMonthYear = RegularConstants.EMPTY_STRING;
		DBContext dbContext = new DBContext();
		ArrayList<GenericOption> invMonths = null;
		invMonths = getGenericOptions("COMMON", "MONTHS", dbContext, null);
		webContext.getRequest().setAttribute("COMMON_MONTHS", invMonths);
		dbContext.close();
		setProcessBO("patterns.config.framework.bo.inv.pinvemailgenBO");

	}

	public DTObject validateMonth(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		String invoiceMonth = inputDTO.get("INT_MONTH");
		MasterValidator validator = new MasterValidator();
		try {
			if (validator.isEmpty(invoiceMonth)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			} else if (!validator.isCMLOVREC("COMMON", "MONTHS", invoiceMonth)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			return resultDTO;
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return resultDTO;
	}

	public DTObject validateYear(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		String invoiceYear = inputDTO.get("INT_YEAR");
		MasterValidator validator = new MasterValidator();
		try {
			if (validator.isEmpty(invoiceYear)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			if (!validator.isValidYear((invoiceYear))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_YEAR);
				return resultDTO;
			}
			if (!validator.isNumberGreaterThanEqual(context.getCurrentYear(),invoiceYear)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.YEAR_LE_CALYEAR);
				return resultDTO;
			}
			return resultDTO;
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return resultDTO;
	}

	public DTObject validateFields(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		CommonValidator validation = new CommonValidator();
		inputDTO.set("ERROR_PRESENT", "0");
		try {
			resultDTO = validateMonth(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				inputDTO.set("MONTH_ERROR", getErrorResource(resultDTO.get(ContentManager.ERROR), null));
				inputDTO.set("ERROR_PRESENT", "1");
			}
			resultDTO = validateYear(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				inputDTO.set("YEAR_ERROR", getErrorResource(resultDTO.get(ContentManager.ERROR), null));
				inputDTO.set("ERROR_PRESENT", "1");
			}
			return inputDTO;
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();

		}
		return inputDTO;
	}

	public DTObject loadGrid(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		DBContext dbContext = new DBContext();
		try {
			if (inputDTO.get("REVALIDATE").equals("1")) {
				resultDTO = validateFields(inputDTO);
				if (resultDTO.get("ERROR_PRESENT").equals("1")) {
					return resultDTO;
				}
			}
			DBUtil dbutil = dbContext.createUtilInstance();
			int recordCount = 0;
			StringBuffer sqlQuery = new StringBuffer();
			sqlQuery.append("SELECT 0,T.DEBIT_NOTE_INVENTORY,TO_CHAR(T.CONTRACT_DATE,?) AS CONTRACT_DATE,T.CONTRACT_SL,PLC.SUNDRY_DB_AC,C.CUSTOMER_NAME,CM.LOV_LABEL_EN_US, ");
			sqlQuery.append(" T.SORT_KEY,T.NOF_INT_ENTRIES,T.INTEREST_CCY,FN_FORMAT_AMOUNT(T.TOTAL_INTEREST_AMOUNT,T.INTEREST_CCY,CUR.SUB_CCY_UNITS_IN_CCY) AS TOTAL_INTEREST_AMOUNT,");
			sqlQuery.append(" T.PDF_REPORT_IDENTIFIER,T.PDF_FILE_INV_NUM ,PLC.CUSTOMER_ID,PLC.ADDR_SL,");
			sqlQuery.append(" L.REQUESTED_BY,TO_CHAR(L.REQUESTED_DATETIME,?) AS REQUESTED_DATETIME  ");
			sqlQuery.append(" ,CASE E.STATUS WHEN 'S' THEN 'Success' WHEN 'F' THEN 'Failure' WHEN 'I' THEN 'In Process' END AS STATUS ");
			sqlQuery.append(" FROM PLCINTDBNOTE T ");
			sqlQuery.append(" JOIN PRELEASECONTRACT PLC ON (PLC.ENTITY_CODE=T.ENTITY_CODE AND PLC.CONTRACT_DATE=T.CONTRACT_DATE AND PLC.CONTRACT_SL=T.CONTRACT_SL)");
			sqlQuery.append(" JOIN CUSTOMER C ON (C.ENTITY_CODE=PLC.ENTITY_CODE AND C.CUSTOMER_ID=PLC.CUSTOMER_ID) ");
			sqlQuery.append(" JOIN CMLOVREC CM ON (CM.PGM_ID='EPRELEASECONTREG' AND CM.LOV_ID='INT_DEBIT_PRINTING' AND CM.LOV_VALUE=PLC.INT_DEBIT_NOTE_PRINT_CHOICE)");
			sqlQuery.append(" JOIN CURRENCY CUR ON (CUR.CCY_CODE=T.INTEREST_CCY)");
			sqlQuery.append(" LEFT OUTER JOIN PLCINTDBNOTEEMAILSENDLOG L ON (T.ENTITY_CODE=L.ENTITY_CODE AND T.DEBIT_NOTE_INVENTORY=L.DEBIT_NOTE_INVENTORY ");
			sqlQuery.append(" AND L.DTL_SL= (SELECT MAX(DTL_SL) FROM PLCINTDBNOTEEMAILSENDLOG WHERE T.ENTITY_CODE=ENTITY_CODE AND T.DEBIT_NOTE_INVENTORY=DEBIT_NOTE_INVENTORY ))");
			sqlQuery.append(" LEFT OUTER JOIN EMAILOUTQLOG E ON (E.ENTITY_CODE=T.ENTITY_CODE AND E.INVENTORY_NO=L.EMAIL_LOG_INVENTORY_NO ) ");
			sqlQuery.append(" WHERE T.ENTITY_CODE =? AND T.FOR_YEAR=? AND T.FOR_MONTH=? ");
			dbutil.reset();
			dbutil.setMode(DBUtil.PREPARED);
			dbutil.setSql(sqlQuery.toString());
			dbutil.setString(1, context.getDateFormat());
			dbutil.setString(2, context.getDateFormat() + " " + RegularConstants.TIME_FORMAT);
			dbutil.setLong(3, Long.parseLong(context.getEntityCode()));
			dbutil.setInt(4, Integer.parseInt(inputDTO.get("INT_YEAR")));
			dbutil.setInt(5, Integer.parseInt(inputDTO.get("INT_MONTH")));
			ResultSet rsetGrid = dbutil.executeQuery();
			DHTMLXGridUtility gridUtility = new DHTMLXGridUtility();
			gridUtility.init();
			while (rsetGrid.next()) {
				recordCount = recordCount + 1;
				gridUtility.startRow(String.valueOf(rsetGrid.getString("DEBIT_NOTE_INVENTORY")));
				gridUtility.setCell(RegularConstants.EMPTY_STRING);
				gridUtility.setCell(rsetGrid.getString("CONTRACT_DATE"));
				gridUtility.setCell(rsetGrid.getString("CONTRACT_SL"));
				gridUtility.setCell(rsetGrid.getString("SUNDRY_DB_AC"));
				gridUtility.setCell(rsetGrid.getString("CUSTOMER_NAME"));
				gridUtility.setCell(rsetGrid.getString("LOV_LABEL_EN_US"));
				gridUtility.setCell(rsetGrid.getString("SORT_KEY"));
				gridUtility.setCell(rsetGrid.getString("NOF_INT_ENTRIES"));
				gridUtility.setCell(rsetGrid.getString("INTEREST_CCY"));
				gridUtility.setCell(rsetGrid.getString("TOTAL_INTEREST_AMOUNT"));
				gridUtility.setCell(rsetGrid.getString("DEBIT_NOTE_INVENTORY"));
				gridUtility.setCell(rsetGrid.getString("PDF_REPORT_IDENTIFIER"));
				gridUtility.setCell(rsetGrid.getString("PDF_FILE_INV_NUM"));
				gridUtility.setCell(rsetGrid.getString("CUSTOMER_ID"));
				gridUtility.setCell(rsetGrid.getString("ADDR_SL"));

				if (rsetGrid.getString("REQUESTED_BY") != null)
					gridUtility.setCell(rsetGrid.getString("REQUESTED_BY") + "^javascript:viewSentLogDetails(\"" + rsetGrid.getString("DEBIT_NOTE_INVENTORY") + "\")");
				else
					gridUtility.setCell("");
				if (rsetGrid.getString("REQUESTED_DATETIME") != null)
					gridUtility.setCell("" + rsetGrid.getString("REQUESTED_DATETIME") + "^javascript:viewSentLogDetails(\"" + rsetGrid.getString("DEBIT_NOTE_INVENTORY") + "\")");
				else
					gridUtility.setCell("");
				gridUtility.setCell(rsetGrid.getString("STATUS"));

				gridUtility.endRow();

			}
			if (recordCount != 0) {
				gridUtility.finish();
				String resultXML = gridUtility.getXML();
				resultDTO.set(ContentManager.RESULT_XML, resultXML);
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
			} else {
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
				return resultDTO;
			}
			dbutil.reset();
			return resultDTO;
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			dbContext.close();
		}
		return resultDTO;
	}

	public DTObject loadComponentDetails(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		DBContext dbContext = new DBContext();
		try {
			DBUtil dbutil = dbContext.createUtilInstance();
			int recordCount = 0;
			StringBuffer sqlQuery = new StringBuffer();
			sqlQuery.append(" SELECT NB.PAYMENT_SL,SU.NAME,TO_CHAR(LED.FROM_DATE,?) AS FROM_DATE,TO_CHAR(LED.UPTO_DATE,?) AS UPTO_DATE,LED.NOF_DAYS,LED.INTEREST_RATE,LPOP.PAYMENT_CCY, ");
			sqlQuery.append(" FN_FORMAT_AMOUNT(LPOP.PAYMENT_AMOUNT,LPOP.PAYMENT_CCY,C.SUB_CCY_UNITS_IN_CCY)PAYMENT_AMOUNT,LED.INTEREST_CCY,FN_FORMAT_AMOUNT(LED.INTEREST_AMOUNT,LED.INTEREST_CCY,CU.SUB_CCY_UNITS_IN_CCY)INTEREST_AMOUNT");
			sqlQuery.append(" FROM PLCINTDBNOTE N");
			sqlQuery.append(" JOIN PLCINTDBNOTEBRKUP NB ON (NB.ENTITY_CODE=N.ENTITY_CODE AND NB.DEBIT_NOTE_INVENTORY=N.DEBIT_NOTE_INVENTORY) ");
			sqlQuery.append(" JOIN PLCINTERESTLED LED ON (LED.ENTITY_CODE=N.ENTITY_CODE AND LED.CONTRACT_DATE=N.CONTRACT_DATE AND LED.CONTRACT_SL=N.CONTRACT_SL AND ");
			sqlQuery.append("		LED.FOR_YEAR=NB.FOR_YEAR AND LED.FOR_MONTH=NB.FOR_MONTH AND LED.PAYMENT_SL=NB.PAYMENT_SL)");
			sqlQuery.append(" JOIN LEASEPOINVPAY LPOP ON (LPOP.ENTITY_CODE=LED.ENTITY_CODE AND LPOP.CONTRACT_DATE=LED.CONTRACT_DATE AND LPOP.CONTRACT_SL=LED.CONTRACT_SL AND LPOP.PAYMENT_SL=LED.PAYMENT_SL)");
			sqlQuery.append(" JOIN LEASEPO LPO ON  (LPO.ENTITY_CODE=LPOP.ENTITY_CODE AND LPO.CONTRACT_DATE=LPOP.CONTRACT_DATE AND LPO.CONTRACT_SL=LPOP.CONTRACT_SL AND LPO.PO_SL=LPOP.PO_SL)");
			sqlQuery.append(" JOIN SUPPLIER SU ON (SU.ENTITY_CODE=LPO.ENTITY_CODE AND SU.SUPPLIER_ID=LPO.SUPPLIER_ID)");
			sqlQuery.append(" JOIN CURRENCY C ON (C.CCY_CODE=LPOP.PAYMENT_CCY ) ");
			sqlQuery.append(" JOIN CURRENCY CU ON (CU.CCY_CODE=LED.INTEREST_CCY) ");
			sqlQuery.append(" WHERE N.ENTITY_CODE=? AND N.DEBIT_NOTE_INVENTORY=? ");
			dbutil.reset();
			dbutil.setMode(DBUtil.PREPARED);
			dbutil.setSql(sqlQuery.toString());
			dbutil.setString(1, context.getDateFormat());
			dbutil.setString(2, context.getDateFormat());
			dbutil.setLong(3, Long.parseLong(context.getEntityCode()));
			dbutil.setLong(4, Long.parseLong(inputDTO.get("DEBIT_NOTE_INVENTORY")));
			ResultSet rsetGrid = dbutil.executeQuery();
			DHTMLXGridUtility gridUtility = new DHTMLXGridUtility();
			gridUtility.init();
			while (rsetGrid.next()) {
				recordCount = recordCount + 1;
				gridUtility.startRow(String.valueOf(recordCount));
				gridUtility.setCell(rsetGrid.getString("PAYMENT_SL"));
				gridUtility.setCell(rsetGrid.getString("NAME"));
				gridUtility.setCell(rsetGrid.getString("FROM_DATE"));
				gridUtility.setCell(rsetGrid.getString("UPTO_DATE"));
				gridUtility.setCell(rsetGrid.getString("NOF_DAYS"));
				gridUtility.setCell(rsetGrid.getString("INTEREST_RATE"));
				gridUtility.setCell(rsetGrid.getString("PAYMENT_CCY"));
				gridUtility.setCell(rsetGrid.getString("PAYMENT_AMOUNT"));
				gridUtility.setCell(rsetGrid.getString("INTEREST_CCY"));
				gridUtility.setCell(rsetGrid.getString("INTEREST_AMOUNT"));
				gridUtility.endRow();
			}
			if (recordCount != 0) {
				gridUtility.finish();
				String resultXML = gridUtility.getXML();
				resultDTO.set(ContentManager.RESULT_XML, resultXML);
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
			} else {
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
				return resultDTO;
			}
			dbutil.reset();
			return resultDTO;
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			dbContext.close();
		}
		return resultDTO;
	}

	public DTObject processAction(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		try {
			setProcessBO("patterns.config.framework.bo.lss.pintdbnemailgenBO");
			DTObject formDTO = getFormDTO();
			formDTO.set("ENTITY_CODE", context.getEntityCode());
			formDTO.set("USER_ID", context.getUserID());
			formDTO.set("FOR_YEAR", inputDTO.get("INT_YEAR"));
			formDTO.set("FOR_MONTH", inputDTO.get("INT_MONTH"));

			formDTO.setObject("CBD", context.getCurrentBusinessDate().getTime());
			formDTO.setDateFormat(context.getDateFormat());
			DTDObject dtdObject = new DTDObject();
			dtdObject.addColumn(0, "SELECT");
			dtdObject.addColumn(1, "CONTRACT_DATE");
			dtdObject.addColumn(2, "CONTRACT_SL");
			dtdObject.addColumn(3, "SUNDRY_DB_AC");
			dtdObject.addColumn(4, "CUSTOMER_NAME");
			dtdObject.addColumn(5, "CONSOLIDATED_BASIS");
			dtdObject.addColumn(6, "SORT_KEY");
			dtdObject.addColumn(7, "NOF_INT_ENTRIES");
			dtdObject.addColumn(8, "INTEREST_CCY");
			dtdObject.addColumn(9, "TOTAL_INTEREST_AMOUNT");
			dtdObject.addColumn(10, "DEBIT_NOTE_INVENTORY");
			dtdObject.addColumn(11, "PDF_REPORT_IDENTIFIER");
			dtdObject.addColumn(12, "PDF_FILE_INV_NUM");
			dtdObject.addColumn(11, "PDF_REPORT_IDENTIFIER");
			dtdObject.addColumn(12, "PDF_FILE_INV_NUM");
			dtdObject.addColumn(13, "CUSTOMER_ID");
			dtdObject.addColumn(14, "ADDR_SL");
			dtdObject.setDateFormat(context.getDateFormat());
			dtdObject.setXML(inputDTO.get("XML"));
			formDTO.setDTDObject("PINTDBNEMAILGENDTL", dtdObject);
			formDTO.set(RegularConstants.PROCESSBO_CLASS, getProcessBO());
			BackOfficeProcessManager processManager = new BackOfficeProcessManager();
			TBAProcessResult processResult = processManager.delegate(formDTO);
			if (processResult.getProcessStatus().equals(TBAProcessStatus.SUCCESS)) {
				resultDTO = loadGrid(inputDTO);
				return resultDTO;
			} else {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return resultDTO;
	}

	public DTObject viewSentLogDetails(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		DBContext dbContext = new DBContext();
		try {
			DBUtil dbutil = dbContext.createUtilInstance();
			int recordCount = 0;
			StringBuffer sqlQuery = new StringBuffer();
			sqlQuery.append("SELECT L.REQUESTED_BY,TO_CHAR(L.REQUESTED_DATETIME , ?) AS TO_REQUESTED_DATETIME");
			sqlQuery.append(",TO_CHAR(E.SENT_DATE , ?) AS SENT_DATE");
			sqlQuery.append(",CASE E.STATUS WHEN 'S' THEN 'Success' WHEN 'F' THEN 'Failure' WHEN 'I' THEN 'In Process' END AS STATUS,E.INVENTORY_NO ");
			sqlQuery.append(" FROM PLCINTDBNOTEEMAILSENDLOG L ");
			sqlQuery.append(" INNER JOIN EMAILOUTQLOG E ON (E.ENTITY_CODE=L.ENTITY_CODE AND E.INVENTORY_NO=L.EMAIL_LOG_INVENTORY_NO)");
			sqlQuery.append(" WHERE L.ENTITY_CODE=? AND L.DEBIT_NOTE_INVENTORY=? ORDER BY L.REQUESTED_DATETIME DESC");
			dbutil.reset();
			dbutil.setMode(DBUtil.PREPARED);
			dbutil.setSql(sqlQuery.toString());
			dbutil.setString(1, context.getDateFormat() + " " + RegularConstants.TIME_FORMAT);
			dbutil.setString(2, context.getDateFormat() + " " + RegularConstants.TIME_FORMAT);
			dbutil.setLong(3, Long.parseLong(context.getEntityCode()));
			dbutil.setString(4, inputDTO.get("DEBIT_NOTE_INVENTORY"));
			ResultSet rsetGrid = dbutil.executeQuery();
			DHTMLXGridUtility gridUtility = new DHTMLXGridUtility();
			gridUtility.init();
			while (rsetGrid.next()) {
				recordCount = recordCount + 1;
				gridUtility.startRow(String.valueOf(recordCount));
				gridUtility.setCell(String.valueOf(recordCount));
				gridUtility.setCell(rsetGrid.getString("REQUESTED_BY"));
				gridUtility.setCell(rsetGrid.getString("TO_REQUESTED_DATETIME"));
				gridUtility.setCell(rsetGrid.getString("SENT_DATE"));
				gridUtility.setCell(rsetGrid.getString("STATUS"));
				gridUtility.setCell("View^javascript:viewMailDetails(\"" + rsetGrid.getString("INVENTORY_NO") + "\")");
				gridUtility.endRow();
			}
			if (recordCount != 0) {
				gridUtility.finish();
				String resultXML = gridUtility.getXML();
				resultDTO.set(ContentManager.RESULT_XML, resultXML);
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
			} else {
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
				return resultDTO;
			}
			gridUtility.init();
			return resultDTO;
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			dbContext.close();
		}
		return resultDTO;
	}

	public DTObject viewMailDetails(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		MasterValidator validation = new MasterValidator();
		DBUtil dbutil = validation.getDbContext().createUtilInstance();
		try {
			dbutil.reset();
			dbutil.setMode(DBUtil.PREPARED);
			dbutil.setSql("SELECT EMAIL_SUBJECT,EMAIL_TEXT,REQ_DATE,TO_CHAR(SENT_DATE , ?) AS SENT_DATE FROM EMAILOUTQLOG WHERE ENTITY_CODE=? AND INVENTORY_NO=?");
			dbutil.setString(1, context.getDateFormat() + " " + RegularConstants.TIME_FORMAT);
			dbutil.setString(2, context.getEntityCode());
			dbutil.setString(3, inputDTO.get("EMAIL_LOG_INVENTORY_NO"));
			ResultSet rt = dbutil.executeQuery();
			if (rt.next()) {
				resultDTO.set("EMAIL_SUBJECT", rt.getString("EMAIL_SUBJECT"));
				resultDTO.set("EMAIL_TEXT", rt.getString("EMAIL_TEXT"));
				resultDTO.set("SENT_DATE", rt.getString("SENT_DATE"));
			}
			StringBuffer sqlQuery = new StringBuffer();
			sqlQuery.append("SELECT R.REPORT_IDENTIFIER,E.ATTACHMENT_NAME FROM EMAILOUTQATTACH E ");
			sqlQuery.append(" INNER JOIN REPORTDOWNLOAD R ON (R.ENTITY_CODE=E.ENTITY_CODE AND R.FILE_PATH=CONCAT(E.ATTACHMENT_PATH,E.ATTACHMENT_NAME))");
			sqlQuery.append(" WHERE E.ENTITY_CODE=? AND E.INVENTORY_NO=? ");
			dbutil.reset();
			dbutil.setMode(DBUtil.PREPARED);
			dbutil.setSql(sqlQuery.toString());
			dbutil.setString(1, context.getEntityCode());
			dbutil.setString(2, inputDTO.get("EMAIL_LOG_INVENTORY_NO"));
			ResultSet rset = dbutil.executeQuery();
			if (rset.next()) {
				resultDTO.set("PDF_REPORT_FILE_NAME", rset.getString("ATTACHMENT_NAME"));
				resultDTO.set("PDF_REPORT_IDENTIFIER", rset.getString("REPORT_IDENTIFIER"));
			}
			int recordCount = 0;
			dbutil.reset();
			dbutil.setMode(DBUtil.PREPARED);
			dbutil.setSql("SELECT E.EMAIL_RCP_TYPE,E.EMAIL_ID FROM EMAILOUTQRCP E WHERE E.ENTITY_CODE=? AND E.INVENTORY_NO=? ORDER BY E.EMAIL_RCP_TYPE DESC");
			dbutil.setString(1, context.getEntityCode());
			dbutil.setString(2, inputDTO.get("EMAIL_LOG_INVENTORY_NO"));
			ResultSet rsetGrid = dbutil.executeQuery();
			DHTMLXGridUtility gridUtility = new DHTMLXGridUtility();
			gridUtility.init();
			while (rsetGrid.next()) {
				recordCount = recordCount + 1;
				gridUtility.startRow(String.valueOf(recordCount));
				gridUtility.setCell(rsetGrid.getString("EMAIL_ID"));
				if (rsetGrid.getString("EMAIL_RCP_TYPE").equals("T"))
					gridUtility.setCell("To");
				else
					gridUtility.setCell("Cc");
				gridUtility.endRow();
			}
			if (recordCount != 0) {
				gridUtility.finish();
				String resultXML = gridUtility.getXML();
				resultDTO.set(ContentManager.RESULT_XML, resultXML);
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
			} else {
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
				return resultDTO;
			}
			return resultDTO;
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			dbutil.reset();
			validation.close();
		}
		return resultDTO;
	}
}
