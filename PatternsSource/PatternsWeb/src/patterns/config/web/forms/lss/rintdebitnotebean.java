package patterns.config.web.forms.lss;

import java.sql.ResultSet;
import java.util.ArrayList;

import patterns.config.delegate.BackOfficeProcessManager;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.process.TBAProcessResult;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.DHTMLXGridUtility;
import patterns.config.framework.web.GenericOption;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.framework.web.reports.ReportUtils;
import patterns.config.validations.CommonValidator;
import patterns.config.web.forms.GenericFormBean;

public class rintdebitnotebean extends GenericFormBean {

	private static final long serialVersionUID = 1L;
	private String month;
	private String monthYear;

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public String getMonthYear() {
		return monthYear;
	}

	public void setMonthYear(String monthYear) {
		this.monthYear = monthYear;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public void reset() {
		month = RegularConstants.EMPTY_STRING;
		monthYear = RegularConstants.EMPTY_STRING;
		DBContext dbContext = new DBContext();
		ArrayList<GenericOption> monthList = null;
		monthList = getGenericOptions("COMMON", "MONTH", dbContext, null);
		webContext.getRequest().setAttribute("COMMON_MONTH", monthList);
		dbContext.close();
	}

	@Override
	public void validate() {

	}

	public DTObject reValidate(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		String month = inputDTO.get("MONTH");
		String year = inputDTO.get("YEAR");
		resultDTO.set("ERROR_PRESENT", RegularConstants.ZERO);
		CommonValidator validation = new CommonValidator();

		try {
			if (validation.isEmpty(month)) {
				resultDTO.set("ERROR_PRESENT", RegularConstants.ONE);
				resultDTO.set("MONTH_ERROR", getErrorResource(BackOfficeErrorCodes.FIELD_BLANK, null));
				return resultDTO;
			}
			if (validation.isEmpty(year)) {
				resultDTO.set("ERROR_PRESENT", RegularConstants.ONE);
				resultDTO.set("YEAR_ERROR", getErrorResource(BackOfficeErrorCodes.FIELD_BLANK, null));
				return resultDTO;
			}
			if (!validation.isValidYear(year)) {
				resultDTO.set("ERROR_PRESENT", RegularConstants.ONE);
				resultDTO.set("YEAR_ERROR", getErrorResource(BackOfficeErrorCodes.INVALID_YEAR, null));
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public DTObject doLoadGrid(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		DBContext dbContext = new DBContext();
		DBUtil dbutil = dbContext.createUtilInstance();
		try {
			resultDTO = reValidate(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null || RegularConstants.ONE.equals(resultDTO.get("ERROR_PRESENT"))) {
				return resultDTO;
			}
			StringBuffer sqlQuery = new StringBuffer();
			sqlQuery.append("SELECT CONCAT(lpad(P.FOR_MONTH,2,0),SUBSTRING(P.FOR_YEAR,3,4),'/',P.DEBIT_NOTE_INVENTORY)AS DEBIT_NOTE_INVENTORY,PLC.SUNDRY_DB_AC,C.CUSTOMER_NAME,");
			sqlQuery.append("TO_CHAR(P.CONTRACT_DATE,?)CONTRACT_DATE,P.CONTRACT_SL,P.INTEREST_CCY,");
			sqlQuery.append("FN_FORMAT_AMOUNT(P.TOTAL_INTEREST_AMOUNT,P.INTEREST_CCY,CUR.SUB_CCY_UNITS_IN_CCY) TOTAL_INTEREST_AMOUNT,");
			sqlQuery.append("P.PDF_REPORT_IDENTIFIER,TO_CHAR(P.DEBIT_NOTE_DATE,?) DEBIT_NOTE_DATE,P.DEBIT_NOTE_INVENTORY AS DEBNOTE,PLC.BRANCH_CODE,P.NOF_INT_ENTRIES");
			sqlQuery.append(" FROM PLCINTDBNOTE P ");
			sqlQuery.append(" JOIN PRELEASECONTRACT PLC ON (PLC.ENTITY_CODE=P.ENTITY_CODE AND PLC.CONTRACT_DATE=P.CONTRACT_DATE AND PLC.CONTRACT_SL=P.CONTRACT_SL)");
			sqlQuery.append(" JOIN CUSTOMER C ON (C.ENTITY_CODE=PLC.ENTITY_CODE AND C.CUSTOMER_ID=PLC.CUSTOMER_ID)");
			sqlQuery.append(" JOIN CURRENCY CUR ON (CUR.CCY_CODE=P.INTEREST_CCY)");
			sqlQuery.append(" WHERE P.ENTITY_CODE=? AND P.DEBIT_NOTE_MODE='M' AND P.FOR_YEAR=? AND P.FOR_MONTH=?");
			dbutil.reset();
			dbutil.setSql(sqlQuery.toString());
			dbutil.setString(1, context.getDateFormat());
			dbutil.setString(2, context.getDateFormat());
			dbutil.setLong(3, Long.parseLong(context.getEntityCode()));
			dbutil.setInt(4, Integer.parseInt(inputDTO.get("YEAR")));
			dbutil.setInt(5, Integer.parseInt(inputDTO.get("MONTH")));
			ResultSet rsetGrid = dbutil.executeQuery();
			DHTMLXGridUtility gridUtility = new DHTMLXGridUtility();
			gridUtility.init();
			int count = 0;
			while (rsetGrid.next()) {
				gridUtility.startRow();
				gridUtility.setCell("0");
				gridUtility.setCell(rsetGrid.getString("DEBIT_NOTE_INVENTORY"));
				gridUtility.setCell(rsetGrid.getString("SUNDRY_DB_AC"));
				gridUtility.setCell(rsetGrid.getString("CUSTOMER_NAME"));
				gridUtility.setCell(rsetGrid.getString("CONTRACT_DATE") + "-" + rsetGrid.getString("CONTRACT_SL"));
				gridUtility.setCell(rsetGrid.getString("INTEREST_CCY"));
				gridUtility.setCell(rsetGrid.getString("TOTAL_INTEREST_AMOUNT"));
				if (rsetGrid.getString("PDF_REPORT_IDENTIFIER") == null || rsetGrid.getString("PDF_REPORT_IDENTIFIER").trim().length() == 0) {
					gridUtility.setCell(RegularConstants.ZERO);
					gridUtility.setCell("");
				} else {
					gridUtility.setCell(RegularConstants.ONE);
					gridUtility.setCell("Download" + "^javascript:downloadReport(\"" + rsetGrid.getString("PDF_REPORT_IDENTIFIER") + "\")");
				}
				gridUtility.setCell(rsetGrid.getString("DEBNOTE"));
				gridUtility.setCell(rsetGrid.getString("DEBIT_NOTE_DATE"));
				gridUtility.setCell(rsetGrid.getString("BRANCH_CODE"));
				gridUtility.setCell(rsetGrid.getString("NOF_INT_ENTRIES"));
				gridUtility.endRow();
				count++;
			}
			if (count > 0) {
				gridUtility.finish();
				String resultXML = gridUtility.getXML();
				resultDTO.set(ContentManager.RESULT_XML, resultXML);
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
			} else {
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
				return resultDTO;
			}
			return resultDTO;
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			dbutil.reset();
			dbContext.close();
		}
		return resultDTO;
	}

	public DTObject doPrint(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		try {
			inputDTO.set(ContentManager.REPORT_HANDLER, "patterns.config.web.reports.lss.rintdebitnotereport");
			inputDTO.set(ContentManager.REPORT_FORMAT, ReportUtils.EXPORT_PDF + "");
			inputDTO.set("ENTITY_CODE", context.getEntityCode());
			inputDTO.set("DATE_FORMAT", context.getDateFormat());
			resultDTO = processDownload(inputDTO);
			// update
			setProcessBO("patterns.config.framework.bo.lss.rintdebitnoteBO");
			inputDTO.set(RegularConstants.PROCESSBO_CLASS, getProcessBO());
			inputDTO.set("PDF_REPORT_IDENTIFIER", resultDTO.get(ContentManager.REPORT_ID));
			if (resultDTO.get(ContentManager.ERROR) == null || resultDTO.get(ContentManager.ERROR).equals(RegularConstants.EMPTY_STRING)) {
				BackOfficeProcessManager processManager = new BackOfficeProcessManager();
				TBAProcessResult processResult = processManager.delegate(inputDTO);
				if (processResult.getProcessStatus().equals(TBAProcessStatus.SUCCESS)) {
					resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
					resultDTO.set("PDF_REPORT_IDENTIFIER", resultDTO.get(ContentManager.REPORT_ID));
					return resultDTO;
				} else {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
				}
			} else {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.REPORT_ERROR);
				return resultDTO;
			}
			// update
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
		}
		return resultDTO;

	}

}
