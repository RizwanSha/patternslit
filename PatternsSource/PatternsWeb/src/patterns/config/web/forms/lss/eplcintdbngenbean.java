/*
 *
 Author : Raja E
 Created Date : 18-03-2017
 Spec Reference : EPLCINTDBNGEN
 Modification History
 ----------------------------------------------------------------------
 Sl.No		Modified Date	Author			Modified Changes	Version
 ----------------------------------------------------------------------

 */

package patterns.config.web.forms.lss;

import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;

import patterns.config.delegate.BackOfficeProcessManager;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.process.TBAProcessResult;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.framework.web.DHTMLXGridUtility;
import patterns.config.framework.web.GenericOption;
import patterns.config.framework.web.RequestConstants;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.framework.web.reports.ReportUtils;
import patterns.config.validations.CommonValidator;
import patterns.config.web.forms.GenericFormBean;

public class eplcintdbngenbean extends GenericFormBean {

	private static final long serialVersionUID = 1L;

	private String leaseMonth;
	private String leaseMonthYear;

	public String getLeaseMonth() {
		return leaseMonth;
	}

	public void setLeaseMonth(String leaseMonth) {
		this.leaseMonth = leaseMonth;
	}

	public String getLeaseMonthYear() {
		return leaseMonthYear;
	}

	public void setLeaseMonthYear(String leaseMonthYear) {
		this.leaseMonthYear = leaseMonthYear;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public void reset() {
		// TODO Auto-generated method stub
		leaseMonth = RegularConstants.EMPTY_STRING;
		leaseMonthYear = RegularConstants.EMPTY_STRING;

		DBContext dbContext = new DBContext();
		ArrayList<GenericOption> listofMonth = null;
		listofMonth = getGenericOptions("COMMON", "MONTHS", dbContext, null);
		webContext.getRequest().setAttribute("COMMON_MONTHS", listofMonth);
		dbContext.close();
	}

	@Override
	public void validate() {
		validateLeaseMonth();
		validateLeaseMonthYear();
		try {

		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("invoiceDate", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
	}

	public DTObject validate(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		if (!validateLeaseMonth() || !validateLeaseMonthYear()) {
			resultDTO.set(ContentManager.ERROR, getErrorMap().getErrorKey("leaseMonth"));
			resultDTO.set(ContentManager.ERROR_FIELD, "leaseMonth");
			return resultDTO;
		}
		return resultDTO;
	}

	private boolean validateLeaseMonth() {
		CommonValidator validator = new CommonValidator();
		try {
			if (validator.isEmpty(leaseMonth)) {
				getErrorMap().setError("leaseMonth", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validator.isCMLOVREC("COMMON", "MONTHS", leaseMonth)) {
				getErrorMap().setError("leaseMonth", BackOfficeErrorCodes.INVALID_OPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("leaseMonth", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return false;
	}

	private boolean validateLeaseMonthYear() {
		CommonValidator validator = new CommonValidator();
		try {
			if (validator.isEmpty(leaseMonthYear)) {
				getErrorMap().setError("leaseMonth", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validator.isValidYear(leaseMonthYear)) {
				getErrorMap().setError("leaseMonth", BackOfficeErrorCodes.INVALID_YEAR);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("leaseMonth", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return false;
	}

	public DTObject loadPreLeaseContractInterestDetails(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		DBContext dbContext = new DBContext();
		String year = inputDTO.get("YEAR");
		String month = inputDTO.get("MONTH");
		StringBuffer sqlQuery = new StringBuffer(RegularConstants.EMPTY_STRING);
		long serial = 0;
		boolean checkRecord = false;
		String errorCode = null;
		try {
			setLeaseMonthYear(inputDTO.get("YEAR"));
			setLeaseMonth(inputDTO.get("MONTH"));
			DTObject validateDTO = validate(inputDTO);
			if (validateDTO.get(ContentManager.ERROR) != null) {
				return validateDTO;
			}
			DBUtil util = dbContext.createUtilInstance();
			util.setMode(DBUtil.CALLABLE);
			util.setSql("{CALL SP_PLC_GRP_INTEREST_LED(?,?,?,?,?)}");
			util.setLong(1, Long.parseLong(context.getEntityCode()));
			util.setInt(2, Integer.parseInt(month));
			util.setInt(3, Integer.parseInt(year));
			util.registerOutParameter(4, Types.BIGINT);
			util.registerOutParameter(5, Types.VARCHAR);
			util.execute();
			errorCode = String.valueOf(util.getString(5));
			if (!errorCode.equals("S")) {
				resultDTO.set("SP_ERROR", util.getString(5));
				return resultDTO;
			}
			serial = Long.parseLong(util.getString(4));
			util.reset();
			util.setMode(DBUtil.PREPARED);
			sqlQuery.append("SELECT 0,TO_CHAR(T.CONTRACT_DATE,'" + context.getDateFormat() + "')CONTRACT_DATE,T.CONTRACT_SL,PLC.SUNDRY_DB_AC,C.CUSTOMER_NAME,CM.LOV_LABEL_EN_US,");
			sqlQuery.append("T.SORT_KEY,T.NOF_INT_ENTRIES,T.INTEREST_CCY,FN_FORMAT_AMOUNT(T.TOTAL_INTEREST_AMOUNT,T.INTEREST_CCY,CUR.SUB_CCY_UNITS_IN_CCY)TOTAL_INTEREST_AMOUNT,T.TMP_SERIAL,T.GRP_SL");
			sqlQuery.append(" FROM TMPPLCINTDBN T ");
			sqlQuery.append(" JOIN PRELEASECONTRACT PLC ON (PLC.ENTITY_CODE=? AND PLC.CONTRACT_DATE=T.CONTRACT_DATE AND PLC.CONTRACT_SL=T.CONTRACT_SL)");
			sqlQuery.append(" JOIN CUSTOMER C ON (C.ENTITY_CODE=PLC.ENTITY_CODE AND C.CUSTOMER_ID=PLC.CUSTOMER_ID) ");
			sqlQuery.append(" JOIN CMLOVREC CM ON (CM.PGM_ID='COMMON' AND CM.LOV_ID='INT_DEBIT_PRINTING' AND CM.LOV_VALUE=PLC.INT_DEBIT_NOTE_PRINT_CHOICE)");
			sqlQuery.append(" JOIN CURRENCY CUR ON (CUR.CCY_CODE=T.INTEREST_CCY)");
			sqlQuery.append(" WHERE T.TMP_SERIAL=?");
			util.setSql(sqlQuery.toString());
			util.setLong(1, Long.parseLong(context.getEntityCode()));
			util.setLong(2, serial);
			ResultSet rset = util.executeQuery();
			DHTMLXGridUtility gridUtility = new DHTMLXGridUtility();
			gridUtility.init();
			while (rset.next()) {
				gridUtility.startRow();
				gridUtility.setCell("");
				gridUtility.setCell(rset.getString("CONTRACT_DATE"));
				gridUtility.setCell(rset.getString("CONTRACT_SL"));
				gridUtility.setCell(rset.getString("SUNDRY_DB_AC"));
				gridUtility.setCell(rset.getString("CUSTOMER_NAME"));
				gridUtility.setCell(rset.getString("LOV_LABEL_EN_US"));
				gridUtility.setCell(rset.getString("SORT_KEY"));
				gridUtility.setCell(rset.getString("NOF_INT_ENTRIES"));
				gridUtility.setCell(rset.getString("INTEREST_CCY"));
				gridUtility.setCell(rset.getString("TOTAL_INTEREST_AMOUNT"));
				gridUtility.setCell(rset.getString("TMP_SERIAL"));
				gridUtility.setCell(rset.getString("GRP_SL"));
				gridUtility.setCell(RegularConstants.ZERO);
				gridUtility.endRow();
				checkRecord = true;
			}
			if (!checkRecord) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.ENTRY_NOT_AVAILABLE);
				return resultDTO;
			}
			gridUtility.finish();
			resultDTO.set(ContentManager.RESULT_XML, gridUtility.getXML());
			resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			dbContext.close();
		}
		return resultDTO;
	}

	public DTObject loadLedgerDetails(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		DHTMLXGridUtility gridUtility = new DHTMLXGridUtility();
		StringBuffer sqlQuery = new StringBuffer(RegularConstants.EMPTY_STRING);
		boolean recordExist = false;
		try {
			util.reset();
			util.setMode(DBUtil.PREPARED);
			sqlQuery.append("SELECT LED.PAYMENT_SL,SU.NAME,TO_CHAR(LED.FROM_DATE,'" + context.getDateFormat() + "')FROM_DATE,TO_CHAR(LED.UPTO_DATE,'" + context.getDateFormat() + "')UPTO_DATE,");
			sqlQuery.append(" LED.NOF_DAYS,LPOP.PAYMENT_CCY,FN_FORMAT_AMOUNT(LPOP.PAYMENT_AMOUNT,LPOP.PAYMENT_CCY,C1.SUB_CCY_UNITS_IN_CCY)PAYMENT_AMOUNT,");
			sqlQuery.append(" LED.INTEREST_RATE,LED.INTEREST_CCY,FN_FORMAT_AMOUNT(LED.INTEREST_AMOUNT,LED.INTEREST_CCY,C2.SUB_CCY_UNITS_IN_CCY)INTEREST_AMOUNT");
			sqlQuery.append(" FROM TMPPLCINTDBNBRKUP TB ");
			sqlQuery.append(" JOIN PLCINTERESTLED LED ON (LED.ENTITY_CODE=? AND LED.CONTRACT_DATE=? AND LED.CONTRACT_SL=? AND ");
			sqlQuery.append(" LED.FOR_YEAR=TB.FOR_YEAR AND LED.FOR_MONTH=TB.FOR_MONTH AND LED.PAYMENT_SL=TB.PAYMENT_SL) ");
			sqlQuery.append(" JOIN LEASEPOINVPAY LPOP ON (LPOP.ENTITY_CODE=LED.ENTITY_CODE AND LPOP.CONTRACT_DATE=LED.CONTRACT_DATE AND ");
			sqlQuery.append(" LPOP.CONTRACT_SL=LED.CONTRACT_SL AND LPOP.PAYMENT_SL=LED.PAYMENT_SL) ");
			sqlQuery.append(" JOIN LEASEPO LPO ON  (LPO.ENTITY_CODE=LPOP.ENTITY_CODE AND LPO.CONTRACT_DATE=LPOP.CONTRACT_DATE AND ");
			sqlQuery.append(" LPO.CONTRACT_SL=LPOP.CONTRACT_SL AND LPO.PO_SL=LPOP.PO_SL) ");
			sqlQuery.append(" JOIN SUPPLIER SU ON (SU.ENTITY_CODE=LPO.ENTITY_CODE AND SU.SUPPLIER_ID=LPO.SUPPLIER_ID) ");
			sqlQuery.append(" JOIN CURRENCY C1 ON (C1.CCY_CODE=LPOP.PAYMENT_CCY) ");
			sqlQuery.append(" JOIN CURRENCY C2 ON (C2.CCY_CODE=LED.INTEREST_CCY) ");
			sqlQuery.append(" WHERE TB.TMP_SERIAL=? AND TB.GRP_SL=?");
			util.setSql(sqlQuery.toString());
			util.setLong(1, Long.parseLong(context.getEntityCode()));
			util.setDate(2, new java.sql.Date(BackOfficeFormatUtils.getDate(inputDTO.get("CONTRACT_DATE"), context.getDateFormat()).getTime()));
			util.setLong(3, Long.parseLong(inputDTO.get("CONTRACT_SL")));
			// util.setLong(4, Long.parseLong(context.getEntityCode()));
			// util.setDate(5, new
			// java.sql.Date(BackOfficeFormatUtils.getDate(inputDTO.get("CONTRACT_DATE"),
			// context.getDateFormat()).getTime()));
			// util.setLong(6, Long.parseLong(inputDTO.get("CONTRACT_SL")));
			util.setLong(4, Long.parseLong(inputDTO.get("TMP_SERIAL")));
			util.setInt(5, Integer.parseInt(inputDTO.get("GRP_SL")));
			ResultSet rset = util.executeQuery();
			gridUtility.init();
			while (rset.next()) {
				gridUtility.startRow();
				gridUtility.setCell(rset.getString("PAYMENT_SL"));
				gridUtility.setCell(rset.getString("NAME"));
				gridUtility.setCell(rset.getString("FROM_DATE"));
				gridUtility.setCell(rset.getString("UPTO_DATE"));
				gridUtility.setCell(rset.getString("NOF_DAYS"));
				gridUtility.setCell(rset.getString("INTEREST_RATE"));
				gridUtility.setCell(rset.getString("PAYMENT_CCY"));
				gridUtility.setCell(rset.getString("PAYMENT_AMOUNT"));
				gridUtility.setCell(rset.getString("INTEREST_CCY"));
				gridUtility.setCell(rset.getString("INTEREST_AMOUNT"));
				gridUtility.endRow();
				recordExist = true;
			}
			if (!recordExist) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.ENTRY_NOT_AVAILABLE);
				return resultDTO;
			}
			gridUtility.finish();
			String resultXML = gridUtility.getXML();
			inputDTO.set(ContentManager.RESULT_XML, resultXML);
			inputDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);

		} catch (Exception e) {
			e.printStackTrace();
			inputDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			dbContext.close();
		}
		return inputDTO;
	}

	public DTObject processAction(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		try {
			setProcessBO("patterns.config.framework.bo.lss.eplcintdbngenBO");
			DTObject formDTO = getFormDTO();
			formDTO.set("ENTITY_CODE", context.getEntityCode());
			formDTO.set("USER_ID", context.getUserID());
			formDTO.set("TMP_SERIAL", inputDTO.get("TMP_SERIAL"));
			formDTO.set("GRP_SL", inputDTO.get("GRP_SL"));
			formDTO.set("ENTRY_MODE", "M");
			formDTO.set("PROCESS_ID", "EPLCINTDBNGEN");
			formDTO.set(RegularConstants.PROCESSBO_CLASS, getProcessBO());
			BackOfficeProcessManager processManager = new BackOfficeProcessManager();
			TBAProcessResult processResult = processManager.delegate(formDTO);
			if (processResult.getProcessStatus().equals(TBAProcessStatus.SUCCESS)) {
				resultDTO.set(ContentManager.STATUS, RegularConstants.SUCCESS);
			} else {
				resultDTO.set(ContentManager.STATUS, RegularConstants.FAILURE);
			}
			resultDTO.set(ContentManager.RESULT, getErrorResource(processResult.getErrorCode(), null));
			Object additionalInfo = processResult.getAdditionalInfo();
			if (additionalInfo != null && additionalInfo instanceof String) {
				resultDTO.set(RequestConstants.ADDITIONAL_INFO, processResult.getAdditionalInfo().toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return resultDTO;
	}

	public DTObject doPrint(DTObject inputDTO) {
		try {
			inputDTO.set(ContentManager.REPORT_HANDLER, "patterns.config.web.reports.lss.replcintdbngenreport");
			inputDTO.set("REPORT_FORMAT", inputDTO.get("REPORT_FORMAT"));
			inputDTO.set("ENTITY_CODE", context.getEntityCode());
			inputDTO.set("TMP_SERIAL", inputDTO.get("TMP_SERIAL"));
			if (inputDTO.get("REPORT_FORMAT").equals("") || inputDTO.get("REPORT_FORMAT") == null)
				inputDTO.set(ContentManager.REPORT_FORMAT, ReportUtils.EXPORT_PDF + "");
			else
				inputDTO.set(ContentManager.REPORT_FORMAT, inputDTO.get("REPORT_FORMAT"));
			processDownload(inputDTO);
		} catch (Exception e) {
			e.printStackTrace();
			inputDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
		}
		return inputDTO;
	}

}
