package patterns.config.web.forms.lss;

import java.sql.ResultSet;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.service.DTDObject;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.LeaseValidator;
import patterns.config.validations.MasterValidator;
import patterns.config.validations.RegistrationValidator;
import patterns.config.web.forms.GenericFormBean;

/**
 * 
 * 
 * @version 1.0
 * @author Kalimuthu U
 * @since 22-MAR-2017 <br>
 *        <b>ELEASEUSERATTR</b> <BR>
 *        <U><B> Lease - Additional Attributes Specification</B></U> <br>
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 */
public class eleaseuserattrbean extends GenericFormBean {

	private static final long serialVersionUID = 1L;
	private String leaseCode;
	private String customerID;
	private String aggrementSerial;
	private String scheduleID;
	private String fieldId;
	private String fieldValue;
	private String fieldRemarks;
	private String xmleleaseuserattrGrid;
	private boolean enabled;
	private String remarks;

	public String getLeaseCode() {
		return leaseCode;
	}

	public void setLeaseCode(String leaseCode) {
		this.leaseCode = leaseCode;
	}

	public String getCustomerID() {
		return customerID;
	}

	public void setCustomerID(String customerID) {
		this.customerID = customerID;
	}

	public String getAggrementSerial() {
		return aggrementSerial;
	}

	public void setAggrementSerial(String aggrementSerial) {
		this.aggrementSerial = aggrementSerial;
	}

	public String getScheduleID() {
		return scheduleID;
	}

	public void setScheduleID(String scheduleID) {
		this.scheduleID = scheduleID;
	}

	public String getFieldId() {
		return fieldId;
	}

	public void setFieldId(String fieldId) {
		this.fieldId = fieldId;
	}

	public String getFieldValue() {
		return fieldValue;
	}

	public void setFieldValue(String fieldValue) {
		this.fieldValue = fieldValue;
	}

	public String getFieldRemarks() {
		return fieldRemarks;
	}

	public void setFieldRemarks(String fieldRemarks) {
		this.fieldRemarks = fieldRemarks;
	}

	public String getXmleleaseuserattrGrid() {
		return xmleleaseuserattrGrid;
	}

	public void setXmleleaseuserattrGrid(String xmleleaseuserattrGrid) {
		this.xmleleaseuserattrGrid = xmleleaseuserattrGrid;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void reset() {
		leaseCode = RegularConstants.EMPTY_STRING;
		customerID = RegularConstants.EMPTY_STRING;
		aggrementSerial = RegularConstants.EMPTY_STRING;
		scheduleID = RegularConstants.EMPTY_STRING;
		fieldId = RegularConstants.EMPTY_STRING;
		fieldValue = RegularConstants.EMPTY_STRING;
		fieldRemarks = RegularConstants.EMPTY_STRING;
		enabled = false;
		remarks = RegularConstants.EMPTY_STRING;
		setProcessBO("patterns.config.framework.bo.lss.eleaseuserattrBO");
	}

	public void validate() {
		if (validateLeaseCode() && validateAggrementSerial() && validateScheduleID()) {
			validateGrid();
			validateRemarks();
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.set("LESSEE_CODE", leaseCode);
				formDTO.set("AGREEMENT_NO", aggrementSerial);
				formDTO.set("SCHEDULE_ID", scheduleID);
				if (getAction().equals(ADD)) {
					formDTO.set("ENABLED", decodeBooleanToString(true));
				} else {
					formDTO.set("ENABLED", decodeBooleanToString(enabled));
				}
				formDTO.set("REMARKS", remarks);
				DTDObject dtdObject = new DTDObject();
				dtdObject.addColumn(0, "SELECT");
				dtdObject.addColumn(1, "FIELD_ID");
				dtdObject.addColumn(2, "DESCRIPTION");
				dtdObject.addColumn(3, "FIELD_VALUE");
				dtdObject.addColumn(4, "FIELD_TYPE");
				dtdObject.addColumn(5, "FIELD_DECIMAL");
				dtdObject.addColumn(6, "FIELD_SIZE");
				dtdObject.setXML(xmleleaseuserattrGrid);
				formDTO.setDTDObject("eleaseuserattrdtl", dtdObject);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("leaseCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
	}

	private boolean validateLeaseCode() {
		MasterValidator validation = new MasterValidator();
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("SUNDRY_DB_AC", leaseCode);
			formDTO.set(ContentManager.USER_ACTION, getAction());
			formDTO = validateLeaseCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("lesseCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("lesseCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public DTObject validateLeaseCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		LeaseValidator validation = new LeaseValidator();
		RegistrationValidator validatior = new RegistrationValidator();
		String lesseCode = inputDTO.get("SUNDRY_DB_AC");
		try {
			if (validation.isEmpty(lesseCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH, ContentManager.FETCH_ALL);
			resultDTO = validation.valildateLesseCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != RegularConstants.NULL) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			inputDTO.set("CUSTOMER_ID", resultDTO.get("CUSTOMER_ID"));
			inputDTO.set(ContentManager.FETCH_COLUMNS, "CUSTOMER_ID,CUSTOMER_NAME");
			resultDTO = validatior.valildateCustomerID(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
			validatior.close();
		}
		return resultDTO;
	}

	public DTObject valildateCustomerID(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		try {
			String sqlQuery = "SELECT CUSTOMER_ID,LEASE_PRODUCT_CODE FROM CUSTOMERACDTL WHERE ENTITY_CODE=? AND SUNDRY_DB_AC=?";
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, context.getEntityCode());
			util.setString(2, inputDTO.get("SUNDRY_DB_AC"));
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				resultDTO.set("CUSTOMER_ID", rset.getString("CUSTOMER_ID"));
				resultDTO.set("LEASE_PRODUCT_CODE", rset.getString("LEASE_PRODUCT_CODE"));
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			util.reset();
			dbContext.close();
		}
		return resultDTO;
	}

	public String getCustomerID(DTObject inputDTO) {
		String sqlQuery = ContentManager.EMPTY_STRING;
		sqlQuery = "SELECT CUSTOMER_ID FROM CUSTOMERACDTL WHERE ENTITY_CODE=? AND SUNDRY_DB_AC=?";
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		String customerId = null;
		try {
			util.setMode(DBUtil.PREPARED);
			util.setSql(sqlQuery);
			util.setString(1, context.getEntityCode());
			util.setString(2, inputDTO.get("SUNDRY_DB_AC"));
			ResultSet rs = util.executeQuery();
			if (rs.next()) {
				customerId = rs.getString(1);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			util.reset();
			dbContext.close();
		}
		return customerId;
	}

	private boolean validateAggrementSerial() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("CUSTOMER_ID", customerID);
			formDTO.set("AGREEMENT_NO", aggrementSerial);
			formDTO.set(ContentManager.USER_ACTION, USAGE);
			formDTO = validateAggrementSerial(formDTO);
			if (formDTO.get(ContentManager.ERROR) != RegularConstants.NULL) {
				getErrorMap().setError("aggrementSerial", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("aggrementSerial", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateAggrementSerial(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		LeaseValidator validation = new LeaseValidator();
		String agreementNo = inputDTO.get("AGREEMENT_NO");
		String customerID = inputDTO.get("CUSTOMER_ID");
		try {
			if (validation.isEmpty(agreementNo)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set("CUSTOMER_ID", customerID);
			inputDTO.set("AGREEMENT_NO", agreementNo);
			resultDTO = validation.validateCustomerAgreement(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != RegularConstants.NULL) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	private boolean validateScheduleID() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("LESSEE_CODE", leaseCode);
			formDTO.set("AGREEMENT_NO", aggrementSerial);
			formDTO.set("SCHEDULE_ID", scheduleID);
			formDTO.set(ContentManager.USER_ACTION, getAction());
			formDTO = validateScheduleID(formDTO);
			if (formDTO.get(ContentManager.ERROR) != RegularConstants.NULL) {
				getErrorMap().setError("scheduleID", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("scheduleID", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateScheduleID(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		LeaseValidator validation = new LeaseValidator();
		String scheduleID = inputDTO.get("SCHEDULE_ID");
		String action = inputDTO.get(ContentManager.USER_ACTION);
		try {
			if (validation.isEmpty(scheduleID)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "SCHEDULE_ID,TO_CHAR(AUTH_ON,'" + context.getDateFormat() + "') AUTH_ON,LEASE_CLOSURE_ON");
			resultDTO = validation.validateScheduleID(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != RegularConstants.NULL) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
			if (resultDTO.get("AUTH_ON") == RegularConstants.NULL) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNAUTH_LEASE_DETAILS);//
				return resultDTO;
			}
			if (resultDTO.get("LEASE_CLOSURE_ON") != RegularConstants.NULL) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.LEASE_CLOSED);//
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH, ContentManager.FETCH_ALL);
			inputDTO.set(ContentManager.TABLE, "LEASEUSERATTR");
			String columns[] = new String[] { context.getEntityCode(), inputDTO.get("LESSEE_CODE"), inputDTO.get("AGREEMENT_NO"), inputDTO.get("SCHEDULE_ID") };
			inputDTO.setObject(ContentManager.PROGRAM_KEY, columns);
			resultDTO = validation.validatePK(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (action.equals(ADD)) {
				if (ContentManager.DATA_AVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_EXISTS);
					return resultDTO;
				}
			} else if (action.equals(MODIFY)) {
				if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_NOT_EXISTS);
					return resultDTO;
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateFieldID(String fieldId) {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(fieldId)) {
				getErrorMap().setError("fieldId", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			DTObject formDTO = getFormDTO();
			formDTO.set("FIELD_ID", fieldId);
			formDTO.set("CUSTOMER_ID_HIDDEN", customerID);
			formDTO.set(ContentManager.ACTION, USAGE);
			formDTO = validateFieldID(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("fieldId", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("fieldId", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public DTObject validateFieldID(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		MasterValidator validation = new MasterValidator();
		// String fieldID = inputDTO.get("FIELD_ID");
		DBContext dbContext = new DBContext();
		try {
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION,FIELD_VALUE_TYPE,FIELD_DECIMALS,FIELD_SIZE,CUSTOMER_ID,ASSET_TYPE_CODE,FIELD_FOR");
			resultDTO = validation.ValidateFieldID(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
			if (!validation.isEmpty(resultDTO.get("CUSTOMER_ID"))) {
				if (!inputDTO.get("CUSTOMER_ID_HIDDEN").equals(resultDTO.get("CUSTOMER_ID"))) {
					Object[] errorParam = { resultDTO.get("CUSTOMER_ID"), inputDTO.get("CUSTOMER_ID_HIDDEN") };
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RESERVED_FOR_CUSTOMER);
					resultDTO.setObject(ContentManager.ERROR_PARAM, errorParam);
					return resultDTO;
				}
			}
			if (!resultDTO.get("FIELD_FOR").equals("L")) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_NOT_ALLOWED_LEASE_ATTRIBUTES);
				return resultDTO;
			}

		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
			dbContext.close();
		}
		return resultDTO;
	}

	private boolean validateGrid() {
		CommonValidator validation = new CommonValidator();
		try {
			DTObject formDTO = getFormDTO();
			DTObject inputDTO = new DTObject();

			DTDObject dtdObject = new DTDObject();
			dtdObject.addColumn(0, "SELECT");
			dtdObject.addColumn(1, "FIELD_ID");
			dtdObject.addColumn(2, "DESCRIPTION");
			dtdObject.addColumn(3, "FIELD_VALUE");
			dtdObject.addColumn(4, "FIELD_TYPE");
			dtdObject.addColumn(5, "FIELD_DECIMAL");
			dtdObject.addColumn(6, "FIELD_SIZE");
			dtdObject.setXML(xmleleaseuserattrGrid);
			if (dtdObject.getRowCount() <= 0) {
				getErrorMap().setError("fieldId", BackOfficeErrorCodes.ATLEAST_ONE_ROW);
				return false;
			}
			if (!validation.checkDuplicateRows(dtdObject, 1)) {
				getErrorMap().setError("fieldId", BackOfficeErrorCodes.DUPLICATE_DATA);
				return false;
			}
			for (int i = 0; i < dtdObject.getRowCount(); i++) {
				if (!validateFieldID(dtdObject.getValue(i, 1))) {
					getErrorMap().setError("fieldId", BackOfficeErrorCodes.INVALID_ACTION);
					return false;
				}
				if (dtdObject.getValue(i, 4).equals("T")) {
					inputDTO.set("PRECISION", dtdObject.getValue(i, 6));
					if (!validateFieldRemarks(dtdObject.getValue(i, 4))) {
						getErrorMap().setError("fieldId", BackOfficeErrorCodes.INVALID_ACTION);
						return false;
					}
				} else if (!dtdObject.getValue(i, 4).equals("T")) {
					inputDTO.set("FIELD_VALUE", dtdObject.getValue(i, 3));
					String fieldDecimal = dtdObject.getValue(i, 5);
					if (validation.isEmpty(dtdObject.getValue(i, 5))) {
						fieldDecimal = RegularConstants.ZERO;
					}
					inputDTO.set("PRECISION", String.valueOf((Integer.parseInt(dtdObject.getValue(i, 6)) - Integer.parseInt(fieldDecimal))));
					if (dtdObject.getValue(i, 4).equals("A") || dtdObject.getValue(i, 4).equals("F")) {
						inputDTO.set("SCALE", fieldDecimal);
					} else if (dtdObject.getValue(i, 4).equals("N")) {
						inputDTO.set("SCALE", fieldDecimal);
					}
					formDTO = validation.isValidDynamicIntFractions(inputDTO);
					if (formDTO.getObject(ContentManager.ERROR_PARAM) != null) {
						Object[] errorParam = formDTO.getObjectL(ContentManager.ERROR_PARAM);
						getErrorMap().setError("fieldValue", formDTO.get(ContentManager.ERROR), errorParam);
					}
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("fieldId", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return true;
	}

	public boolean validateFieldRemarks(String fieldRemarks) {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(fieldRemarks)) {
				getErrorMap().setError("fieldRemarks", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("fieldRemarks", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateRemarks() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (remarks != null && remarks.length() > 0) {
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			if (getAction().equals(MODIFY)) {
				if (commonValidator.isEmpty(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}
}
