/*

 Modification History
 ----------------------------------------------------------------------
 Sl.No		Modified Date	Author			Modified Changes	  Version
 ----------------------------------------------------------------------

 1		08-04-2017		RAJA E				version 3.1	
 ------------------------------------------------------------------------

 */

package patterns.config.web.forms.lss;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.ArrayList;

import patterns.config.constants.ProgramConstants;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.service.DTDObject;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.framework.web.DHTMLXGridUtility;
import patterns.config.framework.web.GenericOption;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.LeaseValidator;
import patterns.config.validations.MasterValidator;
import patterns.config.validations.RegistrationValidator;
import patterns.config.web.forms.GenericFormBean;

public class eleasepoinvbean extends GenericFormBean {

	private static final long serialVersionUID = 1L;
	private String leaseCode;
	private String custId;
	private String custName;
	private String agreeNo;
	private String scheduleId;
	private String purchOrdSl;
	private String invSl;
	private String invDate;
	private String invNum;
	private String invAmt;
	private String invAmtFormat;
	private String invAmt_curr;
	private String xmlGrid;
	private String remarks;
	private String poDate;
	private String outStandingAmt;
	private String purchOrdSlDisplay;
	private String scheduleIDHidden;
	private String preLeaseRef;
	private String preLeaseRefDate;
	private String preLeaseRefSerial;
	private String invFor;
	private String paySl;
	private String pgmGrid2;

	private String totalValHidden;

	// asset grid details

	private String assetClassifi;
	private String quantity;
	private String assetDescription;
	private String assetModel;
	private String configuration;
	private String value;
	private String valueFormat;
	private String cst;
	private String cstFormat;
	private String vat;
	private String vatFormat;
	private String st;
	private String stFormat;
	private String tcs;
	private String tcsFormat;
	private String vatOnAmount;
	private String vatOnAmountFormat;
	private String vatOnAmount_curr;
	private String totalPoAmt;
	private String xmleleasepoinvassetGrid;
	// private String totalAmount;
	private String totalValueAmount;

	// private String aggrementDate;

	public String getTotalValHidden() {
		return totalValHidden;
	}

	public void setTotalValHidden(String totalValHidden) {
		this.totalValHidden = totalValHidden;
	}

	public String getInvFor() {
		return invFor;
	}

	public void setInvFor(String invFor) {
		this.invFor = invFor;
	}

	public String getPaySl() {
		return paySl;
	}

	public void setPaySl(String paySl) {
		this.paySl = paySl;
	}

	public String getPgmGrid2() {
		return pgmGrid2;
	}

	public void setPgmGrid2(String pgmGrid2) {
		this.pgmGrid2 = pgmGrid2;
	}

	public String getXmlGrid2() {
		return xmlGrid2;
	}

	public void setXmlGrid2(String xmlGrid2) {
		this.xmlGrid2 = xmlGrid2;
	}

	private String xmlGrid2;

	public String getPreLeaseRefDate() {
		return preLeaseRefDate;
	}

	public void setPreLeaseRefDate(String preLeaseRefDate) {
		this.preLeaseRefDate = preLeaseRefDate;
	}

	public String getPreLeaseRefSerial() {
		return preLeaseRefSerial;
	}

	public void setPreLeaseRefSerial(String preLeaseRefSerial) {
		this.preLeaseRefSerial = preLeaseRefSerial;
	}

	public String getPreLeaseRef() {
		return preLeaseRef;
	}

	public void setPreLeaseRef(String preLeaseRef) {
		this.preLeaseRef = preLeaseRef;
	}

	public String getScheduleIDHidden() {
		return scheduleIDHidden;
	}

	public void setScheduleIDHidden(String scheduleIDHidden) {
		this.scheduleIDHidden = scheduleIDHidden;
	}

	public String getPurchOrdSlDisplay() {
		return purchOrdSlDisplay;
	}

	public void setPurchOrdSlDisplay(String purchOrdSlDisplay) {
		this.purchOrdSlDisplay = purchOrdSlDisplay;
	}

	public String getPoDate() {
		return poDate;
	}

	public void setPoDate(String poDate) {
		this.poDate = poDate;
	}

	public String getOutStandingAmt() {
		return outStandingAmt;
	}

	public void setOutStandingAmt(String outStandingAmt) {
		this.outStandingAmt = outStandingAmt;
	}

	public String getLeaseCode() {
		return leaseCode;
	}

	public void setLeaseCode(String leaseCode) {
		this.leaseCode = leaseCode;
	}

	public String getCustId() {
		return custId;
	}

	public void setCustId(String custId) {
		this.custId = custId;
	}

	public String getCustName() {
		return custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	public String getAgreeNo() {
		return agreeNo;
	}

	public void setAgreeNo(String agreeNo) {
		this.agreeNo = agreeNo;
	}

	public String getScheduleId() {
		return scheduleId;
	}

	public void setScheduleId(String scheduleId) {
		this.scheduleId = scheduleId;
	}

	public String getPurchOrdSl() {
		return purchOrdSl;
	}

	public void setPurchOrdSl(String purchOrdSl) {
		this.purchOrdSl = purchOrdSl;
	}

	public String getInvSl() {
		return invSl;
	}

	public void setInvSl(String invSl) {
		this.invSl = invSl;
	}

	public String getInvDate() {
		return invDate;
	}

	public void setInvDate(String invDate) {
		this.invDate = invDate;
	}

	public String getInvNum() {
		return invNum;
	}

	public void setInvNum(String invNum) {
		this.invNum = invNum;
	}

	public String getInvAmt() {
		return invAmt;
	}

	public void setInvAmt(String invAmt) {
		this.invAmt = invAmt;
	}

	public String getInvAmtFormat() {
		return invAmtFormat;
	}

	public void setInvAmtFormat(String invAmtFormat) {
		this.invAmtFormat = invAmtFormat;
	}

	public String getInvAmt_curr() {
		return invAmt_curr;
	}

	public void setInvAmt_curr(String invAmt_curr) {
		this.invAmt_curr = invAmt_curr;
	}

	public String getXmlGrid() {
		return xmlGrid;
	}

	public void setXmlGrid(String xmlGrid) {
		this.xmlGrid = xmlGrid;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	// asset grid

	public String getXmleleasepoinvassetGrid() {
		return xmleleasepoinvassetGrid;
	}

	public void setXmleleasepoinvassetGrid(String xmleleasepoinvassetGrid) {
		this.xmleleasepoinvassetGrid = xmleleasepoinvassetGrid;
	}

	public String getAssetClassifi() {
		return assetClassifi;
	}

	public void setAssetClassifi(String assetClassifi) {
		this.assetClassifi = assetClassifi;
	}

	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public String getAssetDescription() {
		return assetDescription;
	}

	public void setAssetDescription(String assetDescription) {
		this.assetDescription = assetDescription;
	}

	public String getAssetModel() {
		return assetModel;
	}

	public void setAssetModel(String assetModel) {
		this.assetModel = assetModel;
	}

	public String getConfiguration() {
		return configuration;
	}

	public void setConfiguration(String configuration) {
		this.configuration = configuration;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getValueFormat() {
		return valueFormat;
	}

	public void setValueFormat(String valueFormat) {
		this.valueFormat = valueFormat;
	}

	public String getCst() {
		return cst;
	}

	public void setCst(String cst) {
		this.cst = cst;
	}

	public String getCstFormat() {
		return cstFormat;
	}

	public void setCstFormat(String cstFormat) {
		this.cstFormat = cstFormat;
	}

	public String getVat() {
		return vat;
	}

	public void setVat(String vat) {
		this.vat = vat;
	}

	public String getVatFormat() {
		return vatFormat;
	}

	public void setVatFormat(String vatFormat) {
		this.vatFormat = vatFormat;
	}

	public String getSt() {
		return st;
	}

	public void setSt(String st) {
		this.st = st;
	}

	public String getStFormat() {
		return stFormat;
	}

	public void setStFormat(String stFormat) {
		this.stFormat = stFormat;
	}

	public String getTcs() {
		return tcs;
	}

	public void setTcs(String tcs) {
		this.tcs = tcs;
	}

	public String getTcsFormat() {
		return tcsFormat;
	}

	public void setTcsFormat(String tcsFormat) {
		this.tcsFormat = tcsFormat;
	}

	public String getVatOnAmount() {
		return vatOnAmount;
	}

	public void setVatOnAmount(String vatOnAmount) {
		this.vatOnAmount = vatOnAmount;
	}

	public String getVatOnAmountFormat() {
		return vatOnAmountFormat;
	}

	public void setVatOnAmountFormat(String vatOnAmountFormat) {
		this.vatOnAmountFormat = vatOnAmountFormat;
	}

	public String getVatOnAmount_curr() {
		return vatOnAmount_curr;
	}

	public void setVatOnAmount_curr(String vatOnAmount_curr) {
		this.vatOnAmount_curr = vatOnAmount_curr;
	}

	public String getTotalPoAmt() {
		return totalPoAmt;
	}

	public void setTotalPoAmt(String totalPoAmt) {
		this.totalPoAmt = totalPoAmt;
	}

	// public String getTotalAmount() {
	// return totalAmount;
	// }
	//
	// public void setTotalAmount(String totalAmount) {
	// this.totalAmount = totalAmount;
	// }
	//

	public String getTotalValueAmount() {
		return totalValueAmount;
	}

	public void setTotalValueAmount(String totalValueAmount) {
		this.totalValueAmount = totalValueAmount;
	}

	@Override
	public void reset() {
		// TODO Auto-generated method stub
		leaseCode = RegularConstants.EMPTY_STRING;
		custId = RegularConstants.EMPTY_STRING;
		custName = RegularConstants.EMPTY_STRING;
		agreeNo = RegularConstants.EMPTY_STRING;
		scheduleId = RegularConstants.EMPTY_STRING;
		purchOrdSl = RegularConstants.EMPTY_STRING;
		purchOrdSlDisplay = RegularConstants.EMPTY_STRING;
		invSl = RegularConstants.EMPTY_STRING;
		invSl = RegularConstants.EMPTY_STRING;
		invDate = RegularConstants.EMPTY_STRING;
		invFor = RegularConstants.EMPTY_STRING;
		invNum = RegularConstants.EMPTY_STRING;
		invAmt = RegularConstants.EMPTY_STRING;
		invAmtFormat = RegularConstants.EMPTY_STRING;
		invAmt_curr = RegularConstants.EMPTY_STRING;
		paySl = RegularConstants.EMPTY_STRING;
		remarks = RegularConstants.EMPTY_STRING;
		DBContext dbContext = new DBContext();
		ArrayList<GenericOption> invList = null;
		invList = getGenericOptions("ELEASEPOINV", "INV_FOR", dbContext, null);
		webContext.getRequest().setAttribute("ELEASEPOINV_INV_FOR", invList);
		dbContext.close();
		setProcessBO("patterns.config.framework.bo.lss.eleasepoinvBO");
	}

	@Override
	public void validate() {
		if (validatePreLeaseContractDate() && validatePreLeaseContractRefSerial()) {
			// TODO Auto-generated method stub
			validateLeaseCode();
			validateAgreementNo();
			validateScheduleID();
			validateInvSl();
			validateGrid();
			validateInvFor();
			validateinvDate();
			validateinvNumber();
			validateinvAmtFormat();
			validateGrid2();
			validateAssetDtlGrid();
			validateRemarks();
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.set("CONTRACT_DATE", preLeaseRefDate);
				formDTO.set("CONTRACT_SL", preLeaseRefSerial);
				formDTO.set("PO_SL", purchOrdSlDisplay);
				if (invSl != null && !invSl.equals(RegularConstants.EMPTY_STRING))
					formDTO.set("INVOICE_SL", invSl);
				else
					formDTO.set("INVOICE_SL", RegularConstants.EMPTY_STRING);
				formDTO.set("INV_FOR", invFor);
				formDTO.set("INVOICE_DATE", invDate);
				formDTO.set("INVOICE_NUMBER", invNum);
				formDTO.set("INVOICE_CCY", invAmt_curr);
				formDTO.set("INVOICE_AMOUNT", invAmt);
				formDTO.set("INVOICE_AMOUNT_PAID_SOFAR", "0");
				formDTO.set("REMARKS", remarks);

				DTDObject dtdObject = new DTDObject();
				dtdObject.addColumn(0, "SELECT");
				dtdObject.addColumn(1, "PO_ENTRYSL");
				dtdObject.addColumn(2, "PO_DATE");
				dtdObject.addColumn(3, "PO_NUM");
				dtdObject.addColumn(4, "PO_CURR");
				dtdObject.addColumn(5, "PO_AMT");
				dtdObject.addColumn(6, "PO_OUTSTANDING");
				dtdObject.addColumn(7, "PO_OUTSTANDING_UNFORMAT");
				dtdObject.addColumn(8, "TOTPOUNFORMAT");
				dtdObject.addColumn(9, "SCHEDULEIDDISPLAY");
				dtdObject.setXML(xmlGrid);
				formDTO.setDTDObject("ELEASEPOINVDTL", dtdObject);
				// formDTO.set("PAY_SL", paySl);
				if (invFor.equals("P")) {
					DTDObject dtdObject1 = new DTDObject();
					dtdObject1.addColumn(0, "SELECT");
					dtdObject1.addColumn(1, "PAYMENT_SL");
					dtdObject1.addColumn(2, "DATE_OF_AMT");
					dtdObject1.addColumn(3, "PO_AMT_FORMAT");
					dtdObject1.addColumn(4, "PO_AMT_UNFORMAT");
					dtdObject1.setXML(xmlGrid2);
					formDTO.setDTDObject("ELEASEPOINVDTL2", dtdObject1);
				}
				setAction("A");

				DTDObject dtdObject2 = new DTDObject();
				dtdObject2.addColumn(0, "SL");
				// dtdObject.addColumn(1, "LINK");
				dtdObject2.addColumn(1, "ASSET_TYPE_CODE");
				dtdObject2.addColumn(2, "ASSET_DESCRIPTION");
				dtdObject2.addColumn(3, "QUANTITY");
				dtdObject2.addColumn(4, "ASSET_MODEL");
				dtdObject2.addColumn(5, "ASSET_CONFIG");
				dtdObject2.addColumn(6, "ASSET_VALUE");
				dtdObject2.addColumn(7, "VALUE");
				// dtdObject2.addColumn(9, "ASSET_VAT");
				// dtdObject2.addColumn(10, "ASSET_ST");
				// dtdObject2.addColumn(11, "ASSET_TCS");
				// dtdObject2.addColumn(12, "ASSET_VAT_ON_AMOUNT");
				// dtdObject2.addColumn(13, "TOTAL");
				// dtdObject2.addColumn(14, "VALUE");
				// dtdObject2.addColumn(15, "CST");
				// dtdObject2.addColumn(16, "VAT");
				// dtdObject2.addColumn(17, "ST");
				// dtdObject2.addColumn(18, "TCS");
				// dtdObject2.addColumn(19, "VAT_ON_AMOUNT");
				dtdObject2.addColumn(8, "ASSET_MODEL");
				dtdObject2.addColumn(9, "ASSET_CONFIG");
				dtdObject2.setXML(xmleleasepoinvassetGrid);
				formDTO.setDTDObject("LEASEPOINVASSETS", dtdObject2);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("leaseCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
	}

	public boolean validatePreLeaseContractDate() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (commonValidator.isEmpty(preLeaseRefDate)) {
				getErrorMap().setError("preLeaseRefDate", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			java.util.Date dateInstance = commonValidator.isValidDate(preLeaseRefDate);
			if (dateInstance == null) {
				getErrorMap().setError("preLeaseRefDate", BackOfficeErrorCodes.INVALID_DATE);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("preLeaseRefDate", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}

	public boolean validatePreLeaseContractRefSerial() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("CONTRACT_DATE", preLeaseRefDate);
			formDTO.set("CONTRACT_SL", preLeaseRefSerial);
			formDTO.set(ContentManager.ACTION, getAction());
			formDTO = validatePreLeaseContractRefSerial(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("preLeaseRef", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("preLeaseRef", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validatePreLeaseContractRefSerial(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		LeaseValidator validation = new LeaseValidator();
		RegistrationValidator validator = new RegistrationValidator();
		String contractSl = inputDTO.get("CONTRACT_SL");
		String action = inputDTO.get("ACTION");
		String custumorID = RegularConstants.EMPTY_STRING;
		String schID = RegularConstants.EMPTY_STRING;
		String leaseCode = RegularConstants.EMPTY_STRING;
		String agrrementSl = RegularConstants.EMPTY_STRING;
		try {
			if (validation.isEmpty(contractSl)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			if (!validation.isValidNumber(contractSl)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_NUMBER);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "CUSTOMER_ID,SUNDRY_DB_AC,SCHEDULE_ID,AGREEMENT_NO");
			resultDTO = validation.validatePreLeaseContractReferenceSerial(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_NOT_EXISTS);
				return resultDTO;
			}
			custumorID = resultDTO.get("CUSTOMER_ID");
			leaseCode = resultDTO.get("SUNDRY_DB_AC");
			agrrementSl = resultDTO.get("AGREEMENT_NO");
			schID = resultDTO.get("SCHEDULE_ID");
			inputDTO.set("CUSTOMER_ID", custumorID);
			inputDTO.set(ContentManager.FETCH_COLUMNS, "CUSTOMER_NAME");
			resultDTO = validator.valildateCustomerID(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			String customerName = resultDTO.get("CUSTOMER_NAME");
			inputDTO.set(ContentManager.USER_ACTION, action);
			resultDTO = loadDetails(inputDTO);
			resultDTO.set(ContentManager.RESULT_XML, resultDTO.get(ContentManager.RESULT_XML));
			resultDTO.set("CUSTOMER_ID", custumorID);
			resultDTO.set("SUNDRY_DB_AC", leaseCode);
			resultDTO.set("AGREEMENT_NO", agrrementSl);
			resultDTO.set("SCHEDULE_ID", schID);
			resultDTO.set("CUSTOMER_NAME", customerName);
			return resultDTO;
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
			validator.close();
		}
		return resultDTO;
	}

	public boolean validateLeaseCode() {
		CommonValidator validation = new CommonValidator();
		try {
			if (!validation.isEmpty(leaseCode)) {
				DTObject formDTO = getFormDTO();
				formDTO.set("SUNDRY_DB_AC", leaseCode);
				formDTO.set(ContentManager.ACTION, USAGE);
				formDTO = validateLeaseCode(formDTO);
				if (formDTO.get(ContentManager.ERROR) != null) {
					getErrorMap().setError("leaseCode", formDTO.get(ContentManager.ERROR));
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("leaseCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public DTObject validateLeaseCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		LeaseValidator validation = new LeaseValidator();
		RegistrationValidator validatior = new RegistrationValidator();
		try {
			inputDTO.set(ContentManager.FETCH, ContentManager.FETCH_ALL);
			resultDTO = validation.valildateLesseCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
			custId = resultDTO.get("CUSTOMER_ID");
			inputDTO.set("CUSTOMER_ID", custId);
			inputDTO.set(ContentManager.FETCH_COLUMNS, "CUSTOMER_ID,CUSTOMER_NAME");
			resultDTO = validatior.valildateCustomerID(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
			String custName = resultDTO.get("CUSTOMER_NAME");
			resultDTO.set("CUSTOMER_NAME", custName);
			resultDTO.set("CUSTOMER_ID", custId);
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
			validatior.close();
		}
		return resultDTO;
	}

	public boolean validateAgreementNo() {
		CommonValidator validation = new CommonValidator();
		try {
			if (!validation.isEmpty(agreeNo) && validation.isEmpty(custId)) {
				DTObject formDTO = getFormDTO();
				formDTO.set("LESSEE_CODE", leaseCode);
				formDTO.set("AGREEMENT_NO", agreeNo);
				formDTO.set("SCHEDULE_ID", scheduleIDHidden);
				formDTO.set("PO_SL", purchOrdSlDisplay);
				formDTO.set("CUSTOMER_ID", custId);
				formDTO.set(ContentManager.ACTION, getAction());
				formDTO = validateAgreementNo(formDTO);
				if (formDTO.get(ContentManager.ERROR) != null) {
					getErrorMap().setError("agreeNo", formDTO.get(ContentManager.ERROR));
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("agreeNo", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public DTObject validateAgreementNo(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		LeaseValidator validation = new LeaseValidator();
		try {
			inputDTO.set(ContentManager.FETCH_COLUMNS, "AGREEMENT_NO");
			resultDTO = validation.validateCustomerAgreement(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateScheduleID() {
		CommonValidator validation = new CommonValidator();
		try {
			if (!validation.isEmpty(scheduleId)) {
				DTObject formDTO = getFormDTO();
				formDTO.set("LESSEE_CODE", leaseCode);
				formDTO.set("AGREEMENT_NO", agreeNo);
				formDTO.set("SCHEDULE_ID", scheduleIDHidden);
				formDTO.set("PO_SL", purchOrdSlDisplay);
				formDTO.set(ContentManager.ACTION, getAction());
				formDTO = validateScheduleID(formDTO);
				if (formDTO.get(ContentManager.ERROR) != null) {
					getErrorMap().setError("scheduleId", formDTO.get(ContentManager.ERROR));
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("scheduleId", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public DTObject validateScheduleID(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		LeaseValidator validation = new LeaseValidator();
		try {
			inputDTO.set(ContentManager.FETCH_COLUMNS, "SCHEDULE_ID,AUTH_ON,LEASE_CLOSURE_ON,ASSET_CCY");
			resultDTO = validation.validateScheduleID(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != RegularConstants.NULL) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
			if (resultDTO.getObject("AUTH_ON") == RegularConstants.NULL) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNAUTH_LEASE_DETAILS);//
				return resultDTO;
			}
			if (resultDTO.getObject("LEASE_CLOSURE_ON") != RegularConstants.NULL) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.LEASE_CLOSED);//
				return resultDTO;
			}
			// assetCCY = resultDTO.get("ASSET_CCY");
			// resultDTO.set("ASSET_CCY", assetCCY);
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public DTObject loadDetails(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBContext dbContext = new DBContext();
		// String leaseCode = inputDTO.get("LESSEE_CODE");
		// String agreeNo = inputDTO.get("AGREEMENT_NO");
		// String scheduleId = inputDTO.get("SCHEDULE_ID");
		String poSL = inputDTO.get("PO_SL");
		Date contractDate = null;
		contractDate = new java.sql.Date(BackOfficeFormatUtils.getDate(inputDTO.get("CONTRACT_DATE"), BackOfficeConstants.JAVA_DATE_FORMAT).getTime());
		String contractSerial = inputDTO.get("CONTRACT_SL");
		StringBuffer sqlQuery = new StringBuffer();
		DBUtil util = dbContext.createUtilInstance();
		boolean recordExist = false;
		String action = inputDTO.get("ACTION");
		try {
			util.reset();
			sqlQuery.append("SELECT L.PO_SL,TO_CHAR(L.PO_DATE, ?) AS PO_DATE,L.PO_NUMBER,L.PO_CCY,FN_FORMAT_AMOUNT(L.TOTAL_PO_AMOUNT,L.PO_CCY,C.SUB_CCY_UNITS_IN_CCY) AS POAMTFORMAT,CASE  WHEN ? IS NULL THEN FN_FORMAT_AMOUNT(IFNULL(L.TOTAL_PO_AMOUNT-C2,L.TOTAL_PO_AMOUNT),L.PO_CCY,C.SUB_CCY_UNITS_IN_CCY)ELSE FN_FORMAT_AMOUNT(IFNULL(L.TOTAL_PO_AMOUNT - C2+C2,L.TOTAL_PO_AMOUNT),L.PO_CCY,C.SUB_CCY_UNITS_IN_CCY) END AS OUTSTANDINGFORMAT,CASE   WHEN ? IS NULL THEN IFNULL((L.TOTAL_PO_AMOUNT-C2),L.TOTAL_PO_AMOUNT) ELSE IFNULL(L.TOTAL_PO_AMOUNT - C2+C2,L.TOTAL_PO_AMOUNT) END AS OUTSTANDINGUNFORMAT,(L.TOTAL_PO_AMOUNT) AS POUNFORMAT FROM LEASEPO L LEFT OUTER JOIN(SELECT M.INVOICE_AMOUNT, SUM(M.INVOICE_AMOUNT)  AS C2,PO_SL AS C3 FROM LEASEPOINV M WHERE M.ENTITY_CODE = ? AND M.CONTRACT_DATE = ? AND M.CONTRACT_SL = ?  AND REJ_ON IS NULL AND M.PO_SL=IFNULL(?,M.PO_SL) GROUP BY M.ENTITY_CODE , M.CONTRACT_DATE , M.CONTRACT_SL ,M.PO_SL) AS C1 ON C3 = L.PO_SL JOIN  CURRENCY C ON L.PO_CCY = C.CCY_CODE WHERE L.ENTITY_CODE = ? AND L.CONTRACT_DATE = ? AND L.CONTRACT_SL = ?  AND L.PO_SL=IFNULL(?,L.PO_SL)");
			util.setSql(sqlQuery.toString());
			util.setString(1, context.getDateFormat());
			if (action.equals("RECTIFY")) {
				util.setString(2, poSL);
			} else {
				util.setString(2, RegularConstants.NULL);
			}
			if (action.equals("RECTIFY")) {
				util.setString(3, poSL);
			} else {
				util.setString(3, RegularConstants.NULL);
			}
			util.setInt(4, Integer.parseInt(context.getEntityCode()));
			util.setDate(5, contractDate);
			util.setString(6, contractSerial);
			// util.setString(7, scheduleId);
			if (action.equals("RECTIFY")) {
				util.setString(7, poSL);
			} else {
				util.setString(7, RegularConstants.NULL);
			}
			util.setInt(8, Integer.parseInt(context.getEntityCode()));
			util.setDate(9, contractDate);
			util.setString(10, contractSerial);
			if (action.equals("RECTIFY")) {
				util.setString(11, poSL);
			} else {
				util.setString(11, RegularConstants.NULL);
			}
			ResultSet rset = util.executeQuery();
			DHTMLXGridUtility gridUtility = new DHTMLXGridUtility();
			gridUtility.init();
			int index = 0;
			while (rset.next()) {
				gridUtility.startRow(String.valueOf(index));
				gridUtility.setCell("");
				gridUtility.setCell(rset.getString("PO_SL"));
				gridUtility.setCell(rset.getString("PO_DATE"));
				gridUtility.setCell(rset.getString("PO_NUMBER"));
				gridUtility.setCell(rset.getString("PO_CCY"));
				gridUtility.setCell(rset.getString("POAMTFORMAT"));
				gridUtility.setCell(rset.getString("OUTSTANDINGFORMAT"));
				gridUtility.setCell(rset.getString("OUTSTANDINGUNFORMAT"));
				gridUtility.setCell(rset.getString("POUNFORMAT"));
				gridUtility.endRow();
				index++;
				recordExist = true;
			}
			if (!recordExist) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_NOT_EXISTS);
				return resultDTO;
			}
			gridUtility.finish();
			String resultXML = gridUtility.getXML();
			resultDTO.set(ContentManager.RESULT_XML, resultXML);
			resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			dbContext.close();
			util.reset();
		}
		return resultDTO;
	}

	public boolean validateInvSl() {
		DTObject formDTO = new DTObject();
		CommonValidator validation = new CommonValidator();
		try {
			if (getAction().equals("RECTIFY")) {
				if (validation.isEmpty(invSl)) {
					getErrorMap().setError("invSl", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!validation.isValidNumber(invSl)) {
					getErrorMap().setError("invSl", BackOfficeErrorCodes.INVALID_NUMBER);
					return false;
				}
				formDTO.set("ENTITY_CODE", context.getBranchCode());
				formDTO.set("CONTRACT_DATE", preLeaseRefDate);
				formDTO.set("CONTRACT_SL", preLeaseRefSerial);
				formDTO.set("PO_SL", purchOrdSlDisplay);
				formDTO.set("INVOICE_SL", invSl);
				formDTO.set(ContentManager.ACTION, getAction());
				formDTO = validateInvSl(formDTO);
				if (formDTO.get(ContentManager.ERROR) != null) {
					getErrorMap().setError("invSl", formDTO.get(ContentManager.ERROR));
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("invSl", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public DTObject validateInvSl(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		MasterValidator validator = new MasterValidator();
		try {
			inputDTO.set(ContentManager.TABLE, "LEASEPOINV");
			inputDTO.set(ContentManager.FETCH_COLUMNS, "INVOICE_NUMBER");
			String columns[] = new String[] { context.getEntityCode(), inputDTO.get("CONTRACT_DATE"), inputDTO.get("CONTRACT_SL"), inputDTO.get("PO_SL"), inputDTO.get("INVOICE_SL") };
			inputDTO.setObject(ContentManager.PROGRAM_KEY, columns);
			resultDTO = validator.validatePK(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return resultDTO;
	}

	public boolean validateInvFor() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(invFor)) {
				getErrorMap().setError("invFor", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isCMLOVREC("ELEASEPOINV", "INV_FOR", invFor)) {
				getErrorMap().setError("invFor", BackOfficeErrorCodes.INVALID_OPTION);
				return false;
			}

			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("invFor", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateinvDate() {
		CommonValidator commonvalidation = new CommonValidator();
		try {
			if (commonvalidation.isEmpty(invDate)) {
				getErrorMap().setError("invDate", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			java.util.Date dateInstance = commonvalidation.isValidDate(invDate);
			if (dateInstance == null) {
				getErrorMap().setError("invDate", BackOfficeErrorCodes.INVALID_DATE);
				return false;
			}
			if (commonvalidation.isDateGreaterThanCBD(invDate)) {
				getErrorMap().setError("invDate", BackOfficeErrorCodes.DATE_LCBD);
				return false;
			}
			if (commonvalidation.isDateLesser(invDate, poDate)) {
				getErrorMap().setError("invDate", BackOfficeErrorCodes.DATE_LESS_PO_DATE);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("invDate", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			commonvalidation.close();
		}
		return false;
	}

	public boolean validateinvNumber() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(invNum)) {
				getErrorMap().setError("invNum", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			DTObject formDTO = getFormDTO();
			formDTO.set("ENTITY_CODE", context.getEntityCode());
			formDTO.set("CONTRACT_DATE", preLeaseRefDate);
			formDTO.set("CONTRACT_SL", preLeaseRefSerial);
			formDTO.set("PURCH_ORDER_SL", purchOrdSlDisplay);
			formDTO.set("INVOICE_SL", invSl);
			formDTO.set("INVOICE_NUMBER", invNum);
			formDTO = validateinvNumber(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("invNum", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("invNum", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public DTObject validateinvNumber(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		MasterValidator validation = new MasterValidator();
		try {
			String primaryKey = inputDTO.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + inputDTO.get("CONTRACT_DATE") + RegularConstants.PK_SEPARATOR + inputDTO.get("CONTRACT_SL") + RegularConstants.PK_SEPARATOR + inputDTO.get("PURCH_ORDER_SL") + RegularConstants.PK_SEPARATOR + inputDTO.get("INVOICE_SL");
			String dedupKey = context.getEntityCode() + RegularConstants.PK_SEPARATOR + inputDTO.get("INVOICE_NUMBER") + RegularConstants.PK_SEPARATOR + RegularConstants.ONE;
			DTObject formDTO = new DTObject();
			formDTO.set(ContentManager.PROGRAM_ID, "ELEASEPOINV");
			formDTO.set(ContentManager.PURPOSE_ID, "INVOICE");
			formDTO.set(ContentManager.PROGRAM_KEY, primaryKey);
			formDTO.set(ContentManager.DEDUP_KEY, dedupKey);
			resultDTO = validation.isDeDupAlreadyExist(formDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
				Object[] errorParam = { resultDTO.getObject("LINKED_TO_PK") };
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INV_NUM_EXISTS);
				resultDTO.setObject(ContentManager.ERROR_PARAM, errorParam);
				return resultDTO;
			}

		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateinvNum() {
		CommonValidator commonvalidation = new CommonValidator();
		try {
			if (commonvalidation.isEmpty(invNum)) {
				getErrorMap().setError("invNum", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!commonvalidation.isValidDescription(invNum)) {
				getErrorMap().setError("invNum", BackOfficeErrorCodes.INVALID_DESCRIPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("invNum", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			commonvalidation.close();
		}
		return false;
	}

	private boolean validateinvAmtFormat() {
		CommonValidator validation = new CommonValidator();
		DTObject formDTO = getFormDTO();
		try {
			if (validation.isEmpty(invAmt)) {
				getErrorMap().setError("invAmt", BackOfficeErrorCodes.HMS_MANDATORY);
				return false;
			}
			if (validation.isValidNegativeAmount(invAmt)) {
				getErrorMap().setError("invAmt", BackOfficeErrorCodes.HMS_POSITIVE_AMOUNT);
				return false;
			}
			if (validation.isZeroAmount(invAmt)) {
				getErrorMap().setError("invAmt", BackOfficeErrorCodes.HMS_ZERO_AMOUNT);
				return false;
			}
			if (!validation.isValidAmount(invAmt)) {
				getErrorMap().setError("invAmt", BackOfficeErrorCodes.INVALID_AMOUNT);
				return false;
			}
			formDTO.set("CURRENCY_CODE", invAmt_curr);
			formDTO.set(ProgramConstants.AMOUNT, invAmt);
			formDTO.set(ProgramConstants.CURRENCY_UNITS, context.getBaseCurrencyUnits());
			formDTO.set(ContentManager.USER_ACTION, getAction());
			formDTO = validation.validateCurrencySmallAmount(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("invAmt", formDTO.get(ContentManager.ERROR));
				return false;
			}
			if (validation.isAmountGreater(invAmt, outStandingAmt)) {
				getErrorMap().setError("invAmt", BackOfficeErrorCodes.INVOICE_AMOUNT_LESSER);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("invAmt", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	private boolean validateGrid2() {
		CommonValidator validation = new CommonValidator();
		// java.util.Date dateInstance = null;
		try {
			if (invFor.equals("P")) {
				DTDObject dtdObject = new DTDObject();
				dtdObject.addColumn(0, "SELECT");
				dtdObject.addColumn(1, "PAYMENT_SL");
				dtdObject.addColumn(2, "DATE_OF_AMT");
				dtdObject.addColumn(3, "PO_AMT_FORMAT");
				dtdObject.addColumn(4, "PO_AMT_UNFORMAT");
				dtdObject.setXML(xmlGrid2);
				if (dtdObject.getRowCount() <= 0) {
					getErrorMap().setError("paySl", BackOfficeErrorCodes.ATLEAST_ONE_ROW);
					return false;
				}
				if (!validation.checkDuplicateRows(dtdObject, 1)) {
					getErrorMap().setError("paySl", BackOfficeErrorCodes.DUPLICATE_DATA);
					return false;
				}
				if (!validation.isAmountEqual(invAmt, totalValueAmount)) {
					getErrorMap().setError("paySl", BackOfficeErrorCodes.INVOICE_AMOUNT_EQUAL);
					return false;
				}
				for (int i = 0; i < dtdObject.getRowCount(); i++) {
					if (!validatePaymentSerial(dtdObject.getValue(i, 1))) {
						getErrorMap().setError("paySl", BackOfficeErrorCodes.INVALID_ACTION);
						return false;
					}
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("paySl", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return true;
	}

	private boolean validatePaymentSerial(String paySl) {
		CommonValidator validation = new CommonValidator();
		try {
			if (invFor.equals("P")) {
				if (validation.isEmpty(paySl)) {
					getErrorMap().setError("paySl", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!validation.isValidNumber(paySl)) {
					getErrorMap().setError("paySl", BackOfficeErrorCodes.NUMERIC_CHECK);
					return false;
				}
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.set("CONTRACT_DATE", preLeaseRefDate);
				formDTO.set("CONTRACT_SL", preLeaseRefSerial);
				formDTO.set("PAYMENT_SL", paySl);
				formDTO.set("PURCH_SL", purchOrdSlDisplay);
				formDTO.set(ContentManager.ACTION, getAction());
				formDTO = validatePaymentSerial(formDTO);
				if (formDTO.get(ContentManager.ERROR) != null) {
					getErrorMap().setError("paySl", formDTO.get(ContentManager.ERROR));
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("paySl", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public DTObject validatePaymentSerial(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		LeaseValidator validation = new LeaseValidator();
		try {
			inputDTO.set(ContentManager.FETCH_COLUMNS, "AUTH_ON,PO_SL,INVOICE_SL,PAYMENT_AMOUNT_BC,TO_CHAR(PAYMENT_DATE,'" + context.getDateFormat() + "')PAYMENT_DATE");
			resultDTO = validation.validateLeasePoInvPay(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != RegularConstants.NULL) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
			if (resultDTO.getObject("AUTH_ON") == RegularConstants.NULL) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNAUTH_LEASE_DETAILS);//
				return resultDTO;
			}
			if (!resultDTO.get("PO_SL").equals(inputDTO.get("PURCH_SL"))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.PAY_NOT_BELONG_PO);// /
																							// NEW
				return resultDTO;
			}
			if (!validation.isEmpty(resultDTO.get("INVOICE_SL"))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.PAY_ASS_ALREADY);// NEW
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	private boolean validateGrid() {
		CommonValidator validation = new CommonValidator();
		boolean findCheckedRow = false;
		try {
			DTDObject dtdObject = new DTDObject();
			dtdObject.addColumn(0, "SELECT");
			dtdObject.addColumn(1, "PO_ENTRYSL");
			dtdObject.addColumn(2, "PO_DATE");
			dtdObject.addColumn(3, "PO_NUM");
			dtdObject.addColumn(4, "PO_CURR");
			dtdObject.addColumn(5, "PO_AMT");
			dtdObject.addColumn(6, "PO_OUTSTANDING");
			dtdObject.addColumn(7, "PO_OUTSTANDING_UNFORMAT");
			dtdObject.addColumn(8, "TOTPOUNFORMAT");
			dtdObject.addColumn(9, "SCHEDULEIDDISPLAY");
			dtdObject.setXML(xmlGrid);
			if (dtdObject.getRowCount() <= 0) {
				getErrorMap().setError("agreeNo", BackOfficeErrorCodes.ATLEAST_ONE_ROW);
				return false;
			}
			if (!validation.checkDuplicateRows(dtdObject, 1)) {
				getErrorMap().setError("agreeNo", BackOfficeErrorCodes.DUPLICATE_DATA);
				return false;
			}
			for (int i = 0; i < dtdObject.getRowCount(); i++) {
				if (dtdObject.getValue(i, 0).equals(RegularConstants.ONE)) {
					poDate = dtdObject.getValue(i, 2);
					outStandingAmt = dtdObject.getValue(i, 7);
					if (validation.isZeroAmount(outStandingAmt)) {
						outStandingAmt = dtdObject.getValue(i, 8);
					}
					findCheckedRow = true;
				}
			}
			if (!findCheckedRow) {
				getErrorMap().setError("invFor", BackOfficeErrorCodes.SELECT_ATLEAST_ONE_POGRID);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("agreeNo", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return true;
	}

	public boolean validateAssetClassification(String assetClassifi) {
		try {
			DTObject formDTO = getFormDTO();
			formDTO.set("ASSET_TYPE_CODE", assetClassifi);
			formDTO.set(ContentManager.ACTION, USAGE);
			formDTO = validateAssetClassification(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("assetClassifi", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("assetClassifi", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateAssetClassification(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		MasterValidator validator = new MasterValidator();
		String assetClassifi = inputDTO.get("ASSET_TYPE_CODE");
		try {
			if (validator.isEmpty(assetClassifi)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION,MODEL_NO,CONFIG_DTLS");
			resultDTO = validator.validateAssetType(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return resultDTO;
	}

	public boolean validateAssetDescription(String assetDesc) {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(assetDesc)) {
				getErrorMap().setError("assetDescription", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidRemarks(assetDesc)) {
				getErrorMap().setError("assetDescription", BackOfficeErrorCodes.INVALID_DESCRIPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("assetDescription", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateQuantity(String quantity) {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(quantity)) {
				getErrorMap().setError("quantity", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (validation.isZero(quantity)) {
				getErrorMap().setError("quantity", BackOfficeErrorCodes.ZERO_CHECK);
				return false;
			}
			if (!validation.isValidNumber(quantity)) {
				getErrorMap().setError("quantity", BackOfficeErrorCodes.INVALID_NUMBER);
				return false;
			}
			if (validation.isValidNegativeNumber(quantity)) {
				getErrorMap().setError("quantity", BackOfficeErrorCodes.POSITIVE_CHECK);
				return false;
			}
			if (validation.isNumberGreater(quantity, "99999")) {
				getErrorMap().setError("quantity", BackOfficeErrorCodes.INVALID_NUMBER);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("quantity", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateAssetModel(String assetModel, String modelNo) {
		CommonValidator validation = new CommonValidator();
		try {
			if (!modelNo.equals("N")) {
				if (modelNo.equals("M")) {
					if (validation.isEmpty(assetModel)) {
						getErrorMap().setError("assetModel", BackOfficeErrorCodes.FIELD_BLANK);
						return false;
					}
				}
				if (!validation.isEmpty(assetModel)) {
					if (!validation.isValidOtherInformation25(assetModel)) {
						getErrorMap().setError("assetModel", BackOfficeErrorCodes.INVALID_DESCRIPTION);
						return false;
					}
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("assetModel", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateConfiguration(String configuration, String configDtl) {
		CommonValidator validation = new CommonValidator();
		try {
			if (!configDtl.equals("N")) {
				if (configDtl.equals("M")) {
					if (validation.isEmpty(configuration)) {
						getErrorMap().setError("configuration", BackOfficeErrorCodes.FIELD_BLANK);
						return false;
					}
				}
				if (!validation.isEmpty(configuration)) {
					if (!validation.isValidOtherInformation25(configuration)) {
						getErrorMap().setError("configuration", BackOfficeErrorCodes.INVALID_DESCRIPTION);
						return false;
					}
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("configuration", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateValue(String value) {
		CommonValidator validator = new CommonValidator();
		DTObject formDTO = new DTObject();
		try {
			if (validator.isEmpty(value)) {
				getErrorMap().setError("valueFormat", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (validator.isZeroAmount(value)) {
				getErrorMap().setError("valueFormat", BackOfficeErrorCodes.AMOUNT_NOT_ZERO);
				return false;
			}
			if (validator.isAmountGreater(value, invAmt)) {
				getErrorMap().setError("valueFormat", BackOfficeErrorCodes.INVOICE_AMOUNT_EQUAL);
				return false;
			}
			formDTO.set("CURRENCY_CODE", context.getBaseCurrency());
			formDTO.set(ProgramConstants.AMOUNT, value);
			formDTO = validator.validateCurrencySmallAmount(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("valueFormat", formDTO.get(ContentManager.ERROR));
				return false;
			}
			if (validator.isValidNegativeAmount(value)) {
				getErrorMap().setError("valueFormat", BackOfficeErrorCodes.PBS_POSITIVE_AMOUNT);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("valueFormat", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return false;
	}

	// public boolean validateCST(String cst) {
	// CommonValidator validator = new CommonValidator();
	// DTObject formDTO = new DTObject();
	// try {
	// if (!validator.isEmpty(cst)) {
	// formDTO.set("CURRENCY_CODE", context.getBaseCurrency());
	// formDTO.set(ProgramConstants.AMOUNT, cst);
	// formDTO = validator.validateCurrencySmallAmount(formDTO);
	// if (formDTO.get(ContentManager.ERROR) != null) {
	// getErrorMap().setError("cstFormat", formDTO.get(ContentManager.ERROR));
	// return false;
	// }
	// if (validator.isValidNegativeAmount(cst)) {
	// getErrorMap().setError("cstFormat",
	// BackOfficeErrorCodes.PBS_POSITIVE_AMOUNT);
	// return false;
	// }
	// }
	// return true;
	// } catch (Exception e) {
	// e.printStackTrace();
	// getErrorMap().setError("cstFormat",
	// BackOfficeErrorCodes.UNSPECIFIED_ERROR);
	// } finally {
	// validator.close();
	// }
	// return false;
	// }
	//
	// public boolean validateVAT(String vat) {
	// CommonValidator validator = new CommonValidator();
	// DTObject formDTO = new DTObject();
	// try {
	// if (!validator.isEmpty(vat)) {
	// formDTO.set("CURRENCY_CODE", context.getBaseCurrency());
	// formDTO.set(ProgramConstants.AMOUNT, vat);
	// formDTO = validator.validateCurrencySmallAmount(formDTO);
	// if (formDTO.get(ContentManager.ERROR) != null) {
	// getErrorMap().setError("vatFormat", formDTO.get(ContentManager.ERROR));
	// return false;
	// }
	// if (validator.isValidNegativeAmount(vat)) {
	// getErrorMap().setError("vatFormat",
	// BackOfficeErrorCodes.PBS_POSITIVE_AMOUNT);
	// return false;
	// }
	// }
	// return true;
	// } catch (Exception e) {
	// e.printStackTrace();
	// getErrorMap().setError("vatFormat",
	// BackOfficeErrorCodes.UNSPECIFIED_ERROR);
	// } finally {
	// validator.close();
	// }
	// return false;
	// }
	//
	// public boolean validateST(String st) {
	// CommonValidator validator = new CommonValidator();
	// DTObject formDTO = new DTObject();
	// try {
	// if (!validator.isEmpty(st)) {
	// formDTO.set("CURRENCY_CODE", context.getBaseCurrency());
	// formDTO.set(ProgramConstants.AMOUNT, st);
	// formDTO = validator.validateCurrencySmallAmount(formDTO);
	// if (formDTO.get(ContentManager.ERROR) != null) {
	// getErrorMap().setError("stFormat", formDTO.get(ContentManager.ERROR));
	// return false;
	// }
	// if (validator.isValidNegativeAmount(st)) {
	// getErrorMap().setError("stFormat",
	// BackOfficeErrorCodes.PBS_POSITIVE_AMOUNT);
	// return false;
	// }
	// }
	// return true;
	// } catch (Exception e) {
	// e.printStackTrace();
	// getErrorMap().setError("stFormat",
	// BackOfficeErrorCodes.UNSPECIFIED_ERROR);
	// } finally {
	// validator.close();
	// }
	// return false;
	// }
	//
	// public boolean validateTCS(String tcs) {
	// CommonValidator validator = new CommonValidator();
	// DTObject formDTO = new DTObject();
	// try {
	// if (!validator.isEmpty(tcs)) {
	// formDTO.set("CURRENCY_CODE", context.getBaseCurrency());
	// formDTO.set(ProgramConstants.AMOUNT, tcs);
	// formDTO = validator.validateCurrencySmallAmount(formDTO);
	// if (formDTO.get(ContentManager.ERROR) != null) {
	// getErrorMap().setError("tcsFormat", formDTO.get(ContentManager.ERROR));
	// return false;
	// }
	// if (validator.isValidNegativeAmount(tcs)) {
	// getErrorMap().setError("tcsFormat",
	// BackOfficeErrorCodes.PBS_POSITIVE_AMOUNT);
	// return false;
	// }
	// }
	// return true;
	// } catch (Exception e) {
	// e.printStackTrace();
	// getErrorMap().setError("tcsFormat",
	// BackOfficeErrorCodes.UNSPECIFIED_ERROR);
	// } finally {
	// validator.close();
	// }
	// return false;
	// }
	//
	// public boolean validateVatOnAmount(String vatOnAmount, String vat) {
	// CommonValidator validator = new CommonValidator();
	// DTObject formDTO = new DTObject();
	// try {
	// if (!validator.isZeroAmount(vat)) {
	// if (validator.isEmpty(vatOnAmount)) {
	// getErrorMap().setError("vatOnAmountFormat",
	// BackOfficeErrorCodes.FIELD_BLANK);
	// return false;
	// }
	// formDTO.set("CURRENCY_CODE", vatOnAmount_curr);
	// formDTO.set(ProgramConstants.AMOUNT, vatOnAmount);
	// formDTO = validator.validateCurrencySmallAmount(formDTO);
	// if (formDTO.get(ContentManager.ERROR) != null) {
	// getErrorMap().setError("vatOnAmountFormat",
	// formDTO.get(ContentManager.ERROR));
	// return false;
	// }
	// if (validator.isZeroAmount(vatOnAmount)) {
	// getErrorMap().setError("vatOnAmountFormat",
	// BackOfficeErrorCodes.AMOUNT_NOT_ZERO);
	// return false;
	// }
	// if (validator.isValidNegativeAmount(vatOnAmount)) {
	// getErrorMap().setError("vatOnAmountFormat",
	// BackOfficeErrorCodes.PBS_POSITIVE_AMOUNT);
	// return false;
	// }
	// }
	// return true;
	// } catch (Exception e) {
	// e.printStackTrace();
	// getErrorMap().setError("vatOnAmountFormat",
	// BackOfficeErrorCodes.UNSPECIFIED_ERROR);
	// } finally {
	// validator.close();
	// }
	// return false;
	// }

	public boolean validateRemarks() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (remarks != null && remarks.length() > 0) {
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			if (getRectify().equals(RegularConstants.COLUMN_ENABLE)) {
				if (commonValidator.isEmpty(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}

	private boolean validateAssetDtlGrid() {
		CommonValidator validation = new CommonValidator();
		try {
			DTDObject dtdObject = new DTDObject();
			dtdObject.addColumn(0, "SL");
			// dtdObject.addColumn(1, "LINK");
			dtdObject.addColumn(1, "ASSET_TYPE_CODE");
			dtdObject.addColumn(2, "ASSET_DESCRIPTION");
			dtdObject.addColumn(3, "QUANTITY");
			dtdObject.addColumn(4, "ASSET_MODEL");
			dtdObject.addColumn(5, "ASSET_CONFIG");
			dtdObject.addColumn(6, "ASSET_VALUE");
			dtdObject.addColumn(7, "VALUE");
			// dtdObject.addColumn(9, "ASSET_VAT");
			// dtdObject.addColumn(10, "ASSET_ST");
			// dtdObject.addColumn(11, "ASSET_TCS");
			// dtdObject.addColumn(12, "ASSET_VAT_ON_AMOUNT");
			// dtdObject.addColumn(13, "TOTAL");
			// dtdObject.addColumn(14, "VALUE");
			// dtdObject.addColumn(15, "CST");
			// dtdObject.addColumn(16, "VAT");
			// dtdObject.addColumn(17, "ST");
			// dtdObject.addColumn(18, "TCS");
			// dtdObject.addColumn(19, "VAT_ON_AMOUNT");
			dtdObject.addColumn(8, "ASSET_MODEL");
			dtdObject.addColumn(9, "ASSET_CONFIG");
			dtdObject.setXML(xmleleasepoinvassetGrid);
			if (dtdObject.getRowCount() <= 0) {
				getErrorMap().setError("assetClassifi", BackOfficeErrorCodes.ATLEAST_ONE_ROW);
				return false;
			}
			if (!validation.isAmountEqual(invAmt, totalValueAmount)) {
				getErrorMap().setError("assetClassifi", BackOfficeErrorCodes.VALUE_AMOUNT_EQUAL);
				return false;
			}
			for (int i = 0; i < dtdObject.getRowCount(); i++) {
				if (!validateAssetClassification(dtdObject.getValue(i, 1))) {
					getErrorMap().setError("assetClassifi", BackOfficeErrorCodes.INVALID_ACTION);
					return false;
				}
				if (!validateAssetDescription(dtdObject.getValue(i, 2))) {
					getErrorMap().setError("quantity", BackOfficeErrorCodes.INVALID_ACTION);
					return false;
				}
				if (!validateQuantity(dtdObject.getValue(i, 3))) {
					getErrorMap().setError("quantity", BackOfficeErrorCodes.INVALID_ACTION);
					return false;
				}
				if (!validateAssetModel(dtdObject.getValue(i, 4), dtdObject.getValue(i, 8))) {
					getErrorMap().setError("assetModel", BackOfficeErrorCodes.INVALID_ACTION);
					return false;
				}
				if (!validateConfiguration(dtdObject.getValue(i, 5), dtdObject.getValue(i, 9))) {
					getErrorMap().setError("configuration", BackOfficeErrorCodes.INVALID_ACTION);
					return false;
				}
				if (!validateValue(dtdObject.getValue(i, 7))) {
					getErrorMap().setError("valueFormat", BackOfficeErrorCodes.INVALID_ACTION);
					return false;
				}
				// if (!validateCST(dtdObject.getValue(i, 15))) {
				// getErrorMap().setError("cstFormat",
				// BackOfficeErrorCodes.INVALID_ACTION);
				// return false;
				// }
				// if (!validateVAT(dtdObject.getValue(i, 16))) {
				// getErrorMap().setError("vatFormat",
				// BackOfficeErrorCodes.INVALID_ACTION);
				// return false;
				// }
				// if (!validateST(dtdObject.getValue(i, 17))) {
				// getErrorMap().setError("stFormat",
				// BackOfficeErrorCodes.INVALID_ACTION);
				// return false;
				// }
				// if (!validateTCS(dtdObject.getValue(i, 18))) {
				// getErrorMap().setError("tcsFormat",
				// BackOfficeErrorCodes.INVALID_ACTION);
				// return false;
				// }
				// if (!validateVatOnAmount(dtdObject.getValue(i, 19),
				// dtdObject.getValue(i, 16))) {
				// getErrorMap().setError("vatOnAmountFormat",
				// BackOfficeErrorCodes.INVALID_ACTION);
				// return false;
				// }
			}

			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("assetClassifi", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

}
