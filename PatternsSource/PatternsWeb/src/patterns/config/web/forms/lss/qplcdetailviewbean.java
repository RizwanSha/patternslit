package patterns.config.web.forms.lss;

import java.math.BigDecimal;
import java.sql.ResultSet;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.framework.web.DHTMLXGridUtility;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.framework.web.reports.ReportRM;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.LeaseValidator;
import patterns.config.validations.MasterValidator;
import patterns.config.web.forms.GenericFormBean;

public class qplcdetailviewbean extends GenericFormBean {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String preLeaseReference;
	private String preLeaseReferenceDate;
	private String preLeaseReferenceSerial;
	private String lesseCode;
	private String customerID;
	private String customerName;
	private String leaseAgreementSl;
	private String scheduleID;
	private String preLeaseContractRefParam;

	public String getPreLeaseReference() {
		return preLeaseReference;
	}

	public void setPreLeaseReference(String preLeaseReference) {
		this.preLeaseReference = preLeaseReference;
	}

	public String getPreLeaseReferenceDate() {
		return preLeaseReferenceDate;
	}

	public void setPreLeaseReferenceDate(String preLeaseReferenceDate) {
		this.preLeaseReferenceDate = preLeaseReferenceDate;
	}

	public void setPreLeaseReferenceSerial(String preLeaseReferenceSerial) {
		this.preLeaseReferenceSerial = preLeaseReferenceSerial;
	}

	public String getPreLeaseReferenceSerial() {
		return preLeaseReferenceSerial;
	}

	public String getLesseCode() {
		return lesseCode;
	}

	public void setLesseCode(String lesseCode) {
		this.lesseCode = lesseCode;
	}

	public String getCustomerID() {
		return customerID;
	}

	public void setCustomerID(String customerID) {
		this.customerID = customerID;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getLeaseAgreementSl() {
		return leaseAgreementSl;
	}

	public void setLeaseAgreementSl(String leaseAgreementSl) {
		this.leaseAgreementSl = leaseAgreementSl;
	}

	public String getScheduleID() {
		return scheduleID;
	}

	public void setScheduleID(String scheduleID) {
		this.scheduleID = scheduleID;
	}

	public String getPreLeaseContractRefParam() {
		return preLeaseContractRefParam;
	}

	public void setPreLeaseContractRefParam(String preLeaseContractRefParam) {
		this.preLeaseContractRefParam = preLeaseContractRefParam;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public void reset() {
		preLeaseReference = RegularConstants.EMPTY_STRING;
		preLeaseReferenceDate = RegularConstants.EMPTY_STRING;
		preLeaseReferenceSerial = RegularConstants.EMPTY_STRING;
		lesseCode = RegularConstants.EMPTY_STRING;
		customerID = RegularConstants.EMPTY_STRING;
		customerName = RegularConstants.EMPTY_STRING;
		leaseAgreementSl = RegularConstants.EMPTY_STRING;
		scheduleID = RegularConstants.EMPTY_STRING;
		preLeaseContractRefParam = RegularConstants.EMPTY_STRING;
		if (webContext.getRequest().getParameter("pk") != null) {
			preLeaseContractRefParam = webContext.getRequest().getParameter("pk");
		}
	}

	@Override
	public void validate() {
	}

	public DTObject getPreLeaseContractDetails(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		DTObject custmerDTO = new DTObject();
		LeaseValidator validation = new LeaseValidator();
		MasterValidator validator = new MasterValidator();
		String preLeaseDate = inputDTO.get("CONTRACT_DATE");
		String preLeaseSerial = inputDTO.get("CONTRACT_SL");
		try {
			if (validation.isEmpty(preLeaseDate) || validation.isEmpty(preLeaseSerial)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			if (validation.isValidDate(preLeaseDate) == null) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_DATE);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "CUSTOMER_ID,SUNDRY_DB_AC,AGREEMENT_NO,SCHEDULE_ID");
			resultDTO = validation.validatePreLeaseContractReferenceSerial(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.REFERENCE_DOES_NOT_EXIST);
			}
			inputDTO.set("CUSTOMER_ID", resultDTO.get("CUSTOMER_ID"));
			inputDTO.set(ContentManager.FETCH_COLUMNS, "CUSTOMER_NAME");
			custmerDTO = validator.valildateCustomerID(inputDTO);
			if (custmerDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, custmerDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (ContentManager.DATA_UNAVAILABLE.equals(custmerDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
			}
			resultDTO.set("CUSTOMER_NAME", custmerDTO.get("CUSTOMER_NAME"));
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
			validator.close();
		}
		return resultDTO;
	}

	public DTObject getPurchaseOrderDetails(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		CommonValidator validator = new CommonValidator();
		StringBuffer sqlQuery = new StringBuffer();

		DBUtil util = validator.getDbContext().createUtilInstance();
		String preLeaseDate = inputDTO.get("CONTRACT_DATE");
		String preLeaseSerial = inputDTO.get("CONTRACT_SL");
		BigDecimal totalAmount = new BigDecimal("0");
		BigDecimal totalAmountPaidSoFar = new BigDecimal("0");
		int invoiceCount = 0;
		String currency = RegularConstants.EMPTY_STRING;
		String currUnit = RegularConstants.EMPTY_STRING;
		try {
			sqlQuery.append("SELECT LEASEPO.PO_SL,TO_CHAR(LEASEPO.PO_DATE,?) PO_DATE,LEASEPO.PO_NUMBER,LEASEPO.SUPPLIER_ID,SUPPLIER.NAME SUPPLY_DESC,LEASEPO.PO_CCY,");
			sqlQuery.append("FN_FORMAT_AMOUNT(LEASEPO.TOTAL_PO_AMOUNT,LEASEPO.PO_CCY,C.SUB_CCY_UNITS_IN_CCY) FORMAT_PO_AMOUNT,LEASEPO.TOTAL_PO_AMOUNT,");
			sqlQuery.append("FN_FORMAT_AMOUNT(LEASEPO.PO_AMOUNT_PAID_SO_FAR,LEASEPO.PO_CCY,C.SUB_CCY_UNITS_IN_CCY) FRMT_PAID_SO_FAR,LEASEPO.PO_AMOUNT_PAID_SO_FAR,COUNT(LEASEPOINV.INVOICE_SL) INVOICE_SL,C.SUB_CCY_UNITS_IN_CCY ");
			sqlQuery.append("FROM LEASEPO LEASEPO ");
			sqlQuery.append("JOIN LEASEPOINV LEASEPOINV ON(LEASEPOINV.ENTITY_CODE=LEASEPO.ENTITY_CODE AND LEASEPOINV.CONTRACT_DATE=LEASEPO.CONTRACT_DATE AND LEASEPOINV.CONTRACT_SL=LEASEPO.CONTRACT_SL AND LEASEPOINV.PO_SL=LEASEPO.PO_SL) ");
			sqlQuery.append("JOIN SUPPLIER SUPPLIER ON (SUPPLIER.ENTITY_CODE=LEASEPO.ENTITY_CODE AND SUPPLIER.SUPPLIER_ID=LEASEPO.SUPPLIER_ID) ");
			sqlQuery.append("JOIN CURRENCY C ON (C.CCY_CODE=LEASEPO.PO_CCY) ");
			sqlQuery.append("WHERE LEASEPO.ENTITY_CODE=? AND LEASEPO.CONTRACT_DATE=? AND LEASEPO.CONTRACT_SL=? AND LEASEPO.AUTH_ON IS NOT NULL GROUP BY LEASEPOINV.INVOICE_SL ORDER BY LEASEPOINV.INVOICE_SL ");
			util.reset();
			util.setSql(sqlQuery.toString());
			util.setString(1, context.getDateFormat());
			util.setLong(2, Long.parseLong(context.getEntityCode()));
			util.setDate(3, new java.sql.Date(BackOfficeFormatUtils.getDate(preLeaseDate, BackOfficeConstants.JAVA_DATE_FORMAT).getTime()));
			util.setInt(4, Integer.parseInt(preLeaseSerial));
			ResultSet rset = util.executeQuery();
			DHTMLXGridUtility gridUtility = new DHTMLXGridUtility();
			if (rset.next()) {
				gridUtility.init();
				do {
					gridUtility.startRow();
					gridUtility.setCell("" + rset.getString("PO_SL") + "^javascript:viewLeasePODetails(\"" + rset.getString("PO_SL") + "\")");
					gridUtility.setCell(rset.getString("PO_DATE"));
					gridUtility.setCell(rset.getString("PO_NUMBER"));
					gridUtility.setCell(rset.getString("SUPPLIER_ID"));
					gridUtility.setCell(rset.getString("SUPPLY_DESC"));
					gridUtility.setCell(rset.getString("PO_CCY"));
					gridUtility.setCell(rset.getString("FORMAT_PO_AMOUNT"));
					gridUtility.setCell("" + rset.getString("FRMT_PAID_SO_FAR") + "^javascript:showPaymentDetails(\"" + rset.getString("PO_SL") + "\")");
					gridUtility.setCell("" + rset.getString("INVOICE_SL") + "^javascript:showInvoicesDetails(\"" + rset.getString("PO_SL") + "\")");
					totalAmount = totalAmount.add(rset.getBigDecimal("TOTAL_PO_AMOUNT"));
					totalAmountPaidSoFar = totalAmountPaidSoFar.add(rset.getBigDecimal("PO_AMOUNT_PAID_SO_FAR"));
					currency = rset.getString("PO_CCY");
					currUnit = rset.getString("SUB_CCY_UNITS_IN_CCY");
					invoiceCount = invoiceCount + rset.getInt(11);
					gridUtility.endRow();
				} while (rset.next());
				gridUtility.finish();
				String resultXML = gridUtility.getXML();
				resultDTO.set(ContentManager.RESULT_XML, resultXML);
				String totalAmt = ReportRM.fmc(totalAmount, currency, currUnit);
				String totalAmntPaidSofar = ReportRM.fmc(totalAmountPaidSoFar, currency, currUnit);
				resultDTO.set("TOTAL_AMOUNT", totalAmt);
				resultDTO.set("TOTAL_AMOUNT_PAID_SO_FAR", totalAmntPaidSofar);
				resultDTO.set("TOTAL_INVOICE_COUNT", String.valueOf(invoiceCount));
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return resultDTO;
	}

	public DTObject getInvoiceDetails(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		CommonValidator validator = new CommonValidator();
		StringBuffer sqlQuery = new StringBuffer();
		DBUtil util = validator.getDbContext().createUtilInstance();
		String preLeaseDate = inputDTO.get("CONTRACT_DATE");
		String preLeaseSerial = inputDTO.get("CONTRACT_SL");
		String poSerial = inputDTO.get("PO_SL");
		try {
			sqlQuery.append("SELECT LEASEPOINV.PO_SL,LEASEPOINV.INVOICE_SL,TO_CHAR(LEASEPOINV.INVOICE_DATE,?) INVOICE_DATE,LEASEPOINV.INVOICE_NUMBER,");
			sqlQuery.append("FN_FORMAT_AMOUNT(LEASEPOINV.INVOICE_AMOUNT,LEASEPOINV.INVOICE_CCY,C.SUB_CCY_UNITS_IN_CCY) FORMAT_INVOICE_AMOUNT,");
			sqlQuery.append("FN_FORMAT_AMOUNT(LEASEPOINV.INVOICE_AMOUNT_PAID_SOFAR,LEASEPOINV.INVOICE_CCY,C.SUB_CCY_UNITS_IN_CCY) FORMAT_INVOICE_AMOUNT_PAID_SO_FAR,LEASEPOINV.INVOICE_AMOUNT_PAID_SOFAR ");
			sqlQuery.append("FROM LEASEPOINV LEASEPOINV ");
			sqlQuery.append("JOIN CURRENCY C ON (C.CCY_CODE=LEASEPOINV.INVOICE_CCY) ");
			sqlQuery.append("WHERE LEASEPOINV.ENTITY_CODE=? AND LEASEPOINV.CONTRACT_DATE=? AND LEASEPOINV.CONTRACT_SL=? AND LEASEPOINV.PO_SL=? AND LEASEPOINV.AUTH_ON IS NOT NULL");
			util.reset();
			util.setSql(sqlQuery.toString());
			util.setString(1, context.getDateFormat());
			util.setLong(2, Long.parseLong(context.getEntityCode()));
			util.setDate(3, new java.sql.Date(BackOfficeFormatUtils.getDate(preLeaseDate, BackOfficeConstants.JAVA_DATE_FORMAT).getTime()));
			util.setInt(4, Integer.parseInt(preLeaseSerial));
			util.setInt(5, Integer.parseInt(poSerial));
			ResultSet rset = util.executeQuery();
			DHTMLXGridUtility gridUtility = new DHTMLXGridUtility();
			if (rset.next()) {
				gridUtility.init();
				do {
					gridUtility.startRow();
					gridUtility.setCell("" + rset.getString("INVOICE_SL") + "^javascript:viewLeasePOInvDetails(\"" + rset.getString("PO_SL") + "\",\"" + rset.getString("INVOICE_SL") + "\")");
					gridUtility.setCell(rset.getString("INVOICE_DATE"));
					gridUtility.setCell(rset.getString("INVOICE_NUMBER"));
					gridUtility.setCell(rset.getString("FORMAT_INVOICE_AMOUNT"));
					gridUtility.setCell("" + rset.getString("FORMAT_INVOICE_AMOUNT_PAID_SO_FAR") + "^javascript:showPaymentDetails(\"" + rset.getString("PO_SL") + "\",\"" + rset.getString("INVOICE_SL") + "\")");
					gridUtility.endRow();
				} while (rset.next());
				gridUtility.finish();
				String resultXML = gridUtility.getXML();
				resultDTO.set(ContentManager.RESULT_XML, resultXML);
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return resultDTO;
	}

	public DTObject getPaymentDetails(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		CommonValidator validator = new CommonValidator();
		StringBuffer sqlQuery = new StringBuffer();
		DBUtil util = validator.getDbContext().createUtilInstance();
		String preLeaseDate = inputDTO.get("CONTRACT_DATE");
		String preLeaseSerial = inputDTO.get("CONTRACT_SL");
		String poSerial = inputDTO.get("PO_SL");
		String invoiceSerial = inputDTO.get("INVOICE_SL");
		BigDecimal totalPaymentAmount = new BigDecimal("0");
		BigDecimal totalInterestAmount = new BigDecimal("0");
		String currency = RegularConstants.EMPTY_STRING;
		String currUnit = RegularConstants.EMPTY_STRING;
		try {
			sqlQuery.append("SELECT LEASEPOINVPAY.PAYMENT_SL,TO_CHAR(LEASEPOINVPAY.PAYMENT_DATE,?) PAYMENT_DATE,");
			sqlQuery.append("FN_FORMAT_AMOUNT(LEASEPOINVPAY.PAYMENT_AMOUNT,LEASEPOINVPAY.PAYMENT_CCY,C.SUB_CCY_UNITS_IN_CCY) FORMAT_PAYMENT_AMOUNT,");
			sqlQuery.append("LEASEPOINVPAY.PAYMENT_AMOUNT,LEASEPOINVPAY.PAYMENT_REFERENCE,CMLOV.LOV_LABEL_EN_US PAID_AGAINST,PLCINTERESTLED.INTEREST_CCY,C.SUB_CCY_UNITS_IN_CCY,");
			sqlQuery.append("FN_FORMAT_AMOUNT(SUM(PLCINTERESTLED.INTEREST_AMOUNT),PLCINTERESTLED.INTEREST_CCY,C.SUB_CCY_UNITS_IN_CCY) FORMAT_INTEREST_AMOUNT,SUM(PLCINTERESTLED.INTEREST_AMOUNT) INTEREST_AMOUNT ");
			sqlQuery.append("FROM LEASEPOINVPAY LEASEPOINVPAY ");
			sqlQuery.append("JOIN PLCINTERESTLED PLCINTERESTLED ON (PLCINTERESTLED.ENTITY_CODE=LEASEPOINVPAY.ENTITY_CODE AND ");
			sqlQuery.append("PLCINTERESTLED.CONTRACT_DATE=LEASEPOINVPAY.CONTRACT_DATE AND PLCINTERESTLED.CONTRACT_SL=LEASEPOINVPAY.CONTRACT_SL AND PLCINTERESTLED.PAYMENT_SL=LEASEPOINVPAY.PAYMENT_SL) ");
			sqlQuery.append("JOIN CURRENCY C ON (C.CCY_CODE=LEASEPOINVPAY.PAYMENT_CCY) ");
			sqlQuery.append("LEFT OUTER JOIN CMLOVREC CMLOV ON (CMLOV.PGM_ID='COMMON' AND CMLOV.LOV_ID='PAY_FOR' AND CMLOV.LOV_VALUE=LEASEPOINVPAY.PAY_FOR) ");
			sqlQuery.append("WHERE LEASEPOINVPAY.ENTITY_CODE=? AND LEASEPOINVPAY.CONTRACT_DATE=? AND LEASEPOINVPAY.CONTRACT_SL=? AND LEASEPOINVPAY.PO_SL=? AND LEASEPOINVPAY.AUTH_ON IS NOT NULL");
			if (!validator.isEmpty(invoiceSerial)) {
				sqlQuery.append(" AND LEASEPOINVPAY.INVOICE_SL=?");
			}
			sqlQuery.append(" GROUP BY LEASEPOINVPAY.PAYMENT_SL ORDER BY LEASEPOINVPAY.PAYMENT_SL");
			util.reset();
			util.setSql(sqlQuery.toString());
			util.setString(1, context.getDateFormat());
			util.setLong(2, Long.parseLong(context.getEntityCode()));
			util.setDate(3, new java.sql.Date(BackOfficeFormatUtils.getDate(preLeaseDate, BackOfficeConstants.JAVA_DATE_FORMAT).getTime()));
			util.setInt(4, Integer.parseInt(preLeaseSerial));
			util.setInt(5, Integer.parseInt(poSerial));
			if (!validator.isEmpty(invoiceSerial)) {
				util.setInt(6, Integer.parseInt(invoiceSerial));
			}
			ResultSet rset = util.executeQuery();
			DHTMLXGridUtility gridUtility = new DHTMLXGridUtility();
			if (rset.next()) {
				gridUtility.init();
				do {
					gridUtility.startRow();
					gridUtility.setCell("" + rset.getString("PAYMENT_SL") + "^javascript:viewLeasePOInvPayDetails(\"" + rset.getString("PAYMENT_SL") + "\")");
					gridUtility.setCell(rset.getString("PAYMENT_DATE"));
					gridUtility.setCell(rset.getString("FORMAT_PAYMENT_AMOUNT"));
					gridUtility.setCell(rset.getString("PAYMENT_REFERENCE"));
					gridUtility.setCell(rset.getString("PAID_AGAINST"));
					gridUtility.setCell("" + rset.getString("FORMAT_INTEREST_AMOUNT") + "^javascript:showInterimInterestDetails(\"" + rset.getString("PAYMENT_SL") + "\")");
					gridUtility.setCell(invoiceSerial);
					totalPaymentAmount = totalPaymentAmount.add(rset.getBigDecimal("PAYMENT_AMOUNT"));
					totalInterestAmount = totalInterestAmount.add(rset.getBigDecimal("INTEREST_AMOUNT"));
					currency = rset.getString("INTEREST_CCY");
					currUnit = rset.getString("SUB_CCY_UNITS_IN_CCY");
					gridUtility.endRow();
				} while (rset.next());
				gridUtility.finish();
				String resultXML = gridUtility.getXML();
				resultDTO.set(ContentManager.RESULT_XML, resultXML);
				String totalAmt = ReportRM.fmc(totalPaymentAmount, currency, currUnit);
				String totalInterestAmnt = ReportRM.fmc(totalInterestAmount, currency, currUnit);
				resultDTO.set("TOTAL_AMOUNT", totalAmt);
				resultDTO.set("TOTAL_INTEREST_AMOUNT", totalInterestAmnt);
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return resultDTO;
	}

	public DTObject getInterimInterestDetails(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		CommonValidator validator = new CommonValidator();
		StringBuffer sqlQuery = new StringBuffer();
		DBUtil util = validator.getDbContext().createUtilInstance();
		String preLeaseDate = inputDTO.get("CONTRACT_DATE");
		String preLeaseSerial = inputDTO.get("CONTRACT_SL");
		String paymentSerial = inputDTO.get("PAYMENT_SL");
		BigDecimal totalInterestAmount = new BigDecimal("0");
		String currency = RegularConstants.EMPTY_STRING;
		String currUnit = RegularConstants.EMPTY_STRING;
		String forYearMonth = RegularConstants.EMPTY_STRING;
		String forMonth = RegularConstants.EMPTY_STRING;
		int noOfDays = 0;
		try {
			sqlQuery.append("SELECT PLCINTERESTLED.FOR_YEAR,PLCINTERESTLED.FOR_MONTH,TO_CHAR(PLCINTERESTLED.FROM_DATE,?) FROM_DATE,");
			sqlQuery.append("TO_CHAR(PLCINTERESTLED.UPTO_DATE,?) UPTO_DATE ,PLCINTERESTLED.NOF_DAYS,PLCINTERESTLED.INTEREST_RATE,PLCINTERESTLED.INTEREST_CCY,C.SUB_CCY_UNITS_IN_CCY,");
			sqlQuery.append("FN_FORMAT_AMOUNT(PLCINTERESTLED.INTEREST_AMOUNT,PLCINTERESTLED.INTEREST_CCY,C.SUB_CCY_UNITS_IN_CCY) FORMAT_INTEREST_AMOUNT,PLCINTERESTLED.INTEREST_AMOUNT ");
			sqlQuery.append("FROM PLCINTERESTLED PLCINTERESTLED ");
			sqlQuery.append("JOIN CURRENCY C ON (C.CCY_CODE=PLCINTERESTLED.INTEREST_CCY) ");
			sqlQuery.append("WHERE PLCINTERESTLED.ENTITY_CODE=? AND PLCINTERESTLED.CONTRACT_DATE=? AND PLCINTERESTLED.CONTRACT_SL=? ");
			sqlQuery.append("AND PLCINTERESTLED.PAYMENT_SL=? ORDER BY PLCINTERESTLED.PAYMENT_SL");
			util.reset();
			util.setSql(sqlQuery.toString());
			util.setString(1, context.getDateFormat());
			util.setString(2, context.getDateFormat());
			util.setLong(3, Long.parseLong(context.getEntityCode()));
			util.setDate(4, new java.sql.Date(BackOfficeFormatUtils.getDate(preLeaseDate, BackOfficeConstants.JAVA_DATE_FORMAT).getTime()));
			util.setInt(5, Integer.parseInt(preLeaseSerial));
			util.setInt(6, Integer.parseInt(paymentSerial));
			ResultSet rset = util.executeQuery();
			DHTMLXGridUtility gridUtility = new DHTMLXGridUtility();
			if (rset.next()) {
				gridUtility.init();
				do {
					gridUtility.startRow();
					forMonth = rset.getString("FOR_MONTH");
					if (forMonth.length() == 1)
						forMonth = 0 + rset.getString("FOR_MONTH");
					forYearMonth = rset.getString("FOR_YEAR") + "-" + forMonth;
					gridUtility.setCell(forYearMonth);
					gridUtility.setCell(rset.getString("FROM_DATE"));
					gridUtility.setCell(rset.getString("UPTO_DATE"));
					gridUtility.setCell(rset.getString("NOF_DAYS"));
					gridUtility.setCell(rset.getString("INTEREST_RATE"));
					gridUtility.setCell(rset.getString("FORMAT_INTEREST_AMOUNT"));
					totalInterestAmount = totalInterestAmount.add(rset.getBigDecimal("INTEREST_AMOUNT"));
					currency = rset.getString("INTEREST_CCY");
					currUnit = rset.getString("SUB_CCY_UNITS_IN_CCY");
					noOfDays = noOfDays + rset.getInt(5);
					gridUtility.endRow();
				} while (rset.next());
				gridUtility.finish();
				String resultXML = gridUtility.getXML();
				resultDTO.set(ContentManager.RESULT_XML, resultXML);
				String totalAmt = ReportRM.fmc(totalInterestAmount, currency, currUnit);
				resultDTO.set("TOTAL_INTEREST_AMOUNT", totalAmt);
				resultDTO.set("TOTAL_NO_OF_DAYS", String.valueOf(noOfDays));
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return resultDTO;
	}

}
