package patterns.config.web.forms.inv;

import java.sql.ResultSet;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.framework.web.DHTMLXGridUtility;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.LeaseValidator;
import patterns.config.validations.MasterValidator;
import patterns.config.validations.RegistrationValidator;
import patterns.config.web.forms.GenericFormBean;

public class qinvcomponentbean extends GenericFormBean {

	private static final long serialVersionUID = 1L;
	private String invoiceDate;
	private String invoiceCycleNumber;
	private boolean intialInterim;
	private boolean monthlyRent;
	private boolean finalInterim;
	private boolean interest;
	private String leaseProductCode;
	private String customerCode;
	private String agreementNo;
	private String scheduleId;
	private String customerId;

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public boolean isIntialInterim() {
		return intialInterim;
	}

	public void setIntialInterim(boolean intialInterim) {
		this.intialInterim = intialInterim;
	}

	public boolean isMonthlyRent() {
		return monthlyRent;
	}

	public void setMonthlyRent(boolean monthlyRent) {
		this.monthlyRent = monthlyRent;
	}

	public boolean isFinalInterim() {
		return finalInterim;
	}

	public void setFinalInterim(boolean finalInterim) {
		this.finalInterim = finalInterim;
	}

	public boolean isInterest() {
		return interest;
	}

	public void setInterest(boolean interest) {
		this.interest = interest;
	}

	public String getLeaseProductCode() {
		return leaseProductCode;
	}

	public void setLeaseProductCode(String leaseProductCode) {
		this.leaseProductCode = leaseProductCode;
	}

	public String getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(String invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public String getInvoiceCycleNumber() {
		return invoiceCycleNumber;
	}

	public void setInvoiceCycleNumber(String invoiceCycleNumber) {
		this.invoiceCycleNumber = invoiceCycleNumber;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getAgreementNo() {
		return agreementNo;
	}

	public void setAgreementNo(String agreementNo) {
		this.agreementNo = agreementNo;
	}

	public String getScheduleId() {
		return scheduleId;
	}

	public void setScheduleId(String scheduleId) {
		this.scheduleId = scheduleId;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public void reset() {

		invoiceDate = RegularConstants.EMPTY_STRING;
		invoiceCycleNumber = RegularConstants.EMPTY_STRING;
		intialInterim = false;
		monthlyRent = false;
		finalInterim = false;
		interest = false;
		customerCode = RegularConstants.EMPTY_STRING;
		agreementNo = RegularConstants.EMPTY_STRING;
		scheduleId = RegularConstants.EMPTY_STRING;
	}

	@Override
	public void validate() {

	}

	public DTObject validate(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		if (!validateInvoiceDate()) {
			resultDTO.set(ContentManager.ERROR, getErrorMap().getErrorKey("invoiceDate"));
			resultDTO.set(ContentManager.ERROR_FIELD, "invoiceDate");
			return resultDTO;
		}
		if (!validateInvoiceCycleNumber()) {
			resultDTO.set(ContentManager.ERROR, getErrorMap().getErrorKey("invoiceCycleNumber"));
			resultDTO.set(ContentManager.ERROR_FIELD, "invoiceCycleNumber");
			return resultDTO;
		}
		if (!validateLeaseProductCode()) {
			resultDTO.set(ContentManager.ERROR, getErrorMap().getErrorKey("leaseProductCode"));
			resultDTO.set(ContentManager.ERROR_FIELD, "leaseProductCode");
			return resultDTO;
		}
		if (!validatecustCode()) {
			resultDTO.set(ContentManager.ERROR, getErrorMap().getErrorKey("customerCode"));
			resultDTO.set(ContentManager.ERROR_FIELD, "customerCode");
			return resultDTO;
		}
		if (!validateAgreementNo()) {
			if (getErrorMap().getErrorKey("agreementNo") != null) {
				resultDTO.set(ContentManager.ERROR, getErrorMap().getErrorKey("agreementNo"));
				resultDTO.set(ContentManager.ERROR_FIELD, "agreementNo");
			} else if (getErrorMap().getErrorKey("customerCode") != null) {
				resultDTO.set(ContentManager.ERROR, getErrorMap().getErrorKey("customerCode"));
				resultDTO.set(ContentManager.ERROR_FIELD, "customerCode");
			}
			return resultDTO;
		}
		if (!validateScheduleID()) {
			if (getErrorMap().getErrorKey("scheduleId") != null) {
				resultDTO.set(ContentManager.ERROR, getErrorMap().getErrorKey("scheduleId"));
				resultDTO.set(ContentManager.ERROR_FIELD, "agreementNo");
			} else if (getErrorMap().getErrorKey("customerCode") != null) {
				resultDTO.set(ContentManager.ERROR, getErrorMap().getErrorKey("customerCode"));
				resultDTO.set(ContentManager.ERROR_FIELD, "customerCode");
			}
			return resultDTO;
		
		}
		return resultDTO;
	}

	private boolean validateInvoiceDate() {
		CommonValidator validator = new CommonValidator();
		try {
			if (validator.isEmpty(invoiceDate)) {
				getErrorMap().setError("invoiceDate", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (validator.isValidDate(invoiceDate) == null) {
				getErrorMap().setError("invoiceDate", BackOfficeErrorCodes.INVALID_DATE);
				return false;
			}
			if (validator.isDateLesserCBD(invoiceDate)) {
				getErrorMap().setError("invoiceDate", BackOfficeErrorCodes.DATE_LECBD);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("invoiceDate", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return false;
	}

	private boolean validateInvoiceCycleNumber() {
		try {
			if (!invoiceCycleNumber.isEmpty()) {
				DTObject formDTO = new DTObject();
				formDTO.set("INV_CYCLE_NUMBER", invoiceCycleNumber);
				formDTO.set(ContentManager.ACTION, USAGE);
				formDTO = ValidateInvoiceCycleNumber(formDTO);
				if (formDTO.get(ContentManager.ERROR) != null) {
					getErrorMap().setError("invoiceCycleNumber", formDTO.get(ContentManager.ERROR));
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("invoiceCycleNumber", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject ValidateInvoiceCycleNumber(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		MasterValidator validation = new MasterValidator();
		String invoiceCycleNumber = inputDTO.get("INV_CYCLE_NUMBER");
		try {
			if (!validation.isEmpty(invoiceCycleNumber)) {
				inputDTO.set(ContentManager.FETCH_COLUMNS, "INV_CYCLE_NUMBER,INV_RENTAL_DAY_FROM,INV_RENTAL_DAY_UPTO");
				resultDTO = validation.validateInvoiceCycleNumber(inputDTO);
				if (resultDTO.get(ContentManager.ERROR) != null) {
					resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
					return resultDTO;
				}
				if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
					return resultDTO;
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	private boolean validateLeaseProductCode() {
		CommonValidator validator = new CommonValidator();
		try {
			if (!validator.isEmpty(leaseProductCode)) {
				DTObject formDTO = new DTObject();
				formDTO.set("LEASE_PRODUCT_CODE", leaseProductCode);
				formDTO.set(ContentManager.ACTION, USAGE);
				formDTO = ValidateLeaseProductCode(formDTO);
				if (formDTO.get(ContentManager.ERROR) != null) {
					getErrorMap().setError("leaseProductCode", formDTO.get(ContentManager.ERROR));
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("leaseProductCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return false;
	}

	public DTObject ValidateLeaseProductCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		LeaseValidator validation = new LeaseValidator();
		String leaseProductCode = inputDTO.get("LEASE_PRODUCT_CODE");
		try {
			if (!validation.isEmpty(leaseProductCode)) {
				inputDTO.set(ContentManager.FETCH_COLUMNS, "LEASE_PRODUCT_CODE,DESCRIPTION");
				resultDTO = validation.validateLeaseProductCode(inputDTO);
				if (resultDTO.get(ContentManager.ERROR) != null) {
					resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
					return resultDTO;
				}
				if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
					return resultDTO;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	private boolean validatecustCode() {
		CommonValidator validator = new CommonValidator();
		try {
			if (!validator.isEmpty(customerCode)) {
				DTObject formDTO = new DTObject();
				formDTO.set("SUNDRY_DB_AC", customerCode);
				formDTO.set(ContentManager.ACTION, USAGE);
				formDTO = ValidateCustomerCode(formDTO);
				if (formDTO.get(ContentManager.ERROR) != null) {
					getErrorMap().setError("customerCode", formDTO.get(ContentManager.ERROR));
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("customerCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return false;
	}

	public DTObject ValidateCustomerCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		DTObject custinputDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		LeaseValidator validation = new LeaseValidator();
		RegistrationValidator regValidator = new RegistrationValidator();
		try {
			inputDTO.set(ContentManager.FETCH, ContentManager.FETCH_ALL);
			resultDTO = validation.valildateLesseCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}

			custinputDTO.set(ContentManager.ACTION, USAGE);
			custinputDTO.set("CUSTOMER_ID", resultDTO.get("CUSTOMER_ID"));
			custinputDTO.set(ContentManager.FETCH_COLUMNS, "CUSTOMER_ID,CUSTOMER_NAME");
			resultDTO = regValidator.valildateCustomerID(custinputDTO);

			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}

		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
			regValidator.close();
		}
		return resultDTO;
	}

	private boolean validateAgreementNo() {
		CommonValidator validator = new CommonValidator();
		try {
			if (!validator.isEmpty(customerCode)) {
				if (validator.isEmpty(agreementNo)) {
					getErrorMap().setError("agreementNo", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
			} else {
				if (!validator.isEmpty(agreementNo)) {
					if (validator.isEmpty(customerCode)) {
						getErrorMap().setError("customerCode", BackOfficeErrorCodes.FIELD_BLANK);
						return false;
					}
				}
			}
			DTObject formDTO = new DTObject();
			formDTO.set("CUSTOMER_ID", customerId);
			formDTO.set("AGREEMENT_NO", agreementNo);
			formDTO.set(ContentManager.ACTION, USAGE);
			formDTO = ValidateAgreementNumber(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("agreementNo", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("agreementNo", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return false;
	}

	public DTObject ValidateAgreementNumber(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		LeaseValidator validation = new LeaseValidator();
		String agreementNo = inputDTO.get("AGREEMENT_NO");
		try {
			if (!validation.isEmpty(agreementNo)) {
				inputDTO.set(ContentManager.FETCH_COLUMNS, "AGREEMENT_NO");
				resultDTO = validation.validateCustomerAgreement(inputDTO);
				if (resultDTO.get(ContentManager.ERROR) != null) {
					resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
					return resultDTO;
				}
				if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
					return resultDTO;
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	private boolean validateScheduleID() {
		CommonValidator validator = new CommonValidator();
		try {
			if (!validator.isEmpty(customerCode)) {
				if (validator.isEmpty(scheduleId)) {
					getErrorMap().setError("scheduleId", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				DTObject formDTO = new DTObject();
				formDTO.set("LESSEE_CODE", leaseProductCode);
				formDTO.set("AGREEMENT_NO", agreementNo);
				formDTO.set("SCHEDULE_ID", scheduleId);
				formDTO.set(ContentManager.ACTION, USAGE);
				formDTO = ValidateScheduleId(formDTO);
				if (formDTO.get(ContentManager.ERROR) != null) {
					getErrorMap().setError("scheduleId", formDTO.get(ContentManager.ERROR));
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("scheduleId", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return false;
	}

	public DTObject ValidateScheduleId(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		LeaseValidator validation = new LeaseValidator();
		try {
			inputDTO.set(ContentManager.FETCH_COLUMNS, "SCHEDULE_ID,E_STATUS,LEASE_CLOSURE_ON");
			resultDTO = validation.validateScheduleID(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != RegularConstants.NULL) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}

			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.REFERENCE_DOES_NOT_EXIST);
				return resultDTO;
			}
			if (resultDTO.get("E_STATUS").equals(RegularConstants.REJECTED)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNAUTH_LEASE_DETAILS);//
				return resultDTO;
			}
			if (resultDTO.get("LEASE_CLOSURE_ON") != RegularConstants.NULL) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.LEASE_CLOSED);//
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public DTObject getInvoiceDetails(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		DBContext dbContext = new DBContext();
		CommonValidator validator = new CommonValidator();
		DBUtil util = dbContext.createUtilInstance();
		DHTMLXGridUtility gridUtility = new DHTMLXGridUtility();
		StringBuffer sqlQuery = new StringBuffer(RegularConstants.EMPTY_STRING);
		boolean recordExist = false;
		try {
			setInvoiceCycleNumber(inputDTO.get("INV_CYCLE_NUMBER"));
			setInvoiceDate(inputDTO.get("INV_DATE"));
			setLeaseProductCode(inputDTO.get("PRODUCT_CODE"));
			setCustomerCode(inputDTO.get("LESSEE_CODE"));
			setCustomerId(inputDTO.get("CUSTOMER_ID"));
			setAgreementNo(inputDTO.get("AGREEMENT_NO"));
			setScheduleId(inputDTO.get("SCHEDULE_ID"));
			DTObject validateDTO = validate(inputDTO);
			if (validateDTO.get(ContentManager.ERROR) != null) {
				return validateDTO;
			}
			sqlQuery.append("SELECT I.ENTITY_CODE,I.LESSEE_CODE,I.AGREEMENT_NO,I.SCHEDULE_ID,I.CUSTOMER_ID,TO_CHAR(I.INV_DATE,'" + context.getDateFormat() + "')INV_DATE,I.LESSEE_CODE,");
			sqlQuery.append("I.AGREEMENT_NO,I.CUSTOMER_ID,I.SCHEDULE_ID,I.INV_CYCLE_NUMBER,I.INV_CCY,FN_FORMAT_AMOUNT(I.INV_AMOUNT,I.INV_CCY,C.SUB_CCY_UNITS_IN_CCY)AS INV_AMOUNT,I.ENTRY_DATE,");
			sqlQuery.append("I.ENTRY_SL,I.CREATED_BY,I.LEASE_PRODUCT_CODE ");
			sqlQuery.append("FROM INVCOMPONENT I ");
			sqlQuery.append("JOIN CURRENCY C ON(C.CCY_CODE=I.INV_CCY) ");
			if (!validator.isEmpty(inputDTO.get("PRODUCT_CODE")))
				sqlQuery.append("JOIN LEASE L ");
			if (!validator.isEmpty(inputDTO.get("LESSEE_CODE")))
				sqlQuery.append("ON I.LESSEE_CODE=L.LESSEE_CODE AND I.AGREEMENT_NO=L.AGREEMENT_NO AND I.SCHEDULE_ID=L.SCHEDULE_ID ");
			sqlQuery.append("WHERE I.ENTITY_CODE=? AND I.INV_DATE=? AND I.INV_FOR_RENTAL=? AND I.INV_FOR_INTIAL_INTERIM=? ");
			sqlQuery.append("AND I.INV_FOR_FINAL_INTERIM=? AND I.INV_FOR_INTEREST=? ");
			if (!validator.isEmpty(inputDTO.get("INV_CYCLE_NUMBER")))
				sqlQuery.append(" AND I.INV_CYCLE_NUMBER= ?");
			if (!validator.isEmpty(inputDTO.get("LESSEE_CODE")))
				sqlQuery.append(" AND L.LESSEE_CODE= ?");
			if (!validator.isEmpty(inputDTO.get("PRODUCT_CODE")))
				sqlQuery.append(" AND L.LEASE_PRODUCT_CODE= ?");
			if (!validator.isEmpty(inputDTO.get("AGREEMENT_NO")))
				sqlQuery.append(" AND I.AGREEMENT_NO= ?");
			if (!validator.isEmpty(inputDTO.get("SCHEDULE_ID")))
				sqlQuery.append(" AND I.SCHEDULE_ID= ?");
			int i = 1;
			util.reset();
			util.setMode(DBUtil.PREPARED);
			util.setSql(sqlQuery.toString());
			util.setString(i++, context.getEntityCode());
			util.setDate(i++, new java.sql.Date(BackOfficeFormatUtils.getDate(inputDTO.get("INV_DATE"), context.getDateFormat()).getTime()));
			util.setString(i++, inputDTO.get("INV_FOR_INTIAL_INTERIM"));
			util.setString(i++, inputDTO.get("INV_FOR_RENTAL"));
			util.setString(i++, inputDTO.get("INV_FOR_FINAL_INTERIM"));
			util.setString(i++, inputDTO.get("INV_FOR_INTEREST"));
			if (!validator.isEmpty(inputDTO.get("INV_CYCLE_NUMBER")))
				util.setString(i++, inputDTO.get("INV_CYCLE_NUMBER"));
			if (!validator.isEmpty(inputDTO.get("LESSEE_CODE")))
				util.setString(i++, inputDTO.get("LESSEE_CODE"));
			if (!validator.isEmpty(inputDTO.get("PRODUCT_CODE")))
				util.setString(i++, inputDTO.get("PRODUCT_CODE"));
			if (!validator.isEmpty(inputDTO.get("AGREEMENT_NO")))
				util.setString(i++, inputDTO.get("AGREEMENT_NO"));
			if (!validator.isEmpty(inputDTO.get("SCHEDULE_ID")))
				util.setString(i++, inputDTO.get("SCHEDULE_ID"));
			ResultSet rset = util.executeQuery();
			gridUtility.init();
			while (rset.next()) {
				gridUtility.startRow();
				gridUtility.setCell(rset.getString("INV_DATE"));
				gridUtility.setCell(rset.getString("LEASE_PRODUCT_CODE"));
				gridUtility.setCell(rset.getString("LESSEE_CODE"));
				gridUtility.setCell(rset.getString("AGREEMENT_NO"));
				gridUtility.setCell(rset.getString("SCHEDULE_ID"));
				gridUtility.setCell(rset.getString("INV_CYCLE_NUMBER"));
				gridUtility.setCell(rset.getString("INV_CCY"));
				gridUtility.setCell(rset.getString("INV_AMOUNT"));
				gridUtility.setCell(rset.getString("CREATED_BY"));
				gridUtility.setCell(rset.getString("ENTRY_DATE"));
				gridUtility.setCell(rset.getString("ENTRY_SL"));
				gridUtility.endRow();
				recordExist = true;
			}
			if (!recordExist) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_NOT_EXISTS);
				return resultDTO;
			}
			gridUtility.finish();
			String resultXML = gridUtility.getXML();
			inputDTO.set(ContentManager.RESULT_XML, resultXML);
			inputDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
		} catch (Exception e) {
			e.printStackTrace();
			inputDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			dbContext.close();
			validator.close();

		}
		return inputDTO;
	}

	public DTObject loadDetailView(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		DHTMLXGridUtility gridUtility = new DHTMLXGridUtility();
		StringBuffer sqlQuery = new StringBuffer(RegularConstants.EMPTY_STRING);
		boolean recordExist = false;
		try {
			sqlQuery.append("SELECT ID.ENTRY_SL,TO_CHAR(ID.START_DATE,'" + context.getDateFormat() + "')START_DATE, ");
			sqlQuery.append("TO_CHAR(ID.END_DATE,'" + context.getDateFormat() + "')END_DATE,ID.CCY,FN_FORMAT_AMOUNT(ID.PRINCIPAL_AMOUNT,ID.CCY,C.SUB_CCY_UNITS_IN_CCY)AS PRINCIPAL_AMOUNT, ");
			sqlQuery.append("FN_FORMAT_AMOUNT(ID.INTEREST_AMOUNT,ID.CCY,C.SUB_CCY_UNITS_IN_CCY)AS INTEREST_AMOUNT, ");
			sqlQuery.append("FN_FORMAT_AMOUNT(ID.RENTAL_AMOUNT,ID.CCY,C.SUB_CCY_UNITS_IN_CCY)AS RENTAL_AMOUNT, ");
			sqlQuery.append("FN_FORMAT_AMOUNT(ID.EXECUTORY_AMOUNT,ID.CCY,C.SUB_CCY_UNITS_IN_CCY)AS EXECUTORY_AMOUNT, ");
			sqlQuery.append("FN_FORMAT_AMOUNT(ID.TAX_AMT,ID.CCY,C.SUB_CCY_UNITS_IN_CCY)AS TAX_AMT , ");
			sqlQuery.append("DECODE_CMLOVREC('COMMON','INVOICE_FOR',ID.FINANCE_SCHED_INV_FOR)AS FINANCE_SCHED_INV_FOR  ");
			sqlQuery.append("FROM INVCOMPDTL ID ");
			sqlQuery.append("JOIN CURRENCY C ON(C.CCY_CODE=ID.CCY) WHERE ID.ENTITY_CODE=? AND ID.ENTRY_DATE=? AND ID.ENTRY_SL=? ");
			sqlQuery.append("ORDER BY ID.START_DATE");
			util.reset();
			util.setMode(DBUtil.PREPARED);
			util.setSql(sqlQuery.toString());
			util.setString(1, context.getEntityCode());
			util.setString(2, inputDTO.get("ENTRY_DATE"));
			util.setString(3, inputDTO.get("ENTRY_SL"));
			ResultSet rset = util.executeQuery();
			gridUtility.init();
			while (rset.next()) {
				gridUtility.startRow();
				gridUtility.setCell(rset.getString("ENTRY_SL"));
				gridUtility.setCell(rset.getString("START_DATE"));
				gridUtility.setCell(rset.getString("END_DATE"));
				gridUtility.setCell(rset.getString("CCY"));
				gridUtility.setCell(rset.getString("PRINCIPAL_AMOUNT"));
				gridUtility.setCell(rset.getString("INTEREST_AMOUNT"));
				gridUtility.setCell(rset.getString("RENTAL_AMOUNT"));
				gridUtility.setCell(rset.getString("EXECUTORY_AMOUNT"));
				gridUtility.setCell(rset.getString("TAX_AMT"));
				gridUtility.setCell(rset.getString("FINANCE_SCHED_INV_FOR"));
				gridUtility.endRow();
				recordExist = true;
			}
			if (!recordExist) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_NOT_EXISTS);
				return resultDTO;
			}
			gridUtility.finish();
			String resultXML = gridUtility.getXML();
			inputDTO.set(ContentManager.RESULT_XML, resultXML);
			inputDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);

		} catch (Exception e) {
			e.printStackTrace();
			inputDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			dbContext.close();
		}
		return inputDTO;
	}
}