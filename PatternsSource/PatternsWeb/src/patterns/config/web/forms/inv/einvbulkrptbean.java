package patterns.config.web.forms.inv;

import java.util.ArrayList;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.GenericOption;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.InvoiceValidator;
import patterns.config.validations.MasterValidator;
/*
 * The program will be used to Insert record to INVBULKRPTPE table. 
 *
 Author : Pavan kumar.R
 Created Date : 08-April-2017
 Spec Reference PSD-INV-EINVBULKRPT
 Modification History
 -----------------------------------------------------------------------------------------------------
 Sl.No		Modified Date	Author			Modified Changes					Version
 
 -----------------------------------------------------------------------------------------------------

 */

import patterns.config.web.forms.GenericFormBean;

public class einvbulkrptbean extends GenericFormBean {

	private static final long serialVersionUID = 1L;
	private String entryDate;
	private String entrySerial;
	private String invoiceCycleNumber;
	private String invMonth;
	private String invMonthYear;
	private String productCode;
	private String remarks;

	public String getEntryDate() {
		return entryDate;
	}

	public void setEntryDate(String entryDate) {
		this.entryDate = entryDate;
	}

	public String getEntrySerial() {
		return entrySerial;
	}

	public void setEntrySerial(String entrySerial) {
		this.entrySerial = entrySerial;
	}

	public String getInvoiceCycleNumber() {
		return invoiceCycleNumber;
	}

	public void setInvoiceCycleNumber(String invoiceCycleNumber) {
		this.invoiceCycleNumber = invoiceCycleNumber;
	}

	public String getInvMonth() {
		return invMonth;
	}

	public void setInvMonth(String invMonth) {
		this.invMonth = invMonth;
	}

	public String getInvMonthYear() {
		return invMonthYear;
	}

	public void setInvMonthYear(String invMonthYear) {
		this.invMonthYear = invMonthYear;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void reset() {
		entryDate = RegularConstants.EMPTY_STRING;
		entrySerial = RegularConstants.EMPTY_STRING;
		invoiceCycleNumber = RegularConstants.EMPTY_STRING;
		invMonth = RegularConstants.EMPTY_STRING;
		invMonthYear = RegularConstants.EMPTY_STRING;
		productCode = RegularConstants.EMPTY_STRING;
		remarks = RegularConstants.EMPTY_STRING;
		
		DBContext dbContext = new DBContext();
		ArrayList<GenericOption> monthList = null;
		monthList = getGenericOptions("COMMON", "MONTHS", dbContext, null);
		webContext.getRequest().setAttribute("COMMON_MONTHS", monthList);
		dbContext.close();
		
		
		setProcessBO("patterns.config.framework.bo.inv.einvbulkrptBO");
	}

	public void validate() {
		if (validateEntryDate() && validateEntrySerial()) {
			validateInvoiceCycleNumber();
			validateInvoiceMonth();
			validateInvoiceMonthYear();
			validateLeaseProductCode();
			validateRemarks();
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.setObject("ENTRY_DATE", entryDate);
				formDTO.setObject("ENTRY_SL", entrySerial);
				formDTO.set("INV_CYCLE_NUMBER", invoiceCycleNumber);
				formDTO.set("INV_YEAR", invMonthYear);
				formDTO.set("INV_MONTH", invMonth);
				formDTO.set("PRODUCT_CODE", productCode);
				formDTO.set("REMARKS", remarks);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("entry", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
	}

	private boolean validateEntryDate() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (commonValidator.isEmpty(entryDate)) {
				getErrorMap().setError("entry", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (commonValidator.isValidDate(entryDate)==null) {
				getErrorMap().setError("entry", BackOfficeErrorCodes.INVALID_DATE);
				return false;
			}
			if (commonValidator.isDateGreaterThanCBD(entryDate)) {
				getErrorMap().setError("entry", BackOfficeErrorCodes.DATE_ECBD);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("entryDate", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}

	private boolean validateEntrySerial() {
		CommonValidator validation = new CommonValidator();
		try {
			if (getAction().equals(MODIFY)) {
				getErrorMap().setError("entry", BackOfficeErrorCodes.INVALID_ACTION);
				return false;
			}
			if (getRectify().equals(RegularConstants.COLUMN_ENABLE)) {
				DTObject input = new DTObject();
				if (validation.isEmpty(entrySerial)) {
					getErrorMap().setError("entry", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}

				if (!validation.isValidNumber(entrySerial)) {
					getErrorMap().setError("entry", BackOfficeErrorCodes.INVALID_NUMBER);
					return false;
				}
				input.set("ENTRY_DATE", entryDate);
				input.set("ENTRY_SL", entrySerial);
				input.set(ContentManager.ACTION, getAction());
				input = validateEntrySerial(input);
				if (input.get(ContentManager.ERROR) != null) {
					getErrorMap().setError("entry", input.get(ContentManager.ERROR));
					return false;
				}
				return true;
			} else {
				if (!validation.isEmpty(entrySerial)) {
					getErrorMap().setError("entry", BackOfficeErrorCodes.REFERENCE_SERIAL_AUTO_GENERATED);
					return false;
				}
			}

			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("entry", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public DTObject validateEntrySerial(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.NULL);
		CommonValidator validator = new CommonValidator();
		try {
			inputDTO.set(ContentManager.TABLE, "INVOICEBULKRPT");
			inputDTO.set(ContentManager.FETCH_COLUMNS, "E_STATUS");
			String columns[] = new String[] { context.getEntityCode(), inputDTO.get("ENTRY_DATE"), inputDTO.get("ENTRY_SL") };
			inputDTO.setObject(ContentManager.PROGRAM_KEY, columns);
			resultDTO = validator.validatePK(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.REFERENCE_DOES_NOT_EXIST);
				return resultDTO;
			}
			// AFTER AUTHORIZATION MODIFICATION NOT ALLOWED START
			if (!resultDTO.getObject("E_STATUS").equals(RegularConstants.EMPTY_STRING) ) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.REFERENCE_ALREADY_AUTHORIZED);
				return resultDTO;
			}
			// AFTER AUTHORIZATION MODIFICATION NOT ALLOWED END

		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return resultDTO;
	}

	private boolean validateInvoiceCycleNumber() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("INV_CYCLE_NUMBER", invoiceCycleNumber);
			formDTO.set(ContentManager.ACTION, USAGE);
			formDTO = validateInvoiceCycleNumber(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("invoiceCycleNumber", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("invoiceCycleNumber", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateInvoiceCycleNumber(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		InvoiceValidator validation = new InvoiceValidator();
		String invCycleNumber = inputDTO.get("INV_CYCLE_NUMBER");
		try {
			if (validation.isEmpty(invCycleNumber)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			resultDTO = validation.validateInvoiceCycleNumber(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	private boolean validateInvoiceMonth() {
		CommonValidator validator = new CommonValidator();
		try {
			if (validator.isEmpty(invMonth)) {
				getErrorMap().setError("invMonth", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validator.isCMLOVREC("COMMON", "MONTHS", invMonth)) {
				getErrorMap().setError("invMonth", BackOfficeErrorCodes.INVALID_OPTION);
				return false;
			}

			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("invMonth", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return false;
	}

	private boolean validateInvoiceMonthYear() {
		CommonValidator validator = new CommonValidator();
		try {
			if (validator.isEmpty(invMonthYear)) {
				getErrorMap().setError("invMonth", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validator.isValidYear(invMonthYear)) {
				getErrorMap().setError("invMonth", BackOfficeErrorCodes.INVALID_YEAR);
				return false;
			}

			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("invMonth", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return false;
	}

	private boolean validateLeaseProductCode() {
		CommonValidator validator = new CommonValidator();
		try {
			if (validator.isEmpty(productCode)) {
				return true;
			}
			DTObject formDTO = new DTObject();
			formDTO.set("LEASE_PRODUCT_CODE", productCode);
			formDTO.set(ContentManager.ACTION, USAGE);
			formDTO = validateLeaseProductCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("productCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("productCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return false;
	}

	public DTObject validateLeaseProductCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		MasterValidator validation = new MasterValidator();
		try {
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
			resultDTO = validation.validateLeaseProductCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	private boolean validateRemarks() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (remarks != null && remarks.length() > 0) {
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			if (commonValidator.isEmpty(remarks)) {
				getErrorMap().setError("remarks", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!commonValidator.isValidRemarks(remarks)) {
				getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
				return false;
			}

			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}

}
