package patterns.config.web.forms.inv;

import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;

import patterns.config.delegate.BackOfficeProcessManager;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.process.TBAProcessResult;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.framework.web.DHTMLXGridUtility;
import patterns.config.framework.web.GenericOption;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.framework.web.reports.ReportUtils;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.MasterValidator;
import patterns.config.web.forms.GenericFormBean;

public class rinvoicebean extends GenericFormBean {

	private static final long serialVersionUID = -6219657363150466160L;
	private String invoiceCycleNumber;
	private String invoiceMonth;
	private String invoiceMonthYear;

	public String getInvoiceCycleNumber() {
		return invoiceCycleNumber;
	}

	public void setInvoiceCycleNumber(String invoiceCycleNumber) {
		this.invoiceCycleNumber = invoiceCycleNumber;
	}

	public String getInvoiceMonth() {
		return invoiceMonth;
	}

	public void setInvoiceMonth(String invoiceMonth) {
		this.invoiceMonth = invoiceMonth;
	}

	public String getInvoiceMonthYear() {
		return invoiceMonthYear;
	}

	public void setInvoiceMonthYear(String invoiceMonthYear) {
		this.invoiceMonthYear = invoiceMonthYear;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void validate() {
	}

	@Override
	public void reset() {
		invoiceCycleNumber = RegularConstants.EMPTY_STRING;
		invoiceMonth = RegularConstants.EMPTY_STRING;
		invoiceMonthYear = RegularConstants.EMPTY_STRING;
		DBContext dbContext = new DBContext();
		ArrayList<GenericOption> invMonths = null;
		invMonths = getGenericOptions("COMMON", "MONTH", dbContext, "--");
		webContext.getRequest().setAttribute("COMMON_MONTH", invMonths);
		dbContext.close();
		setProcessBO("patterns.config.framework.bo.inv.pinvemailgenBO");

	}

	public DTObject validateInvoicesForCycleNumber(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		String invForCycleNumber = inputDTO.get("INV_CYCLE_NUMBER");
		MasterValidator validator = new MasterValidator();
		try {
			if (validator.isEmpty(invForCycleNumber)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "INV_RENTAL_DAY_FROM,INV_RENTAL_DAY_UPTO");
			resultDTO = validator.validateInvoiceCycleNumber(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
			return resultDTO;
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return resultDTO;
	}

	public DTObject returnFromDateAndUpDate(DTObject inputDTO) {
		try {
			StringBuffer fromDate = new StringBuffer();
			StringBuffer upToDate = new StringBuffer();
			if (inputDTO.get("INV_RENTAL_DAY_FROM").trim().length() == 1)
				fromDate.append(RegularConstants.ZERO).append(inputDTO.get("INV_RENTAL_DAY_FROM"));
			else
				fromDate.append(inputDTO.get("INV_RENTAL_DAY_FROM"));
			fromDate.append("-");
			if (inputDTO.get("INV_MONTH").trim().length() == 1)
				fromDate.append(RegularConstants.ZERO).append(inputDTO.get("INV_MONTH"));
			else
				fromDate.append(inputDTO.get("INV_MONTH"));
			fromDate.append("-").append(inputDTO.get("INV_YEAR"));

			if (inputDTO.get("INV_RENTAL_DAY_UPTO").trim().length() == 1)
				upToDate.append(RegularConstants.ZERO).append(inputDTO.get("INV_RENTAL_DAY_UPTO"));
			else
				upToDate.append(inputDTO.get("INV_RENTAL_DAY_UPTO"));
			upToDate.append("-");
			if (inputDTO.get("INV_MONTH").trim().length() == 1)
				upToDate.append(RegularConstants.ZERO).append(inputDTO.get("INV_MONTH"));
			else
				upToDate.append(inputDTO.get("INV_MONTH"));
			upToDate.append("-").append(inputDTO.get("INV_YEAR"));

			inputDTO.set("FROM_DATE", fromDate.toString());
			inputDTO.set("UPTO_DATE", upToDate.toString());
			return inputDTO;
		} catch (Exception e) {
			e.printStackTrace();
			inputDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return inputDTO;
	}

	public DTObject loadGrid(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		MasterValidator validation = new MasterValidator();
		DBUtil dbutil = validation.getDbContext().createUtilInstance();
		try {
			if (inputDTO.get("REVALIDATE").equals("1")) {
				resultDTO = validateFields(inputDTO);
				if (resultDTO.get("ERROR_PRESENT").equals("1")) {
					return resultDTO;
				} else {
					resultDTO = returnFromDateAndUpDate(inputDTO);
					inputDTO.set("FROM_DATE", resultDTO.get("FROM_DATE"));
					inputDTO.set("UPTO_DATE", resultDTO.get("UPTO_DATE"));
				}
			}
			int recordCount = 0;
			StringBuffer sqlQuery = new StringBuffer();
			sqlQuery.append("SELECT CONCAT(I.LEASE_PRODUCT_CODE,'/',I.INV_YEAR,'/',I.INV_MONTH,'/',I.INV_SERIAL) AS INVNO,");
			sqlQuery.append(" TO_CHAR(I.INV_DATE,?) AS TO_INV_DATE,CM.LOV_LABEL_EN_US,C.CUSTOMER_NAME,FN_FORMAT_AMOUNT(I.TOTAL_INV_AMT,I.INV_CCY,CU.SUB_CCY_UNITS_IN_CCY) AS TOTAL_INV_AMT,I.TOTAL_INV_AMT TOT_AMNT,I.INV_CCY,CU.SUB_CCY_UNITS_IN_CCY INV_CCY_UNITS, ");
			sqlQuery.append(" CASE WHEN (IS_PDF_GENERATED='1' OR IS_EXCEL_GENERATED='1') THEN '1' ELSE '0' END REPORT_GEN,I.PDF_REPORT_IDENTIFIER,I.EXCEL_REPORT_IDENTIFIER");
			sqlQuery.append(" FROM INVOICES I ");
			sqlQuery.append(" INNER JOIN CUSTOMER C ON (I.ENTITY_CODE=C.ENTITY_CODE AND I.CUSTOMER_ID=C.CUSTOMER_ID)");
			sqlQuery.append(" INNER JOIN CMLOVREC CM ON (CM.PGM_ID='COMMON' AND CM.LOV_ID='INVOICE_TYPE' AND CM.LOV_VALUE=I.INV_TYPE) ");
			sqlQuery.append(" INNER JOIN CURRENCY CU ON (CU.CCY_CODE=I.INV_CCY)");
			sqlQuery.append(" WHERE I.ENTITY_CODE=? AND I.INV_YEAR=? AND I.INV_MONTH=? AND I.INV_FOR_CYCLE_NUMBER=? AND I.INV_DATE BETWEEN ? AND ?  ");
			dbutil.reset();
			dbutil.setMode(DBUtil.PREPARED);
			dbutil.setSql(sqlQuery.toString());
			dbutil.setString(1, context.getDateFormat());
			dbutil.setInt(2, Integer.parseInt(context.getEntityCode()));
			dbutil.setInt(3, Integer.parseInt(inputDTO.get("INV_YEAR")));
			dbutil.setInt(4, Integer.parseInt(inputDTO.get("INV_MONTH")));
			dbutil.setInt(5, Integer.parseInt(inputDTO.get("INV_CYCLE_NUMBER")));
			dbutil.setDate(6, new java.sql.Date(BackOfficeFormatUtils.getDate(inputDTO.get("FROM_DATE"), BackOfficeConstants.JAVA_DATE_FORMAT).getTime()));
			dbutil.setDate(7, new java.sql.Date(BackOfficeFormatUtils.getDate(inputDTO.get("UPTO_DATE"), BackOfficeConstants.JAVA_DATE_FORMAT).getTime()));
			ResultSet rsetGrid = dbutil.executeQuery();
			DHTMLXGridUtility gridUtility = new DHTMLXGridUtility();
			gridUtility.init();
			while (rsetGrid.next()) {
				recordCount = recordCount + 1;
				gridUtility.startRow(String.valueOf(recordCount));
				gridUtility.setCell("0");
				gridUtility.setCell(rsetGrid.getString("INVNO"));
				gridUtility.setCell(rsetGrid.getString("TO_INV_DATE"));
				gridUtility.setCell(rsetGrid.getString("LOV_LABEL_EN_US"));
				gridUtility.setCell(rsetGrid.getString("CUSTOMER_NAME"));
				gridUtility.setCell(rsetGrid.getString("INV_CCY"));
				gridUtility.setCell(rsetGrid.getString("TOTAL_INV_AMT"));
				gridUtility.setCell(rsetGrid.getString("REPORT_GEN"));
				if (rsetGrid.getString("PDF_REPORT_IDENTIFIER") != null)
					gridUtility.setCell("Download" + "^javascript:downloadReport(\"" + rsetGrid.getString("PDF_REPORT_IDENTIFIER") + "\")");
				else
					gridUtility.setCell(" ");
				if (rsetGrid.getString("EXCEL_REPORT_IDENTIFIER") != null)
					gridUtility.setCell("Download" + "^javascript:downloadReport(\"" + rsetGrid.getString("EXCEL_REPORT_IDENTIFIER") + "\")");
				else
					gridUtility.setCell(" ");
				gridUtility.setCell(rsetGrid.getString("TOT_AMNT"));
				gridUtility.setCell(rsetGrid.getString("INV_CCY_UNITS"));
				gridUtility.endRow();
			}
			if (recordCount != 0) {
				gridUtility.finish();
				String resultXML = gridUtility.getXML();
				resultDTO.set(ContentManager.RESULT_XML, resultXML);
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
			} else {
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
				return resultDTO;
			}
			return resultDTO;
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			dbutil.reset();
			validation.close();
		}
		return resultDTO;
	}

	public DTObject generateInvoiceReport(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		try {
			util.reset();
			util.setMode(DBUtil.CALLABLE);
			util.setSql("CALL SP_RINVINDV_USERDEF_FLDS(?,?,?,?,?,?,?)");
			util.setInt(1, Integer.parseInt(context.getEntityCode()));
			util.setString(2, inputDTO.get("LEASE_PROD_CODE"));
			util.setInt(3, Integer.parseInt(inputDTO.get("INV_YEAR")));
			util.setInt(4, Integer.parseInt(inputDTO.get("INV_MONTH")));
			util.setInt(5, Integer.parseInt(inputDTO.get("INV_SERIAL")));
			util.registerOutParameter(6, Types.BIGINT);
			util.registerOutParameter(7, Types.VARCHAR);
			util.execute();
			if (util.getString(7).equals("S")) {
				inputDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				inputDTO.set(ContentManager.REPORT_HANDLER, "patterns.config.web.reports.inv.rinvoicereport");
				long tempSl = util.getLong(6);
				inputDTO.set("TEMP_SL", String.valueOf(tempSl));
				util.reset();
				util.setSql("SELECT IT.CONSOLIDATED,IT.CONSOLIDATION_IN_GRID,IT.CONSOLIDATION_IN_ANNEX,IT.ANNEX_MODE,IT.GROUPING_IN_GRID,IT.GROUPING_IN_ANNEX,C.CONCISE_DESCRIPTION,C.SUB_CCY_CONCISE_DESCRIPTION FROM INVOICES I INNER JOIN INVTEMPLATECODE IT ON IT.ENTITY_CODE=I.ENTITY_CODE AND IT.INVTEMPLATE_CODE=I.INVOICE_TEMPLATE INNER JOIN CURRENCY C ON (C.CCY_CODE=I.INV_CCY)  WHERE I.ENTITY_CODE=? AND I.LEASE_PRODUCT_CODE=? AND I.INV_YEAR=? AND I.INV_MONTH=? AND I.INV_SERIAL=?");
				util.setInt(1, Integer.parseInt(context.getEntityCode()));
				util.setString(2, inputDTO.get("LEASE_PROD_CODE"));
				util.setInt(3, Integer.parseInt(inputDTO.get("INV_YEAR")));
				util.setInt(4, Integer.parseInt(inputDTO.get("INV_MONTH")));
				util.setInt(5, Integer.parseInt(inputDTO.get("INV_SERIAL")));
				ResultSet rs = util.executeQuery();
				if (rs.next()) {
					inputDTO.set("CONSOLIDATED", rs.getString("CONSOLIDATED"));
					inputDTO.set("CONSOLIDATION_IN_GRID", rs.getString("CONSOLIDATION_IN_GRID"));
					inputDTO.set("CONSOLIDATION_IN_ANNEX", rs.getString("CONSOLIDATION_IN_ANNEX"));
					inputDTO.set("ANNEX_MODE", rs.getString("ANNEX_MODE"));
					inputDTO.set("GROUPING_IN_GRID", rs.getString("GROUPING_IN_GRID"));
					inputDTO.set("GROUPING_IN_ANNEX", rs.getString("GROUPING_IN_ANNEX"));
					inputDTO.set("CONCISE_DESCRIPTION", rs.getString("CONCISE_DESCRIPTION"));
					inputDTO.set("SUB_CCY_CONCISE_DESCRIPTION", rs.getString("SUB_CCY_CONCISE_DESCRIPTION"));
				}
				if (inputDTO.get("REPORT_FORMAT") != null && !inputDTO.get("REPORT_FORMAT").equals(""))
					inputDTO.set(ContentManager.REPORT_FORMAT, inputDTO.get("REPORT_FORMAT"));
				else
					inputDTO.set(ContentManager.REPORT_FORMAT, ReportUtils.EXPORT_PDF + "");
				setProcessBO("patterns.config.framework.bo.inv.rinvoiceBO");
				DTObject formDTO = inputDTO;
				String reportFmtType = "1";
				if (inputDTO.get("CONSOLIDATED").equals(RegularConstants.ONE) && inputDTO.get("CONSOLIDATION_IN_GRID").equals(RegularConstants.ONE)) {
					if (inputDTO.get("CONSOLIDATION_IN_ANNEX").equals(RegularConstants.ONE))
						reportFmtType = "4";
					else
						reportFmtType = "3";
					inputDTO.set("TEMP_SL", String.valueOf(tempSl));
					inputDTO = getConsolidatedGridResult(inputDTO);
				}
				if (inputDTO.get("CONSOLIDATED").equals(RegularConstants.ONE) && inputDTO.get("CONSOLIDATION_IN_ANNEX").equals(RegularConstants.ONE)) {
					if (inputDTO.get("CONSOLIDATION_IN_GRID").equals(RegularConstants.ZERO))
						reportFmtType = "2";
					inputDTO.set("TEMP_SL", String.valueOf(tempSl));
					inputDTO = getConsolidatedAnnexureResult(inputDTO);
				}
				inputDTO.set("REPORT_FMT_TYPE", reportFmtType);
				if (inputDTO.get("CONSOLIDATED").equals(RegularConstants.ONE) && inputDTO.get("CONSOLIDATION_IN_ANNEX").equals(RegularConstants.ONE)) {
					if (inputDTO.get("ANNEX_MODE").equals("P")) {
						inputDTO.set(ContentManager.REPORT_FORMAT, ReportUtils.EXPORT_PDF + "");
						resultDTO = processDownload(inputDTO);
						if (resultDTO.get(ContentManager.ERROR) != null && resultDTO.get(ContentManager.ERROR).equals(RegularConstants.EMPTY_STRING) ) 
							return resultDTO;
						formDTO.set("PDF_REPORT_IDENTIFIER", resultDTO.get(ContentManager.REPORT_ID));
					} else {
						resultDTO = processDownload(inputDTO);
						if (resultDTO.get(ContentManager.ERROR) != null && resultDTO.get(ContentManager.ERROR).equals(RegularConstants.EMPTY_STRING) ) 
							return resultDTO;
						formDTO.set("PDF_REPORT_IDENTIFIER", resultDTO.get(ContentManager.REPORT_ID));
						inputDTO.set(ContentManager.REPORT_FORMAT, ReportUtils.EXPORT_XLS + "");
						resultDTO = processDownload(inputDTO);
						if (resultDTO.get(ContentManager.ERROR) != null && resultDTO.get(ContentManager.ERROR).equals(RegularConstants.EMPTY_STRING) ) 
							return resultDTO;
						formDTO.set("EXCEL_REPORT_IDENTIFIER", resultDTO.get(ContentManager.REPORT_ID));
					}
				} else {
					resultDTO = processDownload(inputDTO);
					if (resultDTO.get(ContentManager.ERROR) != null && resultDTO.get(ContentManager.ERROR).equals(RegularConstants.EMPTY_STRING) ) 
						return resultDTO;
					formDTO.set("PDF_REPORT_IDENTIFIER", resultDTO.get(ContentManager.REPORT_ID));
				}
				dbContext.close();
				formDTO.set(RegularConstants.PROCESSBO_CLASS, getProcessBO());
				if (resultDTO.get(ContentManager.ERROR) == null || resultDTO.get(ContentManager.ERROR).equals(RegularConstants.EMPTY_STRING)) {					BackOfficeProcessManager processManager = new BackOfficeProcessManager();
					TBAProcessResult processResult = processManager.delegate(formDTO);
					if (processResult.getProcessStatus().equals(TBAProcessStatus.SUCCESS)) {
						resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
						resultDTO.set("PDF_REPORT_IDENTIFIER", formDTO.get("PDF_REPORT_IDENTIFIER"));
						resultDTO.set("EXCEL_REPORT_IDENTIFIER", formDTO.get("EXCEL_REPORT_IDENTIFIER"));
						return resultDTO;
					} else {
						resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
					}
				}
			} else {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.REPORT_ERROR);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			if (dbContext != null)
				dbContext.close();
		}
		return resultDTO;
	}

	private DTObject getConsolidatedGridResult(DTObject resultDTO) {
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		ArrayList<String> fields = new ArrayList<String>();
		ArrayList<String> fieldsDesc = new ArrayList<String>();
		ArrayList<String> fieldsType = new ArrayList<String>();
		ArrayList<String> orderSl = new ArrayList<String>();
		ArrayList<String> groupType = new ArrayList<String>();
		String lessee = "";
		try {
			util.reset();
			util.setSql("SELECT FIELD_ID,ORD_SL,FIELD_DESC,FIELD_VALUE_TYPE,FIELD_SIZE,GROUP_TYPE,CONCAT(LESSEE_CODE,AGREEMENT_NO,SCHEDULE_ID) FROM TMPINDIVINVREPORT WHERE TMP_SERIAL=? AND GRID_ANNEX_BOTH IN ('B','G') ORDER BY LESSEE_CODE,AGREEMENT_NO,SCHEDULE_ID,ORD_SL");
			util.setLong(1, Long.parseLong(resultDTO.get("TEMP_SL")));
			ResultSet rset = util.executeQuery();
			while (rset.next()) {
				if (lessee.equals("") || lessee.equals(rset.getString(7))) {
					lessee = rset.getString(7);
					if (!fields.contains(rset.getString(1))) {
						orderSl.add(rset.getString(2));
						fields.add(rset.getString(1));
						fieldsDesc.add(rset.getString(3));
						fieldsType.add(rset.getString(4));
						groupType.add(rset.getString(6));
					}
				} else {
					break;
				}
			}
			StringBuffer sb = new StringBuffer();
			StringBuffer query = new StringBuffer();
			sb.append("SELECT ");
			for (int i = 0; i < fields.size(); i++) {
				if (i > 0)
					sb.append(",");
				sb.append(" MIN( CASE WHEN FIELD_ID ='" + fields.get(i) + "'  THEN FIELD_VALUE END ) AS " + fields.get(i));
			}
			sb.append(" FROM TMPINDIVINVREPORT WHERE TMP_SERIAL='");
			sb.append(resultDTO.get("TEMP_SL"));
			sb.append("' GROUP BY LESSEE_CODE,AGREEMENT_NO,SCHEDULE_ID");
			if (resultDTO.get("GROUPING_IN_GRID").equals(RegularConstants.ONE)) {
				String group_clause = "";
				query.append("SELECT ");
				for (int i = 0; i < fields.size(); i++) {
					if (i > 0)
						query.append(",");
					if ("S".equals(groupType.get(i))) {
						query.append("SUM(T." + fields.get(i) + ") AS " + fields.get(i));
					} else if ("C".equals(groupType.get(i))) {
						query.append("COUNT(1) AS " + fields.get(i));
					} else {
						query.append("T." + fields.get(i) + " AS " + fields.get(i));
						group_clause = group_clause + fields.get(i);
						group_clause = group_clause + ",";
					}
				}
				group_clause = group_clause.substring(0, group_clause.length() - 1);
				query.append(" FROM (");
				query.append(sb.toString());
				query.append(") T");
				if (group_clause.length() > 0) {
					query.append(" GROUP BY ");
					query.append(group_clause);
				}
			} else {
				query.append(sb.toString());
			}
			resultDTO.set("GRID_QUERY", query.toString());
			resultDTO.setObject("GRID_COLUMN_FIELDS", fields);
			resultDTO.setObject("GRID_COLUMN_DESC", fieldsDesc);
			resultDTO.setObject("GRID_COLUMN_TYPES", fieldsType);
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			dbContext.close();
		}
		return resultDTO;
	}

	private DTObject getConsolidatedAnnexureResult(DTObject resultDTO) {
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		ArrayList<String> fields = new ArrayList<String>();
		ArrayList<String> fieldsDesc = new ArrayList<String>();
		ArrayList<String> fieldsType = new ArrayList<String>();
		ArrayList<String> orderSl = new ArrayList<String>();
		ArrayList<String> groupType = new ArrayList<String>();
		String lessee = "";
		try {
			util.reset();
			util.setSql("SELECT FIELD_ID,ORD_SL,FIELD_DESC,FIELD_VALUE_TYPE,FIELD_SIZE,GROUP_TYPE,CONCAT(LESSEE_CODE,AGREEMENT_NO,SCHEDULE_ID) FROM TMPINDIVINVREPORT WHERE TMP_SERIAL=? AND GRID_ANNEX_BOTH IN ('B','A') ORDER BY LESSEE_CODE,AGREEMENT_NO,SCHEDULE_ID,ORD_SL");
			util.setLong(1, Long.parseLong(resultDTO.get("TEMP_SL")));
			ResultSet rset = util.executeQuery();
			while (rset.next()) {
				if (lessee.equals("") || lessee.equals(rset.getString(7))) {
					lessee = rset.getString(7);
					if (!fields.contains(rset.getString(1))) {
						orderSl.add(rset.getString(2));
						fields.add(rset.getString(1));
						fieldsDesc.add(rset.getString(3));
						fieldsType.add(rset.getString(4));
						groupType.add(rset.getString(6));
					}
				} else {
					break;
				}
			}
			
			StringBuffer sb = new StringBuffer();
			StringBuffer query = new StringBuffer();
			sb.append("SELECT ");
			for (int i = 0; i < fields.size(); i++) {
				if (i > 0)
					sb.append(",");
				sb.append(" MIN( CASE WHEN FIELD_ID ='" + fields.get(i) + "'  THEN FIELD_VALUE END ) AS " + fields.get(i));
			}
			sb.append(" FROM TMPINDIVINVREPORT WHERE TMP_SERIAL='");
			sb.append(resultDTO.get("TEMP_SL"));
			sb.append("' GROUP BY LESSEE_CODE,AGREEMENT_NO,SCHEDULE_ID");
			
			
			
			if (resultDTO.get("GROUPING_IN_ANNEX").equals(RegularConstants.ONE)) {
				String group_clause = "";
				query.append("SELECT ");
				for (int i = 0; i < fields.size(); i++) {
					if (i > 0)
						query.append(",");
					if ("S".equals(groupType.get(i))) {
						query.append("SUM(T." + fields.get(i) + ") AS " + fields.get(i));
					} else if ("C".equals(groupType.get(i))) {
						query.append("COUNT(1) AS " + fields.get(i));
					} else {
						query.append("T." + fields.get(i) + " AS " + fields.get(i));
						group_clause = group_clause + fields.get(i);
						group_clause = group_clause + ",";
					}
				}
				group_clause = group_clause.substring(0, group_clause.length() - 1);
				query.append(" FROM (");
				query.append(sb.toString());
				query.append(") T");
				if (group_clause.length() > 0) {
					query.append(" GROUP BY ");
					query.append(group_clause);
				}
			} else {
				query.append(sb.toString());
			}
			resultDTO.set("ANNEX_QUERY", query.toString());
			resultDTO.setObject("ANNEX_COLUMN_FIELDS", fields);
			resultDTO.setObject("ANNEX_COLUMN_DESC", fieldsDesc);
			resultDTO.setObject("ANNEX_COLUMN_TYPES", fieldsType);
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			dbContext.close();
		}
		return resultDTO;
	}

	public DTObject validateFields(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		CommonValidator validation = new CommonValidator();
		inputDTO.set("ERROR_PRESENT", "0");
		try {
			resultDTO = validateMonth(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				inputDTO.set("INV_MONTH_ERROR", getErrorResource(resultDTO.get(ContentManager.ERROR), null));
				inputDTO.set("ERROR_PRESENT", "1");
			}
			resultDTO = validateYear(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				inputDTO.set("INV_YEAR_ERROR", getErrorResource(resultDTO.get(ContentManager.ERROR), null));
				inputDTO.set("ERROR_PRESENT", "1");
			}
			resultDTO = validateInvoicesForCycleNumber(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				inputDTO.set("INVOICECYCLENUMBER_ERROR", getErrorResource(resultDTO.get(ContentManager.ERROR), null));
				inputDTO.set("ERROR_PRESENT", "1");
			}
			if (inputDTO.get("ERROR_PRESENT").equals("0")) {
				inputDTO.set("INV_RENTAL_DAY_FROM", resultDTO.get("INV_RENTAL_DAY_FROM"));
				inputDTO.set("INV_RENTAL_DAY_UPTO", resultDTO.get("INV_RENTAL_DAY_UPTO"));
			}
			return inputDTO;
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();

		}
		return inputDTO;
	}

	public DTObject validateMonth(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		String invoiceMonth = inputDTO.get("INV_MONTH");
		MasterValidator validator = new MasterValidator();
		try {
			if (validator.isEmpty(invoiceMonth)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			} else if (!validator.isCMLOVREC("COMMON", "MONTH", invoiceMonth)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			return resultDTO;
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return resultDTO;
	}

	public DTObject validateYear(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		String invoiceYear = inputDTO.get("INV_YEAR");
		MasterValidator validator = new MasterValidator();
		try {
			if (validator.isEmpty(invoiceYear)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			if (!validator.isValidYear((invoiceYear))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_YEAR);
				return resultDTO;
			}
			return resultDTO;
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return resultDTO;
	}

}
