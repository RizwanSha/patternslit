package patterns.config.web.forms.inv;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.ArrayList;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.service.DTDObject;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.framework.web.DHTMLXGridUtility;
import patterns.config.framework.web.GenericOption;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.MasterValidator;
import patterns.config.validations.RegistrationValidator;
import patterns.config.web.forms.GenericFormBean;

/**
 * 
 * 
 * @version 1.0
 * @author Kalimuthu U
 * @since 03-APRIL-2017 <br>
 *        <b>EINVOICECAN</b> <BR>
 *        <U><B> Invoice Cancellation </B></U> <br>
 */
public class einvoicecanbean extends GenericFormBean {

	private static final long serialVersionUID = 1L;
	private String cancelRefSerial;
	private String cancelRefDate;
	private String leaseProductCode;
	private String invMonthYear;
	private String invMonth;
	private String stateCode;
	private String dueDate;
	private String customerID;
	private String remarks;
	private String xmleinvoiceCanGrid;
	private String einvoiceCanGridValue;

	public String getCancelRefSerial() {
		return cancelRefSerial;
	}

	public void setCancelRefSerial(String cancelRefSerial) {
		this.cancelRefSerial = cancelRefSerial;
	}

	public String getCancelRefDate() {
		return cancelRefDate;
	}

	public void setCancelRefDate(String cancelRefDate) {
		this.cancelRefDate = cancelRefDate;
	}

	public String getLeaseProductCode() {
		return leaseProductCode;
	}

	public void setLeaseProductCode(String leaseProductCode) {
		this.leaseProductCode = leaseProductCode;
	}

	public String getInvMonthYear() {
		return invMonthYear;
	}

	public void setInvMonthYear(String invMonthYear) {
		this.invMonthYear = invMonthYear;
	}

	public String getInvMonth() {
		return invMonth;
	}

	public void setInvMonth(String invMonth) {
		this.invMonth = invMonth;
	}

	public String getStateCode() {
		return stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	public String getDueDate() {
		return dueDate;
	}

	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}

	public String getCustomerID() {
		return customerID;
	}

	public void setCustomerID(String customerID) {
		this.customerID = customerID;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getXmleinvoiceCanGrid() {
		return xmleinvoiceCanGrid;
	}

	public void setXmleinvoiceCanGrid(String xmleinvoiceCanGrid) {
		this.xmleinvoiceCanGrid = xmleinvoiceCanGrid;
	}

	public String getEinvoiceCanGridValue() {
		return einvoiceCanGridValue;
	}

	public void setEinvoiceCanGridValue(String einvoiceCanGridValue) {
		this.einvoiceCanGridValue = einvoiceCanGridValue;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void reset() {
		cancelRefSerial = RegularConstants.EMPTY_STRING;
		cancelRefDate = RegularConstants.EMPTY_STRING;
		leaseProductCode = RegularConstants.EMPTY_STRING;
		invMonthYear = RegularConstants.EMPTY_STRING;
		invMonth = RegularConstants.EMPTY_STRING;
		stateCode = RegularConstants.EMPTY_STRING;
		dueDate = RegularConstants.EMPTY_STRING;
		customerID = RegularConstants.EMPTY_STRING;
		remarks = RegularConstants.EMPTY_STRING;

		DBContext dbContext = new DBContext();
		ArrayList<GenericOption> invoiceYear = null;
		invoiceYear = getGenericOptions("COMMON", "MONTHS", dbContext, "Select");
		webContext.getRequest().setAttribute("COMMON_MONTHS", invoiceYear);
		setProcessBO("patterns.config.framework.bo.inv.einvoicecanBO");
	}

	public void validate() {
		if (validateCancelRefSerial() && validateCancelRefDate()) {
			validateGrid();
			validateRemarks();
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.setObject("ENTRY_DATE", cancelRefDate);
				formDTO.setObject("ENTRY_SL", cancelRefSerial);
				formDTO.set("REMARKS", remarks);
				DTDObject dtdObject = new DTDObject();
				dtdObject.addColumn(0, "SELECT");
				dtdObject.addColumn(1, "INV_YEAR");
				dtdObject.addColumn(2, "INV_MONTH");
				dtdObject.addColumn(3, "PRODUCT_CODE");
				dtdObject.addColumn(4, "INVOICE_NUMBER");
				dtdObject.addColumn(5, "LESSEE_CODE");
				dtdObject.addColumn(6, "STATE_CODE");
				dtdObject.addColumn(7, "CUSTOMER_ID");
				dtdObject.addColumn(8, "CUSTOMER_NAME");
				dtdObject.addColumn(9, "DUE_DATE");
				dtdObject.addColumn(10, "TOTAL_INVOICES");
				dtdObject.addColumn(11, "CCY");
				dtdObject.addColumn(12, "INVOICE_AMOUNT");
				dtdObject.addColumn(13, "INVOICE_SERIAL");
				dtdObject.setXML(xmleinvoiceCanGrid);
				formDTO.setDTDObject("xmleinvoiceCanGrid", dtdObject);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("einvoiceCanGridValue", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
	}

	public DTObject validate(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		if (!validateLeaseProductCode()) {
			resultDTO.set(ContentManager.ERROR, getErrorMap().getErrorKey("leaseProductCode"));
			resultDTO.set(ContentManager.ERROR_FIELD, "leaseProductCode");
			return resultDTO;
		}
		if (!validateInvoiceMonth()) {
			resultDTO.set(ContentManager.ERROR, getErrorMap().getErrorKey("invMonth"));
			resultDTO.set(ContentManager.ERROR_FIELD, "invMonth");
			return resultDTO;
		}
		if (!validateInvoiceMonthYear()) {
			resultDTO.set(ContentManager.ERROR, getErrorMap().getErrorKey("invMonth"));
			resultDTO.set(ContentManager.ERROR_FIELD, "invMonth");
			return resultDTO;
		}
		if (!validateDueDate()) {
			resultDTO.set(ContentManager.ERROR, getErrorMap().getErrorKey("dueDate"));
			resultDTO.set(ContentManager.ERROR_FIELD, "dueDate");
			return resultDTO;
		}
		if (!validateStateCode()) {
			resultDTO.set(ContentManager.ERROR, getErrorMap().getErrorKey("stateCode"));
			resultDTO.set(ContentManager.ERROR_FIELD, "stateCode");
			return resultDTO;
		}
		if (!validateCustomerID()) {
			resultDTO.set(ContentManager.ERROR, getErrorMap().getErrorKey("customerID"));
			resultDTO.set(ContentManager.ERROR_FIELD, "customerID");
			return resultDTO;
		}
		return resultDTO;
	}

	private boolean validateCancelRefDate() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (commonValidator.isEmpty(cancelRefDate)) {
				getErrorMap().setError("cancelRefDate", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (commonValidator.isDateGreaterThanCBD(cancelRefDate)) {
				getErrorMap().setError("cancelRefDate", BackOfficeErrorCodes.DATE_ECBD);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("cancelRefDate", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}

	private boolean validateCancelRefSerial() {
		CommonValidator validation = new CommonValidator();
		try {
			if (getAction().equals(MODIFY)) {
				getErrorMap().setError("cancelRefSerial", BackOfficeErrorCodes.INVALID_ACTION);
				return false;
			}
			if (getRectify().equals(RegularConstants.COLUMN_ENABLE)) {
				getErrorMap().setError("cancelRefSerial", BackOfficeErrorCodes.INVALID_ACTION);
				return false;
				/*DTObject input = new DTObject();
				DTObject result = new DTObject();
				if (validation.isEmpty(cancelRefSerial)) {
					getErrorMap().setError("cancelRefSerial", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}

				if (!validation.isValidNumber(cancelRefSerial)) {
					getErrorMap().setError("cancelRefSerial", BackOfficeErrorCodes.INVALID_NUMBER);
					return false;
				}
				input.set("ENTRY_DATE", cancelRefDate);
				input.set("ENTRY_SL", cancelRefSerial);
				input.set(ContentManager.ACTION, getAction());
				result = validateCancelRefSerial(input);
				if (result.get(ContentManager.ERROR) != null) {
					getErrorMap().setError("cancelRefSerial", result.get(ContentManager.ERROR));
					return false;
				}
				return true;*/
			} else {
				if (!validation.isEmpty(cancelRefSerial)) {
					getErrorMap().setError("cancelRefSerial", BackOfficeErrorCodes.REFERENCE_SERIAL_AUTO_GENERATED);
					return false;
				}
			}

			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("cancelRefSerial", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public DTObject validateCancelRefSerial(DTObject inputDTO) { //FOR FUTURE USE
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.NULL);
		CommonValidator validator = new CommonValidator();
		try {
			inputDTO.set(ContentManager.TABLE, "INVOICECAN");
			inputDTO.set(ContentManager.FETCH_COLUMNS, "E_STATUS");
			String columns[] = new String[] { context.getEntityCode(), inputDTO.get("ENTRY_DATE"), inputDTO.get("ENTRY_SL") };
			inputDTO.setObject(ContentManager.PROGRAM_KEY, columns);
			resultDTO = validator.validatePK(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (getRectify().equals(RegularConstants.COLUMN_ENABLE)) {
				if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.REFERENCE_DOES_NOT_EXIST);
				}
				// AFTER AUTHORIZATION MODIFICATION NOT ALLOWED
				if (resultDTO.getObject("E_STATUS").equals(RegularConstants.AUTHORIZED)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.REFERENCE_REJ_REF_MODIFY_NA);
					return resultDTO;
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return resultDTO;
	}

	private boolean validateLeaseProductCode() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("LEASE_PRODUCT_CODE", leaseProductCode);
			formDTO.set(ContentManager.USER_ACTION, USAGE);
			formDTO = validateLeaseProductCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("leaseProductCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("leaseProductCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateLeaseProductCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		MasterValidator validation = new MasterValidator();
		String leaseProductCode = inputDTO.get("LEASE_PRODUCT_CODE");
		try {
			if (validation.isEmpty(leaseProductCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
			resultDTO = validation.validateLeaseProductCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	private boolean validateInvoiceMonth() {
		CommonValidator validator = new CommonValidator();
		try {
			if (validator.isEmpty(invMonth)) {
				getErrorMap().setError("invMonth", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validator.isCMLOVREC("COMMON", "MONTHS", invMonth)) {
				getErrorMap().setError("invMonth", BackOfficeErrorCodes.INVALID_OPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("invMonth", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return false;
	}

	private boolean validateInvoiceMonthYear() {
		CommonValidator validator = new CommonValidator();
		try {
			if (validator.isEmpty(invMonthYear)) {
				getErrorMap().setError("invMonth", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validator.isValidYear(invMonthYear)) {
				getErrorMap().setError("invMonth", BackOfficeErrorCodes.INVALID_YEAR);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("invMonthYear", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return false;
	}

	public boolean validateStateCode() {
		try {
			if (!stateCode.equals(RegularConstants.EMPTY_STRING)) {
				DTObject formDTO = getFormDTO();
				formDTO.set("STATE_CODE", stateCode);
				formDTO.set(ContentManager.USER_ACTION, USAGE);
				formDTO = validateStateCode(formDTO);
				if (formDTO.get(ContentManager.ERROR) != null) {
					getErrorMap().setError("stateCode", formDTO.get(ContentManager.ERROR));
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("stateCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateStateCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		MasterValidator validation = new MasterValidator();
		String stateCode = inputDTO.get("STATE_CODE");
		try {
			if (validation.isEmpty(stateCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
			resultDTO = validation.validateStateCode(inputDTO);
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	private boolean validateDueDate() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (!commonValidator.isEmpty(dueDate)) {
				if (!commonValidator.isDateGreaterThanCBD(dueDate)) {
					getErrorMap().setError("dueDate", BackOfficeErrorCodes.INVALID_DATE);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("dueDate", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}

	public boolean validateCustomerID() {
		try {
			if (!customerID.equals(RegularConstants.EMPTY_STRING)) {
				DTObject formDTO = new DTObject();
				formDTO.set("CUSTOMER_ID", customerID);
				formDTO.set(ContentManager.ACTION, USAGE);
				formDTO = validateCustomerID(formDTO);
				if (formDTO.get(ContentManager.ERROR) != null) {
					getErrorMap().setError("customerID", formDTO.get(ContentManager.ERROR));
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("customerID", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateCustomerID(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		RegistrationValidator validation = new RegistrationValidator();
		String customerId = inputDTO.get("CUSTOMER_ID");
		try {
			if (validation.isEmpty(customerId)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			if (!validation.isValidNumber(customerId)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.NUMERIC_CHECK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "CUSTOMER_NAME");
			resultDTO = validation.valildateCustomerID(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public DTObject loadInvoiceDetails(DTObject inputDTO) {
		logger.logDebug("loadInvoiceDetails() Begin");
		DTObject resultDTO = new DTObject();
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		setLeaseProductCode(inputDTO.get("LEASE_PRODUCT_CODE"));
		setInvMonthYear(inputDTO.get("INV_YEAR"));
		setInvMonth(inputDTO.get("INV_MONTH"));
		setStateCode(inputDTO.get("STATE_CODE"));
		setDueDate(inputDTO.get("DUE_DATE"));
		setCustomerID(inputDTO.get("CUSTOMER_ID"));
		DHTMLXGridUtility gridUtility = new DHTMLXGridUtility();
		StringBuffer sqlQuery = new StringBuffer(RegularConstants.EMPTY_STRING);
		int i = 4;
		try {
			DTObject validateDTO = validate(inputDTO);
			if (validateDTO.get(ContentManager.ERROR) != null) {
				return validateDTO;
			}
			util.reset();
			util.setMode(DBUtil.PREPARED);
			sqlQuery.append("SELECT G.INV_SERIAL,G.STATE_CODE,G.CUSTOMER_ID,G.INV_YEAR,G.INV_MONTH,G.LEASE_PRODUCT_CODE,TO_CHAR(G.DUE_DATE,'" + context.getDateFormat() + "')DUE_DATE,");
			sqlQuery.append("FN_FORMAT_INVOICE_NUMBER(G.LEASE_PRODUCT_CODE,G.INV_YEAR,G.INV_MONTH,G.INV_SERIAL) AS INV_SERIAL_COMPINATION,");
			sqlQuery.append("G.INV_CCY,G.TOTAL_INV,");
			sqlQuery.append(" FN_FORMAT_AMOUNT(G.TOTAL_INV_AMT,G.INV_CCY,CU.SUB_CCY_UNITS_IN_CCY) AS TOTAL_INV_AMT,CA.SUNDRY_DB_AC, C.CUSTOMER_NAME");
			sqlQuery.append(" FROM INVOICES G JOIN CUSTOMER C ON(C.ENTITY_CODE=G.ENTITY_CODE AND C.CUSTOMER_ID=G.CUSTOMER_ID)");
			sqlQuery.append(" JOIN CUSTOMERACDTL CA ON (CA.ENTITY_CODE=G.ENTITY_CODE AND CA.CUSTOMER_ID=G.CUSTOMER_ID AND CA.LEASE_PRODUCT_CODE=G.LEASE_PRODUCT_CODE)");
			sqlQuery.append(" INNER JOIN CURRENCY CU ON (G.INV_CCY=CU.CCY_CODE)");
			sqlQuery.append(" WHERE G.ENTITY_CODE=? AND G.LEASE_PRODUCT_CODE=? AND G.INV_YEAR=? AND G.INV_MONTH=? AND G.CANCEL_ENTRY_DATE IS NULL");
			if (!stateCode.equals(RegularConstants.EMPTY_STRING)) {
				sqlQuery.append(" AND G.STATE_CODE=?");
			}
			if (!dueDate.equals(RegularConstants.EMPTY_STRING)) {
				sqlQuery.append(" AND G.DUE_DATE=?");
			}
			if (!customerID.equals(RegularConstants.EMPTY_STRING)) {
				sqlQuery.append(" AND G.CUSTOMER_ID=?");
			}
			util.setSql(sqlQuery.toString());
			util.setLong(1, Long.parseLong(context.getEntityCode()));
			util.setString(2, leaseProductCode);
			util.setString(3, invMonthYear);
			util.setString(4, invMonth);
			if (!stateCode.equals(RegularConstants.EMPTY_STRING)) {
				i++;
				util.setString(i, stateCode);
			}
			if (!dueDate.equals(RegularConstants.EMPTY_STRING)) {
				i++;
				Date dueDateSql = new java.sql.Date(BackOfficeFormatUtils.getDate(dueDate, BackOfficeConstants.TBA_DATE_FORMAT).getTime());
				util.setDate(i, dueDateSql);
			}
			if (!customerID.equals(RegularConstants.EMPTY_STRING)) {
				i++;
				util.setString(i, customerID);
			}
			ResultSet rset = util.executeQuery();
			gridUtility.init();
			if (rset.next()) {
				do {
					gridUtility.startRow();
					gridUtility.setCell("");
					gridUtility.setCell(rset.getString("INV_YEAR"));
					gridUtility.setCell(rset.getString("INV_MONTH"));
					gridUtility.setCell(rset.getString("LEASE_PRODUCT_CODE"));
					gridUtility.setCell(rset.getString("INV_SERIAL_COMPINATION"));
					gridUtility.setCell(rset.getString("SUNDRY_DB_AC"));
					gridUtility.setCell(rset.getString("STATE_CODE"));
					gridUtility.setCell(rset.getString("CUSTOMER_ID"));
					gridUtility.setCell(rset.getString("CUSTOMER_NAME"));
					gridUtility.setCell(rset.getString("DUE_DATE"));
					gridUtility.setCell(rset.getString("TOTAL_INV"));
					gridUtility.setCell(rset.getString("INV_CCY"));
					gridUtility.setCell(rset.getString("TOTAL_INV_AMT"));
					gridUtility.setCell(rset.getString("INV_SERIAL"));
					gridUtility.endRow();
				} while (rset.next());
			} else {
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
				return resultDTO;
			}
			gridUtility.finish();
			String resultXML = gridUtility.getXML();
			inputDTO.set(ContentManager.RESULT_XML, resultXML);
			inputDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);

		} catch (Exception e) {
			e.printStackTrace();
			inputDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			dbContext.close();
		}
		logger.logDebug("loadInvoiceDetails() End");
		return inputDTO;
	}

	private boolean validateGrid() {
		CommonValidator validation = new CommonValidator();
		try {
			DTDObject dtdObject = new DTDObject();
			dtdObject.addColumn(0, "SELECT");
			dtdObject.addColumn(1, "INV_YEAR");
			dtdObject.addColumn(2, "INV_MONTH");
			dtdObject.addColumn(3, "PRODUCT_CODE");
			dtdObject.addColumn(4, "INVOICE_NUMBER");
			dtdObject.addColumn(5, "LESSEE_CODE");
			dtdObject.addColumn(6, "STATE_CODE");
			dtdObject.addColumn(7, "CUSTOMER_ID");
			dtdObject.addColumn(8, "CUSTOMER_NAME");
			dtdObject.addColumn(9, "DUE_DATE");
			dtdObject.addColumn(10, "TOTAL_INVOICES");
			dtdObject.addColumn(11, "CCY");
			dtdObject.addColumn(12, "INVOICE_AMOUNT");
			dtdObject.addColumn(13, "INVOICE_SERIAL");
			dtdObject.setXML(xmleinvoiceCanGrid);
			if (dtdObject.getRowCount() == 0) {
				getErrorMap().setError("einvoiceCanGridValue", BackOfficeErrorCodes.ATLEAST_ONE_ROW);
				return false;
			}
			for (int i = 0; i < dtdObject.getRowCount(); i++) {
				if (validateCancelInvoice(dtdObject.getValue(i, 3), dtdObject.getValue(i, 1), dtdObject.getValue(i, 2), dtdObject.getValue(i, 13))) {
					getErrorMap().setError("einvoiceCanGridValue", BackOfficeErrorCodes.INVOICE_ALREADY_CANCEL);
					return false;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("einvoiceCanGridValue", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return true;
	}

	boolean getCancelInvoice() {
		return true;
	}

	private boolean validateCancelInvoice(String leaseProductCode, String invMonthYear, String invMonth, String invSerial) {
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		try {
			StringBuffer sqlQuery = new StringBuffer();
			sqlQuery.append("SELECT G.CANCEL_ENTRY_DATE,G.CANCEL_ENTRY_SL FROM INVOICES G");
			sqlQuery.append(" WHERE G.ENTITY_CODE=? AND INV_YEAR=? AND INV_MONTH=? AND LEASE_PRODUCT_CODE=? AND INV_SERIAL=? AND CANCEL_ENTRY_DATE IS NOT NULL");
			util.setMode(DBUtil.PREPARED);
			util.setSql(sqlQuery.toString());

			util.setLong(1, Long.parseLong(context.getEntityCode()));
			util.setInt(2, Integer.parseInt(invMonthYear));
			util.setInt(3, Integer.parseInt(invMonth));
			util.setString(4, leaseProductCode);
			util.setLong(5, Long.parseLong(invSerial));
			ResultSet rs = util.executeQuery();
			if (rs.next()) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			util.reset();
			dbContext.close();
		}
		return true;
	}

	private boolean validateRemarks() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (remarks != null && remarks.length() > 0) {
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			if (commonValidator.isEmpty(remarks)) {
				getErrorMap().setError("remarks", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!commonValidator.isValidRemarks(remarks)) {
				getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
				return false;
			}

			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}
}
