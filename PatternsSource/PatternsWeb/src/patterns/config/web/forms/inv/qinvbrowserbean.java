package patterns.config.web.forms.inv;

import java.sql.ResultSet;
import java.util.ArrayList;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.framework.web.DHTMLXGridUtility;
import patterns.config.framework.web.GenericOption;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.LeaseValidator;
import patterns.config.validations.MasterValidator;
import patterns.config.validations.RegistrationValidator;
import patterns.config.web.forms.GenericFormBean;

public class qinvbrowserbean extends GenericFormBean {

	private static final long serialVersionUID = 1L;
	private String fromDate;
	private String uptoDate;
	private String lesseeCode;
	private String customerId;
	private String productCode;
	private String dueFor;
	private String status;
	private String fromDateLowerLimit;
	private String nofMonthAllowed;

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getUptoDate() {
		return uptoDate;
	}

	public void setUptoDate(String uptoDate) {
		this.uptoDate = uptoDate;
	}

	public String getLesseeCode() {
		return lesseeCode;
	}

	public void setLesseeCode(String lesseeCode) {
		this.lesseeCode = lesseeCode;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getDueFor() {
		return dueFor;
	}

	public void setDueFor(String dueFor) {
		this.dueFor = dueFor;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getFromDateLowerLimit() {
		return fromDateLowerLimit;
	}

	public void setFromDateLowerLimit(String fromDateLowerLimit) {
		this.fromDateLowerLimit = fromDateLowerLimit;
	}

	public String getNofMonthAllowed() {
		return nofMonthAllowed;
	}

	public void setNofMonthAllowed(String nofMonthAllowed) {
		this.nofMonthAllowed = nofMonthAllowed;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public void reset() {
		fromDate = RegularConstants.EMPTY_STRING;
		uptoDate = RegularConstants.EMPTY_STRING;
		lesseeCode = RegularConstants.EMPTY_STRING;
		customerId = RegularConstants.EMPTY_STRING;
		productCode = RegularConstants.EMPTY_STRING;
		dueFor = RegularConstants.EMPTY_STRING;
		status = RegularConstants.EMPTY_STRING;
		fromDateLowerLimit = RegularConstants.EMPTY_STRING;
		nofMonthAllowed = RegularConstants.EMPTY_STRING;

		DTObject resultDTO = new DTObject();
		resultDTO = getDateRangeParam();
		if (resultDTO.get(ContentManager.ERROR) == null) {
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
				fromDateLowerLimit = resultDTO.get("FROMDATE_LOWER_LIMIT");
				nofMonthAllowed = resultDTO.get("NO_OF_MONTHS_ALLOWED");
			}
		}
		DBContext dbContext = new DBContext();
		ArrayList<GenericOption> dueforList = null;
		dueforList = getGenericOptions("QINVBROWSER", "DUE_FOR", dbContext, null);
		webContext.getRequest().setAttribute("QINVBROWSER_DUE_FOR", dueforList);

		ArrayList<GenericOption> statusList = null;
		statusList = getGenericOptions("QINVBROWSER", "STATUS", dbContext, null);
		webContext.getRequest().setAttribute("QINVBROWSER_STATUS", statusList);
		dbContext.close();
	}

	@Override
	public void validate() {

	}

	public DTObject getDateRangeParam() {
		DTObject resultDTO = new DTObject();
		DBContext dbContext = new DBContext();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		try {
			DBUtil util = dbContext.createUtilInstance();
			util.reset();
			util.setSql("SELECT TO_CHAR(FROMDATE_LOWER_LIMIT,?) FROMDATE_LOWER_LIMIT,NO_OF_MONTHS_ALLOWED FROM REPORTDATERANGEPARAM WHERE ENTITY_CODE=? AND PROGRAM_ID=?");
			util.setString(1, context.getDateFormat());
			util.setLong(2, Long.parseLong(context.getEntityCode()));
			util.setString(3, context.getProcessID());
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				resultDTO.set("FROMDATE_LOWER_LIMIT", rset.getString("FROMDATE_LOWER_LIMIT"));
				resultDTO.set("NO_OF_MONTHS_ALLOWED", rset.getString("NO_OF_MONTHS_ALLOWED"));
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			dbContext.close();
		}
		return resultDTO;
	}

	public DTObject validateLesseeCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		DTObject result = new DTObject();
		resultDTO.set(ContentManager.ERROR, ContentManager.DATA_UNAVAILABLE);
		RegistrationValidator validation = new RegistrationValidator();
		MasterValidator validator = new MasterValidator();
		LeaseValidator validate = new LeaseValidator();
		String custLeaseCode = inputDTO.get("SUNDRY_DB_AC");
		try {
			if (!validation.isEmpty(custLeaseCode)) {
			}
			resultDTO = getCustomerLesseeCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
			String leaseProduct = resultDTO.get("LEASE_PRODUCT_CODE");
			inputDTO.set("CUSTOMER_ID", resultDTO.get("CUSTOMER_ID"));
			inputDTO.set(ContentManager.FETCH_COLUMNS, "CUSTOMER_ID,CUSTOMER_NAME");
			resultDTO = validation.valildateCustomerID(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			} else if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
				inputDTO.set("LEASE_PRODUCT_CODE", leaseProduct);
				inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
				result = validator.validateLeaseProductCode(inputDTO);
				if (result.get(ContentManager.ERROR) != null) {
					resultDTO.set(ContentManager.ERROR, result.get(ContentManager.ERROR));
					return resultDTO;
				}
				if (result.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
					return resultDTO;
				}
				resultDTO.set("LEASE_PRODUCT_CODE_DESC", result.get("DESCRIPTION"));
				resultDTO.set("LEASE_PRODUCT_CODE", leaseProduct);
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
			validator.close();
			validate.close();
		}
		return resultDTO;
	}

	public DTObject getCustomerLesseeCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		try {
			String sqlQuery = "SELECT CUSTOMER_ID,LEASE_PRODUCT_CODE FROM CUSTOMERACDTL WHERE ENTITY_CODE=? AND SUNDRY_DB_AC=?";
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, context.getEntityCode());
			util.setString(2, inputDTO.get("SUNDRY_DB_AC"));
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				resultDTO.set("CUSTOMER_ID", rset.getString("CUSTOMER_ID"));
				resultDTO.set("LEASE_PRODUCT_CODE", rset.getString("LEASE_PRODUCT_CODE"));
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			util.reset();
			dbContext.close();
		}
		return resultDTO;
	}

	public DTObject validateCustomerId(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		RegistrationValidator validation = new RegistrationValidator();
		String customerID = inputDTO.get("CUSTOMER_ID");
		try {
			if (!validation.isEmpty(customerID)) {
				inputDTO.set(ContentManager.FETCH_COLUMNS, "CUSTOMER_NAME");
				resultDTO = validation.valildateCustomerID(inputDTO);
				if (resultDTO.get(ContentManager.ERROR) != null) {
					resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
					return resultDTO;
				}
				if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
					return resultDTO;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public DTObject validateProductCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		MasterValidator validation = new MasterValidator();
		String productCode = inputDTO.get("LEASE_PRODUCT_CODE");
		try {
			if (!validation.isEmpty(productCode)) {
				inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
				resultDTO = validation.validateLeaseProductCode(inputDTO);
				if (resultDTO.get(ContentManager.ERROR) != null) {
					resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
					return resultDTO;
				}
				if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
					return resultDTO;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public DTObject doLoadGrid(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		StringBuffer query = new StringBuffer();
		CommonValidator validator = new CommonValidator();
		boolean rowExists = false;
		try {
			setFromDate(inputDTO.get("FROM_DATE"));
			setUptoDate(inputDTO.get("UPTO_DATE"));
			setLesseeCode(inputDTO.get("SUNDRY_DB_AC"));
			setCustomerId(inputDTO.get("CUSTOMER_ID"));
			setProductCode(inputDTO.get("LEASE_PRODUCT_CODE"));
			setDueFor(inputDTO.get("DUE_FOR"));
			setStatus(inputDTO.get("STATUS"));
			setFromDateLowerLimit(inputDTO.get("FROMDATE_LOWER_LIMIT"));
			setNofMonthAllowed(inputDTO.get("NO_OF_MONTH_ALLOWED"));
			DTObject validateDTO = validate(inputDTO);
			if (validateDTO.get(ContentManager.ERROR) != null) {
				return validateDTO;
			}
			DBUtil util = validator.getDbContext().createUtilInstance();
			query.append("SELECT TO_CHAR(LFS.SCHEDULED_INV_DATE,?) SCHEDULED_INV_DATE,L.LEASE_PRODUCT_CODE");
			query.append(",LFS.LESSEE_CODE,LFS.AGREEMENT_NO,LFS.SCHEDULE_ID,C.CUSTOMER_NAME,L.CUSTOMER_ID,CM.LOV_LABEL_EN_US DUE_FOR,");
			query.append("TO_CHAR(LFS.RENTAL_START_DATE,?) RENTAL_START_DATE,TO_CHAR(LFS.RENTAL_END_DATE,?)");
			query.append("RENTAL_END_DATE,TO_CHAR(LFS.COMP_ENTRY_DATE,?) COMP_ENTRY_DATE,LFS.COMP_ENTRY_SL,");
			query.append("FN_FORMAT_INVOICE_NUMBER(I.LEASE_PRODUCT_CODE,I.INV_YEAR,I.INV_MONTH,I.INV_SERIAL) AS FORMAT_INVOICE_NUMBER");
			query.append(" FROM LEASEFINANCESCHEDULE LFS ");
			query.append(" JOIN CMLOVREC CM ON (CM.PGM_ID='QINVBROWSER' AND CM.LOV_ID='DUE_FOR' AND CM.LOV_VALUE=LFS.INV_FOR)");
			query.append(" JOIN LEASE L ON (L.ENTITY_CODE=LFS.ENTITY_CODE AND L.LESSEE_CODE=LFS.LESSEE_CODE ");
			query.append(" AND L.AGREEMENT_NO=LFS.AGREEMENT_NO AND L.SCHEDULE_ID=LFS.SCHEDULE_ID AND ");
			query.append(" L.LEASE_CLOSURE_ON IS NULL AND L.E_STATUS='A'");
			if (!validator.isEmpty(inputDTO.get("LEASE_PRODUCT_CODE"))) {
				query.append(" AND L.LEASE_PRODUCT_CODE='");
				query.append(inputDTO.get("LEASE_PRODUCT_CODE"));
				query.append("'");
			}
			if (!validator.isEmpty(inputDTO.get("CUSTOMER_ID"))) {
				query.append(" AND L.CUSTOMER_ID='");
				query.append(inputDTO.get("CUSTOMER_ID"));
				query.append("'");
			}
			query.append(")");
			query.append(" JOIN CUSTOMER C ON(C.ENTITY_CODE=L.ENTITY_CODE AND C.CUSTOMER_ID=L.CUSTOMER_ID)");
			query.append(" LEFT OUTER JOIN INVCOMPONENT I ON (I.ENTITY_CODE=LFS.ENTITY_CODE AND I.ENTRY_DATE=LFS.COMP_ENTRY_DATE AND I.ENTRY_SL=LFS.COMP_ENTRY_SL) ");
			query.append(" WHERE LFS.ENTITY_CODE=? AND LFS.SCHEDULED_INV_DATE BETWEEN ? AND ?");
			if (!validator.isEmpty(inputDTO.get("SUNDRY_DB_AC"))) {
				query.append(" AND LFS.LESSEE_CODE='");
				query.append(inputDTO.get("SUNDRY_DB_AC"));
				query.append("'");
			}
			if (!validator.isEmpty(inputDTO.get("DUE_FOR")) && !RegularConstants.ZERO.equals(inputDTO.get("DUE_FOR"))) {
				query.append(" AND LFS.INV_FOR='");
				query.append(inputDTO.get("DUE_FOR"));
				query.append("'");
			}
			if (!validator.isEmpty(inputDTO.get("STATUS")) && !"B".equals(inputDTO.get("STATUS"))) {
				if ("N".equals(inputDTO.get("STATUS")))
					query.append(" AND LFS.COMP_ENTRY_DATE IS NULL");
				else if ("I".equals(inputDTO.get("STATUS")))
					query.append(" AND LFS.COMP_ENTRY_DATE IS NOT NULL");
			}
			util.reset();
			util.setSql(query.toString());
			util.setString(1, context.getDateFormat());
			util.setString(2, context.getDateFormat());
			util.setString(3, context.getDateFormat());
			util.setString(4, context.getDateFormat());
			util.setLong(5, Long.parseLong(context.getEntityCode()));
			util.setDate(6, new java.sql.Date(BackOfficeFormatUtils.getDate(inputDTO.get("FROM_DATE"), context.getDateFormat()).getTime()));
			util.setDate(7, new java.sql.Date(BackOfficeFormatUtils.getDate(inputDTO.get("UPTO_DATE"), context.getDateFormat()).getTime()));
			ResultSet rset = util.executeQuery();
			DHTMLXGridUtility gridUtility = new DHTMLXGridUtility();
			gridUtility.init();
			while (rset.next()) {
				gridUtility.startRow();
				gridUtility.setCell(rset.getString("SCHEDULED_INV_DATE"));
				gridUtility.setCell(rset.getString("LEASE_PRODUCT_CODE"));
				gridUtility.setCell(rset.getString("LESSEE_CODE"));
				gridUtility.setCell(rset.getString("AGREEMENT_NO"));
				gridUtility.setCell("<a href='javascript:void(0);' onclick=viewLease('" + rset.getString("LESSEE_CODE") + "|" + rset.getString("AGREEMENT_NO") + "|" + rset.getString("SCHEDULE_ID") + "')>" + rset.getString("SCHEDULE_ID") + "</a>");
				gridUtility.setCell(rset.getString("CUSTOMER_NAME"));
				gridUtility.setCell("<a href='javascript:void(0);' onclick=viewCustomerReg('" + rset.getString("CUSTOMER_ID") + "')>" + rset.getString("CUSTOMER_ID") + "</a>");
				gridUtility.setCell(rset.getString("DUE_FOR"));
				gridUtility.setCell(rset.getString("RENTAL_START_DATE"));
				gridUtility.setCell(rset.getString("RENTAL_END_DATE"));
				if (rset.getString("COMP_ENTRY_DATE") != null)
				gridUtility.setCell("<a href='javascript:void(0);' onclick=invoiceReference('" + rset.getString("COMP_ENTRY_DATE") + "|" + rset.getString("COMP_ENTRY_SL") + "')>" + rset.getString("COMP_ENTRY_DATE") + "|" + rset.getString("COMP_ENTRY_SL") + "</a>");
				if (rset.getString("FORMAT_INVOICE_NUMBER") != null)
					gridUtility.setCell("<a href='javascript:void(0);' onclick=invoiceNumber('" + rset.getString("FORMAT_INVOICE_NUMBER") + "')>" + rset.getString("FORMAT_INVOICE_NUMBER") + "</a>");
				else
					gridUtility.setCell(RegularConstants.EMPTY_STRING);
				gridUtility.endRow();
				rowExists = true;
			}
			util.reset();
			if (rowExists) {
				gridUtility.finish();
				String resultXML = gridUtility.getXML();
				resultDTO.set(ContentManager.RESULT_XML, resultXML);
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
			} else {
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return resultDTO;
	}

	public DTObject validate(DTObject inputDTO) {
		logger.logDebug("validate() Begin");
		DTObject resultDTO = new DTObject();
		if (!validateFromDate()) {
			resultDTO.set(ContentManager.ERROR, getErrorMap().getErrorKey("fromDate"));
			resultDTO.set(ContentManager.ERROR_FIELD, "fromDate");
			if (getErrorMap().getErrorKey("fromDate").equals(BackOfficeErrorCodes.DATE_GE_LOWER_LIMIT_DATE)) {
				Object[] param = { fromDateLowerLimit };
				resultDTO.setObject(ContentManager.ERROR_PARAM, param);
			}
			return resultDTO;
		}
		if (!validateUpToDate()) {
			resultDTO.set(ContentManager.ERROR, getErrorMap().getErrorKey("uptoDate"));
			resultDTO.set(ContentManager.ERROR_FIELD, "uptoDate");
			if (getErrorMap().getErrorKey("uptoDate").equals(BackOfficeErrorCodes.RANGE_LE_MONTHS)) {
				Object[] param = { nofMonthAllowed };
				resultDTO.setObject(ContentManager.ERROR_PARAM, param);
			}
			return resultDTO;
		}
		if (!validateLesseeCode()) {
			resultDTO.set(ContentManager.ERROR, getErrorMap().getErrorKey("lesseeCode"));
			resultDTO.set(ContentManager.ERROR_FIELD, "lesseeCode");
			return resultDTO;
		}
		if (!validateCustomerID()) {
			resultDTO.set(ContentManager.ERROR, getErrorMap().getErrorKey("customerId"));
			resultDTO.set(ContentManager.ERROR_FIELD, "customerId");
			return resultDTO;
		}
		if (!validateProductCode()) {
			resultDTO.set(ContentManager.ERROR, getErrorMap().getErrorKey("productCode"));
			resultDTO.set(ContentManager.ERROR_FIELD, "productCode");
			return resultDTO;
		}
		if (!validateDueFor()) {
			resultDTO.set(ContentManager.ERROR, getErrorMap().getErrorKey("dueFor"));
			resultDTO.set(ContentManager.ERROR_FIELD, "dueFor");
			return resultDTO;
		}
		if (!validateStatus()) {
			resultDTO.set(ContentManager.ERROR, getErrorMap().getErrorKey("status"));
			resultDTO.set(ContentManager.ERROR_FIELD, "status");
			return resultDTO;
		}
		logger.logDebug("validate() end");
		return resultDTO;
	}

	public boolean validateFromDate() {
		CommonValidator validation = new CommonValidator();
		try {
			java.util.Date dateInstance = validation.isValidDate(fromDate);
			if (validation.isEmpty(fromDate)) {
				getErrorMap().setError("fromDate", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (dateInstance == null) {
				getErrorMap().setError("fromDate", BackOfficeErrorCodes.INVALID_DATE);
				return false;
			}
			if (validation.isDateLesser(fromDate, fromDateLowerLimit)) {
				getErrorMap().setError("fromDate", BackOfficeErrorCodes.DATE_GE_LOWER_LIMIT_DATE);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("fromDate", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateUpToDate() {
		CommonValidator validation = new CommonValidator();
		try {
			java.util.Date uptoDatedateInstance = validation.isValidDate(uptoDate);
			if (validation.isEmpty(uptoDate)) {
				getErrorMap().setError("uptoDate", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (uptoDatedateInstance == null) {
				getErrorMap().setError("uptoDate", BackOfficeErrorCodes.INVALID_DATE);
				return false;
			}
			if (!validation.isDateGreaterThanEqualCBD(uptoDate)) {
				getErrorMap().setError("uptoDate", BackOfficeErrorCodes.DATE_GECBD);
				return false;
			}
			DBUtil util = validation.getDbContext().createUtilInstance();
			util.reset();
			util.setSql("SELECT TIMESTAMPDIFF(MONTH, ?,?)");
			util.setDate(1, new java.sql.Date(BackOfficeFormatUtils.getDate(fromDate, BackOfficeConstants.JAVA_DATE_FORMAT).getTime()));
			util.setDate(2, new java.sql.Date(BackOfficeFormatUtils.getDate(uptoDate, BackOfficeConstants.JAVA_DATE_FORMAT).getTime()));
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				if (rset.getInt(1) > Integer.parseInt(nofMonthAllowed)) {
					getErrorMap().setError("uptoDate", BackOfficeErrorCodes.RANGE_LE_MONTHS);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("uptoDate", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	private boolean validateLesseeCode() {
		CommonValidator validation = new CommonValidator();
		try {
			if (!validation.isEmpty(lesseeCode)) {
				DTObject formDTO = new DTObject();
				formDTO.set("SUNDRY_DB_AC", lesseeCode);
				formDTO.set(ContentManager.USER_ACTION, USAGE);
				formDTO = validateLesseeCode(formDTO);
				if (formDTO.get(ContentManager.ERROR) != RegularConstants.NULL) {
					getErrorMap().setError("lesseeCode", formDTO.get(ContentManager.ERROR));
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("lesseeCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public boolean validateCustomerID() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("CUSTOMER_ID", customerId);
			formDTO.set(ContentManager.USER_ACTION, USAGE);
			formDTO = validateCustomerId(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {

				getErrorMap().setError("customerId", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("customerId", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public boolean validateProductCode() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("LEASE_PRODUCT_CODE", productCode);
			formDTO.set(ContentManager.USER_ACTION, USAGE);
			formDTO = validateProductCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("productCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("customerId", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public boolean validateDueFor() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(dueFor)) {
				getErrorMap().setError("dueFor", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isCMLOVREC("QINVBROWSER", "DUE_FOR", dueFor)) {
				getErrorMap().setError("dueFor", BackOfficeErrorCodes.INVALID_OPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("dueFor", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateStatus() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(status)) {
				getErrorMap().setError("status", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isCMLOVREC("QINVBROWSER", "STATUS", status)) {
				getErrorMap().setError("status", BackOfficeErrorCodes.INVALID_OPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("status", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}
}
