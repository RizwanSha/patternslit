package patterns.config.web.forms.inv;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.framework.web.GenericOption;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.MasterValidator;
import patterns.config.validations.RegistrationValidator;
import patterns.config.web.forms.GenericFormBean;

public class rinvbulkgenbean extends GenericFormBean {

	private static final long serialVersionUID = -6219657363150466160L;
	private String billingCycle;
	private String month;
	private String monthYear;
	private String fromDate;
	private String uptoDate;
	private String productCode;
	private String customerId;
	static final int BUFFER = 1048576;

	public String getBillingCycle() {
		return billingCycle;
	}

	public void setBillingCycle(String billingCycle) {
		this.billingCycle = billingCycle;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public String getMonthYear() {
		return monthYear;
	}

	public void setMonthYear(String monthYear) {
		this.monthYear = monthYear;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getUptoDate() {
		return uptoDate;
	}

	public void setUptoDate(String uptoDate) {
		this.uptoDate = uptoDate;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void validate() {
	}

	@Override
	public void reset() {
		billingCycle = RegularConstants.EMPTY_STRING;
		month = RegularConstants.EMPTY_STRING;
		monthYear = RegularConstants.EMPTY_STRING;
		fromDate = RegularConstants.EMPTY_STRING;
		uptoDate = RegularConstants.EMPTY_STRING;
		productCode = RegularConstants.EMPTY_STRING;
		customerId = RegularConstants.EMPTY_STRING;
		DBContext dbContext = new DBContext();
		ArrayList<GenericOption> monthList = null;
		monthList = getGenericOptions("COMMON", "MONTH", dbContext, null);
		webContext.getRequest().setAttribute("COMMON_MONTH", monthList);
		dbContext.close();
	}

	public DTObject validateBillingCycle(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		String invForCycleNumber = inputDTO.get("INV_CYCLE_NUMBER");
		MasterValidator validator = new MasterValidator();
		try {
			if (validator.isEmpty(invForCycleNumber)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "INV_RENTAL_DAY_FROM,INV_RENTAL_DAY_UPTO");
			resultDTO = validator.validateInvoiceCycleNumber(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
			return resultDTO;
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return resultDTO;
	}

	public DTObject validateLeaseProductCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		MasterValidator validation = new MasterValidator();
		try {
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
			resultDTO = validation.validateLeaseProductCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}else {
				if(RegularConstants.ZERO.equals(inputDTO.get("SERVER_SIDE"))){
					String productCodeDesc = resultDTO.get("DESCRIPTION");
					resultDTO = getInvoiceCount(inputDTO);
					if (resultDTO.get(ContentManager.ERROR) != null) {
						return resultDTO;
					}
					resultDTO.set("DESCRIPTION", productCodeDesc);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public DTObject validateCustomerId(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		RegistrationValidator validation = new RegistrationValidator();
		String customerId = inputDTO.get("CUSTOMER_ID");
		try {
			if (!validation.isEmpty(customerId)) {
				if (!validation.isValidNumber(customerId)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.NUMERIC_CHECK);
					return resultDTO;
				}
				inputDTO.set(ContentManager.FETCH_COLUMNS, "CUSTOMER_NAME");
				resultDTO = validation.valildateCustomerID(inputDTO);
				if (resultDTO.get(ContentManager.ERROR) != null) {
					resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
					return resultDTO;
				}
				if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
					return resultDTO;
				}
			}
			if(RegularConstants.ZERO.equals(inputDTO.get("SERVER_SIDE"))){
				String customerName = resultDTO.get("CUSTOMER_NAME");
				resultDTO = getInvoiceCount(inputDTO);
				if (resultDTO.get(ContentManager.ERROR) != null) {
					return resultDTO;
				}
				resultDTO.set("CUSTOMER_NAME", customerName);
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public DTObject getInvoiceCount(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		CommonValidator validator = new CommonValidator();
		DBContext dbContext = validator.getDbContext();
		DBUtil util = dbContext.createUtilInstance();
		try {
			if (!validator.isEmpty(inputDTO.get("FROM_DATE")) && !validator.isEmpty(inputDTO.get("UPTO_DATE"))) {
				StringBuffer sqlQuery = new StringBuffer();
				sqlQuery.append("SELECT COUNT(1) NO_OF_INVOICES FROM INVOICES I ");
				sqlQuery.append("JOIN INVTEMPLATECODE IT ON(IT.ENTITY_CODE=I.ENTITY_CODE AND IT.INVTEMPLATE_CODE=I.INVOICE_TEMPLATE AND ELECTRONIC_COPY_ONLY='0')");
				sqlQuery.append("WHERE I.ENTITY_CODE=? AND I.INV_DATE BETWEEN ? AND ? AND I.LEASE_PRODUCT_CODE=? ");
				if(!validator.isEmpty(inputDTO.get("CUSTOMER_ID")))
					sqlQuery.append(" AND I.CUSTOMER_ID=?");
				util.reset();
				util.setSql(sqlQuery.toString());
				util.setLong(1, Long.parseLong(context.getEntityCode()));
				util.setDate(2, new java.sql.Date(BackOfficeFormatUtils.getDate(inputDTO.get("FROM_DATE"), context.getDateFormat()).getTime()));
				util.setDate(3, new java.sql.Date(BackOfficeFormatUtils.getDate(inputDTO.get("UPTO_DATE"), context.getDateFormat()).getTime()));
				util.setString(4, inputDTO.get("LEASE_PRODUCT_CODE"));
				if(!validator.isEmpty(inputDTO.get("CUSTOMER_ID")))
					util.setString(5, inputDTO.get("CUSTOMER_ID"));
				ResultSet rset = util.executeQuery();
				if (rset.next()) {
					resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
					resultDTO.set("NO_OF_INVOICES", rset.getString("NO_OF_INVOICES"));
				}
			}
			return resultDTO;
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			dbContext.close();
			validator.close();
		}
		return resultDTO;
	}

	public DTObject doSubmit(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		CommonValidator validator = new CommonValidator();
		DBContext dbContext = validator.getDbContext();
		try {
			setBillingCycle(inputDTO.get("INV_CYCLE_NUMBER"));
			setMonth(inputDTO.get("MONTH"));
			setMonthYear(inputDTO.get("YEAR"));
			setProductCode(inputDTO.get("LEASE_PRODUCT_CODE"));
			setCustomerId(inputDTO.get("CUSTOMER_ID"));
			setFromDate(inputDTO.get("FROM_DATE"));
			setUptoDate(inputDTO.get("UPTO_DATE"));
			DTObject validateDTO = validate(inputDTO);
			if (validateDTO.get(ContentManager.ERROR) != null) {
				return validateDTO;
			}
			if (!validator.isEmpty(inputDTO.get("FROM_DATE")) && !validator.isEmpty(inputDTO.get("UPTO_DATE"))) {
				List<DTObject> generateReportList = generateReports(inputDTO);
				if (generateReportList == null) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
					return resultDTO;
				}
				rinvoicebean rinvoice = new rinvoicebean();
				for (int i = 0; i < generateReportList.size(); i++) {
					resultDTO = rinvoice.generateInvoiceReport(generateReportList.get(i));
				}
			}
			return resultDTO;
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			dbContext.close();
			validator.close();
		}
		return resultDTO;
	}

	public List<DTObject> generateReports(DTObject inputDTO) {
		DTObject input = new DTObject();
		CommonValidator validator = new CommonValidator();
		DBContext dbContext = validator.getDbContext();
		DBUtil util = dbContext.createUtilInstance();
		List<DTObject> generateReportObj = new ArrayList<DTObject>();
		try {
			if (!validator.isEmpty(inputDTO.get("FROM_DATE")) && !validator.isEmpty(inputDTO.get("UPTO_DATE"))) {
				StringBuffer sqlQuery = new StringBuffer();
				sqlQuery.append("SELECT I.LEASE_PRODUCT_CODE,I.INV_YEAR,I.INV_MONTH,I.INV_SERIAL,I.TOTAL_INV_AMT,I.INV_CCY,C.SUB_CCY_UNITS_IN_CCY");
				sqlQuery.append(" FROM INVOICES I ");
				sqlQuery.append(" JOIN INVTEMPLATECODE IT ON(IT.ENTITY_CODE=I.ENTITY_CODE AND IT.INVTEMPLATE_CODE=I.INVOICE_TEMPLATE AND ELECTRONIC_COPY_ONLY='0')");
				sqlQuery.append(" JOIN CURRENCY C ON (C.CCY_CODE=I.INV_CCY)");
				sqlQuery.append(" WHERE I.ENTITY_CODE=? AND I.INV_DATE BETWEEN ? AND ? AND I.LEASE_PRODUCT_CODE=? ");
				if(!validator.isEmpty(inputDTO.get("CUSTOMER_ID")))
					sqlQuery.append(" AND I.CUSTOMER_ID=?");
				sqlQuery.append(" AND I.IS_PDF_GENERATED='0'");
				util.reset();
				util.setSql(sqlQuery.toString());
				util.setLong(1, Long.parseLong(context.getEntityCode()));
				util.setDate(2, new java.sql.Date(BackOfficeFormatUtils.getDate(inputDTO.get("FROM_DATE"), context.getDateFormat()).getTime()));
				util.setDate(3, new java.sql.Date(BackOfficeFormatUtils.getDate(inputDTO.get("UPTO_DATE"), context.getDateFormat()).getTime()));
				util.setString(4, inputDTO.get("LEASE_PRODUCT_CODE"));
				if(!validator.isEmpty(inputDTO.get("CUSTOMER_ID")))
					util.setString(5, inputDTO.get("CUSTOMER_ID"));
				ResultSet rset = util.executeQuery();
				while (rset.next()) {
					input = new DTObject();
					input.set("ENTITY_CODE", context.getEntityCode());
					input.set("LEASE_PROD_CODE", rset.getString("LEASE_PRODUCT_CODE"));
					input.set("INV_YEAR", rset.getString("INV_YEAR"));
					input.set("INV_MONTH", rset.getString("INV_MONTH"));
					input.set("INV_SERIAL", rset.getString("INV_SERIAL"));
					input.set("TOTAL_INV_AMT", rset.getString("TOTAL_INV_AMT"));
					input.set("INV_CCY", rset.getString("INV_CCY"));
					input.set("INV_CCY_UNITS", rset.getString("SUB_CCY_UNITS_IN_CCY"));
					input.set("REPORT_TYPE", "RINVOICE");
					generateReportObj.add(input);
				}
			}
			return generateReportObj;
		} catch (Exception e) {
			e.printStackTrace();
			generateReportObj = null;
		} finally {
			validator.close();
		}
		return generateReportObj;
	}

	public DTObject validate(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();

		if (!validateBillingCycle()) {
			resultDTO.set(ContentManager.ERROR, getErrorMap().getErrorKey("billingCycle"));
			resultDTO.set(ContentManager.ERROR_FIELD, "billingCycle");
			return resultDTO;
		}
		if (!validateMonth()) {
			resultDTO.set(ContentManager.ERROR, getErrorMap().getErrorKey("month"));
			resultDTO.set(ContentManager.ERROR_FIELD, "month");
			return resultDTO;
		}
		if (!validateMonthYear()) {
			resultDTO.set(ContentManager.ERROR, getErrorMap().getErrorKey("monthYear"));
			resultDTO.set(ContentManager.ERROR_FIELD, "month");
			resultDTO.setObject(ContentManager.ERROR_PARAM,  getErrorMap().getErrorParam("monthYear"));
			return resultDTO;
		}
		if (!validateProductCode()) {
			resultDTO.set(ContentManager.ERROR, getErrorMap().getErrorKey("productCode"));
			resultDTO.set(ContentManager.ERROR_FIELD, "productCode");
			return resultDTO;
		}
		if (!validateCustomerId()) {
			resultDTO.set(ContentManager.ERROR, getErrorMap().getErrorKey("customerId"));
			resultDTO.set(ContentManager.ERROR_FIELD, "customerId");
			return resultDTO;
		}
		return resultDTO;
	}

	public boolean validateBillingCycle() {
		CommonValidator validation = new CommonValidator();
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("INV_CYCLE_NUMBER", billingCycle);
			formDTO.set(ContentManager.ACTION, USAGE);
			formDTO = validateBillingCycle(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("billingCycle", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("billingCycle", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateMonth() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(month)) {
				getErrorMap().setError("month", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isCMLOVREC("COMMON", "MONTH", month)) {
				getErrorMap().setError("month", BackOfficeErrorCodes.INVALID_OPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("month", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateMonthYear() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(monthYear)) {
				getErrorMap().setError("monthYear", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidYear(monthYear)) {
				getErrorMap().setError("monthYear", BackOfficeErrorCodes.INVALID_YEAR);
				return false;
			}
			if (Integer.parseInt(monthYear) < 1990) {
				Object[] param={"1990"};
				getErrorMap().setError("monthYear", BackOfficeErrorCodes.TC_YEAR_CANNOT_BE_LT,param);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("monthYear", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateProductCode() {
		CommonValidator validation = new CommonValidator();
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("LEASE_PRODUCT_CODE", productCode);
			formDTO.set("FROM_DATE", fromDate);
			formDTO.set("UPTO_DATE", uptoDate);
			formDTO.set("SERVER_SIDE", RegularConstants.ONE);
			formDTO.set(ContentManager.ACTION, USAGE);
			formDTO = validateLeaseProductCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("productCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("productCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateCustomerId() {
		CommonValidator validation = new CommonValidator();
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("CUSTOMER_ID", customerId);
			formDTO.set("FROM_DATE", fromDate);
			formDTO.set("UPTO_DATE", uptoDate);
			formDTO.set("LEASE_PRODUCT_CODE", productCode);
			formDTO.set("SERVER_SIDE", RegularConstants.ONE);
			formDTO.set(ContentManager.ACTION, USAGE);
			formDTO = validateCustomerId(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("customerId", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("customerId", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}
}
