package patterns.config.web.forms.inv;

import java.sql.ResultSet;
import java.util.ArrayList;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.framework.web.GenericOption;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.framework.web.reports.ReportUtils;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.InvoiceValidator;
import patterns.config.web.forms.GenericFormBean;

/**
 * 
 * 
 * @version 1.0
 * @author Dileep Kumar Reddy T
 * @since 21-03-2017 <br>
 *        <b>Modified History</b> <BR>
 *        <U><B> Sl.No Modified Date Author Modified Changes Version </B></U>
 *        <br>
 * 
 * 
 * 
 * 
 */
public class rinvoiceenvprintbean extends GenericFormBean {

	private static final long serialVersionUID = 1L;

	private String invoiceCycleNo;
	private String invMonth;
	private String invMonthYear;
	private String carrierName;
	private String senderInfo;

	public String getInvoiceCycleNo() {
		return invoiceCycleNo;
	}

	public void setInvoiceCycleNo(String invoiceCycleNo) {
		this.invoiceCycleNo = invoiceCycleNo;
	}

	public String getInvMonth() {
		return invMonth;
	}

	public void setInvMonth(String invMonth) {
		this.invMonth = invMonth;
	}

	public String getInvMonthYear() {
		return invMonthYear;
	}

	public void setInvMonthYear(String invMonthYear) {
		this.invMonthYear = invMonthYear;
	}

	public String getCarrierName() {
		return carrierName;
	}

	public void setCarrierName(String carrierName) {
		this.carrierName = carrierName;
	}

	public String getSenderInfo() {
		return senderInfo;
	}

	public void setSenderInfo(String senderInfo) {
		this.senderInfo = senderInfo;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void reset() {
		invoiceCycleNo = RegularConstants.EMPTY_STRING;
		invMonth = RegularConstants.EMPTY_STRING;
		invMonthYear = RegularConstants.EMPTY_STRING;
		carrierName = RegularConstants.EMPTY_STRING;
		senderInfo = RegularConstants.EMPTY_STRING;
		DBContext dbContext = new DBContext();
		ArrayList<GenericOption> month = null;
		month = getGenericOptions("COMMON", "MONTH", dbContext, "--");
		webContext.getRequest().setAttribute("COMMON_MONTH", month);
		dbContext.close();
	}

	public void validate() {

	}

	private boolean validateCarrierName() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (!commonValidator.isEmpty(carrierName)) {
				if (!commonValidator.isValidOtherInformation25(carrierName)) {
					getErrorMap().setError("carrierName", BackOfficeErrorCodes.INVALID_VALUE);
					return false;
				}
				return true;
			}

		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("carrierName", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}

	private boolean validateSenderInfo() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (!commonValidator.isEmpty(senderInfo)) {
				if (!commonValidator.isValidOtherInformation100(senderInfo)) {
					getErrorMap().setError("senderInfo", BackOfficeErrorCodes.INVALID_VALUE);
					return false;
				}
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("senderInfo", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}

	public DTObject validateInvoiceCycleNo(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		InvoiceValidator validation = new InvoiceValidator();
		String invCycleNo = inputDTO.get("INV_CYCLE_NUMBER");
		try {
			if (validation.isEmpty(invCycleNo)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "INV_RENTAL_DAY_FROM,INV_RENTAL_DAY_UPTO");
			resultDTO = validation.validateInvoiceCycleNumber(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	private boolean validateMonth() {
		CommonValidator validation = new CommonValidator();
		try {
			if (!validation.isEmpty(invMonth)) {
				if (!validation.isCMLOVREC("COMMON", "MONTH", invMonth)) {
					getErrorMap().setError("staffGridInfo", BackOfficeErrorCodes.INVALID_OPTION);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("staffGridInfo", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	private boolean validateMonthYear() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(invMonthYear)) {
				getErrorMap().setError("invMonth", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidYear(invMonthYear)) {
				getErrorMap().setError("invMonth", BackOfficeErrorCodes.INVALID_YEAR);
				return false;
			}
			if (validation.isNumberLesser(invMonthYear, "1990")) {
				getErrorMap().setError("invMonth", BackOfficeErrorCodes.YEAR_CANT_LE_1990);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("invMonth", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public DTObject getEnvolopeCount(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		CommonValidator validator = new CommonValidator();
		StringBuffer sqlQuery = new StringBuffer();
		String invoiceCycleNo = inputDTO.get("INV_CYCLE_NUMBER");
		DBUtil util = validator.getDbContext().createUtilInstance();
		String invoiceFromDay = inputDTO.get("INV_FROM_DAY");
		String invoiceUptoDay = inputDTO.get("INV_UPTO_DAY");
		String invoiceMonth = inputDTO.get("INV_MONTH");
		if (invoiceFromDay.length() == 1)
			invoiceFromDay = "0" + invoiceFromDay;
		if (invoiceUptoDay.length() == 1)
			invoiceUptoDay = "0" + invoiceUptoDay;
		if (invoiceMonth.length() == 1)
			invoiceMonth = "0" + invoiceMonth;
		String invoiceFromDate = invoiceFromDay + "-" + invoiceMonth + "-" + inputDTO.get("INV_MONTH_YEAR");
		String invoiceUptoDate = invoiceUptoDay + "-" + invoiceMonth + "-" + inputDTO.get("INV_MONTH_YEAR");
		try {
			resultDTO = validateInputFields(inputDTO);
			if (RegularConstants.ONE.equals(resultDTO.get("ERROR_EXISTS"))) {
				return resultDTO;
			}
			sqlQuery.append("SELECT COUNT(DISTINCT INVOICES.CUSTOMER_ID,INVOICES.BILLING_ADDR_SL) ");
			sqlQuery.append("FROM INVOICES INVOICES ");
			sqlQuery.append(
					"JOIN INVTEMPLATECODE INVTEMPLATECODE ON(INVTEMPLATECODE.ENTITY_CODE=INVOICES.ENTITY_CODE AND INVTEMPLATECODE.INVTEMPLATE_CODE=INVOICES.INVOICE_TEMPLATE AND INVTEMPLATECODE.ELECTRONIC_COPY_ONLY='0')");
			sqlQuery.append(
					"JOIN CUSTOMER CUSTOMER ON (CUSTOMER.ENTITY_CODE=INVOICES.ENTITY_CODE AND CUSTOMER.CUSTOMER_ID=INVOICES.CUSTOMER_ID) ");
			sqlQuery.append(
					"JOIN CUSTOMERADDRESS CUSTOMERADDRESS ON(CUSTOMERADDRESS.ENTITY_CODE=CUSTOMER.ENTITY_CODE AND CUSTOMERADDRESS.CUSTOMER_ID=CUSTOMER.CUSTOMER_ID AND CUSTOMERADDRESS.ADDR_SL=INVOICES.BILLING_ADDR_SL) ");
			sqlQuery.append(
					"JOIN STATE STATE ON(STATE.ENTITY_CODE=CUSTOMERADDRESS.ENTITY_CODE AND STATE.STATE_CODE=CUSTOMERADDRESS.STATE_CODE) ");
			sqlQuery.append(
					"WHERE INVOICES.ENTITY_CODE=? AND INVOICES.INV_FOR_CYCLE_NUMBER=? AND INVOICES.INV_DATE BETWEEN ? AND ?");
			util.reset();
			util.setSql(sqlQuery.toString());
			util.setLong(1, Long.parseLong(context.getEntityCode()));
			util.setInt(2, Integer.parseInt(invoiceCycleNo));
			util.setDate(3, new java.sql.Date(
					BackOfficeFormatUtils.getDate(invoiceFromDate, BackOfficeConstants.JAVA_DATE_FORMAT).getTime()));
			util.setDate(4, new java.sql.Date(
					BackOfficeFormatUtils.getDate(invoiceUptoDate, BackOfficeConstants.JAVA_DATE_FORMAT).getTime()));
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				resultDTO.set("ENVOLOPE_COUNT", rset.getString(1));
			}
			resultDTO.set("INV_FROM_DATE", invoiceFromDate);
			resultDTO.set("INV_UPTO_DATE", invoiceUptoDate);
			resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return resultDTO;
	}

	public DTObject validateInputFields(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set("ERROR_EXISTS", RegularConstants.ZERO);
		try {
			resultDTO = validateInvoiceCycleNo(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set("ERROR_INV_CYCLE", getErrorResource(resultDTO.get(ContentManager.ERROR), null));
				resultDTO.set("ERROR_EXISTS", RegularConstants.ONE);
				resultDTO.set(ContentManager.ERROR, RegularConstants.NULL);
				return resultDTO;
			}
			setInvMonth(inputDTO.get("INV_MONTH"));
			if (!validateMonth()) {
				resultDTO.set("ERROR_INV_MONTH", getErrorResource(getErrorMap().getErrorKey("invMonth"),
						getErrorMap().getErrorParam("invMonth")));
				resultDTO.set("ERROR_EXISTS", RegularConstants.ONE);
				return resultDTO;
			}
			setInvMonthYear(inputDTO.get("INV_MONTH_YEAR"));
			if (!validateMonthYear()) {
				resultDTO.set("ERROR_INV_YEAR", getErrorResource(getErrorMap().getErrorKey("invMonth"),
						getErrorMap().getErrorParam("invMonthYear")));
				resultDTO.set("ERROR_EXISTS", RegularConstants.ONE);
				return resultDTO;
			}
			if (inputDTO.get("VALIDATE_COURIER_DETAILS") != RegularConstants.NULL) {
				setCarrierName(inputDTO.get("CARRIER_NAME"));
				if (!validateCarrierName()) {
					resultDTO.set("ERROR_CARRIER_NAME", getErrorResource(getErrorMap().getErrorKey("carrierName"),
							getErrorMap().getErrorParam("carrierName")));
					resultDTO.set("ERROR_EXISTS", RegularConstants.ONE);
					return resultDTO;
				}
				setSenderInfo(inputDTO.get("SENDER_INFO"));
				if (!validateSenderInfo()) {
					resultDTO.set("ERROR_SENDER_INFO", getErrorResource(getErrorMap().getErrorKey("senderInfo"),
							getErrorMap().getErrorParam("senderInfo")));
					resultDTO.set("ERROR_EXISTS", RegularConstants.ONE);
					return resultDTO;
				}
			}
		} catch (Exception e) {

		}
		return resultDTO;
	}

	public DTObject printInvoiceEvnvolopes(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		CommonValidator validation = new CommonValidator();
		resultDTO.set(ContentManager.ERROR, RegularConstants.NULL);
		try {
			resultDTO = validateInputFields(inputDTO);
			if (RegularConstants.ONE.equals(resultDTO.get("ERROR_EXISTS"))) {
				return resultDTO;
			}
			if (validation.isEmpty(inputDTO.get("INV_FROM_DATE"))
					|| validation.isEmpty(inputDTO.get("INV_UPTO_DATE"))) {
				resultDTO.set("ERROR_INV_DATE_RANGE", BackOfficeErrorCodes.INVOICE_DATES_MANDATORY);
				return resultDTO;
			}
			inputDTO.set("ENTITY_CODE", context.getEntityCode());
			inputDTO.set(ContentManager.REPORT_HANDLER, "patterns.config.web.reports.inv.rinvoiceenvprintreport");
			if (RegularConstants.EMPTY_STRING.equals(inputDTO.get("REPORT_FORMAT"))
					|| inputDTO.get("REPORT_FORMAT") == null)
				inputDTO.set(ContentManager.REPORT_FORMAT, ReportUtils.EXPORT_PDF + "");
			else
				inputDTO.set(ContentManager.REPORT_FORMAT, inputDTO.get("REPORT_FORMAT"));
			resultDTO = processDownload(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null
					&& (!resultDTO.get(ContentManager.ERROR).equals(RegularConstants.EMPTY_STRING))) {
				return resultDTO;
			}
			return resultDTO;
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}
}
