package patterns.config.web.forms.inv;

import java.sql.ResultSet;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.framework.web.DHTMLXGridUtility;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.MasterValidator;
import patterns.config.web.forms.GenericFormBean;

public class qinvcompviewbean extends GenericFormBean {

	private static final long serialVersionUID = 1L;
	private String invoiceDate;
	private String lesseeCode;
	private String customerId;
	private String agreementSerial;
	private String scheduleId;
	private String customerName;
	private String productCode;
	private String billingAddressSerial;
	private String lessorBranchCode;
	private String lessorStateCode;
	private String invoiceType;
	private String componentAmount;
	private String createdBy;
	private String createdOn;

	public String getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(String invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public String getLesseeCode() {
		return lesseeCode;
	}

	public void setLesseeCode(String lesseeCode) {
		this.lesseeCode = lesseeCode;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getAgreementSerial() {
		return agreementSerial;
	}

	public void setAgreementSerial(String agreementSerial) {
		this.agreementSerial = agreementSerial;
	}

	public String getScheduleId() {
		return scheduleId;
	}

	public void setScheduleId(String scheduleId) {
		this.scheduleId = scheduleId;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getBillingAddressSerial() {
		return billingAddressSerial;
	}

	public void setBillingAddressSerial(String billingAddressSerial) {
		this.billingAddressSerial = billingAddressSerial;
	}

	public String getLessorBranchCode() {
		return lessorBranchCode;
	}

	public void setLessorBranchCode(String lessorBranchCode) {
		this.lessorBranchCode = lessorBranchCode;
	}

	public String getLessorStateCode() {
		return lessorStateCode;
	}

	public void setLessorStateCode(String lessorStateCode) {
		this.lessorStateCode = lessorStateCode;
	}

	public String getInvoiceType() {
		return invoiceType;
	}

	public void setInvoiceType(String invoiceType) {
		this.invoiceType = invoiceType;
	}

	public String getComponentAmount() {
		return componentAmount;
	}

	public void setComponentAmount(String componentAmount) {
		this.componentAmount = componentAmount;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	@Override
	public void reset() {

		invoiceDate = RegularConstants.EMPTY_STRING;
		lesseeCode = RegularConstants.EMPTY_STRING;
		customerId = RegularConstants.EMPTY_STRING;
		agreementSerial = RegularConstants.EMPTY_STRING;
		scheduleId = RegularConstants.EMPTY_STRING;
		customerName = RegularConstants.EMPTY_STRING;
		productCode = RegularConstants.EMPTY_STRING;
		billingAddressSerial = RegularConstants.EMPTY_STRING;
		lessorBranchCode = RegularConstants.EMPTY_STRING;
		lessorStateCode = RegularConstants.EMPTY_STRING;
		invoiceType = RegularConstants.EMPTY_STRING;
		componentAmount = RegularConstants.EMPTY_STRING;
		createdBy = RegularConstants.EMPTY_STRING;
		createdOn = RegularConstants.EMPTY_STRING;
	}

	public DTObject validateLoadGrid(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		MasterValidator validation = new MasterValidator();
		try {
			DBUtil dbutil = validation.getDbContext().createUtilInstance();
			int recordCount = 0;
			StringBuffer sqlQuery = new StringBuffer();
			sqlQuery.append("SELECT B.DTL_SL,CM.LOV_LABEL_EN_US,TO_CHAR(B.START_DATE,?) AS START_DATE,	TO_CHAR(B.END_DATE,?) AS END_DATE,");
			sqlQuery.append("FN_FORMAT_AMOUNT(B.PRINCIPAL_AMOUNT,B.CCY,C.SUB_CCY_UNITS_IN_CCY) PRINCIPAL_AMOUNT,FN_FORMAT_AMOUNT(B.INTEREST_AMOUNT,B.CCY,C.SUB_CCY_UNITS_IN_CCY) INTEREST_AMOUNT,");
			sqlQuery.append("FN_FORMAT_AMOUNT(B.RENTAL_AMOUNT,B.CCY,C.SUB_CCY_UNITS_IN_CCY) RENTAL_AMOUNT,FN_FORMAT_AMOUNT(B.EXECUTORY_AMOUNT,B.CCY,C.SUB_CCY_UNITS_IN_CCY) EXECUTORY_AMOUNT,");
			sqlQuery.append("FN_FORMAT_AMOUNT(B.TAX_AMT,B.CCY,C.SUB_CCY_UNITS_IN_CCY) TAX_AMT ");
			sqlQuery.append(" FROM INVCOMPDTL B ");
			sqlQuery.append("JOIN CURRENCY C ON(C.CCY_CODE=B.CCY) JOIN CMLOVREC CM ON (PGM_ID='COMMON' AND LOV_ID = 'INVOICE_FOR' AND LOV_VALUE= B.FINANCE_SCHED_INV_FOR)");
			sqlQuery.append("WHERE B.ENTITY_CODE=? AND B.ENTRY_DATE=? AND B.ENTRY_SL=?");
			dbutil.reset();
			dbutil.setMode(DBUtil.PREPARED);
			dbutil.setSql(sqlQuery.toString());

			dbutil.setString(1, context.getDateFormat());
			dbutil.setString(2, context.getDateFormat());
			dbutil.setLong(3, Long.parseLong(context.getEntityCode()));
			dbutil.setDate(4, new java.sql.Date(BackOfficeFormatUtils.getDate(inputDTO.get("ENTRY_DATE"), context.getDateFormat()).getTime()));
			dbutil.setInt(5, Integer.parseInt(inputDTO.get("ENTRY_SL")));
			ResultSet rsetGrid = dbutil.executeQuery();
			DHTMLXGridUtility gridUtility = new DHTMLXGridUtility();
			gridUtility.init();
			// StringBuffer sb = new StringBuffer();
			while (rsetGrid.next()) {
				recordCount = recordCount + 1;
				gridUtility.startRow(String.valueOf(recordCount));

				gridUtility.setCell(rsetGrid.getString("LOV_LABEL_EN_US"));
				gridUtility.setCell(rsetGrid.getString("START_DATE"));
				gridUtility.setCell(rsetGrid.getString("END_DATE"));
				gridUtility.setCell(rsetGrid.getString("PRINCIPAL_AMOUNT"));
				gridUtility.setCell(rsetGrid.getString("INTEREST_AMOUNT"));
				gridUtility.setCell(rsetGrid.getString("RENTAL_AMOUNT"));
				gridUtility.setCell(rsetGrid.getString("EXECUTORY_AMOUNT"));
				gridUtility.setCell("" + rsetGrid.getString("TAX_AMT") + "^javascript:viewTaxAmtDetails(\"" + rsetGrid.getString("DTL_SL") + "\")");
				gridUtility.endRow();
			}
			if (recordCount != 0) {
				gridUtility.finish();
				String resultXML = gridUtility.getXML();
				resultDTO.set(ContentManager.RESULT_XML, resultXML);
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
			} else {
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
				return resultDTO;
			}
			return resultDTO;
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public DTObject validateTaxGrid(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		MasterValidator validation = new MasterValidator();
		try {
			DBUtil dbutil = validation.getDbContext().createUtilInstance();
			int recordCount = 0;
			StringBuffer sqlQuery = new StringBuffer();
			sqlQuery.append("SELECT B.DTL_SL,B.STATE_CODE,B.TAX_CODE,T.CONCISE_DESCRIPTION,");
			sqlQuery.append("FN_FORMAT_AMOUNT(B.TAX_RATE,B.TAX_CCY,C.SUB_CCY_UNITS_IN_CCY) TAX_RATE,");
			sqlQuery.append("FN_FORMAT_AMOUNT(B.TAX_ON_AMT_PORTION,B.TAX_CCY,C.SUB_CCY_UNITS_IN_CCY) TAX_ON_AMT_PORTION,FN_FORMAT_AMOUNT(B.TAX_ON_AMT,B.TAX_CCY,C.SUB_CCY_UNITS_IN_CCY) TAX_ON_AMT,");
			sqlQuery.append("FN_FORMAT_AMOUNT(B.TAX_AMT,B.TAX_CCY,C.SUB_CCY_UNITS_IN_CCY) TAX_AMT ");
			sqlQuery.append(" FROM INVCOMPTAXDTL B ");
			sqlQuery.append("LEFT OUTER JOIN TAX T ON (B.ENTITY_CODE=T.ENTITY_CODE AND B.STATE_CODE=T.STATE_CODE AND B.TAX_CODE=T.TAX_CODE)  ");
			sqlQuery.append("LEFT OUTER JOIN CURRENCY C ON(C.CCY_CODE=B.TAX_CCY)");
			sqlQuery.append("WHERE  B.ENTITY_CODE=? AND B.ENTRY_DATE=? AND B.ENTRY_SL=? AND B.DTL_SL=?");
			dbutil.reset();
			dbutil.setMode(DBUtil.PREPARED);
			dbutil.setSql(sqlQuery.toString());

			dbutil.setLong(1, Long.parseLong(context.getEntityCode()));
			dbutil.setDate(2, new java.sql.Date(BackOfficeFormatUtils.getDate(inputDTO.get("ENTRY_DATE"), context.getDateFormat()).getTime()));
			dbutil.setInt(3, Integer.parseInt(inputDTO.get("ENTRY_SL")));
			dbutil.setInt(4, Integer.parseInt(inputDTO.get("DTL_SL")));
			ResultSet rsetGrid = dbutil.executeQuery();
			DHTMLXGridUtility gridUtility = new DHTMLXGridUtility();
			gridUtility.init();
			// StringBuffer sb = new StringBuffer();
			while (rsetGrid.next()) {
				recordCount = recordCount + 1;
				gridUtility.startRow(String.valueOf(recordCount));

				gridUtility.setCell(rsetGrid.getString("STATE_CODE"));
				gridUtility.setCell(rsetGrid.getString("TAX_CODE"));
				gridUtility.setCell(rsetGrid.getString("CONCISE_DESCRIPTION"));
				gridUtility.setCell(rsetGrid.getString("TAX_RATE"));
				gridUtility.setCell(rsetGrid.getString("TAX_ON_AMT_PORTION"));
				gridUtility.setCell(rsetGrid.getString("TAX_ON_AMT"));
				gridUtility.setCell(rsetGrid.getString("TAX_AMT"));
				gridUtility.endRow();
			}
			if (recordCount != 0) {
				gridUtility.finish();
				String resultXML = gridUtility.getXML();
				resultDTO.set(ContentManager.RESULT_XML, resultXML);
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
			} else {
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
				return resultDTO;
			}
			return resultDTO;
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	@Override
	public void validate() {
		// TODO Auto-generated method stub

	}
}
