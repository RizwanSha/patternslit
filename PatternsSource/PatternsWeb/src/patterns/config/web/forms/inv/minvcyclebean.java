package patterns.config.web.forms.inv;

/*
 *
 Author : Javeed S
 Created Date : 25-JAN-2016
 Spec Reference : PPBS/INV/0017-MINVCYCLE - Invoice Cycles Maintenance
 Modification History
 ----------------------------------------------------------------------
 Sl.No		Modified Date	Author			Modified Changes	Version
 ----------------------------------------------------------------------

 */
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.InvoiceValidator;
import patterns.config.web.forms.GenericFormBean;

public class minvcyclebean extends GenericFormBean {

	private static final long serialVersionUID = -6219657363150466160L;

	private String invoiceCycleNumber;
	private String invoicesForRentalDateDaysFrom;
	private String upto;
	private String invoiceGenerationInAdvanceBy;
	private String remarks;

	public String getInvoiceCycleNumber() {
		return invoiceCycleNumber;
	}

	public void setInvoiceCycleNumber(String invoiceCycleNumber) {
		this.invoiceCycleNumber = invoiceCycleNumber;
	}

	public String getInvoicesForRentalDateDaysFrom() {
		return invoicesForRentalDateDaysFrom;
	}

	public void setInvoicesForRentalDateDaysFrom(String invoicesForRentalDateDaysFrom) {
		this.invoicesForRentalDateDaysFrom = invoicesForRentalDateDaysFrom;
	}

	public String getUpto() {
		return upto;
	}

	public void setUpto(String upto) {
		this.upto = upto;
	}

	public String getInvoiceGenerationInAdvanceBy() {
		return invoiceGenerationInAdvanceBy;
	}

	public void setInvoiceGenerationInAdvanceBy(String invoiceGenerationInAdvanceBy) {
		this.invoiceGenerationInAdvanceBy = invoiceGenerationInAdvanceBy;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void reset() {
		invoiceCycleNumber = RegularConstants.EMPTY_STRING;
		invoicesForRentalDateDaysFrom = RegularConstants.EMPTY_STRING;
		upto = RegularConstants.EMPTY_STRING;
		invoiceGenerationInAdvanceBy = RegularConstants.EMPTY_STRING;
		remarks = RegularConstants.EMPTY_STRING;
		setProcessBO("patterns.config.framework.bo.inv.minvcycleBO");

	}

	@Override
	public void validate() {
		if (validateInvoiceCycleNumber()) {
			validateInvoicesForRentalDateDaysFrom();
			validateUpto();
			validateInvoiceGenerationInAdvanceBy();
			validateRemarks();
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.set("INV_CYCLE_NUMBER", invoiceCycleNumber);
				formDTO.set("INV_RENTAL_DAY_FROM", invoicesForRentalDateDaysFrom);
				formDTO.set("INV_RENTAL_DAY_UPTO", upto);
				formDTO.set("INV_GEN_ADV_DAYS", invoiceGenerationInAdvanceBy);
				formDTO.set("REMARKS", remarks);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("interestRateTypeCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);

		}
	}

	public boolean validateInvoiceCycleNumber() {
		try {
			DTObject formDTO = getFormDTO();
			formDTO.set("INV_CYCLE_NUMBER", invoiceCycleNumber);
			formDTO.set(ContentManager.ACTION, getAction());
			formDTO = validateInvoiceCycleNumber(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("invoiceCycleNumber", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("invoiceCycleNumber", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateInvoiceCycleNumber(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		InvoiceValidator validation = new InvoiceValidator();
		String invoiceCycleNumber = inputDTO.get("INV_CYCLE_NUMBER");
		String action = inputDTO.get(ContentManager.ACTION);
		try {
			if (validation.isEmpty(invoiceCycleNumber)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			resultDTO = validation.validateInvoiceCycleNumber(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (action.equals(ADD)) {
				if (ContentManager.DATA_AVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_EXISTS);
				}
			} else {
				if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_NOT_EXISTS);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateInvoicesForRentalDateDaysFrom() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(invoicesForRentalDateDaysFrom)) {
				getErrorMap().setError("invoicesForRentalDateDaysFrom", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidNumber(invoicesForRentalDateDaysFrom)) {
				getErrorMap().setError("invoicesForRentalDateDaysFrom", BackOfficeErrorCodes.NUMERIC_CHECK);
				return false;
			}
			if (validation.isZero(invoicesForRentalDateDaysFrom)) {
				getErrorMap().setError("invoicesForRentalDateDaysFrom", BackOfficeErrorCodes.ZERO_CHECK);
				return false;
			}
			if (validation.isValidNegativeNumber(invoicesForRentalDateDaysFrom)) {
				getErrorMap().setError("invoicesForRentalDateDaysFrom", BackOfficeErrorCodes.POSITIVE_CHECK);
				return false;
			}
			if (validation.isNumberGreater(invoicesForRentalDateDaysFrom, "31")) {
				getErrorMap().setError("invoicesForRentalDateDaysFrom", BackOfficeErrorCodes.MAXVAL_31);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("invoicesForRentalDateDaysFrom", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateUpto() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(upto)) {
				getErrorMap().setError("upto", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidNumber(upto)) {
				getErrorMap().setError("upto", BackOfficeErrorCodes.NUMERIC_CHECK);
				return false;
			}
			if (validation.isZero(upto)) {
				getErrorMap().setError("upto", BackOfficeErrorCodes.ZERO_CHECK);
				return false;
			}
			if (validation.isValidNegativeNumber(upto)) {
				getErrorMap().setError("upto", BackOfficeErrorCodes.POSITIVE_CHECK);
				return false;
			}
			if (validation.isNumberGreater(upto, "31")) {
				getErrorMap().setError("upto", BackOfficeErrorCodes.MAXVAL_31);
				return false;
			}
			if (validation.isNumberLesserThanEqual(upto, invoicesForRentalDateDaysFrom)) {
				getErrorMap().setError("upto", BackOfficeErrorCodes.TC_RENTALDAYS_SHUDNOT_LESS_N_EQUAL);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("upto", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateInvoiceGenerationInAdvanceBy() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(invoiceGenerationInAdvanceBy)) {
				getErrorMap().setError("invoiceGenerationInAdvanceBy", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidNumber(invoiceGenerationInAdvanceBy)) {
				getErrorMap().setError("invoiceGenerationInAdvanceBy", BackOfficeErrorCodes.NUMERIC_CHECK);
				return false;
			}
			if (validation.isZero(invoiceGenerationInAdvanceBy)) {
				getErrorMap().setError("upto", BackOfficeErrorCodes.ZERO_CHECK);
				return false;
			}
			if (validation.isValidNegativeNumber(invoiceGenerationInAdvanceBy)) {
				getErrorMap().setError("invoiceGenerationInAdvanceBy", BackOfficeErrorCodes.POSITIVE_CHECK);
				return false;
			}
			if (validation.isNumberGreater(invoiceGenerationInAdvanceBy, "30")) {
				getErrorMap().setError("upto", BackOfficeErrorCodes.ZERO_CHECK);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("description", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateRemarks() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (remarks != null && remarks.length() > 0) {
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			if (getAction().equals(MODIFY)) {
				if (commonValidator.isEmpty(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}

}
