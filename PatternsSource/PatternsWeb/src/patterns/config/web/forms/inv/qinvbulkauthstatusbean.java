package patterns.config.web.forms.inv;

import java.sql.ResultSet;
import java.util.ArrayList;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.CM_LOVREC;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.framework.web.DHTMLXGridUtility;
import patterns.config.framework.web.GenericOption;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.MasterValidator;
/*
 * The program will be used to View  Invoice Bulk Authorization status 
 *
 Author : Pavan kumar.R
 Created Date : 25-March-2017
 Spec Reference PSD-INV-
 Modification History
 -----------------------------------------------------------------------------------------------------
 Sl.No		Modified Date	Author			Modified Changes					Version
 -----------------------------------------------------------------------------------------------------

 */
import patterns.config.web.forms.GenericFormBean;

public class qinvbulkauthstatusbean extends GenericFormBean {

	private static final long serialVersionUID = -6219657363150466160L;
	private String dueOnDate;
	private String processStatus;

	public String getDueOnDate() {
		return dueOnDate;
	}

	public void setDueOnDate(String dueOnDate) {
		this.dueOnDate = dueOnDate;
	}

	public String getProcessStatus() {
		return processStatus;
	}

	public void setProcessStatus(String processStatus) {
		this.processStatus = processStatus;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void validate() {
	}

	@Override
	public void reset() {
		dueOnDate = RegularConstants.EMPTY_STRING;
		processStatus = RegularConstants.EMPTY_STRING;
		DBContext dbContext = new DBContext();
		ArrayList<GenericOption> processStatusArrayList = null;
		processStatusArrayList = getGenericOptions("QINVBULKAUTHSTATUS", "INV_PROCESS_STATUS", dbContext, null);
		webContext.getRequest().setAttribute("INV_PROCESS_STATUS", processStatusArrayList);
		dbContext.close();
	}

	public DTObject validateProcessStatus(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		MasterValidator validator = new MasterValidator();
		try {
			if (validator.isEmpty(processStatus)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			} else if (!validator.isCMLOVREC("QINVBULKAUTHSTATUS", "INV_PROCESS_STATUS", processStatus)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_OPTION);
				return resultDTO;
			}
			return resultDTO;
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return resultDTO;
	}

	public DTObject validateDueDate(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		MasterValidator validator = new MasterValidator();
		try {
			if (validator.isEmpty(dueOnDate)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			if (validator.isValidDate(dueOnDate) == null) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_DATE);
				return resultDTO;
			}
			return resultDTO;
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return resultDTO;
	}

	public DTObject validateFields(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		inputDTO.set("ERROR_PRESENT", RegularConstants.ZERO);
		try {
			resultDTO = validateDueDate(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				inputDTO.set("DUE_DATE_ERROR", getErrorResource(resultDTO.get(ContentManager.ERROR), null));
				inputDTO.set("ERROR_PRESENT", RegularConstants.ONE);
			}
			resultDTO = validateProcessStatus(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				inputDTO.set("PROC_STATUS_ERROR", getErrorResource(resultDTO.get(ContentManager.ERROR), null));
				inputDTO.set("ERROR_PRESENT", RegularConstants.ONE);
			}
			return inputDTO;
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
		}
		return inputDTO;
	}

	public DTObject loadGrid(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		DBContext dbContext = new DBContext();
		Long success=0L;
		Long failure=0L;
		Long underprocess=0L;
		Long total=0L;
		Long fetch=0L;
		try {
			setProcessStatus(inputDTO.get("PROC_STATUS"));
			setDueOnDate(inputDTO.get("DUE_DATE"));
			resultDTO = validateFields(inputDTO);
			if (resultDTO.get("ERROR_PRESENT").equals(RegularConstants.ONE)) {
				return resultDTO;
			} else {
				DBUtil util = dbContext.createUtilInstance();
				StringBuffer sqlQuery = new StringBuffer(RegularConstants.EMPTY_STRING);
				util.reset();
				util.setMode(DBUtil.PREPARED);
				sqlQuery.append(" SELECT TO_CHAR(G.ENTRY_DATE,?) AS ENTRY_DATE,G.ENTRY_SL,G.INV_YEAR,CMM.LOV_LABEL_EN_US,G.INVOICE_TEMPLATE,G.INV_CCY,G.TOTAL_INV, ");
				sqlQuery.append(" FN_FORMAT_AMOUNT(G.TOTAL_INV_AMT,G.INV_CCY,CU.SUB_CCY_UNITS_IN_CCY) AS TOTAL_INV_AMT,");
				sqlQuery.append(" CA.SUNDRY_DB_AC, C.CUSTOMER_NAME,IV.PROC_STATUS,CPS.LOV_LABEL_EN_US AS PROC_STATUS_DESC,CACT.LOV_LABEL_EN_US AS PROC_ACTION_TYPE");
				sqlQuery.append(" FROM INVCOMPGROUP G ");
				sqlQuery.append(" INNER JOIN INVAUTHDTL IV ON (IV.ENTITY_CODE=G.ENTITY_CODE AND IV.ENTRY_DATE=G.AUTH_ENTRY_DATE AND ");
				sqlQuery.append(" IV.ENTRY_SL=G.AUTH_ENTRY_SL AND IV.GRP_ENTRY_DATE=G.ENTRY_DATE AND IV.GRP_ENTRY_SL=G.ENTRY_SL)");
				sqlQuery.append(" INNER JOIN CUSTOMER C ON(C.ENTITY_CODE=G.ENTITY_CODE AND C.CUSTOMER_ID=G.CUSTOMER_ID)");
				sqlQuery.append(" INNER JOIN CUSTOMERACDTL CA ON (CA.ENTITY_CODE=G.ENTITY_CODE AND CA.CUSTOMER_ID=G.CUSTOMER_ID AND CA.LEASE_PRODUCT_CODE=G.LEASE_PRODUCT_CODE)");
				sqlQuery.append(" INNER JOIN CURRENCY CU ON (G.INV_CCY=CU.CCY_CODE) ");
				sqlQuery.append(" INNER JOIN CMLOVREC CMM ON (CMM.PGM_ID='COMMON' AND CMM.LOV_ID='MONTH_SHORT' AND CMM.LOV_VALUE=G.INV_MONTH) ");
				sqlQuery.append(" INNER JOIN CMLOVREC CPS ON (CPS.PGM_ID='QINVBULKAUTHSTATUS' AND CPS.LOV_ID='INV_PROCESS_STATUS' AND CPS.LOV_VALUE=IV.PROC_STATUS) ");
				sqlQuery.append(" INNER JOIN CMLOVREC CACT ON (CACT.PGM_ID='COMMON' AND CACT.LOV_ID='AUTHORIZATION_STATUS' AND CACT.LOV_VALUE=G.ACTION_TYPE) ");
				sqlQuery.append(" WHERE G.ENTITY_CODE=? AND G.DUE_DATE=? AND IV.PROC_STATUS IN (?,?,?,?)");
				logger.logDebug("QUERY :"+sqlQuery.toString());
				util.setSql(sqlQuery.toString());
				util.setString(1, context.getDateFormat());
				util.setLong(2, Long.parseLong(context.getEntityCode()));
				util.setDate(3, new java.sql.Date(BackOfficeFormatUtils.getDate(inputDTO.get("DUE_DATE"), context.getDateFormat()).getTime()));
				if (processStatus.equals("A")){
					util.setString(4, CM_LOVREC.QINVBULKAUTHSTATUS_INV_PROCESS_STATUS_U);
					util.setString(5, CM_LOVREC.QINVBULKAUTHSTATUS_INV_PROCESS_STATUS_S);
					util.setString(6, CM_LOVREC.QINVBULKAUTHSTATUS_INV_PROCESS_STATUS_F);
					util.setString(7, CM_LOVREC.QINVBULKAUTHSTATUS_INV_PROCESS_STATUS_T);
				}else{
					util.setString(4, processStatus);
					util.setString(5, processStatus);
					util.setString(6, processStatus);
					util.setString(7, processStatus);
				}
					
				ResultSet rset = util.executeQuery();
				DHTMLXGridUtility gridUtility = new DHTMLXGridUtility();
				gridUtility.init();
				boolean checkRecord = false;
				while (rset.next()) {
					gridUtility.startRow();
					gridUtility.setCell("");
					gridUtility.setCell(rset.getString("LOV_LABEL_EN_US"));
					gridUtility.setCell(rset.getString("INV_YEAR"));
					gridUtility.setCell(rset.getString("SUNDRY_DB_AC"));
					gridUtility.setCell(rset.getString("CUSTOMER_NAME"));
					gridUtility.setCell(rset.getString("INVOICE_TEMPLATE"));
					gridUtility.setCell(rset.getString("INV_CCY"));
					gridUtility.setCell(rset.getString("TOTAL_INV_AMT"));
					gridUtility.setCell(rset.getString("ENTRY_DATE"));
					gridUtility.setCell(rset.getString("ENTRY_SL"));
					gridUtility.setCell(rset.getString("PROC_STATUS_DESC"));
					gridUtility.setCell(rset.getString("PROC_ACTION_TYPE"));
					if(rset.getString("PROC_STATUS").equals(CM_LOVREC.QINVBULKAUTHSTATUS_INV_PROCESS_STATUS_S)){
						success++;
					}else if(rset.getString("PROC_STATUS").equals(CM_LOVREC.QINVBULKAUTHSTATUS_INV_PROCESS_STATUS_U)){
						underprocess++;
					}else if(rset.getString("PROC_STATUS").equals(CM_LOVREC.QINVBULKAUTHSTATUS_INV_PROCESS_STATUS_F)){
						failure++;
					}else if(rset.getString("PROC_STATUS").equals(CM_LOVREC.QINVBULKAUTHSTATUS_INV_PROCESS_STATUS_T)){
						fetch++;
					}
					
					total++;
					gridUtility.endRow();
					checkRecord = true;
				}
				if (!checkRecord) {
					resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
					return resultDTO;
				} else {
					gridUtility.finish();
					resultDTO.set(ContentManager.RESULT_XML, gridUtility.getXML());
					resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
					resultDTO.set("SUCCESS", Long.toString(success));
					resultDTO.set("UNDERPROCESS", Long.toString(underprocess));
					resultDTO.set("FAILURE", Long.toString(failure));
					resultDTO.set("TOTAL", Long.toString(total));
					resultDTO.set("FETCH", Long.toString(fetch));
					return resultDTO;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			dbContext.close();
		}
		return resultDTO;
	}

	public DTObject loadComponentDetails(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		DBContext dbContext = new DBContext();
		try {
			DBUtil util = dbContext.createUtilInstance();
			DHTMLXGridUtility gridUtility = new DHTMLXGridUtility();
			StringBuffer sqlQuery = new StringBuffer(RegularConstants.EMPTY_STRING);
			boolean recordExist = false;
			util.reset();
			util.setMode(DBUtil.PREPARED);
			sqlQuery.append(" SELECT I.ENTITY_CODE,I.LESSEE_CODE,I.AGREEMENT_NO,I.SCHEDULE_ID,CONCAT(I.ENTITY_CODE,'|',I.LESSEE_CODE,'|',I.AGREEMENT_NO,'|',I.SCHEDULE_ID)AS LEASE_PK,");
			sqlQuery.append(" I.LESSOR_BRANCH_CODE,I.LESSOR_STATE_CODE,I.INV_TYPE,CM.LOV_LABEL_EN_US,I.INV_CYCLE_NUMBER,I.INV_CCY,");
			sqlQuery.append(" FN_FORMAT_AMOUNT(I.INV_AMOUNT,I.INV_CCY,C.SUB_CCY_UNITS_IN_CCY)INV_AMOUNT,");
			sqlQuery.append(" FN_FORMAT_AMOUNT(I.TAX_AMT,I.INV_CCY,C.SUB_CCY_UNITS_IN_CCY)TAX_AMT,");
			sqlQuery.append(" CONCAT(TO_CHAR(I.ENTRY_DATE,?),'|',I.ENTRY_SL,'|',I.LESSEE_CODE,'|',I.AGREEMENT_NO,'|',I.SCHEDULE_ID) AS PK ");
			sqlQuery.append(" FROM INVCOMPGROUPDTL DT ");
			sqlQuery.append(" INNER JOIN INVCOMPONENT I ON (I.ENTRY_DATE=DT.COMP_ENTRY_DATE AND I.ENTRY_SL=DT.COMP_ENTRY_SL)");
			sqlQuery.append(" INNER JOIN CMLOVREC CM ON (CM.PGM_ID='COMMON' AND CM.LOV_ID='INVOICE_TYPE' AND CM.LOV_VALUE=I.INV_TYPE) ");
			sqlQuery.append(" INNER JOIN  CURRENCY C ON (I.INV_CCY=C.CCY_CODE) ");
			sqlQuery.append(" WHERE DT.ENTITY_CODE=? AND DT.ENTRY_DATE=? and DT.ENTRY_SL=?");
			util.setSql(sqlQuery.toString());
			util.setString(1, context.getDateFormat());
			util.setLong(2, Long.parseLong(context.getEntityCode()));
			util.setDate(3, new java.sql.Date(BackOfficeFormatUtils.getDate(inputDTO.get("ENTRY_DATE"), context.getDateFormat()).getTime()));
			util.setInt(4, Integer.parseInt(inputDTO.get("ENTRY_SL")));
			ResultSet rset = util.executeQuery();
			gridUtility.init();
			while (rset.next()) {
				gridUtility.startRow();
				gridUtility.setCell(rset.getString("LESSEE_CODE"));
				gridUtility.setCell(rset.getString("AGREEMENT_NO"));
				gridUtility.setCell(rset.getString("SCHEDULE_ID") + "^javascript:viewLeaseDetails(\"" + rset.getString("LEASE_PK") + "\")");
				gridUtility.setCell(rset.getString("LESSOR_BRANCH_CODE"));
				gridUtility.setCell(rset.getString("LESSOR_STATE_CODE"));
				gridUtility.setCell(rset.getString("LOV_LABEL_EN_US"));
				gridUtility.setCell(rset.getString("INV_CYCLE_NUMBER"));
				gridUtility.setCell(rset.getString("INV_CCY"));
				gridUtility.setCell(rset.getString("INV_AMOUNT"));
				gridUtility.setCell(rset.getString("TAX_AMT"));
				gridUtility.setCell("view" + "^javascript:viewBreakUpDetails(\"" + rset.getString("PK") + "\")");
				gridUtility.endRow();
				recordExist = true;
			}
			if (!recordExist) {
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
				return resultDTO;
			} else {
				gridUtility.finish();
				String resultXML = gridUtility.getXML();
				inputDTO.set(ContentManager.RESULT_XML, resultXML);
				inputDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				util.reset();
			}
		} catch (Exception e) {
			e.printStackTrace();
			inputDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			dbContext.close();
		}
		return inputDTO;
	}

	public DTObject loadBreakUpDetails(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		DBContext dbContext = new DBContext();
		try {
			DBUtil util = dbContext.createUtilInstance();
			DHTMLXGridUtility gridUtility = new DHTMLXGridUtility();
			StringBuffer sqlQuery = new StringBuffer(RegularConstants.EMPTY_STRING);
			boolean recordExist = false;
			util.reset();
			util.setMode(DBUtil.PREPARED);
			sqlQuery.append("SELECT TO_CHAR(I.START_DATE,'" + context.getDateFormat() + "')START_DATE,TO_CHAR(I.END_DATE,'" + context.getDateFormat() + "')END_DATE,");
			sqlQuery.append("I.FINANCE_SCHED_INV_FOR,CM.LOV_LABEL_EN_US,I.CCY,");
			sqlQuery.append("FN_FORMAT_AMOUNT(I.PRINCIPAL_AMOUNT,I.CCY,C.SUB_CCY_UNITS_IN_CCY)PRINCIPAL_AMOUNT,");
			sqlQuery.append("FN_FORMAT_AMOUNT(I.INTEREST_AMOUNT,I.CCY,C.SUB_CCY_UNITS_IN_CCY)INTEREST_AMOUNT,");
			sqlQuery.append("FN_FORMAT_AMOUNT(I.RENTAL_AMOUNT,I.CCY,C.SUB_CCY_UNITS_IN_CCY)RENTAL_AMOUNT,");
			sqlQuery.append("FN_FORMAT_AMOUNT(I.EXECUTORY_AMOUNT,I.CCY,C.SUB_CCY_UNITS_IN_CCY)EXECUTORY_AMOUNT,");
			sqlQuery.append("FN_FORMAT_AMOUNT(I.TAX_AMT,I.CCY,C.SUB_CCY_UNITS_IN_CCY)TAX_AMT ");
			sqlQuery.append("FROM INVCOMPDTL I ");
			sqlQuery.append(" INNER JOIN CURRENCY C ON I.CCY=C.CCY_CODE ");
			sqlQuery.append(" INNER JOIN CMLOVREC CM ON (CM.PGM_ID='COMMON' AND CM.LOV_ID='INVOICE_FOR' AND CM.LOV_VALUE=I.FINANCE_SCHED_INV_FOR) ");
			sqlQuery.append(" WHERE I.ENTITY_CODE=? AND I.ENTRY_DATE=? AND I.ENTRY_SL=?");
			util.setSql(sqlQuery.toString());
			util.setLong(1, Long.parseLong(context.getEntityCode()));
			util.setDate(2, new java.sql.Date(BackOfficeFormatUtils.getDate(inputDTO.get("ENTRY_DATE"), context.getDateFormat()).getTime()));
			util.setLong(3, Long.parseLong(inputDTO.get("ENTRY_SL")));
			ResultSet rset = util.executeQuery();
			gridUtility.init();
			while (rset.next()) {
				gridUtility.startRow();
				gridUtility.setCell(rset.getString("LOV_LABEL_EN_US"));
				gridUtility.setCell(rset.getString("START_DATE"));
				gridUtility.setCell(rset.getString("END_DATE"));
				gridUtility.setCell(rset.getString("CCY"));
				gridUtility.setCell(rset.getString("PRINCIPAL_AMOUNT"));
				gridUtility.setCell(rset.getString("INTEREST_AMOUNT"));
				gridUtility.setCell(rset.getString("RENTAL_AMOUNT"));
				gridUtility.setCell(rset.getString("EXECUTORY_AMOUNT"));
				gridUtility.setCell(rset.getString("TAX_AMT"));
				gridUtility.endRow();
				recordExist = true;
			}
			if (!recordExist) {
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
				return resultDTO;
			} else {
				gridUtility.finish();
				String resultXML = gridUtility.getXML();
				inputDTO.set(ContentManager.RESULT_XML, resultXML);
				inputDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				util.reset();
			}

		} catch (Exception e) {
			e.printStackTrace();
			inputDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			dbContext.close();
		}
		return inputDTO;
	}

}
