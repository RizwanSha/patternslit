package patterns.config.web.forms.inv;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.commons.lang3.StringUtils;

import patterns.config.delegate.BackOfficeProcessManager;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.process.TBAProcessResult;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.framework.web.GenericOption;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.framework.web.reports.ReportUtils;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.MasterValidator;
import patterns.config.web.forms.GenericFormBean;

import com.itextpdf.text.Document;
import com.itextpdf.text.pdf.PdfCopy;
import com.itextpdf.text.pdf.PdfImportedPage;
import com.itextpdf.text.pdf.PdfReader;

public class rinvoiceseqprintbean extends GenericFormBean {

	private static final long serialVersionUID = -6219657363150466160L;
	private String billingCycle;
	private String month;
	private String monthYear;
	private String fromDate;
	private String uptoDate;
	private String nofInvoices;
	private String printingMode;
	private String nofInvoicesInSplit;
	private String nofGroupedInvoices;
	static final int BUFFER = 1048576;

	public String getBillingCycle() {
		return billingCycle;
	}

	public void setBillingCycle(String billingCycle) {
		this.billingCycle = billingCycle;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public String getMonthYear() {
		return monthYear;
	}

	public void setMonthYear(String monthYear) {
		this.monthYear = monthYear;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getUptoDate() {
		return uptoDate;
	}

	public void setUptoDate(String uptoDate) {
		this.uptoDate = uptoDate;
	}

	public String getNofInvoices() {
		return nofInvoices;
	}

	public void setNofInvoices(String nofInvoices) {
		this.nofInvoices = nofInvoices;
	}

	public String getPrintingMode() {
		return printingMode;
	}

	public void setPrintingMode(String printingMode) {
		this.printingMode = printingMode;
	}

	public String getNofInvoicesInSplit() {
		return nofInvoicesInSplit;
	}

	public void setNofInvoicesInSplit(String nofInvoicesInSplit) {
		this.nofInvoicesInSplit = nofInvoicesInSplit;
	}

	public String getNofGroupedInvoices() {
		return nofGroupedInvoices;
	}

	public void setNofGroupedInvoices(String nofGroupedInvoices) {
		this.nofGroupedInvoices = nofGroupedInvoices;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void validate() {
	}

	@Override
	public void reset() {
		billingCycle = RegularConstants.EMPTY_STRING;
		month = RegularConstants.EMPTY_STRING;
		monthYear = RegularConstants.EMPTY_STRING;
		fromDate = RegularConstants.EMPTY_STRING;
		uptoDate = RegularConstants.EMPTY_STRING;
		nofInvoices = RegularConstants.EMPTY_STRING;
		printingMode = RegularConstants.EMPTY_STRING;
		nofInvoicesInSplit = RegularConstants.EMPTY_STRING;
		nofGroupedInvoices = RegularConstants.EMPTY_STRING;
		DBContext dbContext = new DBContext();
		ArrayList<GenericOption> monthList = null;
		monthList = getGenericOptions("COMMON", "MONTH", dbContext, null);
		webContext.getRequest().setAttribute("COMMON_MONTH", monthList);

		ArrayList<GenericOption> printingModeList = null;
		printingModeList = getGenericOptions("RINVOICESEQPRINT", "PRINT_MODE", dbContext, "--");
		webContext.getRequest().setAttribute("RINVOICESEQPRINT_PRINT_MODE", printingModeList);

		dbContext.close();
	}

	public DTObject validateBillingCycle(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		String invForCycleNumber = inputDTO.get("INV_CYCLE_NUMBER");
		MasterValidator validator = new MasterValidator();
		try {
			if (validator.isEmpty(invForCycleNumber)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "INV_RENTAL_DAY_FROM,INV_RENTAL_DAY_UPTO");
			resultDTO = validator.validateInvoiceCycleNumber(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
			return resultDTO;
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return resultDTO;
	}

	public DTObject getInvoiceCount(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		CommonValidator validator = new CommonValidator();
		DBContext dbContext = validator.getDbContext();
		DBUtil util = dbContext.createUtilInstance();
		try {
			if (!validator.isEmpty(inputDTO.get("FROM_DATE")) && !validator.isEmpty(inputDTO.get("UPTO_DATE"))) {
				StringBuffer sqlQuery = new StringBuffer();
				sqlQuery.append("SELECT COUNT(1) NO_OF_INVOICES FROM INVOICES I ");
				sqlQuery.append("JOIN INVTEMPLATECODE IT ON(IT.ENTITY_CODE=I.ENTITY_CODE AND IT.INVTEMPLATE_CODE=I.INVOICE_TEMPLATE AND ELECTRONIC_COPY_ONLY='0')");
				sqlQuery.append("WHERE I.ENTITY_CODE=? AND I.INV_DATE BETWEEN ? AND ? AND I.IS_PDF_GENERATED='1'");
				util.reset();
				util.setSql(sqlQuery.toString());
				util.setLong(1, Long.parseLong(context.getEntityCode()));
				util.setDate(2, new java.sql.Date(BackOfficeFormatUtils.getDate(inputDTO.get("FROM_DATE"), context.getDateFormat()).getTime()));
				util.setDate(3, new java.sql.Date(BackOfficeFormatUtils.getDate(inputDTO.get("UPTO_DATE"), context.getDateFormat()).getTime()));
				ResultSet rset = util.executeQuery();
				if (rset.next()) {
					resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
					resultDTO.set("NO_OF_INVOICES", rset.getString("NO_OF_INVOICES"));
				}
			}
			return resultDTO;
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			dbContext.close();
			validator.close();
		}
		return resultDTO;
	}

	public DTObject doPrint(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		CommonValidator validator = new CommonValidator();
		DBContext dbContext = validator.getDbContext();
		DBUtil util = dbContext.createUtilInstance();
		String downloadFilePath = null;
		String downloadFileName = "RINVOICE_GROUP" + StringUtils.leftPad(inputDTO.get("MONTH"), 2, RegularConstants.ZERO) + inputDTO.get("YEAR");
		try {
			if (!validator.isEmpty(inputDTO.get("FROM_DATE")) && !validator.isEmpty(inputDTO.get("UPTO_DATE"))) {
				// Creating temp folder begin
				ReportUtils utils = new ReportUtils();
				String folderName = inputDTO.get("PROGRAM_ID") + "_TEMP_" + utils.getReportSequence();
				String configuredPath = getConfiguredPath();
				if (!new File(configuredPath + "/" + folderName).mkdirs()) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
					return resultDTO;
				}
				
				
				// Creating temp folder end
				/*List<DTObject> generateReportList = generateReports(inputDTO);
				if (generateReportList == null) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
					return resultDTO;
				}
				rinvoicebean rinvoice = new rinvoicebean();
				for (int i = 0; i < generateReportList.size(); i++) {
					resultDTO = rinvoice.generateInvoiceReport(generateReportList.get(i));
					if (!validator.isEmpty(resultDTO.get(ContentManager.ERROR))) {
						return resultDTO;
					}
				}*/

				// get blob data to pdf in to temp folder begin
				List<String> pdfIdentifiers = new ArrayList<String>();
				List<String> custmerIds = new ArrayList<String>();
				DTObject custmerIdCounts = new DTObject();
				StringBuffer sqlQuery = new StringBuffer();
				sqlQuery.append("SELECT I.CUSTOMER_ID,I.PDF_REPORT_IDENTIFIER,C.FILE_DATA");
				sqlQuery.append(" FROM INVOICES I ");
				sqlQuery.append(" JOIN INVTEMPLATECODE IT ON(IT.ENTITY_CODE=I.ENTITY_CODE AND IT.INVTEMPLATE_CODE=I.INVOICE_TEMPLATE AND IT.ELECTRONIC_COPY_ONLY='0')");
				sqlQuery.append(" JOIN CMNFILEINVENTORYSRC");
				sqlQuery.append(inputDTO.get("FIN_YEAR"));
				sqlQuery.append(" C ON (C.ENTITY_CODE=I.ENTITY_CODE AND C.FILE_INV_NUM=I.PDF_FILE_INV_NUM)");
				sqlQuery.append(" WHERE I.ENTITY_CODE=? AND I.INV_DATE BETWEEN ? AND ? AND I.IS_PDF_GENERATED='1'");
				sqlQuery.append(" ORDER BY I.CUSTOMER_ID,I.BILLING_ADDR_SL");
				util.reset();
				util.setSql(sqlQuery.toString());
				util.setLong(1, Long.parseLong(context.getEntityCode()));
				util.setDate(2, new java.sql.Date(BackOfficeFormatUtils.getDate(inputDTO.get("FROM_DATE"), context.getDateFormat()).getTime()));
				util.setDate(3, new java.sql.Date(BackOfficeFormatUtils.getDate(inputDTO.get("UPTO_DATE"), context.getDateFormat()).getTime()));
				ResultSet rset = util.executeQuery();
				String filename = RegularConstants.EMPTY_STRING;

				File genearteFile = null;
				FileOutputStream output = null;
				InputStream in = null;

				while (rset.next()) {
					filename = "RINVOICE" + "_" + rset.getString("PDF_REPORT_IDENTIFIER") + ".pdf";
					genearteFile = new File(configuredPath + "/" + folderName + "/" + filename);
					output = new FileOutputStream(genearteFile);
					in = rset.getBinaryStream("FILE_DATA");
					byte[] buffer = new byte[1048576];
					while (in.read(buffer) > 0) {
						output.write(buffer);
					}
					output.close();
					in.close();

					pdfIdentifiers.add(rset.getString("PDF_REPORT_IDENTIFIER"));
					custmerIds.add(rset.getString("CUSTOMER_ID"));
					if (custmerIdCounts.containsKey(rset.getString("CUSTOMER_ID")))
						custmerIdCounts.set(rset.getString("CUSTOMER_ID"), String.valueOf(Integer.parseInt(custmerIdCounts.get(rset.getString("CUSTOMER_ID"))) + 1));
					else
						custmerIdCounts.set(rset.getString("CUSTOMER_ID"), "1");
				}
				// get blob data to pdf in to temp folder end

				// merge pdfs begin
				int group = 0;

				Document doc = new Document();
				if (inputDTO.get("PRINT_MODE").equals("A")) {
					String path = configuredPath + "/" + folderName + "/" + downloadFileName + ".pdf";
					downloadFilePath = path;
					File outputFile = new File(path);
					output = new FileOutputStream(outputFile);
					PdfCopy copy = new PdfCopy(doc, output);
					doc.open();
					PdfReader reader = null;
					PdfImportedPage curPg = null;
					for (int i = 0; i < pdfIdentifiers.size(); i++) {
						reader = new PdfReader(configuredPath + "/" + folderName + "/" + "RINVOICE" + "_" + pdfIdentifiers.get(i) + ".pdf");
						for (int j = 1; j <= reader.getNumberOfPages(); ++j) {
							curPg = copy.getImportedPage(reader, j);
							copy.addPage(curPg);
						}
						reader.close();
					}
					doc.close();
					output.close();
					String reportIdentifier = utils.getReportSequence();
					String deploymentContext = utils.getDeploymentContext();
					String userID = context.getUserID();
					String entityCode = context.getEntityCode();
					utils.createReportDownload(entityCode, reportIdentifier, deploymentContext, userID, 1, path);
					resultDTO.set(ContentManager.REPORT_ID, reportIdentifier);
				} else {
					String basepath = configuredPath + "/" + folderName + "/";
					boolean newGroup = false;
					String path = basepath + "RINVOICE_GROUP_" + String.valueOf(++group) + ".pdf";

					Document doc1 = new Document();
					File outputFile = new File(path);
					output = new FileOutputStream(outputFile);
					PdfReader reader = null;
					PdfImportedPage curPg = null;
					PdfCopy copy = new PdfCopy(doc1, output);
					doc1.open();
					String custmerid = null;
					int pdfCount = 0;

					for (int i = 0; i < pdfIdentifiers.size(); i++) {
						if (custmerid != null && !custmerid.equals(custmerIds.get(i))) {
							if (inputDTO.get("PRINT_MODE").equals("C")) {
								newGroup = true;
							} else {
								int nofInvoicePerSplit = Integer.parseInt(inputDTO.get("NO_OF_INVOICES_IN_SPLIT"));
								if (pdfCount > nofInvoicePerSplit || (Integer.parseInt(custmerIdCounts.get(custmerIds.get(i))) + pdfCount) > nofInvoicePerSplit + 10)
									newGroup = true;
							}
						}
						custmerid = custmerIds.get(i);
						if (newGroup) {
							path = basepath + "RINVOICE_GROUP_" + String.valueOf(++group) + ".pdf";
							newGroup = false;
							doc1.close();
							output.close();
							doc1 = new Document();
							outputFile = new File(path);
							output = new FileOutputStream(outputFile);
							copy = new PdfCopy(doc1, output);
							doc1.open();
							pdfCount = 0;
						}
						reader = new PdfReader(configuredPath + "/" + folderName + "/" + "RINVOICE" + "_" + pdfIdentifiers.get(i) + ".pdf");
						for (int j = 1; j <= reader.getNumberOfPages(); ++j) {
							curPg = copy.getImportedPage(reader, j);
							copy.addPage(curPg);
						}
						pdfCount++;
						reader.close();
					}

					doc1.close();
					output.close();

					// making zip file begin
					String zipFilePath = configuredPath + "/" + folderName + "/";
					String fileName = downloadFileName + ".zip";
					FileOutputStream dest = new FileOutputStream(zipFilePath + fileName);
					ZipOutputStream zipOutput = new ZipOutputStream(new BufferedOutputStream(dest));
					FileInputStream fis = null;
					for (int g = 1; g <= group; g++) {
						File input = new File(configuredPath + "/" + folderName + "/" + "RINVOICE_GROUP_" + String.valueOf(g) + ".pdf");
						fis = new FileInputStream(input);
						ZipEntry entry = new ZipEntry("RINVOICE_GROUP_" + String.valueOf(g) + ".pdf");
						zipOutput.putNextEntry(entry);
						int count;
						byte[] buffer = new byte[BUFFER];
						while ((count = fis.read(buffer)) > 0) {
							zipOutput.write(buffer, 0, count);
						}
						fis.close();
					}
					zipOutput.close();
					dest.close();
					resultDTO.set("FILENAME", fileName);
					resultDTO.set("FILEPATH", zipFilePath);
					downloadFilePath = zipFilePath + fileName;
					// making zip file end
				}

				// merge pdfs end

				// delete files begin
				File file = new File(configuredPath + "/" + folderName);
				String[] files;
				if (file.isDirectory()) {
					files = file.list();
					for (int i = 0; i < files.length; i++) {
						if (!files[i].contains(downloadFileName)) {
							File deleteFile = new File(file, files[i]);
							deleteFile.delete();
						}
					}
				}
				// delete files end

			}
			dbContext.close();
			validator.close();
			/*setProcessBO("patterns.config.framework.bo.inv.rinvoiceseqprintBO");
			inputDTO.set(RegularConstants.PROCESSBO_CLASS, getProcessBO());
			if (resultDTO.get(ContentManager.ERROR) == null || resultDTO.get(ContentManager.ERROR).equals(RegularConstants.EMPTY_STRING)) {
				inputDTO.set("ENTITY_CODE", context.getEntityCode());
				inputDTO.set("FILE_PATH", downloadFilePath);
				BackOfficeProcessManager processManager = new BackOfficeProcessManager();
				TBAProcessResult processResult = processManager.delegate(inputDTO);
				if (processResult.getProcessStatus().equals(TBAProcessStatus.SUCCESS)) {
					return resultDTO;
				} else {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
				}
			} else {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.REPORT_ERROR);
				return resultDTO;
			}*/
			return resultDTO;
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			if (dbContext != null)
				dbContext.close();
			if (validator != null)
				validator.close();
		}
		return resultDTO;
	}

	public List<DTObject> generateReports(DTObject inputDTO) {
		DTObject input = new DTObject();
		CommonValidator validator = new CommonValidator();
		DBContext dbContext = validator.getDbContext();
		DBUtil util = dbContext.createUtilInstance();
		List<DTObject> generateReportObj = new ArrayList<DTObject>();
		try {
			if (!validator.isEmpty(inputDTO.get("FROM_DATE")) && !validator.isEmpty(inputDTO.get("UPTO_DATE"))) {
				StringBuffer sqlQuery = new StringBuffer();
				sqlQuery.append("SELECT I.LEASE_PRODUCT_CODE,I.INV_YEAR,I.INV_MONTH,I.INV_SERIAL,I.TOTAL_INV_AMT,I.INV_CCY,C.SUB_CCY_UNITS_IN_CCY");
				sqlQuery.append(" FROM INVOICES I ");
				sqlQuery.append(" JOIN INVTEMPLATECODE IT ON(IT.ENTITY_CODE=I.ENTITY_CODE AND IT.INVTEMPLATE_CODE=I.INVOICE_TEMPLATE AND ELECTRONIC_COPY_ONLY='0')");
				sqlQuery.append(" JOIN CURRENCY C ON (C.CCY_CODE=I.INV_CCY)");
				sqlQuery.append("WHERE I.ENTITY_CODE=? AND I.INV_DATE BETWEEN ? AND ? AND I.IS_PDF_GENERATED='0'");
				util.reset();
				util.setSql(sqlQuery.toString());
				util.setLong(1, Long.parseLong(context.getEntityCode()));
				util.setDate(2, new java.sql.Date(BackOfficeFormatUtils.getDate(inputDTO.get("FROM_DATE"), context.getDateFormat()).getTime()));
				util.setDate(3, new java.sql.Date(BackOfficeFormatUtils.getDate(inputDTO.get("UPTO_DATE"), context.getDateFormat()).getTime()));
				ResultSet rset = util.executeQuery();
				while (rset.next()) {
					input = new DTObject();
					input.set("ENTITY_CODE", context.getEntityCode());
					input.set("LEASE_PROD_CODE", rset.getString("LEASE_PRODUCT_CODE"));
					input.set("INV_YEAR", rset.getString("INV_YEAR"));
					input.set("INV_MONTH", rset.getString("INV_MONTH"));
					input.set("INV_SERIAL", rset.getString("INV_SERIAL"));
					input.set("TOTAL_INV_AMT", rset.getString("TOTAL_INV_AMT"));
					input.set("INV_CCY", rset.getString("INV_CCY"));
					input.set("INV_CCY_UNITS", rset.getString("SUB_CCY_UNITS_IN_CCY"));
					input.set("REPORT_TYPE", "RINVOICE");
					generateReportObj.add(input);
				}
			}
			return generateReportObj;
		} catch (Exception e) {
			e.printStackTrace();
			generateReportObj = null;
		} finally {
			validator.close();
		}
		return generateReportObj;
	}

	public String getConfiguredPath() {
		String path = RegularConstants.EMPTY_STRING;
		DBContext dbContext = new DBContext();
		DBUtil utils = dbContext.createUtilInstance();
		try {
			utils.reset();
			String sqlQuery = "SELECT REPORT_GENERATION_PATH FROM SYSTEMFOLDERS WHERE ENTITY_CODE=? AND EFFT_DATE = (SELECT MAX(EFFT_DATE) FROM SYSTEMFOLDERS WHERE ENTITY_CODE = ?  AND EFFT_DATE <= FN_GETCD(?)) ";
			utils.setSql(sqlQuery);
			utils.setLong(1, Long.parseLong(context.getEntityCode()));
			utils.setLong(2, Long.parseLong(context.getEntityCode()));
			utils.setLong(3, Long.parseLong(context.getEntityCode()));
			ResultSet rset = utils.executeQuery();
			if (rset.next()) {
				path = rset.getString(1);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			utils.reset();
			dbContext.close();
		}
		return path;
	}

	public String getFileInvNum(DTObject inputDTO) {
		String fileInvNum = null;
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		try {
			util.reset();
			String sqlQuery = "SELECT DOWNLOAD_FILE_INVENTORY FROM INVOICESEQPRINT WHERE ENTITY_CODE=? AND INV_CYCLE_NUMBER =? AND INV_YEAR=? AND INV_MONTH=? AND NOF_INVOICES=? AND PRINT_MODE=? AND NOF_INV_IN_SPLIT=?";
			util.setSql(sqlQuery);
			util.setLong(1, Long.parseLong(context.getEntityCode()));
			util.setInt(2, Integer.parseInt(inputDTO.get("INV_CYCLE_NUMBER")));
			util.setInt(3, Integer.parseInt(inputDTO.get("YEAR")));
			util.setInt(4, Integer.parseInt(inputDTO.get("MONTH")));
			util.setInt(5, Integer.parseInt(inputDTO.get("NO_OF_INVOICES")));
			util.setString(7, inputDTO.get("PRINT_MODE"));
			util.setInt(8, Integer.parseInt(inputDTO.get("NO_OF_INVOICES_IN_SPLIT")));
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				fileInvNum = rset.getString("DOWNLOAD_FILE_INVENTORY");
			}
		} catch (Exception e) {
		} finally {
			util.reset();
			dbContext.close();
		}
		return fileInvNum;
	}
}
