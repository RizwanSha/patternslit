package patterns.config.web.forms.inv;

import java.sql.ResultSet;
import java.util.ArrayList;

import patterns.config.delegate.BackOfficeProcessManager;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.MessageParam;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.process.TBAProcessResult;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.framework.web.DHTMLXGridUtility;
import patterns.config.framework.web.GenericOption;
import patterns.config.framework.web.RequestConstants;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.InventoryValidator;
import patterns.config.validations.InvoiceValidator;
import patterns.config.validations.LeaseValidator;
import patterns.config.validations.MasterValidator;
import patterns.config.validations.RegistrationValidator;
import patterns.config.web.forms.GenericFormBean;

public class einvgenprocessbean extends GenericFormBean {

	private static final long serialVersionUID = -6219657363150466160L;

	private String invoiceCycleNumber;
	private String invMonth;
	private String invMonthYear;
	private String genDate;
	private String invoiceBranch;
	private String invoiceTempCode;
	private String leaseProdCode;
	private String custCode;
	
	public String getInvoiceCycleNumber() {
		return invoiceCycleNumber;
	}

	public void setInvoiceCycleNumber(String invoiceCycleNumber) {
		this.invoiceCycleNumber = invoiceCycleNumber;
	}

	public String getInvMonth() {
		return invMonth;
	}

	public void setInvMonth(String invMonth) {
		this.invMonth = invMonth;
	}

	public String getInvMonthYear() {
		return invMonthYear;
	}

	public void setInvMonthYear(String invMonthYear) {
		this.invMonthYear = invMonthYear;
	}

	public String getGenDate() {
		return genDate;
	}

	public void setGenDate(String genDate) {
		this.genDate = genDate;
	}

	public String getInvoiceBranch() {
		return invoiceBranch;
	}

	public void setInvoiceBranch(String invoiceBranch) {
		this.invoiceBranch = invoiceBranch;
	}

	public String getInvoiceTempCode() {
		return invoiceTempCode;
	}

	public void setInvoiceTempCode(String invoiceTempCode) {
		this.invoiceTempCode = invoiceTempCode;
	}

	public String getLeaseProdCode() {
		return leaseProdCode;
	}

	public void setLeaseProdCode(String leaseProdCode) {
		this.leaseProdCode = leaseProdCode;
	}

	public String getCustCode() {
		return custCode;
	}

	public void setCustCode(String custCode) {
		this.custCode = custCode;
	}

			
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public void reset() {
		invoiceCycleNumber = RegularConstants.EMPTY_STRING;
		invMonth = RegularConstants.EMPTY_STRING;
		invMonthYear = RegularConstants.EMPTY_STRING;
		genDate = RegularConstants.EMPTY_STRING;
		invoiceBranch = RegularConstants.EMPTY_STRING;
		invoiceTempCode = RegularConstants.EMPTY_STRING;
		leaseProdCode = RegularConstants.EMPTY_STRING;
		custCode = RegularConstants.EMPTY_STRING;
		
		DBContext dbContext = new DBContext();
		ArrayList<GenericOption> monthList = null;
		monthList = getGenericOptions("COMMON", "MONTHS", dbContext, null);
		webContext.getRequest().setAttribute("COMMON_MONTHS", monthList);
		dbContext.close();
		
	}

	@Override
	public void validate() {
		
	}

	public DTObject validate(DTObject inputDTO) {
		logger.logDebug("validate() Begin");
		DTObject resultDTO = new DTObject();
		if(!validateInvoiceCycleNumber()){
			resultDTO.set(ContentManager.ERROR, getErrorMap().getErrorKey("invoiceCycleNumber"));
			resultDTO.set(ContentManager.ERROR_FIELD,"invoiceCycleNumber");
			return resultDTO;
		}
		if(!validateInvoiceMonth()){
			resultDTO.set(ContentManager.ERROR, getErrorMap().getErrorKey("invMonth"));
			resultDTO.set(ContentManager.ERROR_FIELD,"invMonth");
			return resultDTO;
		}
		if(!validateInvoiceMonthYear()){
			resultDTO.set(ContentManager.ERROR, getErrorMap().getErrorKey("invMonthYear"));
			resultDTO.set(ContentManager.ERROR_FIELD,"invMonth");
			return resultDTO;
		}
		if(!validateGenDate()){
			resultDTO.set(ContentManager.ERROR, getErrorMap().getErrorKey("genDate"));
			resultDTO.set(ContentManager.ERROR_FIELD,"genDate");
			return resultDTO;
		}
		if(!validateInvoiceBranchCode()){
			resultDTO.set(ContentManager.ERROR, getErrorMap().getErrorKey("invoiceBranch"));
			resultDTO.set(ContentManager.ERROR_FIELD,"invoiceBranch");
			return resultDTO;
		}
		if(!validateInvoiceTemplateCode()){
			resultDTO.set(ContentManager.ERROR, getErrorMap().getErrorKey("invoiceTempCode"));
			resultDTO.set(ContentManager.ERROR_FIELD,"invoiceTempCode");
			return resultDTO;
		}
		if(!validateLeaseProductCode()){
			resultDTO.set(ContentManager.ERROR, getErrorMap().getErrorKey("leaseProdCode"));
			resultDTO.set(ContentManager.ERROR_FIELD,"leaseProdCode");
			return resultDTO;
		}
		if(!validatecustCode()){
			resultDTO.set(ContentManager.ERROR, getErrorMap().getErrorKey("custCode"));
			resultDTO.set(ContentManager.ERROR_FIELD,"custCode");
			return resultDTO;
		}
		logger.logDebug("validate() end");
		return resultDTO;
	}	
	
	public DTObject processAction(DTObject inputDTO) {
		logger.logDebug("processAction() Begin");
		DTObject resultDTO=new DTObject();
		try{
			setInvoiceCycleNumber(inputDTO.get("invoiceCycleNumber"));
			setInvMonth(inputDTO.get("invMonth"));
			setInvMonthYear(inputDTO.get("invMonthYear"));
			setGenDate(BackOfficeFormatUtils.getDate(context.getCurrentBusinessDate(),context.getDateFormat()) );
			setInvoiceBranch(inputDTO.get("invoiceBranch"));
			setInvoiceTempCode(inputDTO.get("invoiceTempCode"));
			setLeaseProdCode(inputDTO.get("leaseProdCode"));
			setCustCode(inputDTO.get("custCode"));
			setProcessBO("patterns.config.framework.bo.inv.einvgenprocessBO");
			DTObject validateDTO=validate(inputDTO);
			if(validateDTO.get(ContentManager.ERROR)!=null){
				return validateDTO;
			}
			
			DTObject formDTO=getFormDTO();
			formDTO.set("ENTITY_CODE", context.getEntityCode());
			formDTO.set("INV_CYCLE_NUMBER", getInvoiceCycleNumber());
			//formDTO.set("INV_DATE", getInvoiceDate());
			formDTO.set("INV_MONTH",getInvMonth());
			formDTO.set("INV_YEAR", getInvMonthYear());
			formDTO.set("GENERATION_DATE", getGenDate());
			formDTO.set("BRANCH", !getInvoiceBranch().equals(RegularConstants.EMPTY_STRING)?getInvoiceBranch():RegularConstants.NULL);
			formDTO.set("TEMPLATE_CODE", !getInvoiceTempCode().equals(RegularConstants.EMPTY_STRING)?getInvoiceTempCode():RegularConstants.NULL);
			formDTO.set("PRODUCT_CODE", !getLeaseProdCode().equals(RegularConstants.EMPTY_STRING)?getLeaseProdCode():RegularConstants.NULL);
			formDTO.set("LESSE_CODE", !getCustCode().equals(RegularConstants.EMPTY_STRING)?getCustCode():RegularConstants.NULL);
			formDTO.set("USER_ID", context.getUserID());
			formDTO.set("PROCESS_ID", "EINVGENPROCESS");
			formDTO.set(RegularConstants.PROCESSBO_CLASS, getProcessBO());
			
			BackOfficeProcessManager processManager = new BackOfficeProcessManager();
			logger.logDebug("formDTO "+formDTO.toString());
			TBAProcessResult processResult = processManager.delegate(formDTO);
			if (processResult.getProcessStatus().equals(TBAProcessStatus.SUCCESS)) {
				resultDTO.set(ContentManager.STATUS, RegularConstants.SUCCESS);
				resultDTO.set(ContentManager.RESULT, getErrorResource(BackOfficeErrorCodes.PROCESS_SUCCESS, null));
			} else {
				resultDTO.set(ContentManager.STATUS, RegularConstants.FAILURE);
				resultDTO.set(ContentManager.RESULT, getErrorResource(BackOfficeErrorCodes.PROCESS_NOT_SUCCESS, null));
			}
			Object additionalInfo = processResult.getAdditionalInfo();
			if (additionalInfo != null && additionalInfo instanceof String) {
				resultDTO.set(RequestConstants.ADDITIONAL_INFO, processResult.getAdditionalInfo().toString());
			}else if (additionalInfo != null && additionalInfo instanceof MessageParam) {
				MessageParam messageParam = (MessageParam) additionalInfo;
				resultDTO.set(RequestConstants.ADDITIONAL_INFO, getErrorResource(messageParam.getCode(), messageParam.toParameterArray()));
			}
		}catch(Exception e){
			e.printStackTrace();
			logger.logError(e.getLocalizedMessage());
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		logger.logDebug("processAction() End");
		return resultDTO;
	}
	
	private boolean validateInvoiceCycleNumber() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("INV_CYCLE_NUMBER", invoiceCycleNumber);
			formDTO.set(ContentManager.ACTION, USAGE);
			formDTO = validateInvoiceCycleNumber(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("invoiceCycleNumber", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("invoiceCycleNumber", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}
	
	public DTObject validateInvoiceCycleNumber(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		InvoiceValidator validation = new InvoiceValidator();
		String invCycleNumber = inputDTO.get("INV_CYCLE_NUMBER");
		try {
			if (validation.isEmpty(invCycleNumber)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			resultDTO = validation.validateInvoiceCycleNumber(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}
	
	
	
	private boolean validateInvoiceMonth() {
		CommonValidator validator = new CommonValidator();
		try {
			if (validator.isEmpty(invMonth)) {
				getErrorMap().setError("invMonth", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validator.isCMLOVREC("COMMON","MONTHS",invMonth)) {
				getErrorMap().setError("invMonth", BackOfficeErrorCodes.INVALID_OPTION);
				return false;
			}
			
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("invMonth", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return false;
	}
	
	private boolean validateInvoiceMonthYear() {
		CommonValidator validator = new CommonValidator();
		try {
			if (validator.isEmpty(invMonthYear)) {
				getErrorMap().setError("invMonthYear", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validator.isValidYear(invMonthYear)) {
				getErrorMap().setError("invMonthYear", BackOfficeErrorCodes.INVALID_YEAR);
				return false;
			}
			
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("invMonthYear", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return false;
	}
	
	private boolean validateGenDate() {
		CommonValidator validator = new CommonValidator();
		try {
			if (validator.isEmpty(genDate)) {
				getErrorMap().setError("genDate", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (validator.isValidDate(genDate)==null) {
				getErrorMap().setError("genDate", BackOfficeErrorCodes.INVALID_DATE);
				return false;
			}
			
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("genDate", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return false;
	}

	private boolean validateInvoiceBranchCode() {
		CommonValidator validator = new CommonValidator(); 
		try {
			if (validator.isEmpty(invoiceBranch)) {
				return true;
			}
			DTObject formDTO = new DTObject();
			formDTO.set("BRANCH_CODE", invoiceBranch);
			formDTO.set(ContentManager.ACTION, USAGE);
			formDTO = validateInvoiceBranchCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("invoiceBranch", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("invoiceBranch", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally{
			validator.close();
		}
		return false;
	}

	public DTObject validateInvoiceBranchCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		MasterValidator validation = new MasterValidator();
	//	String branchCode = inputDTO.get("BRANCH_CODE");
		try {
			
			inputDTO.set(ContentManager.FETCH_COLUMNS, "BRANCH_NAME");
			resultDTO = validation.validateBranchCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}
	
	private boolean validateInvoiceTemplateCode() {
		CommonValidator validator = new CommonValidator();
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("INVTEMPLATE_CODE", invoiceTempCode);
			formDTO.set(ContentManager.ACTION, USAGE);
			if (validator.isEmpty(invoiceTempCode)) {
				return true;
			}
			formDTO = validateInvoiceTemplateCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("invoiceTempCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("invoiceTempCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally{
			validator.close();
		}
		return false;
	}

	public DTObject validateInvoiceTemplateCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		InventoryValidator validation = new InventoryValidator();
		try {
			
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
			resultDTO = validation.ValidateInvTemplateCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}
	
	private boolean validateLeaseProductCode() {
		CommonValidator validator = new CommonValidator();
		try {
			if (validator.isEmpty(leaseProdCode)) {
				return true;
			}
			DTObject formDTO = new DTObject();
			formDTO.set("LEASE_PRODUCT_CODE", leaseProdCode);
			formDTO.set(ContentManager.ACTION, USAGE);
			formDTO = validateLeaseProductCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("leaseProdCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("leaseProdCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally{
			validator.close();
		}
		return false;
	}

	public DTObject validateLeaseProductCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		MasterValidator validation = new MasterValidator();
		try {
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
			resultDTO = validation.validateLeaseProductCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}
	
	private boolean validatecustCode() {
		CommonValidator validator = new CommonValidator();
		try {
			if (validator.isEmpty(custCode)) {
				return true;
			}
			DTObject formDTO = new DTObject();
			formDTO.set("SUNDRY_DB_AC", custCode);
			formDTO.set(ContentManager.ACTION, USAGE);
			formDTO = validatecustCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("custCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("custCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally{
			validator.close();
		}
		return false;
	}

	public DTObject validatecustCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		DTObject custinputDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		LeaseValidator validation = new LeaseValidator();
		RegistrationValidator regValidator = new RegistrationValidator();
		try {
			inputDTO.set(ContentManager.FETCH, ContentManager.FETCH_ALL);
			resultDTO = validation.valildateLesseCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
			
			custinputDTO.set(ContentManager.ACTION, USAGE);
			custinputDTO.set("CUSTOMER_ID", resultDTO.get("CUSTOMER_ID"));
			custinputDTO.set(ContentManager.FETCH_COLUMNS, "CUSTOMER_NAME");
			resultDTO = regValidator.valildateCustomerID(custinputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
			regValidator.close();
		}
		return resultDTO;
	}
	
	
	public DTObject showResultGrid(DTObject inputDTO) {
		logger.logDebug("einvgenprocessbean showResultGrid() Begin");
		DTObject resultDTO = new DTObject();
		DBContext dbContext = new DBContext();
		try {
			StringBuffer sqlQuery = new StringBuffer(RegularConstants.EMPTY_STRING);
			boolean checkRecord = false;
			DBUtil util = dbContext.createUtilInstance();
			util.reset();
			util.setMode(DBUtil.PREPARED);
			sqlQuery.append(" SELECT TO_CHAR(G.ENTRY_DATE,?) AS ENTRY_DATE,G.ENTRY_SL,G.INV_YEAR,CMM.LOV_LABEL_EN_US,G.INVOICE_TEMPLATE,G.INV_CCY,G.TOTAL_INV, ");
			sqlQuery.append(" FN_FORMAT_AMOUNT(G.TOTAL_INV_AMT,G.INV_CCY,CU.SUB_CCY_UNITS_IN_CCY) AS TOTAL_INV_AMT,");
			sqlQuery.append(" CA.SUNDRY_DB_AC, C.CUSTOMER_NAME,TO_CHAR(G.DUE_DATE,?) AS DUE_DATE");
			sqlQuery.append(" FROM TMPINVCOMPGROUPBRKUP T");
			sqlQuery.append(" INNER JOIN INVCOMPGROUP G ON (G.ENTITY_CODE=? AND T.GRP_ENTRY_DATE=G.ENTRY_DATE AND T.GRP_ENTRY_SL=G.ENTRY_SL)");
			sqlQuery.append(" INNER JOIN  CUSTOMER C ON(C.ENTITY_CODE=G.ENTITY_CODE AND C.CUSTOMER_ID=G.CUSTOMER_ID)");
			sqlQuery.append(" INNER JOIN CUSTOMERACDTL CA ON (CA.ENTITY_CODE=G.ENTITY_CODE AND CA.CUSTOMER_ID=G.CUSTOMER_ID AND CA.LEASE_PRODUCT_CODE=G.LEASE_PRODUCT_CODE)");
			sqlQuery.append(" INNER JOIN CMLOVREC CMM ON (CMM.PGM_ID='COMMON' AND CMM.LOV_ID='MONTH_SHORT' AND CMM.LOV_VALUE=G.INV_MONTH) ");
			sqlQuery.append(" INNER JOIN CURRENCY CU ON (G.INV_CCY=CU.CCY_CODE) ");
			sqlQuery.append(" WHERE T.TMP_SERIAL  =? ");
			logger.logDebug("Query:" + sqlQuery.toString());
			util.setSql(sqlQuery.toString());
			util.setString(1, context.getDateFormat());
			util.setString(2, context.getDateFormat());
			util.setLong(3, Long.parseLong(context.getEntityCode()));
			util.setLong(4, Long.parseLong(inputDTO.get("TEMP_SERIAL")));
			ResultSet rset = util.executeQuery();
			DHTMLXGridUtility gridUtility = new DHTMLXGridUtility();
			gridUtility.init();
			while (rset.next()) {
				gridUtility.startRow();
				gridUtility.setCell(rset.getString("LOV_LABEL_EN_US"));
				gridUtility.setCell(rset.getString("INV_YEAR"));
				gridUtility.setCell(rset.getString("SUNDRY_DB_AC"));
				gridUtility.setCell(rset.getString("CUSTOMER_NAME"));
				gridUtility.setCell(rset.getString("DUE_DATE"));
				gridUtility.setCell(rset.getString("INVOICE_TEMPLATE"));
				gridUtility.setCell(rset.getString("TOTAL_INV"));
				gridUtility.setCell(rset.getString("INV_CCY"));
				gridUtility.setCell(rset.getString("TOTAL_INV_AMT"));
				gridUtility.setCell(rset.getString("ENTRY_DATE"));
				gridUtility.setCell(rset.getString("ENTRY_SL"));
				gridUtility.endRow();
				checkRecord = true;
			}
			if (!checkRecord) {
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
				return resultDTO;
			} else {
				gridUtility.finish();
				resultDTO.set(ContentManager.RESULT_XML, gridUtility.getXML());
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				util.reset();
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			dbContext.close();
		}
		logger.logDebug("einvgenprocessbean showResultGrid() end");
		return resultDTO;
	}

	
	
	
	
	
//popup LoadGrid Start	
	public DTObject loadComponentDetails(DTObject inputDTO) {
		logger.logDebug("einvgenprocessbean loadComponentDetails() Begin");
		DTObject resultDTO = new DTObject();
		DBContext dbContext = new DBContext();
		try {
			DBUtil util = dbContext.createUtilInstance();
			DHTMLXGridUtility gridUtility = new DHTMLXGridUtility();
			StringBuffer sqlQuery = new StringBuffer(RegularConstants.EMPTY_STRING);
			boolean recordExist = false;
			util.reset();
			util.setMode(DBUtil.PREPARED);
			sqlQuery.append(" SELECT I.ENTITY_CODE,I.LESSEE_CODE,I.AGREEMENT_NO,I.SCHEDULE_ID,CONCAT(I.ENTITY_CODE,'|',I.LESSEE_CODE,'|',I.AGREEMENT_NO,'|',I.SCHEDULE_ID)AS LEASE_PK,");
			sqlQuery.append(" I.LESSOR_BRANCH_CODE,I.LESSOR_STATE_CODE,I.INV_TYPE,CM.LOV_LABEL_EN_US,I.INV_CYCLE_NUMBER,I.INV_CCY,");
			sqlQuery.append(" FN_FORMAT_AMOUNT(I.INV_AMOUNT,I.INV_CCY,C.SUB_CCY_UNITS_IN_CCY)INV_AMOUNT,");
			sqlQuery.append(" FN_FORMAT_AMOUNT(I.TAX_AMT,I.INV_CCY,C.SUB_CCY_UNITS_IN_CCY)TAX_AMT,");
			sqlQuery.append(" CONCAT(TO_CHAR(I.ENTRY_DATE,?),'|',I.ENTRY_SL,'|',I.LESSEE_CODE,'|',I.AGREEMENT_NO,'|',I.SCHEDULE_ID) AS PK ");
			sqlQuery.append(" FROM INVCOMPGROUPDTL DT ");
			sqlQuery.append(" INNER JOIN INVCOMPONENT I ON (I.ENTRY_DATE=DT.COMP_ENTRY_DATE AND I.ENTRY_SL=DT.COMP_ENTRY_SL)");
			sqlQuery.append(" INNER JOIN CMLOVREC CM ON (CM.PGM_ID='COMMON' AND CM.LOV_ID='INVOICE_TYPE' AND CM.LOV_VALUE=I.INV_TYPE) ");
			sqlQuery.append(" INNER JOIN  CURRENCY C ON (I.INV_CCY=C.CCY_CODE) ");
			sqlQuery.append(" WHERE DT.ENTITY_CODE=? AND DT.ENTRY_DATE=? and DT.ENTRY_SL=?");
			logger.logDebug("Query:" + sqlQuery.toString());
			util.setSql(sqlQuery.toString());
			util.setString(1, context.getDateFormat());
			util.setString(1, context.getDateFormat());
			util.setLong(2, Long.parseLong(context.getEntityCode()));
			util.setDate(3, new java.sql.Date(BackOfficeFormatUtils.getDate(inputDTO.get("ENTRY_DATE"), context.getDateFormat()).getTime()));
			util.setInt(4, Integer.parseInt(inputDTO.get("ENTRY_SL")));
			ResultSet rset = util.executeQuery();
			gridUtility.init();
			while (rset.next()) {
				gridUtility.startRow();
				gridUtility.setCell(rset.getString("LESSEE_CODE"));
				gridUtility.setCell(rset.getString("AGREEMENT_NO"));
				gridUtility.setCell(rset.getString("SCHEDULE_ID") + "^javascript:viewLeaseDetails(\"" + rset.getString("LEASE_PK") + "\")");
				gridUtility.setCell(rset.getString("LESSOR_BRANCH_CODE"));
				gridUtility.setCell(rset.getString("LESSOR_STATE_CODE"));
				gridUtility.setCell(rset.getString("LOV_LABEL_EN_US"));
				gridUtility.setCell(rset.getString("INV_CYCLE_NUMBER"));
				gridUtility.setCell(rset.getString("INV_CCY"));
				gridUtility.setCell(rset.getString("INV_AMOUNT"));
				gridUtility.setCell(rset.getString("TAX_AMT"));
				gridUtility.setCell("view" + "^javascript:viewBreakUpDetails(\"" + rset.getString("PK") + "\")");
				gridUtility.endRow();
				recordExist = true;
			}
			if (!recordExist) {
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
				return resultDTO;
			} else {
				gridUtility.finish();
				String resultXML = gridUtility.getXML();
				inputDTO.set(ContentManager.RESULT_XML, resultXML);
				inputDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				util.reset();
			}

		} catch (Exception e) {
			e.printStackTrace();
			inputDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			dbContext.close();
		}
		logger.logDebug("einvgenprocessbean loadComponentDetails() Begin");
		return inputDTO;
	}

	public DTObject loadBreakUpDetails(DTObject inputDTO) {
		logger.logDebug("einvgenprocessbean loadBreakUpDetails() Begin");
		DTObject resultDTO = new DTObject();
		DBContext dbContext = new DBContext();
		try {
			DBUtil util = dbContext.createUtilInstance();
			DHTMLXGridUtility gridUtility = new DHTMLXGridUtility();
			StringBuffer sqlQuery = new StringBuffer(RegularConstants.EMPTY_STRING);
			boolean recordExist = false;
			util.reset();
			util.setMode(DBUtil.PREPARED);
			sqlQuery.append("SELECT TO_CHAR(I.START_DATE,'" + context.getDateFormat() + "')START_DATE,TO_CHAR(I.END_DATE,'" + context.getDateFormat() + "')END_DATE,");
			sqlQuery.append("I.FINANCE_SCHED_INV_FOR,CM.LOV_LABEL_EN_US,I.CCY,");
			sqlQuery.append("FN_FORMAT_AMOUNT(I.PRINCIPAL_AMOUNT,I.CCY,C.SUB_CCY_UNITS_IN_CCY)PRINCIPAL_AMOUNT,");
			sqlQuery.append("FN_FORMAT_AMOUNT(I.INTEREST_AMOUNT,I.CCY,C.SUB_CCY_UNITS_IN_CCY)INTEREST_AMOUNT,");
			sqlQuery.append("FN_FORMAT_AMOUNT(I.RENTAL_AMOUNT,I.CCY,C.SUB_CCY_UNITS_IN_CCY)RENTAL_AMOUNT,");
			sqlQuery.append("FN_FORMAT_AMOUNT(I.EXECUTORY_AMOUNT,I.CCY,C.SUB_CCY_UNITS_IN_CCY)EXECUTORY_AMOUNT,");
			sqlQuery.append("FN_FORMAT_AMOUNT(I.TAX_AMT,I.CCY,C.SUB_CCY_UNITS_IN_CCY)TAX_AMT ");
			sqlQuery.append("FROM INVCOMPDTL I ");
			sqlQuery.append(" INNER JOIN CURRENCY C ON I.CCY=C.CCY_CODE ");
			sqlQuery.append(" INNER JOIN CMLOVREC CM ON (CM.PGM_ID='COMMON' AND CM.LOV_ID='INVOICE_FOR' AND CM.LOV_VALUE=I.FINANCE_SCHED_INV_FOR) ");
			sqlQuery.append(" WHERE I.ENTITY_CODE=? AND I.ENTRY_DATE=? AND I.ENTRY_SL=?");
			util.setSql(sqlQuery.toString());
			logger.logDebug("Query:" + sqlQuery.toString());
			util.setLong(1, Long.parseLong(context.getEntityCode()));
			util.setDate(2, new java.sql.Date(BackOfficeFormatUtils.getDate(inputDTO.get("ENTRY_DATE"), context.getDateFormat()).getTime()));
			util.setLong(3, Long.parseLong(inputDTO.get("ENTRY_SL")));
			ResultSet rset = util.executeQuery();
			gridUtility.init();
			while (rset.next()) {
				gridUtility.startRow();
				gridUtility.setCell(rset.getString("LOV_LABEL_EN_US"));
				gridUtility.setCell(rset.getString("START_DATE"));
				gridUtility.setCell(rset.getString("END_DATE"));
				gridUtility.setCell(rset.getString("CCY"));
				gridUtility.setCell(rset.getString("PRINCIPAL_AMOUNT"));
				gridUtility.setCell(rset.getString("INTEREST_AMOUNT"));
				gridUtility.setCell(rset.getString("RENTAL_AMOUNT"));
				gridUtility.setCell(rset.getString("EXECUTORY_AMOUNT"));
				gridUtility.setCell(rset.getString("TAX_AMT"));
				gridUtility.endRow();
				recordExist = true;
			}
			if (!recordExist) {
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
				return resultDTO;
			} else {
				gridUtility.finish();
				String resultXML = gridUtility.getXML();
				inputDTO.set(ContentManager.RESULT_XML, resultXML);
				inputDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				util.reset();
			}
		} catch (Exception e) {
			e.printStackTrace();
			inputDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			dbContext.close();
		}
		logger.logDebug("einvgenprocessbean loadBreakUpDetails() end");
		return inputDTO;
	}
	
	
	
	
	
}