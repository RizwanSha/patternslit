package patterns.config.web.forms.inv;

import java.util.ArrayList;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.service.DTDObject;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.GenericOption;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.InventoryValidator;
import patterns.config.validations.MasterValidator;
import patterns.config.web.forms.GenericFormBean;

public class minvtemplatebean extends GenericFormBean {

	private static final long serialVersionUID = -6219657363150466160L;

	private String invTempCode;
	private String description;
	private String conciseDescription;
	private String vatCst;
	private boolean serviceTax;
	private boolean shadedInvoice;
	private boolean generateDate;
	private boolean captionTnc;
	private boolean eoaPrint;
	private boolean consoBasis;
	private String consoName;
	private boolean consoGrid;
	private boolean consoAnnex;
	private String annexMode;
	private boolean scheduleId;
	private boolean frequency;
	private boolean disbursalAmt;
	private String fieldId;

	private String fieldId2;

	private String fieldType;
	private boolean enabled;
	private String remarks;
	private String xmlMinvtemplateGrid;
	private String xmlMinvtemplateGrid2;
	private String gridAnex;

	private boolean templconsol;
	private boolean invtempl;
	private boolean sign;

	private boolean printleasesch;
	private boolean printleasedeal;
	private boolean printdistamt;

	private boolean grpreqd;
	private boolean listanex;
	private boolean grpreqdanex;

	private String fieldType2;

	private String vatCstDisplay;
	private String annexModeDisplay;
	private String fieldTypeDisplay;
	private String fieldType2Display;
	private String gridAnexDisplay;
	private String groupTypeDisplay;
	private boolean grpreqdHidden;
	private boolean grpreqdanexHidden;
	private String transactionErrors;

	public boolean isPrintleasesch() {
		return printleasesch;
	}

	public String getTransactionErrors() {
		return transactionErrors;
	}

	public void setTransactionErrors(String transactionErrors) {
		this.transactionErrors = transactionErrors;
	}

	public boolean isGrpreqd() {
		return grpreqd;
	}

	public void setGrpreqd(boolean grpreqd) {
		this.grpreqd = grpreqd;
	}

	public String getVatCstDisplay() {
		return vatCstDisplay;
	}

	public void setVatCstDisplay(String vatCstDisplay) {
		this.vatCstDisplay = vatCstDisplay;
	}

	public String getAnnexModeDisplay() {
		return annexModeDisplay;
	}

	public void setAnnexModeDisplay(String annexModeDisplay) {
		this.annexModeDisplay = annexModeDisplay;
	}

	public String getFieldTypeDisplay() {
		return fieldTypeDisplay;
	}

	public void setFieldTypeDisplay(String fieldTypeDisplay) {
		this.fieldTypeDisplay = fieldTypeDisplay;
	}

	public String getFieldType2Display() {
		return fieldType2Display;
	}

	public void setFieldType2Display(String fieldType2Display) {
		this.fieldType2Display = fieldType2Display;
	}

	public String getGridAnexDisplay() {
		return gridAnexDisplay;
	}

	public void setGridAnexDisplay(String gridAnexDisplay) {
		this.gridAnexDisplay = gridAnexDisplay;
	}

	public String getGroupTypeDisplay() {
		return groupTypeDisplay;
	}

	public void setGroupTypeDisplay(String groupTypeDisplay) {
		this.groupTypeDisplay = groupTypeDisplay;
	}

	public boolean isListanex() {
		return listanex;
	}

	public void setListanex(boolean listanex) {
		this.listanex = listanex;
	}

	public boolean isGrpreqdanex() {
		return grpreqdanex;
	}

	public void setGrpreqdanex(boolean grpreqdanex) {
		this.grpreqdanex = grpreqdanex;
	}

	public void setPrintleasesch(boolean printleasesch) {
		this.printleasesch = printleasesch;
	}

	public boolean isPrintleasedeal() {
		return printleasedeal;
	}

	public void setPrintleasedeal(boolean printleasedeal) {
		this.printleasedeal = printleasedeal;
	}

	public boolean isPrintdistamt() {
		return printdistamt;
	}

	public void setPrintdistamt(boolean printdistamt) {
		this.printdistamt = printdistamt;
	}

	public String getGridAnex() {
		return gridAnex;
	}

	public boolean isTemplconsol() {
		return templconsol;
	}

	public void setTemplconsol(boolean templconsol) {
		this.templconsol = templconsol;
	}

	public boolean isInvtempl() {
		return invtempl;
	}

	public void setInvtempl(boolean invtempl) {
		this.invtempl = invtempl;
	}

	public boolean isSign() {
		return sign;
	}

	public void setSign(boolean sign) {
		this.sign = sign;
	}

	public void setGridAnex(String gridAnex) {
		this.gridAnex = gridAnex;
	}

	public String getFieldId2() {
		return fieldId2;
	}

	public void setFieldId2(String fieldId2) {
		this.fieldId2 = fieldId2;
	}

	public String getXmlMinvtemplateGrid2() {
		return xmlMinvtemplateGrid2;
	}

	public void setXmlMinvtemplateGrid2(String xmlMinvtemplateGrid2) {
		this.xmlMinvtemplateGrid2 = xmlMinvtemplateGrid2;
	}

	private String groupType;

	public String getGroupType() {
		return groupType;
	}

	public void setGroupType(String groupType) {
		this.groupType = groupType;
	}

	public String getInvTempCode() {
		return invTempCode;
	}

	public void setInvTempCode(String invTempCode) {
		this.invTempCode = invTempCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getConciseDescription() {
		return conciseDescription;
	}

	public void setConciseDescription(String conciseDescription) {
		this.conciseDescription = conciseDescription;
	}

	public String getVatCst() {
		return vatCst;
	}

	public void setVatCst(String vatCst) {
		this.vatCst = vatCst;
	}

	public boolean isServiceTax() {
		return serviceTax;
	}

	public void setServiceTax(boolean serviceTax) {
		this.serviceTax = serviceTax;
	}

	public boolean isShadedInvoice() {
		return shadedInvoice;
	}

	public void setShadedInvoice(boolean shadedInvoice) {
		this.shadedInvoice = shadedInvoice;
	}

	public boolean isGenerateDate() {
		return generateDate;
	}

	public void setGenerateDate(boolean generateDate) {
		this.generateDate = generateDate;
	}

	public boolean isCaptionTnc() {
		return captionTnc;
	}

	public void setCaptionTnc(boolean captionTnc) {
		this.captionTnc = captionTnc;
	}

	public boolean isEoaPrint() {
		return eoaPrint;
	}

	public void setEoaPrint(boolean eoaPrint) {
		this.eoaPrint = eoaPrint;
	}

	public boolean isConsoBasis() {
		return consoBasis;
	}

	public void setConsoBasis(boolean consoBasis) {
		this.consoBasis = consoBasis;
	}

	public String getConsoName() {
		return consoName;
	}

	public void setConsoName(String consoName) {
		this.consoName = consoName;
	}

	public boolean isConsoGrid() {
		return consoGrid;
	}

	public void setConsoGrid(boolean consoGrid) {
		this.consoGrid = consoGrid;
	}

	public boolean isConsoAnnex() {
		return consoAnnex;
	}

	public void setConsoAnnex(boolean consoAnnex) {
		this.consoAnnex = consoAnnex;
	}

	public String getAnnexMode() {
		return annexMode;
	}

	public void setAnnexMode(String annexMode) {
		this.annexMode = annexMode;
	}

	public boolean isScheduleId() {
		return scheduleId;
	}

	public void setScheduleId(boolean scheduleId) {
		this.scheduleId = scheduleId;
	}

	public boolean isFrequency() {
		return frequency;
	}

	public void setFrequency(boolean frequency) {
		this.frequency = frequency;
	}

	public boolean isDisbursalAmt() {
		return disbursalAmt;
	}

	public void setDisbursalAmt(boolean disbursalAmt) {
		this.disbursalAmt = disbursalAmt;
	}

	public String getFieldId() {
		return fieldId;
	}

	public void setFieldId(String fieldId) {
		this.fieldId = fieldId;
	}

	public String getFieldType() {
		return fieldType;
	}

	public void setFieldType(String fieldType) {
		this.fieldType = fieldType;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getXmlMinvtemplateGrid() {
		return xmlMinvtemplateGrid;
	}

	public void setXmlMinvtemplateGrid(String xmlMinvtemplateGrid) {
		this.xmlMinvtemplateGrid = xmlMinvtemplateGrid;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public boolean isGrpreqdHidden() {
		return grpreqdHidden;
	}

	public void setGrpreqdHidden(boolean grpreqdHidden) {
		this.grpreqdHidden = grpreqdHidden;
	}

	public boolean isGrpreqdanexHidden() {
		return grpreqdanexHidden;
	}

	public void setGrpreqdanexHidden(boolean grpreqdanexHidden) {
		this.grpreqdanexHidden = grpreqdanexHidden;
	}

	@Override
	public void reset() {
		invTempCode = RegularConstants.EMPTY_STRING;
		description = RegularConstants.EMPTY_STRING;
		conciseDescription = RegularConstants.EMPTY_STRING;
		vatCst = RegularConstants.EMPTY_STRING;
		grpreqd = false;
		grpreqdHidden = false;
		grpreqdanex = false;
		grpreqdanexHidden = false;
		serviceTax = false;
		shadedInvoice = false;
		generateDate = false;
		captionTnc = false;
		eoaPrint = false;
		// consolidated = false;
		consoBasis = false;
		consoName = RegularConstants.EMPTY_STRING;
		consoGrid = false;
		consoAnnex = false;
		annexMode = RegularConstants.EMPTY_STRING;
		scheduleId = false;
		frequency = false;
		disbursalAmt = false;
		// asset = false;
		// assetDetails = false;
		templconsol = false;
		invtempl = false;
		printleasesch = false;
		printleasedeal = false;
		printdistamt = false;
		sign = false;
		fieldId = RegularConstants.EMPTY_STRING;
		fieldType = RegularConstants.EMPTY_STRING;
		enabled = false;
		remarks = RegularConstants.EMPTY_STRING;
		DBContext dbContext = new DBContext();
		ArrayList<GenericOption> vatCst = null;
		vatCst = getGenericOptions("COMMON", "CONNECT_POSITION", dbContext, RegularConstants.NULL);
		webContext.getRequest().setAttribute("COMMON_CONNECT_POSITION", vatCst);
		ArrayList<GenericOption> annexMode = null;
		annexMode = getGenericOptions("COMMON", "ANNEX_MODE", dbContext, "--");
		webContext.getRequest().setAttribute("COMMON_ANNEX_MODE", annexMode);
		ArrayList<GenericOption> assetDetails = null;
		assetDetails = getGenericOptions("COMMON", "CONS_PRESENTATION", dbContext, "--");
		webContext.getRequest().setAttribute("COMMON_CONS_PRESENTATION", assetDetails);
		ArrayList<GenericOption> fieldType = null;
		fieldType = getGenericOptions("MINVTEMPLATE", "FIELD_TYPE", dbContext, "--");
		webContext.getRequest().setAttribute("MINVTEMPLATE_FIELD_TYPE", fieldType);
		ArrayList<GenericOption> fieldType2 = null;
		fieldType2 = getGenericOptions("MINVTEMPLATE", "FIELD_TYPE", dbContext, "--");
		webContext.getRequest().setAttribute("MINVTEMPLATE_FIELD_TYPE", fieldType2);
		ArrayList<GenericOption> groupType = null;
		groupType = getGenericOptions("MINVTEMPLATE", "GROUP_TYPE", dbContext, null);
		webContext.getRequest().setAttribute("MINVTEMPLATE_GROUP_TYPE", groupType);
		ArrayList<GenericOption> gridAnex = null;
		gridAnex = getGenericOptions("MINVTEMPLATE", "GRID_ANEX", dbContext, null);
		webContext.getRequest().setAttribute("MINVTEMPLATE_GRID_ANEX", gridAnex);

		dbContext.close();
		setProcessBO("patterns.config.framework.bo.inv.minvtemplateBO");
	}

	public String getFieldType2() {
		return fieldType2;
	}

	public void setFieldType2(String fieldType2) {
		this.fieldType2 = fieldType2;
	}

	@Override
	public void validate() {
		String errMsg = RegularConstants.EMPTY_STRING;
		int errors = 0;
		if (validateInvTempCode()) {
			validateDescription();
			validateConciseDescription();
			// TAB1
			validateVatCst();
			// TAB2
			validateConsoGrid();
			validateGrpReqd();
			validateListAnex();
			validateGrpReqdAnex();
			validateAnnexMode();

			if (getErrorMap().length() > errors) {
				errMsg = errMsg + "Consolidation Configuration  ,";
				errors = getErrorMap().length();
			}
			// TAB3
			validateGrid();
			validateConsoName();
			if (getErrorMap().length() > errors) {
				errMsg = errMsg + "Consolidation Basis   ,";
				errors = getErrorMap().length();
			}
			// TAB4
			validateGrid2();
			if (getErrorMap().length() > errors) {
				errMsg = errMsg + "Print Attributes and Grouping   ";
				errors = getErrorMap().length();
			}
			validateRemarks();

			if (errors > 0) {
				Object[] errorParam = { errMsg };
				getErrorMap().setError("transactionErrors", BackOfficeErrorCodes.TAB_ERROR_CHECK, errorParam);
			}
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.set("INVTEMPLATE_CODE", invTempCode);
				formDTO.set("DESCRIPTION", description);
				formDTO.set("CONCISE_DESCRIPTION", conciseDescription);
				formDTO.set("PRESENTATION_TYPE", "I");
				formDTO.set("VAT_CST_AT_HEADER_FOOTER", vatCst);
				formDTO.set("SERVICE_TAX_NO_PRINTED", decodeBooleanToString(serviceTax));
				formDTO.set("SHADED_INVOICE_HEADER", decodeBooleanToString(shadedInvoice));
				formDTO.set("GENERATION_DATE_PRINTED", decodeBooleanToString(generateDate));
				formDTO.set("CAPTION_FOR_TC_PRINTED", decodeBooleanToString(captionTnc));
				formDTO.set("EOA_DISCLAIMER_PRINTED", decodeBooleanToString(eoaPrint));
				formDTO.set("CONSOLIDATED", decodeBooleanToString(templconsol));
				formDTO.set("SCHEDULE_ID_PRINTED", decodeBooleanToString(printleasesch));
				formDTO.set("LEASE_FREQDEAL_TYPE_PRINTED", decodeBooleanToString(printleasedeal));
				formDTO.set("DISB_AMOUNT_PRINTED", decodeBooleanToString(printdistamt));
				formDTO.set("PRINT_CONSOLIDATION_BASIS", decodeBooleanToString(consoBasis));
				formDTO.set("CONSOLIDATION_NAME", consoName); //
				formDTO.set("CONSOLIDATION_IN_GRID", decodeBooleanToString(consoGrid));
				formDTO.set("CONSOLIDATION_IN_ANNEX", decodeBooleanToString(listanex));
				formDTO.set("ANNEX_MODE", annexMode);
				formDTO.set("GROUPING_IN_GRID", decodeBooleanToString(grpreqdHidden)); //
				formDTO.set("GROUPING_IN_ANNEX", decodeBooleanToString(grpreqdanexHidden));//
				formDTO.set("ELECTRONIC_COPY_ONLY", decodeBooleanToString(invtempl));
				formDTO.set("REQUIRES_SIGN_ATTACH", decodeBooleanToString(sign));

				formDTO.set("FIELD_REF_ASSET_TABLE", fieldType);
				formDTO.set("IS_MANDATORY", RegularConstants.ONE);
				formDTO.set("GRID_ANNEX_BOTH", gridAnex);
				formDTO.set("GROUP_TYPE", groupType);

				formDTO.set("CONS_FIELD_REF_ASSET_TABLE", fieldType2);

				if (getAction().equals(ADD)) {
					formDTO.set("ENABLED", decodeBooleanToString(true));
				} else {
					formDTO.set("ENABLED", decodeBooleanToString(enabled));
				}
				formDTO.set("REMARKS", remarks);
				if (decodeBooleanToString(templconsol).equals(RegularConstants.ONE)) {
					DTDObject dtdObject = new DTDObject();
					dtdObject.addColumn(0, "SL");
					dtdObject.addColumn(1, "FIELD_ID");
					dtdObject.addColumn(2, "DESCRIPTION");
					dtdObject.addColumn(3, "FIELD_TYPE");
					dtdObject.addColumn(4, "FIELD_TYPE_HIDDEN");
					dtdObject.setXML(xmlMinvtemplateGrid);
					formDTO.setDTDObject("INVTEMPLATECODECONSBASIS", dtdObject);
				}
				DTDObject dtdObject2 = new DTDObject();
				dtdObject2.addColumn(0, "SL");
				dtdObject2.addColumn(1, "FIELD_ID");
				dtdObject2.addColumn(2, "DESCRIPTION");
				dtdObject2.addColumn(3, "FLD_TYPE");
				dtdObject2.addColumn(4, "FLD_TYPE_HIDDEN");
				dtdObject2.addColumn(5, "INCLUDE_FIELD_IN");
				dtdObject2.addColumn(6, "INCLUDE_FIELD_IN_HIDDEN");
				dtdObject2.addColumn(7, "GROUP_TYPE");
				dtdObject2.addColumn(8, "GROUP_TYPE_HIDDEN");
				dtdObject2.setXML(xmlMinvtemplateGrid2);
				formDTO.setDTDObject("INVTEMPLATECODEDTL", dtdObject2);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("invTempCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
	}

	public boolean validateInvTempCode() {
		try {
			DTObject formDTO = getFormDTO();
			formDTO.set("INVTEMPLATE_CODE", invTempCode);
			formDTO.set(ContentManager.USER_ACTION, getAction());
			formDTO = validateInvTempCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("invTempCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("invTempCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateInvTempCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		InventoryValidator validation = new InventoryValidator();
		String invTempCode = inputDTO.get("INVTEMPLATE_CODE");
		String action = inputDTO.get(ContentManager.USER_ACTION);
		try {
			if (validation.isEmpty(invTempCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			resultDTO = validation.ValidateInvTemplateCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (action.equals(ADD)) {
				if (ContentManager.DATA_AVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_EXISTS);
					return resultDTO;
				}
			} else {
				if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_NOT_EXISTS);
					return resultDTO;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateDescription() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(description)) {
				getErrorMap().setError("description", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidDescription(description)) {
				getErrorMap().setError("description", BackOfficeErrorCodes.INVALID_DESCRIPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("description", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateConciseDescription() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(conciseDescription)) {
				getErrorMap().setError("conciseDescription", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidConciseDescription(conciseDescription)) {
				getErrorMap().setError("conciseDescription", BackOfficeErrorCodes.INVALID_DESCRIPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("conciseDescription", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateVatCst() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(vatCstDisplay)) {
				getErrorMap().setError("vatCst", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isCMLOVREC("COMMON", "CONNECT_POSITION", vatCstDisplay)) {
				getErrorMap().setError("vatCst", BackOfficeErrorCodes.INVALID_OPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("vatCst", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateConsoName() {
		CommonValidator validation = new CommonValidator();
		try {
			if (consoBasis) {
				if (validation.isEmpty(consoName)) {
					getErrorMap().setError("consoName", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("consoName", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateConsoGrid() {
		try {
			if (!templconsol) {
				if (consoGrid) {
					getErrorMap().setError("consoGrid", BackOfficeErrorCodes.CONS_BASIS_NOTREQD);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("consoGrid", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public boolean validateGrpReqd() {
		try {
			if (!templconsol) {
				if (grpreqdHidden) {
					getErrorMap().setError("grpreqd", BackOfficeErrorCodes.CONS_BASIS_NOTREQD);
					return false;
				}
			} else if (decodeBooleanToString(consoGrid).equals(RegularConstants.ZERO) && decodeBooleanToString(grpreqdHidden).equals(RegularConstants.ONE)) {
				getErrorMap().setError("grpreqd", BackOfficeErrorCodes.VALUE_NOT_ALLOWED);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("grpreqd", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public boolean validateListAnex() {
		try {
			if (!templconsol) {
				if (listanex) {
					getErrorMap().setError("listanex", BackOfficeErrorCodes.CONS_BASIS_NOTREQD);
					return false;
				}
			} else if (decodeBooleanToString(consoGrid).equals(RegularConstants.ZERO) && decodeBooleanToString(listanex).equals(RegularConstants.ZERO)) {
				getErrorMap().setError("grpreqd", BackOfficeErrorCodes.CONS_REQD_GRIDANEX);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("listanex", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public boolean validateGrpReqdAnex() {
		try {
			if (!templconsol) {
				if (grpreqdanexHidden) {
					getErrorMap().setError("grpreqdanex", BackOfficeErrorCodes.CONS_BASIS_NOTREQD);
					return false;
				}
			}
			if (decodeBooleanToString(listanex).equals(RegularConstants.ZERO) && decodeBooleanToString(grpreqdanexHidden).equals(RegularConstants.ONE)) {
				getErrorMap().setError("grpreqdanex", BackOfficeErrorCodes.VALUE_NOT_ALLOWED);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("grpreqdanex", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public boolean validateAnnexMode() {
		CommonValidator validation = new CommonValidator();
		try {
			if (!templconsol) {
				if (!validation.isEmpty(annexModeDisplay)) {
					getErrorMap().setError("annexMode", BackOfficeErrorCodes.CONS_BASIS_NOTREQD);
					return false;
				}
			}
			if (decodeBooleanToString(listanex).equals(RegularConstants.ZERO)) {
				if (!validation.isEmpty(annexModeDisplay)) {
					getErrorMap().setError("annexMode", BackOfficeErrorCodes.VALUE_NOT_ALLOWED);
					return false;
				}
			} else {
				if (validation.isEmpty(annexModeDisplay)) {
					getErrorMap().setError("annexMode", BackOfficeErrorCodes.HMS_MANDATORY);
					return false;
				}
				if (!validation.isCMLOVREC("COMMON", "ANNEX_MODE", annexModeDisplay)) {
					getErrorMap().setError("annexMode", BackOfficeErrorCodes.INVALID_OPTION);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("annexMode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateFieldId(String fieldId) {
		try {
			DTObject formDTO = getFormDTO();
			formDTO.set("FIELD_ID", fieldId);
			formDTO.set(ContentManager.USER_ACTION, getAction());
			formDTO = validateFieldId(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("fieldId", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("fieldId", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateFieldId(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		InventoryValidator validation = new InventoryValidator();
		MasterValidator masterValidation = new MasterValidator();
		String fieldId = inputDTO.get("FIELD_ID");
		try {
			if (validation.isEmpty(fieldId)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH, ContentManager.FETCH_ALL);
			resultDTO = validation.ValidateInvFieldId(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				inputDTO.set(ContentManager.FETCH, ContentManager.FETCH_ALL);
				resultDTO = masterValidation.validateFieldId(inputDTO);
				if (resultDTO.get(ContentManager.ERROR) != null) {
					resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
					return resultDTO;
				}
				if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_NOT_EXISTS);
					return resultDTO;
				}
				resultDTO.set("FIELD_REF_ASSET_TABLE", "U");
				return resultDTO;
			}
			resultDTO.set("FIELD_REF_ASSET_TABLE", "S");
			return resultDTO;
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
			masterValidation.close();
		}
		return resultDTO;
	}

	public boolean validateFieldId2(String fieldId2) {
		try {
			DTObject formDTO = getFormDTO();
			formDTO.set("FIELD_ID", fieldId2);
			formDTO.set(ContentManager.USER_ACTION, getAction());
			formDTO = validateFieldId2(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("fieldId2", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("fieldId2", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public boolean validateGridAnex(String gridAnex) {
		CommonValidator validation = new CommonValidator();
		try {
			if (!templconsol) {
				if (!validation.isEmpty(gridAnex)) {
					getErrorMap().setError("gridAnex", BackOfficeErrorCodes.CONS_DETAILS_NOTREQ);
					return false;
				}
			} else if (validation.isEmpty(gridAnex)) {
				getErrorMap().setError("gridAnex", BackOfficeErrorCodes.CONS_DETAILS_REQ);
				return false;
			}
			if (consoGrid != listanex && decodeBooleanToString(consoGrid).equals(RegularConstants.ONE)) {
				if (!"G".equals(gridAnex)) {
					getErrorMap().setError("gridAnex", BackOfficeErrorCodes.CONS_ONLY_GRID);
					return false;
				}
			}
			if (consoGrid != listanex && decodeBooleanToString(listanex).equals(RegularConstants.ONE)) {
				if (!"A".equals(gridAnex)) {
					getErrorMap().setError("gridAnex", BackOfficeErrorCodes.CONS_ONLY_ANEX);
					return false;
				}
			}
			if (!validation.isEmpty(gridAnex)) {
				if (!validation.isCMLOVREC("MINVTEMPLATE", "GRID_ANEX", gridAnex)) {
					getErrorMap().setError("gridAnex", BackOfficeErrorCodes.INVALID_OPTION);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("gridAnex", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateGroupType(String groupType) {
		CommonValidator validation = new CommonValidator();
		try {
			if (!templconsol) {
				if (!groupType.equals("N")) {
					getErrorMap().setError("groupType", BackOfficeErrorCodes.NO_GRP_REQD);
					return false;
				}
			}

			if (!validation.isCMLOVREC("MINVTEMPLATE", "GROUP_TYPE", groupType)) {
				getErrorMap().setError("groupType", BackOfficeErrorCodes.INVALID_OPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("groupType", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public DTObject validateFieldId2(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		MasterValidator masterValidation = new MasterValidator();
		String fieldId = inputDTO.get("FIELD_ID");
		try {
			if (masterValidation.isEmpty(fieldId)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH, ContentManager.FETCH_ALL);
			resultDTO = masterValidation.ValidateLeaseFld(inputDTO); //
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				inputDTO.set(ContentManager.FETCH, ContentManager.FETCH_ALL);
				resultDTO = masterValidation.validateFieldId(inputDTO);
				if (resultDTO.get(ContentManager.ERROR) != null) {
					resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
					return resultDTO;
				}
				if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_NOT_EXISTS);
					return resultDTO;
				}
				resultDTO.set("FIELD_REF_ASSET_TABLE", "U");
				return resultDTO;
			}
			resultDTO.set("FIELD_REF_ASSET_TABLE", "S");
			return resultDTO;
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			masterValidation.close();
		}
		return resultDTO;
	}

	public boolean validateRemarks() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (remarks != null && remarks.length() > 0) {
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			if (getAction().equals(MODIFY)) {
				if (commonValidator.isEmpty(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}

	private boolean validateGrid() {
		CommonValidator validation = new CommonValidator();
		try {
			if (templconsol) {
				DTDObject dtdObject = new DTDObject();
				dtdObject.addColumn(0, "SL");
				dtdObject.addColumn(1, "FIELD_ID");
				dtdObject.addColumn(2, "DESCRIPTION");
				dtdObject.addColumn(3, "FIELD_TYPE");
				dtdObject.addColumn(4, "FIELD_TYPE_HIDDEN");
				dtdObject.setXML(xmlMinvtemplateGrid);
				if (dtdObject.getRowCount() <= 0) {
					getErrorMap().setError("fieldId", BackOfficeErrorCodes.ATLEAST_ONE_ROW);
					return false;
				}
				if (!validation.checkDuplicateRows(dtdObject, 1, 4)) {
					getErrorMap().setError("fieldId", BackOfficeErrorCodes.DUPLICATE_DATA);
					return false;
				}
				for (int i = 0; i < dtdObject.getRowCount(); i++) {
					if (!validateFieldId(dtdObject.getValue(i, 1))) {
						getErrorMap().setError("fieldId", BackOfficeErrorCodes.INVALID_ACTION);
						return false;
					}
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("fieldId", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	private boolean validateGrid2() {
		CommonValidator validation = new CommonValidator();
		try {
			DTDObject dtdObject2 = new DTDObject();
			dtdObject2.addColumn(0, "SL");
			dtdObject2.addColumn(1, "FIELD_ID");
			dtdObject2.addColumn(2, "DESCRIPTION");
			dtdObject2.addColumn(3, "FLD_TYPE");
			dtdObject2.addColumn(4, "FLD_TYPE_HIDDEN");
			dtdObject2.addColumn(5, "INCLUDE_FIELD_IN");
			dtdObject2.addColumn(6, "INCLUDE_FIELD_IN_HIDDEN");
			dtdObject2.addColumn(7, "GROUP_TYPE");
			dtdObject2.addColumn(8, "GROUP_TYPE_HIDDEN");
			dtdObject2.setXML(xmlMinvtemplateGrid2);
			if (dtdObject2.getRowCount() <= 0) {
				getErrorMap().setError("fieldId2", BackOfficeErrorCodes.ATLEAST_ONE_ROW);
				return false;
			}
			if (!validation.checkDuplicateRows(dtdObject2, 1, 4)) {
				getErrorMap().setError("fieldId2", BackOfficeErrorCodes.DUPLICATE_DATA);
				return false;
			}
			for (int i = 0; i < dtdObject2.getRowCount(); i++) {
				if (!validateFieldId2(dtdObject2.getValue(i, 1))) {
					getErrorMap().setError("fieldId2", BackOfficeErrorCodes.INVALID_ACTION);
					return false;
				}
				if (!validateGridAnex(dtdObject2.getValue(i, 6))) {
					// getErrorMap().setError("gridAnex",
					// BackOfficeErrorCodes.INVALID_ACTION);
					return false;
				}
				if (!validateGroupType(dtdObject2.getValue(i, 8))) {
					// getErrorMap().setError("groupType",
					// BackOfficeErrorCodes.INVALID_ACTION);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("fieldId2", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

}
