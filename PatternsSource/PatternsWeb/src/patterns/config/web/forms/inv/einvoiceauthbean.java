/*
 *
 Author : Raja E
 Created Date : 03-02-2017
 Spec Reference : EINVOICEAUTH
 Modification History
 -----------------------------------------------------------------------------------------------------
 Sl.No		Modified Date	Author			Modified Changes	                      Version
 1			24-03-2017		Pavan 			removed invoking sp SP_INV_GEN_GROUP_COMP	1.1	
 											alter the screen grid column and allowed 
 											bulk Authorization				
 -------------------------------------------------------------------------------------------------------

 */

package patterns.config.web.forms.inv;

import java.sql.ResultSet;
import java.util.ArrayList;

import patterns.config.delegate.BackOfficeProcessManager;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.MessageParam;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.process.TBAProcessResult;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTDObject;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.framework.web.DHTMLXGridUtility;
import patterns.config.framework.web.GenericOption;
import patterns.config.framework.web.RequestConstants;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.framework.web.reports.ReportUtils;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.MasterValidator;
import patterns.config.web.forms.GenericFormBean;

public class einvoiceauthbean extends GenericFormBean {
	private static final long serialVersionUID = -6219657363150466160L;

	private String invoiceDate;
	private String invMonth;
	private String invMonthYear;
	private String leaseProdCode;
	private String processAction;
	private String gridXML;
	private String remarks;

	public String getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(String invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public String getInvMonth() {
		return invMonth;
	}

	public void setInvMonth(String invMonth) {
		this.invMonth = invMonth;
	}

	public String getInvMonthYear() {
		return invMonthYear;
	}

	public void setInvMonthYear(String invMonthYear) {
		this.invMonthYear = invMonthYear;
	}

	public String getLeaseProdCode() {
		return leaseProdCode;
	}

	public void setLeaseProdCode(String leaseProdCode) {
		this.leaseProdCode = leaseProdCode;
	}

	public String getProcessAction() {
		return processAction;
	}

	public void setProcessAction(String processAction) {
		this.processAction = processAction;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getGridXML() {
		return gridXML;
	}

	public void setGridXML(String gridXML) {
		this.gridXML = gridXML;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void reset() {
		invoiceDate = RegularConstants.EMPTY_STRING;
		invMonth = RegularConstants.EMPTY_STRING;
		invMonthYear = RegularConstants.EMPTY_STRING;
		leaseProdCode = RegularConstants.EMPTY_STRING;
		remarks = RegularConstants.EMPTY_STRING;

		DBContext dbContext = new DBContext();
		ArrayList<GenericOption> monthList = null;
		monthList = getGenericOptions("COMMON", "MONTHS", dbContext, null);
		webContext.getRequest().setAttribute("COMMON_MONTHS", monthList);

		ArrayList<GenericOption> authStatusList = null;
		authStatusList = getGenericOptions("COMMON", "AUTHORIZATION_STATUS", dbContext, "--");
		webContext.getRequest().setAttribute("COMMON_AUTHORIZATION_STATUS", authStatusList);
		dbContext.close();

	}

	@Override
	public void validate() {

	}

	public DTObject validateloadPendingGrid(DTObject inputDTO) {
		logger.logDebug("validate() Begin");
		DTObject resultDTO = new DTObject();
		if (!validateLeaseProductCode()) {
			resultDTO.set(ContentManager.ERROR, getErrorMap().getErrorKey("leaseProdCode"));
			resultDTO.set(ContentManager.ERROR_FIELD, "leaseProdCode");
			return resultDTO;
		}
		if (!validateInvoiceMonth()) {
			resultDTO.set(ContentManager.ERROR, getErrorMap().getErrorKey("invMonth"));
			resultDTO.set(ContentManager.ERROR_FIELD, "invMonth");
			return resultDTO;
		}
		if (!validateInvoiceMonthYear()) {
			resultDTO.set(ContentManager.ERROR, getErrorMap().getErrorKey("invMonth"));
			resultDTO.set(ContentManager.ERROR_FIELD, "invMonth");
			return resultDTO;
		}
		if (!validateInvoiceDate()) {
			resultDTO.set(ContentManager.ERROR, getErrorMap().getErrorKey("invoiceDate"));
			resultDTO.set(ContentManager.ERROR_FIELD, "invoiceDate");
			return resultDTO;
		}
		logger.logDebug("validate() End");
		return resultDTO;
	}

	public DTObject validate(DTObject inputDTO) {
		logger.logDebug("validate(DTObject) Begin");
		DTObject resultDTO = new DTObject();
		if (!validateLeaseProductCode()) {
			resultDTO.set(ContentManager.ERROR, getErrorMap().getErrorKey("leaseProdCode"));
			resultDTO.set(ContentManager.ERROR_FIELD, "leaseProdCode");
			return resultDTO;
		}
		if (!validateInvoiceMonth()) {
			resultDTO.set(ContentManager.ERROR, getErrorMap().getErrorKey("invMonth"));
			resultDTO.set(ContentManager.ERROR_FIELD, "invMonth");
			return resultDTO;
		}
		if (!validateInvoiceMonthYear()) {
			resultDTO.set(ContentManager.ERROR, getErrorMap().getErrorKey("invMonth"));
			resultDTO.set(ContentManager.ERROR_FIELD, "invMonth");
			return resultDTO;
		}
		if (!validateGrid()) {
			resultDTO.set(ContentManager.ERROR, getErrorMap().getErrorKey("gridXML"));
			resultDTO.set(ContentManager.ERROR_FIELD, "invAuthGrid");
			return resultDTO;
		}
		if (!validateProcessAction()) {
			resultDTO.set(ContentManager.ERROR, getErrorMap().getErrorKey("processAction"));
			resultDTO.set(ContentManager.ERROR_FIELD, "processAction");
			return resultDTO;
		}
		if (!validateRemarks()) {
			resultDTO.set(ContentManager.ERROR, getErrorMap().getErrorKey("remarks"));
			resultDTO.set(ContentManager.ERROR_FIELD, "remarks");
			return resultDTO;
		}
		logger.logDebug("validate(DTObject) End");
		return resultDTO;
	}

	private boolean validateLeaseProductCode() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("LEASE_PRODUCT_CODE", leaseProdCode);
			formDTO.set(ContentManager.ACTION, USAGE);
			formDTO = validateLeaseProductCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("leaseProdCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("leaseProdCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateLeaseProductCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		MasterValidator validation = new MasterValidator();
		try {
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
			resultDTO = validation.validateLeaseProductCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	private boolean validateInvoiceMonth() {
		CommonValidator validator = new CommonValidator();
		try {
			if (validator.isEmpty(invMonth)) {
				getErrorMap().setError("invMonth", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validator.isCMLOVREC("COMMON", "MONTHS", invMonth)) {
				getErrorMap().setError("invMonth", BackOfficeErrorCodes.INVALID_OPTION);
				return false;
			}

			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("invMonth", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return false;
	}

	private boolean validateInvoiceMonthYear() {
		CommonValidator validator = new CommonValidator();
		try {
			if (validator.isEmpty(invMonthYear)) {
				getErrorMap().setError("invMonth", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validator.isValidYear(invMonthYear)) {
				getErrorMap().setError("invMonth", BackOfficeErrorCodes.INVALID_YEAR);
				return false;
			}

			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("invMonth", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return false;
	}

	private boolean validateInvoiceDate() {
		CommonValidator validator = new CommonValidator();
		try {
			if (!validator.isEmpty(invoiceDate)) {
				if (validator.isValidDate(invoiceDate) == null) {
					getErrorMap().setError("invoiceDate", BackOfficeErrorCodes.INVALID_DATE);
					return false;
				}
				/*
				 * if (validator.isDateGreaterThanCBD(invoiceDate)) {
				 * getErrorMap().setError("invoiceDate",
				 * BackOfficeErrorCodes.DATE_LECBD); return false; }
				 */
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("invoiceDate", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return false;
	}

	private boolean validateGrid() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (commonValidator.isEmpty(gridXML)) {
				getErrorMap().setError("gridXML", BackOfficeErrorCodes.ATLEAST_ONE_ROW);
				return false;
			}
			DTDObject dtdObject = new DTDObject();
			dtdObject.addColumn(0, "GRP_ENTRY_DATE");
			dtdObject.addColumn(1, "GRP_ENTRY_SL");
			dtdObject.setDateFormat(context.getDateFormat());
			dtdObject.setXML(gridXML);
			if (dtdObject.getRowCount() == 0) {
				getErrorMap().setError("gridXML", BackOfficeErrorCodes.ATLEAST_ONE_ROW);
				return false;
			}

			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("gridXML", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	private boolean validateProcessAction() {
		CommonValidator validator = new CommonValidator();
		try {
			if (validator.isEmpty(processAction)) {
				getErrorMap().setError("processAction", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validator.isCMLOVREC("COMMON", "AUTHORIZATION_STATUS", processAction)) {
				getErrorMap().setError("processAction", BackOfficeErrorCodes.INVALID_OPTION);
				return false;
			}

			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("processAction", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return false;
	}

	public boolean validateRemarks() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (remarks != null && remarks.length() > 0) {
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			if (getProcessAction().equals(RegularConstants.REJECTED)) {
				if (commonValidator.isEmpty(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}

	public DTObject loadPendingRecords(DTObject inputDTO) {
		logger.logDebug("loadPendingRecords() Begin");
		DTObject resultDTO = new DTObject();
		DBContext dbContext = new DBContext();
		try {
			setLeaseProdCode(inputDTO.get("LEASE_PRODUCT_CODE"));
			setInvMonth(inputDTO.get("INVOICE_MONTH"));
			setInvMonthYear(inputDTO.get("INVOICE_YEAR"));
			setInvoiceDate(inputDTO.get("INVOICE_DATE"));
			DTObject validateDTO = validateloadPendingGrid(inputDTO);
			if (validateDTO.get(ContentManager.ERROR) != null) {
				return validateDTO;
			}
			StringBuffer sqlQuery = new StringBuffer(RegularConstants.EMPTY_STRING);
			boolean checkRecord = false;
			DBUtil util = dbContext.createUtilInstance();
			util.reset();
			util.setMode(DBUtil.PREPARED);
			sqlQuery.append(" SELECT TO_CHAR(G.ENTRY_DATE,?) AS ENTRY_DATE,G.ENTRY_SL,G.INV_YEAR,CMM.LOV_LABEL_EN_US,G.INVOICE_TEMPLATE,G.INV_CCY,G.TOTAL_INV, ");
			sqlQuery.append(" FN_FORMAT_AMOUNT(G.TOTAL_INV_AMT,G.INV_CCY,CU.SUB_CCY_UNITS_IN_CCY) AS TOTAL_INV_AMT,");
			sqlQuery.append(" CA.SUNDRY_DB_AC, C.CUSTOMER_NAME,TO_CHAR(G.DUE_DATE,?) AS DUE_DATE");
			sqlQuery.append(" FROM INVCOMPGROUP G ");
			sqlQuery.append(" JOIN CUSTOMER C ON(C.ENTITY_CODE=G.ENTITY_CODE AND C.CUSTOMER_ID=G.CUSTOMER_ID)");
			sqlQuery.append(" JOIN CUSTOMERACDTL CA ON (CA.ENTITY_CODE=G.ENTITY_CODE AND CA.CUSTOMER_ID=G.CUSTOMER_ID AND CA.LEASE_PRODUCT_CODE=G.LEASE_PRODUCT_CODE)");
			sqlQuery.append(" INNER JOIN CMLOVREC CMM ON (CMM.PGM_ID='COMMON' AND CMM.LOV_ID='MONTH_SHORT' AND CMM.LOV_VALUE=G.INV_MONTH) ");
			sqlQuery.append(" INNER JOIN CURRENCY CU ON (G.INV_CCY=CU.CCY_CODE) ");
			sqlQuery.append(" WHERE G.ENTITY_CODE=? AND G.LEASE_PRODUCT_CODE=? AND G.INV_YEAR=? AND G.INV_MONTH=? ");
			if (!(inputDTO.get("INVOICE_DATE").trim()).equals(RegularConstants.EMPTY_STRING))
				sqlQuery.append(" AND G.DUE_DATE=?");
			sqlQuery.append(" AND G.AUTH_ENTRY_DATE IS NULL ");
			logger.logDebug("Query:" + sqlQuery.toString());
			util.setSql(sqlQuery.toString());
			util.setString(1, context.getDateFormat());
			util.setString(2, context.getDateFormat());
			util.setLong(3, Long.parseLong(context.getEntityCode()));
			util.setString(4, inputDTO.get("LEASE_PRODUCT_CODE"));
			util.setInt(5, Integer.parseInt(inputDTO.get("INVOICE_YEAR")));
			util.setInt(6, Integer.parseInt(inputDTO.get("INVOICE_MONTH")));
			if (!(inputDTO.get("INVOICE_DATE").trim()).equals(RegularConstants.EMPTY_STRING))
				util.setDate(7, new java.sql.Date(BackOfficeFormatUtils.getDate(inputDTO.get("INVOICE_DATE"), context.getDateFormat()).getTime()));
			ResultSet rset = util.executeQuery();
			DHTMLXGridUtility gridUtility = new DHTMLXGridUtility();
			gridUtility.init();
			while (rset.next()) {
				gridUtility.startRow();
				gridUtility.setCell("");
				gridUtility.setCell(rset.getString("LOV_LABEL_EN_US"));
				gridUtility.setCell(rset.getString("INV_YEAR"));
				gridUtility.setCell(rset.getString("SUNDRY_DB_AC"));
				gridUtility.setCell(rset.getString("CUSTOMER_NAME"));
				gridUtility.setCell(rset.getString("DUE_DATE"));
				gridUtility.setCell(rset.getString("INVOICE_TEMPLATE"));
				gridUtility.setCell(rset.getString("TOTAL_INV"));
				gridUtility.setCell(rset.getString("INV_CCY"));
				gridUtility.setCell(rset.getString("TOTAL_INV_AMT"));
				gridUtility.setCell(rset.getString("ENTRY_DATE"));
				gridUtility.setCell(rset.getString("ENTRY_SL"));
				gridUtility.setCell(RegularConstants.ZERO);
				gridUtility.endRow();
				checkRecord = true;

			}
			if (!checkRecord) {
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
				return resultDTO;
			} else {
				gridUtility.finish();
				resultDTO.set(ContentManager.RESULT_XML, gridUtility.getXML());
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				util.reset();
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			dbContext.close();
		}
		logger.logDebug("loadPendingRecords() end");
		return resultDTO;
	}

	public DTObject loadComponentDetails(DTObject inputDTO) {
		logger.logDebug("loadComponentDetails() Begin");
		DTObject resultDTO = new DTObject();
		DBContext dbContext = new DBContext();
		try {
			DBUtil util = dbContext.createUtilInstance();
			DHTMLXGridUtility gridUtility = new DHTMLXGridUtility();
			StringBuffer sqlQuery = new StringBuffer(RegularConstants.EMPTY_STRING);
			boolean recordExist = false;
			util.reset();
			util.setMode(DBUtil.PREPARED);
			sqlQuery.append(" SELECT I.ENTITY_CODE,I.LESSEE_CODE,I.AGREEMENT_NO,I.SCHEDULE_ID,CONCAT(I.ENTITY_CODE,'|',I.LESSEE_CODE,'|',I.AGREEMENT_NO,'|',I.SCHEDULE_ID)AS LEASE_PK,");
			sqlQuery.append(" I.LESSOR_BRANCH_CODE,I.LESSOR_STATE_CODE,I.INV_TYPE,CM.LOV_LABEL_EN_US,I.INV_CYCLE_NUMBER,I.INV_CCY,");
			sqlQuery.append(" FN_FORMAT_AMOUNT(I.INV_AMOUNT,I.INV_CCY,C.SUB_CCY_UNITS_IN_CCY)INV_AMOUNT,");
			sqlQuery.append(" FN_FORMAT_AMOUNT(I.TAX_AMT,I.INV_CCY,C.SUB_CCY_UNITS_IN_CCY)TAX_AMT,");
			sqlQuery.append(" CONCAT(TO_CHAR(I.ENTRY_DATE,?),'|',I.ENTRY_SL,'|',I.LESSEE_CODE,'|',I.AGREEMENT_NO,'|',I.SCHEDULE_ID) AS PK ");
			sqlQuery.append(" FROM INVCOMPGROUPDTL DT ");
			sqlQuery.append(" INNER JOIN INVCOMPONENT I ON (I.ENTRY_DATE=DT.COMP_ENTRY_DATE AND I.ENTRY_SL=DT.COMP_ENTRY_SL)");
			sqlQuery.append(" INNER JOIN CMLOVREC CM ON (CM.PGM_ID='COMMON' AND CM.LOV_ID='INVOICE_TYPE' AND CM.LOV_VALUE=I.INV_TYPE) ");
			sqlQuery.append(" INNER JOIN  CURRENCY C ON (I.INV_CCY=C.CCY_CODE) ");
			sqlQuery.append(" WHERE DT.ENTITY_CODE=? AND DT.ENTRY_DATE=? and DT.ENTRY_SL=?");
			logger.logDebug("Query:" + sqlQuery.toString());
			util.setSql(sqlQuery.toString());
			util.setString(1, context.getDateFormat());
			util.setLong(2, Long.parseLong(context.getEntityCode()));
			util.setDate(3, new java.sql.Date(BackOfficeFormatUtils.getDate(inputDTO.get("ENTRY_DATE"), context.getDateFormat()).getTime()));
			util.setInt(4, Integer.parseInt(inputDTO.get("ENTRY_SL")));
			ResultSet rset = util.executeQuery();
			gridUtility.init();
			while (rset.next()) {
				gridUtility.startRow();
				gridUtility.setCell(rset.getString("LESSEE_CODE"));
				gridUtility.setCell(rset.getString("AGREEMENT_NO"));
				gridUtility.setCell(rset.getString("SCHEDULE_ID") + "^javascript:viewLeaseDetails(\"" + rset.getString("LEASE_PK") + "\")");
				gridUtility.setCell(rset.getString("LESSOR_BRANCH_CODE"));
				gridUtility.setCell(rset.getString("LESSOR_STATE_CODE"));
				gridUtility.setCell(rset.getString("LOV_LABEL_EN_US"));
				gridUtility.setCell(rset.getString("INV_CYCLE_NUMBER"));
				gridUtility.setCell(rset.getString("INV_CCY"));
				gridUtility.setCell(rset.getString("INV_AMOUNT"));
				gridUtility.setCell(rset.getString("TAX_AMT"));
				gridUtility.setCell("view" + "^javascript:viewBreakUpDetails(\"" + rset.getString("PK") + "\")");
				gridUtility.endRow();
				recordExist = true;
			}
			if (!recordExist) {
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
				return resultDTO;
			} else {
				gridUtility.finish();
				String resultXML = gridUtility.getXML();
				inputDTO.set(ContentManager.RESULT_XML, resultXML);
				inputDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				util.reset();
			}

		} catch (Exception e) {
			e.printStackTrace();
			inputDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			dbContext.close();
		}
		logger.logDebug("loadComponentDetails() Begin");
		return inputDTO;
	}

	public DTObject loadBreakUpDetails(DTObject inputDTO) {
		logger.logDebug("loadBreakUpDetails() Begin");
		DTObject resultDTO = new DTObject();
		DBContext dbContext = new DBContext();
		try {
			DBUtil util = dbContext.createUtilInstance();
			DHTMLXGridUtility gridUtility = new DHTMLXGridUtility();
			StringBuffer sqlQuery = new StringBuffer(RegularConstants.EMPTY_STRING);
			boolean recordExist = false;
			util.reset();
			util.setMode(DBUtil.PREPARED);
			sqlQuery.append("SELECT TO_CHAR(I.START_DATE,'" + context.getDateFormat() + "')START_DATE,TO_CHAR(I.END_DATE,'" + context.getDateFormat() + "')END_DATE,");
			sqlQuery.append("I.FINANCE_SCHED_INV_FOR,CM.LOV_LABEL_EN_US,I.CCY,");
			sqlQuery.append("FN_FORMAT_AMOUNT(I.PRINCIPAL_AMOUNT,I.CCY,C.SUB_CCY_UNITS_IN_CCY)PRINCIPAL_AMOUNT,");
			sqlQuery.append("FN_FORMAT_AMOUNT(I.INTEREST_AMOUNT,I.CCY,C.SUB_CCY_UNITS_IN_CCY)INTEREST_AMOUNT,");
			sqlQuery.append("FN_FORMAT_AMOUNT(I.RENTAL_AMOUNT,I.CCY,C.SUB_CCY_UNITS_IN_CCY)RENTAL_AMOUNT,");
			sqlQuery.append("FN_FORMAT_AMOUNT(I.EXECUTORY_AMOUNT,I.CCY,C.SUB_CCY_UNITS_IN_CCY)EXECUTORY_AMOUNT,");
			sqlQuery.append("FN_FORMAT_AMOUNT(I.TAX_AMT,I.CCY,C.SUB_CCY_UNITS_IN_CCY)TAX_AMT ");
			sqlQuery.append("FROM INVCOMPDTL I ");
			sqlQuery.append(" INNER JOIN CURRENCY C ON I.CCY=C.CCY_CODE ");
			sqlQuery.append(" INNER JOIN CMLOVREC CM ON (CM.PGM_ID='COMMON' AND CM.LOV_ID='INVOICE_FOR' AND CM.LOV_VALUE=I.FINANCE_SCHED_INV_FOR) ");
			sqlQuery.append(" WHERE I.ENTITY_CODE=? AND I.ENTRY_DATE=? AND I.ENTRY_SL=?");
			util.setSql(sqlQuery.toString());
			logger.logDebug("Query:" + sqlQuery.toString());
			util.setLong(1, Long.parseLong(context.getEntityCode()));
			util.setDate(2, new java.sql.Date(BackOfficeFormatUtils.getDate(inputDTO.get("ENTRY_DATE"), context.getDateFormat()).getTime()));
			util.setLong(3, Long.parseLong(inputDTO.get("ENTRY_SL")));
			ResultSet rset = util.executeQuery();
			gridUtility.init();
			while (rset.next()) {
				gridUtility.startRow();
				gridUtility.setCell(rset.getString("LOV_LABEL_EN_US"));
				gridUtility.setCell(rset.getString("START_DATE"));
				gridUtility.setCell(rset.getString("END_DATE"));
				gridUtility.setCell(rset.getString("CCY"));
				gridUtility.setCell(rset.getString("PRINCIPAL_AMOUNT"));
				gridUtility.setCell(rset.getString("INTEREST_AMOUNT"));
				gridUtility.setCell(rset.getString("RENTAL_AMOUNT"));
				gridUtility.setCell(rset.getString("EXECUTORY_AMOUNT"));
				gridUtility.setCell(rset.getString("TAX_AMT"));
				gridUtility.endRow();
				recordExist = true;
			}
			if (!recordExist) {
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
				return resultDTO;
			} else {
				gridUtility.finish();
				String resultXML = gridUtility.getXML();
				inputDTO.set(ContentManager.RESULT_XML, resultXML);
				inputDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				util.reset();
			}
		} catch (Exception e) {
			e.printStackTrace();
			inputDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			dbContext.close();
		}
		logger.logDebug("loadBreakUpDetails() end");
		return inputDTO;
	}

	public DTObject processRequest(DTObject inputDTO) {
		logger.logDebug("processAction() Begin");
		DTObject resultDTO = new DTObject();
		try {
			setLeaseProdCode(inputDTO.get("LEASE_PRODUCT_CODE"));
			setInvMonth(inputDTO.get("INVOICE_MONTH"));
			setInvMonthYear(inputDTO.get("INVOICE_YEAR"));
			setGridXML(inputDTO.get("XML_DATA"));
			setProcessAction(inputDTO.get("PROCESS_ACTION"));
			setRemarks(inputDTO.get("REMARKS"));
			setProcessBO("patterns.config.framework.bo.inv.einvoiceauthBO");
			DTObject validateDTO = validate(inputDTO);
			if (validateDTO.get(ContentManager.ERROR) != null) {
				return validateDTO;
			}

			DTObject formDTO = getFormDTO();
			formDTO.set("ENTITY_CODE", context.getEntityCode());
			formDTO.set("USER_ID", context.getUserID());
			formDTO.set("PRODUCT_CODE", leaseProdCode);
			formDTO.set("INV_MONTH", invMonth);
			formDTO.set("INV_YEAR", invMonthYear);
			formDTO.set("ACTION_TYPE", processAction);
			formDTO.set("REMARKS", remarks);
			formDTO.setObject("ENTRY_DATE", context.getCurrentBusinessDate());
			formDTO.set("PROCESS_ID", "EINVOICEAUTH");
			DTDObject dtdObject = new DTDObject();
			dtdObject.addColumn(0, "GRP_ENTRY_DATE");
			dtdObject.addColumn(1, "GRP_ENTRY_SL");
			dtdObject.setDateFormat(context.getDateFormat());
			dtdObject.setXML(inputDTO.get("XML_DATA"));
			formDTO.setDTDObject("EINVOICEAUTHDTL", dtdObject);
			formDTO.set(RegularConstants.PROCESSBO_CLASS, getProcessBO());

			BackOfficeProcessManager processManager = new BackOfficeProcessManager();
			logger.logDebug("formDTO:" + formDTO.toString());
			TBAProcessResult processResult = processManager.delegate(formDTO);
			if (processResult.getProcessStatus().equals(TBAProcessStatus.SUCCESS)) {
				resultDTO.set(ContentManager.STATUS, RegularConstants.SUCCESS);
				resultDTO.set(ContentManager.RESULT, getErrorResource(BackOfficeErrorCodes.REQUEST_SUBMITTED, null));
			} else {
				resultDTO.set(ContentManager.STATUS, RegularConstants.FAILURE);
				resultDTO.set(ContentManager.RESULT, getErrorResource(BackOfficeErrorCodes.PROCESS_NOT_SUCCESS, null));
			}
			
			Object additionalInfo = processResult.getAdditionalInfo();
			if (additionalInfo != null && additionalInfo instanceof String) {
				resultDTO.set(RequestConstants.ADDITIONAL_INFO, processResult.getAdditionalInfo().toString());
			} else if (additionalInfo != null && additionalInfo instanceof MessageParam) {
				MessageParam messageParam = (MessageParam) additionalInfo;
				resultDTO.set(RequestConstants.ADDITIONAL_INFO, getErrorResource(messageParam.getCode(), messageParam.toParameterArray()));
			}

		} catch (Exception e) {
			e.printStackTrace();
			logger.logError(e.getLocalizedMessage());
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		logger.logDebug("processAction() End");
		return resultDTO;
	}

	// added on 26-03-2017
	public DTObject doPrint(DTObject inputDTO) throws Exception {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBContext dbContext = new DBContext();
		try {
			setLeaseProdCode(inputDTO.get("LEASE_PRODUCT_CODE"));
			setInvMonth(inputDTO.get("INVOICE_MONTH"));
			setInvMonthYear(inputDTO.get("INVOICE_YEAR"));
			setInvoiceDate(inputDTO.get("DUE_DATE"));
			DTObject validateDTO = validateloadPendingGrid(inputDTO);
			if (validateDTO.get(ContentManager.ERROR) != null) {
				return validateDTO;
			}
			inputDTO.set("DATE_FORMAT", BackOfficeConstants.TBA_DATE_FORMAT);
			inputDTO.set(ContentManager.REPORT_HANDLER, "patterns.config.web.reports.inv.rinvoiceauthreport");
			if (inputDTO.get("REPORT_FORMAT").equals("") || inputDTO.get("REPORT_FORMAT") == null)
				inputDTO.set(ContentManager.REPORT_FORMAT, ReportUtils.EXPORT_XLS + "");
			else
				inputDTO.set(ContentManager.REPORT_FORMAT, inputDTO.get("REPORT_FORMAT"));
			StringBuffer sqlQuery = new StringBuffer(RegularConstants.EMPTY_STRING);
			DBUtil util = dbContext.createUtilInstance();
			util.reset();
			util.setMode(DBUtil.PREPARED);
			sqlQuery.append(" SELECT TO_CHAR(G.ENTRY_DATE,?) AS ENTRY_DATE,G.ENTRY_SL,CMM.LOV_LABEL_EN_US INVMONTH,G.INV_YEAR, C.CUSTOMER_NAME,GD.LESSEE_CODE,GD.AGREEMENT_NO,GD.SCHEDULE_ID,");
			sqlQuery.append(" IVCD.FINANCE_SCHED_INV_FOR,IVCD.CCY,IVCD.PRINCIPAL_AMOUNT, IVCD.INTEREST_AMOUNT,IVCD.RENTAL_AMOUNT, IVCD.EXECUTORY_AMOUNT,IVCD.TAX_AMT ");
			sqlQuery.append(" ,TO_CHAR(IVCD.START_DATE,?) AS START_DATE ,TO_CHAR(IVCD.END_DATE,?) AS END_DATE ");
			sqlQuery.append(" ,TO_CHAR(IVCD.DUE_DATE,?) AS DUE_DATE ,CM.LOV_LABEL_EN_US AS SCHEDULE_FOR ");
			sqlQuery.append(" FROM INVCOMPGROUP G ");
			sqlQuery.append(" JOIN INVCOMPGROUPDTL GD ON(GD.ENTITY_CODE=G.ENTITY_CODE AND GD.ENTRY_DATE=G.ENTRY_DATE AND GD.ENTRY_SL=G.ENTRY_SL) ");
			sqlQuery.append(" JOIN INVCOMPONENT IVC ON (IVC.ENTITY_CODE=GD.ENTITY_CODE AND IVC.ENTRY_DATE=GD.COMP_ENTRY_DATE AND IVC.ENTRY_SL=GD.COMP_ENTRY_SL) ");
			sqlQuery.append(" JOIN INVCOMPDTL IVCD ON (IVCD.ENTITY_CODE=IVC.ENTITY_CODE AND IVCD.ENTRY_DATE=IVC.ENTRY_DATE AND IVCD.ENTRY_SL=IVC.ENTRY_SL)");
			sqlQuery.append(" JOIN CUSTOMER C ON(C.ENTITY_CODE=G.ENTITY_CODE AND C.CUSTOMER_ID=G.CUSTOMER_ID)");
			sqlQuery.append(" JOIN CMLOVREC CMM ON (CMM.PGM_ID='COMMON' AND CMM.LOV_ID='MONTH_SHORT' AND CMM.LOV_VALUE=G.INV_MONTH)");
			sqlQuery.append(" JOIN CMLOVREC CM ON (CM.PGM_ID='COMMON' AND CM.LOV_ID='INVOICE_FOR' AND CM.LOV_VALUE=IVCD.FINANCE_SCHED_INV_FOR)");
			sqlQuery.append(" WHERE G.ENTITY_CODE=? AND G.LEASE_PRODUCT_CODE=? AND G.INV_YEAR=? AND G.INV_MONTH=? ");
			if (!(inputDTO.get("INVOICE_DATE").trim()).equals(RegularConstants.EMPTY_STRING))
				sqlQuery.append(" AND G.DUE_DATE=?");
			sqlQuery.append(" AND G.AUTH_ENTRY_DATE IS NULL ");
			logger.logDebug("Report Query:" + sqlQuery.toString());
			util.setSql(sqlQuery.toString());
			util.setString(1, context.getDateFormat());
			util.setString(2, context.getDateFormat());
			util.setString(3, context.getDateFormat());
			util.setString(4, context.getDateFormat());
			util.setLong(5, Long.parseLong(context.getEntityCode()));
			util.setString(6, inputDTO.get("LEASE_PRODUCT_CODE"));
			util.setInt(7, Integer.parseInt(inputDTO.get("INVOICE_YEAR")));
			util.setInt(8, Integer.parseInt(inputDTO.get("INVOICE_MONTH")));
			if (!(inputDTO.get("INVOICE_DATE").trim()).equals(RegularConstants.EMPTY_STRING))
				util.setDate(9, new java.sql.Date(BackOfficeFormatUtils.getDate(inputDTO.get("INVOICE_DATE"), context.getDateFormat()).getTime()));
			ResultSet rset = util.executeQuery();
			inputDTO.setObject("RESULT_SET", rset);
			resultDTO = processDownload(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) == null)
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			dbContext.close();
		}
		return resultDTO;
	}
}
