package patterns.config.web.forms.inv;

import java.sql.ResultSet;
import java.util.ArrayList;

import patterns.config.delegate.BackOfficeProcessManager;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.process.TBAProcessResult;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTDObject;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.framework.web.DHTMLXGridUtility;
import patterns.config.framework.web.GenericOption;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.MasterValidator;
/*
 * The program will be used to send Email generated Invoice. 
 *
 Author : Pavan kumar.R
 Created Date : 03-March-2017
 Spec Reference PSD-INV-
 Modification History
 -----------------------------------------------------------------------------------------------------
 Sl.No		Modified Date	Author			Modified Changes					Version
 1		22-March-2017	Pavan Kumar R	 removed invocation of Job Class  	1.1
 -----------------------------------------------------------------------------------------------------

 */
import patterns.config.web.forms.GenericFormBean;

public class pinvemailgenbean extends GenericFormBean {

	private static final long serialVersionUID = -6219657363150466160L;
	private String invoiceCycleNumber;
	private String invoiceMonth;
	private String invoiceMonthYear;

	public String getInvoiceCycleNumber() {
		return invoiceCycleNumber;
	}

	public void setInvoiceCycleNumber(String invoiceCycleNumber) {
		this.invoiceCycleNumber = invoiceCycleNumber;
	}

	public String getInvoiceMonth() {
		return invoiceMonth;
	}

	public void setInvoiceMonth(String invoiceMonth) {
		this.invoiceMonth = invoiceMonth;
	}

	public String getInvoiceMonthYear() {
		return invoiceMonthYear;
	}

	public void setInvoiceMonthYear(String invoiceMonthYear) {
		this.invoiceMonthYear = invoiceMonthYear;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void validate() {
	}

	@Override
	public void reset() {
		invoiceCycleNumber = RegularConstants.EMPTY_STRING;
		invoiceMonth = RegularConstants.EMPTY_STRING;
		invoiceMonthYear = RegularConstants.EMPTY_STRING;
		DBContext dbContext = new DBContext();
		ArrayList<GenericOption> invMonths = null;
		invMonths = getGenericOptions("COMMON", "MONTHS", dbContext, "--");
		webContext.getRequest().setAttribute("COMMON_MONTHS", invMonths);
		dbContext.close();
		setProcessBO("patterns.config.framework.bo.inv.pinvemailgenBO");

	}

	public DTObject validateInvoicesForCycleNumber(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		String invForCycleNumber = inputDTO.get("INV_CYCLE_NUMBER");
		MasterValidator validator = new MasterValidator();
		try {
			if (validator.isEmpty(invForCycleNumber)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "INV_RENTAL_DAY_FROM,INV_RENTAL_DAY_UPTO");
			resultDTO = validator.validateInvoiceCycleNumber(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
			return resultDTO;
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return resultDTO;
	}

	public DTObject validateMonth(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		String invoiceMonth = inputDTO.get("INV_MONTH");
		MasterValidator validator = new MasterValidator();
		try {
			if (validator.isEmpty(invoiceMonth)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			} else if (!validator.isCMLOVREC("COMMON", "MONTHS", invoiceMonth)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			return resultDTO;
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return resultDTO;
	}

	public DTObject validateYear(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		String invoiceYear = inputDTO.get("INV_YEAR");
		MasterValidator validator = new MasterValidator();
		try {
			if (validator.isEmpty(invoiceYear)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			if (!validator.isValidYear((invoiceYear))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_YEAR);
				return resultDTO;
			}
			return resultDTO;
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return resultDTO;
	}

	public DTObject validateFields(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		inputDTO.set("ERROR_PRESENT", "0");
		try {
			resultDTO = validateMonth(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				inputDTO.set("INV_MONTH_ERROR", getErrorResource(resultDTO.get(ContentManager.ERROR), null));
				inputDTO.set("ERROR_PRESENT", "1");
			}
			resultDTO = validateYear(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				inputDTO.set("INV_YEAR_ERROR", getErrorResource(resultDTO.get(ContentManager.ERROR), null));
				inputDTO.set("ERROR_PRESENT", "1");
			}
			resultDTO = validateInvoicesForCycleNumber(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				inputDTO.set("INVOICECYCLENUMBER_ERROR", getErrorResource(resultDTO.get(ContentManager.ERROR), null));
				inputDTO.set("ERROR_PRESENT", "1");
			}
			if (inputDTO.get("ERROR_PRESENT").equals("0")) {
				inputDTO.set("INV_RENTAL_DAY_FROM", resultDTO.get("INV_RENTAL_DAY_FROM"));
				inputDTO.set("INV_RENTAL_DAY_UPTO", resultDTO.get("INV_RENTAL_DAY_UPTO"));
			}
			return inputDTO;
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {

		}
		return inputDTO;
	}

	public DTObject returnFromDateAndUpDate(DTObject inputDTO) {
		try {
			StringBuffer fromDate = new StringBuffer();
			StringBuffer upToDate = new StringBuffer();
			if (inputDTO.get("INV_RENTAL_DAY_FROM").trim().length() == 1)
				fromDate.append(RegularConstants.ZERO).append(inputDTO.get("INV_RENTAL_DAY_FROM"));
			else
				fromDate.append(inputDTO.get("INV_RENTAL_DAY_FROM"));
			fromDate.append("-");
			if (inputDTO.get("INV_MONTH").trim().length() == 1)
				fromDate.append(RegularConstants.ZERO).append(inputDTO.get("INV_MONTH"));
			else
				fromDate.append(inputDTO.get("INV_MONTH"));
			fromDate.append("-").append(inputDTO.get("INV_YEAR"));

			if (inputDTO.get("INV_RENTAL_DAY_UPTO").trim().length() == 1)
				upToDate.append(RegularConstants.ZERO).append(inputDTO.get("INV_RENTAL_DAY_UPTO"));
			else
				upToDate.append(inputDTO.get("INV_RENTAL_DAY_UPTO"));
			upToDate.append("-");
			if (inputDTO.get("INV_MONTH").trim().length() == 1)
				upToDate.append(RegularConstants.ZERO).append(inputDTO.get("INV_MONTH"));
			else
				upToDate.append(inputDTO.get("INV_MONTH"));
			upToDate.append("-").append(inputDTO.get("INV_YEAR"));

			inputDTO.set("FROM_DATE", fromDate.toString());
			inputDTO.set("UPTO_DATE", upToDate.toString());
			return inputDTO;
		} catch (Exception e) {
			e.printStackTrace();
			inputDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return inputDTO;
	}

	public DTObject loadGrid(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		DBContext dbContext = new DBContext();
		try {
			if (inputDTO.get("REVALIDATE").equals("1")) {
				resultDTO = validateFields(inputDTO);
				if (resultDTO.get("ERROR_PRESENT").equals("1")) {
					return resultDTO;
				} else {
					resultDTO = returnFromDateAndUpDate(inputDTO);
					inputDTO.set("FROM_DATE", resultDTO.get("FROM_DATE"));
					inputDTO.set("UPTO_DATE", resultDTO.get("UPTO_DATE"));
				}
			}
			DBUtil dbutil = dbContext.createUtilInstance();
			int recordCount = 0;
			StringBuffer sqlQuery = new StringBuffer();
			sqlQuery.append("SELECT I.LEASE_PRODUCT_CODE,I.INV_YEAR,I.INV_MONTH,I.INV_SERIAL,I.BILLING_BRANCH_CODE,");
			sqlQuery.append(" TO_CHAR(I.INV_DATE,?) AS TO_INV_DATE,I.CUSTOMER_ID,I.BILLING_ADDR_SL ,I.INV_DATE AS INV_DATE ");
			sqlQuery.append(" ,CONCAT(I.LEASE_PRODUCT_CODE,'|',I.INV_YEAR,'|',I.INV_MONTH,'|',I.INV_SERIAL,'|') AS INVPK ");
			sqlQuery.append(" ,(CASE WHEN I.PDF_REPORT_IDENTIFIER IS NULL THEN '' ELSE I.PDF_REPORT_IDENTIFIER END ) AS PDF_REPORT_IDENTIFIER ,(CASE WHEN I.EXCEL_REPORT_IDENTIFIER IS NULL THEN '' ELSE I.EXCEL_REPORT_IDENTIFIER END ) AS EXCEL_REPORT_IDENTIFIER ");
			sqlQuery.append(" ,(CASE WHEN I.PDF_FILE_INV_NUM IS NULL THEN '' ELSE I.PDF_FILE_INV_NUM END ) AS PDF_FILE_INV_NUM ,(CASE WHEN I.EXCEL_FILE_INV_NUM IS NULL THEN '' ELSE I.EXCEL_FILE_INV_NUM END ) AS EXCEL_FILE_INV_NUM ");
			sqlQuery.append(" ,C.CUSTOMER_NAME,CM.LOV_LABEL_EN_US,FN_FORMAT_AMOUNT(I.TOTAL_INV_AMT,I.INV_CCY,CU.SUB_CCY_UNITS_IN_CCY) AS TOTAL_INV_AMT,L.REQUESTED_BY,TO_CHAR(L.REQUESTED_DATETIME , ?) AS REQUESTED_DATETIME");
			sqlQuery.append(" ,CASE E.STATUS WHEN 'S' THEN 'Success' WHEN 'F' THEN 'Failure' WHEN 'I' THEN 'In Process' END AS STATUS ");

			sqlQuery.append(" FROM INVOICES I INNER JOIN CUSTOMER C ON (I.ENTITY_CODE=C.ENTITY_CODE AND I.CUSTOMER_ID=C.CUSTOMER_ID)");
			sqlQuery.append(" INNER JOIN CMLOVREC CM ON (CM.PGM_ID='COMMON' AND CM.LOV_ID='INVOICE_TYPE' AND CM.LOV_VALUE=I.INV_TYPE) ");
			sqlQuery.append(" INNER JOIN CURRENCY CU ON (CU.CCY_CODE=I.INV_CCY)");
			sqlQuery.append(" LEFT OUTER JOIN INVOICEEMAILSENDLOG L ON (I.ENTITY_CODE=L.ENTITY_CODE AND I.LEASE_PRODUCT_CODE=L.LEASE_PRODUCT_CODE AND I.INV_YEAR=L.INV_YEAR AND I.INV_MONTH=L.INV_MONTH AND I.INV_SERIAL=L.INV_SERIAL ");
			sqlQuery.append(" AND L.DTL_SL= (SELECT MAX(DTL_SL) FROM INVOICEEMAILSENDLOG WHERE I.ENTITY_CODE=ENTITY_CODE AND I.LEASE_PRODUCT_CODE=LEASE_PRODUCT_CODE AND I.INV_YEAR=INV_YEAR AND I.INV_MONTH=INV_MONTH AND I.INV_SERIAL=INV_SERIAL ))");
			sqlQuery.append(" LEFT OUTER JOIN EMAILOUTQLOG E ON (E.ENTITY_CODE=I.ENTITY_CODE AND E.INVENTORY_NO=L.EMAIL_LOG_INVENTORY_NO ) ");
			sqlQuery.append(" WHERE I.ENTITY_CODE=? AND I.INV_YEAR=? AND I.INV_MONTH=? AND I.INV_FOR_CYCLE_NUMBER=? AND I.INV_DATE BETWEEN ? AND ?  AND (IS_PDF_GENERATED='1' OR IS_EXCEL_GENERATED='1')  ");
			dbutil.reset();
			dbutil.setMode(DBUtil.PREPARED);
			dbutil.setSql(sqlQuery.toString());

			dbutil.setString(1, context.getDateFormat());
			dbutil.setString(2, context.getDateFormat() + " " + RegularConstants.TIME_FORMAT);
			dbutil.setLong(3, Long.parseLong(context.getEntityCode()));
			dbutil.setInt(4, Integer.parseInt(inputDTO.get("INV_YEAR")));
			dbutil.setInt(5, Integer.parseInt(inputDTO.get("INV_MONTH")));
			dbutil.setInt(6, Integer.parseInt(inputDTO.get("INV_CYCLE_NUMBER")));
			dbutil.setDate(7, new java.sql.Date(BackOfficeFormatUtils.getDate(inputDTO.get("FROM_DATE"), BackOfficeConstants.JAVA_DATE_FORMAT).getTime()));
			dbutil.setDate(8, new java.sql.Date(BackOfficeFormatUtils.getDate(inputDTO.get("UPTO_DATE"), BackOfficeConstants.JAVA_DATE_FORMAT).getTime()));
			ResultSet rsetGrid = dbutil.executeQuery();
			DHTMLXGridUtility gridUtility = new DHTMLXGridUtility();
			gridUtility.init();
			while (rsetGrid.next()) {
				recordCount = recordCount + 1;
				gridUtility.startRow(String.valueOf(recordCount));
				gridUtility.setCell("0");
				gridUtility.setCell(String.valueOf(recordCount));
				gridUtility.setCell(rsetGrid.getString("BILLING_BRANCH_CODE"));
				gridUtility.setCell("" + rsetGrid.getString("LEASE_PRODUCT_CODE") + RegularConstants.PK_SEPARATOR + rsetGrid.getString("INV_SERIAL") + "^javascript:viewInvDetails(\"" + rsetGrid.getString("INVPK") + "\")");
				gridUtility.setCell("" + rsetGrid.getString("CUSTOMER_NAME") + "^javascript:viewCustomerDetails(\"" + rsetGrid.getString("CUSTOMER_ID") + "\")");
				gridUtility.setCell(rsetGrid.getString("LOV_LABEL_EN_US"));
				gridUtility.setCell(rsetGrid.getString("TOTAL_INV_AMT"));
				if (rsetGrid.getString("REQUESTED_BY") != null)
					gridUtility.setCell("" + rsetGrid.getString("REQUESTED_BY") + "^javascript:viewSentDetails(\"" + rsetGrid.getString("INVPK") + rsetGrid.getString("TO_INV_DATE") + RegularConstants.PK_SEPARATOR + rsetGrid.getString("CUSTOMER_NAME") + RegularConstants.PK_SEPARATOR + "\")");
				else
					gridUtility.setCell("");
				if (rsetGrid.getString("REQUESTED_DATETIME") != null)
					gridUtility.setCell("" + rsetGrid.getString("REQUESTED_DATETIME") + "^javascript:viewSentDetails(\"" + rsetGrid.getString("INVPK") + rsetGrid.getString("TO_INV_DATE") + RegularConstants.PK_SEPARATOR + rsetGrid.getString("CUSTOMER_NAME") + RegularConstants.PK_SEPARATOR + "\")");
				else
					gridUtility.setCell("");
				gridUtility.setCell(rsetGrid.getString("STATUS"));
				gridUtility.setCell(rsetGrid.getString("TO_INV_DATE"));
				gridUtility.setCell(rsetGrid.getString("CUSTOMER_ID"));
				gridUtility.setCell(rsetGrid.getString("PDF_REPORT_IDENTIFIER"));
				gridUtility.setCell(rsetGrid.getString("EXCEL_REPORT_IDENTIFIER"));
				gridUtility.setCell(rsetGrid.getString("BILLING_ADDR_SL"));
				gridUtility.setCell(rsetGrid.getString("LEASE_PRODUCT_CODE"));
				gridUtility.setCell(rsetGrid.getString("INV_YEAR"));
				gridUtility.setCell(rsetGrid.getString("INV_MONTH"));
				gridUtility.setCell(rsetGrid.getString("INV_SERIAL"));
				gridUtility.setCell(rsetGrid.getString("CUSTOMER_NAME"));
				gridUtility.setCell(rsetGrid.getString("PDF_FILE_INV_NUM"));
				gridUtility.setCell(rsetGrid.getString("EXCEL_FILE_INV_NUM"));
				gridUtility.endRow();
			}
			if (recordCount != 0) {
				gridUtility.finish();
				String resultXML = gridUtility.getXML();
				resultDTO.set(ContentManager.RESULT_XML, resultXML);
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				resultDTO.set("FROM_DATE", inputDTO.get("FROM_DATE"));
				resultDTO.set("UPTO_DATE", inputDTO.get("UPTO_DATE"));
			} else {
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
				return resultDTO;
			}
			return resultDTO;
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			dbContext.close();
		}
		return resultDTO;
	}

	public DTObject processAction(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		try {
			setProcessBO("patterns.config.framework.bo.inv.pinvemailgenBO");
			DTObject formDTO = getFormDTO();
			formDTO.set("ENTITY_CODE", context.getEntityCode());
			formDTO.set("USER_ID", context.getUserID());
			formDTO.set("FROM_DATE", inputDTO.get("FROM_DATE"));
			formDTO.set("UPTO_DATE", inputDTO.get("UPTO_DATE"));
			formDTO.setObject("CBD", context.getCurrentBusinessDate().getTime());
			formDTO.setDateFormat(context.getDateFormat());
			DTDObject dtdObject = new DTDObject();
			dtdObject.addColumn(0, "SELECT");
			dtdObject.addColumn(1, "SL");
			dtdObject.addColumn(2, "BRANCH");
			dtdObject.addColumn(3, "INVOICE_NO_LINK");
			dtdObject.addColumn(4, "CUSTOMER_NAME_LINK");
			dtdObject.addColumn(5, "TAXORRETAILS");
			dtdObject.addColumn(6, "AMOUNT");
			dtdObject.addColumn(7, "EMAIL_REQUESTED_BY");
			dtdObject.addColumn(8, "EMAIL_REQUESTED_ON");
			dtdObject.addColumn(9, "EMAIL_STATUS");
			dtdObject.addColumn(10, "INV_DATE");
			dtdObject.addColumn(11, "CUSTOMER_ID");
			dtdObject.addColumn(12, "PDF_REPORT_IDENTIFIER");
			dtdObject.addColumn(13, "EXCEL_REPORT_IDENTIFIER");
			dtdObject.addColumn(14, "BILLING_ADDR_SL");
			dtdObject.addColumn(15, "LEASE_PRODUCT_CODE");
			dtdObject.addColumn(16, "INV_YEAR");
			dtdObject.addColumn(17, "INV_MONTH");
			dtdObject.addColumn(18, "INVOICE_NO");
			dtdObject.addColumn(19, "CUSTOMER_NAME");
			dtdObject.addColumn(20, "PDF_FILE_INV_NUM");
			dtdObject.addColumn(21, "EXCEL_FILE_INV_NUM");

			dtdObject.setDateFormat(context.getDateFormat());
			dtdObject.setXML(inputDTO.get("XML"));
			formDTO.setDTDObject("PINVEMAILGENDTL", dtdObject);
			formDTO.set(RegularConstants.PROCESSBO_CLASS, getProcessBO());
			BackOfficeProcessManager processManager = new BackOfficeProcessManager();
			TBAProcessResult processResult = processManager.delegate(formDTO);
			if (processResult.getProcessStatus().equals(TBAProcessStatus.SUCCESS)) {
				resultDTO = loadGrid(inputDTO);
				return resultDTO;
			} else {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return resultDTO;
	}

	public DTObject viewSentDetails(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		DBContext dbContext = new DBContext();
		try {
			DBUtil dbutil = dbContext.createUtilInstance();
			int recordCount = 0;
			StringBuffer sqlQuery = new StringBuffer();
			sqlQuery.append("SELECT L.REQUESTED_BY,TO_CHAR(L.REQUESTED_DATETIME , ?) AS TO_REQUESTED_DATETIME");
			sqlQuery.append(",TO_CHAR(E.SENT_DATE , ?) AS SENT_DATE");
			sqlQuery.append(",CASE E.STATUS WHEN 'S' THEN 'Success' WHEN 'F' THEN 'Failure' WHEN 'I' THEN 'In Process' END AS STATUS,E.INVENTORY_NO ");
			sqlQuery.append(" FROM INVOICEEMAILSENDLOG L ");
			sqlQuery.append(" INNER JOIN EMAILOUTQLOG E ON (E.ENTITY_CODE=L.ENTITY_CODE AND E.INVENTORY_NO=L.EMAIL_LOG_INVENTORY_NO)");
			sqlQuery.append(" WHERE L.ENTITY_CODE=? AND L.LEASE_PRODUCT_CODE=? AND L.INV_YEAR=? AND L.INV_MONTH=? AND L.INV_SERIAL=? ORDER BY L.REQUESTED_DATETIME DESC");
			dbutil.reset();
			dbutil.setMode(DBUtil.PREPARED);
			dbutil.setSql(sqlQuery.toString());
			dbutil.setString(1, context.getDateFormat() + " " + RegularConstants.TIME_FORMAT);
			dbutil.setString(2, context.getDateFormat() + " " + RegularConstants.TIME_FORMAT);
			dbutil.setLong(3, Long.parseLong(context.getEntityCode()));
			dbutil.setString(4, inputDTO.get("LEASE_PRODUCT_CODE"));
			dbutil.setInt(5, Integer.parseInt(inputDTO.get("INV_YEAR")));
			dbutil.setInt(6, Integer.parseInt(inputDTO.get("INV_MONTH")));
			dbutil.setInt(7, Integer.parseInt(inputDTO.get("INV_SERIAL")));
			ResultSet rsetGrid = dbutil.executeQuery();
			DHTMLXGridUtility gridUtility = new DHTMLXGridUtility();
			gridUtility.init();
			while (rsetGrid.next()) {
				recordCount = recordCount + 1;
				gridUtility.startRow(String.valueOf(recordCount));
				gridUtility.setCell(String.valueOf(recordCount));
				gridUtility.setCell(rsetGrid.getString("REQUESTED_BY"));
				gridUtility.setCell(rsetGrid.getString("TO_REQUESTED_DATETIME"));
				gridUtility.setCell(rsetGrid.getString("SENT_DATE"));
				gridUtility.setCell(rsetGrid.getString("STATUS"));
				gridUtility.setCell("View^javascript:viewMailDetails(\"" + rsetGrid.getString("INVENTORY_NO") + "\")");
				gridUtility.endRow();
			}
			if (recordCount != 0) {
				gridUtility.finish();
				String resultXML = gridUtility.getXML();
				resultDTO.set(ContentManager.RESULT_XML, resultXML);
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
			} else {
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
				return resultDTO;
			}
			dbutil.reset();
			return resultDTO;
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			dbContext.close();
		}
		return resultDTO;
	}

	public DTObject viewMailDetails(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		DBContext dbContext = new DBContext();
		try {
			DBUtil dbutil = dbContext.createUtilInstance();
			dbutil.reset();
			dbutil.setMode(DBUtil.PREPARED);
			dbutil.setSql("SELECT EMAIL_SUBJECT,EMAIL_TEXT,REQ_DATE,TO_CHAR(SENT_DATE , ?) AS SENT_DATE FROM EMAILOUTQLOG WHERE ENTITY_CODE=? AND INVENTORY_NO=?");
			dbutil.setString(1, context.getDateFormat() + " " + RegularConstants.TIME_FORMAT);
			dbutil.setString(2, context.getEntityCode());
			dbutil.setString(3, inputDTO.get("EMAIL_LOG_INVENTORY_NO"));
			ResultSet rt = dbutil.executeQuery();
			if (rt.next()) {
				resultDTO.set("EMAIL_SUBJECT", rt.getString("EMAIL_SUBJECT"));
				resultDTO.set("EMAIL_TEXT", rt.getString("EMAIL_TEXT"));
				resultDTO.set("SENT_DATE", rt.getString("SENT_DATE"));
			}
			StringBuffer sqlQuery = new StringBuffer();
			sqlQuery.append("SELECT R.REPORT_IDENTIFIER,E.ATTACHMENT_NAME FROM EMAILOUTQATTACH E ");
			sqlQuery.append(" INNER JOIN REPORTDOWNLOAD R ON (R.ENTITY_CODE=E.ENTITY_CODE AND R.FILE_PATH=CONCAT(E.ATTACHMENT_PATH,E.ATTACHMENT_NAME))");
			sqlQuery.append(" WHERE E.ENTITY_CODE=? AND E.INVENTORY_NO=? ");
			dbutil.reset();
			dbutil.setMode(DBUtil.PREPARED);
			dbutil.setSql(sqlQuery.toString());
			dbutil.setString(1, context.getEntityCode());
			dbutil.setString(2, inputDTO.get("EMAIL_LOG_INVENTORY_NO"));
			ResultSet rset = dbutil.executeQuery();
			while (rset.next()) {
				if (rset.getString("ATTACHMENT_NAME").toUpperCase().contains(".PDF")) {
					resultDTO.set("PDF_REPORT_FILE_NAME", rset.getString("ATTACHMENT_NAME"));
					resultDTO.set("PDF_REPORT_IDENTIFIER", rset.getString("REPORT_IDENTIFIER"));
				} else {
					resultDTO.set("EXCEL_REPORT_FILE_NAME", rset.getString("ATTACHMENT_NAME"));
					resultDTO.set("EXCEL_REPORT_IDENTIFIER", rset.getString("REPORT_IDENTIFIER"));
				}
			}
			int recordCount = 0;
			dbutil.reset();
			dbutil.setMode(DBUtil.PREPARED);
			dbutil.setSql("SELECT E.EMAIL_RCP_TYPE,E.EMAIL_ID FROM EMAILOUTQRCP E WHERE E.ENTITY_CODE=? AND E.INVENTORY_NO=? ORDER BY E.EMAIL_RCP_TYPE DESC");
			dbutil.setString(1, context.getEntityCode());
			dbutil.setString(2, inputDTO.get("EMAIL_LOG_INVENTORY_NO"));
			ResultSet rsetGrid = dbutil.executeQuery();
			DHTMLXGridUtility gridUtility = new DHTMLXGridUtility();
			gridUtility.init();
			while (rsetGrid.next()) {
				recordCount = recordCount + 1;
				gridUtility.startRow(String.valueOf(recordCount));
				gridUtility.setCell(rsetGrid.getString("EMAIL_ID"));
				if (rsetGrid.getString("EMAIL_RCP_TYPE").equals("T"))
					gridUtility.setCell("To");
				else
					gridUtility.setCell("Cc");
				gridUtility.endRow();
			}
			if (recordCount != 0) {
				gridUtility.finish();
				String resultXML = gridUtility.getXML();
				resultDTO.set(ContentManager.RESULT_XML, resultXML);
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
			} else {
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
				return resultDTO;
			}
			dbutil.reset();
			return resultDTO;
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			dbContext.close();
		}
		return resultDTO;
	}

}
