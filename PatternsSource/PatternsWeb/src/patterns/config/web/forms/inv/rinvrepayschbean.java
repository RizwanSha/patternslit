package patterns.config.web.forms.inv;

import java.sql.ResultSet;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.framework.web.reports.ReportUtils;
import patterns.config.validations.LeaseValidator;
import patterns.config.validations.MasterValidator;
import patterns.config.validations.RegistrationValidator;
import patterns.config.web.forms.GenericFormBean;

public class rinvrepayschbean extends GenericFormBean {

	private static final long serialVersionUID = -6219657363150466160L;
	private String lesseeCode;
	private String agreementNo;
	private String scheduleId;
	private String custIDHidden;

	public String getLesseeCode() {
		return lesseeCode;
	}

	public void setLesseeCode(String lesseeCode) {
		this.lesseeCode = lesseeCode;
	}

	public String getAgreementNo() {
		return agreementNo;
	}

	public void setAgreementNo(String agreementNo) {
		this.agreementNo = agreementNo;
	}

	public String getScheduleId() {
		return scheduleId;
	}

	public void setScheduleId(String scheduleId) {
		this.scheduleId = scheduleId;
	}

	public String getCustIDHidden() {
		return custIDHidden;
	}

	public void setCustIDHidden(String custIDHidden) {
		this.custIDHidden = custIDHidden;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void validate() {
	}

	@Override
	public void reset() {
		lesseeCode = RegularConstants.EMPTY_STRING;
		agreementNo = RegularConstants.EMPTY_STRING;
		scheduleId = RegularConstants.EMPTY_STRING;
		custIDHidden = RegularConstants.EMPTY_STRING;
	}

	public DTObject validateLesseeCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		MasterValidator validation = new MasterValidator();
		RegistrationValidator validatior = new RegistrationValidator();
		DBContext dbContext = new DBContext();
		try {
			inputDTO.set(ContentManager.FETCH_COLUMNS, "SUNDRY_DB_AC");
			resultDTO = getCustomerLesseeCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
			custIDHidden = resultDTO.get("CUSTOMER_ID");
			inputDTO.set("CUSTOMER_ID", custIDHidden);
			inputDTO.set(ContentManager.FETCH_COLUMNS, "CUSTOMER_ID,CUSTOMER_NAME");
			resultDTO = validatior.valildateCustomerID(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			} else if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
				resultDTO.set("CUSTOMER_ID", custIDHidden);
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
			validatior.close();
			dbContext.close();
		}
		return resultDTO;
	}

	public DTObject getCustomerLesseeCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		try {
			String sqlQuery = "SELECT CUSTOMER_ID FROM CUSTOMERACDTL WHERE ENTITY_CODE=? AND SUNDRY_DB_AC=?";
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, context.getEntityCode());
			util.setString(2, inputDTO.get("SUNDRY_DB_AC"));
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				resultDTO.set("CUSTOMER_ID", rset.getString("CUSTOMER_ID"));
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			util.reset();
			dbContext.close();
		}
		return resultDTO;
	}

	public DTObject validateAgreementNo(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		LeaseValidator validation = new LeaseValidator();
		DBContext dbContext = new DBContext();
		try {
			inputDTO.set(ContentManager.FETCH_COLUMNS, "AGREEMENT_NO");
			resultDTO = validation.validateCustomerAgreement(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
			dbContext.close();
		}
		return resultDTO;
	}

	public DTObject validateScheduleID(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		LeaseValidator validation = new LeaseValidator();
		DBContext dbContext = new DBContext();
		try {
			inputDTO.set(ContentManager.FETCH_COLUMNS, "SCHEDULE_ID,TO_CHAR(AUTH_ON,'" + context.getDateFormat() + "') AUTH_ON,LEASE_CLOSURE_ON");
			resultDTO = validation.validateScheduleID(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != RegularConstants.NULL) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
			if (resultDTO.get("AUTH_ON") == RegularConstants.NULL) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNAUTH_LEASE_DETAILS);//
				return resultDTO;
			}
			if (resultDTO.get("LEASE_CLOSURE_ON") != RegularConstants.NULL) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.LEASE_CLOSED);//
				return resultDTO;
			}

		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
			dbContext.close();
		}
		return resultDTO;
	}

	public DTObject generateReport(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		try {
			inputDTO.set(ContentManager.REPORT_HANDLER, "patterns.config.web.reports.inv.rinvrepayschreport");
			if (inputDTO.get("REPORT_FORMAT") != null && !inputDTO.get("REPORT_FORMAT").equals(""))
				inputDTO.set(ContentManager.REPORT_FORMAT, inputDTO.get("REPORT_FORMAT"));
			else
				inputDTO.set(ContentManager.REPORT_FORMAT, ReportUtils.EXPORT_PDF + "");
			resultDTO = processDownload(inputDTO);
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return resultDTO;
	}

}
