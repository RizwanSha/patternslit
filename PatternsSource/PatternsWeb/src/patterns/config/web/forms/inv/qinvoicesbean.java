package patterns.config.web.forms.inv;
import java.sql.ResultSet;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.DHTMLXGridUtility;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.MasterValidator;
import patterns.config.web.forms.GenericFormBean;
public class qinvoicesbean extends GenericFormBean {
	private static final long serialVersionUID = 1L;
	@Override
	public void reset() {
		// TODO Auto-generated method stub
	}
	@Override
	public void validate() {
		// TODO Auto-generated method stub
	}
	public DTObject getInvoiceDetails(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		MasterValidator validation = new MasterValidator();
		try {
			DBUtil dbutil = validation.getDbContext().createUtilInstance();
			StringBuffer invoiceLogCountQuery = new StringBuffer();
			invoiceLogCountQuery.append("SELECT CONCAT(INVOICES.LEASE_PRODUCT_CODE,'/',TO_CHAR(CONCAT(INVOICES.INV_YEAR,'-',INVOICES.INV_MONTH,'-',01),'%b%y'),'/',INVOICES.INV_SERIAL) INVOICE_NO, ");
			invoiceLogCountQuery.append("INVOICES.LEASE_PRODUCT_CODE,TO_CHAR(INVOICES.INV_DATE, ?) INV_DATE,TO_CHAR(INVOICES.DUE_DATE, ?) DUE_DATE,INVOICES.CUSTOMER_ID,INVOICES.INV_TYPE, ");
			invoiceLogCountQuery.append("INVOICES.STATE_CODE,INVOICES.INVOICE_TEMPLATE,INVOICES.TOTAL_INV,TO_CHAR(INVOICES.RENTAL_START_DATE, ?) RENTAL_START_DATE, ");
			invoiceLogCountQuery.append("TO_CHAR(INVOICES.RENTAL_END_DATE, ?) RENTAL_END_DATE,FN_FORMAT_AMOUNT(INVOICES.TAX_AMT,INVOICES.INV_CCY,C.SUB_CCY_UNITS_IN_CCY) TAX_AMT,FN_FORMAT_AMOUNT(INVOICES.TOTAL_INV_AMT,INVOICES.INV_CCY,C.SUB_CCY_UNITS_IN_CCY) TOTAL_INV_AMT,INVOICES.CREATED_BY,");
			invoiceLogCountQuery.append("TO_CHAR(INVOICES.CREATED_ON, ?) CREATED_ON,");
			invoiceLogCountQuery.append("INVOICES.IS_PDF_GENERATED,INVOICES.PDF_REPORT_IDENTIFIER,INVOICES.EXCEL_REPORT_IDENTIFIER,");
			invoiceLogCountQuery.append("LEASEPRODUCT.DESCRIPTION PRODUCT_DESC,CUSTOMER.CUSTOMER_NAME,STATE.DESCRIPTION STATE_DESC,");
			invoiceLogCountQuery.append(" INVTEMPLATECODE.DESCRIPTION INVTEMPLATECODE_DESC,COUNT(INVOICEEMAILSENDLOG.DTL_SL) EMAIL_SL,INVOICES.INV_CCY ");
			invoiceLogCountQuery.append(" FROM INVOICES INVOICES ");
			invoiceLogCountQuery.append(" JOIN LEASEPRODUCT LEASEPRODUCT ON (INVOICES.ENTITY_CODE = LEASEPRODUCT.ENTITY_CODE AND INVOICES.LEASE_PRODUCT_CODE = LEASEPRODUCT.LEASE_PRODUCT_CODE) ");
			invoiceLogCountQuery.append(" LEFT OUTER JOIN CUSTOMER CUSTOMER ON (INVOICES.ENTITY_CODE = CUSTOMER.ENTITY_CODE AND INVOICES.CUSTOMER_ID = CUSTOMER.CUSTOMER_ID) ");
			invoiceLogCountQuery.append(" LEFT OUTER JOIN STATE STATE ON (INVOICES.ENTITY_CODE = STATE.ENTITY_CODE AND INVOICES.STATE_CODE = STATE.STATE_CODE) ");
			invoiceLogCountQuery.append(" LEFT OUTER JOIN INVTEMPLATECODE INVTEMPLATECODE ON (INVOICES.ENTITY_CODE = INVTEMPLATECODE.ENTITY_CODE AND INVOICES.INVOICE_TEMPLATE = INVTEMPLATECODE.INVTEMPLATE_CODE) ");
			invoiceLogCountQuery.append(" LEFT OUTER JOIN CURRENCY C ON(C.CCY_CODE=INVOICES.TAX_AMT)  ");
			invoiceLogCountQuery.append(" LEFT OUTER JOIN INVOICEEMAILSENDLOG INVOICEEMAILSENDLOG ON (INVOICES.ENTITY_CODE = INVOICEEMAILSENDLOG.ENTITY_CODE) ");
			invoiceLogCountQuery.append(" WHERE INVOICES.ENTITY_CODE = ?  AND  INVOICES.LEASE_PRODUCT_CODE =?  AND  INVOICES.INV_YEAR = ? AND  INVOICES.INV_MONTH =?  AND  INVOICES.INV_SERIAL = ?");
			dbutil.reset();
			dbutil.setMode(DBUtil.PREPARED);
			dbutil.setSql(invoiceLogCountQuery.toString());
			dbutil.setString(1, context.getDateFormat());
			dbutil.setString(2, context.getDateFormat());
			dbutil.setString(3, context.getDateFormat());
			dbutil.setString(4, context.getDateFormat());
			dbutil.setString(5, context.getDateFormat() + " " + RegularConstants.TIME_FORMAT);
			dbutil.setLong(6, Long.parseLong(context.getEntityCode()));
			dbutil.setString(7, inputDTO.get("LEASE_PRODUCT_CODE"));
			dbutil.setInt(8, Integer.parseInt(inputDTO.get("INV_YEAR")));
			dbutil.setInt(9, Integer.parseInt(inputDTO.get("INV_MONTH")));
			dbutil.setInt(10, Integer.parseInt(inputDTO.get("INV_SERIAL")));
			ResultSet rset = dbutil.executeQuery();
			if (rset.next()) {
				resultDTO.set("INVOICE_NO", rset.getString("INVOICE_NO"));
				resultDTO.set("LEASE_PRODUCT_CODE", rset.getString("LEASE_PRODUCT_CODE"));
				resultDTO.set("INV_DATE", rset.getString("INV_DATE"));
				resultDTO.set("DUE_DATE", rset.getString("DUE_DATE"));
				resultDTO.set("CUSTOMER_ID", rset.getString("CUSTOMER_ID"));
				resultDTO.set("INV_TYPE", rset.getString("INV_TYPE"));
				resultDTO.set("STATE_CODE", rset.getString("STATE_CODE"));
				resultDTO.set("INVOICE_TEMPLATE", rset.getString("INVOICE_TEMPLATE"));
				resultDTO.set("TOTAL_INV", rset.getString("TOTAL_INV"));
				resultDTO.set("RENTAL_START_DATE", rset.getString("RENTAL_START_DATE"));
				resultDTO.set("RENTAL_END_DATE", rset.getString("RENTAL_END_DATE"));
				resultDTO.set("TAX_AMT", rset.getString("TAX_AMT"));
				resultDTO.set("TOTAL_INV_AMT", rset.getString("TOTAL_INV_AMT"));
				resultDTO.set("CREATED_BY", rset.getString("CREATED_BY"));
				resultDTO.set("CREATED_ON", rset.getString("CREATED_ON"));
				resultDTO.set("IS_PDF_GENERATED", rset.getString("IS_PDF_GENERATED"));
				resultDTO.set("PDF_REPORT_IDENTIFIER", rset.getString("PDF_REPORT_IDENTIFIER"));
				resultDTO.set("EXCEL_REPORT_IDENTIFIER", rset.getString("EXCEL_REPORT_IDENTIFIER"));
				resultDTO.set("INVOICE_NO", rset.getString("INVOICE_NO"));
				resultDTO.set("PRODUCT_DESC", rset.getString("PRODUCT_DESC"));
				resultDTO.set("CUSTOMER_NAME", rset.getString("CUSTOMER_NAME"));
				resultDTO.set("STATE_DESC", rset.getString("STATE_DESC"));
				resultDTO.set("INV_CCY", rset.getString("INV_CCY"));
				
				
				resultDTO.set("INVTEMPLATECODE_DESC", rset.getString("INVTEMPLATECODE_DESC"));
				if (rset.getInt("EMAIL_SL") > 0)
					resultDTO.set("EMAIL_SENT", RegularConstants.ONE);
				else
					resultDTO.set("EMAIL_SENT", RegularConstants.ZERO);
			}
			int recordCount = 0;
			StringBuffer sqlQuery = new StringBuffer();
			sqlQuery.append("SELECT B.BRKUP_SL,CONCAT(TO_CHAR(B.COMP_ENTRY_DATE,?), '|', B.COMP_ENTRY_SL) AS COMP_ENTRY_DATE,B.LESSEE_CODE,");
			sqlQuery.append(" B.AGREEMENT_NO,B.SCHEDULE_ID,C.INV_FOR_INTEREST,C.INV_FOR_INTIAL_INTERIM, ");
			sqlQuery.append(" C.INV_FOR_RENTAL,C.INV_FOR_FINAL_INTERIM,C.INV_AMOUNT,B.STATUS FROM INVOICEBREAKUP B ");
			sqlQuery.append(" LEFT OUTER JOIN INVCOMPONENT C ON (C.ENTITY_CODE = B.ENTITY_CODE AND C.LEASE_PRODUCT_CODE = B.LEASE_PRODUCT_CODE");
			sqlQuery.append(" AND C.INV_YEAR = B.INV_YEAR AND C.INV_MONTH = B.INV_MONTH)");
			sqlQuery.append(" WHERE B.ENTITY_CODE=? AND B.LEASE_PRODUCT_CODE=? AND B.INV_YEAR=? AND B.INV_MONTH=? AND B.INV_SERIAL=?");
			dbutil.reset();
			dbutil.setMode(DBUtil.PREPARED);
			dbutil.setSql(sqlQuery.toString());
			dbutil.setString(1, context.getDateFormat());
			dbutil.setInt(2, Integer.parseInt(context.getEntityCode()));
			dbutil.setString(3, inputDTO.get("LEASE_PRODUCT_CODE"));
			dbutil.setInt(4, Integer.parseInt(inputDTO.get("INV_YEAR")));
			dbutil.setInt(5, Integer.parseInt(inputDTO.get("INV_MONTH")));
			dbutil.setInt(6, Integer.parseInt(inputDTO.get("INV_SERIAL")));
			ResultSet rsetGrid = dbutil.executeQuery();
			DHTMLXGridUtility gridUtility = new DHTMLXGridUtility();
			gridUtility.init();
			StringBuffer sb = new StringBuffer();
			while (rsetGrid.next()) {
				recordCount = recordCount + 1;
				gridUtility.startRow(String.valueOf(recordCount));
				if (rsetGrid.getString("INV_FOR_INTEREST") != null) {
					if (rsetGrid.getString("INV_FOR_INTEREST").equals("1")) {
						sb.append("Interest");
					}
					if (rsetGrid.getString("INV_FOR_INTIAL_INTERIM").equals("1")) {
						sb.append("Initial Interim");
					}
					if (rsetGrid.getString("INV_FOR_RENTAL").equals("1")) {
						sb.append("Rental");
					}
					if (rsetGrid.getString("INV_FOR_FINAL_INTERIM").equals("1")) {
						sb.append("Final Interim");
					}
				}
				gridUtility.setCell(rsetGrid.getString("BRKUP_SL"));
				gridUtility.setCell("" + rsetGrid.getString("COMP_ENTRY_DATE") + "^javascript:viewInvDetails(\"" + rsetGrid.getString("COMP_ENTRY_DATE") + "\")");
				gridUtility.setCell(rsetGrid.getString("LESSEE_CODE"));
				gridUtility.setCell(rsetGrid.getString("AGREEMENT_NO"));
				gridUtility.setCell(rsetGrid.getString("SCHEDULE_ID"));
				gridUtility.setCell(sb.toString());
				gridUtility.setCell(rsetGrid.getString("INV_FOR_INTEREST"));
				gridUtility.setCell(rsetGrid.getString("INV_FOR_INTIAL_INTERIM"));
				gridUtility.setCell(rsetGrid.getString("INV_FOR_RENTAL"));
				gridUtility.setCell(rsetGrid.getString("INV_FOR_FINAL_INTERIM"));
				gridUtility.setCell(rsetGrid.getString("INV_AMOUNT"));
				gridUtility.setCell(rsetGrid.getString("STATUS"));
				gridUtility.endRow();
			}
			if (recordCount != 0) {
				gridUtility.finish();
				String resultXML = gridUtility.getXML();
				resultDTO.set(ContentManager.RESULT_XML, resultXML);
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
			} else {
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
				return resultDTO;
			}
			return resultDTO;
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}
	public DTObject viewAddressRcp(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		MasterValidator validation = new MasterValidator();
		DBUtil dbutil = validation.getDbContext().createUtilInstance();
		try {
			if (!inputDTO.get("PDF_REPORT_IDENTIFIER").trim().equals(RegularConstants.EMPTY_STRING)) {
				StringBuffer sqlQuery = new StringBuffer();
				sqlQuery.append("SELECT R.FILE_PATH FROM REPORTDOWNLOAD R ");
				sqlQuery.append(" WHERE R.ENTITY_CODE=? AND R.REPORT_IDENTIFIER=? ");
				dbutil.reset();
				dbutil.setMode(DBUtil.PREPARED);
				dbutil.setSql(sqlQuery.toString());
				dbutil.setString(1, context.getEntityCode());
				dbutil.setString(2, inputDTO.get("PDF_REPORT_IDENTIFIER"));
				ResultSet rset = dbutil.executeQuery();
				if (rset.next()) {
					if (rset.getString("FILE_PATH") != null) {
						if (rset.getString("FILE_PATH").indexOf("\\") != -1) {
							resultDTO.set("PDF_REPORT_FILE_NAME", rset.getString("FILE_PATH").substring(rset.getString("FILE_PATH").lastIndexOf("\\") + 1, rset.getString("FILE_PATH").length()));
						} else if (rset.getString("FILE_PATH").indexOf("/") != -1) {
							resultDTO.set("PDF_REPORT_FILE_NAME", rset.getString("FILE_PATH").substring(rset.getString("FILE_PATH").lastIndexOf("/") + 1, rset.getString("FILE_PATH").length()));
						} else if (rset.getString("FILE_PATH").indexOf(":") != -1) {
							resultDTO.set("PDF_REPORT_FILE_NAME", rset.getString("FILE_PATH").substring(rset.getString("FILE_PATH").lastIndexOf(":") + 1, rset.getString("FILE_PATH").length()));
						}
					}
				}
			}
			if (!inputDTO.get("EXCEL_REPORT_IDENTIFIER").trim().equals(RegularConstants.EMPTY_STRING)) {
				StringBuffer sqlQuery = new StringBuffer();
				sqlQuery.append("SELECT R.FILE_PATH FROM REPORTDOWNLOAD R ");
				sqlQuery.append(" WHERE R.ENTITY_CODE=? AND R.REPORT_IDENTIFIER=? ");
				dbutil.reset();
				dbutil.setMode(DBUtil.PREPARED);
				dbutil.setSql(sqlQuery.toString());
				dbutil.setString(1, context.getEntityCode());
				dbutil.setString(2, inputDTO.get("EXCEL_REPORT_IDENTIFIER"));
				ResultSet rset = dbutil.executeQuery();
				if (rset.next()) {
					if (rset.getString("FILE_PATH").indexOf("\\") != -1) {
						resultDTO.set("EXCEL_REPORT_FILE_NAME", rset.getString("FILE_PATH").substring(rset.getString("FILE_PATH").lastIndexOf("\\") + 1, rset.getString("FILE_PATH").length()));
					} else if (rset.getString("FILE_PATH").indexOf("/") != -1) {
						resultDTO.set("EXCEL_REPORT_FILE_NAME", rset.getString("FILE_PATH").substring(rset.getString("FILE_PATH").lastIndexOf("/") + 1, rset.getString("FILE_PATH").length()));
					} else if (rset.getString("FILE_PATH").indexOf(":") != -1) {
						resultDTO.set("EXCEL_REPORT_FILE_NAME", rset.getString("FILE_PATH").substring(rset.getString("FILE_PATH").lastIndexOf(":") + 1, rset.getString("FILE_PATH").length()));
					}
				}
			}
			int recordCount = 0;
			dbutil.reset();
			dbutil.setMode(DBUtil.PREPARED);
			dbutil.setSql("SELECT E.EMAIL_RCP_TYPE,E.EMAIL_ID FROM EMAILOUTQRCP E WHERE E.ENTITY_CODE=? AND E.INVENTORY_NO=? ORDER BY E.EMAIL_RCP_TYPE DESC");
			dbutil.setString(1, context.getEntityCode());
			dbutil.setString(2, inputDTO.get("EMAIL_LOG_INVENTORY_NO"));
			ResultSet rsetGrid = dbutil.executeQuery();
			DHTMLXGridUtility gridUtility = new DHTMLXGridUtility();
			gridUtility.init();
			while (rsetGrid.next()) {
				recordCount = recordCount + 1;
				gridUtility.startRow(String.valueOf(recordCount));
				gridUtility.setCell(rsetGrid.getString("EMAIL_ID"));
				if (rsetGrid.getString("EMAIL_RCP_TYPE").equals("T"))
					gridUtility.setCell("To");
				else
					gridUtility.setCell("Cc");
				gridUtility.endRow();
			}
			if (recordCount != 0) {
				gridUtility.finish();
				String resultXML = gridUtility.getXML();
				resultDTO.set(ContentManager.RESULT_XML, resultXML);
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
			} else {
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
				return resultDTO;
			}
			return resultDTO;
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			dbutil.reset();
			validation.close();
		}
		return resultDTO;
	}
	public DTObject viewSentDetails(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		MasterValidator validation = new MasterValidator();
		DBUtil dbutil = validation.getDbContext().createUtilInstance();
		try {
			int recordCount = 0;
			StringBuffer sqlQuery = new StringBuffer();
			sqlQuery.append("SELECT L.REQUESTED_BY,TO_CHAR(L.REQUESTED_DATETIME , ?) AS TO_REQUESTED_DATETIME");
			sqlQuery.append(",TO_CHAR(E.SENT_DATE , ?) AS SENT_DATE");
			sqlQuery.append(",CASE E.STATUS WHEN 'S' THEN 'Success' WHEN 'F' THEN 'Failure' WHEN 'I' THEN 'In Process' END AS STATUS,E.INVENTORY_NO ");
			sqlQuery.append(" FROM INVOICEEMAILSENDLOG L ");
			sqlQuery.append(" INNER JOIN EMAILOUTQLOG E ON (E.ENTITY_CODE=L.ENTITY_CODE AND E.INVENTORY_NO=L.EMAIL_LOG_INVENTORY_NO)");
			sqlQuery.append(" WHERE L.ENTITY_CODE=? AND L.LEASE_PRODUCT_CODE=? AND L.INV_YEAR=? AND L.INV_MONTH=? AND L.INV_SERIAL=? ORDER BY L.REQUESTED_DATETIME DESC");
			dbutil.reset();
			dbutil.setMode(DBUtil.PREPARED);
			dbutil.setSql(sqlQuery.toString());
			dbutil.setString(1, context.getDateFormat() + " " + RegularConstants.TIME_FORMAT);
			dbutil.setString(2, context.getDateFormat() + " " + RegularConstants.TIME_FORMAT);
			dbutil.setLong(3, Long.parseLong(context.getEntityCode()));
			dbutil.setString(4, inputDTO.get("LEASE_PRODUCT_CODE"));
			dbutil.setInt(5, Integer.parseInt(inputDTO.get("INV_YEAR")));
			dbutil.setInt(6, Integer.parseInt(inputDTO.get("INV_MONTH")));
			dbutil.setInt(7, Integer.parseInt(inputDTO.get("INV_SERIAL")));
			ResultSet rsetGrid = dbutil.executeQuery();
			DHTMLXGridUtility gridUtility = new DHTMLXGridUtility();
			gridUtility.init();
			while (rsetGrid.next()) {
				recordCount = recordCount + 1;
				gridUtility.startRow(String.valueOf(recordCount));
				gridUtility.setCell(String.valueOf(recordCount));
				gridUtility.setCell(rsetGrid.getString("REQUESTED_BY"));
				gridUtility.setCell(rsetGrid.getString("TO_REQUESTED_DATETIME"));
				gridUtility.setCell(rsetGrid.getString("SENT_DATE"));
				gridUtility.setCell(rsetGrid.getString("STATUS"));
				gridUtility.setCell("View^javascript:viewAddressRcp(\"" + rsetGrid.getString("INVENTORY_NO") + "\")");
				gridUtility.endRow();
			}
			if (recordCount != 0) {
				gridUtility.finish();
				String resultXML = gridUtility.getXML();
				resultDTO.set(ContentManager.RESULT_XML, resultXML);
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
			} else {
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
				return resultDTO;
			}
			return resultDTO;
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			dbutil.reset();
			validation.close();
		}
		return resultDTO;
	}
}
