package patterns.config.web.forms;

import java.io.InputStream;
import java.sql.ResultSet;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;

import net.sf.jasperreports.engine.JRResultSetDataSource;
import net.sf.jasperreports.engine.JasperPrint;

import org.apache.struts.Globals;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.w3c.dom.Document;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.loggers.ApplicationLogger;
import patterns.config.framework.process.TBAActionType;
import patterns.config.framework.process.TBAProcessAction;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.thread.ApplicationContext;
import patterns.config.framework.thread.WebContext;
import patterns.config.framework.web.FormatUtils;
import patterns.config.framework.web.GenericOption;
import patterns.config.framework.web.RequestConstants;
import patterns.config.framework.web.SecurityUtils;
import patterns.config.framework.web.SessionConstants;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.framework.web.ajax.ErrorMap;
import patterns.config.framework.web.configuration.program.ProgramConfiguration;
import patterns.config.framework.web.configuration.program.ProgramMap;
import patterns.config.framework.web.reports.ReportConstants;
import patterns.config.framework.web.reports.ReportRM;
import patterns.config.framework.web.reports.ReportUtils;
import patterns.config.framework.web.reports.fw.ReportContext;
import patterns.config.framework.web.reports.fw.ReportInterface;

public abstract class GenericFormBean extends ActionForm {
	protected ApplicationLogger logger = null;

	protected final ApplicationContext context = ApplicationContext.getInstance();
	protected final WebContext webContext = WebContext.getInstance();

	public GenericFormBean() {
		logger = ApplicationLogger.getInstance(this.getClass().getSimpleName());
		// logger.logDebug("#()");
		formDTO = new DTObject();
	}

	public static final String UNAUTH_ACTION = "unauth";
	public static final String SUBMIT_ACTION = "submit";
	public static final String RESET_ACTION = "reset";
	public static final String ADD = "A";
	public static final String MODIFY = "M";
	public static final String USAGE = "U";

	private ErrorMap errorMap = new ErrorMap();

	private static final long serialVersionUID = -7495323350415417527L;

	private String command;
	private String action;
	private String actionDescription;
	private DTObject formDTO;
	private String processBO;
	private String tfaRequired;
	private String tfaSuccess;
	private String tfaValue;
	private String tfaNonce;
	private String tfaEncodedValue;
	private String csrfValue;
	private String tfaErrorCode;
	private String parentProcessId;
	private String addAllowed;
	private String modifyAllowed;
	private String viewAllowed;
	private String callSource;
	private String backToTemplate;
	private String sourceKey;
	private String primaryKey;
	private String scenario;

	public void setScenario(String scenario) {
		this.scenario = scenario;
	}

	public String getScenario() {
		return scenario;
	}

	// ARIBALA ADDED ON 09102015 BEGIN
	private String rectify;

	// ARIBALA ADDED ON 09102015 END

	public String getAddAllowed() {
		return addAllowed;
	}

	public void setAddAllowed(String addAllowed) {
		this.addAllowed = addAllowed;
	}

	public String getModifyAllowed() {
		return modifyAllowed;
	}

	public void setModifyAllowed(String modifyAllowed) {
		this.modifyAllowed = modifyAllowed;
	}

	public String getViewAllowed() {
		return viewAllowed;
	}

	public void setViewAllowed(String viewAllowed) {
		this.viewAllowed = viewAllowed;
	}

	public String getParentProcessId() {
		return parentProcessId;
	}

	public void setParentProcessId(String parentProcessId) {
		this.parentProcessId = parentProcessId;
	}

	public String getTfaErrorCode() {
		return tfaErrorCode;
	}

	public void setTfaErrorCode(String tfaErrorCode) {
		this.tfaErrorCode = tfaErrorCode;
	}

	public String getCsrfValue() {
		return csrfValue;
	}

	public void setCsrfValue(String csrfValue) {
		this.csrfValue = csrfValue;
	}

	public String getTfaEncodedValue() {
		return tfaEncodedValue;
	}

	public void setTfaEncodedValue(String tfaEncodedValue) {
		this.tfaEncodedValue = tfaEncodedValue;
	}

	public String getTfaValue() {
		return tfaValue;
	}

	public void setTfaValue(String tfaValue) {
		this.tfaValue = tfaValue;
	}

	public String getTfaNonce() {
		return tfaNonce;
	}

	public void setTfaNonce(String tfaNonce) {
		this.tfaNonce = tfaNonce;
	}

	public String getTfaSuccess() {
		return tfaSuccess;
	}

	public void setTfaSuccess(String tfaSuccess) {
		this.tfaSuccess = tfaSuccess;
	}

	public String getTfaRequired() {
		return tfaRequired;
	}

	public void setTfaRequired(String tfaRequired) {
		this.tfaRequired = tfaRequired;
	}

	public DTObject getFormDTO() {
		formDTO.setDateFormat(context.getDateFormat());
		return formDTO;
	}

	public void setFormDTO(DTObject formDTO) {
		this.formDTO = formDTO;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public ErrorMap getErrorMap() {
		return errorMap;
	}

	public String getCallSource() {
		return callSource;
	}

	public void setCallSource(String callSource) {
		this.callSource = callSource;
	}

	public String getBackToTemplate() {
		return backToTemplate;
	}

	public void setBackToTemplate(String backToTemplate) {
		this.backToTemplate = backToTemplate;
	}

	public String getSourceKey() {
		return sourceKey;
	}

	public void setSourceKey(String sourceKey) {
		this.sourceKey = sourceKey;
	}

	public String getPrimaryKey() {
		return primaryKey;
	}

	public void setPrimaryKey(String primaryKey) {
		this.primaryKey = primaryKey;
	}

	// ARIBALA ADDED ON 09102015 BEGIN
	public String getRectify() {
		return rectify;
	}

	public void setRectify(String rectify) {
		this.rectify = rectify;
	}

	// ARIBALA ADDED ON 09102015 END

	@Override
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		if (context != null) {
			WebContext.getInstance().getSession().setAttribute(Globals.LOCALE_KEY, context.getLocale());
			// logger.logDebug("reset()");
			action = ContentManager.EMPTY_STRING;
			command = ContentManager.EMPTY_STRING;
			formDTO.reset();
			if (!context.isFormPosted()) {
				initializeCSRFGuard();
			}
			// added by swaroopa as on 15-05-2012 begins
			initializeTFA(context.getProcessID());
			// added by swaroopa as on 15-05-2012 ends
			ProgramConfiguration programConfiguration = ProgramMap.getInstance().getConfiguration(context.getProcessID());
			boolean backToTmplt = programConfiguration.isBackToTemplate();
			if (backToTmplt && RegularConstants.COLUMN_ENABLE.equals(request.getParameter("backToTemplate"))) {
				setPrimaryKey((String) request.getParameter("primaryKey"));
				setSourceKey((String) request.getParameter("sourceKey"));
				setBackToTemplate(RegularConstants.COLUMN_ENABLE);
			}
			// logger.logDebug("child reset begin()");
			// winType=(String)request.getAttribute(RequestConstants.WINDOW_TYPE);
			if (context.getCallSource().equals("C"))
				callSource = context.getCallSource();
			// logger.logDebug("child reset end()");

			if (context.getEntityCode() != null) {
				DBContext dbContext = new DBContext();
				ArrayList<GenericOption> scenarioList = null;
				scenarioList = getAvailableScenarios(context.getEntityCode(), context.getProcessID(), dbContext, "Scenarios List");
				if (scenarioList.size() > 1)
					webContext.getRequest().setAttribute("COMMON_SCENARIOLIST", scenarioList);

				dbContext.close();
			}
			reset();
		}
	}

	@Override
	public final ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
		WebContext.getInstance().getSession().setAttribute(Globals.LOCALE_KEY, context.getLocale());
		ActionErrors errors = new ActionErrors();
		formDTO = new DTObject();
		// logger.logDebug("validate()");

		// logger.logDebug("Command : " + command);
		if (command.equals(RESET_ACTION))
			return errors;

		ProgramConfiguration programConfiguration = ProgramMap.getInstance().getConfiguration(context.getProcessID());

		if (context.isFormPosted()) {

			if (isCSRFAttack() && !programConfiguration.isSkipCSRFAttackCheck()) {
				errors.add("genericError", new ActionMessage(BackOfficeErrorCodes.UNAUTHORIZED_ACTION));
			}

		}
		if (errors.size() == 0) {
			if (programConfiguration.isAllocationRequired()) {
				if (!request.getAttribute(RequestConstants.ADD_ALLOWED).equals(RegularConstants.COLUMN_ENABLE) && !request.getAttribute(RequestConstants.MODIFY_ALLOWED).equals(RegularConstants.COLUMN_ENABLE)) {
					errors.add("genericError", new ActionMessage(BackOfficeErrorCodes.UNAUTHORIZED_ACTION));
				}
			}
			if (errors.size() == 0) {
				// added by swaroopa as on 15-05-2012 begins
				boolean tfaValid = isTFAValid(context.getProcessID());
				if (tfaValid) {
					setTfaSuccess(RegularConstants.COLUMN_ENABLE);
				} else {
					setTfaSuccess(RegularConstants.COLUMN_DISABLE);
				}
				if (!tfaValid && !isTFAHandlingSelfManaged()) {
					formDTO.set(ContentManager.ERROR, getTfaErrorCode());
					return null;
					// added by swaroopa as on 15-05-2012 ends
				} else {
					validate();
					request.setAttribute(RequestConstants.FORM_DTO, formDTO);
				}
			}
		}
		if (errorMap.length() > 0) {
			request.setAttribute(RequestConstants.ERRORS_PRESENT, RequestConstants.ERRORS_PRESENT);
			request.setAttribute(RequestConstants.ERROR_MAP, errorMap);
		}
		for (String field : errorMap.getFields()) {
			errors.add(field, new ActionMessage(errorMap.getErrorKey(field), errorMap.getErrorParam(field)));
		}

		// errorMap.clear();

		return errors;
	}

	public abstract void reset();

	public abstract void validate();

	public static final ArrayList<GenericOption> getGenericOptions(String programID, String tokenID, DBContext dbContext, String blankString) {
		ApplicationContext context = ApplicationContext.getInstance();
		ArrayList<GenericOption> genericList = new ArrayList<GenericOption>();
		int counter = 0;
		if (blankString != null) {
			genericList.add(counter, new GenericOption(ContentManager.EMPTY_STRING, blankString));
			++counter;
		}
		Locale locale = context.getLocale();
		if (locale == null)
			locale = Locale.US;
		String localeName = locale.toString().toUpperCase(context.getLocale());
		String sqlQuery = "SELECT LOV_VALUE, LOV_LABEL_" + localeName + " FROM CMLOVREC WHERE PGM_ID = ? AND LOV_ID = ? ORDER BY LOV_SL ASC";
		DBUtil util = dbContext.createUtilInstance();
		try {
			util.setMode(DBUtil.PREPARED);
			util.setSql(sqlQuery);
			util.setString(1, programID);
			util.setString(2, tokenID);
			ResultSet rs = util.executeQuery();
			while (rs.next()) {
				genericList.add(counter, new GenericOption(rs.getString(1), rs.getString(2)));
				++counter;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			util.reset();
		}
		return genericList;
	}

	public static final ArrayList<GenericOption> getAvailablePrograms(String entityCode, String roleCode, DBContext dbContext, String blankString) {
		ArrayList<GenericOption> genericList = new ArrayList<GenericOption>();
		String sqlQuery = "SELECT RD.PGM_ID,M.MPGM_DESCN FROM ROLEPGMALLOCDTL RD,MPGM M WHERE RD.ENTITY_CODE = ? AND RD.ROLE_CODE = ? AND RD.EFFT_DATE = (SELECT MAX(R.EFFT_DATE) FROM ROLEPGMALLOC R WHERE R.ENTITY_CODE = ? AND R.ROLE_CODE = ? AND R.AU_ON IS NOT NULL)AND RD.PGM_ID = M.MPGM_ID ORDER BY RD.PGM_ID ASC";
		DBUtil util = dbContext.createUtilInstance();
		try {
			util.setMode(DBUtil.PREPARED);
			util.setSql(sqlQuery);
			util.setString(1, entityCode);
			util.setString(2, roleCode);
			util.setString(3, entityCode);
			util.setString(4, roleCode);
			ResultSet rs = util.executeQuery();
			int counter = 0;
			genericList.add(counter, new GenericOption(ContentManager.EMPTY_STRING, blankString));
			counter++;
			while (rs.next()) {
				genericList.add(counter, new GenericOption(rs.getString(1), rs.getString(2)));
				counter++;
			}
		} catch (Exception e) {
			e.printStackTrace();

		} finally {
			util.reset();
		}
		return genericList;
	}

	public void setProcessBO(String processBO) {
		this.processBO = processBO;
	}

	public String getProcessBO() {
		return processBO;
	}

	public String decodeBooleanToString(boolean value) {
		return FormatUtils.decodeBooleanToString(value);
	}

	public boolean decodeStringToBoolean(String value) {
		return FormatUtils.decodeStringToBoolean(value);
	}

	public void setActionDescription(String actionDescription) {
		this.actionDescription = actionDescription;
	}

	public String getActionDescription() {
		return actionDescription;
	}

	public String getErrorResource(String key, Object[] param) {
		// logger.logDebug("getCommonResource()");
		String message = ContentManager.EMPTY_STRING;
		try {
			ResourceBundle bundle = ResourceBundle.getBundle("patterns/config/web/forms/Validation", context.getLocale());
			message = bundle.getString(key);
			message = MessageFormat.format(message, param);
		} catch (Exception e) {
			ResourceBundle bundle = ResourceBundle.getBundle("patterns/config/web/forms/Validation");
			try {
				message = bundle.getString(key);
				message = MessageFormat.format(message, param);
			} catch (Exception e1) {
				e1.printStackTrace();
				logger.logError("getCommonResource(1) :: " + e.getLocalizedMessage());
			}
		}
		return message;
	}

	public String getCommonResource(String key, Object[] param) {
		// logger.logDebug("getCommonResource()");
		String message = ContentManager.EMPTY_STRING;
		try {
			ResourceBundle bundle = ResourceBundle.getBundle("patterns/config/web/forms/Messages", context.getLocale());
			message = bundle.getString(key);
			message = MessageFormat.format(message, param);
		} catch (Exception e) {
			ResourceBundle bundle = ResourceBundle.getBundle("patterns/config/web/forms/Messages");
			try {
				message = bundle.getString(key);
				message = MessageFormat.format(message, param);
			} catch (Exception e1) {
				e1.printStackTrace();
				logger.logError("getCommonResource(1) :: " + e.getLocalizedMessage());
			}
		}
		return message;
	}

	// added by swaroopa as on 15-05-2012 begins
	public void initializeTFA(String programID) {
		ProgramConfiguration programConfiguration = ProgramMap.getInstance().getConfiguration(programID);
		boolean programTfaRequired = programConfiguration.isTfaRequired();
		boolean credentialManagementOption = programConfiguration.isCredentialManagementOption();
		if (programTfaRequired) {
			if (credentialManagementOption) {
				String tfaRequired = (String) webContext.getSession().getAttribute(SessionConstants.TFA_REQ);
				String loginTfaRequired = (String) webContext.getSession().getAttribute(SessionConstants.LOGIN_TFA_REQ);
				if (tfaRequired.equals(RegularConstants.COLUMN_ENABLE)) {
					if (loginTfaRequired.equals(RegularConstants.COLUMN_ENABLE)) {
						setTfaRequired(RegularConstants.COLUMN_ENABLE);
						setTfaNonce((String) webContext.getSession().getAttribute(SessionConstants.TFA_NONCE));
					} else {
						setTfaRequired(RegularConstants.COLUMN_DISABLE);
					}
				} else {
					setTfaRequired(RegularConstants.COLUMN_DISABLE);
				}
			} else {
				String tfaRequired = (String) webContext.getSession().getAttribute(SessionConstants.TFA_REQ);
				if (tfaRequired.equals(RegularConstants.COLUMN_ENABLE)) {
					setTfaRequired(RegularConstants.COLUMN_ENABLE);
					setTfaNonce((String) webContext.getSession().getAttribute(SessionConstants.TFA_NONCE));
				} else {
					setTfaRequired(RegularConstants.COLUMN_DISABLE);
				}
			}
		} else {
			setTfaRequired(RegularConstants.COLUMN_DISABLE);
		}
		webContext.getRequest().setAttribute(SessionConstants.TFA_REQ, getTfaRequired());
	}

	public boolean isTFAValid(String programID) {
		boolean tfaSuccess = true;
		// PKIValidator validator = new PKIValidator();
		// try {
		// ProgramConfiguration programConfiguration =
		// ProgramMap.getInstance().getConfiguration(programID);
		// boolean programTfaRequired = programConfiguration.isTfaRequired();
		// if (!programTfaRequired) {
		// tfaSuccess = true;
		// return tfaSuccess;
		// }
		// String tfaRequired = (String)
		// webContext.getSession().getAttribute(SessionConstants.TFA_REQ);
		// if (tfaRequired.equals(RegularConstants.COLUMN_DISABLE)) {
		// tfaSuccess = true;
		// return tfaSuccess;
		// } else {
		// boolean credentialManagementOption =
		// programConfiguration.isCredentialManagementOption();
		// if (credentialManagementOption) {
		// String loginTfaRequired = (String)
		// webContext.getSession().getAttribute(SessionConstants.LOGIN_TFA_REQ);
		// if (loginTfaRequired.equals(RegularConstants.COLUMN_DISABLE)) {
		// tfaSuccess = true;
		// return tfaSuccess;
		// }
		// }
		//
		// String tfaNonce = (String)
		// WebContext.getInstance().getSession().getAttribute(SessionConstants.TFA_NONCE);
		// Hashtable hashTable = HttpUtils.parseQueryString(getTfaValue());
		// String[] requestTFANonce = (String[]) hashTable.get("tfaNonce");
		// if (tfaNonce == null || requestTFANonce == null ||
		// !tfaNonce.equals(requestTFANonce[0])) {
		// tfaSuccess = false;
		// return tfaSuccess;
		// }
		// DTObject input = new DTObject();
		// input.set("ENTITY_CODE", context.getEntityCode());
		// input.set("USER_ID", context.getUserID());
		// input.set("ACTUAL_VALUE", getTfaValue());
		// input.set("ENCODED_VALUE", getTfaEncodedValue());
		// input.set("USER_CERT_INV_NUM", (String)
		// WebContext.getInstance().getSession().getAttribute(SessionConstants.CERT_INV_NUM));
		// input.set("TFA_REQ", tfaRequired);
		//
		// DTObject result = validator.verifyBankUserPKIFromWeb(input);
		// if (result.get(ContentManager.ERROR) != null) {
		// setTfaErrorCode(result.get(ContentManager.ERROR));
		// tfaSuccess = false;
		// } else {
		// tfaSuccess = true;
		// }
		// }
		// } catch (Exception e) {
		// e.printStackTrace();
		// tfaSuccess = false;
		// } finally {
		// validator.close();
		// }
		return tfaSuccess;
	}

	// added by swaroopa as on 15-05-2012 ends

	public void initializeCSRFGuard() {
		try {
			String sessionKey = context.getProcessID() + RequestConstants.CSRF_CONSTANT;
			String sessionValue = (String) webContext.getSession().getAttribute(sessionKey);
			if (sessionValue == null) {
				sessionValue = SecurityUtils.generateCSRFToken();
				webContext.getSession().setAttribute(sessionKey, sessionValue);
			}
			csrfValue = sessionValue;
			// logger.logDebug("initializeCSRFGuard :: " + csrfValue);
		} catch (Exception e) {
			e.printStackTrace();
			logger.logDebug("initializeCSRFGuard(1) :: " + e.getLocalizedMessage());
		}
	}

	public boolean isCSRFAttack() {
		try {
			String sessionKey = context.getProcessID() + RequestConstants.CSRF_CONSTANT;
			String sessionValue = (String) webContext.getSession().getAttribute(sessionKey);
			if (sessionValue != null && sessionValue.equals(csrfValue)) {
				return false;
			}
			logger.logDebug("isCSRFAttack() :: Attack :: " + sessionValue + " :: " + csrfValue);
		} catch (Exception e) {
			e.printStackTrace();
			logger.logDebug("isCSRFAttack(1) :: " + e.getLocalizedMessage());
		}
		return true;
	}

	public boolean isTFAHandlingSelfManaged() {
		return false;
	}

	public static final ArrayList<GenericOption> getPrinterOptions(DBContext dbContext, String blankString) {
		ApplicationContext context = ApplicationContext.getInstance();
		ArrayList<GenericOption> genericList = new ArrayList<GenericOption>();
		int counter = 0;
		if (blankString != null) {
			genericList.add(counter, new GenericOption(ContentManager.EMPTY_STRING, blankString));
			++counter;
		}
		Locale locale = context.getLocale();
		if (locale == null)
			locale = Locale.US;
		String sqlQuery = "SELECT PRINTER_ID, PRINTER_NAME FROM CMNPRINTERS WHERE PRINTER_ORG_ID=? AND PRINTER_ENABLED='1'  ORDER BY PRINTER_ID ";
		DBUtil util = dbContext.createUtilInstance();
		try {
			util.setMode(DBUtil.PREPARED);
			util.setSql(sqlQuery);
			util.setString(1, context.getEntityCode());
			ResultSet rs = util.executeQuery();
			while (rs.next()) {
				genericList.add(counter, new GenericOption(rs.getString(1), rs.getString(2)));
				++counter;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			util.reset();
		}
		return genericList;
	}

	private int reportExportType;

	public int getReportExportType() {
		return reportExportType;
	}

	private static final String REPORT_BASE_PATH = "patterns/config/web/reports";

	protected DTObject processDownload(DTObject resultDTO) {
		ReportUtils utils = new ReportUtils();
		try {
			reportExportType = Integer.valueOf(resultDTO.get(ContentManager.REPORT_FORMAT));
			String reportHandlerName = resultDTO.get(ContentManager.REPORT_HANDLER);
			String programID = resultDTO.get("REPORT_TYPE");
			ReportInterface reportInterface = utils.getReportHandlerInstance(reportHandlerName);
			if (reportInterface == null) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_REPORT_ERROR);
				Object[] errorParam = { reportHandlerName };
				resultDTO.set(ContentManager.ERROR_MESSAGE, getErrorResource("report.handlerinvalid", errorParam));
				return resultDTO;
			}
			// String
			// stationeryType=utils.getStationeryType(context.getEntityCode(),context.getUserID(),programID);
			resultDTO.setObject(ContentManager.REPORT_FORMAT, reportExportType);
			reportInterface.setContext(new ReportContext(webContext.getRequest(), webContext.getResponse(), webContext.getSession(), resultDTO));
			reportInterface.init();
			reportInterface.initResource();
			int reportProcessorType = reportInterface.getReportProcessorType();
			JasperPrint jasperPrint = null;
			/** Dynamic Jasper  Report Begin**/
			List<JasperPrint> jasperPrintList = null;
			/** Dynamic Jasper  Report End**/
			// JasperReport jasperReport = null;
			InputStream jasperInputStream = null;
			HashMap<String, Object> reportParameters = new HashMap<String, Object>(reportInterface.getReportParameters());
			/*
			 * if(stationeryType.equals(RegularConstants.EMPTY_STATIONERY_TYPE)){
			 * reportParameters.put("SUBREPORT_DIRHP", REPORT_BASE_PATH +
			 * "/common/headerE.jasper");
			 * reportParameters.put("SUBREPORT_DIRHL", REPORT_BASE_PATH +
			 * "/common/headerE.jasper"); }else{
			 * reportParameters.put("SUBREPORT_DIRHP", REPORT_BASE_PATH +
			 * "/common/headerP.jasper");
			 * reportParameters.put("SUBREPORT_DIRHL", REPORT_BASE_PATH +
			 * "/common/headerL.jasper"); }
			 */
			reportParameters.put("SUBREPORT_DIRHP", REPORT_BASE_PATH + "/common/headerP.jasper");
			reportParameters.put("SUBREPORT_DIRHL", REPORT_BASE_PATH + "/common/headerL.jasper");
			reportParameters.put("SUBREPORT_DIRFP", REPORT_BASE_PATH + "/common/footerP.jasper");
			reportParameters.put("SUBREPORT_DIRFL", REPORT_BASE_PATH + "/common/footerL.jasper");
			reportParameters.put("SUBREPORT_DIR", REPORT_BASE_PATH + reportParameters.get("SUBREPORT_PATH"));
			reportParameters.put("imgpath", this.getClass().getClassLoader().getResourceAsStream(REPORT_BASE_PATH + "/resources/images/Header-3.png"));
			// reportParameters.put("rbifont", REPORT_BASE_PATH +
			// "/resources/images/rbi.jpg");
			// reportParameters.put("CustomerCode", context.getCustomerName() +
			// " ( " + context.getCustomerCode() + " )");
			reportParameters.put("LoggedUser", context.getUserID());
			reportParameters.put("RM", new ReportRM(utils.getCommonResource(reportInterface.getResourceBundle())));
			reportParameters.put("RMC", new ReportRM(utils.getCommonResource(reportInterface.getCommonResourceBundle())));
			reportParameters.put("DATE_FORMAT", context.getDateFormat());
/** Dynamic Jasper  Report Begin**/
			if (!reportInterface.isDynamic()) {
				jasperInputStream = utils.getJasperStream(REPORT_BASE_PATH + reportInterface.getReportPath());
				if (jasperInputStream == null) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_REPORT_ERROR);
					Object[] errorParam = { reportInterface.getReportPath() };
					resultDTO.set(ContentManager.ERROR_MESSAGE, getErrorResource("report.pathinvalid", errorParam));
					return resultDTO;
				}
				switch (reportProcessorType) {
				case ReportConstants.REPORT_PROCESSOR_JASPER:
					jasperPrint = utils.getJasperPrint(jasperInputStream, reportParameters, utils.getConnection());
					break;
				case ReportConstants.REPORT_PROCESSOR_RESULT_SET:
					JRResultSetDataSource resultSetDataSource = reportInterface.processResultSet();
					reportParameters.put("REPORT_CONNECTION", utils.getConnection());
					jasperPrint = utils.getJasperPrint(jasperInputStream, reportParameters, resultSetDataSource);
					break;
				case ReportConstants.REPORT_PROCESSOR_XML:
					Document document = reportInterface.processXMLData();
					reportParameters.put("XML_DATA_DOCUMENT", document);
					jasperPrint = utils.getJasperPrint(jasperInputStream, reportParameters);
					break;
				case ReportConstants.REPORT_PROCESSOR_CONNECTION:
					DBContext dbContext = new DBContext();
					jasperPrint = utils.getJasperPrint(jasperInputStream, reportParameters, dbContext.openConnection());
					dbContext.close();
					break;
				}
				if (jasperPrint == null) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_REPORT_ERROR);
					// resultDTO.set(ContentManager.ERROR_MESSAGE,
					// getErrorResource("report.generationerror"));
					return resultDTO;
				}

				if (jasperPrint.getPages().isEmpty()) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_EMPTY_GRID);
					return resultDTO;
				}
			} else {

				/*
				 * jasperReport=utils.getJasperReport(reportInterface.
				 * getDyanmicReportTemplate(REPORT_BASE_PATH)); if (jasperReport
				 * == null) { resultDTO.set(ContentManager.ERROR,
				 * BackOfficeErrorCodes.HMS_REPORT_ERROR); Object[] errorParam =
				 * { reportInterface.getReportPath() };
				 * resultDTO.set(ContentManager.ERROR_MESSAGE,
				 * getErrorResource("report.pathinvalid", errorParam)); return
				 * resultDTO; }
				 */
				jasperPrintList = utils.getDynamicJasperPrint(reportInterface.getDyanmicReportContent(REPORT_BASE_PATH), reportInterface.getDyanmicReportProcessorType(), reportInterface.getDyanmicReportProcessData(), reportParameters);

			}
/** Dynamic Jasper  Report End**/
			reportInterface.releaseResource();
			reportInterface.destroy();

			String reportIdentifier = utils.getReportSequence();
			if (reportIdentifier == null || reportIdentifier.equals(RegularConstants.EMPTY_STRING)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_REPORT_ERROR);
				// resultDTO.set(ContentManager.ERROR_MESSAGE,
				// getErrorResource("report.reportiderror"));
				return resultDTO;
			}
			String deploymentContext = utils.getDeploymentContext();
			String reportName = utils.getReportName(programID, reportIdentifier, reportExportType);
			String userID = context.getUserID();
			String entityCode = context.getEntityCode();
			String reportFilePath = utils.getReportFilePath(entityCode, reportName);
			if (reportFilePath == null || reportFilePath.equals(RegularConstants.EMPTY_STRING)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_REPORT_ERROR);
				// resultDTO.set(ContentManager.ERROR_MESSAGE,
				// getErrorResource("report.reportpatherror"));
				return resultDTO;
			}

			String reportOutputPath = utils.getReportFileOutputPath(reportFilePath, reportName);
/** Dynamic Jasper  Report Begin**/
			if (!reportInterface.isDynamic())
				utils.exportJasperReport(jasperPrint, reportExportType, reportOutputPath);
			else {
				utils.exportJasperReport(jasperPrintList, reportExportType, reportOutputPath);
			}
			/** Dynamic Jasper  Report End**/
			utils.createReportDownload(entityCode, reportIdentifier, deploymentContext, userID, reportExportType, reportOutputPath);

			if (reportInterface.isError()) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_REPORT_ERROR);
				resultDTO.set(ContentManager.ERROR_MESSAGE, reportInterface.getErrorMessage());
			} else {
				resultDTO.set(ContentManager.REPORT_ID, reportIdentifier);
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_REPORT_ERROR);
			resultDTO.set(ContentManager.ERROR_MESSAGE, e.getLocalizedMessage());
			return resultDTO;
		} catch (Throwable e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_REPORT_ERROR);
			return resultDTO;
		} finally {
			utils.close();
		}
		return resultDTO;
	}

	
	public boolean checkProgramAccess(String programId, String action) {
		boolean flag = false;
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		try {
			util.setMode(DBUtil.PREPARED);
			util.setSql("SELECT FN_ISPGMACCESSALLOWED(?,?,?,?) FROM DUAL");
			util.setString(1, context.getEntityCode());
			util.setString(2, context.getRoleCode());
			util.setString(3, programId);
			util.setString(4, action);
			ResultSet rs = util.executeQuery();
			if (rs.next()) {
				if (rs.getString(1).equals(RegularConstants.COLUMN_ENABLE)) {
					flag = true;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			flag = false;
		} finally {
			util.reset();
			dbContext.close();
		}
		return flag;
	}

	public static final ArrayList<GenericOption> getTitle(DBContext dbContext, String blankString, String type) {
		ArrayList<GenericOption> genericList = new ArrayList<GenericOption>();
		String sqlQuery = ContentManager.EMPTY_STRING;
		int counter = 0;
		if (blankString != null) {
			genericList.add(counter, new GenericOption(ContentManager.EMPTY_STRING, blankString));
			++counter;
		}
		if (type.equals(ContentManager.PROFESSIONAL_TITLE)) {
			sqlQuery = "SELECT TITLE_CODE FROM TITLE WHERE TITLE_OCCU IS NOT NULL AND ENABLED='1'"; // professional
																						// title
		} else {
			sqlQuery = "SELECT TITLE_CODE FROM TITLE WHERE TITLE_OCCU IS NULL AND ENABLED='1' "; // general
																					// title
		}
		DBUtil util = dbContext.createUtilInstance();
		try {
			util.setMode(DBUtil.PREPARED);
			util.setSql(sqlQuery);
			ResultSet rs = util.executeQuery();
			while (rs.next()) {
				genericList.add(counter, new GenericOption(rs.getString(1), rs.getString(1)));
				++counter;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			util.reset();
		}
		return genericList;
	}

	public static final ArrayList<GenericOption> getGeneralTitlewithoutChild(DBContext dbContext, String blankString) {
		ArrayList<GenericOption> genericList = new ArrayList<GenericOption>();
		String sqlQuery = ContentManager.EMPTY_STRING;
		int counter = 0;
		if (blankString != null) {
			genericList.add(counter, new GenericOption(ContentManager.EMPTY_STRING, blankString));
			++counter;
		}
		sqlQuery = "SELECT TITLE_CODE FROM TITLE WHERE TITLE_CHILDREN_USG='0' AND TITLE_OCCU IS NULL "; // general
		DBUtil util = dbContext.createUtilInstance();
		try {
			util.setMode(DBUtil.PREPARED);
			util.setSql(sqlQuery);
			ResultSet rs = util.executeQuery();
			while (rs.next()) {
				genericList.add(counter, new GenericOption(rs.getString(1), rs.getString(1)));
				++counter;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			util.reset();
		}
		return genericList;
	}

	public DTObject processPrintRequest(DTObject input) {
		return input;
	}

	public static final ArrayList<GenericOption> getAvailableScenarios(String entityCode, String programId, DBContext dbContext, String blankString) {
		ArrayList<GenericOption> genericList = new ArrayList<GenericOption>();
		String sqlQuery = "SELECT S.SCENARIO_CODE,F.DESCRIPTION FROM AVLFORMSCENARIODTL S JOIN FORMSCENARIO F ON (F.ENTITY_CODE=S.ENTITY_CODE AND F.SCENARIO_CODE=S.SCENARIO_CODE AND F.PROGRAM_ID=S.PROGRAM_ID) WHERE S.ENTITY_CODE =? AND S.PROGRAM_ID=? AND S.EFFT_DATE= (SELECT MAX(EFFT_DATE) FROM AVLFORMSCENARIODTL WHERE ENTITY_CODE =? AND PROGRAM_ID=?) ORDER BY SCENARIO_CODE ASC";
		DBUtil util = dbContext.createUtilInstance();
		try {
			util.setMode(DBUtil.PREPARED);
			util.setSql(sqlQuery);
			util.setLong(1, Long.parseLong(entityCode));
			util.setString(2, programId);
			util.setLong(3, Long.parseLong(entityCode));
			util.setString(4, programId);
			ResultSet rs = util.executeQuery();
			int counter = 0;
			genericList.add(counter, new GenericOption(ContentManager.EMPTY_STRING, blankString));
			counter++;
			while (rs.next()) {
				genericList.add(counter, new GenericOption(rs.getString(1), rs.getString(2)));
				counter++;
			}
		} catch (Exception e) {
			e.printStackTrace();

		} finally {
			util.reset();
		}
		return genericList;
	}

	protected final TBAProcessAction getProcessAction(DTObject inputDTO,TBAActionType actionType) {
		TBAProcessAction processAction = new TBAProcessAction();
		processAction.setPartitionNo(context.getPartitionNo());
		processAction.setActionEntity(context.getEntityCode());
		processAction.setActionBranch(context.getBranchCode());
		processAction.setActionType(actionType);
		processAction.setActionUser(context.getUserID());
		processAction.setActionDate(context.getCurrentBusinessDate());
		processAction.setActionCustomer(context.getCustomerCode());
		processAction.setProcessID(inputDTO.get("PROGRAM_ID").toUpperCase(context.getLocale()));
		processAction.setActionIP(context.getClientIP());
		processAction.setFinYear(context.getFinYear());
		processAction.setCurrentYear(context.getCurrentYear());
		processAction.setDateFormat(context.getDateFormat());
		processAction.setActionRole(context.getRoleCode());
		processAction.setClusterCode(context.getClusterCode());
		return processAction;
	}
	public static final ArrayList<GenericOption> getGenericOptionsWithOutConcatValue(String programID, String tokenID, DBContext dbContext, String blankString) {
		ApplicationContext context = ApplicationContext.getInstance();
		ArrayList<GenericOption> genericList = new ArrayList<GenericOption>();
		int counter = 0;
		if (blankString != null) {
			genericList.add(counter, new GenericOption(ContentManager.EMPTY_STRING, blankString));
			++counter;
		}
		Locale locale = context.getLocale();
		if (locale == null)
			locale = Locale.US;
		String localeName = locale.toString().toUpperCase(context.getLocale());
		String sqlQuery = "SELECT LOV_VALUE, LOV_LABEL_" + localeName + " FROM CMLOVREC WHERE PGM_ID = ? AND LOV_ID = ? ORDER BY LOV_SL ASC";
		DBUtil util = dbContext.createUtilInstance();
		try {
			util.setMode(DBUtil.PREPARED);
			util.setSql(sqlQuery);
			util.setString(1, programID);
			util.setString(2, tokenID);
			ResultSet rs = util.executeQuery();
			while (rs.next()) {
				genericList.add(counter, new GenericOption(rs.getString(1), rs.getString(2)));
				++counter;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			util.reset();
		}
		return genericList;
	}
}
