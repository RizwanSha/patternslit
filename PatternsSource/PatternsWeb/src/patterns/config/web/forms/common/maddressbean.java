package patterns.config.web.forms.common;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import patterns.config.delegate.BackOfficeProcessManager;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.pki.ProcessTFAInfo;
import patterns.config.framework.process.TBAProcessResult;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.MasterValidator;
import patterns.config.web.forms.GenericFormBean;

public class maddressbean extends GenericFormBean {
	public maddressbean() {
		setProcessBO("patterns.config.framework.bo.common.maddressBO");
	}

	private static final long serialVersionUID = -6219657363150466160L;

	private String personId;
	private String addressType;
	private String countryCode;
	private String countryCode_desc;
	private String oldAddressLine1;
	private String oldAddressLine2;
	private String oldAddressLine3;
	private String oldAddressLine4;
	private String addressLine1;
	private String addressLine2;
	private String addressLine3;
	private String addressLine4;
	private String addressPortion;
	private String pinCode;
	private String inventoryNumber;
	private String operationType;
	private String operationStatus;
	private String currentTab;
	private String geoUnitStructure;
	public boolean localAddress;
	public boolean permanentAddress;
	public boolean officialUse;
	public boolean addrForComm;
	public boolean localAddressExist;
	public boolean permanentAddressExist;
	public boolean officialUseExist;
	public boolean addrForCommExist;
	private String personSearch;
	private String xmladdress;
	private String parentProgramId;
	private String parentPrimaryKey;
	private String linkProgramId;
	private String linkPrimaryKey;
	private String requestType;
	private String sourceKey;
	private String sourceValue;
	private String conversationID;
	private String returnType;
	private String parentAction;
	private String oldAddressLine5;

	public String getConversationID() {
		return conversationID;
	}

	public void setConversationID(String conversationID) {
		this.conversationID = conversationID;
	}

	public String getOldAddressLine5() {
		return oldAddressLine5;
	}

	public String getCountryCode_desc() {
		return countryCode_desc;
	}

	public void setCountryCode_desc(String countryCode_desc) {
		this.countryCode_desc = countryCode_desc;
	}

	public void setOldAddressLine5(String oldAddressLine5) {
		this.oldAddressLine5 = oldAddressLine5;
	}

	

	public String getGeoUnitStructure() {
		return geoUnitStructure;
	}

	public void setGeoUnitStructure(String geoUnitStructure) {
		this.geoUnitStructure = geoUnitStructure;
	}

	

	public String getPersonSearch() {
		return personSearch;
	}

	public void setPersonSearch(String personSearch) {
		this.personSearch = personSearch;
	}

	public String getPersonId() {
		return personId;
	}

	public void setPersonId(String personId) {
		this.personId = personId;
	}

	public String getAddressType() {
		return addressType;
	}

	public void setAddressType(String addressType) {
		this.addressType = addressType;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getOldAddressLine1() {
		return oldAddressLine1;
	}

	public void setOldAddressLine1(String oldAddressLine1) {
		this.oldAddressLine1 = oldAddressLine1;
	}

	public String getOldAddressLine2() {
		return oldAddressLine2;
	}

	public void setOldAddressLine2(String oldAddressLine2) {
		this.oldAddressLine2 = oldAddressLine2;
	}

	public String getOldAddressLine3() {
		return oldAddressLine3;
	}

	public void setOldAddressLine3(String oldAddressLine3) {
		this.oldAddressLine3 = oldAddressLine3;
	}

	public String getOldAddressLine4() {
		return oldAddressLine4;
	}

	public void setOldAddressLine4(String oldAddressLine4) {
		this.oldAddressLine4 = oldAddressLine4;
	}

	public String getAddressLine1() {
		return addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public String getAddressLine2() {
		return addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public String getAddressLine3() {
		return addressLine3;
	}

	public void setAddressLine3(String addressLine3) {
		this.addressLine3 = addressLine3;
	}

	public String getAddressLine4() {
		return addressLine4;
	}

	public void setAddressLine4(String addressLine4) {
		this.addressLine4 = addressLine4;
	}

	public String getAddressPortion() {
		return addressPortion;
	}

	public void setAddressPortion(String addressPortion) {
		this.addressPortion = addressPortion;
	}

	public String getPinCode() {
		return pinCode;
	}

	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}

	public String getInventoryNumber() {
		return inventoryNumber;
	}

	public void setInventoryNumber(String inventoryNumber) {
		this.inventoryNumber = inventoryNumber;
	}

	public String getOperationStatus() {
		return operationStatus;
	}

	public void setOperationStatus(String operationStatus) {
		this.operationStatus = operationStatus;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getCurrentTab() {
		return currentTab;
	}

	public void setCurrentTab(String currentTab) {
		this.currentTab = currentTab;
	}

	public boolean isLocalAddress() {
		return localAddress;
	}

	public void setLocalAddress(boolean localAddress) {
		this.localAddress = localAddress;
	}

	public boolean isPermanentAddress() {
		return permanentAddress;
	}

	public void setPermanentAddress(boolean permanentAddress) {
		this.permanentAddress = permanentAddress;
	}

	public boolean isOfficialUse() {
		return officialUse;
	}

	public void setOfficialUse(boolean officialUse) {
		this.officialUse = officialUse;
	}

	public String getXmladdress() {
		return xmladdress;
	}

	public void setXmladdress(String xmladdress) {
		this.xmladdress = xmladdress;
	}

	public boolean isLocalAddressExist() {
		return localAddressExist;
	}

	public void setLocalAddressExist(boolean localAddressExist) {
		this.localAddressExist = localAddressExist;
	}

	public boolean isPermanentAddressExist() {
		return permanentAddressExist;
	}

	public void setPermanentAddressExist(boolean permanentAddressExist) {
		this.permanentAddressExist = permanentAddressExist;
	}

	public boolean isOfficialUseExist() {
		return officialUseExist;
	}

	public void setOfficialUseExist(boolean officialUseExist) {
		this.officialUseExist = officialUseExist;
	}

	public String getParentProgramId() {
		return parentProgramId;
	}

	public void setParentProgramId(String parentProgramId) {
		this.parentProgramId = parentProgramId;
	}

	public String getParentPrimaryKey() {
		return parentPrimaryKey;
	}

	public void setParentPrimaryKey(String parentPrimaryKey) {
		this.parentPrimaryKey = parentPrimaryKey;
	}

	public String getLinkProgramId() {
		return linkProgramId;
	}

	public void setLinkProgramId(String linkProgramId) {
		this.linkProgramId = linkProgramId;
	}

	public String getLinkPrimaryKey() {
		return linkPrimaryKey;
	}

	public void setLinkPrimaryKey(String linkPrimaryKey) {
		this.linkPrimaryKey = linkPrimaryKey;
	}

	public String getRequestType() {
		return requestType;
	}

	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}

	public String getSourceKey() {
		return sourceKey;
	}

	public void setSourceKey(String sourceKey) {
		this.sourceKey = sourceKey;
	}

	public String getSourceValue() {
		return sourceValue;
	}

	public void setSourceValue(String sourceValue) {
		this.sourceValue = sourceValue;
	}

	public String getReturnType() {
		return returnType;
	}

	public void setReturnType(String returnType) {
		this.returnType = returnType;
	}

	public String getParentAction() {
		return parentAction;
	}

	public void setParentAction(String parentAction) {
		this.parentAction = parentAction;
	}

	public boolean isAddrForComm() {
		return addrForComm;
	}

	public void setAddrForComm(boolean addrForComm) {
		this.addrForComm = addrForComm;
	}

	public boolean isAddrForCommExist() {
		return addrForCommExist;
	}

	public void setAddrForCommExist(boolean addrForCommExist) {
		this.addrForCommExist = addrForCommExist;
	}

	@Override
	public void reset() {
		personId = RegularConstants.EMPTY_STRING;
		addressType = RegularConstants.EMPTY_STRING;
		countryCode = RegularConstants.EMPTY_STRING;
		addressLine1 = RegularConstants.EMPTY_STRING;
		addressLine2 = RegularConstants.EMPTY_STRING;
		addressLine3 = RegularConstants.EMPTY_STRING;
		addressLine4 = RegularConstants.EMPTY_STRING;
		geoUnitStructure = RegularConstants.EMPTY_STRING;
		localAddress = false;
		permanentAddress = false;
		officialUse = false;
		currentTab = "";
		setProcessBO("patterns.config.framework.bo.common.maddressBO");
		parentProgramId = webContext.getRequest().getParameter("_CGQMParentProgramId");
		parentPrimaryKey = webContext.getRequest().getParameter("_CGQMParentPK");
		linkProgramId = "MADDRESS";
		requestType = webContext.getRequest().getParameter("_CGQMRequestType");
		parentAction = webContext.getRequest().getParameter("_CGQMParentAction");
		sourceKey = webContext.getRequest().getParameter("_CGQMSourceKey");
		sourceValue = webContext.getRequest().getParameter("_CGQMSourceValue");
		conversationID = webContext.getRequest().getParameter("_CGQMConversationId");
		loadLinkProgramDetails();
	}

	@Override
	public void validate() {
		if ((!operationStatus.equals("R"))&&(!operationStatus.equals("U"))) {
			if(validateAddressType()){
			validateAddressLine1();
			validateAddressLine2();
			validateAddressLine3();
			validateAddressLine4();
			}

		}
		try {
			if (getErrorMap().length() == 0) {
				setParentProcessId(parentProgramId);
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.set("PARTITION_NO", context.getPartitionNo());
				formDTO.set("PERSONID", personId);
				formDTO.set("ADDR_TYPE", addressType);
				formDTO.set("COUNTRY_CODE", countryCode);
				formDTO.set("ADDRESS_LINE1", addressLine1);
				formDTO.set("ADDRESS_LINE2", addressLine2);
				formDTO.set("ADDRESS_LINE3", addressLine3);
				formDTO.set("ADDRESS_LINE4", addressLine4);
				formDTO.set("GEO_UNIT_ID", geoUnitStructure);
				if (operationStatus.equals("R")) {
					formDTO.set("ENABLED", decodeBooleanToString(false));
				} else {
					formDTO.set("ENABLED", decodeBooleanToString(true));
				}
				if (inventoryNumber != null && (!inventoryNumber.equals(RegularConstants.EMPTY_STRING))) {
					formDTO.set("ADDR_INV_NO", inventoryNumber);
				}
				formDTO.set("PARENT_PGM_ID", parentProgramId);
				formDTO.set("CONVERSATION_ID", conversationID);
				formDTO.set("LINKED_PGM_ID", linkProgramId);
				formDTO.set("PARENT_PGM_PK", parentPrimaryKey);
				formDTO.set("OPERATION_TYPE", operationType);
				formDTO.set("OPERATION_STATUS", operationStatus);
				ProcessTFAInfo processTFAInfo = new ProcessTFAInfo();
				processTFAInfo.setUserId(context.getUserID());
				processTFAInfo.setDeploymentContext(context.getDeploymentContext());
				processTFAInfo.setDigitalCertificateInventoryNumber(context.getDigitalCertificateInventoryNumber());
				processTFAInfo.setTfaRequired(getTfaRequired());
				processTFAInfo.setTfaSuccess(getTfaSuccess());
				processTFAInfo.setTfaValue(getTfaValue());
				processTFAInfo.setTfaEncodedValue(getTfaEncodedValue());
				BackOfficeProcessManager processManager = new BackOfficeProcessManager();
				formDTO.set("PARTITION_NO", context.getPartitionNo());
				formDTO.set("USER_ID", context.getUserID());
				formDTO.set("IP_ADDRESS", context.getClientIP());
				formDTO.setObject("CBD", context.getCurrentBusinessDate());
				formDTO.setObject("PKI", processTFAInfo);
				formDTO.set(RegularConstants.PROCESSBO_CLASS, getProcessBO());
				TBAProcessResult processResult = processManager.delegate(formDTO);
				if (processResult.getProcessStatus().equals(TBAProcessStatus.SUCCESS)) {
					if (processResult.getResponseDTO() != null) {
						if (processResult.getResponseDTO().get(RegularConstants.ERROR) != null) {
							getErrorMap().setError("personId", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
							return;
						} else if (processResult.getResponseDTO().get(RegularConstants.ADDITIONAL_INFO) != null) {
							getErrorMap().setError("personId", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
							return;
						}
					}
					if (getAction().equals(ADD) && inventoryNumber.equals(RegularConstants.EMPTY_STRING)) {
						inventoryNumber = processResult.getAdditionalInfo().toString();
					}
				} else {
					getErrorMap().setError("personId", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("personId", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
	}

	public DTObject updateInventory(DTObject input) {
		input.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		try {
			personId = input.get("PERSON_ID");
			addressType = input.get("ADDR_TYPE");
			setAction(input.get("OPERATION_TYPE"));
			countryCode = input.get("COUNTRY_CODE");
			addressLine1 = input.get("ADDR_LINE1");
			addressLine2 = input.get("ADDR_LINE2");
			addressLine3 = input.get("ADDR_LINE3");
			addressLine4 = input.get("ADDR_LINE4");
			geoUnitStructure = input.get("GEO_UNIT_ID");
			inventoryNumber = input.get("INVENTORY_NO");
			operationType = input.get("OPERATION_TYPE");
			operationStatus = input.get("OPERATION_STATUS");
			currentTab = input.get("ACTIVE_TAB");
			parentProgramId = input.get("PARENT_PGM_ID");
			parentPrimaryKey = input.get("PARENT_PGM_PK");
			linkProgramId = input.get("LINKED_PGM_ID");
			conversationID = input.get("CONVERSATION_ID");
			validate();
			if (getErrorMap().length() == 0) {
				input.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				input.set("INVENTORY_NO", inventoryNumber);
			}
		} catch (Exception e) {

		}
		return input;
	}

	public boolean validateAddressType() {
		MasterValidator validation = new MasterValidator();
		try {
			if (validation.isEmpty(addressType)) {
				getErrorMap().setError("addressType", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			DTObject formDTO = getFormDTO();
			formDTO.set("ADDR_TYPE", addressType);
			formDTO.set(ContentManager.USER_ACTION, MODIFY);
			formDTO = validateAddressTypeCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("addressType", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("addressType", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateAddressLine1() {
		MasterValidator validation = new MasterValidator();
		try {
			if (validation.isEmpty(addressLine1)) {
				getErrorMap().setError("addressLine1", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("addressLine1", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateAddressLine2() {
		MasterValidator validation = new MasterValidator();
		try {
			if (validation.isEmpty(addressLine2)) {
				getErrorMap().setError("addressLine2", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (validation.isEmpty(addressLine1) && (!validation.isEmpty(addressLine2))) {
				getErrorMap().setError("addressLine2", BackOfficeErrorCodes.HMS_ABOVELINE_BLANK);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("addressLine2", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateAddressLine3() {
		MasterValidator validation = new MasterValidator();
		try {
			if (validation.isEmpty(addressLine2) && (!validation.isEmpty(addressLine3))) {
				getErrorMap().setError("addressLine3", BackOfficeErrorCodes.HMS_ABOVELINE_BLANK);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("addressLine3", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateAddressLine4() {
		MasterValidator validation = new MasterValidator();
		try {
			if (validation.isEmpty(addressLine3) && (!validation.isEmpty(addressLine4))) {
				getErrorMap().setError("addressLine4", BackOfficeErrorCodes.HMS_ABOVELINE_BLANK);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("addressLine4", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public DTObject loadOldAddress(DTObject input) {
		CommonValidator validator = new CommonValidator();
		try {
			input = validator.loadOldAddress(input);
		} catch (Exception e) {

		} finally {
			validator.close();
		}

		return input;
	}

	public DTObject loadSearchGrid(DTObject input) {
		CommonValidator validator = new CommonValidator();
		try {
			input = validator.getSearchAddressDetail(input);
		} catch (Exception e) {

		} finally {
			validator.close();
		}

		return input;
	}

	public DTObject validatePersonId(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		MasterValidator validation = new MasterValidator();
		String personId = inputDTO.get("PERSON_ID");
		try {
			if (validation.isEmpty(personId)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "PERSON_NAME");
			resultDTO = validation.validatePersonId(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateAddressTypeCode() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("ADDR_TYPE", addressType);
			formDTO.set(ContentManager.USER_ACTION, USAGE);
			formDTO = validateAddressTypeCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("addressTypeCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("addressTypeCode", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		}
		return false;

	}

	public DTObject loadAddressDetails(DTObject input) {
		CommonValidator validator = new CommonValidator();
		try {
			input = validator.loadAddressDetails(input);
		} catch (Exception e) {

		} finally {
			validator.close();
		}

		return input;
	}

	public DTObject validateAddressTypeCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		MasterValidator validation = new MasterValidator();
		String addressTypeCode = inputDTO.get("ADDR_TYPE");
		try {
			if (validation.isEmpty(addressTypeCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
			resultDTO = validation.validateAddressTypeCode(inputDTO);
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public DTObject getLocation(DTObject inputDTO) {
		MasterValidator validation = new MasterValidator();
		DTObject resultDTO = new DTObject();
		DBContext dbContext = new DBContext();
		DBUtil dbutil = dbContext.createUtilInstance();
		String sqlQuery = null;
		try {
			inputDTO.set(ContentManager.USER_ACTION, "USAGE");
			inputDTO.set(ContentManager.FETCH_COLUMNS, "GEO_UNIT_STRUC_CODE,DESCRIPTION");
			resultDTO = validation.validateCountryCode(inputDTO);
			if (ContentManager.DATA_AVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set("COUNT_DESC", resultDTO.get("DESCRIPTION"));
				if (resultDTO.get("GEO_UNIT_STRUC_CODE") != null) {
					sqlQuery = "CALL SP_GET_GUS_STRUC_NAME(?,?,?,?)";
					dbutil.reset();
					dbutil.setMode(DBUtil.CALLABLE);
					dbutil.setSql(sqlQuery);
					dbutil.setString(1, resultDTO.get("GEO_UNIT_STRUC_CODE"));
					dbutil.registerOutParameter(2, Types.VARCHAR);
					dbutil.registerOutParameter(3, Types.VARCHAR);
					dbutil.registerOutParameter(4, Types.VARCHAR);
					dbutil.execute();
					if (dbutil.getString(4) == null) {
						resultDTO.set("V_FIRST_GUS", dbutil.getString(2));
						resultDTO.set("V_GUS_STRUCTURE", dbutil.getString(3));
						resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
					} else {
						resultDTO.set(ContentManager.ERROR, dbutil.getString(4));
					}
				}
			}
			return resultDTO;
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
			dbContext.close();
		}
		return resultDTO;
	}

	public DTObject getAddressLabels(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		DTObject result = new DTObject();
		MasterValidator validation = new MasterValidator();
		String addrType = inputDTO.get("ADDR_TYPE");
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		DBUtil dbutil = dbContext.createUtilInstance();
		String sqlQuery = null;
		try {
			sqlQuery = "SELECT * FROM ADDRESSTYPE WHERE ADDR_TYPE=?";
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, addrType);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				decodeRow(rset, resultDTO);
				inputDTO.set("COUNTRY_CODE", resultDTO.get("ADDR_COU_CODE"));
				inputDTO.set(ContentManager.USER_ACTION, "USAGE");
				inputDTO.set(ContentManager.FETCH_COLUMNS, "GEO_UNIT_STRUC_CODE,DESCRIPTION");
				result = validation.validateCountryCode(inputDTO);
				if (ContentManager.DATA_AVAILABLE.equals(result.get(ContentManager.RESULT))) {
					resultDTO.set("COUNT_DESC", result.get("DESCRIPTION"));
					if (result.get("GEO_UNIT_STRUC_CODE") != null) {
						sqlQuery = "CALL SP_GET_GUS_STRUC_NAME(?,?,?,?)";
						dbutil.reset();
						dbutil.setMode(DBUtil.CALLABLE);
						dbutil.setSql(sqlQuery);
						dbutil.setString(1, result.get("GEO_UNIT_STRUC_CODE"));
						dbutil.registerOutParameter(2, Types.VARCHAR);
						dbutil.registerOutParameter(3, Types.VARCHAR);
						dbutil.registerOutParameter(4, Types.VARCHAR);
						dbutil.execute();
						if (dbutil.getString(4) == null) {
							resultDTO.set("V_FIRST_GUS", dbutil.getString(2));
							resultDTO.set("V_GUS_STRUCTURE", dbutil.getString(3));
							resultDTO.set("GEO_UNIT_STRUC_CODE", result.get("GEO_UNIT_STRUC_CODE"));
							resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
						} else {
							resultDTO.set(ContentManager.ERROR, dbutil.getString(4));
						}
					}
				}
			}
			return resultDTO;
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
			dbContext.close();
		}
		return resultDTO;
	}

	public final DTObject decodeRow(ResultSet rset, DTObject result) throws SQLException {
		for (int i = 1; i < rset.getMetaData().getColumnCount() + 1; i++) {
			if (rset.getMetaData().getColumnTypeName(i).equals("DATE")) {
				result.setObject(rset.getMetaData().getColumnName(i), rset.getTimestamp(i));
			} else if (rset.getMetaData().getColumnTypeName(i).equals("BLOB")) {
				result.setObject(rset.getMetaData().getColumnName(i), rset.getBytes(i));
			} else {
				String value = rset.getString(i);
				if (value == null)
					value = "";
				result.set(rset.getMetaData().getColumnName(i), value);
			}
		}
		return result;
	}

	public boolean loadLinkProgramDetails() {
		CommonValidator validator = new CommonValidator();
		try {
			DTObject formDTO = getFormDTO();
			formDTO.set("PARENT_PGM_ID", parentProgramId);
			formDTO.set("LINKED_PGM_ID", linkProgramId);
			formDTO = validator.getLinkProgramDetails(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("addressType", BackOfficeErrorCodes.HMS_LINK_PGMCFG_UNAVAIL);
				return false;
			}
			if (formDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				getErrorMap().setError("addressType", BackOfficeErrorCodes.HMS_LINK_PGMCFG_UNAVAIL);
				return false;
			}
			returnType = formDTO.get("RETURN_TYPE");
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("addressType", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return true;
	}
}