package patterns.config.web.forms.common;

import java.util.ArrayList;

import patterns.config.delegate.BackOfficeProcessManager;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.pki.ProcessTFAInfo;
import patterns.config.framework.process.TBAProcessResult;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTDObject;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.GenericOption;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.MasterValidator;
import patterns.config.web.forms.GenericFormBean;

public class mcontactbean extends GenericFormBean {
	public mcontactbean() {
		setProcessBO("patterns.config.framework.bo.common.mcontactBO");
	}

	private static final long serialVersionUID = -6219657363150466160L;

	private String personId;
	private String contactType;
	private String landlineCountry;
	private String stdCode;
	private String landlineNumber;
	private String mobileCountry;
	private String mobileNumber;
	private String emailId;
	private boolean official;
	private String inventoryNumber;
	private String operationStatus;
	private String numberSearch;
	private String emailSearch;
	private String personSearch;
	private String currentTab;
	private String inventoryNumberList;
	private String conversationID;
	private String parentProgramId;
	private String parentPrimaryKey;
	private String linkProgramId;
	private String linkPrimaryKey;
	private String requestType;
	private String sourceKey;
	private String sourceValue;
	private String returnType;
	private String parentAction;

	public String getConversationID() {
		return conversationID;
	}

	public void setConversationID(String conversationID) {
		this.conversationID = conversationID;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getPersonId() {
		return personId;
	}

	public void setPersonId(String personId) {
		this.personId = personId;
	}

	public String getContactType() {
		return contactType;
	}

	public void setContactType(String contactType) {
		this.contactType = contactType;
	}

	public String getLandlineCountry() {
		return landlineCountry;
	}

	public void setLandlineCountry(String landlineCountry) {
		this.landlineCountry = landlineCountry;
	}

	public String getStdCode() {
		return stdCode;
	}

	public void setStdCode(String stdCode) {
		this.stdCode = stdCode;
	}

	public String getLandlineNumber() {
		return landlineNumber;
	}

	public void setLandlineNumber(String landlineNumber) {
		this.landlineNumber = landlineNumber;
	}

	public String getMobileCountry() {
		return mobileCountry;
	}

	public void setMobileCountry(String mobileCountry) {
		this.mobileCountry = mobileCountry;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public boolean isOfficial() {
		return official;
	}

	public void setOfficial(boolean official) {
		this.official = official;
	}

	public String getInventoryNumber() {
		return inventoryNumber;
	}

	public void setInventoryNumber(String inventoryNumber) {
		this.inventoryNumber = inventoryNumber;
	}

	public String getOperationStatus() {
		return operationStatus;
	}

	public void setOperationStatus(String operationStatus) {
		this.operationStatus = operationStatus;
	}

	public String getNumberSearch() {
		return numberSearch;
	}

	public void setNumberSearch(String numberSearch) {
		this.numberSearch = numberSearch;
	}

	public String getEmailSearch() {
		return emailSearch;
	}

	public void setEmailSearch(String emailSearch) {
		this.emailSearch = emailSearch;
	}

	public String getPersonSearch() {
		return personSearch;
	}

	public void setPersonSearch(String personSearch) {
		this.personSearch = personSearch;
	}

	public String getCurrentTab() {
		return currentTab;
	}

	public void setCurrentTab(String currentTab) {
		this.currentTab = currentTab;
	}

	public String getInventoryNumberList() {
		return inventoryNumberList;
	}

	public void setInventoryNumberList(String inventoryNumberList) {
		this.inventoryNumberList = inventoryNumberList;
	}

	public String getParentProgramId() {
		return parentProgramId;
	}

	public void setParentProgramId(String parentProgramId) {
		this.parentProgramId = parentProgramId;
	}

	public String getParentPrimaryKey() {
		return parentPrimaryKey;
	}

	public void setParentPrimaryKey(String parentPrimaryKey) {
		this.parentPrimaryKey = parentPrimaryKey;
	}

	public String getLinkProgramId() {
		return linkProgramId;
	}

	public void setLinkProgramId(String linkProgramId) {
		this.linkProgramId = linkProgramId;
	}

	public String getLinkPrimaryKey() {
		return linkPrimaryKey;
	}

	public void setLinkPrimaryKey(String linkPrimaryKey) {
		this.linkPrimaryKey = linkPrimaryKey;
	}

	public String getRequestType() {
		return requestType;
	}

	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}

	public String getSourceKey() {
		return sourceKey;
	}

	public void setSourceKey(String sourceKey) {
		this.sourceKey = sourceKey;
	}

	public String getSourceValue() {
		return sourceValue;
	}

	public void setSourceValue(String sourceValue) {
		this.sourceValue = sourceValue;
	}

	public String getReturnType() {
		return returnType;
	}

	public void setReturnType(String returnType) {
		this.returnType = returnType;
	}

	public String getParentAction() {
		return parentAction;
	}

	public void setParentAction(String parentAction) {
		this.parentAction = parentAction;
	}

	@Override
	public void reset() {
		personId = RegularConstants.EMPTY_STRING;
		contactType = RegularConstants.EMPTY_STRING;
		landlineCountry = RegularConstants.EMPTY_STRING;
		stdCode = RegularConstants.EMPTY_STRING;
		landlineNumber = RegularConstants.EMPTY_STRING;
		mobileCountry = RegularConstants.EMPTY_STRING;
		mobileNumber = RegularConstants.EMPTY_STRING;
		emailId = RegularConstants.EMPTY_STRING;
		official = false;
		currentTab = RegularConstants.EMPTY_STRING;

		DBContext dbContext = new DBContext();
		ArrayList<GenericOption> contactTypeList = null;
		contactTypeList = getGenericOptions("COMMON", "CONTACTTYPE", dbContext, "--");
		webContext.getRequest().setAttribute("COMMON_CONTACTTYPE", contactTypeList);
		dbContext.close();

		parentProgramId = webContext.getRequest().getParameter("_CGQMParentProgramId");
		parentPrimaryKey = webContext.getRequest().getParameter("_CGQMParentPK");
		linkProgramId = "MCONTACT";
		requestType = webContext.getRequest().getParameter("_CGQMRequestType");
		parentAction = webContext.getRequest().getParameter("_CGQMParentAction");
		sourceKey = webContext.getRequest().getParameter("_CGQMSourceKey");
		sourceValue = webContext.getRequest().getParameter("_CGQMSourceValue");
		conversationID = webContext.getRequest().getParameter("_CGQMConversationId");
		loadLinkProgramDetails();
		
	}

	@Override
	public void validate() {
		
		if (currentTab.equals("contact_0")) {
			if (!operationStatus.equals("R") && !operationStatus.equals("U"))  {
				validateContactType();
				validateCountryCode();
				validateStdCode();
				validateLandlinePhoneNumber();
				validateMobileNumber();
				validateEmailID();
			}
		} else {
			if (inventoryNumberList != null && !inventoryNumberList.equals(RegularConstants.EMPTY_STRING))
				validateGridInputs();
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("PARTITION_NO", context.getPartitionNo());
				formDTO.set("PARENT_PGM_ID", parentProgramId);
				formDTO.set("LINKED_PGM_ID", linkProgramId);
				formDTO.set("PARENT_PGM_PK", parentPrimaryKey);
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.set("CONTACT_TYPE", contactType);
				formDTO.set("LL_COUNTRY_CODE", landlineCountry);
				formDTO.set("LL_LOC_STD_CODE", stdCode);
				formDTO.set("LL_CONTACT_NUMBER", landlineNumber);
				formDTO.set("MOB_COUNTRY_CODE", mobileCountry);
				formDTO.set("MOB_CONTACT_NUMBER", mobileNumber);
				formDTO.set("CONVERSATION_ID", conversationID);
				if (operationStatus.equals("R")) {
					formDTO.set("ENABLED", decodeBooleanToString(false));
				} else {
					formDTO.set("ENABLED", decodeBooleanToString(true));
				}
				formDTO.set("OFFICIAL_USE", decodeBooleanToString(official));
				formDTO.set("EMAIL_ID", emailId);
				if (inventoryNumber != null && (!inventoryNumber.equals(RegularConstants.EMPTY_STRING))) {
					formDTO.set("CONTACT_INV_NO", inventoryNumber);
				}
				
				formDTO.set("OPERATION_TYPE", getAction());
				formDTO.set("OPERATION_STATUS", operationStatus);
				ProcessTFAInfo processTFAInfo = new ProcessTFAInfo();
				processTFAInfo.setUserId(context.getUserID());
				processTFAInfo.setDeploymentContext(context.getDeploymentContext());
				processTFAInfo.setDigitalCertificateInventoryNumber(context.getDigitalCertificateInventoryNumber());
				processTFAInfo.setTfaRequired(getTfaRequired());
				processTFAInfo.setTfaSuccess(getTfaSuccess());
				processTFAInfo.setTfaValue(getTfaValue());
				processTFAInfo.setTfaEncodedValue(getTfaEncodedValue());
				BackOfficeProcessManager processManager = new BackOfficeProcessManager();
				formDTO.set("PARTITION_NO", context.getPartitionNo());
				formDTO.set("USER_ID", context.getUserID());
				formDTO.set("IP_ADDRESS", context.getClientIP());
				formDTO.setObject("CBD", context.getCurrentBusinessDate());
				formDTO.setObject("PKI", processTFAInfo);
				formDTO.set(RegularConstants.PROCESSBO_CLASS, getProcessBO());
				TBAProcessResult processResult = processManager.delegate(formDTO);
				if (processResult.getProcessStatus().equals(TBAProcessStatus.SUCCESS)) {
					if (processResult.getResponseDTO() != null) {
						if (processResult.getResponseDTO().get(RegularConstants.ERROR) != null) {
							getErrorMap().setError("personId", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
							return;
						} else if (processResult.getResponseDTO().get(RegularConstants.ADDITIONAL_INFO) != null) {
							getErrorMap().setError("personId", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
							return;
						}
					}
					if (processResult.getAdditionalInfo() != null) {
						inventoryNumber = processResult.getAdditionalInfo().toString();
					}
				} else {
					getErrorMap().setError("personId", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("personId", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
	}

	public DTObject updateInventory(DTObject input) {
		input.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		try {
			personId = input.get("PERSON_ID");
			contactType = input.get("CONTACT_TYPE");
			setAction(input.get("OPERATION_TYPE"));
			landlineCountry = input.get("LL_COUNTRY");
			stdCode = input.get("LL_STD_CODE");
			landlineNumber = input.get("LL_NUMBER");
			mobileCountry = input.get("MOB_COUNTRY");
			mobileNumber = input.get("MOB_NUMBER");
			emailId = input.get("EMAIL_ID");
			official = decodeStringToBoolean(input.get("OFFICIAL"));
			inventoryNumber = input.get("INVENTORY_NO");
			operationStatus = input.get("OPERATION_STATUS");
			currentTab = input.get("ACTIVE_TAB");

			parentProgramId = input.get("PARENT_PGM_ID");
			parentPrimaryKey = input.get("PARENT_PGM_PK");
			linkProgramId = input.get("LINKED_PGM_ID");
			conversationID = input.get("CONVERSATION_ID");

			validate();
			if (getErrorMap().length() == 0) {
				input.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				input.set("INVENTORY_NO", inventoryNumber);
			}
		} catch (Exception e) {

		}
		return input;
	}


	public boolean validateContactType() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(contactType)) {
				getErrorMap().setError("contactType", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isCMLOVREC("COMMON", "CONTACTTYPE", contactType)) {
				getErrorMap().setError("contactType", BackOfficeErrorCodes.HMS_INVALID_CONTACT_TYPE);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("contactType", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateCountryCode() {
		if (contactType.equals("E")) {
			return true;
		}
		String value = "";
		if (contactType.equals("L")) {
			value = landlineCountry;
		} else {
			value = mobileCountry;
		}
		return true;
	}

	public boolean validateStdCode() {
		if (contactType.equals("E") || contactType.equals("M")) {
			return true;
		}
		return true;
	}

	public boolean validateLandlinePhoneNumber() {
		if (contactType.equals("E") || contactType.equals("M")) {
			return true;
		}
		CommonValidator validator = new CommonValidator();
		try {
			if (!validator.isValidTelephoneNumber(landlineNumber)) {
				getErrorMap().setError("landlineNumber", BackOfficeErrorCodes.INVALID_PHONE_OPR);
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("landlineNumber", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return true;
	}

	public boolean validateMobileNumber() {
		if (contactType.equals("E") || contactType.equals("L")) {
			return true;
		}
		CommonValidator validator = new CommonValidator();
		try {
			if (!validator.isValidPhoneNumber(mobileNumber)) {
				getErrorMap().setError("mobileNumber", BackOfficeErrorCodes.INVALID_MOBILE_NUMBER);
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("mobileNumber", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return true;
	}

	public boolean validateEmailID() {
		if (contactType.equals("L") || contactType.equals("M")) {
			return true;
		}
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(emailId)) {
				getErrorMap().setError("emailId", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isEmpty(emailId)) {
				if (!validation.isValidEmail(emailId)) {
					getErrorMap().setError("emailId", BackOfficeErrorCodes.INVALID_EMAIL_ID);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("emailId", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateGridInputs() {
		CommonValidator validator = new CommonValidator();
		try {

			DTDObject dtdObject = new DTDObject();
			dtdObject.addColumn(0, "SELECT");
			dtdObject.addColumn(1, "OFFICIAL");
			dtdObject.addColumn(2, "INVNO");
			dtdObject.setXML(inventoryNumberList);
			if (dtdObject.getRowCount() == 0) {
				getErrorMap().setError("personId", BackOfficeErrorCodes.ADD_ATLEAST_ONE_ROW);
				return false;
			}
			if (!validator.checkDuplicateRows(dtdObject, 2)) {
				getErrorMap().setError("personId", BackOfficeErrorCodes.DUPLICATE_DATA);
				return false;
			}

			int flag = 0;
			for (int i = 0; i < dtdObject.getRowCount(); ++i) {
				boolean optionEnabled = decodeStringToBoolean(dtdObject.getValue(i, 0));
				if (optionEnabled) {
					if (personId != null && !personId.equals(RegularConstants.EMPTY_STRING)) {
						if (validator.validatePersonContactNumber(personId, dtdObject.getValue(i, 2))) {
							getErrorMap().setError("personId", BackOfficeErrorCodes.HMS_CONTACT_ALREADY_AVL);
							return false;
						}
					}
					flag++;
				}
			}
			if (flag == 0) {
				getErrorMap().setError("personId", BackOfficeErrorCodes.CHECK_ATLEAST_ONE);
				return false;
			}
		} catch (Exception e) {
			getErrorMap().setError("personId", BackOfficeErrorCodes.ERROR_IN_GRID);
		} finally {
			validator.close();
		}
		return true;
	}

	public boolean validateInventoryNumber(String inventoryNo) {
		if (requestType.equals("T")) {
			return true;
		}
		MasterValidator validation = new MasterValidator();
		try {
			DTObject formDTO = getFormDTO();
			formDTO.set("INV_NO", inventoryNo);
			formDTO = validation.validateContactInventoryNumber(formDTO);

			if (formDTO.get(ContentManager.ERROR) != null) {
				return false;
			}
			if (formDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			validation.close();
		}
		return false;
	}

	public DTObject loadGrid(DTObject input) {
		CommonValidator validator = new CommonValidator();
		try {
			input = validator.getContactDetailGrid(input);
		} catch (Exception e) {

		} finally {
			validator.close();
		}

		return input;
	}

	public DTObject loadSearchGrid(DTObject input) {
		CommonValidator validator = new CommonValidator();
		try {
			input = validator.getSearchContactDetail(input);
		} catch (Exception e) {

		} finally {
			validator.close();
		}

		return input;
	}

	public DTObject loadContactDetails(DTObject input) {
		CommonValidator validator = new CommonValidator();
		try {
			input = validator.loadContactDetails(input);
		} catch (Exception e) {

		} finally {
			validator.close();
		}

		return input;
	}

	public boolean loadLinkProgramDetails() {
		CommonValidator validator = new CommonValidator();
		try {
			DTObject formDTO = getFormDTO();
			formDTO.set("PARENT_PGM_ID", parentProgramId);
			formDTO.set("LINKED_PGM_ID", linkProgramId);
			formDTO = validator.getLinkProgramDetails(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("contactType", BackOfficeErrorCodes.HMS_LINK_PGMCFG_UNAVAIL);
				return false;
			}
			if (formDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				getErrorMap().setError("contactType", BackOfficeErrorCodes.HMS_LINK_PGMCFG_UNAVAIL);
				return false;
			}
			returnType = formDTO.get("RETURN_TYPE");
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("contactType", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return true;
	}
}