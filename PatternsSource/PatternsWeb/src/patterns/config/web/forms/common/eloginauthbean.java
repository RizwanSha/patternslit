package patterns.config.web.forms.common;

import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.PasswordUtils;
import patterns.config.framework.web.SessionConstants;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.AccessValidator;
import patterns.config.web.forms.GenericFormBean;

public class eloginauthbean extends GenericFormBean {

	private static final long serialVersionUID = -6219657363150466160L;

	private String userID;
	private String randomSalt;
	private String password;
	private String passwordSalt;
	private String hashedPassword;
	private String multiSessionCheck;
	private String tmpOrgId;

	public String getMultiSessionCheck() {
		return multiSessionCheck;
	}

	public void setMultiSessionCheck(String multiSessionCheck) {
		this.multiSessionCheck = multiSessionCheck;
	}

	public String getRandomSalt() {
		return randomSalt;
	}

	public void setRandomSalt(String randomSalt) {
		this.randomSalt = randomSalt;
	}

	public String getPasswordSalt() {
		return passwordSalt;
	}

	public void setPasswordSalt(String passwordSalt) {
		this.passwordSalt = passwordSalt;
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public void setHashedPassword(String hashedPassword) {
		this.hashedPassword = hashedPassword;
	}

	public String getHashedPassword() {
		return hashedPassword;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getTmpOrgId() {
		return tmpOrgId;
	}

	public void setTmpOrgId(String tmpOrgId) {
		this.tmpOrgId = tmpOrgId;
	}

	@Override
	public void reset() {
		hashedPassword = ContentManager.EMPTY_STRING;
		password = ContentManager.EMPTY_STRING;
		userID = (String) webContext.getSession().getAttribute(SessionConstants.USER_ID);
		tmpOrgId = (String) webContext.getSession().getAttribute(SessionConstants.ENTITY_CODE) + "|" + (String) webContext.getSession().getAttribute(SessionConstants.ORG_NAME);
		multiSessionCheck = (String) webContext.getSession().getAttribute(SessionConstants.MULTI_SESSION_CHECK);
		passwordSalt = (String) webContext.getSession().getAttribute(SessionConstants.PASSWORD_SALT);
		if (!context.isFormPosted()) {
			randomSalt = PasswordUtils.getSalt();
			webContext.getSession().setAttribute(SessionConstants.RANDOM_SALT, randomSalt);
		}
	}

	@Override
	public void validate() {
		userID = (String) webContext.getSession().getAttribute(SessionConstants.USER_ID);
		String passwordValid = decodeBooleanToString(isPasswordValid());
		passwordValid = "1";

		setProcessBO("patterns.config.framework.bo.common.eloginauthBO");
		if (userID == null) {
			userID = ContentManager.EMPTY_STRING;
		}
		if (hashedPassword == null) {
			hashedPassword = ContentManager.EMPTY_STRING;
		}

		DTObject formDTO = getFormDTO();
		formDTO.set("P_ENTITY_CODE", context.getEntityCode());
		formDTO.set("P_PARTITION_NO", context.getPartitionNo());
		formDTO.set("P_USER_ID", userID);
		formDTO.set("P_FIRST_PIN_VALID", passwordValid);
		formDTO.set("P_TFA_VALID", "1");
		formDTO.set("P_USER_IP", webContext.getRequest().getRemoteAddr());
		formDTO.set("P_USER_AGENT", webContext.getRequest().getHeader("User-Agent"));
		formDTO.set("P_LOGIN_SESSION_ID", webContext.getSession().getId());
		formDTO.set("P_SERVER_ADDR", webContext.getRequest().getServerName());
		formDTO.set("P_MULTI_SESSION_CHECK", getMultiSessionCheck());
	}

	public boolean isPasswordValid() {
		randomSalt = (String) webContext.getSession().getAttribute(SessionConstants.RANDOM_SALT);
		String userPassword = RegularConstants.EMPTY_STRING;
		AccessValidator validator = new AccessValidator();
		try {
			userPassword = validator.getInternalUserPassword(userID);
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError("isPasswordValid(1)" + e.getLocalizedMessage());
		} finally {
			validator.close();
		}

		String encryptedValue = PasswordUtils.getEncryptedHashSalt(userPassword, randomSalt);
		logger.logDebug("Hashed Value : " + hashedPassword);
		logger.logDebug("Encrypted Value : " + encryptedValue);
		if (encryptedValue.equals(hashedPassword)) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public boolean isTFAHandlingSelfManaged() {
		return true;
	}
}