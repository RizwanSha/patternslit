package patterns.config.web.forms.common;

import java.sql.ResultSet;
import java.util.Date;
import java.util.Enumeration;

import javax.servlet.http.HttpSession;

import patterns.config.delegate.BackOfficeProcessManager;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.process.TBAProcessResult;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.framework.web.RequestConstants;
import patterns.config.framework.web.SessionConstants;
import patterns.config.web.forms.GenericFormBean;

public class elogoutbean extends GenericFormBean {
	

	private static final long serialVersionUID = -6219657363150466160L;

	@SuppressWarnings("rawtypes")
	@Override
	public void reset() {
		HttpSession session = webContext.getSession();
		if (session != null) {
			String errorMessage = (String) session.getAttribute(RequestConstants.ADDITIONAL_INFO);
			webContext.getRequest().setAttribute(RequestConstants.ADDITIONAL_INFO, errorMessage);
			String entityCode = context.getEntityCode();
			String userID = context.getUserID();
			Date loginDateTime = (Date) session.getAttribute(SessionConstants.LOGIN_DATE_TIME);
			if (userID != null && !userID.trim().equals(RegularConstants.EMPTY_STRING)) {
				DTObject formDTO = new DTObject();
				formDTO.set(RegularConstants.PROCESSBO_CLASS, "patterns.config.framework.bo.common.elogoutBO");
				formDTO.set("P_ENTITY_CODE", entityCode);
				formDTO.set("P_USER_ID", userID);
				formDTO.setObject("P_LOGIN_DATE", loginDateTime);
				formDTO.set("P_REASON", RegularConstants.MANUAL_LOGOUT);
				formDTO.set("P_SESSION_ID", session.getId());
				BackOfficeProcessManager processManager = new BackOfficeProcessManager();
				TBAProcessResult processResult = processManager.delegate(formDTO);
				if (processResult.getProcessStatus().equals(TBAProcessStatus.FAILURE)) {
					logger.logInfo("Logout Failure");
				} else {
					logger.logInfo("Logout Success");
				}
			}
			webContext.getRequest().setAttribute("loginDate",BackOfficeFormatUtils.getDate(loginDateTime,"EEEE dd,MMMM YYYY,HH:mm:ss "));
			webContext.getRequest().setAttribute("logoutDate",getlogoutDateTime());
			Enumeration enumeration = session.getAttributeNames();
			while (enumeration.hasMoreElements()) {
				String key = (String) enumeration.nextElement();
				session.removeAttribute(key);
			}
			// session.invalidate();
		}
	}

	@Override
	public void validate() {

	}
	
	public String getlogoutDateTime() {
		String logoutDateTime=null;
		DBContext dbContext=new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		try {
			String sql = "SELECT NOW() FROM DUAL";
			util.reset();
			util.setMode(DBUtil.PREPARED);
			util.setSql(sql);
			ResultSet rs = util.executeQuery();
			if (rs.next()) {
				logoutDateTime=BackOfficeFormatUtils.getDate(rs.getTimestamp(1), "EEEE dd,MMMM YYYY,HH:mm:ss ");
			}
			return logoutDateTime;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			util.reset();
			dbContext.close();
		}
		return logoutDateTime;
	}

}
