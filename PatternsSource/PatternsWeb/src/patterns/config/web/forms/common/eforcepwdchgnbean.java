package patterns.config.web.forms.common;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.CM_LOVREC;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.PasswordUtils;
import patterns.config.framework.web.RequestConstants;
import patterns.config.framework.web.SessionConstants;
import patterns.config.validations.AccessValidator;
import patterns.config.web.forms.GenericFormBean;

public class eforcepwdchgnbean extends GenericFormBean {

	private static final long serialVersionUID = -6219657363150466160L;

	private String minLength;
	private String minAlpha;
	private String minNumeric;
	private String minSpecial;
	private String minPrevent;
	private String passwordPolicy;
	private String loginCurrentPassword;
	private String loginCurrentPasswordSalt;
	private String loginCurrentPasswordHashed;

	private String loginNewPassword;
	private String loginNewPasswordHashed;
	private String loginRenewPassword;
	private String randomSalt;

	public String getMinLength() {
		return minLength;
	}

	public void setMinLength(String minLength) {
		this.minLength = minLength;
	}

	public String getMinAlpha() {
		return minAlpha;
	}

	public void setMinAlpha(String minAlpha) {
		this.minAlpha = minAlpha;
	}

	public String getMinNumeric() {
		return minNumeric;
	}

	public void setMinNumeric(String minNumeric) {
		this.minNumeric = minNumeric;
	}

	public String getMinSpecial() {
		return minSpecial;
	}

	public void setMinSpecial(String minSpecial) {
		this.minSpecial = minSpecial;
	}

	public String getMinPrevent() {
		return minPrevent;
	}

	public void setMinPrevent(String minPrevent) {
		this.minPrevent = minPrevent;
	}

	public String getLoginCurrentPassword() {
		return loginCurrentPassword;
	}

	public void setLoginCurrentPassword(String loginCurrentPassword) {
		this.loginCurrentPassword = loginCurrentPassword;
	}

	public String getLoginCurrentPasswordSalt() {
		return loginCurrentPasswordSalt;
	}

	public void setLoginCurrentPasswordSalt(String loginCurrentPasswordSalt) {
		this.loginCurrentPasswordSalt = loginCurrentPasswordSalt;
	}

	public String getLoginCurrentPasswordHashed() {
		return loginCurrentPasswordHashed;
	}

	public void setLoginCurrentPasswordHashed(String loginCurrentPasswordHashed) {
		this.loginCurrentPasswordHashed = loginCurrentPasswordHashed;
	}

	public String getLoginNewPassword() {
		return loginNewPassword;
	}

	public void setLoginNewPassword(String loginNewPassword) {
		this.loginNewPassword = loginNewPassword;
	}

	public String getLoginNewPasswordHashed() {
		return loginNewPasswordHashed;
	}

	public void setLoginNewPasswordHashed(String loginNewPasswordHashed) {
		this.loginNewPasswordHashed = loginNewPasswordHashed;
	}

	public String getLoginRenewPassword() {
		return loginRenewPassword;
	}

	public void setLoginRenewPassword(String loginRenewPassword) {
		this.loginRenewPassword = loginRenewPassword;
	}

	public String getRandomSalt() {
		return randomSalt;
	}

	public void setRandomSalt(String randomSalt) {
		this.randomSalt = randomSalt;
	}

	@Override
	public void reset() {
		String errorMessage = (String) webContext.getSession().getAttribute(RequestConstants.ADDITIONAL_INFO);
		webContext.getRequest().setAttribute(RequestConstants.ADDITIONAL_INFO, errorMessage);
		webContext.getSession().removeAttribute(RequestConstants.ADDITIONAL_INFO);
		loginCurrentPasswordHashed = RegularConstants.EMPTY_STRING;
		loginCurrentPassword = RegularConstants.EMPTY_STRING;
		loginNewPassword = RegularConstants.EMPTY_STRING;
		loginRenewPassword = RegularConstants.EMPTY_STRING;
		setProcessBO("patterns.config.framework.bo.common.eforcepwdchgnBO");
		AccessValidator validator = new AccessValidator();
		try {
			DTObject formDTO = getFormDTO();
			formDTO = validator.getSYSCPM(formDTO);
			minLength = formDTO.get("MIN_PWD_LEN");
			minNumeric = formDTO.get("MIN_PWD_NUM");
			minAlpha = formDTO.get("MIN_PWD_ALPHA");
			minSpecial = formDTO.get("MIN_PWD_SPECIAL");
			minPrevent = formDTO.get("PREVENT_PREV_PWD");
			setPasswordPolicy(formDTO.get("PASSWORD_POLICY"));
			loginCurrentPasswordSalt = validator.getInternalUserPasswordSalt();
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError("reset(1) : " + e.getLocalizedMessage());
		} finally {
			validator.close();
		}
		if (!context.isFormPosted()) {
			randomSalt = PasswordUtils.getSalt();
			webContext.getSession().setAttribute(SessionConstants.RANDOM_SALT, randomSalt);
		}
		randomSalt = (String) webContext.getSession().getAttribute(SessionConstants.RANDOM_SALT);
	}

	@Override
	public void validate() {
		validatePassword();

		if (getErrorMap().length() == 0) {
			DTObject formDTO = getFormDTO();
			formDTO.set("P_PARTITION_NO", context.getPartitionNo());
			formDTO.set("P_ENTITY_CODE", context.getEntityCode());
			formDTO.set("P_USER_ID", context.getUserID());
			String salt = PasswordUtils.getSalt();
			String encryptedPassword = PasswordUtils.getEncryptedHashSalt(loginNewPasswordHashed, salt);
			formDTO.set("P_LOGIN_NEW_PIN", encryptedPassword);
			formDTO.set("P_LOGIN_PIN_SALT", salt);
			formDTO.set("P_AUTH_USER_ID", context.getUserID());
			formDTO.set("P_REMARKS", "CHANGE @ FORCED PASSWORD CHANGE");
			formDTO.set("P_CHANGE_REQD", RegularConstants.COLUMN_DISABLE);
			formDTO.set("P_DELIVERY_REQD", RegularConstants.COLUMN_DISABLE);
			formDTO.set("P_CLEAR_PIN", RegularConstants.NULL);
		}
	}

	public boolean validatePassword() {
		AccessValidator validation = new AccessValidator();
		try {
			String randomSalt = (String) webContext.getSession().getAttribute(SessionConstants.RANDOM_SALT);
			String userCurrentPassword = validation.getInternalUserPassword(context.getUserID());
			String encryptedValue = PasswordUtils.getEncryptedHashSalt(userCurrentPassword, randomSalt);
			if (!encryptedValue.equals(loginCurrentPasswordHashed)) {
				getErrorMap().setError("loginCurrentPassword", BackOfficeErrorCodes.INVALID_CREDENTIALS);
				return false;
			}
			if (validation.isPasswordFromPreviousList(loginNewPasswordHashed, CM_LOVREC.COMMON_PINTYPE_L, minPrevent)) {
				getErrorMap().setError("loginNewPassword", BackOfficeErrorCodes.PREV_PWD_NOT_ALLOWED);
				return false;
			}
			if (validation.isRestrictedPassword(loginNewPasswordHashed)) {
				getErrorMap().setError("loginNewPassword", BackOfficeErrorCodes.RESTRICTED_PWD_NOT_ALLOWED);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			validation.close();
		}
		return false;
	}

	public void setPasswordPolicy(String passwordPolicy) {
		this.passwordPolicy = passwordPolicy;
	}

	public String getPasswordPolicy() {
		return passwordPolicy;
	}

}
