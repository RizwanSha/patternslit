package patterns.config.web.forms.common;

import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.SessionConstants;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.web.forms.GenericFormBean;

public class eloginbean extends GenericFormBean {

	private static final long serialVersionUID = -6219657363150466160L;

	private String userID;
	private String tmpOrgId;

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public String getTmpOrgId() {
		return tmpOrgId;
	}

	public void setTmpOrgId(String tmpOrgId) {
		this.tmpOrgId = tmpOrgId;
	}

	@Override
	public void reset() {
		setProcessBO("patterns.config.framework.bo.common.eloginBO");
		userID = ContentManager.EMPTY_STRING;
		if (!context.isFormPosted()) {
			webContext.getSession().setMaxInactiveInterval(SessionConstants.MAX_LOGIN_INACTIVE_INTERVAL_SECONDS);
			webContext.getSession().setAttribute(SessionConstants.TEMP_SESSION_START, RegularConstants.COLUMN_ENABLE);
		}
	}

	@Override
	public void validate() {
		if (userID == null)
			userID = ContentManager.EMPTY_STRING;
		userID = userID.toUpperCase(context.getLocale());
		DTObject formDTO = getFormDTO();
		formDTO.set("P_PARTITION_NO", context.getPartitionNo());
		formDTO.set("P_USER_ID", userID);
		formDTO.set("P_USER_IP", webContext.getRequest().getRemoteAddr());
		formDTO.set("P_USER_AGENT", webContext.getRequest().getHeader("User-Agent"));
		formDTO.set("P_LOGIN_SESSION_ID", webContext.getSession().getId());
		formDTO.set("P_SERVER_ADDR", webContext.getRequest().getServerName());
	}
}