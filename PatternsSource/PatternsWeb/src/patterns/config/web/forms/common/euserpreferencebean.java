package patterns.config.web.forms.common;

import java.sql.ResultSet;
import java.util.ArrayList;





import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.GenericOption;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.framework.web.reports.ReportConstants;
import patterns.config.web.actions.GenericAction;
import patterns.config.web.forms.GenericFormBean;

public class euserpreferencebean extends GenericFormBean {

	private static final long serialVersionUID = 1L;
    private String mode;
	private String hidPrintStationeryType;
	public String getHidPrintStationeryType() {
		return hidPrintStationeryType;
	}

	public void setHidPrintStationeryType(String hidPrintStationeryType) {
		this.hidPrintStationeryType = hidPrintStationeryType;
	}
	
	@Override
	public void reset() {
		setProcessBO("patterns.config.framework.bo.common.euserpreferenceBO");
		mode=RegularConstants.EMPTY_STRING;
		DBContext dbContext = new DBContext();
		ArrayList<GenericOption> activityList = null;
		activityList = getGenericOptions("COMMON", "PRINT_STATIONERY_TYPES", dbContext, null);
		webContext.getRequest().setAttribute("COMMON_PRINT_STATIONERY_TYPES", activityList);
		setHidPrintStationeryType(RegularConstants.DEFAULT_STATIONERY_TYPE);
		getUserPreference(dbContext);
		dbContext.close();
	}

	@Override
	public void validate() {
		setCommand(RESET_ACTION);
		try {
			DTObject formDTO = getFormDTO();
			if(hidPrintStationeryType.equals(RegularConstants.EMPTY_STRING)){
				formDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
			}
			else{
				formDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.set("USER_ID", context.getUserID());
				formDTO.set("PRINT_STATIONERY_TYPE", hidPrintStationeryType);
				formDTO.set("MODE", mode);
				setCommand(GenericAction.SUCCESS_ACTION);
			}
			
		} catch (Exception e) {
			getErrorMap().setError("hidPrintStationeryType", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
			e.printStackTrace();
		} finally {
			
		}
	}
	
	public void getUserPreference(DBContext dbContext) {
		String sqlQuery = "SELECT PRINT_STATIONERY_TYPE FROM USERPREFERENCES WHERE ENTITY_CODE = ? AND USER_ID = ?";
		DBUtil util = dbContext.createUtilInstance();
		try {
				util.setMode(DBUtil.PREPARED);
				util.setSql(sqlQuery);
				util.setString(1, context.getEntityCode());
				util.setString(2, context.getUserID());
				ResultSet rs = util.executeQuery();
				if(rs.next()){
					//setHidPrintStationeryType(rs.getString("PRINT_STATIONERY_TYPE")!=null?rs.getString("PRINT_STATIONERY_TYPE"):ReportConstants.DEFAULT_STATIONERY_TYPE);
					webContext.getRequest().setAttribute("PRINT_STATIONERY_TYPE", rs.getString("PRINT_STATIONERY_TYPE")!=null?rs.getString("PRINT_STATIONERY_TYPE"):RegularConstants.DEFAULT_STATIONERY_TYPE);
					mode="U";
				}else{
					mode="I";
					webContext.getRequest().setAttribute("PRINT_STATIONERY_TYPE", RegularConstants.DEFAULT_STATIONERY_TYPE);
				}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			util.reset();
		}
	}
}