package patterns.config.web.forms.common;

import java.sql.ResultSet;
import java.util.ArrayList;

import javax.servlet.http.HttpSession;

import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.GenericOption;
import patterns.config.framework.web.SessionConstants;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.framework.web.configuration.menu.MenuUtils;
import patterns.config.validations.AccessValidator;
import patterns.config.web.actions.GenericAction;
import patterns.config.web.forms.GenericFormBean;

public class eroleupdationbean extends GenericFormBean {

	private static final long serialVersionUID = 1L;

	private String hidRoleCode;
	public String roleAvailable;

	public String getRoleAvailable() {
		return roleAvailable;
	}

	public void setRoleAvailable(String roleAvailable) {
		this.roleAvailable = roleAvailable;
	}

	public String getHidRoleCode() {
		return hidRoleCode;
	}

	public void setHidRoleCode(String hidRoleCode) {
		this.hidRoleCode = hidRoleCode;
	}

	@Override
	public void reset() {
	}

	@Override
	public void validate() {
		setProcessBO("patterns.config.framework.bo.common.eroleupdationBO");
		setCommand(RESET_ACTION);
		HttpSession session = webContext.getSession();
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		// added by swaroopa as on 14-05-2012 begins
		AccessValidator validator = new AccessValidator(dbContext);
		String loginAuthError = RegularConstants.EMPTY_STRING;
		// added by swaroopa as on 14-05-2012 ends
		try {
			MenuUtils menuUtils = new MenuUtils();
			DTObject result = menuUtils.validateRoleCode(hidRoleCode); // call validation
			if (result.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
				String sqlQuery = "SELECT ROLE_TYPE FROM ROLES WHERE ENTITY_CODE =? AND CODE =?";
				String roleType = RegularConstants.EMPTY_STRING;
				util.setMode(DBUtil.PREPARED);
				util.setSql(sqlQuery);
				util.setString(1, context.getEntityCode());
				util.setString(2, hidRoleCode);
				ResultSet rs = util.executeQuery();
				if (rs.next())
					roleType = rs.getString(1);
				if (!(hidRoleCode.equals(RegularConstants.EMPTY_STRING) || roleType.equals(RegularConstants.EMPTY_STRING))) {
					session.setAttribute(SessionConstants.ROLE_TYPE, roleType);
					session.setAttribute(SessionConstants.ROLE_CODE, hidRoleCode);
					
					session.removeAttribute(SessionConstants.CONSOLE_LIST);
					session.removeAttribute(SessionConstants.CONSOLE_CODE);

					ArrayList<GenericOption> consoleList = menuUtils.getConsoleList(roleType,hidRoleCode);
					session.setAttribute(SessionConstants.CONSOLE_LIST, consoleList);
					if (consoleList.size() > 0) {
						session.setAttribute(SessionConstants.CONSOLE_CODE, consoleList.get(0).getId());
					} else {
						session.setAttribute(SessionConstants.CONSOLE_CODE, RegularConstants.EMPTY_STRING);
					}
					setCommand(GenericAction.SUCCESS_ACTION);
					// check tfa for the new role
					// added by swaroopa as on 14-05-2012 begins

				/*	DTObject resultDTO = validator.tfaRequiredForRole(hidRoleCode, context.getUserID());
					if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
						session.setAttribute(SessionConstants.TFA_REQ, resultDTO.get(SessionConstants.TFA_REQ));
						session.setAttribute(SessionConstants.LOGIN_TFA_REQ, resultDTO.get(SessionConstants.LOGIN_TFA_REQ));
						if (resultDTO.get(SessionConstants.TFA_REQ).equals(RegularConstants.COLUMN_ENABLE)) {
							session.setAttribute(SessionConstants.CERT_INV_NUM, resultDTO.get(SessionConstants.CERT_INV_NUM));
							loginAuthError = resultDTO.get(ContentManager.ERROR);
						}
					}*/

					// added by swaroopa as on 14-05-2012 ends
				}
			} else {
				setCommand(GenericAction.FAILURE_ACTION);
			}
			DTObject formDTO = getFormDTO();
			formDTO.set("ENTITY_CODE", context.getEntityCode());
			formDTO.set("USER_ID", context.getUserID());
			formDTO.set("ROLE_CODE", hidRoleCode);
			formDTO.set(ContentManager.ERROR, loginAuthError);// added by swaroopa as on 14-05-2012
			if (getCommand().equals(GenericAction.SUCCESS_ACTION))
				formDTO.set("ROLE_AVAILABLE", RegularConstants.COLUMN_ENABLE);
			else
				formDTO.set("ROLE_AVAILABLE", RegularConstants.COLUMN_DISABLE);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			util.reset();
			dbContext.close();
		}
	}
}