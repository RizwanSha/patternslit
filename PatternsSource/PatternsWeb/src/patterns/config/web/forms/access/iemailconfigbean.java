package patterns.config.web.forms.access;

import java.util.ArrayList;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.process.TBAActionType;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.framework.web.GenericOption;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.AccessValidator;
import patterns.config.validations.CommonValidator;
import patterns.config.web.forms.GenericFormBean;

public class iemailconfigbean extends GenericFormBean {

	private static final long serialVersionUID = 1L;
	private String emailCode;
	private String effectiveDate;
	private String sendProtocol;
	private boolean sslRequired;
	private String receiveProtocol;
	private String senderName;
	private String senderEmail;
	private boolean proxyRequired;
	private String proxyServer;
	private String proxyServerPort;
	private boolean proxyServerAuthentication;
	private String proxyAccountName;
	private String proxyAccountPassword;
	private String outgoingMailServer;
	private String outgoingMailServerPort;
	private boolean outgoingMailServerAuthentication;
	private String outgoingAccountName;
	private String outgoingAccountPassword;
	private String remarks;
	private String sendAllowed;
	private String receiveAlloewd;

	public String getEmailCode() {
		return emailCode;
	}

	public void setEmailCode(String emailCode) {
		this.emailCode = emailCode;
	}

	public String getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public String getSendProtocol() {
		return sendProtocol;
	}

	public void setSendProtocol(String sendProtocol) {
		this.sendProtocol = sendProtocol;
	}

	public boolean isSslRequired() {
		return sslRequired;
	}

	public void setSslRequired(boolean sslRequired) {
		this.sslRequired = sslRequired;
	}

	public String getReceiveProtocol() {
		return receiveProtocol;
	}

	public void setReceiveProtocol(String receiveProtocol) {
		this.receiveProtocol = receiveProtocol;
	}

	public String getSenderName() {
		return senderName;
	}

	public void setSenderName(String senderName) {
		this.senderName = senderName;
	}

	public String getSenderEmail() {
		return senderEmail;
	}

	public void setSenderEmail(String senderEmail) {
		this.senderEmail = senderEmail;
	}

	public boolean isProxyRequired() {
		return proxyRequired;
	}

	public void setProxyRequired(boolean proxyRequired) {
		this.proxyRequired = proxyRequired;
	}

	public String getProxyServer() {
		return proxyServer;
	}

	public void setProxyServer(String proxyServer) {
		this.proxyServer = proxyServer;
	}

	public String getProxyServerPort() {
		return proxyServerPort;
	}

	public void setProxyServerPort(String proxyServerPort) {
		this.proxyServerPort = proxyServerPort;
	}

	public boolean isProxyServerAuthentication() {
		return proxyServerAuthentication;
	}

	public void setProxyServerAuthentication(boolean proxyServerAuthentication) {
		this.proxyServerAuthentication = proxyServerAuthentication;
	}

	public String getProxyAccountName() {
		return proxyAccountName;
	}

	public void setProxyAccountName(String proxyAccountName) {
		this.proxyAccountName = proxyAccountName;
	}

	public String getProxyAccountPassword() {
		return proxyAccountPassword;
	}

	public void setProxyAccountPassword(String proxyAccountPassword) {
		this.proxyAccountPassword = proxyAccountPassword;
	}

	public String getOutgoingMailServer() {
		return outgoingMailServer;
	}

	public void setOutgoingMailServer(String outgoingMailServer) {
		this.outgoingMailServer = outgoingMailServer;
	}

	public String getOutgoingMailServerPort() {
		return outgoingMailServerPort;
	}

	public void setOutgoingMailServerPort(String outgoingMailServerPort) {
		this.outgoingMailServerPort = outgoingMailServerPort;
	}

	public boolean isOutgoingMailServerAuthentication() {
		return outgoingMailServerAuthentication;
	}

	public void setOutgoingMailServerAuthentication(boolean outgoingMailServerAuthentication) {
		this.outgoingMailServerAuthentication = outgoingMailServerAuthentication;
	}

	public String getOutgoingAccountName() {
		return outgoingAccountName;
	}

	public void setOutgoingAccountName(String outgoingAccountName) {
		this.outgoingAccountName = outgoingAccountName;
	}

	public String getOutgoingAccountPassword() {
		return outgoingAccountPassword;
	}

	public void setOutgoingAccountPassword(String outgoingAccountPassword) {
		this.outgoingAccountPassword = outgoingAccountPassword;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getSendAllowed() {
		return sendAllowed;
	}

	public void setSendAllowed(String sendAllowed) {
		this.sendAllowed = sendAllowed;
	}

	public String getReceiveAlloewd() {
		return receiveAlloewd;
	}

	public void setReceiveAlloewd(String receiveAlloewd) {
		this.receiveAlloewd = receiveAlloewd;
	}

	public void reset() {
		emailCode = RegularConstants.EMPTY_STRING;
		effectiveDate = RegularConstants.EMPTY_STRING;
		sendProtocol = RegularConstants.EMPTY_STRING;
		sslRequired = false;
		receiveProtocol = RegularConstants.EMPTY_STRING;
		senderName = RegularConstants.EMPTY_STRING;
		senderEmail = RegularConstants.EMPTY_STRING;
		proxyRequired = false;
		proxyServer = RegularConstants.EMPTY_STRING;
		proxyServerPort = RegularConstants.EMPTY_STRING;
		proxyServerAuthentication = false;
		proxyAccountName = RegularConstants.EMPTY_STRING;
		proxyAccountPassword = RegularConstants.EMPTY_STRING;
		outgoingMailServer = RegularConstants.EMPTY_STRING;
		outgoingMailServerPort = RegularConstants.EMPTY_STRING;
		outgoingMailServerAuthentication = false;
		outgoingAccountName = RegularConstants.EMPTY_STRING;
		outgoingAccountPassword = RegularConstants.EMPTY_STRING;
		remarks = RegularConstants.EMPTY_STRING;
		sendAllowed = RegularConstants.EMPTY_STRING;
		receiveAlloewd = RegularConstants.EMPTY_STRING;

		DBContext dbContext = new DBContext();

		ArrayList<GenericOption> sendProtocol = null;
		sendProtocol = getGenericOptions("COMMON", "SENDPROTOCOL", dbContext, "Select");
		webContext.getRequest().setAttribute("COMMON_SENDPROTOCOL", sendProtocol);

		ArrayList<GenericOption> receiveProtocol = null;
		receiveProtocol = getGenericOptions("COMMON", "REVDPROTOCOL", dbContext, "Select");
		webContext.getRequest().setAttribute("COMMON_REVDPROTOCOL", receiveProtocol);

		dbContext.close();
		setProcessBO("patterns.config.framework.bo.access.iemailconfigBO");
	}

	@Override
	public void validate() {
		if (validateEmailCode() && validateEfftDate()) {
			validateSendProtocol();
			validateReceiveProtocol();
			validateSenderName();
			validateSenderEmail();
			if (proxyRequired) {
				validateProxyServer();
				validateProxyServerPort();
			}
			if (proxyServerAuthentication) {
				validateProxyAccountName();
				validateProxyAccountPassword();
			}
			validateOutgoingMailServer();
			validateOutgoingMailServerPort();
			if (outgoingMailServerAuthentication) {
				validateOutgoingAccountName();
				validateOutgoingAccountPassword();
			}
			validateRemarks();
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.set("EMAIL_CODE", emailCode);
				formDTO.setObject("EFFT_DATE", BackOfficeFormatUtils.getDate(effectiveDate, context.getDateFormat()));
				formDTO.set("SEND_PROTO_TYPE", sendProtocol);
				formDTO.set("RECEIVE_PROTO_TYPE", receiveProtocol);
				formDTO.set("SSL_REQUIRED", decodeBooleanToString(sslRequired));
				formDTO.set("COMMUNICATION_NAME", senderName);
				formDTO.set("COMMUNICATION_EMAIL", senderEmail);
				formDTO.set("PROXY_REQUIRED", decodeBooleanToString(proxyRequired));
				if (proxyRequired) {
					formDTO.set("PROXY_SERVER", proxyServer);
					formDTO.set("PROXY_SERVER_PORT", proxyServerPort);
					if (proxyServerAuthentication) {
						formDTO.set("PROXY_USER_NAME", proxyAccountName);
						formDTO.set("PROXY_PASSWORD", proxyAccountPassword);
					} else {
						formDTO.set("PROXY_USER_NAME", RegularConstants.NULL);
						formDTO.set("PROXY_PASSWORD", RegularConstants.NULL);
					}
				} else {
					formDTO.set("PROXY_SERVER", RegularConstants.NULL);
					formDTO.set("PROXY_SERVER_PORT", RegularConstants.NULL);
					formDTO.set("PROXY_USER_NAME", RegularConstants.NULL);
					formDTO.set("PROXY_PASSWORD", RegularConstants.NULL);
				}
				formDTO.set("PROXY_AUTH_REQD", decodeBooleanToString(proxyServerAuthentication));
				if (sendAllowed.equals(RegularConstants.COLUMN_ENABLE)) {
					formDTO.set("OUTGOING_MAIL_SERVER", outgoingMailServer);
					formDTO.set("OUTGOING_MAIL_SERVER_PORT", outgoingMailServerPort);
				} else {
					formDTO.set("OUTGOING_MAIL_SERVER", RegularConstants.NULL);
					formDTO.set("OUTGOING_MAIL_SERVER_PORT", RegularConstants.NULL);
				}
				formDTO.set("OUTGOING_AUTH_REQD", decodeBooleanToString(outgoingMailServerAuthentication));
				if (outgoingMailServerAuthentication) {
					formDTO.set("OUTGOING_USER_NAME", outgoingAccountName);
					formDTO.set("OUTGOING_PASSWORD", outgoingAccountPassword);
				} else {
					formDTO.set("OUTGOING_USER_NAME", RegularConstants.NULL);
					formDTO.set("OUTGOING_PASSWORD", RegularConstants.NULL);
				}
				formDTO.set("REMARKS", remarks);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("emailCode", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		}
	}

	public boolean validateEmailCode() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("EMAIL_CODE", emailCode);
			formDTO.set("ACTION", getAction());
			formDTO = validateEmailCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("emailCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			getErrorMap().setError("emailCode", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		}
		return false;

	}

	public DTObject validateEmailCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		AccessValidator validation = new AccessValidator();
		String emailCode = inputDTO.get("EMAIL_CODE");
		try {
			if (validation.isEmpty(emailCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "EMAIL_DESCN,EMAIL_SEND_ALLOWED,EMAIL_RECV_ALLOWED");
			resultDTO = validation.validateEmailCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_CODE);
				return resultDTO;
			}
			sendAllowed = resultDTO.get("EMAIL_SEND_ALLOWED");
			receiveAlloewd = resultDTO.get("EMAIL_RECV_ALLOWED");
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateEfftDate() {
		try {
			DTObject formDTO = getFormDTO();
			formDTO.set("ENTITY_CODE", context.getEntityCode());
			formDTO.set("EMAIL_CODE", emailCode);
			formDTO.set("EFFT_DATE", effectiveDate);
			formDTO.set(ContentManager.USER_ACTION, getAction());
			formDTO = validateEffectiveDate(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("effectiveDate", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("effectiveDate", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateEffectiveDate(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, ContentManager.DATA_UNAVAILABLE);
		CommonValidator commonvalidation = new CommonValidator();
		String entityCode = inputDTO.get("ENTITY_CODE");
		String emailCode = inputDTO.get("EMAIL_CODE");
		String effectiveDate = inputDTO.get("EFFT_DATE");
		String action = inputDTO.get(ContentManager.USER_ACTION);
		try {
			if (commonvalidation.isEmpty(effectiveDate)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_FIELD_BLANK);
				return resultDTO;
			}
			TBAActionType AT;
			if (action.equals(ADD))
				AT = TBAActionType.ADD;
			else
				AT = TBAActionType.MODIFY;
			if (!commonvalidation.isEffectiveDateValid(effectiveDate, AT)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_EFFECTIVE_DATE);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "REMARKS");
			inputDTO.set(ContentManager.TABLE, "EMAILINTFCFG");
			String columns[] = new String[] { entityCode, emailCode, effectiveDate };
			inputDTO.setObject(ContentManager.PROGRAM_KEY, columns);
			resultDTO = commonvalidation.validatePK(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (action.equals(ADD)) {
				if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_RECORD_EXISTS);
				}
			} else if (action.equals(MODIFY)) {
				if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_RECORD_NOT_EXISTS);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			commonvalidation.close();
		}
		return resultDTO;
	}

	public boolean validateSendProtocol() {
		CommonValidator validation = new CommonValidator();
		try {
			if (sendAllowed.equals(RegularConstants.COLUMN_ENABLE)) {
				if (validation.isEmpty(sendProtocol)) {
					getErrorMap().setError("sendProtocol", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!validation.isCMLOVREC("COMMON", "SENDPROTOCOL", sendProtocol)) {
					getErrorMap().setError("sendProtocol", BackOfficeErrorCodes.HMS_INVALID_OPTION);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("sendProtocol", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateReceiveProtocol() {
		CommonValidator validation = new CommonValidator();
		try {
			if (receiveAlloewd.equals(RegularConstants.COLUMN_ENABLE)) {
				if (validation.isEmpty(receiveProtocol)) {
					getErrorMap().setError("receiveProtocol", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!validation.isCMLOVREC("COMMON", "REVDPROTOCOL", receiveProtocol)) {
					getErrorMap().setError("receiveProtocol", BackOfficeErrorCodes.HMS_INVALID_OPTION);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("receiveProtocol", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateSenderName() {
		CommonValidator validation = new CommonValidator();
		try {
			if (sendAllowed.equals(RegularConstants.COLUMN_ENABLE)) {
				if (validation.isEmpty(senderName)) {
					getErrorMap().setError("senderName", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!validation.isValidName(senderName)) {
					getErrorMap().setError("senderName", BackOfficeErrorCodes.INVALID_NAME);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("senderName", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateSenderEmail() {
		CommonValidator validation = new CommonValidator();
		try {
			if (sendAllowed.equals(RegularConstants.COLUMN_ENABLE)) {
				if (validation.isEmpty(senderEmail)) {
					getErrorMap().setError("senderEmail", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!validation.isValidEmail(senderEmail)) {
					getErrorMap().setError("senderEmail", BackOfficeErrorCodes.INVALID_EMAIL_ID);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("senderEmail", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateProxyServer() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(proxyServer)) {
				getErrorMap().setError("proxyServer", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidServerAddress(proxyServer)) {
				getErrorMap().setError("proxyServer", BackOfficeErrorCodes.INVALID_SERVER_ADDRESS);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("proxyServer", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateProxyServerPort() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(proxyServerPort)) {
				getErrorMap().setError("proxyServerPort", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidNumber(proxyServerPort)) {
				getErrorMap().setError("proxyServerPort", BackOfficeErrorCodes.INVALID_NUMBER);
				return false;
			}
			if (!validation.isValidPort(proxyServerPort)) {
				getErrorMap().setError("proxyServerPort", BackOfficeErrorCodes.INVALID_PORT);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("proxyServerPort", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateProxyAccountName() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(proxyAccountName)) {
				getErrorMap().setError("proxyAccountName", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidName(proxyAccountName)) {
				getErrorMap().setError("proxyAccountName", BackOfficeErrorCodes.INVALID_NAME);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("proxyAccountName", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateProxyAccountPassword() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(proxyAccountPassword)) {
				getErrorMap().setError("proxyAccountPassword", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidCredential(proxyAccountPassword)) {
				getErrorMap().setError("proxyAccountPassword", BackOfficeErrorCodes.INVALID_CREDENTIALS);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("proxyAccountPassword", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateOutgoingMailServer() {
		CommonValidator validation = new CommonValidator();
		try {
			if (sendAllowed.equals(RegularConstants.COLUMN_ENABLE)) {
				if (validation.isEmpty(outgoingMailServer)) {
					getErrorMap().setError("outgoingMailServer", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!validation.isValidServerAddress(outgoingMailServer)) {
					getErrorMap().setError("outgoingMailServer", BackOfficeErrorCodes.INVALID_SERVER_ADDRESS);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("outgoingMailServer", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateOutgoingMailServerPort() {
		CommonValidator validation = new CommonValidator();
		try {
			if (sendAllowed.equals(RegularConstants.COLUMN_ENABLE)) {
				if (validation.isEmpty(outgoingMailServerPort)) {
					getErrorMap().setError("outgoingMailServerPort", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!validation.isValidNumber(outgoingMailServerPort)) {
					getErrorMap().setError("outgoingMailServerPort", BackOfficeErrorCodes.INVALID_NUMBER);
					return false;
				}
				if (!validation.isValidPort(outgoingMailServerPort)) {
					getErrorMap().setError("outgoingMailServerPort", BackOfficeErrorCodes.INVALID_PORT);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("outgoingMailServerPort", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateOutgoingAccountName() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(outgoingAccountName)) {
				getErrorMap().setError("outgoingAccountName", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidName(outgoingAccountName)) {
				getErrorMap().setError("outgoingAccountName", BackOfficeErrorCodes.INVALID_NAME);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("outgoingAccountName", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateOutgoingAccountPassword() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(outgoingAccountPassword)) {
				getErrorMap().setError("outgoingAccountPassword", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidCredential(outgoingAccountPassword)) {
				getErrorMap().setError("outgoingAccountPassword", BackOfficeErrorCodes.INVALID_CREDENTIALS);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("outgoingAccountPassword", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateRemarks() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (remarks != null && remarks.length() > 0) {
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.HMS_INVALID_REMARKS);
					return false;
				}
			}
			if (getAction().equals(MODIFY)) {
				if (commonValidator.isEmpty(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.HMS_FIELD_BLANK);
					return false;
				}
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.HMS_INVALID_REMARKS);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}
}