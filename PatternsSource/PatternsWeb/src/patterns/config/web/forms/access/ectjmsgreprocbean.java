package patterns.config.web.forms.access;

import java.sql.ResultSet;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.DHTMLXGridUtility;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.web.forms.GenericFormBean;

public class ectjmsgreprocbean extends GenericFormBean {

	private String programIdentifier;
	private static final long serialVersionUID = 1L;

	public String getProgramIdentifier() {
		return programIdentifier;
	}

	public void setProgramIdentifier(String programIdentifier) {
		this.programIdentifier = programIdentifier;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public void reset() {
		programIdentifier = RegularConstants.EMPTY_STRING;

	}

	@Override
	public void validate() {
		// TODO Auto-generated method stub
		;
	}

	public DTObject programIdentifierValidate(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		/*MSPR BEGIN 17072015*/
		//CommonValidator validation = new CommonValidator();
		//DBUtil util = validation.getDbContext().createUtilInstance();
		DBContext dbContext = new DBContext();
		/*MSPR END 17072015*/
		String programIdentifier = inputDTO.get("PROGRAM_IDENTIFIER");
		try {
			/*MSPR BEGIN 17072015*/
			DBUtil util = dbContext.createUtilInstance();
			/*MSPR END 17072015*/
			StringBuffer sqlQuery = new StringBuffer("SELECT MPGM_ID FROM CTJMSGCONFIG WHERE ENTITY_CODE=? AND PGM_IDENTFR=?");
			util.reset();
			util.setSql(sqlQuery.toString());
			util.setString(1, context.getEntityCode());
			util.setString(2, programIdentifier);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				resultDTO.set("MPGM_ID", rset.getString(1));
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
			}
			/*MSPR BEGIN 17072015*/
			util.reset();
			/*MSPR END 17072015*/
			String a = resultDTO.get("MPGM_ID");
			if (a == null) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_RECORD_NOT_EXISTS);

			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		}
		/*MSPR BEGIN 17072015*/
		finally{
			dbContext.close();
		}
		/*MSPR END 17072015*/
		return resultDTO;

	}

	public DTObject loadGrid(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBContext dbContext = new DBContext();
		/*MSPR BEGIN 17072015*/
		//DBUtil util = dbContext.createUtilInstance();
		String programIdentifier = inputDTO.get("PROGRAM_IDENTIFIER");
		//String sqlQuery = null;
		/*MSPR END 17072015*/
		try {
			/*MSPR BEGIN 17072015*/
			DBUtil util = dbContext.createUtilInstance();
			/*MSPR BEGIN 17072015*/
			//sqlQuery = "SELECT A.MSG_REF_ID,A.MSG_PGM_IDENTFR,A.MSG_STATUS,A.MSG_ERR_CODE,A.MSG_ERR_DESCN FROM CTJINQREF A WHERE (A.MSG_REF_ID) NOT IN (SELECT B.MSG_REF_ID FROM CTJPROCINQPE B) AND A.ENTITY_CODE=? AND A.MSG_PGM_IDENTFR=? AND A.MSG_STATUS=?";
			String sqlQuery = "SELECT A.MSG_REF_ID,A.MSG_PGM_IDENTFR,A.MSG_STATUS,A.MSG_ERR_CODE,A.MSG_ERR_DESCN FROM CTJINQREF A WHERE (A.MSG_REF_ID) NOT IN (SELECT B.MSG_REF_ID FROM CTJPROCINQPE B) AND A.ENTITY_CODE=? AND A.MSG_PGM_IDENTFR=? AND A.MSG_STATUS=?";
			/*MSPR END 17072015*/
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, context.getEntityCode());
			util.setString(2, programIdentifier);
			util.setString(3, "F");
			ResultSet rset = util.executeQuery();
			DHTMLXGridUtility gridUtility = new DHTMLXGridUtility();
			gridUtility.init();
			while (rset.next()) {
				gridUtility.startRow();
				gridUtility.setCell(rset.getString(1));
				gridUtility.setCell(rset.getString(2));
				gridUtility.setCell(rset.getString(3));
				gridUtility.setCell(rset.getString(4));
				gridUtility.setCell(rset.getString(5));
				gridUtility.endRow();
			}
			gridUtility.finish();
			/*MSPR BEGIN 17072015*/
			util.reset();
			/*MSPR END 17072015*/
			String resultXML = gridUtility.getXML();
			resultDTO.set(ContentManager.RESULT_XML, resultXML);
			resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			dbContext.close();
		}
		return resultDTO;
	}

	public DTObject updateCtjProcInqPe(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		
		/*MSPR BEGIN 17072015*/
		//CommonValidator validation = new CommonValidator();
		//DBUtil util = validation.getDbContext().createUtilInstance();
		DBContext dbContext = new DBContext();
		/*MSPR END 17072015*/
		String messageRefernceId = inputDTO.get("MESSAGE_REF_ID");
		int count = 0;
		try {
			/*MSPR BEGIN 17072015*/
			DBUtil util = dbContext.createUtilInstance();
			/*MSPR BEGIN 17072015*/
			StringBuffer sqlQuery = new StringBuffer("INSERT INTO CTJPROCINQPE(ENTITY_CODE,MSG_REF_ID) VALUES (?, ?)");
			util.reset();
			util.setSql(sqlQuery.toString());
			util.setString(1, context.getEntityCode());
			util.setString(2, messageRefernceId);
			count = util.executeUpdate();
			if (count > 0) {
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
			}
			/*MSPR BEGIN 17072015*/
			util.reset();
			/*MSPR END 17072015*/
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		}/*MSPR BEGIN 17072015*/
		finally{
			dbContext.close();
		}
		/*MSPR END 17072015*/
		return resultDTO;

	}

}
