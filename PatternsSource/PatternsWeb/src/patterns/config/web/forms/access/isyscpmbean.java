package patterns.config.web.forms.access;

import java.util.ArrayList;
import java.util.HashMap;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.BindParameterType;
import patterns.config.framework.database.DBEntry;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.process.TBAActionType;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.framework.web.GenericOption;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.AccessValidator;
import patterns.config.web.forms.GenericFormBean;

public class isyscpmbean extends GenericFormBean {

	private static final long serialVersionUID = -6219657363150466160L;
	private int MAXVAL_50 = 50;
	private int MAXVAL_99 = 99;
	private int MAXVAL_9999 = 9999;
	private int VAL_ZERO = 0;

	private String effectiveDate;
	private String minLengthUID;
	private String minLengthPwd;
	private String minNumPwd;
	private String minAlphaPwd;
	private String minSCPwd;
	private String forcePwdChgn;

	private String prevPwdCnt;
	private String uwCnt;
	private String sawCnt;
	private String lockUIDUACnt;
	private String lockUIDNUCnt;
	private String lockUIDUOCnt;
	// private String UIDExpPeriod;
	private String pwdExpPeriod;

	private String totalLeasePeriod;
	private String autoLogoutPeriod;
	private boolean multipleSessionAllowed;
	private boolean ipRestrictUsers;
	private boolean actRoleAllowed;
	private boolean adminActivation;
	private String pwdDelMechanism;
	private String pwdManualType;

	public String getPwdDelMechanism() {
		return pwdDelMechanism;
	}

	public void setPwdDelMechanism(String pwdDelMechanism) {
		this.pwdDelMechanism = pwdDelMechanism;
	}

	public String getPwdManualType() {
		return pwdManualType;
	}

	public void setPwdManualType(String pwdManualType) {
		this.pwdManualType = pwdManualType;
	}

	private String remarks;

	public boolean isAdminActivation() {
		return adminActivation;
	}

	public void setAdminActivation(boolean adminActivation) {
		this.adminActivation = adminActivation;
	}

	public void reset() {
		DBContext dbContext = new DBContext();
		ArrayList<GenericOption> pwdDelMechanismList = null;
		pwdDelMechanismList = getGenericOptions("COMMON", "PWDDELIVERY", dbContext, "--");
		webContext.getRequest().setAttribute("COMMON_PWDDELIVERY", pwdDelMechanismList);
		ArrayList<GenericOption> pwdManualTypeList = null;
		pwdManualTypeList = getGenericOptions("COMMON", "PWDMANUALTYPE", dbContext, "--");
		webContext.getRequest().setAttribute("COMMON_PWDMANUALTYPE", pwdManualTypeList);
		dbContext.close();
		remarks = RegularConstants.EMPTY_STRING;
		setProcessBO("patterns.config.framework.bo.access.isyscpmBO");
	}

	@Override
	public void validate() {
		if (validateEffectiveDate()) {
			isNumericRange("minLengthUID", minLengthUID, MAXVAL_50, true);
			isNumericRange("minLengthPwd", minLengthPwd, MAXVAL_99, true);
			isNumericRange("minNumPwd", minNumPwd, MAXVAL_99, false);
			isNumericRange("minAlphaPwd", minAlphaPwd, MAXVAL_99, false);
			isNumericRange("minSCPwd", minSCPwd, MAXVAL_99, false);
			isNumericRange("forcePwdChgn", forcePwdChgn, MAXVAL_9999, false);
			isNumericRange("prevPwdCnt", prevPwdCnt, MAXVAL_99, false);
			isNumericRange("uwCnt", uwCnt, MAXVAL_99, false);
			isNumericRange("sawCnt", sawCnt, MAXVAL_99, false);
			isNumericRange("lockUIDUACnt", lockUIDUACnt, MAXVAL_99, false);
			isNumericRange("lockUIDNUCnt", lockUIDNUCnt, MAXVAL_9999, false);
			isNumericRange("lockUIDUOCnt", lockUIDUOCnt, MAXVAL_9999, false);
			// isNumericRange("UIDExpPeriod", UIDExpPeriod, MAXVAL_9999, false);
			isNumericRange("pwdExpPeriod", pwdExpPeriod, MAXVAL_9999, false);
			isNumericRange("autoLogoutPeriod", autoLogoutPeriod, MAXVAL_9999, true);

			checkPasswordLength();
			validatePwdDeliveryMechanism();
			if (pwdDelMechanism.equals("4")) {
				validatePwdManualType();
			}
			validateRemarks();
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.setObject("EFFT_DATE", BackOfficeFormatUtils.getDate(effectiveDate, context.getDateFormat()));
				formDTO.set("MIN_UID_LEN", minLengthUID);
				formDTO.set("MIN_PWD_LEN", minLengthPwd);
				formDTO.set("MIN_PWD_NUM", minNumPwd);
				formDTO.set("MIN_PWD_ALPHA", minAlphaPwd);
				formDTO.set("MIN_PWD_SPECIAL", minSCPwd);
				formDTO.set("FORCE_PWD_CHGN", forcePwdChgn);
				formDTO.set("PREVENT_PREV_PWD", prevPwdCnt);
				formDTO.set("UNSUCC_ATTMPT_UW", uwCnt);
				formDTO.set("UNSUCC_ATTMPT_SW", sawCnt);
				formDTO.set("LOCK_UID", lockUIDUACnt);
				formDTO.set("INACT_NOT_USED", lockUIDNUCnt);
				formDTO.set("INACT_ONCE_USED", lockUIDUOCnt);
				formDTO.set("INACT_UID_EXPIRY", RegularConstants.COLUMN_DISABLE);
				formDTO.set("INACT_PWD_EXPIRY", pwdExpPeriod);
				formDTO.set("TOTAL_LEASE_PERIOD", totalLeasePeriod);
				formDTO.set("AUTO_LOGOUT_SECS", autoLogoutPeriod);
				formDTO.set("MULTIPLE_SESSION_ALLOWED", decodeBooleanToString(multipleSessionAllowed));
				formDTO.set("ADMIN_USER_IP_RESTRICT", decodeBooleanToString(ipRestrictUsers));
				formDTO.set("MAX_ACTING_ROLE", decodeBooleanToString(actRoleAllowed));
				formDTO.set("ADMIN_ACTIVATION_REQD", decodeBooleanToString(adminActivation));
				formDTO.set("PWD_DELIVERY_MECHANISM", pwdDelMechanism);
				formDTO.set("PWD_MANUAL_TYPE", pwdManualType);
				formDTO.set("REMARKS", remarks);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("effectiveDate", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
	}

	private boolean validatePwdDeliveryMechanism() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(pwdDelMechanism)) {
				getErrorMap().setError("pwdDelMechanism", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}

			if (!validation.isCMLOVREC("COMMON", "PWDDELIVERY", pwdDelMechanism)) {
				getErrorMap().setError("pwdDelMechanism", BackOfficeErrorCodes.HMS_RECORD_NOT_EXISTS);
				return false;
			}

		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("pwdDelMechanism", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return true;
	}

	private boolean validatePwdManualType() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(pwdManualType)) {
				getErrorMap().setError("pwdManualType", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}

			if (!validation.isCMLOVREC("COMMON", "PWDMANUALTYPE", pwdManualType)) {
				getErrorMap().setError("pwdManualType", BackOfficeErrorCodes.HMS_RECORD_NOT_EXISTS);
				return false;
			}

		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("pwdManualType", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return true;
	}

	public String getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public String getMinLengthUID() {
		return minLengthUID;
	}

	public void setMinLengthUID(String minLengthUID) {
		this.minLengthUID = minLengthUID;
	}

	public String getMinLengthPwd() {
		return minLengthPwd;
	}

	public void setMinLengthPwd(String minLengthPwd) {
		this.minLengthPwd = minLengthPwd;
	}

	public String getMinNumPwd() {
		return minNumPwd;
	}

	public void setMinNumPwd(String minNumPwd) {
		this.minNumPwd = minNumPwd;
	}

	public String getMinAlphaPwd() {
		return minAlphaPwd;
	}

	public void setMinAlphaPwd(String minAlphaPwd) {
		this.minAlphaPwd = minAlphaPwd;
	}

	public String getMinSCPwd() {
		return minSCPwd;
	}

	public void setMinSCPwd(String minSCPwd) {
		this.minSCPwd = minSCPwd;
	}

	public String getForcePwdChgn() {
		return forcePwdChgn;
	}

	public void setForcePwdChgn(String forcePwdChgn) {
		this.forcePwdChgn = forcePwdChgn;
	}

	public String getPrevPwdCnt() {
		return prevPwdCnt;
	}

	public void setPrevPwdCnt(String prevPwdCnt) {
		this.prevPwdCnt = prevPwdCnt;
	}

	public String getUwCnt() {
		return uwCnt;
	}

	public void setUwCnt(String uwCnt) {
		this.uwCnt = uwCnt;
	}

	public String getSawCnt() {
		return sawCnt;
	}

	public void setSawCnt(String sawCnt) {
		this.sawCnt = sawCnt;
	}

	public String getLockUIDUACnt() {
		return lockUIDUACnt;
	}

	public void setLockUIDUACnt(String lockUIDUACnt) {
		this.lockUIDUACnt = lockUIDUACnt;
	}

	public String getLockUIDNUCnt() {
		return lockUIDNUCnt;
	}

	public void setLockUIDNUCnt(String lockUIDNUCnt) {
		this.lockUIDNUCnt = lockUIDNUCnt;
	}

	public String getLockUIDUOCnt() {
		return lockUIDUOCnt;
	}

	public void setLockUIDUOCnt(String lockUIDUOCnt) {
		this.lockUIDUOCnt = lockUIDUOCnt;
	}

	/*
	 * public String getUIDExpPeriod() { return UIDExpPeriod; }
	 * 
	 * public void setUIDExpPeriod(String uIDExpPeriod) { UIDExpPeriod =
	 * uIDExpPeriod; }
	 */
	public String getTotalLeasePeriod() {
		return totalLeasePeriod;
	}

	public void setTotalLeasePeriod(String totalLeasePeriod) {
		this.totalLeasePeriod = totalLeasePeriod;
	}

	public String getAutoLogoutPeriod() {
		return autoLogoutPeriod;
	}

	public void setAutoLogoutPeriod(String autoLogoutPeriod) {
		this.autoLogoutPeriod = autoLogoutPeriod;
	}

	public boolean isMultipleSessionAllowed() {
		return multipleSessionAllowed;
	}

	public void setMultipleSessionAllowed(boolean multipleSessionAllowed) {
		this.multipleSessionAllowed = multipleSessionAllowed;
	}

	public boolean isIpRestrictUsers() {
		return ipRestrictUsers;
	}

	public void setIpRestrictUsers(boolean ipRestrictUsers) {
		this.ipRestrictUsers = ipRestrictUsers;
	}

	public boolean isActRoleAllowed() {
		return actRoleAllowed;
	}

	public void setActRoleAllowed(boolean actRoleAllowed) {
		this.actRoleAllowed = actRoleAllowed;
	}

	public boolean validateEffectiveDate() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(effectiveDate)) {
				getErrorMap().setError("effectiveDate", BackOfficeErrorCodes.HMS_FIELD_BLANK);
				return false;
			}
			TBAActionType AT;
			if (getAction().equals(ADD))
				AT = TBAActionType.ADD;
			else
				AT = TBAActionType.MODIFY;
			if (validation.isEffectiveDateValid(effectiveDate, AT)) {
				HashMap<Integer, DBEntry> whereClauseFieldsMap = new HashMap<Integer, DBEntry>();
				whereClauseFieldsMap.put(1, new DBEntry("ENTITY_CODE", context.getEntityCode(), BindParameterType.VARCHAR));
				whereClauseFieldsMap.put(2, new DBEntry("EFFT_DATE", BackOfficeFormatUtils.getDate(effectiveDate, context.getDateFormat()), BindParameterType.DATE));
				boolean isRecordAvailable = validation.isRecordAvailable("SYSCPMHIST", whereClauseFieldsMap);
				if (isRecordAvailable) {
					if (getAction().equals(ADD)) {
						getErrorMap().setError("effectiveDate", BackOfficeErrorCodes.RECORD_EXISTS);
						return false;
					}
					return true;
				} else {
					if (getAction().equals(MODIFY)) {
						getErrorMap().setError("effectiveDate", BackOfficeErrorCodes.RECORD_NOT_EXISTS);
						return false;
					}
					return true;
				}
			} else {
				getErrorMap().setError("effectiveDate", BackOfficeErrorCodes.INVALID_EFFECTIVE_DATE);
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("effectiveDate", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public DTObject validateEffectiveDate(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		AccessValidator validation = new AccessValidator();
		String efftDate = inputDTO.get("EFFT_DATE");
		String action = inputDTO.get(ContentManager.USER_ACTION);
		try {

			if (validation.isEmpty(efftDate)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_FIELD_BLANK);
				return resultDTO;
			}
			HashMap<Integer, DBEntry> whereClauseFieldsMap = new HashMap<Integer, DBEntry>();
			whereClauseFieldsMap.put(1, new DBEntry("ENTITY_CODE", context.getEntityCode(), BindParameterType.VARCHAR));
			whereClauseFieldsMap.put(2, new DBEntry("EFFT_DATE", BackOfficeFormatUtils.getDate(inputDTO.get("EFFT_DATE"), context.getDateFormat()), BindParameterType.DATE));
			boolean isRecordAvailable = validation.isRecordAvailable("SYSCPMHIST", whereClauseFieldsMap);
			if (isRecordAvailable) {
				if (action.equals(ADD)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_RECORD_EXISTS);
				}
			} else {
				if (action.equals(MODIFY)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_RECORD_NOT_EXISTS);
				}

			}

		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateRemarks() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (remarks != null && remarks.length() > 0) {
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.HMS_INVALID_REMARKS);
					return false;
				}
			}
			if (getAction().equals(MODIFY)) {
				if (commonValidator.isEmpty(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.HMS_FIELD_BLANK);
					return false;
				}
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.HMS_INVALID_REMARKS);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}

	public boolean isNumericRange(String field, String input, int range, boolean zeroCheck) {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(input)) {
				getErrorMap().setError(field, BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidNumber(input)) {
				getErrorMap().setError(field, BackOfficeErrorCodes.INVALID_NUMBER);
				return false;
			}
			int value = Integer.parseInt(input);
			if (value > range) {
				getErrorMap().setError(field, BackOfficeErrorCodes.MAX_LENGTH);
				return false;
			}
			if (zeroCheck) {
				if (validation.isZero(input)) {
					getErrorMap().setError(field, BackOfficeErrorCodes.ZERO_CHECK);
					return false;
				}
			}
			if (value < VAL_ZERO) {
				getErrorMap().setError(field, BackOfficeErrorCodes.POSITIVE_CHECK);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError(field, BackOfficeErrorCodes.INVALID_NUMBER);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean checkPasswordLength() {
		int passLength = Integer.parseInt(minLengthPwd);
		int totalLength = Integer.parseInt(minNumPwd) + Integer.parseInt(minAlphaPwd) + Integer.parseInt(minSCPwd);
		if (totalLength != passLength) {
			getErrorMap().setError("minSCPwd", BackOfficeErrorCodes.PASSWORD_LEN_CHECK);
			return false;
		}
		return true;
	}

	public void setPwdExpPeriod(String pwdExpPeriod) {
		this.pwdExpPeriod = pwdExpPeriod;
	}

	public String getPwdExpPeriod() {
		return pwdExpPeriod;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
}
