package patterns.config.web.forms.access;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.AccessValidator;
import patterns.config.web.forms.GenericFormBean;

public class qconsbroleallocbean extends GenericFormBean {

	private static final long serialVersionUID = -6219657363150466160L;

	private String userID;
	private String primaryRoleCode;

	public String getPrimaryRoleCode() {
		return primaryRoleCode;
	}

	public void setPrimaryRoleCode(String primaryRoleCode) {
		this.primaryRoleCode = primaryRoleCode;
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	@Override
	public void reset() {
		userID = RegularConstants.EMPTY_STRING;
		primaryRoleCode = RegularConstants.EMPTY_STRING;

	}

	@Override
	public void validate() {

	}

	public DTObject validateUserID(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		DTObject formDTO = getFormDTO();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		AccessValidator validation = new AccessValidator();
		try {
			String userID = inputDTO.get("USER_ID");
			if (validation.isEmpty(userID)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			formDTO.set(ContentManager.USER_ID, userID);
			formDTO.set(ContentManager.FETCH, ContentManager.FETCH_ALL);
			formDTO = validation.validateInternalUser(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, formDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			String roleCode = ContentManager.EMPTY_STRING;
			if (!formDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_USERID);
				return resultDTO;
			}
			roleCode = formDTO.get("ROLE_CODE");
			resultDTO.set("ROLE_CODE", roleCode);
			if (context.isInternalAdministrator()) {
				String _branchCode = formDTO.get("BRANCH_CODE");
				if (_branchCode.equals(RegularConstants.COLUMN_DISABLE)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INSUFFICIENT_ROLE_RIGHTS);
					return resultDTO;
				}
			}
			if (context.isInternalOperations()) {
				String _branchCode = formDTO.get("BRANCH_CODE");
				if (!_branchCode.equals(RegularConstants.COLUMN_DISABLE)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INSUFFICIENT_ROLE_RIGHTS);
					return resultDTO;
				}
			}
			resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			validation.close();
		}
		return resultDTO;
	}

}
