package patterns.config.web.forms.access;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.BindParameterType;
import patterns.config.framework.database.DBEntry;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.process.TBAActionType;
import patterns.config.framework.service.DTDObject;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.framework.web.DHTMLXGridUtility;
import patterns.config.framework.web.GenericOption;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.AccessValidator;
import patterns.config.validations.CommonValidator;
import patterns.config.web.forms.GenericFormBean;

public class eroleconsoleallocbean extends GenericFormBean {

	private static final long serialVersionUID = -6219657363150466160L;

	private String roleType;
	private String effectiveDate;
	private String remarks;
	private String xmlConsoleAllocGrid;
	private String gridbox_rowid;

	public String getRoleType() {
		return roleType;
	}

	public void setRoleType(String roleType) {
		this.roleType = roleType;
	}

	public String getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public String getXmlConsoleAllocGrid() {
		return xmlConsoleAllocGrid;
	}

	public void setXmlConsoleAllocGrid(String xmlConsoleAllocGrid) {
		this.xmlConsoleAllocGrid = xmlConsoleAllocGrid;
	}

	public String getGridbox_rowid() {
		return gridbox_rowid;
	}

	public void setGridbox_rowid(String gridbox_rowid) {
		this.gridbox_rowid = gridbox_rowid;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public void reset() {
		setRoleType(RegularConstants.EMPTY_STRING);
		setEffectiveDate(RegularConstants.EMPTY_STRING);
		setXmlConsoleAllocGrid(RegularConstants.EMPTY_STRING);
		remarks = RegularConstants.EMPTY_STRING;
		DBContext dbContext = new DBContext();
		ArrayList<GenericOption> roleTypeList = null;
		roleTypeList = getGenericOptions("COMMON", "ROLES", dbContext, "--");
		webContext.getRequest().setAttribute("COMMON_ROLES", roleTypeList);
		dbContext.close();
		setProcessBO("patterns.config.framework.bo.access.eroleconsoleallocBO");
	}

	public void validate() {
		if (validateRoleType()) {
			if (validateEffectiveDate()) {
				if (validatePK()) {
					validateRemarks();
					validateGrid();
				}
			}
		}
		try {
			if (getErrorMap().length() == 0) {

				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.set("ROLE_TYPE", roleType);
				formDTO.setObject("EFFT_DATE", BackOfficeFormatUtils.getDate(effectiveDate, context.getDateFormat()));
				formDTO.set("REMARKS", remarks);
				DTDObject dtdObject = new DTDObject();
				dtdObject.addColumn(0, "SL");
				dtdObject.addColumn(1, "CODE");
				dtdObject.addColumn(2, "DESCRIPTION");
				dtdObject.addColumn(3, "ENABLED");
				formDTO.setDTDObject("ROLECONSOLEALLOCHISTDTL", dtdObject);
				formDTO.setXMLDTDObject("ROLECONSOLEALLOCHISTDTL", xmlConsoleAllocGrid);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("roleType", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
	}

	public boolean validateRoleType() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(roleType)) {
				getErrorMap().setError("roleType", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isCMLOVREC("COMMON", "ROLES", roleType)) {
				getErrorMap().setError("roleType", BackOfficeErrorCodes.INVALID_CODE);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("roleType", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateEffectiveDate() {
		CommonValidator validation = new CommonValidator();
		try {
			TBAActionType AT;
			if (getAction().equals(ADD))
				AT = TBAActionType.ADD;
			else
				AT = TBAActionType.MODIFY;

			if (validation.isEffectiveDateValid(effectiveDate, AT)) {
				return true;
			} else {
				getErrorMap().setError("effectiveDate", BackOfficeErrorCodes.INVALID_EFFECTIVE_DATE);
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("effectiveDate", BackOfficeErrorCodes.UNSPECIFIED_ERROR);

		} finally {
			validation.close();
		}
		return false;
	}

	private boolean validateGrid() {
		AccessValidator validation = new AccessValidator();
		DTDObject dtdObject = new DTDObject();
		try {
			dtdObject.addColumn(0, "SL");
			dtdObject.addColumn(1, "CODE");
			dtdObject.addColumn(2, "DESCRIPTION");
			dtdObject.addColumn(3, "ENABLED");
			dtdObject.setXML(xmlConsoleAllocGrid);
			if (dtdObject.getRowCount() == 0) {
				getErrorMap().setError("roleType", BackOfficeErrorCodes.ADD_ATLEAST_ONE_ROW);
				return false;
			}
			if (!validation.checkDuplicateRows(dtdObject, 1)) {
				getErrorMap().setError("roleType", BackOfficeErrorCodes.DUPLICATE_DATA);
				return false;
			}
			int flag = 0;
			for (int i = 0; i < dtdObject.getRowCount(); i++) {
				boolean consoleEnabled = decodeStringToBoolean(dtdObject.getValue(i, 3));
				if (consoleEnabled) {
					String consoleCode = dtdObject.getValue(i, 1);
					DTObject formDTO = getFormDTO();
					formDTO.set(ContentManager.CODE, consoleCode);
					formDTO.set(ContentManager.ACTION, "U");
					formDTO.set(ContentManager.FETCH, ContentManager.FETCH_ALL);
					formDTO = validation.validateConsoleCode(formDTO);
					if (formDTO.get(ContentManager.ERROR) != null) {
						getErrorMap().setError("roleType", formDTO.get(ContentManager.ERROR));
						return false;
					}
					if (formDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
						getErrorMap().setError("roleType", BackOfficeErrorCodes.INVALID_CODE);
						return false;
					}
					flag++;
				}
			}
			if (flag == 0) {
				getErrorMap().setError("roleType", BackOfficeErrorCodes.CHECK_ATLEAST_ONE);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("roleType", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validatePK() {
		CommonValidator validation = new CommonValidator();
		try {
			HashMap<Integer, DBEntry> whereClauseFieldsMap = new HashMap<Integer, DBEntry>();
			whereClauseFieldsMap.put(1, new DBEntry("ENTITY_CODE", context.getEntityCode(), BindParameterType.VARCHAR));
			whereClauseFieldsMap.put(2, new DBEntry("ROLE_TYPE", roleType, BindParameterType.VARCHAR));
			whereClauseFieldsMap.put(3, new DBEntry("EFFT_DATE", BackOfficeFormatUtils.getDate(effectiveDate, context.getDateFormat()), BindParameterType.DATE));
			boolean isRecordAvailable = validation.isRecordAvailable("ROLECONSOLEALLOCHIST", whereClauseFieldsMap);
			if (isRecordAvailable) {
				if (getAction().equals(ADD)) {
					getErrorMap().setError("roleType", BackOfficeErrorCodes.RECORD_EXISTS);
					return false;
				}
				return true;
			} else {
				if (getAction().equals(MODIFY)) {
					getErrorMap().setError("roleType", BackOfficeErrorCodes.RECORD_NOT_EXISTS);
					return false;
				}
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("roleType", BackOfficeErrorCodes.UNSPECIFIED_ERROR);

		} finally {
			validation.close();
		}
		return false;
	}

	public DTObject validateEffectiveDate(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		AccessValidator validation = new AccessValidator();
		String efftDate = inputDTO.get("EFFT_DATE");
		String action = inputDTO.get(ContentManager.USER_ACTION);
		try {
			if (validation.isEmpty(efftDate)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_FIELD_BLANK);
				return resultDTO;
			}
			HashMap<Integer, DBEntry> whereClauseFieldsMap = new HashMap<Integer, DBEntry>();
			whereClauseFieldsMap.put(1, new DBEntry("ENTITY_CODE", context.getEntityCode(), BindParameterType.VARCHAR));
			whereClauseFieldsMap.put(2, new DBEntry("ROLE_TYPE", inputDTO.get("ROLE_TYPE"), BindParameterType.VARCHAR));
			whereClauseFieldsMap.put(3, new DBEntry("EFFT_DATE", BackOfficeFormatUtils.getDate(inputDTO.get("EFFT_DATE"), context.getDateFormat()), BindParameterType.DATE));
			boolean isRecordAvailable = validation.isRecordAvailable("ROLECONSOLEALLOCHIST", whereClauseFieldsMap);
			if (isRecordAvailable) {
				if (action.equals(ADD)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_RECORD_EXISTS);
				}
			} else {
				if (action.equals(MODIFY)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_RECORD_NOT_EXISTS);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public DTObject loadConsoleGrid(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		try {
			DBContext dbContext = new DBContext();
			try {
				DBUtil util = dbContext.createUtilInstance();
				String sql = "SELECT @ROWNUM:=@ROWNUM+1,CODE,DESCRIPTION,'0' FROM CONSOLE,(SELECT @ROWNUM:=0) R WHERE ENTITY_CODE = ? AND ENABLED='1'";
				util.reset();
				util.setSql(sql);
				util.setString(1, context.getEntityCode());
				ResultSet rset = util.executeQuery();
				DHTMLXGridUtility gridUtility = new DHTMLXGridUtility();
				gridUtility.init();
				while (rset.next()) {
					gridUtility.startRow(rset.getString(2));
					gridUtility.setCell(rset.getString(1));
					gridUtility.setCell(rset.getString(2));
					gridUtility.setCell(rset.getString(3));
					gridUtility.setCell(rset.getString(4));
					gridUtility.endRow();
				}
				gridUtility.finish();
				util.reset();
				String resultXML = gridUtility.getXML();
				resultDTO.set(ContentManager.RESULT_XML, resultXML);
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
			} catch (Exception e) {
				e.printStackTrace();
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			} finally {
				dbContext.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
		}
		return resultDTO;
	}

	public boolean validateRemarks() {
		CommonValidator commonValidator = new CommonValidator();
		try {
				if (remarks!=null && remarks.length()>0) {
					if(!commonValidator.isValidRemarks(remarks)){
						getErrorMap().setError("remarks", BackOfficeErrorCodes.HMS_INVALID_REMARKS);
						return false;
					}
				}
				if (getAction().equals(MODIFY)) {
					if (commonValidator.isEmpty(remarks)) {
						getErrorMap().setError("remarks", BackOfficeErrorCodes.HMS_FIELD_BLANK);
						return false;
					}
					if(!commonValidator.isValidRemarks(remarks)){
						getErrorMap().setError("remarks", BackOfficeErrorCodes.HMS_INVALID_REMARKS);
						return false;
					}
				}
				return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}
}