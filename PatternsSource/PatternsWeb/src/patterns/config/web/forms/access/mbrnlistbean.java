package patterns.config.web.forms.access;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.service.DTDObject;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.AccessValidator;
import patterns.config.validations.CommonValidator;
import patterns.config.web.forms.GenericFormBean;

public class mbrnlistbean extends GenericFormBean {

	private static final long serialVersionUID = -6219657363150466160L;

	private String brnListCode;
	private String brnListDescription;
	private String brnCode;
	private String xmlBrnList;
	private String gridbox_rowid;
	private boolean enabled;
	private String remarks;

	public String getBrnCode() {
		return brnCode;
	}

	public String getBrnListCode() {
		return brnListCode;
	}

	public String getBrnListDescription() {
		return brnListDescription;
	}

	public String getGridbox_rowid() {
		return gridbox_rowid;
	}

	public String getRemarks() {
		return remarks;
	}

	public String getXmlBrnList() {
		return xmlBrnList;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void reset() {
		brnListCode = RegularConstants.EMPTY_STRING;
		brnCode = RegularConstants.EMPTY_STRING;
		brnListDescription = RegularConstants.EMPTY_STRING;
		enabled = false;
		remarks = RegularConstants.EMPTY_STRING;
		setProcessBO("patterns.config.framework.bo.access.mbrnlistBO");
	}

	public void setBrnCode(String brnCode) {
		this.brnCode = brnCode;
	}

	public void setBrnListCode(String brnListCode) {
		this.brnListCode = brnListCode;
	}

	public void setBrnListDescription(String brnListDescription) {
		this.brnListDescription = brnListDescription;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public void setGridbox_rowid(String gridbox_rowid) {
		this.gridbox_rowid = gridbox_rowid;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public void setXmlBrnList(String xmlBrnList) {
		this.xmlBrnList = xmlBrnList;
	}

	public void validate() {
		if (validateBrnListCode()) {
			validateDescription();
			validateRemarks();
			validateGrid();
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.set("CODE", brnListCode);
				formDTO.set("DESCRIPTION", brnListDescription);
				if (getAction().equals(ADD)) {
					formDTO.set("ENABLED", decodeBooleanToString(true));
				} else {
					formDTO.set("ENABLED", decodeBooleanToString(enabled));
				}
				formDTO.set("REMARKS", remarks);
				DTDObject dtdObject = new DTDObject();
				dtdObject.addColumn(0, "SL");
				dtdObject.addColumn(1, "BRANCH_CODE");
				dtdObject.addColumn(2, "DESCN");
				dtdObject.setXML(xmlBrnList);
				formDTO.setDTDObject("BRANCHLISTDTL", dtdObject);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("brnListCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
	}

	private boolean validateBrnCode(String value) {
		DTObject inputDTO = new DTObject();
		inputDTO.set("BRANCH_CODE", value);
		inputDTO = validateBranchCode(inputDTO);
		if (inputDTO.get(ContentManager.ERROR) != null) {
			getErrorMap().setError("brnCode", inputDTO.get(ContentManager.ERROR));
			return false;
		}
		return true;
	}

	public DTObject validateBranchCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		DTObject formDTO = getFormDTO();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		AccessValidator validation = new AccessValidator();
		try {
			String brnCode = inputDTO.get("BRANCH_CODE");
			if (validation.isEmpty(brnCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			formDTO.set("MBRN_CODE", brnCode);
			formDTO = validation.validateBranch(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, formDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (!formDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
			resultDTO.set("MBRN_NAME", formDTO.get("MBRN_NAME"));
			resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateBrnListCode() {
		AccessValidator validation = new AccessValidator();
		try {
			if (validation.isEmpty(brnListCode)) {
				getErrorMap().setError("brnListCode", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidCode(brnListCode)) {
				getErrorMap().setError("brnListCode", BackOfficeErrorCodes.INVALID_CODE);
				return false;
			}
			DTObject formDTO = getFormDTO();
			formDTO.set(ContentManager.CODE, brnListCode);
			formDTO = validation.validateBranchListCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("brnListCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			if (getAction().equals(ADD)) {
				if (formDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
					getErrorMap().setError("brnListCode", BackOfficeErrorCodes.RECORD_EXISTS);
					return false;
				}
				return true;
			} else if (getAction().equals(MODIFY)) {
				if (formDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
					getErrorMap().setError("brnListCode", BackOfficeErrorCodes.RECORD_NOT_EXISTS);
					return false;
				}
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("brnListCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateDescription() {
		CommonValidator validation = new CommonValidator();
		try {
			if (!validation.isValidDescription(brnListDescription)) {
				getErrorMap().setError("brnListDescription", BackOfficeErrorCodes.INVALID_DESCRIPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("brnListDescription", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	private boolean validateGrid() {
		AccessValidator validation = new AccessValidator();
		try {
			DTDObject dtdObject = new DTDObject();
			dtdObject.addColumn(0, "SL");
			dtdObject.addColumn(1, "BRANCH_CODE");
			dtdObject.addColumn(2, "DESCN");
			dtdObject.setXML(xmlBrnList);
			if (dtdObject.getRowCount() == 0) {
				getErrorMap().setError("brnCode", BackOfficeErrorCodes.ADD_ATLEAST_ONE_ROW);
				return false;
			}
			if (!validation.checkDuplicateRows(dtdObject, 1)) {
				getErrorMap().setError("brnCode", BackOfficeErrorCodes.DUPLICATE_DATA);
				return false;
			}
			for (int i = 0; i < dtdObject.getRowCount(); i++) {
				if (!validateBrnCode(dtdObject.getValue(i, 1))) {
					getErrorMap().setError("brnCode", BackOfficeErrorCodes.INVALID_CODE);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("brnCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateRemarks() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(remarks)) {
				getErrorMap().setError("remarks", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidRemarks(remarks)) {
				getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}
}
