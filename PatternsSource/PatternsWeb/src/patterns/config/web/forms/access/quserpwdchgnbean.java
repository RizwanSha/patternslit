package patterns.config.web.forms.access;

import java.sql.Date;
import java.sql.ResultSet;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.framework.web.DHTMLXGridUtility;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.AccessValidator;
import patterns.config.web.forms.GenericFormBean;

public class quserpwdchgnbean extends GenericFormBean {

	private static final long serialVersionUID = -6219657363150466160L;

	private String userID;
	private String fromDate;
	private String toDate;

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public void reset() {
		userID = RegularConstants.EMPTY_STRING;
		fromDate = RegularConstants.EMPTY_STRING;
		toDate = RegularConstants.EMPTY_STRING;
	}

	public void validate() {

	}

	public DTObject validateUserID(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		AccessValidator validation = new AccessValidator();
		String userID = inputDTO.get("USER_ID");
		try {
			if (!validation.isEmpty(userID)) {
				inputDTO.set(ContentManager.FETCH_COLUMNS, "USER_ID,USER_NAME");
				resultDTO = validation.validateInternalUser(inputDTO);
				if (resultDTO.get(ContentManager.ERROR) != null) {
					resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
					return resultDTO;
				}
				if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_USERID);
					return resultDTO;
				}
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public DTObject loadUserPwdGrid(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);

		Date fromDate = null;
		String userId = null;
		Date toDate = null;

		AccessValidator validator = new AccessValidator();
		try {

			toDate = new java.sql.Date(BackOfficeFormatUtils.getDate(inputDTO.get("TO_DATE"), BackOfficeConstants.JAVA_DATE_FORMAT).getTime());
			if (!validator.isEmpty(inputDTO.get("FROM_DATE")))
				fromDate = new java.sql.Date(BackOfficeFormatUtils.getDate(inputDTO.get("FROM_DATE"), BackOfficeConstants.JAVA_DATE_FORMAT).getTime());

			if (!validator.isEmpty(inputDTO.get("USER_ID"))) {
				userId = inputDTO.get("USER_ID");
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			validator.close();
		}

		DHTMLXGridUtility gridUtility = new DHTMLXGridUtility();
		DBContext dbContext = new DBContext();
		try {
			DBUtil util = dbContext.createUtilInstance();
			util.reset();
			util.setMode(DBUtil.CALLABLE);
			util.setSql("CALL SP_USERPWDCHGN(?,?,?,?)");
			util.setString(1, context.getEntityCode());
			util.setString(2, userId);
			util.setDate(3, fromDate);
			util.setDate(4, toDate);
			util.execute();
			util.reset();
			util.setMode(DBUtil.PREPARED);
			String sql = "SELECT @ROWNUM:=@ROWNUM+1,A.USER_ID,U.USER_NAME,A.CHANGE_DATETIME FROM TMPUSERPWDCHGN A,(SELECT @ROWNUM:=0) X,USERS U,ROLES B WHERE A.ENTITY_NUM=? AND U.USER_ID = A.USER_ID AND U.ENTITY_CODE = B.ENTITY_CODE AND U.ROLE_CODE = B.CODE AND B.ROLE_TYPE IN ('1','2','3','4')";
			if (userId != null)
				sql = sql + " AND A.USER_ID=?";
			util.setSql(sql);
			util.setString(1, context.getEntityCode());
			if (userId != null)
				util.setString(2, inputDTO.get("USER_ID"));
			ResultSet rset = util.executeQuery();
			gridUtility.init();
			while (rset.next()) {
				gridUtility.startRow(rset.getString(1));
				gridUtility.setCell(rset.getString(1));
				gridUtility.setCell(rset.getString(2));
				gridUtility.setCell(rset.getString(3));
				gridUtility.setCell(rset.getString(4));
				gridUtility.endRow();
			}
			gridUtility.finish();
			util.reset();
			resultDTO.set("XML", gridUtility.getXML());
			resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			dbContext.close();
		}
		return resultDTO;
	}
}