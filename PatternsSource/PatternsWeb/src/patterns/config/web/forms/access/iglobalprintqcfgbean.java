package patterns.config.web.forms.access;

import java.util.HashMap;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.BindParameterType;
import patterns.config.framework.database.DBEntry;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.process.TBAActionType;
import patterns.config.framework.service.DTDObject;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.AccessValidator;
import patterns.config.validations.CommonValidator;
import patterns.config.web.forms.FileFormBean;

/**
 * This program is used for configuring global print queues.
 * 
 * @version 1.0
 * @author Tulasi Thota
 * @since 19-FEB-2015 <br>
 *        <b>Modified History</b> <BR>
 *        <U><B> Sl.No Modified Date Author Modified Changes Version </B></U> <br>
 * 
 */
public class iglobalprintqcfgbean extends FileFormBean {

	private static final long serialVersionUID = 547725176292318079L;
	private String effectiveDate;
	private String printTypeId;
	private String printQueueId;
	private String xmlPrinterConfigGrid;
	private String gridbox_rowid;

	public String getPrintTypeId() {
		return printTypeId;
	}

	public void setPrintTypeId(String printTypeId) {
		this.printTypeId = printTypeId;
	}

	public String getPrintQueueId() {
		return printQueueId;
	}

	public void setPrintQueueId(String printQueueId) {
		this.printQueueId = printQueueId;
	}

	public String getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public String getXmlPrinterConfigGrid() {
		return xmlPrinterConfigGrid;
	}

	public void setXmlPrinterConfigGrid(String xmlPrinterConfigGrid) {
		this.xmlPrinterConfigGrid = xmlPrinterConfigGrid;
	}

	public String getGridbox_rowid() {
		return gridbox_rowid;
	}

	public void setGridbox_rowid(String gridbox_rowid) {
		this.gridbox_rowid = gridbox_rowid;
	}

	public void reset() {
		effectiveDate = RegularConstants.EMPTY_STRING;
		printTypeId = RegularConstants.EMPTY_STRING;
		printQueueId = RegularConstants.EMPTY_STRING;
		setProcessBO("patterns.config.framework.bo.access.iglobalprintqcfgBO");
	}

	public void validate() {
		if (validateEffectiveDate()) {
			validateGrid();
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.setObject(
						"EFFT_DATE",
						BackOfficeFormatUtils.getDate(effectiveDate,
								context.getDateFormat()));
				DTDObject dtdObject = new DTDObject();
				dtdObject.addColumn(0, "SL");
				dtdObject.addColumn(1, "PRNTYPE_ID");
				dtdObject.addColumn(2, "DISCRIPTION");
				dtdObject.addColumn(3, "PRNQ_ID");
				dtdObject.addColumn(4, "DISCRIPTION");
				dtdObject.setXML(xmlPrinterConfigGrid);
				formDTO.setDTDObject("GLOBALPRNQCFGHISTDTL", dtdObject);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("effectiveDate",
					BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
	}

	public boolean validateEffectiveDate() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(effectiveDate)) {
				getErrorMap().setError("effectiveDate",
						BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			TBAActionType AT;
			if (getAction().equals(ADD))
				AT = TBAActionType.ADD;
			else
				AT = TBAActionType.MODIFY;
			if (!validation.isEffectiveDateValid(effectiveDate, AT)) {
				getErrorMap().setError("effectiveDate",
						BackOfficeErrorCodes.INVALID_EFFECTIVE_DATE);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("effectiveDate",
					BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public DTObject validateEffectiveDate(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		AccessValidator validation = new AccessValidator();
		String efftDate = inputDTO.get("EFFT_DATE");
		String action = inputDTO.get(ContentManager.USER_ACTION);
		try {
			if (validation.isEmpty(efftDate)) {
				resultDTO.set(ContentManager.ERROR,
						BackOfficeErrorCodes.HMS_FIELD_BLANK);
				return resultDTO;
			}
			HashMap<Integer, DBEntry> whereClauseFieldsMap = new HashMap<Integer, DBEntry>();
			whereClauseFieldsMap.put(1,
					new DBEntry("ENTITY_CODE", context.getEntityCode(),
							BindParameterType.VARCHAR));
			whereClauseFieldsMap.put(
					2,
					new DBEntry("EFFT_DATE",
							BackOfficeFormatUtils.getDate(
									inputDTO.get("EFFT_DATE"),
									context.getDateFormat()),
							BindParameterType.DATE));
			boolean isRecordAvailable = validation.isRecordAvailable(
					"GLOBALPRNQCFGHIST", whereClauseFieldsMap);
			if (isRecordAvailable) {
				if (action.equals(ADD)) {
					resultDTO.set(ContentManager.ERROR,
							BackOfficeErrorCodes.HMS_RECORD_EXISTS);
				}
			} else {
				if (action.equals(MODIFY)) {
					resultDTO.set(ContentManager.ERROR,
							BackOfficeErrorCodes.HMS_RECORD_NOT_EXISTS);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR,
					BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	private boolean checkPrintTypeId(String value) {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(value)) {
				return false;
			}
			DTObject formDTO = getFormDTO();
			formDTO.set("PRNTYPE_ID", value);
			formDTO.set(ContentManager.USER_ACTION, USAGE);
			formDTO = validatePrintTypeId(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("printTypeId",
						formDTO.get(ContentManager.ERROR));
				return false;
			}
			if (!formDTO.get(ContentManager.RESULT).equalsIgnoreCase(
					ContentManager.DATA_AVAILABLE)) {
				getErrorMap().setError("printTypeId",
						BackOfficeErrorCodes.INVALID_CODE);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("printTypeId",
					BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	private boolean validateGrid() {
		AccessValidator validation = new AccessValidator();
		try {
			DTDObject dtdObject = new DTDObject();
			dtdObject.addColumn(0, "SL");
			dtdObject.addColumn(1, "PRNTYPE_ID");
			dtdObject.addColumn(2, "DISCRIPTION");
			dtdObject.addColumn(3, "PRNQ_ID");
			dtdObject.addColumn(4, "DISCRIPTION");
			dtdObject.setXML(xmlPrinterConfigGrid);
			if (dtdObject.getRowCount() <= 0) {
				getErrorMap().setError("printTypeId",
						BackOfficeErrorCodes.HMS_ATLEAST_ONE_ROW);
				return false;
			}
			if (!validation.checkDuplicateRows(dtdObject, 1)) {
				getErrorMap().setError("printTypeId",
						BackOfficeErrorCodes.HMS_DUPLICATE_DATA);
				return false;
			}
			for (int i = 0; i < dtdObject.getRowCount(); i++) {
				if (!checkPrintTypeId(dtdObject.getValue(i, 1))) {
					getErrorMap().setError("printTypeId",
							BackOfficeErrorCodes.INVALID_ACTION);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("printTypeId",
					BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public DTObject validatePrintTypeId(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		AccessValidator accessValidation = new AccessValidator();
		String printQueId = inputDTO.get("PRNTYPE_ID");
		try {
			if (accessValidation.isEmpty(printQueId)) {
				resultDTO.set(ContentManager.ERROR,
						BackOfficeErrorCodes.HMS_FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
			resultDTO = accessValidation.validatePrintType(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR,
						resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO
					.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR,
						BackOfficeErrorCodes.HMS_RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR,
					BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			accessValidation.close();
		}
		return resultDTO;
	}

	public DTObject validatePrintQueueId(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		AccessValidator masterValidation = new AccessValidator();
		String printQueId = inputDTO.get("PRNQ_ID");
		try {
			if (masterValidation.isEmpty(printQueId)) {
				resultDTO.set(ContentManager.ERROR,
						BackOfficeErrorCodes.HMS_FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
			resultDTO = masterValidation.validatePrintQueueId(inputDTO);
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO
					.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR,
						BackOfficeErrorCodes.HMS_RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR,
					BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			masterValidation.close();
		}
		return resultDTO;
	}
}