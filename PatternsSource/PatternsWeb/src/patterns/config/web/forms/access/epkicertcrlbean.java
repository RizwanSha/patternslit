package patterns.config.web.forms.access;

import java.sql.ResultSet;
import java.util.HashMap;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.BindParameterType;
import patterns.config.framework.database.DBEntry;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.AccessValidator;
import patterns.config.validations.CommonValidator;
import patterns.config.web.forms.FileFormBean;

public class epkicertcrlbean extends FileFormBean {

	private static final long serialVersionUID = -6219657363150466160L;

	private boolean enabled;
	private String remarks;
	private String fileInventoryNumber;
	private String rootCaCode;
	private String fileName;
	private String filePurpose;
	private String changeDateTime;

	public String getChangeDateTime() {
		return changeDateTime;
	}

	public void setChangeDateTime(String changeDateTime) {
		this.changeDateTime = changeDateTime;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getFilePurpose() {
		return filePurpose;
	}

	public void setFilePurpose(String filePurpose) {
		this.filePurpose = filePurpose;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileInventoryNumber() {
		return fileInventoryNumber;
	}

	public void setFileInventoryNumber(String fileInventoryNumber) {
		this.fileInventoryNumber = fileInventoryNumber;
	}

	public String getRootCaCode() {
		return rootCaCode;
	}

	public void setRootCaCode(String rootCaCode) {
		this.rootCaCode = rootCaCode;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public void reset() {
		remarks = RegularConstants.EMPTY_STRING;
		enabled = false;
		fileInventoryNumber = RegularConstants.EMPTY_STRING;
		rootCaCode = RegularConstants.EMPTY_STRING;
		setProcessBO("patterns.config.framework.bo.access.epkicertcrlBO");
	}

	public void validate() {
		if (validateRootCaCode()) {
			if (validatePK()) {
				validateRemarks();
				if (enabled) {
					validateInventoryNumber();
				}
			}
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.set("REMARKS", remarks);
				formDTO.set("ROOT_CA_CODE", rootCaCode);
				formDTO.set("ENABLED", decodeBooleanToString(enabled));
				if (enabled) {
					formDTO.set("INV_NUM", fileInventoryNumber);
				} else {
					formDTO.set("INV_NUM", RegularConstants.NULL);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("rootCaCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
	}

	public boolean validateRootCaCode() {
		AccessValidator validation = new AccessValidator();
		DTObject formDTO = getFormDTO();
		try {
			if (validation.isEmpty(rootCaCode)) {
				getErrorMap().setError("rootCaCode", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			formDTO.set(ContentManager.CODE, rootCaCode);
			formDTO.set(ContentManager.FETCH, ContentManager.FETCH_ALL);
			formDTO = validation.validateRootCaCode(formDTO);
			if (formDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				getErrorMap().setError("rootCaCode", BackOfficeErrorCodes.INVALID_CODE);
				return false;
			}
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("rootCaCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("rootCaCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateRemarks() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(remarks)) {
				getErrorMap().setError("remarks", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidRemarks(remarks)) {
				getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validatePK() {
		CommonValidator validation = new CommonValidator();
		try {
			HashMap<Integer, DBEntry> whereClauseFieldsMap = new HashMap<Integer, DBEntry>();

			whereClauseFieldsMap.put(1, new DBEntry("ENTITY_CODE", context.getEntityCode(), BindParameterType.VARCHAR));
			whereClauseFieldsMap.put(2, new DBEntry("ROOT_CA_CODE", rootCaCode, BindParameterType.VARCHAR));
			boolean isRecordAvailable = validation.isRecordAvailable("PKICERTAUTHCRL", whereClauseFieldsMap);
			if (isRecordAvailable) {
				if (getAction().equals(ADD)) {
					getErrorMap().setError("rootCaCode", BackOfficeErrorCodes.RECORD_EXISTS);
					return false;
				}
				return true;
			} else {
				if (getAction().equals(MODIFY)) {
					getErrorMap().setError("rootCaCode", BackOfficeErrorCodes.RECORD_NOT_EXISTS);
					return false;
				}
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("rootCaCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateInventoryNumber() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(fileInventoryNumber)) {
				getErrorMap().setError("fileInventoryNumber", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("fileInventoryNumber", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public DTObject fetchFileName(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
		DBContext dbContext = new DBContext();
		try {
			String inventoryNumber = inputDTO.get("CERT_INV_NUM");
			String sqlQuery = "SELECT DTL.CERT_FILE_NAME CERT_FILE_NAME FROM PKICERTINVENTORY  DTL WHERE DTL.ENTITY_CODE = ? AND DTL.CERT_INV_NUM = ?";
			DBUtil util = dbContext.createUtilInstance();
			util.setSql(sqlQuery);
			util.setString(1, context.getEntityCode());
			util.setString(2, inventoryNumber);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				resultDTO.set("CERT_FILE_NAME", rset.getString(1));
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
			}
			util.reset();
			return resultDTO;
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			dbContext.close();
		}
		return resultDTO;
	}
}