package patterns.config.web.forms.access;

import java.util.HashMap;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.BindParameterType;
import patterns.config.framework.database.CM_LOVREC;
import patterns.config.framework.database.DBEntry;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.process.TBAActionType;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.AccessValidator;
import patterns.config.validations.CommonValidator;
import patterns.config.web.forms.GenericFormBean;

public class erolebrnlistbean extends GenericFormBean {

	private static final long serialVersionUID = -6219657363150466160L;

	private String roleCode;
	private String brnListCode;
	private String effectiveDate;
	private String remarks;
	private boolean enabled;

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public String getRoleCode() {
		return roleCode;
	}

	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}

	public String getBrnListCode() {
		return brnListCode;
	}

	public void setBrnListCode(String brnListCode) {
		this.brnListCode = brnListCode;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public void reset() {
		roleCode = RegularConstants.EMPTY_STRING;
		effectiveDate = RegularConstants.EMPTY_STRING;
		brnListCode = RegularConstants.EMPTY_STRING;
		remarks = RegularConstants.EMPTY_STRING;
		setProcessBO("patterns.config.framework.bo.access.erolebrnlistBO");
	}

	public void validate() {
		if (validateRoleCode()) {
			if (validateEffectiveDate()) {
				validatePK();
				if (enabled)
					validateBranchListCode();
				validateRemarks();
			}
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.set("ROLE_CODE", roleCode);
				formDTO.setObject("EFFT_DATE", BackOfficeFormatUtils.getDate(effectiveDate, context.getDateFormat()));
				formDTO.set("ENABLED", decodeBooleanToString(enabled));
				if (enabled) {
					formDTO.set("BRANCH_LIST_CODE", brnListCode);
				} else {
					formDTO.set("BRANCH_LIST_CODE", RegularConstants.NULL);
				}
				formDTO.set("REMARKS", remarks);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("roleCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
	}

	public boolean validateRoleCode() {
		AccessValidator validation = new AccessValidator();
		try {
			if (validation.isEmpty(roleCode)) {
				getErrorMap().setError("roleCode", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			DTObject formDTO = getFormDTO();
			formDTO.set(ContentManager.CODE, roleCode);
			formDTO.set(ContentManager.FETCH, ContentManager.FETCH_ALL);
			formDTO = validation.validateInternalRole(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("roleCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			if (formDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				getErrorMap().setError("roleCode", BackOfficeErrorCodes.INVALID_CODE);
				return false;
			}
			String roleType = formDTO.get("ROLE_TYPE");
			if (!(roleType.equals(CM_LOVREC.COMMON_ROLES_1) || roleType.equals(CM_LOVREC.COMMON_ROLES_2) || roleType.equals(CM_LOVREC.COMMON_ROLES_3) || roleType.equals(CM_LOVREC.COMMON_ROLES_4))) {
				getErrorMap().setError("roleCode", BackOfficeErrorCodes.INSUFFICIENT_ROLE_RIGHTS);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("roleCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateEffectiveDate() {
		CommonValidator validation = new CommonValidator();
		try {
			TBAActionType AT;
			if (getAction().equals(ADD))
				AT = TBAActionType.ADD;
			else
				AT = TBAActionType.MODIFY;
			if (validation.isEffectiveDateValid(effectiveDate, AT)) {
				return true;
			} else {
				getErrorMap().setError("effectiveDate", BackOfficeErrorCodes.INVALID_EFFECTIVE_DATE);
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("effectiveDate", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateBranchListCode() {
		AccessValidator validation = new AccessValidator();
		try {
			if (validation.isEmpty(brnListCode)) {
				getErrorMap().setError("brnListCode", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			DTObject formDTO = getFormDTO();
			formDTO.set(ContentManager.CODE, brnListCode);
			formDTO = validation.validateBranchListCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("brnListCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			if (formDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				getErrorMap().setError("brnListCode", BackOfficeErrorCodes.INVALID_CODE);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("brnListCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateRemarks() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(remarks)) {
				getErrorMap().setError("remarks", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidRemarks(remarks)) {
				getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validatePK() {
		AccessValidator validation = new AccessValidator();
		HashMap<Integer, DBEntry> whereClauseFieldsMap = new HashMap<Integer, DBEntry>();
		whereClauseFieldsMap.put(1, new DBEntry("ENTITY_CODE", context.getEntityCode(), BindParameterType.VARCHAR));
		whereClauseFieldsMap.put(2, new DBEntry("ROLE_CODE", roleCode, BindParameterType.VARCHAR));
		whereClauseFieldsMap.put(3, new DBEntry("EFFT_DATE", BackOfficeFormatUtils.getDate(effectiveDate, context.getDateFormat()), BindParameterType.DATE));

		boolean isRecordAvailable = validation.isRecordAvailable("ROLEBRNLISTALLOC", whereClauseFieldsMap);
		try {
			if (isRecordAvailable) {
				if (getAction().equals(ADD)) {
					getErrorMap().setError("roleCode", BackOfficeErrorCodes.RECORD_EXISTS);
					return false;
				}
				return true;
			} else {
				if (getAction().equals(MODIFY)) {
					getErrorMap().setError("roleCode", BackOfficeErrorCodes.RECORD_NOT_EXISTS);
					return false;
				}
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("roleCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}
}