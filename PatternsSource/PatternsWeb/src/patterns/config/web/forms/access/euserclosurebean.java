package patterns.config.web.forms.access;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.process.TBAActionType;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeDateUtils;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.AccessValidator;
import patterns.config.validations.CommonValidator;
import patterns.config.web.forms.GenericFormBean;

public class euserclosurebean extends GenericFormBean {

	private static final long serialVersionUID = -6219657363150466160L;

	private String userId;
	private String closureDate;
	private String remarks;
	private String entryDate;
	private String daySerial;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getClosureDate() {
		return closureDate;
	}

	public void setClosureDate(String closureDate) {
		this.closureDate = closureDate;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getEntryDate() {
		return entryDate;
	}

	public void setEntryDate(String entryDate) {
		this.entryDate = entryDate;
	}

	public String getDaySerial() {
		return daySerial;
	}

	public void setDaySerial(String daySerial) {
		this.daySerial = daySerial;
	}

	public void reset() {
		entryDate = RegularConstants.EMPTY_STRING;
		daySerial = RegularConstants.EMPTY_STRING;
		userId = RegularConstants.EMPTY_STRING;
		closureDate = RegularConstants.EMPTY_STRING;
		remarks = RegularConstants.EMPTY_STRING;
		setProcessBO("patterns.config.framework.bo.access.euserclosureBO");
	}

	public void validate() {
		if (validateEntryDate()) {
			validateUserID();
			validateRemarks();
			validateClosureDate();
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.setObject("ENTRY_DATE", BackOfficeFormatUtils.getDate(entryDate, context.getDateFormat()));
				if (daySerial != null && !daySerial.equals(RegularConstants.EMPTY_STRING))
					formDTO.set("DAY_SL", daySerial);
				formDTO.set("CODE", userId);
				formDTO.setObject("DATE_OF_RELIVING", BackOfficeFormatUtils.getDate(closureDate, context.getDateFormat()));
				formDTO.setObject("REMARKS", remarks);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("userId", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
	}

	public boolean validateEntryDate() {
		CommonValidator validation = new CommonValidator();
		try {
			java.util.Date dateInstance = validation.isValidDate(entryDate);
			java.util.Date currentBusinessDate = context.getCurrentBusinessDate();
			if (!BackOfficeDateUtils.isDateEqual(dateInstance, currentBusinessDate)) {
				getErrorMap().setError("entryDate", BackOfficeErrorCodes.DATE_ECBD);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("entryDate", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateClosureDate() {
		CommonValidator validator = new CommonValidator();
		try {
			if (!validator.isEffectiveDateValid(closureDate, TBAActionType.MODIFY)) {
				getErrorMap().setError("closureDate", BackOfficeErrorCodes.INVALID_EFFECTIVE_DATE);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			validator.close();
		}
	}

	public boolean validateUserID() {
		AccessValidator validation = new AccessValidator();
		try {
			if (validation.isEmpty(userId)) {
				getErrorMap().setError("userId", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			DTObject formDTO = getFormDTO();
			formDTO.set(ContentManager.USER_ID, userId);
			formDTO.set(ContentManager.FETCH, ContentManager.FETCH_ALL);
			formDTO = validation.validateInternalUser(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("userId", formDTO.get(ContentManager.ERROR));
				return false;
			}
			if (formDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				getErrorMap().setError("userId", BackOfficeErrorCodes.RECORD_NOT_EXISTS);
				return false;
			}
			if (formDTO.get("DATE_OF_RELIVING") != null && !formDTO.get("DATE_OF_RELIVING").equals(ContentManager.EMPTY_STRING)) {
				getErrorMap().setError("userId", BackOfficeErrorCodes.RECORD_NOT_EXISTS);
				return false;
			}
			String _branchCode = formDTO.get("BRANCH_CODE");
			if (_branchCode.equals(RegularConstants.COLUMN_DISABLE)) {
				getErrorMap().setError("userId", BackOfficeErrorCodes.INSUFFICIENT_ROLE_RIGHTS);
				return false;
			}
			if (userId.equals(context.getUserID())) {
				getErrorMap().setError("userId", BackOfficeErrorCodes.CANNOT_ALLOCATE_TO_SELF);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("userId", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public DTObject getUserDetails(DTObject input) {
		input.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		userId = input.get("USER_ID");
		AccessValidator validation = new AccessValidator();
		try {
			input.set(ContentManager.FETCH, ContentManager.FETCH_ALL);
			input = validation.validateInternalUser(input);
			if (input.get(ContentManager.ERROR) != null) {
				input.set(ContentManager.ERROR, input.get(ContentManager.ERROR));
				return input;
			}
			if (input.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				input.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_NOT_EXISTS);
				return input;
			}
			if (input.get("DATE_OF_RELIVING") != null && !input.get("DATE_OF_RELIVING").equals(ContentManager.EMPTY_STRING)) {
				input.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_NOT_EXISTS);
				return input;
			}
			if (userId.equals(context.getUserID())) {
				input.set(ContentManager.ERROR, BackOfficeErrorCodes.CANNOT_ALLOCATE_TO_SELF);
				return input;
			}
			input.set("INV_NUMBER", input.get("ADDR_INV_NUM"));
			input = validation.getAddressDetails(input);
			if (input.get(ContentManager.ERROR) != null) {
				return input;
			}
			if (input.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				input.set(ContentManager.ERROR, BackOfficeErrorCodes.ADDR_TYPE_UNAVAIL);
				return input;
			}
			input.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
		} catch (Exception e) {
			e.printStackTrace();
			input.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return input;
	}

	public boolean validateRemarks() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(remarks)) {
				getErrorMap().setError("remarks", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidRemarks(remarks)) {
				getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}
}