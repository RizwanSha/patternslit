package patterns.config.web.forms.access;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.BindParameterType;
import patterns.config.framework.database.DBEntry;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.process.TBAActionType;
import patterns.config.framework.service.DTDObject;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.AccessValidator;
/**
 * This program is used for maintaining user printer configurations.
 * 
 * 
 * @version 1.0
 * @author vivekananda Reddy
 * @since 19-Feb-2015 <br>
 *        <b>Modified History</b> <BR>
 *        <U><B> Sl.No Modified Date Author Modified Changes Version </B></U> <br>
 */
import patterns.config.web.forms.GenericFormBean;

public class iuserprintercfgbean extends GenericFormBean {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public String userId;
	public String effectiveDate;
	public String printTypeId;
	public String printerId;
	public String defaultPrinter;
	public String xmlUserPrinterConfigGrid;
	public String pgmId;
	public String progPrinterId;
	public String xmlUserProgramPrinterConfigGrid;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public String getPrintTypeId() {
		return printTypeId;
	}

	public void setPrintTypeId(String printTypeId) {
		this.printTypeId = printTypeId;
	}

	public String getPrinterId() {
		return printerId;
	}

	public void setPrinterId(String printerId) {
		this.printerId = printerId;
	}

	public String getDefaultPrinter() {
		return defaultPrinter;
	}

	public void setDefaultPrinter(String defaultPrinter) {
		this.defaultPrinter = defaultPrinter;
	}

	public String getPgmId() {
		return pgmId;
	}

	public void setPgmId(String pgmId) {
		this.pgmId = pgmId;
	}

	public String getProgPrinterId() {
		return progPrinterId;
	}

	public void setProgPrinterId(String progPrinterId) {
		this.progPrinterId = progPrinterId;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getXmlUserPrinterConfigGrid() {
		return xmlUserPrinterConfigGrid;
	}

	public void setXmlUserPrinterConfigGrid(String xmlUserPrinterConfigGrid) {
		this.xmlUserPrinterConfigGrid = xmlUserPrinterConfigGrid;
	}

	public String getXmlUserProgramPrinterConfigGrid() {
		return xmlUserProgramPrinterConfigGrid;
	}

	public void setXmlUserProgramPrinterConfigGrid(String xmlUserProgramPrinterConfigGrid) {
		this.xmlUserProgramPrinterConfigGrid = xmlUserProgramPrinterConfigGrid;
	}

	@Override
	public void reset() {
		userId = RegularConstants.EMPTY_STRING;
		effectiveDate = RegularConstants.EMPTY_STRING;
		printTypeId = RegularConstants.EMPTY_STRING;
		printerId = RegularConstants.EMPTY_STRING;
		defaultPrinter = RegularConstants.EMPTY_STRING;
		pgmId = RegularConstants.EMPTY_STRING;
		progPrinterId = RegularConstants.EMPTY_STRING;
		setProcessBO("patterns.config.framework.bo.access.iuserprintercfgBO");
	}

	@Override
	public void validate() {
		if (validateUserId()) {
			if (validateEffectiveDate()) {
				validatePrinterConfigGrid();
				validateProgramPrinterConfigGrid();
				validateDefaultPrinter();
			}
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.set("USER_ID", userId);
				formDTO.set("EFFT_DATE", effectiveDate);
				DTDObject dtdObject = new DTDObject();
				dtdObject.addColumn(0, "SL");
				dtdObject.addColumn(1, "PRNTYPE_ID");
				dtdObject.addColumn(2, "DESCRIPTION");
				dtdObject.addColumn(3, "PRINTER_ID");
				dtdObject.addColumn(4, "PRINTER_DESC");
				dtdObject.addColumn(5, "DEFAULT_PRINTER");
				dtdObject.setXML(xmlUserPrinterConfigGrid);
				formDTO.setDTDObject("IUSERPRINTERCFGHISTDTL", dtdObject);
				DTDObject dtdObjectpgm = new DTDObject();
				dtdObjectpgm.addColumn(0, "SL");
				dtdObjectpgm.addColumn(1, "PGM_ID");
				dtdObjectpgm.addColumn(2, "PGM_DESCN");
				dtdObjectpgm.addColumn(3, "PRINTER_ID");
				dtdObjectpgm.addColumn(4, "PRINTER_DESC");
				dtdObjectpgm.setXML(xmlUserProgramPrinterConfigGrid);
				formDTO.setDTDObject("USERPRINTERCFGHISTPGM", dtdObjectpgm);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("userId", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		}
	}

	public boolean validateUserId() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("USER_ID", userId);
			formDTO.set("ACTION", getAction());
			formDTO = validateUserId(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("userId", formDTO.get(ContentManager.ERROR));
				return false;
			}

			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("userId", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		}
		return false;
	}

	public boolean validateEffectiveDate() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("EFFT_DATE", effectiveDate);
			formDTO.set("USER_ID", userId);
			formDTO.set("ACTION", getAction());
			formDTO = validateEffectiveDate(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("effectiveDate", formDTO.get(ContentManager.ERROR));
				return false;
			}

			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("effectiveDate", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		}
		return false;
	}

	public boolean validatePrinterConfigGrid() {
		AccessValidator validation = new AccessValidator();
		try {
			DTDObject dtdObject = new DTDObject();
			dtdObject.addColumn(0, "SL");
			dtdObject.addColumn(1, "PRNTYPE_ID");
			dtdObject.addColumn(2, "DESCRIPTION");
			dtdObject.addColumn(3, "PRINTER_ID");
			dtdObject.addColumn(4, "PRINTER_DESC");
			dtdObject.addColumn(5, "DEFAULT_PRINTER");
			dtdObject.setXML(xmlUserPrinterConfigGrid);
			if (dtdObject.getRowCount() <= 0) {
				getErrorMap().setError("printTypeId", BackOfficeErrorCodes.HMS_ATLEAST_ONE_ROW);
				return false;
			}
			if (!validation.checkDuplicateRows(dtdObject, 1, 3)) {
				getErrorMap().setError("printTypeId", BackOfficeErrorCodes.HMS_DUPLICATE_DATA);
				return false;
			}
			for (int i = 0; i < dtdObject.getRowCount(); i++) {
				if (!validatePrintTypeId(dtdObject.getValue(i, 1))) {
					getErrorMap().setError("printTypeId", BackOfficeErrorCodes.HMS_INVALID_ACTION);
					return false;
				}
				if (!validatePrinterId(dtdObject.getValue(i, 3), dtdObject.getValue(i, 1))) {
					getErrorMap().setError("printerId", BackOfficeErrorCodes.HMS_INVALID_ACTION);
					return false;
				}

			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("printTypeId", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateDefaultPrinter() {
		try {
			DTDObject dtdObject = new DTDObject();
			dtdObject.addColumn(0, "SL");
			dtdObject.addColumn(1, "PRNTYPE_ID");
			dtdObject.addColumn(2, "DESCRIPTION");
			dtdObject.addColumn(3, "PRINTER_ID");
			dtdObject.addColumn(4, "PRINTER_DESC");
			dtdObject.addColumn(5, "DEFAULT_PRINTER");
			dtdObject.setXML(xmlUserPrinterConfigGrid);
			List<String> prnttype = new ArrayList<>();
			for (int i = 0; i < dtdObject.getRowCount(); i++) {
				prnttype.add(i, dtdObject.getValue(i, 1));
			}
			Set<String> prnttypef = new HashSet<String>(prnttype);
			Iterator<String> itr = prnttypef.iterator();
			int count = 0;
			while (itr.hasNext()) {
				String temp = itr.next();
				for (int k = 0; k < dtdObject.getRowCount(); k++) {
					if (temp.equalsIgnoreCase(dtdObject.getValue(k, 1))) {
						if (dtdObject.getValue(k, 5).equalsIgnoreCase("Yes")) {
							count++;
							if (count > 1) {
								getErrorMap().setError("printTypeId", BackOfficeErrorCodes.HMS_ATLEAST_ONE_DEFT_PRINTER);
								return false;
							}
						}
					}
				}
				if (count == 0) {
					getErrorMap().setError("printTypeId", BackOfficeErrorCodes.HMS_EACH_PRNTYPE_ONE_DFT_PRINTER);
					return false;
				}
				count = 0;
			}

			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("printTypeId", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
		}
		return false;
	}

	public boolean validateProgramPrinterConfigGrid() {
		AccessValidator validation = new AccessValidator();
		try {
			DTDObject dtdObjectpgm = new DTDObject();
			dtdObjectpgm.addColumn(0, "SL");
			dtdObjectpgm.addColumn(1, "PGM_ID");
			dtdObjectpgm.addColumn(2, "PGM_DESCN");
			dtdObjectpgm.addColumn(3, "PRINTER_ID");
			dtdObjectpgm.addColumn(4, "PRINTER_DESC");
			dtdObjectpgm.setXML(xmlUserProgramPrinterConfigGrid);
			if (dtdObjectpgm.getRowCount() <= 0) {
				getErrorMap().setError("pgmId", BackOfficeErrorCodes.HMS_ATLEAST_ONE_ROW);
				return false;
			}
			if (!validation.checkDuplicateRows(dtdObjectpgm, 1)) {
				getErrorMap().setError("pgmId", BackOfficeErrorCodes.HMS_DUPLICATE_DATA);
				return false;
			}
			for (int i = 0; i < dtdObjectpgm.getRowCount(); i++) {
				if (!validatePgmId(dtdObjectpgm.getValue(i, 1))) {
					getErrorMap().setError("pgmId", BackOfficeErrorCodes.HMS_INVALID_ACTION);
					return false;
				}
				if (!validatePrinterIdpgm(dtdObjectpgm.getValue(i, 3), dtdObjectpgm.getValue(i, 1))) {
					getErrorMap().setError("printerId", BackOfficeErrorCodes.HMS_INVALID_ACTION);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("pgmId", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validatePrintTypeId(String printTypeId) {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("PRNTYPE_ID", printTypeId);
			formDTO.set("ACTION", getAction());
			formDTO = validatePrintTypeId(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("printTypeId", formDTO.get(ContentManager.ERROR));
				return false;
			}

			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("printTypeId", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		}
		return false;
	}

	public boolean validatePrinterId(String printerId, String prntType) {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("PRINTER_ID", printerId);
			formDTO.set("PRNTYPE_ID", prntType);
			formDTO.set("USER_ID", userId);
			formDTO.set("ACTION", getAction());
			formDTO = validatePrinterId(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("printerId", formDTO.get(ContentManager.ERROR));
				return false;
			}

			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("printerId", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		}
		return false;
	}

	public boolean validatePrinterIdpgm(String printerId, String pgmId) {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("PRINTER_ID", printerId);
			formDTO.set("PGM_ID", pgmId);
			formDTO.set("USER_ID", userId);
			formDTO.set("ACTION", getAction());
			formDTO = validatePrinterId(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("printerId", formDTO.get(ContentManager.ERROR));
				return false;
			}

			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("printerId", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		}
		return false;
	}

	public boolean validatePgmId(String pgmId) {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("MPGM_ID", pgmId);
			formDTO.set("ACTION", getAction());
			formDTO = validatePgmId(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("pgmId", formDTO.get(ContentManager.ERROR));
				return false;
			}

			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("pgmId", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		}
		return false;
	}

	// public boolean validateProgPrinterId() {
	// try {
	// DTObject formDTO = new DTObject();
	// formDTO.set("PRINTER_ID", progPrinterId);
	// formDTO.set("ACTION", getAction());
	// formDTO = validatePrinterId(formDTO);
	// if (formDTO.get(ContentManager.ERROR) != null) {
	// getErrorMap().setError("progPrinterId",
	// formDTO.get(ContentManager.ERROR));
	// return false;
	// }
	//
	// return true;
	// } catch (Exception e) {
	// e.printStackTrace();
	// getErrorMap().setError("progPrinterId",
	// BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
	// }
	// return false;
	// }

	public DTObject validateUserId(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		AccessValidator validation = new AccessValidator();
		String userId = inputDTO.get("USER_ID");
		try {
			if (validation.isEmpty(userId)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH, ContentManager.FETCH_ALL);
			resultDTO = validation.validateInternalUser(inputDTO);
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_RECORD_NOT_EXISTS);

			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public DTObject validateEffectiveDate(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		AccessValidator validation = new AccessValidator();
		String efftDate = inputDTO.get("EFFT_DATE");
		String action = inputDTO.get(ContentManager.USER_ACTION);
		TBAActionType AT;
		try {
			if (validation.isEmpty(efftDate)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_FIELD_BLANK);
				return resultDTO;
			}
			if (action.equals(ADD))
				AT = TBAActionType.ADD;
			else
				AT = TBAActionType.MODIFY;
			if (!validation.isEffectiveDateValid(efftDate, AT)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_EFFECTIVE_DATE);
				return resultDTO;
			}
			HashMap<Integer, DBEntry> whereClauseFieldsMap = new HashMap<Integer, DBEntry>();
			whereClauseFieldsMap.put(1, new DBEntry("ENTITY_CODE", context.getEntityCode(), BindParameterType.VARCHAR));
			whereClauseFieldsMap.put(2, new DBEntry("USER_ID", inputDTO.get("USER_ID"), BindParameterType.VARCHAR));
			whereClauseFieldsMap.put(3, new DBEntry("EFFT_DATE", BackOfficeFormatUtils.getDate(inputDTO.get("EFFT_DATE"), context.getDateFormat()), BindParameterType.DATE));
			boolean isRecordAvailable = validation.isRecordAvailable("USERPRINTERCFGHIST", whereClauseFieldsMap);
			if (isRecordAvailable) {
				if (action.equals(ADD)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_RECORD_EXISTS);
					return resultDTO;
				}
			} else {
				if (action.equals(MODIFY)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_RECORD_NOT_EXISTS);
					return resultDTO;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public DTObject validatePrintTypeId(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		AccessValidator validation = new AccessValidator();
		String printTypeId = inputDTO.get("PRNTYPE_ID");
		try {
			if (validation.isEmpty(printTypeId)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
			resultDTO = validation.validatePrintType(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_RECORD_NOT_EXISTS);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public DTObject validatePrinterId(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		AccessValidator validation = new AccessValidator();
		String printerid = inputDTO.get("PRINTER_ID");
		String prntType = inputDTO.get("PRNTYPE_ID");
		String userId = inputDTO.get("USER_ID");
		try {
			if (validation.isEmpty(printerid)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "PRINTER_DESC");
			resultDTO = validation.validatePrinterId(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_RECORD_NOT_EXISTS);
				return resultDTO;
			} else if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
				DBContext dbContext = new DBContext();
				DBUtil util = dbContext.createUtilInstance();
				String sqlQuery = null;
				try {

					sqlQuery = "SELECT COUNT(1) FROM PRINTERREG P,PRINTERREGDTL D WHERE " + "P.ENTITY_CODE=D.ENTITY_CODE AND P.PRINTER_ID=D.PRINTER_ID AND P.ENTITY_CODE=? AND P.ENABLED='1' " + "AND P.PRINTER_ID=? AND D.PRNTYPE_ID=? AND "
							+ "(BRN_CODE = (SELECT BRANCH_CODE FROM USERS WHERE PARTITION_NO=? AND " + "ENTITY_CODE=? AND USER_ID=?) OR " + "BRNLIST_CODE = (SELECT BRNLIST_CODE FROM ENTITYBRNLISTDTL WHERE ENTITY_CODE=?" + " AND BRANCH_CODE = (SELECT BRANCH_CODE FROM USERS WHERE PARTITION_NO='? "
							+ "AND ENTITY_CODE='? AND USER_ID=?)))";
					util.reset();
					util.setSql(sqlQuery);
					util.setString(1, context.getEntityCode());
					util.setString(2, printerid);
					util.setString(3, prntType);
					util.setString(4, context.getPartitionNo());
					util.setString(5, context.getEntityCode());
					util.setString(6, userId);
					util.setString(7, context.getPartitionNo());
					util.setString(8, context.getEntityCode());
					util.setString(9, userId);
					ResultSet rset = util.executeQuery();
					if (!rset.next()) {
						resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_RECORD_NOT_EXISTS);
					}
				} catch (Exception e) {
					e.printStackTrace();
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
				} finally {
					dbContext.close();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public DTObject validatePrinterIdpgm(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		AccessValidator validation = new AccessValidator();
		String printerid = inputDTO.get("PRINTER_ID");
		String pgmId = inputDTO.get("PGM_ID");
		String userId = inputDTO.get("USER_ID");
		try {
			if (validation.isEmpty(printerid)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "PRINTER_DESC");
			resultDTO = validation.validatePrinterId(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_RECORD_NOT_EXISTS);
				return resultDTO;
			} else if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
				DBContext dbContext = new DBContext();
				DBUtil util = dbContext.createUtilInstance();
				String sqlQuery = null;
				try {

					sqlQuery = "SELECT COUNT(1) FROM PRINTERREG P,PRINTERREGDTL D WHERE " + "P.ENTITY_CODE=D.ENTITY_CODE AND P.PRINTER_ID=D.PRINTER_ID AND P.ENTITY_CODE=? AND P.ENABLED='1' " + "AND P.PRINTER_ID=? AND D.PRNTYPE_ID=(SELECT PRINT_TYPE FROM MPGMCONFIG WHERE MPGM_ID=?) AND "
							+ "(BRN_CODE = (SELECT BRANCH_CODE FROM USERS WHERE PARTITION_NO=? AND " + "ENTITY_CODE=? AND USER_ID=?) OR " + "BRNLIST_CODE = (SELECT BRNLIST_CODE FROM ENTITYBRNLISTDTL WHERE ENTITY_CODE=?" + " AND BRANCH_CODE = (SELECT BRANCH_CODE FROM USERS WHERE PARTITION_NO='? "
							+ "AND ENTITY_CODE='? AND USER_ID=?)))";
					util.reset();
					util.setSql(sqlQuery);
					util.setString(1, context.getEntityCode());
					util.setString(2, printerid);
					util.setString(3, pgmId);
					util.setString(4, context.getPartitionNo());
					util.setString(5, context.getEntityCode());
					util.setString(6, userId);
					util.setString(7, context.getPartitionNo());
					util.setString(8, context.getEntityCode());
					util.setString(9, userId);
					ResultSet rset = util.executeQuery();
					if (!rset.next()) {
						resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_RECORD_NOT_EXISTS);
					}

				} catch (Exception e) {
					e.printStackTrace();
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
				} finally {
					dbContext.close();
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public DTObject validatePgmId(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		AccessValidator validation = new AccessValidator();
		String printerid = inputDTO.get("MPGM_ID");
		try {
			if (validation.isEmpty(printerid)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH, ContentManager.FETCH_ALL);
			resultDTO = validation.validateOption(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_RECORD_NOT_EXISTS);
				return resultDTO;
			}

		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}
}
