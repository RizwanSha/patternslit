package patterns.config.web.forms.access;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.AccessValidator;
import patterns.config.validations.CommonValidator;
import patterns.config.web.forms.GenericFormBean;

public class ealertstatusbean extends GenericFormBean {

	private static final long serialVersionUID = 1L;

	private String processorCode;
	private boolean enabled;
	private String cronExpression;
	private String recordPerRequest;
	private String remarks;
	private String tempJobNature;
	private String tempJobCategory;

	public String getTempJobCategory() {
		return tempJobCategory;
	}

	public String getTempJobNature() {
		return tempJobNature;
	}

	public void setTempJobCategory(String tempJobCategory) {
		this.tempJobCategory = tempJobCategory;
	}

	public void setTempJobNature(String tempJobNature) {
		this.tempJobNature = tempJobNature;
	}

	public String getProcessorCode() {
		return processorCode;
	}

	public void setProcessorCode(String processorCode) {
		this.processorCode = processorCode;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getCronExpression() {
		return cronExpression;
	}

	public void setCronExpression(String cronExpression) {
		this.cronExpression = cronExpression;
	}

	public String getRecordPerRequest() {
		return recordPerRequest;
	}

	public void setRecordPerRequest(String recordPerRequest) {
		this.recordPerRequest = recordPerRequest;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public void reset() {
		processorCode = RegularConstants.EMPTY_STRING;
		enabled = false;
		cronExpression = RegularConstants.EMPTY_STRING;
		recordPerRequest = RegularConstants.EMPTY_STRING;
		remarks = RegularConstants.EMPTY_STRING;
		setProcessBO("patterns.config.framework.bo.access.ealertstatusBO");
	}

	public void validate() {
		validateProcessorCode();
		if (enabled) {
			validateCronExpression();
			validateRecordPerRequest();
			validateRemarks();
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.set("CODE", processorCode);
				formDTO.set("STATUS", decodeBooleanToString(enabled));
				if (enabled) {
					formDTO.set("CRON_EXPRESSION", cronExpression);
					formDTO.set("RECORD_PER_REQUEST", recordPerRequest);
				} else {
					formDTO.set("CRON_EXPRESSION", RegularConstants.NULL);
					formDTO.set("RECORD_PER_REQUEST", RegularConstants.NULL);
				}
				formDTO.set("REMARKS", remarks);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("processorCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
	}

	public boolean validateProcessorCode() {
		AccessValidator validation = new AccessValidator();
		try {
			if (validation.isEmpty(processorCode)) {
				getErrorMap().setError("processorCode", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			DTObject formDTO = getFormDTO();
			formDTO.set(ContentManager.FETCH, ContentManager.FETCH_ALL);
			formDTO.set("JOB_CODE", processorCode);
			formDTO.set(ContentManager.TABLE, "ALERTPROCSTATUS");
			formDTO = validation.validateAlertMaster(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("processorCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			if (getAction().equals(MODIFY)) {
				if (formDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
					getErrorMap().setError("processorCode", BackOfficeErrorCodes.RECORD_NOT_EXISTS);
					return false;
				}
				return true;
			} else {
				getErrorMap().setError("processorCode", BackOfficeErrorCodes.INVALID_ACTION);
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("processorCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);

		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateRecordPerRequest() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(recordPerRequest)) {
				return true;
			}
			if (!validation.isValidNumber(recordPerRequest)) {
				getErrorMap().setError("recordPerRequest", BackOfficeErrorCodes.INVALID_NUMBER);
				return false;
			}
			if (validation.isZero(recordPerRequest)) {
				getErrorMap().setError("recordPerRequest", BackOfficeErrorCodes.ZERO_CHECK);
				return false;
			}
			if (!validation.isValidPositiveNumber(recordPerRequest)) {
				getErrorMap().setError("recordPerRequest", BackOfficeErrorCodes.POSITIVE_CHECK);
				return false;
			}

			if (!validation.isValidLength(recordPerRequest, 1, 8)) {
				getErrorMap().setError("recordPerRequest", BackOfficeErrorCodes.MAX_LENGTH);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("recordPerRequest", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateCronExpression() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(cronExpression)) {
				getErrorMap().setError("cronExpression", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidCronExp(cronExpression)) {
				getErrorMap().setError("cronExpression", BackOfficeErrorCodes.INVALID_CRON);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("cronExpression", BackOfficeErrorCodes.UNSPECIFIED_ERROR);

		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateRemarks() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(remarks)) {
				getErrorMap().setError("remarks", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidRemarks(remarks)) {
				getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}
}