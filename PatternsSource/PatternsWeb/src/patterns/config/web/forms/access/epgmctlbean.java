package patterns.config.web.forms.access;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.AccessValidator;
import patterns.config.web.forms.GenericFormBean;

public class epgmctlbean extends GenericFormBean {

	private static final long serialVersionUID = -6219657363150466160L;

	private String optionID;
	private boolean authReqd;
	private boolean doubleAuthReqd;

	public boolean isDoubleAuthReqd() {
		return doubleAuthReqd;
	}

	public void setDoubleAuthReqd(boolean doubleAuthReqd) {
		this.doubleAuthReqd = doubleAuthReqd;
	}

	public boolean isAuthReqd() {
		return authReqd;
	}

	public void setAuthReqd(boolean authReqd) {
		this.authReqd = authReqd;
	}

	public String getOptionID() {
		return optionID;
	}

	public void setOptionID(String optionID) {
		this.optionID = optionID;
	}

	public void reset() {
		optionID = RegularConstants.EMPTY_STRING;
		setProcessBO("patterns.config.framework.bo.access.epgmctlBO");
	}

	public void validate() {
		validateOptionID();
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("MPGM_ID", optionID);
				formDTO.set("MPGM_AUTH_REQD", decodeBooleanToString(authReqd));
				formDTO.set("MPGM_DBL_AUTH_REQD", decodeBooleanToString(doubleAuthReqd));
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("optionID", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
	}

	public DTObject validateOptionID(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DTObject formDTO = getFormDTO();
		AccessValidator validation = new AccessValidator();
		try {
			String optionID = inputDTO.get("PGM_ID");
			if (validation.isEmpty(optionID)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			formDTO.set("MPGM_ID", optionID);
			formDTO.set(ContentManager.FETCH, ContentManager.FETCH_ALL);
			formDTO = validation.validateOption(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, formDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (!formDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_OPTION);
				return resultDTO;
			}
			resultDTO.set("MPGM_DESCN", formDTO.get("MPGM_DESCN"));
			boolean tbaRequired = decodeStringToBoolean(formDTO.get("TBA_REQ"));
			if (!tbaRequired) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.TBA_NOT_ALLOWED);
				return resultDTO;
			}
			resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
		} catch (Exception e) {

			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateOptionID() {
		AccessValidator validation = new AccessValidator();
		try {
			if (validation.isEmpty(optionID)) {
				getErrorMap().setError("optionID", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			DTObject formDTO = getFormDTO();
			formDTO.set("MPGM_ID", optionID);
			formDTO.set(ContentManager.FETCH, ContentManager.FETCH_ALL);
			formDTO = validation.validateOption(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("optionID", formDTO.get(ContentManager.ERROR));
				return false;
			}
			if (formDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				getErrorMap().setError("optionID", BackOfficeErrorCodes.INVALID_OPTION);
				return false;
			}
			boolean tbaRequired = decodeStringToBoolean(formDTO.get("TBA_REQ"));
			if (!tbaRequired) {
				getErrorMap().setError("optionID", BackOfficeErrorCodes.TBA_NOT_ALLOWED);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("optionID", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateDoubleAuthReq() {
		try {
			if (!authReqd && doubleAuthReqd) {
				getErrorMap().setError("doubleAuthReqd", BackOfficeErrorCodes.CHECK_FIRST_AUTH_FOR_DBL_AUTH);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("doubleAuthReqd", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
		}
		return false;
	}
}