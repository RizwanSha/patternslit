package patterns.config.web.forms.access;

import java.util.HashMap;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.BindParameterType;
import patterns.config.framework.database.DBEntry;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.process.TBAActionType;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.validations.AccessValidator;
import patterns.config.validations.CommonValidator;
import patterns.config.web.forms.GenericFormBean;

public class ifoldermaintenancebean extends GenericFormBean {

	private static final long serialVersionUID = 1L;

	private String effectiveDate;
	private String folderCBSFileUpload;
	private String folderReportsGeneration;
	private String folderPortalUpload;
	private String folderTempUpload;
	private String remarks;

	public String getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public String getFolderCBSFileUpload() {
		return folderCBSFileUpload;
	}

	public void setFolderCBSFileUpload(String folderCBSFileUpload) {
		this.folderCBSFileUpload = folderCBSFileUpload;
	}

	public String getFolderReportsGeneration() {
		return folderReportsGeneration;
	}

	public void setFolderReportsGeneration(String folderReportsGeneration) {
		this.folderReportsGeneration = folderReportsGeneration;
	}

	public String getFolderPortalUpload() {
		return folderPortalUpload;
	}

	public void setFolderPortalUpload(String folderPortalUpload) {
		this.folderPortalUpload = folderPortalUpload;
	}

	public String getFolderTempUpload() {
		return folderTempUpload;
	}

	public void setFolderTempUpload(String folderTempUpload) {
		this.folderTempUpload = folderTempUpload;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public void reset() {
		effectiveDate = RegularConstants.EMPTY_STRING;
		folderReportsGeneration = RegularConstants.EMPTY_STRING;
		folderPortalUpload = RegularConstants.EMPTY_STRING;
		folderCBSFileUpload = RegularConstants.EMPTY_STRING;
		folderTempUpload = RegularConstants.EMPTY_STRING;
		remarks = RegularConstants.EMPTY_STRING;
		setProcessBO("patterns.config.framework.bo.access.ifoldermaintenanceBO");
	}

	public void validate() {
		if (validateEffectiveDate()) {
			validateFolderReportsGeneration();
			validateFolderPortalUpload();
			validateFolderCBSFileUpload();
			validateFolderTempUpload();
			validateRemarks();
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.setObject("EFFT_DATE", BackOfficeFormatUtils.getDate(effectiveDate, context.getDateFormat()));
				formDTO.set("REPORT_GENERATION_PATH", folderReportsGeneration);
				formDTO.set("PORTAL_FILE_UPLOAD_PATH", folderPortalUpload);
				formDTO.set("TEMP_FILE_UPLOAD_PATH", folderTempUpload);
				formDTO.set("CBS_FILE_TRANSFER_PATH", folderCBSFileUpload);
				formDTO.set("REMARKS", remarks);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("effectiveDate", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
	}

	public boolean validateEffectiveDate() {
		CommonValidator validation = new CommonValidator();
		try {
			TBAActionType AT;
			if (getAction().equals(ADD))
				AT = TBAActionType.ADD;
			else
				AT = TBAActionType.MODIFY;
			if (validation.isEffectiveDateValid(effectiveDate, AT)) {
				HashMap<Integer, DBEntry> whereClauseFieldsMap = new HashMap<Integer, DBEntry>();
				whereClauseFieldsMap.put(1, new DBEntry("ENTITY_CODE", context.getEntityCode(), BindParameterType.VARCHAR));
				whereClauseFieldsMap.put(2, new DBEntry("EFFT_DATE", BackOfficeFormatUtils.getDate(effectiveDate, context.getDateFormat()), BindParameterType.DATE));

				boolean isRecordAvailable = validation.isRecordAvailable("SYSTEMFOLDERS", whereClauseFieldsMap);
				if (isRecordAvailable) {
					if (getAction().equals(ADD)) {
						getErrorMap().setError("effectiveDate", BackOfficeErrorCodes.RECORD_EXISTS);
						return false;
					}
					return true;
				} else {
					if (getAction().equals(MODIFY)) {
						getErrorMap().setError("effectiveDate", BackOfficeErrorCodes.RECORD_NOT_EXISTS);
						return false;
					}
					return true;
				}
			} else {
				getErrorMap().setError("effectiveDate", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("effectiveDate", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateFolderReportsGeneration() {
		AccessValidator validation = new AccessValidator();
		try {
			if (validation.isEmpty(folderReportsGeneration)) {
				getErrorMap().setError("folderReportsGeneration", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidPath(folderReportsGeneration)) {
				getErrorMap().setError("folderReportsGeneration", BackOfficeErrorCodes.INVALID_FORMAT);
				return false;
			}
			if (!validation.isFolder(folderReportsGeneration)) {
				getErrorMap().setError("folderReportsGeneration", BackOfficeErrorCodes.INVALID_FOLDER);
				return false;
			}
			if (!validation.isFolderAvailable(folderReportsGeneration)) {
				getErrorMap().setError("folderReportsGeneration", BackOfficeErrorCodes.FOLDER_NOT_AVAILABLE);
				return false;
			}
			if (!validation.isFolderWriteEnabled(folderReportsGeneration)) {
				getErrorMap().setError("folderReportsGeneration", BackOfficeErrorCodes.FOLDER_NOT_WRITEABLE);
				return false;
			}
			if (!validation.isFolderReadEnabled(folderReportsGeneration)) {
				getErrorMap().setError("folderReportsGeneration", BackOfficeErrorCodes.FOLDER_NOT_READABLE);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("folderReportsGeneration", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateFolderPortalUpload() {
		CommonValidator validation = new CommonValidator();
		try {

			if (validation.isEmpty(folderPortalUpload)) {
				getErrorMap().setError("folderPortalUpload", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidPath(folderPortalUpload)) {
				getErrorMap().setError("folderPortalUpload", BackOfficeErrorCodes.INVALID_FORMAT);
				return false;
			}
			if (!validation.isFolder(folderPortalUpload)) {
				getErrorMap().setError("folderPortalUpload", BackOfficeErrorCodes.INVALID_FOLDER);
				return false;
			}
			if (!validation.isFolderAvailable(folderPortalUpload)) {
				getErrorMap().setError("folderPortalUpload", BackOfficeErrorCodes.FOLDER_NOT_AVAILABLE);
				return false;
			}
			if (!validation.isFolderWriteEnabled(folderPortalUpload)) {
				getErrorMap().setError("folderPortalUpload", BackOfficeErrorCodes.FOLDER_NOT_WRITEABLE);
				return false;
			}
			if (!validation.isFolderReadEnabled(folderPortalUpload)) {
				getErrorMap().setError("folderPortalUpload", BackOfficeErrorCodes.FOLDER_NOT_READABLE);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("folderPortalUpload", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateFolderTempUpload() {
		CommonValidator validation = new CommonValidator();
		try {

			if (validation.isEmpty(folderTempUpload)) {
				getErrorMap().setError("folderTempUpload", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidPath(folderTempUpload)) {
				getErrorMap().setError("folderTempUpload", BackOfficeErrorCodes.INVALID_FORMAT);
				return false;
			}
			if (!validation.isFolder(folderTempUpload)) {
				getErrorMap().setError("folderTempUpload", BackOfficeErrorCodes.INVALID_FOLDER);
				return false;
			}
			if (!validation.isFolderAvailable(folderTempUpload)) {
				getErrorMap().setError("folderTempUpload", BackOfficeErrorCodes.FOLDER_NOT_AVAILABLE);
				return false;
			}
			if (!validation.isFolderWriteEnabled(folderTempUpload)) {
				getErrorMap().setError("folderTempUpload", BackOfficeErrorCodes.FOLDER_NOT_WRITEABLE);
				return false;
			}
			if (!validation.isFolderReadEnabled(folderTempUpload)) {
				getErrorMap().setError("folderTempUpload", BackOfficeErrorCodes.FOLDER_NOT_READABLE);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("folderTempUpload", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateFolderCBSFileUpload() {
		CommonValidator validation = new CommonValidator();
		try {

			if (validation.isEmpty(folderCBSFileUpload)) {
				getErrorMap().setError("folderCBSFileUpload", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidPath(folderCBSFileUpload)) {
				getErrorMap().setError("folderCBSFileUpload", BackOfficeErrorCodes.INVALID_FORMAT);
				return false;
			}
			if (!validation.isFolder(folderCBSFileUpload)) {
				getErrorMap().setError("folderCBSFileUpload", BackOfficeErrorCodes.INVALID_FOLDER);
				return false;
			}
			if (!validation.isFolderAvailable(folderCBSFileUpload)) {
				getErrorMap().setError("folderCBSFileUpload", BackOfficeErrorCodes.FOLDER_NOT_AVAILABLE);
				return false;
			}
			if (!validation.isFolderWriteEnabled(folderCBSFileUpload)) {
				getErrorMap().setError("folderCBSFileUpload", BackOfficeErrorCodes.FOLDER_NOT_WRITEABLE);
				return false;
			}
			if (!validation.isFolderReadEnabled(folderCBSFileUpload)) {
				getErrorMap().setError("folderCBSFileUpload", BackOfficeErrorCodes.FOLDER_NOT_READABLE);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("folderCBSFileUpload", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateRemarks() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(remarks)) {
				getErrorMap().setError("remarks", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidRemarks(remarks)) {
				getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}
}