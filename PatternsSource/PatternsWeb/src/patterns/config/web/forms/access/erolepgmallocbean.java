package patterns.config.web.forms.access;

import java.sql.ResultSet;
import java.util.HashMap;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.BindParameterType;
import patterns.config.framework.database.CM_LOVREC;
import patterns.config.framework.database.DBEntry;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.process.TBAActionType;
import patterns.config.framework.service.DTDObject;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.framework.web.DHTMLXGridUtility;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.AccessValidator;
import patterns.config.validations.CommonValidator;
import patterns.config.web.forms.GenericFormBean;

public class erolepgmallocbean extends GenericFormBean {

	private static final long serialVersionUID = -6219657363150466160L;

	private String roleType;
	private String roleCode;
	private String moduleCode;
	private String effectiveDate;
	private String remarks;
	private String xmlPgmList;

	public String getModuleCode() {
		return moduleCode;
	}

	public void setModuleCode(String moduleCode) {
		this.moduleCode = moduleCode;
	}

	public String getXmlPgmList() {
		return xmlPgmList;
	}

	public void setXmlPgmList(String xmlPgmList) {
		this.xmlPgmList = xmlPgmList;
	}

	public String getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getRoleCode() {
		return roleCode;
	}

	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}

	public void reset() {
		roleCode = RegularConstants.EMPTY_STRING;
		moduleCode = RegularConstants.EMPTY_STRING;
		effectiveDate = RegularConstants.EMPTY_STRING;
		remarks = RegularConstants.EMPTY_STRING;
		setProcessBO("patterns.config.framework.bo.access.erolepgmallocBO");
	}

	public void validate() {
		if (validateRoleCode()) {
			if (validateModuleCode()) {
				if (validateEffectiveDate()) {
					if (validatePK()) {
						validateRemarks();
						validateGrid();
					}
				}
			}
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.set("ROLE_CODE", roleCode);
				formDTO.set("MODULE_ID", moduleCode);
				formDTO.setObject("EFFT_DATE", BackOfficeFormatUtils.getDate(effectiveDate, context.getDateFormat()));
				formDTO.set("REMARKS", remarks);
				DTDObject dtdObject = new DTDObject();
				dtdObject.addColumn(0, "SL");
				dtdObject.addColumn(1, "PGM_ID");
				dtdObject.addColumn(2, "PGM_DESCN");
				dtdObject.addColumn(3, "ACCESS_ALLOWED");
				dtdObject.addColumn(4, "ADD_ALLOWED");
				dtdObject.addColumn(5, "MODIFY_ALLOWED");
				dtdObject.addColumn(6, "VIEW_ALLOWED");
				dtdObject.addColumn(7, "AUTHORIZE_ALLOWED");
				dtdObject.addColumn(8, "REJECTION_ALLOWED");
				dtdObject.addColumn(9, "PROCESS_ALLOWED");
				dtdObject.setXML(xmlPgmList);
				formDTO.setDTDObject("ROLEPGMALLOCHISTDTL", dtdObject);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("roleCode", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		}
	}

	public DTObject roleCodeValidate(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		AccessValidator validation = new AccessValidator();
		String role = inputDTO.get("ROLE_CODE");
		String action = inputDTO.get(ContentManager.USER_ACTION);
		try {
			if (validation.isEmpty(role)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH, ContentManager.FETCH_ALL);
			inputDTO.set(ContentManager.CODE, role);
			resultDTO = validation.validateInternalRole(inputDTO);
			roleType = resultDTO.get("ROLE_TYPE");
			if (!validation.isEmpty(roleType)) {
				if (context.isInternalAdministrator()) {
					if (!roleType.equals(CM_LOVREC.COMMON_ROLES_1) && !roleType.equals(CM_LOVREC.COMMON_ROLES_2) && !roleType.equals(CM_LOVREC.COMMON_ROLES_3) && !roleType.equals(CM_LOVREC.COMMON_ROLES_4)) {
						resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INSUFFICIENT_ROLE_RIGHTS);
						return resultDTO;
					}
				}
				if (context.isInternalOperations()) {
					if (!roleType.equals(CM_LOVREC.COMMON_ROLES_5) && !roleType.equals(CM_LOVREC.COMMON_ROLES_6)) {
						resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INSUFFICIENT_ROLE_RIGHTS);
						return resultDTO;
					}
				}
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_RECORD_NOT_EXISTS);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public DTObject moduleValidate(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		AccessValidator validation = new AccessValidator();
		String module = inputDTO.get("MODULE_ID");
		String action = inputDTO.get(ContentManager.USER_ACTION);
		try {
			if (validation.isEmpty(module)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH, ContentManager.FETCH_ALL);
			inputDTO.set(ContentManager.CODE, module);
			resultDTO = validation.validateModuleCode(inputDTO);
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_RECORD_NOT_EXISTS);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public DTObject validateEffectiveDate(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		AccessValidator validation = new AccessValidator();
		String efftDate = inputDTO.get("EFFT_DATE");
		String action = inputDTO.get(ContentManager.USER_ACTION);
		TBAActionType AT;
		try {

			if (validation.isEmpty(efftDate)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_FIELD_BLANK);
				return resultDTO;
			}
			
			if (action.equals(ADD))
				AT = TBAActionType.ADD;
			else
				AT = TBAActionType.MODIFY;
			if (!validation.isEffectiveDateValid(efftDate, AT)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_EFFECTIVE_DATE);
				return resultDTO;
			}
			HashMap<Integer, DBEntry> whereClauseFieldsMap = new HashMap<Integer, DBEntry>();
			whereClauseFieldsMap.put(1, new DBEntry("ENTITY_CODE", context.getEntityCode(), BindParameterType.VARCHAR));
			whereClauseFieldsMap.put(2, new DBEntry("ROLE_CODE", inputDTO.get("ROLE_CODE"), BindParameterType.VARCHAR));
			whereClauseFieldsMap.put(3, new DBEntry("MODULE_ID", inputDTO.get("MODULE_ID"), BindParameterType.VARCHAR));
			whereClauseFieldsMap.put(4, new DBEntry("EFFT_DATE", BackOfficeFormatUtils.getDate(inputDTO.get("EFFT_DATE"), context.getDateFormat()), BindParameterType.DATE));
			boolean isRecordAvailable = validation.isRecordAvailable("ROLEPGMALLOCHIST", whereClauseFieldsMap);
			if (isRecordAvailable) {
				if (action.equals(ADD)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_RECORD_EXISTS);
					return resultDTO;
				}
			} else {
				if (action.equals(MODIFY)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_RECORD_NOT_EXISTS);
					return resultDTO;
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateRoleCode() {
		try {
			DTObject formDTO = getFormDTO();
			formDTO.set("ROLE_CODE", roleCode);
			formDTO.set("ACTION", getAction());
			formDTO.set(ContentManager.FETCH, ContentManager.FETCH_ALL);
			formDTO = roleCodeValidate(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("roleCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("roleCode", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		}
		return false;
	}

	public boolean validateModuleCode() {
		try {
			DTObject formDTO = getFormDTO();
			formDTO.set("MODULE_ID", moduleCode);
			formDTO.set("ACTION", getAction());
			formDTO.set(ContentManager.FETCH, ContentManager.FETCH_ALL);
			formDTO = moduleValidate(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("moduleCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("moduleCode", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		}
		return false;
	}

	public boolean validateEffectiveDate() {
		try {
			DTObject formDTO = getFormDTO();
			formDTO.set("ROLE_CODE", roleCode);
			formDTO.set("MODULE_ID", moduleCode);
			formDTO.set("EFFT_DATE", effectiveDate);
			formDTO.set("ACTION", getAction());
			formDTO.set(ContentManager.FETCH, ContentManager.FETCH_ALL);
			formDTO = validateEffectiveDate(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("effectiveDate", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("effectiveDate", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		}
		return false;
	}

	public boolean validatePK() {
		CommonValidator validation = new CommonValidator();
		try {
			HashMap<Integer, DBEntry> whereClauseFieldsMap = new HashMap<Integer, DBEntry>();
			whereClauseFieldsMap.put(1, new DBEntry("ENTITY_CODE", context.getEntityCode(), BindParameterType.VARCHAR));
			whereClauseFieldsMap.put(2, new DBEntry("ROLE_CODE", roleCode, BindParameterType.VARCHAR));
			whereClauseFieldsMap.put(3, new DBEntry("MODULE_ID", moduleCode, BindParameterType.VARCHAR));
			whereClauseFieldsMap.put(4, new DBEntry("EFFT_DATE", BackOfficeFormatUtils.getDate(effectiveDate, context.getDateFormat()), BindParameterType.DATE));
			boolean isRecordAvailable = validation.isRecordAvailable("ROLEPGMALLOCHIST", whereClauseFieldsMap);
			if (isRecordAvailable) {
				if (getAction().equals(ADD)) {
					getErrorMap().setError("roleCode", BackOfficeErrorCodes.HMS_RECORD_EXISTS);
					return false;
				}
				return true;
			} else {
				if (getAction().equals(MODIFY)) {
					getErrorMap().setError("roleCode", BackOfficeErrorCodes.HMS_RECORD_NOT_EXISTS);
					return false;
				}
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("roleCode", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateGrid() {
		AccessValidator validation = new AccessValidator();
		try {
			DTObject data = new DTObject();
			DTDObject dtdObject = new DTDObject();
			dtdObject.addColumn(0, "SL");
			dtdObject.addColumn(1, "PGM_ID");
			dtdObject.addColumn(2, "PGM_DESCN");
			dtdObject.addColumn(3, "ACCESS_ALLOWED");
			dtdObject.addColumn(4, "ADD_ALLOWED");
			dtdObject.addColumn(5, "MODIFY_ALLOWED");
			dtdObject.addColumn(6, "VIEW_ALLOWED");
			dtdObject.addColumn(7, "AUTHORIZE_ALLOWED");
			dtdObject.addColumn(8, "REJECTION_ALLOWED");
			dtdObject.addColumn(9, "PROCESS_ALLOWED");
			dtdObject.setXML(xmlPgmList);
			data.setDTDObject("ROLEPGMALLOCHISTDTL", dtdObject);
			if (dtdObject.getRowCount() == 0) {
				getErrorMap().setError("roleCode", BackOfficeErrorCodes.ADD_ATLEAST_ONE_ROW);
				return false;
			}
			if (!validation.checkDuplicateRows(dtdObject, 1)) {
				getErrorMap().setError("roleCode", BackOfficeErrorCodes.DUPLICATE_DATA);
				return false;
			}
			int flag = 0;
			for (int i = 0; i < dtdObject.getRowCount(); ++i) {
				boolean optionEnabled = decodeStringToBoolean(dtdObject.getValue(i, 3));
				if (optionEnabled) {
					String optionID = dtdObject.getValue(i, 1);
					DTObject formDTO = getFormDTO();
					formDTO.set("MPGM_ID", optionID);
					formDTO.set("ROLE_TYPE", roleType);
					formDTO = validation.isProgramMappedToRoleType(formDTO);
					if (formDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
						getErrorMap().setError("roleCode", BackOfficeErrorCodes.HMS_INVALID_CODE);
						return false;
					}
					if (formDTO.get(ContentManager.ERROR) != null) {
						getErrorMap().setError("roleCode", formDTO.get(ContentManager.ERROR));
						return false;
					}
					flag++;
				}
			}
			if (flag == 0) {
				getErrorMap().setError("roleCode", BackOfficeErrorCodes.CHECK_ATLEAST_ONE);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("roleCode", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public DTObject getRolePrograms(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		String roleCode = inputDTO.get("ROLE_CODE");
		String modulec = inputDTO.get("MODULE_ID");
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		AccessValidator validator = new AccessValidator();
		try {
			inputDTO.set(ContentManager.CODE, roleCode);
			inputDTO.set(ContentManager.FETCH, ContentManager.FETCH_ALL);
			resultDTO = validator.validateInternalRole(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (!resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_CODE);
				return resultDTO;
			}
			String roleType = resultDTO.get("ROLE_TYPE");
			DBContext dbContext = new DBContext();
			DBUtil util = dbContext.createUtilInstance();
			String sqlQuery = null;

			try {
				sqlQuery = "SELECT  @ROWNUM:=@ROWNUM+1 R,P.PGM_ID,M.MPGM_DESCN,'0',M.ADD_ALLOWED,M.MODIFY_ALLOWED,M.VIEW_ALLOWED,M.AUTHORIZE_ALLOWED,M.REJECT_ALLOWED,M.PROCESS_ALLOWED FROM ROLEPGMMAP P, MPGM M, (SELECT @ROWNUM:= 0) ROWNUM WHERE P.ENTITY_CODE = ? AND P.ROLE_TYPE = ? AND M.MPGM_MODULE=? AND P.PGM_ID = M.MPGM_ID";
				util.reset();
				util.setSql(sqlQuery);
				util.setString(1, context.getEntityCode());
				util.setString(2, roleType);
				util.setString(3, modulec);
				ResultSet rset = util.executeQuery();
				DHTMLXGridUtility gridUtility = new DHTMLXGridUtility();
				gridUtility.init();
				while (rset.next()) {
					gridUtility.startRow(rset.getString(2));
					gridUtility.setCell(rset.getString(1));
					gridUtility.setCell(rset.getString(2));
					gridUtility.setCell(rset.getString(3));
					gridUtility.setCell(rset.getString(4));
					gridUtility.setCell("0");
					gridUtility.setCell(rset.getString(5));
					gridUtility.setCell("0");
					gridUtility.setCell(rset.getString(6));
					gridUtility.setCell("0");
					gridUtility.setCell(rset.getString(7));
					gridUtility.setCell("0");
					gridUtility.setCell(rset.getString(8));
					gridUtility.setCell("0");
					gridUtility.setCell(rset.getString(9));
					gridUtility.setCell("0");
					gridUtility.setCell(rset.getString(10));
					gridUtility.endRow();
				}
				gridUtility.finish();
				String resultXML = gridUtility.getXML();
				resultDTO.set(ContentManager.RESULT_XML, resultXML);
				resultDTO.set("ROLE_TYPE", roleType);
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
			} catch (Exception e) {
				e.printStackTrace();
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
			} finally {
				dbContext.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return resultDTO;
	}

	public boolean validateRemarks() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (remarks != null && remarks.length() > 0) {
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.HMS_INVALID_REMARKS);
					return false;
				}
			}
			if (getAction().equals(MODIFY)) {
				if (commonValidator.isEmpty(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.HMS_FIELD_BLANK);
					return false;
				}
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.HMS_INVALID_REMARKS);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}
}