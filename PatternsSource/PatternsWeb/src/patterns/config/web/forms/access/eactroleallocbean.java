package patterns.config.web.forms.access;

import java.util.Date;
import java.util.HashMap;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.BindParameterType;
import patterns.config.framework.database.DBEntry;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.AccessValidator;
import patterns.config.validations.CommonValidator;
import patterns.config.web.forms.GenericFormBean;

public class eactroleallocbean extends GenericFormBean {

	private static final long serialVersionUID = 1L;
	private String userID;
	private String roleCode;
	private boolean enabled;
	private String fromDate;
	private String toDate;
	private String remarks;

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public String getRoleCode() {
		return roleCode;
	}

	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public void reset() {
		userID = RegularConstants.EMPTY_STRING;
		roleCode = RegularConstants.EMPTY_STRING;
		fromDate = RegularConstants.EMPTY_STRING;
		toDate = RegularConstants.EMPTY_STRING;
		remarks = RegularConstants.EMPTY_STRING;
		setProcessBO("patterns.config.framework.bo.access.eactroleallocBO");
	}

	public void validate() {
		if (validateUserID()) {
			if (validateRoleCode()) {
				if (validatePK()) {
					if (enabled) {
						validateFromDate();
						validateToDate();
					}
					validateRemarks();
				}
			}
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.set("USER_ID", userID);
				formDTO.set("ROLE_CODE", roleCode);
				formDTO.set("ENABLED", decodeBooleanToString(enabled));
				if (enabled) {
					formDTO.setObject("FROM_DATE", BackOfficeFormatUtils.getDate(fromDate, context.getDateFormat()));
					formDTO.setObject("UPTO_DATE", BackOfficeFormatUtils.getDate(toDate, context.getDateFormat()));
				} else {
					formDTO.setObject("FROM_DATE", RegularConstants.NULL);
					formDTO.setObject("UPTO_DATE", RegularConstants.NULL);
				}
				formDTO.set("REMARKS", remarks);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("userID", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
	}
	
	public DTObject validateUserId(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		AccessValidator validation = new AccessValidator();
		String userId = inputDTO.get("USER_ID");
		try {

			if (validation.isEmpty(userId)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_FIELD_BLANK);
				return resultDTO;
			}
			if (userId.equals(context.getUserID())) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.CANNOT_ALLOCATE_TO_SELF);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH, ContentManager.FETCH_ALL);
			resultDTO = validation.validateInternalUser(inputDTO);
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_RECORD_NOT_EXISTS);
				return resultDTO;
			}
			if (userId.equals(context.getUserID())) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.CANNOT_ALLOCATE_TO_SELF);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateUserID() {
		AccessValidator validation = new AccessValidator();
		try {
			if (validation.isEmpty(userID)) {
				getErrorMap().setError("userID", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			DTObject formDTO = getFormDTO();
			formDTO.set(ContentManager.USER_ID, userID);
			formDTO.set(ContentManager.FETCH, ContentManager.FETCH_ALL);
			formDTO = validation.validateInternalUser(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("userID", formDTO.get(ContentManager.ERROR));
				return false;
			}
			if (formDTO.get(ContentManager.RESULT).equalsIgnoreCase(ContentManager.DATA_UNAVAILABLE)) {
				getErrorMap().setError("userID", BackOfficeErrorCodes.INVALID_CODE);
				return false;
			}
			if (userID.equals(context.getUserID())) {
				getErrorMap().setError("userID", BackOfficeErrorCodes.CANNOT_ALLOCATE_TO_SELF);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("userID", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public DTObject validateRoleCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		AccessValidator validation = new AccessValidator();
		String roleCode = inputDTO.get("CODE");
		try {
			if (validation.isEmpty(roleCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH, ContentManager.FETCH_ALL);
			resultDTO = validation.validateInternalRole(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_RECORD_NOT_EXISTS);
				return resultDTO;
			}

		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}
	public boolean validateRoleCode() {
		AccessValidator validation = new AccessValidator();
		try {
			if (validation.isEmpty(roleCode)) {
				getErrorMap().setError("roleCode", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			DTObject formDTO = getFormDTO();
			formDTO.set(ContentManager.FETCH, ContentManager.FETCH_ALL);
			formDTO.set(ContentManager.CODE, roleCode);
			formDTO.set(ContentManager.ACTION, getAction());
			formDTO = validation.validateInternalRole(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("roleCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			if (formDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				getErrorMap().setError("roleCode", BackOfficeErrorCodes.INVALID_CODE);
				return false;
			}
			
			String userRoleCode = validation.getInternalUserRoleCode(userID);
			if (userRoleCode.equals(roleCode)) {
				getErrorMap().setError("roleCode", BackOfficeErrorCodes.PRIMARY_ROLE_NOT_ALLOWED);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("roleCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateFromDate() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(fromDate)) {
				getErrorMap().setError("fromDate", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			Date date = validation.isValidDate(fromDate);
			if (date == null) {
				getErrorMap().setError("fromDate", BackOfficeErrorCodes.INVALID_DATE);
				return false;
			}
			if (!validation.isDateGreaterThanEqualCBD(fromDate)) {
				getErrorMap().setError("fromDate", BackOfficeErrorCodes.DATE_GECBD);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("fromDate", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateToDate() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(toDate)) {
				getErrorMap().setError("toDate", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			Date date = validation.isValidDate(fromDate);
			if (date == null) {
				getErrorMap().setError("toDate", BackOfficeErrorCodes.INVALID_DATE);
				return false;
			}
			if (!validation.isToDateGEFromDate(toDate, fromDate)) {
				getErrorMap().setError("toDate", BackOfficeErrorCodes.TO_DATE_GE_FROM_DATE);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("toDate", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;

	}

	public boolean validatePK() {
		AccessValidator validation = new AccessValidator();
		HashMap<Integer, DBEntry> whereClauseFieldsMap = new HashMap<Integer, DBEntry>();
		whereClauseFieldsMap.put(1, new DBEntry("ENTITY_CODE", context.getEntityCode(), BindParameterType.VARCHAR));
		whereClauseFieldsMap.put(2, new DBEntry("USER_ID", userID, BindParameterType.VARCHAR));
		whereClauseFieldsMap.put(3, new DBEntry("ROLE_CODE", roleCode, BindParameterType.VARCHAR));
		boolean isRecordAvailable = validation.isRecordAvailable("USERSACTROLEALLOC", whereClauseFieldsMap);
		try {
			if (isRecordAvailable) {
				if (getAction().equals(ADD)) {
					getErrorMap().setError("userID", BackOfficeErrorCodes.RECORD_EXISTS);
					return false;
				}
				return true;
			} else {

				if (getAction().equals(MODIFY)) {
					getErrorMap().setError("userID", BackOfficeErrorCodes.RECORD_NOT_EXISTS);
					return false;
				}
				return true;

			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("userID", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateRemarks() {
		CommonValidator commonValidator = new CommonValidator();
		try {
				if (remarks!=null && remarks.length()>0) {
					if(!commonValidator.isValidRemarks(remarks)){
						getErrorMap().setError("remarks", BackOfficeErrorCodes.HMS_INVALID_REMARKS);
						return false;
					}
				}
				if (getAction().equals(MODIFY)) {
					if (commonValidator.isEmpty(remarks)) {
						getErrorMap().setError("remarks", BackOfficeErrorCodes.HMS_FIELD_BLANK);
						return false;
					}
					if(!commonValidator.isValidRemarks(remarks)){
						getErrorMap().setError("remarks", BackOfficeErrorCodes.HMS_INVALID_REMARKS);
						return false;
					}
				}
				return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}

}
