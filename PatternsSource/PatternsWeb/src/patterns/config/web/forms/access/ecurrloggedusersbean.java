package patterns.config.web.forms.access;

import java.sql.ResultSet;

import patterns.config.delegate.BackOfficeProcessManager;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.process.TBAProcessResult;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTDObject;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.DHTMLXGridUtility;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.AccessValidator;
import patterns.config.web.forms.GenericFormBean;

public class ecurrloggedusersbean extends GenericFormBean {

	public ecurrloggedusersbean() {
		setProcessBO("patterns.config.framework.bo.access.ecurrloggedusersBO");
	}

	private static final long serialVersionUID = -6219657363150466160L;
	private String currentLoggedUserCount;
	private String xmlCurrLoggedUsers;

	public String getXmlCurrLoggedUsers() {
		return xmlCurrLoggedUsers;
	}

	public void setXmlCurrLoggedUsers(String xmlCurrLoggedUsers) {
		this.xmlCurrLoggedUsers = xmlCurrLoggedUsers;
	}

	public String getCurrentLoggedUserCount() {
		return currentLoggedUserCount;
	}

	public void setCurrentLoggedUserCount(String currentLoggedUserCount) {
		this.currentLoggedUserCount = currentLoggedUserCount;
	}

	public void reset() {
		currentLoggedUserCount = RegularConstants.EMPTY_STRING;

	}

	public void validate() {
	}

	public DTObject processLogout(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		try {
			DTObject formDTO = validateGrid(inputDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, formDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			formDTO.set("P_ENTITY_CODE", context.getEntityCode());
			formDTO.set("P_ADMIN_USER_ID", context.getUserID());
			formDTO.set("P_SESSION_ID", webContext.getRequest().getSession().getId());
			String xmlData = inputDTO.get("XML_DATA");
			DTDObject dtdObject = new DTDObject();
			dtdObject.addColumn(0, "SELECT");
			dtdObject.addColumn(1, "USER_ID");
			dtdObject.addColumn(2, "LOGIN_DATETIME");
			dtdObject.addColumn(3, "LOGIN_SESSION_ID");
			dtdObject.addColumn(4, "ADMIN_USER_ID");
			dtdObject.setXML(xmlData);
			formDTO.setDTDObject("USERSFORCEDLOGOUT", dtdObject);
			formDTO.set(RegularConstants.PROCESSBO_CLASS, getProcessBO());
			BackOfficeProcessManager processManager = new BackOfficeProcessManager();
			TBAProcessResult processResult = processManager.delegate(formDTO);
			if (processResult.getProcessStatus().equals(TBAProcessStatus.SUCCESS)) {
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
			} else {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.OPERATION_UNSUCCESSFUL);
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return resultDTO;
	}

	private DTObject validateGrid(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		DTObject formDTO = getFormDTO();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		AccessValidator validation = new AccessValidator();
		try {
			String xmlData = inputDTO.get("XML_DATA");
			if (validation.isEmpty(xmlData)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			DTDObject dtdObject = new DTDObject();
			dtdObject.addColumn(0, "SELECT");
			dtdObject.addColumn(1, "USER_ID");
			dtdObject.addColumn(2, "LOGIN_DATETIME");
			dtdObject.addColumn(3, "LOGIN_SESSION_ID");
			dtdObject.addColumn(4, "ADMIN_USER_ID");
			dtdObject.setXML(xmlData);
			formDTO.setDTDObject("USERSFORCEDLOGOUT", dtdObject);
			dtdObject = formDTO.getDTDObject("USERSFORCEDLOGOUT");
			outer: for (int i = 0; i < dtdObject.getRowCount(); ++i) {
				for (int j = 0; j < dtdObject.getColCount(); ++j) {
					if (j == 0) {
						String value = dtdObject.getValue(i, j);
						if (!value.equals(RegularConstants.COLUMN_ENABLE)) {
							continue outer;
						}
					}
					if (j == 1) {
						String userID = dtdObject.getValue(i, j);
						formDTO.set(ContentManager.USER_ID, userID);
						formDTO.set(ContentManager.FETCH, ContentManager.FETCH_ALL);
						formDTO = validation.validateInternalUser(formDTO);
						if (formDTO.get(ContentManager.ERROR) != null) {
							resultDTO.set(ContentManager.ERROR, formDTO.get(ContentManager.ERROR));
							return resultDTO;
						}
						if (!formDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
							resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_USERID);
							return resultDTO;
						}
						if (context.isInternalAdministrator()) {
							String _branchCode = formDTO.get("BRANCH_CODE");
							if (_branchCode.equals(RegularConstants.COLUMN_DISABLE)) {
								resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INSUFFICIENT_ROLE_RIGHTS);
								return resultDTO;
							}
						}
						if (context.isInternalOperations()) {
							String _branchCode = formDTO.get("BRANCH_CODE");
							if (!_branchCode.equals(RegularConstants.COLUMN_DISABLE)) {
								resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INSUFFICIENT_ROLE_RIGHTS);
								return resultDTO;
							}
						}
					}
				}
			}
			resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;

	}

	public DTObject loadGridValues(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBContext dbContext = new DBContext();
		try {
			DBUtil util = dbContext.createUtilInstance();
			String sql = null;
			if (context.isInternalAdministrator()) {
				sql = "SELECT BL.USER_ID, B.USER_NAME,TO_CHAR(BL.LOGIN_DATETIME,'"+BackOfficeConstants.TBA_XML_DATETIME_FORMAT+"') LOGIN_DATETIME, BL.LOGIN_SESSION_ID, BL.LOGIN_SESSION_IP,BL.USER_AGENT FROM USERS B, USERSLOGIN BL, ROLES R WHERE BL.ENTITY_CODE=? AND BL.USER_ID= B.USER_ID AND BL.LOGOUT_DATETIME IS NULL AND B.ENTITY_CODE = R.ENTITY_CODE AND B.ROLE_CODE = R.CODE AND R.ROLE_TYPE IN ('1','2','3','4') AND BL.USER_ID <> ? ORDER BY BL.USER_ID,BL.LOGIN_DATETIME DESC";
			}
			if (context.isInternalOperations()) {
				sql = "SELECT BL.USER_ID, B.USER_NAME,TO_CHAR(BL.LOGIN_DATETIME,"+BackOfficeConstants.TBA_XML_DATETIME_FORMAT+") LOGIN_DATETIME, BL.LOGIN_SESSION_ID, BL.LOGIN_SESSION_IP,BL.USER_AGENT FROM USERS B, USERSLOGIN BL, ROLES R WHERE BL.ENTITY_CODE=? AND BL.USER_ID= B.USER_ID AND BL.LOGOUT_DATETIME IS NULL AND B.ENTITY_CODE = R.ENTITY_CODE AND B.ROLE_CODE = R.CODE AND R.ROLE_TYPE IN ('5','6') AND BL.USER_ID <> ? ORDER BY BL.USER_ID,BL.LOGIN_DATETIME DESC";
			}
			util.setSql(sql);
			util.setString(1, context.getEntityCode());
			util.setString(2, context.getUserID());
			ResultSet rset = util.executeQuery();
			int columnCount = rset.getMetaData().getColumnCount();
			DHTMLXGridUtility utility = new DHTMLXGridUtility();
			utility.init();
			while (rset.next()) {
				utility.startRow();
				utility.setCell("0");
				for (int j = 1; j <= columnCount; ++j) {
					utility.setCell(rset.getString(j));
				}
				utility.endRow();
			}
			utility.finish();
			util.reset();
			resultDTO.set(ContentManager.RESULT_XML, utility.getXML());
			resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			dbContext.close();
		}
		return resultDTO;
	}
}