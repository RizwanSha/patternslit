package patterns.config.web.forms.access;

import java.util.HashMap;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.BindParameterType;
import patterns.config.framework.database.DBEntry;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.process.TBAActionType;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.AccessValidator;
import patterns.config.validations.CommonValidator;
import patterns.config.web.forms.GenericFormBean;

public class euserbrnallocbean extends GenericFormBean {

	private static final long serialVersionUID = 1L;

	private String userID;
	private String effectiveDate;
	private String branchCode;
	private String remarks;
	private String currentBranchCode;

	public String getCurrentBranchCode() {
		return currentBranchCode;
	}

	public void setCurrentBranchCode(String currentBranchCode) {
		this.currentBranchCode = currentBranchCode;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public String getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public void reset() {
		userID = RegularConstants.EMPTY_STRING;
		effectiveDate = RegularConstants.EMPTY_STRING;
		branchCode = RegularConstants.EMPTY_STRING;
		remarks = RegularConstants.EMPTY_STRING;
		setProcessBO("patterns.config.framework.bo.access.euserbrnallocBO");
	}

	public void validate() {
		if (validateUserID()) {
			if (validateEffectiveDate()) {
				if (validatePK()) {
					validateBranchCode();
					validateRemarks();
				}
			}
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.set("USER_ID", userID);
				formDTO.setObject("EFFT_DATE", BackOfficeFormatUtils.getDate(effectiveDate, context.getDateFormat()));
				formDTO.set("BRANCH_CODE", branchCode);
				formDTO.set("REMARKS", remarks);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("userID", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
	}

	public boolean validateUserID() {
		AccessValidator validation = new AccessValidator();
		try {
			if (validation.isEmpty(userID)) {
				getErrorMap().setError("userID", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			DTObject formDTO = getFormDTO();
			formDTO.set(ContentManager.USER_ID, userID);
			formDTO.set(ContentManager.FETCH, ContentManager.FETCH_ALL);
			formDTO = validation.validateInternalUser(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("userID", formDTO.get(ContentManager.ERROR));
				return false;
			}
			if (formDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				getErrorMap().setError("userID", BackOfficeErrorCodes.RECORD_NOT_EXISTS);
				return false;
			}
			if (context.isInternalAdministrator()) {
				String _branchCode = formDTO.get("BRANCH_CODE");
				if (_branchCode.equals(RegularConstants.COLUMN_DISABLE)) {
					getErrorMap().setError("userID", BackOfficeErrorCodes.INSUFFICIENT_ROLE_RIGHTS);
					return false;
				}
			}
			if (context.isInternalOperations()) {
				String _branchCode = formDTO.get("BRANCH_CODE");
				if (!_branchCode.equals(RegularConstants.COLUMN_DISABLE)) {
					getErrorMap().setError("userID", BackOfficeErrorCodes.INSUFFICIENT_ROLE_RIGHTS);
					return false;
				}
			}
			if (userID.equals(context.getUserID())) {
				getErrorMap().setError("userID", BackOfficeErrorCodes.CANNOT_ALLOCATE_TO_SELF);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("userID", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateEffectiveDate() {
		CommonValidator validation = new CommonValidator();
		TBAActionType AT;
		try {
			if (getAction().equals(ADD))
				AT = TBAActionType.ADD;
			else
				AT = TBAActionType.MODIFY;
			if (validation.isEmpty(effectiveDate)) {
				getErrorMap().setError("effectiveDate", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isEffectiveDateValid(effectiveDate, AT)) {
				getErrorMap().setError("effectiveDate", BackOfficeErrorCodes.INVALID_DATE);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("effectiveDate", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validatePK() {
		AccessValidator validation = new AccessValidator();
		HashMap<Integer, DBEntry> whereClauseFieldsMap = new HashMap<Integer, DBEntry>();
		whereClauseFieldsMap.put(1, new DBEntry("ENTITY_CODE", context.getEntityCode(), BindParameterType.VARCHAR));
		whereClauseFieldsMap.put(2, new DBEntry("USER_ID", userID, BindParameterType.VARCHAR));
		whereClauseFieldsMap.put(3, new DBEntry("EFFT_DATE", BackOfficeFormatUtils.getDate(effectiveDate, context.getDateFormat()), BindParameterType.DATE));
		boolean isRecordAvailable = validation.isRecordAvailable("USERSBRNALLOC", whereClauseFieldsMap);
		try {
			if (isRecordAvailable) {

				if (getAction().equals(ADD)) {
					getErrorMap().setError("userID", BackOfficeErrorCodes.RECORD_EXISTS);
					return false;
				}
				return true;
			} else {

				if (getAction().equals(MODIFY)) {
					getErrorMap().setError("userID", BackOfficeErrorCodes.RECORD_NOT_EXISTS);
					return false;
				}
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("userID", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateBranchCode() {
		AccessValidator validation = new AccessValidator();
		try {
			DTObject formDTO = getFormDTO();
			formDTO.set("MBRN_CODE", branchCode);
			formDTO = validation.validateBranch(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("branchCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			if (formDTO.get(ContentManager.RESULT).equalsIgnoreCase(ContentManager.DATA_UNAVAILABLE)) {
				getErrorMap().setError("branchCode", BackOfficeErrorCodes.INVALID_CODE);
				return false;
			}
			String userBranchCode = validation.getInternalUserBranchCode(userID);
			if (userBranchCode.equals(branchCode)) {
				getErrorMap().setError("branchCode", BackOfficeErrorCodes.PRIMARY_BRANCH_NOT_ALLOWED);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("branchCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public DTObject validateCurrentBranch(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DTObject formDTO = getFormDTO();
		AccessValidator validation = new AccessValidator();
		try {
			String userID = inputDTO.get("USER_ID");
			if (validation.isEmpty(userID)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			String userBranchCode = validation.getInternalUserBranchCode(userID);
			formDTO.set("MBRN_CODE", userBranchCode);
			formDTO.set(ContentManager.FETCH, ContentManager.FETCH_ALL);
			formDTO = validation.validateBranch(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, formDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (!formDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
			String description = formDTO.get("MBRN_NAME");
			resultDTO.set("MBRN_CODE", userBranchCode);
			resultDTO.set("MBRN_NAME", description);
			resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateRemarks() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(remarks)) {
				getErrorMap().setError("remarks", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidRemarks(remarks)) {
				getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}
}