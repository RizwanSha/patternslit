package patterns.config.web.forms.access;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.ArrayList;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.framework.web.DHTMLXGridUtility;
import patterns.config.framework.web.GenericOption;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.AccessValidator;
import patterns.config.web.forms.GenericFormBean;

public class quserloginattemptbean extends GenericFormBean {

	private static final long serialVersionUID = -6219657363150466160L;

	private String userID;
	private String fromDate;
	private String toDate;
	private String attemptType;

	public String getAttemptType() {
		return attemptType;
	}

	public void setAttemptType(String attemptType) {
		this.attemptType = attemptType;
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public void reset() {
		userID = RegularConstants.EMPTY_STRING;
		fromDate = RegularConstants.EMPTY_STRING;
		toDate = RegularConstants.EMPTY_STRING;
		attemptType = RegularConstants.EMPTY_STRING;
		DBContext dbContext = new DBContext();
		ArrayList<GenericOption> attemptTypeList = null;
		attemptTypeList = getGenericOptions("COMMON", "LOGINATTEMPT", dbContext, "--Attempt Type--");
		webContext.getRequest().setAttribute("COMMON_LOGINATTEMPT", attemptTypeList);
		dbContext.close();
	}

	public void validate() {

	}

	public DTObject loadUserLoginGrid(DTObject inputDTO) {
		inputDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		String entityCode = context.getEntityCode();
		String userId = inputDTO.get("USER_ID");
		Date toDate = new java.sql.Date(BackOfficeFormatUtils.getDate(inputDTO.get("TO_DATE"), BackOfficeConstants.JAVA_DATE_FORMAT).getTime());
		Date fromDate = new java.sql.Date(BackOfficeFormatUtils.getDate(inputDTO.get("FROM_DATE"), BackOfficeConstants.JAVA_DATE_FORMAT).getTime());
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		DHTMLXGridUtility gridUtility = new DHTMLXGridUtility();
		try {
			if (inputDTO.get("ATTEMPT_TYPE").equals("S")) {
				String sqlQuery = "SELECT @ROWNUM:=@ROWNUM+1,A.USER_ID,A.LOGIN_DATETIME ,A.LOGOUT_DATETIME,A.REASON,A.LOGIN_SESSION_IP, A.LOGIN_SESSION_ID,A.LOGOUT_SESSION_ID, A.USER_AGENT FROM USERSLOGINHIST A,(SELECT @ROWNUM:=0) X WHERE A.ENTITY_CODE=? AND A.USER_ID=? AND A.LOGIN_DATETIME BETWEEN ? AND ?";
				util.reset();
				util.setSql(sqlQuery);
				util.setString(1, entityCode);
				util.setString(2, userId);
				util.setDate(3, fromDate);
				util.setDate(4, toDate);
				ResultSet rset = util.executeQuery();
				gridUtility.init();
				while (rset.next()) {
					gridUtility.startRow(rset.getString(1));
					gridUtility.setCell(rset.getString(1));
					gridUtility.setCell(rset.getString(2));
					gridUtility.setCell(rset.getString(3));
					gridUtility.setCell(rset.getString(4));
					gridUtility.setCell(rset.getString(5));
					gridUtility.setCell(rset.getString(6));
					gridUtility.setCell(rset.getString(7));
					gridUtility.setCell(rset.getString(8));
					gridUtility.setCell(rset.getString(9));
					gridUtility.endRow();
				}
				gridUtility.finish();
				inputDTO.set("XML", gridUtility.getXML());
				inputDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
			} else if (inputDTO.get("ATTEMPT_TYPE").equals("U")) {
				String sqlQuery = "SELECT @ROWNUM:=@ROWNUM+1,A.USER_ID,A.ATTEMPT_DATETIME,A.REASON,A.USER_AGENT,A.USER_IP,A.LOGIN_SESSION_ID FROM USERSLOGINATTEMPTS A,(SELECT @ROWNUM:=0) X WHERE A.ENTITY_CODE=? AND A.USER_ID=? AND A.ATTEMPT_DATETIME BETWEEN ? AND ?";
				util.reset();
				util.setSql(sqlQuery);
				util.setString(1, entityCode);
				util.setString(2, userId);
				util.setDate(3, fromDate);
				util.setDate(4, toDate);
				ResultSet rset = util.executeQuery();
				gridUtility.init();
				while (rset.next()) {
					gridUtility.startRow(rset.getString(1));
					gridUtility.setCell(rset.getString(1));
					gridUtility.setCell(rset.getString(2));
					gridUtility.setCell(rset.getString(3));
					gridUtility.setCell(rset.getString(4));
					gridUtility.setCell(rset.getString(5));
					gridUtility.setCell(rset.getString(6));
					gridUtility.setCell(rset.getString(7));
					gridUtility.endRow();
				}
				gridUtility.finish();
				inputDTO.set("XML", gridUtility.getXML());
				inputDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
			} else if (inputDTO.get("ATTEMPT_TYPE").equals("X")) {
				String sqlQuery = "SELECT @ROWNUM:=@ROWNUM+1,A.USER_ID,A.LOGIN_DATETIME ,A.LOGOUT_DATETIME,A.REASON,A.LOGIN_SESSION_IP, A.LOGIN_SESSION_ID,A.LOGOUT_SESSION_ID, A.USER_AGENT FROM USERSLOGINHIST A,(SELECT @ROWNUM:=0) X WHERE A.ENTITY_CODE=? AND A.USER_ID=? AND A.LOGIN_DATETIME BETWEEN ? AND ?";
				util.reset();
				util.setSql(sqlQuery);
				util.setString(1, entityCode);
				util.setString(2, userId);
				util.setDate(3, fromDate);
				util.setDate(4, toDate);
				ResultSet rset = util.executeQuery();
				gridUtility.init();
				while (rset.next()) {
					gridUtility.startRow(rset.getString(1));
					gridUtility.setCell(rset.getString(1));
					gridUtility.setCell(rset.getString(2));
					gridUtility.setCell(rset.getString(3));
					gridUtility.setCell(rset.getString(4));
					gridUtility.setCell(rset.getString(5));
					gridUtility.setCell(rset.getString(6));
					gridUtility.setCell(rset.getString(7));
					gridUtility.setCell(rset.getString(8));
					gridUtility.setCell(rset.getString(9));
					gridUtility.endRow();
				}
				gridUtility.finish();
				inputDTO.set("XML1", gridUtility.getXML());
				sqlQuery = "SELECT @ROWNUM:=@ROWNUM+1,A.USER_ID,A.ATTEMPT_DATETIME,A.REASON,A.USER_AGENT,A.USER_IP,A.LOGIN_SESSION_ID FROM USERSLOGINATTEMPTS A,(SELECT @ROWNUM:=0) X WHERE A.ENTITY_CODE=? AND A.USER_ID=? AND A.ATTEMPT_DATETIME BETWEEN ? AND ?";
				util.reset();
				util.setSql(sqlQuery);
				util.setString(1, entityCode);
				util.setString(2, userId);
				util.setDate(3, fromDate);
				util.setDate(4, toDate);
				ResultSet rset2 = util.executeQuery();
				gridUtility.init();
				while (rset2.next()) {
					gridUtility.startRow(rset2.getString(1));
					gridUtility.setCell(rset2.getString(1));
					gridUtility.setCell(rset2.getString(2));
					gridUtility.setCell(rset2.getString(3));
					gridUtility.setCell(rset2.getString(4));
					gridUtility.setCell(rset2.getString(5));
					gridUtility.setCell(rset2.getString(6));
					gridUtility.setCell(rset2.getString(7));
					gridUtility.endRow();
				}
				gridUtility.finish();
				inputDTO.set("XML2", gridUtility.getXML());
				inputDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			inputDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			dbContext.close();
		}
		return inputDTO;
	}

	public DTObject validateUserID(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		AccessValidator validation = new AccessValidator();
		String userID = inputDTO.get("USER_ID");
		try {
			if (validation.isEmpty(userID)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS,"USER_ID,USER_NAME");
			resultDTO = validation.validateInternalUser(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_USERID);
				return resultDTO;
			}
			resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

}
