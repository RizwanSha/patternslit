package patterns.config.web.forms.access;

import java.util.ArrayList;
import java.util.HashMap;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.BindParameterType;
import patterns.config.framework.database.DBEntry;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.framework.web.GenericOption;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.AccessValidator;
import patterns.config.validations.CommonValidator;
import patterns.config.web.forms.GenericFormBean;

public class euseractivationbean extends GenericFormBean {

	private static final long serialVersionUID = -6219657363150466160L;

	private String userID;
	private String userActivationStatus;
	private String intimationMode;
	private String intimationReferenceNo;
	private String intimationDate;
	private String remarks;
	private boolean userActivation;
	private String tmpActivationStatus;
	private String activationPolicy;

	public String getActivationPolicy() {
		return activationPolicy;
	}

	public void setActivationPolicy(String activationPolicy) {
		this.activationPolicy = activationPolicy;
	}

	public String getTmpActivationStatus() {
		return tmpActivationStatus;
	}

	public void setTmpActivationStatus(String tmpActivationStatus) {
		this.tmpActivationStatus = tmpActivationStatus;
	}

	public boolean isUserActivation() {
		return userActivation;
	}

	public void setUserActivation(boolean userActivation) {
		this.userActivation = userActivation;
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public String getUserActivationStatus() {
		return userActivationStatus;
	}

	public void setUserActivationStatus(String userActivationStatus) {
		this.userActivationStatus = userActivationStatus;
	}

	public String getIntimationMode() {
		return intimationMode;
	}

	public void setIntimationMode(String intimationMode) {
		this.intimationMode = intimationMode;
	}

	public String getIntimationReferenceNo() {
		return intimationReferenceNo;
	}

	public void setIntimationReferenceNo(String intimationReferenceNo) {
		this.intimationReferenceNo = intimationReferenceNo;
	}

	public String getIntimationDate() {
		return intimationDate;
	}

	public void setIntimationDate(String intimationDate) {
		this.intimationDate = intimationDate;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public void reset() {
		userID = RegularConstants.EMPTY_STRING;
		userActivationStatus = RegularConstants.EMPTY_STRING;
		intimationMode = RegularConstants.EMPTY_STRING;
		intimationDate = RegularConstants.EMPTY_STRING;
		intimationReferenceNo = RegularConstants.EMPTY_STRING;
		remarks = RegularConstants.EMPTY_STRING;

		DBContext dbContext = new DBContext();
		ArrayList<GenericOption> currentStatusList = null;
		ArrayList<GenericOption> intimationList = null;
		currentStatusList = getGenericOptions("COMMON", "ACTIVATIONSTATUS", dbContext, "--");
		webContext.getRequest().setAttribute("COMMON_ACTIVATIONSTATUS", currentStatusList);
		intimationList = getGenericOptions("COMMON", "INTIMATIONMODE", dbContext, "--");
		webContext.getRequest().setAttribute("COMMON_INTIMATIONMODE", intimationList);
		dbContext.close();
		DTObject formDTO = getFormDTO();
		AccessValidator validator = new AccessValidator();
		formDTO.set(ContentManager.CODE, userID);
		formDTO = validator.getSYSCPM(formDTO);
		boolean activationEnabled = false;
		if (formDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
			activationEnabled = decodeStringToBoolean(formDTO.get("ADMIN_ACTIVATION_REQD"));
		}
		validator.close();
		Object[] param = new Object[] {};
		if (activationEnabled) {
			setActivationPolicy(getCommonResource("form.activationpolicy2", param));
		} else {
			setActivationPolicy(getCommonResource("form.activationpolicy1", param));
		}

		setProcessBO("patterns.config.framework.bo.access.euseractivationBO");
	}

	public void validate() {
		if (userActivation) {
			if (validateUserID()) {
				validateIntimationMode();
				validateIntimationReferenceNo();
				validateIntimationDate();
				validateRemarks();
				validatePK();
			}
		} else {
			getErrorMap().setError("userID", BackOfficeErrorCodes.INVALID_USER_ACTIVATION);
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.set("USER_ID", userID);
				formDTO.set("INTIMATION_MODE", intimationMode);
				formDTO.set("INTIMATION_REF_NO", intimationReferenceNo);
				formDTO.setObject("INTIMATION_DATE", BackOfficeFormatUtils.getDate(intimationDate, context.getDateFormat()));
				formDTO.set("REMARKS", remarks);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("userID", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
	}

	public boolean validateUserID() {
		AccessValidator validation = new AccessValidator();
		DTObject formDTO = getFormDTO();
		try {
			if (validation.isEmpty(userID)) {
				getErrorMap().setError("userID", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			formDTO.set(ContentManager.USER_ID, userID);
			formDTO.set(ContentManager.FETCH, ContentManager.FETCH_ALL);
			formDTO = validation.validateInternalUser(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("userID", formDTO.get(ContentManager.ERROR));
				return false;
			}
			if (getAction().equals(MODIFY)) {
				if (formDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
					getErrorMap().setError("userID", BackOfficeErrorCodes.RECORD_NOT_EXISTS);
					return false;
				}
				if (context.isInternalAdministrator()) {
					String _branchCode = formDTO.get("BRANCH_CODE");
					if (_branchCode.equals(RegularConstants.COLUMN_DISABLE)) {
						getErrorMap().setError("userID", BackOfficeErrorCodes.INSUFFICIENT_ROLE_RIGHTS);
						return false;
					}
				}
				if (context.isInternalOperations()) {
					String _branchCode = formDTO.get("BRANCH_CODE");
					if (!_branchCode.equals(RegularConstants.COLUMN_DISABLE)) {
						getErrorMap().setError("userID", BackOfficeErrorCodes.INSUFFICIENT_ROLE_RIGHTS);
						return false;
					}
				}
				if (!userActivation) {
					getErrorMap().setError("userID", BackOfficeErrorCodes.INVALID_USER_ACTIVATION);
					return false;
				}
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("userID", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public DTObject validateUserId(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		AccessValidator validation = new AccessValidator();
		String userId = inputDTO.get("USER_ID");
		String userActivation = inputDTO.get("activation");
		String action = inputDTO.get(ContentManager.USER_ACTION);
		try {

			if (validation.isEmpty(userId)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_FIELD_BLANK);
				return resultDTO;
			}
			if (userId.equals(context.getUserID())) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.CANNOT_ALLOCATE_TO_SELF);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH, ContentManager.FETCH_ALL);
			resultDTO = validation.validateInternalUser(inputDTO);
			if (action.equals(ADD)) {
				if (ContentManager.DATA_AVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_RECORD_EXISTS);
				}
			} else {
				if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_RECORD_NOT_EXISTS);

				}
			}
			if (userActivation.equals("0")) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_USER_ACTIVATION);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateIntimationMode() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(intimationMode)) {
				getErrorMap().setError("intimationMode", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isCMLOVREC("COMMON", "INTIMATIONMODE", intimationMode)) {
				getErrorMap().setError("intimationMode", BackOfficeErrorCodes.INVALID_CODE);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("intimationMode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateIntimationReferenceNo() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(intimationReferenceNo)) {
				return true;
			}
			if (!validation.isValidRefNumber(intimationReferenceNo)) {
				getErrorMap().setError("intimationReferenceNo", BackOfficeErrorCodes.MAX_LENGTH_EXCEED);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("intimationReferenceNo", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateIntimationDate() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(intimationDate)) {
				getErrorMap().setError("intimationDate", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			java.util.Date dateInstance = validation.isValidDate(intimationDate);
			if (dateInstance != null) {
				if (!validation.isDateLesserEqualCBD(intimationDate)) {
					getErrorMap().setError("intimationDate", BackOfficeErrorCodes.DATE_LCBD);
					return false;
				}
				return true;
			} else {
				getErrorMap().setError("intimationDate", BackOfficeErrorCodes.INVALID_DATE);
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("intimationDate", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateRemarks() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(remarks)) {
				getErrorMap().setError("remarks", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidRemarks(remarks)) {
				getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validatePK() {
		if (getAction().equals(ADD)) {
			getErrorMap().setError("userID", BackOfficeErrorCodes.INVALID_ACTION);
			return false;
		}
		CommonValidator validation = new CommonValidator();
		try {
			if (getAction().equals(MODIFY)) {
				HashMap<Integer, DBEntry> whereClauseFieldsMap = new HashMap<Integer, DBEntry>();
				whereClauseFieldsMap.put(1, new DBEntry("ENTITY_CODE", context.getEntityCode(), BindParameterType.VARCHAR));
				whereClauseFieldsMap.put(2, new DBEntry("USER_ID", userID, BindParameterType.VARCHAR));
				boolean isRecordAvailable = validation.isRecordAvailable("USERSACTIVATION", whereClauseFieldsMap);
				if (isRecordAvailable) {
					return true;
				} else {
					getErrorMap().setError("userID", BackOfficeErrorCodes.RECORD_NOT_EXISTS);
					return false;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("userID", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}
}