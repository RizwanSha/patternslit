package patterns.config.web.forms.access;

import java.util.HashMap;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.BindParameterType;
import patterns.config.framework.database.CM_LOVREC;
import patterns.config.framework.database.DBEntry;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.process.TBAActionType;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.AccessValidator;
import patterns.config.validations.CommonValidator;
import patterns.config.web.forms.GenericFormBean;

public class iuserpkiauthbean extends GenericFormBean {

	private String roleCode;
	private String effectiveDate;
	private boolean tfaMandatory;
	private boolean loginAuthReqd;
	private String remarks;

	public String getRoleCode() {
		return roleCode;
	}

	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}

	public String getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public boolean isTfaMandatory() {
		return tfaMandatory;
	}

	public void setTfaMandatory(boolean tfaMandatory) {
		this.tfaMandatory = tfaMandatory;
	}

	public boolean isLoginAuthReqd() {
		return loginAuthReqd;
	}

	public void setLoginAuthReqd(boolean loginAuthReqd) {
		this.loginAuthReqd = loginAuthReqd;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	private static final long serialVersionUID = 1L;

	public void reset() {
		roleCode = RegularConstants.EMPTY_STRING;
		effectiveDate = RegularConstants.EMPTY_STRING;
		remarks = RegularConstants.EMPTY_STRING;
		setProcessBO("patterns.config.framework.bo.access.iuserpkiauthBO");
	}

	public void validate() {
		if (validateRoleCode()) {
			if (validateEffectiveDate()) {
				validateRemarks();
				validatePK();
			}
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.set("ROLE_CODE", roleCode);
				formDTO.setObject("EFFT_DATE", BackOfficeFormatUtils.getDate(effectiveDate, context.getDateFormat()));
				formDTO.set("TFA_MAND", decodeBooleanToString(tfaMandatory));
				if (tfaMandatory) {
					formDTO.set("LOGIN_AUTH_REQD", decodeBooleanToString(loginAuthReqd));
				} else {
					formDTO.set("LOGIN_AUTH_REQD", RegularConstants.NULL);
				}
				formDTO.set("REMARKS", remarks);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("roleCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
	}

	public boolean validateRoleCode() {
		AccessValidator validation = new AccessValidator();
		try {
			if (validation.isEmpty(roleCode)) {
				getErrorMap().setError("roleCode", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			DTObject formDTO = getFormDTO();
			formDTO.set(ContentManager.FETCH, ContentManager.FETCH_ALL);
			formDTO.set(ContentManager.CODE, roleCode);
			formDTO = validation.validateInternalRole(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("roleCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			if (formDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				getErrorMap().setError("roleCode", BackOfficeErrorCodes.INVALID_ROLECODE);
				return false;
			}
			String roleType = formDTO.get("ROLE_TYPE");
			if (context.isInternalAdministrator()) {
				if (!roleType.equals(CM_LOVREC.COMMON_ROLES_1) && !roleType.equals(CM_LOVREC.COMMON_ROLES_2) && !roleType.equals(CM_LOVREC.COMMON_ROLES_3) && !roleType.equals(CM_LOVREC.COMMON_ROLES_4)) {
					getErrorMap().setError("roleCode", BackOfficeErrorCodes.INSUFFICIENT_ROLE_RIGHTS);
					return false;
				}
			}
			if (context.isInternalOperations()) {
				if (!roleType.equals(CM_LOVREC.COMMON_ROLES_5) && !roleType.equals(CM_LOVREC.COMMON_ROLES_6)) {
					getErrorMap().setError("roleCode", BackOfficeErrorCodes.INSUFFICIENT_ROLE_RIGHTS);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("roleCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateEffectiveDate() {
		CommonValidator validation = new CommonValidator();
		try {
			TBAActionType AT;
			if (getAction().equals(ADD))
				AT = TBAActionType.ADD;
			else
				AT = TBAActionType.MODIFY;
			if (validation.isEmpty(effectiveDate)) {
				getErrorMap().setError("effectiveDate", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isEffectiveDateValid(effectiveDate, AT)) {
				getErrorMap().setError("effectiveDate", BackOfficeErrorCodes.INVALID_DATE);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("effectiveDate", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validatePK() {
		CommonValidator validation = new CommonValidator();
		try {
			HashMap<Integer, DBEntry> whereClauseFieldsMap = new HashMap<Integer, DBEntry>();
			whereClauseFieldsMap.put(1, new DBEntry("ENTITY_CODE", context.getEntityCode(), BindParameterType.VARCHAR));
			whereClauseFieldsMap.put(2, new DBEntry("ROLE_CODE", roleCode, BindParameterType.VARCHAR));
			whereClauseFieldsMap.put(3, new DBEntry("EFFT_DATE", BackOfficeFormatUtils.getDate(effectiveDate, context.getDateFormat()), BindParameterType.DATE));
			boolean isRecordAvailable = validation.isRecordAvailable("USERSPKIAUTH", whereClauseFieldsMap);
			if (isRecordAvailable) {
				if (getAction().equals(ADD)) {
					getErrorMap().setError("roleCode", BackOfficeErrorCodes.RECORD_EXISTS);
					return false;
				}

			} else {
				if (getAction().equals(MODIFY)) {
					getErrorMap().setError("roleCode", BackOfficeErrorCodes.RECORD_NOT_EXISTS);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("roleCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateRemarks() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(remarks)) {
				getErrorMap().setError("remarks", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidRemarks(remarks)) {
				getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}
}