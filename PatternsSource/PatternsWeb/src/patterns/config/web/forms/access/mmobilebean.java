package patterns.config.web.forms.access;

import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.service.DTObject;
import patterns.config.web.forms.GenericFormBean;

public class mmobilebean extends GenericFormBean {

	private static final long serialVersionUID = -6219657363150466160L;

	private String mobileNumber;
	private String userid;
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	
	
	@Override
	public void reset() {
		mobileNumber = RegularConstants.EMPTY_STRING;
		userid = RegularConstants.EMPTY_STRING;
		setProcessBO("patterns.config.framework.bo.access.mmobileBO");
	}
	@Override
	public void validate() {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.set("MOBILENUMBER", mobileNumber);
				formDTO.set("USERID", userid);
			}
	}

}