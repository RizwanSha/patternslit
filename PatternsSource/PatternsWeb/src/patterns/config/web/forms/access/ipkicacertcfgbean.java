package patterns.config.web.forms.access;

import java.sql.ResultSet;
import java.util.HashMap;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.BindParameterType;
import patterns.config.framework.database.DBEntry;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.process.TBAActionType;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.AccessValidator;
import patterns.config.validations.CommonValidator;
import patterns.config.web.forms.FileFormBean;

public class ipkicacertcfgbean extends FileFormBean {

	private static final long serialVersionUID = -6219657363150466160L;

	private String rootCaCode;

	private String effectiveDate;
	private boolean revoked;
	private String remarks;
	private String fileInventoryNumber;
	private String filePurpose;

	public String getFilePurpose() {
		return filePurpose;
	}

	public void setFilePurpose(String filePurpose) {
		this.filePurpose = filePurpose;
	}

	public String getFileInventoryNumber() {
		return fileInventoryNumber;
	}

	public void setFileInventoryNumber(String fileInventoryNumber) {
		this.fileInventoryNumber = fileInventoryNumber;
	}

	public String getRootCaCode() {
		return rootCaCode;
	}

	public void setRootCaCode(String rootCaCode) {
		this.rootCaCode = rootCaCode;
	}

	public String getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public boolean isRevoked() {
		return revoked;
	}

	public void setRevoked(boolean revoked) {
		this.revoked = revoked;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public void reset() {
		rootCaCode = RegularConstants.EMPTY_STRING;
		effectiveDate = RegularConstants.EMPTY_STRING;
		remarks = RegularConstants.EMPTY_STRING;
		revoked = false;
		fileInventoryNumber = RegularConstants.EMPTY_STRING;
		setProcessBO("patterns.config.framework.bo.access.ipkicacertcfgBO");
	}

	public void validate() {
		if (validateRootCaCode()) {
			if (validateEffectiveDate()) {
				if (validatePK()) {
					validateRemarks();
					if (!revoked) {
						validateInventoryNumber();
					}
				}
			}
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.set("CA_CODE", rootCaCode);
				formDTO.setObject("EFFT_DATE", BackOfficeFormatUtils.getDate(effectiveDate, context.getDateFormat()));
				formDTO.set("REVOKED", decodeBooleanToString(revoked));
				formDTO.set("INV_NUM", fileInventoryNumber);
				formDTO.set("REMARKS", remarks);

			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("rootCaCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
	}

	public boolean validateRootCaCode() {
		AccessValidator validation = new AccessValidator();
		DTObject formDTO = getFormDTO();
		try {
			if (validation.isEmpty(rootCaCode)) {
				getErrorMap().setError("rootCaCode", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			formDTO.set(ContentManager.CODE, rootCaCode);
			formDTO.set(ContentManager.FETCH, ContentManager.FETCH_ALL);
			formDTO = validation.validateRootCaCode(formDTO);
			if (formDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				getErrorMap().setError("rootCaCode", BackOfficeErrorCodes.RECORD_NOT_EXISTS);
				return false;
			}
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("rootCaCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("rootCaCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateEffectiveDate() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(effectiveDate)) {
				getErrorMap().setError("effectiveDate", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			TBAActionType AT;
			if (getAction().equals(ADD))
				AT = TBAActionType.ADD;
			else
				AT = TBAActionType.MODIFY;
			if (!validation.isEffectiveDateValid(effectiveDate, AT)) {
				getErrorMap().setError("effectiveDate", BackOfficeErrorCodes.INVALID_EFFECTIVE_DATE);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("effectiveDate", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateInventoryNumber() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(fileInventoryNumber)) {
				getErrorMap().setError("fileInventoryNumber", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("fileInventoryNumber", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateRemarks() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(remarks)) {
				getErrorMap().setError("remarks", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidRemarks(remarks)) {
				getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validatePK() {
		CommonValidator validation = new CommonValidator();
		try {
			HashMap<Integer, DBEntry> whereClauseFieldsMap = new HashMap<Integer, DBEntry>();
			whereClauseFieldsMap.put(1, new DBEntry("ENTITY_CODE", context.getEntityCode(), BindParameterType.VARCHAR));
			whereClauseFieldsMap.put(2, new DBEntry("CA_CODE", rootCaCode, BindParameterType.VARCHAR));
			whereClauseFieldsMap.put(3, new DBEntry("EFFT_DATE", BackOfficeFormatUtils.getDate(effectiveDate, context.getDateFormat()), BindParameterType.DATE));
			boolean isRecordAvailable = validation.isRecordAvailable("PKICACERTCFG", whereClauseFieldsMap);
			if (isRecordAvailable) {
				if (getAction().equals(ADD)) {
					getErrorMap().setError("rootCaCode", BackOfficeErrorCodes.RECORD_EXISTS);
					return false;
				}
				return true;
			} else {
				if (getAction().equals(MODIFY)) {
					getErrorMap().setError("rootCaCode", BackOfficeErrorCodes.RECORD_NOT_EXISTS);
					return false;
				}
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("rootCaCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public DTObject fetchFileName(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
		DBContext dbContext = new DBContext();
		try {
			String inventoryNumber = inputDTO.get("CERT_INV_NUM");
			String sqlQuery = "SELECT DTL.CERT_FILE_NAME CERT_FILE_NAME FROM PKICERTINVENTORY  DTL WHERE DTL.ENTITY_CODE = ? AND DTL.CERT_INV_NUM = ?";
			DBUtil util = dbContext.createUtilInstance();
			util.setSql(sqlQuery);
			util.setString(1, context.getEntityCode());
			util.setString(2, inventoryNumber);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				resultDTO.set("CERT_FILE_NAME", rset.getString(1));
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
			}
			util.reset();
			return resultDTO;
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			dbContext.close();
		}
		return resultDTO;
	}
}