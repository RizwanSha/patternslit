package patterns.config.web.forms.access;

import java.sql.ResultSet;
import java.sql.Types;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.CSVUtility;
import patterns.config.framework.web.FileWritingUtility;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.framework.web.reports.ReportConstants;
import patterns.config.framework.web.reports.ReportUtils;
import patterns.config.validations.AccessValidator;
import patterns.config.web.forms.GenericFormBean;

public class qoprlogbean extends GenericFormBean {

	private static final long serialVersionUID = 1L;
	private String userID;
	private String fromDate;
	private String uptoDate;
	private String optionID;

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getUptoDate() {
		return uptoDate;
	}

	public void setUptoDate(String uptoDate) {
		this.uptoDate = uptoDate;
	}

	public String getOptionID() {
		return optionID;
	}

	public void setOptionID(String optionID) {
		this.optionID = optionID;
	}

	public void reset() {
		userID = RegularConstants.EMPTY_STRING;
		optionID = RegularConstants.EMPTY_STRING;
		uptoDate = RegularConstants.EMPTY_STRING;
		fromDate = RegularConstants.EMPTY_STRING;
	}

	public void validate() {

	}

	public DTObject validateUserID(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		DTObject formDTO = getFormDTO();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		AccessValidator validation = new AccessValidator();
		try {
			String userID = inputDTO.get("USER_ID");
			if (validation.isEmpty(userID)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			formDTO.set(ContentManager.USER_ID, userID);
			formDTO.set(ContentManager.FETCH, ContentManager.FETCH_ALL);
			formDTO = validation.validateInternalUser(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, formDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			String roleCode = ContentManager.EMPTY_STRING;
			if (!formDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_USERID);
				return resultDTO;
			}
			roleCode = formDTO.get("ROLE_CODE");
			resultDTO.set("ROLE_CODE", roleCode);
			
			resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public DTObject validateOptionID(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		DTObject formDTO = getFormDTO();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		AccessValidator validation = new AccessValidator();
		try {
			String optionID = inputDTO.get("MPGM_ID");
			if (validation.isEmpty(optionID)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			formDTO.set("MPGM_ID", optionID);
			formDTO = validation.validateOption(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, formDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (!formDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_OPTION);
				return resultDTO;
			}
			resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public DTObject getOperationLog(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBContext dbContext = new DBContext();
		try {
			String userID = inputDTO.get("USER_ID");
			String optionID = inputDTO.get("OPTION_ID");
			String fromDate = inputDTO.get("FROM_DATE");
			String toDate = inputDTO.get("UPTO_DATE");
			String reportType = inputDTO.get("REPORT_TYPE");
//			CustomerValidator validator = new CustomerValidator(dbContext);
//			inputDTO = validator.getSystemFolders(inputDTO);
//			if (inputDTO.get(ContentManager.ERROR) != null) {
//				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
//				return resultDTO;
//			}
//			if (inputDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
//				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FOLDER_NOT_AVAILABLE);
//				return resultDTO;
//			}
			String reportGenerationPath = inputDTO.get("REPORT_GENERATION_PATH");
			ReportUtils utils = new ReportUtils(dbContext);
			String reportIdentifier = utils.getReportSequence();
			String reportName = utils.getReportName(reportType, reportIdentifier, ReportConstants.REPORT_FORMAT_CSV);
			String downloadPath = utils.getReportFileOutputPath(reportGenerationPath, reportName);
			DBUtil util = dbContext.createUtilInstance();
			util.reset();
			util.setMode(DBUtil.CALLABLE);
			util.setSql("CALL PKG_REPORT.SP_INIT_REPORT(?,?,?,?)");
			util.setString(1, context.getEntityCode());
			util.setString(2, context.getDeploymentContext());
			util.setString(3, context.getUserID());
			util.registerOutParameter(4, Types.VARCHAR);
			util.execute();
			String systemDateTIme = util.getString(4);
			util.reset();
			CSVUtility utility = new CSVUtility(CSVUtility.DelimiterSeparator.DL_COMMA, CSVUtility.RecordSeparator.RS_LF);
			utility.init();
			utility.startHead();
			utility.setCell(context.getUserID());
			utility.setCell(systemDateTIme);
			utility.endHead();
			utility.startHead();
			if (context.isInternalAdministrator()) {
				utility.setCell("SL");
				utility.setCell("OFFICE CODE");
				utility.setCell("OFFICE NAME");
				utility.setCell("LOGIN DATE TIME");
				utility.setCell("OPTION DESCRIPTION");
				utility.setCell("ACCESS TIME");
			} else {
				utility.setCell("SL");
				utility.setCell("CUSTOMER CODE");
				utility.setCell("CUSTOMER NAME");
				utility.setCell("LOGIN DATE TIME");
				utility.setCell("OPTION DESCRIPTION");
				utility.setCell("ACCESS TIME");
			}
			utility.endHead();
			String sqlQuery = RegularConstants.NULL;
			if (context.isInternalAdministrator()) {
				if (optionID.equals(RegularConstants.EMPTY_STRING)) {
					sqlQuery = "SELECT ROWNUM SL, OL.OPRLOG_BRANCH_CODE OPRLOG_BRANCH_CODE, M.MBRN_NAME MBRN_NAME, TO_CHAR(OL.OPRLOG_LOGIN_DATETIME,'DD-MON-YYYY HH24:MI:SS') OPRLOG_LOGIN_DATETIME, OL.OPRLOG_FORM_NAME OPRLOG_FORM_NAME, TO_CHAR(OL.OPRLOG_IN_TIME,'DD-MON-YYYY HH24:MI:SS') ACCESS_TIME FROM OPRLOG OL, USERS BU, MBRN M WHERE OL.ENTITY_CODE=? AND OL.OPRLOG_USER_ID = ? AND OL.OPRLOG_USER_ID = BU.USER_ID AND OL.ENTITY_CODE = BU.ENTITY_CODE AND OL.OPRLOG_IN_TIME BETWEEN TO_DATE(?,?) AND (TO_DATE(?,?) + 1) AND OL.ENTITY_CODE = M.MBRN_ENTITY_NUM AND OL.OPRLOG_BRANCH_CODE = M.MBRN_CODE ORDER BY SL,BU.BRANCH_CODE,OL.OPRLOG_USER_ID, OL.OPRLOG_LOGIN_DATETIME, OL.OPRLOG_FORM_NAME";
					util.reset();
					util.setMode(DBUtil.PREPARED);
					util.setSql(sqlQuery);
					util.setString(1, context.getEntityCode());
					util.setString(2, userID);
					util.setString(3, fromDate);
					util.setString(4, context.getDateFormat());
					util.setString(5, toDate);
					util.setString(6, context.getDateFormat());
					ResultSet rset = util.executeQuery();
					int columnCount = rset.getMetaData().getColumnCount();
					while (rset.next()) {
						utility.startRow();
						for (int i = 1; i <= columnCount; i++) {
							utility.setCell(rset.getString(i));
						}
						utility.endRow();
					}
					utility.finish();
					FileWritingUtility writer = new FileWritingUtility();
					boolean written = writer.writeToFile(downloadPath, utility.getData());
					if (written) {
						utils.createReportDownload(context.getEntityCode(), reportIdentifier, context.getDeploymentContext(), context.getUserID(), ReportConstants.REPORT_FORMAT_CSV, downloadPath);
						resultDTO.set(ContentManager.REPORT_ID, reportIdentifier);
						resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
					} else {
						resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.ERROR_WRITING_FILE);
					}
				} else {
					sqlQuery = "SELECT ROWNUM SL, OL.OPRLOG_BRANCH_CODE OPRLOG_BRANCH_CODE, M.MBRN_NAME MBRN_NAME,TO_CHAR(OL.OPRLOG_LOGIN_DATETIME,'DD-MON-YYYY HH24:MI:SS') OPRLOG_LOGIN_DATETIME, OL.OPRLOG_FORM_NAME OPRLOG_FORM_NAME, TO_CHAR(OL.OPRLOG_IN_TIME,'DD-MON-YYYY HH24:MI:SS') ACCESS_TIME FROM OPRLOG OL, USERS BU,MBRN M WHERE OL.ENTITY_CODE=? AND OL.OPRLOG_USER_ID = ? AND OL.OPRLOG_FORM_NAME = ? AND OL.OPRLOG_USER_ID = BU.USER_ID AND OL.ENTITY_CODE = BU.ENTITY_CODE AND OL.OPRLOG_IN_TIME BETWEEN TO_DATE(?,?) AND (TO_DATE(?,?) + 1) AND OL.ENTITY_CODE = M.MBRN_ENTITY_NUM AND OL.OPRLOG_BRANCH_CODE = M.MBRN_CODE ORDER BY SL,BU.BRANCH_CODE,OL.OPRLOG_USER_ID, OL.OPRLOG_LOGIN_DATETIME, OL.OPRLOG_FORM_NAME";
					util.reset();
					util.setMode(DBUtil.PREPARED);
					util.setSql(sqlQuery);
					util.setString(1, context.getEntityCode());
					util.setString(2, userID);
					util.setString(3, optionID);
					util.setString(4, fromDate);
					util.setString(5, context.getDateFormat());
					util.setString(6, toDate);
					util.setString(7, context.getDateFormat());
					ResultSet rset = util.executeQuery();
					int columnCount = rset.getMetaData().getColumnCount();
					while (rset.next()) {
						utility.startRow();
						for (int i = 1; i <= columnCount; i++) {
							utility.setCell(rset.getString(i));
						}
						utility.endRow();
					}
					utility.finish();
					FileWritingUtility writer = new FileWritingUtility();
					boolean written = writer.writeToFile(downloadPath, utility.getData());
					if (written) {
						utils.createReportDownload(context.getEntityCode(), reportIdentifier, context.getDeploymentContext(), context.getUserID(), ReportConstants.REPORT_FORMAT_CSV, downloadPath);
						resultDTO.set(ContentManager.REPORT_ID, reportIdentifier);
						resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
					} else {
						resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.ERROR_WRITING_FILE);
					}
				}
			} else {
				if (optionID.equals(RegularConstants.EMPTY_STRING)) {
					sqlQuery = "SELECT ROWNUM SL, C.CUSTOMER_CODE CUSTOMER_CODE , C.CUSTOMER_NAME CUSTOMER_NAME, TO_CHAR(OL.OPRLOG_LOGIN_DATETIME,'DD-MON-YYYY HH24:MI:SS') OPRLOG_LOGIN_DATETIME, OL.OPRLOG_FORM_NAME OPRLOG_FORM_NAME, TO_CHAR(OL.OPRLOG_IN_TIME,'DD-MON-YYYY HH24:MI:SS') ACCESS_TIME FROM OPRLOG OL, USERS BU, CUSTOMER C WHERE OL.ENTITY_CODE= ? AND OL.OPRLOG_USER_ID = ? AND OL.OPRLOG_USER_ID = BU.USER_ID AND OL.ENTITY_CODE = BU.ENTITY_CODE AND OL.OPRLOG_IN_TIME BETWEEN TO_DATE(?,?) AND (TO_DATE(?,?) + 1) AND BU.ENTITY_CODE = C.ENTITY_CODE AND BU.CUSTOMER_CODE = C.CUSTOMER_CODE ORDER BY SL,BU.CUSTOMER_CODE,OL.OPRLOG_USER_ID, OL.OPRLOG_LOGIN_DATETIME, OL.OPRLOG_FORM_NAME";
					util.reset();
					util.setMode(DBUtil.PREPARED);
					util.setSql(sqlQuery);
					util.setString(1, context.getEntityCode());
					util.setString(2, userID);
					util.setString(3, fromDate);
					util.setString(4, context.getDateFormat());
					util.setString(5, toDate);
					util.setString(6, context.getDateFormat());
					ResultSet rset = util.executeQuery();
					int columnCount = rset.getMetaData().getColumnCount();
					while (rset.next()) {
						utility.startRow();
						for (int i = 1; i <= columnCount; i++) {
							utility.setCell(rset.getString(i));
						}
						utility.endRow();
					}
					utility.finish();
					FileWritingUtility writer = new FileWritingUtility();
					boolean written = writer.writeToFile(downloadPath, utility.getData());
					if (written) {
						utils.createReportDownload(context.getEntityCode(), reportIdentifier, context.getDeploymentContext(), context.getUserID(), ReportConstants.REPORT_FORMAT_CSV, downloadPath);
						resultDTO.set(ContentManager.REPORT_ID, reportIdentifier);
						resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
					} else {
						resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.ERROR_WRITING_FILE);
					}
				} else {
					sqlQuery = "SELECT ROWNUM SL, C.CUSTOMER_CODE CUSTOMER_CODE, C.CUSTOMER_NAME CUSTOMER_NAME, TO_CHAR(OL.OPRLOG_LOGIN_DATETIME,'DD-MON-YYYY HH24:MI:SS') OPRLOG_LOGIN_DATETIME, OL.OPRLOG_FORM_NAME OPRLOG_FORM_NAME, TO_CHAR(OL.OPRLOG_IN_TIME,'DD-MON-YYYY HH24:MI:SS') ACCESS_TIME FROM OPRLOG OL, USERS BU, CUSTOMER C WHERE OL.ENTITY_CODE= ? AND OL.OPRLOG_USER_ID = ? AND OL.OPRLOG_FORM_NAME = ? AND OL.OPRLOG_USER_ID = BU.USER_ID AND OL.ENTITY_CODE = BU.ENTITY_CODE AND OL.OPRLOG_IN_TIME BETWEEN TO_DATE(?,?) AND (TO_DATE(?,?) + 1) AND BU.ENTITY_CODE = C.ENTITY_CODE AND BU.CUSTOMER_CODE = C.CUSTOMER_CODE ORDER BY SL,BU.BRANCH_CODE,OL.OPRLOG_USER_ID, OL.OPRLOG_LOGIN_DATETIME, OL.OPRLOG_FORM_NAME";
					util.reset();
					util.setMode(DBUtil.PREPARED);
					util.setSql(sqlQuery);
					util.setString(1, context.getEntityCode());
					util.setString(2, userID);
					util.setString(3, optionID);
					util.setString(4, fromDate);
					util.setString(5, context.getDateFormat());
					util.setString(6, toDate);
					util.setString(7, context.getDateFormat());
					ResultSet rset = util.executeQuery();
					int columnCount = rset.getMetaData().getColumnCount();
					while (rset.next()) {
						utility.startRow();
						for (int i = 1; i <= columnCount; i++) {
							utility.setCell(rset.getString(i));
						}
						utility.endRow();
					}
					utility.finish();
					FileWritingUtility writer = new FileWritingUtility();
					boolean written = writer.writeToFile(downloadPath, utility.getData());
					if (written) {
						utils.createReportDownload(context.getEntityCode(), reportIdentifier, context.getDeploymentContext(), context.getUserID(), ReportConstants.REPORT_FORMAT_CSV, downloadPath);
						resultDTO.set(ContentManager.REPORT_ID, reportIdentifier);
						resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
					} else {
						resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.ERROR_WRITING_FILE);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.ERROR_WRITING_FILE);
		} finally {
			dbContext.close();
		}
		return resultDTO;
	}
}