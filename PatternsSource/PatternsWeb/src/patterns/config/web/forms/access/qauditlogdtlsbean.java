package patterns.config.web.forms.access;


import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.AccessValidator;
import patterns.config.web.forms.GenericFormBean;

public class qauditlogdtlsbean extends GenericFormBean {

	private static final long serialVersionUID = -6219657363150466160L;
	private String hidPrimaryKey;

	public String getHidPrimaryKey() {
		return hidPrimaryKey;
	}

	public void setHidPrimaryKey(String hidPrimaryKey) {
		this.hidPrimaryKey = hidPrimaryKey;
	}

	public void reset() {
		String pk = webContext.getRequest().getParameter("pk");
		if (pk != null) {
			hidPrimaryKey = pk;
		}
	}

	public void validate() {
	}

	public DTObject getAuditLogDetails(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		AccessValidator validation = new AccessValidator();
		try {
			resultDTO = validation.getAuditlogDetails(inputDTO);
		} catch (Exception e) {
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
			resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

}