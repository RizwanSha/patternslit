package patterns.config.web.forms.access;

import java.util.HashMap;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.BindParameterType;
import patterns.config.framework.database.CM_LOVREC;
import patterns.config.framework.database.DBEntry;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.process.TBAActionType;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.AccessValidator;
import patterns.config.validations.CommonValidator;
import patterns.config.web.forms.GenericFormBean;

public class euseractroleallocbean extends GenericFormBean {

	private static final long serialVersionUID = 1L;
	
	private String userID;
	private String roleCode;
	private boolean enabled;
	private String fromDate;
	private String toDate;

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public String getRoleCode() {
		return roleCode;
	}

	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	@Override
	public void reset() {
		userID = RegularConstants.EMPTY_STRING;
		roleCode = RegularConstants.EMPTY_STRING;
		fromDate = RegularConstants.EMPTY_STRING;
		toDate = RegularConstants.EMPTY_STRING;
		setProcessBO("patterns.config.framework.bo.access.euseractroleallocBO");

	}

	@Override
	public void validate() {
		if (validateUserID()) {
			if (validateRoleCode()) {
				if (validatePK()) {
					if (enabled) {
						validateFromDate();
						validateToDate();
					}
				}
			}
		}

		if (getErrorMap().length() == 0) {
			DTObject formDTO = getFormDTO();
			formDTO.set("ENTITY_CODE", context.getEntityCode());
			formDTO.set("USER_ID", userID);
			formDTO.set("ROLE_CODE", roleCode);
			formDTO.set("ENABLED", decodeBooleanToString(enabled));
			formDTO.setObject("FROM_DATE", BackOfficeFormatUtils.getDate(fromDate, context.getDateFormat()));
			formDTO.setObject("UPTO_DATE", BackOfficeFormatUtils.getDate(toDate, context.getDateFormat()));
		}
	}

	public boolean validateUserID() {
		AccessValidator validation = new AccessValidator();
		try {
			if (validation.isEmpty(userID)) {
				getErrorMap().setError("userID", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			DTObject formDTO = getFormDTO();
			formDTO.set(ContentManager.USER_ID, userID);
			formDTO.set(ContentManager.FETCH, ContentManager.FETCH_ALL);
			formDTO = validation.validateInternalUser(formDTO);
			if (formDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				getErrorMap().setError("userID", BackOfficeErrorCodes.RECORD_NOT_EXISTS);
				return false;
			}
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("userID", formDTO.get(ContentManager.ERROR));
				return false;
			}
			if (context.isInternalAdministrator()) {
				String _branchCode = formDTO.get("BRANCH_CODE");
				if (_branchCode.equals(RegularConstants.COLUMN_DISABLE)) {
					getErrorMap().setError("userID", BackOfficeErrorCodes.INSUFFICIENT_ROLE_RIGHTS);
					return false;
				}
			}
			if (context.isInternalOperations()) {
				String _branchCode = formDTO.get("BRANCH_CODE");
				if (!_branchCode.equals(RegularConstants.COLUMN_DISABLE)) {
					getErrorMap().setError("userID", BackOfficeErrorCodes.INSUFFICIENT_ROLE_RIGHTS);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("userID", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateRoleCode() {
		AccessValidator validation = new AccessValidator();
		try {
			if (validation.isEmpty(roleCode)) {
				getErrorMap().setError("roleCode", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			DTObject formDTO = getFormDTO();
			formDTO.set(ContentManager.CODE, roleCode);
			formDTO.set(ContentManager.FETCH, ContentManager.FETCH_ALL);
			formDTO = validation.validateInternalRole(formDTO);
			if (formDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				getErrorMap().setError("roleCode", BackOfficeErrorCodes.INVALID_CODE);
				return false;
			}
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("roleCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			String roleType = formDTO.get("ROLE_TYPE");
			if (context.isInternalAdministrator()) {
				if (!roleType.equals(CM_LOVREC.COMMON_ROLES_1) && !roleType.equals(CM_LOVREC.COMMON_ROLES_2) && !roleType.equals(CM_LOVREC.COMMON_ROLES_3) && !roleType.equals(CM_LOVREC.COMMON_ROLES_4)) {
					getErrorMap().setError("roleCode", BackOfficeErrorCodes.INSUFFICIENT_ROLE_RIGHTS);
					return false;
				}
			}
			if (context.isInternalOperations()) {
				if (!roleType.equals(CM_LOVREC.COMMON_ROLES_5) && !roleType.equals(CM_LOVREC.COMMON_ROLES_6)) {
					getErrorMap().setError("roleCode", BackOfficeErrorCodes.INSUFFICIENT_ROLE_RIGHTS);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("roleCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateFromDate() {
		CommonValidator validation = new CommonValidator();
		TBAActionType AT;
		try {
			if (getAction().equals(ADD))
				AT = TBAActionType.ADD;
			else
				AT = TBAActionType.MODIFY;
			if (validation.isEmpty(fromDate)) {
				getErrorMap().setError("fromDate", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (validation.isEffectiveDateValid(fromDate, AT)) {
				return true;
			} else {
				getErrorMap().setError("fromDate", BackOfficeErrorCodes.INVALID_DATE);
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			validation.close();
		}
	}

	public boolean validateToDate() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(toDate)) {
				getErrorMap().setError("toDate", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (validation.isValidDateUptoDate(toDate, fromDate)) {
				return true;
			} else {
				getErrorMap().setError("toDate", BackOfficeErrorCodes.INVALID_DATE);
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			validation.close();
		}
	}

	public boolean validatePK() {
		AccessValidator validation = new AccessValidator();
		HashMap<Integer, DBEntry> whereClauseFieldsMap = new HashMap<Integer, DBEntry>();
		whereClauseFieldsMap.put(1, new DBEntry("ENTITY_CODE", context.getEntityCode(), BindParameterType.VARCHAR));
		whereClauseFieldsMap.put(2, new DBEntry("USER_ID", userID, BindParameterType.VARCHAR));
		whereClauseFieldsMap.put(3, new DBEntry("ROLE_CODE", roleCode, BindParameterType.VARCHAR));
		boolean isRecordAvailable = validation.isRecordAvailable("USERSACTROLEALLOC", whereClauseFieldsMap);
		try {
			if (isRecordAvailable) {
				if (getAction().equals(ADD)) {
					getErrorMap().setError("userID", BackOfficeErrorCodes.RECORD_EXISTS);
					return false;
				}
				return true;
			} else {

				if (getAction().equals(MODIFY)) {
					getErrorMap().setError("userID", BackOfficeErrorCodes.RECORD_NOT_EXISTS);
					return false;
				}
				return true;

			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			validation.close();
		}
		return true;
	}
}
