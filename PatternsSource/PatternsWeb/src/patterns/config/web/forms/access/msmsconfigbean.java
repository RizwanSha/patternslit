package patterns.config.web.forms.access;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.process.TBAActionType;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.validations.CommonValidator;
import patterns.config.web.forms.GenericFormBean;

public class msmsconfigbean extends GenericFormBean {

	private static final long serialVersionUID = 1L;

	private String effectiveDate;
	private boolean authReqd;
	private String serverURL;
	private String userNameKey;
	private String userNameValue;
	private String pwdKey;
	private String pwdValue;
	private String mobileNumkey;
	private String textKey;

	public String getTextKey() {
		return textKey;
	}

	public void setTextKey(String textKey) {
		this.textKey = textKey;
	}

	public String getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public String getServerURL() {
		return serverURL;
	}

	public void setServerURL(String serverURL) {
		this.serverURL = serverURL;
	}

	public boolean isAuthReqd() {
		return authReqd;
	}

	public void setAuthReqd(boolean authReqd) {
		this.authReqd = authReqd;
	}

	public String getUserNameKey() {
		return userNameKey;
	}

	public void setUserNameKey(String userNameKey) {
		this.userNameKey = userNameKey;
	}

	public String getUserNameValue() {
		return userNameValue;
	}

	public void setUserNameValue(String userNameValue) {
		this.userNameValue = userNameValue;
	}

	public String getPwdKey() {
		return pwdKey;
	}

	public void setPwdKey(String pwdKey) {
		this.pwdKey = pwdKey;
	}

	public String getPwdValue() {
		return pwdValue;
	}

	public void setPwdValue(String pwdValue) {
		this.pwdValue = pwdValue;
	}

	public String getMobileNumkey() {
		return mobileNumkey;
	}

	public void setMobileNumkey(String mobileNumkey) {
		this.mobileNumkey = mobileNumkey;
	}

	@Override
	public void reset() {
		effectiveDate = RegularConstants.EMPTY_STRING;
		authReqd = false;
		serverURL = RegularConstants.EMPTY_STRING;
		userNameKey = RegularConstants.EMPTY_STRING;
		userNameValue = RegularConstants.EMPTY_STRING;
		pwdKey = RegularConstants.EMPTY_STRING;
		pwdValue = RegularConstants.EMPTY_STRING;
		mobileNumkey = RegularConstants.EMPTY_STRING;
		textKey = RegularConstants.EMPTY_STRING;
		setProcessBO("patterns.config.framework.bo.access.msmsconfigBO");
	}

	public void validate() {
		if (validateEffectiveDate()) {
			validateServerURL();
			if (authReqd) {
				validateUserNameKey();
				validateUserNameValue();
				validatePwdKey();
				validatePwdValue();
			}
			validateMobileNumKey();
			validateTextKey();
		}

		if (getErrorMap().length() == 0) {
			DTObject formDTO = getFormDTO();
			formDTO.set("ENTITY_CODE", context.getEntityCode());
			formDTO.setObject("EFFT_DATE", BackOfficeFormatUtils.getDate(effectiveDate, context.getDateFormat()));
			formDTO.set("SERVER_URL", serverURL);
			formDTO.set("AUTH_REQD", decodeBooleanToString(authReqd));
			if (authReqd) {
				formDTO.set("USER_NAME_KEY", userNameKey);
				formDTO.set("USER_NAME_VALUE", userNameValue);
				formDTO.set("PASSWORD_KEY", pwdKey);
				formDTO.set("PASSWORD_VALUE", pwdValue);
			} else {
				formDTO.set("USER_NAME_KEY", null);
				formDTO.set("USER_NAME_VALUE", null);
				formDTO.set("PASSWORD_KEY", null);
				formDTO.set("PASSWORD_VALUE", null);
			}

			formDTO.set("MOBILE_NUMBER_KEY", mobileNumkey);
			formDTO.set("TEXT_KEY", textKey);
		}
	}

	public boolean validateEffectiveDate() {

		CommonValidator validation = new CommonValidator();
		try {
			TBAActionType AT;
			if (getAction().equals(ADD))
				AT = TBAActionType.ADD;
			else
				AT = TBAActionType.MODIFY;
			if (!validation.isEffectiveDateValid(effectiveDate, AT)) {
				getErrorMap().setError("effectiveDate", BackOfficeErrorCodes.INVALID_CODE);
				return false;
			}
		} catch (Exception e) {

		} finally {
			validation.close();
		}
		return true;
	}

	public boolean validateServerURL() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(serverURL)) {
				getErrorMap().setError("serverURL", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidUrl(serverURL)) {
				getErrorMap().setError("serverURL", BackOfficeErrorCodes.INVALID_URL);
				return false;
			}
		} catch (Exception e) {
		} finally {
			validation.close();
		}
		return true;
	}

	public boolean validateMobileNumKey() {

		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(mobileNumkey)) {
				getErrorMap().setError("mobileNumkey", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidKey(mobileNumkey)) {
				getErrorMap().setError("mobileNumkey", BackOfficeErrorCodes.INVALID_KEY);
				return false;
			}
		} catch (Exception e) {
		} finally {
			validation.close();
		}
		return true;
	}

	public boolean validateTextKey() {

		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(textKey)) {
				getErrorMap().setError("textKey", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidValue(textKey)) {
				getErrorMap().setError("textKey", BackOfficeErrorCodes.INVALID_VALUE);
				return false;
			}
		} catch (Exception e) {
		} finally {
			validation.close();
		}
		return true;
	}

	public boolean validateUserNameKey() {

		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(userNameKey)) {
				getErrorMap().setError("userNameKey", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidKey(userNameKey)) {
				getErrorMap().setError("userNameKey", BackOfficeErrorCodes.INVALID_KEY);
				return false;
			}
		} catch (Exception e) {
		} finally {
			validation.close();
		}
		return true;
	}

	public boolean validateUserNameValue() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(userNameValue)) {
				getErrorMap().setError("userNameValue", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidValue(userNameValue)) {
				getErrorMap().setError("userNameValue", BackOfficeErrorCodes.INVALID_VALUE);
				return false;
			}
		} catch (Exception e) {
		} finally {
			validation.close();
		}
		return true;
	}

	public boolean validatePwdKey() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(pwdKey)) {
				getErrorMap().setError("pwdKey", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidKey(pwdKey)) {
				getErrorMap().setError("pwdKey", BackOfficeErrorCodes.INVALID_KEY);
				return false;
			}
		} catch (Exception e) {
		} finally {
			validation.close();
		}
		return true;
	}

	public boolean validatePwdValue() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(pwdValue)) {
				getErrorMap().setError("pwdValue", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidValue(pwdValue)) {
				getErrorMap().setError("pwdValue", BackOfficeErrorCodes.INVALID_VALUE);
				return false;
			}
		} catch (Exception e) {
		} finally {
			validation.close();
		}
		return true;
	}

}
