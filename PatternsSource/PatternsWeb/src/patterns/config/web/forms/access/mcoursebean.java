package patterns.config.web.forms.access;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.service.DTObject;
import patterns.config.validations.CommonValidator;
import patterns.config.web.forms.GenericFormBean;

public class mcoursebean extends GenericFormBean {

	private static final long serialVersionUID = -6219657363150466160L;

	private String courseId;
	private String courseTitle;
	private String courseDescn;
	private String position;
	private boolean enabled;
	private String remarks;

	public String getRemarks() {
		return remarks;
	}

	

	public boolean isEnabled() {
		return enabled;
	}

	@Override
	public void reset() {
		courseId = RegularConstants.EMPTY_STRING;
		courseTitle = RegularConstants.EMPTY_STRING;
		courseDescn = RegularConstants.EMPTY_STRING;
		position = RegularConstants.EMPTY_STRING;
		enabled = false;
		remarks = RegularConstants.EMPTY_STRING;
		
		setProcessBO("patterns.config.framework.bo.access.mcourseBO");
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	
	@Override
	public void validate() {
		
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.set("CODE", courseId);
				formDTO.set("COURSE_TITLE", courseTitle);
				formDTO.set("COURSE_DESCN", courseDescn);
				if (getAction().equals(ADD)) {
					formDTO.set("ACTIVE", decodeBooleanToString(true));
				} else {
					formDTO.set("ACTIVE", decodeBooleanToString(enabled));
				}
				formDTO.set("COURSE_POSITION", position);
				formDTO.set("REMARKS", remarks);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("roleCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
	}

	public boolean validateRemarks() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(remarks)) {
				getErrorMap().setError("remarks", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidRemarks(remarks)) {
				getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	


	public String getCourseId() {
		return courseId;
	}



	public void setCourseId(String courseId) {
		this.courseId = courseId;
	}



	public String getCourseTitle() {
		return courseTitle;
	}



	public void setCourseTitle(String courseTitle) {
		this.courseTitle = courseTitle;
	}



	public String getCourseDescn() {
		return courseDescn;
	}



	public void setCourseDescn(String courseDescn) {
		this.courseDescn = courseDescn;
	}



	public String getPosition() {
		return position;
	}



	public void setPosition(String position) {
		this.position = position;
	}
}