package patterns.config.web.forms.access;

import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.Set;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.BindParameterType;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.objects.TableInfo;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBInfo;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.framework.web.DHTMLXGridUtility;
import patterns.config.framework.web.GenericOption;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.AccessValidator;
import patterns.config.validations.CommonValidator;
import patterns.config.web.forms.GenericFormBean;

public class qauditlogdtlshistbean extends GenericFormBean {
	private TableInfo tableInfo = null;

	private static final long serialVersionUID = -6219657363150466160L;
	private String hidPrimaryKey;
	private String fromTable;
	private String actionType;
	private String detailTableSerial;
	private String viewType;

	public String getViewType() {
		return viewType;
	}

	public void setViewType(String viewType) {
		this.viewType = viewType;
	}

	public String getDetailTableSerial() {
		return detailTableSerial;
	}

	public void setDetailTableSerial(String detailTableSerial) {
		this.detailTableSerial = detailTableSerial;
	}

	public TableInfo getTableInfo() {
		return tableInfo;
	}

	public void setTableInfo(TableInfo tableInfo) {
		this.tableInfo = tableInfo;
	}

	public String getActionType() {
		return actionType;
	}

	public void setActionType(String actionType) {
		this.actionType = actionType;
	}

	public String getFromTable() {
		return fromTable;
	}

	public void setFromTable(String fromTable) {
		this.fromTable = fromTable;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getHidPrimaryKey() {
		return hidPrimaryKey;
	}

	public void setHidPrimaryKey(String hidPrimaryKey) {
		this.hidPrimaryKey = hidPrimaryKey;
	}

	public void reset() {
		fromTable = RegularConstants.EMPTY_STRING;
		detailTableSerial = RegularConstants.EMPTY_STRING;
		actionType = RegularConstants.EMPTY_STRING;

		String pk = webContext.getRequest().getParameter("pk");
		if (pk != null) {
			hidPrimaryKey = pk;
		}
		DBContext dbContext = new DBContext();
		ArrayList<GenericOption> Serial = null;
		Serial = getGenericOptions("QAUDITLOGDTLSHIST", "SERIAL", dbContext, null);
		webContext.getRequest().setAttribute("QAUDITLOGDTLSHIST_SERIAL", Serial);
		ArrayList<GenericOption> dtlSerial = null;
		dtlSerial = getGenericOptions("QAUDITLOGDTLSHIST", "DTL_SERIAL", dbContext, null);
		webContext.getRequest().setAttribute("QAUDITLOGDTLSHIST_DTL_SERIAL", dtlSerial);
		ArrayList<GenericOption> actionTypeList = getGenericOptions("COMMON", "AUDITLOG", dbContext, "--Action Type--");
		webContext.getRequest().setAttribute("COMMON_QAUDITLOG", actionTypeList);
		ArrayList<GenericOption> viewDtls = null;
		viewDtls = getGenericOptions("COMMON", "VIEW_TYPE", dbContext, "- View Type -");
		webContext.getRequest().setAttribute("COMMON_VIEW_TYPE", viewDtls);
		dbContext.close();
	}

	public DTObject getTableDetails(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		DTObject formDTO = new DTObject();
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		String sqlQuery = RegularConstants.EMPTY_STRING;
		String[] pkInfo = inputDTO.get("PK").split(",");
		String tableSl = RegularConstants.EMPTY_STRING;
		String tableName = RegularConstants.EMPTY_STRING;
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		AccessValidator validation = new AccessValidator();
		Date date = new java.sql.Date(BackOfficeFormatUtils.getDate(pkInfo[3], BackOfficeConstants.JAVA_DATE_FORMAT).getTime());
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		Integer x = c.get(Calendar.YEAR);
		String year = x.toString();

		sqlQuery = ("SELECT ATLOGD_DTL_SL,ATLOGD_TABLE_NAME FROM AUDITLOGDTL" + year + " WHERE ENTITY_CODE=? AND ATLOGD_FORM_NAME=? AND ATLOGD_PK=? AND ATLOGD_TABLE_NAME !='FWRKOPERATIONS' GROUP BY ATLOGD_TABLE_NAME ORDER BY ATLOGD_TABLE_NAME ASC ");
		try {
			util.setMode(DBUtil.PREPARED);
			util.setSql(sqlQuery.toString());
			util.setString(1, context.getEntityCode());
			util.setString(2, pkInfo[0]);
			util.setString(3, pkInfo[2]);
			ResultSet rs = util.executeQuery();
			if (rs.next()) {
				do {
					tableSl = tableSl + RegularConstants.PK_SEPARATOR + rs.getString(2);
					tableName = tableName + RegularConstants.PK_SEPARATOR + rs.getString(2);
				} while (rs.next());
			}
			resultDTO.set("TABLE_SL", tableSl);
			resultDTO.set("TABLE_NAME", tableName);
			inputDTO.set("MPGM_ID", pkInfo[0]);
			inputDTO.set(ContentManager.FETCH, ContentManager.FETCH_ALL);
			formDTO = validation.validateOption(inputDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, formDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			resultDTO.set("MODULE_NAME", formDTO.get("MPGM_MODULE"));
			resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			util.reset();
			dbContext.close();
			validation.close();
		}
		return resultDTO;
	}

	public DTObject getDtlTableDetails(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		AccessValidator validation = new AccessValidator();
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		String[] pkInfo = inputDTO.get("PK").split(",");
		String fromTable = inputDTO.get("FROM_TABLE");
		String actionType = inputDTO.get("AUCTION_TYPE");
		try {
			inputDTO.set("MPGM_ID", pkInfo[0]);
			inputDTO.set(ContentManager.FETCH, ContentManager.FETCH_ALL);
			resultDTO = validation.validateOption(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_RECORD_NOT_EXISTS);
				return resultDTO;
			}
			if (ContentManager.DATA_AVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				if (!fromTable.equals(resultDTO.get("MPGM_TABLE_NAME"))) {
					StringBuffer buffer = new StringBuffer();
					String tableSl = RegularConstants.EMPTY_STRING;
					String tableName = RegularConstants.EMPTY_STRING;
					String userID = RegularConstants.EMPTY_STRING;

					Date fromYear = new java.sql.Date(BackOfficeFormatUtils.getDate(pkInfo[3], BackOfficeConstants.JAVA_DATE_FORMAT).getTime());
					Date uptoYear = new java.sql.Date(BackOfficeFormatUtils.getDate(pkInfo[4], BackOfficeConstants.JAVA_DATE_FORMAT).getTime());
					Calendar c = Calendar.getInstance();
					Calendar c1 = Calendar.getInstance();
					c.setTime(fromYear);
					c1.setTime(uptoYear);
					Integer x = c.get(Calendar.YEAR);
					Integer y = c1.get(Calendar.YEAR);
					String fromYear1 = x.toString();
					String uptoYear1 = y.toString();

					while (Integer.parseInt(fromYear1) <= Integer.parseInt(uptoYear1)) {
						buffer = new StringBuffer();
						buffer.append("SELECT A.ATLOG_LOG_SL,A.ATLOG_ACTION,A.ATLOG_USER_ID,TO_CHAR(A.ATLOG_DATE,'" + BackOfficeConstants.TBA_DATE_FORMAT + "') ATLOG_DATE FROM AUDITLOG" + fromYear1 + " A WHERE A.ENTITY_CODE=? AND A.ATLOG_FORM_NAME=? AND A.ATLOG_PK=? ");
						if (!actionType.equals("X"))
							buffer.append("AND A.ATLOG_ACTION= ? ");
						buffer.append(" GROUP BY ATLOG_LOG_SL ORDER BY ATLOG_LOG_SL ASC ");
						util.reset();
						util.setSql(buffer.toString());
						util.setString(1, context.getEntityCode());
						util.setString(2, pkInfo[0]);
						util.setString(3, pkInfo[2]);
						if (!actionType.equals("X"))
							util.setString(4, actionType);
						ResultSet rs = util.executeQuery();
						if (rs.next()) {
							do {
								if (rs.getString(2).equals("A"))
									userID = " Created By ";
								else
									userID = " Modified By ";
								if (tableSl.isEmpty()) {
									tableSl = rs.getString(1);
									tableName = rs.getString(1) + RegularConstants.DATE_SEPARATOR + userID + RegularConstants.TIME_SEPARATOR + rs.getString(3) + RegularConstants.SINGLE_SPACE + rs.getString(4);
								} else {
									tableSl += RegularConstants.PK_SEPARATOR + rs.getString(1);
									tableName += RegularConstants.PK_SEPARATOR + rs.getString(1) + RegularConstants.DATE_SEPARATOR + userID + RegularConstants.TIME_SEPARATOR + rs.getString(3) + RegularConstants.SINGLE_SPACE + rs.getString(4);
								}
							} while (rs.next());
						}
						fromYear1 = String.valueOf(Integer.parseInt(fromYear1) + 1);
					}
					resultDTO.set("DTL_TABLE_SL", tableSl);
					resultDTO.set("DTL_TABLE_NAME", tableName);
				}
			}
			resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			util.reset();
			dbContext.close();
			validation.close();
		}
		return resultDTO;
	}

	public DTObject loadAuditLogDetails(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		StringBuffer buffer = new StringBuffer();
		DBContext dbContext = new DBContext();
		String sqlQuery = null;
		String sql = null;
		String xml = null;
		DBUtil util = dbContext.createUtilInstance();
		DBUtil util5 = dbContext.createUtilInstance();
		DBUtil util1 = dbContext.createUtilInstance();
		StringBuffer query = new StringBuffer();
		String mtable = inputDTO.get("FROM_TABLE");
		String dtlSerial = inputDTO.get("DTL_SERIAL");
		String actionType = inputDTO.get("AUCTION_TYPE");
		String viewType = inputDTO.get("VIEW_TYPE");
		String[] pkInfo = inputDTO.get("PK").split(",");
		CommonValidator validation = new CommonValidator();
		String serial = null;
		String checkNewOld = null;
		int newRecords = 0;
		int oldRecords = 0;
		try {
			util.reset();
			util.setMode(DBUtil.CALLABLE);
			util.setSql("CALL SP_AUDITLOGDTL(?,?,?,?,?,?,?,?,?)");
			util.setString(1, context.getEntityCode());
			util.setString(2, pkInfo[0]);
			util.setString(3, pkInfo[2]);
			util.setDate(4, new java.sql.Date(BackOfficeFormatUtils.getDate(pkInfo[3], BackOfficeConstants.JAVA_DATE_FORMAT).getTime()));
			util.setDate(5, new java.sql.Date(BackOfficeFormatUtils.getDate(pkInfo[4], BackOfficeConstants.JAVA_DATE_FORMAT).getTime()));
			if (validation.isEmpty(actionType))
				util.setString(6, RegularConstants.NULL);
			else
				util.setString(6, actionType);
			if (validation.isEmpty(dtlSerial))
				util.setString(7, RegularConstants.NULL);
			else
				util.setString(7, dtlSerial);
			util.registerOutParameter(8, Types.VARCHAR);
			util.registerOutParameter(9, Types.VARCHAR);
			util.execute();
			serial = util.getString(8);
			int tableCount = Integer.parseInt(util.getString(9));
			if (tableCount > 0) {
				sql = " SELECT ATLOGDTL_IMAGE_TYPE FROM RTMPAUDITLOGDTL  WHERE ENTITY_CODE=? AND INV_NO =? GROUP BY ATLOGDTL_IMAGE_TYPE ORDER BY ATLOGDTL_IMAGE_TYPE ASC ";
				util5.reset();
				util5.setSql(sql.toString());
				util5.setString(1, context.getEntityCode());
				util5.setString(2, serial);
				ResultSet rrr = util5.executeQuery();
				while (rrr.next()) {
					checkNewOld = rrr.getString("ATLOGDTL_IMAGE_TYPE");
					if (checkNewOld.equals("N"))
						newRecords++;
					else if (checkNewOld.equals("O"))
						oldRecords++;

					buffer = new StringBuffer();
					query = new StringBuffer();
					DBInfo info = new DBInfo();
					buffer.append("( SELECT ");
					tableInfo = info.getTableInfo(mtable);
					Set<String> noUpdateColumnInfo = tableInfo.getNoUpdationColumnInfo();
					String[] fieldNames = tableInfo.getColumnInfo().getColumnNames();
					Map<String, BindParameterType> columnType = tableInfo.getColumnInfo().getColumnMapping();
					for (String fieldName : fieldNames) {
						if (!noUpdateColumnInfo.contains(fieldName)) {
							if (columnType.get(fieldName).equals(BindParameterType.DATE)) {
								query.append(" TO_CHAR(TO_DATE(EXTRACTVALUE(T.ATLOGDTL_DATA,'//" + fieldName + "'),'" + BackOfficeConstants.TBA_XML_DATE_FORMAT + "'),'" + BackOfficeConstants.TBA_DATE_FORMAT + "')" + fieldName + ",");
							} else if (columnType.get(fieldName).equals(BindParameterType.DATETIME)) {
								query.append(" TO_CHAR(TO_DATE(EXTRACTVALUE(T.ATLOGDTL_DATA,'//" + fieldName + "'),'" + BackOfficeConstants.TBA_XML_DATE_FORMAT + "'),'" + BackOfficeConstants.TBA_XML_DATE_FORMAT + "')" + fieldName + ",");
							} else {
								query.append(" EXTRACTVALUE(T.ATLOGDTL_DATA,'//" + fieldName + "')" + fieldName + ",");
							}
						}
					}
					query.trimToSize();
					buffer.append(query.substring(0, query.length() - 1));
					buffer.append(" ,T.ATLOGDTL_LOG_SL FROM RTMPAUDITLOGDTL T WHERE T.ENTITY_CODE=? AND T.INV_NO =? AND T.TABLE_NAME =? AND T.ATLOGDTL_IMAGE_TYPE =? ORDER BY ATLOGDTL_LOG_SL ASC ) ");
					util.reset();
					util.setSql(buffer.toString());
					util.setString(1, context.getEntityCode());
					util.setString(2, serial);
					util.setString(3, mtable);
					util.setString(4, checkNewOld);
					ResultSet rs = util.executeQuery();
					String[] columnComments = info.getTableInfo(mtable).getColumnInfo().getColumnComments();
					String[] columnName = info.getTableInfo(mtable).getColumnInfo().getColumnNames();
					String[] primaryKeyInfo = tableInfo.getPrimaryKeyInfo().getColumnNames();

					String[] removeComments = new String[] { "Entity Code", "Enabled", "Tba Key", "partition No", "Code in Use", "E Status", "Auth On", "Rej On" };
					String[] removeColumns = new String[] { "ENTITY_CODE", "ENABLED", "TBA_KEY", "PARTITION_NO", "CODE_IN_USE ", "E_STATUS", "AUTH_ON", "REJ_ON" };
					DHTMLXGridUtility table = new DHTMLXGridUtility();
					table.init();
					table.startHead();
					table.setColumn("Serial", "45", "ro", DHTMLXGridUtility.ColumnAlignment.LEFT);
					int newCount = 0;
					int placeHolderCount = 0;
					ArrayList<Float> columnsSize = new ArrayList<Float>();

					if (viewType.equals("A")) {
						for (int i = 0; i < columnComments.length; i++) {
							if (!Arrays.asList(removeColumns).contains(columnName[newCount++])) {
								if (!Arrays.asList(removeComments).contains(columnComments[i])) {
									if (validation.isEmpty(columnComments[i])) {
										columnsSize.add(placeHolderCount, (float) (rs.getMetaData().getColumnLabel(i + 1).length() * 8.7));
										table.setColumn(rs.getMetaData().getColumnLabel(i + 1), "{" + placeHolderCount + "}", "ro", DHTMLXGridUtility.ColumnAlignment.LEFT);
									} else {
										columnsSize.add(placeHolderCount, (float) (columnComments[i].length() * 8.7));
										table.setColumn(columnComments[i], "{" + placeHolderCount + "}", "ro", DHTMLXGridUtility.ColumnAlignment.LEFT);
									}
									placeHolderCount++;
								}
							}
						}
					} else if (viewType.equals("P")) {
						for (int i = 0; i < columnComments.length; i++) {
							if (!Arrays.asList(removeColumns).contains(columnName[newCount++])) {
								if (!Arrays.asList(primaryKeyInfo).contains(columnName[i])) {
									if (!Arrays.asList(removeComments).contains(columnComments[i])) {
										if (validation.isEmpty(columnComments[i])) {
											columnsSize.add(placeHolderCount, (float) (rs.getMetaData().getColumnLabel(i + 1).length() * 8.7));
											table.setColumn(rs.getMetaData().getColumnLabel(i + 1), "{" + placeHolderCount + "}", "ro", DHTMLXGridUtility.ColumnAlignment.LEFT);
										} else {
											columnsSize.add(placeHolderCount, (float) (columnComments[i].length() * 8.7));
											table.setColumn(columnComments[i], "{" + placeHolderCount + "}", "ro", DHTMLXGridUtility.ColumnAlignment.LEFT);
										}
										placeHolderCount++;
									}
								}
							}
						}
					} else if (viewType.equals("O")) {
						for (int i = 0; i < columnComments.length; i++) {
							if (!Arrays.asList(removeColumns).contains(columnName[newCount++])) {
								if (!Arrays.asList(removeComments).contains(columnComments[i])) {
									if (validation.isEmpty(columnComments[i])) {
										columnsSize.add(placeHolderCount, (float) (rs.getMetaData().getColumnLabel(i + 1).length() * 8.7));
										table.setColumn(rs.getMetaData().getColumnLabel(i + 1), "{" + placeHolderCount + "}", "ro", DHTMLXGridUtility.ColumnAlignment.LEFT);
									} else {
										columnsSize.add(placeHolderCount, (float) (columnComments[i].length() * 8.7));
										table.setColumn(columnComments[i], "{" + placeHolderCount + "}", "ro", DHTMLXGridUtility.ColumnAlignment.LEFT);
									}
									placeHolderCount++;
								}
							}
						}
					}

					table.setColumn("Created By", "90", "ro", DHTMLXGridUtility.ColumnAlignment.LEFT);
					table.setColumn("Created On", "135", "ro", DHTMLXGridUtility.ColumnAlignment.LEFT);
					table.setColumn("Modified By", "92", "ro", DHTMLXGridUtility.ColumnAlignment.LEFT);
					table.setColumn("Modified On", "140", "ro", DHTMLXGridUtility.ColumnAlignment.LEFT);
					table.setColumn("Authorized By", "92", "ro", DHTMLXGridUtility.ColumnAlignment.LEFT);
					table.setColumn("Authorized On", "140", "ro", DHTMLXGridUtility.ColumnAlignment.LEFT);
					table.endHead();
					String crOn = RegularConstants.EMPTY_STRING;
					String crBy = RegularConstants.EMPTY_STRING;
					String auOn = RegularConstants.EMPTY_STRING;
					String auBy = RegularConstants.EMPTY_STRING;
					String moOn = RegularConstants.EMPTY_STRING;
					String moBy = RegularConstants.EMPTY_STRING;
					String auction = RegularConstants.EMPTY_STRING;
					ArrayList<String> preRow = new ArrayList<String>();

					Integer count = 0;
					Integer sl = 1;
					while (rs.next()) {
						count++;
						table.startRow();
						table.setCell(String.valueOf(sl++));
						int sizeCount = 0;
						if (validation.isEmpty(dtlSerial)) {
							if (viewType.equals("A")) {
								for (int j = 1; j < rs.getMetaData().getColumnCount(); j++) {
									if (!Arrays.asList(removeColumns).contains(rs.getMetaData().getColumnLabel(j))) {
										if (count > 1 && !preRow.get(j - 1).equals(rs.getString(j))) {
											table.setCell(rs.getString(j), "style", " color:red ");
										} else {
											table.setCell(rs.getString(j));
										}

										if (rs.getString(j) != null) {
											if (rs.getString(j).length() * 8.7 > columnsSize.get(sizeCount))
												columnsSize.set(sizeCount, (float) (rs.getString(j).length() * 8.7));
										}
										sizeCount++;
									}
									if (preRow.size() > j)
										preRow.remove(j - 1);
									if (rs.getString(j) != null)
										preRow.add(j - 1, rs.getString(j));
									else
										preRow.add(j - 1, "");
								}
							} else if (viewType.equals("P")) {
								for (int j = 1; j < rs.getMetaData().getColumnCount(); j++) {
									if (!Arrays.asList(primaryKeyInfo).contains(rs.getMetaData().getColumnLabel(j))) {
										if (!Arrays.asList(removeColumns).contains(rs.getMetaData().getColumnLabel(j))) {
											if (count > 1 && !preRow.get(j - 1).equals(rs.getString(j))) {
												table.setCell(rs.getString(j), "style", " color:red ");
											} else {
												table.setCell(rs.getString(j));
											}
											if (rs.getString(j) != null) {
												if (rs.getString(j).length() * 8.7 > columnsSize.get(sizeCount))
													columnsSize.set(sizeCount, (float) (rs.getString(j).length() * 8.7));
											}
											sizeCount++;
										}
									}
									if (preRow.size() > j)
										preRow.remove(j - 1);
									if (rs.getString(j) != null)
										preRow.add(j - 1, rs.getString(j));
									else
										preRow.add(j - 1, "");
								}
							} else if (viewType.equals("O")) {
								for (int j = 1; j < rs.getMetaData().getColumnCount(); j++) {
									if (!Arrays.asList(removeColumns).contains(rs.getMetaData().getColumnLabel(j))) {
										if (count == 1)
											columnsSize.set(sizeCount, (float) 0);
										if (count > 1 && !preRow.get(j - 1).equals(rs.getString(j))) {
											table.setCell(rs.getString(j), "style", " color:red ");
											if (rs.getString(j) != null) {
												if (rs.getString(j).length() * 8.7 > columnsSize.get(sizeCount))
													columnsSize.set(sizeCount, (float) (rs.getString(j).length() * 8.7));
											}
										} else {
											table.setCell(rs.getString(j));
										}
										sizeCount++;
									}
									if (preRow.size() > j)
										preRow.remove(j - 1);
									if (rs.getString(j) != null)
										preRow.add(j - 1, rs.getString(j));
									else
										preRow.add(j - 1, "");
								}
							}
						} else if (!validation.isEmpty(dtlSerial)) {
							for (int j = 1; j < rs.getMetaData().getColumnCount(); j++) {
								if (!Arrays.asList(removeColumns).contains(rs.getMetaData().getColumnLabel(j))) {
									table.setCell(rs.getString(j));
								}
								if (rs.getString(j) != null && columnsSize.size() > sizeCount) {
									if (rs.getString(j).length() * 8.7 > columnsSize.get(sizeCount))
										columnsSize.set(sizeCount, (float) (rs.getString(j).length() * 8.7));
								}
								sizeCount++;
							}
						}

						sqlQuery = " SELECT EXTRACTVALUE(T.ATLOGDTL_DATA, '/ROWS/ROW[1]/OPER_TYPE') TYPE1,TO_CHAR(EXTRACTVALUE(T.ATLOGDTL_DATA, '/ROWS/ROW[1]/OPER_DATETIME'),'%d-%m-%Y %H:%i:%s') AS DATETIME1,EXTRACTVALUE(T.ATLOGDTL_DATA, '/ROWS/ROW[1]/OPER_USER_ID') USER1,EXTRACTVALUE(T.ATLOGDTL_DATA, '/ROWS/ROW[2]/OPER_TYPE') TYPE2,TO_CHAR(EXTRACTVALUE(T.ATLOGDTL_DATA, '/ROWS/ROW[2]/OPER_DATETIME'),'%d-%m-%Y %H:%i:%s') AS DATETIME2,EXTRACTVALUE(T.ATLOGDTL_DATA, '/ROWS/ROW[2]/OPER_USER_ID') USER2,EXTRACTVALUE(T.ATLOGDTL_DATA, '/ROWS/ROW[3]/OPER_TYPE') TYPE3,IF(EXTRACTVALUE(T.ATLOGDTL_DATA,'/ROWS/ROW[3]/OPER_DATETIME') = '' ,'',TO_CHAR(EXTRACTVALUE(T.ATLOGDTL_DATA,'/ROWS/ROW[3]/OPER_DATETIME'),'%d-%m-%Y %H:%i:%s')) AS DATETIME3,EXTRACTVALUE(T.ATLOGDTL_DATA, '/ROWS/ROW[3]/OPER_USER_ID') USER3 , T.ATLOGTL_ACTION  FROM RTMPAUDITLOGDTL T "
								+ " WHERE ENTITY_CODE=? AND INV_NO =? AND T.TABLE_NAME ='FWRKOPERATIONS' AND T.ATLOGDTL_LOG_SL=? AND T.ATLOGDTL_IMAGE_TYPE =?  ORDER BY ATLOGDTL_LOG_SL ASC ";
						util1.reset();
						util1.setSql(sqlQuery.toString());
						util1.setString(1, context.getEntityCode());
						util1.setString(2, serial);
						util1.setString(3, rs.getString("ATLOGDTL_LOG_SL"));
						util1.setString(4, checkNewOld);
						ResultSet rset = util1.executeQuery();
						if (rset.next()) {
							auOn = rset.getString("DATETIME1");
							auBy = rset.getString("USER1");
							crOn = rset.getString("DATETIME2");
							crBy = rset.getString("USER2");
							moOn = rset.getString("DATETIME3");
							moBy = rset.getString("USER3");
							auction = rset.getString("ATLOGTL_ACTION");
						}
						table.setCell(crBy);
						table.setCell(crOn);
						if (validation.isEmpty(dtlSerial)) {
							table.setCell(moBy, "style", "font-weight: bold; color:red ");
							table.setCell(moOn, "style", "font-weight: bold; color:red ");
						} else {
							table.setCell(moBy);
							table.setCell(moOn);
						}
						if (validation.isEmpty(dtlSerial) && auction.equals("M")) {
							table.setCell(auBy, "style", "font-weight: bold; color:red ");
							table.setCell(auOn, "style", "font-weight: bold; color:red ");
						} else {
							table.setCell(auBy);
							table.setCell(auOn);
						}
						table.endRow();
					}
					table.finish();
					xml = table.getXML();
					for (int k = 0; k < columnsSize.size(); k++) {
						xml = xml.replace("{" + k + "}", columnsSize.get(k).toString());
					}
					if (dtlSerial.equals(RegularConstants.EMPTY_STRING)) {
						resultDTO.set("GRID_DTL", xml);
					} else {
						if (checkNewOld.equals("N"))
							resultDTO.set("GRID_DTL", xml);
						else if (checkNewOld.equals("O"))
							resultDTO.set("GRID", xml);
					}
					if (checkNewOld.equals("N"))
						newRecords = count;
					else if (checkNewOld.equals("O"))
						oldRecords = count;
					resultDTO.set("ROWS_DIFF", Integer.toString((newRecords - oldRecords)));

				}
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
			} else if (String.valueOf(tableCount).equals(RegularConstants.ZERO)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_RECORD_NOT_EXISTS);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError(" getTableName(1) - " + e.getLocalizedMessage());
			buffer.append("");
		} finally {
			util.reset();
			util1.reset();
			util5.reset();
			dbContext.close();
			validation.close();
		}
		return resultDTO;
	}

	public void validate() {
	}

}