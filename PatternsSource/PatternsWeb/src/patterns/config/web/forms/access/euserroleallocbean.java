package patterns.config.web.forms.access;

import java.util.HashMap;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.BindParameterType;
import patterns.config.framework.database.DBEntry;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.process.TBAActionType;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.AccessValidator;
import patterns.config.validations.CommonValidator;
import patterns.config.web.forms.GenericFormBean;

public class euserroleallocbean extends GenericFormBean {

	private static final long serialVersionUID = 1L;

	private String userID;
	private String effectiveDate;
	private String roleCode;
	private String remarks;
	private String currentRoleCode;

	public String getCurrentRoleCode() {
		return currentRoleCode;
	}

	public void setCurrentRoleCode(String currentRoleCode) {
		this.currentRoleCode = currentRoleCode;
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public String getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public String getRoleCode() {
		return roleCode;
	}

	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public void reset() {
		userID = RegularConstants.EMPTY_STRING;
		effectiveDate = RegularConstants.EMPTY_STRING;
		roleCode = RegularConstants.EMPTY_STRING;
		remarks = RegularConstants.EMPTY_STRING;
		setProcessBO("patterns.config.framework.bo.access.euserroleallocBO");
	}

	public void validate() {
		if (validateUserID()) {
			if (validateEffectiveDate()) {
				if (validatePK()) {
					validateRoleCode();
					validateRemarks();
				}
			}
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.set("USER_ID", userID);
				formDTO.setObject("EFFT_DATE", BackOfficeFormatUtils.getDate(effectiveDate, context.getDateFormat()));
				formDTO.set("ROLE_CODE", roleCode);
				formDTO.set("REMARKS", remarks);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("userID", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
	}

	public boolean validateUserID() {
		AccessValidator validation = new AccessValidator();
		try {
			if (validation.isEmpty(userID)) {
				getErrorMap().setError("userID", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			DTObject formDTO = getFormDTO();
			formDTO.set(ContentManager.USER_ID, userID);
			formDTO.set(ContentManager.FETCH, ContentManager.FETCH_ALL);
			formDTO = validation.validateInternalUser(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("userID", formDTO.get(ContentManager.ERROR));
				return false;
			}
			if (formDTO.get(ContentManager.RESULT).equalsIgnoreCase(ContentManager.DATA_UNAVAILABLE)) {
				getErrorMap().setError("userID", BackOfficeErrorCodes.INVALID_CODE);
				return false;
			}

			if (userID.equals(context.getUserID())) {
				getErrorMap().setError("userID", BackOfficeErrorCodes.CANNOT_ALLOCATE_TO_SELF);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("userID", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateRoleCode() {
		AccessValidator validation = new AccessValidator();
		try {
			if (validation.isEmpty(roleCode)) {
				getErrorMap().setError("roleCode", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			DTObject formDTO = getFormDTO();
			formDTO.set(ContentManager.FETCH, ContentManager.FETCH_ALL);
			formDTO.set(ContentManager.CODE, roleCode);
			formDTO.set(ContentManager.ACTION, getAction());
			formDTO = validation.validateInternalRole(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("roleCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			if (formDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				getErrorMap().setError("roleCode", BackOfficeErrorCodes.INVALID_CODE);
				return false;
			}

			String userRoleCode = validation.getInternalUserRoleCode(userID);
			if (userRoleCode.equals(roleCode)) {
				getErrorMap().setError("roleCode", BackOfficeErrorCodes.PRIMARY_ROLE_NOT_ALLOWED);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("roleCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public DTObject validateCurrentRole(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		AccessValidator validation = new AccessValidator();
		try {
			String userID = inputDTO.get("USER_ID");
			if (validation.isEmpty(userID)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			resultDTO = validation.validateInternalUser(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
			if (userID.equals(context.getUserID())) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.CANNOT_ALLOCATE_TO_SELF);
				return resultDTO;
			}
			String userName = resultDTO.get("USER_NAME");
			String userRoleCode = validation.getInternalUserRoleCode(userID);
			inputDTO.set(ContentManager.CODE, userRoleCode);
			inputDTO.set(ContentManager.FETCH, ContentManager.FETCH_ALL);
			resultDTO = validation.validateInternalRole(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (!resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_ROLECODE);
				return resultDTO;
			}
			resultDTO.set("USER_NAME", userName);
			resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public DTObject validateRoleCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		AccessValidator validation = new AccessValidator();
		String roleCode = inputDTO.get("CODE");
		try {
			if (validation.isEmpty(roleCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH, ContentManager.FETCH_ALL);
			resultDTO = validation.validateInternalRole(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_RECORD_NOT_EXISTS);
				return resultDTO;
			}

		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateEffectiveDate() {
		CommonValidator validation = new CommonValidator();
		TBAActionType AT;
		try {
			if (getAction().equals(ADD))
				AT = TBAActionType.ADD;
			else
				AT = TBAActionType.MODIFY;
			if (validation.isEmpty(effectiveDate)) {
				getErrorMap().setError("effectiveDate", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isEffectiveDateValid(effectiveDate, AT)) {
				getErrorMap().setError("effectiveDate", BackOfficeErrorCodes.INVALID_DATE);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("effectiveDate", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public DTObject validateEffectiveDate(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		AccessValidator validation = new AccessValidator();
		String efftDate = inputDTO.get("EFFT_DATE");
		String action = inputDTO.get(ContentManager.USER_ACTION);
		try {

			if (validation.isEmpty(efftDate)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_FIELD_BLANK);
				return resultDTO;
			}
			HashMap<Integer, DBEntry> whereClauseFieldsMap = new HashMap<Integer, DBEntry>();
			whereClauseFieldsMap.put(1, new DBEntry("ENTITY_CODE", context.getEntityCode(), BindParameterType.VARCHAR));
			whereClauseFieldsMap.put(1, new DBEntry("USER_ID", inputDTO.get("USER_ID"), BindParameterType.VARCHAR));
			whereClauseFieldsMap.put(2, new DBEntry("EFFT_DATE", BackOfficeFormatUtils.getDate(inputDTO.get("EFFT_DATE"), context.getDateFormat()), BindParameterType.DATE));
			boolean isRecordAvailable = validation.isRecordAvailable("USERSROLEALLOCHIST", whereClauseFieldsMap);
			
				if (action.equals(ADD)) {
					if (isRecordAvailable) {
						resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_RECORD_EXISTS);
					}	
				} 
				if (action.equals(MODIFY)) {
					if (!isRecordAvailable) {
						resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_RECORD_NOT_EXISTS);
					}
				}	
			

		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validatePK() {
		AccessValidator validation = new AccessValidator();
		HashMap<Integer, DBEntry> whereClauseFieldsMap = new HashMap<Integer, DBEntry>();
		whereClauseFieldsMap.put(1, new DBEntry("ENTITY_CODE", context.getEntityCode(), BindParameterType.VARCHAR));
		whereClauseFieldsMap.put(2, new DBEntry("USER_ID", userID, BindParameterType.VARCHAR));
		whereClauseFieldsMap.put(3, new DBEntry("EFFT_DATE", BackOfficeFormatUtils.getDate(effectiveDate, context.getDateFormat()), BindParameterType.DATE));
		boolean isRecordAvailable = validation.isRecordAvailable("USERSROLEALLOCHIST", whereClauseFieldsMap);
		try {
			if (isRecordAvailable) {

				if (getAction().equals(ADD)) {
					getErrorMap().setError("userID", BackOfficeErrorCodes.RECORD_EXISTS);
					return false;
				}
				return true;
			} else {
				if (getAction().equals(MODIFY)) {
					getErrorMap().setError("userID", BackOfficeErrorCodes.RECORD_NOT_EXISTS);
					return false;
				}
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("userID", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateRemarks() {
		CommonValidator commonValidator = new CommonValidator();
		try {
				if (remarks!=null && remarks.length()>0) {
					if(!commonValidator.isValidRemarks(remarks)){
						getErrorMap().setError("remarks", BackOfficeErrorCodes.HMS_INVALID_REMARKS);
						return false;
					}
				}
				if (getAction().equals(MODIFY)) {
					if (commonValidator.isEmpty(remarks)) {
						getErrorMap().setError("remarks", BackOfficeErrorCodes.HMS_FIELD_BLANK);
						return false;
					}
					if(!commonValidator.isValidRemarks(remarks)){
						getErrorMap().setError("remarks", BackOfficeErrorCodes.HMS_INVALID_REMARKS);
						return false;
					}
				}
				return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}
}