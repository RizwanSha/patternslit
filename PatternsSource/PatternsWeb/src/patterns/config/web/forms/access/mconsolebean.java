package patterns.config.web.forms.access;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.AccessValidator;
import patterns.config.validations.CommonValidator;
import patterns.config.web.forms.GenericFormBean;

public class mconsolebean extends GenericFormBean {

	private static final long serialVersionUID = 1L;

	private String consoleCode;
	private String description;
	private boolean enabled;
	private String remarks;

	public String getConsoleCode() {
		return consoleCode;
	}

	public String getDescription() {
		return description;
	}

	public String getRemarks() {
		return remarks;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void reset() {
		consoleCode = RegularConstants.EMPTY_STRING;
		description = RegularConstants.EMPTY_STRING;
		enabled = false;
		remarks = RegularConstants.EMPTY_STRING;
		setProcessBO("patterns.config.framework.bo.access.mconsoleBO");
	}

	public void setConsoleCode(String consoleCode) {
		this.consoleCode = consoleCode;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public void validate() {
		if (validateConsoleCode()) {
			validateDescription();
			validateRemarks();
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.set("CODE", consoleCode);
				formDTO.set("DESCRIPTION", description);
				if (getAction().equals(ADD)) {
					formDTO.set("ENABLED", decodeBooleanToString(true));
				} else {
					formDTO.set("ENABLED", decodeBooleanToString(enabled));
				}
				formDTO.set("REMARKS", remarks);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("consoleCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
	}

	public boolean validateConsoleCode() {
		AccessValidator validation = new AccessValidator();
		try {
			if (validation.isEmpty(consoleCode)) {
				getErrorMap().setError("consoleCode", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidCode(consoleCode)) {
				getErrorMap().setError("consoleCode", BackOfficeErrorCodes.INVALID_FORMAT);
				return false;
			}
			DTObject formDTO = getFormDTO();
			formDTO.set(ContentManager.CODE, consoleCode);
			formDTO.set(ContentManager.USER_ACTION, getAction());
			formDTO = validation.validateConsoleCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("consoleCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			if (getAction().equals(ADD)) {
				if (formDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
					getErrorMap().setError("consoleCode", BackOfficeErrorCodes.RECORD_EXISTS);
					return false;
				}
				return true;
			} else if (getAction().equals(MODIFY)) {
				if (formDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
					getErrorMap().setError("consoleCode", BackOfficeErrorCodes.RECORD_NOT_EXISTS);
					return false;
				}
				return true;
			} else {
				getErrorMap().setError("consoleCode", BackOfficeErrorCodes.INVALID_ACTION);
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("consoleCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public DTObject validateConsoleCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		AccessValidator validation = new AccessValidator();
		String consoleCode = inputDTO.get("CODE");
		String action = inputDTO.get(ContentManager.USER_ACTION);
		try {

			if (validation.isEmpty(consoleCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH, ContentManager.FETCH_ALL);
			resultDTO = validation.validateConsoleCode(inputDTO);
			if (action.equals(ADD)) {
				if (ContentManager.DATA_AVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_RECORD_EXISTS);
				}
			} else {
				if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_RECORD_NOT_EXISTS);

				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateDescription() {
		CommonValidator validation = new CommonValidator();
		try {
			if (!validation.isValidDescription(description)) {
				getErrorMap().setError("description", BackOfficeErrorCodes.INVALID_DESCRIPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("description", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateRemarks() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (remarks != null && remarks.length() > 0) {
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.HMS_INVALID_REMARKS);
					return false;
				}
			}
			if (getAction().equals(MODIFY)) {
				if (commonValidator.isEmpty(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.HMS_FIELD_BLANK);
					return false;
				}
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.HMS_INVALID_REMARKS);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}
}