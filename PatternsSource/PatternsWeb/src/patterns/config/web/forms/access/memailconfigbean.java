package patterns.config.web.forms.access;

import java.util.HashMap;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.BindParameterType;
import patterns.config.framework.database.DBEntry;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.process.TBAActionType;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.validations.CommonValidator;
import patterns.config.web.forms.GenericFormBean;

public class memailconfigbean extends GenericFormBean {

	private static final long serialVersionUID = 1L;

	private String effectiveDate;
	private String incomingServer;
	private String incomingPort;
	private boolean incomingAuthReq;
	private String incomingUserName;
	private String incomingPwd;
	private String outgoingServer;
	private String outgoingPort;
	private String bankName;
	private String bankEmail;
	private boolean sslReq;

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBankEmail() {
		return bankEmail;
	}

	public void setBankEmail(String bankEmail) {
		this.bankEmail = bankEmail;
	}

	public boolean isSslReq() {
		return sslReq;
	}

	public void setSslReq(boolean sslReq) {
		this.sslReq = sslReq;
	}

	private boolean outgoingAuthReq;
	private String outgoingUserName;
	private String outgoingPwd;

	public boolean isIncomingAuthReq() {
		return incomingAuthReq;
	}

	public void setIncomingAuthReq(boolean incomingAuthReq) {
		this.incomingAuthReq = incomingAuthReq;
	}

	public boolean isOutgoingAuthReq() {
		return outgoingAuthReq;
	}

	public void setOutgoingAuthReq(boolean outgoingAuthReq) {
		this.outgoingAuthReq = outgoingAuthReq;
	}

	public String getIncomingServer() {
		return incomingServer;
	}

	public void setIncomingServer(String incomingServer) {
		this.incomingServer = incomingServer;
	}

	public String getIncomingPort() {
		return incomingPort;
	}

	public void setIncomingPort(String incomingPort) {
		this.incomingPort = incomingPort;
	}

	public String getIncomingUserName() {
		return incomingUserName;
	}

	public void setIncomingUserName(String incomingUserName) {
		this.incomingUserName = incomingUserName;
	}

	public String getIncomingPwd() {
		return incomingPwd;
	}

	public void setIncomingPwd(String incomingPwd) {
		this.incomingPwd = incomingPwd;
	}

	public String getOutgoingServer() {
		return outgoingServer;
	}

	public void setOutgoingServer(String outgoingServer) {
		this.outgoingServer = outgoingServer;
	}

	public String getOutgoingPort() {
		return outgoingPort;
	}

	public void setOutgoingPort(String outgoingPort) {
		this.outgoingPort = outgoingPort;
	}

	public String getOutgoingUserName() {
		return outgoingUserName;
	}

	public void setOutgoingUserName(String outgoingUserName) {
		this.outgoingUserName = outgoingUserName;
	}

	public String getOutgoingPwd() {
		return outgoingPwd;
	}

	public void setOutgoingPwd(String outgoingPwd) {
		this.outgoingPwd = outgoingPwd;
	}

	public String getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	@Override
	public void reset() {
		effectiveDate = RegularConstants.EMPTY_STRING;
		incomingServer = RegularConstants.EMPTY_STRING;
		incomingPort = RegularConstants.EMPTY_STRING;
		incomingAuthReq = false;
		incomingUserName = RegularConstants.EMPTY_STRING;
		incomingPwd = RegularConstants.EMPTY_STRING;
		outgoingServer = RegularConstants.EMPTY_STRING;
		outgoingPort = RegularConstants.EMPTY_STRING;
		outgoingAuthReq = false;
		outgoingUserName = RegularConstants.EMPTY_STRING;
		outgoingPwd = RegularConstants.EMPTY_STRING;
		bankName = RegularConstants.EMPTY_STRING;
		bankEmail = RegularConstants.EMPTY_STRING;
		sslReq = false;
		setProcessBO("patterns.config.framework.bo.access.memailconfigBO");
	}

	@Override
	public void validate() {
		if (validateEffectiveDate()) {
			validateBankName();
			validateBankEmail();
			validateIncomingServer();
			validateIncomingPort();
			if (incomingAuthReq) {
				validateIncomingUserName();
				validateIncomingUserPwd();
			}
			validateOutgoingServer();
			validateOutgoingPort();
			if (outgoingAuthReq) {
				validateOutgoingUserName();
				validateOutgoingUserPwd();
			}
		}
		if (getErrorMap().length() == 0) {
			DTObject formDTO = getFormDTO();
			formDTO.set("ENTITY_CODE", context.getEntityCode());
			formDTO.setObject("EFFT_DATE", BackOfficeFormatUtils.getDate(effectiveDate, context.getDateFormat()));
			formDTO.set("BANK_NAME", bankName);
			formDTO.set("BANK_EMAIL", bankEmail);
			formDTO.set("SSL_REQUIRED", decodeBooleanToString(sslReq));
			formDTO.set("INCOMING_MAIL_SERVER", incomingServer);
			formDTO.set("INCOMING_MAIL_SERVER_PORT", incomingPort);
			formDTO.set("INCOMING_AUTH_REQD", decodeBooleanToString(incomingAuthReq));
			if (incomingAuthReq) {
				formDTO.set("INCOMING_USER_NAME", incomingUserName);
				formDTO.set("INCOMING_PASSWORD", incomingPwd);
			} else {
				formDTO.set("INCOMING_USER_NAME", null);
				formDTO.set("INCOMING_PASSWORD", null);
			}
			formDTO.set("OUTGOING_MAIL_SERVER", outgoingServer);
			formDTO.set("OUTGOING_MAIL_SERVER_PORT", outgoingPort);
			formDTO.set("OUTGOING_AUTH_REQD", decodeBooleanToString(outgoingAuthReq));
			if (outgoingAuthReq) {
				formDTO.set("OUTGOING_USER_NAME", outgoingUserName);
				formDTO.set("OUTGOING_PASSWORD", outgoingPwd);
			} else {
				formDTO.set("OUTGOING_USER_NAME", null);
				formDTO.set("OUTGOING_PASSWORD", null);

			}
		}
	}

	public boolean validateBankEmail() {
		CommonValidator validation = new CommonValidator();
		try {

			if (validation.isEmpty(bankEmail)) {
				getErrorMap().setError("bankEmail", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidEmail(bankEmail)) {
				getErrorMap().setError("bankEmail", BackOfficeErrorCodes.INVALID_EMAIL_ID);
				return false;
			}

		} catch (Exception e) {

			e.printStackTrace();
		} finally {
			validation.close();
		}

		return true;
	}

	public boolean validateBankName() {
		CommonValidator validation = new CommonValidator();
		try {

			if (validation.isEmpty(bankName)) {
				getErrorMap().setError("bankName", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidName(bankName)) {
				getErrorMap().setError("bankName", BackOfficeErrorCodes.INVALID_NAME);
				return false;
			}

		} catch (Exception e) {

			e.printStackTrace();
		} finally {
			validation.close();
		}

		return true;
	}

	public boolean validateEffectiveDate() {
		CommonValidator validation = new CommonValidator();
		try {
			TBAActionType AT;
			if (validation.isEmpty(effectiveDate)) {
				getErrorMap().setError("effectiveDate", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (getAction().equals(ADD))
				AT = TBAActionType.ADD;
			else
				AT = TBAActionType.MODIFY;
			if (validation.isEffectiveDateValid(effectiveDate, AT)) {
				HashMap<Integer, DBEntry> whereClauseFieldsMap = new HashMap<Integer, DBEntry>();

				whereClauseFieldsMap.put(1, new DBEntry("ENTITY_CODE", context.getEntityCode(), BindParameterType.VARCHAR));
				whereClauseFieldsMap.put(2, new DBEntry("EFFT_DATE", BackOfficeFormatUtils.getDate(effectiveDate, context.getDateFormat()), BindParameterType.DATE));
				boolean isRecordAvailable = validation.isRecordAvailable("EMAILCONFIG", whereClauseFieldsMap);
				if (isRecordAvailable) {
					if (getAction().equals(ADD)) {
						getErrorMap().setError("effectiveDate", BackOfficeErrorCodes.RECORD_EXISTS);
						return false;
					}
					return true;
				} else {
					if (getAction().equals(MODIFY)) {
						getErrorMap().setError("effectiveDate", BackOfficeErrorCodes.RECORD_NOT_EXISTS);
						return false;
					}
					return true;
				}
			} else {
				getErrorMap().setError("effectiveDate", BackOfficeErrorCodes.INVALID_DATE);
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateIncomingServer() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(incomingServer)) {
				getErrorMap().setError("incomingServer", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
		} catch (Exception e) {

		} finally {
			validation.close();
		}
		return true;
	}

	public boolean validateIncomingPort() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(incomingPort)) {
				getErrorMap().setError("incomingPort", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidPort(incomingPort)) {
				getErrorMap().setError("incomingPort", BackOfficeErrorCodes.INVALID_PORT);
				return false;
			}
			return true;
		} catch (Exception e) {

		} finally {
			validation.close();
		}
		return false;

	}

	public boolean validateIncomingUserName() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(incomingUserName)) {
				getErrorMap().setError("incomingUserName", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidCredential(incomingUserName)) {
				getErrorMap().setError("incomingUserName", BackOfficeErrorCodes.INVALID_CREDENTIALS);
				return false;
			}
			return true;
		} catch (Exception e) {

		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateIncomingUserPwd() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(incomingPwd)) {
				getErrorMap().setError("incomingPwd", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidCredential(incomingPwd)) {
				getErrorMap().setError("incomingPwd", BackOfficeErrorCodes.INVALID_CREDENTIALS);
				return false;
			}
			return true;
		} catch (Exception e) {

		} finally {
			validation.close();
		}
		return false;

	}

	public boolean validateOutgoingServer() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(outgoingServer)) {
				getErrorMap().setError("outgoingServer", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
		} catch (Exception e) {

		} finally {
			validation.close();
		}
		return true;

	}

	public boolean validateOutgoingPort() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(outgoingPort)) {
				getErrorMap().setError("outgoingPort", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidPort(outgoingPort)) {
				getErrorMap().setError("outgoingPort", BackOfficeErrorCodes.INVALID_PORT);
				return false;
			}
			return true;
		} catch (Exception e) {

		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateOutgoingUserName() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(outgoingUserName)) {
				getErrorMap().setError("outgoingUserName", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidCredential(outgoingUserName)) {
				getErrorMap().setError("outgoingUserName", BackOfficeErrorCodes.INVALID_CREDENTIALS);
				return false;
			}
			return true;
		} catch (Exception e) {

		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateOutgoingUserPwd() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(outgoingPwd)) {
				getErrorMap().setError("outgoingPwd", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidCredential(outgoingPwd)) {
				getErrorMap().setError("outgoingPwd", BackOfficeErrorCodes.INVALID_CREDENTIALS);
				return false;
			}
			return true;
		} catch (Exception e) {

		} finally {
			validation.close();
		}
		return false;
	}

}
