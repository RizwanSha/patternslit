package patterns.config.web.forms.access;

import java.util.HashMap;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.BindParameterType;
import patterns.config.framework.database.DBEntry;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.PasswordUtils;
import patterns.config.framework.web.SessionConstants;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.AccessValidator;
import patterns.config.validations.CommonValidator;
import patterns.config.web.forms.GenericFormBean;

public class epwdresetbean extends GenericFormBean {

	private static final long serialVersionUID = -6219657363150466160L;

	public epwdresetbean() {
		setProcessBO("patterns.config.framework.bo.access.epwdresetBO");
	}

	private String userID;
	private String createPassword;
	private String confirmPassword;
	private String remarks;
	private String pwdDelivery;
	private String pwdDeliveryType;
	private String passwordPolicy;
	private String minLength;
	private String minAlpha;
	private String minNumeric;
	private String minSpecial;
	private String minPrevent;
	private String loginNewPasswordHashed;
	private String randomSalt;

	public String getMinLength() {
		return minLength;
	}

	public void setMinLength(String minLength) {
		this.minLength = minLength;
	}

	public String getMinAlpha() {
		return minAlpha;
	}

	public void setMinAlpha(String minAlpha) {
		this.minAlpha = minAlpha;
	}

	public String getMinNumeric() {
		return minNumeric;
	}

	public void setMinNumeric(String minNumeric) {
		this.minNumeric = minNumeric;
	}

	public String getMinSpecial() {
		return minSpecial;
	}

	public void setMinSpecial(String minSpecial) {
		this.minSpecial = minSpecial;
	}

	public String getMinPrevent() {
		return minPrevent;
	}

	public void setMinPrevent(String minPrevent) {
		this.minPrevent = minPrevent;
	}

	public String getLoginNewPasswordHashed() {
		return loginNewPasswordHashed;
	}

	public void setLoginNewPasswordHashed(String loginNewPasswordHashed) {
		this.loginNewPasswordHashed = loginNewPasswordHashed;
	}

	public String getRandomSalt() {
		return randomSalt;
	}

	public void setRandomSalt(String randomSalt) {
		this.randomSalt = randomSalt;
	}

	public String getPwdDeliveryType() {
		return pwdDeliveryType;
	}

	public void setPwdDeliveryType(String pwdDeliveryType) {
		this.pwdDeliveryType = pwdDeliveryType;
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public String getCreatePassword() {
		return createPassword;
	}

	public void setCreatePassword(String createPassword) {
		this.createPassword = createPassword;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getPwdDelivery() {
		return pwdDelivery;
	}

	public void setPwdDelivery(String pwdDelivery) {
		this.pwdDelivery = pwdDelivery;
	}

	public void reset() {
		userID = RegularConstants.EMPTY_STRING;
		createPassword = RegularConstants.EMPTY_STRING;
		confirmPassword = RegularConstants.EMPTY_STRING;
		remarks = RegularConstants.EMPTY_STRING;
		pwdDelivery = RegularConstants.EMPTY_STRING;
		pwdDeliveryType = RegularConstants.EMPTY_STRING;
		AccessValidator validator = new AccessValidator();
		try {
			DTObject formDTO = getFormDTO();
			formDTO = validator.getSYSCPM(formDTO);
			minLength = formDTO.get("MIN_PWD_LEN");
			minAlpha = formDTO.get("MIN_PWD_ALPHA");
			minNumeric = formDTO.get("MIN_PWD_NUM");
			minSpecial = formDTO.get("MIN_PWD_SPECIAL");
			minPrevent = formDTO.get("PREVENT_PREV_PWD");
			pwdDelivery = formDTO.get("PWD_DELIVERY_MECHANISM");
			pwdDeliveryType = formDTO.get("PWD_MANUAL_TYPE");
			setPasswordPolicy(formDTO.get("PASSWORD_POLICY"));
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError("reset(1) : " + e.getLocalizedMessage());
		} finally {
			validator.close();
		}
		if (!context.isFormPosted()) {
			randomSalt = PasswordUtils.getSalt();
			webContext.getSession().setAttribute(SessionConstants.RANDOM_SALT, randomSalt);
		}
		randomSalt = (String) webContext.getSession().getAttribute(SessionConstants.RANDOM_SALT);

	}

	public void setPasswordPolicy(String passwordPolicy) {
		this.passwordPolicy = passwordPolicy;
	}

	public String getPasswordPolicy() {
		return passwordPolicy;
	}

	public void validate() {
		if (validatePK()) {
			validateUserId();
			validRemarks();
			if (pwdDelivery.equals("4") && pwdDeliveryType.equals("2")) {
				validatePasswords();
			}
		}
		if (getErrorMap().length() == 0) {
			DTObject formDTO = getFormDTO();
			String salt = null;
			String encryptedPassword = null;
			if (pwdDelivery.equals("4") && pwdDeliveryType.equals("2")) {
				salt = PasswordUtils.getSalt();
				encryptedPassword = PasswordUtils.getEncryptedHashSalt(loginNewPasswordHashed, salt);
			}
			formDTO.set("ENTITY_CODE", context.getEntityCode());
			formDTO.set("USER_ID", userID);
			formDTO.set("CLEAR_PIN", createPassword);
			formDTO.set("SALT", salt);
			formDTO.set("ENCYPT_PASSWORD", encryptedPassword);
			formDTO.set("PWD_DELIVERY_CHOICE", pwdDelivery);
			formDTO.set("PWD_DELIVERY_TYPE", pwdDeliveryType);
			formDTO.set("REMARKS", remarks);
		}
	}

	public boolean validateUserId() {
		AccessValidator validation = new AccessValidator();
		DTObject formDTO = getFormDTO();
		try {
			if (validation.isEmpty(userID)) {
				getErrorMap().setError("userID", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			formDTO.set(ContentManager.USER_ID, userID);
			formDTO.set(ContentManager.FETCH, ContentManager.FETCH_ALL);
			formDTO = validation.validateInternalUser(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("userID", formDTO.get(ContentManager.ERROR));
				return false;
			}
			if (userID.equals(context.getUserID())) {
				getErrorMap().setError("userID", BackOfficeErrorCodes.CANNOT_ALLOCATE_TO_SELF);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("userID", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	private boolean validatePasswords() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(createPassword)) {
				getErrorMap().setError("createPassword", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (validation.isEmpty(confirmPassword)) {
				getErrorMap().setError("confirmPassword", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!createPassword.equals(confirmPassword)) {
				getErrorMap().setError("createPassword", BackOfficeErrorCodes.PASSWORD_MISMATCH);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;

	}

	public boolean validateRemarks() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (commonValidator.isEmpty(remarks)) {
				getErrorMap().setError("remarks", BackOfficeErrorCodes.HMS_FIELD_BLANK);
				return false;
			}
			if (remarks != null && remarks.length() > 0) {
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.HMS_INVALID_REMARKS);
					return false;
				}
			}

			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}

	public boolean validRemarks() {
		CommonValidator validation = new CommonValidator();
		try {
			if (!validation.isValidRemarks(remarks)) {
				getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validatePK() {
		CommonValidator validation = new CommonValidator();
		try {
			HashMap<Integer, DBEntry> whereClauseFieldsMap = new HashMap<Integer, DBEntry>();
			whereClauseFieldsMap.put(1, new DBEntry("ENTITY_CODE", context.getEntityCode(), BindParameterType.VARCHAR));
			whereClauseFieldsMap.put(2, new DBEntry("USER_ID", userID, BindParameterType.VARCHAR));
			boolean isRecordAvailable = validation.isRecordAvailable("USERSPWDRESET", whereClauseFieldsMap);
			if (isRecordAvailable) {
				if (!getAction().equals(MODIFY)) {
					getErrorMap().setError("userID", BackOfficeErrorCodes.RECORD_NOT_EXISTS);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("userID", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

}