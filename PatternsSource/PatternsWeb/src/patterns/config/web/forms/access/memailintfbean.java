/**This program will be used to create Email Interface Maintenance
 * 
 *
 * @version 1.0
 * @author AjayKhanna
 * @since 07-April-2016 <br>
 *        <b>Modified History</b> <BR>
 *        <U><B> Sl.No Modified Date Author Modified Changes Version
 * 
 *        1. 06-April-2016 Ajay Khanna Special Date Method has Added
 * 
 *        2. 07-April-2016 Ajay Khanna code review by Rizwan Sha sir
 * 
 * 
 * 
 * 
 * 
 */
package patterns.config.web.forms.access;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.AccessValidator;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.MasterValidator;
import patterns.config.web.forms.GenericFormBean;

public class memailintfbean extends GenericFormBean {

	private static final long serialVersionUID = 1L;
	private String emailCode;
	private String description;
	private boolean sendAllowed;
	private boolean receiveAllowed;
	private boolean enabled;
	private String remarks;
	
	

	public String getEmailCode() {
		return emailCode;
	}

	public void setEmailCode(String emailCode) {
		this.emailCode = emailCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public boolean isSendAllowed() {
		return sendAllowed;
	}

	public void setSendAllowed(boolean sendAllowed) {
		this.sendAllowed = sendAllowed;
	}

	public boolean isreceiveAllowed() {
		return receiveAllowed;
	}

	public void setreceiveAllowed(boolean receiveAllowed) {
		this.receiveAllowed = receiveAllowed;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void reset() {
		emailCode = RegularConstants.EMPTY_STRING;
		description = RegularConstants.EMPTY_STRING;
		sendAllowed = false;
		receiveAllowed = false;
		enabled = false;
		remarks = RegularConstants.EMPTY_STRING;
		setProcessBO("patterns.config.framework.bo.access.memailintfBO");
	}

	@Override
	public void validate() {
		if (validateEmailCode()) {
			validateDescription();
			validateRemarks();
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("EMAIL_CODE", emailCode);
				formDTO.set("EMAIL_DESCN", description);
				formDTO.set("EMAIL_SEND_ALLOWED", decodeBooleanToString(sendAllowed));
				formDTO.set("EMAIL_RECV_ALLOWED", decodeBooleanToString(receiveAllowed));
				if (getAction().equals(ADD)) {
					formDTO.set("ENABLED", decodeBooleanToString(true));
				} else {
					formDTO.set("ENABLED", decodeBooleanToString(enabled));
				}
				formDTO.set("REMARKS", remarks);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("emailCode", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		}
	}

	public boolean validateEmailCode() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("EMAIL_CODE", emailCode);
			formDTO.set("ACTION", getAction());
			formDTO = validateEmailCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("emailCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			getErrorMap().setError("emailCode", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		}
		return false;

	}

	public DTObject validateEmailCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		AccessValidator validation = new AccessValidator();
		String emailCode = inputDTO.get("EMAIL_CODE");
		String action = inputDTO.get(ContentManager.USER_ACTION);
		try {
			if (validation.isEmpty(emailCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "EMAIL_DESCN");
			resultDTO = validEmailCode(inputDTO);
			if (action.equals(ADD)) {
				if (ContentManager.DATA_AVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_RECORD_EXISTS);
				}
			} else {
				if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_RECORD_NOT_EXISTS);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateDescription() {
		CommonValidator validation = new CommonValidator();
		try {
		if (validation.isEmpty(description)) {
				getErrorMap().setError("description", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidDescription(description)) {
				getErrorMap().setError("description", BackOfficeErrorCodes.HMS_INVALID_DESCRIPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("description", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	
	public boolean validateRemarks() {
		CommonValidator commonValidator = new CommonValidator();
		try {
				if (remarks!=null && remarks.length()>0) {
					if(!commonValidator.isValidRemarks(remarks)){
						getErrorMap().setError("remarks", BackOfficeErrorCodes.HMS_INVALID_REMARKS);
						return false;
					}
				}
				if (getAction().equals(MODIFY)) {
					if (commonValidator.isEmpty(remarks)) {
						getErrorMap().setError("remarks", BackOfficeErrorCodes.HMS_FIELD_BLANK);
						return false;
					}
					if(!commonValidator.isValidRemarks(remarks)){
						getErrorMap().setError("remarks", BackOfficeErrorCodes.HMS_INVALID_REMARKS);
						return false;
					}
				}
				return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}
	
	public final DTObject validEmailCode(DTObject input) {
		DTObject result = new DTObject();
		String emailCode = input.get("EMAIL_CODE");
		String action = input.get(ContentManager.USER_ACTION);
		CommonValidator validator= new CommonValidator();
		try {
			if (action.equals(ADD)) {
				
				if (!validator.isValidCodePattern(emailCode)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_FORMAT);
					return result;
				}
				if (!validator.isValidLength(emailCode,validator.LENGTH_1,validator.LENGTH_12)) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_LENGTH);
					return result;
				}
			}
			input.set(ContentManager.CODE, emailCode);
			input.set(ContentManager.TABLE, "EMAILINTF");
			input.set(ContentManager.COLUMN, "EMAIL_CODE");
			result = validator.validateGenericCode(input);
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
			return result;
		}
		return result;
	}
}