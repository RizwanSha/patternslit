package patterns.config.web.forms.access;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.framework.web.DHTMLXGridUtility;
import patterns.config.framework.web.GenericOption;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.AccessValidator;
import patterns.config.web.forms.GenericFormBean;

public class qauditlogbean extends GenericFormBean {

	private static final long serialVersionUID = -6219657363150466160L;

	private String optionID;
	private String userID;
	private String fromDate;
	private String toDate;
	private String actionType;
	private String xmlInnerGrid;
	private String gridbox_rowid;

	public String getXmlInnerGrid() {
		return xmlInnerGrid;
	}

	public void setXmlInnerGrid(String xmlInnerGrid) {
		this.xmlInnerGrid = xmlInnerGrid;
	}

	public String getGridbox_rowid() {
		return gridbox_rowid;
	}

	public void setGridbox_rowid(String gridbox_rowid) {
		this.gridbox_rowid = gridbox_rowid;
	}

	public String getOptionID() {
		return optionID;
	}

	public void setOptionID(String optionID) {
		this.optionID = optionID;
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getActionType() {
		return actionType;
	}

	public void setActionType(String actionType) {
		this.actionType = actionType;
	}

	public void reset() {
		optionID = RegularConstants.EMPTY_STRING;
		userID = RegularConstants.EMPTY_STRING;
		fromDate = RegularConstants.EMPTY_STRING;
		toDate = RegularConstants.EMPTY_STRING;
		actionType = RegularConstants.EMPTY_STRING;
		DBContext dbContext = new DBContext();
		ArrayList<GenericOption> actionTypeList = getGenericOptions("COMMON", "AUDITLOG", dbContext, "--Action Type--");
		webContext.getRequest().setAttribute("COMMON_QAUDITLOG", actionTypeList);
		dbContext.close();
	}

	public void validate() {
	}

	public DTObject validateOptionID(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		AccessValidator validation = new AccessValidator();
		String optionId = inputDTO.get("MPGM_ID");
		try {

			if (!validation.isEmpty(optionId)) {
				inputDTO.set(ContentManager.FETCH_COLUMNS, "MPGM_ID,MPGM_DESCN");
				resultDTO = validation.validateOption(inputDTO);
				if (resultDTO.get(ContentManager.ERROR) != null) {
					resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
					return resultDTO;
				}
				if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_RECORD_NOT_EXISTS);
					return resultDTO;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public DTObject validateUserID(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		AccessValidator validation = new AccessValidator();
		String userID = inputDTO.get("USER_ID");
		try {
			if (!validation.isEmpty(userID)) {
				inputDTO.set(ContentManager.FETCH_COLUMNS, "USER_ID,USER_NAME");
				resultDTO = validation.validateInternalUser(inputDTO);
				if (resultDTO.get(ContentManager.ERROR) != null) {
					resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
					return resultDTO;
				}
				if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_USERID);
					return resultDTO;
				}
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public DTObject loadAuditLogGrid(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);

		Date fromDate = null;
		String userId = null;
		String optionId = null;
		Date toDate = null;

		AccessValidator validator = new AccessValidator();
		try {

			toDate = new java.sql.Date(BackOfficeFormatUtils.getDate(inputDTO.get("TO_DATE"), BackOfficeConstants.JAVA_DATE_FORMAT).getTime());
			if (!validator.isEmpty(inputDTO.get("FROM_DATE")))
				fromDate = new java.sql.Date(BackOfficeFormatUtils.getDate(inputDTO.get("FROM_DATE"), BackOfficeConstants.JAVA_DATE_FORMAT).getTime());

			if (!validator.isEmpty(inputDTO.get("OPTION_ID"))) {
				optionId = inputDTO.get("OPTION_ID");
			}
			if (!validator.isEmpty(inputDTO.get("USER_ID"))) {
				userId = inputDTO.get("USER_ID");
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			validator.close();
		}

		DBContext dbContext = new DBContext();
		try {
			DBUtil util = dbContext.createUtilInstance();
			util.reset();
			util.setMode(DBUtil.CALLABLE);
			util.setSql("CALL SP_AUDITLOG(?,?,?,?,?,?,?)");
			util.setString(1, context.getEntityCode());
			util.setString(2, optionId);
			util.setString(3, userId);
			util.setDate(4, fromDate);
			util.setDate(5, toDate);
			util.setString(6, inputDTO.get("ACTION_TYPE"));
			util.registerOutParameter(7, Types.VARCHAR);
			util.execute();
			resultDTO.set("INV_NO", util.getString(7));
			resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			dbContext.close();
		}
		return resultDTO;
	}

}