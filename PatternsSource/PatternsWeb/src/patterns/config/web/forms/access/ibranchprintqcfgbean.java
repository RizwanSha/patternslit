package patterns.config.web.forms.access;

import java.util.HashMap;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.BindParameterType;
import patterns.config.framework.database.DBEntry;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.process.TBAActionType;
import patterns.config.framework.service.DTDObject;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.AccessValidator;
import patterns.config.web.forms.GenericFormBean;

public class ibranchprintqcfgbean extends GenericFormBean {

	private static final long serialVersionUID = 1L;

	private String branchCode;
	private String effectiveDate;
	private String xmlBrnPrntGrid;
	private String printType;
	private String printQueue;
	private String gridbox_rowid;

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public String getXmlBrnPrntGrid() {
		return xmlBrnPrntGrid;
	}

	public void setXmlBrnPrntGrid(String xmlBrnPrntGrid) {
		this.xmlBrnPrntGrid = xmlBrnPrntGrid;
	}

	public String getPrintType() {
		return printType;
	}

	public void setPrintType(String printType) {
		this.printType = printType;
	}

	public String getPrintQueue() {
		return printQueue;
	}

	public void setPrintQueue(String printQueue) {
		this.printQueue = printQueue;
	}

	public String getGridbox_rowid() {
		return gridbox_rowid;
	}

	public void setGridbox_rowid(String gridbox_rowid) {
		this.gridbox_rowid = gridbox_rowid;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void reset() {
		branchCode = RegularConstants.EMPTY_STRING;
		effectiveDate = RegularConstants.EMPTY_STRING;
		xmlBrnPrntGrid = RegularConstants.EMPTY_STRING;
		printType = RegularConstants.EMPTY_STRING;
		printQueue = RegularConstants.EMPTY_STRING;
		gridbox_rowid = RegularConstants.EMPTY_STRING;
		setProcessBO("patterns.config.framework.bo.access.ibranchprintqcfgBO");

	}

	public void validate() {
		if (validateBrnchCode()) {
			if (validateEffectDate()) {
				validateGrid();
			}
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.set("BRN_CODE", branchCode);
				formDTO.set("EFFT_DATE", effectiveDate);
				DTDObject dtdObject = new DTDObject();
				dtdObject.addColumn(0, "SL");
				dtdObject.addColumn(1, "PRNTYPE_ID");
				dtdObject.addColumn(2, "DESCRIPTION");
				dtdObject.addColumn(3, "PRNQ_ID");
				dtdObject.addColumn(4, "DESCRIPTION");
				dtdObject.setXML(xmlBrnPrntGrid);
				formDTO.setDTDObject("BRNPRNQCFGHISTDTL", dtdObject);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("branchCode",
					BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
	}

	public boolean validateBrnchCode() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("BRANCH_CODE", branchCode);
			formDTO.set("ACTION", getAction());
			formDTO = validateBranchCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("branchCode",
						formDTO.get(ContentManager.ERROR));
				return false;
			}

			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("branchCode",
					BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateBranchCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		AccessValidator validation = new AccessValidator();
		String branchCode = inputDTO.get("BRANCH_CODE");
		try {
			if(validation.isEmpty(branchCode)){
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
			resultDTO = validation.validateEntityBranch(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_RECORD_NOT_EXISTS);
				return resultDTO;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateEffectDate() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set(ContentManager.USER_ACTION, getAction());
			formDTO.set("EFFT_DATE", effectiveDate);
			formDTO.set("BRN_CODE", branchCode);
			formDTO = validateEffectiveDate(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("effectiveDate",
						formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("effectiveDate",
					BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateEffectiveDate(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		AccessValidator validation = new AccessValidator();
		String efftDate = inputDTO.get("EFFT_DATE");
		String action = inputDTO.get(ContentManager.USER_ACTION);
		TBAActionType AT;
		try {

			if (validation.isEmpty(efftDate)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_FIELD_BLANK);
				return resultDTO;
			}
			
			if (action.equals(ADD))
				AT = TBAActionType.ADD;
			else
				AT = TBAActionType.MODIFY;
			if (!validation.isEffectiveDateValid(efftDate, AT)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_EFFECTIVE_DATE);
				return resultDTO;
			}
			HashMap<Integer, DBEntry> whereClauseFieldsMap = new HashMap<Integer, DBEntry>();
			whereClauseFieldsMap.put(1, new DBEntry("ENTITY_CODE", context.getEntityCode(), BindParameterType.VARCHAR));
			whereClauseFieldsMap.put(2, new DBEntry("BRN_CODE", inputDTO.get("BRN_CODE"), BindParameterType.VARCHAR));
			whereClauseFieldsMap.put(3, new DBEntry("EFFT_DATE", BackOfficeFormatUtils.getDate(inputDTO.get("EFFT_DATE"), context.getDateFormat()), BindParameterType.DATE));
			boolean isRecordAvailable = validation.isRecordAvailable("BRNPRNQCFGHIST", whereClauseFieldsMap);
			if (isRecordAvailable) {
				if (action.equals(ADD)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_RECORD_EXISTS);
					return resultDTO;
				}
			} else {
				if (action.equals(MODIFY)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_RECORD_NOT_EXISTS);
					return resultDTO;
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}
	
	public boolean validatePrntType(String printType) {
		   try {
				DTObject formDTO = getFormDTO();
				formDTO.set("PRNTYPE_ID", printType);
				formDTO.set("ACTION", USAGE);
				formDTO = validatePrintTypeID(formDTO);
				if (formDTO.get(ContentManager.ERROR) != null) {
					getErrorMap().setError("printType", formDTO.get(ContentManager.ERROR));
					return false;
				}
				return true;
			} catch (Exception e) {
				e.printStackTrace();
				getErrorMap().setError("printType", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
			} 
			return false;
		}
		
		public DTObject validatePrintTypeID(DTObject inputDTO) {
			DTObject resultDTO = new DTObject();
			resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
			AccessValidator validation = new AccessValidator();
			String printType = inputDTO.get("PRNTYPE_ID");
			try {
				if(validation.isEmpty(printType)){
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_FIELD_BLANK);
					return resultDTO;
				}
				inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
				resultDTO = validation.validatePrintType(inputDTO);
				if (resultDTO.get(ContentManager.ERROR) != null) {
					resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
					return resultDTO;
				}
				if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_RECORD_NOT_EXISTS);
					return resultDTO;
				}
				
			} catch (Exception e) {
				e.printStackTrace();
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
			} finally {
				validation.close();
			}
			return resultDTO;
		}
		
		public boolean validatePrntQueue(String printQueue) {
			   try {
					DTObject formDTO = getFormDTO();
					formDTO.set("BRANCH_CODE", branchCode);
					formDTO.set("PRNQ_ID", printQueue);
					formDTO.set("ACTION", USAGE);
					formDTO = validatePrintQueue(formDTO);
					if (formDTO.get(ContentManager.ERROR) != null) {
						getErrorMap().setError("printQueue", formDTO.get(ContentManager.ERROR));
						return false;
					}
					return true;
				} catch (Exception e) {
					e.printStackTrace();
					getErrorMap().setError("printQueue", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
				} 
				return false;
			}
			
			public DTObject validatePrintQueue(DTObject inputDTO) {
				DTObject resultDTO = new DTObject();
				resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
				AccessValidator validation = new AccessValidator();
				String branchCode = inputDTO.get("BRANCH_CODE");
				String printQueue = inputDTO.get("PRNQ_ID");
				try {
					if(validation.isEmpty(printQueue)){
						resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_FIELD_BLANK);
						return resultDTO;
					}
					inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION,BRN_CODE");
					resultDTO = validation.validatePrintQueueId(inputDTO);
					String branCode = resultDTO.get("BRN_CODE");
 					if (resultDTO.get(ContentManager.ERROR) != null) {
						resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
						return resultDTO;
					}
					if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
						resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_RECORD_NOT_EXISTS);
						return resultDTO;
					}
					if(!branchCode.equals(branCode)){
						resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_PRNTQ_UNAVAIL_BRN);
						return resultDTO;
					}
					
				} catch (Exception e) {
					e.printStackTrace();
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
				} finally {
					validation.close();
				}
				return resultDTO;
			}

	private boolean validateGrid() {
		AccessValidator validation = new AccessValidator();
		try {
			DTDObject dtdObject = new DTDObject();
			dtdObject.addColumn(0, "SL");
			dtdObject.addColumn(1, "PRNTYPE_ID");
			dtdObject.addColumn(2, "DESCRIPTION");
			dtdObject.addColumn(3, "PRNQ_ID");
			dtdObject.addColumn(4, "DESCRIPTION");
			dtdObject.setXML(xmlBrnPrntGrid);
			if (dtdObject.getRowCount() <= 0) {
				getErrorMap().setError("printType", BackOfficeErrorCodes.HMS_ATLEAST_ONE_ROW);
				return false;
			}
			if (!validation.checkDuplicateRows(dtdObject, 1)) {
				getErrorMap().setError("printType", BackOfficeErrorCodes.HMS_DUPLICATE_DATA);
				return false;
			}
			for (int i = 0; i < dtdObject.getRowCount(); i++) {
				if (!validatePrntType(dtdObject.getValue(i, 1))) {
					getErrorMap().setError("printType", BackOfficeErrorCodes.HMS_INVALID_ACTION);
					return false;
				}
				if (!validatePrntQueue(dtdObject.getValue(i, 3))) {
					getErrorMap().setError("printQueue", BackOfficeErrorCodes.HMS_INVALID_ACTION);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("printType", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}
}