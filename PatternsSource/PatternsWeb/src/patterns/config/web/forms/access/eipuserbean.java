package patterns.config.web.forms.access;

import java.util.HashMap;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.BindParameterType;
import patterns.config.framework.database.DBEntry;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.process.TBAActionType;
import patterns.config.framework.service.DTDObject;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.AccessValidator;
import patterns.config.validations.CommonValidator;
import patterns.config.web.forms.GenericFormBean;

public class eipuserbean extends GenericFormBean {
	private static final long serialVersionUID = 1L;

	private String userID;
	private String remarks;
	private String xmlIPList;
	private String IPAddress;
	private String gridbox_rowid;
	public boolean enabled;

	private String effectiveDate;

	public String getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public String getIPAddress() {
		return IPAddress;
	}

	public void setIPAddress(String iPAddress) {
		IPAddress = iPAddress;
	}

	public String getXmlIPList() {
		return xmlIPList;
	}

	public void setXmlIPList(String xmlIPList) {
		this.xmlIPList = xmlIPList;
	}

	public String getGridbox_rowid() {
		return gridbox_rowid;
	}

	public void setGridbox_rowid(String gridbox_rowid) {
		this.gridbox_rowid = gridbox_rowid;
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public void reset() {
		userID = RegularConstants.EMPTY_STRING;
		effectiveDate = RegularConstants.EMPTY_STRING;
		remarks = RegularConstants.EMPTY_STRING;
		xmlIPList = RegularConstants.EMPTY_STRING;
		setProcessBO("patterns.config.framework.bo.access.eipuserBO");
	}

	public void validate() {
		if (validateUserID()) {
			if (validateEffectiveDate()) {
				validatePK();
				if (enabled) {
					validateGrid();
				}
				validateRemarks();
			}
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.set("USER_ID", userID);
				formDTO.setObject("EFFT_DATE", BackOfficeFormatUtils.getDate(effectiveDate, context.getDateFormat()));
				formDTO.set("ENABLED", decodeBooleanToString(enabled));
				formDTO.set("REMARKS", remarks);
				if (enabled) {
					DTDObject dtdObject = new DTDObject();
					dtdObject.addColumn(0, "SL");
					dtdObject.addColumn(1, "IPADDRESS");
					dtdObject.setXML(xmlIPList);
					formDTO.setDTDObject("USERIPRESTRICTDTL", dtdObject);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("userID", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
	}

	public boolean validateEffectiveDate() {
		CommonValidator validation = new CommonValidator();
		TBAActionType AT;
		try {
			if (validation.isEmpty(effectiveDate)) {
				getErrorMap().setError("effectiveDate", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (getAction().equals(ADD))
				AT = TBAActionType.ADD;
			else
				AT = TBAActionType.MODIFY;
			if (!validation.isEffectiveDateValid(effectiveDate, AT)) {
				getErrorMap().setError("effectiveDate", BackOfficeErrorCodes.INVALID_DATE);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("effectiveDate", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validatePK() {

		AccessValidator validation = new AccessValidator();
		HashMap<Integer, DBEntry> whereClauseFieldsMap = new HashMap<Integer, DBEntry>();
		whereClauseFieldsMap.put(1, new DBEntry("ENTITY_CODE", context.getEntityCode(), BindParameterType.VARCHAR));
		whereClauseFieldsMap.put(2, new DBEntry("USER_ID", userID, BindParameterType.VARCHAR));
		whereClauseFieldsMap.put(3, new DBEntry("EFFT_DATE", BackOfficeFormatUtils.getDate(effectiveDate, context.getDateFormat()), BindParameterType.DATE));
		boolean isRecordAvailable = validation.isRecordAvailable("USERIPRESTRICTHIST", whereClauseFieldsMap);
		try {
			if (isRecordAvailable) {
				if (getAction().equals(ADD)) {
					getErrorMap().setError("userID", BackOfficeErrorCodes.RECORD_EXISTS);
					return false;
				}
				return true;
			} else {
				if (getAction().equals(MODIFY)) {
					getErrorMap().setError("userID", BackOfficeErrorCodes.RECORD_NOT_EXISTS);
					return false;
				}
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("userID", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateRemarks() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(remarks)) {
				getErrorMap().setError("remarks", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidRemarks(remarks)) {
				getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateUserID() {
		AccessValidator validation = new AccessValidator();
		try {
			if (!validation.isValidCode(userID)) {
				getErrorMap().setError("userID", BackOfficeErrorCodes.INVALID_CODE);
				return false;
			}
			DTObject formDTO = getFormDTO();
			formDTO.set(ContentManager.FETCH, ContentManager.FETCH_ALL);
			formDTO.set(ContentManager.USER_ID, userID);
			formDTO = validation.validateInternalUser(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("userID", formDTO.get(ContentManager.ERROR));
				return false;
			}
			if (formDTO.get(ContentManager.RESULT).equalsIgnoreCase(ContentManager.DATA_UNAVAILABLE)) {
				getErrorMap().setError("userID", BackOfficeErrorCodes.INVALID_USERID);
				validation.close();
				return false;
			}
			
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("userID", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}
	
	public DTObject validateEffectiveDate(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		AccessValidator validation = new AccessValidator();
		String efftDate = inputDTO.get("EFFT_DATE");
		String action = inputDTO.get(ContentManager.USER_ACTION);
		try {

			if (validation.isEmpty(efftDate)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_FIELD_BLANK);
				return resultDTO;
			}
			HashMap<Integer, DBEntry> whereClauseFieldsMap = new HashMap<Integer, DBEntry>();
			whereClauseFieldsMap.put(1, new DBEntry("ENTITY_CODE", context.getEntityCode(), BindParameterType.VARCHAR));
			whereClauseFieldsMap.put(2, new DBEntry("USER_ID", inputDTO.get("USER_ID"), BindParameterType.VARCHAR));
			whereClauseFieldsMap.put(3, new DBEntry("EFFT_DATE", BackOfficeFormatUtils.getDate(inputDTO.get("EFFT_DATE"), context.getDateFormat()), BindParameterType.DATE));
			boolean isRecordAvailable = validation.isRecordAvailable("USERIPRESTRICTHIST", whereClauseFieldsMap);
			if (isRecordAvailable) {
				if (action.equals(ADD)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_RECORD_EXISTS);
				}
			} else {
				if (action.equals(MODIFY)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_RECORD_NOT_EXISTS);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}


	private boolean validateGrid() {
		AccessValidator validation = new AccessValidator();
		try {
			DTDObject dtdObject = new DTDObject();
			dtdObject.addColumn(0, "SL");
			dtdObject.addColumn(1, "IPADDRESS");
			dtdObject.setXML(xmlIPList);
			if (dtdObject.getRowCount() == 0) {
				getErrorMap().setError("IPAddress", BackOfficeErrorCodes.ADD_ATLEAST_ONE_ROW);
				return false;
			}
			if (!validation.checkDuplicateRows(dtdObject, 1)) {
				getErrorMap().setError("IPAddress", BackOfficeErrorCodes.DUPLICATE_DATA);
				return false;
			}

			for (int i = 0; i < dtdObject.getRowCount(); i++) {
				if (!checkIPAddress(dtdObject.getValue(i, 1))) {
					getErrorMap().setError("IPAddress", BackOfficeErrorCodes.INVALID_IP);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("IPAddress", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	private boolean checkIPAddress(String value) {
		AccessValidator validation = new AccessValidator();
		try {
			if (validation.isEmpty(value)) {
				return false;
			}
			if (!validation.isValidIPAddress(value)) {
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("IPAddress", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public DTObject validateIP(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		AccessValidator validation = new AccessValidator();
		try {
			String ipaddress = inputDTO.get("IP_ADDRESS");
			if (validation.isEmpty(ipaddress)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			if (!validation.isValidIPAddress(ipaddress)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_IP);
				return resultDTO;
			}
			resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}
}