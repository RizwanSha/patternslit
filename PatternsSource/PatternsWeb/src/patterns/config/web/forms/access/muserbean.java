package patterns.config.web.forms.access;

import java.sql.ResultSet;
import java.util.Optional;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.CM_LOVREC;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.process.TBAActionType;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.PasswordUtils;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.AccessValidator;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.EmployeeValidator;
import patterns.config.validations.MasterValidator;
import patterns.config.web.forms.GenericFormBean;

public class muserbean extends GenericFormBean {

	private static final long serialVersionUID = 1L;

	private String userID;
	private String roleCode;
	private String empCode;
	private String titleName;
	private String dateOfJoin;
	private String emailID;
	private String branchCode;
	private String mobileNumber;
	private String hidminUserLength;
	private String userPolicy;
	private String joinDate;
	private String relivingDate;
	private String timer;
	private String createPassword;
	private String confirmPassword;
	private String pwdDelivery;
	private String pwdDeliveryType;
	private String passwordPolicy;
	private String minLength;
	private String minAlpha;
	private String minNumeric;
	private String minSpecial;
	private String minPrevent;
	private String loginNewPasswordHashed;
	private String randomSalt;
	private String remarks;

	

	public String getTitleName() {
		return titleName;
	}

	public void setTitleName(String titleName) {
		this.titleName = titleName;
	}

	public String getCreatePassword() {
		return createPassword;
	}

	public void setCreatePassword(String createPassword) {
		this.createPassword = createPassword;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public String getPwdDeliveryType() {
		return pwdDeliveryType;
	}

	public void setPwdDeliveryType(String pwdDeliveryType) {
		this.pwdDeliveryType = pwdDeliveryType;
	}

	public String getPasswordPolicy() {
		return passwordPolicy;
	}

	public void setPasswordPolicy(String passwordPolicy) {
		this.passwordPolicy = passwordPolicy;
	}

	public String getMinLength() {
		return minLength;
	}

	public void setMinLength(String minLength) {
		this.minLength = minLength;
	}

	public String getMinAlpha() {
		return minAlpha;
	}

	public void setMinAlpha(String minAlpha) {
		this.minAlpha = minAlpha;
	}

	public String getMinNumeric() {
		return minNumeric;
	}

	public void setMinNumeric(String minNumeric) {
		this.minNumeric = minNumeric;
	}

	public String getMinSpecial() {
		return minSpecial;
	}

	public void setMinSpecial(String minSpecial) {
		this.minSpecial = minSpecial;
	}

	public String getMinPrevent() {
		return minPrevent;
	}

	public void setMinPrevent(String minPrevent) {
		this.minPrevent = minPrevent;
	}

	public String getLoginNewPasswordHashed() {
		return loginNewPasswordHashed;
	}

	public void setLoginNewPasswordHashed(String loginNewPasswordHashed) {
		this.loginNewPasswordHashed = loginNewPasswordHashed;
	}

	public String getRandomSalt() {
		return randomSalt;
	}

	public void setRandomSalt(String randomSalt) {
		this.randomSalt = randomSalt;
	}

	public String getTimer() {
		return timer;
	}

	public String getEmpCode() {
		return empCode;
	}

	public void setEmpCode(String empCode) {
		this.empCode = empCode;
	}

	public void setTimer(String timer) {
		this.timer = timer;
	}

	public String getPwdDelivery() {
		return pwdDelivery;
	}

	public void setPwdDelivery(String pwdDelivery) {
		this.pwdDelivery = pwdDelivery;
	}

	public String getRelivingDate() {
		return relivingDate;
	}

	public void setRelivingDate(String relivingDate) {
		this.relivingDate = relivingDate;
	}

	public String getJoinDate() {
		return joinDate;
	}

	public void setJoinDate(String joinDate) {
		this.joinDate = joinDate;
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public String getDateOfJoin() {
		return dateOfJoin;
	}

	public void setDateOfJoin(String dateOfJoin) {
		this.dateOfJoin = dateOfJoin;
	}

	public String getRoleCode() {
		return roleCode;
	}

	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}

	public String getEmailID() {
		return emailID;
	}

	public void setEmailID(String emailID) {
		this.emailID = emailID;
	}
	
	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getHidminUserLength() {
		return hidminUserLength;
	}

	public void setHidminUserLength(String hidminUserLength) {
		this.hidminUserLength = hidminUserLength;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getUserPolicy() {
		return userPolicy;
	}

	public void setUserPolicy(String userPolicy) {
		this.userPolicy = userPolicy;
	}

	

	public void reset() {
		userID = RegularConstants.EMPTY_STRING;
		roleCode = RegularConstants.EMPTY_STRING;
		titleName = RegularConstants.EMPTY_STRING;
		dateOfJoin = RegularConstants.EMPTY_STRING;
		mobileNumber = RegularConstants.EMPTY_STRING;
		emailID = RegularConstants.EMPTY_STRING;
		branchCode = RegularConstants.EMPTY_STRING;
		remarks = RegularConstants.EMPTY_STRING;
		pwdDelivery = RegularConstants.EMPTY_STRING;
		setProcessBO("patterns.config.framework.bo.access.muserBO");

	

		AccessValidator validator = new AccessValidator();
		DTObject formDTO = getFormDTO();
		formDTO = validator.getSYSCPM(formDTO);
		if (formDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
			String minUserLength = formDTO.get("MIN_UID_LEN");
			hidminUserLength = minUserLength;
			Object[] param = new Object[] { hidminUserLength };
			setUserPolicy(getCommonResource("form.userpolicy1", param));
			minLength = formDTO.get("MIN_PWD_LEN");
			minAlpha = formDTO.get("MIN_PWD_ALPHA");
			minNumeric = formDTO.get("MIN_PWD_NUM");
			minSpecial = formDTO.get("MIN_PWD_SPECIAL");
			minPrevent = formDTO.get("PREVENT_PREV_PWD");
			pwdDelivery = formDTO.get("PWD_DELIVERY_MECHANISM");
			pwdDeliveryType = formDTO.get("PWD_MANUAL_TYPE");
			setPasswordPolicy(formDTO.get("PASSWORD_POLICY"));
		}
		validator.close();
	}

	public void validate() {
		CommonValidator commonvalidator= new CommonValidator(); 
		if (validateUserID()) {
			validateRoleCode();
			validateEmployeeCode();
			if (commonvalidator.isEmpty(empCode)) {
				validateTitleName();
				validateDateOfJoining();
				validateMobileNumber();
				validateEmailID();
				validateBranchCode();
			}
			validateRemarks();
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				String salt = null;
				String encryptedPassword = null;
				if (pwdDelivery.equals("4") && pwdDeliveryType.equals("2")) {
					salt = PasswordUtils.getSalt();
					encryptedPassword = PasswordUtils.getEncryptedHashSalt(loginNewPasswordHashed, salt);
					formDTO.set("SALT", salt);
					formDTO.set("ENCRYPT_PASSWORD", encryptedPassword);
				}
				formDTO.set("PARTITION_NO", context.getPartitionNo());
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.set("USER_ID", userID);
				formDTO.set("ROLE_CODE", roleCode);
				formDTO.set("REMARKS", remarks);
				formDTO.set("BRANCH_CODE", branchCode);
				formDTO.set("PWD_DELIVERY", pwdDelivery);
				formDTO.set("USER_NAME", titleName);
				formDTO.set("DATE_OF_JOINING", dateOfJoin);
				formDTO.set("MOBILE_NUMBER", mobileNumber);
				formDTO.set("EMAIL_ID", emailID);
				if (!commonvalidator.isEmpty(empCode)) {
					formDTO.set("EMP_CODE", empCode);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("userID", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}finally{
			commonvalidator.close();
		}
	}

	public boolean validateUserID() {
		AccessValidator validation = new AccessValidator();
		DTObject formDTO = getFormDTO();
		try {
			if (validation.isEmpty(userID)) {
				getErrorMap().setError("userID", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			formDTO.set("USER_ID", userID);
			formDTO.set("MIN_LENGTH", hidminUserLength);
			formDTO.set(ContentManager.USER_ACTION, getAction());
			formDTO = validateUser(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("userID", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("userID", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public DTObject validateUserId(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		AccessValidator validation = new AccessValidator();
		String userId = inputDTO.get("USER_ID");
		String action = inputDTO.get(ContentManager.USER_ACTION);
		try {
			if (validation.isEmpty(userId)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			if (userId.equals(context.getUserID())) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.CANNOT_ALLOCATE_TO_SELF);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH, ContentManager.FETCH_ALL);
			resultDTO = validation.validateInternalUser(inputDTO);
			if (action.equals(ADD)) {
				if (ContentManager.DATA_AVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_EXISTS);
					return resultDTO;
				}
			} else {
				if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_NOT_EXISTS);
					return resultDTO;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public DTObject validateUser(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		AccessValidator validation = new AccessValidator();
		String userId = inputDTO.get("USER_ID");
		String action = inputDTO.get(ContentManager.USER_ACTION);
		try {
			if (validation.isEmpty(userId)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			if (!validation.isValidUserID(userId)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_FORMAT);
				return resultDTO;
			}
			int userIDLength = userId.length();
			int minUserLength = Integer.parseInt(inputDTO.get("MIN_LENGTH"));
			if (action.equals(ADD)) {
				if (userIDLength < minUserLength) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_USERID_LENGTH);
					return resultDTO;
				}
			}
			if (userId.equals(context.getUserID())) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.CANNOT_ALLOCATE_TO_SELF);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH, ContentManager.FETCH_ALL);
			resultDTO = validation.validateInternalUser(inputDTO);
			if (action.equals(ADD)) {
				if (ContentManager.DATA_AVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_EXISTS);
					return resultDTO;
				}
			} else {
				if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_NOT_EXISTS);
					return resultDTO;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	// public boolean validateUsername() {
	// CommonValidator validation = new CommonValidator();
	// try {
	// if (validation.isEmpty(userName)) {
	// getErrorMap().setError("userName", BackOfficeErrorCodes.FIELD_BLANK);
	// return false;
	// }
	// if (!validation.isValidName(userName)) {
	// getErrorMap().setError("userName", BackOfficeErrorCodes.INVALID_NAME);
	// return false;
	// }
	// return true;
	// } catch (Exception e) {
	// e.printStackTrace();
	// getErrorMap().setError("userName",
	// BackOfficeErrorCodes.UNSPECIFIED_ERROR);
	// } finally {
	// validation.close();
	// }
	// return false;
	// }

	public boolean validateRoleCode() {
		try {
			DTObject formDTO = getFormDTO();
			formDTO.set("ROLE_CODE", roleCode);
			formDTO.set(ContentManager.ACTION, USAGE);
			formDTO = validateRoleCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("roleCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("roleCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	private boolean validateEmployeeCode() {
		try {
			DTObject formDTO = getFormDTO();
			formDTO.set("EMP_CODE", empCode);
			formDTO.set("USER_ID", userID);
			formDTO.set(ContentManager.ACTION, USAGE);
			formDTO = validateEmployeeCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("empCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("empCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateEmployeeCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		EmployeeValidator validation = new EmployeeValidator();
		String employeeCode = inputDTO.get("STAFF_CODE");
		String userId = inputDTO.get("USER_ID");
		boolean alreadyActiveUser = false;
		try {
			if (!validation.isEmpty(employeeCode)) {
				inputDTO.set(ContentManager.FETCH_COLUMNS, "NAME,EMAIL_ID,MOBILE_NO,TO_CHAR(DATE_OF_JOINING,'" + BackOfficeConstants.TBA_DATE_FORMAT + "') DATE_OF_JOINING,BRANCH_CODE");
				resultDTO = validation.validateStaffEmpCode(inputDTO);
				if (resultDTO.get(ContentManager.ERROR) != null) {
					resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
					return resultDTO;
				}
				else if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
					return resultDTO;
				}
				else if (ContentManager.DATA_AVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					String branchCode =resultDTO.get("BRANCH_CODE");
					Optional<String> opBranch= Optional.ofNullable(branchCode);
					if(!opBranch.isPresent() || RegularConstants.EMPTY_STRING.equals(branchCode)){
						resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.BRANCH_NOT_EXIST_USER);
						return resultDTO;
					}
				}
				inputDTO.set(ContentManager.FETCH_COLUMNS, "EMP_ID");
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

//	private boolean isAlreadyActiveUser(String ColumnName, String ColumnValue, String userId, DTObject resultDTO) {
//		DBContext dbContext = new DBContext();
//		DBUtil util = dbContext.createUtilInstance();
//		String sqlQuery = RegularConstants.EMPTY_STRING;
//		try {
//			sqlQuery = "SELECT COUNT(1),USER_ID FROM USERS WHERE STATUS='A' AND ENTITY_CODE=? AND " + ColumnName + "=? AND USER_ID <>?";
//			util.reset();
//			util.setSql(sqlQuery);
//			util.setInt(1, Integer.parseInt(context.getEntityCode()));
//			util.setString(2, ColumnValue);
//			util.setString(3, userId);
//			ResultSet rset = util.executeQuery();
//			if (rset.next()) {
//				if (rset.getInt(1) > 0) {
//					Object[] errorParam = { rset.getString(2) };
//					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_PERSN_ALREADY_ACTIVE);
//					resultDTO.setObject(ContentManager.ERROR_PARAM, errorParam);
//					return true;
//				}
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//		} finally {
//			util.reset();
//			dbContext.close();
//		}
//		return false;
//	}

	public boolean validateTitleName() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(titleName)) {
				getErrorMap().setError("titleName", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidName(titleName)) {
				getErrorMap().setError("titleName", BackOfficeErrorCodes.INVALID_NAME);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("titleName", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public DTObject validateUserLoginCheck(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		String sqlQuery = RegularConstants.EMPTY_STRING;
		String userId = inputDTO.get("USER_ID");
		try {
			sqlQuery = "SELECT COUNT(1) FROM USERSLOGINHIST WHERE ENTITY_CODE=? AND USER_ID=?";
			util.reset();
			util.setSql(sqlQuery);
			util.setInt(1, Integer.parseInt(context.getEntityCode()));
			util.setString(2, userId);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				if (rset.getInt(1) > 0) {
					resultDTO.set("ALREADY_LOGIN", RegularConstants.ONE);
					return resultDTO;
				} else
					resultDTO.set("ALREADY_LOGIN", RegularConstants.ZERO);
			} else
				resultDTO.set("ALREADY_LOGIN", RegularConstants.ZERO);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			util.reset();
			dbContext.close();
		}
		return resultDTO;
	}

	public DTObject validateRoleCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		AccessValidator validation = new AccessValidator();
		String roleCode = inputDTO.get("ROLE_CODE");
		try {
			if (validation.isEmpty(roleCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH, ContentManager.FETCH_ALL);
			inputDTO.set(ContentManager.CODE, roleCode);
			resultDTO = validation.validateInternalRole(inputDTO);
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INSUFFICIENT_ROLE_RIGHTS);
				return resultDTO;
			}
			String roleType = resultDTO.get("ROLE_TYPE");
			if (context.isInternalAdministrator()) {
				if (!roleType.equals(CM_LOVREC.COMMON_ROLES_1) && !roleType.equals(CM_LOVREC.COMMON_ROLES_2) && !roleType.equals(CM_LOVREC.COMMON_ROLES_3) && !roleType.equals(CM_LOVREC.COMMON_ROLES_4)) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INSUFFICIENT_ROLE_RIGHTS);
					return resultDTO;
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}


	public boolean validateDateOfJoining() {
		CommonValidator validation = new CommonValidator();
		try {
			TBAActionType AT;
			if (getAction().equals(ADD))
				AT = TBAActionType.ADD;
			else
				AT = TBAActionType.MODIFY;
			if (validation.isEmpty(dateOfJoin)) {
				getErrorMap().setError("dateOfJoin", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			java.util.Date dateInstance = validation.isValidDate(dateOfJoin);
			if (dateInstance == null) {
				getErrorMap().setError("dateOfJoin", BackOfficeErrorCodes.INVALID_DATE);
				return false;
			}
				if (!validation.isEffectiveDateValid(dateOfJoin, AT)) {
					getErrorMap().setError("dateOfJoin", BackOfficeErrorCodes.DATE_GECBD);
					return false;
				}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("dateOfJoin", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}


	public boolean validateEmailID() {
		AccessValidator validation = new AccessValidator();
		try {
			if (!validation.isEmpty(emailID)) {
				if (!validation.isValidEmail(emailID)) {
					getErrorMap().setError("emailID", BackOfficeErrorCodes.INVALID_EMAIL_ID);
					return false;
				}
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("emailID", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}
	
	public boolean validateBranchCode() {
		try {
			DTObject formDTO = getFormDTO();
			formDTO.set("BRANCH_CODE", branchCode);
			formDTO.set(ContentManager.USER_ACTION, getAction());
			formDTO = validateBranchCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("branchCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("branchCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateBranchCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		MasterValidator validation = new MasterValidator();
		branchCode = inputDTO.get("BRANCH_CODE");
		try {
			if (validation.isEmpty(branchCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "BRANCH_NAME");
			resultDTO = validation.validateBranchCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateMobileNumber() {
		AccessValidator validation = new AccessValidator();
		try {
			if (!validation.isEmpty(mobileNumber)) {
				if (!validation.isValidPhoneNumber(mobileNumber)) {
					getErrorMap().setError("mobileNumber", BackOfficeErrorCodes.HMS_INVALID_MOBILE);
					return false;
				}
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("mobileNumber", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateRemarks() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (remarks != null && remarks.length() > 0) {
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			if (getAction().equals(MODIFY)) {
				if (commonValidator.isEmpty(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}

	public DTObject getPwdDeliveryChoice(DTObject inputDTO) {
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		try {
			String sql = "SELECT PWD_DELIVERY_MECHANISM FROM SYSCPM WHERE ENTITY_CODE=? AND EFFT_DATE <= FN_GETCD(?)";
			util.reset();
			util.setSql(sql);
			util.setString(1, context.getEntityCode());
			util.setString(2, context.getEntityCode());
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				inputDTO.set("PWD_DELIVERY_MECHANISM", rset.getString("PWD_DELIVERY_MECHANISM"));
			}
		} catch (Exception e) {
			e.printStackTrace();
			inputDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			dbContext.close();
			util.reset();
		}
		return inputDTO;
	}
}
