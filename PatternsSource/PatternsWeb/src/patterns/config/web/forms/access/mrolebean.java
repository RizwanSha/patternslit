package patterns.config.web.forms.access;

import java.util.ArrayList;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.GenericOption;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.AccessValidator;
import patterns.config.validations.CommonValidator;
import patterns.config.web.forms.GenericFormBean;

public class mrolebean extends GenericFormBean {

	private static final long serialVersionUID = -6219657363150466160L;

	private String roleCode;
	private String roleDescription;
	private String roleType;
	private boolean enabled;
	private String remarks;

	public String getRemarks() {
		return remarks;
	}

	public String getRoleCode() {
		return roleCode;
	}

	public String getRoleDescription() {
		return roleDescription;
	}

	public String getRoleType() {
		return roleType;
	}

	
	public boolean isEnabled() {
		return enabled;
	}

	
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public void reset() {
		roleCode = RegularConstants.EMPTY_STRING;
		roleDescription = RegularConstants.EMPTY_STRING;
		roleType = RegularConstants.EMPTY_STRING;
		enabled = false;
		remarks = RegularConstants.EMPTY_STRING;
		DBContext dbContext = new DBContext();
		if (context.isInternalAdministrator()) {
			ArrayList<GenericOption> roleTypeList = null;
			roleTypeList = getGenericOptions("COMMON", "ADMROLES", dbContext, "--");
			webContext.getRequest().setAttribute("COMMON_ROLES", roleTypeList);
		}
		if (context.isInternalOperations()) {
			ArrayList<GenericOption> roleTypeList = null;
			roleTypeList = getGenericOptions("COMMON", "OPRROLES", dbContext, "--");
			webContext.getRequest().setAttribute("COMMON_ROLES", roleTypeList);
		}
		dbContext.close();
		setProcessBO("patterns.config.framework.bo.access.mroleBO");
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}

	public void setRoleDescription(String roleDescription) {
		this.roleDescription = roleDescription;
	}

	public void setRoleType(String roleType) {
		this.roleType = roleType;
	}

	
	@Override
	public void validate() {
		if (validateRoleCode()) {
			validateRoleDescription();
			validateRoleType();
			validateRemarks();
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.set("CODE", roleCode);
				formDTO.set("DESCRIPTION", roleDescription);
				if (getAction().equals(ADD)) {
					formDTO.set("ENABLED", decodeBooleanToString(true));
					formDTO.set("ROLE_TYPE", roleType);
				} else {
					formDTO.set("ENABLED", decodeBooleanToString(enabled));
				}
				formDTO.set("REMARKS", remarks);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("roleCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
	}

	public boolean validateRemarks() {
		CommonValidator commonValidator = new CommonValidator();
		try {
				if (remarks!=null && remarks.length()>0) {
					if(!commonValidator.isValidRemarks(remarks)){
						getErrorMap().setError("remarks", BackOfficeErrorCodes.HMS_INVALID_REMARKS);
						return false;
					}
				}
				if (getAction().equals(MODIFY)) {
					if (commonValidator.isEmpty(remarks)) {
						getErrorMap().setError("remarks", BackOfficeErrorCodes.HMS_FIELD_BLANK);
						return false;
					}
					if(!commonValidator.isValidRemarks(remarks)){
						getErrorMap().setError("remarks", BackOfficeErrorCodes.HMS_INVALID_REMARKS);
						return false;
					}
				}
				return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}
	

	public boolean validateRoleCode() {
		AccessValidator validation = new AccessValidator();
		try {
			if (validation.isEmpty(roleCode)) {
				getErrorMap().setError("roleCode", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidCode(roleCode)) {
				getErrorMap().setError("roleCode", BackOfficeErrorCodes.INVALID_FORMAT);
				return false;
			}
			DTObject formDTO = getFormDTO();
			formDTO.set(ContentManager.FETCH, ContentManager.FETCH_ALL);
			formDTO.set(ContentManager.CODE, roleCode);
			formDTO.set(ContentManager.USER_ACTION, getAction());
			formDTO = validation.validateInternalRole(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("roleCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			if (getAction().equals(ADD)) {
				if (formDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
					getErrorMap().setError("roleCode", BackOfficeErrorCodes.RECORD_EXISTS);
					return false;
				}
				return true;
			} else if (getAction().equals(MODIFY)) {
				if (formDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
					getErrorMap().setError("roleCode", BackOfficeErrorCodes.RECORD_NOT_EXISTS);
					return false;
				}
				String roleType = formDTO.get("ROLE_TYPE");
				if (context.isInternalAdministrator()) {
					if (!validation.isCMLOVREC("COMMON", "ADMROLES", roleType)) {
						getErrorMap().setError("roleType", BackOfficeErrorCodes.INSUFFICIENT_ROLE_RIGHTS);
						return false;
					}
					return true;
				}

			} else {
				getErrorMap().setError("roleCode", BackOfficeErrorCodes.INVALID_ACTION);
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("roleCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public DTObject validateRoleCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		AccessValidator validation = new AccessValidator();
		String roleCode = inputDTO.get("ROLE_CODE");
		String action = inputDTO.get(ContentManager.USER_ACTION);
		try {

			if (validation.isEmpty(roleCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH, ContentManager.FETCH_ALL);
			inputDTO.set(ContentManager.CODE, roleCode);
			resultDTO = validation.validateInternalRole(inputDTO);
			if (action.equals(ADD)) {
				if (ContentManager.DATA_AVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_RECORD_EXISTS);
				}
			} else {
				if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_RECORD_NOT_EXISTS);

				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateRoleDescription() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(roleDescription)) {
				getErrorMap().setError("roleDescription", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidDescription(roleDescription)) {
				getErrorMap().setError("roleDescription", BackOfficeErrorCodes.INVALID_DESCRIPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("roleDescription", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateRoleType() {
		CommonValidator validation = new CommonValidator();
		try {
			if (getAction().equals(MODIFY)) {
				return true;
			}
			if (validation.isEmpty(roleType)) {
				getErrorMap().setError("roleType", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (context.isInternalAdministrator()) {
				if (!validation.isCMLOVREC("COMMON", "ADMROLES", roleType)) {
					getErrorMap().setError("roleType", BackOfficeErrorCodes.INSUFFICIENT_ROLE_RIGHTS);
					return false;
				}
			}
			if (context.isInternalOperations()) {
				if (!validation.isCMLOVREC("COMMON", "OPRROLES", roleType)) {
					getErrorMap().setError("roleType", BackOfficeErrorCodes.INSUFFICIENT_ROLE_RIGHTS);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("roleType", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}
}
