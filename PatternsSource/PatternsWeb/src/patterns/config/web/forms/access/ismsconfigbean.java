/**This program allows maintenance of the sms server configuration.
 * @version 1.0
 * @author  ChandraKala P
 * @since   13-May-2016
 * <br>
 * <b>Modified History</b>
 * <BR>
 * <U><B>
 * Sl.No		Modified Date	Author			Modified Changes		Version
 * </B></U>
 * <br>
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 */
package patterns.config.web.forms.access;

import java.util.ArrayList;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.process.TBAActionType;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.framework.web.GenericOption;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.AccessValidator;
import patterns.config.validations.CommonValidator;
import patterns.config.web.forms.GenericFormBean;

public class ismsconfigbean extends GenericFormBean {
	private static final long serialVersionUID = 1L;

	private String smsCode;
	private String effectiveDate;
	private boolean httpAllowed;
	private boolean sslRequired;
	/** Communication Details */
	private String senderNumber;
	private String serverUrl;
	/** Proxy Configuration Details */
	private boolean proxyRequired;
	private String proxyServer;
	private String proxyServerPort;
	private boolean proxyServerAuthentication;
	private String proxyAccountName;
	private String proxyAccountPassword;
	/** ismsconfig.section4=Outgoing Server Details */
	private boolean authenticationRequired;
	private String userNameKey;
	private String accountName;
	private String accountPasswordKey;
	private String accountPassword;
	private String mobileNumberKey;
	private String messageKey;

	/** SMPP Configuration */
	private boolean smppAllowed;
	private String systemType;
	private String addressRange;
	private boolean transceiverEnabled;
	private String mainServer;
	private String mainServerPort;
	private String failOverServer;
	private String failOverServerPort;
	private String dispatchType;
	private String retryTimeout;
	private String remarks;

	private String sendAllowed;

	public String getSmsCode() {
		return smsCode;
	}

	public void setSmsCode(String smsCode) {
		this.smsCode = smsCode;
	}

	public String getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public boolean isHttpAllowed() {
		return httpAllowed;
	}

	public void setHttpAllowed(boolean httpAllowed) {
		this.httpAllowed = httpAllowed;
	}

	public boolean isSslRequired() {
		return sslRequired;
	}

	public void setSslRequired(boolean sslRequired) {
		this.sslRequired = sslRequired;
	}

	public String getSenderNumber() {
		return senderNumber;
	}

	public void setSenderNumber(String senderNumber) {
		this.senderNumber = senderNumber;
	}

	public String getServerUrl() {
		return serverUrl;
	}

	public void setServerUrl(String serverUrl) {
		this.serverUrl = serverUrl;
	}

	public boolean isProxyRequired() {
		return proxyRequired;
	}

	public void setProxyRequired(boolean proxyRequired) {
		this.proxyRequired = proxyRequired;
	}

	public String getProxyServer() {
		return proxyServer;
	}

	public void setProxyServer(String proxyServer) {
		this.proxyServer = proxyServer;
	}

	public String getProxyServerPort() {
		return proxyServerPort;
	}

	public void setProxyServerPort(String proxyServerPort) {
		this.proxyServerPort = proxyServerPort;
	}

	public boolean isProxyServerAuthentication() {
		return proxyServerAuthentication;
	}

	public void setProxyServerAuthentication(boolean proxyServerAuthentication) {
		this.proxyServerAuthentication = proxyServerAuthentication;
	}

	public String getProxyAccountName() {
		return proxyAccountName;
	}

	public void setProxyAccountName(String proxyAccountName) {
		this.proxyAccountName = proxyAccountName;
	}

	public String getProxyAccountPassword() {
		return proxyAccountPassword;
	}

	public void setProxyAccountPassword(String proxyAccountPassword) {
		this.proxyAccountPassword = proxyAccountPassword;
	}

	public boolean isAuthenticationRequired() {
		return authenticationRequired;
	}

	public void setAuthenticationRequired(boolean authenticationRequired) {
		this.authenticationRequired = authenticationRequired;
	}

	public String getUserNameKey() {
		return userNameKey;
	}

	public void setUserNameKey(String userNameKey) {
		this.userNameKey = userNameKey;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public String getAccountPassword() {
		return accountPassword;
	}

	public void setAccountPassword(String accountPassword) {
		this.accountPassword = accountPassword;
	}

	public String getMobileNumberKey() {
		return mobileNumberKey;
	}

	public void setMobileNumberKey(String mobileNumberKey) {
		this.mobileNumberKey = mobileNumberKey;
	}

	public String getMessageKey() {
		return messageKey;
	}

	public void setMessageKey(String messageKey) {
		this.messageKey = messageKey;
	}

	public boolean isSmppAllowed() {
		return smppAllowed;
	}

	public void setSmppAllowed(boolean smppAllowed) {
		this.smppAllowed = smppAllowed;
	}

	public String getSystemType() {
		return systemType;
	}

	public void setSystemType(String systemType) {
		this.systemType = systemType;
	}

	public String getAddressRange() {
		return addressRange;
	}

	public void setAddressRange(String addressRange) {
		this.addressRange = addressRange;
	}

	public boolean isTransceiverEnabled() {
		return transceiverEnabled;
	}

	public void setTransceiverEnabled(boolean transceiverEnabled) {
		this.transceiverEnabled = transceiverEnabled;
	}

	public String getMainServer() {
		return mainServer;
	}

	public void setMainServer(String mainServer) {
		this.mainServer = mainServer;
	}

	public String getMainServerPort() {
		return mainServerPort;
	}

	public void setMainServerPort(String mainServerPort) {
		this.mainServerPort = mainServerPort;
	}

	public String getFailOverServer() {
		return failOverServer;
	}

	public void setFailOverServer(String failOverServer) {
		this.failOverServer = failOverServer;
	}

	public String getFailOverServerPort() {
		return failOverServerPort;
	}

	public void setFailOverServerPort(String failOverServerPort) {
		this.failOverServerPort = failOverServerPort;
	}

	public String getDispatchType() {
		return dispatchType;
	}

	public void setDispatchType(String dispatchType) {
		this.dispatchType = dispatchType;
	}

	public String getRetryTimeout() {
		return retryTimeout;
	}

	public void setRetryTimeout(String retryTimeout) {
		this.retryTimeout = retryTimeout;
	}

	public String getRemarks() {
		return remarks;
	}

	public String getAccountPasswordKey() {
		return accountPasswordKey;
	}

	public void setAccountPasswordKey(String accountPasswordKey) {
		this.accountPasswordKey = accountPasswordKey;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getSendAllowed() {
		return sendAllowed;
	}

	public void setSendAllowed(String sendAllowed) {
		this.sendAllowed = sendAllowed;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public void reset() {
		smsCode = RegularConstants.EMPTY_STRING;
		effectiveDate = RegularConstants.EMPTY_STRING;
		httpAllowed = false;
		sslRequired = false;
		senderNumber = RegularConstants.EMPTY_STRING;
		serverUrl = RegularConstants.EMPTY_STRING;
		proxyRequired = false;
		proxyServer = RegularConstants.EMPTY_STRING;
		proxyServerPort = RegularConstants.EMPTY_STRING;
		proxyServerAuthentication = false;
		proxyAccountName = RegularConstants.EMPTY_STRING;
		proxyAccountPassword = RegularConstants.EMPTY_STRING;
		authenticationRequired = false;
		userNameKey = RegularConstants.EMPTY_STRING;
		accountName = RegularConstants.EMPTY_STRING;
		accountPasswordKey = RegularConstants.EMPTY_STRING;
		accountPassword = RegularConstants.EMPTY_STRING;
		mobileNumberKey = RegularConstants.EMPTY_STRING;
		messageKey = RegularConstants.EMPTY_STRING;
		smppAllowed = false;
		systemType = RegularConstants.EMPTY_STRING;
		addressRange = RegularConstants.EMPTY_STRING;
		transceiverEnabled = false;
		mainServer = RegularConstants.EMPTY_STRING;
		mainServerPort = RegularConstants.EMPTY_STRING;
		failOverServer = RegularConstants.EMPTY_STRING;
		failOverServerPort = RegularConstants.EMPTY_STRING;
		dispatchType = RegularConstants.EMPTY_STRING;
		retryTimeout = RegularConstants.EMPTY_STRING;
		remarks = RegularConstants.EMPTY_STRING;
		sendAllowed = RegularConstants.EMPTY_STRING;

		DBContext dbContext = new DBContext();
		ArrayList<GenericOption> smsType = null;
		smsType = getGenericOptions("COMMON", "SMSTYPE", dbContext, "--");
		webContext.getRequest().setAttribute("COMMON_SMSTYPE", smsType);
		dbContext.close();
		setProcessBO("patterns.config.framework.bo.access.ismsconfigBO");
	}

	public void validate() {
		if (validateSMSCode() && validateEffectDate()) {
			if (httpAllowed) {
				validateServerURL();
				if (proxyRequired) {
					validateProxyServer();
					validateProxyServerPort();
				}
				if (proxyServerAuthentication) {
					validateProxyAccountName();
					validateProxyAccountPassword();
				}
				if (authenticationRequired) {
					validateUserNameKey();
					validateAccountName();
					validateAccountPasswordKey();
					validateAccountPassword();
					validateMobileNumberKey();
					validateMessageKey();
				}
			}
			if (smppAllowed) {
				validateSourceNumber();
				validateSystemType();
				validateAddressRange();
				validateDispatchType();
				validateMainServer();
				validateMainServerPort();
				validateFailOverServer();
				validateFailServerPort();
				if (dispatchType.equals("P")) {
					validateRetryTimeout();
				}
			}
			validateRemarks();
		}

		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.setObject("EFFT_DATE", BackOfficeFormatUtils.getDate(effectiveDate, context.getDateFormat()));
				formDTO.set("SMS_CODE", smsCode);
				formDTO.set("SMSC_HTTP_ALLOWED", decodeBooleanToString(httpAllowed));
				formDTO.set("SSL_REQUIRED", decodeBooleanToString(sslRequired));
				if (httpAllowed) {
					formDTO.set("SERVER_URL", serverUrl);
				} else {
					formDTO.set("SOURCE_NUMBER", RegularConstants.NULL);
					formDTO.set("SERVER_URL", RegularConstants.NULL);
				}
				formDTO.set("PROXY_REQUIRED", decodeBooleanToString(proxyRequired));
				if (proxyRequired) {
					formDTO.set("PROXY_SERVER", proxyServer);
					formDTO.set("PROXY_SERVER_PORT", proxyServerPort);
				} else {
					formDTO.set("PROXY_SERVER", RegularConstants.NULL);
					formDTO.set("PROXY_SERVER_PORT", RegularConstants.NULL);
				}
				formDTO.set("PROXY_AUTH_REQD", decodeBooleanToString(proxyServerAuthentication));
				if (proxyServerAuthentication) {
					formDTO.set("PROXY_USER_NAME", proxyAccountName);
					formDTO.set("PROXY_PASSWORD", proxyAccountPassword);
				} else {
					formDTO.set("PROXY_USER_NAME", RegularConstants.NULL);
					formDTO.set("PROXY_PASSWORD", RegularConstants.NULL);
				}
				formDTO.set("AUTH_REQD", decodeBooleanToString(authenticationRequired));
				if (authenticationRequired) {
					formDTO.set("USER_NAME_KEY", userNameKey);
					formDTO.set("USER_NAME", accountName);
					formDTO.set("PASSWORD_KEY", accountPasswordKey);
					formDTO.set("PASSWORD", accountPassword);
					formDTO.set("MOBILE_NUMBER_KEY", mobileNumberKey);
					formDTO.set("MESSAGE_KEY", messageKey);
				} else {
					formDTO.set("USER_NAME_KEY", RegularConstants.NULL);
					formDTO.set("USER_NAME", RegularConstants.NULL);
					formDTO.set("PASSWORD_KEY", RegularConstants.NULL);
					formDTO.set("PASSWORD", RegularConstants.NULL);
					formDTO.set("MOBILE_NUMBER_KEY", RegularConstants.NULL);
					formDTO.set("MESSAGE_KEY", RegularConstants.NULL);
				}
				formDTO.set("SMSC_SMPP_ALLOWED", decodeBooleanToString(smppAllowed));
				if (smppAllowed) {
					formDTO.set("SOURCE_NUMBER", senderNumber);
					formDTO.set("SYSTEM_TYPE", systemType);
					formDTO.set("ADDRESS_RANGE", addressRange);
					formDTO.set("TRANSCEIVER_MODE", decodeBooleanToString(transceiverEnabled));
					formDTO.set("MAIN_SMPP_SERVER", mainServer);
					formDTO.set("MAIN_SMPP_PORT", mainServerPort);
					formDTO.set("FAILOVER_SMPP_SERVER", failOverServer);
					formDTO.set("FAILOVER_SMPP_PORT", failOverServerPort);
					formDTO.set("SMS_TYPE", dispatchType);
				} else {
					formDTO.set("SYSTEM_TYPE", RegularConstants.NULL);
					formDTO.set("ADDRESS_RANGE", RegularConstants.NULL);
					formDTO.set("TRANSCEIVER_MODE", decodeBooleanToString(transceiverEnabled));
					formDTO.set("MAIN_SMPP_SERVER", RegularConstants.NULL);
					formDTO.set("MAIN_SMPP_PORT", RegularConstants.NULL);
					formDTO.set("FAILOVER_SMPP_SERVER", RegularConstants.NULL);
					formDTO.set("FAILOVER_SMPP_PORT", RegularConstants.NULL);
					formDTO.set("SMS_TYPE", RegularConstants.NULL);
				}
				if (dispatchType.equals("P")) {
					formDTO.set("RETRY_TIMEOUT", retryTimeout);
				} else {
					formDTO.set("RETRY_TIMEOUT", RegularConstants.NULL);
				}
				formDTO.set("REMARKS", remarks);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("smsCode", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		}
	}

	public boolean validateSMSCode() {
		try {
			DTObject formDTO = getFormDTO();
			formDTO.set("SMS_CODE", smsCode);
			formDTO.set("ACTION", ContentManager.USER_ACTION);
			formDTO = validateSMSCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("smsCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("smsCode", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateSMSCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		AccessValidator validation = new AccessValidator();
		String smsCode = inputDTO.get("SMS_CODE");
		try {
			if (validation.isEmpty(smsCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "SMS_DESCN,SMS_SEND_ALLOWED");
			resultDTO = validation.validateSMSCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_CODE);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateEffectDate() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("ACTION", getAction());
			formDTO.set("SMS_CODE", smsCode);
			formDTO.set("EFFT_DATE", effectiveDate);
			formDTO = validateEffectiveDate(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("effectiveDate", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("effectiveDate", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateEffectiveDate(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		CommonValidator validation = new CommonValidator();
		String effectiveDate = inputDTO.get("EFFT_DATE");
		String smsCode = inputDTO.get("SMS_CODE");
		String action = inputDTO.get(ContentManager.USER_ACTION);
		try {
			if (validation.isEmpty(effectiveDate)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_FIELD_BLANK);
				return resultDTO;
			}
			TBAActionType AT;
			if (action.equals(ADD))
				AT = TBAActionType.ADD;
			else
				AT = TBAActionType.MODIFY;
			if (!validation.isEffectiveDateValid(effectiveDate, AT)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_EFFECTIVE_DATE);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "SMS_CODE");
			inputDTO.set(ContentManager.TABLE, "SMSINTFCFG");
			String columns[] = new String[] { context.getEntityCode(), smsCode, effectiveDate };
			inputDTO.setObject(ContentManager.PROGRAM_KEY, columns);
			resultDTO = validation.validatePK(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (action.equals(ADD)) {
				if (ContentManager.DATA_AVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_RECORD_EXISTS);
					return resultDTO;
				}
			} else if (action.equals(MODIFY)) {
				if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_RECORD_NOT_EXISTS);
					return resultDTO;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateServerURL() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(serverUrl)) {
				getErrorMap().setError("serverUrl", BackOfficeErrorCodes.HMS_FIELD_BLANK);
				return false;
			}
			if (!validation.isValidUrl(serverUrl)) {
				getErrorMap().setError("serverUrl", BackOfficeErrorCodes.HMS_INVALID_URL);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("serverUrl", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateProxyServer() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(proxyServer)) {
				getErrorMap().setError("proxyServer", BackOfficeErrorCodes.HMS_FIELD_BLANK);
				return false;
			}
			if (!validation.isValidServerAddress(proxyServer)) {
				getErrorMap().setError("proxyServer", BackOfficeErrorCodes.HMS_INVALID_SERVER_ADDRESS);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("proxyServer", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateProxyServerPort() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(proxyServerPort)) {
				getErrorMap().setError("proxyServerPort", BackOfficeErrorCodes.HMS_FIELD_BLANK);
				return false;
			}
			if (!validation.isValidNumber(proxyServerPort)) {
				getErrorMap().setError("proxyServerPort", BackOfficeErrorCodes.HMS_INVALID_NUMBER);
				return false;
			}
			if (!validation.isValidPort(proxyServerPort)) {
				getErrorMap().setError("proxyServerPort", BackOfficeErrorCodes.HMS_INVALID_PORT);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("proxyServerPort", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateProxyAccountName() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(proxyAccountName)) {
				getErrorMap().setError("proxyAccountName", BackOfficeErrorCodes.HMS_FIELD_BLANK);
				return false;
			}
			if (!validation.isValidName(proxyAccountName)) {
				getErrorMap().setError("proxyAccountName", BackOfficeErrorCodes.HMS_INVALID_NAME);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("proxyAccountName", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateProxyAccountPassword() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(proxyAccountPassword)) {
				getErrorMap().setError("proxyAccountPassword", BackOfficeErrorCodes.HMS_FIELD_BLANK);
				return false;
			}
			if (!validation.isValidCredential(proxyAccountPassword)) {
				getErrorMap().setError("proxyAccountPassword", BackOfficeErrorCodes.HMS_INVALID_CREDENTIALS);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("proxyAccountPassword", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateUserNameKey() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(userNameKey)) {
				getErrorMap().setError("userNameKey", BackOfficeErrorCodes.HMS_FIELD_BLANK);
				return false;
			}
			if (!validation.isValidDescription(userNameKey, 50)) {
				getErrorMap().setError("userNameKey", BackOfficeErrorCodes.HMS_INVALID_DESCRIPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("userNameKey", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateAccountName() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(accountName)) {
				getErrorMap().setError("accountName", BackOfficeErrorCodes.HMS_FIELD_BLANK);
				return false;
			}
			if (!validation.isValidDescription(accountName, 50)) {
				getErrorMap().setError("accountName", BackOfficeErrorCodes.HMS_INVALID_DESCRIPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("accountName", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateAccountPasswordKey() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(accountPasswordKey)) {
				getErrorMap().setError("accountPasswordKey", BackOfficeErrorCodes.HMS_FIELD_BLANK);
				return false;
			}
			if (!validation.isValidDescription(accountPasswordKey, 50)) {
				getErrorMap().setError("accountPasswordKey", BackOfficeErrorCodes.HMS_INVALID_DESCRIPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("accountPasswordKey", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateAccountPassword() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(accountPassword)) {
				getErrorMap().setError("accountPassword", BackOfficeErrorCodes.HMS_FIELD_BLANK);
				return false;
			}
			if (!validation.isValidCredential(accountPassword)) {
				getErrorMap().setError("accountPassword", BackOfficeErrorCodes.HMS_INVALID_CREDENTIALS);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("accountPassword", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateMobileNumberKey() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(mobileNumberKey)) {
				getErrorMap().setError("mobileNumberKey", BackOfficeErrorCodes.HMS_FIELD_BLANK);
				return false;
			}
			if (!validation.isValidDescription(mobileNumberKey, 50)) {
				getErrorMap().setError("mobileNumberKey", BackOfficeErrorCodes.HMS_INVALID_DESCRIPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("mobileNumberKey", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateMessageKey() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(messageKey)) {
				getErrorMap().setError("messageKey", BackOfficeErrorCodes.HMS_FIELD_BLANK);
				return false;
			}
			if (!validation.isValidDescription(messageKey, 50)) {
				getErrorMap().setError("messageKey", BackOfficeErrorCodes.HMS_INVALID_DESCRIPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("messageKey", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateSourceNumber() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(senderNumber)) {
				getErrorMap().setError("senderNumber", BackOfficeErrorCodes.HMS_FIELD_BLANK);
				return false;
			}
			if (!validation.isValidNumber(senderNumber)) {
				getErrorMap().setError("senderNumber", BackOfficeErrorCodes.HMS_INVALID_NUMBER);
				return false;
			}
			if (validation.isZero(senderNumber)) {
				getErrorMap().setError("senderNumber", BackOfficeErrorCodes.HMS_ZERO_CHECK);
				return false;
			}
			if (!validation.isValidPositiveNumber(senderNumber)) {
				getErrorMap().setError("senderNumber", BackOfficeErrorCodes.HMS_POSITIVE_CHECK);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("senderNumber", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateSystemType() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(systemType)) {
				getErrorMap().setError("systemType", BackOfficeErrorCodes.HMS_FIELD_BLANK);
				return false;
			}
			if (!validation.isValidDescription(systemType, 100)) {
				getErrorMap().setError("systemType", BackOfficeErrorCodes.HMS_INVALID_DESCRIPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("systemType", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateAddressRange() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(addressRange)) {
				getErrorMap().setError("addressRange", BackOfficeErrorCodes.HMS_FIELD_BLANK);
				return false;
			}
			if (!validation.isValidDescription(addressRange, 100)) {
				getErrorMap().setError("addressRange", BackOfficeErrorCodes.HMS_INVALID_DESCRIPTION);
				return false;
			}

			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("addressRange", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateMainServer() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(mainServer)) {
				getErrorMap().setError("mainServer", BackOfficeErrorCodes.HMS_FIELD_BLANK);
				return false;
			}
			if (!validation.isValidServerAddress(mainServer)) {
				getErrorMap().setError("mainServer", BackOfficeErrorCodes.HMS_INVALID_SERVER_ADDRESS);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("mainServer", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateMainServerPort() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(mainServerPort)) {
				getErrorMap().setError("mainServerPort", BackOfficeErrorCodes.HMS_FIELD_BLANK);
				return false;
			}
			if (!validation.isValidNumber(mainServerPort)) {
				getErrorMap().setError("mainServerPort", BackOfficeErrorCodes.HMS_INVALID_NUMBER);
				return false;
			}
			if (!validation.isValidPort(mainServerPort)) {
				getErrorMap().setError("mainServerPort", BackOfficeErrorCodes.HMS_INVALID_PORT);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("mainServerPort", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateFailOverServer() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(failOverServer)) {
				getErrorMap().setError("failOverServer", BackOfficeErrorCodes.HMS_FIELD_BLANK);
				return false;
			}
			if (!validation.isValidServerAddress(failOverServer)) {
				getErrorMap().setError("failOverServer", BackOfficeErrorCodes.HMS_INVALID_SERVER_ADDRESS);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("failOverServer", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateFailServerPort() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(failOverServerPort)) {
				getErrorMap().setError("failOverServerPort", BackOfficeErrorCodes.HMS_FIELD_BLANK);
				return false;
			}
			if (!validation.isValidNumber(failOverServerPort)) {
				getErrorMap().setError("failOverServerPort", BackOfficeErrorCodes.HMS_INVALID_NUMBER);
				return false;
			}
			if (!validation.isValidPort(failOverServerPort)) {
				getErrorMap().setError("failOverServerPort", BackOfficeErrorCodes.HMS_INVALID_PORT);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("failOverServerPort", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateDispatchType() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(dispatchType)) {
				getErrorMap().setError("dispatchType", BackOfficeErrorCodes.HMS_FIELD_BLANK);
				return false;
			}
			if (!validation.isCMLOVREC("COMMON", "SMSTYPE", dispatchType)) {
				getErrorMap().setError("dispatchType", BackOfficeErrorCodes.HMS_INVALID_CODE);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("dispatchType", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateRetryTimeout() {
		CommonValidator validation = new CommonValidator();
		try {
			if (dispatchType.equals("P")) {
				if (validation.isEmpty(retryTimeout)) {
					getErrorMap().setError("retryTimeout", BackOfficeErrorCodes.HMS_FIELD_BLANK);
					return false;
				}
				if (!validation.isValidNumber(retryTimeout)) {
					getErrorMap().setError("retryTimeout", BackOfficeErrorCodes.HMS_INVALID_NUMBER);
					return false;
				}
				if (validation.isZero(retryTimeout)) {
					getErrorMap().setError("retryTimeout", BackOfficeErrorCodes.HMS_ZERO_CHECK);
					return false;
				}
				if (!validation.isValidPositiveNumber(retryTimeout)) {
					getErrorMap().setError("retryTimeout", BackOfficeErrorCodes.HMS_POSITIVE_CHECK);
					return false;
				}
				if (!validation.isNumberLesserThanEqual(retryTimeout, "9999")) {
					getErrorMap().setError("retryTimeout", BackOfficeErrorCodes.HMS_MAX_LENGTH);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("retryTimeout", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateRemarks() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (remarks != null && remarks.length() > 0) {
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.HMS_INVALID_REMARKS);
					return false;
				}
			}
			if (getAction().equals(MODIFY)) {
				if (commonValidator.isEmpty(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.HMS_FIELD_BLANK);
					return false;
				}
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.HMS_INVALID_REMARKS);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}
}
