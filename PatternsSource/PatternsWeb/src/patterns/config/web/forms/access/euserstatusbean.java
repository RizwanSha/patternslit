package patterns.config.web.forms.access;

import java.util.ArrayList;
import java.util.HashMap;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.BindParameterType;
import patterns.config.framework.database.DBEntry;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.GenericOption;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.AccessValidator;
import patterns.config.validations.CommonValidator;
import patterns.config.web.forms.GenericFormBean;

public class euserstatusbean extends GenericFormBean {

	private static final long serialVersionUID = -6219657363150466160L;

	private String userID;
	private String currentUserStatus;
	private String status;
	private String remarks;
	private String tmpCurrentUserStatus;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getCurrentUserStatus() {
		return currentUserStatus;
	}

	public void setCurrentUserStatus(String currentUserStatus) {
		this.currentUserStatus = currentUserStatus;
	}

	public String getTmpCurrentUserStatus() {
		return tmpCurrentUserStatus;
	}

	public void setTmpCurrentUserStatus(String tmpCurrentUserStatus) {
		this.tmpCurrentUserStatus = tmpCurrentUserStatus;
	}

	@Override
	public void reset() {
		userID = RegularConstants.EMPTY_STRING;
		currentUserStatus = RegularConstants.EMPTY_STRING;
		status = RegularConstants.EMPTY_STRING;
		remarks = RegularConstants.EMPTY_STRING;
		DBContext dbContext = new DBContext();
		ArrayList<GenericOption> currentStatusList = null;
		ArrayList<GenericOption> changeStatusList = null;
		currentStatusList = getGenericOptions("COMMON", "USERSTATUS", dbContext, "--");
		webContext.getRequest().setAttribute("COMMON_USERSTATUS", currentStatusList);
		changeStatusList = getGenericOptions("COMMON", "USERSTATUSACTION", dbContext, "--");
		webContext.getRequest().setAttribute("COMMON_USERSTATUSACTION", changeStatusList);
		dbContext.close();
		setProcessBO("patterns.config.framework.bo.access.euserstatusBO");
	}

	public void validate() {
		if (validateUserID()) {
			validatePK();
			if (validateCurrentStatus()) {
				if (validateChangeStatus()) {
					validateRemarks();
				}
			}
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.set("USER_ID", userID);
				formDTO.set("STATUS", status);
				formDTO.set("REMARKS", remarks);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("userID", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
	}

	public boolean validatePK() {
		if (getAction().equals(ADD)) {
			getErrorMap().setError("userID", BackOfficeErrorCodes.INVALID_ACTION);
			return false;
		}
		CommonValidator validation = new CommonValidator();
		try {
			if (getAction().equals(MODIFY)) {
				HashMap<Integer, DBEntry> whereClauseFieldsMap = new HashMap<Integer, DBEntry>();
				whereClauseFieldsMap.put(1, new DBEntry("ENTITY_CODE", context.getEntityCode(), BindParameterType.VARCHAR));
				whereClauseFieldsMap.put(2, new DBEntry("USER_ID", userID, BindParameterType.VARCHAR));
				boolean isRecordAvailable = validation.isRecordAvailable("USERSSTATUS", whereClauseFieldsMap);
				if (isRecordAvailable) {
					return true;
				} else {
					getErrorMap().setError("userID", BackOfficeErrorCodes.RECORD_NOT_EXISTS);
					return false;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("userID", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateUserID() {
		
		try {
			DTObject formDTO = getFormDTO();
			formDTO.set(ContentManager.USER_ID, userID);
			formDTO.set(ContentManager.FETCH, ContentManager.FETCH_ALL);
			formDTO = validateUserId(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("userID", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("userID", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} 
		return false;
	}

	public DTObject validateUserId(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		AccessValidator validation = new AccessValidator();
		String userId = inputDTO.get("USER_ID");
		try {

			if (validation.isEmpty(userId)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_FIELD_BLANK);
				return resultDTO;
			}
			if (userId.equals(context.getUserID())) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.CANNOT_ALLOCATE_TO_SELF);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH, ContentManager.FETCH_ALL);
			resultDTO = validation.validateInternalUser(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				return resultDTO;
			}
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
			if (userId.equals(context.getUserID())) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.CANNOT_ALLOCATE_TO_SELF);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	

	public boolean validateCurrentStatus() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(currentUserStatus)) {
				getErrorMap().setError("currentUserStatus", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isCMLOVREC("COMMON", "USERSTATUS", currentUserStatus)) {
				getErrorMap().setError("currentUserStatus", BackOfficeErrorCodes.INVALID_OPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("currentUserStatus", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateChangeStatus() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(status)) {
				getErrorMap().setError("status", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isCMLOVREC("COMMON", "USERSTATUSACTION", status)) {
				getErrorMap().setError("status", BackOfficeErrorCodes.INVALID_OPTION);
				return false;
			}
			if (status.equals(currentUserStatus)) {
				getErrorMap().setError("status", BackOfficeErrorCodes.CURR_STATUS_NOT_ALLOWED);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("status", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateRemarks() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(remarks)) {
				getErrorMap().setError("remarks", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidRemarks(remarks)) {
				getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}
}