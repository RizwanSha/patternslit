package patterns.config.web.forms.eodsod;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.Map;

import patterns.config.delegate.BackOfficeProcessManager;
import patterns.config.eodsod.EodSodProcess;
import patterns.config.eodsod.EodSodUtil;
import patterns.config.eodsod.ProcessInfoBean;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.pki.ProcessTFAInfo;
import patterns.config.framework.process.TBAProcessResult;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.thread.ApplicationContext;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.framework.web.DHTMLXGridUtility;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.EodSodValidator;
import patterns.config.web.forms.GenericFormBean;

public class eeodbean extends GenericFormBean {

	public eeodbean() {
		setProcessBO("patterns.config.framework.bo.eodsod.eeodBO");
	}

	private static final long serialVersionUID = -6219657363150466160L;
	private String currentDate;
	private String previousDay;
	private String previousDate;
	private String nextDate;
	private String remarks;
	private boolean confirmClose;
	private String eodInProcess;
	private String eodCompl;
	private String sodInProcess;
	private String sodCompl;
	private String processXml;
	private String runSerial;
	private String eodStatus;

	DTObject result = new DTObject();

	public String getCurrentDate() {
		return currentDate;
	}

	public void setCurrentDate(String currentDate) {
		this.currentDate = currentDate;
	}

	public String getPreviousDate() {
		return previousDate;
	}

	public void setPreviousDate(String previousDate) {
		this.previousDate = previousDate;
	}

	public String getNextDate() {
		return nextDate;
	}

	public void setNextDate(String nextDate) {
		this.nextDate = nextDate;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public boolean isConfirmClose() {
		return confirmClose;
	}

	public void setConfirmClose(boolean confirmClose) {
		this.confirmClose = confirmClose;
	}

	public String getPreviousDay() {
		return previousDay;
	}

	public void setPreviousDay(String previousDay) {
		this.previousDay = previousDay;
	}

	public String getEodInProcess() {
		return eodInProcess;
	}

	public void setEodInProcess(String eodInProcess) {
		this.eodInProcess = eodInProcess;
	}

	public String getEodCompl() {
		return eodCompl;
	}

	public void setEodCompl(String eodCompl) {
		this.eodCompl = eodCompl;
	}

	public String getSodInProcess() {
		return sodInProcess;
	}

	public void setSodInProcess(String sodInProcess) {
		this.sodInProcess = sodInProcess;
	}

	public String getSodCompl() {
		return sodCompl;
	}

	public void setSodCompl(String sodCompl) {
		this.sodCompl = sodCompl;
	}

	public ApplicationContext getContext() {
		return context;
	}

	public String getProcessXml() {
		return processXml;
	}

	public void setProcessXml(String processXml) {
		this.processXml = processXml;
	}

	public String getRunSerial() {
		return runSerial;
	}

	public void setRunSerial(String runSerial) {
		this.runSerial = runSerial;
	}

	public String getEodStatus() {
		return eodStatus;
	}

	public void setEodStatus(String eodStatus) {
		this.eodStatus = eodStatus;
	}

	public void reset() {
		confirmClose = false;
		currentDate = RegularConstants.EMPTY_STRING;
		previousDate = RegularConstants.EMPTY_STRING;
		nextDate = RegularConstants.EMPTY_STRING;
		remarks = RegularConstants.EMPTY_STRING;
		eodInProcess = RegularConstants.EMPTY_STRING;
		eodCompl = RegularConstants.EMPTY_STRING;
		sodInProcess = RegularConstants.EMPTY_STRING;
		sodCompl = RegularConstants.EMPTY_STRING;

		readMainCont(true);
	}

	public void validate() {
		if (!readMainCont(false)) {
			return;
		}
		if (sodInProcess.equals(RegularConstants.COLUMN_ENABLE)) {
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_SOD_IN_PROGRESS);
			return;
		}
		if (sodCompl.equals(RegularConstants.COLUMN_DISABLE)) {
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_SOD_NOT_DONE);
			return;
		}
		if (eodInProcess.equals(RegularConstants.COLUMN_ENABLE)) {
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_EOD_IN_PROGRESS);
			return;
		}
		if (eodCompl.equals(RegularConstants.COLUMN_ENABLE)) {
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_EOD_ALREADY_DONE);
			return;
		}

		EodSodValidator validator = new EodSodValidator();
		try {
			if (validator.isUsersLoggedIn()) {
				result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_USERS_LOGGED_IN);
				return;
			}
			if (validator.isAuthorizationPending(true)) {
				result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_FINREC_IN_PENDINGQ);
				return;
			}
			if (validator.isAuthorizationPending(false)) {
				result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_EODOPR_IN_PENDINGQ);
				return;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			validator.close();
		}

		validateRemarks();

	}

	public DTObject processAction(DTObject inputDTO) {
		remarks = inputDTO.get("REMARKS");
		validate();
		if (!result.containsKey(ContentManager.ERROR)) {
			Date cbd = new java.sql.Date(BackOfficeFormatUtils.getDate(currentDate, context.getDateFormat()).getTime());
			DTObject formDTO = getFormDTO();

			formDTO.set("ENTITY_CODE", context.getEntityCode());

			ProcessTFAInfo processTFAInfo = new ProcessTFAInfo();
			processTFAInfo.setUserId(context.getUserID());
			processTFAInfo.setDeploymentContext(context.getDeploymentContext());
			processTFAInfo.setDigitalCertificateInventoryNumber(context.getDigitalCertificateInventoryNumber());
			processTFAInfo.setTfaRequired(getTfaRequired());
			processTFAInfo.setTfaSuccess(getTfaSuccess());
			processTFAInfo.setTfaValue(getTfaValue());
			processTFAInfo.setTfaEncodedValue(getTfaEncodedValue());

			BackOfficeProcessManager processManager = new BackOfficeProcessManager();
			formDTO.set("PARTITION_NO", context.getPartitionNo());
			formDTO.set("USER_ID", context.getUserID());
			formDTO.set("IP_ADDRESS", context.getClientIP());
			formDTO.setObject("CBD", cbd);
			formDTO.setObject("PKI", processTFAInfo);
			formDTO.set("REMARKS", remarks);

			formDTO.set(RegularConstants.PROCESSBO_CLASS, getProcessBO());
			TBAProcessResult processResult = processManager.delegate(formDTO);
			if (processResult.getProcessStatus().equals(TBAProcessStatus.SUCCESS)) {
				if (processResult.getResponseDTO() != null) {
					if (processResult.getResponseDTO().get(RegularConstants.ERROR) != null) {
						result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
						return result;
					} else if (processResult.getResponseDTO().get(RegularConstants.ADDITIONAL_INFO) != null) {
						result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
						return result;
					}
				}
				runSerial = processResult.getGeneratedID();
				eodStatus = (String) processResult.getAdditionalInfo();
				result.set("SERIAL", runSerial);
				result.set("STATUS", eodStatus);
			} else {
				result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
				return result;
			}

			if (eodStatus.equals("P")) {
				EodSodProcess process = new EodSodProcess(context.getEntityCode());
				try {
					Map<String, ProcessInfoBean> processMap = process.readProcessInfo(EodSodProcess.EOD_PROCESS);
					DTObject input = new DTObject();
					input.setObject("PROCESS_MAP", processMap);
					input.setObject("CBD", cbd);
					input.set("RUN_SERIAL", String.valueOf(runSerial));
					input.set("PROCESS_TYPE", "E");
					input.setObject("CONTEXT", context);
					EodSodUtil util = new EodSodUtil(input);
					Thread t = new Thread(util);
					t.start();
					loadGrid();
					if (!processXml.equals(RegularConstants.EMPTY_STRING)) {
						result.set(ContentManager.RESULT_XML, processXml);
					}
				} catch (Exception e) {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
					return result;
				} finally {
					process.close();
				}
			}
			result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
		}
		return result;
	}

	private boolean readMainCont(boolean onLoad) {
		EodSodValidator validator = new EodSodValidator();
		try {
			DTObject formDTO = validator.readMainContDetails();
			if (formDTO.get(ContentManager.ERROR) != null) {
				if (onLoad) {
					getErrorMap().setError("currentDate", formDTO.get(ContentManager.ERROR));
				} else {
					result.set(ContentManager.ERROR, formDTO.get(ContentManager.ERROR));
				}
				return false;
			}
			if (formDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				if (onLoad) {
					getErrorMap().setError("currentDate", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
				} else {
					result.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
				}
				return false;
			}

			currentDate = formDTO.get("CURR_BUSINESS_DATE");
			previousDate = formDTO.get("PREV_BUSINESS_DATE");
			eodInProcess = formDTO.get("EOD_IN_PROGRESS");
			eodCompl = formDTO.get("EOD_COMPL");
			sodInProcess = formDTO.get("SOD_IN_PROGRESS");
			sodCompl = formDTO.get("SOD_COMPL");
			if (onLoad) {
				remarks = formDTO.get("REMARKS");
				if ((eodCompl.equals(RegularConstants.COLUMN_DISABLE) || eodInProcess.equals(RegularConstants.COLUMN_ENABLE))
						&& sodCompl.equals(RegularConstants.COLUMN_ENABLE)) {
					processXml = loadCurrentProcessingDetails();
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			validator.close();
		}
		return false;
	}

	public void loadGrid() {
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		processXml = RegularConstants.EMPTY_STRING;
		try {
			String sql = "SELECT D.PROCESS_ID,P.PROCESS_NAME FROM EODSODPROCDTL D,EODSODPROCESSES P WHERE D.PROCESS_ID=P.PROCESS_ID AND D.ENTITY_CODE=? AND D.PROCESS_FLAG='E' ORDER BY D.SL";
			util.reset();
			util.setSql(sql);
			util.setString(1, getContext().getEntityCode());
			ResultSet rset = util.executeQuery();
			DHTMLXGridUtility gridUtility = new DHTMLXGridUtility();
			gridUtility.init();
			int count = 1;
			while (rset.next()) {
				gridUtility.startRow(rset.getString(1));
				gridUtility.setCell(String.valueOf(count));
				gridUtility.setCell(rset.getString(1));
				gridUtility.setCell(rset.getString(2));
				gridUtility.setCell("");
				gridUtility.setCell("");
				gridUtility.setCell("");
				gridUtility.endRow();
				count++;
			}
			gridUtility.finish();
			if (count > 1) {
				processXml = gridUtility.getXML();
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			dbContext.close();
		}
	}

	public DTObject loadProcessStatus(DTObject inputDTO) {
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		DTObject result = new DTObject();
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		runSerial = inputDTO.get("SERIAL");
		currentDate = inputDTO.get("CBD");
		try {
			String sql = "SELECT D.PROCESS_ID,P.PROCESS_NAME,D.PROCESS_STATUS,TO_CHAR(D.PROCESS_START_TIME,'%d-%m-%Y %H:%i:%s' ),TO_CHAR(D.PROCESS_END_TIME,'%d-%m-%Y %H:%i:%s') FROM EODPROCESSRUNDTL D,EODSODPROCESSES P WHERE D.ENTITY_CODE=? AND D.PROCESS_DATE=? AND D.RUN_SL=? AND D.PROCESS_ID=P.PROCESS_ID ORDER BY D.SL";
			util.reset();
			util.setSql(sql);
			util.setString(1, getContext().getEntityCode());
			util.setDate(2, new java.sql.Date(BackOfficeFormatUtils.getDate(currentDate, context.getDateFormat()).getTime()));
			util.setInt(3, Integer.parseInt(runSerial));
			ResultSet rset = util.executeQuery();
			DHTMLXGridUtility gridUtility = new DHTMLXGridUtility();
			gridUtility.init();
			int count = 1;
			while (rset.next()) {
				gridUtility.startRow(rset.getString(1));
				gridUtility.setCell(String.valueOf(count));
				gridUtility.setCell(rset.getString(1));
				gridUtility.setCell(rset.getString(2));
				if (rset.getString(3).equals("S")) {
					gridUtility.setCell("SUCCESS");
				} else if (rset.getString(3).equals("P")) {
					gridUtility.setCell("PENDING");
				} else {
					gridUtility.setCell("FAILED");
				}
				gridUtility.setCell(rset.getString(4));
				gridUtility.setCell(rset.getString(5));
				gridUtility.endRow();
				count++;
			}
			gridUtility.finish();
			String resultXML = gridUtility.getXML();
			result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
			result.set(ContentManager.RESULT_XML, resultXML);
			result.set("STATUS", processStatus());
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			dbContext.close();
		}
		return result;
	}

	public String processStatus() {
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		String status = null;
		try {
			String sql = "SELECT RUN_STATUS FROM EODPROCESSRUN WHERE ENTITY_CODE=? AND PROCESS_DATE=? AND RUN_SL=?";
			util.reset();
			util.setSql(sql);
			util.setString(1, getContext().getEntityCode());
			util.setDate(2, new java.sql.Date(BackOfficeFormatUtils.getDate(currentDate, context.getDateFormat()).getTime()));
			util.setInt(3, Integer.parseInt(runSerial));
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				status = rset.getString(1);
			}
			return status;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			dbContext.close();
		}
		return status;
	}

	public boolean validateRemarks() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (commonValidator.isEmpty(remarks)) {
				result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_FIELD_BLANK);
				return false;
			}
			if (!commonValidator.isValidRemarks(remarks)) {
				result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_REMARKS);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			result.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}

	public String loadCurrentProcessingDetails() {
		DBContext dbContext = new DBContext();
		DBUtil dbUtil = dbContext.createUtilInstance();
		try {
			runSerial = "0";
			Date cbd = new java.sql.Date(BackOfficeFormatUtils.getDate(currentDate, context.getDateFormat()).getTime());
			String sqlQuery = "SELECT IFNULL(MAX(RUN_SL),0) FROM EODPROCESSRUN WHERE ENTITY_CODE=? AND PROCESS_DATE=?";
			dbUtil.reset();
			dbUtil.setSql(sqlQuery);
			dbUtil.setString(1, context.getEntityCode());
			dbUtil.setDate(2, cbd);
			ResultSet rset = dbUtil.executeQuery();
			if (rset.next()) {
				runSerial = rset.getString(1);
				if (runSerial.equals("0")) {
					return "";
				}
			}

			String sql = "SELECT * FROM (SELECT D.PROCESS_ID,P.PROCESS_NAME,D.PROCESS_STATUS,TO_CHAR(D.PROCESS_START_TIME,'%d-%m-%Y %H:%i:%s' ),TO_CHAR(D.PROCESS_END_TIME,'%d-%m-%Y %H:%i:%s'),D.SL FROM EODPROCESSRUNDTL D, EODSODPROCESSES P WHERE D.ENTITY_CODE=? AND D.PROCESS_DATE=? AND D.PROCESS_STATUS='S' AND D.PROCESS_ID=P.PROCESS_ID UNION SELECT D.PROCESS_ID,P.PROCESS_NAME,D.PROCESS_STATUS,TO_CHAR(D.PROCESS_START_TIME,'%d-%m-%Y %H:%i:%s' ), TO_CHAR(D.PROCESS_END_TIME,'%d-%m-%Y %H:%i:%s'),D.SL FROM EODPROCESSRUNDTL D, EODSODPROCESSES P WHERE D.ENTITY_CODE=? AND D.PROCESS_DATE=? AND D.RUN_SL=? AND D.PROCESS_ID=P.PROCESS_ID) M ORDER BY SL";
			dbUtil.reset();
			dbUtil.setSql(sql);
			dbUtil.setString(1, getContext().getEntityCode());
			dbUtil.setDate(2, cbd);
			dbUtil.setString(3, getContext().getEntityCode());
			dbUtil.setDate(4, cbd);
			dbUtil.setString(5, runSerial);
			rset = dbUtil.executeQuery();
			DHTMLXGridUtility gridUtility = new DHTMLXGridUtility();
			gridUtility.init();
			int count = 1;
			while (rset.next()) {
				gridUtility.startRow(rset.getString(1));
				gridUtility.setCell(String.valueOf(count));
				gridUtility.setCell(rset.getString(1));
				gridUtility.setCell(rset.getString(2));
				if (rset.getString(3).equals("S")) {
					gridUtility.setCell("SUCCESS");
				} else if (rset.getString(3).equals("P")) {
					gridUtility.setCell("PENDING");
				} else {
					gridUtility.setCell("FAILED");
				}
				gridUtility.setCell(rset.getString(4));
				gridUtility.setCell(rset.getString(5));
				gridUtility.endRow();
				count++;
			}
			gridUtility.finish();
			if (count > 1) {
				processXml = gridUtility.getXML();
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			dbContext.close();
		}
		return processXml;
	}
}