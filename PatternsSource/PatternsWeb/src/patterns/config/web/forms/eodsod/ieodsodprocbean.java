package patterns.config.web.forms.eodsod;
/*
Author : Pavan kumar.R
Created Date : 28-MAR-2015
Spec Reference: HMS-PSD-EODSOD-003
Modification History
----------------------------------------------------------------------
Sl.No		Modified Date	Author			Modified Changes	Version
----------------------------------------------------------------------

*/

import java.util.ArrayList;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.process.TBAActionType;
import patterns.config.framework.service.DTDObject;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.GenericOption;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.AccessValidator;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.EodSodValidator;
import patterns.config.web.forms.GenericFormBean;

public class ieodsodprocbean extends GenericFormBean {
	private static final long serialVersionUID = -6219657363150466160L;
	private String processType;
	private String efftCDate;
	private String processId;
	private String processState;
	private String xmlieodsodprocGrid;

	public String getProcessId() {
		return processId;
	}

	public void setProcessId(String processId) {
		this.processId = processId;
	}

	public String getProcessState() {
		return processState;
	}

	public void setProcessState(String processState) {
		this.processState = processState;
	}

	public String getProcessType() {
		return processType;
	}

	public void setProcessType(String processType) {
		this.processType = processType;
	}

	public String getEfftCDate() {
		return efftCDate;
	}

	public void setEfftCDate(String efftCDate) {
		this.efftCDate = efftCDate;
	}

	public String getXmlieodsodprocGrid() {
		return xmlieodsodprocGrid;
	}

	public void setXmlieodsodprocGrid(String xmlieodsodprocGrid) {
		this.xmlieodsodprocGrid = xmlieodsodprocGrid;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void reset() {
		processType = RegularConstants.EMPTY_STRING;
		efftCDate = RegularConstants.EMPTY_STRING;
		processId = RegularConstants.EMPTY_STRING;
		processState = RegularConstants.EMPTY_STRING;
		xmlieodsodprocGrid = RegularConstants.EMPTY_STRING;
		DBContext dbContext = new DBContext();
		ArrayList<GenericOption> directionList = null;
		directionList = getGenericOptions("IEODSODPROC", "PROCESS_FLAG", dbContext, null);
		webContext.getRequest().setAttribute("IEODSODPROC_PROCESS_FLAG", directionList);
		ArrayList<GenericOption> processStateList = null;
		processStateList = getGenericOptions("IEODSODPROC", "PROCESS_FLAG_S", dbContext, null);
		webContext.getRequest().setAttribute("IEODSODPROC_PROCESS_FLAG_S", processStateList);
		dbContext.close();
		setProcessBO("patterns.config.framework.bo.eodsod.ieodsodprocBO");
	}

	public void validate() {
		if (validateProcessType() && validateEffectiveDate()) {
			validateGrid();

		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.set("PROCESS_FLAG", processType);
				formDTO.set("EFFT_DATE", efftCDate);
				DTDObject dtdObject = new DTDObject();
				dtdObject.addColumn(0, "SL");
				dtdObject.addColumn(1, "PROCESS_ID");
				dtdObject.addColumn(2, "PROCESS_NAME");
				dtdObject.addColumn(3, "PROCESSSTATEDESC");
				dtdObject.addColumn(4, "PROCESS_STATE");
				dtdObject.setXML(xmlieodsodprocGrid);
				formDTO.setDTDObject("EODSODPROCHISTDTL", dtdObject);
			}

		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("processType", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		}
	}

	private boolean validateProcessType() {
		CommonValidator validation = new CommonValidator();
		try {
			if (!validation.isCMLOVREC("IEODSODPROC", "PROCESS_FLAG", processType)) {
				getErrorMap().setError("processType", BackOfficeErrorCodes.HMS_INVALID_OPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("processType", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;

	}

	private boolean validateEffectiveDate() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set(ContentManager.USER_ACTION, getAction());
			formDTO.set("PROCESS_FLAG", processType);
			formDTO.set("EFFT_DATE", efftCDate);
			formDTO = validateEffectiveDate(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("efftCDate", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("efftCDate", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateEffectiveDate(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		CommonValidator commonvalidation = new CommonValidator();
		String processType = inputDTO.get("PROCESS_FLAG");
		String efftCDate = inputDTO.get("EFFT_DATE");
		String action = inputDTO.get(ContentManager.USER_ACTION);
		try {
			if (commonvalidation.isEmpty(efftCDate)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_FIELD_BLANK);
				return resultDTO;
			}
			TBAActionType AT;
			if (action.equals(ADD))
				AT = TBAActionType.ADD;
			else
				AT = TBAActionType.MODIFY;
			if (!commonvalidation.isEffectiveDateValid(efftCDate, AT)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_EFFECTIVE_DATE);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "EFFT_DATE");
			inputDTO.set(ContentManager.TABLE, "EODSODPROCHIST");
			String columns[] = new String[] { context.getEntityCode(), processType, efftCDate };
			inputDTO.setObject(ContentManager.PROGRAM_KEY, columns);
			resultDTO = commonvalidation.validatePK(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (action.equals(ADD)) {
				if (ContentManager.DATA_AVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_RECORD_EXISTS);
					return resultDTO;
				}
			} else if (action.equals(MODIFY)) {
				if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_RECORD_NOT_EXISTS);
					return resultDTO;
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			commonvalidation.close();
		}
		return resultDTO;
	}

	private boolean validateGrid() {
		AccessValidator validation = new AccessValidator();
		try {
			DTDObject dtdObject = new DTDObject();
			dtdObject.addColumn(0, "SL");
			dtdObject.addColumn(1, "PROCESS_ID");
			dtdObject.addColumn(2, "PROCESS_STATE");
			dtdObject.addColumn(3, "PROCESSSTATEDESC");
			dtdObject.addColumn(4, "PROCESS_STATE");
			dtdObject.setXML(xmlieodsodprocGrid);
			if (dtdObject.getRowCount() <= 0) {
				getErrorMap().setError("processId", BackOfficeErrorCodes.HMS_ATLEAST_ONE_ROW);
				return false;
			}
			if (!validation.checkDuplicateRows(dtdObject, 1)) {
				getErrorMap().setError("processId", BackOfficeErrorCodes.HMS_DUPLICATE_DATA);
				return false;
			}
			for (int i = 0; i < dtdObject.getRowCount(); i++) {
				if (!validateprocessId(dtdObject.getValue(i, 1))) {
					getErrorMap().setError("processId", BackOfficeErrorCodes.HMS_INVALID_ACTION);
					return false;
				}
				if (!validateProcessState(dtdObject.getValue(i, 4))) {
					getErrorMap().setError("processState", BackOfficeErrorCodes.HMS_INVALID_ACTION);
					return false;
				}

			}

			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("printTypeId", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	private boolean validateprocessId(String processId) {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("PROCESS_FLAG", processType);
			formDTO.set("PROCESS_ID", processId);
			formDTO.set("ACTION", USAGE);
			formDTO = validateprocessId(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("processId", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("processId", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateprocessId(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, RegularConstants.EMPTY_STRING);
		EodSodValidator validation = new EodSodValidator();
		String processType = inputDTO.get("PROCESS_FLAG");
		String processId = inputDTO.get("PROCESS_ID");
		try {
			if ((validation.isEmpty(processId)) && (validation.isEmpty(processType))){
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_FIELD_BLANK);
				return resultDTO;

			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "PROCESS_NAME");
			resultDTO = validation.validateProcessName(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_INVALID_CODE);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	private boolean validateProcessState(String processState) {
		CommonValidator validation = new CommonValidator();
		try {
			if (!validation.isCMLOVREC("IEODSODPROC", "PROCESS_FLAG_S", processState)) {
				getErrorMap().setError("processState", BackOfficeErrorCodes.HMS_INVALID_OPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("processType", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;

	}

}
