/*
 *This Program used to create set of processed to run at EOD or SOD.

 Author : KARTHIKEYAN.P
 Created Date : 01-Apr-2015
 Spec Reference :
 Modification History
 ----------------------------------------------------------------------
 Sl.No		Modified Date	Author			Modified Changes	Version
 ----------------------------------------------------------------------

 */
package patterns.config.web.forms.eodsod;

import java.util.ArrayList;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.GenericOption;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.EodSodValidator;
import patterns.config.validations.MasterValidator;
import patterns.config.web.forms.GenericFormBean;

public class meodsodprocessbean extends GenericFormBean {

	private static final long serialVersionUID = 1L;
	private String processId;
	private String description;
	private String routine;
	private String className;
	private boolean runAtEOD;
	private boolean runAtSOD;
	private String runFreq;
	private boolean enabled;
	private String remarks;

	public String getProcessId() {
		return processId;
	}

	public void setProcessId(String processId) {
		this.processId = processId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getRoutine() {
		return routine;
	}

	public void setRoutine(String routine) {
		this.routine = routine;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public boolean isRunAtEOD() {
		return runAtEOD;
	}

	public void setRunAtEOD(boolean runAtEOD) {
		this.runAtEOD = runAtEOD;
	}

	public boolean isRunAtSOD() {
		return runAtSOD;
	}

	public void setRunAtSOD(boolean runAtSOD) {
		this.runAtSOD = runAtSOD;
	}

	public String getRunFreq() {
		return runFreq;
	}

	public void setRunFreq(String runFreq) {
		this.runFreq = runFreq;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void reset() {
		processId = RegularConstants.EMPTY_STRING;
		description = RegularConstants.EMPTY_STRING;
		routine = RegularConstants.EMPTY_STRING;
		className = RegularConstants.EMPTY_STRING;
		runAtEOD = false;
		runAtSOD = false;
		enabled = false;
		runFreq = RegularConstants.EMPTY_STRING;
		remarks = RegularConstants.EMPTY_STRING;

		DBContext dbContext = new DBContext();
		ArrayList<GenericOption> runFreq = getGenericOptions("COMMON", "PROC_FREQ", dbContext, null);
		webContext.getRequest().setAttribute("COMMON_PROC_FREQ", runFreq);
		dbContext.close();

		setProcessBO("patterns.config.framework.bo.eodsod.meodsodprocessBO");
	}

	public void validate() {
		if (validateProcessId()) {
			validateDescription();
			validateRoutine();
			validateClassName();
			validateRunAtSOD();
			validateRunFreq();
			validateRemarks();
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("PROCESS_ID", processId);
				formDTO.set("PROCESS_NAME", description);
				formDTO.set("PROCESS_ROUTINE", routine);
				formDTO.set("PROCESS_CLASS", className);
				formDTO.set("PROCESS_ALLOW_EOD", decodeBooleanToString(runAtEOD));
				formDTO.set("PROCESS_ALLOW_SOD", decodeBooleanToString(runAtSOD));
				formDTO.set("PROCESS_RUN_FREQ", runFreq);
				if (getAction().equals(ADD)) {
					formDTO.set("ENABLED", decodeBooleanToString(true));
				} else {
					formDTO.set("ENABLED", decodeBooleanToString(enabled));
				}
				formDTO.set("REMARKS", remarks);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("processId", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		}
	}

	public boolean validateProcessId() {
		MasterValidator validation = new MasterValidator();
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("PROCESS_ID", processId);
			formDTO.set("ACTION", getAction());
			if (validation.isEmpty(processId)) {
				getErrorMap().setError("processId", BackOfficeErrorCodes.HMS_FIELD_BLANK);
				return false;
			}
			formDTO = validateProcessId(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("processId", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("processId", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		}
		return false;

	}

	public DTObject validateProcessId(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		EodSodValidator validation = new EodSodValidator();
		String action = inputDTO.get(ContentManager.USER_ACTION);
		try {
			inputDTO.set(ContentManager.FETCH_COLUMNS, "PROCESS_NAME");
			resultDTO = validation.validateProcessId(inputDTO);
			if (action.equals(ADD)) {
				if (ContentManager.DATA_AVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_RECORD_EXISTS);
				}
			} else {
				if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_RECORD_NOT_EXISTS);

				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateDescription() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(description)) {
				getErrorMap().setError("description", BackOfficeErrorCodes.HMS_FIELD_BLANK);
				return false;
			}
			if (!validation.isValidDescription(description)) {
				getErrorMap().setError("description", BackOfficeErrorCodes.HMS_INVALID_DESCRIPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("description", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateRoutine() {
		CommonValidator validation = new CommonValidator();
		try {
			if (!validation.isEmpty(routine)) {
				if (!validation.isValidDescription(routine)) {
					getErrorMap().setError("routine", BackOfficeErrorCodes.HMS_INVALID_DESCRIPTION);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("routine", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateClassName() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(routine) && validation.isEmpty(className)) {
				getErrorMap().setError("className", BackOfficeErrorCodes.HMS_EODSOD_PROC_MAND);
				return false;
			} else if (!validation.isEmpty(routine) && !validation.isEmpty(className)) {
				getErrorMap().setError("className", BackOfficeErrorCodes.HMS_EODSOD_PROC_MAND);
				return false;
			} else {
				if (!validation.isEmpty(className)) {
					if (!validation.isValidDescription(className)) {
						getErrorMap().setError("className", BackOfficeErrorCodes.HMS_INVALID_DESCRIPTION);
						return false;
					}
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("className", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateRunAtSOD() {
		CommonValidator validation = new CommonValidator();
		try {
			if (!(runAtEOD)) {
				if (!(runAtSOD)) {
					getErrorMap().setError("runAtSOD", BackOfficeErrorCodes.HMS_RUN_EODSOD_MAND);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("runAtSOD", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateRunFreq() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(runFreq)) {
				getErrorMap().setError("runFreq", BackOfficeErrorCodes.HMS_MANDATORY);
				return false;
			}
			if (!validation.isCMLOVREC("COMMON", "PROC_FREQ", runFreq)) {
				getErrorMap().setError("runFreq", BackOfficeErrorCodes.HMS_INVALID_OPTION);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("runFreq", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateRemarks() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (remarks != null && remarks.length() > 0) {
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.HMS_INVALID_REMARKS);
					return false;
				}
			}
			if (getAction().equals(MODIFY)) {
				if (commonValidator.isEmpty(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.HMS_FIELD_BLANK);
					return false;
				}
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.HMS_INVALID_REMARKS);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.HMS_UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}
}