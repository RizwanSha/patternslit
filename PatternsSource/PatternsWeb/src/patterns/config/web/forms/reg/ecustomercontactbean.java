/*
 *
 Author : Raja E
 Created Date : 09-02-2017
 Spec Reference : ECUSTOMERCONTACT
 Modification History
 ----------------------------------------------------------------------
 Sl.No		Modified Date	Author			Modified Changes	Version
 ----------------------------------------------------------------------

 */

package patterns.config.web.forms.reg;

import java.util.ArrayList;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.process.TBAActionType;
import patterns.config.framework.service.DTDObject;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.GenericOption;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.framework.web.configuration.menu.MenuUtils;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.MasterValidator;
import patterns.config.validations.RegistrationValidator;
import patterns.config.web.forms.GenericFormBean;

public class ecustomercontactbean extends GenericFormBean {

	private static final long serialVersionUID = 1L;

	private String customerID;
	private String addressSerial;
	private String effectiveDate;
	private String title;
	private String contactPerson;
	private String contactDetails1;
	private String contactDetails2;
	private String emailID;
	private boolean enabled;
	private boolean alternateEmail;
	private String remarks;
	private String xmlcontactDetailGrid;
	private String contactpk;
	private String redirectPageId;
	private String redirectPagePath;

	public String getCustomerID() {
		return customerID;
	}

	public void setCustomerID(String customerID) {
		this.customerID = customerID;
	}

	public String getAddressSerial() {
		return addressSerial;
	}

	public void setAddressSerial(String addressSerial) {
		this.addressSerial = addressSerial;
	}

	public String getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContactPerson() {
		return contactPerson;
	}

	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}

	public String getContactDetails1() {
		return contactDetails1;
	}

	public void setContactDetails1(String contactDetails1) {
		this.contactDetails1 = contactDetails1;
	}

	public String getContactDetails2() {
		return contactDetails2;
	}

	public void setContactDetails2(String contactDetails2) {
		this.contactDetails2 = contactDetails2;
	}

	public String getEmailID() {
		return emailID;
	}

	public void setEmailID(String emailID) {
		this.emailID = emailID;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public boolean isAlternateEmail() {
		return alternateEmail;
	}

	public void setAlternateEmail(boolean alternateEmail) {
		this.alternateEmail = alternateEmail;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getXmlcontactDetailGrid() {
		return xmlcontactDetailGrid;
	}

	public void setXmlcontactDetailGrid(String xmlcontactDetailGrid) {
		this.xmlcontactDetailGrid = xmlcontactDetailGrid;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getContactpk() {
		return contactpk;
	}

	public void setContactpk(String contactpk) {
		this.contactpk = contactpk;
	}

	public String getRedirectPageId() {
		return redirectPageId;
	}

	public void setRedirectPageId(String redirectPageId) {
		this.redirectPageId = redirectPageId;
	}

	public String getRedirectPagePath() {
		return redirectPagePath;
	}

	public void setRedirectPagePath(String redirectPagePath) {
		this.redirectPagePath = redirectPagePath;
	}

	@Override
	public void reset() {
		// TODO Auto-generated method stub
		DBContext dbContext = new DBContext();
		ArrayList<GenericOption> title1 = null;
		title1 = getTitle(dbContext, "--", ContentManager.PROFESSIONAL_TITLE);
		webContext.getRequest().setAttribute("COMMON_TITLE1", title1);
		dbContext.close();
		customerID = RegularConstants.EMPTY_STRING;
		addressSerial = RegularConstants.EMPTY_STRING;
		effectiveDate = RegularConstants.EMPTY_STRING;
		title = RegularConstants.EMPTY_STRING;
		contactPerson = RegularConstants.EMPTY_STRING;
		contactDetails1 = RegularConstants.EMPTY_STRING;
		contactDetails2 = RegularConstants.EMPTY_STRING;
		emailID = RegularConstants.EMPTY_STRING;
		enabled = false;
		alternateEmail = false;
		remarks = RegularConstants.EMPTY_STRING;
		contactpk = RegularConstants.EMPTY_STRING;
		xmlcontactDetailGrid = RegularConstants.EMPTY_STRING;
		setProcessBO("patterns.config.framework.bo.reg.ecustomercontactBO");

		if (webContext.getRequest().getParameter("contactpk") != null) {
			contactpk = webContext.getRequest().getParameter("contactpk");
		}

		if (webContext.getRequest().getParameter("_RL") != null) {
			redirectPageId = webContext.getRequest().getParameter("_RL");
			redirectPagePath = MenuUtils.resolveForward(redirectPageId, webContext.getRequest());
		}
	}

	@Override
	public void validate() {
		// TODO Auto-generated method stub
		if (validateCustomerID() && validateAddressSerial() && validateEffectiveDate()) {
			validateContactDetailsGrid();
			validateRemarks();
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.set("CUSTOMER_ID", customerID);
				formDTO.set("ADDR_SL", addressSerial);
				formDTO.set("EFF_DATE", effectiveDate);
				if (getAction().equals(ADD)) {
					formDTO.set("ENABLED", decodeBooleanToString(true));
				} else {
					formDTO.set("ENABLED", decodeBooleanToString(enabled));
				}
				formDTO.set("REMARKS", remarks);

				DTDObject dtdObject = new DTDObject();
				dtdObject.addColumn(0, "SL");
				dtdObject.addColumn(1, "ACTION");
				dtdObject.addColumn(2, "CONTACT_TITLE");
				dtdObject.addColumn(3, "CONTACT_PERSON");
				dtdObject.addColumn(4, "CONTACT_NUM_1");
				dtdObject.addColumn(5, "CONTACT_NUM_2");
				dtdObject.addColumn(6, "EMAIL_ID");
				dtdObject.addColumn(7, "ALTERNATE_EMAIL");
				dtdObject.setXML(xmlcontactDetailGrid);
				formDTO.setDTDObject("CUSTOMERCONTACTHISTDTL", dtdObject);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("customerID", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
	}

	public boolean validateCustomerID() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("CUSTOMER_ID", customerID);
			formDTO.set(ContentManager.ACTION, USAGE);
			formDTO = validateCustomerID(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("customerID", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("customerID", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateCustomerID(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		RegistrationValidator validation = new RegistrationValidator();
		String customerId = inputDTO.get("CUSTOMER_ID");
		try {
			if (validation.isEmpty(customerId)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			if (!validation.isValidNumber(customerId)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.NUMERIC_CHECK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "CUSTOMER_NAME");
			resultDTO = validation.valildateCustomerID(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateAddressSerial() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("CUSTOMER_ID", customerID);
			formDTO.set("ADDR_SL", addressSerial);
			formDTO.set(ContentManager.ACTION, USAGE);
			formDTO = validateAddressSerial(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("addressSerial", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("addressSerial", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateAddressSerial(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		RegistrationValidator validation = new RegistrationValidator();
		String addrSl = inputDTO.get("ADDR_SL");
		try {
			if (validation.isEmpty(addrSl)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			if (!validation.isValidNumber(addrSl)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_NUMBER);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH, ContentManager.FETCH_ALL);
			resultDTO = validation.validateCustomerAddressSerial(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_ADDRESS_SERIAL);
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateEffectiveDate() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("CUSTOMER_ID", customerID);
			formDTO.set("ADDR_SL", addressSerial);
			formDTO.set("EFF_DATE", effectiveDate);
			formDTO.set(ContentManager.USER_ACTION, getAction());
			formDTO = validateEffectiveDate(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("effectiveDate", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("effectiveDate", BackOfficeErrorCodes.UNSPECIFIED_ERROR);

		}
		return false;
	}

	public DTObject validateEffectiveDate(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		CommonValidator commonvalidation = new CommonValidator();
		String custId = inputDTO.get("CUSTOMER_ID");
		String addrSl = inputDTO.get("ADDR_SL");
		String effectiveDate = inputDTO.get("EFF_DATE");
		String action = inputDTO.get(ContentManager.USER_ACTION);
		try {
			if (commonvalidation.isEmpty(effectiveDate)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			TBAActionType AT;
			if (action.equals(ADD))
				AT = TBAActionType.ADD;
			else
				AT = TBAActionType.MODIFY;
			if (!commonvalidation.isEffectiveDateValid(effectiveDate, AT)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_EFFECTIVE_DATE);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "ENABLED");
			inputDTO.set(ContentManager.TABLE, "CUSTOMERCONTACTHIST");
			String columns[] = new String[] { context.getEntityCode(), custId, addrSl, effectiveDate };
			inputDTO.setObject(ContentManager.PROGRAM_KEY, columns);
			resultDTO = commonvalidation.validatePK(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (action.equals(ADD)) {
				if (ContentManager.DATA_AVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_EXISTS);
					return resultDTO;
				}
			} else if (action.equals(MODIFY)) {
				if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
					resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_NOT_EXISTS);
					return resultDTO;
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonvalidation.close();
		}
		return resultDTO;
	}

	public boolean validateTitle(String title) {
		MasterValidator validation = new MasterValidator();
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("TITLE_CODE", title);
			formDTO.set(ContentManager.USER_ACTION, USAGE);
			formDTO = validation.validateTitleCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("title", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("title", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateContactPerson(String validatetitle, String contactPerson) {
		CommonValidator validation = new CommonValidator();
		try {
			if (!validation.isEmpty(validatetitle)) {
				if (validation.isEmpty(contactPerson)) {
					getErrorMap().setError("title", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
			}
			if (!validation.isEmpty(contactPerson)) {
				if (validation.isEmpty(validatetitle)) {
					getErrorMap().setError("contactPerson", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
			}
			if (!validation.isValidDescription(contactPerson)) {
				getErrorMap().setError("contactPerson", BackOfficeErrorCodes.INVALID_NAME);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("contactPerson", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateContactDetails1(String contactDetails1) {
		CommonValidator validation = new CommonValidator();
		try {
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("contactDetails1", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateContactDetail2(String contactDetails2) {
		CommonValidator validation = new CommonValidator();
		try {

			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("contactDetails2", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateEmailID(String emailID) {
		CommonValidator validation = new CommonValidator();
		try {
			if (!validation.isEmpty(emailID)) {
				if (!validation.isValidEmail(emailID)) {
					getErrorMap().setError("emailID", BackOfficeErrorCodes.INVALID_EMAIL_ID);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("emailID", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	private boolean validateContactDetailsGrid() {
		CommonValidator validation = new CommonValidator();
		try {
			DTDObject dtdObject = new DTDObject();
			dtdObject.addColumn(0, "SL");
			dtdObject.addColumn(1, "ACTION");
			dtdObject.addColumn(2, "CONTACT_TITLE");
			dtdObject.addColumn(3, "CONTACT_PERSON");
			dtdObject.addColumn(4, "CONTACT_NUM_1");
			dtdObject.addColumn(5, "CONTACT_NUM_2");
			dtdObject.addColumn(6, "EMAIL_ID");
			dtdObject.addColumn(7, "ALTERNATE_EMAIL");
			dtdObject.setXML(xmlcontactDetailGrid);
			if (dtdObject.getRowCount() <= 0) {
				getErrorMap().setError("gridError", BackOfficeErrorCodes.ATLEAST_ONE_ROW);
				return false;
			}
			for (int i = 0; i < dtdObject.getRowCount(); i++) {
				if (!validateTitle(dtdObject.getValue(i, 2))) {
					getErrorMap().setError("gridError", BackOfficeErrorCodes.INVALID_ACTION);
					return false;
				}
				if (!validateContactPerson(dtdObject.getValue(i, 2), dtdObject.getValue(i, 3))) {
					getErrorMap().setError("gridError", BackOfficeErrorCodes.INVALID_ACTION);
					return false;
				}
				if (!validateContactDetails1(dtdObject.getValue(i, 4))) {
					getErrorMap().setError("gridError", BackOfficeErrorCodes.INVALID_ACTION);
					return false;
				}
				if (!validateContactDetail2(dtdObject.getValue(i, 5))) {
					getErrorMap().setError("gridError", BackOfficeErrorCodes.INVALID_ACTION);
					return false;
				}
				if (!validateEmailID(dtdObject.getValue(i, 6))) {
					getErrorMap().setError("gridError", BackOfficeErrorCodes.INVALID_ACTION);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("gridError", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateRemarks() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (remarks != null && remarks.length() > 0) {
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			if (getAction().equals(MODIFY)) {
				if (commonValidator.isEmpty(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}
}
