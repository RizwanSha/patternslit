/*
 *
 Author : Raja E
 Created Date : 07-02-2017
 Spec Reference : ECUSTOMERADDRESS
 Modification History
 ----------------------------------------------------------------------
 Sl.No		Modified Date	Author			Modified Changes	Version
 ----------------------------------------------------------------------

 */

package patterns.config.web.forms.reg;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.framework.web.configuration.menu.MenuUtils;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.MasterValidator;
import patterns.config.validations.RegistrationValidator;
import patterns.config.web.forms.GenericFormBean;

public class ecustomeraddressbean extends GenericFormBean {

	private static final long serialVersionUID = 1L;

	private String customerID;
	private String addressSerial;
	private String officeAddrIdentifier;
	private String address;
	private String pinCode;
	private String state;
	private String tanNo;
	private boolean removeAddress;
	private String remarks;
	private String addresspk;
	private String redirectPageId;
	private String redirectPagePath;

	public String getCustomerID() {
		return customerID;
	}

	public void setCustomerID(String customerID) {
		this.customerID = customerID;
	}

	public String getAddressSerial() {
		return addressSerial;
	}

	public void setAddressSerial(String addressSerial) {
		this.addressSerial = addressSerial;
	}

	public String getOfficeAddrIdentifier() {
		return officeAddrIdentifier;
	}

	public void setOfficeAddrIdentifier(String officeAddrIdentifier) {
		this.officeAddrIdentifier = officeAddrIdentifier;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPinCode() {
		return pinCode;
	}

	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getTanNo() {
		return tanNo;
	}

	public void setTanNo(String tanNo) {
		this.tanNo = tanNo;
	}

	public boolean isRemoveAddress() {
		return removeAddress;
	}

	public void setRemoveAddress(boolean removeAddress) {
		this.removeAddress = removeAddress;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getAddresspk() {
		return addresspk;
	}

	public void setAddresspk(String addresspk) {
		this.addresspk = addresspk;
	}

	public String getRedirectPageId() {
		return redirectPageId;
	}

	public void setRedirectPageId(String redirectPageId) {
		this.redirectPageId = redirectPageId;
	}

	public String getRedirectPagePath() {
		return redirectPagePath;
	}

	public void setRedirectPagePath(String redirectPagePath) {
		this.redirectPagePath = redirectPagePath;
	}

	@Override
	public void reset() {
		// TODO Auto-generated method stub
		customerID = RegularConstants.EMPTY_STRING;
		addressSerial = RegularConstants.EMPTY_STRING;
		officeAddrIdentifier = RegularConstants.EMPTY_STRING;
		address = RegularConstants.EMPTY_STRING;
		pinCode = RegularConstants.EMPTY_STRING;
		state = RegularConstants.EMPTY_STRING;
		tanNo = RegularConstants.EMPTY_STRING;
		removeAddress = false;
		addresspk = RegularConstants.EMPTY_STRING;
		remarks = RegularConstants.EMPTY_STRING;
		setProcessBO("patterns.config.framework.bo.reg.ecustomeraddressBO");

		String pk = webContext.getRequest().getParameter("pk");
		if (pk != null) {
			addresspk = pk;
		}

		// addresspk = WebContext.getInstance().getRequest().getParameter("pk");
		if (webContext.getRequest().getParameter("addresspk") != null) {
			addresspk = webContext.getRequest().getParameter("addresspk");
		}
		if (webContext.getRequest().getParameter("_RL") != null) {
			redirectPageId = webContext.getRequest().getParameter("_RL");
			redirectPagePath = MenuUtils.resolveForward(redirectPageId,
					webContext.getRequest());
		}
	}

	@Override
	public void validate() {
		// TODO Auto-generated method stub
		if (validateCustomerID() && validateAddressSerial()) {
			validateOfficeAddrIdentifier();
			validateAddress();
			validatePinCode();
			validateStateCode();
			validateTanNo();
			validateRemarks();
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.set("CUSTOMER_ID", customerID);
				formDTO.set("ADDR_SL", addressSerial);
				formDTO.set("OFFICE_ADDRESS_ID", officeAddrIdentifier);
				formDTO.set("ADDRESS", address);
				formDTO.set("PIN_CODE", pinCode);
				formDTO.set("STATE_CODE", state);
				formDTO.set("TAN_NO", tanNo);
				if (getAction().equals(ADD))
					formDTO.set("DETELED_FLAG", decodeBooleanToString(false));
				else
					formDTO.set("DETELED_FLAG",
							decodeBooleanToString(removeAddress));
				formDTO.set("REMARKS", remarks);

			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("customerID",
					BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
	}

	public boolean validateCustomerID() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("CUSTOMER_ID", customerID);
			formDTO.set(ContentManager.ACTION, USAGE);
			formDTO = validateCustomerID(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("customerID",
						formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("customerID",
					BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateCustomerID(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		RegistrationValidator validation = new RegistrationValidator();
		String customerId = inputDTO.get("CUSTOMER_ID");
		try {
			if (validation.isEmpty(customerId)) {
				resultDTO.set(ContentManager.ERROR,
						BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			if (!validation.isValidNumber(customerId)) {
				resultDTO.set(ContentManager.ERROR,
						BackOfficeErrorCodes.NUMERIC_CHECK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "CUSTOMER_NAME");
			resultDTO = validation.valildateCustomerID(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR,
						resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO
					.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR,
						BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR,
					BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateAddressSerial() {
		try {
			if (getAction().equals(MODIFY)
					|| getRectify().equals(RegularConstants.COLUMN_ENABLE)) {
				DTObject formDTO = new DTObject();
				formDTO.set("CUSTOMER_ID", customerID);
				formDTO.set("ADDR_SL", addressSerial);
				formDTO.set(ContentManager.ACTION, getAction());
				formDTO = validateAddressSerial(formDTO);
				if (formDTO.get(ContentManager.ERROR) != null) {
					getErrorMap().setError("addressSerial",
							formDTO.get(ContentManager.ERROR));
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("addressSerial",
					BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateAddressSerial(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		RegistrationValidator validation = new RegistrationValidator();
		String addrSl = inputDTO.get("ADDR_SL");
		try {
			if (validation.isEmpty(addrSl)) {
				resultDTO.set(ContentManager.ERROR,
						BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			if (!validation.isValidNumber(addrSl)) {
				resultDTO.set(ContentManager.ERROR,
						BackOfficeErrorCodes.INVALID_NUMBER);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH, ContentManager.FETCH_ALL);
			resultDTO = validation.validateCustomerAddressSerial(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR,
						resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO
					.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR,
						BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR,
					BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateOfficeAddrIdentifier() {
		CommonValidator validation = new CommonValidator();
		try {
			if (!validation.isEmpty(officeAddrIdentifier)) {
				if (!validation.isValidConciseDescription(officeAddrIdentifier)) {
					getErrorMap().setError("officeAddrIdentifier",
							BackOfficeErrorCodes.INVALID_CONCISE_DESCRIPTION);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("officeAddrIdentifier",
					BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateAddress() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(address)) {
				getErrorMap().setError("address",
						BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidAddress(address)) {
				getErrorMap().setError("address",
						BackOfficeErrorCodes.INVALID_ADDRESS);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("address",
					BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validatePinCode() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(pinCode)) {
				getErrorMap().setError("pinCode",
						BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidNumber(pinCode)) {
				getErrorMap().setError("pinCode",
						BackOfficeErrorCodes.NUMERIC_CHECK);
				return false;
			}
			if (validation.isValidNegativeNumber(pinCode)) {
				getErrorMap().setError("pinCode",
						BackOfficeErrorCodes.POSITIVE_CHECK);
				return false;
			}
			if (pinCode.length() != 6) {
				getErrorMap().setError("pinCode",
						BackOfficeErrorCodes.PINCODE_LENGTH);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("pinCode",
					BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateStateCode() {
		try {
			DTObject formDTO = getFormDTO();
			formDTO.set("STATE_CODE", state);
			formDTO.set(ContentManager.USER_ACTION, USAGE);
			formDTO = validateStateCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("state",
						formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("state",
					BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateStateCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		MasterValidator validation = new MasterValidator();
		String stateCode = inputDTO.get("STATE_CODE");
		try {
			if (validation.isEmpty(stateCode)) {
				resultDTO.set(ContentManager.ERROR,
						BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
			resultDTO = validation.validateStateCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR,
						resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(
					ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR,
						BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}

		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR,
					BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateTanNo() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(tanNo)) {
				getErrorMap().setError("tanNo",
						BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidDescription(tanNo)) {
				getErrorMap().setError("tanNo",
						BackOfficeErrorCodes.INVALID_TAN_NO);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("tanNo",
					BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateRemarks() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (remarks != null && remarks.length() > 0) {
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks",
							BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			if (getAction().equals(MODIFY)) {
				if (commonValidator.isEmpty(remarks)) {
					getErrorMap().setError("remarks",
							BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks",
							BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks",
					BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}
}
