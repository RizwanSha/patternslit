package patterns.config.web.forms.reg;

import java.sql.ResultSet;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.DHTMLXGridUtility;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.LeaseValidator;
import patterns.config.validations.MasterValidator;
import patterns.config.validations.RegistrationValidator;
import patterns.config.web.forms.GenericFormBean;

public class ecustomeraddconbean extends GenericFormBean {

	private static final long serialVersionUID = 1L;
	private String lesseeCode;
	private String customerID;
	private String xmlAddressGrid;
	private String xmlContactGrid;
	private String addresspk;
	private String contactpk;

	public String getLesseeCode() {
		return lesseeCode;
	}

	public void setLesseeCode(String lesseeCode) {
		this.lesseeCode = lesseeCode;
	}

	public String getCustomerID() {
		return customerID;
	}

	public void setCustomerID(String customerID) {
		this.customerID = customerID;
	}

	public String getXmlAddressGrid() {
		return xmlAddressGrid;
	}

	public void setXmlAddressGrid(String xmlAddressGrid) {
		this.xmlAddressGrid = xmlAddressGrid;
	}

	public String getXmlContactGrid() {
		return xmlContactGrid;
	}

	public void setXmlContactGrid(String xmlContactGrid) {
		this.xmlContactGrid = xmlContactGrid;
	}

	public String getAddresspk() {
		return addresspk;
	}

	public void setAddresspk(String addresspk) {
		this.addresspk = addresspk;
	}

	public String getContactpk() {
		return contactpk;
	}

	public void setContactpk(String contactpk) {
		this.contactpk = contactpk;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public void reset() {
		// TODO Auto-generated method stub
		lesseeCode = RegularConstants.EMPTY_STRING;
		customerID = RegularConstants.EMPTY_STRING;
		xmlAddressGrid = RegularConstants.EMPTY_STRING;
		xmlContactGrid = RegularConstants.EMPTY_STRING;
		if (webContext.getRequest().getParameter("addresspk") != null) {
			addresspk = webContext.getRequest().getParameter("addresspk");
		}
		if (webContext.getRequest().getParameter("contactpk") != null) {
			contactpk = webContext.getRequest().getParameter("contactpk");
		}
		// setProcessBO("patterns.config.framework.bo.reg.ecustomeraddconBO");
	}

	@Override
	public void validate() {
		if (validateLesseeCode()) {
			validateCustomerID();
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.set("SUNDRY_DB_AC", lesseeCode);
				formDTO.set("CUSTOMER_ID", customerID);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("customerID", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
	}

	public boolean validateLesseeCode() {
		try {

			DTObject formDTO = getFormDTO();
			formDTO.set("SUNDRY_DB_AC", lesseeCode);
			formDTO.set(ContentManager.USER_ACTION, getAction());
			formDTO = validateLesseeCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("lesseeCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("lesseeCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateLesseeCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		LeaseValidator validation = new LeaseValidator();
		String lesseeCode = inputDTO.get("SUNDRY_DB_AC");
		try {
			if (validation.isEmpty(lesseeCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH, ContentManager.FETCH_ALL);
			resultDTO = validation.validateLesseeCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
			inputDTO.set("CUSTOMER_ID", resultDTO.get("CUSTOMER_ID"));
			resultDTO = loadGridValues(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
			resultDTO.set("CUSTOMER_ID", inputDTO.get("CUSTOMER_ID"));
			return resultDTO;
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateCustomerID() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("CUSTOMER_ID", customerID);
			formDTO.set(ContentManager.ACTION, USAGE);
			formDTO = validateCustomerID(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("customerID", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("customerID", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateCustomerID(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		RegistrationValidator validation = new RegistrationValidator();
		String customerId = inputDTO.get("CUSTOMER_ID");
		try {
			if (validation.isEmpty(customerId)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			if (!validation.isValidNumber(customerId)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.NUMERIC_CHECK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "CUSTOMER_NAME");
			resultDTO = validation.valildateCustomerID(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_NOT_EXISTS);
				return resultDTO;
			}
			resultDTO = loadGridValues(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
			return resultDTO;

		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	// today

	public DTObject loadGridValues(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		MasterValidator validator = new MasterValidator();
		try {
			DBUtil dbutil = validator.getDbContext().createUtilInstance();
			StringBuffer sqlQuery = new StringBuffer(RegularConstants.EMPTY_STRING);
			sqlQuery.append("SELECT C.CUSTOMER_NAME,A.ADDR_SL,A.OFFICE_ADDRESS_ID,A.ADDRESS,A.PIN_CODE,A.STATE_CODE,A.TAN_NO,A.DETELED_FLAG,TO_CHAR(F.OPER_DATETIME , ?) AS ADDED_ON,");
			sqlQuery.append("CA.ADDR_SL AS CONTACT_SL  FROM CUSTOMER C ");
			sqlQuery.append(" INNER JOIN CUSTOMERADDRESS A ON (C.ENTITY_CODE = A.ENTITY_CODE AND C.CUSTOMER_ID = A.CUSTOMER_ID) ");
			sqlQuery.append("LEFT OUTER JOIN CUSTOMERCONTACT CA ON (C.ENTITY_CODE = CA.ENTITY_CODE AND C.CUSTOMER_ID = CA.CUSTOMER_ID AND CA.ADDR_SL = A.ADDR_SL) ");
			sqlQuery.append(" LEFT OUTER JOIN FWRKOPERATIONS F ON (F.ENTITY_CODE = A.ENTITY_CODE AND F.FORM_NAME =? AND F.FORM_PK=CONCAT(A.ENTITY_CODE,'|',A.CUSTOMER_ID,'|',A.ADDR_SL) AND F.OPER_TYPE='E') ");
			sqlQuery.append("WHERE C.ENTITY_CODE=? AND C.CUSTOMER_ID=?");

			dbutil.reset();
			dbutil.setSql(sqlQuery.toString());
			dbutil.setString(1, BackOfficeConstants.TBA_DATE_FORMAT);
			dbutil.setString(2, "ECUSTOMERADDRESS");
			dbutil.setLong(3, Long.parseLong(context.getEntityCode()));
			dbutil.setLong(4, Long.parseLong(inputDTO.get("CUSTOMER_ID")));
			ResultSet rset = dbutil.executeQuery();
			DHTMLXGridUtility gridUtility = new DHTMLXGridUtility();
			gridUtility.init();
			int rowId = 0;
			while (rset.next()) {
				if (rowId == 0) {
					resultDTO.set("CUSTOMER_NAME", rset.getString("CUSTOMER_NAME"));
				}
				rowId = rowId + 1;
				gridUtility.startRow(String.valueOf(rowId));
				gridUtility.setCell("Edit^javascript:editCustomerAddress(\"" + context.getEntityCode() + "|" + inputDTO.get("CUSTOMER_ID") + "|" + rset.getString("ADDR_SL") + "\")");
				gridUtility.setCell(rset.getString("ADDR_SL"));
				gridUtility.setCell(rset.getString("OFFICE_ADDRESS_ID"));
				gridUtility.setCell(rset.getString("ADDRESS"));
				gridUtility.setCell(rset.getString("PIN_CODE"));
				gridUtility.setCell(rset.getString("STATE_CODE"));
				gridUtility.setCell(rset.getString("TAN_NO"));
				gridUtility.setCell(rset.getString("DETELED_FLAG"));
				gridUtility.setCell(rset.getString("ADDED_ON"));
				if (rset.getString("CONTACT_SL") != RegularConstants.NULL)
					gridUtility.setCell("View^javascript:viewCustomerContact(\"" + context.getEntityCode() + "|" + inputDTO.get("CUSTOMER_ID") + "|" + rset.getString("CONTACT_SL") + "\")");
				else
					gridUtility.setCell(RegularConstants.EMPTY_STRING);
				gridUtility.setCell(context.getEntityCode() + "|" + inputDTO.get("CUSTOMER_ID") + "|" + rset.getString("ADDR_SL"));
				gridUtility.endRow();
			}
			gridUtility.finish();
			if (rowId != 0) {
				String resultXML = gridUtility.getXML();
				resultDTO.set(ContentManager.RESULT_XML, resultXML);
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
			} else {
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return resultDTO;
	}

	public DTObject validateViewCustomerContact(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		MasterValidator validator = new MasterValidator();
		try {
			DBUtil dbutil = validator.getDbContext().createUtilInstance();
			StringBuffer sqlQuery = new StringBuffer(RegularConstants.EMPTY_STRING);
			sqlQuery.append("SELECT TO_CHAR(CA.EFF_DATE , '" + context.getDateFormat() + "') AS EFF_DATE,CD.DTL_SL,CD.CONTACT_PERSON,CD.CONTACT_NUM_1,CD.CONTACT_NUM_2,CD.EMAIL_ID FROM CUSTOMERCONTACT CA ");
			sqlQuery.append("INNER JOIN CUSTOMERCONTACTDTL CD ON (CA.ENTITY_CODE = CD.ENTITY_CODE AND CA.CUSTOMER_ID = CD.CUSTOMER_ID AND CA.ADDR_SL = CD.ADDR_SL) ");
			sqlQuery.append("WHERE CA.ENTITY_CODE=? AND CD.CUSTOMER_ID=? and CD.ADDR_SL=?");

			dbutil.reset();
			dbutil.setSql(sqlQuery.toString());
			dbutil.setLong(1, Long.parseLong(context.getEntityCode()));
			dbutil.setLong(2, Long.parseLong(inputDTO.get("CUSTOMER_ID")));
			dbutil.setLong(3, Integer.parseInt(inputDTO.get("ADDR_SL")));
			ResultSet rset = dbutil.executeQuery();
			DHTMLXGridUtility gridUtility = new DHTMLXGridUtility();
			gridUtility.init();
			int rowId = 0;
			while (rset.next()) {
				if (rowId == 0) {
					resultDTO.set("DTL_SL", rset.getString("DTL_SL"));
					resultDTO.set("EFF_DATE", rset.getString("EFF_DATE"));
				}
				rowId = rowId + 1;
				gridUtility.startRow(String.valueOf(rowId));
				gridUtility.setCell(rset.getString("DTL_SL"));
				gridUtility.setCell(rset.getString("CONTACT_PERSON"));
				gridUtility.setCell(rset.getString("CONTACT_NUM_1"));
				gridUtility.setCell(rset.getString("CONTACT_NUM_2"));
				gridUtility.setCell(rset.getString("EMAIL_ID"));
				gridUtility.setCell(context.getEntityCode() + "|" + inputDTO.get("CUSTOMER_ID") + "|" + inputDTO.get("ADDR_SL") + "|" + inputDTO.get("EFF_DATE"));
				gridUtility.endRow();
			}
			gridUtility.finish();
			if (rowId != 0) {
				String resultXML = gridUtility.getXML();
				resultDTO.set(ContentManager.RESULT_XML, resultXML);
				resultDTO.set("ADDR_SL", inputDTO.get("ADDR_SL"));
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
			} else {
				resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return resultDTO;
	}
}
