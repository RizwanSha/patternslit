/*
 *
 Author : Raja E
 Created Date : 02-02-2017
 Spec Reference : ECUSTOMERREG
 Modification History
 version 3.0
  Created Date : 15-02-2017
 ----------------------------------------------------------------------
 Sl.No		Modified Date	Author			Modified Changes	Version
 ----------------------------------------------------------------------

 */

package patterns.config.web.forms.reg;

import java.util.ArrayList;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.service.DTDObject;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.EmployeeValidator;
import patterns.config.validations.MasterValidator;
import patterns.config.validations.RegistrationValidator;
import patterns.config.web.forms.GenericFormBean;

public class ecustomerregbean extends GenericFormBean {

	private static final long serialVersionUID = 1L;
	private String customerID;
	private String customerName;
	private String dateOfReg;
	private String product;
	private String debtorAccount;
	private String accountIDPrincipal;
	private String accountIDInterest;
	private String portfolioCode;
	private String regionCode;
	private String panNo;
	private String regOfficeAddress;
	private String pinCode;
	private String state;
	private String managerStaffID;
	private String xmlproductDetailGrid;
	private String accountIDUnearned;

	public String getCustomerID() {
		return customerID;
	}

	public void setCustomerID(String customerID) {
		this.customerID = customerID;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getDateOfReg() {
		return dateOfReg;
	}

	public void setDateOfReg(String dateOfReg) {
		this.dateOfReg = dateOfReg;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getDebtorAccount() {
		return debtorAccount;
	}

	public void setDebtorAccount(String debtorAccount) {
		this.debtorAccount = debtorAccount;
	}

	public String getAccountIDPrincipal() {
		return accountIDPrincipal;
	}

	public void setAccountIDPrincipal(String accountIDPrincipal) {
		this.accountIDPrincipal = accountIDPrincipal;
	}

	public String getAccountIDInterest() {
		return accountIDInterest;
	}

	public void setAccountIDInterest(String accountIDInterest) {
		this.accountIDInterest = accountIDInterest;
	}
	
	public String getPortfolioCode() {
		return portfolioCode;
	}

	public void setPortfolioCode(String portfolioCode) {
		this.portfolioCode = portfolioCode;
	}

	public String getRegionCode() {
		return regionCode;
	}

	public void setRegionCode(String regionCode) {
		this.regionCode = regionCode;
	}

	public String getPanNo() {
		return panNo;
	}

	public void setPanNo(String panNo) {
		this.panNo = panNo;
	}

	public String getRegOfficeAddress() {
		return regOfficeAddress;
	}

	public void setRegOfficeAddress(String regOfficeAddress) {
		this.regOfficeAddress = regOfficeAddress;
	}

	public String getPinCode() {
		return pinCode;
	}

	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getManagerStaffID() {
		return managerStaffID;
	}

	public void setManagerStaffID(String managerStaffID) {
		this.managerStaffID = managerStaffID;
	}

	public String getXmlproductDetailGrid() {
		return xmlproductDetailGrid;
	}

	public void setXmlproductDetailGrid(String xmlproductDetailGrid) {
		this.xmlproductDetailGrid = xmlproductDetailGrid;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getAccountIDUnearned() {
		return accountIDUnearned;
	}

	public void setAccountIDUnearned(String accountIDUnearned) {
		this.accountIDUnearned = accountIDUnearned;
	}

	public void reset() {
		customerID = RegularConstants.EMPTY_STRING;
		customerName = RegularConstants.EMPTY_STRING;
		dateOfReg = RegularConstants.EMPTY_STRING;
		product = RegularConstants.EMPTY_STRING;
		debtorAccount = RegularConstants.EMPTY_STRING;
		accountIDPrincipal = RegularConstants.EMPTY_STRING;
		accountIDInterest = RegularConstants.EMPTY_STRING;
		portfolioCode = RegularConstants.EMPTY_STRING;
		panNo = RegularConstants.EMPTY_STRING;
		regionCode = RegularConstants.EMPTY_STRING;
		regOfficeAddress = RegularConstants.EMPTY_STRING;
		pinCode = RegularConstants.EMPTY_STRING;
		state = RegularConstants.EMPTY_STRING;
		managerStaffID = RegularConstants.EMPTY_STRING;
		accountIDUnearned = RegularConstants.EMPTY_STRING;
		xmlproductDetailGrid = RegularConstants.EMPTY_STRING;
		setProcessBO("patterns.config.framework.bo.reg.ecustomerregBO");
	}

	public void validate() {
		if (validateCustomerID()) {
			validateCustomerName();
			validateProductDetailsGrid();
			validatePanNo();
			validateRegionCode();
			validateRegOfficeAddress();
			validatePinCode();
			validateStateCode();
			validateManagerStaffID();
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.set("CUSTOMER_ID", customerID);
				formDTO.set("CUSTOMER_NAME", customerName);
				formDTO.set("DATE_OF_REG", dateOfReg);
				formDTO.set("PAN_NO", panNo);
				formDTO.set("REGION_CODE", regionCode);
				formDTO.set("REG_ADDRESS", regOfficeAddress);
				formDTO.set("PIN_CODE", pinCode);
				formDTO.set("STATE_CODE", state);
				formDTO.set("STAFF_CODE", managerStaffID);

				DTDObject dtdObject = new DTDObject();
				dtdObject.addColumn(0, "SL");
				dtdObject.addColumn(1, "ACTION");
				dtdObject.addColumn(2, "LEASE_PRODUCT_CODE");
				dtdObject.addColumn(3, "PORTFOLIO_CODE");
				dtdObject.addColumn(4, "SUNDRY_DB_AC");
				dtdObject.addColumn(5, "PRINCIPAL_AC");
				dtdObject.addColumn(6, "INTEREST_AC");
				dtdObject.addColumn(7, "UNEARNED_INTEREST_AC");
				dtdObject.addColumn(8, "LEASE_TYPE");
				dtdObject.setXML(xmlproductDetailGrid);
				formDTO.setDTDObject("CUSTOMERACDTL", dtdObject);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("customerID", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
	}

	public boolean validateCustomerID() {
		try {
			if (getAction().equals(MODIFY)) {
				getErrorMap().setError("customerID", BackOfficeErrorCodes.INVALID_ACTION);
				return false;
			}
			if (getRectify().equals(RegularConstants.COLUMN_ENABLE)) {
				DTObject formDTO = new DTObject();
				formDTO.set("CUSTOMER_ID", customerID);
				formDTO.set(ContentManager.ACTION, getAction());
				formDTO = validateCustomerID(formDTO);
				if (formDTO.get(ContentManager.ERROR) != null) {
					getErrorMap().setError("customerID", formDTO.get(ContentManager.ERROR));
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("customerID", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateCustomerID(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		RegistrationValidator validation = new RegistrationValidator();
		String customerId = inputDTO.get("CUSTOMER_ID");
		try {
			if (validation.isEmpty(customerId)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			if (!validation.isValidNumber(customerId)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.NUMERIC_CHECK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "CUSTOMER_NAME");
			resultDTO = validation.valildateCustomerID(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateCustomerName() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(customerName)) {
				getErrorMap().setError("customerName", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidName(customerName)) {
				getErrorMap().setError("customerName", BackOfficeErrorCodes.INVALID_NAME);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("customerName", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateProductCode(String product) {
		try {
			DTObject formDTO = getFormDTO();
			formDTO.set("LEASE_PRODUCT_CODE", product);
			formDTO.set(ContentManager.USER_ACTION, USAGE);
			formDTO = validateProductCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("product", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("product", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateProductCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		MasterValidator validation = new MasterValidator();
		String productCode = inputDTO.get("LEASE_PRODUCT_CODE");
		try {
			if (validation.isEmpty(productCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION,LEASE_TYPE");
			resultDTO = validation.validateLeaseProductCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validatePortfolioCode(String portfolioCode) {
		try {
			DTObject formDTO = getFormDTO();
			formDTO.set("PORTFOLIO_CODE", portfolioCode);
			formDTO.set(ContentManager.USER_ACTION, USAGE);
			formDTO = validatePortfolioCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("portfolioCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("portfolioCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validatePortfolioCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		MasterValidator validation = new MasterValidator();
		String portfolioCode = inputDTO.get("PORTFOLIO_CODE");
		try {
			if (validation.isEmpty(portfolioCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
			resultDTO = validation.validatePortFolioCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}

		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}
	
	public boolean validateDebtorAccount(String debtorAccount) {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(debtorAccount)) {
				getErrorMap().setError("debtorAccount", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidOtherInformation25(debtorAccount)) {
				getErrorMap().setError("debtorAccount", BackOfficeErrorCodes.LENGTH_10);
				return false;
			}
			DTObject formDTO = new DTObject();
			formDTO.set("PROGRAM_ID", context.getProcessID());
			formDTO.set("CUSTOMER_ID", customerID);
			formDTO.set("SUNDRY_DB_AC", debtorAccount);
			formDTO.set("ACTION", USAGE);
			formDTO = validateDebtorAccount(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("debtorAccount", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("debtorAccount", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public DTObject validateDebtorAccount(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		MasterValidator validation = new MasterValidator();
		try {
			resultDTO = isDebtorAccountExist(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	private DTObject isDebtorAccountExist(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		CommonValidator validator = new CommonValidator();
		try {
			String primaryKey = context.getEntityCode() + RegularConstants.PK_SEPARATOR + inputDTO.get("CUSTOMER_ID");
			String dedupKey = context.getEntityCode() + RegularConstants.PK_SEPARATOR + inputDTO.get("SUNDRY_DB_AC");
			DTObject formDTO = new DTObject();
			formDTO.set(ContentManager.PROGRAM_ID, inputDTO.get("PROGRAM_ID"));
			formDTO.set(ContentManager.PURPOSE_ID, "SUNDRY_DB_ACCOUNT");
			formDTO.set(ContentManager.PROGRAM_KEY, primaryKey);
			formDTO.set(ContentManager.DEDUP_KEY, dedupKey);
			resultDTO = validator.isDeDupAlreadyExist(formDTO);
			if (resultDTO.get(ContentManager.ERROR) != null)
				return resultDTO;
			else if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.DEBTOR_ACCOUNT_LINKED);
				Object[] param = { resultDTO.get("LINKED_TO_PK") };
				resultDTO.setObject(ContentManager.ERROR_PARAM, param);
				return resultDTO;
			}

		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return resultDTO;
	}

	public boolean validateAccountIDPrincipal(String accountIDPrincipal) {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(accountIDPrincipal)) {
				getErrorMap().setError("accountIDPrincipal", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidOtherInformation25(accountIDPrincipal)) {
				getErrorMap().setError("accountIDPrincipal", BackOfficeErrorCodes.LENGTH_10);
				return false;
			}
			DTObject formDTO = new DTObject();
			formDTO.set("PROGRAM_ID", context.getProcessID());
			formDTO.set("CUSTOMER_ID", customerID);
			formDTO.set("PRINCIPAL_AC", accountIDPrincipal);
			formDTO.set("ACTION", USAGE);
			formDTO = validatePrincipalAccount(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("accountIDPrincipal", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("accountIDPrincipal", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public DTObject validatePrincipalAccount(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		MasterValidator validation = new MasterValidator();
		try {
			resultDTO = isPrincipalAccountExist(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	private DTObject isPrincipalAccountExist(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		CommonValidator validator = new CommonValidator();
		try {
			String primaryKey = context.getEntityCode() + RegularConstants.PK_SEPARATOR + inputDTO.get("CUSTOMER_ID");
			String dedupKey = context.getEntityCode() + RegularConstants.PK_SEPARATOR + inputDTO.get("PRINCIPAL_AC");
			DTObject formDTO = new DTObject();
			formDTO.set(ContentManager.PROGRAM_ID, inputDTO.get("PROGRAM_ID"));
			formDTO.set(ContentManager.PURPOSE_ID, "PRINCIPAL_ACCOUNT");
			formDTO.set(ContentManager.PROGRAM_KEY, primaryKey);
			formDTO.set(ContentManager.DEDUP_KEY, dedupKey);
			resultDTO = validator.isDeDupAlreadyExist(formDTO);
			if (resultDTO.get(ContentManager.ERROR) != null)
				return resultDTO;
			else if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.ACCOUNT_ID_LINKED);
				String customerID=resultDTO.get("LINKED_TO_PK").substring(2, resultDTO.get("LINKED_TO_PK").length());
				Object[] param = { customerID };
				resultDTO.setObject(ContentManager.ERROR_PARAM, param);
				return resultDTO;
			}

		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return resultDTO;
	}

	public boolean validateAccountIDInterest(String accountIDInterest) {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(accountIDInterest)) {
				getErrorMap().setError("debtorAccount", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidOtherInformation25(accountIDInterest)) {
				getErrorMap().setError("accountIDInterest", BackOfficeErrorCodes.LENGTH_10);
				return false;
			}
			DTObject formDTO = new DTObject();
			formDTO.set("PROGRAM_ID", context.getProcessID());
			formDTO.set("CUSTOMER_ID", customerID);
			formDTO.set("INTEREST_AC", accountIDInterest);
			formDTO.set("ACTION", USAGE);
			formDTO = validateAccountIDInterest(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("accountIDInterest", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("accountIDInterest", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public DTObject validateAccountIDInterest(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		MasterValidator validation = new MasterValidator();
		try {
			resultDTO = isAccountIDInterestExist(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	private DTObject isAccountIDInterestExist(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		CommonValidator validator = new CommonValidator();
		try {
			String primaryKey = context.getEntityCode() + RegularConstants.PK_SEPARATOR + inputDTO.get("CUSTOMER_ID");
			String dedupKey = context.getEntityCode() + RegularConstants.PK_SEPARATOR + inputDTO.get("INTEREST_AC");
			DTObject formDTO = new DTObject();
			formDTO.set(ContentManager.PROGRAM_ID, inputDTO.get("PROGRAM_ID"));
			formDTO.set(ContentManager.PURPOSE_ID, "INTEREST_ACCOUNT");
			formDTO.set(ContentManager.PROGRAM_KEY, primaryKey);
			formDTO.set(ContentManager.DEDUP_KEY, dedupKey);
			resultDTO = validator.isDeDupAlreadyExist(formDTO);
			if (resultDTO.get(ContentManager.ERROR) != null)
				return resultDTO;
			else if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.ACCOUNT_ID_LINKED);
				Object[] param = { resultDTO.get("LINKED_TO_PK") };
				resultDTO.setObject(ContentManager.ERROR_PARAM, param);
				return resultDTO;
			}

		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return resultDTO;
	}

	public boolean validateAccountIDUnearned(String accountIDUnearned) {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(accountIDUnearned)) {
				getErrorMap().setError("accountIDUnearned", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidOtherInformation25(accountIDUnearned)) {
				getErrorMap().setError("accountIDUnearned", BackOfficeErrorCodes.LENGTH_10);
				return false;
			}
			DTObject formDTO = new DTObject();
			formDTO.set("PROGRAM_ID", context.getProcessID());
			formDTO.set("CUSTOMER_ID", customerID);
			formDTO.set("UNEARNED_INTEREST_AC", accountIDUnearned);
			formDTO.set("ACTION", USAGE);
			formDTO = validateUnearnedInterestAccount(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("accountIDUnearned", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("accountIDUnearned", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public DTObject validateUnearnedInterestAccount(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		MasterValidator validation = new MasterValidator();
		try {
			resultDTO = isUnearnedInterestAccountExist(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	private DTObject isUnearnedInterestAccountExist(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		CommonValidator validator = new CommonValidator();
		try {
			String primaryKey = context.getEntityCode() + RegularConstants.PK_SEPARATOR + inputDTO.get("CUSTOMER_ID");
			String dedupKey = context.getEntityCode() + RegularConstants.PK_SEPARATOR + inputDTO.get("UNEARNED_INTEREST_AC");
			DTObject formDTO = new DTObject();
			formDTO.set(ContentManager.PROGRAM_ID, inputDTO.get("PROGRAM_ID"));
			formDTO.set(ContentManager.PURPOSE_ID, "UNEARNED_INTEREST_ACCOUNT");
			formDTO.set(ContentManager.PROGRAM_KEY, primaryKey);
			formDTO.set(ContentManager.DEDUP_KEY, dedupKey);
			resultDTO = validator.isDeDupAlreadyExist(formDTO);
			if (resultDTO.get(ContentManager.ERROR) != null)
				return resultDTO;
			else if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.ACCOUNT_ID_LINKED);
				Object[] param = { resultDTO.get("LINKED_TO_PK") };
				resultDTO.setObject(ContentManager.ERROR_PARAM, param);
				return resultDTO;
			}

		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return resultDTO;
	}

	public boolean validatePanNo() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(panNo)) {
				getErrorMap().setError("panNo", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidPanNo(panNo)) {
				getErrorMap().setError("panNo", BackOfficeErrorCodes.INVALID_PAN_NO);
				return false;
			}
			DTObject formDTO = new DTObject();
			formDTO.set("PROGRAM_ID", context.getProcessID());
			formDTO.set("CUSTOMER_ID", customerID);
			formDTO.set("PAN_NO", panNo);
			formDTO.set("ACTION", USAGE);
			formDTO = validatePanNo(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("panNo", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("panNo", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public DTObject validatePanNo(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		MasterValidator validation = new MasterValidator();
		try {
			resultDTO = isPANNumberExist(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	private DTObject isPANNumberExist(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		CommonValidator validator = new CommonValidator();
		try {
			String primaryKey = context.getEntityCode() + RegularConstants.PK_SEPARATOR + inputDTO.get("CUSTOMER_ID");
			String dedupKey = context.getEntityCode() + RegularConstants.PK_SEPARATOR + inputDTO.get("PAN_NO");
			DTObject formDTO = new DTObject();
			formDTO.set(ContentManager.PROGRAM_ID, inputDTO.get("PROGRAM_ID"));
			formDTO.set(ContentManager.PURPOSE_ID, "PAN_NUMBER");
			formDTO.set(ContentManager.PROGRAM_KEY, primaryKey);
			formDTO.set(ContentManager.DEDUP_KEY, dedupKey);
			resultDTO = validator.isDeDupAlreadyExist(formDTO);
			if (resultDTO.get(ContentManager.ERROR) != null)
				return resultDTO;
			else if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.PAN_NO_LINKED);
				Object[] param = { resultDTO.get("LINKED_TO_PK") };
				resultDTO.setObject(ContentManager.ERROR_PARAM, param);
				return resultDTO;
			}

		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return resultDTO;
	}

	public boolean validateRegionCode() {
		try {
			DTObject formDTO = getFormDTO();
			formDTO.set("REGION_CODE", regionCode);
			formDTO.set(ContentManager.USER_ACTION, USAGE);
			formDTO = validateRegionCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("regionCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("regionCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateRegionCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		MasterValidator validation = new MasterValidator();
		String regionCode = inputDTO.get("REGION_CODE");
		try {
			if (validation.isEmpty(regionCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
			resultDTO = validation.validateRegionCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}

		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateRegOfficeAddress() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(regOfficeAddress)) {
				getErrorMap().setError("regOfficeAddress", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidAddress(regOfficeAddress)) {
				getErrorMap().setError("regOfficeAddress", BackOfficeErrorCodes.INVALID_ADDRESS);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("regOfficeAddress", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validatePinCode() {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(pinCode)) {
				getErrorMap().setError("pinCode", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidNumber(pinCode)) {
				getErrorMap().setError("pinCode", BackOfficeErrorCodes.NUMERIC_CHECK);
				return false;
			}
			if (validation.isValidNegativeNumber(pinCode)) {
				getErrorMap().setError("pinCode", BackOfficeErrorCodes.POSITIVE_CHECK);
				return false;
			}
			if (pinCode.length() != 6) {
				getErrorMap().setError("pinCode", BackOfficeErrorCodes.PINCODE_LENGTH);
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("pinCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean validateStateCode() {
		try {
			DTObject formDTO = getFormDTO();
			formDTO.set("STATE_CODE", state);
			formDTO.set(ContentManager.USER_ACTION, USAGE);
			formDTO = validateStateCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("state", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("state", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateStateCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		MasterValidator validation = new MasterValidator();
		String stateCode = inputDTO.get("STATE_CODE");
		try {
			if (validation.isEmpty(stateCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
			resultDTO = validation.validateStateCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}

		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateManagerStaffID() {
		try {
			DTObject formDTO = getFormDTO();
			formDTO.set("STAFF_CODE", managerStaffID);
			formDTO.set(ContentManager.USER_ACTION, USAGE);
			formDTO = validateManagerStaffID(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("managerStaffID", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("managerStaffID", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateManagerStaffID(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		EmployeeValidator validation = new EmployeeValidator();
		String staffCode = inputDTO.get("STAFF_CODE");
		try {
			if (validation.isEmpty(staffCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH, ContentManager.FETCH_ALL);
			resultDTO = validation.validateStaffCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}

		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	private boolean validateProductDetailsGrid() {
		CommonValidator validation = new CommonValidator();
		try {
			DTDObject dtdObject = new DTDObject();
			dtdObject.addColumn(0, "SL");
			dtdObject.addColumn(1, "ACTION");
			dtdObject.addColumn(2, "LEASE_PRODUCT_CODE");
			dtdObject.addColumn(3, "PORTFOLIO_CODE");
			dtdObject.addColumn(4, "SUNDRY_DB_AC");
			dtdObject.addColumn(5, "PRINCIPAL_AC");
			dtdObject.addColumn(6, "INTEREST_AC");
			dtdObject.addColumn(7, "UNEARNED_INTEREST_AC");
			dtdObject.addColumn(8, "LEASE_TYPE");
			dtdObject.setXML(xmlproductDetailGrid);
			if (dtdObject.getRowCount() <= 0) {
				getErrorMap().setError("gridError", BackOfficeErrorCodes.ATLEAST_ONE_ROW);
				return false;
			}
			if (!validation.checkDuplicateRows(dtdObject, 4)) {
				getErrorMap().setError("gridError", BackOfficeErrorCodes.DUPLICATE_DATA);
				return false;
			}
			if (!validation.checkDuplicateRows(dtdObject, 5)) {
				getErrorMap().setError("gridError", BackOfficeErrorCodes.DUPLICATE_DATA);
				return false;
			}
			if (!checkDuplicate(dtdObject, 6)) {
				getErrorMap().setError("gridError", BackOfficeErrorCodes.DUPLICATE_DATA);
				return false;
			}
			if (!checkDuplicate(dtdObject, 7)) {
				getErrorMap().setError("gridError", BackOfficeErrorCodes.DUPLICATE_DATA);
				return false;
			}			
			for (int i = 0; i < dtdObject.getRowCount(); i++) {
				if (!validateProductCode(dtdObject.getValue(i, 2))) {
					getErrorMap().setError("gridError", BackOfficeErrorCodes.INVALID_ACTION);
					return false;
				}
				if (!validatePortfolioCode(dtdObject.getValue(i, 3))) {
					getErrorMap().setError("gridError", BackOfficeErrorCodes.INVALID_ACTION);
					return false;
				}
				if (!validateDebtorAccount(dtdObject.getValue(i, 4))) {
					getErrorMap().setError("gridError", BackOfficeErrorCodes.INVALID_ACTION);
					return false;
				}
				if (!dtdObject.getValue(i, 8).equals("O")) {
					if (!validateAccountIDPrincipal(dtdObject.getValue(i, 5))) {
						getErrorMap().setError("gridError", BackOfficeErrorCodes.INVALID_ACTION);
						return false;
					}
					if (!validateAccountIDInterest(dtdObject.getValue(i, 6))) {
						getErrorMap().setError("gridError", BackOfficeErrorCodes.INVALID_ACTION);
						return false;
					}
					if (!validateAccountIDUnearned(dtdObject.getValue(i, 7))) {
						getErrorMap().setError("gridError", BackOfficeErrorCodes.INVALID_ACTION);
						return false;
					}
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("gridError", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean checkDuplicate(DTDObject dtdObject, int index) {
		ArrayList<String> list = new ArrayList<String>();
		for (int i = 0; i < dtdObject.getRowCount(); i++) {
			String value = dtdObject.getValue(i, index);
			if (!value.isEmpty()) {
				if (list.contains(value)) {
					return false;
				}
				list.add(value);
			}
		}
		return true;
	}
}