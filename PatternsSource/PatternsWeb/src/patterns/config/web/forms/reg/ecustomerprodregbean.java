/*
 *
 Author : Jayashree P
 Created Date : 08-04-2017
 Spec Reference : ECUSTOMERPRODREG.HLD
 
 Modification History
 ----------------------------------------------------------------------
 Sl.No		Modified Date	Author			Modified Changes	Version
 ----------------------------------------------------------------------

 */

package patterns.config.web.forms.reg;

import java.util.ArrayList;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.service.DTDObject;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.CommonValidator;
import patterns.config.validations.MasterValidator;
import patterns.config.validations.RegistrationValidator;
import patterns.config.web.forms.GenericFormBean;

public class ecustomerprodregbean extends GenericFormBean {

	private static final long serialVersionUID = 1L;
	private String customerID;
	private String customerName;
	private String dateOfReg;
	private String product;
	private String debtorAccount;
	private String accountIDPrincipal;
	private String accountIDInterest;
	private String portfolioCode;
	private String remarks;
	private String xmlproductDetailGrid;
	private String accountIDUnearned;
	private String entrySl;

	public String getCustomerID() {
		return customerID;
	}

	public void setCustomerID(String customerID) {
		this.customerID = customerID;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getDateOfReg() {
		return dateOfReg;
	}

	public void setDateOfReg(String dateOfReg) {
		this.dateOfReg = dateOfReg;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getDebtorAccount() {
		return debtorAccount;
	}

	public void setDebtorAccount(String debtorAccount) {
		this.debtorAccount = debtorAccount;
	}

	public String getAccountIDPrincipal() {
		return accountIDPrincipal;
	}

	public void setAccountIDPrincipal(String accountIDPrincipal) {
		this.accountIDPrincipal = accountIDPrincipal;
	}

	public String getAccountIDInterest() {
		return accountIDInterest;
	}

	public void setAccountIDInterest(String accountIDInterest) {
		this.accountIDInterest = accountIDInterest;
	}

	public String getPortfolioCode() {
		return portfolioCode;
	}

	public void setPortfolioCode(String portfolioCode) {
		this.portfolioCode = portfolioCode;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getXmlproductDetailGrid() {
		return xmlproductDetailGrid;
	}

	public void setXmlproductDetailGrid(String xmlproductDetailGrid) {
		this.xmlproductDetailGrid = xmlproductDetailGrid;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getAccountIDUnearned() {
		return accountIDUnearned;
	}

	public void setAccountIDUnearned(String accountIDUnearned) {
		this.accountIDUnearned = accountIDUnearned;
	}

	public String getEntrySl() {
		return entrySl;
	}

	public void setEntrySl(String entrySl) {
		this.entrySl = entrySl;
	}

	public void reset() {
		customerID = RegularConstants.EMPTY_STRING;
		customerName = RegularConstants.EMPTY_STRING;
		dateOfReg = RegularConstants.EMPTY_STRING;
		product = RegularConstants.EMPTY_STRING;
		debtorAccount = RegularConstants.EMPTY_STRING;
		accountIDPrincipal = RegularConstants.EMPTY_STRING;
		accountIDInterest = RegularConstants.EMPTY_STRING;
		portfolioCode = RegularConstants.EMPTY_STRING;
		remarks = RegularConstants.EMPTY_STRING;
		accountIDUnearned = RegularConstants.EMPTY_STRING;
		xmlproductDetailGrid = RegularConstants.EMPTY_STRING;
		entrySl = RegularConstants.EMPTY_STRING;
		setProcessBO("patterns.config.framework.bo.reg.ecustomerprodregBO");
	}

	public void validate() {
		if (validateCustomerID() && validateEntrySerial()) {

			validateProductDetailsGrid();
			validateRemarks();
		}
		try {
			if (getErrorMap().length() == 0) {
				DTObject formDTO = getFormDTO();
				formDTO.set("ENTITY_CODE", context.getEntityCode());
				formDTO.set("CUSTOMER_ID", customerID);
				formDTO.set("CUSTOMER_NAME", customerName);
				formDTO.set("PROD_ADDED_ON", dateOfReg);
				formDTO.set("REMARKS", remarks);
				DTDObject dtdObject = new DTDObject();
				dtdObject.addColumn(0, "SL");
				dtdObject.addColumn(1, "ACTION");
				dtdObject.addColumn(2, "LEASE_PRODUCT_CODE");
				dtdObject.addColumn(3, "PORTFOLIO_CODE");
				dtdObject.addColumn(4, "SUNDRY_DB_AC");
				dtdObject.addColumn(5, "PRINCIPAL_AC");
				dtdObject.addColumn(6, "INTEREST_AC");
				dtdObject.addColumn(7, "UNEARNED_INTEREST_AC");
				dtdObject.addColumn(8, "LEASE_TYPE");
				dtdObject.setXML(xmlproductDetailGrid);
				formDTO.setDTDObject("CUSTOMERPRODREGACDTL", dtdObject);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("customerID", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
	}

	public boolean validateCustomerID() {
		try {
			DTObject formDTO = new DTObject();
			formDTO.set("CUSTOMER_ID", customerID);
			formDTO.set(ContentManager.ACTION, getAction());
			formDTO = validateCustomerID(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("customerID", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("customerID", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateCustomerID(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		RegistrationValidator validation = new RegistrationValidator();
		String customerId = inputDTO.get("CUSTOMER_ID");
		try {
			if (validation.isEmpty(customerId)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			if (!validation.isValidNumber(customerId)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.NUMERIC_CHECK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "CUSTOMER_NAME,TO_CHAR(DATE_OF_REG,'" + context.getDateFormat() + "') DATE_OF_REG,E_STATUS");
			resultDTO = validation.valildateCustomerID(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
			if (resultDTO.get("E_STATUS") == null || !("A".equals(resultDTO.get("E_STATUS")))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.CUSTOMER_NOT_AUTHORISED);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateEntrySerial() {
		try {
			if (getAction().equals(MODIFY)) {
				getErrorMap().setError("poSerial", BackOfficeErrorCodes.INVALID_ACTION);
				return false;
			}
			if (getRectify().equals(RegularConstants.COLUMN_ENABLE)) {
				DTObject formDTO = new DTObject();
				formDTO.set("CUSTOMER_ID", customerID);
				formDTO.set("ENTRY_SL", entrySl);
				formDTO.set(ContentManager.ACTION, getAction());
				formDTO = validateEntrySerial(formDTO);
				if (formDTO.get(ContentManager.ERROR) != null) {
					getErrorMap().setError("poSerial", formDTO.get(ContentManager.ERROR));
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("poSerial", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateEntrySerial(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		CommonValidator validation = new CommonValidator();
		String entrySl = inputDTO.get("ENTRY_SL");
		try {
			if (validation.isEmpty(entrySl)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			if (!validation.isValidNumber(entrySl)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_NUMBER);
				return resultDTO;
			}
			inputDTO.set(ContentManager.TABLE, "CUSTOMERPRODREG");
			inputDTO.set(ContentManager.FETCH_COLUMNS, "E_STATUS");
			String columns[] = new String[] { context.getEntityCode(), inputDTO.get("CUSTOMER_ID"), entrySl };
			inputDTO.setObject(ContentManager.PROGRAM_KEY, columns);
			resultDTO = validation.validatePK(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (ContentManager.DATA_UNAVAILABLE.equals(resultDTO.get(ContentManager.RESULT))) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.REFERENCE_DOES_NOT_EXIST);
				return resultDTO;
			}

			if (!resultDTO.getObject(ContentManager.E_STATUS).equals(RegularConstants.EMPTY_STRING)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.RECORD_ALREADY_AUTORIZED);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateProductCode(String product) {
		try {
			DTObject formDTO = getFormDTO();
			formDTO.set("LEASE_PRODUCT_CODE", product);
			formDTO.set(ContentManager.USER_ACTION, USAGE);
			formDTO = validateProductCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("product", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("product", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validateProductCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		MasterValidator validation = new MasterValidator();
		String productCode = inputDTO.get("LEASE_PRODUCT_CODE");
		try {
			if (validation.isEmpty(productCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION,LEASE_TYPE");
			resultDTO = validation.validateLeaseProductCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validatePortfolioCode(String portfolioCode) {
		try {
			DTObject formDTO = getFormDTO();
			formDTO.set("PORTFOLIO_CODE", portfolioCode);
			formDTO.set(ContentManager.USER_ACTION, USAGE);
			formDTO = validatePortfolioCode(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("portfolioCode", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("portfolioCode", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		}
		return false;
	}

	public DTObject validatePortfolioCode(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		MasterValidator validation = new MasterValidator();
		String portfolioCode = inputDTO.get("PORTFOLIO_CODE");
		try {
			if (validation.isEmpty(portfolioCode)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.FIELD_BLANK);
				return resultDTO;
			}
			inputDTO.set(ContentManager.FETCH_COLUMNS, "DESCRIPTION");
			resultDTO = validation.validatePortFolioCode(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
			if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.INVALID_CODE);
				return resultDTO;
			}

		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	public boolean validateDebtorAccount(String debtorAccount) {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(debtorAccount)) {
				getErrorMap().setError("debtorAccount", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidOtherInformation25(debtorAccount)) {
				getErrorMap().setError("debtorAccount", BackOfficeErrorCodes.LENGTH_10);
				return false;
			}
			DTObject formDTO = new DTObject();
			formDTO.set("PROGRAM_ID", context.getProcessID());
			formDTO.set("CUSTOMER_ID", customerID);
			formDTO.set("SUNDRY_DB_AC", debtorAccount);
			formDTO.set("ENTRY_SL", entrySl);
			formDTO.set("ACTION", USAGE);
			formDTO = validateDebtorAccount(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("debtorAccount", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("debtorAccount", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public DTObject validateDebtorAccount(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		MasterValidator validation = new MasterValidator();
		try {
			resultDTO = isDebtorAccountExist(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	private DTObject isDebtorAccountExist(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		CommonValidator validator = new CommonValidator();
		try {
			String primaryKey = context.getEntityCode() + RegularConstants.PK_SEPARATOR + inputDTO.get("CUSTOMER_ID") + RegularConstants.PK_SEPARATOR + inputDTO.get("ENTRY_SL");
			String dedupKey = context.getEntityCode() + RegularConstants.PK_SEPARATOR + inputDTO.get("SUNDRY_DB_AC");
			DTObject formDTO = new DTObject();
			formDTO.set(ContentManager.PROGRAM_ID, inputDTO.get("PROGRAM_ID"));
			formDTO.set(ContentManager.PURPOSE_ID, "SUNDRY_DB_ACCOUNT");
			formDTO.set(ContentManager.PROGRAM_KEY, primaryKey);
			formDTO.set(ContentManager.DEDUP_KEY, dedupKey);
			resultDTO = validator.isDeDupAlreadyExist(formDTO);
			if (resultDTO.get(ContentManager.ERROR) != null)
				return resultDTO;
			else if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.DEBTOR_ACCOUNT_LINKED);
				Object[] param = { resultDTO.get("LINKED_TO_PK") };
				resultDTO.setObject(ContentManager.ERROR_PARAM, param);
				return resultDTO;
			}
			formDTO.set(ContentManager.PROGRAM_ID, "ECUSTOMERREG");
			resultDTO = validator.isDeDupAlreadyExist(formDTO);
			if (resultDTO.get(ContentManager.ERROR) != null)
				return resultDTO;
			else if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.DEBTOR_ACCOUNT_LINKED);
				Object[] param = { resultDTO.get("LINKED_TO_PK") };
				resultDTO.setObject(ContentManager.ERROR_PARAM, param);
				return resultDTO;
			}

		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return resultDTO;
	}

	public boolean validateAccountIDPrincipal(String accountIDPrincipal) {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(accountIDPrincipal)) {
				getErrorMap().setError("accountIDPrincipal", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidOtherInformation25(accountIDPrincipal)) {
				getErrorMap().setError("accountIDPrincipal", BackOfficeErrorCodes.LENGTH_10);
				return false;
			}
			DTObject formDTO = new DTObject();
			formDTO.set("PROGRAM_ID", context.getProcessID());
			formDTO.set("CUSTOMER_ID", customerID);
			formDTO.set("PRINCIPAL_AC", accountIDPrincipal);
			formDTO.set("ENTRY_SL", entrySl);
			formDTO.set("ACTION", USAGE);
			formDTO = validatePrincipalAccount(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("accountIDPrincipal", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("accountIDPrincipal", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public DTObject validatePrincipalAccount(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		MasterValidator validation = new MasterValidator();
		try {
			resultDTO = isPrincipalAccountExist(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	private DTObject isPrincipalAccountExist(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		CommonValidator validator = new CommonValidator();
		try {
			String primaryKey = context.getEntityCode() + RegularConstants.PK_SEPARATOR + inputDTO.get("CUSTOMER_ID") + RegularConstants.PK_SEPARATOR + inputDTO.get("ENTRY_SL");
			String dedupKey = context.getEntityCode() + RegularConstants.PK_SEPARATOR + inputDTO.get("PRINCIPAL_AC");
			DTObject formDTO = new DTObject();
			formDTO.set(ContentManager.PROGRAM_ID, inputDTO.get("PROGRAM_ID"));
			formDTO.set(ContentManager.PURPOSE_ID, "PRINCIPAL_ACCOUNT");
			formDTO.set(ContentManager.PROGRAM_KEY, primaryKey);
			formDTO.set(ContentManager.DEDUP_KEY, dedupKey);
			resultDTO = validator.isDeDupAlreadyExist(formDTO);
			if (resultDTO.get(ContentManager.ERROR) != null)
				return resultDTO;
			else if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.ACCOUNT_ID_LINKED);
				String customerID = resultDTO.get("LINKED_TO_PK").substring(2, resultDTO.get("LINKED_TO_PK").length());
				Object[] param = { customerID };
				resultDTO.setObject(ContentManager.ERROR_PARAM, param);
				return resultDTO;
			}
			formDTO.set(ContentManager.PROGRAM_ID, "ECUSTOMERREG");
			resultDTO = validator.isDeDupAlreadyExist(formDTO);
			if (resultDTO.get(ContentManager.ERROR) != null)
				return resultDTO;
			else if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.ACCOUNT_ID_LINKED);
				Object[] param = { resultDTO.get("LINKED_TO_PK") };
				resultDTO.setObject(ContentManager.ERROR_PARAM, param);
				return resultDTO;
			}

		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return resultDTO;
	}

	public boolean validateAccountIDInterest(String accountIDInterest) {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(accountIDInterest)) {
				getErrorMap().setError("debtorAccount", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidOtherInformation25(accountIDInterest)) {
				getErrorMap().setError("accountIDInterest", BackOfficeErrorCodes.LENGTH_10);
				return false;
			}
			DTObject formDTO = new DTObject();
			formDTO.set("PROGRAM_ID", context.getProcessID());
			formDTO.set("CUSTOMER_ID", customerID);
			formDTO.set("ENTRY_SL", entrySl);
			formDTO.set("INTEREST_AC", accountIDInterest);
			formDTO.set("ACTION", USAGE);
			formDTO = validateAccountIDInterest(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("accountIDInterest", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("accountIDInterest", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public DTObject validateAccountIDInterest(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		MasterValidator validation = new MasterValidator();
		try {
			resultDTO = isAccountIDInterestExist(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	private DTObject isAccountIDInterestExist(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		CommonValidator validator = new CommonValidator();
		try {
			String primaryKey = context.getEntityCode() + RegularConstants.PK_SEPARATOR + inputDTO.get("CUSTOMER_ID") + RegularConstants.PK_SEPARATOR + inputDTO.get("ENTRY_SL");
			String dedupKey = context.getEntityCode() + RegularConstants.PK_SEPARATOR + inputDTO.get("INTEREST_AC");
			DTObject formDTO = new DTObject();
			formDTO.set(ContentManager.PROGRAM_ID, inputDTO.get("PROGRAM_ID"));
			formDTO.set(ContentManager.PURPOSE_ID, "INTEREST_ACCOUNT");
			formDTO.set(ContentManager.PROGRAM_KEY, primaryKey);
			formDTO.set(ContentManager.DEDUP_KEY, dedupKey);
			resultDTO = validator.isDeDupAlreadyExist(formDTO);
			if (resultDTO.get(ContentManager.ERROR) != null)
				return resultDTO;
			else if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.ACCOUNT_ID_LINKED);
				Object[] param = { resultDTO.get("LINKED_TO_PK") };
				resultDTO.setObject(ContentManager.ERROR_PARAM, param);
				return resultDTO;
			}
			formDTO.set(ContentManager.PROGRAM_ID, "ECUSTOMERREG");
			resultDTO = validator.isDeDupAlreadyExist(formDTO);
			if (resultDTO.get(ContentManager.ERROR) != null)
				return resultDTO;
			else if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.ACCOUNT_ID_LINKED);
				Object[] param = { resultDTO.get("LINKED_TO_PK") };
				resultDTO.setObject(ContentManager.ERROR_PARAM, param);
				return resultDTO;
			}

		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return resultDTO;
	}

	public boolean validateAccountIDUnearned(String accountIDUnearned) {
		CommonValidator validation = new CommonValidator();
		try {
			if (validation.isEmpty(accountIDUnearned)) {
				getErrorMap().setError("accountIDUnearned", BackOfficeErrorCodes.FIELD_BLANK);
				return false;
			}
			if (!validation.isValidOtherInformation25(accountIDUnearned)) {
				getErrorMap().setError("accountIDUnearned", BackOfficeErrorCodes.LENGTH_10);
				return false;
			}
			DTObject formDTO = new DTObject();
			formDTO.set("PROGRAM_ID", context.getProcessID());
			formDTO.set("CUSTOMER_ID", customerID);
			formDTO.set("ENTRY_SL", entrySl);
			formDTO.set("UNEARNED_INTEREST_AC", accountIDUnearned);
			formDTO.set("ACTION", USAGE);
			formDTO = validateUnearnedInterestAccount(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getErrorMap().setError("accountIDUnearned", formDTO.get(ContentManager.ERROR));
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("accountIDUnearned", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public DTObject validateUnearnedInterestAccount(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		MasterValidator validation = new MasterValidator();
		try {
			resultDTO = isUnearnedInterestAccountExist(inputDTO);
			if (resultDTO.get(ContentManager.ERROR) != null) {
				resultDTO.set(ContentManager.ERROR, resultDTO.get(ContentManager.ERROR));
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return resultDTO;
	}

	private DTObject isUnearnedInterestAccountExist(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		CommonValidator validator = new CommonValidator();
		try {
			String primaryKey = context.getEntityCode() + RegularConstants.PK_SEPARATOR + inputDTO.get("CUSTOMER_ID") + RegularConstants.PK_SEPARATOR + inputDTO.get("ENTRY_SL");
			String dedupKey = context.getEntityCode() + RegularConstants.PK_SEPARATOR + inputDTO.get("UNEARNED_INTEREST_AC");
			DTObject formDTO = new DTObject();
			formDTO.set(ContentManager.PROGRAM_ID, inputDTO.get("PROGRAM_ID"));
			formDTO.set(ContentManager.PURPOSE_ID, "UNEARNED_INTEREST_ACCOUNT");
			formDTO.set(ContentManager.PROGRAM_KEY, primaryKey);
			formDTO.set(ContentManager.DEDUP_KEY, dedupKey);
			resultDTO = validator.isDeDupAlreadyExist(formDTO);
			if (resultDTO.get(ContentManager.ERROR) != null)
				return resultDTO;
			else if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.ACCOUNT_ID_LINKED);
				Object[] param = { resultDTO.get("LINKED_TO_PK") };
				resultDTO.setObject(ContentManager.ERROR_PARAM, param);
				return resultDTO;
			}
			formDTO.set(ContentManager.PROGRAM_ID, "ECUSTOMERREG");
			resultDTO = validator.isDeDupAlreadyExist(formDTO);
			if (resultDTO.get(ContentManager.ERROR) != null)
				return resultDTO;
			else if (resultDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
				resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.ACCOUNT_ID_LINKED);
				Object[] param = { resultDTO.get("LINKED_TO_PK") };
				resultDTO.setObject(ContentManager.ERROR_PARAM, param);
				return resultDTO;
			}

		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validator.close();
		}
		return resultDTO;
	}

	private boolean validateProductDetailsGrid() {
		CommonValidator validation = new CommonValidator();
		try {
			DTDObject dtdObject = new DTDObject();
			dtdObject.addColumn(0, "SL");
			dtdObject.addColumn(1, "ACTION");
			dtdObject.addColumn(2, "LEASE_PRODUCT_CODE");
			dtdObject.addColumn(3, "PORTFOLIO_CODE");
			dtdObject.addColumn(4, "SUNDRY_DB_AC");
			dtdObject.addColumn(5, "PRINCIPAL_AC");
			dtdObject.addColumn(6, "INTEREST_AC");
			dtdObject.addColumn(7, "UNEARNED_INTEREST_AC");
			dtdObject.addColumn(8, "LEASE_TYPE");
			dtdObject.setXML(xmlproductDetailGrid);
			if (dtdObject.getRowCount() <= 0) {
				getErrorMap().setError("gridError", BackOfficeErrorCodes.ATLEAST_ONE_ROW);
				return false;
			}
			if (!validation.checkDuplicateRows(dtdObject, 4)) {
				getErrorMap().setError("gridError", BackOfficeErrorCodes.DUPLICATE_DATA);
				return false;
			}
			if (!validation.checkDuplicateRows(dtdObject, 5)) {
				getErrorMap().setError("gridError", BackOfficeErrorCodes.DUPLICATE_DATA);
				return false;
			}
			if (!checkDuplicate(dtdObject, 6)) {
				getErrorMap().setError("gridError", BackOfficeErrorCodes.DUPLICATE_DATA);
				return false;
			}
			if (!checkDuplicate(dtdObject, 7)) {
				getErrorMap().setError("gridError", BackOfficeErrorCodes.DUPLICATE_DATA);
				return false;
			}
			for (int i = 0; i < dtdObject.getRowCount(); i++) {
				if (!validateProductCode(dtdObject.getValue(i, 2))) {
					getErrorMap().setError("gridError", BackOfficeErrorCodes.INVALID_ACTION);
					return false;
				}
				if (!validatePortfolioCode(dtdObject.getValue(i, 3))) {
					getErrorMap().setError("gridError", BackOfficeErrorCodes.INVALID_ACTION);
					return false;
				}
				if (!validateDebtorAccount(dtdObject.getValue(i, 4))) {
					getErrorMap().setError("gridError", BackOfficeErrorCodes.INVALID_ACTION);
					return false;
				}
				if (!dtdObject.getValue(i, 8).equals("O")) {
					if (!validateAccountIDPrincipal(dtdObject.getValue(i, 5))) {
						getErrorMap().setError("gridError", BackOfficeErrorCodes.INVALID_ACTION);
						return false;
					}
					if (!validateAccountIDInterest(dtdObject.getValue(i, 6))) {
						getErrorMap().setError("gridError", BackOfficeErrorCodes.INVALID_ACTION);
						return false;
					}
					if (!validateAccountIDUnearned(dtdObject.getValue(i, 7))) {
						getErrorMap().setError("gridError", BackOfficeErrorCodes.INVALID_ACTION);
						return false;
					}
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("gridError", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			validation.close();
		}
		return false;
	}

	public boolean checkDuplicate(DTDObject dtdObject, int index) {
		ArrayList<String> list = new ArrayList<String>();
		for (int i = 0; i < dtdObject.getRowCount(); i++) {
			String value = dtdObject.getValue(i, index);
			if (!value.isEmpty()) {
				if (list.contains(value)) {
					return false;
				}
				list.add(value);
			}
		}
		return true;
	}

	public boolean validateRemarks() {
		CommonValidator commonValidator = new CommonValidator();
		try {
			if (getAction().equals(ADD) && remarks != null && remarks.length() > 0) {
				if (commonValidator.isEmpty(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			} else if (getAction().equals(MODIFY)) {
				if (commonValidator.isEmpty(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.FIELD_BLANK);
					return false;
				}
				if (!commonValidator.isValidRemarks(remarks)) {
					getErrorMap().setError("remarks", BackOfficeErrorCodes.INVALID_REMARKS);
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getErrorMap().setError("remarks", BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			commonValidator.close();
		}
		return false;
	}
}