package patterns.config.web.forms;

public abstract class FileFormBean extends GenericFormBean {

	private static final long serialVersionUID = -8723690826057218556L;
	private String fileInventoryNumber;
	private String fileName;
	private String fileExtensionList;
	private String fileDescn;
	private String fileAltText;

	public String getFileExtensionList() {
		return fileExtensionList;
	}

	public String getFileDescn() {
		return fileDescn;
	}

	public void setFileDescn(String fileDescn) {
		this.fileDescn = fileDescn;
	}

	public String getFileAltText() {
		return fileAltText;
	}

	public void setFileAltText(String fileAltText) {
		this.fileAltText = fileAltText;
	}

	public void setFileExtensionList(String fileExtensionList) {
		this.fileExtensionList = fileExtensionList;
	}

	public String getFileInventoryNumber() {
		return fileInventoryNumber;
	}

	public void setFileInventoryNumber(String fileInventoryNumber) {
		this.fileInventoryNumber = fileInventoryNumber;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

}