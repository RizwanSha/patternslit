package patterns.config.web.utils;

public class HTMLMenuUtility {
	StringBuffer buffer = null;

	public HTMLMenuUtility() {
		buffer = new StringBuffer(2048);
	}

	public void init() {
		buffer.append("<ul class='first-of-type'>");
	}

	public void finish() {
		buffer.append("</ul>");
	}

	public void startHorizontalCategory(int parentCounter, String id, String align, String text) {
		buffer.append("<li id='A").append(parentCounter).append("_li' class='yuimenubaritem first-of-type yuimenubaritem-hassubmenu'>");
		buffer.append("<a class='yuimenubaritemlabel yuimenubaritemlabel-hassubmenu' id='A").append(parentCounter).append("' onmouseover=\"menuLoader.loadMenuGrid(this,'").append(id).append("','").append(align).append("')\" onmouseout='menuLoader.setRootDelay(this.id)'>");
		buffer.append(text);
		buffer.append("</a>");
		buffer.append("<div id='A" + parentCounter + "_licg' >");
	}

	public void endHorizontalCategory() {
		buffer.append("</div>");
		buffer.append("</li>");
	}

	public void startParent(String text, String align) {
		buffer.append("<div class='yuimenu yui-module yui-overlay yui-overlay-hidden'");
		buffer.append("id='" + text + "_cg'>");
		buffer.append("<div class='bd'><ul class='first-of-type'>");
	}

	public void endParent() {

		buffer.append("</ul>");
		buffer.append("</div>");
		buffer.append("</div>");
	}

	public void startChild(String id, String text, String parent, String align) {
		if (align.equals("L"))
			buffer.append("<li class='yuimenuitemleft first-of-type' id='" + id + "_li' groupindex='0' index='0'><a id='" + id + "' href='javascript:void(0);' onmouseover='menuLoader.selectInnerChild(this)' onmouseout='menuLoader.removeInnerChild(this)' class='yuimenuitemlabelleft'>" + text + "</a>");
		else
			buffer.append("<li class='yuimenuitem first-of-type' id='" + id + "_li' groupindex='0' index='0'><a id='" + id + "' href='javascript:void(0);' onmouseover='menuLoader.selectInnerChild(this)' onmouseout='menuLoader.removeInnerChild(this)' class='yuimenuitemlabel'>" + text + "</a>");

	}

	public void startInnerChild(String id, String text, String align) {
		if (align.equals("L"))
			buffer.append("<li id='" + id + "_li' class='yuimenuitemleft yuimenuitemleft-hassubmenu'  groupindex='0' index='0'><a id=" + id + " href='javascript:void(0);' onmouseover='menuLoader.selectInnerParent(this)' onmouseout='menuLoader.delayChange(this)'  class='yuimenuitemlabelleft yuimenuitemlabelleft-hassubmenu'>" + text + "</a><div id='" + id + "_licg'>");
		else
			buffer.append("<li id='" + id + "_li' class='yuimenuitem yuimenuitem-hassubmenu'  groupindex='0' index='0'><a id=" + id + " href='javascript:void(0);' onmouseover='menuLoader.selectInnerParent(this)' onmouseout='menuLoader.delayChange(this)'  class='yuimenuitemlabel yuimenuitemlabel-hassubmenu'>" + text + "</a><div id='" + id + "_licg'>");
	}

	public void endInnerChild() {
		buffer.append("</div>");
		buffer.append("</li>");
	}

	public void endChild() {
		buffer.append("</li>");
	}

	public String getHTML() {
		buffer.trimToSize();
		return buffer.toString();
	}
}
