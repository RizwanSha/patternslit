package patterns.config.web.utils;

import java.sql.ResultSet;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.AccessValidator;
import patterns.config.web.forms.GenericFormBean;

public class formScenarioExtractor extends GenericFormBean {

	private static final long serialVersionUID = -6219657363150466160L;

	private String scenario;

	public void setScenario(String scenario) {
		this.scenario = scenario;
	}

	public String getScenario() {
		return scenario;
	}

	public void reset() {
		// TODO Auto-generated method stub

	}

	@Override
	public void validate() {
		// TODO Auto-generated method stub

	}

	public DTObject getScenarioData(DTObject inputDTO) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, RegularConstants.NULL);
		AccessValidator validation = new AccessValidator();
		String scenarioCode = inputDTO.get("SCENARIO_CODE");
		String programId = inputDTO.get("PROGRAM_ID");
		DBUtil util = validation.getDbContext().createUtilInstance();
		String sqlQuery = RegularConstants.EMPTY_STRING;

		try {
			if (!validation.isEmpty(scenarioCode)) {
				sqlQuery = "SELECT I.FILE_DATA SCENARIO_FILE_DATA FROM CMNFILEINVENTORY I JOIN FORMSCENARIO F ON(I.ENTITY_CODE=F.ENTITY_CODE AND I.FILE_INV_NUM=F.SCENARIO_INV_NUM) WHERE F.ENTITY_CODE=? AND F.SCENARIO_CODE=? AND F.PROGRAM_ID=?";
				util.reset();
				util.setSql(sqlQuery);
				util.setLong(1, Long.parseLong(context.getEntityCode()));
				util.setString(2, scenarioCode);
				util.setString(3, programId);
				ResultSet rset = util.executeQuery();
				if (rset.next()) {
					resultDTO.set("SCENARIO_FILE_DATA", rset.getString("SCENARIO_FILE_DATA"));
				}
				sqlQuery = RegularConstants.EMPTY_STRING;
				sqlQuery = "SELECT I.FILE_DATA MODEL_FILE_DATA FROM CMNFILEINVENTORY I JOIN FORMMODEL M ON(I.FILE_INV_NUM=M.MODEL_INV_NUM) WHERE I.ENTITY_CODE=? AND M.PROGRAM_ID=?";
				util.reset();
				util.setSql(sqlQuery);
				util.setLong(1, Long.parseLong(context.getEntityCode()));
				util.setString(2, programId);
				ResultSet rset1 = util.executeQuery();
				if (rset1.next()) {
					resultDTO.set("MODEL_FILE_DATA", rset1.getString("MODEL_FILE_DATA"));
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			util.reset();
			validation.close();
		}
		return resultDTO;
	}

}
