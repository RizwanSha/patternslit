package patterns.config.framework.monitor;

import java.util.Enumeration;

import javax.servlet.ServletContext;

import patterns.config.framework.loggers.ApplicationLogger;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.configuration.lookup.LookupMap;
import patterns.config.framework.web.configuration.program.ProgramMap;
import patterns.config.framework.web.configuration.record.RecordFinderMap;
import patterns.config.framework.web.filters.LoadBalancerConfiguration;
import patterns.config.validations.BaseDomainValidator;

import com.patterns.jobs.application.monitor.ApplicationValidator;
import com.patterns.jobs.bean.loader.JobLoader;
import com.patterns.jobs.bean.loader.TemplateLoader;
import com.patterns.jobs.quartz.QuartzManager;

public class ApplicationWatch {

	private ApplicationLogger logger = null;

	private static volatile ApplicationWatch instance = null;

	private ApplicationWatch() {
		logger = ApplicationLogger.getInstance(this.getClass().getSimpleName());
		logger.logInfo("#");
	}

	public static ApplicationWatch getInstance() {
		if (instance == null) {
			instance = new ApplicationWatch();
		}
		return instance;
	}

	public void start(ServletContext context) {
		logger.logInfo("start()");
		ContextReference.setContext(context);
		BaseDomainValidator validator = new BaseDomainValidator();
		try {
			LoadBalancerConfiguration loadBalancerConfiguration = validator.getLoadBalancerConfiguration();
			LoadBalancerConfigurationReference.setConfiguration(loadBalancerConfiguration);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			validator.close();
		}
		RecordFinderMap.reloadConfiguration();
		LookupMap.reloadConfiguration();
		ProgramMap.reloadConfiguration();
		//06-03-2017
		TemplateLoader.getInstance().reloadConfiguration();

		JobApplicationContext.getInstance().setContext(getInitParameters(context));

		JobLoader.getInstance().reloadConfiguration();
		QuartzManager.getInstance().startScheduler();
		QuartzManager.getInstance().startJobManager();

	}

	public void stop() {
		logger.logInfo("stop()");
		RecordFinderMap.unloadConfiguration();
		LookupMap.unloadConfiguration();
		ProgramMap.unloadConfiguration();
//		TemplateLoader.getInstance().unloadConfiguration();
		ServletContext context = ContextReference.getContext();

		JobLoader.getInstance().unloadConfiguration();
		QuartzManager.getInstance().stopJobManager();
		QuartzManager.getInstance().stopScheduler();
		
		ApplicationValidator validator = new ApplicationValidator();
		try {
			validator.releaseWorkers();
		} catch (Exception e) {

		} finally {
			validator.close();
		}
	}

	private DTObject getInitParameters(ServletContext context) {
		logger.logDebug("getInitParameters() Begin");
		DTObject valueBean = new DTObject();
		Enumeration<String> initParameters = context.getInitParameterNames();
		while (initParameters.hasMoreElements()) {
			String key = initParameters.nextElement();
			String value = context.getInitParameter(key);
			valueBean.set(key, value);
		}
		logger.logDebug("getInitParameters() End");
		return valueBean;
	}
}