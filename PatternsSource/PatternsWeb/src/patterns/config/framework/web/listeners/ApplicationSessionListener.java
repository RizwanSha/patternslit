package patterns.config.framework.web.listeners;

import java.util.Date;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import patterns.config.delegate.BackOfficeProcessManager;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.loggers.ApplicationLogger;
import patterns.config.framework.process.TBAProcessResult;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.SessionConstants;

public class ApplicationSessionListener implements HttpSessionListener, HttpSessionAttributeListener {
	private ApplicationLogger logger = null;

	public ApplicationSessionListener() {
		logger = ApplicationLogger.getInstance(this.getClass().getSimpleName());
		// logger.logDebug("#()");
	}

	public void sessionCreated(HttpSessionEvent se) {
		logger.logDebug("sessionCreated() : " + se.getSession().getId());
	}

	public void sessionDestroyed(HttpSessionEvent se) {
		logger.logDebug("sessionDestroyed() : " + se.getSession().getId());
		logger.logInfo("sessionDestroyed " + se.getSession().getId());
		HttpSession session = se.getSession();
		String entityCode = (String) session.getAttribute(SessionConstants.ENTITY_CODE);
		logger.logDebug("sessionDestroyed() : ENTITY CODE : " + entityCode);
		String userID = (String) session.getAttribute(SessionConstants.USER_ID);
		logger.logDebug("sessionDestroyed() : USER ID : " + entityCode);
		Date loginDateTime = (Date) session.getAttribute(SessionConstants.LOGIN_DATE_TIME);
		logger.logDebug("sessionDestroyed() : LOGIN DATE TIME : " + loginDateTime);

		if (userID != null && !userID.trim().equals(RegularConstants.EMPTY_STRING)) {
			String logoutReason = null;
			logoutReason = RegularConstants.AUTO_LOGOUT;
			DTObject formDTO = new DTObject();
			formDTO.set(RegularConstants.PROCESSBO_CLASS, "patterns.config.framework.bo.common.elogoutBO");
			formDTO.set("P_ENTITY_CODE", entityCode);
			formDTO.set("P_USER_ID", userID);
			formDTO.setObject("P_LOGIN_DATE", loginDateTime);
			formDTO.set("P_REASON", logoutReason);
			formDTO.set("P_SESSION_ID", session.getId());
			BackOfficeProcessManager processManager = new BackOfficeProcessManager();
			TBAProcessResult processResult = processManager.delegate(formDTO);
			if (processResult.getProcessStatus().equals(TBAProcessStatus.FAILURE)) {
				logger.logInfo("Logout Failure");
			} else {
				logger.logInfo("Logout Success");
			}
		}
	}

	public void attributeAdded(HttpSessionBindingEvent event) {
		// logger.logDebug("attributeAdded() : " + event.getName() + " : " + event.getValue());
	}

	public void attributeRemoved(HttpSessionBindingEvent event) {
		// logger.logDebug("attributeRemoved() : " + event.getName() + " : " + event.getValue());
	}

	public void attributeReplaced(HttpSessionBindingEvent event) {
		logger.logDebug("attributeReplaced() : " + event.getName() + " : " + event.getValue());
	}
}
