package patterns.config.framework.web.listeners;

import java.util.Enumeration;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import patterns.config.framework.loggers.ApplicationLogger;
import patterns.config.framework.monitor.ApplicationWatch;
import patterns.config.framework.service.DTObject;

public class ApplicationContextListener implements ServletContextListener {
	ApplicationLogger logger = null;

	public ApplicationContextListener() {
		logger = ApplicationLogger.getInstance(this.getClass().getSimpleName());
		logger.logInfo("#()");
	}

	public void contextInitialized(ServletContextEvent sce) {
		logger.logInfo("#contextInitialized() Begin");
		DTObject initParameters = getInitParameters(sce.getServletContext());
		//ApplicationController.getInstance().start(initParameters);
		ApplicationWatch.getInstance().start(sce.getServletContext());
		logger.logInfo("#contextInitialized() End");
	}

	public void contextDestroyed(ServletContextEvent sce) {
		logger.logInfo("#contextDestroyed() Begin");
		ApplicationWatch.getInstance().stop();
		logger.logInfo("#contextDestroyed() End");
	}

	private DTObject getInitParameters(ServletContext context) {
		logger.logInfo("getInitParameters() Begin");
		DTObject valueBean = new DTObject();
		Enumeration<String> initParameters = context.getInitParameterNames();
		while (initParameters.hasMoreElements()) {
			String key = initParameters.nextElement();
			String value = context.getInitParameter(key);
			valueBean.set(key, value);
		}
		logger.logInfo("getInitParameters() End");
		return valueBean;
	}
}
