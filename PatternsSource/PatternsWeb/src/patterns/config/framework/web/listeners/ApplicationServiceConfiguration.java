/*
 * Copyright (c) 2014, 2015, Patterns Software Design Institute. All
 * rights reserved. Unauthorized copying of this file, via any medium is
 * strictly prohibited. PATTERNS SDI PROPRIETARY/CONFIDENTIAL. Use is
 * subject to license terms.
 */
package patterns.config.framework.web.listeners;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import com.patterns.mobile.services.pbs.AccountOpeningService;
import com.patterns.mobile.services.pbs.AccountStatementService;
import com.patterns.mobile.services.pbs.AccountSummaryService;
import com.patterns.mobile.services.pbs.CustomerRegistrationService;
import com.patterns.mobile.services.pbs.DocumentDispatchService;
import com.patterns.mobile.services.pbs.LoginService;
import com.patterns.mobile.services.pbs.LogoutService;
import com.patterns.mobile.services.pbs.LookupService;
import com.patterns.mobile.services.pbs.WalletDeliveryService;

/**
 * The Class ApplicationServiceConfiguration loads the list of restful web
 * services
 */
@ApplicationPath("/")
public class ApplicationServiceConfiguration extends Application {

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.ws.rs.core.Application#getClasses()
	 */
	@Override
	public Set<Class<?>> getClasses() {
		Set<Class<?>> serviceList = new HashSet<Class<?>>();
		serviceList.add(AccountSummaryService.class);
		serviceList.add(AccountStatementService.class);
		serviceList.add(WalletDeliveryService.class);
		serviceList.add(AccountOpeningService.class);
		serviceList.add(CustomerRegistrationService.class);
		serviceList.add(LoginService.class);
		serviceList.add(LogoutService.class);
		serviceList.add(DocumentDispatchService.class);
		serviceList.add(LookupService.class);
		return serviceList;
	}
}
