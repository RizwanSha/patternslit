package patterns.config.framework.web.configuration.data;

import java.lang.reflect.Method;

import patterns.config.framework.loggers.ApplicationLogger;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.AJAXContentManager;
import patterns.config.framework.web.ajax.ContentManager;

public class DataManager extends AJAXContentManager {

	private static final String REFLECTION_CLASS = "_ReflectionClass";
	private static final String REFLECTION_METHOD = "_ReflectionMethod";
	private ApplicationLogger logger = null;

	public DataManager() {
		super();
		logger = ApplicationLogger.getInstance(this.getClass().getSimpleName());
		// logger.logDebug("#()");
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public DTObject getData(DTObject input) {
		// logger.logDebug("getData()");
		DTObject result = new DTObject();
		result.set(ContentManager.ERROR, ContentManager.EMPTY_STRING);
		Class classObject = null;
		Object object = null;
		Method methodObject = null;
		Class[] parameterTypes = new Class[] { DTObject.class };
		Object[] params = new Object[] { input };
		try {
			classObject = Class.forName(input.get(REFLECTION_CLASS));
			object = classObject.newInstance();
			methodObject = classObject.getMethod(input.get(REFLECTION_METHOD), parameterTypes);
			result = (DTObject) methodObject.invoke(object, params);
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError("getData(1) : " + e.getLocalizedMessage() + " : " + input);
			result.set(ContentManager.ERROR, e.getLocalizedMessage());
		}
		if (!result.containsKey(ContentManager.ERROR) || result.get(ContentManager.ERROR) == null)
			result.set(ContentManager.ERROR, ContentManager.EMPTY_STRING);
		return result;
	}
}
