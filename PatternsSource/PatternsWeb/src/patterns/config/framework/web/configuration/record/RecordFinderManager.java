package patterns.config.framework.web.configuration.record;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.regex.Pattern;

import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.BindParameterType;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.loggers.ApplicationLogger;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.framework.web.DHTMLXGridUtility;
import patterns.config.framework.web.ajax.AJAXContentManager;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.framework.web.ajax.InterceptorPurpose;
import patterns.config.validations.CommonValidator;

public class RecordFinderManager extends AJAXContentManager {
	private String tokenID = EMPTY_STRING;
	private String programID = EMPTY_STRING;
	private String arguments = EMPTY_STRING;
	private String startPosition = EMPTY_STRING;
	private String buffer = EMPTY_STRING;
	private String IFDATE = "4";
	private String[] searchColumns = null;
	private String[] searchTexts = null;
	private String orderColumn = EMPTY_STRING;
	private String sortOrder = EMPTY_STRING;
	private boolean init = false;
	private RecordFinderConfiguration recordFinderConfiguration = null;
	private int positionStart = 0;
	private int positionEnd = 0;
	private String sqlQuery = EMPTY_STRING;
	private int rowCount = 0;
	boolean partitionEnabled = false;
	Object[] bucketList;
	ApplicationLogger logger = null;
	private static final String SELECT_CLAUSE = "#{SELECT_CLAUSE}#";
	private static final String SELECT_CLAUSE_X = "#{SELECT_CLAUSE_X}#";
	private static final String WHERE_CLAUSE = "#{WHERE_CLAUSE}#";
	private static final String WHERE_CLAUSE_X = "#{WHERE_CLAUSE_X}#";
	private static final String ORDER_CLAUSE = "#{ORDER_CLAUSE}#";
	private static final String ORDER_CLAUSE_X = "#{ORDER_CLAUSE_X}#";

	public RecordFinderManager() {
		super();
		logger = ApplicationLogger.getInstance(this.getClass().getSimpleName());
		// logger.logDebug("#()");
	}

	public DTObject getData(DTObject input) {
		// logger.logDebug("getData()");
		DTObject result = new DTObject();
		try {
			RecordFinderMap recordFinderMap = RecordFinderMap.getInstance();
			result.set(RESULT, DATA_UNAVAILABLE);
			programID = input.get(PROGRAM_KEY);
			if (programID == null) {
				logger.logError("RECORD_FINDER_PROGRAM_ID not specified");
				return result;
			} else {
				programID = programID.trim().toUpperCase(getContext().getLocale());
				logger.logDebug("setting RECORD_FINDER_PROGRAM_ID : " + programID);
			}
			tokenID = input.get(TOKEN_KEY);
			if (tokenID == null) {
				logger.logError("RECORD_FINDER_TOKEN_KEY not specified");
				return result;
			} else {
				tokenID = tokenID.trim().toUpperCase(getContext().getLocale());
				logger.logDebug("setting RECORD_FINDER_TOKEN_KEY : " + tokenID);
				recordFinderConfiguration = recordFinderMap.getConfiguration(programID, tokenID);
				if (recordFinderConfiguration == null) {
					logger.logError("Invalid RECORD_FINDER_TOKEN_KEY : " + tokenID);
					return result;
				}

				if (!recordFinderConfiguration.isDirectAccess()) {
					logger.logError("Detected RECORD_FINDER Attack : " + tokenID);
					return result;
				}
				if (recordFinderConfiguration.isIntercepted()) {
					input = processFilter(InterceptorPurpose.RECORDFINDER_PROCESS_RESOLVE, recordFinderConfiguration.getInterceptionClass(), input);
					if (input == null) {
						logger.logError("Invalid RECORD_FINDER_INTERCEPTION : " + tokenID);
						return result;
					}
					programID = input.get(PROGRAM_KEY);
					tokenID = input.get(TOKEN_KEY);
					recordFinderConfiguration = recordFinderMap.getConfiguration(programID, tokenID);
					if (recordFinderConfiguration == null) {
						logger.logError("Invalid RECORD_FINDER_TOKEN_KEY : " + tokenID);
						return result;
					}
					CommonValidator validator = new CommonValidator();
					try {
						DTObject partitionInfo = validator.getPartitionInfo(programID);
						if (partitionInfo.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
							logger.logError("Invalid PROGRAM : " + programID);
							return result;
						}
						partitionEnabled = partitionInfo.get("PARTITION_YEAR_WISE").equals("1") ? true : false;
						if (partitionEnabled) {
							input = processFilter(InterceptorPurpose.RECORDFINDER_ARGUMENTS_RESOLVE, recordFinderConfiguration.getInterceptionClass(), input);
							if (input == null) {
								logger.logError("Invalid RECORD_FINDER_INTERCEPTION : " + tokenID);
								return result;
							}
							bucketList = (Object[]) input.getObject(ContentManager.YEAR_PARAM);
						}
					} catch (Exception e) {
						logger.logError("Invalid RECORD_FINDER_INTERCEPTION : " + tokenID);
						return result;
					} finally {
						validator.close();
					}
				}

			}

			if (!input.containsKey(ARGUMENT_KEY)) {
				logger.logDebug("setting RECORD_FINDER_ARGS : default");
				arguments = EMPTY_STRING;
				input.set(ARGUMENT_KEY, EMPTY_STRING);
			} else {
				arguments = input.get(ARGUMENT_KEY);
				logger.logDebug("setting RECORD_FINDER_ARGS : " + arguments);
			}

			if (!input.containsKey(START_POSITION_KEY)) {
				// logger.logDebug("setting START_POSITION_KEY : default");
				startPosition = DEFAULT_POSITION;
			} else {
				startPosition = input.get(START_POSITION_KEY).trim();
				// logger.logDebug("setting START_POSITION_KEY : " +
				// startPosition);

				try {
					Integer.parseInt(startPosition);
				} catch (NumberFormatException e) {
					logger.logError("Invalid START_POSITION_KEY : " + startPosition);
					return result;
				}
			}
			if (!input.containsKey(BUFFER_KEY)) {
				// logger.logDebug("setting BUFFER_KEY : default");
				buffer = DEFAULT_BUFFER;
				input.set(BUFFER_KEY, DEFAULT_BUFFER);
			} else {
				buffer = input.get(BUFFER_KEY).trim();
				// logger.logDebug("setting BUFFER_KEY : " + buffer);
				try {
					Integer.parseInt(buffer);
				} catch (NumberFormatException e) {
					logger.logError("Invalid BUFFER_KEY : " + buffer);
					return result;
				}
				buffer = DEFAULT_BUFFER;
			}

			if (!input.containsKey(INIT_KEY)) {
				// logger.logDebug("setting INIT_KEY : default");
				init = true;
			} else {
				init = input.get(INIT_KEY).trim().equals("1");
				// logger.logDebug("setting INIT_KEY : " + buffer);
			}
			if (!input.containsKey(ORDER_COLUMN)) {
				// logger.logDebug("setting ORDER_COLUMN : default");
				orderColumn = "";
			} else {
				orderColumn = input.get(ORDER_COLUMN).trim();
				// logger.logDebug("setting ORDER_COLUMN : " + orderColumn);
				try {
					Integer.parseInt(orderColumn);
				} catch (NumberFormatException e) {
					logger.logError("Invalid ORDER_COLUMN : " + orderColumn);
					return result;
				}
			}
			if (!input.containsKey(SORT_ORDER)) {
				// logger.logDebug("setting SORT_ORDER : default");
				sortOrder = SORT_ASC;
				input.set(SORT_ORDER, SORT_ASC);
			} else {
				sortOrder = input.get(SORT_ORDER).trim();
				// logger.logDebug("setting SORT_ORDER : " + sortOrder);
				if (!(sortOrder.equals(SORT_ASC) || sortOrder.equals(SORT_DESC))) {
					logger.logError("Invalid SORT_ORDER : " + sortOrder);
					return result;
				}
			}
			int columnCount = recordFinderConfiguration.getColumnNames().length;
			searchColumns = new String[columnCount];
			searchTexts = new String[columnCount];
			for (int i = 0; i < columnCount; ++i) {
				if (input.containsKey("qg" + (i + 1))) {
					String text = input.get("qg" + (i + 1));
					searchColumns[i] = i + 1 + "";
					if (text == null || text.trim().equals(""))
						searchTexts[i] = null;
					else {
						searchTexts[i] = text;
					}

				}
			}

			generateSQLStatement(result);
			if (result.hasError()) {
				logger.logError("getData() : generateSQLStatement() " + result.get(ERROR));
				return result;
			}
			executeSQLStatement(result);
			if (result.hasError()) {
				logger.logError("executeSQLStatement()" + result.get(ERROR));
				return result;
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError(" getData(1) - " + e.getLocalizedMessage() + " : " + input);
			result.set(RESULT, DATA_UNAVAILABLE);
		}
		return result;
	}

	private void generateSQLStatement(DTObject result) throws Exception {
		// logger.logDebug("generateSQLStatement()");
		String basicSQL = recordFinderConfiguration.getSqlQuery();
		boolean isAndAvail = true;
		positionStart = Integer.parseInt(startPosition);
		positionEnd = positionStart + Integer.parseInt(buffer);

		boolean selectClauseAvailable = false;
		StringBuffer completeSQL = new StringBuffer(EMPTY_STRING);
		StringBuffer selectSql = new StringBuffer(EMPTY_STRING);
		if (basicSQL.contains(SELECT_CLAUSE)) {
			selectClauseAvailable = true;
			for (int index = 0; index < recordFinderConfiguration.getColumnNames().length; index++) {
				if (recordFinderConfiguration.getAliasNames()[index] != null) {
					selectSql.append(recordFinderConfiguration.getAliasNames()[index]).append(".");
				}
				selectSql.append(recordFinderConfiguration.getColumnNames()[index]);
				if (index != (recordFinderConfiguration.getColumnNames().length - 1))
					selectSql.append(",");
			}
			selectSql.append(" ");
			completeSQL.append(basicSQL.replace(SELECT_CLAUSE, selectSql.toString()));
		}
		StringBuffer selectXSql = new StringBuffer(EMPTY_STRING);
		if (!selectClauseAvailable) {
			if (basicSQL.contains(SELECT_CLAUSE_X)) {
				for (int index = 0; index < recordFinderConfiguration.getPredefinedColumns().length; index++) {
					selectXSql.append(recordFinderConfiguration.getPredefinedColumns()[index]);
					if (index != (recordFinderConfiguration.getPredefinedColumns().length - 1))
						selectXSql.append(",");
				}
				selectXSql.append(" ");
				completeSQL.append(basicSQL.replace(SELECT_CLAUSE_X, selectXSql.toString()));
			}
		}
		StringBuffer whereQuery = new StringBuffer(EMPTY_STRING);
		if (arguments != null && !arguments.equals("")) {
			String[] sql_arguments = arguments.split("\\|");
			int argumentCount = sql_arguments.length;
			if (argumentCount > 0) {
				ArrayList<RecordFinderFilter> arrayList = recordFinderConfiguration.getRecordFinderFilter();
				if (arrayList.size() > 0) {
					for (int z = 0; z < argumentCount; z++) {
						if (!sql_arguments[z].equals("")) {
							if (!isAndAvail) {
								whereQuery.append(" AND ");
							}
							isAndAvail = false;
							if (recordFinderConfiguration.getAliasNames()[z] != null) {
								whereQuery.append(recordFinderConfiguration.getAliasNames()[z]).append(".");
							}
							whereQuery.append(arrayList.get(z).getColumnName().toUpperCase(getContext().getLocale())).append(" = ? ");

						}
					}
				}
			}

		}

		int i = 0;
		isAndAvail = whereQuery.toString().equalsIgnoreCase(EMPTY_STRING);
		for (String searchColumn : searchColumns) {
			if (searchTexts[i] != null) {
				if (!isAndAvail) {

					whereQuery.append(" AND ");
				}
				isAndAvail = false;

				switch (recordFinderConfiguration.getColumnTypes()[Integer.parseInt(searchColumn)]) {
				case VARCHAR:
					whereQuery.append(" LOWER(");
					if (recordFinderConfiguration.getAliasNames()[Integer.parseInt(searchColumn)] != null) {
						whereQuery.append(recordFinderConfiguration.getAliasNames()[Integer.parseInt(searchColumn)]).append(".");
					}
					whereQuery.append(recordFinderConfiguration.getColumnNames()[Integer.parseInt(searchColumn)].toUpperCase(getContext().getLocale())).append(") LIKE  LOWER(?) ");
					break;
				case DATE:
					if (recordFinderConfiguration.getAliasNames()[Integer.parseInt(searchColumn)] != null) {
						whereQuery.append(recordFinderConfiguration.getAliasNames()[Integer.parseInt(searchColumn)]).append(".");
					}
					whereQuery.append(recordFinderConfiguration.getColumnNames()[Integer.parseInt(searchColumn)].toUpperCase(getContext().getLocale())).append(" >= TO_DATE(?,'#DATE_FORMAT#') ");
					break;
				case BIGDECIMAL:
					if (recordFinderConfiguration.getAliasNames()[Integer.parseInt(searchColumn)] != null) {
						whereQuery.append(recordFinderConfiguration.getAliasNames()[Integer.parseInt(searchColumn)]).append(".");
					}
					whereQuery.append(recordFinderConfiguration.getColumnNames()[Integer.parseInt(searchColumn)].toUpperCase(getContext().getLocale())).append(" = ? ");
					break;
				case INTEGER:
					if (recordFinderConfiguration.getAliasNames()[Integer.parseInt(searchColumn)] != null) {
						whereQuery.append(recordFinderConfiguration.getAliasNames()[Integer.parseInt(searchColumn)]).append(".");
					}
					whereQuery.append(recordFinderConfiguration.getColumnNames()[Integer.parseInt(searchColumn)].toUpperCase(getContext().getLocale())).append(" = ? ");
					break;
				case LONG:
					if (recordFinderConfiguration.getAliasNames()[Integer.parseInt(searchColumn)] != null) {
						whereQuery.append(recordFinderConfiguration.getAliasNames()[Integer.parseInt(searchColumn)]).append(".");
					}
					whereQuery.append(recordFinderConfiguration.getColumnNames()[Integer.parseInt(searchColumn)].toUpperCase(getContext().getLocale())).append(" = ? ");
					break;
				case TIMESTAMP:
					if (recordFinderConfiguration.getAliasNames()[Integer.parseInt(searchColumn)] != null) {
						whereQuery.append(recordFinderConfiguration.getAliasNames()[Integer.parseInt(searchColumn)]).append(".");
					}
					whereQuery.append(recordFinderConfiguration.getColumnNames()[Integer.parseInt(searchColumn)].toUpperCase(getContext().getLocale())).append(" >= str_to_date(?,'#DATETIME_FORMAT#') ");
					break;
				default:
					if (recordFinderConfiguration.getAliasNames()[Integer.parseInt(searchColumn)] != null) {
						whereQuery.append(recordFinderConfiguration.getAliasNames()[Integer.parseInt(searchColumn)]).append(".");
					}
					whereQuery.append(recordFinderConfiguration.getColumnNames()[Integer.parseInt(searchColumn)].toUpperCase(getContext().getLocale())).append(") LIKE  LOWER(?) ");
					break;
				}
			}
			i++;
		}
		StringBuffer whereSql = new StringBuffer(EMPTY_STRING);
		if (basicSQL.contains(WHERE_CLAUSE_X)) {
			if (!whereQuery.toString().equalsIgnoreCase(EMPTY_STRING)) {
				whereSql.append(" AND ").append(whereQuery.toString());
			}
			completeSQL = new StringBuffer(completeSQL.toString().replace(WHERE_CLAUSE_X, whereSql.toString()));
		} else if (basicSQL.contains(WHERE_CLAUSE)) {
			if (!whereQuery.toString().equalsIgnoreCase(EMPTY_STRING)) {
				whereSql.append(" WHERE ").append(whereQuery.toString());
			}
			completeSQL = new StringBuffer(completeSQL.toString().replace(WHERE_CLAUSE, whereSql.toString()));
		}

		StringBuffer orderQuery = new StringBuffer(EMPTY_STRING);

		if (!orderColumn.equals(EMPTY_STRING) && !sortOrder.equals(EMPTY_STRING)) {
			if (recordFinderConfiguration.getAliasNames()[(Integer.parseInt(orderColumn) - 1)] != null) {
				orderQuery.append(recordFinderConfiguration.getAliasNames()[(Integer.parseInt(orderColumn) - 1)]).append(".");
			}
			orderQuery.append(recordFinderConfiguration.getColumnNames()[(Integer.parseInt(orderColumn) - 1)]).append(" ").append(sortOrder);
		} else {
			for (int index = 0; index < recordFinderConfiguration.getColumnNames().length; index++) {
				String sortingOrder = recordFinderConfiguration.getDefaultSort()[index];
				if (sortingOrder != null && !sortingOrder.equals(EMPTY_STRING)) {
					sortingOrder = sortingOrder.equals("A") ? "ASC" : "DESC";
					if (recordFinderConfiguration.getAliasNames()[index] != null) {
						orderQuery.append(" ").append(recordFinderConfiguration.getAliasNames()[index]).append(".");
					}
					orderQuery.append(recordFinderConfiguration.getColumnNames()[index]).append(" ").append(sortingOrder).append(",");
				}
			}
			if (!orderQuery.toString().equals(EMPTY_STRING)) {
				orderQuery = new StringBuffer(orderQuery.toString().substring(0, orderQuery.toString().length() - 1));
			}
		}

		StringBuffer orderSql = new StringBuffer(EMPTY_STRING);
		if (completeSQL.toString().contains(ORDER_CLAUSE_X)) {
			orderSql.append(" ,").append(orderQuery.toString());
		} else if (completeSQL.toString().contains(ORDER_CLAUSE)) {
			orderSql.append(" ORDER BY ").append(orderQuery.toString());
		}

		completeSQL = new StringBuffer(completeSQL.toString().replace(ORDER_CLAUSE_X, orderSql.toString()));
		completeSQL = new StringBuffer(completeSQL.toString().replace(ORDER_CLAUSE, orderSql.toString()));

		if (partitionEnabled) {
			completeSQL = new StringBuffer(MessageFormat.format(completeSQL.toString(), bucketList));
		}

		String recordCountSql = EMPTY_STRING;
		if (selectClauseAvailable) {
			recordCountSql = completeSQL.toString().replaceFirst(Pattern.quote(MessageFormat.format(selectSql.toString().trim(), "")), "COUNT(1)");
		}
		if (!selectClauseAvailable) {
			recordCountSql = completeSQL.toString().replaceFirst(Pattern.quote(MessageFormat.format(selectXSql.toString().trim(), "")), "COUNT(1)");
		}
		if (!orderSql.equals(EMPTY_STRING) && !orderSql.equals(" ")) {
			recordCountSql = recordCountSql.replace(orderSql, EMPTY_STRING);
		}

		basicSQL = preProcessSQL(completeSQL.toString());
		recordCountSql = preProcessSQL(recordCountSql);

		completeSQL.setLength(0);
		completeSQL.append(basicSQL);
		// rowCount = getRowCount(recordCountSql);
		completeSQL.append(" LIMIT " + positionStart + "," + DEFAULT_BUFFER);
		sqlQuery = completeSQL.toString();
		logger.logInfo(sqlQuery);
	}

	private void executeSQLStatement(DTObject result) {
		// logger.logDebug("executeSQLStatement()");
		DBUtil util = getDbContext().createUtilInstance();
		int argumentCount = 0;
		try {
			result.set(RESULT, ROW_NOT_PRESENT);
			util.reset();
			util.setSql(sqlQuery);
			int i = 0;
			int k = 1;
			if (arguments != null && !arguments.equals("")) {
				String[] sql_arguments = arguments.split("\\|");
				argumentCount = sql_arguments.length;
				if (argumentCount > 0) {
					ArrayList<RecordFinderFilter> arrayList = recordFinderConfiguration.getRecordFinderFilter();
					if (arrayList.size() > 0) {
						for (int z = 0; z < argumentCount; z++) {

							if (arrayList.get(z).getColumnType().equals(IFDATE)) {
								Date date = BackOfficeFormatUtils.getDate(sql_arguments[z], getContext().getDateFormat());
								if (date == null)
									date = new Date();
								util.setDate(k, new java.sql.Date(date.getTime()));

							} else {
								util.setString(k, sql_arguments[z]);
							}

						}
					} else {
						for (int z = 0; z < argumentCount; z++) {
							if (sql_arguments[z].equals("")) {
								util.setString(k, null);
							} else {
								util.setString(k, sql_arguments[z]);
							}

						}
					}
					k++;
				}
			}

			for (String searchColumn : searchColumns) {
				if (searchTexts[i] != null) {
					switch (recordFinderConfiguration.getColumnTypes()[Integer.parseInt(searchColumn)]) {
					case VARCHAR:

						util.setString(k, "%" + searchTexts[i] + "%");
						break;
					case DATE:
						util.setString(k, searchTexts[i]);
						break;
					case INTEGER:
					case LONG:
					default:
						util.setString(k, searchTexts[i]);
						break;
					}
					k++;
				}
				++i;
			}
			ResultSet resultSet = util.executeQuery();
			String xmlStr = getXML(positionStart, resultSet);
			if (!xmlStr.equals("")) {
				result.set(RESULT, ROW_PRESENT);
				result.set(CONTENT, xmlStr);
				result.set(RESULT, DATA_AVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError(" executeSQLStatement(1) - " + e.getLocalizedMessage());
			result.set(ERROR, e.getLocalizedMessage());
			result.set(RESULT, DATA_UNAVAILABLE);
		} finally {
			util.reset();
		}
	}

	
	private String getXML(long posStart, ResultSet resultSet) {
		DHTMLXGridUtility contentUtility = new DHTMLXGridUtility();
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(BackOfficeFormatUtils.getSimpleDateFormat(getContext().getDateFormat()));
			SimpleDateFormat sdtf = new SimpleDateFormat(BackOfficeFormatUtils.getSimpleDateFormat(BackOfficeConstants.JAVA_DATE_TIME_FORMAT));
			ResultSetMetaData metaData = resultSet.getMetaData();
			int columnCount = metaData.getColumnCount();
			StringBuffer searchColumnBuffer = new StringBuffer();
			StringBuffer searchHeaderBuffer = new StringBuffer();
			if (init && positionStart == 0) {
				contentUtility.startHead();
				int i = 0;
				int totalWidth = 0;
				for (String columnName : recordFinderConfiguration.getColumnHeadings_en_US()) {
					totalWidth += Integer.parseInt(recordFinderConfiguration.getColumnWidths()[i]);
					contentUtility.setColumn(columnName, recordFinderConfiguration.getColumnWidths()[i], recordFinderConfiguration.getColumnUI()[i], recordFinderConfiguration.getColumnAlign()[i]);
					if (recordFinderConfiguration.getSearchColumns()[i]) {
						searchColumnBuffer.append(i).append("|");
						if (recordFinderConfiguration.getColumnTypes()[i].equals(BindParameterType.DATE)) {
							searchHeaderBuffer.append("#server_date_filter").append(",");
						}else if (recordFinderConfiguration.getColumnTypes()[i].equals(BindParameterType.TIMESTAMP)) {
							searchHeaderBuffer.append("#server_dateTime_filter").append(",");
						} else {
							searchHeaderBuffer.append("#server_text_filter").append(",");
						}
					} else {
						searchHeaderBuffer.append(",");
					}
					i++;
				}
				totalWidth = totalWidth + 10;
				searchColumnBuffer.trimToSize();
				searchHeaderBuffer.trimToSize();
				if (searchHeaderBuffer.length() != 0) {
					contentUtility.setBeforeInitCommand("attachHeader", searchHeaderBuffer.substring(0, searchHeaderBuffer.length() - 1));
				}
				contentUtility.setBeforeInitCommand("setImagePath", "public/cdn/vendor/dhtmlxGrid/codebase/imgs/");
				contentUtility.setBeforeInitCommand("adjustQueryGrid", totalWidth + "");
				if (searchColumnBuffer.length() != 0) {
					contentUtility.setBeforeInitCommand("setQueryGrid", searchColumnBuffer.substring(0, searchColumnBuffer.length() - 1));
				}
				contentUtility.endHead();
			}

			int rowNumber = positionStart + 1;
			int currentRowCount = 0;
			while (resultSet.next()) {
				contentUtility.startRow(String.valueOf(rowNumber));
				if (recordFinderConfiguration.getColumnUI()[0].equals(RecordFinderConfiguration.UITYPE_LINK)) {
					contentUtility.setCell("Edit^javascript:doGridSelect(\"" + resultSet.getString(2) + "\")");
				} else {
					contentUtility.setCell(resultSet.getString(1));
				}
				for (int i = 2; i <= columnCount; i++) {
					if (resultSet.getMetaData().getColumnTypeName(i).equals("DATE") || resultSet.getMetaData().getColumnTypeName(i).equals("DATETIME") || resultSet.getMetaData().getColumnTypeName(i).equals("TIMESTAMP")) {
						if (resultSet.getDate(i) == null) {
							contentUtility.setCell("");
						}else if(resultSet.getMetaData().getColumnTypeName(i).equals("DATETIME")){
							contentUtility.setCell(sdtf.format(resultSet.getTimestamp(i)));
						}else {
							contentUtility.setCell(sdf.format(resultSet.getDate(i)));
						}
					} else {
						if (resultSet.getString(i) == null) {
							contentUtility.setCell("");
						} else {
							contentUtility.setCell(resultSet.getString(i));
						}
					}
				}
				contentUtility.endRow();

				++rowNumber;
				++currentRowCount;
			}

			if (currentRowCount == Integer.parseInt(DEFAULT_BUFFER)) {
				rowCount = Integer.parseInt(startPosition) + Integer.parseInt(DEFAULT_BUFFER) + 1;
			} else {
				rowCount = Integer.parseInt(startPosition) + currentRowCount;
			}

		} catch (Exception e) {
			e.printStackTrace();
			logger.logError(" getXML(1) - " + e.getLocalizedMessage());
		}

		DHTMLXGridUtility utility = new DHTMLXGridUtility();
		utility.init(Long.valueOf(rowCount), Long.valueOf(startPosition));

		utility.appendContent(contentUtility.getXML());

		utility.finish();

		return utility.getXML().toString();
	}

	private int getRowCount(String sql) throws Exception {
		// logger.logDebug("getRowCount()");
		int rowsCount = 0;
		int argumentCount = 0;

		DBUtil util = getDbContext().createUtilInstance();
		try {

			util.reset();
			util.setSql(sql);
			int i = 0;
			int k = 1;
			if (arguments != null && !arguments.equals("")) {
				String[] sql_arguments = arguments.split("\\|");
				argumentCount = sql_arguments.length;
				if (argumentCount > 0) {

					ArrayList<RecordFinderFilter> arrayList = recordFinderConfiguration.getRecordFinderFilter();
					if (arrayList.size() > 0) {
						for (int z = 0; z < argumentCount; z++) {

							if (arrayList.get(z).getColumnType().equals(IFDATE)) {
								Date date = BackOfficeFormatUtils.getDate(sql_arguments[z], getContext().getDateFormat());
								if (date == null)
									date = new Date();
								util.setDate(k, new java.sql.Date(date.getTime()));

							} else {
								util.setString(k, sql_arguments[z]);

							}

						}
					} else {
						for (int z = 0; z < argumentCount; z++) {
							if (sql_arguments[z].equals("")) {
								util.setString(k, null);
							} else {
								util.setString(k, sql_arguments[z]);

							}
						}
					}
					k++;
				}
			}

			for (String searchColumn : searchColumns) {
				if (searchTexts[i] != null) {
					switch (recordFinderConfiguration.getColumnTypes()[Integer.parseInt(searchColumn)]) {
					case VARCHAR:

						util.setString(k, "%" + searchTexts[i] + "%");
						break;
					case DATE:
						util.setString(k, searchTexts[i]);

						break;
					case INTEGER:
					case LONG:
					default:
						util.setString(k, searchTexts[i]);
						break;
					}
					k++;
				}
				++i;
			}
			ResultSet resultSet = util.executeQuery();
			if (resultSet.next()) {
				rowsCount = resultSet.getInt(1);
			} else {
				rowsCount = 0;
			}
		} catch (Exception e) {
			e.printStackTrace();
			rowsCount = 0;
			logger.logError(" getRowCount() - " + e.getLocalizedMessage());
			throw e;
		} finally {
			util.reset();
		}
		return rowsCount;
	}
}