package patterns.config.framework.web.configuration.mtm;

import java.sql.Date;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import patterns.config.framework.DatasourceConfigurationManager;
import patterns.config.framework.database.BindParameterType;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.objects.TableInfo;
import patterns.config.framework.database.utils.DBInfo;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.database.utils.MySqlDBInfo;
import patterns.config.framework.database.utils.OrclDBInfo;
import patterns.config.framework.loggers.ApplicationLogger;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.web.ajax.AJAXContentManager;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.CommonValidator;

public class RecordQueryManager extends AJAXContentManager {

	private static final String PROGRAM_ID = "_ProgramID";
	private static final String TOKEN_ID = "_TokenID";
	private static final String RMTM_PRIMARY_KEY = "_PrimaryKey";
	private static final String RMTM_REQUEST_TYPE = "_RMtmReqType";
	public static final String TBA_DATE_FORMAT = "dd-MM-yyyy HH24:MI:SS";
	private ApplicationLogger logger = null;
	private TableInfo tableInfo = null;
	private ResultSet rset = null;

	private String programID;
	private String primaryKeyValues = null;
	private String reqtype = "";
	private String type = "";
	int counter = 0;
	private StringBuffer sqlCommand = null;
	private Date entryDate = null;
	private String mTable;
	private long entrySl;
	private DTObject result = new DTObject();
	private ArrayList<String> mTableArrayList = new ArrayList<String>();
	private String[] keyFields;
	private BindParameterType[] keyFieldTypes;
	private String programType;

	public String getProgramType() {
		return programType;
	}

	public void setProgramType(String programType) {
		this.programType = programType;
	}

	public RecordQueryManager() {
		super();
		logger = ApplicationLogger.getInstance(this.getClass().getSimpleName());
		// logger.logDebug("#()");
	}

	public DTObject getData(DTObject input) {
		// logger.logDebug("getData()");
		CommonValidator validator = new CommonValidator();
		try {
			RecordQueryMap dataMap = RecordQueryMap.getInstance();
			programID = input.get(PROGRAM_ID).toUpperCase(getContext().getLocale());
			//ARIBALA ADDED ON 09102015 BEGIN
			setProgramType(validator.getProgramType(programID));
			//ARIBALA ADDED ON 09102015 END
			primaryKeyValues = input.get(RMTM_PRIMARY_KEY).trim();
			reqtype = input.get(RMTM_REQUEST_TYPE).trim();
			String processedTableName="";
			String tokenID = input.get(TOKEN_ID);
			String sqlQuery = dataMap.getDescriptor(programID, tokenID);
			sqlQuery = preProcessSQL(sqlQuery);
			sqlCommand = new StringBuffer(sqlQuery);
			String pattern = GridQueryMap.NAME_SEPERATOR + "([A-Za-z0-9_])+" + GridQueryMap.NAME_SEPERATOR;
			Pattern p = Pattern.compile(pattern);
			int start = 0;
			int end = 0;
			do {
				Matcher m = p.matcher(sqlCommand.toString());
				if (m.find()) {
					start = m.start();
					end = m.end();
					Pattern p1 = Pattern.compile("([A-Za-z0-9_])+");
					Matcher m1 = p1.matcher(m.group());
					if (m1.find()) {
						mTable = m1.group();
						mTableArrayList.add(mTable);
					}
					if (reqtype.equals("B")) {
						type = "T";
						processedTableName = getTableName(mTable);
					} else {
						type = reqtype;
						processedTableName = getTableName(mTable);
					}
					sqlCommand.replace(start, end, processedTableName);
				} else {
					break;
				}
			} while (true);
			logger.logInfo("getData() : SQL Query : " + sqlCommand);
			result.set(ERROR, "");
			executeSQLQuery();
			if (reqtype.equals("B")) {
				if (counter == 0) {
					sqlCommand = new StringBuffer(sqlQuery);;
					start = 0;
					end = 0;
					do {
						Matcher m = p.matcher(sqlCommand.toString());
						if (m.find()) {
							start = m.start();
							end = m.end();
							Pattern p1 = Pattern.compile("([A-Za-z0-9_])+");
							Matcher m1 = p1.matcher(m.group());
							if (m1.find()) {
								mTable = m1.group();
								mTableArrayList.add(mTable);
							}

							type = "M";
							processedTableName = getTableName(mTable);

							sqlCommand.replace(start, end, processedTableName);
						} else {
							break;
						}
					} while (true);
					executeSQLQuery();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError(" getData(1) - " + e.getLocalizedMessage() + input);
			result.set(ERROR, e.getLocalizedMessage());
			result.set(RESULT, DATA_UNAVAILABLE);
		} finally {
			validator.close();
		}
		return result;
	}

	private String getTableName(String mtable) {
		// logger.logDebug(" getTableName()");
		StringBuffer buffer = new StringBuffer();
		DBUtil dbutil = getDbContext().createUtilInstance();
		StringBuffer query = new StringBuffer();
		try {
			//ARIBALA ADDED ON 09102015 BEGIN
			if (type.equalsIgnoreCase("M") || getProgramType().equals("M")) {
				//ARIBALA ADDED ON 09102015 END
				buffer.append(mtable);
			} else {
				buffer.append("( SELECT");
				dbutil.reset();
				dbutil.setMode(DBUtil.PREPARED);
				dbutil.setSql("SELECT TBAQ_ENTRY_DATE,TBAQ_ENTRY_SL,TBAQ_OPERN_FLG FROM TBAAUTHQ WHERE  ENTITY_CODE = ?  AND  TBAAUTHQ.TBAQ_PGM_ID = ? AND TBAAUTHQ.TBAQ_MAIN_PK = ? ");
				dbutil.setString(1, getContext().getEntityCode());
				dbutil.setString(2, programID);
				dbutil.setString(3, primaryKeyValues);
				ResultSet rs = dbutil.executeQuery();
				if (rs.next()) {
					entryDate = rs.getDate("TBAQ_ENTRY_DATE");
					entrySl = rs.getLong("TBAQ_ENTRY_SL");
				}
				DBInfo info = new DBInfo();
				tableInfo = info.getTableInfo(mtable);
				String[] fieldNames = tableInfo.getColumnInfo().getColumnNames();
				Map<String, BindParameterType> columnType = tableInfo.getColumnInfo().getColumnMapping();
				for (String fieldName : fieldNames) {
					if (columnType.get(fieldName).equals(BindParameterType.DATE)) {
						query.append(" TO_DATE(EXTRACTVALUE(T.TBADTL_DATA_BLOCK,'//" + fieldName + "'),'" + BackOfficeConstants.JAVA_TBA_DB_DATE_FORMAT + "')" + fieldName + ",");
					} else {
						query.append(" EXTRACTVALUE(T.TBADTL_DATA_BLOCK,'//" + fieldName + "')" + fieldName + ",");
					}
				}
				query.trimToSize();
				buffer.append(query.substring(0, query.length() - 1));
				buffer.append(" FROM TBAAUTHDTL T WHERE T.ENTITY_CODE=? AND T.TBADTL_MAIN_PK=?");
				buffer.append(" AND T.TBADTL_ENTRY_DATE=? AND T.TBADTL_ENTRY_SL= ? AND T.TBADTL_TABLE_NAME=?");
				buffer.append(" )");
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError(" getTableName(1) - " + e.getLocalizedMessage());
		} finally {
			dbutil.reset();
		}
		return buffer.toString();
	}

	private void executeSQLQuery() throws Exception {
		// logger.logDebug(" executeSQLQuery()");
		DBUtil util = getDbContext().createUtilInstance();
		int index = 1;
		try {
			util.reset();
			util.setSql(sqlCommand.toString());
			//ARIBALA CHANGED ON 09102015 BEGIN
			if (type.equalsIgnoreCase("T") && getProgramType().equals("A")) {
				//ARIBALA CHANGED ON 09102015 BEGIN
				for (int i = 0; i < mTableArrayList.size(); ++i) {
					util.setString(index++, getContext().getEntityCode());
					util.setString(index++, primaryKeyValues);
					util.setDate(index++, entryDate);
					util.setLong(index++, entrySl);
					util.setString(index++, mTableArrayList.get(i));
				}
			}
			String[] arguments = primaryKeyValues.split("\\|");
			for (int i = 0; i < arguments.length; ++i) {
				util.setString(i + index, arguments[i]);
			}
			rset = util.executeQuery();
			SimpleDateFormat sdf = new SimpleDateFormat(getContext().getDateFormat());
			if (rset.next()) {
				for (int j = 1; j < rset.getMetaData().getColumnCount() + 1; ++j) {
					if (rset.getMetaData().getColumnTypeName(j).equals("DATE")) {
						if (rset.getDate(j) != null) {
							result.set(rset.getMetaData().getColumnName(j), sdf.format(rset.getDate(j)));
						} else {
							result.set(rset.getMetaData().getColumnName(j), null);
						}
					} else {
						result.set(rset.getMetaData().getColumnName(j), rset.getString(j));
					}
				}
				counter++;
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError(" executeSQLQuery(1) - " + e.getLocalizedMessage());
			throw e;
		} finally {
			util.reset();
		}
	}
}
