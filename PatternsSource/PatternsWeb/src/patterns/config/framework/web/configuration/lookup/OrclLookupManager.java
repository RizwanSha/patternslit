package patterns.config.framework.web.configuration.lookup;

import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.StringTokenizer;

import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.loggers.ApplicationLogger;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.DHTMLXGridUtility;
import patterns.config.framework.web.ajax.AJAXContentManager;
import patterns.config.framework.web.ajax.InterceptorPurpose;

public class OrclLookupManager extends AJAXContentManager {

	private String tokenID = "";
	private String programID = "";
	private String arguments = "";
	private String startPosition = "";

	private String buffer = "";
	private String searchText = "";
	private String searchColumn = "";
	private String orderColumn = "";
	private String sortOrder = "";
	private boolean init = false;

	private LookupConfiguration lookupConfiguration = null;
	private int positionStart = 0;
	private int positionEnd = 0;
	private String sqlQuery = "";
	private int rowCount = 0;
	ApplicationLogger logger = null;

	public OrclLookupManager() {
		super();
		logger = ApplicationLogger.getInstance(this.getClass().getSimpleName());
		// logger.logDebug("#()");
	}

	public DTObject getData(DTObject input) {
		// logger.logDebug("getData()");
		DTObject result = new DTObject();
		try {
			LookupMap lookUpMap = LookupMap.getInstance();
			result.set(RESULT, DATA_UNAVAILABLE);
			programID = input.get(PROGRAM_KEY);
			if (programID == null) {
				logger.logError("LOOKUP_PROGRAM_ID not specified");
				return result;
			} else {
				programID = programID.trim().toUpperCase(getContext().getLocale());
				logger.logDebug("setting LOOKUP_PROGRAM_ID : " + programID);
			}
			tokenID = input.get(TOKEN_KEY);
			if (tokenID == null) {
				logger.logError("LOOKUP_TOKEN_KEY not specified");
				return result;
			} else {
				tokenID = tokenID.trim().toUpperCase(getContext().getLocale());
				logger.logDebug("setting LOOKUP_TOKEN_KEY : " + tokenID);
				lookupConfiguration = lookUpMap.getConfiguration(programID, tokenID);
				if (lookupConfiguration == null) {
					logger.logError("Invalid LOOKUP_TOKEN_KEY : " + tokenID);
					return result;
				}
				if (!lookupConfiguration.isDirectAccess()) {
					logger.logError("Detected LOOKUP_TOKEN Attack : " + tokenID);
					return result;
				}
				if (lookupConfiguration.isIntercepted()) {
					input = processFilter(InterceptorPurpose.LOOKUP_PROCESS_RESOLVE, lookupConfiguration.getInterceptionClass(), input);
					if (input == null) {
						logger.logError("Invalid LOOKUP_INTERCEPTION : " + tokenID);
						return result;
					}
					programID = input.get(PROGRAM_KEY);
					tokenID = input.get(TOKEN_KEY);
					lookupConfiguration = lookUpMap.getConfiguration(programID, tokenID);
					if (lookupConfiguration == null) {
						logger.logError("Invalid LOOKUP_TOKEN_KEY : " + tokenID);
						return result;
					}
				}
			}
			if (!input.containsKey(ARGUMENT_KEY)) {
				logger.logDebug("setting LOOKUP_ARGS : default");
				arguments = EMPTY_STRING;
				input.set(ARGUMENT_KEY, EMPTY_STRING);
			} else {
				arguments = input.get(ARGUMENT_KEY);
				logger.logDebug("setting LOOKUP_ARGS : " + arguments);
			}
			if (!input.containsKey(START_POSITION_KEY)) {
				// logger.logDebug("setting START_POSITION_KEY : default");
				startPosition = DEFAULT_POSITION;
			} else {
				startPosition = input.get(START_POSITION_KEY).trim();
				// logger.logDebug("setting START_POSITION_KEY : " + startPosition);
				try {
					Integer.parseInt(startPosition);
				} catch (NumberFormatException e) {
					logger.logError("Invalid START_POSITION_KEY : " + startPosition);
					return result;
				}
			}

			if (!input.containsKey(BUFFER_KEY)) {
				// logger.logDebug("setting BUFFER_KEY : default");
				buffer = DEFAULT_BUFFER;
				input.set(BUFFER_KEY, DEFAULT_BUFFER);
			} else {
				buffer = input.get(BUFFER_KEY).trim();
				// logger.logDebug("setting BUFFER_KEY : " + buffer);
				try {
					Integer.parseInt(buffer);
				} catch (NumberFormatException e) {
					logger.logError("Invalid BUFFER_KEY : " + buffer);
					return result;
				}
				buffer = DEFAULT_BUFFER;
			}
			if (!input.containsKey(INIT_KEY)) {
				// logger.logDebug("setting INIT_KEY : default");
				init = true;
			} else {
				init = input.get(INIT_KEY).trim().equals("1");
				// logger.logDebug("setting INIT_KEY : " + buffer);
			}

			if (!input.containsKey(SEARCH_TEXT)) {
				// logger.logDebug("setting SEARCH_TEXT : default");
				searchText = EMPTY_STRING;
			} else {
				searchText = input.get(SEARCH_TEXT).trim();
				// logger.logDebug("setting SEARCH_TEXT : " + searchText);
			}
			if (!input.containsKey(SEARCH_COLUMN)) {
				// logger.logDebug("setting SEARCH_COLUMN : default");
				searchColumn = DEFAULT_POSITION;

			} else {
				searchColumn = input.get(SEARCH_COLUMN).trim();
				// logger.logDebug("setting SEARCH_COLUMN : " + searchColumn);
				try {
					Integer.parseInt(searchColumn);
				} catch (NumberFormatException e) {
					logger.logError("Invalid SEARCH_COLUMN : " + searchColumn);
					return result;
				}
			}

			if (!input.containsKey(ORDER_COLUMN)) {
				// logger.logDebug("setting ORDER_COLUMN : default");
				orderColumn = "1";
			} else {
				orderColumn = input.get(ORDER_COLUMN).trim();
				// logger.logDebug("setting ORDER_COLUMN : " + orderColumn);
				try {
					Integer.parseInt(orderColumn);
				} catch (NumberFormatException e) {
					logger.logError("Invalid ORDER_COLUMN : " + orderColumn);
					return result;
				}
			}
			if (!input.containsKey(SORT_ORDER)) {
				// logger.logDebug("setting SORT_ORDER : default");
				sortOrder = SORT_ASC;
				input.set(SORT_ORDER, SORT_ASC);
			} else {
				sortOrder = input.get(SORT_ORDER).trim();
				// logger.logDebug("setting SORT_ORDER : " + sortOrder);
				if (!(sortOrder.equals(SORT_ASC) || sortOrder.equals(SORT_DESC))) {
					logger.logError("Invalid SORT_ORDER : " + sortOrder);
					return result;
				}
			}

			generateSQLStatement(result);
			if (result.hasError()) {
				logger.logError("SQL Statement Generation has error");
				return result;
			}
			executeSQLStatement(result);
			if (result.hasError()) {
				logger.logError("SQL Statement Generation has error");
				return result;
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError("getData(1) : " + e.getLocalizedMessage() + " : " + input);
			result.set(RESULT, DATA_UNAVAILABLE);
		}
		return result;
	}

	private void generateSQLStatement(DTObject result) throws Exception {
		// logger.logDebug("generateSQLStatement()");
		String basicSQL = lookupConfiguration.getSqlQuery();

		positionStart = Integer.parseInt(startPosition);
		positionEnd = positionStart + Integer.parseInt(buffer);

		StringBuffer completeSQL = new StringBuffer();
		completeSQL.append("SELECT ");
		int columnCount = 0;
		for (String columnName : lookupConfiguration.getColumnNames()) {
			columnCount++;
			completeSQL.append(columnName);
			if (columnCount != lookupConfiguration.getColumnNames().length)
				completeSQL.append(",");
			completeSQL.append(" ");

		}
		completeSQL.append(",ROWNUM R FROM (SELECT * FROM ( ");
		completeSQL.append(basicSQL);
		completeSQL.append(" ) ");
		if (!searchText.equals(EMPTY_STRING)) {
			completeSQL.append(" WHERE  ");
			switch (lookupConfiguration.getColumnTypes()[Integer.parseInt(searchColumn)]) {
			case VARCHAR:
			default:
				searchText = "%" + searchText + "%";
				completeSQL.append(" LOWER(" + lookupConfiguration.getColumnNames()[Integer.parseInt(searchColumn)].toUpperCase(getContext().getLocale()) + ") LIKE  LOWER(?) ");
				break;
			case DATE:
			case TIMESTAMP:
				completeSQL.append(" " + lookupConfiguration.getColumnNames()[Integer.parseInt(searchColumn)].toUpperCase(getContext().getLocale()) + " >= to_date(?,'#DATE_FORMAT#') ");
				break;
			case BIGDECIMAL:
			case INTEGER:
			case LONG:
				completeSQL.append(" " + lookupConfiguration.getColumnNames()[Integer.parseInt(searchColumn)].toUpperCase(getContext().getLocale()) + " = ? ");
				break;
			}
		}

		completeSQL.append(" ORDER BY (" + orderColumn + ") " + sortOrder + " ) ");

		basicSQL = preProcessSQL(completeSQL.toString());
		completeSQL.setLength(0);
		completeSQL.append(basicSQL);
		rowCount = getRowCount(completeSQL.toString());
		completeSQL.append("WHERE ROWNUM <= " + positionEnd);
		sqlQuery = "SELECT * FROM (" + completeSQL.toString() + " ) WHERE R > " + positionStart;
		logger.logInfo(sqlQuery);
	}

	private void executeSQLStatement(DTObject result) {
		// logger.logDebug("executeSQLStatement()");
		DBUtil util = getDbContext().createUtilInstance();
		int bindValuesCount = 0;
		int argumentCount = 0;
		int _index = 1;
		try {
			result.set(RESULT, ROW_NOT_PRESENT);
			util.reset();
			util.setSql(sqlQuery);
			if (sqlQuery.indexOf("?") > -1) {
				bindValuesCount = (new StringTokenizer(sqlQuery, "?").countTokens()) - 1;
				String[] sql_arguments = arguments.split("\\|");
				if (arguments.equals(EMPTY_STRING)) {
					argumentCount = 0;
				} else {
					argumentCount = sql_arguments.length;
				}
				if (argumentCount == 0 && bindValuesCount > 0) {
					util.setString(_index, searchText);
				} else if (bindValuesCount != argumentCount) {
					for (int k = 1; k <= argumentCount; k++) {
						util.setString(_index, sql_arguments[k - 1]);
						_index++;
					}
					util.setString(_index, searchText);
				} else if ((bindValuesCount == argumentCount) && argumentCount > 0) {
					for (int k = 1; k <= argumentCount; k++) {
						util.setString(_index, sql_arguments[k - 1]);
						_index++;
					}
				}
			}

			ResultSet resultSet = util.executeQuery();
			String xmlStr = getXML(rowCount, positionStart, resultSet);
			if (!xmlStr.equals("")) {
				result.set(RESULT, ROW_PRESENT);
				result.set(CONTENT, xmlStr);
				result.set(RESULT, DATA_AVAILABLE);
			}
			util.reset();

		} catch (Exception e) {
			e.printStackTrace();
			logger.logError(" executeSQLStatement(1) - " + e.getLocalizedMessage());
			result.set(RESULT, DATA_UNAVAILABLE);
		} finally {
		}
	}

	public String getXML(int rowCount, long posStart, ResultSet resultSet) {
		// logger.logDebug("getXML()");
		DHTMLXGridUtility utility = new DHTMLXGridUtility();
		int colCount;
		ResultSet rsGeneric = null;
		try {
			rsGeneric = resultSet;
			colCount = rsGeneric.getMetaData().getColumnCount();
			SimpleDateFormat sdf = new SimpleDateFormat(BackOfficeConstants.TBA_DATE_FORMAT);
			utility.init(rowCount, positionStart);
			StringBuffer comboBuffer = new StringBuffer();
			if (init && positionStart == 0) {
				utility.startHead();
				int i = 0;
				for (String columnName : lookupConfiguration.getColumnHeadings_en_US()) {
					utility.setColumn(columnName, "*", "ro", "left", "server");
					if (i != 0) {
						comboBuffer.append("|");
					}
					comboBuffer.append(i).append("#").append(lookupConfiguration.getColumnHeadings_en_US()[i]);
					i++;
				}
				comboBuffer.trimToSize();
				utility.setafterInitCommand("setLookupCombo", comboBuffer.toString());
				utility.endHead();
			}

			while (rsGeneric.next()) {
				utility.startRow(rsGeneric.getInt(colCount) + "");
				for (int i = 1; i < colCount; i++) {
					if (rsGeneric.getMetaData().getColumnTypeName(i).equals("DATE")) {
						if (rsGeneric.getDate(i) == null) {
							utility.setCell("");
						} else {
							utility.setCell(sdf.format(rsGeneric.getDate(i)));
						}
					} else {
						if (rsGeneric.getString(i) == null) {
							utility.setCell("");
						} else {
							utility.setCell(rsGeneric.getString(i));
						}
					}
				}
				utility.endRow();
			}

		} catch (Exception e) {
			e.printStackTrace();
			logger.logError(" getXML(1) - " + e.getLocalizedMessage());
			return "";
		}
		utility.finish();
		return utility.getXML();
	}

	private int getRowCount(String sql) throws Exception {
		// logger.logDebug("getRowCount()");
		int rowsCount = 0;
		String tempSQL = "";
		DBUtil util = getDbContext().createUtilInstance();
		try {
			tempSQL = "SELECT COUNT(1) FROM (" + sql + ")";
			util.reset();
			util.setSql(tempSQL);
			int bindValuesCount = 0;
			int argumentCount = 0;
			int _index = 1;
			if (tempSQL.indexOf("?") > -1) {
				bindValuesCount = (new StringTokenizer(tempSQL, "?").countTokens()) - 1;
				String[] sql_arguments = arguments.split("\\|");
				if (arguments.equals(EMPTY_STRING)) {
					argumentCount = 0;
				} else {
					argumentCount = sql_arguments.length;
				}
				if (argumentCount == 0 && bindValuesCount > 0) {
					util.setString(_index, searchText);
				} else if (bindValuesCount != argumentCount) {
					for (int k = 1; k <= argumentCount; k++) {
						util.setString(_index, sql_arguments[k - 1]);
						_index++;
					}
					util.setString(_index, searchText);
				} else if ((bindValuesCount == argumentCount) && argumentCount > 0) {
					for (int k = 1; k <= argumentCount; k++) {
						util.setString(_index, sql_arguments[k - 1]);
						_index++;
					}
				}
			}
			ResultSet resultSet = util.executeQuery();
			if (resultSet.next()) {
				rowsCount = resultSet.getInt(1);
			} else {
				rowsCount = 0;
			}
			util.reset();
		} catch (Exception e) {
			rowsCount = 0;
			e.printStackTrace();
			logger.logError(" getRowCount(1) - " + e.getLocalizedMessage());
		} finally {
		}
		return rowsCount;
	}
}
