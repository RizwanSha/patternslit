package patterns.config.framework.web.configuration.menu;

import java.io.Serializable;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.LinkedList;


import javax.servlet.http.HttpServletRequest;

import org.apache.struts.config.ModuleConfig;
import org.apache.struts.util.ModuleUtils;

import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.thread.ApplicationContext;
import patterns.config.framework.web.FormatUtils;
import patterns.config.framework.web.GenericOption;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.AccessValidator;

public class MenuUtils implements Serializable {

	private static final long serialVersionUID = -3707773454639955245L;
	private ApplicationContext context = ApplicationContext.getInstance();

	public LinkedList<GenericOption> getRoles(String userID) {
		LinkedList<GenericOption> genericList = new LinkedList<GenericOption>();
		DBContext dbContext = new DBContext();
		try {
			DBUtil util = dbContext.createUtilInstance();
			String entityCode = context.getEntityCode();
			DTObject result = validateUser(entityCode, userID);
			if (result.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
				String primaryRoleSql = "SELECT BR.CODE,BR.DESCRIPTION FROM ROLES BR WHERE BR.ENTITY_CODE = ? AND BR.CODE= FN_GETUSERSROLECD(?,?,?) AND BR.ENABLED='1'";
				util.setMode(DBUtil.PREPARED);
				util.setSql(primaryRoleSql);
				util.setString(1, entityCode);
				util.setString(2, context.getPartitionNo());
				util.setString(3, entityCode);
				util.setString(4, userID);
				ResultSet rs = util.executeQuery();
				int counter = 0;
				if (rs.next()) {
					String value = rs.getString(2) + " (" + rs.getString(1) + ")";
					genericList.add(counter, new GenericOption(rs.getString(1), value));
					counter++;
				}
				util.reset();
				AccessValidator validator = new AccessValidator(dbContext);
				result = validator.getSYSCPM(result);
				if (result.get(ContentManager.ERROR) == null) {
					if (result.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
						boolean actingRoleAllowed = FormatUtils.decodeStringToBoolean(result.get("MAX_ACTING_ROLE"));
						if (actingRoleAllowed) {
							String actingRoleSql = "SELECT BUA.ROLE_CODE,BR.DESCRIPTION FROM USERSACTROLEALLOC BUA,ROLES BR WHERE BUA.ENTITY_CODE=? AND BUA.USER_ID=? AND BUA.ENABLED='1' AND BUA.FROM_DATE<=FN_GETCD(?) AND BUA.UPTO_DATE >=FN_GETCD(?) AND BUA.ENTITY_CODE=BR.ENTITY_CODE AND BUA.ROLE_CODE=BR.CODE AND BR.ENABLED ='1' ";
							util.setMode(DBUtil.PREPARED);
							util.setSql(actingRoleSql);
							util.setString(1, entityCode);
							util.setString(2, userID);
							util.setString(3, entityCode);
							util.setString(4, entityCode);
							ResultSet resultSet = util.executeQuery();
							while (resultSet.next()) {
								String value = resultSet.getString(2) + " (" + resultSet.getString(1) + ")";
								genericList.add(counter, new GenericOption(resultSet.getString(1), value));
								counter++;
							}
							util.reset();
						}
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			dbContext.close();
		}
		return genericList;
	}

	public ArrayList<GenericOption> getConsoleList(String roleType,String roleCode) {
		ArrayList<GenericOption> genericList = new ArrayList<GenericOption>();
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		try {
			// validate
			String entityCode = context.getEntityCode();
			DTObject result = validateRoleCode(roleCode);
			if (result.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
				DTObject result1 = validateRoleType(roleCode, roleType);
				if (result1.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
					String sqlQuery = "SELECT RC.CONSOLE_CODE,C.DESCRIPTION FROM ROLECONSOLEALLOCDTL RC,CONSOLE C WHERE RC.ENTITY_CODE = ? AND RC.ROLE_TYPE = ? AND RC.CONSOLE_CODE = C.CODE AND C.ENABLED='1' ORDER BY C.CODE ASC";
					util.setMode(DBUtil.PREPARED);
					util.setSql(sqlQuery);
					util.setString(1, entityCode);
					util.setString(2, roleType);



					ResultSet rs = util.executeQuery();
					int counter = 0;
					while (rs.next()) {
						ArrayList<GenericOption> programList = getProgramList(rs.getString(1), dbContext);
						if (!programList.isEmpty()) {
							genericList.add(counter, new GenericOption(rs.getString(1), rs.getString(2)));
							counter++;
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			util.reset();
			dbContext.close();
		}
		return genericList;
	}

	public ArrayList<GenericOption> getProgramList(String consoleCode, DBContext dbContext) {
		ArrayList<GenericOption> genericList = new ArrayList<GenericOption>();

		//String sqlQuery = "SELECT MM.MPGM_ID, MM.MPGM_DESCN FROM MPGM MM WHERE MM.MPGM_ID IN (SELECT R.PGM_ID FROM ROLEPGMALLOCDTL R,MPGM M WHERE R.ENTITY_CODE=? AND R.ROLE_CODE=? AND R.PGM_ID=M.MPGM_ID AND R.MODULE_ID=M.MPGM_MODULE AND R.PGM_ID IN (SELECT CPGM.PGM_ID FROM CONSOLEPGMALLOCDTL CPGM WHERE CPGM.ENTITY_CODE=? AND CPGM.CONSOLE_CODE =? ORDER BY CPGM.SL) ) AND MM.REQD_MENU='1' ";
		String sqlQuery = "SELECT MM.MPGM_ID, MM.MPGM_DESCN FROM MPGM MM JOIN ROLEPGMALLOCDTL R ON (R.ENTITY_CODE = ? AND R.ROLE_CODE = ? AND R.PGM_ID = MM.MPGM_ID AND R.MODULE_ID = MM.MPGM_MODULE ) JOIN CONSOLEPGMALLOCDTL CPGM ON (CPGM.ENTITY_CODE = ? AND CPGM.CONSOLE_CODE = ? AND R.PGM_ID = CPGM.PGM_ID) WHERE MM.REQD_MENU = '1' ORDER BY CPGM.SL";
		DBUtil util = dbContext.createUtilInstance();
		String entityCode = context.getEntityCode();
		String roleCode = context.getRoleCode();
		String roleType = context.getRoleType();
		if (roleCode != null && roleType != null) {
			try {
				// validate
				DTObject result = validateConsolePrograms(roleType, consoleCode);
				if (result.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
					util.setMode(DBUtil.PREPARED);
					util.setSql(sqlQuery);
					util.setString(1, entityCode);
					util.setString(2, roleCode);
					util.setString(3, entityCode);
					util.setString(4, consoleCode);
					ResultSet rs = util.executeQuery();
					int counter = 0;
					while (rs.next()) {
						genericList.add(counter, new GenericOption(rs.getString(1), rs.getString(2)));
						counter++;
					}
				} else {
					genericList.add(0, new GenericOption("", ""));
				}
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				util.reset();
			}
		}
		return genericList;
	}

	public ArrayList<GenericOption> getProgramList(String consoleCode) {
		ArrayList<GenericOption> genericList = null;
		DBContext dbContext = new DBContext();
		try {
			genericList = getProgramList(consoleCode, dbContext);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			dbContext.close();
		}
		return genericList;
	}

	public final int generateFUPSequence() {
		int serial = 0;
		DBContext dbContext = new DBContext();
		try {
			dbContext.setAutoCommit(false);
			DBUtil util = dbContext.createUtilInstance();
			String sqlQuery = "SELECT FN_NEXTVAL('SEQ_FREQUSEDPGM') AS INV_SEQ FROM DUAL";
			util.reset();
			util.setSql(sqlQuery);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				serial = rset.getInt(1);
			}
			dbContext.commit();
		} catch (Exception e) {
			e.printStackTrace();
			try {
				dbContext.rollback();
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		} finally {
			dbContext.close();
		}
		return serial;
	}

	public ArrayList<GenericOption> getFrequentlyUsedPrograms() {
		ArrayList<GenericOption> genericList = new ArrayList<GenericOption>();
		DBContext dbContext = new DBContext();
		try {
			dbContext.setAutoCommit(false);
			String entityCode = context.getEntityCode();
			String userID = context.getUserID();
			String roleCode = context.getRoleCode();
			DTObject result = validateRoleCode(roleCode);
			int serial = generateFUPSequence();
			if (result.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
				DBUtil util = dbContext.createUtilInstance();
				String prepareSql = "CALL SP_FREQUSEDPGM(?,?,?,?)";
				util.setMode(DBUtil.CALLABLE);
				util.setSql(prepareSql);
				util.setString(1, entityCode);
				util.setString(2, userID);
				util.setString(3, roleCode);
				util.setInt(4, serial);
				util.execute();
				util.reset();
				String readSql = "SELECT PGM_ID,DESCRIPTION FROM TMPFREQUENTPGM WHERE SL = ?";
				util.setMode(DBUtil.PREPARED);
				util.setSql(readSql);
				util.setInt(1, serial);
				ResultSet rs = util.executeQuery();
				while (rs.next()) {
					genericList.add(new GenericOption(rs.getString(1), rs.getString(2)));
				}
				util.reset();
				String deleteSql = "DELETE FROM TMPFREQUENTPGM WHERE SL = ?";
				util.setMode(DBUtil.PREPARED);
				util.setSql(deleteSql);
				util.setInt(1, serial);
				util.executeUpdate();
				util.reset();

				dbContext.commit();
			}
		} catch (Exception e) {
			try {
				dbContext.rollback();
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		} finally {
			dbContext.close();
		}
		return genericList;
	}
	
	public ArrayList<GenericOption> getRelatedPrograms() {
		ArrayList<GenericOption> genericList = new ArrayList<GenericOption>();
		DBContext dbContext = new DBContext();
		try {
			String roleCode = context.getRoleCode();
			DTObject result = validateRoleCode(roleCode);
			if (result.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
				DBUtil util = dbContext.createUtilInstance();
				StringBuffer readSql=new StringBuffer(RegularConstants.EMPTY_STRING);
				readSql.append("SELECT MR.PROGRAM_ID,M.MPGM_DESCN ");
				readSql.append("FROM MPGMRELATEDPGM MR ");
				readSql.append("JOIN MPGM M ON(M.MPGM_ID=MR.PROGRAM_ID) ");
				readSql.append("JOIN ROLEPGMALLOCDTL R ON(R.ENTITY_CODE=? AND R.ROLE_CODE=? AND R.PGM_ID=MR.PROGRAM_ID) ");
				readSql.append("WHERE MR.MPGM_ID=?");
				util.setMode(DBUtil.PREPARED);
				util.setSql(readSql.toString());
				util.setLong(1, Long.parseLong(context.getEntityCode()));
				util.setString(2, roleCode);
				util.setString(3, context.getProcessID());
				ResultSet rs = util.executeQuery();
				while (rs.next()) {
					genericList.add(new GenericOption(rs.getString(1), rs.getString(2)));
				}
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			dbContext.close();
		}
		return genericList;
	}

	public DTObject validateUser(String entityCode, String userID) {
		DTObject result = new DTObject();
		String sqlQuery = RegularConstants.EMPTY_STRING;
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		if (userID != RegularConstants.NULL) {
			try {
				sqlQuery = "SELECT 1 FROM USERS WHERE ENTITY_CODE=? AND USER_ID=?";
				util.reset();
				util.setSql(sqlQuery);
				util.setString(1, entityCode);
				util.setString(2, userID);
				ResultSet rset = util.executeQuery();
				if (rset.next()) {
					result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				} else {
					result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
				}
			} catch (Exception e) {
				result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
			} finally {
				util.reset();
				dbContext.close();
			}
		}
		return result;
	}

	public DTObject validateConsolePrograms(String roleType, String consoleCode) {
		DTObject result = new DTObject();
		String sqlQuery = RegularConstants.EMPTY_STRING;
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		try {
			String entityCode = context.getEntityCode();
			sqlQuery = "SELECT 1 FROM ROLECONSOLEALLOCDTL WHERE ENTITY_CODE=? AND ROLE_TYPE=? AND CONSOLE_CODE =?";
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, entityCode);
			util.setString(2, roleType);
			util.setString(3, consoleCode);



			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
			} else {
				result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
			}
		} catch (Exception e) {
			result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		} finally {
			util.reset();
			dbContext.close();
		}
		return result;
	}

	public DTObject validateRoleType(String roleCode, String roleType) {
		DTObject result = new DTObject();
		String sqlQuery = RegularConstants.EMPTY_STRING;
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		try {
			String entityCode = context.getEntityCode();
			sqlQuery = "SELECT 1 FROM ROLES WHERE ENTITY_CODE=? AND CODE=? AND ROLE_TYPE=? AND ENABLED='1'";
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, entityCode);
			util.setString(2, roleCode);
			util.setString(3, roleType);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
			} else {
				result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
			}
		} catch (Exception e) {
			result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		} finally {
			util.reset();
			dbContext.close();
		}
		return result;
	}

	public ArrayList<GenericOption> filterSmartNavigation(String programID) {

		ArrayList<GenericOption> genericList = new ArrayList<GenericOption>();
		DBContext dbContext = new DBContext();
		String entityCode = context.getEntityCode();
		String roleCode = context.getRoleCode();
		String userValue = "%" + programID.toLowerCase(context.getLocale()) + "%";
		int counter = 0;
		DTObject result = validateRoleCode(roleCode);
		if (result.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
			try {

				String sqlQuery = "SELECT M.MPGM_ID, M.MPGM_DESCN FROM MPGM M WHERE M.MPGM_ID IN (SELECT PGM_ID FROM ROLEPGMALLOCDTL RPT WHERE RPT.ENTITY_CODE=? AND RPT.ROLE_CODE = ?) AND M.REQD_MENU = '1' AND (LOWER(M.MPGM_ID) LIKE ? OR LOWER(M.MPGM_DESCN) LIKE ?)";
				DBUtil util = dbContext.createUtilInstance();
				util.setMode(DBUtil.PREPARED);
				util.setSql(sqlQuery);
				util.setString(1, entityCode);
				util.setString(2, roleCode);
				util.setString(3, userValue);










				util.setString(4, userValue);
				ResultSet rs = util.executeQuery();
				while (rs.next()) {
					genericList.add(counter, new GenericOption(rs.getString(1), rs.getString(2)));
					counter++;
				}
				util.reset();
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				dbContext.close();
			}
		}
		genericList.trimToSize();
		return genericList;
	}

	public DTObject validateRoleCode(String hidRoleCode) {
		DTObject result = new DTObject();
		String sqlQuery = RegularConstants.EMPTY_STRING;
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);

		try (DBContext dbContext = new DBContext()) {
			DBUtil util = dbContext.createUtilInstance();
			String entityCode = context.getEntityCode();
			String userID = context.getUserID();
			sqlQuery = "SELECT 1 FROM ROLES BR WHERE BR.ENTITY_CODE = ? AND BR.CODE= FN_GETUSERSROLECD(?,?,?) AND BR.CODE = ? AND BR.ENABLED='1'";
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, entityCode);
			util.setString(2, context.getPartitionNo());
			util.setString(3, entityCode);
			util.setString(4, userID);
			util.setString(5, hidRoleCode);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				return result;
			}
			util.reset();
			sqlQuery = "SELECT 1 FROM USERSACTROLEALLOC BUA,ROLES BR WHERE BUA.ENTITY_CODE=? AND BUA.USER_ID=? AND BUA.ROLE_CODE = ? AND BUA.FROM_DATE <=FN_GETCD(?) AND BUA.UPTO_DATE>=FN_GETCD(?) AND BUA.ENABLED ='1' AND BUA.ENTITY_CODE  =BR.ENTITY_CODE AND BUA.ROLE_CODE=BR.CODE AND BR.ENABLED= '1'";
			util.setSql(sqlQuery);
			util.setString(1, entityCode);
			util.setString(2, userID);
			util.setString(3, hidRoleCode);
			util.setString(4, entityCode);
			util.setString(5, entityCode);
			ResultSet resultSet = util.executeQuery();
			if (resultSet.next()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
			}
			util.reset();
			return result;
		} catch (Exception e) {
			result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		}
		return result;
	}

	public static String resolveForward(String forwardName, HttpServletRequest request) {
		ModuleConfig config = ModuleUtils.getInstance().getModuleConfig(null, request, request.getServletContext());
		return config.findForwardConfig(forwardName).getPath().replaceFirst("/", "");
	}
}