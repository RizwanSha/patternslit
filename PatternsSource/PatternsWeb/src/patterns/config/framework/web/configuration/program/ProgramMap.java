package patterns.config.framework.web.configuration.program;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.loggers.ApplicationLogger;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.RequestConstants;
import patterns.json.JSONObject;

public class ProgramMap {
	public static final String PROGRAM_CLASS_CUSTOM = "0";
	public static final String PROGRAM_CLASS_MASTER = "1";
	public static final String PROGRAM_CLASS_ENTRY = "2";
	public static final String PROGRAM_CLASS_PARAMTER = "3";
	public static final String PROGRAM_CLASS_QUERY = "4";
	public static final String PROGRAM_CLASS_AUTHORIZATION = "5";
	public static final String PROGRAM_CLASS_VIEW = "6";
	public static final String PROGRAM_CLASS_UTILITY = "7";
	public static final String PROGRAM_CLASS_UI = "8";
	public static final String PROGRAM_CLASS_FORM_FULLSCREEN = "9";

	public static final String ELOGIN = "ELOGIN";
	public static final String ELOGINAUTH = "ELOGINAUTH";
	public static final String EUNAUTHACCESS = "EUNAUTHACCESS";
	public static final String ELOGOUT = "ELOGOUT";
	public static final String ERESULT = "ERESULT";
	public static final String EFORCEPWDCHGN = "EFORCEPWDCHGN";
	public static final String EPWDCHGN = "EPWDCHGN";
	public static final String ETFAUTH = "ETFAUTH";
	public static final String EFREQUSEDPGM = "EFREQUSEDPGM";
	public static final String ECONSOLENAV = "ECONSOLENAV";
	public static final String ACCORDMENUCONSOLE = "ACCORDMENUCONSOLE";
	public static final String ESMARTNAV = "ESMARTNAV";
	public static final String QNTFNVIEW = "QNTFNVIEW";
	public static final String EROLEUPDATION = "EROLEUPDATION";
	public static final String ELANDING = "ELANDING";
	public static final String QCERTIFICATEINFO = "QCERTIFICATEINFO";
	public static final String EUSERPREFERENCE = "EUSERPREFERENCE";
	
	public static final String LOOKUPPROCESSOR = "LOOKUPPROCESSOR";
	public static final String SMARTLOOKUPPROCESSOR = "SMARTLOOKUPPROCESSOR";
	public static final String RECORDFINDERPROCESSOR = "RECORDFINDERPROCESSOR";
	public static final String QUERYVIEWPROCESSOR = "QUERYVIEWPROCESSOR";
	public static final String TBAPROCESSOR = "TBAPROCESSOR";
	public static final String MTMPROCESSOR = "MTMPROCESSOR";
	public static final String COMPONENTMTMPROCESSOR = "COMPONENTMTMPROCESSOR";
	public static final String DATAPROCESSOR = "DATAPROCESSOR";
	public static final String GRIDQUERYPROCESSOR = "GRIDQUERYPROCESSOR";
	public static final String COMPONENTGRIDQUERYPROCESSOR = "COMPONENTGRIDQUERYPROCESSOR";
	public static final String RECORDQUERYPROCESSOR = "RECORDQUERYPROCESSOR";
	public static final String COMPONENTRECORDQUERYPROCESSOR = "COMPONENTRECORDQUERYPROCESSOR";
	public static final String FILEUPLOADIDPROCESSOR = "FILEUPLOADIDPROCESSOR";
	public static final String FILEUPLOADINFOPROCESSOR = "FILEUPLOADINFOPROCESSOR";
	public static final String FILEUPLOADPROCESSOR = "FILEUPLOADPROCESSOR";
	public static final String IMAGEDOWNLOADPROCESSOR = "IMAGEDOWNLOADPROCESSOR";
	public static final String FORMVALIDATIONPROCESSOR = "FORMVALIDATIONPROCESSOR";
	public static final String TRANSACTIONRESPONSEPROCESSOR = "TRANSACTIONRESPONSEPROCESSOR";
	public static final String REPORTDOWNLOADPROCESSOR = "REPORTDOWNLOADPROCESSOR";
	public static final String FILEDOWNLOADPROCESSOR = "FILEDOWNLOADPROCESSOR";

	private static Set<String> nonSessionSet = new HashSet<String>();
	static {
		nonSessionSet.add(ELOGIN);
		nonSessionSet.add(EUNAUTHACCESS);
	}

	private static Set<String> nonRefererCheckSet = new HashSet<String>();
	static {
		nonRefererCheckSet.add(ELOGIN);
		nonRefererCheckSet.add(ELOGOUT);
		nonRefererCheckSet.add(EUNAUTHACCESS);
	}

	private static Set<String> nonOperationLogSet = new HashSet<String>();
	static {
		nonOperationLogSet.add(ELOGIN);
		nonOperationLogSet.add(ELOGINAUTH);
		nonOperationLogSet.add(ELOGOUT);
		nonOperationLogSet.add(EUNAUTHACCESS);
		nonOperationLogSet.add(ERESULT);
		nonOperationLogSet.add(ECONSOLENAV);
		nonOperationLogSet.add(ACCORDMENUCONSOLE);
		
		nonOperationLogSet.add(ESMARTNAV);
		nonOperationLogSet.add(EFREQUSEDPGM);
		nonOperationLogSet.add(ETFAUTH);
	}

	private static Set<String> nonAllocationSet = new HashSet<String>();
	static {
		nonAllocationSet.add(ELOGIN);
		nonAllocationSet.add(ELOGINAUTH);
		nonAllocationSet.add(EUNAUTHACCESS);
		nonAllocationSet.add(ELOGOUT);
		nonAllocationSet.add(ERESULT);
		nonAllocationSet.add(EFORCEPWDCHGN);
		nonAllocationSet.add(EPWDCHGN);
		nonAllocationSet.add(ETFAUTH);
		nonAllocationSet.add(EFREQUSEDPGM);
		nonAllocationSet.add(ECONSOLENAV);
		nonAllocationSet.add(ACCORDMENUCONSOLE);
		nonAllocationSet.add(ESMARTNAV);
		nonAllocationSet.add(QNTFNVIEW);
		nonAllocationSet.add(EROLEUPDATION);
		nonAllocationSet.add(ELANDING);
		nonAllocationSet.add(QCERTIFICATEINFO);
		nonAllocationSet.add(EUSERPREFERENCE);
	}

	private static Set<String> forcedPasswordContextSet = new HashSet<String>();
	static {
		forcedPasswordContextSet.add(EFORCEPWDCHGN);
	}

	private static Set<String> loginAuthenticationContextSet = new HashSet<String>();
	static {
		loginAuthenticationContextSet.add(ELOGINAUTH);
	}

	private static Set<String> skipCSRFAttackCheckSet = new HashSet<String>();
	static {
		skipCSRFAttackCheckSet.add(EROLEUPDATION);
		skipCSRFAttackCheckSet.add(EUSERPREFERENCE);
	}

	private static Set<String> credentialManagementOptionSet = new HashSet<String>();
	static {
		credentialManagementOptionSet.add(ELOGINAUTH);
		credentialManagementOptionSet.add(EFORCEPWDCHGN);
		credentialManagementOptionSet.add(EPWDCHGN);
	}

	private static final ReentrantReadWriteLock readWriteLock = new ReentrantReadWriteLock();

	// private static final Lock read = readWriteLock.readLock();

	private static final Lock write = readWriteLock.writeLock();

	private static final ReentrantReadWriteLock readWriteLockGlobal = new ReentrantReadWriteLock();

	private static final Lock writeGlobal = readWriteLockGlobal.writeLock();

	private ApplicationLogger logger = null;

	private Map<String, ProgramConfiguration> internalMap;

	private Map<String, ProgramConfiguration> internalLocalMap;

	private static volatile ProgramMap instance = null;

	private ProgramMap() {
		logger = ApplicationLogger.getInstance(this.getClass().getSimpleName());
		// logger.logDebug("#()");
	}

	public ProgramConfiguration getConfiguration(String programID) {
		// logger.logDebug("getConfiguration()");
		ProgramConfiguration configuration = null;
		try {
			// read.lock();
			configuration = internalMap.get(programID);
			if (configuration == null) {
				logger.logError("getConfiguration(1): empty configuration : " + programID);
				return null;
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError(" getConfiguration(2) - " + e.getLocalizedMessage());
		} finally {
			// read.unlock();
		}
		return configuration;
	}

	private void initConfiguration() {
		internalMap.putAll(internalLocalMap);
		internalLocalMap.clear();
		logger.logDebug("initConfiguration() " + internalMap);
	}

	private void resetConfiguration() {
		logger.logDebug("resetConfiguration()");
		if (internalMap == null)
			internalMap = new HashMap<String, ProgramConfiguration>(400, 1F);
		internalMap.clear();
	}

	public static void unloadConfiguration() {
		try {
			writeGlobal.lock();
			if (instance != null) {
				instance.resetConfiguration();
				instance = null;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			writeGlobal.unlock();
		}
	}

	public static void reloadConfiguration() {
		try {
			writeGlobal.lock();
			if (instance == null) {
				instance = new ProgramMap();
			}
			instance.loadConfiguration();
			try {
				write.lock();
				instance.resetConfiguration();
				instance.initConfiguration();
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				write.unlock();
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			writeGlobal.unlock();
		}
	}

	private void loadConfiguration() {
		logger.logDebug("loadConfiguration()");
		DBContext dbContext = new DBContext();
		if (internalLocalMap == null)
			internalLocalMap = new HashMap<String, ProgramConfiguration>(400, 1F);
		internalLocalMap.clear();
		try {
			setSchemaName();
			logger.logDebug("loadConfiguration() : Before Query Execution");
			DBUtil util = dbContext.createUtilInstance();
			util.reset();
			String sqlQuery = "SELECT MPGM_ID, MPGM_DESCN,MPGM_CLASS, TFA_REQ,MPGM_QUERY_PATH ,BACK_TO_TEMPLATE_ALLOWED,PRINT_REQUIRED FROM MPGM M  ORDER BY MPGM_MODULE, MPGM_SPEC_ID";
			util.setSql(sqlQuery);
			ResultSet rs = util.executeQuery();
			logger.logDebug("loadConfiguration() : Query Executed");
			while (rs.next()) {
				ProgramConfiguration programConfiguration = null;
				String programID = rs.getString("MPGM_ID");
				logger.logDebug("loadConfiguration() : Program ID : " + programID + " begins");
				String programDescription = rs.getString("MPGM_DESCN");
				String programClass = rs.getString("MPGM_CLASS");
				String rendererType = RequestConstants.RENDERER_TYPE_UTILITY;
				String queryProgramID = rs.getString("MPGM_QUERY_PATH");
				if (programClass.equals(PROGRAM_CLASS_MASTER) || programClass.equals(PROGRAM_CLASS_ENTRY) || programClass.equals(PROGRAM_CLASS_PARAMTER) || programClass.equals(PROGRAM_CLASS_QUERY) || programClass.equals(PROGRAM_CLASS_AUTHORIZATION)) {
					rendererType = RequestConstants.RENDERER_TYPE_FORM;
				} else if (programClass.equals(PROGRAM_CLASS_FORM_FULLSCREEN)) {
					rendererType = RequestConstants.RENDERER_TYPE_FORM_FULLSCREEEN;
				} else if (programClass.equals(PROGRAM_CLASS_VIEW)) {
					rendererType = RequestConstants.RENDERER_TYPE_VIEW;
				} else if (programClass.equals(PROGRAM_CLASS_CUSTOM)) {
					rendererType = RequestConstants.RENDERER_TYPE_CUSTOM;
				} else if (programClass.equals(PROGRAM_CLASS_UI)) {
					rendererType = RequestConstants.RENDERER_TYPE_UTILITY;
				}
				boolean tfaRequired = ((rs.getString("TFA_REQ") == null) ? false : (rs.getString("TFA_REQ").equals(RegularConstants.COLUMN_ENABLE) ? true : false));
				boolean operationLogRequired = !nonOperationLogSet.contains(programID);
				boolean sessionRequired = !nonSessionSet.contains(programID);
				boolean allocationRequired = !nonAllocationSet.contains(programID);
				boolean forcedPasswordContext = forcedPasswordContextSet.contains(programID);
				boolean loginAuthenticationContext = loginAuthenticationContextSet.contains(programID);
				boolean skipCSRFAttackCheck = skipCSRFAttackCheckSet.contains(programID);
				boolean refererCheckRequired = !nonRefererCheckSet.contains(programID);
				boolean credentialManagementOption = credentialManagementOptionSet.contains(programID);
				boolean backToTemplate= (rs.getString("BACK_TO_TEMPLATE_ALLOWED").equals(RegularConstants.COLUMN_ENABLE) ? true : false);
				boolean printRequired= (rs.getString("PRINT_REQUIRED").equals(RegularConstants.COLUMN_ENABLE) ? true : false);
				
				programConfiguration = new ProgramConfiguration();
				programConfiguration.setProgramID(programID);
				programConfiguration.setProgramDescription(programDescription);
				programConfiguration.setProgramClass(programClass);
				programConfiguration.setQueryProgramID(queryProgramID);
				programConfiguration.setRendererType(rendererType);
				programConfiguration.setTfaRequired(tfaRequired);
				programConfiguration.setOperationLogRequired(operationLogRequired);
				programConfiguration.setSessionRequired(sessionRequired);
				programConfiguration.setAllocationRequired(allocationRequired);
				programConfiguration.setForcedPasswordContext(forcedPasswordContext);
				programConfiguration.setLoginAuthenticationContext(loginAuthenticationContext);
				programConfiguration.setSkipCSRFAttackCheck(skipCSRFAttackCheck);
				programConfiguration.setRefererCheckRequired(refererCheckRequired);
				programConfiguration.setCredentialManagementOption(credentialManagementOption);
				programConfiguration.setBackToTemplate(backToTemplate);
				programConfiguration.setPrintRequired(printRequired);
				internalLocalMap.put(programID, programConfiguration);
				logger.logDebug("loadConfiguration() : Program ID : " + programID + " ends");
			}
			util.reset();

			ProgramConfiguration lookupProcessorConfiguration = new ProgramConfiguration();
			lookupProcessorConfiguration.setProgramID(LOOKUPPROCESSOR);
			lookupProcessorConfiguration.setProgramDescription("LookupProcessor Servlet");
			lookupProcessorConfiguration.setProgramClass(PROGRAM_CLASS_UTILITY);
			lookupProcessorConfiguration.setRendererType(RequestConstants.RENDERER_TYPE_UTILITY);
			lookupProcessorConfiguration.setTfaRequired(false);
			lookupProcessorConfiguration.setOperationLogRequired(false);
			lookupProcessorConfiguration.setSessionRequired(true);
			lookupProcessorConfiguration.setAllocationRequired(false);
			internalLocalMap.put(LOOKUPPROCESSOR, lookupProcessorConfiguration);

			ProgramConfiguration smartLookupProcessorConfiguration = new ProgramConfiguration();
			smartLookupProcessorConfiguration.setProgramID(SMARTLOOKUPPROCESSOR);
			smartLookupProcessorConfiguration.setProgramDescription("Smart LookupProcessor Servlet");
			smartLookupProcessorConfiguration.setProgramClass(PROGRAM_CLASS_UTILITY);
			smartLookupProcessorConfiguration.setRendererType(RequestConstants.RENDERER_TYPE_UTILITY);
			smartLookupProcessorConfiguration.setTfaRequired(false);
			smartLookupProcessorConfiguration.setOperationLogRequired(false);
			smartLookupProcessorConfiguration.setSessionRequired(true);
			smartLookupProcessorConfiguration.setAllocationRequired(false);
			internalLocalMap.put(SMARTLOOKUPPROCESSOR, smartLookupProcessorConfiguration);

			
			ProgramConfiguration recordFinderProcessorConfiguration = new ProgramConfiguration();
			recordFinderProcessorConfiguration.setProgramID(RECORDFINDERPROCESSOR);
			recordFinderProcessorConfiguration.setProgramDescription("RecordFinderProcessor Servlet");
			recordFinderProcessorConfiguration.setProgramClass(PROGRAM_CLASS_UTILITY);
			recordFinderProcessorConfiguration.setRendererType(RequestConstants.RENDERER_TYPE_UTILITY);
			recordFinderProcessorConfiguration.setTfaRequired(false);
			recordFinderProcessorConfiguration.setOperationLogRequired(false);
			recordFinderProcessorConfiguration.setRefererCheckRequired(true);
			recordFinderProcessorConfiguration.setSessionRequired(true);
			recordFinderProcessorConfiguration.setAllocationRequired(false);
			internalLocalMap.put(RECORDFINDERPROCESSOR, recordFinderProcessorConfiguration);
			
			ProgramConfiguration queryViewProcessorConfiguration = new ProgramConfiguration();
			queryViewProcessorConfiguration.setProgramID(QUERYVIEWPROCESSOR);
			queryViewProcessorConfiguration.setProgramDescription("QueryViewProcessor Servlet");
			queryViewProcessorConfiguration.setProgramClass(PROGRAM_CLASS_UTILITY);
			queryViewProcessorConfiguration.setRendererType(RequestConstants.RENDERER_TYPE_UTILITY);
			queryViewProcessorConfiguration.setTfaRequired(false);
			queryViewProcessorConfiguration.setOperationLogRequired(false);
			queryViewProcessorConfiguration.setRefererCheckRequired(true);
			queryViewProcessorConfiguration.setSessionRequired(true);
			queryViewProcessorConfiguration.setAllocationRequired(false);
			internalLocalMap.put(QUERYVIEWPROCESSOR, queryViewProcessorConfiguration);

			ProgramConfiguration tbaProcessorConfiguration = new ProgramConfiguration();
			tbaProcessorConfiguration.setProgramID(TBAPROCESSOR);
			tbaProcessorConfiguration.setProgramDescription("TBAProcessor Servlet");
			tbaProcessorConfiguration.setProgramClass(PROGRAM_CLASS_UTILITY);
			tbaProcessorConfiguration.setRendererType(RequestConstants.RENDERER_TYPE_UTILITY);
			tbaProcessorConfiguration.setTfaRequired(false);
			tbaProcessorConfiguration.setOperationLogRequired(false);
			tbaProcessorConfiguration.setSessionRequired(true);
			tbaProcessorConfiguration.setAllocationRequired(false);
			internalLocalMap.put(TBAPROCESSOR, tbaProcessorConfiguration);

			ProgramConfiguration mtmProcessorConfiguration = new ProgramConfiguration();
			mtmProcessorConfiguration.setProgramID(MTMPROCESSOR);
			mtmProcessorConfiguration.setProgramDescription("MTMProcessor Servlet");
			mtmProcessorConfiguration.setProgramClass(PROGRAM_CLASS_UTILITY);
			mtmProcessorConfiguration.setRendererType(RequestConstants.RENDERER_TYPE_UTILITY);
			mtmProcessorConfiguration.setTfaRequired(false);
			mtmProcessorConfiguration.setOperationLogRequired(false);
			mtmProcessorConfiguration.setSessionRequired(true);
			mtmProcessorConfiguration.setAllocationRequired(false);
			internalLocalMap.put(MTMPROCESSOR, mtmProcessorConfiguration);
			
			ProgramConfiguration componentMtmProcessorConfiguration = new ProgramConfiguration();
			componentMtmProcessorConfiguration.setProgramID(COMPONENTMTMPROCESSOR);
			componentMtmProcessorConfiguration.setProgramDescription("ComponentMTMProcessor Servlet");
			componentMtmProcessorConfiguration.setProgramClass(PROGRAM_CLASS_UTILITY);
			componentMtmProcessorConfiguration.setRendererType(RequestConstants.RENDERER_TYPE_UTILITY);
			componentMtmProcessorConfiguration.setTfaRequired(false);
			componentMtmProcessorConfiguration.setOperationLogRequired(false);
			componentMtmProcessorConfiguration.setSessionRequired(true);
			componentMtmProcessorConfiguration.setAllocationRequired(false);
			internalLocalMap.put(COMPONENTMTMPROCESSOR, componentMtmProcessorConfiguration);

			ProgramConfiguration dataProcessorConfiguration = new ProgramConfiguration();
			dataProcessorConfiguration.setProgramID(DATAPROCESSOR);
			dataProcessorConfiguration.setProgramDescription("DataProcessor Servlet");
			dataProcessorConfiguration.setProgramClass(PROGRAM_CLASS_UTILITY);
			dataProcessorConfiguration.setRendererType(RequestConstants.RENDERER_TYPE_UTILITY);
			dataProcessorConfiguration.setTfaRequired(false);
			dataProcessorConfiguration.setOperationLogRequired(false);
			dataProcessorConfiguration.setSessionRequired(true);
			dataProcessorConfiguration.setAllocationRequired(false);
			internalLocalMap.put(DATAPROCESSOR, dataProcessorConfiguration);

			ProgramConfiguration gridQueryProcessorConfiguration = new ProgramConfiguration();
			gridQueryProcessorConfiguration.setProgramID(GRIDQUERYPROCESSOR);
			gridQueryProcessorConfiguration.setProgramDescription("GridQueryProcessor Servlet");
			gridQueryProcessorConfiguration.setProgramClass(PROGRAM_CLASS_UTILITY);
			gridQueryProcessorConfiguration.setRendererType(RequestConstants.RENDERER_TYPE_UTILITY);
			gridQueryProcessorConfiguration.setTfaRequired(false);
			gridQueryProcessorConfiguration.setOperationLogRequired(false);
			gridQueryProcessorConfiguration.setSessionRequired(true);
			gridQueryProcessorConfiguration.setAllocationRequired(false);
			internalLocalMap.put(GRIDQUERYPROCESSOR, gridQueryProcessorConfiguration);
			
			ProgramConfiguration componentGridQueryProcessorConfiguration = new ProgramConfiguration();
			componentGridQueryProcessorConfiguration.setProgramID(COMPONENTGRIDQUERYPROCESSOR);
			componentGridQueryProcessorConfiguration.setProgramDescription("ComponentGridQueryProcessor Servlet");
			componentGridQueryProcessorConfiguration.setProgramClass(PROGRAM_CLASS_UTILITY);
			componentGridQueryProcessorConfiguration.setRendererType(RequestConstants.RENDERER_TYPE_UTILITY);
			componentGridQueryProcessorConfiguration.setTfaRequired(false);
			componentGridQueryProcessorConfiguration.setOperationLogRequired(false);
			componentGridQueryProcessorConfiguration.setSessionRequired(true);
			componentGridQueryProcessorConfiguration.setAllocationRequired(false);
			internalLocalMap.put(COMPONENTGRIDQUERYPROCESSOR, componentGridQueryProcessorConfiguration);

			ProgramConfiguration recordQueryProcessorConfiguration = new ProgramConfiguration();
			recordQueryProcessorConfiguration.setProgramID(RECORDQUERYPROCESSOR);
			recordQueryProcessorConfiguration.setProgramDescription("RecordQueryProcessor Servlet");
			recordQueryProcessorConfiguration.setProgramClass(PROGRAM_CLASS_UTILITY);
			recordQueryProcessorConfiguration.setRendererType(RequestConstants.RENDERER_TYPE_UTILITY);
			recordQueryProcessorConfiguration.setTfaRequired(false);
			recordQueryProcessorConfiguration.setOperationLogRequired(false);
			recordQueryProcessorConfiguration.setSessionRequired(true);
			recordQueryProcessorConfiguration.setAllocationRequired(false);
			internalLocalMap.put(RECORDQUERYPROCESSOR, recordQueryProcessorConfiguration);
			
			ProgramConfiguration componentRecordQueryProcessorConfiguration = new ProgramConfiguration();
			componentRecordQueryProcessorConfiguration.setProgramID(COMPONENTRECORDQUERYPROCESSOR);
			componentRecordQueryProcessorConfiguration.setProgramDescription("ComponentRecordQueryProcessor Servlet");
			componentRecordQueryProcessorConfiguration.setProgramClass(PROGRAM_CLASS_UTILITY);
			componentRecordQueryProcessorConfiguration.setRendererType(RequestConstants.RENDERER_TYPE_UTILITY);
			componentRecordQueryProcessorConfiguration.setTfaRequired(false);
			componentRecordQueryProcessorConfiguration.setOperationLogRequired(false);
			componentRecordQueryProcessorConfiguration.setSessionRequired(true);
			componentRecordQueryProcessorConfiguration.setAllocationRequired(false);
			internalLocalMap.put(COMPONENTRECORDQUERYPROCESSOR, componentRecordQueryProcessorConfiguration);

			ProgramConfiguration fileUploadIDProcessor = new ProgramConfiguration();
			fileUploadIDProcessor.setProgramID(FILEUPLOADIDPROCESSOR);
			fileUploadIDProcessor.setProgramDescription("FileUploadIDProcessor Servlet");
			fileUploadIDProcessor.setProgramClass(PROGRAM_CLASS_UTILITY);
			fileUploadIDProcessor.setRendererType(RequestConstants.RENDERER_TYPE_UTILITY);
			fileUploadIDProcessor.setTfaRequired(false);
			fileUploadIDProcessor.setOperationLogRequired(false);
			fileUploadIDProcessor.setSessionRequired(true);
			fileUploadIDProcessor.setAllocationRequired(false);
			internalLocalMap.put(FILEUPLOADIDPROCESSOR, fileUploadIDProcessor);

			ProgramConfiguration fileUploadInfoProcessor = new ProgramConfiguration();
			fileUploadInfoProcessor.setProgramID(FILEUPLOADINFOPROCESSOR);
			fileUploadInfoProcessor.setProgramDescription("FileUploaInfoProcessor Servlet");
			fileUploadInfoProcessor.setProgramClass(PROGRAM_CLASS_UTILITY);
			fileUploadInfoProcessor.setRendererType(RequestConstants.RENDERER_TYPE_UTILITY);
			fileUploadInfoProcessor.setTfaRequired(false);
			fileUploadInfoProcessor.setOperationLogRequired(false);
			fileUploadInfoProcessor.setSessionRequired(true);
			fileUploadInfoProcessor.setAllocationRequired(false);
			internalLocalMap.put(FILEUPLOADINFOPROCESSOR, fileUploadInfoProcessor);

			ProgramConfiguration imageDownloadProcessor = new ProgramConfiguration();
			imageDownloadProcessor.setProgramID(IMAGEDOWNLOADPROCESSOR);
			imageDownloadProcessor.setProgramDescription("ImageDownloadProcessor Servlet");
			imageDownloadProcessor.setProgramClass(PROGRAM_CLASS_UTILITY);
			imageDownloadProcessor.setRendererType(RequestConstants.RENDERER_TYPE_UTILITY);
			imageDownloadProcessor.setTfaRequired(false);
			imageDownloadProcessor.setOperationLogRequired(false);
			imageDownloadProcessor.setSessionRequired(true);
			imageDownloadProcessor.setAllocationRequired(false);
			internalLocalMap.put(IMAGEDOWNLOADPROCESSOR, imageDownloadProcessor);
			
			ProgramConfiguration formValidationProcessor = new ProgramConfiguration();
			imageDownloadProcessor.setProgramID(IMAGEDOWNLOADPROCESSOR);
			imageDownloadProcessor.setProgramDescription("formValidationProcessor Servlet");
			imageDownloadProcessor.setProgramClass(PROGRAM_CLASS_UTILITY);
			imageDownloadProcessor.setRendererType(RequestConstants.RENDERER_TYPE_UTILITY);
			imageDownloadProcessor.setTfaRequired(false);
			imageDownloadProcessor.setOperationLogRequired(false);
			imageDownloadProcessor.setSessionRequired(true);
			imageDownloadProcessor.setAllocationRequired(false);
			internalLocalMap.put(FORMVALIDATIONPROCESSOR, formValidationProcessor);
			
			ProgramConfiguration transactionResponseProcessor = new ProgramConfiguration();
			imageDownloadProcessor.setProgramID(IMAGEDOWNLOADPROCESSOR);
			imageDownloadProcessor.setProgramDescription("transactionResponseProcessor Servlet");
			imageDownloadProcessor.setProgramClass(PROGRAM_CLASS_UTILITY);
			imageDownloadProcessor.setRendererType(RequestConstants.RENDERER_TYPE_UTILITY);
			imageDownloadProcessor.setTfaRequired(false);
			imageDownloadProcessor.setOperationLogRequired(false);
			imageDownloadProcessor.setSessionRequired(true);
			imageDownloadProcessor.setAllocationRequired(false);
			internalLocalMap.put(TRANSACTIONRESPONSEPROCESSOR, transactionResponseProcessor);
			
			
			ProgramConfiguration fileUploadProcessor = new ProgramConfiguration();
			fileUploadProcessor.setProgramID(FILEUPLOADPROCESSOR);
			fileUploadProcessor.setProgramDescription("FileUploaInfoProcessor Servlet");
			fileUploadProcessor.setProgramClass(PROGRAM_CLASS_UTILITY);
			fileUploadProcessor.setRendererType(RequestConstants.RENDERER_TYPE_UTILITY);
			fileUploadProcessor.setTfaRequired(false);
			fileUploadProcessor.setOperationLogRequired(false);
			fileUploadProcessor.setSessionRequired(true);
			fileUploadProcessor.setAllocationRequired(false);
			internalLocalMap.put(FILEUPLOADPROCESSOR, fileUploadProcessor);

			ProgramConfiguration reportDownloadProcessorConfiguration = new ProgramConfiguration();
			reportDownloadProcessorConfiguration.setProgramID(REPORTDOWNLOADPROCESSOR);
			reportDownloadProcessorConfiguration.setProgramDescription("ReportDownloadProcessor Servlet");
			reportDownloadProcessorConfiguration.setProgramClass(PROGRAM_CLASS_UTILITY);
			reportDownloadProcessorConfiguration.setRendererType(RequestConstants.RENDERER_TYPE_UTILITY);
			reportDownloadProcessorConfiguration.setTfaRequired(false);
			reportDownloadProcessorConfiguration.setOperationLogRequired(false);
			reportDownloadProcessorConfiguration.setSessionRequired(true);
			reportDownloadProcessorConfiguration.setAllocationRequired(false);
			internalLocalMap.put(REPORTDOWNLOADPROCESSOR, reportDownloadProcessorConfiguration);
			
			ProgramConfiguration fileDownloadProcessor = new ProgramConfiguration();
			fileDownloadProcessor.setProgramID(FILEDOWNLOADPROCESSOR);
			fileDownloadProcessor.setProgramDescription("FileDownloadInfoProcessor Servlet");
			fileDownloadProcessor.setProgramClass(PROGRAM_CLASS_UTILITY);
			fileDownloadProcessor.setRendererType(RequestConstants.RENDERER_TYPE_UTILITY);
			fileDownloadProcessor.setTfaRequired(false);
			fileDownloadProcessor.setOperationLogRequired(false);
			fileDownloadProcessor.setSessionRequired(true);
			fileDownloadProcessor.setAllocationRequired(false);
			internalLocalMap.put(FILEDOWNLOADPROCESSOR, fileDownloadProcessor);

			setFormStyle(util);
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError(" loadConfiguration(1) - " + e.getLocalizedMessage());
		} finally {
			dbContext.close();
		}
	}
	
	private void setFormStyle(DBUtil util) throws SQLException{
		JSONObject formStyleConfigData=null;
		JSONObject formStyle=null;
		JSONObject elementStyleMapping=null;
		try{
			 util.reset();
			 util.setSql("SELECT FILE_DATA FORM_STYLE_DATA FROM CMNFILEINVENTORY WHERE ENTITY_CODE=0 AND FILE_INV_NUM=0");
			 ResultSet rset = util.executeQuery();
			 if (rset.next()) {
				formStyleConfigData=new JSONObject(rset.getString("FORM_STYLE_DATA"));
			 }
			
			if(formStyleConfigData!=null){
				util.reset();
				util.setSql("SELECT PROGRAM_ID,ELEMENT_ID,CLASS_NAME FROM FORMSTYLECONFIG ORDER BY 1");
				rset = util.executeQuery();
				ProgramConfiguration programConfiguration=null;
				if (rset.next()) {
					do{
						System.out.println("Element id "+rset.getString("ELEMENT_ID"));
						programConfiguration=internalLocalMap.get(rset.getString("PROGRAM_ID"));
						System.out.println("CLASS_NAME id "+rset.getString("CLASS_NAME"));
						if(formStyleConfigData.get(rset.getString("CLASS_NAME"))!=null){
							if(programConfiguration.getFormStyle()==null)
								formStyle=new JSONObject();
							else
								formStyle=programConfiguration.getFormStyle();
							
							if(programConfiguration.getElementStyleMapping()==null)
								elementStyleMapping=new JSONObject();
							else
								elementStyleMapping=programConfiguration.getElementStyleMapping();
							
							formStyle.put(rset.getString("CLASS_NAME"), formStyleConfigData.get(rset.getString("CLASS_NAME")).toString());
							elementStyleMapping.put(rset.getString("ELEMENT_ID"), rset.getString("CLASS_NAME"));
							programConfiguration.setFormStyle(formStyle);
							programConfiguration.setElementStyleMapping(elementStyleMapping);
						}	
					}while(rset.next());
				}
			}
		}finally{
			util.reset();
		}
		
	}
	
	private void setSchemaName() {
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		try {
			String sqlQuery = "SELECT DSRC_SERVER_TYPE from dsource WHERE DSRC_STATUS='1'";
			util.reset();
			util.setMode(DBUtil.PREPARED);
			util.setSql(sqlQuery);
			ResultSet rs = util.executeQuery();
			if (rs.next()) {
				String dbType=rs.getString(1);
				DTObject inputDTO = new DTObject();
				sqlQuery = "UPDATE dsource SET DSRC_NAME = {0} WHERE DSRC_STATUS ='1'";
				util.reset();
				util.setMode(DBUtil.PREPARED);
				if(dbType.equals("1")) { //Oracle
					inputDTO.setObject("SCHEMA_NAME", "SYS_CONTEXT('USERENV','CURRENT_SCHEMA')");
				}else{
					inputDTO.setObject("SCHEMA_NAME", "DATABASE()");
				}
				sqlQuery=MessageFormat.format(sqlQuery, new Object[] {inputDTO.getObject("SCHEMA_NAME")});
				util.setSql(sqlQuery);
				util.executeUpdate();
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			dbContext.close();
		}
	}	
	public static ProgramMap getInstance() {
		if (instance == null) {
			reloadConfiguration();
		}
		return instance;
	}
}
