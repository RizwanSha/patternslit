package patterns.config.framework.web.configuration.mtm;

import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.BindParameterType;
import patterns.config.framework.database.objects.TableInfo;
import patterns.config.framework.database.utils.DBInfo;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.loggers.ApplicationLogger;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.AJAXContentManager;

public class ComponentRecordQueryManager extends AJAXContentManager {

	private static final String RMTM_PARENT_PROGRAM_ID = "_ParentProgramID";
	private static final String RMTM_PARENT_PRIMARY_KEY = "_ParentPrimaryKey";
	private static final String RMTM_LINKED_PROGRAM_ID = "_LinkedProgramID";
	private static final String RMTM_LINKED_PRIMARY_KEY = "_LinkedPrimaryKey";
	private static final String TOKEN_ID = "_TokenID";
	public static final String COMP_NAME_SEPERATOR = "###";
	public static final String TBA_NAME_SEPERATOR = "@@";
	public static final String QUERY_MAIN = "M";
	public static final String QUERY_TBA = "T";
	public static final String QUERY_BOTH = "B";

	private static final String RMTM_REQUEST_TYPE = "_RMtmReqType";
	public static final String TBA_DATE_FORMAT = "dd-MM-yyyy HH24:MI:SS";
	private ApplicationLogger logger = null;
	private TableInfo tableInfo = null;
	private ResultSet rset = null;

	private String linkedProgramID;
	private String linkedPrimaryKeyValues = null;
	private String parentProgramID;
	private String parentPrimaryKeyValues = null;
	private String reqType = "";
	private StringBuffer sqlCommand = null;
	private String entryDate = null;
	private String mTable;
	private long entrySl;
	private DTObject result = new DTObject();
	private ArrayList<String> mTableArrayList = new ArrayList<String>();
	private String[] keyFields;
	private BindParameterType[] keyFieldTypes;
	int counter = 0;

	public ComponentRecordQueryManager() {
		super();
		logger = ApplicationLogger.getInstance(this.getClass().getSimpleName());
		// logger.logDebug("#()");
	}

	public DTObject getData(DTObject input) {
		// logger.logDebug("getData()");
		try {
			RecordQueryMap dataMap = RecordQueryMap.getInstance();
			linkedProgramID = input.get(RMTM_LINKED_PROGRAM_ID).toUpperCase(getContext().getLocale());
			linkedPrimaryKeyValues = input.get(RMTM_LINKED_PRIMARY_KEY);
			parentProgramID = input.get(RMTM_PARENT_PROGRAM_ID).toUpperCase(getContext().getLocale());
			parentPrimaryKeyValues = input.get(RMTM_PARENT_PRIMARY_KEY).trim();
			reqType = input.get(RMTM_REQUEST_TYPE).trim();
			String tokenID = input.get(TOKEN_ID);
			String sqlQuery = dataMap.getDescriptor(linkedProgramID, tokenID);
			sqlCommand = new StringBuffer(sqlQuery);

			if (reqType.equals(QUERY_MAIN)) {
				executeSQLQuery();
			} else if (reqType.equals(QUERY_TBA)) {
				formTBAQuery();
				formLinkTBAQuery();
				executeSQLQuery();
			} else if (reqType.equals(QUERY_BOTH)) {
				formTBAQuery();
				formLinkTBAQuery();
				executeSQLQuery();
				if (counter == 0) {
					reqType = "M";
					sqlCommand = new StringBuffer(sqlQuery);
					formTBAQuery();
					formLinkTBAQuery();
					executeSQLQuery();
				}
			}
			System.out.println("Final Query:" + sqlQuery);
			logger.logInfo("getData() : SQL Query : " + sqlCommand);
			result.set(ERROR, "");
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError(" getData(1) - " + e.getLocalizedMessage() + input);
			result.set(ERROR, e.getLocalizedMessage());
			result.set(RESULT, DATA_UNAVAILABLE);
		} finally {
		}
		return result;
	}

	private boolean formTBAQuery() {
		String pattern = TBA_NAME_SEPERATOR + "([A-Za-z0-9_])+" + TBA_NAME_SEPERATOR;
		Pattern p = Pattern.compile(pattern);
		int start = 0;
		int end = 0;
		do {
			Matcher m = p.matcher(sqlCommand.toString());
			if (m.find()) {
				start = m.start();
				end = m.end();
				Pattern p1 = Pattern.compile("([A-Za-z0-9_])+");
				Matcher m1 = p1.matcher(m.group());
				if (m1.find()) {
					mTable = m1.group();
					mTableArrayList.add(mTable);
				}
				sqlCommand.replace(start, end, getTableName(mTable, false));
			} else {
				break;
			}
		} while (true);

		return true;
	}

	private boolean formLinkTBAQuery() {
		String pattern = COMP_NAME_SEPERATOR + "([A-Za-z0-9_])+" + COMP_NAME_SEPERATOR;
		Pattern p = Pattern.compile(pattern);
		int start = 0;
		int end = 0;
		do {
			Matcher m = p.matcher(sqlCommand.toString());
			if (m.find()) {
				start = m.start();
				end = m.end();
				Pattern p1 = Pattern.compile("([A-Za-z0-9_])+");
				Matcher m1 = p1.matcher(m.group());
				if (m1.find()) {
					mTable = m1.group();
					mTableArrayList.add(mTable);
				}
				sqlCommand.replace(start, end, getTableName(mTable, true));
			} else {
				break;
			}
		} while (true);

		return true;
	}

	private String getTableName(String mtable, boolean isComponent) {
		StringBuffer buffer = new StringBuffer();
		DBUtil dbutil = getDbContext().createUtilInstance();
		StringBuffer query = new StringBuffer();
		try {
			if (reqType.equalsIgnoreCase("M")) {
				buffer.append(mtable);
				return buffer.toString();
			}
			if (!isComponent) {
				buffer.append("( SELECT");
				dbutil.reset();
				dbutil.setMode(DBUtil.PREPARED);
				dbutil.setSql("SELECT TO_CHAR(TBAQ_ENTRY_DATE,'%d-%m-%Y') TBAQ_ENTRY_DATE,TBAQ_ENTRY_SL,TBAQ_OPERN_FLG FROM TBAAUTHQ WHERE ENTITY_CODE = ?  AND  TBAQ_PGM_ID = ? AND TBAQ_MAIN_PK = ? ");
				dbutil.setString(1, getContext().getEntityCode());
				dbutil.setString(2, parentProgramID);
				dbutil.setString(3, parentPrimaryKeyValues);
				ResultSet rs = dbutil.executeQuery();
				if (rs.next()) {
					entryDate = rs.getString("TBAQ_ENTRY_DATE");
					entrySl = rs.getLong("TBAQ_ENTRY_SL");
				}
				DBInfo info = new DBInfo();
				tableInfo = info.getTableInfo(mtable);
				Set<String> noUpdateColumnInfo = tableInfo.getNoUpdationColumnInfo();
				String[] fieldNames = tableInfo.getColumnInfo().getColumnNames();
				Map<String, BindParameterType> columnType = tableInfo.getColumnInfo().getColumnMapping();
				for (String fieldName : fieldNames) {
					if (!noUpdateColumnInfo.contains(fieldName)) {
						if (columnType.get(fieldName).equals(BindParameterType.DATE)) {
							query.append(" TO_DATE(EXTRACTVALUE(T.TBADTL_DATA_BLOCK,'//" + fieldName + "'),'"
									+ BackOfficeConstants.JAVA_TBA_DB_DATE_FORMAT + "')" + fieldName + ",");
						} else {
							query.append(" EXTRACTVALUE(T.TBADTL_DATA_BLOCK,'//" + fieldName + "')" + fieldName + ",");
						}
					}
				}
				query.trimToSize();
				buffer.append(query.substring(0, query.length() - 1));
				buffer.append(" FROM TBAAUTHDTL T WHERE T.ENTITY_CODE='").append(getContext().getEntityCode());
				buffer.append("' AND T.TBADTL_PGM_ID = '").append(parentProgramID);
				buffer.append("' AND T.TBADTL_MAIN_PK='").append(parentPrimaryKeyValues);
				buffer.append("' AND T.TBADTL_ENTRY_DATE=TO_DATE('").append(entryDate);
				buffer.append("','%d-%m-%Y') AND T.TBADTL_DTL_SL= '").append(entrySl);
				buffer.append("' AND T.TBADTL_TABLE_NAME='").append(mtable);
				buffer.append("' )");
			} else {
				buffer.append("( SELECT");
				DBInfo info = new DBInfo();
				tableInfo = info.getTableInfo(mtable);
				Set<String> noUpdateColumnInfo = tableInfo.getNoUpdationColumnInfo();
				String[] fieldNames = tableInfo.getColumnInfo().getColumnNames();
				Map<String, BindParameterType> columnType = tableInfo.getColumnInfo().getColumnMapping();
				for (String fieldName : fieldNames) {
					if (!noUpdateColumnInfo.contains(fieldName)) {
						if (columnType.get(fieldName).equals(BindParameterType.DATE)) {
							query.append(" TO_DATE(EXTRACTVALUE(TC.DATA_BLOCK,'//" + fieldName + "'),'"
									+ BackOfficeConstants.JAVA_TBA_DB_DATE_FORMAT + "')" + fieldName + ",");
						} else {
							query.append(" EXTRACTVALUE(TC.DATA_BLOCK,'//" + fieldName + "')" + fieldName + ",");
						}
					}
				}

				query.trimToSize();
				buffer.append(query.substring(0, query.length() - 1));
				buffer.append(" FROM PGMLINKEDTBA TC WHERE TC.ENTITY_CODE='").append(getContext().getEntityCode());
				buffer.append("' AND TC.PARENT_PGM_ID = '").append(parentProgramID);
				buffer.append("' AND TC.PARENT_PGM_PK = '").append(parentPrimaryKeyValues);
				buffer.append("' AND TC.LINKED_PGM_ID='").append(linkedProgramID);
				buffer.append("' AND TC.LINKED_PGM_PK='").append(linkedPrimaryKeyValues);
				buffer.append("' AND TC.TABLE_NAME='").append(mtable);
				buffer.append("' AND TC.BLOCK_SL=1) ");
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError(" getTableName(1) : " + e.getLocalizedMessage());
			buffer.append("");
		} finally {
			dbutil.reset();
		}
		return buffer.toString();
	}

	private void executeSQLQuery() throws Exception {
		// logger.logDebug(" executeSQLQuery()");
		DBUtil util = getDbContext().createUtilInstance();
		int index = 1;
		try {
			util.reset();
			util.setSql(sqlCommand.toString());

			String[] arguments = linkedPrimaryKeyValues.split("\\|");
			for (int i = 0; i < arguments.length; ++i) {
				util.setString(i + index, arguments[i]);
			}

			rset = util.executeQuery();
			SimpleDateFormat sdf = new SimpleDateFormat(getContext().getDateFormat());
			if (rset.next()) {
				for (int j = 1; j < rset.getMetaData().getColumnCount() + 1; ++j) {
					if (rset.getMetaData().getColumnTypeName(j).equals("DATE")) {
						if (rset.getDate(j) != null) {
							result.set(rset.getMetaData().getColumnName(j), sdf.format(rset.getDate(j)));
						} else {
							result.set(rset.getMetaData().getColumnName(j), null);
						}
					} else {
						result.set(rset.getMetaData().getColumnName(j), rset.getString(j));
					}
				}
				counter++;
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError(" executeSQLQuery(1) - " + e.getLocalizedMessage());
			throw e;
		} finally {
			util.reset();
		}
	}
}
