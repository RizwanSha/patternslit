package patterns.config.framework.web.configuration.lookup;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import patterns.config.framework.database.BindParameterType;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.loggers.ApplicationLogger;

public class LookupMap {

	private static final ReentrantReadWriteLock readWriteLock = new ReentrantReadWriteLock();

	// private static final Lock read = readWriteLock.readLock();

	private static final Lock write = readWriteLock.writeLock();

	private static final ReentrantReadWriteLock readWriteLockGlobal = new ReentrantReadWriteLock();

	private static final Lock writeGlobal = readWriteLockGlobal.writeLock();

	private ApplicationLogger logger = null;

	private Map<String, Map<String, LookupConfiguration>> internalMap;
	private Map<String, Map<String, LookupConfiguration>> internalLocalMap;

	private static volatile LookupMap instance = null;

	private LookupMap() {
		logger = ApplicationLogger.getInstance(this.getClass().getSimpleName());
		// logger.logDebug("#()");
	}

	public LookupConfiguration getConfiguration(String programID, String tokenID) {
		// logger.logDebug("getConfiguration()");
		LookupConfiguration configuration = null;
		try {
			// read.lock();
			Map<String, LookupConfiguration> programLevelMap = internalMap.get(programID);
			if (programLevelMap == null || programLevelMap.size() == 0) {
				logger.logError("#Empty ProgramLevelMap :: " + programID + " -- " + tokenID);
				return null;
			}
			configuration = programLevelMap.get(tokenID);
			if (configuration == null) {
				logger.logError("#Empty Configuration :: " + programID + " -- " + tokenID);
				return null;
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError(" getConfiguration(1) - " + e.getLocalizedMessage());
		} finally {
			// read.unlock();
		}
		return configuration;
	}

	private void initConfiguration() {
		internalMap.putAll(internalLocalMap);
		internalLocalMap.clear();
	}

	protected void resetConfiguration() {
		logger.logDebug("resetConfiguration()");
		if (internalMap == null)
			internalMap = new HashMap<String, Map<String, LookupConfiguration>>(1000, 1F);
		internalMap.clear();
	}

	public static void unloadConfiguration() {
		try {
			writeGlobal.lock();
			if (instance != null) {
				instance.resetConfiguration();
				instance = null;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			writeGlobal.unlock();
		}
	}

	public static void reloadConfiguration() {
		try {
			writeGlobal.lock();
			if (instance == null) {
				instance = new LookupMap();
			}
			instance.loadConfiguration();
			try {
				write.lock();
				instance.resetConfiguration();
				instance.initConfiguration();
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				write.unlock();
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			writeGlobal.unlock();
		}
	}

	protected void loadConfiguration() {
		logger.logDebug("loadConfiguration()");
		DBContext dbContext = new DBContext();
		if (internalLocalMap == null)
			internalLocalMap = new HashMap<String, Map<String, LookupConfiguration>>(1000, 1F);
		internalLocalMap.clear();
		HashMap<String, LookupConfiguration> helpTokens = null;
		try {
			DBUtil utilOuter = dbContext.createUtilInstance();
			utilOuter.reset();
			String sqlQuery = "SELECT PROGRAM_ID,TOKEN_ID,SQL_QUERY,DIRECT_ACCESS,INTERCEPT_REQD,INTERCEPTION_CLASS,DEFAULT_LOAD_REQD,YEARWISE_ACCESS FROM LOOKUPMASTER ORDER BY PROGRAM_ID,TOKEN_ID";
			utilOuter.setSql(sqlQuery);
			ResultSet rs = utilOuter.executeQuery();
			while (rs.next()) {
				LookupConfiguration lookupConfiguration = null;
				String programID = rs.getString("PROGRAM_ID");
				String tokenID = rs.getString("TOKEN_ID");
				String tokenSQLQuery = rs.getString("SQL_QUERY");
				String directAccess = rs.getString("DIRECT_ACCESS");
				String intercepted = rs.getString("INTERCEPT_REQD");
				String interceptionClass = rs.getString("INTERCEPTION_CLASS");
				String defaultLoading = rs.getString("DEFAULT_LOAD_REQD");
				String yearWiseAccess = rs.getString("YEARWISE_ACCESS");

				int columnCount = 0;
				if (!internalLocalMap.containsKey(programID)) {
					helpTokens = new HashMap<String, LookupConfiguration>(2, 1F);
					internalLocalMap.put(programID, helpTokens);
				}
				if (!helpTokens.containsKey(tokenID)) {
					lookupConfiguration = new LookupConfiguration();
					lookupConfiguration.setProgramID(programID);
					lookupConfiguration.setTokenID(tokenID);
					if (directAccess != null && directAccess.equals("1")) {
						lookupConfiguration.setDirectAccess(true);
					} else {
						lookupConfiguration.setDirectAccess(false);
					}
					if (intercepted != null && intercepted.equals("1")) {
						lookupConfiguration.setIntercepted(true);
						lookupConfiguration.setInterceptionClass(interceptionClass);
					} else {
						lookupConfiguration.setIntercepted(false);
					}
					if (defaultLoading != null && defaultLoading.equals("1")) {
						lookupConfiguration.setDefaultLoadingRequired(true);
					} else {
						lookupConfiguration.setDefaultLoadingRequired(false);
					}
					if (yearWiseAccess != null && yearWiseAccess.equals("1")) {
						lookupConfiguration.setYearWiseFilterRequired(true);
					} else {
						lookupConfiguration.setYearWiseFilterRequired(false);
					}
					helpTokens.put(tokenID, lookupConfiguration);

				}
				if (!lookupConfiguration.isIntercepted() || (tokenSQLQuery != null && !tokenSQLQuery.equals(RegularConstants.EMPTY_STRING))) {
					lookupConfiguration.setSqlQuery(tokenSQLQuery);
					DBUtil utilInner = dbContext.createUtilInstance();
					sqlQuery = "SELECT COUNT(COLUMN_SL) FROM LOOKUPMASTERCOLS WHERE PROGRAM_ID=? and TOKEN_ID=? ORDER BY COLUMN_SL";
					utilInner.setSql(sqlQuery);
					utilInner.setString(1, programID);
					utilInner.setString(2, tokenID);
					ResultSet rs1 = utilInner.executeQuery();
					if (rs1.next()) {
						columnCount = rs1.getInt(1);
					}
					utilInner.reset();
					String[] columnNames;
					String[] predefinedColumns;
					String[] aliasNames;
					BindParameterType[] columnTypes;
					String[] columnDescriptions_en_US;
					boolean[] defaultSearchColumns;
					boolean[] filterAllowed;
					String[] defaultSorting;
					if (columnCount > 0) {
						columnNames = new String[columnCount];
						predefinedColumns = new String[columnCount];
						aliasNames = new String[columnCount];
						columnTypes = new BindParameterType[columnCount];
						columnDescriptions_en_US = new String[columnCount];
						defaultSearchColumns = new boolean[columnCount];
						filterAllowed = new boolean[columnCount];
						defaultSorting = new String[columnCount];
						sqlQuery = "SELECT COLUMN_NAME,COLUMN_TYPE,DEFAULT_SEARCH_COLUMN,COLUMN_DESC_EN_US,ALIAS_NAME,FILTER_ALLOWED,DEFAULT_SORT,PREDEFINE_COLS FROM LOOKUPMASTERCOLS where PROGRAM_ID=? and TOKEN_ID=? ORDER BY COLUMN_SL";
						utilInner.setSql(sqlQuery);
						utilInner.setString(1, programID);
						utilInner.setString(2, tokenID);
						rs1 = utilInner.executeQuery();
						int j = 0;
						while (rs1.next()) {
							columnNames[j] = rs1.getString(1);
							columnTypes[j] = getBindDataType(rs1.getString(2));
							String defaultSearchColumn = rs1.getString(3);
							columnDescriptions_en_US[j] = rs1.getString(4);
							if (defaultSearchColumn != null && defaultSearchColumn.equals("1")) {
								defaultSearchColumns[j] = true;
							} else {
								defaultSearchColumns[j] = false;
							}
							aliasNames[j] = rs1.getString(5);
							String filterAllow = rs1.getString(6);
							if (filterAllow != null && filterAllow.equals("1")) {
								filterAllowed[j] = true;
							}
							defaultSorting[j] = rs1.getString(7);
							predefinedColumns[j] = rs1.getString(8);
							j++;
						}
						utilInner.reset();
						lookupConfiguration.setColumnNames(columnNames);
						lookupConfiguration.setColumnHeadings_en_US(columnDescriptions_en_US);
						lookupConfiguration.setColumnTypes(columnTypes);
						lookupConfiguration.setDefaultSearchColumns(defaultSearchColumns);
						lookupConfiguration.setAliasNames(aliasNames);
						lookupConfiguration.setFilterAllowed(filterAllowed);
						lookupConfiguration.setDefaultSort(defaultSorting);
						lookupConfiguration.setPredefinedColumns(predefinedColumns);
					}
				}
			}
			utilOuter.reset();
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError(" loadConfiguration(1) - " + e.getLocalizedMessage());
		} finally {
			dbContext.close();
		}
	}

	public static BindParameterType getBindDataType(String dataType) throws Exception {
		BindParameterType parameterType = null;
		char coldataType = dataType.charAt(0);
		switch (coldataType) {
		case '1':
		default:
			parameterType = BindParameterType.VARCHAR;
			break;
		case '2':
			parameterType = BindParameterType.DATE;
			break;
		case '3':
			parameterType = BindParameterType.TIMESTAMP;
			break;
		case '4':
			parameterType = BindParameterType.INTEGER;
			break;
		case '5':
			parameterType = BindParameterType.LONG;
			break;
		case '6':
			parameterType = BindParameterType.BIGDECIMAL;
			break;
		case '7':
			parameterType = BindParameterType.BIGINT;
			break;
		case '9':
			parameterType = BindParameterType.CUSTOM;
			break;
		}
		return parameterType;
	}

	public static LookupMap getInstance() {
		if (instance == null) {
			reloadConfiguration();
		}
		return instance;
	}
}