package patterns.config.framework.web.configuration.record;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.loggers.ApplicationLogger;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.AJAXContentManager;
import patterns.config.framework.web.ajax.InterceptorPurpose;
import patterns.json.JSONObject;

public class QueryViewManager extends AJAXContentManager {
	public final String LOAD_TYPE = "_TYPE";
	private String tokenID = "";
	private String programID = "";
	private RecordFinderConfiguration recordFinderConfiguration = null;
	private ApplicationLogger logger = null;

	public QueryViewManager() {
		super();
		logger = ApplicationLogger.getInstance(this.getClass().getSimpleName());
		// logger.logDebug("#()");
	}

	public DTObject getData(DTObject input) {
		// logger.logDebug("getData()");
		DTObject result = new DTObject();
		JSONObject jsonObject = new JSONObject();
		try {
			RecordFinderMap recordFinderMap = RecordFinderMap.getInstance();
			jsonObject.put(RESULT, DATA_UNAVAILABLE);
			programID = input.get(PROGRAM_KEY);
			if (programID == null) {
				logger.logError("RECORD_FINDER_PROGRAM_ID not specified");
				return result;
			} else {
				programID = programID.trim().toUpperCase(getContext().getLocale());
				logger.logDebug("setting v_PROGRAM_ID : " + programID);
			}
			tokenID = input.get(TOKEN_KEY);
			if (tokenID == null) {
				logger.logError("RECORD_FINDER_TOKEN_KEY not specified");
				return result;
			} else {
				tokenID = tokenID.trim().toUpperCase(getContext().getLocale());
				logger.logDebug("setting RECORD_FINDER_TOKEN_KEY : " + tokenID);
				recordFinderConfiguration = recordFinderMap.getConfiguration(programID, tokenID);
				if (recordFinderConfiguration == null) {
					logger.logError("Invalid RECORD_FINDER_TOKEN_KEY : " + tokenID);
					return result;
				}

				if (!recordFinderConfiguration.isDirectAccess()) {
					logger.logError("Detected RECORD_FINDER Attack : " + tokenID);
					return result;
				}
				if (recordFinderConfiguration.isIntercepted()) {
					input = processFilter(InterceptorPurpose.RECORDFINDER_PROCESS_RESOLVE, recordFinderConfiguration.getInterceptionClass(), input);
					if (input == null) {
						logger.logError("Invalid RECORD_FINDER_INTERCEPTION : " + tokenID);
						return result;
					}
					programID = input.get(PROGRAM_KEY);
					tokenID = input.get(TOKEN_KEY);
					recordFinderConfiguration = recordFinderMap.getConfiguration(programID, tokenID);
					if (recordFinderConfiguration == null) {
						logger.logError("Invalid RECORD_FINDER_TOKEN_KEY : " + tokenID);
						return result;
					}
				}
				ArrayList<RecordFinderFilter> arrayList = recordFinderConfiguration.getRecordFinderFilter();
				for (int i = 0; i < arrayList.size(); i++) {
					if (arrayList.get(i).getColumnType().equals("5")) {
						RecordFinderFilter filter = arrayList.get(i);
						filter.setComboList(getComboList(filter.getComboProgramId(), filter.getComboLovId()));

					}
				}
				jsonObject.put(RESULT, DATA_AVAILABLE);
				jsonObject.put(CONTENT, arrayList);

			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError(" getData(1) - " + e.getLocalizedMessage() + " : " + input);
		}
		result.set(CONTENT, jsonObject.toString());
		return result;
	}

	private HashMap<String, String> getComboList(String programId, String lovId) {
		HashMap<String, String> comboMap = new HashMap<String, String>();
		String sqlQuery = "SELECT LOV_VALUE, LOV_LABEL_EN_US  FROM CMLOVREC WHERE PGM_ID = ? AND LOV_ID = ? ORDER BY LOV_SL ASC";
		DBUtil util = getDbContext().createUtilInstance();
		try {
			util.setMode(DBUtil.PREPARED);
			util.setSql(sqlQuery);
			util.setString(1, programId);
			util.setString(2, lovId);
			ResultSet rs = util.executeQuery();
			while (rs.next()) {
				comboMap.put(rs.getString(1), rs.getString(2));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			util.reset();
		}
		return comboMap;
	}
}