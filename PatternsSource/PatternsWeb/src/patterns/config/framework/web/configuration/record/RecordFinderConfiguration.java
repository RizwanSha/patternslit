package patterns.config.framework.web.configuration.record;

import java.io.Serializable;
import java.util.ArrayList;

import patterns.config.framework.database.BindParameterType;

public class RecordFinderConfiguration implements Serializable {
	public static final String UI_READ_ONLY_TEXT = "1";
	public static final String UI_DATE_BOX = "2";
	public static final String UI_CHECK_BOX = "9";
	public static final String UI_RADIO = "8";
	public static final String UI_EDITABLE = "7";

	public static final String UITYPE_RO = "ro";
	public static final String UITYPE_LINK = "link";
	public static final String UITYPE_CH = "ch";
	public static final String UITYPE_RA = "ra";

	public static final String ALIGN_RIGHT = "right";
	public static final String ALIGN_LEFT = "left";
	public static final String ALIGN_CENTER = "center";

	private static final long serialVersionUID = 4869907043512263269L;

	public String[] getColumnNames() {
		return columnNames;
	}

	public void setColumnNames(String[] columnNames) {
		this.columnNames = columnNames;
	}

	public String getSqlQuery() {
		return sqlQuery;
	}

	public void setSqlQuery(String sqlQuery) {
		this.sqlQuery = sqlQuery;
	}

	public BindParameterType[] getColumnTypes() {
		return columnTypes;
	}

	public void setColumnTypes(BindParameterType[] columnTypes) {
		this.columnTypes = columnTypes;
	}

	public void setSplitAt(String splitAt) {
		this.splitAt = splitAt;
	}

	public String getSplitAt() {
		return splitAt;
	}

	public void setColumnWidths(String[] columnWidths) {
		this.columnWidths = columnWidths;
	}

	public String[] getColumnWidths() {
		return columnWidths;
	}

	public void setProgramID(String programID) {
		this.programID = programID;
	}

	public String getProgramID() {
		return programID;
	}

	public void setTokenID(String tokenID) {
		this.tokenID = tokenID;
	}

	public String getTokenID() {
		return tokenID;
	}

	public void setSearchColumns(boolean[] searchColumns) {
		this.searchColumns = searchColumns;
	}

	public boolean[] getSearchColumns() {
		return searchColumns;
	}

	public void setColumnHeadings_en_US(String[] columnHeadings_en_US) {
		this.columnHeadings_en_US = columnHeadings_en_US;
	}

	public String[] getColumnHeadings_en_US() {
		return columnHeadings_en_US;
	}

	public void setHiddenColumns(boolean[] hiddenColumns) {
		this.hiddenColumns = hiddenColumns;
	}

	public boolean[] getHiddenColumns() {
		return hiddenColumns;
	}

	public void setDirectAccess(boolean directAccess) {
		this.directAccess = directAccess;
	}

	public boolean isDirectAccess() {
		return directAccess;
	}

	public void setIntercepted(boolean intercepted) {
		this.intercepted = intercepted;
	}

	public boolean isIntercepted() {
		return intercepted;
	}

	public void setInterceptionClass(String interceptionClass) {
		this.interceptionClass = interceptionClass;
	}

	public String getInterceptionClass() {
		return interceptionClass;
	}

	public String[] getColumnUI() {
		return columnUI;
	}

	public void setColumnUI(String[] columnUI) {
		this.columnUI = columnUI;
	}

	public String[] getColumnAlign() {
		return columnAlign;
	}

	public void setColumnAlign(String[] columnAlign) {
		this.columnAlign = columnAlign;
	}

	private BindParameterType[] columnTypes;
	private String[] columnAlign;
	private String[] columnUI;
	private String[] columnNames;
	private String[] columnHeadings_en_US;
	private boolean[] hiddenColumns;
	private String sqlQuery;
	private boolean[] searchColumns;
	private String[] columnWidths;
	private String splitAt;
	private String programID;
	private String tokenID;
	private boolean directAccess;
	private boolean intercepted;
	private String interceptionClass;
	private ArrayList<RecordFinderFilter> recordFinderFilter;
	private String[] aliasNames;
	private String[] predefinedColumns;
	private String[] defaultSort;

	public String[] getAliasNames() {
		return aliasNames;
	}

	public void setAliasNames(String[] aliasNames) {
		this.aliasNames = aliasNames;
	}

	public String[] getPredefinedColumns() {
		return predefinedColumns;
	}

	public void setPredefinedColumns(String[] predefinedColumns) {
		this.predefinedColumns = predefinedColumns;
	}

	public String[] getDefaultSort() {
		return defaultSort;
	}

	public void setDefaultSort(String[] defaultSort) {
		this.defaultSort = defaultSort;
	}

	public ArrayList<RecordFinderFilter> getRecordFinderFilter() {
		return recordFinderFilter;
	}

	public void setRecordFinderFilter(ArrayList<RecordFinderFilter> recordFinderFilter) {
		this.recordFinderFilter = recordFinderFilter;
	}
}
