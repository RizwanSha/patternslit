package patterns.config.framework.web.configuration.mtm;

import java.util.HashMap;
import java.util.Map;

public class RecordQueryMap {
	public static final String NAME_SEPERATOR = "@@";
	public static final String COMPONENT_NAME_SEPERATOR = "###";
	public static final String TBA_DATE_FORMAT = "%d-%m-%Y";
	public static final String CONTEXT_DATE_FORMAT = "dd-MM-yyyy";
	private Map<String, Map<String, String>> internalMap;
	private static volatile RecordQueryMap instance;
	static {
		instance = new RecordQueryMap();
	}

	private RecordQueryMap() {
		internalMap = new HashMap<String, Map<String, String>>();
		loadGridTokens();
	}

	private void loadGridTokens() {
		loadAccessGridTokens();
		// loadOperationsGridTokens();
		// loadCustomerAdminGridTokens();
		// loadOltasGridTokens();
		loadMembersGridTokens();
		loadCommonGridTokens();
		loadRegistrationTokens();
		loadFASGridTokens();
	}

	private void loadRegistrationTokens() {
		// MPERDONDB BEGIN
		HashMap<String, String> mpersonMap = new HashMap<String, String>();
		mpersonMap
				.put("MPERSONDB_MAIN",
						"SELECT P.LEGAL_CONN_REL_CODE,P.LEGAL_CONN_TITLE_CODE1,P.LEGAL_CONN_TITLE_CODE2,P.LEGAL_CONN_PERSON_NAME,P.DEPENDENT_FLAG,P.DEPEND_REL_CODE,P.DEPEND_TITLE1,P.DEPEND_TITLE2,P.DEPEND_PERSON_NAME,P.RELATIONSHIP_INFO,R.DESCRIPTION  FROM PERSONOTHINFO  P, RELATIONREF R WHERE P.PARTITION_NO=? AND P.PERSON_ID=? AND P.LEGAL_CONN_REL_CODE=R.RELREF_CODE ");
		mpersonMap
				.put("MPERSONDB_TBA",
						"SELECT P.LEGAL_CONN_REL_CODE,P.LEGAL_CONN_TITLE_CODE1,P.LEGAL_CONN_TITLE_CODE2,P.LEGAL_CONN_PERSON_NAME,P.DEPENDENT_FLAG,P.DEPEND_REL_CODE,P.DEPEND_TITLE1,P.DEPEND_TITLE2,P.DEPEND_PERSON_NAME,P.RELATIONSHIP_INFO,R.DESCRIPTION  FROM "
								+ NAME_SEPERATOR
								+ "PERSONOTHINFO"
								+ NAME_SEPERATOR
								+ " P, RELATIONREF R WHERE P.PARTITION_NO=? AND P.PERSON_ID=? AND P.LEGAL_CONN_REL_CODE=R.RELREF_CODE ");
		internalMap.put("MPERSONDB", mpersonMap);
		// MPERDONDB END
	}

	private void loadCommonGridTokens() {
		// MADDRESS BEGIN
		HashMap<String, String> maddressMap = new HashMap<String, String>();
		maddressMap
				.put("MADDRESS_T",
						"SELECT A.ADDR_INV_NO,A.ADDR_TYPE,T.DESCRIPTION ADDR_DESC,A.COUNTRY_CODE,C.DESCRIPTION COUNT_DESC,A.ADDRESS_LINE1,A.ADDRESS_LINE2,A.ADDRESS_LINE3,A.ADDRESS_LINE4,A.GEO_UNIT_ID FROM "
								+ COMPONENT_NAME_SEPERATOR
								+ "ADDRESSDB"
								+ COMPONENT_NAME_SEPERATOR
								+ " A,ADDRESSTYPE T,COUNTRY C WHERE A.PARTITION_NO=? AND A.ADDR_INV_NO=?  AND T.ADDR_TYPE= A.ADDR_TYPE AND C.COUNTRY_CODE= A.COUNTRY_CODE");
		maddressMap
				.put("MADDRESS_M",
						"SELECT A.ADDR_INV_NO,A.ADDR_TYPE,T.DESCRIPTION ADDR_DESC,A.COUNTRY_CODE,C.DESCRIPTION COUNT_DESC,A.ADDRESS_LINE1,A.ADDRESS_LINE2,A.ADDRESS_LINE3,A.ADDRESS_LINE4,A.GEO_UNIT_ID FROM ADDRESSDB A,ADDRESSTYPE T,COUNTRY C WHERE A.PARTITION_NO=? AND A.ADDR_INV_NO=? AND  T.ADDR_TYPE= A.ADDR_TYPE AND C.COUNTRY_CODE= A.COUNTRY_CODE");
		maddressMap
				.put("MADDRESS_OLD_T",
						"SELECT P.OMG_ADDRESS1,P.OMG_ADDRESS2,P.OMG_ADDRESS3,P.OMG_ADDRESS4,P.OMG_ADDRESS5  FROM "
								+ NAME_SEPERATOR
								+ "PERSONOTHINFO"
								+ NAME_SEPERATOR
								+ "  P WHERE P.PARTITION_NO=? AND P.PERSON_ID=?");
		maddressMap
				.put("MADDRESS_OLD_M",
						"SELECT P.OMG_ADDRESS1,P.OMG_ADDRESS2,P.OMG_ADDRESS3,P.OMG_ADDRESS4,P.OMG_ADDRESS5  FROM PERSONOTHINFO  P WHERE P.PARTITION_NO=? AND P.PERSON_ID=? ");
		internalMap.put("MADDRESS", maddressMap);
		// MADDRESS END

		// MCONTACT BEGIN
		HashMap<String, String> mcontactMap = new HashMap<String, String>();
		mcontactMap
				.put("MCONTACT_T",
						"SELECT C.CONTACT_INV_NO,C.CONTACT_TYPE,C.LL_COUNTRY_CODE,C.LL_LOC_STD_CODE,C.LL_CONTACT_NUMBER,C.MOB_COUNTRY_CODE,C.MOB_CONTACT_NUMBER,C.EMAIL_ID FROM "
								+ COMPONENT_NAME_SEPERATOR
								+ "CONTACTDB"
								+ COMPONENT_NAME_SEPERATOR
								+ " C WHERE C.PARTITION_NO=? AND C.CONTACT_INV_NO=? ");
		mcontactMap
				.put("MCONTACT_M",
						"SELECT C.CONTACT_INV_NO,C.CONTACT_TYPE,C.LL_COUNTRY_CODE,C.LL_LOC_STD_CODE,C.LL_CONTACT_NUMBER,C.MOB_COUNTRY_CODE,C.MOB_CONTACT_NUMBER,C.EMAIL_ID FROM CONTACTDB C WHERE C.PARTITION_NO=? AND C.CONTACT_INV_NO=? ");
		internalMap.put("MCONTACT", mcontactMap);
		// MCONTACT END
	}

	private void loadAccessGridTokens() {
		// EUSERACTIVATION BEGIN
		HashMap<String, String> euseractivationMap = new HashMap<String, String>();
		euseractivationMap.put("EUSERACTIVATION",
				"SELECT DTL.USER_ACTIVATION_REQD STATUS FROM " + NAME_SEPERATOR
						+ "USERS" + NAME_SEPERATOR
						+ " DTL WHERE DTL.ENTITY_CODE = ? AND DTL.USER_ID = ?");
		internalMap.put("EUSERACTIVATION", euseractivationMap);
		// EUSERACTIVATION END

		// MUSER BEGIN
		HashMap<String, String> muserMap = new HashMap<String, String>();
		muserMap.put(
				"TBA_ADDRESS_DTL",
				"SELECT H.* FROM "
						+ NAME_SEPERATOR
						+ "USERS"
						+ NAME_SEPERATOR
						+ " M,  "
						+ NAME_SEPERATOR
						+ "ADDRINVHIST"
						+ NAME_SEPERATOR
						+ " H WHERE M.ENTITY_CODE=? AND M.USER_ID=? AND M.ENTITY_CODE=H.AIH_ORG_ID AND M.ADDR_INV_NUM=H.AIH_ADDR_INV_NO");
		muserMap.put(
				"ADDRESS_DTL",
				"SELECT AIH_ADDR_INV_NO,AIH_ADDR1,AIH_ADDR2,AIH_ADDR3,AIH_ADDR4,AIH_ADDR5,AIH_LOC,AIH_COUNTRY,AIH_PINCODE FROM "
						+ NAME_SEPERATOR
						+ "ADDRINVHIST"
						+ NAME_SEPERATOR
						+ " H WHERE H.AIH_ORG_ID=?  AND H.AIH_ADDR_INV_NO =? AND H.AIH_SL=(SELECT MAX(AIH_SL) FROM "
						+ NAME_SEPERATOR
						+ "ADDRINVHIST"
						+ NAME_SEPERATOR
						+ " WHERE AIH_ORG_ID=? AND AIH_ADDR_INV_NO=?)");
		internalMap.put("MUSER", muserMap);
		// MUSER END
	}

	private void loadOperationsGridTokens() {

		// MCORPCUST BEGIN
		HashMap<String, String> mcorpcustMap = new HashMap<String, String>();
		mcorpcustMap
				.put("CUSTOMER_MAIN",
						"SELECT DTL.CUSTOMER_INDUS_CODE,DTL.CUSTOMER_SUB_INDUS_CODE,DTL.CUSTOMER_ORG_QUALIFIER,DTL.CUSTOMER_GROUP_CODE,(SELECT INDUS.INDUSTRY_NAME FROM INDUSTRIES INDUS WHERE INDUS.INDUSTRY_CODE=DTL.CUSTOMER_INDUS_CODE) AS INDUSTRY_NAME,(SELECT SUBINDUS.SUBINDUS_INDUS_NAME FROM SUBINDUSTRIES SUBINDUS WHERE SUBINDUS.SUBINDUS_CODE=DTL.CUSTOMER_SUB_INDUS_CODE) AS SUBINDUS_INDUS_NAME FROM "
								+ NAME_SEPERATOR
								+ "CORPCUSTOMER"
								+ NAME_SEPERATOR
								+ " DTL WHERE DTL.ENTITY_CODE =? AND DTL.CUSTOMER_CODE =?");
		internalMap.put("MCORPCUST", mcorpcustMap);
		// MCORPCUST END
		// MJOINTCUST BEGIN
		HashMap<String, String> mjointcustMap = new HashMap<String, String>();
		mjointcustMap.put("CUSTOMER_JOINT", "SELECT DTL.NUM_OF_CLIENTS FROM "
				+ NAME_SEPERATOR + "JOINTCUSTOMER" + NAME_SEPERATOR
				+ " DTL WHERE DTL.ENTITY_CODE =? AND DTL.CUSTOMER_CODE =?");
		internalMap.put("MJOINTCUST", mjointcustMap);
		// MJOINTCUST END
		// MINDVCUST BEGIN
		HashMap<String, String> mindvcustMap = new HashMap<String, String>();
		mindvcustMap
				.put("CUSTOMER_INDV",
						"SELECT DTL.CUSTOMER_DOB,DTL.CUSTOMER_SEX FROM "
								+ NAME_SEPERATOR
								+ "INDVCUSTOMER"
								+ NAME_SEPERATOR
								+ " DTL  WHERE DTL.ENTITY_CODE = ? AND DTL.CUSTOMER_CODE =?");
		internalMap.put("MINDVCUST", mindvcustMap);
		// MINDVCUST END

		// ECUPWDRESET BEGIN
		HashMap<String, String> ecupwdresetMap = new HashMap<String, String>();
		ecupwdresetMap.put("PWDRESET",
				"SELECT DTL.FIRST_PIN,DTL.SECOND_PIN,DTL.REMARKS FROM "
						+ NAME_SEPERATOR + "USERSPWDRESET" + NAME_SEPERATOR
						+ " DTL WHERE DTL.ENTITY_CODE =? AND DTL.USER_ID =?");
		internalMap.put("ECUPWDRESET", ecupwdresetMap);
		// ECUPWDRESET END

		// ECUSTUSERACTIVATION BEGIN
		HashMap<String, String> ecustuseractivationMap = new HashMap<String, String>();
		ecustuseractivationMap.put("ECUSTUSERACTIVATION",
				"SELECT DTL.USER_ACTIVATION_REQD STATUS FROM " + NAME_SEPERATOR
						+ "USERS" + NAME_SEPERATOR
						+ " DTL WHERE DTL.ENTITY_CODE = ? AND DTL.USER_ID = ?");
		internalMap.put("ECUSTUSERACTIVATION", ecustuseractivationMap);
		// ECUSTUSERACTIVATION END
	}

	private void loadCustomerAdminGridTokens() {
	}

	private void loadMembersGridTokens() {
		// EMEMREG BEGIN
		HashMap<String, String> ememregMap = new HashMap<String, String>();
		ememregMap
				.put("RES_ADDRESS_DTL",
						"SELECT H.*,M.MI_SUB_TYPE FROM "
								+ NAME_SEPERATOR
								+ "MEMINV"
								+ NAME_SEPERATOR
								+ " M,  "
								+ NAME_SEPERATOR
								+ "ADDRINVHIST"
								+ NAME_SEPERATOR
								+ " H WHERE M.MI_ORG_ID=? AND M.MI_SMN=? AND M.MI_TYPE='A' AND M.MI_SUB_TYPE='R' AND M.MI_ORG_ID=H.AIH_ORG_ID AND M.MI_INVN_NO=H.AIH_ADDR_INV_NO AND H.AIH_SL='1'");
		ememregMap
				.put("OFF_ADDRESS_DTL",
						"SELECT H.*,M.MI_SUB_TYPE FROM "
								+ NAME_SEPERATOR
								+ "MEMINV"
								+ NAME_SEPERATOR
								+ " M,  "
								+ NAME_SEPERATOR
								+ "ADDRINVHIST"
								+ NAME_SEPERATOR
								+ " H WHERE M.MI_ORG_ID=? AND M.MI_SMN=? AND M.MI_TYPE='A' AND M.MI_SUB_TYPE='O' AND M.MI_ORG_ID=H.AIH_ORG_ID AND M.MI_INVN_NO=H.AIH_ADDR_INV_NO AND H.AIH_SL='1'");
		ememregMap
				.put("RES_ADDRINV_DTL",
						"SELECT H.*,M.MI_SUB_TYPE FROM "
								+ NAME_SEPERATOR
								+ "MEMINV"
								+ NAME_SEPERATOR
								+ " M, ADDRINV H WHERE M.MI_ORG_ID=? AND M.MI_SMN=? AND M.MI_TYPE='A' AND M.MI_SUB_TYPE='R' AND M.MI_ORG_ID=H.AI_ORG_ID AND M.MI_INVN_NO=H.AI_ADDR_INVNTRY_NO");
		ememregMap
				.put("OFF_ADDRINV_DTL",
						"SELECT H.*,M.MI_SUB_TYPE FROM "
								+ NAME_SEPERATOR
								+ "MEMINV"
								+ NAME_SEPERATOR
								+ " M, ADDRINV H WHERE M.MI_ORG_ID=? AND M.MI_SMN=? AND M.MI_TYPE='A' AND M.MI_SUB_TYPE='O' AND M.MI_ORG_ID=H.AI_ORG_ID AND M.MI_INVN_NO=H.AI_ADDR_INVNTRY_NO");
		internalMap.put("EMEMREG", ememregMap);
		// EMEMREG END

		// EMEMADDRCHANGES BEGIN
		HashMap<String, String> ememaddrchangesMap = new HashMap<String, String>();
		ememaddrchangesMap
				.put("ADDRESS_DTL",
						"SELECT H.* FROM "
								+ NAME_SEPERATOR
								+ "MEMADDRCHANGES"
								+ NAME_SEPERATOR
								+ " M,  "
								+ NAME_SEPERATOR
								+ "ADDRINVHIST"
								+ NAME_SEPERATOR
								+ " H WHERE M.MAC_ORG_ID=? AND M.MAC_SMN=? AND M.MAC_ENTRY_DATE=TO_DATE(?,'"
								+ TBA_DATE_FORMAT
								+ "') AND M.MAC_DAY_SL=? AND H.AIH_ORG_ID=M.MAC_ORG_ID AND M.MAC_ADDR_INV_NO_AFTR_CHNG=H.AIH_ADDR_INV_NO");
		internalMap.put("EMEMADDRCHANGES", ememaddrchangesMap);
		// EMEMADDRCHANGES END
	}

	private void loadOltasGridTokens() {

	}

	private void loadFASGridTokens() {
		// MGLMAST BEGIN
		HashMap<String, String> mglmastMap = new HashMap<String, String>();
		mglmastMap
				.put("MGLMAST_ROOT_ARRTIBUTE",
						"SELECT GM.GL_ACCOUNTING_TYPE,GM.BAL_EXCEP_ALLOWED,GM.BAL_SHEET_ITEM_TYPE,GM.CASH_GL,GM.BANK_ACCOUNT_GL,GM.GL_TRAN_TYPE,GM.OPERATING_ACCOUNT,GM.FOLIO_RELATIONSHIP,GM.ENTRY_LEVEL_RECON_REQD ,GM.PRECEED_ENTRY_TYPE,GM.RECON_TYPE,GM.CHQ_ONHAND_GL FROM "
								+ NAME_SEPERATOR
								+ "GLMAST"
								+ NAME_SEPERATOR
								+ " GL, "
								+ NAME_SEPERATOR
								+ "GLMASTROOTATTR"
								+ NAME_SEPERATOR
								+ " GM WHERE GL.PARTITION_NO=? AND GL.GL_HEAD_CODE=? AND GL.PARTITION_NO=GM.PARTITION_NO AND GL.ROOT_GL_HEAD=GM.ROOT_GL_HEAD");
		internalMap.put("MGLMAST", mglmastMap);
		// MGLMAST END
	}

	public static RecordQueryMap getInstance() {
		if (instance == null)
			instance = new RecordQueryMap();
		return instance;
	}

	public String getDescriptor(String programId, String tokenId) {
		return internalMap.get(programId).get(tokenId);
	}
}
