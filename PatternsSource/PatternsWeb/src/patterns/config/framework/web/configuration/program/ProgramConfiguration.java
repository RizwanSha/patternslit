package patterns.config.framework.web.configuration.program;

import java.io.Serializable;
import patterns.json.JSONObject;
public class ProgramConfiguration implements Serializable {

	private static final long serialVersionUID = 4869907043512263269L;

	private String programID;
	private String programDescription;
	private String programClass;
	private String queryProgramID;
	private boolean tfaRequired;
	private boolean sessionRequired;
	private boolean loginAuthenticationContext;
	private boolean forcedPasswordContext;
	private boolean operationLogRequired;
	private boolean refererCheckRequired;
	private boolean allocationRequired;
	private String rendererType;
	private boolean skipCSRFAttackCheck;
	private boolean credentialManagementOption;
	private boolean backToTemplate;
	private boolean printRequired;
	private JSONObject formStyle;
	private JSONObject elementStyleMapping;

	public boolean isLoginAuthenticationContext() {
		return loginAuthenticationContext;
	}

	public void setLoginAuthenticationContext(boolean loginAuthenticationContext) {
		this.loginAuthenticationContext = loginAuthenticationContext;
	}

	public boolean isCredentialManagementOption() {
		return credentialManagementOption;
	}

	public void setCredentialManagementOption(boolean credentialManagementOption) {
		this.credentialManagementOption = credentialManagementOption;
	}

	public boolean isForcedPasswordContext() {
		return forcedPasswordContext;
	}

	public void setForcedPasswordContext(boolean forcedPasswordContext) {
		this.forcedPasswordContext = forcedPasswordContext;
	}

	public String getProgramID() {
		return programID;
	}

	public void setProgramID(String programID) {
		this.programID = programID;
	}

	public String getQueryProgramID() {
		return queryProgramID;
	}

	public void setQueryProgramID(String queryProgramID) {
		this.queryProgramID = queryProgramID;
	}

	public String getProgramDescription() {
		return programDescription;
	}

	public void setProgramDescription(String programDescription) {
		this.programDescription = programDescription;
	}

	public String getProgramClass() {
		return programClass;
	}

	public void setProgramClass(String programClass) {
		this.programClass = programClass;
	}

	public boolean isTfaRequired() {
		return tfaRequired;
	}

	public void setTfaRequired(boolean tfaRequired) {
		this.tfaRequired = tfaRequired;
	}

	public String getRendererType() {
		return rendererType;
	}

	public void setRendererType(String rendererType) {
		this.rendererType = rendererType;
	}

	public boolean isSessionRequired() {
		return sessionRequired;
	}

	public void setSessionRequired(boolean sessionRequired) {
		this.sessionRequired = sessionRequired;
	}

	public boolean isOperationLogRequired() {
		return operationLogRequired;
	}

	public void setOperationLogRequired(boolean operationLogRequired) {
		this.operationLogRequired = operationLogRequired;
	}

	public boolean isAllocationRequired() {
		return allocationRequired;
	}

	public void setAllocationRequired(boolean allocationRequired) {
		this.allocationRequired = allocationRequired;
	}

	public boolean isSkipCSRFAttackCheck() {
		return skipCSRFAttackCheck;
	}

	public void setSkipCSRFAttackCheck(boolean skipCSRFAttackCheck) {
		this.skipCSRFAttackCheck = skipCSRFAttackCheck;
	}

	public boolean isRefererCheckRequired() {
		return refererCheckRequired;
	}

	public void setRefererCheckRequired(boolean refererCheckRequired) {
		this.refererCheckRequired = refererCheckRequired;
	}
	
	public boolean isBackToTemplate() {
		return backToTemplate;
	}

	public void setBackToTemplate(boolean backToTemplate) {
		this.backToTemplate= backToTemplate;
	}
	public boolean isPrintRequired() {
		return printRequired;
	}

	public void setPrintRequired(boolean printRequired) {
		this.printRequired= printRequired;
	}
	
	public JSONObject getFormStyle() {
		return formStyle;
	}

	public void setFormStyle(JSONObject formStyle) {
		this.formStyle = formStyle;
	}

	public JSONObject getElementStyleMapping() {
		return elementStyleMapping;
	}

	public void setElementStyleMapping(JSONObject elementStyleMapping) {
		this.elementStyleMapping = elementStyleMapping;
	}
}