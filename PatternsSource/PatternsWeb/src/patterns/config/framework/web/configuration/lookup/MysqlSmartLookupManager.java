package patterns.config.framework.web.configuration.lookup;

import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.StringTokenizer;

import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.loggers.ApplicationLogger;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.DHTMLXGridUtility;
import patterns.config.framework.web.ajax.AJAXContentManager;
import patterns.config.framework.web.ajax.InterceptorPurpose;
import patterns.json.JSONArray;
import patterns.json.JSONObject;

public class MysqlSmartLookupManager extends AJAXContentManager {

	private String tokenID = "";
	private String programID = "";
	private String arguments = "";
	private String startPosition = "";

	private String buffer = "";
	private String searchText = "";
	private String searchColumn = "";
	private String orderColumn = "";
	private String sortOrder = "";

	private LookupConfiguration lookupConfiguration = null;
	private int positionStart = 0;
	private int positionEnd = 0;
	private String sqlQuery = "";
	ApplicationLogger logger = null;

	public MysqlSmartLookupManager() {
		super();
		logger = ApplicationLogger.getInstance(this.getClass().getSimpleName());
		// logger.logDebug("#()");
	}

	public DTObject getData(DTObject input) {
		// logger.logDebug("getData()");
		DTObject result = new DTObject();
		try {
			LookupMap lookUpMap = LookupMap.getInstance();
			result.set(RESULT, DATA_UNAVAILABLE);
			programID = input.get(PROGRAM_KEY);
			if (programID == null) {
				logger.logError("LOOKUP_PROGRAM_ID not specified");
				return result;
			} else {
				programID = programID.trim().toUpperCase(getContext().getLocale());
				logger.logDebug("setting LOOKUP_PROGRAM_ID : " + programID);
			}
			tokenID = input.get(TOKEN_KEY);
			if (tokenID == null) {
				logger.logError("LOOKUP_TOKEN_KEY not specified");
				return result;
			} else {
				tokenID = tokenID.trim().toUpperCase(getContext().getLocale());
				logger.logDebug("setting LOOKUP_TOKEN_KEY : " + tokenID);
				lookupConfiguration = lookUpMap.getConfiguration(programID, tokenID);
				if (lookupConfiguration == null) {
					logger.logError("Invalid LOOKUP_TOKEN_KEY : " + tokenID);
					return result;
				}
				if (!lookupConfiguration.isDirectAccess()) {
					logger.logError("Detected LOOKUP_TOKEN Attack : " + tokenID);
					return result;
				}
				if (lookupConfiguration.isIntercepted()) {
					input = processFilter(InterceptorPurpose.LOOKUP_PROCESS_RESOLVE, lookupConfiguration.getInterceptionClass(), input);
					if (input == null) {
						logger.logError("Invalid LOOKUP_INTERCEPTION : " + tokenID);
						return result;
					}
					programID = input.get(PROGRAM_KEY);
					tokenID = input.get(TOKEN_KEY);
					lookupConfiguration = lookUpMap.getConfiguration(programID, tokenID);
					if (lookupConfiguration == null) {
						logger.logError("Invalid LOOKUP_TOKEN_KEY : " + tokenID);
						return result;
					}
				}
			}
			if (!input.containsKey(ARGUMENT_KEY)) {
				logger.logDebug("setting LOOKUP_ARGS : default");
				arguments = EMPTY_STRING;
				input.set(ARGUMENT_KEY, EMPTY_STRING);
			} else {
				arguments = input.get(ARGUMENT_KEY);
				logger.logDebug("setting LOOKUP_ARGS : " + arguments);
			}
			if (!input.containsKey(START_POSITION_KEY)) {
				// logger.logDebug("setting START_POSITION_KEY : default");
				startPosition = DEFAULT_POSITION;
			} else {
				startPosition = input.get(START_POSITION_KEY).trim();
				// logger.logDebug("setting START_POSITION_KEY : " + startPosition);
				try {
					Integer.parseInt(startPosition);
				} catch (NumberFormatException e) {
					logger.logError("Invalid START_POSITION_KEY : " + startPosition);
					return result;
				}
			}

			if (!input.containsKey(BUFFER_KEY)) {
				// logger.logDebug("setting BUFFER_KEY : default");
				buffer = DEFAULT_BUFFER;
				input.set(BUFFER_KEY, DEFAULT_BUFFER);
			} else {
				buffer = input.get(BUFFER_KEY).trim();
				// logger.logDebug("setting BUFFER_KEY : " + buffer);
				try {
					Integer.parseInt(buffer);
				} catch (NumberFormatException e) {
					logger.logError("Invalid BUFFER_KEY : " + buffer);
					return result;
				}
				buffer = DEFAULT_BUFFER;
			}

			if (!input.containsKey(SEARCH_TEXT)) {
				// logger.logDebug("setting SEARCH_TEXT : default");
				searchText = EMPTY_STRING;
			} else {
				searchText = input.get(SEARCH_TEXT).trim();
				// logger.logDebug("setting SEARCH_TEXT : " + searchText);
			}
			if (!input.containsKey(SEARCH_COLUMN)) {
				// logger.logDebug("setting SEARCH_COLUMN : default");
				searchColumn = DEFAULT_POSITION;

			} else {
				searchColumn = input.get(SEARCH_COLUMN).trim();
				// logger.logDebug("setting SEARCH_COLUMN : " + searchColumn);
				try {
					Integer.parseInt(searchColumn);
				} catch (NumberFormatException e) {
					logger.logError("Invalid SEARCH_COLUMN : " + searchColumn);
					return result;
				}
			}

			if (!input.containsKey(ORDER_COLUMN)) {
				// logger.logDebug("setting ORDER_COLUMN : default");
				orderColumn = "1";
			} else {
				orderColumn = input.get(ORDER_COLUMN).trim();
				// logger.logDebug("setting ORDER_COLUMN : " + orderColumn);
				try {
					Integer.parseInt(orderColumn);
				} catch (NumberFormatException e) {
					logger.logError("Invalid ORDER_COLUMN : " + orderColumn);
					return result;
				}
			}
			if (!input.containsKey(SORT_ORDER)) {
				// logger.logDebug("setting SORT_ORDER : default");
				sortOrder = SORT_ASC;
				input.set(SORT_ORDER, SORT_ASC);
			} else {
				sortOrder = input.get(SORT_ORDER).trim();
				// logger.logDebug("setting SORT_ORDER : " + sortOrder);
				if (!(sortOrder.equals(SORT_ASC) || sortOrder.equals(SORT_DESC))) {
					logger.logError("Invalid SORT_ORDER : " + sortOrder);
					return result;
				}
			}

			generateSQLStatement(result);
			if (result.hasError()) {
				logger.logError("SQL Statement Generation has error");
				return result;
			}
			executeSQLStatement(result);
			if (result.hasError()) {
				logger.logError("SQL Statement Generation has error");
				return result;
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError("getData(1) : " + e.getLocalizedMessage() + " : " + input);
			result.set(RESULT, DATA_UNAVAILABLE);
		}
		return result;
	}

	private void generateSQLStatement(DTObject result) throws Exception {
		// logger.logDebug("generateSQLStatement()");
		String basicSQL = lookupConfiguration.getSqlQuery();

		positionStart = Integer.parseInt(startPosition);
		positionEnd = positionStart + Integer.parseInt(buffer);

		StringBuffer completeSQL = new StringBuffer();
		completeSQL.append("SELECT ");
		int columnCount = 0;
		for (String columnName : lookupConfiguration.getColumnNames()) {
			columnCount++;
			completeSQL.append("XM." + columnName);
			if (columnCount != lookupConfiguration.getColumnNames().length)
				completeSQL.append(",");
			completeSQL.append(" ");

		}
		completeSQL.append(",@ROWNUM:=@ROWNUM+1 ROWNUM FROM (SELECT XI.* FROM ( ");
		completeSQL.append(basicSQL);
		completeSQL.append(" ) ");
		if (!searchText.equals(EMPTY_STRING)) {
			completeSQL.append(" XI WHERE  ");
			switch (lookupConfiguration.getColumnTypes()[Integer.parseInt(searchColumn)]) {
			case VARCHAR:
			default:
				searchText = "%" + searchText + "%";
				completeSQL.append(" LOWER(XI." + lookupConfiguration.getColumnNames()[Integer.parseInt(searchColumn)].toUpperCase(getContext().getLocale()) + ") LIKE  LOWER(?) ");
				break;
			case DATE:
			case TIMESTAMP:
				completeSQL.append(" XI." + lookupConfiguration.getColumnNames()[Integer.parseInt(searchColumn)].toUpperCase(getContext().getLocale()) + " >= str_to_date(?,'#DATE_FORMAT#') ");
				break;
			case BIGDECIMAL:
			case INTEGER:
			case LONG:
				completeSQL.append(" XI." + lookupConfiguration.getColumnNames()[Integer.parseInt(searchColumn)].toUpperCase(getContext().getLocale()) + " = ? ");
				break;
			}
		}

		if (!searchText.equals(EMPTY_STRING))
			completeSQL.append(" ORDER BY (" + orderColumn + ") " + sortOrder + " ) XM,(SELECT @rownum:=0) XXM ");
		else
			completeSQL.append(" XI ORDER BY (" + orderColumn + ") " + sortOrder + " ) XM,(SELECT @rownum:=0) XXM ");

		basicSQL = preProcessSQL(completeSQL.toString());
		completeSQL.setLength(0);
		completeSQL.append(basicSQL);
		completeSQL.append(" LIMIT " + positionEnd);
		sqlQuery = "SELECT R.* FROM (" + completeSQL.toString() + " ) R WHERE R.ROWNUM > " + positionStart;
		logger.logInfo(sqlQuery);
	}

	private void executeSQLStatement(DTObject result) {
		// logger.logDebug("executeSQLStatement()");
		DBUtil util = getDbContext().createUtilInstance();
		int bindValuesCount = 0;
		int argumentCount = 0;
		int _index = 1;
		try {
			result.set(RESULT, ROW_NOT_PRESENT);
			util.reset();
			util.setSql(sqlQuery);
			if (sqlQuery.indexOf("?") > -1) {
				bindValuesCount = (new StringTokenizer(sqlQuery, "?").countTokens()) - 1;
				String[] sql_arguments = arguments.split("\\|");
				if (arguments.equals(EMPTY_STRING)) {
					argumentCount = 0;
				} else {
					argumentCount = sql_arguments.length;
				}
				if (argumentCount == 0 && bindValuesCount > 0) {
					util.setString(_index, searchText);
				} else if (bindValuesCount != argumentCount) {
					for (int k = 1; k <= argumentCount; k++) {
						util.setString(_index, sql_arguments[k - 1]);
						_index++;
					}
					util.setString(_index, searchText);
				} else if ((bindValuesCount == argumentCount) && argumentCount > 0) {
					for (int k = 1; k <= argumentCount; k++) {
						util.setString(_index, sql_arguments[k - 1]);
						_index++;
					}
				}
			}

			ResultSet resultSet = util.executeQuery();
			JSONObject jsonStr = getJSON(resultSet);
			if (jsonStr !=null) {
				result.set(RESULT, ROW_PRESENT);
				result.setObject(CONTENT, jsonStr);
				result.set(RESULT, DATA_AVAILABLE);
			}
			util.reset();

		} catch (Exception e) {
			e.printStackTrace();
			logger.logError(" executeSQLStatement(1) - " + e.getLocalizedMessage());
			result.set(RESULT, DATA_UNAVAILABLE);
		} finally {
		}
	}
	
	public String getXML(ResultSet resultSet) {
		// logger.logDebug("getXML()");
		DHTMLXGridUtility utility = new DHTMLXGridUtility();
		int colCount;
		ResultSet rsGeneric = null;
		try {
			rsGeneric = resultSet;
			colCount = rsGeneric.getMetaData().getColumnCount();
			SimpleDateFormat sdf = new SimpleDateFormat(BackOfficeConstants.JAVA_TBA_DATE_FORMAT);
			utility.init();

			while (rsGeneric.next()) {
				utility.startRow(rsGeneric.getInt(colCount) + "");
				for (int i = 1; i < colCount; i++) {
					if (rsGeneric.getMetaData().getColumnTypeName(i).equals("DATE")) {
						if (rsGeneric.getDate(i) == null) {
							utility.setCell("");
						} else {
							utility.setCell(sdf.format(rsGeneric.getDate(i)));
						}
					} else {
						if (rsGeneric.getString(i) == null) {
							utility.setCell("");
						} else {
							utility.setCell(rsGeneric.getString(i));
						}
					}
				}
				utility.endRow();
			}

		} catch (Exception e) {
			e.printStackTrace();
			logger.logError(" getXML(1) - " + e.getLocalizedMessage());
			return "";
		}
		utility.finish();
		return utility.getXML();
	}

	public JSONObject getJSON(ResultSet resultSet) {
		// logger.logDebug("getXML()");
		JSONObject responseDetailsJson = null;
		JSONArray jsonArray = new JSONArray();
		ResultSet rsGeneric = null;
		try {
			rsGeneric = resultSet;
			
			JSONObject formDetailsJson = new JSONObject();
			formDetailsJson.put("value", "");
			formDetailsJson.put("text", "-SELECT-");
			jsonArray.put(formDetailsJson);
			
			while (rsGeneric.next()) {
				formDetailsJson = new JSONObject();
				formDetailsJson.put("value", rsGeneric.getString(1));
				formDetailsJson.put("text", rsGeneric.getString(2));
				jsonArray.put(formDetailsJson);
			}
			if (jsonArray.length() > 1) {
				responseDetailsJson = new JSONObject();
				responseDetailsJson.put("options", jsonArray);
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError(" getXML(1) - " + e.getLocalizedMessage());
		}
		return responseDetailsJson;
	}
}
