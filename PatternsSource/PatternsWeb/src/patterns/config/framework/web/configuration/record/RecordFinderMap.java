package patterns.config.framework.web.configuration.record;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import patterns.config.framework.database.BindParameterType;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.loggers.ApplicationLogger;
import patterns.config.framework.web.configuration.lookup.LookupMap;

public class RecordFinderMap {

	private static final ReentrantReadWriteLock readWriteLock = new ReentrantReadWriteLock();

	// private static final Lock read = readWriteLock.readLock();

	private static final Lock write = readWriteLock.writeLock();

	private static final ReentrantReadWriteLock readWriteLockGlobal = new ReentrantReadWriteLock();

	private static final Lock writeGlobal = readWriteLockGlobal.writeLock();

	private ApplicationLogger logger = null;

	private Map<String, Map<String, RecordFinderConfiguration>> internalMap;
	private Map<String, Map<String, RecordFinderConfiguration>> internalLocalMap;

	private static volatile RecordFinderMap instance = null;

	private RecordFinderMap() {
		logger = ApplicationLogger.getInstance(this.getClass().getSimpleName());
		logger.logDebug("#()");
	}

	public RecordFinderConfiguration getConfiguration(String programID, String tokenID) {
		logger.logDebug("getConfiguration()");
		RecordFinderConfiguration configuration = null;
		try {
			// read.lock();
			Map<String, RecordFinderConfiguration> programLevelMap = internalMap.get(programID);
			if (programLevelMap == null || programLevelMap.size() == 0) {
				logger.logError("#Empty ProgramLevelMap :: " + programID + " -- " + tokenID);
				return null;
			}
			configuration = programLevelMap.get(tokenID);
			if (configuration == null) {
				logger.logError("#Empty Configuration :: " + programID + " -- " + tokenID);
				return null;
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError(" getConfiguration() - " + e.getLocalizedMessage());
		} finally {
			// read.unlock();
		}
		return configuration;
	}

	private void initConfiguration() {
		internalMap.putAll(internalLocalMap);
		internalLocalMap.clear();
	}

	private void resetConfiguration() {
		logger.logDebug("resetConfiguration()");
		if (internalMap == null)
			internalMap = new HashMap<String, Map<String, RecordFinderConfiguration>>(1000, 1F);
		internalMap.clear();
	}

	public static void unloadConfiguration() {
		try {
			writeGlobal.lock();
			if (instance != null) {
				instance.resetConfiguration();
				instance = null;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			writeGlobal.unlock();
		}
	}

	public static void reloadConfiguration() {
		try {
			writeGlobal.lock();
			if (instance == null) {
				instance = new RecordFinderMap();
			}
			instance.loadConfiguration();
			try {
				write.lock();
				instance.resetConfiguration();
				instance.initConfiguration();
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				write.unlock();
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			writeGlobal.unlock();
		}
	}

	private void loadConfiguration() {
		logger.logDebug("loadConfiguration()");
		DBContext dbContext = new DBContext();
		if (internalLocalMap == null)
			internalLocalMap = new HashMap<String, Map<String, RecordFinderConfiguration>>(1000, 1F);
		internalLocalMap.clear();
		HashMap<String, RecordFinderConfiguration> helpTokens = null;
		try {
			DBUtil utilOuter = dbContext.createUtilInstance();
			utilOuter.reset();
			String sqlQuery = "SELECT PROGRAM_ID,TOKEN_ID,SQL_QUERY,SPLIT_AT,DIRECT_ACCESS,INTERCEPT_REQD,INTERCEPTION_CLASS FROM QUERYGRIDMASTER ORDER BY PROGRAM_ID,TOKEN_ID";
			utilOuter.setSql(sqlQuery);
			ResultSet rs = utilOuter.executeQuery();
			while (rs.next()) {
				RecordFinderConfiguration recordFinderConfiguration = null;
				String programID = rs.getString("PROGRAM_ID");
				String tokenID = rs.getString("TOKEN_ID");
				String tokenSQLQuery = rs.getString("SQL_QUERY");
				String directAccess = rs.getString("DIRECT_ACCESS");
				String intercepted = rs.getString("INTERCEPT_REQD");
				String interceptionClass = rs.getString("INTERCEPTION_CLASS");
				int columnCount = 0;
				if (!internalLocalMap.containsKey(programID)) {
					helpTokens = new HashMap<String, RecordFinderConfiguration>(2, 1F);
					internalLocalMap.put(programID, helpTokens);
				}
				if (!helpTokens.containsKey(tokenID)) {
					recordFinderConfiguration = new RecordFinderConfiguration();
					recordFinderConfiguration.setProgramID(programID);
					recordFinderConfiguration.setTokenID(tokenID);
					if (directAccess != null && directAccess.equals("1")) {
						recordFinderConfiguration.setDirectAccess(true);
					} else {
						recordFinderConfiguration.setDirectAccess(false);
					}
					if (intercepted != null && intercepted.equals("1")) {
						recordFinderConfiguration.setIntercepted(true);
						recordFinderConfiguration.setInterceptionClass(interceptionClass);
					} else {
						recordFinderConfiguration.setIntercepted(false);
					}
					helpTokens.put(tokenID, recordFinderConfiguration);
				}
				if (!recordFinderConfiguration.isIntercepted() || (tokenSQLQuery != null && !tokenSQLQuery.equals(RegularConstants.EMPTY_STRING))) {
					recordFinderConfiguration.setSqlQuery(tokenSQLQuery);
					DBUtil utilInner = dbContext.createUtilInstance();
					sqlQuery = "SELECT COUNT(COLUMN_SL) FROM QUERYGRIDMASTERCOLS WHERE PROGRAM_ID=? and TOKEN_ID=? ORDER BY COLUMN_SL";
					utilInner.setSql(sqlQuery);
					utilInner.setString(1, programID);
					utilInner.setString(2, tokenID);
					ResultSet rs1 = utilInner.executeQuery();
					if (rs1.next()) {
						columnCount = rs1.getInt(1);
					}
					utilInner.reset();
					String[] columnNames;
					BindParameterType[] columnTypes;
					String[] columnUI;
					String[] columnAlign;
					boolean[] searchColumns;
					boolean[] hiddenColumns;
					String[] columnWidths;
					String[] columnDescriptions_en_US;
					String[] predefinedColumns;
					String[] aliasNames;
					String[] defaultSorting;
					if (columnCount > 0) {
						aliasNames = new String[columnCount];
						predefinedColumns = new String[columnCount];
						defaultSorting = new String[columnCount];
						columnNames = new String[columnCount];
						columnTypes = new BindParameterType[columnCount];
						hiddenColumns = new boolean[columnCount];
						searchColumns = new boolean[columnCount];
						columnWidths = new String[columnCount];
						columnDescriptions_en_US = new String[columnCount];
						columnAlign = new String[columnCount];
						columnUI = new String[columnCount];
						sqlQuery = "SELECT ALIAS_NAME,COLUMN_NAME,COLUMN_TYPE,COLUMN_HIDDEN,SEARCH_ALLOWED,COLUMN_WIDTH,COLUMN_DESC_EN_US,DEFAULT_SORT,PREDEFINE_COLS FROM QUERYGRIDMASTERCOLS where PROGRAM_ID=? and TOKEN_ID=? ORDER BY COLUMN_SL";
						utilInner.setSql(sqlQuery);
						utilInner.setString(1, programID);
						utilInner.setString(2, tokenID);
						rs1 = utilInner.executeQuery();
						int j = 0;
						while (rs1.next()) {
							aliasNames[j] = rs1.getString(1);
							columnNames[j] = rs1.getString(2);
							columnTypes[j] = LookupMap.getBindDataType(rs1.getString(3));
							columnAlign[j] = getColumnAlignment(rs1.getString(3));
							columnUI[j] = getColumnUI(rs1.getString(3));
							String columnHidden = rs1.getString(4);
							if (columnHidden != null && columnHidden.equals("1")) {
								hiddenColumns[j] = true;
							} else {
								hiddenColumns[j] = false;
							}
							String searchColumn = rs1.getString(5);
							if (searchColumn != null && searchColumn.equals("1")) {
								searchColumns[j] = true;
							} else {
								searchColumns[j] = false;
							}
							columnWidths[j] = rs1.getString(6);
							columnDescriptions_en_US[j] = rs1.getString(7);
							defaultSorting[j] = rs1.getString(8);
							predefinedColumns[j] = rs1.getString(9);
							j++;
						}
						utilInner.reset();
						recordFinderConfiguration.setColumnNames(columnNames);
						recordFinderConfiguration.setColumnTypes(columnTypes);
						recordFinderConfiguration.setColumnUI(columnUI);
						recordFinderConfiguration.setColumnAlign(columnAlign);
						recordFinderConfiguration.setHiddenColumns(hiddenColumns);
						recordFinderConfiguration.setSearchColumns(searchColumns);
						recordFinderConfiguration.setColumnWidths(columnWidths);
						recordFinderConfiguration.setColumnHeadings_en_US(columnDescriptions_en_US);
						recordFinderConfiguration.setAliasNames(aliasNames);
						recordFinderConfiguration.setDefaultSort(defaultSorting);
						recordFinderConfiguration.setPredefinedColumns(predefinedColumns);
					}
				}
				/* to fetch filters for the program and token Id */
				DBUtil filterUtil = dbContext.createUtilInstance();
				sqlQuery = "SELECT COLUMN_NAME,COLUMN_TYPE,COLUMN_DESC_EN_US,COLUMN_MANDATORY,COLUMN_DEFAULT_VALUE,COMBO_PGM_ID,COMBO_LOV_ID,ALIAS_NAME FROM QUERYGRIDFILTERS WHERE PROGRAM_ID=? and TOKEN_ID=? ORDER BY COLUMN_SL";
				filterUtil.setSql(sqlQuery);
				filterUtil.setString(1, programID);
				filterUtil.setString(2, tokenID);
				ResultSet rset = filterUtil.executeQuery();
				ArrayList<RecordFinderFilter> filterList = new ArrayList<RecordFinderFilter>();
				while (rset.next()) {
					RecordFinderFilter filter = new RecordFinderFilter();
					filter.setColumnName(rset.getString(1));
					filter.setColumnType(rset.getString(2));
					filter.setColumnDescription(rset.getString(3));
					filter.setMandatory(rset.getBoolean(4));
					filter.setDefaultValue(rset.getString(5));
					filter.setComboProgramId(rset.getString(6));
					filter.setComboLovId(rset.getString(7));
					filter.setAliasNames(rset.getString(8));
					filterList.add(filter);
				}
				filterUtil.reset();
				recordFinderConfiguration.setRecordFinderFilter(filterList);

			}
			utilOuter.reset();
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError(" loadConfiguration() - " + e.getLocalizedMessage());
		} finally {
			dbContext.close();
		}
	}

	public static RecordFinderMap getInstance() {
		if (instance == null) {
			reloadConfiguration();
		}
		return instance;
	}

	public static String getColumnUI(String dataType) throws Exception {
		String uiType = null;
		char coldataType = dataType.charAt(0);
		switch (coldataType) {
		case '1':
		default:
			uiType = RecordFinderConfiguration.UITYPE_RO;
			break;
		case '2':
			uiType = RecordFinderConfiguration.UITYPE_RO;
			break;
		case '3':
			uiType = RecordFinderConfiguration.UITYPE_RO;
			break;
		case '4':
			uiType = RecordFinderConfiguration.UITYPE_RO;
			break;
		case '5':
			uiType = RecordFinderConfiguration.UITYPE_RO;
			break;
		case '6':
			uiType = RecordFinderConfiguration.UITYPE_RO;
			break;
		case '8':
			uiType = RecordFinderConfiguration.UITYPE_LINK;
			break;
		case '9':
			uiType = RecordFinderConfiguration.UITYPE_CH;
			break;
		}
		return uiType;
	}

	public static String getColumnAlignment(String dataType) throws Exception {
		String alignMode = null;
		char coldataType = dataType.charAt(0);
		switch (coldataType) {
		case '1':
		default:
			alignMode = RecordFinderConfiguration.ALIGN_LEFT;
			break;
		case '2':
			alignMode = RecordFinderConfiguration.ALIGN_LEFT;
			break;
		case '3':
			alignMode = RecordFinderConfiguration.ALIGN_LEFT;
			break;
		case '4':
			alignMode = RecordFinderConfiguration.ALIGN_LEFT;
			break;
		case '5':
			alignMode = RecordFinderConfiguration.ALIGN_LEFT;
			break;
		case '6':
			alignMode = RecordFinderConfiguration.ALIGN_RIGHT;
			break;
		case '8':
			alignMode = RecordFinderConfiguration.ALIGN_CENTER;
			break;
		case '9':
			alignMode = RecordFinderConfiguration.ALIGN_CENTER;
			break;
		}
		return alignMode;
	}
}