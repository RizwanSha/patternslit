package patterns.config.framework.web.configuration.mtm;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.BindParameterType;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.objects.TableInfo;
import patterns.config.framework.database.utils.DBInfo;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.loggers.ApplicationLogger;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.AJAXContentManager;
import patterns.config.framework.web.ajax.ContentManager;

public class ComponentGridQueryManager extends AJAXContentManager {

	private static final String TOKEN_ID = "_TokenID";
	private static final String PARENT_PROGRAM_ID = "_ParentProgramId";
	private static final String PARENT_PRIMARY_KEY = "_ParentPrimaryKey";
	private static final String LINK_PROGRAM_ID = "_LinkProgramId";
	private static final String LINK_PRIMARY_KEY = "_LinkPrimaryKey";
	private static final String CGQM_CONV_ID = "_CGQMConversationId";
	private static final String REQUEST_TYPE = "_ReqType";
	public static final String COMP_NAME_SEPERATOR = "###";
	public static final String TBA_NAME_SEPERATOR = "@@";

	public static final String QUERY_MAIN = "M";
	public static final String QUERY_TBA = "T";
	public static final String QUERY_BOTH = "B";

	private ApplicationLogger logger = null;
	private TableInfo tableInfo = null;
	private ResultSet rset = null;

	private String parentProgramID;
	private String parentPrimaryKeyValues = null;
	private String conversationID = null;
	private String linkProgramID;
	private String linkPrimaryKeyValues = null;

	private StringBuffer sqlQueryBuffer = null;
	private String entryDate = null;
	private String mTable;
	private ArrayList<String> mTableArrayList = new ArrayList<String>();
	private long entrySl;
	int counter = 0;
	private String requestType = RegularConstants.EMPTY_STRING;

	public ComponentGridQueryManager() {
		super();
		logger = ApplicationLogger.getInstance(this.getClass().getSimpleName());
		// logger.logDebug("#()");
	}

	public DTObject getData(DTObject input) {
		DTObject result = new DTObject();
		try {
			GridQueryMap dataMap = GridQueryMap.getInstance();
			parentProgramID = input.get(PARENT_PROGRAM_ID).toUpperCase(getContext().getLocale());
			parentPrimaryKeyValues = input.get(PARENT_PRIMARY_KEY).trim();
			linkProgramID = input.get(LINK_PROGRAM_ID);
			linkPrimaryKeyValues = input.get(LINK_PRIMARY_KEY);
			conversationID=input.get(CGQM_CONV_ID);
			requestType = input.get(REQUEST_TYPE).trim();
			String tokenID = input.get(TOKEN_ID);
			String sqlQuery = dataMap.getDescriptor(linkProgramID, tokenID);
			sqlQuery = preProcessSQL(sqlQuery);
			StringBuffer buffer = new StringBuffer("");
			sqlQueryBuffer = new StringBuffer(sqlQuery);

			formTBAQuery();
			formLinkTBAQuery();

			if (requestType.equals("S")) {
				buffer = executeProgramLinkSQLQuery();
			} else {
				buffer = executeSQLQuery();

				if (requestType.equals(QUERY_BOTH)) {
					if (counter == 0) {
						requestType = QUERY_MAIN;
						sqlQueryBuffer = new StringBuffer(sqlQuery);
						formTBAQuery();
						formLinkTBAQuery();
						buffer = executeSQLQuery();
					}
				}
			}

			System.out.println("Final Query:" + sqlQueryBuffer);
			result.set(ContentManager.CONTENT, buffer.toString());
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError(" getData(1) : " + e.getLocalizedMessage() + " : " + input);
			result.set(ERROR, e.getLocalizedMessage());
			result.set(RESULT, DATA_UNAVAILABLE);
		} finally {
		}
		return result;
	}

	private boolean formTBAQuery() {
		String pattern = GridQueryMap.NAME_SEPERATOR + "([A-Za-z0-9_])+" + GridQueryMap.NAME_SEPERATOR;
		Pattern p = Pattern.compile(pattern);
		int start = 0;
		int end = 0;
		do {
			Matcher m = p.matcher(sqlQueryBuffer.toString());
			if (m.find()) {
				start = m.start();
				end = m.end();
				Pattern p1 = Pattern.compile("([A-Za-z0-9_])+");
				Matcher m1 = p1.matcher(m.group());
				if (m1.find()) {
					mTable = m1.group();
					mTableArrayList.add(mTable);
				}
				sqlQueryBuffer.replace(start, end, getTableName(mTable, false));
			} else {
				break;
			}
		} while (true);

		return true;
	}

	private boolean formLinkTBAQuery() {
		String pattern = COMP_NAME_SEPERATOR + "([A-Za-z0-9_])+" + COMP_NAME_SEPERATOR;
		Pattern p = Pattern.compile(pattern);
		int start = 0;
		int end = 0;
		do {
			Matcher m = p.matcher(sqlQueryBuffer.toString());
			if (m.find()) {
				start = m.start();
				end = m.end();
				Pattern p1 = Pattern.compile("([A-Za-z0-9_])+");
				Matcher m1 = p1.matcher(m.group());
				if (m1.find()) {
					mTable = m1.group();
					mTableArrayList.add(mTable);
				}
				sqlQueryBuffer.replace(start, end, getTableName(mTable, true));
			} else {
				break;
			}
		} while (true);

		return true;
	}

	private String getTableName(String mtable, boolean isComponent) {
		StringBuffer buffer = new StringBuffer();
		DBUtil dbutil = getDbContext().createUtilInstance();
		StringBuffer query = new StringBuffer();
		try {
			if (requestType.equals(QUERY_MAIN)) {
				buffer.append(mtable);
				return buffer.toString();
			}
			if (!isComponent) {
				buffer.append("( SELECT");
				dbutil.reset();
				dbutil.setMode(DBUtil.PREPARED);
				dbutil.setSql("SELECT TO_CHAR(TBAQ_ENTRY_DATE,'%d-%m-%Y') TBAQ_ENTRY_DATE,TBAQ_ENTRY_SL,TBAQ_OPERN_FLG FROM TBAAUTHQ WHERE ENTITY_CODE = ?  AND  TBAQ_PGM_ID = ? AND TBAQ_MAIN_PK = ? ");
				dbutil.setString(1, getContext().getEntityCode());
				dbutil.setString(2, parentProgramID);
				dbutil.setString(3, parentPrimaryKeyValues);
				ResultSet rs = dbutil.executeQuery();
				if (rs.next()) {
					entryDate = rs.getString("TBAQ_ENTRY_DATE");
					entrySl = rs.getLong("TBAQ_ENTRY_SL");
				}
				DBInfo info = new DBInfo();
				tableInfo = info.getTableInfo(mtable);
				Set<String> noUpdateColumnInfo = tableInfo.getNoUpdationColumnInfo();
				String[] fieldNames = tableInfo.getColumnInfo().getColumnNames();
				Map<String, BindParameterType> columnType = tableInfo.getColumnInfo().getColumnMapping();
				for (String fieldName : fieldNames) {
					if (!noUpdateColumnInfo.contains(fieldName)) {
						if (columnType.get(fieldName).equals(BindParameterType.DATE)) {
							query.append(" TO_DATE(EXTRACTVALUE(T.TBADTL_DATA_BLOCK,'//" + fieldName + "'),'"
									+ BackOfficeConstants.JAVA_TBA_DB_DATE_FORMAT + "')" + fieldName + ",");
						} else {
							query.append(" EXTRACTVALUE(T.TBADTL_DATA_BLOCK,'//" + fieldName + "')" + fieldName + ",");
						}
					}
				}
				query.trimToSize();
				buffer.append(query.substring(0, query.length() - 1));
				buffer.append(" FROM TBAAUTHDTL T WHERE T.ENTITY_CODE='").append(getContext().getEntityCode());
				buffer.append("' AND T.TBADTL_PGM_ID = '").append(parentProgramID);
				buffer.append("' AND T.TBADTL_MAIN_PK='").append(parentPrimaryKeyValues);
				buffer.append("' AND T.TBADTL_ENTRY_DATE=TO_DATE('").append(entryDate);
				buffer.append("','%d-%m-%Y') AND T.TBADTL_DTL_SL= '").append(entrySl);
				buffer.append("' AND T.TBADTL_TABLE_NAME='").append(mtable);
				buffer.append("' )");
			} else {
				buffer.append("( SELECT");
				DBInfo info = new DBInfo();
				tableInfo = info.getTableInfo(mtable);
				Set<String> noUpdateColumnInfo = tableInfo.getNoUpdationColumnInfo();
				String[] fieldNames = tableInfo.getColumnInfo().getColumnNames();
				Map<String, BindParameterType> columnType = tableInfo.getColumnInfo().getColumnMapping();
				for (String fieldName : fieldNames) {
					if (!noUpdateColumnInfo.contains(fieldName)) {
						if (columnType.get(fieldName).equals(BindParameterType.DATE)) {
							query.append(" TO_DATE(EXTRACTVALUE(TC.DATA_BLOCK,'//" + fieldName + "'),'"
									+ BackOfficeConstants.JAVA_TBA_DB_DATE_FORMAT + "')" + fieldName + ",");
						} else {
							query.append(" EXTRACTVALUE(TC.DATA_BLOCK,'//" + fieldName + "')" + fieldName + ",");
						}
					}
				}

				query.trimToSize();
				buffer.append(query.substring(0, query.length() - 1));
				buffer.append(" FROM PGMLINKEDTBA TC WHERE TC.ENTITY_CODE='").append(getContext().getEntityCode());
				buffer.append("' AND TC.PARENT_PGM_ID = '").append(parentProgramID);
				buffer.append("' AND TC.LINKED_PGM_ID='").append(linkProgramID);
				// buffer.append("' AND TC.LINKED_PGM_PK='").append(linkProgramPrimaryKeyValues);
				buffer.append("' AND TC.TABLE_NAME='").append(mtable);
				if(parentPrimaryKeyValues!=null && !parentPrimaryKeyValues.equals(RegularConstants.EMPTY_STRING)){
					buffer.append("' AND ( TC.PARENT_PGM_PK = '").append(parentPrimaryKeyValues);
					buffer.append("' OR TC.CONVERSATION_ID = '").append(conversationID);
					buffer.append("' ))");
				}else{
					buffer.append("' AND TC.CONVERSATION_ID = '").append(conversationID);
					buffer.append("' )");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError(" getTableName(1) : " + e.getLocalizedMessage());
			buffer.append("");
		} finally {
			dbutil.reset();
		}
		return buffer.toString();
	}

	private StringBuffer executeSQLQuery() throws Exception {
		StringBuffer buffer = new StringBuffer(1024);
		DBUtil util = getDbContext().createUtilInstance();
		int index = 1;
		try {
			buffer.append("<rows pos='0'>");

			util.reset();
			util.setSql(sqlQueryBuffer.toString());

			String[] arguments;
			int i = 0;
			if (linkPrimaryKeyValues != null && !linkPrimaryKeyValues.equals(RegularConstants.EMPTY_STRING)) {
				arguments = linkPrimaryKeyValues.split("\\|");
				for (int j = i; j < arguments.length && linkPrimaryKeyValues.length() != 0; ++j) {
					util.setString(j + index, arguments[j]);
				}
			} else {
				arguments = parentPrimaryKeyValues.split("\\|");
				for (int j = i; j < arguments.length && parentPrimaryKeyValues.length() != 0; ++j) {
					util.setString(j + index, arguments[j]);
				}
			}

			rset = util.executeQuery();

			int columnCount = rset.getMetaData().getColumnCount();
			i = 1;
			while (rset.next()) {
				buffer.append("<row id='" + i + "'>");
				buffer.append("<cell>0</cell>");
				for (int k = 1; k <= columnCount; ++k) {
					buffer.append("<cell><![CDATA[");
					buffer.append(rset.getString(k));
					buffer.append("]]></cell>");
				}
				buffer.append("</row>");
				++i;
				counter++;
			}
			buffer.append("</rows>");
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError("executeSQLQuery(1) : " + e.getLocalizedMessage());
		} finally {
			util.reset();

		}
		return buffer;
	}
	
	private StringBuffer executeProgramLinkSQLQuery() throws Exception {
		StringBuffer buffer = new StringBuffer(1024);
		DBUtil util = getDbContext().createUtilInstance();
		try {
			buffer.append("<rows pos='0'>");

			util.reset();
			util.setSql(sqlQueryBuffer.toString());

			rset = util.executeQuery();

			int columnCount = rset.getMetaData().getColumnCount();
			int i = 1;
			while (rset.next()) {
				buffer.append("<row id='" + i + "'>");
				buffer.append("<cell>0</cell>");
				for (int k = 1; k <= columnCount; ++k) {
					buffer.append("<cell><![CDATA[");
					buffer.append(rset.getString(k));
					buffer.append("]]></cell>");
				}
				buffer.append("</row>");
				++i;
			}
			buffer.append("</rows>");
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError("executeSQLQuery(1) : " + e.getLocalizedMessage());
		} finally {
			util.reset();

		}
		return buffer;
	}
}
