package patterns.config.framework.web.configuration.mtm;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import patterns.config.framework.DatasourceConfigurationManager;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.BindParameterType;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.objects.TableInfo;
import patterns.config.framework.database.utils.DBInfo;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.database.utils.MySqlDBInfo;
import patterns.config.framework.database.utils.OrclDBInfo;
import patterns.config.framework.loggers.ApplicationLogger;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.framework.web.ajax.AJAXContentManager;

public class ComponentMtmManager extends AJAXContentManager {

	private ApplicationLogger logger = null;

	private static final String MTM_REQUEST_TYPE = "_MtmReqType";
	private static final String MTM_PARENT_PROGRAM_ID = "_MtmParentProgramID";
	private static final String MTM_PARENT_PRIMARY_KEY = "_MtmParentPrimaryKey";
	private static final String MTM_LINKED_PROGRAM_ID = "_MtmLinkedProgramID";
	private static final String MTM_LINKED_PRIMARY_KEY = "_MtmLinkedPrimaryKey";

	private static final String MTM_ALT_TABLE_NAME = "_MtmAltTable";

	private String reqtype = RegularConstants.EMPTY_STRING;
	private String[] arguments;
	private String parentPrimaryKeyValues;
	private String linkedPrimaryKeyValues;
	private String mainTable = RegularConstants.NULL;
	private String sqlQuery = RegularConstants.NULL;
	private String[] keyFields;
	private BindParameterType[] keyFieldTypes;
	private DTObject result = new DTObject();
	private TableInfo tableInfo = null;
	private String parentProgramID = RegularConstants.NULL;
	private String linkedProgramID = RegularConstants.NULL;

	public ComponentMtmManager() {
		super();
		logger = ApplicationLogger.getInstance(this.getClass().getSimpleName());
		// logger.logDebug("#()");
	}

	public DTObject getData(DTObject input) {
		// logger.logDebug("getData()");
		DBUtil dbutil = getDbContext().createUtilInstance();
		try {
			// boolean isModifyReq = false;
			// isModifyReq = input.containsKey("M") ? true : false;
			result.set(RESULT, DATA_UNAVAILABLE);
			result.set(ERROR, EMPTY_STRING);
			if (!input.containsKey(MTM_REQUEST_TYPE)) {
				logger.logError("MTM_REQUEST_TYPE not specified");
				return result;
			} else {
				reqtype = input.get(MTM_REQUEST_TYPE).trim();
				logger.logDebug("setting MTM_REQUEST_TYPE : " + reqtype);
			}
			if (!input.containsKey(MTM_PARENT_PROGRAM_ID)) {
				logger.logError("MTM_PROGRAM_ID not specified");
				mainTable = "";
			} else {
				parentProgramID = input.get(MTM_PARENT_PROGRAM_ID).trim().toUpperCase(getContext().getLocale());
				linkedProgramID = input.get(MTM_LINKED_PROGRAM_ID).trim().toUpperCase(getContext().getLocale());
				if (input.containsKey(MTM_ALT_TABLE_NAME)) {
					mainTable = input.get(MTM_ALT_TABLE_NAME).trim();
				} else {
					dbutil.reset();
					dbutil.setSql("SELECT MPGM_TABLE_NAME FROM MPGM WHERE MPGM_ID = ?");
					dbutil.setString(1, linkedProgramID);
					ResultSet rset = dbutil.executeQuery();
					if (rset.next()) {
						mainTable = rset.getString(1);
					} else {
						mainTable = "";
					}
				}
				logger.logDebug("setting MTM_TABLE : " + mainTable);
				dbutil.reset();
				DBInfo info = new DBInfo();
				tableInfo = info.getTableInfo(mainTable);
				keyFields = tableInfo.getPrimaryKeyInfo().getColumnNames();
				keyFieldTypes = tableInfo.getPrimaryKeyInfo().getColumnTypes();
			}
			if (!input.containsKey(MTM_PARENT_PRIMARY_KEY)) {
				logger.logDebug("setting MTM_TABLE : default");
				return result;
			} else {
				parentPrimaryKeyValues = input.get(MTM_PARENT_PRIMARY_KEY).trim();
				linkedPrimaryKeyValues = input.get(MTM_LINKED_PRIMARY_KEY).trim();
				arguments = linkedPrimaryKeyValues.split("\\|");
				logger.logDebug("setting MTM_PRIMARY_KEY : " + parentPrimaryKeyValues);
			}
			if (reqtype.equalsIgnoreCase("M")) {
				generateSQLStatement("M");
				if (result.hasError()) {
					logger.logError("SQL Statement Generation has error");
					return result;
				}
				executeSQLStatement();
				if (result.hasError()) {
					logger.logError("SQL Statement Execution has error");
					return result;
				}
			} else if (reqtype.equalsIgnoreCase("T")) {
				generateSQLStatement("T");
				executeTBASQLStatement();
			} else {
				if (isAuthorizationRequired()) {
					generateSQLStatement("T");
					executeTBASQLStatement();
				}
				if (result.get(RESULT).equals(DATA_UNAVAILABLE)) {
					generateSQLStatement("M");
					if (result.hasError()) {
						logger.logError("SQL Statement Generation has error");
						return result;
					}
					executeSQLStatement();
					if (result.hasError()) {
						logger.logError("SQL Statement Execution has error");
						return result;
					}
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError(" getData(1) - " + e.getLocalizedMessage() + " : " + input);
			result.set(ERROR, e.getLocalizedMessage());
			result.set(RESULT, DATA_UNAVAILABLE);
		} finally {
			dbutil.reset();
		}
		return result;
	}

	private void executeSQLStatement() throws Exception {
		// logger.logDebug("executeSQLStatement()");
		DBUtil util = getDbContext().createUtilInstance();
		String tbaKeyValue = null;
		boolean stopUsage = false;
		boolean recordExists = false;
		try {
			result.set(RESULT, DATA_UNAVAILABLE);
			result.set(ERROR, EMPTY_STRING);
			util.reset();
			util.setSql(sqlQuery);
			int k = 1;
			Map<String, BindParameterType> columnType = tableInfo.getColumnInfo().getColumnMapping();
			for (int i = 0; i < keyFieldTypes.length; i++) {
				BindParameterType fieldType = columnType.get(keyFields[i]);
				switch (fieldType) {

				case LONG:
					util.setLong(k, Long.parseLong(arguments[i]));
					break;
				case DATE:
					String value = arguments[i];
					util.setDate(k,
							new java.sql.Date(BackOfficeFormatUtils.getDate(value, BackOfficeConstants.TBA_DATE_FORMAT)
									.getTime()));
					break;
				case INTEGER:
					util.setInt(k, Integer.parseInt(arguments[i]));
					break;
				case BIGDECIMAL:
					util.setBigDecimal(k, new BigDecimal(keyFields[i]));
					break;
				case VARCHAR:
				default:
					util.setString(k, arguments[i]);
					break;
				}
				k++;
			}
			ResultSet rs = util.executeQuery();
			// SimpleDateFormat sdf =
			// BackOfficeFormatUtils.getSimpleDateFormat(getContext().getDateFormat());
			SimpleDateFormat sdf = new SimpleDateFormat(BackOfficeFormatUtils.getSimpleDateFormat(getContext()
					.getDateFormat()));
			// SimpleDateFormat sdft = new
			// SimpleDateFormat(getContext().getDateFormat() + " HH:mm:ss");
			SimpleDateFormat sdft = new SimpleDateFormat(BackOfficeFormatUtils.getSimpleDateFormat(getContext()
					.getDateFormat()) + " HH:mm:ss");
			while (rs.next()) {
				recordExists = true;
				try {
					tbaKeyValue = null;
					tbaKeyValue = rs.getString("TBA_MAIN_KEY");
				} catch (Exception e) {
					tbaKeyValue = null;
				}
				stopUsage = false;
				if (!((tbaKeyValue == null || tbaKeyValue.trim().equalsIgnoreCase("")))) {
					if (isUsagePrevented() == false) {
						stopUsage = true;
					}
				}
				if (stopUsage == false) {
					result.set(RESULT, DATA_AVAILABLE);
					for (int i = 1; i < rs.getMetaData().getColumnCount() + 1; i++) {
						if (rs.getMetaData().getColumnTypeName(i).equals("DATE")) {
							if (rs.getDate(i) != null) {
								result.set(rs.getMetaData().getColumnLabel(i), sdf.format(rs.getDate(i)));
							} else {
								result.set(rs.getMetaData().getColumnLabel(i), null);
							}
						} else if (rs.getMetaData().getColumnTypeName(i).equals("DATETIME")
								|| rs.getMetaData().getColumnTypeName(i).equals("TIMESTAMP")) {
							if (rs.getDate(i) != null) {
								result.set(rs.getMetaData().getColumnLabel(i), sdft.format(rs.getTimestamp(i)));
							} else {
								result.set(rs.getMetaData().getColumnLabel(i), null);
							}
						} else {
							result.set(rs.getMetaData().getColumnLabel(i), rs.getString(i));
						}
					}
				} else {
					result.set(ERROR, "Record is not authorized, cannot proceed");
					result.set(RESULT, DATA_UNAVAILABLE);
				}

			}
			util.reset();
			if (recordExists) {
				// ARIBALA CHANGED ON 19/02/2014 BEG
				getAuditDetails();
				// ARIBALA CHANGED ON 19/02/2014 END
				result.set(RESULT, DATA_AVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError(" executeSQLStatement(1) - " + e.getLocalizedMessage());
			throw e;
		} finally {
			util.reset();
		}
	}

	private void getAuditDetails() {
		DBUtil util = getDbContext().createUtilInstance();
		try {
			util.reset();
			util.setMode(DBUtil.CALLABLE);
			util.setSql("{CALL SP_GET_AUDITDETAILS(?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
			util.setString(1, getContext().getPartitionNo());
			util.setString(2, getContext().getEntityCode());
			util.setString(3, linkedProgramID);
			util.setString(4, mainTable);
			util.setString(5, linkedPrimaryKeyValues);
			util.registerOutParameter(6, Types.DATE);
			util.registerOutParameter(7, Types.VARCHAR);
			util.registerOutParameter(8, Types.VARCHAR);
			util.registerOutParameter(9, Types.DATE);
			util.registerOutParameter(10, Types.VARCHAR);
			util.registerOutParameter(11, Types.VARCHAR);
			util.registerOutParameter(12, Types.DATE);
			util.registerOutParameter(13, Types.VARCHAR);
			util.registerOutParameter(14, Types.VARCHAR);

			util.execute();
			SimpleDateFormat sdft = new SimpleDateFormat(BackOfficeConstants.JAVA_DATE_FORMAT + " HH:mm:ss");

			if (util.getDate(6) != null)
				result.set("CR_ON", sdft.format(util.getTimestamp(6)));
			if (util.getDate(9) != null)
				result.set("MO_ON", sdft.format(util.getTimestamp(9)));
			if (util.getDate(12) != null)
				result.set("AU_ON", sdft.format(util.getTimestamp(12)));
			result.set("CR_BY", util.getString(7));
			result.set("CR_BY_NAME", util.getString(8));
			result.set("MO_BY", util.getString(10));
			result.set("MO_BY_NAME", util.getString(11));
			result.set("AU_BY", util.getString(13));
			result.set("AU_BY_NAME", util.getString(14));

			util.reset();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void executeTBASQLStatement() throws Exception {
		// logger.logDebug("executeTBASQLStatement()");
		DBUtil util = getDbContext().createUtilInstance();
		boolean stopUsage = false;
		boolean recordExists = false;
		try {
			result.set(RESULT, DATA_UNAVAILABLE);
			result.set(ERROR, EMPTY_STRING);
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, getContext().getEntityCode());
			util.setString(2, parentProgramID);
			util.setString(3, parentPrimaryKeyValues);
			util.setString(4, linkedProgramID);
			util.setString(5, linkedPrimaryKeyValues);
			util.setString(6, mainTable);

			ResultSet rs = util.executeQuery();
			// SimpleDateFormat sdf = new
			// SimpleDateFormat(getContext().getDateFormat());
			// SimpleDateFormat sdft = new
			// SimpleDateFormat(getContext().getDateFormat() + " HH:mm:ss");

			SimpleDateFormat sdf = new SimpleDateFormat(BackOfficeFormatUtils.getSimpleDateFormat(getContext()
					.getDateFormat()));
			SimpleDateFormat sdft = new SimpleDateFormat(BackOfficeFormatUtils.getSimpleDateFormat(getContext()
					.getDateFormat()) + " HH:mm:ss");

			while (rs.next()) {
				recordExists = true;
				stopUsage = false;
				if (stopUsage == false) {
					result.set(RESULT, DATA_AVAILABLE);
					for (int i = 1; i < rs.getMetaData().getColumnCount() + 1; i++) {
						if (rs.getMetaData().getColumnTypeName(i).equals("DATE")) {
							if (rs.getMetaData().getColumnName(i).equals("AU_ON")
									|| rs.getMetaData().getColumnName(i).equals("CR_ON")
									|| rs.getMetaData().getColumnName(i).equals("MO_ON")) {
								if (rs.getDate(i) != null) {
									result.set(rs.getMetaData().getColumnLabel(i), sdft.format(rs.getTimestamp(i)));
								} else {
									result.set(rs.getMetaData().getColumnLabel(i), null);
								}
							} else {
								if (rs.getDate(i) != null) {
									result.set(rs.getMetaData().getColumnLabel(i), sdf.format(rs.getDate(i)));
								} else {
									result.set(rs.getMetaData().getColumnLabel(i), null);
								}
							}
						} else {
							result.set(rs.getMetaData().getColumnLabel(i), rs.getString(i));
						}
					}
				} else {
					result.set(ERROR, "Record is not authorized, cannot proceed");
					result.set(RESULT, DATA_UNAVAILABLE);
				}
			}
			util.reset();
			if (recordExists)
				getAuditDetails();
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError(" executeTBASQLStatement(1) - " + e.getLocalizedMessage());
			throw e;
		} finally {
			util.reset();
		}
	}

	private void generateSQLStatement(String type) throws Exception {
		// logger.logDebug("generateSQLStatement()");
		DBUtil util = getDbContext().createUtilInstance();
		String sqlStatement = "";
		ResultSet rs = null;

		StringBuffer selectBuff = new StringBuffer(200);
		StringBuffer whereBuff = new StringBuffer(200);
		StringBuffer tableBuff = new StringBuffer(200);

		String mainTableAlias = "T1";

		List<FkTable> fkTableList = new LinkedList<FkTable>();
		try {
			selectBuff.append("SELECT T1.*");
			tableBuff.append(" FROM ").append(getTableName(mainTable, type)).append(" ").append(mainTableAlias)
					.append(" ");
			util.reset();
			sqlStatement = "SELECT FKMT_TABLE_NAME,FKMT_TABLE_ALIAS,FKMT_TABLE_TYPE FROM FKMAPTBL WHERE FKMT_PGM_ID=? ORDER BY FKMT_SL";
			util.setSql(sqlStatement);
			util.setString(1, linkedProgramID);

			rs = util.executeQuery();
			while (rs.next()) {
				FkTable fkTable = new FkTable();
				fkTable.setTableAlias(rs.getString("FKMT_TABLE_ALIAS"));
				fkTable.setTableName(rs.getString("FKMT_TABLE_NAME"));
				fkTable.setTableType(rs.getString("FKMT_TABLE_TYPE"));
				fkTableList.add(fkTable);
			}
			util.reset();

			for (FkTable fkTableInstance : fkTableList) {
				if (fkTableInstance.getTableType().equals("1")) {
					mainTableAlias = fkTableInstance.getTableAlias();
					selectBuff = new StringBuffer("SELECT ");
					selectBuff.append(fkTableInstance.getTableAlias()).append(".*");
					tableBuff = new StringBuffer("");

					tableBuff.append(" FROM ").append(getTableName(fkTableInstance.getTableName(), type)).append(" ")
							.append(fkTableInstance.getTableAlias()).append(" ");
				}

				if (fkTableInstance.getTableType().equals("2")) {
					// tableBuff.append(fkTableInstance.getTableName()).append(" ").append(fkTableInstance.getTableAlias()).append(" ");
					util.reset();
					sqlStatement = "SELECT FKMJ_SRC_TABLE_ALIAS,FKMJ_JOIN_TABLE_ALIAS,FKMJ_JOIN_TYPE FROM FKMAPJOIN WHERE FKMJ_PGM_ID=? AND FKMJ_JOIN_TABLE_ALIAS=? ORDER BY FKMJ_SL";
					util.setSql(sqlStatement);
					util.setString(1, linkedProgramID);
					util.setString(2, fkTableInstance.getTableAlias());
					rs = util.executeQuery();
					if (rs.next()) {
						tableBuff.append(getJoinType(rs.getInt("FKMJ_JOIN_TYPE")))
								.append(fkTableInstance.getTableName()).append(" ")
								.append(fkTableInstance.getTableAlias()).append(" ON ");
					}

					util.reset();

					sqlStatement = "SELECT FKMC_SRC_TABLE_ALIAS,FKMC_SRC_TABLE_COL,FKMC_OPR,FKMC_JOIN_TABLE_ALIAS,FKMC_JOIN_TABLE_COL FROM FKMAPCOND WHERE FKMC_PGM_ID=? AND FKMC_JOIN_TABLE_ALIAS=? ORDER BY FKMC_SL";
					util.setSql(sqlStatement);
					util.setString(1, linkedProgramID);
					util.setString(2, fkTableInstance.getTableAlias());

					rs = util.executeQuery();
					int counter = 1;

					while (rs.next()) {
						if (counter > 1) {

							tableBuff.append(" AND ");
						}
						tableBuff.append(rs.getString("FKMC_SRC_TABLE_ALIAS")).append(".")
								.append(rs.getString("FKMC_SRC_TABLE_COL"))
								.append(getOperationType(rs.getInt("FKMC_OPR")))
								.append(rs.getString("FKMC_JOIN_TABLE_ALIAS")).append(".")
								.append(rs.getString("FKMC_JOIN_TABLE_COL"));
						counter++;
					}
					util.reset();

				}

			}

			if (type.equalsIgnoreCase("M")) {
				whereBuff = whereBuff.append(" WHERE ");
				int i = 0;
				for (String fieldName : keyFields) {
					if (i < keyFields.length - 1) {
						whereBuff.append(mainTableAlias).append(".").append(fieldName).append(" = ?  AND  ");
					} else {
						whereBuff.append(mainTableAlias).append(".").append(fieldName).append(" = ? ");
					}
					++i;
				}
			}

			sqlStatement = "SELECT FKMCL_TABLE_ALIAS,FKMCL_TABLE_COL FROM FKMAPCOLS WHERE FKMCL_PGM_ID=? ORDER BY FKMCL_SL";
			util.setSql(sqlStatement);
			util.setString(1, linkedProgramID);

			rs = util.executeQuery();
			while (rs.next()) {
				selectBuff.append(",").append(rs.getString("FKMCL_TABLE_ALIAS")).append(".")
						.append(rs.getString("FKMCL_TABLE_COL")).append(" ").append(rs.getString("FKMCL_TABLE_ALIAS"))
						.append("_").append(rs.getString("FKMCL_TABLE_COL"));

			}
			util.reset();

			selectBuff.append(tableBuff).append(whereBuff);
			sqlQuery = selectBuff.toString();
			logger.logDebug("generateSQLStatement() : SQL Query : " + sqlQuery);
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError(" generateSQLStatement(1) - " + e.getLocalizedMessage());
			throw e;
		} finally {
			util.reset();
		}
	}

	private String getJoinType(int type) {
		String joinType = "";
		switch (type) {
		case 1:
			joinType = " INNER JOIN ";
			break;
		case 2:
			joinType = " LEFT OUTER JOIN ";
			break;
		case 3:
			joinType = " RIGHT OUTER JOIN ";
			break;
		case 4:
			joinType = " FULL OUTER JOIN ";
			break;
		}
		return joinType;
	}

	private String getOperationType(int type) {
		String operationType = "";
		switch (type) {
		case 1:
			operationType = " IS NULL ";
			break;
		case 2:
			operationType = " NOT NULL ";
			break;
		case 3:
			operationType = " = ";
			break;
		case 4:
			operationType = " <> ";
			break;
		case 5:
			operationType = " > ";
			break;
		case 6:
			operationType = " < ";
			break;
		case 7:
			operationType = " >= ";
			break;
		case 8:
			operationType = " <= ";
			break;
		case 9:
			operationType = " IN ";
			break;
		case 10:
			operationType = " NOT IN ";
			break;
		case 11:
			operationType = " BETWEEN ";
			break;
		}
		return operationType;
	}

	private String getTableName(String mtable, String reqtype) {
		StringBuffer buffer = new StringBuffer();
		DBUtil dbutil = getDbContext().createUtilInstance();
		StringBuffer query = new StringBuffer();
		try {
			if (reqtype.equalsIgnoreCase("M")) {
				buffer.append(mtable);
			} else {
				buffer.append("( SELECT");
				DBInfo info = null;
				String dataSource = DatasourceConfigurationManager.getDatabaseSource();
				if (dataSource.equals(RegularConstants.ORACLE_DATA_SOURCE)) {
					info = new OrclDBInfo();
				} else {
					info = new MySqlDBInfo();
				}
				tableInfo = info.getTableInfo(mtable);
				Set<String> noUpdateColumnInfo = tableInfo.getNoUpdationColumnInfo();
				String[] fieldNames = tableInfo.getColumnInfo().getColumnNames();
				Map<String, BindParameterType> columnType = tableInfo.getColumnInfo().getColumnMapping();
				for (String fieldName : fieldNames) {
					if (!noUpdateColumnInfo.contains(fieldName)) {
						if (columnType.get(fieldName).equals(BindParameterType.DATE)) {
							query.append(" TO_DATE(EXTRACTVALUE(T.DATA_BLOCK,'//" + fieldName + "'),'"
									+ BackOfficeConstants.JAVA_TBA_DB_DATE_FORMAT + "')" + fieldName + ",");
						} else {
							query.append(" EXTRACTVALUE(T.DATA_BLOCK,'//" + fieldName + "')" + fieldName + ",");
						}
					}
				}
				query.trimToSize();
				buffer.append(query.substring(0, query.length() - 1));
				buffer.append(" FROM PGMLINKEDTBA T WHERE T.ENTITY_CODE=? AND T.PARENT_PGM_ID = ? AND T.PARENT_PGM_PK=?");
				buffer.append(" AND T.LINKED_PGM_ID=? AND T.LINKED_PGM_PK= ? AND T.TABLE_NAME=? AND BLOCK_SL=1");
				buffer.append(" )");
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError(" getTableName(1) - " + e.getLocalizedMessage());
			buffer.append("");
		} finally {
			dbutil.reset();
		}
		return buffer.toString();
	}

	private boolean isUsagePrevented() {
		logger.logDebug("isUsagePrevented()");
		String sqlStatement;
		ResultSet rsTBAcheck = null;
		String w_auth_req = "0";
		boolean flag = false;
		DBUtil util = getDbContext().createUtilInstance();
		try {
			util.reset();
			sqlStatement = "SELECT C.MPGM_TRANSIT_CHOICE FROM MPGM M ,MPGMCONFIG C WHERE  M.MPGM_TABLE_NAME = ? AND M.MPGM_ID=C.MPGM_ID";
			util.setSql(sqlStatement);
			util.setString(1, mainTable);
			rsTBAcheck = util.executeQuery();
			while (rsTBAcheck.next()) {
				w_auth_req = rsTBAcheck.getString("MPGM_TRANSIT_CHOICE");
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError(" isUsagePrevented(1) - " + e.getLocalizedMessage());
			w_auth_req = "0";
		} finally {
			util.reset();
			if (w_auth_req.equalsIgnoreCase("O")) {
				flag = false;
			} else if (w_auth_req.equalsIgnoreCase("P")) {
				flag = true;
			}
		}
		return flag;
	}

	private boolean isAuthorizationRequired() {
		logger.logDebug("isAuthorizationRequired()");
		String sqlStatement;
		ResultSet rsTBAcheck = null;
		String tba_req = "0";
		boolean flag = false;
		DBUtil util = getDbContext().createUtilInstance();
		try {
			util.reset();
			sqlStatement = "SELECT C.MPGM_AUTH_REQD FROM MPGM M ,MPGMCONFIG C WHERE  M.MPGM_TABLE_NAME = ? AND M.MPGM_ID=C.MPGM_ID";
			util.setSql(sqlStatement);
			util.setString(1, mainTable);
			rsTBAcheck = util.executeQuery();
			while (rsTBAcheck.next()) {
				tba_req = rsTBAcheck.getString(1);
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError(" isUsagePrevented(1) - " + e.getLocalizedMessage());
			tba_req = "0";
		} finally {
			util.reset();
			if (tba_req.equalsIgnoreCase("0")) {
				flag = false;
			} else {
				flag = true;
			}
		}
		return flag;
	}
}