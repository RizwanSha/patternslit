
package patterns.config.framework.web.configuration.lookup;

import java.io.Serializable;

import patterns.config.framework.database.BindParameterType;

public class LookupConfiguration implements Serializable {

	private static final long serialVersionUID = 4869907043512263269L;
	private String[] columnHeadings_en_US;
	private String[] columnNames;
	private String[] aliasNames;
	private String[] predefinedColumns;
	private boolean[] filterAllowed;
	private String[] defaultSort;
	private BindParameterType[] columnTypes;
	private boolean[] defaultSearchColumns;
	private String programID;
	private String sqlQuery;
	private String tokenID;
	private boolean directAccess;
	private boolean intercepted;
	private String interceptionClass;
	private boolean defaultLoadingRequired;
	private boolean yearWiseFilterRequired;

	public String[] getColumnNames() {
		return columnNames;
	}

	public String[] getAliasNames() {
		return aliasNames;
	}

	public void setAliasNames(String[] aliasNames) {
		this.aliasNames = aliasNames;
	}

	public boolean[] getFilterAllowed() {
		return filterAllowed;
	}

	public void setFilterAllowed(boolean[] filterAllowed) {
		this.filterAllowed = filterAllowed;
	}

	public String[] getDefaultSort() {
		return defaultSort;
	}

	public void setDefaultSort(String[] defaultSort) {
		this.defaultSort = defaultSort;
	}

	public boolean isDefaultLoadingRequired() {
		return defaultLoadingRequired;
	}

	public void setDefaultLoadingRequired(boolean defaultLoadingRequired) {
		this.defaultLoadingRequired = defaultLoadingRequired;
	}

	public void setColumnNames(String[] columnNames) {
		this.columnNames = columnNames;
	}

	public BindParameterType[] getColumnTypes() {
		return columnTypes;
	}

	public void setColumnTypes(BindParameterType[] columnTypes) {
		this.columnTypes = columnTypes;
	}

	public String getProgramID() {
		return programID;
	}

	public void setProgramID(String programID) {
		this.programID = programID;
	}

	public String getSqlQuery() {
		return sqlQuery;
	}

	public void setSqlQuery(String sqlQuery) {
		this.sqlQuery = sqlQuery;
	}

	public String getTokenID() {
		return tokenID;
	}

	public void setTokenID(String tokenID) {
		this.tokenID = tokenID;
	}

	public void setDefaultSearchColumns(boolean[] defaultSearchColumns) {
		this.defaultSearchColumns = defaultSearchColumns;
	}

	public boolean[] getDefaultSearchColumns() {
		return defaultSearchColumns;
	}

	public void setColumnHeadings_en_US(String[] columnHeadings_en_US) {
		this.columnHeadings_en_US = columnHeadings_en_US;
	}

	public String[] getColumnHeadings_en_US() {
		return columnHeadings_en_US;
	}

	public void setIntercepted(boolean intercepted) {
		this.intercepted = intercepted;
	}

	public boolean isIntercepted() {
		return intercepted;
	}

	public void setDirectAccess(boolean directAccess) {
		this.directAccess = directAccess;
	}

	public boolean isDirectAccess() {
		return directAccess;
	}

	public void setInterceptionClass(String interceptionClass) {
		this.interceptionClass = interceptionClass;
	}

	public String getInterceptionClass() {
		return interceptionClass;
	}

	public String[] getPredefinedColumns() {
		return predefinedColumns;
	}

	public void setPredefinedColumns(String[] predefinedColumns) {
		this.predefinedColumns = predefinedColumns;
	}

	public boolean isYearWiseFilterRequired() {
		return yearWiseFilterRequired;
	}

	public void setYearWiseFilterRequired(boolean yearWiseFilterRequired) {
		this.yearWiseFilterRequired = yearWiseFilterRequired;
	}
}
