package patterns.config.framework.web.configuration.data;

import java.sql.ResultSet;

import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.loggers.ApplicationLogger;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.DHTMLXGridUtility;
import patterns.config.framework.web.ajax.AJAXContentManager;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.framework.web.configuration.program.ProgramConfiguration;
import patterns.config.framework.web.configuration.program.ProgramMap;
import patterns.config.validations.AccessValidator;

public class TBAManager extends AJAXContentManager {

	private static final String PROGRAM_ID = "_ProgramID";
	private ApplicationLogger logger = null;

	public TBAManager() {
		super();
		logger = ApplicationLogger.getInstance(this.getClass().getSimpleName());
		// logger.logDebug("#()");
	}

	public DTObject getData(DTObject input) {
		// logger.logDebug("getData()");
		DTObject result = new DTObject();
		AccessValidator validation = new AccessValidator();
		try {
			String programID = input.get(PROGRAM_ID).toUpperCase(getContext().getLocale());
			boolean available = false;
			DTObject operationResult = validation.isOptionAllocatedToRole(programID, getContext().getRoleCode());
			if (getContext().isAuthorizationRole()) {
				if (operationResult.get("VIEW_ALLOWED").equals(RegularConstants.COLUMN_ENABLE)) {
					available = true;
				}
			} else {
				if (operationResult.get("ADD_ALLOWED").equals(RegularConstants.COLUMN_ENABLE) || operationResult.get("MODIFY_ALLOWED").equals(RegularConstants.COLUMN_ENABLE)) {
					available = true;
				}
			}

			ResultSet resultSet = null;
			if (available) {
				DBUtil util = getDbContext().createUtilInstance();
				StringBuffer sqlQuery = new StringBuffer("SELECT TBAQ_MAIN_PK,DATE_FORMAT(TBAQ_ENTRY_DATE,'" + ContentManager.SESSION_DATE_FORMAT + "')|| '|' || TBAQ_ENTRY_SL ,TBAQ_PGM_ID, TBAQ_MAIN_PK, DATE_FORMAT(TBAQ_ENTRY_DATE,'" + ContentManager.SESSION_DATE_FORMAT + "'), IFNULL(TBAQ_DISPLAY_DTLS,'--'), DECODE_CMLOVREC('COMMON','USEROPERATION',TBAQ_OPERN_FLG) TBAQ_OPERN, TBAQ_OPERN_FLG,TBAQ_DONE_BY FROM TBAAUTHQ  WHERE ENTITY_CODE = '" + ContentManager.SESSION_ENTITY_CODE + "' AND TBAQ_PGM_ID = ? AND TBAQ_DONE_BY  IN ");
				if (validation.isSessionForCustomerAdministrator()) {
					sqlQuery.append("(SELECT USER_ID FROM USERS WHERE ENTITY_CODE = '" + ContentManager.SESSION_ENTITY_CODE + "' AND CUSTOMER_CODE='" + ContentManager.SESSION_CUSTOMER_CODE + "')");
				} else if (validation.isSessionForInternalAdministrator() || validation.isSessionForInternalOperations()) {
					sqlQuery.append("(SELECT BU.USER_ID FROM USERS BU,ROLES BR WHERE BU.ENTITY_CODE = '" + ContentManager.SESSION_ENTITY_CODE + "' AND BR.ENTITY_CODE = BU.ENTITY_CODE AND BR.CODE = FN_GETUSERSROLECD(BU.PARTITION_NO,BU.ENTITY_CODE,BU.USER_ID) AND ");
					if (getContext().isInternalAdministrator()) {
						sqlQuery.append(" BR.ROLE_TYPE IN ('1','2'))");
					} else if (getContext().isInternalOperations()) {
						sqlQuery.append(" BR.ROLE_TYPE IN ('3','4'))");
					}
				}
				String sqlQueryString = preProcessSQL(sqlQuery.toString());
				util.reset();
				util.setSql(sqlQueryString);
				util.setString(1, programID);
				resultSet = util.executeQuery();
				String xmlStr = getXML(resultSet);
				result.set(CONTENT, xmlStr);
				util.reset();
			} else {
				result.set(CONTENT, getXML(null));
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError("getData(1) : " + e.getLocalizedMessage() + " : " + input);
			result.set(CONTENT, getXML(null));
		} finally {
			validation.close();
		}
		return result;
	}

	public String getXML(ResultSet resultSet) {
		logger.logDebug("getXML()");
		DHTMLXGridUtility utility = new DHTMLXGridUtility();
		utility.init();
		try {
			int columnCount = resultSet.getMetaData().getColumnCount();
			if (resultSet != null) {
				while (resultSet.next()) {
					utility.startRow();
					// utility.setCell(RegularConstants.EMPTY_STRING);
					utility.setCell("Edit^javascript:doTBAGridEditRow(\"" + resultSet.getString("TBAQ_MAIN_PK") + "," + resultSet.getString("TBAQ_OPERN_FLG") + "\")");
					for (int j = 1; j <= columnCount; ++j) {
						utility.setCell(resultSet.getString(j));
					}
					utility.endRow();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError(" getXML() - " + e.getLocalizedMessage());
			return ContentManager.EMPTY_STRING;
		}
		utility.finish();
		return utility.getXML();
	}
}
