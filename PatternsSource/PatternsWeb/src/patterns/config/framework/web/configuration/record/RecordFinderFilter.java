package patterns.config.framework.web.configuration.record;

import java.io.Serializable;
import java.util.HashMap;

public class RecordFinderFilter implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String columnName;
	private String columnType;
	private String columnDescription;
	private boolean isMandatory;
	private String defaultValue;
	private String comboProgramId;
	private String comboLovId;
	private String aliasNames;
	private HashMap<String, String> comboList;

	public HashMap<String, String> getComboList() {
		return comboList;
	}

	public void setComboList(HashMap<String, String> comboList) {
		this.comboList = comboList;
	}

	public String getColumnName() {
		return columnName;
	}

	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	public String getAliasNames() {
		return aliasNames;
	}

	public void setAliasNames(String aliasNames) {
		this.aliasNames = aliasNames;
	}

	public String getColumnType() {
		return columnType;
	}

	public void setColumnType(String columnType) {
		this.columnType = columnType;
	}

	
	public String getColumnDescription() {
		return columnDescription;
	}

	public void setColumnDescription(String columnDescription) {
		this.columnDescription = columnDescription;
	}

	public boolean isMandatory() {
		return isMandatory;
	}

	public void setMandatory(boolean isMandatory) {
		this.isMandatory = isMandatory;
	}

	public String getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	public String getComboProgramId() {
		return comboProgramId;
	}

	public void setComboProgramId(String comboProgramId) {
		this.comboProgramId = comboProgramId;
	}

	public String getComboLovId() {
		return comboLovId;
	}

	public void setComboLovId(String comboLovId) {
		this.comboLovId = comboLovId;
	}

}
