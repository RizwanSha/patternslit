package patterns.config.framework.web.configuration.mtm;

import java.sql.Date;
import java.sql.ResultSet;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.BindParameterType;
import patterns.config.framework.database.objects.TableInfo;
import patterns.config.framework.database.utils.DBInfo;
import patterns.config.framework.database.utils.DBUtil;

import patterns.config.framework.loggers.ApplicationLogger;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.AJAXContentManager;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.framework.web.ajax.InterceptorPurpose;
import patterns.config.validations.CommonValidator;

public class GridQueryManager extends AJAXContentManager {

	private static final String PROGRAM_ID = "_ProgramID";
	private static final String TOKEN_ID = "_TokenID";
	private static final String GQM_PRIMARY_KEY = "_GQMPrimaryKey";
	private static final String GQM_REQUEST_TYPE = "_GQMReqType";

	private ApplicationLogger logger = null;
	private TableInfo tableInfo = null;
	private ResultSet rset = null;

	private String programID;
	private String primaryKeyValues = null;
	private String reqtype = "";
	private String type = "";
	// private StringBuffer sqlQueryBuffer = null;
	private Date entryDate = null;
	private String mTable;
	private ArrayList<String> mTableArrayList = new ArrayList<String>();
	private long entrySl;
	int counter = 0;
	private Object[] bucketParam=new Object[1];;
	boolean partitionEnabled =false;
	private String programType;
	
	public String getProgramType() {
		return programType;
	}
	public void setProgramType(String programType) {
		this.programType = programType;
	}
	
	public GridQueryManager() {
		super();
		logger = ApplicationLogger.getInstance(this.getClass().getSimpleName());
		// logger.logDebug("#()");
	}

	public DTObject getData(DTObject input) {
		DTObject result = new DTObject();
		StringBuffer buffer = new StringBuffer();
		String processedTableName = "";
		try {

			GridQueryMap dataMap = GridQueryMap.getInstance();
			programID = input.get(PROGRAM_ID).toUpperCase(getContext().getLocale());
			primaryKeyValues = input.get(GQM_PRIMARY_KEY).trim();
			reqtype = input.get(GQM_REQUEST_TYPE).trim();

			String tokenID = input.get(TOKEN_ID);

			CommonValidator validator = new CommonValidator();
			try {
				DTObject partitionInfo = validator.getPartitionInfo(programID);
				//ARIBALA ADDED ON 09102015 BEGIN
				setProgramType(validator.getProgramType(programID));
				//ARIBALA ADDED ON 09102015 END
				if (partitionInfo.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
					logger.logError("Invalid PROGRAM : " + programID);
					return result;
				}
				partitionEnabled = partitionInfo.get("PARTITION_YEAR_WISE").equals("1") ? true : false;
				if (partitionEnabled) {
					DTObject dtoObj = new DTObject();
					dtoObj.set(ContentManager.PROGRAM_ID, programID);
					dtoObj.set(ContentManager.TOKEN_KEY, tokenID);
					dtoObj.set(ContentManager.ARGUMENT_KEY, primaryKeyValues);
					dtoObj = processFilter(InterceptorPurpose.GRIDQUERY_ARGUMENTS_RESOLVE, partitionInfo.get("INTERCEPTOR_CLASS"), dtoObj);
					if (dtoObj == null) {
						logger.logError("Invalid INTERCEPTIOR_CLASS : " + programID);
						return result;
					}
					if (dtoObj.containsKey("BUCKET_VALUE")) {
						bucketParam[0] = dtoObj.get("BUCKET_VALUE");
					}
					// bucketParam = (Object[])
					// input.getObject(ContentManager.YEAR_PARAM);
				}
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				validator.close();
			}
			String sqlQuery = dataMap.getDescriptor(programID, tokenID);
			sqlQuery = preProcessSQL(sqlQuery);

			String pattern = GridQueryMap.NAME_SEPERATOR + "([A-Za-z0-9_])+" + GridQueryMap.NAME_SEPERATOR;
			Pattern p = Pattern.compile(pattern);
			int start = 0;
			int end = 0;
			{
				StringBuffer sqlQueryBuffer = new StringBuffer(sqlQuery);
				do {
					Matcher m = p.matcher(sqlQueryBuffer.toString());
					if (m.find()) {
						start = m.start();
						end = m.end();
						Pattern p1 = Pattern.compile("([A-Za-z0-9_])+");
						Matcher m1 = p1.matcher(m.group());
						if (m1.find()) {
							mTable = m1.group();
							mTableArrayList.add(mTable);
						}

						if (reqtype.equals("B")) {
							type = "T";
							processedTableName = getTableName(mTable);
						} else {
							type = reqtype;
							processedTableName = getTableName(mTable);
						}

						sqlQueryBuffer.replace(start, end, processedTableName);
					} else {
						break;
					}
				} while (true);
				System.out.println("Final Query:" + sqlQueryBuffer);
				buffer = executeSQLQuery(sqlQueryBuffer.toString());
			}
			if (reqtype.equals("B")) {
				if (counter == 0) {
					StringBuffer sqlQueryBuffer = new StringBuffer(sqlQuery);
					start = 0;
					end = 0;
					do {
						Matcher m = p.matcher(sqlQueryBuffer.toString());
						if (m.find()) {
							start = m.start();
							end = m.end();
							Pattern p1 = Pattern.compile("([A-Za-z0-9_])+");
							Matcher m1 = p1.matcher(m.group());
							if (m1.find()) {
								mTable = m1.group();
								mTableArrayList.add(mTable);
							}

							type = "M";
							processedTableName = getTableName(mTable);

							sqlQueryBuffer.replace(start, end, processedTableName);
						} else {
							break;
						}
					} while (true);
					buffer = executeSQLQuery(sqlQueryBuffer.toString());
				}
			}
			result.set(ContentManager.CONTENT, buffer.toString());
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError(" getData(1) : " + e.getLocalizedMessage() + " : " + input);
			result.set(ERROR, e.getLocalizedMessage());
			result.set(RESULT, DATA_UNAVAILABLE);
		} finally {
		}
		return result;
	}

	private String getTableName(String mtable) {
		StringBuffer buffer = new StringBuffer();
		DBUtil dbutil = getDbContext().createUtilInstance();
		StringBuffer query = new StringBuffer();
		try {
			mtable = MessageFormat.format(mtable, bucketParam);
			//ARIBALA CHANGED ON 09102015 BEGIN
			if (type.equalsIgnoreCase("M") || getProgramType().equals("M")) {
				//ARIBALA CHANGED ON 09102015 END
				buffer.append(mtable);
			} else {
				buffer.append("( SELECT");
				dbutil.reset();
				dbutil.setMode(DBUtil.PREPARED);
				dbutil.setSql("SELECT TBAQ_ENTRY_DATE,TBAQ_ENTRY_SL,TBAQ_OPERN_FLG FROM TBAAUTHQ WHERE  ENTITY_CODE = ?  AND  TBAQ_PGM_ID = ? AND TBAQ_MAIN_PK = ? ");
				dbutil.setString(1, getContext().getEntityCode());
				dbutil.setString(2, programID);
				dbutil.setString(3, primaryKeyValues);
				ResultSet rs = dbutil.executeQuery();
				if (rs.next()) {
					entryDate = rs.getDate("TBAQ_ENTRY_DATE");
					entrySl = rs.getLong("TBAQ_ENTRY_SL");
				}
				DBInfo info = new DBInfo();
				tableInfo = info.getTableInfo(mtable);
				Set<String> noUpdateColumnInfo = tableInfo.getNoUpdationColumnInfo();
				String[] fieldNames = tableInfo.getColumnInfo().getColumnNames();
				Map<String, BindParameterType> columnType = tableInfo.getColumnInfo().getColumnMapping();
				for (String fieldName : fieldNames) {
					if (!noUpdateColumnInfo.contains(fieldName)) {
						if (columnType.get(fieldName).equals(BindParameterType.DATE)) {
							query.append(" TO_DATE(EXTRACTVALUE(T.TBADTL_DATA_BLOCK,'//" + fieldName + "'),'" + BackOfficeConstants.JAVA_TBA_DB_DATE_FORMAT + "')" + fieldName + ",");
						} else {
							query.append(" EXTRACTVALUE(T.TBADTL_DATA_BLOCK,'//" + fieldName + "')" + fieldName + ",");
						}
					}
				}
				query.trimToSize();
				buffer.append(query.substring(0, query.length() - 1));
				buffer.append(" FROM TBAAUTHDTL T WHERE T.ENTITY_CODE=? AND T.TBADTL_PGM_ID = ? AND T.TBADTL_MAIN_PK=?");
				buffer.append(" AND T.TBADTL_ENTRY_DATE=? AND T.TBADTL_ENTRY_SL= ? AND T.TBADTL_TABLE_NAME=?");
				buffer.append(" )");
			}

		} catch (Exception e) {
			e.printStackTrace();
			logger.logError(" getTableName(1) : " + e.getLocalizedMessage());
			buffer.append("");
		} finally {
			dbutil.reset();
		}
		return buffer.toString();
	}

	private StringBuffer executeSQLQuery(String sqlQuery) throws Exception {
		StringBuffer buffer = new StringBuffer(1024);
		DBUtil util = getDbContext().createUtilInstance();
		int index = 1;
		try {
			buffer.append("<rows pos='0'>");
			// PRAS
			String formattedQuery ="";
			if(partitionEnabled){
				formattedQuery = sqlQuery.replaceAll("(?<!')'(?!')", "''");
				formattedQuery = MessageFormat.format(formattedQuery, bucketParam);
			}else
				formattedQuery = sqlQuery;
			
			util.reset();
			util.setSql(formattedQuery);
			if (type.equalsIgnoreCase("T") && getProgramType().equals("A")) {
				for (int i = 0; i < mTableArrayList.size(); ++i) {
					util.setString(index++, getContext().getEntityCode());
					util.setString(index++, programID);
					util.setString(index++, primaryKeyValues);
					util.setDate(index++, entryDate);
					util.setLong(index++, entrySl);
					util.setString(index++, mTableArrayList.get(i));
				}
			}
			String[] arguments = primaryKeyValues.split("\\|");
			for (int i = 0; i < arguments.length && primaryKeyValues.length() != 0; ++i) {
				util.setString(i + index, arguments[i]);
			}
			rset = util.executeQuery();
			int i = 1;

			int columnCount = rset.getMetaData().getColumnCount();
			while (rset.next()) {
				buffer.append("<row id='" + i + "'>");
				buffer.append("<cell>0</cell>");
				for (int j = 1; j <= columnCount; ++j) {
					buffer.append("<cell><![CDATA[");
					buffer.append(rset.getString(j));
					buffer.append("]]></cell>");
				}
				buffer.append("</row>");
				++i;
				counter++;
			}
			buffer.append("</rows>");
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError("executeSQLQuery(1) : " + e.getLocalizedMessage());
		} finally {
			util.reset();

		}
		return buffer;
	}
}
