package patterns.config.framework.web.configuration.mtm;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.Types;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.BindParameterType;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.objects.TableInfo;
import patterns.config.framework.database.utils.DBInfo;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.loggers.ApplicationLogger;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.framework.web.ajax.AJAXContentManager;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.framework.web.ajax.InterceptorPurpose;

public class MtmManager extends AJAXContentManager {

	private ApplicationLogger logger = null;

	private static final String MTM_REQUEST_TYPE = "_MtmReqType";
	private static final String MTM_PRIMARY_KEY = "_MtmPrimaryKey";
	private static final String MTM_PROGRAM_ID = "_MtmProgramID";
	private static final String MTM_ALT_TABLE_NAME = "_MtmAltTable";

	private String reqtype = RegularConstants.EMPTY_STRING;
	private String[] arguments;
	private String primaryKeyValues;
	private StringBuilder mainTable = new StringBuilder(RegularConstants.EMPTY_STRING);
	private String sqlQuery = RegularConstants.NULL;
	private Date entryDate = null;
	private long entrySl = 0L;
	private String[] keyFields;
	private BindParameterType[] keyFieldTypes;
	private DTObject result = new DTObject();
	private TableInfo tableInfo = null;
	private String programID = RegularConstants.NULL;
	private boolean isModifyReq = false;
	private String interceptorClass = RegularConstants.EMPTY_STRING;
	private String bucketValue = RegularConstants.EMPTY_STRING;
	private Object[] bucketParam = new Object[1];
	private String mpgmTable = RegularConstants.EMPTY_STRING;
	private String authorizedRequired;
	private String transitChoice;
	private String paritionYearWise;
	private String partitionYear = "";
	private String programType;

	public String getAuthorizedRequired() {
		return authorizedRequired;
	}

	public void setAuthorizedRequired(String authorizedRequired) {
		this.authorizedRequired = authorizedRequired;
	}

	public String getTransitChoice() {
		return transitChoice;
	}

	public void setTransitChoice(String transitChoice) {
		this.transitChoice = transitChoice;
	}

	public String getParitionYearWise() {
		return paritionYearWise;
	}

	public void setParitionYearWise(String paritionYearWise) {
		this.paritionYearWise = paritionYearWise;
	}

	public String getProgramType() {
		return programType;
	}

	public void setProgramType(String programType) {
		this.programType = programType;
	}
	public MtmManager() {
		super();
		logger = ApplicationLogger.getInstance(this.getClass().getSimpleName());
		// logger.logDebug("#()");
	}

	public DTObject getData(DTObject input) {
		// logger.logDebug("getData()");
		DBUtil dbutil = getDbContext().createUtilInstance();
		try {
			isModifyReq = input.containsKey("M") ? true : false;
			result.set(RESULT, DATA_UNAVAILABLE);
			result.set(ERROR, EMPTY_STRING);
			if (!input.containsKey(MTM_REQUEST_TYPE)) {
				logger.logError("MTM_REQUEST_TYPE not specified");
				return result;
			} else {
				reqtype = input.get(MTM_REQUEST_TYPE).trim();
				logger.logDebug("setting MTM_REQUEST_TYPE : " + reqtype);
			}
			if (!input.containsKey(MTM_PRIMARY_KEY)) {
				logger.logDebug("setting MTM_TABLE : default");
				return result;
			} else {
				primaryKeyValues = input.get(MTM_PRIMARY_KEY).trim();
				logger.logDebug("setting MTM_PRIMARY_KEY : " + primaryKeyValues);
			}
			if (!input.containsKey(MTM_PROGRAM_ID)) {
				logger.logError("MTM_PROGRAM_ID not specified");

			} else {
				programID = input.get(MTM_PROGRAM_ID).trim().toUpperCase(getContext().getLocale());
				//ARIBALA ADDED ON 09102015 BEGIN
				getProgramTypeDetails();
				//ARIBALA ADDED ON 09102015 END
				if (input.containsKey(MTM_ALT_TABLE_NAME)) {
					mainTable = new StringBuilder(input.get(MTM_ALT_TABLE_NAME).trim());
				} else {
					dbutil.reset();
					dbutil.setSql("SELECT MPGM_TABLE_NAME,INTERCEPTOR_CLASS FROM MPGM WHERE MPGM_ID = ?");
					dbutil.setString(1, programID);
					ResultSet rset = dbutil.executeQuery();
					if (rset.next()) {
						mpgmTable = rset.getString(1);
						mainTable.append(mpgmTable);
						interceptorClass = rset.getString("INTERCEPTOR_CLASS");
						if (interceptorClass != null && !interceptorClass.equals("")) {
							DTObject dtoObj=new DTObject();
							dtoObj.set(ContentManager.PROGRAM_ID, programID);
							dtoObj.set(ContentManager.ARGUMENT_KEY, primaryKeyValues);
							dtoObj.set(ContentManager.MAIN_TABLE, mpgmTable);
							dtoObj = processFilter(InterceptorPurpose.MTM_ARGUMENTS_RESOLVE, interceptorClass, dtoObj);
							if (dtoObj == null) {
								logger.logError("Invalid MTM_INTERCEPTION ");
								return result;
							}
							
							if (getParitionYearWise().equals(RegularConstants.COLUMN_ENABLE)) {
								if (dtoObj.containsKey("BUCKET_VALUE")) {
									bucketParam[0] = dtoObj.get("BUCKET_VALUE");
									partitionYear=bucketParam[0].toString();
									mainTable.append("{0}");
								}
							}
						}

					}
				}
				arguments = primaryKeyValues.split("\\|");
				mainTable = new StringBuilder(MessageFormat.format(mainTable.toString(), bucketParam));
				logger.logDebug("setting MTM_TABLE : " + mainTable);
				dbutil.reset();
				DBInfo info = new DBInfo();

				tableInfo = info.getTableInfo(mainTable.toString());
				keyFields = tableInfo.getPrimaryKeyInfo().getColumnNames();
				keyFieldTypes = tableInfo.getPrimaryKeyInfo().getColumnTypes();
			}
			//ARIBALA CHANGED ON 09102015 BEGIN
			if (reqtype.equalsIgnoreCase("M") || getProgramType().equals("M")) {
			//ARIBALA CHANGED ON 09102015 END	
				generateSQLStatement("M");
				if (result.hasError()) {
					logger.logError("SQL Statement Generation has error");
					return result;
				}
				executeSQLStatement();
				if (result.hasError()) {
					logger.logError("SQL Statement Execution has error");
					return result;
				}
			} else if (reqtype.equalsIgnoreCase("T")) {
				dbutil.reset();
				dbutil.setMode(DBUtil.PREPARED);
				dbutil.setSql("SELECT TBAQ_ENTRY_DATE,TBAQ_ENTRY_SL,TBAQ_OPERN_FLG FROM TBAAUTHQ WHERE  ENTITY_CODE = ?  AND  TBAAUTHQ.TBAQ_PGM_ID = ? AND TBAAUTHQ.TBAQ_MAIN_PK = ? ");
				dbutil.setString(1, getContext().getEntityCode());
				dbutil.setString(2, programID);
				dbutil.setString(3, primaryKeyValues);
				ResultSet rs = dbutil.executeQuery();
				if (rs.next()) {
					entryDate = rs.getDate("TBAQ_ENTRY_DATE");
					entrySl = rs.getLong("TBAQ_ENTRY_SL");
					generateSQLStatement("T");
					executeTBASQLStatement();
					// checkRecordInTBA();
				}
				dbutil.reset();
			} else {
				if (getAuthorizedRequired().equals(RegularConstants.COLUMN_ENABLE)) {
					dbutil.reset();
					dbutil.setMode(DBUtil.PREPARED);
					dbutil.setSql("SELECT TBAQ_ENTRY_DATE,TBAQ_ENTRY_SL,TBAQ_OPERN_FLG FROM TBAAUTHQ WHERE ENTITY_CODE = ? AND TBAAUTHQ.TBAQ_PGM_ID = ? AND TBAAUTHQ.TBAQ_MAIN_PK = ? ");
					dbutil.setString(1, getContext().getEntityCode());
					dbutil.setString(2, programID);
					dbutil.setString(3, primaryKeyValues);
					ResultSet rs = dbutil.executeQuery();
					if (rs.next()) {
						entryDate = rs.getDate("TBAQ_ENTRY_DATE");
						entrySl = rs.getLong("TBAQ_ENTRY_SL");
						generateSQLStatement("T");
						executeTBASQLStatement();
						// checkRecordInTBA();
					}
					dbutil.reset();
				}
				if (result.get(RESULT).equals(DATA_UNAVAILABLE)) {
					generateSQLStatement("M");
					if (result.hasError()) {
						logger.logError("SQL Statement Generation has error");
						return result;
					}
					executeSQLStatement();
					if (result.hasError()) {
						logger.logError("SQL Statement Execution has error");
						return result;
					}
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError(" getData(1) - " + e.getLocalizedMessage() + " : " + input);
			result.set(ERROR, e.getLocalizedMessage());
			result.set(RESULT, DATA_UNAVAILABLE);
		} finally {
			dbutil.reset();
		}
		return result;
	}

	private void executeSQLStatement() throws Exception {
		// logger.logDebug("executeSQLStatement()");
		DBUtil util = getDbContext().createUtilInstance();
		String tbaKeyValue = null;
		boolean stopUsage = false;
		boolean recordExists = false;
		try {
			result.set(RESULT, DATA_UNAVAILABLE);
			result.set(ERROR, EMPTY_STRING);
			System.out.println("SQL QUERY : " + sqlQuery);
			util.reset();
			util.setSql(sqlQuery);
			int k = 1;
			Map<String, BindParameterType> columnType = tableInfo.getColumnInfo().getColumnMapping();
			for (int i = 0; i < keyFieldTypes.length; i++) {
				BindParameterType fieldType = columnType.get(keyFields[i]);
				switch (fieldType) {

				case LONG:
					util.setLong(k, Long.parseLong(arguments[i]));
					break;
				case DATE:
					String value = arguments[i];
					util.setDate(k, new java.sql.Date(BackOfficeFormatUtils.getDate(value, BackOfficeConstants.TBA_DATE_FORMAT).getTime()));
					break;
				case INTEGER:
					util.setInt(k, Integer.parseInt(arguments[i]));
					break;
				case BIGDECIMAL:
					util.setBigDecimal(k, new BigDecimal(keyFields[i]));
					break;
				case VARCHAR:
				default:
					util.setString(k, arguments[i]);
					break;
				}
				k++;
			}
			ResultSet rs = util.executeQuery();

			SimpleDateFormat sdf = new SimpleDateFormat(BackOfficeFormatUtils.getSimpleDateFormat(getContext().getDateFormat()));

			SimpleDateFormat sdft = new SimpleDateFormat(BackOfficeFormatUtils.getSimpleDateFormat(getContext().getDateFormat()) + " HH:mm:ss");
			while (rs.next()) {
				recordExists = true;
				try {
					tbaKeyValue = null;
					tbaKeyValue = rs.getString("TBA_MAIN_KEY");
				} catch (Exception e) {
					tbaKeyValue = null;
				}
				stopUsage = false;
				if (!((tbaKeyValue == null || tbaKeyValue.trim().equalsIgnoreCase("")))) {
					if (getTransitChoice().equals("1")) {
						stopUsage = true;
					}
				}
				if (stopUsage == false) {
					result.set(RESULT, DATA_AVAILABLE);
					for (int i = 1; i < rs.getMetaData().getColumnCount() + 1; i++) {
						if (rs.getMetaData().getColumnTypeName(i).equals("DATE")) {
							if (rs.getDate(i) != null) {
								result.set(rs.getMetaData().getColumnLabel(i), sdf.format(rs.getDate(i)));
							} else {
								result.set(rs.getMetaData().getColumnLabel(i), null);
							}
						} else if (rs.getMetaData().getColumnTypeName(i).equals("DATETIME") || rs.getMetaData().getColumnTypeName(i).equals("TIMESTAMP")) {
							if (rs.getDate(i) != null) {
								result.set(rs.getMetaData().getColumnLabel(i), sdft.format(rs.getTimestamp(i)));
							} else {
								result.set(rs.getMetaData().getColumnLabel(i), null);
							}
						} else {
							result.set(rs.getMetaData().getColumnLabel(i), rs.getString(i));
						}
					}
				} else {
					result.set(ERROR, "Record is not authorized, cannot proceed");
					result.set(RESULT, DATA_UNAVAILABLE);
				}

			}
			util.reset();
			if (recordExists) {
				// ARIBALA CHANGED ON 19/02/2014 BEG
				if (!isModifyReq) {
					getAuditDetails();
				}
				// ARIBALA CHANGED ON 19/02/2014 END
				result.set(RESULT, DATA_AVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError(" executeSQLStatement(1) - " + e.getLocalizedMessage());
			throw e;
		} finally {
			util.reset();
		}
	}

	private void getAuditDetails() {
		DBUtil util = getDbContext().createUtilInstance();
		try {
			util.reset();
			util.setMode(DBUtil.CALLABLE);
			util.setSql("{CALL SP_GET_AUDITDETAILS(?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
			util.setString(1, getContext().getPartitionNo());
			util.setString(2, getContext().getEntityCode());
			util.setString(3, programID);
			util.setString(4, primaryKeyValues);
			util.setString(5, partitionYear);
			util.registerOutParameter(6, Types.DATE);
			util.registerOutParameter(7, Types.VARCHAR);
			util.registerOutParameter(8, Types.VARCHAR);
			util.registerOutParameter(9, Types.DATE);
			util.registerOutParameter(10, Types.VARCHAR);
			util.registerOutParameter(11, Types.VARCHAR);
			util.registerOutParameter(12, Types.DATE);
			util.registerOutParameter(13, Types.VARCHAR);
			util.registerOutParameter(14, Types.VARCHAR);

			util.execute();
			SimpleDateFormat sdft = new SimpleDateFormat(BackOfficeConstants.JAVA_DATE_FORMAT + " HH:mm:ss");

			if (util.getDate(6) != null)
				result.set("CR_ON", sdft.format(util.getTimestamp(6)));
			if (util.getDate(9) != null)
				result.set("MO_ON", sdft.format(util.getTimestamp(9)));
			if (util.getDate(12) != null)
				result.set("AU_ON", sdft.format(util.getTimestamp(12)));
			result.set("CR_BY", util.getString(7));
			result.set("CR_BY_NAME", util.getString(8));
			result.set("MO_BY", util.getString(10));
			result.set("MO_BY_NAME", util.getString(11));
			result.set("AU_BY", util.getString(13));
			result.set("AU_BY_NAME", util.getString(14));

			util.reset();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void executeTBASQLStatement() throws Exception {
		// logger.logDebug("executeTBASQLStatement()");
		DBUtil util = getDbContext().createUtilInstance();
		boolean stopUsage = false;
		boolean recordExists = false;
		try {
			result.set(RESULT, DATA_UNAVAILABLE);
			result.set(ERROR, EMPTY_STRING);
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, getContext().getEntityCode());
			util.setString(2, programID);
			util.setString(3, primaryKeyValues);
			util.setDate(4, entryDate);
			util.setLong(5, entrySl);
			util.setString(6, mainTable.toString());

			ResultSet rs = util.executeQuery();

			SimpleDateFormat sdf = new SimpleDateFormat(BackOfficeFormatUtils.getSimpleDateFormat(getContext().getDateFormat()));
			SimpleDateFormat sdft = new SimpleDateFormat(BackOfficeFormatUtils.getSimpleDateFormat(getContext().getDateFormat()) + " HH:mm:ss");

			while (rs.next()) {
				recordExists = true;
				stopUsage = false;
				if (stopUsage == false) {
					result.set(RESULT, DATA_AVAILABLE);
					for (int i = 1; i < rs.getMetaData().getColumnCount() + 1; i++) {
						if (rs.getMetaData().getColumnTypeName(i).equals("DATE")) {
							if (rs.getMetaData().getColumnName(i).equals("AU_ON") || rs.getMetaData().getColumnName(i).equals("CR_ON") || rs.getMetaData().getColumnName(i).equals("MO_ON")) {
								if (rs.getDate(i) != null) {
									result.set(rs.getMetaData().getColumnLabel(i), sdft.format(rs.getTimestamp(i)));
								} else {
									result.set(rs.getMetaData().getColumnLabel(i), null);
								}
							} else {
								if (rs.getDate(i) != null) {
									result.set(rs.getMetaData().getColumnLabel(i), sdf.format(rs.getDate(i)));
								} else {
									result.set(rs.getMetaData().getColumnLabel(i), null);
								}
							}
						} else {
							result.set(rs.getMetaData().getColumnLabel(i), rs.getString(i));
						}
					}
				} else {
					result.set(ERROR, "Record is not authorized, cannot proceed");
					result.set(RESULT, DATA_UNAVAILABLE);
				}
			}
			util.reset();
			if (recordExists) {
				if (!isModifyReq) {
					getAuditDetails();
				}
			}
			result.set(RESULT, DATA_AVAILABLE);
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError(" executeTBASQLStatement(1) - " + e.getLocalizedMessage());
			throw e;
		} finally {
			util.reset();
		}
	}

	private void generateSQLStatement(String type) throws Exception {
		// logger.logDebug("generateSQLStatement()");
		DBUtil util = getDbContext().createUtilInstance();
		String sqlStatement = "";
		ResultSet rs = null;

		StringBuffer selectBuff = new StringBuffer(200);
		StringBuffer whereBuff = new StringBuffer(200);
		StringBuffer tableBuff = new StringBuffer(200);

		String mainTableAlias = "T1";

		List<FkTable> fkTableList = new LinkedList<FkTable>();
		try {
			selectBuff.append("SELECT T1.*");
			tableBuff.append(" FROM ").append(getTableName(mainTable.toString(), type)).append(" ").append(mainTableAlias).append(" ");
			util.reset();
			sqlStatement = "SELECT FKMT_TABLE_NAME,FKMT_TABLE_ALIAS,FKMT_TABLE_TYPE FROM FKMAPTBL WHERE FKMT_PGM_ID=? ORDER BY FKMT_SL";
			util.setSql(sqlStatement);
			util.setString(1, programID);

			rs = util.executeQuery();
			while (rs.next()) {
				FkTable fkTable = new FkTable();
				fkTable.setTableAlias(rs.getString("FKMT_TABLE_ALIAS"));
				fkTable.setTableName(rs.getString("FKMT_TABLE_NAME"));
				fkTable.setTableType(rs.getString("FKMT_TABLE_TYPE"));
				fkTableList.add(fkTable);
			}
			util.reset();

			for (FkTable fkTableInstance : fkTableList) {
				String refernceTableName = MessageFormat.format(fkTableInstance.getTableName(), bucketParam);
				if (fkTableInstance.getTableType().equals("1")) {
					mainTableAlias = fkTableInstance.getTableAlias();
					selectBuff = new StringBuffer("SELECT ");
					selectBuff.append(fkTableInstance.getTableAlias()).append(".*");
					tableBuff = new StringBuffer("");

					tableBuff.append(" FROM ").append(getTableName(refernceTableName, type)).append(" ").append(fkTableInstance.getTableAlias()).append(" ");
				}

				if (fkTableInstance.getTableType().equals("2")) {

					util.reset();
					sqlStatement = "SELECT FKMJ_SRC_TABLE_ALIAS,FKMJ_JOIN_TABLE_ALIAS,FKMJ_JOIN_TYPE FROM FKMAPJOIN WHERE FKMJ_PGM_ID=? AND FKMJ_JOIN_TABLE_ALIAS=? ORDER BY FKMJ_SL";
					util.setSql(sqlStatement);
					util.setString(1, programID);
					util.setString(2, fkTableInstance.getTableAlias());
					rs = util.executeQuery();
					if (rs.next()) {
						tableBuff.append(getJoinType(rs.getInt("FKMJ_JOIN_TYPE"))).append(refernceTableName).append(" ").append(fkTableInstance.getTableAlias()).append(" ON ");
					}

					util.reset();

					sqlStatement = "SELECT FKMC_SRC_TABLE_ALIAS,FKMC_SRC_TABLE_COL,FKMC_OPR,FKMC_JOIN_TABLE_ALIAS,FKMC_JOIN_TABLE_COL FROM FKMAPCOND WHERE FKMC_PGM_ID=? AND FKMC_JOIN_TABLE_ALIAS=? ORDER BY FKMC_SL";
					util.setSql(sqlStatement);
					util.setString(1, programID);
					util.setString(2, fkTableInstance.getTableAlias());

					rs = util.executeQuery();
					int counter = 1;

					while (rs.next()) {
						if (counter > 1) {

							tableBuff.append(" AND ");
						}
						tableBuff.append(rs.getString("FKMC_SRC_TABLE_ALIAS")).append(".").append(rs.getString("FKMC_SRC_TABLE_COL")).append(getOperationType(rs.getInt("FKMC_OPR"))).append(rs.getString("FKMC_JOIN_TABLE_ALIAS")).append(".").append(rs.getString("FKMC_JOIN_TABLE_COL"));
						counter++;
					}
					util.reset();

				}

			}

			if (type.equalsIgnoreCase("M")) {
				whereBuff = whereBuff.append(" WHERE ");
				int i = 0;
				for (String fieldName : keyFields) {
					if (i < keyFields.length - 1) {
						whereBuff.append(mainTableAlias).append(".").append(fieldName).append(" = ?  AND  ");
					} else {
						whereBuff.append(mainTableAlias).append(".").append(fieldName).append(" = ? ");
					}
					++i;
				}
			}

			sqlStatement = "SELECT FKMCL_TABLE_ALIAS,FKMCL_TABLE_COL FROM FKMAPCOLS WHERE FKMCL_PGM_ID=? ORDER BY FKMCL_SL";
			util.setSql(sqlStatement);
			util.setString(1, programID);

			rs = util.executeQuery();
			while (rs.next()) {
				selectBuff.append(",").append(rs.getString("FKMCL_TABLE_ALIAS")).append(".").append(rs.getString("FKMCL_TABLE_COL")).append(" ").append(rs.getString("FKMCL_TABLE_ALIAS")).append("_").append(rs.getString("FKMCL_TABLE_COL"));

			}
			util.reset();

			selectBuff.append(tableBuff).append(whereBuff);
			sqlQuery = selectBuff.toString();
			logger.logDebug("generateSQLStatement() : SQL Query : " + sqlQuery);
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError(" generateSQLStatement(1) - " + e.getLocalizedMessage());
			throw e;
		} finally {
			util.reset();
		}
	}

	private String getJoinType(int type) {
		String joinType = "";
		switch (type) {
		case 1:
			joinType = " INNER JOIN ";
			break;
		case 2:
			joinType = " LEFT OUTER JOIN ";
			break;
		case 3:
			joinType = " RIGHT OUTER JOIN ";
			break;
		case 4:
			joinType = " FULL OUTER JOIN ";
			break;
		}
		return joinType;
	}

	private String getOperationType(int type) {
		String operationType = "";
		switch (type) {
		case 1:
			operationType = " IS NULL ";
			break;
		case 2:
			operationType = " NOT NULL ";
			break;
		case 3:
			operationType = " = ";
			break;
		case 4:
			operationType = " <> ";
			break;
		case 5:
			operationType = " > ";
			break;
		case 6:
			operationType = " < ";
			break;
		case 7:
			operationType = " >= ";
			break;
		case 8:
			operationType = " <= ";
			break;
		case 9:
			operationType = " IN ";
			break;
		case 10:
			operationType = " NOT IN ";
			break;
		case 11:
			operationType = " BETWEEN ";
			break;
		}
		return operationType;
	}

	private String getTableName(String mtable, String reqtype) {
		StringBuffer buffer = new StringBuffer();
		DBUtil dbutil = getDbContext().createUtilInstance();
		StringBuffer query = new StringBuffer();
		try {
			mtable = MessageFormat.format(mtable, bucketParam);
			if (reqtype.equalsIgnoreCase("M")) {
				buffer.append(mtable);
			} else {
				buffer.append("( SELECT");
				dbutil.reset();
				dbutil.setMode(DBUtil.PREPARED);
				dbutil.setSql("SELECT TBAQ_ENTRY_DATE,TBAQ_ENTRY_SL,TBAQ_OPERN_FLG FROM TBAAUTHQ WHERE  ENTITY_CODE = ?  AND  TBAQ_PGM_ID = ? AND TBAQ_MAIN_PK = ? ");
				dbutil.setString(1, getContext().getEntityCode());
				dbutil.setString(2, programID);
				dbutil.setString(3, primaryKeyValues);
				ResultSet rs = dbutil.executeQuery();
				if (rs.next()) {
					entryDate = rs.getDate("TBAQ_ENTRY_DATE");
					entrySl = rs.getLong("TBAQ_ENTRY_SL");
				}
				DBInfo info = new DBInfo();

				tableInfo = info.getTableInfo(mtable);
				Set<String> noUpdateColumnInfo = tableInfo.getNoUpdationColumnInfo();
				String[] fieldNames = tableInfo.getColumnInfo().getColumnNames();
				Map<String, BindParameterType> columnType = tableInfo.getColumnInfo().getColumnMapping();
				for (String fieldName : fieldNames) {
					if (!noUpdateColumnInfo.contains(fieldName)) {
						if (columnType.get(fieldName).equals(BindParameterType.DATE)) {
							query.append(" TO_DATE(EXTRACTVALUE(T.TBADTL_DATA_BLOCK,'//" + fieldName + "'),'" + BackOfficeConstants.TBA_XML_DATE_FORMAT + "')" + fieldName + ",");
						} else {
							query.append(" EXTRACTVALUE(T.TBADTL_DATA_BLOCK,'//" + fieldName + "')" + fieldName + ",");
						}
					}
				}
				query.trimToSize();
				buffer.append(query.substring(0, query.length() - 1));
				buffer.append(" FROM TBAAUTHDTL T WHERE T.ENTITY_CODE=? AND T.TBADTL_PGM_ID = ? AND T.TBADTL_MAIN_PK=?");
				buffer.append(" AND T.TBADTL_ENTRY_DATE=? AND T.TBADTL_ENTRY_SL= ? AND T.TBADTL_TABLE_NAME=?");
				buffer.append(" )");
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError(" getTableName(1) - " + e.getLocalizedMessage());
			buffer.append("");
		} finally {
			dbutil.reset();
		}
		return buffer.toString();
	}

	private void getProgramTypeDetails() {
		logger.logDebug("isUsagePrevented()");
		String sqlStatement;
		ResultSet rs = null;
		DBUtil util = getDbContext().createUtilInstance();
		try {
			util.reset();
			sqlStatement = "SELECT C.MPGM_TRANSIT_CHOICE,C.MPGM_AUTH_REQD,M.PARTITION_YEAR_WISE,C.MPGM_TYPE FROM MPGM M ,MPGMCONFIG C WHERE  M.MPGM_ID = ? AND M.MPGM_ID=C.MPGM_ID";
			util.setSql(sqlStatement);
			util.setString(1, programID);
			rs = util.executeQuery();
			while (rs.next()) {
				setTransitChoice(rs.getString("MPGM_TRANSIT_CHOICE"));
				setAuthorizedRequired(rs.getString("MPGM_AUTH_REQD"));
				setParitionYearWise(rs.getString("PARTITION_YEAR_WISE"));
				setProgramType(rs.getString("MPGM_TYPE"));
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError(" isUsagePrevented(1) - " + e.getLocalizedMessage());
		} finally {
			util.reset();
		}
	}

}