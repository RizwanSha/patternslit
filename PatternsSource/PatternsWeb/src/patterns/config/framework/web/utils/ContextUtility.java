package patterns.config.framework.web.utils;

import java.sql.Timestamp;
import java.util.Locale;

import javax.servlet.ServletContext;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.loggers.ApplicationLogger;
import patterns.config.framework.thread.ApplicationContext;
import patterns.config.framework.thread.ApplicationContextImpl;
import patterns.config.framework.web.RequestConstants;
import patterns.config.framework.web.SessionConstants;

public class ContextUtility {

	private final ApplicationLogger logger;

	public ContextUtility() {
		logger = ApplicationLogger.getInstance(this.getClass().getSimpleName());
	}

	public ApplicationContext loadApplicationContext(ServletContext servletContext, HttpSession session, HttpServletRequest request, HttpServletResponse response, boolean isJspRequest, boolean isStrutsRequest, boolean isServletRequest) {
		ApplicationContext applicationContext = (ApplicationContext) session.getAttribute(SessionConstants.APPLICATION_CONTEXT);

		if (session.getAttribute(SessionConstants.USER_ID) != null) {
			if (applicationContext == null || applicationContext.getRoleCode() == null) {
				ApplicationContextImpl context = new ApplicationContextImpl();
				context.setPartitionNo((String) session.getAttribute(SessionConstants.PARTITION_NO));
				context.setEntityCode((String) session.getAttribute(SessionConstants.ENTITY_CODE));
				context.setUserID((String) session.getAttribute(SessionConstants.USER_ID));
				context.setBranchCode((String) session.getAttribute(SessionConstants.BRANCH_CODE));
				context.setCustomerCode((String) session.getAttribute(SessionConstants.CUSTOMER_CODE));
				context.setRoleCode((String) session.getAttribute(SessionConstants.ROLE_CODE));
				context.setRoleType((String) session.getAttribute(SessionConstants.ROLE_TYPE));
				context.setBaseCurrency((String) session.getAttribute(SessionConstants.BASE_CURRENCY));
				context.setBaseCurrencyUnits((String) session.getAttribute(SessionConstants.CURRENCY_UNITS));
				context.setDateFormat((String) session.getAttribute(SessionConstants.DATE_FORMAT));
				context.setCurrentBusinessDate((java.sql.Date) session.getAttribute(SessionConstants.CBD));
				context.setClientIP((String) session.getAttribute(SessionConstants.CLIENT_IP));
				context.setUserAgent((String) session.getAttribute(SessionConstants.USER_AGENT));
				context.setLoginDateTime((Timestamp) session.getAttribute(SessionConstants.LOGIN_DATE_TIME));
				context.setDigitalCertificateInventoryNumber((String) session.getAttribute(SessionConstants.CERT_INV_NUM));
				context.setDeploymentContext((String) session.getAttribute(SessionConstants.DEPLOYMENT_CONTEXT));
				context.setSecureSite((Boolean) session.getAttribute(SessionConstants.SECURE_SITE));
				context.setCurrentYear((String) session.getAttribute(SessionConstants.CURR_YEAR));
				context.setFinYear((String) session.getAttribute(SessionConstants.FIN_YEAR));

				applicationContext = context;
			}
		} else {
			logger.logDebug("Session Unavailable");
			ApplicationContextImpl context = new ApplicationContextImpl();

			String partitionNo = servletContext.getInitParameter(RegularConstants.INIT_DEPLOYMENT_PARTITION);
			session.setAttribute(SessionConstants.PARTITION_NO, partitionNo);
			context.setPartitionNo(partitionNo);

			// String entityCode = servletContext.getInitParameter(RegularConstants.INIT_DEPLOYMENT_ENTITY);
			// session.setAttribute(SessionConstants.ENTITY_CODE, entityCode);
			// context.setEntityCode(entityCode);

			String https = servletContext.getInitParameter(RegularConstants.INIT_HTTPS);
			boolean secureSite = false;
			if (https == null || https.equals(RegularConstants.COLUMN_DISABLE)) {
				secureSite = false;
			} else {
				secureSite = true;
			}
			session.setAttribute(SessionConstants.SECURE_SITE, secureSite);
			context.setSecureSite(secureSite);
			String httpsMode = servletContext.getInitParameter(RegularConstants.INIT_HTTPS);
			boolean httpsEnabled = httpsMode != null && httpsMode.equals(RegularConstants.COLUMN_ENABLE);
			Cookie sessionCookie = new Cookie(RegularConstants.SESSIONID_COOKIE, session.getId());
			sessionCookie.setPath("/");
			if (httpsEnabled) {
				sessionCookie.setSecure(true);
			}
			response.addCookie(sessionCookie);

			String deploymentContext = servletContext.getInitParameter(RegularConstants.INIT_ACCESS_TYPE);
			session.setAttribute(SessionConstants.DEPLOYMENT_CONTEXT, deploymentContext);
			context.setDeploymentContext(deploymentContext);

			String clientIP = request.getRemoteAddr();
			session.setAttribute(SessionConstants.CLIENT_IP, clientIP);
			context.setClientIP(clientIP);

			String userAgent = request.getHeader("User-Agent");
			session.setAttribute(SessionConstants.USER_AGENT, userAgent);
			context.setUserAgent(userAgent);

			applicationContext = context;
		}

		if (applicationContext instanceof ApplicationContextImpl) {
			ApplicationContextImpl context = (ApplicationContextImpl) applicationContext;

			boolean formPosted = isStrutsRequest ? true : false;
			context.setFormPosted(formPosted);

			boolean operationLogRequired = isServletRequest ? false : true;
			context.setOperationLogRequired(operationLogRequired);

			context.setProcessID((String) request.getAttribute(RequestConstants.PROCESS_ID));
			context.setProcessDescription((String) request.getAttribute(RequestConstants.PROCESS_DESCN));
			context.setLocale((Locale) session.getAttribute(SessionConstants.LOCALE));
			context.setRoleCode((String) session.getAttribute(SessionConstants.ROLE_CODE));
			context.setRoleType((String) session.getAttribute(SessionConstants.ROLE_TYPE));
			context.setCallSource(request.getParameter("callSource")==null ? "S" : request.getParameter("callSource"));
		}
		return applicationContext;
	}

	public ApplicationContext loadApplicationContextOnLogin(HttpSession session, HttpServletRequest request) {
		ApplicationContext applicationContext = ApplicationContext.getInstance();

		if (applicationContext != null && applicationContext instanceof ApplicationContextImpl) {
			ApplicationContextImpl context = (ApplicationContextImpl) applicationContext;
			context.setPartitionNo((String) session.getAttribute(SessionConstants.PARTITION_NO));
			context.setEntityCode((String) session.getAttribute(SessionConstants.ENTITY_CODE));
			context.setUserID((String) session.getAttribute(SessionConstants.USER_ID));
			context.setBranchCode((String) session.getAttribute(SessionConstants.BRANCH_CODE));
			context.setCustomerCode((String) session.getAttribute(SessionConstants.CUSTOMER_CODE));
			context.setRoleCode((String) session.getAttribute(SessionConstants.ROLE_CODE));
			context.setRoleType((String) session.getAttribute(SessionConstants.ROLE_TYPE));
			context.setBaseCurrency((String) session.getAttribute(SessionConstants.BASE_CURRENCY));
			context.setBaseCurrencyUnits((String) session.getAttribute(SessionConstants.CURRENCY_UNITS));
			context.setDateFormat((String) session.getAttribute(SessionConstants.DATE_FORMAT));
			context.setCurrentBusinessDate((java.sql.Date) session.getAttribute(SessionConstants.CBD));
			context.setClientIP((String) session.getAttribute(SessionConstants.CLIENT_IP));
			context.setUserAgent((String) session.getAttribute(SessionConstants.USER_AGENT));
			context.setLoginDateTime((Timestamp) session.getAttribute(SessionConstants.LOGIN_DATE_TIME));
			context.setDigitalCertificateInventoryNumber((String) session.getAttribute(SessionConstants.CERT_INV_NUM));
			context.setDeploymentContext((String) session.getAttribute(SessionConstants.DEPLOYMENT_CONTEXT));
			context.setSecureSite((Boolean) session.getAttribute(SessionConstants.SECURE_SITE));
			context.setCurrentYear((String) session.getAttribute(SessionConstants.CURR_YEAR));
			context.setFinYear((String) session.getAttribute(SessionConstants.FIN_YEAR));
			context.setClusterCode((String) session.getAttribute(SessionConstants.CLUSTER_CODE));

			applicationContext = context;
		}
		return applicationContext;
	}

}
