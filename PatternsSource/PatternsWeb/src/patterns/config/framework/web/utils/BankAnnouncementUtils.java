package patterns.config.framework.web.utils;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.thread.ApplicationContext;

public class BankAnnouncementUtils {
	private ApplicationContext context = ApplicationContext.getInstance();

	public static class Announcement {
		private String announceID;
		private String announceTitle;
		private String externalLink;
		private String announceUrl;
		private String announceUrlTitle;

		public String getExternalLink() {
			return externalLink;
		}

		public void setExternalLink(String externalLink) {
			this.externalLink = externalLink;
		}

		public String getAnnounceTitle() {
			return announceTitle;
		}

		public String getAnnounceID() {
			return announceID;
		}

		public void setAnnounceID(String announceID) {
			this.announceID = announceID;
		}

		public void setAnnounceTitle(String announceTitle) {
			this.announceTitle = announceTitle;
		}

		public String getAnnounceUrl() {
			return announceUrl;
		}

		public void setAnnounceUrl(String announceUrl) {
			this.announceUrl = announceUrl;
		}

		public String getAnnounceUrlTitle() {
			return announceUrlTitle;
		}

		public void setAnnounceUrlTitle(String announceUrlTitle) {
			this.announceUrlTitle = announceUrlTitle;
		}

		public Announcement(String announceID, String announceTitle, String announceUrl, String announceUrlTitle, String externalLink) {
			this.announceID = announceID;
			this.announceTitle = announceTitle;
			this.announceUrl = announceUrl;
			this.announceUrlTitle = announceUrlTitle;
			this.externalLink = externalLink;
		}

	}

	public List<BankAnnouncementUtils.Announcement> getAnnounceListBeforeLogin() {
		List<BankAnnouncementUtils.Announcement> announceList = new ArrayList<BankAnnouncementUtils.Announcement>();
		DBContext dbContext = new DBContext();
		try {
			String sqlQuery = "SELECT ANNOUNCE_ID, ANNOUNCE_TITLE, ANNOUNCE_URL, ANNOUNCE_URL_TITLE,EXTERNAL_LINK FROM ANNOUNCEMENTS WHERE ENTITY_CODE = ? AND ANNOUNCE_VISIBLITY = 'B' AND ANNOUNCE_DISABLED = '0' AND ANNOUNCE_START_DT <= FN_GETCD(?) AND ANNOUNCE_END_DT >= FN_GETCD(?) ORDER BY ANNOUNCE_ID DESC";
			DBUtil util = dbContext.createUtilInstance();
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, context.getEntityCode());
			util.setString(2, context.getEntityCode());
			util.setString(3, context.getEntityCode());
			ResultSet rset = util.executeQuery();
			while (rset.next()) {
				announceList.add(new Announcement(rset.getString(1), rset.getString(2), rset.getString(3), rset.getString(4), rset.getString(5)));
			}
			util.reset();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			dbContext.close();
		}
		return announceList;
	}

	public List<BankAnnouncementUtils.Announcement> getAnnounceListAfterLogin() {
		List<BankAnnouncementUtils.Announcement> announceList = new ArrayList<BankAnnouncementUtils.Announcement>();
		DBContext dbContext = new DBContext();
		try {
			// String sqlQuery =
			// "SELECT ANNOUNCE_ID, ANNOUNCE_TITLE, ANNOUNCE_URL, ANNOUNCE_URL_TITLE, EXTERNAL_LINK FROM BANKANNOUNCEMENTS WHERE ENTITY_CODE = ?  AND ANNOUNCE_VISIBLITY = 'A' AND ANNOUNCE_DISABLED = '0' AND ANNOUNCE_START_DT <= FN_GETCD(?) AND ANNOUNCE_END_DT >= FN_GETCD(?) AND (ANNOUNCE_USER_ID = ? OR ANNOUNCE_ROLE_TYPE = ? OR ANNOUNCE_CUSTOMER_CODE = ? OR ANNOUNCE_BRANCH_CODE = ?)";
			String sqlQuery = "SELECT ANNOUNCE_ID, ANNOUNCE_TITLE, ANNOUNCE_URL, ANNOUNCE_URL_TITLE, EXTERNAL_LINK FROM ANNOUNCEMENTS WHERE ENTITY_CODE = ?  AND ANNOUNCE_VISIBLITY = 'A' AND ANNOUNCE_DISABLED = '0' AND ANNOUNCE_START_DT <= FN_GETCD(?) AND ANNOUNCE_END_DT >= FN_GETCD(?) AND (ANNOUNCE_USER_ID = ? OR (ANNOUNCE_ROLE_TYPE = ? AND ANNOUNCE_CUSTOMER_CODE = ?) OR (ANNOUNCE_ROLE_TYPE = ? AND ANNOUNCE_BRANCH_CODE = ?)) ORDER BY ANNOUNCE_ID DESC";
			DBUtil util = dbContext.createUtilInstance();
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, context.getEntityCode());
			util.setString(2, context.getEntityCode());
			util.setString(3, context.getEntityCode());
			util.setString(4, context.getUserID());
			util.setString(5, context.getRoleType());
			util.setString(6, context.getCustomerCode());
			util.setString(7, context.getRoleType());
			util.setString(8, context.getBranchCode());
			ResultSet rset = util.executeQuery();
			while (rset.next()) {
				announceList.add(new Announcement(rset.getString(1), rset.getString(2), rset.getString(3), rset.getString(4), rset.getString(5)));
			}
			util.reset();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			dbContext.close();
		}
		return announceList;
	}

}
