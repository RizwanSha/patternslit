package patterns.config.framework.web.utils;

import java.sql.ResultSet;

import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.web.utils.HTMLMenuUtility;

public class CustomerMenuPreviewUtils {

	private DBContext dbContext;

	public CustomerMenuPreviewUtils(DBContext dbContext) {
		this.dbContext = dbContext;
	}

	public CustomerMenuPreviewUtils() {
		this.dbContext = new DBContext();
	}

	public void close() {
		dbContext.close();
	}

	public String generateCustomerMenu(String entityCode, String serviceBouquet) {
		HTMLMenuUtility utility = new HTMLMenuUtility();
		utility.init();
		try {
			DBUtil utils = dbContext.createUtilInstance();
			utils.reset();
			String sqlQuery = "SELECT SCD.SCAT_CODE,S.DESCRIPTION FROM SCATTYPEALLOCDTL SCD,SCAT S WHERE SCD.ENTITY_CODE =?  AND SCD.SERVICE_TYPE_CODE=? AND SCD.EFFT_DATE = (SELECT MAX(EFFT_DATE) FROM SCATTYPEALLOC WHERE ENTITY_CODE  =? AND SERVICE_TYPE_CODE=? AND EFFT_DATE <=FN_GETCD(?))  AND S.ENABLED='1' AND SCD.ENTITY_CODE = S.ENTITY_CODE AND SCD.SCAT_CODE =S.CODE ORDER BY SCD.SL";
			utils.setSql(sqlQuery);
			utils.setString(1, entityCode);
			utils.setString(2, serviceBouquet);
			utils.setString(3, entityCode);
			utils.setString(4, serviceBouquet);
			utils.setString(5, entityCode);
			ResultSet rset = utils.executeQuery();
			int parentCounter = 0;
			while (rset.next()) {
				parentCounter++;
				utility.startHorizontalCategory(parentCounter, rset.getString(1), "R", rset.getString(2));
				treeTraverse(utility, entityCode, rset.getString(1), "A" + parentCounter, "R");
				utility.endHorizontalCategory();
			}
			utils.reset();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			utility.finish();
		}
		return utility.getHTML();
	}

	public void treeTraverse(HTMLMenuUtility utility, String entityCode, String parentCategory, String ID, String align) {
		try {
			String sqlQuery = "SELECT CODE,DESCRIPTION FROM SCAT WHERE ENTITY_CODE=?  AND CODE=? AND ENABLED='1' ORDER BY CODE ASC";
			DBUtil util = dbContext.createUtilInstance();
			util.reset();
			util.setSql(sqlQuery);
			util.setString(1, entityCode);
			util.setString(2, parentCategory);
			ResultSet rset = util.executeQuery();
			while (rset.next()) {
				int parentCount = 0;
				utility.startParent(ID, align);
				String parentCode = rset.getString(1);
				sqlQuery = "SELECT SCD.SERVICE_TYPE ,SCD.SERVICE_CAT,SCD.SERVICE_CODE FROM SCATALLOCDTL SCD WHERE SCD.ENTITY_CODE=? AND SCD.SCAT_CODE=? AND SCD.EFFT_DATE=(SELECT MAX(EFFT_DATE) FROM SCATALLOC  WHERE ENTITY_CODE =? AND EFFT_DATE<=FN_GETCD(?) AND SCAT_CODE =SCD.SCAT_CODE)";
				DBUtil utilsub = dbContext.createUtilInstance();
				utilsub.reset();
				utilsub.setSql(sqlQuery);
				utilsub.setString(1, entityCode);
				utilsub.setString(2, parentCode);
				utilsub.setString(3, entityCode);
				utilsub.setString(4, entityCode);
				ResultSet rsetsub = utilsub.executeQuery();
				while (rsetsub.next()) {
					parentCount++;
					if (rsetsub.getString(1).equals("S")) {
						sqlQuery = "SELECT DESCRIPTION FROM SERVICES WHERE ENTITY_CODE=? AND CODE=?";
						DBUtil utilchild = dbContext.createUtilInstance();
						utilchild.reset();
						utilchild.setSql(sqlQuery);
						utilchild.setString(1, entityCode);
						utilchild.setString(2, rsetsub.getString(3));
						ResultSet rsetchild = utilchild.executeQuery();
						if (rsetchild.next()) {
							utility.startChild(ID + "_C" + parentCount, rsetchild.getString(1), parentCode, align);
							utility.endChild();
						}
						utilchild.reset();
					} else if (rsetsub.getString(1).equals("C")) {
						sqlQuery = "SELECT DESCRIPTION FROM SCAT WHERE ENTITY_CODE=? AND CODE=?";
						DBUtil utilparent = dbContext.createUtilInstance();
						utilparent.reset();
						utilparent.setSql(sqlQuery);
						utilparent.setString(1, entityCode);
						utilparent.setString(2, rsetsub.getString(2));
						ResultSet rsetparent = utilparent.executeQuery();
						if (rsetparent.next()) {
							utility.startInnerChild(ID + "_P" + parentCount, rsetparent.getString(1), align);
							treeTraverse(utility, entityCode, rsetsub.getString(2), ID + "_P" + parentCount, align);
							utility.endInnerChild();
						}
						utilparent.reset();
					}
				}
				utilsub.reset();
				utility.endParent();
			}
			util.reset();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
		}
	}

}
