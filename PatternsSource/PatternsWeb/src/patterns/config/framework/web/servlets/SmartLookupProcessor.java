package patterns.config.framework.web.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.framework.web.configuration.lookup.MysqlSmartLookupManager;
import patterns.json.JSONObject;

public class SmartLookupProcessor extends AJAXContentProcessor {
	static final long serialVersionUID = 1L;

	public SmartLookupProcessor() {
		super();
	}

	public void process(HttpServletRequest request, HttpServletResponse response, DTObject form) throws ServletException, IOException {
		// getLogger().logDebug("process()");
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		JSONObject jsonResponse = new JSONObject();
		MysqlSmartLookupManager manager = new MysqlSmartLookupManager();
		try {
			manager.init();
			DTObject dataObject = manager.getData(form);
			jsonResponse = (JSONObject) dataObject.getObject(ContentManager.CONTENT);
			getLogger().logDebug(jsonResponse.toString());
		} catch (Exception e) {
			e.printStackTrace();
			getLogger().logError("process(1)" + e.getLocalizedMessage());
		} finally {
			manager.destroy();
		}
		PrintWriter writer = response.getWriter();
		writer.write(jsonResponse.toString());
		writer.flush();
		writer.close();
		// getLogger().logDebug("process(/)");
	}
}
