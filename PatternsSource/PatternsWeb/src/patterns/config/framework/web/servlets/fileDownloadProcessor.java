package patterns.config.framework.web.servlets;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.BaseDomainValidator;

public class fileDownloadProcessor extends AJAXContentProcessor {

	static final long serialVersionUID = 1L;

	private static final int DEFAULT_BUFFER_SIZE = 10240; // 10KB.

	public fileDownloadProcessor() {
		super();
	}

	public void process(HttpServletRequest request, HttpServletResponse response, DTObject form) throws ServletException, IOException {
		boolean processingSuccessful = false;
		getLogger().logDebug("process()");
		String fileName = RegularConstants.EMPTY_STRING;
		String filePath = RegularConstants.EMPTY_STRING;
		
		fileName = request.getParameter("FILENAME");
		filePath = request.getParameter("FILEPATH");
		BaseDomainValidator validator = new BaseDomainValidator();
		try {
			if(validator.isEmpty(filePath)){
				form = validator.getSystemFolders(form);
				if (form.get(ContentManager.ERROR) == null) {
					if (form.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
						filePath = form.get("FILE_DOWNLOAD_PATH");
					}
				}
			}
			filePath = filePath + fileName;
			File file = new File(filePath);
			if (file.exists()) {
				String contentType = getServletContext().getMimeType(file.getName());
				if (contentType == null) {
					contentType = "application/octet-stream";
				}
				response.reset();
				response.setBufferSize(DEFAULT_BUFFER_SIZE);
				response.setContentType(contentType);
				response.setHeader("Content-Length", String.valueOf(file.length()));
				response.setHeader("Content-Disposition", "attachment; filename=\"" + file.getName() + "\"");

				BufferedInputStream input = null;
				BufferedOutputStream output = null;

				try {
					input = new BufferedInputStream(new FileInputStream(file), DEFAULT_BUFFER_SIZE);
					output = new BufferedOutputStream(response.getOutputStream(), DEFAULT_BUFFER_SIZE);

					byte[] buffer = new byte[DEFAULT_BUFFER_SIZE];
					int length;
					while ((length = input.read(buffer)) > 0) {
						output.write(buffer, 0, length);
					}
					output.flush();
					processingSuccessful = true;
				} catch (Exception e) {
					e.printStackTrace();
					getLogger().logError("process(1) " + e.getLocalizedMessage());
					processingSuccessful = false;
				} finally {
					close(output);
					close(input);
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			getLogger().logError("process(1) " + e.getLocalizedMessage());
		} finally {
			validator.close();
		}
		if (!processingSuccessful) {
			response.sendError(HttpServletResponse.SC_NOT_FOUND);
			return;
		}
		getLogger().logDebug("process(/)");
	}

	private static void close(Closeable resource) {
		if (resource != null) {
			try {
				resource.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
