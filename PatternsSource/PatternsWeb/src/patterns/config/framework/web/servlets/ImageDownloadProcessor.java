package patterns.config.framework.web.servlets;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.Closeable;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.BaseDomainValidator;

public class ImageDownloadProcessor extends AJAXContentProcessor {

	static final long serialVersionUID = 1L;

	private static final int DEFAULT_BUFFER_SIZE = 10240; // 10KB.

	public ImageDownloadProcessor() {
		super();
	}

	public void process(HttpServletRequest request, HttpServletResponse response, DTObject form) throws ServletException, IOException {
		boolean processingSuccessful = false;
		getLogger().logDebug("process()");
		String inventoryNumber = request.getParameter("INVNUM");
		String type = request.getParameter("TYPE");
		BaseDomainValidator validator = new BaseDomainValidator();
		try {

			DTObject formDTO = new DTObject();
			formDTO.reset();
			formDTO.set(ContentManager.CODE, inventoryNumber);
			if(request.getParameter("INVSRC").equals("T")){
				formDTO = validator.validateFileInventory(formDTO);
			}
			else{
				formDTO = validator.validateSourceInventory(formDTO);
			}
			
			if (formDTO.get(ContentManager.ERROR) != null) {
				return;
			}
			if (formDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				return;

			} else {
				byte[] fileData = null;
				String contentType = null;
				if (contentType == null) {
					contentType = "application/octet-stream";
				}
				response.reset();
				response.setBufferSize(DEFAULT_BUFFER_SIZE);
				String fileExtension = formDTO.get("FILE_EXTENSION");
				
				if (type.equals("M")) {
					fileData = (byte[]) formDTO.getObject("FILE_DATA");
					if (fileExtension.equalsIgnoreCase("pdf")) {
						contentType = "application/pdf";
						response.setContentType(contentType);
						response.setHeader("Content-Disposition", "filename='" + formDTO.get("FILE_NAME") + "'");
					} else if (fileExtension.equalsIgnoreCase("doc") || fileExtension.equalsIgnoreCase("docx")) {
						contentType = "application/msword";
						response.setContentType(contentType);
						response.setHeader("Content-Disposition", "inline;filename=" + formDTO.get("FILE_NAME"));
						response.setHeader("Content-Description", "Microsoft Word Format");
						response.setHeader("File-Extensions", "docx");
					} else {
						contentType = "application/octet-stream";
						response.setHeader("Content-Disposition", "attachment;filename='" + formDTO.get("FILE_NAME") + "'");
						response.setContentType(contentType);
						response.setHeader("Content-Length", String.valueOf(fileData.length));
					}
				} else {
					fileData = (byte[]) formDTO.getObject("THUMBNAIL_FILE");
					contentType = "application/octet-stream";
					response.setHeader("Content-Disposition", "attachment;filename='" + formDTO.get("FILE_NAME") + "'");
					response.setContentType(contentType);
					response.setHeader("Content-Length", String.valueOf(fileData.length));
				}
				
				ByteArrayInputStream input = null;
				BufferedOutputStream output = null;

				try {

					input = new ByteArrayInputStream(fileData);
					output = new BufferedOutputStream(response.getOutputStream(), DEFAULT_BUFFER_SIZE);
					byte[] buffer = new byte[DEFAULT_BUFFER_SIZE];
					int length;
					while ((length = input.read(buffer)) > 0) {
						output.write(buffer, 0, length);
					}

					output.flush();
					processingSuccessful = true;
				} catch (Exception e) {
					e.printStackTrace();
					getLogger().logError("process(1) " + e.getLocalizedMessage());
					processingSuccessful = false;
				} finally {
					close(output);
					close(input);
				}
			
			}

		} catch (Exception e) {
			e.printStackTrace();
			getLogger().logError("process(1) " + e.getLocalizedMessage());
		} finally {
			validator.close();
		}
		if (!processingSuccessful) {
			response.sendError(HttpServletResponse.SC_NOT_FOUND);
			return;
		}
		getLogger().logDebug("process(/)");
	}

	private static void close(Closeable resource) {
		if (resource != null) {
			try {
				resource.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}