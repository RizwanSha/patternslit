package patterns.config.framework.web.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.RequestConstants;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.framework.web.ajax.ErrorMap;
import patterns.json.JSONObject;

public class TransactionResponseProcessor extends AJAXContentProcessor {
	static final long serialVersionUID = 1L;

	public TransactionResponseProcessor() {
		super();
	}

	public void process(HttpServletRequest request, HttpServletResponse response, DTObject form) throws ServletException, IOException {
		response.setContentType("text/xml");
		response.setCharacterEncoding("UTF-8");
		JSONObject jsonResponse = new JSONObject();
		JSONObject jsonTransaction = new JSONObject();
		jsonResponse.put(ContentManager.TRANSACTION, jsonTransaction);
		try {

			String action = request.getParameter(RequestConstants.SUCCESS_FLAG);
			if (action == null) {
				action = (String) request.getAttribute(RequestConstants.SUCCESS_FLAG);
			}
			jsonTransaction.put(ContentManager.STATUS, action);
			jsonTransaction.put(RequestConstants.ADDITIONAL_INFO, request.getParameter(RequestConstants.ADDITIONAL_INFO));
			jsonTransaction.put(RequestConstants.ADDITIONAL_DETAILS, request.getParameter(RequestConstants.ADDITIONAL_DETAILS));
			jsonTransaction.put(RequestConstants.REDIRECT_LINK, request.getParameter(RequestConstants.REDIRECT_LINK));
			jsonTransaction.put(RequestConstants.CURRENT_OPERATION, request.getParameter(RequestConstants.CURRENT_OPERATION));

		} catch (Exception e) {
			e.printStackTrace();
			getLogger().logError("process(1)" + e.getLocalizedMessage());
		} finally {
		}
		System.out.println(jsonResponse.toString());
		PrintWriter writer = response.getWriter();
		writer.write(jsonResponse.toString());
		writer.flush();
		writer.close();
	}
}
