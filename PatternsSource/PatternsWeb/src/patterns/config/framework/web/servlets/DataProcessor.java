package patterns.config.framework.web.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.framework.web.configuration.data.DataManager;

public class DataProcessor extends AJAXContentProcessor {

	static final long serialVersionUID = 1L;

	public DataProcessor() {
		super();
	}

	public void process(HttpServletRequest request, HttpServletResponse response, DTObject form) throws ServletException, IOException {
		// getLogger().logDebug("process()");
		response.setContentType("text/xml");
		response.setCharacterEncoding("UTF-8");
		StringBuffer buffer = new StringBuffer();
		buffer.append("<").append(ContentManager.ROWS).append(">");
		DataManager manager = new DataManager();
		try {
			manager.init();
			DTObject dataObject = manager.getData(form);
			if (!dataObject.get(ContentManager.ERROR).equals(ContentManager.EMPTY_STRING)) {
				buffer.append("<").append(ContentManager.RECORD).append(">");
				buffer.append("<").append(ContentManager.ERROR).append(">");
				buffer.append(getErrorMessage(dataObject.get(ContentManager.ERROR),dataObject.getObjectL(ContentManager.ERROR_PARAM)));
				buffer.append("</").append(ContentManager.ERROR).append(">");
				buffer.append("<").append(ContentManager.ERROR_FIELD).append(">");
				buffer.append(dataObject.get(ContentManager.ERROR_FIELD)!=null?dataObject.get(ContentManager.ERROR_FIELD):RegularConstants.EMPTY_STRING);
				buffer.append("</").append(ContentManager.ERROR_FIELD).append(">");
				buffer.append("</").append(ContentManager.RECORD).append(">");
			} else {
				String content = dataObject.toXML();
				buffer.append("<").append(ContentManager.RECORD).append(">");
				buffer.append(content);
				buffer.append("<").append(ContentManager.ERROR).append(">");
				buffer.append(ContentManager.EMPTY_STRING);
				buffer.append("</").append(ContentManager.ERROR).append(">");
				buffer.append("</").append(ContentManager.RECORD).append(">");
			}
			buffer.append("</").append(ContentManager.ROWS).append(">");
		} catch (Exception e) {
			e.printStackTrace();
			getLogger().logError("process(1) " + e.getLocalizedMessage());
		} finally {
			manager.destroy();
		}

		PrintWriter writer = response.getWriter();
		String result = buffer.toString();
		getLogger().logDebug(result);
		writer.write(result);
		writer.flush();
		writer.close();
		// getLogger().logDebug("process(/)");
	}
}