package patterns.config.framework.web.servlets;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import patterns.config.framework.service.DTObject;

public abstract class AJAXContentProcessor extends ContentProcessor {
	static final long serialVersionUID = 1L;

	public AJAXContentProcessor() {
		super();
	}

	@SuppressWarnings("unchecked")
	public final void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//getLogger().logDebug("doPost()");
		DTObject form = new DTObject();
		Enumeration<String> parameterNames = (Enumeration<String>) request.getParameterNames();
		while (parameterNames.hasMoreElements()) {
			String key = parameterNames.nextElement();
			String value = request.getParameter(key);
			form.set(key, value);
		}
		process(request, response, form);
		//getLogger().logDebug("doPost(/)");
	}

}