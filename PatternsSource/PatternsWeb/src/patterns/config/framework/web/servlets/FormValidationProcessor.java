package patterns.config.framework.web.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ResourceBundle;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import patterns.config.framework.service.DTObject;
import patterns.config.framework.thread.ApplicationContext;
import patterns.config.framework.web.RequestConstants;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.framework.web.ajax.ErrorMap;
import patterns.json.JSONObject;

public class FormValidationProcessor extends AJAXContentProcessor {

	static final long serialVersionUID = 1L;

	private static final int DEFAULT_BUFFER_SIZE = 10240; // 10KB.

	public FormValidationProcessor() {
		super();
	}

	public void process(HttpServletRequest request, HttpServletResponse response, DTObject form) throws ServletException, IOException {
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		String errorsPresent = (String) request.getAttribute(RequestConstants.ERRORS_PRESENT);
		ErrorMap errorMap = null;
		JSONObject jsonResponse = new JSONObject();
		JSONObject jsonErrors = new JSONObject();
		jsonResponse.put(ContentManager.ERRORS, jsonErrors);
		try {
			if (errorsPresent != null) {
				errorMap = (ErrorMap) request.getAttribute(RequestConstants.ERROR_MAP);
			}
			if (errorMap.length() > 0) {
				ResourceBundle bundle = ResourceBundle.getBundle("patterns/config/web/forms/Validation", ApplicationContext.getInstance().getLocale());
				for (String field : errorMap.getFields()) {
					jsonErrors.put(field, bundle.getString(errorMap.getErrorKey(field)));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			getLogger().logError("process(1)" + e.getLocalizedMessage());
		} finally {
		}
		System.out.println(jsonResponse.toString());
		PrintWriter writer = response.getWriter();
		writer.write(jsonResponse.toString());
		writer.flush();
		writer.close();
	}

}