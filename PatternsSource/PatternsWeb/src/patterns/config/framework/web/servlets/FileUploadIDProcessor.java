package patterns.config.framework.web.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import patterns.config.framework.service.DTObject;
import patterns.config.framework.thread.WebContext;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.BaseDomainValidator;

public class FileUploadIDProcessor extends RawContentProcessor {
	private static final long serialVersionUID = 8490385659395361708L;

	public final void process(HttpServletRequest request, HttpServletResponse response, DTObject inputDTO) throws ServletException, IOException {
		getLogger().logDebug("process()");
		PrintWriter writer = response.getWriter();
		BaseDomainValidator baseDomainValidator = new BaseDomainValidator();
		try {
			DTObject formDTO = new DTObject();
			formDTO = baseDomainValidator.generateInventorySequence(formDTO);
			if (formDTO.get(ContentManager.ERROR) != null) {
				getLogger().logError("process(1) :: " + getErrorMessage(formDTO.get(ContentManager.ERROR)));
				getLogger().logDebug("process(/1)");
				return;
			}
			if (formDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
				getLogger().logError("process(2) :: Inventoty Generation Error");
				getLogger().logDebug("process(/2)");
				return;
			}
			String inventoryNumber = formDTO.get("INV_SEQ");
			WebContext.getInstance().getSession().setAttribute("FileUpload.Progress." + inventoryNumber, "0");
			writer.print(inventoryNumber);
			writer.flush();
		} catch (Exception e) {
			e.printStackTrace();
			getLogger().logError("process(3) :: " + e.getLocalizedMessage());
		} finally {
			baseDomainValidator.close();
			if (writer != null)
				writer.close();
		}
		getLogger().logDebug("process(/3)");
		return;
	}
}
