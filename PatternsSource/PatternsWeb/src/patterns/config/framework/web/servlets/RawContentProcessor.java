package patterns.config.framework.web.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public abstract class RawContentProcessor extends ContentProcessor {
	static final long serialVersionUID = 1L;

	public RawContentProcessor() {
		super();
	}

	public final void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// getLogger().logDebug("doPost()");
		process(request, response, null);
		// getLogger().logDebug("doPost(/)");
	}

}