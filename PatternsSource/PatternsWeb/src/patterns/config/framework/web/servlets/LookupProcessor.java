package patterns.config.framework.web.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import patterns.config.framework.DatasourceConfigurationManager;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBInfo;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.AJAXContentManager;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.framework.web.configuration.lookup.LookupMap;
import patterns.config.framework.web.configuration.lookup.MysqlLookupManager;
import patterns.config.framework.web.configuration.lookup.OrclLookupManager;

public class LookupProcessor extends AJAXContentProcessor {
	static final long serialVersionUID = 1L;

	public LookupProcessor() {
		super();
	}

	public void process(HttpServletRequest request, HttpServletResponse response, DTObject form) throws ServletException, IOException {
		// getLogger().logDebug("process()");
		response.setContentType("text/xml");
		response.setCharacterEncoding("UTF-8");
		StringBuffer buffer = new StringBuffer();
		MysqlLookupManager manager = new MysqlLookupManager();
		try {
			manager.init();
			DTObject dataObject = manager.getData(form);
			String content = dataObject.get(ContentManager.CONTENT);
			getLogger().logDebug(content);
			buffer.append(content);
		} catch (Exception e) {
			e.printStackTrace();
			getLogger().logError("process(1)" + e.getLocalizedMessage());
		} finally {
			manager.destroy();
		}
		PrintWriter writer = response.getWriter();
		writer.write(buffer.toString());
		writer.flush();
		writer.close();
		// getLogger().logDebug("process(/)");
	}
}
