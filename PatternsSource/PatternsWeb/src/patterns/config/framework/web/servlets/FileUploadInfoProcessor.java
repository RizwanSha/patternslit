package patterns.config.framework.web.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import patterns.config.framework.service.DTObject;
import patterns.config.framework.thread.WebContext;

public class FileUploadInfoProcessor extends RawContentProcessor {
	private static final long serialVersionUID = 8490385659395361708L;

	public final void process(HttpServletRequest request, HttpServletResponse response, DTObject inputDTO) throws ServletException, IOException {
		getLogger().logDebug("process()");
		PrintWriter writer = response.getWriter();
		try {
			String inventoryNumber = WebContext.getInstance().getRequest().getParameter("sessionId");
			Object status = WebContext.getInstance().getSession().getAttribute("FileUpload.Progress." + inventoryNumber);
			String value = null;
			if (status != null)
				value = status.toString();
			writer.print(value);
			writer.flush();
		} catch (Exception e) {
			e.printStackTrace();
			getLogger().logError("process(3) :: " + e.getLocalizedMessage());
		} finally {
			if (writer != null)
				writer.close();
		}
		getLogger().logDebug("process(/)");
	}
}
