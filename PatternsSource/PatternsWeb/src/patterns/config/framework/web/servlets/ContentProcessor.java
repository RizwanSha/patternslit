package patterns.config.framework.web.servlets;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import patterns.config.framework.loggers.ApplicationLogger;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.thread.ApplicationContext;

public abstract class ContentProcessor extends javax.servlet.http.HttpServlet {
	static final long serialVersionUID = 1L;
	private ApplicationLogger logger = null;

	public ApplicationLogger getLogger() {
		return logger;
	}

	public ContentProcessor() {
		logger = ApplicationLogger.getInstance(this.getClass().getSimpleName());
		logger.logDebug("#()");
	}

	public final void destroy() {
		logger.logDebug("destroy()");
		super.destroy();
	}

	public final void init() throws ServletException {
		logger.logDebug("init()");
		super.init();
	}

	@Override
	protected final void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// logger.logDebug("doGet()");
		doPost(request, response);
		// logger.logDebug("doGet(/)");
	}

	public final String getErrorMessage(String errorKey, Object... params) {
		ApplicationContext context = ApplicationContext.getInstance();
		// logger.logDebug("getErrorMessage()");
		ResourceBundle bundle = ResourceBundle.getBundle("patterns/config/web/forms/Validation", context.getLocale());
		String message = bundle.getString(errorKey);
		return MessageFormat.format(message, params);
	}

	public abstract void process(HttpServletRequest request, HttpServletResponse response, DTObject form) throws ServletException, IOException;

	public static Map<String, String> getUrlParameters(String url) throws UnsupportedEncodingException {
		Map<String, String> params = new HashMap<String, String>();
		String[] urlParts = url.split("\\?");
		if (urlParts.length > 1) {
			String query = urlParts[1];
			for (String param : query.split("&")) {
				String pair[] = param.split("=");
				String key = URLDecoder.decode(pair[0], "UTF-8");
				String value = "";
				if (pair.length > 1) {
					value = URLDecoder.decode(pair[1], "UTF-8");
				}
				params.put(key, value);
			}
		}
		return params;
	}

}