package patterns.config.framework.web.servlets;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.BaseDomainValidator;

public class FileUploadProcessor extends RawContentProcessor {
	private static final long serialVersionUID = 8490385659395361708L;
	String programId = RegularConstants.NULL;
	String instanceId = RegularConstants.NULL;
	// private String ALLOWED_FILE_EXTENSIONS = "";
	HttpServletResponse processorResponse = null;

	private void setError(String errorMessage) throws IOException {
		processorResponse.getWriter().write("{\"state\":false, \"extra\":{\"info\":\"error\",\"param\":\"Server Error:" + errorMessage + " \"}}");
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public void process(HttpServletRequest request, HttpServletResponse response, patterns.config.framework.service.DTObject form) throws ServletException, IOException {

		getLogger().logDebug("process()");
		processorResponse = response;
		String inventoryNumber = "";
		String temporaryFileUploadPath = "";
		String purpose = RegularConstants.NULL;
		String allowedExt = RegularConstants.NULL;
		String filename = "";
		double filesize = 0;
		// boolean checksumReqd = false;
		// boolean isMultipart = FileUpload.isMultipartContent(request);
		String mode = request.getParameter("mode");
		String action = "";
		ServletFileUpload uploader = null;
		List<FileItem> items = null;
		try {
			if (mode == null || (mode != null && !mode.equals("conf") && !mode.equals("sl"))) {
				uploader = new ServletFileUpload(new DiskFileItemFactory());
				items = uploader.parseRequest(request);
			}

			if (mode == null) {
				mode = "";
				for (FileItem item : items) {
					if (item.getFieldName().equals("mode")) {
						InputStream is = item.getInputStream();
						BufferedReader br = new BufferedReader(new InputStreamReader(is));
						StringBuilder sb = new StringBuilder();
						String line;
						while ((line = br.readLine()) != null) {
							sb.append(line);
						}
						mode = sb.toString();
					}

					if (item.getFieldName().equals("action")) {
						InputStream is = item.getInputStream();
						BufferedReader br = new BufferedReader(new InputStreamReader(is));
						StringBuilder sb = new StringBuilder();
						String line;
						while ((line = br.readLine()) != null) {
							sb.append(line);
						}
						action = sb.toString();
					}
				}
			}
			if (mode.equals("conf")) {
				int maxPostSize = 2000000;
				response.setHeader("Content-type", "text/json");
				response.getWriter().write("{ \"maxFileSize\":" + maxPostSize + " }");
			}
			if (mode.equals("html4") || mode.equals("flash") || mode.equals("html5")) {
				response.setHeader("Content-type", "text/html");
				if (action.equals("cancel")) {
					response.getWriter().write("{\"state\":\"cancelled\"}");
				} else {
					temporaryFileUploadPath = request.getParameter("FILE_PATH");
					BaseDomainValidator validator = new BaseDomainValidator();
					DTObject formDTO = new DTObject();
					if (validator.isEmpty(temporaryFileUploadPath)) {
						try {
							formDTO = validator.getSystemFolders(formDTO);
							if (formDTO.get(ContentManager.ERROR) != null) {
								getLogger().logError("process(1) :: " + getErrorMessage(formDTO.get(ContentManager.ERROR)));
								getLogger().logDebug("process(/1)");
								setError(getErrorMessage(formDTO.get(ContentManager.ERROR)));
								return;
							}
							if (formDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
								getLogger().logError("process(2) :: System Folder Unavailable");
								getLogger().logDebug("process(/2)");
								setError(getErrorMessage(BackOfficeErrorCodes.SYSFOLDER_CFG_UNAVAILABLE));
								return;
							}
							temporaryFileUploadPath = formDTO.get("FILE_UPLOAD_PATH");
						} catch (Exception e) {
							e.printStackTrace();
							validator.close();
							setError(getErrorMessage(BackOfficeErrorCodes.UNSPECIFIED_ERROR));
							return;
						}
					}
					boolean fileUploaded = false;
					try {
						Iterator iter = items.iterator();
						File uploadedFile = null;
						while (iter.hasNext()) {
							FileItem item = (FileItem) iter.next();
							if (!item.isFormField()) {
								filename = item.getName();
								int i2 = filename.lastIndexOf("\\");
								if (i2 > -1)
									filename = filename.substring(i2 + 1);

								File dirs = new File(temporaryFileUploadPath);
								// dirs.mkdirs();
								if (dirs.exists()) {
									if (!dirs.canRead()) {
										setError(getErrorMessage(BackOfficeErrorCodes.UPLOAD_DIR_NOT_READ));
										return;
									}
									if (!dirs.canWrite()) {
										setError(getErrorMessage(BackOfficeErrorCodes.UPLOAD_DIR_NOT_WRITE));
										return;
									}
									uploadedFile = new File(dirs, filename);
									item.write(uploadedFile);
									fileUploaded = true;
								} else {
									setError(getErrorMessage(BackOfficeErrorCodes.UPLOAD_DIR_NOT_EXIST));
									return;
								}
							}
						}
						if (!fileUploaded) {
							setError(getErrorMessage(BackOfficeErrorCodes.FILE_UPLOAD_FAILED));
							return;
						}
						if (fileUploaded) {
							String uploadFileName = uploadedFile.getName();
							allowedExt = request.getParameter("ALLOWED_EXTENSIONS");
							programId = request.getParameter("PGM_ID");
							purpose = request.getParameter("PURPOSE");
							byte[] uploadData;
							byte[] ThumbnailData = null;
							String checkSum = null;
							formDTO = new DTObject();
							String fileFormat = uploadFileName.substring(uploadFileName.lastIndexOf(".") + 1, uploadFileName.length());
							filesize = uploadedFile.length() * 1.0d / (1024d * 1204d);
							if (allowedExt == null || allowedExt.equals(RegularConstants.EMPTY_STRING)) {
								DTObject input = new DTObject();
								input.set("FILE_FORMAT", fileFormat);
								input.set("PGM_ID", programId);
								input.set("PURPOSE", purpose);
								DTObject filePurposeDTO = validator.readFilePurposeParameter(input);
								if (filePurposeDTO.get(ContentManager.ERROR) != null) {
									setError(getErrorMessage(filePurposeDTO.get(ContentManager.ERROR)));
									return;
								}
								if (filePurposeDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
									setError(getErrorMessage(BackOfficeErrorCodes.FILEPURPOSE_CFG_UNAVAILABLE));
									return;
								}

								try {

									if (!validator.isValidFileFormat(filePurposeDTO.get("ALLOWED_EXTENSIONS"), fileFormat)) {
										setError(getErrorMessage(BackOfficeErrorCodes.INVALID_FILE_FORMAT));
										return;
									}

									/* UPDATE FILE INVENTORY BEGIN */

									try {
										if (filePurposeDTO.get("THUMBNAIL_REQD").equals("1"))
											ThumbnailData = validator.getThumbnail(uploadedFile);
									} catch (Exception e) {
										getLogger().logError("process(5) :: " + e.getLocalizedMessage());
										setError(getErrorMessage(BackOfficeErrorCodes.UPLOAD_FILE_THUMBNAIL_ERROR));
										return;
									}

									try {
										if (filePurposeDTO.get("CHECKSUM_REQD").equals("1"))
											checkSum = validator.getFileChecksum(uploadedFile);
									} catch (Exception e) {
										getLogger().logError("process(5) :: " + e.getLocalizedMessage());
										setError(getErrorMessage(BackOfficeErrorCodes.UPLOAD_FILE_CHECKSUM_ERROR));
										return;
									}
								} catch (Exception e) {
									e.printStackTrace();
									getLogger().logError("process(14) :: " + e.getLocalizedMessage());
									setError(getErrorMessage(BackOfficeErrorCodes.UNSPECIFIED_ERROR));
									return;
								}
							} else {
								if (!validator.isValidFileFormat(allowedExt, fileFormat)) {
									setError(getErrorMessage(BackOfficeErrorCodes.INVALID_FILE_FORMAT));
									return;
								}
							}
							try {

								long uploadedFileSizeRawLength = uploadedFile.length();
								if (uploadedFileSizeRawLength == 0) {
									setError(getErrorMessage(BackOfficeErrorCodes.BLANK_FILE));
									return;
								}

								try {
									uploadData = validator.getFileData(uploadedFile);
									int len = uploadData.length;
								} catch (Exception e) {
									getLogger().logError("process(5) :: " + e.getLocalizedMessage());
									setError(getErrorMessage(BackOfficeErrorCodes.UPLOAD_FILE_NOT_READ));
									return;
								}

								formDTO = validator.generateFileInventorySequence(formDTO);
								if (formDTO.get(ContentManager.ERROR) != null) {
									setError(getErrorMessage(formDTO.get(ContentManager.ERROR)));
									return;
								}
								if (formDTO.get(ContentManager.RESULT).equals(ContentManager.DATA_UNAVAILABLE)) {
									setError(getErrorMessage(BackOfficeErrorCodes.FILE_INVENTORY_GENERROR));
									return;
								}

								// SEQ_FILEUPLOAD
								inventoryNumber = formDTO.get("INV_SEQ");
								String status = RegularConstants.COLUMN_DISABLE;
								formDTO.reset();
								formDTO.set("INV_NUM", inventoryNumber);
								formDTO.setObject("FILE", uploadData);
								formDTO.setObject("THUMB_NAIL", ThumbnailData);
								formDTO.set("FILE_NAME", uploadFileName);
								formDTO.set("IN_USE", status);
								formDTO.set("PURPOSE", purpose);
								formDTO.set("EXTENSION", fileFormat);
								// formDTO.set("FILE_DSCN", descn);
								formDTO.set("GROUP_FILE_INV", inventoryNumber);
								formDTO.set("CHECKSUM", checkSum);

								try {
									boolean updated = validator.updateFileInventory(formDTO);
									if (!updated) {
										setError(getErrorMessage(BackOfficeErrorCodes.INVALID_FILE_UPLOAD));
										return;
									}
								} catch (Exception e) {
									getLogger().logError("process(6) :: " + e.getLocalizedMessage());
									setError(getErrorMessage(BackOfficeErrorCodes.INVALID_FILE_UPLOAD));
									return;
								}
								/* UPDATE FILE INVENTORY END */

							} catch (Exception e) {
								e.printStackTrace();
								getLogger().logError("process(14) :: " + e.getLocalizedMessage());
								setError(getErrorMessage(BackOfficeErrorCodes.UNSPECIFIED_ERROR));
								return;
							} finally {
								validator.close();
							}
							response.getWriter().write("{\"state\":true,\"name\":\"" + filename.replace("\"", "\\\"") + "\",\"size\":" + filesize
							// + ",\"extra\":{\"info\":\"invNo\",\"param\":\"" +
							// inventoryNumber + "\"}}");
							// + ",\"extra\":{\"invNo\":\"" +
							// inventoryNumber + "\"}}");
									+ ",\"extra\":{\"invNo\":\"" + inventoryNumber + "\",\"format\":\"" + fileFormat + "\" }}");
						}
					} catch (Exception e) {
						getLogger().logError("process(15) :: " + e.getLocalizedMessage());
						setError(getErrorMessage(BackOfficeErrorCodes.UNSPECIFIED_ERROR));
						return;
					}
				}
			}
		} catch (FileUploadException e) {
			response.getWriter().write("{state:'cancelled'}");
		}
	}
}
