package patterns.config.framework.web.filters;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileUpload;

import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.loggers.ApplicationLogger;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.thread.ApplicationContext;
import patterns.config.framework.thread.WebContext;
import patterns.config.framework.web.BackOfficeOperationLogUtils;
import patterns.config.framework.web.DHTMLXGridUtility;
import patterns.config.framework.web.RequestConstants;
import patterns.config.framework.web.SessionConstants;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.framework.web.configuration.program.ProgramConfiguration;
import patterns.config.framework.web.configuration.program.ProgramMap;
import patterns.config.validations.AccessValidator;

public class ApplicationSecurityFilter extends CommonFilter {

	private ApplicationLogger logger = null;
	private static Set<String> ignoreExtensionSet = new HashSet<String>();
	static {
		ignoreExtensionSet.add(".css");
		ignoreExtensionSet.add(".js");
		ignoreExtensionSet.add(".xml");
		ignoreExtensionSet.add(".gif");
		ignoreExtensionSet.add(".png");
		ignoreExtensionSet.add(".jpg");
		ignoreExtensionSet.add(".jpeg");
		ignoreExtensionSet.add(".doc");
		ignoreExtensionSet.add(".pdf");
		ignoreExtensionSet.add(".html");
	}

	public ApplicationSecurityFilter() {
		super();
		logger = ApplicationLogger.getInstance(this.getClass().getSimpleName());
		// logger.logDebug("#()");
	}

	public void processRequest(FilterChain chain) throws IOException, ServletException {
		// logger.logDebug("processRequest() BEGIN");
		WebContext webContext = WebContext.getInstance();
		HttpServletRequest _request = webContext.getRequest();
		HttpServletResponse _response = webContext.getResponse();
		HttpSession session = _request.getSession(false);

		String _path = _request.getContextPath();
		String _basePath = _request.getScheme() + "://" + _request.getServerName() + ":" + _request.getServerPort() + _path + "/";
		String unauthAccessURL = _basePath + "Renderer/common/eunauthaccess.jsp";
		boolean ajaxRequest = false;
		@SuppressWarnings("deprecation")
		boolean isMultipart = FileUpload.isMultipartContent(_request);
		if (!isMultipart) {
			String ajaxToken = _request.getParameter(RequestConstants.AJAX_TOKEN);
			if (ajaxToken != null && ajaxToken.equals(RegularConstants.COLUMN_ENABLE)) {
				ajaxRequest = true;
			}
		}
		logger.logDebug("AJAX : " + ajaxRequest);
		/* CacheDisable Begins */
		_response.setCharacterEncoding("UTF-8");
		_response.setDateHeader("Expires", 0);
		// _response.setHeader("Pragma", "no-cache");
		_response.setHeader("Pragma", "no-store");
		if (_request.getProtocol().equals("HTTP/1.1")) {
			// _response.setHeader("Cache-Control",
			// "max-age=0, no-cache, must-revalidate, no-store");
			_response.setHeader("Cache-Control", "no-store");
		}
		/* CacheDisable Ends */
		ApplicationContext context = ApplicationContext.getInstance();
		String programID = context.getProcessID();

		ProgramConfiguration programConfiguration = ProgramMap.getInstance().getConfiguration(programID);
		if (programConfiguration == null) {
			String URI = _request.getRequestURI();
			if (URI.indexOf(".") >= 0) {
				String extension = URI.substring(URI.indexOf("."));
				if (ignoreExtensionSet.contains(extension.toLowerCase())) {
					return;
				}
			}
			logger.logError("processRequest() : Missing Program Configuration " + programID);
			_response.sendRedirect(unauthAccessURL);
			return;
		}

		String referer = webContext.getRequest().getHeader("Referer");
		if (referer == null) {
			if (programConfiguration.isRefererCheckRequired()) {
				logger.logError("processRequest() : Missing Referer " + programID);
				_response.sendRedirect(unauthAccessURL);
				return;
			}
		}

		String roleCode = context.getRoleCode();
		String queryProgramID = programConfiguration.getQueryProgramID();
		boolean operationLogRequired = programConfiguration.isOperationLogRequired();
		boolean sessionRequired = programConfiguration.isSessionRequired();
		boolean allocationRequired = programConfiguration.isAllocationRequired();
		boolean forcedPasswordRequired = programConfiguration.isForcedPasswordContext();
		boolean loginAuthenticationRequired = programConfiguration.isLoginAuthenticationContext();

		String rendererType = programConfiguration.getRendererType();
		_request.setAttribute(RequestConstants.RENDERER_TYPE, rendererType);
		if (rendererType.equals(RequestConstants.RENDERER_TYPE_CUSTOM) || rendererType.equals(RequestConstants.RENDERER_TYPE_FORM) || rendererType.equals(RequestConstants.RENDERER_TYPE_VIEW) || rendererType.equals(RequestConstants.RENDERER_TYPE_FORM_FULLSCREEEN)) {
			_response.setContentType("text/html");
		}

		if (!sessionRequired) {
			chain.doFilter(_request, _response);
		} else {
			boolean sessionValid = false;
			String userID = null;
			if (session != null)
				userID = (String) session.getAttribute(SessionConstants.USER_ID);
			if (userID != null && !userID.trim().equals(ContentManager.EMPTY_STRING)) {
				boolean nonceCookieValid = false;
				Cookie[] cookies = _request.getCookies();
				for (Cookie cookie : cookies) {
					if (cookie.getName().equals(RegularConstants.NONCE_COOKIE)) {
						String cookieValue = cookie.getValue();
						String tfaNonceValue = (String) session.getAttribute(SessionConstants.TFA_NONCE);
						if (tfaNonceValue.equals(cookieValue)) {
							nonceCookieValid = true;
						}
						break;
					}
				}
				if (!nonceCookieValid) {
					if (ajaxRequest) {
						DHTMLXGridUtility utility = new DHTMLXGridUtility();
						utility.init(true);
						utility.finish();
						String responseXML = utility.getXML();
						_response.getOutputStream().write(responseXML.getBytes());
					} else {
						_response.sendRedirect(unauthAccessURL);

					}
					return;
				}
				if (loginAuthenticationRequired) {
					sessionValid = true;
				} else {
					AccessValidator validator = new AccessValidator();
					try {
						Timestamp systemDateTime = validator.getSystemCurrentDateTime();
						if (systemDateTime != null) {
							session.setAttribute(SessionConstants.SERVER_DATE_TIME, systemDateTime);
							sessionValid = true;
						}
					} catch (Exception e) {
						e.printStackTrace();
					} finally {
						validator.close();
					}
				}
			}
			if (!sessionValid) {
				_response.setHeader("Patterns-Session-Valid",RegularConstants.COLUMN_DISABLE );
				if (ajaxRequest) {
					DHTMLXGridUtility utility = new DHTMLXGridUtility();
					utility.init(true);
					utility.finish();
					String responseXML = utility.getXML();
					_response.getOutputStream().write(responseXML.getBytes());
				} else {
					_response.sendRedirect(unauthAccessURL);

				}
				
				return;
			}
			_response.setHeader("Patterns-Session-Valid",RegularConstants.COLUMN_ENABLE );
			if (operationLogRequired && context.isOperationLogRequired()) {
				BackOfficeOperationLogUtils operationLogUtils = new BackOfficeOperationLogUtils();
				operationLogUtils.logOperation();
			}
			if (allocationRequired) {
				AccessValidator validation = new AccessValidator();
				try {
					// boolean mainOptionAvailable = false;
					// boolean viewOptionAvailable = false;
					boolean isSODDone = validation.isSODDone();
					DTObject result = validation.isOptionAllocatedToRole(programID, roleCode);
					if (result.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
						if (isSODDone) {
							if (result.get("ADD_ALLOWED").equals(RegularConstants.COLUMN_ENABLE)) {
								_request.setAttribute(RequestConstants.ADD_ALLOWED, RegularConstants.COLUMN_ENABLE);
							} else {
								_request.setAttribute(RequestConstants.ADD_ALLOWED, RegularConstants.COLUMN_DISABLE);
							}
							if (result.get("MODIFY_ALLOWED").equals(RegularConstants.COLUMN_ENABLE)) {
								_request.setAttribute(RequestConstants.MODIFY_ALLOWED, RegularConstants.COLUMN_ENABLE);
								_request.setAttribute(RequestConstants.TEMPLATE_ALLOWED, RegularConstants.COLUMN_ENABLE);
							} else {
								_request.setAttribute(RequestConstants.MODIFY_ALLOWED, RegularConstants.COLUMN_DISABLE);
								_request.setAttribute(RequestConstants.TEMPLATE_ALLOWED, RegularConstants.COLUMN_DISABLE);
							}
						} else {
							_request.setAttribute(RequestConstants.ADD_ALLOWED, RegularConstants.COLUMN_DISABLE);
							_request.setAttribute(RequestConstants.MODIFY_ALLOWED, RegularConstants.COLUMN_DISABLE);
							_request.setAttribute(RequestConstants.TEMPLATE_ALLOWED, RegularConstants.COLUMN_DISABLE);
						}
						if (result.get("VIEW_ALLOWED").equals(RegularConstants.COLUMN_ENABLE)) {
							_request.setAttribute(RequestConstants.VIEW_ALLOWED, RegularConstants.COLUMN_ENABLE);
						} else {
							_request.setAttribute(RequestConstants.VIEW_ALLOWED, RegularConstants.COLUMN_DISABLE);
						}
					} else {
						_response.sendRedirect(unauthAccessURL);
						return;
					}
					/*
					 * if (!mainOptionAvailable && !viewOptionAvailable) {
					 * _response.sendRedirect(unauthAccessURL); return; }
					 */
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					validation.close();
				}
			} else {
				String passwordResetContext = (String) session.getAttribute(SessionConstants.PASSWORD_RESET);
				if (passwordResetContext != null && passwordResetContext.equals(RegularConstants.COLUMN_ENABLE)) {
					if (!forcedPasswordRequired) {
						_response.sendRedirect(unauthAccessURL);
						return;
					}
				}
				if (forcedPasswordRequired && (passwordResetContext == null || passwordResetContext.equals(RegularConstants.COLUMN_DISABLE))) {
					_response.sendRedirect(unauthAccessURL);
					return;
				}

				String loginAuthenticationContext = (String) session.getAttribute(SessionConstants.LOGIN_AUTHENTICATION_CONTEXT);
				if (loginAuthenticationContext != null && loginAuthenticationContext.equals(RegularConstants.COLUMN_ENABLE)) {
					if (!loginAuthenticationRequired) {
						_response.sendRedirect(unauthAccessURL);
						return;
					}
				}
				if (loginAuthenticationRequired && (loginAuthenticationContext == null || loginAuthenticationContext.equals(RegularConstants.COLUMN_DISABLE))) {
					_response.sendRedirect(unauthAccessURL);
					return;
				}
			}
			chain.doFilter(_request, _response);
		}
	}
}