/*
 * Copyright (c) 2014, 2015, Patterns Software Design Institute. All
 * rights reserved. Unauthorized copying of this file, via any medium is
 * strictly prohibited. PATTERNS SDI PROPRIETARY/CONFIDENTIAL. Use is
 * subject to license terms.
 */
package patterns.config.framework.web.filters;

import java.io.IOException;
import java.sql.Timestamp;

import javax.servlet.FilterChain;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.loggers.ApplicationLogger;
import patterns.config.framework.monitor.ContextReference;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.thread.ApplicationContext;
import patterns.config.framework.thread.MobileApplicationContextImpl;
import patterns.config.framework.thread.WebContext;
import patterns.config.framework.thread.WebContextImpl;
import patterns.config.framework.web.SessionConstants;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.validations.MiddlewareValidator;

/**
 * The Class MobileApplicationContextFilter set the ApplicationContext for
 * Mobile Application
 */
public class MobileApplicationContextFilter extends CommonFilter {

	/** The logger. */
	private ApplicationLogger logger = null;

	/**
	 * Instantiates a new mobile application context filter.
	 */
	public MobileApplicationContextFilter() {
		super();
		logger = ApplicationLogger.getInstance(this.getClass().getSimpleName());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.patterns.framework.web.filters.CommonFilter#processRequest
	 * (javax.servlet.FilterChain)
	 */
	@Override
	public void processRequest(FilterChain chain) throws IOException, ServletException {
		WebContext webContext = WebContext.getInstance();
		HttpServletRequest _request = webContext.getRequest();
		HttpServletResponse _response = webContext.getResponse();
		HttpSession session = webContext.getSession();
		String URI = _request.getRequestURI();
		logger.logDebug("Form : " + URI);

		java.util.Locale locale = null;
		locale = (java.util.Locale) session.getAttribute(SessionConstants.LOCALE);
		if (locale == null) {
			locale = java.util.Locale.US;
			session.setAttribute(SessionConstants.LOCALE, locale);
		}

		String applicationType = URI.substring(URI.indexOf("/MPBS/") + 6).substring(0, 3);

		MobileApplicationContextImpl applicationContext = new MobileApplicationContextImpl();
		ServletContext servletContext = ContextReference.getContext();
		MiddlewareValidator validator = new MiddlewareValidator();
		java.sql.Date currentBusinessDate = null;
		DTObject mobileContext = new DTObject();
		DTObject sessionDetailsDTO = new DTObject();
		try {
			currentBusinessDate = validator.getCurrentBusinessDate(servletContext.getInitParameter(RegularConstants.INIT_DEPLOYMENT_ENTITY));
			DTObject result = validator.getAppControlParameters(applicationType, servletContext.getInitParameter(RegularConstants.INIT_DEPLOYMENT_ENTITY));
			if (result.get(ContentManager.ERROR) == null) {
				if (result.get(ContentManager.RESULT).equals(ContentManager.DATA_AVAILABLE)) {
					result.remove(ContentManager.RESULT);
					for (String key : result.getKeys()) {
						mobileContext.set(key, result.get(key));
					}
				}
			}
			String sessionID = _request.getParameter("sessionId");
			if (sessionID != null) {
				DTObject sessionInputDTO = new DTObject();
				sessionInputDTO.set("ENTITY_CODE", servletContext.getInitParameter(RegularConstants.INIT_DEPLOYMENT_ENTITY));
				sessionInputDTO.set("APP_ID", result.get("APPL_EXT_APP_CODE"));
				sessionInputDTO.set("SESSION_ID", sessionID);
				sessionDetailsDTO = validator.getSessionDetails(sessionInputDTO);
				if (sessionDetailsDTO.getInternalMap().size() == 0) {
					sessionDetailsDTO.set("USER_ID", "SAURABH");
					sessionDetailsDTO.set("USER_NAME", "SAURABH");
					sessionDetailsDTO.set("BRANCH_CODE", "001");
					sessionDetailsDTO.set("CLUSTER_CODE", "A00");
					sessionDetailsDTO.setObject("LAST_LOGIN_DATETIME", null);
					sessionDetailsDTO.set("FIN_YEAR", "2016");
					sessionDetailsDTO.set("ROLE_CODE", "ADMINM");
					sessionDetailsDTO.set("ROLE_DESC", "ROLE_DESC");
					sessionDetailsDTO.set("DATE_FORMAT", "%d-%m-%Y");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError(e.getLocalizedMessage());
		} finally {
			validator.close();
		}
		applicationContext.setCurrentBusinessDate(currentBusinessDate);
		applicationContext.setEntityCode(servletContext.getInitParameter(RegularConstants.INIT_DEPLOYMENT_ENTITY));
		applicationContext.setPartitionNo(servletContext.getInitParameter(RegularConstants.INIT_DEPLOYMENT_PARTITION));
		applicationContext.setDateFormat(servletContext.getInitParameter(RegularConstants.INIT_MOBILE_DATE_FORMAT));
		// applicationContext.setInterfaceDateFormat(servletContext.getInitParameter(RegularConstants.INIT_INTERFACE_DATE_FORMAT));

		applicationContext.setBranchCode(sessionDetailsDTO.get("BRANCH_CODE"));
		applicationContext.setBranchDescription(sessionDetailsDTO.get("BRANCH_DESC"));
		applicationContext.setUserName(sessionDetailsDTO.get("USER_NAME"));
		applicationContext.setUserID(sessionDetailsDTO.get("USER_ID"));
		applicationContext.setLastLoginDateTime((Timestamp) sessionDetailsDTO.getObject("LAST_LOGIN_DATETIME"));
		applicationContext.setLoginDateTime((Timestamp) sessionDetailsDTO.getObject("LOGIN_DATETIME"));
		applicationContext.setClusterCode(sessionDetailsDTO.get("CLUSTER_CODE"));
		applicationContext.setFinYear(sessionDetailsDTO.get("FIN_YEAR"));
		applicationContext.setRoleCode(sessionDetailsDTO.get("ROLE_CODE"));
		applicationContext.setRoleDescription(sessionDetailsDTO.get("ROLE_DESC"));
		applicationContext.setDateFormat(sessionDetailsDTO.get("DATE_FORMAT"));
		applicationContext.setMobileContextParam(mobileContext);
		Cookie sessionCookie = new Cookie(RegularConstants.SESSIONID_COOKIE, session.getId());
		sessionCookie.setPath("/");
		_response.addCookie(sessionCookie);
		_response.addHeader("Access-Contol-Allow-Origin", "*");
		_response.addHeader("Access-Contol-Allow-Methods", "GET,POST");
		_response.addHeader("Access-Contol-Allow-Credentials", "true");

		applicationContext.init();
		ApplicationContext.setInstance(applicationContext);
		session.setAttribute(SessionConstants.APPLICATION_CONTEXT, applicationContext);
		chain.doFilter(_request, _response);
		applicationContext.destroy();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.patterns.framework.web.filters.CommonFilter#doFilter(javax
	 * .servlet.ServletRequest, javax.servlet.ServletResponse,
	 * javax.servlet.FilterChain)
	 */
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		WebContextImpl webContext = new WebContextImpl();
		HttpServletRequest _request = (HttpServletRequest) request;
		HttpServletResponse _response = (HttpServletResponse) response;
		webContext.setRequest(_request);
		webContext.setResponse(_response);
		webContext.init();
		super.doFilter(_request, _response, chain);
		webContext.destroy();
	}
}
