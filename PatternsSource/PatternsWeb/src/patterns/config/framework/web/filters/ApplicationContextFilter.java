package patterns.config.framework.web.filters;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.FilterChain;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import patterns.config.framework.loggers.ApplicationLogger;
import patterns.config.framework.monitor.ContextReference;
import patterns.config.framework.thread.ApplicationContext;
import patterns.config.framework.thread.WebContext;
import patterns.config.framework.thread.WebContextImpl;
import patterns.config.framework.web.RequestConstants;
import patterns.config.framework.web.SessionConstants;
import patterns.config.framework.web.configuration.program.ProgramConfiguration;
import patterns.config.framework.web.configuration.program.ProgramMap;
import patterns.config.framework.web.utils.ContextUtility;

public class ApplicationContextFilter extends CommonFilter {

	private ApplicationLogger logger = null;

	public ApplicationContextFilter() {
		super();
		logger = ApplicationLogger.getInstance(this.getClass().getSimpleName());
		// logger.logDebug("#()");
	}

	@Override
	public void processRequest(FilterChain chain) throws IOException, ServletException {
		// logger.logDebug("processRequest() BEGIN");

		WebContext webContext = WebContext.getInstance();
		HttpServletRequest _request = webContext.getRequest();
		HttpServletResponse _response = webContext.getResponse();
		HttpSession session = webContext.getSession();
		ServletContext servletContext = ContextReference.getContext();

		// IDENTIFY LOCALE BEGIN
		Locale locale = (Locale) session.getAttribute(patterns.config.framework.web.SessionConstants.LOCALE);
		if (locale == null) {
			locale = java.util.Locale.US;
			session.setAttribute(patterns.config.framework.web.SessionConstants.LOCALE, locale);
		}
		// logger.logInfo("Locale : " + locale);
		// IDENTIFY LOCALE END

		String URI = _request.getRequestURI();
		logger.logDebug("URI : " + URI);

		boolean isWellformedURI = URI.lastIndexOf("/") != -1;
		boolean isJspRequest = URI.lastIndexOf(".jsp") != -1;
		boolean isStrutsRequest = URI.lastIndexOf("/do/") != -1;
		boolean isServletRequest = URI.lastIndexOf("/servlet/") != -1;

		if (isWellformedURI && (isJspRequest || isStrutsRequest || isServletRequest)) {

			// IDENTIFY PROGRAM BEGIN
			String programID;
			int programIdStartIndex = URI.lastIndexOf("/") + 1;
			if (isStrutsRequest) {
				if (URI.contains(";")) {
					int jspIdStartIndex = URI.lastIndexOf(";");
					programID = URI.substring(programIdStartIndex,jspIdStartIndex).toUpperCase(locale);
				} else {
					programID = URI.substring(programIdStartIndex).toUpperCase(locale);
				}
			} else if (isJspRequest) {
				int jspIdStartIndex = URI.lastIndexOf(".jsp");
				programID = URI.substring(programIdStartIndex, jspIdStartIndex).toUpperCase(locale);
			} else if (isServletRequest) {
				programID = URI.substring(programIdStartIndex).toUpperCase(locale);
			} else {
				// logger.logDebug("processRequest() :: Invalid");
				String _path = _request.getContextPath();
				String _basePath = _request.getScheme() + "://" + _request.getServerName() + ":"
						+ _request.getServerPort() + _path + "/";
				String unauthAccessURL = _basePath + "Renderer/common/eunauthaccess.jsp";
				_response.sendRedirect(unauthAccessURL);
				return;
			}
			_request.setAttribute(RequestConstants.PROCESS_ID, programID);
			_request.setAttribute(RequestConstants.PROCESS_ID_LOWER, programID.toLowerCase(locale));
			ProgramConfiguration programCFG = ProgramMap.getInstance().getConfiguration(programID);
			String programDescription = programCFG.getProgramDescription();
			_request.setAttribute(RequestConstants.PROCESS_DESCN, programDescription);
			/* ARIBALA CHANGED ON 16032015 BEG */
			// For setting main program PROGRAM_CLASS
			if (programCFG.isAllocationRequired()) {
				_request.setAttribute(RequestConstants.PROCESS_CLASS, programCFG.getProgramClass());
			}
			/* ARIBALA CHANGED ON 16032015 END */
			logger.logDebug("PROGRAM : " + programID);

			// IDENTIFY PROGRAM END

			// logger.logDebug("Application Context");
			ApplicationContext applicationContext = new ContextUtility().loadApplicationContext(servletContext,
					session, _request, _response, isJspRequest, isStrutsRequest, isServletRequest);
			ApplicationContext.setInstance(applicationContext);
			session.setAttribute(SessionConstants.APPLICATION_CONTEXT, applicationContext);
			chain.doFilter(_request, _response);
			ApplicationContext.destroyInstance();
		}

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException,
			ServletException {
		// logger.logDebug("doFilter()");
		// logger.logDebug("doFilter() :: Instantiate WebContext BEGIN");
		WebContextImpl webContext = new WebContextImpl();
		HttpServletRequest _request = (HttpServletRequest) request;
		HttpServletResponse _response = (HttpServletResponse) response;
		webContext.setRequest(_request);
		webContext.setResponse(_response);
		webContext.init();
		// logger.logDebug("doFilter() :: Instantiate WebContext END");
		super.doFilter(_request, _response, chain);
		webContext.destroy();
	}

}
