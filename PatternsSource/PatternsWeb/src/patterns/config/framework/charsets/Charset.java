package patterns.config.framework.charsets;
//  RTextPrinter
//  Copyright (C)
//
//  Java4Less@Confluencia.net
//  All rights reserved
//
// Adquisition , use and distribution of this code is subject to restriction:
//  - You may modify the source code in order to adapt it to your needs.
//  - Redistribution of this ( or a modified version) source code is prohibited. You may only redistribute compiled versions.
//  - You may redistribute the compiled version as part of your application, not a new java component with the same purpose as this one.
//  - You may not remove this notice from the source code
//  - This notice disclaim all warranties of all material
//  - You may not copy and paste any code into external files.

/**
 * This class stores information about a character set
 */
public class Charset {

  /**
   * name of the set
   */
  public String name="";

  public java.util.Vector aliases=new java.util.Vector();


  /**
   * number of chars in the set
   */
  public int charCount=0;

  /**
   * characters in the set (the mnemonic values)
   */
  public int[] charactersUnicode=new int[512];

  /**
   * codes
   */
  public int[] charactersCodes=new int[512];

  public Charset() {
  }

  /**
   * number of chars in the set
   */
  public int getCharCount() {
    return charCount;
  }

  /**
   * get character at a given possition
   */
  public int getCharAt(int i) {return charactersCodes[i];}



  /**
   * add character to the set
   */
  protected void addChar(int code,int unicode) {
      charactersCodes[charCount]=code;
      charactersUnicode[charCount]=unicode;
      charCount++;
  }

  /**
   * name of the set
   */
  public String getName() {return name;}

  /**
   * name of the set
   */
  protected void setName(String s) {name=s;}

/**
 * get aliases (vector of Strings)
 */
  public java.util.Vector getAlises() { return this.aliases;}

/**
 * add an alias
 */
  protected void addAlias(String alias) {this.aliases.addElement(alias);}

  /**
   * convert an integer to a byte[]
   */
   protected byte[] toByteArray(int i) {
      if (i<=256) {
            byte[] b= new byte[1];
            b[0]=(byte) i;
            return b;
      }
      else {
            byte[] b= new byte[2];
            b[0]=(byte) (i % 256);
            b[1]=(byte) Math.floor(i / 256);
            return b;
      }
   }

  /**
   * convert a byte[] to integer
   */
   protected int fromByteArray(byte[] b) {
      int val=b[0];
      if (b.length==2) val=b[0] + b[1]*256;

      return val;
   }

  /**
   * convert char to unicode
   */
   public byte[] toUnicode(byte[] b)  {
      int val= fromByteArray(b);
      if (val<0) val=256+val;

      for (int i=0;i<this.charCount;i++)
          if (this.charactersCodes[i]==val) return toByteArray(this.charactersUnicode[i]);

       return null;
   }

  /**
   * convert char from unicode to the char set
   */
   public byte[] fromUnicode(byte[] b) {
      int val= fromByteArray(b);
      if (val<0) val=256+val;

      for (int i=0;i<this.charCount;i++)
          if (this.charactersUnicode[i]==val) return toByteArray(this.charactersCodes[i]);

       return null;
   }


   /**
    * print all characters in char set (for debugging purposes)
    */
    public void printCharSet(java.io.PrintStream out) throws Exception{

       out.println("Name: " + this.name);
       for (int i=0;i<this.aliases.size();i++) out.println("Alias: " +  ((String) this.aliases.elementAt(i) ));

       for (int i=0;i<this.charCount;i++) {
           out.println(Integer.toHexString(this.charactersCodes[i]) + "  "+ new String(this.toByteArray(this.charactersUnicode[i])) +" "+ Integer.toHexString(this.charactersUnicode[i]));
       }

    }

    /**
    * export to internal format
    */
    protected void exportCharSet(java.io.PrintWriter out) throws Exception{

       out.println("name: " + this.name);
       for (int i=0;i<this.aliases.size();i++) out.println("pseudo: " +  ((String) this.aliases.elementAt(i) ));

       out.println("table");
       for (int i=0;i<this.charCount;i++) {
           out.println(Integer.toHexString(this.charactersCodes[i]) + " "+ Integer.toHexString(this.charactersUnicode[i]));
       }
      out.println("etable");
      out.println("----------------------------------------");

    }

}