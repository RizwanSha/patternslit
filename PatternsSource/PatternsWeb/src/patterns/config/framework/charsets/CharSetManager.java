package patterns.config.framework.charsets;
//  RTextPrinter
//  Copyright (C)
//
//  Java4Less@Confluencia.net
//  All rights reserved
//
// Adquisition , use and distribution of this code is subject to restriction:
//  - You may modify the source code in order to adapt it to your needs.
//  - Redistribution of this ( or a modified version) source code is prohibited. You may only redistribute compiled versions.
//  - You may redistribute the compiled version as part of your application, not a new java component with the same purpose as this one.
//  - You may not remove this notice from the source code
//  - This notice disclaim all warranties of all material
//  - You may not copy and paste any code into external files.
import java.util.Hashtable;
import java.util.Vector;
import java.net.URL;
import java.io.*;
import java.util.Enumeration;
import java.util.jar.JarFile;
import java.util.jar.JarEntry;
import java.net.JarURLConnection;

/**
 * this class provider method for encoding characters
 */
public class CharSetManager {

  private static CharSetManager defaultInstance=null;
  // sets indexed by name
  private Hashtable setsHash=new Hashtable();


  // all available char sets
  private Vector sets=new Vector();

  /**
   * gets an instance of a manager
   */
  public static CharSetManager getInstance()  {

      try {

      if (defaultInstance==null) defaultInstance=new CharSetManager();

      } catch (Exception e) {
        e.printStackTrace();
      }

      return defaultInstance;
  }


  /**
   * constructor
   */
  protected CharSetManager() throws Exception {
      loadMaps2();
  }

  /**
   * getlist of supported charsets
   */
   public String[] getCharSetsList() {
     String[] list=new String[this.setsHash.size()];
     int i=0;

     Enumeration enumeration=this.setsHash.keys();
     while (enumeration.hasMoreElements())
          list[i++]=((String) enumeration.nextElement());


     return list;
   }

   /**
    * get a given charset
    */
    public Charset getCharSet(String name) {
          if (this.setsHash.containsKey(name)) return (Charset) this.setsHash.get(name);

          return null;
    }

  /**
   * get first word in line
   */
  protected String getFirstWord(String line) {
  int p=0;

  p=line.indexOf(" ");
  if (p>0) return line.substring(0,p);
  else return line;

  }


  /**
   * remove first word in line
   */
  protected String removeFirstWord(String line) {
  int p=0;

  p=line.indexOf(" ");
  if (p>0) return line.substring(p).trim();
  else return "";

  }




  /**
   * load all charsets
   */
  private void loadMaps2() throws Exception  {
      Enumeration enumeration=this.getClass().getClassLoader().getResources("com/java4less/charsets/maps");

      while (enumeration.hasMoreElements()) {
        URL url=(URL) enumeration.nextElement();
        JarURLConnection jarcon=(JarURLConnection) url.openConnection();
        JarFile jar= jarcon.getJarFile();
        Enumeration enum2=jar.entries();
        while (enum2.hasMoreElements()) {
          JarEntry jarEntry=(JarEntry) enum2.nextElement();
          String url2=jarEntry.getName();

          //System.out.println(url2);
          String mapsPackage="com/java4less/charsets/maps/";
          if ((url2.startsWith("com/java4less/charsets/maps/")) && (url2.length()>mapsPackage.length())) {
            String urlStr=url.toString()+"/"+url2.substring(mapsPackage.length());
            //System.out.println(urlStr);
            loadCharSet2(new URL(urlStr));
          }

        }
      }

  }


   private void loadCharSet2(URL fileURL) throws Exception  {
    //URL fileURL=getClass().getResource("/com/java4less/charsets/rfc1345/charsets.txt"));
    BufferedReader bi= new BufferedReader(new InputStreamReader(fileURL.openStream()));
    String line="";
    String val="";
    Charset charset=null;
    boolean done=false;
    boolean gettingChartMap=false;

    while ((line=bi.readLine())!=null) {
        done=false;
        line=line.trim();

       // System.out.println(line);

        if (line.startsWith("name: ")) {
            charset=new Charset();
            line=this.removeFirstWord(line);
            val=this.getFirstWord(line);
            this.setsHash.put(val,charset);
            this.sets.addElement(charset);
            charset.setName(val);
            done=true;
            //System.out.println("Loading " + val);
        }

        if (line.startsWith("pseudo: ")) {
            line=this.removeFirstWord(line);
            val=this.getFirstWord(line);
            this.setsHash.put(val,charset);
            charset.addAlias(val);
            //System.out.println("Alias " + val);
            done=true;
        }

        if (line.startsWith("table")) {
            gettingChartMap=true;
            done=true;
        }

        if (line.startsWith("etable")) {
            gettingChartMap=false;
            done=true;
        }


        if ((!done) && (line.length()>0) && (gettingChartMap)) {
              int code=0;
              int unicode=0;

              val=this.getFirstWord(line);
              code=Integer.parseInt(val,16);

              line=this.removeFirstWord(line);
              val=this.getFirstWord(line);
              unicode=Integer.parseInt(val,16);

              charset.addChar(code,unicode);
        } // if

    } // while


    bi.close();

  }


}