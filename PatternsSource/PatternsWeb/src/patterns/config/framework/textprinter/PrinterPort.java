package patterns.config.framework.textprinter;
//  RTextPrinter
//  Copyright (C)
//
//  Java4Less@Confluencia.net
//  All rights reserved
//
// Adquisition , use and distribution of this code is subject to restriction:
//  - You may modify the source code in order to adapt it to your needs.
//  - Redistribution of this ( or a modified version) source code is prohibited. You may only redistribute compiled versions.
//  - You may redistribute the compiled version as part of your application, not a new java component with the same purpose as this one.
//  - You may not remove this notice from the source code
//  - This notice disclaim all warranties of all material
//  - You may not copy and paste any code into external files.


public abstract class PrinterPort {


  /**
  * time (milliseconds) to wait for the bytes to be sent to the printer
  */
  protected int timeout=2000; // milliseconds

  /**
  * time (milliseconds) to wait for the bytes to be sent to the printer
  */
  public void setTimeout(int i) {
    timeout=i;
  }

  /**
  * time (milliseconds) to wait for the bytes to be sent to the printer
  */
  public int getTimeout() {
    return timeout;
  }

 /**
  * opens port
  */
  public void open()  throws TextPrinterException  {

  }

  /**
   * closes port
   */
  public void close()  throws TextPrinterException  {

  }

  /**
   * Writes an array of bytes to the port
   */
  public void write(byte[] s,int len)  throws TextPrinterException  {
  }

  /**
   * Writes an array of bytes to the port
   */
  public void write(byte[] s)  throws TextPrinterException  {
        write(s,s.length);
  }

   /**
   * Writes a string to the port
   */
  public void write(String s)  throws TextPrinterException  {
  }

  /**
   * is printer online? returns null if the information is not known.
   */
   public Boolean isOnline() {
    return null;
  }

  /**
   * is printer in Error? returns null if the information is not known.
   */
   public Boolean isInError() {
        return null;
  }

  /**
   * is paper out? returns null if the information is not known.
   */
   public Boolean isPaperOut() {
       return null;
  }

  /**
   * is the printer busy? returns null if the information is not known.
   */
   public Boolean isBusy() {
       return null;
  }

}