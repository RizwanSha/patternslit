package patterns.config.framework.textprinter;
//  RTextPrinter
//  Copyright (C)
//
//  Java4Less@Confluencia.net
//  All rights reserved
//
// Adquisition , use and distribution of this code is subject to restriction:
//  - You may modify the source code in order to adapt it to your needs.
//  - Redistribution of this ( or a modified version) source code is prohibited. You may only redistribute compiled versions.
//  - You may redistribute the compiled version as part of your application, not a new java component with the same purpose as this one.
//  - You may not remove this notice from the source code
//  - This notice disclaim all warranties of all material
//  - You may not copy and paste any code into external files.

/**
 * size of the paper to be used
 */
public class PaperSize {

  public static  PaperSize A4=new PaperSize("A4",210,297,false); // 8.2 - 11.6
  public static  PaperSize LETTER=new PaperSize("LETTER",8.5,11,true);
  public static  PaperSize EXECUTIVE=new PaperSize("EXECUTIVE",7.25,10.5,true);
  public static  PaperSize LEGAL=new PaperSize("LEGAL",8.5,15,true);
  public static  PaperSize A3=new PaperSize("A3",297,420,false);
  public static  PaperSize LEDGER=new PaperSize("LEDGER",11,17,true);
  /**
   * page width
   */
   private double width=0;

  /**
   * page height
   */
   private double height=0;

  private String name="";

  /**
   * unit is inches, if false we use millimiters
   */
   private boolean inches=false;

  public PaperSize(String sName ,double w,double h, boolean inch) {
      this.inches=inch;
      this.width=w;
      this.height=h;
      this.name=sName;
  }

  /**
   * get width in  inches
   */
   public double getWidth() {
      if (this.inches) return this.width;

      return this.width/25.6;
   }

  /**
   * get height in  inches
   */
   public double getHeight() {
      if (this.inches) return this.height;

      return this.height/25.6;
   }

  /**
   * get name of the paper size
   */
   public String getName() {  return this.name;}

}