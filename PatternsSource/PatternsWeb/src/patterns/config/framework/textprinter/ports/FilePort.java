package patterns.config.framework.textprinter.ports;
//  RTextPrinter
//  Copyright (C)
//
//  Java4Less@Confluencia.net
//  All rights reserved
//
// Adquisition , use and distribution of this code is subject to restriction:
//  - You may modify the source code in order to adapt it to your needs.
//  - Redistribution of this ( or a modified version) source code is prohibited. You may only redistribute compiled versions.
//  - You may redistribute the compiled version as part of your application, not a new java component with the same purpose as this one.
//  - You may not remove this notice from the source code
//  - This notice disclaim all warranties of all material
//  - You may not copy and paste any code into external files.
import java.io.FileOutputStream;

import patterns.config.framework.textprinter.PrinterPort;
import patterns.config.framework.textprinter.TextPrinterException;
import patterns.config.framework.textprinter.exceptions.CouldNotOpenPrinterException;


/**
 * Class used to access the printer using a file. For example, in windows you can use:<BR>
 * - "PRN:" for a local printer<BR>
 * - "//server/printername:" for a network printer<BR>
 * - "/dev/lpr": for Linux<BR>
 */
public class FilePort extends PrinterPort implements Runnable {

  String sFilename="";
  FileOutputStream os=null;
  boolean closed=true;

  // used as response from the writerThread
  String writeError=null;
  // input data for the writerThread
  byte[] dataToWrite=null;
  int dataToWriteLen=0;

  public FilePort(String filename) {
    sFilename=filename;
  }


 /**
  * opens port
  */
  public void open() throws TextPrinterException {
   try {
    os=new FileOutputStream(sFilename);
    closed=false;

    } catch (Exception e) {
        CouldNotOpenPrinterException t=new CouldNotOpenPrinterException(e.getMessage());
        throw t;
    }
  }

  /**
   * closes port
   */
  public void close()  throws TextPrinterException {


    try {
      closed=true;
      os.close();
      //writeThread.join(); // wait for writerThread to finish
    } catch (Exception e) {
        TextPrinterException t=new TextPrinterException(e.getMessage());
        throw t;
    }

  }

     /**
   * Writes a string to the port
   */
  public void write(String s)  throws TextPrinterException  {
     this.write(s.getBytes());
  }

  /**
   * Writes an array of bytes to the port
   */
  public void write(byte[] s,int len) throws TextPrinterException {
    try {
        this.dataToWrite=s;
        this.dataToWriteLen=len;

        this.writeError=null;
        Thread writerThread=null;
        writerThread=new Thread(this);
        writerThread.start();
        writerThread.join(this.timeout); // wait for write operation to finish

        // did not finish it work
        if (writerThread.isAlive()) {
                    writerThread.stop();
                    TextPrinterException t=new TextPrinterException("Could not write to printer file. Timeout.");
                    throw t;
        }

        // finished with an exception
        if (this.writeError!=null) {
                    TextPrinterException t=new TextPrinterException("Could not write to printer file. " + this.writeError);
                    throw t;
        }

        // wait for data to be written
   /*     long startTime=System.currentTimeMillis();

        while (this.dataToWrite!=null) {
            Thread.sleep(100);

            if ((System.currentTimeMillis()-startTime) > this.timeout) {
                    TextPrinterException t=new TextPrinterException("Could not write to printer file. Time out.");
                    throw t;
            }
        }*/

    } catch (Exception e) {
        TextPrinterException t=new TextPrinterException(e.getMessage());
        throw t;
    }
  }

  /**
   * start writer thread
   */
   public void run() {

     try {
         writeError=null;
         os.write(this.dataToWrite,0,this.dataToWriteLen);

     } catch (Exception e1) {
          this.writeError=e1.getMessage();
     }

     /*
      while (!this.closed) {
          try {
          Thread.sleep(1000); // 1 second
          } catch (Exception e) {e.printStackTrace();}

          // write data if we have any
          if (this.dataToWrite!=null) {
              os.write(this.dataToWrite);
              this.dataToWrite=null;
          }

      }

      } catch (Exception e1) {e1.printStackTrace();}
    */

   }

}