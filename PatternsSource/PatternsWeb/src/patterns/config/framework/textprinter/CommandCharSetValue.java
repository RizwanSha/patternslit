package patterns.config.framework.textprinter;
//  RTextPrinter
//  Copyright (C)
//
//  Java4Less@Confluencia.net
//  All rights reserved
//
// Adquisition , use and distribution of this code is subject to restriction:
//  - You may modify the source code in order to adapt it to your needs.
//  - Redistribution of this ( or a modified version) source code is prohibited. You may only redistribute compiled versions.
//  - You may redistribute the compiled version as part of your application, not a new java component with the same purpose as this one.
//  - You may not remove this notice from the source code
//  - This notice disclaim all warranties of all material
//  - You may not copy and paste any code into external files.

/**
 * this class stores information about a character set value used in the CMD_SELECT_CHAR_SET command
 */
public class CommandCharSetValue extends CommandParamValue {

  /**
   * name of the character set as recognized by RTextPrinter
   */
  public String charSet="";

  public CommandCharSetValue(String n , String v,String c) {
      super(n,v);
      charSet=c;
  }
}