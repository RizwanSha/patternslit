package patterns.config.framework.textprinter.printers;

import patterns.config.framework.textprinter.Command;

//  RTextPrinter
//  Copyright (C)
//
//  Java4Less@Confluencia.net
//  All rights reserved
//
// Adquisition , use and distribution of this code is subject to restriction:
//  - You may modify the source code in order to adapt it to your needs.
//  - Redistribution of this ( or a modified version) source code is prohibited. You may only redistribute compiled versions.
//  - You may redistribute the compiled version as part of your application, not a new java component with the same purpose as this one.
//  - You may not remove this notice from the source code
//  - This notice disclaim all warranties of all material
//  - You may not copy and paste any code into external files.



public class ProPrinterIII extends ProPrinterXL {

  public ProPrinterIII() {
      super();

      setCommand(new Command(CMD_ITALIC_ON,"Esc%G"));
      setCommand(new Command(CMD_ITALIC_OFF,"Esc%H"));

      super.clearSpacing();
      super.addSpacing("8","Esc0");
      super.addSpacing("12","EscA6");
      super.addSpacing("18","EscA4");
      super.addSpacing("24","EscA3");
      super.addSpacing("72","EscA#");

      super.clearCharSet();

  }
}