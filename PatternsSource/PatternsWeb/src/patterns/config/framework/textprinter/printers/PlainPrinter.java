package patterns.config.framework.textprinter.printers;

import patterns.config.framework.textprinter.Command;
import patterns.config.framework.textprinter.TextPrinter;

//  RTextPrinter
//  Copyright (C)
//
//  Java4Less@Confluencia.net
//  All rights reserved
//
// Adquisition , use and distribution of this code is subject to restriction:
//  - You may modify the source code in order to adapt it to your needs.
//  - Redistribution of this ( or a modified version) source code is prohibited. You may only redistribute compiled versions.
//  - You may redistribute the compiled version as part of your application, not a new java component with the same purpose as this one.
//  - You may not remove this notice from the source code
//  - This notice disclaim all warranties of all material
//  - You may not copy and paste any code into external files.

public class PlainPrinter extends TextPrinter {

  public PlainPrinter() {


          super();
          setCommand(new Command(CMD_RESET,""));
          setCommand(new Command(CMD_BOLD_ON,""));
          setCommand(new Command(CMD_BOLD_OFF,""));

          setCommand(new Command(CMD_ITALIC_ON,""));
          setCommand(new Command(CMD_ITALIC_OFF,""));

          setCommand(new Command(CMD_DOUBLESTRIKE_ON,""));
          setCommand(new Command(CMD_DOUBLESTRIKE_OFF,""));

          setCommand(new Command(CMD_UNDERLINED_ON,""));
          setCommand(new Command(CMD_UNDERLINED_OFF,""));

          setCommand(new Command(CMD_DOUBLEWIDE_ON,""));
          setCommand(new Command(CMD_DOUBLEWIDE_OFF,""));

          setCommand(new Command(CMD_CONDENSED_ON,""));
          setCommand(new Command(CMD_CONDENSED_OFF,""));

          setCommand(new Command(CMD_SUBSCRIPT_ON,""));
          setCommand(new Command(CMD_SUBSCRIPT_OFF,""));

          setCommand(new Command(CMD_SUPERSCRIPT_ON,""));
          setCommand(new Command(CMD_SUPERSCRIPT_OFF,""));

          setCommand(new Command(CMD_PAGE_LENGTH_LINES,""));
          setCommand(new Command(CMD_PAGE_WIDTH,""));

          setCommand(new Command(CMD_TOP_MARGIN,""));
          setCommand(new Command(CMD_BOTTOM_MARGIN,""));
          setCommand(new Command(CMD_LEFT_MARGIN,""));
          setCommand(new Command(CMD_RIGHT_MARGIN,""));

          setCommand(new Command(CMD_SELECT_FONT,""));
          setCommand(new Command(CMD_SELECT_CHAR_SET,""));
          setCommand(new Command(CMD_PITCH,""));
          setCommand(new Command(CMD_PROPORTIONAL_ON,""));
          setCommand(new Command(CMD_PROPORTIONAL_OFF,""));

          setCommand(new Command(CMD_PORTRAIT,""));
          setCommand(new Command(CMD_LANDSCAPE,""));
          setCommand(new Command(CMD_INTERSPACE,""));
          setCommand(new Command(CMD_QUALITY,""));
          setCommand(new Command(CMD_DRAFT,""));
  }
}