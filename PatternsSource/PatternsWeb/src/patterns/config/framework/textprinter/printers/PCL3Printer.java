package patterns.config.framework.textprinter.printers;

import patterns.config.framework.textprinter.Command;
import patterns.config.framework.textprinter.PaperSize;
import patterns.config.framework.textprinter.TextPrinter;

//  RTextPrinter
//  Copyright (C)
//
//  Java4Less@Confluencia.net
//  All rights reserved
//
// Adquisition , use and distribution of this code is subject to restriction:
//  - You may modify the source code in order to adapt it to your needs.
//  - Redistribution of this ( or a modified version) source code is prohibited. You may only redistribute compiled versions.
//  - You may redistribute the compiled version as part of your application, not a new java component with the same purpose as this one.
//  - You may not remove this notice from the source code
//  - This notice disclaim all warranties of all material
//  - You may not copy and paste any code into external files.


/**
 * commands HP laser printers supporting the PCL 3 language
 */
public class PCL3Printer extends TextPrinter {

  public PCL3Printer() {

          super();

          hLineChar='\u2500'; // U2500 BOX DRAWINGS LIGHT HORIZONTAL
          vLineChar='\u2502';  // U2502 BOX DRAWINGS LIGHT VERTICAL
          tlCornerChar='\u250C'; // U250C BOX DRAWINGS LIGHT DOWN AND RIGHT
          trCornerChar='\u2510'; // U2510 BOX DRAWINGS LIGHT DOWN AND LEFT
          blCornerChar='\u2514'; // U2514 BOX DRAWINGS LIGHT UP AND RIGHT
          brCornerChar='\u2518'; // U2518 BOX DRAWINGS LIGHT UP AND LEFT
          vrLineChar='\u251C'; // U252C BOX DRAWINGS LIGHT VERTICAL AND RIGHT
          vlLineChar='\u2524'; // U2524 BOX DRAWINGS LIGHT VERTICAL AND LEFT
          htLineChar='\u2534'; // U2534 BOX DRAWINGS LIGHT UP AND HORIZONTAL
          hbLineChar='\u252C'; // U252C BOX DRAWINGS LIGHT DOWN AND HORIZONTAL
          crossLineChar='\u253C';  // U253C BOX DRAWINGS LIGHT VERTICAL AND HORIZONTAL
          linesCharSet="PC-8";


          setCommand(new Command(CMD_RESET,""+ESC+"E"));
          setCommand(new Command(CMD_BOLD_ON,""+ESC+"(s3B"));
          setCommand(new Command(CMD_BOLD_OFF,""+ESC+"(s0B"));

          setCommand(new Command(CMD_ITALIC_ON,""+ESC+"(s1S"));
          setCommand(new Command(CMD_ITALIC_OFF,""+ESC+"(s0S"));

          setCommand(new Command(CMD_DOUBLESTRIKE_ON,""));
          setCommand(new Command(CMD_DOUBLESTRIKE_OFF,""));

          setCommand(new Command(CMD_UNDERLINED_ON,""+ESC+"&d0D"));
          setCommand(new Command(CMD_UNDERLINED_OFF,""+ESC+"&d@"));

          setCommand(new Command(CMD_DOUBLEWIDE_ON,""));
          setCommand(new Command(CMD_DOUBLEWIDE_OFF,""));

          setCommand(new Command(CMD_CONDENSED_ON,""));
          setCommand(new Command(CMD_CONDENSED_OFF,""));

          setCommand(new Command(CMD_SUBSCRIPT_ON,"Esc(s-1U"));
          setCommand(new Command(CMD_SUBSCRIPT_OFF,"Esc(s0U"));

          setCommand(new Command(CMD_SUPERSCRIPT_ON,"Esc(s1U"));
          setCommand(new Command(CMD_SUPERSCRIPT_OFF,"Esc(s0U"));

          /*
          The value field (#) sets the text length in lines referenced from the top margin.
          The user default text length is invoked whenever the orientation, page
          length, page size, or top margin is changed. The user default text length is computed as follows:   Text length= logical page in inches - (top margin in inches + 0.5 inches)       */
          setCommand(new Command(CMD_PAGE_LENGTH_LINES,"")); // ""+ESC+"&l#F" number of lines from top margin or ""+ESC+"&l#P"
          /**
           * When fixed pitch fonts are selected, all printable characters including
          the Space and Backspace characters are affected by HMI. When
          proportional fonts are selected, the HMI affects only the Space control
          code character.
          HMI is reset to match the new font when any of the font
          characteristics are changed and when switching between primary
          and secondary fonts with Shift In and Shift Out.
          HMI is equal to the pitch value in the font header. The factory default
          fonts HMI is 12 (12/120 = 1/10 inch per character, or 10 characters
          per inch).
           */
          setCommand(new Command(CMD_HMI,"Esc&k#H")); // calculate HMI based on number of columns, page size and resoltuion

          // Receipt of a Top Margin command resets the text length according to the following equation: Text length= logical page in inches - (top margin in inches + 0.5 inches)
          setCommand(new Command(CMD_TOP_MARGIN,""+ESC+"&l#E")); // default 1/2 inch down from top of logical page1, this command reset the page length
          setCommand(new Command(CMD_BOTTOM_MARGIN,""));
          setCommand(new Command(CMD_LEFT_MARGIN,""+ESC+"&a#L")); // default left maring is 0
          setCommand(new Command(CMD_RIGHT_MARGIN,""+ESC+"&a#M")); //  default is logical page right bound

          setCommand(new Command(CMD_SELECT_FONT,"#"));
          setCommand(new Command(CMD_SELECT_CHAR_SET,"#"));
          /**
          The Pitch command designates the horizontal spacing of a
          fixed- spaced (bitmap or scalable) font in terms of the number of
          characters per inch. This characteristic is ignored when selecting a
          proportionally-spaced (bitmap or scalable) font,
           */
          setCommand(new Command(CMD_PITCH,"#")); //
          setCommand(new Command(CMD_PROPORTIONAL_ON,"Esc(s1P"));
          setCommand(new Command(CMD_PROPORTIONAL_OFF,"Esc(s0P"));

          setCommand(new Command(CMD_PORTRAIT,""));//parseCommand("Esc&l0O");
          setCommand(new Command(CMD_LANDSCAPE,"Esc&l1O"));
          setCommand(new Command(CMD_INTERSPACE,"#"));  // ""+ESC+"&l#D ? // default is 6 lines/inch. The following equation converts lines-per-inch spacing to VMI: VMI= 48 * (1/linesperinch)
          setCommand(new Command(CMD_QUALITY,"Esc(s1Q"));
          setCommand(new Command(CMD_DRAFT,"Esc(s2Q"));
          setCommand(new Command(CMD_PAPER_SIZE,"Esc&l#A"));
          setCommand(new Command(CMD_RESOLUTION,""));
          setCommand(new Command(CMD_HORIZONTAL_POSITIONING,"Esc&a#C")); // position cursor at a given column. The width of a column is defined by the current Horizontal Motion Index. This sequence ignores margins and can therefore be used to set the current active position (CAP) to any location along the current line.

          addDependency(CMD_SELECT_FONT,CMD_HMI);
          addDependency(CMD_PITCH,CMD_HMI);
          addDependency(CMD_PROPORTIONAL_ON,CMD_HMI);
          addDependency(CMD_PROPORTIONAL_OFF,CMD_HMI);
          addDependency(CMD_CONDENSED_ON,CMD_HMI);
          addDependency(CMD_CONDENSED_OFF,CMD_HMI);
          addDependency(CMD_DOUBLEWIDE_ON,CMD_HMI);
          addDependency(CMD_DOUBLEWIDE_OFF,CMD_HMI);

          super.addFont("Univers","Esc(s4148T");//,"true","false"}, // the third element indicates if this is a proportional font and the forth if it is a fixed font
          super.addFont("LinePrinter","Esc(s0T");//,"false","true"}, // fixed
          super.addFont("CG Times","Esc(s4101T");//,"true","false"},
          super.addFont("Courier","Esc(s4099T");//,"false","true"}, // fixed
          super.addFont("Albertus","Esc(s4362T");//,"true","false"},
          super.addFont("Antique Olive","Esc(s4168T");//,"true","false"},
          super.addFont("Clarendon","Esc(s4140T");//,"true","false"},
          super.addFont("Coronet","Esc(s4116T");//,"true","false"},
          super.addFont("Garamond Antiqua","Esc(s4197T");//,"true","false"},
          super.addFont("Letter Gothic","Esc(s4102T");//,"false","true"}, // fixed
          super.addFont("Marigold","Esc(s4297T");//,"true","false"},
          super.addFont("CG Omega","Esc(s4113T");//,"true","false"},
          super.addFont("Arial","Esc(s16602T");//,"true","false"},
          super.addFont("Times New Roman","Esc(s16901T");//,"true","false"},
          super.addFont("Symbol","Esc(s16686T");//,"true","false"},
          super.addFont("Wingdings","Esc(s31402T");//,"true","false"}

          super.addPitch("10","Esc(s10H");
          super.addPitch("12","Esc(s12H");

          super.addSpacing("1","Esc&l1D");
          super.addSpacing("2","Esc&l2D");
          super.addSpacing("3","Esc&l3D");
          super.addSpacing("4","Esc&l4D");
          super.addSpacing("6","Esc&l6D");
          super.addSpacing("8","Esc&l8D");
          super.addSpacing("12","Esc&l12D");
          super.addSpacing("16","Esc&l16D");
          super.addSpacing("24","Esc&l24D");
          super.addSpacing("48","Esc&l48D");

          super.addPaperSize("EXECUTIVE","1");
          super.addPaperSize("LETTER","2");
          super.addPaperSize("LEGAL","3");
          super.addPaperSize("LEDGER","6");
          super.addPaperSize("A4","26");
          super.addPaperSize("A3","27");


/**
 * The HP LaserJet printers provide several 7-bit ISO (International
Organization for Standardization) sets to
support European languages. Each ISO symbol set is a unique
ordering of symbols contained within the Roman-8 symbol set. The printer
automatically generates the requested ISO font from an HP
Roman-8 font.
 */
          super.addCharSet("ISO-IR-60","Esc(0D","Norwegian 1");
          super.addCharSet("ISO-IR-61","Esc(1D","Norwegian 2");
          super.addCharSet("ISO-IR-4","Esc(1E","United Kingdom");
          super.addCharSet("ISO-IR-69","Esc(1F","French");
          super.addCharSet("ISO-IR-21","Esc(1G","German");
          super.addCharSet("ISO-IR-15","Esc(0I","Italian");
          super.addCharSet("ISO-IR-14","Esc(0K","JIS ASCII");
          super.addCharSet("ISO-IR-57","Esc(2K","Chinese");
          super.addCharSet("ISO-8859-1","Esc(0N","Latin 1");
          super.addCharSet("ISO-IR-11","Esc(0S","Swedish");
          super.addCharSet("ISO-IR-17","Esc(2S","Spanish 1");
          super.addCharSet("ISO-IR-10","Esc(3S","Swedish 2");
          super.addCharSet("ISO-IR-16","Esc(4S","Portuguese 1");
          super.addCharSet("IISO-IR-84","Esc(5S","Portuguese 2");
          super.addCharSet("ISO-IR-85","Esc(6S","Spanish 2");
          super.addCharSet("ISO-IR-6","Esc(0U","ASCII");
          super.addCharSet("ISO-IR-2","Esc(2U","IRV");
          super.addCharSet("ROMAN8","Esc(8U","Roman8"); // DEFAULT
          super.addCharSet("Microsoft Publishing","Esc(6J","");
          super.addCharSet("Desktop","Esc(7J","Desktop");
          super.addCharSet("PS Text","Esc(10J","PS Text");
          super.addCharSet("Ventura International","Esc(13J","");
          super.addCharSet("Ventura US","Esc(14J","");
          super.addCharSet("Ventura ITC Zapf Dingbatsdingbats","Esc(9L","");
          super.addCharSet("PS ITC Zapf Dingbatsdingbats","Esc(10L","");
          super.addCharSet("ITC Zapf Dingbatsdingbats Series 100","Esc(11L","");
          super.addCharSet("ITC Zapf Dingbatsdingbats Series 200","Esc(12L","");
          super.addCharSet("ITC Zapf Dingbatsdingbats Series 300","Esc(13L","");
          super.addCharSet("PS Math","Esc(5M","");
          super.addCharSet("Ventura","Math Esc(6M","");
          super.addCharSet("Math-8","Esc(8M","");
          super.addCharSet("Legal","Esc(1U","");
          super.addCharSet("1252","Esc(9U","Windows");
          super.addCharSet("Pi Font","Esc(15U","");
          super.addCharSet("PC-8","Esc(10U","437"); // can be used to paint lines
          super.addCharSet("PC-8 D/N","Esc(11U","");
          super.addCharSet("850","Esc(12U","PC-850"); // PC 850 contains line-draw characters

          super.defaultCharSet="ROMAN8";


  }

   /**
   * set line spacing to get desired number of lines in paper
   */
   protected void calculateLineSpacing(int rows) {
      double logicalHeight=0;

      // get current pager size
      if (this.jobSetup.paperSize==null) return;
      if ((this.jobSetup.resolution!=300) &&  (this.jobSetup.resolution!=600)) return;

      // get logical width
      logicalHeight=this.jobSetup.paperSize.getHeight();

      // set required line spacing
      if (this.jobSetup.interspacing.length()==0)  this.jobSetup.interspacing=this.getBestLineSpacing(rows,logicalHeight);
      //this.jobSetup.interspacing=(int) (1/(logicalHeight/rows));

      if (debug) {
        System.out.println("Settings for " + rows + " lines per page and paper height " + this.jobSetup.paperSize.getHeight()+ " inches:");
        System.out.println("Line spacing: "+this.jobSetup.interspacing);
      }


   }


  /**
   * set pitch and HMI to get a number of columns
   */
   protected void calculateColumnWidth(int cols) {
      int logicalWidth=0;

      // get current pager size
      if (this.jobSetup.paperSize==null) return;
      if ((this.jobSetup.resolution!=300) &&  (this.jobSetup.resolution!=600)) return;

      // get logical width
      if (this.jobSetup.paperSize==PaperSize.LETTER)  logicalWidth=2400;
      if (this.jobSetup.paperSize==PaperSize.A4)  logicalWidth=2338;
      if (this.jobSetup.paperSize==PaperSize.A3)  logicalWidth=3365;
      if (this.jobSetup.paperSize==PaperSize.LEDGER)  logicalWidth=3150;
      if (this.jobSetup.paperSize==PaperSize.LEGAL)  logicalWidth=2400;
      if (this.jobSetup.paperSize==PaperSize.EXECUTIVE)  logicalWidth=2025;

      if (this.jobSetup.resolution==600) logicalWidth=logicalWidth*2;

      // set required pitch
      double inches=logicalWidth/this.jobSetup.resolution;
      double requiredPitch=(1/(inches/cols));

      // set required HMI
      double requiredHMI=120/requiredPitch;

      if (this.jobSetup.pitch==-1) this.jobSetup.pitch=Math.floor(requiredPitch*100)/100;
      if (this.jobSetup.columnWidth==-1) this.jobSetup.columnWidth=Math.floor(requiredHMI*100)/100;

      if (debug) {
        System.out.println("Settings for " + cols + " columns and paper width " + this.jobSetup.paperSize.getWidth()+ " inches:");
        System.out.println("Pitch: "+this.jobSetup.pitch);
        System.out.println("HMI: "+this.jobSetup.columnWidth);
      }

   }

}