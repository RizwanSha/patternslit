package patterns.config.framework.textprinter.printers;

import patterns.config.framework.textprinter.Command;
import patterns.config.framework.textprinter.TextPrinter;
import patterns.config.framework.textprinter.TextPrinterException;

//  RTextPrinter
//  Copyright (C)
//
//  Java4Less@Confluencia.net
//  All rights reserved
//
// Adquisition , use and distribution of this code is subject to restriction:
//  - You may modify the source code in order to adapt it to your needs.
//  - Redistribution of this ( or a modified version) source code is prohibited. You may only redistribute compiled versions.
//  - You may redistribute the compiled version as part of your application, not a new java component with the same purpose as this one.
//  - You may not remove this notice from the source code
//  - This notice disclaim all warranties of all material
//  - You may not copy and paste any code into external files.



/**
 * commands for OKI Microline emulation
 */
public class OKIMicroline extends TextPrinter {

  public OKIMicroline() {

          super();

          setCommand(new Command(CMD_RESET,"Esc"+((char) 24)));
          setCommand(new Command(CMD_BOLD_ON,"EscT"));
          setCommand(new Command(CMD_BOLD_OFF,"EscI"));

          setCommand(new Command(CMD_ITALIC_ON,"Esc!/"));
          setCommand(new Command(CMD_ITALIC_OFF,"Esc!*"));

          setCommand(new Command(CMD_DOUBLESTRIKE_ON,""));
          setCommand(new Command(CMD_DOUBLESTRIKE_OFF,""));

          setCommand(new Command(CMD_UNDERLINED_ON,"EscC"));
          setCommand(new Command(CMD_UNDERLINED_OFF,"EscD"));

          setCommand(new Command(CMD_DOUBLEWIDE_ON,""));
          setCommand(new Command(CMD_DOUBLEWIDE_OFF,""));

          setCommand(new Command(CMD_CONDENSED_ON,""));
          setCommand(new Command(CMD_CONDENSED_OFF,""));

          setCommand(new Command(CMD_SUBSCRIPT_ON,"EscL"));
          setCommand(new Command(CMD_SUBSCRIPT_OFF,"EscM"));

          setCommand(new Command(CMD_SUPERSCRIPT_ON,"EscJ"));
          setCommand(new Command(CMD_SUPERSCRIPT_OFF,"EscK"));


          setCommand(new Command(CMD_PAGE_LENGTH_LINES,"EscF#"));
          setCommand(new Command(CMD_PAGE_LENGTH_INCHES,"EscG"));
          setCommand(new Command(CMD_PAGE_WIDTH,""));

          // this value will be cancelled by the page length command so leave it empty
          setCommand(new Command(CMD_TOP_MARGIN,""));
          setCommand(new Command(CMD_BOTTOM_MARGIN,""));
          setCommand(new Command(CMD_LEFT_MARGIN,""));

          setCommand(new Command(CMD_RIGHT_MARGIN,""));

          setCommand(new Command(CMD_SELECT_FONT,"#"));
          setCommand(new Command(CMD_SELECT_CHAR_SET,"#"));
          setCommand(new Command(CMD_PITCH,"#"));
          setCommand(new Command(CMD_PROPORTIONAL_ON,"EscY"));
          setCommand(new Command(CMD_PROPORTIONAL_OFF,"EscZ"));

          setCommand(new Command(CMD_PORTRAIT,""));
          setCommand(new Command(CMD_LANDSCAPE,""));
          setCommand(new Command(CMD_INTERSPACE,""));
          setCommand(new Command(CMD_QUALITY,""));
          setCommand(new Command(CMD_DRAFT,""));

          setCommand(new Command(CMD_HMI,""));

          setCommand(new Command(CMD_HORIZONTAL_POSITIONING,""));

          super.addCharSet("Standard","Esc!0","Standard");
          super.addCharSet("BlockGraphics","Esc!1","BlockGraphics");
          super.addCharSet("LineGraphics","Esc!2","LineGraphics");
          super.addCharSet("Publisher","Esc!Z","Publisher");

          super.addCharSet("ISO-IR-60","Esc!"+((char) 0x47),"Norwegian 1");
          super.addCharSet("ISO-IR-4","Esc!"+((char) 0x42),"United Kingdom");
          super.addCharSet("ISO-IR-69","Esc!"+((char) 0x44),"French");
          super.addCharSet("ISO-IR-21","Esc!"+((char) 0x43),"German");
          super.addCharSet("ISO-IR-15","Esc!"+((char) 0x49),"Italian");
          super.addCharSet("ISO-8859-1","Esc!"+((char) 0x4C),"Latin 1");
          super.addCharSet("ISO-IR-11","Esc!"+((char) 0x45),"Swedish");
          super.addCharSet("ISO-IR-17","Esc!"+((char) 0x4B),"Spanish 1");
          super.addCharSet("ISO-IR-6","Esc!"+((char) 0x40),"ASCII");
          super.addCharSet("Microsoft Publishing","Esc!"+((char) 0x5A),"");
          super.addCharSet("Denmark","Esc!"+((char) 0x46),"Denmark");


          super.addFont("Courier","Esc1");
          super.addFont("Gothic","Esc3");


          super.addPitch("10",""+((char) 30));
          super.addPitch("12",""+((char) 28));
          super.addPitch("15","Escg");
          super.addPitch("17.1",""+((char) 29));
          super.addPitch("20","Esc#3");

          super.addSpacing("6","Esc6");
          super.addSpacing("8","Esc8");
          super.addSpacing("144","Esc%9#"); // n/144

          super.defaultCharSet="ISO-IR-6";

  }


   /**
  * execute command with 1 parameter
  */
  protected void executeCommand(String name,String param) throws TextPrinterException {

      if (name.equals(CMD_PAGE_LENGTH_INCHES)) {
            byte[] bytes=getLowHeightBytes(jobSetup.paperSize.getHeight());
            this.executeCommandInternal(CMD_PAGE_LENGTH_INCHES);
            addToBuffer(bytes);
      }
      else super.executeCommand(name,param);

  }

}