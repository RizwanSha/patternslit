package patterns.config.framework.textprinter.printers;

import patterns.config.framework.textprinter.Command;
import patterns.config.framework.textprinter.TextPrinter;
//  RTextPrinter
//  Copyright (C)
//
//  Java4Less@Confluencia.net
//  All rights reserved
//
// Adquisition , use and distribution of this code is subject to restriction:
//  - You may modify the source code in order to adapt it to your needs.
//  - Redistribution of this ( or a modified version) source code is prohibited. You may only redistribute compiled versions.
//  - You may redistribute the compiled version as part of your application, not a new java component with the same purpose as this one.
//  - You may not remove this notice from the source code
//  - This notice disclaim all warranties of all material
//  - You may not copy and paste any code into external files.



/**
 * commands for IBM Pro Printer and compatible
 */
public class ProPrinterXL extends TextPrinter {

  public ProPrinterXL() {

          super();

          hLineChar='\u2500'; // U2500 BOX DRAWINGS LIGHT HORIZONTAL
          vLineChar='\u2502';  // U2502 BOX DRAWINGS LIGHT VERTICAL
          tlCornerChar='\u250C'; // U250C BOX DRAWINGS LIGHT DOWN AND RIGHT
          trCornerChar='\u2510'; // U2510 BOX DRAWINGS LIGHT DOWN AND LEFT
          blCornerChar='\u2514'; // U2514 BOX DRAWINGS LIGHT UP AND RIGHT
          brCornerChar='\u2518'; // U2518 BOX DRAWINGS LIGHT UP AND LEFT
          vrLineChar='\u251C'; // U252C BOX DRAWINGS LIGHT VERTICAL AND RIGHT
          vlLineChar='\u2524'; // U2524 BOX DRAWINGS LIGHT VERTICAL AND LEFT
          htLineChar='\u2534'; // U2534 BOX DRAWINGS LIGHT UP AND HORIZONTAL
          hbLineChar='\u252C'; // U252C BOX DRAWINGS LIGHT DOWN AND HORIZONTAL
          crossLineChar='\u253C';  // U253C BOX DRAWINGS LIGHT VERTICAL AND HORIZONTAL
          linesCharSet="437";

          setCommand(new Command(CMD_RESET,""));
          setCommand(new Command(CMD_BOLD_ON,"EscE"));
          setCommand(new Command(CMD_BOLD_OFF,"EscF"));

          setCommand(new Command(CMD_ITALIC_ON,"Esc["+((char) 3)+"m")); // Esc%G
          setCommand(new Command(CMD_ITALIC_OFF,"Esc["+((char) 23)+"m")); // Esc%H

          setCommand(new Command(CMD_DOUBLESTRIKE_ON,"EscG"));
          setCommand(new Command(CMD_DOUBLESTRIKE_OFF,"EscH"));

          setCommand(new Command(CMD_UNDERLINED_ON,"Esc-1"));
          setCommand(new Command(CMD_UNDERLINED_OFF,"Esc-0"));

          setCommand(new Command(CMD_DOUBLEWIDE_ON,"EscW1"));
          setCommand(new Command(CMD_DOUBLEWIDE_OFF,"EscW0"));

          setCommand(new Command(CMD_CONDENSED_ON,""));
          setCommand(new Command(CMD_CONDENSED_OFF,""));

          setCommand(new Command(CMD_SUBSCRIPT_ON,"EscS1"));
          setCommand(new Command(CMD_SUBSCRIPT_OFF,"EscT"));

          setCommand(new Command(CMD_SUPERSCRIPT_ON,"EscS0"));
          setCommand(new Command(CMD_SUPERSCRIPT_OFF,"EscT"));

          //Esc C n m Set form length in lines or inches.
          // This command specifies the physical page length in multiples of the current line spacing
          // and sets the current vertical position as the first print line (top of form).
          setCommand(new Command(CMD_PAGE_LENGTH_LINES,"EscC#")); // in lines < 7F, in inches EscNULL#
          setCommand(new Command(CMD_PAGE_LENGTH_INCHES,"Esc"+((char) 0)+"#"));
          setCommand(new Command(CMD_PAGE_WIDTH,""));

          setCommand(new Command(CMD_TOP_MARGIN,"")); // first print line ESC4
          setCommand(new Command(CMD_BOTTOM_MARGIN,""));
          setCommand(new Command(CMD_LEFT_MARGIN,"")); //ESC;
          setCommand(new Command(CMD_RIGHT_MARGIN,"")); //ESCX##  left and right marign in columns

          setCommand(new Command(CMD_SELECT_FONT,"")); // Esck
          setCommand(new Command(CMD_SELECT_CHAR_SET,"#")); // code page

          setCommand(new Command(CMD_PITCH,"#"));
          setCommand(new Command(CMD_PROPORTIONAL_ON,"EscP1")); // supported for all fonts except OCR
          setCommand(new Command(CMD_PROPORTIONAL_OFF,"EscP0"));

          setCommand(new Command(CMD_PORTRAIT,""));
          setCommand(new Command(CMD_LANDSCAPE,""));
          setCommand(new Command(CMD_INTERSPACE,"#"));
          setCommand(new Command(CMD_QUALITY,"")); // sets cpi10 "Esc"+(char(0x49))+(char(0x02))
          setCommand(new Command(CMD_DRAFT,"")); // sets cpi10 "Esc"+(char(0x49))+(char(0x00))
          setCommand(new Command(CMD_HORIZONTAL_POSITIONING,""));


          super.addPitch("10","Ctrl+R");
          super.addPitch("12","Esc:");
          super.addPitch("15","Escg"); // to check
          super.addPitch("17.1","Ctrl+R"+((char) 15));
          super.addPitch("20","Esc:"+((char) 15));  // to check


          /**
           Esc 0 1/8 inch line spacing.
          Esc A n Set text line spacing. n is in units of n/72".
           */
          super.addSpacing("8","Esc0");
          super.addSpacing("12","EscA6");
          super.addSpacing("18","EscA4");
          super.addSpacing("24","EscA3");
          super.addSpacing("72","EscA#");
          super.addSpacing("216","Esc3#");


         super.clearCharSet();
         super.addCharSet("437","EscT"+((char) 4)+((char) 0)+((char) 0)+((char) Math.floor(437 / 256) )+((char) (437 % 256) ),"USA");
         super.addCharSet("850","EscT"+((char) 4)+((char) 0)+((char) 0)+((char) Math.floor(850 / 256) )+((char) (850 % 256) )+"","Multilingual");
         super.addCharSet("860","EscT"+((char) 4)+((char) 0)+((char) 0)+((char) Math.floor(860 / 256) )+((char) (860 % 256) )+"","Portugal");
         super.addCharSet("863","EscT"+((char) 4)+((char) 0)+((char) 0)+((char) Math.floor(863 / 256) )+((char) (863 % 256) )+"","863");
         super.addCharSet("865","EscT"+((char) 4)+((char) 0)+((char) 0)+((char) Math.floor(865 / 256) )+((char) (865 % 256) )+"","Norway");
         super.addCharSet("858","EscT"+((char) 4)+((char) 0)+((char) 0)+((char) Math.floor(858 / 256) )+((char) (858 % 256) )+"","858");




          super.defaultCharSet="437";

  }



}