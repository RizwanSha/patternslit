package patterns.config.framework.textprinter.printers;

import patterns.config.framework.textprinter.Command;
import patterns.config.framework.textprinter.TextPrinter;

//  RTextPrinter
//  Copyright (C)
//
//  Java4Less@Confluencia.net
//  All rights reserved
//
// Adquisition , use and distribution of this code is subject to restriction:
//  - You may modify the source code in order to adapt it to your needs.
//  - Redistribution of this ( or a modified version) source code is prohibited. You may only redistribute compiled versions.
//  - You may redistribute the compiled version as part of your application, not a new java component with the same purpose as this one.
//  - You may not remove this notice from the source code
//  - This notice disclaim all warranties of all material
//  - You may not copy and paste any code into external files.



/**
 * commands for Printronix P-Series emulation.
 */
public class PrintronixPSeries extends TextPrinter {

  public static String SFCC=""+((char) 1);

  public PrintronixPSeries() {

          super();

          hLineChar='\u2500'; // U2500 BOX DRAWINGS LIGHT HORIZONTAL
          vLineChar='\u2502';  // U2502 BOX DRAWINGS LIGHT VERTICAL
          tlCornerChar='\u250C'; // U250C BOX DRAWINGS LIGHT DOWN AND RIGHT
          trCornerChar='\u2510'; // U2510 BOX DRAWINGS LIGHT DOWN AND LEFT
          blCornerChar='\u2514'; // U2514 BOX DRAWINGS LIGHT UP AND RIGHT
          brCornerChar='\u2518'; // U2518 BOX DRAWINGS LIGHT UP AND LEFT
          vrLineChar='\u251C'; // U252C BOX DRAWINGS LIGHT VERTICAL AND RIGHT
          vlLineChar='\u2524'; // U2524 BOX DRAWINGS LIGHT VERTICAL AND LEFT
          htLineChar='\u2534'; // U2534 BOX DRAWINGS LIGHT UP AND HORIZONTAL
          hbLineChar='\u252C'; // U252C BOX DRAWINGS LIGHT DOWN AND HORIZONTAL
          crossLineChar='\u253C';  // U253C BOX DRAWINGS LIGHT VERTICAL AND HORIZONTAL
          linesCharSet="437";

          setCommand(new Command(CMD_RESET,""));
          setCommand(new Command(CMD_BOLD_ON,SFCC+"G"));
          setCommand(new Command(CMD_BOLD_OFF,SFCC+"H"));

          setCommand(new Command(CMD_ITALIC_ON,""));
          setCommand(new Command(CMD_ITALIC_OFF,""));

          setCommand(new Command(CMD_DOUBLESTRIKE_ON,""));
          setCommand(new Command(CMD_DOUBLESTRIKE_OFF,""));

          setCommand(new Command(CMD_UNDERLINED_ON,SFCC+"-1"));
          setCommand(new Command(CMD_UNDERLINED_OFF,SFCC+"-0"));

          setCommand(new Command(CMD_DOUBLEWIDE_ON,""));
          setCommand(new Command(CMD_DOUBLEWIDE_OFF,""));

          setCommand(new Command(CMD_CONDENSED_ON,""));
          setCommand(new Command(CMD_CONDENSED_OFF,""));

          setCommand(new Command(CMD_SUBSCRIPT_ON,SFCC+"S"+((char) 1)));
          setCommand(new Command(CMD_SUBSCRIPT_OFF,SFCC+"T"));

          setCommand(new Command(CMD_SUPERSCRIPT_ON,SFCC+"S"+((char) 0)));
          setCommand(new Command(CMD_SUPERSCRIPT_OFF,SFCC+"T"));


          setCommand(new Command(CMD_PAGE_LENGTH_LINES,""));// SFCC LINES;n where n=1 to 192
          /* SFCC INCHES; n.f
          Purpose Sets the length of forms (paper) in inches.
          where n = 1 through 24 (hex 01 through hex 18) specify the number of
          inches on a page.
          f = fractional number in .5-inch increments (minimum forms
          length is .5 inches).
          */
          setCommand(new Command(CMD_PAGE_LENGTH_INCHES,""));


          setCommand(new Command(CMD_PAGE_WIDTH,""));
          setCommand(new Command(CMD_TOP_MARGIN,""));

          setCommand(new Command(CMD_BOTTOM_MARGIN,""));


          setCommand(new Command(CMD_LEFT_MARGIN,"")); // select pitch before setting margins

          setCommand(new Command(CMD_RIGHT_MARGIN,""));

          setCommand(new Command(CMD_SELECT_FONT,""));
          setCommand(new Command(CMD_SELECT_CHAR_SET,"#")); // ""+ESC+"R(#)"));
          setCommand(new Command(CMD_PITCH,"#"));
          setCommand(new Command(CMD_PROPORTIONAL_ON,""));
          setCommand(new Command(CMD_PROPORTIONAL_OFF,""));

          setCommand(new Command(CMD_PORTRAIT,""));
          setCommand(new Command(CMD_LANDSCAPE,""));
          setCommand(new Command(CMD_INTERSPACE,"#"));
          setCommand(new Command(CMD_QUALITY,""));
          setCommand(new Command(CMD_DRAFT,""));
          setCommand(new Command(CMD_HMI,""));

          setCommand(new Command(CMD_HORIZONTAL_POSITIONING,""));


          super.addCharSet("437",SFCC+((char) 108)+"000","USA");
          super.addCharSet("850",SFCC+((char) 108)+"001","Multilingual");


          super.addPitch("10",SFCC+"X*0");
          super.addPitch("12",SFCC+"X*1");;
          super.addPitch("13",SFCC+"X*2");
          super.addPitch("15",SFCC+"X*3");
          super.addPitch("17",SFCC+"X*4");
          super.addPitch("20",SFCC+"X*5");

          super.addSpacing("6",SFCC+"2");
          super.addSpacing("8",SFCC+"0");
          super.addSpacing("72",SFCC+"A#");
          super.addSpacing("216",SFCC+"3#");


          super.defaultCharSet="437";

  }



}