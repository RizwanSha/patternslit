package patterns.config.framework.textprinter.printers;
//  RTextPrinter
//  Copyright (C)
//
//  Java4Less@Confluencia.net
//  All rights reserved
//
// Adquisition , use and distribution of this code is subject to restriction:
//  - You may modify the source code in order to adapt it to your needs.
//  - Redistribution of this ( or a modified version) source code is prohibited. You may only redistribute compiled versions.
//  - You may redistribute the compiled version as part of your application, not a new java component with the same purpose as this one.
//  - You may not remove this notice from the source code
//  - This notice disclaim all warranties of all material
//  - You may not copy and paste any code into external files.



/**
 * Epson LX300+
 */
public class EpsonLX300 extends ESCPPrinter {

  public EpsonLX300() {

  super();

          //Always set the pitch before setting the margins. Do not assume what the pitch setting wil
         //setCommand(new Command(CMD_LEFT_MARGIN,""+ESC+"l#CHAR#")); // select pitch before setting margins
          // The printer calculates the right margin based on 10 cpi if proportional spacing
          //setCommand(new Command(CMD_RIGHT_MARGIN,""+ESC+"Q#CHAR#"));


  super.addCharSet("ISO8859-7",((char) 27)+"(t"+((char) 3)+((char)0)+"0"+((char) 29)+((char) 7)+((char) 27)+"t0","Latin/Greek");


  }


}