package patterns.config.framework.textprinter.printers;

import patterns.config.framework.textprinter.Command;
import patterns.config.framework.textprinter.TextPrinter;
import patterns.config.framework.textprinter.TextPrinterException;

//  RTextPrinter
//  Copyright (C)
//
//  Java4Less@Confluencia.net
//  All rights reserved
//
// Adquisition , use and distribution of this code is subject to restriction:
//  - You may modify the source code in order to adapt it to your needs.
//  - Redistribution of this ( or a modified version) source code is prohibited. You may only redistribute compiled versions.
//  - You may redistribute the compiled version as part of your application, not a new java component with the same purpose as this one.
//  - You may not remove this notice from the source code
//  - This notice disclaim all warranties of all material
//  - You may not copy and paste any code into external files.



/**
 * commands for Epson ESCP or compatible printers.
 */
public class ESCPPrinter extends TextPrinter {

  public ESCPPrinter() {

          super();

          hLineChar='\u2500'; // U2500 BOX DRAWINGS LIGHT HORIZONTAL
          vLineChar='\u2502';  // U2502 BOX DRAWINGS LIGHT VERTICAL
          tlCornerChar='\u250C'; // U250C BOX DRAWINGS LIGHT DOWN AND RIGHT
          trCornerChar='\u2510'; // U2510 BOX DRAWINGS LIGHT DOWN AND LEFT
          blCornerChar='\u2514'; // U2514 BOX DRAWINGS LIGHT UP AND RIGHT
          brCornerChar='\u2518'; // U2518 BOX DRAWINGS LIGHT UP AND LEFT
          vrLineChar='\u251C'; // U252C BOX DRAWINGS LIGHT VERTICAL AND RIGHT
          vlLineChar='\u2524'; // U2524 BOX DRAWINGS LIGHT VERTICAL AND LEFT
          htLineChar='\u2534'; // U2534 BOX DRAWINGS LIGHT UP AND HORIZONTAL
          hbLineChar='\u252C'; // U252C BOX DRAWINGS LIGHT DOWN AND HORIZONTAL
          crossLineChar='\u253C';  // U253C BOX DRAWINGS LIGHT VERTICAL AND HORIZONTAL
          linesCharSet="437";

          setCommand(new Command(CMD_RESET,""+ESC+"@"));
          setCommand(new Command(CMD_BOLD_ON,"EscE"));
          setCommand(new Command(CMD_BOLD_OFF,"EscF"));

          setCommand(new Command(CMD_ITALIC_ON,"Esc4"));
          setCommand(new Command(CMD_ITALIC_OFF,"Esc5"));

          setCommand(new Command(CMD_DOUBLESTRIKE_ON,"EscG"));
          setCommand(new Command(CMD_DOUBLESTRIKE_OFF,"EscH"));

          setCommand(new Command(CMD_UNDERLINED_ON,""+ESC+((char) 45)+((char) 1)));
          setCommand(new Command(CMD_UNDERLINED_OFF,""+ESC+((char) 45)+((char) 0)));

          setCommand(new Command(CMD_DOUBLEWIDE_ON,""+ESC+((char) 87)+((char) 1)));
          setCommand(new Command(CMD_DOUBLEWIDE_OFF,""+ESC+((char) 87)+((char) 0)));

          setCommand(new Command(CMD_CONDENSED_ON,"Esc"+((char) 15)));
          setCommand(new Command(CMD_CONDENSED_OFF,""+((char) 18)));

          setCommand(new Command(CMD_SUBSCRIPT_ON,"EscS"+((char) 1)));
          setCommand(new Command(CMD_SUBSCRIPT_OFF,"EscT"));

          setCommand(new Command(CMD_SUPERSCRIPT_ON,"EscS"+((char) 0)));
          setCommand(new Command(CMD_SUPERSCRIPT_OFF,"EscT"));

          // ESC C n, this resets top and bottom margins. Page length in inches "EscC"++((char) 0)"#"
          /*
          Changing the line spacing does not affect the current page-length setting.
          Sets the page length to n lines in the current line spacing
          Depends on default-setting mode or DIP-switch setting
          */
          setCommand(new Command(CMD_PAGE_LENGTH_LINES,"EscC#CHAR#"));
          setCommand(new Command(CMD_PAGE_LENGTH_INCHES,"EscC"+((char) 0)+"#CHAR#"));
          setCommand(new Command(CMD_PAGE_WIDTH,""));

          // this value will be cancelled by the page length command so leave it empty
          setCommand(new Command(CMD_TOP_MARGIN,""));

          // Sending this command cancels the top-margin setting.
          // only used for continous paper printing, cancels top maring setting
          /**
            With ESC/P 2 printers, use the ESC ( c command instead; this allows you to set both top and bottom margins on continuous and single-sheet paper.
            ESC ( c nL nH tL tH bL bH
            nL = 4, nH = 0
           */
          setCommand(new Command(CMD_BOTTOM_MARGIN,""+ESC+"N#CHAR#"));

          /**
           Set the page length before paper is loaded or when the print position is at the top-ofform
position. Otherwise, the current print position becomes the top-of-form position.
Setting the page length cancels the top and bottom margin settings.
           */
          //Always set the pitch before setting the margins. Do not assume what the pitch setting wil
         //setCommand(new Command(CMD_LEFT_MARGIN,""+ESC+"l#CHAR#")); // select pitch before setting margins
          // The printer calculates the right margin based on 10 cpi if proportional spacing
          //setCommand(new Command(CMD_RIGHT_MARGIN,""+ESC+"Q#CHAR#"));

          setCommand(new Command(CMD_SELECT_FONT,""+ESC+"k#")); //""+ESC+"k(#)"));
          setCommand(new Command(CMD_SELECT_CHAR_SET,"#")); // ""+ESC+"R(#)"));
          setCommand(new Command(CMD_PITCH,"#"));
          setCommand(new Command(CMD_PROPORTIONAL_ON,""+ESC+"p1"));
          setCommand(new Command(CMD_PROPORTIONAL_OFF,""+ESC+"p0"));

          setCommand(new Command(CMD_PORTRAIT,""));
          setCommand(new Command(CMD_LANDSCAPE,""));
          setCommand(new Command(CMD_INTERSPACE,"#"));
          setCommand(new Command(CMD_QUALITY,""+ESC+"x1"));
          setCommand(new Command(CMD_DRAFT,""+ESC+"x0"));
          /** cancelled if one of this commands is issued
            cpi 10
            cpi 10.5
            cpi 15
            double width printing
            reset
            condensed printing
           */
          setCommand(new Command(CMD_HMI,"Escc"));

          setCommand(new Command(CMD_HORIZONTAL_POSITIONING,"Esc$"));

          super.addFont("Roman",""+((char) 0));
          super.addFont("SansSerif",""+((char) 1));
          super.addFont("Courier",""+((char) 2));
          super.addFont("Prestige",""+((char) 3)); // fonts 3 to 9 are not available on all printers
          super.addFont("Script",""+((char) 4));
          super.addFont("OCR-B",""+((char) 5));
          super.addFont("OCR-A",""+((char) 6));
          super.addFont("Orator",""+((char) 7));
          super.addFont("Orator-S",""+((char) 8));
          super.addFont("Script-C",""+((char) 9));

         // super.addCharSet("437","Esct"+((char) 0)+"EscR"+((char) 0),"USA");
         super.addCharSet("437","Esct1"+((char) 0),"USA");
          super.addCharSet("ISO-IR-69","EsctCRTL+@EscR"+((char) 1),"France");
          super.addCharSet("ISO-IR-21","EsctCRTL+@EscR"+((char) 2),"Germany");
          super.addCharSet("ISO-IR-4","EsctCRTL+@EscR"+((char) 3),"England");
          super.addCharSet("Denmark","EsctCRTL+@EscR"+((char) 4),"Denmark");
          super.addCharSet("Sweden","EsctCRTL+@EscR"+((char) 5),"Sweden");
          super.addCharSet("ISO-IR-15","EsctCRTL+@EscR"+((char) 6),"Italy");
          super.addCharSet("ISO-IR-17","EsctCRTL+@EscR"+((char) 7),"Spain");
          super.addCharSet("Japan","EsctCRTL+@EscR"+((char) 8),"Japan");
          super.addCharSet("ISO-IR-60","EsctCRTL+@EscR"+((char) 9),"Norway");
          super.addCharSet("Denmark II","EsctCRTL+@EscR"+((char) 10),"Denmark II");
          super.addCharSet("ISO-IR-85","EsctCRTL+@EscR"+((char) 11),"Spain II");
          super.addCharSet("Latin America","EsctCRTL+@EscR"+((char) 12),"Latin America");
          super.addCharSet("Korea","EsctCRTL+@EscR"+((char) 13),"Korea");
            super.addCharSet("ISO8859-7",((char) 27)+"(t"+((char) 3)+((char)0)+"0"+((char) 29)+((char) 7)+((char) 27)+"t0","Latin/Greek");

          super.addPitch("10","EscP");
          super.addPitch("12","EscM");
          super.addPitch("15","Escg");

          super.addSpacing("6","Esc2");
          super.addSpacing("8","Esc0");
          super.addSpacing("60","EscA#CHAR#");
          super.addSpacing("180","Esc3#CHAR#");
          super.addSpacing("360","Esc+#CHAR#");


          super.defaultCharSet="437";

  }




  /**
   * move cursor to a given column
   */
   protected void moveToHPosition(int col) throws  TextPrinterException  {

      double colInches=((this.jobSetup.columnWidth*col*60)/256);

      byte[] bytesPosition=getLowHeightBytes(colInches);
      this.executeCommand(this.CMD_HORIZONTAL_POSITIONING);
      addToBuffer(bytesPosition);
   }

   /**
  * execute command with 1 parameter
  */
  protected void executeCommand(String name,String param) throws TextPrinterException {

     if (name.equals(CMD_LEFT_MARGIN)) {
            // epson says the minimul left margin is 1
            int m=Integer.parseInt(param);
            if (m==0) m=1;
            executeCommandInternal(CMD_LEFT_MARGIN,""+ m);
      }
      else super.executeCommand(name,param);

  }

}