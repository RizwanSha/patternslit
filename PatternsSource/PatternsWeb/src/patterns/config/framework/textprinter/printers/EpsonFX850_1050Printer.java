package patterns.config.framework.textprinter.printers;

import patterns.config.framework.textprinter.Command;

//  RTextPrinter
//  Copyright (C)
//
//  Java4Less@Confluencia.net
//  All rights reserved
//
// Adquisition , use and distribution of this code is subject to restriction:
//  - You may modify the source code in order to adapt it to your needs.
//  - Redistribution of this ( or a modified version) source code is prohibited. You may only redistribute compiled versions.
//  - You may redistribute the compiled version as part of your application, not a new java component with the same purpose as this one.
//  - You may not remove this notice from the source code
//  - This notice disclaim all warranties of all material
//  - You may not copy and paste any code into external files.

public class EpsonFX850_1050Printer extends ESCPPrinter {

  public EpsonFX850_1050Printer() {
      super();

      setCommand(new Command(CMD_RESET,"Esc@Esct"+((char) 1)));

      // this printer only supports 2 character sets
      // 1. Italics
      // 2. Epson graphic characters (ASCII)

      super.clearCharSet();



      super.addCharSet("437","Esct1"+((char) 0),"USA");
      super.addCharSet("ISO-IR-69","EsctCRTL+@EscR"+((char) 1),"France");
      super.addCharSet("ISO-IR-21","EsctCRTL+@EscR"+((char) 2),"Germany");
      super.addCharSet("ISO-IR-4","EsctCRTL+@EscR"+((char) 3),"England");
      super.addCharSet("Denmark","EsctCRTL+@EscR"+((char) 4),"Denmark");
      super.addCharSet("Sweden","EsctCRTL+@EscR"+((char) 5),"Sweden");
      super.addCharSet("ISO-IR-15","EsctCRTL+@EscR"+((char) 6),"Italy");
      super.addCharSet("ISO-IR-17","EsctCRTL+@EscR"+((char) 7),"Spain");
      super.addCharSet("Japan","EsctCRTL+@EscR"+((char) 8),"Japan");
      super.addCharSet("ISO-IR-60","EsctCRTL+@EscR"+((char) 9),"Norway");
      super.addCharSet("Denmark II","EsctCRTL+@EscR"+((char) 10),"Denmark II");
      super.addCharSet("ISO-IR-85","EsctCRTL+@EscR"+((char) 11),"Spain II");
      super.addCharSet("Latin America","EsctCRTL+@EscR"+((char) 12),"Latin America");
      super.addCharSet("Korea","EsctCRTL+@EscR"+((char) 13),"Korea");

      super.defaultCharSet="437";

      this.debug=true;

  }
}