package patterns.config.framework.textprinter.printers;

import patterns.config.framework.textprinter.Command;
import patterns.config.framework.textprinter.TextPrinter;
import patterns.config.framework.textprinter.TextPrinterException;

//  RTextPrinter
//  Copyright (C)
//
//  Java4Less@Confluencia.net
//  All rights reserved
//
// Adquisition , use and distribution of this code is subject to restriction:
//  - You may modify the source code in order to adapt it to your needs.
//  - Redistribution of this ( or a modified version) source code is prohibited. You may only redistribute compiled versions.
//  - You may redistribute the compiled version as part of your application, not a new java component with the same purpose as this one.
//  - You may not remove this notice from the source code
//  - This notice disclaim all warranties of all material
//  - You may not copy and paste any code into external files.



/**
 * commands for Epson ESCP2 or compatible printers.
 */
public class ESCP2Printer extends TextPrinter {

  public ESCP2Printer() {

          super();

          hLineChar='\u2500'; // U2500 BOX DRAWINGS LIGHT HORIZONTAL
          vLineChar='\u2502';  // U2502 BOX DRAWINGS LIGHT VERTICAL
          tlCornerChar='\u250C'; // U250C BOX DRAWINGS LIGHT DOWN AND RIGHT
          trCornerChar='\u2510'; // U2510 BOX DRAWINGS LIGHT DOWN AND LEFT
          blCornerChar='\u2514'; // U2514 BOX DRAWINGS LIGHT UP AND RIGHT
          brCornerChar='\u2518'; // U2518 BOX DRAWINGS LIGHT UP AND LEFT
          vrLineChar='\u251C'; // U252C BOX DRAWINGS LIGHT VERTICAL AND RIGHT
          vlLineChar='\u2524'; // U2524 BOX DRAWINGS LIGHT VERTICAL AND LEFT
          htLineChar='\u2534'; // U2534 BOX DRAWINGS LIGHT UP AND HORIZONTAL
          hbLineChar='\u252C'; // U252C BOX DRAWINGS LIGHT DOWN AND HORIZONTAL
          crossLineChar='\u253C';  // U253C BOX DRAWINGS LIGHT VERTICAL AND HORIZONTAL
          linesCharSet="437";

          setCommand(new Command(CMD_RESET,""+ESC+"@"));
          setCommand(new Command(CMD_BOLD_ON,"EscE"));
          setCommand(new Command(CMD_BOLD_OFF,"EscF"));

          setCommand(new Command(CMD_ITALIC_ON,"Esc4"));
          setCommand(new Command(CMD_ITALIC_OFF,"Esc5"));

          setCommand(new Command(CMD_DOUBLESTRIKE_ON,"EscG"));
          setCommand(new Command(CMD_DOUBLESTRIKE_OFF,"EscH"));

          setCommand(new Command(CMD_UNDERLINED_ON,""+ESC+((char) 45)+((char) 1)));
          setCommand(new Command(CMD_UNDERLINED_OFF,""+ESC+((char) 45)+((char) 0)));

          setCommand(new Command(CMD_DOUBLEWIDE_ON,""+ESC+((char) 87)+((char) 1)));
          setCommand(new Command(CMD_DOUBLEWIDE_OFF,""+ESC+((char) 87)+((char) 0)));

          setCommand(new Command(CMD_CONDENSED_ON,"Esc"+((char) 15)));
          setCommand(new Command(CMD_CONDENSED_OFF,""+((char) 18)));

          setCommand(new Command(CMD_SUBSCRIPT_ON,"EscS"+((char) 1)));
          setCommand(new Command(CMD_SUBSCRIPT_OFF,"EscT"));

          setCommand(new Command(CMD_SUPERSCRIPT_ON,"EscS"+((char) 0)));
          setCommand(new Command(CMD_SUPERSCRIPT_OFF,"EscT"));

          // ESC C n, this resets top and bottom margins. Page length in inches "EscC"++((char) 0)"#"
          /*
          Changing the line spacing does not affect the current page-length setting.
          Sets the page length to n lines in the current line spacing
          Depends on default-setting mode or DIP-switch setting
          */
          setCommand(new Command(CMD_PAGE_LENGTH_LINES,"EscC#CHAR#"));
          setCommand(new Command(CMD_PAGE_LENGTH_INCHES,"EscC"+((char) 0)+"#CHAR#"));
          setCommand(new Command(CMD_PAGE_WIDTH,""));

          // this value will be cancelled by the page length command so leave it empty
          setCommand(new Command(CMD_TOP_MARGIN,""));

          // Sending this command cancels the top-margin setting.
          // only used for continous paper printing, cancels top maring setting
          /**
            With ESC/P 2 printers, use the ESC ( c command instead; this allows you to set both top and bottom margins on continuous and single-sheet paper.
            ESC ( c nL nH tL tH bL bH
            nL = 4, nH = 0
           */
          setCommand(new Command(CMD_BOTTOM_MARGIN,""+ESC+"N#CHAR#"));

          /**
           Set the page length before paper is loaded or when the print position is at the top-ofform
position. Otherwise, the current print position becomes the top-of-form position.
Setting the page length cancels the top and bottom margin settings.
           */
          //Always set the pitch before setting the margins. Do not assume what the pitch setting wil
          //setCommand(new Command(CMD_LEFT_MARGIN,""+ESC+"l#CHAR#")); // select pitch before setting margins
          // The printer calculates the right margin based on 10 cpi if proportional spacing
          //setCommand(new Command(CMD_RIGHT_MARGIN,""+ESC+"Q#CHAR#"));

          setCommand(new Command(CMD_SELECT_FONT,""+ESC+"k#")); //""+ESC+"k(#)"));
          setCommand(new Command(CMD_SELECT_CHAR_SET,"#")); // ""+ESC+"R(#)"));
          setCommand(new Command(CMD_PITCH,"#"));
          setCommand(new Command(CMD_PROPORTIONAL_ON,""+ESC+"p1"));
          setCommand(new Command(CMD_PROPORTIONAL_OFF,""+ESC+"p0"));

          setCommand(new Command(CMD_PORTRAIT,""));
          setCommand(new Command(CMD_LANDSCAPE,""));
          setCommand(new Command(CMD_INTERSPACE,"#"));
          setCommand(new Command(CMD_QUALITY,""+ESC+"x1"));
          setCommand(new Command(CMD_DRAFT,""+ESC+"x0"));
          /** cancelled if one of this commands is issued
            cpi 10
            cpi 10.5
            cpi 15
            double width printing
            reset
            condensed printing
           */
          setCommand(new Command(CMD_HMI,"Escc"));

          setCommand(new Command(CMD_HORIZONTAL_POSITIONING,"Esc$"));

          super.addFont("Roman",""+((char) 0));
          super.addFont("SansSerif",""+((char) 1));
          super.addFont("Courier",""+((char) 2));
          super.addFont("Prestige",""+((char) 3));
          super.addFont("Script",""+((char) 4));
          super.addFont("OCR-B",""+((char) 5));
          super.addFont("OCR-A",""+((char) 6));
          super.addFont("Orator",""+((char) 7));
          super.addFont("Orator-S",""+((char) 8));
          super.addFont("Script-C",""+((char) 9));


          super.addCharSet("437","Esc(tCRTL+CCRTL+@1"+((char) 1)+((char) 0)+"Esct1","USA");
          super.addCharSet("ISO-IR-69","Esc(tCRTL+CCRTL+@0"+((char) 0)+((char) 0)+"Esct0EscR"+((char) 1),"France");
          super.addCharSet("ISO-IR-21","Esc(tCRTL+CCRTL+@0"+((char) 0)+((char) 0)+"Esct0EscR"+((char) 2),"Germany");
          super.addCharSet("ISO-IR-4","Esc(tCRTL+CCRTL+@0"+((char) 0)+((char) 0)+"Esct0EscR"+((char) 3),"England");
          super.addCharSet("Denmark","Esc(tCRTL+CCRTL+@0"+((char) 0)+((char) 0)+"Esct0EscR"+((char) 4),"Denmark");
          super.addCharSet("Sweden","Esc(tCRTL+CCRTL+@0"+((char) 0)+((char) 0)+"Esct0EscR"+((char) 5),"Sweden");
          super.addCharSet("ISO-IR-15","Esc(tCRTL+CCRTL+@0"+((char) 0)+((char) 0)+"Esct0EscR"+((char) 6),"Italy");
          super.addCharSet("ISO-IR-17","Esc(tCRTL+CCRTL+@0"+((char) 0)+((char) 0)+"Esct0EscR"+((char) 7),"Spain");
          super.addCharSet("Japan","Esc(tCRTL+CCRTL+@0"+((char) 0)+((char) 0)+"Esct0EscR"+((char) 8),"Japan");
          super.addCharSet("ISO-IR-60","Esc(tCRTL+CCRTL+@0"+((char) 0)+((char) 0)+"Esct0EscR"+((char) 9),"Norway");
          super.addCharSet("Denmark II","Esc(tCRTL+CCRTL+@0"+((char) 0)+((char) 0)+"Esct0EscR"+((char) 10),"Denmark II");
          super.addCharSet("ISO-IR-85","Esc(tCRTL+CCRTL+@0"+((char) 0)+((char) 0)+"Esct0EscR"+((char) 11),"Spain II");
          super.addCharSet("Latin America","Esc(tCRTL+CCRTL+@0"+((char) 0)+((char) 0)+"Esct0EscR"+((char) 12),"Latin America");
          super.addCharSet("Korea","Esc(tCRTL+CCRTL+@1"+((char) 1)+((char) 0)+"Esct1EscR"+((char) 13),"Korea");
          super.addCharSet("932","Esc(tCRTL+CCRTL+@0"+((char) 2)+((char) 0)+"Esct0","Japanese");
          super.addCharSet("850","Esc(tCRTL+CCRTL+@0"+((char) 3)+((char) 0)+"Esct0","Multilingual");
          super.addCharSet("851","Esc(tCRTL+CCRTL+@0"+((char) 4)+((char) 0)+"Esct0","Greek");
          super.addCharSet("853","Esc(tCRTL+CCRTL+@0"+((char) 5)+((char) 0)+"Esct0","Turkish");
          super.addCharSet("855","Esc(tCRTL+CCRTL+@0"+((char) 6)+((char) 0)+"Esct0","Cyrillic");
          super.addCharSet("860","Esc(tCRTL+CCRTL+@0"+((char) 7)+((char) 0)+"Esct0","Portugal");
          super.addCharSet("863","Esc(tCRTL+CCRTL+@0"+((char) 8)+((char) 0)+"Esct0","Canada-French");
          super.addCharSet("865","Esc(tCRTL+CCRTL+@0"+((char) 9)+((char) 0)+"Esct0","Norway");
          super.addCharSet("852","Esc(tCRTL+CCRTL+@0"+((char) 10)+((char) 0)+"Esct0","East Europe");
          super.addCharSet("857","Esc(tCRTL+CCRTL+@0"+((char) 11)+((char) 0)+"Esct0","Turkish II");
          super.addCharSet("862","Esc(tCRTL+CCRTL+@0"+((char) 12)+((char) 0)+"Esct0","Hebrew");
          super.addCharSet("864","Esc(tCRTL+CCRTL+@0"+((char) 13)+((char) 0)+"Esct0","Arabic");
          super.addCharSet("866","Esc(tCRTL+CCRTL+@0"+((char) 14)+((char) 0)+"Esct0","Russian");
          super.addCharSet("ISO8859-7","Esc(tCRTL+CCRTL+@0"+((char) 29)+((char) 7)+"Esct0","Latin/Greek");
          super.addCharSet("ISO8859-1","Esc(tCRTL+CCRTL+@0"+((char) 29)+((char) 16)+"Esct0","Latin 1");
          super.addCharSet("ISO8859-2","Esc(tCRTL+CCRTL+@0"+((char) 127)+((char) 2)+"Esct0","Latin 2");
          super.addCharSet("861","Esc(tCRTL+CCRTL+@0"+((char) 24)+((char) 0)+"Esct0","Iceland");

          super.addCharSet("KU42","Esc(tCRTL+CCRTL+@0"+((char) 18)+((char) 0)+"Esct0","K.U. Thai");
          super.addCharSet("TIS11","Esc(tCRTL+CCRTL+@0"+((char) 19)+((char) 0)+"Esct0","TS 988 Thai");
          super.addCharSet("TIS18","Esc(tCRTL+CCRTL+@0"+((char) 20)+((char) 0)+"Esct0","GENERAL Thai");
          super.addCharSet("TIS17","Esc(tCRTL+CCRTL+@0"+((char) 21)+((char) 0)+"Esct0","SIC STD. Thai");
          super.addCharSet("TIS13","Esc(tCRTL+CCRTL+@0"+((char) 22)+((char) 0)+"Esct0","IBM STD. Thai");
          super.addCharSet("TIS16","Esc(tCRTL+CCRTL+@0"+((char) 23)+((char) 0)+"Esct0","SIC OLD Thai");


/*
19 0 TIS11 (TS 988 Thai)
20 0 TIS18 (GENERAL Thai)
21 0 TIS17 (SIC STD. Thai)
22 0 TIS13 (IBM STD. Thai)
23 0 TIS16 (SIC OLD Thai)
25 0 BRASCII
d2 d3 Table name
26 0 Abicomp
27 0 MAZOWIA (Poland)
28 0 Code MJK (CSFR)
30 0 TSM/WIN (Thai system manager)
31 0 ISO Latin 1T (Turkish)
32 0 Bulgaria
33 0 Hebrew 7
34 0 Hebrew 8
35 0 Roman 8
36 0 PC774 (Lithuania)
37 0 Estonia (Estonia)
38 0 ISCII
39 0 PC-ISCII
40 0 PC APTEC
41 0 PC708
42 0 PC720
112 0 OCR-B
127 1 ISO Latin 1
127 7 ISO Latin 7 (Greek)
*/


          super.addPitch("10","EscP");
          super.addPitch("12","EscM");
          super.addPitch("15","Escg");

          super.addSpacing("6","Esc2");
          super.addSpacing("8","Esc0");
          super.addSpacing("60","EscA#CHAR#");
          super.addSpacing("180","Esc3#CHAR#");
          super.addSpacing("360","Esc+#CHAR#");


          super.defaultCharSet="437";

  }




  /**
   * move cursor to a given column
   */
   protected void moveToHPosition(int col) throws  TextPrinterException  {

      double colInches=((this.jobSetup.columnWidth*col*60)/256);

      byte[] bytesPosition=getLowHeightBytes(colInches);
      this.executeCommand(this.CMD_HORIZONTAL_POSITIONING);
      addToBuffer(bytesPosition);
   }

   /**
  * execute command with 1 parameter
  */
  protected void executeCommand(String name,String param) throws TextPrinterException {

      if (name.equals(CMD_HMI)) {
            // only ESC/P2
            byte[] bytesHMI=getLowHeightBytes(jobSetup.columnWidth*360/256);
            this.executeCommandInternal(CMD_HMI);
            addToBuffer(bytesHMI);
      }
      else if (name.equals(CMD_LEFT_MARGIN)) {
            // epson says the minimul left margin is 1
            int m=Integer.parseInt(param);
            if (m==0) m=1;
            executeCommandInternal(CMD_LEFT_MARGIN,""+ m);
      }
      else super.executeCommand(name,param);

  }

}