package patterns.config.framework.textprinter.printers;

import patterns.config.framework.textprinter.Command;
import patterns.config.framework.textprinter.TextPrinter;

//  RTextPrinter
//  Copyright (C)
//
//  Java4Less@Confluencia.net
//  All rights reserved
//
// Adquisition , use and distribution of this code is subject to restriction:
//  - You may modify the source code in order to adapt it to your needs.
//  - Redistribution of this ( or a modified version) source code is prohibited. You may only redistribute compiled versions.
//  - You may redistribute the compiled version as part of your application, not a new java component with the same purpose as this one.
//  - You may not remove this notice from the source code
//  - This notice disclaim all warranties of all material
//  - You may not copy and paste any code into external files.


/**
 * commands for IBM laser printers
 */
public class PPDSPrinter extends TextPrinter {

  public PPDSPrinter() {


          super();
          // Esc [ K Set initial printer conditions. i.e. page margins, orientation, default Font, and other operating environment variables.
          setCommand(new Command(CMD_RESET,""));
          setCommand(new Command(CMD_BOLD_ON,"EscE"));
          setCommand(new Command(CMD_BOLD_OFF,"EscF"));

          setCommand(new Command(CMD_ITALIC_ON,""));
          setCommand(new Command(CMD_ITALIC_OFF,""));

          setCommand(new Command(CMD_DOUBLESTRIKE_ON,"EscG"));
          setCommand(new Command(CMD_DOUBLESTRIKE_OFF,"EscH"));

          setCommand(new Command(CMD_UNDERLINED_ON,"Esc-"));
          setCommand(new Command(CMD_UNDERLINED_OFF,"Esc-"));

          setCommand(new Command(CMD_DOUBLEWIDE_ON,"EscW"));
          setCommand(new Command(CMD_DOUBLEWIDE_OFF,"EscW"));

          setCommand(new Command(CMD_CONDENSED_ON,""));
          setCommand(new Command(CMD_CONDENSED_OFF,""));

          setCommand(new Command(CMD_SUBSCRIPT_ON,"EscS"));
          setCommand(new Command(CMD_SUBSCRIPT_OFF,"EscT"));

          setCommand(new Command(CMD_SUPERSCRIPT_ON,"EscS"));
          setCommand(new Command(CMD_SUPERSCRIPT_OFF,"EscT"));

          // Esc C Set page length in lines or inches.
          setCommand(new Command(CMD_PAGE_LENGTH_LINES,""));
          setCommand(new Command(CMD_PAGE_WIDTH,""));

          // Esc 4 Set Top of Form.
          setCommand(new Command(CMD_TOP_MARGIN,""));
          setCommand(new Command(CMD_BOTTOM_MARGIN,""));
          // Esc X Set horizontal margins.
          setCommand(new Command(CMD_LEFT_MARGIN,""));
          setCommand(new Command(CMD_RIGHT_MARGIN,""));

          setCommand(new Command(CMD_SELECT_FONT,""));
          setCommand(new Command(CMD_SELECT_CHAR_SET,""));

          setCommand(new Command(CMD_PITCH,"#"));
          setCommand(new Command(CMD_PROPORTIONAL_ON,"EscP"));
          setCommand(new Command(CMD_PROPORTIONAL_OFF,"EscP"));

          setCommand(new Command(CMD_PORTRAIT,"Esck"));
          setCommand(new Command(CMD_LANDSCAPE,"Escl"));
          /**           Esc 0 Set 1/8 line spacing.Esc 1 Set 7/72 line spacing.
           */
          setCommand(new Command(CMD_INTERSPACE,""));
          setCommand(new Command(CMD_QUALITY,""));
          setCommand(new Command(CMD_DRAFT,""));

          super.addPitch("10","Ctrl+R");
          super.addPitch("12","Esc:");
          super.addPitch("17.1","Ctrl+O");

  }
}