package patterns.config.framework.textprinter.printers;

import patterns.config.framework.textprinter.Command;

//  RTextPrinter
//  Copyright (C)
//
//  Java4Less@Confluencia.net
//  All rights reserved
//
// Adquisition , use and distribution of this code is subject to restriction:
//  - You may modify the source code in order to adapt it to your needs.
//  - Redistribution of this ( or a modified version) source code is prohibited. You may only redistribute compiled versions.
//  - You may redistribute the compiled version as part of your application, not a new java component with the same purpose as this one.
//  - You may not remove this notice from the source code
//  - This notice disclaim all warranties of all material
//  - You may not copy and paste any code into external files.



public class ProPrinterXL24 extends ProPrinterXL {

  public ProPrinterXL24() {
      super();

      setCommand(new Command(CMD_SELECT_FONT,"Esc[#x"));
      setCommand(new Command(CMD_HORIZONTAL_POSITIONING,"Esc[#"+ ((char) 0x60)));

      super.clearFonts();
      super.addFont("Data","1");
      super.addFont("Roman","2");
      super.addFont("SansSerif","3");
      super.addFont("Courier","4");
      super.addFont("Prestige","5");
      super.addFont("Script","6");
      super.addFont("OCR-B","7");
      super.addFont("OCR-A","8");
      super.addFont("Orator-C","9");
      super.addFont("Orator","10");
      super.addFont("Data Block","11");
      super.addFont("Data Large","12");

      super.clearCharSet();
      super.addCharSet("437","Esc[;"+((char) 1)+"w","USA");
      super.addCharSet("850","Esc[;"+((char) 2)+"w","Multilingual");
      super.addCharSet("860","Esc[;"+((char) 3)+"w","Portugal");
      super.addCharSet("863","Esc[;"+((char) 4)+"w","863");
      super.addCharSet("865","Esc[;"+((char) 5)+"w","Norway");
      super.addCharSet("858","Esc[;"+((char) 6)+"w","858");
      super.addCharSet("ISO-IR-6","Esc["+((char) 1)+"w","ASCII");
      super.addCharSet("ISO-IR-69","Esc["+((char) 2)+"w","French");
      super.addCharSet("ISO-IR-21","Esc["+((char) 3)+"w","German");
      super.addCharSet("ISO-IR-4","Esc["+((char) 3)+"w","United Kingdom");
      super.addCharSet("ISO-IR-17","Esc["+((char) 6)+"w","Spanish 1");
      super.addCharSet("ISO-IR-15","Esc["+((char) 7)+"w","Italian");
      super.addCharSet("ISO-IR-17","Esc["+((char) 8)+"w","Spanish 1");
      super.addCharSet("ISO-IR-60","Esc["+((char) 10)+"w","Norwegian 1");
      super.addCharSet("ISO-IR-85","Esc["+((char) 12)+"w","Spanish 2");
  }
}