package patterns.config.framework.textprinter.printers;

import patterns.config.framework.textprinter.TextPrinter;
//  RTextPrinter
//  Copyright (C)
//
//  Java4Less@Confluencia.net
//  All rights reserved
//
// Adquisition , use and distribution of this code is subject to restriction:
//  - You may modify the source code in order to adapt it to your needs.
//  - Redistribution of this ( or a modified version) source code is prohibited. You may only redistribute compiled versions.
//  - You may redistribute the compiled version as part of your application, not a new java component with the same purpose as this one.
//  - You may not remove this notice from the source code
//  - This notice disclaim all warranties of all material
//  - You may not copy and paste any code into external files.
;



/**
 * Epson 9-PIN Esc/p printers
 */
public class TestPrinter extends ESCP2Printer {

  public TestPrinter() {
      super();

  resetCommand(TextPrinter.CMD_PORTRAIT);
  resetCommand(TextPrinter.CMD_PROPORTIONAL_OFF);
  resetCommand(TextPrinter.CMD_TOP_MARGIN);
  resetCommand(TextPrinter.CMD_BOTTOM_MARGIN);
  resetCommand(TextPrinter.CMD_PAGE_LENGTH_LINES);
  resetCommand(TextPrinter.CMD_LEFT_MARGIN);
  resetCommand(TextPrinter.CMD_RIGHT_MARGIN);
  resetCommand(TextPrinter.CMD_QUALITY);

  super.clearSpacing();
  super.addSpacing("6","Esc2");
  super.addSpacing("8","Esc0");
  super.addSpacing("180","Esc3#CHAR#");
  super.addSpacing("360","Esc+#CHAR#");

  //this.debug=true;
  super.defaultCharSet="850";
  }
}