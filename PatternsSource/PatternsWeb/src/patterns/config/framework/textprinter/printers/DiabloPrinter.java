package patterns.config.framework.textprinter.printers;

import patterns.config.framework.textprinter.Command;
import patterns.config.framework.textprinter.TextPrinter;

//  RTextPrinter
//  Copyright (C)
//
//  Java4Less@Confluencia.net
//  All rights reserved
//
// Adquisition , use and distribution of this code is subject to restriction:
//  - You may modify the source code in order to adapt it to your needs.
//  - Redistribution of this ( or a modified version) source code is prohibited. You may only redistribute compiled versions.
//  - You may redistribute the compiled version as part of your application, not a new java component with the same purpose as this one.
//  - You may not remove this notice from the source code
//  - This notice disclaim all warranties of all material
//  - You may not copy and paste any code into external files.


public class DiabloPrinter extends TextPrinter {

  public DiabloPrinter() {


          super();
          /**
           * Esc ? Enable auto Carriage Return.
           Esc ! Disable auto Carriage Return.
           */
          setCommand(new Command(CMD_RESET,""));
          setCommand(new Command(CMD_BOLD_ON,"EscO"));
          setCommand(new Command(CMD_BOLD_OFF,"Esc&"));

          setCommand(new Command(CMD_ITALIC_ON,""));
          setCommand(new Command(CMD_ITALIC_OFF,""));

          setCommand(new Command(CMD_DOUBLESTRIKE_ON,""));
          setCommand(new Command(CMD_DOUBLESTRIKE_OFF,""));

          setCommand(new Command(CMD_UNDERLINED_ON,"EscE"));
          setCommand(new Command(CMD_UNDERLINED_OFF,"EscR"));

          setCommand(new Command(CMD_DOUBLEWIDE_ON,""));
          setCommand(new Command(CMD_DOUBLEWIDE_OFF,""));

          setCommand(new Command(CMD_CONDENSED_ON,""));
          setCommand(new Command(CMD_CONDENSED_OFF,""));

          setCommand(new Command(CMD_SUBSCRIPT_ON,""));
          setCommand(new Command(CMD_SUBSCRIPT_OFF,""));

          setCommand(new Command(CMD_SUPERSCRIPT_ON,""));
          setCommand(new Command(CMD_SUPERSCRIPT_OFF,""));

          setCommand(new Command(CMD_PAGE_LENGTH_LINES,"EscCtrl+L#"));
          setCommand(new Command(CMD_PAGE_WIDTH,""));

/**
 Esc Ctrl+K (n) Absolute vertical to print line (n).
Esc Ctrl+I (n) Absolute horizontal Tab Stop to print column
Esc T Set top page Margin at current position.
Esc L Set bottom page Margin at current position
Esc 9 Set left Margin at current position.
Esc 0 Set right Margin at current position
 */
          setCommand(new Command(CMD_TOP_MARGIN,""));
          setCommand(new Command(CMD_BOTTOM_MARGIN,""));
          setCommand(new Command(CMD_LEFT_MARGIN,""));
          setCommand(new Command(CMD_RIGHT_MARGIN,""));

          setCommand(new Command(CMD_SELECT_FONT,""));
          setCommand(new Command(CMD_SELECT_CHAR_SET,""));
          setCommand(new Command(CMD_PITCH,""));
          setCommand(new Command(CMD_PROPORTIONAL_ON,"EscP"));
          setCommand(new Command(CMD_PROPORTIONAL_OFF,"EscQ"));

          setCommand(new Command(CMD_PORTRAIT,""));
          setCommand(new Command(CMD_LANDSCAPE,""));
          setCommand(new Command(CMD_INTERSPACE,""));
          setCommand(new Command(CMD_QUALITY,""));
          setCommand(new Command(CMD_DRAFT,""));

  }
}