package patterns.config.framework.textprinter;
//  RTextPrinter
//  Copyright (C)
//
//  Java4Less@Confluencia.net
//  All rights reserved
//
// Adquisition , use and distribution of this code is subject to restriction:
//  - You may modify the source code in order to adapt it to your needs.
//  - Redistribution of this ( or a modified version) source code is prohibited. You may only redistribute compiled versions.
//  - You may redistribute the compiled version as part of your application, not a new java component with the same purpose as this one.
//  - You may not remove this notice from the source code
//  - This notice disclaim all warranties of all material
//  - You may not copy and paste any code into external files.

/**
 * this class stores information about a chunck of text to be printed.
 */
public class TextProperties {

  /**
   * is it italic?
   */
  public boolean italic=false;
  /**
   * is it bold?
   */
  public boolean bold=false;
  /**
   * is it underlined?
   */
 public boolean underlined=false;

  /**
   * use subscript
   */
 public boolean subscript=false;


  /**
   * use superscript
   */
 public boolean superscript=false;

  /**
   * use double strike?
   */
  public boolean doubleStrike=false;

  /**
   * use double wide?
   */
  public boolean doubleWide=false;

  /**
   * name of the font to use
   */
   public String fontName="";

   /// this properties are normally set only in the JObProperties object
   ///--------------------------------------------------------------------

    /**
     * Size of the characters 10, 12 or 15 (characters/inch). This has an effect on the number of columns the page. Default value is -1 will use the printer's default value. You can also let TexPrinter calculate the pitch based on the desired number of colums and the page width.
     */
     public double pitch=-1;

     /**
      * proportional printing. If false all characters will have the same fixed width.
      */
      public boolean proportional=false;

      /**
       * use condensed charaters
       */
      public boolean condensed=false;

         ///--------------------------------------------------------------------

   /**
    * character set to use
    */
   public String characterSet="";

   /**
    * create a clone
    */
    public TextProperties getClone() {
      TextProperties p=new TextProperties();
      p.bold=this.bold;
      p.characterSet=this.characterSet;
      p.doubleStrike=this.doubleStrike;
      p.doubleWide=this.doubleWide;
      p.fontName=this.fontName;
      p.italic=this.italic;
      p.subscript=this.subscript;
      p.superscript=this.superscript;
      p.underlined=this.underlined;
      p.pitch=this.pitch;
      p.proportional=this.proportional;
      p.condensed=this.condensed;

      return p;
    }


}