package patterns.config.framework.textprinter;
//  RTextPrinter
//  Copyright (C)
//
//  Java4Less@Confluencia.net
//  All rights reserved
//
// Adquisition , use and distribution of thtis code is subject to restriction:
//  - You may modify the source code in order to adapt it to your needs.
//  - Redistribution of this ( or a modified version) source code is prohibited. You may only redistribute compiled versions.
//  - You may redistribute the compiled version as part of your application, not a new java component with the same purpose as this one.
//  - You may not remove this notice from the source code
//  - This notice disclaim all warranties of all material
//  - You may not copy and paste any code into external files.
import java.awt.Font;
import java.io.UnsupportedEncodingException;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import patterns.config.framework.charsets.CharSetManager;
import patterns.config.framework.charsets.Charset;

public abstract class TextPrinter {

protected final char ESC=(char) 27;

protected final char LF=(char)  10;
protected final char CR=(char) 13;
protected final char FF=(char) 12;

/**
 * commands
 */
protected Hashtable commands=new Hashtable();

/**
 * commands, these values must be set by the subclasses
 */
protected static String CMD_RESET="CMD_RESET";
protected static String CMD_BOLD_ON="CMD_BOLD_ON";
protected static String CMD_BOLD_OFF="CMD_BOLD_OFF";
protected static String CMD_HORIZONTAL_POSITIONING="CMD_HORIZONTAL_POSITIONING"; // position cursor at a given column
protected static String CMD_ITALIC_ON="CMD_ITALIC_ON";
protected static String CMD_ITALIC_OFF="CMD_ITALIC_OFF";
protected static String CMD_DOUBLESTRIKE_ON="CMD_DOUBLESTRIKE_ON";
protected static String CMD_DOUBLESTRIKE_OFF="CMD_DOUBLESTRIKE_OFF";
protected static String CMD_UNDERLINED_ON="CMD_UNDERLINED_ON";
protected static String CMD_UNDERLINED_OFF="CMD_UNDERLINED_OFF";
protected static String CMD_DOUBLEWIDE_ON="CMD_DOUBLEWIDE_ON";
protected static  String CMD_DOUBLEWIDE_OFF="CMD_DOUBLEWIDE_OFF";
protected static String CMD_CONDENSED_ON="CMD_CONDENSED_ON";
protected static String CMD_CONDENSED_OFF="CMD_CONDENSED_OFF";
protected static String CMD_SUBSCRIPT_ON="CMD_SUBSCRIPT_ON";
protected static String CMD_SUBSCRIPT_OFF="CMD_SUBSCRIPT_OFF";
protected static String CMD_SUPERSCRIPT_ON="CMD_SUPERSCRIPT_ON";
protected static String CMD_SUPERSCRIPT_OFF="CMD_SUPERSCRIPT_OFF";
protected static String CMD_PAGE_LENGTH_LINES="CMD_PAGE_LENGTH_LINES";
protected static String CMD_PAGE_LENGTH_INCHES="CMD_PAGE_LENGTH_INCHES";
protected static String CMD_PAGE_WIDTH="CMD_PAGE_WIDTH";
protected static String CMD_TOP_MARGIN="CMD_TOP_MARGIN";
protected static String CMD_BOTTOM_MARGIN="CMD_BOTTOM_MARGIN";
protected static String CMD_LEFT_MARGIN="CMD_LEFT_MARGIN";
protected static String CMD_RIGHT_MARGIN="CMD_RIGHT_MARGIN";
protected static String CMD_SELECT_FONT="CMD_SELECT_FONT";
protected static String CMD_SELECT_CHAR_SET="CMD_SELECT_CHAR_SET";
protected static String CMD_PITCH="CMD_PITCH";
protected static String CMD_PROPORTIONAL_ON="CMD_PROPORTIONAL_ON";
protected static String CMD_PROPORTIONAL_OFF="CMD_PROPORTIONAL_OFF";
protected static String CMD_PORTRAIT="CMD_PORTRAIT";
protected static String CMD_LANDSCAPE="CMD_LANDSCAPE";
protected static String CMD_INTERSPACE="CMD_INTERSPACE";
protected static String CMD_QUALITY="CMD_QUALITY";
protected static String CMD_DRAFT="CMD_DRAFT";
protected static String CMD_HMI="CMD_HMI";
protected static String CMD_PAPER_SIZE="CMD_PAPER_SIZE";
protected static String CMD_RESOLUTION="CMD_RESOLUTION";


/**
 * conversion tables
 */
protected Hashtable fontValues=new Hashtable();
protected Hashtable charSetValues=new Hashtable();
protected Hashtable pitchValues=new Hashtable();
protected Hashtable interspacingValues=new Hashtable();
protected Hashtable paperSizeValues=new Hashtable();
protected Hashtable resolutionValues=new Hashtable();

protected Hashtable cmdDependencies=new Hashtable();

protected boolean bLFfterCR=true;


protected char[][] pageData=null;
protected TextProperties[][] pageDataProperties=null;

/**
 * do not print trailing spaces
 */
public boolean rtrimLines=false;

/**
 * job setup
 */
protected JobProperties jobSetup= null;

/**
 * current position
 */
protected int currentRow=0;

/**
 * current position
 */
protected int currentCol=0;

/**
 * parallel port
 */
protected PrinterPort port=null;

/**
 * size of buffer, bytes will be sent to the port only when the buffer is full
 */
protected int bufferSize=100;

/**
 * number of chars in the buffer
 */
protected int bufferPointer=0;

/**
 * buffer
 */
protected byte[] buffer=new byte[bufferSize+1];

/**
 * implement the margin per software since the printer will not do it
 */
public boolean leftMarginPerSoftware=false;
/**
 * implement the margin per software since the printer will not do it
 */
public boolean topMarginPerSoftware=false;

/**
 * issue the FF command when bottom margin reached?
 */
public boolean feedFormPerSoftware=true;

/**
 */

/**
 * horizontal line character (for line printing).
 */
public char hLineChar='-';

/**
 * vertical line character (for line printing).
 */
public char vLineChar='|';

/**
 * vertical line connected to a horizontal line on the right size
 */
public char vrLineChar='+';

/**
 * vertical line connected to a horizontal line on the left size
 */
public char vlLineChar='+';

/**
 * horizontal line connected to a vertical line on the top
 */
public char htLineChar='+';

/**
 * horizontal line connected to a  vertical  line on the bottom
 */
public char hbLineChar='+';

/**
 * crossing lines characters
 */
public char crossLineChar='+';

/**
 * top left corner character (for line printing).
 */
public char tlCornerChar='+';
/**
 * top right corner character (for line printing).
 */
public char trCornerChar='+';
/**
 * bottom left corner character (for line printing).
 */
public char blCornerChar='+';
/**
 * bottom right corner character (for line printing).
 */
public char brCornerChar='+';

/**
 * character set used to draw lines. If null it uses the current set  (for line printing).
 */
public String linesCharSet=null;

/**
 * convert unicode to the printers char set
 */
public boolean performCharSetConversion=true;

/**
 * printer's default charset
 */
public String defaultCharSet="";

/**
 * enable debug output
 */
public boolean debug=false;

/**
 * calculate character size based on the desired number of columns?
 */
public boolean calculatePitchAndHMI=false;

/**
 * calculate line spacing based on the number of lines in the page
 */
public boolean calculateLineSpacing=false;


/**
 * char set converter
 */
protected static CharSetManager charSetManager=null;

/**
 * current properties
 */
TextProperties currentProp=null;

/**
 * character used for empty positions
 */
char emptyCharater=0;

/**
 * current page
 */
private int page=0;

private boolean d() {return false;}

/**
 * initializes class
 */
protected void init(){

}

/**
 * add command to the list of commands supported by the printer
 */
public void setCommand(Command cmd) {
    if (!this.commands.containsKey(cmd.getName())) {
        this.commands.put(cmd.getName(),cmd);
    }
}

/**
 * delete command
 */
public void resetCommand(String cmd){

if (this.commands.containsKey(cmd)) this.commands.remove(cmd);
}


/**
 * get command by name
 */
public Command getCommand(String name) {
    if (this.commands.containsKey(name)) return (Command) this.commands.get(name);
    else return null;
}



/**
 * execute command with 1 parametert
 */
 protected void executeCommand(String name,String param) throws TextPrinterException {


   executeCommandInternal(name,param);

 }

/**
 * execute command with 1 parametert
 */
  protected final void executeCommandInternal(String name,String param) throws TextPrinterException {


    Command cmd=getCommand(name);

    if (cmd!=null) {
        addToBuffer(replaceParameter(cmd.getCommand(),param));
        if (debug) System.out.println(cmd.getName() + ": "+ param);

        this.executeDependencies(name);
    }

 }


/**
 * execute command
 */
 protected void executeCommand(String name) throws TextPrinterException {


    executeCommandInternal(name);

 }

  protected final void executeCommandInternal(String name) throws TextPrinterException {


    Command cmd=getCommand(name);

    if (cmd!=null) {
        addToBuffer(cmd.getCommand());
        if (debug) System.out.println(cmd.getName());

        this.executeDependencies(name);
    }

 }


/**
 * size of buffer, bytes will be sent to the port only when the buffer is full
 */
public void setBufferSize(int s) {
  byte[] newBuffer=new byte[bufferSize+1];
  bufferSize=s;

  for (int i=0;((i<buffer.length) && (i<newBuffer.length));i++) newBuffer[i]=buffer[i];

  buffer=newBuffer;
}

/**
 * size of buffer, bytes will be sent to the port only when the buffer is full
 */
public int getBufferSize() {
  return bufferSize;
}

/**
 *  Some printers need a line feed after every carriage return, other don't
 */
 public void setLFafterCR(boolean b) {
  bLFfterCR=b;
  }

   /**
  *  print char given position using the text properties object
  */
 private void printString(char c,int row, int col, TextProperties t) {
       printString(""+c,row,col,t);
 }

 /**
  *  print at the given position using the text properties object
  */
 public void printString(String s,int row, int col, TextProperties t) {


    if (t==null) t=new TextProperties();
    TextProperties tCloned=t.getClone();

    for (int i=0;i<s.length();i++) {
      // do not write beyond the right margin
      if ((col+i)>(jobSetup.cols-jobSetup.rightMargin)) break;

      // check for out of bounds index
     if ((row<pageData.length) && ((col+i)<pageData[0].length))
     try {
            pageData[row][col+i]=s.charAt(i);
            pageDataProperties[row][col+i]=tCloned;
      } catch (Exception e) {
      e.printStackTrace();
      }
    }


 }

 /**
  * print at the given position using a java.awt.Font as TextProperties substitute
  */
 public void printString(String s,int row ,int col, Font font) {
      TextProperties p=new TextProperties();

      p.bold=font.isBold();
      p.italic=font.isItalic();
      p.fontName=font.getFontName();
      this.printString(s,row,col,p);

 }



  /**
  *  new line
  */
 public void newLine()  throws TextPrinterException {
        currentRow=currentRow+1;
        currentCol=this.jobSetup.leftMargin;
        if (currentRow>(jobSetup.rows-jobSetup.bottomMargin)) finishPage();
 }

 /**
  * does this printer has a command for horizontal positioning?
  */
 protected boolean supportsCommand(String scmd) {
    Command cmd=this.getCommand(scmd);

    if (cmd==null) return false;

    if (cmd.getCommand().length()==0) return false;

    return true;
 }

  /**
  *  new page
  */
 public void newPage()  throws TextPrinterException {
        finishPage();
 }

 /**
  * print a horizontal line
  */
 public void printHorizontalLine(int row, int col1, int col2) {
    TextProperties prop=new TextProperties();

    if (this.linesCharSet!=null)
      if (this.linesCharSet.length()>0) {
          prop.characterSet=this.linesCharSet;
      }



    if (col2<col1) {
      int tmp=col2;
      col2=col1;
      col1=tmp;
    }

    for (int i=col1;i<=col2;i++) this.setHChar(row,i,(i==col1),(i==col2),prop);

 }

 /**
  * print a vertical line
  */
 public void printVerticalLine(int col, int row1, int row2) {

   TextProperties prop=new TextProperties();

    if (this.linesCharSet!=null)
      if (this.linesCharSet.length()>0) {
          prop.characterSet=this.linesCharSet;
      }



    if (row2<row1) {
      int tmp=row2;
      row2=row1;
      row1=tmp;
    }

    for (int i=row1;i<=row2;i++)  this.setVChar(i,col,(i==row1),(i==row2),prop);


 }

 /**
  * print a rectangle.
  */
 public void printRectangle(int x1,int y1, int x2, int y2) {

    TextProperties prop=new TextProperties();

    if (this.linesCharSet!=null)
      if (this.linesCharSet.length()>0) {
          prop.characterSet=this.linesCharSet;
      }

    // horizontal lines
    this.printHorizontalLine(y1,x1,x2);
    this.printHorizontalLine(y2,x1,x2);

    // vertical lines
    this.printVerticalLine(x1,y1,y2);
    this.printVerticalLine(x2,y1,y2);


    // corners
    //this.printString(""+this.tlCornerChar,y1,x1,prop);
    //this.printString(""+this.trCornerChar,y1,x2,prop);
    //this.printString(""+this.blCornerChar,y2,x1,prop);
    //this.printString(""+this.brCornerChar,y2,x2,prop);

 }

  /**
  *  print at the current position using the default text properties object
  */
public void printString(String s)  throws TextPrinterException {
    this.printString(s,this.getDefaultTextProperties());
 }

 /**
  *  print at the current position using the text properties object
  */
 public void printString(String s, TextProperties t)  throws TextPrinterException {
        this.printString(s,currentRow,currentCol,t);
        currentCol=currentCol+s.length();
        if (currentCol>(jobSetup.cols-jobSetup.rightMargin)) currentRow++;
        if (currentRow>(jobSetup.rows-jobSetup.bottomMargin)) finishPage();

 }

 /**
  * print at the current position using a java.awt.Font as TextProperties substitute
  */
 public void printString(String s, Font font)  throws TextPrinterException {
        this.printString(s,currentRow,currentCol,font);
        currentCol=currentCol+s.length();
        if (currentCol>=(jobSetup.cols-jobSetup.rightMargin)) currentRow++;
        if (currentRow>(jobSetup.rows-jobSetup.bottomMargin)) finishPage();
 }


   /**
   * add char to the output buffer
   */
  protected void writeCharToBuffer(char c,TextProperties newProp)  throws TextPrinterException {

          if (newProp==null) newProp=currentProp.getClone();
          else newProp=newProp.getClone();

          setJobDefaults(newProp);

          // this can modify the new char set
          byte[] mappedBytes=null;
          try {
               mappedBytes=this.charMapping(c,newProp);
          } catch (UnsupportedEncodingException e) {throw new TextPrinterException(e.getMessage());}


          processCommandsForTextProperties(currentProp,newProp);


          String curFont=currentProp.fontName;
          String curCharSet=currentProp.characterSet;

          currentProp=newProp;
          // if the new font is empty keep using the same as before
          if (currentProp.fontName.length()==0) currentProp.fontName=curFont;
          // if the charset is empty keep using the same as before
          if (currentProp.characterSet.length()==0) currentProp.characterSet=curCharSet;

          addToBuffer(mappedBytes); // write now
  }

 /**
  * finish page, send to printer now
  */
  protected void finishPage() throws TextPrinterException {

  if ((d()) && (page>3)) return;

  page++;


   byte[] b=new byte[1];
   boolean lastCharWasEmpty=false;

   // implement top margin
   if (topMarginPerSoftware)
          for (int h=0;h<jobSetup.topMargin;h++) {
            // new line
            b[0]=(byte) this.CR;
            addToBuffer(b);
            if (this.bLFfterCR) {
                b[0]=(byte) this.LF;
                addToBuffer(b);
            }
    }

   if (d()) {
        int r=pageData.length/2;
        int c=pageData[0].length/2;
        TextProperties tp=this.getDefaultTextProperties();
        pageDataProperties[r][c]=tp;
        pageDataProperties[r][c+1]=tp;
        pageDataProperties[r][c+2]=tp;
        pageDataProperties[r][c+3]=tp;
        pageData[r][c]='D';
        pageData[r][c+1]='E';
        pageData[r][c+2]='M';
        pageData[r][c+3]='O';
   }

   for (int i=jobSetup.topMargin;i<jobSetup.rows-jobSetup.bottomMargin;i++) {
     lastCharWasEmpty=false;
     // implement left margin
      char space=' '; // print space
      if (topMarginPerSoftware)
          for (int h=0;h<jobSetup.leftMargin;h++) writeCharToBuffer(space,null);


      int maxCol=jobSetup.cols-jobSetup.rightMargin;
      int minCol=jobSetup.leftMargin;

      if (this.rtrimLines)
         for (int j=maxCol-1;j>=minCol;j--) {
               if (!((pageDataProperties[i][j]==null) || (pageData[i][j]==this.emptyCharater))) {
                  maxCol=j+1;

                  break;
               }
         if (j==minCol) {
             maxCol=minCol;
             break;
         }
      }

      for (int j=minCol;j<maxCol;j++) {

       if ((pageDataProperties[i][j]==null) || (pageData[i][j]==this.emptyCharater)) {

          // if we do not use proportional, use white spaces for columns positioning
          if ((!this.jobSetup.proportional) || (!this.supportsCommand(TextPrinter.CMD_HORIZONTAL_POSITIONING)))
          writeCharToBuffer(space,this.getDefaultTextProperties());

          if ((this.jobSetup.proportional) && (this.supportsCommand(TextPrinter.CMD_HORIZONTAL_POSITIONING)))
                    lastCharWasEmpty=true;

        }
        else {

          if (lastCharWasEmpty) this.moveToHPosition(j); // move to given column if we use proportional fonts

          TextProperties newProp=pageDataProperties[i][j];
          writeCharToBuffer(pageData[i][j],newProp);
          lastCharWasEmpty=false;
          }
      }

      // new line
      b[0]=(byte) this.CR;
      addToBuffer(b);
      if (this.bLFfterCR) {
        b[0]=(byte) this.LF;
        addToBuffer(b);
      }
   }

   // next page
   b[0]=(byte) this.FF;
   if (!feedFormPerSoftware) {
      if (debug) System.out.println("\nAdding FF command to the buffer\n");
      addToBuffer(b);
   }
   else
    if (debug) System.out.println("\nHardware FF disabled\n");

   resetPageData();
  }



  /**
   * move cursor to a given column
   */
   protected void moveToHPosition(int col) throws  TextPrinterException  {
      this.executeCommand(TextPrinter.CMD_HORIZONTAL_POSITIONING,""+col);
   }

  /**
   * issues command for current text properties
   */
   protected void processCommandsForTextProperties(TextProperties current,TextProperties next) throws TextPrinterException {

     // disable bold
     if ((current.bold) && (!next.bold))
          this.executeCommand(TextPrinter.CMD_BOLD_OFF);

     // enable bold
     if ((!current.bold) && (next.bold)) {
          this.executeCommand(TextPrinter.CMD_BOLD_ON);
      }

     // disable italic
     if ((current.italic) && (!next.italic)) {
          this.executeCommand(TextPrinter.CMD_ITALIC_OFF);
      }

     // enable italic
     if ((!current.italic) && (next.italic)) {
          this.executeCommand(TextPrinter.CMD_ITALIC_ON);
      }

     // disable  underlined
     if ((current.underlined) && (!next.underlined)) {
     this.executeCommand(TextPrinter.CMD_UNDERLINED_OFF);
    }

     // enable underlined
     if ((!current.underlined) && (next.underlined)) {
      this.executeCommand(TextPrinter.CMD_UNDERLINED_ON);
      }

     // disable double strike
     if ((current.doubleStrike) && (!next.doubleStrike)) {
      this.executeCommand(TextPrinter.CMD_DOUBLESTRIKE_OFF);
    }

     // enable double strike
     if ((!current.doubleStrike) && (next.doubleStrike)) {
     this.executeCommand(TextPrinter.CMD_DOUBLESTRIKE_ON);

     }

     // disable double wide
     if ((current.doubleWide) && (!next.doubleWide)) {
      this.executeCommand(TextPrinter.CMD_DOUBLEWIDE_OFF);

      }

     // enable double wide
     if ((!current.doubleWide) && (next.doubleWide)) {
      this.executeCommand(TextPrinter.CMD_DOUBLEWIDE_ON);


      }

     // disable superscript
     if ((current.superscript) && (!next.superscript)) {
      this.executeCommand(TextPrinter.CMD_SUPERSCRIPT_OFF);

      }

     // enable superscript
     if ((!current.superscript) && (next.superscript)) {
      this.executeCommand(TextPrinter.CMD_SUPERSCRIPT_ON);

      }

     // disable subscript
     if ((current.subscript) && (!next.subscript)) {
      this.executeCommand(TextPrinter.CMD_SUBSCRIPT_OFF);

      }

     // enable subcript
     if ((!current.subscript) && (next.subscript)) {
      this.executeCommand(TextPrinter.CMD_SUBSCRIPT_ON);

    }

    if ((!current.proportional) && (next.proportional)) {
          this.executeCommand(TextPrinter.CMD_PROPORTIONAL_ON);

    }

    if ((current.proportional) && (!next.proportional)) {
          this.executeCommand(TextPrinter.CMD_PROPORTIONAL_OFF);

     }

    if ((!current.condensed) && (next.condensed)) {
          this.executeCommand(TextPrinter.CMD_CONDENSED_ON);

    }

    if ((current.condensed) && (!next.condensed)) {
          this.executeCommand(TextPrinter.CMD_CONDENSED_OFF);

     }

     processCommandsForPitch(""+current.pitch,""+next.pitch);

     processCommandsForFont(current.fontName,next.fontName);

     if (next.characterSet.length()==0) next.characterSet=this.defaultCharSet;
     if (current.characterSet.length()==0) current.characterSet=this.defaultCharSet;
     processCommandsForCharacterSet(current.characterSet,next.characterSet);


   }

   /**
    * a command dependency
    */
    protected void addDependency(String mainCommand,String dependent) {
          Vector dep=null;

          if (this.cmdDependencies.containsKey(mainCommand)) dep=(Vector) this.cmdDependencies.get(mainCommand);

          if (dep==null) {
              dep=new Vector();
              this.cmdDependencies.put(mainCommand,dep);
          }

          dep.addElement(dependent);

    }

   /**
    * process command dependencies. These are commands that should be reissued after a given command has been executed
    */
    protected void executeDependencies(String cmdName) throws TextPrinterException {

          Vector dep=null;

          if (this.cmdDependencies.containsKey(cmdName)) dep=(Vector) this.cmdDependencies.get(cmdName);

          if (dep==null) return;

          if (debug) System.out.println("Processing dependencies");

          for (int i=0;i<dep.size();i++){
               String depCommand=(String) dep.elementAt(i);

               if (depCommand.equals(TextPrinter.CMD_HMI))
                    if (this.jobSetup.columnWidth>0) executeCommand(TextPrinter.CMD_HMI,""+this.jobSetup.columnWidth);

          }

          if (debug) System.out.println("End of dependencies");
    }

   /**
    * look up value in map table.
    */
    protected String mapTableLookup(Hashtable table, String value) {

      if (table==null) return null;
      //System.out.println("Looking for " + value);
      java.util.Enumeration e=table.elements();
      while (e.hasMoreElements()) {
          CommandParamValue p=(CommandParamValue) e.nextElement();
          //System.out.println("element " + p.name);
          if (p.name.equals(value)) return  p.value;
          if (p.name.equals("*")) return  p.value;
      }

      return null;
   }

   /**
    * replace parameter (char #) in command
    */
    protected String replaceParameter(String command,String value) throws TextPrinterException {


      int p=command.indexOf("#CHAR#");
      if (p>=0) {
        if (value.length()==0) value="0";

        byte[] b=new byte[1];
        b[0]=(byte) ( 0xff & (int) Math.floor(Double.parseDouble(value)));
//        if (b[0]<0) b[0]=256+b[0];

      try {
        command=command.substring(0,p)+ new String(b,jobSetup.javaEncoding) +command.substring(p+6);
        } catch (Exception e) {throw new TextPrinterException(e.getMessage());}
      }

      p=command.indexOf("#");
      if (p>=0) {
        command=command.substring(0,p)+value+command.substring(p+1);
      }


      return command;
    }


   /**
    * enable new font
    */
    protected void processCommandsForFont(String currentFont,String newFont)  throws TextPrinterException  {

      if (!currentFont.equals(newFont))
        if (!newFont.equals("")) {
          String mappedFont=mapTableLookup(this.fontValues,newFont);
          if (mappedFont!=null) {
              executeCommand(TextPrinter.CMD_SELECT_FONT,mappedFont);

            }
      }
    }

   /**
    * set paper size
    */
    protected void processCommandsPaperSize(PaperSize paper)  throws TextPrinterException  {


          String mappedPaperName=mapTableLookup(this.paperSizeValues,paper.getName());
          if (mappedPaperName!=null) {
              executeCommand(TextPrinter.CMD_PAPER_SIZE,mappedPaperName);
            }

    }

   /**
    * set resolution
    */
    protected void processCommandsResolution(int resolution)  throws TextPrinterException  {


          String mappedRes=mapTableLookup(this.resolutionValues,""+resolution);
          if (mappedRes!=null) {
              executeCommand(TextPrinter.CMD_RESOLUTION,mappedRes);
            }

    }


  /**
   * get best line spacing for a given page height and the number of lines
   * spacing will have the format "1/8" , "2/72" .....
   */
   protected String getBestLineSpacing(int lines, double pageHeight) {
     int bestLines=1000;
     String bestSpacing="";

     // iterate of the available spacings
     java.util.Enumeration e=this.interspacingValues.elements();
     while (e.hasMoreElements()) {
          CommandParamValue p=(CommandParamValue) e.nextElement();
          if (Integer.parseInt(p.name)>=60) {
              for (int j=1;j<Integer.parseInt(p.name);j++) {
                    String sp=""+j+"/"+p.name;
                    int tmp=getLinesForSpacing(sp,pageHeight);

                    // perfect spacing found
                    if (tmp==lines) return sp;

                    if (tmp>=lines)
                      if ((bestLines-lines)>(tmp-lines)) {
                          bestLines=tmp;
                          bestSpacing=sp;
                    }
              }
          }
          else {
              String sp="1/"+p.name;
              int tmp=getLinesForSpacing(sp,pageHeight);

              // perfect spacing found
               if (tmp==lines) return sp;

               if (tmp>=lines)
                      if ((bestLines-lines)>(tmp-lines)) {
                          bestLines=tmp;
                          bestSpacing=sp;
               }
          }
     }

      if (debug)
        System.out.println("Best line spacing:  " + bestSpacing);


    return bestSpacing;

   }

   /**
    * get number of lines we get using this line spacing. Will return -1 if the number of lines is not integer
    * spacing will have the format "1/8" , "2/72" .....
    */
    protected int getLinesForSpacing(String spacing,double pageHeight) {
        int p=spacing.indexOf("/");
        String a=spacing.substring(0,p);
        String b=spacing.substring(p+1);
        int ai=Integer.parseInt(a);
        int bi=Integer.parseInt(b);

        double lines=(pageHeight*bi)/ai;

        //if (lines==Math.floor(lines)       )
        return (int) lines;

        // we do not get an integer number of lines using this spacing.
        //return -1;
    }


   /**
    * enable new character set
    */
    protected void processCommandsForCharacterSet(String currentSet,String newSet)  throws TextPrinterException {
      if (!currentSet.equals(newSet))
        if (!newSet.equals("")) {
          String mappedSet=mapTableLookup(this.charSetValues,newSet);
          if (mappedSet!=null) {
            executeCommand(TextPrinter.CMD_SELECT_CHAR_SET,mappedSet);

          }
      }
    }

   /**
    * enable new pitch
    */
    protected void processCommandsForLineSpacing(String spacing)  throws TextPrinterException  {

        int p=spacing.indexOf("/");
        String a=spacing.substring(0,p);
        String b=spacing.substring(p+1);

        String mappedValue=mapTableLookup(this.interspacingValues,b);
        if (mappedValue!=null) {
             mappedValue=this.replaceParameter(mappedValue,a);
             executeCommand(TextPrinter.CMD_INTERSPACE,mappedValue);
        }
        else {
            try {
              // iterate of the available spacings

              java.util.Enumeration e=this.interspacingValues.elements();
              while (e.hasMoreElements()) {
                CommandParamValue cmd=(CommandParamValue) e.nextElement();
                if (Integer.parseInt(cmd.name)>=60) {
                    double tmp=(Double.parseDouble(cmd.name)/Double.parseDouble(b));
                    if (tmp==Math.floor(tmp)) {
                          mappedValue=this.replaceParameter(cmd.value,""+((int) tmp));
                          executeCommand(TextPrinter.CMD_INTERSPACE,mappedValue);
                          return;
                    }
                }
              }

              } catch  (Exception e) {e.printStackTrace();}
        } //else

    }


   /**
    * enable new pitch
    */
    protected void processCommandsForPitch(String currentPitch,String newPitch)  throws TextPrinterException  {

      if (!currentPitch.equals(newPitch))
        if (!newPitch.equals("")) {

          String mappedPitch=getBestPitchCommand(newPitch);
          if (mappedPitch==null) return;

          mappedPitch=this.replaceParameter(mappedPitch,newPitch);
          executeCommand(TextPrinter.CMD_PITCH,mappedPitch);

          }

    }

    /**
     * get best pitch from table, either a perfect match or the nearest value which is higher than the required pitch
     */
     protected String getBestPitchCommand(String pitch) {
          int bestPitch=0;
          String bestPitchCommand=null;
          double iPitch=Double.parseDouble(pitch);

          java.util.Enumeration e=this.pitchValues.elements();
          while (e.hasMoreElements()) {
              CommandParamValue p=(CommandParamValue) e.nextElement();

              if (p.name.equals("*")) return p.value;
              if (Double.parseDouble(p.name)==iPitch) return p.value;

              if ((Double.parseDouble(p.name)>iPitch) &&
              (Double.parseDouble(p.name)<bestPitch) ) {
                    bestPitch=Integer.parseInt(p.name);
                    bestPitchCommand=p.value;
                  }

          }

          return bestPitchCommand;

     }


    /**
     * set up job, page size, margins, etc...
     */
    protected void processCommandsForJob() throws TextPrinterException {

        executeCommand(TextPrinter.CMD_RESET);

        addToBuffer(this.jobSetup.userResetCommands);
        if (debug) System.out.println("User reset command: "+ this.jobSetup.userResetCommands);

        processCommandsResolution(this.jobSetup.resolution);

        // paper size
        if (this.jobSetup.paperSize!=null) this.processCommandsPaperSize(this.jobSetup.paperSize);


        // orientation
        if (this.jobSetup.landscape) {
            executeCommand(TextPrinter.CMD_LANDSCAPE);
        } else {
            executeCommand(TextPrinter.CMD_PORTRAIT);
        }

        if (this.jobSetup.proportional)
          this.executeCommand(TextPrinter.CMD_PROPORTIONAL_ON);

        if (!this.jobSetup.proportional)
          this.executeCommand(TextPrinter.CMD_PROPORTIONAL_OFF);

        // page width and length
       //executeCommand(TextPrinter.CMD_PAGE_WIDTH, ""+this.jobSetup.cols);

       if (this.calculateLineSpacing) this.calculateLineSpacing(jobSetup.rows);
       if (this.calculatePitchAndHMI) this.calculateColumnWidth(jobSetup.cols);

        // font size
        if (this.jobSetup.pitch>0) this.processCommandsForPitch("",""+this.jobSetup.pitch);
        // columns width
        if (this.jobSetup.columnWidth>0) executeCommand(TextPrinter.CMD_HMI,""+this.jobSetup.columnWidth);

        // interspacing
        if (this.jobSetup.interspacing.length()>0) {
          processCommandsForLineSpacing(this.jobSetup.interspacing);
        }

       // condensed
       if (this.jobSetup.condensed)  executeCommand(TextPrinter.CMD_CONDENSED_ON);
       else  executeCommand(TextPrinter.CMD_CONDENSED_OFF);
        // first set top margin
        if (!this.topMarginPerSoftware) {
            executeCommand(TextPrinter.CMD_TOP_MARGIN, ""+ this.jobSetup.topMargin);
            if ((this.jobSetup.bottomMargin>0) && (this.jobSetup.rows>this.jobSetup.bottomMargin)) executeCommand(TextPrinter.CMD_BOTTOM_MARGIN, ""+(this.jobSetup.rows-this.jobSetup.bottomMargin));
        }
        else {
            executeCommand(TextPrinter.CMD_TOP_MARGIN, "0");
            //executeCommand(TextPrinter.CMD_BOTTOM_MARGIN, "0");
        }

        // then set page length

       // the preferred mode is setting the page length inches instead of lines
       if ((this.supportsCommand(TextPrinter.CMD_PAGE_LENGTH_INCHES)) && (this.jobSetup.paperSize!=null))  executeCommand(TextPrinter.CMD_PAGE_LENGTH_INCHES,""+  this.jobSetup.paperSize.getHeight());
       else executeCommand(TextPrinter.CMD_PAGE_LENGTH_LINES,""+ this.jobSetup.rows);



        // margins
        if (!this.leftMarginPerSoftware) {
            executeCommand(TextPrinter.CMD_LEFT_MARGIN,  ""+this.jobSetup.leftMargin);

            executeCommand(TextPrinter.CMD_RIGHT_MARGIN, ""+(jobSetup.cols-this.jobSetup.rightMargin));

        }
        else {
            executeCommand(TextPrinter.CMD_LEFT_MARGIN,  "0");

            executeCommand(TextPrinter.CMD_RIGHT_MARGIN,  "" +jobSetup.cols);
        }


        // quality
        if (this.jobSetup.draftQuality)  {
            executeCommand(TextPrinter.CMD_DRAFT);
            }
        else {
          executeCommand(TextPrinter.CMD_QUALITY);
          }

        // set default char set
        if (this.defaultCharSet.length()>0) processCommandsForCharacterSet("",this.defaultCharSet);

        addToBuffer(this.jobSetup.userDefinedCommands);
        if (debug) System.out.println("User command: "+ this.jobSetup.userDefinedCommands);

    }

   /**
   * set line spacing to get desired number of lines in paper
   */
   protected void calculateLineSpacing(int rows) {

        if (this.jobSetup.paperSize==null) return;

        if (this.jobSetup.interspacing.length()==0) this.jobSetup.interspacing =getBestLineSpacing(rows,this.jobSetup.paperSize.getHeight());
   }


  /**
   * set pitch and HMI to get a number of columns
   */
   protected void calculateColumnWidth(int cols) {

      if (this.jobSetup.paperSize==null) return;
      double requiredHMI=this.jobSetup.paperSize.getWidth()/cols;

      double requiredPitch=1/requiredHMI;

      if (this.jobSetup.pitch==-1) this.jobSetup.pitch=requiredPitch;
      if (this.jobSetup.columnWidth==-1) this.jobSetup.columnWidth=requiredHMI;

      if (debug) {
        System.out.println("Settings for " + cols + " columns and paper width " + this.jobSetup.paperSize.getWidth()+ " inches:");
        System.out.println("Pitch: "+this.jobSetup.pitch);
        System.out.println("HMI: "+this.jobSetup.columnWidth);
      }
   }


 /**
  * start printing job
  */
 public void startJob(PrinterPort p,JobProperties prop)  throws TextPrinterException  {

     // demo
     page=1;
     if (d()) {
      System.out.println("RTextPrinter evaluation version");
      System.out.println("www.java4less.com");
      System.out.println("-------------------------------------------");
      System.out.println("WARNING! LIMITATIONS OF THE EVALUATION VERSION:");
      System.out.println("Prints a maximum of 3 pages.");
      System.out.println("Printed pages have a watermark in the middle of the page.");
      System.out.println("-------------------------------------------");
      System.out.println("- The evaluation version of the product can only be used for evaluation purposes. ");
      System.out.println("- You may not include any component of this version in any final product or application.");
      System.out.println("- You may not redistribute any components of this product.");
      System.out.println("- You must delete this software after 30 days if you have not purchased the full version.");
      System.out.println("- Reverse engineering, reverse compiling, or disassembly of the Software is expressly prohibited.");
      System.out.println("-------------------------------------------");
     }

     if (charSetManager==null) charSetManager=CharSetManager.getInstance();

    jobSetup=prop;
    resetPageData();
    this.port=p;
    this.port.open();
    currentProp=this.getDefaultTextProperties();
    processCommandsForJob();
 }

 /**
  * finishes printing job
  */
 public void endJob() throws TextPrinterException {
   finishPage();
   flushBuffer();
   this.port.close();
 }

 /**
  * creates a new page
  */
  protected void resetPageData() {
    currentRow=this.jobSetup.topMargin;
    currentCol=this.jobSetup.leftMargin;
    pageData=new char[jobSetup.rows][jobSetup.cols];
    pageDataProperties= new TextProperties[jobSetup.rows][jobSetup.cols];
  }

  /**
   * flush buffer
   */
  protected void flushBuffer() throws TextPrinterException {

      if (bufferPointer>0) port.write(buffer,bufferPointer);

      bufferPointer=0;

  }

  /**
   * perform char set mapping
   */
   protected byte[] charMapping(char c,TextProperties prop)  throws TextPrinterException,UnsupportedEncodingException   {

        String s="" + c;
         if (! this.performCharSetConversion) {

              return s.getBytes(jobSetup.javaEncoding);
            }


         if (prop==null) prop=this.getDefaultTextProperties();


         String sSet=prop.characterSet;
         if (sSet==null) sSet=this.defaultCharSet;
         if (sSet.length()==0)  sSet=this.defaultCharSet;

         if (sSet.length()==0)  return s.getBytes(jobSetup.javaEncoding);
         //if (sSet.toLowerCase().startsWith("java:")==0)  return s.getBytes(jobSetup.javaEncoding.substring(5));
         Charset set=charSetManager.getCharSet(sSet);

         if (set==null)
            // try using the internal charset name
            if (this.charSetValues.containsKey(sSet)) {
                String sSet2=((CommandCharSetValue) this.charSetValues.get(sSet)).charSet;
                //System.out.println("");
                //System.out.println("***   "+sSet2);
                //System.out.println("");
                if (sSet2.length()>0) {
                    set=charSetManager.getCharSet(sSet2);
                    if ((set==null) && (sSet2.toLowerCase().startsWith("java:"))) return s.getBytes(sSet2.substring(5));
                }
         }

         if (set!=null) {
                byte[] mappedBytes=set.fromUnicode(s.substring(0,1).getBytes("UTF-16LE"));

                if ((mappedBytes==null) && (this.jobSetup.autoChartSetSelection)) {
                    String newCharSet=getCharSetforUnicode(s.substring(0,1).getBytes("UTF-16LE"));
                    if (newCharSet!=null) {

                        Charset set2=TextPrinter.charSetManager.getCharSet(newCharSet);
                        mappedBytes=set2.fromUnicode(s.substring(0,1).getBytes("UTF-16LE"));

                        // set new char set
                        prop.characterSet=newCharSet;
                    }
                }

                return mappedBytes;

         }

         // char set not found
         return s.getBytes(jobSetup.javaEncoding);
   }

   /**
    * find a char set that can map the given unicode char
    */
    protected String getCharSetforUnicode(byte[] b) {

        java.util.Enumeration e= this.charSetValues.elements();
        while (e.hasMoreElements()) {
            CommandParamValue p=(CommandParamValue) e.nextElement();

            Charset c=this.charSetManager.getCharSet(p.name);
            if (c!=null)
              if (c.fromUnicode(b)!=null) return p.name; // character found in this set!
        }

        return null;

    }


  /**
   * add bytes to the output buffer
   */
  protected void addToBuffer(String s)  throws TextPrinterException  {
    try {
    addToBuffer(s.getBytes(jobSetup.javaEncoding));
    } catch (UnsupportedEncodingException e) {throw new TextPrinterException(e.getMessage());}
  }

  /**
   * add bytes to the output buffer
   */
  protected void addToBuffer(byte[] b)  throws TextPrinterException  {

      boolean isCommand=false;
      if (b==null) return;

      for (int i=0;i<b.length;i++) {

       if (debug) {
          if (i==0) {

              if (b[i]==this.LF) System.out.println("LF ");
              else if (b[i]==this.FF) System.out.print("FF ");
              else if (b[i]==this.CR) System.out.print("CR ");
              else if (b[i]==this.ESC) {
                  System.out.print("ESC ");
                  isCommand=true;
              }
              else {
                System.out.print(Integer.toHexString(b[i] & 0xFF).toUpperCase()+"("+new String(b)+") ");
              }

          } else {
            if (isCommand)    System.out.print(Integer.toHexString(b[i]).toUpperCase()+"("+new String(b,i,1)+") ");
            else   System.out.print(Integer.toHexString(b[i]).toUpperCase()+"("+new String(b)+") ");
          }

       }


            buffer[bufferPointer++]=b[i];
            if (bufferPointer==bufferSize) flushBuffer();

     } // for



  }

  /**
   * get list of supported char sets
   */
   public String[] getSupportedCharSets() {

   Enumeration e=this.charSetValues.elements();
   Vector v=new Vector();

   while (e.hasMoreElements()) {
    CommandCharSetValue p=(CommandCharSetValue) e.nextElement();

      //if (p.charSet.length()>0) v.addElement(p.charSet); // take descriptive character set name
      v.addElement(p.name); // take descriptive character set code
    }

      return convertToArray(v);
   }

  /**
   * get list of first element in array
   */
   public String[] getFirstElements(String[][] t) {

   Vector v=new Vector();

   for (int i=0;i<t.length;i++) {
      v.addElement(t[i][0]);
    }

      return convertToArray(v);
   }


  /**
   * get list of supported fonts
   */
   public Enumeration getSupportedFonts() {
      return this.fontValues.keys();
   }

  /**
   * get list of supported fonts sizes
   */
   public Enumeration getSupportedCharPitch() {
      return this.pitchValues.keys();
   }

  /**
   * get list of supported interspacing
   */
   public Enumeration getSupportedInterspacing() {
      return this.interspacingValues.keys();
   }


   private String[] convertToArray(java.util.Vector v) {
      Object[] o=v.toArray();
      String[] s=new String[o.length];

      for (int i=0;i<o.length;i++) s[i]=(String) o[i];

      return s;
   }

   /**
    * print array in standard output
    */
   public void printArray(String[] t) {

      for (int i=0;i<t.length;i++) System.out.println(t[i]);
   }

   /**
    * create default printer job
    */
    public JobProperties getDefaultJobProperties() {
        JobProperties j=new JobProperties();
        j.topMargin=0;
        return j;
    }

   /**
    * create default text properties
    */
    public TextProperties getDefaultTextProperties() {
        TextProperties t=new TextProperties();

        setJobDefaults(t);

        if (this.jobSetup!=null){
             t.proportional=jobSetup.proportional;
             t.condensed=jobSetup.condensed;
        }

        return t;
    }

    /**
     * set default values for character properties
     */
    private void setJobDefaults(TextProperties t)  {
        if (this.jobSetup!=null){
              if (t.pitch<=0) t.pitch=jobSetup.pitch;
              if (t.characterSet.length()==0) t.characterSet=this.defaultCharSet;
        }
    }

 /**
    * compare character at a given position in page
    */
    private boolean isChar(int line,int col,char[] c) {
        if (line>=pageData.length) return false;
        if (col>=pageData[line].length) return false;

        for (int i=0;i<c.length;i++)
           if (pageData[line][col]==c[i]) return true;

        return false;
    }

    /**
     * set horizontal char as part of a line
     */
     private void setHChar(int line,int col,boolean firstInLine,boolean lastInLine,TextProperties t) {

          if (line>=pageData.length) return;
          if (col>=pageData[0].length) return;

          // do nothing, there is a horizontal line char is this position already
          if (isChar(line,col,new char[] {this.hLineChar,this.hbLineChar,this.htLineChar,this.crossLineChar,})) return;
          if ((lastInLine) && (isChar(line,col,new char[] {this.trCornerChar,this.brCornerChar}))) return;
          if ((firstInLine) && (isChar(line,col,new char[] {this.tlCornerChar,this.blCornerChar}))) return;
          if ((firstInLine) && (isChar(line,col,new char[] {this.vrLineChar}))) return;
          if ((lastInLine) && (isChar(line,col,new char[] {this.vlLineChar}))) return;

          // there is a vertical line at this position, this can be a line cross or a corner
          if (pageData[line][col]==this.vLineChar) {

              if ((isChar(line+1,col,new char[] {this.htLineChar,this.vLineChar,this.vrLineChar,this.vlLineChar,this.blCornerChar,this.brCornerChar,this.crossLineChar})) &&
                (!isChar(line-1,col,new char[] {this.hbLineChar,this.vLineChar,this.vrLineChar,this.vlLineChar,this.tlCornerChar,this.trCornerChar,this.crossLineChar}))) {
                      // the vertical continues on the bottom   only
                      if (lastInLine) printString(this.trCornerChar,line,col,t);
                      else if (firstInLine) printString(this.tlCornerChar,line,col,t);
                      else printString(this.hbLineChar,line,col,t);
                      return;
                }
              else   if ((!isChar(line+1,col,new char[] {this.htLineChar,this.vLineChar,this.vrLineChar,this.vlLineChar,this.blCornerChar,this.brCornerChar,this.crossLineChar})) &&
                (isChar(line-1,col,new char[] {this.hbLineChar,this.vLineChar,this.vrLineChar,this.vlLineChar,this.tlCornerChar,this.trCornerChar,this.crossLineChar}))) {
                       // the vertical continues on the top only
                      if (lastInLine) printString(this.brCornerChar,line,col,t);
                      else if (firstInLine) printString(this.blCornerChar,line,col,t);
                      else printString(this.htLineChar,line,col,t);
                      return;
                }
              else  if ((isChar(line+1,col,new char[] {this.htLineChar,this.vLineChar,this.vrLineChar,this.vlLineChar,this.blCornerChar,this.brCornerChar,this.crossLineChar})) &&
                (isChar(line-1,col,new char[] {this.hbLineChar,this.vLineChar,this.vrLineChar,this.vlLineChar,this.tlCornerChar,this.trCornerChar,this.crossLineChar}))) {
                      // the vertical continues on the top and bottom
                      if (lastInLine) printString(this.vlLineChar,line,col,t);
                      else if (firstInLine)
                        printString(this.vrLineChar,line,col,t);
                      else
                      printString(this.crossLineChar,line,col,t);
                      return;
                }
               else {
                      // the vertical line does not continue on the top nor on the bottom
                      if (lastInLine) printString(this.vlLineChar,line,col,t);
                      else if (firstInLine)
                          printString(this.vrLineChar,line,col,t);
                      else
                      printString(this.crossLineChar,line,col,t);

                      return;
               }

          }

          if ((pageData[line][col]==this.vrLineChar) || (pageData[line][col]==this.vlLineChar)) {
              printString(this.crossLineChar,line,col,t);
              return;
          }

          if ((pageData[line][col]==this.trCornerChar) || (pageData[line][col]==this.tlCornerChar)) {
              printString(this.hbLineChar,line,col,t);
              return;
          }

           if ((pageData[line][col]==this.brCornerChar) || (pageData[line][col]==this.blCornerChar)) {
              printString(this.htLineChar,line,col,t);
              return;
          }

          printString(this.hLineChar,line,col,t);

     }

    /**
     * set vertical char as part of a line
     */
     private void setVChar(int line,int col,boolean firstInLine,boolean lastInLine,TextProperties t) {
          if (line>=pageData.length) return;
          if (col>=pageData[0].length) return;

          // do nothing, there is a horizontal line char is this position already
          if (isChar(line,col,new char[] {this.vLineChar,this.vlLineChar,this.vrLineChar,this.crossLineChar,})) return;
          if ((lastInLine) && (isChar(line,col,new char[] {this.brCornerChar,this.blCornerChar}))) return;
          if ((firstInLine) && (isChar(line,col,new char[] {this.tlCornerChar,this.trCornerChar}))) return;
          if ((firstInLine) && (isChar(line,col,new char[] {this.hbLineChar}))) return;
          if ((lastInLine) && (isChar(line,col,new char[] {this.htLineChar}))) return;

          // there is a hor. line at this position, this can be a line cross or a corner
          if (pageData[line][col]==this.hLineChar) {

              if ((isChar(line,col+1,new char[] {this.vlLineChar,this.hbLineChar,this.htLineChar,this.hLineChar,this.trCornerChar,this.brCornerChar,this.crossLineChar})) &&
                (!isChar(line,col-1,new char[] {this.vrLineChar,this.hbLineChar,this.htLineChar,this.hLineChar,this.tlCornerChar,this.blCornerChar,this.crossLineChar}))) {
                      // the vertical continues on the right side only
                      if (lastInLine) printString(this.blCornerChar,line,col,t);
                      else if (firstInLine) printString(this.tlCornerChar,line,col,t);
                      else
                          printString(this.vrLineChar,line,col,t);
                      return;
                }
              if ((!isChar(line,col+1,new char[] {this.vlLineChar,this.hbLineChar,this.htLineChar,this.hLineChar,this.trCornerChar,this.brCornerChar,this.crossLineChar})) &&
                (isChar(line,col-1,new char[] {this.vrLineChar,this.hbLineChar,this.htLineChar,this.hLineChar,this.tlCornerChar,this.blCornerChar,this.crossLineChar}))) {
                       // the vertical continues on the left only
                      if (lastInLine) printString(this.brCornerChar,line,col,t);
                      else if (firstInLine) printString(this.trCornerChar,line,col,t);
                      else printString(this.vlLineChar,line,col,t);
                      return;
                }
              if ((isChar(line,col+1,new char[] {this.vlLineChar,this.hbLineChar,this.htLineChar,this.hLineChar,this.trCornerChar,this.brCornerChar,this.crossLineChar})) &&
                (isChar(line,col-1,new char[] {this.vrLineChar,this.hbLineChar,this.htLineChar,this.hLineChar,this.tlCornerChar,this.blCornerChar,this.crossLineChar}))) {
                      // the vertical continues on the right and left side
                      if (lastInLine) printString(this.htLineChar,line,col,t);
                      else if (firstInLine) printString(this.hbLineChar,line,col,t);
                      else printString(this.crossLineChar,line,col,t);
                      return;
                }
               else {
                      // the vertical line does not continue on the left nor on the right side
                      if (lastInLine) printString(this.htLineChar,line,col,t);
                      else if (firstInLine) printString(this.hbLineChar,line,col,t);
                      else printString(this.crossLineChar,line,col,t);
                      return;
               }

          }

          if ((pageData[line][col]==this.hbLineChar) || (pageData[line][col]==this.htLineChar)) {
              printString(this.crossLineChar,line,col,t);
              return;
          }

           if ((pageData[line][col]==this.blCornerChar) || (pageData[line][col]==this.tlCornerChar)) {
              printString(this.vrLineChar,line,col,t);
              return;
          }

           if ((pageData[line][col]==this.brCornerChar) || (pageData[line][col]==this.trCornerChar)) {
              printString(this.vlLineChar,line,col,t);
              return;
          }

          printString(this.vLineChar,line,col,t);

     }

     /**
      * add a font supported by the printer
      */
      protected void addFont(String name,String valueOrCommand) {
            this.addParamValue(fontValues,new CommandParamValue(name,valueOrCommand));
      }

     /**
      * clear list of spacing
      */
      protected void clearSpacing() {
          this.interspacingValues.clear();
      }

     /**
      * clear list of character sets
      */
      protected void clearCharSet() {
          charSetValues.clear();
      }

     /**
      * clear list of fonts
      */
      protected void clearFonts() {
          fontValues.clear();
      }

     /**
      * clear list of char sizes
      */
      protected void clearCPIList() {
          pitchValues.clear();
      }

     /**
      * add a characterset supported by the printer
      */
      protected void addCharSet(String name,String valueOrCommand,String charSet) {
            this.addParamValue(charSetValues,new CommandCharSetValue(name,valueOrCommand,charSet));
      }

      /**
       * add value in a hash table
       */
       protected void addParamValue(Hashtable table,CommandParamValue val) {
            if (table.containsKey(val.name)) table.remove(val.name);

            val.value=Command.parseCommand(val.value);
            table.put(val.name,val);
       }

     /**
      * add a pitch supported by the printer
      */
      protected void addPitch(String name,String valueOrCommand) {
            this.addParamValue(pitchValues,new CommandParamValue(name,valueOrCommand));
      }

     /**
      * add a line spacing supported by the printer
      */
      public void addSpacing(String name,String valueOrCommand) {
            this.addParamValue(interspacingValues,new CommandParamValue(name,valueOrCommand));
      }

      /**
      * add a paper size supported by the printer
      */
      protected void addPaperSize(String name,String valueOrCommand) {
            this.addParamValue(paperSizeValues,new CommandParamValue(name,valueOrCommand));
      }

      /**
      * add a resolution supported by the printer
      */
      protected void addResolution(String name,String valueOrCommand) {
            this.addParamValue(resolutionValues,new CommandParamValue(name,valueOrCommand));
      }

    protected byte[] getLowHeightBytes(double i) {
      byte[] b= new byte[2];

      b[0]=(byte) (i % 256);
      b[1]=(byte) (Math.floor(i / 256));

      return b;
    }
}