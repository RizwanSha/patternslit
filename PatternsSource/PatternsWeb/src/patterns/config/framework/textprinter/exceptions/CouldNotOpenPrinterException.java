package  patterns.config.framework.textprinter.exceptions;

import patterns.config.framework.textprinter.TextPrinterException;

//  RTextPrinter
//  Copyright (C)
//
//  Java4Less@Confluencia.net
//  All rights reserved
//
// Adquisition , use and distribution of this code is subject to restriction:
//  - You may modify the source code in order to adapt it to your needs.
//  - Redistribution of this ( or a modified version) source code is prohibited. You may only redistribute compiled versions.
//  - You may redistribute the compiled version as part of your application, not a new java component with the same purpose as this one.
//  - You may not remove this notice from the source code
//  - This notice disclaim all warranties of all material
//  - You may not copy and paste any code into external files.


public class CouldNotOpenPrinterException extends TextPrinterException {

  /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

public CouldNotOpenPrinterException(String msg) {
        super(msg);
 }

}