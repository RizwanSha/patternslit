package patterns.config.framework.textprinter;
//  RTextPrinter
//  Copyright (C)
//
//  Java4Less@Confluencia.net
//  All rights reserved
//
// Adquisition , use and distribution of this code is subject to restriction:
//  - You may modify the source code in order to adapt it to your needs.
//  - Redistribution of this ( or a modified version) source code is prohibited. You may only redistribute compiled versions.
//  - You may redistribute the compiled version as part of your application, not a new java component with the same purpose as this one.
//  - You may not remove this notice from the source code
//  - This notice disclaim all warranties of all material
//  - You may not copy and paste any code into external files.


/**
 * ESC control command
 */
public class Command {

protected static final char ESC=(char) 27;

  /**
   * command name
   */
  String name="";

  /**
   * control charaters
   */
   String controlString="";

  public Command(String sName,String sControl) {
      this.controlString=parseCommand(sControl);
      this.name=sName;
  }

  /**
   * get name
   */
   public String getName() {
      return this.name;
   }

   public String getCommand() {
      return controlString;
   }

  /**
   * creates the printer command by parsing a description of the command:
   *
   * 1. it replaces the ESC string with the esc character
   * 2. it parses the CRTL+ string
   * 3. it removes empty characters
   */
  public static String parseCommand(String s) {

    // replace ESC
    int p=s.toUpperCase().indexOf("ESC");
    while (p>=0) {
       s=s.substring(0,p)+ESC+s.substring(p+3);
       p=s.toUpperCase().indexOf("ESC");
    }

    // parse CRTL+
    p=s.toUpperCase().indexOf("CRTL+");
    while (p>=0) {
      s=s.substring(0,p)+ (char) ( ((byte) (s.charAt(p+5)))-64) +s.substring(p+6);
      p=s.toUpperCase().indexOf("CRTL+");
    }

    /*p=s.toUpperCase().indexOf(" ");
    while (p>=0)  {
        s=s.substring(0,p)+s.substring(p+1);
        p=s.toUpperCase().indexOf(" ");
      }*/

    return s;

  }

}