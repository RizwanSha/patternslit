package patterns.config.framework.textprinter;
//  RTextPrinter
//  Copyright (C)
//
//  Java4Less@Confluencia.net
//  All rights reserved
//
// Adquisition , use and distribution of this code is subject to restriction:
//  - You may modify the source code in order to adapt it to your needs.
//  - Redistribution of this ( or a modified version) source code is prohibited. You may only redistribute compiled versions.
//  - You may redistribute the compiled version as part of your application, not a new java component with the same purpose as this one.
//  - You may not remove this notice from the source code
//  - This notice disclaim all warranties of all material
//  - You may not copy and paste any code into external files.

/**
 * this class stores information about the printer job. It stored global information for th job like, page size and margins.
 */
public class JobProperties {

/**
 * size of the page, lines
 */
  public int rows=60;

/**
 * size of the page, columns
 */
  public int cols=80;

  /**
   * top margin (number of lines)
   */
  public int topMargin=0;
  /**
   * bottom margin (number of lines)
   */
   public int bottomMargin=0;
  /**
   * left margin (number of columns)
   */
  public int leftMargin=0;
  /**
   * right margin (number of columns)
   */
  public int rightMargin=0;

  /**
   * line interspacing. This has an effect on the number of lines in the page. The default empty string will use the printer's default value. You can also let TexPrinter calculate the line spacing based on the desired number of lines and the page height.
   *  The format of the line spacing must be "1/8" (8 lines per inch), "1/6", "1/10", "12/72" .....
   */
   public String interspacing="";

   /**
    * landscape format, usually only laser printers support this.
    */
    public boolean  landscape=false;

    /**
     * printing quality
     */
     public boolean draftQuality=false;


    /**
     * Size of the characters 10, 12 or 15 (characters/inch). This has an effect on the number of columns the page. Default value is -1 will use the printer's default value. You can also let TexPrinter calculate the pitch based on the desired number of colums and the page width.
     */
     public double pitch=-1;

     /**
      * wide of columns in inches. This has an effect on the number of columns the page. Default value is -1 will use the printer's default value.  You can also let TexPrinter calculate the column width based on the desired number of colums and the page width.
      */
     public double columnWidth=-1;

      /**
       * allow automatic change of character set if the characters to be printed cannot be found in the defined set.
       */
      public boolean autoChartSetSelection=false;

      /**
       * any user defined control commands to be executed at the beginning of the job.  This will be executed as the last command of the job setup command sequence.
       */
      public String userDefinedCommands="";

      /**
       * any user defined control commands to be executed for resetting the printer. This will be executed as the first command.
       */
      public String userResetCommands="";

    /**
     * printer resolution, only required if the pitch and line spacing must be set automatically.
     */
     public int resolution=300;

    /**
     * paper size, only required if the pitch and line spacing must be set automatically.
     */
     public PaperSize paperSize=null;

     /**
      * proportional printing. If false all characters will have the same fixed width.
      */
      public boolean proportional=false;

      /**
       * use condensed charaters
       */
      public boolean condensed=false;

      /**
       * jave encoding to be used if no character set conversion is performed
       */
       public String javaEncoding=System.getProperty("file.encoding"); // "ISO-8859-1"

}