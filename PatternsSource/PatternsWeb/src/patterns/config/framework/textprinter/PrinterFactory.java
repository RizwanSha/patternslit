package patterns.config.framework.textprinter;

import patterns.config.framework.textprinter.printers.PlainPrinter;
import patterns.config.framework.textprinter.printers.TestPrinter;

//  RTextPrinter
//  Copyright (C)
//
//  Java4Less@Confluencia.net
//  All rights reserved
//
// Adquisition , use and distribution of this code is subject to restriction:
//  - You may modify the source code in order to adapt it to your needs.
//  - Redistribution of this ( or a modified version) source code is prohibited. You may only redistribute compiled versions.
//  - You may redistribute the compiled version as part of your application, not a new java component with the same purpose as this one.
//  - You may not remove this notice from the source code
//  - This notice disclaim all warranties of all material
//  - You may not copy and paste any code into external files.


/**
 * this class is used to get an instance of a printer by name.
 */
public class PrinterFactory {

  // supported printers and implementing class
  protected static String[][] printers={
  {"IBM-PROPRINTER","patterns.config.framework.textprinter.printers.ProPrinterXL"},
  {"IBM-PROPRINTER-III","patterns.config.framework.textprinter.printers.ProPrinterIII"},
  {"IBM-PROPRINTER-XL","patterns.config.framework.textprinter.printers.ProPrinterXL"},
  {"IBM-PROPRINTER-XL24","patterns.config.framework.textprinter.printers.ProPrinterXL24"},
  {"IBM-PPDS","patterns.config.framework.textprinter.printers.PPDSPrinter"},

  {"EPSON-ESCP2","patterns.config.framework.textprinter.printers.ESCP2Printer"},
  {"EPSON-ESCP","patterns.config.framework.textprinter.printers.ESCPPrinter"},
  {"EPSON-9PIN-ESCP","patterns.config.framework.textprinter.printers.ESCP9PinPrinter"},
  {"EPSON-FX850","patterns.config.framework.textprinter.printers.EpsonFX850_1050Printer"},
  {"EPSON-FX1050","patterns.config.framework.textprinter.printers.EpsonFX850_1050Printer"},
  {"EPSON-LX300","patterns.config.framework.textprinter.printers.EpsonLX300"},
  {"EPSON-LQ1070","patterns.config.framework.textprinter.printers.EpsonLQ1070"},

  {"HP-PCL","patterns.config.framework.textprinter.printers.PCL5Printer"},
  {"HP-PCL3","patterns.config.framework.textprinter.printers.PCL3Printer"},
  {"HP-PCL5","patterns.config.framework.textprinter.printers.PCL5Printer"},

  {"DIABLO","patterns.config.framework.textprinter.printers.DiabloPrinter"},

  {"PLAIN","patterns.config.framework.textprinter.printers.PlainPrinter"},

  {"OKI-MICROLINE","patterns.config.framework.textprinter.printers.OKIMicroline"},
  {"P-SERIES","patterns.config.framework.textprinter.printers.PrintronixPSeries"},

  {"PanasonicKX_P3123","patterns.config.framework.textprinter.printers.PanasonicKX_P3123"},
  {"PanasonicKX_P1150","patterns.config.framework.textprinter.printers.PanasonicKX_P1150"}
  };

  /**
   * get a printer instance. Supported names are:
   * - IBM-PROPRINTER: Proprinters or compatible.<BR>
   * - EPSON-ESCP2: Epson Esc/P2 or compatible<BR>
   * - EPSON-ESCP: Epson Esc/P or compatible<BR>
   * - IBM-PPDS<BR>
   * - HP-PCL3: HP PCL 3 or compatible (also known as Laset Jet Plus Emulation.<BR>
   * - HP-PCL5: HP PCL 3 or compatible<BR>
   * - DIABLO<BR>
   * - IBM-PROPRINTERXL24<BR>
   * - EPSON-FX850<BR>
   * - EPSON-FX1050<BR>
   * - PLAIN: for other printers.<BR>
   * - OKI-MICROLINE<BR>
   */
  public static TextPrinter getPrinter(String name) {

      TextPrinter p=null;

      /*if (name.equalsIgnoreCase("IBM-PROPRINTER")) p= new ProPrinter();
      if (name.equalsIgnoreCase("EPSON-ESCP2")) p= new ESCP2Printer();
      if (name.equalsIgnoreCase("EPSON-ESCP")) p= new ESCPPrinter();
      if (name.equalsIgnoreCase("IBM-PPDS")) p= new PPDSPrinter();
      if (name.equalsIgnoreCase("HP-PCL")) p= new PCL5Printer();
      if (name.equalsIgnoreCase("HP-PCL5")) p= new PCL5Printer();
      if (name.equalsIgnoreCase("HP-PCL3")) p= new PCL3Printer();
      if (name.equalsIgnoreCase("DIABLO")) p= new DiabloPrinter();*/

      for (int i=0;i<printers.length;i++)
          if (printers[i][0].equalsIgnoreCase(name)) {
              try {
                  Class printerClass=Class.forName(printers[i][1]);
                  p=(TextPrinter) printerClass.newInstance();
                } catch (Exception e) {e.printStackTrace();}
          }

  //    if (name.equalsIgnoreCase("TEST")) p=new TestPrinter();
//
    //  if (p==null) p=new PlainPrinter();

      p.init();

      return p;
  }

  /**
   * returns list of supported printers
   */
  public static String[] getSupportedPrinters() {
      String[] l=new String[printers.length];

      for (int i=0;i<printers.length;i++) l[i]=printers[i][0];

      return l;
  }


}