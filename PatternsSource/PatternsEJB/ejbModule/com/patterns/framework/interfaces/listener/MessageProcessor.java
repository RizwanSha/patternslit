package com.patterns.framework.interfaces.listener;

import java.sql.ResultSet;
import java.util.HashMap;

import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.loggers.ApplicationLogger;

public class MessageProcessor {

	public static final String MESSAGE_CONTENT = "MSG_CONTENT";
	public static final String ENTITY_CODE = "ENTITY_CODE";
	public static final String SYSTEM_CODE = "SYSTEM_CODE";
	public static final String SOURCE_QUEUE = "SOURCE_QUEUE";
	public static final String CORRELATION_ID = "CORRELATION_ID";
	public static final String MESSAGE_PRIORITY = "MSG_PRIORITY";
	
	private ApplicationLogger logger = ApplicationLogger.getInstance(this.getClass().getSimpleName());

	public void processInboundMessage(HashMap<String, String> inMsgMap) throws Exception {
		logger.logDebug("processInboundMessage() - Begin");
		DBContext databaseContext = new DBContext();
		try {
			String inventoryNumber = RegularConstants.EMPTY_STRING;
			byte[] content = inMsgMap.get("MSG_CONTENT").getBytes();

			DBUtil dbUtil = databaseContext.createUtilInstance();
			dbUtil.reset();
			dbUtil.setMode(DBUtil.PREPARED);
			dbUtil.setSql("SELECT FN_NEXTVAL('SEQ_IW_INV_NO') FROM DUAL");
			ResultSet readRSet = dbUtil.executeQuery();
			if (readRSet.next()) {
				inventoryNumber = readRSet.getString(1);
			}

			String logInsertQuery = "INSERT INTO CTJINQLOG (ENTITY_CODE,INVENTORY_NO,MSG_CONTENT,SYSTEM_INTIME,STATUS,SOURCE_QUEUE,MQ_PRIORITY,DIRECTION) VALUES (?,?,?,FN_GETCDT(?),?,?,?,?)";
			dbUtil.reset();
			dbUtil.setMode(DBUtil.PREPARED);
			dbUtil.setSql(logInsertQuery);
			dbUtil.setString(1, inMsgMap.get(MessageProcessor.ENTITY_CODE));
			dbUtil.setString(2, inventoryNumber);
			dbUtil.setBytes(3, content);
			dbUtil.setString(4, inMsgMap.get(MessageProcessor.ENTITY_CODE));
			dbUtil.setString(5, "N");
			dbUtil.setString(6, inMsgMap.get(MessageProcessor.SOURCE_QUEUE));
			dbUtil.setString(7, inMsgMap.get(MessageProcessor.MESSAGE_PRIORITY));
			dbUtil.setString(8, "1");
			dbUtil.executeUpdate();
			dbUtil.reset();

			logInsertQuery = "INSERT INTO CTJINQPE (ENTITY_CODE,INVENTORY_NO) VALUES (?,?)";
			dbUtil.reset();
			dbUtil.setMode(DBUtil.PREPARED);
			dbUtil.setSql(logInsertQuery);
			dbUtil.setString(1, inMsgMap.get(MessageProcessor.ENTITY_CODE));
			dbUtil.setString(2, inventoryNumber);
			dbUtil.executeUpdate();
		} catch (Exception e) {
			logger.logError(e.getLocalizedMessage());
			e.printStackTrace();
			throw e;
		} finally {
			databaseContext.close();
		}
		logger.logDebug("processInboundMessage() - End");
	}
}