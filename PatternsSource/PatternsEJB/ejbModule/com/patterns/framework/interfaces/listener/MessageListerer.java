package com.patterns.framework.interfaces.listener;

import java.util.HashMap;

import javax.annotation.Resource;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJBException;
import javax.ejb.MessageDriven;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import org.jboss.ejb3.annotation.ResourceAdapter;

import patterns.config.framework.database.RegularConstants;


/**
 * Message-Driven Bean implementation class for: MessageListerer
 */

/*@MessageDriven(activationConfig = {
		@ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge"),  
	      @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),  
	      @ActivationConfigProperty(propertyName = "destination", propertyValue = "INQ") })
@ResourceAdapter("activemq-ra.rar")*/
public class MessageListerer implements MessageListener {
	@Resource(mappedName = "java:/activemq/ConnectionFactory")
	private ConnectionFactory connectionFactory;
	@Resource(mappedName = "java:/activemq/INQ")
	private Destination queue;
	private Connection connection;

	public void init() throws JMSException {
		connection = connectionFactory.createConnection();
		connection.start();
	}

	public void destroy() throws JMSException {
		connection.close();
	}

	/**
	 * Default constructor.
	 */
	public MessageListerer() {
	}

	/**
	 * @see MessageListener#onMessage(Message)
	 */
	public void onMessage(Message message) {
		try {
			init();
			String msgContent = RegularConstants.EMPTY_STRING;
			String correlationID = RegularConstants.EMPTY_STRING;
			String hexCorrelationID = RegularConstants.EMPTY_STRING;

			HashMap<String, String> inMsgMap = new HashMap<String, String>();
			MessageProcessor messageProcessor = new MessageProcessor();
			int priority = 0;

			correlationID = message.getJMSCorrelationID();
			if (correlationID != null && correlationID.startsWith("ID:"))
				correlationID = correlationID.substring(3);
			if (correlationID != null)
				hexCorrelationID = new String(hexToByte(correlationID));

			priority = message.getJMSPriority();
			inMsgMap.put(MessageProcessor.CORRELATION_ID, hexCorrelationID);
			inMsgMap.put(MessageProcessor.MESSAGE_PRIORITY, priority + "");

			// Read message content
			if (message instanceof TextMessage) {
				TextMessage textMessage = (TextMessage) message;
				msgContent = textMessage.getText();
			}
			System.out.println("Message from MessageListener ==== " + msgContent);
			inMsgMap.put(MessageProcessor.MESSAGE_CONTENT, msgContent);

			// Read environment variables
//			InitialContext ic = new InitialContext();
//			String systemCode = (String) ic.lookup("java:comp/env/SYSTEM_CODE");
//			String sourceQueue = (String) ic.lookup("java:comp/env/SOURCE_QUEUE");
//			String entityCode = Integer.valueOf(ic.lookup("java:comp/env/ENTITY_CODE").toString()).toString();
			String entityCode="1";
			String sourceQueue="INQ";
//
//			inMsgMap.put(MessageProcessor.SYSTEM_CODE, systemCode);
			inMsgMap.put(MessageProcessor.SOURCE_QUEUE, sourceQueue);
			inMsgMap.put(MessageProcessor.ENTITY_CODE, entityCode);
			messageProcessor.processInboundMessage(inMsgMap);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				destroy();
			} catch (JMSException e) {
				throw new EJBException("Error in closing connection", e);
			}
		}
	}

	private byte[] hexToByte(String hexString) {
		int arrayCount = 0, position = 0;
		hexString = hexString.toUpperCase();
		byte[] bytes = new byte[hexString.length() / 2];

		while (arrayCount < bytes.length) {
			bytes[arrayCount] = (byte) Integer.parseInt(hexString.substring(position, position + 2), 16);

			// Increment my counters
			position += 2;
			arrayCount++;
		}

		return bytes;
	}
}
