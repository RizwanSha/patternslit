package com.patterns.framework.ejb;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.transaction.UserTransaction;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.bo.ServiceBO;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.process.TBAProcessInfo;
import patterns.config.framework.process.TBAProcessResult;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.service.ServiceLocator;
import patterns.config.framework.thread.TBAContextImpl;

/**
 * Session Bean implementation class ProcessDispatcher
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class ProcessDispatcher implements ProcessDispatcherRemote, ProcessDispatcherLocal {
	@Resource
	SessionContext sessionContext;
	private UserTransaction userTransaction;

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public TBAProcessResult processRequest(TBAProcessInfo processInfo) {
		TBAProcessResult processResult = new TBAProcessResult();
		Class<? extends Object> boClass = null;
		Object object = null;
		try {
			ServiceLocator locator = ServiceLocator.getInstance();
			boClass = locator.getBO(processInfo.getProcessBO());
			object = boClass.newInstance();
		} catch (Exception e) {
			e.printStackTrace();
			String error = e.getLocalizedMessage();
			processResult.setAdditionalInfo(error);
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			return processResult;
		}

		DBContext dbContext = new DBContext();
		TBAContextImpl processContextImpl = new TBAContextImpl();
		try {
			beginTransaction();
			processContextImpl.setDBContext(dbContext);
			processContextImpl.setProcessAction(processInfo.getProcessAction());
			processContextImpl.setProcessData(processInfo.getProcessData());
			processContextImpl.setProcessID(processInfo.getProcessAction().getProcessID());
			processContextImpl.setProcessTFAInfo(processInfo.getProcessTFAInfo());
			processContextImpl.init();
			GenericBO businessObject = (GenericBO) object;
			businessObject.initialize();
			businessObject.init();
			processContextImpl.startTransaction();
			if (processContextImpl.getResultDTO() != null) {
				processResult.setResponseDTO(processContextImpl.getResultDTO());
			}
			processResult = businessObject.processRequest();
			if (processResult.getProcessStatus().equals(TBAProcessStatus.SUCCESS)) {
				processContextImpl.commitTransaction();
				if (processContextImpl.getResultDTO() != null) {
					processResult.setResponseDTO(processContextImpl.getResultDTO());
				}
				commitTransaction();
			} else if (processResult.getProcessStatus().equals(TBAProcessStatus.FAILURE)) {
				throw new Exception();
			}
		} catch (Throwable e) {
			e.printStackTrace();
			if (processResult == null) {
				processResult = new TBAProcessResult();
			}
			rollbackTransaction();
			String error = e.getLocalizedMessage();
			processResult.setAdditionalInfo(error);
			processResult.setErrorMessage(e.getLocalizedMessage());
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
		} finally {
			if (processContextImpl != null) {
				processContextImpl.destroy();
			}
		}
		return processResult;
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public TBAProcessResult processRequest(DTObject inputDTO) {
		TBAProcessResult processResult = new TBAProcessResult();
		Class<? extends Object> boClass = null;
		Object object = null;
		try {
			ServiceLocator locator = ServiceLocator.getInstance();
			boClass = locator.getBO(inputDTO.get(RegularConstants.PROCESSBO_CLASS));
			object = boClass.newInstance();
		} catch (Exception e) {
			e.printStackTrace();
			String error = e.getLocalizedMessage();
			processResult.setAdditionalInfo(error);
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			return processResult;
		}
		DBContext dbContext = new DBContext();
		try {
			beginTransaction();
			ServiceBO businessObject = (ServiceBO) object;
			businessObject.initialize(inputDTO, dbContext);
			processResult = businessObject.processRequest();
			if (processResult.getProcessStatus().equals(TBAProcessStatus.FAILURE))
				throw new Exception(processResult.getErrorCode());
			commitTransaction();
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
		} catch (Throwable e) {
			e.printStackTrace();
			if (processResult == null) {
				processResult = new TBAProcessResult();
			}
			rollbackTransaction();
			String error = e.getLocalizedMessage();
			processResult.setAdditionalInfo(error);
			processResult.setErrorMessage(e.getLocalizedMessage());
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
		} finally {
			if (dbContext != null)
				dbContext.close();
		}
		return processResult;
	}

	private void beginTransaction() throws Exception {
		try {
			userTransaction = sessionContext.getUserTransaction();
			/* UNLIMITED TIMEOUT */
			userTransaction.setTransactionTimeout(0);
			userTransaction.begin();
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	private void commitTransaction() throws Exception {
		userTransaction.commit();
	}

	private void rollbackTransaction() {
		try {
			userTransaction.rollback();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
