package patterns.config.framework.utils;

import java.sql.Types;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.loggers.ApplicationLogger;
import patterns.config.framework.service.DTObject;

public class AuditOperations {
	private ApplicationLogger logger = null;

	public AuditOperations(DBContext dbcontext) {
		logger = ApplicationLogger.getInstance(this.getClass().getSimpleName());
		this.dbContext=dbcontext;
	}

	public ApplicationLogger getLogger() {
		return logger;
	}

	private DBContext dbContext = null;

	public final DBContext getDbContext() {
		return dbContext;
	}
	public DTObject updateAuditOperations(DTObject inputDTO, String ActionType) {
		DBUtil dbutil = getDbContext().createUtilInstance();
		DTObject resultDTO = new DTObject();
		try {
			dbutil.reset();
			dbutil.setMode(DBUtil.CALLABLE);
			dbutil.setSql("CALL SP_UPDATE_AUDITFIELDS(?,?,?,?,?,?,?,?,?,?,?)");
			dbutil.setString(1, inputDTO.get("ENTITY_CODE"));
			dbutil.setString(2, inputDTO.get("PROCESS_ID"));
			dbutil.setString(3, inputDTO.get("AUDIT_SOURCE_KEY"));
			dbutil.setString(4, ActionType);
			dbutil.setString(5, inputDTO.get("OPER_USER_ID"));
			dbutil.setString(6, inputDTO.get("OPER_DATETIME"));
			dbutil.setString(7, RegularConstants.ZERO);
			dbutil.setString(8, RegularConstants.NULL);
			dbutil.setString(9, RegularConstants.NULL);
			dbutil.setString(10, RegularConstants.EMPTY_STRING);
			dbutil.registerOutParameter(11, Types.VARCHAR);
			dbutil.execute();
			String status = dbutil.getString(11);
			if (!status.equals(RegularConstants.SP_SUCCESS)) {
				resultDTO.setObject(RegularConstants.ERROR, BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
				return resultDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.setObject(RegularConstants.ERROR, BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
		} finally {
			dbutil.reset();
		}
		return resultDTO;
	}

}
