package patterns.config.framework.utils;

import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.loggers.ApplicationLogger;

public class BaseProcess {
	private ApplicationLogger logger = null;

	public BaseProcess(DBContext dbcontext) {
		logger = ApplicationLogger.getInstance(this.getClass().getSimpleName());
		this.dbContext=dbcontext;
	}

	public ApplicationLogger getLogger() {
		return logger;
	}

	private DBContext dbContext = null;

	public final DBContext getDbContext() {
		return dbContext;
	}

}
