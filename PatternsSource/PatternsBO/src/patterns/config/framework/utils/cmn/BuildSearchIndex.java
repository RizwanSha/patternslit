package patterns.config.framework.utils.cmn;

import java.sql.Types;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.loggers.ApplicationLogger;
import patterns.config.framework.service.DTDObject;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.ContentManager;

public class BuildSearchIndex {
	private ApplicationLogger logger = null;

	public BuildSearchIndex(DBContext dbcontext) {
		logger = ApplicationLogger.getInstance(this.getClass().getSimpleName());
		this.dbContext = dbcontext;
	}

	public ApplicationLogger getLogger() {
		return logger;
	}

	private DBContext dbContext = null;

	public final DBContext getDbContext() {
		return dbContext;
	}

	public DTObject createBuildSearch(DTDObject inputDTDO, String action) {
		DBUtil util = getDbContext().createUtilInstance();
		DTObject resultDTO = new DTObject();
		try {
			if (action.equals("M")) {
				util.reset();
				util.setSql("DELETE FROM CUSTOMERNAMEPORTIONIDXCOV WHERE ENTITY_CODE = ? AND CIF_NO= ?");
				util.setMode(DBUtil.PREPARED);
				util.setString(1, inputDTDO.getValue(0, "P_ENTITY"));
				util.setString(2, inputDTDO.getValue(0, "P_CIF_NO"));
				util.executeUpdate();
			}
			short i = 0;
			util.reset();
			util.setMode(DBUtil.CALLABLE);
			util.setSql("CALL SP_CREATE_BUILDNAMEINDEX(?,?,?,?,?)");
			for (; i < inputDTDO.getRowCount(); i++) {
				util.setString(1, inputDTDO.getValue(i, "TYPE"));
				util.setString(2, inputDTDO.getValue(i, "TEXT"));
				util.setString(3, inputDTDO.getValue(i, "P_ENTITY"));
				util.setString(4, inputDTDO.getValue(i, "P_CIF_NO"));
				util.registerOutParameter(5, Types.VARCHAR);
				util.executeUpdate();
				String status = util.getString(5);
				if (!status.equals(RegularConstants.SP_SUCCESS)) {
					resultDTO.setObject(ContentManager.RESULT, RegularConstants.SP_SUCCESS);
					return resultDTO;
				} else {
					resultDTO.setObject(ContentManager.RESULT, RegularConstants.SP_SUCCESS);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.setObject(ContentManager.RESULT, RegularConstants.SP_FAILURE);
		} finally {
			util.reset();
		}
		return resultDTO;
	}
}
