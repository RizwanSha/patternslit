/*
 *
 Author : Raja E
 Created Date : 03-Dec-2014
 Spec Reference : mglmast
 Modification History
 ----------------------------------------------------------------------
 Sl.No		Modified Date	Author			Modified Changes	Version
 ----------------------------------------------------------------------
 
 */

package patterns.config.framework.bo.acnts;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;

public class mglmastBO extends GenericBO {

	TBADynaSQL mglmast = null;

	public mglmastBO() {
	}

	public void init() {
		mglmast = new TBADynaSQL("GLMAST", true);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("GL_HEAD_CODE");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("DESCRIPTION"));
		mglmast.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		mglmast.setField("GL_HEAD_CODE", processData.get("GL_HEAD_CODE"));
		dataExists = mglmast.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				mglmast.setNew(true);
				mglmast.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				mglmast.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				mglmast.setField("PARENT_GL_HEAD_CODE", processData.get("PARENT_GL_HEAD_CODE"));
				mglmast.setField("LAST_IN_HIERARCHY", processData.get("LAST_IN_HIERARCHY"));
				mglmast.setField("ASSET_GL", processData.get("ASSET_GL"));
				mglmast.setField("LIABILITY_GL", processData.get("LIABILITY_GL"));
				mglmast.setField("PRIMARY_A_L", processData.get("PRIMARY_A_L"));
				mglmast.setField("INCOME_GL", processData.get("INCOME_GL"));
				mglmast.setField("EXPENSE_GL", processData.get("EXPENSE_GL"));
				mglmast.setField("PRIMARY_I_E", processData.get("PRIMARY_I_E"));
				mglmast.setField("BANK_AC", processData.get("BANK_AC"));
				mglmast.setField("CASH_AC", processData.get("CASH_AC"));
				mglmast.setField("FIXED_ASSET_AC", processData.get("FIXED_ASSET_AC"));
				mglmast.setField("NOMINAL_AC", processData.get("NOMINAL_AC"));
				mglmast.setField("NOMINAL_ENTRY_TO_BE_CREATED", processData.get("NOMINAL_ENTRY_TO_BE_CREATED"));
				mglmast.setField("REVERSAL_TYPE", processData.get("REVERSAL_TYPE"));
				mglmast.setField("TXN_SUMMARY_TO_BE_PRINTED", processData.get("TXN_SUMMARY_TO_BE_PRINTED"));
				mglmast.setField("GL_OPEN_FROM_DATE", processData.getObject("GL_OPEN_FROM_DATE"));
				mglmast.setField("REMARKS", processData.get("REMARKS"));
				if (mglmast.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mglmast.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				mglmast.setNew(false);
				mglmast.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				mglmast.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				mglmast.setField("PARENT_GL_HEAD_CODE", processData.get("PARENT_GL_HEAD_CODE"));
				mglmast.setField("LAST_IN_HIERARCHY", processData.get("LAST_IN_HIERARCHY"));
				mglmast.setField("ASSET_GL", processData.get("ASSET_GL"));
				mglmast.setField("LIABILITY_GL", processData.get("LIABILITY_GL"));
				mglmast.setField("PRIMARY_A_L", processData.get("PRIMARY_A_L"));
				mglmast.setField("INCOME_GL", processData.get("INCOME_GL"));
				mglmast.setField("EXPENSE_GL", processData.get("EXPENSE_GL"));
				mglmast.setField("PRIMARY_I_E", processData.get("PRIMARY_I_E"));
				mglmast.setField("BANK_AC", processData.get("BANK_AC"));
				mglmast.setField("CASH_AC", processData.get("CASH_AC"));
				mglmast.setField("FIXED_ASSET_AC", processData.get("FIXED_ASSET_AC"));
				mglmast.setField("NOMINAL_AC", processData.get("NOMINAL_AC"));
				mglmast.setField("NOMINAL_ENTRY_TO_BE_CREATED", processData.get("NOMINAL_ENTRY_TO_BE_CREATED"));
				mglmast.setField("REVERSAL_TYPE", processData.get("REVERSAL_TYPE"));
				mglmast.setField("TXN_SUMMARY_TO_BE_PRINTED", processData.get("TXN_SUMMARY_TO_BE_PRINTED"));
				mglmast.setField("GL_OPEN_FROM_DATE", processData.getObject("GL_OPEN_FROM_DATE"));
				mglmast.setField("REMARKS", processData.get("REMARKS"));
				if (mglmast.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mglmast.close();
		}
	}
}