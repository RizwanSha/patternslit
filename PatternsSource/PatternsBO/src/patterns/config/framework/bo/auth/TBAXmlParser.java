package patterns.config.framework.bo.auth;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

public class TBAXmlParser {
	private Document doc = null;
	private Map<String, String> xmlDataMap;
	public TBAXmlParser() {

	}
	public Document parseXML(InputStream is) throws SAXException, IOException, ParserConfigurationException {
		doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(is);
		return doc;
	}
	public Map<String, String> getXmlDataMap() {
		return xmlDataMap;
	}
	public void setXmlDataMap(Map<String, String> xmlDataMap) {
		this.xmlDataMap = xmlDataMap;
	}
	
	public void ParseXMLData(){
		xmlDataMap=new HashMap<String,String>();
		InputStream ins=new InputStream() {
			
			@Override
			public int read() throws IOException {
				// TODO Auto-generated method stub
				return 0;
			}
		};
	}
}
