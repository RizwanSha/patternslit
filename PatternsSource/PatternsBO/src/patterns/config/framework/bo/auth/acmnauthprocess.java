package patterns.config.framework.bo.auth;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.StringTokenizer;

import org.apache.commons.collections.iterators.EntrySetMapIterator;

import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.BindParameterType;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.objects.TableInfo;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBInfo;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.utils.BaseUtility;

public class acmnauthprocess {
	private int errorStatus;
	private String errorMessage;
	private int approvalRejectionFlag;
	private String entityCode;
	private String programId;
	private String sourceKey;
	private Date entryDate;
	private int detailSerial;
	private String approveRejectOption;
	private String userId;
	private String customerId;
	private Date cbd;
	private String ipAddress;
	private DTObject mpgmObject;
	private DTObject tbaAuthQueueObject;
	private DTObject tbaAuthObject;
	private boolean skipUpdate = false;
	private DBContext dbContext = null;
	private long auditlogSl;
	private Timestamp auditlogDateTime;
	private String W_MAIN_PK_WHERE_QUERY;
	private String dataBlock;
	private boolean logRequired = false;
	StringBuffer mainQuery = new StringBuffer();
	StringBuffer detailQuery = new StringBuffer();
	StringBuffer deleteQuery = new StringBuffer();
	private boolean deleteQueryUpdate = false;
	private LinkedHashMap<String, String> auditMap = new LinkedHashMap<String, String>();
	private final String NewDataChar = "N";
	private final String OldDataChar = "O";
	private final String TableValueSep = "@";
	private final String authorized = "A";
	private final String rejected = "R";
	private String ENTRY_MODE = "E";
	private String MODIFY_MODE = "M";
	private HashMap<String, Boolean> deleteQueryMap = new HashMap<String, Boolean>();
	private String tableName = null;
	private String partitionNo;
	private String atlogTBAKey;
	private String newDataBlock;
	private String oldDataBlock;
	private String auditKey;
	private String partitionYear = "";

	public String getPartitionYear() {
		return partitionYear;
	}

	public void setPartitionYear(String partitionYear) {
		this.partitionYear = partitionYear;
	}

	public String getPartitionNo() {
		return partitionNo;
	}

	public void setPartitionNo(String partitionNo) {
		this.partitionNo = partitionNo;
	}

	public DBContext getDbContext() {
		return dbContext;
	}

	public void setDbContext(DBContext dbContext) {
		this.dbContext = dbContext;
	}

	public String getEntityCode() {
		return entityCode;
	}

	public void setEntityCode(String entityCode) {
		this.entityCode = entityCode;
	}

	public String getProgramId() {
		return programId;
	}

	public void setProgramId(String programId) {
		this.programId = programId;
	}

	public String getSourceKey() {
		return sourceKey;
	}

	public void setSourceKey(String sourceKey) {
		this.sourceKey = sourceKey;
	}

	public Date getEntryDate() {
		return entryDate;
	}

	public void setEntryDate(Date entryDate) {
		this.entryDate = entryDate;
	}

	public int getDetailSerial() {
		return detailSerial;
	}

	public void setDetailSerial(int detailSerial) {
		this.detailSerial = detailSerial;
	}

	public String getApproveRejectOption() {
		return approveRejectOption;
	}

	public void setApproveRejectOption(String approveRejectOption) {
		this.approveRejectOption = approveRejectOption;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public Date getCbd() {
		return cbd;
	}

	public void setCbd(Date cbd) {
		this.cbd = cbd;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public long getAuditlogSl() {
		return auditlogSl;
	}

	public void setAuditlogSl(long auditlogSl) {
		this.auditlogSl = auditlogSl;
	}

	public Timestamp getAuditlogDateTime() {
		return auditlogDateTime;
	}

	public void setAuditlogDateTime(Timestamp auditlogDateTime) {
		this.auditlogDateTime = auditlogDateTime;
	}

	public boolean isLogRequired() {
		return logRequired;
	}

	public void setLogRequired(boolean logRequired) {
		this.logRequired = logRequired;
	}

	public void setErrorStatus(int errorStatus) {
		this.errorStatus = errorStatus;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public void setApprovalRejectionFlag(int approvalRejectionFlag) {
		this.approvalRejectionFlag = approvalRejectionFlag;
	}

	public acmnauthprocess() {
	}

	public int getErrorStatus() {
		return errorStatus;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public int getApprovalRejectionFlag() {
		return approvalRejectionFlag;
	}

	public void initialize(String entityCode, String programId, String sourceKey, Date entryDate, int detailSerial, String approveRejectOption, String userId, String customerId, Date cbd, String ipAddress) {
		this.entityCode = entityCode;
		this.programId = programId;
		this.sourceKey = sourceKey;
		this.entryDate = entryDate;
		this.detailSerial = detailSerial;
		this.approveRejectOption = approveRejectOption;
		this.userId = userId;
		this.customerId = customerId;
		this.cbd = cbd;
		this.ipAddress = ipAddress;
	}

	public void processCommonAuthorization() throws TBAFrameworkException {
		errorStatus = 0;
		errorMessage = "";
		if (!checkInputValues()) {
			return;
		}
		getProgramLock();
		getTBAAuthDetails();
		skipUpdate = false;
		approvalRejectionFlag = 1;
		if (approveRejectOption.equals(authorized) && mpgmObject.get("MPGM_DBL_AUTH_REQD").equals(RegularConstants.COLUMN_ENABLE)) {
			// if (tbaAuthObject.get("TBAAUTH_FIRST_AUTH_ON") == null) {
			updateFirstAuthorizationDetails();
			approvalRejectionFlag = 2;
			skipUpdate = true;
			// }

		}
		checkForPartitionTables();
		if (skipUpdate == false) {
			if (approveRejectOption.equals(authorized)) {
				if (mpgmObject.get("MPGM_TYPE").equals("A")) {
					processForAbstrationTypeAuthorization();
				} else if (mpgmObject.get("MPGM_TYPE").equals("M")) {
					processForMechanismTypeAuthorization();
				}
			} else if (approveRejectOption.equals(rejected)) {
				if (mpgmObject.get("MPGM_TYPE").equals("A")) {
					processForAbstrationTypeRejection();
				} else if (mpgmObject.get("MPGM_TYPE").equals("M")) {
					processForMechanismTypeRejection();
				}

			}

			deleteDeDupRecords();

		}

	}

	private void checkForPartitionTables() {
		if (mpgmObject.get("PARTITION_YEAR_WISE").equals("1")) {
			setPartitionYear(BaseUtility.resolvePartitionBucketID(programId, sourceKey, getDbContext()));
		} else {
			setPartitionYear("");
		}

	}

	/*
	 * private void deleteDeDupRecords() { DBUtil dbutil =
	 * dbContext.createUtilInstance(); try { int count = 0; dbutil.reset();
	 * dbutil.setMode(DBUtil.PREPARED); dbutil.setSql(
	 * "SELECT COUNT(1) FROM TBADEDUPCHECK WHERE ENTITY_CODE=? AND PGM_ID=?");
	 * dbutil.setString(1, entityCode); dbutil.setString(2, programId);
	 * ResultSet rs = dbutil.executeQuery(); if (rs.next()) { count =
	 * rs.getInt(1); } if (count > 0) { dbutil.reset();
	 * dbutil.setMode(DBUtil.PREPARED);
	 * dbutil.setSql("DELETE FROM TBADEDUPCHECK WHERE ENTITY_CODE=? AND PGM_ID=?"
	 * ); dbutil.setString(1, entityCode); dbutil.setString(2, programId);
	 * dbutil.executeUpdate(); } } catch (SQLException e) { e.printStackTrace();
	 * } finally { dbutil.reset(); }
	 * 
	 * } private void deleteDeDupRecords() throws TBAFrameworkException{
	 * List<String> purposeIdList=new ArrayList<String>(); DBUtil dbutil =
	 * dbContext.createUtilInstance(); try { dbutil.reset();
	 * dbutil.setMode(DBUtil.PREPARED);
	 * dbutil.setSql("SELECT PURPOSE_ID FROM PGMDEDUPCFG WHERE PGM_ID=?");
	 * dbutil.setString(1, programId); ResultSet rs = dbutil.executeQuery(); if
	 * (rs.next()) { do{ purposeIdList.add(rs.getString(1)); }while(rs.next());
	 * } dbutil.reset(); dbutil.setSql(
	 * "DELETE FROM TBADEDUPCHECK WHERE PARTITION_NO=? AND PGM_ID=? AND PURPOSE_ID=? AND PGM_PK=?"
	 * ); for(String purposeId : purposeIdList){ dbutil.setString(1,
	 * getPartitionNo()); dbutil.setString(2, programId); dbutil.setString(3,
	 * purposeId); dbutil.setString(4, sourceKey); dbutil.executeUpdate(); } }
	 * catch (SQLException e) { throw new
	 * TBAFrameworkException(e.getLocalizedMessage()); } finally {
	 * dbutil.reset(); }
	 * 
	 * }
	 */

	private void deleteDeDupRecords() throws TBAFrameworkException {
		DBUtil dbutil = dbContext.createUtilInstance();
		try {
			dbutil.reset();
			dbutil.setSql("DELETE FROM TBADEDUPCHECK WHERE PARTITION_NO=? AND PGM_ID=?  AND PGM_PK=? AND PURPOSE_ID IN (SELECT PURPOSE_ID FROM PGMDEDUPCFG WHERE PGM_ID=?)");
			dbutil.setString(1, getPartitionNo());
			dbutil.setString(2, programId);
			dbutil.setString(3, sourceKey);
			dbutil.setString(4, programId);
			dbutil.executeUpdate();
		} catch (SQLException e) {
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			dbutil.reset();
		}

	}

	private void startAuditLogAction() {
		DBUtil dbutil = dbContext.createUtilInstance();
		try {
			dbutil.reset();
			dbutil.setMode(DBUtil.CALLABLE);
			dbutil.setSql("{CALL SP_AUDITLOG_START_LOGACTION( ?,?,?,?,?,?,?,?)}");
			dbutil.setString(1, entityCode);
			dbutil.setString(2, programId);
			dbutil.setString(3, sourceKey);
			dbutil.setString(4, userId);
			dbutil.setString(5, tbaAuthQueueObject.get("TBAQ_OPERN_FLG"));
			dbutil.setString(6, atlogTBAKey);
			dbutil.registerOutParameter(7, Types.BIGINT);
			dbutil.registerOutParameter(8, Types.TIMESTAMP);
			dbutil.execute();
			auditlogSl = dbutil.getLong(7);
			auditlogDateTime = dbutil.getTimestamp(8);

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			dbutil.reset();
		}
	}

	private void endAuditLogAction() throws TBAFrameworkException {
		int auditLogDtlsl = 1;
		try {
			Map.Entry IndRec;
			LinkedHashMap<String, String> collectionMap = auditMap;
			if (collectionMap != null) {
				Iterator it = collectionMap.entrySet().iterator();
				while (it.hasNext()) {
					IndRec = ((Map.Entry) it.next());
					if (IndRec != null) {
						saveLoggingDetails(IndRec.getKey().toString(), IndRec.getValue().toString(), auditLogDtlsl);
						auditLogDtlsl = auditLogDtlsl + 1;
					}
				}
			}

		} catch (Exception e) {
			throw new TBAFrameworkException(e.getLocalizedMessage());
		}

	}

	private void saveLoggingDetails(String Key, String dataValue, int auditlogDtlSl) throws TBAFrameworkException {
		String V_TABLE_NAME = "";
		String V_KEY_VALUE = "";
		String V_PK_VALUE = "";
		String V_IMAGE_TYPE = "";
		DBUtil dbutil = dbContext.createUtilInstance();
		try {
			StringTokenizer stnSQLcol = new StringTokenizer(Key, "@");
			if (stnSQLcol.hasMoreTokens())
				V_TABLE_NAME = stnSQLcol.nextToken();
			if (stnSQLcol.hasMoreTokens())
				V_KEY_VALUE = stnSQLcol.nextToken();
			V_IMAGE_TYPE = V_KEY_VALUE.substring(0, 1);
			V_PK_VALUE = V_KEY_VALUE.substring(1);
			dbutil.reset();
			dbutil.setMode(DBUtil.CALLABLE);

			dbutil.setSql("{CALL SP_AUDITLOG_END_LOGACTION( ?,?,?,?,?,?,?,?,?,?)}");
			dbutil.setString(1, entityCode);
			dbutil.setString(2, programId);
			dbutil.setString(3, V_PK_VALUE);
			dbutil.setTimestamp(4, auditlogDateTime);
			dbutil.setLong(5, auditlogSl);
			dbutil.setLong(6, auditlogDtlSl);
			dbutil.setString(7, V_TABLE_NAME);
			dbutil.setString(8, V_IMAGE_TYPE);
			dbutil.setString(9, RegularConstants.EMPTY_STRING);
			dbutil.setString(10, dataValue);

			dbutil.execute();
			dbutil.reset();
		} catch (SQLException e) {
			e.printStackTrace();
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			dbutil.reset();
		}
	}

	private void updateFirstAuthorizationDetails() {
		DBUtil dbutil = dbContext.createUtilInstance();
		try {

			dbutil.reset();
			dbutil.setMode(DBUtil.PREPARED);
			dbutil.setSql("UPDATE TBAAUTHQ TQ   SET TQ.TBAQ_FIRST_AUTH_BY = ?   WHERE ENTITY_CODE = ?  AND TQ.TBAQ_PGM_ID  = ?  AND TQ.TBAQ_MAIN_PK  = ?  AND TQ.TBAQ_ENTRY_DATE    = ?  AND TQ.TBAQ_ENTRY_SL        = ?  ");
			dbutil.setString(1, userId);
			dbutil.setString(2, entityCode);
			dbutil.setString(3, programId);
			dbutil.setString(4, sourceKey);
			dbutil.setDate(5, entryDate);
			dbutil.setInt(6, detailSerial);
			dbutil.executeUpdate();
			updateTBAauthAction(authorized);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			dbutil.reset();
		}

	}

	private boolean checkInputValues() {
		if (userId == null || userId.equals("")) {
			errorStatus = 1;
			errorMessage = "ERROR IN INPUT VALUES - USER ID";
			return false;
		}
		if (programId == null || programId.equals("")) {
			errorStatus = 1;
			errorMessage = "ERROR IN INPUT VALUES - PROGRAM ID";
			return false;
		}
		if (entryDate == null || entryDate.equals("")) {
			errorStatus = 1;
			errorMessage = "ERROR IN INPUT VALUES - ENTRY DATE ";
			return false;
		}
		if (sourceKey == null || sourceKey.equals("")) {
			errorStatus = 1;
			errorMessage = "ERROR IN INPUT VALUES - PRIMARY KEY";
			return false;
		}
		if (detailSerial == 0) {
			errorStatus = 1;
			errorMessage = "ERROR IN INPUT VALUES - DETAIL SERIAL";
			return false;
		}
		if (cbd == null || cbd.equals("")) {
			errorStatus = 1;
			errorMessage = "ERROR IN INPUT VALUES - CURRENT BUSINESS DATE";
			return false;
		}
		if (approveRejectOption == null || approveRejectOption.equals("")) {
			errorStatus = 1;
			errorMessage = "ERROR IN INPUT VALUES - AUTH/REJ OPTION";
			return false;
		}
		return true;
	}

	private void getTBAAuthDetails() throws TBAFrameworkException {
		DBUtil dbutil = dbContext.createUtilInstance();
		try {
			dbutil.reset();
			dbutil.setMode(DBUtil.PREPARED);
			dbutil.setSql("SELECT * FROM TBAAUTHQ T WHERE T.ENTITY_CODE=? AND T.TBAQ_PGM_ID=? AND T.TBAQ_MAIN_PK=? AND T.TBAQ_ENTRY_DATE=? AND T.TBAQ_ENTRY_SL=?  ");
			dbutil.setString(1, entityCode);
			dbutil.setString(2, programId);
			dbutil.setString(3, sourceKey);
			dbutil.setDate(4, entryDate);
			dbutil.setInt(5, detailSerial);
			ResultSet rs = dbutil.executeQuery();
			if (rs.next()) {
				tbaAuthQueueObject = new DTObject();
				for (int i = 1; i < rs.getMetaData().getColumnCount() + 1; i++) {
					tbaAuthQueueObject.set(rs.getMetaData().getColumnName(i), rs.getString(i));
				}
			}
			rs.close();
			if (tbaAuthQueueObject == null) {
				errorStatus = 1;
				errorMessage = "TBAAUTHQ ROW NOT AVAILABLE";
				return;
			}
			dbutil.reset();
			dbutil.setMode(DBUtil.PREPARED);
			dbutil.setSql("SELECT * FROM TBAAUTH T WHERE T.ENTITY_CODE=? AND T.TBAAUTH_PGM_ID=? AND T.TBAAUTH_MAIN_PK=? AND T.TBAAUTH_ENTRY_DATE=? AND T.TBAAUTH_ENTRY_SL=?  ");
			dbutil.setString(1, entityCode);
			dbutil.setString(2, programId);
			dbutil.setString(3, sourceKey);
			dbutil.setDate(4, entryDate);
			dbutil.setInt(5, detailSerial);
			rs = dbutil.executeQuery();
			if (rs.next()) {
				tbaAuthObject = new DTObject();
				atlogTBAKey = rs.getDate("TBAAUTH_ENTRY_DATE") + "|" + rs.getString("TBAAUTH_ENTRY_SL");
				for (int i = 1; i < rs.getMetaData().getColumnCount() + 1; i++) {
					tbaAuthObject.set(rs.getMetaData().getColumnName(i), rs.getString(i));
				}
			}
			rs.close();
			if (tbaAuthObject == null) {
				errorStatus = 1;
				errorMessage = "TBAAUTH ROW NOT AVAILABLE";
				return;
			}
			rs.close();
			dbutil.reset();
			dbutil.setMode(DBUtil.PREPARED);
			dbutil.setSql("SELECT M.MPGM_ID,M.MPGM_TABLE_NAME,M.AUTHORIZE_ALLOWED,M.REJECT_ALLOWED,C.TBA_REQ,C.MPGM_AUTH_REQD,C.MPGM_DBL_AUTH_REQD,C.MPGM_TYPE,C.MPGM_OPERATION,C.CODE_UNIFORMITY_REQD,C.MPGM_TRANSIT_CHOICE,M.PARTITION_YEAR_WISE FROM MPGM M ,MPGMCONFIG C WHERE M.MPGM_ID=? AND M.MPGM_ID=C.MPGM_ID   ");
			dbutil.setString(1, programId);
			rs = dbutil.executeQuery();
			if (rs.next()) {
				mpgmObject = new DTObject();
				tableName = rs.getString("MPGM_TABLE_NAME");
				for (int i = 1; i < rs.getMetaData().getColumnCount() + 1; i++) {
					mpgmObject.set(rs.getMetaData().getColumnName(i), rs.getString(i));
				}
			}
			rs.close();
			if (mpgmObject == null) {
				errorStatus = 1;
				errorMessage = "MPGM ROW NOT AVAILABLE";
				return;
			}

		} catch (Exception e) {
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			dbutil.reset();
		}

	}

	private void processForAbstrationTypeAuthorization() throws TBAFrameworkException {
		DBUtil dbutil = dbContext.createUtilInstance();
		try {
			dbutil.reset();
			dbutil.setMode(DBUtil.PREPARED);
			dbutil.setSql("SELECT TBADTL_TABLE_NAME,TBADTL_BLOCK_SL,TBADTL_DATA_BLOCK,TBADTL_TABLE_TYPE FROM TBAAUTHDTL WHERE ENTITY_CODE = ? AND TBADTL_PGM_ID = ? AND  TBADTL_MAIN_PK = ? AND TBADTL_ENTRY_DATE =?  AND TBADTL_ENTRY_SL = ? ORDER BY TBADTL_TABLE_TYPE DESC, TBADTL_TABLE_NAME,TBADTL_BLOCK_SL ");
			dbutil.setString(1, entityCode);
			dbutil.setString(2, programId);
			dbutil.setString(3, sourceKey);
			dbutil.setDate(4, entryDate);
			dbutil.setInt(5, detailSerial);
			ResultSet rs = dbutil.executeQuery();

			while (rs.next()) {
				tableName = rs.getString("TBADTL_TABLE_NAME");
				if (deleteQueryMap.get(tableName) == null) {
					deleteQueryMap.put(tableName, new Boolean(false));
				}
				processQuery(rs.getString("TBADTL_DATA_BLOCK"), rs.getString("TBADTL_TABLE_NAME"), rs.getString("TBADTL_TABLE_TYPE"), rs.getInt("TBADTL_BLOCK_SL"));
				executeQuery(rs.getString("TBADTL_TABLE_TYPE"));
			}
			dbutil.reset();
			if (tbaAuthObject.get("TBAAUTH_OPERN_FLG").equals("A")) {
				updateAuditOperations(ENTRY_MODE, tbaAuthObject.get("TBAAUTH_DONE_BY"), tbaAuthObject.get("TBAAUTH_DONE_ON"));
			} else {
				updateAuditOperations(MODIFY_MODE, tbaAuthObject.get("TBAAUTH_DONE_BY"), tbaAuthObject.get("TBAAUTH_DONE_ON"));
			}
			deleteTBAQueueDetails(authorized);
			updateAuditOperations(authorized, userId, null);
			setAuditOperationsToAuditLog(NewDataChar);
			
			if (auditMap.size() > 0) {
				if (isLogRequired()) {
					startAuditLogAction();
					endAuditLogAction();
				}
			}

		} catch (Exception e) {
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			dbutil.reset();
		}

	}

	private void processForMechanismTypeAuthorization() throws TBAFrameworkException {
		updateAuthorizationProcess(authorized);
		if (tbaAuthObject.get("TBAAUTH_OPERN_FLG").equals("A")) {
			updateAuditOperations(ENTRY_MODE, tbaAuthObject.get("TBAAUTH_DONE_BY"), tbaAuthObject.get("TBAAUTH_DONE_ON"));
		} else {
			updateAuditOperations(MODIFY_MODE, tbaAuthObject.get("TBAAUTH_DONE_BY"), tbaAuthObject.get("TBAAUTH_DONE_ON"));
		}
		deleteTBAQueueDetails(authorized);
		updateAuditOperations(authorized, userId, null);
		setAuditOperationsToAuditLog(NewDataChar);
		if (auditMap.size() > 0) {
			if (isLogRequired()) {
				startAuditLogAction();
				endAuditLogAction();
			}
		}

	}

	private void processForMechanismTypeRejection() throws TBAFrameworkException {
		updateRejectionProcess(rejected);
		deleteTBAQueueDetails(rejected);

	}

	private void updateAuditOperations(String actionType, String operUserId, String operDateTime) {
		DBUtil dbutil = dbContext.createUtilInstance();
		try {
			dbutil.reset();
			dbutil.setMode(DBUtil.CALLABLE);
			dbutil.setSql("CALL SP_UPDATE_AUDITFIELDS(?,?,?,?,?,?,?,?,?,?,?)");
			dbutil.setString(1, entityCode);
			dbutil.setString(2, programId);
			dbutil.setString(3, sourceKey);
			dbutil.setString(4, actionType);
			dbutil.setString(5, operUserId);
			dbutil.setString(6, operDateTime);
			dbutil.setString(7, "1");
			dbutil.setDate(8, entryDate);
			dbutil.setInt(9, detailSerial);
			dbutil.setString(10, getPartitionYear());
			dbutil.registerOutParameter(11, Types.VARCHAR);
			dbutil.execute();
			String status = dbutil.getString(11);
			errorMessage = status;

		} catch (Exception e) {
			e.printStackTrace();
			errorMessage = e.getLocalizedMessage();
		} catch (Throwable e) {
			e.printStackTrace();
			errorMessage = e.getLocalizedMessage();
		} finally {
			dbutil.reset();
		}
	}

	private void setAuditOperationsToAuditLog(String imageType) {
		StringBuffer buffer = new StringBuffer();
		buffer.append("<ROWS>");
		DBUtil dbutil = dbContext.createUtilInstance();
		try {
			dbutil.reset();
			dbutil.setMode(DBUtil.CALLABLE);
			dbutil.setSql("SELECT * FROM FWRKOPERATIONS WHERE ENTITY_CODE=? AND FORM_NAME=? AND FORM_PK=?");
			dbutil.setString(1, entityCode);
			dbutil.setString(2, programId);
			dbutil.setString(3, sourceKey);
			ResultSet rs = dbutil.executeQuery();
			int columnCount = rs.getMetaData().getColumnCount();
			while (rs.next()) {
				buffer.append("<ROW>");
				for (int i = 1; i <= columnCount; i++) {
					if (rs.getMetaData().getColumnTypeName(i).equals("DATE")) {
						Date value = rs.getDate(i);
						if (value != null) {
							buffer.append("<").append(rs.getMetaData().getColumnName(i)).append("><![CDATA[").append(BackOfficeFormatUtils.getDate((java.util.Date) value, BackOfficeConstants.TBA_XML_DATE_FORMAT)).append("]]></").append(rs.getMetaData().getColumnName(i)).append(">");
						} else {
							buffer.append("<").append(rs.getMetaData().getColumnName(i)).append(" N=\"N\" />");
						}
					} else {
						String value = rs.getString(i);
						if (value == null) {
							buffer.append("<").append(rs.getMetaData().getColumnName(i)).append(" N=\"N\" />");
						} else {
							buffer.append("<").append(rs.getMetaData().getColumnName(i)).append("><![CDATA[").append(value).append("]]></").append(rs.getMetaData().getColumnName(i)).append(">");
						}
					}
				}
				buffer.append("</ROW>");
			}
			buffer.append("</ROWS>");
			buffer.trimToSize();
			String auditKey = "";
			auditKey = "FWRKOPERATIONS" + TableValueSep + imageType + sourceKey;
			auditMap.put(auditKey, buffer.toString());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void executeQuery(String tableType) throws TBAFrameworkException {
		DBUtil dbutil = dbContext.createUtilInstance();
		try {

			if (tableType.equals("S")) {
				dbutil.reset();
				dbutil.setMode(DBUtil.PREPARED);
				dbutil.setSql(mainQuery.toString());
				System.out.println(mainQuery.toString());
				dbutil.executeUpdate();
			} else if (tableType.equals("M")) {
				if (tbaAuthObject.get("TBAAUTH_OPERN_FLG").equals("M")) {
					deleteQueryUpdate = deleteQueryMap.get(tableName).booleanValue();
					if (!deleteQueryUpdate) {
						dbutil.reset();
						dbutil.setMode(DBUtil.PREPARED);
						dbutil.setSql(deleteQuery.toString());
						dbutil.executeUpdate();
						deleteQueryMap.put(tableName, new Boolean(true));
					}
				}
				dbutil.reset();
				dbutil.setMode(DBUtil.PREPARED);
				dbutil.setSql(detailQuery.toString());
				dbutil.executeUpdate();
			}

		} catch (Exception e) {
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			dbutil.reset();
		}

	}

	private void processQuery(String dataBlock, String tableName, String tableType, int blockSerial) {
		DBInfo info = new DBInfo();
		TableInfo tableInfo = info.getTableInfo(tableName);
		String[] fieldNames = tableInfo.getColumnInfo().getColumnNames();
		Map<String, BindParameterType> columnType = tableInfo.getColumnInfo().getColumnMapping();
		String[] keyFields = tableInfo.getPrimaryKeyInfo().getColumnNames();
		BindParameterType[] keyFieldsColumnType = tableInfo.getPrimaryKeyInfo().getColumnTypes();
		String[] pkInfo = sourceKey.split("\\" + RegularConstants.PK_SEPARATOR);
		StringBuffer query = new StringBuffer();
		detailQuery = new StringBuffer();
		deleteQuery = new StringBuffer();
		mainQuery = new StringBuffer();// added by jayashree as on 16/07/2015
		String auditKey = "";
		newDataBlock = dataBlock;
		if (tableType.equals("S")) {
			if (tbaAuthObject.get("TBAAUTH_OPERN_FLG").equals("A")) {
				query.append("INSERT INTO " + tableName + " SELECT ");
				for (String fieldName : fieldNames) {
					if (columnType.get(fieldName).equals(BindParameterType.DATE) || columnType.get(fieldName).equals(BindParameterType.DATETIME) || columnType.get(fieldName).equals(BindParameterType.TIMESTAMP)) {
						query.append("IF(EXTRACTVALUE('" + dataBlock + "','//" + fieldName + "')='',NULL," + " TO_DATETIME(EXTRACTVALUE('" + dataBlock + "','//" + fieldName + "'),'" + BackOfficeConstants.TBA_XML_DATE_FORMAT + "'))" + ",");

					}  else if (columnType.get(fieldName).equals(BindParameterType.INTEGER) || columnType.get(fieldName).equals(BindParameterType.BIGINT) || columnType.get(fieldName).equals(BindParameterType.BIGDECIMAL) || columnType.get(fieldName).equals(BindParameterType.DECIMAL)) {
						query.append(" IF(EXTRACTVALUE('" + dataBlock + "','//" + fieldName + "')='',NULL," + "EXTRACTVALUE('" + dataBlock + "','//" + fieldName + "')),");
					} else {
						query.append(" EXTRACTVALUE('" + dataBlock + "','//" + fieldName + "')" + ",");
					}
				}
				query.trimToSize();
				mainQuery.append(query.substring(0, query.length() - 1));
				mainQuery.append(" FROM DUAL");
				auditKey = tableName + TableValueSep + NewDataChar + sourceKey;
				auditMap.put(auditKey, newDataBlock);
			} else if (tbaAuthObject.get("TBAAUTH_OPERN_FLG").equals("M")) {
				oldDataBlock = getImageData(tableName, keyFields, keyFieldsColumnType, sourceKey);
				query.append("UPDATE  " + tableName + " SET  ");
				for (String fieldName : fieldNames) {
				 if (columnType.get(fieldName).equals(BindParameterType.DATE) || columnType.get(fieldName).equals(BindParameterType.DATETIME) || columnType.get(fieldName).equals(BindParameterType.TIMESTAMP)) {
						query.append(fieldName).append(" = IF(EXTRACTVALUE('" + dataBlock + "','//" + fieldName + "')='',NULL," + " TO_DATETIME(EXTRACTVALUE('" + dataBlock + "','//" + fieldName + "'),'" + BackOfficeConstants.TBA_XML_DATE_FORMAT + "'))" + ",");

					} else if (columnType.get(fieldName).equals(BindParameterType.INTEGER) || columnType.get(fieldName).equals(BindParameterType.BIGINT) || columnType.get(fieldName).equals(BindParameterType.BIGDECIMAL) || columnType.get(fieldName).equals(BindParameterType.DECIMAL)) {
						query.append(fieldName).append(" = IF(EXTRACTVALUE('" + dataBlock + "','//" + fieldName + "')='',NULL," + "EXTRACTVALUE('" + dataBlock + "','//" + fieldName + "')),");
					} else {
						query.append(fieldName).append(" =  EXTRACTVALUE('" + dataBlock + "','//" + fieldName + "')" + ",");
					}
				}
				query.trimToSize();
				mainQuery.append(query.substring(0, query.length() - 1));
				if (keyFields.length > 0)
					mainQuery.append(" WHERE ");
				int i = 0;
				for (String fieldName : keyFields) {

					if (keyFieldsColumnType[i] == BindParameterType.DATE) {
						if (i < keyFields.length - 1) {
							mainQuery.append(fieldName).append(" = TO_DATE('" + pkInfo[i] + "','" + BackOfficeConstants.TBA_XML_DATE_FORMAT + "')  AND ");
						} else {
							mainQuery.append(fieldName).append(" = TO_DATE('" + pkInfo[i] + "','" + BackOfficeConstants.TBA_XML_DATE_FORMAT + "')");
						}
					} else {
						if (i < keyFields.length - 1) {
							mainQuery.append(fieldName).append(" = '" + pkInfo[i] + "'  AND ");
						} else {
							mainQuery.append(fieldName).append(" = '" + pkInfo[i] + "'");
						}
					}

					++i;
				}
				auditKey = tableName + TableValueSep + OldDataChar + sourceKey;
				auditMap.put(auditKey, oldDataBlock);
				setAuditOperationsToAuditLog(OldDataChar);
				auditKey = tableName + TableValueSep + NewDataChar + sourceKey;
				auditMap.put(auditKey, newDataBlock);

			}
		} else if (tableType.equals("M")) {
			query = new StringBuffer();
			String dtlPkInfo = getDtlTablePkInfo(dataBlock, keyFields);
			if (tbaAuthObject.get("TBAAUTH_OPERN_FLG").equals("M")) {

				deleteQueryUpdate = deleteQueryMap.get(tableName).booleanValue();
				if (!deleteQueryUpdate) {
					getOldValuesforAudit(tableName, keyFields, keyFieldsColumnType, sourceKey);
					deleteQuery.append("DELETE FROM " + tableName + " WHERE ");
					int i = 0;
					for (i = 0; i < keyFields.length - 1; i++) {

						if (keyFieldsColumnType[i] == BindParameterType.DATE) {
							if (i < keyFields.length - 2) {
								deleteQuery.append(keyFields[i]).append(" = TO_DATE('" + pkInfo[i] + "','" + BackOfficeConstants.TBA_XML_DATE_FORMAT + "')  AND ");
							} else {
								deleteQuery.append(keyFields[i]).append(" = TO_DATE('" + pkInfo[i] + "','" + BackOfficeConstants.TBA_XML_DATE_FORMAT + "')");
							}
						} else {
							if (i < keyFields.length - 2) {
								deleteQuery.append(keyFields[i]).append(" = '" + pkInfo[i] + "'  AND ");
							} else {
								deleteQuery.append(keyFields[i]).append(" = '" + pkInfo[i] + "'");
							}
						}

					}

				}
			}
			query.append("INSERT INTO " + tableName + " SELECT ");
			for (String fieldName : fieldNames) {
				if (columnType.get(fieldName).equals(BindParameterType.DATE) || columnType.get(fieldName).equals(BindParameterType.DATETIME) || columnType.get(fieldName).equals(BindParameterType.TIMESTAMP)) {
					query.append("IF(EXTRACTVALUE('" + dataBlock + "','//" + fieldName + "')='',NULL," + " TO_DATETIME(EXTRACTVALUE('" + dataBlock + "','//" + fieldName + "'),'" + BackOfficeConstants.TBA_XML_DATE_FORMAT + "'))" + ",");

				} else if (columnType.get(fieldName).equals(BindParameterType.INTEGER) || columnType.get(fieldName).equals(BindParameterType.BIGINT) || columnType.get(fieldName).equals(BindParameterType.BIGDECIMAL) || columnType.get(fieldName).equals(BindParameterType.DECIMAL)) {
					query.append(" IF(EXTRACTVALUE('" + dataBlock + "','//" + fieldName + "')='',NULL," + "EXTRACTVALUE('" + dataBlock + "','//" + fieldName + "')),");
				} else {
					query.append(" EXTRACTVALUE('" + dataBlock + "','//" + fieldName + "')" + ",");
				}
			}
			query.trimToSize();
			detailQuery.append(query.substring(0, query.length() - 1));
			detailQuery.append(" FROM DUAL");

			auditKey = tableName + TableValueSep + NewDataChar + sourceKey + TableValueSep + dtlPkInfo;
			auditMap.put(auditKey, newDataBlock);
		}

	}

	private void getOldValuesforAudit(String tableName, String[] keyFields, BindParameterType[] keyFieldsColumnType, String sourceKey) {
		StringBuffer sqlQuery = new StringBuffer();
		StringBuffer buffer = new StringBuffer();

		sqlQuery.append("SELECT * FROM ").append(tableName).append(" WHERE ");
		int i = 0;
		sqlQuery.trimToSize();
		DBUtil util = dbContext.createUtilInstance();
		util.reset();
		try {
			util.setSql(sqlQuery.toString());
			String[] pkInfo = sourceKey.split("\\" + RegularConstants.PK_SEPARATOR);
			i = 0;
			for (String fieldName : keyFields) {

				if (keyFieldsColumnType[i] == BindParameterType.DATE) {
					if (i < keyFields.length - 2) {
						sqlQuery.append(fieldName).append(" = TO_DATE('" + pkInfo[i] + "','" + BackOfficeConstants.TBA_XML_DATE_FORMAT + "')  AND ");
					} else {
						sqlQuery.append(fieldName).append(" = TO_DATE('" + pkInfo[i] + "','" + BackOfficeConstants.TBA_XML_DATE_FORMAT + "')");
					}
				} else {
					if (i < keyFields.length - 2) {
						sqlQuery.append(fieldName).append(" = '" + pkInfo[i] + "'  AND ");
					} else {
						sqlQuery.append(fieldName).append(" = '" + pkInfo[i] + "'");
					}
				}

				++i;
			}
			sqlQuery.append("");
			util.setSql(sqlQuery.toString());
			ResultSet rs = util.executeQuery();
			String auditKey = "";
			int columnCount = rs.getMetaData().getColumnCount();
			int j = 1;
			while (rs.next()) {
				buffer = new StringBuffer();
				buffer.append("<ROW>");
				for (i = 1; i <= columnCount; i++) {
					if (rs.getMetaData().getColumnTypeName(i).equals("DATE")) {
						Date value = rs.getDate(i);
						if (value != null) {
							buffer.append("<").append(rs.getMetaData().getColumnName(i)).append("><![CDATA[").append(BackOfficeFormatUtils.getDate((java.util.Date) value, BackOfficeConstants.TBA_XML_DATE_FORMAT)).append("]]></").append(rs.getMetaData().getColumnName(i)).append(">");
						} else {
							buffer.append("<").append(rs.getMetaData().getColumnName(i)).append(" N=\"N\" />");
						}
					} else {
						String value = rs.getString(i);
						if (value == null) {
							buffer.append("<").append(rs.getMetaData().getColumnName(i)).append(" N=\"N\" />");
						} else {
							buffer.append("<").append(rs.getMetaData().getColumnName(i)).append("><![CDATA[").append(value).append("]]></").append(rs.getMetaData().getColumnName(i)).append(">");
						}
					}

				}
				buffer.append("</ROW>");
				buffer.trimToSize();
				auditKey = tableName + TableValueSep + OldDataChar + sourceKey + TableValueSep + rs.getString(keyFields[keyFields.length - 1]);
				auditMap.put(auditKey, buffer.toString());
				j++;
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			util.reset();
		}

	}

	private String getImageData(String tableName, String[] keyFields, BindParameterType[] keyFieldsColumnType, String sourceKey) {
		StringBuffer sqlQuery = new StringBuffer();
		StringBuffer buffer = new StringBuffer();
		buffer.append("<ROW>");
		sqlQuery.append("SELECT * FROM ").append(tableName).append(" WHERE ");
		int i = 0;
		sqlQuery.trimToSize();
		DBUtil util = dbContext.createUtilInstance();
		util.reset();
		try {
			util.setSql(sqlQuery.toString());
			String[] pkInfo = sourceKey.split("\\" + RegularConstants.PK_SEPARATOR);
			i = 0;
			for (String fieldName : keyFields) {

				if (keyFieldsColumnType[i] == BindParameterType.DATE) {
					if (i < keyFields.length - 1) {
						sqlQuery.append(fieldName).append(" = TO_DATE('" + pkInfo[i] + "','" + BackOfficeConstants.TBA_XML_DATE_FORMAT + "')  AND ");
					} else {
						sqlQuery.append(fieldName).append(" = TO_DATE('" + pkInfo[i] + "','" + BackOfficeConstants.TBA_XML_DATE_FORMAT + "')");
					}
				} else {
					if (i < keyFields.length - 1) {
						sqlQuery.append(fieldName).append(" = '" + pkInfo[i] + "'  AND ");
					} else {
						sqlQuery.append(fieldName).append(" = '" + pkInfo[i] + "'");
					}
				}

				++i;
			}
			util.setSql(sqlQuery.toString());
			ResultSet rs = util.executeQuery();
			int columnCount = rs.getMetaData().getColumnCount();
			if (rs.next()) {
				for (i = 1; i <= columnCount; i++) {
					if (rs.getMetaData().getColumnTypeName(i).equals("DATE")) {
						Date value = rs.getDate(i);
						if (value != null) {
							buffer.append("<").append(rs.getMetaData().getColumnName(i)).append("><![CDATA[").append(BackOfficeFormatUtils.getDate((java.util.Date) value, BackOfficeConstants.TBA_XML_DATE_FORMAT)).append("]]></").append(rs.getMetaData().getColumnName(i)).append(">");
						} else {
							buffer.append("<").append(rs.getMetaData().getColumnName(i)).append(" N=\"N\" />");
						}
					} else {
						String value = rs.getString(i);
						if (value == null) {
							buffer.append("<").append(rs.getMetaData().getColumnName(i)).append(" N=\"N\" />");
						} else {
							buffer.append("<").append(rs.getMetaData().getColumnName(i)).append("><![CDATA[").append(value).append("]]></").append(rs.getMetaData().getColumnName(i)).append(">");
						}
					}

				}
			}
			buffer.append("</ROW>");
			buffer.trimToSize();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			util.reset();
		}
		return buffer.toString();
	}

	private String getDtlTablePkInfo(String dataBlock, String[] keyFields) {
		DBUtil dbutil = dbContext.createUtilInstance();
		String pkInfo = "";
		String query = "";
		query += ("SELECT concat(");
		try {
			for (String fieldName : keyFields) {
				query += " EXTRACTVALUE('" + dataBlock + "','//" + fieldName + "')" + ",'|',";
			}
			query = query.substring(0, query.length() - 5);
			query += " ) FROM DUAL";
			dbutil.reset();
			dbutil.setMode(DBUtil.PREPARED);
			dbutil.setSql(query);
			ResultSet rs = dbutil.executeQuery();
			if (rs.next()) {
				pkInfo = rs.getString(1);
			}
			rs.close();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			dbutil.reset();
		}
		return pkInfo;
	}

	private void processForAbstrationTypeRejection() throws TBAFrameworkException {
		DBUtil dbutil = dbContext.createUtilInstance();
		try {

			dbutil.reset();
			dbutil.setMode(DBUtil.PREPARED);
			dbutil.setSql("DELETE FROM TBAAUTHQ WHERE ENTITY_CODE     = ?  AND TBAQ_PGM_ID  = ?  AND TBAQ_MAIN_PK  = ?  AND TBAQ_ENTRY_DATE    = ?  AND TBAQ_ENTRY_SL        = ?  ");
			dbutil.setString(1, entityCode);
			dbutil.setString(2, programId);
			dbutil.setString(3, sourceKey);
			dbutil.setDate(4, entryDate);
			dbutil.setInt(5, detailSerial);
			dbutil.executeUpdate();
			dbutil.reset();
			dbutil.setMode(DBUtil.PREPARED);
			dbutil.setSql("UPDATE TBAAUTH T   SET T.TBAAUTH_STATUS = 'R' WHERE T.ENTITY_CODE=? AND T.TBAAUTH_PGM_ID=? AND T.TBAAUTH_MAIN_PK=? AND T.TBAAUTH_ENTRY_DATE=? AND T.TBAAUTH_ENTRY_SL=?  ");
			dbutil.setString(1, entityCode);
			dbutil.setString(2, programId);
			dbutil.setString(3, sourceKey);
			dbutil.setDate(4, entryDate);
			dbutil.setInt(5, detailSerial);
			dbutil.executeUpdate();
			updateTBAauthAction(rejected);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			dbutil.reset();
		}

	}

	private void updateTBAauthAction(String action) {
		DBUtil dbutil = dbContext.createUtilInstance();
		int actionSerial = 0;
		try {

			dbutil.reset();
			dbutil.setMode(DBUtil.PREPARED);
			dbutil.setSql(" SELECT IFNULL(MAX(TBAAUTHA_ACTION_SL),0)+ 1 FROM  TBAAUTHACT WHERE ENTITY_CODE=? AND TBAAUTHA_PGM_ID=? AND TBAAUTHA_MAIN_PK=? AND TBAAUTHA_ENTRY_DATE=? AND TBAAUTHA_ENTRY_SL=? ");
			dbutil.setString(1, entityCode);
			dbutil.setString(2, programId);
			dbutil.setString(3, sourceKey);
			dbutil.setDate(4, entryDate);
			dbutil.setInt(5, detailSerial);
			ResultSet rs = dbutil.executeQuery();
			if (rs.next()) {
				actionSerial = rs.getInt(1);
			}
			dbutil.reset();
			dbutil.setMode(DBUtil.PREPARED);
			dbutil.setSql("INSERT INTO TBAAUTHACT  (ENTITY_CODE,TBAAUTHA_PGM_ID,TBAAUTHA_MAIN_PK,TBAAUTHA_ENTRY_DATE,TBAAUTHA_ENTRY_SL,TBAAUTHA_ACTION_SL,TBAAUTHA_ACTION,TBAAUTHA_ACTION_BY,TBAAUTHA_ACTION_ON) VALUES (?,?,?,?,?,?,?,?,?)");
			dbutil.setString(1, entityCode);
			dbutil.setString(2, programId);
			dbutil.setString(3, sourceKey);
			dbutil.setDate(4, entryDate);
			dbutil.setInt(5, detailSerial);
			dbutil.setInt(6, actionSerial);
			dbutil.setString(7, action);
			dbutil.setString(8, userId);
			dbutil.setDate(9, cbd);
			dbutil.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			dbutil.reset();
		}

	}

	private void updateAuthorizationProcess(String status) throws TBAFrameworkException {
		DBUtil dbutil = dbContext.createUtilInstance();
		if (tbaAuthObject.get("TBAAUTH_OPERN_FLG").equals("M")) {
			if (mpgmObject.get("MPGM_TRANSIT_CHOICE").equals("2")) {
				// move new image to source
				moveTBAImageToSouce();
			}
		}
		tableName = mpgmObject.get("MPGM_TABLE_NAME");
		DBInfo info = new DBInfo();
		TableInfo tableInfo = info.getTableInfo(tableName);
		String[] keyFields = tableInfo.getPrimaryKeyInfo().getColumnNames();
		BindParameterType[] keyFieldsColumnType = tableInfo.getPrimaryKeyInfo().getColumnTypes();
		StringBuffer sqlQuery = new StringBuffer();
		try {
			String[] pkInfo = sourceKey.split("\\" + RegularConstants.PK_SEPARATOR);
			sqlQuery.append("UPDATE ").append(tableName).append(" SET ");
			sqlQuery.append("E_STATUS").append(" = '" + status + "' ,AUTH_ON=SYSDATE() , TBA_KEY = NULL");
			sqlQuery.trimToSize();
			if (keyFields.length > 0)
				sqlQuery.append(" WHERE ");
			int i = 0;
			for (String fieldName : keyFields) {

				if (keyFieldsColumnType[i] == BindParameterType.DATE) {
					if (i < keyFields.length - 1) {
						sqlQuery.append(fieldName).append(" = TO_DATE('" + pkInfo[i] + "','" + BackOfficeConstants.TBA_XML_DATE_FORMAT + "')  AND ");
					} else {
						sqlQuery.append(fieldName).append(" = TO_DATE('" + pkInfo[i] + "','" + BackOfficeConstants.TBA_XML_DATE_FORMAT + "')");
					}
				} else {
					if (i < keyFields.length - 1) {
						sqlQuery.append(fieldName).append(" = '" + pkInfo[i] + "'  AND ");
					} else {
						sqlQuery.append(fieldName).append(" = '" + pkInfo[i] + "'");
					}
				}

				++i;
			}
			dbutil.reset();
			dbutil.setMode(DBUtil.PREPARED);
			dbutil.setSql(sqlQuery.toString());
			dbutil.executeUpdate();
			String auditKey = "";
			if (tbaAuthObject.get("TBAAUTH_OPERN_FLG").equals("A")) {
				newDataBlock = getImageData(tableName, keyFields, keyFieldsColumnType, sourceKey);
				auditKey = tableName + TableValueSep + NewDataChar + sourceKey;
				auditMap.put(auditKey, newDataBlock);
			} else {
				if (mpgmObject.get("MPGM_TRANSIT_CHOICE").equals("1")) {
					// get old image from TBAAUTHDTL table
					getTBAOldSourceImage();

					setAuditOperationsToAuditLog(OldDataChar);
				}
				newDataBlock = getImageData(tableName, keyFields, keyFieldsColumnType, sourceKey);
				auditKey = tableName + TableValueSep + NewDataChar + sourceKey;
				auditMap.put(auditKey, newDataBlock);
			}

		} catch (SQLException e) {
			e.printStackTrace();
			errorMessage = e.getLocalizedMessage();
		} finally {
			dbutil.reset();
		}
	}

	private void getTBAOldSourceImage() throws TBAFrameworkException {
		DBUtil dbutil = dbContext.createUtilInstance();
		String blockImage = "";
		try {

			dbutil.reset();
			dbutil.setMode(DBUtil.PREPARED);
			dbutil.setSql("SELECT TBADTL_TABLE_NAME,TBADTL_BLOCK_SL,TBADTL_DATA_BLOCK,TBADTL_TABLE_TYPE FROM TBAAUTHDTL WHERE ENTITY_CODE = ? AND TBADTL_PGM_ID = ? AND  TBADTL_MAIN_PK = ? AND TBADTL_ENTRY_DATE =?  AND TBADTL_ENTRY_SL = ? ORDER BY TBADTL_TABLE_TYPE DESC, TBADTL_TABLE_NAME,TBADTL_BLOCK_SL ");
			dbutil.setString(1, entityCode);
			dbutil.setString(2, programId);
			dbutil.setString(3, sourceKey);
			dbutil.setDate(4, entryDate);
			dbutil.setInt(5, detailSerial);
			ResultSet rs = dbutil.executeQuery();

			while (rs.next()) {
				blockImage = rs.getString("TBADTL_DATA_BLOCK");
				auditKey = rs.getString("TBADTL_TABLE_NAME") + TableValueSep + OldDataChar + sourceKey + TableValueSep + rs.getString("TBADTL_BLOCK_SL");
				auditMap.put(auditKey, blockImage);
			}
		} catch (Exception e) {
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			dbutil.reset();
		}
	}

	private void updateRejectionProcess(String status) throws TBAFrameworkException {
		DBUtil dbutil = dbContext.createUtilInstance();
		if (tbaAuthObject.get("TBAAUTH_OPERN_FLG").equals("M")) {
			if (mpgmObject.get("MPGM_TRANSIT_CHOICE").equals("1")) {
				// move old image to source
				moveTBAImageToSouce();
			}
		} else {
			DBInfo info = new DBInfo();
			TableInfo tableInfo = info.getTableInfo(tableName);
			String[] keyFields = tableInfo.getPrimaryKeyInfo().getColumnNames();
			BindParameterType[] keyFieldsColumnType = tableInfo.getPrimaryKeyInfo().getColumnTypes();
			StringBuffer sqlQuery = new StringBuffer();
			try {
				String[] pkInfo = sourceKey.split("\\" + RegularConstants.PK_SEPARATOR);
				sqlQuery.append("UPDATE ").append(tableName).append(" SET ");
				sqlQuery.append("E_STATUS").append(" = '" + status + "' ,REJ_ON=SYSDATE() , TBA_KEY = NULL");
				sqlQuery.trimToSize();
				if (keyFields.length > 0)
					sqlQuery.append(" WHERE ");
				int i = 0;
				for (String fieldName : keyFields) {

					if (keyFieldsColumnType[i] == BindParameterType.DATE) {
						if (i < keyFields.length - 1) {
							sqlQuery.append(fieldName).append(" = TO_DATE('" + pkInfo[i] + "','" + BackOfficeConstants.TBA_XML_DATE_FORMAT + "')  AND ");
						} else {
							sqlQuery.append(fieldName).append(" = TO_DATE('" + pkInfo[i] + "','" + BackOfficeConstants.TBA_XML_DATE_FORMAT + "')");
						}
					} else {
						if (i < keyFields.length - 1) {
							sqlQuery.append(fieldName).append(" = '" + pkInfo[i] + "'  AND ");
						} else {
							sqlQuery.append(fieldName).append(" = '" + pkInfo[i] + "'");
						}
					}

					++i;
				}
				dbutil.reset();
				dbutil.setMode(DBUtil.PREPARED);
				dbutil.setSql(sqlQuery.toString());
				dbutil.executeUpdate();
			} catch (SQLException e) {
				e.printStackTrace();
				errorMessage = e.getLocalizedMessage();
			} finally {
				dbutil.reset();
			}
		}
	}

	private void moveTBAImageToSouce() throws TBAFrameworkException {
		DBUtil dbutil = dbContext.createUtilInstance();
		try {
			dbutil.reset();
			dbutil.setMode(DBUtil.PREPARED);
			dbutil.setSql("SELECT TBADTL_TABLE_NAME,TBADTL_BLOCK_SL,TBADTL_DATA_BLOCK,TBADTL_TABLE_TYPE FROM TBAAUTHDTL WHERE ENTITY_CODE = ? AND TBADTL_PGM_ID = ? AND  TBADTL_MAIN_PK = ? AND TBADTL_ENTRY_DATE =?  AND TBADTL_ENTRY_SL = ? ORDER BY TBADTL_TABLE_TYPE DESC, TBADTL_TABLE_NAME,TBADTL_BLOCK_SL ");
			dbutil.setString(1, entityCode);
			dbutil.setString(2, programId);
			dbutil.setString(3, sourceKey);
			dbutil.setDate(4, entryDate);
			dbutil.setInt(5, detailSerial);
			ResultSet rs = dbutil.executeQuery();

			while (rs.next()) {
				tableName = rs.getString("TBADTL_TABLE_NAME");
				if (deleteQueryMap.get(tableName) == null) {
					deleteQueryMap.put(tableName, new Boolean(false));
				}
				processQuery(rs.getString("TBADTL_DATA_BLOCK"), rs.getString("TBADTL_TABLE_NAME"), rs.getString("TBADTL_TABLE_TYPE"), rs.getInt("TBADTL_BLOCK_SL"));
				executeQuery(rs.getString("TBADTL_TABLE_TYPE"));
			}
		} catch (Exception e) {
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			dbutil.reset();
		}

	}

	private void getProgramLock() throws TBAFrameworkException {
		DBUtil dbutil = dbContext.createUtilInstance();
		try {
			dbutil.reset();
			dbutil.setMode(DBUtil.CALLABLE);
			dbutil.setSql("CALL SP_ACQUIRE_LOCK( ?,?,?,?,?,?)");
			dbutil.setString(1, entityCode);
			dbutil.setString(2, programId);
			dbutil.registerOutParameter(3, Types.INTEGER);
			dbutil.registerOutParameter(4, Types.INTEGER);
			dbutil.registerOutParameter(5, Types.INTEGER);
			dbutil.registerOutParameter(6, Types.VARCHAR);
			dbutil.execute();
			int V_LOG_REQ = dbutil.getInt(3);
			int V_ADD_LOG_REQ = dbutil.getInt(4);
			int V_LOCK_STATUS = dbutil.getInt(5);
			String V_LOCK_ERR_MSG = dbutil.getString(6) == RegularConstants.NULL ? RegularConstants.EMPTY_STRING : dbutil.getString(6);
			if (V_LOG_REQ == 1)
				logRequired = true;
			else
				logRequired = false;

			if (V_LOCK_STATUS == 1)
				throw new TBAFrameworkException(V_LOCK_ERR_MSG);

		} catch (Exception e) {
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			dbutil.reset();
		}
	}

	private void deleteTBAQueueDetails(String status) {
		DBUtil dbutil = dbContext.createUtilInstance();
		try {

			dbutil.reset();
			dbutil.setMode(DBUtil.PREPARED);
			dbutil.setSql("DELETE FROM  TBAAUTHQ    WHERE ENTITY_CODE= ?  AND TBAQ_PGM_ID= ?  AND TBAQ_MAIN_PK= ?  AND TBAQ_ENTRY_DATE= ?  AND TBAQ_ENTRY_SL= ?  ");
			dbutil.setString(1, entityCode);
			dbutil.setString(2, programId);
			dbutil.setString(3, sourceKey);
			dbutil.setDate(4, entryDate);
			dbutil.setInt(5, detailSerial);
			dbutil.executeUpdate();
			dbutil.reset();
			dbutil.setMode(DBUtil.PREPARED);
			dbutil.setSql("UPDATE TBAAUTH T   SET T.TBAAUTH_STATUS = ?  WHERE T.ENTITY_CODE=? AND T.TBAAUTH_PGM_ID=? AND T.TBAAUTH_MAIN_PK=? AND T.TBAAUTH_ENTRY_DATE=? AND T.TBAAUTH_ENTRY_SL=?  ");
			dbutil.setString(1, status);
			dbutil.setString(2, entityCode);
			dbutil.setString(3, programId);
			dbutil.setString(4, sourceKey);
			dbutil.setDate(5, entryDate);
			dbutil.setInt(6, detailSerial);
			dbutil.executeUpdate();
			updateTBAauthAction(status);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			dbutil.reset();
		}

	}

}