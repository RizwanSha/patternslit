package patterns.config.framework.bo.auth;

import java.lang.reflect.Method;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import patterns.config.authorization.CommonTBAUpdate;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.ServiceBO;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.interfaces.OutwardMessageProcessor;
import patterns.config.framework.process.TBAProcessResult;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeDateUtils;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.framework.web.FormatUtils;
import patterns.config.framework.web.ajax.ContentManager;

public class acmnauthBO extends ServiceBO {

	public acmnauthBO() {
	}

	public void init() {
	}

	public TBAProcessResult processRequest() {
		DTObject result = null;
		try {
			result = performPGMCTLOperation();
			if (result.get("W_AUTH_REJ_FLG").equals("1") || result.get("W_AUTH_REJ_FLG").equals("2")) {
				processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
				processResult.setAdditionalInfo(result.get("W_AUTH_REJ_FLG"));
				if (processData.get("AUTH_REJ_OPTION").equals("A")) {
					/* ARIBALA ADDED FOR JAVA TO COBOL INTERFACE - BEGIN */
					String action = processData.get("ACTION_TYPE").equals("A") ? "W" : "R";
					if (checkOutwardReqd(action)) {
						OutwardMessageProcessor messageProcessor = new OutwardMessageProcessor();
						DTObject input = new DTObject();
						input.set("ENTITY_CODE", processData.get("ENTITY_CODE"));
						input.set("PGM_ID", processData.get("TBAQ_PGM_ID"));
						input.set("PRIMARY_KEY", processData.get("TBAQ_MAIN_PK"));
						input.setObject("DBCONTEXT", getDbContext());
						input.set("ACTION", action);
						messageProcessor.process(input);
					}
					/* ARIBALA ADDED FOR JAVA TO COBOL INTERFACE - END */
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(e.getLocalizedMessage());
		}

		return processResult;
	}

	private boolean releasePickupAccess() throws Exception {
		DBUtil dbutil = getDbContext().createUtilInstance();
		boolean statusFlag = false;
		try {
			dbutil.reset();
			dbutil.setMode(DBUtil.CALLABLE);
			dbutil.setSql("CALL SP_RELEASE_PICKUP(?,?,?,?,?)");
			dbutil.setString(1, processData.get("ENTITY_CODE"));
			dbutil.setString(2, processData.get("SOURCE_TABLE"));
			dbutil.setString(3, processData.get("TBAQ_PGM_ID"));
			dbutil.setString(4, processData.get("TBAQ_MAIN_PK"));
			dbutil.registerOutParameter(5, Types.VARCHAR);
			dbutil.execute();
			String status = dbutil.getString(5);
			if (status == null || !status.equals(RegularConstants.SP_SUCCESS))
				statusFlag = true;
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} catch (Throwable e) {
			e.printStackTrace();
			throw new Exception(BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			dbutil.reset();
		}
		return statusFlag;
	}

	private DTObject performPGMCTLOperation() throws TBAFrameworkException {
		DTObject resultDTO = null;
		DTObject resultTBADTO = null;
		String _className = ContentManager.EMPTY_STRING;
		DTObject dto = new DTObject();
		DBUtil dbutil = getDbContext().createUtilInstance();
		String authRejOpt = processData.get("AUTH_REJ_OPTION");
		boolean recordExists = false;
		try {
			dbutil.reset();
			dbutil.setMode(DBUtil.PREPARED);
			dbutil.setSql("SELECT PGMCTL_CLASS_NAME FROM PGMCTL WHERE PGMCTL_PGM_ID = ?");
			dbutil.setString(1, processData.get("TBAQ_PGM_ID"));
			ResultSet rs = dbutil.executeQuery();
			if (rs.next()) {
				_className = rs.getString("PGMCTL_CLASS_NAME");
				recordExists = true;
			}
			dbutil.reset();
			if (recordExists) {
				dto = new DTObject();
				dto.set("PARTITION_NO", processData.get("PARTITION_NO"));
				dto.set("SOURCE_KEY", processData.get("TBAQ_MAIN_PK"));
				dto.set("TBA_MAIN_KEY", processData.get("TBAQ_ENTRY_DATE") + "|" + processData.get("TBAQ_DTL_SL"));
				dto.set("USER_ID", processData.get("USER_ID"));
				dto.set("ACTION_TYPE", processData.get("ACTION_TYPE"));
				dto.setObject("CBD", processData.getObject("CBD"));
				dto.set("CLASS_NAME", _className);
				dto.set("METHOD_NAME", RegularConstants.PROCESS_METHOD);
				dto.set(CommonTBAUpdate.PROCESS_STAGE, CommonTBAUpdate.BEFORE);
				dto.set(CommonTBAUpdate.PROCESS_OPTION, authRejOpt);
				resultTBADTO = pgmctlreflection(dto);

			}

			resultDTO = processForAuthorisation(processData);

			if (resultDTO.get(ContentManager.ERROR).equals(ContentManager.EMPTY_STRING)) {
				if (recordExists) {
					if (resultDTO.get("W_AUTH_REJ_FLG").trim().equalsIgnoreCase("1")) {
						dto = new DTObject();
						dto.set("PARTITION_NO", processData.get("PARTITION_NO"));
						dto.set("SOURCE_KEY", processData.get("TBAQ_MAIN_PK"));
						dto.set("TBA_MAIN_KEY", processData.get("TBAQ_ENTRY_DATE") + "|" + processData.get("TBAQ_DTL_SL"));
						dto.set("USER_ID", processData.get("USER_ID"));
						dto.set("ACTION_TYPE", processData.get("ACTION_TYPE"));
						dto.setObject("CBD", processData.getObject("CBD"));
						dto.set("CLASS_NAME", _className);
						dto.set("METHOD_NAME", RegularConstants.PROCESS_METHOD);
						dto.set(CommonTBAUpdate.PROCESS_STAGE, CommonTBAUpdate.AFTER);
						dto.set(CommonTBAUpdate.PROCESS_OPTION, authRejOpt);
						resultTBADTO = pgmctlreflection(dto);

						boolean pgmEfftDateReq = isEffectiveDateProcessingReq(processData.get("TBAQ_PGM_ID"));
						if (pgmEfftDateReq) {
							String[] strSourceKey = BackOfficeFormatUtils.splitSourceKey(processData.get("TBAQ_MAIN_PK"));
							String efftDateString = strSourceKey[strSourceKey.length - 1];
							Date cbd = getSystemDate(processData.get("ENTITY_CODE"));
							java.util.Date effetiveDate = FormatUtils.getDate(efftDateString,BackOfficeConstants.JAVA_DATE_FORMAT);
							boolean isCurrentDate = BackOfficeDateUtils.isDateEqual(effetiveDate, cbd);
							if (isCurrentDate) {
								dto = new DTObject();
								dto.set("PARTITION_NO", processData.get("PARTITION_NO"));
								dto.set("SOURCE_KEY", processData.get("TBAQ_MAIN_PK"));
								dto.set("TBA_MAIN_KEY", processData.get("TBAQ_ENTRY_DATE") + "|" + processData.get("TBAQ_DTL_SL"));
								dto.set("USER_ID", processData.get("USER_ID"));
								dto.set("ACTION_TYPE", processData.get("ACTION_TYPE"));
								dto.setObject("CBD", processData.getObject("CBD"));
								dto.set("CLASS_NAME", _className);
								dto.set("METHOD_NAME", RegularConstants.PROCESS_EFFTDATE_METHOD);
								dto.set(CommonTBAUpdate.PROCESS_OPTION, authRejOpt);
								resultTBADTO = pgmctlreflection(dto);

							} else {
								StringBuffer _sql = new StringBuffer("INSERT INTO EFFDTFWDQ  VALUES (?,?,?,?,?)");
								dbutil.reset();
								dbutil.setSql(_sql.toString());
								dbutil.setString(1, processData.get("ENTITY_CODE"));
								dbutil.setString(2, processData.get("TBAQ_PGM_ID"));
								dbutil.setDate(3, new Date(effetiveDate.getTime()));
								dbutil.setString(4, processData.get("TBAQ_MAIN_PK"));
								dbutil.setString(5, processData.get("ACTION_TYPE"));
								dbutil.executeUpdate();
							}
						}
					}
				}
				
			} else {
				throw new Exception(resultDTO.get(ContentManager.ERROR));
			}
			if (resultTBADTO!=null && resultTBADTO.containsKey(RegularConstants.ADDITIONAL_INFO)) {
				resultDTO.set(RegularConstants.ADDITIONAL_INFO, resultTBADTO.get(RegularConstants.ADDITIONAL_INFO));
			}
		} catch (Exception e) {
			throw new TBAFrameworkException(e.getLocalizedMessage());
		}
		return resultDTO;

	}

	private boolean isEffectiveDateProcessingReq(String programId) {
		boolean flag = false;
		DBUtil dbutil = getDbContext().createUtilInstance();
		try {
			dbutil.reset();
			dbutil.setMode(DBUtil.PREPARED);
			dbutil.setSql("SELECT C.MPGM_EFFDATE_REQ FROM MPGMCONFIG C,MPGM M WHERE M.MPGM_ID =? AND M.MPGM_ID = C.MPGM_ID");
			dbutil.setString(1, programId);
			ResultSet rs = dbutil.executeQuery();
			if (rs.next()) {
				String efftDateProcessReq = rs.getString(1);
				if (efftDateProcessReq.equals("1"))
					flag = true;
				else
					flag = false;
			}
		} catch (SQLException e) {
			flag = false;
		} finally {
			dbutil.reset();
		}
		return flag;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private DTObject pgmctlreflection(DTObject inputObject) throws TBAFrameworkException {
		DTObject resultDTO = null;
		Class qmClass = null;
		Object qmObject = null;
		Method getDataMethod = null;
		Class[] parameterTypes = new Class[] { DTObject.class, DBContext.class };
		Object[] params = new Object[] { inputObject, getDbContext() };
		try {
			qmClass = Class.forName(inputObject.get("CLASS_NAME"));
			qmObject = qmClass.newInstance();
			getDataMethod = qmClass.getMethod(inputObject.get("METHOD_NAME"), parameterTypes);
			resultDTO = (DTObject) getDataMethod.invoke(qmObject, params);
		} catch (Exception e) {
			e.printStackTrace();
			throw new TBAFrameworkException(e.getLocalizedMessage());
		}
		return resultDTO;
	}

	public DTObject processForAuthorisation(DTObject input) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, ContentManager.EMPTY_STRING);
		String entityCode = input.get("ENTITY_CODE");
		String programID = input.get("TBAQ_PGM_ID");
		String sourceKey = input.get("TBAQ_MAIN_PK");
		Date tbaEntryDate = new java.sql.Date(BackOfficeFormatUtils.getDate(input.get("TBAQ_ENTRY_DATE"), BackOfficeConstants.TBA_DATE_FORMAT).getTime());
		DBUtil util = getDbContext().createUtilInstance();
		int tbaDetailSerial = Integer.parseInt(input.get("TBAQ_DTL_SL"));
		String approveRejectOption = input.get("AUTH_REJ_OPTION");
		if (sourceKey.trim().startsWith("|"))
			sourceKey = " " + sourceKey;
		if (sourceKey.trim().endsWith("|"))
			sourceKey = sourceKey + " ";
		try {
			util.reset();
			util.setMode(DBUtil.CALLABLE);
			util.setSql("SELECT TBAQ_TABLE_NAME FROM TBAAUTHQ WHERE ENTITY_CODE = ? AND TBAQ_PGM_ID = ? AND TBAQ_MAIN_PK = ? AND TBAQ_ENTRY_DATE = ? AND TBAQ_ENTRY_SL = ?");
			util.setString(1, entityCode);
			util.setString(2, programID);
			util.setString(3, sourceKey);
			util.setDate(4, tbaEntryDate);
			util.setInt(5, tbaDetailSerial);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				String sourceTable = rset.getString(1);
				util.reset();
				acmnauthprocess authprocess = new acmnauthprocess();
				authprocess.setEntityCode(entityCode);
				authprocess.setProgramId(programID);
				authprocess.setSourceKey(sourceKey);
				authprocess.setEntryDate(tbaEntryDate);
				authprocess.setDetailSerial(tbaDetailSerial);
				authprocess.setApproveRejectOption(approveRejectOption);
				authprocess.setUserId(input.get("USER_ID"));
				authprocess.setCustomerId(input.get("CUST_ID"));
				authprocess.setCbd((java.sql.Date) input.getObject("CBD"));
				authprocess.setIpAddress(input.get("IP_ADDRESS"));
				authprocess.setDbContext(getDbContext());
				authprocess.setPartitionNo(input.get("PARTITION_NO"));

				authprocess.processCommonAuthorization();
				int errorStatus = authprocess.getErrorStatus();
				String errorMessage = authprocess.getErrorMessage();
				int approvalRejectionFlag = authprocess.getApprovalRejectionFlag();

				// added by swaroopa as on 26-04-2012 ends
				if (errorStatus == 1) {
					resultDTO.set(ContentManager.ERROR, errorMessage);
				}
				resultDTO.set("W_AUTH_REJ_FLG", String.valueOf(approvalRejectionFlag));
			}
			util.reset();
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return resultDTO;
	}

	public DTObject processForAuthorisation1(DTObject input) {
		DTObject resultDTO = new DTObject();
		resultDTO.set(ContentManager.ERROR, ContentManager.EMPTY_STRING);
		String entityCode = input.get("ENTITY_CODE");
		String programID = input.get("TBAQ_PGM_ID");
		String sourceKey = input.get("TBAQ_MAIN_PK");
		Date tbaEntryDate = new java.sql.Date(BackOfficeFormatUtils.getDate(input.get("TBAQ_ENTRY_DATE"), BackOfficeConstants.TBA_DATE_FORMAT).getTime());
		DBUtil util = getDbContext().createUtilInstance();
		int tbaDetailSerial = Integer.parseInt(input.get("TBAQ_DTL_SL"));
		String approveRejectOption = input.get("AUTH_REJ_OPTION");
		if (sourceKey.trim().startsWith("|"))
			sourceKey = " " + sourceKey;
		if (sourceKey.trim().endsWith("|"))
			sourceKey = sourceKey + " ";
		try {
			util.reset();
			util.setMode(DBUtil.CALLABLE);
			util.setSql("SELECT TBAQ_TABLE_NAME FROM TBAAUTHQ WHERE ENTITY_CODE = ? AND TBAQ_PGM_ID = ? AND TBAQ_MAIN_PK = ? AND TBAQ_ENTRY_DATE = ? AND TBAQ_ENTRY_SL = ?");
			util.setString(1, entityCode);
			util.setString(2, programID);
			util.setString(3, sourceKey);
			util.setDate(4, tbaEntryDate);
			util.setInt(5, tbaDetailSerial);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				String sourceTable = rset.getString(1);
				util.reset();
				acmnauthprocess authprocess = new acmnauthprocess();

				util.setMode(DBUtil.CALLABLE);
				util.setSql("CALL PKG_COMMON_AUTHORISATION.COMMON_AUTHORISATION(?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
				util.setString(1, entityCode);
				util.setString(2, programID);
				util.setString(3, sourceKey);
				util.setDate(4, tbaEntryDate);
				util.setInt(5, tbaDetailSerial);
				util.setString(6, approveRejectOption);
				util.setString(7, input.get("USER_ID"));
				// added by swaroopa as on 26-04-2012 begins
				util.setString(8, input.get("CUST_ID"));
				util.setDate(9, (java.sql.Date) input.getObject("CBD"));
				util.setString(10, input.get("BRN_CODE"));
				util.setString(11, input.get("IP_ADDRESS"));
				util.registerOutParameter(12, Types.INTEGER);
				util.registerOutParameter(13, Types.VARCHAR);
				util.registerOutParameter(14, Types.INTEGER);
				util.execute();
				int errorStatus = util.getInt(12);
				String errorMessage = util.getString(13);
				int approvalRejectionFlag = util.getInt(14);

				// added by swaroopa as on 26-04-2012 ends
				if (errorStatus == 1) {
					resultDTO.set(ContentManager.ERROR, errorMessage);
				} else {
					if (approveRejectOption.equals("A") && approvalRejectionFlag == 1) {
						// updatePGMGENLOG(entityCode, programID, sourceTable,
						// sourceKey);
					}
				}
				resultDTO.set("W_AUTH_REJ_FLG", String.valueOf(approvalRejectionFlag));

			}
			util.reset();
		} catch (Exception e) {
			e.printStackTrace();
			resultDTO.set(ContentManager.ERROR, BackOfficeErrorCodes.UNSPECIFIED_ERROR);
		} finally {
			util.reset();
		}
		return resultDTO;
	}

	private void updatePGMGENLOG(String entityCode, String programID, String sourceTable, String sourceKey) throws TBAFrameworkException {
		DBUtil dbutil = getDbContext().createUtilInstance();
		try {
			dbutil.reset();
			dbutil.setMode(DBUtil.CALLABLE);
			dbutil.setSql("CALL PKG_EVENTS.SP_GEN_PGMLOG(?,?,?,?,?)");
			dbutil.setString(1, entityCode);
			dbutil.setString(2, programID);
			dbutil.setString(3, sourceTable);
			dbutil.setString(4, sourceKey);
			dbutil.registerOutParameter(5, Types.VARCHAR);
			dbutil.executeUpdate();
		} catch (Exception e) {
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			dbutil.reset();
		}
	}

	/* ARIBALA ADDED FOR JAVA TO COBOL INTERFACE - BEGIN */
	private boolean checkOutwardReqd(String action) {
		DBUtil dbutil = getDbContext().createUtilInstance();
		boolean statusFlag = false;
		String sql = "SELECT PROC_CODE FROM JTCMSGCONFIG WHERE ENTITY_CODE=? AND PGM_ID=? AND OPERATION=?";
		try {
			dbutil.reset();
			dbutil.setMode(DBUtil.CALLABLE);
			dbutil.setSql(sql);
			dbutil.setString(1, processData.get("ENTITY_CODE"));
			dbutil.setString(2, processData.get("TBAQ_PGM_ID"));
			dbutil.setString(3, action);
			dbutil.executeQuery();
			ResultSet rs = dbutil.executeQuery();
			if (rs.next()) {
				statusFlag = true;
			}
		} catch (Exception e) {

		} finally {
			dbutil.reset();
		}
		return statusFlag;
	}
	/* ARIBALA ADDED FOR JAVA TO COBOL INTERFACE - END */
}