package patterns.config.framework.bo.auth;

import java.lang.reflect.Method;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import patterns.config.authorization.CommonTBAUpdate;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.ServiceBO;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.interfaces.OutwardMessageProcessor;
import patterns.config.framework.process.TBAProcessResult;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeDateUtils;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.framework.web.FormatUtils;
import patterns.config.framework.web.ajax.ContentManager;

public class afintranauthBO extends ServiceBO {

	public afintranauthBO() {
	}

	public void init() {
	}

	public TBAProcessResult processRequest() {
		DTObject result = null;
		try {
			result = performPGMCTLOperation();
			if (result.get(ContentManager.ERROR).equals(RegularConstants.EMPTY_STRING)) {
				processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
				processResult.setAdditionalInfo(result);

			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(e.getLocalizedMessage());
		}

		return processResult;
	}

	private DTObject performPGMCTLOperation() throws TBAFrameworkException {
		DTObject resultDTO = new DTObject();
		DTObject resultTBADTO = null;
		String _className = ContentManager.EMPTY_STRING;
		DTObject dto = new DTObject();
		DBUtil dbutil = getDbContext().createUtilInstance();
		String authRejOpt = processData.get("AUTH_REJ_OPTION");
		boolean recordExists = false;
		resultDTO.set(ContentManager.ERROR, RegularConstants.EMPTY_STRING);
		try {
			dbutil.reset();
			dbutil.setMode(DBUtil.PREPARED);
			dbutil.setSql("SELECT PGMCTL_CLASS_NAME FROM PGMCTL WHERE PGMCTL_PGM_ID = ?");
			dbutil.setString(1, processData.get("SOURCE_PGM"));
			ResultSet rs = dbutil.executeQuery();
			if (rs.next()) {
				_className = rs.getString("PGMCTL_CLASS_NAME");
				recordExists = true;
			}
			dbutil.reset();
			if (recordExists) {
				dto = new DTObject();
				dto.set("PARTITION_NO", processData.get("PARTITION_NO"));
				dto.set("SOURCE_KEY", processData.get("SOURCE_KEY"));
				dto.set("TBA_MAIN_KEY", processData.get("BATCH_DATE") + "|" + processData.get("BATCH_NO"));
				dto.set("USER_ID", processData.get("USER_ID"));
				dto.set("ACTION_TYPE", processData.get("ACTION_TYPE"));
				dto.setObject("CBD", processData.getObject("CBD"));
				dto.set("CLASS_NAME", _className);
				dto.set("METHOD_NAME", RegularConstants.PROCESS_METHOD);
				dto.set(CommonTBAUpdate.PROCESS_STAGE, CommonTBAUpdate.BEFORE);
				dto.set(CommonTBAUpdate.PROCESS_OPTION, authRejOpt);
				resultTBADTO = pgmctlreflection(dto);
			}
			if (resultTBADTO != null && resultTBADTO.containsKey(RegularConstants.ADDITIONAL_INFO)) {
				resultDTO.set(RegularConstants.ADDITIONAL_INFO, resultTBADTO.get(RegularConstants.ADDITIONAL_INFO));
			}
		} catch (Exception e) {
			resultDTO.set(ContentManager.ERROR, e.getLocalizedMessage());
			throw new TBAFrameworkException(e.getLocalizedMessage());
		}
		return resultDTO;

	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private DTObject pgmctlreflection(DTObject inputObject) throws TBAFrameworkException {
		DTObject resultDTO = null;
		Class qmClass = null;
		Object qmObject = null;
		Method getDataMethod = null;
		Class[] parameterTypes = new Class[] { DTObject.class, DBContext.class };
		Object[] params = new Object[] { inputObject, getDbContext() };
		try {
			qmClass = Class.forName(inputObject.get("CLASS_NAME"));
			qmObject = qmClass.newInstance();
			getDataMethod = qmClass.getMethod(inputObject.get("METHOD_NAME"), parameterTypes);
			resultDTO = (DTObject) getDataMethod.invoke(qmObject, params);
		} catch (Exception e) {
			e.printStackTrace();
			throw new TBAFrameworkException(e.getLocalizedMessage());
		}
		return resultDTO;
	}

}