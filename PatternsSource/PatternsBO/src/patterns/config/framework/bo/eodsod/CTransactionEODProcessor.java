package patterns.config.framework.bo.eodsod;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.Types;

import patterns.config.framework.bo.ServiceBO;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.process.TBAProcessResult;
import patterns.config.framework.process.TBAProcessStatus;

public class CTransactionEODProcessor extends ServiceBO {
	private String entityCode;
	private String branchCode;
	private String partitionNumber;
	private Date currentDate;
	private String errorCode;

	@Override
	public TBAProcessResult processRequest() {
		try {
			entityCode = processData.get("ENTITY_CODE");
			partitionNumber = processData.get("PARTITION_NO");
			currentDate = (Date) processData.getObject("CBD");
			if (isProcessCompleted())
				processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
			else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(errorCode);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(e.getLocalizedMessage());
		}
		return processResult;
	}

	private boolean isProcessCompleted() throws Exception {
		DBUtil dbUtil = getDbContext().createUtilInstance();
		try {
			String sqlQuery = "SELECT B.BRANCH_CODE FROM ENTITYBRN B WHERE B.ENTITY_CODE=? AND B.ENABLED='1' ";
			dbUtil.reset();
			dbUtil.setMode(DBUtil.PREPARED);
			dbUtil.setSql(sqlQuery);
			dbUtil.setLong(1, Long.parseLong(entityCode));
			ResultSet rs = dbUtil.executeQuery();
			while (rs.next()) {
				branchCode = rs.getString("BRANCH_CODE");
				if (!updateGLProcess())
					return false;
				if (!updateAccountProcess())
					return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e.getCause());
		} finally {
			dbUtil.reset();
		}
	}

	private boolean updateAccountProcess() throws Exception {
		DBUtil dbUtil = getDbContext().createUtilInstance();
		String error = null;
		try {
			String sqlQuery = "CALL SP_CTRAN_EOD_ACNT_PROC(?,?,?,?,?);";
			dbUtil.reset();
			dbUtil.setMode(DBUtil.CALLABLE);
			dbUtil.setSql(sqlQuery);
			dbUtil.setLong(1, Long.parseLong(entityCode));
			dbUtil.setString(2, branchCode);
			dbUtil.setInt(3, Integer.parseInt(partitionNumber));
			dbUtil.setDate(4, currentDate);
			dbUtil.registerOutParameter(5, Types.VARCHAR);
			dbUtil.executeUpdate();
			error = dbUtil.getString(5);
			if (error != null && !error.equals(RegularConstants.EMPTY_STRING)) {
				errorCode = error;
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e.getCause());
		} finally {
			dbUtil.reset();
		}
	}

	private boolean updateGLProcess() throws Exception {
		DBUtil dbUtil = getDbContext().createUtilInstance();
		String error = null;
		try {
			String sqlQuery = "CALL SP_CTRAN_EOD_GL_PROC(?,?,?,?,?);";
			dbUtil.reset();
			dbUtil.setMode(DBUtil.CALLABLE);
			dbUtil.setSql(sqlQuery);
			dbUtil.setLong(1, Long.parseLong(entityCode));
			dbUtil.setString(2, branchCode);
			dbUtil.setInt(3, Integer.parseInt(partitionNumber));
			dbUtil.setDate(4, currentDate);
			dbUtil.registerOutParameter(5, Types.VARCHAR);
			dbUtil.executeUpdate();
			error = dbUtil.getString(5);
			if (error != null && !error.equals(RegularConstants.EMPTY_STRING)) {
				errorCode = error;
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e.getCause());
		} finally {
			dbUtil.reset();
		}
	}
}