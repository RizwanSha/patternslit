/*
 *This Program used to create set of processed to run at EOD or SOD.

 Author : KARTHIKEYAN.P
 Created Date : 01-Apr-2015
 Spec Reference :
 Modification History
 ----------------------------------------------------------------------
 Sl.No		Modified Date	Author			Modified Changes	Version
 ----------------------------------------------------------------------

 */
package patterns.config.framework.bo.eodsod;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;

public class meodsodprocessBO extends GenericBO {

	TBADynaSQL meodsodproc = null;

	public meodsodprocessBO() {
	}

	public void init() {
		meodsodproc = new TBADynaSQL("EODSODPROCESSES", true);
		primaryKey =  processData.get("PROCESS_ID");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("PROCESS_NAME"));
		meodsodproc.setField("PROCESS_ID", processData.get("PROCESS_ID"));
		dataExists = meodsodproc.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				meodsodproc.setNew(true);
				meodsodproc.setField("PROCESS_NAME", processData.get("PROCESS_NAME"));
				meodsodproc.setField("PROCESS_ROUTINE", processData.get("PROCESS_ROUTINE"));
				meodsodproc.setField("PROCESS_CLASS", processData.get("PROCESS_CLASS"));
				meodsodproc.setField("PROCESS_ALLOW_EOD", processData.get("PROCESS_ALLOW_EOD"));
				meodsodproc.setField("PROCESS_ALLOW_SOD", processData.get("PROCESS_ALLOW_SOD"));
				meodsodproc.setField("PROCESS_RUN_FREQ", processData.get("PROCESS_RUN_FREQ"));
				meodsodproc.setField("ENABLED", processData.get("ENABLED"));
				meodsodproc.setField("REMARKS", processData.get("REMARKS"));
				if (meodsodproc.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			meodsodproc.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				meodsodproc.setNew(false);
				meodsodproc.setField("PROCESS_NAME", processData.get("PROCESS_NAME"));
				meodsodproc.setField("PROCESS_ROUTINE", processData.get("PROCESS_ROUTINE"));
				meodsodproc.setField("PROCESS_CLASS", processData.get("PROCESS_CLASS"));
				meodsodproc.setField("PROCESS_ALLOW_EOD", processData.get("PROCESS_ALLOW_EOD"));
				meodsodproc.setField("PROCESS_ALLOW_SOD", processData.get("PROCESS_ALLOW_SOD"));
				meodsodproc.setField("PROCESS_RUN_FREQ", processData.get("PROCESS_RUN_FREQ"));
				meodsodproc.setField("ENABLED", processData.get("ENABLED"));
				meodsodproc.setField("REMARKS", processData.get("REMARKS"));
				if (meodsodproc.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			meodsodproc.close();
		}
	}
}