package patterns.config.framework.bo.eodsod;

import java.sql.Date;
import java.sql.Types;
import java.util.Calendar;

import patterns.config.framework.bo.ServiceBO;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.process.TBAProcessResult;
import patterns.config.framework.process.TBAProcessStatus;

public class TruncateTableProcessor extends ServiceBO {
	private String entityCode;
	private Date currentDate;
	private String errorCode;

	@Override
	public TBAProcessResult processRequest() {
		try {
			entityCode = processData.get("ENTITY_CODE");
			currentDate = (Date) processData.getObject("CBD");
			if (isProcessCompleted())
				processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
			else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(errorCode);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(e.getLocalizedMessage());
		}
		return processResult;
	}

	private boolean isProcessCompleted() throws Exception {
		DBUtil dbUtil = getDbContext().createUtilInstance();
		String error = null;
		try {
			String sqlQuery = "CALL SP_EOD_TRUC_TEMPTBL_RECORDS(?,?,?,?);";
			dbUtil.reset();
			dbUtil.setMode(DBUtil.CALLABLE);
			dbUtil.setSql(sqlQuery);
			dbUtil.setLong(1, Long.parseLong(entityCode));
			dbUtil.setDate(2, currentDate);
			dbUtil.registerOutParameter(3, Types.CHAR);
			dbUtil.registerOutParameter(4, Types.VARCHAR);
			dbUtil.executeUpdate();
			String status = dbUtil.getString(3);
			error = dbUtil.getString(4);
			if (!status.equals(RegularConstants.SUCCESS)) {
				errorCode = error;
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e.getCause());
		} finally {
			dbUtil.reset();
		}
	}

}