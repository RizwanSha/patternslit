package patterns.config.framework.bo.eodsod;

import java.sql.Types;

import patterns.config.framework.bo.ServiceBO;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.process.TBAProcessResult;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTObject;

public class EodSodRoutineProcessor extends ServiceBO {

	public EodSodRoutineProcessor() {
	}

	@Override
	public TBAProcessResult processRequest() {
		try {
			String PROCESS_SUCCESS = "S";
			String PROCESS_FAILURE = "F";
			DTObject result = new DTObject();
			DBUtil procUtil = getDbContext().createUtilInstance();
			StringBuilder sqlQuery = new StringBuilder("CALL ");
			sqlQuery.append(processData.get("ROUTINE_NAME")).append("(?,?,?,?,?)");
			procUtil.setMode(DBUtil.CALLABLE);
			procUtil.setSql(sqlQuery.toString());
			procUtil.setString(1, processData.get("PARTITION_NO"));
			procUtil.setString(2, processData.get("ENTITY_CODE"));
			procUtil.setDate(3, (java.sql.Date) processData.getObject("CBD"));
			procUtil.registerOutParameter(4, Types.VARCHAR);
			procUtil.registerOutParameter(5, Types.VARCHAR);
			procUtil.execute();
			String errorStatus = procUtil.getString(4);
			String errorCode = procUtil.getString(5);
			if (errorStatus != null && errorStatus.equals("S")) {
				result.set("STATUS", PROCESS_SUCCESS);
			} else {
				result.set("STATUS", PROCESS_FAILURE);
				result.set("ERROR_CODE", errorCode);
			}
			processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
			processResult.setAdditionalInfo(result);
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(e.getLocalizedMessage());
		}
		return processResult;
	}
}