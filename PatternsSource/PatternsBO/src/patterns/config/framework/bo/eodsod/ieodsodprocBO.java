package patterns.config.framework.bo.eodsod;
/*
Author : Pavan kumar.R
Created Date : 28-MAR-2015
Spec Reference: HMS-PSD-EODSOD-003
Modification History
----------------------------------------------------------------------
Sl.No		Modified Date	Author			Modified Changes	Version
----------------------------------------------------------------------

*/


import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.BindParameterType;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTDObject;
import patterns.config.framework.web.BackOfficeFormatUtils;

public class ieodsodprocBO extends GenericBO {
	TBADynaSQL ieodsodproc = null;  
	TBADynaSQL ieodsodprocdtl = null; 
	public ieodsodprocBO() {
	}

	public void init() {
		ieodsodproc = new TBADynaSQL("EODSODPROCHIST", true);
		ieodsodprocdtl = new TBADynaSQL("EODSODPROCHISTDTL", false);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("PROCESS_FLAG")  + RegularConstants.PK_SEPARATOR + BackOfficeFormatUtils.getDate(processData.getDate("EFFT_DATE"), BackOfficeConstants.TBA_DATE_FORMAT);
		tbaContext.setTbaPrimarykey(primaryKey);
		ieodsodproc.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		ieodsodproc.setField("PROCESS_FLAG", processData.get("PROCESS_FLAG"));
		ieodsodproc.setField("EFFT_DATE", processData.getDate("EFFT_DATE"));
		dataExists = ieodsodproc.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {

		try {
			if (!isDataExists()) {
				ieodsodproc.setNew(true);
				if (ieodsodproc.save()) {
						DTDObject detailData = processData.getDTDObject("EODSODPROCHISTDTL");
						ieodsodprocdtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
						ieodsodprocdtl.setField("PROCESS_FLAG", processData.get("PROCESS_FLAG"));
						ieodsodprocdtl.setField("EFFT_DATE", processData.getDate("EFFT_DATE"));
						ieodsodprocdtl.setNew(true);
						for (int i = 0; i < detailData.getRowCount(); i++) {
							ieodsodprocdtl.setField("SL", String.valueOf(i + 1));
							ieodsodprocdtl.setField("PROCESS_ID", detailData.getValue(i, 1));
							ieodsodprocdtl.setField("PROCESS_STATE", detailData.getValue(i, 4));
							if (!ieodsodprocdtl.save()) {
								processResult.setProcessStatus(TBAProcessStatus.FAILURE);
								processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
								return;
							}
						}
						processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
						processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_UPDATED);
					} else {
						processResult.setProcessStatus(TBAProcessStatus.FAILURE);
						processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
					}
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_EXISTS);
				}
			} catch (Exception e) {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
				throw new TBAFrameworkException(e.getLocalizedMessage());
			} finally {
				ieodsodproc.close();
				ieodsodprocdtl.close();
			}
		}
	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				ieodsodproc.setNew(false);
				if (ieodsodproc.save()) {
					String[] ieodsodprocdtlFields = { "ENTITY_CODE", "PROCESS_FLAG","EFFT_DATE" ,"SL" };
					BindParameterType[] ieodsodprocdtlFieldTypes = { BindParameterType.BIGINT, BindParameterType.VARCHAR, BindParameterType.DATE, BindParameterType.INTEGER };
					ieodsodprocdtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
					ieodsodprocdtl.setField("PROCESS_FLAG", processData.get("PROCESS_FLAG"));
					ieodsodprocdtl.setField("EFFT_DATE", processData.getDate("EFFT_DATE"));
					ieodsodprocdtl.deleteByFieldsAudit(ieodsodprocdtlFields, ieodsodprocdtlFieldTypes);
					DTDObject detailData = processData.getDTDObject("EODSODPROCHISTDTL");
					ieodsodprocdtl.setNew(true);
					for (int i = 0; i < detailData.getRowCount(); i++) {
						ieodsodprocdtl.setField("SL", String.valueOf(i + 1));
						ieodsodprocdtl.setField("PROCESS_ID", detailData.getValue(i, 1));
						ieodsodprocdtl.setField("PROCESS_STATE", detailData.getValue(i, 4));
						if (!ieodsodprocdtl.save()) {
							processResult.setProcessStatus(TBAProcessStatus.FAILURE);
							processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
							return;
						}
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			ieodsodproc.close();
			ieodsodprocdtl.close();
		}
	}

}