package patterns.config.framework.bo.eodsod;

import java.sql.Date;

import patterns.config.eodsod.EodSodProcess;
import patterns.config.framework.bo.ServiceBO;
import patterns.config.framework.process.TBAProcessResult;
import patterns.config.framework.process.TBAProcessStatus;

public class eeodBO extends ServiceBO {

	public eeodBO() {
	}

	@Override
	public TBAProcessResult processRequest() {
		int runSerial = 0;
		Date currentDate = (Date) processData.getObject("CBD");
		String remarks = processData.get("REMARKS");
		try {
			EodSodProcess process = new EodSodProcess(getDbContext(), currentDate);
			process.updateMainContEod(true, true, remarks);
			runSerial = process.getEodRunSerial();
			process.updateEodPendingStatus(runSerial);
			if (!process.isProcessingRequired(EodSodProcess.EOD_PROCESS)) {
				process.updateEodStatus(runSerial, "S");
				process.updateMainContEod(false, true, null);
				processResult.setAdditionalInfo("S");
			} else {
				processResult.setAdditionalInfo("P");
			}
			processResult.setGeneratedID(String.valueOf(runSerial));
			processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(e.getLocalizedMessage());
		}
		return processResult;
	}
}