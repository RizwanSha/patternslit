package patterns.config.framework.bo.eodsod;

import java.sql.Date;

import patterns.config.eodsod.EodSodProcess;
import patterns.config.framework.bo.ServiceBO;
import patterns.config.framework.process.TBAProcessResult;
import patterns.config.framework.process.TBAProcessStatus;

public class esodBO extends ServiceBO {

	public esodBO() {
	}

	@Override
	public TBAProcessResult processRequest() {
		int runSerial = 0;
		Date currentDate = (Date) processData.getObject("CBD");
		String remarks = processData.get("REMARKS");
		try {
			EodSodProcess process = new EodSodProcess(getDbContext(), currentDate);
			process.updateMainContSod(true, true, remarks);
			process.updateMainContSod(false, true, null);
			runSerial = process.getSodRunSerial();
			process.updateSodPendingStatus(runSerial);

			if (!process.isProcessingRequired(EodSodProcess.SOD_PROCESS)) {
				process.updateSodStatus(runSerial, "S");
				processResult.setAdditionalInfo("S");
			} else {
				processResult.setAdditionalInfo("P");
			}
			processResult.setGeneratedID(String.valueOf(runSerial));
			processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(e.getLocalizedMessage());
		}
		return processResult;
	}
}