package patterns.config.framework.bo.eodsod;

import java.lang.reflect.Method;
import java.sql.Date;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import patterns.config.authorization.CommonTBAUpdate;
import patterns.config.framework.bo.ServiceBO;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.process.TBAProcessResult;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTObject;

public class EffectiveDateProcessor extends ServiceBO {
	private String entityCode;
	private String partitionNumber;
	private Date currentDate;

	@Override
	public TBAProcessResult processRequest() {
		try {
			entityCode = processData.get("ENTITY_CODE");
			partitionNumber = processData.get("PARTITION_NO");
			currentDate = (Date) processData.getObject("CBD");

			List<EffectiveDateBean> recordList = loadEffectiveRecords();

			DTObject inputDTO = new DTObject();
			Class qmClass = null;
			Object qmObject = null;
			Method getDataMethod = null;
			Class[] parameterTypes = new Class[] { DTObject.class, DBContext.class };

			for (EffectiveDateBean bean : recordList) {
				inputDTO.reset();
				inputDTO.set("PARTITION_NO", partitionNumber);
				inputDTO.set("SOURCE_KEY", bean.getPrimaryKey());
				inputDTO.set("TBA_MAIN_KEY", "");
				// inputDTO.set("USER_ID", processData.get("USER_ID"));
				inputDTO.set("ACTION_TYPE", bean.getActionType());
				inputDTO.setObject("CBD", currentDate);
				inputDTO.set(CommonTBAUpdate.PROCESS_OPTION, "A");

				Object[] params = new Object[] { inputDTO, getDbContext() };
				qmClass = Class.forName(bean.getHandlerName());
				qmObject = qmClass.newInstance();
				getDataMethod = qmClass.getMethod(RegularConstants.PROCESS_EFFTDATE_METHOD, parameterTypes);
				inputDTO = (DTObject) getDataMethod.invoke(qmObject, params);
			}

			removePendingRecords();
			processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(e.getLocalizedMessage());
		}
		return processResult;
	}

	private List<EffectiveDateBean> loadEffectiveRecords() throws Exception {
		List<EffectiveDateBean> recordMap = new ArrayList<EffectiveDateBean>();
		DBUtil dbUtil = getDbContext().createUtilInstance();
		try {
			EffectiveDateBean bean = new EffectiveDateBean();
			String sqlQuery = "SELECT Q.PGM_ID,Q.MAIN_PK,Q.ACTION_TYPE,P.PGMCTL_CLASS_NAME FROM EFFDTFWDQ Q,PGMCTL P WHERE Q.ENTITY_CODE=? AND Q.EFFT_DATE=? AND Q.PGM_ID=P.PGMCTL_PGM_ID";
			dbUtil.reset();
			dbUtil.setMode(DBUtil.PREPARED);
			dbUtil.setSql(sqlQuery);
			dbUtil.setString(1, entityCode);
			dbUtil.setDate(2, currentDate);
			ResultSet rset = dbUtil.executeQuery();
			while (rset.next()) {
				bean = new EffectiveDateBean();
				bean.setProgramId(rset.getString("PGM_ID"));
				bean.setPrimaryKey(rset.getString("MAIN_PK"));
				bean.setHandlerName(rset.getString("PGMCTL_CLASS_NAME"));
				bean.setActionType(rset.getString("ACTION_TYPE"));
				recordMap.add(bean);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e.getCause());
		} finally {
			dbUtil.reset();
		}
		return recordMap;
	}

	private void removePendingRecords() throws Exception {
		DBUtil dbUtil = getDbContext().createUtilInstance();
		try {
			String sqlQuery = "DELETE FROM EFFDTFWDQ WHERE ENTITY_CODE=? AND EFFT_DATE=?";
			dbUtil.reset();
			dbUtil.setSql(sqlQuery);
			dbUtil.setString(1, entityCode);
			dbUtil.setDate(2, currentDate);
			dbUtil.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e.getCause());
		} finally {
			dbUtil.reset();
		}
	}
}