package patterns.config.framework.bo.eodsod;

import java.sql.Date;
import java.sql.Types;
import java.util.Calendar;

import patterns.config.framework.bo.ServiceBO;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.process.TBAProcessResult;
import patterns.config.framework.process.TBAProcessStatus;

public class TableCreationProcessor extends ServiceBO {
	private String entityCode;
	private Date currentDate;
	private String errorCode;

	@Override
	public TBAProcessResult processRequest() {
		try {
			entityCode = processData.get("ENTITY_CODE");
			currentDate = (Date) processData.getObject("CBD");
			Calendar cal = Calendar.getInstance();
			cal.setTime(currentDate);
			if (cal.get(Calendar.DAY_OF_MONTH) == 1) {
				if (isProcessCompleted())
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
				else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(errorCode);
				}
			}else{
				processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(e.getLocalizedMessage());
		}
		return processResult;
	}

	

	private boolean isProcessCompleted() throws Exception {
		DBUtil dbUtil = getDbContext().createUtilInstance();
		String error = null;
		try {
			String sqlQuery = "CALL SP_SOD_TBL_CREATION(?,?,?,?);";
			dbUtil.reset();
			dbUtil.setMode(DBUtil.CALLABLE);
			dbUtil.setSql(sqlQuery);
			dbUtil.setLong(1, Long.parseLong(entityCode));
			dbUtil.setDate(2, currentDate);
			dbUtil.registerOutParameter(3, Types.CHAR);
			dbUtil.registerOutParameter(4, Types.VARCHAR);
			dbUtil.executeUpdate();
			String status=dbUtil.getString(3);
			error = dbUtil.getString(4);
			if (!status.equals(RegularConstants.SUCCESS)) {
				errorCode = error;
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e.getCause());
		} finally {
			dbUtil.reset();
		}
	}

	
}