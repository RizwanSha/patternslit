package patterns.config.framework.bo;

import patterns.config.framework.service.DTObject;


public abstract class InventoryBO extends BaseBO {
	
	public abstract DTObject moveToHistory(DTObject input);
	
	public abstract DTObject update(DTObject input);
	
	public abstract DTObject delete(DTObject input);
	
}