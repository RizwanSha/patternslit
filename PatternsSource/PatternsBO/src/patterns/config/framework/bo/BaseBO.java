package patterns.config.framework.bo;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.loggers.ApplicationLogger;

public class BaseBO {

	private ApplicationLogger logger = null;

	public BaseBO() {
		logger = ApplicationLogger.getInstance(this.getClass().getSimpleName());
	}

	public ApplicationLogger getLogger() {
		return logger;
	}

	private DBContext dbContext = null;

	public final DBContext getDbContext() {
		return dbContext;
	}

	public void initialize(DBContext context) {
		dbContext = context;
	}

	public final Timestamp getSystemTime() {
		Timestamp systemTime = null;
		DBUtil dbutil = getDbContext().createUtilInstance();
		try {
			dbutil.reset();
			dbutil.setMode(DBUtil.PREPARED);
			dbutil.setSql("SELECT NOW() FROM DUAL");
			ResultSet rs = dbutil.executeQuery();
			if (rs.next()) {
				systemTime = rs.getTimestamp(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			dbutil.reset();
		}
		return systemTime;
	}

	public final Date getSystemDate(String entity) {
		Date systemDate = null;
		DBUtil dbutil = getDbContext().createUtilInstance();
		try {
			dbutil.reset();
			dbutil.setMode(DBUtil.PREPARED);
			dbutil.setSql("SELECT FN_GETCD(?) FROM DUAL");
			dbutil.setString(1, entity);
			ResultSet rs = dbutil.executeQuery();
			if (rs.next()) {
				systemDate = rs.getDate(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			dbutil.reset();
		}
		return systemDate;
	}

	public final long getInventoryNumber(int invType) {
		long invNumber = 0;
		DBUtil dbutil = getDbContext().createUtilInstance();
		String type = RegularConstants.EMPTY_STRING;
		try {
			switch (invType) {
			case 1:
				type = "SEQ_ADDRESS";
				break;
			case 2:
				type = "SEQ_PHONE";
				break;
			case 3:
				type = "SEQ_DESC";
				break;
			case 4:
				type = "SEQ_GRP";
				break;
			case 5:
				type = "SEQ_BHOTNINTERNALNO";
				break;

			}
			dbutil.reset();
			dbutil.setMode(DBUtil.PREPARED);
			dbutil.setSql("SELECT FN_NEXTVAL(?)");
			dbutil.setString(1, type);
			ResultSet rs = dbutil.executeQuery();
			if (rs.next()) {
				invNumber = rs.getLong(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			dbutil.reset();
		}
		return invNumber;
	}
}
