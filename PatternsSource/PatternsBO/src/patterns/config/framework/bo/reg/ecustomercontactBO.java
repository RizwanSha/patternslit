package patterns.config.framework.bo.reg;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.BindParameterType;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTDObject;
import patterns.config.framework.web.BackOfficeFormatUtils;

public class ecustomercontactBO extends GenericBO {
	TBADynaSQL ecustomercontact = null;
	TBADynaSQL ecustomercontactdtl = null;

	public ecustomercontactBO() {
	}

	public void init() {
		ecustomercontact = new TBADynaSQL("CUSTOMERCONTACTHIST", true);
		ecustomercontactdtl = new TBADynaSQL("CUSTOMERCONTACTHISTDTL", false);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("CUSTOMER_ID") + RegularConstants.PK_SEPARATOR + processData.get("ADDR_SL") + RegularConstants.PK_SEPARATOR + BackOfficeFormatUtils.getDate(processData.getDate("EFF_DATE"), BackOfficeConstants.TBA_DATE_FORMAT);
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("CUSTOMER_ID"));
		ecustomercontact.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		ecustomercontact.setField("CUSTOMER_ID", processData.get("CUSTOMER_ID"));
		ecustomercontact.setField("ADDR_SL", processData.get("ADDR_SL"));
		ecustomercontact.setField("EFF_DATE", processData.getDate("EFF_DATE"));
		dataExists = ecustomercontact.loadByKeyFields();

	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				ecustomercontact.setNew(true);
				ecustomercontact.setField("ENABLED", processData.get("ENABLED"));
				ecustomercontact.setField("REMARKS", processData.get("REMARKS"));
				if (ecustomercontact.save()) {
					ecustomercontactdtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
					ecustomercontactdtl.setField("CUSTOMER_ID", processData.get("CUSTOMER_ID"));
					ecustomercontactdtl.setField("ADDR_SL", processData.get("ADDR_SL"));
					ecustomercontactdtl.setField("EFF_DATE", processData.getDate("EFF_DATE"));
					DTDObject detailData = processData.getDTDObject("CUSTOMERCONTACTHISTDTL");
					ecustomercontactdtl.setNew(true);
					for (int i = 0; i < detailData.getRowCount(); ++i) {
						
						ecustomercontactdtl.setField("DTL_SL", Integer.valueOf(i + 1).toString());
						if (!detailData.getValue(i, 2).equals(RegularConstants.EMPTY_STRING)) {
							ecustomercontactdtl.setField("CONTACT_TITLE", detailData.getValue(i, 2));
						}
						if (!detailData.getValue(i, 3).equals(RegularConstants.EMPTY_STRING)) {
							ecustomercontactdtl.setField("CONTACT_PERSON", detailData.getValue(i, 3));
						}
						if (!detailData.getValue(i, 4).equals(RegularConstants.EMPTY_STRING)) {
							ecustomercontactdtl.setField("CONTACT_NUM_1", detailData.getValue(i, 4));
						}
						if (!detailData.getValue(i, 5).equals(RegularConstants.EMPTY_STRING)) {
							ecustomercontactdtl.setField("CONTACT_NUM_2", detailData.getValue(i, 5));
						}
						if (!detailData.getValue(i, 6).equals(RegularConstants.EMPTY_STRING)) {
							ecustomercontactdtl.setField("EMAIL_ID", detailData.getValue(i, 6));
						}
						ecustomercontactdtl.setField("ALTERNATE_EMAIL", detailData.getValue(i, 7));
						if (!ecustomercontactdtl.save()) {
							processResult.setProcessStatus(TBAProcessStatus.FAILURE);
							processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
							return;
						}
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			ecustomercontact.close();
			ecustomercontactdtl.close();
		}
	}

	protected void modify() throws TBAFrameworkException {

		try {
			if (isDataExists()) {
				ecustomercontact.setNew(false);
				ecustomercontact.setField("ENABLED", processData.get("ENABLED"));
				ecustomercontact.setField("REMARKS", processData.get("REMARKS"));
				if (ecustomercontact.save()) {
					String[] ecustomercontactFields = { "ENTITY_CODE", "CUSTOMER_ID", "ADDR_SL", "EFF_DATE", "DTL_SL" };
					BindParameterType[] ecustomercontactTypes = { BindParameterType.BIGINT, BindParameterType.BIGINT, BindParameterType.BIGINT, BindParameterType.DATE, BindParameterType.BIGINT };
					ecustomercontactdtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
					ecustomercontactdtl.setField("CUSTOMER_ID", processData.get("CUSTOMER_ID"));
					ecustomercontactdtl.setField("ADDR_SL", processData.get("ADDR_SL"));
					ecustomercontactdtl.setField("EFF_DATE", processData.getDate("EFF_DATE"));
					ecustomercontactdtl.deleteByFieldsAudit(ecustomercontactFields, ecustomercontactTypes);
					DTDObject detailData = processData.getDTDObject("CUSTOMERCONTACTHISTDTL");
					ecustomercontactdtl.setNew(true);
					for (int i = 0; i < detailData.getRowCount(); ++i) {
						ecustomercontactdtl.setField("DTL_SL", Integer.valueOf(i + 1).toString());
						if (!detailData.getValue(i, 2).equals(RegularConstants.EMPTY_STRING)) {
							ecustomercontactdtl.setField("CONTACT_TITLE", detailData.getValue(i, 2));
						}
						if (!detailData.getValue(i, 3).equals(RegularConstants.EMPTY_STRING)) {
							ecustomercontactdtl.setField("CONTACT_PERSON", detailData.getValue(i, 3));
						}
						if (!detailData.getValue(i, 4).equals(RegularConstants.EMPTY_STRING)) {
							ecustomercontactdtl.setField("CONTACT_NUM_1", detailData.getValue(i, 4));
						}
						if (!detailData.getValue(i, 5).equals(RegularConstants.EMPTY_STRING)) {
							ecustomercontactdtl.setField("CONTACT_NUM_2", detailData.getValue(i, 5));
						}
						if (!detailData.getValue(i, 6).equals(RegularConstants.EMPTY_STRING)) {
							ecustomercontactdtl.setField("EMAIL_ID", detailData.getValue(i, 6));
						}
						ecustomercontactdtl.setField("ALTERNATE_EMAIL", detailData.getValue(i, 7));
						if (!ecustomercontactdtl.save()) {
							processResult.setProcessStatus(TBAProcessStatus.FAILURE);
							processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
							return;
						}
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}

		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			ecustomercontact.close();
			ecustomercontactdtl.close();
		}
	}

}
