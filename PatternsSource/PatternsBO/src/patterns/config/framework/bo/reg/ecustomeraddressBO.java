


package patterns.config.framework.bo.reg;

import java.util.ArrayList;
import java.util.List;

import patterns.config.framework.bo.AdditionalDetailInfo;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.bo.MessageParam;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAActionType;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTObject;

public class ecustomeraddressBO extends GenericBO {

	TBADynaSQL ecustomeraddress = null;

	public ecustomeraddressBO() {
	}

	public void init() {
		long addressSerial = 0;
		ecustomeraddress = new TBADynaSQL("CUSTOMERADDRESS", true);
		if (tbaContext.getProcessAction().getActionType().equals(TBAActionType.ADD) && !tbaContext.getProcessAction().isRectification()) {
			DTObject input = new DTObject();
			input.set("SOURCE_KEY", processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("CUSTOMER_ID") + RegularConstants.PK_SEPARATOR + processData.get("ADDR_SL"));
			input.set("PGM_ID", tbaContext.getProcessID());
			addressSerial = ecustomeraddress.getTBAReferenceSerial(input);
			processData.set("ADDR_SL", Long.toString(addressSerial));
		}
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("CUSTOMER_ID") + RegularConstants.PK_SEPARATOR + processData.get("ADDR_SL");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("ADDR_SL"));
		ecustomeraddress.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		ecustomeraddress.setField("CUSTOMER_ID", processData.get("CUSTOMER_ID"));
		ecustomeraddress.setField("ADDR_SL", processData.get("ADDR_SL"));
		dataExists = ecustomeraddress.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (tbaContext.getProcessAction().isRectification()) {
				ecustomeraddress.setNew(false);
				if (!isDataExists()) {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
				}
			} else {
				ecustomeraddress.setNew(true);
				if (isDataExists()) {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
				}
			}
			ecustomeraddress.setField("OFFICE_ADDRESS_ID", processData.get("OFFICE_ADDRESS_ID"));
			ecustomeraddress.setField("ADDRESS", processData.get("ADDRESS"));
			ecustomeraddress.setField("PIN_CODE", processData.get("PIN_CODE"));
			ecustomeraddress.setField("STATE_CODE", processData.get("STATE_CODE"));
			ecustomeraddress.setField("TAN_NO", processData.get("TAN_NO"));
			ecustomeraddress.setField("DETELED_FLAG", processData.get("DETELED_FLAG"));
			ecustomeraddress.setField("REMARKS", processData.get("REMARKS"));
			if (ecustomeraddress.save()) {
				if (!tbaContext.getProcessAction().isRectification()) {
					AdditionalDetailInfo additionalInfo = new AdditionalDetailInfo();
					additionalInfo.setHeading(new MessageParam(BackOfficeErrorCodes.ECUST_GEN_RESULT));
					List<MessageParam> key = new ArrayList<MessageParam>();
					key.add(new MessageParam(BackOfficeErrorCodes.ADDR_SL));
					additionalInfo.setKey(key);
					List<String> value = new ArrayList<String>();
					value.add(processData.get("ADDR_SL"));
					additionalInfo.setValue(value);
					processResult.setAdditionalInfo(additionalInfo);
				}
				processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);

			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			ecustomeraddress.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				ecustomeraddress.setNew(false);
				ecustomeraddress.setField("OFFICE_ADDRESS_ID", processData.get("OFFICE_ADDRESS_ID"));
				ecustomeraddress.setField("ADDRESS", processData.get("ADDRESS"));
				ecustomeraddress.setField("PIN_CODE", processData.get("PIN_CODE"));
				ecustomeraddress.setField("STATE_CODE", processData.get("STATE_CODE"));
				ecustomeraddress.setField("TAN_NO", processData.get("TAN_NO"));
				ecustomeraddress.setField("DETELED_FLAG", processData.get("DETELED_FLAG"));
				ecustomeraddress.setField("REMARKS", processData.get("REMARKS"));
				if (ecustomeraddress.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			ecustomeraddress.close();
		}
	}
}
