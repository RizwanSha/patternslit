/*
 *
 Author : Jayashree P
 Created Date : 08-04-2017
 Spec Reference : ECUSTOMERPRODREG.HLD
 
 Modification History
 ----------------------------------------------------------------------
 Sl.No		Modified Date	Author			Modified Changes	Version
 ----------------------------------------------------------------------

 */

package patterns.config.framework.bo.reg;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.BindParameterType;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAActionType;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTDObject;
import patterns.config.framework.service.DTObject;

public class ecustomerprodregBO extends GenericBO {

	TBADynaSQL ecustomerprodreg = null;
	TBADynaSQL ecustomerprodregaccdtl = null;

	public ecustomerprodregBO() {
	}

	public void init() {
		long entrySl = 0;
		ecustomerprodreg = new TBADynaSQL("CUSTOMERPRODREG", true);
		ecustomerprodregaccdtl = new TBADynaSQL("CUSTOMERPRODREGACDTL", false);
		if (tbaContext.getProcessAction().getActionType().equals(TBAActionType.ADD) && !tbaContext.getProcessAction().isRectification()) {
			DTObject input = new DTObject();
			input.set("SOURCE_KEY", processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR +  processData.get("CUSTOMER_ID"));
			input.set("PGM_ID", tbaContext.getProcessID());
			entrySl = ecustomerprodreg.getTBAReferenceSerial(input);
			processData.set("ENTRY_SL", Long.toString(entrySl));
		}
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("CUSTOMER_ID")+ RegularConstants.PK_SEPARATOR + processData.get("ENTRY_SL");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("CUSTOMER_ID"));
		ecustomerprodreg.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		ecustomerprodreg.setField("CUSTOMER_ID", processData.get("CUSTOMER_ID"));
		ecustomerprodreg.setField("ENTRY_SL", processData.get("ENTRY_SL"));
		dataExists = ecustomerprodreg.loadByKeyFields();
	}

	@Override
	protected void add() throws TBAFrameworkException {
		// TODO Auto-generated method stub
		try {
			if (tbaContext.getProcessAction().isRectification()) {
				ecustomerprodreg.setNew(false);
				if (!isDataExists()) {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
				}
			} else {
				ecustomerprodreg.setNew(true);
				if (isDataExists()) {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
				}
			}
			ecustomerprodreg.setField("PROD_ADDED_ON", processData.getDate("PROD_ADDED_ON"));
			ecustomerprodreg.setField("REMARKS", processData.get("REMARKS"));
			if (ecustomerprodreg.save()) {
				if (processData.containsKey("CUSTOMERPRODREGACDTL")) {
					DTDObject detailData = processData.getDTDObject("CUSTOMERPRODREGACDTL");
					for (int i = 0; i < detailData.getRowCount(); ++i) {
						ecustomerprodreg.setField("PURPOSE_ID", "SUNDRY_DB_ACCOUNT");
						ecustomerprodreg.setField("DEDUP_KEY", processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + detailData.getValue(i, 4));
						if (!ecustomerprodreg.udateTBADuplicateCheck()) {
							processResult.setProcessStatus(TBAProcessStatus.FAILURE);
							processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
							throw new TBAFrameworkException();
						}
						if (!detailData.getValue(i, 8).equals("O")) {
							ecustomerprodreg.setField("PURPOSE_ID", "PRINCIPAL_ACCOUNT");
							ecustomerprodreg.setField("DEDUP_KEY", processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + detailData.getValue(i, 5));
							if (!ecustomerprodreg.udateTBADuplicateCheck()) {
								processResult.setProcessStatus(TBAProcessStatus.FAILURE);
								processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
								throw new TBAFrameworkException();
							}
							ecustomerprodreg.setField("PURPOSE_ID", "INTEREST_ACCOUNT");
							ecustomerprodreg.setField("DEDUP_KEY", processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + detailData.getValue(i, 6));
							if (!ecustomerprodreg.udateTBADuplicateCheck()) {
								processResult.setProcessStatus(TBAProcessStatus.FAILURE);
								processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
								throw new TBAFrameworkException();
							}
							ecustomerprodreg.setField("PURPOSE_ID", "UNEARNED_INTEREST_ACCOUNT");
							ecustomerprodreg.setField("DEDUP_KEY", processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + detailData.getValue(i, 7));
							if (!ecustomerprodreg.udateTBADuplicateCheck()) {
								processResult.setProcessStatus(TBAProcessStatus.FAILURE);
								processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
								throw new TBAFrameworkException();
							}
						}
					}
				}
				ecustomerprodregaccdtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
				ecustomerprodregaccdtl.setField("CUSTOMER_ID", processData.get("CUSTOMER_ID"));
				ecustomerprodregaccdtl.setField("ENTRY_SL", processData.get("ENTRY_SL"));
				if (tbaContext.getProcessAction().isRectification()) {
					String[] ecustomerprodregaccdtlFields = { "ENTITY_CODE", "CUSTOMER_ID", "DTL_SL" };
					BindParameterType[] ecustomerprodregaccdtlTypes = { BindParameterType.BIGINT, BindParameterType.BIGINT, BindParameterType.INTEGER };
					ecustomerprodregaccdtl.deleteByFieldsAudit(ecustomerprodregaccdtlFields, ecustomerprodregaccdtlTypes);
				}
				DTDObject detailData = processData.getDTDObject("CUSTOMERPRODREGACDTL");
				ecustomerprodregaccdtl.setNew(true);
				
				for (int i = 0; i < detailData.getRowCount(); ++i) {
					ecustomerprodregaccdtl.setField("DTL_SL", Integer.valueOf(i + 1).toString());
					ecustomerprodregaccdtl.setField("LEASE_PRODUCT_CODE", detailData.getValue(i, 2));
					ecustomerprodregaccdtl.setField("PORTFOLIO_CODE", detailData.getValue(i, 3));
					ecustomerprodregaccdtl.setField("SUNDRY_DB_AC", detailData.getValue(i, 4));
					if (!detailData.getValue(i, 8).equals("O")) {
						ecustomerprodregaccdtl.setField("PRINCIPAL_AC", detailData.getValue(i, 5));
						ecustomerprodregaccdtl.setField("INTEREST_AC", detailData.getValue(i, 6));
						ecustomerprodregaccdtl.setField("UNEARNED_INTEREST_AC", detailData.getValue(i, 7));
					} else {
						ecustomerprodregaccdtl.setField("PRINCIPAL_AC", RegularConstants.NULL);
						ecustomerprodregaccdtl.setField("INTEREST_AC", RegularConstants.NULL);
						ecustomerprodregaccdtl.setField("UNEARNED_INTEREST_AC", RegularConstants.NULL);
					}
					if (!ecustomerprodregaccdtl.save()) {
						processResult.setProcessStatus(TBAProcessStatus.FAILURE);
						processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
						return;
					}
				}
				processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			ecustomerprodreg.close();
			ecustomerprodregaccdtl.close();
		}
	}

	protected void modify() throws TBAFrameworkException {

	}
}
