package patterns.config.framework.bo.reg;

import java.util.ArrayList;
import java.util.List;

import patterns.config.framework.bo.AdditionalDetailInfo;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.bo.MessageParam;
import patterns.config.framework.database.BindParameterType;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAActionType;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTDObject;
import patterns.config.framework.service.DTObject;

public class ecustomerregBO extends GenericBO {

	TBADynaSQL ecustomerreg = null;
	TBADynaSQL ecustomerregaccdtl = null;

	public ecustomerregBO() {
	}

	public void init() {
		long customerId = 0;
		ecustomerreg = new TBADynaSQL("CUSTOMER", true);
		ecustomerregaccdtl = new TBADynaSQL("CUSTOMERACDTL", false);
		if (tbaContext.getProcessAction().getActionType().equals(TBAActionType.ADD) && !tbaContext.getProcessAction().isRectification()) {
			DTObject input = new DTObject();
			input.set("SOURCE_KEY", processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("CUSTOMER_ID"));
			input.set("PGM_ID", tbaContext.getProcessID());
			customerId = ecustomerreg.getTBAReferenceSerial(input);
			processData.set("CUSTOMER_ID", Long.toString(customerId));
		}
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("CUSTOMER_ID");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("CUSTOMER_ID"));
		ecustomerreg.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		ecustomerreg.setField("CUSTOMER_ID", processData.get("CUSTOMER_ID"));
		dataExists = ecustomerreg.loadByKeyFields();
	}

	@Override
	protected void add() throws TBAFrameworkException {
		// TODO Auto-generated method stub
		try {
			if (tbaContext.getProcessAction().isRectification()) {
				ecustomerreg.setNew(false);
				if (!isDataExists()) {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
				}
			} else {
				ecustomerreg.setNew(true);
				if (isDataExists()) {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
				}
			}
			ecustomerreg.setField("CUSTOMER_NAME", processData.get("CUSTOMER_NAME"));
			ecustomerreg.setField("DATE_OF_REG", processData.getDate("DATE_OF_REG"));
			ecustomerreg.setField("PAN_NO", processData.get("PAN_NO"));
			ecustomerreg.setField("REGION_CODE", processData.get("REGION_CODE"));
			ecustomerreg.setField("REG_ADDRESS", processData.get("REG_ADDRESS"));
			ecustomerreg.setField("PIN_CODE", processData.get("PIN_CODE"));
			ecustomerreg.setField("STATE_CODE", processData.get("STATE_CODE"));
			ecustomerreg.setField("STAFF_CODE", processData.get("STAFF_CODE"));
			if (ecustomerreg.save()) {
				ecustomerreg.setField("PURPOSE_ID", "PAN_NUMBER");
				ecustomerreg.setField("DEDUP_KEY", processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("PAN_NO"));
				if (!ecustomerreg.udateTBADuplicateCheck()) {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
					throw new TBAFrameworkException();
				}
				if (processData.containsKey("CUSTOMERACDTL")) {
					DTDObject detailData = processData.getDTDObject("CUSTOMERACDTL");
					for (int i = 0; i < detailData.getRowCount(); ++i) {
						ecustomerreg.setField("PURPOSE_ID", "SUNDRY_DB_ACCOUNT");
						ecustomerreg.setField("DEDUP_KEY", processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + detailData.getValue(i, 4));
						if (!ecustomerreg.udateTBADuplicateCheck()) {
							processResult.setProcessStatus(TBAProcessStatus.FAILURE);
							processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
							throw new TBAFrameworkException();
						}
						if (!detailData.getValue(i, 8).equals("O")) {
							ecustomerreg.setField("PURPOSE_ID", "PRINCIPAL_ACCOUNT");
							ecustomerreg.setField("DEDUP_KEY", processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + detailData.getValue(i, 5));
							if (!ecustomerreg.udateTBADuplicateCheck()) {
								processResult.setProcessStatus(TBAProcessStatus.FAILURE);
								processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
								throw new TBAFrameworkException();
							}
							ecustomerreg.setField("PURPOSE_ID", "INTEREST_ACCOUNT");
							ecustomerreg.setField("DEDUP_KEY", processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + detailData.getValue(i, 6));
							if (!ecustomerreg.udateTBADuplicateCheck()) {
								processResult.setProcessStatus(TBAProcessStatus.FAILURE);
								processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
								throw new TBAFrameworkException();
							}
							ecustomerreg.setField("PURPOSE_ID", "UNEARNED_INTEREST_ACCOUNT");
							ecustomerreg.setField("DEDUP_KEY", processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + detailData.getValue(i, 7));
							if (!ecustomerreg.udateTBADuplicateCheck()) {
								processResult.setProcessStatus(TBAProcessStatus.FAILURE);
								processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
								throw new TBAFrameworkException();
							}
						}
					}
				}
				ecustomerregaccdtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
				ecustomerregaccdtl.setField("CUSTOMER_ID", processData.get("CUSTOMER_ID"));
				if (tbaContext.getProcessAction().isRectification()) {
					String[] ecustomerregaccdtlFields = { "ENTITY_CODE", "CUSTOMER_ID", "DTL_SL" };
					BindParameterType[] ecustomerregaccdtlTypes = { BindParameterType.BIGINT, BindParameterType.BIGINT, BindParameterType.INTEGER };
					ecustomerregaccdtl.deleteByFieldsAudit(ecustomerregaccdtlFields, ecustomerregaccdtlTypes);
				}
				DTDObject detailData = processData.getDTDObject("CUSTOMERACDTL");
				ecustomerregaccdtl.setNew(true);
				for (int i = 0; i < detailData.getRowCount(); ++i) {
					ecustomerregaccdtl.setField("DTL_SL", Integer.valueOf(i + 1).toString());
					ecustomerregaccdtl.setField("LEASE_PRODUCT_CODE", detailData.getValue(i, 2));
					ecustomerregaccdtl.setField("PORTFOLIO_CODE", detailData.getValue(i, 3));
					ecustomerregaccdtl.setField("SUNDRY_DB_AC", detailData.getValue(i, 4));
					if (!detailData.getValue(i, 8).equals("O")) {
						ecustomerregaccdtl.setField("PRINCIPAL_AC", detailData.getValue(i, 5));
						ecustomerregaccdtl.setField("INTEREST_AC", detailData.getValue(i, 6));
						ecustomerregaccdtl.setField("UNEARNED_INTEREST_AC", detailData.getValue(i, 7));
					} else {
						ecustomerregaccdtl.setField("PRINCIPAL_AC", RegularConstants.NULL);
						ecustomerregaccdtl.setField("INTEREST_AC", RegularConstants.NULL);
						ecustomerregaccdtl.setField("UNEARNED_INTEREST_AC", RegularConstants.NULL);
					}
					if (!ecustomerregaccdtl.save()) {
						processResult.setProcessStatus(TBAProcessStatus.FAILURE);
						processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
						return;
					}
				}
				if (!tbaContext.getProcessAction().isRectification()) {
					AdditionalDetailInfo additionalInfo = new AdditionalDetailInfo();
					additionalInfo.setHeading(new MessageParam(BackOfficeErrorCodes.ECUST_GEN_RESULT));
					List<MessageParam> key = new ArrayList<MessageParam>();
					key.add(new MessageParam(BackOfficeErrorCodes.ECUST_GEN_SL));
					additionalInfo.setKey(key);
					List<String> value = new ArrayList<String>();
					value.add(processData.get("CUSTOMER_ID"));
					additionalInfo.setValue(value);
					processResult.setAdditionalInfo(additionalInfo);
				}
				processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			ecustomerreg.close();
			ecustomerregaccdtl.close();
		}
	}

	protected void modify() throws TBAFrameworkException {

	}
}
