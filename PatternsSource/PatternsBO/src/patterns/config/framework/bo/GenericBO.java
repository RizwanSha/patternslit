package patterns.config.framework.bo;

import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAActionType;
import patterns.config.framework.process.TBAProcessAction;
import patterns.config.framework.process.TBAProcessResult;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.thread.TBAContext;

public abstract class GenericBO extends BaseBO {
	protected TBAContext tbaContext = null;
	protected DTObject processData = null;
	protected TBAProcessAction processAction = null;
	protected TBAProcessResult processResult = null;
	protected boolean dataExists = false;
	protected String primaryKey = null;

	public TBAProcessResult processRequest() {
		try {
			TBAActionType actionType = tbaContext.getProcessAction().getActionType();
			if (tbaContext.isFinancialOperation()) {
				processResult.setFinancialOperation(true);
			}
			// ARIBALA ADDED ON 09102015 BEGIN
			// if(tbaContext.getProgramType().equals("M") &&
			// tbaContext.getProcessAction().isRectification()){
			// actionType = TBAActionType.MODIFY;
			// }
			// ARIBALA ADDED ON 09102015 END
			switch (actionType) {
			case ADD:
				add();
				break;
			case MODIFY:
				modify();
				break;
			default:
				break;
			}
		} catch (Exception e) {
			String error = e.getLocalizedMessage();
			processResult.setAdditionalInfo(error);
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
		}
		return processResult;
	}

	public String getFinYear() {
		return tbaContext.getProcessAction().getFinYear();
	}

	public String getCurrentYear() {
		return tbaContext.getProcessAction().getCurrentYear();
	}

	public void initialize() {
		tbaContext = TBAContext.getInstance();
		super.initialize(tbaContext.getDBContext());
		processResult = new TBAProcessResult();
		processAction = tbaContext.getProcessAction();
		processData = tbaContext.getProcessData();
		getLogger().logDebug("Process Data : " + processData);
	}

	public abstract void init() throws TBAFrameworkException;

	protected abstract void add() throws TBAFrameworkException;

	protected abstract void modify() throws TBAFrameworkException;

	public String getFinancialOperationBO() {
		return null;
	}

	public boolean isDataExists() {
		return dataExists;
	}

	public void setDataExists(boolean dataExists) {
		this.dataExists = dataExists;
	}

}
