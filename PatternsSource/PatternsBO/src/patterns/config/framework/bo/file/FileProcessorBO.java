package patterns.config.framework.bo.file;

import java.util.Date;
import java.util.List;

import patterns.config.framework.bo.ServiceBO;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.FileConstants;
import patterns.config.framework.database.FileConstants.StepStage;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.process.TBAProcessResult;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.middleware.utils.SplitManagerBean;

/**
 * @author PATTERNS0020
 * 
 */
public class FileProcessorBO extends ServiceBO {

	private String entityCode;
	private String sourceKey;

	public enum ProcessStage {
		BEGIN, PROCESS, END

	}

	public FileProcessorBO() {
	}

	@Override
	public TBAProcessResult processRequest() {
		int requestType = processData.getInteger(FileConstants.REQUEST_TYPE_KEY);
		entityCode = processData.get(FileConstants.ENTITY_CODE);
		sourceKey = processData.get(FileConstants.SOURCE_KEY);
		try {
			switch (requestType) {
			case FileConstants.STEP_PROC_BEGIN:
				processBegin();
				break;
			case FileConstants.STEP_FILE_VALIDATION:
				validateFile();
				break;
			case FileConstants.STEP_OPEN_BATCH:
				openBatch();
				break;
			case FileConstants.STEP_PREPARE_SPLIT_MANAGER:
				prepareSplitManager();
				break;
			case FileConstants.STEP_PROCESS_DEBIT_SPLITS:
				processDebitSplits();
				break;
			case FileConstants.STEP_PROCESS_CREDIT_SPLITS:
				processCreditSplits();
				break;
			case FileConstants.STEP_VALIDATE_RECORDS:
				validateRecords();
				break;
			case FileConstants.STEP_UPDATE_DEBIT_LEGS:
				updateDebitLegs();
				break;
			case FileConstants.STEP_UPDATE_CREDIT_LEGS:
				updateCreditLegs();
				break;
			case FileConstants.STEP_POST_DEBITS:
				postDebits();
				break;
			case FileConstants.STEP_POST_CREDITS:
				postCredits();
				break;
			case FileConstants.STEP_CLOSE_BATCH:
				closeBatch();
				break;
			case FileConstants.STEP_RESPONSE_FILE_GEN:
				responseFileGeneration();
				break;
			case FileConstants.STEP_END:
				processEnd();
				break;
			case FileConstants.STEP_ERROR:
				processError();
				break;
			case FileConstants.STEP_SPLIT_UPDATES:
				updateSplitStatus();
				break;
			}
			processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(e.getLocalizedMessage());
		}
		return processResult;
	}

	private void processError() throws Exception {
		DBUtil dbUtil = getDbContext().createUtilInstance();
		try {
			String fileProcStatusUpdateQuery = "UPDATE FILEPROCSTATUS SET END_TIME=NOW(),PROCESS_STATUS='F',ERROR_CODE=?,ERROR_DESC=DECODE_CMLOVREC('COMMON','FILEPROCERROR',?) WHERE ENTITY_CODE = ? AND SOURCE_KEY = ?";
			dbUtil.setMode(DBUtil.PREPARED);
			dbUtil.setSql(fileProcStatusUpdateQuery);
			dbUtil.setString(1, processData.get("ERROR_CODE"));
			dbUtil.setString(2, processData.get("ERROR_CODE"));
			dbUtil.setString(3, entityCode);
			dbUtil.setString(4, sourceKey);
			dbUtil.executeUpdate();
			removeFileProcessRetryQueue();
			updateFileUploadRecord(false);
		} catch (Exception e) {
			throw new Exception(e);
		} finally {
			dbUtil.reset();
		}
	}

	private void processBegin() throws Exception {
		updateFileProcessStatus("", ProcessStage.BEGIN);
	}

	private void processEnd() throws Exception {
		DBUtil dbUtil = getDbContext().createUtilInstance();
		try {
			String fileProcStatusUpdateQuery = "UPDATE FILEPROCSTATUS SET END_TIME=NOW(),PROCESS_STATUS='S'WHERE ENTITY_CODE = ? AND SOURCE_KEY = ?";
			dbUtil.setMode(DBUtil.PREPARED);
			dbUtil.setSql(fileProcStatusUpdateQuery);
			dbUtil.setString(1, entityCode);
			dbUtil.setString(2, sourceKey);
			dbUtil.executeUpdate();
			removeFileProcessRetryQueue();
			updateFileUploadRecord(true);
		} catch (Exception e) {
			throw new Exception(e);
		} finally {
			dbUtil.reset();
		}
	}

	private void removeFileProcessRetryQueue() throws Exception {
		DBUtil dbUtil = getDbContext().createUtilInstance();
		try {
			String updateQuery = "DELETE FROM FILEPROCQ WHERE ENTITY_CODE = ? AND SOURCE_KEY = ?";
			dbUtil.setMode(DBUtil.PREPARED);
			dbUtil.setSql(updateQuery);
			dbUtil.setString(1, entityCode);
			dbUtil.setString(2, sourceKey);
			dbUtil.executeUpdate();
		} catch (Exception e) {
			throw new Exception(e);
		} finally {
			dbUtil.reset();
		}
	}

	private void updateFileUploadRecord(boolean success) throws Exception {
		DBUtil dbUtil = getDbContext().createUtilInstance();
		try {
			String[] strsrcKey = BackOfficeFormatUtils.splitSourceKey(sourceKey);
			long entityCode = Long.valueOf(strsrcKey[0]);
			String officeCode = strsrcKey[1];
			String purposeCode = strsrcKey[2];
			Date uploadDate = BackOfficeFormatUtils.getDate(strsrcKey[3], BackOfficeConstants.JAVA_DATE_FORMAT);
			int uploadSerial = Integer.valueOf(strsrcKey[4]);

			String fileUploadUpdateQuery = "UPDATE FILEUPLOADPGM SET PROCESS_STATUS=?,PROCESS_ON=NOW() WHERE ENTITY_CODE = ? AND OFFICE_CODE = ? AND PURPOSE_CODE = ? AND UPLOAD_DATE = ? AND UPLOAD_SL = ?";
			dbUtil.setMode(DBUtil.PREPARED);
			dbUtil.setSql(fileUploadUpdateQuery);
			dbUtil.setString(1, success ? FileConstants.PROCESS_SUCCESS : FileConstants.PROCESS_FAILURE);
			dbUtil.setLong(2, entityCode);
			dbUtil.setString(3, officeCode);
			dbUtil.setString(4, purposeCode);
			dbUtil.setDate(5, new java.sql.Date(uploadDate.getTime()));
			dbUtil.setInt(6, uploadSerial);
			dbUtil.executeUpdate();

		} catch (Exception e) {
			throw new Exception(e);
		} finally {
			dbUtil.reset();
		}
	}

	private void validateFile() throws Exception {
		StepStage stage = (StepStage) processData.getObject(FileConstants.STEP_STAGE_KEY);
		switch (stage) {
		case BEGIN:
			updateFileProcessStatusDetails(FileConstants.STEP_FILE_VALIDATION, FileConstants.STATUS_VALIDATE_FILE, true);
			updateFileProcessStatus(FileConstants.STATUS_VALIDATE_FILE, ProcessStage.PROCESS);
			break;
		case END:
			updateFileProcessStatusDetails(FileConstants.STEP_FILE_VALIDATION, FileConstants.STATUS_VALIDATE_FILE, false);
			break;
		}
	}

	private void openBatch() throws Exception {
		StepStage stage = (StepStage) processData.getObject(FileConstants.STEP_STAGE_KEY);
		switch (stage) {
		case BEGIN:
			updateFileProcessStatusDetails(FileConstants.STEP_OPEN_BATCH, FileConstants.STATUS_OPEN_BATCH, true);
			updateFileProcessStatus(FileConstants.STATUS_OPEN_BATCH, ProcessStage.PROCESS);
			break;
		case END:
			openBatchProcess();
			updateFileProcessStatusDetails(FileConstants.STEP_OPEN_BATCH, FileConstants.STATUS_OPEN_BATCH, false);
			break;
		}
	}

	private void openBatchProcess() throws Exception {
		DBUtil dbUtil = getDbContext().createUtilInstance();
		try {
			String updateQuery = "UPDATE FILEPROCSTATUS SET TRAN_PK=? WHERE ENTITY_CODE = ? AND SOURCE_KEY = ?";
			dbUtil.setMode(DBUtil.PREPARED);
			dbUtil.setSql(updateQuery);
			dbUtil.setString(1, processData.get("TRAN_KEY"));
			dbUtil.setString(2, entityCode);
			dbUtil.setString(3, sourceKey);
			dbUtil.executeUpdate();
		} catch (Exception e) {
			throw new Exception(e);
		} finally {
			dbUtil.reset();
		}
	}

	private void prepareSplitManager() throws Exception {
		StepStage stage = (StepStage) processData.getObject(FileConstants.STEP_STAGE_KEY);
		switch (stage) {
		case BEGIN:
			updateFileProcessStatusDetails(FileConstants.STEP_PREPARE_SPLIT_MANAGER, FileConstants.STATUS_PREPARE_SPLIT, true);
			updateFileProcessStatus(FileConstants.STATUS_PREPARE_SPLIT, ProcessStage.PROCESS);
			break;
		case END:
			prepareSplitsProcess();
			updateFileProcessStatusDetails(FileConstants.STEP_PREPARE_SPLIT_MANAGER, FileConstants.STATUS_PREPARE_SPLIT, false);
			break;
		}
	}

	private void prepareSplitsProcess() throws Exception {
		prepareSplitsProcess_updateFileSplitDetails();
		prepareSplitsProcess_updateSplitStrategyDetails();
	}

	private void prepareSplitsProcess_updateFileSplitDetails() throws Exception {
		DBUtil dbUtil = getDbContext().createUtilInstance();
		try {
			@SuppressWarnings("unchecked")
			List<SplitManagerBean> splitList = (List<SplitManagerBean>) processData.getObject(FileConstants.SPLIT_INFO_KEY);
			String insertQuery = "INSERT INTO FILESPLITDTL (ENTITY_CODE,SOURCE_KEY,SPLIT_SL,NO_OF_RECORDS,FILE_BASED_TRAN,START_OFFSET,SPLIT_LENGTH,TRANEG_START_SL) VALUES (?,?,?,?,?,?,?,?)";
			dbUtil.setMode(DBUtil.PREPARED);
			dbUtil.setSql(insertQuery);
			dbUtil.setString(1, entityCode);
			dbUtil.setString(2, sourceKey);
			int counter = 1;
			for (SplitManagerBean bean : splitList) {
				dbUtil.setInt(3, counter);
				dbUtil.setLong(4, bean.getNumberOfRecords());
				dbUtil.setString(5, bean.isFileBasedTransaction() ? RegularConstants.COLUMN_ENABLE : RegularConstants.COLUMN_DISABLE);
				dbUtil.setLong(6, bean.getStartOffSet());
				dbUtil.setLong(7, bean.getLength());
				dbUtil.setLong(8, bean.getTranSl());
				dbUtil.executeUpdate();
				++counter;
			}
		} catch (Exception e) {
			throw new Exception(e);
		} finally {
			dbUtil.reset();
		}
	}

	private void prepareSplitsProcess_updateSplitStrategyDetails() throws Exception {
		DBUtil dbUtil = getDbContext().createUtilInstance();
		try {
			String splitCount = processData.get(FileConstants.SPLIT_COUNT_KEY);
			String threshHold = processData.get(FileConstants.SPLIT_THRESHOLD_KEY);
			String updateQuery = "UPDATE FILEPROCSTATUS SET SPLIT_COUNT=?,SPLIT_THRESH_HOLD=? WHERE ENTITY_CODE = ? AND SOURCE_KEY = ?";
			dbUtil.setMode(DBUtil.PREPARED);
			dbUtil.setSql(updateQuery);
			dbUtil.setString(1, splitCount);
			dbUtil.setString(2, threshHold);
			dbUtil.setString(3, entityCode);
			dbUtil.setString(4, sourceKey);
			int count = dbUtil.executeUpdate();
			if (count == 0) {
				throw new Exception("FILEPROCSTATUS Update Failure");
			}
		} catch (Exception e) {
			throw new Exception(e);
		} finally {
			dbUtil.reset();
		}
	}

	private void processDebitSplits() throws Exception {
		StepStage stage = (StepStage) processData.getObject(FileConstants.STEP_STAGE_KEY);
		switch (stage) {
		case BEGIN:
			updateFileProcessStatusDetails(FileConstants.STEP_PROCESS_DEBIT_SPLITS, FileConstants.STATUS_PROCESS_DEBIT_SPLIT, true);
			updateFileProcessStatus(FileConstants.STATUS_PROCESS_DEBIT_SPLIT, ProcessStage.PROCESS);
			break;
		case END:
			updateFileProcessStatusDetails(FileConstants.STEP_PROCESS_DEBIT_SPLITS, FileConstants.STATUS_PROCESS_DEBIT_SPLIT, false);
			break;
		}
	}

	private void processCreditSplits() throws Exception {
		StepStage stage = (StepStage) processData.getObject(FileConstants.STEP_STAGE_KEY);
		switch (stage) {
		case BEGIN:
			updateFileProcessStatusDetails(FileConstants.STEP_PROCESS_CREDIT_SPLITS, FileConstants.STATUS_PROCESS_CREDIT_SPLIT, true);
			updateFileProcessStatus(FileConstants.STATUS_PROCESS_CREDIT_SPLIT, ProcessStage.PROCESS);
			break;
		case END:
			updateFileProcessStatusDetails(FileConstants.STEP_PROCESS_CREDIT_SPLITS, FileConstants.STATUS_PROCESS_CREDIT_SPLIT, false);
			break;
		}
	}

	private void validateRecords() throws Exception {
		StepStage stage = (StepStage) processData.getObject(FileConstants.STEP_STAGE_KEY);
		switch (stage) {
		case BEGIN:
			updateFileProcessStatusDetails(FileConstants.STEP_VALIDATE_RECORDS, FileConstants.STATUS_VALIDATE_RECORDS, true);
			updateFileProcessStatus(FileConstants.STATUS_VALIDATE_RECORDS, ProcessStage.PROCESS);
			break;
		case END:

			updateFileProcessStatusDetails(FileConstants.STEP_VALIDATE_RECORDS, FileConstants.STATUS_VALIDATE_RECORDS, false);
			break;
		}
	}

	private void updateDebitLegs() throws Exception {
		StepStage stage = (StepStage) processData.getObject(FileConstants.STEP_STAGE_KEY);
		switch (stage) {
		case BEGIN:
			updateFileProcessStatusDetails(FileConstants.STEP_UPDATE_DEBIT_LEGS, FileConstants.STATUS_UPDATE_DEBIT_LEGS, true);
			updateFileProcessStatus(FileConstants.STATUS_UPDATE_DEBIT_LEGS, ProcessStage.PROCESS);
			break;
		case END:
			updateFileProcessStatusDetails(FileConstants.STEP_UPDATE_DEBIT_LEGS, FileConstants.STATUS_UPDATE_DEBIT_LEGS, false);
			break;
		}
	}

	private void updateCreditLegs() throws Exception {
		StepStage stage = (StepStage) processData.getObject(FileConstants.STEP_STAGE_KEY);
		switch (stage) {
		case BEGIN:
			updateFileProcessStatusDetails(FileConstants.STEP_UPDATE_CREDIT_LEGS, FileConstants.STATUS_UPDATE_CREDIT_LEGS, true);
			updateFileProcessStatus(FileConstants.STATUS_UPDATE_CREDIT_LEGS, ProcessStage.PROCESS);
			break;
		case END:
			updateFileProcessStatusDetails(FileConstants.STEP_UPDATE_CREDIT_LEGS, FileConstants.STATUS_UPDATE_CREDIT_LEGS, false);
			break;
		}
	}

	private void postDebits() throws Exception {
		StepStage stage = (StepStage) processData.getObject(FileConstants.STEP_STAGE_KEY);
		switch (stage) {
		case BEGIN:
			updateFileProcessStatusDetails(FileConstants.STEP_POST_DEBITS, FileConstants.STATUS_POST_DEBITS, true);
			updateFileProcessStatus(FileConstants.STATUS_POST_DEBITS, ProcessStage.PROCESS);
			break;
		case END:
			updateFileProcessStatusDetails(FileConstants.STEP_POST_DEBITS, FileConstants.STATUS_POST_DEBITS, false);
			break;
		}
	}

	private void postCredits() throws Exception {
		StepStage stage = (StepStage) processData.getObject(FileConstants.STEP_STAGE_KEY);
		switch (stage) {
		case BEGIN:
			updateFileProcessStatusDetails(FileConstants.STEP_POST_CREDITS, FileConstants.STATUS_POST_CREDITS, true);
			updateFileProcessStatus(FileConstants.STATUS_POST_CREDITS, ProcessStage.PROCESS);
			break;
		case END:
			updateFileProcessStatusDetails(FileConstants.STEP_POST_CREDITS, FileConstants.STATUS_POST_CREDITS, false);
			break;
		}
	}

	private void closeBatch() throws Exception {
		StepStage stage = (StepStage) processData.getObject(FileConstants.STEP_STAGE_KEY);
		switch (stage) {
		case BEGIN:
			updateFileProcessStatusDetails(FileConstants.STEP_CLOSE_BATCH, FileConstants.STATUS_CLOSE_BATCH, true);
			updateFileProcessStatus(FileConstants.STATUS_CLOSE_BATCH, ProcessStage.PROCESS);
			break;
		case END:
			updateFileProcessStatusDetails(FileConstants.STEP_CLOSE_BATCH, FileConstants.STATUS_CLOSE_BATCH, false);
			break;
		}
	}

	private void responseFileGeneration() throws Exception {
		StepStage stage = (StepStage) processData.getObject(FileConstants.STEP_STAGE_KEY);
		switch (stage) {
		case BEGIN:
			updateFileProcessStatusDetails(FileConstants.STEP_RESPONSE_FILE_GEN, FileConstants.STATUS_RESPONSE_FILE_GENERATION, true);
			updateFileProcessStatus(FileConstants.STATUS_RESPONSE_FILE_GENERATION, ProcessStage.PROCESS);
			break;
		case END:
			updateFileProcessStatusDetails(FileConstants.STEP_RESPONSE_FILE_GEN, FileConstants.STATUS_RESPONSE_FILE_GENERATION, false);
			break;
		}
	}

	private void updateSplitStatus() throws Exception {
		int splitStage = processData.getInteger(FileConstants.SPLIT_STAGE_KEY);
		StepStage stage = (StepStage) processData.getObject(FileConstants.STEP_STAGE_KEY);
		int splitId = processData.getInteger(FileConstants.SPLIT_ID_KEY);
		String updateQuery = "";
		switch (splitStage) {
		case FileConstants.SPLIT_STAGE_VALIDATION:
			switch (stage) {
			case BEGIN:
				updateQuery = "UPDATE FILESPLITDTL SET VALIDATE_START=NOW() WHERE ENTITY_CODE =? AND SOURCE_KEY = ? AND SPLIT_SL = ?";
				break;
			case END:
				updateQuery = "UPDATE FILESPLITDTL SET VALIDATE_END=NOW() WHERE ENTITY_CODE =? AND SOURCE_KEY = ? AND SPLIT_SL = ?";
				break;
			}
			break;
		case FileConstants.SPLIT_STAGE_UPDATE_LEGS:
			switch (stage) {
			case BEGIN:
				updateQuery = "UPDATE FILESPLITDTL SET UPDATE_START=NOW() WHERE ENTITY_CODE =? AND SOURCE_KEY = ? AND SPLIT_SL = ?";
				break;
			case END:
				updateQuery = "UPDATE FILESPLITDTL SET UPDATE_END=NOW() WHERE ENTITY_CODE =? AND SOURCE_KEY = ? AND SPLIT_SL = ?";
				break;
			}
			break;
		case FileConstants.SPLIT_STAGE_POST_EFFECTS:
			switch (stage) {
			case BEGIN:
				updateQuery = "UPDATE FILESPLITDTL SET POSTING_START=NOW() WHERE ENTITY_CODE =? AND SOURCE_KEY = ? AND SPLIT_SL = ?";
				break;
			case END:
				updateQuery = "UPDATE FILESPLITDTL SET POSTING_END=NOW() WHERE ENTITY_CODE =? AND SOURCE_KEY = ? AND SPLIT_SL = ?";
				break;
			}
			break;
		}
		DBUtil dbUtil = getDbContext().createUtilInstance();
		try {
			dbUtil.setMode(DBUtil.PREPARED);
			dbUtil.setSql(updateQuery);
			dbUtil.setString(1, entityCode);
			dbUtil.setString(2, sourceKey);
			dbUtil.setInt(3, splitId);
			dbUtil.executeUpdate();
		} catch (Exception e) {
			throw new Exception(e);
		} finally {
			dbUtil.reset();
		}
	}

	private void updateFileProcessStatusDetails(int stage, String stageStatus, boolean isStageBegin) throws Exception {
		DBUtil dbUtil = getDbContext().createUtilInstance();
		try {
			if (isStageBegin) {
				String updateQuery = "INSERT INTO FILEPROCSTATUSDTL (ENTITY_CODE,SOURCE_KEY,DTL_SL,STATUS,START_TIME) VALUES (?,?,?,?,NOW())";
				dbUtil.setMode(DBUtil.PREPARED);
				dbUtil.setSql(updateQuery);
				dbUtil.setString(1, entityCode);
				dbUtil.setString(2, sourceKey);
				dbUtil.setInt(3, stage);
				dbUtil.setString(4, stageStatus);
			} else {
				String updateQuery = "UPDATE FILEPROCSTATUSDTL SET STATUS = ?,END_TIME=NOW() WHERE ENTITY_CODE = ? AND SOURCE_KEY = ? AND DTL_SL=?";
				dbUtil.setMode(DBUtil.PREPARED);
				dbUtil.setSql(updateQuery);
				dbUtil.setString(1, stageStatus);
				dbUtil.setString(2, entityCode);
				dbUtil.setString(3, sourceKey);
				dbUtil.setInt(4, stage);
			}
			dbUtil.executeUpdate();
		} catch (Exception e) {
			throw new Exception(e);
		} finally {
			dbUtil.reset();
		}
	}

	private void updateFileProcessStatus(String currentStage, ProcessStage stage) throws Exception {
		DBUtil dbUtil = getDbContext().createUtilInstance();
		try {
			switch (stage) {
			case BEGIN:
				String insertQuery = "INSERT INTO FILEPROCSTATUS (ENTITY_CODE,SOURCE_KEY,STATUS,START_TIME,PROCESS_STATUS) VALUES (?,?,?,NOW(),'I')";
				dbUtil.setMode(DBUtil.PREPARED);
				dbUtil.setSql(insertQuery);
				dbUtil.setString(1, entityCode);
				dbUtil.setString(2, sourceKey);
				dbUtil.setString(3, FileConstants.STATUS_PROC_BEGIN);
				dbUtil.executeUpdate();
				insertQuery = "INSERT INTO FILEPROCSTATUSDTL (ENTITY_CODE,SOURCE_KEY,DTL_SL,STATUS,START_TIME,END_TIME) VALUES (?,?,1,?,NOW(),NOW())";
				dbUtil.reset();
				dbUtil.setMode(DBUtil.PREPARED);
				dbUtil.setSql(insertQuery);
				dbUtil.setString(1, entityCode);
				dbUtil.setString(2, sourceKey);
				dbUtil.setString(3, FileConstants.STATUS_PROC_BEGIN);
				dbUtil.executeUpdate();
				break;
			case PROCESS:
				String processUpdateQuery = "UPDATE FILEPROCSTATUS SET STATUS=? WHERE ENTITY_CODE = ? AND SOURCE_KEY = ?";
				dbUtil.setMode(DBUtil.PREPARED);
				dbUtil.setSql(processUpdateQuery);
				dbUtil.setString(1, currentStage);
				dbUtil.setString(2, entityCode);
				dbUtil.setString(3, sourceKey);
				dbUtil.executeUpdate();
				break;
			case END:
				String endUpdateQuery = "UPDATE FILEPROCSTATUS SET END_TIME=NOW(),PROCESS_STATUS=? WHERE ENTITY_CODE = ? AND SOURCE_KEY = ?";
				dbUtil.setMode(DBUtil.PREPARED);
				dbUtil.setSql(endUpdateQuery.toString());
				dbUtil.setString(1, processData.get("STATUS"));
				dbUtil.setString(2, entityCode);
				dbUtil.setString(3, sourceKey);
				dbUtil.executeUpdate();
				break;
			}
		} catch (Exception e) {
			throw new Exception(e);
		} finally {
			dbUtil.reset();
		}
	}
}