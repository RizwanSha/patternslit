package patterns.config.framework.bo;

import patterns.config.framework.DatasourceConfigurationManager;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.process.TBAProcessResult;
import patterns.config.framework.service.DTObject;

public abstract class ServiceBO extends BaseBO {
	protected DTObject processData = null;
	protected TBAProcessResult processResult = null;
	protected String dataSource="";

	public abstract TBAProcessResult processRequest();

	public void initialize(DTObject processData, DBContext dbContext) {
		super.initialize(dbContext);
		this.processResult = new TBAProcessResult();
		this.processData = processData;
		getLogger().logDebug("Process Data :: " + processData);
		dataSource=DatasourceConfigurationManager.getDatabaseSource();
	}
	
	
	

	// added by swaroopa as on 01-06-2012 begins
	/*protected void updateLoginUserAudit(DTObject input) throws Exception {
		String sqlQuery = "{CALL PKG_AUDIT_PKI.UPDATE_LOGINUSER_AUDIT(?,?,?,?,?,?,?,?,?,?,?,?,?)}";
		DBUtil util = getDbContext().createUtilInstance();
		try {
			util.reset();
			util.setMode(DBUtil.CALLABLE);
			util.setSql(sqlQuery);
			util.setString(1, input.get("P_ENTITY_CODE"));
			util.setString(2, input.get("P_PROCESS_ID"));
			util.setString(3, input.get("P_BRANCH_CODE"));
			util.setString(4, input.get("P_CUSTOMER_CODE"));
			util.setString(5, input.get("P_ACTION_CONTEXT"));
			util.setString(6, input.get("P_USER_ID"));
			util.setString(7, input.get("P_CERT_INV_NUM"));
			util.setString(8, input.get("P_USER_IP"));
			util.setString(9, input.get("P_TFA_VALUE"));
			util.setString(10, input.get("P_ENC_VALUE"));
			util.setString(11, input.get("P_UPDATE_TYPE"));
			util.setTimestamp(12, (Timestamp) input.getObject("P_CHANGE_DATETIME"));
			util.registerOutParameter(13, Types.VARCHAR);
			util.execute();
			String errorCode = util.getString(13);
			if (!errorCode.equals(RegularConstants.SP_SUCCESS)) {
				throw new Exception(BackOfficeErrorCodes.LOGIN_AUDIT_UPDATE_FAIL);
			}
		} catch (Exception e) {
			throw e;
		} finally {
			util.reset();
		}
	} // added by swaroopa as on 01-06-2012 ends
	*/

}
