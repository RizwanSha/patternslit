package patterns.config.framework.bo.lss;

import java.util.ArrayList;
import java.util.List;

import patterns.config.framework.bo.AdditionalDetailInfo;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.bo.MessageParam;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.BindParameterType;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAActionType;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTDObject;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;

public class eleasepoBO extends GenericBO {

	TBADynaSQL eleasepo = null;
	TBADynaSQL eleasepodtl = null;

	public eleasepoBO() {
	}

	@Override
	public void init() {
		long poSerial = 0;
		eleasepo = new TBADynaSQL("LEASEPO", true);
		eleasepodtl = new TBADynaSQL("LEASEPOASSETS", false);
		if (tbaContext.getProcessAction().getActionType().equals(TBAActionType.ADD) && !tbaContext.getProcessAction().isRectification()) {
			DTObject input = new DTObject();
			input.set("SOURCE_KEY", processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + BackOfficeFormatUtils.getDate(processData.getDate("CONTRACT_DATE"), BackOfficeConstants.TBA_DATE_FORMAT) + RegularConstants.PK_SEPARATOR + processData.get("CONTRACT_SL"));
			input.set("PGM_ID", tbaContext.getProcessID());
			poSerial = eleasepo.getTBAReferenceSerial(input);
			processData.set("PO_SL", Long.toString(poSerial));
		}
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + BackOfficeFormatUtils.getDate(processData.getDate("CONTRACT_DATE"), BackOfficeConstants.TBA_DATE_FORMAT) + RegularConstants.PK_SEPARATOR + processData.get("CONTRACT_SL") + RegularConstants.PK_SEPARATOR + processData.get("PO_SL");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("PO_SL"));
		eleasepo.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		eleasepo.setField("CONTRACT_DATE", processData.getDate("CONTRACT_DATE"));
		eleasepo.setField("CONTRACT_SL", processData.get("CONTRACT_SL"));
		eleasepo.setField("PO_SL", processData.get("PO_SL"));
		dataExists = eleasepo.loadByKeyFields();
	}

	@Override
	protected void add() throws TBAFrameworkException {
		try {
			if (tbaContext.getProcessAction().isRectification()) {
				eleasepo.setNew(false);
				if (!isDataExists()) {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
				}
			} else {
				eleasepo.setNew(true);
				if (isDataExists()) {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
				}
			}
			eleasepo.setField("PO_DATE", processData.getDate("PO_DATE"));
			eleasepo.setField("PO_NUMBER", processData.get("PO_NUMBER"));
			eleasepo.setField("SUPPLIER_ID", processData.get("SUPPLIER_ID"));
			eleasepo.setField("PO_CCY", processData.get("PO_CCY"));
			eleasepo.setField("TOTAL_PO_AMOUNT", processData.get("TOTAL_PO_AMOUNT"));
			eleasepo.setField("PO_AMOUNT_PAID_SO_FAR", processData.get("PO_AMOUNT_PAID_SO_FAR"));
			eleasepo.setField("REMARKS", processData.get("REMARKS"));
			if (eleasepo.save()) {
				eleasepo.setField("PURPOSE_ID", "PO_NUMBER");
				eleasepo.setField("DEDUP_KEY", processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("PO_NUMBER"));
				if (!eleasepo.udateTBADuplicateCheck()) {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
					throw new TBAFrameworkException();
				}
				eleasepodtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
				eleasepodtl.setField("CONTRACT_DATE", processData.getDate("CONTRACT_DATE"));
				eleasepodtl.setField("CONTRACT_SL", processData.get("CONTRACT_SL"));
				eleasepodtl.setField("PO_SL", processData.get("PO_SL"));
				if (tbaContext.getProcessAction().isRectification()) {
					String[] leasepodtlFields = { "ENTITY_CODE", "CONTRACT_DATE", "CONTRACT_SL", "PO_SL", "DTL_SL" };
					BindParameterType[] leasepodtlTypes = { BindParameterType.BIGINT, BindParameterType.DATE, BindParameterType.INTEGER, BindParameterType.INTEGER, BindParameterType.INTEGER };
					eleasepodtl.deleteByFieldsAudit(leasepodtlFields, leasepodtlTypes);
				}
				DTDObject detailData = processData.getDTDObject("LEASEPOASSETS");
				eleasepodtl.setNew(true);
				for (int i = 0; i < detailData.getRowCount(); ++i) {
					eleasepodtl.setField("DTL_SL", Integer.valueOf(i + 1).toString());
					eleasepodtl.setField("ASSET_TYPE_CODE", detailData.getValue(i, 1));
					eleasepodtl.setField("QUANTITY", detailData.getValue(i, 3));
					eleasepodtl.setField("ASSET_DESCRIPTION", detailData.getValue(i, 2));
					eleasepodtl.setField("ASSET_MODEL", detailData.getValue(i, 4));
					eleasepodtl.setField("ASSET_CONFIG", detailData.getValue(i, 5));
					eleasepodtl.setField("ASSET_VALUE", RegularConstants.ZERO);
					eleasepodtl.setField("ASSET_CST", RegularConstants.ZERO);
					eleasepodtl.setField("ASSET_VAT", RegularConstants.ZERO);
					eleasepodtl.setField("ASSET_ST", RegularConstants.ZERO);
					eleasepodtl.setField("ASSET_TCS", RegularConstants.ZERO);
					eleasepodtl.setField("ASSET_VAT_ON_AMOUNT", RegularConstants.ZERO);
					if (!eleasepodtl.save()) {
						processResult.setProcessStatus(TBAProcessStatus.FAILURE);
						processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
						return;
					}
				}
				if (!tbaContext.getProcessAction().isRectification()) {
					AdditionalDetailInfo additionalInfo = new AdditionalDetailInfo();
					additionalInfo.setHeading(new MessageParam(BackOfficeErrorCodes.ELEASE_GEN_RESULT));
					List<MessageParam> key = new ArrayList<MessageParam>();
					key.add(new MessageParam(BackOfficeErrorCodes.PO_SERIAL));
					additionalInfo.setKey(key);
					List<String> value = new ArrayList<String>();
					value.add(processData.get("PO_SL"));
					additionalInfo.setValue(value);
					processResult.setAdditionalInfo(additionalInfo);
				}
				processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			eleasepo.close();
			eleasepodtl.close();
		}
	}

	protected void modify() throws TBAFrameworkException {

	}
}
