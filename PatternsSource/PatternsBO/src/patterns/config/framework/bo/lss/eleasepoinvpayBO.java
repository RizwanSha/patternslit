package patterns.config.framework.bo.lss;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import patterns.config.framework.bo.AdditionalDetailInfo;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.bo.MessageParam;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.web.BackOfficeFormatUtils;

public class eleasepoinvpayBO extends GenericBO {
	long paySerial = 0;
	TBADynaSQL eleasepoinvpay = null;

	public eleasepoinvpayBO() {
	}

	public void init() {
		eleasepoinvpay = new TBADynaSQL("LEASEPOINVPAY", true);
		if (processData.get("PAYMENT_SL") != RegularConstants.EMPTY_STRING)
			paySerial = Integer.parseInt(processData.get("PAYMENT_SL"));
		else
			paySerial = (int) eleasepoinvpay.getTBAReferenceKey(processData);

		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + BackOfficeFormatUtils.getDate((Date) processData.getDate("CONTRACT_DATE"), BackOfficeConstants.TBA_DATE_FORMAT) + RegularConstants.PK_SEPARATOR + processData.get("CONTRACT_SL") + RegularConstants.PK_SEPARATOR + paySerial;
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("CONTRACT_SL"));
		eleasepoinvpay.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		eleasepoinvpay.setField("CONTRACT_DATE", processData.getDate("CONTRACT_DATE"));
		eleasepoinvpay.setField("CONTRACT_SL", processData.get("CONTRACT_SL"));
		eleasepoinvpay.setField("PAYMENT_SL", String.valueOf(paySerial));
		dataExists = eleasepoinvpay.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				eleasepoinvpay.setNew(true);
			} else {
				eleasepoinvpay.setNew(false);
			}
			eleasepoinvpay.setField("PO_SL", processData.get("PO_SL"));
			eleasepoinvpay.setField("PAY_FOR", processData.get("PAY_FOR"));
			eleasepoinvpay.setField("INVOICE_SL", processData.get("INVOICE_SL"));
			eleasepoinvpay.setField("PAYMENT_DATE", processData.getDate("PAYMENT_DATE"));
			eleasepoinvpay.setField("PAYMENT_CCY", processData.get("PAYMENT_CCY"));
			eleasepoinvpay.setField("PAYMENT_AMOUNT", processData.get("PAYMENT_AMOUNT"));
			eleasepoinvpay.setField("PAYMENT_AMOUNT_BC", processData.get("PAYMENT_AMOUNT_BC"));
			eleasepoinvpay.setField("PAYMENT_REFERENCE", processData.get("PAYMENT_REFERENCE"));
			eleasepoinvpay.setField("REMARKS", processData.get("REMARKS"));
			if (eleasepoinvpay.save()) {
				if (!tbaContext.getProcessAction().isRectification()) {
					AdditionalDetailInfo additionalInfo = new AdditionalDetailInfo();
					additionalInfo.setHeading(new MessageParam(BackOfficeErrorCodes.INV_RESULT));
					List<MessageParam> key = new ArrayList<MessageParam>();
					key.add(new MessageParam(BackOfficeErrorCodes.PAY_SERIAL));
					additionalInfo.setKey(key);
					List<String> value = new ArrayList<String>();
					value.add(String.valueOf(paySerial));
					additionalInfo.setValue(value);
					processResult.setAdditionalInfo(additionalInfo);
				}
				processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			}

		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			eleasepoinvpay.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		// TODO Auto-generated method stub

	}
}