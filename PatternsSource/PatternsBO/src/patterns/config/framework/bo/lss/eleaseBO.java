package patterns.config.framework.bo.lss;

/*
 *
 Author : NARESHKUMAR D
 Created Date : 08-MAR-2017
 Spec Reference : TATACAP/LSS/0004-ELEASE  - Lease Details Maintenance
 Modification History
 ----------------------------------------------------------------------
 Sl.No		Modified Date	Author			Modified Changes	Version
 ----------------------------------------------------------------------

 */
import java.util.Date;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.BindParameterType;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTDObject;

public class eleaseBO extends GenericBO {

	TBADynaSQL lease = null;
	TBADynaSQL leaseAssets = null;
	TBADynaSQL leaseAssetComponents = null;
	TBADynaSQL leaseRentStruc = null;
	TBADynaSQL leasePayment = null;
	TBADynaSQL leasePaymentHist = null;
	TBADynaSQL leaseFinSchedule = null;
	TBADynaSQL leaseFinScheduleHist = null;

	public eleaseBO() {
	}

	public void init() {
		lease = new TBADynaSQL("LEASE", true);
		leaseAssets = new TBADynaSQL("LEASEASSETS", false);
		leaseAssetComponents = new TBADynaSQL("LEASEASSETCOMP", false);
		leaseRentStruc = new TBADynaSQL("LEASERENTSTRUC", false);
		leasePayment = new TBADynaSQL("LEASEREPAYMENT", false);
		leasePaymentHist = new TBADynaSQL("LEASEREPAYMENTHIST", false);
		leaseFinSchedule = new TBADynaSQL("LEASEFINANCESCHEDULE", false);
		leaseFinScheduleHist = new TBADynaSQL("LEASEFINANCESCHEDULEHIST", false);

		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("LESSEE_CODE") + RegularConstants.PK_SEPARATOR + processData.get("AGREEMENT_NO") + RegularConstants.PK_SEPARATOR + processData.get("SCHEDULE_ID");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("LESSEE_CODE"));
		lease.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		lease.setField("LESSEE_CODE", processData.get("LESSEE_CODE"));
		lease.setField("AGREEMENT_NO", processData.get("AGREEMENT_NO"));
		lease.setField("SCHEDULE_ID", processData.get("SCHEDULE_ID"));
		dataExists = lease.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (tbaContext.getProcessAction().isRectification()) {
				if (isDataExists()) {
					lease.setNew(false);
					if (!recitfy()) {
						processResult.setProcessStatus(TBAProcessStatus.FAILURE);
						processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
					}
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
				}
			} else if (!isDataExists()) {
				lease.setNew(true);
				if (!update()) {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			lease.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				lease.setNew(false);
				if (!recitfy()) {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			lease.close();
		}
	}

	protected boolean recitfy() throws TBAFrameworkException {
		String[] leaseAssetFields = { "ENTITY_CODE", "LESSEE_CODE", "AGREEMENT_NO", "SCHEDULE_ID", "ASSET_SL" };
		BindParameterType[] leaseAssetFieldsTypes = { BindParameterType.BIGINT, BindParameterType.VARCHAR, BindParameterType.VARCHAR, BindParameterType.VARCHAR, BindParameterType.INTEGER };
		leaseAssets.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		leaseAssets.setField("LESSEE_CODE", processData.get("LESSEE_CODE"));
		leaseAssets.setField("AGREEMENT_NO", processData.get("AGREEMENT_NO"));
		leaseAssets.setField("SCHEDULE_ID", processData.get("SCHEDULE_ID"));
		leaseAssets.deleteByFieldsAudit(leaseAssetFields, leaseAssetFieldsTypes);

		String[] leaseAssetCompFields = { "ENTITY_CODE", "LESSEE_CODE", "AGREEMENT_NO", "SCHEDULE_ID", "ASSET_SL" };
		BindParameterType[] leaseAssetCompFieldsTypes = { BindParameterType.BIGINT, BindParameterType.VARCHAR, BindParameterType.VARCHAR, BindParameterType.VARCHAR, BindParameterType.INTEGER };
		leaseAssetComponents.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		leaseAssetComponents.setField("LESSEE_CODE", processData.get("LESSEE_CODE"));
		leaseAssetComponents.setField("AGREEMENT_NO", processData.get("AGREEMENT_NO"));
		leaseAssetComponents.setField("SCHEDULE_ID", processData.get("SCHEDULE_ID"));
		leaseAssetComponents.deleteByFieldsAudit(leaseAssetCompFields, leaseAssetCompFieldsTypes);

		String[] leaseRentStrucFields = { "ENTITY_CODE", "LESSEE_CODE", "AGREEMENT_NO", "SCHEDULE_ID", "DTL_SL" };
		BindParameterType[] leaseRentStrucTypes = { BindParameterType.BIGINT, BindParameterType.VARCHAR, BindParameterType.VARCHAR, BindParameterType.VARCHAR, BindParameterType.INTEGER };
		leaseRentStruc.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		leaseRentStruc.setField("LESSEE_CODE", processData.get("LESSEE_CODE"));
		leaseRentStruc.setField("AGREEMENT_NO", processData.get("AGREEMENT_NO"));
		leaseRentStruc.setField("SCHEDULE_ID", processData.get("SCHEDULE_ID"));
		leaseRentStruc.deleteByFieldsAudit(leaseRentStrucFields, leaseRentStrucTypes);

		String[] leaseFinSchedFields = { "ENTITY_CODE", "LESSEE_CODE", "AGREEMENT_NO", "SCHEDULE_ID", "DTL_SL" };
		BindParameterType[] leaseFinSchedTypes = { BindParameterType.BIGINT, BindParameterType.VARCHAR, BindParameterType.VARCHAR, BindParameterType.VARCHAR, BindParameterType.INTEGER };
		leaseFinSchedule.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		leaseFinSchedule.setField("LESSEE_CODE", processData.get("LESSEE_CODE"));
		leaseFinSchedule.setField("AGREEMENT_NO", processData.get("AGREEMENT_NO"));
		leaseFinSchedule.setField("SCHEDULE_ID", processData.get("SCHEDULE_ID"));
		leaseFinSchedule.deleteByFieldsAudit(leaseFinSchedFields, leaseFinSchedTypes);

		String[] leaseFinSchedFieldsHist = { "ENTITY_CODE", "LESSEE_CODE", "AGREEMENT_NO", "SCHEDULE_ID", "EFF_DATE" };
		BindParameterType[] leaseFinSchedTypesHist = { BindParameterType.BIGINT, BindParameterType.VARCHAR, BindParameterType.VARCHAR, BindParameterType.VARCHAR, BindParameterType.DATE };
		leaseFinScheduleHist.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		leaseFinScheduleHist.setField("LESSEE_CODE", processData.get("LESSEE_CODE"));
		leaseFinScheduleHist.setField("AGREEMENT_NO", processData.get("AGREEMENT_NO"));
		leaseFinScheduleHist.setField("SCHEDULE_ID", processData.get("SCHEDULE_ID"));
		leaseFinScheduleHist.deleteByFieldsAudit(leaseFinSchedFieldsHist, leaseFinSchedTypesHist);

		String[] leasePaymentFields = { "ENTITY_CODE", "LESSEE_CODE", "AGREEMENT_NO", "SCHEDULE_ID", "DTL_SL" };
		BindParameterType[] leasePaymentTypes = { BindParameterType.BIGINT, BindParameterType.VARCHAR, BindParameterType.VARCHAR, BindParameterType.VARCHAR, BindParameterType.INTEGER };
		leasePayment.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		leasePayment.setField("LESSEE_CODE", processData.get("LESSEE_CODE"));
		leasePayment.setField("AGREEMENT_NO", processData.get("AGREEMENT_NO"));
		leasePayment.setField("SCHEDULE_ID", processData.get("SCHEDULE_ID"));
		leasePayment.deleteByFieldsAudit(leasePaymentFields, leasePaymentTypes);

		String[] leasePaymentFieldsHist = { "ENTITY_CODE", "LESSEE_CODE", "AGREEMENT_NO", "SCHEDULE_ID", "EFF_DATE" };
		BindParameterType[] leasePaymentTypesHist = { BindParameterType.BIGINT, BindParameterType.VARCHAR, BindParameterType.VARCHAR, BindParameterType.VARCHAR, BindParameterType.DATE };
		leasePaymentHist.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		leasePaymentHist.setField("LESSEE_CODE", processData.get("LESSEE_CODE"));
		leasePaymentHist.setField("AGREEMENT_NO", processData.get("AGREEMENT_NO"));
		leasePaymentHist.setField("SCHEDULE_ID", processData.get("SCHEDULE_ID"));
		leasePaymentHist.deleteByFieldsAudit(leasePaymentFieldsHist, leasePaymentTypesHist);

		if (!update()) {
			return false;
		}
		return true;
	}

	private boolean update() throws TBAFrameworkException {
		System.out.println("begin   " + new Date());
		lease.setField("CUSTOMER_ID", processData.get("CUSTOMER_ID"));
		lease.setField("CONTRACT_DATE", processData.getDate("CONTRACT_DATE"));
		lease.setField("CONTRACT_SL", processData.get("CONTRACT_SL"));
		lease.setField("DATE_OF_CREATION", processData.getDate("DATE_OF_CREATION"));
		lease.setField("DATE_OF_COMMENCEMENT", processData.getDate("DATE_OF_COMMENCEMENT"));
		lease.setField("LEASE_PRODUCT_CODE", processData.get("LEASE_PRODUCT_CODE"));
		lease.setField("ASSET_CCY", processData.get("ASSET_CCY"));
		lease.setField("BILLING_ADDR_SL", processData.get("BILLING_ADDR_SL"));
		lease.setField("SHIP_ADDR_SL", processData.get("SHIP_ADDR_SL"));
		lease.setField("LESSOR_BRANCH_CODE", processData.get("LESSOR_BRANCH_CODE"));
		lease.setField("LESSOR_STATE_CODE", processData.get("LESSOR_STATE_CODE"));
		lease.setField("NOF_ASSETS", processData.get("NOF_ASSETS"));

		lease.setField("TENOR", processData.get("TENOR"));
		lease.setField("NON_CANCEL_PERIOD", processData.get("NON_CANCEL_PERIOD"));

		lease.setField("REPAY_FREQ", processData.get("REPAY_FREQ"));
		lease.setField("ADVANCE_ARREARS_FLAG", processData.get("ADVANCE_ARREARS_FLAG"));
		lease.setField("FIRST_REPAY_PERIOD_START_DATE", processData.getDate("FIRST_REPAY_PERIOD_START_DATE"));
		lease.setField("INIT_INTERIM_PERIOD_FROM", processData.getDate("INIT_INTERIM_PERIOD_FROM"));
		lease.setField("INIT_INTERIM_PERIOD_UPTO", processData.getDate("INIT_INTERIM_PERIOD_UPTO"));
		lease.setField("FINAL_INTERIM_PERIOD_FROM", processData.getDate("FINAL_INTERIM_PERIOD_FROM"));
		lease.setField("FINAL_INTERIM_PERIOD_UPTO", processData.getDate("FINAL_INTERIM_PERIOD_UPTO"));
		lease.setField("BILLING_FIRST_RENTAL_DATE", processData.getDate("BILLING_FIRST_RENTAL_DATE"));
		lease.setField("BILLING_LAST_RENTAL_DATE", processData.getDate("BILLING_LAST_RENTAL_DATE"));
		lease.setField("GROSS_ASSET_COST", processData.get("GROSS_ASSET_COST"));

		lease.setField("COMMENCEMENT_VALUE", processData.get("COMMENCEMENT_VALUE"));
		lease.setField("LEASE_RENTAL_PERIOD_OPTION", processData.get("LEASE_RENTAL_PERIOD_OPTION"));

		lease.setField("RV_PER", processData.get("RV_PER"));
		lease.setField("RV_AMOUNT", processData.get("RV_AMOUNT"));
		lease.setField("RV_TYPE", processData.get("RV_TYPE"));
		lease.setField("IRR_VALUE", processData.get("IRR_VALUE"));

		lease.setField("INIT_BROKEN_PERIOD_RENT", processData.get("INIT_BROKEN_PERIOD_RENT"));
		lease.setField("INIT_BROKEN_PERIOD_EXEC_CHGS", processData.get("INIT_BROKEN_PERIOD_EXEC_CHGS"));

		lease.setField("FINAL_BROKEN_PERIOD_RENT", processData.get("FINAL_BROKEN_PERIOD_RENT"));
		lease.setField("FINAL_BROKEN_PERIOD_EXEC_CHGS", processData.get("FINAL_BROKEN_PERIOD_EXEC_CHGS"));
		lease.setField("DEAL_TYPE", processData.get("DEAL_TYPE"));
		lease.setField("TAX_SCHEDULE_CODE", processData.get("TAX_SCHEDULE_CODE"));

		lease.setField("TDS_GROSS_NET_INDICATOR", processData.get("TDS_GROSS_NET_INDICATOR"));

		lease.setField("BANK_PROCESS_CODE", processData.get("BANK_PROCESS_CODE"));
		lease.setField("INVOICE_TEMPLATE", processData.get("INVOICE_TEMPLATE"));
		lease.setField("FAM_CODE", processData.get("FAM_CODE"));
		lease.setField("INV_CYCLE_NUMBER", processData.get("INV_CYCLE_NUMBER"));
		lease.setField("INIT_INTERIM_CYCLE_NUMBER", processData.get("INIT_INTERIM_CYCLE_NUMBER"));
		lease.setField("FINAL_INTERIM_CYCLE_NUMBER", processData.get("FINAL_INTERIM_CYCLE_NUMBER"));
		lease.setField("LAM_HIER_CODE", processData.get("LAM_HIER_CODE"));
		lease.setField("REMARKS", processData.get("REMARKS"));
		if (lease.save()) {

			if (!updateLeaseAssets()) {
				System.out.println("Error while updating Lease Assets");
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				return false;
			} else if (!updateLeaseAssetComponents()) {
				System.out.println("Error while updating Lease Asset Components");
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				return false;
			} else if (!updateLeaseRentStruc()) {
				System.out.println("Error while updating Lease Rent Structure");
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				return false;
			} else if (!updateLeaseFinSchedule()) {
				System.out.println("Error while updating Lease Finance Schedule");
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				return false;
			} else if (!updateLeaseFinScheduleHist()) {
				System.out.println("Error while updating Lease Finance Schedule Hist");
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				return false;
			} else if (!updateLeasePayment()) {
				System.out.println("Error while updating Lease Payments");
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				return false;
			} else if (!updateLeasePaymentHist()) {
				System.out.println("Error while updating Lease Payments Hist");
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				return false;
			}
			System.out.println("begin   " + new Date());
			processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
		} else {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			return false;
		}

		return true;
	}

	private boolean updateLeaseAssets() throws TBAFrameworkException {
		try {
			DTDObject detailData = processData.getDTDObject("LEASEASSETS_GRID");
			leaseAssets.setNew(true);
			for (int i = 0; i < detailData.getRowCount(); ++i) {
				leaseAssets.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
				leaseAssets.setField("LESSEE_CODE", processData.get("LESSEE_CODE"));
				leaseAssets.setField("AGREEMENT_NO", processData.get("AGREEMENT_NO"));
				leaseAssets.setField("SCHEDULE_ID", processData.get("SCHEDULE_ID"));

				leaseAssets.setField("ASSET_SL", detailData.getValue(i, "ASSET_SL"));
				leaseAssets.setField("ASSET_TYPE_CODE", detailData.getValue(i, "ASSET_TYPE_CODE"));
				leaseAssets.setField("NOF_ASSET_COMP", detailData.getValue(i, "NOF_ASSET_COMP"));

				if (!leaseAssets.save()) {
					return false;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new TBAFrameworkException();
		} finally {

		}
		return true;
	}

	private boolean updateLeaseAssetComponents() throws TBAFrameworkException {
		try {

			DTDObject dtdObject = processData.getDTDObject("LEASEASSETCOMP_GRID");
			leaseAssetComponents.setNew(true);
			for (int i = 0; i < dtdObject.getRowCount(); i++) {
				leaseAssetComponents.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
				leaseAssetComponents.setField("LESSEE_CODE", processData.get("LESSEE_CODE"));
				leaseAssetComponents.setField("AGREEMENT_NO", processData.get("AGREEMENT_NO"));
				leaseAssetComponents.setField("SCHEDULE_ID", processData.get("SCHEDULE_ID"));
				leaseAssetComponents.setField("ASSET_SL", dtdObject.getValue(i, "ASSET_SL"));

				leaseAssetComponents.setField("ASSET_COMP_SL", dtdObject.getValue(i, "ASSET_COMP_SL"));
				leaseAssetComponents.setField("ASSET_DESCRIPTION", dtdObject.getValue(i, "ASSET_DESCRIPTION"));
				if (dtdObject.getValue(i, "CUSTOMER_ASSET_ID").length() != 0)
					leaseAssetComponents.setField("CUSTOMER_ASSET_ID", dtdObject.getValue(i, "CUSTOMER_ASSET_ID"));
				leaseAssetComponents.setField("ASSET_QTY", dtdObject.getValue(i, "ASSET_QTY"));
				if (dtdObject.getValue(i, "ASSET_MODEL").length() != 0)
					leaseAssetComponents.setField("ASSET_MODEL", dtdObject.getValue(i, "ASSET_MODEL"));
				if (dtdObject.getValue(i, "ASSET_CONFIG").length() != 0)
					leaseAssetComponents.setField("ASSET_CONFIG", dtdObject.getValue(i, "ASSET_CONFIG"));
				leaseAssetComponents.setField("IND_DTLS_REQD", dtdObject.getValue(i, "IND_DTLS_REQD"));

				if (!leaseAssetComponents.save()) {
					return false;
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			throw new TBAFrameworkException();
		} finally {

		}
		return true;
	}

	private boolean updateLeaseRentStruc() throws TBAFrameworkException {
		try {
			DTDObject detailData = processData.getDTDObject("LEASERENTSTRUC_GRID");
			leaseRentStruc.setNew(true);
			for (int i = 0; i < detailData.getRowCount(); ++i) {

				leaseRentStruc.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
				leaseRentStruc.setField("LESSEE_CODE", processData.get("LESSEE_CODE"));
				leaseRentStruc.setField("AGREEMENT_NO", processData.get("AGREEMENT_NO"));
				leaseRentStruc.setField("SCHEDULE_ID", processData.get("SCHEDULE_ID"));
				leaseRentStruc.setField("DTL_SL", String.valueOf(i + 1));

				leaseRentStruc.setField("UPTO_TENOR", detailData.getValue(i, "UPTO_TENOR"));
				leaseRentStruc.setField("LEASE_RENTAL_RATE", detailData.getValue(i, "LEASE_RENTAL_RATE"));
				leaseRentStruc.setField("TENOR_RENT", detailData.getValue(i, "TENOR_RENT"));
				leaseRentStruc.setField("TENOR_EXEC_CHGS", detailData.getValue(i, "TENOR_EXEC_CHGS"));

				if (!leaseRentStruc.save()) {
					return false;
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			throw new TBAFrameworkException();
		} finally {

		}
		return true;
	}

	private boolean updateLeaseFinSchedule() throws TBAFrameworkException {
		try {
			DTDObject detailData = processData.getDTDObject("LEASEFINANCESCHEDULE_GRID");
			leaseFinSchedule.setNew(true);
			int count = 1;
			for (int i = 0; i < detailData.getRowCount(); ++i) {
				leaseFinSchedule.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
				leaseFinSchedule.setField("LESSEE_CODE", processData.get("LESSEE_CODE"));
				leaseFinSchedule.setField("AGREEMENT_NO", processData.get("AGREEMENT_NO"));
				leaseFinSchedule.setField("SCHEDULE_ID", processData.get("SCHEDULE_ID"));
				leaseFinSchedule.setField("INV_FOR", detailData.getValue(i, "INV_FOR"));
				if (detailData.getValue(i, "INV_FOR").equals("1")) {
					leaseFinSchedule.setField("DTL_SL", "1");
				} else if (detailData.getValue(i, "INV_FOR").equals("2")) {
					leaseFinSchedule.setField("DTL_SL", count++);
				} else if (detailData.getValue(i, "INV_FOR").equals("3")) {
					leaseFinSchedule.setField("DTL_SL", "1");
				}
				leaseFinSchedule.setField("RENTAL_START_DATE", detailData.getDate(i, "RENTAL_START_DATE"));
				leaseFinSchedule.setField("RENTAL_END_DATE", detailData.getDate(i, "RENTAL_END_DATE"));
				leaseFinSchedule.setField("PRINCIPAL_AMOUNT", detailData.getValue(i, "PRINCIPAL_AMOUNT"));
				leaseFinSchedule.setField("INTEREST_AMOUNT", detailData.getValue(i, "INTEREST_AMOUNT"));
				leaseFinSchedule.setField("EXECUTORY_AMOUNT", detailData.getValue(i, "EXECUTORY_AMOUNT"));
				leaseFinSchedule.setField("RENTAL_AMOUNT", detailData.getValue(i, "RENTAL_AMOUNT"));
				if ("1".equals(processData.get("ADVANCE_ARREARS_FLAG")))
					leaseFinSchedule.setField("SCHEDULED_INV_DATE", detailData.getDate(i, "RENTAL_START_DATE"));
				else if ("2".equals(processData.get("ADVANCE_ARREARS_FLAG")))
					leaseFinSchedule.setField("SCHEDULED_INV_DATE", detailData.getDate(i, "RENTAL_END_DATE"));

				if (!leaseFinSchedule.save()) {
					return false;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new TBAFrameworkException();
		} finally {

		}
		return true;
	}

	private boolean updateLeaseFinScheduleHist() throws TBAFrameworkException {
		try {

			DTDObject detailData = processData.getDTDObject("LEASEFINANCESCHEDULE_GRID");
			leaseFinScheduleHist.setNew(true);
			int count = 1;
			for (int i = 0; i < detailData.getRowCount(); ++i) {
				if (detailData.getValue(i, "INV_FOR").equals("2")) {
					leaseFinScheduleHist.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
					leaseFinScheduleHist.setField("LESSEE_CODE", processData.get("LESSEE_CODE"));
					leaseFinScheduleHist.setField("AGREEMENT_NO", processData.get("AGREEMENT_NO"));
					leaseFinScheduleHist.setField("SCHEDULE_ID", processData.get("SCHEDULE_ID"));
					leaseFinScheduleHist.setField("EFF_DATE", tbaContext.getProcessActionDateTime());
					leaseFinScheduleHist.setField("DTL_SL", count++);
					leaseFinScheduleHist.setField("RENTAL_START_DATE", detailData.getDate(i, "RENTAL_START_DATE"));
					leaseFinScheduleHist.setField("RENTAL_END_DATE", detailData.getDate(i, "RENTAL_END_DATE"));
					leaseFinScheduleHist.setField("PRINCIPAL_AMOUNT", detailData.getValue(i, "PRINCIPAL_AMOUNT"));
					leaseFinScheduleHist.setField("INTEREST_AMOUNT", detailData.getValue(i, "INTEREST_AMOUNT"));
					leaseFinScheduleHist.setField("EXECUTORY_AMOUNT", detailData.getValue(i, "EXECUTORY_AMOUNT"));
					leaseFinScheduleHist.setField("RENTAL_AMOUNT", detailData.getValue(i, "RENTAL_AMOUNT"));
					if ("1".equals(processData.get("ADVANCE_ARREARS_FLAG")))
						leaseFinScheduleHist.setField("SCHEDULED_INV_DATE", detailData.getDate(i, "RENTAL_START_DATE"));
					else if ("2".equals(processData.get("ADVANCE_ARREARS_FLAG")))
						leaseFinScheduleHist.setField("SCHEDULED_INV_DATE", detailData.getDate(i, "RENTAL_END_DATE"));
					if (!leaseFinScheduleHist.save()) {
						return false;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new TBAFrameworkException();
		} finally {

		}
		return true;
	}

	private boolean updateLeasePayment() throws TBAFrameworkException {
		try {

			DTDObject detailData = processData.getDTDObject("LEASEREPAYMENT_GRID");
			leasePayment.setNew(true);
			for (int i = 0; i < detailData.getRowCount(); ++i) {
				leasePayment.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
				leasePayment.setField("LESSEE_CODE", processData.get("LESSEE_CODE"));
				leasePayment.setField("AGREEMENT_NO", processData.get("AGREEMENT_NO"));
				leasePayment.setField("SCHEDULE_ID", processData.get("SCHEDULE_ID"));
				leasePayment.setField("DTL_SL", detailData.getValue(i, "DTL_SL"));

				leasePayment.setField("UPTO_TENOR", detailData.getValue(i, "UPTO_TENOR"));
				leasePayment.setField("REPAY_MODE", detailData.getValue(i, "REPAY_MODE"));
				if (!detailData.getValue(i, "ECS_DB_IFSC").equals(RegularConstants.EMPTY_STRING))
					leasePayment.setField("ECS_DB_IFSC", detailData.getValue(i, "ECS_DB_IFSC"));

				if (!detailData.getValue(i, "ECS_DB_BANK").equals(RegularConstants.EMPTY_STRING))
					leasePayment.setField("ECS_DB_BANK", detailData.getValue(i, "ECS_DB_BANK"));

				if (!detailData.getValue(i, "ECS_DB_BRANCH").equals(RegularConstants.EMPTY_STRING))
					leasePayment.setField("ECS_DB_BRANCH", detailData.getValue(i, "ECS_DB_BRANCH"));

				if (!detailData.getValue(i, "ECS_DB_AC_NO").equals(RegularConstants.EMPTY_STRING))
					leasePayment.setField("ECS_DB_AC_NO", detailData.getValue(i, "ECS_DB_AC_NO"));

				if (!detailData.getValue(i, "ECS_DB_PEAK_AMOUNT").isEmpty())
					leasePayment.setField("ECS_DB_PEAK_AMOUNT", detailData.getValue(i, "ECS_DB_PEAK_AMOUNT"));
				else
					leasePayment.setField("ECS_DB_PEAK_AMOUNT", RegularConstants.NULL);

				if (!detailData.getValue(i, "ECS_MANDATE_REFNO").equals(RegularConstants.EMPTY_STRING))
					leasePayment.setField("ECS_MANDATE_REFNO", detailData.getValue(i, "ECS_MANDATE_REFNO"));

				if (!detailData.getValue(i, "ECS_MANDATE_DATE").equals(RegularConstants.EMPTY_STRING))
					leasePayment.setField("ECS_MANDATE_DATE", detailData.getDate(i, "ECS_MANDATE_DATE"));

				if (!leasePayment.save()) {
					return false;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new TBAFrameworkException();
		} finally {

		}
		return true;
	}

	private boolean updateLeasePaymentHist() throws TBAFrameworkException {
		try {

			DTDObject detailData = processData.getDTDObject("LEASEREPAYMENT_GRID");
			leasePaymentHist.setNew(true);
			for (int i = 0; i < detailData.getRowCount(); ++i) {
				leasePaymentHist.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
				leasePaymentHist.setField("LESSEE_CODE", processData.get("LESSEE_CODE"));
				leasePaymentHist.setField("AGREEMENT_NO", processData.get("AGREEMENT_NO"));
				leasePaymentHist.setField("SCHEDULE_ID", processData.get("SCHEDULE_ID"));
				leasePaymentHist.setField("EFF_DATE", tbaContext.getProcessActionDateTime());
				leasePaymentHist.setField("DTL_SL", detailData.getValue(i, "DTL_SL"));

				leasePaymentHist.setField("UPTO_TENOR", detailData.getValue(i, "UPTO_TENOR"));
				leasePaymentHist.setField("REPAY_MODE", detailData.getValue(i, "REPAY_MODE"));
				if (!detailData.getValue(i, "ECS_DB_IFSC").equals(RegularConstants.EMPTY_STRING))
					leasePaymentHist.setField("ECS_DB_IFSC", detailData.getValue(i, "ECS_DB_IFSC"));

				if (!detailData.getValue(i, "ECS_DB_BANK").equals(RegularConstants.EMPTY_STRING))
					leasePaymentHist.setField("ECS_DB_BANK", detailData.getValue(i, "ECS_DB_BANK"));

				if (!detailData.getValue(i, "ECS_DB_BRANCH").equals(RegularConstants.EMPTY_STRING))
					leasePaymentHist.setField("ECS_DB_BRANCH", detailData.getValue(i, "ECS_DB_BRANCH"));

				if (!detailData.getValue(i, "ECS_DB_AC_NO").equals(RegularConstants.EMPTY_STRING))
					leasePaymentHist.setField("ECS_DB_AC_NO", detailData.getValue(i, "ECS_DB_AC_NO"));

				if (!detailData.getValue(i, "ECS_DB_PEAK_AMOUNT").isEmpty())
					leasePaymentHist.setField("ECS_DB_PEAK_AMOUNT", detailData.getValue(i, "ECS_DB_PEAK_AMOUNT"));
				else
					leasePaymentHist.setField("ECS_DB_PEAK_AMOUNT", RegularConstants.NULL);

				if (!detailData.getValue(i, "ECS_MANDATE_REFNO").equals(RegularConstants.EMPTY_STRING))
					leasePaymentHist.setField("ECS_MANDATE_REFNO", detailData.getValue(i, "ECS_MANDATE_REFNO"));

				if (!detailData.getValue(i, "ECS_MANDATE_DATE").equals(RegularConstants.EMPTY_STRING))
					leasePaymentHist.setField("ECS_MANDATE_DATE", detailData.getDate(i, "ECS_MANDATE_DATE"));

				if (!leasePaymentHist.save()) {
					return false;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new TBAFrameworkException();
		} finally {

		}
		return true;
	}

}