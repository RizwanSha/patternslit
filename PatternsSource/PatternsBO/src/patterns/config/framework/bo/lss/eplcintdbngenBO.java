package patterns.config.framework.bo.lss;

import java.sql.Types;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.ServiceBO;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessResult;
import patterns.config.framework.process.TBAProcessStatus;

public class eplcintdbngenBO extends ServiceBO {

	public eplcintdbngenBO() {
	}

	public void init() {

	}

	@Override
	public TBAProcessResult processRequest() {
		try {
			acquireLock();
			authorizedRecord();
		} catch (Exception e) {
			e.getLocalizedMessage();
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.PROCESS_NOT_SUCCESS);
			processResult.setAdditionalInfo(e.getLocalizedMessage());
		}
		return processResult;
	}

	private void authorizedRecord() throws TBAFrameworkException {
		DBUtil dbutil = getDbContext().createUtilInstance();
		try {
			dbutil.reset();
			dbutil.setMode(DBUtil.CALLABLE);
			dbutil.setSql("{CALL SP_PLC_GEN_DEBIT_NOTE(?,?,?,?,?,?,?)}");
			dbutil.setLong(1, Long.parseLong(processData.get("ENTITY_CODE")));
			dbutil.setLong(2, Long.parseLong(processData.get("TMP_SERIAL")));
			dbutil.setInt(3, Integer.parseInt(processData.get("GRP_SL")));
			dbutil.setString(4, processData.get("ENTRY_MODE"));
			dbutil.setString(5, processData.get("USER_ID"));
			dbutil.registerOutParameter(6, Types.BIGINT);
			dbutil.registerOutParameter(7, Types.VARCHAR);
			dbutil.execute();
			if (dbutil.getString(7).equals("S")) {
				processResult.setErrorCode(BackOfficeErrorCodes.PROCESS_SUCCESS);
				processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
			} else {
				processResult.setErrorCode(BackOfficeErrorCodes.PROCESS_NOT_SUCCESS);
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			}

		} catch (Exception e) {
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			dbutil.reset();
		}
	}

	private void acquireLock() throws TBAFrameworkException {
		DBUtil dbutil = getDbContext().createUtilInstance();
		try {
			dbutil.reset();
			dbutil.setMode(DBUtil.CALLABLE);
			dbutil.setSql("{CALL SP_ACQUIRE_LOCK( ?,?,?,?,?,?)}");
			dbutil.setString(1, processData.get("ENTITY_CODE"));
			dbutil.setString(2, processData.get("PROCESS_ID"));
			dbutil.registerOutParameter(3, Types.INTEGER);
			dbutil.registerOutParameter(4, Types.INTEGER);
			dbutil.registerOutParameter(5, Types.INTEGER);
			dbutil.registerOutParameter(6, Types.VARCHAR);
			dbutil.execute();
			// int V_LOG_REQ = dbutil.getInt(3);
			// int V_ADD_LOG_REQ = dbutil.getInt(4);
			int V_LOCK_STATUS = dbutil.getInt(5);
			String V_LOCK_ERR_MSG = dbutil.getString(6) == RegularConstants.NULL ? RegularConstants.EMPTY_STRING : dbutil.getString(6);
			if (V_LOCK_STATUS == 1)
				throw new TBAFrameworkException(V_LOCK_ERR_MSG);

		} catch (Exception e) {
			e.printStackTrace();
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			dbutil.reset();
		}
	}
}
