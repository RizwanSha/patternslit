package patterns.config.framework.bo.lss;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.web.BackOfficeFormatUtils;

public class ileaseintparamBO extends GenericBO {

	TBADynaSQL leaseintparamhist = null;

	public void init() {
		leaseintparamhist = new TBADynaSQL("LEASEINTPARAMHIST", true);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("LEASE_PRODUCT_CODE") + RegularConstants.PK_SEPARATOR + processData.get("PORTFOLIO_CODE") + RegularConstants.PK_SEPARATOR
				+ BackOfficeFormatUtils.getDate(processData.getDate("EFF_DATE"), BackOfficeConstants.TBA_DATE_FORMAT);
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("LEASE_PRODUCT_CODE"));
		leaseintparamhist.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		leaseintparamhist.setField("LEASE_PRODUCT_CODE", processData.get("LEASE_PRODUCT_CODE"));
		leaseintparamhist.setField("PORTFOLIO_CODE", processData.get("PORTFOLIO_CODE"));
		leaseintparamhist.setField("EFF_DATE", processData.getDate("EFF_DATE"));
		dataExists = leaseintparamhist.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				leaseintparamhist.setNew(true);
				leaseintparamhist.setField("REMIT_TO_ACNO", processData.get("REMIT_TO_ACNO"));
				leaseintparamhist.setField("IFSC_CODE", processData.get("IFSC_CODE"));
				leaseintparamhist.setField("ENABLED", processData.get("ENABLED"));
				leaseintparamhist.setField("REMARKS", processData.get("REMARKS"));
				if (leaseintparamhist.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			leaseintparamhist.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				leaseintparamhist.setNew(false);
				leaseintparamhist.setField("REMIT_TO_ACNO", processData.get("REMIT_TO_ACNO"));
				leaseintparamhist.setField("IFSC_CODE", processData.get("IFSC_CODE"));
				leaseintparamhist.setField("ENABLED", processData.get("ENABLED"));
				leaseintparamhist.setField("REMARKS", processData.get("REMARKS"));
				if (leaseintparamhist.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			leaseintparamhist.close();
		}
	}
}
