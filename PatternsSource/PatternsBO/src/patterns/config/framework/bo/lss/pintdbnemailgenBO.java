package patterns.config.framework.bo.lss;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import patterns.config.framework.bo.ServiceBO;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessResult;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTDObject;
/*
 * E-Mail Generation for Debit Note 
 *
 Author : Pavan kumar.R
 Created Date : 21-March-2017
 Spec Reference PSD-LSS-
 Modification History
 ----------------------------------------------------------------------
 Sl.No		Modified Date	Author			Modified Changes	Version

 ----------------------------------------------------------------------

 */
import patterns.config.framework.service.DTObject;

public class pintdbnemailgenBO extends ServiceBO {

	public pintdbnemailgenBO() {
	}

	public void init() {

	}

	@Override
	public TBAProcessResult processRequest() {
		try {
			if (processData.getDTDObject("PINTDBNEMAILGENDTL") != null) {
				DTDObject detailData = processData.getDTDObject("PINTDBNEMAILGENDTL");
				for (int i = 0; i < detailData.getRowCount(); i++) {
					DTObject formDTO = new DTObject();
					formDTO.reset();
					String templateCode = "INTDEBIT";
					formDTO.set("TEMPLATE_CODE", templateCode);
					formDTO.set("INTERFACE_CODE", "DISEMAIL");
					formDTO.set("ENTITY_CODE", processData.get("ENTITY_CODE"));
					formDTO.set("USER_ID", processData.get("USER_ID"));
					formDTO.set("FOR_YEAR", processData.get("FOR_YEAR"));
					formDTO.set("FOR_MONTH", processData.get("FOR_MONTH"));
					formDTO.set("DEBIT_NOTE_INVENTORY", detailData.getValue(i, "DEBIT_NOTE_INVENTORY"));
					formDTO.set("GEN_DATE", new java.sql.Timestamp(new java.util.Date().getTime()).toString());
					formDTO.set("CUSTOMER_NAME", detailData.getValue(i, "CUSTOMER_NAME"));
					formDTO.set("ADDR_SL", detailData.getValue(i, "ADDR_SL"));
					formDTO.set("CUSTOMER_ID", detailData.getValue(i, "CUSTOMER_ID"));
					formDTO.set("PDF_REPORT_IDENTIFIER", detailData.getValue(i, "PDF_REPORT_IDENTIFIER"));
					formDTO.set("PDF_FILE_INV_NUM", detailData.getValue(i, "PDF_FILE_INV_NUM"));
					if (insertDataIntoPLCINTDBNOTEEMAILSENDLOG(formDTO)) {
						if (!pushDataToLogTable(formDTO)) {
							processResult.setProcessStatus(TBAProcessStatus.FAILURE);
						}
					} else {
						processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					}
				}
			}
			processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
		} catch (Exception e) {
			e.getLocalizedMessage();
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setAdditionalInfo(e.getLocalizedMessage());
		}
		return processResult;
	}

	public boolean pushDataToLogTable(DTObject formDTO) throws TBAFrameworkException {
		try {
			String className = "com.patterns.jobs.message.handler.CommunicationHandler";
			String methodName = "dispatchEMailWithInventoryNumber";
			formDTO = getAttachmentFile(formDTO);
			formDTO = getEmailList(formDTO);
			formDTO.set("CLASS_NAME", className);
			formDTO.set("METHOD_NAME", methodName);
			String status = invokeCommunicationHandler(getDbContext(), formDTO).get("RESULT");
			if (status.equals(TBAProcessStatus.FAILURE)) {
				throw new TBAFrameworkException("Issue in generating Email");
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			throw new TBAFrameworkException(e.getLocalizedMessage());

		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private DTObject invokeCommunicationHandler(DBContext dbcontext, DTObject inputObject) throws TBAFrameworkException {
		DTObject resultDTO = null;
		Class qmClass = null;
		Object qmObject = null;
		Method getDataMethod = null;
		Class[] parameterTypes = new Class[] { DBContext.class, DTObject.class };
		Object[] params = new Object[] { dbcontext, inputObject };
		try {
			qmClass = Class.forName(inputObject.get("CLASS_NAME"));
			qmObject = qmClass.newInstance();
			getDataMethod = qmClass.getMethod(inputObject.get("METHOD_NAME"), parameterTypes);
			resultDTO = (DTObject) getDataMethod.invoke(qmObject, params);
		} catch (Exception e) {
			throw new TBAFrameworkException(e.getLocalizedMessage());
		}
		return resultDTO;
	}

	DTObject getAttachmentFile(DTObject formDTO) throws TBAFrameworkException {
		DBUtil util = getDbContext().createUtilInstance();
		Map<String, String> attachment = new HashMap<String, String>();
		File f;
		try {
			util.reset();
			if (!formDTO.get("PDF_REPORT_IDENTIFIER").equals(RegularConstants.EMPTY_STRING)) {
				util.setSql("SELECT FILE_PATH FROM REPORTDOWNLOAD  WHERE ENTITY_CODE = ? AND REPORT_IDENTIFIER=?");
				util.setString(1, processData.get("ENTITY_CODE"));
				util.setString(2, formDTO.get("PDF_REPORT_IDENTIFIER"));
				ResultSet rset = util.executeQuery();
				if (rset.next()) {
					f = new File(rset.getString("FILE_PATH"));
					if (f.exists())
						attachment.put(f.getName(), f.getParent());
				}
			}
			if (attachment.size() == 0) {
				String reportPath = reGenerateFiles(Long.parseLong(formDTO.get("PDF_FILE_INV_NUM")), Integer.parseInt(formDTO.get("FOR_YEAR")), formDTO.get("PDF_REPORT_IDENTIFIER"));
				if (!reportPath.trim().equals(RegularConstants.EMPTY_STRING)) {
					f = new File(reportPath);
					attachment.put(f.getName(), f.getParent());
				}
			}

			if (attachment.size() >= 1)
				formDTO.setObject("ATTACHMENT", attachment);
			else
				throw new TBAFrameworkException("Issue in Fetching Files");
			formDTO.set("FETCH_FILE_SUCCESS", "SUCCESS");
		} catch (Exception e) {
			e.printStackTrace();
			throw new TBAFrameworkException("Issue in Fetching Files");
		} finally {
			util.reset();
		}
		return formDTO;
	}

	String reGenerateFiles(long FileInvNo, int invYear, String reportIdentifier) throws TBAFrameworkException {
		DBUtil util = getDbContext().createUtilInstance();
		String reportPath = null;
		boolean newfileGenerated = false;
		try {
			util.reset();
			util.setSql("SELECT REPORT_GENERATION_PATH  FROM SYSTEMFOLDERS WHERE ENTITY_CODE = ? AND EFFT_DATE = (SELECT MAX(EFFT_DATE) FROM SYSTEMFOLDERS WHERE ENTITY_CODE = ? AND EFFT_DATE <= FN_GETCD(?))");
			util.setString(1, processData.get("ENTITY_CODE"));
			util.setString(2, processData.get("ENTITY_CODE"));
			util.setString(3, processData.get("ENTITY_CODE"));
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				reportPath = rset.getString("REPORT_GENERATION_PATH");
			}
			util.reset();
			util.setSql("SELECT C.FILE_DATA,D.FILE_NAME,D.FILE_EXTENSION FROM CMNFILEINVENTORYSRC? C INNER JOIN CMNFILEINVENTORYSRCDTL D ON (C.ENTITY_CODE=D.ENTITY_CODE AND C.FILE_INV_NUM=D.FILE_INV_NUM) WHERE C.ENTITY_CODE = ? AND C.FILE_INV_NUM=?");
			util.setInt(1, invYear);
			util.setString(2, processData.get("ENTITY_CODE"));
			util.setLong(3, FileInvNo);
			ResultSet rt = util.executeQuery();
			if (rt.next()) {
				if (!rt.getString("FILE_DATA").trim().equals(RegularConstants.EMPTY_STRING)) {
					reportPath = reportPath + rt.getString("FILE_NAME") + "." + rt.getString("FILE_EXTENSION");
					File genearteFile = new File(reportPath);
					FileOutputStream output = new FileOutputStream(genearteFile);
					InputStream in = rt.getBinaryStream("FILE_DATA");
					byte[] buffer = new byte[4096];
					while (in.read(buffer) > 0) {
						output.write(buffer);
					}
					output.close();
					in.close();
					newfileGenerated = true;
				}
			}
			if (newfileGenerated) {
				util.reset();
				util.setSql("UPDATE REPORTDOWNLOAD SET FILE_PATH=?  WHERE ENTITY_CODE = ? AND REPORT_IDENTIFIER=?");
				util.setString(1, reportPath);
				util.setString(2, processData.get("ENTITY_CODE"));
				util.setString(3, reportIdentifier);
				util.executeUpdate();
			}
			if (newfileGenerated)
				return reportPath;
			else
				return RegularConstants.EMPTY_STRING;

		} catch (Exception e) {
			e.printStackTrace();
			throw new TBAFrameworkException("Issue in generating Files");
		} finally {
			util.reset();
		}
	}

	DTObject getEmailList(DTObject formDTO) throws TBAFrameworkException {
		DBUtil util = getDbContext().createUtilInstance();
		List<String> toEmailList = new ArrayList<String>();
		List<String> ccEmailList = new ArrayList<String>();
		try {
			util.reset();
			util.setSql("SELECT C.EMAIL_ID,C.ALTERNATE_EMAIL FROM CUSTOMERCONTACTDTL C WHERE C.ENTITY_CODE=? AND C.CUSTOMER_ID=? AND C.ADDR_SL=? ");
			util.setLong(1, Long.parseLong(processData.get("ENTITY_CODE")));
			util.setLong(2, Long.parseLong(formDTO.get("CUSTOMER_ID")));
			util.setInt(3, Integer.parseInt(formDTO.get("ADDR_SL")));
			ResultSet rset = util.executeQuery();
			while (rset.next()) {
				if (rset.getString("ALTERNATE_EMAIL").equals("1"))
					ccEmailList.add(rset.getString("EMAIL_ID"));
				else
					toEmailList.add(rset.getString("EMAIL_ID"));
			}
			if (toEmailList.size() >= 1)
				formDTO.setObject("TO_EMAIL_ID_LIST", toEmailList);

			util.reset();

			if (ccEmailList.size() >= 1)
				formDTO.setObject("CC_EMAIL_ID_LIST", ccEmailList);
			if (ccEmailList.size() >= 1 || toEmailList.size() >= 1) {
				formDTO.setObject("FETCH_EMAIL_SUCCESS", "SUCCESS");
			} else {
				throw new TBAFrameworkException("Issue in Fetching EmailId");
			}

		} catch (Exception e) {
			e.printStackTrace();
			throw new TBAFrameworkException("Issue in Fetching EmailId");
		} finally {
			util.reset();
		}
		return formDTO;
	}

	private boolean insertDataIntoPLCINTDBNOTEEMAILSENDLOG(DTObject inputDTO) throws TBAFrameworkException {
		StringBuffer sqlQuery = new StringBuffer(RegularConstants.EMPTY_STRING);
		sqlQuery.append("INSERT INTO PLCINTDBNOTEEMAILSENDLOG(ENTITY_CODE,DEBIT_NOTE_INVENTORY,DTL_SL,EMAIL_LOG_INVENTORY_NO,REQUESTED_DATETIME,REQUESTED_BY)");
		sqlQuery.append("SELECT ?,?,COALESCE(MAX(DTL_SL)+1,1),?,?,? FROM PLCINTDBNOTEEMAILSENDLOG  WHERE ENTITY_CODE=? AND DEBIT_NOTE_INVENTORY=? ");
		try {
			DBUtil util = getDbContext().createUtilInstance();
			util.reset();
			util.setMode(DBUtil.PREPARED);
			util.setSql("SELECT FN_NEXTVAL('SEQ_EMAIL') FROM DUAL");
			ResultSet emailSequenceRSet = util.executeQuery();
			if (emailSequenceRSet.next()) {
				if (emailSequenceRSet.getString(1) != null)
					inputDTO.set("INVENTORY_NO", emailSequenceRSet.getString(1));
				else
					throw new TBAFrameworkException("Issue in generating Email InventoryNo");
			}
			util.reset();
			util.setMode(DBUtil.PREPARED);
			util.setSql(sqlQuery.toString());
			util.setLong(1, Long.parseLong(processData.get("ENTITY_CODE")));
			util.setLong(2, Long.parseLong(inputDTO.get("DEBIT_NOTE_INVENTORY")));
			util.setLong(3, Long.parseLong(inputDTO.get("INVENTORY_NO")));
			util.setString(4, new java.sql.Timestamp(new java.util.Date().getTime()).toString());
			util.setString(5, processData.get("USER_ID"));
			util.setLong(6, Long.parseLong(processData.get("ENTITY_CODE")));
			util.setLong(7, Long.parseLong(inputDTO.get("DEBIT_NOTE_INVENTORY")));
			util.executeUpdate();
			util.reset();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			throw new TBAFrameworkException(e.getLocalizedMessage());
		}
	}
}
