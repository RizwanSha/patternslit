package patterns.config.framework.bo.lss;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import patterns.config.framework.bo.AdditionalDetailInfo;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.bo.MessageParam;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.BindParameterType;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAActionType;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;

public class eleasepoinvupldBO extends GenericBO {

	TBADynaSQL eleasepoinvupld = null;
	TBADynaSQL eleasepoinvuplddtl = null;
	TBADynaSQL eleasepoinvupldpaydtl = null;
	TBADynaSQL eleasepoinvupldstdtl = null;

	public eleasepoinvupldBO() {
	}

	@Override
	public void init() {
		long serial = 0;
		eleasepoinvupld = new TBADynaSQL("LEASEPOINVUPLD", true);
		eleasepoinvuplddtl = new TBADynaSQL("LEASEPOINVUPLDDTL", false);
		eleasepoinvupldpaydtl = new TBADynaSQL("LEASEPOINVUPLDPAYDTL", false);
		eleasepoinvupldstdtl = new TBADynaSQL("LEASEPOINVUPLDASTDTL", false);
		if (tbaContext.getProcessAction().getActionType().equals(TBAActionType.ADD) && !tbaContext.getProcessAction().isRectification()) {
			DTObject input = new DTObject();
			input.set("SOURCE_KEY", processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + BackOfficeFormatUtils.getDate(processData.getDate("ENTRY_DATE"), BackOfficeConstants.TBA_DATE_FORMAT));
			input.set("PGM_ID", tbaContext.getProcessID());
			serial = eleasepoinvupld.getTBAReferenceSerial(input);
			processData.set("ENTRY_SL", Long.toString(serial));
		}
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + BackOfficeFormatUtils.getDate(processData.getDate("ENTRY_DATE"), BackOfficeConstants.TBA_DATE_FORMAT) + RegularConstants.PK_SEPARATOR + processData.get("ENTRY_SL");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("ENTRY_SL"));
		eleasepoinvupld.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		eleasepoinvupld.setField("ENTRY_DATE", processData.getDate("ENTRY_DATE"));
		eleasepoinvupld.setField("ENTRY_SL", processData.get("ENTRY_SL"));
		dataExists = eleasepoinvupld.loadByKeyFields();
	}

	@Override
	protected void add() throws TBAFrameworkException {
		// TODO Auto-generated method stub
		try {
			if (!checkUploadFile()) {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			}
			if (tbaContext.getProcessAction().isRectification()) {
				eleasepoinvupld.setNew(false);
				if (isDataExists()) {
					eleasepoinvupld.setNew(false);
					if (!recitfy()) {
						processResult.setProcessStatus(TBAProcessStatus.FAILURE);
						processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
					}
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
				}
			} else if (!isDataExists()) {
				eleasepoinvupld.setNew(true);
				if (!updateUploadDetails()) {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}

		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			eleasepoinvupld.close();
			eleasepoinvuplddtl.close();
		}
	}

	@Override
	protected void modify() throws TBAFrameworkException {
		// TODO Auto-generated method stub

	}

	private boolean checkUploadFile() {
		boolean recordNotMatched = true;
		DBUtil util = getDbContext().createUtilInstance();
		String sqlQuery = "SELECT SUBSTRING_INDEX(CONTRACT_REF, '-', 3)AS CONT_DATE,REPLACE(CONTRACT_REF,CONCAT(SUBSTRING_INDEX(CONTRACT_REF, '-', 3),'-'),'')AS CONT_SL FROM TMPPOINVUPLDDTL WHERE ENTITY_CODE = ? AND TMP_SERIAL=?";
		try {
			util.setMode(DBUtil.PREPARED);
			util.setSql(sqlQuery);
			util.setLong(1, Long.parseLong(processData.get("ENTITY_CODE")));
			util.setLong(2, Long.parseLong(processData.get("TMP_SERIAL")));
			ResultSet rs = util.executeQuery();
			while (rs.next()) {
				int sl = Integer.parseInt(rs.getString("CONT_SL"));
				int conSl = Integer.parseInt(processData.get("CONTRACT_SL"));
				recordNotMatched = false;
				if (BackOfficeFormatUtils.getDate(processData.get("CONTRACT_DATE"), BackOfficeConstants.TBA_DATE_FORMAT).getTime() != BackOfficeFormatUtils.getDate(rs.getString("CONT_DATE"), BackOfficeConstants.TBA_DATE_FORMAT).getTime() || conSl != sl) {
					recordNotMatched = false;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			util.reset();
		}
		return recordNotMatched;
	}

	protected boolean recitfy() throws TBAFrameworkException {
		String[] eleasepoinvupldFields = { "ENTITY_CODE", "ENTRY_DATE", "ENTRY_SL", "DTL_SL" };
		BindParameterType[] eleasepoinvupldTypes = { BindParameterType.BIGINT, BindParameterType.DATE, BindParameterType.BIGINT, BindParameterType.INTEGER };
		eleasepoinvuplddtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		eleasepoinvuplddtl.setField("ENTRY_DATE", processData.getDate("ENTRY_DATE"));
		eleasepoinvuplddtl.setField("ENTRY_SL", processData.get("ENTRY_SL"));
		eleasepoinvuplddtl.deleteByFieldsAudit(eleasepoinvupldFields, eleasepoinvupldTypes);

		String[] eleasepoinvupldpayFields = { "ENTITY_CODE", "ENTRY_DATE", "ENTRY_SL", "DTL_SL" };
		BindParameterType[] eleasepoinvupldpayTypes = { BindParameterType.BIGINT, BindParameterType.DATE, BindParameterType.BIGINT, BindParameterType.INTEGER };
		eleasepoinvupldpaydtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		eleasepoinvupldpaydtl.setField("ENTRY_DATE", processData.getDate("ENTRY_DATE"));
		eleasepoinvupldpaydtl.setField("ENTRY_SL", processData.get("ENTRY_SL"));
		eleasepoinvupldpaydtl.deleteByFieldsAudit(eleasepoinvupldpayFields, eleasepoinvupldpayTypes);

		String[] eleasepoinvupldstdtlFields = { "ENTITY_CODE", "ENTRY_DATE", "ENTRY_SL", "DTL_SL" };
		BindParameterType[] eleasepoinvupldstdtlTypes = { BindParameterType.BIGINT, BindParameterType.DATE, BindParameterType.BIGINT, BindParameterType.INTEGER };
		eleasepoinvupldstdtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		eleasepoinvupldstdtl.setField("ENTRY_DATE", processData.getDate("ENTRY_DATE"));
		eleasepoinvupldstdtl.setField("ENTRY_SL", processData.get("ENTRY_SL"));
		eleasepoinvupldstdtl.deleteByFieldsAudit(eleasepoinvupldstdtlFields, eleasepoinvupldstdtlTypes);

		if (!updateUploadDetails()) {
			return false;
		}
		return true;
	}

	private boolean updateUploadDetails() throws TBAFrameworkException {

		eleasepoinvupld.setField("CONTRACT_DATE", processData.getDate("CONTRACT_DATE"));
		eleasepoinvupld.setField("CONTRACT_SL", processData.get("CONTRACT_SL"));
		eleasepoinvupld.setField("PO_SL", processData.get("PO_SL"));
		eleasepoinvupld.setField("FILE_INV_NUM", processData.get("FILE_INV_NUM"));
		eleasepoinvupld.setField("REMARKS", processData.get("REMARKS"));
		eleasepoinvupld.setField("UPLOADED_BY", processData.get("UPLOADED_BY"));
		eleasepoinvupld.setField("UPLOADED_ON", tbaContext.getProcessActionDateTime());
		if (eleasepoinvupld.save()) {

			if (!updateLeaseInvoiceUploadDetails()) {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				return false;
			}
			if (!tbaContext.getProcessAction().isRectification()) {
				AdditionalDetailInfo additionalInfo = new AdditionalDetailInfo();
				additionalInfo.setHeading(new MessageParam(BackOfficeErrorCodes.ECUST_GEN_RESULT));
				List<MessageParam> key = new ArrayList<MessageParam>();
				key.add(new MessageParam(BackOfficeErrorCodes.EBILL_UPLOAD_SERIAL));
				additionalInfo.setKey(key);
				List<String> value = new ArrayList<String>();
				value.add(processData.get("ENTRY_SL"));
				additionalInfo.setValue(value);
				processResult.setAdditionalInfo(additionalInfo);
			}
			processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
		} else {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			return false;
		}

		return true;
	}

	private boolean updateLeaseInvoiceUploadDetails() throws TBAFrameworkException {
		try {
			int i = 1;
			DBUtil util = getDbContext().createUtilInstance();
			util.reset();
			util.setMode(DBUtil.PREPARED);
			util.setSql("SELECT CONTRACT_REF,PO_NUMBER,PAY_SL_LIST,INVOICE_DATE,INVOICE_NUMBER,INVOICE_CCY,INVOICE_AMOUNT FROM TMPPOINVUPLDDTL WHERE ENTITY_CODE=? AND TMP_SERIAL=? GROUP BY PO_NUMBER,INVOICE_DATE,INVOICE_NUMBER,INVOICE_CCY,INVOICE_AMOUNT ORDER BY CONTRACT_REF,PO_NUMBER,INVOICE_DATE,INVOICE_NUMBER");
			util.setLong(1, Long.parseLong(processData.get("ENTITY_CODE")));
			util.setLong(2, Long.parseLong(processData.get("TMP_SERIAL")));
			ResultSet rs = util.executeQuery();
			while (rs.next()) {
				eleasepoinvuplddtl.setNew(true);
				eleasepoinvuplddtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
				eleasepoinvuplddtl.setField("ENTRY_DATE", processData.getDate("ENTRY_DATE"));
				eleasepoinvuplddtl.setField("ENTRY_SL", processData.get("ENTRY_SL"));
				eleasepoinvuplddtl.setField("DTL_SL", i);
				eleasepoinvuplddtl.setField("CONTRACT_DATE", processData.getDate("CONTRACT_DATE"));
				eleasepoinvuplddtl.setField("CONTRACT_SL", processData.get("CONTRACT_SL"));
				eleasepoinvuplddtl.setField("PO_SL", processData.get("PO_SL"));
				eleasepoinvuplddtl.setField("PO_NUMBER", rs.getString("PO_NUMBER"));
				if (rs.getString("PAY_SL_LIST").isEmpty() || rs.getString("PAY_SL_LIST") == RegularConstants.NULL)
					eleasepoinvuplddtl.setField("INV_FOR", "N");
				else
					eleasepoinvuplddtl.setField("INV_FOR", "P");
				eleasepoinvuplddtl.setField("INVOICE_DATE", rs.getString("INVOICE_DATE"));
				eleasepoinvuplddtl.setField("INVOICE_NUMBER", rs.getString("INVOICE_NUMBER"));
				eleasepoinvuplddtl.setField("INVOICE_CCY", rs.getString("INVOICE_CCY"));
				eleasepoinvuplddtl.setField("INVOICE_AMOUNT", rs.getString("INVOICE_AMOUNT"));
				processData.set("SL", String.valueOf(i));
				if (!eleasepoinvuplddtl.save()) {
					return false;
				}
				if (!rs.getString("PAY_SL_LIST").isEmpty() && rs.getString("PAY_SL_LIST") != RegularConstants.NULL)
					updateUploadPaymentDetails(processData.get("SL"), rs.getString("PAY_SL_LIST"));
				updateUploadAssetDetails(processData.get("SL"), rs.getString("CONTRACT_REF"), rs.getString("PO_NUMBER"), rs.getString("INVOICE_DATE"), rs.getString("INVOICE_NUMBER"));
				i++;
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new TBAFrameworkException();
		} finally {

		}
		return true;
	}

	private boolean updateUploadPaymentDetails(String sl, String paymentSl) throws TBAFrameworkException {
		try {
			if (paymentSl.contains(",")) {
				StringTokenizer st = new StringTokenizer(paymentSl.toString(), ",");
				while (st.hasMoreTokens()) {
					String paySl = st.nextToken();
					eleasepoinvupldpaydtl.setNew(true);
					eleasepoinvupldpaydtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
					eleasepoinvupldpaydtl.setField("ENTRY_DATE", processData.getDate("ENTRY_DATE"));
					eleasepoinvupldpaydtl.setField("ENTRY_SL", processData.get("ENTRY_SL"));
					eleasepoinvupldpaydtl.setField("DTL_SL", sl);
					eleasepoinvupldpaydtl.setField("PAY_SL", paySl);
					if (!eleasepoinvupldpaydtl.save()) {
						return false;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new TBAFrameworkException();
		} finally {
		}
		return true;
	}

	private boolean updateUploadAssetDetails(String sl, String plcRef, String poNumber, String invDate, String invNo) throws TBAFrameworkException {
		try {
			int j = 1;
			DBUtil util1 = getDbContext().createUtilInstance();
			util1.reset();
			util1.setMode(DBUtil.PREPARED);
			util1.setSql("SELECT ASSET_TYPE_CODE,QUANTITY,ASSET_DESCRIPTION,ASSET_MODEL,ASSET_CONFIG,ASSET_VALUE FROM TMPPOINVUPLDDTL WHERE ENTITY_CODE=? AND TMP_SERIAL=? AND CONTRACT_REF=? AND PO_NUMBER=? AND INVOICE_DATE=? AND INVOICE_NUMBER=?");
			util1.setLong(1, Long.parseLong(processData.get("ENTITY_CODE")));
			util1.setLong(2, Long.parseLong(processData.get("TMP_SERIAL")));
			util1.setString(3, plcRef);
			util1.setString(4, poNumber);
			util1.setString(5, invDate);
			util1.setString(6, invNo);
			ResultSet rset4 = util1.executeQuery();
			while (rset4.next()) {
				eleasepoinvupldstdtl.setNew(true);
				eleasepoinvupldstdtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
				eleasepoinvupldstdtl.setField("ENTRY_DATE", processData.getDate("ENTRY_DATE"));
				eleasepoinvupldstdtl.setField("ENTRY_SL", processData.get("ENTRY_SL"));
				eleasepoinvupldstdtl.setField("DTL_SL", sl);
				eleasepoinvupldstdtl.setField("ASSET_SL", j++);
				eleasepoinvupldstdtl.setField("ASSET_TYPE_CODE", rset4.getString("ASSET_TYPE_CODE"));
				eleasepoinvupldstdtl.setField("QUANTITY", rset4.getString("QUANTITY"));
				eleasepoinvupldstdtl.setField("ASSET_DESCRIPTION", rset4.getString("ASSET_DESCRIPTION"));
				eleasepoinvupldstdtl.setField("ASSET_MODEL", rset4.getString("ASSET_MODEL"));
				eleasepoinvupldstdtl.setField("ASSET_CONFIG", rset4.getString("ASSET_CONFIG"));
				eleasepoinvupldstdtl.setField("ASSET_VALUE", rset4.getString("ASSET_VALUE"));
				eleasepoinvupldstdtl.setField("ASSET_CST", RegularConstants.ZERO);
				eleasepoinvupldstdtl.setField("ASSET_VAT", RegularConstants.ZERO);
				eleasepoinvupldstdtl.setField("ASSET_ST", RegularConstants.ZERO);
				eleasepoinvupldstdtl.setField("ASSET_TCS", RegularConstants.ZERO);
				eleasepoinvupldstdtl.setField("ASSET_VAT_ON_AMOUNT", RegularConstants.ZERO);
				if (!eleasepoinvupldstdtl.save()) {
					return false;
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			throw new TBAFrameworkException();
		} finally {
			// util.reset();
		}
		return true;
	}
}
