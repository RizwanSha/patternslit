package patterns.config.framework.bo.lss;

import java.util.ArrayList;
import java.util.List;

import patterns.config.framework.bo.AdditionalDetailInfo;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.bo.MessageParam;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.BindParameterType;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAActionType;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTDObject;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;

public class eleasepdcBO extends GenericBO {

	TBADynaSQL eleasepdc = null;
	TBADynaSQL eleasepdcdtl = null;

	public eleasepdcBO() {
	}

	@Override
	public void init() {
		long pdcSerial = 0;
		eleasepdc = new TBADynaSQL("LEASEPDC", true);
		eleasepdcdtl = new TBADynaSQL("LEASEPDCDTLS", false);
		if (tbaContext.getProcessAction().getActionType().equals(TBAActionType.ADD) && !tbaContext.getProcessAction().isRectification()) {
			DTObject input = new DTObject();
			input.set("SOURCE_KEY", processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("LESSEE_CODE") + RegularConstants.PK_SEPARATOR + processData.get("AGREEMENT_NO") + RegularConstants.PK_SEPARATOR + processData.get("SCHEDULE_ID"));
			input.set("PGM_ID", tbaContext.getProcessID());
			pdcSerial = eleasepdc.getTBAReferenceSerial(input);
			processData.set("PDC_LOT_SL", Long.toString(pdcSerial));
		}
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("LESSEE_CODE") + RegularConstants.PK_SEPARATOR + processData.get("AGREEMENT_NO") + RegularConstants.PK_SEPARATOR + processData.get("SCHEDULE_ID") + RegularConstants.PK_SEPARATOR + processData.get("PDC_LOT_SL");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("LESSEE_CODE"));
		eleasepdc.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		eleasepdc.setField("LESSEE_CODE", processData.get("LESSEE_CODE"));
		eleasepdc.setField("AGREEMENT_NO", processData.get("AGREEMENT_NO"));
		eleasepdc.setField("SCHEDULE_ID", processData.get("SCHEDULE_ID"));
		eleasepdc.setField("PDC_LOT_SL", processData.get("PDC_LOT_SL"));
		dataExists = eleasepdc.loadByKeyFields();
	}

	@Override
	protected void add() throws TBAFrameworkException {
		// TODO Auto-generated method stub
		try {
			if (tbaContext.getProcessAction().isRectification()) {
				eleasepdc.setNew(false);
				if (!isDataExists()) {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
				}
			} else {
				eleasepdc.setNew(true);
				if (isDataExists()) {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
				}
			}
			eleasepdc.setField("LOT_DISABLED", processData.get("LOT_DISABLED"));
			if (processData.get("LOT_DISABLED").equals(RegularConstants.ZERO)) {
				eleasepdc.setField("DATE_OF_ENTRY", processData.getDate("DATE_OF_ENTRY"));
				eleasepdc.setField("CHQ_BANK", processData.get("CHQ_BANK"));
				eleasepdc.setField("CHQ_BRANCH", processData.get("CHQ_BRANCH"));
				eleasepdc.setField("CHQ_BRANCH_IFSC", processData.get("CHQ_BRANCH_IFSC"));
				eleasepdc.setField("CHQ_AC_NO", processData.get("CHQ_AC_NO"));
				eleasepdc.setField("CHQ_AC_NAME", processData.get("CHQ_AC_NAME"));
				eleasepdc.setField("BANK_PROCESS_CODE", processData.get("BANK_PROCESS_CODE"));
				eleasepdc.setField("NOF_PDC", processData.get("NOF_PDC"));
			}
			eleasepdc.setField("REMARKS", processData.get("REMARKS"));
			if (eleasepdc.save()) {
				if (processData.get("LOT_DISABLED").equals(RegularConstants.ZERO)) {
					eleasepdcdtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
					eleasepdcdtl.setField("LESSEE_CODE", processData.get("LESSEE_CODE"));
					eleasepdcdtl.setField("AGREEMENT_NO", processData.get("AGREEMENT_NO"));
					eleasepdcdtl.setField("SCHEDULE_ID", processData.get("SCHEDULE_ID"));
					eleasepdcdtl.setField("PDC_LOT_SL", processData.get("PDC_LOT_SL"));
					if (tbaContext.getProcessAction().isRectification()) {
						String[] leasepdcdtlFields = { "ENTITY_CODE", "LESSEE_CODE", "AGREEMENT_NO", "SCHEDULE_ID", "PDC_LOT_SL", "DTL_SL" };
						BindParameterType[] leasepdcdtlTypes = { BindParameterType.BIGINT, BindParameterType.VARCHAR, BindParameterType.VARCHAR, BindParameterType.VARCHAR, BindParameterType.INTEGER, BindParameterType.INTEGER };
						eleasepdcdtl.deleteByFieldsAudit(leasepdcdtlFields, leasepdcdtlTypes);
					}
					DTDObject detailData = processData.getDTDObject("LEASEPDCDTLS");
					eleasepdcdtl.setNew(true);
					for (int i = 0; i < detailData.getRowCount(); ++i) {
						eleasepdcdtl.setField("DTL_SL", Integer.valueOf(i + 1).toString());
						eleasepdcdtl.setField("CHQ_INSTRUMENT_NO", detailData.getValue(i, 1));
						eleasepdcdtl.setField("CHQ_DATE", BackOfficeFormatUtils.getDate(detailData.getValue(i, 2), BackOfficeConstants.TBA_DATE_FORMAT));
						eleasepdcdtl.setField("CHQ_AMOUNT", detailData.getValue(i, 4));
						if (!eleasepdcdtl.save()) {
							processResult.setProcessStatus(TBAProcessStatus.FAILURE);
							processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
							return;
						}
					}
				}
				if (!tbaContext.getProcessAction().isRectification()) {
					AdditionalDetailInfo additionalInfo = new AdditionalDetailInfo();
					additionalInfo.setHeading(new MessageParam(BackOfficeErrorCodes.ECUST_GEN_RESULT));
					List<MessageParam> key = new ArrayList<MessageParam>();
					key.add(new MessageParam(BackOfficeErrorCodes.PDC_LOT_SERIAL));
					additionalInfo.setKey(key);
					List<String> value = new ArrayList<String>();
					value.add(processData.get("PDC_LOT_SL"));
					additionalInfo.setValue(value);
					processResult.setAdditionalInfo(additionalInfo);
				}
				processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			eleasepdc.close();
			eleasepdcdtl.close();
		}
	}

	protected void modify() throws TBAFrameworkException {

	}
}
