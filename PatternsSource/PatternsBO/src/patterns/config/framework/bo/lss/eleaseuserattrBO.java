
/**
 * 
 * 
 * @version 1.0
 * @author Kalimuthu U
 * @since 22-MAR-2017 <br>
 *        <b>ELEASEUSERATTR</b> <BR>
 *        <U><B> Lease - Additional Attributes Specification</B></U> <br>
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 */
package patterns.config.framework.bo.lss;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.BindParameterType;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTDObject;

public class eleaseuserattrBO extends GenericBO {

	TBADynaSQL eleaseuserattr = null;
	TBADynaSQL eleaseuserattrdtl = null;

	public eleaseuserattrBO() {
	}

	@Override
	public void init() {
		eleaseuserattr = new TBADynaSQL("LEASEUSERATTR", true);
		eleaseuserattrdtl = new TBADynaSQL("LEASEUSERATTRDTL", false);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("LESSEE_CODE") + RegularConstants.PK_SEPARATOR + processData.get("AGREEMENT_NO") + RegularConstants.PK_SEPARATOR + processData.get("SCHEDULE_ID");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("LESSEE_CODE"));
		eleaseuserattr.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		eleaseuserattr.setField("LESSEE_CODE", processData.get("LESSEE_CODE"));
		eleaseuserattr.setField("AGREEMENT_NO", processData.get("AGREEMENT_NO"));
		eleaseuserattr.setField("SCHEDULE_ID", processData.get("SCHEDULE_ID"));
		dataExists = eleaseuserattr.loadByKeyFields();

	}

	@Override
	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				eleaseuserattr.setField("ENABLED", processData.get("ENABLED"));
				eleaseuserattr.setField("REMARKS", processData.get("REMARKS"));
				eleaseuserattr.setNew(true);
				if (eleaseuserattr.save()) {
					eleaseuserattrdtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
					eleaseuserattrdtl.setField("LESSEE_CODE", processData.get("LESSEE_CODE"));
					eleaseuserattrdtl.setField("AGREEMENT_NO", processData.get("AGREEMENT_NO"));
					eleaseuserattrdtl.setField("SCHEDULE_ID", processData.get("SCHEDULE_ID"));
					eleaseuserattrdtl.setField("FIELD_SL", processData.get("FIELD_SL"));
					DTDObject detailData = processData.getDTDObject("eleaseuserattrdtl");
					eleaseuserattrdtl.setNew(true);
					int count = 1;
					for (int i = 0; i < detailData.getRowCount(); ++i) {
						eleaseuserattrdtl.setField("FIELD_SL", count);
						eleaseuserattrdtl.setField("FIELD_ID", detailData.getValue(i, 1));
						eleaseuserattrdtl.setField("FIELD_VALUE_TYPE", detailData.getValue(i, 4));
						if (detailData.getValue(i, 4).equals("T")) {
							eleaseuserattrdtl.setField("FIELD_TEXT", detailData.getValue(i, 3));
							eleaseuserattrdtl.setField("FIELD_NUMERIC", RegularConstants.NULL);
						} else if (!detailData.getValue(i, 4).equals("T")) {
							eleaseuserattrdtl.setField("FIELD_TEXT", RegularConstants.NULL);
							eleaseuserattrdtl.setField("FIELD_NUMERIC", detailData.getValue(i, 3));
						}
						if (!eleaseuserattrdtl.save()) {
							processResult.setProcessStatus(TBAProcessStatus.FAILURE);
							processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
							return;
						}
						count++;
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());

		} finally {
			eleaseuserattr.close();
			eleaseuserattrdtl.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				eleaseuserattr.setField("ENABLED", processData.get("ENABLED"));
				eleaseuserattr.setField("REMARKS", processData.get("REMARKS"));
				eleaseuserattr.setNew(false);
				if (eleaseuserattr.save()) {
					String[] eleaseuserattrFields = { "ENTITY_CODE", "LESSEE_CODE", "AGREEMENT_NO", "SCHEDULE_ID", "FIELD_SL" };
					BindParameterType[] eleaseuserattrFieldsTypes = { BindParameterType.BIGINT, BindParameterType.VARCHAR, BindParameterType.VARCHAR, BindParameterType.VARCHAR, BindParameterType.INTEGER };
					eleaseuserattrdtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
					eleaseuserattrdtl.setField("LESSEE_CODE", processData.get("LESSEE_CODE"));
					eleaseuserattrdtl.setField("AGREEMENT_NO", processData.get("AGREEMENT_NO"));
					eleaseuserattrdtl.setField("SCHEDULE_ID", processData.get("SCHEDULE_ID"));
					eleaseuserattrdtl.setField("FIELD_SL", processData.get("FIELD_SL"));
					eleaseuserattrdtl.deleteByFieldsAudit(eleaseuserattrFields, eleaseuserattrFieldsTypes);
					DTDObject detailData = processData.getDTDObject("eleaseuserattrdtl");
					eleaseuserattrdtl.setNew(true);
					int count = 1;
					for (int i = 0; i < detailData.getRowCount(); ++i) {
						eleaseuserattrdtl.setField("FIELD_SL", count);
						eleaseuserattrdtl.setField("FIELD_ID", detailData.getValue(i, 1));
						eleaseuserattrdtl.setField("FIELD_VALUE_TYPE", detailData.getValue(i, 4));
						if (detailData.getValue(i, 4).equals("T")) {
							eleaseuserattrdtl.setField("FIELD_TEXT", detailData.getValue(i, 3));
							eleaseuserattrdtl.setField("FIELD_NUMERIC", RegularConstants.NULL);
						} else if (!detailData.getValue(i, 4).equals("T")) {
							eleaseuserattrdtl.setField("FIELD_TEXT", RegularConstants.NULL);
							eleaseuserattrdtl.setField("FIELD_NUMERIC", detailData.getValue(i, 3));
						}
						if (!eleaseuserattrdtl.save()) {
							processResult.setProcessStatus(TBAProcessStatus.FAILURE);
							processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
							return;
						}
						count++;
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			eleaseuserattr.close();
			eleaseuserattrdtl.close();
		}
	}

}
