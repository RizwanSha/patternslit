package patterns.config.framework.bo.lss;

import java.util.ArrayList;
import java.util.List;

import patterns.config.framework.bo.AdditionalDetailInfo;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.bo.MessageParam;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAActionType;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;

public class eleaseclosureBO extends GenericBO {

	TBADynaSQL eleaseclosure = null;

	public void init() {
		long eleaseSl = 0;
		eleaseclosure = new TBADynaSQL("LEASECLOSURE", true);
		if (tbaContext.getProcessAction().getActionType().equals(TBAActionType.ADD) && !tbaContext.getProcessAction().isRectification()) {
			DTObject input = new DTObject();
			input.set("SOURCE_KEY", processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + BackOfficeFormatUtils.getDate(processData.getDate("ENTRY_DATE"), BackOfficeConstants.TBA_DATE_FORMAT));
			input.set("PGM_ID", tbaContext.getProcessID());
			eleaseSl = eleaseclosure.getTBAReferenceSerial(input);
			processData.set("ENTRY_SL", Long.toString(eleaseSl));
		}
		primaryKey = processData.get("ENTITY_CODE")+ RegularConstants.PK_SEPARATOR + BackOfficeFormatUtils.getDate(processData.getDate("ENTRY_DATE"), BackOfficeConstants.TBA_DATE_FORMAT) + RegularConstants.PK_SEPARATOR + processData.get("ENTRY_SL");
		eleaseclosure.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		eleaseclosure.setField("ENTRY_DATE", processData.getDate("ENTRY_DATE"));
		eleaseclosure.setField("ENTRY_SL", processData.get("ENTRY_SL"));
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("ENTRY_SL"));
		dataExists = eleaseclosure.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (tbaContext.getProcessAction().isRectification()) {
				eleaseclosure.setNew(false);
				if (!isDataExists()) {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
				}
			} else {
				eleaseclosure.setNew(true);
				if (isDataExists()) {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
				}
			}
				eleaseclosure.setField("LESSEE_CODE", processData.get("LESSEE_CODE"));
				eleaseclosure.setField("AGREEMENT_NO", processData.get("AGREEMENT_NO"));
				eleaseclosure.setField("SCHEDULE_ID", processData.get("SCHEDULE_ID"));
				eleaseclosure.setField("CLOSURE_ON", processData.getObject("CLOSURE_ON"));
				eleaseclosure.setField("CLOSURE_BY", processData.get("CLOSURE_BY"));
				eleaseclosure.setField("REFERENCE_NO", processData.get("REFERENCE_NO"));
				eleaseclosure.setField("REMARKS", processData.get("REMARKS"));
				if (eleaseclosure.save()) {
					if (!tbaContext.getProcessAction().isRectification()) {
						AdditionalDetailInfo additionalInfo = new AdditionalDetailInfo();
						additionalInfo.setHeading(new MessageParam(BackOfficeErrorCodes.ELEASECLOSURE_GEN_RESULT));  
						List<MessageParam> key = new ArrayList<MessageParam>();
						key.add(new MessageParam(BackOfficeErrorCodes.ELEASECLOSURE_LOT));
						additionalInfo.setKey(key);
						List<String> value = new ArrayList<String>();
						value.add(processData.get("ENTRY_SL"));
						additionalInfo.setValue(value);
						processResult.setAdditionalInfo(additionalInfo);
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			eleaseclosure.close();
		}
	}

	@Override
	protected void modify() throws TBAFrameworkException {
		// TODO Auto-generated method stub
		
	}
}
