package patterns.config.framework.bo.lss;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import patterns.config.framework.bo.AdditionalDetailInfo;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.bo.MessageParam;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.BindParameterType;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAActionType;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTDObject;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;

public class eleasepoinvBO extends GenericBO {
	long invoiceSerial = 0;
	TBADynaSQL leasepoinv = null;
	TBADynaSQL leasepoinvdtl = null;
	TBADynaSQL leasepoinvassetdtl = null;

	public eleasepoinvBO() {
	}

	public void init() {
		leasepoinv = new TBADynaSQL("LEASEPOINV", true);
		leasepoinvdtl = new TBADynaSQL("LEASEPOINVDTL", false);
		leasepoinvassetdtl = new TBADynaSQL("LEASEPOINVASSETS", false);
		if (tbaContext.getProcessAction().getActionType().equals(TBAActionType.ADD) && !tbaContext.getProcessAction().isRectification()) {
			DTObject input = new DTObject();
			input.set("SOURCE_KEY", processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + BackOfficeFormatUtils.getDate((Date) processData.getDate("CONTRACT_DATE"), BackOfficeConstants.TBA_DATE_FORMAT) + RegularConstants.PK_SEPARATOR + processData.get("CONTRACT_SL") + RegularConstants.PK_SEPARATOR + processData.get("PO_SL"));
			input.set("PGM_ID", tbaContext.getProcessID());
			invoiceSerial = leasepoinv.getTBAReferenceSerial(input);
			processData.set("INVOICE_SL", Long.toString(invoiceSerial));
		}
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + BackOfficeFormatUtils.getDate((Date) processData.getDate("CONTRACT_DATE"), BackOfficeConstants.TBA_DATE_FORMAT) + RegularConstants.PK_SEPARATOR + processData.get("CONTRACT_SL") + RegularConstants.PK_SEPARATOR + processData.get("PO_SL") + RegularConstants.PK_SEPARATOR + processData.get("INVOICE_SL");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("LESSEE_CODE"));
		leasepoinv.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		leasepoinv.setField("CONTRACT_DATE", processData.getDate("CONTRACT_DATE"));
		leasepoinv.setField("CONTRACT_SL", processData.get("CONTRACT_SL"));
		leasepoinv.setField("PO_SL", processData.get("PO_SL"));
		leasepoinv.setField("INVOICE_SL", processData.get("INVOICE_SL"));
		dataExists = leasepoinv.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (tbaContext.getProcessAction().isRectification()) {
				leasepoinv.setNew(false);
				if (!isDataExists()) {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
				}
			} else {
				leasepoinv.setNew(true);
				if (isDataExists()) {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
				}
			}
			leasepoinv.setField("INV_FOR", processData.get("INV_FOR"));
			leasepoinv.setField("INVOICE_DATE", processData.getDate("INVOICE_DATE"));
			leasepoinv.setField("INVOICE_NUMBER", processData.get("INVOICE_NUMBER"));
			leasepoinv.setField("INVOICE_CCY", processData.get("INVOICE_CCY"));
			leasepoinv.setField("INVOICE_AMOUNT", processData.get("INVOICE_AMOUNT"));
			leasepoinv.setField("INVOICE_AMOUNT_PAID_SOFAR", processData.get("INVOICE_AMOUNT_PAID_SOFAR"));
			leasepoinv.setField("REMARKS", processData.get("REMARKS"));
			if (leasepoinv.save()) {
				if (processData.get("INV_FOR").equals("P")) {
					leasepoinvdtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
					leasepoinvdtl.setField("CONTRACT_DATE", processData.getDate("CONTRACT_DATE"));
					leasepoinvdtl.setField("CONTRACT_SL", processData.get("CONTRACT_SL"));
					leasepoinvdtl.setField("PO_SL", processData.get("PO_SL"));
					leasepoinvdtl.setField("INVOICE_SL", processData.get("INVOICE_SL"));

					if (tbaContext.getProcessAction().isRectification()) {
						String[] leasepoinvdtlFields = { "ENTITY_CODE", "CONTRACT_DATE", "CONTRACT_SL", "PO_SL", "INVOICE_SL", "DTL_SL" };
						BindParameterType[] leasepoinvdtlFieldTypes = { BindParameterType.VARCHAR, BindParameterType.DATE, BindParameterType.INTEGER, BindParameterType.INTEGER, BindParameterType.INTEGER, BindParameterType.INTEGER };
						leasepoinvdtl.deleteByFieldsAudit(leasepoinvdtlFields, leasepoinvdtlFieldTypes);
					}
					DTDObject detailData = processData.getDTDObject("ELEASEPOINVDTL2");
					leasepoinvdtl.setNew(true);
					for (int i = 0; i < detailData.getRowCount(); ++i) {
						leasepoinvdtl.setField("DTL_SL", Integer.valueOf(i + 1).toString());
						leasepoinvdtl.setField("PAY_SL", detailData.getValue(i, 1));
						if (!leasepoinvdtl.save()) {
							processResult.setProcessStatus(TBAProcessStatus.FAILURE);
							processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
							return;
						}
					}
				}
				leasepoinvassetdtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
				leasepoinvassetdtl.setField("CONTRACT_DATE", processData.getDate("CONTRACT_DATE"));
				leasepoinvassetdtl.setField("CONTRACT_SL", processData.get("CONTRACT_SL"));
				leasepoinvassetdtl.setField("PO_SL", processData.get("PO_SL"));
				leasepoinvassetdtl.setField("INVOICE_SL", processData.get("INVOICE_SL"));
				if (tbaContext.getProcessAction().isRectification()) {
					String[] leasepoinvassetFields = { "ENTITY_CODE", "CONTRACT_DATE", "CONTRACT_SL", "PO_SL", "INVOICE_SL", "DTL_SL" };
					BindParameterType[] leasepoinvassetTypes = { BindParameterType.BIGINT, BindParameterType.DATE, BindParameterType.INTEGER, BindParameterType.INTEGER, BindParameterType.INTEGER, BindParameterType.INTEGER };
					leasepoinvassetdtl.deleteByFieldsAudit(leasepoinvassetFields, leasepoinvassetTypes);
				}
				DTDObject detailData1 = processData.getDTDObject("LEASEPOINVASSETS");
				leasepoinvassetdtl.setNew(true);
				for (int i = 0; i < detailData1.getRowCount(); ++i) {
					leasepoinvassetdtl.setField("DTL_SL", Integer.valueOf(i + 1).toString());
					leasepoinvassetdtl.setField("ASSET_TYPE_CODE", detailData1.getValue(i, "ASSET_TYPE_CODE"));
					leasepoinvassetdtl.setField("QUANTITY", detailData1.getValue(i, "QUANTITY"));
					leasepoinvassetdtl.setField("ASSET_DESCRIPTION", detailData1.getValue(i, "ASSET_DESCRIPTION"));
					leasepoinvassetdtl.setField("ASSET_MODEL", detailData1.getValue(i, "ASSET_MODEL"));
					leasepoinvassetdtl.setField("ASSET_CONFIG", detailData1.getValue(i, "ASSET_CONFIG"));
					leasepoinvassetdtl.setField("ASSET_VALUE", detailData1.getValue(i, "VALUE"));
					leasepoinvassetdtl.setField("ASSET_CST", RegularConstants.ZERO);
					leasepoinvassetdtl.setField("ASSET_VAT", RegularConstants.ZERO);
					leasepoinvassetdtl.setField("ASSET_ST", RegularConstants.ZERO);
					leasepoinvassetdtl.setField("ASSET_TCS", RegularConstants.ZERO);
					leasepoinvassetdtl.setField("ASSET_VAT_ON_AMOUNT", RegularConstants.ZERO);
					if (!leasepoinvassetdtl.save()) {
						processResult.setProcessStatus(TBAProcessStatus.FAILURE);
						processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
						return;
					}
				}
				if (!tbaContext.getProcessAction().isRectification()) {
					AdditionalDetailInfo additionalInfo = new AdditionalDetailInfo();
					additionalInfo.setHeading(new MessageParam(BackOfficeErrorCodes.INV_RESULT));
					List<MessageParam> key = new ArrayList<MessageParam>();
					key.add(new MessageParam(BackOfficeErrorCodes.INV_SERIAL));
					additionalInfo.setKey(key);
					List<String> value = new ArrayList<String>();
					value.add(processData.get("INVOICE_SL"));
					additionalInfo.setValue(value);
					processResult.setAdditionalInfo(additionalInfo);
				}
				processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			}

		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			leasepoinv.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		// TODO Auto-generated method stub

	}
}