package patterns.config.framework.bo.lss;



import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.MessageParam;
import patterns.config.framework.bo.ServiceBO;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessResult;
import patterns.config.framework.process.TBAProcessStatus;

public class eintdebitledBO extends ServiceBO {

	

	public eintdebitledBO() {
	}

	public void init() {
		
	}

	@Override
	public TBAProcessResult processRequest() {
		try{
			  acquireLock();
			  interestGenProcess();
		}catch(Exception e){
			e.getLocalizedMessage();
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.PROCESS_NOT_SUCCESS);
			processResult.setAdditionalInfo(e.getLocalizedMessage());
		}
		return processResult;
	}
	
	private void interestGenProcess() throws TBAFrameworkException {
		DBUtil dbutil = getDbContext().createUtilInstance();
		try {
			dbutil.reset();
			dbutil.setMode(DBUtil.CALLABLE);
			dbutil.setSql("{CALL SP_PLC_GEN_INTEREST_LED(?,?,?,?,?,?,?,?,?,?)}");
			dbutil.setLong(1, Long.parseLong(processData.get("ENTITY_CODE")));
			dbutil.setInt(2, Integer.parseInt(processData.get("INT_MONTH")));
			dbutil.setInt(3, Integer.parseInt(processData.get("INT_YEAR")));
			dbutil.setString(4,processData.get("PRODUCT_CODE"));
			dbutil.setString(5,processData.get("LESSEE_CODE"));
			dbutil.setString(6,processData.get("ENTRY_MODE"));
			dbutil.setString(7,processData.get("USER_ID"));
			dbutil.registerOutParameter(8, Types.BIGINT);
			dbutil.registerOutParameter(9, Types.BIGINT);
			dbutil.registerOutParameter(10, Types.VARCHAR);
			dbutil.execute();
			
			if (dbutil.getString(10).equals(RegularConstants.SP_SUCCESS)){
				processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
				processResult.setErrorCode(BackOfficeErrorCodes.PROCESS_SUCCESS);
				processResult.setGeneratedID(dbutil.getString(8));
				
				MessageParam messageParam = new MessageParam();
				messageParam.setCode(BackOfficeErrorCodes.INV_INTEREST_LEDGER_GEN_MESSGAE);
				List<String> parameters =new ArrayList<String>();
				parameters.add(dbutil.getString(9));//Record Count
				parameters.add(dbutil.getString(8));//Temp Serial
				messageParam.setParameters(parameters);
				processResult.setAdditionalInfo(messageParam);
			}else{
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.PROCESS_NOT_SUCCESS);
				
			}	

		} catch (Exception e) {
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			dbutil.reset();
		}
	}
	
	
	private void acquireLock() throws TBAFrameworkException {
		DBUtil dbutil = getDbContext().createUtilInstance();
		try {
			dbutil.reset();
			dbutil.setMode(DBUtil.CALLABLE);
			dbutil.setSql("{CALL SP_ACQUIRE_LOCK( ?,?,?,?,?,?)}");
			dbutil.setString(1, processData.get("ENTITY_CODE"));
			dbutil.setString(2, processData.get("PROCESS_ID"));
			dbutil.registerOutParameter(3, Types.INTEGER);
			dbutil.registerOutParameter(4, Types.INTEGER);
			dbutil.registerOutParameter(5, Types.INTEGER);
			dbutil.registerOutParameter(6, Types.VARCHAR);
			dbutil.execute();
			//int V_LOG_REQ = dbutil.getInt(3);
			//int V_ADD_LOG_REQ = dbutil.getInt(4);
			int V_LOCK_STATUS = dbutil.getInt(5);
			String V_LOCK_ERR_MSG = dbutil.getString(6) == RegularConstants.NULL ? RegularConstants.EMPTY_STRING : dbutil.getString(6);
			if (V_LOCK_STATUS == 1)
				throw new TBAFrameworkException(V_LOCK_ERR_MSG);

		} catch (Exception e) {
			e.printStackTrace();
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			dbutil.reset();
		}
	}
}
