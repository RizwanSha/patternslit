package patterns.config.framework.bo.lss;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.BindParameterType;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTDObject;
import patterns.config.framework.web.BackOfficeFormatUtils;

public class istaffportfolioBO extends GenericBO {
	TBADynaSQL istaffportfolio = null;
	TBADynaSQL istaffportfolioHist = null;
	TBADynaSQL istaffportfolioHistDtl = null;

	public istaffportfolioBO() {
	}

	public void init() {
		istaffportfolio = new TBADynaSQL("STAFFPORTFOLIOHIST", true);
		istaffportfolioHist = new TBADynaSQL("STAFFPFROLESHIST", false);
		istaffportfolioHistDtl = new TBADynaSQL("STAFFPFROLECONFHIST", false);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("BRANCH_CODE") + RegularConstants.PK_SEPARATOR + BackOfficeFormatUtils.getDate(processData.getDate("EFF_DATE"), BackOfficeConstants.TBA_DATE_FORMAT);
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("BRANCH_CODE"));
		istaffportfolio.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		istaffportfolio.setField("BRANCH_CODE", processData.get("BRANCH_CODE"));
		istaffportfolio.setField("EFF_DATE", processData.getDate("EFF_DATE"));
		dataExists = istaffportfolio.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				istaffportfolio.setNew(true);
				istaffportfolio.setField("ENABLED", processData.get("ENABLED"));
				istaffportfolio.setField("REMARKS", processData.get("REMARKS"));
				if (istaffportfolio.save()) {
					istaffportfolioHist.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
					istaffportfolioHist.setField("BRANCH_CODE", processData.get("BRANCH_CODE"));
					istaffportfolioHist.setField("EFF_DATE", processData.getDate("EFF_DATE"));
					DTDObject staffPfHistObject = processData.getDTDObject("ROLEHIERARCHYDTL");
					istaffportfolioHist.setNew(true);
					for (int i = 0; i < staffPfHistObject.getRowCount(); ++i) {
						istaffportfolioHist.setField("STAFF_ROLE_CODE", staffPfHistObject.getValue(i, 1));
						istaffportfolioHist.setField("NOF_STAFF", staffPfHistObject.getValue(i, 2));
						if (!istaffportfolioHist.save()) {
							processResult.setProcessStatus(TBAProcessStatus.FAILURE);
							processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
						}
					}
					istaffportfolioHistDtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
					istaffportfolioHistDtl.setField("BRANCH_CODE", processData.get("BRANCH_CODE"));
					istaffportfolioHistDtl.setField("EFF_DATE", processData.getDate("EFF_DATE"));
					DTDObject staffPfConfHistObject = processData.getDTDObject("STAFFDETAIL");
					istaffportfolioHistDtl.setNew(true);
					for (int j = 0; j < staffPfConfHistObject.getRowSize(); j++) {
						istaffportfolioHistDtl.setField("STAFF_ROLE_CODE", staffPfConfHistObject.getValue(j, 1));
						istaffportfolioHistDtl.setField("DTL_SL", staffPfConfHistObject.getValue(j, 2));
						istaffportfolioHistDtl.setField("STAFF_CODE", staffPfConfHistObject.getValue(j, 3));
						if (!RegularConstants.EMPTY_STRING.equals(staffPfConfHistObject.getValue(j, 4)))
							istaffportfolioHistDtl.setField("PORTFOLIO_CODE", staffPfConfHistObject.getValue(j, 4));
						if (!istaffportfolioHistDtl.save()) {
							processResult.setProcessStatus(TBAProcessStatus.FAILURE);
							processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
						}
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			istaffportfolio.close();
			istaffportfolioHist.close();
			istaffportfolioHistDtl.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				istaffportfolio.setNew(false);
				istaffportfolio.setField("ENABLED", processData.get("ENABLED"));
				istaffportfolio.setField("REMARKS", processData.get("REMARKS"));
				if (istaffportfolio.save()) {
					String[] istaffportfolioHistFields = { "ENTITY_CODE", "BRANCH_CODE", "EFF_DATE", "STAFF_ROLE_CODE" };
					BindParameterType[] istaffportfolioHistFieldTypes = { BindParameterType.BIGINT, BindParameterType.VARCHAR, BindParameterType.DATE, BindParameterType.VARCHAR };
					istaffportfolioHist.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
					istaffportfolioHist.setField("BRANCH_CODE", processData.get("BRANCH_CODE"));
					istaffportfolioHist.setField("EFF_DATE", processData.getDate("EFF_DATE"));
					istaffportfolioHist.deleteByFieldsAudit(istaffportfolioHistFields, istaffportfolioHistFieldTypes);
					DTDObject staffRoleObject = processData.getDTDObject("ROLEHIERARCHYDTL");
					istaffportfolioHist.setNew(true);
					for (int i = 0; i < staffRoleObject.getRowCount(); ++i) {
						istaffportfolioHist.setField("STAFF_ROLE_CODE", staffRoleObject.getValue(i, 1));
						istaffportfolioHist.setField("NOF_STAFF", staffRoleObject.getValue(i, 2));
						if (!istaffportfolioHist.save()) {
							processResult.setProcessStatus(TBAProcessStatus.FAILURE);
							processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
							return;
						}
					}
					String[] istaffportfolioHistDtlFields = { "ENTITY_CODE", "BRANCH_CODE", "EFF_DATE", "STAFF_ROLE_CODE" };
					BindParameterType[] istaffportfolioHistDtlFieldTypes = { BindParameterType.BIGINT, BindParameterType.VARCHAR, BindParameterType.DATE, BindParameterType.VARCHAR };
					istaffportfolioHistDtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
					istaffportfolioHistDtl.setField("BRANCH_CODE", processData.get("BRANCH_CODE"));
					istaffportfolioHistDtl.setField("EFF_DATE", processData.getDate("EFF_DATE"));
					istaffportfolioHistDtl.deleteByFieldsAudit(istaffportfolioHistDtlFields, istaffportfolioHistDtlFieldTypes);
					DTDObject staffDetailObject = processData.getDTDObject("STAFFDETAIL");
					istaffportfolioHistDtl.setNew(true);
					for (int i = 0; i < staffDetailObject.getRowCount(); ++i) {
						istaffportfolioHistDtl.setField("STAFF_ROLE_CODE", staffDetailObject.getValue(i, 1));
						istaffportfolioHistDtl.setField("DTL_SL", staffDetailObject.getValue(i, 2));
						istaffportfolioHistDtl.setField("STAFF_CODE", staffDetailObject.getValue(i, 3));
						istaffportfolioHistDtl.setField("PORTFOLIO_CODE", staffDetailObject.getValue(i, 4));
						if (!istaffportfolioHistDtl.save()) {
							processResult.setProcessStatus(TBAProcessStatus.FAILURE);
							processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
							return;
						}
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			istaffportfolio.close();
			istaffportfolioHist.close();
			istaffportfolioHistDtl.close();
		}
	}
}
