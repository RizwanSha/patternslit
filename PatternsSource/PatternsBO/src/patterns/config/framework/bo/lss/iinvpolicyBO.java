package patterns.config.framework.bo.lss;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.BindParameterType;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTDObject;
import patterns.config.framework.web.BackOfficeFormatUtils;

public class iinvpolicyBO extends GenericBO {

	TBADynaSQL invpolicyhist = null;
	TBADynaSQL invpolicyhistdtl = null;

	public iinvpolicyBO() {
	}

	public void init() {
		invpolicyhist = new TBADynaSQL("INVPOLICYHIST", true);
		invpolicyhistdtl = new TBADynaSQL("INVPOLICYHISTDTL", false);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("CCY_CODE") + RegularConstants.PK_SEPARATOR + BackOfficeFormatUtils.getDate(processData.getDate("EFF_DATE"), BackOfficeConstants.TBA_DATE_FORMAT);
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("CCY_CODE"));
		invpolicyhist.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		invpolicyhist.setField("CCY_CODE", processData.get("CCY_CODE"));
		invpolicyhist.setField("EFF_DATE", processData.getDate("EFF_DATE"));
		dataExists = invpolicyhist.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				invpolicyhist.setNew(true);
				invpolicyhist.setField("ENABLED", processData.get("ENABLED"));
				invpolicyhist.setField("REMARKS", processData.get("REMARKS"));
				if (invpolicyhist.save()) {
					invpolicyhistdtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
					invpolicyhistdtl.setField("CCY_CODE", processData.get("CCY_CODE"));
					invpolicyhistdtl.setField("EFF_DATE", processData.getDate("EFF_DATE"));
					DTDObject detailData = processData.getDTDObject("INVPOLICYHISTDTL");
					invpolicyhistdtl.setNew(true);
					for (int i = 0; i < detailData.getRowCount(); i++) {
						invpolicyhistdtl.setField("DTL_SL", Integer.valueOf(i + 1));
						invpolicyhistdtl.setField("UPTO_INV_AMOUNT", detailData.getValue(i, 6));
						invpolicyhistdtl.setField("NOF_SIGNS", detailData.getValue(i, 4));
						invpolicyhistdtl.setField("SIGN_TYPE", detailData.getValue(i, 5));
						if (!invpolicyhistdtl.save()) {
							processResult.setProcessStatus(TBAProcessStatus.FAILURE);
							processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
						}
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			invpolicyhist.close();
			invpolicyhistdtl.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				invpolicyhist.setNew(false);
				invpolicyhist.setField("COMPONENT_VIEW_CHOICE", processData.get("COMPONENT_VIEW_CHOICE"));
				invpolicyhist.setField("ENABLED", processData.get("ENABLED"));
				invpolicyhist.setField("REMARKS", processData.get("REMARKS"));
				if (invpolicyhist.save()) {
					String[] invpolicyhistdtlFields = { "ENTITY_CODE", "CCY_CODE", "EFF_DATE", "DTL_SL" };
					BindParameterType[] invpolicydtlFieldTypes = { BindParameterType.BIGINT, BindParameterType.VARCHAR, BindParameterType.DATE, BindParameterType.INTEGER };
					invpolicyhistdtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
					invpolicyhistdtl.setField("CCY_CODE", processData.get("CCY_CODE"));
					invpolicyhistdtl.setField("EFF_DATE", processData.getDate("EFF_DATE"));
					invpolicyhistdtl.deleteByFieldsAudit(invpolicyhistdtlFields, invpolicydtlFieldTypes);
					DTDObject detailData = processData.getDTDObject("INVPOLICYHISTDTL");
					invpolicyhistdtl.setNew(true);
					for (int i = 0; i < detailData.getRowCount(); ++i) {
						invpolicyhistdtl.setField("DTL_SL", Integer.valueOf(i + 1));
						invpolicyhistdtl.setField("UPTO_INV_AMOUNT", detailData.getValue(i, 6));
						invpolicyhistdtl.setField("NOF_SIGNS", detailData.getValue(i, 4));
						invpolicyhistdtl.setField("SIGN_TYPE", detailData.getValue(i, 5));
						if (!invpolicyhistdtl.save()) {
							processResult.setProcessStatus(TBAProcessStatus.FAILURE);
							processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
							return;
						}
					}

					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}

		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			invpolicyhist.close();
			invpolicyhistdtl.close();
		}
	}
}
