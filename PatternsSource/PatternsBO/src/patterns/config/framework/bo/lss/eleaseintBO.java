/*
 *The program used for Lease Purchase Order (PO) Details Entry

 Author : ARULPRASATH A
 Created Date : 21-FEB-2017
 Spec Reference : PPBS/LSS-ELEASEPO
 Modification History
 ----------------------------------------------------------------------
 Sl.No		Modified Date	Author			Modified Changes	Version
 ----------------------------------------------------------------------

 */

package patterns.config.framework.bo.lss;

import java.util.ArrayList;
import java.util.List;

import patterns.config.framework.bo.AdditionalDetailInfo;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.bo.MessageParam;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.BindParameterType;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAActionType;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTDObject;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.framework.web.ajax.ContentManager;

public class eleaseintBO extends GenericBO {

	TBADynaSQL eleaseint = null;

	public eleaseintBO() {
	}

	@Override
	public void init() {
		long entrySerial = 0;
		eleaseint = new TBADynaSQL("LEASEINT", true);
		if (tbaContext.getProcessAction().getActionType().equals(TBAActionType.ADD) && !tbaContext.getProcessAction().isRectification()) {
			DTObject input = new DTObject();
			input.set("SOURCE_KEY", processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("LESSEE_CODE") + RegularConstants.PK_SEPARATOR + processData.get("AGREEMENT_NO") + RegularConstants.PK_SEPARATOR + processData.get("SCHEDULE_ID"));
			input.set("PGM_ID", tbaContext.getProcessID());
			entrySerial = eleaseint.getTBAReferenceSerial(input);
			processData.set("ENTRY_SL", Long.toString(entrySerial));
		}
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("LESSEE_CODE") + RegularConstants.PK_SEPARATOR + processData.get("AGREEMENT_NO") + RegularConstants.PK_SEPARATOR + processData.get("SCHEDULE_ID") + RegularConstants.PK_SEPARATOR + processData.get("ENTRY_SL");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("LESSEE_CODE"));
		eleaseint.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		eleaseint.setField("LESSEE_CODE", processData.get("LESSEE_CODE"));
		eleaseint.setField("AGREEMENT_NO", processData.get("AGREEMENT_NO"));
		eleaseint.setField("SCHEDULE_ID", processData.get("SCHEDULE_ID"));
		eleaseint.setField("ENTRY_SL", processData.get("ENTRY_SL"));
		dataExists = eleaseint.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		// TODO Auto-generated method stub
		try {
			if (tbaContext.getProcessAction().isRectification()) {
				eleaseint.setNew(false);
				if (!isDataExists()) {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
				}
			} else {
				eleaseint.setNew(true);
				if (isDataExists()) {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
				}
			}
			eleaseint.setField("DATE_OF_ENTRY", processData.getDate("DATE_OF_ENTRY"));
			eleaseint.setField("INT_FROM_DATE", processData.getDate("INT_FROM_DATE"));
			eleaseint.setField("INT_UPTO_DATE", processData.getDate("INT_UPTO_DATE"));
			eleaseint.setField("INT_FOR_DAYS", processData.get("INT_FOR_DAYS"));
			eleaseint.setField("APPLIED_INT_RATE", processData.get("APPLIED_INT_RATE"));
			eleaseint.setField("INT_ON_AMOUNT_CCY", processData.get("INT_ON_AMOUNT_CCY"));
			eleaseint.setField("INT_ON_AMOUNT", processData.get("INT_ON_AMOUNT"));
			eleaseint.setField("INV_AMOUNT_CCY", processData.get("INV_AMOUNT_CCY"));
			eleaseint.setField("INV_AMOUNT", processData.get("INV_AMOUNT"));
			eleaseint.setField("INV_DATE", processData.getDate("INV_DATE"));
			eleaseint.setField("FINANCE_SCHED_DTL_SL", processData.get("FINANCE_SCHED_DTL_SL"));
			eleaseint.setField("REMARKS", processData.get("REMARKS"));
			if (eleaseint.save()) {
				if (!tbaContext.getProcessAction().isRectification()) {
					AdditionalDetailInfo additionalInfo = new AdditionalDetailInfo();
					additionalInfo.setHeading(new MessageParam(BackOfficeErrorCodes.ELEASE_GEN_RESULT));
					List<MessageParam> key = new ArrayList<MessageParam>();
					key.add(new MessageParam(BackOfficeErrorCodes.ENTRY_SERIAL));
					additionalInfo.setKey(key);
					List<String> value = new ArrayList<String>();
					value.add(processData.get("ENTRY_SL"));
					additionalInfo.setValue(value);
					processResult.setAdditionalInfo(additionalInfo);
				}
				processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			eleaseint.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
	}
}
