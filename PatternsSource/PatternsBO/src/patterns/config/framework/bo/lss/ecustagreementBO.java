/*


 *The program used for Customer Agreement Maintenance

 Author : ARULPRASATH A
 Created Date : 13-FEB-2017
 Spec Reference : PPBS/LSS-ECUSTAGREEMENT
 Modification History
 ----------------------------------------------------------------------
 Sl.No		Modified Date	Author			Modified Changes	Version
 ----------------------------------------------------------------------

 */

package patterns.config.framework.bo.lss;

import java.util.ArrayList;
import java.util.List;

import patterns.config.framework.bo.AdditionalDetailInfo;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.bo.MessageParam;
import patterns.config.framework.database.BindParameterType;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTDObject;

public class ecustagreementBO extends GenericBO {
	TBADynaSQL ecustagreement = null;
	TBADynaSQL ecustagreementdtl = null;
	int agreementSerial = 0;

	public void init() {
		ecustagreement = new TBADynaSQL("CUSTAGREEMENT", true);
		ecustagreementdtl = new TBADynaSQL("CUSTAGREEMENTPROD", false);
		if (processData.containsKey("AGREEMENT_NO"))
			agreementSerial = Integer.parseInt(processData.get("AGREEMENT_NO"));
		else
			agreementSerial = (int) ecustagreement.getTBAReferenceKey(processData);
		processData.set("AGREEMENT_NO", Long.toString(agreementSerial));
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("CUSTOMER_ID")
				+ RegularConstants.PK_SEPARATOR + agreementSerial;
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("CUSTOMER_ID"));
		ecustagreement.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		ecustagreement.setField("CUSTOMER_ID", processData.get("CUSTOMER_ID"));
		ecustagreement.setField("AGREEMENT_NO", String.valueOf(agreementSerial));
		dataExists = ecustagreement.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				ecustagreement.setNew(true);
				ecustagreement.setField("AGREEMENT_REF_NO", processData.get("AGREEMENT_REF_NO"));
				ecustagreement.setField("AGREEMENT_DATE", processData.getDate("AGREEMENT_DATE"));
				ecustagreement.setField("SANCTION_REF_NO", processData.get("SANCTION_REF_NO"));
				ecustagreement.setField("SANCTION_CCY", processData.get("SANCTION_CCY"));
				ecustagreement.setField("SANCTION_AMOUNT", processData.get("SANCTION_AMOUNT"));
				ecustagreement.setField("ENABLED", RegularConstants.ONE);
				ecustagreement.setField("REMARKS", processData.get("REMARKS"));
				ecustagreement.setNew(true);
				if (ecustagreement.save()) {
					if (!ecustagreement.udateTBADuplicateCheck()) {
						processResult.setProcessStatus(TBAProcessStatus.FAILURE);
						processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
						throw new TBAFrameworkException();
					}
					ecustagreementdtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
					ecustagreementdtl.setField("CUSTOMER_ID", processData.get("CUSTOMER_ID"));
					ecustagreementdtl.setField("AGREEMENT_NO", processData.get("AGREEMENT_NO"));
					DTDObject detailData = processData.getDTDObject("CUSTAGREEMENTPROD");
					ecustagreementdtl.setNew(true);
					for (int i = 0; i < detailData.getRowCount(); ++i) {
						ecustagreementdtl.setField("DTL_SL", String.valueOf(i + 1));
						ecustagreementdtl.setField("LEASE_PRODUCT_CODE", detailData.getValue(i, 2));
						if (!ecustagreementdtl.save()) {
							processResult.setProcessStatus(TBAProcessStatus.FAILURE);
							processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
						}
					}
					if (!tbaContext.getProcessAction().isRectification()) {
						AdditionalDetailInfo additionalInfo = new AdditionalDetailInfo();
						additionalInfo.setHeading(new MessageParam(BackOfficeErrorCodes.ECUST_GEN_RESULT));
						List<MessageParam> key = new ArrayList<MessageParam>();
						key.add(new MessageParam(BackOfficeErrorCodes.AGRMNT_SL));
						additionalInfo.setKey(key);
						List<String> value = new ArrayList<String>();
						value.add(processData.get("AGREEMENT_NO"));
						additionalInfo.setValue(value);
						processResult.setAdditionalInfo(additionalInfo);
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			ecustagreement.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				ecustagreement.setField("AGREEMENT_REF_NO", processData.get("AGREEMENT_REF_NO"));
				ecustagreement.setField("AGREEMENT_DATE", processData.getDate("AGREEMENT_DATE"));
				ecustagreement.setField("SANCTION_REF_NO", processData.get("SANCTION_REF_NO"));
				ecustagreement.setField("SANCTION_CCY", processData.get("SANCTION_CCY"));
				ecustagreement.setField("SANCTION_AMOUNT", processData.get("SANCTION_AMOUNT"));
				ecustagreement.setField("REMARKS", processData.get("REMARKS"));
				ecustagreement.setNew(false);
				if (ecustagreement.save()) {
					String[] ecustagreementdtlFields = { "ENTITY_CODE", "CUSTOMER_ID", "AGREEMENT_NO", "DTL_SL" };
					BindParameterType[] ecustagreementdtlFieldTypes = { BindParameterType.BIGINT,
							BindParameterType.BIGINT, BindParameterType.VARCHAR, BindParameterType.INTEGER };
					ecustagreementdtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
					ecustagreementdtl.setField("CUSTOMER_ID", processData.get("CUSTOMER_ID"));
					ecustagreementdtl.setField("AGREEMENT_NO", processData.get("AGREEMENT_NO"));
					ecustagreementdtl.deleteByFieldsAudit(ecustagreementdtlFields, ecustagreementdtlFieldTypes);
					DTDObject detailData = processData.getDTDObject("CUSTAGREEMENTPROD");
					ecustagreementdtl.setNew(true);
					for (int i = 0; i < detailData.getRowCount(); ++i) {
						ecustagreementdtl.setField("DTL_SL", String.valueOf(i + 1));
						ecustagreementdtl.setField("LEASE_PRODUCT_CODE", detailData.getValue(i, 2));
						if (!ecustagreementdtl.save()) {
							processResult.setProcessStatus(TBAProcessStatus.FAILURE);
							processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
							return;
						}
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			ecustagreement.close();
		}
	}
}
