package patterns.config.framework.bo.lss;

import java.util.ArrayList;
import java.util.List;

import patterns.config.framework.bo.AdditionalDetailInfo;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.bo.MessageParam;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAActionType;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;

public class epreleasecontregBO extends GenericBO {

	TBADynaSQL epreleasecontreg = null;

	public epreleasecontregBO() {
	}

	public void init() {
		long preLeaseSl = 0;
		epreleasecontreg = new TBADynaSQL("PRELEASECONTRACT", true);
		if (tbaContext.getProcessAction().getActionType().equals(TBAActionType.ADD) && !tbaContext.getProcessAction().isRectification()) {
			DTObject input = new DTObject();
			input.set("SOURCE_KEY", processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + BackOfficeFormatUtils.getDate(processData.getDate("CONTRACT_DATE"), BackOfficeConstants.TBA_DATE_FORMAT));
			input.set("PGM_ID", tbaContext.getProcessID());
			preLeaseSl = epreleasecontreg.getTBAReferenceSerial(input);
			processData.set("CONTRACT_SL", Long.toString(preLeaseSl));
		}
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + BackOfficeFormatUtils.getDate(processData.getDate("CONTRACT_DATE"), BackOfficeConstants.TBA_DATE_FORMAT) + RegularConstants.PK_SEPARATOR + processData.get("CONTRACT_SL");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("CONTRACT_SL"));
		epreleasecontreg.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		epreleasecontreg.setField("CONTRACT_DATE", processData.getDate("CONTRACT_DATE"));
		epreleasecontreg.setField("CONTRACT_SL", processData.get("CONTRACT_SL"));
		dataExists = epreleasecontreg.loadByKeyFields();
	}

	@Override
	protected void add() throws TBAFrameworkException {
		// TODO Auto-generated method stub
		try {
			if (tbaContext.getProcessAction().isRectification()) {
				epreleasecontreg.setNew(false);
				if (!isDataExists()) {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
				}
			} else {
				epreleasecontreg.setNew(true);
				if (isDataExists()) {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
				}
			}
			epreleasecontreg.setField("CUSTOMER_ID", processData.get("CUSTOMER_ID"));
			epreleasecontreg.setField("SUNDRY_DB_AC", processData.get("SUNDRY_DB_AC"));
			epreleasecontreg.setField("LEASE_PRODUCT_CODE", processData.get("LEASE_PRODUCT_CODE"));
			epreleasecontreg.setField("BRANCH_CODE", processData.get("BRANCH_CODE"));
			epreleasecontreg.setField("ADDR_SL", processData.get("ADDR_SL"));
			epreleasecontreg.setField("CONTACT_SL", processData.get("CONTACT_SL"));
			epreleasecontreg.setField("INTERIM_INT_APPL", processData.get("INTERIM_INT_APPL"));
			epreleasecontreg.setField("INT_RATE_FOR_PRIOR_INVEST", processData.get("INT_RATE_FOR_PRIOR_INVEST"));
			epreleasecontreg.setField("INTERIM_INT_BILLING", processData.get("INTERIM_INT_BILLING"));
			epreleasecontreg.setField("INT_DEBIT_NOTE_PRINT_CHOICE", processData.get("INT_DEBIT_NOTE_PRINT_CHOICE"));
			epreleasecontreg.setField("REMARKS", processData.get("REMARKS"));
			epreleasecontreg.setField("AGREEMENT_NO", processData.get("AGREEMENT_NO"));
			epreleasecontreg.setField("SCHEDULE_ID", processData.get("SCHEDULE_ID"));
			if (epreleasecontreg.save()) {
				if (!tbaContext.getProcessAction().isRectification()) {
					AdditionalDetailInfo additionalInfo = new AdditionalDetailInfo();
					additionalInfo.setHeading(new MessageParam(BackOfficeErrorCodes.ECUST_GEN_RESULT));
					List<MessageParam> key = new ArrayList<MessageParam>();
					key.add(new MessageParam(BackOfficeErrorCodes.PRE_LEASE_CONTRACTREF_SERIAL));
					additionalInfo.setKey(key);
					List<String> value = new ArrayList<String>();
					value.add(processData.get("CONTRACT_SL"));
					additionalInfo.setValue(value);
					processResult.setAdditionalInfo(additionalInfo);
				}
				processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			epreleasecontreg.close();
		}
	}

	protected void modify() throws TBAFrameworkException {

	}
}
