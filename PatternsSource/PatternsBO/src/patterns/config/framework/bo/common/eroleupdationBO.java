package patterns.config.framework.bo.common;

import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.process.TBAProcessResult;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.ServiceBO;

public class eroleupdationBO extends ServiceBO {

	public eroleupdationBO() {
	}

	public void init() {
	}

	public TBAProcessResult processRequest() {
		String sqlQuery = ContentManager.EMPTY_STRING;
		if (processData.get("ROLE_AVAILABLE").equals(RegularConstants.COLUMN_ENABLE)) {
			sqlQuery = "INSERT INTO ROLEUPDATION(ENTITY_CODE,USER_ID,CHANGE_DATETIME,ROLE_CODE)VALUES(?,?,FN_GETCDT(?),?)";
		} else {
			sqlQuery = "INSERT INTO ROLEUPDATIONFAIL(ENTITY_CODE,USER_ID,CHANGE_DATETIME,ROLE_CODE)VALUES(?,?,FN_GETCDT(?),SUBSTR(?,1,250))";
		}

		DBUtil util = getDbContext().createUtilInstance();
		try {
			util.reset();
			util.setMode(DBUtil.PREPARED);
			util.setSql(sqlQuery);
			util.setString(1, processData.get("ENTITY_CODE"));
			util.setString(2, processData.get("USER_ID"));
			util.setString(3, processData.get("ENTITY_CODE"));
			util.setString(4, processData.get("ROLE_CODE"));
			int updateCount = util.executeUpdate();
			if (updateCount < 1) {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			} else {
				processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(e.getLocalizedMessage());
		}

		return processResult;
	}
}