package patterns.config.framework.bo.common;

import patterns.config.framework.bo.InventoryBO;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.service.DTDObject;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.ContentManager;

public class ContactDB extends InventoryBO {
	private TBADynaSQL contactsDB = null;
	private TBADynaSQL contactsDBHist = null;

	private TBADynaSQL personContactDB = null;

	public ContactDB(DBContext context) {
		super.initialize(context);
	}

	public DTObject create(DTObject input) {
		contactsDB = new TBADynaSQL("CONTACTSDB", false);
		input.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		try {
			long invNumber = 0;
			invNumber = getInventoryNumber(RegularConstants.PHONE_INV_NUM);
			contactsDB.setNew(true);
			contactsDB.setField("ENTITY_CODE", input.get("ENTITY_CODE"));
			contactsDB.setField("CONTACT_INV_NO", String.valueOf(invNumber));
			contactsDB.setField("CONTACT_TYPE", input.get("CONTACT_TYPE"));
			contactsDB.setField("LL_COUNTRY_CODE", input.get("LL_COUNTRY_CODE"));
			contactsDB.setField("LL_LOC_STD_CODE", input.get("LL_LOC_STD_CODE"));
			contactsDB.setField("LL_CONTACT_NUMBER", input.get("LL_CONTACT_NUMBER"));
			contactsDB.setField("MOB_COUNTRY_CODE", input.get("MOB_COUNTRY_CODE"));
			contactsDB.setField("MOB_CONTACT_NUMBER", input.get("MOB_CONTACT_NUMBER"));
			contactsDB.setField("EMAIL_ID", input.get("EMAIL_ID"));
			contactsDB.setField("ENABLED", input.get("ENABLED"));
			contactsDB.setField("REMARKS", input.get("REMARKS"));

			if (contactsDB.save()) {
				input.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				input.set("CONTACT_INV_NO", String.valueOf(invNumber));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			contactsDB.close();
		}
		return input;
	}

	public boolean createAll(DTObject input) {
		personContactDB = new TBADynaSQL("PERSONCONTDB", false);
		try {
			personContactDB.setNew(true);
			personContactDB.setField("ENTITY_CODE", input.get("ENTITY_CODE"));
			personContactDB.setField("PERSON_ID", input.get("PERSON_ID"));
			DTDObject detailData = input.getDTDObject("CONTACTLIST");
			for (int i = 0; i < detailData.getRowCount(); ++i) {
				if (detailData.getValue(i, 0).equalsIgnoreCase(RegularConstants.COLUMN_ENABLE)) {

					personContactDB.setField("CONTACT_INV_NO", detailData.getValue(i, 2));
					personContactDB.setField("OFFICIAL_USE", detailData.getValue(i, 1));
					personContactDB.setField("ADDED_ON_DATE", personContactDB.getCurrentDate());
					personContactDB.setField("ENABLED", "1");
					
					personContactDB.save();
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			personContactDB.close();
		}
		return false;
	}

	@Override
	public DTObject moveToHistory(DTObject input) {
		contactsDBHist = new TBADynaSQL("CONTACTSDBHIST", false);
		input.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		try {
			long serial = 1;
			long invNumber = 0;
			String sourceKey = RegularConstants.EMPTY_STRING;
			invNumber = Long.parseLong(input.get("CONTACT_INV_NO"));
			sourceKey = String.valueOf(invNumber);

			input.set("SOURCE_KEY", sourceKey);
			input.set("PGM_ID", "CONTACTSDBHIST");
			serial = contactsDBHist.getTBAReferenceSerial(input);

			String query = "INSERT INTO CONTACTSDBHIST SELECT ENTITY_CODE,CONTACT_INV_NO,?,CONTACT_TYPE,LL_COUNTRY_CODE,LL_LOC_STD_CODE,LL_CONTACT_NUMBER,MOB_COUNTRY_CODE,MOB_CONTACT_NUMBER,EMAIL_ID,ENABLED,NULL,NULL FROM CONTACTSDB WHERE CONTACT_INV_NO=?";
			DBUtil dbUtil = getDbContext().createUtilInstance();
			dbUtil.setMode(DBUtil.PREPARED);
			dbUtil.setSql(query);
			dbUtil.setString(1, String.valueOf(serial));
			dbUtil.setString(2, String.valueOf(invNumber));
			int count = dbUtil.executeUpdate();
			dbUtil.reset();
			if (count == 1) {
				input.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			contactsDBHist.close();
		}
		return input;
	}

	@Override
	public DTObject update(DTObject input) {
		contactsDB = new TBADynaSQL("CONTACTSDB", false);
		input.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		try {
			contactsDB.setNew(false);
			contactsDB.setField("ENTITY_CODE", input.get("ENTITY_CODE"));
			contactsDB.setField("CONTACT_INV_NO", input.get("CONTACT_INV_NO"));
			contactsDB.setField("CONTACT_TYPE", input.get("CONTACT_TYPE"));
			contactsDB.setField("LL_COUNTRY_CODE", input.get("LL_COUNTRY_CODE"));
			contactsDB.setField("LL_LOC_STD_CODE", input.get("LL_LOC_STD_CODE"));
			contactsDB.setField("LL_CONTACT_NUMBER", input.get("LL_CONTACT_NUMBER"));
			contactsDB.setField("MOB_COUNTRY_CODE", input.get("MOB_COUNTRY_CODE"));
			contactsDB.setField("MOB_CONTACT_NUMBER", input.get("MOB_CONTACT_NUMBER"));
			contactsDB.setField("EMAIL_ID", input.get("EMAIL_ID"));
			contactsDB.setField("ENABLED", input.get("ENABLED"));
			contactsDB.setField("REMARKS", input.get("REMARKS"));
			if (contactsDB.save()) {
				input.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			contactsDB.close();
		}
		return input;
	}

	@Override
	public DTObject delete(DTObject input) {
		// TODO Auto-generated method stub
		return null;
	}
}
