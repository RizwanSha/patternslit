package patterns.config.framework.bo.common;

import java.sql.Types;

import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.process.TBAProcessResult;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.ServiceBO;

public class eforcepwdchgnBO extends ServiceBO {

	public eforcepwdchgnBO() {
	}

	public void init() {
	}

	public TBAProcessResult processRequest() {
		DBUtil util = getDbContext().createUtilInstance();
		try {

			util.reset();
			util.setMode(DBUtil.CALLABLE);
			util.setSql("{CALL SP_USER_PWD_UPDATE(?,?,?,?,?,?,?,?,?,?,?,?,?)}");
			util.setString(1, processData.get("P_PARTITION_NO"));
			util.setString(2, processData.get("P_ENTITY_CODE"));
			util.setString(3, processData.get("P_USER_ID"));
			util.setString(4, processData.get("P_LOGIN_NEW_PIN"));
			util.setString(5, processData.get("P_LOGIN_PIN_SALT"));
			util.setString(6, processData.get("P_AUTH_USER_ID"));
			util.setString(7, processData.get("P_REMARKS"));
			util.setString(8, processData.get("P_CHANGE_REQD"));
			util.setString(9, processData.get("P_DELIVERY_REQD"));
			util.setString(10, processData.get("P_CLEAR_PIN"));
			util.setString(11, RegularConstants.COLUMN_DISABLE);
			util.registerOutParameter(12, Types.TIMESTAMP);
			util.registerOutParameter(13, Types.VARCHAR);
			util.execute();
			String errorStatus = util.getString(13);
			if (errorStatus != null && errorStatus.equals(RegularConstants.SP_SUCCESS)) {
				processData.setObject("P_UPDATE_TYPE", "P");
				processData.setObject("P_CHANGE_DATETIME", util.getTimestamp(12));
				//updateLoginUserAudit(processData);
				processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(e.getLocalizedMessage());
		}
		return processResult;
	}
}