package patterns.config.framework.bo.common;


import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.process.TBAProcessResult;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.ServiceBO;


public class euserpreferenceBO extends ServiceBO {

	public euserpreferenceBO() {
	}
	
	public void init() {
	}

	public TBAProcessResult processRequest() {
		String sqlQuery = null;
		DBUtil util = getDbContext().createUtilInstance();
		try {
			if (processData.get("MODE").equals("I")) {
				sqlQuery = "INSERT INTO USERPREFERENCES(ENTITY_CODE,USER_ID,PRINT_STATIONERY_TYPE)VALUES(?,?,?)";
				util.reset();
				util.setMode(DBUtil.PREPARED);
				util.setSql(sqlQuery);
				util.setString(1, processData.get("ENTITY_CODE"));
				util.setString(2, processData.get("USER_ID"));
				util.setString(3, processData.get("PRINT_STATIONERY_TYPE"));
				util.executeUpdate();
				processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
				processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_UPDATED);
			} else if (processData.get("MODE").equals("U")) {
				sqlQuery = "UPDATE USERPREFERENCES SET PRINT_STATIONERY_TYPE=? WHERE ENTITY_CODE=? AND USER_ID=?";
				util.reset();
				util.setMode(DBUtil.PREPARED);
				util.setSql(sqlQuery);
				util.setString(1, processData.get("PRINT_STATIONERY_TYPE"));
				util.setString(2, processData.get("ENTITY_CODE"));
				util.setString(3, processData.get("USER_ID"));
				util.executeUpdate();
				processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
				processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_UPDATED);
			}else{
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
			}
			
			
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
			processResult.setErrorCode(e.getLocalizedMessage());
		}finally{
			util.reset();
		}
		return processResult;
	}

}