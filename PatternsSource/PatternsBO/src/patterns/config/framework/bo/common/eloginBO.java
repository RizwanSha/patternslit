package patterns.config.framework.bo.common;

import java.sql.Types;

import patterns.config.framework.bo.ServiceBO;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.process.TBAProcessResult;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.SessionConstants;

public class eloginBO extends ServiceBO {

	public eloginBO() {
	}

	public void init() {
	}

	public TBAProcessResult processRequest() {
		String sqlQuery = "CALL SP_USER_LOGIN_INIT(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		DBUtil util = getDbContext().createUtilInstance();
		try {
			util.reset();
			util.setMode(DBUtil.CALLABLE);
			util.setSql(sqlQuery);
			util.setString(1, processData.get("P_PARTITION_NO"));
			util.setString(2, processData.get("P_USER_ID"));
			util.setString(3, processData.get("P_USER_IP"));
			util.setString(4, processData.get("P_USER_AGENT"));
			util.setString(5, processData.get("P_LOGIN_SESSION_ID"));
			util.setString(6, processData.get("P_SERVER_ADDR"));
			util.registerOutParameter(7, Types.VARCHAR);
			util.registerOutParameter(8, Types.VARCHAR);
			util.registerOutParameter(9, Types.VARCHAR);
			util.registerOutParameter(10, Types.VARCHAR);
			util.registerOutParameter(11, Types.VARCHAR);
			util.registerOutParameter(12, Types.VARCHAR);
			util.registerOutParameter(13, Types.VARCHAR);
			util.registerOutParameter(14, Types.VARCHAR);
			util.execute();
			DTObject result = new DTObject();
			String errorStatus = util.getString(14);
			if (errorStatus != null && errorStatus.equals(RegularConstants.SP_SUCCESS)) {
				result.set(SessionConstants.ENTITY_CODE, util.getString(7));
				result.set(SessionConstants.USER_ID, processData.get("P_USER_ID"));
				result.set(SessionConstants.CLIENT_IP, processData.get("P_USER_IP"));
				result.set(SessionConstants.USER_AGENT, processData.get("P_USER_AGENT"));
				result.set(SessionConstants.PASSWORD_SALT, util.getString(8));
				result.set(SessionConstants.TFA_REQ, util.getString(9));
				result.set(SessionConstants.LOGIN_TFA_REQ, util.getString(10));
				result.set(SessionConstants.CERT_INV_NUM, util.getString(11));
				result.set(SessionConstants.MULTI_SESSION_CHECK, util.getString(12));
				result.set(SessionConstants.ORG_NAME, util.getString(13));
				processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
				processResult.setAdditionalInfo(result);
			} else {
				processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
				processResult.setErrorCode(errorStatus);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(e.getLocalizedMessage());
		}

		return processResult;
	}
}