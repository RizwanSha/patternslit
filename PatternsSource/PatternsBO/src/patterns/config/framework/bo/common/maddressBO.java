package patterns.config.framework.bo.common;

import java.sql.ResultSet;

import patterns.config.framework.bo.ServiceBO;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.process.TBAProcessResult;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTObject;


public class maddressBO extends ServiceBO {

	boolean isSuccess = false;
	String mainProgramId = RegularConstants.EMPTY_STRING;
	String currentProgramId = RegularConstants.EMPTY_STRING;
	String mainProgramPK = RegularConstants.EMPTY_STRING;
	String currentProgramPK = RegularConstants.EMPTY_STRING;
	String currentOperation = RegularConstants.EMPTY_STRING;
	String operationType = RegularConstants.EMPTY_STRING;
	String operationStatus = RegularConstants.EMPTY_STRING;
	long inventoryNumber = 0;

	public maddressBO() {
	}

	public TBAProcessResult processRequest() {
		operationType = processData.get("OPERATION_TYPE");
		operationStatus = processData.get("OPERATION_STATUS");
		mainProgramId = processData.get("PARENT_PGM_ID");
		mainProgramPK = processData.get("PARENT_PGM_PK");
		currentProgramId = processData.get("LINKED_PGM_ID");
		currentProgramPK = processData.get("LINKED_PGM_PK");
		currentOperation = processData.get("OPERATION_TYPE");
		try {
			if (operationType.equals("A")) {
				if (processData.containsKey("ADDR_INV_NO")) {
					inventoryNumber = Long.parseLong(processData.get("ADDR_INV_NO"));
				}
				inventoryNumber = create();
				if (inventoryNumber == 0) {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					return processResult;
				}
				processResult.setAdditionalInfo(inventoryNumber);
			}else if(operationStatus.equals("U")){
				if (!updateExistingAddress()) {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					return processResult;
				}
			}
			else {
				if (!update()) {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					return processResult;
				}
			}
			processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(e.getLocalizedMessage());
		}
		return processResult;
	}

	private long create() {
		TBADynaSQL addressDB = new TBADynaSQL("ADDRESSDB", false, getDbContext());
		long invNumber = 0;
		if (inventoryNumber == 0) {
			invNumber = getInventoryNumber(RegularConstants.ADDR_INV_NUM);
		} else {
			invNumber = inventoryNumber;
		}

		try {
			addressDB.setField("ADDR_INV_NO", String.valueOf(invNumber));
			addressDB.setField("ADDR_TYPE", processData.get("ADDR_TYPE"));
			addressDB.setField("COUNTRY_CODE", processData.get("COUNTRY_CODE"));
			addressDB.setField("ADDRESS_LINE1", processData.get("ADDRESS_LINE1"));
			addressDB.setField("ADDRESS_LINE2", processData.get("ADDRESS_LINE2"));
			addressDB.setField("ADDRESS_LINE3", processData.get("ADDRESS_LINE3"));
			addressDB.setField("ADDRESS_LINE4", processData.get("ADDRESS_LINE4"));
			addressDB.setField("GEO_UNIT_ID", processData.get("GEO_UNIT_ID"));
			addressDB.setField("ENABLED", processData.get("ENABLED"));
			addressDB.setField("REMARKS", processData.get("REMARKS"));
			String xmlData=addressDB.getRowData();
			
			
			addressDB.setNew(true);
			
			addressDB.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
			addressDB.setField("PARTITION_NO", processData.get("PARTITION_NO"));
			addressDB.setField("PARENT_PGM_ID", mainProgramId);
			addressDB.setField("PARENT_PGM_PK", mainProgramPK);
			addressDB.setField("CONVERSATION_ID", processData.get("CONVERSATION_ID"));
			addressDB.setField("LINKED_PGM_ID", currentProgramId);
			addressDB.setField("LINKED_PGM_PK", (processData.get("ENTITY_CODE") + "|" + String.valueOf(invNumber)));
			addressDB.setField("TABLE_NAME", "ADDRESSDB");
			addressDB.setField("OPERATION_TYPE", currentOperation);
			addressDB.setField("TABLE_TYPE", "S");

			addressDB.setField("ADDR_INV_NO", String.valueOf(invNumber));
			addressDB.setField("ADDR_TYPE", processData.get("ADDR_TYPE"));
			addressDB.setField("COUNTRY_CODE", processData.get("COUNTRY_CODE"));
			addressDB.setField("ADDRESS_LINE1", processData.get("ADDRESS_LINE1"));
			addressDB.setField("ADDRESS_LINE2", processData.get("ADDRESS_LINE2"));
			addressDB.setField("ADDRESS_LINE3", processData.get("ADDRESS_LINE3"));
			addressDB.setField("ADDRESS_LINE4", processData.get("ADDRESS_LINE4"));
			addressDB.setField("GEO_UNIT_ID", processData.get("GEO_UNIT_ID"));
			addressDB.setField("ENABLED", processData.get("ENABLED"));
			addressDB.setField("REMARKS", processData.get("REMARKS"));
			addressDB.saveInventory();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			addressDB.close();
		}
		return invNumber;

	}

	public boolean moveToHistory() {
		TBADynaSQL addressDBHist = new TBADynaSQL("ADDRESSDBHIST", false, getDbContext());
		try {

			long serial = 1;
			long invNumber = 0;
			String sourceKey = RegularConstants.EMPTY_STRING;
			invNumber = Long.parseLong(processData.get("ADDR_INV_NO"));
			sourceKey = String.valueOf(invNumber);
			DTObject input = new DTObject();
			input.set("SOURCE_KEY", sourceKey);
			input.set("PGM_ID", "ADDRESSDBHIST");
			serial = addressDBHist.getTBAReferenceSerial(input);
			addressDBHist.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
			addressDBHist.setField("PARTITION_NO", processData.get("PARTITION_NO"));
			addressDBHist.setField("PARENT_PGM_ID", mainProgramId);
			addressDBHist.setField("CONVERSATION_ID", processData.get("CONVERSATION_ID"));
			addressDBHist.setField("PARENT_PGM_PK", mainProgramPK);
			addressDBHist.setField("LINKED_PGM_ID", currentProgramId);
			addressDBHist.setField("LINKED_PGM_PK", (processData.get("PARTITION_NO") + "|" + String.valueOf(invNumber) + "|" + String.valueOf(serial)));
			addressDBHist.setField("INV_NUMBER", String.valueOf(invNumber));
			addressDBHist.setField("OPERATION_TYPE", "A");
			addressDBHist.setField("TABLE_NAME", "ADDRESSDBHIST");

			String query = "SELECT PARTITION_NO,ADDR_INV_NO,ADDR_TYPE,COUNTRY_CODE,ADDRESS_LINE1,ADDRESS_LINE2,ADDRESS_LINE3,ADDRESS_LINE4,GEO_UNIT_ID,ENABLED,REMARKS FROM ADDRESSDB WHERE PARTITION_NO=? AND ADDR_INV_NO=?";
			DBUtil dbUtil = getDbContext().createUtilInstance();
			dbUtil.setMode(DBUtil.PREPARED);
			dbUtil.setSql(query);
			dbUtil.setString(1, processData.get("PARTITION_NO"));
			dbUtil.setString(2, String.valueOf(invNumber));
			ResultSet rset = dbUtil.executeQuery();
			if (rset.next()) {
				addressDBHist.setNew(true);
				addressDBHist.setField("PARTITION_NO", rset.getString("PARTITION_NO"));
				addressDBHist.setField("ADDR_INV_NO", rset.getString("ADDR_INV_NO"));
				addressDBHist.setField("SL", String.valueOf(serial));
				addressDBHist.setField("ADDR_TYPE", rset.getString("ADDR_TYPE"));
				addressDBHist.setField("COUNTRY_CODE", rset.getString("COUNTRY_CODE"));
				addressDBHist.setField("ADDRESS_LINE1", rset.getString("ADDRESS_LINE1"));
				addressDBHist.setField("ADDRESS_LINE2", rset.getString("ADDRESS_LINE2"));
				addressDBHist.setField("ADDRESS_LINE3", rset.getString("ADDRESS_LINE3"));
				addressDBHist.setField("ADDRESS_LINE4", rset.getString("ADDRESS_LINE4"));
				addressDBHist.setField("GEO_UNIT_ID", rset.getString("GEO_UNIT_ID"));
				addressDBHist.setField("ENABLED", rset.getString("ENABLED"));
				addressDBHist.setField("REMARKS", rset.getString("REMARKS"));
				addressDBHist.saveInventory();
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			addressDBHist.close();
		}
		return false;
	}

	public boolean update() {
		TBADynaSQL addressDB = new TBADynaSQL("ADDRESSDB", false, getDbContext());
		try {

			addressDB.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
			addressDB.setField("PARTITION_NO", processData.get("PARTITION_NO"));
			addressDB.setField("PARENT_PGM_ID", mainProgramId);
			addressDB.setField("CONVERSATION_ID", processData.get("CONVERSATION_ID"));
			addressDB.setField("PARENT_PGM_PK", mainProgramPK);
			addressDB.setField("LINKED_PGM_ID", currentProgramId);
			addressDB.setField("LINKED_PGM_PK", (processData.get("PARTITION_NO") + "|" + processData.get("ADDR_INV_NO")));
			addressDB.setField("INV_NUMBER", processData.get("ADDR_INV_NO"));
			addressDB.setField("OPERATION_TYPE", currentOperation);
			addressDB.setField("TABLE_NAME", "ADDRESSDB");
			if (operationStatus.equals("R")) {
				addressDB.deleteInventoryData();

			} else {
				moveToHistory();
				addressDB.setNew(false);
				addressDB.setField("ADDR_INV_NO", processData.get("ADDR_INV_NO"));
				addressDB.setField("ADDR_TYPE", processData.get("ADDR_TYPE"));
				addressDB.setField("COUNTRY_CODE", processData.get("COUNTRY_CODE"));
				addressDB.setField("ADDRESS_LINE1", processData.get("ADDRESS_LINE1"));
				addressDB.setField("ADDRESS_LINE2", processData.get("ADDRESS_LINE2"));
				addressDB.setField("ADDRESS_LINE3", processData.get("ADDRESS_LINE3"));
				addressDB.setField("ADDRESS_LINE4", processData.get("ADDRESS_LINE4"));
				addressDB.setField("GEO_UNIT_ID", processData.get("GEO_UNIT_ID"));
				addressDB.setField("ENABLED", processData.get("ENABLED"));
				addressDB.setField("REMARKS", processData.get("REMARKS"));
				addressDB.saveInventory();
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			addressDB.close();
		}
		return false;
	}

public boolean updateExistingAddress() {
	TBADynaSQL addressDB = new TBADynaSQL("ADDRESSDB", false, getDbContext());
	try {
		addressDB.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		addressDB.setField("PARTITION_NO", processData.get("PARTITION_NO"));
		addressDB.setField("PARENT_PGM_ID", mainProgramId);
		addressDB.setField("CONVERSATION_ID", processData.get("CONVERSATION_ID"));
		addressDB.setField("PARENT_PGM_PK", mainProgramPK);
		addressDB.setField("LINKED_PGM_ID", currentProgramId);
		addressDB.setField("LINKED_PGM_PK", (processData.get("PARTITION_NO") + "|" + processData.get("ADDR_INV_NO")));
		addressDB.setField("INV_NUMBER", processData.get("ADDR_INV_NO"));
		addressDB.setField("OPERATION_TYPE", currentOperation);
		addressDB.setField("TABLE_NAME", "ADDRESSDB");
		String query = "SELECT PARTITION_NO,ADDR_INV_NO,ADDR_TYPE,COUNTRY_CODE,ADDRESS_LINE1,ADDRESS_LINE2,ADDRESS_LINE3,ADDRESS_LINE4,GEO_UNIT_ID,ENABLED,REMARKS FROM ADDRESSDB WHERE PARTITION_NO=? AND ADDR_INV_NO=?";
		DBUtil dbUtil = getDbContext().createUtilInstance();
		dbUtil.setMode(DBUtil.PREPARED);
		dbUtil.setSql(query);
		dbUtil.setString(1, processData.get("PARTITION_NO"));
		dbUtil.setString(2, processData.get("ADDR_INV_NO"));
		ResultSet rset = dbUtil.executeQuery();
		if (rset.next()) {
			addressDB.setNew(false);
			addressDB.setField("ADDR_INV_NO", rset.getString("ADDR_INV_NO"));
			addressDB.setField("ADDR_TYPE", rset.getString("ADDR_TYPE"));
			addressDB.setField("COUNTRY_CODE", rset.getString("COUNTRY_CODE"));
			addressDB.setField("ADDRESS_LINE1", rset.getString("ADDRESS_LINE1"));
			addressDB.setField("ADDRESS_LINE2", rset.getString("ADDRESS_LINE2"));
			addressDB.setField("ADDRESS_LINE3", rset.getString("ADDRESS_LINE3"));
			addressDB.setField("ADDRESS_LINE4", rset.getString("ADDRESS_LINE4"));
			addressDB.setField("GEO_UNIT_ID", rset.getString("GEO_UNIT_ID"));
			addressDB.setField("ENABLED", rset.getString("ENABLED"));
			addressDB.setField("REMARKS", rset.getString("REMARKS"));
			addressDB.saveInventory();
		}
		return true;
	} catch (Exception e) {
		e.printStackTrace();
	} finally {
		addressDB.close();
	}
	return false;
}
}