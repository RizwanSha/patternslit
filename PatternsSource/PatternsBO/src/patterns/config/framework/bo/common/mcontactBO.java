package patterns.config.framework.bo.common;

import java.sql.ResultSet;

import patterns.config.framework.bo.ServiceBO;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.process.TBAProcessResult;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTObject;

public class mcontactBO extends ServiceBO {

	TBADynaSQL mcontact = null;
	boolean isSuccess = false;
	String mainProgramId = RegularConstants.EMPTY_STRING;
	String currentProgramId = RegularConstants.EMPTY_STRING;
	String mainProgramPK = RegularConstants.EMPTY_STRING;
	String currentProgramPK = RegularConstants.EMPTY_STRING;
	String currentOperation = RegularConstants.EMPTY_STRING;
	String operationType = RegularConstants.EMPTY_STRING;
	String operationStatus = RegularConstants.EMPTY_STRING;
	long inventoryNumber = 0;
	
	public mcontactBO() {
	}

	@Override
	public TBAProcessResult processRequest() {
		operationType = processData.get("OPERATION_TYPE");
		operationStatus = processData.get("OPERATION_STATUS");
		mainProgramId = processData.get("PARENT_PGM_ID");
		mainProgramPK = processData.get("PARENT_PGM_PK");
		currentProgramId = processData.get("LINKED_PGM_ID");
		currentProgramPK = processData.get("LINKED_PGM_PK");
		currentOperation = processData.get("OPERATION_TYPE");
		try {
			if (operationType.equals("A")) {
				if (processData.containsKey("CONTACT_INV_NO")) {
					inventoryNumber = Long.parseLong(processData.get("CONTACT_INV_NO"));
				}
				inventoryNumber = create();
				if (inventoryNumber == 0) {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					return processResult;
				}
				processResult.setAdditionalInfo(inventoryNumber);
			}else if(operationStatus.equals("U")){
				if (!updateExistingContact()) {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					return processResult;
				}
			} else {
				if (!update()) {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					return processResult;
				}
			}
			processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(e.getLocalizedMessage());
		}
		return processResult;
	}

	private long create() {
		TBADynaSQL contactsDB = new TBADynaSQL("CONTACTDB", false, getDbContext());
		long invNumber = 0;
		if (inventoryNumber == 0) {
			invNumber = getInventoryNumber(RegularConstants.ADDR_INV_NUM);
		} else {
			invNumber = inventoryNumber;
		}
		try {
			contactsDB.setNew(true);
			contactsDB.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
			contactsDB.setField("PARTITION_NO", processData.get("PARTITION_NO"));
			contactsDB.setField("PARENT_PGM_ID", mainProgramId);
			contactsDB.setField("PARENT_PGM_PK", mainProgramPK);
			contactsDB.setField("LINKED_PGM_ID", currentProgramId);
			contactsDB.setField("CONVERSATION_ID", processData.get("CONVERSATION_ID"));
			contactsDB.setField("LINKED_PGM_PK", (processData.get("PARTITION_NO") + "|" + String.valueOf(invNumber)));
			contactsDB.setField("TABLE_NAME", "CONTACTDB");
			contactsDB.setField("OPERATION_TYPE", currentOperation);
			contactsDB.setField("TABLE_TYPE", "S");
			contactsDB.setField("CONTACT_INV_NO", String.valueOf(invNumber));
			contactsDB.setField("CONTACT_TYPE", processData.get("CONTACT_TYPE"));
			contactsDB.setField("LL_COUNTRY_CODE", processData.get("LL_COUNTRY_CODE"));
			contactsDB.setField("LL_LOC_STD_CODE", processData.get("LL_LOC_STD_CODE"));
			contactsDB.setField("LL_CONTACT_NUMBER", processData.get("LL_CONTACT_NUMBER"));
			contactsDB.setField("MOB_COUNTRY_CODE", processData.get("MOB_COUNTRY_CODE"));
			contactsDB.setField("MOB_CONTACT_NUMBER", processData.get("MOB_CONTACT_NUMBER"));
			contactsDB.setField("EMAIL_ID", processData.get("EMAIL_ID"));
			contactsDB.setField("ENABLED", processData.get("ENABLED"));
			contactsDB.setField("REMARKS", processData.get("REMARKS"));
			contactsDB.saveInventory();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			contactsDB.close();
		}
		return invNumber;
	}

	public boolean moveToHistory() {
		TBADynaSQL contactsDBHist = new TBADynaSQL("CONTACTDBHIST", false, getDbContext());
		try {
			long serial = 1;
			long invNumber = 0;
			String sourceKey = RegularConstants.EMPTY_STRING;
			invNumber = Long.parseLong(processData.get("CONTACT_INV_NO"));
			sourceKey = String.valueOf(invNumber);

			DTObject input = new DTObject();
			input.set("SOURCE_KEY", sourceKey);
			input.set("PGM_ID", "CONTACTDBHIST");
			serial = contactsDBHist.getTBAReferenceSerial(input);
			contactsDBHist.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
			contactsDBHist.setField("PARTITION_NO", processData.get("PARTITION_NO"));
			contactsDBHist.setField("PARENT_PGM_ID", mainProgramId);
			contactsDBHist.setField("PARENT_PGM_PK", mainProgramPK);
			contactsDBHist.setField("CONVERSATION_ID", processData.get("CONVERSATION_ID"));
			contactsDBHist.setField("LINKED_PGM_ID", currentProgramId);
			contactsDBHist.setField("LINKED_PGM_PK", (processData.get("PARTITION_NO") + "|" + String.valueOf(invNumber)	+ "|" + String.valueOf(serial)));
			contactsDBHist.setField("OPERATION_TYPE", "A");
			contactsDBHist.setField("TABLE_NAME", "CONTACTDBHIST");

			String query = "SELECT CONTACT_INV_NO,CONTACT_TYPE,LL_COUNTRY_CODE,LL_LOC_STD_CODE,LL_CONTACT_NUMBER,MOB_COUNTRY_CODE,MOB_CONTACT_NUMBER,EMAIL_ID,ENABLED,REMARKS FROM CONTACTDB WHERE PARTITION_NO=? AND CONTACT_INV_NO=?";
			DBUtil dbUtil = getDbContext().createUtilInstance();
			dbUtil.setMode(DBUtil.PREPARED);
			dbUtil.setSql(query);
			dbUtil.setString(1, processData.get("PARTITION_NO"));
			dbUtil.setString(2, String.valueOf(invNumber));
			ResultSet rset = dbUtil.executeQuery();
			if (rset.next()) {
				contactsDBHist.setNew(true);
				contactsDBHist.setField("CONTACT_INV_NO", rset.getString("CONTACT_INV_NO"));
				contactsDBHist.setField("SL", String.valueOf(serial));
				contactsDBHist.setField("CONTACT_TYPE", rset.getString("CONTACT_TYPE"));
				contactsDBHist.setField("LL_COUNTRY_CODE", rset.getString("LL_COUNTRY_CODE"));
				contactsDBHist.setField("LL_LOC_STD_CODE", rset.getString("LL_LOC_STD_CODE"));
				contactsDBHist.setField("LL_CONTACT_NUMBER", rset.getString("LL_CONTACT_NUMBER"));
				contactsDBHist.setField("MOB_COUNTRY_CODE", rset.getString("MOB_COUNTRY_CODE"));
				contactsDBHist.setField("MOB_CONTACT_NUMBER", rset.getString("MOB_CONTACT_NUMBER"));
				contactsDBHist.setField("EMAIL_ID", rset.getString("EMAIL_ID"));
				contactsDBHist.setField("ENABLED", rset.getString("ENABLED"));
				contactsDBHist.setField("REMARKS", rset.getString("REMARKS"));
				contactsDBHist.saveInventory();
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			contactsDBHist.close();
		}
		return false;
	}

	public boolean update() {
		TBADynaSQL contactsDB = new TBADynaSQL("CONTACTDB", true, getDbContext());
		try {
			contactsDB.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
			contactsDB.setField("PARTITION_NO", processData.get("PARTITION_NO"));
			contactsDB.setField("PARENT_PGM_ID", mainProgramId);
			contactsDB.setField("PARENT_PGM_PK", mainProgramPK);
			contactsDB.setField("CONVERSATION_ID", processData.get("CONVERSATION_ID"));
			contactsDB.setField("LINKED_PGM_ID", currentProgramId);
			contactsDB.setField("LINKED_PGM_PK",(processData.get("PARTITION_NO") + "|" + processData.get("CONTACT_INV_NO")));
			contactsDB.setField("INV_NUMBER", processData.get("CONTACT_INV_NO"));
			contactsDB.setField("OPERATION_TYPE", currentOperation);
			contactsDB.setField("TABLE_NAME", "CONTACTDB");
			if (operationStatus.equals("R")) {
				contactsDB.deleteInventoryData();
			} else {
				moveToHistory();
				contactsDB.setNew(false);
				contactsDB.setField("CONTACT_INV_NO", processData.get("CONTACT_INV_NO"));
				contactsDB.setField("CONTACT_TYPE", processData.get("CONTACT_TYPE"));
				contactsDB.setField("LL_COUNTRY_CODE", processData.get("LL_COUNTRY_CODE"));
				contactsDB.setField("LL_LOC_STD_CODE", processData.get("LL_LOC_STD_CODE"));
				contactsDB.setField("LL_CONTACT_NUMBER", processData.get("LL_CONTACT_NUMBER"));
				contactsDB.setField("MOB_COUNTRY_CODE", processData.get("MOB_COUNTRY_CODE"));
				contactsDB.setField("MOB_CONTACT_NUMBER", processData.get("MOB_CONTACT_NUMBER"));
				contactsDB.setField("EMAIL_ID", processData.get("EMAIL_ID"));
				contactsDB.setField("ENABLED", processData.get("ENABLED"));
				contactsDB.setField("REMARKS", processData.get("REMARKS"));
				contactsDB.saveInventory();
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			contactsDB.close();
		}
		return false;
	}
	public boolean updateExistingContact() {
		TBADynaSQL contactsDB = new TBADynaSQL("CONTACTDB", true, getDbContext());
		try {
			contactsDB.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
			contactsDB.setField("PARTITION_NO", processData.get("PARTITION_NO"));
			contactsDB.setField("PARENT_PGM_ID", mainProgramId);
			contactsDB.setField("PARENT_PGM_PK", mainProgramPK);
			contactsDB.setField("CONVERSATION_ID", processData.get("CONVERSATION_ID"));
			contactsDB.setField("LINKED_PGM_ID", currentProgramId);
			contactsDB.setField("LINKED_PGM_PK",(processData.get("PARTITION_NO") + "|" + processData.get("CONTACT_INV_NO")));
			contactsDB.setField("INV_NUMBER", processData.get("CONTACT_INV_NO"));
			contactsDB.setField("OPERATION_TYPE", currentOperation);
			contactsDB.setField("TABLE_NAME", "CONTACTDB");

			String query = "SELECT CONTACT_INV_NO,CONTACT_TYPE,LL_COUNTRY_CODE,LL_LOC_STD_CODE,LL_CONTACT_NUMBER,MOB_COUNTRY_CODE,MOB_CONTACT_NUMBER,EMAIL_ID,ENABLED,REMARKS FROM CONTACTDB WHERE PARTITION_NO=? AND CONTACT_INV_NO=?";
			DBUtil dbUtil = getDbContext().createUtilInstance();
			dbUtil.setMode(DBUtil.PREPARED);
			dbUtil.setSql(query);
			dbUtil.setString(1, processData.get("PARTITION_NO"));
			dbUtil.setString(2, processData.get("CONTACT_INV_NO"));
			ResultSet rset = dbUtil.executeQuery();
			if (rset.next()) {
				contactsDB.setNew(true);
				contactsDB.setField("CONTACT_INV_NO", rset.getString("CONTACT_INV_NO"));
				contactsDB.setField("CONTACT_TYPE", rset.getString("CONTACT_TYPE"));
				contactsDB.setField("LL_COUNTRY_CODE", rset.getString("LL_COUNTRY_CODE"));
				contactsDB.setField("LL_LOC_STD_CODE", rset.getString("LL_LOC_STD_CODE"));
				contactsDB.setField("LL_CONTACT_NUMBER", rset.getString("LL_CONTACT_NUMBER"));
				contactsDB.setField("MOB_COUNTRY_CODE", rset.getString("MOB_COUNTRY_CODE"));
				contactsDB.setField("MOB_CONTACT_NUMBER", rset.getString("MOB_CONTACT_NUMBER"));
				contactsDB.setField("EMAIL_ID", rset.getString("EMAIL_ID"));
				contactsDB.setField("ENABLED", rset.getString("ENABLED"));
				contactsDB.setField("REMARKS", rset.getString("REMARKS"));
				contactsDB.saveInventory();
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			contactsDB.close();
		}
		return false;
	}
}