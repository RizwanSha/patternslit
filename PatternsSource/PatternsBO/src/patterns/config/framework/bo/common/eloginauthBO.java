package patterns.config.framework.bo.common;

import java.sql.Types;

import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.process.TBAProcessResult;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.SessionConstants;
import patterns.config.framework.bo.ServiceBO;

public class eloginauthBO extends ServiceBO {

	public eloginauthBO() {
	}

	public void init() {
	}

	public TBAProcessResult processRequest() {
		String sqlQuery = "CALL SP_USER_LOGIN_VALID(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		DBUtil util = getDbContext().createUtilInstance();
		try {
			util.reset();
			util.setMode(DBUtil.CALLABLE);
			util.setSql(sqlQuery);
			util.setString(1, processData.get("P_PARTITION_NO"));
			util.setString(2, processData.get("P_ENTITY_CODE"));
			util.setString(3, processData.get("P_USER_ID"));
			util.setString(4, processData.get("P_FIRST_PIN_VALID"));
			util.setString(5, processData.get("P_TFA_VALID"));
			util.setString(6, processData.get("P_USER_IP"));
			util.setString(7, processData.get("P_USER_AGENT"));
			util.setString(8, processData.get("P_LOGIN_SESSION_ID"));
			util.setString(9, processData.get("P_SERVER_ADDR"));
			util.setString(10, processData.get("P_MULTI_SESSION_CHECK"));
			util.registerOutParameter(11, Types.VARCHAR);
			util.registerOutParameter(12, Types.DATE);
			util.registerOutParameter(13, Types.VARCHAR);
			util.registerOutParameter(14, Types.INTEGER);
			util.registerOutParameter(15, Types.VARCHAR);
			util.registerOutParameter(16, Types.VARCHAR);
			util.registerOutParameter(17, Types.TIMESTAMP);
			util.registerOutParameter(18, Types.VARCHAR);
			util.registerOutParameter(19, Types.TIMESTAMP);
			util.registerOutParameter(20, Types.VARCHAR);
			util.registerOutParameter(21, Types.SMALLINT);
			util.registerOutParameter(22, Types.CHAR);
			util.registerOutParameter(23, Types.NUMERIC);
			util.registerOutParameter(24, Types.CHAR);
			util.registerOutParameter(25, Types.VARCHAR);
			util.registerOutParameter(26, Types.NUMERIC);
			util.registerOutParameter(27, Types.NUMERIC);
			util.registerOutParameter(28, Types.NUMERIC);
			util.registerOutParameter(29, Types.VARCHAR);
			util.registerOutParameter(30, Types.VARCHAR);
			
			util.execute();
			DTObject result = new DTObject();
			String errorStatus = util.getString(25);
			if (errorStatus != null && errorStatus.equals(RegularConstants.SP_SUCCESS)) {
				processData.setObject("P_UPDATE_TYPE", "L");
				processData.setObject("P_CHANGE_DATETIME", util.getTimestamp(19));
				result.set(SessionConstants.ENTITY_CODE, processData.get("P_ENTITY_CODE"));
				result.set(SessionConstants.USER_ID, processData.get("P_USER_ID"));
				result.set(SessionConstants.CLIENT_IP, processData.get("P_USER_IP"));
				result.set(SessionConstants.USER_AGENT, processData.get("P_USER_AGENT"));
				result.set(SessionConstants.BASE_CURRENCY, util.getString(11));
				result.setObject(SessionConstants.CBD, util.getDate(12));
				result.set(SessionConstants.USER_NAME, util.getString(13));
				result.setObject("P_AUTO_LOGOUT_SECS", util.getInt(14));
				result.set(SessionConstants.ROLE_TYPE, util.getString(15));
				result.set(SessionConstants.ROLE_CODE, util.getString(16));
				result.setObject(SessionConstants.LAST_LOGIN_DATE_TIME, util.getTimestamp(17));
				result.set(SessionConstants.PASSWORD_RESET, util.getString(18));
				result.setObject(SessionConstants.LOGIN_DATE_TIME, util.getTimestamp(19));
				result.set(SessionConstants.DATE_FORMAT, util.getString(20));
				result.set(SessionConstants.CURRENCY_UNITS, util.getString(21));
				result.set(SessionConstants.EMPCODE_TYPE, util.getString(22));
				result.set(SessionConstants.EMPCODE_SIZE, util.getString(23));
				result.set(SessionConstants.REG_NO_YR_PREFIX_REQD, util.getString(24));
				result.set(SessionConstants.CURR_YEAR, util.getString(26));
				result.set(SessionConstants.FIN_YEAR, util.getString(27));
				result.set(SessionConstants.START_FIN_MONTH, util.getString(28));
				result.set(SessionConstants.BRANCH_CODE, util.getString(29));
				result.set(SessionConstants.CLUSTER_CODE, util.getString(30));
				processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
				processResult.setAdditionalInfo(result);
			} else {
				processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
				processResult.setErrorCode(errorStatus);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(e.getLocalizedMessage());
		}

		return processResult;
	}

}