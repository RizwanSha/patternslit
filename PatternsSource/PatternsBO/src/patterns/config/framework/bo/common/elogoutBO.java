package patterns.config.framework.bo.common;

import java.sql.Timestamp;
import java.sql.Types;

import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.process.TBAProcessResult;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.bo.ServiceBO;

public class elogoutBO extends ServiceBO {

	public elogoutBO() {
	}

	public void init() {
	}

	public TBAProcessResult processRequest() {
		//String sqlQuery = "CALL PKG_BANKUSER_COMMON.SP_BANKUSER_LOGOUT(?,?,?,?,?,?)";
		String sqlQuery = "{CALL SP_USER_LOGOUT(?,?,?,?,?,?)}";
		DBUtil util = getDbContext().createUtilInstance();
		util.reset();
		try {
			util.setMode(DBUtil.CALLABLE);
			util.setSql(sqlQuery);
			util.setString(1, processData.get("P_ENTITY_CODE"));
			util.setString(2, processData.get("P_USER_ID"));
			util.setTimestamp(3, (Timestamp) processData.getObject("P_LOGIN_DATE"));
			util.setString(4, processData.get("P_REASON"));
			util.setString(5, processData.get("P_SESSION_ID"));
			util.registerOutParameter(6, Types.VARCHAR);
			util.execute();
			DTObject result = new DTObject();
			String errorStatus = util.getString(6);
			if (errorStatus != null && errorStatus.equals(RegularConstants.SP_SUCCESS)) {
				processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
				processResult.setAdditionalInfo(result);
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(errorStatus);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(e.getLocalizedMessage());
		}

		return processResult;
	}
}