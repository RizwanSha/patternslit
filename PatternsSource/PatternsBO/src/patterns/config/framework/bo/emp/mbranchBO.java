package patterns.config.framework.bo.emp;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;

public class mbranchBO extends GenericBO {

	TBADynaSQL mbranch = null;

	public mbranchBO() {
	}

	public void init() {
		mbranch = new TBADynaSQL("BRANCH", true);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("BRANCH_CODE");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("BRANCH_CODE"));
		mbranch.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		mbranch.setField("BRANCH_CODE", processData.get("BRANCH_CODE"));
		dataExists = mbranch.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				mbranch.setNew(true);
				mbranch.setField("BRANCH_NAME", processData.get("BRANCH_NAME"));
				mbranch.setField("ADDRESS", processData.get("ADDRESS"));
				mbranch.setField("PIN_CODE", processData.get("PIN_CODE"));
				mbranch.setField("STATE_CODE", processData.get("STATE_CODE"));
				mbranch.setField("REGION_CODE", processData.get("REGION_CODE"));
				mbranch.setField("AREA_CODE", processData.get("AREA_CODE"));
				mbranch.setField("VAT_TIN_NO", processData.get("VAT_TIN_NO"));
				mbranch.setField("CST_TIN_NO", processData.get("CST_TIN_NO"));
				mbranch.setField("ENABLED", processData.get("ENABLED"));
				mbranch.setField("REMARKS", processData.get("REMARKS"));
				if (mbranch.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
					mbranch.sendInterfaceOutwardMessage();
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}

			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mbranch.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				mbranch.setNew(false);
				mbranch.setField("AREA_CODE", processData.get("AREA_CODE"));
				mbranch.setField("BRANCH_NAME", processData.get("BRANCH_NAME"));
				mbranch.setField("ADDRESS", processData.get("ADDRESS"));
				mbranch.setField("PIN_CODE", processData.get("PIN_CODE"));
				mbranch.setField("STATE_CODE", processData.get("STATE_CODE"));
				mbranch.setField("REGION_CODE", processData.get("REGION_CODE"));
				mbranch.setField("AREA_CODE", processData.get("AREA_CODE"));
				mbranch.setField("VAT_TIN_NO", processData.get("VAT_TIN_NO"));
				mbranch.setField("CST_TIN_NO", processData.get("CST_TIN_NO"));
				mbranch.setField("ENABLED", processData.get("ENABLED"));
				mbranch.setField("REMARKS", processData.get("REMARKS"));
				if (mbranch.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
					mbranch.sendInterfaceOutwardMessage();
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mbranch.close();
		}
	}
}