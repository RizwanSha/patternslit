package patterns.config.framework.bo.emp;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;

public class mlamhierBO extends GenericBO {

	TBADynaSQL lamhier = null;

	public mlamhierBO() {
	}

	public void init() {
		lamhier = new TBADynaSQL("LAMHIER", true);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("LAM_HIER_CODE");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("LAM_HIER_CODE"));
		lamhier.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		lamhier.setField("LAM_HIER_CODE", processData.get("LAM_HIER_CODE"));
		dataExists = lamhier.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {

		try {
			if (!isDataExists()) {
				lamhier.setNew(true);
				lamhier.setField("LAM_HIER_CODE", processData.get("LAM_HIER_CODE"));
				lamhier.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				lamhier.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				lamhier.setField("ENABLED", processData.get("ENABLED"));
				lamhier.setField("REMARKS", processData.get("REMARKS"));
				if (lamhier.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			lamhier.close();
		}

	}

	protected void modify() throws TBAFrameworkException {

		try {
			if (isDataExists()) {
				lamhier.setNew(false);
				lamhier.setField("LAM_HIER_CODE", processData.get("LAM_HIER_CODE"));
				lamhier.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				lamhier.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				lamhier.setField("ENABLED", processData.get("ENABLED"));
				lamhier.setField("REMARKS", processData.get("REMARKS"));
				if (lamhier.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			lamhier.close();
		}
	}

}