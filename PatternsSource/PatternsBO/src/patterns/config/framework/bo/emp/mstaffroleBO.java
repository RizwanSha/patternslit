package patterns.config.framework.bo.emp;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;

public class mstaffroleBO extends GenericBO {

	TBADynaSQL mstaffrole = null;

	public mstaffroleBO() {
	}

	public void init() {
		mstaffrole = new TBADynaSQL("STAFFROLE", true);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("STAFF_ROLE_CODE");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("STAFF_ROLE_CODE"));
		mstaffrole.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		mstaffrole.setField("STAFF_ROLE_CODE", processData.get("STAFF_ROLE_CODE"));
		dataExists = mstaffrole.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				mstaffrole.setNew(true);
				mstaffrole.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				mstaffrole.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				mstaffrole.setField("PARENT_STAFF_ROLE_CODE", processData.get("PARENT_STAFF_ROLE_CODE"));
				mstaffrole.setField("BRANCH_ROLE", processData.get("BRANCH_ROLE"));
				mstaffrole.setField("ROLE_CATEGORY", processData.get("ROLE_CATEGORY"));
				mstaffrole.setField("BRANCH_ROLE", processData.get("BRANCH_ROLE"));
				mstaffrole.setField("REGIONAL_ROLE", processData.get("REGIONAL_ROLE"));
				mstaffrole.setField("NATIONAL_ROLE", processData.get("NATIONAL_ROLE"));
				mstaffrole.setField("ENABLED", processData.get("ENABLED"));
				mstaffrole.setField("REMARKS", processData.get("REMARKS"));
				if (mstaffrole.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mstaffrole.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				mstaffrole.setNew(false);
				mstaffrole.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				mstaffrole.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				mstaffrole.setField("PARENT_STAFF_ROLE_CODE", processData.get("PARENT_STAFF_ROLE_CODE"));
				mstaffrole.setField("ROLE_CATEGORY", processData.get("ROLE_CATEGORY"));
				mstaffrole.setField("BRANCH_ROLE", processData.get("BRANCH_ROLE"));
				mstaffrole.setField("REGIONAL_ROLE", processData.get("REGIONAL_ROLE"));
				mstaffrole.setField("NATIONAL_ROLE", processData.get("NATIONAL_ROLE"));
				mstaffrole.setField("ENABLED", processData.get("ENABLED"));
				mstaffrole.setField("REMARKS", processData.get("REMARKS"));
				if (mstaffrole.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mstaffrole.close();
		}
	}
}
