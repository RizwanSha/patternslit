package patterns.config.framework.bo.emp;

/*
 *
 Author : NARESHKUMAR D
 Created Date : 30-JAN-2017
 Spec Reference : TATACAP/EMP/0013-ISTAFFSIGNATURE - Staff Signature Image File Uploads and Maintenance
 Modification History
 ----------------------------------------------------------------------
 Sl.No		Modified Date	Author			Modified Changes	Version
 ----------------------------------------------------------------------

 */
import java.util.Date;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.web.BackOfficeFormatUtils;

public class istaffsignatureBO extends GenericBO {
	TBADynaSQL istaffsignature = null;

	public istaffsignatureBO() {
	}

	public void init() {
		istaffsignature = new TBADynaSQL("STAFFSIGNATUREHIST", true);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("STAFF_CODE") + RegularConstants.PK_SEPARATOR + BackOfficeFormatUtils.getDate((Date) processData.getObject("EFF_DATE"), BackOfficeConstants.TBA_DATE_FORMAT);
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(primaryKey);
		istaffsignature.setField("ENTITY_CODE", Long.parseLong(processData.get("ENTITY_CODE")));
		istaffsignature.setField("STAFF_CODE", processData.get("STAFF_CODE"));
		istaffsignature.setField("EFF_DATE", processData.getObject("EFF_DATE"));
		dataExists = istaffsignature.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				istaffsignature.setNew(true);
				istaffsignature.setField("FILE_INV_NO", processData.get("FILE_INV_NO"));
				istaffsignature.setField("REFERENCE_NO", processData.get("REFERENCE_NO"));
				if (istaffsignature.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			istaffsignature.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				istaffsignature.setNew(false);
				istaffsignature.setField("FILE_INV_NO", processData.get("FILE_INV_NO"));
				istaffsignature.setField("REFERENCE_NO", processData.get("REFERENCE_NO"));
				if (istaffsignature.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			istaffsignature.close();
		}
	}

}