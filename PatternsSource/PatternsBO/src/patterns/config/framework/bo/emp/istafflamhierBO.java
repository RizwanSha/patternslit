package patterns.config.framework.bo.emp;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.web.BackOfficeFormatUtils;

public class istafflamhierBO extends GenericBO {
	TBADynaSQL stafflamhierhist = null;

	public void init() {
		stafflamhierhist = new TBADynaSQL("STAFFLAMHIERHIST", true);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("STAFF_CODE") + RegularConstants.PK_SEPARATOR + BackOfficeFormatUtils.getDate(processData.getDate("EFF_DATE"), BackOfficeConstants.TBA_DATE_FORMAT);
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("STAFF_CODE"));
		stafflamhierhist.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		stafflamhierhist.setField("STAFF_CODE", processData.get("STAFF_CODE"));
		stafflamhierhist.setField("EFF_DATE", processData.getDate("EFF_DATE"));
		dataExists = stafflamhierhist.loadByKeyFields();
	}

	@Override
	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				stafflamhierhist.setNew(true);
				stafflamhierhist.setField("LAM_HIER_CODE", processData.get("LAM_HIER_CODE"));
				stafflamhierhist.setField("ALLOCATION_REF", processData.get("ALLOCATION_REF"));
				stafflamhierhist.setField("ENABLED", processData.get("ENABLED"));
				stafflamhierhist.setField("REMARKS", processData.get("REMARKS"));
				if (stafflamhierhist.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			stafflamhierhist.close();
		}
	}

	@Override
	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				stafflamhierhist.setNew(false);
				stafflamhierhist.setField("LAM_HIER_CODE", processData.get("LAM_HIER_CODE"));
				stafflamhierhist.setField("ALLOCATION_REF", processData.get("ALLOCATION_REF"));
				stafflamhierhist.setField("ENABLED", processData.get("ENABLED"));
				stafflamhierhist.setField("REMARKS", processData.get("REMARKS"));
				if (stafflamhierhist.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			stafflamhierhist.close();
		}
	}

}
