package patterns.config.framework.bo.emp;

/*
 *
 Author : Javeed S
 Created Date : 08-FEB-2016
 Spec Reference : PPBS/EMP/0037-MSTAFF - STAFF DETAILS MAINTENANCE
 Modification History
 ----------------------------------------------------------------------
 Sl.No		Modified Date	Author			Modified Changes	Version
 ----------------------------------------------------------------------

 */
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;

public class mstaffBO extends GenericBO {

	TBADynaSQL staff = null;

	public mstaffBO() {
	}

	public void init() {
		staff = new TBADynaSQL("STAFF", true);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("STAFF_CODE");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("STAFF_CODE"));
		staff.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		staff.setField("STAFF_CODE", processData.get("STAFF_CODE"));
		dataExists = staff.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {

		try {
			if (!isDataExists()) {
				staff.setNew(true);
				staff.setField("STAFF_CODE", processData.get("STAFF_CODE"));
				staff.setField("NAME", processData.get("NAME"));
				staff.setField("DATE_OF_JOINING", processData.getDate("DATE_OF_JOINING"));
				staff.setField("EMAIL_ID", processData.get("EMAIL_ID"));
				staff.setField("MOBILE_NO", processData.get("MOBILE_NO"));
				staff.setField("BRANCH_CODE", processData.get("BRANCH_CODE"));
				staff.setField("STAFF_ROLE_CODE", processData.get("STAFF_ROLE_CODE"));
				staff.setField("REGION_CODE", processData.get("REGION_CODE"));
				staff.setField("AREA_CODE", processData.get("AREA_CODE"));
				staff.setField("REPORTING_STAFF_CODE", processData.get("REPORTING_STAFF_CODE"));
				staff.setField("REMARKS", processData.get("REMARKS"));
				if (staff.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			staff.close();
		}

	}

	protected void modify() throws TBAFrameworkException {

		try {
			if (isDataExists()) {
				staff.setNew(false);
				staff.setField("STAFF_CODE", processData.get("STAFF_CODE"));
				staff.setField("NAME", processData.get("NAME"));
				staff.setField("DATE_OF_JOINING", processData.getDate("DATE_OF_JOINING"));
				staff.setField("EMAIL_ID", processData.get("EMAIL_ID"));
				staff.setField("MOBILE_NO", processData.get("MOBILE_NO"));
				staff.setField("BRANCH_CODE", processData.get("BRANCH_CODE"));
				staff.setField("STAFF_ROLE_CODE", processData.get("STAFF_ROLE_CODE"));
				staff.setField("REGION_CODE", processData.get("REGION_CODE"));
				staff.setField("AREA_CODE", processData.get("AREA_CODE"));
				staff.setField("REPORTING_STAFF_CODE", processData.get("REPORTING_STAFF_CODE"));
				staff.setField("REMARKS", processData.get("REMARKS"));
				if (staff.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
					staff.sendInterfaceOutwardMessage();
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			staff.close();
		}

	}
}
