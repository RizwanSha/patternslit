package patterns.config.framework.bo.emp;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.web.BackOfficeFormatUtils;

public class estaffrolebrnBO extends GenericBO {

	TBADynaSQL estaffrolebrn = null;

	public void init() {
		estaffrolebrn = new TBADynaSQL("STAFFROLEBRNHIST", true);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("STAFF_CODE") + RegularConstants.PK_SEPARATOR+ BackOfficeFormatUtils.getDate(processData.getDate("EFF_DATE"), BackOfficeConstants.TBA_DATE_FORMAT);
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("STAFF_CODE"));
		estaffrolebrn.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		estaffrolebrn.setField("STAFF_CODE", processData.get("STAFF_CODE"));
		estaffrolebrn.setField("EFF_DATE", processData.getDate("EFF_DATE"));
		dataExists = estaffrolebrn.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				estaffrolebrn.setNew(true);
				estaffrolebrn.setField("STAFF_ROLE_CODE", processData.get("STAFF_ROLE_CODE"));
				estaffrolebrn.setField("BRANCH_CODE", processData.get("BRANCH_CODE"));
				estaffrolebrn.setField("ENABLED", processData.get("ENABLED"));
				estaffrolebrn.setField("REMARKS", processData.get("REMARKS"));
				if (estaffrolebrn.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			estaffrolebrn.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				estaffrolebrn.setNew(false);
				estaffrolebrn.setField("STAFF_ROLE_CODE", processData.get("STAFF_ROLE_CODE"));
				estaffrolebrn.setField("BRANCH_CODE", processData.get("BRANCH_CODE"));
				estaffrolebrn.setField("ENABLED", processData.get("ENABLED"));
				estaffrolebrn.setField("REMARKS", processData.get("REMARKS"));
				if (estaffrolebrn.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			estaffrolebrn.close();
		}
	}

}
