package patterns.config.framework.bo.access;

import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;

public class mpkicertauthBO extends GenericBO {

	TBADynaSQL mpkicertauth = null;

	public mpkicertauthBO() {
	}

	public void init() {
		mpkicertauth = new TBADynaSQL("PKICERTAUTH", true);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("CODE");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("DESCRIPTION"));
		mpkicertauth.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		mpkicertauth.setField("CODE", processData.get("CODE"));
		dataExists = mpkicertauth.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				mpkicertauth.setNew(true);
				mpkicertauth.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				mpkicertauth.setField("HIERARCHY_LEVEL", processData.get("HIERARCHY_LEVEL"));
				mpkicertauth.setField("PARENT_CODE", processData.get("PARENT_CODE"));
				mpkicertauth.setField("CERT_AUTH_HTTP_URL", processData.get("CERT_AUTH_HTTP_URL"));
				mpkicertauth.setField("CERT_AUTH_LDAP_URL", processData.get("CERT_AUTH_LDAP_URL"));
				mpkicertauth.setField("OCSP_VERIFY_AVLBL", processData.get("OCSP_VERIFY_AVLBL"));
				mpkicertauth.setField("OCSP_HTTP_URL", processData.get("OCSP_HTTP_URL"));
				mpkicertauth.setField("CRL_HTTP_URL", processData.get("CRL_HTTP_URL"));
				mpkicertauth.setField("HTTP_REFRESH_INTERVAL", processData.get("HTTP_REFRESH_INTERVAL"));
				mpkicertauth.setField("CRL_LDAP_URL", processData.get("CRL_LDAP_URL"));
				mpkicertauth.setField("LDAP_REFRESH_INTERVAL", processData.get("LDAP_REFRESH_INTERVAL"));
				mpkicertauth.setField("ENABLED", processData.get("ENABLED"));
				mpkicertauth.setField("REMARKS", processData.get("REMARKS"));
				if (mpkicertauth.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				mpkicertauth.setNew(false);
				mpkicertauth.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				mpkicertauth.setField("HIERARCHY_LEVEL", processData.get("HIERARCHY_LEVEL"));
				mpkicertauth.setField("PARENT_CODE", processData.get("PARENT_CODE"));
				mpkicertauth.setField("CERT_AUTH_HTTP_URL", processData.get("CERT_AUTH_HTTP_URL"));
				mpkicertauth.setField("CERT_AUTH_LDAP_URL", processData.get("CERT_AUTH_LDAP_URL"));
				mpkicertauth.setField("OCSP_VERIFY_AVLBL", processData.get("OCSP_VERIFY_AVLBL"));
				mpkicertauth.setField("OCSP_HTTP_URL", processData.get("OCSP_HTTP_URL"));
				mpkicertauth.setField("CRL_HTTP_URL", processData.get("CRL_HTTP_URL"));
				mpkicertauth.setField("HTTP_REFRESH_INTERVAL", processData.get("HTTP_REFRESH_INTERVAL"));
				mpkicertauth.setField("CRL_LDAP_URL", processData.get("CRL_LDAP_URL"));
				mpkicertauth.setField("LDAP_REFRESH_INTERVAL", processData.get("LDAP_REFRESH_INTERVAL"));
				mpkicertauth.setField("ENABLED", processData.get("ENABLED"));
				mpkicertauth.setField("REMARKS", processData.get("REMARKS"));
				if (mpkicertauth.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mpkicertauth.close();
		}
	}
}