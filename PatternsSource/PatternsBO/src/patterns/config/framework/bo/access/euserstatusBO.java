package patterns.config.framework.bo.access;

import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;

public class euserstatusBO extends GenericBO {

	public euserstatusBO() {
	}

	TBADynaSQL muserstatus = null;

	public void init() {
		muserstatus = new TBADynaSQL("USERSSTATUS", true);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("USER_ID");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("USER_ID"));
		muserstatus.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		muserstatus.setField("USER_ID", processData.get("USER_ID"));
		dataExists = muserstatus.loadByKeyFields();
	}

	@Override
	protected void add() throws TBAFrameworkException {
		processResult.setProcessStatus(TBAProcessStatus.FAILURE);
		processResult.setErrorCode(BackOfficeErrorCodes.INVALID_ACTION);
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				muserstatus.setNew(false);
				muserstatus.setField("CHANGE_DATETIME", tbaContext.getSystemTime());
				muserstatus.setField("STATUS", processData.get("STATUS"));
				muserstatus.setField("REMARKS", processData.get("REMARKS"));
				if (muserstatus.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			muserstatus.close();
		}
	}
}