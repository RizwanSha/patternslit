package patterns.config.framework.bo.access;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import patterns.config.framework.bo.InventoryBO;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.service.DTDObject;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.ContentManager;

public class AddressIO extends InventoryBO {
	private TBADynaSQL addressinventoryhist = null;
	private TBADynaSQL addressinventory = null;
	private TBADynaSQL addressInventoryTemp = null;
	private TBADynaSQL phoneInventoryTemp = null;

	public AddressIO(DBContext context) {
		super.initialize(context);
	}
	public DTObject create(DTObject input) {
		addressinventoryhist = new TBADynaSQL("ADDRINVHIST", false);
		input.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		try {
			long serial = 1;
			long invNumber = 0;
			String sourceKey = RegularConstants.EMPTY_STRING;
			if (!input.containsKey("INV_NUMBER")) {
				invNumber = getInventoryNumber(RegularConstants.ADDR_INV_NUM);
			} else {
				invNumber = Long.parseLong(input.get("INV_NUMBER"));
			}
			sourceKey = input.get("ORG_ID") + RegularConstants.PK_SEPARATOR + invNumber;
			input.set("SOURCE_KEY", sourceKey);
			input.set("PGM_ID", "ADDRINVHIST");
			serial = addressinventoryhist.getTBAReferenceSerial(input);

			addressinventoryhist.setNew(true);
			addressinventoryhist.setField("AIH_ORG_ID", input.get("ORG_ID"));
			addressinventoryhist.setField("AIH_ADDR_INV_NO", String.valueOf(invNumber));
			addressinventoryhist.setField("AIH_SL", String.valueOf(serial));
			addressinventoryhist.setField("AIH_ENTRY_DATE", getSystemDate(input.get("ORG_ID")));
			addressinventoryhist.setField("AIH_ADDR1", input.get("AIH_ADDR1"));
			addressinventoryhist.setField("AIH_ADDR2", input.get("AIH_ADDR2"));
			addressinventoryhist.setField("AIH_ADDR3", input.get("AIH_ADDR3"));
			addressinventoryhist.setField("AIH_ADDR4", input.get("AIH_ADDR4"));
			addressinventoryhist.setField("AIH_ADDR5", input.get("AIH_ADDR5"));
			addressinventoryhist.setField("AIH_LOC", input.get("AIH_LOC"));
			addressinventoryhist.setField("AIH_COUNTRY", input.get("AIH_COUNTRY"));
			addressinventoryhist.setField("AIH_PINCODE", input.get("AIH_PINCODE"));
			
			if (addressinventoryhist.save()) {
				input.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				input.set("ADDR_INV_NUM", String.valueOf(invNumber));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			addressinventoryhist.close();
		}
		return input;
	}

	@Override
	public DTObject moveToHistory(DTObject input) {
		addressinventoryhist = new TBADynaSQL("ADDRINVHIST", false);
		input.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		try {
			long serial = 1;
			long invNumber = 0;
			String sourceKey = RegularConstants.EMPTY_STRING;
			if (!input.containsKey("INV_NUMBER")) {
				invNumber = getInventoryNumber(RegularConstants.ADDR_INV_NUM);
			} else {
				invNumber = Long.parseLong(input.get("INV_NUMBER"));
			}
			sourceKey = input.get("ORG_ID") + RegularConstants.PK_SEPARATOR + invNumber;
			input.set("SOURCE_KEY", sourceKey);
			input.set("PGM_ID", "ADDRINVHIST");
			serial = addressinventoryhist.getTBAReferenceSerial(input);

			addressinventoryhist.setNew(true);
			addressinventoryhist.setField("AIH_ORG_ID", input.get("ORG_ID"));
			addressinventoryhist.setField("AIH_ADDR_INV_NO", String.valueOf(invNumber));
			addressinventoryhist.setField("AIH_SL", String.valueOf(serial));
			addressinventoryhist.setField("AIH_ENTRY_DATE", getSystemDate(input.get("ORG_ID")));
			addressinventoryhist.setField("AIH_ADDR1", input.get("AIH_ADDR1"));
			addressinventoryhist.setField("AIH_ADDR2", input.get("AIH_ADDR2"));
			addressinventoryhist.setField("AIH_ADDR3", input.get("AIH_ADDR3"));
			addressinventoryhist.setField("AIH_ADDR4", input.get("AIH_ADDR4"));
			addressinventoryhist.setField("AIH_ADDR5", input.get("AIH_ADDR5"));
			addressinventoryhist.setField("AIH_LOC", input.get("AIH_LOC"));
			addressinventoryhist.setField("AIH_PINCODE", input.get("AIH_PINCODE"));
			addressinventoryhist.setField("AIH_COUNTRY", input.get("AIH_COUNTRY"));
			addressinventoryhist.setField("AIH_OFF_ADDR", input.get("AIH_OFF_ADDR"));
			if (addressinventoryhist.save()) {
				input.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				input.set("ADDR_INV_NUM", String.valueOf(invNumber));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			addressinventoryhist.close();
		}
		return input;
	}

	public DTObject moveToInventory(DTObject input) {
		addressinventory = new TBADynaSQL("ADDRINV", false);
		input.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		try {
			long invNumber = 0;
			if (!input.containsKey("AI_ADDR_INV_NO")) {
				invNumber = getInventoryNumber(RegularConstants.ADDR_INV_NUM);
			} else {
				invNumber = Long.parseLong(input.get("AI_ADDR_INV_NO"));
			}

			addressinventory.setNew(true);
			addressinventory.setField("AI_ORG_ID", input.get("AI_ORG_ID"));
			addressinventory.setField("AI_ADDR_INV_NO", String.valueOf(invNumber));
			addressinventory.setField("AI_ADDR1", input.get("AI_ADDR1"));
			addressinventory.setField("AI_ADDR2", input.get("AI_ADDR2"));
			addressinventory.setField("AI_ADDR3", input.get("AI_ADDR3"));
			addressinventory.setField("AI_ADDR4", input.get("AI_ADDR4"));
			addressinventory.setField("AI_ADDR5", input.get("AI_ADDR5"));
			addressinventory.setField("AI_LOC", input.get("AI_LOC"));
			addressinventory.setField("AI_PINCODE", input.get("AI_PINCODE"));
			addressinventory.setField("AI_COUNTRY", input.get("AI_COUNTRY"));
			addressinventory.setField("AI_OFF_ADDR", input.get("AI_OFF_ADDR"));
			if (addressinventory.save()) {
				input.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				input.set("ADDR_INV_NUM", String.valueOf(invNumber));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			addressinventory.close();
		}
		return input;
	}

	public DTObject moveToHistoryAll(List<DTObject> input) {
		addressinventoryhist = new TBADynaSQL("ADDRINVHIST", false);
		DTObject result = new DTObject();
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DTObject _input = new DTObject();
		long serial = 0;
		try {
			int saved = 0;
			for (DTObject mInput : input) {
				String sourceKey = mInput.get("ORG_ID") + RegularConstants.PK_SEPARATOR + mInput.get("INV_NUMBER");
				_input.reset();
				_input.set("SOURCE_KEY", sourceKey);
				_input.set("PGM_ID", "ADDRINVHIST");
				serial = addressinventoryhist.getTBAReferenceSerial(_input);

				addressinventoryhist.setNew(true);
				addressinventoryhist.setField("AIH_ORG_ID", mInput.get("ORG_ID"));
				addressinventoryhist.setField("AIH_ADDR_INV_NO", mInput.get("INV_NUMBER"));
				addressinventoryhist.setField("AIH_SL", String.valueOf(serial));
				addressinventoryhist.setField("AIH_ENTRY_DATE", getSystemDate(mInput.get("ORG_ID")));
				addressinventoryhist.setField("AIH_ADDR1", mInput.get("AIH_ADDR1"));
				addressinventoryhist.setField("AIH_ADDR2", mInput.get("AIH_ADDR2"));
				addressinventoryhist.setField("AIH_ADDR3", mInput.get("AIH_ADDR3"));
				addressinventoryhist.setField("AIH_ADDR4", mInput.get("AIH_ADDR4"));
				addressinventoryhist.setField("AIH_ADDR5", mInput.get("AIH_ADDR5"));
				addressinventoryhist.setField("AIH_LOC", mInput.get("AIH_LOC"));
				addressinventoryhist.setField("AIH_PINCODE", mInput.get("AIH_PINCODE"));
				addressinventoryhist.setField("AIH_COUNTRY", mInput.get("AIH_COUNTRY"));
				addressinventoryhist.setField("AIH_OFF_ADDR", mInput.get("AIH_OFF_ADDR"));
				if (addressinventoryhist.save()) {
					saved++;
				}
			}
			if (saved == input.size()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			addressinventoryhist.close();
		}
		return result;
	}

	public DTObject moveToInventoryAll(List<DTObject> input) {
		addressinventory = new TBADynaSQL("ADDRINV", false);
		DTObject result = new DTObject();
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		try {
			int saved = 0;
			for (DTObject mInput : input) {
				addressinventory.setNew(true);
				addressinventory.setField("AI_ORG_ID", mInput.get("ORG_ID"));
				addressinventory.setField("AI_ADDR_INV_NO", mInput.get("INV_NUMBER"));
				addressinventory.setField("AI_ADDR1", mInput.get("AI_ADDR1"));
				addressinventory.setField("AI_ADDR2", mInput.get("AI_ADDR2"));
				addressinventory.setField("AI_ADDR3", mInput.get("AI_ADDR3"));
				addressinventory.setField("AI_ADDR4", mInput.get("AI_ADDR4"));
				addressinventory.setField("AI_ADDR5", mInput.get("AI_ADDR5"));
				addressinventory.setField("AI_LOC", mInput.get("AI_LOC"));
				addressinventory.setField("AI_PINCODE", mInput.get("AI_PINCODE"));
				addressinventory.setField("AI_COUNTRY", mInput.get("AI_COUNTRY"));
				addressinventory.setField("AI_OFF_ADDR", mInput.get("AI_OFF_ADDR"));
				if (addressinventory.save()) {
					saved++;
				}
			}
			if (saved == input.size()) {
				result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			addressinventory.close();
		}
		return result;
	}

	@Override
	public DTObject update(DTObject input) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DTObject delete(DTObject input) {
		// TODO Auto-generated method stub
		return null;
	}

	public DTObject createInventory(DTObject input) {
		input.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil dbutil = getDbContext().createUtilInstance();
		try {
			String memberNumber = input.get("MEMBER_NUMBER");
			String entityCode = input.get("ORG_ID");
			String addrType = input.get("TYPE");
			String sql = "INSERT INTO ADDRINV SELECT AIH_ORG_ID,AIH_ADDR_INV_NO, AIH_ADDR1,AIH_ADDR2,AIH_ADDR3,AIH_ADDR4,AIH_ADDR5,AIH_LOC,AIH_PINCODE,AIH_COUNTRY,NULL,NULL,AIH_OFF_ADDR FROM ADDRINVHIST WHERE AIH_ORG_ID=? AND AIH_ADDR_INV_NO=(SELECT MI_INVN_NO FROM MEMINV WHERE MI_ORG_ID=? AND MI_SMN=? AND MI_TYPE='A' AND MI_SUB_TYPE=? AND MI_REMOVED_ON IS NULL) AND AIH_SL='1' ";
			dbutil.reset();
			dbutil.setSql(sql);
			dbutil.setString(1, entityCode);
			dbutil.setString(2, entityCode);
			dbutil.setString(3, memberNumber);
			dbutil.setString(4, addrType);
			int count = dbutil.executeUpdate();
			if (count == 1) {
				input.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			dbutil.reset();
		}
		return input;
	}

	public DTObject createInventoryDetail(DTObject input) {
		input.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		DBUtil dbutil = getDbContext().createUtilInstance();
		try {
			String memberNumber = input.get("MEMBER_NUMBER");
			String entityCode = input.get("ORG_ID");
			String addrType = input.get("TYPE");
			String invNum = RegularConstants.EMPTY_STRING;
			String sql = "SELECT MI_INVN_NO FROM MEMINV WHERE MI_ORG_ID=? AND MI_SMN=? AND MI_TYPE='A' AND MI_SUB_TYPE=? AND MI_REMOVED_ON IS NULL";
			dbutil.reset();
			dbutil.setSql(sql);
			dbutil.setString(1, entityCode);
			dbutil.setString(2, memberNumber);
			dbutil.setString(3, addrType);
			ResultSet rset = dbutil.executeQuery();
			if (rset.next()) {
				invNum = rset.getString(1);
			} else {
				input.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
				return input;
			}
			sql = "INSERT INTO ADDRINVTELREL SELECT ?,?,@ROWNUM:=@ROWNUM+1 ROWNUM,MI_INVN_NO,'0' FROM MEMINV,(SELECT @rownum:=0) X WHERE MI_ORG_ID=? AND MI_SMN=? AND MI_TYPE = 'T' AND MI_SUB_TYPE=? AND MI_REMOVED_ON IS NULL";
			dbutil.reset();
			dbutil.setSql(sql);
			dbutil.setString(1, entityCode);
			dbutil.setString(2, invNum);
			dbutil.setString(3, entityCode);
			dbutil.setString(4, memberNumber);
			dbutil.setString(5, addrType);
			int count = dbutil.executeUpdate();
			input.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			dbutil.reset();
		}
		return input;
	}

	public Map<String, DTObject> moveToAddrTempList(Map<String, DTObject> input) {
		addressInventoryTemp = new TBADynaSQL("ADDRINVTEMP", false);
		DTObject result = new DTObject();
		Map<String, DTObject> resultList = new HashMap<String, DTObject>();
		result.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		try {
			for (Map.Entry<String, DTObject> invMap : input.entrySet()) {
				result = new DTObject();
				String key = invMap.getKey();
				DTObject mInput = invMap.getValue();

				long serial = 1;
				long invNumber = 0;
				String sourceKey = RegularConstants.EMPTY_STRING;
				if (!mInput.containsKey("AI_ADDR_INV_NO")) {
					invNumber = getInventoryNumber(RegularConstants.ADDR_INV_NUM);
				} else {
					invNumber = Long.parseLong(mInput.get("AI_ADDR_INV_NO"));
				}
				sourceKey = mInput.get("AI_ORG_ID") + RegularConstants.PK_SEPARATOR + invNumber;
				mInput.set("SOURCE_KEY", sourceKey);
				mInput.set("PGM_ID", "ADDRINVTEMP");
				serial = addressInventoryTemp.getTBAReferenceSerial(mInput);

				addressInventoryTemp.setNew(true);
				addressInventoryTemp.setField("AIT_ORG_ID", mInput.get("AI_ORG_ID"));
				addressInventoryTemp.setField("AIT_ADDR_INV_NO", String.valueOf(invNumber));
				addressInventoryTemp.setField("AIT_SL", String.valueOf(serial));
				addressInventoryTemp.setField("AIT_ADDR1", mInput.get("AI_ADDR1"));
				addressInventoryTemp.setField("AIT_ADDR2", mInput.get("AI_ADDR2"));
				addressInventoryTemp.setField("AIT_ADDR3", mInput.get("AI_ADDR3"));
				addressInventoryTemp.setField("AIT_ADDR4", mInput.get("AI_ADDR4"));
				addressInventoryTemp.setField("AIT_ADDR5", mInput.get("AI_ADDR5"));
				addressInventoryTemp.setField("AIT_LOC", mInput.get("AI_LOC"));
				addressInventoryTemp.setField("AIT_PINCODE", mInput.get("AI_PINCODE"));
				addressInventoryTemp.setField("AIT_COUNTRY", mInput.get("AI_COUNTRY"));
				addressInventoryTemp.setField("AIT_OFF_ADDR", mInput.get("AI_OFF_ADDR"));
				if (addressInventoryTemp.save()) {
					result.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
					result.set("AIT_ADDR_INV_NO", String.valueOf(invNumber));
					result.set("AIT_SL", String.valueOf(serial));
					if (mInput.containsKey("TELINV"))
						result.setDTDObject("PHONE", moveToPhoneTemp(mInput.getDTDObject("TELINV")));
				}
				resultList.put(key, result);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			addressInventoryTemp.close();
		}
		return resultList;
	}

	public DTObject moveToAddrTemp(DTObject input) {
		addressInventoryTemp = new TBADynaSQL("ADDRINVTEMP", false);
		input.set(ContentManager.RESULT, ContentManager.DATA_UNAVAILABLE);
		try {
			long serial = 1;
			long invNumber = 0;
			String sourceKey = RegularConstants.EMPTY_STRING;
			if (!input.containsKey("AI_ADDR_INV_NO")) {
				invNumber = getInventoryNumber(RegularConstants.ADDR_INV_NUM);
			} else {
				invNumber = Long.parseLong(input.get("AI_ADDR_INV_NO"));
			}
			sourceKey = input.get("AI_ORG_ID") + RegularConstants.PK_SEPARATOR + invNumber;
			input.set("SOURCE_KEY", sourceKey);
			input.set("PGM_ID", "ADDRINVTEMP");
			serial = addressInventoryTemp.getTBAReferenceSerial(input);

			addressInventoryTemp.setNew(true);
			addressInventoryTemp.setField("AIT_ORG_ID", input.get("AI_ORG_ID"));
			addressInventoryTemp.setField("AIT_ADDR_INV_NO", String.valueOf(invNumber));
			addressInventoryTemp.setField("AIT_SL", String.valueOf(serial));
			addressInventoryTemp.setField("AIT_ADDR1", input.get("AI_ADDR1"));
			addressInventoryTemp.setField("AIT_ADDR2", input.get("AI_ADDR2"));
			addressInventoryTemp.setField("AIT_ADDR3", input.get("AI_ADDR3"));
			addressInventoryTemp.setField("AIT_ADDR4", input.get("AI_ADDR4"));
			addressInventoryTemp.setField("AIT_ADDR5", input.get("AI_ADDR5"));
			addressInventoryTemp.setField("AIT_LOC", input.get("AI_LOC"));
			addressInventoryTemp.setField("AIT_PINCODE", input.get("AI_PINCODE"));
			addressInventoryTemp.setField("AIT_COUNTRY", input.get("AI_COUNTRY"));
			addressInventoryTemp.setField("AIT_OFF_ADDR", input.get("AI_OFF_ADDR"));
			if (addressInventoryTemp.save()) {
				input.set(ContentManager.RESULT, ContentManager.DATA_AVAILABLE);
				input.set("AIT_ADDR_INV_NO", String.valueOf(invNumber));
				input.set("AIT_SL", String.valueOf(serial));
				if (input.containsKey("TELINV"))
					input.setDTDObject("PHONE", moveToPhoneTemp(input.getDTDObject("TELINV")));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			addressInventoryTemp.close();
		}
		return input;
	}

	public DTDObject moveToPhoneTemp(DTDObject input) {
		phoneInventoryTemp = new TBADynaSQL("TELINVTEMP", false);
		DTDObject latestPhoneList = new DTDObject();
		try {
			long serial = 1;
			long invNumber = 0;
			String sourceKey = RegularConstants.EMPTY_STRING;
			DTObject inputDTO = new DTObject();

			latestPhoneList.addColumn(0, "MACD_SL");
			latestPhoneList.addColumn(1, "MACD_INV_NUM");
			latestPhoneList.addColumn(2, "MACD_INV_NUM_SL");
			latestPhoneList.addColumn(3, "MACD_OPR_TYPE");

			int colCount = input.getColCount();

			for (int i = 0; i < input.getRowCount(); i++) {
				invNumber = Long.parseLong(input.getValue(i, 1));
				sourceKey = Long.parseLong(input.getValue(i, 0)) + RegularConstants.PK_SEPARATOR + invNumber;
				inputDTO.set("SOURCE_KEY", sourceKey);
				inputDTO.set("PGM_ID", "TELINVTEMP");
				serial = phoneInventoryTemp.getTBAReferenceSerial(inputDTO);

				phoneInventoryTemp.setNew(true);
				phoneInventoryTemp.setField("TELT_ORG_ID", input.getValue(i, 0));
				phoneInventoryTemp.setField("TELT_INVNTRY_NO", String.valueOf(invNumber));
				phoneInventoryTemp.setField("TELT_SL", String.valueOf(serial));
				phoneInventoryTemp.setField("TELT_TYPE", input.getValue(i, 2));
				phoneInventoryTemp.setField("TELT_LL_CNTRY_CODE", input.getValue(i, 3));
				phoneInventoryTemp.setField("TELT_LL_LOC_STD_CODE", input.getValue(i, 4));
				phoneInventoryTemp.setField("TELT_TEL_NO", input.getValue(i, 5));
				phoneInventoryTemp.setField("TELT_MOBILE_NO", input.getValue(i, 6));
				phoneInventoryTemp.setField("TELT_OFFICE_NO", input.getValue(i, 7));

				latestPhoneList.addRow();
				latestPhoneList.setValue(i, 0, String.valueOf(i + 1));
				latestPhoneList.setValue(i, 1, String.valueOf(invNumber));
				latestPhoneList.setValue(i, 2, String.valueOf(serial));
				if (colCount != 9)
					latestPhoneList.setValue(i, 3, "N");
				else
					latestPhoneList.setValue(i, 3, input.getValue(i, 8));
				if (!phoneInventoryTemp.save()) {
					System.out.println("ERROR : " + invNumber);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			phoneInventoryTemp.close();
		}
		return latestPhoneList;
	}
}
