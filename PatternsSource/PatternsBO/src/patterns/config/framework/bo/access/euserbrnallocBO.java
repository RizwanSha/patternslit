package patterns.config.framework.bo.access;

import java.util.Date;

import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;

public class euserbrnallocBO extends GenericBO {

	public euserbrnallocBO() {
	}

	TBADynaSQL euserbrnalloc = null;

	public void init() {
		euserbrnalloc = new TBADynaSQL("USERSBRNALLOC", true);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("USER_ID") + RegularConstants.PK_SEPARATOR + BackOfficeFormatUtils.getDate((Date) processData.getObject("EFFT_DATE"), BackOfficeConstants.TBA_DATE_FORMAT);
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("USER_ID"));
		euserbrnalloc.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		euserbrnalloc.setField("USER_ID", processData.get("USER_ID"));
		euserbrnalloc.setField("EFFT_DATE", processData.getObject("EFFT_DATE"));
		dataExists = euserbrnalloc.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				euserbrnalloc.setNew(true);
				euserbrnalloc.setField("BRANCH_CODE", processData.get("BRANCH_CODE"));
				euserbrnalloc.setField("REMARKS", processData.get("REMARKS"));
				if (euserbrnalloc.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				euserbrnalloc.setNew(false);
				euserbrnalloc.setField("BRANCH_CODE", processData.get("BRANCH_CODE"));
				euserbrnalloc.setField("REMARKS", processData.get("REMARKS"));
				if (euserbrnalloc.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			euserbrnalloc.close();
		}
	}
}