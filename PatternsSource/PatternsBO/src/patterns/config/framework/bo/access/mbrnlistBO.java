package patterns.config.framework.bo.access;

import patterns.config.framework.database.BindParameterType;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTDObject;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;

public class mbrnlistBO extends GenericBO {

	TBADynaSQL mbrnlist = null;
	TBADynaSQL mbrnlistdtl = null;

	public mbrnlistBO() {
	}

	public void init() {
		mbrnlist = new TBADynaSQL("BRANCHLIST", true);
		mbrnlistdtl = new TBADynaSQL("BRANCHLISTDTL", false);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("CODE");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("DESCRIPTION"));
		mbrnlist.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		mbrnlist.setField("CODE", processData.get("CODE"));
		dataExists = mbrnlist.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				mbrnlist.setNew(true);
				mbrnlist.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				mbrnlist.setField("ENABLED", processData.get("ENABLED"));
				mbrnlist.setField("REMARKS", processData.get("REMARKS"));
				if (mbrnlist.save()) {
					mbrnlistdtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
					mbrnlistdtl.setField("CODE", processData.get("CODE"));
					DTDObject detailData = processData.getDTDObject("BRANCHLISTDTL");
					mbrnlistdtl.setNew(true);
					for (int i = 0; i < detailData.getRowCount(); ++i) {
						mbrnlistdtl.setField("SL", Integer.valueOf(i + 1));
						mbrnlistdtl.setField("BRANCH_CODE", detailData.getValue(i, 1));
						if (!mbrnlistdtl.save()) {
							processResult.setProcessStatus(TBAProcessStatus.FAILURE);
							processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
							return;
						}
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());

		} finally {
			mbrnlist.close();
			mbrnlistdtl.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				mbrnlist.setNew(false);
				mbrnlist.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				mbrnlist.setField("ENABLED", processData.get("ENABLED"));
				mbrnlist.setField("REMARKS", processData.get("REMARKS"));
				if (mbrnlist.save()) {
					String[] bankbranchlistdtlFields = { "ENTITY_CODE", "CODE", "SL" };
					BindParameterType[] bankbranchlistdtlFieldTypes = { BindParameterType.VARCHAR, BindParameterType.VARCHAR, BindParameterType.INTEGER };
					mbrnlistdtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
					mbrnlistdtl.setField("CODE", processData.get("CODE"));
					mbrnlistdtl.deleteByFieldsAudit(bankbranchlistdtlFields, bankbranchlistdtlFieldTypes);
					DTDObject detailData = processData.getDTDObject("BRANCHLISTDTL");
					mbrnlistdtl.setNew(true);
					for (int i = 0; i < detailData.getRowCount(); ++i) {
						mbrnlistdtl.setField("SL", Integer.valueOf(i + 1));
						mbrnlistdtl.setField("BRANCH_CODE", detailData.getValue(i, 1));
						if (!mbrnlistdtl.save()) {
							processResult.setProcessStatus(TBAProcessStatus.FAILURE);
							processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
							return;
						}
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}

		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());

		} finally {
			mbrnlist.close();
			mbrnlistdtl.close();
		}
	}
}