package patterns.config.framework.bo.access;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;

public class mroleBO extends GenericBO {

	TBADynaSQL mrole = null;

	public mroleBO() {
	}

	public void init() {
		mrole = new TBADynaSQL("ROLES", true);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("CODE");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("CODE"));
		mrole.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		mrole.setField("CODE", processData.get("CODE"));
		dataExists = mrole.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				mrole.setNew(true);
				mrole.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				mrole.setField("ROLE_TYPE", processData.get("ROLE_TYPE"));
				mrole.setField("ENABLED", processData.get("ENABLED"));
				mrole.setField("REMARKS", processData.get("REMARKS"));
				if (mrole.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
					mrole.sendInterfaceOutwardMessage();
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}

			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mrole.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				mrole.setNew(false);
				mrole.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				mrole.setField("ENABLED", processData.get("ENABLED"));
				mrole.setField("REMARKS", processData.get("REMARKS"));
				if (mrole.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
					mrole.sendInterfaceOutwardMessage();
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mrole.close();
		}
	}
}