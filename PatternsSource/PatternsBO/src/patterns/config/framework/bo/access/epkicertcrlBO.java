package patterns.config.framework.bo.access;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;

public class epkicertcrlBO extends GenericBO {

	TBADynaSQL epkicertcrl = null;

	public epkicertcrlBO() {
	}

	public void init() {
		epkicertcrl = new TBADynaSQL("PKICERTAUTHCRL", true);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("ROOT_CA_CODE");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("ROOT_CA_CODE"));
		epkicertcrl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		epkicertcrl.setField("ROOT_CA_CODE", processData.getObject("ROOT_CA_CODE"));
		dataExists = epkicertcrl.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				epkicertcrl.setField("CHANGE_DATETIME", tbaContext.getSystemTime());
				epkicertcrl.setField("ENABLED", processData.get("ENABLED"));
				epkicertcrl.setField("REMARKS", processData.get("REMARKS"));
				epkicertcrl.setField("INV_NUM", processData.get("INV_NUM"));
				epkicertcrl.setField("ROOT_CA_CODE", processData.get("ROOT_CA_CODE"));
				if (epkicertcrl.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				epkicertcrl.setNew(false);
				epkicertcrl.setField("CHANGE_DATETIME", tbaContext.getSystemTime());
				epkicertcrl.setField("ENABLED", processData.get("ENABLED"));
				epkicertcrl.setField("REMARKS", processData.get("REMARKS"));
				epkicertcrl.setField("INV_NUM", processData.get("INV_NUM"));
				epkicertcrl.setField("ROOT_CA_CODE", processData.get("ROOT_CA_CODE"));
				if (epkicertcrl.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}

			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		}
	}

	protected boolean checkInventoryNum() throws TBAFrameworkException {
		DBUtil util = getDbContext().createUtilInstance();
		try {
			String sql = "SELECT CERT_SERIAL FROM PKICERTINVENTORY WHERE ENTITY_CODE = ? AND CERT_INV_NUM=?";
			util.reset();
			util.setMode(DBUtil.PREPARED);
			util.setSql(sql);
			util.setString(1, processData.get("ENTITY_CODE"));
			util.setString(2, processData.get("INV_NUM"));
			ResultSet rset = util.executeQuery();
			String serial = RegularConstants.EMPTY_STRING;
			if (rset.next()) {
				serial = rset.getString(1);
			}
			String objectID = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + RegularConstants.INIT_ACCESS_TYPE_INTERNAL + RegularConstants.PK_SEPARATOR + processData.get("USER_ID");
			String objectValue = processData.get("ROOT_CA_CODE") + RegularConstants.PK_SEPARATOR + serial;
			sql = "DELETE FROM DUPCHKGW WHERE ENTITY_CODE = ? AND OBJECT_CLASS = ? AND OBJECT_ATTR = ? AND OBJECT_VALUE = ?";
			util.reset();
			util.setMode(DBUtil.PREPARED);
			util.setSql(sql);
			util.setString(1, processData.get("ENTITY_CODE"));
			util.setString(2, "CERTIFICATE");
			util.setString(3, "ID");
			util.setString(4, objectValue);
			util.executeUpdate();
			sql = "INSERT INTO DUPCHKGW (ENTITY_CODE,OBJECT_CLASS,OBJECT_ATTR,OBJECT_VALUE,OBJECT_ID) VALUES (?,?,?,?,?)";
			util.reset();
			util.setMode(DBUtil.PREPARED);
			util.setSql(sql);
			util.setString(1, processData.get("ENTITY_CODE"));
			util.setString(2, "CERTIFICATE");
			util.setString(3, "ID");
			util.setString(4, objectValue);
			util.setString(5, objectID);
			util.executeUpdate();
			sql = "UPDATE PKICERTINVENTORY SET ROOT_CA_CODE=?,DEPLOYMENT_CONTEXT=? ,USER_ID=? WHERE ENTITY_CODE = ? AND CERT_INV_NUM =?";
			util.reset();
			util.setSql(sql);
			util.setString(1, processData.get("ROOT_CA_CODE"));
			util.setString(2, RegularConstants.INIT_ACCESS_TYPE_EXTERNAL);
			util.setString(3, processData.get("USER_ID"));
			util.setString(4, processData.get("ENTITY_CODE"));
			util.setString(5, processData.get("INV_NUM"));
			util.executeUpdate();
			sql = "SELECT  1 FROM PKICERTINVENTORY I WHERE I.ENTITY_CODE=? AND I.CERT_SERIAL=? AND I.ROOT_CA_CODE=? AND I.CERT_IN_USE='1' AND NOT (I.DEPLOYMENT_CONTEXT='E' AND I.USER_ID =?)";
			util.reset();
			util.setSql(sql);
			util.setString(1, processData.get("ENTITY_CODE"));
			util.setString(2, serial);
			util.setString(3, processData.get("ROOT_CA_CODE"));
			util.setString(4, processData.get("USER_ID"));
			rset = util.executeQuery();
			if (rset.next()) {
				return false;
			} else {
				sql = "UPDATE PKICERTINVENTORY SET CERT_IN_USE=? WHERE ENTITY_CODE = ? AND CERT_INV_NUM =?";
				util.reset();
				util.setSql(sql);
				util.setString(1, RegularConstants.COLUMN_ENABLE);
				util.setString(2, processData.get("ENTITY_CODE"));
				util.setString(3, processData.get("INV_NUM"));
				util.executeUpdate();
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			util.reset();
		}
	}

	protected boolean checkCertificateInventoryNum() throws TBAFrameworkException {
		DBUtil util = getDbContext().createUtilInstance();
		try {
			String sql = "SELECT INV_NUM FROM USERSTFA WHERE ENTITY_CODE = ? AND USER_ID=? AND EFFT_DATE=TO_DATE(?,'" + BackOfficeConstants.TBA_DATE_FORMAT + "')";
			util.reset();
			util.setMode(DBUtil.PREPARED);
			util.setSql(sql);
			util.setString(1, processData.get("ENTITY_CODE"));
			util.setString(2, processData.get("USER_ID"));
			util.setString(3, BackOfficeFormatUtils.getDate((Date) processData.getObject("EFFT_DATE"), BackOfficeConstants.TBA_DATE_FORMAT));
			ResultSet rset = util.executeQuery();
			String inventoryNumber = RegularConstants.NULL;
			if (rset.next()) {
				inventoryNumber = rset.getString(1);
			}
			if (inventoryNumber != null && !inventoryNumber.equals(processData.get("INV_NUM"))) {
				sql = "SELECT 1 FROM PKIENCDATALOG WHERE ENTITY_CODE = ? AND CERT_INV_NUM=?";
				util.reset();
				util.setMode(DBUtil.PREPARED);
				util.setSql(sql);
				util.setString(1, processData.get("ENTITY_CODE"));
				util.setString(2, inventoryNumber);
				rset = util.executeQuery();
				if (rset.next()) {
					return false;
				} else {
					return true;
				}
			} else {
				return true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
}