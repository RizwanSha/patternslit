package patterns.config.framework.bo.access;

import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;

public class ebkannouncedtlsBO extends GenericBO {

	TBADynaSQL eannounce = null;

	public ebkannouncedtlsBO() {
	}

	public void init() {
		eannounce = new TBADynaSQL("ANNOUNCEMENTS", true);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("ANNOUNCE_ID");
		tbaContext.setTbaPrimarykey(primaryKey);

		tbaContext.setDisplayDetails(processData.get("ANNOUNCE_ID"));
		eannounce.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		eannounce.setField("ANNOUNCE_ID", processData.get("ANNOUNCE_ID"));
		dataExists = eannounce.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				eannounce.setNew(true);
				eannounce.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
				eannounce.setField("ANNOUNCE_ID", processData.get("ANNOUNCE_ID"));
				eannounce.setField("ANNOUNCE_START_DT", processData.getObject("ANNOUNCE_START_DT"));
				eannounce.setField("ANNOUNCE_END_DT", processData.getObject("ANNOUNCE_END_DT"));
				eannounce.setField("ANNOUNCE_TITLE", processData.get("ANNOUNCE_TITLE"));
				eannounce.setField("EXTERNAL_LINK", processData.get("EXTERNAL_LINK"));
				eannounce.setField("ANNOUNCE_URL", processData.get("ANNOUNCE_URL"));
				eannounce.setField("ANNOUNCE_URL_TITLE", processData.get("ANNOUNCE_URL_TITLE"));
				eannounce.setField("ANNOUNCE_VISIBLITY", processData.get("ANNOUNCE_VISIBLITY"));
				eannounce.setField("ANNOUNCE_DISABLED", processData.get("ANNOUNCE_DISABLED"));
				eannounce.setField("ANNOUNCE_DISABLED_DATE", RegularConstants.NULL);
				eannounce.setField("ANNOUNCE_USER_ID", processData.get("ANNOUNCE_USER_ID"));
				eannounce.setField("ANNOUNCE_ROLE_TYPE", processData.get("ANNOUNCE_ROLE_TYPE"));
				eannounce.setField("ANNOUNCE_CUSTOMER_CODE", processData.get("ANNOUNCE_CUSTOMER_CODE"));
				eannounce.setField("ANNOUNCE_BRANCH_CODE", processData.get("ANNOUNCE_BRANCH_CODE"));
				eannounce.setField("REMARKS", processData.get("REMARKS"));
				if (eannounce.save()) {
					processResult.setAdditionalInfo("Generated Announcement ID : " + processData.get("ANNOUNCE_ID"));
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());

		} finally {
			eannounce.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				eannounce.setNew(false);
				eannounce.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
				eannounce.setField("ANNOUNCE_ID", processData.get("ANNOUNCE_ID"));
				eannounce.setField("ANNOUNCE_START_DT", processData.getObject("ANNOUNCE_START_DT"));
				eannounce.setField("ANNOUNCE_END_DT", processData.getObject("ANNOUNCE_END_DT"));
				eannounce.setField("ANNOUNCE_TITLE", processData.get("ANNOUNCE_TITLE"));
				eannounce.setField("EXTERNAL_LINK", processData.get("EXTERNAL_LINK"));
				eannounce.setField("ANNOUNCE_URL", processData.get("ANNOUNCE_URL"));
				eannounce.setField("ANNOUNCE_URL_TITLE", processData.get("ANNOUNCE_URL_TITLE"));
				eannounce.setField("ANNOUNCE_DISABLED", processData.get("ANNOUNCE_DISABLED"));
				if (processData.get("ANNOUNCE_DISABLED").equals(RegularConstants.COLUMN_ENABLE)) {
					eannounce.setField("ANNOUNCE_DISABLED_DATE", tbaContext.getSystemDate());
				} else {
					eannounce.setField("ANNOUNCE_DISABLED_DATE", RegularConstants.NULL);
				}
				eannounce.setField("REMARKS", processData.get("REMARKS"));
				if (eannounce.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}

		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			eannounce.close();
		}
	}
}