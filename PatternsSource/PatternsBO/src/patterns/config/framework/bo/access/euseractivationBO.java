package patterns.config.framework.bo.access;

import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;

public class euseractivationBO extends GenericBO {

	TBADynaSQL euseractivation = null;

	public euseractivationBO() {
	}

	public void init() {
		euseractivation = new TBADynaSQL("USERSACTIVATION", true);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("USER_ID");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("USER_ID"));
		euseractivation.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		euseractivation.setField("USER_ID", processData.get("USER_ID"));
		dataExists = euseractivation.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		processResult.setProcessStatus(TBAProcessStatus.FAILURE);
		processResult.setErrorCode(BackOfficeErrorCodes.INVALID_ACTION);
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				euseractivation.setNew(false);
				euseractivation.setField("CHANGE_DATETIME", tbaContext.getSystemTime());
				euseractivation.setField("INTIMATION_MODE", processData.get("INTIMATION_MODE"));
				euseractivation.setField("INTIMATION_REF_NO", processData.get("INTIMATION_REF_NO"));
				euseractivation.setField("INTIMATION_DATE", processData.getObject("INTIMATION_DATE"));
				euseractivation.setField("REMARKS", processData.get("REMARKS"));
				if (euseractivation.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			euseractivation.close();
		}
	}
}