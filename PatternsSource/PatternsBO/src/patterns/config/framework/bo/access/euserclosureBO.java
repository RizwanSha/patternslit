package patterns.config.framework.bo.access;

import java.util.Date;

import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;

public class euserclosureBO extends GenericBO {

	TBADynaSQL euserclosure = null;
	int daySerial = 0;

	public euserclosureBO() {
	}

	public void init() {
		euserclosure = new TBADynaSQL("USERCLOSURE", true);
		if (processData.containsKey("DAY_SL"))
			daySerial = Integer.parseInt(processData.get("DAY_SL"));
		else
			daySerial = (int) euserclosure.getTBAReferenceKey(processData);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + BackOfficeFormatUtils.getDate((Date) processData.getObject("ENTRY_DATE"), BackOfficeConstants.TBA_DATE_FORMAT) + RegularConstants.PK_SEPARATOR + daySerial;
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.getObject("DATE_OF_RELIVING").toString());
		euserclosure.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		euserclosure.setField("ENTRY_DATE", processData.getObject("ENTRY_DATE"));
		euserclosure.setField("DAY_SL", String.valueOf(daySerial));
		dataExists = euserclosure.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				euserclosure.setNew(true);
				euserclosure.setField("USER_ID", processData.get("USER_ID"));
				euserclosure.setField("DATE_OF_RELIVING", processData.getObject("DATE_OF_RELIVING"));
				euserclosure.setField("REMARKS", processData.get("REMARKS"));
				if (euserclosure.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			euserclosure.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		processResult.setProcessStatus(TBAProcessStatus.FAILURE);
		processResult.setErrorCode(BackOfficeErrorCodes.INVALID_ACTION);
	}
}