package patterns.config.framework.bo.access;

import java.util.Date;

import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;

public class ifoldermaintenanceBO extends GenericBO {

	TBADynaSQL ifolder = null;

	public ifoldermaintenanceBO() {
	}

	public void init() throws TBAFrameworkException {
		ifolder = new TBADynaSQL("SYSTEMFOLDERS", true);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + BackOfficeFormatUtils.getDate((Date) processData.getObject("EFFT_DATE"), BackOfficeConstants.TBA_DATE_FORMAT);
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.getObject("EFFT_DATE").toString());
		ifolder.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		ifolder.setField("EFFT_DATE", processData.getObject("EFFT_DATE"));
		dataExists = ifolder.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				ifolder.setField("REMARKS", processData.getObject("REMARKS"));
				ifolder.setNew(true);
				ifolder.setField("REPORT_GENERATION_PATH", processData.get("REPORT_GENERATION_PATH"));
				ifolder.setField("PORTAL_FILE_UPLOAD_PATH", processData.get("PORTAL_FILE_UPLOAD_PATH"));
				ifolder.setField("TEMP_FILE_UPLOAD_PATH", processData.get("TEMP_FILE_UPLOAD_PATH"));
				ifolder.setField("CBS_FILE_TRANSFER_PATH", processData.get("CBS_FILE_TRANSFER_PATH"));
				if (ifolder.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());

		} finally {
			ifolder.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				ifolder.setField("REMARKS", processData.getObject("REMARKS"));
				ifolder.setNew(false);
				ifolder.setField("REPORT_GENERATION_PATH", processData.get("REPORT_GENERATION_PATH"));
				ifolder.setField("PORTAL_FILE_UPLOAD_PATH", processData.get("PORTAL_FILE_UPLOAD_PATH"));
				ifolder.setField("TEMP_FILE_UPLOAD_PATH", processData.get("TEMP_FILE_UPLOAD_PATH"));
				ifolder.setField("CBS_FILE_TRANSFER_PATH", processData.get("CBS_FILE_TRANSFER_PATH"));
				if (ifolder.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			ifolder.close();
		}
	}
}