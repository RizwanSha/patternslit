package patterns.config.framework.bo.access;

import java.util.Date;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.BindParameterType;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTDObject;
import patterns.config.framework.web.BackOfficeFormatUtils;

public class erolepgmallocBO extends GenericBO {

	TBADynaSQL erolepgmallochist = null;
	TBADynaSQL erolepgmallochistdtl = null;

	public erolepgmallocBO() {
	}

	public void init() {
		erolepgmallochist = new TBADynaSQL("ROLEPGMALLOCHIST", true);
		erolepgmallochistdtl = new TBADynaSQL("ROLEPGMALLOCHISTDTL", false);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("ROLE_CODE") + RegularConstants.PK_SEPARATOR + processData.get("MODULE_ID") + RegularConstants.PK_SEPARATOR + BackOfficeFormatUtils.getDate((Date) processData.getObject("EFFT_DATE"), BackOfficeConstants.TBA_DATE_FORMAT);
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("ROLE_CODE"));
		erolepgmallochist.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		erolepgmallochist.setField("ROLE_CODE", processData.get("ROLE_CODE"));
		erolepgmallochist.setField("MODULE_ID", processData.get("MODULE_ID"));
		erolepgmallochist.setField("EFFT_DATE", processData.getObject("EFFT_DATE"));
		dataExists = erolepgmallochist.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				erolepgmallochist.setField("REMARKS", processData.getObject("REMARKS"));
				erolepgmallochist.setNew(true);
				if (erolepgmallochist.save()) {
					erolepgmallochistdtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
					erolepgmallochistdtl.setField("ROLE_CODE", processData.get("ROLE_CODE"));
					erolepgmallochistdtl.setField("MODULE_ID", processData.get("MODULE_ID"));
					erolepgmallochistdtl.setField("EFFT_DATE", processData.getObject("EFFT_DATE"));
					DTDObject detailData = processData.getDTDObject("ROLEPGMALLOCHISTDTL");
					erolepgmallochistdtl.setNew(true);
					int count = 1;
					for (int i = 0; i < detailData.getRowCount(); ++i) {
						if (detailData.getValue(i, 3).equalsIgnoreCase(RegularConstants.COLUMN_ENABLE)) {
							erolepgmallochistdtl.setField("SL", count);
							erolepgmallochistdtl.setField("PGM_ID", detailData.getValue(i, 1));
							erolepgmallochistdtl.setField("ADD_ALLOWED", detailData.getValue(i, 4));
							erolepgmallochistdtl.setField("MODIFY_ALLOWED", detailData.getValue(i, 5));
							erolepgmallochistdtl.setField("VIEW_ALLOWED", detailData.getValue(i, 6));
							erolepgmallochistdtl.setField("AUTHORIZE_ALLOWED", detailData.getValue(i, 7));
							erolepgmallochistdtl.setField("REJECTION_ALLOWED", detailData.getValue(i, 8));
							erolepgmallochistdtl.setField("PROCESS_ALLOWED", detailData.getValue(i, 9));
							if (!erolepgmallochistdtl.save()) {
								processResult.setProcessStatus(TBAProcessStatus.FAILURE);
								processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
								return;
							}
							count++;
						}
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());

		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				erolepgmallochist.setField("REMARKS", processData.getObject("REMARKS"));
				erolepgmallochist.setNew(false);
				if (erolepgmallochist.save()) {
					String[] erolepgmallochistdtlFields = { "ENTITY_CODE", "ROLE_CODE", "MODULE_ID", "EFFT_DATE", "SL" };
					BindParameterType[] erolepgmallochistdtlFieldTypes = { BindParameterType.VARCHAR, BindParameterType.VARCHAR, BindParameterType.VARCHAR, BindParameterType.DATE, BindParameterType.INTEGER };
					erolepgmallochistdtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
					erolepgmallochistdtl.setField("ROLE_CODE", processData.get("ROLE_CODE"));
					erolepgmallochistdtl.setField("MODULE_ID", processData.get("MODULE_ID"));
					erolepgmallochistdtl.setField("EFFT_DATE", processData.getObject("EFFT_DATE"));
					erolepgmallochistdtl.deleteByFieldsAudit(erolepgmallochistdtlFields, erolepgmallochistdtlFieldTypes);
					DTDObject detailData = processData.getDTDObject("ROLEPGMALLOCHISTDTL");
					erolepgmallochistdtl.setNew(true);
					int count = 1;
					for (int i = 0; i < detailData.getRowCount(); ++i) {
						if (detailData.getValue(i, 3).equalsIgnoreCase(RegularConstants.COLUMN_ENABLE)) {
							erolepgmallochistdtl.setField("SL", count);
							erolepgmallochistdtl.setField("PGM_ID", detailData.getValue(i, 1));
							erolepgmallochistdtl.setField("ADD_ALLOWED", detailData.getValue(i, 4));
							erolepgmallochistdtl.setField("MODIFY_ALLOWED", detailData.getValue(i, 5));
							erolepgmallochistdtl.setField("VIEW_ALLOWED", detailData.getValue(i, 6));
							erolepgmallochistdtl.setField("AUTHORIZE_ALLOWED", detailData.getValue(i, 7));
							erolepgmallochistdtl.setField("REJECTION_ALLOWED", detailData.getValue(i, 8));
							erolepgmallochistdtl.setField("PROCESS_ALLOWED", detailData.getValue(i, 9));
							if (!erolepgmallochistdtl.save()) {
								processResult.setProcessStatus(TBAProcessStatus.FAILURE);
								processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
								return;
							}
							count++;
						}
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		}
	}
}