package patterns.config.framework.bo.access;

import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;

public class mcourseBO extends GenericBO {

	TBADynaSQL mcourse = null;

	public mcourseBO() {
	}

	public void init() {
		mcourse = new TBADynaSQL("COURSE", true);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("CODE");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("DESCRIPTION"));
		mcourse.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		mcourse.setField("CODE", processData.get("CODE"));
		dataExists = mcourse.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				mcourse.setNew(true);
				mcourse.setField("COURSE_TITLE", processData.get("COURSE_TITLE"));
				mcourse.setField("COURSE_DESCN", processData.get("COURSE_DESCN"));
				mcourse.setField("ACTIVE", processData.get("ACTIVE"));
				mcourse.setField("COURSE_POSITION", processData.get("COURSE_POSITION"));
				mcourse.setField("REMARKS", processData.get("REMARKS"));
				if (mcourse.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}

			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mcourse.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				mcourse.setNew(false);
				mcourse.setField("COURSE_TITLE", processData.get("COURSE_TITLE"));
				mcourse.setField("COURSE_DESCN", processData.get("COURSE_DESCN"));
				mcourse.setField("ACTIVE", processData.get("ACTIVE"));
				mcourse.setField("COURSE_POSITION", processData.get("COURSE_POSITION"));
				mcourse.setField("REMARKS", processData.get("REMARKS"));
				if (mcourse.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mcourse.close();
		}
	}
}