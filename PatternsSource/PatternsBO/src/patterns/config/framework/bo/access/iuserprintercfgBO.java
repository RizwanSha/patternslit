package patterns.config.framework.bo.access;

import java.util.Date;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.BindParameterType;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTDObject;
import patterns.config.framework.web.BackOfficeFormatUtils;

/**
 * 
 * 
 * @version 1.0
 * @author Vivekananda Reddy
 * @since 19-Feb-2015 <br>
 *        <b>Modified History</b> <BR>
 *        <U><B> Sl.No Modified Date Author Modified Changes Version </B></U> <br>
 * 
 * 
 */

public class iuserprintercfgBO extends GenericBO {
	TBADynaSQL iuserprintercfghist = null;
	TBADynaSQL iuserprintercfghistdtl = null;
	TBADynaSQL iuserprintercfghistpgm = null;

	public void init() {
		iuserprintercfghist = new TBADynaSQL("USERPRINTERCFGHIST", true);
		iuserprintercfghistdtl = new TBADynaSQL("USERPRINTERCFGHISTDTL", false);
		iuserprintercfghistpgm = new TBADynaSQL("USERPRINTERCFGHISTPGM", false);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("USER_ID") + RegularConstants.PK_SEPARATOR + BackOfficeFormatUtils.getDate((Date) processData.getDate("EFFT_DATE"), BackOfficeConstants.TBA_DATE_FORMAT);
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("USER_ID"));
		iuserprintercfghist.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		iuserprintercfghist.setField("USER_ID", processData.get("USER_ID"));
		iuserprintercfghist.setField("EFFT_DATE", processData.getDate("EFFT_DATE"));
		dataExists = iuserprintercfghist.loadByKeyFields();

	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				iuserprintercfghist.setNew(true);
				if (iuserprintercfghist.save()) {
					iuserprintercfghistdtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
					iuserprintercfghistdtl.setField("USER_ID", processData.get("USER_ID"));
					iuserprintercfghistdtl.setField("EFFT_DATE", processData.getDate("EFFT_DATE"));
					DTDObject detailData = processData.getDTDObject("IUSERPRINTERCFGHISTDTL");
					iuserprintercfghistdtl.setNew(true);
					for (int i = 0; i < detailData.getRowCount(); ++i) {
						iuserprintercfghistdtl.setField("SL", Integer.valueOf(i + 1).toString());
						iuserprintercfghistdtl.setField("PRNTYPE_ID", detailData.getValue(i, 1));
						iuserprintercfghistdtl.setField("PRINTER_ID", detailData.getValue(i, 3));
						if (detailData.getValue(i, 5).equalsIgnoreCase("Yes")) {
							iuserprintercfghistdtl.setField("DEFAULT_PRINTER", "1");
						} else {
							iuserprintercfghistdtl.setField("DEFAULT_PRINTER", "0");
						}
						if (!iuserprintercfghistdtl.save()) {
							processResult.setProcessStatus(TBAProcessStatus.FAILURE);
							processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
							return;
						}
					}
					iuserprintercfghistpgm.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
					iuserprintercfghistpgm.setField("USER_ID", processData.get("USER_ID"));
					iuserprintercfghistpgm.setField("EFFT_DATE", processData.getDate("EFFT_DATE"));
					DTDObject detailDatapgm = processData.getDTDObject("USERPRINTERCFGHISTPGM");
					iuserprintercfghistpgm.setNew(true);
					for (int i = 0; i < detailDatapgm.getRowCount(); ++i) {
						iuserprintercfghistpgm.setField("SL", Integer.valueOf(i + 1).toString());
						iuserprintercfghistpgm.setField("PGM_ID", detailDatapgm.getValue(i, 1));
						iuserprintercfghistpgm.setField("PRINTER_ID", detailDatapgm.getValue(i, 3));
						if (!iuserprintercfghistpgm.save()) {
							processResult.setProcessStatus(TBAProcessStatus.FAILURE);
							processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
							return;
						}
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			iuserprintercfghist.close();
			iuserprintercfghistdtl.close();
			iuserprintercfghistpgm.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				iuserprintercfghist.setNew(false);
				if (iuserprintercfghist.save()) {
					String[] iuserprintercfghistdtlFields = { "ENTITY_CODE", "USER_ID", "EFFT_DATE", "SL" };
					BindParameterType[] iuserprintercfghistdtlFieldTypes = { BindParameterType.VARCHAR, BindParameterType.VARCHAR, BindParameterType.DATE, BindParameterType.INTEGER };
					iuserprintercfghistdtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
					iuserprintercfghistdtl.setField("USER_ID", processData.get("USER_ID"));
					iuserprintercfghistdtl.setField("EFFT_DATE", processData.getDate("EFFT_DATE"));
					iuserprintercfghistdtl.deleteByFieldsAudit(iuserprintercfghistdtlFields, iuserprintercfghistdtlFieldTypes);
					DTDObject detailData = processData.getDTDObject("IUSERPRINTERCFGHISTDTL");
					iuserprintercfghistdtl.setNew(true);
					for (int i = 0; i < detailData.getRowCount(); ++i) {
						iuserprintercfghistdtl.setField("SL", Integer.valueOf(i + 1).toString());
						iuserprintercfghistdtl.setField("PRNTYPE_ID", detailData.getValue(i, 1));
						iuserprintercfghistdtl.setField("PRINTER_ID", detailData.getValue(i, 3));
						if (detailData.getValue(i, 5).equalsIgnoreCase("Yes")) {
							iuserprintercfghistdtl.setField("DEFAULT_PRINTER", "1");
						} else {
							iuserprintercfghistdtl.setField("DEFAULT_PRINTER", "0");
						}
						if (!iuserprintercfghistdtl.save()) {
							processResult.setProcessStatus(TBAProcessStatus.FAILURE);
							processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
							return;
						}
					}
					String[] iuserprintercfghistpgmFields = { "ENTITY_CODE", "USER_ID", "EFFT_DATE", "SL" };
					BindParameterType[] iuserprintercfghistpgmFieldTypes = { BindParameterType.VARCHAR, BindParameterType.VARCHAR, BindParameterType.DATE, BindParameterType.INTEGER };
					iuserprintercfghistpgm.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
					iuserprintercfghistpgm.setField("USER_ID", processData.get("USER_ID"));
					iuserprintercfghistpgm.setField("EFFT_DATE", processData.getDate("EFFT_DATE"));
					iuserprintercfghistpgm.deleteByFieldsAudit(iuserprintercfghistpgmFields, iuserprintercfghistpgmFieldTypes);
					DTDObject detailDatapgm = processData.getDTDObject("USERPRINTERCFGHISTPGM");
					iuserprintercfghistpgm.setNew(true);
					for (int i = 0; i < detailDatapgm.getRowCount(); ++i) {
						iuserprintercfghistpgm.setField("SL", Integer.valueOf(i + 1).toString());
						iuserprintercfghistpgm.setField("PGM_ID", detailDatapgm.getValue(i, 1));
						iuserprintercfghistpgm.setField("PRINTER_ID", detailDatapgm.getValue(i, 3));
						if (!iuserprintercfghistpgm.save()) {
							processResult.setProcessStatus(TBAProcessStatus.FAILURE);
							processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
							return;
						}
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		}

	}

}
