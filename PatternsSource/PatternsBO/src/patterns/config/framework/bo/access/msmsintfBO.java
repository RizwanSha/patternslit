/*
 *
 Author :Chandrakala P
 Created Date : 10-April-2016
 Spec Reference : HMS-PSD-Access-msmsintf
 Modification History
 ----------------------------------------------------------------------
 Sl.No		Modified Date	Author			Modified Changes	Version
 ----------------------------------------------------------------------
 
 */

package patterns.config.framework.bo.access;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;

public class msmsintfBO extends GenericBO {

	TBADynaSQL msmsintf = null;

	public msmsintfBO() {
	}

	public void init() {
		msmsintf = new TBADynaSQL("SMSINTF", true);
		primaryKey =processData.get("SMS_CODE");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("SMS_DESCN"));
		msmsintf.setField("SMS_CODE", processData.get("SMS_CODE"));
		dataExists = msmsintf.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				msmsintf.setNew(true);
				msmsintf.setField("SMS_DESCN", processData.get("SMS_DESCN"));
				msmsintf.setField("SMS_SEND_ALLOWED", processData.get("SMS_SEND_ALLOWED"));
				msmsintf.setField("SMS_RECV_ALLOWED", processData.get("SMS_RECV_ALLOWED"));
				msmsintf.setField("ENABLED", processData.get("ENABLED"));
				msmsintf.setField("REMARKS", processData.get("REMARKS"));
				if (msmsintf.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			msmsintf.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				msmsintf.setNew(false);
				msmsintf.setField("SMS_DESCN", processData.get("SMS_DESCN"));
				msmsintf.setField("SMS_SEND_ALLOWED", processData.get("SMS_SEND_ALLOWED"));
				msmsintf.setField("SMS_RECV_ALLOWED", processData.get("SMS_RECV_ALLOWED"));
				msmsintf.setField("ENABLED", processData.get("ENABLED"));
				msmsintf.setField("REMARKS", processData.get("REMARKS"));
				if (msmsintf.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			msmsintf.close();
		}
	}
}