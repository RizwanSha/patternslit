package patterns.config.framework.bo.access;

import java.sql.Types;

import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.process.TBAProcessResult;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTDObject;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.ServiceBO;

public class ecurrloggedusersBO extends ServiceBO {

	public ecurrloggedusersBO() {
	}

	public void init() {
	}

	public TBAProcessResult processRequest() {
		DTDObject detailData = processData.getDTDObject("USERSFORCEDLOGOUT");
		DBUtil util = getDbContext().createUtilInstance();
		try {
			for (int i = 0; i < detailData.getRowCount(); ++i) {
				if (detailData.getValue(i, 0).equalsIgnoreCase(RegularConstants.COLUMN_ENABLE)) {
					String sqlQuery1 = "{CALL SP_USER_FORCE_LOGOUT(?,?,TO_DATETIME(?,'"+BackOfficeConstants.TBA_XML_DATETIME_FORMAT+"'),?,?,?,?)}";
					util.setMode(DBUtil.CALLABLE);
					util.setSql(sqlQuery1);
					util.setString(1, processData.get("P_ENTITY_CODE"));
					util.setString(2, detailData.getValue(i, 1));
					util.setString(3, detailData.getValue(i, 2));
					util.setString(4, processData.get("P_SESSION_ID"));
					util.setString(5, RegularConstants.FORCED_LOGOUT);
					util.setString(6, processData.get("P_ADMIN_USER_ID"));
					util.registerOutParameter(7, Types.VARCHAR);
					util.execute();
					String errorStatus = util.getString(7);
					if (errorStatus.equalsIgnoreCase("S")) {
						processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
						processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
					} else {
						processResult.setProcessStatus(TBAProcessStatus.FAILURE);
						processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
						break;
					}
				}
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
		} finally {
			util.reset();
		}
		return processResult;
	}
}