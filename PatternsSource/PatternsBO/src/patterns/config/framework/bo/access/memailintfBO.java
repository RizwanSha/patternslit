/*
 *
 Author :AjayKhanna A
 Created Date : 06-April-2016
 Spec Reference : HMS-PSD-Access-Memailintf
 Modification History
 ----------------------------------------------------------------------
 Sl.No		Modified Date	Author			Modified Changes	Version
 ----------------------------------------------------------------------
 
 */

package patterns.config.framework.bo.access;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;

public class memailintfBO extends GenericBO {

	TBADynaSQL memailintf = null;

	public memailintfBO() {
	}

	public void init() {
		memailintf = new TBADynaSQL("EMAILINTF", true);
		primaryKey =processData.get("EMAIL_CODE");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("EMAIL_DESCN"));
		memailintf.setField("EMAIL_CODE", processData.get("EMAIL_CODE"));
		dataExists = memailintf.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				memailintf.setNew(true);
				memailintf.setField("EMAIL_DESCN", processData.get("EMAIL_DESCN"));
				memailintf.setField("EMAIL_SEND_ALLOWED", processData.get("EMAIL_SEND_ALLOWED"));
				memailintf.setField("EMAIL_RECV_ALLOWED", processData.get("EMAIL_RECV_ALLOWED"));
				memailintf.setField("ENABLED", processData.get("ENABLED"));
				memailintf.setField("REMARKS", processData.get("REMARKS"));
				if (memailintf.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			memailintf.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				memailintf.setNew(false);
				memailintf.setField("EMAIL_DESCN", processData.get("EMAIL_DESCN"));
				memailintf.setField("EMAIL_SEND_ALLOWED", processData.get("EMAIL_SEND_ALLOWED"));
				memailintf.setField("EMAIL_RECV_ALLOWED", processData.get("EMAIL_RECV_ALLOWED"));
				memailintf.setField("ENABLED", processData.get("ENABLED"));
				memailintf.setField("REMARKS", processData.get("REMARKS"));
				if (memailintf.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			memailintf.close();
		}
	}
}