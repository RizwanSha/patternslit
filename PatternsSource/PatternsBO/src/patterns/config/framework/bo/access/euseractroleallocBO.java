package patterns.config.framework.bo.access;

import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;

public class euseractroleallocBO extends GenericBO {

	TBADynaSQL useractrolealloc = null;

	public euseractroleallocBO() {
	}

	public void init() {
		useractrolealloc = new TBADynaSQL("USERSACTROLEALLOC", true);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("USER_ID") + RegularConstants.PK_SEPARATOR + processData.get("ROLE_CODE");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("USER_ID"));
		useractrolealloc.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		useractrolealloc.setField("USER_ID", processData.get("USER_ID"));
		useractrolealloc.setField("ROLE_CODE", processData.get("ROLE_CODE"));
		dataExists = useractrolealloc.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				useractrolealloc.setNew(true);
				useractrolealloc.setField("CHANGE_DATETIME",tbaContext.getSystemDate());;
				useractrolealloc.setField("ENABLED", processData.get("ENABLED"));
				useractrolealloc.setField("FROM_DATE", processData.getObject("FROM_DATE"));
				useractrolealloc.setField("UPTO_DATE", processData.getObject("UPTO_DATE"));
				if (useractrolealloc.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}

			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			useractrolealloc.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				useractrolealloc.setNew(false);
				useractrolealloc.setField("CHANGE_DATETIME", processData.getObject("CHANGE_DATETIME"));
				useractrolealloc.setField("ENABLED", processData.get("ENABLED"));
				useractrolealloc.setField("FROM_DATE", processData.getObject("FROM_DATE"));
				useractrolealloc.setField("UPTO_DATE", processData.getObject("UPTO_DATE"));
				if (useractrolealloc.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);

				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			useractrolealloc.close();
		}
	}
}