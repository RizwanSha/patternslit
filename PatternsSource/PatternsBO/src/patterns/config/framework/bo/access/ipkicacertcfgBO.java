package patterns.config.framework.bo.access;

import java.sql.ResultSet;
import java.util.Date;

import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;

public class ipkicacertcfgBO extends GenericBO {

	TBADynaSQL ipkicacertcfg = null;

	public ipkicacertcfgBO() {
	}

	public void init() {
		ipkicacertcfg = new TBADynaSQL("PKICACERTCFG", true);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("CA_CODE") + RegularConstants.PK_SEPARATOR + BackOfficeFormatUtils.getDate((Date) processData.getObject("EFFT_DATE"), BackOfficeConstants.TBA_DATE_FORMAT);
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("CA_CODE"));
		ipkicacertcfg.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		ipkicacertcfg.setField("CA_CODE", processData.get("CA_CODE"));
		ipkicacertcfg.setField("EFFT_DATE", processData.getObject("EFFT_DATE"));
		dataExists = ipkicacertcfg.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				if (checkInventoryNum()) {
					ipkicacertcfg.setNew(true);
					ipkicacertcfg.setField("REVOKED", processData.get("REVOKED"));
					ipkicacertcfg.setField("INV_NUM", processData.get("INV_NUM"));
					ipkicacertcfg.setField("REMARKS", processData.get("REMARKS"));
					if (ipkicacertcfg.save()) {
						processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
						processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
					} else {
						processResult.setProcessStatus(TBAProcessStatus.FAILURE);
						processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
					}
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			ipkicacertcfg.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				if (checkInventoryNum()) {
					ipkicacertcfg.setNew(false);
					ipkicacertcfg.setField("REVOKED", processData.get("REVOKED"));
					ipkicacertcfg.setField("INV_NUM", processData.get("INV_NUM"));
					ipkicacertcfg.setField("REMARKS", processData.get("REMARKS"));
					if (ipkicacertcfg.save()) {
						processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
						processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
					} else {
						processResult.setProcessStatus(TBAProcessStatus.FAILURE);
						processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
					}
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			ipkicacertcfg.close();
		}
	}

	protected boolean checkInventoryNum() throws TBAFrameworkException {
		DBUtil util = getDbContext().createUtilInstance();
		try {
			String sql = "SELECT CERT_SERIAL FROM PKICERTINVENTORY WHERE ENTITY_CODE = ? AND CERT_INV_NUM=?";
			util.reset();
			util.setMode(DBUtil.PREPARED);
			util.setSql(sql);
			util.setString(1, processData.get("ENTITY_CODE"));
			util.setString(2, processData.get("INV_NUM"));
			ResultSet rset = util.executeQuery();
			String serial = RegularConstants.EMPTY_STRING;
			if (rset.next()) {
				serial = rset.getString(1);
			}
			String objectID = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("CA_CODE");
			String objectValue = processData.get("CA_CODE") + RegularConstants.PK_SEPARATOR + serial;
			sql = "DELETE FROM DUPCHKGW WHERE ENTITY_CODE = ? AND OBJECT_CLASS = ? AND OBJECT_ATTR = ? AND OBJECT_VALUE = ?";
			util.reset();
			util.setMode(DBUtil.PREPARED);
			util.setSql(sql);
			util.setString(1, processData.get("ENTITY_CODE"));
			util.setString(2, "CERTIFICATE");
			util.setString(3, "ID");
			util.setString(4, objectValue);
			util.executeUpdate();
			sql = "INSERT INTO DUPCHKGW (ENTITY_CODE,OBJECT_CLASS,OBJECT_ATTR,OBJECT_VALUE,OBJECT_ID) VALUES (?,?,?,?,?)";
			util.reset();
			util.setMode(DBUtil.PREPARED);
			util.setSql(sql);
			util.setString(1, processData.get("ENTITY_CODE"));
			util.setString(2, "CERTIFICATE");
			util.setString(3, "ID");
			util.setString(4, objectValue);
			util.setString(5, objectID);
			util.executeUpdate();
			util.reset();
			sql = "UPDATE PKICERTINVENTORY SET CERT_IN_USE=? WHERE ENTITY_CODE = ? AND CERT_INV_NUM =?";
			util.setSql(sql);
			util.setString(1, RegularConstants.COLUMN_ENABLE);
			util.setString(2, processData.get("ENTITY_CODE"));
			util.setString(3, processData.get("INV_NUM"));
			util.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			util.reset();
		}
		return true;
	}
}