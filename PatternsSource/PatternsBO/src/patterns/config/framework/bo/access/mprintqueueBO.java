/*
 *
 Author : A Aswaq
 Created Date : 18-Feb-2015
 Spec Reference : HMS-PSD-ACC-034-mprintqueue
 Modification History
 ----------------------------------------------------------------------
 Sl.No		Modified Date	Author			Modified Changes	Version
 ----------------------------------------------------------------------
 
 */

package patterns.config.framework.bo.access;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;

public class mprintqueueBO extends GenericBO {

	TBADynaSQL mprintqueue = null;

	public mprintqueueBO() {
	}

	public void init() {
		mprintqueue = new TBADynaSQL("PRNQ", true);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("PRNQ_ID");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("DESCRIPTION"));
		mprintqueue.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		mprintqueue.setField("PRNQ_ID", processData.get("PRNQ_ID"));
		dataExists = mprintqueue.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				mprintqueue.setNew(true);
				mprintqueue.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				mprintqueue.setField("PRNTYPE_ID", processData.get("PRNTYPE_ID"));
				mprintqueue.setField("BRN_CODE", processData.get("BRN_CODE"));
				mprintqueue.setField("BRNLIST_CODE", processData.get("BRNLIST_CODE"));
				mprintqueue.setField("SRC_TYPE", processData.get("SRC_TYPE"));
				mprintqueue.setField("SRC_PATH", processData.get("SRC_PATH"));
				mprintqueue.setField("ENABLED", processData.get("ENABLED"));
				mprintqueue.setField("REMARKS", processData.get("REMARKS"));
				if (mprintqueue.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mprintqueue.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				mprintqueue.setNew(false);
				mprintqueue.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				mprintqueue.setField("PRNTYPE_ID", processData.get("PRNTYPE_ID"));
				mprintqueue.setField("BRN_CODE", processData.get("BRN_CODE"));
				mprintqueue.setField("BRNLIST_CODE", processData.get("BRNLIST_CODE"));
				mprintqueue.setField("SRC_TYPE", processData.get("SRC_TYPE"));
				mprintqueue.setField("SRC_PATH", processData.get("SRC_PATH"));
				mprintqueue.setField("ENABLED", processData.get("ENABLED"));
				mprintqueue.setField("REMARKS", processData.get("REMARKS"));
					if (mprintqueue.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mprintqueue.close();
		}
	}
}