/* This program is used for maintaining print types. 
 * 
 * @version 1.0 
 * @author  Nareshkumar D
 * @since   23-Feb-2015
 * <br>
 * <b>Modified History</b>
 * <BR>
 * <U><B>
 * Sl.No		Modified Date	Author			Modified Changes		Version
 * </B></U>
 * <br>
 * 
 *  
 */

package patterns.config.framework.bo.access;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;

public class mprinttypeBO extends GenericBO {

	TBADynaSQL mprinttype = null;

	public mprinttypeBO() {
	}

	public void init() {
		mprinttype = new TBADynaSQL("PRNTYPE", true);
		primaryKey = processData.get("ENTITY_CODE")
				+ RegularConstants.PK_SEPARATOR + processData.get("PRNTYPE_ID");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("DESCRIPTION"));
		mprinttype.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		mprinttype.setField("PRNTYPE_ID", processData.get("PRNTYPE_ID"));
		dataExists = mprinttype.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				mprinttype.setNew(true);
				mprinttype.setField("DESCRIPTION",
						processData.get("DESCRIPTION"));
				mprinttype.setField("PRNT_TYPE", processData.get("PRNT_TYPE"));
				mprinttype.setField("ENABLED", processData.get("ENABLED"));
				mprinttype.setField("REMARKS", processData.get("REMARKS"));
				if (mprinttype.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult
							.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult
							.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult
						.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult
					.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mprinttype.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				mprinttype.setNew(false);
				mprinttype.setField("DESCRIPTION",
						processData.get("DESCRIPTION"));
				mprinttype.setField("PRNT_TYPE", processData.get("PRNT_TYPE"));
				mprinttype.setField("ENABLED", processData.get("ENABLED"));
				mprinttype.setField("REMARKS", processData.get("REMARKS"));
				if (mprinttype.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult
							.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult
							.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult
						.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult
					.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mprinttype.close();
		}
	}
}