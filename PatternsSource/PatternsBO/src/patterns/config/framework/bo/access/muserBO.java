package patterns.config.framework.bo.access;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;

public class muserBO extends GenericBO {
	TBADynaSQL muser = null;
	long invNumber = 0;

	public muserBO() {
	}

	public void init() {
		muser = new TBADynaSQL("USERS", true);
		primaryKey = processData.get("PARTITION_NO") + RegularConstants.PK_SEPARATOR + processData.get("USER_ID");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("USER_NAME"));
		muser.setField("PARTITION_NO", processData.get("PARTITION_NO"));
		muser.setField("USER_ID", processData.get("USER_ID"));
		dataExists = muser.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				muser.setNew(true);
				muser.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
				muser.setField("TITLE_CODE", processData.get("TITLE_CODE"));
				muser.setField("USER_NAME", processData.get("USER_NAME"));
				muser.setField("ROLE_CODE", processData.get("ROLE_CODE"));
				muser.setField("EMP_CODE", processData.get("EMP_CODE"));
				muser.setField("DATE_OF_JOINING", processData.getDate("DATE_OF_JOINING"));
				muser.setField("EMAIL_ID", processData.get("EMAIL_ID"));
				muser.setField("BRANCH_CODE", processData.get("BRANCH_CODE"));
				muser.setField("MOBILE_NUMBER", processData.get("MOBILE_NUMBER"));
				muser.setField("PWD_DELIVERY_CHOICE", processData.get("PWD_DELIVERY"));
				if (processData.containsKey("SALT")) {
					muser.setField("FIRST_PIN", processData.get("ENCRYPT_PASSWORD"));
					muser.setField("FIRST_PIN_SALT", processData.get("SALT"));
				}
				muser.setField("REMARKS", processData.get("REMARKS"));
				if (muser.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			muser.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				muser.setNew(false);
				muser.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
				muser.setField("TITLE_CODE", processData.get("TITLE_CODE"));
				muser.setField("USER_NAME", processData.get("USER_NAME"));
				muser.setField("EMP_CODE", processData.get("EMP_CODE"));
				muser.setField("DATE_OF_JOINING", processData.getDate("DATE_OF_JOINING"));
				muser.setField("EMAIL_ID", processData.get("EMAIL_ID"));
				muser.setField("BRANCH_CODE", processData.get("BRANCH_CODE"));
				muser.setField("MOBILE_NUMBER", processData.get("MOBILE_NUMBER"));
				muser.setField("PWD_DELIVERY_CHOICE", processData.get("PWD_DELIVERY"));
				muser.setField("REMARKS", processData.get("REMARKS"));
				if (muser.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			muser.close();
		}
	}
}
