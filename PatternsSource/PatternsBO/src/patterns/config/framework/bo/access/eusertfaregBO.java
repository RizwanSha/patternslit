package patterns.config.framework.bo.access;

import java.sql.ResultSet;
import java.util.Date;

import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;

public class eusertfaregBO extends GenericBO {

	TBADynaSQL eusertfareg = null;

	public eusertfaregBO() {
	}

	public void init() {
		eusertfareg = new TBADynaSQL("USERSTFA", true);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("USER_ID") + RegularConstants.PK_SEPARATOR + BackOfficeFormatUtils.getDate((Date) processData.getObject("EFFT_DATE"), BackOfficeConstants.TBA_DATE_FORMAT);
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("USER_ID"));
		eusertfareg.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		eusertfareg.setField("USER_ID", processData.get("USER_ID"));
		eusertfareg.setField("EFFT_DATE", processData.getObject("EFFT_DATE"));
		dataExists = eusertfareg.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				if (checkCertificateInventoryNum()) {
					if (checkInventoryNum()) {
						eusertfareg.setNew(true);
						eusertfareg.setField("REVOKED", processData.get("REVOKED"));
						eusertfareg.setField("ROOT_CA_CODE", processData.get("ROOT_CA_CODE"));
						eusertfareg.setField("REMARKS", processData.get("REMARKS"));
						eusertfareg.setField("INV_NUM", processData.get("INV_NUM"));
						if (eusertfareg.save()) {
							processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
							processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
						} else {
							processResult.setProcessStatus(TBAProcessStatus.FAILURE);
							processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
						}
					}
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				if (checkCertificateInventoryNum()) {
					if (checkInventoryNum()) {
						eusertfareg.setNew(false);
						eusertfareg.setField("REVOKED", processData.get("REVOKED"));
						eusertfareg.setField("ROOT_CA_CODE", processData.get("ROOT_CA_CODE"));
						eusertfareg.setField("REMARKS", processData.get("REMARKS"));
						eusertfareg.setField("INV_NUM", processData.get("INV_NUM"));
						if (eusertfareg.save()) {
							processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
							processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
						}
					} else {
						processResult.setProcessStatus(TBAProcessStatus.FAILURE);
						processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
					}
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				}
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		}
	}

	protected boolean checkInventoryNum() throws TBAFrameworkException {
		DBUtil util = getDbContext().createUtilInstance();
		try {
			String sql = "SELECT CERT_SERIAL FROM PKICERTINVENTORY WHERE ENTITY_CODE = ? AND CERT_INV_NUM=?";
			util.reset();
			util.setMode(DBUtil.PREPARED);
			util.setSql(sql);
			util.setString(1, processData.get("ENTITY_CODE"));
			util.setString(2, processData.get("INV_NUM"));
			ResultSet rset = util.executeQuery();
			String serial = RegularConstants.EMPTY_STRING;
			if (rset.next()) {
				serial = rset.getString(1);
			}
			String objectID = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + RegularConstants.INIT_ACCESS_TYPE_INTERNAL + RegularConstants.PK_SEPARATOR + processData.get("USER_ID");
			String objectValue = processData.get("ROOT_CA_CODE") + RegularConstants.PK_SEPARATOR + serial;
			sql = "DELETE FROM DUPCHKGW WHERE ENTITY_CODE = ? AND OBJECT_CLASS = ? AND OBJECT_ATTR = ? AND OBJECT_VALUE = ?";
			util.reset();
			util.setMode(DBUtil.PREPARED);
			util.setSql(sql);
			util.setString(1, processData.get("ENTITY_CODE"));
			util.setString(2, "CERTIFICATE");
			util.setString(3, "ID");
			util.setString(4, objectValue);
			util.executeUpdate();
			sql = "INSERT INTO DUPCHKGW (ENTITY_CODE,OBJECT_CLASS,OBJECT_ATTR,OBJECT_VALUE,OBJECT_ID) VALUES (?,?,?,?,?)";
			util.reset();
			util.setMode(DBUtil.PREPARED);
			util.setSql(sql);
			util.setString(1, processData.get("ENTITY_CODE"));
			util.setString(2, "CERTIFICATE");
			util.setString(3, "ID");
			util.setString(4, objectValue);
			util.setString(5, objectID);
			util.executeUpdate();
			sql = "UPDATE PKICERTINVENTORY SET ROOT_CA_CODE=? ,DEPLOYMENT_CONTEXT=?,USER_ID=? WHERE ENTITY_CODE = ? AND CERT_INV_NUM =? ";
			util.reset();
			util.setSql(sql);
			util.setString(1, processData.get("ROOT_CA_CODE"));
			util.setString(2, RegularConstants.INIT_ACCESS_TYPE_INTERNAL);
			util.setString(3, processData.get("USER_ID"));
			util.setString(4, processData.get("ENTITY_CODE"));
			util.setString(5, processData.get("INV_NUM"));
			util.executeUpdate();
			sql = "UPDATE PKICERTINVENTORY SET CERT_IN_USE=? WHERE ENTITY_CODE = ? AND CERT_INV_NUM =?";
			util.reset();
			util.setSql(sql);
			util.setString(1, RegularConstants.COLUMN_ENABLE);
			util.setString(2, processData.get("ENTITY_CODE"));
			util.setString(3, processData.get("INV_NUM"));
			util.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			util.reset();
		}
		return true;
	}

	protected boolean checkCertificateInventoryNum() throws TBAFrameworkException {
		if (processData.get("REVOKED").equals(RegularConstants.COLUMN_ENABLE)) {
			return true;
		}
		DBUtil util = getDbContext().createUtilInstance();
		boolean rejected = false;
		try {
			String sql = "DELETE FROM PKICERTINVENTORY WHERE ENTITY_CODE=? AND DEPLOYMENT_CONTEXT='I' AND USER_ID =? AND CERT_INV_NUM NOT IN (SELECT DISTINCT(INV_NUM) FROM USERSTFA WHERE ENTITY_CODE = ? and USER_ID =? AND INV_NUM <>? )";
			util.reset();
			util.setMode(DBUtil.PREPARED);
			util.setSql(sql);
			util.setString(1, processData.get("ENTITY_CODE"));
			util.setString(2, processData.get("USER_ID"));
			util.setString(3, processData.get("ENTITY_CODE"));
			util.setString(4, processData.get("USER_ID"));
			util.setString(5, processData.get("INV_NUM"));
			util.executeUpdate();
			sql = "SELECT CERT_SERIAL FROM PKICERTINVENTORY WHERE ENTITY_CODE = ? AND CERT_INV_NUM=?";
			util.reset();
			util.setMode(DBUtil.PREPARED);
			util.setSql(sql);
			util.setString(1, processData.get("ENTITY_CODE"));
			util.setString(2, processData.get("INV_NUM"));
			ResultSet rset = util.executeQuery();
			String serial = RegularConstants.EMPTY_STRING;
			if (rset.next()) {
				serial = rset.getString(1);
			}
			sql = "SELECT CERT_INV_NUM,DEPLOYMENT_CONTEXT,USER_ID FROM PKICERTINVENTORY WHERE ENTITY_CODE=? AND ROOT_CA_CODE=? AND CERT_SERIAL =? AND NOT (DEPLOYMENT_CONTEXT='I'AND USER_ID=?) AND CERT_IN_USE='1'";
			util.reset();
			util.setMode(DBUtil.PREPARED);
			util.setSql(sql);
			util.setString(1, processData.get("ENTITY_CODE"));
			util.setString(2, processData.get("ROOT_CA_CODE"));
			util.setString(3, serial);
			util.setString(4, processData.get("USER_ID"));
			rset = util.executeQuery();
			while (rset.next()) {
				String deploymentContext = rset.getString(2);
				String inventoryNumber = rset.getString(1);
				String userID = rset.getString(3);
				if (deploymentContext.equals("E")) {
					sql = "SELECT 1 FROM USERSTFA WHERE ENTITY_CODE=? AND USER_ID=? AND EFFT_DATE=(SELECT MAX(EFFT_DATE) FROM USERSTFA WHERE ENTITY_CODE=? AND USER_ID=? AND EFFT_DATE <=FN_GETCD(?)) AND INV_NUM = ?";
				} else {
					sql = "SELECT 1 FROM USERSTFA WHERE ENTITY_CODE=? AND USER_ID=? AND EFFT_DATE=(SELECT MAX(EFFT_DATE) FROM USERSTFA WHERE ENTITY_CODE=? AND USER_ID=? AND EFFT_DATE <=FN_GETCD(?)) AND INV_NUM = ?";
				}
				DBUtil util1 = getDbContext().createUtilInstance();
				util1.reset();
				util1.setMode(DBUtil.PREPARED);
				util1.setSql(sql);
				util1.setString(1, processData.get("ENTITY_CODE"));
				util1.setString(2, userID);
				util1.setString(3, processData.get("ENTITY_CODE"));
				util1.setString(4, userID);
				util1.setString(5, processData.get("ENTITY_CODE"));
				util1.setString(6, inventoryNumber);
				ResultSet rset1 = util1.executeQuery();
				if (rset1.next()) {
					rejected = true;
					processResult.setErrorCode(BackOfficeErrorCodes.CERT_ALREADY_ALLOCATED);
					break;
				} else {
					if (deploymentContext.equals("E")) {
						sql = "SELECT 1 FROM USERSTFA WHERE ENTITY_CODE = ? AND USER_ID = ? AND INV_NUM= ? ";
					} else {
						sql = "SELECT 1 FROM USERSTFA WHERE ENTITY_CODE = ? AND USER_ID = ? AND INV_NUM= ?";
					}
					DBUtil util2 = getDbContext().createUtilInstance();
					util2.reset();
					util2.setMode(DBUtil.PREPARED);
					util2.setSql(sql);
					util2.setString(1, processData.get("ENTITY_CODE"));
					util2.setString(2, userID);
					util2.setString(3, inventoryNumber);
					ResultSet rset2 = util2.executeQuery();
					if (rset2.next()) {
						sql = "SELECT COUNT(1) FROM PKIENCDATALOG WHERE ENTITY_CODE=? AND CERT_INV_NUM=?";
						DBUtil util3 = getDbContext().createUtilInstance();
						util3.reset();
						util3.setMode(DBUtil.PREPARED);
						util3.setSql(sql);
						util3.setString(1, processData.get("ENTITY_CODE"));
						util3.setString(2, inventoryNumber);
						util3.executeQuery();
						ResultSet rset3 = util3.executeQuery();
						if (rset3.next()) {
							if (rset3.getInt(1) > 0) {
								rejected = true;
								processResult.setErrorCode(BackOfficeErrorCodes.CERT_ALREADY_IN_USE);
								break;
							}
						}

					} else {
						rejected = true;
						processResult.setErrorCode(BackOfficeErrorCodes.CERT_ALREADY_ALLOCATED_TO_ANOTHER_USER);
						break;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			util.reset();
		}
		return !rejected;
	}
}
