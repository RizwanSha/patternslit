package patterns.config.framework.bo.access;

import java.util.Date;

import patterns.config.framework.database.BindParameterType;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTDObject;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;

public class eipbranchBO extends GenericBO {

	TBADynaSQL eipbranch = null;
	TBADynaSQL eipbranchdtl = null;

	public eipbranchBO() {
	}

	public void init() {
		eipbranch = new TBADynaSQL("BRNIPRESTRICT", true);
		eipbranchdtl = new TBADynaSQL("BRNIPRESTRICTDTL", false);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("BRANCH_CODE") + RegularConstants.PK_SEPARATOR + BackOfficeFormatUtils.getDate((Date) processData.getObject("EFFT_DATE"), BackOfficeConstants.TBA_DATE_FORMAT);
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("BRANCH_CODE"));
		eipbranch.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		eipbranch.setField("BRANCH_CODE", processData.get("BRANCH_CODE"));
		eipbranch.setField("EFFT_DATE", processData.getObject("EFFT_DATE"));
		dataExists = eipbranch.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				eipbranch.setNew(true);
				eipbranch.setField("ENABLED", processData.get("ENABLED"));
				eipbranch.setField("REMARKS", processData.get("REMARKS"));
				if (eipbranch.save()) {
					if (processData.get("ENABLED").equals(RegularConstants.COLUMN_ENABLE)) {
						eipbranchdtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
						eipbranchdtl.setField("BRANCH_CODE", processData.get("BRANCH_CODE"));
						eipbranchdtl.setField("EFFT_DATE", processData.getObject("EFFT_DATE"));
						DTDObject detailData = processData.getDTDObject("BRNIPRESTRICTDTL");
						eipbranchdtl.setNew(true);
						for (int i = 0; i < detailData.getRowCount(); ++i) {
							eipbranchdtl.setField("SL", Integer.valueOf(i + 1));
							eipbranchdtl.setField("IPADDRESS", detailData.getValue(i, 1));
							if (!eipbranchdtl.save()) {
								processResult.setProcessStatus(TBAProcessStatus.FAILURE);
								processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
								return;
							}
						}
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			eipbranch.close();
			eipbranchdtl.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				eipbranch.setNew(false);
				eipbranch.setField("ENABLED", processData.get("ENABLED"));
				eipbranch.setField("REMARKS", processData.get("REMARKS"));
				if (eipbranch.save()) {
					String[] eipbranchdtlFields = { "ENTITY_CODE", "BRANCH_CODE", "EFFT_DATE", "SL" };
					BindParameterType[] eipbranchdtlFieldTypes = { BindParameterType.VARCHAR, BindParameterType.VARCHAR, BindParameterType.DATE, BindParameterType.INTEGER };
					eipbranchdtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
					eipbranchdtl.setField("BRANCH_CODE", processData.get("BRANCH_CODE"));
					eipbranchdtl.setField("EFFT_DATE", processData.getObject("EFFT_DATE"));
					eipbranchdtl.deleteByFieldsAudit(eipbranchdtlFields, eipbranchdtlFieldTypes);
					if (processData.get("ENABLED").equals(RegularConstants.COLUMN_ENABLE)) {
						DTDObject detailData = processData.getDTDObject("BRNIPRESTRICTDTL");
						eipbranchdtl.setNew(true);
						for (int i = 0; i < detailData.getRowCount(); ++i) {
							eipbranchdtl.setField("SL", Integer.valueOf(i + 1));
							eipbranchdtl.setField("IPADDRESS", detailData.getValue(i, 1));
							if (!eipbranchdtl.save()) {
								processResult.setProcessStatus(TBAProcessStatus.FAILURE);
								processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
								return;
							}
						}
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			eipbranch.close();
			eipbranchdtl.close();
		}
	}
}
