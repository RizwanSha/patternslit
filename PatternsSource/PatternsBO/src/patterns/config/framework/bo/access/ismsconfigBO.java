package patterns.config.framework.bo.access;

import java.util.Date;

import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;

public class ismsconfigBO extends GenericBO {
	TBADynaSQL ismsconfig = null;

	public ismsconfigBO() {
	}

	public void init() {
		ismsconfig = new TBADynaSQL("SMSINTFCFG", true);
		primaryKey = processData.get("ENTITY_CODE")+RegularConstants.PK_SEPARATOR+processData.get("SMS_CODE") + RegularConstants.PK_SEPARATOR + BackOfficeFormatUtils.getDate((Date) processData.getObject("EFFT_DATE"), BackOfficeConstants.TBA_DATE_FORMAT);
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.getObject("EFFT_DATE").toString());
		ismsconfig.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		ismsconfig.setField("EFFT_DATE", processData.getObject("EFFT_DATE"));
		ismsconfig.setField("SMS_CODE", processData.get("SMS_CODE"));
		dataExists = ismsconfig.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				ismsconfig.setNew(true);
				ismsconfig.setField("SMSC_HTTP_ALLOWED", processData.get("SMSC_HTTP_ALLOWED"));
				ismsconfig.setField("SSL_REQUIRED", processData.get("SSL_REQUIRED"));
				ismsconfig.setField("HTTP_METHOD", processData.get("HTTP_METHOD"));
				ismsconfig.setField("SERVER_URL", processData.get("SERVER_URL"));
				ismsconfig.setField("PROXY_REQUIRED", processData.get("PROXY_REQUIRED"));
				ismsconfig.setField("PROXY_SERVER", processData.get("PROXY_SERVER"));
				ismsconfig.setField("PROXY_SERVER_PORT", processData.get("PROXY_SERVER_PORT"));
				ismsconfig.setField("PROXY_AUTH_REQD", processData.get("PROXY_AUTH_REQD"));
				ismsconfig.setField("PROXY_USER_NAME", processData.get("PROXY_USER_NAME"));
				ismsconfig.setField("PROXY_PASSWORD", processData.get("PROXY_PASSWORD"));
				ismsconfig.setField("AUTH_REQD", processData.get("AUTH_REQD"));
				ismsconfig.setField("USER_NAME_KEY", processData.get("USER_NAME_KEY"));
				ismsconfig.setField("USER_NAME", processData.get("USER_NAME"));
				ismsconfig.setField("PASSWORD_KEY", processData.get("PASSWORD_KEY"));
				ismsconfig.setField("PASSWORD", processData.get("PASSWORD"));
				ismsconfig.setField("MOBILE_NUMBER_KEY", processData.get("MOBILE_NUMBER_KEY"));
				ismsconfig.setField("MESSAGE_KEY", processData.get("MESSAGE_KEY"));
				ismsconfig.setField("SMSC_SMPP_ALLOWED", processData.get("SMSC_SMPP_ALLOWED"));
				ismsconfig.setField("SYSTEM_TYPE", processData.get("SYSTEM_TYPE"));
				ismsconfig.setField("MAIN_SMPP_SERVER", processData.get("MAIN_SMPP_SERVER"));
				ismsconfig.setField("MAIN_SMPP_PORT", processData.get("MAIN_SMPP_PORT"));
				ismsconfig.setField("FAILOVER_SMPP_SERVER", processData.get("FAILOVER_SMPP_SERVER"));
				ismsconfig.setField("FAILOVER_SMPP_PORT", processData.get("FAILOVER_SMPP_PORT"));
				ismsconfig.setField("SOURCE_NUMBER", processData.get("SOURCE_NUMBER"));
				ismsconfig.setField("TRANSCEIVER_MODE", processData.get("TRANSCEIVER_MODE"));
				ismsconfig.setField("ADDRESS_RANGE", processData.get("ADDRESS_RANGE"));
				ismsconfig.setField("SMS_TYPE", processData.get("SMS_TYPE"));
				ismsconfig.setField("RETRY_TIMEOUT", processData.get("RETRY_TIMEOUT"));
				ismsconfig.setField("REMARKS", processData.get("REMARKS"));
				if (ismsconfig.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			ismsconfig.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				ismsconfig.setNew(false);
				ismsconfig.setField("SMSC_HTTP_ALLOWED", processData.get("SMSC_HTTP_ALLOWED"));
				ismsconfig.setField("SSL_REQUIRED", processData.get("SSL_REQUIRED"));
				ismsconfig.setField("HTTP_METHOD", processData.get("HTTP_METHOD"));
				ismsconfig.setField("SERVER_URL", processData.get("SERVER_URL"));
				ismsconfig.setField("PROXY_REQUIRED", processData.get("PROXY_REQUIRED"));
				ismsconfig.setField("PROXY_SERVER", processData.get("PROXY_SERVER"));
				ismsconfig.setField("PROXY_SERVER_PORT", processData.get("PROXY_SERVER_PORT"));
				ismsconfig.setField("PROXY_AUTH_REQD", processData.get("PROXY_AUTH_REQD"));
				ismsconfig.setField("PROXY_USER_NAME", processData.get("PROXY_USER_NAME"));
				ismsconfig.setField("PROXY_PASSWORD", processData.get("PROXY_PASSWORD"));
				ismsconfig.setField("AUTH_REQD", processData.get("AUTH_REQD"));
				ismsconfig.setField("USER_NAME_KEY", processData.get("USER_NAME_KEY"));
				ismsconfig.setField("USER_NAME", processData.get("USER_NAME"));
				ismsconfig.setField("PASSWORD_KEY", processData.get("PASSWORD_KEY"));
				ismsconfig.setField("PASSWORD", processData.get("PASSWORD"));
				ismsconfig.setField("MOBILE_NUMBER_KEY", processData.get("MOBILE_NUMBER_KEY"));
				ismsconfig.setField("MESSAGE_KEY", processData.get("MESSAGE_KEY"));
				ismsconfig.setField("SMSC_SMPP_ALLOWED", processData.get("SMSC_SMPP_ALLOWED"));
				ismsconfig.setField("SYSTEM_TYPE", processData.get("SYSTEM_TYPE"));
				ismsconfig.setField("MAIN_SMPP_SERVER", processData.get("MAIN_SMPP_SERVER"));
				ismsconfig.setField("MAIN_SMPP_PORT", processData.get("MAIN_SMPP_PORT"));
				ismsconfig.setField("FAILOVER_SMPP_SERVER", processData.get("FAILOVER_SMPP_SERVER"));
				ismsconfig.setField("FAILOVER_SMPP_PORT", processData.get("FAILOVER_SMPP_PORT"));
				ismsconfig.setField("SOURCE_NUMBER", processData.get("SOURCE_NUMBER"));
				ismsconfig.setField("TRANSCEIVER_MODE", processData.get("TRANSCEIVER_MODE"));
				ismsconfig.setField("ADDRESS_RANGE", processData.get("ADDRESS_RANGE"));
				ismsconfig.setField("SMS_TYPE", processData.get("SMS_TYPE"));
				ismsconfig.setField("RETRY_TIMEOUT", processData.get("RETRY_TIMEOUT"));
              		ismsconfig.setField("REMARKS", processData.get("REMARKS"));
				if (ismsconfig.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			ismsconfig.close();
		}
	}
}