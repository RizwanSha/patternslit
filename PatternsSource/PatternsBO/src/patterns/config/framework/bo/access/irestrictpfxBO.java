package patterns.config.framework.bo.access;

import java.util.Date;

import patterns.config.framework.database.BindParameterType;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTDObject;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;

public class irestrictpfxBO extends GenericBO {

	TBADynaSQL erestrictpfx = null;
	TBADynaSQL erestrictpfxdtl = null;

	public irestrictpfxBO() {
	}

	public void init() {
		erestrictpfx = new TBADynaSQL("RESTRICTPFX", true);
		erestrictpfxdtl = new TBADynaSQL("RESTRICTPFXDTL", false);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + BackOfficeFormatUtils.getDate((Date) processData.getObject("EFFT_DATE"), BackOfficeConstants.TBA_DATE_FORMAT);
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.getObject("EFFT_DATE").toString());
		erestrictpfx.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		erestrictpfx.setField("EFFT_DATE", processData.getObject("EFFT_DATE"));
		dataExists = erestrictpfx.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				erestrictpfx.setNew(true);
				erestrictpfx.setField("PFX_RESTRICT_REQD", processData.get("PFX_RESTRICT_REQD"));
				erestrictpfx.setField("REMARKS", processData.get("REMARKS"));
				if (erestrictpfx.save()) {
					if (processData.get("PFX_RESTRICT_REQD").equals(RegularConstants.COLUMN_ENABLE)) {
						erestrictpfxdtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
						erestrictpfxdtl.setField("EFFT_DATE", processData.getObject("EFFT_DATE"));
						DTDObject detailData = processData.getDTDObject("RESTRICTPFXDTL");
						erestrictpfxdtl.setNew(true);
						for (int i = 0; i < detailData.getRowCount(); ++i) {
							erestrictpfxdtl.setField("SL", Integer.valueOf(i + 1));
							erestrictpfxdtl.setField("PFX", detailData.getValue(i, 1));
							if (!erestrictpfxdtl.save()) {
								processResult.setProcessStatus(TBAProcessStatus.FAILURE);
								processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
								return;
							}
						}
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			erestrictpfx.close();
			erestrictpfxdtl.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				erestrictpfx.setNew(false);
				erestrictpfx.setField("PFX_RESTRICT_REQD", processData.get("PFX_RESTRICT_REQD"));
				erestrictpfx.setField("REMARKS", processData.get("REMARKS"));
				if (erestrictpfx.save()) {
					String[] erestrictpfxdtlFields = { "ENTITY_CODE", "EFFT_DATE", "SL" };
					BindParameterType[] erestrictpfxdtlFieldTypes = { BindParameterType.VARCHAR, BindParameterType.DATE, BindParameterType.INTEGER };
					erestrictpfxdtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
					erestrictpfxdtl.setField("EFFT_DATE", processData.getObject("EFFT_DATE"));
					erestrictpfxdtl.deleteByFieldsAudit(erestrictpfxdtlFields, erestrictpfxdtlFieldTypes);
					if (processData.get("PFX_RESTRICT_REQD").equals(RegularConstants.COLUMN_ENABLE)) {
						DTDObject detailData = processData.getDTDObject("RESTRICTPFXDTL");
						erestrictpfxdtl.setNew(true);
						for (int i = 0; i < detailData.getRowCount(); ++i) {
							erestrictpfxdtl.setField("SL", Integer.valueOf(i + 1));
							erestrictpfxdtl.setField("PFX", detailData.getValue(i, 1));
							if (!erestrictpfxdtl.save()) {
								processResult.setProcessStatus(TBAProcessStatus.FAILURE);
								processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
								return;
							}
						}
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			erestrictpfx.close();
			erestrictpfxdtl.close();
		}
	}
}