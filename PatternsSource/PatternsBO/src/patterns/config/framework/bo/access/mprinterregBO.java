package patterns.config.framework.bo.access;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.BindParameterType;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTDObject;

public class mprinterregBO extends GenericBO {

	TBADynaSQL mprinterreg = null;
	TBADynaSQL mprinterregdtl = null;

	public mprinterregBO() {
	}

	public void init() {
		mprinterreg = new TBADynaSQL("PRINTERREG", true);
		mprinterregdtl = new TBADynaSQL("PRINTERREGDTL", false);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("PRINTER_ID");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("PRINTER_DESC"));
		mprinterreg.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		mprinterreg.setField("PRINTER_ID", processData.get("PRINTER_ID"));
		dataExists = mprinterreg.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				mprinterreg.setNew(true);
				mprinterreg.setField("PRINTER_DESC", processData.get("PRINTER_DESC"));
				mprinterreg.setField("PRINTER_NAME", processData.get("PRINTER_NAME"));
				mprinterreg.setField("PRINTER_ADDRESS", processData.get("PRINTER_ADDRESS"));
				mprinterreg.setField("PRINTER_TYPE", processData.get("PRINTER_TYPE"));
				mprinterreg.setField("BRN_CODE", processData.get("BRN_CODE"));
				mprinterreg.setField("BRNLIST_CODE", processData.get("BRNLIST_CODE"));
				mprinterreg.setField("STATUS", processData.get("STATUS"));
				mprinterreg.setField("ENABLED", processData.get("ENABLED"));
				mprinterreg.setField("REMARKS", processData.get("REMARKS"));
				if (mprinterreg.save()) {
					mprinterregdtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
					mprinterregdtl.setField("PRINTER_ID", processData.get("PRINTER_ID"));
					DTDObject detailData = processData.getDTDObject("PRINTERREGDTL");
					mprinterregdtl.setNew(true);
					for (int i = 0; i < detailData.getRowCount(); ++i) {
						mprinterregdtl.setField("SL", Integer.valueOf(i + 1));
						mprinterregdtl.setField("PRNTYPE_ID", detailData.getValue(i, 1));
						if (!mprinterregdtl.save()) {
							processResult.setProcessStatus(TBAProcessStatus.FAILURE);
							processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
							return;
						}
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());

		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				mprinterreg.setNew(false);
				mprinterreg.setField("PRINTER_DESC", processData.get("PRINTER_DESC"));
				mprinterreg.setField("PRINTER_NAME", processData.get("PRINTER_NAME"));
				mprinterreg.setField("PRINTER_ADDRESS", processData.get("PRINTER_ADDRESS"));
				mprinterreg.setField("PRINTER_TYPE", processData.get("PRINTER_TYPE"));
				mprinterreg.setField("BRN_CODE", processData.get("BRN_CODE"));
				mprinterreg.setField("BRNLIST_CODE", processData.get("BRNLIST_CODE"));
				mprinterreg.setField("STATUS", processData.get("STATUS"));
				mprinterreg.setField("ENABLED", processData.get("ENABLED"));
				mprinterreg.setField("REMARKS", processData.get("REMARKS"));
				if (mprinterreg.save()) {
					String[] mprinterregdtlFields = { "ENTITY_CODE", "PRINTER_ID", "SL" };
					BindParameterType[] mprinterregdtlFieldTypes = { BindParameterType.BIGINT, BindParameterType.VARCHAR, BindParameterType.INTEGER };
					mprinterregdtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
					mprinterregdtl.setField("PRINTER_ID", processData.get("PRINTER_ID"));
					mprinterregdtl.deleteByFieldsAudit(mprinterregdtlFields, mprinterregdtlFieldTypes);
					DTDObject detailData = processData.getDTDObject("PRINTERREGDTL");
					mprinterregdtl.setNew(true);
					for (int i = 0; i < detailData.getRowCount(); ++i) {
						mprinterregdtl.setField("SL", Integer.valueOf(i + 1));
						mprinterregdtl.setField("PRNTYPE_ID", String.valueOf(detailData.getValue(i, 1)));
						if (!mprinterregdtl.save()) {
							processResult.setProcessStatus(TBAProcessStatus.FAILURE);
							processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
							return;
						}
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}

		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		}
	}
}