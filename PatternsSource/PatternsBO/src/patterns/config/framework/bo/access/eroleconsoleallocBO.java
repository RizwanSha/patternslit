package patterns.config.framework.bo.access;

import java.util.Date;
import patterns.config.framework.database.BindParameterType;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTDObject;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;

public class eroleconsoleallocBO extends GenericBO {

	TBADynaSQL roleconsolealloc = null;
	TBADynaSQL roleconsoleallocdtl = null;

	public eroleconsoleallocBO() {
	}

	public void init() {
		roleconsolealloc = new TBADynaSQL("ROLECONSOLEALLOCHIST", true);
		roleconsoleallocdtl = new TBADynaSQL("ROLECONSOLEALLOCHISTDTL", false);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("ROLE_TYPE") + RegularConstants.PK_SEPARATOR + BackOfficeFormatUtils.getDate((Date) processData.getObject("EFFT_DATE"), BackOfficeConstants.TBA_DATE_FORMAT);
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("ROLE_TYPE"));
		tbaContext.setDisplayDetails(processData.getObject("EFFT_DATE").toString());
		roleconsolealloc.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		roleconsolealloc.setField("ROLE_TYPE", processData.get("ROLE_TYPE"));
		roleconsolealloc.setField("EFFT_DATE", processData.getObject("EFFT_DATE"));
		dataExists = roleconsolealloc.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				roleconsolealloc.setField("REMARKS", processData.getObject("REMARKS"));
				roleconsolealloc.setNew(true);
				if (roleconsolealloc.save()) {
					roleconsoleallocdtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
					roleconsoleallocdtl.setField("ROLE_TYPE", processData.get("ROLE_TYPE"));
					roleconsoleallocdtl.setField("EFFT_DATE", processData.getObject("EFFT_DATE"));
					DTDObject detailData = processData.getDTDObject("ROLECONSOLEALLOCHISTDTL");
					roleconsoleallocdtl.setNew(true);
					int count = 1;
					for (int i = 0; i < detailData.getRowCount(); ++i) {
						if (detailData.getValue(i, 3).equals(RegularConstants.COLUMN_ENABLE)) {
							roleconsoleallocdtl.setField("SL", count);
							roleconsoleallocdtl.setField("CONSOLE_CODE", detailData.getValue(i, 1));
							if (!roleconsoleallocdtl.save()) {
								processResult.setProcessStatus(TBAProcessStatus.FAILURE);
								processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
								return;
							}
							count++;
						}
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			roleconsolealloc.close();
			roleconsoleallocdtl.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				roleconsolealloc.setField("REMARKS", processData.getObject("REMARKS"));
				roleconsolealloc.setNew(false);
				if (roleconsolealloc.save()) {
					String[] roleconsoleallocdtlFields = { "ENTITY_CODE", "ROLE_TYPE", "EFFT_DATE", "SL" };
					BindParameterType[] roleconsoleallocdtlFieldsTypes = { BindParameterType.VARCHAR, BindParameterType.VARCHAR, BindParameterType.DATE, BindParameterType.INTEGER };
					roleconsoleallocdtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
					roleconsoleallocdtl.setField("ROLE_TYPE", processData.get("ROLE_TYPE"));
					roleconsoleallocdtl.setField("EFFT_DATE", processData.getObject("EFFT_DATE"));
					roleconsoleallocdtl.deleteByFieldsAudit(roleconsoleallocdtlFields, roleconsoleallocdtlFieldsTypes);
					DTDObject detailData = processData.getDTDObject("ROLECONSOLEALLOCHISTDTL");
					roleconsoleallocdtl.setNew(true);
					int count = 1;
					for (int i = 0; i < detailData.getRowCount(); ++i) {
						if (detailData.getValue(i, 3).equals(RegularConstants.COLUMN_ENABLE)) {
							roleconsoleallocdtl.setField("SL", count);
							roleconsoleallocdtl.setField("CONSOLE_CODE", detailData.getValue(i, 1));
							if (!roleconsoleallocdtl.save()) {
								processResult.setProcessStatus(TBAProcessStatus.FAILURE);
								processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
								return;
							}
							count++;
						}
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}

		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			roleconsolealloc.close();
			roleconsoleallocdtl.close();
		}
	}
}