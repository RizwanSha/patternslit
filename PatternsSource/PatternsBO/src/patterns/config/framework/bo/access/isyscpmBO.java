package patterns.config.framework.bo.access;

import java.util.Date;

import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;

public class isyscpmBO extends GenericBO {

	TBADynaSQL syscpm = null;

	public isyscpmBO() {
	}

	public void init() {
		syscpm = new TBADynaSQL("SYSCPMHIST", true);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + BackOfficeFormatUtils.getDate((Date) processData.getObject("EFFT_DATE"), BackOfficeConstants.TBA_DATE_FORMAT);
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(BackOfficeFormatUtils.getDate((Date) processData.getObject("EFFT_DATE"), BackOfficeConstants.TBA_DATE_FORMAT));
		syscpm.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		syscpm.setField("EFFT_DATE", processData.getObject("EFFT_DATE"));
		dataExists = syscpm.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				syscpm.setNew(true);
				syscpm.setField("MIN_UID_LEN", Integer.valueOf(processData.get("MIN_UID_LEN")));
				syscpm.setField("MIN_PWD_LEN", Integer.valueOf(processData.get("MIN_PWD_LEN")));
				syscpm.setField("MIN_PWD_NUM", Integer.valueOf(processData.get("MIN_PWD_NUM")));
				syscpm.setField("MIN_PWD_ALPHA", Integer.valueOf(processData.get("MIN_PWD_ALPHA")));
				syscpm.setField("MIN_PWD_SPECIAL", Integer.valueOf(processData.get("MIN_PWD_SPECIAL")));
				syscpm.setField("FORCE_PWD_CHGN", Integer.valueOf(processData.get("FORCE_PWD_CHGN")));
				syscpm.setField("PREVENT_PREV_PWD", Integer.valueOf(processData.get("PREVENT_PREV_PWD")));
				syscpm.setField("UNSUCC_ATTMPT_UW", Integer.valueOf(processData.get("UNSUCC_ATTMPT_UW")));
				syscpm.setField("UNSUCC_ATTMPT_SW", Integer.valueOf(processData.get("UNSUCC_ATTMPT_SW")));
				syscpm.setField("LOCK_UID", Integer.valueOf(processData.get("LOCK_UID")));
				syscpm.setField("TOTAL_LEASE_PERIOD", Integer.valueOf(processData.get("TOTAL_LEASE_PERIOD")));
				syscpm.setField("AUTO_LOGOUT_SECS", Integer.valueOf(processData.get("AUTO_LOGOUT_SECS")));
				syscpm.setField("INACT_NOT_USED", Integer.valueOf(processData.get("INACT_NOT_USED")));
				syscpm.setField("INACT_ONCE_USED", Integer.valueOf(processData.get("INACT_ONCE_USED")));
				syscpm.setField("INACT_UID_EXPIRY", Integer.valueOf(processData.get("INACT_UID_EXPIRY")));
				syscpm.setField("INACT_PWD_EXPIRY", Integer.valueOf(processData.get("INACT_PWD_EXPIRY")));
				syscpm.setField("MULTIPLE_SESSION_ALLOWED", processData.get("MULTIPLE_SESSION_ALLOWED"));
				syscpm.setField("ADMIN_USER_IP_RESTRICT", processData.get("ADMIN_USER_IP_RESTRICT"));
				syscpm.setField("MAX_ACTING_ROLE", processData.get("MAX_ACTING_ROLE"));
				syscpm.setField("ADMIN_ACTIVATION_REQD", processData.get("ADMIN_ACTIVATION_REQD"));
				syscpm.setField("PWD_DELIVERY_MECHANISM", processData.get("PWD_DELIVERY_MECHANISM"));
				syscpm.setField("PWD_MANUAL_TYPE", processData.get("PWD_MANUAL_TYPE"));
				syscpm.setField("REMARKS", processData.get("REMARKS"));
				if (syscpm.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				syscpm.setNew(false);
				syscpm.setField("MIN_UID_LEN", Integer.valueOf(processData.get("MIN_UID_LEN")));
				syscpm.setField("MIN_PWD_LEN", Integer.valueOf(processData.get("MIN_PWD_LEN")));
				syscpm.setField("MIN_PWD_NUM", Integer.valueOf(processData.get("MIN_PWD_NUM")));
				syscpm.setField("MIN_PWD_ALPHA", Integer.valueOf(processData.get("MIN_PWD_ALPHA")));
				syscpm.setField("MIN_PWD_SPECIAL", Integer.valueOf(processData.get("MIN_PWD_SPECIAL")));
				syscpm.setField("FORCE_PWD_CHGN", Integer.valueOf(processData.get("FORCE_PWD_CHGN")));
				syscpm.setField("PREVENT_PREV_PWD", Integer.valueOf(processData.get("PREVENT_PREV_PWD")));
				syscpm.setField("UNSUCC_ATTMPT_UW", Integer.valueOf(processData.get("UNSUCC_ATTMPT_UW")));
				syscpm.setField("UNSUCC_ATTMPT_SW", Integer.valueOf(processData.get("UNSUCC_ATTMPT_SW")));
				syscpm.setField("LOCK_UID", Integer.valueOf(processData.get("LOCK_UID")));
				syscpm.setField("TOTAL_LEASE_PERIOD", Integer.valueOf(processData.get("TOTAL_LEASE_PERIOD")));
				syscpm.setField("AUTO_LOGOUT_SECS", Integer.valueOf(processData.get("AUTO_LOGOUT_SECS")));
				syscpm.setField("INACT_NOT_USED", Integer.valueOf(processData.get("INACT_NOT_USED")));
				syscpm.setField("INACT_ONCE_USED", Integer.valueOf(processData.get("INACT_ONCE_USED")));
				syscpm.setField("INACT_UID_EXPIRY", Integer.valueOf(processData.get("INACT_UID_EXPIRY")));
				syscpm.setField("INACT_PWD_EXPIRY", Integer.valueOf(processData.get("INACT_PWD_EXPIRY")));
				syscpm.setField("MULTIPLE_SESSION_ALLOWED", processData.get("MULTIPLE_SESSION_ALLOWED"));
				syscpm.setField("ADMIN_USER_IP_RESTRICT", processData.get("ADMIN_USER_IP_RESTRICT"));
				syscpm.setField("ADMIN_BRN_IP_RESTRICT", processData.get("ADMIN_BRN_IP_RESTRICT"));
				syscpm.setField("MAX_ACTING_ROLE", processData.get("MAX_ACTING_ROLE"));
				syscpm.setField("ADMIN_ACTIVATION_REQD", processData.get("ADMIN_ACTIVATION_REQD"));
				syscpm.setField("PWD_DELIVERY_MECHANISM", processData.get("PWD_DELIVERY_MECHANISM"));
				syscpm.setField("PWD_MANUAL_TYPE", processData.get("PWD_MANUAL_TYPE"));
				syscpm.setField("REMARKS", processData.get("REMARKS"));
				if (syscpm.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);

				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		}
	}
}