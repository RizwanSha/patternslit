package patterns.config.framework.bo.access;

import java.util.Date;

import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.BindParameterType;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTDObject;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;

public class ipwdneglistBO extends GenericBO {

	TBADynaSQL ipwdneglist = null;
	TBADynaSQL ipwdneglistdtl = null;

	public ipwdneglistBO() {
	}

	public void init() {
		ipwdneglist = new TBADynaSQL("PWDNEGLIST", true);
		ipwdneglistdtl = new TBADynaSQL("PWDNEGLISTDTL", false);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + BackOfficeFormatUtils.getDate((Date) processData.getObject("EFFT_DATE"), BackOfficeConstants.TBA_DATE_FORMAT);
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(BackOfficeFormatUtils.getDate((Date) processData.getObject("EFFT_DATE"), BackOfficeConstants.TBA_DATE_FORMAT));
		ipwdneglist.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		ipwdneglist.setField("EFFT_DATE", processData.getObject("EFFT_DATE"));
		dataExists = ipwdneglist.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				ipwdneglist.setNew(true);
				ipwdneglist.setField("REMARKS", processData.get("REMARKS"));
				if (ipwdneglist.save()) {
					ipwdneglistdtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
					ipwdneglistdtl.setField("EFFT_DATE", processData.getObject("EFFT_DATE"));
					DTDObject detailData = processData.getDTDObject("PWDNEGLISTDTL");
					ipwdneglistdtl.setNew(true);
					for (int i = 0; i < detailData.getRowCount(); ++i) {
						ipwdneglistdtl.setField("SL", Integer.valueOf(i + 1));
						ipwdneglistdtl.setField("PASSWORD", detailData.getValue(i, 1));
						if (!ipwdneglistdtl.save()) {
							processResult.setProcessStatus(TBAProcessStatus.FAILURE);
							processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
							return;
						}
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());

		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				ipwdneglist.setNew(false);
				ipwdneglist.setField("REMARKS", processData.get("REMARKS"));
				if (ipwdneglist.save()) {
					String[] ipwdneglistdtlFields = { "ENTITY_CODE", "EFFT_DATE", "SL" };
					BindParameterType[] ipwdneglistdtlFieldTypes = { BindParameterType.VARCHAR, BindParameterType.DATE, BindParameterType.INTEGER };
					ipwdneglistdtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
					ipwdneglistdtl.setField("EFFT_DATE", processData.getObject("EFFT_DATE"));
					ipwdneglistdtl.deleteByFieldsAudit(ipwdneglistdtlFields, ipwdneglistdtlFieldTypes);
					DTDObject detailData = processData.getDTDObject("PWDNEGLISTDTL");
					ipwdneglistdtl.setNew(true);
					for (int i = 0; i < detailData.getRowCount(); ++i) {
						ipwdneglistdtl.setField("SL", Integer.valueOf(i + 1));
						ipwdneglistdtl.setField("PASSWORD", String.valueOf(detailData.getValue(i, 1)));
						if (!ipwdneglistdtl.save()) {
							processResult.setProcessStatus(TBAProcessStatus.FAILURE);
							processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
							return;
						}
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}

		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		}
	}
}