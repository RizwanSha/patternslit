package patterns.config.framework.bo.access;

import java.util.ArrayList;
import java.util.List;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.bo.MessageParam;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;

public class epwdresetBO extends GenericBO {

	TBADynaSQL epwdreset = null;

	public epwdresetBO() {
	}

	public void init() {
		epwdreset = new TBADynaSQL("USERSPWDRESET", true);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("USER_ID");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("USER_ID"));
		epwdreset.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		epwdreset.setField("USER_ID", processData.get("USER_ID"));
		dataExists = epwdreset.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		throw new TBAFrameworkException("Unsupported Operation");
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				epwdreset.setNew(false);
				epwdreset.setField("CHANGE_DATETIME", tbaContext.getSystemTime());
				epwdreset.setField("CLEAR_PIN", processData.get("CLEAR_PIN"));
				epwdreset.setField("SALT", processData.get("SALT"));
				epwdreset.setField("ENCYPT_PASSWORD", processData.get("ENCYPT_PASSWORD"));
				epwdreset.setField("FIRST_PIN", RegularConstants.COLUMN_ENABLE);
				epwdreset.setField("SECOND_PIN", RegularConstants.COLUMN_DISABLE);
				epwdreset.setField("REMARKS", processData.get("REMARKS"));
				if (epwdreset.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
					//processResult.setAdditionalInfo(processData.get("CLEAR_PIN"));
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}

			} else {
				epwdreset.setNew(true);
				epwdreset.setField("CHANGE_DATETIME", tbaContext.getSystemTime());
				epwdreset.setField("CLEAR_PIN", processData.get("CLEAR_PIN"));
				epwdreset.setField("SALT", processData.get("SALT"));
				epwdreset.setField("ENCYPT_PASSWORD", processData.get("ENCYPT_PASSWORD"));
				epwdreset.setField("FIRST_PIN", RegularConstants.COLUMN_ENABLE);
				epwdreset.setField("SECOND_PIN", RegularConstants.COLUMN_DISABLE);
				epwdreset.setField("REMARKS", processData.get("REMARKS"));
				if (epwdreset.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);

				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			}
			
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			epwdreset.close();
		}
	}
}