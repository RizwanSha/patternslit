package patterns.config.framework.bo.access;


import java.util.Date;

import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;

public class erolebrnlistBO extends GenericBO {

	TBADynaSQL erolebrnlist = null;

	public erolebrnlistBO() {
	}

	public void init() {
		erolebrnlist = new TBADynaSQL("ROLEBRNLISTALLOC", true);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("ROLE_CODE") + RegularConstants.PK_SEPARATOR + BackOfficeFormatUtils.getDate((Date) processData.getObject("EFFT_DATE"), BackOfficeConstants.TBA_DATE_FORMAT);
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("ROLE_CODE"));
		erolebrnlist.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		erolebrnlist.setField("ROLE_CODE", processData.get("ROLE_CODE"));
		erolebrnlist.setField("EFFT_DATE", processData.getObject("EFFT_DATE"));
		dataExists = erolebrnlist.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				erolebrnlist.setNew(true);
				erolebrnlist.setField("ENABLED", processData.get("ENABLED"));
				erolebrnlist.setField("BRANCH_LIST_CODE", processData.get("BRANCH_LIST_CODE"));
				erolebrnlist.setField("REMARKS", processData.get("REMARKS"));
				if (erolebrnlist.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			erolebrnlist.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				erolebrnlist.setNew(false);
				erolebrnlist.setField("ENABLED", processData.get("ENABLED"));
				erolebrnlist.setField("BRANCH_LIST_CODE", processData.get("BRANCH_LIST_CODE"));
				erolebrnlist.setField("REMARKS", processData.get("REMARKS"));
				if (erolebrnlist.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			erolebrnlist.close();
		}
	}
}