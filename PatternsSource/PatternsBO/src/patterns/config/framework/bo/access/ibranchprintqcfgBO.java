package patterns.config.framework.bo.access;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.BindParameterType;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTDObject;
import patterns.config.framework.web.BackOfficeFormatUtils;

public class ibranchprintqcfgBO extends GenericBO {
	TBADynaSQL branchprintqcfg = null;
	TBADynaSQL branchprintqcfgdtl = null;

	public ibranchprintqcfgBO() {
	}

	public void init() {
		branchprintqcfg = new TBADynaSQL("BRNPRNQCFGHIST", true);
		branchprintqcfgdtl = new TBADynaSQL("BRNPRNQCFGHISTDTL", false);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("BRN_CODE") + RegularConstants.PK_SEPARATOR + BackOfficeFormatUtils.getDate(processData.getDate("EFFT_DATE"), BackOfficeConstants.TBA_DATE_FORMAT);
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("BRN_CODE"));
		tbaContext.setDisplayDetails(processData.get("EFFT_DATE"));
		branchprintqcfg.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		branchprintqcfg.setField("BRN_CODE", processData.get("BRN_CODE"));
		branchprintqcfg.setField("EFFT_DATE", processData.getDate("EFFT_DATE"));
		dataExists = branchprintqcfg.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				branchprintqcfg.setNew(true);
				if (branchprintqcfg.save()) {
					branchprintqcfgdtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
					branchprintqcfgdtl.setField("BRN_CODE", processData.get("BRN_CODE"));
					branchprintqcfgdtl.setField("EFFT_DATE", processData.getDate("EFFT_DATE"));
					DTDObject detailData = processData.getDTDObject("BRNPRNQCFGHISTDTL");
					branchprintqcfgdtl.setNew(true);
					for (int i = 0; i < detailData.getRowCount(); ++i) {
						branchprintqcfgdtl.setField("SL", Integer.valueOf(i + 1));
						branchprintqcfgdtl.setField("PRNTYPE_ID", detailData.getValue(i, 1));
						branchprintqcfgdtl.setField("PRNQ_ID", detailData.getValue(i, 3));
						if (!branchprintqcfgdtl.save()) {
							processResult.setProcessStatus(TBAProcessStatus.FAILURE);
							processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);

						}
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			branchprintqcfg.close();
			branchprintqcfgdtl.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				branchprintqcfg.setNew(false);
				if (branchprintqcfg.save()) {
					String[] branchprintqcfgdtlFields = { "ENTITY_CODE", "BRN_CODE", "EFFT_DATE", "SL" };
					BindParameterType[] branchprintqcfgdtlFieldTypes = { BindParameterType.VARCHAR, BindParameterType.VARCHAR, BindParameterType.DATE, BindParameterType.INTEGER };
					branchprintqcfgdtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
					branchprintqcfgdtl.setField("BRN_CODE", processData.get("BRN_CODE"));
					branchprintqcfgdtl.setField("EFFT_DATE", processData.getDate("EFFT_DATE"));
					branchprintqcfgdtl.deleteByFieldsAudit(branchprintqcfgdtlFields, branchprintqcfgdtlFieldTypes);
					DTDObject detailData = processData.getDTDObject("BRNPRNQCFGHISTDTL");
					branchprintqcfgdtl.setNew(true);
					for (int i = 0; i < detailData.getRowCount(); ++i) {
						branchprintqcfgdtl.setField("SL", Integer.valueOf(i + 1));
						branchprintqcfgdtl.setField("PRNTYPE_ID", detailData.getValue(i, 1));
						branchprintqcfgdtl.setField("PRNQ_ID", detailData.getValue(i, 3));
						if (!branchprintqcfgdtl.save()) {
							processResult.setProcessStatus(TBAProcessStatus.FAILURE);
							processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
							return;
						}
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}

		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			branchprintqcfg.close();
			branchprintqcfgdtl.close();
		}
	}
}
