package patterns.config.framework.bo.access;

import java.util.Date;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.BindParameterType;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTDObject;
import patterns.config.framework.web.BackOfficeFormatUtils;

public class econsolepgmallocBO extends GenericBO {
	TBADynaSQL econsolepgmalloc = null;
	TBADynaSQL econsolepgmallocdtl = null;

	public econsolepgmallocBO() {
	}

	public void init() {
		econsolepgmalloc = new TBADynaSQL("CONSOLEPGMALLOCHIST", true);
		econsolepgmallocdtl = new TBADynaSQL("CONSOLEPGMALLOCHISTDTL", false);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("CONSOLE_CODE") + RegularConstants.PK_SEPARATOR + BackOfficeFormatUtils.getDate((Date) processData.getObject("EFFT_DATE"), BackOfficeConstants.TBA_DATE_FORMAT);
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("CONSOLE_CODE"));
		econsolepgmalloc.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		econsolepgmalloc.setField("CONSOLE_CODE", processData.get("CONSOLE_CODE"));
		econsolepgmalloc.setField("EFFT_DATE", processData.getObject("EFFT_DATE"));
		dataExists = econsolepgmalloc.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				econsolepgmalloc.setField("REMARKS", processData.get("REMARKS"));
				econsolepgmalloc.setNew(true);
				if (econsolepgmalloc.save()) {
					econsolepgmallocdtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
					econsolepgmallocdtl.setField("CONSOLE_CODE", processData.get("CONSOLE_CODE"));
					econsolepgmallocdtl.setField("EFFT_DATE", processData.getObject("EFFT_DATE"));
					DTDObject detailData = processData.getDTDObject("CONSOLEPGMALLOCHISTDTL");
					econsolepgmallocdtl.setNew(true);
					for (int i = 0; i < detailData.getRowCount(); ++i) {
						econsolepgmallocdtl.setField("SL", Integer.valueOf(i + 1));
						econsolepgmallocdtl.setField("PGM_ID", detailData.getValue(i, 1));
						if (!econsolepgmallocdtl.save()) {
							processResult.setProcessStatus(TBAProcessStatus.FAILURE);
							processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);

						}
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			econsolepgmalloc.close();
			econsolepgmallocdtl.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				econsolepgmalloc.setField("REMARKS", processData.get("REMARKS"));
				econsolepgmalloc.setNew(false);
				if (econsolepgmalloc.save()) {
					String[] econsolepgmdtlFields = { "ENTITY_CODE", "CONSOLE_CODE", "EFFT_DATE", "SL" };
					BindParameterType[] econsolepgmdtlFieldTypes = { BindParameterType.VARCHAR, BindParameterType.VARCHAR, BindParameterType.DATE, BindParameterType.INTEGER };
					econsolepgmallocdtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
					econsolepgmallocdtl.setField("CONSOLE_CODE", processData.get("CONSOLE_CODE"));
					econsolepgmallocdtl.setField("EFFT_DATE", processData.getObject("EFFT_DATE"));
					econsolepgmallocdtl.deleteByFieldsAudit(econsolepgmdtlFields, econsolepgmdtlFieldTypes);
					DTDObject detailData = processData.getDTDObject("CONSOLEPGMALLOCHISTDTL");
					econsolepgmallocdtl.setNew(true);
					for (int i = 0; i < detailData.getRowCount(); ++i) {
						econsolepgmallocdtl.setField("SL", Integer.valueOf(i + 1));
						econsolepgmallocdtl.setField("PGM_ID", detailData.getValue(i, 1));
						if (!econsolepgmallocdtl.save()) {
							processResult.setProcessStatus(TBAProcessStatus.FAILURE);
							processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
							return;
						}
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}

		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			econsolepgmalloc.close();
			econsolepgmallocdtl.close();
		}
	}
}
