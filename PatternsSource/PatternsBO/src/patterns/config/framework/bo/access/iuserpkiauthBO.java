package patterns.config.framework.bo.access;

import java.util.Date;

import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;

public class iuserpkiauthBO extends GenericBO {

	TBADynaSQL iuserpkiauth = null;

	public iuserpkiauthBO() {
	}

	public void init() {
		iuserpkiauth = new TBADynaSQL("USERSPKIAUTH", true);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("ROLE_CODE") + RegularConstants.PK_SEPARATOR + BackOfficeFormatUtils.getDate((Date) processData.getObject("EFFT_DATE"), BackOfficeConstants.TBA_DATE_FORMAT);
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("ROLE_CODE"));
		iuserpkiauth.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		iuserpkiauth.setField("ROLE_CODE", processData.get("ROLE_CODE"));
		iuserpkiauth.setField("EFFT_DATE", processData.getObject("EFFT_DATE"));
		dataExists = iuserpkiauth.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				iuserpkiauth.setNew(true);
				iuserpkiauth.setField("TFA_MAND", processData.get("TFA_MAND"));
				iuserpkiauth.setField("LOGIN_AUTH_REQD", processData.get("LOGIN_AUTH_REQD"));
				iuserpkiauth.setField("REMARKS", processData.get("REMARKS"));
				if (iuserpkiauth.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}

			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			iuserpkiauth.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				iuserpkiauth.setNew(false);
				iuserpkiauth.setField("TFA_MAND", processData.get("TFA_MAND"));
				iuserpkiauth.setField("LOGIN_AUTH_REQD", processData.get("LOGIN_AUTH_REQD"));
				iuserpkiauth.setField("REMARKS", processData.get("REMARKS"));
				if (iuserpkiauth.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);

				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			iuserpkiauth.close();
		}
	}
}