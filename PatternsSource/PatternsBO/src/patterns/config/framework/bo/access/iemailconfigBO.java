package patterns.config.framework.bo.access;

import java.util.Date;

import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;

public class iemailconfigBO extends GenericBO {

	TBADynaSQL iemailconfig = null;

	public iemailconfigBO() {
	}

	public void init() {
		iemailconfig = new TBADynaSQL("EMAILINTFCFG", true);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("EMAIL_CODE") + RegularConstants.PK_SEPARATOR + BackOfficeFormatUtils.getDate((Date) processData.getObject("EFFT_DATE"), BackOfficeConstants.TBA_DATE_FORMAT);
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("REMARKS"));
		iemailconfig.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		iemailconfig.setField("EMAIL_CODE", processData.get("EMAIL_CODE"));
		iemailconfig.setField("EFFT_DATE", processData.getObject("EFFT_DATE"));
		dataExists = iemailconfig.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				iemailconfig.setNew(true);
				iemailconfig.setField("SEND_PROTO_TYPE", processData.get("SEND_PROTO_TYPE"));
				iemailconfig.setField("RECEIVE_PROTO_TYPE", processData.get("RECEIVE_PROTO_TYPE"));
				iemailconfig.setField("COMMUNICATION_NAME", processData.get("COMMUNICATION_NAME"));
				iemailconfig.setField("COMMUNICATION_EMAIL", processData.get("COMMUNICATION_EMAIL"));
				iemailconfig.setField("SSL_REQUIRED", processData.get("SSL_REQUIRED"));
				iemailconfig.setField("PROXY_REQUIRED", processData.get("PROXY_REQUIRED"));
				iemailconfig.setField("PROXY_SERVER", processData.get("PROXY_SERVER"));
				iemailconfig.setField("PROXY_SERVER_PORT", processData.get("PROXY_SERVER_PORT"));
				iemailconfig.setField("PROXY_AUTH_REQD", processData.get("PROXY_AUTH_REQD"));
				iemailconfig.setField("PROXY_USER_NAME", processData.get("PROXY_USER_NAME"));
				iemailconfig.setField("PROXY_PASSWORD", processData.get("PROXY_PASSWORD"));
				iemailconfig.setField("OUTGOING_MAIL_SERVER", processData.get("OUTGOING_MAIL_SERVER"));
				iemailconfig.setField("OUTGOING_MAIL_SERVER_PORT", processData.get("OUTGOING_MAIL_SERVER_PORT"));
				iemailconfig.setField("OUTGOING_AUTH_REQD", processData.get("OUTGOING_AUTH_REQD"));
				iemailconfig.setField("OUTGOING_USER_NAME", processData.get("OUTGOING_USER_NAME"));
				iemailconfig.setField("OUTGOING_PASSWORD", processData.get("OUTGOING_PASSWORD"));
				iemailconfig.setField("REMARKS", processData.get("REMARKS"));
				if (iemailconfig.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			iemailconfig.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				iemailconfig.setNew(false);
				iemailconfig.setField("SEND_PROTO_TYPE", processData.get("SEND_PROTO_TYPE"));
				iemailconfig.setField("RECEIVE_PROTO_TYPE", processData.get("RECEIVE_PROTO_TYPE"));
				iemailconfig.setField("COMMUNICATION_NAME", processData.get("COMMUNICATION_NAME"));
				iemailconfig.setField("COMMUNICATION_EMAIL", processData.get("COMMUNICATION_EMAIL"));
				iemailconfig.setField("SSL_REQUIRED", processData.get("SSL_REQUIRED"));
				iemailconfig.setField("PROXY_REQUIRED", processData.get("PROXY_REQUIRED"));
				iemailconfig.setField("PROXY_SERVER", processData.get("PROXY_SERVER"));
				iemailconfig.setField("PROXY_SERVER_PORT", processData.get("PROXY_SERVER_PORT"));
				iemailconfig.setField("PROXY_AUTH_REQD", processData.get("PROXY_AUTH_REQD"));
				iemailconfig.setField("PROXY_USER_NAME", processData.get("PROXY_USER_NAME"));
				iemailconfig.setField("PROXY_PASSWORD", processData.get("PROXY_PASSWORD"));
				iemailconfig.setField("OUTGOING_MAIL_SERVER", processData.get("OUTGOING_MAIL_SERVER"));
				iemailconfig.setField("OUTGOING_MAIL_SERVER_PORT", processData.get("OUTGOING_MAIL_SERVER_PORT"));
				iemailconfig.setField("OUTGOING_AUTH_REQD", processData.get("OUTGOING_AUTH_REQD"));
				iemailconfig.setField("OUTGOING_USER_NAME", processData.get("OUTGOING_USER_NAME"));
				iemailconfig.setField("OUTGOING_PASSWORD", processData.get("OUTGOING_PASSWORD"));
				iemailconfig.setField("REMARKS", processData.get("REMARKS"));
				if (iemailconfig.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			iemailconfig.close();
		}
	}
}