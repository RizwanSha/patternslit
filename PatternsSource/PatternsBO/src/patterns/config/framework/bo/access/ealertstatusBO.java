package patterns.config.framework.bo.access;

import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;

public class ealertstatusBO extends GenericBO {
	TBADynaSQL ealertstatus = null;

	public ealertstatusBO() {
	}

	public void init() {
		ealertstatus = new TBADynaSQL("ALERTPROCSTATUS", true);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("CODE");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("CODE"));
		ealertstatus.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		ealertstatus.setField("CODE", processData.get("CODE"));
		dataExists = ealertstatus.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		throw new TBAFrameworkException("Unsupported Operation");
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				ealertstatus.setNew(false);
				ealertstatus.setField("CHANGE_DATETIME", tbaContext.getSystemTime());
				ealertstatus.setField("STATUS", processData.get("STATUS"));
				ealertstatus.setField("CRON_EXPRESSION", processData.get("CRON_EXPRESSION"));
				ealertstatus.setField("RECORD_PER_REQUEST", processData.get("RECORD_PER_REQUEST"));
				ealertstatus.setField("REMARKS", processData.get("REMARKS"));
				if (ealertstatus.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			ealertstatus.close();
		}
	}
}