package patterns.config.framework.bo.access;

import java.util.Date;

import patterns.config.framework.database.BindParameterType;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTDObject;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;

public class eipuserBO extends GenericBO {

	TBADynaSQL eipuser = null;
	TBADynaSQL eipuserdtl = null;

	public eipuserBO() {
	}

	public void init() {
		
		eipuser = new TBADynaSQL("USERIPRESTRICTHIST", true);
		eipuserdtl = new TBADynaSQL("USERIPRESTRICTHISTDTL", false);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("USER_ID") + RegularConstants.PK_SEPARATOR + BackOfficeFormatUtils.getDate((Date) processData.getObject("EFFT_DATE"), BackOfficeConstants.TBA_DATE_FORMAT);
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("USER_ID"));
		eipuser.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		eipuser.setField("USER_ID", processData.get("USER_ID"));
		eipuser.setField("EFFT_DATE", processData.getObject("EFFT_DATE"));
		dataExists = eipuser.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				eipuser.setNew(true);
				eipuser.setField("ENABLED", processData.get("ENABLED"));
				eipuser.setField("REMARKS", processData.get("REMARKS"));
				if (eipuser.save()) {
					if (processData.get("ENABLED").equals(RegularConstants.COLUMN_ENABLE)) {
						eipuserdtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
						eipuserdtl.setField("USER_ID", processData.get("USER_ID"));
						eipuserdtl.setField("EFFT_DATE", processData.getObject("EFFT_DATE"));
						DTDObject detailData = processData.getDTDObject("USERIPRESTRICTDTL");
						eipuserdtl.setNew(true);
						for (int i = 0; i < detailData.getRowCount(); ++i) {
							eipuserdtl.setField("SL", Integer.valueOf(i + 1));
							eipuserdtl.setField("IPADDRESS", detailData.getValue(i, 1));
							if (!eipuserdtl.save()) {
								processResult.setProcessStatus(TBAProcessStatus.FAILURE);
								processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
								return;
							}
						}
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			eipuser.close();
			eipuserdtl.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				eipuser.setNew(false);
				eipuser.setField("ENABLED", processData.get("ENABLED"));
				eipuser.setField("REMARKS", processData.get("REMARKS"));
				if (eipuser.save()) {
					String[] eipuserdtlFields = { "ENTITY_CODE", "USER_ID", "EFFT_DATE", "SL" };
					BindParameterType[] eipuserdtlFieldTypes = { BindParameterType.VARCHAR, BindParameterType.VARCHAR, BindParameterType.DATE, BindParameterType.INTEGER };
					eipuserdtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
					eipuserdtl.setField("USER_ID", processData.get("USER_ID"));
					eipuserdtl.setField("EFFT_DATE", processData.getObject("EFFT_DATE"));
					eipuserdtl.deleteByFieldsAudit(eipuserdtlFields, eipuserdtlFieldTypes);
					if (processData.get("ENABLED").equals(RegularConstants.COLUMN_ENABLE)) {
						DTDObject detailData = processData.getDTDObject("USERIPRESTRICTDTL");
						eipuserdtl.setNew(true);
						for (int i = 0; i < detailData.getRowCount(); ++i) {
							eipuserdtl.setField("SL", Integer.valueOf(i + 1));
							eipuserdtl.setField("IPADDRESS", detailData.getValue(i, 1));
							if (!eipuserdtl.save()) {
								processResult.setProcessStatus(TBAProcessStatus.FAILURE);
								processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
								return;
							}
						}
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			eipuser.close();
			eipuserdtl.close();
		}
	}
}