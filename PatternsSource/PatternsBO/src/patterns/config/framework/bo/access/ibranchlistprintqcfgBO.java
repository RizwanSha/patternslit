package patterns.config.framework.bo.access;
/*

*
Author : Pavan kumar.R
Created Date : 20-FEB-2015
Spec Reference: PSD-ACC-38
Modification History
----------------------------------------------------------------------
Sl.No		Modified Date	Author			Modified Changes	Version
----------------------------------------------------------------------

*/


import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.BindParameterType;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTDObject;
import patterns.config.framework.web.BackOfficeFormatUtils;

public class ibranchlistprintqcfgBO extends GenericBO {
	TBADynaSQL ibranchlistprintqcfg = null;  
	TBADynaSQL ibranchlistprintqcfgdtl = null; 
	public ibranchlistprintqcfgBO() {
	}

	public void init() {
		ibranchlistprintqcfg = new TBADynaSQL("BRNLISTPRNQCFGHIST", true);
		ibranchlistprintqcfgdtl = new TBADynaSQL("BRNLISTPRNQCFGHISTDTL", false);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("BRNLIST_CODE")  + RegularConstants.PK_SEPARATOR + BackOfficeFormatUtils.getDate(processData.getDate("EFFT_DATE"), BackOfficeConstants.TBA_DATE_FORMAT);
		tbaContext.setTbaPrimarykey(primaryKey);
		ibranchlistprintqcfg.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		ibranchlistprintqcfg.setField("BRNLIST_CODE", processData.get("BRNLIST_CODE"));
		ibranchlistprintqcfg.setField("EFFT_DATE", processData.getDate("EFFT_DATE"));
		dataExists = ibranchlistprintqcfg.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {

		try {
			if (!isDataExists()) {
				ibranchlistprintqcfg.setNew(true);
				if (ibranchlistprintqcfg.save()) {
						DTDObject detailData = processData.getDTDObject("BRNLISTPRNQCFGHISTDTL");
						ibranchlistprintqcfgdtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
						ibranchlistprintqcfgdtl.setField("BRNLIST_CODE", processData.get("BRNLIST_CODE"));
						ibranchlistprintqcfgdtl.setField("EFFT_DATE", processData.getDate("EFFT_DATE"));
						ibranchlistprintqcfgdtl.setNew(true);
						for (int i = 0; i < detailData.getRowCount(); i++) {
							ibranchlistprintqcfgdtl.setField("SL", String.valueOf(i + 1));
							ibranchlistprintqcfgdtl.setField("PRNTYPE_ID", detailData.getValue(i, 1));
							ibranchlistprintqcfgdtl.setField("PRNQ_ID", detailData.getValue(i, 3));
							if (!ibranchlistprintqcfgdtl.save()) {
								processResult.setProcessStatus(TBAProcessStatus.FAILURE);
								processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
								return;
							}
						}
						processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
						processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_UPDATED);
					} else {
						processResult.setProcessStatus(TBAProcessStatus.FAILURE);
						processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
					}
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_EXISTS);
				}
			} catch (Exception e) {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
				throw new TBAFrameworkException(e.getLocalizedMessage());
			} finally {
				ibranchlistprintqcfg.close();
				ibranchlistprintqcfgdtl.close();
			}
		}
	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				ibranchlistprintqcfg.setNew(false);
				if (ibranchlistprintqcfg.save()) {
					String[] ibranchlistprintqcfgdtlFields = { "ENTITY_CODE", "BRNLIST_CODE","EFFT_DATE" ,"SL" };
					BindParameterType[] ibranchlistprintqcfgdtlFieldTypes = { BindParameterType.BIGINT, BindParameterType.VARCHAR, BindParameterType.DATE, BindParameterType.INTEGER };
					ibranchlistprintqcfgdtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
					ibranchlistprintqcfgdtl.setField("BRNLIST_CODE", processData.get("BRNLIST_CODE"));
					ibranchlistprintqcfgdtl.setField("EFFT_DATE", processData.getDate("EFFT_DATE"));
					ibranchlistprintqcfgdtl.deleteByFieldsAudit(ibranchlistprintqcfgdtlFields, ibranchlistprintqcfgdtlFieldTypes);
					DTDObject detailData = processData.getDTDObject("BRNLISTPRNQCFGHISTDTL");
					ibranchlistprintqcfgdtl.setNew(true);
					for (int i = 0; i < detailData.getRowCount(); i++) {
						ibranchlistprintqcfgdtl.setField("SL", String.valueOf(i + 1));
						ibranchlistprintqcfgdtl.setField("PRNTYPE_ID", detailData.getValue(i, 1));
						ibranchlistprintqcfgdtl.setField("PRNQ_ID", detailData.getValue(i, 3));
						if (!ibranchlistprintqcfgdtl.save()) {
							processResult.setProcessStatus(TBAProcessStatus.FAILURE);
							processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
							return;
						}
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			ibranchlistprintqcfg.close();
			ibranchlistprintqcfgdtl.close();
		}
	}

}
