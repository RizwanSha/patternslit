package patterns.config.framework.bo.access;

import java.util.Date;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.BindParameterType;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTDObject;
import patterns.config.framework.web.BackOfficeFormatUtils;

/**
 *  This program is used for configuring global print queues.
 * @version 1.0
 * @author Tulasi Thota
 * @since 19-FEB-2015 <br>
 *        <b>Modified History</b> <BR>
 *        <U><B> Sl.No Modified Date Author Modified Changes Version </B></U> <br>
 * 
 */
public class iglobalprintqcfgBO extends GenericBO {
	TBADynaSQL iglobalprintqcfg = null;
	TBADynaSQL iglobalprintqcfgdtl = null;

	public iglobalprintqcfgBO() {
	}

	public void init() {
		iglobalprintqcfg = new TBADynaSQL("GLOBALPRNQCFGHIST", true);
		iglobalprintqcfgdtl = new TBADynaSQL("GLOBALPRNQCFGHISTDTL", false);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + BackOfficeFormatUtils.getDate((Date) processData.getObject("EFFT_DATE"), BackOfficeConstants.TBA_DATE_FORMAT);
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("ENTITY_CODE"));
		tbaContext.setDisplayDetails(processData.getObject("EFFT_DATE").toString());
		iglobalprintqcfg.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		iglobalprintqcfg.setField("EFFT_DATE", processData.getObject("EFFT_DATE"));
		dataExists = iglobalprintqcfg.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				iglobalprintqcfg.setNew(true);
				if (iglobalprintqcfg.save()) {
					iglobalprintqcfgdtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
					iglobalprintqcfgdtl.setField("EFFT_DATE", processData.getObject("EFFT_DATE"));
					DTDObject detailData = processData.getDTDObject("GLOBALPRNQCFGHISTDTL");
					iglobalprintqcfgdtl.setNew(true);
					for (int i = 0; i < detailData.getRowCount(); ++i) {
						iglobalprintqcfgdtl.setField("SL", Integer.valueOf(i + 1));
						iglobalprintqcfgdtl.setField("PRNTYPE_ID", detailData.getValue(i,1));
						iglobalprintqcfgdtl.setField("PRNQ_ID", detailData.getValue(i, 3));
						if (!iglobalprintqcfgdtl.save()) {
							processResult.setProcessStatus(TBAProcessStatus.FAILURE);
							processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
						}
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			}
			else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			iglobalprintqcfg.close();
			iglobalprintqcfgdtl.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				iglobalprintqcfg.setNew(false);
				if (iglobalprintqcfg.save()) {
					String[] iglobalprintqcfgdtlFields = { "ENTITY_CODE","EFFT_DATE","SL"};
					BindParameterType[] iglobalprintqcfgFieldTypes = { BindParameterType.VARCHAR,BindParameterType.DATE,BindParameterType.INTEGER };
					iglobalprintqcfgdtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
					iglobalprintqcfgdtl.setField("EFFT_DATE", processData.getObject("EFFT_DATE"));
					iglobalprintqcfgdtl.deleteByFieldsAudit(iglobalprintqcfgdtlFields, iglobalprintqcfgFieldTypes);
					DTDObject detailData = processData.getDTDObject("GLOBALPRNQCFGHISTDTL");
					iglobalprintqcfgdtl.setNew(true);
					for (int i = 0; i < detailData.getRowCount(); ++i) {
						iglobalprintqcfgdtl.setField("SL", Integer.valueOf(i + 1));
						iglobalprintqcfgdtl.setField("PRNTYPE_ID", detailData.getValue(i,1));
						iglobalprintqcfgdtl.setField("PRNQ_ID", detailData.getValue(i, 3));
						if (!iglobalprintqcfgdtl.save()) {
							processResult.setProcessStatus(TBAProcessStatus.FAILURE);
							processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
							return;
						}
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}

		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			iglobalprintqcfg.close();
			iglobalprintqcfgdtl.close();
		}
	}
}
