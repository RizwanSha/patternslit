package patterns.config.framework.bo.access;

import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;

public class epgmctlBO extends GenericBO {
	TBADynaSQL epgmctl = null;

	public epgmctlBO() {
	}

	public void init() {
		epgmctl = new TBADynaSQL("MPGMCONFIG", true);
		primaryKey = processData.get("MPGM_ID");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("MPGM_DESCN"));
		epgmctl.setField("MPGM_ID", processData.get("MPGM_ID"));
		dataExists = epgmctl.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		throw new TBAFrameworkException("Unsupported Operation");
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				epgmctl.setNew(false);
				epgmctl.setField("MPGM_AUTH_REQD", processData.get("MPGM_AUTH_REQD"));
				epgmctl.setField("MPGM_DBL_AUTH_REQD", processData.get("MPGM_DBL_AUTH_REQD"));
				if (epgmctl.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			epgmctl.close();
		}
	}
}