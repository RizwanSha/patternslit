package patterns.config.framework.bo.inv;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.ResultSet;
import patterns.config.framework.bo.ServiceBO;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessResult;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.ContentManager;

public class rinvoiceBO extends ServiceBO {

	public rinvoiceBO() {
	}

	public void init() {

	}

	@Override
	public TBAProcessResult processRequest() {
		try {
			String excelInvNo = null;
			String pdfInvNo = null;
			if (processData.get("EXCEL_REPORT_IDENTIFIER") != null && !processData.get("EXCEL_REPORT_IDENTIFIER").equals(ContentManager.EMPTY_STRING)) {
				processData.set("REPORT_ID", processData.get("EXCEL_REPORT_IDENTIFIER"));
				excelInvNo = generateInventorySequence();
				if (!insertFileInventory(processData, "E", excelInvNo))
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			}
			if (processData.get("PDF_REPORT_IDENTIFIER") != null && !processData.get("PDF_REPORT_IDENTIFIER").equals(ContentManager.EMPTY_STRING)) {
				processData.set("REPORT_ID", processData.get("PDF_REPORT_IDENTIFIER"));
				pdfInvNo = generateInventorySequence();
				if (!insertFileInventory(processData, "P", pdfInvNo))
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			}
			if (!updateInvoiceReportIdentigfier(processData, excelInvNo, pdfInvNo))
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
		} catch (Exception e) {
			e.getLocalizedMessage();
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setAdditionalInfo(e.getLocalizedMessage());
		}
		return processResult;
	}

	private boolean updateInvoiceReportIdentigfier(DTObject inputDTO, String excelInvNo, String pdfInvNo) throws TBAFrameworkException {
		StringBuffer sqlQuery = new StringBuffer(RegularConstants.EMPTY_STRING);
		sqlQuery.append("UPDATE INVOICES SET ");
		if (inputDTO.get("PDF_REPORT_IDENTIFIER") != null && pdfInvNo != null) {
			sqlQuery.append("IS_PDF_GENERATED='1', PDF_FILE_INV_NUM=\"" + pdfInvNo + "\", PDF_REPORT_IDENTIFIER = '");
			sqlQuery.append(inputDTO.get("PDF_REPORT_IDENTIFIER") + "'");
		}
		if (inputDTO.get("EXCEL_REPORT_IDENTIFIER") != null && excelInvNo != null) {
			if (inputDTO.get("PDF_REPORT_IDENTIFIER") != null)
				sqlQuery.append(",");
			sqlQuery.append("IS_EXCEL_GENERATED='1', EXCEL_FILE_INV_NUM=\"" + excelInvNo + "\", EXCEL_REPORT_IDENTIFIER = '");
			sqlQuery.append(inputDTO.get("EXCEL_REPORT_IDENTIFIER") + "'");
		}
		sqlQuery.append(" WHERE ENTITY_CODE=? AND LEASE_PRODUCT_CODE=? AND INV_YEAR=? AND INV_MONTH=? AND INV_SERIAL=?");
		try {
			DBUtil util = getDbContext().createUtilInstance();
			util.reset();
			util.setMode(DBUtil.PREPARED);
			util.setSql(sqlQuery.toString());
			util.setLong(1, Long.parseLong(inputDTO.get("ENTITY_CODE")));
			util.setString(2, inputDTO.get("LEASE_PROD_CODE"));
			util.setLong(3, Integer.parseInt(inputDTO.get("INV_YEAR")));
			util.setLong(4, Integer.parseInt(inputDTO.get("INV_MONTH")));
			util.setLong(5, Long.parseLong(inputDTO.get("INV_SERIAL")));
			util.executeUpdate();
			util.reset();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			throw new TBAFrameworkException(e.getLocalizedMessage());
		}
	}

	private boolean insertFileInventory(DTObject inputDTO, String reportType, String invNo) throws TBAFrameworkException {
		StringBuffer sqlQuery = new StringBuffer(RegularConstants.EMPTY_STRING);
		sqlQuery.append("INSERT INTO CMNFILEINVENTORYSRC" + inputDTO.get("INV_YEAR") + "(ENTITY_CODE,FILE_INV_NUM,FILE_DATA) VALUES(?,?,?) ");
		try {
			DBUtil util = getDbContext().createUtilInstance();
			util.reset();
			util.setMode(DBUtil.PREPARED);
			util.setSql(sqlQuery.toString());
			util.setLong(1, Long.parseLong(inputDTO.get("ENTITY_CODE")));
			util.setLong(2, Long.parseLong(invNo));
			String fileLoc = getFileLocation(inputDTO);
			File file = new File(fileLoc);
			if (fileLoc != null) {
				util.setBytes(3, getFileData(file));
			} else {
				util.setString(3, null);
			}
			util.executeUpdate();
			int i = file.getName().lastIndexOf('.');
			sqlQuery = new StringBuffer(RegularConstants.EMPTY_STRING);
			sqlQuery.append("INSERT INTO CMNFILEINVENTORYSRCDTL(ENTITY_CODE,FILE_INV_NUM,FILE_NAME,FILE_LOC,IN_USE,FILE_EXTENSION) VALUES(?,?,?,?,?,?) ");
			util.reset();
			util.setMode(DBUtil.PREPARED);
			util.setSql(sqlQuery.toString());
			util.setLong(1, Long.parseLong(inputDTO.get("ENTITY_CODE")));
			util.setLong(2, Long.parseLong(invNo));
			util.setString(3, file.getName());
			util.setString(4, fileLoc);
			util.setString(5, RegularConstants.ZERO);
			util.setString(6, file.getName().substring(i + 1));
			util.executeUpdate();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			throw new TBAFrameworkException(e.getLocalizedMessage());
		}
	}

	private String getFileLocation(DTObject inputDTO) throws TBAFrameworkException, IOException {
		StringBuffer sqlQuery = new StringBuffer(RegularConstants.EMPTY_STRING);
		String fileLoc = null;
		DBUtil util = getDbContext().createUtilInstance();
		sqlQuery.append("SELECT FILE_PATH FROM REPORTDOWNLOAD WHERE ENTITY_CODE=? AND REPORT_IDENTIFIER=?");
		try {
			util.reset();
			util.setMode(DBUtil.PREPARED);
			util.setSql(sqlQuery.toString());
			util.setLong(1, Long.parseLong(inputDTO.get("ENTITY_CODE")));
			util.setString(2, inputDTO.get("REPORT_ID"));
			ResultSet rs = util.executeQuery();
			if (rs.next()) {
				fileLoc = rs.getString(1);
			}
			return fileLoc;
		} catch (Exception e) {
			e.printStackTrace();
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			util.reset();
		}
	}

	private byte[] getFileData(File file) throws Exception {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		BufferedInputStream bis = new BufferedInputStream(new FileInputStream(file));
		try {
			byte[] buffer = new byte[1048576];
			do {
				int count = bis.read(buffer);
				if (count == -1) {
					break;
				} else {
					baos.write(buffer, 0, count);
				}
			} while (true);

		} finally {
			if (bis != null)
				bis.close();
			if (baos != null)
				baos.close();
		}
		return baos.toByteArray();
	}

	private String generateInventorySequence() {
		String invSequence = null;
		DBUtil util = getDbContext().createUtilInstance();
		try {
			String sqlQuery = "SELECT FN_NEXTVAL('SEQ_FILEUPLOAD') AS INV_SEQ FROM DUAL";
			util.reset();
			util.setSql(sqlQuery);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				invSequence = rset.getString(1);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			util.reset();
		}
		return invSequence;
	}

}
