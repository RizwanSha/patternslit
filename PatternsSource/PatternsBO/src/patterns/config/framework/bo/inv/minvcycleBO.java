package patterns.config.framework.bo.inv;

/*
 *
 Author : Javeed S
 Created Date : 25-JAN-2016
 Spec Reference : PPBS/INV/0017-MINVCYCLE - Invoice Cycles Maintenance
 Modification History
 ----------------------------------------------------------------------
 Sl.No		Modified Date	Author			Modified Changes	Version
 ----------------------------------------------------------------------

 */

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;

public class minvcycleBO extends GenericBO {

	TBADynaSQL invcycle = null;

	public minvcycleBO() {
	}

	public void init() {
		invcycle = new TBADynaSQL("INVCYCLE", true);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("INV_CYCLE_NUMBER");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("INV_CYCLE_NUMBER"));
		invcycle.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		invcycle.setField("INV_CYCLE_NUMBER", processData.get("INV_CYCLE_NUMBER"));
		dataExists = invcycle.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {

		try {
			if (!isDataExists()) {
				invcycle.setNew(true);
				invcycle.setField("INV_CYCLE_NUMBER", processData.get("INV_CYCLE_NUMBER"));
				invcycle.setField("INV_RENTAL_DAY_FROM", processData.get("INV_RENTAL_DAY_FROM"));
				invcycle.setField("INV_RENTAL_DAY_UPTO", processData.get("INV_RENTAL_DAY_UPTO"));
				invcycle.setField("INV_GEN_ADV_DAYS", processData.get("INV_GEN_ADV_DAYS"));
				invcycle.setField("REMARKS", processData.get("REMARKS"));
				if (invcycle.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			invcycle.close();
		}

	}

	protected void modify() throws TBAFrameworkException {

		try {
			if (isDataExists()) {
				invcycle.setNew(false);
				invcycle.setField("INV_CYCLE_NUMBER", processData.get("INV_CYCLE_NUMBER"));
				invcycle.setField("INV_RENTAL_DAY_FROM", processData.get("INV_RENTAL_DAY_FROM"));
				invcycle.setField("INV_RENTAL_DAY_UPTO", processData.get("INV_RENTAL_DAY_UPTO"));
				invcycle.setField("INV_GEN_ADV_DAYS", processData.get("INV_GEN_ADV_DAYS"));
				invcycle.setField("REMARKS", processData.get("REMARKS"));
				if (invcycle.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
					invcycle.sendInterfaceOutwardMessage();
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			invcycle.close();
		}

	}
}
