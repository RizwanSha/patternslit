package patterns.config.framework.bo.inv;



import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.MessageParam;
import patterns.config.framework.bo.ServiceBO;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessResult;
import patterns.config.framework.process.TBAProcessStatus;

public class einvgenprocessBO extends ServiceBO {

	

	public einvgenprocessBO() {
	}

	public void init() {
		
	}

	@Override
	public TBAProcessResult processRequest() {
		getLogger().logDebug("processRequest Begin");
		try{
			  if(!acquireLock()){
				  processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				  MessageParam messageParam = new MessageParam();
				  messageParam.setCode(BackOfficeErrorCodes.UNABLE_TO_ACQUIRE_LOCK);
				  processResult.setAdditionalInfo(messageParam);
				  processResult.setErrorCode(BackOfficeErrorCodes.PROCESS_NOT_SUCCESS);
			  }else{
				  invoiceGenProcess();
			  }	  
		}catch(Exception e){
			e.getLocalizedMessage();
			getLogger().logError(e.getLocalizedMessage());
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.PROCESS_NOT_SUCCESS);
			processResult.setAdditionalInfo(e.getLocalizedMessage());
		}
		getLogger().logDebug("processRequest End");
		return processResult;
	}
	
	private void invoiceGenProcess() throws TBAFrameworkException {
		getLogger().logDebug("invoiceGenProcess() Begin");
		DBUtil dbutil = getDbContext().createUtilInstance();
		try {
			dbutil.reset();
			dbutil.setMode(DBUtil.CALLABLE);
			dbutil.setSql("{CALL SP_INV_COMP_GENERATION(?,?,?,?,?,?,?,?,?,?,?,?,?)}");
			dbutil.setLong(1, Long.parseLong(processData.get("ENTITY_CODE")));
			dbutil.setInt(2, Integer.parseInt(processData.get("INV_CYCLE_NUMBER")));
			dbutil.setInt(3, Integer.parseInt(processData.get("INV_MONTH")));
			dbutil.setInt(4, Integer.parseInt(processData.get("INV_YEAR")));
			dbutil.setDate(5,new java.sql.Date(processData.getDate("GENERATION_DATE").getTime()));
			dbutil.setString(6,processData.get("BRANCH"));
			dbutil.setString(7,processData.get("TEMPLATE_CODE"));
			dbutil.setString(8,processData.get("PRODUCT_CODE"));
			dbutil.setString(9,processData.get("LESSE_CODE"));
			dbutil.setString(10,processData.get("USER_ID"));
			dbutil.registerOutParameter(11, Types.DATE);
			dbutil.registerOutParameter(12, Types.DATE);
			dbutil.registerOutParameter(13, Types.VARCHAR);
			dbutil.execute();
			getLogger().logDebug("SP_INV_COMP_GENERATION FROM DATE:"+dbutil.getString(11));
			getLogger().logDebug("SP_INV_COMP_GENERATION UPTO DATE:"+dbutil.getString(12));
			getLogger().logDebug("SP_INV_COMP_GENERATION STATUS:"+dbutil.getString(13));
			if (!dbutil.getString(13).equals(RegularConstants.SP_SUCCESS)){
				processResult.setErrorCode(BackOfficeErrorCodes.PROCESS_NOT_SUCCESS);
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				return;
			}	
			java.sql.Date fromDate=dbutil.getDate(11);
			java.sql.Date uptoDate=dbutil.getDate(12);
			
			
			dbutil.reset();
			dbutil.setMode(DBUtil.CALLABLE);
			dbutil.setSql("{CALL SP_INV_COMP_GROUP(?,?,?,?,?,?)}");
			dbutil.setLong(1, Long.parseLong(processData.get("ENTITY_CODE")));
			dbutil.setDate(2, fromDate);
			dbutil.setDate(3, uptoDate);
			dbutil.registerOutParameter(4, Types.BIGINT);
			dbutil.registerOutParameter(5, Types.INTEGER);
			dbutil.registerOutParameter(6, Types.VARCHAR);
			dbutil.execute();
			getLogger().logDebug("SP_INV_COMP_GROUP GENERATED SERIAL:"+dbutil.getString(4));
			getLogger().logDebug("SP_INV_COMP_GROUP STATUS:"+dbutil.getString(5));
			if (dbutil.getString(6).equals(RegularConstants.SP_SUCCESS)){
				processResult.setErrorCode(BackOfficeErrorCodes.PROCESS_SUCCESS);
				processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
				processResult.setGeneratedID(dbutil.getString(4));
				
				MessageParam messageParam = new MessageParam();
				messageParam.setCode(BackOfficeErrorCodes.INVOICE_GROUP_MESSGAE);
				List<String> parameters =new ArrayList<String>();
				parameters.add(dbutil.getString(5));//Record Count
				parameters.add(dbutil.getString(4));//Temp Serial
				messageParam.setParameters(parameters);
				processResult.setAdditionalInfo(messageParam);
			}else{
				processResult.setErrorCode(BackOfficeErrorCodes.PROCESS_NOT_SUCCESS);
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getLogger().logError(e.getLocalizedMessage());
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			dbutil.reset();
		}
		getLogger().logDebug("invoiceGenProcess() End");
	}
	
	
	private boolean acquireLock() throws TBAFrameworkException {
		getLogger().logDebug("acquireLock begin");
		DBUtil dbutil = getDbContext().createUtilInstance();
		try {
			dbutil.reset();
			dbutil.setMode(DBUtil.CALLABLE);
			dbutil.setSql("{CALL SP_ACQUIRE_LOCK( ?,?,?,?,?,?)}");
			dbutil.setString(1, processData.get("ENTITY_CODE"));
			dbutil.setString(2, processData.get("PROCESS_ID"));
			dbutil.registerOutParameter(3, Types.INTEGER);
			dbutil.registerOutParameter(4, Types.INTEGER);
			dbutil.registerOutParameter(5, Types.INTEGER);
			dbutil.registerOutParameter(6, Types.VARCHAR);
			dbutil.execute();
			int V_LOCK_STATUS = dbutil.getInt(5);
			getLogger().logDebug("SP_ACQUIRE_LOCK STATUS :"+V_LOCK_STATUS);
			String V_LOCK_ERR_MSG = dbutil.getString(6) == RegularConstants.NULL ? RegularConstants.EMPTY_STRING : dbutil.getString(6);
			getLogger().logDebug("SP_ACQUIRE_LOCK MSG :"+V_LOCK_ERR_MSG);
			if (V_LOCK_STATUS == 1)
				//throw new TBAFrameworkException(V_LOCK_ERR_MSG);
				return false;

		} catch (Exception e) {
			e.printStackTrace();
			getLogger().logError(e.getLocalizedMessage());
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			dbutil.reset();
		}
		getLogger().logDebug("acquireLock end");
		return true;
	}
}
