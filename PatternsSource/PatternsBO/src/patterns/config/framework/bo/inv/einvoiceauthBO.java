package patterns.config.framework.bo.inv;

import java.sql.Date;
import java.sql.Types;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.MessageParam;
import patterns.config.framework.bo.ServiceBO;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.CM_LOVREC;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessResult;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTDObject;
/*
 *
 Author : Raja E
 Created Date : 03-02-2017
 Spec Reference : EINVOICEAUTH
 Modification History
 -----------------------------------------------------------------------------------------------------
 Sl.No		Modified Date	Author			Modified Changes	                      Version
 1			24-03-2017		Pavan 			removed invoking sp SP_INV_GEN_GROUP_COMP	1.1	
 alter the screen grid column and allowed 
 bulk Authorization				
 -------------------------------------------------------------------------------------------------------

 */
import patterns.config.framework.web.BackOfficeFormatUtils;

public class einvoiceauthBO extends ServiceBO {
	public einvoiceauthBO() {
	}

	public void init() {
		
	}

	@Override
	public TBAProcessResult processRequest() {
		getLogger().logDebug("authorizedRecord() Begin");
		try {
			  if(!acquireLock()){
				  processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				  MessageParam messageParam = new MessageParam();
				  messageParam.setCode(BackOfficeErrorCodes.UNABLE_TO_ACQUIRE_LOCK);
				  processResult.setAdditionalInfo(messageParam);
				  processResult.setErrorCode(BackOfficeErrorCodes.PROCESS_NOT_SUCCESS);
			  }else{
				  insertInvAuth();
				  insertInvAuthdtl();
				  insertPETable();
				  processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
				  processResult.setErrorCode(BackOfficeErrorCodes.REQUEST_SUBMITTED);  
			  }	 
		} catch (Exception e) {
			e.getLocalizedMessage();
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.PROCESS_NOT_SUCCESS);
			if(processResult.getAdditionalInfo().equals(RegularConstants.EMPTY_STRING))
				processResult.setAdditionalInfo(e.getLocalizedMessage());
		}
		getLogger().logDebug("authorizedRecord() End");
		return processResult;
	}

	private boolean acquireLock() throws TBAFrameworkException {
		getLogger().logDebug("acquireLock begin");
		DBUtil dbutil = getDbContext().createUtilInstance();
		try {
			dbutil.reset();
			dbutil.setMode(DBUtil.CALLABLE);
			dbutil.setSql("{CALL SP_ACQUIRE_LOCK( ?,?,?,?,?,?)}");
			dbutil.setString(1, processData.get("ENTITY_CODE"));
			dbutil.setString(2, processData.get("PROCESS_ID"));
			dbutil.registerOutParameter(3, Types.INTEGER);
			dbutil.registerOutParameter(4, Types.INTEGER);
			dbutil.registerOutParameter(5, Types.INTEGER);
			dbutil.registerOutParameter(6, Types.VARCHAR);
			dbutil.execute();
			int V_LOCK_STATUS = dbutil.getInt(5);
			getLogger().logDebug("SP_ACQUIRE_LOCK STATUS :"+V_LOCK_STATUS);
			String V_LOCK_ERR_MSG = dbutil.getString(6) == RegularConstants.NULL ? RegularConstants.EMPTY_STRING : dbutil.getString(6);
			getLogger().logDebug("SP_ACQUIRE_LOCK MSG :"+V_LOCK_ERR_MSG);
			if (V_LOCK_STATUS == 1)
				//throw new TBAFrameworkException(V_LOCK_ERR_MSG);
				return false;

		} catch (Exception e) {
			e.printStackTrace();
			getLogger().logError(e.getLocalizedMessage());
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			dbutil.reset();
		}
		getLogger().logDebug("acquireLock end");
		return true;
	}

	public void insertInvAuth() throws TBAFrameworkException {
		getLogger().logDebug("insertInvAuth() Begin");
		DBUtil dbutil = getDbContext().createUtilInstance();
		DBUtil dbutiInvAuth = getDbContext().createUtilInstance();
		String authpk=RegularConstants.EMPTY_STRING;
		String invauthpk=RegularConstants.EMPTY_STRING;
		int serial = 0;
		long fromInvSerial =0;
		long uptoInvSerial =0;
		try {
			authpk=processData.get("ENTITY_CODE")+RegularConstants.PK_SEPARATOR+BackOfficeFormatUtils.getDate((java.sql.Date)processData.getObject("ENTRY_DATE"), BackOfficeConstants.TBA_DATE_FORMAT);
			dbutil.reset();
			dbutil.setMode(DBUtil.CALLABLE);
			dbutil.setSql("{CALL FN_GET_TBA_REFSL(?,?,?)}");
			dbutil.setString(1, processData.get("PROCESS_ID"));
			dbutil.setString(2, authpk);
			dbutil.registerOutParameter(3, Types.INTEGER);
			dbutil.execute();
			serial = dbutil.getInt(3);
			getLogger().logDebug("ENTRY_SL:"+String.valueOf(serial));
			processData.set("ENTRY_SL", String.valueOf(serial));
			
			if(processData.get("ACTION_TYPE").equals(RegularConstants.AUTHORIZED)){
				invauthpk=processData.get("ENTITY_CODE")+RegularConstants.PK_SEPARATOR+processData.get("PRODUCT_CODE")+RegularConstants.PK_SEPARATOR+processData.get("INV_YEAR")+RegularConstants.PK_SEPARATOR+processData.get("INV_MONTH");
				dbutil.reset();
				dbutil.setMode(DBUtil.CALLABLE);
				dbutil.setSql("{CALL SP_GET_TBA_REFSL_RANGE(?,?,?,?,?)}");
				dbutil.setString(1, processData.get("PROCESS_ID"));
				dbutil.setString(2, invauthpk);
				dbutil.setInt(3, processData.getDTDObject("EINVOICEAUTHDTL").getRowCount());
				dbutil.registerOutParameter(4, Types.BIGINT);
				dbutil.registerOutParameter(5, Types.BIGINT);
				dbutil.execute();
				fromInvSerial = dbutil.getLong(4);
				uptoInvSerial = dbutil.getLong(5);
			}else{
				fromInvSerial = 0;
				uptoInvSerial = 0;
			}
			
			getLogger().logDebug("FROM_INVOICE_SL:"+String.valueOf(fromInvSerial));
			getLogger().logDebug("UPTO_INVOICE_SL:"+String.valueOf(uptoInvSerial));
			processData.set("FROM_INVOICE_SL",String.valueOf(fromInvSerial));
			processData.set("UPTO_INVOICE_SL",String.valueOf(uptoInvSerial));
			processData.set("ENTRY_SL", String.valueOf(serial));

			dbutiInvAuth.reset();
			dbutiInvAuth.setMode(DBUtil.PREPARED);
			dbutiInvAuth.setSql("INSERT INTO INVAUTH (ENTITY_CODE,ENTRY_DATE,ENTRY_SL,LEASE_PRODUCT_CODE,INV_YEAR,INV_MONTH,ACTION_TYPE,FROM_INVOICE_SL,UPTO_INVOICE_SL,REMARKS,E_STATUS,AUTH_ON,PROC_BY) VALUES (?,?,?,?,?,?,?,?,?,?,?,FN_GETCDT(?),?)");
			dbutiInvAuth.setLong(1, Long.parseLong(processData.get("ENTITY_CODE")));
			dbutiInvAuth.setDate(2, (Date) processData.getObject("ENTRY_DATE"));
			dbutiInvAuth.setInt(3, serial);
			dbutiInvAuth.setString(4, processData.get("PRODUCT_CODE"));
			dbutiInvAuth.setInt(5, Integer.parseInt(processData.get("INV_YEAR")));
			dbutiInvAuth.setInt(6, Integer.parseInt(processData.get("INV_MONTH")));
			dbutiInvAuth.setString(7, processData.get("ACTION_TYPE"));
			dbutiInvAuth.setLong(8, fromInvSerial);
			dbutiInvAuth.setLong(9, uptoInvSerial);
			dbutiInvAuth.setString(10, processData.get("REMARKS"));
			dbutiInvAuth.setString(11, RegularConstants.AUTHORIZED);//E_STATUS
			dbutiInvAuth.setLong(12, Long.parseLong(processData.get("ENTITY_CODE")));//AUTH_ON
			dbutiInvAuth.setString(13, processData.get("USER_ID"));
			dbutiInvAuth.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			dbutil.reset();
			dbutiInvAuth.reset();
		}
		getLogger().logDebug("insertInvAuth() eND");
	}
	
	private void insertInvAuthdtl() throws TBAFrameworkException {
		getLogger().logDebug("insertInvAuthdtl() Begin");
		Date grpentryDate = null;
		int grpentrySl = 0;
		long invoiceSl =0;
		DBUtil dbutilInvCompGrp = getDbContext().createUtilInstance();
		DBUtil dbutilInvAuthdtl = getDbContext().createUtilInstance();
		try {
			dbutilInvCompGrp.setMode(DBUtil.PREPARED);
			dbutilInvCompGrp.setSql("UPDATE INVCOMPGROUP SET ACTION_TYPE=?,AUTH_ENTRY_DATE=?,AUTH_ENTRY_SL=? WHERE ENTITY_CODE=? AND ENTRY_DATE =? AND ENTRY_SL =? AND AUTH_ENTRY_DATE IS NULL");
			dbutilInvAuthdtl.setMode(DBUtil.PREPARED);
			dbutilInvAuthdtl.setSql("INSERT INTO INVAUTHDTL (ENTITY_CODE,ENTRY_DATE,ENTRY_SL,DTL_SL,GRP_ENTRY_DATE,GRP_ENTRY_SL,INVOICE_SL,PROC_STATUS) VALUES (?,?,?,?,?,?,?,?)");
			DTDObject detailData = processData.getDTDObject("EINVOICEAUTHDTL");
			getLogger().logDebug("DETAILDATA " + processData.getDTDObject("EINVOICEAUTHDTL"));
			for (int i = 0; i < detailData.getRowCount(); i++) {
				getLogger().logDebug("GRP_ENTRY_DATE " + detailData.getValue(i, "GRP_ENTRY_DATE"));
				getLogger().logDebug("GRP_ENTRY_SL " + detailData.getValue(i, "GRP_ENTRY_SL"));
				grpentryDate = new java.sql.Date(BackOfficeFormatUtils.getDate(detailData.getValue(i, "GRP_ENTRY_DATE"), BackOfficeConstants.TBA_DATE_FORMAT).getTime());
				grpentrySl = Integer.parseInt(detailData.getValue(i, "GRP_ENTRY_SL"));
				if(processData.get("ACTION_TYPE").equals(RegularConstants.AUTHORIZED)){
					invoiceSl = Long.parseLong(processData.get("FROM_INVOICE_SL"))+i;
					getLogger().logDebug("INVOICE_SL " +invoiceSl);
				}else{
					invoiceSl = 0;
				}
				dbutilInvCompGrp.setString(1, processData.get("ACTION_TYPE"));
				dbutilInvCompGrp.setDate(2, (Date) processData.getObject("ENTRY_DATE"));
				dbutilInvCompGrp.setInt(3, Integer.parseInt(processData.get("ENTRY_SL")));
				dbutilInvCompGrp.setLong(4, Long.parseLong(processData.get("ENTITY_CODE")));
				dbutilInvCompGrp.setDate(5, grpentryDate);
				dbutilInvCompGrp.setInt(6, grpentrySl);
				if (dbutilInvCompGrp.executeUpdate() == 1) {
					dbutilInvAuthdtl.setLong(1, Long.parseLong(processData.get("ENTITY_CODE")));
					dbutilInvAuthdtl.setDate(2, (Date) processData.getObject("ENTRY_DATE"));
					dbutilInvAuthdtl.setInt(3, Integer.parseInt(processData.get("ENTRY_SL")));
					dbutilInvAuthdtl.setInt(4, i+1);
					dbutilInvAuthdtl.setDate(5, grpentryDate);
					dbutilInvAuthdtl.setInt(6, grpentrySl);
					dbutilInvAuthdtl.setLong(7, invoiceSl);
					dbutilInvAuthdtl.setString(8, CM_LOVREC.EINVOICEAUTH_JOB_STATUS_T);//PE table also gets update in the current transaction 
					dbutilInvAuthdtl.executeUpdate();
				} else {
					getLogger().logDebug("One or more entries already processed");
					MessageParam messageParam = new MessageParam();
					messageParam.setCode(BackOfficeErrorCodes.ENTRIES_ALREADY_PROCESSED);
					processResult.setAdditionalInfo(messageParam);
					throw new TBAFrameworkException("One or more entries already processed");
				}
				
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			dbutilInvCompGrp.reset();
			dbutilInvAuthdtl.reset();
		}
		getLogger().logDebug("insertInvAuthdtl() End");
	}

	private void insertPETable() throws TBAFrameworkException {
		getLogger().logDebug("insertPETable() Begin");
		DBUtil dbutilInvPE = getDbContext().createUtilInstance();
		try {
			dbutilInvPE.setMode(DBUtil.PREPARED);
			if(processData.get("ACTION_TYPE").equals(RegularConstants.AUTHORIZED)){
				dbutilInvPE.setSql("INSERT INTO INVAUTHPE (ENTITY_CODE,GRP_ENTRY_DATE,GRP_ENTRY_SL,AUTH_ENTRY_DATE,AUTH_ENTRY_SL,INVOICE_SL) SELECT ENTITY_CODE,GRP_ENTRY_DATE,GRP_ENTRY_SL,ENTRY_DATE,ENTRY_SL,INVOICE_SL FROM INVAUTHDTL WHERE ENTITY_CODE=? AND ENTRY_DATE=? AND ENTRY_SL=?");
			}else if(processData.get("ACTION_TYPE").equals(RegularConstants.REJECTED)){
				dbutilInvPE.setSql("INSERT INTO INVREJPE (ENTITY_CODE,GRP_ENTRY_DATE,GRP_ENTRY_SL,AUTH_ENTRY_DATE,AUTH_ENTRY_SL) SELECT ENTITY_CODE,GRP_ENTRY_DATE,GRP_ENTRY_SL,ENTRY_DATE,ENTRY_SL FROM INVAUTHDTL WHERE ENTITY_CODE=? AND ENTRY_DATE=? AND ENTRY_SL=?");
			}	
			dbutilInvPE.setLong(1, Long.parseLong(processData.get("ENTITY_CODE")));
			dbutilInvPE.setDate(2, (Date) processData.getObject("ENTRY_DATE"));
			dbutilInvPE.setInt(3, Integer.parseInt(processData.get("ENTRY_SL")));
			dbutilInvPE.executeUpdate();
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			dbutilInvPE.reset();
		}
		getLogger().logDebug("insertPETable() End");
	}
}
