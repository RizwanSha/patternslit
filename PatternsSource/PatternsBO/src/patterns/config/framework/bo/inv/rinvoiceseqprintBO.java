package patterns.config.framework.bo.inv;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.sql.ResultSet;
import java.sql.Types;

import patterns.config.framework.bo.ServiceBO;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessResult;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTObject;

public class rinvoiceseqprintBO extends ServiceBO {

	public rinvoiceseqprintBO() {
	}

	public void init() {

	}

	@Override
	public TBAProcessResult processRequest() {
		try {
			String fileInvNum = generateInventorySequence();
			if (!insertIntoInvoiceseqprint(processData, fileInvNum))
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			if (!insertFileInventory(processData, fileInvNum))
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
		} catch (Exception e) {
			e.getLocalizedMessage();
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setAdditionalInfo(e.getLocalizedMessage());
		}
		return processResult;
	}

	private boolean insertIntoInvoiceseqprint(DTObject inputDTO, String fileInvNum) throws TBAFrameworkException {
		StringBuffer sqlQuery = new StringBuffer(RegularConstants.EMPTY_STRING);
		try {
			int serial=getTBAReferenceSerial(inputDTO);
			sqlQuery.append("INSERT INTO INVOICESEQPRINT (ENTITY_CODE,INV_CYCLE_NUMBER,INV_YEAR,INV_MONTH,EXEC_SL,NOF_INVOICES,PRINT_MODE,NOF_INV_IN_SPLIT,DOWNLOAD_FILE_INVENTORY)");
			sqlQuery.append(" VALUES(?,?,?,?,?,?,?,?,?)");
			DBUtil util = getDbContext().createUtilInstance();
			util.reset();
			util.setMode(DBUtil.PREPARED);
			util.setSql(sqlQuery.toString());
			util.setLong(1, Long.parseLong(inputDTO.get("ENTITY_CODE")));
			util.setInt(2, Integer.parseInt(inputDTO.get("INV_CYCLE_NUMBER")));
			util.setInt(3, Integer.parseInt(inputDTO.get("YEAR")));
			util.setInt(4, Integer.parseInt(inputDTO.get("MONTH")));
			util.setInt(5, serial);
			util.setInt(6, Integer.parseInt(inputDTO.get("NO_OF_INVOICES")));
			util.setString(7, inputDTO.get("PRINT_MODE"));
			util.setInt(8, Integer.parseInt(inputDTO.get("NO_OF_INVOICES_IN_SPLIT")));
			util.setLong(9, Long.parseLong(fileInvNum));
			util.executeUpdate();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			throw new TBAFrameworkException(e.getLocalizedMessage());
		}
	}

	private boolean insertFileInventory(DTObject inputDTO, String fileInvNum) throws TBAFrameworkException {
		StringBuffer sqlQuery = new StringBuffer(RegularConstants.EMPTY_STRING);
		sqlQuery.append("INSERT INTO CMNFILEINVENTORYSRC" + inputDTO.get("FIN_YEAR") + "(ENTITY_CODE,FILE_INV_NUM,FILE_DATA) VALUES(?,?,?) ");
		try {
			DBUtil util = getDbContext().createUtilInstance();
			util.reset();
			util.setMode(DBUtil.PREPARED);
			util.setSql(sqlQuery.toString());
			util.setLong(1, Long.parseLong(inputDTO.get("ENTITY_CODE")));
			util.setLong(2, Long.parseLong(fileInvNum));
			String fileLoc = inputDTO.get("FILE_PATH");
			File file = new File(fileLoc);
			if (fileLoc != null) {
				util.setBytes(3, getFileData(file));
			} else {
				util.setString(3, null);
			}
			util.executeUpdate();
			int i = file.getName().lastIndexOf('.');
			sqlQuery = new StringBuffer(RegularConstants.EMPTY_STRING);
			sqlQuery.append("INSERT INTO CMNFILEINVENTORYSRCDTL(ENTITY_CODE,FILE_INV_NUM,FILE_NAME,FILE_LOC,IN_USE,FILE_EXTENSION) VALUES(?,?,?,?,?,?) ");
			util.reset();
			util.setMode(DBUtil.PREPARED);
			util.setSql(sqlQuery.toString());
			util.setLong(1, Long.parseLong(inputDTO.get("ENTITY_CODE")));
			util.setLong(2, Long.parseLong(fileInvNum));
			util.setString(3, file.getName());
			util.setString(4, fileLoc);
			util.setString(5, RegularConstants.ZERO);
			util.setString(6, file.getName().substring(i + 1));
			util.executeUpdate();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			throw new TBAFrameworkException(e.getLocalizedMessage());
		}
	}

	private byte[] getFileData(File file) throws Exception {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		BufferedInputStream bis = new BufferedInputStream(new FileInputStream(file));
		try {
			byte[] buffer = new byte[1048576];
			do {
				int count = bis.read(buffer);
				if (count == -1) {
					break;
				} else {
					baos.write(buffer, 0, count);
				}
			} while (true);

		} finally {
			if (bis != null)
				bis.close();
			if (baos != null)
				baos.close();
		}
		return baos.toByteArray();
	}

	private String generateInventorySequence() throws TBAFrameworkException {
		String invSequence = null;
		DBUtil util = getDbContext().createUtilInstance();
		try {
			String sqlQuery = "SELECT FN_NEXTVAL('SEQ_FILEUPLOAD') AS INV_SEQ FROM DUAL";
			util.reset();
			util.setSql(sqlQuery);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				invSequence = rset.getString(1);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			util.reset();
		}
		return invSequence;
	}

	public int getTBAReferenceSerial(DTObject inputDTO) throws TBAFrameworkException {
		String sourceKey = inputDTO.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + inputDTO.get("INV_CYCLE_NUMBER") + RegularConstants.PK_SEPARATOR + inputDTO.get("YEAR") + RegularConstants.PK_SEPARATOR + inputDTO.get("MONTH");
		String programId = inputDTO.get("PROGRAM_ID");
		DBUtil util = getDbContext().createUtilInstance();
		int serial = 0;

		try {
			util.reset();
			util.setMode(DBUtil.CALLABLE);
			String sql = "CALL FN_GET_TBA_REFSL(?,?,?)";
			util.setSql(sql);
			util.setString(1, programId);
			util.setString(2, sourceKey);
			util.registerOutParameter(3, Types.INTEGER);
			util.execute();
			serial = util.getInt(3);

		} catch (Exception e) {
			e.printStackTrace();
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			util.reset();
		}
		return serial;
	}

}
