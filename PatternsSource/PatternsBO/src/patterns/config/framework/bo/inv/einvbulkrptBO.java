package patterns.config.framework.bo.inv;

import java.util.ArrayList;
import java.util.List;

import patterns.config.framework.bo.AdditionalDetailInfo;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.bo.MessageParam;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAActionType;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTObject;
/*
 * The program will be used to Insert record to INVBULKRPTPE table. 
 *
 Author : Pavan kumar.R
 Created Date : 08-April-2017
 Spec Reference PSD-INV-EINVBULKRPT
 Modification History
 -----------------------------------------------------------------------------------------------------
 Sl.No		Modified Date	Author			Modified Changes					Version
 
 -----------------------------------------------------------------------------------------------------

 */
import patterns.config.framework.web.BackOfficeFormatUtils;

public class einvbulkrptBO extends GenericBO {

	TBADynaSQL einvbulkrpt = null;
	long entryDateSl = 0;

	public void init() {
		einvbulkrpt = new TBADynaSQL("INVOICEBULKRPT", true);
		if (tbaContext.getProcessAction().getActionType().equals(TBAActionType.ADD) && !tbaContext.getProcessAction().isRectification()) {
			DTObject input = new DTObject();
			input.set("SOURCE_KEY", processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + BackOfficeFormatUtils.getDate(processData.getDate("ENTRY_DATE"), BackOfficeConstants.TBA_DATE_FORMAT));
			input.set("PGM_ID", tbaContext.getProcessID());
			entryDateSl = einvbulkrpt.getTBAReferenceSerial(input);
			processData.set("ENTRY_SL", Long.toString(entryDateSl));
		}
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + BackOfficeFormatUtils.getDate(processData.getDate("ENTRY_DATE"), BackOfficeConstants.TBA_DATE_FORMAT) + RegularConstants.PK_SEPARATOR + processData.get("ENTRY_SL");
		einvbulkrpt.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		einvbulkrpt.setField("ENTRY_DATE", processData.getDate("ENTRY_DATE"));
		einvbulkrpt.setField("ENTRY_SL", processData.get("ENTRY_SL"));
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(primaryKey);
		dataExists = einvbulkrpt.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (tbaContext.getProcessAction().isRectification()) {
				einvbulkrpt.setNew(false);
				if (!isDataExists()) {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
				}
			} else {
				einvbulkrpt.setNew(true);
				if (isDataExists()) {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
				}
			}
			einvbulkrpt.setField("INV_CYCLE_NUMBER", processData.get("INV_CYCLE_NUMBER"));
			einvbulkrpt.setField("INV_YEAR", processData.get("INV_YEAR"));
			einvbulkrpt.setField("INV_MONTH", processData.get("INV_MONTH"));
			einvbulkrpt.setField("PRODUCT_CODE", processData.getObject("PRODUCT_CODE"));
			einvbulkrpt.setField("REMARKS", processData.get("REMARKS"));
			if (einvbulkrpt.save()) {
				if (!tbaContext.getProcessAction().isRectification()) {
					AdditionalDetailInfo additionalInfo = new AdditionalDetailInfo();
					additionalInfo.setHeading(new MessageParam(BackOfficeErrorCodes.RESULT_REFERENCE_HEADER));
					List<MessageParam> key = new ArrayList<MessageParam>();
					additionalInfo.setKey(key);
					List<String> value = new ArrayList<String>();
					key.add(new MessageParam(BackOfficeErrorCodes.REFERENCE_ENTRY_DATE));
					value.add(processData.get("ENTRY_DATE"));
					key.add(new MessageParam(BackOfficeErrorCodes.REFERENCE_ENTRY_SERIAL));
					value.add(processData.get("ENTRY_SL"));
					additionalInfo.setValue(value);
					processResult.setAdditionalInfo(additionalInfo);
				}
				processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			einvbulkrpt.close();
		}
	}

	@Override
	protected void modify() throws TBAFrameworkException {
		// TODO Auto-generated method stub

	}
}
