package patterns.config.framework.bo.inv;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import patterns.config.framework.bo.AdditionalDetailInfo;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.bo.MessageParam;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAActionType;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTDObject;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.framework.web.ajax.ContentManager;

public class einvoicecanBO extends GenericBO {

	TBADynaSQL einvoicecan = null;
	TBADynaSQL einvoicecandtl = null;

	public void init() {
		long eleaseSl = 0;
		einvoicecan = new TBADynaSQL("INVOICECAN", true);
		einvoicecandtl = new TBADynaSQL("INVOICECANDTL", false);
		if (tbaContext.getProcessAction().getActionType().equals(TBAActionType.ADD) && !tbaContext.getProcessAction().isRectification()) {
			DTObject input = new DTObject();
			input.set("SOURCE_KEY", processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + BackOfficeFormatUtils.getDate(processData.getDate("ENTRY_DATE"), BackOfficeConstants.TBA_DATE_FORMAT));
			input.set("PGM_ID", tbaContext.getProcessID());
			eleaseSl = einvoicecan.getTBAReferenceSerial(input);
			processData.set("ENTRY_SL", Long.toString(eleaseSl));
		}
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + BackOfficeFormatUtils.getDate(processData.getDate("ENTRY_DATE"), BackOfficeConstants.TBA_DATE_FORMAT) + RegularConstants.PK_SEPARATOR + processData.get("ENTRY_SL");
		einvoicecan.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		einvoicecan.setField("ENTRY_DATE", processData.getDate("ENTRY_DATE"));
		einvoicecan.setField("ENTRY_SL", processData.get("ENTRY_SL"));
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("ENTRY_SL"));
		dataExists = einvoicecan.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			einvoicecan.setNew(true);
			if (isDataExists()) {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
			einvoicecan.setField("REMARKS", processData.get("REMARKS"));
			if (einvoicecan.save()) {
				einvoicecandtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
				einvoicecandtl.setField("ENTRY_DATE", processData.getDate("ENTRY_DATE"));
				einvoicecandtl.setField("ENTRY_SL", processData.get("ENTRY_SL"));
				DTDObject detailData = processData.getDTDObject("xmleinvoiceCanGrid");
				einvoicecandtl.setNew(true);
				int count = 1;
				for (int i = 0; i < detailData.getRowCount(); ++i) {
						einvoicecandtl.setField("DTL_SL", count);
						einvoicecandtl.setField("INV_YEAR", detailData.getValue(i, 1));
						einvoicecandtl.setField("INV_MONTH", detailData.getValue(i, 2));
						einvoicecandtl.setField("LEASE_PRODUCT_CODE", detailData.getValue(i, 3));
						einvoicecandtl.setField("INV_SERIAL", detailData.getValue(i, 13));
						if (!einvoicecandtl.save()) {
							processResult.setProcessStatus(TBAProcessStatus.FAILURE);
							processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
							return;
						}
						count++;
						updateInvoiceDtails(detailData.getValue(i, 3),detailData.getValue(i, 1),detailData.getValue(i, 2),detailData.getValue(i, 13));
				}
				AdditionalDetailInfo additionalInfo = new AdditionalDetailInfo();
				additionalInfo.setHeading(new MessageParam(BackOfficeErrorCodes.EINVOICECAN_GEN_RESULT));
				List<MessageParam> key = new ArrayList<MessageParam>();
				additionalInfo.setKey(key);
				List<String> value = new ArrayList<String>();
				key.add(new MessageParam(BackOfficeErrorCodes.EINVOICECAN_REF_DATE));
				value.add(processData.get("ENTRY_DATE"));
				key.add(new MessageParam(BackOfficeErrorCodes.EINVOICECAN_REF_SERIAL));
				value.add(processData.get("ENTRY_SL"));
				additionalInfo.setValue(value);
				processResult.setAdditionalInfo(additionalInfo);
				processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			einvoicecan.close();
		}
	}

	@Override
	protected void modify() throws TBAFrameworkException {
		// TODO Auto-generated method stub

	}

	protected void updateInvoiceDtails(String productCode,String invYear,String invMonth,String invSerial) throws TBAFrameworkException {
		String sqlQuery = ContentManager.EMPTY_STRING;
		sqlQuery = "UPDATE INVOICES SET CANCEL_ENTRY_DATE=?,CANCEL_ENTRY_SL=? WHERE ENTITY_CODE =? AND LEASE_PRODUCT_CODE=? AND INV_YEAR=? AND INV_MONTH=? AND INV_SERIAL=? AND CANCEL_ENTRY_DATE IS NULL";
		DBUtil util = getDbContext().createUtilInstance();
		Date entryDate = new java.sql.Date(BackOfficeFormatUtils.getDate(processData.get("ENTRY_DATE"), BackOfficeConstants.TBA_DATE_FORMAT).getTime());
		try {
			util.setMode(DBUtil.PREPARED);
			util.setSql(sqlQuery);
			util.setDate(1, entryDate);
			util.setInt(2, Integer.parseInt(processData.get("ENTRY_SL")));
			util.setString(3, processData.get("ENTITY_CODE"));
			util.setString(4, productCode);
			util.setInt(5, Integer.parseInt(invYear));
			util.setInt(6, Integer.parseInt(invMonth));
			util.setLong(7, Long.parseLong(invSerial));
			int count = util.executeUpdate();
			if (count != 1) {
				throw new TBAFrameworkException("Selected Invoice already cancelled");
			}

		} catch (Exception e) {
			e.printStackTrace();
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			util.reset();
		}
	}
}
