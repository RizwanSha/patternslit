package patterns.config.framework.bo.inv;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.BindParameterType;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTDObject;

public class minvtemplateBO extends GenericBO {
	TBADynaSQL minvtemplate = null;
	TBADynaSQL minvtemplatedtl = null;
	TBADynaSQL minvtemplatecodeconbasics = null;

	public minvtemplateBO() {
	}

	public void init() {
		minvtemplate = new TBADynaSQL("INVTEMPLATECODE", true);
		minvtemplatedtl = new TBADynaSQL("INVTEMPLATECODEDTL", false);
		minvtemplatecodeconbasics = new TBADynaSQL("INVTEMPLATECODECONSBASIS", false);

		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("INVTEMPLATE_CODE");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("DESCRIPTION"));
		minvtemplate.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		minvtemplate.setField("INVTEMPLATE_CODE", processData.get("INVTEMPLATE_CODE"));
		dataExists = minvtemplate.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				minvtemplate.setNew(true);
				minvtemplate.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				minvtemplate.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				minvtemplate.setField("PRESENTATION_TYPE", processData.get("PRESENTATION_TYPE"));
				minvtemplate.setField("VAT_CST_AT_HEADER_FOOTER", processData.get("VAT_CST_AT_HEADER_FOOTER"));
				minvtemplate.setField("SERVICE_TAX_NO_PRINTED", processData.get("SERVICE_TAX_NO_PRINTED"));
				minvtemplate.setField("SHADED_INVOICE_HEADER", processData.get("SHADED_INVOICE_HEADER"));
				minvtemplate.setField("GENERATION_DATE_PRINTED", processData.get("GENERATION_DATE_PRINTED"));
				minvtemplate.setField("CAPTION_FOR_TC_PRINTED", processData.get("CAPTION_FOR_TC_PRINTED"));
				minvtemplate.setField("EOA_DISCLAIMER_PRINTED", processData.get("EOA_DISCLAIMER_PRINTED"));
				minvtemplate.setField("CONSOLIDATED", processData.get("CONSOLIDATED"));
				minvtemplate.setField("SCHEDULE_ID_PRINTED", processData.get("SCHEDULE_ID_PRINTED"));
				minvtemplate.setField("LEASE_FREQDEAL_TYPE_PRINTED", processData.get("LEASE_FREQDEAL_TYPE_PRINTED"));
				minvtemplate.setField("DISB_AMOUNT_PRINTED", processData.get("DISB_AMOUNT_PRINTED"));
				minvtemplate.setField("PRINT_CONSOLIDATION_BASIS", processData.get("PRINT_CONSOLIDATION_BASIS"));
				minvtemplate.setField("CONSOLIDATION_NAME", processData.get("CONSOLIDATION_NAME"));
				minvtemplate.setField("CONSOLIDATION_IN_GRID", processData.get("CONSOLIDATION_IN_GRID"));
				minvtemplate.setField("CONSOLIDATION_IN_ANNEX", processData.get("CONSOLIDATION_IN_ANNEX"));
				minvtemplate.setField("ANNEX_MODE", processData.get("ANNEX_MODE"));
				minvtemplate.setField("GROUPING_IN_GRID", processData.get("GROUPING_IN_GRID"));
				minvtemplate.setField("GROUPING_IN_ANNEX", processData.get("GROUPING_IN_ANNEX"));
				minvtemplate.setField("ELECTRONIC_COPY_ONLY", processData.get("ELECTRONIC_COPY_ONLY"));
				minvtemplate.setField("REQUIRES_SIGN_ATTACH", processData.get("REQUIRES_SIGN_ATTACH"));
				minvtemplate.setField("ENABLED", processData.get("ENABLED"));
				minvtemplate.setField("REMARKS", processData.get("REMARKS"));
				if (minvtemplate.save()) {
					if (processData.get("CONSOLIDATED").equals(RegularConstants.ONE)) {
						// first grid
						minvtemplatecodeconbasics.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
						minvtemplatecodeconbasics.setField("INVTEMPLATE_CODE", processData.get("INVTEMPLATE_CODE"));
						DTDObject detailData2 = processData.getDTDObject("INVTEMPLATECODECONSBASIS");
						minvtemplatecodeconbasics.setNew(true);
						for (int i = 0; i < detailData2.getRowCount(); ++i) {
							minvtemplatecodeconbasics.setField("DTL_SL", String.valueOf(i + 1));
							minvtemplatecodeconbasics.setField("CONS_FIELD_ID", detailData2.getValue(i, 1));
							minvtemplatecodeconbasics.setField("CONS_FIELD_REF_ASSET_TABLE", detailData2.getValue(i, 4));
							if (!minvtemplatecodeconbasics.save()) {
								processResult.setProcessStatus(TBAProcessStatus.FAILURE);
								processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
							}
						}
					}
					minvtemplatedtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
					minvtemplatedtl.setField("INVTEMPLATE_CODE", processData.get("INVTEMPLATE_CODE"));
					DTDObject detailData = processData.getDTDObject("INVTEMPLATECODEDTL");
					minvtemplatedtl.setNew(true);
					for (int i = 0; i < detailData.getRowCount(); ++i) {
						minvtemplatedtl.setField("DTL_SL", String.valueOf(i + 1));
						minvtemplatedtl.setField("FIELD_ID", detailData.getValue(i, 1));
						minvtemplatedtl.setField("FIELD_REF_ASSET_TABLE", detailData.getValue(i, 4));
						minvtemplatedtl.setField("IS_MANDATORY", RegularConstants.ONE);
						minvtemplatedtl.setField("GRID_ANNEX_BOTH", detailData.getValue(i, 6));
						minvtemplatedtl.setField("GROUP_TYPE", detailData.getValue(i, 8));
						if (!minvtemplatedtl.save()) {
							processResult.setProcessStatus(TBAProcessStatus.FAILURE);
							processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
						}
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			minvtemplate.close();
			minvtemplatedtl.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				minvtemplate.setNew(false);
				minvtemplate.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				minvtemplate.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				minvtemplate.setField("PRESENTATION_TYPE", processData.get("PRESENTATION_TYPE"));
				minvtemplate.setField("VAT_CST_AT_HEADER_FOOTER", processData.get("VAT_CST_AT_HEADER_FOOTER"));
				minvtemplate.setField("SERVICE_TAX_NO_PRINTED", processData.get("SERVICE_TAX_NO_PRINTED"));
				minvtemplate.setField("SHADED_INVOICE_HEADER", processData.get("SHADED_INVOICE_HEADER"));
				minvtemplate.setField("GENERATION_DATE_PRINTED", processData.get("GENERATION_DATE_PRINTED"));
				minvtemplate.setField("CAPTION_FOR_TC_PRINTED", processData.get("CAPTION_FOR_TC_PRINTED"));
				minvtemplate.setField("EOA_DISCLAIMER_PRINTED", processData.get("EOA_DISCLAIMER_PRINTED"));
				minvtemplate.setField("CONSOLIDATED", processData.get("CONSOLIDATED"));
				minvtemplate.setField("SCHEDULE_ID_PRINTED", processData.get("SCHEDULE_ID_PRINTED"));
				minvtemplate.setField("LEASE_FREQDEAL_TYPE_PRINTED", processData.get("LEASE_FREQDEAL_TYPE_PRINTED"));
				minvtemplate.setField("DISB_AMOUNT_PRINTED", processData.get("DISB_AMOUNT_PRINTED"));
				minvtemplate.setField("PRINT_CONSOLIDATION_BASIS", processData.get("PRINT_CONSOLIDATION_BASIS"));
				minvtemplate.setField("CONSOLIDATION_NAME", processData.get("CONSOLIDATION_NAME"));
				minvtemplate.setField("CONSOLIDATION_IN_GRID", processData.get("CONSOLIDATION_IN_GRID"));
				minvtemplate.setField("CONSOLIDATION_IN_ANNEX", processData.get("CONSOLIDATION_IN_ANNEX"));
				minvtemplate.setField("ANNEX_MODE", processData.get("ANNEX_MODE"));
				minvtemplate.setField("GROUPING_IN_GRID", processData.get("GROUPING_IN_GRID"));
				minvtemplate.setField("GROUPING_IN_ANNEX", processData.get("GROUPING_IN_ANNEX"));
				minvtemplate.setField("ELECTRONIC_COPY_ONLY", processData.get("ELECTRONIC_COPY_ONLY"));
				minvtemplate.setField("REQUIRES_SIGN_ATTACH", processData.get("REQUIRES_SIGN_ATTACH"));
				minvtemplate.setField("ENABLED", processData.get("ENABLED"));
				minvtemplate.setField("REMARKS", processData.get("REMARKS"));
				if (minvtemplate.save()) {
					String[] minvtemplConFields = { "ENTITY_CODE", "INVTEMPLATE_CODE", "DTL_SL" };
					BindParameterType[] minvtemplConFieldTypes = { BindParameterType.BIGINT, BindParameterType.VARCHAR, BindParameterType.INTEGER };
					minvtemplatecodeconbasics.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
					minvtemplatecodeconbasics.setField("INVTEMPLATE_CODE", processData.get("INVTEMPLATE_CODE"));
					minvtemplatecodeconbasics.deleteByFieldsAudit(minvtemplConFields, minvtemplConFieldTypes);
					if (processData.get("CONSOLIDATED").equals(RegularConstants.ONE)) {
						DTDObject detailData2 = processData.getDTDObject("INVTEMPLATECODECONSBASIS");
						minvtemplatecodeconbasics.setNew(true);
						for (int i = 0; i < detailData2.getRowCount(); ++i) {
							minvtemplatecodeconbasics.setField("DTL_SL", String.valueOf(i + 1));
							minvtemplatecodeconbasics.setField("CONS_FIELD_ID", detailData2.getValue(i, 1));
							minvtemplatecodeconbasics.setField("CONS_FIELD_REF_ASSET_TABLE", detailData2.getValue(i, 4));
							if (!minvtemplatecodeconbasics.save()) {
								processResult.setProcessStatus(TBAProcessStatus.FAILURE);
								processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
								return;
							}
						}
					}

					String[] invtemplatecodedtlFields = { "ENTITY_CODE", "INVTEMPLATE_CODE", "DTL_SL" };
					BindParameterType[] invtemplatecodedtlFieldTypes = { BindParameterType.BIGINT, BindParameterType.VARCHAR, BindParameterType.INTEGER };
					minvtemplatedtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
					minvtemplatedtl.setField("INVTEMPLATE_CODE", processData.get("INVTEMPLATE_CODE"));
					minvtemplatedtl.deleteByFieldsAudit(invtemplatecodedtlFields, invtemplatecodedtlFieldTypes);
					DTDObject detailData = processData.getDTDObject("INVTEMPLATECODEDTL");
					minvtemplatedtl.setNew(true);
					for (int i = 0; i < detailData.getRowCount(); ++i) {
						minvtemplatedtl.setField("DTL_SL", String.valueOf(i + 1));
						minvtemplatedtl.setField("FIELD_ID", detailData.getValue(i, 1));
						minvtemplatedtl.setField("FIELD_REF_ASSET_TABLE", detailData.getValue(i, 4));
						minvtemplatedtl.setField("IS_MANDATORY", RegularConstants.ONE);
						minvtemplatedtl.setField("GRID_ANNEX_BOTH", detailData.getValue(i, 6));
						minvtemplatedtl.setField("GROUP_TYPE", detailData.getValue(i, 8));
						if (!minvtemplatedtl.save()) {
							processResult.setProcessStatus(TBAProcessStatus.FAILURE);
							processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
							return;
						}
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			minvtemplate.close();
			minvtemplatedtl.close();
		}
	}
}
