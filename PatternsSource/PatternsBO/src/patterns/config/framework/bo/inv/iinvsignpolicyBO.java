
package patterns.config.framework.bo.inv;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.web.BackOfficeFormatUtils;

public class iinvsignpolicyBO extends GenericBO {

	TBADynaSQL iinvsignpolicy = null;

	public iinvsignpolicyBO() {
	}

	public void init() {
		iinvsignpolicy = new TBADynaSQL("INVSIGNPOLICYHIST", true);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("BRANCH_CODE") + RegularConstants.PK_SEPARATOR + BackOfficeFormatUtils.getDate(processData.getDate("EFF_DATE"), BackOfficeConstants.TBA_DATE_FORMAT);
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("BRANCH_CODE"));
		iinvsignpolicy.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		iinvsignpolicy.setField("BRANCH_CODE", processData.get("BRANCH_CODE"));
		iinvsignpolicy.setField("EFF_DATE", processData.getDate("EFF_DATE"));
		dataExists = iinvsignpolicy.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				iinvsignpolicy.setNew(true);
				iinvsignpolicy.setField("FIRST_SIGN_STAFF_CODE", processData.get("FIRST_SIGN_STAFF_CODE"));
				iinvsignpolicy.setField("SECOND_SIGN_STAFF_CODE", processData.get("SECOND_SIGN_STAFF_CODE"));
				iinvsignpolicy.setField("ENABLED", processData.get("ENABLED"));
				iinvsignpolicy.setField("REMARKS", processData.get("REMARKS"));
				if (iinvsignpolicy.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			iinvsignpolicy.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				iinvsignpolicy.setNew(false);
				iinvsignpolicy.setField("FIRST_SIGN_STAFF_CODE", processData.get("FIRST_SIGN_STAFF_CODE"));
				iinvsignpolicy.setField("SECOND_SIGN_STAFF_CODE", processData.get("SECOND_SIGN_STAFF_CODE"));
				iinvsignpolicy.setField("ENABLED", processData.get("ENABLED"));
				iinvsignpolicy.setField("REMARKS", processData.get("REMARKS"));
				if (iinvsignpolicy.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			iinvsignpolicy.close();
		}
	}
}
