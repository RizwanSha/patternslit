package patterns.config.framework.bo.inv;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.sql.Date;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import patterns.config.framework.bo.ServiceBO;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessResult;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTDObject;
import patterns.config.framework.service.DTObject;
/*
 * The program will be used to send Email generated Invoice. 
 *
 Author : Pavan kumar.R
 Created Date : 03-March-2017
 Spec Reference PSD-INV-
 Modification History
 -----------------------------------------------------------------------------------------------------
 Sl.No		Modified Date	Author			Modified Changes					Version
 1		22-March-2017	Pavan Kumar R	 removed invocation of Job Class  	1.1
 -----------------------------------------------------------------------------------------------------

 */
import patterns.config.framework.web.BackOfficeFormatUtils;

public class pinvemailgenBO extends ServiceBO {

	public pinvemailgenBO() {
	}

	public void init() {

	}

	@Override
	public TBAProcessResult processRequest() {
		try {
			if (processData.getDTDObject("PINVEMAILGENDTL") != null) {
				DTDObject detailData = processData.getDTDObject("PINVEMAILGENDTL");
				for (int i = 0; i < detailData.getRowCount(); i++) {
					DTObject formDTO = new DTObject();
					formDTO.reset();
					String templateCode = "INVOICE";
					formDTO.set("TEMPLATE_CODE", templateCode);
					formDTO.set("INTERFACE_CODE", "DISEMAIL");
					formDTO.set("ENTITY_CODE", processData.get("ENTITY_CODE"));
					formDTO.set("USER_ID", processData.get("USER_ID"));
					formDTO.set("GEN_DATE", new java.sql.Timestamp(new java.util.Date().getTime()).toString());
					formDTO.set("CUSTOMER_NAME", detailData.getValue(i, "CUSTOMER_NAME"));
					formDTO.set("INVOICE_NO", detailData.getValue(i, "INVOICE_NO"));
					formDTO.set("INV_DATE", detailData.getValue(i, "INV_DATE"));
					formDTO.set("BILLING_ADDR_SL", detailData.getValue(i, "BILLING_ADDR_SL"));
					formDTO.set("CUSTOMER_ID", detailData.getValue(i, "CUSTOMER_ID"));
					formDTO.set("PDF_REPORT_IDENTIFIER", detailData.getValue(i, "PDF_REPORT_IDENTIFIER"));
					formDTO.set("EXCEL_REPORT_IDENTIFIER", detailData.getValue(i, "EXCEL_REPORT_IDENTIFIER"));
					formDTO.set("PDF_FILE_INV_NUM", detailData.getValue(i, "PDF_FILE_INV_NUM"));
					formDTO.set("EXCEL_FILE_INV_NUM", detailData.getValue(i, "EXCEL_FILE_INV_NUM"));
					formDTO.set("LEASE_PRODUCT_CODE", detailData.getValue(i, "LEASE_PRODUCT_CODE"));
					formDTO.set("INV_YEAR", detailData.getValue(i, "INV_YEAR"));
					formDTO.set("INV_MONTH", detailData.getValue(i, "INV_MONTH"));
					formDTO.set("INV_SERIAL", detailData.getValue(i, "INVOICE_NO"));
					if (insertDataIntoINVOICEEMAILSENDLOG(formDTO)) {
						if (!pushDataToLogTable(formDTO)) {
							processResult.setProcessStatus(TBAProcessStatus.FAILURE);
						}
					} else {
						processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					}
				}
			}
			processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
		} catch (Exception e) {
			e.getLocalizedMessage();
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setAdditionalInfo(e.getLocalizedMessage());
		}
		return processResult;
	}

	public boolean pushDataToLogTable(DTObject formDTO) throws TBAFrameworkException {
		try {
			String className = "com.patterns.jobs.message.handler.CommunicationHandler";
			String methodName = "dispatchEMailWithInventoryNumber";
			formDTO = getAttachmentFile(formDTO);
			formDTO = getEmailList(formDTO);
			formDTO.set("CLASS_NAME", className);
			formDTO.set("METHOD_NAME", methodName);
			String status = invokeCommunicationHandler(getDbContext(), formDTO).get("RESULT");
			if (status.equals(TBAProcessStatus.FAILURE)) {
				throw new TBAFrameworkException("Issue in generating Email");
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			throw new TBAFrameworkException(e.getLocalizedMessage());

		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private DTObject invokeCommunicationHandler(DBContext dbcontext, DTObject inputObject) throws TBAFrameworkException {
		DTObject resultDTO = null;
		Class qmClass = null;
		Object qmObject = null;
		Method getDataMethod = null;
		Class[] parameterTypes = new Class[] { DBContext.class, DTObject.class };
		Object[] params = new Object[] { dbcontext, inputObject };
		try {
			qmClass = Class.forName(inputObject.get("CLASS_NAME"));
			qmObject = qmClass.newInstance();
			getDataMethod = qmClass.getMethod(inputObject.get("METHOD_NAME"), parameterTypes);
			resultDTO = (DTObject) getDataMethod.invoke(qmObject, params);
		} catch (Exception e) {
			throw new TBAFrameworkException(e.getLocalizedMessage());
		}
		return resultDTO;
	}

	DTObject getAttachmentFile(DTObject formDTO) throws TBAFrameworkException {
		DBUtil util = getDbContext().createUtilInstance();
		Map<String, String> attachment = new HashMap<String, String>();
		File f;
		try {
			util.reset();
			if (!formDTO.get("PDF_REPORT_IDENTIFIER").equals(RegularConstants.EMPTY_STRING)) {
				util.setSql("SELECT FILE_PATH FROM REPORTDOWNLOAD  WHERE ENTITY_CODE = ? AND REPORT_IDENTIFIER=?");
				util.setString(1, processData.get("ENTITY_CODE"));
				util.setString(2, formDTO.get("PDF_REPORT_IDENTIFIER"));
				ResultSet rset = util.executeQuery();
				if (rset.next()) {
					f = new File(rset.getString("FILE_PATH"));
					if (f.exists())
						attachment.put(f.getName(), f.getParent());
					else
						formDTO.set("PDF", formDTO.get("PDF_REPORT_IDENTIFIER"));
				}
			}
			if (!formDTO.get("EXCEL_REPORT_IDENTIFIER").equals(RegularConstants.EMPTY_STRING)) {
				util.setSql("SELECT FILE_PATH FROM REPORTDOWNLOAD  WHERE ENTITY_CODE = ? AND REPORT_IDENTIFIER=?");
				util.setString(1, processData.get("ENTITY_CODE"));
				util.setString(2, formDTO.get("EXCEL_REPORT_IDENTIFIER"));
				ResultSet rset = util.executeQuery();
				if (rset.next()) {
					f = new File(rset.getString("FILE_PATH"));
					if (f.exists())
						attachment.put(f.getName(), f.getParent());
					else
						formDTO.set("XLSX", formDTO.get("EXCEL_REPORT_IDENTIFIER"));
				}
			}
			if (formDTO.get("PDF") != null) {
				String reportPath = reGenerateFiles(Long.parseLong(formDTO.get("PDF_FILE_INV_NUM")), Integer.parseInt(formDTO.get("INV_YEAR")), formDTO.get("PDF"));
				if (!reportPath.trim().equals(RegularConstants.EMPTY_STRING)) {
					f = new File(reportPath);
					attachment.put(f.getName(), f.getParent());
				}
			}
			if (formDTO.get("XLSX") != null) {
				String reportPath = reGenerateFiles(Long.parseLong(formDTO.get("EXCEL_FILE_INV_NUM")), Integer.parseInt(formDTO.get("INV_YEAR")), formDTO.get("XLSX"));
				if (!reportPath.trim().equals(RegularConstants.EMPTY_STRING)) {
					f = new File(reportPath);
					attachment.put(f.getName(), f.getParent());
				}
			}
			if (attachment.size() >= 1)
				formDTO.setObject("ATTACHMENT", attachment);
			else
				throw new TBAFrameworkException("Issue in Fetching Files");

			formDTO.set("FETCH_FILE_SUCCESS", "SUCCESS");
		} catch (Exception e) {
			e.printStackTrace();
			throw new TBAFrameworkException("Issue in Fetching Files");
		} finally {
			util.reset();
		}
		return formDTO;
	}

	String reGenerateFiles(long FileInvNo, int invYear, String reportIdentifier) throws TBAFrameworkException {
		DBUtil util = getDbContext().createUtilInstance();
		String reportPath = null;
		boolean newfileGenerated = false;
		try {
			util.reset();
			util.setSql("SELECT REPORT_GENERATION_PATH  FROM SYSTEMFOLDERS WHERE ENTITY_CODE = ? AND EFFT_DATE = (SELECT MAX(EFFT_DATE) FROM SYSTEMFOLDERS WHERE ENTITY_CODE = ? AND EFFT_DATE <= FN_GETCD(?))");
			util.setString(1, processData.get("ENTITY_CODE"));
			util.setString(2, processData.get("ENTITY_CODE"));
			util.setString(3, processData.get("ENTITY_CODE"));
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				reportPath = rset.getString("REPORT_GENERATION_PATH");
			}
			util.reset();
			util.setSql("SELECT C.FILE_DATA,D.FILE_NAME,D.FILE_EXTENSION FROM CMNFILEINVENTORYSRC? C INNER JOIN CMNFILEINVENTORYSRCDTL D ON (C.ENTITY_CODE=D.ENTITY_CODE AND C.FILE_INV_NUM=D.FILE_INV_NUM) WHERE C.ENTITY_CODE = ? AND C.FILE_INV_NUM=?");
			util.setInt(1, invYear);
			util.setString(2, processData.get("ENTITY_CODE"));
			util.setLong(3, FileInvNo);
			ResultSet rt = util.executeQuery();
			if (rt.next()) {
				if (!rt.getString("FILE_DATA").trim().equals(RegularConstants.EMPTY_STRING)) {
					reportPath = reportPath + rt.getString("FILE_NAME") + "." + rt.getString("FILE_EXTENSION");
					File genearteFile = new File(reportPath);
					FileOutputStream output = new FileOutputStream(genearteFile);
					InputStream in = rt.getBinaryStream("FILE_DATA");
					byte[] buffer = new byte[4096];
					while (in.read(buffer) > 0) {
						output.write(buffer);
					}
					output.close();
					in.close();
					newfileGenerated = true;
				}
			}
			if (newfileGenerated) {
				util.reset();
				util.setSql("UPDATE REPORTDOWNLOAD SET FILE_PATH=?  WHERE ENTITY_CODE = ? AND REPORT_IDENTIFIER=?");
				util.setString(1, reportPath);
				util.setString(2, processData.get("ENTITY_CODE"));
				util.setString(3, reportIdentifier);
				util.executeUpdate();
			}
			if (newfileGenerated)
				return reportPath;
			else
				return RegularConstants.EMPTY_STRING;

		} catch (Exception e) {
			e.printStackTrace();
			throw new TBAFrameworkException("Issue in generating Files");
		} finally {
			util.reset();
		}
	}

	DTObject getEmailList(DTObject formDTO) throws TBAFrameworkException {
		DBUtil util = getDbContext().createUtilInstance();
		ArrayList<String> toEmailList = new ArrayList<String>();
		ArrayList<String> ccEmailList = new ArrayList<String>();
		try {
			util.reset();
			util.setSql("SELECT C.EMAIL_ID,C.ALTERNATE_EMAIL FROM CUSTOMERCONTACTDTL C WHERE C.ENTITY_CODE=? AND C.CUSTOMER_ID=? AND C.ADDR_SL=? ");
			util.setLong(1, Long.parseLong(processData.get("ENTITY_CODE")));
			util.setString(2, formDTO.get("CUSTOMER_ID"));
			util.setString(3, formDTO.get("BILLING_ADDR_SL"));
			ResultSet rset = util.executeQuery();
			while (rset.next()) {
				if (rset.getString("ALTERNATE_EMAIL").equals("1"))
					ccEmailList.add(rset.getString("EMAIL_ID"));
				else
					toEmailList.add(rset.getString("EMAIL_ID"));
			}
			if (toEmailList.size() >= 1)
				formDTO.setObject("TO_EMAIL_ID_LIST", toEmailList);

			util.reset();
			StringBuffer sqlQuery = new StringBuffer();
			sqlQuery.append("SELECT S.EMAIL_ID FROM INVOICES I");
			sqlQuery.append(" INNER JOIN INVOICEBREAKUP B ON (B.ENTITY_CODE=I.ENTITY_CODE AND B.LEASE_PRODUCT_CODE=I.LEASE_PRODUCT_CODE AND B.INV_YEAR=I.INV_YEAR AND B.INV_MONTH=I.INV_MONTH AND B.INV_SERIAL=I.INV_SERIAL) ");
			sqlQuery.append(" INNER JOIN LEASE L ON  (L.ENTITY_CODE=B.ENTITY_CODE AND L.LESSEE_CODE=B.LESSEE_CODE AND L.AGREEMENT_NO=B.AGREEMENT_NO AND L.SCHEDULE_ID=B.SCHEDULE_ID)");
			sqlQuery.append("INNER JOIN STAFF S ON (S.ENTITY_CODE=L.ENTITY_CODE AND S.STAFF_CODE=L.RM_STAFF_CODE AND S.STAFF_STATUS='A') ");
			sqlQuery.append("WHERE I.ENTITY_CODE=? AND I.LEASE_PRODUCT_CODE=? AND I.INV_YEAR=? AND I.INV_MONTH=? and I.INV_SERIAL=?");
			util.setSql(sqlQuery.toString());
			util.setLong(1, Long.parseLong(processData.get("ENTITY_CODE")));
			util.setString(2, formDTO.get("LEASE_PRODUCT_CODE"));
			util.setInt(3, Integer.parseInt(formDTO.get("INV_YEAR")));
			util.setInt(4, Integer.parseInt(formDTO.get("INV_MONTH")));
			util.setLong(5, Long.parseLong(formDTO.get("INV_SERIAL")));
			ResultSet reset = util.executeQuery();
			while (reset.next()) {
				ccEmailList.add(reset.getString("EMAIL_ID"));
			}
			if (ccEmailList.size() >= 1)
				formDTO.setObject("CC_EMAIL_ID_LIST", ccEmailList);
			if (ccEmailList.size() >= 1 || toEmailList.size() >= 1) {
				formDTO.setObject("FETCH_EMAIL_SUCCESS", "SUCCESS");
			} else {
				throw new TBAFrameworkException("Issue in Fetching EmailId");
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new TBAFrameworkException("Issue in Fetching EmailId");
		} finally {
			util.reset();
		}
		return formDTO;
	}

	private boolean insertDataIntoINVOICEEMAILSENDLOG(DTObject inputDTO) throws TBAFrameworkException {
		StringBuffer sqlQuery = new StringBuffer(RegularConstants.EMPTY_STRING);
		sqlQuery.append("INSERT INTO INVOICEEMAILSENDLOG(ENTITY_CODE,LEASE_PRODUCT_CODE,INV_YEAR,INV_MONTH,INV_SERIAL,DTL_SL,INV_DATE,EMAIL_LOG_INVENTORY_NO,REQUESTED_DATETIME,REQUESTED_BY)");
		sqlQuery.append("SELECT ?,?,?,?,?,COALESCE(MAX(DTL_SL)+1,1),?,?,?,? FROM INVOICEEMAILSENDLOG  WHERE ENTITY_CODE=? AND LEASE_PRODUCT_CODE=? AND INV_YEAR=? AND INV_MONTH=? AND INV_SERIAL=?");
		try {
			DBUtil util = getDbContext().createUtilInstance();
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT FN_NEXTVAL('SEQ_EMAIL'),DATE_FORMAT(?,'%b%y') FROM DUAL");
			util.reset();
			util.setMode(DBUtil.PREPARED);
			util.setSql(sql.toString());
			util.setString(1, inputDTO.get("INV_YEAR") + '-' + inputDTO.get("INV_MONTH") + "-01");
			ResultSet emailSequenceRSet = util.executeQuery();
			if (emailSequenceRSet.next()) {
				inputDTO.set("INVENTORY_NO", emailSequenceRSet.getString(1));
				inputDTO.set("MONTH_YEAR", emailSequenceRSet.getString(2));
			} else
				throw new TBAFrameworkException("Issue in generating Email InventoryNo");
			Date InvDate = new java.sql.Date(BackOfficeFormatUtils.getDate(inputDTO.get("INV_DATE"), BackOfficeConstants.TBA_DATE_FORMAT).getTime());

			util.reset();
			util.setMode(DBUtil.PREPARED);
			util.setSql(sqlQuery.toString());
			util.setLong(1, Long.parseLong(processData.get("ENTITY_CODE")));
			util.setString(2, inputDTO.get("LEASE_PRODUCT_CODE"));
			util.setLong(3, Long.parseLong(inputDTO.get("INV_YEAR")));
			util.setLong(4, Long.parseLong(inputDTO.get("INV_MONTH")));
			util.setLong(5, Long.parseLong(inputDTO.get("INV_SERIAL")));
			util.setDate(6, InvDate);
			util.setLong(7, Long.parseLong(inputDTO.get("INVENTORY_NO")));
			util.setString(8, new java.sql.Timestamp(new java.util.Date().getTime()).toString());
			util.setString(9, processData.get("USER_ID"));
			util.setLong(10, Long.parseLong(processData.get("ENTITY_CODE")));
			util.setString(11, inputDTO.get("LEASE_PRODUCT_CODE"));
			util.setInt(12, Integer.parseInt(inputDTO.get("INV_YEAR")));
			util.setInt(13, Integer.parseInt(inputDTO.get("INV_MONTH")));
			util.setInt(14, Integer.parseInt(inputDTO.get("INV_SERIAL")));
			util.executeUpdate();

			util.reset();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			throw new TBAFrameworkException(e.getLocalizedMessage());
		}
	}
}
