package patterns.config.framework.bo.bil;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import patterns.config.framework.bo.AdditionalDetailInfo;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.bo.MessageParam;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.BindParameterType;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAActionType;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;

public class ebillfilestatupldBO extends GenericBO {

	TBADynaSQL ebillfilestatupld = null;
	TBADynaSQL ebillfilestatuplddtl = null;

	public ebillfilestatupldBO() {
	}

	@Override
	public void init() {
		long serial = 0;
		ebillfilestatupld = new TBADynaSQL("BILLFILESTATUPLD", true);
		ebillfilestatuplddtl = new TBADynaSQL("BILLFILESTATUPLDDTL", false);
		if (tbaContext.getProcessAction().getActionType().equals(TBAActionType.ADD) && !tbaContext.getProcessAction().isRectification()) {
			DTObject input = new DTObject();
			input.set("SOURCE_KEY", processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + BackOfficeFormatUtils.getDate(processData.getDate("ENTRY_DATE"), BackOfficeConstants.TBA_DATE_FORMAT));
			input.set("PGM_ID", tbaContext.getProcessID());
			serial = ebillfilestatupld.getTBAReferenceSerial(input);
			processData.set("ENTRY_SL", Long.toString(serial));
		}
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + BackOfficeFormatUtils.getDate(processData.getDate("ENTRY_DATE"), BackOfficeConstants.TBA_DATE_FORMAT) + RegularConstants.PK_SEPARATOR + processData.get("ENTRY_SL");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("ENTRY_DATE"));
		ebillfilestatupld.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		ebillfilestatupld.setField("ENTRY_DATE", processData.getDate("ENTRY_DATE"));
		ebillfilestatupld.setField("ENTRY_SL", processData.get("ENTRY_SL"));
		dataExists = ebillfilestatupld.loadByKeyFields();
	}

	@Override
	protected void add() throws TBAFrameworkException {
		// TODO Auto-generated method stub
		try {
			if (tbaContext.getProcessAction().isRectification()) {
				ebillfilestatupld.setNew(false);
				if (!isDataExists()) {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
				}
			} else {
				ebillfilestatupld.setNew(true);
				if (isDataExists()) {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
				}
			}
			ebillfilestatupld.setField("FILE_INV_NUM", processData.get("FILE_INV_NUM"));
			ebillfilestatupld.setField("REMARKS", processData.get("REMARKS"));
			ebillfilestatupld.setField("UPLOADED_BY", processData.get("UPLOADED_BY"));
			ebillfilestatupld.setField("UPLOADED_ON", tbaContext.getProcessActionDateTime());
			if (ebillfilestatupld.save()) {
				ebillfilestatuplddtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
				ebillfilestatuplddtl.setField("ENTRY_DATE", processData.getDate("ENTRY_DATE"));
				ebillfilestatuplddtl.setField("ENTRY_SL", processData.get("ENTRY_SL"));
				if (tbaContext.getProcessAction().isRectification()) {
					String[] ebillFileStatusFields = { "ENTITY_CODE", "ENTRY_DATE", "ENTRY_SL", "DTL_SL" };
					BindParameterType[] ebillFileStatusTypes = { BindParameterType.BIGINT, BindParameterType.DATE, BindParameterType.BIGINT, BindParameterType.INTEGER };
					ebillfilestatuplddtl.deleteByFieldsAudit(ebillFileStatusFields, ebillFileStatusTypes);
				}
				DBUtil util = getDbContext().createUtilInstance();
				util.reset();
				util.setMode(DBUtil.PREPARED);
				util.setSql("SELECT DTL_SL,REF_DOC_NO,ACCOUNT_NO,STATUS,OTH_SYS_REF_NUMBER,LEASE_PRODUCT_CODE,INV_YEAR,INV_MONTH,INV_SERIAL,BRKUP_SL FROM TMPBILLFILESTAT WHERE ENTITY_CODE=? AND TEMP_SL=?");
				util.setString(1, processData.get("ENTITY_CODE"));
				util.setString(2, processData.get("TEMP_SL"));
				ResultSet rs = util.executeQuery();
				while (rs.next()) {
					ebillfilestatuplddtl.setNew(true);
					ebillfilestatuplddtl.setField("DTL_SL",  rs.getString("DTL_SL"));
					ebillfilestatuplddtl.setField("REF_DOC_NO", rs.getString("REF_DOC_NO"));
					ebillfilestatuplddtl.setField("ACCOUNT_NO", rs.getString("ACCOUNT_NO"));
					ebillfilestatuplddtl.setField("STATUS", rs.getString("STATUS"));
					ebillfilestatuplddtl.setField("OTH_SYS_REF_NUMBER", rs.getString("OTH_SYS_REF_NUMBER"));
					ebillfilestatuplddtl.setField("LEASE_PRODUCT_CODE", rs.getString("LEASE_PRODUCT_CODE"));
					ebillfilestatuplddtl.setField("INV_YEAR", rs.getString("INV_YEAR"));
					ebillfilestatuplddtl.setField("INV_MONTH", rs.getString("INV_MONTH"));
					ebillfilestatuplddtl.setField("INV_SERIAL", rs.getString("INV_SERIAL"));
					ebillfilestatuplddtl.setField("BRKUP_SL", rs.getString("BRKUP_SL"));
					if (!ebillfilestatuplddtl.save()) {
						processResult.setProcessStatus(TBAProcessStatus.FAILURE);
						processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
						return;
					}
				}
				if (!tbaContext.getProcessAction().isRectification()) {
					AdditionalDetailInfo additionalInfo = new AdditionalDetailInfo();
					additionalInfo.setHeading(new MessageParam(BackOfficeErrorCodes.ECUST_GEN_RESULT));
					List<MessageParam> key = new ArrayList<MessageParam>();
					key.add(new MessageParam(BackOfficeErrorCodes.EBILL_UPLOAD_SERIAL));
					additionalInfo.setKey(key);
					List<String> value = new ArrayList<String>();
					value.add(processData.get("ENTRY_SL"));
					additionalInfo.setValue(value);
					processResult.setAdditionalInfo(additionalInfo);
				}
				processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			ebillfilestatupld.close();
			ebillfilestatuplddtl.close();
		}
	}

	@Override
	protected void modify() throws TBAFrameworkException {
		// TODO Auto-generated method stub

	}

}
