package patterns.config.framework.bo.cmn;

import java.util.Date;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.BindParameterType;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTDObject;
import patterns.config.framework.web.BackOfficeFormatUtils;

public class mintratesBO extends GenericBO {

	TBADynaSQL intrateshist = null;
	TBADynaSQL intrateshistdtl = null;

	public mintratesBO() {
	}

	public void init() {
		intrateshist = new TBADynaSQL("INTRATESHIST", true);
		intrateshistdtl = new TBADynaSQL("INTRATESHISTDTL", false);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("INT_RATE_TYPE_CODE") + RegularConstants.PK_SEPARATOR + BackOfficeFormatUtils.getDate((Date) processData.getDate("EFF_DATE"), BackOfficeConstants.TBA_DATE_FORMAT);
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("INT_RATE_TYPE_CODE"));
		intrateshist.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		intrateshist.setField("INT_RATE_TYPE_CODE", processData.get("INT_RATE_TYPE_CODE"));
		intrateshist.setField("EFF_DATE", processData.getDate("EFF_DATE"));
		dataExists = intrateshist.loadByKeyFields();

	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				intrateshist.setField("TENOR_SPEC_CODE", processData.get("TENOR_SPEC_CODE"));
				intrateshist.setField("TENOR_AS_OF_DATE", processData.getDate("TENOR_AS_OF_DATE"));
				intrateshist.setField("INT_RATE", processData.get("INT_RATE"));
				intrateshist.setField("ENABLED", processData.get("ENABLED"));
				intrateshist.setField("REMARKS", processData.get("REMARKS"));
				intrateshist.setNew(true);
				if (intrateshist.save()) {
					if (processData.get("INTR_APPLICABLE").equals(RegularConstants.ONE)) {
						intrateshistdtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
						intrateshistdtl.setField("INT_RATE_TYPE_CODE", processData.get("INT_RATE_TYPE_CODE"));
						intrateshistdtl.setField("EFF_DATE", processData.getDate("EFF_DATE"));
						DTDObject detailData = processData.getDTDObject("INTRATESHISTDTL");
						intrateshistdtl.setNew(true);
						int count = 1;
						for (int i = 0; i < detailData.getRowCount(); ++i) {
							intrateshistdtl.setField("DTL_SL", count);
							intrateshistdtl.setField("UPTO_PERIOD", detailData.getValue(i, 1));
							intrateshistdtl.setField("PERIOD_FLAG", detailData.getValue(i, 4));
							intrateshistdtl.setField("TENOR_DESCRIPTION", detailData.getValue(i, 3));
							intrateshistdtl.setField("INT_RATE", detailData.getValue(i, 5));
							if (!intrateshistdtl.save()) {
								processResult.setProcessStatus(TBAProcessStatus.FAILURE);
								processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
								return;
							}
							count++;
						}
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());

		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				intrateshist.setField("TENOR_SPEC_CODE", processData.get("TENOR_SPEC_CODE"));
				intrateshist.setField("TENOR_AS_OF_DATE", processData.getDate("TENOR_AS_OF_DATE"));
				intrateshist.setField("INT_RATE", processData.get("INT_RATE"));
				intrateshist.setField("ENABLED", processData.get("ENABLED"));
				intrateshist.setField("REMARKS", processData.get("REMARKS"));
				intrateshist.setNew(false);
				if (intrateshist.save()) {
					if (processData.get("INTR_APPLICABLE").equals(RegularConstants.ONE)) {
						String[] IntrateshistdtlFields = { "ENTITY_CODE", "INT_RATE_TYPE_CODE", "EFF_DATE", "DTL_SL" };
						BindParameterType[] IntrateshistdtlFieldTypes = { BindParameterType.BIGINT, BindParameterType.VARCHAR, BindParameterType.DATE, BindParameterType.INTEGER };
						intrateshistdtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
						intrateshistdtl.setField("INT_RATE_TYPE_CODE", processData.get("INT_RATE_TYPE_CODE"));
						intrateshistdtl.setField("EFF_DATE", processData.getDate("EFF_DATE"));
						intrateshistdtl.deleteByFieldsAudit(IntrateshistdtlFields, IntrateshistdtlFieldTypes);
						DTDObject detailData = processData.getDTDObject("INTRATESHISTDTL");
						intrateshistdtl.setNew(true);
						int count = 1;
						for (int i = 0; i < detailData.getRowCount(); ++i) {
							intrateshistdtl.setField("DTL_SL", count);
							intrateshistdtl.setField("UPTO_PERIOD", detailData.getValue(i, 1));
							intrateshistdtl.setField("PERIOD_FLAG", detailData.getValue(i, 4));
							intrateshistdtl.setField("TENOR_DESCRIPTION", detailData.getValue(i, 3));
							intrateshistdtl.setField("INT_RATE", detailData.getValue(i, 5));
							if (!intrateshistdtl.save()) {
								processResult.setProcessStatus(TBAProcessStatus.FAILURE);
								processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
								return;
							}
							count++;
						}
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		}
	}
}