package patterns.config.framework.bo.cmn;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;

/**
 * 
 * 
 * @version 1.0
 * @author M Vivekananda Reddy
 * @since 28-Jun-2016 <br>
 *        <b>Modified History</b>  ARULPRASATH <BR>
 *        <U><B> Sl.No Modified Date Author Modified Changes Version </B></U> <br>
 * 
 * 
 */

public class maccountsubtypeBO extends GenericBO {

	TBADynaSQL maccountsubtype = null;

	public maccountsubtypeBO() {
	}

	public void init() {
		maccountsubtype = new TBADynaSQL("ACCOUNTSUBTYPES", true);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("ACCOUNT_SUBTYPE_CODE");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("DESCRIPTION"));
		maccountsubtype.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		maccountsubtype.setField("ACCOUNT_SUBTYPE_CODE", processData.get("ACCOUNT_SUBTYPE_CODE"));
		dataExists = maccountsubtype.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				maccountsubtype.setNew(true);
				maccountsubtype.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				maccountsubtype.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				maccountsubtype.setField("ACC_SUBTYPE_ALPHA_CODE", processData.get("ACC_SUBTYPE_ALPHA_CODE"));
				maccountsubtype.setField("CUST_SEGMENT_CODE", processData.get("CUST_SEGMENT_CODE"));
				maccountsubtype.setField("RESERVED_FOR_ACCOUNT_TYPE", processData.get("RESERVED_FOR_ACCOUNT_TYPE"));
				maccountsubtype.setField("ENABLED", processData.get("ENABLED"));
				maccountsubtype.setField("REMARKS", processData.get("REMARKS"));
				if (maccountsubtype.save()) {
					maccountsubtype.setField("PURPOSE_ID", "ACC_SUBTYPE_ALPHA_CODE");
					maccountsubtype.setField("DEDUP_KEY", processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("ACC_SUBTYPE_ALPHA_CODE"));
					if (processData.get("ACC_SUBTYPE_ALPHA_CODE") != null && !maccountsubtype.udateTBADuplicateCheck()) {
						processResult.setProcessStatus(TBAProcessStatus.FAILURE);
						processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
						throw new TBAFrameworkException();
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			maccountsubtype.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				maccountsubtype.setNew(false);
				maccountsubtype.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				maccountsubtype.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				maccountsubtype.setField("ACC_SUBTYPE_ALPHA_CODE", processData.get("ACC_SUBTYPE_ALPHA_CODE"));
				maccountsubtype.setField("CUST_SEGMENT_CODE", processData.get("CUST_SEGMENT_CODE"));
				maccountsubtype.setField("RESERVED_FOR_ACCOUNT_TYPE", processData.get("RESERVED_FOR_ACCOUNT_TYPE"));
				maccountsubtype.setField("ENABLED", processData.get("ENABLED"));
				maccountsubtype.setField("REMARKS", processData.get("REMARKS"));
				if (maccountsubtype.save()) {
					maccountsubtype.setField("PURPOSE_ID", "ACC_SUBTYPE_ALPHA_CODE");
					maccountsubtype.setField("DEDUP_KEY", processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("ACC_SUBTYPE_ALPHA_CODE"));
					if (processData.get("ACC_SUBTYPE_ALPHA_CODE") != null && !maccountsubtype.udateTBADuplicateCheck()) {
						processResult.setProcessStatus(TBAProcessStatus.FAILURE);
						processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
						throw new TBAFrameworkException();
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);

				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			maccountsubtype.close();
		}
	}
}
