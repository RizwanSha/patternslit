package patterns.config.framework.bo.cmn;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.BindParameterType;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;
/*
 * The program will be used to create and maintain Financial Reporting Layout Details Maintenance. 
 *
 Author : Pavan kumar.R
 Created Date : 12-DEC-2016
 Spec Reference CMN-00018
 Modification History
 ----------------------------------------------------------------------
 Sl.No		Modified Date	Author			Modified Changes	Version
 ----------------------------------------------------------------------

 */
import patterns.config.framework.service.DTDObject;


public class mfinrptlayoutBO extends GenericBO {
	TBADynaSQL mfinrptlayout = null;
	TBADynaSQL finrptlayoutaggr = null;
	

	public void init() {
		mfinrptlayout = new TBADynaSQL("FINRPTLAYOUT", true);
		finrptlayoutaggr = new TBADynaSQL("FINRPTLAYOUTAGGR", false);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("REPORT_LAYOUT_CODE");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("REPORT_LAYOUT_CODE"));
		mfinrptlayout.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		mfinrptlayout.setField("REPORT_LAYOUT_CODE", processData.get("REPORT_LAYOUT_CODE"));
		dataExists = mfinrptlayout.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				mfinrptlayout.setNew(true);
				mfinrptlayout.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				mfinrptlayout.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				mfinrptlayout.setField("ENABLED", processData.get("ENABLED"));
				mfinrptlayout.setField("REMARKS", processData.get("REMARKS"));
				mfinrptlayout.setField("REPORT_HEADER", processData.get("REPORT_HEADER"));
				mfinrptlayout.setField("PRINT_ASON_DATE", processData.get("PRINT_ASON_DATE"));
				mfinrptlayout.setField("NOF_REPORTING_SECTIONS", processData.get("NOF_REPORTING_SECTIONS"));
				mfinrptlayout.setField("REPORT_FOR", processData.get("REPORT_FOR"));
				mfinrptlayout.setField("PRN_SL_NOS_FOR_ELEM", processData.get("PRN_SL_NOS_FOR_ELEM"));
				mfinrptlayout.setField("PRN_SL_NOS_FOR_BRKUP", processData.get("PRN_SL_NOS_FOR_BRKUP"));
				mfinrptlayout.setField("GL_AC_TYPE_FOR_SERIAL_1", processData.get("GL_AC_TYPE_FOR_SERIAL_1"));
				mfinrptlayout.setField("GL_AC_TYPE_FOR_SERIAL_2", processData.get("GL_AC_TYPE_FOR_SERIAL_2"));
				mfinrptlayout.setField("GL_AC_TYPE_FOR_SERIAL_3", processData.get("GL_AC_TYPE_FOR_SERIAL_3"));
				mfinrptlayout.setField("GL_AC_TYPE_FOR_SERIAL_4", processData.get("GL_AC_TYPE_FOR_SERIAL_4"));
				mfinrptlayout.setField("COLUMNAR_RPT_LAYOUT", processData.get("COLUMNAR_RPT_LAYOUT"));
				mfinrptlayout.setField("REPORT_LAYOUT_1", processData.get("REPORT_LAYOUT_1"));
				mfinrptlayout.setField("REPORT_LAYOUT_2", processData.get("REPORT_LAYOUT_2"));
				
				mfinrptlayout.setField("SECTION_TOTALS_REQD", processData.get("SECTION_TOTALS_REQD"));
				mfinrptlayout.setField("PAGE_TOTALS_REQD", processData.get("PAGE_TOTALS_REQD"));
				mfinrptlayout.setField("OVERALL_AGGR_REQD", processData.get("OVERALL_AGGR_REQD"));
				mfinrptlayout.setField("NOF_AGGREGATIONS", processData.get("NOF_AGGREGATIONS"));
				
				mfinrptlayout.setField("REPORT_IN_NUMBERS", processData.get("REPORT_IN_NUMBERS"));
				mfinrptlayout.setField("REPORT_FRAC_DIGITS", processData.get("REPORT_FRAC_DIGITS"));
				mfinrptlayout.setField("ROUND_OFF_CHOICE", processData.get("ROUND_OFF_CHOICE"));
				mfinrptlayout.setField("ROUND_OFF_FACTOR", processData.get("ROUND_OFF_FACTOR"));
				mfinrptlayout.setField("RPT_ADJ_FOR_SECTION_TOTALS_REQD", processData.get("RPT_ADJ_FOR_SECTION_TOTALS_REQD"));
				mfinrptlayout.setField("ADJ_SECTIONS_FIRST", processData.get("ADJ_SECTIONS_FIRST"));
				mfinrptlayout.setField("ADJ_SECTIONS_SECOND", processData.get("ADJ_SECTIONS_SECOND"));	
				mfinrptlayout.setField("AMT_SIGN_DISPLAY", processData.get("AMT_SIGN_DISPLAY"));
				mfinrptlayout.setField("NEGATIVE_AMT_DISPLAY", processData.get("NEGATIVE_AMT_DISPLAY"));
				if (mfinrptlayout.save()) {
					if (processData.getDTDObject("FINRPTLAYOUTAGGR") != null) {
						DTDObject detailData = processData.getDTDObject("FINRPTLAYOUTAGGR");
						finrptlayoutaggr.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
						finrptlayoutaggr.setField("REPORT_LAYOUT_CODE", processData.get("REPORT_LAYOUT_CODE"));
						finrptlayoutaggr.setNew(true);
						for (int i = 0; i < detailData.getRowCount(); i++) {
							finrptlayoutaggr.setField("AGGREGATION_SL", String.valueOf(i + 1));
							finrptlayoutaggr.setField("CONSIDER_SERIAL_1_AS", detailData.getValue(i, "CONSIDER_SERIAL_1_AS"));
							finrptlayoutaggr.setField("CONSIDER_SERIAL_2_AS", detailData.getValue(i, "CONSIDER_SERIAL_2_AS"));
							finrptlayoutaggr.setField("CONSIDER_SERIAL_3_AS", detailData.getValue(i, "CONSIDER_SERIAL_3_AS"));
							finrptlayoutaggr.setField("CONSIDER_SERIAL_4_AS", detailData.getValue(i, "CONSIDER_SERIAL_4_AS"));
							finrptlayoutaggr.setField("LABEL_FOR_POS_AGGR", detailData.getValue(i, "LABEL_FOR_POS_AGGR"));
							finrptlayoutaggr.setField("LABEL_FOR_NEG_AGGR", detailData.getValue(i, "LABEL_FOR_NEG_AGGR"));
							if (!finrptlayoutaggr.save()) {
								processResult.setProcessStatus(TBAProcessStatus.FAILURE);
								processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
								return;
							}
						}
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mfinrptlayout.close();
			finrptlayoutaggr.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				mfinrptlayout.setNew(false);
				mfinrptlayout.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				mfinrptlayout.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				mfinrptlayout.setField("ENABLED", processData.get("ENABLED"));
				mfinrptlayout.setField("REMARKS", processData.get("REMARKS"));
				mfinrptlayout.setField("REPORT_HEADER", processData.get("REPORT_HEADER"));
				mfinrptlayout.setField("PRINT_ASON_DATE", processData.get("PRINT_ASON_DATE"));
				mfinrptlayout.setField("NOF_REPORTING_SECTIONS", processData.get("NOF_REPORTING_SECTIONS"));
				mfinrptlayout.setField("REPORT_FOR", processData.get("REPORT_FOR"));
				mfinrptlayout.setField("PRN_SL_NOS_FOR_ELEM", processData.get("PRN_SL_NOS_FOR_ELEM"));
				mfinrptlayout.setField("PRN_SL_NOS_FOR_BRKUP", processData.get("PRN_SL_NOS_FOR_BRKUP"));
				mfinrptlayout.setField("GL_AC_TYPE_FOR_SERIAL_1", processData.get("GL_AC_TYPE_FOR_SERIAL_1"));
				mfinrptlayout.setField("GL_AC_TYPE_FOR_SERIAL_2", processData.get("GL_AC_TYPE_FOR_SERIAL_2"));
				mfinrptlayout.setField("GL_AC_TYPE_FOR_SERIAL_3", processData.get("GL_AC_TYPE_FOR_SERIAL_3"));
				mfinrptlayout.setField("GL_AC_TYPE_FOR_SERIAL_4", processData.get("GL_AC_TYPE_FOR_SERIAL_4"));
				mfinrptlayout.setField("COLUMNAR_RPT_LAYOUT", processData.get("COLUMNAR_RPT_LAYOUT"));
				mfinrptlayout.setField("REPORT_LAYOUT_1", processData.get("REPORT_LAYOUT_1"));
				mfinrptlayout.setField("REPORT_LAYOUT_2", processData.get("REPORT_LAYOUT_2"));
				mfinrptlayout.setField("SECTION_TOTALS_REQD", processData.get("SECTION_TOTALS_REQD"));
				mfinrptlayout.setField("PAGE_TOTALS_REQD", processData.get("PAGE_TOTALS_REQD"));
				mfinrptlayout.setField("OVERALL_AGGR_REQD", processData.get("OVERALL_AGGR_REQD"));
				mfinrptlayout.setField("NOF_AGGREGATIONS", processData.get("NOF_AGGREGATIONS"));
				mfinrptlayout.setField("REPORT_IN_NUMBERS", processData.get("REPORT_IN_NUMBERS"));
				mfinrptlayout.setField("REPORT_FRAC_DIGITS", processData.get("REPORT_FRAC_DIGITS"));
				mfinrptlayout.setField("ROUND_OFF_CHOICE", processData.get("ROUND_OFF_CHOICE"));
				mfinrptlayout.setField("ROUND_OFF_FACTOR", processData.get("ROUND_OFF_FACTOR"));
				mfinrptlayout.setField("RPT_ADJ_FOR_SECTION_TOTALS_REQD", processData.get("RPT_ADJ_FOR_SECTION_TOTALS_REQD"));
				mfinrptlayout.setField("ADJ_SECTIONS_FIRST", processData.get("ADJ_SECTIONS_FIRST"));
				mfinrptlayout.setField("ADJ_SECTIONS_SECOND", processData.get("ADJ_SECTIONS_SECOND"));	
				mfinrptlayout.setField("AMT_SIGN_DISPLAY", processData.get("AMT_SIGN_DISPLAY"));
				mfinrptlayout.setField("NEGATIVE_AMT_DISPLAY", processData.get("NEGATIVE_AMT_DISPLAY"));
				if (mfinrptlayout.save()) {
					String[] finrptlayoutaggrFields = { "ENTITY_CODE", "REPORT_LAYOUT_CODE", "AGGREGATION_SL" };
					BindParameterType[] finrptlayoutaggrFieldsTypes = { BindParameterType.BIGINT, BindParameterType.VARCHAR, BindParameterType.BIGINT };
					finrptlayoutaggr.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
					finrptlayoutaggr.setField("REPORT_LAYOUT_CODE", processData.get("REPORT_LAYOUT_CODE"));
					finrptlayoutaggr.deleteByFieldsAudit(finrptlayoutaggrFields, finrptlayoutaggrFieldsTypes);
					if (processData.getDTDObject("FINRPTLAYOUTAGGR") != null) {
						DTDObject detailData = processData.getDTDObject("FINRPTLAYOUTAGGR");
						finrptlayoutaggr.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
						finrptlayoutaggr.setField("REPORT_LAYOUT_CODE", processData.get("REPORT_LAYOUT_CODE"));
						finrptlayoutaggr.setNew(true);
						for (int i = 0; i < detailData.getRowCount(); i++) {
							finrptlayoutaggr.setField("AGGREGATION_SL", String.valueOf(i + 1));
							finrptlayoutaggr.setField("CONSIDER_SERIAL_1_AS", detailData.getValue(i, "CONSIDER_SERIAL_1_AS"));
							finrptlayoutaggr.setField("CONSIDER_SERIAL_2_AS", detailData.getValue(i, "CONSIDER_SERIAL_2_AS"));
							finrptlayoutaggr.setField("CONSIDER_SERIAL_3_AS", detailData.getValue(i, "CONSIDER_SERIAL_3_AS"));
							finrptlayoutaggr.setField("CONSIDER_SERIAL_4_AS", detailData.getValue(i, "CONSIDER_SERIAL_4_AS"));
							finrptlayoutaggr.setField("LABEL_FOR_POS_AGGR", detailData.getValue(i, "LABEL_FOR_POS_AGGR"));
							finrptlayoutaggr.setField("LABEL_FOR_NEG_AGGR", detailData.getValue(i, "LABEL_FOR_NEG_AGGR"));
							if (!finrptlayoutaggr.save()) {
								processResult.setProcessStatus(TBAProcessStatus.FAILURE);
								processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
								return;
							}
						}
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mfinrptlayout.close();
			finrptlayoutaggr.close();
		}
	}

}

