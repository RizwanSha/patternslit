package patterns.config.framework.bo.cmn;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.BindParameterType;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTDObject;
import patterns.config.framework.web.BackOfficeFormatUtils;

public class iprodccyconfBO extends GenericBO {
	TBADynaSQL iprodccyconf = null;
	TBADynaSQL iprodccyconfccy = null;

	public void init() {
		iprodccyconf = new TBADynaSQL("PRODCURRHISTMAIN", true);
		iprodccyconfccy = new TBADynaSQL("PRODCURRHISTDTL", false);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("PRODUCT_CODE") + RegularConstants.PK_SEPARATOR + BackOfficeFormatUtils.getDate(processData.getDate("EFF_DATE"), BackOfficeConstants.TBA_DATE_FORMAT);
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("PRODUCT_CODE"));
		iprodccyconf.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		iprodccyconf.setField("PRODUCT_CODE", processData.get("PRODUCT_CODE"));
		iprodccyconf.setField("EFF_DATE", processData.getDate("EFF_DATE"));
		dataExists = iprodccyconf.loadByKeyFields();

	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				iprodccyconf.setNew(true);
				iprodccyconf.setField("ENABLED", processData.get("ENABLED"));
				iprodccyconf.setField("REMARKS", processData.get("REMARKS"));
				if (iprodccyconf.save()) {
					iprodccyconfccy.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
					iprodccyconfccy.setField("PRODUCT_CODE", processData.get("PRODUCT_CODE"));
					iprodccyconfccy.setField("EFF_DATE", processData.getDate("EFF_DATE"));
					DTDObject detailData = processData.getDTDObject("PRODCURRHISTDTL");
					iprodccyconfccy.setNew(true);
					for (int i = 0; i < detailData.getRowCount(); ++i) {
						iprodccyconfccy.setField("SERIAL", Integer.valueOf(i + 1).toString());
						iprodccyconfccy.setField("CURR_CODE", detailData.getValue(i, 1));
						if (!iprodccyconfccy.save()) {
							processResult.setProcessStatus(TBAProcessStatus.FAILURE);
							processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
							return;
						}
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			iprodccyconf.close();
			iprodccyconfccy.close();
		}
	}

	protected void modify() throws TBAFrameworkException {

		try {
			if (isDataExists()) {
				iprodccyconf.setNew(false);
				iprodccyconf.setField("ENABLED", processData.get("ENABLED"));
				iprodccyconf.setField("REMARKS", processData.get("REMARKS"));
				if (iprodccyconf.save()) {
					String[] iprodccyconfFields = { "ENTITY_CODE", "PRODUCT_CODE", "EFF_DATE", "SERIAL" };
					BindParameterType[] iprodccyconfTypes = { BindParameterType.BIGINT, BindParameterType.VARCHAR, BindParameterType.DATE, BindParameterType.BIGINT };
					iprodccyconfccy.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
					iprodccyconfccy.setField("PRODUCT_CODE", processData.get("PRODUCT_CODE"));
					iprodccyconfccy.setField("EFF_DATE", processData.getDate("EFF_DATE"));
					iprodccyconfccy.deleteByFieldsAudit(iprodccyconfFields, iprodccyconfTypes);
					DTDObject detailData = processData.getDTDObject("PRODCURRHISTDTL");
					iprodccyconfccy.setNew(true);
					for (int i = 0; i < detailData.getRowCount(); ++i) {
						iprodccyconfccy.setField("SERIAL", Integer.valueOf(i + 1).toString());
						iprodccyconfccy.setField("CURR_CODE", detailData.getValue(i, 1));
						if (!iprodccyconfccy.save()) {
							processResult.setProcessStatus(TBAProcessStatus.FAILURE);
							processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
							return;
						}
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}

		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			iprodccyconf.close();
			iprodccyconfccy.close();
		}
	}

}
