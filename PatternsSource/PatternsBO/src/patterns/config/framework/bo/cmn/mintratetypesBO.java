package patterns.config.framework.bo.cmn;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;

/**
 * 
 * 
 * @version 1.0
 * @author Dileep Kumar Reddy T
 * @since 30-Nov-2016 <br>
 *        <b>Modified History</b> <BR>
 *        <U><B> Sl.No Modified Date Author Modified Changes Version </B></U> <br>
 * 
 * 
 */

public class mintratetypesBO extends GenericBO {
	TBADynaSQL mintratetypes = null;

	public void init() {
		mintratetypes = new TBADynaSQL("INTRATETYPES", true);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("INT_RATE_TYPE_CODE");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("INT_RATE_TYPE_CODE"));
		mintratetypes.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		mintratetypes.setField("INT_RATE_TYPE_CODE", processData.get("INT_RATE_TYPE_CODE"));
		dataExists = mintratetypes.loadByKeyFields();

	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				mintratetypes.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				mintratetypes.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				mintratetypes.setField("STANDARD_RATE", processData.get("STANDARD_RATE"));
				mintratetypes.setField("STD_INT_RATE_TYPE_CODE", processData.get("STD_INT_RATE_TYPE_CODE"));
				mintratetypes.setField("INT_RATE_CCY_CODE", processData.get("INT_RATE_CCY_CODE"));
				mintratetypes.setField("INTERNAL_EXTERNAL", processData.get("INTERNAL_EXTERNAL"));
				mintratetypes.setField("INT_RATES_BY_TENOR", processData.get("INT_RATES_BY_TENOR"));
				mintratetypes.setField("INT_RATE_PURPOSE", processData.get("INT_RATE_PURPOSE"));
				mintratetypes.setField("LOAN_INT_RATE_PURPOSE", processData.get("LOAN_INT_RATE_PURPOSE"));
				mintratetypes.setField("ENABLED", processData.get("ENABLED"));
				mintratetypes.setField("REMARKS", processData.get("REMARKS"));
				mintratetypes.setNew(true);
				if (mintratetypes.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mintratetypes.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				mintratetypes.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				mintratetypes.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				mintratetypes.setField("STANDARD_RATE", processData.get("STANDARD_RATE"));
				mintratetypes.setField("STD_INT_RATE_TYPE_CODE", processData.get("STD_INT_RATE_TYPE_CODE"));
				mintratetypes.setField("INT_RATE_CCY_CODE", processData.get("INT_RATE_CCY_CODE"));
				mintratetypes.setField("INTERNAL_EXTERNAL", processData.get("INTERNAL_EXTERNAL"));
				mintratetypes.setField("INT_RATES_BY_TENOR", processData.get("INT_RATES_BY_TENOR"));
				mintratetypes.setField("INT_RATE_PURPOSE", processData.get("INT_RATE_PURPOSE"));
				mintratetypes.setField("LOAN_INT_RATE_PURPOSE", processData.get("LOAN_INT_RATE_PURPOSE"));
				mintratetypes.setField("ENABLED", processData.get("ENABLED"));
				mintratetypes.setField("REMARKS", processData.get("REMARKS"));
				mintratetypes.setNew(false);
				if (mintratetypes.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mintratetypes.close();
		}
	}
}
