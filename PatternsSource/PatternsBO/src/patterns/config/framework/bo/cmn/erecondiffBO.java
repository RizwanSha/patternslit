package patterns.config.framework.bo.cmn;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import patterns.config.framework.bo.AdditionalDetailInfo;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.bo.MessageParam;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;

/**
 * 
 * 
 * @version 1.0
 * @author Dileep Kumar Reddy T
 * @since 27-Dec-2016 <br>
 *       <b>Modified History ARULPRASATH </b> <BR>
 *       <U><B> Sl.No Modified Date Author Modified Changes Version </B></U> <br>
 * 
 * 
 */

public class erecondiffBO extends GenericBO {
	TBADynaSQL erecondiff = null;
	long entrySerial = 0;

	public void init() {
		erecondiff = new TBADynaSQL("GLACRD", true);
		if (processData.containsKey("RECON_DIFF_ENTRY_NO"))
			entrySerial = Long.parseLong(processData.get("RECON_DIFF_ENTRY_NO"));
		else
			entrySerial = (long) erecondiff.getTBAReferenceKey(processData);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("BRANCH_CODE") + RegularConstants.PK_SEPARATOR + processData.get("GL_HEAD_CODE") + RegularConstants.PK_SEPARATOR + entrySerial;
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("BRANCH_CODE"));
		erecondiff.setField("ENTITY_CODE", Long.parseLong(processData.get("ENTITY_CODE")));
		erecondiff.setField("BRANCH_CODE", processData.get("BRANCH_CODE"));
		erecondiff.setField("GL_HEAD_CODE", processData.get("GL_HEAD_CODE"));
		erecondiff.setField("RECON_DIFF_ENTRY_NO", entrySerial);
		dataExists = erecondiff.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				erecondiff.setField("ENTRY_DATE", processData.getDate("ENTRY_DATE"));
				erecondiff.setField("DIFF_AMT_OF_ENTRY_CCY_AC", processData.get("DIFF_AMT_OF_ENTRY_CCY_AC"));
				erecondiff.setField("DIFF_AMT_OF_ENTRY_AMT_AC", processData.get("DIFF_AMT_OF_ENTRY_AMT_AC"));
				erecondiff.setField("DIFF_AMT_OF_ENTRY_DBCR_AC", processData.get("DIFF_AMT_OF_ENTRY_DBCR_AC"));
				erecondiff.setField("DIFF_AMT_OF_ENTRY_CCY_BC", processData.get("DIFF_AMT_OF_ENTRY_CCY_BC"));
				erecondiff.setField("DIFF_AMT_OF_ENTRY_AMT_BC", processData.get("DIFF_AMT_OF_ENTRY_AMT_BC"));
				erecondiff.setField("DIFF_AMT_OF_ENTRY_DBCR_BC", processData.get("DIFF_AMT_OF_ENTRY_DBCR_BC"));
				erecondiff.setField("OS_BAL_OF_RECON_DIFF_ENTRY_AC", new BigDecimal(processData.get("OS_BAL_OF_RECON_DIFF_ENTRY_AC")).add(new BigDecimal(processData.get("DIFF_AMT_OF_ENTRY_AMT_AC"))));
				erecondiff.setField("OS_BAL_OF_RECON_DIFF_ENTRY_BC", new BigDecimal(processData.get("OS_BAL_OF_RECON_DIFF_ENTRY_BC")).add(new BigDecimal(processData.get("DIFF_AMT_OF_ENTRY_AMT_BC"))));
				erecondiff.setField("REMARKS", processData.get("REMARKS"));
				erecondiff.setNew(true);
				if (erecondiff.save()) {
					AdditionalDetailInfo additionalInfo = new AdditionalDetailInfo();
					additionalInfo.setHeading(new MessageParam(BackOfficeErrorCodes.PBS_GL_DETAILS));
					List<MessageParam> key = new ArrayList<MessageParam>();
					key.add(new MessageParam(BackOfficeErrorCodes.PBS_GL_HEAD));
					key.add(new MessageParam(BackOfficeErrorCodes.PBS_ENTRY_NO));
					additionalInfo.setKey(key);
					List<String> value = new ArrayList<String>();
					value.add(processData.get("GL_HEAD_CODE"));
					value.add(String.valueOf(erecondiff.getField("RECON_DIFF_ENTRY_NO")));
					additionalInfo.setValue(value);
					processResult.setAdditionalInfo(additionalInfo);
					
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			erecondiff.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				BigDecimal oldDifferenceActual = new BigDecimal(erecondiff.getField("DIFF_AMT_OF_ENTRY_AMT_AC").toString());
				BigDecimal oldDifferenceBase = new BigDecimal(erecondiff.getField("DIFF_AMT_OF_ENTRY_AMT_BC").toString());
				erecondiff.setField("ENTRY_DATE", processData.getDate("ENTRY_DATE"));
				erecondiff.setField("DIFF_AMT_OF_ENTRY_CCY_AC", processData.get("DIFF_AMT_OF_ENTRY_CCY_AC"));
				erecondiff.setField("DIFF_AMT_OF_ENTRY_AMT_AC", processData.get("DIFF_AMT_OF_ENTRY_AMT_AC"));
				erecondiff.setField("DIFF_AMT_OF_ENTRY_DBCR_AC", processData.get("DIFF_AMT_OF_ENTRY_DBCR_AC"));
				erecondiff.setField("DIFF_AMT_OF_ENTRY_CCY_BC", processData.get("DIFF_AMT_OF_ENTRY_CCY_BC"));
				erecondiff.setField("DIFF_AMT_OF_ENTRY_AMT_BC", processData.get("DIFF_AMT_OF_ENTRY_AMT_BC"));
				erecondiff.setField("DIFF_AMT_OF_ENTRY_DBCR_BC", processData.get("DIFF_AMT_OF_ENTRY_DBCR_BC"));
				erecondiff.setField("OS_BAL_OF_RECON_DIFF_ENTRY_AC", new BigDecimal(processData.get("OS_BAL_OF_RECON_DIFF_ENTRY_AC")).subtract(oldDifferenceActual).add(new BigDecimal(processData.get("DIFF_AMT_OF_ENTRY_AMT_AC"))));
				erecondiff.setField("OS_BAL_OF_RECON_DIFF_ENTRY_BC", new BigDecimal(processData.get("OS_BAL_OF_RECON_DIFF_ENTRY_BC")).subtract(oldDifferenceBase).add(new BigDecimal(processData.get("DIFF_AMT_OF_ENTRY_AMT_BC"))));
				erecondiff.setField("REMARKS", processData.get("REMARKS"));
				erecondiff.setNew(false);
				if (erecondiff.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			erecondiff.close();
		}
	}
}
