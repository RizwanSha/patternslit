package patterns.config.framework.bo.cmn;

import java.util.Date;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.web.BackOfficeFormatUtils;

public class iprodintparamBO extends GenericBO {

	TBADynaSQL iprodintparam = null;

	public iprodintparamBO() {
	}

	public void init() {

		iprodintparam = new TBADynaSQL("PRODINTPARAMHIST", true);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("PRODUCT_CODE") + RegularConstants.PK_SEPARATOR + processData.get("INT_PAY_RECOVERY") + RegularConstants.PK_SEPARATOR + processData.get("CCY_CODE") + RegularConstants.PK_SEPARATOR
				+ BackOfficeFormatUtils.getDate((Date) processData.getDate("EFF_DATE"), BackOfficeConstants.TBA_DATE_FORMAT);
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("PRODUCT_CODE") + RegularConstants.PK_SEPARATOR + processData.get("INT_PAY_RECOVERY"));
		iprodintparam.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		iprodintparam.setField("PRODUCT_CODE", processData.get("PRODUCT_CODE"));
		iprodintparam.setField("INT_PAY_RECOVERY", processData.get("INT_PAY_RECOVERY"));
		iprodintparam.setField("CCY_CODE", processData.get("CCY_CODE"));
		iprodintparam.setField("EFF_DATE", processData.getDate("EFF_DATE"));
		dataExists = iprodintparam.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				iprodintparam.setNew(true);
				iprodintparam.setField("INT_METHOD", processData.get("INT_METHOD"));
				iprodintparam.setField("PROD_INT_TYPE", processData.get("PROD_INT_TYPE"));
				iprodintparam.setField("PROD_COMPOUND_FREQ", processData.get("PROD_COMPOUND_FREQ"));
				iprodintparam.setField("COMPOUND_FREQ_ALIGN", processData.get("COMPOUND_FREQ_ALIGN"));
				iprodintparam.setField("INT_ACCRUAL_FREQ", processData.get("INT_ACCRUAL_FREQ"));
				iprodintparam.setField("INT_PAY_RECO_FREQ", processData.get("INT_PAY_RECO_FREQ"));
				iprodintparam.setField("FIXED_INT_FREQ", processData.get("FIXED_INT_FREQ"));
				iprodintparam.setField("MONTHLY_INT_ALLOW", processData.get("MONTHLY_INT_ALLOW"));
				iprodintparam.setField("QUARTERLY_INT_ALLOW", processData.get("QUARTERLY_INT_ALLOW"));
				iprodintparam.setField("HALF_YEARLY_INT_ALLOW", processData.get("HALF_YEARLY_INT_ALLOW"));
				iprodintparam.setField("YEARLY_INT_ALLOW", processData.get("YEARLY_INT_ALLOW"));
				iprodintparam.setField("ON_MATURITY_INT_ALLOW", processData.get("ON_MATURITY_INT_ALLOW"));
				iprodintparam.setField("PROD_BASIS", processData.get("PROD_BASIS"));
				iprodintparam.setField("MONTH_PROD_BEGIN_DAY", processData.get("MONTH_PROD_BEGIN_DAY"));
				iprodintparam.setField("MONTH_PROD_END_DAY", processData.get("MONTH_PROD_END_DAY"));
				iprodintparam.setField("BALANCE_SELECTION", processData.get("BALANCE_SELECTION"));
				iprodintparam.setField("INTERIM_ACCR_ROUND_OFF_CHOICE", processData.get("INTERIM_ACCR_ROUND_OFF_CHOICE"));
				iprodintparam.setField("INTERIM_ACCR_ROUND_OFF_FACTOR", processData.get("INTERIM_ACCR_ROUND_OFF_FACTOR"));
				iprodintparam.setField("END_ACCR_ROUND_OFF_CHOICE", processData.get("END_ACCR_ROUND_OFF_CHOICE"));
				iprodintparam.setField("END_ACCR_ROUND_OFF_FACTOR", processData.get("END_ACCR_ROUND_OFF_FACTOR"));
				iprodintparam.setField("INT_CREDIT_DEBIT_TO", processData.get("INT_CREDIT_DEBIT_TO"));
				iprodintparam.setField("SIMPLE_INT_REQD", processData.get("SIMPLE_INT_REQD"));
				iprodintparam.setField("INT_RATES_SPEC", processData.get("INT_RATES_SPEC"));
				iprodintparam.setField("INT_RATES_CHG_BY_AC_TYPE", processData.get("INT_RATES_CHG_BY_AC_TYPE"));
				iprodintparam.setField("INT_RATES_CHG_BY_AC_SUB_TYPE", processData.get("INT_RATES_CHG_BY_AC_SUB_TYPE"));
				iprodintparam.setField("INT_RATE_TYPE_CODE_NORMAL", processData.get("INT_RATE_TYPE_CODE_NORMAL"));
				iprodintparam.setField("INT_RATE_TYPE_CODE_OD", processData.get("INT_RATE_TYPE_CODE_OD"));
				iprodintparam.setField("INT_RATE_TYPE_CODE_PENAL", processData.get("INT_RATE_TYPE_CODE_PENAL"));
				iprodintparam.setField("ENABLED", processData.get("ENABLED"));
				iprodintparam.setField("REMARKS", processData.get("REMARKS"));
				if (iprodintparam.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				iprodintparam.setNew(false);
				iprodintparam.setField("INT_METHOD", processData.get("INT_METHOD"));
				iprodintparam.setField("PROD_INT_TYPE", processData.get("PROD_INT_TYPE"));
				iprodintparam.setField("PROD_COMPOUND_FREQ", processData.get("PROD_COMPOUND_FREQ"));
				iprodintparam.setField("COMPOUND_FREQ_ALIGN", processData.get("COMPOUND_FREQ_ALIGN"));
				iprodintparam.setField("INT_ACCRUAL_FREQ", processData.get("INT_ACCRUAL_FREQ"));
				iprodintparam.setField("INT_PAY_RECO_FREQ", processData.get("INT_PAY_RECO_FREQ"));
				iprodintparam.setField("FIXED_INT_FREQ", processData.get("FIXED_INT_FREQ"));
				iprodintparam.setField("MONTHLY_INT_ALLOW", processData.get("MONTHLY_INT_ALLOW"));
				iprodintparam.setField("QUARTERLY_INT_ALLOW", processData.get("QUARTERLY_INT_ALLOW"));
				iprodintparam.setField("HALF_YEARLY_INT_ALLOW", processData.get("HALF_YEARLY_INT_ALLOW"));
				iprodintparam.setField("YEARLY_INT_ALLOW", processData.get("YEARLY_INT_ALLOW"));
				iprodintparam.setField("ON_MATURITY_INT_ALLOW", processData.get("ON_MATURITY_INT_ALLOW"));
				iprodintparam.setField("PROD_BASIS", processData.get("PROD_BASIS"));
				iprodintparam.setField("MONTH_PROD_BEGIN_DAY", processData.get("MONTH_PROD_BEGIN_DAY"));
				iprodintparam.setField("MONTH_PROD_END_DAY", processData.get("MONTH_PROD_END_DAY"));
				iprodintparam.setField("BALANCE_SELECTION", processData.get("BALANCE_SELECTION"));
				iprodintparam.setField("INTERIM_ACCR_ROUND_OFF_CHOICE", processData.get("INTERIM_ACCR_ROUND_OFF_CHOICE"));
				iprodintparam.setField("INTERIM_ACCR_ROUND_OFF_FACTOR", processData.get("INTERIM_ACCR_ROUND_OFF_FACTOR"));
				iprodintparam.setField("END_ACCR_ROUND_OFF_CHOICE", processData.get("END_ACCR_ROUND_OFF_CHOICE"));
				iprodintparam.setField("END_ACCR_ROUND_OFF_FACTOR", processData.get("END_ACCR_ROUND_OFF_FACTOR"));
				iprodintparam.setField("INT_CREDIT_DEBIT_TO", processData.get("INT_CREDIT_DEBIT_TO"));
				iprodintparam.setField("SIMPLE_INT_REQD", processData.get("SIMPLE_INT_REQD"));
				iprodintparam.setField("INT_RATES_SPEC", processData.get("INT_RATES_SPEC"));
				iprodintparam.setField("INT_RATES_CHG_BY_AC_TYPE", processData.get("INT_RATES_CHG_BY_AC_TYPE"));
				iprodintparam.setField("INT_RATES_CHG_BY_AC_SUB_TYPE", processData.get("INT_RATES_CHG_BY_AC_SUB_TYPE"));
				iprodintparam.setField("INT_RATE_TYPE_CODE_NORMAL", processData.get("INT_RATE_TYPE_CODE_NORMAL"));
				iprodintparam.setField("INT_RATE_TYPE_CODE_OD", processData.get("INT_RATE_TYPE_CODE_OD"));
				iprodintparam.setField("INT_RATE_TYPE_CODE_PENAL", processData.get("INT_RATE_TYPE_CODE_PENAL"));
				iprodintparam.setField("ENABLED", processData.get("ENABLED"));
				iprodintparam.setField("REMARKS", processData.get("REMARKS"));
				if (iprodintparam.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			iprodintparam.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				iprodintparam.setNew(true);
				iprodintparam.setField("INT_METHOD", processData.get("INT_METHOD"));
				iprodintparam.setField("PROD_INT_TYPE", processData.get("PROD_INT_TYPE"));
				iprodintparam.setField("PROD_COMPOUND_FREQ", processData.get("PROD_COMPOUND_FREQ"));
				iprodintparam.setField("COMPOUND_FREQ_ALIGN", processData.get("COMPOUND_FREQ_ALIGN"));
				iprodintparam.setField("INT_ACCRUAL_FREQ", processData.get("INT_ACCRUAL_FREQ"));
				iprodintparam.setField("INT_PAY_RECO_FREQ", processData.get("INT_PAY_RECO_FREQ"));
				iprodintparam.setField("FIXED_INT_FREQ", processData.get("FIXED_INT_FREQ"));
				iprodintparam.setField("MONTHLY_INT_ALLOW", processData.get("MONTHLY_INT_ALLOW"));
				iprodintparam.setField("QUARTERLY_INT_ALLOW", processData.get("QUARTERLY_INT_ALLOW"));
				iprodintparam.setField("HALF_YEARLY_INT_ALLOW", processData.get("HALF_YEARLY_INT_ALLOW"));
				iprodintparam.setField("YEARLY_INT_ALLOW", processData.get("YEARLY_INT_ALLOW"));
				iprodintparam.setField("ON_MATURITY_INT_ALLOW", processData.get("ON_MATURITY_INT_ALLOW"));
				iprodintparam.setField("PROD_BASIS", processData.get("PROD_BASIS"));
				iprodintparam.setField("MONTH_PROD_BEGIN_DAY", processData.get("MONTH_PROD_BEGIN_DAY"));
				iprodintparam.setField("MONTH_PROD_END_DAY", processData.get("MONTH_PROD_END_DAY"));
				iprodintparam.setField("BALANCE_SELECTION", processData.get("BALANCE_SELECTION"));
				iprodintparam.setField("INTERIM_ACCR_ROUND_OFF_CHOICE", processData.get("INTERIM_ACCR_ROUND_OFF_CHOICE"));
				iprodintparam.setField("INTERIM_ACCR_ROUND_OFF_FACTOR", processData.get("INTERIM_ACCR_ROUND_OFF_FACTOR"));
				iprodintparam.setField("END_ACCR_ROUND_OFF_CHOICE", processData.get("END_ACCR_ROUND_OFF_CHOICE"));
				iprodintparam.setField("END_ACCR_ROUND_OFF_FACTOR", processData.get("END_ACCR_ROUND_OFF_FACTOR"));
				iprodintparam.setField("INT_CREDIT_DEBIT_TO", processData.get("INT_CREDIT_DEBIT_TO"));
				iprodintparam.setField("SIMPLE_INT_REQD", processData.get("SIMPLE_INT_REQD"));
				iprodintparam.setField("INT_RATES_SPEC", processData.get("INT_RATES_SPEC"));
				iprodintparam.setField("INT_RATES_CHG_BY_AC_TYPE", processData.get("INT_RATES_CHG_BY_AC_TYPE"));
				iprodintparam.setField("INT_RATES_CHG_BY_AC_SUB_TYPE", processData.get("INT_RATES_CHG_BY_AC_SUB_TYPE"));
				iprodintparam.setField("INT_RATE_TYPE_CODE_NORMAL", processData.get("INT_RATE_TYPE_CODE_NORMAL"));
				iprodintparam.setField("INT_RATE_TYPE_CODE_OD", processData.get("INT_RATE_TYPE_CODE_OD"));
				iprodintparam.setField("INT_RATE_TYPE_CODE_PENAL", processData.get("INT_RATE_TYPE_CODE_PENAL"));
				iprodintparam.setField("ENABLED", processData.get("ENABLED"));
				iprodintparam.setField("REMARKS", processData.get("REMARKS"));
				if (iprodintparam.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				iprodintparam.setNew(false);
				iprodintparam.setField("INT_METHOD", processData.get("INT_METHOD"));
				iprodintparam.setField("PROD_INT_TYPE", processData.get("PROD_INT_TYPE"));
				iprodintparam.setField("PROD_COMPOUND_FREQ", processData.get("PROD_COMPOUND_FREQ"));
				iprodintparam.setField("COMPOUND_FREQ_ALIGN", processData.get("COMPOUND_FREQ_ALIGN"));
				iprodintparam.setField("INT_ACCRUAL_FREQ", processData.get("INT_ACCRUAL_FREQ"));
				iprodintparam.setField("INT_PAY_RECO_FREQ", processData.get("INT_PAY_RECO_FREQ"));
				iprodintparam.setField("FIXED_INT_FREQ", processData.get("FIXED_INT_FREQ"));
				iprodintparam.setField("MONTHLY_INT_ALLOW", processData.get("MONTHLY_INT_ALLOW"));
				iprodintparam.setField("QUARTERLY_INT_ALLOW", processData.get("QUARTERLY_INT_ALLOW"));
				iprodintparam.setField("HALF_YEARLY_INT_ALLOW", processData.get("HALF_YEARLY_INT_ALLOW"));
				iprodintparam.setField("YEARLY_INT_ALLOW", processData.get("YEARLY_INT_ALLOW"));
				iprodintparam.setField("ON_MATURITY_INT_ALLOW", processData.get("ON_MATURITY_INT_ALLOW"));
				iprodintparam.setField("PROD_BASIS", processData.get("PROD_BASIS"));
				iprodintparam.setField("MONTH_PROD_BEGIN_DAY", processData.get("MONTH_PROD_BEGIN_DAY"));
				iprodintparam.setField("MONTH_PROD_END_DAY", processData.get("MONTH_PROD_END_DAY"));
				iprodintparam.setField("BALANCE_SELECTION", processData.get("BALANCE_SELECTION"));
				iprodintparam.setField("INTERIM_ACCR_ROUND_OFF_CHOICE", processData.get("INTERIM_ACCR_ROUND_OFF_CHOICE"));
				iprodintparam.setField("INTERIM_ACCR_ROUND_OFF_FACTOR", processData.get("INTERIM_ACCR_ROUND_OFF_FACTOR"));
				iprodintparam.setField("END_ACCR_ROUND_OFF_CHOICE", processData.get("END_ACCR_ROUND_OFF_CHOICE"));
				iprodintparam.setField("END_ACCR_ROUND_OFF_FACTOR", processData.get("END_ACCR_ROUND_OFF_FACTOR"));
				iprodintparam.setField("INT_CREDIT_DEBIT_TO", processData.get("INT_CREDIT_DEBIT_TO"));
				iprodintparam.setField("SIMPLE_INT_REQD", processData.get("SIMPLE_INT_REQD"));
				iprodintparam.setField("INT_RATES_SPEC", processData.get("INT_RATES_SPEC"));
				iprodintparam.setField("INT_RATES_CHG_BY_AC_TYPE", processData.get("INT_RATES_CHG_BY_AC_TYPE"));
				iprodintparam.setField("INT_RATES_CHG_BY_AC_SUB_TYPE", processData.get("INT_RATES_CHG_BY_AC_SUB_TYPE"));
				iprodintparam.setField("INT_RATE_TYPE_CODE_NORMAL", processData.get("INT_RATE_TYPE_CODE_NORMAL"));
				iprodintparam.setField("INT_RATE_TYPE_CODE_OD", processData.get("INT_RATE_TYPE_CODE_OD"));
				iprodintparam.setField("INT_RATE_TYPE_CODE_PENAL", processData.get("INT_RATE_TYPE_CODE_PENAL"));
				iprodintparam.setField("ENABLED", processData.get("ENABLED"));
				iprodintparam.setField("REMARKS", processData.get("REMARKS"));
				if (iprodintparam.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			iprodintparam.close();
		}
	}

}
