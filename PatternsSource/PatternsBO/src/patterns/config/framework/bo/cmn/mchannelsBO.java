package patterns.config.framework.bo.cmn;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;

public class mchannelsBO extends GenericBO {
	TBADynaSQL mchannels = null;
	public mchannelsBO() {
		
	}

	public void init() {		
		mchannels = new TBADynaSQL("CHANNELS", true);
		primaryKey = processData.get("ENTITY_CODE")+RegularConstants.PK_SEPARATOR + processData.get("CHANNEL_CODE");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("DESCRIPTION"));
		mchannels.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		mchannels.setField("CHANNEL_CODE", processData.get("CHANNEL_CODE"));
		dataExists = mchannels.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				mchannels.setNew(true);
				mchannels.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				mchannels.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				mchannels.setField("RETAIL_BANKING_FLAG", processData.get("RETAIL_BANKING_FLAG"));
				mchannels.setField("CORPORATE_BANKING_FLAG", processData.get("CORPORATE_BANKING_FLAG"));
				mchannels.setField("PURPOSE_OF_CHANNEL", processData.get("PURPOSE_OF_CHANNEL"));
				mchannels.setField("ENABLED", processData.get("ENABLED"));
				mchannels.setField("REMARKS", processData.get("REMARKS"));
				
				if (mchannels.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mchannels.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				mchannels.setNew(false);
				mchannels.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				mchannels.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				mchannels.setField("RETAIL_BANKING_FLAG", processData.get("RETAIL_BANKING_FLAG"));
				mchannels.setField("CORPORATE_BANKING_FLAG", processData.get("CORPORATE_BANKING_FLAG"));
				mchannels.setField("PURPOSE_OF_CHANNEL", processData.get("PURPOSE_OF_CHANNEL"));
				mchannels.setField("ENABLED", processData.get("ENABLED"));
				mchannels.setField("REMARKS", processData.get("REMARKS"));
				if (mchannels.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mchannels.close();
		}
	}
}
