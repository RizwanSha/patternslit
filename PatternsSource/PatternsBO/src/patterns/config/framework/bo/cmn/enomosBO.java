package patterns.config.framework.bo.cmn;

import java.util.ArrayList;
import java.util.List;

import patterns.config.framework.bo.AdditionalDetailInfo;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.bo.MessageParam;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAActionType;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTObject;

/**
 * 
 * 
 * @version 1.0
 * @author Vivekananda Reddy
 * @since 14-DEC-2016 <br>
 *        <b>Modified History</b> <BR>
 *        <U><B> Sl.No Modified Date Author Modified Changes Version </B></U> <br>
 * 
 * 
 */
public class enomosBO extends GenericBO {
	TBADynaSQL enomos = null;

	public enomosBO() {
	}

	public void init() {
		enomos = new TBADynaSQL("NOMOS");
		long sytemRefNo = 0;
		if (tbaContext.getProcessAction().getActionType().equals(TBAActionType.ADD)) {
			DTObject input = new DTObject();
			input.set("SOURCE_KEY", processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("BRANCH_CODE") + RegularConstants.PK_SEPARATOR + processData.get("GL_HEAD_CODE"));
			input.set("PGM_ID", tbaContext.getProcessID());
			sytemRefNo = (long) enomos.getTBAReferenceSerial(input);
			processData.set("SYSTEM_REF_NO", Long.toString(sytemRefNo));
		}
		long sytemTranRefNo = 0;

		if (RegularConstants.ONE.equals(processData.get("NEW_REF_REQ"))) {
			DTObject input = new DTObject();
			input.set("SOURCE_KEY", processData.get("SYSTEM_TRAN_REF_NO_DATE"));
			input.set("PGM_ID", tbaContext.getProcessID());
			sytemTranRefNo = (long) enomos.getTBAReferenceSerial(input);
			processData.set("SYSTEM_TRAN_REF_NO_BATCH", Long.toString(sytemTranRefNo));
			processData.set("SYSTEM_TRAN_REF_NO_SERIAL", RegularConstants.ONE);
		}
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("BRANCH_CODE") + RegularConstants.PK_SEPARATOR + processData.get("GL_HEAD_CODE") + RegularConstants.PK_SEPARATOR + processData.get("SYSTEM_REF_NO");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("BRANCH_CODE"));
		enomos = new TBADynaSQL("NOMOS", true);
		enomos.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		enomos.setField("BRANCH_CODE", processData.get("BRANCH_CODE"));
		enomos.setField("GL_HEAD_CODE", processData.get("GL_HEAD_CODE"));
		enomos.setField("SYSTEM_REF_NO", processData.get("SYSTEM_REF_NO"));
		dataExists = enomos.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				enomos.setNew(true);
				enomos.setField("ENTRY_TYPE", processData.get("ENTRY_TYPE"));
				enomos.setField("OP_BAL_ENTRY", processData.get("OP_BAL_ENTRY"));
				enomos.setField("INTER_BRN_TRAN_TYPES", processData.get("INTER_BRN_TRAN_TYPES"));
				enomos.setField("ENTRY_DATE", processData.getDate("ENTRY_DATE"));
				enomos.setField("TRAN_DB_CR_FLAG", processData.get("TRAN_DB_CR_FLAG"));
				enomos.setField("ORIG_ENTRY_CCY_AC", processData.get("ORIG_ENTRY_CCY_AC"));
				enomos.setField("ORIG_ENTRY_AMT_AC", processData.get("ORIG_ENTRY_AMT_AC"));
				enomos.setField("ORIG_ENTRY_CCY_BC", processData.get("ORIG_ENTRY_CCY_BC"));
				enomos.setField("ORIG_ENTRY_AMT_BC", processData.get("ORIG_ENTRY_AMT_BC"));
				enomos.setField("MANUAL_ENTRY_REF_NO", processData.get("MANUAL_ENTRY_REF_NO"));
				enomos.setField("SYSTEM_TRAN_REF_NO_DATE", processData.getDate("SYSTEM_TRAN_REF_NO_DATE"));
				enomos.setField("SYSTEM_TRAN_REF_NO_BATCH", processData.get("SYSTEM_TRAN_REF_NO_BATCH"));
				enomos.setField("SYSTEM_TRAN_REF_NO_SERIAL", processData.get("SYSTEM_TRAN_REF_NO_SERIAL"));
				enomos.setField("OPEN_BAL_OF_ENTRY_AC", processData.get("OPEN_BAL_OF_ENTRY_AC"));
				enomos.setField("OPEN_BAL_OF_ENTRY_DBCR_AC", processData.get("OPEN_BAL_OF_ENTRY_DBCR_AC"));
				enomos.setField("OPEN_BAL_OF_ENTRY_BC", processData.get("OPEN_BAL_OF_ENTRY_BC"));
				enomos.setField("OPEN_BAL_OF_ENTRY_DBCR_BC", processData.get("OPEN_BAL_OF_ENTRY_DBCR_AC"));
				enomos.setField("OPEN_BALANCE_BASE_DATE", processData.getDate("OPEN_BALANCE_BASE_DATE"));
				enomos.setField("CIF_NO", processData.get("CIF_NO"));
				enomos.setField("EMP_ID", processData.get("EMP_ID"));
				enomos.setField("REMARKS", processData.get("REMARKS"));
				if (enomos.save()) {
					AdditionalDetailInfo additionalInfo = new AdditionalDetailInfo();
					additionalInfo.setHeading(new MessageParam(BackOfficeErrorCodes.PBS_NOMINAL_RESULT_HEADER));

					List<MessageParam> key = new ArrayList<MessageParam>();
					key.add(new MessageParam(BackOfficeErrorCodes.PBS_SYSTEM_REF_NO));
					if (RegularConstants.ONE.equals(processData.get("NEW_REF_REQ")))
						key.add(new MessageParam(BackOfficeErrorCodes.PBS_SYSTEM_TRAN_REF_NO));
					additionalInfo.setKey(key);

					List<String> value = new ArrayList<String>();
					value.add(processData.get("SYSTEM_REF_NO"));
					if (RegularConstants.ONE.equals(processData.get("NEW_REF_REQ")))
						value.add(processData.get("SYSTEM_TRAN_REF_NO_DATE") + RegularConstants.PK_SEPARATOR + processData.get("SYSTEM_TRAN_REF_NO_BATCH") + RegularConstants.PK_SEPARATOR + processData.get("SYSTEM_TRAN_REF_NO_SERIAL"));

					additionalInfo.setValue(value);
					processResult.setAdditionalInfo(additionalInfo);
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			enomos.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				enomos.setNew(false);
				enomos.setField("ENTRY_TYPE", processData.get("ENTRY_TYPE"));
				enomos.setField("OP_BAL_ENTRY", processData.get("OP_BAL_ENTRY"));
				enomos.setField("INTER_BRN_TRAN_TYPES", processData.get("INTER_BRN_TRAN_TYPES"));
				enomos.setField("ENTRY_DATE", processData.getDate("ENTRY_DATE"));
				enomos.setField("TRAN_DB_CR_FLAG", processData.get("TRAN_DB_CR_FLAG"));
				enomos.setField("ORIG_ENTRY_CCY_AC", processData.get("ORIG_ENTRY_CCY_AC"));
				enomos.setField("ORIG_ENTRY_AMT_AC", processData.get("ORIG_ENTRY_AMT_AC"));
				enomos.setField("ORIG_ENTRY_CCY_BC", processData.get("ORIG_ENTRY_CCY_BC"));
				enomos.setField("ORIG_ENTRY_AMT_BC", processData.get("ORIG_ENTRY_AMT_BC"));
				enomos.setField("MANUAL_ENTRY_REF_NO", processData.get("MANUAL_ENTRY_REF_NO"));
				if (RegularConstants.ONE.equals(processData.get("NEW_REF_REQ"))) {
					enomos.setField("SYSTEM_TRAN_REF_NO_DATE", processData.getDate("SYSTEM_TRAN_REF_NO_DATE"));
					enomos.setField("SYSTEM_TRAN_REF_NO_BATCH", processData.get("SYSTEM_TRAN_REF_NO_BATCH"));
					enomos.setField("SYSTEM_TRAN_REF_NO_SERIAL", processData.get("SYSTEM_TRAN_REF_NO_SERIAL"));
				}
				enomos.setField("OPEN_BAL_OF_ENTRY_AC", processData.get("OPEN_BAL_OF_ENTRY_AC"));
				enomos.setField("OPEN_BAL_OF_ENTRY_DBCR_AC", processData.get("OPEN_BAL_OF_ENTRY_DBCR_AC"));
				enomos.setField("OPEN_BAL_OF_ENTRY_BC", processData.get("OPEN_BAL_OF_ENTRY_BC"));
				enomos.setField("OPEN_BAL_OF_ENTRY_DBCR_BC", processData.get("OPEN_BAL_OF_ENTRY_DBCR_AC"));
				enomos.setField("OPEN_BALANCE_BASE_DATE", processData.getDate("OPEN_BALANCE_BASE_DATE"));
				enomos.setField("CIF_NO", processData.get("CIF_NO"));
				enomos.setField("EMP_ID", processData.get("EMP_ID"));
				enomos.setField("REMARKS", processData.get("REMARKS"));
				if (enomos.save()) {
					AdditionalDetailInfo additionalInfo = new AdditionalDetailInfo();
					additionalInfo.setHeading(new MessageParam(BackOfficeErrorCodes.PBS_NOMINAL_RESULT_HEADER));

					List<MessageParam> key = new ArrayList<MessageParam>();
					List<String> value = new ArrayList<String>();
					if (RegularConstants.ONE.equals(processData.get("NEW_REF_REQ"))) {
						key.add(new MessageParam(BackOfficeErrorCodes.PBS_SYSTEM_TRAN_REF_NO));
						additionalInfo.setKey(key);
						value.add(processData.get("SYSTEM_TRAN_REF_NO_DATE") + RegularConstants.PK_SEPARATOR + processData.get("SYSTEM_TRAN_REF_NO_BATCH") + RegularConstants.PK_SEPARATOR + processData.get("SYSTEM_TRAN_REF_NO_SERIAL"));
						additionalInfo.setValue(value);
						processResult.setAdditionalInfo(additionalInfo);
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			enomos.close();
		}
	}
}