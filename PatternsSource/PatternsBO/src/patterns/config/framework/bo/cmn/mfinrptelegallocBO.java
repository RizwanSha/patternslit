package patterns.config.framework.bo.cmn;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.BindParameterType;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTDObject;
public class mfinrptelegallocBO extends GenericBO {

	TBADynaSQL finrptelegalloc = null;
	TBADynaSQL finrptelegallocdtl = null;

	public mfinrptelegallocBO() {
	}

	public void init() {
		finrptelegalloc = new TBADynaSQL("FINRPTELEGLALLOC", true);
		finrptelegallocdtl = new TBADynaSQL("FINRPTELEGLALLOCDTL", false);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("REPORT_ELEMENT_CODE");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("REPORT_ELEMENT_CODE"));
		finrptelegalloc.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		finrptelegalloc.setField("REPORT_ELEMENT_CODE", processData.get("REPORT_ELEMENT_CODE"));
		dataExists = finrptelegalloc.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				finrptelegalloc.setField("REMARKS", processData.getObject("REMARKS"));
				finrptelegalloc.setNew(true);
				if (finrptelegalloc.save()) {
					finrptelegallocdtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
					finrptelegallocdtl.setField("REPORT_ELEMENT_CODE", processData.get("REPORT_ELEMENT_CODE"));
					DTDObject detailData = processData.getDTDObject("FINRPTELEGLALLOCDTL");
					finrptelegallocdtl.setNew(true);
					int count = 1;
					for (int i = 0; i < detailData.getRowCount(); ++i) {
						finrptelegallocdtl.setField("ELEMENT_SL", count);
						finrptelegallocdtl.setField("GL_HEAD_CODE", detailData.getValue(i, 1));
						finrptelegallocdtl.setField("PICK_UP_CONDITION", detailData.getValue(i, 3));
						finrptelegallocdtl.setField("REPORTING_LABEL", detailData.getValue(i, 4));
						finrptelegallocdtl.setField("ADJ_DIFF_TOBE_ADDED", detailData.getValue(i, 5));
						if (!finrptelegallocdtl.save()) {
							processResult.setProcessStatus(TBAProcessStatus.FAILURE);
							processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
							return;
						}
						count++;
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());

		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				finrptelegalloc.setField("REMARKS", processData.getObject("REMARKS"));
				finrptelegalloc.setNew(false);
				if (finrptelegalloc.save()) {
					String[] finrptelegallocdtlFields = { "ENTITY_CODE", "REPORT_ELEMENT_CODE", "ELEMENT_SL" };
					BindParameterType[] finrptelegallocdtlFieldTypes = { BindParameterType.BIGINT, BindParameterType.VARCHAR, BindParameterType.INTEGER };
					finrptelegallocdtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
					finrptelegallocdtl.setField("REPORT_ELEMENT_CODE", processData.get("REPORT_ELEMENT_CODE"));
					finrptelegallocdtl.setField("ELEMENT_SL", processData.get("ELEMENT_SL"));
					finrptelegallocdtl.deleteByFieldsAudit(finrptelegallocdtlFields, finrptelegallocdtlFieldTypes);
					DTDObject detailData = processData.getDTDObject("FINRPTELEGLALLOCDTL");
					finrptelegallocdtl.setNew(true);
					int count = 1;
					for (int i = 0; i < detailData.getRowCount(); ++i) {
						finrptelegallocdtl.setField("ELEMENT_SL", count);
						finrptelegallocdtl.setField("GL_HEAD_CODE", detailData.getValue(i, 1));
						finrptelegallocdtl.setField("PICK_UP_CONDITION", detailData.getValue(i, 3));
						finrptelegallocdtl.setField("REPORTING_LABEL", detailData.getValue(i, 4));
						finrptelegallocdtl.setField("ADJ_DIFF_TOBE_ADDED", detailData.getValue(i, 5));
						if (!finrptelegallocdtl.save()) {
							processResult.setProcessStatus(TBAProcessStatus.FAILURE);
							processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
							return;
						}
						count++;
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		}
	}
}