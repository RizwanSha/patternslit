package patterns.config.framework.bo.cmn;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;

public class mintmethodsBO extends GenericBO {
	TBADynaSQL mintmethods = null;

	public mintmethodsBO() {
	}

	public void init() {
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("INT_METHOD");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("INT_METHOD"));
		mintmethods = new TBADynaSQL("INTMETHODS", true);
		mintmethods.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		mintmethods.setField("INT_METHOD", processData.get("INT_METHOD"));
		dataExists = mintmethods.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				mintmethods.setNew(true);
				mintmethods.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				mintmethods.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				mintmethods.setField("CALC_FOR", processData.get("CALC_FOR"));
				mintmethods.setField("INT_METHOD_CHOICE", processData.get("INT_METHOD_CHOICE"));
				mintmethods.setField("ENABLED", processData.get("ENABLED"));
				mintmethods.setField("REMARKS", processData.get("REMARKS"));
				if (mintmethods.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mintmethods.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				mintmethods.setNew(false);
				mintmethods.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				mintmethods.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				mintmethods.setField("CALC_FOR", processData.get("CALC_FOR"));
				mintmethods.setField("INT_METHOD_CHOICE", processData.get("INT_METHOD_CHOICE"));
				mintmethods.setField("ENABLED", processData.get("ENABLED"));
				mintmethods.setField("REMARKS", processData.get("REMARKS"));
				if (mintmethods.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mintmethods.close();
		}
	}
}