package patterns.config.framework.bo.cmn;

import patterns.config.framework.bo.BackOfficeErrorCodes;
/**
 * This program will be used to maintain Employee Details and to test about
 * employee PF number and Esi number eligible for current employee Code.
 * 
 * 
 * @version 1.0
 * @author TULASI THOTA
 * @since 26-Nov-2014 <br>
 *        <b>Modified History</b> <BR>
 *        <U><B> Sl.No Modified Date Author Modified Changes Version 1
 *        03-02-2016 Naresh 3 fields added </B></U> <br>
 *        @15-07-2016
 * Person Id Field Removed and Person Daetils capturing in screen..  (@Modified author Pavan Kumar R)
 */


import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;


public class mempdbBO extends GenericBO {

	TBADynaSQL mempdb = null;

	public mempdbBO() {
	}

	public void init() {
		mempdb = new TBADynaSQL("EMPDB", true);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("EMP_CODE");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("EMP_CODE"));
		mempdb.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		mempdb.setField("EMP_CODE", processData.get("EMP_CODE"));
		dataExists = mempdb.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				mempdb.setNew(true);
				mempdb.setField("TITLE_CODE", processData.get("TITLE_CODE"));
				mempdb.setField("PERSON_NAME", processData.get("PERSON_NAME"));
				mempdb.setField("GENDER", processData.get("GENDER"));
				mempdb.setField("DATE_OF_BIRTH", processData.getDate("DATE_OF_BIRTH"));
				mempdb.setField("FATHER_HUSBAND_FLAG", processData.get("FATHER_HUSBAND_FLAG"));
				mempdb.setField("FATHER_OR_HUSBAND_NAME", processData.get("FATHER_OR_HUSBAND_NAME"));
				mempdb.setField("RETIREMENT_DATE", processData.getDate("RETIREMENT_DATE"));
				mempdb.setField("DATE_OF_JOIN", processData.getDate("DATE_OF_JOIN"));
				mempdb.setField("PF_APPLICABLE", processData.get("PF_APPLICABLE"));
				mempdb.setField("PF_NUMBER", processData.get("PF_NUMBER"));
				mempdb.setField("PENSION_FUND_REQD", processData.get("PENSION_FUND_REQD"));
				mempdb.setField("ELIGIBLE_FOR_ESI", processData.get("ELIGIBLE_FOR_ESI"));
				mempdb.setField("EMP_ESI_NO", processData.get("EMP_ESI_NO"));
				mempdb.setField("EMP_QUALIFICATION", processData.get("EMP_QUALIFICATION"));
				mempdb.setField("EMAIL_ID", processData.get("EMAIL_ID"));
				mempdb.setField("MOBILE_NO", processData.get("MOBILE_NO"));
				mempdb.setField("REMARKS", processData.getObject("REMARKS"));
				if (mempdb.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mempdb.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				mempdb.setNew(false);
				mempdb.setField("TITLE_CODE", processData.get("TITLE_CODE"));
				mempdb.setField("PERSON_NAME", processData.get("PERSON_NAME"));
				mempdb.setField("GENDER", processData.get("GENDER"));
				mempdb.setField("DATE_OF_BIRTH", processData.getDate("DATE_OF_BIRTH"));
				mempdb.setField("EMAIL_ID", processData.get("EMAIL_ID"));
				mempdb.setField("MOBILE", processData.get("MOBILE"));
				mempdb.setField("FATHER_HUSBAND_FLAG", processData.get("FATHER_HUSBAND_FLAG"));
				mempdb.setField("FATHER_OR_HUSBAND_NAME", processData.get("FATHER_OR_HUSBAND_NAME"));
				mempdb.setField("RETIREMENT_DATE", processData.getDate("RETIREMENT_DATE"));
				mempdb.setField("DATE_OF_JOIN", processData.getDate("DATE_OF_JOIN"));
				mempdb.setField("PF_APPLICABLE", processData.get("PF_APPLICABLE"));
				mempdb.setField("PF_NUMBER", processData.get("PF_NUMBER"));
				mempdb.setField("PENSION_FUND_REQD", processData.get("PENSION_FUND_REQD"));
				mempdb.setField("ELIGIBLE_FOR_ESI", processData.get("ELIGIBLE_FOR_ESI"));
				mempdb.setField("EMP_ESI_NO", processData.get("EMP_ESI_NO"));
				mempdb.setField("EMP_QUALIFICATION", processData.get("EMP_QUALIFICATION"));
				mempdb.setField("EMAIL_ID", processData.get("EMAIL_ID"));
				mempdb.setField("MOBILE_NO", processData.get("MOBILE_NO"));
				mempdb.setField("REMARKS", processData.getObject("REMARKS"));
				if (mempdb.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mempdb.close();
		}
	}

}
