/*
 *
 Author : Aswaq A
 Created Date : 18-07-2016
 Spec Reference : PPB-PSD-CMN-0006-EUSERREGIONALLOC 
 Modification History
 ----------------------------------------------------------------------
 Sl.No		Modified Date	Author			Modified Changes	Version
 ----------------------------------------------------------------------

 */

package patterns.config.framework.bo.cmn;

import java.util.Date;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.BindParameterType;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTDObject;
import patterns.config.framework.web.BackOfficeFormatUtils;

public class euserregionallocBO extends GenericBO {

	TBADynaSQL euserregionallochist = null;
	TBADynaSQL euserregionallochistdtl = null;

	public euserregionallocBO() {
	}

	public void init() throws TBAFrameworkException {
		euserregionallochist = new TBADynaSQL("USERREGIONALLOCHIST", true);
		euserregionallochistdtl = new TBADynaSQL("USERREGIONALLOCHISTDTL", false);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("FIELD_USER_ID") + RegularConstants.PK_SEPARATOR + BackOfficeFormatUtils.getDate((Date) processData.getObject("EFF_DATE"), BackOfficeConstants.TBA_DATE_FORMAT);
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("FIELD_USER_ID"));
		euserregionallochist.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		euserregionallochist.setField("FIELD_USER_ID", processData.get("FIELD_USER_ID"));
		euserregionallochist.setField("EFF_DATE", processData.getObject("EFF_DATE"));
		dataExists = euserregionallochist.loadByKeyFields();
	}

	@Override
	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				euserregionallochist.setNew(true);
				euserregionallochist.setField("ENABLED", processData.get("ENABLED"));
				euserregionallochist.setField("REMARKS", processData.get("REMARKS"));
				if (euserregionallochist.save()) {
					euserregionallochistdtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
					euserregionallochistdtl.setField("FIELD_USER_ID", processData.get("FIELD_USER_ID"));
					euserregionallochistdtl.setField("EFF_DATE", processData.getObject("EFF_DATE"));
					DTDObject detailData = processData.getDTDObject("USERREGIONALLOCHISTDTL");
					euserregionallochistdtl.setNew(true);
					for (int i = 0; i < detailData.getRowCount(); ++i) {
						euserregionallochistdtl.setField("DTSL", Integer.valueOf(i + 1).toString());
						euserregionallochistdtl.setField("GEO_REGION_CODE", detailData.getValue(i, 1));
						if (!euserregionallochistdtl.save()) {
							processResult.setProcessStatus(TBAProcessStatus.FAILURE);
							processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
							return;
						}
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			euserregionallochist.close();
		}
	}

	@Override
	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				euserregionallochist.setNew(false);
				euserregionallochist.setField("ENABLED", processData.get("ENABLED"));
				euserregionallochist.setField("REMARKS", processData.get("REMARKS"));
				if (euserregionallochist.save()) {
					String[] euserregionallochistFields = { "ENTITY_CODE", "FIELD_USER_ID","EFF_DATE","DTSL" };
					BindParameterType[] euserregionallochistTypes = { BindParameterType.VARCHAR, BindParameterType.VARCHAR,BindParameterType.DATE, BindParameterType.BIGINT };
					euserregionallochistdtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
					euserregionallochistdtl.setField("FIELD_USER_ID", processData.get("FIELD_USER_ID"));
					euserregionallochistdtl.setField("EFF_DATE", processData.getObject("EFF_DATE"));
					euserregionallochistdtl.deleteByFieldsAudit(euserregionallochistFields, euserregionallochistTypes);
					DTDObject detailData = processData.getDTDObject("USERREGIONALLOCHISTDTL");
					euserregionallochistdtl.setNew(true);
					for (int i = 0; i < detailData.getRowCount(); ++i) {
						euserregionallochistdtl.setField("DTSL", Integer.valueOf(i + 1).toString());
						euserregionallochistdtl.setField("GEO_REGION_CODE", detailData.getValue(i, 1));
						if (!euserregionallochistdtl.save()) {
							processResult.setProcessStatus(TBAProcessStatus.FAILURE);
							processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
							return;
						}
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			euserregionallochist.close();
		}
	}

}
