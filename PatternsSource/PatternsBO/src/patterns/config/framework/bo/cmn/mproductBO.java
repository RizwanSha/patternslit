package patterns.config.framework.bo.cmn;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.BindParameterType;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTDObject;
import patterns.config.framework.web.ajax.ContentManager;

/**
 * 
 * 
 * @version 1.0
 * @author Dileep Kumar Reddy T
 * @since 27-Jun-2016 <br>
 *        <b>Modified History</b> ARULPRASATH <BR>
 *        <U><B> Sl.No Modified Date Author Modified Changes Version </B></U> <br>
 * 
 * 
 */

public class mproductBO extends GenericBO {
	TBADynaSQL mproduct = null;
	TBADynaSQL mproductdtl = null;

	public void init() {
		mproduct = new TBADynaSQL("PRODUCTS", true);
		mproductdtl = new TBADynaSQL("PRODUCTSKYC", false);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("PRODUCT_CODE");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("PRODUCT_CODE"));
		mproduct.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		mproduct.setField("PRODUCT_CODE", processData.get("PRODUCT_CODE"));
		dataExists = mproduct.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				mproduct.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				mproduct.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				mproduct.setField("PRODUCT_ALPHA_CODE", processData.get("PRODUCT_ALPHA_CODE"));
				mproduct.setField("PRODUCT_GROUP_CODE", processData.get("PRODUCT_GROUP_CODE"));
				mproduct.setField("PRODUCT_CLASS_CODE", processData.get("PRODUCT_CLASS_CODE"));
				mproduct.setField("START_DATE", processData.getDate("START_DATE"));
				mproduct.setField("CLOSURE_DATE", processData.getDate("CLOSURE_DATE"));
				mproduct.setField("ACNT_TYPE_REQUIRED", processData.get("ACNT_TYPE_REQUIRED"));
				mproduct.setField("ACNT_SUB_TYPE_REQUIRED", processData.get("ACNT_SUB_TYPE_REQUIRED"));
				mproduct.setField("ENABLED", processData.get("ENABLED"));
				mproduct.setField("REMARKS", processData.get("REMARKS"));
				mproduct.setNew(true);
				if (mproduct.save()) {
					mproductdtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
					mproductdtl.setField("PRODUCT_CODE", processData.get("PRODUCT_CODE"));
					DTDObject detailData = processData.getDTDObject("PRODUCTSKYC");
					mproductdtl.setNew(true);
					for (int i = 0; i < detailData.getRowCount(); ++i) {
						mproductdtl.setField("SL", String.valueOf(i + 1));
						mproductdtl.setField("KYC_CAT_CODE", detailData.getValue(i, 1));
						if (!mproductdtl.save()) {
							processResult.setProcessStatus(TBAProcessStatus.FAILURE);
							processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
						}
					}
					mproduct.setField(ContentManager.PURPOSE_ID, "PRODUCT_ALPHA_CODE");
					mproduct.setField(ContentManager.DEDUP_KEY, processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("PRODUCT_ALPHA_CODE"));
					if (processData.get("ENABLED").equals(RegularConstants.ONE) && !mproduct.udateTBADuplicateCheck()) {
						processResult.setProcessStatus(TBAProcessStatus.FAILURE);
						processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
						throw new TBAFrameworkException();
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mproduct.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				mproduct.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				mproduct.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				mproduct.setField("PRODUCT_ALPHA_CODE", processData.get("PRODUCT_ALPHA_CODE"));
				mproduct.setField("PRODUCT_GROUP_CODE", processData.get("PRODUCT_GROUP_CODE"));
				mproduct.setField("PRODUCT_CLASS_CODE", processData.get("PRODUCT_CLASS_CODE"));
				mproduct.setField("START_DATE", processData.getDate("START_DATE"));
				mproduct.setField("CLOSURE_DATE", processData.getDate("CLOSURE_DATE"));
				mproduct.setField("ACNT_TYPE_REQUIRED", processData.get("ACNT_TYPE_REQUIRED"));
				mproduct.setField("ACNT_SUB_TYPE_REQUIRED", processData.get("ACNT_SUB_TYPE_REQUIRED"));
				mproduct.setField("ENABLED", processData.get("ENABLED"));
				mproduct.setField("REMARKS", processData.get("REMARKS"));
				mproduct.setNew(false);
				if (mproduct.save()) {
					String[] mproductdtlFields = { "ENTITY_CODE", "PRODUCT_CODE", "SL" };
					BindParameterType[] mproductdtlFieldTypes = { BindParameterType.BIGINT, BindParameterType.BIGINT, BindParameterType.INTEGER };
					mproductdtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
					mproductdtl.setField("PRODUCT_CODE", processData.get("PRODUCT_CODE"));
					mproductdtl.setField("SL", processData.get("SL"));
					mproductdtl.deleteByFieldsAudit(mproductdtlFields, mproductdtlFieldTypes);
					DTDObject detailData = processData.getDTDObject("PRODUCTSKYC");
					mproductdtl.setNew(true);
					for (int i = 0; i < detailData.getRowCount(); ++i) {
						mproductdtl.setField("SL", String.valueOf(i + 1));
						mproductdtl.setField("KYC_CAT_CODE", detailData.getValue(i, 1));
						if (!mproductdtl.save()) {
							processResult.setProcessStatus(TBAProcessStatus.FAILURE);
							processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
							return;
						}
					}
					mproduct.setField(ContentManager.PURPOSE_ID, "PRODUCT_ALPHA_CODE");
					mproduct.setField(ContentManager.DEDUP_KEY, processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("PRODUCT_ALPHA_CODE"));
					if (processData.get("ENABLED").equals("1") && !mproduct.udateTBADuplicateCheck()) {
						processResult.setProcessStatus(TBAProcessStatus.FAILURE);
						processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
						throw new TBAFrameworkException();
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mproduct.close();
		}
	}

}
