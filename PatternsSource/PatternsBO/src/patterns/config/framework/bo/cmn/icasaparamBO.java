package patterns.config.framework.bo.cmn;

import java.util.Date;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.BindParameterType;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTDObject;
import patterns.config.framework.web.BackOfficeFormatUtils;

public class icasaparamBO extends GenericBO {
	TBADynaSQL casaparamhist = null;
	TBADynaSQL casaparamhistdtl = null;

	public icasaparamBO() {
	}

	@Override
	public void init() throws TBAFrameworkException {
		// TODO Auto-generated method stub
		casaparamhist = new TBADynaSQL("CASAPARAMHIST", true);
		casaparamhistdtl = new TBADynaSQL("CASAPARAMHISTDTL", false);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("PRODUCT_CODE") + RegularConstants.PK_SEPARATOR + processData.get("CCY_CODE") + RegularConstants.PK_SEPARATOR
				+ BackOfficeFormatUtils.getDate((Date) processData.getDate("EFF_DATE"), BackOfficeConstants.TBA_DATE_FORMAT);
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("PRODUCT_CODE"));
		casaparamhist.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		casaparamhist.setField("PRODUCT_CODE", processData.get("PRODUCT_CODE"));
		casaparamhist.setField("CCY_CODE", processData.get("CCY_CODE"));
		casaparamhist.setField("EFF_DATE", processData.getDate("EFF_DATE"));
		dataExists = casaparamhist.loadByKeyFields();
	}

	@Override
	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				casaparamhist.setField("CASH_CR_ALLOWED", processData.get("CASH_CR_ALLOWED"));
				casaparamhist.setField("CASH_CR_MIN_AMT", processData.get("CASH_CR_MIN_AMT"));
				casaparamhist.setField("CASH_CR_MAX_AMT", processData.get("CASH_CR_MAX_AMT"));
				casaparamhist.setField("TRF_CR_ALLOWED", processData.get("TRF_CR_ALLOWED"));
				casaparamhist.setField("TRF_CR_MIN_AMT", processData.get("TRF_CR_MIN_AMT"));
				casaparamhist.setField("TRF_CR_MAX_AMT", processData.get("TRF_CR_MAX_AMT"));
				casaparamhist.setField("CLG_CR_ALLOWED", processData.get("CLG_CR_ALLOWED"));
				casaparamhist.setField("CLG_CR_MIN_AMT", processData.get("CLG_CR_MIN_AMT"));
				casaparamhist.setField("CLG_CR_MAX_AMT", processData.get("CLG_CR_MAX_AMT"));
				casaparamhist.setField("CASH_DB_ALLOWED", processData.get("CASH_DB_ALLOWED"));
				casaparamhist.setField("CASH_DB_MIN_AMT", processData.get("CASH_DB_MIN_AMT"));
				casaparamhist.setField("CASH_DB_MAX_AMT", processData.get("CASH_DB_MAX_AMT"));
				casaparamhist.setField("TRF_DB_ALLOWED", processData.get("TRF_DB_ALLOWED"));
				casaparamhist.setField("TRF_DB_MIN_AMT", processData.get("TRF_DB_MIN_AMT"));
				casaparamhist.setField("TRF_DB_MAX_AMT", processData.get("TRF_DB_MAX_AMT"));
				casaparamhist.setField("CLG_DB_ALLOWED", processData.get("CLG_DB_ALLOWED"));
				casaparamhist.setField("CLG_DB_MIN_AMT", processData.get("CLG_DB_MIN_AMT"));
				casaparamhist.setField("CLG_DB_MAX_AMT", processData.get("CLG_DB_MAX_AMT"));
				casaparamhist.setField("MIN_BALANCE_REQD", processData.get("MIN_BALANCE_REQD"));
				casaparamhist.setField("MAX_BALANCE_ALLOWED", processData.get("MAX_BALANCE_ALLOWED"));
				casaparamhist.setField("BALANCE_CHECK_FREQ", processData.get("BALANCE_CHECK_FREQ"));
				casaparamhist.setField("LINK_EXT_BANK_REQD", processData.get("LINK_EXT_BANK_REQD"));
				casaparamhist.setField("MIN_AVG_BALANCE_REQD", processData.get("MIN_AVG_BALANCE_REQD"));
				casaparamhist.setField("MIN_AVG_BALANCE_AMT", processData.get("MIN_AVG_BALANCE_AMT"));
				casaparamhist.setField("AVG_BALANCE_PERIOD", processData.get("AVG_BALANCE_PERIOD"));
				casaparamhist.setField("BROKEN_PERIOD_TREATMENT", processData.get("BROKEN_PERIOD_TREATMENT"));
				casaparamhist.setField("ENABLED", processData.get("ENABLED"));
				casaparamhist.setField("REMARKS", processData.get("REMARKS"));
				casaparamhist.setNew(true);
				if (casaparamhist.save()) {
					casaparamhistdtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
					casaparamhistdtl.setField("PRODUCT_CODE", processData.get("PRODUCT_CODE"));
					casaparamhistdtl.setField("CCY_CODE", processData.get("CCY_CODE"));
					casaparamhistdtl.setField("EFF_DATE", processData.getDate("EFF_DATE"));
					DTDObject detailData = processData.getDTDObject("CASAPARAMHISTDTL");
					casaparamhistdtl.setNew(true);
					int count = 1;
					for (int i = 0; i < detailData.getRowCount(); ++i) {
						casaparamhistdtl.setField("DTL_SL", count);
						casaparamhistdtl.setField("TRAN_MODE", detailData.getValue(i, 1));
						casaparamhistdtl.setField("TRAN_TYPE", detailData.getValue(i, 3));
						casaparamhistdtl.setField("CEILING_AMT", detailData.getValue(i, 6));
						casaparamhistdtl.setField("PID_REQUIREMENT", detailData.getValue(i, 7));
						casaparamhistdtl.setField("POI_REQD", detailData.getValue(i, 9));
						casaparamhistdtl.setField("POA_REQD", detailData.getValue(i, 11));
						casaparamhistdtl.setField("PID_CODE", detailData.getValue(i, 13));
						if (!casaparamhistdtl.save()) {
							processResult.setProcessStatus(TBAProcessStatus.FAILURE);
							processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
							return;
						}
						count++;
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());

		}
	}

	@Override
	protected void modify() throws TBAFrameworkException {
		// TODO Auto-generated method stub
		try {
			if (isDataExists()) {
				casaparamhist.setField("CASH_CR_ALLOWED", processData.get("CASH_CR_ALLOWED"));
				casaparamhist.setField("CASH_CR_MIN_AMT", processData.get("CASH_CR_MIN_AMT"));
				casaparamhist.setField("CASH_CR_MAX_AMT", processData.get("CASH_CR_MAX_AMT"));
				casaparamhist.setField("TRF_CR_ALLOWED", processData.get("TRF_CR_ALLOWED"));
				casaparamhist.setField("TRF_CR_MIN_AMT", processData.get("TRF_CR_MIN_AMT"));
				casaparamhist.setField("TRF_CR_MAX_AMT", processData.get("TRF_CR_MAX_AMT"));
				casaparamhist.setField("CLG_CR_ALLOWED", processData.get("CLG_CR_ALLOWED"));
				casaparamhist.setField("CLG_CR_MIN_AMT", processData.get("CLG_CR_MIN_AMT"));
				casaparamhist.setField("CLG_CR_MAX_AMT", processData.get("CLG_CR_MAX_AMT"));
				casaparamhist.setField("CASH_DB_ALLOWED", processData.get("CASH_DB_ALLOWED"));
				casaparamhist.setField("CASH_DB_MIN_AMT", processData.get("CASH_DB_MIN_AMT"));
				casaparamhist.setField("CASH_DB_MAX_AMT", processData.get("CASH_DB_MAX_AMT"));
				casaparamhist.setField("TRF_DB_ALLOWED", processData.get("TRF_DB_ALLOWED"));
				casaparamhist.setField("TRF_DB_MIN_AMT", processData.get("TRF_DB_MIN_AMT"));
				casaparamhist.setField("TRF_DB_MAX_AMT", processData.get("TRF_DB_MAX_AMT"));
				casaparamhist.setField("CLG_DB_ALLOWED", processData.get("CLG_DB_ALLOWED"));
				casaparamhist.setField("CLG_DB_MIN_AMT", processData.get("CLG_DB_MIN_AMT"));
				casaparamhist.setField("CLG_DB_MAX_AMT", processData.get("CLG_DB_MAX_AMT"));
				casaparamhist.setField("MIN_BALANCE_REQD", processData.get("MIN_BALANCE_REQD"));
				casaparamhist.setField("MAX_BALANCE_ALLOWED", processData.get("MAX_BALANCE_ALLOWED"));
				casaparamhist.setField("BALANCE_CHECK_FREQ", processData.get("BALANCE_CHECK_FREQ"));
				casaparamhist.setField("LINK_EXT_BANK_REQD", processData.get("LINK_EXT_BANK_REQD"));
				casaparamhist.setField("MIN_AVG_BALANCE_REQD", processData.get("MIN_AVG_BALANCE_REQD"));
				casaparamhist.setField("MIN_AVG_BALANCE_AMT", processData.get("MIN_AVG_BALANCE_AMT"));
				casaparamhist.setField("AVG_BALANCE_PERIOD", processData.get("AVG_BALANCE_PERIOD"));
				casaparamhist.setField("BROKEN_PERIOD_TREATMENT", processData.get("BROKEN_PERIOD_TREATMENT"));
				casaparamhist.setField("ENABLED", processData.get("ENABLED"));
				casaparamhist.setField("REMARKS", processData.get("REMARKS"));
				casaparamhist.setNew(false);
				if (casaparamhist.save()) {
					String[] casaparamhistdtlFields = { "ENTITY_CODE", "PRODUCT_CODE", "CCY_CODE", "EFF_DATE", "DTL_SL" };
					BindParameterType[] casaparamhistdtlFieldTypes = { BindParameterType.BIGINT, BindParameterType.VARCHAR, BindParameterType.VARCHAR, BindParameterType.DATE, BindParameterType.INTEGER };
					casaparamhistdtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
					casaparamhistdtl.setField("PRODUCT_CODE", processData.get("PRODUCT_CODE"));
					casaparamhistdtl.setField("CCY_CODE", processData.get("CCY_CODE"));
					casaparamhistdtl.setField("EFF_DATE", processData.getDate("EFF_DATE"));
					casaparamhistdtl.deleteByFieldsAudit(casaparamhistdtlFields, casaparamhistdtlFieldTypes);
					DTDObject detailData = processData.getDTDObject("CASAPARAMHISTDTL");
					casaparamhistdtl.setNew(true);
					int count = 1;
					for (int i = 0; i < detailData.getRowCount(); ++i) {
						casaparamhistdtl.setField("DTL_SL", count);
						casaparamhistdtl.setField("TRAN_MODE", detailData.getValue(i, 1));
						casaparamhistdtl.setField("TRAN_TYPE", detailData.getValue(i, 3));
						casaparamhistdtl.setField("CEILING_AMT", detailData.getValue(i, 6));
						casaparamhistdtl.setField("PID_REQUIREMENT", detailData.getValue(i, 7));
						casaparamhistdtl.setField("POI_REQD", detailData.getValue(i, 9));
						casaparamhistdtl.setField("POA_REQD", detailData.getValue(i, 11));
						casaparamhistdtl.setField("PID_CODE", detailData.getValue(i, 13));
						if (!casaparamhistdtl.save()) {
							processResult.setProcessStatus(TBAProcessStatus.FAILURE);
							processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
							return;
						}
						count++;
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		}
	}
}
