package patterns.config.framework.bo.cmn;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.BindParameterType;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTDObject;
//import patterns.config.framework.web.ajax.ContentManager;
import patterns.config.framework.web.BackOfficeFormatUtils;

public class iactypeinsbkconfBO extends GenericBO {
	TBADynaSQL iactypeinsbkconf = null;
	TBADynaSQL iactypeinsbkconfdtl = null;

	public iactypeinsbkconfBO() {
	}

	public void init() {
		iactypeinsbkconf = new TBADynaSQL("ACTYPEINSBKCONFHIST", true);
		iactypeinsbkconfdtl = new TBADynaSQL("ACTYPEINSBKCONFHISTDTL", false);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("ACCOUNT_TYPE_CODE") + RegularConstants.PK_SEPARATOR + BackOfficeFormatUtils.getDate(processData.getDate("EFF_DATE"), BackOfficeConstants.TBA_DATE_FORMAT);

		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("ACCOUNT_TYPE_CODE"));

		iactypeinsbkconf.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		iactypeinsbkconf.setField("ACCOUNT_TYPE_CODE", processData.get("ACCOUNT_TYPE_CODE"));
		iactypeinsbkconf.setField("EFF_DATE", processData.getDate("EFF_DATE"));
		dataExists = iactypeinsbkconf.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				iactypeinsbkconf.setNew(true);
				iactypeinsbkconf.setField("ENABLED", processData.get("ENABLED"));
				iactypeinsbkconf.setField("REMARKS", processData.get("REMARKS"));
				if (iactypeinsbkconf.save()) {
					iactypeinsbkconfdtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
					iactypeinsbkconfdtl.setField("ACCOUNT_TYPE_CODE", processData.get("ACCOUNT_TYPE_CODE"));
					iactypeinsbkconfdtl.setField("EFF_DATE", processData.getDate("EFF_DATE"));
					DTDObject detailData = processData.getDTDObject("ACTYPEINSBKCONFHISTDTL");
					iactypeinsbkconfdtl.setNew(true);
					for (int i = 0; i < detailData.getRowCount(); ++i) {
						iactypeinsbkconfdtl.setField("SL", String.valueOf(i + 1));
						iactypeinsbkconfdtl.setField("INST_TYPE_CODE", detailData.getValue(i, 1));
						iactypeinsbkconfdtl.setField("BOOK_CODE", detailData.getValue(i, 3));
						if (!iactypeinsbkconfdtl.save()) {
							processResult.setProcessStatus(TBAProcessStatus.FAILURE);
							processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
							return;
						}
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
					
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			iactypeinsbkconf.close();
			iactypeinsbkconfdtl.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				iactypeinsbkconf.setNew(false);
				iactypeinsbkconf.setField("ENABLED", processData.get("ENABLED"));
				iactypeinsbkconf.setField("REMARKS", processData.get("REMARKS"));
				if (iactypeinsbkconf.save()) {
					String[] iactypeinsbkconfcontrolFields = { "ENTITY_CODE", "ACCOUNT_TYPE_CODE", "EFF_DATE", "SL" };
					BindParameterType[] iactypeinsbkconfcontrolFieldTypes = { BindParameterType.BIGINT, BindParameterType.INTEGER, BindParameterType.DATE, BindParameterType.INTEGER };
					iactypeinsbkconfdtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
					iactypeinsbkconfdtl.setField("ACCOUNT_TYPE_CODE", processData.get("ACCOUNT_TYPE_CODE"));
					iactypeinsbkconfdtl.setField("EFF_DATE", processData.getDate("EFF_DATE"));
					iactypeinsbkconfdtl.setField("SL", processData.get("SL"));
					iactypeinsbkconfdtl.deleteByFieldsAudit(iactypeinsbkconfcontrolFields, iactypeinsbkconfcontrolFieldTypes);
					iactypeinsbkconfdtl.setNew(true);
					DTDObject detailData = processData.getDTDObject("ACTYPEINSBKCONFHISTDTL");
					for (int i = 0; i < detailData.getRowCount(); ++i) {
						iactypeinsbkconfdtl.setField("SL", String.valueOf(i + 1));
						iactypeinsbkconfdtl.setField("INST_TYPE_CODE", detailData.getValue(i, 1));
						iactypeinsbkconfdtl.setField("BOOK_CODE", detailData.getValue(i, 3));
						if (!iactypeinsbkconfdtl.save()) {
							processResult.setProcessStatus(TBAProcessStatus.FAILURE);
							processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
							return;
						}
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			iactypeinsbkconf.close();
			iactypeinsbkconfdtl.close();
		}
	}
}
