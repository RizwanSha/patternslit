package patterns.config.framework.bo.cmn;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;
public class massetfieldsBO extends GenericBO {
	TBADynaSQL massetfields = null;
	public massetfieldsBO() {
	}
	public void init() {
		massetfields = new TBADynaSQL("ASSETFIELD", true);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("FIELD_ID");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("DESCRIPTION"));
		massetfields.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		massetfields.setField("FIELD_ID", processData.get("FIELD_ID"));
		dataExists = massetfields.loadByKeyFields();
	}
	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				massetfields.setNew(true);
				massetfields.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				massetfields.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				massetfields.setField("FIELD_FOR", processData.get("FIELD_FOR"));
				massetfields.setField("FIELD_VALUE_TYPE", processData.get("FIELD_VALUE_TYPE"));
				massetfields.setField("FIELD_SIZE", processData.get("FIELD_SIZE"));
				massetfields.setField("FIELD_DECIMALS", processData.get("FIELD_DECIMALS"));
				massetfields.setField("CUSTOMER_ID", processData.get("CUSTOMER_ID"));
				massetfields.setField("ASSET_TYPE_CODE", processData.get("ASSET_TYPE_CODE"));
				massetfields.setField("ENABLED", processData.get("ENABLED"));
				massetfields.setField("REMARKS", processData.get("REMARKS"));
				if (massetfields.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			massetfields.close();
		}
	}
	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				massetfields.setNew(false);
				massetfields.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				massetfields.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				massetfields.setField("FIELD_FOR", processData.get("FIELD_FOR"));
				massetfields.setField("FIELD_VALUE_TYPE", processData.get("FIELD_VALUE_TYPE"));
				massetfields.setField("FIELD_SIZE", processData.get("FIELD_SIZE"));
				massetfields.setField("FIELD_DECIMALS", processData.get("FIELD_DECIMALS"));
				massetfields.setField("CUSTOMER_ID", processData.get("CUSTOMER_ID"));
				massetfields.setField("ASSET_TYPE_CODE", processData.get("ASSET_TYPE_CODE"));
				massetfields.setField("ENABLED", processData.get("ENABLED"));
				massetfields.setField("REMARKS", processData.get("REMARKS"));
				if (massetfields.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			}else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			massetfields.close();
		}
	}
}