package patterns.config.framework.bo.cmn;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;

public class macstatuscodesBO extends GenericBO {

	TBADynaSQL macstatuscodes = null;

	public macstatuscodesBO() {
	}

	public void init() {
		macstatuscodes = new TBADynaSQL("ACNTSTATUS", true);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("ACNT_STATUS_CODE");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("ACNT_STATUS_CODE"));
		macstatuscodes.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		macstatuscodes.setField("ACNT_STATUS_CODE", processData.get("ACNT_STATUS_CODE"));
		dataExists = macstatuscodes.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				macstatuscodes.setNew(true);
				macstatuscodes.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				macstatuscodes.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				macstatuscodes.setField("PRE_OPENING_STATUS", processData.get("PRE_OPENING_STATUS"));
				macstatuscodes.setField("NORMAL_STATUS", processData.get("NORMAL_STATUS"));
				macstatuscodes.setField("OVERDUE_STATUS", processData.get("OVERDUE_STATUS"));
				macstatuscodes.setField("CLOSED_STATUS", processData.get("CLOSED_STATUS"));
				macstatuscodes.setField("INOPERATIVE_STATUS", processData.get("INOPERATIVE_STATUS"));
				macstatuscodes.setField("DORMANT_STATUS", processData.get("DORMANT_STATUS"));
				macstatuscodes.setField("TRAN_REG_AUTH_STATUS", processData.get("TRAN_REG_AUTH_STATUS"));
				macstatuscodes.setField("REG_AUTH_NAME", processData.get("REG_AUTH_NAME"));
				macstatuscodes.setField("NO_TRAN_STATUS", processData.get("NO_TRAN_STATUS"));
				macstatuscodes.setField("NO_TRAN_PERIOD", processData.get("NO_TRAN_PERIOD"));
				macstatuscodes.setField("BAL_TRANSFER_REQ_STATUS", processData.get("BAL_TRANSFER_REQ_STATUS"));
				macstatuscodes.setField("PAY_GL_HEAD", processData.get("PAY_GL_HEAD"));
				macstatuscodes.setField("GL_CHG_REQ_STATUS", processData.get("GL_CHG_REQ_STATUS"));
				macstatuscodes.setField("ENABLED", processData.get("ENABLED"));
				macstatuscodes.setField("REMARKS", processData.get("REMARKS"));
				if (macstatuscodes.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			macstatuscodes.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				macstatuscodes.setNew(false);
				macstatuscodes.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				macstatuscodes.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				macstatuscodes.setField("PRE_OPENING_STATUS", processData.get("PRE_OPENING_STATUS"));
				macstatuscodes.setField("NORMAL_STATUS", processData.get("NORMAL_STATUS"));
				macstatuscodes.setField("OVERDUE_STATUS", processData.get("OVERDUE_STATUS"));
				macstatuscodes.setField("CLOSED_STATUS", processData.get("CLOSED_STATUS"));
				macstatuscodes.setField("INOPERATIVE_STATUS", processData.get("INOPERATIVE_STATUS"));
				macstatuscodes.setField("DORMANT_STATUS", processData.get("DORMANT_STATUS"));
				macstatuscodes.setField("TRAN_REG_AUTH_STATUS", processData.get("TRAN_REG_AUTH_STATUS"));
				macstatuscodes.setField("REG_AUTH_NAME", processData.get("REG_AUTH_NAME"));
				macstatuscodes.setField("NO_TRAN_STATUS", processData.get("NO_TRAN_STATUS"));
				macstatuscodes.setField("NO_TRAN_PERIOD", processData.get("NO_TRAN_PERIOD"));
				macstatuscodes.setField("BAL_TRANSFER_REQ_STATUS", processData.get("BAL_TRANSFER_REQ_STATUS"));
				macstatuscodes.setField("PAY_GL_HEAD", processData.get("PAY_GL_HEAD"));
				macstatuscodes.setField("GL_CHG_REQ_STATUS", processData.get("GL_CHG_REQ_STATUS"));
				macstatuscodes.setField("ENABLED", processData.get("ENABLED"));
				macstatuscodes.setField("REMARKS", processData.get("REMARKS"));
				if (macstatuscodes.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			macstatuscodes.close();
		}
	}
}
