package patterns.config.framework.bo.cmn;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;

public class mtitlesBO extends GenericBO {

	TBADynaSQL mtitles = null;

	public mtitlesBO() {
	}

	public void init() {
		mtitles = new TBADynaSQL("TITLE", true);
		primaryKey = processData.get("ENTITY_CODE")+ RegularConstants.PK_SEPARATOR +processData.get("TITLE_CODE");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("EXT_DESCRIPTION"));
		mtitles.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		mtitles.setField("TITLE_CODE", processData.get("TITLE_CODE"));
		dataExists = mtitles.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				mtitles.setNew(true);
				mtitles.setField("EXT_DESCRIPTION", processData.get("EXT_DESCRIPTION"));
				mtitles.setField("INT_DESCRIPTION", processData.get("INT_DESCRIPTION"));
				mtitles.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				mtitles.setField("TITLE_USAGE", processData.get("TITLE_USAGE"));
				mtitles.setField("TITLE_CHILDREN_USG", processData.get("TITLE_CHILDREN_USG"));
				mtitles.setField("TITLE_DECEASED_USG", processData.get("TITLE_DECEASED_USG"));
				mtitles.setField("TITLE_OCCU", processData.get("TITLE_OCCU"));
				mtitles.setField("TITLE_OTH_OCCU", processData.get("TITLE_OTH_OCCU"));
				mtitles.setField("DEFAULT_MARITAL_STATUS", processData.get("DEFAULT_MARITAL_STATUS"));
				mtitles.setField("MARITAL_STATUS_FIXED", processData.get("MARITAL_STATUS_FIXED"));
				mtitles.setField("ENABLED", processData.get("ENABLED"));
				mtitles.setField("REMARKS", processData.get("REMARKS"));
				if (mtitles.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mtitles.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				mtitles.setNew(false);
				mtitles.setField("EXT_DESCRIPTION", processData.get("EXT_DESCRIPTION"));
				mtitles.setField("INT_DESCRIPTION", processData.get("INT_DESCRIPTION"));
				mtitles.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				mtitles.setField("TITLE_USAGE", processData.get("TITLE_USAGE"));
				mtitles.setField("TITLE_CHILDREN_USG", processData.get("TITLE_CHILDREN_USG"));
				mtitles.setField("TITLE_DECEASED_USG", processData.get("TITLE_DECEASED_USG"));
				mtitles.setField("TITLE_OCCU", processData.get("TITLE_OCCU"));
				mtitles.setField("TITLE_OTH_OCCU", processData.get("TITLE_OTH_OCCU"));
				mtitles.setField("DEFAULT_MARITAL_STATUS", processData.get("DEFAULT_MARITAL_STATUS"));
				mtitles.setField("MARITAL_STATUS_FIXED", processData.get("MARITAL_STATUS_FIXED"));
				mtitles.setField("ENABLED", processData.get("ENABLED"));
				mtitles.setField("REMARKS", processData.get("REMARKS"));
				if (mtitles.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mtitles.close();
		}
	}
}
