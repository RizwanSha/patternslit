package patterns.config.framework.bo.cmn;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;

public class mnarrcodesBO extends GenericBO {

	TBADynaSQL mnarrcodes = null;

	public mnarrcodesBO() {
	}

	public void init() {
		mnarrcodes = new TBADynaSQL("NARRCODES", true);
		primaryKey = processData.get("ENTITY_CODE")+ RegularConstants.PK_SEPARATOR +processData.get("NARR_CODE");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("DESCRIPTION"));
		mnarrcodes.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		mnarrcodes.setField("NARR_CODE", processData.get("NARR_CODE"));
		dataExists = mnarrcodes.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				mnarrcodes.setNew(true);
				mnarrcodes.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				mnarrcodes.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				mnarrcodes.setField("TRAN_MODE", processData.get("TRAN_MODE"));
				mnarrcodes.setField("ENABLED", processData.get("ENABLED"));
				mnarrcodes.setField("REMARKS", processData.get("REMARKS"));
				if (mnarrcodes.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mnarrcodes.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				mnarrcodes.setNew(false);
				mnarrcodes.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				mnarrcodes.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				mnarrcodes.setField("TRAN_MODE", processData.get("TRAN_MODE"));
				mnarrcodes.setField("ENABLED", processData.get("ENABLED"));
				mnarrcodes.setField("REMARKS", processData.get("REMARKS"));
				if (mnarrcodes.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mnarrcodes.close();
		}
	}
}