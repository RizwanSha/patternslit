package patterns.config.framework.bo.cmn;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;

/*
 * The program will be used to create and maintain currency codes. Currency codes will be used in financial entries and displays across the application. 
 *
 Author : Pavan kumar.R
 Created Date : 11-Nov-2014
 Spec Reference HMS-PSD-CMN-0034-mcurrency
 Modification History
 ----------------------------------------------------------------------
 Sl.No		Modified Date	Author			Modified Changes	Version
 ----------------------------------------------------------------------

 */

public class mcurrencyBO extends GenericBO {

	TBADynaSQL mcurrency = null;

	public mcurrencyBO() {
	}

	public void init() {
		mcurrency = new TBADynaSQL("CURRENCY", true);
		primaryKey = processData.get("CCY_CODE");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("DESCRIPTION"));
		mcurrency.setField("CCY_CODE", processData.get("CCY_CODE"));
		dataExists = mcurrency.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				mcurrency.setNew(true);
				mcurrency.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				mcurrency.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				mcurrency.setField("CCY_NOTATION", processData.get("CCY_NOTATION"));

				mcurrency.setField("HAS_SUB_CCY", processData.get("HAS_SUB_CCY"));
				mcurrency.setField("SUB_CCY_DESCRIPTION", processData.get("SUB_CCY_DESCRIPTION"));
				mcurrency.setField("SUB_CCY_CONCISE_DESCRIPTION", processData.get("SUB_CCY_CONCISE_DESCRIPTION"));

				mcurrency.setField("SUB_CCY_NOTATION", processData.get("SUB_CCY_NOTATION"));
				mcurrency.setField("SUB_CCY_UNITS_IN_CCY", processData.get("SUB_CCY_UNITS_IN_CCY"));
				mcurrency.setField("ISO_CCY_CODE", processData.get("ISO_CCY_CODE"));

				mcurrency.setField("ENABLED", processData.get("ENABLED"));
				mcurrency.setField("REMARKS", processData.get("REMARKS"));
				if (mcurrency.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
				}

			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mcurrency.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				mcurrency.setNew(false);
				mcurrency.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				mcurrency.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				mcurrency.setField("CCY_NOTATION", processData.get("CCY_NOTATION"));
				mcurrency.setField("HAS_SUB_CCY", processData.get("HAS_SUB_CCY"));
				mcurrency.setField("SUB_CCY_DESCRIPTION", processData.get("SUB_CCY_DESCRIPTION"));
				mcurrency.setField("SUB_CCY_CONCISE_DESCRIPTION", processData.get("SUB_CCY_CONCISE_DESCRIPTION"));
				mcurrency.setField("SUB_CCY_NOTATION", processData.get("SUB_CCY_NOTATION"));
				mcurrency.setField("SUB_CCY_UNITS_IN_CCY", processData.get("SUB_CCY_UNITS_IN_CCY"));
				mcurrency.setField("ISO_CCY_CODE", processData.get("ISO_CCY_CODE"));
				mcurrency.setField("ENABLED", processData.get("ENABLED"));
				mcurrency.setField("REMARKS", processData.get("REMARKS"));
				if (mcurrency.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mcurrency.close();
		}
	}
}
