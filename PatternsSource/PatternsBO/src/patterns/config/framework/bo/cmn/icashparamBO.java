/*
 *
 Author :A PRASHANTH
 Created Date : 01-JULY-2016
 Spec Reference : PPBS/CMN/0016-ICASHPARAM
 Modification History
 ----------------------------------------------------------------------
 Sl.No		Modified Date	Author			Modified Changes	Version
 ----------------------------------------------------------------------
 
 */

package patterns.config.framework.bo.cmn;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;

public class icashparamBO extends GenericBO {

	TBADynaSQL icashparam = null;

	public icashparamBO() {
	}

	public void init() {
										
		icashparam = new TBADynaSQL("CASHMODPARAM", true);
		primaryKey = processData.get("ENTITY_CODE");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("DESCRIPTION"));
		icashparam.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		dataExists = icashparam.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				icashparam.setNew(true);
				icashparam.setField("OFFICE_GL_HEAD", processData.get("OFFICE_GL_HEAD"));
				icashparam.setField("HEAD_CASHIER_GL_HEAD", processData.get("HEAD_CASHIER_GL_HEAD"));
				icashparam.setField("COUNTER_CASH_GL_HEAD", processData.get("COUNTER_CASH_GL_HEAD"));
				icashparam.setField("FIELD_STAFF_CASH_GL_HEAD", processData.get("FIELD_STAFF_CASH_GL_HEAD"));
				icashparam.setField("BANK_CORR_CASH_GL_HEAD", processData.get("BANK_CORR_CASH_GL_HEAD"));
				icashparam.setField("INTER_OFF_CASH_TRF_GL_HEAD", processData.get("INTER_OFF_CASH_TRF_GL_HEAD"));
				icashparam.setField("SAME_OFFICE_CASH_TRF_GL", processData.get("SAME_OFFICE_CASH_TRF_GL"));
				icashparam.setField("DENOMINATION_REQ", processData.get("DENOMINATION_REQ"));
				icashparam.setField("VAULT_WITHDRAWAL_FTRAN_CODE", processData.get("VAULT_WITHDRAWAL_FTRAN_CODE"));
				icashparam.setField("VAULT_DEPOSIT_FTRAN_CODE", processData.get("VAULT_DEPOSIT_FTRAN_CODE"));
				icashparam.setField("CTC_FTRAN_CODE", processData.get("CTC_FTRAN_CODE"));
				icashparam.setField("REMARKS", processData.get("REMARKS"));
				if (icashparam.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				icashparam.setNew(false);
				icashparam.setField("OFFICE_GL_HEAD", processData.get("OFFICE_GL_HEAD"));
				icashparam.setField("HEAD_CASHIER_GL_HEAD", processData.get("HEAD_CASHIER_GL_HEAD"));
				icashparam.setField("COUNTER_CASH_GL_HEAD", processData.get("COUNTER_CASH_GL_HEAD"));
				icashparam.setField("FIELD_STAFF_CASH_GL_HEAD", processData.get("FIELD_STAFF_CASH_GL_HEAD"));
				icashparam.setField("BANK_CORR_CASH_GL_HEAD", processData.get("BANK_CORR_CASH_GL_HEAD"));
				icashparam.setField("INTER_OFF_CASH_TRF_GL_HEAD", processData.get("INTER_OFF_CASH_TRF_GL_HEAD"));
				icashparam.setField("SAME_OFFICE_CASH_TRF_GL", processData.get("SAME_OFFICE_CASH_TRF_GL"));
				icashparam.setField("DENOMINATION_REQ", processData.get("DENOMINATION_REQ"));
				icashparam.setField("VAULT_WITHDRAWAL_FTRAN_CODE", processData.get("VAULT_WITHDRAWAL_FTRAN_CODE"));
				icashparam.setField("VAULT_DEPOSIT_FTRAN_CODE", processData.get("VAULT_DEPOSIT_FTRAN_CODE"));
				icashparam.setField("CTC_FTRAN_CODE", processData.get("CTC_FTRAN_CODE"));
				icashparam.setField("REMARKS", processData.get("REMARKS"));
				if (icashparam.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			icashparam.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
	}
}
