package patterns.config.framework.bo.cmn;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;

public class mpidtypeBO extends GenericBO {

	TBADynaSQL mpidtype = null;

	public mpidtypeBO() {
	}

	public void init() {
		mpidtype = new TBADynaSQL("PIDCODES", true);
		primaryKey = processData.get("PID_CODE");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("DESCRIPTION"));
		mpidtype.setField("PID_CODE", processData.get("PID_CODE"));
		dataExists = mpidtype.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				mpidtype.setNew(true);
				mpidtype.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				mpidtype.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				mpidtype.setField("INCOME_TAX_PAYER_ID", processData.get("INCOME_TAX_PAYER_ID"));
				mpidtype.setField("NATIONAL_ID", processData.get("NATIONAL_ID"));
				mpidtype.setField("PASSPORT_ID", processData.get("PASSPORT_ID"));
				mpidtype.setField("DOC_USED_FOR_ID_PROOF", processData.get("DOC_USED_FOR_ID_PROOF"));
				mpidtype.setField("ID_PROOF_OFFICIALLY_VALID_DOC", processData.get("ID_PROOF_OFFICIALLY_VALID_DOC"));
				mpidtype.setField("ID_PROOF_SIMPLIFIED_MEASURE_KYC", processData.get("ID_PROOF_SIMPLIFIED_MEASURE_KYC"));
				mpidtype.setField("DOC_USED_FOR_ADDR_PROOF", processData.get("DOC_USED_FOR_ADDR_PROOF"));
				mpidtype.setField("ADDR_PROOF_OFFICIALLY_VALID_DOC", processData.get("ADDR_PROOF_OFFICIALLY_VALID_DOC"));
				mpidtype.setField("ADDR_PROOF_SIMPLIFIED_MEASURE_KYC", processData.get("ADDR_PROOF_SIMPLIFIED_MEASURE_KYC"));
				mpidtype.setField("DOC_APPL_FOR_COUNTRY", processData.get("DOC_APPL_FOR_COUNTRY"));
				mpidtype.setField("DOC_ISSUER_NAME", processData.get("DOC_ISSUER_NAME"));
				mpidtype.setField("DOC_HAS_ISSUE_DATE", processData.get("DOC_HAS_ISSUE_DATE"));
				mpidtype.setField("DOC_HAS_EXPIRY_DATE", processData.get("DOC_HAS_EXPIRY_DATE"));
				mpidtype.setField("ALT_ACCOUNT_ID", processData.get("ALT_ACCOUNT_ID"));
				mpidtype.setField("PID_FOR_BPL_CARD", processData.get("PID_FOR_BPL_CARD"));
				mpidtype.setField("ENABLED", processData.get("ENABLED"));
				mpidtype.setField("REMARKS", processData.getObject("REMARKS"));
				if (mpidtype.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mpidtype.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				mpidtype.setNew(false);
				mpidtype.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				mpidtype.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				mpidtype.setField("INCOME_TAX_PAYER_ID", processData.get("INCOME_TAX_PAYER_ID"));
				mpidtype.setField("NATIONAL_ID", processData.get("NATIONAL_ID"));
				mpidtype.setField("PASSPORT_ID", processData.get("PASSPORT_ID"));
				mpidtype.setField("DOC_USED_FOR_ID_PROOF", processData.get("DOC_USED_FOR_ID_PROOF"));
				mpidtype.setField("ID_PROOF_OFFICIALLY_VALID_DOC", processData.get("ID_PROOF_OFFICIALLY_VALID_DOC"));
				mpidtype.setField("ID_PROOF_SIMPLIFIED_MEASURE_KYC", processData.get("ID_PROOF_SIMPLIFIED_MEASURE_KYC"));
				mpidtype.setField("DOC_USED_FOR_ADDR_PROOF", processData.get("DOC_USED_FOR_ADDR_PROOF"));
				mpidtype.setField("ADDR_PROOF_OFFICIALLY_VALID_DOC", processData.get("ADDR_PROOF_OFFICIALLY_VALID_DOC"));
				mpidtype.setField("ADDR_PROOF_SIMPLIFIED_MEASURE_KYC", processData.get("ADDR_PROOF_SIMPLIFIED_MEASURE_KYC"));
				mpidtype.setField("DOC_APPL_FOR_COUNTRY", processData.get("DOC_APPL_FOR_COUNTRY"));
				mpidtype.setField("DOC_ISSUER_NAME", processData.get("DOC_ISSUER_NAME"));
				mpidtype.setField("DOC_HAS_ISSUE_DATE", processData.get("DOC_HAS_ISSUE_DATE"));
				mpidtype.setField("DOC_HAS_EXPIRY_DATE", processData.get("DOC_HAS_EXPIRY_DATE"));
				mpidtype.setField("ALT_ACCOUNT_ID", processData.get("ALT_ACCOUNT_ID"));
				mpidtype.setField("PID_FOR_BPL_CARD", processData.get("PID_FOR_BPL_CARD"));
				mpidtype.setField("ENABLED", processData.get("ENABLED"));
				mpidtype.setField("REMARKS", processData.getObject("REMARKS"));
				if (mpidtype.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mpidtype.close();
		}
	}
}
