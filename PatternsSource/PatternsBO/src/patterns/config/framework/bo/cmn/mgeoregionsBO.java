package patterns.config.framework.bo.cmn;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.web.ajax.ContentManager;

public class mgeoregionsBO extends GenericBO {

	TBADynaSQL mgeoregions = null;
	public mgeoregionsBO() {
	}

	public void init() {
		mgeoregions = new TBADynaSQL("GEOREGIONS", true);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("GEO_REGION_CODE");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("DESCRIPTION"));
		mgeoregions.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		mgeoregions.setField("GEO_REGION_CODE", processData.get("GEO_REGION_CODE"));
		dataExists = mgeoregions.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				mgeoregions.setNew(true);
				mgeoregions.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				mgeoregions.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				mgeoregions.setField("PINCODE_GEOUNITID", processData.get("PINCODE_GEOUNITID"));
				mgeoregions.setField("OFFICE_CODE", processData.get("OFFICE_CODE"));
				mgeoregions.setField("MAP_INV_NO", processData.get("MAP_INV_NO"));
				mgeoregions.setField("ENABLED", processData.get("ENABLED"));
				mgeoregions.setField("REMARKS", processData.get("REMARKS"));
				if (mgeoregions.save()) {
					mgeoregions.setField(ContentManager.PURPOSE_ID, "BRANCH");
					mgeoregions.setField(ContentManager.DEDUP_KEY, processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("PINCODE_GEOUNITID") + RegularConstants.PK_SEPARATOR + processData.get("OFFICE_CODE") + RegularConstants.PK_SEPARATOR + processData.get("ENABLED"));
					if (!mgeoregions.udateTBADuplicateCheck()) {
						processResult.setProcessStatus(TBAProcessStatus.FAILURE);
						processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
						throw new TBAFrameworkException();
					}
					if (processData.get("MAP_INV_NO") != RegularConstants.EMPTY_STRING) {
						
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mgeoregions.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				mgeoregions.setNew(false);
				mgeoregions.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				mgeoregions.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				mgeoregions.setField("PINCODE_GEOUNITID", processData.get("PINCODE_GEOUNITID"));
				mgeoregions.setField("OFFICE_CODE", processData.get("OFFICE_CODE"));
				mgeoregions.setField("ENABLED", processData.get("ENABLED"));
				mgeoregions.setField("MAP_INV_NO", processData.get("MAP_INV_NO"));
				mgeoregions.setField("REMARKS", processData.get("REMARKS"));
				if (mgeoregions.save()) {
					mgeoregions.setField(ContentManager.PURPOSE_ID, "BRANCH");
					mgeoregions.setField(ContentManager.DEDUP_KEY, processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("PINCODE_GEOUNITID") + RegularConstants.PK_SEPARATOR + processData.get("OFFICE_CODE") + RegularConstants.PK_SEPARATOR + processData.get("ENABLED"));
					if (!mgeoregions.udateTBADuplicateCheck()) {
						processResult.setProcessStatus(TBAProcessStatus.FAILURE);
						processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
						throw new TBAFrameworkException();
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mgeoregions.close();
		}
	}
	
}