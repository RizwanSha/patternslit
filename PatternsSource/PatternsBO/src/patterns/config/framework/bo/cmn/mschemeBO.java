package patterns.config.framework.bo.cmn;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;

public class mschemeBO extends GenericBO {
	TBADynaSQL mscheme = null;

	public mschemeBO() {
	}

	public void init() {
		mscheme = new TBADynaSQL("SCHEMES", true);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("SCHEME_CODE");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("SCHEME_CODE"));
		mscheme.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		mscheme.setField("SCHEME_CODE", processData.get("SCHEME_CODE"));
		dataExists = mscheme.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				mscheme.setNew(true);
				mscheme.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				mscheme.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				mscheme.setField("SCHEME_FOR", processData.get("SCHEME_FOR"));
				mscheme.setField("PARENT_SCH_CODE", processData.get("PARENT_SCH_CODE"));
				mscheme.setField("PID_CODE", processData.get("PID_CODE"));
				mscheme.setField("SCHEME_HAS_ENROL", processData.get("SCHEME_HAS_ENROL"));
				mscheme.setField("ENROL_DESCRIPTION", processData.get("ENROL_DESCRIPTION"));
				mscheme.setField("PAY_CONTROL_TOKEN_REF", processData.get("PAY_CONTROL_TOKEN_REF"));
				mscheme.setField("TOKEN_REF_DESCRIPTION", processData.get("TOKEN_REF_DESCRIPTION"));
				mscheme.setField("ENABLED", processData.get("ENABLED"));
				mscheme.setField("REMARKS", processData.get("REMARKS"));
				if (mscheme.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);

				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mscheme.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				mscheme.setNew(false);
				mscheme.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				mscheme.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				mscheme.setField("SCHEME_FOR", processData.get("SCHEME_FOR"));
				mscheme.setField("PARENT_SCH_CODE", processData.get("PARENT_SCH_CODE"));
				mscheme.setField("PID_CODE", processData.get("PID_CODE"));
				mscheme.setField("SCHEME_HAS_ENROL", processData.get("SCHEME_HAS_ENROL"));
				mscheme.setField("ENROL_DESCRIPTION", processData.get("ENROL_DESCRIPTION"));
				mscheme.setField("PAY_CONTROL_TOKEN_REF", processData.get("PAY_CONTROL_TOKEN_REF"));
				mscheme.setField("TOKEN_REF_DESCRIPTION", processData.get("TOKEN_REF_DESCRIPTION"));
				mscheme.setField("ENABLED", processData.get("ENABLED"));
				mscheme.setField("REMARKS", processData.get("REMARKS"));
				if (mscheme.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}

		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mscheme.close();
		}
	}

}
