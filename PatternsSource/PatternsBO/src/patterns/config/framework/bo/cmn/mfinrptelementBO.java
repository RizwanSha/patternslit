package patterns.config.framework.bo.cmn;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
/*
 * The program will be used to create and maintain  Report Element Code. Financial Report Elements Maintenance. 
 *
 Author : Pavan kumar.R
 Created Date : 29-Nov-2016
 Spec Reference PSD-CMN-00022
 Modification History
 ----------------------------------------------------------------------
 Sl.No		Modified Date	Author			Modified Changes	Version
 ----------------------------------------------------------------------

 */
import patterns.config.framework.process.TBAProcessStatus;

public class mfinrptelementBO extends GenericBO {
	TBADynaSQL mfinrptelement = null;

	public void init() {
		mfinrptelement = new TBADynaSQL("FINRPTELEMENT", true);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("REPORT_ELEMENT_CODE");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("REPORT_ELEMENT_CODE"));
		mfinrptelement.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		mfinrptelement.setField("REPORT_ELEMENT_CODE", processData.get("REPORT_ELEMENT_CODE"));
		dataExists = mfinrptelement.loadByKeyFields();

	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				mfinrptelement.setNew(true);
				mfinrptelement.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				mfinrptelement.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				mfinrptelement.setField("PARENT_REPORT_ELEMENT_CODE", processData.get("PARENT_REPORT_ELEMENT_CODE"));
				mfinrptelement.setField("LEAF_LEVEL_ELEMENT", processData.get("LEAF_LEVEL_ELEMENT"));
				mfinrptelement.setField("ACTNG_TYPES_ALLOWED", processData.get("ACTNG_TYPES_ALLOWED"));
				mfinrptelement.setField("ELEMENT_BELONGS_TO", processData.get("ELEMENT_BELONGS_TO"));
				mfinrptelement.setField("ENABLED", processData.get("ENABLED"));
				mfinrptelement.setField("REMARKS", processData.get("REMARKS"));
				if (mfinrptelement.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mfinrptelement.close();
		}
	}

	protected void modify() throws TBAFrameworkException {

		try {
			if (isDataExists()) {
				mfinrptelement.setNew(false);
				mfinrptelement.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				mfinrptelement.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				mfinrptelement.setField("PARENT_REPORT_ELEMENT_CODE", processData.get("PARENT_REPORT_ELEMENT_CODE"));
				mfinrptelement.setField("LEAF_LEVEL_ELEMENT", processData.get("LEAF_LEVEL_ELEMENT"));
				mfinrptelement.setField("ACTNG_TYPES_ALLOWED", processData.get("ACTNG_TYPES_ALLOWED"));
				mfinrptelement.setField("ELEMENT_BELONGS_TO", processData.get("ELEMENT_BELONGS_TO"));
				mfinrptelement.setField("ENABLED", processData.get("ENABLED"));
				mfinrptelement.setField("REMARKS", processData.get("REMARKS"));
				if (mfinrptelement.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mfinrptelement.close();
		}
	}

}
