package patterns.config.framework.bo.cmn;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;

/**
 * 
 * 
 * @version 1.0
 * @author P Vishnu
 * @since 28-Jan-2017 <br>
 *        <b>Modified History</b> <BR>
 *        <U><B> Sl.No Modified Date Author Modified Changes Version </B></U> <br>
 * 
 * 
 */

public class mglheadBO extends GenericBO {

	TBADynaSQL mglhead = null;

	public mglheadBO() {
	}

	public void init() {
		mglhead = new TBADynaSQL("GLHEAD", true);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("GL_HEAD_CODE");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("DESCRIPTION"));
		mglhead.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		mglhead.setField("GL_HEAD_CODE", processData.get("GL_HEAD_CODE"));
		dataExists = mglhead.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				mglhead.setNew(true);
				mglhead.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				mglhead.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				mglhead.setField("LEASE_PRODUCT_CODE", processData.get("LEASE_PRODUCT_CODE"));
				mglhead.setField("ENABLED", processData.get("ENABLED"));
				mglhead.setField("REMARKS", processData.get("REMARKS"));
				if (mglhead.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mglhead.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				mglhead.setNew(false);
				mglhead.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				mglhead.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				mglhead.setField("LEASE_PRODUCT_CODE", processData.get("LEASE_PRODUCT_CODE"));
				mglhead.setField("ENABLED", processData.get("ENABLED"));
				mglhead.setField("REMARKS", processData.get("REMARKS"));
				if (mglhead.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mglhead.close();
		}
	}
}
