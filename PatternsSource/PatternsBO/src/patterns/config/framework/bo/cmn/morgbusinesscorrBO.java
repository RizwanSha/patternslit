package patterns.config.framework.bo.cmn;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.BindParameterType;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTDObject;
import patterns.config.framework.web.BackOfficeFormatUtils;

/**
 * 
 * 
 * @version 1.0
 * @author Dileep Kumar Reddy T
 * @since 30-Jun-2016 <br>
 *        Third Party Organizations as Business Correspondents <b>Modified
 *        History</b> <BR>
 *        <U><B> Sl.No Modified Date Author Modified Changes Version </B></U> <br>
 * 
 * 
 */

public class morgbusinesscorrBO extends GenericBO {
	TBADynaSQL orgbusinesscorrhist = null;
	TBADynaSQL orgbusinesscorrhistdtl = null;

	public void init() {
		orgbusinesscorrhist = new TBADynaSQL("ORGBUSINESSCORRHIST", true);
		orgbusinesscorrhistdtl = new TBADynaSQL("ORGBUSINESSCORRHISTDTL", false);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("ORG_CODE") + RegularConstants.PK_SEPARATOR + BackOfficeFormatUtils.getDate(processData.getDate("EFF_DATE"), BackOfficeConstants.TBA_DATE_FORMAT);
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("ORG_CODE"));
		orgbusinesscorrhist.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		orgbusinesscorrhist.setField("ORG_CODE", processData.get("ORG_CODE"));
		orgbusinesscorrhist.setField("EFF_DATE", processData.getDate("EFF_DATE"));
		dataExists = orgbusinesscorrhist.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				orgbusinesscorrhist.setField("ENABLEDASCORRDENT", processData.get("ENABLEDASCORRDENT"));
				orgbusinesscorrhist.setField("ENABLED", processData.get("ENABLED"));
				orgbusinesscorrhist.setField("REMARKS", processData.get("REMARKS"));
				orgbusinesscorrhist.setNew(true);
				if (orgbusinesscorrhist.save()) {
					orgbusinesscorrhistdtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
					orgbusinesscorrhistdtl.setField("ORG_CODE", processData.get("ORG_CODE"));
					orgbusinesscorrhistdtl.setField("EFF_DATE", processData.getDate("EFF_DATE"));
					DTDObject detailData = processData.getDTDObject("ORGBUSINESSCORRHISTDTL");
					orgbusinesscorrhistdtl.setNew(true);
					for (int i = 0; i < detailData.getRowCount(); ++i) {
						orgbusinesscorrhistdtl.setField("SL", Integer.valueOf(i + 1).toString());
						orgbusinesscorrhistdtl.setField("COUNTRY_CODE", detailData.getValue(i, 1));
						orgbusinesscorrhistdtl.setField("GEO_UNIT_STATE_ID", detailData.getValue(i, 2));
						orgbusinesscorrhistdtl.setField("GEO_UNIT_DISTRICT_ID", detailData.getValue(i, 4));
						if (!orgbusinesscorrhistdtl.save()) {
							processResult.setProcessStatus(TBAProcessStatus.FAILURE);
							processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
							return;
						}
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			orgbusinesscorrhist.close();
			orgbusinesscorrhistdtl.close();
		}
	}

	protected void modify() throws TBAFrameworkException {

		try {
			if (isDataExists()) {
				orgbusinesscorrhist.setField("ENABLEDASCORRDENT", processData.get("ENABLEDASCORRDENT"));
				orgbusinesscorrhist.setField("ENABLED", processData.get("ENABLED"));
				orgbusinesscorrhist.setField("REMARKS", processData.get("REMARKS"));
				orgbusinesscorrhist.setNew(false);
				if (orgbusinesscorrhist.save()) {
					String[] orgbusinesscorrhistdtlFields = { "ENTITY_CODE", "ORG_CODE", "EFF_DATE", "SL" };
					BindParameterType[] orgbusinesscorrhistdtlTypes = { BindParameterType.VARCHAR, BindParameterType.VARCHAR, BindParameterType.DATE, BindParameterType.BIGINT };
					orgbusinesscorrhistdtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
					orgbusinesscorrhistdtl.setField("ORG_CODE", processData.get("ORG_CODE"));
					orgbusinesscorrhistdtl.setField("EFF_DATE", processData.getDate("EFF_DATE"));
					orgbusinesscorrhistdtl.deleteByFieldsAudit(orgbusinesscorrhistdtlFields, orgbusinesscorrhistdtlTypes);
					DTDObject detailData = processData.getDTDObject("ORGBUSINESSCORRHISTDTL");
					orgbusinesscorrhistdtl.setNew(true);
					for (int i = 0; i < detailData.getRowCount(); ++i) {
						orgbusinesscorrhistdtl.setField("SL", Integer.valueOf(i + 1).toString());
						orgbusinesscorrhistdtl.setField("COUNTRY_CODE", detailData.getValue(i, 1));
						orgbusinesscorrhistdtl.setField("GEO_UNIT_STATE_ID", detailData.getValue(i, 2));
						orgbusinesscorrhistdtl.setField("GEO_UNIT_DISTRICT_ID", detailData.getValue(i, 4));
						if (!orgbusinesscorrhistdtl.save()) {
							processResult.setProcessStatus(TBAProcessStatus.FAILURE);
							processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
							return;
						}
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}

		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			orgbusinesscorrhist.close();
			orgbusinesscorrhistdtl.close();
		}
	}
}
