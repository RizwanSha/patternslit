package patterns.config.framework.bo.cmn;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;

public class mkyccategoryBO extends GenericBO {
	TBADynaSQL mkyccategory = null;
	public mkyccategoryBO() {
		
	}

	public void init() {		
		mkyccategory = new TBADynaSQL("KYCCATEGORY", true);
		primaryKey = processData.get("ENTITY_CODE")+RegularConstants.PK_SEPARATOR + processData.get("KYC_CATEGORY");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("DESCRIPTION"));
		mkyccategory.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		mkyccategory.setField("KYC_CATEGORY", processData.get("KYC_CATEGORY"));
		dataExists = mkyccategory.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				mkyccategory.setNew(true);
				mkyccategory.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				mkyccategory.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				mkyccategory.setField("CATEGORY_FOR", processData.get("CATEGORY_FOR"));
				mkyccategory.setField("POI_REQD", processData.get("POI_REQD"));
				mkyccategory.setField("POA_REQD", processData.get("POA_REQD"));
				mkyccategory.setField("ENABLED", processData.get("ENABLED"));
				mkyccategory.setField("REMARKS", processData.get("REMARKS"));
				
				if (mkyccategory.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mkyccategory.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				mkyccategory.setNew(false);
				mkyccategory.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				mkyccategory.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				mkyccategory.setField("CATEGORY_FOR", processData.get("CATEGORY_FOR"));
				mkyccategory.setField("POI_REQD", processData.get("POI_REQD"));
				mkyccategory.setField("POA_REQD", processData.get("POA_REQD"));
				mkyccategory.setField("ENABLED", processData.get("ENABLED"));
				mkyccategory.setField("REMARKS", processData.get("REMARKS"));
				if (mkyccategory.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mkyccategory.close();
		}
	}
}
