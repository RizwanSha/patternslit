package patterns.config.framework.bo.cmn;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.web.BackOfficeFormatUtils;


public class itaxbyusleaseBO extends GenericBO {

	TBADynaSQL itaxbyusproducthist = null;

	public itaxbyusleaseBO() {
	}

	public void init() {
		itaxbyusproducthist = new TBADynaSQL("TAXBYUSLEASEHIST", true);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("LESSEE_CODE") + RegularConstants.PK_SEPARATOR + processData.get("AGREEMENT_NO") + RegularConstants.PK_SEPARATOR + processData.get("SCHEDULE_ID") + RegularConstants.PK_SEPARATOR + processData.get("STATE_CODE") + RegularConstants.PK_SEPARATOR + processData.get("TAX_CODE") + RegularConstants.PK_SEPARATOR +BackOfficeFormatUtils.getDate(processData.getDate("EFF_DATE"), BackOfficeConstants.TBA_DATE_FORMAT) ;
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("LESSEE_CODE"));
		itaxbyusproducthist.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		itaxbyusproducthist.setField("LESSEE_CODE", processData.get("LESSEE_CODE"));
		itaxbyusproducthist.setField("AGREEMENT_NO", processData.get("AGREEMENT_NO"));
		itaxbyusproducthist.setField("SCHEDULE_ID", processData.get("SCHEDULE_ID"));
		itaxbyusproducthist.setField("STATE_CODE", processData.get("STATE_CODE"));
		itaxbyusproducthist.setField("TAX_CODE", processData.get("TAX_CODE"));
		itaxbyusproducthist.setField("EFF_DATE", processData.getDate("EFF_DATE"));
		dataExists = itaxbyusproducthist.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				itaxbyusproducthist.setNew(true);
				itaxbyusproducthist.setField("TAX_BY_US", processData.get("TAX_BY_US"));
				itaxbyusproducthist.setField("TAX_BYUS_GL_HEAD_CODE", processData.get("TAX_BYUS_GL_HEAD_CODE"));
				itaxbyusproducthist.setField("ENABLED", processData.get("ENABLED"));
				itaxbyusproducthist.setField("REMARKS", processData.get("REMARKS"));
				if (itaxbyusproducthist.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
					itaxbyusproducthist.sendInterfaceOutwardMessage();
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}

			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			itaxbyusproducthist.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				itaxbyusproducthist.setNew(false);
				itaxbyusproducthist.setField("TAX_BY_US", processData.get("TAX_BY_US"));
				itaxbyusproducthist.setField("TAX_BYUS_GL_HEAD_CODE", processData.get("TAX_BYUS_GL_HEAD_CODE"));
				itaxbyusproducthist.setField("ENABLED", processData.get("ENABLED"));
				itaxbyusproducthist.setField("REMARKS", processData.get("REMARKS"));
				if (itaxbyusproducthist.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			itaxbyusproducthist.close();
		}
	}
}
