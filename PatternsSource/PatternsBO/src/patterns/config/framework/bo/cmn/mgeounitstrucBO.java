package patterns.config.framework.bo.cmn;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
/**
 *The program will be used to create and maintain various geounitstructure codes 
	*
	* 
	* @version 1.0
	* @author  Prashanth
	* @since   3-Dec-2014
	* <br>
	* <b>Modified History</b>
	* <BR>
	* <U><B>
	* Sl.No		Modified Date	Author			Modified Changes		Version
	* </B></U>
	* <br>
	* 
	* 
	* 
	* 
	* 
	* 
	* 
	*/
public class mgeounitstrucBO extends GenericBO {
	
	TBADynaSQL mgeounitstruc = null;

	public mgeounitstrucBO() {
	}

	public void init() {
		mgeounitstruc = new TBADynaSQL("GEOUNITSTRUC", true);
		primaryKey = processData.get("GEO_UNIT_STRUC_CODE");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("DESCRIPTION"));
		mgeounitstruc.setField("GEO_UNIT_STRUC_CODE", processData.get("GEO_UNIT_STRUC_CODE"));
		dataExists = mgeounitstruc.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				mgeounitstruc.setNew(true);
				mgeounitstruc.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				mgeounitstruc.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				mgeounitstruc.setField("PARENT_GUS_CODE", processData.get("PARENT_GUS_CODE"));
				mgeounitstruc.setField("LAST_IN_HIERARCHY", processData.get("LAST_IN_HIERARCHY"));
				mgeounitstruc.setField("UNIT_TYPE_REQ", processData.get("UNIT_TYPE_REQ"));
				mgeounitstruc.setField("DATA_PATTERN_REQ", processData.get("DATA_PATTERN_REQ"));
				mgeounitstruc.setField("DATA_PATTERN", processData.get("DATA_PATTERN"));
				mgeounitstruc.setField("ENABLED", processData.get("ENABLED"));
				mgeounitstruc.setField("REMARKS", processData.get("REMARKS"));
				if (mgeounitstruc.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
				}

			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mgeounitstruc.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				mgeounitstruc.setNew(false);
				mgeounitstruc.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				mgeounitstruc.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				mgeounitstruc.setField("PARENT_GUS_CODE", processData.get("PARENT_GUS_CODE"));
				mgeounitstruc.setField("LAST_IN_HIERARCHY", processData.get("LAST_IN_HIERARCHY"));
				mgeounitstruc.setField("UNIT_TYPE_REQ", processData.get("UNIT_TYPE_REQ"));
				mgeounitstruc.setField("DATA_PATTERN_REQ", processData.get("DATA_PATTERN_REQ"));
				mgeounitstruc.setField("DATA_PATTERN", processData.get("DATA_PATTERN"));
				mgeounitstruc.setField("ENABLED", processData.get("ENABLED"));
				mgeounitstruc.setField("REMARKS", processData.get("REMARKS"));
				if (mgeounitstruc.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mgeounitstruc.close();
		}
	}
}