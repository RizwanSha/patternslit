package patterns.config.framework.bo.cmn;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;

public class mproductclassBO extends GenericBO {

	TBADynaSQL mproductclass = null;

	public mproductclassBO() {
	}

	public void init() {
		mproductclass = new TBADynaSQL("PRODUCTCLASS", true);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("PRODUCT_CLASS_CODE");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("PRODUCT_CLASS_CODE"));
		mproductclass.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		mproductclass.setField("PRODUCT_CLASS_CODE", processData.get("PRODUCT_CLASS_CODE"));
		dataExists = mproductclass.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				mproductclass.setNew(true);
				mproductclass.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				mproductclass.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				mproductclass.setField("PREMIUM_PRODUCT_CLASS_FLAG", processData.get("PREMIUM_PRODUCT_CLASS_FLAG"));
				mproductclass.setField("NO_FRILLS_PRODUCT_CLASS_FLAG", processData.get("NO_FRILLS_PRODUCT_CLASS_FLAG"));
				mproductclass.setField("ENABLED", processData.get("ENABLED"));
				mproductclass.setField("REMARKS", processData.get("REMARKS"));
				if (mproductclass.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mproductclass.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				mproductclass.setNew(false);
				mproductclass.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				mproductclass.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				mproductclass.setField("PREMIUM_PRODUCT_CLASS_FLAG", processData.get("PREMIUM_PRODUCT_CLASS_FLAG"));
				mproductclass.setField("NO_FRILLS_PRODUCT_CLASS_FLAG", processData.get("NO_FRILLS_PRODUCT_CLASS_FLAG"));
				mproductclass.setField("ENABLED", processData.get("ENABLED"));
				mproductclass.setField("REMARKS", processData.get("REMARKS"));
				if (mproductclass.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mproductclass.close();
		}
	}
}
