/*
 *The program used for Product Availability Control Maintenance

 Author : ARULPRASATH A
 Created Date : 01-Dec-2016
 Spec Reference : PPBS/CMN-IPRODAVLCONTROL 
 Modification History
 ----------------------------------------------------------------------
 Sl.No		Modified Date	Author			Modified Changes	Version
 ----------------------------------------------------------------------

 */

package patterns.config.framework.bo.cmn;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.BindParameterType;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTDObject;
import patterns.config.framework.web.BackOfficeFormatUtils;

public class iprodavlcontrolBO extends GenericBO {

	TBADynaSQL iprodavlcontrol = null;
	TBADynaSQL iprodavlcseghistdtl = null;
	TBADynaSQL iprodavlcorghistdtl = null;
	TBADynaSQL iprodavlcgtyphistdtl = null;

	public iprodavlcontrolBO() {
	}

	public void init() {
		iprodavlcontrol = new TBADynaSQL("PRODAVLCNTRLHISTM", true);
		iprodavlcseghistdtl = new TBADynaSQL("PRODAVLCSEGHISTDTL", false);
		iprodavlcorghistdtl = new TBADynaSQL("PRODAVLCORGHISTDTL", false);
		iprodavlcgtyphistdtl = new TBADynaSQL("PRODAVLCGTYPHISTDTL", false);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("PRODUCT_CODE") + RegularConstants.PK_SEPARATOR + BackOfficeFormatUtils.getDate(processData.getDate("EFF_DATE"), BackOfficeConstants.TBA_DATE_FORMAT);
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("PRODUCT_CODE"));
		iprodavlcontrol.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		iprodavlcontrol.setField("PRODUCT_CODE", processData.get("PRODUCT_CODE"));
		iprodavlcontrol.setField("EFF_DATE", processData.getDate("EFF_DATE"));
		dataExists = iprodavlcontrol.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				iprodavlcontrol.setNew(true);
				iprodavlcontrol.setField("AVL_FOR_INDIVIDUALS", processData.get("AVL_FOR_INDIVIDUALS"));
				iprodavlcontrol.setField("AVL_FOR_ORGANIZATIONS", processData.get("AVL_FOR_ORGANIZATIONS"));
				iprodavlcontrol.setField("CUST_SEG_CNTRL_FLAG", processData.get("CUST_SEG_CNTRL_FLAG"));
				iprodavlcontrol.setField("CUST_ORG_CNTRL_FLAG", processData.get("CUST_ORG_CNTRL_FLAG"));
				iprodavlcontrol.setField("CUST_GROUP_TYPE_CNTRL_FLAG", processData.get("CUST_GROUP_TYPE_CNTRL_FLAG"));
				iprodavlcontrol.setField("ENABLED", processData.get("ENABLED"));
				iprodavlcontrol.setField("REMARKS", processData.get("REMARKS"));
				if (iprodavlcontrol.save()) {
					iprodavlcseghistdtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
					iprodavlcseghistdtl.setField("PRODUCT_CODE", processData.get("PRODUCT_CODE"));
					iprodavlcseghistdtl.setField("EFF_DATE", processData.getDate("EFF_DATE"));
					DTDObject detailData = processData.getDTDObject("PRODAVLCSEGHISTDTL");
					iprodavlcseghistdtl.setNew(true);
					for (int i = 0; i < detailData.getRowCount(); ++i) {
						iprodavlcseghistdtl.setField("SL", String.valueOf(i + 1));
						iprodavlcseghistdtl.setField("CUST_SEGMENT", detailData.getValue(i, 1));
						if (!iprodavlcseghistdtl.save()) {
							processResult.setProcessStatus(TBAProcessStatus.FAILURE);
							processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
							return;
						}
					}
					iprodavlcorghistdtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
					iprodavlcorghistdtl.setField("PRODUCT_CODE", processData.get("PRODUCT_CODE"));
					iprodavlcorghistdtl.setField("EFF_DATE", processData.getDate("EFF_DATE"));
					detailData.clear();
					detailData = processData.getDTDObject("PRODAVLCORGHISTDTL");
					iprodavlcorghistdtl.setNew(true);
					for (int i = 0; i < detailData.getRowCount(); ++i) {
						iprodavlcorghistdtl.setField("SL", String.valueOf(i + 1));
						iprodavlcorghistdtl.setField("ORGNIZATION_CODE", detailData.getValue(i, 1));
						if (!iprodavlcorghistdtl.save()) {
							processResult.setProcessStatus(TBAProcessStatus.FAILURE);
							processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
							return;
						}
					}
					iprodavlcgtyphistdtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
					iprodavlcgtyphistdtl.setField("PRODUCT_CODE", processData.get("PRODUCT_CODE"));
					iprodavlcgtyphistdtl.setField("EFF_DATE", processData.getDate("EFF_DATE"));
					detailData.clear();
					detailData = processData.getDTDObject("PRODAVLCGTYPHISTDTL");
					iprodavlcgtyphistdtl.setNew(true);
					for (int i = 0; i < detailData.getRowCount(); ++i) {
						iprodavlcgtyphistdtl.setField("SL", String.valueOf(i + 1));
						iprodavlcgtyphistdtl.setField("CUST_GROUP_TYPE", detailData.getValue(i, 1));
						if (!iprodavlcgtyphistdtl.save()) {
							processResult.setProcessStatus(TBAProcessStatus.FAILURE);
							processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
							return;
						}
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			iprodavlcontrol.close();
			iprodavlcseghistdtl.close();
			iprodavlcorghistdtl.close();
			iprodavlcgtyphistdtl.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				iprodavlcontrol.setNew(false);
				iprodavlcontrol.setField("AVL_FOR_INDIVIDUALS", processData.get("AVL_FOR_INDIVIDUALS"));
				iprodavlcontrol.setField("AVL_FOR_ORGANIZATIONS", processData.get("AVL_FOR_ORGANIZATIONS"));
				iprodavlcontrol.setField("CUST_SEG_CNTRL_FLAG", processData.get("CUST_SEG_CNTRL_FLAG"));
				iprodavlcontrol.setField("CUST_ORG_CNTRL_FLAG", processData.get("CUST_ORG_CNTRL_FLAG"));
				iprodavlcontrol.setField("CUST_GROUP_TYPE_CNTRL_FLAG", processData.get("CUST_GROUP_TYPE_CNTRL_FLAG"));
				iprodavlcontrol.setField("ENABLED", processData.get("ENABLED"));
				iprodavlcontrol.setField("REMARKS", processData.get("REMARKS"));
				if (iprodavlcontrol.save()) {
					String[] iprodavlcontrolFields = { "ENTITY_CODE", "PRODUCT_CODE", "EFF_DATE", "SL" };
					BindParameterType[] iprodavlcontrolFieldTypes = { BindParameterType.BIGINT, BindParameterType.INTEGER, BindParameterType.DATE, BindParameterType.INTEGER };
					iprodavlcseghistdtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
					iprodavlcseghistdtl.setField("PRODUCT_CODE", processData.get("PRODUCT_CODE"));
					iprodavlcseghistdtl.setField("EFF_DATE", processData.getDate("EFF_DATE"));
					iprodavlcseghistdtl.setField("SL", processData.get("SL"));
					iprodavlcseghistdtl.deleteByFieldsAudit(iprodavlcontrolFields, iprodavlcontrolFieldTypes);
					iprodavlcseghistdtl.setNew(true);
					DTDObject detailData = processData.getDTDObject("PRODAVLCSEGHISTDTL");
					for (int i = 0; i < detailData.getRowCount(); ++i) {
						iprodavlcseghistdtl.setField("SL", String.valueOf(i + 1));
						iprodavlcseghistdtl.setField("CUST_SEGMENT", detailData.getValue(i, 1));
						if (!iprodavlcseghistdtl.save()) {
							processResult.setProcessStatus(TBAProcessStatus.FAILURE);
							processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
							return;
						}
					}
					String[] iprodavlcontrolFields1 = { "ENTITY_CODE", "PRODUCT_CODE", "EFF_DATE", "SL" };
					BindParameterType[] iprodavlcontrolFieldTypes1 = { BindParameterType.BIGINT, BindParameterType.INTEGER, BindParameterType.DATE, BindParameterType.INTEGER };
					iprodavlcorghistdtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
					iprodavlcorghistdtl.setField("PRODUCT_CODE", processData.get("PRODUCT_CODE"));
					iprodavlcorghistdtl.setField("EFF_DATE", processData.getDate("EFF_DATE"));
					iprodavlcorghistdtl.setField("SL", processData.get("SL"));
					iprodavlcorghistdtl.deleteByFieldsAudit(iprodavlcontrolFields1, iprodavlcontrolFieldTypes1);
					iprodavlcorghistdtl.setNew(true);
					detailData = processData.getDTDObject("PRODAVLCORGHISTDTL");
					for (int i = 0; i < detailData.getRowCount(); ++i) {
						iprodavlcorghistdtl.setField("SL", String.valueOf(i + 1));
						iprodavlcorghistdtl.setField("ORGNIZATION_CODE", detailData.getValue(i, 1));
						if (!iprodavlcorghistdtl.save()) {
							processResult.setProcessStatus(TBAProcessStatus.FAILURE);
							processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
							return;
						}
					}
					String[] iprodavlcontrolFields2 = { "ENTITY_CODE", "PRODUCT_CODE", "EFF_DATE", "SL" };
					BindParameterType[] iprodavlcontrolFieldTypes2 = { BindParameterType.BIGINT, BindParameterType.INTEGER, BindParameterType.DATE, BindParameterType.INTEGER };
					iprodavlcgtyphistdtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
					iprodavlcgtyphistdtl.setField("PRODUCT_CODE", processData.get("PRODUCT_CODE"));
					iprodavlcgtyphistdtl.setField("EFF_DATE", processData.getDate("EFF_DATE"));
					iprodavlcgtyphistdtl.setField("SL", processData.get("SL"));
					iprodavlcgtyphistdtl.deleteByFieldsAudit(iprodavlcontrolFields2, iprodavlcontrolFieldTypes2);
					iprodavlcgtyphistdtl.setNew(true);
					detailData = processData.getDTDObject("PRODAVLCGTYPHISTDTL");
					for (int i = 0; i < detailData.getRowCount(); ++i) {
						iprodavlcgtyphistdtl.setField("SL", String.valueOf(i + 1));
						iprodavlcgtyphistdtl.setField("CUST_GROUP_TYPE", detailData.getValue(i, 1));
						if (!iprodavlcgtyphistdtl.save()) {
							processResult.setProcessStatus(TBAProcessStatus.FAILURE);
							processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
							return;
						}
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			iprodavlcontrol.close();
			iprodavlcseghistdtl.close();
			iprodavlcorghistdtl.close();
			iprodavlcgtyphistdtl.close();
		}
	}
}
