package patterns.config.framework.bo.cmn;

/**
 * This program will be used to enter PID Registration for a Person
 * 
 * @version 1.0
 * @author Jayashree
 * @since 04-Dec-2014 <br>
 *  <b>Modified History</b> <BR>
 *  *      @author RAJA
 * 		@since 08-JULY-2016 <br>
 *       
 *        
 *        <U><B> Sl.No Modified Date Author Modified Changes Version </B></U> <br>
 */
import java.util.ArrayList;
import java.util.List;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.bo.MessageParam;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.web.ajax.ContentManager;

public class epidregBO extends GenericBO {

	TBADynaSQL epidreg = null;
	int pidRegSl = 0;

	public epidregBO() {
	}

	public void init() {
		epidreg = new TBADynaSQL("CIFIDENTIFICATIONDOC", true);
		if (processData.containsKey("DOC_SL")) {
			pidRegSl = Integer.parseInt(processData.get("DOC_SL"));
		} else {
			pidRegSl = (int) epidreg.getTBAReferenceKey(processData);
		}
		primaryKey = processData.get("CIF_NO") + RegularConstants.PK_SEPARATOR + String.valueOf(pidRegSl);
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("CIF_NO"));
		epidreg.setField("CIF_NO", processData.get("CIF_NO"));
		epidreg.setField("DOC_SL", String.valueOf(pidRegSl));
		dataExists = epidreg.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				epidreg.setNew(true);
				epidreg.setField("PID_CODE", processData.get("PID_CODE"));
				epidreg.setField("DATE_OF_REG", processData.getDate("DATE_OF_REG"));
				epidreg.setField("DOCUMENT_NUMBER", processData.get("DOCUMENT_NUMBER"));
				epidreg.setField("DOCUMENT_ISSUE_DATE", processData.getDate("DOCUMENT_ISSUE_DATE"));
				epidreg.setField("DOCUMENT_EXPIRY_DATE", processData.getDate("DOCUMENT_EXPIRY_DATE"));
				epidreg.setField("REMARKS", processData.get("REMARKS"));
				epidreg.setField("DATE_OF_DE_REG", processData.getDate("DATE_OF_DE_REG"));
				epidreg.setField("REASON_FOR_DE_REG", processData.get("REASON_FOR_DE_REG"));
				epidreg.setField("PID_DE_REG", processData.get("PID_DE_REG"));
				if (epidreg.save()) {
					epidreg.setField(ContentManager.PURPOSE_ID, "PID_NUMBER");
					epidreg.setField(ContentManager.DEDUP_KEY, processData.get("PID_CODE") + RegularConstants.PK_SEPARATOR + processData.get("PID_NUMBER"));
					if (!epidreg.udateTBADuplicateCheck()) {
						processResult.setProcessStatus(TBAProcessStatus.FAILURE);
						processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
						throw new TBAFrameworkException();
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
					MessageParam mesageParam = new MessageParam();
					mesageParam.setCode(BackOfficeErrorCodes.PBS_PID_GEN_SL);
					List<String> regList = new ArrayList<String>();
					regList.add(String.valueOf(pidRegSl));
					regList.add(processData.get("CIF_NO"));
					mesageParam.setParameters(regList);
					processResult.setAdditionalInfo(mesageParam);

				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			epidreg.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				epidreg.setNew(false);
				epidreg.setField("PID_DE_REG", processData.get("PID_DE_REG"));
				if(processData.get("PID_DE_REG").equals("0")){
					epidreg.setField("PID_CODE", processData.get("PID_CODE"));
					epidreg.setField("DATE_OF_REG", processData.getDate("DATE_OF_REG"));
					epidreg.setField("DOCUMENT_NUMBER", processData.get("DOCUMENT_NUMBER"));
					epidreg.setField("DOCUMENT_ISSUE_DATE", processData.getDate("DOCUMENT_ISSUE_DATE"));
					epidreg.setField("DOCUMENT_EXPIRY_DATE", processData.getDate("DOCUMENT_EXPIRY_DATE"));
					epidreg.setField("REMARKS", processData.get("REMARKS"));
				}else{
					epidreg.setField("DATE_OF_DE_REG", processData.getDate("DATE_OF_DE_REG"));
					epidreg.setField("REASON_FOR_DE_REG", processData.get("REASON_FOR_DE_REG"));
				}	
				
				if (epidreg.save()) {
					epidreg.setField(ContentManager.PURPOSE_ID, "PID_NUMBER");
					epidreg.setField(ContentManager.DEDUP_KEY, processData.get("PID_CODE") + RegularConstants.PK_SEPARATOR + processData.get("DOCUMENT_NUMBER"));
					if (!epidreg.udateTBADuplicateCheck()) {
						processResult.setProcessStatus(TBAProcessStatus.FAILURE);
						processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
						throw new TBAFrameworkException();
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			epidreg.close();
		}
	}
}
