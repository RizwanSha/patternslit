/*
 *
 Author : Raja E
 Created Date : 28-01-2017
 Spec Reference : MASSETTYPE
 <b>Modified History</b> ARULPRASATH <BR>
   <U><B> Sl.No Modified Date Author Modified Changes Version </B></U> <br>
 ----------------------------------------------------------------------
 Sl.No		Modified Date	Author			Modified Changes	Version
 ----------------------------------------------------------------------

 */

package patterns.config.framework.bo.cmn;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;

public class massettypeBO extends GenericBO {
	TBADynaSQL massettype = null;

	public massettypeBO() {
	}

	public void init() {
		massettype = new TBADynaSQL("ASSETTYPE", true);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("ASSET_TYPE_CODE");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("ASSET_TYPE_CODE"));
		massettype.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		massettype.setField("ASSET_TYPE_CODE", processData.get("ASSET_TYPE_CODE"));
		dataExists = massettype.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				massettype.setNew(true);
				massettype.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				massettype.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				massettype.setField("AUTO_NON_AUTO", processData.get("AUTO_NON_AUTO"));
				massettype.setField("MODEL_NO", processData.get("MODEL_NO"));
				massettype.setField("CONFIG_DTLS", processData.get("CONFIG_DTLS"));
				massettype.setField("ENABLED", processData.get("ENABLED"));
				massettype.setField("REMARKS", processData.get("REMARKS"));
				if (massettype.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			massettype.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				massettype.setNew(false);
				massettype.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				massettype.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				massettype.setField("AUTO_NON_AUTO", processData.get("AUTO_NON_AUTO"));
				massettype.setField("MODEL_NO", processData.get("MODEL_NO"));
				massettype.setField("CONFIG_DTLS", processData.get("CONFIG_DTLS"));
				massettype.setField("ENABLED", processData.get("ENABLED"));
				massettype.setField("REMARKS", processData.get("REMARKS"));
				if (massettype.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			massettype.close();
		}
	}
}
