package patterns.config.framework.bo.cmn;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;

public class minsinvnbookcdBO extends GenericBO {
	TBADynaSQL minsinvnbookcd = null;
	public minsinvnbookcdBO() {
	}
	
	@Override
	public void init() throws TBAFrameworkException {
		minsinvnbookcd = new TBADynaSQL("INSBOOKCD", true);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("INST_TYPE_CODE")+ RegularConstants.PK_SEPARATOR + processData.get("BOOK_CODE");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("DESCRIPTION"));
		minsinvnbookcd.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		minsinvnbookcd.setField("INST_TYPE_CODE", processData.get("INST_TYPE_CODE"));
		minsinvnbookcd.setField("BOOK_CODE", processData.get("BOOK_CODE"));
		dataExists = minsinvnbookcd.loadByKeyFields();
	}

	@Override
	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				minsinvnbookcd.setNew(true);
				minsinvnbookcd.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				minsinvnbookcd.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				minsinvnbookcd.setField("NOF_LEAVES", processData.getObject("NOF_LEAVES"));
				minsinvnbookcd.setField("ENABLED", processData.get("ENABLED"));
				minsinvnbookcd.setField("REMARKS", processData.get("REMARKS"));
				if (minsinvnbookcd.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			minsinvnbookcd.close();
		}
	}

	@Override
	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				minsinvnbookcd.setNew(false);
				minsinvnbookcd.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				minsinvnbookcd.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				minsinvnbookcd.setField("NOF_LEAVES", processData.getObject("NOF_LEAVES"));
				minsinvnbookcd.setField("ENABLED", processData.get("ENABLED"));
				minsinvnbookcd.setField("REMARKS", processData.get("REMARKS"));
				if (minsinvnbookcd.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			minsinvnbookcd.close();
		}
	}
}