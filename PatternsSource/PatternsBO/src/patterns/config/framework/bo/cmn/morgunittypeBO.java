package patterns.config.framework.bo.cmn;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;

public class morgunittypeBO extends GenericBO {
	TBADynaSQL morgunittype = null;

	@Override
	public void init() throws TBAFrameworkException {
		morgunittype = new TBADynaSQL("ORGANIZAITIONUNITTYPE", true);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("ORG_UNIT_TYPE");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("DESCRIPTION"));
		morgunittype.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		morgunittype.setField("ORG_UNIT_TYPE", processData.get("ORG_UNIT_TYPE"));
		dataExists = morgunittype.loadByKeyFields();
	}

	@Override
	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				morgunittype.setNew(true);
				morgunittype.setField("ORG_UNIT_TYPE", processData.get("ORG_UNIT_TYPE"));
				morgunittype.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				morgunittype.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				morgunittype.setField("PARENT_ORG_UNIT_TYPE", processData.get("PARENT_ORG_UNIT_TYPE"));
				morgunittype.setField("FO_OPERATING_UNIT", processData.get("FO_OPERATING_UNIT"));
				morgunittype.setField("SELF_BANKING_UNIT", processData.get("SELF_BANKING_UNIT"));
				morgunittype.setField("BO_OPERATING_UNIT", processData.get("BO_OPERATING_UNIT"));
				morgunittype.setField("IT_CELL", processData.get("IT_CELL"));
				morgunittype.setField("ENABLED", processData.get("ENABLED"));
				morgunittype.setField("REMARKS", processData.get("REMARKS"));
				if (morgunittype.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			morgunittype.close();
		}
	}

	@Override
	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				morgunittype.setNew(false);
				morgunittype.setField("ORG_UNIT_TYPE", processData.get("ORG_UNIT_TYPE"));
				morgunittype.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				morgunittype.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				morgunittype.setField("PARENT_ORG_UNIT_TYPE", processData.get("PARENT_ORG_UNIT_TYPE"));
				morgunittype.setField("FO_OPERATING_UNIT", processData.get("FO_OPERATING_UNIT"));
				morgunittype.setField("SELF_BANKING_UNIT", processData.get("SELF_BANKING_UNIT"));
				morgunittype.setField("BO_OPERATING_UNIT", processData.get("BO_OPERATING_UNIT"));
				morgunittype.setField("IT_CELL", processData.get("IT_CELL"));
				morgunittype.setField("ENABLED", processData.get("ENABLED"));
				morgunittype.setField("REMARKS", processData.get("REMARKS"));
				if (morgunittype.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			morgunittype.close();
		}
	}
}