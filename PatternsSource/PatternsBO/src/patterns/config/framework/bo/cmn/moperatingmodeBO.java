package patterns.config.framework.bo.cmn;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;

public class moperatingmodeBO extends GenericBO {
	TBADynaSQL moperatingmode = null;

	public moperatingmodeBO() {
	}

	public void init() {
		moperatingmode = new TBADynaSQL("OPERATINGMODES", true);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("OPERATING_MODE");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("OPERATING_MODE"));
		moperatingmode.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		moperatingmode.setField("OPERATING_MODE", processData.get("OPERATING_MODE"));
		dataExists = moperatingmode.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				moperatingmode.setNew(true);
				moperatingmode.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				moperatingmode.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				moperatingmode.setField("FOR_SINGLE_ACC", processData.get("FOR_SINGLE_ACC"));
				moperatingmode.setField("FOR_JOINT_ACC", processData.get("FOR_JOINT_ACC"));
				moperatingmode.setField("FOR_ILLITERATE_ACC", processData.get("FOR_ILLITERATE_ACC"));
				moperatingmode.setField("ENABLED", processData.get("ENABLED"));
				moperatingmode.setField("REMARKS", processData.get("REMARKS"));
				if (moperatingmode.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);

				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			moperatingmode.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				moperatingmode.setNew(false);
				moperatingmode.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				moperatingmode.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				moperatingmode.setField("FOR_SINGLE_ACC", processData.get("FOR_SINGLE_ACC"));
				moperatingmode.setField("FOR_JOINT_ACC", processData.get("FOR_JOINT_ACC"));
				moperatingmode.setField("FOR_ILLITERATE_ACC", processData.get("FOR_ILLITERATE_ACC"));
				moperatingmode.setField("ENABLED", processData.get("ENABLED"));
				moperatingmode.setField("REMARKS", processData.get("REMARKS"));
				if (moperatingmode.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}

		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			moperatingmode.close();
		}
	}

}
