/*
 *
 Author : NARESHKUMAR D
 Created Date : 01-JULY-2016
 Spec Reference : PPBS/CMN/0015-MENTITYBRANCH - Entity Branch code Master Maintenance (1-Master, Abstraction Program)
 Modification History
 ----------------------------------------------------------------------
 Sl.No		Modified Date	Author			Modified Changes	Version
 ----------------------------------------------------------------------
 
 */

package patterns.config.framework.bo.cmn;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;

public class mentitybranchBO extends GenericBO {

	TBADynaSQL mentitybranch = null;

	public mentitybranchBO() {
	}

	public void init() {
		mentitybranch = new TBADynaSQL("ENTITYBRN", true);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("BRANCH_CODE");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("DESCRIPTION"));
		mentitybranch.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		mentitybranch.setField("BRANCH_CODE", processData.get("BRANCH_CODE"));
		dataExists = mentitybranch.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				mentitybranch.setNew(true);
				mentitybranch.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				mentitybranch.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				mentitybranch.setField("OPEN_ON_DATE", processData.getDate("OPEN_ON_DATE"));
				mentitybranch.setField("BRANCH_DETAILS", processData.get("BRANCH_DETAILS"));
				mentitybranch.setField("GEO_UNIT_STATE_CODE", processData.get("GEO_UNIT_STATE_CODE"));
				mentitybranch.setField("TYPE_OF_ORG_UNIT", processData.get("TYPE_OF_ORG_UNIT"));
				mentitybranch.setField("CONTROLLING_OFFICE_CODE", processData.get("CONTROLLING_OFFICE_CODE"));
				mentitybranch.setField("CLUSTER_CODE", processData.get("CLUSTER_CODE"));
				mentitybranch.setField("CLOSED_ON_DATE", processData.getDate("CLOSED_ON_DATE"));
				mentitybranch.setField("ENABLED", processData.get("ENABLED"));
				mentitybranch.setField("REMARKS", processData.get("REMARKS"));
				if (mentitybranch.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mentitybranch.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				mentitybranch.setNew(false);
				mentitybranch.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				mentitybranch.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				mentitybranch.setField("OPEN_ON_DATE", processData.getDate("OPEN_ON_DATE"));
				mentitybranch.setField("BRANCH_DETAILS", processData.get("BRANCH_DETAILS"));
				mentitybranch.setField("GEO_UNIT_STATE_CODE", processData.get("GEO_UNIT_STATE_CODE"));
				mentitybranch.setField("TYPE_OF_ORG_UNIT", processData.get("TYPE_OF_ORG_UNIT"));
				mentitybranch.setField("CONTROLLING_OFFICE_CODE", processData.get("CONTROLLING_OFFICE_CODE"));
				mentitybranch.setField("CLUSTER_CODE", processData.get("CLUSTER_CODE"));
				mentitybranch.setField("CLOSED_ON_DATE", processData.getDate("CLOSED_ON_DATE"));
				mentitybranch.setField("ENABLED", processData.get("ENABLED"));
				mentitybranch.setField("REMARKS", processData.get("REMARKS"));
				if (mentitybranch.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mentitybranch.close();
		}
	}
}
