package patterns.config.framework.bo.cmn;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.BindParameterType;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTDObject;

public class mindassetBO extends GenericBO {
	TBADynaSQL mindasset = null;
	TBADynaSQL mindassetdtl = null;

	public mindassetBO() {
	}

	@Override
	public void init() throws TBAFrameworkException {
		// TODO Auto-generated method stub
		mindasset = new TBADynaSQL("LEASEASSETDETAIL", true);
		mindassetdtl = new TBADynaSQL("LEASEASSETFIELD", false);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("LESSEE_CODE") + RegularConstants.PK_SEPARATOR + processData.get("AGREEMENT_NO") + RegularConstants.PK_SEPARATOR + processData.get("SCHEDULE_ID") + RegularConstants.PK_SEPARATOR+ processData.get("ASSET_SL") + RegularConstants.PK_SEPARATOR + processData.get("ASSET_COMP_SL") + RegularConstants.PK_SEPARATOR + processData.get("ASSET_QTY_SL");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("LESSEE_CODE"));
		mindasset.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		mindasset.setField("LESSEE_CODE", processData.get("LESSEE_CODE"));
		mindasset.setField("AGREEMENT_NO", processData.get("AGREEMENT_NO"));
		mindasset.setField("SCHEDULE_ID", processData.get("SCHEDULE_ID"));
		mindasset.setField("ASSET_SL", processData.get("ASSET_SL"));
		mindasset.setField("ASSET_COMP_SL", processData.get("ASSET_COMP_SL"));
		mindasset.setField("ASSET_QTY_SL", processData.get("ASSET_QTY_SL"));
		dataExists = mindasset.loadByKeyFields();
	}

	@Override
	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				mindasset.setField("INDIVIDUAL_ASSET_ID", processData.get("INDIVIDUAL_ASSET_ID"));
				mindasset.setField("ENABLED", processData.get("ENABLED"));
				mindasset.setField("REMARKS", processData.get("REMARKS"));
				mindasset.setNew(true);
				if (mindasset.save()) {
					mindassetdtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
					mindassetdtl.setField("LESSEE_CODE", processData.get("LESSEE_CODE"));
					mindassetdtl.setField("AGREEMENT_NO", processData.get("AGREEMENT_NO"));
					mindassetdtl.setField("SCHEDULE_ID", processData.get("SCHEDULE_ID"));
					mindassetdtl.setField("ASSET_SL", processData.get("ASSET_SL"));
					mindassetdtl.setField("ASSET_COMP_SL", processData.get("ASSET_COMP_SL"));
					mindassetdtl.setField("ASSET_QTY_SL", processData.get("ASSET_QTY_SL"));
					DTDObject detailData = processData.getDTDObject("MINDASSETDTL");
					mindassetdtl.setNew(true);
					int count = 1;
					for (int i = 0; i < detailData.getRowCount(); ++i) {
						mindassetdtl.setField("FIELD_SL", count);
						mindassetdtl.setField("FIELD_ID", detailData.getValue(i, 1));
						mindassetdtl.setField("FIELD_VALUE_TYPE", detailData.getValue(i, 4));
						if (detailData.getValue(i, 4).equals("T")) {
							mindassetdtl.setField("FIELD_TEXT", detailData.getValue(i, 3));
							mindassetdtl.setField("FIELD_NUMERIC", RegularConstants.NULL);
						} else if (!detailData.getValue(i, 4).equals("T")) {
							mindassetdtl.setField("FIELD_TEXT", RegularConstants.NULL);
							mindassetdtl.setField("FIELD_NUMERIC", detailData.getValue(i, 3));
						}
						if (!mindassetdtl.save()) {
							processResult.setProcessStatus(TBAProcessStatus.FAILURE);
							processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
							return;
						}
						count++;
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());

		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				mindasset.setField("INDIVIDUAL_ASSET_ID", processData.get("INDIVIDUAL_ASSET_ID"));
				mindasset.setField("ENABLED", processData.get("ENABLED"));
				mindasset.setField("REMARKS", processData.get("REMARKS"));
				mindasset.setNew(false);
				if (mindasset.save()) {
					String[] mindassetdtlFields = { "ENTITY_CODE", "LESSEE_CODE", "AGREEMENT_NO", "SCHEDULE_ID", "ASSET_SL", "ASSET_COMP_SL", "ASSET_QTY_SL", "FIELD_SL" };
					BindParameterType[] mindassetdtlFieldTypes = { BindParameterType.BIGINT, BindParameterType.VARCHAR, BindParameterType.VARCHAR, BindParameterType.VARCHAR , BindParameterType.INTEGER, BindParameterType.INTEGER, BindParameterType.INTEGER, BindParameterType.INTEGER};
					mindassetdtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
					mindassetdtl.setField("LESSEE_CODE", processData.get("LESSEE_CODE"));
					mindassetdtl.setField("AGREEMENT_NO", processData.get("AGREEMENT_NO"));
					mindassetdtl.setField("SCHEDULE_ID", processData.get("SCHEDULE_ID"));
					mindassetdtl.setField("ASSET_SL", processData.get("ASSET_SL"));
					mindassetdtl.setField("ASSET_COMP_SL", processData.get("ASSET_COMP_SL"));
					mindassetdtl.setField("ASSET_QTY_SL", processData.get("ASSET_QTY_SL"));
					mindassetdtl.setField("FIELD_SL", processData.get("FIELD_SL"));
					mindassetdtl.deleteByFieldsAudit(mindassetdtlFields, mindassetdtlFieldTypes);
					DTDObject detailData = processData.getDTDObject("MINDASSETDTL");
					mindassetdtl.setNew(true);
						int count = 1;
						for (int i = 0; i < detailData.getRowCount(); ++i) {
							mindassetdtl.setField("FIELD_SL", count);
							mindassetdtl.setField("FIELD_ID", detailData.getValue(i, 1));
							mindassetdtl.setField("FIELD_VALUE_TYPE", detailData.getValue(i, 4));
							if (detailData.getValue(i, 4).equals("T")) {
								mindassetdtl.setField("FIELD_TEXT", detailData.getValue(i, 3));
								mindassetdtl.setField("FIELD_NUMERIC", RegularConstants.NULL);
							} else if (!detailData.getValue(i, 4).equals("T")) {
								mindassetdtl.setField("FIELD_TEXT", RegularConstants.NULL);
								mindassetdtl.setField("FIELD_NUMERIC", detailData.getValue(i, 3));
							}
							if (!mindassetdtl.save()) {
								processResult.setProcessStatus(TBAProcessStatus.FAILURE);
								processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
								return;
							}
							count++;
						}
						processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
						processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
					} else {
						processResult.setProcessStatus(TBAProcessStatus.FAILURE);
						processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
					}
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
				}
			} catch (Exception e) {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				throw new TBAFrameworkException(e.getLocalizedMessage());

			}
		}
}
