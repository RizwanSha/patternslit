/*
 *The program used for Financial Reporting Leaf Element Attributes Maintenance

 Author : ARULPRASATH A
 Created Date : 03-Dec-2016
 Spec Reference : PPBS/CMN-MFINRPTLEAFATRB
 Modification History
 ----------------------------------------------------------------------
 Sl.No		Modified Date	Author			Modified Changes	Version
 ----------------------------------------------------------------------

 */
package patterns.config.framework.bo.cmn;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;

public class mfinrptleafatrbBO extends GenericBO {
	TBADynaSQL mfinrptleafatrb = null;

	public mfinrptleafatrbBO() {

	}

	public void init() {
		mfinrptleafatrb = new TBADynaSQL("FINRPTLEAFATRB", true);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("REPORT_ELEMENT_CODE");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("DESCRIPTION"));
		mfinrptleafatrb.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		mfinrptleafatrb.setField("REPORT_ELEMENT_CODE", processData.get("REPORT_ELEMENT_CODE"));
		dataExists = mfinrptleafatrb.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				mfinrptleafatrb.setNew(true);
				mfinrptleafatrb.setField("ADJ_DIFF_TOBE_ADDED", processData.get("ADJ_DIFF_TOBE_ADDED"));
				mfinrptleafatrb.setField("ELEMENT_AMT_DISP_OPT", processData.get("ELEMENT_AMT_DISP_OPT"));
				mfinrptleafatrb.setField("DISP_PRINT_OPTION", processData.get("DISP_PRINT_OPTION"));
				mfinrptleafatrb.setField("PRINT_NOF_ACS", processData.get("PRINT_NOF_ACS"));
				mfinrptleafatrb.setField("PRINT_NOF_NOMINAL_ENTRIES", processData.get("PRINT_NOF_NOMINAL_ENTRIES"));
				mfinrptleafatrb.setField("SEPARATE_SCH_REQD", processData.get("SEPARATE_SCH_REQD"));
				mfinrptleafatrb.setField("REMARKS", processData.get("REMARKS"));
				if (mfinrptleafatrb.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mfinrptleafatrb.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				mfinrptleafatrb.setNew(false);
				mfinrptleafatrb.setField("ADJ_DIFF_TOBE_ADDED", processData.get("ADJ_DIFF_TOBE_ADDED"));
				mfinrptleafatrb.setField("ELEMENT_AMT_DISP_OPT", processData.get("ELEMENT_AMT_DISP_OPT"));
				mfinrptleafatrb.setField("DISP_PRINT_OPTION", processData.get("DISP_PRINT_OPTION"));
				mfinrptleafatrb.setField("PRINT_NOF_ACS", processData.get("PRINT_NOF_ACS"));
				mfinrptleafatrb.setField("PRINT_NOF_NOMINAL_ENTRIES", processData.get("PRINT_NOF_NOMINAL_ENTRIES"));
				mfinrptleafatrb.setField("SEPARATE_SCH_REQD", processData.get("SEPARATE_SCH_REQD"));
				mfinrptleafatrb.setField("REMARKS", processData.get("REMARKS"));
				if (mfinrptleafatrb.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mfinrptleafatrb.close();
		}
	}
}
