package patterns.config.framework.bo.cmn;

import java.util.Date;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.BindParameterType;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTDObject;
import patterns.config.framework.web.BackOfficeFormatUtils;

public class minttenortaxBO extends GenericBO {
	TBADynaSQL inttenortaxhist = null;
	TBADynaSQL inttenortaxhistdtl = null;

	public minttenortaxBO() {
	}

	@Override
	public void init() throws TBAFrameworkException {
		// TODO Auto-generated method stub
		inttenortaxhist = new TBADynaSQL("INTTENORTAXHIST", true);
		inttenortaxhistdtl = new TBADynaSQL("INTTENORTAXHISTDTL", false);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("INT_RATE_TYPE_CODE") + RegularConstants.PK_SEPARATOR + BackOfficeFormatUtils.getDate((Date) processData.getDate("EFF_DATE"), BackOfficeConstants.TBA_DATE_FORMAT);
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("INT_RATE_TYPE_CODE"));
		inttenortaxhist.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		inttenortaxhist.setField("INT_RATE_TYPE_CODE", processData.get("INT_RATE_TYPE_CODE"));
		inttenortaxhist.setField("EFF_DATE", processData.getDate("EFF_DATE"));
		dataExists = inttenortaxhist.loadByKeyFields();
	}

	@Override
	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				inttenortaxhist.setField("TENOR_SPEC_CODE", processData.get("TENOR_SPEC_CODE"));
				inttenortaxhist.setField("TAX_APP_ON_INTEREST", processData.get("TAX_APP_ON_INTEREST"));
				inttenortaxhist.setField("INT_RATE_INCL_OF_TAX", processData.get("INT_RATE_INCL_OF_TAX"));
				inttenortaxhist.setField("ENABLED", processData.get("ENABLED"));
				inttenortaxhist.setField("REMARKS", processData.get("REMARKS"));
				inttenortaxhist.setNew(true);
				if (inttenortaxhist.save()) {
					if (processData.get("TAX_APP_ON_INTEREST").equals(RegularConstants.ONE)) {
						inttenortaxhistdtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
						inttenortaxhistdtl.setField("INT_RATE_TYPE_CODE", processData.get("INT_RATE_TYPE_CODE"));
						inttenortaxhistdtl.setField("EFF_DATE", processData.getDate("EFF_DATE"));
						DTDObject detailData = processData.getDTDObject("INTTENORTAXHISTDTL");
						inttenortaxhistdtl.setNew(true);
						int count = 1;
						for (int i = 0; i < detailData.getRowCount(); ++i) {
							inttenortaxhistdtl.setField("DTL_SL", count);
							inttenortaxhistdtl.setField("TAX_CODE", detailData.getValue(i, 1));
							if (!inttenortaxhistdtl.save()) {
								processResult.setProcessStatus(TBAProcessStatus.FAILURE);
								processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
								return;
							}
							count++;
						}
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());

		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				inttenortaxhist.setField("TENOR_SPEC_CODE", processData.get("TENOR_SPEC_CODE"));
				inttenortaxhist.setField("TAX_APP_ON_INTEREST", processData.get("TAX_APP_ON_INTEREST"));
				inttenortaxhist.setField("INT_RATE_INCL_OF_TAX", processData.get("INT_RATE_INCL_OF_TAX"));
				inttenortaxhist.setField("ENABLED", processData.get("ENABLED"));
				inttenortaxhist.setField("REMARKS", processData.get("REMARKS"));
				inttenortaxhist.setNew(false);
				if (inttenortaxhist.save()) {
					String[] inttenortaxhistdtlFields = { "ENTITY_CODE", "INT_RATE_TYPE_CODE", "EFF_DATE", "DTL_SL" };
					BindParameterType[] inttenortaxhistdtlFieldTypes = { BindParameterType.BIGINT, BindParameterType.VARCHAR, BindParameterType.DATE, BindParameterType.INTEGER };
					inttenortaxhistdtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
					inttenortaxhistdtl.setField("INT_RATE_TYPE_CODE", processData.get("INT_RATE_TYPE_CODE"));
					inttenortaxhistdtl.setField("EFF_DATE", processData.getDate("EFF_DATE"));
					inttenortaxhistdtl.deleteByFieldsAudit(inttenortaxhistdtlFields, inttenortaxhistdtlFieldTypes);
					DTDObject detailData = processData.getDTDObject("INTTENORTAXHISTDTL");
					inttenortaxhistdtl.setNew(true);
					if (processData.get("TAX_APP_ON_INTEREST").equals(RegularConstants.ONE)) {
						int count = 1;
						for (int i = 0; i < detailData.getRowCount(); ++i) {
							inttenortaxhistdtl.setField("DTL_SL", count);
							inttenortaxhistdtl.setField("TAX_CODE", detailData.getValue(i, 1));
							if (!inttenortaxhistdtl.save()) {
								processResult.setProcessStatus(TBAProcessStatus.FAILURE);
								processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
								return;
							}
							count++;
						}
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		}
	}
}
