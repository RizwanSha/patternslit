package patterns.config.framework.bo.cmn;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.web.ajax.ContentManager;

public class minstypesBO extends GenericBO {

	TBADynaSQL minstypes = null;

	public minstypesBO() {
	}

	public void init() {
		minstypes = new TBADynaSQL("INSTTYPES", true);
		primaryKey = processData.get("ENTITY_CODE")+ RegularConstants.PK_SEPARATOR +processData.get(" INST_TYPE_CODE");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("DESCRIPTION"));
		minstypes.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		minstypes.setField("INST_TYPE_CODE", processData.get("INST_TYPE_CODE"));
		dataExists = minstypes.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				minstypes.setNew(true);
				minstypes.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				minstypes.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				minstypes.setField("INST_TYPE_FLAG", processData.get("INST_TYPE_FLAG"));
				minstypes.setField("MICR_CODE_FOR_INST_TYPE", processData.get("MICR_CODE_FOR_INST_TYPE"));
				minstypes.setField("INV_MAINTAINED_IN_SYS", processData.get("INV_MAINTAINED_IN_SYS"));
				minstypes.setField("PAYABLE_AT_ALL_BRNS", processData.get("PAYABLE_AT_ALL_BRNS"));
				minstypes.setField("INS_WITH_PREALLOC_AC_NO", processData.get("INS_WITH_PREALLOC_AC_NO"));
				minstypes.setField("INS_PREFIX_REQD", processData.get("INS_PREFIX_REQD"));
				minstypes.setField("PERSONALIZATION_REQD", processData.get("PERSONALIZATION_REQD"));
				minstypes.setField("PERSONALIZATION_ON_BRN_STOCK ", processData.get("PERSONALIZATION_ON_BRN_STOCK"));
				minstypes.setField("ENABLED", processData.get("ENABLED"));
				minstypes.setField("REMARKS", processData.get("REMARKS"));
				if (minstypes.save()) {
					minstypes.setField(ContentManager.PURPOSE_ID, "MICR_CODE_FOR_INST_TYPE");
					minstypes.setField(ContentManager.DEDUP_KEY, processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("MICR_CODE_FOR_INST_TYPE"));
					if (processData.get("MICR_CODE_FOR_INST_TYPE") != null && !minstypes.udateTBADuplicateCheck()) {
						processResult.setProcessStatus(TBAProcessStatus.FAILURE);
						processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
						throw new TBAFrameworkException();
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			minstypes.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				minstypes.setNew(false);
				minstypes.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				minstypes.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				minstypes.setField("INST_TYPE_FLAG", processData.get("INST_TYPE_FLAG"));
				minstypes.setField("MICR_CODE_FOR_INST_TYPE", processData.get("MICR_CODE_FOR_INST_TYPE"));
				minstypes.setField("INV_MAINTAINED_IN_SYS", processData.get("INV_MAINTAINED_IN_SYS"));
				minstypes.setField("PAYABLE_AT_ALL_BRNS", processData.get("PAYABLE_AT_ALL_BRNS"));
				minstypes.setField("INS_WITH_PREALLOC_AC_NO", processData.get("INS_WITH_PREALLOC_AC_NO"));
				minstypes.setField("INS_PREFIX_REQD", processData.get("INS_PREFIX_REQD"));
				minstypes.setField("PERSONALIZATION_REQD", processData.get("PERSONALIZATION_REQD"));
				minstypes.setField("PERSONALIZATION_ON_BRN_STOCK", processData.get("PERSONALIZATION_ON_BRN_STOCK"));
				minstypes.setField("ENABLED", processData.get("ENABLED"));
				minstypes.setField("REMARKS", processData.get("REMARKS"));
				if (minstypes.save()) {
					minstypes.setField(ContentManager.PURPOSE_ID, "MICR_CODE_FOR_INST_TYPE");
					minstypes.setField(ContentManager.DEDUP_KEY, processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("MICR_CODE_FOR_INST_TYPE"));
					if (processData.get("MICR_CODE_FOR_INST_TYPE") != null && !minstypes.udateTBADuplicateCheck()) {
						processResult.setProcessStatus(TBAProcessStatus.FAILURE);
						processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
						throw new TBAFrameworkException();
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			minstypes.close();
		}
	}
}