package patterns.config.framework.bo.cmn;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.web.ajax.ContentManager;

public class maccounttypeBO extends GenericBO {

	TBADynaSQL maccounttype = null;

	public maccounttypeBO() {
	}

	public void init() {
		maccounttype = new TBADynaSQL("ACCOUNTTYPES", true);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("ACCOUNT_TYPE_CODE");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("DESCRIPTION"));
		maccounttype.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		maccounttype.setField("ACCOUNT_TYPE_CODE", processData.get("ACCOUNT_TYPE_CODE"));
		dataExists = maccounttype.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				maccounttype.setNew(true);
				maccounttype.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				maccounttype.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				maccounttype.setField("ALPHA_CODE", processData.get("ALPHA_CODE"));
				maccounttype.setField("SUB_ACNT_TYPE_AVAILABLE", processData.get("SUB_ACNT_TYPE_AVAILABLE"));
				maccounttype.setField("GOVT_SCHEME_CODE", processData.get("GOVT_SCHEME_CODE"));
				maccounttype.setField("CHEQUE_BOOK_FAC_AVAILABLE", processData.get("CHEQUE_BOOK_FAC_AVAILABLE"));
				maccounttype.setField("PASS_BOOK_FAC_AVAILABLE", processData.get("PASS_BOOK_FAC_AVAILABLE"));
				maccounttype.setField("DEBIT_CARD_FAC_AVAILABLE", processData.get("DEBIT_CARD_FAC_AVAILABLE"));
				maccounttype.setField("ENABLED", processData.get("ENABLED"));
				maccounttype.setField("REMARKS", processData.get("REMARKS"));
				if (maccounttype.save()) {
					maccounttype.setField(ContentManager.PURPOSE_ID, "ALPHA_CODE");
					maccounttype.setField(ContentManager.DEDUP_KEY, processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("ALPHA_CODE"));
					if (processData.get("ALPHA_CODE") != null && !maccounttype.udateTBADuplicateCheck()) {
						processResult.setProcessStatus(TBAProcessStatus.FAILURE);
						processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
						throw new TBAFrameworkException();
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			maccounttype.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				maccounttype.setNew(false);
				maccounttype.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				maccounttype.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				maccounttype.setField("ALPHA_CODE", processData.get("ALPHA_CODE"));
				maccounttype.setField("SUB_ACNT_TYPE_AVAILABLE", processData.get("SUB_ACNT_TYPE_AVAILABLE"));
				maccounttype.setField("GOVT_SCHEME_CODE", processData.get("GOVT_SCHEME_CODE"));
				maccounttype.setField("CHEQUE_BOOK_FAC_AVAILABLE", processData.get("CHEQUE_BOOK_FAC_AVAILABLE"));
				maccounttype.setField("PASS_BOOK_FAC_AVAILABLE", processData.get("PASS_BOOK_FAC_AVAILABLE"));
				maccounttype.setField("DEBIT_CARD_FAC_AVAILABLE", processData.get("DEBIT_CARD_FAC_AVAILABLE"));
				maccounttype.setField("ENABLED", processData.get("ENABLED"));
				maccounttype.setField("REMARKS", processData.get("REMARKS"));
				if (maccounttype.save()) {
					maccounttype.setField("PURPOSE_ID", "ALPHA_CODE");
					maccounttype.setField(ContentManager.DEDUP_KEY, processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("ALPHA_CODE"));
					if (processData.get("ALPHA_CODE") != null && !maccounttype.udateTBADuplicateCheck()) {
						processResult.setProcessStatus(TBAProcessStatus.FAILURE);
						processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
						throw new TBAFrameworkException();
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			maccounttype.close();
		}
	}
}
