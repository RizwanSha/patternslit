package patterns.config.framework.bo.cmn;

import java.util.Date;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.web.BackOfficeFormatUtils;

public class ischpayacparamBO extends GenericBO {

	TBADynaSQL ischpayacparam = null;

	public ischpayacparamBO() {
	}

	@Override
	public void init() throws TBAFrameworkException {

		ischpayacparam = new TBADynaSQL("SCHPAYPARAMHIST", true);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("SCHEME_CODE") + RegularConstants.PK_SEPARATOR + BackOfficeFormatUtils.getDate((Date) processData.getObject("EFFT_DATE"), BackOfficeConstants.TBA_DATE_FORMAT);
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("REMARKS"));
		ischpayacparam.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		ischpayacparam.setField("SCHEME_CODE", processData.get("SCHEME_CODE"));
		ischpayacparam.setField("EFFT_DATE", processData.getObject("EFFT_DATE"));
		dataExists = ischpayacparam.loadByKeyFields();
	}

	@Override
	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				ischpayacparam.setNew(true);
				ischpayacparam.setField("BULK_PAY_PREFUNDED", processData.get("BULK_PAY_PREFUNDED"));
				ischpayacparam.setField("DB_FROM_FILE", processData.get("DB_FROM_FILE"));
				ischpayacparam.setField("DB_AMOUNT_SRC", processData.getObject("DB_AMOUNT_SRC"));
				ischpayacparam.setField("DB_GL_HEAD", processData.get("DB_GL_HEAD"));
				ischpayacparam.setField("DB_AC_NON_PREFUND", processData.get("DB_AC_NON_PREFUND"));
				ischpayacparam.setField("CR_GL_HEAD", processData.get("CR_GL_HEAD"));
				ischpayacparam.setField("FAILED_CR_GL_HEAD", processData.get("FAILED_CR_GL_HEAD"));
				ischpayacparam.setField("ENABLED", processData.get("ENABLED"));
				ischpayacparam.setField("REMARKS", processData.get("REMARKS"));
				if (ischpayacparam.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			ischpayacparam.close();
		}
	}

	@Override
	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				ischpayacparam.setNew(false);
				ischpayacparam.setField("BULK_PAY_PREFUNDED", processData.get("BULK_PAY_PREFUNDED"));
				ischpayacparam.setField("DB_FROM_FILE", processData.get("DB_FROM_FILE"));
				ischpayacparam.setField("DB_AMOUNT_SRC", processData.getObject("DB_AMOUNT_SRC"));
				ischpayacparam.setField("DB_GL_HEAD", processData.get("DB_GL_HEAD"));
				ischpayacparam.setField("DB_AC_NON_PREFUND", processData.get("DB_AC_NON_PREFUND"));
				ischpayacparam.setField("CR_GL_HEAD", processData.get("CR_GL_HEAD"));
				ischpayacparam.setField("FAILED_CR_GL_HEAD", processData.get("FAILED_CR_GL_HEAD"));
				ischpayacparam.setField("ENABLED", processData.get("ENABLED"));
				ischpayacparam.setField("REMARKS", processData.get("REMARKS"));
				if (ischpayacparam.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			ischpayacparam.close();
		}
	}
}