package patterns.config.framework.bo.cmn;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.web.BackOfficeFormatUtils;


public class itaxbyuscustomerBO extends GenericBO {

	TBADynaSQL itaxbyuscustomerhist = null;

	public itaxbyuscustomerBO() {
	}

	public void init() {
		itaxbyuscustomerhist = new TBADynaSQL("TAXBYUSCUSTOMERHIST", true);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("CUSTOMER_ID") + RegularConstants.PK_SEPARATOR + processData.get("STATE_CODE") + RegularConstants.PK_SEPARATOR + processData.get("TAX_CODE") + RegularConstants.PK_SEPARATOR + BackOfficeFormatUtils.getDate(processData.getDate("EFF_DATE"), BackOfficeConstants.TBA_DATE_FORMAT);
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("CUSTOMER_ID"));
		itaxbyuscustomerhist.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		itaxbyuscustomerhist.setField("CUSTOMER_ID", processData.get("CUSTOMER_ID"));
		itaxbyuscustomerhist.setField("STATE_CODE", processData.get("STATE_CODE"));
		itaxbyuscustomerhist.setField("TAX_CODE", processData.get("TAX_CODE"));
		itaxbyuscustomerhist.setField("EFF_DATE", processData.getDate("EFF_DATE"));
		dataExists = itaxbyuscustomerhist.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				itaxbyuscustomerhist.setNew(true);
				itaxbyuscustomerhist.setField("TAX_BY_US", processData.get("TAX_BY_US"));
				itaxbyuscustomerhist.setField("TAX_BYUS_GL_HEAD_CODE", processData.get("TAX_BYUS_GL_HEAD_CODE"));
				itaxbyuscustomerhist.setField("ENABLED", processData.get("ENABLED"));
				itaxbyuscustomerhist.setField("REMARKS", processData.get("REMARKS"));
				if (itaxbyuscustomerhist.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}

			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			itaxbyuscustomerhist.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				itaxbyuscustomerhist.setNew(false);
				itaxbyuscustomerhist.setField("TAX_BY_US", processData.get("TAX_BY_US"));
				itaxbyuscustomerhist.setField("TAX_BYUS_GL_HEAD_CODE", processData.get("TAX_BYUS_GL_HEAD_CODE"));
				itaxbyuscustomerhist.setField("ENABLED", processData.get("ENABLED"));
				itaxbyuscustomerhist.setField("REMARKS", processData.get("REMARKS"));
				if (itaxbyuscustomerhist.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			itaxbyuscustomerhist.close();
		}
	}
}
