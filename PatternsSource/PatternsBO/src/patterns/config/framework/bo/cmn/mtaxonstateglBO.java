package patterns.config.framework.bo.cmn;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;

public class mtaxonstateglBO extends GenericBO {
	TBADynaSQL mtaxonstategl = null;

	public void init() throws TBAFrameworkException {
		mtaxonstategl = new TBADynaSQL("TAXONSTATEGL", true);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("STATE_CODE") + RegularConstants.PK_SEPARATOR + processData.get("TAX_CODE") + RegularConstants.PK_SEPARATOR + processData.get("TAX_ON_STATE_CODE");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("TAX_ON_STATE_CODE"));
		mtaxonstategl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		mtaxonstategl.setField("STATE_CODE", processData.get("STATE_CODE"));
		mtaxonstategl.setField("TAX_CODE", processData.get("TAX_CODE"));
		mtaxonstategl.setField("TAX_ON_STATE_CODE", processData.get("TAX_ON_STATE_CODE"));
		dataExists = mtaxonstategl.loadByKeyFields();
	}

	@Override
	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				mtaxonstategl.setNew(true);
				mtaxonstategl.setField("TAX_PAY_GL_HEAD_CODE", processData.get("TAX_PAY_GL_HEAD_CODE"));
				mtaxonstategl.setField("ENABLED", processData.get("ENABLED"));	
				mtaxonstategl.setField("REMARKS", processData.get("REMARKS"));
				if (mtaxonstategl.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mtaxonstategl.close();
		}
	}

	@Override
	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				mtaxonstategl.setNew(false);
				mtaxonstategl.setField("TAX_PAY_GL_HEAD_CODE", processData.get("TAX_PAY_GL_HEAD_CODE"));
				mtaxonstategl.setField("ENABLED", processData.get("ENABLED"));	
				mtaxonstategl.setField("REMARKS", processData.get("REMARKS"));
				if (mtaxonstategl.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mtaxonstategl.close();
		}
	}

}
