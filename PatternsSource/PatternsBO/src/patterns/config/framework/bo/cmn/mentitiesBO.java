package patterns.config.framework.bo.cmn;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;

public class mentitiesBO extends GenericBO {
	TBADynaSQL mentities = null;

	public mentitiesBO() {

	}

	public void init() {
		mentities = new TBADynaSQL("ENTITIES", true);
		primaryKey = processData.get("ENTITY_CODE");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("ENTITY_NAME"));
		mentities.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		dataExists = mentities.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				mentities.setNew(false);
				mentities.setField("ENTITY_NAME", processData.get("ENTITY_NAME"));
				mentities.setField("DATE_OF_INCORP", processData.getDate("DATE_OF_INCORP"));
				mentities.setField("BANK_LIC_TYPES", processData.get("BANK_LIC_TYPES"));
				mentities.setField("BASE_CCY_CODE", processData.get("BASE_CCY_CODE"));
				mentities.setField("BASE_ADDR_TYPE", processData.get("BASE_ADDR_TYPE"));
				mentities.setField("OUR_COUNTRY_CODE", processData.get("OUR_COUNTRY_CODE"));
				mentities.setField("FIN_YEAR_START_MONTH", processData.get("FIN_YEAR_START_MONTH"));
				mentities.setField("START_DAY_OF_WEEK", processData.get("START_DAY_OF_WEEK"));
				mentities.setField("BRANCH_REQD", processData.get("BRANCH_REQD"));
				mentities.setField("BRN_OFFICE_NO_OF_DIGITS", processData.get("BRN_OFFICE_NO_OF_DIGITS"));
				mentities.setField("MULTI_CCY_ALLOWED", processData.get("MULTI_CCY_ALLOWED"));
				mentities.setField("GEO_UNIT_STRUC_STATE_CODE", processData.get("GEO_UNIT_STRUC_STATE_CODE"));
				mentities.setField("GEO_UNIT_STRUC_DIST_CODE", processData.get("GEO_UNIT_STRUC_DIST_CODE"));
				mentities.setField("GEO_UNIT_STRUC_PINCODE", processData.get("GEO_UNIT_STRUC_PINCODE"));
				mentities.setField("ENTITY_DISSOLVED", processData.get("ENTITY_DISSOLVED"));
				mentities.setField("ENTITY_DISSOLVED_ON", processData.getDate("ENTITY_DISSOLVED_ON"));
				mentities.setField("REMARKS", processData.get("REMARKS"));
				if (mentities.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mentities.close();
		}
	}
}
