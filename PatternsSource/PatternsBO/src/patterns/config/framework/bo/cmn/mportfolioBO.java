package patterns.config.framework.bo.cmn;

/*
*
Author : Javeed S
Created Date : 28-JAN-2016
Spec Reference : PPBS/CMN/0001-MPORTFOLIO -Portfolio Maintenance
Modification History
----------------------------------------------------------------------
Sl.No		Modified Date	Author			Modified Changes	Version
----------------------------------------------------------------------

*/

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;

public class mportfolioBO extends GenericBO {

	TBADynaSQL portfolio = null;

	public mportfolioBO() {
	}

	public void init() {
		portfolio = new TBADynaSQL("PORTFOLIO", true);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("PORTFOLIO_CODE");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("PORTFOLIO_CODE"));
		portfolio.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		portfolio.setField("PORTFOLIO_CODE", processData.get("PORTFOLIO_CODE"));
		dataExists = portfolio.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				portfolio.setNew(true);
				portfolio.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				portfolio.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				portfolio.setField("PARENT_PORTFOLIO_CODE", processData.get("PARENT_PORTFOLIO_CODE"));
				portfolio.setField("STATUS", processData.get("STATUS"));
				portfolio.setField("ENABLED", processData.get("ENABLED"));
				portfolio.setField("REMARKS", processData.get("REMARKS"));
				if (portfolio.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
					portfolio.sendInterfaceOutwardMessage();
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			portfolio.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				portfolio.setNew(false);
				portfolio.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				portfolio.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				portfolio.setField("PARENT_PORTFOLIO_CODE", processData.get("PARENT_PORTFOLIO_CODE"));
				portfolio.setField("STATUS", processData.get("STATUS"));
				portfolio.setField("ENABLED", processData.get("ENABLED"));
				portfolio.setField("REMARKS", processData.get("REMARKS"));
				if (portfolio.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
					portfolio.sendInterfaceOutwardMessage();
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			portfolio.close();
		}
	}
}