package patterns.config.framework.bo.cmn;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;

public class mproductgroupBO extends GenericBO {
	TBADynaSQL mproductgroup = null;
	public mproductgroupBO() {
		
	}

	public void init() {		
		mproductgroup = new TBADynaSQL("PRODUCTGROUP", true);
		primaryKey = processData.get("ENTITY_CODE")+RegularConstants.PK_SEPARATOR + processData.get("PRODUCT_GROUP_CODE");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("DESCRIPTION"));
		mproductgroup.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		mproductgroup.setField("PRODUCT_GROUP_CODE", processData.get("PRODUCT_GROUP_CODE"));
		dataExists = mproductgroup.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				mproductgroup.setNew(true);
				mproductgroup.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				mproductgroup.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				mproductgroup.setField("PRODUCT_GROUP_ALPHA_CODE", processData.get("PRODUCT_GROUP_ALPHA_CODE"));
				mproductgroup.setField("GROUP_TYPE", processData.get("GROUP_TYPE"));
				mproductgroup.setField("ENABLED", processData.get("ENABLED"));
				mproductgroup.setField("REMARKS", processData.get("REMARKS"));
				if (mproductgroup.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mproductgroup.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				mproductgroup.setNew(false);
				mproductgroup.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				mproductgroup.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				mproductgroup.setField("PRODUCT_GROUP_ALPHA_CODE", processData.get("PRODUCT_GROUP_ALPHA_CODE"));
				mproductgroup.setField("GROUP_TYPE", processData.get("GROUP_TYPE"));
				mproductgroup.setField("ENABLED", processData.get("ENABLED"));
				mproductgroup.setField("REMARKS", processData.get("REMARKS"));
				if (mproductgroup.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mproductgroup.close();
		}
	}
}
