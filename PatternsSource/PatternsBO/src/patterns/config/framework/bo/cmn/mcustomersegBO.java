package patterns.config.framework.bo.cmn;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;

public class mcustomersegBO extends GenericBO {
	TBADynaSQL mcustomerseg = null;

	public mcustomersegBO() {
	}

	public void init() {
		mcustomerseg = new TBADynaSQL("CUSTOMERSEG", true);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("CUST_SEGMENT_CODE");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("DESCRIPTION"));
		mcustomerseg.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		mcustomerseg.setField("CUST_SEGMENT_CODE", processData.get("CUST_SEGMENT_CODE"));
		dataExists = mcustomerseg.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				mcustomerseg.setNew(true);
				mcustomerseg.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				mcustomerseg.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				mcustomerseg.setField("SEGMENT_FOR", processData.get("SEGMENT_FOR"));
				mcustomerseg.setField("PRIORTY_SECTOR_SEG", processData.get("PRIORTY_SECTOR_SEG"));
				mcustomerseg.setField("SENIOR_CIT_SEG", processData.get("SENIOR_CIT_SEG"));
				mcustomerseg.setField("WOMEN_SEG", processData.get("WOMEN_SEG"));
				mcustomerseg.setField("STAFF_SEG", processData.get("STAFF_SEG"));
				mcustomerseg.setField("ENABLED", processData.get("ENABLED"));
				mcustomerseg.setField("REMARKS", processData.get("REMARKS"));
				if (mcustomerseg.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mcustomerseg.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				mcustomerseg.setNew(false);
				mcustomerseg.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				mcustomerseg.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				mcustomerseg.setField("SEGMENT_FOR", processData.get("SEGMENT_FOR"));
				mcustomerseg.setField("PRIORTY_SECTOR_SEG", processData.get("PRIORTY_SECTOR_SEG"));
				mcustomerseg.setField("SENIOR_CIT_SEG", processData.get("SENIOR_CIT_SEG"));
				mcustomerseg.setField("WOMEN_SEG", processData.get("WOMEN_SEG"));
				mcustomerseg.setField("STAFF_SEG", processData.get("STAFF_SEG"));
				mcustomerseg.setField("ENABLED", processData.get("ENABLED"));
				mcustomerseg.setField("REMARKS", processData.get("REMARKS"));
				if (mcustomerseg.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}

		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mcustomerseg.close();
		}
	}
}