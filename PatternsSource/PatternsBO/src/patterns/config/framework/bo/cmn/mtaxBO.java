package patterns.config.framework.bo.cmn;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.BindParameterType;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTDObject;

public class mtaxBO extends GenericBO {

	TBADynaSQL mtax = null;
	TBADynaSQL mtaxdtl = null;

	public mtaxBO() {
	}

	public void init() {
		mtax = new TBADynaSQL("TAX", true);
		mtaxdtl = new TBADynaSQL("TAXSCHEDULE", false);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("STATE_CODE") + RegularConstants.PK_SEPARATOR + processData.get("TAX_CODE");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("DESCRIPTION"));
		mtax.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		mtax.setField("STATE_CODE", processData.get("STATE_CODE"));
		mtax.setField("TAX_CODE", processData.get("TAX_CODE"));
		dataExists = mtax.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				mtax.setNew(true);
				mtax.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				mtax.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				mtax.setField("TAX_TYPE", processData.get("TAX_TYPE"));
				mtax.setField("TAX_ON", processData.get("TAX_ON"));
				mtax.setField("TAX_ON_TAX_CODE", processData.get("TAX_ON_TAX_CODE"));
				mtax.setField("TAX_INPUT_GL_HEAD_CODE", processData.get("TAX_INPUT_GL_HEAD_CODE"));
				mtax.setField("TAX_PAY_GL_HEAD_CODE", processData.get("TAX_PAY_GL_HEAD_CODE"));
				mtax.setField("TAX_BYUS_GL_HEAD_CODE", processData.get("TAX_BYUS_GL_HEAD_CODE"));
				mtax.setField("NOF_TAX_SCHEDULES", processData.get("NOF_TAX_SCHEDULES"));
				mtax.setField("TAX_BYUS_PROD_ALLOWED", processData.get("TAX_BYUS_PROD_ALLOWED"));
				mtax.setField("TAX_BYUS_CUST_ALLOWED", processData.get("TAX_BYUS_CUST_ALLOWED"));
				mtax.setField("TAX_BYUS_LEASE_ALLOWED", processData.get("TAX_BYUS_LEASE_ALLOWED"));
				mtax.setField("ENABLED", processData.get("ENABLED"));
				mtax.setField("REMARKS", processData.get("REMARKS"));
				if (mtax.save()) {
					mtaxdtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
					mtaxdtl.setField("STATE_CODE", processData.get("STATE_CODE"));
					mtaxdtl.setField("TAX_CODE", processData.get("TAX_CODE"));
					DTDObject detailData = processData.getDTDObject("TAXSCHEDULE");
					mtaxdtl.setNew(true);
					for (int i = 0; i < detailData.getRowCount(); ++i) {
						mtaxdtl.setField("DTL_SL", String.valueOf(i + 1));
						mtaxdtl.setField("TAX_SCHEDULE_CODE", detailData.getValue(i, 1));
						if (!mtaxdtl.save()) {
							processResult.setProcessStatus(TBAProcessStatus.FAILURE);
							processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
						}
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mtax.close();
			mtaxdtl.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				mtax.setNew(false);
				mtax.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				mtax.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				mtax.setField("TAX_TYPE", processData.get("TAX_TYPE"));
				mtax.setField("TAX_ON", processData.get("TAX_ON"));
				mtax.setField("TAX_ON_TAX_CODE", processData.get("TAX_ON_TAX_CODE"));
				mtax.setField("TAX_INPUT_GL_HEAD_CODE", processData.get("TAX_INPUT_GL_HEAD_CODE"));
				mtax.setField("TAX_PAY_GL_HEAD_CODE", processData.get("TAX_PAY_GL_HEAD_CODE"));
				mtax.setField("TAX_BYUS_GL_HEAD_CODE", processData.get("TAX_BYUS_GL_HEAD_CODE"));
				mtax.setField("NOF_TAX_SCHEDULES", processData.get("NOF_TAX_SCHEDULES"));
				mtax.setField("TAX_BYUS_PROD_ALLOWED", processData.get("TAX_BYUS_PROD_ALLOWED"));
				mtax.setField("TAX_BYUS_CUST_ALLOWED", processData.get("TAX_BYUS_CUST_ALLOWED"));
				mtax.setField("TAX_BYUS_LEASE_ALLOWED", processData.get("TAX_BYUS_LEASE_ALLOWED"));
				mtax.setField("ENABLED", processData.get("ENABLED"));
				mtax.setField("REMARKS", processData.get("REMARKS"));
				if (mtax.save()) {
					String[] mtaxdtlFields = { "ENTITY_CODE", "STATE_CODE", "TAX_CODE", "DTL_SL" };
					BindParameterType[] mtaxdtlFieldTypes = { BindParameterType.BIGINT, BindParameterType.VARCHAR, BindParameterType.VARCHAR, BindParameterType.INTEGER };
					mtaxdtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
					mtaxdtl.setField("STATE_CODE", processData.get("STATE_CODE"));
					mtaxdtl.setField("TAX_CODE", processData.get("TAX_CODE"));
					mtaxdtl.deleteByFieldsAudit(mtaxdtlFields, mtaxdtlFieldTypes);
					DTDObject detailData = processData.getDTDObject("TAXSCHEDULE");
					mtaxdtl.setNew(true);
					for (int i = 0; i < detailData.getRowCount(); ++i) {
						mtaxdtl.setField("DTL_SL", String.valueOf(i + 1));
						mtaxdtl.setField("TAX_SCHEDULE_CODE", detailData.getValue(i, 1));
						if (!mtaxdtl.save()) {
							processResult.setProcessStatus(TBAProcessStatus.FAILURE);
							processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
						}
					}

					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mtax.close();
			mtaxdtl.close();
		}
	}
}
