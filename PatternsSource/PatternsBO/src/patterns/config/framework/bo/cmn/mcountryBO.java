package patterns.config.framework.bo.cmn;
/**
* This program will be used to create codes to represent Countries. 
* The program will also configure if the country code needs a Geographical Organization -
*  that would be used while specifying the Address.

*
* 
* @version 1.0
* @author PRASHANTH
* @since   25-Nov-2014
* <br>
* <b>Modified History</b>
* <BR>
* <U><B>
* Sl.No		Modified Date	Author			Modified Changes		Version
* </B></U>
* <br>
* 
* 
* 
* 
* 
* 
* 
*/

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;

public class mcountryBO extends GenericBO {

	TBADynaSQL mcountry = null;
	public mcountryBO() {
	}

	public void init() {
		mcountry = new TBADynaSQL("COUNTRY", true);
		primaryKey = processData.get("COUNTRY_CODE");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("DESCRIPTION"));
		mcountry.setField("COUNTRY_CODE", processData.get("COUNTRY_CODE"));
		dataExists = mcountry.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				mcountry.setNew(true);
				mcountry.setField("COUNTRY_CODE", processData.get("COUNTRY_CODE"));
				mcountry.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				mcountry.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				mcountry.setField("GEO_UNIT_STRUC_REQ", processData.get("GEO_UNIT_STRUC_REQ"));
				mcountry.setField("GEO_UNIT_STRUC_CODE", processData.get("GEO_UNIT_STRUC_CODE"));
				mcountry.setField("ENABLED", processData.get("ENABLED"));
				mcountry.setField("REMARKS", processData.get("REMARKS"));

				if (mcountry.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
				}

			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mcountry.close();
		}
	}


	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				mcountry.setNew(false);
				mcountry.setField("COUNTRY_CODE", processData.get("COUNTRY_CODE"));
				mcountry.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				mcountry.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				mcountry.setField("GEO_UNIT_STRUC_REQ", processData.get("GEO_UNIT_STRUC_REQ"));
				mcountry.setField("GEO_UNIT_STRUC_CODE", processData.get("GEO_UNIT_STRUC_CODE"));
				mcountry.setField("ENABLED", processData.get("ENABLED"));
				mcountry.setField("REMARKS", processData.get("REMARKS"));

				if (mcountry.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mcountry.close();
		}
	}
}

