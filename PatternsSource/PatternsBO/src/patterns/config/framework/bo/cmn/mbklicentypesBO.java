package patterns.config.framework.bo.cmn;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;

/**
 * 
 * 
 * @version 1.0
 * @author Dileep Kumar Reddy T
 * @since 18-Nov-2016 <br>
 *        <b>Modified History</b> <BR>
 *        <U><B> Sl.No Modified Date Author Modified Changes Version </B></U> <br>
 * 
 * 
 */

public class mbklicentypesBO extends GenericBO {
	TBADynaSQL mbklicentypes = null;

	public void init() {
		mbklicentypes = new TBADynaSQL("BANKLICENSE", true);
		primaryKey = processData.get("BANK_LIC_TYPES");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("BANK_LIC_TYPES"));
		mbklicentypes.setField("BANK_LIC_TYPES", processData.get("BANK_LIC_TYPES"));
		dataExists = mbklicentypes.loadByKeyFields();

	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				mbklicentypes.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				mbklicentypes.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				mbklicentypes.setField("CASA_ALLOWED", processData.get("CASA_ALLOWED"));
				mbklicentypes.setField("TERM_DEPOSIT_ALLOWED", processData.get("TERM_DEPOSIT_ALLOWED"));
				mbklicentypes.setField("LOANS_ALLOWED", processData.get("LOANS_ALLOWED"));
				mbklicentypes.setField("PAYMENT_SERVICES_ALLOWED", processData.get("PAYMENT_SERVICES_ALLOWED"));
				mbklicentypes.setField("COLLECTION_SERVICES_ALLOWED", processData.get("COLLECTION_SERVICES_ALLOWED"));
				mbklicentypes.setField("ENABLED", processData.get("ENABLED"));
				mbklicentypes.setField("REMARKS", processData.get("REMARKS"));
				mbklicentypes.setNew(true);
				if (mbklicentypes.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mbklicentypes.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				mbklicentypes.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				mbklicentypes.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				mbklicentypes.setField("CASA_ALLOWED", processData.get("CASA_ALLOWED"));
				mbklicentypes.setField("TERM_DEPOSIT_ALLOWED", processData.get("TERM_DEPOSIT_ALLOWED"));
				mbklicentypes.setField("LOANS_ALLOWED", processData.get("LOANS_ALLOWED"));
				mbklicentypes.setField("PAYMENT_SERVICES_ALLOWED", processData.get("PAYMENT_SERVICES_ALLOWED"));
				mbklicentypes.setField("COLLECTION_SERVICES_ALLOWED", processData.get("COLLECTION_SERVICES_ALLOWED"));
				mbklicentypes.setField("ENABLED", processData.get("ENABLED"));
				mbklicentypes.setField("REMARKS", processData.get("REMARKS"));
				mbklicentypes.setNew(false);
				if (mbklicentypes.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mbklicentypes.close();
		}
	}
}
