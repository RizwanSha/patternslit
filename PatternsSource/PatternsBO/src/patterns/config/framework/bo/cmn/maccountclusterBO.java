/*
 *
 Author : NARESHKUMAR D
 Created Date : 29-JUN-2016
 Spec Reference : PPBS/CMN/0005-,MACCOUNTCLUSTER - Account Cluster codes maintenance(1-Master, Abstraction Program)
 Modification History
 ----------------------------------------------------------------------
 Sl.No		Modified Date	Author			Modified Changes	Version
 ----------------------------------------------------------------------
 
 */

package patterns.config.framework.bo.cmn;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.web.ajax.ContentManager;

public class maccountclusterBO extends GenericBO {

	TBADynaSQL maccountcluster = null;

	public maccountclusterBO() {
	}

	public void init() {
		maccountcluster = new TBADynaSQL("ACNTCLUSTERS", true);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("ACNT_CLUSTER_CODE");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("DESCRIPTION"));
		maccountcluster.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		maccountcluster.setField("ACNT_CLUSTER_CODE", processData.get("ACNT_CLUSTER_CODE"));
		dataExists = maccountcluster.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				maccountcluster.setNew(true);
				maccountcluster.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				maccountcluster.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				maccountcluster.setField("CLUSTER_ALPHA_CODE", processData.get("CLUSTER_ALPHA_CODE"));
				maccountcluster.setField("CLUSTER_STATE_CODE", processData.get("CLUSTER_STATE_CODE"));
				maccountcluster.setField("ENABLED", processData.get("ENABLED"));
				maccountcluster.setField("REMARKS", processData.get("REMARKS"));
				if (maccountcluster.save()) {
					maccountcluster.setField(ContentManager.PURPOSE_ID, "CONNECTION_CHK");
					maccountcluster.setField(ContentManager.DEDUP_KEY, processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("CLUSTER_ALPHA_CODE"));
					if (!maccountcluster.udateTBADuplicateCheck()) {
						processResult.setProcessStatus(TBAProcessStatus.FAILURE);
						processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
						throw new TBAFrameworkException();
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			maccountcluster.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				maccountcluster.setNew(false);
				maccountcluster.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				maccountcluster.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				maccountcluster.setField("CLUSTER_ALPHA_CODE", processData.get("CLUSTER_ALPHA_CODE"));
				maccountcluster.setField("CLUSTER_STATE_CODE", processData.get("CLUSTER_STATE_CODE"));
				maccountcluster.setField("ENABLED", processData.get("ENABLED"));
				maccountcluster.setField("REMARKS", processData.get("REMARKS"));
				if (maccountcluster.save()) {
					maccountcluster.setField(ContentManager.PURPOSE_ID, "CONNECTION_CHK");
					maccountcluster.setField(ContentManager.DEDUP_KEY, processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("CLUSTER_ALPHA_CODE"));
					if (!maccountcluster.udateTBADuplicateCheck()) {
						processResult.setProcessStatus(TBAProcessStatus.FAILURE);
						processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
						throw new TBAFrameworkException();
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			maccountcluster.close();
		}
	}
}
