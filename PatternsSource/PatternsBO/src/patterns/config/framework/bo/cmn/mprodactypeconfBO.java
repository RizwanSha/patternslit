package patterns.config.framework.bo.cmn;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.BindParameterType;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTDObject;
import patterns.config.framework.web.BackOfficeFormatUtils;

public class mprodactypeconfBO extends GenericBO {
	TBADynaSQL iprodccyconfhist = null;
	TBADynaSQL iprodccyconfhistdtl = null;

	public void init() {
		iprodccyconfhist = new TBADynaSQL("PRODACTYPECONFHIST", true);
		iprodccyconfhistdtl = new TBADynaSQL("PRODACTYPECONFHISTDTL", false);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("PRODUCT_CODE") + RegularConstants.PK_SEPARATOR + processData.get("CCY_CODE") + RegularConstants.PK_SEPARATOR + BackOfficeFormatUtils.getDate(processData.getDate("EFF_DATE"), BackOfficeConstants.TBA_DATE_FORMAT);
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("PRODUCT_CODE"));
		iprodccyconfhist.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		iprodccyconfhist.setField("PRODUCT_CODE", processData.get("PRODUCT_CODE"));
		iprodccyconfhist.setField("CCY_CODE", processData.get("CCY_CODE"));
		iprodccyconfhist.setField("EFF_DATE", processData.getDate("EFF_DATE"));
		dataExists = iprodccyconfhist.loadByKeyFields();

	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				iprodccyconfhist.setNew(true);
				iprodccyconfhist.setField("ENABLED", processData.get("ENABLED"));
				iprodccyconfhist.setField("REMARKS", processData.get("REMARKS"));
				if (iprodccyconfhist.save()) {
					iprodccyconfhistdtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
					iprodccyconfhistdtl.setField("PRODUCT_CODE", processData.get("PRODUCT_CODE"));
					iprodccyconfhistdtl.setField("CCY_CODE", processData.get("CCY_CODE"));
					iprodccyconfhistdtl.setField("EFF_DATE", processData.getDate("EFF_DATE"));
					DTDObject detailData = processData.getDTDObject("PRODACTYPECONFHISTDTL");
					iprodccyconfhistdtl.setNew(true);
					for (int i = 0; i < detailData.getRowCount(); ++i) {
						iprodccyconfhistdtl.setField("DT_SL", Integer.valueOf(i + 1).toString());
						iprodccyconfhistdtl.setField("ACCOUNT_TYPE_CODE", detailData.getValue(i, 1));
						iprodccyconfhistdtl.setField("ACCOUNT_SUBTYPE_CODE", detailData.getValue(i, 3));
						if (!iprodccyconfhistdtl.save()) {
							processResult.setProcessStatus(TBAProcessStatus.FAILURE);
							processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
							return;
						}
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			iprodccyconfhist.close();
			iprodccyconfhistdtl.close();
		}
	}

	protected void modify() throws TBAFrameworkException {

		try {
			if (isDataExists()) {
				iprodccyconfhist.setNew(false);
				iprodccyconfhist.setField("ENABLED", processData.get("ENABLED"));
				iprodccyconfhist.setField("REMARKS", processData.get("REMARKS"));
				if (iprodccyconfhist.save()) {
					String[] mprodactypeconfFields = { "ENTITY_CODE", "PRODUCT_CODE", "CCY_CODE", "EFF_DATE", "DT_SL" };
					BindParameterType[] mprodactypeconfTypes = { BindParameterType.BIGINT, BindParameterType.VARCHAR, BindParameterType.VARCHAR, BindParameterType.DATE, BindParameterType.BIGINT };
					iprodccyconfhistdtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
					iprodccyconfhistdtl.setField("PRODUCT_CODE", processData.get("PRODUCT_CODE"));
					iprodccyconfhistdtl.setField("CCY_CODE", processData.get("CCY_CODE"));
					iprodccyconfhistdtl.setField("EFF_DATE", processData.getDate("EFF_DATE"));
					iprodccyconfhistdtl.deleteByFieldsAudit(mprodactypeconfFields, mprodactypeconfTypes);
					DTDObject detailData = processData.getDTDObject("PRODACTYPECONFHISTDTL");
					iprodccyconfhistdtl.setNew(true);
					for (int i = 0; i < detailData.getRowCount(); ++i) {
						iprodccyconfhistdtl.setField("DT_SL", Integer.valueOf(i + 1).toString());
						iprodccyconfhistdtl.setField("ACCOUNT_TYPE_CODE", detailData.getValue(i, 1));
						iprodccyconfhistdtl.setField("ACCOUNT_SUBTYPE_CODE", detailData.getValue(i, 3));
						if (!iprodccyconfhistdtl.save()) {
							processResult.setProcessStatus(TBAProcessStatus.FAILURE);
							processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
							return;
						}
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}

		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			iprodccyconfhist.close();
			iprodccyconfhistdtl.close();
		}
	}

}
