package patterns.config.framework.bo.cmn;

/*
 *
 Author : Javeed S
 Created Date : 06-FEB-2016
 Spec Reference : PPBS/CHG/0029-MPROCCODESBO - Banking Process Codes Maintenance
 Modification History
 ----------------------------------------------------------------------
 Sl.No		Modified Date	Author			Modified Changes	Version
 ----------------------------------------------------------------------

 */

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;

public class mbankproccodesBO extends GenericBO {

	TBADynaSQL bankprocesscodes = null;

	public mbankproccodesBO() {
	}

	public void init() {
		bankprocesscodes = new TBADynaSQL("BANKPROCESSCODES", true);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("BANK_PROCESS_CODE");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("BANK_PROCESS_CODE"));
		bankprocesscodes.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		bankprocesscodes.setField("BANK_PROCESS_CODE", processData.get("BANK_PROCESS_CODE"));
		dataExists = bankprocesscodes.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {

		try {
			if (!isDataExists()) {
				bankprocesscodes.setNew(true);
				bankprocesscodes.setField("BANK_PROCESS_CODE", processData.get("BANK_PROCESS_CODE"));
				bankprocesscodes.setField("NAME", processData.get("NAME"));
				bankprocesscodes.setField("SHORT_NAME", processData.get("SHORT_NAME"));
				bankprocesscodes.setField("GL_TOBE_USED", processData.get("GL_TOBE_USED"));
				bankprocesscodes.setField("ENABLED", processData.get("ENABLED"));
				bankprocesscodes.setField("REMARKS", processData.get("REMARKS"));
				if (bankprocesscodes.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			bankprocesscodes.close();
		}

	}

	protected void modify() throws TBAFrameworkException {

		try {
			if (isDataExists()) {
				bankprocesscodes.setNew(false);
				bankprocesscodes.setField("BANK_PROCESS_CODE", processData.get("BANK_PROCESS_CODE"));
				bankprocesscodes.setField("NAME", processData.get("NAME"));
				bankprocesscodes.setField("SHORT_NAME", processData.get("SHORT_NAME"));
				bankprocesscodes.setField("GL_TOBE_USED", processData.get("GL_TOBE_USED"));
				bankprocesscodes.setField("ENABLED", processData.get("ENABLED"));
				bankprocesscodes.setField("REMARKS", processData.get("REMARKS"));
				if (bankprocesscodes.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
					bankprocesscodes.sendInterfaceOutwardMessage();
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			bankprocesscodes.close();
		}

	}
}
