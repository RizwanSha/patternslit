package patterns.config.framework.bo.cmn;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;

public class mleaseproductBO extends GenericBO {
	TBADynaSQL leaseproduct = null;
	@Override
	public void init() throws TBAFrameworkException {
		leaseproduct = new TBADynaSQL("LEASEPRODUCT", true);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("LEASE_PRODUCT_CODE");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("LEASE_PRODUCT_CODE"));
		leaseproduct.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		leaseproduct.setField("LEASE_PRODUCT_CODE", processData.get("LEASE_PRODUCT_CODE"));
		dataExists = leaseproduct.loadByKeyFields();
	}

	@Override
	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				leaseproduct.setNew(true);
				leaseproduct.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				leaseproduct.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				leaseproduct.setField("LEASE_TYPE", processData.get("LEASE_TYPE"));
				leaseproduct.setField("LEASE_ASSET_IN_CCY", processData.get("LEASE_ASSET_IN_CCY"));
				leaseproduct.setField("SUNDRY_DEBTOR_GL_HEAD_CODE", processData.get("SUNDRY_DEBTOR_GL_HEAD_CODE"));	
				leaseproduct.setField("RENT_RECV_PRIN_GL_HEAD_CODE", processData.get("RENT_RECV_PRIN_GL_HEAD_CODE"));
				leaseproduct.setField("RENT_RECV_INT_GL_HEAD_CODE", processData.get("RENT_RECV_INT_GL_HEAD_CODE"));	
				leaseproduct.setField("UNEARNED_INCOME_GL_HEAD_CODE", processData.get("UNEARNED_INCOME_GL_HEAD_CODE"));
				leaseproduct.setField("ENABLED", processData.get("ENABLED"));	
				leaseproduct.setField("REMARKS", processData.get("REMARKS"));
				if (leaseproduct.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			leaseproduct.close();
		}
	}

	@Override
	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				leaseproduct.setNew(false);
				leaseproduct.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				leaseproduct.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				leaseproduct.setField("LEASE_TYPE", processData.get("LEASE_TYPE"));
				leaseproduct.setField("CCY_CODE", processData.get("LEASE_ASSET_IN_CCY"));
				leaseproduct.setField("SUNDRY_DEBTOR_GL_HEAD_CODE", processData.get("SUNDRY_DEBTOR_GL_HEAD_CODE"));	
				leaseproduct.setField("RENT_RECV_PRIN_GL_HEAD_CODE", processData.get("RENT_RECV_PRIN_GL_HEAD_CODE"));
				leaseproduct.setField("RENT_RECV_INT_GL_HEAD_CODE", processData.get("RENT_RECV_INT_GL_HEAD_CODE"));	
				leaseproduct.setField("UNEARNED_INCOME_GL_HEAD_CODE", processData.get("UNEARNED_INCOME_GL_HEAD_CODE"));
				leaseproduct.setField("ENABLED", processData.get("ENABLED"));	
				leaseproduct.setField("REMARKS", processData.get("REMARKS"));
				if (leaseproduct.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			leaseproduct.close();
		}
	}

}
