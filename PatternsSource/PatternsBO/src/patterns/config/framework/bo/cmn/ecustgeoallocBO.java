package patterns.config.framework.bo.cmn;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.BindParameterType;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTDObject;
import patterns.config.framework.web.BackOfficeFormatUtils;

public class ecustgeoallocBO extends GenericBO {

	TBADynaSQL custgeoallochist = null;
	TBADynaSQL custgeoallochistdtl = null;

	public ecustgeoallocBO() {
	}

	@Override
	public void init() throws TBAFrameworkException {
		custgeoallochist = new TBADynaSQL("CUSTGEOALLOCHIST", true);
		custgeoallochistdtl = new TBADynaSQL("CUSTGEOALLOCHISTDTL", false);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("CIF_NO") + RegularConstants.PK_SEPARATOR + BackOfficeFormatUtils.getDate(processData.getDate("EFF_DATE"), BackOfficeConstants.TBA_DATE_FORMAT);
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("CIF_NO") + RegularConstants.PK_SEPARATOR + processData.getDate("EFF_DATE"));
		custgeoallochist.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		custgeoallochist.setField("CIF_NO", processData.get("CIF_NO"));
		custgeoallochist.setField("EFF_DATE", processData.getDate("EFF_DATE"));
		dataExists = custgeoallochist.loadByKeyFields();

	}

	@Override
	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				custgeoallochist.setField("ENABLED", processData.get("ENABLED"));
				custgeoallochist.setField("REMARKS", processData.get("REMARKS"));
				custgeoallochist.setNew(true);
				if (custgeoallochist.save()) {
					custgeoallochistdtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
					custgeoallochistdtl.setField("CIF_NO", processData.get("CIF_NO"));
					custgeoallochistdtl.setField("EFF_DATE", processData.getDate("EFF_DATE"));
					DTDObject detailData = processData.getDTDObject("CUSTGEOALLOCHISTDTL");
					custgeoallochistdtl.setNew(true);
					for (int i = 0; i < detailData.getRowCount(); ++i) {
						if (detailData.getValue(i, 0).equals("1")) {
							custgeoallochistdtl.setField("DTSL", Integer.valueOf(i + 1).toString());
							custgeoallochistdtl.setField("GEO_REGION_CODE", detailData.getValue(i, 1));

							if (!custgeoallochistdtl.save()) {
								processResult.setProcessStatus(TBAProcessStatus.FAILURE);
								processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
								return;
							}
						}
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			custgeoallochistdtl.close();
			custgeoallochistdtl.close();
		}
	}

	protected void modify() throws TBAFrameworkException {

		try {
			if (isDataExists()) {
				custgeoallochist.setField("ENABLED", processData.get("ENABLED"));
				custgeoallochist.setField("REMARKS", processData.get("REMARKS"));
				custgeoallochist.setNew(false);
				if (custgeoallochist.save()) {
					String[] custgeoallochistdtlFields = { "ENTITY_CODE", "CIF_NO", "EFF_DATE", "DTSL" };
					BindParameterType[] custgeoallochistdtlFieldTypes = { BindParameterType.BIGINT, BindParameterType.BIGINT, BindParameterType.DATE, BindParameterType.BIGINT };
					custgeoallochistdtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
					custgeoallochistdtl.setField("CIF_NO", processData.get("CIF_NO"));
					custgeoallochistdtl.setField("EFF_DATE", processData.getDate("EFF_DATE"));
					custgeoallochistdtl.deleteByFieldsAudit(custgeoallochistdtlFields, custgeoallochistdtlFieldTypes);
					DTDObject detailData = processData.getDTDObject("CUSTGEOALLOCHISTDTL");
					custgeoallochistdtl.setNew(true);
					for (int i = 0; i < detailData.getRowCount(); ++i) {
						if (detailData.getValue(i, 0).equals("1")) {
							custgeoallochistdtl.setField("DTSL", String.valueOf(i + 1));
							custgeoallochistdtl.setField("GEO_REGION_CODE", detailData.getValue(i, 1));
							if (!custgeoallochistdtl.save()) {
								processResult.setProcessStatus(TBAProcessStatus.FAILURE);
								processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
								return;
							}
						}
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			custgeoallochist.close();
			custgeoallochistdtl.close();
		}
	}
}
