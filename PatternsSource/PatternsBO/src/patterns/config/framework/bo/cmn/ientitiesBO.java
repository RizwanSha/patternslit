package patterns.config.framework.bo.cmn;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;

public class ientitiesBO extends GenericBO {

	TBADynaSQL ientities = null;

	public ientitiesBO() {
	}

	public void init() {
		ientities = new TBADynaSQL("ENTITIES", true);
		primaryKey = processData.get("ENTITY_CODE");
		tbaContext.setTbaPrimarykey(primaryKey);
		ientities.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		tbaContext.setDisplayDetails(processData.get("ENTITY_ID"));
		dataExists = ientities.loadByKeyFields();
	}
	
	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				ientities.setNew(false);
				ientities.setField("ENTITY_ID", processData.get("ENTITY_ID"));
				ientities.setField("ENTITY_NAME", processData.get("ENTITY_NAME"));
				ientities.setField("REG_ADDRESS", processData.get("REG_ADDRESS"));
				ientities.setField("PIN_CODE", processData.get("PIN_CODE"));
				ientities.setField("STATE_CODE", processData.get("STATE_CODE"));
				ientities.setField("PAN_NO", processData.get("PAN_NO"));
				ientities.setField("REMARKS", processData.get("REMARKS"));
				if (ientities.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			}else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			ientities.close();
		}
	}

	@Override
	protected void add() throws TBAFrameworkException {
		// TODO Auto-generated method stub
		
	}

}
