package patterns.config.framework.bo.cmn;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;

public class morganizationBO extends GenericBO {
	TBADynaSQL morganization = null;
	public morganizationBO() {
		
	}

	public void init() {		
		morganization = new TBADynaSQL("ORGANIZATIONS", true);
		primaryKey = processData.get("ENTITY_CODE")+RegularConstants.PK_SEPARATOR + processData.get("ORG_CODE");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("DESCRIPTION"));
		morganization.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		morganization.setField("ORG_CODE", processData.get("ORG_CODE"));
		dataExists = morganization.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				morganization.setNew(true);
				morganization.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				morganization.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				morganization.setField("CONTACT_PERSON_TITLE", processData.get("CONTACT_PERSON_TITLE"));
				morganization.setField("CONTACT_PERSON_NAME", processData.get("CONTACT_PERSON_NAME"));
				morganization.setField("CONTACT_EMAIL_ID", processData.get("CONTACT_EMAIL_ID"));
				morganization.setField("ADDRESS", processData.get("ADDRESS"));
				morganization.setField("ENABLED", processData.get("ENABLED"));
				morganization.setField("REMARKS", processData.get("REMARKS"));
				
				if (morganization.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			morganization.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				morganization.setNew(false);
				morganization.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				morganization.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				morganization.setField("CONTACT_PERSON_TITLE", processData.get("CONTACT_PERSON_TITLE"));
				morganization.setField("CONTACT_PERSON_NAME", processData.get("CONTACT_PERSON_NAME"));
				morganization.setField("CONTACT_EMAIL_ID", processData.get("CONTACT_EMAIL_ID"));
				morganization.setField("ADDRESS", processData.get("ADDRESS"));
				morganization.setField("ENABLED", processData.get("ENABLED"));
				morganization.setField("REMARKS", processData.get("REMARKS"));
				if (morganization.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			morganization.close();
		}
	}
}
