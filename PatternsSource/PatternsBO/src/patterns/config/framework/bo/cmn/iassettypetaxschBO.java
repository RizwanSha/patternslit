package patterns.config.framework.bo.cmn;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.BindParameterType;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTDObject;
import patterns.config.framework.web.BackOfficeFormatUtils;

public class iassettypetaxschBO extends GenericBO {
	TBADynaSQL iassettypetaxsch = null;
	TBADynaSQL iassettypetaxschdtl = null;

	public iassettypetaxschBO() {
	}

	public void init() {
		iassettypetaxsch = new TBADynaSQL("ASSETTYPETAXSCHHIST", true);
		iassettypetaxschdtl = new TBADynaSQL("ASSETTYPETAXSCHHISTDTL", false);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("ASSET_TYPE_CODE") + RegularConstants.PK_SEPARATOR + BackOfficeFormatUtils.getDate(processData.getDate("EFF_DATE"), BackOfficeConstants.TBA_DATE_FORMAT);
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("ASSET_TYPE_CODE"));
		iassettypetaxsch.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		iassettypetaxsch.setField("ASSET_TYPE_CODE", processData.get("ASSET_TYPE_CODE"));
		iassettypetaxsch.setField("EFF_DATE", processData.getDate("EFF_DATE"));
		dataExists = iassettypetaxsch.loadByKeyFields();

	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				iassettypetaxsch.setNew(true);
				iassettypetaxsch.setField("ENABLED", processData.get("ENABLED"));
				iassettypetaxsch.setField("REMARKS", processData.get("REMARKS"));
				if (iassettypetaxsch.save()) {
					iassettypetaxschdtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
					iassettypetaxschdtl.setField("ASSET_TYPE_CODE", processData.get("ASSET_TYPE_CODE"));
					iassettypetaxschdtl.setField("EFF_DATE", processData.getDate("EFF_DATE"));
					DTDObject detailData = processData.getDTDObject("ASSETTYPETAXSCHHISTDTL");
					iassettypetaxschdtl.setNew(true);
					for (int i = 0; i < detailData.getRowCount(); ++i) {
						iassettypetaxschdtl.setField("DTL_SL", Integer.valueOf(i + 1).toString());
						iassettypetaxschdtl.setField("STATE_CODE", detailData.getValue(i, 2));
						iassettypetaxschdtl.setField("TAX_SCHEDULE_CODE", detailData.getValue(i, 4));
						if (!iassettypetaxschdtl.save()) {
							processResult.setProcessStatus(TBAProcessStatus.FAILURE);
							processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
							return;
						}
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			iassettypetaxsch.close();
			iassettypetaxschdtl.close();
		}
	}

	protected void modify() throws TBAFrameworkException {

		try {
			if (isDataExists()) {
				iassettypetaxsch.setNew(false);
				iassettypetaxsch.setField("ENABLED", processData.get("ENABLED"));
				iassettypetaxsch.setField("REMARKS", processData.get("REMARKS"));
				if (iassettypetaxsch.save()) {
					String[] iassettypetaxschFields = { "ENTITY_CODE", "ASSET_TYPE_CODE", "EFF_DATE", "DTL_SL" };
					BindParameterType[] iassettypetaxschTypes = { BindParameterType.BIGINT, BindParameterType.VARCHAR, BindParameterType.DATE, BindParameterType.INTEGER };
					iassettypetaxschdtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
					iassettypetaxschdtl.setField("ASSET_TYPE_CODE", processData.get("ASSET_TYPE_CODE"));
					iassettypetaxschdtl.setField("EFF_DATE", processData.getDate("EFF_DATE"));
					iassettypetaxschdtl.deleteByFieldsAudit(iassettypetaxschFields, iassettypetaxschTypes);
					DTDObject detailData = processData.getDTDObject("ASSETTYPETAXSCHHISTDTL");
					iassettypetaxschdtl.setNew(true);
					for (int i = 0; i < detailData.getRowCount(); ++i) {
						iassettypetaxschdtl.setField("DTL_SL", Integer.valueOf(i + 1).toString());
						iassettypetaxschdtl.setField("STATE_CODE", detailData.getValue(i, 2));
						iassettypetaxschdtl.setField("TAX_SCHEDULE_CODE", detailData.getValue(i, 4));
						if (!iassettypetaxschdtl.save()) {
							processResult.setProcessStatus(TBAProcessStatus.FAILURE);
							processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
							return;
						}
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}

		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			iassettypetaxsch.close();
			iassettypetaxschdtl.close();
		}
	}

}
