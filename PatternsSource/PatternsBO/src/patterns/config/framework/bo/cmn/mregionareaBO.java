package patterns.config.framework.bo.cmn;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;


public class mregionareaBO extends GenericBO {

	TBADynaSQL mregionarea = null;

	public mregionareaBO() {
	}

	public void init() {
		mregionarea = new TBADynaSQL("REGIONAREA", true);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("REGION_CODE") + RegularConstants.PK_SEPARATOR + processData.get("AREA_CODE");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("DESCRIPTION"));
		mregionarea.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		mregionarea.setField("REGION_CODE", processData.get("REGION_CODE"));
		mregionarea.setField("AREA_CODE", processData.get("AREA_CODE"));
		dataExists = mregionarea.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				mregionarea.setNew(true);
				mregionarea.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				mregionarea.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				mregionarea.setField("ENABLED", processData.get("ENABLED"));
				mregionarea.setField("REMARKS", processData.get("REMARKS"));
				if (mregionarea.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mregionarea.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				mregionarea.setNew(false);
				mregionarea.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				mregionarea.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				mregionarea.setField("ENABLED", processData.get("ENABLED"));
				mregionarea.setField("REMARKS", processData.get("REMARKS"));
				if (mregionarea.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mregionarea.close();
		}
	}
}
