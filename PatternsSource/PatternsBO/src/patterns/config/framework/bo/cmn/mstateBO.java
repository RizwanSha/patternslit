package patterns.config.framework.bo.cmn;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;

public class mstateBO extends GenericBO {

	TBADynaSQL mstate = null;

	public mstateBO() {
	}

	public void init() {
		mstate = new TBADynaSQL("STATE", true);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("STATE_CODE");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("DESCRIPTION"));
		mstate.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		mstate.setField("STATE_CODE", processData.get("STATE_CODE"));
		dataExists = mstate.loadByKeyFields();
	}
	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				mstate.setNew(true);
				mstate.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				mstate.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				mstate.setField("STATE_UT", processData.get("STATE_UT"));
				mstate.setField("OLD_STATE_SPLIT_ON", processData.getDate("OLD_STATE_SPLIT_ON"));
				mstate.setField("NEW_STATE_FORMATION_ON", processData.getDate("NEW_STATE_FORMATION_ON"));
				mstate.setField("INVOICE_TYPE", processData.get("INVOICE_TYPE"));
				mstate.setField("ENABLED", processData.get("ENABLED"));
				mstate.setField("REMARKS", processData.get("REMARKS"));
				if (mstate.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mstate.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				mstate.setNew(false);
				mstate.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				mstate.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				mstate.setField("STATE_UT", processData.get("STATE_UT"));
				mstate.setField("OLD_STATE_SPLIT_ON", processData.getDate("OLD_STATE_SPLIT_ON"));
				mstate.setField("NEW_STATE_FORMATION_ON", processData.getDate("NEW_STATE_FORMATION_ON"));
				mstate.setField("INVOICE_TYPE", processData.get("INVOICE_TYPE"));
				mstate.setField("ENABLED", processData.get("ENABLED"));
				mstate.setField("REMARKS", processData.get("REMARKS"));
				if (mstate.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			}else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mstate.close();
		}
	}

}
