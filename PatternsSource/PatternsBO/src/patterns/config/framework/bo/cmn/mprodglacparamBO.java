package patterns.config.framework.bo.cmn;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.BindParameterType;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTDObject;

public class mprodglacparamBO extends GenericBO {

	TBADynaSQL mprodglacparam = null;
	TBADynaSQL mprodglacparamdtl = null;
	
	@Override
	public void init() throws TBAFrameworkException {
		mprodglacparam = new TBADynaSQL("PRODGLACPARAM", true);
		mprodglacparamdtl = new TBADynaSQL("PRODGLACPARAMDTL", false);
			primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("PRODUCT_CODE")+ RegularConstants.PK_SEPARATOR+ processData.get("CCY_CODE");
			tbaContext.setTbaPrimarykey(primaryKey);
			tbaContext.setDisplayDetails(processData.get("PRODUCT_CODE"));
			tbaContext.setDisplayDetails(processData.get("CCY_CODE"));
			mprodglacparam.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
			mprodglacparam.setField("PRODUCT_CODE", processData.get("PRODUCT_CODE"));
			mprodglacparam.setField("CCY_CODE", processData.get("CCY_CODE"));
			dataExists = mprodglacparam.loadByKeyFields();		
	}

	@Override
	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				mprodglacparam.setField("PRODUCT_GL_HEAD_CODE", processData.get("PRODUCT_GL_HEAD_CODE"));
				mprodglacparam.setField("REMARKS", processData.get("REMARKS"));
				mprodglacparam.setNew(true);
				if (mprodglacparam.save()) {
					mprodglacparamdtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
					mprodglacparamdtl.setField("PRODUCT_CODE", processData.get("PRODUCT_CODE"));
					mprodglacparamdtl.setField("CCY_CODE", processData.get("CCY_CODE"));
					DTDObject detailData = processData.getDTDObject("PRODGLACPARAMDTL");
					mprodglacparamdtl.setNew(true);
					for (int i = 0; i < detailData.getRowCount(); ++i) {
						mprodglacparamdtl.setField("DT_SL", Integer.valueOf(i + 1).toString());
						mprodglacparamdtl.setField("ACNT_STATUS_CODE", detailData.getValue(i, 1));
						mprodglacparamdtl.setField("GL_HEAD_CODE", detailData.getValue(i, 2));
						if (!mprodglacparamdtl.save()) {
							processResult.setProcessStatus(TBAProcessStatus.FAILURE);
							processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
							return;
						}
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mprodglacparam.close();
			mprodglacparamdtl.close();
		}
	}

	@Override
	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				mprodglacparam.setField("PRODUCT_GL_HEAD_CODE", processData.get("PRODUCT_GL_HEAD_CODE"));
				mprodglacparam.setField("REMARKS", processData.get("REMARKS"));
				mprodglacparam.setNew(false);
				if (mprodglacparam.save()) {
					String[] mprodglacparamdtlFields = { "ENTITY_CODE", "PRODUCT_CODE","CCY_CODE","DT_SL" };
					BindParameterType[] mprodglacparamdtlFieldTypes = { BindParameterType.BIGINT, BindParameterType.VARCHAR, BindParameterType.VARCHAR ,BindParameterType.BIGINT};
					mprodglacparamdtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
					mprodglacparamdtl.setField("PRODUCT_CODE", processData.get("PRODUCT_CODE"));
					mprodglacparamdtl.setField("CCY_CODE", processData.get("CCY_CODE"));
					mprodglacparamdtl.deleteByFieldsAudit(mprodglacparamdtlFields, mprodglacparamdtlFieldTypes);
					DTDObject detailData = processData.getDTDObject("PRODGLACPARAMDTL");
					mprodglacparamdtl.setNew(true);
					for (int i = 0; i < detailData.getRowCount(); ++i) {
						mprodglacparamdtl.setField("DT_SL", String.valueOf(i + 1));
						mprodglacparamdtl.setField("ACNT_STATUS_CODE", detailData.getValue(i, 1));
						mprodglacparamdtl.setField("GL_HEAD_CODE", detailData.getValue(i, 2));
						if (!mprodglacparamdtl.save()) {
							processResult.setProcessStatus(TBAProcessStatus.FAILURE);
							processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
							return;
						}
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mprodglacparam.close();
			mprodglacparamdtl.close();
		}
	}
}
