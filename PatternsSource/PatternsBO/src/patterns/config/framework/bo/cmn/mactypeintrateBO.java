package patterns.config.framework.bo.cmn;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.web.BackOfficeFormatUtils;

public class mactypeintrateBO extends GenericBO {

	TBADynaSQL mactypeintrate = null;

	public mactypeintrateBO() {
	}

	public void init() {
		mactypeintrate = new TBADynaSQL("ACTYPEINTRATEHIST", true);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("PRODUCT_CODE") + RegularConstants.PK_SEPARATOR + processData.get("ACCOUNT_TYPE_CODE") + RegularConstants.PK_SEPARATOR + processData.get("ACCOUNT_SUBTYPE_CODE") + RegularConstants.PK_SEPARATOR + processData.get("CCY_CODE") + RegularConstants.PK_SEPARATOR
				+ BackOfficeFormatUtils.getDate(processData.getDate("EFF_DATE"), BackOfficeConstants.TBA_DATE_FORMAT);
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("EFF_DATE"));
		mactypeintrate.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		mactypeintrate.setField("PRODUCT_CODE", processData.get("PRODUCT_CODE"));
		mactypeintrate.setField("ACCOUNT_TYPE_CODE", processData.get("ACCOUNT_TYPE_CODE"));
		mactypeintrate.setField("ACCOUNT_SUBTYPE_CODE", processData.get("ACCOUNT_SUBTYPE_CODE"));
		mactypeintrate.setField("CCY_CODE", processData.get("CCY_CODE"));
		mactypeintrate.setField("EFF_DATE", processData.getDate("EFF_DATE"));
		dataExists = mactypeintrate.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				mactypeintrate.setNew(true);
				mactypeintrate.setField("INT_RATE_TYPE_CODE_NORMAL", processData.get("INT_RATE_TYPE_CODE_NORMAL"));
				mactypeintrate.setField("INT_RATE_TYPE_CODE_OD", processData.get("INT_RATE_TYPE_CODE_OD"));
				mactypeintrate.setField("INT_RATE_TYPE_CODE_PENAL", processData.get("INT_RATE_TYPE_CODE_PENAL"));
				mactypeintrate.setField("ENABLED", processData.get("ENABLED"));
				mactypeintrate.setField("REMARKS", processData.get("REMARKS"));
				if (mactypeintrate.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mactypeintrate.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				mactypeintrate.setNew(false);
				mactypeintrate.setField("INT_RATE_TYPE_CODE_NORMAL", processData.get("INT_RATE_TYPE_CODE_NORMAL"));
				mactypeintrate.setField("INT_RATE_TYPE_CODE_OD", processData.get("INT_RATE_TYPE_CODE_OD"));
				mactypeintrate.setField("INT_RATE_TYPE_CODE_PENAL", processData.get("INT_RATE_TYPE_CODE_PENAL"));
				mactypeintrate.setField("ENABLED", processData.get("ENABLED"));
				mactypeintrate.setField("REMARKS", processData.get("REMARKS"));
				if (mactypeintrate.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mactypeintrate.close();
		}
	}
}