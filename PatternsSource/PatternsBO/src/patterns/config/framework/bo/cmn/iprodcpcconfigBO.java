package patterns.config.framework.bo.cmn;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.web.BackOfficeFormatUtils;

public class iprodcpcconfigBO extends GenericBO {
	TBADynaSQL prodcpconfighist = null;

	public void init() {
		prodcpconfighist = new TBADynaSQL("PRODCPCONFIGHIST", true);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("LEASE_PRODUCT_CODE") + RegularConstants.PK_SEPARATOR + processData.get("PORTFOLIO_CODE") + RegularConstants.PK_SEPARATOR
				+ BackOfficeFormatUtils.getDate(processData.getDate("EFF_DATE"), BackOfficeConstants.TBA_DATE_FORMAT);
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("LEASE_PRODUCT_CODE"));
		prodcpconfighist.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		prodcpconfighist.setField("LEASE_PRODUCT_CODE", processData.get("LEASE_PRODUCT_CODE"));
		prodcpconfighist.setField("PORTFOLIO_CODE", processData.get("PORTFOLIO_CODE"));
		prodcpconfighist.setField("EFF_DATE", processData.getDate("EFF_DATE"));
		dataExists = prodcpconfighist.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				prodcpconfighist.setNew(true);
				prodcpconfighist.setField("COST_CP_CENTER_CODE", processData.get("COST_CP_CENTER_CODE"));
				prodcpconfighist.setField("PROFIT_CP_CENTER_CODE", processData.get("PROFIT_CP_CENTER_CODE"));
				prodcpconfighist.setField("ENABLED", processData.get("ENABLED"));
				prodcpconfighist.setField("REMARKS", processData.get("REMARKS"));
				if (prodcpconfighist.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			prodcpconfighist.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				prodcpconfighist.setNew(false);
				prodcpconfighist.setField("COST_CP_CENTER_CODE", processData.get("COST_CP_CENTER_CODE"));
				prodcpconfighist.setField("PROFIT_CP_CENTER_CODE", processData.get("PROFIT_CP_CENTER_CODE"));
				prodcpconfighist.setField("ENABLED", processData.get("ENABLED"));
				prodcpconfighist.setField("REMARKS", processData.get("REMARKS"));
				if (prodcpconfighist.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			prodcpconfighist.close();
		}
	}
}