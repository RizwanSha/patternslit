package patterns.config.framework.bo.cmn;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.BindParameterType;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTDObject;
import patterns.config.framework.web.BackOfficeFormatUtils;

public class ismallacpolicyBO extends GenericBO {
	TBADynaSQL ismallacpolicyhist = null;
	TBADynaSQL ismallacpolicyhistdtl = null;

	public void init() {
		ismallacpolicyhist = new TBADynaSQL("SMALLACPOLICYHIST", true);
		ismallacpolicyhistdtl = new TBADynaSQL("SMALLACPOLICYHISTDTL", false);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("KYC_CATEGORY") + RegularConstants.PK_SEPARATOR + processData.get("CCY_CODE") + RegularConstants.PK_SEPARATOR + BackOfficeFormatUtils.getDate(processData.getDate("EFF_DATE"), BackOfficeConstants.TBA_DATE_FORMAT);
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("KYC_CATEGORY"));
		ismallacpolicyhist.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		ismallacpolicyhist.setField("KYC_CATEGORY", processData.get("KYC_CATEGORY"));
		ismallacpolicyhist.setField("CCY_CODE", processData.get("CCY_CODE"));
		ismallacpolicyhist.setField("EFF_DATE", processData.getDate("EFF_DATE"));
		dataExists = ismallacpolicyhist.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				ismallacpolicyhist.setNew(true);
				ismallacpolicyhist.setField("DOC_REF_NUMBER", processData.get("DOC_REF_NUMBER"));
				ismallacpolicyhist.setField("DOC_DATE", processData.getDate("DOC_DATE"));
				ismallacpolicyhist.setField("APPLY_AGGR_CONTROLS", processData.get("APPLY_AGGR_CONTROLS"));
				ismallacpolicyhist.setField("MAX_BALANCE_ALLOWED", processData.get("MAX_BALANCE_ALLOWED"));
				ismallacpolicyhist.setField("MAX_AMT_CR_TRAN", processData.get("MAX_AMT_CR_TRAN"));
				ismallacpolicyhist.setField("MAX_AMT_DR_TRAN", processData.get("MAX_AMT_DR_TRAN"));
				ismallacpolicyhist.setField("IN_FOREX_REM_ALLOWED", processData.get("IN_FOREX_REM_ALLOWED"));
				ismallacpolicyhist.setField("OUT_FOREX_REM_ALLOWED", processData.get("OUT_FOREX_REM_ALLOWED"));
				ismallacpolicyhist.setField("ENABLED", processData.get("ENABLED"));
				ismallacpolicyhist.setField("REMARKS", processData.get("REMARKS"));
				if (ismallacpolicyhist.save()) {
					if (processData.get("APPLY_AGGR_CONTROLS").equals(RegularConstants.ONE)) {
						ismallacpolicyhistdtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
						ismallacpolicyhistdtl.setField("KYC_CATEGORY", processData.get("KYC_CATEGORY"));
						ismallacpolicyhistdtl.setField("CCY_CODE", processData.get("CCY_CODE"));
						ismallacpolicyhistdtl.setField("EFF_DATE", processData.getDate("EFF_DATE"));
						DTDObject detailData = processData.getDTDObject("SMALLACPOLICYHISTDTL");
						ismallacpolicyhistdtl.setNew(true);
						for (int i = 0; i < detailData.getRowCount(); ++i) {
							ismallacpolicyhistdtl.setField("DTL_SL", Integer.valueOf(i + 1).toString());
							ismallacpolicyhistdtl.setField("TRAN_TYPE_DC", detailData.getValue(i, 6));
							ismallacpolicyhistdtl.setField("FOR_PERIOD", detailData.getValue(i, 7));
							ismallacpolicyhistdtl.setField("YEAR_CONSIDER", detailData.getValue(i, 8));
							ismallacpolicyhistdtl.setField("AGGREGATION_LIMIT", detailData.getValue(i, 9));
							ismallacpolicyhistdtl.setField("CONSIDER_SYS_POST_TRAN", detailData.getValue(i, 10));
							if (!ismallacpolicyhistdtl.save()) {
								processResult.setProcessStatus(TBAProcessStatus.FAILURE);
								processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
								return;
							}
						}
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			ismallacpolicyhist.close();
			ismallacpolicyhistdtl.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				ismallacpolicyhist.setNew(false);
				ismallacpolicyhist.setField("DOC_REF_NUMBER", processData.get("DOC_REF_NUMBER"));
				ismallacpolicyhist.setField("DOC_DATE", processData.getDate("DOC_DATE"));
				ismallacpolicyhist.setField("APPLY_AGGR_CONTROLS", processData.get("APPLY_AGGR_CONTROLS"));
				ismallacpolicyhist.setField("MAX_BALANCE_ALLOWED", processData.get("MAX_BALANCE_ALLOWED"));
				ismallacpolicyhist.setField("MAX_AMT_CR_TRAN", processData.get("MAX_AMT_CR_TRAN"));
				ismallacpolicyhist.setField("MAX_AMT_DR_TRAN", processData.get("MAX_AMT_DR_TRAN"));
				ismallacpolicyhist.setField("IN_FOREX_REM_ALLOWED", processData.get("IN_FOREX_REM_ALLOWED"));
				ismallacpolicyhist.setField("OUT_FOREX_REM_ALLOWED", processData.get("OUT_FOREX_REM_ALLOWED"));
				ismallacpolicyhist.setField("ENABLED", processData.get("ENABLED"));
				ismallacpolicyhist.setField("REMARKS", processData.get("REMARKS"));
				if (ismallacpolicyhist.save()) {
					if (processData.get("APPLY_AGGR_CONTROLS").equals(RegularConstants.ONE)) {
						String[] ismallAccountPolicyFields = { "ENTITY_CODE", "KYC_CATEGORY", "CCY_CODE", "EFF_DATE", "DTL_SL" };
						BindParameterType[] ismallaccountPolicyTypes = { BindParameterType.BIGINT, BindParameterType.VARCHAR, BindParameterType.VARCHAR, BindParameterType.DATE, BindParameterType.BIGINT };
						ismallacpolicyhistdtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
						ismallacpolicyhistdtl.setField("KYC_CATEGORY", processData.get("KYC_CATEGORY"));
						ismallacpolicyhistdtl.setField("CCY_CODE", processData.get("CCY_CODE"));
						ismallacpolicyhistdtl.setField("EFF_DATE", processData.getDate("EFF_DATE"));
						ismallacpolicyhistdtl.deleteByFieldsAudit(ismallAccountPolicyFields, ismallaccountPolicyTypes);
						DTDObject detailData = processData.getDTDObject("SMALLACPOLICYHISTDTL");
						ismallacpolicyhistdtl.setNew(true);
						for (int i = 0; i < detailData.getRowCount(); ++i) {
							ismallacpolicyhistdtl.setField("DTL_SL", Integer.valueOf(i + 1).toString());
							ismallacpolicyhistdtl.setField("TRAN_TYPE_DC", detailData.getValue(i, 6));
							ismallacpolicyhistdtl.setField("FOR_PERIOD", detailData.getValue(i, 7));
							ismallacpolicyhistdtl.setField("YEAR_CONSIDER", detailData.getValue(i, 8));
							ismallacpolicyhistdtl.setField("AGGREGATION_LIMIT", detailData.getValue(i, 9));
							ismallacpolicyhistdtl.setField("CONSIDER_SYS_POST_TRAN", detailData.getValue(i, 10));
							if (!ismallacpolicyhistdtl.save()) {
								processResult.setProcessStatus(TBAProcessStatus.FAILURE);
								processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
								return;
							}
						}
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}

		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			ismallacpolicyhist.close();
			ismallacpolicyhistdtl.close();
		}
	}

}
