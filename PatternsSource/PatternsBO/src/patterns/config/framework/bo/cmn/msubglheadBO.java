package patterns.config.framework.bo.cmn;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;

/**
 * 
 * 
 * @version 1.0
 * @author P Vishnu
 * @since 28-Jan-2017 <br>
 *        <b>Modified History</b> <BR>
 *        <U><B> Sl.No Modified Date Author Modified Changes Version </B></U> <br>
 * 
 * 
 */

public class msubglheadBO extends GenericBO {

	TBADynaSQL msubglhead = null;

	public msubglheadBO() {
	}

	public void init() {
		msubglhead = new TBADynaSQL("SUBGLHEAD", true);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("GL_HEAD_CODE") + RegularConstants.PK_SEPARATOR + processData.get("SUB_GL_HEAD_CODE");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("DESCRIPTION"));
		msubglhead.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		msubglhead.setField("GL_HEAD_CODE", processData.get("GL_HEAD_CODE"));
		msubglhead.setField("SUB_GL_HEAD_CODE", processData.get("SUB_GL_HEAD_CODE"));
		dataExists = msubglhead.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				msubglhead.setNew(true);
				msubglhead.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				msubglhead.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				msubglhead.setField("CUST_SEGMENT_CODE", processData.get("CUST_SEGMENT_CODE"));
				msubglhead.setField("PORTFOLIO_CODE", processData.get("PORTFOLIO_CODE"));
				msubglhead.setField("ENABLED", processData.get("ENABLED"));
				msubglhead.setField("REMARKS", processData.get("REMARKS"));
				if (msubglhead.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			msubglhead.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				msubglhead.setNew(false);
				msubglhead.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				msubglhead.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				msubglhead.setField("CUST_SEGMENT_CODE", processData.get("CUST_SEGMENT_CODE"));
				msubglhead.setField("PORTFOLIO_CODE", processData.get("PORTFOLIO_CODE"));
				msubglhead.setField("ENABLED", processData.get("ENABLED"));
				msubglhead.setField("REMARKS", processData.get("REMARKS"));
				if (msubglhead.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			msubglhead.close();
		}
	}
}
