package patterns.config.framework.bo.cmn;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;

public class mibtrantypesBO extends GenericBO {
	TBADynaSQL mibtrantypes = null;

	public void init() {
		mibtrantypes = new TBADynaSQL("INTERBANKTRANTYPES", true);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("INTER_BANK_TRAN_TYPES");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("DESCRIPTION"));
		mibtrantypes.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		mibtrantypes.setField("INTER_BANK_TRAN_TYPES", processData.get("INTER_BANK_TRAN_TYPES"));
		dataExists = mibtrantypes.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				mibtrantypes.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				mibtrantypes.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				mibtrantypes.setField("LIABILITY", processData.get("LIABILITY"));
				mibtrantypes.setField("ORIGINATING_DEBITS_ALLOWED", processData.get("ORIGINATING_DEBITS_ALLOWED"));
				mibtrantypes.setField("NUMBERING_REQUIRED", processData.get("NUMBERING_REQUIRED"));
				mibtrantypes.setField("MANUAL_NUMBERS_ALLOWED", processData.get("MANUAL_NUMBERS_ALLOWED"));
				mibtrantypes.setField("ENABLED", processData.get("ENABLED"));
				mibtrantypes.setField("REMARKS", processData.get("REMARKS"));
				mibtrantypes.setNew(true);
				if (mibtrantypes.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mibtrantypes.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				mibtrantypes.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				mibtrantypes.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				mibtrantypes.setField("LIABILITY", processData.get("LIABILITY"));
				mibtrantypes.setField("ORIGINATING_DEBITS_ALLOWED", processData.get("ORIGINATING_DEBITS_ALLOWED"));
				mibtrantypes.setField("NUMBERING_REQUIRED", processData.get("NUMBERING_REQUIRED"));
				mibtrantypes.setField("MANUAL_NUMBERS_ALLOWED", processData.get("MANUAL_NUMBERS_ALLOWED"));
				mibtrantypes.setField("ENABLED", processData.get("ENABLED"));
				mibtrantypes.setField("REMARKS", processData.get("REMARKS"));
				mibtrantypes.setNew(false);
				if (mibtrantypes.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mibtrantypes.close();
		}
	}

}
