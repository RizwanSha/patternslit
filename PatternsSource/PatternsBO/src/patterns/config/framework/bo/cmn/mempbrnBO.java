/*
 *
 Author : ARULPRASATH A
 Created Date : 17-AUG-2016
 Spec Reference : PPBS/CMN/0023-MEMPBRN - Employee Branch (1-Master, Abstraction Program)
 Modification History
 ----------------------------------------------------------------------
 Sl.No		Modified Date	Author			Modified Changes	Version
 ----------------------------------------------------------------------
 
 */


package patterns.config.framework.bo.cmn;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.web.BackOfficeFormatUtils;

public class mempbrnBO extends GenericBO {

	TBADynaSQL mempbrn = null;

	public mempbrnBO() {
	}

	public void init() {
		mempbrn = new TBADynaSQL("EMPBRNHIST", true);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("EMP_CODE") + RegularConstants.PK_SEPARATOR + BackOfficeFormatUtils.getDate(processData.getDate("EFF_DATE"), BackOfficeConstants.TBA_DATE_FORMAT);
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("BRANCH_CODE"));
		mempbrn.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		mempbrn.setField("EMP_CODE", processData.get("EMP_CODE"));
		mempbrn.setField("EFF_DATE", processData.getDate("EFF_DATE"));
		dataExists = mempbrn.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				mempbrn.setNew(true);
				mempbrn.setField("BRANCH_CODE", processData.get("BRANCH_CODE"));
				mempbrn.setField("ENABLED", processData.get("ENABLED"));
				mempbrn.setField("REMARKS", processData.get("REMARKS"));
				if (mempbrn.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mempbrn.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				mempbrn.setNew(false);
				mempbrn.setField("BRANCH_CODE", processData.get("BRANCH_CODE"));
				mempbrn.setField("ENABLED", processData.get("ENABLED"));
				mempbrn.setField("REMARKS", processData.get("REMARKS"));
				if (mempbrn.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mempbrn.close();
		}
	}
}
