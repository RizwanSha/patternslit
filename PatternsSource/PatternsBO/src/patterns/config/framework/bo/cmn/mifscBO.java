package patterns.config.framework.bo.cmn;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;

public class mifscBO extends GenericBO {

	TBADynaSQL mifsc = null;

	public mifscBO() {
	}

	public void init() {
		mifsc = new TBADynaSQL("IFSC", true);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("IFSC_CODE");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("IFSC_CODE"));
		mifsc.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		mifsc.setField("IFSC_CODE", processData.get("IFSC_CODE"));
		dataExists = mifsc.loadByKeyFields();
	}
	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				mifsc.setNew(true);
				mifsc.setField("BANK_NAME", processData.get("BANK_NAME"));
				mifsc.setField("BRANCH_NAME", processData.get("BRANCH_NAME"));
				mifsc.setField("ADDRESS", processData.get("ADDRESS"));
				mifsc.setField("MICR_CODE", processData.get("MICR_CODE"));
				mifsc.setField("CITY", processData.get("CITY"));
				mifsc.setField("STATE_CODE",processData.get("STATE_CODE"));
				mifsc.setField("ENABLED", processData.get("ENABLED"));
				mifsc.setField("REMARKS", processData.get("REMARKS"));
				if (mifsc.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mifsc.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				mifsc.setNew(false);
				mifsc.setField("BANK_NAME", processData.get("BANK_NAME"));
				mifsc.setField("BRANCH_NAME", processData.get("BRANCH_NAME"));
				mifsc.setField("ADDRESS", processData.get("ADDRESS"));
				mifsc.setField("MICR_CODE", processData.get("MICR_CODE"));
				mifsc.setField("CITY", processData.get("CITY"));
				mifsc.setField("STATE_CODE",processData.get("STATE_CODE"));
				mifsc.setField("ENABLED", processData.get("ENABLED"));
				mifsc.setField("REMARKS", processData.get("REMARKS"));
				if (mifsc.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			}else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mifsc.close();
		}
	}

}
