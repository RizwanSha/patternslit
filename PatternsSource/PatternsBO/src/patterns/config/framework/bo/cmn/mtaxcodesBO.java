package patterns.config.framework.bo.cmn;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;

public class mtaxcodesBO extends GenericBO {

	TBADynaSQL mtaxcodes = null;

	public mtaxcodesBO() {
	}

	public void init() {
		mtaxcodes = new TBADynaSQL("TAXCODES", true);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("TAX_CODE");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("DESCRIPTION"));
		mtaxcodes.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		mtaxcodes.setField("TAX_CODE", processData.get("TAX_CODE"));
		dataExists = mtaxcodes.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				mtaxcodes.setNew(true);
				mtaxcodes.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				mtaxcodes.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				mtaxcodes.setField("TAX_ON_CUST_INCOME", processData.get("TAX_ON_CUST_INCOME"));
				mtaxcodes.setField("TAX_ON_BANK_INCOME", processData.get("TAX_ON_BANK_INCOME"));
				mtaxcodes.setField("ADDNL_SURCHARGE_CESS", processData.get("ADDNL_SURCHARGE_CESS"));
				mtaxcodes.setField("BASE_TAX_CODE", processData.get("BASE_TAX_CODE"));
				mtaxcodes.setField("TAX_APPLICABILITY", processData.get("TAX_APPLICABILITY"));
				mtaxcodes.setField("CUT_OFF_AMT_PERIOD", processData.get("CUT_OFF_AMT_PERIOD"));
				mtaxcodes.setField("TAX_CUT_OFF_POLICY", processData.get("TAX_CUT_OFF_POLICY"));
				mtaxcodes.setField("TAX_CUT_OFF_AMT_AGGR", processData.get("TAX_CUT_OFF_AMT_AGGR"));
				mtaxcodes.setField("TAX_FIN_YEAR_START_MONTH", processData.get("TAX_FIN_YEAR_START_MONTH"));
				mtaxcodes.setField("TAX_ROUND_OFF_CHOICE", processData.get("TAX_ROUND_OFF_CHOICE"));
				mtaxcodes.setField("TAX_ROUND_OFF_FACTOR", processData.get("TAX_ROUND_OFF_FACTOR"));
				mtaxcodes.setField("PAYABLE_GL_HEAD_CODE", processData.get("PAYABLE_GL_HEAD_CODE"));
				mtaxcodes.setField("TAX_REMIT_FREQ", processData.get("TAX_REMIT_FREQ"));
				mtaxcodes.setField("TAX_PAID_BEFORE_DAY", processData.get("TAX_PAID_BEFORE_DAY"));
				mtaxcodes.setField("TAX_REVERSAL_BEFORE_REMIT_ALLOW", processData.get("TAX_REVERSAL_BEFORE_REMIT_ALLOW"));
				mtaxcodes.setField("TAX_AUTHORITY", processData.get("TAX_AUTHORITY"));
				mtaxcodes.setField("TAX_AUTHORITY_NAME", processData.get("TAX_AUTHORITY_NAME"));
				mtaxcodes.setField("TAX_PID_CODE", processData.get("TAX_PID_CODE"));
				mtaxcodes.setField("FORM_DECL_TAX_WAIVED", processData.get("FORM_DECL_TAX_WAIVED"));
				mtaxcodes.setField("FORM_NAME_SENCIT", processData.get("FORM_NAME_SENCIT"));
				mtaxcodes.setField("FORM_NAME_OTH", processData.get("FORM_NAME_OTH"));
				mtaxcodes.setField("ENABLED", processData.get("ENABLED"));
				mtaxcodes.setField("REMARKS", processData.get("REMARKS"));
				if (mtaxcodes.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mtaxcodes.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				mtaxcodes.setNew(false);
				mtaxcodes.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				mtaxcodes.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				mtaxcodes.setField("TAX_ON_CUST_INCOME", processData.get("TAX_ON_CUST_INCOME"));
				mtaxcodes.setField("TAX_ON_BANK_INCOME", processData.get("TAX_ON_BANK_INCOME"));
				mtaxcodes.setField("ADDNL_SURCHARGE_CESS", processData.get("ADDNL_SURCHARGE_CESS"));
				mtaxcodes.setField("BASE_TAX_CODE", processData.get("BASE_TAX_CODE"));
				mtaxcodes.setField("TAX_APPLICABILITY", processData.get("TAX_APPLICABILITY"));
				mtaxcodes.setField("CUT_OFF_AMT_PERIOD", processData.get("CUT_OFF_AMT_PERIOD"));
				mtaxcodes.setField("TAX_CUT_OFF_POLICY", processData.get("TAX_CUT_OFF_POLICY"));
				mtaxcodes.setField("TAX_CUT_OFF_AMT_AGGR", processData.get("TAX_CUT_OFF_AMT_AGGR"));
				mtaxcodes.setField("TAX_FIN_YEAR_START_MONTH", processData.get("TAX_FIN_YEAR_START_MONTH"));
				mtaxcodes.setField("TAX_ROUND_OFF_CHOICE", processData.get("TAX_ROUND_OFF_CHOICE"));
				mtaxcodes.setField("TAX_ROUND_OFF_FACTOR", processData.get("TAX_ROUND_OFF_FACTOR"));
				mtaxcodes.setField("PAYABLE_GL_HEAD_CODE", processData.get("PAYABLE_GL_HEAD_CODE"));
				mtaxcodes.setField("TAX_REMIT_FREQ", processData.get("TAX_REMIT_FREQ"));
				mtaxcodes.setField("TAX_PAID_BEFORE_DAY", processData.get("TAX_PAID_BEFORE_DAY"));
				mtaxcodes.setField("TAX_REVERSAL_BEFORE_REMIT_ALLOW", processData.get("TAX_REVERSAL_BEFORE_REMIT_ALLOW"));
				mtaxcodes.setField("TAX_AUTHORITY", processData.get("TAX_AUTHORITY"));
				mtaxcodes.setField("TAX_AUTHORITY_NAME", processData.get("TAX_AUTHORITY_NAME"));
				mtaxcodes.setField("TAX_PID_CODE", processData.get("TAX_PID_CODE"));
				mtaxcodes.setField("FORM_DECL_TAX_WAIVED", processData.get("FORM_DECL_TAX_WAIVED"));
				mtaxcodes.setField("FORM_NAME_SENCIT", processData.get("FORM_NAME_SENCIT"));
				mtaxcodes.setField("FORM_NAME_OTH", processData.get("FORM_NAME_OTH"));
				mtaxcodes.setField("ENABLED", processData.get("ENABLED"));
				mtaxcodes.setField("REMARKS", processData.get("REMARKS"));
				if (mtaxcodes.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mtaxcodes.close();
		}
	}
}
