package patterns.config.framework.bo.cmn;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.web.ajax.ContentManager;

public class mcpcentercodeBO extends GenericBO {

	TBADynaSQL mcpcentercode = null;

	public mcpcentercodeBO() {
	}

	public void init() {
		mcpcentercode = new TBADynaSQL("CPCENTERCODE", true);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("CP_CENTER_CODE");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("CP_CENTER_CODE"));
		mcpcentercode.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		mcpcentercode.setField("CP_CENTER_CODE", processData.get("CP_CENTER_CODE"));
		dataExists = mcpcentercode.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				mcpcentercode.setNew(true);
				mcpcentercode.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				mcpcentercode.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				mcpcentercode.setField("PROFIT_COST", processData.get("PROFIT_COST"));
				mcpcentercode.setField("ENABLED", processData.get("ENABLED"));
				mcpcentercode.setField("REMARKS", processData.get("REMARKS"));
				if (mcpcentercode.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}

			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mcpcentercode.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				mcpcentercode.setNew(false);
				mcpcentercode.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				mcpcentercode.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				mcpcentercode.setField("PROFIT_COST", processData.get("PROFIT_COST"));
				mcpcentercode.setField("ENABLED", processData.get("ENABLED"));
				mcpcentercode.setField("REMARKS", processData.get("REMARKS"));
				if (mcpcentercode.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mcpcentercode.close();
		}
	}
}