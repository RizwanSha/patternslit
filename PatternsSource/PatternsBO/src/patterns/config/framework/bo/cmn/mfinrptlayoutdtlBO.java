package patterns.config.framework.bo.cmn;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.BindParameterType;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTDObject;
import patterns.config.framework.web.BackOfficeFormatUtils;

public class mfinrptlayoutdtlBO extends GenericBO {

	TBADynaSQL mfinrptlayout = null;
	TBADynaSQL mfinrptlayoutdtl = null;

	public mfinrptlayoutdtlBO() {
	}

	public void init() {
		mfinrptlayout = new TBADynaSQL("FINRPTLAYOUTCONFHIST", true);
		mfinrptlayoutdtl = new TBADynaSQL("FINRPTLAYOUTCONFHISTDTL", false);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("REPORT_LAYOUT_CODE") + RegularConstants.PK_SEPARATOR + processData.get("REPORT_SECTION_SL") + RegularConstants.PK_SEPARATOR + BackOfficeFormatUtils.getDate(processData.getDate("EFF_DATE"), BackOfficeConstants.TBA_DATE_FORMAT);
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("REPORT_LAYOUT_CODE"));
		mfinrptlayout.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		mfinrptlayout.setField("REPORT_LAYOUT_CODE", processData.get("REPORT_LAYOUT_CODE"));
		mfinrptlayout.setField("REPORT_SECTION_SL", processData.get("REPORT_SECTION_SL"));
		mfinrptlayout.setField("EFF_DATE", processData.getDate("EFF_DATE"));
		dataExists = mfinrptlayout.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				mfinrptlayout.setNew(true);
				mfinrptlayout.setField("ENABLED", processData.get("ENABLED"));
				mfinrptlayout.setField("REMARKS", processData.get("REMARKS"));
				if (mfinrptlayout.save()) {
					mfinrptlayoutdtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
					mfinrptlayoutdtl.setField("REPORT_LAYOUT_CODE", processData.get("REPORT_LAYOUT_CODE"));
					mfinrptlayoutdtl.setField("REPORT_SECTION_SL", processData.get("REPORT_SECTION_SL"));
					mfinrptlayoutdtl.setField("EFF_DATE", processData.getDate("EFF_DATE"));
					DTDObject detailData = processData.getDTDObject("FINRPTLAYOUTCONFHISTDTL");
					mfinrptlayoutdtl.setNew(true);
					for (int i = 0; i < detailData.getRowCount(); ++i) {
						mfinrptlayoutdtl.setField("DTL_SL", String.valueOf(i + 1));
						mfinrptlayoutdtl.setField("REPORT_ELEMENT_CODE", detailData.getValue(i, 1));
						if (!mfinrptlayoutdtl.save()) {
							processResult.setProcessStatus(TBAProcessStatus.FAILURE);
							processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
						}
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mfinrptlayout.close();
			mfinrptlayoutdtl.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				mfinrptlayout.setNew(false);
				mfinrptlayout.setField("ENABLED", processData.get("ENABLED"));
				mfinrptlayout.setField("REMARKS", processData.get("REMARKS"));
				if (mfinrptlayout.save()) {
					String[] mfinrptlayoutdtlFields = { "ENTITY_CODE", "REPORT_LAYOUT_CODE", "REPORT_SECTION_SL", "EFF_DATE", "DTL_SL" };
					BindParameterType[] mfinrptlayoutdtlFieldTypes = { BindParameterType.BIGINT, BindParameterType.VARCHAR, BindParameterType.VARCHAR, BindParameterType.DATE, BindParameterType.INTEGER };
					mfinrptlayoutdtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
					mfinrptlayoutdtl.setField("REPORT_LAYOUT_CODE", processData.get("REPORT_LAYOUT_CODE"));
					mfinrptlayoutdtl.setField("REPORT_SECTION_SL", processData.get("REPORT_SECTION_SL"));
					mfinrptlayoutdtl.setField("EFF_DATE", processData.getDate("EFF_DATE"));
					mfinrptlayoutdtl.setField("DTL_SL", processData.get("DTL_SL"));
					mfinrptlayoutdtl.deleteByFieldsAudit(mfinrptlayoutdtlFields, mfinrptlayoutdtlFieldTypes);
					if (processData.containsKey("FINRPTLAYOUTCONFHISTDTL")) {
						DTDObject detailData = processData.getDTDObject("FINRPTLAYOUTCONFHISTDTL");
						mfinrptlayoutdtl.setNew(true);
						for (int i = 0; i < detailData.getRowCount(); ++i) {
							mfinrptlayoutdtl.setField("DTL_SL", String.valueOf(i + 1));
							mfinrptlayoutdtl.setField("REPORT_ELEMENT_CODE", detailData.getValue(i, 1));
							if (!mfinrptlayoutdtl.save()) {
								processResult.setProcessStatus(TBAProcessStatus.FAILURE);
								processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
							}
						}
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mfinrptlayout.close();
			mfinrptlayoutdtl.close();
		}
	}
}
