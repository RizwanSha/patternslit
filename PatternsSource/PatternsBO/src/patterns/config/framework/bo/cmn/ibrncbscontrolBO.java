package patterns.config.framework.bo.cmn;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;

public class ibrncbscontrolBO extends GenericBO {
	TBADynaSQL ibrncbscontrol = null;
	public ibrncbscontrolBO() {
		
	}

	public void init() {		
		ibrncbscontrol = new TBADynaSQL("BRNCBSCONTROL", true);
		primaryKey = processData.get("ENTITY_CODE")+RegularConstants.PK_SEPARATOR + processData.get("BRANCH_CODE");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("BRANCH_CODE"));
		ibrncbscontrol.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		ibrncbscontrol.setField("BRANCH_CODE", processData.get("BRANCH_CODE"));
		dataExists = ibrncbscontrol.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				ibrncbscontrol.setNew(true);
				ibrncbscontrol.setField("IN_CBS_FROM_DATE", processData.getDate("IN_CBS_FROM_DATE"));
				ibrncbscontrol.setField("TRAN_MIGRATED_FROM_DATE", processData.getDate("TRAN_MIGRATED_FROM_DATE"));
				ibrncbscontrol.setField("CURRENT_CBS_LINK_STATUS", processData.get("CURRENT_CBS_LINK_STATUS"));
				ibrncbscontrol.setField("ENABLED", processData.get("ENABLED"));
				ibrncbscontrol.setField("REMARKS", processData.get("REMARKS"));
				
				if (ibrncbscontrol.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			ibrncbscontrol.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				ibrncbscontrol.setNew(false);
				ibrncbscontrol.setField("IN_CBS_FROM_DATE", processData.getDate("IN_CBS_FROM_DATE"));
				ibrncbscontrol.setField("TRAN_MIGRATED_FROM_DATE", processData.getDate("TRAN_MIGRATED_FROM_DATE"));
				ibrncbscontrol.setField("CURRENT_CBS_LINK_STATUS", processData.get("CURRENT_CBS_LINK_STATUS"));
				ibrncbscontrol.setField("ENABLED", processData.get("ENABLED"));
				ibrncbscontrol.setField("REMARKS", processData.get("REMARKS"));
				if (ibrncbscontrol.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			ibrncbscontrol.close();
		}
	}
}
