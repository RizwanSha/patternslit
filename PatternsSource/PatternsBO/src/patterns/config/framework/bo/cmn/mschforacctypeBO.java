/*
 *
 Author : Raja E
 Created Date : 01-07-2016
 Spec Reference : PPBS/CMN/0013-MSCHFORACCTYPE
 Modification History
 ----------------------------------------------------------------------
 Sl.No		Modified Date	Author			Modified Changes	Version
 ----------------------------------------------------------------------
 
 */

package patterns.config.framework.bo.cmn;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.BindParameterType;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTDObject;
import patterns.config.framework.web.BackOfficeFormatUtils;

public class mschforacctypeBO extends GenericBO {

	TBADynaSQL schforacctypehist = null;
	TBADynaSQL schforacctypehistdtl = null;

	public mschforacctypeBO() {
	}

	public void init() throws TBAFrameworkException {
		// TODO Auto-generated method stub
		schforacctypehist = new TBADynaSQL("SCHFORACCTYPEHIST", true);
		schforacctypehistdtl = new TBADynaSQL("SCHFORACCTYPEHISTDTL", false);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("ACCOUNT_TYPE_CODE") + RegularConstants.PK_SEPARATOR + BackOfficeFormatUtils.getDate(processData.getDate("EFF_DATE"), BackOfficeConstants.TBA_DATE_FORMAT);
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("ACCOUNT_TYPE_CODE"));
		schforacctypehist.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		schforacctypehist.setField("ACCOUNT_TYPE_CODE", processData.get("ACCOUNT_TYPE_CODE"));
		schforacctypehist.setField("EFF_DATE", processData.getDate("EFF_DATE"));
		dataExists = schforacctypehist.loadByKeyFields();
	}

	@Override
	protected void add() throws TBAFrameworkException {
		// TODO Auto-generated method stub
		try {
			if (!isDataExists()) {
				schforacctypehist.setField("ENABLED", processData.get("ENABLED"));
				schforacctypehist.setField("REMARKS", processData.get("REMARKS"));
				schforacctypehist.setNew(true);
				if (schforacctypehist.save()) {
					schforacctypehistdtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
					schforacctypehistdtl.setField("ACCOUNT_TYPE_CODE", processData.get("ACCOUNT_TYPE_CODE"));
					schforacctypehistdtl.setField("EFF_DATE", processData.getDate("EFF_DATE"));
					DTDObject detailData = processData.getDTDObject("SCHFORACCTYPEHISTDTL");
					schforacctypehistdtl.setNew(true);
					int k=0;
					for (int i = 0; i < detailData.getRowCount(); ++i) {
						if (detailData.getValue(i, 0).equals("1")) {
							k=k+1;
							schforacctypehistdtl.setField("SL", Integer.valueOf(k).toString());
							schforacctypehistdtl.setField("SCHEME_CODE", detailData.getValue(i, 1));
							if (!schforacctypehistdtl.save()) {
								processResult.setProcessStatus(TBAProcessStatus.FAILURE);
								processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
								return;
							}
						}
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			schforacctypehist.close();
			schforacctypehistdtl.close();
		}
	}

	@Override
	protected void modify() throws TBAFrameworkException {
		// TODO Auto-generated method stub
		try {
			if (isDataExists()) {
				schforacctypehist.setField("ENABLED", processData.get("ENABLED"));
				schforacctypehist.setField("REMARKS", processData.get("REMARKS"));
				schforacctypehist.setNew(false);
				if (schforacctypehist.save()) {
					String[] doctweeklyoffhistdtlFields = { "ENTITY_CODE", "ACCOUNT_TYPE_CODE", "EFF_DATE", "SL" };
					BindParameterType[] doctweeklyoffhistdtlFieldTypes = { BindParameterType.BIGINT, BindParameterType.VARCHAR, BindParameterType.DATE, BindParameterType.BIGINT };
					schforacctypehistdtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
					schforacctypehistdtl.setField("ACCOUNT_TYPE_CODE", processData.get("ACCOUNT_TYPE_CODE"));
					schforacctypehistdtl.setField("EFF_DATE", processData.getDate("EFF_DATE"));
					schforacctypehistdtl.deleteByFieldsAudit(doctweeklyoffhistdtlFields, doctweeklyoffhistdtlFieldTypes);
					DTDObject detailData = processData.getDTDObject("SCHFORACCTYPEHISTDTL");
					schforacctypehistdtl.setNew(true);
					int k=0;
					for (int i = 0; i < detailData.getRowCount(); ++i) {
						if (detailData.getValue(i, 0).equals("1")) {
							k=k+1;
							schforacctypehistdtl.setField("SL", Integer.valueOf(k).toString());
							schforacctypehistdtl.setField("SCHEME_CODE", detailData.getValue(i, 1));
							if (!schforacctypehistdtl.save()) {
								processResult.setProcessStatus(TBAProcessStatus.FAILURE);
								processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
								return;
							}
						}
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			schforacctypehist.close();
			schforacctypehistdtl.close();
		}
	}
}
