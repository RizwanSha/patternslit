package patterns.config.framework.bo.cmn;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.BindParameterType;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTDObject;
import patterns.config.framework.web.BackOfficeFormatUtils;

public class itaxschrateBO extends GenericBO {

	TBADynaSQL itaxschrate = null;
	TBADynaSQL itaxschratedtl = null;

	public itaxschrateBO() {
	}

	public void init() {
		itaxschrate = new TBADynaSQL("TAXRATEHIST", true);
		itaxschratedtl = new TBADynaSQL("TAXRATEDTLHIST", false);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("STATE_CODE") + RegularConstants.PK_SEPARATOR + processData.get("TAX_CODE") + RegularConstants.PK_SEPARATOR + BackOfficeFormatUtils.getDate(processData.getDate("EFF_DATE"), BackOfficeConstants.TBA_DATE_FORMAT);
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("STATE_CODE"));
		itaxschrate.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		itaxschrate.setField("STATE_CODE", processData.get("STATE_CODE"));
		itaxschrate.setField("TAX_CODE", processData.get("TAX_CODE"));
		itaxschrate.setField("EFF_DATE", processData.getDate("EFF_DATE"));
		dataExists = itaxschrate.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				itaxschrate.setNew(true);
				itaxschrate.setField("TAX_ON_AMT_PORTION", processData.get("TAX_ON_AMT_PORTION"));
				itaxschrate.setField("ENABLED", processData.get("ENABLED"));
				itaxschrate.setField("REMARKS", processData.get("REMARKS"));
				if (itaxschrate.save()) {
					itaxschratedtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
					itaxschratedtl.setField("STATE_CODE", processData.get("STATE_CODE"));
					itaxschratedtl.setField("TAX_CODE", processData.get("TAX_CODE"));
					itaxschratedtl.setField("EFF_DATE", processData.getDate("EFF_DATE"));
					DTDObject detailData = processData.getDTDObject("TAXRATEDTLHIST");
					itaxschratedtl.setNew(true);
					for (int i = 0; i < detailData.getRowCount(); ++i) {
						itaxschratedtl.setField("DTL_SL", String.valueOf(i + 1));
						itaxschratedtl.setField("TAX_SCHEDULE_CODE", detailData.getValue(i, 1));
						itaxschratedtl.setField("TAX_RATE", detailData.getValue(i, 2));
						if (!itaxschratedtl.save()) {
							processResult.setProcessStatus(TBAProcessStatus.FAILURE);
							processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
						}
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			itaxschrate.close();
			itaxschratedtl.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				itaxschrate.setNew(false);
				itaxschrate.setField("TAX_ON_AMT_PORTION", processData.get("TAX_ON_AMT_PORTION"));
				itaxschrate.setField("ENABLED", processData.get("ENABLED"));
				itaxschrate.setField("REMARKS", processData.get("REMARKS"));
				if (itaxschrate.save()) {
					String[] itaxschratedtlFields = { "ENTITY_CODE", "STATE_CODE", "TAX_CODE", "EFF_DATE", "DTL_SL" };
					BindParameterType[] itaxschratedtlFieldTypes = { BindParameterType.BIGINT, BindParameterType.VARCHAR, BindParameterType.VARCHAR, BindParameterType.DATE, BindParameterType.INTEGER };
					itaxschratedtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
					itaxschratedtl.setField("STATE_CODE", processData.get("STATE_CODE"));
					itaxschratedtl.setField("TAX_CODE", processData.get("TAX_CODE"));
					itaxschratedtl.setField("EFF_DATE", processData.getDate("EFF_DATE"));
					itaxschratedtl.deleteByFieldsAudit(itaxschratedtlFields, itaxschratedtlFieldTypes);
					DTDObject detailData = processData.getDTDObject("TAXRATEDTLHIST");
					itaxschratedtl.setNew(true);
					for (int i = 0; i < detailData.getRowCount(); ++i) {
						itaxschratedtl.setField("DTL_SL", String.valueOf(i + 1));
						itaxschratedtl.setField("TAX_SCHEDULE_CODE", detailData.getValue(i, 1));
						itaxschratedtl.setField("TAX_RATE", detailData.getValue(i, 2));
						if (!itaxschratedtl.save()) {
							processResult.setProcessStatus(TBAProcessStatus.FAILURE);
							processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
						}
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			itaxschrate.close();
			itaxschratedtl.close();
		}
	}
}
