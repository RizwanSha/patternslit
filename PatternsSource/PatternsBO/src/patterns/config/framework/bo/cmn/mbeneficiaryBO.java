

package patterns.config.framework.bo.cmn;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;

public class mbeneficiaryBO extends GenericBO {

	TBADynaSQL mbeneficiary = null;

	public mbeneficiaryBO() {
	}

	public void init() {
		mbeneficiary = new TBADynaSQL("BENEFICIARY", true);
		primaryKey = processData.get("ENTITY_CODE")+ RegularConstants.PK_SEPARATOR +processData.get("BENEFICIARY_ID");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("BENEFICIARY_NAME"));
		mbeneficiary.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		mbeneficiary.setField("BENEFICIARY_ID", processData.get("BENEFICIARY_ID"));
		dataExists = mbeneficiary.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				mbeneficiary.setNew(true);
				mbeneficiary.setField("BENEFICIARY_NAME", processData.get("BENEFICIARY_NAME"));
				mbeneficiary.setField("BENEFICIARY_ADDRESS", processData.get("BENEFICIARY_ADDRESS"));
				mbeneficiary.setField("BENEFICIARY_ACCOUNT_NO", processData.get("BENEFICIARY_AC_NO"));
				mbeneficiary.setField("BENEFICIARY_ACCOUNT_TYPE", processData.get("BENEFICIARY_AC_TYPE"));
				mbeneficiary.setField("BENEFICIARY_IFSC_CODE", processData.get("BENEFICIARY_IFSC_CODE"));
				mbeneficiary.setField("ENABLED", processData.get("ENABLED"));
				mbeneficiary.setField("REMARKS", processData.get("REMARKS"));
				if (mbeneficiary.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mbeneficiary.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				mbeneficiary.setNew(false);
				mbeneficiary.setField("BENEFICIARY_NAME", processData.get("BENEFICIARY_NAME"));
				mbeneficiary.setField("BENEFICIARY_ADDRESS", processData.get("BENEFICIARY_ADDRESS"));
				mbeneficiary.setField("BENEFICIARY_ACCOUNT_NO", processData.get("BENEFICIARY_AC_NO"));
				mbeneficiary.setField("BENEFICIARY_ACCOUNT_TYPE", processData.get("BENEFICIARY_AC_TYPE"));
				mbeneficiary.setField("BENEFICIARY_IFSC_CODE", processData.get("BENEFICIARY_IFSC_CODE"));
				mbeneficiary.setField("ENABLED", processData.get("ENABLED"));
				mbeneficiary.setField("REMARKS", processData.get("REMARKS"));
				if (mbeneficiary.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.HMS_RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mbeneficiary.close();
		}
	}
}