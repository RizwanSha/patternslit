package patterns.config.framework.bo.cmn;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;

public class mgeounittypeBO extends GenericBO {

	TBADynaSQL mgeounittype = null;

	public mgeounittypeBO() {
	}

	public void init() {
		mgeounittype = new TBADynaSQL("GEOUNITTYPE", true);
		primaryKey = processData.get("GEO_UNIT_STRUC_CODE") + RegularConstants.PK_SEPARATOR + processData.get("GEO_UNIT_TYPE");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("DESCRIPTION"));
		mgeounittype.setField("GEO_UNIT_STRUC_CODE", processData.get("GEO_UNIT_STRUC_CODE"));
		mgeounittype.setField("GEO_UNIT_TYPE", processData.get("GEO_UNIT_TYPE"));
		dataExists = mgeounittype.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				mgeounittype.setNew(true);
				mgeounittype.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				mgeounittype.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				mgeounittype.setField("ENABLED", processData.get("ENABLED"));
				mgeounittype.setField("REMARKS", processData.get("REMARKS"));
				if (mgeounittype.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mgeounittype.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				mgeounittype.setNew(false);
				mgeounittype.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				mgeounittype.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				mgeounittype.setField("ENABLED", processData.get("ENABLED"));
				mgeounittype.setField("REMARKS", processData.get("REMARKS"));
				if (mgeounittype.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mgeounittype.close();
		}
	}
}
