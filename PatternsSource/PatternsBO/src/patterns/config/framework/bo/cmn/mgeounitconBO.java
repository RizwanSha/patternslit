package patterns.config.framework.bo.cmn;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;

public class mgeounitconBO extends GenericBO {
	TBADynaSQL mgeounitcon = null;

	public mgeounitconBO() {
	}

	public void init() {
		mgeounitcon = new TBADynaSQL("GEOUNITCON", true);
		primaryKey = processData.get("COUNTRY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("GEO_UNIT_ID");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("DESCRIPTION"));
		mgeounitcon.setField("COUNTRY_CODE", processData.get("COUNTRY_CODE"));
		mgeounitcon.setField("GEO_UNIT_ID", processData.get("GEO_UNIT_ID"));
		dataExists = mgeounitcon.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				mgeounitcon.setNew(true);
				mgeounitcon.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				mgeounitcon.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				mgeounitcon.setField("PARENT_GEO_UNIT_ID", processData.get("PARENT_GEO_UNIT_ID"));
				mgeounitcon.setField("GEO_UNIT_STRUC_CODE", processData.get("GEO_UNIT_STRUC_CODE"));
				mgeounitcon.setField("GEO_UNIT_TYPE", processData.get("GEO_UNIT_TYPE"));
				mgeounitcon.setField("ENABLED", processData.get("ENABLED"));
				mgeounitcon.setField("REMARKS", processData.get("REMARKS"));
				if (mgeounitcon.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mgeounitcon.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				mgeounitcon.setNew(false);
				mgeounitcon.setField("DESCRIPTION", processData.get("DESCRIPTION"));
				mgeounitcon.setField("CONCISE_DESCRIPTION", processData.get("CONCISE_DESCRIPTION"));
				mgeounitcon.setField("PARENT_GEO_UNIT_ID", processData.get("PARENT_GEO_UNIT_ID"));
				mgeounitcon.setField("GEO_UNIT_STRUC_CODE", processData.get("GEO_UNIT_STRUC_CODE"));
				mgeounitcon.setField("GEO_UNIT_TYPE", processData.get("GEO_UNIT_TYPE"));
				mgeounitcon.setField("ENABLED", processData.get("ENABLED"));
				mgeounitcon.setField("REMARKS", processData.get("REMARKS"));
				if (mgeounitcon.save()) {
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mgeounitcon.close();
		}
	}
}