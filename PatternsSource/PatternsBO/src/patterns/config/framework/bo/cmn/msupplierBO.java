package patterns.config.framework.bo.cmn;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;

public class msupplierBO extends GenericBO {
	TBADynaSQL msupplier = null;

	public msupplierBO() {
	}

	public void init() {
		msupplier = new TBADynaSQL("SUPPLIER", true);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("SUPPLIER_ID");
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("NAME"));
		msupplier.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		msupplier.setField("SUPPLIER_ID", processData.get("SUPPLIER_ID"));
		dataExists = msupplier.loadByKeyFields();
	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				msupplier.setNew(true);
				msupplier.setField("NAME", processData.get("NAME"));
				msupplier.setField("SHORT_NAME", processData.get("SHORT_NAME"));
				msupplier.setField("PAN_NO", processData.get("PAN_NO"));
				msupplier.setField("IFSC_CODE", processData.get("IFSC_CODE"));
				msupplier.setField("ENABLED", processData.get("ENABLED"));
				msupplier.setField("REMARKS", processData.get("REMARKS"));
				if (msupplier.save()) {
					msupplier.setField("PURPOSE_ID", "PANNUM");
					msupplier.setField("DEDUP_KEY", processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("PAN_NO"));
					if (!msupplier.udateTBADuplicateCheck()) {
						processResult.setProcessStatus(TBAProcessStatus.FAILURE);
						processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
						throw new TBAFrameworkException();
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			msupplier.close();
		}
	}

	protected void modify() throws TBAFrameworkException {
		try {
			if (isDataExists()) {
				msupplier.setNew(false);
				msupplier.setField("NAME", processData.get("NAME"));
				msupplier.setField("SHORT_NAME", processData.get("SHORT_NAME"));
				msupplier.setField("PAN_NO", processData.get("PAN_NO"));
				msupplier.setField("IFSC_CODE", processData.get("IFSC_CODE"));
				msupplier.setField("ENABLED", processData.get("ENABLED"));
				msupplier.setField("REMARKS", processData.get("REMARKS"));
				if (msupplier.save()) {
					msupplier.setField("PURPOSE_ID", "PANNUM");
					msupplier.setField("DEDUP_KEY", processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("PAN_NO"));
					if (!msupplier.udateTBADuplicateCheck()) {
						processResult.setProcessStatus(TBAProcessStatus.FAILURE);
						processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
						throw new TBAFrameworkException();
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			msupplier.close();
		}
	}
}
