package patterns.config.framework.bo.cmn;

import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.GenericBO;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.BindParameterType;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.TBADynaSQL;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTDObject;
import patterns.config.framework.web.BackOfficeFormatUtils;

public class mprodlifecycleBO extends GenericBO {
	TBADynaSQL mprodlifecycle = null;
	TBADynaSQL mprodlifecycledtl = null;

	public void init() {
		mprodlifecycle = new TBADynaSQL("PRODLIFECYCLEHIST", true);
		mprodlifecycledtl = new TBADynaSQL("PRODLIFECYCLEHISTDTL", false);
		primaryKey = processData.get("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + processData.get("PRODUCT_CODE") + RegularConstants.PK_SEPARATOR + BackOfficeFormatUtils.getDate(processData.getDate("EFF_DATE"), BackOfficeConstants.TBA_DATE_FORMAT);
		tbaContext.setTbaPrimarykey(primaryKey);
		tbaContext.setDisplayDetails(processData.get("PRODUCT_CODE"));
		mprodlifecycle.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
		mprodlifecycle.setField("PRODUCT_CODE", processData.get("PRODUCT_CODE"));
		mprodlifecycle.setField("EFF_DATE", processData.getDate("EFF_DATE"));
		dataExists = mprodlifecycle.loadByKeyFields();

	}

	protected void add() throws TBAFrameworkException {
		try {
			if (!isDataExists()) {
				mprodlifecycle.setNew(true);
				mprodlifecycle.setField("ENABLED", processData.get("ENABLED"));
				mprodlifecycle.setField("REMARKS", processData.get("REMARKS"));
				if (mprodlifecycle.save()) {
					mprodlifecycledtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
					mprodlifecycledtl.setField("PRODUCT_CODE", processData.get("PRODUCT_CODE"));
					mprodlifecycledtl.setField("EFF_DATE", processData.getDate("EFF_DATE"));
					DTDObject detailData = processData.getDTDObject("PRODLIFECYCLEHISTDTL");
					mprodlifecycledtl.setNew(true);
					for (int i = 0; i < detailData.getRowCount(); ++i) {
						mprodlifecycledtl.setField("DT_SL", Integer.valueOf(i + 1).toString());
						mprodlifecycledtl.setField("ACNT_STATUS_CODE", detailData.getValue(i, 1));
						if (!mprodlifecycledtl.save()) {
							processResult.setProcessStatus(TBAProcessStatus.FAILURE);
							processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
							return;
						}
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_EXISTS);
			}
		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mprodlifecycle.close();
			mprodlifecycledtl.close();
		}
	}

	protected void modify() throws TBAFrameworkException {

		try {
			if (isDataExists()) {
				mprodlifecycle.setNew(false);
				mprodlifecycle.setField("ENABLED", processData.get("ENABLED"));
				mprodlifecycle.setField("REMARKS", processData.get("REMARKS"));
				if (mprodlifecycle.save()) {
					String[] mprodLifecycleFields = { "ENTITY_CODE", "PRODUCT_CODE", "EFF_DATE", "DT_SL" };
					BindParameterType[] mprodLifecycleTypes = { BindParameterType.BIGINT, BindParameterType.BIGINT, BindParameterType.DATE, BindParameterType.BIGINT };
					mprodlifecycledtl.setField("ENTITY_CODE", processData.get("ENTITY_CODE"));
					mprodlifecycledtl.setField("PRODUCT_CODE", processData.get("PRODUCT_CODE"));
					mprodlifecycledtl.setField("EFF_DATE", processData.getDate("EFF_DATE"));
					mprodlifecycledtl.deleteByFieldsAudit(mprodLifecycleFields, mprodLifecycleTypes);
					DTDObject detailData = processData.getDTDObject("PRODLIFECYCLEHISTDTL");
					mprodlifecycledtl.setNew(true);
					for (int i = 0; i < detailData.getRowCount(); ++i) {
						mprodlifecycledtl.setField("DT_SL", Integer.valueOf(i + 1).toString());
						mprodlifecycledtl.setField("ACNT_STATUS_CODE", detailData.getValue(i, 1));
						if (!mprodlifecycledtl.save()) {
							processResult.setProcessStatus(TBAProcessStatus.FAILURE);
							processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
							return;
						}
					}
					processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_UPDATED);
				} else {
					processResult.setProcessStatus(TBAProcessStatus.FAILURE);
					processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
				}
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_EXISTS);
			}

		} catch (Exception e) {
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(BackOfficeErrorCodes.RECORD_NOT_UPDATED);
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			mprodlifecycle.close();
			mprodlifecycledtl.close();
		}
	}

}
