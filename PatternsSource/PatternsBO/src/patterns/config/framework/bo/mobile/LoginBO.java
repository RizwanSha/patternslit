/*
 * Copyright (c) 2014, 2015, Patterns Software Design Institute. All
 * rights reserved. Unauthorized copying of this file, via any medium is
 * strictly prohibited. PATTERNS SDI PROPRIETARY/CONFIDENTIAL. Use is
 * subject to license terms.
 */
package patterns.config.framework.bo.mobile;

import java.sql.Timestamp;

import patterns.config.framework.bo.ServiceBO;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.process.TBAProcessResult;
import patterns.config.framework.process.TBAProcessStatus;

/**
 * The Class LoginBO.
 */
public class LoginBO extends ServiceBO {

	public LoginBO() {
	}

	public void init() {
	}

	@Override
	public TBAProcessResult processRequest() {
		if (processData.get("STEP").equals("1")) {
			DBUtil dbUtil = getDbContext().createUtilInstance();
			try {
				String sqlQuery = "INSERT INTO SRVLOGININIT (ENTITY_CODE,USER_ID,REQ_ID) VALUES (?,?,?)";
				dbUtil.reset();
				dbUtil.setSql(sqlQuery);
				dbUtil.setLong(1, Long.parseLong(processData.get("ENTITY_CODE")));
				dbUtil.setString(2, processData.get("USER_ID"));
				dbUtil.setString(3, processData.get("REQ_ID"));
				dbUtil.executeUpdate();
				processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
			} catch (Exception e) {
				e.printStackTrace();
				getLogger().logError(e.getLocalizedMessage());
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
				processResult.setErrorCode(e.getLocalizedMessage());
			} finally {
				dbUtil.reset();
			}
		} else if (processData.get("STEP").equals("2")) {
			if (updateSessionDetails()) {
				processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
			} else {
				processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			}
		}
		return processResult;
	}

	private boolean updateSessionDetails() {
		DBUtil dbUtil = getDbContext().createUtilInstance();
		try {
			String sqlQuery = "INSERT INTO SRVSESSIONDETAILS (ENTITY_CODE,APP_ID,SESSION_ID, USER_ID, USER_NAME, BRANCH_CODE, CLUSTER_CODE, LAST_LOGIN_DATETIME,REQ_DATETIME,FIN_YEAR,ROLE_CODE,ROLE_DESC,BRANCH_DESC,DATE_FORMAT) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			dbUtil.reset();
			dbUtil.setSql(sqlQuery);
			dbUtil.setLong(1, Long.parseLong(processData.get("ENTITY_CODE")));
			dbUtil.setString(2, processData.get("APPL_EXT_APP_CODE"));
			dbUtil.setString(3, processData.get("SESSION_ID"));
			dbUtil.setString(4, processData.get("USER_ID"));
			dbUtil.setString(5, processData.get("USER_NAME"));
			dbUtil.setString(6, processData.get("BRANCH_CODE"));
			dbUtil.setString(7, processData.get("CLUSTER_CODE"));
			dbUtil.setTimestamp(8, (Timestamp) processData.getObject("LAST_LOGIN_DATE_TIME"));
			dbUtil.setTimestamp(9, (Timestamp)processData.getObject("LOGIN_DATE_TIME"));
			dbUtil.setLong(10, Long.parseLong(processData.get("FIN_YEAR")));
			dbUtil.setString(11, processData.get("ROLE_CODE"));
			dbUtil.setString(12, processData.get("ROLE_DESC"));
			dbUtil.setString(13, processData.get("BRANCH_DESC"));
			dbUtil.setString(14, processData.get("DATE_FORMAT"));
			dbUtil.executeUpdate();

			sqlQuery = "INSERT INTO SRVLOGINAUTH (ENTITY_CODE, SESSION_ID, USER_ID) VALUES (?,?,?)";
			dbUtil.reset();
			dbUtil.setSql(sqlQuery);
			dbUtil.setLong(1, Long.parseLong(processData.get("ENTITY_CODE")));
			dbUtil.setString(2, processData.get("SESSION_ID"));
			dbUtil.setString(3, processData.get("USER_ID"));
			dbUtil.executeUpdate();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getLogger().logError(e.getLocalizedMessage());
		} finally {
			dbUtil.reset();
		}
		return false;
	}
}