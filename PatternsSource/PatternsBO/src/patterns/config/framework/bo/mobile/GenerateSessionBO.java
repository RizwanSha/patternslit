package patterns.config.framework.bo.mobile;

import patterns.config.framework.bo.ServiceBO;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.process.TBAProcessResult;
import patterns.config.framework.process.TBAProcessStatus;

/**
 * The Class GenerateServiceBO.
 */
public class GenerateSessionBO extends ServiceBO {

	/**
	 * Instantiates a new generate service bo.
	 */
	public GenerateSessionBO() {
	}

	/**
	 * Inits the.
	 */
	public void init() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.patterns.framework.bo.ServiceBO#processRequest()
	 */
	@Override
	public TBAProcessResult processRequest() {
		DBUtil dbUtil = getDbContext().createUtilInstance();
		String sqlQuery = RegularConstants.EMPTY_STRING;
		try {
			sqlQuery = "INSERT INTO SRVSESSIONLOG(ENTITY_CODE,APP_ID,SESSION_ID,REQ_DATETIME,REQUEST_ID) VALUES (?,?,?,FN_GETCDT(?),?)";
			dbUtil.reset();
			dbUtil.setMode(DBUtil.PREPARED);
			dbUtil.setSql(sqlQuery);
			dbUtil.setString(1, processData.get("ENTITY_CODE"));
			dbUtil.setString(2, processData.get("APP_ID"));
			dbUtil.setString(3, processData.get("SESSION_ID"));
			dbUtil.setString(4, processData.get("ENTITY_CODE"));
			dbUtil.setString(5, processData.get("REQUEST_ID"));
			dbUtil.executeUpdate();

			processResult.setProcessStatus(TBAProcessStatus.SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
			processResult.setProcessStatus(TBAProcessStatus.FAILURE);
			processResult.setErrorCode(e.getLocalizedMessage());
		}
		return processResult;
	}
}