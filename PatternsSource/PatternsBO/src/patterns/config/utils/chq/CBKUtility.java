package patterns.config.utils.chq;



import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.loggers.ApplicationLogger;
import patterns.config.framework.service.DTObject;


public class CBKUtility {
	private ApplicationLogger logger = null;

	public CBKUtility(DBContext dbcontext) {
		logger = ApplicationLogger.getInstance(this.getClass().getSimpleName());
		this.dbContext=dbcontext;
	}

	public ApplicationLogger getLogger() {
		return logger;
	}

	private DBContext dbContext = null;

	public final DBContext getDbContext() {
		return dbContext;
	}
	
	public boolean holdBookInventoryNo(DTObject inputDTO) {
		DBUtil dbutil = getDbContext().createUtilInstance();
		StringBuffer sqlQuery = new StringBuffer(RegularConstants.EMPTY_STRING);
		sqlQuery.append("UPDATE INSBOOKSINV SET INV_DELETED='1' ");
		sqlQuery.append("WHERE ENTITY_CODE=? AND BRANCH_CODE=? AND INST_TYPE_CODE=? AND BOOK_CODE=? AND BOOK_INV_NO = ? AND ");
		sqlQuery.append("INV_DELETED='0' ");
		try {
			dbutil.reset();
			dbutil.setMode(DBUtil.PREPARED);
			dbutil.setSql(sqlQuery.toString());
			dbutil.setLong(1, Long.parseLong(inputDTO.get("ENTITY_CODE")));
			dbutil.setString(2, inputDTO.get("BRANCH_CODE"));
			dbutil.setString(3, inputDTO.get("INST_TYPE_CODE"));
			dbutil.setString(4, inputDTO.get("BOOK_CODE"));
			dbutil.setLong(5, Long.parseLong(inputDTO.get("BOOK_INV_NO")));
			int updCnt=dbutil.executeUpdate();
			if(updCnt!=1){
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			dbutil.reset();
		}
		return false;
	}
	
	public boolean holdBulkBookInventoryNo(DTObject inputDTO) {
		DBUtil dbutil = getDbContext().createUtilInstance();
		StringBuffer sqlQuery = new StringBuffer(RegularConstants.EMPTY_STRING);
		sqlQuery.append("UPDATE INSBOOKSINV SET INV_DELETED='1' ");
		sqlQuery.append("WHERE ENTITY_CODE=? AND BRANCH_CODE=? AND INST_TYPE_CODE=? AND BOOK_CODE=? AND BOOK_INV_NO BETWEEN ? AND ? AND ");
		sqlQuery.append("INV_DELETED='0' ");
		try {
			dbutil.reset();
			dbutil.setMode(DBUtil.PREPARED);
			dbutil.setSql(sqlQuery.toString());
			dbutil.setLong(1, Long.parseLong(inputDTO.get("ENTITY_CODE")));
			dbutil.setString(2, inputDTO.get("BRANCH_CODE"));
			dbutil.setString(3, inputDTO.get("INST_TYPE_CODE"));
			dbutil.setString(4, inputDTO.get("BOOK_CODE"));
			dbutil.setLong(5, Long.parseLong(inputDTO.get("FRM_BOOK_INV_NO")));
			dbutil.setLong(6, Long.parseLong(inputDTO.get("UPT_BOOK_INV_NO")));
			int updCnt=dbutil.executeUpdate();
			if(updCnt!=(Long.parseLong(inputDTO.get("UPT_BOOK_INV_NO"))-Long.parseLong(inputDTO.get("FRM_BOOK_INV_NO"))+1)){
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			dbutil.reset();
		}
		return false;
	}
	
	
	
	public boolean releaseBookInventoryNo(DTObject inputDTO) {
		DBUtil dbutil = getDbContext().createUtilInstance();
		StringBuffer sqlQuery = new StringBuffer(RegularConstants.EMPTY_STRING);
		sqlQuery.append("UPDATE INSBOOKSINV SET INV_DELETED='0' ");
		sqlQuery.append("WHERE ENTITY_CODE=? AND BRANCH_CODE=? AND INST_TYPE_CODE=? AND BOOK_CODE=? AND BOOK_INV_NO = ?");
		try {
			dbutil.reset();
			dbutil.setMode(DBUtil.PREPARED);
			dbutil.setSql(sqlQuery.toString());
			dbutil.setLong(1, Long.parseLong(inputDTO.get("ENTITY_CODE")));
			dbutil.setString(2, inputDTO.get("BRANCH_CODE"));
			dbutil.setString(3, inputDTO.get("INST_TYPE_CODE"));
			dbutil.setString(4, inputDTO.get("BOOK_CODE"));
			dbutil.setLong(5, Long.parseLong(inputDTO.get("BOOK_INV_NO")));
			int updCnt=dbutil.executeUpdate();
			if(updCnt!=1){
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			dbutil.reset();
		}
		return false;
	}
	
	public boolean releaseBulkBookInventoryNo(DTObject inputDTO) {
		DBUtil dbutil = getDbContext().createUtilInstance();
		StringBuffer sqlQuery = new StringBuffer(RegularConstants.EMPTY_STRING);
		sqlQuery.append("UPDATE INSBOOKSINV SET INV_DELETED='0' ");
		sqlQuery.append("WHERE ENTITY_CODE=? AND BRANCH_CODE=? AND INST_TYPE_CODE=? AND BOOK_CODE=? AND BOOK_INV_NO BETWEEN ? AND ?  ");
		try {
			dbutil.reset();
			dbutil.setMode(DBUtil.PREPARED);
			dbutil.setSql(sqlQuery.toString());
			dbutil.setLong(1, Long.parseLong(inputDTO.get("ENTITY_CODE")));
			dbutil.setString(2, inputDTO.get("BRANCH_CODE"));
			dbutil.setString(3, inputDTO.get("INST_TYPE_CODE"));
			dbutil.setString(4, inputDTO.get("BOOK_CODE"));
			dbutil.setLong(5, Long.parseLong(inputDTO.get("FRM_BOOK_INV_NO")));
			dbutil.setLong(6, Long.parseLong(inputDTO.get("UPT_BOOK_INV_NO")));
			int updCnt=dbutil.executeUpdate();
			if(updCnt!=(Long.parseLong(inputDTO.get("UPT_BOOK_INV_NO"))-Long.parseLong(inputDTO.get("FRM_BOOK_INV_NO"))+1)){
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			dbutil.reset();
		}
		return false;
	}
	
	public boolean updateBookInventoryStatus(DTObject inputDTO) {
		DBUtil dbutil = getDbContext().createUtilInstance();
		StringBuffer sqlQuery = new StringBuffer(RegularConstants.EMPTY_STRING);
		String invStatus = inputDTO.get("INV_STATUS");
		if(invStatus.equals("1")){
			sqlQuery.append("UPDATE INSBOOKSINV SET SENT_TO_BRANCH_CODE=?, INV_STATUS=? , INV_DELETED='0' ");
		}else if(invStatus.equals("2")){
			sqlQuery.append("UPDATE INSBOOKSINV SET RECVD_FROM_BRANCH_CODE=?, INV_STATUS=? , INV_DELETED='0' ");
		}else if(invStatus.equals("3") || invStatus.equals("4")){
			sqlQuery.append("UPDATE INSBOOKSINV SET INV_STATUS=? , INV_DELETED='0' ");	
		}
		else if(invStatus.equals("5")){
			sqlQuery.append("UPDATE INSBOOKSINV SET PRE_ALLOTTED_ACCOUNT_NO=?, INV_STATUS=? , INV_DELETED='0' ");
		}
		sqlQuery.append("WHERE ENTITY_CODE=? AND BRANCH_CODE=? AND INST_TYPE_CODE=? AND BOOK_CODE=? AND BOOK_INV_NO = ?");
		try {
			dbutil.reset();
			dbutil.setMode(DBUtil.PREPARED);
			dbutil.setSql(sqlQuery.toString());
			dbutil.setString(1, inputDTO.get("FINAL"));
			dbutil.setLong(2, Long.parseLong(inputDTO.get("INV_STATUS")));
			dbutil.setLong(3, Long.parseLong(inputDTO.get("ENTITY_CODE")));
			dbutil.setString(4, inputDTO.get("BRANCH_CODE"));
			dbutil.setString(5, inputDTO.get("INST_TYPE_CODE"));
			dbutil.setString(6, inputDTO.get("BOOK_CODE"));
			dbutil.setLong(7, Long.parseLong(inputDTO.get("BOOK_INV_NO")));
			int updCnt=dbutil.executeUpdate();
			if(updCnt!=1){
				return false;
			}
			if(!updateBookInventoryStatusRef(inputDTO)){
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			dbutil.reset();
		}
		return false;
	}
	
	public boolean updateBulkBookInventoryStatus(DTObject inputDTO) {
		DBUtil dbutil = getDbContext().createUtilInstance();
		StringBuffer sqlQuery = new StringBuffer(RegularConstants.EMPTY_STRING);
		String invStatus = inputDTO.get("INV_STATUS");
		if(invStatus.equals("1")){
			sqlQuery.append("UPDATE INSBOOKSINV SET SENT_TO_BRANCH_CODE=?, INV_STATUS=? , INV_DELETED='0' ");
		}else if(invStatus.equals("2")){
			sqlQuery.append("UPDATE INSBOOKSINV SET RECVD_FROM_BRANCH_CODE=?, INV_STATUS=? , INV_DELETED='0' ");
		}else if(invStatus.equals("5")){
			sqlQuery.append("UPDATE INSBOOKSINV SET PRE_ALLOTTED_ACCOUNT_NO=?, INV_STATUS=? , INV_DELETED='0' ");
		}
		sqlQuery.append("WHERE ENTITY_CODE=? AND BRANCH_CODE=? AND INST_TYPE_CODE=? AND BOOK_CODE=? AND BOOK_INV_NO BETWEEN ? AND ?");
		try {
			dbutil.reset();
			dbutil.setMode(DBUtil.PREPARED);
			dbutil.setSql(sqlQuery.toString());
			//dbutil.setString(1, inputDTO.get("ISSUE_TO_BRN_CODE"));
			dbutil.setString(1, inputDTO.get("FINAL"));
			dbutil.setLong(2, Long.parseLong(inputDTO.get("INV_STATUS")));
			dbutil.setLong(3, Long.parseLong(inputDTO.get("ENTITY_CODE")));
			dbutil.setString(4, inputDTO.get("BRANCH_CODE"));
			dbutil.setString(5, inputDTO.get("INST_TYPE_CODE"));
			dbutil.setString(6, inputDTO.get("BOOK_CODE"));
			dbutil.setLong(7, Long.parseLong(inputDTO.get("FRM_BOOK_INV_NO")));
			dbutil.setLong(8, Long.parseLong(inputDTO.get("UPT_BOOK_INV_NO")));
			int updCnt=dbutil.executeUpdate();
			if(updCnt!=(Long.parseLong(inputDTO.get("UPT_BOOK_INV_NO"))-Long.parseLong(inputDTO.get("FRM_BOOK_INV_NO"))+1)){
				return false;
			}
			if(!updateBulkBookInventoryStatusRef(inputDTO)){
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			dbutil.reset();
		}
		return false;
	}
	
	public boolean updateBookInventoryStatusRef(DTObject inputDTO) {
		DBUtil dbutil = getDbContext().createUtilInstance();
		StringBuffer sqlQuery = new StringBuffer(RegularConstants.EMPTY_STRING);
		sqlQuery.append("INSERT INTO INSBOOKSINVREF (ENTITY_CODE,BRANCH_CODE,INST_TYPE_CODE,BOOK_CODE,BOOK_INV_NO,STATUS_DATE_TIME,");
		sqlQuery.append("INV_STATUS,STATUS_SRC_PGM,STATUS_SRC_TABLE,STATUS_SRC_KEY) VALUES (?,?,?,?,?,FN_GETCDT(?),?,?,?,?)");
		try {
			dbutil.reset();
			dbutil.setMode(DBUtil.PREPARED);
			dbutil.setSql(sqlQuery.toString());
			dbutil.setLong(1, Long.parseLong(inputDTO.get("ENTITY_CODE")));
			dbutil.setString(2, inputDTO.get("BRANCH_CODE"));
			dbutil.setString(3, inputDTO.get("INST_TYPE_CODE"));
			dbutil.setString(4, inputDTO.get("BOOK_CODE"));
			dbutil.setLong(5, Long.parseLong(inputDTO.get("BOOK_INV_NO")));
			dbutil.setLong(6, Long.parseLong(inputDTO.get("ENTITY_CODE")));
			dbutil.setLong(7, Long.parseLong(inputDTO.get("INV_STATUS")));
			dbutil.setString(8, inputDTO.get("STATUS_SRC_PGM"));
			dbutil.setString(9, inputDTO.get("STATUS_SRC_TABLE"));
			dbutil.setString(10, inputDTO.get("STATUS_SRC_KEY"));
			int updCnt=dbutil.executeUpdate();
			if(updCnt!=1){
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			dbutil.reset();
		}
		return false;
	}
	
	public boolean updateBulkBookInventoryStatusRef(DTObject inputDTO) {
		DBUtil dbutil = getDbContext().createUtilInstance();
		StringBuffer sqlQuery = new StringBuffer(RegularConstants.EMPTY_STRING);
		sqlQuery.append("INSERT INTO INSBOOKSINVREF (ENTITY_CODE,BRANCH_CODE,INST_TYPE_CODE,BOOK_CODE,BOOK_INV_NO,STATUS_DATE_TIME,");
		sqlQuery.append("INV_STATUS,STATUS_SRC_PGM,STATUS_SRC_TABLE,STATUS_SRC_KEY) ");
		sqlQuery.append("SELECT INV.ENTITY_CODE,INV.BRANCH_CODE,INV.INST_TYPE_CODE,INV.BOOK_CODE,INV.BOOK_INV_NO,FN_GETCDT(INV.ENTITY_CODE),?,?,?,?  ");
		sqlQuery.append("FROM INSBOOKSINV INV ");
		sqlQuery.append("WHERE INV.ENTITY_CODE=? AND INV.BRANCH_CODE=? AND INV.INST_TYPE_CODE=? AND INV.BOOK_CODE=? AND INV.BOOK_INV_NO BETWEEN ? AND ?");
		try {
			dbutil.reset();
			dbutil.setMode(DBUtil.PREPARED);
			dbutil.setSql(sqlQuery.toString());
			dbutil.setLong(1, Long.parseLong(inputDTO.get("INV_STATUS")));
			dbutil.setString(2, inputDTO.get("STATUS_SRC_PGM"));
			dbutil.setString(3, inputDTO.get("STATUS_SRC_TABLE"));
			dbutil.setString(4, inputDTO.get("STATUS_SRC_KEY"));
			dbutil.setLong(5, Long.parseLong(inputDTO.get("ENTITY_CODE")));
			dbutil.setString(6, inputDTO.get("BRANCH_CODE"));
			dbutil.setString(7, inputDTO.get("INST_TYPE_CODE"));
			dbutil.setString(8, inputDTO.get("BOOK_CODE"));
			dbutil.setLong(9, Long.parseLong(inputDTO.get("FRM_BOOK_INV_NO")));
			dbutil.setLong(10, Long.parseLong(inputDTO.get("UPT_BOOK_INV_NO")));
			int updCnt=dbutil.executeUpdate();
			if(updCnt!=(Long.parseLong(inputDTO.get("UPT_BOOK_INV_NO"))-Long.parseLong(inputDTO.get("FRM_BOOK_INV_NO"))+1)){
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			dbutil.reset();
		}
		return false;
	}
	
}
