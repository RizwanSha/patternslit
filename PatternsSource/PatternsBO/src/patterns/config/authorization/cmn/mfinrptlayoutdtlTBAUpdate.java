package patterns.config.authorization.cmn;

import java.sql.Date;

import patterns.config.authorization.CommonTBAUpdate;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;

public class mfinrptlayoutdtlTBAUpdate extends CommonTBAUpdate {

	@Override
	public DTObject processTBAUpdate(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DTObject processEffDateTBAUpdate(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		DTObject resultDTO = new DTObject();
		DBUtil util = dbContext.createUtilInstance();
		DBUtil util1 = dbContext.createUtilInstance();
		if (inputDTO.get(PROCESS_OPTION).equals(AUTHORIZE)) {
			splitSourceKey(inputDTO);
			String entitycode = strsrcKey[0];
			String reportLayoutCode = strsrcKey[1];
			String sectionSl = strsrcKey[2];
			String effectDate = strsrcKey[3];
			Date effectiveDate = new java.sql.Date(BackOfficeFormatUtils.getDate(effectDate, BackOfficeConstants.TBA_DATE_FORMAT).getTime());
			try {
				util.reset();
				util.setSql("DELETE FROM FINRPTLAYOUTCONFDTL WHERE ENTITY_CODE=? AND REPORT_LAYOUT_CODE=? AND REPORT_SECTION_SL=?");
				util.setString(1, entitycode);
				util.setString(2, reportLayoutCode);
				util.setString(3, sectionSl);
				util.executeUpdate();

				util1.reset();
				util1.setMode(DBUtil.PREPARED);
				util1.setSql("INSERT INTO FINRPTLAYOUTCONFDTL(ENTITY_CODE,REPORT_LAYOUT_CODE,REPORT_SECTION_SL,DTL_SL,REPORT_ELEMENT_CODE) SELECT ENTITY_CODE,REPORT_LAYOUT_CODE,REPORT_SECTION_SL,DTL_SL,REPORT_ELEMENT_CODE FROM FINRPTLAYOUTCONFHISTDTL WHERE ENTITY_CODE=? AND  REPORT_LAYOUT_CODE=? AND REPORT_SECTION_SL=? AND EFF_DATE=?");
				util1.setString(1, entitycode);
				util1.setString(2, reportLayoutCode);
				util1.setString(3, sectionSl);
				util1.setDate(4, effectiveDate);
				util1.executeUpdate();

			} catch (Exception e) {
				e.printStackTrace();
				throw new TBAFrameworkException(e.getLocalizedMessage());
			} finally {
				util.reset();
				util1.reset();
			}
		}
		return resultDTO;
	}
}