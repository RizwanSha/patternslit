package patterns.config.authorization.cmn;

import java.sql.Date;
import java.sql.ResultSet;

import patterns.config.authorization.CommonTBAUpdate;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.service.DTObject;

public class mentitiesTBAUpdate extends CommonTBAUpdate {

	@Override
	public DTObject processTBAUpdate(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {

		DTObject resultDTO = new DTObject();
		DBUtil util = dbContext.createUtilInstance();

		if (inputDTO.get(PROCESS_OPTION).equals(AUTHORIZE)) {
			splitSourceKey(inputDTO);
			String entitycode = strsrcKey[0];
			Date cbd = (java.sql.Date) inputDTO.getObject("CBD");

			try {
				util.setSql("SELECT 1 FROM ENTITIES WHERE ENTITY_CODE = ? AND DATA_VERSION = '1' AND MULTI_CCY_ALLOWED = '0' ");
				util.setString(1, entitycode);
				ResultSet rset = util.executeQuery();
				if (rset.next()) {

					util.reset();
					util.setSql("DELETE  FROM ENTCURR WHERE ENTITY_CODE=?");
					util.setString(1, entitycode);
					util.executeUpdate();

					util.reset();
					util.setMode(DBUtil.PREPARED);
					util.setSql("INSERT INTO ENTCURR(ENTITY_CODE,EFFT_DATE,ENABLED,REMARKS) SELECT ENTITY_CODE,DATE_OF_INCORP,?,REMARKS FROM ENTITIES  WHERE ENTITY_CODE=?");
					util.setString(1, "1");
					util.setString(2, entitycode);
					util.executeUpdate();

					util.reset();
					util.setSql("DELETE  FROM ENTCURRDTL WHERE ENTITY_CODE=? AND SL=?");
					util.setString(1, entitycode);
					util.setString(2, "1");
					util.executeUpdate();

					util.reset();
					util.setMode(DBUtil.PREPARED);
					util.setSql("INSERT INTO ENTCURRDTL(ENTITY_CODE,SL,CURR_CODE) SELECT ENTITY_CODE,?,BASE_CCY_CODE FROM ENTITIES  WHERE ENTITY_CODE=?");
					util.setString(1, "1");
					util.setString(2, entitycode);
					util.executeUpdate();

					util.reset();
					util.setSql("DELETE  FROM ENTCURRHIST WHERE ENTITY_CODE=? AND EFFT_DATE >= ?");
					util.setString(1, entitycode);
					util.setDate(2, cbd);
					util.executeUpdate();

					util.reset();
					util.setMode(DBUtil.PREPARED);
					util.setSql("INSERT INTO ENTCURRHIST(ENTITY_CODE,EFFT_DATE,ENABLED,REMARKS) SELECT ENTITY_CODE,DATE_OF_INCORP,?,REMARKS FROM ENTITIES  WHERE ENTITY_CODE=?");
					util.setString(1, "1");
					util.setString(2, entitycode);
					util.executeUpdate();

					util.reset();
					util.setSql("DELETE  FROM ENTCURRHISTDTL WHERE ENTITY_CODE=? AND EFFT_DATE=? AND SL=? ");
					util.setString(1, entitycode);
					util.setDate(2, cbd);
					util.setString(3, "1");
					util.executeUpdate();

					util.reset();
					util.setMode(DBUtil.PREPARED);
					util.setSql("INSERT INTO ENTCURRHISTDTL(ENTITY_CODE,EFFT_DATE,SL,CURR_CODE) SELECT ENTITY_CODE,DATE_OF_INCORP,?,BASE_CCY_CODE FROM ENTITIES   WHERE  ENTITY_CODE=?");
					util.setString(1, "1");
					util.setString(2, entitycode);
					util.executeUpdate();
				}

			} catch (Exception e) {
				e.printStackTrace();
				throw new TBAFrameworkException(e.getLocalizedMessage());
			} finally {
				util.reset();
			}
		}
		return resultDTO;
	}

	@Override
	public DTObject processEffDateTBAUpdate(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		return null;
	}
}
