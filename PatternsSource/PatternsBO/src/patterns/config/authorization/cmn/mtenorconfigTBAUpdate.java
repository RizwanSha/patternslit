package patterns.config.authorization.cmn;

import java.sql.Date;
import java.sql.ResultSet;

import patterns.config.authorization.CommonTBAUpdate;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.service.DTObject;
/*
 * The program will be used to create and maintain  Tenor Specification Code. Tenor Configuration Specification. 
 *
 Author : Pavan kumar.R
 Created Date : 01-DEC-2016
 Spec Reference CMN-00042
 Modification History
 ----------------------------------------------------------------------
 Sl.No		Modified Date	Author			Modified Changes	Version
 ----------------------------------------------------------------------

 */
import patterns.config.framework.web.BackOfficeFormatUtils;

public class mtenorconfigTBAUpdate extends CommonTBAUpdate {

	@Override
	public DTObject processTBAUpdate(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DTObject processEffDateTBAUpdate(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		DTObject resultDTO = new DTObject();
		DBUtil util = dbContext.createUtilInstance();
		if (inputDTO.get(PROCESS_OPTION).equals(AUTHORIZE)) {
			splitSourceKey(inputDTO);
			long entitycode = Long.parseLong(strsrcKey[0]);
			String tenorSpecificationCode = strsrcKey[1];
			String effectDate = strsrcKey[2];
			Date effectiveDate = new java.sql.Date(BackOfficeFormatUtils.getDate(effectDate, BackOfficeConstants.TBA_DATE_FORMAT).getTime());
			try {
				util.reset();
				util.setMode(DBUtil.PREPARED);
				util.setSql("DELETE  FROM TENORCONFIG WHERE ENTITY_CODE=? AND TENOR_SPEC_CODE=?");
				util.setLong(1, entitycode);
				util.setString(2, tenorSpecificationCode);
				util.executeUpdate();

				util.reset();
				util.setMode(DBUtil.PREPARED);
				util.setSql("DELETE  FROM TENORCONFIGDTL WHERE ENTITY_CODE=? AND TENOR_SPEC_CODE=?");
				util.setLong(1, entitycode);
				util.setString(2, tenorSpecificationCode);
				util.executeUpdate();

				util.reset();
				util.setMode(DBUtil.PREPARED);
				util.setSql("SELECT ENABLED FROM TENORCONFIGHIST WHERE ENTITY_CODE=? AND TENOR_SPEC_CODE =? AND EFF_DATE=?");
				util.setLong(1, entitycode);
				util.setString(2, tenorSpecificationCode);
				util.setDate(3, effectiveDate);
				ResultSet rs = util.executeQuery();
				if (rs.next()) {
					if (rs.getString("ENABLED").equals("1")) {
						util.reset();
						util.setMode(DBUtil.PREPARED);
						util.setSql("INSERT INTO TENORCONFIG (ENTITY_CODE,TENOR_SPEC_CODE,EFF_DATE) SELECT ENTITY_CODE,TENOR_SPEC_CODE,EFF_DATE FROM TENORCONFIGHIST WHERE ENTITY_CODE=? AND TENOR_SPEC_CODE =? AND EFF_DATE=? ");
						util.setLong(1, entitycode);
						util.setString(2, tenorSpecificationCode);
						util.setDate(3, effectiveDate);
						if (util.executeUpdate() <= 0)
							throw new TBAFrameworkException();

						util.reset();
						util.setMode(DBUtil.PREPARED);
						util.setSql("INSERT INTO TENORCONFIGDTL (ENTITY_CODE,TENOR_SPEC_CODE,DTL_SL,UPTO_PERIOD,PERIOD_FLAG,TENOR_DESCRIPTION) SELECT ENTITY_CODE,TENOR_SPEC_CODE,DTL_SL,UPTO_PERIOD,PERIOD_FLAG,TENOR_DESCRIPTION FROM TENORCONFIGHISTDTL WHERE ENTITY_CODE=? AND TENOR_SPEC_CODE =? AND EFF_DATE=? ");
						util.setLong(1, entitycode);
						util.setString(2, tenorSpecificationCode);
						util.setDate(3, effectiveDate);
						if (util.executeUpdate() <= 0)
							throw new TBAFrameworkException();
					}
				} 
			} catch (Exception e) {
				e.printStackTrace();
				throw new TBAFrameworkException(e.getLocalizedMessage());
			} finally {
				util.reset();
			}
		}
		return resultDTO;
	}
}