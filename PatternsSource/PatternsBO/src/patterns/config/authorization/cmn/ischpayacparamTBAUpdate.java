package patterns.config.authorization.cmn;

import java.sql.Date;

import patterns.config.authorization.CommonTBAUpdate;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;

public class ischpayacparamTBAUpdate extends CommonTBAUpdate {

	@Override
	public DTObject processTBAUpdate(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		return null;
	}

	@Override
	public DTObject processEffDateTBAUpdate(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		DTObject resultDTO = new DTObject();
		DBUtil util = dbContext.createUtilInstance();
		if (inputDTO.get(PROCESS_OPTION).equals(AUTHORIZE)) {
			splitSourceKey(inputDTO);
			String entitycode = strsrcKey[0];
			String schemeCode = strsrcKey[1];
			String efftDateString = strsrcKey[2];
			Date effectiveDate = new java.sql.Date(BackOfficeFormatUtils.getDate(efftDateString, BackOfficeConstants.TBA_DATE_FORMAT).getTime());
			try {
				util.reset();
				util.setSql("DELETE FROM SCHPAYPARAM WHERE ENTITY_CODE=? AND SCHEME_CODE=?");
				util.setString(1, entitycode);
				util.setString(2, schemeCode);
				util.executeUpdate();

				util.reset();
				util.setMode(DBUtil.PREPARED);
				util.setSql("INSERT INTO SCHPAYPARAM(ENTITY_CODE,SCHEME_CODE,BULK_PAY_PREFUNDED,DB_FROM_FILE,DB_AMOUNT_SRC,DB_GL_HEAD,DB_AC_NON_PREFUND,CR_GL_HEAD,FAILED_CR_GL_HEAD)SELECT ENTITY_CODE,SCHEME_CODE,BULK_PAY_PREFUNDED,DB_FROM_FILE,DB_AMOUNT_SRC,DB_GL_HEAD,DB_AC_NON_PREFUND,CR_GL_HEAD,FAILED_CR_GL_HEAD FROM SCHPAYPARAMHIST  WHERE ENTITY_CODE=? AND SCHEME_CODE=? AND EFFT_DATE=?");
				util.setString(1, entitycode);
				util.setString(2, schemeCode);
				util.setDate(3, effectiveDate);
				util.executeUpdate();
			} catch (Exception e) {
				e.printStackTrace();
				throw new TBAFrameworkException(e.getLocalizedMessage());
			} finally {
				util.reset();
			}
		}
		return resultDTO;
	}
}