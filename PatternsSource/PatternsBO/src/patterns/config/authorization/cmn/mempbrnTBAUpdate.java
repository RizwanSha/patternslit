package patterns.config.authorization.cmn;

import java.sql.Date;
import java.sql.ResultSet;

import patterns.config.authorization.CommonTBAUpdate;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;

public class mempbrnTBAUpdate extends CommonTBAUpdate {

	@Override
	public DTObject processTBAUpdate(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DTObject processEffDateTBAUpdate(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		DTObject resultDTO = new DTObject();
		DBUtil util = dbContext.createUtilInstance();
		if (inputDTO.get(PROCESS_OPTION).equals(AUTHORIZE)) {
			splitSourceKey(inputDTO);
			long entitycode = Long.parseLong(strsrcKey[0]);
			String empCode = strsrcKey[1];
			String effectDate = strsrcKey[2];
			Date effectiveDate = new java.sql.Date(BackOfficeFormatUtils.getDate(effectDate, BackOfficeConstants.TBA_DATE_FORMAT).getTime());
			try {
				util.reset();
				util.setSql("DELETE FROM EMPBRN WHERE ENTITY_CODE=? AND EMP_CODE=?");
				util.setLong(1, entitycode);
				util.setString(2, empCode);
				util.executeUpdate();

				util.reset();
				util.setMode(DBUtil.PREPARED);
				util.setSql("INSERT INTO EMPBRN(ENTITY_CODE,EMP_CODE,BRANCH_CODE) SELECT ENTITY_CODE,EMP_CODE,BRANCH_CODE FROM EMPBRNHIST WHERE ENTITY_CODE=? AND EMP_CODE=? AND EFF_DATE=?");
				util.setLong(1, entitycode);
				util.setString(2, empCode);
				util.setDate(3, effectiveDate);
				util.executeUpdate();
				
				util.reset();
				util.setMode(DBUtil.PREPARED);
				util.setSql("SELECT BRANCH_CODE FROM EMPBRN WHERE ENTITY_CODE=? AND EMP_CODE=?");
				util.setLong(1, entitycode);
				util.setString(2, empCode);
				ResultSet rs=util.executeQuery();
				String branchCode=null;
				if(rs.next()){
					branchCode=rs.getString(1);
				}
				
				util.reset();
				util.setMode(DBUtil.PREPARED);
				util.setSql("UPDATE USERS SET BRANCH_CODE=? WHERE ENTITY_CODE=? AND EMP_CODE=? ");
				util.setString(1,branchCode);
				util.setLong(2, entitycode);
				util.setString(3, empCode);
				util.executeUpdate();

			} catch (Exception e) {
				e.printStackTrace();
				throw new TBAFrameworkException(e.getLocalizedMessage());
			} finally {
				util.reset();
			}
		}
		return resultDTO;
	}
}