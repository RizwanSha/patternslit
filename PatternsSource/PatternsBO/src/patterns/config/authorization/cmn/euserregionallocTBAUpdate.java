package patterns.config.authorization.cmn;

import java.sql.Date;
import patterns.config.authorization.CommonTBAUpdate;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;

public class euserregionallocTBAUpdate extends CommonTBAUpdate {
	@Override
	public DTObject processTBAUpdate(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		// TODO Auto-generated method stub

		return null;
	}

	@Override
	public DTObject processEffDateTBAUpdate(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		DTObject resultDTO = new DTObject();
		DBUtil util = dbContext.createUtilInstance();
		if (inputDTO.get(PROCESS_OPTION).equals(AUTHORIZE)) {
			splitSourceKey(inputDTO);
			String entitycode = strsrcKey[0];
			String filedUserId = strsrcKey[1];
			String efftDateString = strsrcKey[2];
			Date effetiveDate = new java.sql.Date(BackOfficeFormatUtils.getDate(efftDateString, BackOfficeConstants.TBA_DATE_FORMAT).getTime());
			try {
				util.reset();
				util.setSql("DELETE FROM USERREGIONALLOC WHERE ENTITY_CODE=? AND FIELD_USER_ID=?");
				util.setString(1, entitycode);
				util.setString(2, filedUserId);
				util.execute();

				util.reset();
				util.setMode(DBUtil.PREPARED);
				util.setSql("INSERT INTO USERREGIONALLOC (ENTITY_CODE,FIELD_USER_ID,REMARKS) SELECT ENTITY_CODE,FIELD_USER_ID,REMARKS FROM USERREGIONALLOCHIST WHERE ENTITY_CODE=? AND FIELD_USER_ID=? AND EFF_DATE=?");
				util.setString(1, entitycode);
				util.setString(2, filedUserId);
				util.setDate(3, effetiveDate);
				util.execute();

				util.reset();
				util.setSql("DELETE FROM USERREGIONALLOCDTL WHERE ENTITY_CODE=? AND FIELD_USER_ID=?");
				util.setString(1, entitycode);
				util.setString(2, filedUserId);
				util.execute();

				util.reset();
				util.setMode(DBUtil.PREPARED);
				util.setSql("INSERT INTO USERREGIONALLOCDTL (ENTITY_CODE,FIELD_USER_ID,DTSL,GEO_REGION_CODE) SELECT ENTITY_CODE,FIELD_USER_ID,DTSL,GEO_REGION_CODE FROM USERREGIONALLOCHISTDTL WHERE ENTITY_CODE=? AND FIELD_USER_ID=? AND EFF_DATE=?");
				util.setString(1, entitycode);
				util.setString(2, filedUserId);
				util.setDate(3, effetiveDate);
				util.execute();

			} catch (Exception e) {
				e.printStackTrace();
				throw new TBAFrameworkException(e.getLocalizedMessage());
			} finally {
				util.reset();
			}
		}
		return resultDTO;
	}
}