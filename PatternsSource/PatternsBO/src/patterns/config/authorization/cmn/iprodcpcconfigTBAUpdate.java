package patterns.config.authorization.cmn;

import java.sql.Date;
import java.sql.ResultSet;

import patterns.config.authorization.CommonTBAUpdate;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;

public class iprodcpcconfigTBAUpdate extends CommonTBAUpdate {

	@Override
	public DTObject processTBAUpdate(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public DTObject processEffDateTBAUpdate(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		DTObject resultDTO = new DTObject();
		DBUtil util = dbContext.createUtilInstance();
		DBUtil util1 = dbContext.createUtilInstance();
		if (inputDTO.get(PROCESS_OPTION).equals(AUTHORIZE)) {
			splitSourceKey(inputDTO);
			long entityCode = Long.parseLong(strsrcKey[0]);
			String productCode = strsrcKey[1];
			String portfolioCode = strsrcKey[2];
			String effDate = strsrcKey[3];
			Date effectiveDate = (java.sql.Date) inputDTO.getObject("CBD");
			Date effectDate = new java.sql.Date(BackOfficeFormatUtils.getDate(effDate, BackOfficeConstants.TBA_DATE_FORMAT).getTime());
			if (effectiveDate.equals(effectDate)) {
				try {
					util.reset();
					util.setSql("DELETE FROM PRODCPCONFIG WHERE ENTITY_CODE=? AND LEASE_PRODUCT_CODE=? AND PORTFOLIO_CODE=?");
					util.setLong(1, entityCode);
					util.setString(2, productCode);
					util.setString(3, portfolioCode);
					util.executeUpdate();

					util.reset();
					util.setMode(DBUtil.PREPARED);
					util.setSql("SELECT ENTITY_CODE,LEASE_PRODUCT_CODE,PORTFOLIO_CODE,EFF_DATE,COST_CP_CENTER_CODE,PROFIT_CP_CENTER_CODE FROM PRODCPCONFIGHIST WHERE ENTITY_CODE=? AND LEASE_PRODUCT_CODE=? AND PORTFOLIO_CODE=? AND EFF_DATE=?");
					util.setLong(1, entityCode);
					util.setString(2, productCode);
					util.setString(3, portfolioCode);
					util.setDate(4, effectDate);
					ResultSet rs = util.executeQuery();
					if (rs.next()) {
						util1.reset();
						util1.setMode(DBUtil.PREPARED);
						util1.setSql("INSERT INTO PRODCPCONFIG VALUES(?,?,?,?,?,?)");
						util1.setLong(1, entityCode);
						util1.setString(2, productCode);
						util1.setString(3, portfolioCode);
						util1.setDate(4, effectDate);
						util1.setString(5, rs.getString("COST_CP_CENTER_CODE"));
						util1.setString(6, rs.getString("PROFIT_CP_CENTER_CODE"));
						int count = util1.executeUpdate();
						if (count == 0) {
							throw new TBAFrameworkException();
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
					throw new TBAFrameworkException(e.getLocalizedMessage());
				} finally {
					util.reset();
					util1.reset();
				}
			}
		}
		return resultDTO;
	}
}
