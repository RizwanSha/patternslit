	package patterns.config.authorization.cmn;

	import java.sql.Date;
import java.sql.ResultSet;

	import patterns.config.authorization.CommonTBAUpdate;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;

	public class iassettypetaxschTBAUpdate extends CommonTBAUpdate {

		@Override
		public DTObject processTBAUpdate(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public DTObject processEffDateTBAUpdate(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
			DTObject resultDTO = new DTObject();
			DBUtil util = dbContext.createUtilInstance();
			if (inputDTO.get(PROCESS_OPTION).equals(AUTHORIZE)) {
				splitSourceKey(inputDTO);
				long entitycode = Long.parseLong(strsrcKey[0]);
				String assetTypeCode = strsrcKey[1];
				String effectDate = strsrcKey[2];
				Date effectiveDate = new java.sql.Date(BackOfficeFormatUtils.getDate(effectDate, BackOfficeConstants.TBA_DATE_FORMAT).getTime());
				try {
					util.reset();
					util.setSql("DELETE FROM ASSETTYPETAXSCH WHERE ENTITY_CODE=? AND ASSET_TYPE_CODE=?");
					util.setLong(1, entitycode);
					util.setString(2, assetTypeCode);
					util.executeUpdate();

					util.reset();
					util.setSql("DELETE FROM ASSETTYPETAXSCHDTL WHERE ENTITY_CODE=? AND ASSET_TYPE_CODE=?");
					util.setLong(1, entitycode);
					util.setString(2, assetTypeCode);
					util.executeUpdate();

					util.reset();
					util.setSql("SELECT ENABLED FROM ASSETTYPETAXSCHHIST WHERE ENTITY_CODE=? AND ASSET_TYPE_CODE=? AND EFF_DATE=?");
					util.setLong(1, entitycode);
					util.setString(2, assetTypeCode);
					util.setDate(3, effectiveDate);
					ResultSet rs = util.executeQuery();
					if (rs.next()) {
						if (rs.getString("ENABLED").equals(RegularConstants.ONE)) {
							util.reset();
							util.setSql("INSERT INTO ASSETTYPETAXSCH SELECT ENTITY_CODE,ASSET_TYPE_CODE,EFF_DATE FROM ASSETTYPETAXSCHHIST WHERE ENTITY_CODE=? AND ASSET_TYPE_CODE=? AND EFF_DATE=?");
							util.setLong(1, entitycode);
							util.setString(2, assetTypeCode);
							util.setDate(3, effectiveDate);
							util.executeUpdate();

							util.reset();
							util.setSql("INSERT INTO ASSETTYPETAXSCHDTL SELECT ENTITY_CODE,ASSET_TYPE_CODE,DTL_SL,STATE_CODE,TAX_SCHEDULE_CODE FROM ASSETTYPETAXSCHHISTDTL WHERE ENTITY_CODE=? AND ASSET_TYPE_CODE=? AND EFF_DATE=?");
							util.setLong(1, entitycode);
							util.setString(2, assetTypeCode);
							util.setDate(3, effectiveDate);
							util.executeUpdate();
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
					throw new TBAFrameworkException(e.getLocalizedMessage());
				} finally {
					util.reset();
				}
			}
			return resultDTO;
		}

	}
