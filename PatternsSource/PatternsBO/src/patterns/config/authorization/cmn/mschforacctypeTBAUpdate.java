package patterns.config.authorization.cmn;

import java.sql.Date;

import patterns.config.authorization.CommonTBAUpdate;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;

public class mschforacctypeTBAUpdate extends CommonTBAUpdate {

	@Override
	public DTObject processTBAUpdate(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DTObject processEffDateTBAUpdate(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		DTObject resultDTO = new DTObject();
		DBUtil util = dbContext.createUtilInstance();

		if (inputDTO.get(PROCESS_OPTION).equals(AUTHORIZE)) {
			splitSourceKey(inputDTO);
			String entitycode = strsrcKey[0];
			String accountTypeCode = strsrcKey[1];
			String efftDate = strsrcKey[2];
			Date effectiveDate = new java.sql.Date(BackOfficeFormatUtils.getDate(efftDate, BackOfficeConstants.TBA_DATE_FORMAT).getTime());
			try {
				util.reset();
				util.setSql("DELETE FROM SCHFORACCTYPEDTL WHERE ENTITY_CODE=? AND ACCOUNT_TYPE_CODE=?");
				util.setString(1, entitycode);
				util.setString(2, accountTypeCode);
				util.executeUpdate();
				util.reset();
				util.setSql("INSERT INTO SCHFORACCTYPEDTL(ENTITY_CODE,ACCOUNT_TYPE_CODE,SL,SCHEME_CODE) SELECT ENTITY_CODE,ACCOUNT_TYPE_CODE,SL,SCHEME_CODE FROM SCHFORACCTYPEHISTDTL WHERE ENTITY_CODE=? AND ACCOUNT_TYPE_CODE=? AND EFF_DATE=?");
				util.setString(1, entitycode);
				util.setString(2, accountTypeCode);
				util.setDate(3, effectiveDate);
				util.executeUpdate();
				} catch (Exception e) {
				e.printStackTrace();
				throw new TBAFrameworkException(e.getLocalizedMessage());
			} finally {
				util.reset();
			}
		}
		return resultDTO;
	}

}
