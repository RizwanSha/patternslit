


package patterns.config.authorization.cmn;

import java.sql.Date;
import java.sql.ResultSet;

import patterns.config.authorization.CommonTBAUpdate;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;

/**
 * 
 * 
 * @version 1.0
 * @author P Vishnu
 * @since 20-March-2017 <br>
 *        <b>Modified History</b> <BR>
 *        <U><B> Sl.No Modified Date Author Modified Changes Version </B></U> <br>
 * 
 * 
 */

public class itaxbyusleaseTBAUpdate extends CommonTBAUpdate {
	@Override
	public DTObject processTBAUpdate(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		// TODO Auto-generated method stub

		return null;
	}

	@Override
	public DTObject processEffDateTBAUpdate(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		DTObject resultDTO = new DTObject();
		DBUtil util = dbContext.createUtilInstance();
		if (inputDTO.get(PROCESS_OPTION).equals(AUTHORIZE)) {
			splitSourceKey(inputDTO);
			String entitycode = strsrcKey[0];
			String lesseeCode = strsrcKey[1];
			String agreementNumber = strsrcKey[2];
			String scheduleId = strsrcKey[3];
			String stateCode = strsrcKey[4];
			String taxCode = strsrcKey[5];
			String effectiveDate = strsrcKey[6];
			Date effetiveDateObject = new java.sql.Date(BackOfficeFormatUtils.getDate(effectiveDate, BackOfficeConstants.TBA_DATE_FORMAT).getTime());
			try {
				util.reset();
				util.setSql("DELETE  FROM TAXBYUSLEASE WHERE ENTITY_CODE=? AND LESSEE_CODE=? AND AGREEMENT_NO=? AND SCHEDULE_ID=? AND STATE_CODE=? AND TAX_CODE=?");
				util.setString(1, entitycode);
				util.setString(2, lesseeCode);
				util.setString(3, agreementNumber);
				util.setString(4, scheduleId);
				util.setString(5, stateCode);
				util.setString(6, taxCode);
				util.executeUpdate();
				util.reset();
				util.setSql("SELECT ENABLED FROM TAXBYUSLEASEHIST WHERE ENTITY_CODE=? AND LESSEE_CODE=? AND AGREEMENT_NO=? AND SCHEDULE_ID=? AND STATE_CODE=? AND TAX_CODE=? AND EFF_DATE=? ");
				util.setString(1, entitycode);
				util.setString(2, lesseeCode);
				util.setString(3, agreementNumber);
				util.setString(4, scheduleId);
				util.setString(5, stateCode);
				util.setString(6, taxCode);
				util.setDate(7, effetiveDateObject);
				ResultSet rs = util.executeQuery();
				if (rs.next()) {
					if (rs.getString("ENABLED").equals("1")) {
						util.reset();
						util.setMode(DBUtil.PREPARED);
						util.setSql("INSERT INTO TAXBYUSLEASE(ENTITY_CODE,LESSEE_CODE,AGREEMENT_NO,SCHEDULE_ID,STATE_CODE,TAX_CODE,TAX_BY_US,TAX_BYUS_GL_HEAD_CODE,EFF_DATE) SELECT ENTITY_CODE,LESSEE_CODE,AGREEMENT_NO,SCHEDULE_ID,STATE_CODE,TAX_CODE,TAX_BY_US,TAX_BYUS_GL_HEAD_CODE,EFF_DATE FROM TAXBYUSLEASEHIST  WHERE ENTITY_CODE=? AND LESSEE_CODE=? AND AGREEMENT_NO=? AND SCHEDULE_ID=? AND STATE_CODE=? AND TAX_CODE=? AND EFF_DATE=? ");
						util.setString(1, entitycode);
						util.setString(2, lesseeCode);
						util.setString(3, agreementNumber);
						util.setString(4, scheduleId);
						util.setString(5, stateCode);
						util.setString(6, taxCode);
						util.setDate(7, effetiveDateObject);
						int count = util.executeUpdate();
						if (count == 0)
							throw new TBAFrameworkException();

					}
				}

			} catch (Exception e) {
				e.printStackTrace();
				throw new TBAFrameworkException(e.getLocalizedMessage());
			} finally {
				util.reset();
			}
		}
		return resultDTO;
	}
}
