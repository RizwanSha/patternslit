package patterns.config.authorization.cmn;

import java.sql.Date;

import patterns.config.authorization.CommonTBAUpdate;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;

public class itaxschrateTBAUpdate extends CommonTBAUpdate {

	@Override
	public DTObject processTBAUpdate(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DTObject processEffDateTBAUpdate(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		DTObject resultDTO = new DTObject();
		DBUtil util = dbContext.createUtilInstance();
		DBUtil util1 = dbContext.createUtilInstance();
		if (inputDTO.get(PROCESS_OPTION).equals(AUTHORIZE)) {
			splitSourceKey(inputDTO);
			String entitycode = strsrcKey[0];
			String stateCode = strsrcKey[1];
			String taxCode = strsrcKey[2];
			String effectDate = strsrcKey[3];
			Date effectiveDate = new java.sql.Date(BackOfficeFormatUtils.getDate(effectDate, BackOfficeConstants.TBA_DATE_FORMAT).getTime());
			try {
				util.reset();
				util.setSql("DELETE FROM TAXRATE WHERE ENTITY_CODE=? AND STATE_CODE=? AND TAX_CODE=?");
				util.setString(1, entitycode);
				util.setString(2, stateCode);
				util.setString(3, taxCode);
				util.executeUpdate();

				util1.reset();
				util1.setMode(DBUtil.PREPARED);
				util1.setSql("INSERT INTO TAXRATE(ENTITY_CODE,STATE_CODE,TAX_CODE,EFF_DATE,TAX_ON_AMT_PORTION) SELECT ENTITY_CODE,STATE_CODE,TAX_CODE,EFF_DATE,TAX_ON_AMT_PORTION FROM TAXRATEHIST WHERE ENTITY_CODE=? AND  STATE_CODE=? AND TAX_CODE=? AND EFF_DATE=?");
				util1.setString(1, entitycode);
				util1.setString(2, stateCode);
				util1.setString(3, taxCode);
				util1.setDate(4, effectiveDate);
				util1.executeUpdate();

				util.reset();
				util.setSql("DELETE FROM TAXRATEDTL WHERE ENTITY_CODE=? AND STATE_CODE=? AND TAX_CODE=?");
				util.setString(1, entitycode);
				util.setString(2, stateCode);
				util.setString(3, taxCode);
				util.executeUpdate();

				util1.reset();
				util1.setMode(DBUtil.PREPARED);
				util1.setSql("INSERT INTO TAXRATEDTL(ENTITY_CODE,STATE_CODE,TAX_CODE,DTL_SL,TAX_SCHEDULE_CODE,TAX_RATE) SELECT ENTITY_CODE,STATE_CODE,TAX_CODE,DTL_SL,TAX_SCHEDULE_CODE,TAX_RATE FROM TAXRATEDTLHIST WHERE ENTITY_CODE=? AND STATE_CODE=? AND TAX_CODE=? AND EFF_DATE=?");
				util1.setString(1, entitycode);
				util1.setString(2, stateCode);
				util1.setString(3, taxCode);
				util1.setDate(4, effectiveDate);
				util1.executeUpdate();

			} catch (Exception e) {
				e.printStackTrace();
				throw new TBAFrameworkException(e.getLocalizedMessage());
			} finally {
				util.reset();
				util1.reset();
			}
		}
		return resultDTO;
	}
}