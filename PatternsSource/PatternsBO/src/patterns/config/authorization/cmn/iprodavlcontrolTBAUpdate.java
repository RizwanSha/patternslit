package patterns.config.authorization.cmn;

import java.sql.Date;

import patterns.config.authorization.CommonTBAUpdate;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;

public class iprodavlcontrolTBAUpdate extends CommonTBAUpdate {

	@Override
	public DTObject processTBAUpdate(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DTObject processEffDateTBAUpdate(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		DTObject resultDTO = new DTObject();
		DBUtil util = dbContext.createUtilInstance();
		DBUtil util1 = dbContext.createUtilInstance();
		if (inputDTO.get(PROCESS_OPTION).equals(AUTHORIZE)) {
			splitSourceKey(inputDTO);
			String entitycode = strsrcKey[0];
			String productCode = strsrcKey[1];
			String effectDate = strsrcKey[2];
			Date effectiveDate = new java.sql.Date(BackOfficeFormatUtils.getDate(effectDate, BackOfficeConstants.TBA_DATE_FORMAT).getTime());
			try {
				util.reset();
				util.setSql("DELETE FROM PRODAVLCNTRLMAIN WHERE ENTITY_CODE=? AND PRODUCT_CODE=?");
				util.setString(1, entitycode);
				util.setString(2, productCode);
				util.executeUpdate();

				util1.reset();
				util1.setMode(DBUtil.PREPARED);
				util1.setSql("INSERT INTO PRODAVLCNTRLMAIN(ENTITY_CODE,PRODUCT_CODE,EFF_DATE,AVL_FOR_INDIVIDUALS,AVL_FOR_ORGANIZATIONS,CUST_SEG_CNTRL_FLAG,CUST_ORG_CNTRL_FLAG,CUST_GROUP_TYPE_CNTRL_FLAG,ENABLED,REMARKS) SELECT ENTITY_CODE,PRODUCT_CODE,EFF_DATE,AVL_FOR_INDIVIDUALS,AVL_FOR_ORGANIZATIONS,CUST_SEG_CNTRL_FLAG,CUST_ORG_CNTRL_FLAG,CUST_GROUP_TYPE_CNTRL_FLAG,ENABLED,REMARKS FROM PRODAVLCNTRLHISTM WHERE ENTITY_CODE=? AND PRODUCT_CODE=? AND EFF_DATE=?");
				util1.setString(1, entitycode);
				util1.setString(2, productCode);
				util1.setDate(3, effectiveDate);
				util1.executeUpdate();

				util.reset();
				util.setSql("DELETE FROM PRODAVLSEGDTL WHERE ENTITY_CODE=? AND PRODUCT_CODE=?");
				util.setString(1, entitycode);
				util.setString(2, productCode);
				util.executeUpdate();

				util1.reset();
				util1.setMode(DBUtil.PREPARED);
				util1.setSql("INSERT INTO PRODAVLSEGDTL(ENTITY_CODE,PRODUCT_CODE,SL,CUST_SEGMENT) SELECT ENTITY_CODE,PRODUCT_CODE,SL,CUST_SEGMENT FROM PRODAVLCSEGHISTDTL WHERE ENTITY_CODE=? AND PRODUCT_CODE=? AND EFF_DATE=?");
				util1.setString(1, entitycode);
				util1.setString(2, productCode);
				util1.setDate(3, effectiveDate);
				util1.executeUpdate();

				util.reset();
				util.setSql("DELETE FROM PRODAVLORGDTL WHERE ENTITY_CODE=? AND PRODUCT_CODE=?");
				util.setString(1, entitycode);
				util.setString(2, productCode);
				util.executeUpdate();

				util1.reset();
				util1.setMode(DBUtil.PREPARED);
				util1.setSql("INSERT INTO PRODAVLORGDTL(ENTITY_CODE,PRODUCT_CODE,SL,ORGNIZATION_CODE) SELECT ENTITY_CODE,PRODUCT_CODE,SL,ORGNIZATION_CODE FROM PRODAVLCORGHISTDTL WHERE ENTITY_CODE=? AND PRODUCT_CODE=? AND EFF_DATE=?");
				util1.setString(1, entitycode);
				util1.setString(2, productCode);
				util1.setDate(3, effectiveDate);
				util1.executeUpdate();

				util.reset();
				util.setSql("DELETE FROM PRODAVLGTYPDTL WHERE ENTITY_CODE=? AND PRODUCT_CODE=?");
				util.setString(1, entitycode);
				util.setString(2, productCode);
				util.executeUpdate();

				util1.reset();
				util1.setMode(DBUtil.PREPARED);
				util1.setSql("INSERT INTO PRODAVLGTYPDTL(ENTITY_CODE,PRODUCT_CODE,SL,CUST_GROUP_TYPE) SELECT ENTITY_CODE,PRODUCT_CODE,SL,CUST_GROUP_TYPE FROM PRODAVLCGTYPHISTDTL WHERE ENTITY_CODE=? AND PRODUCT_CODE=? AND EFF_DATE=?");
				util1.setString(1, entitycode);
				util1.setString(2, productCode);
				util1.setDate(3, effectiveDate);
				util1.executeUpdate();

			} catch (Exception e) {
				e.printStackTrace();
				throw new TBAFrameworkException(e.getLocalizedMessage());
			} finally {
				util.reset();
				util1.reset();
			}
		}
		return resultDTO;
	}
}