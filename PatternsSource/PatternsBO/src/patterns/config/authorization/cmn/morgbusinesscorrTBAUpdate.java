package patterns.config.authorization.cmn;

import java.sql.Date;
import patterns.config.authorization.CommonTBAUpdate;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;

/**
 * 
 * 
 * @version 1.0
 * @author Dileep Kumar Reddy T
 * @since 30-Jun-2016 <br>
 *        <b>Modified History</b> <BR>
 *        <U><B> Sl.No Modified Date Author Modified Changes Version </B></U> <br>
 * 
 * 
 */

public class morgbusinesscorrTBAUpdate extends CommonTBAUpdate {
	@Override
	public DTObject processTBAUpdate(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		// TODO Auto-generated method stub

		return null;
	}

	@Override
	public DTObject processEffDateTBAUpdate(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		DTObject resultDTO = new DTObject();
		DBUtil util = dbContext.createUtilInstance();
		DBUtil util1 = dbContext.createUtilInstance();
		if (inputDTO.get(PROCESS_OPTION).equals(AUTHORIZE)) {
			splitSourceKey(inputDTO);
			String entitycode = strsrcKey[0];
			String orgCode = strsrcKey[1];
			String efftDateString = strsrcKey[2];
			Date effetiveDate = new java.sql.Date(BackOfficeFormatUtils.getDate(efftDateString, BackOfficeConstants.TBA_DATE_FORMAT).getTime());
			try {
				util.reset();
				util.setSql("DELETE  FROM ORGBUSINESSCORR WHERE ENTITY_CODE=? AND ORG_CODE=?");
				util.setString(1, entitycode);
				util.setString(2, orgCode);
				util.executeUpdate();

				util1.reset();
				util1.setMode(DBUtil.PREPARED);
				util1.setSql("INSERT INTO ORGBUSINESSCORR(ENTITY_CODE,ORG_CODE,ENABLEDASCORRDENT) SELECT ENTITY_CODE,ORG_CODE,ENABLEDASCORRDENT FROM ORGBUSINESSCORRHIST WHERE ENTITY_CODE=? AND ORG_CODE=? AND EFF_DATE=? ");
				util1.setString(1, entitycode);
				util1.setString(2, orgCode);
				util1.setDate(3, effetiveDate);
				int count = util1.executeUpdate();
				if (count == 0)
					throw new TBAFrameworkException();

				util.reset();
				util.setSql("DELETE  FROM ORGBUSINESSCORRDTL WHERE ENTITY_CODE=? AND ORG_CODE=?");
				util.setString(1, entitycode);
				util.setString(2, orgCode);
				util.executeUpdate();
		
				util1.reset();
				util1.setMode(DBUtil.PREPARED);
				util1.setSql("INSERT INTO ORGBUSINESSCORRDTL(ENTITY_CODE,ORG_CODE,SL,COUNTRY_CODE,GEO_UNIT_STATE_ID,GEO_UNIT_DISTRICT_ID) SELECT ENTITY_CODE,ORG_CODE,SL,COUNTRY_CODE,GEO_UNIT_STATE_ID,GEO_UNIT_DISTRICT_ID FROM ORGBUSINESSCORRHISTDTL WHERE ENTITY_CODE=? AND ORG_CODE=? AND EFF_DATE=? ");
				util1.setString(1, entitycode);
				util1.setString(2, orgCode);
				util1.setDate(3, effetiveDate);
				int count1 = util1.executeUpdate();
				if (count1 == 0)
					throw new TBAFrameworkException();

			} catch (Exception e) {
				e.printStackTrace();
				throw new TBAFrameworkException(e.getLocalizedMessage());
			} finally {
				util.reset();
			}
		}
		return resultDTO;
	}
}
