package patterns.config.authorization.cmn;

import java.sql.Date;

import patterns.config.authorization.CommonTBAUpdate;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;

public class ibkacbalreguTBAUpdate extends CommonTBAUpdate {

	@Override
	public DTObject processTBAUpdate(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		return null;
	}

	@Override
	public DTObject processEffDateTBAUpdate(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		DTObject resultDTO = new DTObject();
		DBUtil util = dbContext.createUtilInstance();
		if (inputDTO.get(PROCESS_OPTION).equals(AUTHORIZE)) {
			splitSourceKey(inputDTO);
			String entitycode = strsrcKey[0];
			String bankLicType = strsrcKey[1];
			String proType = strsrcKey[2];
			String ccyCode = strsrcKey[3];
			String efftDateString = strsrcKey[4];
			Date effectiveDate = new java.sql.Date(BackOfficeFormatUtils.getDate(efftDateString, BackOfficeConstants.TBA_DATE_FORMAT).getTime());
			try {
				util.reset();
				util.setSql("DELETE FROM BKACBALREGU WHERE ENTITY_CODE=? AND BANK_LIC_TYPES=? AND PROD_TYPE=? AND CCY_CODE=? ");
				util.setString(1, entitycode);
				util.setString(2, bankLicType);
				util.setString(3, proType);
				util.setString(4, ccyCode);
				util.executeUpdate();

				util.reset();
				util.setMode(DBUtil.PREPARED);
				util.setSql("INSERT INTO BKACBALREGU(ENTITY_CODE,BANK_LIC_TYPES,PROD_TYPE,CCY_CODE,DAY_END_BAL_REST_REQ,DAY_END_MAX_BALANCE,PER_TXN_REST_REQ,PER_TXN_MAX_ALLOWED)SELECT ENTITY_CODE,BANK_LIC_TYPES,PROD_TYPE,CCY_CODE,DAY_END_BAL_REST_REQ,DAY_END_MAX_BALANCE,PER_TXN_REST_REQ,PER_TXN_MAX_ALLOWED FROM BKACBALREGUHIST WHERE ENTITY_CODE=? AND BANK_LIC_TYPES=? AND PROD_TYPE=? AND CCY_CODE=? AND EFF_DATE=? ");
				util.setString(1, entitycode);
				util.setString(2, bankLicType);
				util.setString(3, proType);
				util.setString(4, ccyCode);
				util.setDate(5, effectiveDate);
				util.executeUpdate();
			} catch (Exception e) {
				e.printStackTrace();
				throw new TBAFrameworkException(e.getLocalizedMessage());
			} finally {
				util.reset();
			}
		}
		return resultDTO;
	}
}