package patterns.config.authorization.cmn;

import java.sql.Date;

import patterns.config.authorization.CommonTBAUpdate;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;

public class mactypeintrateTBAUpdate extends CommonTBAUpdate {

	@Override
	public DTObject processTBAUpdate(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DTObject processEffDateTBAUpdate(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		DTObject resultDTO = new DTObject();
		DBUtil util = dbContext.createUtilInstance();
		if (inputDTO.get(PROCESS_OPTION).equals(AUTHORIZE)) {
			splitSourceKey(inputDTO);
			String entityCode = strsrcKey[0];
			String productCode = strsrcKey[1];
			String accountTypeCode = strsrcKey[2];
			String accountSubTypeCode = strsrcKey[3];
			String currencyCode = strsrcKey[4];
			String effectDate = strsrcKey[5];
			Date effectiveDate = new java.sql.Date(BackOfficeFormatUtils.getDate(effectDate, BackOfficeConstants.TBA_DATE_FORMAT).getTime());
			try {
				util.reset();
				util.setSql("DELETE FROM ACTYPEINTRATE WHERE ENTITY_CODE=? AND PRODUCT_CODE=? AND ACCOUNT_TYPE_CODE=? AND ACCOUNT_SUBTYPE_CODE=? AND CCY_CODE=?");
				util.setString(1, entityCode);
				util.setString(2, productCode);
				util.setString(3, accountTypeCode);
				util.setString(4, accountSubTypeCode);
				util.setString(5, currencyCode);
				util.executeUpdate();

				util.reset();
				util.setMode(DBUtil.PREPARED);
				util.setSql("INSERT INTO ACTYPEINTRATE(ENTITY_CODE,PRODUCT_CODE,ACCOUNT_TYPE_CODE,ACCOUNT_SUBTYPE_CODE,CCY_CODE,EFF_DATE,INT_RATE_TYPE_CODE_NORMAL,INT_RATE_TYPE_CODE_OD,INT_RATE_TYPE_CODE_PENAL) SELECT ENTITY_CODE,PRODUCT_CODE,ACCOUNT_TYPE_CODE,ACCOUNT_SUBTYPE_CODE,CCY_CODE,EFF_DATE,INT_RATE_TYPE_CODE_NORMAL,INT_RATE_TYPE_CODE_OD,INT_RATE_TYPE_CODE_PENAL FROM ACTYPEINTRATEHIST WHERE ENTITY_CODE=? AND PRODUCT_CODE=? AND ACCOUNT_TYPE_CODE=? AND ACCOUNT_SUBTYPE_CODE=? AND CCY_CODE=? AND EFF_DATE=?");
				util.setString(1, entityCode);
				util.setString(2, productCode);
				util.setString(3, accountTypeCode);
				util.setString(4, accountSubTypeCode);
				util.setString(5, currencyCode);
				util.setDate(6, effectiveDate);
				util.executeUpdate();

			} catch (Exception e) {
				e.printStackTrace();
				throw new TBAFrameworkException(e.getLocalizedMessage());
			} finally {
				util.reset();
			}
		}
		return resultDTO;
	}
}