package patterns.config.authorization.cmn;

import java.sql.Date;

import patterns.config.authorization.CommonTBAUpdate;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;

public class iactypeinsbkconfTBAUpdate extends CommonTBAUpdate {

	@Override
	public DTObject processTBAUpdate(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DTObject processEffDateTBAUpdate(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		DTObject resultDTO = new DTObject();
		DBUtil util = dbContext.createUtilInstance();
		if (inputDTO.get(PROCESS_OPTION).equals(AUTHORIZE)) {
			splitSourceKey(inputDTO);
			String entitycode = strsrcKey[0];
			String accountTypeCode = strsrcKey[1];
			String effectDate = strsrcKey[2];
			Date effectiveDate = new java.sql.Date(BackOfficeFormatUtils.getDate(effectDate, BackOfficeConstants.TBA_DATE_FORMAT).getTime());
			try {
				util.reset();
				util.setSql("DELETE FROM ACTYPEINSBKCONF WHERE ENTITY_CODE=? AND ACCOUNT_TYPE_CODE=?");
				util.setString(1, entitycode);
				util.setString(2, accountTypeCode);
				util.executeUpdate();

				util.reset();
				util.setMode(DBUtil.PREPARED);
				util.setSql("INSERT INTO ACTYPEINSBKCONF(ENTITY_CODE,ACCOUNT_TYPE_CODE,EFF_DATE,ENABLED,REMARKS) SELECT ENTITY_CODE,ACCOUNT_TYPE_CODE,EFF_DATE,ENABLED,REMARKS FROM ACTYPEINSBKCONFHIST WHERE ENTITY_CODE=? AND ACCOUNT_TYPE_CODE=? AND EFF_DATE=?");
				util.setString(1, entitycode);
				util.setString(2, accountTypeCode);
				util.setDate(3, effectiveDate);
				util.executeUpdate();

				util.reset();
				util.setSql("DELETE FROM ACTYPEINSBKCONFDTL WHERE ENTITY_CODE=? AND ACCOUNT_TYPE_CODE=?");
				util.setString(1, entitycode);
				util.setString(2, accountTypeCode);
				util.executeUpdate();

				util.reset();
				util.setMode(DBUtil.PREPARED);
				util.setSql("INSERT INTO ACTYPEINSBKCONFDTL(ENTITY_CODE,ACCOUNT_TYPE_CODE,SL,INST_TYPE_CODE,BOOK_CODE) SELECT ENTITY_CODE,ACCOUNT_TYPE_CODE,SL,INST_TYPE_CODE,BOOK_CODE FROM ACTYPEINSBKCONFHISTDTL WHERE ENTITY_CODE=? AND ACCOUNT_TYPE_CODE=? AND EFF_DATE=?");
				util.setString(1, entitycode);
				util.setString(2, accountTypeCode);
				util.setDate(3, effectiveDate);
				util.executeUpdate();

			} catch (Exception e) {
				e.printStackTrace();
				throw new TBAFrameworkException(e.getLocalizedMessage());
			} finally {
				util.reset();
			}
		}
		return resultDTO;
	}
}