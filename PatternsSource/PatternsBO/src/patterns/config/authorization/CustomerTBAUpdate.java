package patterns.config.authorization;

import java.sql.Date;

import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.web.BackOfficeFormatUtils;

public abstract class CustomerTBAUpdate extends CommonTBAUpdate {

	public void euseradallocTBA(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		if (inputDTO.get(PROCESS_OPTION).equals(AUTHORIZE) && inputDTO.get(PROCESS_STAGE).equals(AFTER)) {
			DBUtil dbutil = dbContext.createUtilInstance();
			splitSourceKey(inputDTO);
			String entityCode = strsrcKey[0];
			String userID = strsrcKey[1];
			try {
				dbutil.reset();
				dbutil.setMode(DBUtil.PREPARED);
				dbutil.setSql("INSERT INTO USERSADALLOCHIST SELECT * FROM USERSADALLOC WHERE ENTITY_CODE= ? AND USER_ID= ?");
				dbutil.setString(1, entityCode);
				dbutil.setString(2, userID);
				dbutil.executeUpdate();
			} catch (Exception e) {
				throw new TBAFrameworkException(e.getLocalizedMessage());
			} finally {
				dbutil.reset();
			}
		}

	}

	public void eusersrvcmodelTBA(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		if (inputDTO.get(PROCESS_OPTION).equals(AUTHORIZE) && inputDTO.get(PROCESS_STAGE).equals(AFTER)) {
			DBUtil dbutil = dbContext.createUtilInstance();
			splitSourceKey(inputDTO);
			String entityCode = strsrcKey[0];
			String userID = strsrcKey[1];
			try {
				dbutil.reset();
				dbutil.setMode(DBUtil.PREPARED);
				dbutil.setSql("INSERT INTO USERSSERVICEMDLALLOCHIST SELECT * FROM USERSSERVICEMDLALLOC WHERE ENTITY_CODE= ? AND USER_ID= ?");
				dbutil.setString(1, entityCode);
				dbutil.setString(2, userID);
				dbutil.executeUpdate();
			} catch (Exception e) {
				throw new TBAFrameworkException(e.getLocalizedMessage());
			} finally {
				dbutil.reset();
			}
		}
	}

	public void iauthgroupcompTBA(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		if (inputDTO.get(PROCESS_OPTION).equals(AUTHORIZE) && inputDTO.get(PROCESS_STAGE).equals(BEFORE)) {
			DBUtil dbutil = dbContext.createUtilInstance();
			splitSourceKey(inputDTO);
			String entityCode = strsrcKey[0];
			String customerCode = strsrcKey[1];
			String authorizationGroupCode = strsrcKey[2];
			String effectiveDate = strsrcKey[3];
			Date effectiveDateValue = new java.sql.Date(BackOfficeFormatUtils.getDate(effectiveDate, BackOfficeConstants.TBA_DATE_FORMAT).getTime());
			try {
				dbutil.reset();
				String sql = "DELETE FROM AUTHGROUPDEFDTL WHERE  ENTITY_CODE =? AND CUSTOMER_CODE =? AND AUTH_GROUP_CODE=?  AND  EFFT_DATE = ? ";
				dbutil.reset();
				dbutil.setSql(sql);
				dbutil.setString(1, entityCode);
				dbutil.setString(2, customerCode);
				dbutil.setString(3, authorizationGroupCode);
				dbutil.setDate(4, effectiveDateValue);
				dbutil.executeUpdate();
			} catch (Exception e) {
				throw new TBAFrameworkException(e.getLocalizedMessage());
			} finally {
				dbutil.reset();
			}
		}
	}

	public void esrvreqauthpowerTBA(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		if (inputDTO.get(PROCESS_OPTION).equals(AUTHORIZE) && inputDTO.get(PROCESS_STAGE).equals(BEFORE)) {
			DBUtil dbutil = dbContext.createUtilInstance();
			splitSourceKey(inputDTO);
			String entityCode = strsrcKey[0];
			String customerCode = strsrcKey[1];
			String mandateCode = strsrcKey[2];
			try {
				String sql = "DELETE FROM REQAUTHPOWERSRVCDTL WHERE ENTITY_CODE =? AND CUSTOMER_CODE =? AND MANDATE_CODE=?";
				dbutil.reset();
				dbutil.setSql(sql);
				dbutil.setString(1, entityCode);
				dbutil.setString(2, customerCode);
				dbutil.setString(3, mandateCode);
				dbutil.executeUpdate();
			} catch (Exception e) {
				throw new TBAFrameworkException(e.getLocalizedMessage());
			} finally {
				dbutil.reset();
			}
		}
	}

	public void iservicerolerestrictTBA(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		if (inputDTO.get(PROCESS_OPTION).equals(AUTHORIZE) && inputDTO.get(PROCESS_STAGE).equals(BEFORE)) {
			DBUtil dbutil = dbContext.createUtilInstance();
			splitSourceKey(inputDTO);
			String entityCode = strsrcKey[0];
			String customerCode = strsrcKey[1];
			String serviceCode = strsrcKey[2];
			String effectiveDate = strsrcKey[3];
			Date effectiveDateValue = new java.sql.Date(BackOfficeFormatUtils.getDate(effectiveDate, BackOfficeConstants.TBA_DATE_FORMAT).getTime());
			try {
				dbutil.reset();
				dbutil.setSql("DELETE FROM SERVICEROLERESTRICTDTL WHERE ENTITY_CODE = ? AND CUSTOMER_CODE= ? AND SERVICE_CODE = ? AND EFFT_DATE = ?");
				dbutil.setString(1, entityCode);
				dbutil.setString(2, customerCode);
				dbutil.setString(3, serviceCode);
				dbutil.setDate(4, effectiveDateValue);
				dbutil.executeQuery();
			} catch (Exception e) {
				throw new TBAFrameworkException(e.getLocalizedMessage());
			} finally {
				dbutil.reset();
			}
		}
	}

	public void itopiccfgTBA(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		if (inputDTO.get(PROCESS_OPTION).equals(AUTHORIZE) && inputDTO.get(PROCESS_STAGE).equals(BEFORE)) {
			DBUtil dbutil = dbContext.createUtilInstance();
			splitSourceKey(inputDTO);
			String entityCode = strsrcKey[0];
			String customerCode = strsrcKey[1];
			String code = strsrcKey[2];
			String effectiveDate = strsrcKey[3];
			Date effectiveDateValue = new java.sql.Date(BackOfficeFormatUtils.getDate(effectiveDate, BackOfficeConstants.TBA_DATE_FORMAT).getTime());
			try {
				dbutil.reset();
				dbutil.setSql("DELETE FROM TOPICCFGDTL WHERE ENTITY_CODE = ? AND CUSTOMER_CODE = ? AND TOPIC_CODE = ? AND EFFT_DATE = ?");
				dbutil.setString(1, entityCode);
				dbutil.setString(2, customerCode);
				dbutil.setString(3, code);
				dbutil.setDate(4, effectiveDateValue);
				dbutil.executeUpdate();
			} catch (Exception e) {
				throw new TBAFrameworkException(e.getLocalizedMessage());
			} finally {
				dbutil.reset();
			}
		}
	}

	public void eusertopicallocTBA(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		if (inputDTO.get(PROCESS_OPTION).equals(AUTHORIZE) && inputDTO.get(PROCESS_STAGE).equals(AFTER)) {
			DBUtil dbutil = dbContext.createUtilInstance();
			splitSourceKey(inputDTO);
			String entityCode = strsrcKey[0];
			String userID = strsrcKey[1];
			try {
				dbutil.reset();
				dbutil.setMode(DBUtil.PREPARED);
				dbutil.setSql("INSERT INTO USERSTOPICALLOCHIST SELECT * FROM USERSTOPICALLOC WHERE ENTITY_CODE= ? AND USER_ID= ?");
				dbutil.setString(1, entityCode);
				dbutil.setString(2, userID);
				dbutil.executeUpdate();
			} catch (Exception e) {
				throw new TBAFrameworkException(e.getLocalizedMessage());
			} finally {
				dbutil.reset();
			}
		}
	}

}
