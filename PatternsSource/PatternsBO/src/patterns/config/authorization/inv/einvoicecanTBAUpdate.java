package patterns.config.authorization.inv;

import java.sql.Date;
import java.sql.ResultSet;

import patterns.config.authorization.CommonTBAUpdate;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;

public class einvoicecanTBAUpdate extends CommonTBAUpdate {

	public DTObject processTBAUpdate(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		DTObject resultDTO = new DTObject();
		splitSourceKey(inputDTO);
		if (inputDTO.get(PROCESS_OPTION).equals(AUTHORIZE) && inputDTO.get(PROCESS_STAGE).equals(AFTER)) {
			invoiceRecordAuthorize(dbContext);
		} else {
			if (inputDTO.get(PROCESS_OPTION).equals(REJECT) && inputDTO.get(PROCESS_STAGE).equals(AFTER)) {
				invoiceRecordReject(dbContext);
			}
		}
		return resultDTO;
	}

	public DTObject processEffDateTBAUpdate(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		return null;
	}

	public void invoiceRecordAuthorize(DBContext dbContext) throws TBAFrameworkException {
		long entityCode = Long.parseLong(strsrcKey[0]);
		Date entryDate = new java.sql.Date(BackOfficeFormatUtils.getDate(strsrcKey[1], BackOfficeConstants.TBA_DATE_FORMAT).getTime());
		String entrySl = strsrcKey[2];
		DBUtil util = dbContext.createUtilInstance();
		DBUtil util1 = dbContext.createUtilInstance();
		StringBuffer selQuery = new StringBuffer();
		selQuery.append("SELECT IB.ENTITY_CODE,IB.COMP_ENTRY_DATE,IB.COMP_ENTRY_SL");
		selQuery.append(" FROM INVOICECANDTL IC");
		selQuery.append(" JOIN INVOICEBREAKUP IB ON (IB.ENTITY_CODE=IC.ENTITY_CODE AND IB.LEASE_PRODUCT_CODE=IC.LEASE_PRODUCT_CODE AND IB.INV_YEAR=IC.INV_YEAR AND IB.INV_MONTH=IC.INV_MONTH AND IB.INV_SERIAL=IC.INV_SERIAL)");
		selQuery.append(" WHERE IC.ENTITY_CODE=? AND IC.ENTRY_DATE=? AND IC.ENTRY_SL=?");
		StringBuffer updQuery = new StringBuffer();
		updQuery.append("UPDATE LEASEFINANCESCHEDULE SET COMP_ENTRY_DATE=NULL,COMP_ENTRY_SL=NULL");
		updQuery.append(" WHERE (ENTITY_CODE,LESSEE_CODE,AGREEMENT_NO,SCHEDULE_ID,INV_FOR,DTL_SL)");
		updQuery.append(" IN (SELECT C.ENTITY_CODE,C.LESSEE_CODE,C.AGREEMENT_NO,C.SCHEDULE_ID,CD.FINANCE_SCHED_INV_FOR,CD.FINANCE_SCHED_DTL_SL");
		updQuery.append(" FROM INVCOMPONENT C");
		updQuery.append(" JOIN INVCOMPDTL CD ON (CD.ENTITY_CODE=C.ENTITY_CODE AND CD.ENTRY_DATE=C.ENTRY_DATE AND CD.ENTRY_SL=C.ENTRY_SL AND CD.DTL_SL=1)");
		updQuery.append(" WHERE C.ENTITY_CODE=? AND C.ENTRY_DATE=? AND C.ENTRY_SL=?)");
		try {
			util.reset();
			util.setMode(DBUtil.PREPARED);
			util.setSql(selQuery.toString());
			util.setLong(1, entityCode);
			util.setDate(2, entryDate);
			util.setInt(3, Integer.parseInt(entrySl));
			ResultSet rs = util.executeQuery();
			if (rs.next()) {
				util1.reset();
				util1.setMode(DBUtil.PREPARED);
				util1.setSql(updQuery.toString());
				do {
					util1.setLong(1, Long.parseLong(rs.getString("ENTITY_CODE")));
					util1.setDate(2, rs.getDate("COMP_ENTRY_DATE"));
					util1.setInt(3, rs.getInt("COMP_ENTRY_SL"));
					int count = util1.executeUpdate();
					if (count == 0) {
						throw new TBAFrameworkException();
					}
				} while (rs.next());

			}

		} catch (Exception e) {
			e.printStackTrace();
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			util.reset();
			util1.reset();
		}
	}

	public void invoiceRecordReject(DBContext dbContext) throws TBAFrameworkException {
		long entityCode = Long.parseLong(strsrcKey[0]);
		Date entryDate = new java.sql.Date(BackOfficeFormatUtils.getDate(strsrcKey[1], BackOfficeConstants.TBA_DATE_FORMAT).getTime());
		String entrySl = strsrcKey[2];
		DBUtil util = dbContext.createUtilInstance();
		StringBuffer sqlQuery = new StringBuffer();
		try {
			util.reset();
			util.setMode(DBUtil.PREPARED);
			sqlQuery.append("UPDATE INVOICES SET CANCEL_ENTRY_DATE=null,CANCEL_ENTRY_SL=null");
			sqlQuery.append(" WHERE ENTITY_CODE=? AND CANCEL_ENTRY_DATE=? AND CANCEL_ENTRY_SL=?");
			util.setSql(sqlQuery.toString());
			util.setLong(1, entityCode);
			util.setDate(2, entryDate);
			util.setInt(3, Integer.parseInt(entrySl));
			util.executeUpdate();
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			util.reset();
		}
	}
}
