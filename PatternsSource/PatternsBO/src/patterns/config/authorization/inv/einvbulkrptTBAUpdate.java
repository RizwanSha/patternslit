package patterns.config.authorization.inv;

import java.sql.Date;

import patterns.config.authorization.CommonTBAUpdate;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.CM_LOVREC;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.service.DTObject;
/*
 * The program will be used to Insert record to INVBULKRPTPE table. 
 *
 Author : Pavan kumar.R
 Created Date : 08-April-2017
 Spec Reference PSD-INV-EINVBULKRPT
 Modification History
 -----------------------------------------------------------------------------------------------------
 Sl.No		Modified Date	Author			Modified Changes					Version
 
 -----------------------------------------------------------------------------------------------------

 */
import patterns.config.framework.web.BackOfficeFormatUtils;

public class einvbulkrptTBAUpdate extends CommonTBAUpdate {

	@Override
	public DTObject processTBAUpdate(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		DTObject resultDTO = new DTObject();
		DBUtil util = dbContext.createUtilInstance();
		if (inputDTO.get(PROCESS_OPTION).equals(AUTHORIZE) && inputDTO.get(PROCESS_STAGE).equals(AFTER)) {
			splitSourceKey(inputDTO);
			long entityCode = Long.parseLong(strsrcKey[0]);
			String entryDateString = strsrcKey[1];
			int entryDateSl = Integer.parseInt(strsrcKey[2]);
			Date entryDateObj = new java.sql.Date(BackOfficeFormatUtils.getDate(entryDateString, BackOfficeConstants.TBA_DATE_FORMAT).getTime());
			try {

				util.reset();
				util.setMode(DBUtil.PREPARED);
				util.setSql("INSERT INTO INVBULKRPTPE(ENTITY_CODE,PRINT_ENTRY_DATE,PRINT_ENTRY_SL) VALUES (?,?,?)");
				util.setLong(1, entityCode);
				util.setDate(2, entryDateObj);
				util.setInt(3, entryDateSl);
				util.executeUpdate();

				util.reset();
				util.setMode(DBUtil.PREPARED);
				util.setSql("UPDATE INVOICEBULKRPT SET PROC_STATUS=? WHERE ENTITY_CODE=? AND ENTRY_DATE=? AND ENTRY_SL=?");
				util.setString(1, CM_LOVREC.EINVBULKRPT_JOB_STATUS_T);
				util.setLong(2, entityCode);
				util.setDate(3, entryDateObj);
				util.setInt(4, entryDateSl);
				util.executeUpdate();

			} catch (Exception e) {
				e.printStackTrace();
				throw new TBAFrameworkException(e.getLocalizedMessage());
			} finally {
				util.reset();
			}
		}
		return resultDTO;
	}

	@Override
	public DTObject processEffDateTBAUpdate(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		return null;
	}
}
