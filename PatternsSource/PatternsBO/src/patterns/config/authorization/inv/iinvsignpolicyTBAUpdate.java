package patterns.config.authorization.inv;

import java.sql.Date;
import java.sql.ResultSet;
import patterns.config.authorization.CommonTBAUpdate;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;

public class iinvsignpolicyTBAUpdate extends CommonTBAUpdate {
	public DTObject processEffDateTBAUpdate(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		DTObject resultDTO = new DTObject();
		DBUtil util = dbContext.createUtilInstance();
		if (inputDTO.get(PROCESS_OPTION).equals(AUTHORIZE)) {
			splitSourceKey(inputDTO);
			String entitycode = strsrcKey[0];
			String lesseBranchCode = strsrcKey[1];
			String efftDateString = strsrcKey[3];
			Date effectiveDate = new java.sql.Date(BackOfficeFormatUtils.getDate(efftDateString, BackOfficeConstants.TBA_DATE_FORMAT).getTime());
			try {
				util.reset();
				util.setSql("DELETE FROM INVSIGNPOLICY WHERE ENTITY_CODE=? AND BRANCH_CODE=?");
				util.setString(1, entitycode);
				util.setString(2, lesseBranchCode);
				util.executeUpdate();
				util.reset();
				util.reset();
				util.setSql("SELECT ENABLED FROM INVSIGNPOLICYHIST WHERE ENTITY_CODE=? AND BRANCH_CODE=? AND EFF_DATE=?");
				util.setString(1, entitycode);
				util.setString(2, lesseBranchCode);
				util.setDate(4, effectiveDate);
				ResultSet rs = util.executeQuery();
				if (rs.next()) {
					if (rs.getString("ENABLED").equals("1")) {
						util.reset();
						util.setMode(DBUtil.PREPARED);
						util.setSql("INSERT INTO INVSIGNPOLICY(ENTITY_CODE,BRANCH_CODE,EFF_DATE,FIRST_SIGN_STAFF_CODE,SECOND_SIGN_STAFF_CODE) SELECT ENTITY_CODE,BRANCH_CODE,EFF_DATE,FIRST_SIGN_STAFF_CODE,SECOND_SIGN_STAFF_CODE FROM INVSIGNPOLICYHIST WHERE ENTITY_CODE=? AND BRANCH_CODE=? AND  EFF_DATE=?");
						util.setString(1, entitycode);
						util.setString(2, lesseBranchCode);
						util.setDate(4, effectiveDate);
						if (util.executeUpdate()==0)
							throw new TBAFrameworkException();
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
				throw new TBAFrameworkException(e.getLocalizedMessage());
			} finally {
				util.reset();

			}
		}
		return resultDTO;
	}

	@Override
	public DTObject processTBAUpdate(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		// TODO Auto-generated method stub
		return null;
	}
}