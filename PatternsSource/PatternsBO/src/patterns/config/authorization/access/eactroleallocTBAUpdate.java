package patterns.config.authorization.access;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.Types;

import patterns.config.authorization.CommonTBAUpdate;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.framework.web.PasswordUtils;

/**
 * 
 * @author Swaroopa-0003
 * 
 */
public class eactroleallocTBAUpdate extends CommonTBAUpdate {

	@Override
	public DTObject processTBAUpdate(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		DTObject resultDTO = new DTObject();
		if (inputDTO.get(PROCESS_OPTION).equals(AUTHORIZE) && inputDTO.get(PROCESS_STAGE).equals(AFTER)) {
			DBUtil dbutil = dbContext.createUtilInstance();
			splitSourceKey(inputDTO);
			String entityCode = strsrcKey[0];
			String userID = strsrcKey[1];
			String roleCode = strsrcKey[2];
			try {
				dbutil.reset();
				dbutil.setMode(DBUtil.PREPARED);
				dbutil.setSql("INSERT INTO USERSACTROLEALLOCHIST SELECT * FROM USERSACTROLEALLOC WHERE ENTITY_CODE= ? AND USER_ID= ? AND ROLE_CODE= ?");
				dbutil.setString(1, entityCode);
				dbutil.setString(2, userID);
				dbutil.setString(3, roleCode);
				dbutil.executeUpdate();
			} catch (Exception e) {
				throw new TBAFrameworkException(e.getLocalizedMessage());
			} finally {
				dbutil.reset();
			}
		}
		return resultDTO;
	}

	@Override
	public DTObject processEffDateTBAUpdate(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		return null;
		// TODO Auto-generated method stub

	}

}
