package patterns.config.authorization.access;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.Types;

import patterns.config.authorization.CommonTBAUpdate;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.framework.web.PasswordUtils;

/**
 * 
 * @author Swaroopa-0003
 * 
 */
public class isyscpmTBAUpdate extends CommonTBAUpdate {

	@Override
	public DTObject processTBAUpdate(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		return null;
	}

	@Override
	public DTObject processEffDateTBAUpdate(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		DTObject resultDTO = new DTObject();
		DBUtil util = dbContext.createUtilInstance();
		DBUtil util1 = dbContext.createUtilInstance();
		if (inputDTO.get(PROCESS_OPTION).equals(AUTHORIZE)) {
			splitSourceKey(inputDTO);
			String entitycode = strsrcKey[0];
			String efftDateString = strsrcKey[1];
			Date effetiveDate = new java.sql.Date(BackOfficeFormatUtils.getDate(efftDateString, BackOfficeConstants.TBA_DATE_FORMAT).getTime());
			try {
				util.reset();
				util.setSql("DELETE  FROM SYSCPM WHERE ENTITY_CODE=? ");
				util.setString(1, entitycode);
				util.executeUpdate();

				util1.reset();
				util1.setMode(DBUtil.PREPARED);
				util1.setSql("INSERT INTO SYSCPM  SELECT * FROM SYSCPMHIST WHERE ENTITY_CODE=?  AND EFFT_DATE=? ");
				util1.setString(1, entitycode);
				util1.setDate(2, effetiveDate);
				util1.executeUpdate();

			} catch (Exception e) {
				e.printStackTrace();
				throw new TBAFrameworkException(e.getLocalizedMessage());
			} finally {
				util.reset();
			}
		}
		return resultDTO;

	}

}
