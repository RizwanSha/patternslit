package patterns.config.authorization.access;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.Types;

import patterns.config.authorization.CommonTBAUpdate;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.framework.web.PasswordUtils;

/**
 * 
 * @author Swaroopa-0003
 * 
 */
public class euserstatusTBAUpdate extends CommonTBAUpdate {

	@Override
	public DTObject processTBAUpdate(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		DTObject resultDTO = new DTObject();
		if (inputDTO.get(PROCESS_OPTION).equals(AUTHORIZE) && inputDTO.get(PROCESS_STAGE).equals(AFTER)) {
			if (inputDTO.get("ACTION_TYPE").equals(MODIFY)) {
				DBUtil dbutil = dbContext.createUtilInstance();
				splitSourceKey(inputDTO);
				String entityCode = strsrcKey[0];
				String userID = strsrcKey[1];
				try {
					dbutil.reset();
					dbutil.setMode(DBUtil.CALLABLE);
					dbutil.setSql("{CALL SP_USER_STATUS_UPDATE(?,?,?,?,?,?)}");
					dbutil.setString(1, entityCode);
					dbutil.setString(2, userID);
					dbutil.setString(3, inputDTO.get("USER_ID"));
					dbutil.setString(4, "UPDATION @ USER STATUS CHANGE");
					dbutil.setString(5, RegularConstants.COLUMN_DISABLE);
					dbutil.registerOutParameter(6, Types.VARCHAR);
					dbutil.execute();
					if (!dbutil.getString(6).equals(RegularConstants.SP_SUCCESS)) {
						throw new Exception(dbutil.getString(6));
					}
					dbutil.reset();
					dbutil.setMode(DBUtil.PREPARED);
					dbutil.setSql("SELECT 1 FROM USERS WHERE ENTITY_CODE = ? AND USER_ID = ? AND STATUS = 'A'");
					dbutil.setString(1, entityCode);
					dbutil.setString(2, userID);
					ResultSet rset = dbutil.executeQuery();
					if (rset.next()) {
						dbutil.reset();
						dbutil.setMode(DBUtil.PREPARED);
						dbutil.setSql("DELETE FROM USERSPWDINVALID WHERE ENTITY_CODE = ? AND USER_ID = ?");
						dbutil.setString(1, entityCode);
						dbutil.setString(2, userID);
						dbutil.executeUpdate();
					}
				} catch (Exception e) {
					throw new TBAFrameworkException(e.getLocalizedMessage());
				} finally {
					dbutil.reset();
				}
			}
		}
		return resultDTO;
	}

	@Override
	public DTObject processEffDateTBAUpdate(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		return null;
		// TODO Auto-generated method stub

	}

}
