package patterns.config.authorization.access;

import java.sql.Date;
import patterns.config.authorization.CommonTBAUpdate;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;

public class ibranchprintqcfgTBAUpdate extends CommonTBAUpdate {
	@Override
	public DTObject processTBAUpdate(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		// TODO Auto-generated method stub

		return null;
	}

	@Override
	public DTObject processEffDateTBAUpdate(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		DTObject resultDTO = new DTObject();
		DBUtil util = dbContext.createUtilInstance();
		DBUtil util1 = dbContext.createUtilInstance();
		if (inputDTO.get(PROCESS_OPTION).equals(AUTHORIZE)) {
			splitSourceKey(inputDTO);
			String entitycode = strsrcKey[0];
			String branchCode = strsrcKey[1];
			String efftDateString = strsrcKey[2];
			Date effetiveDate = new java.sql.Date(BackOfficeFormatUtils.getDate(efftDateString, BackOfficeConstants.TBA_DATE_FORMAT).getTime());
			try {
				util.reset();
				util.setSql("DELETE  FROM BRNPRNQCFG WHERE ENTITY_CODE=? AND BRN_CODE=?");
				util.setString(1, entitycode);
				util.setString(2, branchCode);
				util.executeUpdate();

				util1.reset();
				util1.setMode(DBUtil.PREPARED);
				util1.setSql("INSERT INTO BRNPRNQCFG(ENTITY_CODE,BRN_CODE) SELECT ENTITY_CODE,BRN_CODE FROM BRNPRNQCFGHIST WHERE ENTITY_CODE=? AND BRN_CODE=? AND EFFT_DATE=? ");
				util1.setString(1, entitycode);
				util1.setString(2, branchCode);
				util1.setDate(3, effetiveDate);
				util1.executeUpdate();
				
				util.reset();
				util.setSql("DELETE FROM BRNPRNQCFGDTL WHERE ENTITY_CODE=? AND BRN_CODE=?");
				util.setString(1, entitycode);
				util.setString(2, branchCode);
				util.executeUpdate();
				
				util1.reset();
				util1.setMode(DBUtil.PREPARED);
				util1.setSql("INSERT INTO BRNPRNQCFGDTL(ENTITY_CODE,BRN_CODE,SL,PRNTYPE_ID,PRNQ_ID) SELECT ENTITY_CODE,BRN_CODE,SL,PRNTYPE_ID,PRNQ_ID FROM BRNPRNQCFGHISTDTL WHERE ENTITY_CODE=? AND BRN_CODE=? AND EFFT_DATE=? ");
				util1.setString(1, entitycode);
				util1.setString(2, branchCode);
				util1.setDate(3, effetiveDate);
				util1.executeUpdate();

			} catch (Exception e) {
				e.printStackTrace();
				throw new TBAFrameworkException(e.getLocalizedMessage());
			} finally {
				util.reset();
			}
		}
		return resultDTO;
	}
}