package patterns.config.authorization.access;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.Types;

import patterns.config.authorization.CommonTBAUpdate;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;
import patterns.config.framework.web.PasswordUtils;

/**
 * 
 * @author Swaroopa-0003
 * 
 */
public class eipuserTBAUpdate extends CommonTBAUpdate {

	@Override
	public DTObject processTBAUpdate(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		return null;
	}

	@Override
	public DTObject processEffDateTBAUpdate(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		DTObject resultDTO = new DTObject();
		DBUtil util = dbContext.createUtilInstance();
		DBUtil util1 = dbContext.createUtilInstance();
		if (inputDTO.get(PROCESS_OPTION).equals(AUTHORIZE)) {
			splitSourceKey(inputDTO);
			String entitycode = strsrcKey[0];
			String consoleCode = strsrcKey[1];
			String efftDateString = strsrcKey[2];
			Date effetiveDate = new java.sql.Date(BackOfficeFormatUtils.getDate(efftDateString, BackOfficeConstants.TBA_DATE_FORMAT).getTime());
			try {
				util.reset();
				util.setSql("DELETE  FROM USERIPRESTRICTHIST WHERE ENTITY_CODE=? AND USER_ID=?");
				util.setString(1, entitycode);
				util.setString(2, consoleCode);
				util.executeUpdate();

				util1.reset();
				util1.setMode(DBUtil.PREPARED);
				util1.setSql("INSERT INTO USERIPRESTRICT(ENTITY_CODE,USER_ID,EFFT_DATE,REMARKS) SELECT ENTITY_CODE,USER_ID,EFFT_DATE,REMARKS FROM USERIPRESTRICTHIST WHERE ENTITY_CODE=? AND USER_ID=? AND EFFT_DATE=? ");
				util1.setString(1, entitycode);
				util1.setString(2, consoleCode);
				util1.setDate(3, effetiveDate);
				util1.executeUpdate();
				
				util.reset();
				util.setSql("DELETE FROM USERIPRESTRICTDTL WHERE ENTITY_CODE=? AND CONSOLE_CODE=?");
				util.setString(1, entitycode);
				util.setString(2, consoleCode);
				util.executeUpdate();
				
				util1.reset();
				util1.setMode(DBUtil.PREPARED);
				util1.setSql("INSERT INTO USERIPRESTRICTHIST(ENTITY_CODE,USER_ID,SL,IPADDRESS) SELECT ENTITY_CODE,USER_ID,SL,IPADDRESS FROM USERIPRESTRICTHISTDTL WHERE ENTITY_CODE=? AND USER_ID=? AND EFFT_DATE=? ");
				util1.setString(1, entitycode);
				util1.setString(2, consoleCode);
				util1.setDate(3, effetiveDate);
				util1.executeUpdate();

			} catch (Exception e) {
				e.printStackTrace();
				throw new TBAFrameworkException(e.getLocalizedMessage());
			} finally {
				util.reset();
			}
		}
		return resultDTO;

	}

}
