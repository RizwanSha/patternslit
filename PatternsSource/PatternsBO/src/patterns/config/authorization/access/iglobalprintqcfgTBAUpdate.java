package patterns.config.authorization.access;

import java.sql.Date;

import java.sql.ResultSet;

import patterns.config.authorization.CommonTBAUpdate;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;

public class iglobalprintqcfgTBAUpdate extends CommonTBAUpdate {

	@Override
	public DTObject processTBAUpdate(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		DTObject resultDTO = new DTObject();
		DBUtil util = dbContext.createUtilInstance();
		DBUtil util1 = dbContext.createUtilInstance();
		if (inputDTO.get(PROCESS_OPTION).equals(AUTHORIZE)) {
			splitSourceKey(inputDTO);
			String entitycode = strsrcKey[0];
			String efftDateString = strsrcKey[1];
			Date effectiveDate = new java.sql.Date(BackOfficeFormatUtils.getDate(efftDateString, BackOfficeConstants.TBA_DATE_FORMAT).getTime());
			try {
				util.reset();
				util.setSql("DELETE  FROM  GLOBALPRNQCFG WHERE ENTITY_CODE=?");
				util.setString(1, entitycode);
				util.executeUpdate();
				util.reset();
				util.setMode(DBUtil.PREPARED);
				util.setSql("SELECT  * FROM  GLOBALPRNQCFGHIST  WHERE ENTITY_CODE=?  AND  EFFT_DATE=? ");
				util.setString(1, entitycode);
				util.setDate(2, effectiveDate);
				ResultSet globalprint_hist = util.executeQuery();
				while(globalprint_hist.next()) {
					util1.reset();
					util1.setMode(DBUtil.PREPARED);
					util1.setSql("INSERT INTO GLOBALPRNQCFG(ENTITY_CODE) VALUES(?)");
					util1.setString(1, entitycode);
					util1.executeUpdate();
				}
				util.reset();
				util.setSql("DELETE  FROM GLOBALPRNQCFGDTL WHERE ENTITY_CODE=?  ");
				util.setString(1, entitycode);
				util.executeUpdate();
				util.reset();
				util.setMode(DBUtil.PREPARED);
				util.setSql("SELECT * FROM  GLOBALPRNQCFGHISTDTL  WHERE ENTITY_CODE=? AND  EFFT_DATE=?");
				util.setString(1, entitycode);
				util.setDate(2, effectiveDate);
				ResultSet globalprint_histdtl =util.executeQuery();
				while(globalprint_histdtl.next()) {
					util1.reset();
					util1.setMode(DBUtil.PREPARED);
					util1.setSql("INSERT INTO  GLOBALPRNQCFGDTL(ENTITY_CODE,SL,PRNTYPE_ID,PRNQ_ID) VALUES(?,?,?,?)");
					util1.setString(1, entitycode);
					util1.setString(2, globalprint_histdtl.getString("SL"));
					util1.setString(3, globalprint_histdtl.getString("PRNTYPE_ID"));
					util1.setString(4, globalprint_histdtl.getString("PRNQ_ID"));
					util1.executeUpdate();
				}
				
			} catch (Exception e) {
				e.printStackTrace();
				throw new TBAFrameworkException(e.getLocalizedMessage());
			} finally {
				util.reset();
				util1.reset();
			}
		}
		return resultDTO;
	}

	@Override
	public DTObject processEffDateTBAUpdate(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		return null;
		// TODO Auto-generated method stub

	}

}
