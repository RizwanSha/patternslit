package patterns.config.authorization.access;

import java.lang.reflect.Method;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import patterns.config.authorization.CommonTBAUpdate;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.MessageParam;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.PasswordUtils;

/**
 * 
 * @author Swaroopa-0003
 * 
 */
public class epwdresetTBAUpdate extends CommonTBAUpdate {

	public DTObject processTBAUpdate(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		DTObject resultDTO = new DTObject();
		if (inputDTO.get(PROCESS_OPTION).equals(AUTHORIZE) && inputDTO.get(PROCESS_STAGE).equals(AFTER)) {
			if (inputDTO.get("ACTION_TYPE").equals(MODIFY)) {
				DBUtil dbutil = dbContext.createUtilInstance();
				splitSourceKey(inputDTO);
				String entityCode = strsrcKey[0];
				String userID = strsrcKey[1];
				String partitionNo = inputDTO.get("PARTITION_NO");
				Date cbd = (java.sql.Date) inputDTO.getObject("CBD");
				String pwdDeliveryChoice = "";
				String pwdDeliveryType = "";
				try {
					String clearPin = "";
					String salt = "";
					String encryptedPassword = "";
					String remarks = "";
					dbutil.reset();
					dbutil.setMode(DBUtil.PREPARED);
					dbutil.setSql("SELECT PWD_DELIVERY_MECHANISM , PWD_MANUAL_TYPE FROM SYSCPM WHERE ENTITY_CODE = ? AND EFFT_DATE = (SELECT MAX(EFFT_DATE) FROM SYSCPM WHERE ENTITY_CODE = ? AND EFFT_DATE <= FN_GETCD(?))");
					dbutil.setString(1, entityCode);
					dbutil.setString(2, entityCode);
					dbutil.setString(3, entityCode);
					ResultSet resultSet = dbutil.executeQuery();
					if (resultSet.next()) {
						pwdDeliveryChoice = resultSet.getString(1);
						pwdDeliveryType = resultSet.getString(2);

					}
					if (pwdDeliveryChoice.equals("4") && pwdDeliveryType.equals("2")) {
						dbutil.reset();
						dbutil.setMode(DBUtil.PREPARED);
						dbutil.setSql("SELECT CLEAR_PIN,SALT,ENCYPT_PASSWORD,REMARKS FROM USERSPWDRESET WHERE ENTITY_CODE =? AND USER_ID=? ");
						dbutil.setString(1, entityCode);
						dbutil.setString(2, userID);
						ResultSet rs = dbutil.executeQuery();
						if (rs.next()) {
							clearPin = rs.getString("CLEAR_PIN");
							salt = rs.getString("SALT");
							encryptedPassword = rs.getString("ENCYPT_PASSWORD");
							remarks = rs.getString("REMARKS");
						}
					} else {
						clearPin = PasswordUtils.generateUserPassword(dbContext, entityCode, userID, cbd);
						salt = PasswordUtils.getSalt();
						encryptedPassword = PasswordUtils.getEncrypted(clearPin, userID, salt);
					}

					dbutil.reset();
					dbutil.setMode(DBUtil.CALLABLE);
					dbutil.setSql("CALL SP_USER_PWD_UPDATE(?,?,?,?,?,?,?,?,?,?,?,?,?)");
					dbutil.setString(1, partitionNo);
					dbutil.setString(2, entityCode);
					dbutil.setString(3, userID);
					dbutil.setString(4, encryptedPassword);
					dbutil.setString(5, salt);
					dbutil.setString(6, inputDTO.get("USER_ID"));
					if (pwdDeliveryChoice.equals("4") && pwdDeliveryType.equals("2")) {
						dbutil.setString(7, remarks);
						dbutil.setString(8, RegularConstants.COLUMN_ENABLE);
						dbutil.setString(9, RegularConstants.COLUMN_DISABLE);
					} else {
						dbutil.setString(7, "GENERATION @ USER CREATION");
						dbutil.setString(8, RegularConstants.COLUMN_ENABLE);
						dbutil.setString(9, RegularConstants.COLUMN_ENABLE);

					}
					dbutil.setString(10, clearPin);
					dbutil.setString(11, RegularConstants.COLUMN_ENABLE);
					dbutil.registerOutParameter(12, Types.TIMESTAMP);
					dbutil.registerOutParameter(13, Types.VARCHAR);
					dbutil.execute();
					if (!dbutil.getString(13).equals(RegularConstants.SP_SUCCESS)) {
						resultDTO.set(RegularConstants.ADDITIONAL_INFO, dbutil.getString(13));
						throw new Exception(dbutil.getString(13));

					} else {
						if (pwdDeliveryChoice.equals("4") && pwdDeliveryType.equals("1")) {
							MessageParam mesageParam = new MessageParam();
							mesageParam.setCode(BackOfficeErrorCodes.HMS_USER_GEN_PIN);
							List<String> genPin = new ArrayList<String>();
							genPin.add(String.valueOf(clearPin));
							mesageParam.setParameters(genPin);
							resultDTO.setObject(RegularConstants.ADDITIONAL_INFO, mesageParam);
						}

						// @author Pavan Added ON 17-03-2017 Start
						else if (pwdDeliveryChoice.equals("1")) {
							DTObject formDTO = new DTObject();
							dbutil.reset();
							dbutil.setMode(DBUtil.PREPARED);
							dbutil.setSql("SELECT EMAIL_ID,USER_NAME,FN_NEXTVAL('SEQ_EMAIL') AS INVENTORY_NO FROM USERS WHERE PARTITION_NO = ? AND USER_ID = ?");
							dbutil.setInt(1, Integer.parseInt(partitionNo));
							dbutil.setString(2, userID);
							ResultSet reset = dbutil.executeQuery();
							if (reset.next()) {
								List<String> invList = new ArrayList<String>();
								List<String> toEmailIdList = new ArrayList<String>();
								invList.add(reset.getString("INVENTORY_NO"));
								toEmailIdList.add(reset.getString("EMAIL_ID"));
								formDTO.setObject("TO_EMAIL_ID_LIST", toEmailIdList);
								formDTO.set("PASSWORD", clearPin);
								formDTO.set("USER_ID", userID);
								formDTO.set("USER_NAME", reset.getString("USER_NAME"));
								formDTO.set("INVENTORY_NO", reset.getString("INVENTORY_NO"));
								formDTO.set("ENTITY_CODE", entityCode);
								formDTO.set("INTERFACE_CODE", "DISEMAIL");
								String templateCode = "PASSWORD_REGENERATE";
								formDTO.set("TEMPLATE_CODE", templateCode);
								if (pushDataToLogTable(dbContext, formDTO)) {
									MessageParam mesageParam = new MessageParam();
									mesageParam.setCode(BackOfficeErrorCodes.USER_GEN_PIN_EMAIL);
									mesageParam.setParameters(toEmailIdList);
									resultDTO.setObject(RegularConstants.ADDITIONAL_INFO, mesageParam);
									formDTO.setObject("INVENTORY_NO", invList);
									formDTO.set("ENTITY_CODE", entityCode);
								}
							} else {
								throw new TBAFrameworkException("Issue in generating Email");
							}
						}
						// @author Pavan Added ON 17-03-2017 End

					}

				} catch (Exception e) {
					throw new TBAFrameworkException(e.getLocalizedMessage());
				} finally {
					dbutil.reset();
				}
			}
		}
		return resultDTO;
	}

	@Override
	public DTObject processEffDateTBAUpdate(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		return null;
		// TODO Auto-generated method stub

	}

	// @author Pavan Added ON 17-03-2017 Start
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private DTObject invokeCommunicationHandler(DBContext dbcontext, DTObject inputObject) throws TBAFrameworkException {
		DTObject resultDTO = null;
		Class qmClass = null;
		Object qmObject = null;
		Method getDataMethod = null;
		Class[] parameterTypes = new Class[] { DBContext.class, DTObject.class };
		Object[] params = new Object[] { dbcontext, inputObject };
		try {
			qmClass = Class.forName(inputObject.get("CLASS_NAME"));
			qmObject = qmClass.newInstance();
			getDataMethod = qmClass.getMethod(inputObject.get("METHOD_NAME"), parameterTypes);
			resultDTO = (DTObject) getDataMethod.invoke(qmObject, params);
		} catch (Exception e) {
			throw new TBAFrameworkException(e.getLocalizedMessage());
		}
		return resultDTO;
	}

	public boolean pushDataToLogTable(DBContext dbcontext, DTObject formDTO) throws TBAFrameworkException {
		try {
			boolean statusType = false;
			String className = "com.patterns.jobs.message.handler.CommunicationHandler";
			String methodName = "dispatchEMailWithInventoryNumber";
			formDTO.set("CLASS_NAME", className);
			formDTO.set("METHOD_NAME", methodName);
			String status = invokeCommunicationHandler(dbcontext, formDTO).get("RESULT");
			if (status.equals(TBAProcessStatus.FAILURE.toString())) {
				throw new TBAFrameworkException("Issue in generating Email");
			} else
				statusType = true;
			return statusType;
		} catch (Exception e) {
			e.printStackTrace();
			throw new TBAFrameworkException(e.getLocalizedMessage());

		}

	}
	// @author Pavan Added ON 17-03-2017 End
} 