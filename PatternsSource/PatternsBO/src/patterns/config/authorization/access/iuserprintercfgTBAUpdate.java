package patterns.config.authorization.access;

import java.sql.Date;
import patterns.config.authorization.CommonTBAUpdate;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;

public class iuserprintercfgTBAUpdate extends CommonTBAUpdate {
	@Override
	public DTObject processTBAUpdate(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		// TODO Auto-generated method stub

		return null;
	}

	@Override
	public DTObject processEffDateTBAUpdate(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		DTObject resultDTO = new DTObject();
		DBUtil util = dbContext.createUtilInstance();
		if (inputDTO.get(PROCESS_OPTION).equals(AUTHORIZE)) {
			splitSourceKey(inputDTO);
			String entitycode = strsrcKey[0];
			String userId = strsrcKey[1];
			String efftDateString = strsrcKey[2];
			Date effetiveDate = new java.sql.Date(BackOfficeFormatUtils.getDate(efftDateString, BackOfficeConstants.TBA_DATE_FORMAT).getTime());
			try {
				util.reset();
				util.setSql("DELETE  FROM USERPRINTERCFG WHERE ENTITY_CODE=? AND USER_ID=? ");
				util.setString(1, entitycode);
				util.setString(2, userId);
				util.executeUpdate();

				util.reset();
				util.setMode(DBUtil.PREPARED);
				util.setSql("INSERT INTO USERPRINTERCFG(ENTITY_CODE,USER_ID) SELECT ENTITY_CODE,USER_ID FROM USERPRINTERCFGHIST WHERE ENTITY_CODE=? AND USER_ID=? AND EFFT_DATE=? ");
				util.setString(1, entitycode);
				util.setString(2, userId);
				util.setDate(3, effetiveDate);
				util.executeUpdate();

				util.reset();
				util.setSql("DELETE  FROM USERPRINTERCFGDTL WHERE ENTITY_CODE=? AND USER_ID=?");
				util.setString(1, entitycode);
				util.setString(2, userId);
				util.executeUpdate();

				util.reset();
				util.setMode(DBUtil.PREPARED);
				util.setSql("INSERT INTO USERPRINTERCFGDTL(ENTITY_CODE,USER_ID,SL,PRNTYPE_ID,PRINTER_ID,DEFAULT_PRINTER) SELECT ENTITY_CODE,USER_ID,SL,PRNTYPE_ID,PRINTER_ID,DEFAULT_PRINTER FROM USERPRINTERCFGHISTDTL WHERE ENTITY_CODE=? AND USER_ID=? AND EFFT_DATE=?");
				util.setString(1, entitycode);
				util.setString(2, userId);
				util.setDate(3, effetiveDate);
				util.executeUpdate();

				util.reset();
				util.setSql("DELETE  FROM USERPRINTERCFGPGM WHERE ENTITY_CODE=? AND USER_ID=?");
				util.setString(1, entitycode);
				util.setString(2, userId);
				util.executeUpdate();

				util.reset();
				util.setMode(DBUtil.PREPARED);
				util.setSql("INSERT INTO USERPRINTERCFGPGM(ENTITY_CODE,USER_ID,SL,PGM_ID,PRINTER_ID) SELECT ENTITY_CODE,USER_ID,SL,PGM_ID,PRINTER_ID FROM USERPRINTERCFGHISTPGM WHERE ENTITY_CODE=? AND USER_ID=? AND EFFT_DATE=?");
				util.setString(1, entitycode);
				util.setString(2, userId);
				util.setDate(3, effetiveDate);
				util.executeUpdate();

			} catch (Exception e) {
				e.printStackTrace();
				throw new TBAFrameworkException(e.getLocalizedMessage());
			} finally {
				util.reset();
			}
		}
		return resultDTO;
	}
}