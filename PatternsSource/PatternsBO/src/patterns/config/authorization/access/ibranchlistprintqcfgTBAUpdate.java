package patterns.config.authorization.access;

/*

 *
 Author : Pavan kumar.R
 Created Date : 20-FEB-2015
 Spec Reference: PSD-ACC-38
 Modification History
 ----------------------------------------------------------------------
 Sl.No		Modified Date	Author			Modified Changes	Version
 ----------------------------------------------------------------------

 */

import java.sql.Date;

import patterns.config.authorization.CommonTBAUpdate;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;

public class ibranchlistprintqcfgTBAUpdate extends CommonTBAUpdate {
	@Override
	public DTObject processTBAUpdate(DTObject inputDTO, DBContext dbContext)
			throws TBAFrameworkException {
		// TODO Auto-generated method stub

		return null;
	}

	public DTObject processEffDateTBAUpdate(DTObject inputDTO,
			DBContext dbContext) throws TBAFrameworkException {
		DTObject resultDTO = new DTObject();
		DBUtil util = dbContext.createUtilInstance();
		int count = 0;
		int countDL = 0;
		if (inputDTO.get(PROCESS_OPTION).equals(AUTHORIZE)) {
			splitSourceKey(inputDTO);
			String entitycode = strsrcKey[0];
			String branchListCode = strsrcKey[1];
			String efftDateString = strsrcKey[2];

			Date effectiveDate = new java.sql.Date(BackOfficeFormatUtils.getDate(efftDateString,BackOfficeConstants.TBA_DATE_FORMAT).getTime());
			try {
				util.reset();
				util.setMode(DBUtil.PREPARED);
				util.setSql("DELETE  FROM BRNLISTPRNQCFG WHERE ENTITY_CODE=? AND BRNLIST_CODE=? ");
				util.setString(1, entitycode);
				util.setString(2, branchListCode);
				util.executeUpdate();

				util.reset();
				util.setMode(DBUtil.PREPARED);
				util.setSql("INSERT INTO BRNLISTPRNQCFG(ENTITY_CODE,BRNLIST_CODE) SELECT ENTITY_CODE,BRNLIST_CODE FROM BRNLISTPRNQCFGHIST WHERE ENTITY_CODE=? AND BRNLIST_CODE =? AND EFFT_DATE=?");
				util.setString(1, entitycode);
				util.setString(2, branchListCode);
				util.setDate(3, effectiveDate);
				count = util.executeUpdate();
				if (count < 0) {
					throw new TBAFrameworkException();
				}
				util.setMode(DBUtil.PREPARED);
				util.setSql("DELETE  FROM BRNLISTPRNQCFGDTL WHERE ENTITY_CODE=? AND BRNLIST_CODE=? ");
				util.setString(1, entitycode);
				util.setString(2, branchListCode);
				util.executeUpdate();

				util.reset();
				util.setMode(DBUtil.PREPARED);
				util.setSql("INSERT INTO BRNLISTPRNQCFGDTL(ENTITY_CODE,BRNLIST_CODE,SL,PRNTYPE_ID,PRNQ_ID) SELECT ENTITY_CODE,BRNLIST_CODE,SL,PRNTYPE_ID,PRNQ_ID FROM BRNLISTPRNQCFGHISTDTL WHERE ENTITY_CODE=? AND BRNLIST_CODE =?  AND EFFT_DATE=? ");
				util.setString(1, entitycode);
				util.setString(2, branchListCode);
				util.setDate(3, effectiveDate);
				countDL = util.executeUpdate();
				if (countDL < 0) {
					throw new TBAFrameworkException();
				}

			} catch (Exception e) {
				e.printStackTrace();
				throw new TBAFrameworkException(e.getLocalizedMessage());
			} finally {
				util.reset();
			}
		}
		return resultDTO;
	}

}