package patterns.config.authorization.access;

import java.lang.reflect.Method;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import patterns.config.authorization.CommonTBAUpdate;
import patterns.config.framework.bo.BackOfficeErrorCodes;
import patterns.config.framework.bo.MessageParam;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.process.TBAProcessStatus;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.PasswordUtils;

/**
 * 
 * @author Swaroopa-0003
 * 
 */
public class muserTBAUpdate extends CommonTBAUpdate {

	public DTObject processTBAUpdate(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		DTObject resultDTO = new DTObject();
		if (inputDTO.get(PROCESS_OPTION).equals(AUTHORIZE) && inputDTO.get(PROCESS_STAGE).equals(AFTER)) {
			DBUtil dbutil = dbContext.createUtilInstance();
			splitSourceKey(inputDTO);
			String partitionNo = strsrcKey[0];
			String entityCode = "";
			String roleCode = "";
			String userID = strsrcKey[1];
			Date cbd = (java.sql.Date) inputDTO.getObject("CBD");
			String password = "";
			String salt = "";
			String encryptedPassword = "";
			try {
				if (inputDTO.get("ACTION_TYPE").equals(ADD)) {
					/** Get User Details **/
					DTObject result = getUserDetails(dbContext, partitionNo, userID);
					entityCode = result.get("ENTITY_CODE");
					roleCode = result.get("ROLE_CODE");

					/** User Status Updation **/
					dbutil.reset();
					dbutil.setMode(DBUtil.CALLABLE);
					dbutil.setSql("{CALL SP_USER_STATUS_UPDATE(?,?,?,?,?,?)}");
					dbutil.setString(1, entityCode);
					dbutil.setString(2, userID);
					dbutil.setString(3, inputDTO.get("USER_ID"));
					dbutil.setString(4, "UPDATION @ USER CREATION");
					dbutil.setString(5, RegularConstants.COLUMN_ENABLE);
					dbutil.registerOutParameter(6, Types.VARCHAR);
					dbutil.execute();
					if (!dbutil.getString(6).equals(RegularConstants.SP_SUCCESS)) {
						throw new Exception(dbutil.getString(6));
					}
					dbutil.reset();
					dbutil.setMode(DBUtil.PREPARED);
					dbutil.setSql("SELECT PWD_DELIVERY_MECHANISM,PWD_MANUAL_TYPE FROM SYSCPM WHERE ENTITY_CODE = ? AND EFFT_DATE = (SELECT MAX(EFFT_DATE) FROM SYSCPM WHERE ENTITY_CODE = ? AND EFFT_DATE <= FN_GETCD(?))");
					dbutil.setString(1, entityCode);
					dbutil.setString(2, entityCode);
					dbutil.setString(3, entityCode);
					ResultSet resultSet = dbutil.executeQuery();
					String pwdDeliveryChoice = RegularConstants.EMPTY_STRING;
					String pwdManualType = RegularConstants.EMPTY_STRING;
					if (resultSet.next()) {
						pwdDeliveryChoice = resultSet.getString(1);
						pwdManualType = resultSet.getString(2);
					}
					/** User Password Updation **/
					if (pwdDeliveryChoice.equals("4") && pwdManualType.equals("2")) {
						salt = result.get("FIRST_PIN");
						encryptedPassword = result.get("FIRST_PIN_SALT");
					} else {
						password = PasswordUtils.generateUserPassword(dbContext, entityCode, userID, cbd);
						salt = PasswordUtils.getSalt();
						encryptedPassword = PasswordUtils.getEncrypted(password, userID, salt);
					}
					dbutil.reset();
					dbutil.setMode(DBUtil.CALLABLE);
					dbutil.setSql("{CALL SP_USER_PWD_UPDATE(?,?,?,?,?,?,?,?,?,?,?,?,?)}");
					dbutil.setString(1, partitionNo);
					dbutil.setString(2, entityCode);
					dbutil.setString(3, userID);
					dbutil.setString(4, encryptedPassword);
					dbutil.setString(5, salt);
					dbutil.setString(6, inputDTO.get("USER_ID"));
					dbutil.setString(7, "GENERATION @ USER CREATION");
					if (pwdDeliveryChoice.equals("4")) {
						dbutil.setString(8, RegularConstants.COLUMN_ENABLE);
						dbutil.setString(9, RegularConstants.COLUMN_DISABLE);
					} else {
						dbutil.setString(8, RegularConstants.COLUMN_ENABLE);
						dbutil.setString(9, RegularConstants.COLUMN_ENABLE);

					}
					dbutil.setString(10, password);
					dbutil.setString(11, RegularConstants.COLUMN_DISABLE);
					dbutil.registerOutParameter(12, Types.TIMESTAMP);
					dbutil.registerOutParameter(13, Types.VARCHAR);
					dbutil.execute();
					if (!dbutil.getString(13).equals(RegularConstants.SP_SUCCESS)) {
						throw new Exception(dbutil.getString(13));
					}

					/** User Role Alloc **/
					dbutil.reset();
					dbutil.setMode(DBUtil.PREPARED);
					dbutil.setSql("INSERT INTO USERSROLEALLOC (ENTITY_CODE,USER_ID,EFFT_DATE,ROLE_CODE,REMARKS,TBA_KEY) VALUES(?,?,?,?,?,?)");
					dbutil.setString(1, entityCode);
					dbutil.setString(2, userID);
					dbutil.setTimestamp(3, getSystemDate(dbContext, entityCode));
					dbutil.setString(4, roleCode);
					dbutil.setString(5, "SYSTEM GENERATED");
					dbutil.setString(6, null);
					dbutil.executeUpdate();

					dbutil.reset();
					dbutil.setMode(DBUtil.PREPARED);
					dbutil.setSql("INSERT INTO USERSROLEALLOCHIST (ENTITY_CODE,USER_ID,EFFT_DATE,ROLE_CODE,REMARKS,TBA_KEY) VALUES(?,?,?,?,?,?)");
					dbutil.setString(1, entityCode);
					dbutil.setString(2, userID);
					dbutil.setTimestamp(3, getSystemDate(dbContext, entityCode));
					dbutil.setString(4, roleCode);
					dbutil.setString(5, "SYSTEM GENERATED");
					dbutil.setString(6, null);
					dbutil.executeUpdate();

					/** User Activation Updation **/
					dbutil.reset();
					dbutil.setMode(DBUtil.PREPARED);
					dbutil.setSql("SELECT ADMIN_ACTIVATION_REQD FROM SYSCPM WHERE ENTITY_CODE = ? AND EFFT_DATE = (SELECT MAX(EFFT_DATE) FROM SYSCPM WHERE ENTITY_CODE = ? AND EFFT_DATE <= FN_GETCD(?))");
					dbutil.setString(1, entityCode);
					dbutil.setString(2, entityCode);
					dbutil.setString(3, entityCode);
					ResultSet rset = dbutil.executeQuery();
					String userActivation = RegularConstants.COLUMN_DISABLE;
					if (rset.next()) {
						userActivation = rset.getString(1);
					}
					dbutil.reset();
					dbutil.setMode(DBUtil.CALLABLE);
					dbutil.setSql("{CALL SP_USER_ACTIVATION(?,?,?,?,?,?)}");
					dbutil.setString(1, entityCode);
					dbutil.setString(2, userID);
					dbutil.setString(3, inputDTO.get("USER_ID"));
					if (userActivation.equals(RegularConstants.COLUMN_ENABLE)) {
						dbutil.setString(4, RegularConstants.COLUMN_ENABLE);
					} else {
						dbutil.setString(4, RegularConstants.COLUMN_DISABLE);
					}
					dbutil.setString(5, RegularConstants.COLUMN_ENABLE);
					dbutil.registerOutParameter(6, Types.VARCHAR);
					dbutil.execute();
					if (!dbutil.getString(6).equals(RegularConstants.SP_SUCCESS)) {
						throw new Exception(dbutil.getString(6));
					}
					if (pwdDeliveryChoice.equals("4") && pwdManualType.equals("1")) {
						// resultDTO.set(RegularConstants.ADDITIONAL_INFO,
						// "  with System Generated New Pin:" + password);

						MessageParam mesageParam = new MessageParam();
						mesageParam.setCode(BackOfficeErrorCodes.HMS_USER_GEN_PIN);
						List<String> genPin = new ArrayList<String>();
						genPin.add(String.valueOf(password));
						mesageParam.setParameters(genPin);
						resultDTO.setObject(RegularConstants.ADDITIONAL_INFO, mesageParam);
					}

					//@author Pavan  Added ON 17-03-2017 Start    
					else if (pwdDeliveryChoice.equals("1")) {
						DTObject formDTO = new DTObject();
						dbutil.reset();
						dbutil.setMode(DBUtil.PREPARED);
						dbutil.setSql("SELECT EMAIL_ID,USER_NAME,FN_NEXTVAL('SEQ_EMAIL') AS INVENTORY_NO FROM USERS WHERE PARTITION_NO = ? AND USER_ID = ?");
						dbutil.setInt(1, Integer.parseInt(partitionNo));
						dbutil.setString(2, userID);
						ResultSet reset = dbutil.executeQuery();
						if (reset.next()) {
							List<String> invList = new ArrayList<String>();
							List<String> toEmailIdList = new ArrayList<String>();
							invList.add(reset.getString("INVENTORY_NO"));
							toEmailIdList.add(reset.getString("EMAIL_ID"));
							formDTO.setObject("TO_EMAIL_ID_LIST", toEmailIdList);
							formDTO.set("USER_NAME", reset.getString("USER_NAME"));
							formDTO.set("INVENTORY_NO", reset.getString("INVENTORY_NO"));
							formDTO.set("ENTITY_CODE", entityCode);
							formDTO.set("USER_ID", userID);
							formDTO.set("PASSWORD", password);
							formDTO.set("INTERFACE_CODE", "DISEMAIL");
							String templateCode = "PASSWORD_GENERATE";
							formDTO.set("TEMPLATE_CODE", templateCode);
							if (pushDataToLogTable(dbContext, formDTO)) {
								MessageParam mesageParam = new MessageParam();
								mesageParam.setCode(BackOfficeErrorCodes.USER_GEN_PIN_EMAIL);
								mesageParam.setParameters(toEmailIdList);
								resultDTO.setObject(RegularConstants.ADDITIONAL_INFO, mesageParam);
								formDTO.setObject("INVENTORY_NO", invList);
								formDTO.set("ENTITY_CODE", entityCode);
							}
						} else {
							throw new TBAFrameworkException("Issue in generating Email");
						}
					}
				//  Added ON 17-03-2017 End
				}
			} catch (Exception e) {
				e.printStackTrace();
				throw new TBAFrameworkException(e.getLocalizedMessage());
			} finally {
				dbutil.reset();
			}
		}
		return resultDTO;
	}

	@Override
	public DTObject processEffDateTBAUpdate(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		return null;
		// TODO Auto-generated method stub

	}

	//@author Pavan  Added ON 17-03-2017 Start    
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private DTObject invokeCommunicationHandler(DBContext dbcontext, DTObject inputObject) throws TBAFrameworkException {
		DTObject resultDTO = null;
		Class qmClass = null;
		Object qmObject = null;
		Method getDataMethod = null;
		Class[] parameterTypes = new Class[] { DBContext.class, DTObject.class };
		Object[] params = new Object[] { dbcontext, inputObject };
		try {
			qmClass = Class.forName(inputObject.get("CLASS_NAME"));
			qmObject = qmClass.newInstance();
			getDataMethod = qmClass.getMethod(inputObject.get("METHOD_NAME"), parameterTypes);
			resultDTO = (DTObject) getDataMethod.invoke(qmObject, params);
		} catch (Exception e) {
			throw new TBAFrameworkException(e.getLocalizedMessage());
		}
		return resultDTO;
	}

	public boolean pushDataToLogTable(DBContext dbcontext, DTObject formDTO) throws TBAFrameworkException {
		try {
			boolean statusType = false;
			String className = "com.patterns.jobs.message.handler.CommunicationHandler";
			String methodName = "dispatchEMailWithInventoryNumber";
			formDTO.set("CLASS_NAME", className);
			formDTO.set("METHOD_NAME", methodName);
			String status = invokeCommunicationHandler(dbcontext, formDTO).get("RESULT");
			if (status.equals(TBAProcessStatus.FAILURE.toString())) {
				throw new TBAFrameworkException("Issue in generating Email");
			} else
				statusType = true;
			return statusType;
		} catch (Exception e) {
			e.printStackTrace();
			throw new TBAFrameworkException(e.getLocalizedMessage());

		}

	}
	//  Added ON 17-03-2017 End    
	
	
	

}
