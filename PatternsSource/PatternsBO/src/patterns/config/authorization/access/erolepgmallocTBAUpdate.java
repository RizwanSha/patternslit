package patterns.config.authorization.access;

import java.sql.Date;
import patterns.config.authorization.CommonTBAUpdate;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;

public class erolepgmallocTBAUpdate extends CommonTBAUpdate {
	@Override
	public DTObject processTBAUpdate(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		// TODO Auto-generated method stub

		return null;
	}

	@Override
	public DTObject processEffDateTBAUpdate(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		DTObject resultDTO = new DTObject();
		DBUtil util = dbContext.createUtilInstance();
		if (inputDTO.get(PROCESS_OPTION).equals(AUTHORIZE)) {
			splitSourceKey(inputDTO);
			String entitycode = strsrcKey[0];
			String rolecode = strsrcKey[1];
			String modulecode = strsrcKey[2];
			String efftDateString = strsrcKey[3];
			Date effetiveDate = new java.sql.Date(BackOfficeFormatUtils.getDate(efftDateString, BackOfficeConstants.TBA_DATE_FORMAT).getTime());
			try {
				util.reset();
				//util.setSql("DELETE  FROM ROLEPGMALLOC WHERE ENTITY_CODE=? AND ROLE_CODE=? AND MODULE_ID=? AND EFFT_DATE=?");
				util.setSql("DELETE  FROM ROLEPGMALLOC WHERE ENTITY_CODE=? AND ROLE_CODE=? AND MODULE_ID=?");
				util.setString(1, entitycode);
				util.setString(2, rolecode);
				util.setString(3, modulecode);
				//util.setDate(4, effetiveDate);
				util.executeUpdate();

				util.reset();
				util.setMode(DBUtil.PREPARED);
				util.setSql("INSERT INTO ROLEPGMALLOC(ENTITY_CODE,ROLE_CODE,MODULE_ID,EFFT_DATE,REMARKS) SELECT ENTITY_CODE,ROLE_CODE,MODULE_ID,EFFT_DATE,REMARKS FROM ROLEPGMALLOCHIST WHERE ENTITY_CODE=? AND ROLE_CODE=? AND MODULE_ID=? AND EFFT_DATE=? ");
				util.setString(1, entitycode);
				util.setString(2, rolecode);
				util.setString(3, modulecode);
				util.setDate(4, effetiveDate);
				util.executeUpdate();
				
				util.reset();
				//util.setSql("DELETE  FROM ROLEPGMALLOCDTL WHERE ENTITY_CODE=? AND ROLE_CODE=? AND MODULE_ID=? AND EFFT_DATE=?");
				util.setSql("DELETE  FROM ROLEPGMALLOCDTL WHERE ENTITY_CODE=? AND ROLE_CODE=? AND MODULE_ID=?");
				util.setString(1, entitycode);
				util.setString(2, rolecode);
				util.setString(3, modulecode);
				//util.setDate(4, effetiveDate);
				util.executeUpdate();
				
				util.reset();
				util.setMode(DBUtil.PREPARED);
				util.setSql("INSERT INTO ROLEPGMALLOCDTL(ENTITY_CODE,ROLE_CODE,MODULE_ID,EFFT_DATE,SL,PGM_ID,ADD_ALLOWED,MODIFY_ALLOWED,VIEW_ALLOWED,AUTHORIZE_ALLOWED,REJECTION_ALLOWED,PROCESS_ALLOWED) SELECT ENTITY_CODE,ROLE_CODE,MODULE_ID,EFFT_DATE,SL,PGM_ID,ADD_ALLOWED,MODIFY_ALLOWED,VIEW_ALLOWED,AUTHORIZE_ALLOWED,REJECTION_ALLOWED,PROCESS_ALLOWED FROM ROLEPGMALLOCHISTDTL WHERE ENTITY_CODE=? AND ROLE_CODE=? AND MODULE_ID=? AND EFFT_DATE=?");
				
				util.setString(1, entitycode);
				util.setString(2, rolecode);
				util.setString(3, modulecode);
				util.setDate(4, effetiveDate);
				util.executeUpdate();

			} catch (Exception e) {
				e.printStackTrace();
				throw new TBAFrameworkException(e.getLocalizedMessage());
			} finally {
				util.reset();
			}
		}
		return resultDTO;
	}
}