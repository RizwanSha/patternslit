package patterns.config.authorization;

import java.sql.ResultSet;
import java.util.Map;

import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.BindParameterType;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.objects.TableInfo;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBInfo;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.service.DTObject;

public class ComponentTBAUpdate {

	private DBContext dbContext;

	public void moveToInventory(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		this.dbContext = dbContext;
		DBUtil dbUtil = dbContext.createUtilInstance();
		try {
			String entityCode = inputDTO.get("ENTITY_CODE");
			String mainProgramID = inputDTO.get("PARENT_PGM_ID");
			String mainProgramPK = inputDTO.get("PARENT_PGM_PK");
			String inventoryProgramID = inputDTO.get("LINKED_PGM_ID");
			String inventoryList = inputDTO.get("INVENTORY_LIST");

			if (inputDTO.containsKey("MOVE_TO_HIST")) {
				moveToInventoryHist(inputDTO, dbContext);
			}

			String sql = "SELECT TABLE_NAME,OPERATION_TYPE,DATA_BLOCK,LINKED_PGM_PK FROM PGMLINKEDTBA WHERE ENTITY_CODE=? AND PARENT_PGM_ID=? AND PARENT_PGM_PK=? AND LINKED_PGM_ID=? AND LINKED_PGM_PK IN (" + inventoryList + ")";
			dbUtil.setMode(DBUtil.PREPARED);
			dbUtil.setSql(sql);
			dbUtil.setString(1, entityCode);
			dbUtil.setString(2, mainProgramID);
			dbUtil.setString(3, mainProgramPK);
			dbUtil.setString(4, inventoryProgramID);
			ResultSet rset = dbUtil.executeQuery();
			while (rset.next()) {
				update(rset.getString(1), rset.getString(2), rset.getString(3), rset.getString(4));
			}

			deleteTBARecords(inputDTO, dbContext);
		} catch (Exception e) {
			e.printStackTrace();
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			dbUtil.reset();
		}
	}

	public void moveToInventoryHist(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		this.dbContext = dbContext;
		DBUtil dbUtil = dbContext.createUtilInstance();
		try {
			String entityCode = inputDTO.get("ENTITY_CODE");
			String mainProgramID = inputDTO.get("PARENT_PGM_ID");
			String mainProgramPK = inputDTO.get("PARENT_PGM_PK");
			String inventoryProgramID = inputDTO.get("LINKED_PGM_ID");
			String tableName = RegularConstants.EMPTY_STRING;
			tableName = inputDTO.get("TABLE");

			String sql = "SELECT TABLE_NAME,OPERATION_TYPE,DATA_BLOCK,LINKED_PGM_PK FROM PGMLINKEDTBA WHERE ENTITY_CODE=? AND PARENT_PGM_ID=? AND PARENT_PGM_PK=? AND LINKED_PGM_ID=? AND TABLE_NAME=?";
			dbUtil.setMode(DBUtil.PREPARED);
			dbUtil.setSql(sql);
			dbUtil.setString(1, entityCode);
			dbUtil.setString(2, mainProgramID);
			dbUtil.setString(3, mainProgramPK);
			dbUtil.setString(4, inventoryProgramID);
			dbUtil.setString(5, tableName);
			ResultSet rset = dbUtil.executeQuery();
			while (rset.next()) {
				update(rset.getString(1), rset.getString(2), rset.getString(3), rset.getString(4));
			}

		} catch (Exception e) {
			e.printStackTrace();
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			dbUtil.reset();
		}
	}

	public void deleteTBARecords(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		DBUtil dbUtil = dbContext.createUtilInstance();
		try {
			String entityCode = inputDTO.get("ENTITY_CODE");
			String mainProgramID = inputDTO.get("PARENT_PGM_ID");
			String mainProgramPK = inputDTO.get("PARENT_PGM_PK");
			String inventoryProgramID = inputDTO.get("LINKED_PGM_ID");
			String inventoryList = inputDTO.get("INVENTORY_LIST");

			String sql = "DELETE FROM PGMLINKEDTBA WHERE ENTITY_CODE=? AND PARENT_PGM_ID=? AND PARENT_PGM_PK=? AND LINKED_PGM_ID=? AND LINKED_PGM_PK IN (" + inventoryList + ")";
			dbUtil.setMode(DBUtil.PREPARED);
			dbUtil.setSql(sql);
			dbUtil.setString(1, entityCode);
			dbUtil.setString(2, mainProgramID);
			dbUtil.setString(3, mainProgramPK);
			dbUtil.setString(4, inventoryProgramID);
			dbUtil.executeUpdate();

			if (inputDTO.containsKey("MOVE_TO_HIST")) {
				dbUtil.reset();
				sql = "DELETE FROM PGMLINKEDTBA WHERE ENTITY_CODE=? AND PARENT_PGM_ID=? AND PARENT_PGM_PK=? AND LINKED_PGM_ID=? AND TABLE_NAME=?";
				dbUtil.setMode(DBUtil.PREPARED);
				dbUtil.setSql(sql);
				dbUtil.setString(1, entityCode);
				dbUtil.setString(2, mainProgramID);
				dbUtil.setString(3, mainProgramPK);
				dbUtil.setString(4, inventoryProgramID);
				dbUtil.setString(5, inputDTO.get("TABLE"));
				dbUtil.executeUpdate();
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			dbUtil.reset();
		}
	}

	private void update(String tableName, String operationType, String dataBlock, String primaryKey) throws TBAFrameworkException {
		DBUtil dbUtil = dbContext.createUtilInstance();
		try {
			DBInfo info = new DBInfo();
			TableInfo tableInfo = info.getTableInfo(tableName);
			String[] fieldNames = tableInfo.getColumnInfo().getColumnNames();
			Map<String, BindParameterType> columnType = tableInfo.getColumnInfo().getColumnMapping();
			String[] keyFields = tableInfo.getPrimaryKeyInfo().getColumnNames();
			BindParameterType[] keyFieldsColumnType = tableInfo.getPrimaryKeyInfo().getColumnTypes();
			StringBuffer query = new StringBuffer();
			StringBuffer mainQuery = new StringBuffer();
			String[] pkInfo = primaryKey.split("\\" + RegularConstants.PK_SEPARATOR);

			if (operationType.equals("A")) {
				query.append("INSERT INTO " + tableName + " SELECT ");
				for (String fieldName : fieldNames) {
					if (columnType.get(fieldName).equals(BindParameterType.DATE)) {
						query.append(" TO_DATE(EXTRACTVALUE('" + dataBlock + "','//" + fieldName + "'),'" + BackOfficeConstants.JAVA_TBA_DB_DATE_FORMAT + "')" + ",");

					} else if (columnType.get(fieldName).equals(BindParameterType.INTEGER) || columnType.get(fieldName).equals(BindParameterType.BIGINT)) {
						query.append(" IF(EXTRACTVALUE('" + dataBlock + "','//" + fieldName + "')='',NULL," + "EXTRACTVALUE('" + dataBlock + "','//" + fieldName + "')),");
					} else {
						query.append(" EXTRACTVALUE('" + dataBlock + "','//" + fieldName + "')" + ",");
					}
				}
				query.trimToSize();
				mainQuery.append(query.substring(0, query.length() - 1));
				mainQuery.append(" FROM DUAL");
				// auditKey = tableName + TableValueSep + NewDataChar +
				// sourceKey;
			} else if (operationType.equals("M")) {
				query.append("UPDATE  " + tableName + " SET  ");
				for (String fieldName : fieldNames) {
					if (columnType.get(fieldName).equals(BindParameterType.DATE)) {
						query.append(fieldName).append(" =  TO_DATE(EXTRACTVALUE('" + dataBlock + "','//" + fieldName + "'),'" + BackOfficeConstants.JAVA_TBA_DB_DATE_FORMAT + "')" + ",");
					} else if (columnType.get(fieldName).equals(BindParameterType.INTEGER) || columnType.get(fieldName).equals(BindParameterType.BIGINT)) {
						query.append(fieldName).append(" = IF(EXTRACTVALUE('" + dataBlock + "','//" + fieldName + "')='',NULL," + "EXTRACTVALUE('" + dataBlock + "','//" + fieldName + "')),");
					} else {
						query.append(fieldName).append(" =  EXTRACTVALUE('" + dataBlock + "','//" + fieldName + "')" + ",");
					}
				}
				query.trimToSize();
				mainQuery.append(query.substring(0, query.length() - 1));
				if (keyFields.length > 0)
					mainQuery.append(" WHERE ");
				int i = 0;
				for (String fieldName : keyFields) {
					if (keyFieldsColumnType[i] == BindParameterType.DATE) {
						if (i < keyFields.length - 1) {
							mainQuery.append(fieldName).append(" = TO_DATE('" + pkInfo[i] + "','" + BackOfficeConstants.JAVA_TBA_DB_DATE_FORMAT + "')  AND ");
						} else {
							mainQuery.append(fieldName).append(" = TO_DATE('" + pkInfo[i] + "','" + BackOfficeConstants.JAVA_TBA_DB_DATE_FORMAT + "')");
						}
					} else {
						if (i < keyFields.length - 1) {
							mainQuery.append(fieldName).append(" = '" + pkInfo[i] + "'  AND ");
						} else {
							mainQuery.append(fieldName).append(" = '" + pkInfo[i] + "'");
						}
					}
					++i;
				}
				// auditKey = tableName + TableValueSep + OldDataChar +
				// sourceKey;
			}

			dbUtil.reset();
			dbUtil.setMode(DBUtil.PREPARED);
			dbUtil.setSql(mainQuery.toString());
			System.out.println(mainQuery.toString());
			dbUtil.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
			throw new TBAFrameworkException(e.getLocalizedMessage());
		} finally {
			dbUtil.reset();
		}
	}
}