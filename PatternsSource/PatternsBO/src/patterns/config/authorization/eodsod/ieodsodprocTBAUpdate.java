package patterns.config.authorization.eodsod;

/*
Author : Pavan kumar.R
Created Date : 28-MAR-2015
Spec Reference: HMS-PSD-EODSOD-003
Modification History
----------------------------------------------------------------------
Sl.No		Modified Date	Author			Modified Changes	Version
----------------------------------------------------------------------

*/

import java.sql.Date;

import patterns.config.authorization.CommonTBAUpdate;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;

public class ieodsodprocTBAUpdate extends CommonTBAUpdate {
	@Override
	public DTObject processTBAUpdate(DTObject inputDTO, DBContext dbContext)
			throws TBAFrameworkException {
		// TODO Auto-generated method stub

		return null;
	}

	public DTObject processEffDateTBAUpdate(DTObject inputDTO,
			DBContext dbContext) throws TBAFrameworkException {
		DTObject resultDTO = new DTObject();
		DBUtil util = dbContext.createUtilInstance();
		int count = 0;
		int countDL = 0;
		if (inputDTO.get(PROCESS_OPTION).equals(AUTHORIZE)) {
			splitSourceKey(inputDTO);
			String entitycode = strsrcKey[0];
			String processType = strsrcKey[1];
			String efftDateString = strsrcKey[2];

			Date effectiveDate = new java.sql.Date(BackOfficeFormatUtils.getDate(efftDateString,BackOfficeConstants.TBA_DATE_FORMAT).getTime());
			try {
				util.reset();
				util.setMode(DBUtil.PREPARED);
				util.setSql("DELETE  FROM EODSODPROC WHERE ENTITY_CODE=? AND PROCESS_FLAG=? ");
				util.setString(1, entitycode);
				util.setString(2, processType);
				util.executeUpdate();

				util.reset();
				util.setMode(DBUtil.PREPARED);
				util.setSql("INSERT INTO EODSODPROC(ENTITY_CODE,PROCESS_FLAG) SELECT ENTITY_CODE,PROCESS_FLAG FROM EODSODPROCHIST WHERE ENTITY_CODE=? AND PROCESS_FLAG =? AND EFFT_DATE=?");
				util.setString(1, entitycode);
				util.setString(2, processType);
				util.setDate(3, effectiveDate);
				count = util.executeUpdate();
				if (count < 0) {
					throw new TBAFrameworkException();
				}
				util.setMode(DBUtil.PREPARED);
				util.setSql("DELETE  FROM EODSODPROCDTL WHERE ENTITY_CODE=? AND PROCESS_FLAG=? ");
				util.setString(1, entitycode);
				util.setString(2, processType);
				util.executeUpdate();

				util.reset();
				util.setMode(DBUtil.PREPARED);
				util.setSql("INSERT INTO EODSODPROCDTL(ENTITY_CODE,PROCESS_FLAG,SL,PROCESS_ID,PROCESS_STATE) SELECT ENTITY_CODE,PROCESS_FLAG,SL,PROCESS_ID,PROCESS_STATE FROM EODSODPROCHISTDTL WHERE ENTITY_CODE=? AND PROCESS_FLAG =?  AND EFFT_DATE=? ");
				util.setString(1, entitycode);
				util.setString(2, processType);
				util.setDate(3, effectiveDate);
				countDL = util.executeUpdate();
				if (countDL < 0) {
					throw new TBAFrameworkException();
				}

			} catch (Exception e) {
				e.printStackTrace();
				throw new TBAFrameworkException(e.getLocalizedMessage());
			} finally {
				util.reset();
			}
		}
		return resultDTO;
	}

}