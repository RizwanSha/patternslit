package patterns.config.authorization;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.sql.Types;

import patterns.config.framework.database.CM_LOVREC;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.PasswordUtils;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.web.BackOfficeFormatUtils;

public abstract class AccessTBAUpdate extends CommonTBAUpdate {

	public void muserTBA(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		if (inputDTO.get(PROCESS_OPTION).equals(AUTHORIZE) && inputDTO.get(PROCESS_STAGE).equals(AFTER)) {
			DBUtil dbutil = dbContext.createUtilInstance();
			DBUtil _dbutil = dbContext.createUtilInstance();
			splitSourceKey(inputDTO);
			String entityCode = strsrcKey[0];
			String userID = strsrcKey[1];
			Date cbd = (java.sql.Date) inputDTO.getObject("CBD");
			try {
				if (inputDTO.get("ACTION_TYPE").equals(ADD)) {

					/** User Status Updation **/
					dbutil.reset();
					dbutil.setMode(DBUtil.CALLABLE);
					dbutil.setSql("{CALL SP_USER_STATUS_UPDATE(?,?,?,?,?,?)}");
					dbutil.setString(1, entityCode);
					dbutil.setString(2, userID);
					dbutil.setString(3, inputDTO.get("USER_ID"));
					dbutil.setString(4, "UPDATION @ USER CREATION");
					dbutil.setString(5, RegularConstants.COLUMN_ENABLE);
					dbutil.registerOutParameter(6, Types.VARCHAR);
					dbutil.execute();
					if (!dbutil.getString(6).equals(RegularConstants.SP_SUCCESS)) {
						throw new Exception(dbutil.getString(6));
					}
					dbutil.reset();
					dbutil.setMode(DBUtil.PREPARED);
					dbutil.setSql("SELECT PWD_DELIVERY_MECHANISM FROM SYSCPM WHERE ENTITY_CODE = ? AND EFFT_DATE = (SELECT MAX(EFFT_DATE) FROM SYSCPM WHERE ENTITY_CODE = ? AND EFFT_DATE <= FN_GETCD(?))");
					dbutil.setString(1, entityCode);
					dbutil.setString(2, entityCode);
					dbutil.setString(3, entityCode);
					ResultSet resultSet = dbutil.executeQuery();
					String pwdDeliveryChoice = RegularConstants.EMPTY_STRING;
					if (resultSet.next()) {
						pwdDeliveryChoice = resultSet.getString(1);
					}
					/** User Password Updation **/
					if (!pwdDeliveryChoice.equals("4")) {
						String password = PasswordUtils.generateUserPassword(dbContext, entityCode, userID, cbd);
						String salt = PasswordUtils.getSalt();
						String encryptedPassword = PasswordUtils.getEncrypted(password, userID, salt);
						dbutil.reset();
						dbutil.setMode(DBUtil.CALLABLE);
						dbutil.setSql("{CALL SP_USER_PWD_UPDATE(?,?,?,?,?,?,?,?,?,?,?,?)}");
						dbutil.setString(1, entityCode);
						dbutil.setString(2, userID);
						dbutil.setString(3, encryptedPassword);
						dbutil.setString(4, salt);
						dbutil.setString(5, inputDTO.get("USER_ID"));
						dbutil.setString(6, "GENERATION @ USER CREATION");
						dbutil.setString(7, RegularConstants.COLUMN_ENABLE);
						dbutil.setString(8, RegularConstants.COLUMN_ENABLE);
						dbutil.setString(9, password);
						dbutil.setString(10, RegularConstants.COLUMN_DISABLE);
						dbutil.registerOutParameter(11, Types.TIMESTAMP);
						dbutil.registerOutParameter(12, Types.VARCHAR);
						dbutil.execute();
						if (!dbutil.getString(12).equals(RegularConstants.SP_SUCCESS)) {
							throw new Exception(dbutil.getString(12));
						}
					} else {

					}

					/** User Activation Updation **/
					dbutil.reset();
					dbutil.setMode(DBUtil.PREPARED);
					dbutil.setSql("SELECT ADMIN_ACTIVATION_REQD FROM SYSCPM WHERE ENTITY_CODE = ? AND EFFT_DATE = (SELECT MAX(EFFT_DATE) FROM SYSCPM WHERE ENTITY_CODE = ? AND EFFT_DATE <= FN_GETCD(?))");
					dbutil.setString(1, entityCode);
					dbutil.setString(2, entityCode);
					dbutil.setString(3, entityCode);
					ResultSet rset = dbutil.executeQuery();
					String userActivation = RegularConstants.COLUMN_DISABLE;
					if (rset.next()) {
						userActivation = rset.getString(1);
					}
					dbutil.reset();
					dbutil.setMode(DBUtil.CALLABLE);
					dbutil.setSql("{CALL SP_USER_ACTIVATION(?,?,?,?,?,?)}");
					dbutil.setString(1, entityCode);
					dbutil.setString(2, userID);
					dbutil.setString(3, inputDTO.get("USER_ID"));
					if (userActivation.equals(RegularConstants.COLUMN_ENABLE)) {
						dbutil.setString(4, RegularConstants.COLUMN_ENABLE);
					} else {
						dbutil.setString(4, RegularConstants.COLUMN_DISABLE);
					}
					dbutil.setString(5, RegularConstants.COLUMN_ENABLE);
					dbutil.registerOutParameter(6, Types.VARCHAR);
					dbutil.execute();
					if (!dbutil.getString(6).equals(RegularConstants.SP_SUCCESS)) {
						throw new Exception(dbutil.getString(6));
					}
				}
				String invNum = "";
				dbutil.reset();
				dbutil.setMode(DBUtil.PREPARED);
				dbutil.setSql("SELECT addr_inv_num  FROM USERS WHERE ENTITY_CODE=? AND USER_ID=? ");
				dbutil.setString(1, entityCode);
				dbutil.setString(2, userID);
				ResultSet rs = dbutil.executeQuery();
				if (rs.next()) {
					invNum = rs.getString(1);
				}
				if (inputDTO.get("ACTION_TYPE").equals(ADD)) {
					dbutil.reset();
					dbutil.setMode(DBUtil.PREPARED);
					dbutil.setSql("INSERT INTO ADDRINV (AI_ORG_ID,AI_ADDR_INV_NO,AI_ADDR1,AI_ADDR2,AI_ADDR3,AI_ADDR4,AI_ADDR5,AI_LOC,AI_PINCODE,AI_COUNTRY,AI_OFF_ADDR) SELECT AIH_ORG_ID,AIH_ADDR_INV_NO,AIH_ADDR1,AIH_ADDR2,AIH_ADDR3,AIH_ADDR4,AIH_ADDR5,AIH_LOC,AIH_PINCODE,AIH_COUNTRY,AIH_OFF_ADDR FROM ADDRINVHIST WHERE AIH_ORG_ID=? AND AIH_ADDR_INV_NO=? AND AIH_SL=(SELECT MAX(AIH_SL)FROM  ADDRINVHIST WHERE AIH_ORG_ID=? AND AIH_ADDR_INV_NO=?)");
					dbutil.setString(1, entityCode);
					dbutil.setString(2, invNum);
					dbutil.setString(3, entityCode);
					dbutil.setString(4, invNum);
					dbutil.executeUpdate();
				} else if (inputDTO.get("ACTION_TYPE").equals(MODIFY)) {
					dbutil.reset();
					dbutil.setMode(DBUtil.PREPARED);
					dbutil.setSql("SELECT AIH_ORG_ID,AIH_ADDR_INV_NO,AIH_ADDR1,AIH_ADDR2,AIH_ADDR3,AIH_ADDR4,AIH_ADDR5,AIH_LOC,AIH_PINCODE,AIH_COUNTRY FROM ADDRINVHIST WHERE AIH_ORG_ID=? AND AIH_ADDR_INV_NO=? AND AIH_SL=(SELECT MAX(AIH_SL)FROM  ADDRINVHIST WHERE AIH_ORG_ID=? AND AIH_ADDR_INV_NO=? )");
					dbutil.setString(1, entityCode);
					dbutil.setString(2, invNum);
					dbutil.setString(3, entityCode);
					dbutil.setString(4, invNum);
					rs = dbutil.executeQuery();
					if (rs.next()) {
						_dbutil.reset();
						_dbutil.setMode(DBUtil.PREPARED);
						_dbutil.setSql(" UPDATE ADDRINV SET AI_ADDR1  =?, AI_ADDR2  =?,AI_ADDR3  =?,AI_ADDR4  =?,AI_ADDR5  =?,AI_LOC    =?,AI_PINCODE=?,AI_COUNTRY=?  WHERE AI_ORG_ID  = ? AND AI_ADDR_INV_NO=?  ");
						_dbutil.setString(1, rs.getString(3));
						_dbutil.setString(2, rs.getString(4));
						_dbutil.setString(3, rs.getString(5));
						_dbutil.setString(4, rs.getString(6));
						_dbutil.setString(5, rs.getString(7));
						_dbutil.setString(6, rs.getString(8));
						_dbutil.setString(7, rs.getString(9));
						_dbutil.setString(8, rs.getString(10));
						_dbutil.setString(9, entityCode);
						_dbutil.setString(10, invNum);
						_dbutil.executeUpdate();
					}
					_dbutil.reset();
				}
			} catch (Exception e) {
				e.printStackTrace();
				throw new TBAFrameworkException(e.getLocalizedMessage());
			} finally {
				dbutil.reset();
			}
		}
	}

	public void euserstatusTBA(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		if (inputDTO.get(PROCESS_OPTION).equals(AUTHORIZE) && inputDTO.get(PROCESS_STAGE).equals(AFTER)) {
			if (inputDTO.get("ACTION_TYPE").equals(MODIFY)) {
				DBUtil dbutil = dbContext.createUtilInstance();
				splitSourceKey(inputDTO);
				String entityCode = strsrcKey[0];
				String userID = strsrcKey[1];
				try {
					dbutil.reset();
					dbutil.setMode(DBUtil.CALLABLE);
					dbutil.setSql("{CALL SP_USER_STATUS_UPDATE(?,?,?,?,?,?)}");
					dbutil.setString(1, entityCode);
					dbutil.setString(2, userID);
					dbutil.setString(3, inputDTO.get("USER_ID"));
					dbutil.setString(4, "UPDATION @ USER STATUS CHANGE");
					dbutil.setString(5, RegularConstants.COLUMN_DISABLE);
					dbutil.registerOutParameter(6, Types.VARCHAR);
					dbutil.execute();
					if (!dbutil.getString(6).equals(RegularConstants.SP_SUCCESS)) {
						throw new Exception(dbutil.getString(6));
					}
					dbutil.reset();
					dbutil.setMode(DBUtil.PREPARED);
					dbutil.setSql("SELECT 1 FROM USERS WHERE ENTITY_CODE = ? AND USER_ID = ? AND STATUS = 'A'");
					dbutil.setString(1, entityCode);
					dbutil.setString(2, userID);
					ResultSet rset = dbutil.executeQuery();
					if (rset.next()) {
						dbutil.reset();
						dbutil.setMode(DBUtil.PREPARED);
						dbutil.setSql("DELETE FROM USERSPWDINVALID WHERE ENTITY_CODE = ? AND USER_ID = ?");
						dbutil.setString(1, entityCode);
						dbutil.setString(2, userID);
						dbutil.executeUpdate();
					}
				} catch (Exception e) {
					throw new TBAFrameworkException(e.getLocalizedMessage());
				} finally {
					dbutil.reset();
				}
			}
		}
	}

	public void epwdresetTBA(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		if (inputDTO.get(PROCESS_OPTION).equals(AUTHORIZE) && inputDTO.get(PROCESS_STAGE).equals(AFTER)) {
			if (inputDTO.get("ACTION_TYPE").equals(MODIFY)) {
				DBUtil dbutil = dbContext.createUtilInstance();
				splitSourceKey(inputDTO);
				String entityCode = strsrcKey[0];
				String userID = strsrcKey[1];
				Date cbd = (java.sql.Date) inputDTO.getObject("CBD");
				try {
					dbutil.reset();
					String password = PasswordUtils.generateUserPassword(dbContext, entityCode, userID, cbd);
					String salt = PasswordUtils.getSalt();
					String encryptedPassword = PasswordUtils.getEncrypted(password, userID, salt);
					dbutil.reset();
					dbutil.setMode(DBUtil.CALLABLE);
					dbutil.setSql("{CALL SP_USER_PWD_UPDATE(?,?,?,?,?,?,?,?,?,?,?,?,?)}");
					dbutil.setString(1, entityCode);
					dbutil.setString(2, userID);
					dbutil.setString(3, encryptedPassword);
					dbutil.setString(4, salt);
					dbutil.setString(5, inputDTO.get("USER_ID"));
					dbutil.setString(6, "GENERATION @ PASSWORD RESET");
					dbutil.setString(7, RegularConstants.COLUMN_ENABLE);
					dbutil.setString(8, RegularConstants.COLUMN_ENABLE);
					dbutil.setString(9, password);
					dbutil.setString(10, RegularConstants.COLUMN_DISABLE);
					dbutil.setString(11, RegularConstants.COLUMN_ENABLE);
					dbutil.registerOutParameter(12, Types.TIMESTAMP);
					dbutil.registerOutParameter(13, Types.VARCHAR);
					dbutil.execute();
					if (!dbutil.getString(13).equals(RegularConstants.SP_SUCCESS)) {
						throw new Exception(dbutil.getString(13));
					}

				} catch (Exception e) {
					throw new TBAFrameworkException(e.getLocalizedMessage());
				} finally {
					dbutil.reset();
				}
			}
		}
	}

	public void eactroleallocTBA(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		if (inputDTO.get(PROCESS_OPTION).equals(AUTHORIZE) && inputDTO.get(PROCESS_STAGE).equals(AFTER)) {
			DBUtil dbutil = dbContext.createUtilInstance();
			splitSourceKey(inputDTO);
			String entityCode = strsrcKey[0];
			String userID = strsrcKey[1];
			String roleCode = strsrcKey[2];
			try {
				dbutil.reset();
				dbutil.setMode(DBUtil.PREPARED);
				dbutil.setSql("INSERT INTO USERSACTROLEALLOCHIST SELECT * FROM USERSACTROLEALLOC WHERE ENTITY_CODE= ? AND USER_ID= ? AND ROLE_CODE= ?");
				dbutil.setString(1, entityCode);
				dbutil.setString(2, userID);
				dbutil.setString(3, roleCode);
				dbutil.executeUpdate();
			} catch (Exception e) {
				throw new TBAFrameworkException(e.getLocalizedMessage());
			} finally {
				dbutil.reset();
			}
		}
	}

	public void ealertstatusTBA(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		if (inputDTO.get(PROCESS_OPTION).equals(AUTHORIZE) && inputDTO.get(PROCESS_STAGE).equals(AFTER)) {
			if (inputDTO.get("ACTION_TYPE").equals(MODIFY)) {
				DBUtil dbutil = dbContext.createUtilInstance();
				splitSourceKey(inputDTO);
				String entityCode = strsrcKey[0];
				String code = strsrcKey[1];
				try {
					dbutil.reset();
					dbutil.setSql("INSERT INTO ALERTPROCSTATUSHIST  SELECT * FROM ALERTPROCSTATUS WHERE ENTITY_CODE = ? AND CODE= ?");
					dbutil.setString(1, entityCode);
					dbutil.setString(2, code);
					dbutil.executeUpdate();
					dbutil.reset();
					dbutil.setSql("SELECT CRON_EXPRESSION,RECORD_PER_REQUEST, STATUS,AU_ON FROM ALERTPROCSTATUS WHERE ENTITY_CODE = ? AND CODE= ?");
					dbutil.setString(1, entityCode);
					dbutil.setString(2, code);
					ResultSet rset = dbutil.executeQuery();
					if (rset.next()) {
						String cronExpression = rset.getString(1);
						String recordsPerRun = rset.getString(2);
						String status = rset.getString(3);
						Timestamp authorizedDate = rset.getTimestamp(4);
						dbutil.reset();
						dbutil.setSql("SELECT JOB_CATEGORY FROM JOBMAST WHERE ENTITY_CODE = ? AND JOB_CODE= ?");
						dbutil.setString(1, entityCode);
						dbutil.setString(2, code);
						rset = dbutil.executeQuery();
						if (rset.next()) {
							String jobCategory = rset.getString(1);
							dbutil.reset();
							dbutil.setSql("UPDATE JOBMAST SET CRON_EXP = ?, STATUS = ?, AUTH_ON =? WHERE ENTITY_CODE = ? AND JOB_CODE= ?");
							dbutil.setString(1, cronExpression);
							dbutil.setString(2, status);
							dbutil.setTimestamp(3, authorizedDate);
							dbutil.setString(4, entityCode);
							dbutil.setString(5, code);
							dbutil.executeUpdate();
							dbutil.reset();
							if (jobCategory.equals(CM_LOVREC.COMMON_JOBCATEGORY_1)) {
								dbutil.setSql("UPDATE JOBRECPROCCFG SET MAX_RECORDS = ? WHERE ENTITY_CODE = ? AND JOB_CODE= ?");
							} else if (jobCategory.equals(CM_LOVREC.COMMON_JOBCATEGORY_2)) {
								dbutil.setSql("UPDATE JOBEVENTPROCCFG SET MAX_RECORDS = ? WHERE ENTITY_CODE = ? AND JOB_CODE= ?");
							} else if (jobCategory.equals(CM_LOVREC.COMMON_JOBCATEGORY_3)) {
								dbutil.setSql("UPDATE JOBRECDISPATCHCFG SET MAX_RECORDS = ? WHERE ENTITY_CODE = ? AND JOB_CODE= ?");
							}
							dbutil.setString(1, recordsPerRun);
							dbutil.setString(2, entityCode);
							dbutil.setString(3, code);
							dbutil.executeUpdate();
						}
					}

				} catch (Exception e) {
					throw new TBAFrameworkException(e.getLocalizedMessage());
				} finally {
					dbutil.reset();
				}
			}
		}
	}

	public void eipuserTBA(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		if (inputDTO.get(PROCESS_OPTION).equals(AUTHORIZE) && inputDTO.get(PROCESS_STAGE).equals(BEFORE)) {
			DBUtil dbutil = dbContext.createUtilInstance();
			splitSourceKey(inputDTO);
			String entityCode = strsrcKey[0];
			String code = strsrcKey[1];
			String effectiveDate = strsrcKey[2];
			Date effectiveDateValue = new java.sql.Date(BackOfficeFormatUtils.getDate(effectiveDate, BackOfficeConstants.TBA_DATE_FORMAT).getTime());
			try {
				dbutil.reset();
				dbutil.setSql("DELETE FROM USERIPRESTRICTDTL WHERE ENTITY_CODE = ? AND USER_ID= ? AND EFFT_DATE = ?");
				dbutil.setString(1, entityCode);
				dbutil.setString(2, code);
				dbutil.setDate(3, effectiveDateValue);
				dbutil.executeQuery();
			} catch (Exception e) {
				throw new TBAFrameworkException(e.getLocalizedMessage());
			} finally {
				dbutil.reset();
			}
		}
	}

	public void eipbranchTBA(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		if (inputDTO.get(PROCESS_OPTION).equals(AUTHORIZE) && inputDTO.get(PROCESS_STAGE).equals(BEFORE)) {
			DBUtil dbutil = dbContext.createUtilInstance();
			splitSourceKey(inputDTO);
			String entityCode = strsrcKey[0];
			String code = strsrcKey[1];
			String effectiveDate = strsrcKey[2];
			Date effectiveDateValue = new java.sql.Date(BackOfficeFormatUtils.getDate(effectiveDate, BackOfficeConstants.TBA_DATE_FORMAT).getTime());
			try {
				dbutil.reset();
				dbutil.setSql("DELETE FROM BRNIPRESTRICTDTL WHERE ENTITY_CODE = ? AND BRANCH_CODE= ? AND EFFT_DATE = ?");
				dbutil.setString(1, entityCode);
				dbutil.setString(2, code);
				dbutil.setDate(3, effectiveDateValue);
				dbutil.executeQuery();
			} catch (Exception e) {
				throw new TBAFrameworkException(e.getLocalizedMessage());
			} finally {
				dbutil.reset();
			}
		}
	}

	public void ipwdneglistTBA(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		if (inputDTO.get(PROCESS_OPTION).equals(AUTHORIZE) && inputDTO.get(PROCESS_STAGE).equals(BEFORE)) {
			DBUtil dbutil = dbContext.createUtilInstance();
			splitSourceKey(inputDTO);
			String entityCode = strsrcKey[0];
			String effectiveDate = strsrcKey[1];
			Date effectiveDateValue = new java.sql.Date(BackOfficeFormatUtils.getDate(effectiveDate, BackOfficeConstants.TBA_DATE_FORMAT).getTime());
			try {
				dbutil.reset();
				dbutil.setSql("DELETE FROM PWDNEGLISTDTL WHERE ENTITY_CODE = ? AND EFFT_DATE = ?");
				dbutil.setString(1, entityCode);
				dbutil.setDate(2, effectiveDateValue);
				dbutil.executeUpdate();
			} catch (Exception e) {
				throw new TBAFrameworkException(e.getLocalizedMessage());
			} finally {
				dbutil.reset();
			}
		}
	}

	public void euseractivationTBA(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		if (inputDTO.get(PROCESS_OPTION).equals(AUTHORIZE) && inputDTO.get(PROCESS_STAGE).equals(AFTER)) {
			if (inputDTO.get("ACTION_TYPE").equals(MODIFY)) {
				DBUtil dbutil = dbContext.createUtilInstance();
				splitSourceKey(inputDTO);
				String entityCode = strsrcKey[0];
				String userID = strsrcKey[1];
				try {
					dbutil.reset();
					dbutil.setMode(DBUtil.CALLABLE);
					dbutil.setSql("{CALL SP_USER_ACTIVATION(?,?,?,?,?,?)}");
					dbutil.setString(1, entityCode);
					dbutil.setString(2, userID);
					dbutil.setString(3, inputDTO.get("USER_ID"));
					dbutil.setString(4, RegularConstants.COLUMN_DISABLE);
					dbutil.setString(5, RegularConstants.COLUMN_DISABLE);
					dbutil.registerOutParameter(6, Types.VARCHAR);
					dbutil.execute();
					if (!dbutil.getString(6).equals(RegularConstants.SP_SUCCESS)) {
						throw new Exception(dbutil.getString(6));
					}
				} catch (Exception e) {
					throw new TBAFrameworkException(e.getLocalizedMessage());
				} finally {
					dbutil.reset();
				}
			}
		}
	}

	public void irestrictpfxTBA(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		if (inputDTO.get(PROCESS_OPTION).equals(AUTHORIZE) && inputDTO.get(PROCESS_STAGE).equals(BEFORE)) {
			DBUtil dbutil = dbContext.createUtilInstance();
			splitSourceKey(inputDTO);
			String entityCode = strsrcKey[0];
			String effectiveDate = strsrcKey[1];
			Date effectiveDateValue = new java.sql.Date(BackOfficeFormatUtils.getDate(effectiveDate, BackOfficeConstants.TBA_DATE_FORMAT).getTime());
			try {
				dbutil.reset();
				dbutil.setSql("DELETE FROM RESTRICTPFXDTL WHERE ENTITY_CODE = ? AND EFFT_DATE = ?");
				dbutil.setString(1, entityCode);
				dbutil.setDate(2, effectiveDateValue);
				dbutil.executeQuery();
			} catch (Exception e) {
				throw new TBAFrameworkException(e.getLocalizedMessage());
			} finally {
				dbutil.reset();
			}
		}
	}

	public final void eusertfaregTBA(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		if (inputDTO.get(PROCESS_OPTION).equals(REJECT) && inputDTO.get(PROCESS_STAGE).equals(BEFORE)) {
			if (inputDTO.get("ACTION_TYPE").equals(ADD)) {
				DBUtil dbutil = dbContext.createUtilInstance();
				splitSourceKey(inputDTO);
				String srcKey = source_key;
				String entityCode = strsrcKey[0];
				String tbaKey = tba_key;
				String[] tbaKey1 = tbaKey.split("\\|");
				String entryDate = tbaKey1[0];
				String dtlsl = tbaKey1[1];
				String pgmID = "EUSERTFAREG";
				Date tbaEntryDate = new java.sql.Date(BackOfficeFormatUtils.getDate(entryDate, BackOfficeConstants.TBA_DATE_FORMAT).getTime());
				try {
					dbutil.reset();
					String sql = "SELECT EXTRACTVALUE(T.TBADTL_DATA_BLOCK,'/ROW/INV_NUM'),EXTRACTVALUE(T.TBADTL_DATA_BLOCK,'/ROW/REVOKED') FROM TBAAUTHDTL T  WHERE T.TBADTL_ENTITY_NUM=? AND T.TBADTL_PGM_ID=? AND T.TBADTL_MAIN_PK=? AND T.TBADTL_ENTRY_DATE=? AND T.TBADTL_DTL_SL=? AND T.TBADTL_TABLE_NAME=? AND T.TBADTL_BLOCK_SL=?";
					dbutil.setSql(sql);
					dbutil.setString(1, entityCode);
					dbutil.setString(2, pgmID);
					dbutil.setString(3, srcKey);
					dbutil.setDate(4, tbaEntryDate);
					dbutil.setString(5, dtlsl);
					dbutil.setString(6, "USERSTFA");
					dbutil.setString(7, "1");
					ResultSet rset = dbutil.executeQuery();
					String inventoryNum = RegularConstants.EMPTY_STRING;
					String revoked = RegularConstants.EMPTY_STRING;
					if (rset.next()) {
						inventoryNum = rset.getString(1);
						revoked = rset.getString(2);
					}
					if (revoked.equals(RegularConstants.COLUMN_DISABLE)) {
						dbutil.reset();
						sql = "DELETE FROM PKICERTINVENTORY WHERE ENTITY_CODE=? AND CERT_INV_NUM=? ";
						dbutil.setSql(sql);
						dbutil.setString(1, entityCode);
						dbutil.setString(2, inventoryNum);
						dbutil.executeUpdate();
					}

				} catch (Exception e) {
					throw new TBAFrameworkException(e.getLocalizedMessage());
				} finally {
					dbutil.reset();
				}
			}
		}
	}

	public final void ipkicacertcfgTBA(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		if (inputDTO.get(PROCESS_OPTION).equals(REJECT) && inputDTO.get(PROCESS_STAGE).equals(BEFORE)) {
			if (inputDTO.get("ACTION_TYPE").equals(ADD)) {
				DBUtil dbutil = dbContext.createUtilInstance();
				splitSourceKey(inputDTO);
				String srcKey = source_key;
				String entityCode = strsrcKey[0];
				String tbaKey = tba_key;
				String[] tbaKey1 = tbaKey.split("\\|");
				String entryDate = tbaKey1[0];
				String dtlsl = tbaKey1[1];
				String pgmID = "IPKICACERTCFG";
				Date tbaEntryDate = new java.sql.Date(BackOfficeFormatUtils.getDate(entryDate, BackOfficeConstants.TBA_DATE_FORMAT).getTime());
				try {
					dbutil.reset();
					String sql = "SELECT EXTRACTVALUE(T.TBADTL_DATA_BLOCK,'/ROW/INV_NUM'),EXTRACTVALUE(T.TBADTL_DATA_BLOCK,'/ROW/REVOKED') FROM TBAAUTHDTL T  WHERE T.TBADTL_ENTITY_NUM=? AND T.TBADTL_PGM_ID=? AND T.TBADTL_MAIN_PK=? AND T.TBADTL_ENTRY_DATE=? AND T.TBADTL_DTL_SL=? AND T.TBADTL_TABLE_NAME=? AND T.TBADTL_BLOCK_SL=?";
					dbutil.setSql(sql);
					dbutil.setString(1, entityCode);
					dbutil.setString(2, pgmID);
					dbutil.setString(3, srcKey);
					dbutil.setDate(4, tbaEntryDate);
					dbutil.setString(5, dtlsl);
					dbutil.setString(6, "PKICACERTCFG");
					dbutil.setString(7, "1");
					ResultSet rset = dbutil.executeQuery();
					String inventoryNum = RegularConstants.EMPTY_STRING;
					String revoked = RegularConstants.EMPTY_STRING;
					if (rset.next()) {
						inventoryNum = rset.getString(1);
						revoked = rset.getString(2);
					}
					if (revoked.equals(RegularConstants.COLUMN_DISABLE)) {
						dbutil.reset();
						sql = "DELETE FROM PKICERTINVENTORY WHERE ENTITY_CODE=? AND CERT_INV_NUM=? ";
						dbutil.setSql(sql);
						dbutil.setString(1, entityCode);
						dbutil.setString(2, inventoryNum);
						dbutil.executeUpdate();
					}

				} catch (Exception e) {
					throw new TBAFrameworkException(e.getLocalizedMessage());
				} finally {
					dbutil.reset();
				}
			}
		}
	}

	public void epkicertcrlTBA(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		if (inputDTO.get(PROCESS_OPTION).equals(AUTHORIZE) && inputDTO.get(PROCESS_STAGE).equals(AFTER)) {
			DBUtil dbutil = dbContext.createUtilInstance();
			splitSourceKey(inputDTO);
			String entityCode = strsrcKey[0];
			String rootCaCode = strsrcKey[1];
			try {
				dbutil.reset();
				dbutil.setSql("INSERT INTO PKICERTAUTHCRLHIST  SELECT * FROM PKICERTAUTHCRL WHERE ENTITY_CODE = ? AND ROOT_CA_CODE= ?");
				dbutil.setString(1, entityCode);
				dbutil.setString(2, rootCaCode);
				dbutil.executeUpdate();
			} catch (Exception e) {
				throw new TBAFrameworkException(e.getLocalizedMessage());
			} finally {
				dbutil.reset();
			}
		}
	}

}