package patterns.config.authorization.emp;

import java.sql.Date;

import patterns.config.authorization.CommonTBAUpdate;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;

public class istaffsignatureTBAUpdate extends CommonTBAUpdate {
	@Override
	public DTObject processTBAUpdate(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		return null;
	}

	@Override
	public DTObject processEffDateTBAUpdate(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		splitSourceKey(inputDTO);
		String entitycode = strsrcKey[0];
		String staffCode = strsrcKey[1];
		String efftDateString = strsrcKey[2];
		Date effectiveDate = new java.sql.Date(BackOfficeFormatUtils.getDate(efftDateString, BackOfficeConstants.TBA_DATE_FORMAT).getTime());
		DTObject resultDTO = new DTObject();
		DBUtil util = dbContext.createUtilInstance();
		if (inputDTO.get(PROCESS_OPTION).equals(AUTHORIZE)) {
			try {
				util.reset();
				util.setSql("DELETE FROM STAFFSIGNATURE WHERE ENTITY_CODE=? AND STAFF_CODE=?  ");
				util.setString(1, entitycode);
				util.setString(2, staffCode);
				util.executeUpdate();

				util.reset();
				util.setMode(DBUtil.PREPARED);
				util.setSql("INSERT INTO STAFFSIGNATURE(ENTITY_CODE,STAFF_CODE,EFF_DATE,FILE_INV_NO,REFERENCE_NO)SELECT ENTITY_CODE,STAFF_CODE,EFF_DATE,FILE_INV_NO,REFERENCE_NO FROM STAFFSIGNATUREHIST WHERE ENTITY_CODE=? AND STAFF_CODE=? AND EFF_DATE=? ");
				util.setString(1, entitycode);
				util.setString(2, staffCode);
				util.setDate(3, effectiveDate);
				util.executeUpdate();
			} catch (Exception e) {
				e.printStackTrace();
				throw new TBAFrameworkException(e.getLocalizedMessage());
			} finally {
				util.reset();
			}
		}
		return resultDTO;

	}
}
