package patterns.config.authorization.emp;

import java.sql.Date;

import patterns.config.authorization.CommonTBAUpdate;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;

public class estaffrolebrnTBAUpdate extends CommonTBAUpdate {

	public DTObject processTBAUpdate(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		return null;
	}

	public DTObject processEffDateTBAUpdate(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		DTObject resultDTO = new DTObject();
		DBUtil util = dbContext.createUtilInstance();
		if (inputDTO.get(PROCESS_OPTION).equals(AUTHORIZE)) {
			splitSourceKey(inputDTO);
			long entityCode = Long.parseLong(strsrcKey[0]);
			String staffCode = strsrcKey[1];
			String effectDate = strsrcKey[2];
			Date effectiveDate = new java.sql.Date(BackOfficeFormatUtils.getDate(effectDate, BackOfficeConstants.TBA_DATE_FORMAT).getTime());
			try {
				util.reset();
				util.setMode(DBUtil.PREPARED);
				util.setSql("UPDATE STAFF S,STAFFROLEBRNHIST SS SET S.STAFF_ROLE_CODE=SS.STAFF_ROLE_CODE,S.BRANCH_CODE=SS.BRANCH_CODE WHERE S.ENTITY_CODE=?  AND S.STAFF_CODE=? AND SS.EFF_DATE=? AND SS.ENABLED='1'");
				util.setLong(1, entityCode);
				util.setString(2, staffCode);	
				util.setDate(3, effectiveDate);
				int count = util.executeUpdate();
				if (count == 0) {
					throw new TBAFrameworkException();
				}
			} catch (Exception e) {
				e.printStackTrace();
				throw new TBAFrameworkException(e.getLocalizedMessage());
			} finally {
				util.reset();
			}
		}
		return resultDTO;
	}
}
