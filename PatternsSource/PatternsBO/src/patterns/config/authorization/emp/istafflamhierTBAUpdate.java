package patterns.config.authorization.emp;

import java.sql.Date;
import java.sql.ResultSet;

import patterns.config.authorization.CommonTBAUpdate;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;

public class istafflamhierTBAUpdate extends CommonTBAUpdate {

	public DTObject processTBAUpdate(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		return null;
	}
	public DTObject processEffDateTBAUpdate(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		DTObject resultDTO = new DTObject();
		DBUtil util = dbContext.createUtilInstance();
		DBUtil util1 = dbContext.createUtilInstance();
		if (inputDTO.get(PROCESS_OPTION).equals(AUTHORIZE)) {
			splitSourceKey(inputDTO);
			long entityCode = Long.parseLong(strsrcKey[0]);
			String staffCode = strsrcKey[1];
			String effDate = strsrcKey[2];
			Date effectiveDate = (java.sql.Date) inputDTO.getObject("CBD");
			Date effectDate = new java.sql.Date(BackOfficeFormatUtils.getDate(effDate, BackOfficeConstants.TBA_DATE_FORMAT).getTime());
			if (effectiveDate.equals(effectDate)) {
				try {
					util.reset();
					util.setSql("DELETE FROM STAFFLAMHIER WHERE ENTITY_CODE=? AND STAFF_CODE=? ");
					util.setLong(1, entityCode);
					util.setString(2, staffCode);
					util.executeUpdate();
					
					util.reset();
					util.setMode(DBUtil.PREPARED);
					util.setSql("SELECT ENTITY_CODE,STAFF_CODE,EFF_DATE,LAM_HIER_CODE FROM STAFFLAMHIERHIST WHERE ENTITY_CODE=? AND STAFF_CODE=? AND EFF_DATE=?");
					util.setLong(1, entityCode);
					util.setString(2, staffCode);
					util.setDate(3, effectDate);
					ResultSet rs = util.executeQuery();
					if (rs.next()) {
						util1.reset();
						util1.setMode(DBUtil.PREPARED);
						util1.setSql("INSERT INTO STAFFLAMHIER VALUES(?,?,?,?)");
						util1.setLong(1, entityCode);
						util1.setString(2, staffCode);
						util1.setString(3, rs.getString("LAM_HIER_CODE"));
						util1.setDate(4, effectDate);
						int count = util1.executeUpdate();
						if (count == 0) {
							throw new TBAFrameworkException();
						}
					}

				} catch (Exception e) {
					e.printStackTrace();
					throw new TBAFrameworkException(e.getLocalizedMessage());
				} finally {
					util.reset();
					util1.reset();
				}
			}
		}

		return resultDTO;
	}
	
}
