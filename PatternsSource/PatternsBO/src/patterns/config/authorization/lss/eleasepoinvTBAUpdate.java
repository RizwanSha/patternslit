package patterns.config.authorization.lss;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;

import patterns.config.authorization.CommonTBAUpdate;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;

public class eleasepoinvTBAUpdate extends CommonTBAUpdate {

	@Override
	public DTObject processTBAUpdate(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		DTObject resultDTO = new DTObject();
		splitSourceKey(inputDTO);
		try {
			if (inputDTO.get(PROCESS_OPTION).equals(AUTHORIZE) && inputDTO.get(PROCESS_STAGE).equals(AFTER)) {
				resultDTO = getleasepoDTLDetails(dbContext, true);
				if (resultDTO.get("INV_FOR").equals("P")) {
					resultDTO = updateLeasepoInvPay(dbContext, true);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new TBAFrameworkException();
		} finally {
		}

		return resultDTO;
	}

	public DTObject getleasepoDTLDetails(DBContext dbContext, boolean authorize) throws TBAFrameworkException {
		long entityCode = Long.parseLong(strsrcKey[0]);
		String contractdate = strsrcKey[1];
		Date contractDate = new java.sql.Date(BackOfficeFormatUtils.getDate(contractdate, BackOfficeConstants.TBA_DATE_FORMAT).getTime());
		String conSl = strsrcKey[2];
		String poSl = strsrcKey[3];
		String invSl = strsrcKey[4];
		DBUtil util = dbContext.createUtilInstance();
		DTObject resultDTO = new DTObject();
		try {
			util.reset();
			util.setMode(DBUtil.PREPARED);
			util.setSql("SELECT INV_FOR FROM LEASEPOINV L WHERE L.ENTITY_CODE=? AND L.CONTRACT_DATE=? AND L.CONTRACT_SL=? AND L.PO_SL=? AND L.INVOICE_SL=?");
			util.setLong(1, entityCode);
			util.setDate(2, contractDate);
			util.setString(3, conSl);
			util.setString(4, poSl);
			util.setString(5, invSl);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				resultDTO.set("INV_FOR", rset.getString("INV_FOR"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new TBAFrameworkException();
		} finally {
			util.reset();
		}
		return resultDTO;
	}

	public DTObject updateLeasepoInvPay(DBContext dbContext, boolean authorize) throws TBAFrameworkException {
		long entityCode = Long.parseLong(strsrcKey[0]);
		String contractdate = strsrcKey[1];
		Date contractDate = new java.sql.Date(BackOfficeFormatUtils.getDate(contractdate, BackOfficeConstants.TBA_DATE_FORMAT).getTime());
		String conSl = strsrcKey[2];
		String poSl = strsrcKey[3];
		String invSl = strsrcKey[4];
		DBUtil util = dbContext.createUtilInstance();
		DTObject resultDTO = new DTObject();
		try {
			util.reset();
			util.setMode(DBUtil.PREPARED);
			util.setSql("SELECT ENTITY_CODE,CONTRACT_DATE,CONTRACT_SL,PO_SL,INVOICE_SL,DTL_SL,PAY_SL FROM LEASEPOINVDTL L WHERE L.ENTITY_CODE=? AND L.CONTRACT_DATE=? AND L.CONTRACT_SL=? AND L.PO_SL=? AND L.INVOICE_SL=?");
			util.setLong(1, entityCode);
			util.setDate(2, contractDate);
			util.setString(3, conSl);
			util.setString(4, poSl);
			util.setString(5, invSl);
			ResultSet rset = util.executeQuery();
			while (rset.next()) {
				resultDTO.set("PAY_SL", rset.getString("PAY_SL"));
				updateInvPay(dbContext, resultDTO);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new TBAFrameworkException();
		} finally {
			util.reset();
		}
		return resultDTO;
	}

	public void updateInvPay(DBContext dbContext, DTObject inputDTO) throws TBAFrameworkException {
		DBUtil util = dbContext.createUtilInstance();
		long entityCode = Long.parseLong(strsrcKey[0]);
		String contractdate = strsrcKey[1];
		Date contractDate = new java.sql.Date(BackOfficeFormatUtils.getDate(contractdate, BackOfficeConstants.TBA_DATE_FORMAT).getTime());
		String conSl = strsrcKey[2];
		try {
			util.reset();
			util.setMode(DBUtil.PREPARED);
			util.setSql("UPDATE LEASEPOINVPAY SET PAY_FOR='P' WHERE ENTITY_CODE=? AND CONTRACT_DATE=? AND CONTRACT_SL=? AND PAYMENT_SL=?");
			util.setLong(1, entityCode);
			util.setDate(2, contractDate);
			util.setString(3, conSl);
			util.setString(4, inputDTO.get("PAY_SL"));
			int cnt = util.executeUpdate();
			if (cnt != 1) {
				throw new TBAFrameworkException();
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			util.reset();
		}
	}

	@Override
	public DTObject processEffDateTBAUpdate(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		// TODO Auto-generated method stub
		return null;
	}

}
