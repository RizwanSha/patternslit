package patterns.config.authorization.lss;

import java.sql.ResultSet;

import patterns.config.authorization.CommonTBAUpdate;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.service.DTObject;

public class eleasepoTBAUpdate extends CommonTBAUpdate {

	public DTObject processTBAUpdate(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		DTObject resultDTO = new DTObject();
		DBUtil util = dbContext.createUtilInstance();
		DBUtil util1 = dbContext.createUtilInstance();
		if (inputDTO.get(PROCESS_OPTION).equals(AUTHORIZE) && inputDTO.get(PROCESS_STAGE).equals(AFTER)) {
			splitSourceKey(inputDTO);
			long entityCode = Long.parseLong(strsrcKey[0]);
			String lesseeCode = strsrcKey[1];
			String agreementNo = strsrcKey[2];
			String poSl = strsrcKey[3];
			try {
				util.reset();
				util.setMode(DBUtil.PREPARED);
				util.setSql("SELECT LESSEE_CODE,AGREEMENT_NO,SCHEDULE_ID,PO_SL FROM LEASEPO WHERE ENTITY_CODE=? AND LESSEE_CODE=? AND AGREEMENT_NO=? AND PO_SL=?");
				util.setLong(1, entityCode);
				util.setString(2, lesseeCode);
				util.setString(3, agreementNo);
				util.setString(4, poSl);
				ResultSet rs = util.executeQuery();
				if (rs.next()) {
					String scheduleID = rs.getString("SCHEDULE_ID");
					if (scheduleID!=null) {
						util1.reset();
						util1.setMode(DBUtil.PREPARED);
						util1.setSql("INSERT INTO LEASEPODETAILS VALUES(?,?,?,?,?)");
						util1.setLong(1, entityCode);
						util1.setString(2, lesseeCode);
						util1.setString(3, agreementNo);
						util1.setString(4, scheduleID);
						util1.setInt(5, Integer.parseInt(poSl));
						int count = util1.executeUpdate();
						if (count == 0) {
							throw new TBAFrameworkException();
						}
					}
				}

			} catch (Exception e) {
				e.printStackTrace();
				throw new TBAFrameworkException(e.getLocalizedMessage());
			} finally {
				util.reset();
				util1.reset();
			}
		}

		return resultDTO;
	}

	public DTObject processEffDateTBAUpdate(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		return null;
	}
}
