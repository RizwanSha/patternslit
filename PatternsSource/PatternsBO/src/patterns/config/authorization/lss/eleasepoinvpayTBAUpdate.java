package patterns.config.authorization.lss;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;

import patterns.config.authorization.CommonTBAUpdate;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;

public class eleasepoinvpayTBAUpdate extends CommonTBAUpdate {

	@Override
	public DTObject processTBAUpdate(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		DTObject resultDTO = new DTObject();
		splitSourceKey(inputDTO);
		try {
			if (inputDTO.get(PROCESS_OPTION).equals(AUTHORIZE) && inputDTO.get(PROCESS_STAGE).equals(AFTER)) {
				resultDTO = getleasepoInvPayDetails(dbContext);
				updateLeasepo(dbContext, resultDTO);
				if (null != resultDTO.get("INVOICE_SL")) {
					updateLeasepoInv(dbContext, resultDTO);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new TBAFrameworkException();
		} finally {
		}

		return resultDTO;
	}

	public DTObject getleasepoInvPayDetails(DBContext dbContext) throws TBAFrameworkException {
		long entityCode = Long.parseLong(strsrcKey[0]);
		String contractdate = strsrcKey[1];
		Date contractDate = new java.sql.Date(BackOfficeFormatUtils.getDate(contractdate, BackOfficeConstants.TBA_DATE_FORMAT).getTime());
		String conSl = strsrcKey[2];
		String paySL = strsrcKey[3];
		DBUtil util = dbContext.createUtilInstance();
		DTObject resultDTO = new DTObject();
		try {
			util.reset();
			util.setMode(DBUtil.PREPARED);
			util.setSql("SELECT PO_SL,PAY_FOR,INVOICE_SL,PAYMENT_AMOUNT,PAYMENT_AMOUNT_BC,PAYMENT_REFERENCE FROM LEASEPOINVPAY L WHERE L.ENTITY_CODE=? AND L.CONTRACT_DATE=? AND L.CONTRACT_SL=? AND L.PAYMENT_SL=?");
			util.setLong(1, entityCode);
			util.setDate(2, contractDate);
			util.setString(3, conSl);
			util.setString(4, paySL);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				resultDTO.set("PO_SL", rset.getString("PO_SL"));
				resultDTO.set("PAY_FOR", rset.getString("PAY_FOR"));
				resultDTO.set("INVOICE_SL", rset.getString("INVOICE_SL"));
				resultDTO.set("PAYMENT_AMOUNT_BC", rset.getString("PAYMENT_AMOUNT_BC"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new TBAFrameworkException();
		} finally {
			util.reset();
		}
		return resultDTO;
	}

	public void updateLeasepo(DBContext dbContext, DTObject inputDTO) throws TBAFrameworkException {
		DBUtil util = dbContext.createUtilInstance();
		long entityCode = Long.parseLong(strsrcKey[0]);
		String contractdate = strsrcKey[1];
		Date contractDate = new java.sql.Date(BackOfficeFormatUtils.getDate(contractdate, BackOfficeConstants.TBA_DATE_FORMAT).getTime());
		String conSl = strsrcKey[2];
		try {
			util.reset();
			util.setMode(DBUtil.PREPARED);
			util.setSql("UPDATE LEASEPO SET PO_AMOUNT_PAID_SO_FAR=(PO_AMOUNT_PAID_SO_FAR+?) WHERE ENTITY_CODE=? AND CONTRACT_DATE=? AND CONTRACT_SL=? AND PO_SL=?");
			util.setBigDecimal(1, new BigDecimal(inputDTO.get("PAYMENT_AMOUNT_BC")));
			util.setLong(2, entityCode);
			util.setDate(3, contractDate);
			util.setString(4, conSl);
			util.setString(5, inputDTO.get("PO_SL"));
			int cnt = util.executeUpdate();
			if (cnt != 1) {
				throw new TBAFrameworkException();
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			util.reset();
		}
	}

	public void updateLeasepoInv(DBContext dbContext, DTObject inputDTO) throws TBAFrameworkException {
		DBUtil util = dbContext.createUtilInstance();
		long entityCode = Long.parseLong(strsrcKey[0]);
		String contractdate = strsrcKey[1];
		Date contractDate = new java.sql.Date(BackOfficeFormatUtils.getDate(contractdate, BackOfficeConstants.TBA_DATE_FORMAT).getTime());
		String conSl = strsrcKey[2];
		try {
			util.reset();
			util.setMode(DBUtil.PREPARED);
			util.setSql("UPDATE LEASEPOINV SET INVOICE_AMOUNT_PAID_SOFAR=(INVOICE_AMOUNT_PAID_SOFAR+?) WHERE ENTITY_CODE=? AND CONTRACT_DATE=? AND CONTRACT_SL=? AND PO_SL=? AND INVOICE_SL=?");
			util.setBigDecimal(1, new BigDecimal(inputDTO.get("PAYMENT_AMOUNT_BC")));
			util.setLong(2, entityCode);
			util.setDate(3, contractDate);
			util.setString(4, conSl);
			util.setString(5, inputDTO.get("PO_SL"));
			util.setString(6, inputDTO.get("INVOICE_SL"));
			int cnt = util.executeUpdate();
			if (cnt != 1) {
				throw new TBAFrameworkException();
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			util.reset();
		}
	}

	@Override
	public DTObject processEffDateTBAUpdate(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		// TODO Auto-generated method stub
		return null;
	}

}
