package patterns.config.authorization.lss;

import java.sql.Date;

import patterns.config.authorization.CommonTBAUpdate;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;

public class iinvpolicyTBAUpdate extends CommonTBAUpdate {
	public DTObject processTBAUpdate(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		return null;
	}

	public DTObject processEffDateTBAUpdate(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		DTObject resultDTO = new DTObject();
		DBUtil util = dbContext.createUtilInstance();
		if (inputDTO.get(PROCESS_OPTION).equals(AUTHORIZE)) {
			splitSourceKey(inputDTO);
			String entitycode = strsrcKey[0];
			String invoiceCurrency = strsrcKey[1];
			String efftDateString = strsrcKey[2];
			Date effectiveDate = new java.sql.Date(BackOfficeFormatUtils.getDate(efftDateString, BackOfficeConstants.TBA_DATE_FORMAT).getTime());
			try {
				util.reset();
				util.setSql("DELETE  FROM INVPOLICY WHERE ENTITY_CODE=? AND CCY_CODE=?");
				util.setString(1, entitycode);
				util.setString(2, invoiceCurrency);
				util.executeUpdate();

				util.reset();
				util.setMode(DBUtil.PREPARED);
				util.setSql("INSERT INTO INVPOLICY(ENTITY_CODE,CCY_CODE,EFF_DATE) SELECT ENTITY_CODE,CCY_CODE,EFF_DATE FROM INVPOLICYHIST WHERE ENTITY_CODE=? AND CCY_CODE=? AND EFF_DATE=? ");
				util.setString(1, entitycode);
				util.setString(2, invoiceCurrency);
				util.setDate(3, effectiveDate);
				util.executeUpdate();
				
				util.reset();
				util.setSql("DELETE  FROM INVPOLICYDTL WHERE ENTITY_CODE=? AND CCY_CODE=?");
				util.setString(1, entitycode);
				util.setString(2, invoiceCurrency);
				util.executeUpdate();

				util.reset();
				util.setMode(DBUtil.PREPARED);
				util.setSql("INSERT INTO INVPOLICYDTL(ENTITY_CODE,CCY_CODE,DTL_SL) SELECT ENTITY_CODE,MPGM_ID,DTL_SL FROM INVPOLICYHISTDTL WHERE ENTITY_CODE=? AND CCY_CODE=? AND EFF_DATE=? ");
				util.setString(1, entitycode);
				util.setString(2, invoiceCurrency);
				util.setDate(3, effectiveDate);
				util.executeUpdate();
				
			} catch (Exception e) {
				e.printStackTrace();
				throw new TBAFrameworkException(e.getLocalizedMessage());
			} finally {
				util.reset();
			}
		}
		return resultDTO;
	}
}
