package patterns.config.authorization.lss;

import java.sql.Date;
import java.sql.ResultSet;

import patterns.config.authorization.CommonTBAUpdate;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;

public class istaffportfolioTBAUpdate extends CommonTBAUpdate {
	@Override
	public DTObject processTBAUpdate(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		return null;
	}

	@Override
	public DTObject processEffDateTBAUpdate(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		splitSourceKey(inputDTO);
		String entitycode = strsrcKey[0];
		String branchCode = strsrcKey[1];
		String efftDateString = strsrcKey[2];
		Date effectiveDate = new java.sql.Date(BackOfficeFormatUtils.getDate(efftDateString, BackOfficeConstants.TBA_DATE_FORMAT).getTime());
		DTObject resultDTO = new DTObject();
		DBUtil util = dbContext.createUtilInstance();
		DBUtil util1 = dbContext.createUtilInstance();
		if (inputDTO.get(PROCESS_OPTION).equals(AUTHORIZE)) {
			try {
				util.reset();
				util.setSql("DELETE FROM STAFFPFROLES WHERE ENTITY_CODE=? AND BRANCH_CODE=? AND EFF_DATE=?  ");
				util.setString(1, entitycode);
				util.setString(2, branchCode);
				util.setDate(3, effectiveDate);
				util.executeUpdate();

				util.reset();
				util.setMode(DBUtil.PREPARED);
				util.setSql("INSERT INTO STAFFPFROLES(ENTITY_CODE,BRANCH_CODE,STAFF_ROLE_CODE,EFF_DATE,NOF_STAFF)SELECT ENTITY_CODE,BRANCH_CODE,STAFF_ROLE_CODE,EFF_DATE,NOF_STAFF FROM STAFFPFROLESHIST WHERE ENTITY_CODE=? AND BRANCH_CODE=? AND EFF_DATE=? ");
				util.setString(1, entitycode);
				util.setString(2, branchCode);
				util.setDate(3, effectiveDate);
				int count = util.executeUpdate();
				if (count == 0)
					throw new TBAFrameworkException();

				util.reset();
				util.setSql("DELETE FROM STAFFPFROLECONF WHERE ENTITY_CODE=? AND BRANCH_CODE=?");
				util.setString(1, entitycode);
				util.setString(2, branchCode);
				util.executeUpdate();

				util.reset();
				util.setMode(DBUtil.PREPARED);
				util.setSql("INSERT INTO STAFFPFROLECONF(ENTITY_CODE,BRANCH_CODE,STAFF_ROLE_CODE,DTL_SL,STAFF_CODE,PORTFOLIO_CODE)SELECT ENTITY_CODE,BRANCH_CODE,STAFF_ROLE_CODE,DTL_SL,STAFF_CODE,PORTFOLIO_CODE FROM STAFFPFROLECONFHIST WHERE ENTITY_CODE=? AND BRANCH_CODE=? AND EFF_DATE=? ");
				util.setString(1, entitycode);
				util.setString(2, branchCode);
				util.setDate(3, effectiveDate);
				int count1 = util.executeUpdate();
				if (count1 == 0)
					throw new TBAFrameworkException();

				int noOfStaff = 0;
				util.reset();
				util.setMode(DBUtil.PREPARED);
				util.setSql("SELECT SDTL.ENTITY_CODE,SDTL.BRANCH_CODE,SDTL.STAFF_ROLE_CODE,SDTL.STAFF_CODE,S.NOF_STAFF FROM STAFFPFROLECONF SDTL JOIN STAFFPFROLES S ON (S.ENTITY_CODE=SDTL.ENTITY_CODE AND S.BRANCH_CODE=SDTL.BRANCH_CODE AND S.STAFF_ROLE_CODE=SDTL.STAFF_ROLE_CODE AND S.NOF_STAFF='1') WHERE SDTL.ENTITY_CODE=? AND SDTL.BRANCH_CODE=? AND S.EFF_DATE=? ");
				util.setString(1, entitycode);
				util.setString(2, branchCode);
				util.setDate(3, effectiveDate);
				ResultSet rset = util.executeQuery();
				if (rset.next()) {
					noOfStaff = Integer.parseInt(rset.getString(5));
				}
				if (noOfStaff == 1) {
					util1.reset();
					util1.setMode(DBUtil.PREPARED);
					util1.setSql("UPDATE LEASE SET RM_STAFF_CODE = ? WHERE ENTITY_CODE=? AND  LESSOR_BRANCH_CODE=? AND E_STATUS='A' AND LEASE_CLOSURE_ON IS NULL ");
					util1.setString(1, rset.getString(4));
					util1.setLong(2, rset.getLong(1));
					util1.setString(3, rset.getString(2));
					int count2 = util1.executeUpdate();
					if (count2 == 0)
						throw new TBAFrameworkException();
				} else {
					util.reset();
					util.setMode(DBUtil.PREPARED);
					util.setSql("SELECT SDTL.ENTITY_CODE,SDTL.BRANCH_CODE,SDTL.STAFF_ROLE_CODE,SDTL.STAFF_CODE,S.NOF_STAFF,SDTL.PORTFOLIO_CODE FROM STAFFPFROLECONF SDTL JOIN STAFFPFROLES S ON (S.ENTITY_CODE=SDTL.ENTITY_CODE AND S.BRANCH_CODE=SDTL.BRANCH_CODE AND S.STAFF_ROLE_CODE=SDTL.STAFF_ROLE_CODE) WHERE SDTL.ENTITY_CODE=? AND SDTL.BRANCH_CODE=? AND S.EFF_DATE=? ");
					util.setString(1, entitycode);
					util.setString(2, branchCode);
					util.setDate(3, effectiveDate);
					ResultSet rs1 = util.executeQuery();
					while (rs1.next()) {
						util1.reset();
						util1.setMode(DBUtil.PREPARED);
						util1.setSql("UPDATE LEASE L JOIN CUSTOMERACDTL C ON (C.ENTITY_CODE=L.ENTITY_CODE AND C.LEASE_PRODUCT_CODE=L.LEASE_PRODUCT_CODE AND C.SUNDRY_DB_AC=L.LESSEE_CODE) SET L.RM_STAFF_CODE = ? WHERE L.ENTITY_CODE=? AND C.PORTFOLIO_CODE=? ");
						util1.setString(1, rs1.getString(4));
						util1.setLong(2, rs1.getLong(1));
						util1.setString(3, rs1.getString(6));
						util1.executeUpdate();
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
				throw new TBAFrameworkException(e.getLocalizedMessage());
			} finally {
				util.reset();
			}
		}
		return resultDTO;

	}
}
