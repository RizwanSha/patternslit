package patterns.config.authorization.lss;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.Types;

import patterns.config.authorization.CommonTBAUpdate;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;

public class eleasepoinvupldTBAUpdate extends CommonTBAUpdate {

	@Override
	public DTObject processTBAUpdate(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		// TODO Auto-generated method stub
		DTObject resultDTO = new DTObject();
		DBUtil util = dbContext.createUtilInstance();
		DBUtil util1 = dbContext.createUtilInstance();
		if (inputDTO.get(PROCESS_OPTION).equals(AUTHORIZE) && inputDTO.get(PROCESS_STAGE).equals(AFTER)) {
			splitSourceKey(inputDTO);
			long entitycode = Long.parseLong(strsrcKey[0]);
			String enDate = strsrcKey[1];
			Date entryDate = new java.sql.Date(BackOfficeFormatUtils.getDate(enDate, BackOfficeConstants.TBA_DATE_FORMAT).getTime());
			String entrySl = strsrcKey[2];
			try {
				util.reset();
				util.setSql("SELECT L.CONTRACT_DATE,L.CONTRACT_SL,L.PO_SL,LD.INVOICE_AMOUNT,LD.INV_FOR FROM LEASEPOINVUPLD L INNER JOIN  LEASEPOINVUPLDDTL LD ON (L.ENTITY_CODE=LD.ENTITY_CODE AND L.ENTRY_DATE=LD.ENTRY_DATE AND L.ENTRY_SL=LD.ENTRY_SL)  WHERE L.ENTITY_CODE=? AND L.ENTRY_DATE=? AND L.ENTRY_SL=?");
				util.setLong(1, entitycode);
				util.setDate(2, entryDate);
				util.setInt(3, Integer.parseInt(entrySl));
				ResultSet rset = util.executeQuery();
				while (rset.next()) {
					if (rset.getString("INV_FOR").equals("P")) {
						util1.reset();
						util1.setSql("SELECT SUM(PAYMENT_AMOUNT)PAYMENT_AMOUNT FROM LEASEPOINVPAY WHERE ENTITY_CODE=? AND CONTRACT_DATE=? AND CONTRACT_SL=? AND PO_SL=? AND PAY_FOR='A' AND INVOICE_SL IS NULL");
						util1.setLong(1, entitycode);
						util1.setString(2, rset.getString("CONTRACT_DATE"));
						util1.setInt(3, Integer.parseInt(rset.getString("CONTRACT_SL")));
						util1.setInt(4, Integer.parseInt(rset.getString("PO_SL")));
						ResultSet rs = util1.executeQuery();
						if (rs.next()) {
							BigDecimal w_invoiceAmount = new BigDecimal(rset.getString("INVOICE_AMOUNT"));
							BigDecimal w_po_amount = new BigDecimal(rs.getString("PAYMENT_AMOUNT"));
							if (w_po_amount.compareTo(w_invoiceAmount) < 0) {
								throw new TBAFrameworkException("Total Paid Amount is lesser the Invoice Amount specified");
							} else if (w_po_amount.compareTo(w_invoiceAmount) > 0) {
								throw new TBAFrameworkException("Total Paid Amount exceeds the Invoice Amount specified");
							}
						}
					}
				}
				util.reset();
				util.setMode(DBUtil.CALLABLE);
				util.setSql("{CALL SP_ELEASEPOINVUPLD_UPDATE(?,?,?,?)}");
				util.setLong(1, entitycode);
				util.setDate(2, entryDate);
				util.setString(3, entrySl);
				util.registerOutParameter(4, Types.VARCHAR);
				util.execute();
				if (!util.getString(4).equals(RegularConstants.SP_SUCCESS)) {
					throw new Exception(util.getString(4));
				}
			} catch (Exception e) {
				e.printStackTrace();
				throw new TBAFrameworkException(e.getLocalizedMessage());
			} finally {
				util.reset();
			}
		}
		return resultDTO;
	}

	@Override
	public DTObject processEffDateTBAUpdate(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {

		return null;
	}
}