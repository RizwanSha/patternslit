package patterns.config.authorization.lss;

import java.sql.Date;
import java.sql.ResultSet;

import patterns.config.authorization.CommonTBAUpdate;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.service.DTObject;

public class eleaseTBAUpdate extends CommonTBAUpdate {

	public DTObject processEffDateTBAUpdate(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		return null;
	}

	public DTObject processTBAUpdate(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		DTObject resultDTO = new DTObject();
		DBUtil util = dbContext.createUtilInstance();
		if (inputDTO.get(PROCESS_OPTION).equals(AUTHORIZE) && inputDTO.get(PROCESS_STAGE).equals(AFTER)) {
			splitSourceKey(inputDTO);
			String entityCode = strsrcKey[0];
			String lesseeCode = strsrcKey[1];
			String agreementNo = strsrcKey[2];
			String scheduleId = strsrcKey[3];
			Date contractDate = null;
			String contractSl = null;
			try {
				util.reset();
				util.setMode(DBUtil.PREPARED);
				util.setSql("SELECT CONTRACT_DATE,CONTRACT_SL FROM LEASE WHERE ENTITY_CODE = ? AND LESSEE_CODE = ? AND AGREEMENT_NO=? AND SCHEDULE_ID=? ");
				util.setString(1, entityCode);
				util.setString(2, lesseeCode);
				util.setString(3, agreementNo);
				util.setString(4, scheduleId);
				ResultSet rs = util.executeQuery();
				if (rs.next()) {
					contractDate = rs.getDate("CONTRACT_DATE");
					contractSl = rs.getString("CONTRACT_SL");
				}
				if (contractSl != null && contractDate != null) {
					if (!contractSl.isEmpty() && !String.valueOf(contractDate).isEmpty()) {
						util.reset();
						util.setMode(DBUtil.PREPARED);
						util.setSql("UPDATE PRELEASECONTRACT SET AGREEMENT_NO=?,SCHEDULE_ID=?  WHERE  ENTITY_CODE = ? AND CONTRACT_DATE = ? AND  CONTRACT_SL = ? AND E_STATUS='A' ");
						util.setString(1, agreementNo);
						util.setString(2, scheduleId);
						util.setString(3, entityCode);
						util.setDate(4, contractDate);
						util.setString(5, contractSl);
						util.executeUpdate();
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
				throw new TBAFrameworkException(e.getLocalizedMessage());
			} finally {
				util.reset();
			}
		}
		return resultDTO;
	}
}