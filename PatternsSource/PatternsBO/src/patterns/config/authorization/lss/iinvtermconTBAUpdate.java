package patterns.config.authorization.lss;
import java.sql.Date;
import patterns.config.authorization.CommonTBAUpdate;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;

public class iinvtermconTBAUpdate extends CommonTBAUpdate {

	@Override
	public DTObject processTBAUpdate(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DTObject processEffDateTBAUpdate(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		DTObject resultDTO = new DTObject();
		DBUtil util = dbContext.createUtilInstance();

		if (inputDTO.get(PROCESS_OPTION).equals(AUTHORIZE)) {
			splitSourceKey(inputDTO);
			long entitycode = Long.parseLong(strsrcKey[0]);
			String stateCode = strsrcKey[1];
			String effectDate = strsrcKey[2];
			Date effectiveDate = new java.sql.Date(BackOfficeFormatUtils.getDate(effectDate, BackOfficeConstants.TBA_DATE_FORMAT).getTime());
			try {
				util.reset();
				util.setSql("DELETE FROM INVTERMCON WHERE ENTITY_CODE=? AND STATE_CODE=?");
				util.setLong(1, entitycode);
				util.setString(2, stateCode);
				util.executeUpdate();
				
				util.reset();
				util.setSql("INSERT INTO INVTERMCON SELECT ENTITY_CODE,STATE_CODE,EFF_DATE,TERMS_CONDITION FROM INVTERMCONHIST WHERE ENTITY_CODE=? AND STATE_CODE=? AND EFF_DATE=?");
				util.setLong(1, entitycode);
				util.setString(2, stateCode);
				util.setDate(3, effectiveDate);
				util.executeUpdate();
				
			} catch (Exception e) {
				e.printStackTrace();
				throw new TBAFrameworkException(e.getLocalizedMessage());
			} finally {
				util.reset();
			}
		}
		return resultDTO;
	}
}
