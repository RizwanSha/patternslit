package patterns.config.authorization.lss;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import patterns.config.authorization.CommonTBAUpdate;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.service.DTObject;

public class eleaseintTBAUpdate extends CommonTBAUpdate {

	@Override
	public DTObject processTBAUpdate(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		DTObject resultDTO = new DTObject();
		DBUtil util = dbContext.createUtilInstance();
		DBUtil util1 = dbContext.createUtilInstance();
		if (inputDTO.get(PROCESS_OPTION).equals(AUTHORIZE) && inputDTO.get(PROCESS_STAGE).equals(AFTER)) {
			splitSourceKey(inputDTO);
			String entitycode = strsrcKey[0];
			String leeseCode = strsrcKey[1];
			String agreementCode = strsrcKey[2];
			String scheduleId = strsrcKey[3];
			String entrySl = strsrcKey[4];
			int generatedSerial = 0;
			try {
				util.setMode(DBUtil.PREPARED);
				util.setSql("SELECT * FROM LEASEINT WHERE ENTITY_CODE=? AND LESSEE_CODE=? AND AGREEMENT_NO=? AND SCHEDULE_ID=? AND  ENTRY_SL =?");
				util.setString(1, entitycode);
				util.setString(2, leeseCode);
				util.setString(3, agreementCode);
				util.setString(4, scheduleId);
				util.setString(5, entrySl);
				ResultSet rs = util.executeQuery();
				if (rs.next()) {
					String sourceKey = rs.getString("ENTITY_CODE") + RegularConstants.PK_SEPARATOR + rs.getString("LESSEE_CODE") + RegularConstants.PK_SEPARATOR + rs.getString("AGREEMENT_NO")+ RegularConstants.PK_SEPARATOR + rs.getString("SCHEDULE_ID");
					generatedSerial = generateSerial(dbContext,"ELEASEINT",sourceKey);
					util1.reset();
					util1.setMode(DBUtil.PREPARED);
					util1.setSql("INSERT INTO LEASEFINANCESCHEDULE(ENTITY_CODE,LESSEE_CODE,AGREEMENT_NO,SCHEDULE_ID,INV_FOR,DTL_SL,RENTAL_START_DATE,RENTAL_END_DATE,PRINCIPAL_AMOUNT,ASSET_CCY,INTEREST_AMOUNT,RENTAL_AMOUNT,EXECUTORY_AMOUNT,SCHEDULED_INV_DATE,PICKED_FOR_INVOICE) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
					util1.setString(1, rs.getString("ENTITY_CODE"));
					util1.setString(2, rs.getString("LESSEE_CODE"));
					util1.setString(3, rs.getString("AGREEMENT_NO"));
					util1.setString(4, rs.getString("SCHEDULE_ID"));
					util1.setInt(5, 4);
					util1.setInt(6, generatedSerial);
					util1.setString(7, rs.getString("INT_FROM_DATE"));
					util1.setString(8, rs.getString("INT_UPTO_DATE"));
					util1.setInt(9, 0);
					util1.setString(10, rs.getString("INV_AMOUNT_CCY"));
					util1.setString(11, rs.getString("INV_AMOUNT"));
					util1.setInt(12, 0);
					util1.setInt(13, 0);
					util1.setString(14, rs.getString("INV_DATE"));
					util1.setInt(15, 0);
					util1.executeUpdate();
				}
				util.reset();
				util.setMode(DBUtil.PREPARED);
				util.setSql("UPDATE LEASEINT SET FINANCE_SCHED_DTL_SL=? WHERE ENTITY_CODE=? AND LESSEE_CODE=? AND AGREEMENT_NO=? AND SCHEDULE_ID=? AND  ENTRY_SL =?");
				util.setDouble(1, generatedSerial);
				util.setString(2, entitycode);
				util.setString(3, leeseCode);
				util.setString(4, agreementCode);
				util.setString(5, scheduleId);
				util.setString(6, entrySl);
				util.executeUpdate();
			} catch (Exception e) {
				e.printStackTrace();
				throw new TBAFrameworkException(e.getLocalizedMessage());
			} finally {
				util.reset();
				util1.reset();
			}
		}
		return resultDTO;
	}

	public int generateSerial(DBContext dbContext, String processId, String sourceKey) {
		DBUtil util = dbContext.createUtilInstance();
		int serial = 0;
		try {
			util.reset();
			util.setMode(DBUtil.CALLABLE);
			String sql = "CALL FN_GET_TBA_REFSL(?,?,?)";
			util.setSql(sql);
			util.setString(1, processId);
			util.setString(2, sourceKey);
			util.registerOutParameter(3, Types.INTEGER);
			util.execute();
			serial = util.getInt(3);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return serial;
	}

	@Override
	public DTObject processEffDateTBAUpdate(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		// TODO Auto-generated method stub
		return null;
	}
}
