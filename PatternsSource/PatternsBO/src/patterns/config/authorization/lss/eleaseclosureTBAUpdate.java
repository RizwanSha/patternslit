package patterns.config.authorization.lss;

import java.sql.Date;
import java.sql.ResultSet;

import patterns.config.authorization.CommonTBAUpdate;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;

public class eleaseclosureTBAUpdate extends CommonTBAUpdate {

	public DTObject processTBAUpdate(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		DTObject resultDTO = new DTObject();
		DBUtil util = dbContext.createUtilInstance();
		DBUtil util1 = dbContext.createUtilInstance();
		if (inputDTO.get(PROCESS_OPTION).equals(AUTHORIZE) && inputDTO.get(PROCESS_STAGE).equals(AFTER)) {
			splitSourceKey(inputDTO);
			long entityCode = Long.parseLong(strsrcKey[0]);
			String entryDate = strsrcKey[1];
			String entrySl = strsrcKey[2];
			Date leaseClosureOns = (java.sql.Date) inputDTO.getObject("CBD");
			Date entryDates = new java.sql.Date(BackOfficeFormatUtils.getDate(entryDate, BackOfficeConstants.TBA_DATE_FORMAT).getTime());
			try {
				util.reset();
				util.setMode(DBUtil.PREPARED);
				util.setSql("SELECT LESSEE_CODE,AGREEMENT_NO,SCHEDULE_ID FROM LEASECLOSURE WHERE ENTITY_CODE=? AND ENTRY_DATE=? AND ENTRY_SL=?");
				util.setLong(1, entityCode);
				util.setDate(2, entryDates);
				util.setInt(3, Integer.parseInt(entrySl));
				ResultSet rs = util.executeQuery();
				if (rs.next()) {
					util1.reset();
					util1.setMode(DBUtil.PREPARED);
					util1.setSql("UPDATE LEASE S SET S.LEASE_CLOSURE_ON=?,S.CLOSURE_ENTRY_DATE=?,S.CLOSURE_ENTRY_SL=? WHERE ENTITY_CODE=? AND LESSEE_CODE = ? AND AGREEMENT_NO = ? AND SCHEDULE_ID = ?");
					util1.setDate(1, leaseClosureOns);
					util1.setDate(2, entryDates);
					util1.setInt(3, Integer.parseInt(entrySl));
					util1.setLong(4, entityCode);
					util1.setString(5, rs.getString("LESSEE_CODE"));
					util1.setString(6, rs.getString("AGREEMENT_NO"));
					util1.setString(7, rs.getString("SCHEDULE_ID"));
					int count = util1.executeUpdate();
					if (count == 0) {
						throw new TBAFrameworkException();
					}
				}

			} catch (Exception e) {
				e.printStackTrace();
				throw new TBAFrameworkException(e.getLocalizedMessage());
			} finally {
				util.reset();
				util1.reset();
			}
		}

		return resultDTO;
	}

	public DTObject processEffDateTBAUpdate(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		return null;
	}
}
