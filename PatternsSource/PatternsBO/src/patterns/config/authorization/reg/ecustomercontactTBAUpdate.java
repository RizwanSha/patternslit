package patterns.config.authorization.reg;

import java.sql.Date;
import java.sql.ResultSet;

import patterns.config.authorization.CommonTBAUpdate;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;

public class ecustomercontactTBAUpdate extends CommonTBAUpdate {

	@Override
	public DTObject processTBAUpdate(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DTObject processEffDateTBAUpdate(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		DTObject resultDTO = new DTObject();
		DBUtil util = dbContext.createUtilInstance();
		if (inputDTO.get(PROCESS_OPTION).equals(AUTHORIZE)) {
			splitSourceKey(inputDTO);
			long entitycode = Long.parseLong(strsrcKey[0]);
			String customerId = strsrcKey[1];
			String addrSl = strsrcKey[2];
			String effectDate = strsrcKey[3];
			Date effectiveDate = new java.sql.Date(BackOfficeFormatUtils.getDate(effectDate, BackOfficeConstants.TBA_DATE_FORMAT).getTime());
			try {
				util.reset();
				util.setSql("DELETE FROM CUSTOMERCONTACT WHERE ENTITY_CODE=? AND CUSTOMER_ID=? AND ADDR_SL=?");
				util.setLong(1, entitycode);
				util.setString(2, customerId);
				util.setString(3, addrSl);
				util.executeUpdate();

				util.reset();
				util.setSql("DELETE FROM CUSTOMERCONTACTDTL WHERE ENTITY_CODE=? AND CUSTOMER_ID=? AND ADDR_SL=?");
				util.setLong(1, entitycode);
				util.setString(2, customerId);
				util.setString(3, addrSl);
				util.executeUpdate();

				util.reset();
				util.setSql("SELECT ENABLED FROM CUSTOMERCONTACTHIST WHERE ENTITY_CODE=? AND CUSTOMER_ID=? AND ADDR_SL=? AND EFF_DATE=?");
				util.setLong(1, entitycode);
				util.setString(2, customerId);
				util.setString(3, addrSl);
				util.setDate(4, effectiveDate);
				ResultSet rs = util.executeQuery();
				if (rs.next()) {
					if (rs.getString("ENABLED").equals(RegularConstants.ONE)) {
						util.reset();
						util.setSql("INSERT INTO CUSTOMERCONTACT SELECT ENTITY_CODE,CUSTOMER_ID,ADDR_SL,EFF_DATE FROM CUSTOMERCONTACTHIST WHERE ENTITY_CODE=? AND CUSTOMER_ID=? AND ADDR_SL=? AND EFF_DATE=?");
						util.setLong(1, entitycode);
						util.setString(2, customerId);
						util.setString(3, addrSl);
						util.setDate(4, effectiveDate);
						util.executeUpdate();

						util.reset();
						util.setSql("INSERT INTO CUSTOMERCONTACTDTL SELECT ENTITY_CODE,CUSTOMER_ID,ADDR_SL,DTL_SL,CONTACT_TITLE,CONTACT_PERSON,CONTACT_NUM_1,CONTACT_NUM_2,EMAIL_ID,ALTERNATE_EMAIL FROM CUSTOMERCONTACTHISTDTL WHERE ENTITY_CODE=? AND CUSTOMER_ID=? AND ADDR_SL=? AND EFF_DATE=?");
						util.setLong(1, entitycode);
						util.setString(2, customerId);
						util.setString(3, addrSl);
						util.setDate(4, effectiveDate);
						util.executeUpdate();
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
				throw new TBAFrameworkException(e.getLocalizedMessage());
			} finally {
				util.reset();
			}
		}
		return resultDTO;
	}

}
