package patterns.config.authorization.reg;

import java.sql.ResultSet;

import patterns.config.authorization.CommonTBAUpdate;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.service.DTObject;

public class ecustomerprodregTBAUpdate extends CommonTBAUpdate {

	@Override
	public DTObject processTBAUpdate(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		DTObject resultDTO = new DTObject();
		if (inputDTO.get(PROCESS_OPTION).equals(AUTHORIZE) && inputDTO.get(PROCESS_STAGE).equals(AFTER)) {
			DBUtil dbutil = dbContext.createUtilInstance();
			splitSourceKey(inputDTO);
			String entityCode = strsrcKey[0];
			String customerId = strsrcKey[1];
			String entrySl=strsrcKey[2];
			int dtlSl=0;
			try {
				dbutil.reset();
				dbutil.setMode(DBUtil.PREPARED);
				dbutil.setSql("SELECT MAX(DTL_SL) FROM CUSTOMERACDTL WHERE ENTITY_CODE=? AND CUSTOMER_ID=?");
				dbutil.setLong(1, Long.parseLong(entityCode));
				dbutil.setLong(2, Long.parseLong(customerId));
				ResultSet rs=dbutil.executeQuery();
				if (rs.next()){
					dtlSl=rs.getInt(1);
				}
				dbutil.reset();
				dbutil.setMode(DBUtil.PREPARED);
				dbutil.setSql("INSERT INTO CUSTOMERACDTL (SELECT ENTITY_CODE,CUSTOMER_ID,(@ROWID :=@ROWID +1),LEASE_PRODUCT_CODE,PORTFOLIO_CODE,SUNDRY_DB_AC,PRINCIPAL_AC,INTEREST_AC,UNEARNED_INTEREST_AC FROM CUSTOMERPRODREGACDTL,(SELECT @ROWID:=?) ROWID WHERE  ENTITY_CODE=? AND CUSTOMER_ID=? AND ENTRY_SL=? ORDER BY DTL_SL)");
				dbutil.setInt(1, dtlSl);
				dbutil.setLong(2, Long.parseLong(entityCode));
				dbutil.setLong(3, Long.parseLong(customerId));
				dbutil.setInt(4, Integer.parseInt(entrySl));
				dbutil.executeUpdate();
			} catch (Exception e) {
				throw new TBAFrameworkException(e.getLocalizedMessage());
			} finally {
				dbutil.reset();
			}
		}
		return resultDTO;
	}

	@Override
	public DTObject processEffDateTBAUpdate(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		return null;
	}
}
