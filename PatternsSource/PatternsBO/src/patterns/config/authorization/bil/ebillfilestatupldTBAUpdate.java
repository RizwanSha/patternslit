package patterns.config.authorization.bil;

import java.sql.Date;
import java.sql.Types;

import patterns.config.authorization.CommonTBAUpdate;
import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;

public class ebillfilestatupldTBAUpdate extends CommonTBAUpdate {

	@Override
	public DTObject processTBAUpdate(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {
		// TODO Auto-generated method stub
		DTObject resultDTO = new DTObject();
		DBUtil util = dbContext.createUtilInstance();
		if (inputDTO.get(PROCESS_OPTION).equals(AUTHORIZE) && inputDTO.get(PROCESS_STAGE).equals(AFTER)) {
			splitSourceKey(inputDTO);
			long entitycode = Long.parseLong(strsrcKey[0]);
			String enDate = strsrcKey[1];
			Date entryDate = new java.sql.Date(BackOfficeFormatUtils.getDate(enDate, BackOfficeConstants.TBA_DATE_FORMAT).getTime());
			String entrySl = strsrcKey[2];
			try {
				util.reset();
				util.setMode(DBUtil.CALLABLE);
				util.setSql("{CALL SP_EBILLFILE_UPDATE(?,?,?,?)}");
				util.setLong(1, entitycode);
				util.setDate(2, entryDate);
				util.setString(3, entrySl);
				util.registerOutParameter(4, Types.VARCHAR);
				util.execute();
				if (!util.getString(4).equals(RegularConstants.SP_SUCCESS)) {
					throw new Exception(util.getString(4));
				}
			} catch (Exception e) {
				e.printStackTrace();
				throw new TBAFrameworkException(e.getLocalizedMessage());
			} finally {
				util.reset();
			}
		}
		return resultDTO;
	}

	@Override
	public DTObject processEffDateTBAUpdate(DTObject inputDTO, DBContext dbContext) throws TBAFrameworkException {

		return null;
	}
}