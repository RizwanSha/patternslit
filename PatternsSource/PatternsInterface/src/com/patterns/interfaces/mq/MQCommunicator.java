package com.patterns.interfaces.mq;

import javax.jms.Connection;
import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;

import patterns.config.framework.loggers.ApplicationLogger;

public class MQCommunicator {
	protected ApplicationLogger logger = null;
	private static final String INITIAL_RECONNECT_DELAY = "2000";
	private static final String MAX_RECONNECT_ATTEMPT = "2";
	private static final String MAX_RECONNECT_DELAY = "15000";

	public MQCommunicator() {
		logger = ApplicationLogger.getInstance(this.getClass().getSimpleName());
		logger.logDebug("#()");
	}

	private String hostName;
	private int portNumber;
	private String queueName;
	private StringBuffer url = new StringBuffer("");

	public String getHostName() {
		return hostName;
	}

	public void setHostName(String hostName) {
		this.hostName = hostName;
	}

	public int getPortNumber() {
		return portNumber;
	}

	public void setPortNumber(int portNumber) {
		this.portNumber = portNumber;
	}

	public String getQueueName() {
		return queueName;
	}

	public void setQueueName(String queueName) {
		this.queueName = queueName;
	}

	private ActiveMQConnectionFactory connectionFactory = null;
	private Connection connection = null;

	public void init() throws Exception {
		logger.logDebug("init() - " + getHostName() + " - " + getPortNumber());
		try {
			url.append("failover://(tcp://");
			url.append(getHostName()).append(":").append(getPortNumber())
					.append(")initialReconnectDelay=")
					.append(INITIAL_RECONNECT_DELAY)
					.append("&maxReconnectAttempts=")
					.append(MAX_RECONNECT_ATTEMPT)
					.append("&maxReconnectDelay=").append(MAX_RECONNECT_DELAY);
			connectionFactory = new ActiveMQConnectionFactory(url.toString());
			connection = connectionFactory.createConnection();
			connection.start();
		} catch (Exception e) {
			if (e instanceof JMSException) {
				JMSException jmse = (JMSException) e;
				logger.logError("init(1) Error : Completion Code : "
						+ jmse.getLocalizedMessage() + " Reason : "
						+ jmse.getErrorCode());
			} else {
				logger.logError("init(2) : Error - " + e.getLocalizedMessage());
			}
			throw e;
		}
	}

	public void destroy() {
		logger.logDebug("destroy()");
		try {
			if (connection != null) {
				connection.close();
			}
		} catch (Exception e) {
			if (e instanceof JMSException) {
				JMSException jmse = (JMSException) e;
				logger.logError("destroy(1) Error : Completion Code : "
						+ jmse.getLocalizedMessage() + " Reason : "
						+ jmse.getErrorCode());
			} else {
				logger.logError("destroy(2) Error : " + e.getLocalizedMessage());
			}
		}
	}

	public String receiveMessage() throws JMSException {
		logger.logDebug("receiveMessage() - " + queueName);
		String result = "";
		Session session = null;
		try {
			session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
			Destination destination = session.createQueue(getQueueName());
			MessageConsumer consumer = session.createConsumer(destination);
			Message message = consumer.receive(100);
			if (message instanceof TextMessage) {
				TextMessage textMessage = (TextMessage) message;
				System.out.println("Received message" + textMessage.getText()
						+ "'" + textMessage.getJMSCorrelationID());
				result = textMessage.getText().toString();
			}
			consumer.close();
		} catch (Exception e) {
			result = null;
			if (e instanceof JMSException) {
				JMSException jmse = (JMSException) e;
				logger.logError("receiveMessage(1) Error : Completion Code : "
						+ jmse.getLocalizedMessage() + " Reason : "
						+ jmse.getErrorCode());
			} else {
				logger.logError("receiveMessage(2) Error : "
						+ e.getLocalizedMessage());
			}
		} finally {
			if (session != null)
				session.close();
		}
		return result;
	}

	public boolean sendMessage(String message, String correlationID)
			throws JMSException {
		return sendMessage(message, correlationID, false, null, null);
	}

	public boolean sendMessage(String message, String correlationID,
			boolean appIdRequired, String appIdData, String appOriginData)
			throws JMSException {
		logger.logDebug("sendMessage() - " + correlationID + " - " + queueName);
		Session session = null;
		try {
			session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
			Destination destination = session.createQueue(getQueueName());
			MessageProducer producer = session.createProducer(destination);
			producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
			TextMessage textMessage = session.createTextMessage(message);
			textMessage.setJMSCorrelationID(correlationID);
			producer.send(textMessage);
			System.out.println("Message Sent");
			producer.close();
			return true;
		} catch (Exception e) {
			if (e instanceof JMSException) {
				JMSException jmse = (JMSException) e;
				logger.logError("sendMessage(1) Error : Completion Code : "
						+ jmse.getMessage() + " Reason : "
						+ jmse.getErrorCode());
			} else {
				logger.logError("sendMessage(2) Error : "
						+ e.getLocalizedMessage());
			}
			return false;
		} finally {
			if (session != null)
				session.close();
		}
	}

	public static void main(String[] args) throws Exception {
		ActiveMQConnectionFactory _connectionFactory = new ActiveMQConnectionFactory(
				"failover://tcp://localhost:61616");
		Connection connection = _connectionFactory.createConnection();
		connection.start();
		Session session = connection.createSession(false,
				Session.AUTO_ACKNOWLEDGE);
		Destination destination = session.createQueue("REACH.INQ");
		MessageProducer producer = session.createProducer(destination);
		producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
		String text = "Hello world!";
		TextMessage message = session.createTextMessage(text);
		System.out.println("Sent message: " + message.hashCode() + " : "
				+ Thread.currentThread().getName());
		producer.send(message);

		// MessageConsumer consumer = session.createConsumer(destination);
		// Message _message = consumer.receive();
		//
		// if (_message instanceof TextMessage) {
		// TextMessage textMessage = (TextMessage) _message;
		// System.out.println("Received message '" + textMessage.getText() +
		// "'");
		// }
		//
		// session.close();
		// connection.close();

		/*
		 * MQCommunicator mq = new MQCommunicator(); mq.init();
		 * mq.sendMessage("Hello Babloo", "123", 1, 10);
		 * mq.receiveMessage("12345", 100); mq.destroy();
		 */

	}
}