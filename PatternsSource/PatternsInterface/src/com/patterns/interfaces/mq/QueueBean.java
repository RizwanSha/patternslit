package com.patterns.interfaces.mq;

public class QueueBean {

	private String queueCode;
	private String hostName;
	private int port;
	private String channelName;
	private String queueName;
	private String queueManagerName;
	private int queuePriority;
	private int timeout;
	private int expiryInterval;

	public int getTimeout() {
		return timeout;
	}

	public void setTimeout(int timeout) {
		this.timeout = timeout;
	}

	public int getExpiryInterval() {
		return expiryInterval;
	}

	public void setExpiryInterval(int expiryInterval) {
		this.expiryInterval = expiryInterval;
	}

	public String getQueueCode() {
		return queueCode;
	}

	public void setQueueCode(String queueCode) {
		this.queueCode = queueCode;
	}

	public String getHostName() {
		return hostName;
	}

	public void setHostName(String hostName) {
		this.hostName = hostName;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getChannelName() {
		return channelName;
	}

	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}

	public String getQueueName() {
		return queueName;
	}

	public void setQueueName(String queueName) {
		this.queueName = queueName;
	}

	public int getQueuePriority() {
		return queuePriority;
	}

	public void setQueuePriority(int queuePriority) {
		this.queuePriority = queuePriority;
	}

	public void setQueueManagerName(String queueManagerName) {
		this.queueManagerName = queueManagerName;
	}

	public String getQueueManagerName() {
		return queueManagerName;
	}

	@Override
	public boolean equals(Object interfaceMessageBean) {
		if (interfaceMessageBean != null) {
			if (this.toString().equals(interfaceMessageBean.toString()))
				return true;
		}
		return false;
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}

	@Override
	public String toString() {
		StringBuffer comparisionString = new StringBuffer();
		comparisionString.append(queueManagerName).append(" - ").append(hostName).append(" - ").append(port).append(channelName);
		return comparisionString.toString();
	}
}
