package com.patterns.interfaces;

import java.sql.ResultSet;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.loggers.ApplicationLogger;
import patterns.config.framework.service.DTObject;

import com.patterns.interfaces.mq.QueueBean;

public class Utility {
	public static final String DATETIME_FORMAT = "yyyyMMddhhmmss";
	public static final String DATE_FORMAT = "yyyyMMdd";
	DBContext dbContext = null;
	protected ApplicationLogger logger = null;

	public Utility() {
		logger = ApplicationLogger.getInstance(this.getClass().getSimpleName());
		logger.logDebug("#()");
		dbContext = new DBContext();
	}

	public QueueBean getQueueDetails(String entityCode, String queueCode) {
		logger.logDebug("getQueueDetails() Begin");
		DBUtil util = dbContext.createUtilInstance();
		String query = "";
		QueueBean intfMsgQBean = null;
		try {
			util.reset();
			util.setMode(DBUtil.PREPARED);
			query = "SELECT HOST_NAME ,PORT,CHANNEL_NAME,QUEUE_NAME FROM INTFMSGQ WHERE ENTITY_CODE=? AND QUEUE_CODE=?";
			util.setSql(query);
			util.setString(1, entityCode);
			util.setString(2, queueCode);
			ResultSet rset = util.executeQuery();
			if (rset.next()) {
				intfMsgQBean = new QueueBean();
				intfMsgQBean.setHostName(rset.getString(1));
				intfMsgQBean.setPort(rset.getInt(2));
				intfMsgQBean.setChannelName(rset.getString(3));
				intfMsgQBean.setQueueName(rset.getString(4));
			}
			rset.close();
		} catch (Exception e) {
			logger.logError("getQueueDetails() Error - " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			util.reset();
		}
		logger.logDebug("getQueueDetails() End");
		return intfMsgQBean;
	}

	public QueueBean getOUTQueueDetails(String entity_code, String processing_code, String ssic) {
		logger.logDebug("getOUTQueueDetails() Begin");
		DBUtil util = dbContext.createUtilInstance();
		String query = "";
		ResultSet rs = null;
		QueueBean intfMsgQBean = null;
		try {
			util.reset();
			query = "SELECT INTF_OUTQ_CODE ,INTF_OUTQ_PRIORITY ,INTF_OUTQ_TIMEOUT FROM INTFPCQCFG WHERE ENTITY_CODE=? AND  SYSTEM_CODE=? AND PROCESSING_CODE=?";
			util.setMode(DBUtil.PREPARED);
			util.setSql(query);
			util.setString(1, entity_code);
			util.setString(2, ssic);
			util.setString(3, processing_code);
			rs = util.executeQuery();
			String inqCode = "";
			if (rs.next()) {
				inqCode = rs.getString(1);
				intfMsgQBean = new QueueBean();
				intfMsgQBean.setQueueCode(rs.getString(1));
				intfMsgQBean.setQueuePriority(rs.getInt(2));
				intfMsgQBean.setTimeout(rs.getInt(3));
				rs.close();
				util.reset();
				util.setMode(DBUtil.PREPARED);
				query = "SELECT INTFMSGQ_HOST_NAME ,INTFMSGQ_PORT,INTFMSGQ_CHANNEL_NAME,INTFMSGQ_QUEUE_NAME FROM INTFMSGQ WHERE INTFMSGQ_ENTITY_NUM=? AND  INTFMSGQ_QUEUE_CODE=?";
				util.setSql(query);
				util.setString(1, entity_code);
				util.setString(2, inqCode);
				ResultSet rs1 = util.executeQuery();
				if (rs1.next()) {
					intfMsgQBean.setHostName(rs1.getString(1));
					intfMsgQBean.setPort(rs1.getInt(2));
					intfMsgQBean.setChannelName(rs1.getString(3));
					intfMsgQBean.setQueueName(rs1.getString(4));
				}
				rs1.close();
			}
			rs.close();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			util.reset();
		}
		logger.logDebug("getOUTQueueDetails() End");
		return intfMsgQBean;
	}

	public java.sql.Timestamp formatDateTime(String input) {
		logger.logDebug("formatDateTime() Begin");
		java.sql.Timestamp result = null;
		SimpleDateFormat sdf = new SimpleDateFormat(DATETIME_FORMAT);
		try {
			result = new java.sql.Timestamp(sdf.parse(input).getTime());
		} catch (ParseException e) {
			e.printStackTrace();
		}
		logger.logDebug("formatDateTime() end");
		return result;

	}

	public java.sql.Date formatDate(String input) {
		logger.logDebug("formatDate() Begin");
		java.sql.Date result = null;
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
		try {
			result = new java.sql.Date(sdf.parse(input).getTime());

		} catch (ParseException e) {
			e.printStackTrace();
		}
		logger.logDebug("formatDate() End");
		return result;
	}

	public String getOutwardInventoryNumber() {
		logger.logDebug("getOutwardInventoryNumber() Begin");
		String inventoryNumber = null;
		DBUtil util = null;
		try {
			util = dbContext.createUtilInstance();
			util.reset();
			util.setMode(DBUtil.PREPARED);
			util.setSql("SELECT FN_NEXTVAL('OW_INV_NUM')");
			ResultSet rs = util.executeQuery();
			if (rs.next()) {
				inventoryNumber = rs.getString(1);
			}
			util.reset();
		} catch (Exception e) {
			logger.logError("getOutwardInventoryNumber() Error - " + e.getLocalizedMessage());
			inventoryNumber = null;
		} finally {
			util.reset();
		}
		logger.logDebug("getOutwardInventoryNumber() End");
		return inventoryNumber;
	}

	public String getInwardInventoryNumber() {
		logger.logDebug("getInwardInventoryNumber() Begin");
		String inventoryNumber = null;
		DBUtil util = null;
		try {
			util = dbContext.createUtilInstance();
			util.reset();
			util.setMode(DBUtil.PREPARED);
			util.setSql("SELECT FN_NEXTVAL('IW_INV_NUM')");
			ResultSet rs = util.executeQuery();
			if (rs.next()) {
				inventoryNumber = rs.getString(1);
			}
			util.reset();
		} catch (Exception e) {
			logger.logError("getOutwardInventoryNumber() Error - " + e.getLocalizedMessage());
			inventoryNumber = null;
		} finally {
			util.reset();
		}
		logger.logDebug("getInwardInventoryNumber() End");
		return inventoryNumber;
	}

	public boolean updateInwardLog(DTObject inputDTO) {
		logger.logDebug("updateInwardLog() Begin");
		boolean status = false;
		DBUtil util = null;
		String sql = "INSERT INTO INQLOG (ENTITY_CODE,INVENTORY_NO,RECEIVED_DATE,QUEUE_CODE,CONTENT,STATUS) VALUES (?,?,FN_GETCDT(?),?,?,?)";
		try {
			util = dbContext.createUtilInstance();
			util.reset();
			util.setMode(DBUtil.PREPARED);
			util.setSql(sql);
			util.setString(1, inputDTO.get("ENTITY_CODE"));
			util.setString(2, inputDTO.get("OW_INV_NO"));
			util.setString(3, inputDTO.get("ENTITY_CODE"));
			util.setString(4, inputDTO.get("QUEUE"));
			util.setString(5, inputDTO.get("MESSAGE"));
			util.setString(6, inputDTO.get("STATUS"));
			util.executeUpdate();
			status = true;
		} catch (Exception e) {
			logger.logError("updateInwardLog() Error - " + e.getLocalizedMessage());
		} finally {
			util.reset();
		}
		logger.logDebug("updateInwardLog() End");
		return status;
	}

	public void close() {
		if (dbContext != null)
			dbContext.close();
	}

	public void setDBContext() {
		dbContext = new DBContext();
	}
}
