/*
 * Copyright (c) 2014, 2015, Patterns Software Design Institute. All
 * rights reserved. Unauthorized copying of this file, via any medium is
 * strictly prohibited. PATTERNS SDI PROPRIETARY/CONFIDENTIAL. Use is
 * subject to license terms.
 */
package com.patterns.interfaces.beans;

/**
 * The Class InterfaceBean provides common base classes for external
 * interface POJOs
 */
public abstract class InterfaceBean {

}
