/*
 * Copyright (c) 2014, 2015, Patterns Software Design Institute. All
 * rights reserved. Unauthorized copying of this file, via any medium is
 * strictly prohibited. PATTERNS SDI PROPRIETARY/CONFIDENTIAL. Use is
 * subject to license terms.
 */
package com.patterns.interfaces.beans;

import java.util.ArrayList;
import java.util.Map;

/**
 * The Class EmailBean provides getters and setters for email related
 * information
 */
public class EmailBean extends InterfaceBean {

	/** The email interface code. */
	private String emailInterfaceCode;

	/** The email subject. */
	private String emailSubject;

	/** The email body. */
	private String emailBody;

	/** The email id. */
	private String emailID;

	/** The attachment list. */
	private Map<String, String> attachmentList;

	/**
	 * Gets the email interface code.
	 * 
	 * @return the email interface code
	 */
	public String getEmailInterfaceCode() {
		return emailInterfaceCode;
	}

	/**
	 * Sets the email interface code.
	 * 
	 * @param emailInterfaceCode
	 *            the new email interface code
	 */
	public void setEmailInterfaceCode(String emailInterfaceCode) {
		this.emailInterfaceCode = emailInterfaceCode;
	}

	/**
	 * Gets the email subject.
	 * 
	 * @return the email subject
	 */
	public String getEmailSubject() {
		return emailSubject;
	}

	/**
	 * Sets the email subject.
	 * 
	 * @param emailSubject
	 *            the new email subject
	 */
	public void setEmailSubject(String emailSubject) {
		this.emailSubject = emailSubject;
	}

	/**
	 * Gets the email body.
	 * 
	 * @return the email body
	 */
	public String getEmailBody() {
		return emailBody;
	}

	/**
	 * Sets the email body.
	 * 
	 * @param emailBody
	 *            the new email body
	 */
	public void setEmailBody(String emailBody) {
		this.emailBody = emailBody;
	}

	/**
	 * Gets the email id.
	 * 
	 * @return the email id
	 */
	public String getEmailID() {
		return emailID;
	}

	/**
	 * Sets the email id.
	 * 
	 * @param emailID
	 *            the new email id
	 */
	public void setEmailID(String emailID) {
		this.emailID = emailID;
	}

	/**
	 * Gets the attachment list.
	 * 
	 * @return the attachment list
	 */
	public Map<String, String> getAttachmentList() {
		return attachmentList;
	}

	/**
	 * Sets the attachment list.
	 * 
	 * @param attachmentList
	 *            the attachment list
	 */
	public void setAttachmentList(Map<String, String> attachmentList) {
		this.attachmentList = attachmentList;
	}
	
	
	
	
	/** The Email list. */
	private ArrayList<String> toEmailIDList;
	private ArrayList<String> ccEmailIDList;
	private ArrayList<String> bccEmailIDList;
	
	public ArrayList<String> getToEmailIDList() {
		return toEmailIDList;
	}

	public void setToEmailIDList(ArrayList<String> toEmailIDList) {
		this.toEmailIDList = toEmailIDList;
	}

	public ArrayList<String> getCcEmailIDList() {
		return ccEmailIDList;
	}

	public void setCcEmailIDList(ArrayList<String> ccEmailIDList) {
		this.ccEmailIDList = ccEmailIDList;
	}

	public ArrayList<String> getBccEmailIDList() {
		return bccEmailIDList;
	}

	public void setBccEmailIDList(ArrayList<String> bccEmailIDList) {
		this.bccEmailIDList = bccEmailIDList;
	}
	
}
