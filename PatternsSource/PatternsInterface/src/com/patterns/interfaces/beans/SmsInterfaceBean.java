/*
 * Copyright (c) 2014, 2015, Patterns Software Design Institute. All
 * rights reserved. Unauthorized copying of this file, via any medium is
 * strictly prohibited. PATTERNS SDI PROPRIETARY/CONFIDENTIAL. Use is
 * subject to license terms.
 */
package com.patterns.interfaces.beans;

/**
 * The Class SmsInterfaceBean provides getters and setters for SMS
 * related information
 */
public class SmsInterfaceBean extends InterfaceBean {

	/** The sms interface code. */
	private String smsInterfaceCode;

	/** The text message. */
	private String textMessage;

	/** The mobile number. */
	private String mobileNumber;

	/**
	 * Gets the sms interface code.
	 * 
	 * @return the sms interface code
	 */
	public String getSmsInterfaceCode() {
		return smsInterfaceCode;
	}

	/**
	 * Sets the sms interface code.
	 * 
	 * @param smsInterfaceCode
	 *            the new sms interface code
	 */
	public void setSmsInterfaceCode(String smsInterfaceCode) {
		this.smsInterfaceCode = smsInterfaceCode;
	}

	/**
	 * Gets the text message.
	 * 
	 * @return the text message
	 */
	public String getTextMessage() {
		return textMessage;
	}

	/**
	 * Sets the text message.
	 * 
	 * @param textMessage
	 *            the new text message
	 */
	public void setTextMessage(String textMessage) {
		this.textMessage = textMessage;
	}

	/**
	 * Gets the mobile number.
	 * 
	 * @return the mobile number
	 */
	public String getMobileNumber() {
		return mobileNumber;
	}

	/**
	 * Sets the mobile number.
	 * 
	 * @param mobileNumber
	 *            the new mobile number
	 */
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
}
