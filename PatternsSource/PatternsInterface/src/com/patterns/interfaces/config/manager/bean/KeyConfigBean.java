package com.patterns.interfaces.config.manager.bean;

public class KeyConfigBean {

	private String valueDataType;
	private String dateFormat;
	private boolean mandatory;
	private String tableName;
	private String columnName;
	private String valueRegex;
	private int index;
	private String lovrecId;
	private int trimLength;
	private String defaultValue;
	private String handlerName;

	public String getValueDataType() {
		return valueDataType;
	}

	public void setValueDataType(String valueDataType) {
		this.valueDataType = valueDataType;
	}

	public String getDateFormat() {
		return dateFormat;
	}

	public void setDateFormat(String dateFormat) {
		this.dateFormat = dateFormat;
	}

	public boolean isMandatory() {
		return mandatory;
	}

	public void setMandatory(boolean mandatory) {
		this.mandatory = mandatory;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getColumnName() {
		return columnName;
	}

	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	public String getValueRegex() {
		return valueRegex;
	}

	public void setValueRegex(String valueRegex) {
		this.valueRegex = valueRegex;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public String getLovrecId() {
		return lovrecId;
	}

	public void setLovrecId(String lovrecId) {
		this.lovrecId = lovrecId;
	}

	public int getTrimLength() {
		return trimLength;
	}

	public void setTrimLength(int trimLength) {
		this.trimLength = trimLength;
	}

	public String getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	public String getHandlerName() {
		return handlerName;
	}

	public void setHandlerName(String handlerName) {
		this.handlerName = handlerName;
	}
}
