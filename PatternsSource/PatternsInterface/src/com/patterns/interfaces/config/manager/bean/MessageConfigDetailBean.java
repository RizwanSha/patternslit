package com.patterns.interfaces.config.manager.bean;

import java.util.List;
import java.util.Map;

public class MessageConfigDetailBean {

	private int serial;
	private String fileName;
	private String fileType;
	private String tableType;
	private String recordOperation;
	private boolean validationReqd;
	private boolean preprocessReqd;
	private Map<Integer, List<KeyConfigBean>> dependentKeyConfigMap;
	private Map<Integer, List<PreProcessorKeyConfigBean>> preProcessorKeyConfigMap;

	public int getSerial() {
		return serial;
	}

	public void setSerial(int serial) {
		this.serial = serial;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public String getTableType() {
		return tableType;
	}

	public void setTableType(String tableType) {
		this.tableType = tableType;
	}

	public String getRecordOperation() {
		return recordOperation;
	}

	public void setRecordOperation(String recordOperation) {
		this.recordOperation = recordOperation;
	}

	public Map<Integer, List<KeyConfigBean>> getDependentKeyConfigMap() {
		return dependentKeyConfigMap;
	}

	public void setDependentKeyConfigMap(Map<Integer, List<KeyConfigBean>> dependentKeyConfigMap) {
		this.dependentKeyConfigMap = dependentKeyConfigMap;
	}

	public boolean isValidationReqd() {
		return validationReqd;
	}

	public void setValidationReqd(boolean validationReqd) {
		this.validationReqd = validationReqd;
	}

	public boolean isPreprocessReqd() {
		return preprocessReqd;
	}

	public void setPreprocessReqd(boolean preprocessReqd) {
		this.preprocessReqd = preprocessReqd;
	}

	public Map<Integer, List<PreProcessorKeyConfigBean>> getPreProcessorKeyConfigMap() {
		return preProcessorKeyConfigMap;
	}

	public void setPreProcessorKeyConfigMap(Map<Integer, List<PreProcessorKeyConfigBean>> preProcessorKeyConfigMap) {
		this.preProcessorKeyConfigMap = preProcessorKeyConfigMap;
	}
}
