package com.patterns.interfaces.config.manager;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.web.FormatUtils;

import com.patterns.interfaces.config.manager.bean.KeyConfigBean;
import com.patterns.interfaces.config.manager.bean.MessageConfigBean;
import com.patterns.interfaces.config.manager.bean.MessageConfigDetailBean;
import com.patterns.interfaces.config.manager.bean.PreProcessorKeyConfigBean;
import com.patterns.interfaces.config.manager.bean.PreProcessorRecordConfigBean;

public class KeyConfigManager {
	private static final ReentrantReadWriteLock readWriteLockGlobal = new ReentrantReadWriteLock();

	private static final Lock writeGlobal = readWriteLockGlobal.writeLock();
	private static final Lock readGlobal = readWriteLockGlobal.readLock();

	private Map<String, MessageConfigBean> internalMap;
	private Map<String, MessageConfigBean> internalLocalMap;
	private DBContext dbContext = null;
	private DBUtil dbUtil = null;
	private DBUtil innerDBUtil = null;
	private static KeyConfigManager instance = null;
	private boolean readyToRead = false;

	private void initConfiguration() {
		internalMap.putAll(internalLocalMap);
		internalLocalMap.clear();
	}

	private void resetConfiguration() {
		if (internalMap == null)
			internalMap = new HashMap<String, MessageConfigBean>();
		internalMap.clear();
	}

	public static void unloadConfiguration() {
		try {
			writeGlobal.lock();
			if (instance != null) {
				instance.resetConfiguration();
				instance = null;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			writeGlobal.unlock();
		}
	}

	public static void reloadConfiguration() {
		try {
			writeGlobal.lock();
			if (instance == null) {
				instance = new KeyConfigManager();
				instance.readyToRead = false;
				instance.loadConfiguration();
				instance.resetConfiguration();
				instance.initConfiguration();
				instance.readyToRead = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			writeGlobal.unlock();
		}
	}

	public static KeyConfigManager getInstance() {
		if (instance == null) {
			reloadConfiguration();
		} else {
			if (!instance.readyToRead) {
				try {
					readGlobal.lock();
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					readGlobal.unlock();
				}
			}
		}
		return instance;
	}

	public MessageConfigDetailBean getMessageConfiguration(String entity, String processingCode, String fileName, String fileType) {
		MessageConfigBean messageConfigBean = null;
		String keyMap = "";
		keyMap = entity + "|" + processingCode;
		messageConfigBean = internalMap.get(keyMap);
		if (messageConfigBean == null) {
			return null;
		}

		List<MessageConfigDetailBean> configurationList = messageConfigBean.getConfigurationDetails();
		for (MessageConfigDetailBean bean : configurationList) {
			if (bean.getFileName().equals(fileName) && bean.getFileType().equals(fileType)) {
				return bean;
			}
		}
		return null;
	}

	public List<KeyConfigBean> getTableConfiguration(String entity, String processingCode, String fileName, String fileType, String recordOperation) {
		List<KeyConfigBean> tableConfigurationMap = null;
		String keyMap = "";
		keyMap = entity + "|" + processingCode;
		MessageConfigBean messageConfigBean = internalMap.get(keyMap);
		if (messageConfigBean == null) {
			return null;
		}
		List<MessageConfigDetailBean> configurationList = messageConfigBean.getConfigurationDetails();
		for (MessageConfigDetailBean bean : configurationList) {
			if (bean.getFileName().equals(fileName) && bean.getFileType().equals(fileType) && bean.getRecordOperation().equals(recordOperation)) {
				tableConfigurationMap = bean.getDependentKeyConfigMap().get(bean.getSerial());
				break;
			}
		}
		return tableConfigurationMap;
	}

	public List<PreProcessorKeyConfigBean> getPreProcessorTableConfiguration(String entity, String processingCode, String fileName, String fileType) {
		List<PreProcessorKeyConfigBean> tableConfigurationMap = null;
		String keyMap = "";
		keyMap = entity + "|" + processingCode;
		MessageConfigBean messageConfigBean = internalMap.get(keyMap);
		if (messageConfigBean == null) {
			return null;
		}
		List<MessageConfigDetailBean> configurationList = messageConfigBean.getConfigurationDetails();
		for (MessageConfigDetailBean bean : configurationList) {
			if (bean.isPreprocessReqd()) {
				if (bean.getFileName().equals(fileName) && bean.getFileType().equals(fileType)) {
					tableConfigurationMap = bean.getPreProcessorKeyConfigMap().get(bean.getSerial());
					break;
				}
			}
		}
		return tableConfigurationMap;
	}

	public void loadConfiguration() {
		String sql = RegularConstants.EMPTY_STRING;
		String mapKey = RegularConstants.EMPTY_STRING;
		String entity = RegularConstants.EMPTY_STRING;
		if (internalLocalMap == null)
			internalLocalMap = new HashMap<String, MessageConfigBean>();
		internalLocalMap.clear();
		try {
			dbContext = new DBContext();
			dbContext.openConnection();
			dbUtil = dbContext.createUtilInstance();
			innerDBUtil = dbContext.createUtilInstance();
			dbUtil.reset();
			sql = "SELECT PARTITION_NO FROM INSTALL";
			dbUtil.setSql(sql);
			ResultSet rset = dbUtil.executeQuery();
			while (rset.next()) {
				entity = rset.getString(1);
			}

			MessageConfigBean messageConfigBean;
			sql = "SELECT DISTINCT PROC_CODE,PGM_IDENTFR FROM CTJMSGCONFIG WHERE ENTITY_CODE=?";
			dbUtil.setSql(sql);
			dbUtil.setString(1, entity);
			rset = dbUtil.executeQuery();
			while (rset.next()) {
				messageConfigBean = new MessageConfigBean();
				String processingCode = rset.getString("PROC_CODE");
				mapKey = entity + "|" + processingCode;
				Map<Integer, List<KeyConfigBean>> msgDetails = new HashMap<Integer, List<KeyConfigBean>>();
				Map<Integer, List<PreProcessorKeyConfigBean>> preProcessorMsgDetails = new HashMap<Integer, List<PreProcessorKeyConfigBean>>();
				messageConfigBean.setProcessingCode(processingCode);

				sql = "SELECT SL,FILE_NAME,IFNULL(FILE_TYPE,'') FILE_TYPE,TABLE_TYPE,RECORD_OPR,PREPROCESS_REQ,VALIDATE_REQ FROM CTJMSGCONFIGDTL WHERE ENTITY_CODE=? AND PROC_CODE=? ORDER BY SL";
				innerDBUtil.setSql(sql);
				innerDBUtil.setString(1, entity);
				innerDBUtil.setString(2, processingCode);
				ResultSet readMsgCfgRSet = innerDBUtil.executeQuery();
				List<MessageConfigDetailBean> configurationBeanList = new LinkedList<MessageConfigDetailBean>();
				while (readMsgCfgRSet.next()) {
					int serial = readMsgCfgRSet.getInt("SL");
					MessageConfigDetailBean messageConfigKeysBean = new MessageConfigDetailBean();
					messageConfigKeysBean.setSerial(serial);
					messageConfigKeysBean.setFileName(readMsgCfgRSet.getString("FILE_NAME"));
					messageConfigKeysBean.setFileType(readMsgCfgRSet.getString("FILE_TYPE"));
					messageConfigKeysBean.setTableType(readMsgCfgRSet.getString("TABLE_TYPE"));
					messageConfigKeysBean.setRecordOperation(readMsgCfgRSet.getString("RECORD_OPR"));
					messageConfigKeysBean.setValidationReqd(FormatUtils.decodeStringToBoolean(readMsgCfgRSet.getString("VALIDATE_REQ")));
					messageConfigKeysBean.setPreprocessReqd(FormatUtils.decodeStringToBoolean(readMsgCfgRSet.getString("PREPROCESS_REQ")));
					msgDetails.put(serial, getKeyConfigBeanList(entity, processingCode, serial));
					messageConfigKeysBean.setDependentKeyConfigMap(msgDetails);

					if (messageConfigKeysBean.isPreprocessReqd()) {
						preProcessorMsgDetails.put(serial, getPreprocessorKeyConfigBeanList(entity, processingCode, messageConfigKeysBean.getFileName(), messageConfigKeysBean.getFileType()));
						messageConfigKeysBean.setPreProcessorKeyConfigMap(preProcessorMsgDetails);
					}

					configurationBeanList.add(messageConfigKeysBean);
				}
				messageConfigBean.setConfigurationDetails(configurationBeanList);
				internalLocalMap.put(mapKey, messageConfigBean);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			dbUtil.reset();
			innerDBUtil.reset();
			if (dbContext != null) {
				dbContext.close();
			}
		}
	}

	private List<KeyConfigBean> getKeyConfigBeanList(String entity, String processingCode, int serial) {
		KeyConfigBean keyConfigBean = new KeyConfigBean();
		List<KeyConfigBean> beanList = new LinkedList<KeyConfigBean>();
		DBUtil dbUtil = dbContext.createUtilInstance();
		try {
			dbUtil.reset();
			String sql = "SELECT MSG_INDEX,IS_MAND,TABLE_NAME,COLUMN_NAME,DATA_TYPE,DATE_FORMAT,VALUE_REG_EXPR,LOVREC_KEY,VALUE_TRIM,DEFAULT_VALUE,REG_EXPR_HANDLER FROM CTJRECORDCFGDTL WHERE ENTITY_CODE=? AND PROC_CODE=? AND SL=?";
			dbUtil.setSql(sql);
			dbUtil.setString(1, entity);
			dbUtil.setString(2, processingCode);
			dbUtil.setInt(3, serial);
			ResultSet rset = dbUtil.executeQuery();
			while (rset.next()) {
				keyConfigBean = new KeyConfigBean();
				keyConfigBean.setIndex(rset.getInt("MSG_INDEX"));
				keyConfigBean.setMandatory(FormatUtils.decodeStringToBoolean(rset.getString("IS_MAND")));
				keyConfigBean.setTableName(rset.getString("TABLE_NAME"));
				keyConfigBean.setColumnName(rset.getString("COLUMN_NAME"));
				keyConfigBean.setValueDataType(rset.getString("DATA_TYPE"));
				keyConfigBean.setValueRegex(rset.getString("VALUE_REG_EXPR"));
				keyConfigBean.setDateFormat(rset.getString("DATE_FORMAT"));
				keyConfigBean.setLovrecId(rset.getString("LOVREC_KEY"));
				keyConfigBean.setTrimLength(rset.getInt("VALUE_TRIM"));
				keyConfigBean.setDefaultValue(rset.getString("DEFAULT_VALUE"));
				keyConfigBean.setHandlerName(rset.getString("REG_EXPR_HANDLER"));
				
				beanList.add(keyConfigBean);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			dbUtil.reset();
		}
		return beanList;
	}

	private List<PreProcessorKeyConfigBean> getPreprocessorKeyConfigBeanList(String entity, String processingCode, String fileName, String fileType) {
		PreProcessorKeyConfigBean keyConfigBean = null;
		List<PreProcessorKeyConfigBean> beanList = new LinkedList<PreProcessorKeyConfigBean>();
		DBUtil dbUtil = dbContext.createUtilInstance();
		try {
			dbUtil.reset();
			String sql = "SELECT SL,HADDLER_NAME,TABLE_NAME,COLUMN_NAME FROM CTJPREPROCESSCFG WHERE ENTITY_CODE=? AND PROC_CODE=? AND FILE_NAME=? AND FILE_TYPE=?";
			dbUtil.setSql(sql);
			dbUtil.setString(1, entity);
			dbUtil.setString(2, processingCode);
			dbUtil.setString(3, fileName);
			dbUtil.setString(4, fileType);
			ResultSet rset = dbUtil.executeQuery();
			while (rset.next()) {
				keyConfigBean = new PreProcessorKeyConfigBean();
				keyConfigBean.setHandlerName(rset.getString("HADDLER_NAME"));
				keyConfigBean.setTableName(rset.getString("TABLE_NAME"));
				keyConfigBean.setColumnName(rset.getString("COLUMN_NAME"));
				keyConfigBean.setConfigurationDetailsMap(getPreProcessorRecordConfigBeanList(entity, processingCode, fileName, fileType, rset.getInt("SL")));
				beanList.add(keyConfigBean);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			dbUtil.reset();
		}
		return beanList;
	}

	private List<PreProcessorRecordConfigBean> getPreProcessorRecordConfigBeanList(String entity, String processingCode, String fileName, String fileType, int serial) {
		PreProcessorRecordConfigBean keyConfigBean = null;
		List<PreProcessorRecordConfigBean> beanList = new LinkedList<PreProcessorRecordConfigBean>();
		DBUtil dbUtil = dbContext.createUtilInstance();
		try {
			dbUtil.reset();
			String sql = "SELECT MSG_INDEX,IS_MAND,DATA_TYPE,DATE_FORMAT,TABLE_NAME,COLUMN_NAME,LOVREC_KEY,VALUE_TRIM,DEFAULT_VALUE FROM CTJPREPROCESSCFGDTL WHERE ENTITY_CODE=? AND PROC_CODE=?  AND FILE_NAME=? AND FILE_TYPE=? AND SL=?";
			dbUtil.setSql(sql);
			dbUtil.setString(1, entity);
			dbUtil.setString(2, processingCode);
			dbUtil.setString(3, fileName);
			dbUtil.setString(4, fileType);
			dbUtil.setInt(5, serial);
			ResultSet rset = dbUtil.executeQuery();
			while (rset.next()) {
				keyConfigBean = new PreProcessorRecordConfigBean();
				keyConfigBean.setIndex(rset.getInt("MSG_INDEX"));
				keyConfigBean.setMandatory(FormatUtils.decodeStringToBoolean(rset.getString("IS_MAND")));
				keyConfigBean.setValueDataType(rset.getString("DATA_TYPE"));
				keyConfigBean.setDateFormat(rset.getString("DATE_FORMAT"));
				keyConfigBean.setTableName(rset.getString("TABLE_NAME"));
				keyConfigBean.setColumnName(rset.getString("COLUMN_NAME"));
				keyConfigBean.setLovrecId(rset.getString("LOVREC_KEY"));
				keyConfigBean.setTrimLength(rset.getInt("VALUE_TRIM"));
				keyConfigBean.setDefaultValue(rset.getString("DEFAULT_VALUE"));
				beanList.add(keyConfigBean);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			dbUtil.reset();
		}
		return beanList;
	}
}
