package com.patterns.interfaces.config.manager.bean;

import java.util.Map;

public class TableConfiguration {

	public static final int TABLE_TYPE_SINGLE = 1;
	public static final int TABLE_TYPE_MULTIPLE = 2;

	private int tableType;
	private String messageKeyCode;

	public String getMessageKeyCode() {
		return messageKeyCode;
	}

	public void setMessageKeyCode(String messageKeyCode) {
		this.messageKeyCode = messageKeyCode;
	}

	private boolean messageKeyCodeRequired;

	public boolean isMessageKeyCodeRequired() {
		return messageKeyCodeRequired;
	}

	public void setMessageKeyCodeRequired(boolean messageKeyCodeRequired) {
		this.messageKeyCodeRequired = messageKeyCodeRequired;
	}

	public int getTableType() {
		return tableType;
	}

	public void setTableType(int tableType) {
		this.tableType = tableType;
	}

	private Map<String, String> columnMap;

	public Map<String, String> getColumnMap() {
		return columnMap;
	}

	public void setColumnMap(Map<String, String> columnMap) {
		this.columnMap = columnMap;
	}

	private Map<String, String> columnKeyCodeMap;

	public Map<String, String> getColumnKeyCodeMap() {
		return columnKeyCodeMap;
	}

	public void setColumnKeyCodeMap(Map<String, String> columnKeyCodeMap) {
		this.columnKeyCodeMap = columnKeyCodeMap;
	}

}
