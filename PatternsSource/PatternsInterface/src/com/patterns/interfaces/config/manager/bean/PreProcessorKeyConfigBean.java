package com.patterns.interfaces.config.manager.bean;

import java.util.List;

public class PreProcessorKeyConfigBean {

	private String handlerName;
	private String tableName;
	private String columnName;
	private List<PreProcessorRecordConfigBean> configurationDetailsMap;

	public String getHandlerName() {
		return handlerName;
	}

	public void setHandlerName(String handlerName) {
		this.handlerName = handlerName;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getColumnName() {
		return columnName;
	}

	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	public List<PreProcessorRecordConfigBean> getConfigurationDetailsMap() {
		return configurationDetailsMap;
	}

	public void setConfigurationDetailsMap(List<PreProcessorRecordConfigBean> configurationDetailsMap) {
		this.configurationDetailsMap = configurationDetailsMap;
	}
}
