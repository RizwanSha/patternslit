package com.patterns.interfaces.config.manager.bean;

import java.util.List;

public class MessageConfigBean {

	private String processingCode;
	private String messageType;
	private String programId;

	private List<MessageConfigDetailBean> configurationDetails;

	public String getProcessingCode() {
		return processingCode;
	}

	public void setProcessingCode(String processingCode) {
		this.processingCode = processingCode;
	}

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public String getProgramId() {
		return programId;
	}

	public void setProgramId(String programId) {
		this.programId = programId;
	}

	public List<MessageConfigDetailBean> getConfigurationDetails() {
		return configurationDetails;
	}

	public void setConfigurationDetails(List<MessageConfigDetailBean> configurationDetails) {
		this.configurationDetails = configurationDetails;
	}
}
