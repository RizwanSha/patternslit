package com.patterns.interfaces.config.manager.bean;

public class PreProcessorRecordConfigBean {

	private int index;
	private boolean mandatory;
	private String valueDataType;
	private String dateFormat;
	private String tableName;
	private String columnName;
	private String lovrecId;
	private int trimLength;
	private String defaultValue;

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public boolean isMandatory() {
		return mandatory;
	}

	public void setMandatory(boolean mandatory) {
		this.mandatory = mandatory;
	}

	public String getValueDataType() {
		return valueDataType;
	}

	public void setValueDataType(String valueDataType) {
		this.valueDataType = valueDataType;
	}

	public String getDateFormat() {
		return dateFormat;
	}

	public void setDateFormat(String dateFormat) {
		this.dateFormat = dateFormat;
	}

	public String getTableName() {
		return tableName;
	}

	public String getColumnName() {
		return columnName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	public String getLovrecId() {
		return lovrecId;
	}

	public void setLovrecId(String lovrecId) {
		this.lovrecId = lovrecId;
	}

	public int getTrimLength() {
		return trimLength;
	}

	public void setTrimLength(int trimLength) {
		this.trimLength = trimLength;
	}

	public String getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}
}
