/*
 * Copyright (c) 2014, 2015, Patterns Software Design Institute. All
 * rights reserved. Unauthorized copying of this file, via any medium is
 * strictly prohibited. PATTERNS SDI PROPRIETARY/CONFIDENTIAL. Use is
 * subject to license terms.
 */
package com.patterns.interfaces.handler;

import patterns.config.framework.database.utils.DBContext;

import com.patterns.interfaces.beans.InterfaceBean;

/**
 * The Class AbstractHandler provides base functions to implement
 * handlers
 */
public abstract class AbstractHandler {

	/** The entity code. */
	protected String entityCode;

	/** The pojo instance. */
	protected InterfaceBean pojoInstance;

	/**
	 * Instantiates a new abstract handler.
	 */
	public AbstractHandler() {
	}

	/**
	 * Gets the entity code.
	 * 
	 * @return the entity code
	 */
	public String getEntityCode() {
		return entityCode;
	}

	/**
	 * Sets the entity code.
	 * 
	 * @param entityCode
	 *            the new entity code
	 */
	public void setEntityCode(String entityCode) {
		this.entityCode = entityCode;
	}

	/**
	 * Gets the pojo instance.
	 * 
	 * @return the pojo instance
	 */
	public InterfaceBean getPojoInstance() {
		return pojoInstance;
	}

	/**
	 * Sets the pojo instance.
	 * 
	 * @param pojoInstance
	 *            the new pojo instance
	 */
	public void setPojoInstance(InterfaceBean pojoInstance) {
		this.pojoInstance = pojoInstance;
	}

	/**
	 * Process.
	 */
	public abstract void process(DBContext dbContext);
}
