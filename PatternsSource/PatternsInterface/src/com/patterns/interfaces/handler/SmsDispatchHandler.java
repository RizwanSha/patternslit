/*
 * Copyright (c) 2014, 2015, Patterns Software Design Institute. All
 * rights reserved. Unauthorized copying of this file, via any medium is
 * strictly prohibited. PATTERNS SDI PROPRIETARY/CONFIDENTIAL. Use is
 * subject to license terms.
 */
package com.patterns.interfaces.handler;

import java.sql.ResultSet;

import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.loggers.ApplicationLogger;

import com.patterns.interfaces.beans.SmsInterfaceBean;

/**
 * The Class SmsDispatchHandler provides utility functions to dispatch SMS
 */
public class SmsDispatchHandler extends AbstractHandler {

	/** The logger. */
	private final ApplicationLogger logger;

	/**
	 * Instantiates a new sms dispatch handler.
	 */
	public SmsDispatchHandler() {
		logger = ApplicationLogger.getInstance(this.getClass().getSimpleName());
		logger.logDebug("()");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.patterns.interfaces.handler.AbstractHandler#process()
	 */
	@Override
	public void process(DBContext dbContext) {
		logger.logDebug("process() Begin");
		//DBContext dbContext = new DBContext();
		try {
			//dbContext.setAutoCommit(false);

			SmsInterfaceBean smsBean = (SmsInterfaceBean) getPojoInstance();
			String inventoryNumber = RegularConstants.EMPTY_STRING;
			/* GENERATE INVENTORY NUMBER */
			String selectEmailSequenceSql = "SELECT FN_NEXTVAL('SEQ_SMS') FROM DUAL";
			DBUtil selectEmailSequenceUtil = dbContext.createUtilInstance();
			selectEmailSequenceUtil.reset();
			selectEmailSequenceUtil.setMode(DBUtil.PREPARED);
			selectEmailSequenceUtil.setSql(selectEmailSequenceSql);
			ResultSet emailSequenceRSet = selectEmailSequenceUtil
					.executeQuery();
			if (emailSequenceRSet.next()) {
				inventoryNumber = emailSequenceRSet.getString(1);
			}
			selectEmailSequenceUtil.reset();

			/* UPDATE SMSOUTQLOG */
			String insertSmsOutqLogSql = "INSERT INTO SMSOUTQLOG (ENTITY_CODE,INVENTORY_NO,MOBILE_NUMBER,MESSAGE_CONTENT,CREATE_TIME,STATUS,INTERFACE_CODE) VALUES (?,?,?,?,?,'I',?)";
			DBUtil insertSmsOutqLogUtil = dbContext.createUtilInstance();
			insertSmsOutqLogUtil.reset();
			insertSmsOutqLogUtil.setMode(DBUtil.PREPARED);
			insertSmsOutqLogUtil.setSql(insertSmsOutqLogSql);
			insertSmsOutqLogUtil.setString(1, getEntityCode());
			insertSmsOutqLogUtil.setString(2, inventoryNumber);
			insertSmsOutqLogUtil.setString(3, smsBean.getMobileNumber());
			insertSmsOutqLogUtil.setString(4, smsBean.getTextMessage());
			insertSmsOutqLogUtil.setString(5, new java.sql.Timestamp(new java.util.Date().getTime()).toString());
			insertSmsOutqLogUtil.setString(6, smsBean.getSmsInterfaceCode());

			insertSmsOutqLogUtil.executeUpdate();
			insertSmsOutqLogUtil.reset();

			/* UPDATE SMSOUTQPE */
			String insertSmsOutqPESql = "INSERT INTO SMSOUTQPE (ENTITY_CODE,INVENTORY_NO) VALUES (?,?)";
			DBUtil insertSmsOutqPEUtil = dbContext.createUtilInstance();
			insertSmsOutqPEUtil.reset();
			insertSmsOutqPEUtil.setMode(DBUtil.PREPARED);
			insertSmsOutqPEUtil.setSql(insertSmsOutqPESql);
			insertSmsOutqPEUtil.setString(1, getEntityCode());
			insertSmsOutqPEUtil.setString(2, inventoryNumber);
			insertSmsOutqPEUtil.executeUpdate();
			insertSmsOutqPEUtil.reset();

			//dbContext.commit();
			logger.logDebug("process() End");
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError(e.getLocalizedMessage());
			try {
				//dbContext.rollback();
			} catch (Exception re) {
				re.printStackTrace();
				logger.logError(re.getLocalizedMessage());
			}
		} finally {
			//dbContext.close();
		}
	}
	public void process() {
		logger.logDebug("process() Begin");
		DBContext dbContext = new DBContext();
		try {
			dbContext.setAutoCommit(false);

			SmsInterfaceBean smsBean = (SmsInterfaceBean) getPojoInstance();
			String inventoryNumber = RegularConstants.EMPTY_STRING;
			/* GENERATE INVENTORY NUMBER */
			String selectEmailSequenceSql = "SELECT FN_NEXTVAL('SEQ_SMS') FROM DUAL";
			DBUtil selectEmailSequenceUtil = dbContext.createUtilInstance();
			selectEmailSequenceUtil.reset();
			selectEmailSequenceUtil.setMode(DBUtil.PREPARED);
			selectEmailSequenceUtil.setSql(selectEmailSequenceSql);
			ResultSet emailSequenceRSet = selectEmailSequenceUtil
					.executeQuery();
			if (emailSequenceRSet.next()) {
				inventoryNumber = emailSequenceRSet.getString(1);
			}
			selectEmailSequenceUtil.reset();

			/* UPDATE SMSOUTQLOG */
			String insertSmsOutqLogSql = "INSERT INTO SMSOUTQLOG (ENTITY_CODE,INVENTORY_NO,MOBILE_NUMBER,MESSAGE_CONTENT,CREATE_TIME,STATUS,INTERFACE_CODE) VALUES (?,?,?,?,?,'I',?)";
			DBUtil insertSmsOutqLogUtil = dbContext.createUtilInstance();
			insertSmsOutqLogUtil.reset();
			insertSmsOutqLogUtil.setMode(DBUtil.PREPARED);
			insertSmsOutqLogUtil.setSql(insertSmsOutqLogSql);
			insertSmsOutqLogUtil.setString(1, getEntityCode());
			insertSmsOutqLogUtil.setString(2, inventoryNumber);
			insertSmsOutqLogUtil.setString(3, smsBean.getMobileNumber());
			insertSmsOutqLogUtil.setString(4, smsBean.getTextMessage());
			insertSmsOutqLogUtil.setString(5, new java.sql.Timestamp(new java.util.Date().getTime()).toString());
			insertSmsOutqLogUtil.setString(6, smsBean.getSmsInterfaceCode());

			insertSmsOutqLogUtil.executeUpdate();
			insertSmsOutqLogUtil.reset();

			/* UPDATE SMSOUTQPE */
			String insertSmsOutqPESql = "INSERT INTO SMSOUTQPE (ENTITY_CODE,INVENTORY_NO) VALUES (?,?)";
			DBUtil insertSmsOutqPEUtil = dbContext.createUtilInstance();
			insertSmsOutqPEUtil.reset();
			insertSmsOutqPEUtil.setMode(DBUtil.PREPARED);
			insertSmsOutqPEUtil.setSql(insertSmsOutqPESql);
			insertSmsOutqPEUtil.setString(1, getEntityCode());
			insertSmsOutqPEUtil.setString(2, inventoryNumber);
			insertSmsOutqPEUtil.executeUpdate();
			insertSmsOutqPEUtil.reset();

			dbContext.commit();
			logger.logDebug("process() End");
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError(e.getLocalizedMessage());
			try {
				dbContext.rollback();
			} catch (Exception re) {
				re.printStackTrace();
				logger.logError(re.getLocalizedMessage());
			}
		} finally {
			dbContext.close();
		}
	}
}
