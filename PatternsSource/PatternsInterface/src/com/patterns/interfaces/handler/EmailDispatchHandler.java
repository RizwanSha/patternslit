/*
 * Copyright (c) 2014, 2015, Patterns Software Design Institute. All
 * rights reserved. Unauthorized copying of this file, via any medium is
 * strictly prohibited. PATTERNS SDI PROPRIETARY/CONFIDENTIAL. Use is
 * subject to license terms.
 */
package com.patterns.interfaces.handler;

import java.sql.ResultSet;

import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.loggers.ApplicationLogger;

import com.patterns.interfaces.beans.EmailBean;

/**
 * The Class EmailDispatchHandler provides utility functions to dispatch Email
 */
public class EmailDispatchHandler extends AbstractHandler {

	/** The logger. */
	private final ApplicationLogger logger;

	/**
	 * Instantiates a new email dispatch handler.
	 */
	public EmailDispatchHandler() {
		logger = ApplicationLogger.getInstance(this.getClass().getSimpleName());
		logger.logDebug("()");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.patterns.interfaces.handler.AbstractHandler#process()
	 */
	@Override
	public void process(DBContext dbContext) {
		logger.logDebug("process() Begin");
		// DBContext dbContext = new DBContext();
		try {
			// .setAutoCommit(false);
			EmailBean emailBean = (EmailBean) getPojoInstance();
			String inventoryNumber = RegularConstants.EMPTY_STRING;
			/* GENERATE INVENTORY NUMBER */
			String selectEmailSequenceSql = "SELECT FN_NEXTVAL('SEQ_EMAIL') FROM DUAL";
			DBUtil selectEmailSequenceUtil = dbContext.createUtilInstance();
			selectEmailSequenceUtil.reset();
			selectEmailSequenceUtil.setMode(DBUtil.PREPARED);
			selectEmailSequenceUtil.setSql(selectEmailSequenceSql);
			ResultSet emailSequenceRSet = selectEmailSequenceUtil.executeQuery();
			if (emailSequenceRSet.next()) {
				inventoryNumber = emailSequenceRSet.getString(1);
			}
			selectEmailSequenceUtil.reset();

			/* UPDATE EMAILOUTQLOG */
			String insertEmailOutqLogSql = "INSERT INTO EMAILOUTQLOG (ENTITY_CODE,INVENTORY_NO,APPL_CODE,REQ_DATE,EMAIL_SUBJECT,EMAIL_TEXT,STATUS) VALUES (?,?,?,?,?,?,'I')";
			DBUtil insertEmailOutqLogUtil = dbContext.createUtilInstance();
			insertEmailOutqLogUtil.reset();
			insertEmailOutqLogUtil.setMode(DBUtil.PREPARED);
			insertEmailOutqLogUtil.setSql(insertEmailOutqLogSql);
			insertEmailOutqLogUtil.setString(1, getEntityCode());
			insertEmailOutqLogUtil.setString(2, inventoryNumber);
			insertEmailOutqLogUtil.setString(3, emailBean.getEmailInterfaceCode());
			insertEmailOutqLogUtil.setString(4, new java.sql.Timestamp(new java.util.Date().getTime()).toString());
			insertEmailOutqLogUtil.setString(5, emailBean.getEmailSubject());
			insertEmailOutqLogUtil.setString(6, emailBean.getEmailBody());
			insertEmailOutqLogUtil.executeUpdate();
			insertEmailOutqLogUtil.reset();

			/* UPDATE EMAILOUTQRCP */
			String insertEmailRcpSql = "INSERT INTO EMAILOUTQRCP (ENTITY_CODE,INVENTORY_NO,EMAIL_RCP_TYPE,EMAIL_DTL_SL,EMAIL_ID) VALUES (?,?,?,?,?)";

			DBUtil insertEmailRcpUtil = dbContext.createUtilInstance();
			insertEmailRcpUtil.reset();
			insertEmailRcpUtil.setMode(DBUtil.PREPARED);
			insertEmailRcpUtil.setSql(insertEmailRcpSql);
			insertEmailRcpUtil.setString(1, getEntityCode());
			insertEmailRcpUtil.setString(2, inventoryNumber);
			insertEmailRcpUtil.setString(3, "T");
			insertEmailRcpUtil.setInt(4, 1);
			insertEmailRcpUtil.setString(5, emailBean.getEmailID());
			insertEmailRcpUtil.executeUpdate();
			insertEmailRcpUtil.reset();

			if (emailBean.getAttachmentList() != null) {
				String reportPath = RegularConstants.EMPTY_STRING;
				DBUtil readSystemFolderUtil = dbContext.createUtilInstance();
				String readSystemFolderSql = "SELECT * FROM SYSTEMFOLDERS WHERE ENTITY_CODE = ? AND EFFT_DATE = (SELECT MAX(EFFT_DATE) FROM SYSTEMFOLDERS WHERE ENTITY_CODE = ? AND EFFT_DATE <= FN_GETCD(?))";
				readSystemFolderUtil.reset();
				readSystemFolderUtil.setSql(readSystemFolderSql);
				readSystemFolderUtil.setString(1, getEntityCode());
				readSystemFolderUtil.setString(2, getEntityCode());
				readSystemFolderUtil.setString(3, getEntityCode());
				ResultSet rset = readSystemFolderUtil.executeQuery();
				if (rset.next()) {
					reportPath = rset.getString("REPORT_GENERATION_PATH");
				}
				readSystemFolderUtil.reset();

				/* UPDATE EMAILOUTQATTACH */
				String insertEmailAttachSql = "INSERT INTO EMAILOUTQATTACH (ENTITY_CODE,INVENTORY_NO,SL,ATTACHMENT_NAME,ATTACHMENT_PATH) VALUES (?,?,?,?,?)";
				DBUtil insertEmailAttachUtil = dbContext.createUtilInstance();
				int sl = 1;
				for (String attachmentName : emailBean.getAttachmentList().keySet()) {
					/*
					 * String reportFilePath =
					 * reportPath.concat(attachmentName); FileOutputStream
					 * fileOuputStream = new FileOutputStream(reportFilePath);
					 * fileOuputStream
					 * .write(emailBean.getAttachmentList().get(attachmentName
					 * )); fileOuputStream.close();
					 */

					insertEmailAttachUtil.setMode(DBUtil.PREPARED);
					insertEmailAttachUtil.setSql(insertEmailAttachSql);
					insertEmailAttachUtil.setString(1, getEntityCode());
					insertEmailAttachUtil.setString(2, inventoryNumber);
					insertEmailAttachUtil.setInt(3, sl);
					insertEmailAttachUtil.setString(4, attachmentName);
					insertEmailAttachUtil.setString(5, reportPath);
					insertEmailAttachUtil.executeUpdate();
					insertEmailAttachUtil.reset();
					sl++;
				}
			}

			/* UPDATE EMAILOUTQPE */
			String insertEmailOutqPESql = "INSERT INTO EMAILOUTQPE (ENTITY_CODE,INVENTORY_NO) VALUES (?,?)";
			DBUtil insertEmailOutqPEUtil = dbContext.createUtilInstance();
			insertEmailOutqPEUtil.reset();
			insertEmailOutqPEUtil.setMode(DBUtil.PREPARED);
			insertEmailOutqPEUtil.setSql(insertEmailOutqPESql);
			insertEmailOutqPEUtil.setString(1, getEntityCode());
			insertEmailOutqPEUtil.setString(2, inventoryNumber);
			insertEmailOutqPEUtil.executeUpdate();
			insertEmailOutqPEUtil.reset();

			// dbContext.commit();
			logger.logDebug("process() End");
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError(e.getLocalizedMessage());
			logger.logError("process() Exception 1" + e.getLocalizedMessage());
			try {
				// dbContext.rollback();
			} catch (Exception re) {
				re.printStackTrace();
				logger.logError(re.getLocalizedMessage());
				logger.logError("process() Exception 2" + re.getLocalizedMessage());
			}
		} finally {
			// dbContext.close();
		}
	}

	public void process() {
		logger.logDebug("process() Begin");
		DBContext dbContext = new DBContext();
		try {
			dbContext.setAutoCommit(false);
			EmailBean emailBean = (EmailBean) getPojoInstance();
			String inventoryNumber = RegularConstants.EMPTY_STRING;
			/* GENERATE INVENTORY NUMBER */
			String selectEmailSequenceSql = "SELECT FN_NEXTVAL('SEQ_EMAIL') FROM DUAL";
			DBUtil selectEmailSequenceUtil = dbContext.createUtilInstance();
			selectEmailSequenceUtil.reset();
			selectEmailSequenceUtil.setMode(DBUtil.PREPARED);
			selectEmailSequenceUtil.setSql(selectEmailSequenceSql);
			ResultSet emailSequenceRSet = selectEmailSequenceUtil.executeQuery();
			if (emailSequenceRSet.next()) {
				inventoryNumber = emailSequenceRSet.getString(1);
			}
			selectEmailSequenceUtil.reset();

			/* UPDATE EMAILOUTQLOG */
			String insertEmailOutqLogSql = "INSERT INTO EMAILOUTQLOG (ENTITY_CODE,INVENTORY_NO,APPL_CODE,REQ_DATE,EMAIL_SUBJECT,EMAIL_TEXT,STATUS) VALUES (?,?,?,?,?,?,'I')";
			DBUtil insertEmailOutqLogUtil = dbContext.createUtilInstance();
			insertEmailOutqLogUtil.reset();
			insertEmailOutqLogUtil.setMode(DBUtil.PREPARED);
			insertEmailOutqLogUtil.setSql(insertEmailOutqLogSql);
			insertEmailOutqLogUtil.setString(1, getEntityCode());
			insertEmailOutqLogUtil.setString(2, inventoryNumber);
			insertEmailOutqLogUtil.setString(3, emailBean.getEmailInterfaceCode());
			insertEmailOutqLogUtil.setString(4, new java.sql.Timestamp(new java.util.Date().getTime()).toString());
			insertEmailOutqLogUtil.setString(5, emailBean.getEmailSubject());
			insertEmailOutqLogUtil.setString(6, emailBean.getEmailBody());
			insertEmailOutqLogUtil.executeUpdate();
			insertEmailOutqLogUtil.reset();

			/* UPDATE EMAILOUTQRCP */
			String insertEmailRcpSql = "INSERT INTO EMAILOUTQRCP (ENTITY_CODE,INVENTORY_NO,EMAIL_RCP_TYPE,EMAIL_DTL_SL,EMAIL_ID) VALUES (?,?,?,?,?)";

			DBUtil insertEmailRcpUtil = dbContext.createUtilInstance();
			insertEmailRcpUtil.reset();
			insertEmailRcpUtil.setMode(DBUtil.PREPARED);
			insertEmailRcpUtil.setSql(insertEmailRcpSql);
			insertEmailRcpUtil.setString(1, getEntityCode());
			insertEmailRcpUtil.setString(2, inventoryNumber);
			insertEmailRcpUtil.setString(3, "T");
			insertEmailRcpUtil.setInt(4, 1);
			insertEmailRcpUtil.setString(5, emailBean.getEmailID());
			insertEmailRcpUtil.executeUpdate();
			insertEmailRcpUtil.reset();

			if (emailBean.getAttachmentList() != null) {
				String reportPath = RegularConstants.EMPTY_STRING;
				DBUtil readSystemFolderUtil = dbContext.createUtilInstance();
				String readSystemFolderSql = "SELECT * FROM SYSTEMFOLDERS WHERE ENTITY_CODE = ? AND EFFT_DATE = (SELECT MAX(EFFT_DATE) FROM SYSTEMFOLDERS WHERE ENTITY_CODE = ? AND EFFT_DATE <= FN_GETCD(?))";
				readSystemFolderUtil.reset();
				readSystemFolderUtil.setSql(readSystemFolderSql);
				readSystemFolderUtil.setString(1, getEntityCode());
				readSystemFolderUtil.setString(2, getEntityCode());
				readSystemFolderUtil.setString(3, getEntityCode());
				ResultSet rset = readSystemFolderUtil.executeQuery();
				if (rset.next()) {
					reportPath = rset.getString("REPORT_GENERATION_PATH");
				}
				readSystemFolderUtil.reset();

				/* UPDATE EMAILOUTQATTACH */
				String insertEmailAttachSql = "INSERT INTO EMAILOUTQATTACH (ENTITY_CODE,INVENTORY_NO,SL,ATTACHMENT_NAME,ATTACHMENT_PATH) VALUES (?,?,?,?,?)";
				DBUtil insertEmailAttachUtil = dbContext.createUtilInstance();
				int sl = 1;
				for (String attachmentName : emailBean.getAttachmentList().keySet()) {
					/*
					 * String reportFilePath =
					 * reportPath.concat(attachmentName); FileOutputStream
					 * fileOuputStream = new FileOutputStream(reportFilePath);
					 * fileOuputStream
					 * .write(emailBean.getAttachmentList().get(attachmentName
					 * )); fileOuputStream.close();
					 */

					insertEmailAttachUtil.setMode(DBUtil.PREPARED);
					insertEmailAttachUtil.setSql(insertEmailAttachSql);
					insertEmailAttachUtil.setString(1, getEntityCode());
					insertEmailAttachUtil.setString(2, inventoryNumber);
					insertEmailAttachUtil.setInt(3, sl);
					insertEmailAttachUtil.setString(4, attachmentName);
					insertEmailAttachUtil.setString(5, reportPath);
					insertEmailAttachUtil.executeUpdate();
					insertEmailAttachUtil.reset();
					sl++;
				}
			}

			/* UPDATE EMAILOUTQPE */
			String insertEmailOutqPESql = "INSERT INTO EMAILOUTQPE (ENTITY_CODE,INVENTORY_NO) VALUES (?,?)";
			DBUtil insertEmailOutqPEUtil = dbContext.createUtilInstance();
			insertEmailOutqPEUtil.reset();
			insertEmailOutqPEUtil.setMode(DBUtil.PREPARED);
			insertEmailOutqPEUtil.setSql(insertEmailOutqPESql);
			insertEmailOutqPEUtil.setString(1, getEntityCode());
			insertEmailOutqPEUtil.setString(2, inventoryNumber);
			insertEmailOutqPEUtil.executeUpdate();
			insertEmailOutqPEUtil.reset();

			dbContext.commit();
			logger.logDebug("process() End");
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError(e.getLocalizedMessage());
			logger.logError("process() Exception 1" + e.getLocalizedMessage());
			try {
				dbContext.rollback();
			} catch (Exception re) {
				re.printStackTrace();
				logger.logError(re.getLocalizedMessage());
				logger.logError("process() Exception 2" + re.getLocalizedMessage());
			}
		} finally {
			dbContext.close();
		}
	}

	public void processWithInventoryNumber(DBContext dbContext, String inventoryNumber) {
		logger.logDebug("process() Begin");
		// DBContext dbContext = new DBContext();
		try {
			// .setAutoCommit(false);
			EmailBean emailBean = (EmailBean) getPojoInstance();
			/* GENERATE INVENTORY NUMBER */
			DBUtil selectEmailSequenceUtil = dbContext.createUtilInstance();
			selectEmailSequenceUtil.reset();
			/* UPDATE EMAILOUTQLOG */
			String insertEmailOutqLogSql = "INSERT INTO EMAILOUTQLOG (ENTITY_CODE,INVENTORY_NO,APPL_CODE,REQ_DATE,EMAIL_SUBJECT,EMAIL_TEXT,STATUS) VALUES (?,?,?,?,?,?,'I')";
			DBUtil insertEmailOutqLogUtil = dbContext.createUtilInstance();
			insertEmailOutqLogUtil.reset();
			insertEmailOutqLogUtil.setMode(DBUtil.PREPARED);
			insertEmailOutqLogUtil.setSql(insertEmailOutqLogSql);
			insertEmailOutqLogUtil.setString(1, getEntityCode());
			insertEmailOutqLogUtil.setString(2, inventoryNumber);
			insertEmailOutqLogUtil.setString(3, emailBean.getEmailInterfaceCode());
			insertEmailOutqLogUtil.setString(4, new java.sql.Timestamp(new java.util.Date().getTime()).toString());
			insertEmailOutqLogUtil.setString(5, emailBean.getEmailSubject());
			insertEmailOutqLogUtil.setString(6, emailBean.getEmailBody());
			insertEmailOutqLogUtil.executeUpdate();
			insertEmailOutqLogUtil.reset();

			if (emailBean.getToEmailIDList() != null) {
				/* UPDATE EMAILOUTQATTACH */
				String insertEmailRcpSql = "INSERT INTO EMAILOUTQRCP (ENTITY_CODE,INVENTORY_NO,EMAIL_RCP_TYPE,EMAIL_DTL_SL,EMAIL_ID) VALUES (?,?,?,?,?)";
				DBUtil insertEmailRcpUtil = dbContext.createUtilInstance();
				int sl = 1;
				for (String emailID : emailBean.getToEmailIDList()) {
					insertEmailRcpUtil.setMode(DBUtil.PREPARED);
					insertEmailRcpUtil.setSql(insertEmailRcpSql);
					insertEmailRcpUtil.setString(1, getEntityCode());
					insertEmailRcpUtil.setString(2, inventoryNumber);
					insertEmailRcpUtil.setString(3, "T");
					insertEmailRcpUtil.setInt(4, sl);
					insertEmailRcpUtil.setString(5, emailID);
					insertEmailRcpUtil.executeUpdate();
					insertEmailRcpUtil.reset();
					sl++;
				}
			}
			if (emailBean.getCcEmailIDList() != null) {
				String insertEmailRcpSql = "INSERT INTO EMAILOUTQRCP (ENTITY_CODE,INVENTORY_NO,EMAIL_RCP_TYPE,EMAIL_DTL_SL,EMAIL_ID) VALUES (?,?,?,?,?)";
				DBUtil insertEmailRcpUtil = dbContext.createUtilInstance();
				int sl = 1;
				for (String emailID : emailBean.getCcEmailIDList()) {
					insertEmailRcpUtil.setMode(DBUtil.PREPARED);
					insertEmailRcpUtil.setSql(insertEmailRcpSql);
					insertEmailRcpUtil.setString(1, getEntityCode());
					insertEmailRcpUtil.setString(2, inventoryNumber);
					insertEmailRcpUtil.setString(3, "C");
					insertEmailRcpUtil.setInt(4, sl);
					insertEmailRcpUtil.setString(5, emailID);
					insertEmailRcpUtil.executeUpdate();
					insertEmailRcpUtil.reset();
					sl++;
				}
			}

			if (emailBean.getBccEmailIDList() != null) {
				String insertEmailRcpSql = "INSERT INTO EMAILOUTQRCP (ENTITY_CODE,INVENTORY_NO,EMAIL_RCP_TYPE,EMAIL_DTL_SL,EMAIL_ID) VALUES (?,?,?,?,?)";
				DBUtil insertEmailRcpUtil = dbContext.createUtilInstance();
				int sl = 1;
				for (String emailID : emailBean.getBccEmailIDList()) {
					insertEmailRcpUtil.setMode(DBUtil.PREPARED);
					insertEmailRcpUtil.setSql(insertEmailRcpSql);
					insertEmailRcpUtil.setString(1, getEntityCode());
					insertEmailRcpUtil.setString(2, inventoryNumber);
					insertEmailRcpUtil.setString(3, "B");
					insertEmailRcpUtil.setInt(4, sl);
					insertEmailRcpUtil.setString(5, emailID);
					insertEmailRcpUtil.executeUpdate();
					insertEmailRcpUtil.reset();
					sl++;
				}
			}

			if (emailBean.getAttachmentList() != null) {
				String reportPath = RegularConstants.EMPTY_STRING;
				DBUtil readSystemFolderUtil = dbContext.createUtilInstance();
				String readSystemFolderSql = "SELECT * FROM SYSTEMFOLDERS WHERE ENTITY_CODE = ? AND EFFT_DATE = (SELECT MAX(EFFT_DATE) FROM SYSTEMFOLDERS WHERE ENTITY_CODE = ? AND EFFT_DATE <= FN_GETCD(?))";
				readSystemFolderUtil.reset();
				readSystemFolderUtil.setSql(readSystemFolderSql);
				readSystemFolderUtil.setString(1, getEntityCode());
				readSystemFolderUtil.setString(2, getEntityCode());
				readSystemFolderUtil.setString(3, getEntityCode());
				ResultSet rset = readSystemFolderUtil.executeQuery();
				if (rset.next()) {
					reportPath = rset.getString("REPORT_GENERATION_PATH");
				}
				readSystemFolderUtil.reset();
				/* UPDATE EMAILOUTQATTACH */
				String insertEmailAttachSql = "INSERT INTO EMAILOUTQATTACH (ENTITY_CODE,INVENTORY_NO,SL,ATTACHMENT_NAME,ATTACHMENT_PATH) VALUES (?,?,?,?,?)";
				DBUtil insertEmailAttachUtil = dbContext.createUtilInstance();
				int sl = 1;
				for (String attachmentName : emailBean.getAttachmentList().keySet()) {
					/*
					 * String reportFilePath =
					 * reportPath.concat(attachmentName); FileOutputStream
					 * fileOuputStream = new FileOutputStream(reportFilePath);
					 * fileOuputStream
					 * .write(emailBean.getAttachmentList().get(attachmentName
					 * )); fileOuputStream.close();
					 */

					insertEmailAttachUtil.setMode(DBUtil.PREPARED);
					insertEmailAttachUtil.setSql(insertEmailAttachSql);
					insertEmailAttachUtil.setString(1, getEntityCode());
					insertEmailAttachUtil.setString(2, inventoryNumber);
					insertEmailAttachUtil.setInt(3, sl);
					insertEmailAttachUtil.setString(4, attachmentName);
					insertEmailAttachUtil.setString(5, reportPath);
					insertEmailAttachUtil.executeUpdate();
					insertEmailAttachUtil.reset();
					sl++;
				}
			}

			/* UPDATE EMAILOUTQPE */
			String insertEmailOutqPESql = "INSERT INTO EMAILOUTQPE (ENTITY_CODE,INVENTORY_NO) VALUES (?,?)";
			DBUtil insertEmailOutqPEUtil = dbContext.createUtilInstance();
			insertEmailOutqPEUtil.reset();
			insertEmailOutqPEUtil.setMode(DBUtil.PREPARED);
			insertEmailOutqPEUtil.setSql(insertEmailOutqPESql);
			insertEmailOutqPEUtil.setString(1, getEntityCode());
			insertEmailOutqPEUtil.setString(2, inventoryNumber);
			insertEmailOutqPEUtil.executeUpdate();
			insertEmailOutqPEUtil.reset();
			// dbContext.commit();
			logger.logDebug("process() End");
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError(e.getLocalizedMessage());
			logger.logError("process() Exception 1" + e.getLocalizedMessage());
			try {
				// dbContext.rollback();
			} catch (Exception re) {
				re.printStackTrace();
				logger.logError(re.getLocalizedMessage());
				logger.logError("process() Exception 2" + re.getLocalizedMessage());
			}
		} finally {
			// dbContext.close();
		}
	}

}
