package com.patterns.interfaces.interceptors.res.lab;

import patterns.config.framework.interfaces.InterfaceInterceptor;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.ajax.ContentManager;

public class elabresultsintfinterceptor extends InterfaceInterceptor {

	@Override
	public DTObject resolvePartitionArguments(DTObject input) {
		Object[] bucket=new Object[1];
		String arguments = input.get(ARGUMENT_KEY);
		String[] primaryKey = arguments.split("\\|");
		try {
			if (arguments != null) {
				bucket[0] = primaryKey[2];
				input.setObject(ContentManager.YEAR_PARAM, bucket);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
		}
		return input;
	}

}
