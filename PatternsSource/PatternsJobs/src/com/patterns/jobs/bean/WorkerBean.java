package com.patterns.jobs.bean;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.List;

public class WorkerBean implements Serializable {
	private static final long serialVersionUID = 5496532878685335278L;

	private String entityCode;
	private String workerCode;
	private String description;
	private String jobCode;
	private List<BigInteger> jobIndices;

	public String getEntityCode() {
		return entityCode;
	}

	public void setEntityCode(String entityCode) {
		this.entityCode = entityCode;
	}

	public String getWorkerCode() {
		return workerCode;
	}

	public void setWorkerCode(String workerCode) {
		this.workerCode = workerCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getJobCode() {
		return jobCode;
	}

	public void setJobCode(String jobCode) {
		this.jobCode = jobCode;
	}

	public List<BigInteger> getJobIndices() {
		return jobIndices;
	}

	public void setJobIndices(List<BigInteger> jobIndices) {
		this.jobIndices = jobIndices;
	}
}
