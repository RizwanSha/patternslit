package com.patterns.jobs.bean;
public class FileDetails {
		private String entityCode;
		private String sourceKey;
		private String fileName;
		private String purposeCode;
		private String schemeCode;
		private String officeCode;

		public String getEntityCode() {
			return entityCode;
		}

		public void setEntityCode(String entityCode) {
			this.entityCode = entityCode;
		}

		public String getSourceKey() {
			return sourceKey;
		}

		public void setSourceKey(String sourceKey) {
			this.sourceKey = sourceKey;
		}

		public String getFileName() {
			return fileName;
		}

		public void setFileName(String fileName) {
			this.fileName = fileName;
		}

		public String getPurposeCode() {
			return purposeCode;
		}

		public void setPurposeCode(String purposeCode) {
			this.purposeCode = purposeCode;
		}

		public String getSchemeCode() {
			return schemeCode;
		}

		public void setSchemeCode(String schemeCode) {
			this.schemeCode = schemeCode;
		}

		public String getOfficeCode() {
			return officeCode;
		}

		public void setOfficeCode(String officeCode) {
			this.officeCode = officeCode;
		}
	}