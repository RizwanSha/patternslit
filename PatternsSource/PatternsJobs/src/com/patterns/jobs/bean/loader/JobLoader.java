package com.patterns.jobs.bean.loader;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.loggers.ApplicationLogger;

import com.patterns.jobs.bean.JobBean;

public class JobLoader {

	private static final ReentrantReadWriteLock _READ_WRITE_LOCK_GLOBAL = new ReentrantReadWriteLock();

	private static final Lock _WRITE_GLOBAL = _READ_WRITE_LOCK_GLOBAL.writeLock();

	private final ApplicationLogger _LOGGER;

	private JobLoader() {
		_LOGGER = ApplicationLogger.getInstance(this.getClass().getSimpleName());
	}

	private Map<String, JobBean> internalMap;
	private Map<String, JobBean> internalLocalMap;

	private static class JobLoaderHelper {

		private static final JobLoader _INSTANCE = new JobLoader();
	}

	public static JobLoader getInstance() {
		return JobLoaderHelper._INSTANCE;
	}

	public Map<String, JobBean> getAvailableJobs() {
		return internalMap;
	}

	public final void unloadConfiguration() {
		try {
			_WRITE_GLOBAL.lock();
			resetConfiguration();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			_WRITE_GLOBAL.unlock();
		}
	}

	public final void reloadConfiguration() {
		try {
			_WRITE_GLOBAL.lock();
			loadConfiguration();
			resetConfiguration();
			initConfiguration();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			_WRITE_GLOBAL.unlock();
		}
	}

	public final JobBean getJob(String jobCode) {
		return internalMap.get(jobCode);
	}

	private final void initConfiguration() {
		internalMap.putAll(internalLocalMap);
		internalLocalMap.clear();
	}

	private final void resetConfiguration() {
		_LOGGER.logDebug("resetConfiguration()");
		if (internalMap == null) {
			internalMap = new HashMap<String, JobBean>();
		}
		internalMap.clear();
	}

	private final void loadConfiguration() {
		_LOGGER.logDebug("loadConfiguration() Begin");
		if (internalLocalMap == null) {
			internalLocalMap = new HashMap<String, JobBean>();
		}
		internalLocalMap.clear();
		DBContext dbContext = new DBContext();
		try {
			DBUtil jobUtil = dbContext.createUtilInstance();
			String jobQuery = "SELECT JOB_CODE,DESCRIPTION,CONTROLLER,MAX_WORKERS,CRON_EXP,MAX_RECORDS,RETRY_TIMEOUT,AU_ON FROM JOBCLASS WHERE STATUS='1'";
			jobUtil.setSql(jobQuery);
			ResultSet jobRSet = jobUtil.executeQuery();
			while (jobRSet.next()) {
				JobBean job = new JobBean();
				job.setCode(jobRSet.getString(1));
				job.setDescription(jobRSet.getString(2));
				job.setController(jobRSet.getString(3));
				job.setMaxWorker(jobRSet.getInt(4));
				job.setCronExpression(jobRSet.getString(5));
				job.setMaxRecords(jobRSet.getDouble(6));
				job.setRetryTimeOut(jobRSet.getInt(7));
				job.setAuthorizedOn(jobRSet.getTimestamp(8));
				internalLocalMap.put(jobRSet.getString(1), job);
			}
			jobUtil.reset();
		} catch (Exception e) {
			_LOGGER.logError("loadConfiguration() Exception " + e.getLocalizedMessage());
		} finally {
			dbContext.close();
		}
		_LOGGER.logDebug("loadConfiguration() End");
	}
}