package com.patterns.jobs.bean.loader;

import java.sql.ResultSet;

import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;

import com.patterns.jobs.application.validator.JobsValidator;
import com.patterns.jobs.bean.EmailServerBean;

public class EmailServerLoader {

	private String entityCode;
	private String emailInterfaceCode;

	public EmailServerLoader(String entityCode, String emailInterfaceCode) {
		this.entityCode = entityCode;
		this.emailInterfaceCode = emailInterfaceCode;
	}

	public final EmailServerBean getEmailServer() {
		EmailServerBean emailServer = new EmailServerBean();
		DBContext dbContext = new DBContext();
		try {
			String emailServerSql = "SELECT SSL_REQUIRED,COMMUNICATION_NAME,COMMUNICATION_EMAIL,PROXY_REQUIRED,PROXY_SERVER,PROXY_SERVER_PORT,PROXY_AUTH_REQD,PROXY_USER_NAME,PROXY_PASSWORD,OUTGOING_MAIL_SERVER,OUTGOING_MAIL_SERVER_PORT,OUTGOING_AUTH_REQD,OUTGOING_USER_NAME,OUTGOING_PASSWORD,SEND_PROTO_TYPE FROM EMAILINTFCFG WHERE ENTITY_CODE = ? AND EMAIL_CODE=? AND EFFT_DATE = (SELECT MAX(EFFT_DATE) FROM EMAILINTFCFG WHERE ENTITY_CODE  = ? AND EMAIL_CODE=? AND EFFT_DATE <= FN_GETCD(?))";
			DBUtil emailServerUtil = dbContext.createUtilInstance();
			emailServerUtil.reset();
			emailServerUtil.setSql(emailServerSql);
			emailServerUtil.setString(1, entityCode);
			emailServerUtil.setString(2, emailInterfaceCode);
			emailServerUtil.setString(3, entityCode);
			emailServerUtil.setString(4, emailInterfaceCode);
			emailServerUtil.setString(5, entityCode);
			ResultSet emailServerRSet = emailServerUtil.executeQuery();
			if (emailServerRSet.next()) {
				emailServer.setEntityCode(entityCode);
				emailServer.setRequireSSL(JobsValidator.decodeStringToBoolean(emailServerRSet.getString("SSL_REQUIRED")));
				emailServer.setSenderName(emailServerRSet.getString("COMMUNICATION_NAME"));
				emailServer.setSenderMail(emailServerRSet.getString("COMMUNICATION_EMAIL"));
				emailServer.setServerIP(emailServerRSet.getString("OUTGOING_MAIL_SERVER"));
				emailServer.setServerPort(emailServerRSet.getInt("OUTGOING_MAIL_SERVER_PORT"));
				emailServer.setAuthenticationRequired(JobsValidator.decodeStringToBoolean(emailServerRSet.getString("OUTGOING_AUTH_REQD")));
				if (emailServer.isAuthenticationRequired()) {
					emailServer.setAccountName(emailServerRSet.getString("OUTGOING_USER_NAME"));
					emailServer.setAccountPassword(emailServerRSet.getString("OUTGOING_PASSWORD"));
				}
				emailServer.setProxyRequired(JobsValidator.decodeStringToBoolean(emailServerRSet.getString("PROXY_REQUIRED")));
				if (emailServer.isProxyRequired()) {
					emailServer.setProxyServerIP(emailServerRSet.getString("PROXY_SERVER"));
					emailServer.setProxyServerPort(emailServerRSet.getInt("PROXY_SERVER_PORT"));
					emailServer.setProxyAuthenticationRequired(JobsValidator.decodeStringToBoolean(emailServerRSet.getString("PROXY_AUTH_REQD")));
					if (emailServer.isProxyAuthenticationRequired()) {
						emailServer.setProxyUsername(emailServerRSet.getString("PROXY_USER_NAME"));
						emailServer.setProxyPassword(emailServerRSet.getString("PROXY_PASSWORD"));
					}
				}
				emailServer.setSendProtocol(emailServerRSet.getString("SEND_PROTO_TYPE"));
			}
			emailServerUtil.reset();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			dbContext.close();
		}
		return emailServer;
	}
}
