/*
 * Copyright (c) 2014, 2015, Patterns Software Design Institute. All
 * rights reserved. Unauthorized copying of this file, via any medium is
 * strictly prohibited. PATTERNS SDI PROPRIETARY/CONFIDENTIAL. Use is
 * subject to license terms.
 */
package com.patterns.jobs.bean.loader;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import javax.servlet.ServletContext;

import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.loggers.ApplicationLogger;
import patterns.config.framework.monitor.ContextReference;

import com.patterns.jobs.bean.TemplateBean;


/**
 * The Class TemplateLoader provides in-memory information about
 * template configuration
 */
public class TemplateLoader {

	/** The Constant _READ_WRITE_LOCK_GLOBAL. */
	private static final ReentrantReadWriteLock _READ_WRITE_LOCK_GLOBAL = new ReentrantReadWriteLock();

	/** The Constant _WRITE_GLOBAL. */
	private static final Lock _WRITE_GLOBAL = _READ_WRITE_LOCK_GLOBAL.writeLock();

	/** The logger. */
	private final ApplicationLogger logger;

	/**
	 * Instantiates a new template loader.
	 */
	private TemplateLoader() {
		logger = ApplicationLogger.getInstance(this.getClass().getSimpleName());
	}

	/** The internal map. */
	private Map<String, Map<String, TemplateBean>> internalMap;

	/** The internal local map. */
	private Map<String, Map<String, TemplateBean>> internalLocalMap;

	/**
	 * The Class ServiceTemplatesMapHelper.
	 */
	private static class ServiceTemplatesMapHelper {

		/** The Constant _INSTANCE. */
		private static final TemplateLoader _INSTANCE = new TemplateLoader();
	}

	/**
	 * Gets the single instance of TemplateLoader.
	 * 
	 * @return single instance of TemplateLoader
	 */
	public static TemplateLoader getInstance() {
		return ServiceTemplatesMapHelper._INSTANCE;
	}

	/**
	 * Unload configuration.
	 */
	public final void unloadConfiguration() {
		try {
			_WRITE_GLOBAL.lock();
			resetConfiguration();
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError(e.getLocalizedMessage());
		} finally {
			_WRITE_GLOBAL.unlock();
		}
	}

	/**
	 * Reload configuration.
	 */
	public final void reloadConfiguration() {
		try {
			_WRITE_GLOBAL.lock();
			loadConfiguration();
			resetConfiguration();
			initConfiguration();
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError(e.getLocalizedMessage());
		} finally {
			_WRITE_GLOBAL.unlock();
		}
	}

	/**
	 * Inits the configuration.
	 */
	private final void initConfiguration() {
		internalMap.putAll(internalLocalMap);
		internalLocalMap.clear();
	}

	/**
	 * Reset configuration.
	 */
	private final void resetConfiguration() {
		logger.logDebug("resetConfiguration()");
		if (internalMap == null) {
			internalMap = new HashMap<String, Map<String, TemplateBean>>();
		}
		internalMap.clear();
	}

	/**
	 * Gets the template.
	 * 
	 * @param entityCode
	 *            the entity code
	 * @param templateCode
	 *            the template code
	 * @return the template
	 */
	public final TemplateBean getTemplate(String entityCode, String templateCode) {
		return internalMap.get(entityCode).get(templateCode);
	}

	/**
	 * Load configuration.
	 */
	private final void loadConfiguration() {
		logger.logDebug("loadConfiguration()");
		if (internalLocalMap == null) {
			internalLocalMap = new HashMap<String, Map<String, TemplateBean>>();
		}
		internalLocalMap.clear();

		DBContext dbContext = new DBContext();
		try {
				ServletContext servletContext = ContextReference.getContext();
				Map<String, TemplateBean> templateMap = new HashMap<String, TemplateBean>();
				String templateSql = "SELECT CODE,DESCRIPTION,TEMPLATE_TYPE,TEMPLATE_SUB,TEMPLATE_BODY,TEMPLATE_FOOTER FROM NTFNTEMPLATES WHERE ENTITY_CODE=?";
				String entityCode = servletContext.getInitParameter(RegularConstants.INIT_DEPLOYMENT_ENTITY);
				DBUtil templateUtil = dbContext.createUtilInstance();
				templateUtil.setSql(templateSql);
				templateUtil.setString(1, entityCode);
				ResultSet templateRSet = templateUtil.executeQuery();
				while (templateRSet.next()) {
					TemplateBean template = new TemplateBean();
					String templateCode = templateRSet.getString(1);
					template.setEntityCode(entityCode);
					template.setTemplateCode(templateCode);
					template.setDescription(templateRSet.getString(2));
					template.setTemplateType(templateRSet.getString(3));
					template.setSubjectEn(templateRSet.getString(4));
					template.setBodyEn(templateRSet.getString(5));
					template.setFooterEn(templateRSet.getString(6));
					templateMap.put(templateCode, template);
				}
				templateUtil.reset();
				internalLocalMap.put(entityCode, templateMap);
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError(e.getLocalizedMessage());
		} finally {
			dbContext.close();
		}
	}
}