package com.patterns.jobs.bean;

import java.io.Serializable;
import java.sql.Timestamp;

public class JobBean implements Serializable {
	private static final long serialVersionUID = 5496532878685335278L;

	private String code;
	private String description;
	private String controller;
	private int maxWorker;
	private String cronExpression;
	private double maxRecords;
	private int retryTimeOut;

	private Timestamp authorizedOn;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getController() {
		return controller;
	}

	public void setController(String controller) {
		this.controller = controller;
	}

	public Timestamp getAuthorizedOn() {
		return authorizedOn;
	}

	public void setAuthorizedOn(Timestamp authorizedOn) {
		this.authorizedOn = authorizedOn;
	}

	public int getMaxWorker() {
		return maxWorker;
	}

	public void setMaxWorker(int maxWorker) {
		this.maxWorker = maxWorker;
	}

	public String getCronExpression() {
		return cronExpression;
	}

	public void setCronExpression(String cronExpression) {
		this.cronExpression = cronExpression;
	}

	public double getMaxRecords() {
		return maxRecords;
	}

	public void setMaxRecords(double maxRecords) {
		this.maxRecords = maxRecords;
	}

	public int getRetryTimeOut() {
		return retryTimeOut;
	}

	public void setRetryTimeOut(int retryTimeOut) {
		this.retryTimeOut = retryTimeOut;
	}
}
