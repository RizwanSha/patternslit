package com.patterns.jobs.bean;

import java.io.Serializable;

public class SmsBean implements Serializable {

	private static final long serialVersionUID = 5513547060331773288L;

	private String requestInventory;
	private String customerAuthInventory;
	private String templateCode;
	private String mobileNumber;
	private String status;
	private String reason;

	public String getRequestInventory() {
		return requestInventory;
	}

	public void setRequestInventory(String requestInventory) {
		this.requestInventory = requestInventory;
	}

	public String getCustomerAuthInventory() {
		return customerAuthInventory;
	}

	public void setCustomerAuthInventory(String customerAuthInventory) {
		this.customerAuthInventory = customerAuthInventory;
	}

	public String getTemplateCode() {
		return templateCode;
	}

	public void setTemplateCode(String templateCode) {
		this.templateCode = templateCode;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}
}
