package com.patterns.jobs.bean;

public class DebitLegInfo {
	private boolean isPayRefended;
	private boolean isFromFile;
	private String amountSource;
	private String glHead;
	private String accountNo;

	public boolean isPayRefended() {
		return isPayRefended;
	}

	public void setPayRefended(boolean isPayRefended) {
		this.isPayRefended = isPayRefended;
	}

	public String getAmountSource() {
		return amountSource;
	}

	public void setAmountSource(String amountSource) {
		this.amountSource = amountSource;
	}

	public String getGlHead() {
		return glHead;
	}

	public void setGlHead(String glHead) {
		this.glHead = glHead;
	}

	public String getAccountNo() {
		return accountNo;
	}

	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}
}