package com.patterns.jobs.bean;

import java.math.BigDecimal;
import java.math.BigInteger;

public class FileRecord {
		private BigInteger accountNo;
		private String currency;
		private String tranMode;
		private BigDecimal amount;
		private String narration;

		public BigInteger getAccountNo() {
			return accountNo;
		}

		public void setAccountNo(BigInteger accountNo) {
			this.accountNo = accountNo;
		}

		public String getCurrency() {
			return currency;
		}

		public void setCurrency(String currency) {
			this.currency = currency;
		}

		public String getTranMode() {
			return tranMode;
		}

		public void setTranMode(String tranMode) {
			this.tranMode = tranMode;
		}

		public BigDecimal getAmount() {
			return amount;
		}

		public void setAmount(BigDecimal amount) {
			this.amount = amount;
		}

		public String getNarration() {
			return narration;
		}

		public void setNarration(String narration) {
			this.narration = narration;
		}
	}