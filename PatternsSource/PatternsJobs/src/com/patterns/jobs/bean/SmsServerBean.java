package com.patterns.jobs.bean;

import java.io.Serializable;

public class SmsServerBean implements Serializable {

	private static final long serialVersionUID = -2044059457888761606L;

	private String smsCode;
	private String entityCode;
	private boolean sslRequired;
	private boolean proxyRequired;
	private String proxyServerIP;
	private int proxyServerPort;
	private boolean proxyAuthenticationRequired;
	private String proxyUsername;
	private String proxyPassword;

	private boolean smppDispatch;
	private boolean smppAuthenticationRequired;
	private String smppUserName;
	private String smppPassword;
	private String systemType;
	private String addressRange;
	private boolean transceiverMode;
	private String mainSmppServer;
	private int mainSmppPort;
	private String failoverSmppServer;
	private int failoverSmppPort;
	private String serviceType;
	private String sourceNumber;
	private String smsType;
	private int replaceSmsTimeout;
	private String templateCode;

	private boolean httpDispatch;
	private String httpMethod;
	private String serverURL;
	private boolean httpAuthenticationRequired;
	private String userNameKey;
	private String httpUserName;
	private String passwordKey;
	private String httpPassword;
	private String mobileNumberKey;
	private String textKey;
	private String senderKey;
	private String senderName;

	public String getEntityCode() {
		return entityCode;
	}

	public void setEntityCode(String entityCode) {
		this.entityCode = entityCode;
	}

	public boolean isSslRequired() {
		return sslRequired;
	}

	public void setSslRequired(boolean sslRequired) {
		this.sslRequired = sslRequired;
	}

	public boolean isProxyRequired() {
		return proxyRequired;
	}

	public void setProxyRequired(boolean proxyRequired) {
		this.proxyRequired = proxyRequired;
	}

	public String getProxyServerIP() {
		return proxyServerIP;
	}

	public void setProxyServerIP(String proxyServerIP) {
		this.proxyServerIP = proxyServerIP;
	}

	public int getProxyServerPort() {
		return proxyServerPort;
	}

	public void setProxyServerPort(int proxyServerPort) {
		this.proxyServerPort = proxyServerPort;
	}

	public boolean isProxyAuthenticationRequired() {
		return proxyAuthenticationRequired;
	}

	public void setProxyAuthenticationRequired(boolean proxyAuthenticationRequired) {
		this.proxyAuthenticationRequired = proxyAuthenticationRequired;
	}

	public String getProxyUsername() {
		return proxyUsername;
	}

	public void setProxyUsername(String proxyUsername) {
		this.proxyUsername = proxyUsername;
	}

	public String getProxyPassword() {
		return proxyPassword;
	}

	public void setProxyPassword(String proxyPassword) {
		this.proxyPassword = proxyPassword;
	}

	public boolean isSmppDispatch() {
		return smppDispatch;
	}

	public void setSmppDispatch(boolean smppDispatch) {
		this.smppDispatch = smppDispatch;
	}

	public boolean isSmppAuthenticationRequired() {
		return smppAuthenticationRequired;
	}

	public void setSmppAuthenticationRequired(boolean smppAuthenticationRequired) {
		this.smppAuthenticationRequired = smppAuthenticationRequired;
	}

	public String getSmppUserName() {
		return smppUserName;
	}

	public void setSmppUserName(String smppUserName) {
		this.smppUserName = smppUserName;
	}

	public String getSmppPassword() {
		return smppPassword;
	}

	public void setSmppPassword(String smppPassword) {
		this.smppPassword = smppPassword;
	}

	public String getSystemType() {
		return systemType;
	}

	public void setSystemType(String systemType) {
		this.systemType = systemType;
	}

	public String getAddressRange() {
		return addressRange;
	}

	public void setAddressRange(String addressRange) {
		this.addressRange = addressRange;
	}

	public boolean isTransceiverMode() {
		return transceiverMode;
	}

	public void setTransceiverMode(boolean transceiverMode) {
		this.transceiverMode = transceiverMode;
	}

	public String getMainSmppServer() {
		return mainSmppServer;
	}

	public void setMainSmppServer(String mainSmppServer) {
		this.mainSmppServer = mainSmppServer;
	}

	public int getMainSmppPort() {
		return mainSmppPort;
	}

	public void setMainSmppPort(int mainSmppPort) {
		this.mainSmppPort = mainSmppPort;
	}

	public String getFailoverSmppServer() {
		return failoverSmppServer;
	}

	public void setFailoverSmppServer(String failoverSmppServer) {
		this.failoverSmppServer = failoverSmppServer;
	}

	public int getFailoverSmppPort() {
		return failoverSmppPort;
	}

	public void setFailoverSmppPort(int failoverSmppPort) {
		this.failoverSmppPort = failoverSmppPort;
	}

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public String getSourceNumber() {
		return sourceNumber;
	}

	public void setSourceNumber(String sourceNumber) {
		this.sourceNumber = sourceNumber;
	}

	public String getSmsType() {
		return smsType;
	}

	public void setSmsType(String smsType) {
		this.smsType = smsType;
	}

	public int getReplaceSmsTimeout() {
		return replaceSmsTimeout;
	}

	public void setReplaceSmsTimeout(int replaceSmsTimeout) {
		this.replaceSmsTimeout = replaceSmsTimeout;
	}

	public String getTemplateCode() {
		return templateCode;
	}

	public void setTemplateCode(String templateCode) {
		this.templateCode = templateCode;
	}

	public boolean isHttpDispatch() {
		return httpDispatch;
	}

	public void setHttpDispatch(boolean httpDispatch) {
		this.httpDispatch = httpDispatch;
	}

	public String getHttpMethod() {
		return httpMethod;
	}

	public void setHttpMethod(String httpMethod) {
		this.httpMethod = httpMethod;
	}

	public String getServerURL() {
		return serverURL;
	}

	public void setServerURL(String serverURL) {
		this.serverURL = serverURL;
	}

	public boolean isHttpAuthenticationRequired() {
		return httpAuthenticationRequired;
	}

	public void setHttpAuthenticationRequired(boolean httpAuthenticationRequired) {
		this.httpAuthenticationRequired = httpAuthenticationRequired;
	}

	public String getUserNameKey() {
		return userNameKey;
	}

	public void setUserNameKey(String userNameKey) {
		this.userNameKey = userNameKey;
	}

	public String getHttpUserName() {
		return httpUserName;
	}

	public void setHttpUserName(String httpUserName) {
		this.httpUserName = httpUserName;
	}

	public String getPasswordKey() {
		return passwordKey;
	}

	public void setPasswordKey(String passwordKey) {
		this.passwordKey = passwordKey;
	}

	public String getHttpPassword() {
		return httpPassword;
	}

	public void setHttpPassword(String httpPassword) {
		this.httpPassword = httpPassword;
	}

	public String getMobileNumberKey() {
		return mobileNumberKey;
	}

	public void setMobileNumberKey(String mobileNumberKey) {
		this.mobileNumberKey = mobileNumberKey;
	}

	public String getTextKey() {
		return textKey;
	}

	public void setTextKey(String textKey) {
		this.textKey = textKey;
	}

	public String getSenderKey() {
		return senderKey;
	}

	public void setSenderKey(String senderKey) {
		this.senderKey = senderKey;
	}

	public String getSenderName() {
		return senderName;
	}

	public void setSenderName(String senderName) {
		this.senderName = senderName;
	}

	public String getSmsCode() {
		return smsCode;
	}

	public void setSmsCode(String smsCode) {
		this.smsCode = smsCode;
	}
}
