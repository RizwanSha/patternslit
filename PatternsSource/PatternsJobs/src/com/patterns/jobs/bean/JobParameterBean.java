package com.patterns.jobs.bean;

import java.io.Serializable;

public class JobParameterBean implements Serializable {
	private static final long serialVersionUID = 5496532878685335278L;

	private Integer maximumWorkers;
	private String cronExpression;
	private Integer maximumRecords;
	private Integer retryTimeout;
	private String status;

	public Integer getMaximumWorkers() {
		return maximumWorkers;
	}

	public void setMaximumWorkers(Integer maximumWorkers) {
		this.maximumWorkers = maximumWorkers;
	}

	public String getCronExpression() {
		return cronExpression;
	}

	public void setCronExpression(String cronExpression) {
		this.cronExpression = cronExpression;
	}

	public Integer getMaximumRecords() {
		return maximumRecords;
	}

	public void setMaximumRecords(Integer maximumRecords) {
		this.maximumRecords = maximumRecords;
	}

	public Integer getRetryTimeout() {
		return retryTimeout;
	}

	public void setRetryTimeout(Integer retryTimeout) {
		this.retryTimeout = retryTimeout;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
