/*
 * Copyright (c) 2014, 2015, Patterns Software Design Institute. All rights reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * PATTERNS SDI PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
package com.patterns.jobs.bean;

import java.io.Serializable;
import java.util.Date;


/**
 * The Class SmsTextBean.
 */
public class SmsTextBean implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 5513547060331773288L;

	/** The inventory number. */
	private String inventoryNumber;
	
	/** The mobile number. */
	private String mobileNumber;
	
	/** The text. */
	private String text;
	
	/** The created on. */
	private Date createdOn;
	
	/** The status. */
	private String status;
	
	/** The message id. */
	private String messageId;
	
	/** The message content. */
	private String messageContent;
	
	/** The sms interface code. */
	private String smsInterfaceCode;

	/**
	 * Gets the inventory number.
	 * 
	 * @return the inventory number
	 */
	public String getInventoryNumber() {
		return inventoryNumber;
	}

	/**
	 * Sets the inventory number.
	 * 
	 * @param inventoryNumber
	 *            the new inventory number
	 */
	public void setInventoryNumber(String inventoryNumber) {
		this.inventoryNumber = inventoryNumber;
	}

	/**
	 * Gets the mobile number.
	 * 
	 * @return the mobile number
	 */
	public String getMobileNumber() {
		return mobileNumber;
	}

	/**
	 * Sets the mobile number.
	 * 
	 * @param mobileNumber
	 *            the new mobile number
	 */
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	/**
	 * Gets the text.
	 * 
	 * @return the text
	 */
	public String getText() {
		return text;
	}

	/**
	 * Sets the text.
	 * 
	 * @param text
	 *            the new text
	 */
	public void setText(String text) {
		this.text = text;
	}

	/**
	 * Gets the created on.
	 * 
	 * @return the created on
	 */
	public Date getCreatedOn() {
		return createdOn;
	}

	/**
	 * Sets the created on.
	 * 
	 * @param createdOn
	 *            the new created on
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * Gets the status.
	 * 
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 * 
	 * @param status
	 *            the new status
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * Gets the message id.
	 * 
	 * @return the message id
	 */
	public String getMessageId() {
		return messageId;
	}

	/**
	 * Sets the message id.
	 * 
	 * @param messageId
	 *            the new message id
	 */
	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	/**
	 * Gets the message content.
	 * 
	 * @return the message content
	 */
	public String getMessageContent() {
		return messageContent;
	}

	/**
	 * Sets the message content.
	 * 
	 * @param messageContent
	 *            the new message content
	 */
	public void setMessageContent(String messageContent) {
		this.messageContent = messageContent;
	}

	/**
	 * Gets the sms interface code.
	 * 
	 * @return the sms interface code
	 */
	public String getSmsInterfaceCode() {
		return smsInterfaceCode;
	}

	/**
	 * Sets the sms interface code.
	 * 
	 * @param smsInterfaceCode
	 *            the new sms interface code
	 */
	public void setSmsInterfaceCode(String smsInterfaceCode) {
		this.smsInterfaceCode = smsInterfaceCode;
	}
}
