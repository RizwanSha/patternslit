package com.patterns.jobs.bean;

import java.io.Serializable;

public class EmailServerBean implements Serializable {

	private static final long serialVersionUID = -2044059457888761606L;

	private String entityCode;
	private boolean requireSSL;
	private String senderName;
	private String senderMail;
	private String serverIP;
	private int serverPort;
	private boolean authenticationRequired;
	private String accountName;
	private String accountPassword;

	private boolean proxyRequired;
	private String proxyServerIP;
	private int proxyServerPort;
	private boolean proxyAuthenticationRequired;
	private String proxyUsername;
	private String proxyPassword;

	private String sendProtocol;
	private String receiveProtocol;
	

	public String getEntityCode() {
		return entityCode;
	}

	public void setEntityCode(String entityCode) {
		this.entityCode = entityCode;
	}

	public boolean isRequireSSL() {
		return requireSSL;
	}

	public void setRequireSSL(boolean requireSSL) {
		this.requireSSL = requireSSL;
	}

	public String getSenderName() {
		return senderName;
	}

	public void setSenderName(String senderName) {
		this.senderName = senderName;
	}

	public String getSenderMail() {
		return senderMail;
	}

	public void setSenderMail(String senderMail) {
		this.senderMail = senderMail;
	}

	public String getServerIP() {
		return serverIP;
	}

	public void setServerIP(String serverIP) {
		this.serverIP = serverIP;
	}

	public int getServerPort() {
		return serverPort;
	}

	public void setServerPort(int serverPort) {
		this.serverPort = serverPort;
	}

	public boolean isAuthenticationRequired() {
		return authenticationRequired;
	}

	public void setAuthenticationRequired(boolean authenticationRequired) {
		this.authenticationRequired = authenticationRequired;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public String getAccountPassword() {
		return accountPassword;
	}

	public void setAccountPassword(String accountPassword) {
		this.accountPassword = accountPassword;
	}

	public boolean isProxyRequired() {
		return proxyRequired;
	}

	public void setProxyRequired(boolean proxyRequired) {
		this.proxyRequired = proxyRequired;
	}

	public String getProxyServerIP() {
		return proxyServerIP;
	}

	public void setProxyServerIP(String proxyServerIP) {
		this.proxyServerIP = proxyServerIP;
	}

	public int getProxyServerPort() {
		return proxyServerPort;
	}

	public void setProxyServerPort(int proxyServerPort) {
		this.proxyServerPort = proxyServerPort;
	}

	public boolean isProxyAuthenticationRequired() {
		return proxyAuthenticationRequired;
	}

	public void setProxyAuthenticationRequired(boolean proxyAuthenticationRequired) {
		this.proxyAuthenticationRequired = proxyAuthenticationRequired;
	}

	public String getProxyUsername() {
		return proxyUsername;
	}

	public void setProxyUsername(String proxyUsername) {
		this.proxyUsername = proxyUsername;
	}

	public String getProxyPassword() {
		return proxyPassword;
	}

	public void setProxyPassword(String proxyPassword) {
		this.proxyPassword = proxyPassword;
	}

	public String getSendProtocol() {
		return sendProtocol;
	}

	public void setSendProtocol(String sendProtocol) {
		this.sendProtocol = sendProtocol;
	}

	public String getReceiveProtocol() {
		return receiveProtocol;
	}

	public void setReceiveProtocol(String receiveProtocol) {
		this.receiveProtocol = receiveProtocol;
	}
}
