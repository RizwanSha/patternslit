package com.patterns.jobs.bean;

import java.io.Serializable;

public class TemplateBean implements Serializable {

	private static final long serialVersionUID = -2454219901243005424L;

	private String entityCode;
	private String templateCode;
	private String description;
	private String templateType;
	private String subjectEn;
	private String bodyEn;
	private String footerEn;

	public String getEntityCode() {
		return entityCode;
	}

	public void setEntityCode(String entityCode) {
		this.entityCode = entityCode;
	}

	public String getTemplateCode() {
		return templateCode;
	}

	public void setTemplateCode(String templateCode) {
		this.templateCode = templateCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTemplateType() {
		return templateType;
	}

	public void setTemplateType(String templateType) {
		this.templateType = templateType;
	}

	public String getSubjectEn() {
		return subjectEn;
	}

	public void setSubjectEn(String subjectEn) {
		this.subjectEn = subjectEn;
	}

	public String getBodyEn() {
		return bodyEn;
	}

	public void setBodyEn(String bodyEn) {
		this.bodyEn = bodyEn;
	}

	public String getFooterEn() {
		return footerEn;
	}

	public void setFooterEn(String footerEn) {
		this.footerEn = footerEn;
	}
}
