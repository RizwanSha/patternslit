package com.patterns.jobs;

import java.util.HashMap;
import java.util.Map;

import com.patterns.jobs.bean.JobParameterBean;
import com.patterns.jobs.bean.WorkerBean;

public class JobConfigurationContext {

	private static class JobConfigurationContextHelper {
		private static final JobConfigurationContext _INSTANCE = new JobConfigurationContext();
	}

	public static JobConfigurationContext getInstance() {
		return JobConfigurationContextHelper._INSTANCE;
	}

	private Map<String, WorkerBean> workerCollection = new HashMap<String, WorkerBean>();
	private Map<String, JobParameterBean> jobCollection = new HashMap<String, JobParameterBean>();

	public Map<String, WorkerBean> getWorkerCollection() {
		return workerCollection;
	}

	public void setWorkerCollection(Map<String, WorkerBean> workerCollection) {
		this.workerCollection = workerCollection;
	}

	public Map<String, JobParameterBean> getJobCollection() {
		return jobCollection;
	}

	public void setJobCollection(Map<String, JobParameterBean> jobCollection) {
		this.jobCollection = jobCollection;
	}
}
