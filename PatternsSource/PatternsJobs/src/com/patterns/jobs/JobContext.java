package com.patterns.jobs;

import java.util.List;

import patterns.config.framework.service.DTObject;

import com.patterns.jobs.bean.JobBean;
import com.patterns.jobs.bean.WorkerBean;

public class JobContext {

	private JobBean job;

	private DTObject jobData;

	private boolean entityJob;
	private String entityCode;

	private String cronExpression;

	private Integer maximumWorkers;

	private double numberOfRecords;
	private int retryTimeout;

	private List<WorkerBean> workerList;
	private WorkerBean workerInstance;

	public JobBean getJob() {
		return job;
	}

	public void setJob(JobBean job) {
		this.job = job;
	}

	public DTObject getJobData() {
		return jobData;
	}

	public void setJobData(DTObject jobData) {
		this.jobData = jobData;
	}

	public boolean isEntityJob() {
		return entityJob;
	}

	public void setEntityJob(boolean entityJob) {
		this.entityJob = entityJob;
	}

	public String getEntityCode() {
		return entityCode;
	}

	public void setEntityCode(String entityCode) {
		this.entityCode = entityCode;
	}

	public Integer getMaximumWorkers() {
		return maximumWorkers;
	}

	public void setMaximumWorkers(Integer maximumWorkers) {
		this.maximumWorkers = maximumWorkers;
	}

	public String getCronExpression() {
		return cronExpression;
	}

	public void setCronExpression(String cronExpression) {
		this.cronExpression = cronExpression;
	}

	public double getNumberOfRecords() {
		return numberOfRecords;
	}

	public void setNumberOfRecords(double numberOfRecords) {
		this.numberOfRecords = numberOfRecords;
	}

	public int getRetryTimeout() {
		return retryTimeout;
	}

	public void setRetryTimeout(int retryTimeout) {
		this.retryTimeout = retryTimeout;
	}

	public List<WorkerBean> getWorkerList() {
		return workerList;
	}

	public void setWorkerList(List<WorkerBean> workerList) {
		this.workerList = workerList;
	}

	public WorkerBean getWorkerInstance() {
		return workerInstance;
	}

	public void setWorkerInstance(WorkerBean workerInstance) {
		this.workerInstance = workerInstance;
	}
}