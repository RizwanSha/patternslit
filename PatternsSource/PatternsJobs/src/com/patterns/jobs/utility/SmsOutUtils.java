/*
 * Copyright (c) 2014, 2015, Patterns Software Design Institute. All
 * rights reserved. Unauthorized copying of this file, via any medium is
 * strictly prohibited. PATTERNS SDI PROPRIETARY/CONFIDENTIAL. Use is
 * subject to license terms.
 */
package com.patterns.jobs.utility;

import java.net.URLEncoder;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.exception.TBAFrameworkException;
import patterns.config.framework.loggers.ApplicationLogger;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.thread.ApplicationContext;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.patterns.jobs.application.monitor.ApplicationValidator;
import com.patterns.jobs.bean.SmsTextBean;

/**
 * The Class SmsOutUtils provides utility function for managing outgoing SMS
 * life cycle
 */
public class SmsOutUtils {

	/**
	 * The Class SmsContent.
	 */
	public static class SmsContent {

		/** The message id. */
		private String messageId;

		/** The text. */
		private String text;

		/** The source number. */
		private String sourceNumber;

		/** The destination number. */
		private String destinationNumber;

		/** The sent on. */
		private java.util.Date sentOn;

		/** The delivered on. */
		private java.util.Date deliveredOn;

		/** The delivery status. */
		private String deliveryStatus;

		/**
		 * Gets the message id.
		 * 
		 * @return the message id
		 */
		public String getMessageId() {
			return messageId;
		}

		/**
		 * Sets the message id.
		 * 
		 * @param messageId
		 *            the new message id
		 */
		public void setMessageId(String messageId) {
			this.messageId = messageId;
		}

		/**
		 * Gets the text.
		 * 
		 * @return the text
		 */
		public String getText() {
			return text;
		}

		/**
		 * Sets the text.
		 * 
		 * @param text
		 *            the new text
		 */
		public void setText(String text) {
			this.text = text;
		}

		/**
		 * Gets the source number.
		 * 
		 * @return the source number
		 */
		public String getSourceNumber() {
			return sourceNumber;
		}

		/**
		 * Sets the source number.
		 * 
		 * @param sourceNumber
		 *            the new source number
		 */
		public void setSourceNumber(String sourceNumber) {
			this.sourceNumber = sourceNumber;
		}

		/**
		 * Gets the destination number.
		 * 
		 * @return the destination number
		 */
		public String getDestinationNumber() {
			return destinationNumber;
		}

		/**
		 * Sets the destination number.
		 * 
		 * @param destinationNumber
		 *            the new destination number
		 */
		public void setDestinationNumber(String destinationNumber) {
			this.destinationNumber = destinationNumber;
		}

		/**
		 * Gets the sent on.
		 * 
		 * @return the sent on
		 */
		public Date getSentOn() {
			return sentOn;
		}

		/**
		 * Sets the sent on.
		 * 
		 * @param sentOn
		 *            the new sent on
		 */
		public void setSentOn(Date sentOn) {
			this.sentOn = sentOn;
		}

		/**
		 * Gets the delivered on.
		 * 
		 * @return the delivered on
		 */
		public Date getDeliveredOn() {
			return deliveredOn;
		}

		/**
		 * Sets the delivered on.
		 * 
		 * @param deliveredOn
		 *            the new delivered on
		 */
		public void setDeliveredOn(Date deliveredOn) {
			this.deliveredOn = deliveredOn;
		}

		/**
		 * Gets the delivery status.
		 * 
		 * @return the delivery status
		 */
		public String getDeliveryStatus() {
			return deliveryStatus;
		}

		/**
		 * Sets the delivery status.
		 * 
		 * @param deliveryStatus
		 *            the new delivery status
		 */
		public void setDeliveryStatus(String deliveryStatus) {
			this.deliveryStatus = deliveryStatus;
		}
	}

	/** The Constant MAX_RECORDS_BATCH. */
	public static final int MAX_RECORDS_BATCH = 10;

	/** The Constant MAX_THREADS. */
	public static final int MAX_THREADS = 16;

	public static final int MAX_SMS_RETRY = 3;

	/** The logger. */
	private final ApplicationLogger logger;

	/** The entity code. */
	private final String entityCode;

	/** The inventory list. */
	private final List<String> inventoryList;

	/** The list array. */
	private final List<List<SmsTextBean>> listArray = new LinkedList<List<SmsTextBean>>();

	/**
	 * Instantiates a new sms out utils.
	 * 
	 * @param entityCode
	 *            the entity code
	 * @param inventoryList
	 *            the inventory list
	 */
	public SmsOutUtils(String entityCode, List<String> inventoryList) {
		logger = ApplicationLogger.getInstance(this.getClass().getSimpleName());
		logger.logDebug("()");

		this.entityCode = entityCode;
		this.inventoryList = inventoryList;
	}

	/**
	 * Process.
	 * 
	 * @throws Exception
	 *             the exception
	 */
	public void process() throws Exception {
		logger.logDebug("process() Begin");

		long processInventoryStart = System.currentTimeMillis();
		processInventory();
		long processInventoryEnd = System.currentTimeMillis();
		logger.logDebug("process() processInventory Execution : " + (processInventoryEnd - processInventoryStart) + " ms");

		long distributeInventoryStart = System.currentTimeMillis();
		distributeRecords();
		long distributeInventoryEnd = System.currentTimeMillis();
		logger.logDebug("process() distributeInventory Execution : " + (distributeInventoryEnd - distributeInventoryStart) + " ms");

		logger.logDebug("process() End");
	}

	/**
	 * Process inventory.
	 * 
	 * @throws Exception
	 *             the exception
	 */
	private final void processInventory() throws Exception {
		logger.logDebug("processInventory() Begin");
		List<String> inventoryListLocal = new LinkedList<String>();
		DBContext dbContext = new DBContext();
		try {
			dbContext.setAutoCommit(false);
			boolean lockAcquired = false;

			for (String inventoryNumber : inventoryList) {
				// SmppSession smppSession =
				// SmppLifecycleManager.getInstance().acquireSmppSession(entityCode);
				// if (smppSession != null &&
				// smppSession.getSendSession().getSessionState().isBound()) {
				String lockQuery = "SELECT 1 FROM SMSOUTQPE WHERE ENTITY_CODE=? AND INVENTORY_NO=? FOR UPDATE";
				DBUtil lockSmsOutqPeUtil = dbContext.createUtilInstance();
				lockSmsOutqPeUtil.reset();
				lockSmsOutqPeUtil.setSql(lockQuery);
				lockSmsOutqPeUtil.setString(1, entityCode);
				lockSmsOutqPeUtil.setString(2, inventoryNumber);
				ResultSet rs = lockSmsOutqPeUtil.executeQuery();
				if (rs.next()) {
					lockAcquired = true;
				}

				if (lockAcquired) {
					String updateSmsOutqLogSql = "UPDATE SMSOUTQLOG SET STATUS='I' WHERE ENTITY_CODE=? AND INVENTORY_NO =?";
					DBUtil updateSmsOutqLogUtil = dbContext.createUtilInstance();
					updateSmsOutqLogUtil.reset();
					updateSmsOutqLogUtil.setSql(updateSmsOutqLogSql);
					updateSmsOutqLogUtil.setString(1, entityCode);
					updateSmsOutqLogUtil.setString(2, inventoryNumber);

					updateSmsOutqLogUtil.executeUpdate();
					updateSmsOutqLogUtil.reset();

					String deleteSmsInqPeSql = "DELETE FROM SMSOUTQPE WHERE ENTITY_CODE=? AND INVENTORY_NO =?";
					DBUtil deleteSmsInqPeUtil = dbContext.createUtilInstance();
					deleteSmsInqPeUtil.reset();
					deleteSmsInqPeUtil.setSql(deleteSmsInqPeSql);
					deleteSmsInqPeUtil.setString(1, entityCode);
					deleteSmsInqPeUtil.setString(2, inventoryNumber);
					int count = deleteSmsInqPeUtil.executeUpdate();
					deleteSmsInqPeUtil.reset();
					dbContext.commit();
					if (count == 1) {
						inventoryListLocal.add(inventoryNumber);
					}
				}
				// }
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError(e.getLocalizedMessage());
			try {
				dbContext.rollback();
			} catch (Exception re) {
				re.printStackTrace();
				logger.logError(re.getLocalizedMessage());
			}
		} finally {
			dbContext.close();
		}

		int currentRecordNumber = 1;
		List<SmsTextBean> list = new LinkedList<SmsTextBean>();
		for (String inventoryNumber : inventoryListLocal) {
			SmsTextBean smsTextBean = prepareSmsTextBean(inventoryNumber);
			if (smsTextBean != null) {
				list.add(smsTextBean);
				if (currentRecordNumber % MAX_RECORDS_BATCH == 0) {
					listArray.add(list);
					list = new LinkedList<SmsTextBean>();
				}
				++currentRecordNumber;
			}
		}
		if (currentRecordNumber != 1 && ((currentRecordNumber - 1) % MAX_RECORDS_BATCH != 0)) {
			listArray.add(list);
		}

		logger.logDebug("processInventory() End");
	}

	/**
	 * Prepare sms text bean.
	 * 
	 * @param inventoryNumber
	 *            the inventory number
	 * @return the sms text bean
	 */
	public SmsTextBean prepareSmsTextBean(String inventoryNumber) {
		logger.logDebug("prepareSmsTextBean() Begin");
		SmsTextBean smsTextBean = new SmsTextBean();
		smsTextBean.setInventoryNumber(inventoryNumber);
		DBContext dbContext = new DBContext();
		try {
			String smsOutqLogSql = "SELECT MOBILE_NUMBER,RES_STATUS,MESSAGE_CONTENT,INTERFACE_CODE FROM SMSOUTQLOG WHERE ENTITY_CODE=? AND INVENTORY_NO=?";
			DBUtil smsOutqLogUtil = dbContext.createUtilInstance();
			smsOutqLogUtil.setSql(smsOutqLogSql);
			smsOutqLogUtil.setString(1, entityCode);
			smsOutqLogUtil.setString(2, inventoryNumber);
			ResultSet customerInfoRSet = smsOutqLogUtil.executeQuery();
			if (customerInfoRSet.next()) {
				smsTextBean.setMobileNumber(customerInfoRSet.getString(1));
				smsTextBean.setStatus(customerInfoRSet.getString(2));
				smsTextBean.setMessageContent(customerInfoRSet.getString(3));
				smsTextBean.setSmsInterfaceCode(customerInfoRSet.getString(4));
			}
			smsOutqLogUtil.reset();
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError(e.getLocalizedMessage());
		} finally {
			dbContext.close();
		}
		logger.logDebug("prepareSmsTextBean() End");
		return smsTextBean;
	}

	/**
	 * Distribute records.
	 * 
	 * @throws Exception
	 *             the exception
	 */
	private final void distributeRecords() throws Exception {
		ExecutorService service = Executors.newFixedThreadPool(MAX_THREADS);

		List<Future<Boolean>> futures = new ArrayList<Future<Boolean>>();
		int batchId = 1;
		for (List<SmsTextBean> data : listArray) {
			Future<Boolean> f = service.submit(new SmsOutUtils.RecordBatchExecutor(entityCode, batchId, data));
			futures.add(f);
			++batchId;
		}

		boolean processStatus = true;
		for (Future<Boolean> f : futures) {
			try {
				if (!f.get()) {
					processStatus = false;
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
				logger.logError(e.getLocalizedMessage());
				throw new Exception("", e);
			} catch (ExecutionException e) {
				e.printStackTrace();
				logger.logError(e.getLocalizedMessage());
				throw new Exception("", e);
			}
		}
		if (!processStatus) {
			throw new Exception("");
		}
		service.shutdownNow();
	}

	/**
	 * The Class RecordBatchExecutor.
	 */
	public static class RecordBatchExecutor implements Callable<Boolean> {

		/** The entity code. */
		private final String entityCode;

		/** The batch id. */
		private final int batchId;

		/** The data. */
		private final List<SmsTextBean> data;

		/** The logger. */
		private final ApplicationLogger logger;

		/**
		 * Instantiates a new record batch executor.
		 * 
		 * @param entityCode
		 *            the entity code
		 * @param batchId
		 *            the batch id
		 * @param data
		 *            the data
		 */
		public RecordBatchExecutor(String entityCode, int batchId, List<SmsTextBean> data) {
			logger = ApplicationLogger.getInstance(this.getClass().getSimpleName());
			logger.logDebug("()");

			this.batchId = batchId;
			this.data = data;
			this.entityCode = entityCode;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.util.concurrent.Callable#call()
		 */
		@Override
		public Boolean call() {
			logger.logDebug("call() Start - Batch ID:" + batchId);
			Boolean processComplete = Boolean.FALSE;
			DBContext dbContext = new DBContext();
			// String errorCode =
			// SmppErrorCodes.SMS_VALIDATION_INVALID_CUSTOMER_E;
			String errorCode = "E";
			// boolean dispatchRequired = false;
			try {
				// DTObject installDTO = new DTObject();
				dbContext.setAutoCommit(false);
				String hasKey = getInstall(dbContext);//getting temporary hashkey value for sms gateway purpose
				for (SmsTextBean record : data) {
					// SmppSession smppSession =
					// SmppLifecycleManager.getInstance().acquireSmppSession(entityCode);
					boolean isSuccess = false;
					String messageId = RegularConstants.EMPTY_STRING;
					// if (CommonUtility.smppSessionAvailable(smppSession)) {
					// dispatchRequired = true;

					// messageId =
					// smppSession.sendMessage(record.getMobileNumber(),
					// record.getMessageContent(), new java.util.Date(),
					// RegularConstants.REGULAR_SMS);
					// if (messageId != null &&
					// !messageId.equals(RegularConstants.EMPTY_STRING)) {
					// isSuccess = true;
					// updateApplicationLog(record.getMobileNumber(),
					// record.getInventoryNumber(), record.getStatus());
					// } else {
					// isSuccess = false;
					// errorCode = SmppErrorCodes.SMS_DISPATCH_E;
					// }
					// if (dispatchRequired) {

					try {
						String textEncode = URLEncoder.encode(record.getMessageContent(), "UTF-8");

						HttpResponse<JsonNode> response = Unirest.get("https://webaroo-send-message-v1.p.mashape.com/sendMessage?message=" + textEncode + "&phone=" + record.getMobileNumber()).header("X-Mashape-Key", hasKey).asJson();

						System.out.println(response.getStatus());
						System.out.println(response.getStatusText());
						isSuccess = true;
					} catch (Exception e) {
						System.out.println(e);
						isSuccess = false;
					}
					if (isSuccess) {
						// String insertSmsInventorySql =
						// "INSERT INTO SMSDISPATCHINVENTORY (ENTITY_CODE,MESSAGE_ID,INVENTORY_NO,MESSAGE_TYPE) VALUES (?,?,?,?)";
						// DBUtil insertSmsInventoryUtil =
						// dbContext.createUtilInstance();
						// insertSmsInventoryUtil.reset();
						// insertSmsInventoryUtil.setSql(insertSmsInventorySql);
						// insertSmsInventoryUtil.setString(1, entityCode);
						// insertSmsInventoryUtil.setString(2, messageId);
						// insertSmsInventoryUtil.setString(3,
						// record.getInventoryNumber());
						// insertSmsInventoryUtil.setString(4, "");
						// insertSmsInventoryUtil.executeUpdate();
						// insertSmsInventoryUtil.reset();

						String updateSmsOutqLogSql = "UPDATE SMSOUTQLOG SET STATUS='S',SENT_TIME=?,MESSAGE_ID=? WHERE ENTITY_CODE=? AND INVENTORY_NO=?";
						DBUtil updateSmsOutqLogUtil = dbContext.createUtilInstance();
						updateSmsOutqLogUtil.reset();
						updateSmsOutqLogUtil.setSql(updateSmsOutqLogSql);
						updateSmsOutqLogUtil.setString(1, new java.sql.Timestamp(new java.util.Date().getTime()).toString());
						updateSmsOutqLogUtil.setString(2, messageId);
						updateSmsOutqLogUtil.setString(3, entityCode);
						updateSmsOutqLogUtil.setString(4, record.getInventoryNumber());
						updateSmsOutqLogUtil.executeUpdate();
						updateSmsOutqLogUtil.reset();

					} else {
						String updateSmsOutqLogSql = "UPDATE SMSOUTQLOG SET STATUS='F',RETRY_COUNT = (RETRY_COUNT + 1)  WHERE ENTITY_CODE=? AND INVENTORY_NO=?";
						DBUtil updateSmsOutqLogUtil = dbContext.createUtilInstance();
						updateSmsOutqLogUtil.reset();
						updateSmsOutqLogUtil.setSql(updateSmsOutqLogSql);
						updateSmsOutqLogUtil.setString(1, entityCode);
						updateSmsOutqLogUtil.setString(2, record.getInventoryNumber());

						updateSmsOutqLogUtil.executeUpdate();
						updateSmsOutqLogUtil.reset();

						String updateSmsErrorLogSql = "INSERT INTO SMSERRORLOG (ENTITY_CODE,INVENTORY_NO,RETRY_DATETIME,ERROR) VALUES (?,?,FN_GETCDT(?),?)";
						DBUtil updateSmsErrorLogUtil = dbContext.createUtilInstance();
						updateSmsErrorLogUtil.reset();
						updateSmsErrorLogUtil.setSql(updateSmsErrorLogSql);
						updateSmsErrorLogUtil.setString(1, entityCode);
						updateSmsErrorLogUtil.setString(2, record.getInventoryNumber());
						updateSmsErrorLogUtil.setString(3, entityCode);
						updateSmsErrorLogUtil.setString(4, errorCode);
						updateSmsErrorLogUtil.executeUpdate();
						updateSmsErrorLogUtil.reset();

						String smsoutqSql = "SELECT RETRY_COUNT FROM SMSOUTQLOG WHERE ENTITY_CODE=? AND INVENTORY_NO=?";
						DBUtil smsoutqUtil = dbContext.createUtilInstance();
						smsoutqUtil.reset();
						smsoutqUtil.setSql(smsoutqSql);
						smsoutqUtil.setString(1, entityCode);
						smsoutqUtil.setString(2, record.getInventoryNumber());

						ResultSet rset = smsoutqUtil.executeQuery();
						if (rset.next()) {
							int retryCount = rset.getInt(1);
							if (retryCount < MAX_SMS_RETRY) {
								String insertSmsOutSql = "INSERT INTO SMSOUTQPE (ENTITY_CODE, INVENTORY_NO) VALUES (?,?)";
								DBUtil insertSmsOutUtil = dbContext.createUtilInstance();
								insertSmsOutUtil.setSql(insertSmsOutSql);
								insertSmsOutUtil.setString(1, entityCode);
								insertSmsOutUtil.setString(2, record.getInventoryNumber());
								insertSmsOutUtil.executeUpdate();
								insertSmsOutUtil.reset();
							}
						}
						smsoutqUtil.reset();

					}
					// } else {
					// String updateSmsOutqLogSql =
					// "UPDATE SMSOUTQLOG SET STATUS='N' WHERE ENTITY_CODE=? AND INVENTORY_NO=?";
					// DBUtil updateSmsOutqLogUtil =
					// dbContext.createUtilInstance();
					// updateSmsOutqLogUtil.reset();
					// updateSmsOutqLogUtil.setSql(updateSmsOutqLogSql);
					// updateSmsOutqLogUtil.setString(1, entityCode);
					// updateSmsOutqLogUtil.setString(2,
					// record.getInventoryNumber());
					//
					// updateSmsOutqLogUtil.executeUpdate();
					// updateSmsOutqLogUtil.reset();
					// }
					// } else {
					// String insertSmsOutSql =
					// "INSERT INTO SMSOUTQPE (ENTITY_CODE, INVENTORY_NO) VALUES (?,?)";
					// DBUtil insertSmsOutUtil = dbContext.createUtilInstance();
					// insertSmsOutUtil.setSql(insertSmsOutSql);
					// insertSmsOutUtil.setString(1, entityCode);
					// insertSmsOutUtil.setString(2,
					// record.getInventoryNumber());
					// insertSmsOutUtil.executeUpdate();
					// insertSmsOutUtil.reset();
					// }
				}
				dbContext.commit();
				processComplete = Boolean.TRUE;
			} catch (Exception e) {
				e.printStackTrace();
				logger.logError(e.getLocalizedMessage());
				try {
					dbContext.rollback();
				} catch (Exception re) {
					re.printStackTrace();
					logger.logError(re.getLocalizedMessage());
				}
			} finally {
				dbContext.close();
			}

			logger.logDebug("call() End");
			return processComplete;
		}

		public String getInstall(DBContext dbContext) throws TBAFrameworkException {
			DBUtil util = dbContext.createUtilInstance();
			String sql = RegularConstants.EMPTY_STRING;
			String hashKey = RegularConstants.EMPTY_STRING;
			try {
				sql = "SELECT SMS_HASHKEY FROM INSTALL";
				util.reset();
				util.setMode(DBUtil.PREPARED);
				util.setSql(sql);
				ResultSet rset = util.executeQuery();
				if (rset.next()) {
					hashKey = rset.getString("SMS_HASHKEY");
				}
			} catch (Exception e) {
				e.printStackTrace();
				throw new TBAFrameworkException();
			} finally {
				util.reset();
			}
			return hashKey;
		}

		/**
		 * Update application log.
		 * 
		 * @param mobileNumber
		 *            the mobile number
		 * @param smsRequestInventoryNumber
		 *            the sms request inventory number
		 * @param isSuccess
		 *            the is success
		 */
		// private void updateApplicationLog(String mobileNumber, String
		// smsRequestInventoryNumber, String isSuccess) {
		// String countryCode = "91";
		// mobileNumber = mobileNumber.substring(countryCode.length());
		// final char[] mask = mobileNumber.toCharArray();
		// Arrays.fill(mask, 3, mobileNumber.length() - 3, 'X');
		// String maskedMobileNumber = new String(mask);
		// String status = "SUCCESS RESPONSE";
		// if (isSuccess.trim() != null && !isSuccess.equals("S")) {
		// status = "FAILURE RESPONSE";
		// }
		// logger.logDebug("=====================================================================================================");
		// logger.logDebug("RESPONSE MESSAGE SENT TO : " + maskedMobileNumber +
		// " : INVENTORY NO : " + smsRequestInventoryNumber + " STATUS :" +
		// status);
		// logger.logDebug("=====================================================================================================");
		// }
	}
}
