package com.patterns.jobs.utility;

import java.lang.reflect.Method;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.loggers.ApplicationLogger;
import patterns.config.framework.service.DTObject;

import com.patterns.jobs.application.validator.JobConstants;
import com.patterns.jobs.exceptions.JobsErrorCodes;
import com.patterns.jobs.exceptions.JobsException;

public class InwardRecordProcessorUtils {

	public static final int MAX_RECORDS_BATCH = 10;
	public static final int MAX_THREADS = 16;

	private final ApplicationLogger logger;

	private final String entityCode;
	private final List<String> inventoryList;

	private final List<List<DTObject>> listArray = new LinkedList<List<DTObject>>();

	public InwardRecordProcessorUtils(String entityCode, List<String> inventoryList) {
		logger = ApplicationLogger.getInstance(this.getClass().getSimpleName());
		logger.logDebug("()");

		this.entityCode = entityCode;
		this.inventoryList = inventoryList;
	}

	public void process() throws JobsException {
		logger.logDebug("process() Begin");

		long processInventoryStart = System.currentTimeMillis();
		processInventory();
		long processInventoryEnd = System.currentTimeMillis();
		logger.logDebug("process() processInventory Execution : " + (processInventoryEnd - processInventoryStart)
				+ " ms");

		long distributeInventoryStart = System.currentTimeMillis();
		distributeRecords();
		long distributeInventoryEnd = System.currentTimeMillis();
		logger.logDebug("process() distributeInventory Execution : "
				+ (distributeInventoryEnd - distributeInventoryStart) + " ms");

		logger.logDebug("process() End");
	}

	private final void processInventory() throws JobsException {
		logger.logDebug("processInventory() Begin");
		List<String> inventoryListLocal = new LinkedList<String>();
		DBContext dbContext = new DBContext();
		try {
			dbContext.setAutoCommit(false);
			boolean lockAcquired = false;
			for (String inventoryNumber : inventoryList) {
				String lockQuery = "SELECT * FROM CTJPROCINQPE WHERE ENTITY_CODE=? AND MSG_REF_ID=? FOR UPDATE";

				DBUtil lockInwardMessagePeUtil = dbContext.createUtilInstance();
				lockInwardMessagePeUtil.reset();
				lockInwardMessagePeUtil.setSql(lockQuery);
				lockInwardMessagePeUtil.setString(1, entityCode);
				lockInwardMessagePeUtil.setString(2, inventoryNumber);
				ResultSet rs = lockInwardMessagePeUtil.executeQuery();
				if (rs.next()) {
					lockAcquired = true;
				}

				if (lockAcquired) {
					String updateInwardMessageqLogSql = "UPDATE CTJINQREF SET MSG_STATUS='I' WHERE ENTITY_CODE=? AND MSG_REF_ID=?";
					DBUtil updateInwardMessageqLogUtil = dbContext.createUtilInstance();
					updateInwardMessageqLogUtil.reset();
					updateInwardMessageqLogUtil.setSql(updateInwardMessageqLogSql);
					updateInwardMessageqLogUtil.setString(1, entityCode);
					updateInwardMessageqLogUtil.setString(2, inventoryNumber);
					updateInwardMessageqLogUtil.executeUpdate();
					updateInwardMessageqLogUtil.reset();

					String deleteInwardMessagePeSql = "DELETE FROM CTJPROCINQPE WHERE ENTITY_CODE=? AND MSG_REF_ID=?";
					DBUtil deleteInwardMessagePeUtil = dbContext.createUtilInstance();
					deleteInwardMessagePeUtil.reset();
					deleteInwardMessagePeUtil.setSql(deleteInwardMessagePeSql);
					deleteInwardMessagePeUtil.setString(1, entityCode);
					deleteInwardMessagePeUtil.setString(2, inventoryNumber);
					int count = deleteInwardMessagePeUtil.executeUpdate();
					deleteInwardMessagePeUtil.reset();
					dbContext.commit();
					if (count == 1) {
						inventoryListLocal.add(inventoryNumber);
					}
				}
			}
		} catch (Exception e) {
			logger.logError("processPendingRecords() Exception 1" + e.getLocalizedMessage());
			try {
				dbContext.rollback();
			} catch (Exception re) {
				logger.logError("processPendingRecords() Exception 2" + re.getLocalizedMessage());
			}
		} finally {
			dbContext.close();
		}
		int currentRecordNumber = 1;
		List<DTObject> list = new LinkedList<DTObject>();
		for (String inventoryNumber : inventoryListLocal) {
			DTObject messageContent = processMessage(inventoryNumber);
			if (messageContent != null) {
				list.add(messageContent);
				if (currentRecordNumber % MAX_RECORDS_BATCH == 0) {
					listArray.add(list);
					list = new LinkedList<DTObject>();
				}
				++currentRecordNumber;
			}
		}
		if (currentRecordNumber != 1 && ((currentRecordNumber - 1) % MAX_RECORDS_BATCH != 0)) {
			listArray.add(list);
		}
		logger.logDebug("processInventory() End");
	}

	public DTObject processMessage(String referenceNumber) {
		logger.logDebug("parseMessageContent() Begin");
		DTObject messageContent = new DTObject();
		messageContent.set("REF_NUMBER", referenceNumber);
		DBContext dbContext = new DBContext();
		try {
			String readMessageCfgSql = "SELECT C.PROC_CODE,C.MPGM_ID FROM CTJMSGCONFIG C,CTJINQREF R WHERE R.ENTITY_CODE = ? AND R.MSG_REF_ID = ? AND C.ENTITY_CODE=R.ENTITY_CODE AND C.PGM_IDENTFR=R.MSG_PGM_IDENTFR";
			DBUtil readMessageCfgUtil = dbContext.createUtilInstance();
			readMessageCfgUtil.setSql(readMessageCfgSql);
			readMessageCfgUtil.setString(1, entityCode);
			readMessageCfgUtil.setString(2, referenceNumber);
			ResultSet readMessageCfgRset = readMessageCfgUtil.executeQuery();
			if (readMessageCfgRset.next()) {
				String processingCode = readMessageCfgRset.getString("PROC_CODE");
				String programId = readMessageCfgRset.getString("MPGM_ID");
				readMessageCfgUtil.reset();

				String readHandlerCfgSql = "SELECT PROCEDURE_NAME,CONTROLLER_CLASS FROM CTJPROCESSCFG WHERE ENTITY_CODE = ? AND PROC_CODE = ?";
				DBUtil readHandlerCfgUtil = dbContext.createUtilInstance();
				readHandlerCfgUtil.setSql(readHandlerCfgSql);
				readHandlerCfgUtil.setString(1, entityCode);
				readHandlerCfgUtil.setString(2, processingCode);
				ResultSet readHandlerCfgRset = readHandlerCfgUtil.executeQuery();
				if (readHandlerCfgRset.next()) {
					String procedureName = readHandlerCfgRset.getString("PROCEDURE_NAME");
					String controllerName = readHandlerCfgRset.getString("CONTROLLER_CLASS");

					readHandlerCfgUtil.reset();
					if (procedureName == null && controllerName == null) {
						messageContent.set("ERROR", JobsErrorCodes.MSG_HANDLER_UNAVAIL_E);
						messageContent.set("ERROR_DESC", JobsErrorCodes.MSG_HANDLER_UNAVAIL_EDESC);
						return messageContent;
					}
					messageContent.set("PROCEDURE_NAME", procedureName);
					messageContent.set("HANDLER_NAME", controllerName);
					messageContent.set("REF_NUMBER", referenceNumber);
					messageContent.set("PROCESSING_CODE", processingCode);
					messageContent.set("PROGRAM_ID", programId);
				} else {
					messageContent.set("ERROR", JobsErrorCodes.CFG_UNAVAIL_E);
					messageContent.set("ERROR_DESC", JobsErrorCodes.CFG_UNAVAIL_EDESC);
					return messageContent;
				}
			} else {
				messageContent.set("ERROR", JobsErrorCodes.CFG_UNAVAIL_E);
				messageContent.set("ERROR_DESC", JobsErrorCodes.CFG_UNAVAIL_EDESC);
				return messageContent;
			}
		} catch (Exception e) {
			logger.logError("parseMessageContent() Exception" + e.getLocalizedMessage());
		} finally {
			dbContext.close();
		}
		logger.logDebug("parseMessageContent() End");
		return messageContent;
	}

	private final void distributeRecords() throws JobsException {
		ExecutorService service = Executors.newFixedThreadPool(MAX_THREADS);

		List<Future<Boolean>> futures = new ArrayList<Future<Boolean>>();
		int batchId = 1;
		for (List<DTObject> data : listArray) {
			Future<Boolean> f = service.submit(new InwardRecordProcessorUtils.RecordBatchExecutor(entityCode, batchId,
					data));
			futures.add(f);
			++batchId;
		}

		boolean processStatus = true;
		for (Future<Boolean> f : futures) {
			try {
				if (!f.get()) {
					processStatus = false;
				}
			} catch (InterruptedException e) {
				logger.logError("distributeRecords() Exception 1" + e.getLocalizedMessage());
				throw new JobsException(JobsErrorCodes.EMAIL_PROCESS_THREADEXEC_INTR_E, e);
			} catch (ExecutionException e) {
				logger.logError("distributeRecords() Exception 2" + e.getLocalizedMessage());
				throw new JobsException(JobsErrorCodes.EMAIL_PROCESS_THREADEXEC_EXEC_E, e);
			}
		}
		if (!processStatus) {
			throw new JobsException(JobsErrorCodes.EMAIL_PROCESS_THREADEXEC_E);
		}
		service.shutdownNow();
	}

	public static class RecordBatchExecutor implements Callable<Boolean> {

		private final String entityCode;
		private final int batchId;
		private final List<DTObject> data;
		private final ApplicationLogger logger;

		public RecordBatchExecutor(String entityCode, int batchId, List<DTObject> data) {
			logger = ApplicationLogger.getInstance(this.getClass().getSimpleName());
			logger.logDebug("()");

			this.batchId = batchId;
			this.data = data;
			this.entityCode = entityCode;
		}

		@SuppressWarnings({ "unchecked", "rawtypes" })
		@Override
		public Boolean call() {
			logger.logDebug("call() Start - Batch ID:" + batchId);
			Boolean processComplete = Boolean.FALSE;
			DBContext dbContext = new DBContext();
			try {
				dbContext.setAutoCommit(false);
				boolean isError = false;
				for (DTObject record : data) {
					if (record.containsKey("ERROR")) {
						DBUtil updateReferenceUtil = dbContext.createUtilInstance();
						String updateReferenceqSql = "UPDATE CTJINQREF SET MSG_STATUS=?,MSG_ERR_CODE=?,MSG_ERR_DESCN=? WHERE ENTITY_CODE=? AND MSG_REF_ID=?";
						updateReferenceUtil.reset();
						updateReferenceUtil.setSql(updateReferenceqSql);
						updateReferenceUtil.setString(1, "F");
						updateReferenceUtil.setString(2, record.get("ERROR"));
						updateReferenceUtil.setString(3, record.get("ERROR_DESC"));
						updateReferenceUtil.setString(4, entityCode);
						updateReferenceUtil.setString(5, record.get("REF_NUMBER"));
						updateReferenceUtil.executeUpdate();
						updateReferenceUtil.reset();

						String updateInwardMessageqLogSql = "UPDATE CTJINQLOG SET STATUS='F' WHERE ENTITY_CODE=? AND MSG_REF_ID=?";
						DBUtil updateInwardMessageqLogUtil = dbContext.createUtilInstance();
						updateInwardMessageqLogUtil.reset();
						updateInwardMessageqLogUtil.setSql(updateInwardMessageqLogSql);
						updateInwardMessageqLogUtil.setString(1, entityCode);
						updateInwardMessageqLogUtil.setString(2, record.get("REF_NUMBER"));
						updateInwardMessageqLogUtil.executeUpdate();
						updateInwardMessageqLogUtil.reset();

						isError = true;

						logger.logDebug("=====MESSAGE PROCESSING FAILED -  " + record.get("REF_NUMBER") + "=====");
					} else {
						String procedureName = record.get("PROCEDURE_NAME");
						String controllerName = record.get("HANDLER_NAME");
						String referenceNumber = record.get("REF_NUMBER");
						String processingCode = record.get("PROCESSING_CODE");
						String programId = record.get("PROGRAM_ID");

						if (procedureName != null) {
							String sqlQuery = "CALL " + procedureName + "(?,?,?,?)";
							DBUtil dbUtil = dbContext.createUtilInstance();
							dbUtil.reset();
							dbUtil.setMode(DBUtil.CALLABLE);
							dbUtil.setSql(sqlQuery);
							dbUtil.setString(1, entityCode);
							dbUtil.setString(2, referenceNumber);
							dbUtil.setString(3, processingCode);
							dbUtil.setString(4, JobConstants.INWARD_MESSAGE);
							dbUtil.registerOutParameter(5, Types.VARCHAR);
							dbUtil.execute();

							String result = dbUtil.getString(5);
							if (result == null || !result.equals(RegularConstants.SP_SUCCESS)) {
								DBUtil updateReferenceUtil = dbContext.createUtilInstance();
								String updateReferenceqSql = "UPDATE CTJINQREF SET MSG_STATUS=?,MSG_ERR_CODE=?,MSG_ERR_DESCN=? WHERE ENTITY_CODE=? AND MSG_REF_ID=?";
								updateReferenceUtil.reset();
								updateReferenceUtil.setSql(updateReferenceqSql);
								updateReferenceUtil.setString(1, "F");
								updateReferenceUtil.setString(2, JobsErrorCodes.MSG_HANDLER_PROCCESSING_FAIL_E);
								updateReferenceUtil.setString(3, JobsErrorCodes.MSG_HANDLER_PROCCESSING_FAIL_EDESC);
								updateReferenceUtil.setString(4, entityCode);
								updateReferenceUtil.setString(5, referenceNumber);
								updateReferenceUtil.executeUpdate();
								updateReferenceUtil.reset();
								isError = true;
							}
						} else if (controllerName != null) {
							DTObject input = new DTObject();
							input.set("ENTITY_CODE", entityCode);
							input.set("REF_NUMBER", referenceNumber);
							input.set("PROCESSING_CODE", processingCode);
							input.set("MESSAGE_TYPE", JobConstants.INWARD_MESSAGE);
							input.set("PROGRAM_ID", programId);
							input.setObject("DBCONTEXT", dbContext);
							
							logger.logDebug("=====MESSAGE PROCESSING STARTED -  " + record.get("REF_NUMBER") + "=====");

							Class classObject = null;
							Object object = null;
							Method methodObject = null;
							Class[] parameterTypes = new Class[] { DTObject.class };
							Object[] params = new Object[] { input };
							try {
								classObject = Class.forName(controllerName);
								object = classObject.newInstance();
								methodObject = classObject.getMethod("execute", parameterTypes);
								methodObject.invoke(object, params);
							} catch (Exception e) {
								dbContext.rollback();
								DBUtil updateReferenceUtil = dbContext.createUtilInstance();
								String updateReferenceqSql = "UPDATE CTJINQREF SET MSG_STATUS=?,MSG_ERR_CODE=?,MSG_ERR_DESCN=? WHERE ENTITY_CODE=? AND MSG_REF_ID=?";
								updateReferenceUtil.reset();
								updateReferenceUtil.setSql(updateReferenceqSql);
								updateReferenceUtil.setString(1, "F");
								updateReferenceUtil.setString(2, JobsErrorCodes.MSG_HANDLER_PROCCESSING_FAIL_E);
								updateReferenceUtil.setString(3, JobsErrorCodes.MSG_HANDLER_PROCCESSING_FAIL_EDESC);
								updateReferenceUtil.setString(4, entityCode);
								updateReferenceUtil.setString(5, referenceNumber);
								updateReferenceUtil.executeUpdate();
								updateReferenceUtil.reset();
								isError = true;
							}
						}
						if (!isError) {
							DBUtil updateReferenceUtil = dbContext.createUtilInstance();
							String updateReferenceqSql = "UPDATE CTJINQREF SET MSG_STATUS=? WHERE ENTITY_CODE=? AND MSG_REF_ID=?";
							updateReferenceUtil.reset();
							updateReferenceUtil.setSql(updateReferenceqSql);
							updateReferenceUtil.setString(1, "S");
							updateReferenceUtil.setString(2, entityCode);
							updateReferenceUtil.setString(3, referenceNumber);
							updateReferenceUtil.executeUpdate();
							updateReferenceUtil.reset();

							String updateInwardMessageqLogSql = "UPDATE CTJINQLOG SET STATUS='S' WHERE ENTITY_CODE=? AND MSG_REF_ID=?";
							DBUtil updateInwardMessageqLogUtil = dbContext.createUtilInstance();
							updateInwardMessageqLogUtil.reset();
							updateInwardMessageqLogUtil.setSql(updateInwardMessageqLogSql);
							updateInwardMessageqLogUtil.setString(1, entityCode);
							updateInwardMessageqLogUtil.setString(2, referenceNumber);
							updateInwardMessageqLogUtil.executeUpdate();
							updateInwardMessageqLogUtil.reset();
							
							logger.logDebug("=====MESSAGE PROCESSING SUCCESS -  " + record.get("REF_NUMBER") + "=====");
						} else {
							String updateInwardMessageqLogSql = "UPDATE CTJINQLOG SET STATUS='F' WHERE ENTITY_CODE=? AND MSG_REF_ID=?";
							DBUtil updateInwardMessageqLogUtil = dbContext.createUtilInstance();
							updateInwardMessageqLogUtil.reset();
							updateInwardMessageqLogUtil.setSql(updateInwardMessageqLogSql);
							updateInwardMessageqLogUtil.setString(1, entityCode);
							updateInwardMessageqLogUtil.setString(2, record.get("REF_NUMBER"));
							updateInwardMessageqLogUtil.executeUpdate();
							updateInwardMessageqLogUtil.reset();
							
							logger.logDebug("=====MESSAGE PROCESSING FAILED -  " + record.get("REF_NUMBER") + "=====");
						}
					}
				}
				dbContext.commit();
				processComplete = Boolean.TRUE;
			} catch (Exception e) {
				logger.logError("call() Exception 1" + e.getLocalizedMessage());
				try {
					dbContext.rollback();
				} catch (Exception re) {
					logger.logError("call() Exception 2" + re.getLocalizedMessage());
				}
			} finally {
				dbContext.close();
			}

			logger.logDebug("call() End");
			return processComplete;
		}
	}
}
