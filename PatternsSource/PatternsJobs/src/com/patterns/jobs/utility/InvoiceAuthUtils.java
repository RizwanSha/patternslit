package com.patterns.jobs.utility;

import java.sql.Date;

public class InvoiceAuthUtils {
	private long entityCode;
	private Date grpEntryDate;
	private int grpEntrySl;
	private Date authEntryDate;
	private int authEntrySl;
	private long invoiceSL;
	
	
	public long getInvoiceSL() {
		return invoiceSL;
	}
	public void setInvoiceSL(long invoiceSL) {
		this.invoiceSL = invoiceSL;
	}
	public long getEntityCode() {
		return entityCode;
	}
	public void setEntityCode(long entityCode) {
		this.entityCode = entityCode;
	}
	public Date getGrpEntryDate() {
		return grpEntryDate;
	}
	public void setGrpEntryDate(Date grpEntryDate) {
		this.grpEntryDate = grpEntryDate;
	}
	public int getGrpEntrySl() {
		return grpEntrySl;
	}
	public void setGrpEntrySl(int grpEntrySl) {
		this.grpEntrySl = grpEntrySl;
	}
	
	public Date getAuthEntryDate() {
		return authEntryDate;
	}
	public void setAuthEntryDate(Date authEntryDate) {
		this.authEntryDate = authEntryDate;
	}
	public int getAuthEntrySl() {
		return authEntrySl;
	}
	public void setAuthEntrySl(int authEntrySl) {
		this.authEntrySl = authEntrySl;
	}
	

}
