package com.patterns.jobs.utility;

import patterns.config.framework.web.FormatUtils;

public class CommonUtility {
	public static String decodeBooleanToString(boolean value) {
		return FormatUtils.decodeBooleanToString(value);
	}

	public static boolean decodeStringToBoolean(String value) {
		return FormatUtils.decodeStringToBoolean(value);
	}
}
