package com.patterns.jobs.utility;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.loggers.ApplicationLogger;
import patterns.config.framework.service.DTObject;

import com.patterns.jobs.exceptions.JobsErrorCodes;
import com.patterns.jobs.exceptions.JobsException;

public class InwardMessageProcessorUtils {

	public static final int MAX_RECORDS_BATCH = 10;
	public static final int MAX_THREADS = 16;

	public static final int MDR_START_IDX = 0;
	public static final int MDR_END_IDX = 18;
	public static final int TOTAL_MSG_START_IDX = 18;
	public static final int TOTAL_MSG_END_IDX = 23;
	public static final int CUR_MSG_START_IDX = 23;
	public static final int CUR_MSG_END_IDX = 28;
	public static final int PROGRAM_START_IDX = 28;
	public static final int PROGRAM_END_IDX = 36;
	public static final int DATA_START_IDX = 36;

	private final ApplicationLogger logger;

	private final String entityCode;
	private final List<String> inventoryList;

	private final List<List<DTObject>> listArray = new LinkedList<List<DTObject>>();

	public InwardMessageProcessorUtils(String entityCode, List<String> inventoryList) {
		logger = ApplicationLogger.getInstance(this.getClass().getSimpleName());
		logger.logDebug("()");

		this.entityCode = entityCode;
		this.inventoryList = inventoryList;
	}

	public void process() throws JobsException {
		logger.logDebug("process() Begin");

		long processInventoryStart = System.currentTimeMillis();
		processInventory();
		long processInventoryEnd = System.currentTimeMillis();
		logger.logDebug("process() processInventory Execution : " + (processInventoryEnd - processInventoryStart) + " ms");

		long distributeInventoryStart = System.currentTimeMillis();
		distributeRecords();
		long distributeInventoryEnd = System.currentTimeMillis();
		logger.logDebug("process() distributeInventory Execution : " + (distributeInventoryEnd - distributeInventoryStart) + " ms");

		logger.logDebug("process() End");
	}

	private final void processInventory() throws JobsException {
		logger.logDebug("processInventory() Begin");
		List<String> inventoryListLocal = new LinkedList<String>();
		DBContext dbContext = new DBContext();
		try {
			dbContext.setAutoCommit(false);
			boolean lockAcquired = false;
			for (String inventoryNumber : inventoryList) {
				String lockQuery = "SELECT * FROM CTJINQPE WHERE ENTITY_CODE=? AND INVENTORY_NO=? FOR UPDATE";

				DBUtil lockInwardMessagePeUtil = dbContext.createUtilInstance();
				lockInwardMessagePeUtil.reset();
				lockInwardMessagePeUtil.setSql(lockQuery);
				lockInwardMessagePeUtil.setString(1, entityCode);
				lockInwardMessagePeUtil.setString(2, inventoryNumber);
				ResultSet rs = lockInwardMessagePeUtil.executeQuery();
				if (rs.next()) {
					lockAcquired = true;
				}

				if (lockAcquired) {
					String updateInwardMessageqLogSql = "UPDATE CTJINQLOG SET STATUS='I' WHERE ENTITY_CODE=? AND INVENTORY_NO=?";
					DBUtil updateInwardMessageqLogUtil = dbContext.createUtilInstance();
					updateInwardMessageqLogUtil.reset();
					updateInwardMessageqLogUtil.setSql(updateInwardMessageqLogSql);
					updateInwardMessageqLogUtil.setString(1, entityCode);
					updateInwardMessageqLogUtil.setString(2, inventoryNumber);
					updateInwardMessageqLogUtil.executeUpdate();
					updateInwardMessageqLogUtil.reset();

					String deleteInwardMessagePeSql = "DELETE FROM CTJINQPE WHERE ENTITY_CODE=? AND INVENTORY_NO=?";
					DBUtil deleteInwardMessagePeUtil = dbContext.createUtilInstance();
					deleteInwardMessagePeUtil.reset();
					deleteInwardMessagePeUtil.setSql(deleteInwardMessagePeSql);
					deleteInwardMessagePeUtil.setString(1, entityCode);
					deleteInwardMessagePeUtil.setString(2, inventoryNumber);
					int count = deleteInwardMessagePeUtil.executeUpdate();
					deleteInwardMessagePeUtil.reset();
					dbContext.commit();
					if (count == 1) {
						inventoryListLocal.add(inventoryNumber);
					}
				}
			}
		} catch (Exception e) {
			logger.logError("processPendingRecords() Exception 1" + e.getLocalizedMessage());
			try {
				dbContext.rollback();
			} catch (Exception re) {
				logger.logError("processPendingRecords() Exception 2" + re.getLocalizedMessage());
			}
		} finally {
			dbContext.close();
		}
		int currentRecordNumber = 1;
		List<DTObject> list = new LinkedList<DTObject>();
		for (String inventoryNumber : inventoryListLocal) {
			DTObject messageContent = parseMessageContent(inventoryNumber);
			if (messageContent != null) {
				list.add(messageContent);
				if (currentRecordNumber % MAX_RECORDS_BATCH == 0) {
					listArray.add(list);
					list = new LinkedList<DTObject>();
				}
				++currentRecordNumber;
			}
		}
		if (currentRecordNumber != 1 && ((currentRecordNumber - 1) % MAX_RECORDS_BATCH != 0)) {
			listArray.add(list);
		}
		logger.logDebug("processInventory() End");
	}

	public DTObject parseMessageContent(String inventoryNumber) {
		logger.logDebug("parseMessageContent() Begin");
		DTObject messageContent = new DTObject();
		DBContext dbContext = new DBContext();
		try {
			String inwardMessageqLogSql = "SELECT MSG_CONTENT FROM CTJINQLOG WHERE ENTITY_CODE = ? AND INVENTORY_NO = ?";
			DBUtil inwardMessageqLogUtil = dbContext.createUtilInstance();
			inwardMessageqLogUtil.setSql(inwardMessageqLogSql);
			inwardMessageqLogUtil.setString(1, entityCode);
			inwardMessageqLogUtil.setString(2, inventoryNumber);
			ResultSet inwardMessageqLogRset = inwardMessageqLogUtil.executeQuery();
			if (inwardMessageqLogRset.next()) {
				String content = inwardMessageqLogRset.getString("MSG_CONTENT");
				messageContent.set("INV_NUMBER", inventoryNumber);
				messageContent.set("MSG_REF_NO", content.substring(MDR_START_IDX, MDR_END_IDX));
				messageContent.set("TOTAL_BLOCKS", content.substring(TOTAL_MSG_START_IDX, TOTAL_MSG_END_IDX));
				messageContent.set("CURRENT_BLOCK", content.substring(CUR_MSG_START_IDX, CUR_MSG_END_IDX));
				messageContent.set("PROGRAM_ID", content.substring(PROGRAM_START_IDX, PROGRAM_END_IDX));
				messageContent.set("DATA", content.substring(DATA_START_IDX));
			}
			inwardMessageqLogUtil.reset();
		} catch (Exception e) {
			logger.logError("parseMessageContent() Exception" + e.getLocalizedMessage());
		} finally {
			dbContext.close();
		}
		logger.logDebug("parseMessageContent() End");
		return messageContent;
	}

	private final void distributeRecords() throws JobsException {
		ExecutorService service = Executors.newFixedThreadPool(MAX_THREADS);

		List<Future<Boolean>> futures = new ArrayList<Future<Boolean>>();
		int batchId = 1;
		for (List<DTObject> data : listArray) {
			Future<Boolean> f = service.submit(new InwardMessageProcessorUtils.RecordBatchExecutor(entityCode, batchId, data));
			futures.add(f);
			++batchId;
		}

		boolean processStatus = true;
		for (Future<Boolean> f : futures) {
			try {
				if (!f.get()) {
					processStatus = false;
				}
			} catch (InterruptedException e) {
				logger.logError("distributeRecords() Exception 1" + e.getLocalizedMessage());
				throw new JobsException(JobsErrorCodes.EMAIL_PROCESS_THREADEXEC_INTR_E, e);
			} catch (ExecutionException e) {
				logger.logError("distributeRecords() Exception 2" + e.getLocalizedMessage());
				throw new JobsException(JobsErrorCodes.EMAIL_PROCESS_THREADEXEC_EXEC_E, e);
			}
		}
		if (!processStatus) {
			throw new JobsException(JobsErrorCodes.EMAIL_PROCESS_THREADEXEC_E);
		}
		service.shutdownNow();
	}

	public static class RecordBatchExecutor implements Callable<Boolean> {

		private final String entityCode;
		private final int batchId;
		private final List<DTObject> data;
		private final ApplicationLogger logger;

		public RecordBatchExecutor(String entityCode, int batchId, List<DTObject> data) {
			logger = ApplicationLogger.getInstance(this.getClass().getSimpleName());
			logger.logDebug("()");

			this.batchId = batchId;
			this.data = data;
			this.entityCode = entityCode;
		}

		@Override
		public Boolean call() {
			logger.logDebug("call() Start - Batch ID:" + batchId);
			Boolean processComplete = Boolean.FALSE;
			DBContext dbContext = new DBContext();
			try {
				dbContext.setAutoCommit(false);
				for (DTObject record : data) {
					BigDecimal referenceNumber = new BigDecimal(record.get("MSG_REF_NO"));
					boolean isNewMessage = true;
					int receivedBlocks = 0, totalBlockes = Integer.parseInt(record.get("TOTAL_BLOCKS"));
					String readReferenceqLogSql = "SELECT MSG_BLOCK_SEQ FROM CTJINQREF WHERE ENTITY_CODE=? AND MSG_REF_ID=?";
					DBUtil readReferenceqLogUtil = dbContext.createUtilInstance();
					readReferenceqLogUtil.reset();
					readReferenceqLogUtil.setSql(readReferenceqLogSql);
					readReferenceqLogUtil.setString(1, entityCode);
					readReferenceqLogUtil.setString(2, referenceNumber.toString());
					ResultSet readReferenceqLogRSet = readReferenceqLogUtil.executeQuery();
					if (readReferenceqLogRSet.next()) {
						receivedBlocks = readReferenceqLogRSet.getInt(1);
						if (receivedBlocks > 0) {
							isNewMessage = false;
						}
					} else {
						isNewMessage = true;
					}
					readReferenceqLogUtil.reset();

					if (isNewMessage) {
						String insertReferenceqLogSql = "INSERT INTO CTJINQREF (ENTITY_CODE,MSG_REF_ID,MSG_BLOCK_COUNT,MSG_BLOCK_SEQ,MSG_PGM_IDENTFR,MSG_STATUS) VALUES (?,?,?,?,?,?)";
						DBUtil insertReferenceqLogUtil = dbContext.createUtilInstance();
						insertReferenceqLogUtil.reset();
						insertReferenceqLogUtil.setSql(insertReferenceqLogSql);
						insertReferenceqLogUtil.setString(1, entityCode);
						insertReferenceqLogUtil.setString(2, referenceNumber.toString());
						insertReferenceqLogUtil.setInt(3, totalBlockes);
						insertReferenceqLogUtil.setString(4, record.get("CURRENT_BLOCK"));
						insertReferenceqLogUtil.setString(5, record.get("PROGRAM_ID"));
						insertReferenceqLogUtil.setString(6, "N");
						insertReferenceqLogUtil.executeUpdate();
						insertReferenceqLogUtil.reset();
					} else {
						String insertReferenceqLogSql = "UPDATE CTJINQREF SET MSG_BLOCK_SEQ =? WHERE ENTITY_CODE = ? AND MSG_REF_ID=?";
						DBUtil insertReferenceqLogUtil = dbContext.createUtilInstance();
						insertReferenceqLogUtil.reset();
						insertReferenceqLogUtil.setSql(insertReferenceqLogSql);
						insertReferenceqLogUtil.setInt(1, (receivedBlocks + 1));
						insertReferenceqLogUtil.setString(2, entityCode);
						insertReferenceqLogUtil.setString(3, referenceNumber.toString());
						insertReferenceqLogUtil.executeUpdate();
						insertReferenceqLogUtil.reset();
					}
					String updateInwardMessageqLogSql = "UPDATE CTJINQLOG SET MSG_REF_ID=? WHERE ENTITY_CODE=? AND INVENTORY_NO=?";
					DBUtil updateInwardMessageqLogUtil = dbContext.createUtilInstance();
					updateInwardMessageqLogUtil.reset();
					updateInwardMessageqLogUtil.setSql(updateInwardMessageqLogSql);
					updateInwardMessageqLogUtil.setString(1, referenceNumber.toString());
					updateInwardMessageqLogUtil.setString(2, entityCode);
					updateInwardMessageqLogUtil.setString(3, record.get("INV_NUMBER"));
					updateInwardMessageqLogUtil.executeUpdate();
					updateInwardMessageqLogUtil.reset();
					receivedBlocks++;

					String insertBlockSql = "INSERT INTO CTJBLOCK (ENTITY_CODE,MSG_REF_ID,MSG_BLOCK_SEQ,MSG_DATA_CONTENT,INVENTORY_NO) VALUES (?,?,?,?,?)";
					DBUtil insertBlockUtil = dbContext.createUtilInstance();
					insertBlockUtil.reset();
					insertBlockUtil.setSql(insertBlockSql);
					insertBlockUtil.setString(1, entityCode);
					insertBlockUtil.setString(2, referenceNumber.toString());
					insertBlockUtil.setString(3, record.get("CURRENT_BLOCK"));
					insertBlockUtil.setString(4, record.get("DATA"));
					insertBlockUtil.setString(5, record.get("INV_NUMBER"));
					insertBlockUtil.executeUpdate();
					insertBlockUtil.reset();

					if (receivedBlocks == totalBlockes) {
						String insertProcPESql = "INSERT INTO CTJPROCINQPE (ENTITY_CODE,MSG_REF_ID) VALUES (?,?)";
						DBUtil insertProcPEUtil = dbContext.createUtilInstance();
						insertProcPEUtil.reset();
						insertProcPEUtil.setSql(insertProcPESql);
						insertProcPEUtil.setString(1, entityCode);
						insertProcPEUtil.setString(2, referenceNumber.toString());
						insertProcPEUtil.executeUpdate();
						insertProcPEUtil.reset();
					}
				}
				dbContext.commit();
				processComplete = Boolean.TRUE;
			} catch (Exception e) {
				logger.logError("call() Exception 1" + e.getLocalizedMessage());
				try {
					dbContext.rollback();
				} catch (Exception re) {
					logger.logError("call() Exception 2" + re.getLocalizedMessage());
				}
			} finally {
				dbContext.close();
			}

			logger.logDebug("call() End");
			return processComplete;
		}
	}
}
