package com.patterns.jobs.utility;

import java.io.File;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.apache.commons.io.FileUtils;

import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.loggers.ApplicationLogger;

import com.patterns.jobs.bean.EmailServerBean;
import com.patterns.jobs.bean.loader.EmailServerLoader;
import com.patterns.jobs.exceptions.JobsErrorCodes;
import com.patterns.jobs.exceptions.JobsException;
import com.patterns.jobs.utility.EmailUtils.EmailContent;

public class EmailOutUtils {

	public static final int MAX_RECORDS_BATCH = 10;
	public static final int MAX_THREADS = 16;

	private final ApplicationLogger logger;

	private final String entityCode;
	private final List<String> inventoryList;

	private final List<List<EmailContent>> listArray = new LinkedList<List<EmailContent>>();

	public EmailOutUtils(String entityCode, List<String> inventoryList) {
		logger = ApplicationLogger.getInstance(this.getClass().getSimpleName());
		logger.logDebug("()");

		this.entityCode = entityCode;
		this.inventoryList = inventoryList;
	}

	public void process() throws JobsException {
		logger.logDebug("process() Begin");

		long processInventoryStart = System.currentTimeMillis();
		processInventory();
		long processInventoryEnd = System.currentTimeMillis();
		logger.logDebug("process() processInventory Execution : " + (processInventoryEnd - processInventoryStart) + " ms");

		long distributeInventoryStart = System.currentTimeMillis();
		distributeRecords();
		long distributeInventoryEnd = System.currentTimeMillis();
		logger.logDebug("process() distributeInventory Execution : " + (distributeInventoryEnd - distributeInventoryStart) + " ms");

		logger.logDebug("process() End");
	}

	private final void processInventory() throws JobsException {
		logger.logDebug("processInventory() Begin");
		List<String> inventoryListLocal = new LinkedList<String>();
		DBContext dbContext = new DBContext();
		try {
			dbContext.setAutoCommit(false);
			boolean lockAcquired = false;
			for (String inventoryNumber : inventoryList) {
				String lockQuery = "SELECT * FROM EMAILOUTQPE WHERE ENTITY_CODE=? AND INVENTORY_NO=? FOR UPDATE";

				DBUtil lockEmailOutqPeUtil = dbContext.createUtilInstance();
				lockEmailOutqPeUtil.reset();
				lockEmailOutqPeUtil.setSql(lockQuery);
				lockEmailOutqPeUtil.setString(1, entityCode);
				lockEmailOutqPeUtil.setString(2, inventoryNumber);
				ResultSet rs = lockEmailOutqPeUtil.executeQuery();
				if (rs.next()) {
					lockAcquired = true;
				}

				if (lockAcquired) {
					String updateSmsOutqLogSql = "UPDATE EMAILOUTQLOG SET STATUS='I' WHERE ENTITY_CODE=? AND INVENTORY_NO=?";
					DBUtil updateSmsOutqLogUtil = dbContext.createUtilInstance();
					updateSmsOutqLogUtil.reset();
					updateSmsOutqLogUtil.setSql(updateSmsOutqLogSql);
					updateSmsOutqLogUtil.setString(1, entityCode);
					updateSmsOutqLogUtil.setString(2, inventoryNumber);

					updateSmsOutqLogUtil.executeUpdate();
					updateSmsOutqLogUtil.reset();

					String deleteSmsInqPeSql = "DELETE FROM EMAILOUTQPE WHERE ENTITY_CODE=? AND INVENTORY_NO=?";
					DBUtil deleteSmsInqPeUtil = dbContext.createUtilInstance();
					deleteSmsInqPeUtil.reset();
					deleteSmsInqPeUtil.setSql(deleteSmsInqPeSql);
					deleteSmsInqPeUtil.setString(1, entityCode);
					deleteSmsInqPeUtil.setString(2, inventoryNumber);
					int count = deleteSmsInqPeUtil.executeUpdate();
					deleteSmsInqPeUtil.reset();
					dbContext.commit();
					if (count == 1) {
						inventoryListLocal.add(inventoryNumber);
					}
				}
			}
		} catch (Exception e) {
			logger.logError("processPendingRecords() Exception 1" + e.getLocalizedMessage());
			try {
				dbContext.rollback();
			} catch (Exception re) {
				logger.logError("processPendingRecords() Exception 2" + re.getLocalizedMessage());
			}
		} finally {
			dbContext.close();
		}
		int currentRecordNumber = 1;
		List<EmailContent> list = new LinkedList<EmailContent>();
		for (String inventoryNumber : inventoryListLocal) {
			EmailContent emailContentBean = prepareMailContentBean(inventoryNumber);
			if (emailContentBean != null) {
				list.add(emailContentBean);
				if (currentRecordNumber % MAX_RECORDS_BATCH == 0) {
					listArray.add(list);
					list = new LinkedList<EmailContent>();
				}
				++currentRecordNumber;
			}
		}
		if (currentRecordNumber != 1 && ((currentRecordNumber - 1) % MAX_RECORDS_BATCH != 0)) {
			listArray.add(list);
		}
		logger.logDebug("processInventory() End");
	}

	public EmailContent prepareMailContentBean(String inventoryNumber) {
		logger.logDebug("prepareEmailContent() Begin");
		EmailContent emailContent = new EmailContent();
		DBContext dbContext = new DBContext();
		try {
			String emailOutqLogSql = "SELECT REQ_DATE,EMAIL_SUBJECT,EMAIL_TEXT,APPL_CODE FROM EMAILOUTQLOG WHERE ENTITY_CODE = ? AND INVENTORY_NO = ?";
			DBUtil emailOutqLogUtil = dbContext.createUtilInstance();
			emailOutqLogUtil.setSql(emailOutqLogSql);
			emailOutqLogUtil.setString(1, entityCode);
			emailOutqLogUtil.setString(2, inventoryNumber);
			ResultSet smsOutqLogRset = emailOutqLogUtil.executeQuery();
			if (smsOutqLogRset.next()) {
				emailContent.setInventoryNumber(inventoryNumber);
				emailContent.setRequestDate(smsOutqLogRset.getTimestamp(1));
				emailContent.setSubject(smsOutqLogRset.getString(2));
				emailContent.setText(smsOutqLogRset.getString(3));
				emailContent.setEmailInterfaceCode(smsOutqLogRset.getString(4));
				String emailRcpSql = "SELECT EMAIL_RCP_TYPE,EMAIL_ID FROM EMAILOUTQRCP WHERE ENTITY_CODE = ? AND INVENTORY_NO = ?";
				DBUtil emailRcpUtil = dbContext.createUtilInstance();
				emailRcpUtil.setMode(DBUtil.PREPARED);
				emailRcpUtil.setSql(emailRcpSql);
				emailRcpUtil.setString(1, entityCode);
				emailRcpUtil.setString(2, inventoryNumber);
				ResultSet emailRcpRset = emailRcpUtil.executeQuery();
				List<String> toList = new LinkedList<String>();
				List<String> ccList = new LinkedList<String>();
				List<String> bccList = new LinkedList<String>();
				while (emailRcpRset.next()) {
					String rcpType = emailRcpRset.getString(1);
					String emailID = emailRcpRset.getString(2);
					if (rcpType != null) {
						if (rcpType.equals("T")) {
							toList.add(emailID);
						} else if (rcpType.equals("C")) {
							ccList.add(emailID);
						} else if (rcpType.equals("B")) {
							bccList.add(emailID);
						}
					}
				}
				emailRcpUtil.reset();
				String[] toListArray = new String[toList.size()];
				toList.toArray(toListArray);
				emailContent.setToList(toListArray);
				String[] ccListArray = new String[ccList.size()];
				ccList.toArray(ccListArray);
				emailContent.setCcList(ccListArray);
				String[] bccListArray = new String[bccList.size()];
				bccList.toArray(bccListArray);
				emailContent.setBccList(bccListArray);
				
				String emailAttachSql = "SELECT ATTACHMENT_NAME,ATTACHMENT_PATH FROM EMAILOUTQATTACH WHERE ENTITY_CODE = ? AND INVENTORY_NO = ?";
				DBUtil emailAttachUtil = dbContext.createUtilInstance();
				emailAttachUtil.setMode(DBUtil.PREPARED);
				emailAttachUtil.setSql(emailAttachSql);
				emailAttachUtil.setString(1, entityCode);
				emailAttachUtil.setString(2, inventoryNumber);
				ResultSet emailAttachRset = emailAttachUtil.executeQuery();
				Map<String, byte[]> attachmentList = new HashMap<String, byte[]>();
				while (emailAttachRset.next()) {
					String attachment = emailAttachRset.getString("ATTACHMENT_PATH") + emailAttachRset.getString("ATTACHMENT_NAME");
					
					byte[] attachementContent = FileUtils.readFileToByteArray(new File(attachment));
					attachmentList.put(emailAttachRset.getString("ATTACHMENT_NAME"), attachementContent);
				}
				emailAttachUtil.reset();
				emailContent.setAttachList(attachmentList);
			}
			emailOutqLogUtil.reset();
		} catch (Exception e) {
			logger.logError("prepareEmailContent() Exception" + e.getLocalizedMessage());
		} finally {
			dbContext.close();
		}

		logger.logDebug("prepareEmailContent() End");
		return emailContent;
	}

	private final void distributeRecords() throws JobsException {
		ExecutorService service = Executors.newFixedThreadPool(MAX_THREADS);

		List<Future<Boolean>> futures = new ArrayList<Future<Boolean>>();
		int batchId = 1;
		for (List<EmailContent> data : listArray) {
			Future<Boolean> f = service.submit(new EmailOutUtils.RecordBatchExecutor(entityCode, batchId, data));
			futures.add(f);
			++batchId;
		}

		boolean processStatus = true;
		for (Future<Boolean> f : futures) {
			try {
				if (!f.get()) {
					processStatus = false;
				}
			} catch (InterruptedException e) {
				logger.logError("distributeRecords() Exception 1" + e.getLocalizedMessage());
				throw new JobsException(JobsErrorCodes.EMAIL_PROCESS_THREADEXEC_INTR_E, e);
			} catch (ExecutionException e) {
				logger.logError("distributeRecords() Exception 2" + e.getLocalizedMessage());
				throw new JobsException(JobsErrorCodes.EMAIL_PROCESS_THREADEXEC_EXEC_E, e);
			}
		}
		if (!processStatus) {
			throw new JobsException(JobsErrorCodes.EMAIL_PROCESS_THREADEXEC_E);
		}
		service.shutdownNow();
	}

	public static class RecordBatchExecutor implements Callable<Boolean> {

		private final String entityCode;
		private final int batchId;
		private final List<EmailContent> data;
		private final ApplicationLogger logger;

		public RecordBatchExecutor(String entityCode, int batchId, List<EmailContent> data) {
			logger = ApplicationLogger.getInstance(this.getClass().getSimpleName());
			logger.logDebug("()");

			this.batchId = batchId;
			this.data = data;
			this.entityCode = entityCode;
		}

		@Override
		public Boolean call() {
			logger.logDebug("call() Start - Batch ID:" + batchId);
			Boolean processComplete = Boolean.FALSE;
			DBContext dbContext = new DBContext();
			try {
				dbContext.setAutoCommit(false);
				for (EmailContent record : data) {
					EmailServerBean emailServer = new EmailServerLoader(entityCode, record.getEmailInterfaceCode()).getEmailServer();
					EmailUtils emailUtils = new EmailUtils(emailServer);
					boolean isSuccess = false;
					if (emailServer != null) {
						if (emailUtils.sendEmail(record)) {
							isSuccess = true;
						}
					}
					if (isSuccess) {
						String updateEmailOutqLogSql = "UPDATE EMAILOUTQLOG SET STATUS='S',SENT_DATE=? WHERE ENTITY_CODE=? AND INVENTORY_NO=?";
						DBUtil updateEmailOutqLogUtil = dbContext.createUtilInstance();
						updateEmailOutqLogUtil.reset();
						updateEmailOutqLogUtil.setSql(updateEmailOutqLogSql);
						updateEmailOutqLogUtil.setString(1, new java.sql.Timestamp(new java.util.Date().getTime()).toString());
						
						updateEmailOutqLogUtil.setString(2, entityCode);
						updateEmailOutqLogUtil.setString(3, record.getInventoryNumber());

						updateEmailOutqLogUtil.executeUpdate();
						updateEmailOutqLogUtil.reset();
					} else {
						String updateEmailOutqLogSql = "UPDATE EMAILOUTQLOG SET STATUS='F' WHERE ENTITY_CODE=? AND INVENTORY_NO=?";
						DBUtil updateEmailOutqLogUtil = dbContext.createUtilInstance();
						updateEmailOutqLogUtil.reset();
						updateEmailOutqLogUtil.setSql(updateEmailOutqLogSql);
						updateEmailOutqLogUtil.setString(1, entityCode);
						updateEmailOutqLogUtil.setString(2, record.getInventoryNumber());

						updateEmailOutqLogUtil.executeUpdate();
						updateEmailOutqLogUtil.reset();
					}
				}
				dbContext.commit();
				processComplete = Boolean.TRUE;
			} catch (Exception e) {
				logger.logError("call() Exception 1" + e.getLocalizedMessage());
				try {
					dbContext.rollback();
				} catch (Exception re) {
					logger.logError("call() Exception 2" + re.getLocalizedMessage());
				}
			} finally {
				dbContext.close();
			}

			logger.logDebug("call() End");
			return processComplete;
		}
	}
}
