package com.patterns.jobs.utility;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.loggers.ApplicationLogger;
import patterns.config.framework.service.DTObject;

import com.patterns.interfaces.Utility;
import com.patterns.interfaces.mq.MQCommunicator;
import com.patterns.interfaces.mq.QueueBean;
import com.patterns.jobs.exceptions.JobsErrorCodes;
import com.patterns.jobs.exceptions.JobsException;

public class OutwardMessageDispatchUtils {

	public static final int MAX_RECORDS_BATCH = 10;
	public static final int MAX_THREADS = 16;

	public static final int MDR_START_IDX = 0;
	public static final int MDR_END_IDX = 18;
	public static final int TOTAL_MSG_START_IDX = 18;
	public static final int TOTAL_MSG_END_IDX = 23;
	public static final int CUR_MSG_START_IDX = 23;
	public static final int CUR_MSG_END_IDX = 28;
	public static final int PROGRAM_START_IDX = 28;
	public static final int PROGRAM_END_IDX = 36;
	public static final int DATA_START_IDX = 36;

	private final ApplicationLogger logger;

	private final String entityCode;
	private final List<String> inventoryList;

	private final List<List<DTObject>> listArray = new LinkedList<List<DTObject>>();

	public OutwardMessageDispatchUtils(String entityCode, List<String> inventoryList) {
		logger = ApplicationLogger.getInstance(this.getClass().getSimpleName());
		logger.logDebug("()");

		this.entityCode = entityCode;
		this.inventoryList = inventoryList;
	}

	public void process() throws JobsException {
		logger.logDebug("process() Begin");

		long processInventoryStart = System.currentTimeMillis();
		processInventory();
		long processInventoryEnd = System.currentTimeMillis();
		logger.logDebug("process() processInventory Execution : " + (processInventoryEnd - processInventoryStart) + " ms");

		long distributeInventoryStart = System.currentTimeMillis();
		distributeRecords();
		long distributeInventoryEnd = System.currentTimeMillis();
		logger.logDebug("process() distributeInventory Execution : " + (distributeInventoryEnd - distributeInventoryStart) + " ms");

		logger.logDebug("process() End");
	}

	private final void processInventory() throws JobsException {
		logger.logDebug("processInventory() Begin");
		List<String> inventoryListLocal = new LinkedList<String>();
		DBContext dbContext = new DBContext();
		try {
			dbContext.setAutoCommit(false);
			boolean lockAcquired = false;
			for (String inventoryNumber : inventoryList) {
				String lockQuery = "SELECT * FROM JTCOUTPE WHERE ENTITY_CODE=? AND MSG_REF_ID=? FOR UPDATE";

				DBUtil lockOutwardMessagePeUtil = dbContext.createUtilInstance();
				lockOutwardMessagePeUtil.reset();
				lockOutwardMessagePeUtil.setSql(lockQuery);
				lockOutwardMessagePeUtil.setString(1, entityCode);
				lockOutwardMessagePeUtil.setString(2, inventoryNumber);
				ResultSet rs = lockOutwardMessagePeUtil.executeQuery();
				if (rs.next()) {
					lockAcquired = true;
				}

				if (lockAcquired) {
					String updateOutwardMessageqLogSql = "UPDATE JTCOUTQREF SET STATUS='I' WHERE ENTITY_CODE=? AND MSG_REF_ID=?";
					DBUtil updateInwardMessageqLogUtil = dbContext.createUtilInstance();
					updateInwardMessageqLogUtil.reset();
					updateInwardMessageqLogUtil.setSql(updateOutwardMessageqLogSql);
					updateInwardMessageqLogUtil.setString(1, entityCode);
					updateInwardMessageqLogUtil.setString(2, inventoryNumber);
					updateInwardMessageqLogUtil.executeUpdate();
					updateInwardMessageqLogUtil.reset();

					String deleteInwardMessagePeSql = "DELETE FROM JTCOUTPE WHERE ENTITY_CODE=? AND MSG_REF_ID=?";
					DBUtil deleteInwardMessagePeUtil = dbContext.createUtilInstance();
					deleteInwardMessagePeUtil.reset();
					deleteInwardMessagePeUtil.setSql(deleteInwardMessagePeSql);
					deleteInwardMessagePeUtil.setString(1, entityCode);
					deleteInwardMessagePeUtil.setString(2, inventoryNumber);
					int count = deleteInwardMessagePeUtil.executeUpdate();
					deleteInwardMessagePeUtil.reset();
					dbContext.commit();
					if (count == 1) {
						inventoryListLocal.add(inventoryNumber);
					}
				}
			}
		} catch (Exception e) {
			logger.logError("processPendingRecords() Exception 1" + e.getLocalizedMessage());
			try {
				dbContext.rollback();
			} catch (Exception re) {
				logger.logError("processPendingRecords() Exception 2" + re.getLocalizedMessage());
			}
		} finally {
			dbContext.close();
		}
		int currentRecordNumber = 1;
		List<DTObject> list = new LinkedList<DTObject>();
		for (String inventoryNumber : inventoryListLocal) {
			DTObject messageContent = parseMessageContent(inventoryNumber);
			if (messageContent != null) {
				list.add(messageContent);
				if (currentRecordNumber % MAX_RECORDS_BATCH == 0) {
					listArray.add(list);
					list = new LinkedList<DTObject>();
				}
				++currentRecordNumber;
			}
		}
		if (currentRecordNumber != 1 && ((currentRecordNumber - 1) % MAX_RECORDS_BATCH != 0)) {
			listArray.add(list);
		}
		logger.logDebug("processInventory() End");
	}

	public DTObject parseMessageContent(String inventoryNumber) {
		logger.logDebug("parseMessageContent() Begin");
		DTObject messageContent = new DTObject();
		DBContext dbContext = new DBContext();
		List<String> messageList = new LinkedList<String>();
		try {
			String outwardMessageqLogSql = "SELECT MSG_DATA_CONTENT FROM JTCBLOCK WHERE ENTITY_CODE = ? AND MSG_REF_ID = ? ORDER BY MSG_BLOCK_SEQ";
			DBUtil outwardMessageqLogUtil = dbContext.createUtilInstance();
			outwardMessageqLogUtil.setSql(outwardMessageqLogSql);
			outwardMessageqLogUtil.setString(1, entityCode);
			outwardMessageqLogUtil.setString(2, inventoryNumber);
			ResultSet inwardMessageqLogRset = outwardMessageqLogUtil.executeQuery();
			while (inwardMessageqLogRset.next()) {
				messageList.add(inwardMessageqLogRset.getString("MSG_DATA_CONTENT"));
			}
			messageContent.set("MSG_REF_NO", inventoryNumber);
			messageContent.setObject("MESSAGE", messageList);
			outwardMessageqLogUtil.reset();
		} catch (Exception e) {
			logger.logError("parseMessageContent() Exception" + e.getLocalizedMessage());
		} finally {
			dbContext.close();
		}
		logger.logDebug("parseMessageContent() End");
		return messageContent;
	}

	private final void distributeRecords() throws JobsException {
		ExecutorService service = Executors.newFixedThreadPool(MAX_THREADS);

		List<Future<Boolean>> futures = new ArrayList<Future<Boolean>>();
		int batchId = 1;
		for (List<DTObject> data : listArray) {
			Future<Boolean> f = service.submit(new OutwardMessageDispatchUtils.RecordBatchExecutor(entityCode, batchId, data));
			futures.add(f);
			++batchId;
		}

		boolean processStatus = true;
		for (Future<Boolean> f : futures) {
			try {
				if (!f.get()) {
					processStatus = false;
				}
			} catch (InterruptedException e) {
				logger.logError("distributeRecords() Exception 1" + e.getLocalizedMessage());
				throw new JobsException(JobsErrorCodes.EMAIL_PROCESS_THREADEXEC_INTR_E, e);
			} catch (ExecutionException e) {
				logger.logError("distributeRecords() Exception 2" + e.getLocalizedMessage());
				throw new JobsException(JobsErrorCodes.EMAIL_PROCESS_THREADEXEC_EXEC_E, e);
			}
		}
		if (!processStatus) {
			throw new JobsException(JobsErrorCodes.EMAIL_PROCESS_THREADEXEC_E);
		}
		service.shutdownNow();
	}

	public static class RecordBatchExecutor implements Callable<Boolean> {

		private final String entityCode;
		private final int batchId;
		private final List<DTObject> data;
		private final ApplicationLogger logger;

		private final String QUEUE_NAME = "OUTQ";

		public RecordBatchExecutor(String entityCode, int batchId, List<DTObject> data) {
			logger = ApplicationLogger.getInstance(this.getClass().getSimpleName());
			logger.logDebug("()");

			this.batchId = batchId;
			this.data = data;
			this.entityCode = entityCode;
		}

		@SuppressWarnings("unchecked")
		@Override
		public Boolean call() {
			logger.logDebug("call() Start - Batch ID:" + batchId);
			Boolean processComplete = Boolean.FALSE;
			DBContext dbContext = new DBContext();
			try {
				dbContext.setAutoCommit(false);
				for (DTObject record : data) {
					String referenceNumber = record.get("MSG_REF_NO");

					MQCommunicator sendCommunicator = new MQCommunicator();
					List<String> messageList = (List<String>) record.getObject("MESSAGE");
					boolean error = false;
					String errorCode = "";

					QueueBean messageQueueBean = new QueueBean();
					Utility interfaceUtility = new Utility();
					try {
						messageQueueBean = interfaceUtility.getQueueDetails(entityCode, QUEUE_NAME);
						if (messageQueueBean == null) {
							logger.logError("call() " + "QUEUE DETAILS UNAVAILABLE");
							errorCode = "001";
							error = true;
						}
					} catch (Exception e) {
						errorCode = "002";
						error = true;
						logger.logError("call() Error - " + e);
						logger.logDebug(referenceNumber);
						logger.logDebug("#### EXCEPTION OCCURED FOR THE MESSAGE END ###");
					} finally {
						interfaceUtility.close();
					}
					error = false;
					sendCommunicator.setHostName(messageQueueBean.getHostName());
					sendCommunicator.setPortNumber(messageQueueBean.getPort());

					String message = "";
					try {
						sendCommunicator.init();
						sendCommunicator.setQueueName(QUEUE_NAME);
						for (String messageContent : messageList) {
							message = messageContent;
							if (!sendCommunicator.sendMessage(messageContent, "")) {
								error = true;
								errorCode = "002";/* MQ ERROR */
								break;
							}
						}
					} catch (Exception e) {
						logger.logError("processRequest() Error - " + e);
						logger.logDebug("#### EXCEPTION OCCURED FOR THE MESSAGE BEGIN ###");
						logger.logDebug(message);
						logger.logDebug("#### EXCEPTION OCCURED FOR THE MESSAGE END ###");
						error = true;
					} finally {
						sendCommunicator.destroy();
					}
					String status = "S";
					if (error) {
						status = "F";
					}

					String insertReferenceqLogSql = "UPDATE JTCOUTQREF SET STATUS =?,ERROR=? WHERE ENTITY_CODE = ? AND MSG_REF_ID=?";
					DBUtil updateReferenceqLogUtil = dbContext.createUtilInstance();
					updateReferenceqLogUtil.reset();
					updateReferenceqLogUtil.setSql(insertReferenceqLogSql);
					updateReferenceqLogUtil.setString(1, status);
					updateReferenceqLogUtil.setString(2, errorCode);
					updateReferenceqLogUtil.setString(3, entityCode);
					updateReferenceqLogUtil.setString(4, referenceNumber);
					updateReferenceqLogUtil.executeUpdate();
					updateReferenceqLogUtil.reset();
				}
				dbContext.commit();
				processComplete = Boolean.TRUE;
			} catch (Exception e) {
				logger.logError("call() Exception 1" + e.getLocalizedMessage());
				try {
					dbContext.rollback();
				} catch (Exception re) {
					logger.logError("call() Exception 2" + re.getLocalizedMessage());
				}
			} finally {
				dbContext.close();
			}

			logger.logDebug("call() End");
			return processComplete;
		}
	}
}
