package com.patterns.jobs.utility;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Map;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.Message.RecipientType;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import com.patterns.jobs.bean.EmailServerBean;
import com.patterns.jobs.exceptions.JobsErrorCodes;
import com.patterns.jobs.exceptions.JobsException;

public class EmailUtils {

	public static class EmailContent {
		private String inventoryNumber;
		private String emailInterfaceCode;
		private Timestamp requestDate;
		private String subject;
		private String text;
		private String[] toList;
		private String[] ccList;
		private String[] bccList;
		private Map<String, byte[]> attachList;

		public String getInventoryNumber() {
			return inventoryNumber;
		}

		public void setInventoryNumber(String inventoryNumber) {
			this.inventoryNumber = inventoryNumber;
		}

		public Map<String, byte[]> getAttachList() {
			return attachList;
		}

		public void setAttachList(Map<String, byte[]> attachList) {
			this.attachList = attachList;
		}

		public String getSubject() {
			return subject;
		}

		public void setSubject(String subject) {
			this.subject = subject;
		}

		public String getText() {
			return text;
		}

		public void setText(String text) {
			this.text = text;
		}

		public String[] getToList() {
			return toList;
		}

		public void setToList(String[] toList) {
			this.toList = toList;
		}

		public String[] getCcList() {
			return ccList;
		}

		public void setCcList(String[] ccList) {
			this.ccList = ccList;
		}

		public String[] getBccList() {
			return bccList;
		}

		public void setBccList(String[] bccList) {
			this.bccList = bccList;
		}

		public void setRequestDate(Timestamp requestDate) {
			this.requestDate = requestDate;
		}

		public Timestamp getRequestDate() {
			return requestDate;
		}

		public String getEmailInterfaceCode() {
			return emailInterfaceCode;
		}

		public void setEmailInterfaceCode(String emailInterfaceCode) {
			this.emailInterfaceCode = emailInterfaceCode;
		}
	}

	private EmailServerBean emailServer;
	private boolean initialized;
	private String sendingProtocol;

	public EmailUtils(EmailServerBean emailServer) {
		this.emailServer = emailServer;
		this.initialized = false;
		this.sendingProtocol = emailServer.getSendProtocol();
	}

	private final void init() throws MessagingException {
		Properties prop = new Properties();
		if (sendingProtocol.equals("S")) {/* SMTP */
			prop.put("mail.smtp.host", emailServer.getServerIP());
			prop.put("mail.smtp.port", emailServer.getServerPort());
			if (emailServer.isAuthenticationRequired()) {
				prop.put("mail.smtp.auth", "true");
			} else {
				prop.remove("mail.smtp.auth");
			}
			if (emailServer.isRequireSSL()) {
				prop.put("mail.smtp.starttls.enable", "true");
				prop.put("mail.smtp.ssl.enable", "true");
				prop.put("mail.smtp.ssl.socketFactory", "javax.net.ssl.SSLSocketFactory");
				prop.put("mail.smtp.socketFactory.fallback", "false");
				prop.put("mail.smtp.socketFactory.port", emailServer.getServerPort());

			} else {
				prop.remove("mail.smtp.starttls.enable");
			}

			session = Session.getInstance(prop);
			transport = session.getTransport("smtp");
			if (emailServer.isAuthenticationRequired()) {
				transport.connect(emailServer.getServerIP(), emailServer.getAccountName(), emailServer.getAccountPassword());
			} else {
				transport.connect();
			}
		} else {
			/* OTHER PROTOCOL */
		}
	}

	private Transport transport;
	private Session session;

	public final void destroy() {
		if (transport != null) {
			try {
				transport.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public final boolean sendEmail(EmailContent content) throws JobsException {
		boolean isSuccess = false;
		try {
			if (!this.initialized)
				init();
			Multipart multiPart = new MimeMultipart();
			if (content.getText() != null) {
				MimeBodyPart bodyPart = new MimeBodyPart();
				//bodyPart.setText(content.getText(), "charset=utf-8"); 22-03-2017
				bodyPart.setContent(content.getText(), "text/html; charset=utf-8");
				multiPart.addBodyPart(bodyPart);
			}
			if (content.getAttachList() != null) {
				Map<String, byte[]> attachmentList = content.getAttachList();
				for (String attachName : attachmentList.keySet()) {
					MimeBodyPart bodyPart = new MimeBodyPart();
					BufferedDataSource bds = new BufferedDataSource(attachmentList.get(attachName), attachName);
					bodyPart.setDataHandler(new DataHandler(bds));
					bodyPart.setFileName(bds.getName());
					multiPart.addBodyPart(bodyPart);
				}
			}

			MimeMessage message = new MimeMessage(session);
			if (content.getToList() != null && content.getToList().length > 0) {
				message.addRecipients(RecipientType.TO, getAddresses(content.getToList()));
			}
			if (content.getCcList() != null && content.getCcList().length > 0) {
				message.addRecipients(RecipientType.CC, getAddresses(content.getCcList()));
			}
			if (content.getBccList() != null && content.getBccList().length > 0) {
				message.addRecipients(RecipientType.BCC, getAddresses(content.getBccList()));
			}
			message.addFrom(getAddress(emailServer.getSenderMail(), emailServer.getSenderName()));

			message.setSubject(content.getSubject());

			message.setSentDate(content.getRequestDate());

			message.setContent(multiPart);
			transport.sendMessage(message, message.getAllRecipients());
			isSuccess = true;
		} catch (NoSuchProviderException e) {
			throw new JobsException(JobsErrorCodes.EMAIL_PROCESS_NOPROVIDER_E, e);
		} catch (AddressException e) {
			throw new JobsException(JobsErrorCodes.EMAIL_PROCESS_ADDR_EXEC_E, e);
		} catch (MessagingException e) {
			throw new JobsException(JobsErrorCodes.EMAIL_PROCESS_MSG_EXEC_E, e);
		} catch (UnsupportedEncodingException e) {
			throw new JobsException(JobsErrorCodes.EMAIL_PROCESS_ENCODE_EXEC_E, e);
		}
		return isSuccess;
	}

	private final InternetAddress[] getAddresses(String[] recepient) throws AddressException {
		InternetAddress[] addressList = new InternetAddress[recepient.length];
		for (int i = 0; i < recepient.length; ++i) {
			addressList[i] = new InternetAddress(recepient[i]);
		}
		return addressList;
	}

	private final InternetAddress[] getAddress(String email, String name) throws UnsupportedEncodingException {
		InternetAddress[] addressList = new InternetAddress[1];
		InternetAddress address = new InternetAddress(email, name);
		addressList[0] = address;
		return addressList;
	}

	private static class BufferedDataSource implements DataSource {

		private byte[] _data;
		private java.lang.String _name;

		public BufferedDataSource(byte[] data, String name) {
			_data = data;
			_name = name;
		}

		public String getContentType() {
			return "application/octet-stream";
		}

		public InputStream getInputStream() throws IOException {
			return new ByteArrayInputStream(_data);
		}

		public String getName() {
			return _name;
		}

		public OutputStream getOutputStream() throws IOException {
			OutputStream out = new ByteArrayOutputStream();
			out.write(_data);
			return out;
		}

		@SuppressWarnings("unused")
		public Connection getConnection() throws SQLException {
			return null;
		}

		@SuppressWarnings("unused")
		public Connection getConnection(String username, String password) throws SQLException {
			return null;
		}

		@SuppressWarnings("unused")
		public PrintWriter getLogWriter() throws SQLException {
			return null;
		}

		@SuppressWarnings("unused")
		public int getLoginTimeout() throws SQLException {
			return 0;
		}

		@SuppressWarnings("unused")
		public void setLogWriter(PrintWriter out) throws SQLException {

		}

		@SuppressWarnings("unused")
		public void setLoginTimeout(int seconds) throws SQLException {

		}
	}
}
