package com.patterns.jobs.utility;

import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import patterns.config.framework.database.CM_LOVREC;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.loggers.ApplicationLogger;

import com.patterns.jobs.exceptions.JobsErrorCodes;
import com.patterns.jobs.exceptions.JobsException;

public class InvoiceRejRecordProcessorUtils {
	public static final int MAX_RECORDS_BATCH = 10;
	public static final int MAX_THREADS = 16;

	private final ApplicationLogger logger;
	private final List<InvoiceRejUtils> invoiceRejList;

	private final List<List<InvoiceRejUtils>> listArray = new LinkedList<List<InvoiceRejUtils>>();

	public InvoiceRejRecordProcessorUtils(List<InvoiceRejUtils> invoiceRejList) {
		logger = ApplicationLogger.getInstance(this.getClass().getSimpleName());
		logger.logDebug("(invRecordProcessorUtils)");
		this.invoiceRejList = invoiceRejList;
	}

	public void process() throws JobsException {
		logger.logDebug("process() Begin");
		long processInventoryStart = System.currentTimeMillis();
		processInventory();
		long processInventoryEnd = System.currentTimeMillis();
		logger.logDebug("InvoiceAuthRecordProcessorUtils process() processInventory Execution : " + (processInventoryEnd - processInventoryStart) + " ms");

		long distributeInventoryStart = System.currentTimeMillis();
		distributeRecords();
		long distributeInventoryEnd = System.currentTimeMillis();
		logger.logDebug("InvoiceAuthRecordProcessorUtils process() distributeInventory Execution : " + (distributeInventoryEnd - distributeInventoryStart) + " ms");

		logger.logDebug("InvoiceAuthRecordProcessorUtils process() End");
	}

	private final void processInventory() throws JobsException {
		logger.logDebug("InvoiceRejRecordProcessorUtils processInventory() Begin");
		List<InvoiceRejUtils> processingInvoiceRejList = new LinkedList<InvoiceRejUtils>();
		DBContext dbContext = new DBContext();
		DBUtil invRejPeDbUtil = dbContext.createUtilInstance();
		DBUtil deleteInvRejPeDbUtil = dbContext.createUtilInstance();
		DBUtil invAuthDtlDbUtil = dbContext.createUtilInstance();
		try {
			dbContext.setAutoCommit(false);
			invRejPeDbUtil.setMode(DBUtil.PREPARED);
			invRejPeDbUtil.setSql("SELECT * FROM INVREJPE  WHERE ENTITY_CODE=? AND GRP_ENTRY_DATE=? AND GRP_ENTRY_SL=? FOR UPDATE");
			deleteInvRejPeDbUtil.setMode(DBUtil.PREPARED);
			deleteInvRejPeDbUtil.setSql("DELETE FROM INVREJPE  WHERE ENTITY_CODE=? AND GRP_ENTRY_DATE=? AND GRP_ENTRY_SL=?");
			invAuthDtlDbUtil.setMode(DBUtil.PREPARED);
			invAuthDtlDbUtil.setSql("UPDATE INVAUTHDTL SET PROC_STATUS=? WHERE ENTITY_CODE=? AND ENTRY_DATE=? AND ENTRY_SL=? AND GRP_ENTRY_DATE=? AND GRP_ENTRY_SL=?");
			for (InvoiceRejUtils invoiceRejUtilsObject : invoiceRejList) {
				invRejPeDbUtil.setLong(1, invoiceRejUtilsObject.getEntityCode());
				invRejPeDbUtil.setDate(2, invoiceRejUtilsObject.getGrpEntryDate());
				invRejPeDbUtil.setInt(3, invoiceRejUtilsObject.getGrpEntrySl());
				ResultSet rs = invRejPeDbUtil.executeQuery();
				if (rs.next()) {
					deleteInvRejPeDbUtil.setLong(1, invoiceRejUtilsObject.getEntityCode());
					deleteInvRejPeDbUtil.setDate(2, invoiceRejUtilsObject.getGrpEntryDate());
					deleteInvRejPeDbUtil.setInt(3, invoiceRejUtilsObject.getGrpEntrySl());
					deleteInvRejPeDbUtil.executeUpdate();
					
					invAuthDtlDbUtil.setString(1, CM_LOVREC.QINVBULKAUTHSTATUS_INV_PROCESS_STATUS_U);
					invAuthDtlDbUtil.setLong(2, invoiceRejUtilsObject.getEntityCode());
					invAuthDtlDbUtil.setDate(3, invoiceRejUtilsObject.getAuthEntryDate());
					invAuthDtlDbUtil.setInt(4, invoiceRejUtilsObject.getAuthEntrySl());
					invAuthDtlDbUtil.setDate(5, invoiceRejUtilsObject.getGrpEntryDate());
					invAuthDtlDbUtil.setInt(6, invoiceRejUtilsObject.getGrpEntrySl());
					invAuthDtlDbUtil.executeUpdate();
					
					dbContext.commit();
					processingInvoiceRejList.add(invoiceRejUtilsObject);
					
				}
				
			}
		} catch (Exception e) {
			logger.logError("InvoiceRejRecordProcessorUtils processPendingRecords() Exception 1" + e.getLocalizedMessage());
			try {
				dbContext.rollback();
			} catch (Exception re) {
				logger.logError("InvoiceRejRecordProcessorUtils processPendingRecords() Exception 2" + re.getLocalizedMessage());
			}
		} finally {
			dbContext.close();
		}

		int currentRecordNumber = 1;
		List<InvoiceRejUtils> list = new LinkedList<InvoiceRejUtils>();
		for (InvoiceRejUtils processingInvoiceAuth : processingInvoiceRejList) {
			list.add(processingInvoiceAuth);
			if (currentRecordNumber % MAX_RECORDS_BATCH == 0) {
				listArray.add(list);
				list = new LinkedList<InvoiceRejUtils>();
			}
			++currentRecordNumber;

		}
		if (currentRecordNumber != 1 && ((currentRecordNumber - 1) % MAX_RECORDS_BATCH != 0)) {
			listArray.add(list);
		}

		logger.logDebug("InvoiceRejRecordProcessorUtils processInventory() End");
	}

	private final void distributeRecords() throws JobsException {
		ExecutorService service = Executors.newFixedThreadPool(MAX_THREADS);
		List<Future<Boolean>> futures = new ArrayList<Future<Boolean>>();
		int batchId = 1;
		for (List<InvoiceRejUtils> data : listArray) {
			Future<Boolean> f = service.submit(new InvoiceRejRecordProcessorUtils.RecordBatchExecutor(batchId, data));
			try {
				if (f.get()) {
					futures.add(f);
					++batchId;
				}
			}catch (InterruptedException e) {
				logger.logError("InvoiceAuthRecordProcessorUtils distributeRecords() Exception 1" + e.getLocalizedMessage());
				throw new JobsException(JobsErrorCodes.EMAIL_PROCESS_THREADEXEC_INTR_E, e);
			} catch (ExecutionException e) {
				logger.logError("InvoiceAuthRecordProcessorUtils distributeRecords() Exception 2" + e.getLocalizedMessage());
				throw new JobsException(JobsErrorCodes.EMAIL_PROCESS_THREADEXEC_EXEC_E, e);
			}
		}
		boolean processStatus = true;
		for (Future<Boolean> f : futures) {
			try {
				if (!f.get()) {
					processStatus = false;
				}
			} catch (InterruptedException e) {
				logger.logError("InvoiceAuthRecordProcessorUtils distributeRecords() Exception 1" + e.getLocalizedMessage());
				throw new JobsException(JobsErrorCodes.EMAIL_PROCESS_THREADEXEC_INTR_E, e);
			} catch (ExecutionException e) {
				logger.logError("InvoiceAuthRecordProcessorUtils distributeRecords() Exception 2" + e.getLocalizedMessage());
				throw new JobsException(JobsErrorCodes.EMAIL_PROCESS_THREADEXEC_EXEC_E, e);
			}
		}
		if (!processStatus) {
			throw new JobsException(JobsErrorCodes.EMAIL_PROCESS_THREADEXEC_E);
		}

		service.shutdownNow();
	}

	public static class RecordBatchExecutor implements Callable<Boolean> {
		private final int batchId;
		private final List<InvoiceRejUtils> data;
		private final ApplicationLogger logger;

		public RecordBatchExecutor(int batchId, List<InvoiceRejUtils> data) {
			logger = ApplicationLogger.getInstance(this.getClass().getSimpleName());
			logger.logDebug("()");
			this.batchId = batchId;
			this.data = data;
		}

		@Override
		public Boolean call() {
			logger.logDebug("InvoiceRejRecordProcessorUtils call() Start - Batch ID:" + batchId);
			Boolean processComplete = Boolean.FALSE;
			DBContext dbContext = new DBContext();
			DBUtil invrejDbUtil = dbContext.createUtilInstance();
			DBUtil invAuthDtlDbUtil = dbContext.createUtilInstance();
			try {
				dbContext.setAutoCommit(false);
				invrejDbUtil.setMode(DBUtil.CALLABLE);
				invrejDbUtil.setSql("{CALL SP_INV_REJECT_PROCESS(?,?,?,?)}");
				invAuthDtlDbUtil.setMode(DBUtil.PREPARED);
				invAuthDtlDbUtil.setSql("UPDATE INVAUTHDTL SET PROC_STATUS=? WHERE ENTITY_CODE=? AND ENTRY_DATE=? AND ENTRY_SL=? AND GRP_ENTRY_DATE=? AND GRP_ENTRY_SL=?");
				for (InvoiceRejUtils record : data) {
						logger.logDebug("SP_INV_REJECT_PROCESS call() BEGIN" + "");
						logger.logDebug("GRP_ENTRY_DATE " + record.getGrpEntryDate());
						logger.logDebug("GRP_ENTRY_SL " + record.getGrpEntrySl());
						logger.logDebug("GRP_ENTRY_DATE " + record.getAuthEntryDate());
						logger.logDebug("GRP_ENTRY_SL " + record.getAuthEntrySl());
						invrejDbUtil.setLong(1, record.getEntityCode());
						invrejDbUtil.setDate(2, record.getGrpEntryDate());
						invrejDbUtil.setInt(3, record.getGrpEntrySl());
						invrejDbUtil.registerOutParameter(4, Types.VARCHAR);
						invrejDbUtil.execute();
						logger.logDebug("STATUS:"+invrejDbUtil.getString(4));
						logger.logDebug("SP_INV_REJECT_PROCESS call() END");
						if (invrejDbUtil.getString(4).equals(RegularConstants.SP_SUCCESS)) {
							invAuthDtlDbUtil.setString(1, CM_LOVREC.EINVOICEAUTH_JOB_STATUS_S);
							invAuthDtlDbUtil.setLong(2, record.getEntityCode());
							invAuthDtlDbUtil.setDate(3, record.getAuthEntryDate());
							invAuthDtlDbUtil.setInt(4, record.getAuthEntrySl());
							invAuthDtlDbUtil.setDate(5, record.getGrpEntryDate());
							invAuthDtlDbUtil.setInt(6, record.getGrpEntrySl());
							invAuthDtlDbUtil.executeUpdate();
						}else{
							dbContext.rollback();
							invAuthDtlDbUtil.setString(1, CM_LOVREC.EINVOICEAUTH_JOB_STATUS_F);
							invAuthDtlDbUtil.setLong(2, record.getEntityCode());
							invAuthDtlDbUtil.setDate(3, record.getAuthEntryDate());
							invAuthDtlDbUtil.setInt(4, record.getAuthEntrySl());
							invAuthDtlDbUtil.setDate(5, record.getGrpEntryDate());
							invAuthDtlDbUtil.setInt(6, record.getGrpEntrySl());
							invAuthDtlDbUtil.executeUpdate();
						}
						dbContext.commit();
				}
				
				processComplete = Boolean.TRUE;
			} catch (Exception e) {
				logger.logError("InvoiceRejRecordProcessorUtils call() Exception 1" + e.getLocalizedMessage());
				try {
					dbContext.rollback();
				} catch (Exception re) {
					logger.logError("InvoiceRejRecordProcessorUtils call() Exception 2" + re.getLocalizedMessage());
				}
			} finally {
				dbContext.close();
			}
			logger.logDebug("InvoiceRejRecordProcessorUtils call() End");
			return processComplete;
		}
		
		
	}

}
