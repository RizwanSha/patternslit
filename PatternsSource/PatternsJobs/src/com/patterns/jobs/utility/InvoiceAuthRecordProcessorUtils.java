package com.patterns.jobs.utility;

import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import patterns.config.framework.database.CM_LOVREC;
import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.loggers.ApplicationLogger;

import com.patterns.jobs.exceptions.JobsErrorCodes;
import com.patterns.jobs.exceptions.JobsException;

public class InvoiceAuthRecordProcessorUtils {
	public static final int MAX_RECORDS_BATCH = 50;
	public static final int MAX_THREADS = 16;

	private final ApplicationLogger logger;
	private final List<InvoiceAuthUtils> invoiceAuthList;

	private final List<List<InvoiceAuthUtils>> listArray = new LinkedList<List<InvoiceAuthUtils>>();

	public InvoiceAuthRecordProcessorUtils(List<InvoiceAuthUtils> invoiceAuthList) {
		logger = ApplicationLogger.getInstance(this.getClass().getSimpleName());
		logger.logDebug("(invRecordProcessorUtils)");
		this.invoiceAuthList = invoiceAuthList;
	}

	public void process() throws JobsException {
		logger.logDebug("process() Begin");
		long processInventoryStart = System.currentTimeMillis();
		processInventory();
		long processInventoryEnd = System.currentTimeMillis();
		logger.logDebug("InvoiceAuthRecordProcessorUtils process() processInventory Execution : " + (processInventoryEnd - processInventoryStart) + " ms");

		long distributeInventoryStart = System.currentTimeMillis();
		distributeRecords();
		long distributeInventoryEnd = System.currentTimeMillis();
		logger.logDebug("InvoiceAuthRecordProcessorUtils process() distributeInventory Execution : " + (distributeInventoryEnd - distributeInventoryStart) + " ms");

		logger.logDebug("InvoiceAuthRecordProcessorUtils process() End");
	}

	private final void processInventory() throws JobsException {
		logger.logDebug("InvoiceAuthRecordProcessorUtils processInventory() Begin");
		List<InvoiceAuthUtils> processingInvoiceAuthList = new LinkedList<InvoiceAuthUtils>();
		DBContext dbContext = new DBContext();
		DBUtil invPeDbUtil = dbContext.createUtilInstance();
		DBUtil deleteInvoiceAuthPedbUtil = dbContext.createUtilInstance();
		DBUtil invAuthDtlDbUtil = dbContext.createUtilInstance();
		try {
			dbContext.setAutoCommit(false);
			invPeDbUtil.setMode(DBUtil.PREPARED);
			invPeDbUtil.setSql("SELECT * FROM INVAUTHPE  WHERE ENTITY_CODE=? AND GRP_ENTRY_DATE=? AND GRP_ENTRY_SL=? FOR UPDATE");
			deleteInvoiceAuthPedbUtil.setMode(DBUtil.PREPARED);
			deleteInvoiceAuthPedbUtil.setSql("DELETE FROM INVAUTHPE  WHERE ENTITY_CODE=? AND GRP_ENTRY_DATE=? AND GRP_ENTRY_SL=?");
			invAuthDtlDbUtil.setMode(DBUtil.PREPARED);
			invAuthDtlDbUtil.setSql("UPDATE INVAUTHDTL SET PROC_STATUS=? WHERE ENTITY_CODE=? AND ENTRY_DATE=? AND ENTRY_SL=? AND GRP_ENTRY_DATE=? AND GRP_ENTRY_SL=?");
			for (InvoiceAuthUtils InvoiceAuthUtilsObject : invoiceAuthList) {
				invPeDbUtil.setLong(1, InvoiceAuthUtilsObject.getEntityCode());
				invPeDbUtil.setDate(2, InvoiceAuthUtilsObject.getGrpEntryDate());
				invPeDbUtil.setInt(3, InvoiceAuthUtilsObject.getGrpEntrySl());
				ResultSet rs = invPeDbUtil.executeQuery();
				if (rs.next()) {
					deleteInvoiceAuthPedbUtil.setLong(1, InvoiceAuthUtilsObject.getEntityCode());
					deleteInvoiceAuthPedbUtil.setDate(2, InvoiceAuthUtilsObject.getGrpEntryDate());
					deleteInvoiceAuthPedbUtil.setInt(3, InvoiceAuthUtilsObject.getGrpEntrySl());
					deleteInvoiceAuthPedbUtil.executeUpdate();
					
					invAuthDtlDbUtil.setString(1, CM_LOVREC.QINVBULKAUTHSTATUS_INV_PROCESS_STATUS_U);
					invAuthDtlDbUtil.setLong(2, InvoiceAuthUtilsObject.getEntityCode());
					invAuthDtlDbUtil.setDate(3, InvoiceAuthUtilsObject.getAuthEntryDate());
					invAuthDtlDbUtil.setInt(4, InvoiceAuthUtilsObject.getAuthEntrySl());
					invAuthDtlDbUtil.setDate(5, InvoiceAuthUtilsObject.getGrpEntryDate());
					invAuthDtlDbUtil.setInt(6, InvoiceAuthUtilsObject.getGrpEntrySl());
					invAuthDtlDbUtil.executeUpdate();
					dbContext.commit();
					
						processingInvoiceAuthList.add(InvoiceAuthUtilsObject);
					
				}
			}
		} catch (Exception e) {
			logger.logError("InvoiceAuthRecordProcessorUtils processPendingRecords() Exception 1" + e.getLocalizedMessage());
			try {
				dbContext.rollback();
			} catch (Exception re) {
				logger.logError("InvoiceAuthRecordProcessorUtils processPendingRecords() Exception 2" + re.getLocalizedMessage());
			}
		} finally {
			dbContext.close();
		}

		int currentRecordNumber = 1;
		List<InvoiceAuthUtils> list = new LinkedList<InvoiceAuthUtils>();
		for (InvoiceAuthUtils processingInvoiceAuth : processingInvoiceAuthList) {
			list.add(processingInvoiceAuth);
			if (currentRecordNumber % MAX_RECORDS_BATCH == 0) {
				listArray.add(list);
				list = new LinkedList<InvoiceAuthUtils>();
			}
			++currentRecordNumber;

		}
		if (currentRecordNumber != 1 && ((currentRecordNumber - 1) % MAX_RECORDS_BATCH != 0)) {
			listArray.add(list);
		}

		logger.logDebug("InvoiceAuthRecordProcessorUtils processInventory() End");
	}

	private final void distributeRecords() throws JobsException {
		ExecutorService service = Executors.newFixedThreadPool(MAX_THREADS);
		List<Future<Boolean>> futures = new ArrayList<Future<Boolean>>();
		int batchId = 1;
		for (List<InvoiceAuthUtils> data : listArray) {
			Future<Boolean> f = service.submit(new InvoiceAuthRecordProcessorUtils.RecordBatchExecutor(batchId, data));
			try {
				if (f.get()) {
					futures.add(f);
					++batchId;
				}
			}catch (InterruptedException e) {
				logger.logError("InvoiceAuthRecordProcessorUtils distributeRecords() Exception 1" + e.getLocalizedMessage());
				throw new JobsException(JobsErrorCodes.EMAIL_PROCESS_THREADEXEC_INTR_E, e);
			} catch (ExecutionException e) {
				logger.logError("InvoiceAuthRecordProcessorUtils distributeRecords() Exception 2" + e.getLocalizedMessage());
				throw new JobsException(JobsErrorCodes.EMAIL_PROCESS_THREADEXEC_EXEC_E, e);
			}
		}
		boolean processStatus = true;
		for (Future<Boolean> f : futures) {
			try {
				if (!f.get()) {
					processStatus = false;
				}
			} catch (InterruptedException e) {
				logger.logError("InvoiceAuthRecordProcessorUtils distributeRecords() Exception 1" + e.getLocalizedMessage());
				throw new JobsException(JobsErrorCodes.EMAIL_PROCESS_THREADEXEC_INTR_E, e);
			} catch (ExecutionException e) {
				logger.logError("InvoiceAuthRecordProcessorUtils distributeRecords() Exception 2" + e.getLocalizedMessage());
				throw new JobsException(JobsErrorCodes.EMAIL_PROCESS_THREADEXEC_EXEC_E, e);
			}
		}
		if (!processStatus) {
			throw new JobsException(JobsErrorCodes.EMAIL_PROCESS_THREADEXEC_E);
		}

		service.shutdownNow();
	}

	public static class RecordBatchExecutor implements Callable<Boolean> {
		private final int batchId;
		private final List<InvoiceAuthUtils> data;
		private final ApplicationLogger logger;

		public RecordBatchExecutor(int batchId, List<InvoiceAuthUtils> data) {
			logger = ApplicationLogger.getInstance(this.getClass().getSimpleName());
			logger.logDebug("()");
			this.batchId = batchId;
			this.data = data;
		}

		@Override
		public Boolean call() {
			logger.logDebug("InvoiceAuthRecordProcessorUtils call() Start - Batch ID:" + batchId);
			Boolean processComplete = Boolean.FALSE;
			DBContext dbContext = new DBContext();
			DBUtil invgenDbUtil = dbContext.createUtilInstance();
			DBUtil invAuthDtlDbUtil = dbContext.createUtilInstance();
			try {
				dbContext.setAutoCommit(false);
				invgenDbUtil.setMode(DBUtil.CALLABLE);
				invgenDbUtil.setSql("{CALL SP_INV_GEN_PROCESS(?,?,?,?,?)}");
				invAuthDtlDbUtil.setMode(DBUtil.PREPARED);
				invAuthDtlDbUtil.setSql("UPDATE INVAUTHDTL SET PROC_STATUS=? WHERE ENTITY_CODE=? AND ENTRY_DATE=? AND ENTRY_SL=? AND GRP_ENTRY_DATE=? AND GRP_ENTRY_SL=?");
				for (InvoiceAuthUtils record : data) {
						logger.logDebug("SP_INV_GEN_PROCESS call() BEGIN" + "");
						logger.logDebug("GRP_ENTRY_DATE " + record.getGrpEntryDate());
						logger.logDebug("GRP_ENTRY_SL " + record.getGrpEntrySl());
						logger.logDebug("AUTH_ENTRY_DATE " + record.getAuthEntryDate());
						logger.logDebug("AUTH_ENTRY_SL " + record.getAuthEntrySl());
						logger.logDebug("INVOICE_SL " + record.getInvoiceSL());
						invgenDbUtil.setLong(1, record.getEntityCode());
						invgenDbUtil.setDate(2, record.getGrpEntryDate());
						invgenDbUtil.setInt(3, record.getGrpEntrySl());
						invgenDbUtil.setLong(4, record.getInvoiceSL());
						invgenDbUtil.registerOutParameter(5, Types.VARCHAR);
						invgenDbUtil.execute();
						logger.logDebug("SP_INV_GEN_PROCESS call() STATUS:"+invgenDbUtil.getString(5));
						logger.logDebug("SP_INV_GEN_PROCESS call() END");
						if (invgenDbUtil.getString(5).equals(RegularConstants.SP_SUCCESS)) {
							invAuthDtlDbUtil.setString(1, CM_LOVREC.EINVOICEAUTH_JOB_STATUS_S);
							invAuthDtlDbUtil.setLong(2, record.getEntityCode());
							invAuthDtlDbUtil.setDate(3, record.getAuthEntryDate());
							invAuthDtlDbUtil.setInt(4, record.getAuthEntrySl());
							invAuthDtlDbUtil.setDate(5, record.getGrpEntryDate());
							invAuthDtlDbUtil.setInt(6, record.getGrpEntrySl());
							invAuthDtlDbUtil.executeUpdate();
						}else{
							dbContext.rollback();
							invAuthDtlDbUtil.setString(1, CM_LOVREC.EINVOICEAUTH_JOB_STATUS_F);
							invAuthDtlDbUtil.setLong(2, record.getEntityCode());
							invAuthDtlDbUtil.setDate(3, record.getAuthEntryDate());
							invAuthDtlDbUtil.setInt(4, record.getAuthEntrySl());
							invAuthDtlDbUtil.setDate(5, record.getGrpEntryDate());
							invAuthDtlDbUtil.setInt(6, record.getGrpEntrySl());
							invAuthDtlDbUtil.executeUpdate();
						}
						dbContext.commit();
				}
				
				processComplete = Boolean.TRUE;
			} catch (Exception e) {
				logger.logError("InvoiceAuthRecordProcessorUtils call() Exception 1" + e.getLocalizedMessage());
				try {
					dbContext.rollback();
				} catch (Exception re) {
					logger.logError("InvoiceAuthRecordProcessorUtils call() Exception 2" + re.getLocalizedMessage());
				}
			} finally {
				dbContext.close();
			}
			logger.logDebug("InvoiceAuthRecordProcessorUtils call() End");
			return processComplete;
		}
		
		
	}

}
