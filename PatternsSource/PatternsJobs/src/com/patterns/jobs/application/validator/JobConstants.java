package com.patterns.jobs.application.validator;

public final class JobConstants {

	private JobConstants() {

	}

	public static final String COLUMN_ENABLED = "1";
	public static final String COLUMN_DISABLED = "0";
	public static final String EMPTY_STRING = "";
	public static final String ZERO = "0";

	public static final String DATA_AVAILABLE = "1";
	public static final String DATA_UNAVAILABLE = "0";

	public static final String RESULT = "RESULT";
	public static final String ERROR_KEY = "ERROR";
	public static final String ERROR_DETAILS = "COLUMN_ERROR";

	public static final String TOKEN_DELIMITER = ",";

	public static final int JOBINDEX_DEFAULT = 1;

	public static final String JOB_CONTEXT = "JOB_CONTEXT";
	public static final String ENTITY_CODE = "ENTITY_CODE";
	public static final String WORKER_CODE = "WORKER_CODE";
	public static final String JOB_CODE = "JOB_CODE";

	public static final String PENDING_PROCESSING = "1";
	public static final String CONSUMED = "2";
	public static final String PROCESSED = "3";
	public static final String PENDING_DELIVERY = "4";
	public static final String DELIVERED = "5";
	public static final String ERROR = "6";
	public static final String DELIVERY_STATUS = "7";
	public static final String DELIVERY_FAILURE = "8";

	public static final String INWARD_MESSAGE = "I";
	public static final String OUTWARD_MESSAGE = "O";

}
