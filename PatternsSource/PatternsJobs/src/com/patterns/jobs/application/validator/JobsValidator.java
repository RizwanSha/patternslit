package com.patterns.jobs.application.validator;

import java.math.BigInteger;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.loggers.ApplicationLogger;

public class JobsValidator {

	protected final ApplicationLogger logger;
	protected final DBContext dbContext;

	public JobsValidator() {
		logger = ApplicationLogger.getInstance(this.getClass().getSimpleName());
		logger.logDebug("()");
		this.dbContext = new DBContext();
	}

	public JobsValidator(DBContext dbContext) {
		logger = ApplicationLogger.getInstance(this.getClass().getSimpleName());
		logger.logDebug("(DBContext)");
		this.dbContext = dbContext;
	}

	public void close() {
		if (dbContext != null) {
			dbContext.close();
		}
	}

	public final Timestamp getSystemTime() {
		Timestamp systemTime = null;
		DBUtil sysDateTimeRequest = dbContext.createUtilInstance();
		try {
			sysDateTimeRequest.reset();
			sysDateTimeRequest.setSql("SELECT FN_SYSDATETIME() FROM DUAL");
			ResultSet sysDateTimeRSet = sysDateTimeRequest.executeQuery();
			if (sysDateTimeRSet.next()) {
				systemTime = sysDateTimeRSet.getTimestamp(1);
			}
		} catch (SQLException e) {
			logger.logError("fillApplicationDetails() Exception" + e.getErrorCode());
		} finally {
			sysDateTimeRequest.reset();
		}
		return systemTime;
	}

	public static final boolean isEmpty(String value) {
		if (value == null) {
			return false;
		}
		return value.trim().equals(JobConstants.EMPTY_STRING);
	}

	public static final boolean isNumber(String value) {
		try {
			Integer.valueOf(value);
		} catch (NumberFormatException e) {
			return false;
		}
		return true;
	}

	public static final boolean isValidLength(String token, int maxLength) {
		if (token == null || token.trim().length() == 0 || token.length() <= maxLength)
			return true;
		return false;
	}

	public static final boolean isValidPhoneNumber(String value) {
		value = value.replaceAll("\\+", "");
		for (int i = 0; i < value.length(); i++) {
			if (!Character.toString(value.charAt(i)).matches("[0-9]")) {
				return false;
			}
		}
		return true;
	}

	public static boolean decodeStringToBoolean(String value) {
		if (value != null && value.equals(JobConstants.COLUMN_ENABLED))
			return true;
		return false;
	}

	public static String decodeBooleanToString(boolean value) {
		return value ? JobConstants.COLUMN_ENABLED : JobConstants.COLUMN_DISABLED;
	}

	public static final String concatIntegers(List<BigInteger> jobIndices) {
		String result = JobConstants.EMPTY_STRING;
		StringBuffer indexBuffer = new StringBuffer();
		for (BigInteger jobIndex : jobIndices) {
			indexBuffer.append(jobIndex.intValue()).append(",");
		}
		if (jobIndices.size() > 1) {
			result = indexBuffer.substring(0, indexBuffer.lastIndexOf(","));
		}
		return result;
	}

}