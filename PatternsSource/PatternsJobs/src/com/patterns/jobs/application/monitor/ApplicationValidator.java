package com.patterns.jobs.application.monitor;

import java.math.BigInteger;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.service.DTObject;

import com.patterns.jobs.JobConfigurationContext;
import com.patterns.jobs.JobContext;
import com.patterns.jobs.application.validator.JobConstants;
import com.patterns.jobs.application.validator.JobsValidator;
import com.patterns.jobs.bean.JobBean;
import com.patterns.jobs.bean.TemplateBean;
import com.patterns.jobs.bean.WorkerBean;
import com.patterns.jobs.bean.loader.JobLoader;
import com.patterns.jobs.quartz.QuartzManager;

public class ApplicationValidator extends JobsValidator {

	public ApplicationValidator() {
		super();
	}

	public ApplicationValidator(DBContext dbContext) {
		super(dbContext);
	}

	public final void acquireWorkers(DTObject context) {
		DBUtil entityJobsRequest = dbContext.createUtilInstance();
		try {
			Map<String, WorkerBean> workersMap = new HashMap<String, WorkerBean>();

			Map<String, JobBean> jobsMap = JobLoader.getInstance().getAvailableJobs();
			String entityCode = context.get("INIT_DEPLOYMENT_ENTITY");
			for (String jobCode : jobsMap.keySet()) {
				JobBean jobParameterBean = jobsMap.get(jobCode);
				for (int i = 1; i <= jobParameterBean.getMaxWorker(); i++) {
					DTObject inputDTO = new DTObject();
					inputDTO.set(JobConstants.ENTITY_CODE, entityCode);
					inputDTO.set(JobConstants.JOB_CODE, jobCode);
					WorkerBean workerBean = acquireWorker(inputDTO);
					if (workerBean != null && workerBean.getJobCode() != null) {
						workersMap.put(workerBean.getWorkerCode(), workerBean);
					}
				}
			}
			JobConfigurationContext.getInstance().setWorkerCollection(workersMap);
		} catch (Exception e) {
			logger.logError("acquireEntityJobs() Exception" + e.getLocalizedMessage());
		} finally {
			entityJobsRequest.reset();
		}
	}

	public final WorkerBean acquireWorker(DTObject inputDTO) {
		WorkerBean workerBean = new WorkerBean();
		String entityCode = inputDTO.get(JobConstants.ENTITY_CODE);
		String jobCode = inputDTO.get(JobConstants.JOB_CODE);

		DBContext dbContext = new DBContext();
		try {
			dbContext.setAutoCommit(false);
			DBUtil workerLockRequest = dbContext.createUtilInstance();
			String lockSqlQuery = "SELECT WORKER_CODE,DESCRIPTION FROM WORKERS WHERE ENTITY_CODE=? AND JOB_CODE=? AND ENABLED='1' AND STATUS='A' FOR UPDATE";
			workerLockRequest.reset();
			workerLockRequest.setSql(lockSqlQuery);
			workerLockRequest.setString(1, entityCode);
			workerLockRequest.setString(2, jobCode);
			ResultSet lockRSet = workerLockRequest.executeQuery();
			if (lockRSet.next()) {
				List<BigInteger> jobIndexList = new ArrayList<BigInteger>();
				String jobIndexReadSqlQuery = "SELECT JOB_INDEX FROM WORKERSJOBALLOCDTL WHERE ENTITY_CODE=? AND WORKER_CODE=? ORDER BY DTL_SL ASC";
				DBUtil jobIndexReadRequest = dbContext.createUtilInstance();
				jobIndexReadRequest.reset();
				jobIndexReadRequest.setSql(jobIndexReadSqlQuery);
				jobIndexReadRequest.setString(1, entityCode);
				jobIndexReadRequest.setString(2, lockRSet.getString(1));
				ResultSet jobIndexReadRSet = jobIndexReadRequest.executeQuery();
				while (jobIndexReadRSet.next()) {
					jobIndexList.add(new BigInteger(jobIndexReadRSet.getString(1)));
				}
				jobIndexReadRequest.reset();

				workerBean.setEntityCode(entityCode);
				workerBean.setWorkerCode(lockRSet.getString(1));
				workerBean.setDescription(lockRSet.getString(2));
				workerBean.setJobCode(jobCode);
				workerBean.setJobIndices(jobIndexList);

				String workerUpdateSqlQuery = "UPDATE WORKERS SET STATUS='U',LOCK_ON=FN_GETCDT(?) WHERE ENTITY_CODE=? AND WORKER_CODE=?";
				DBUtil workerUpdateRequest = dbContext.createUtilInstance();
				workerUpdateRequest.reset();
				workerUpdateRequest.setSql(workerUpdateSqlQuery);
				workerUpdateRequest.setString(1, entityCode);
				workerUpdateRequest.setString(2, entityCode);
				workerUpdateRequest.setString(3, lockRSet.getString(1));
				workerUpdateRequest.executeUpdate();
				workerUpdateRequest.reset();
			}
			workerLockRequest.reset();
			dbContext.commit();
		} catch (Exception e) {
			logger.logError("acquireWorker1() Exception" + e.getLocalizedMessage());
			try {
				dbContext.rollback();
			} catch (Exception re) {
				logger.logError("acquireWorker2() Exception" + re.getLocalizedMessage());
			}
		} finally {
			dbContext.close();
		}
		return workerBean;
	}

	public final void scheduleWorkers() {
		try {
			Map<String, WorkerBean> workersBeanMap = (Map<String, WorkerBean>) JobConfigurationContext.getInstance().getWorkerCollection();
			for (String workerCode : workersBeanMap.keySet()) {
				WorkerBean workerBean = workersBeanMap.get(workerCode);

				Map<String, JobBean> jobParameterMap = JobLoader.getInstance().getAvailableJobs();
				if (jobParameterMap != null) {
					JobBean jobParameterBean = jobParameterMap.get(workerBean.getJobCode());

					JobContext jobConfigurationBean = new JobContext();
					jobConfigurationBean.setEntityCode(workerBean.getEntityCode());
					jobConfigurationBean.setEntityJob(true);
					jobConfigurationBean.setCronExpression(jobParameterBean.getCronExpression());
					jobConfigurationBean.setMaximumWorkers(jobParameterBean.getMaxWorker());
					jobConfigurationBean.setNumberOfRecords(jobParameterBean.getMaxRecords());
					jobConfigurationBean.setRetryTimeout(jobParameterBean.getRetryTimeOut());
					jobConfigurationBean.setJob(jobParameterBean);
					jobConfigurationBean.setWorkerInstance(workerBean);

					QuartzManager.getInstance().unscheduleJob(jobConfigurationBean);
					QuartzManager.getInstance().scheduleJob(jobConfigurationBean);
				}
			}
		} catch (Exception e) {
			logger.logError("scheduleWorkersInstance() Exception" + e.getLocalizedMessage());
		}
	}

	public final void releaseAllocatedWorkers(DTObject nodeContext) {
		DBContext dbContext = new DBContext();
		try {
			dbContext.setAutoCommit(false);
			String nodeUpdateSqlQuery = "UPDATE WORKERS SET STATUS='A',LOCK_ON=NULL WHERE ENTITY_CODE=?";
			DBUtil nodeUpdateRequest = dbContext.createUtilInstance();
			nodeUpdateRequest.reset();
			nodeUpdateRequest.setSql(nodeUpdateSqlQuery);
			nodeUpdateRequest.setString(1, nodeContext.get(JobConstants.ENTITY_CODE));
			nodeUpdateRequest.executeUpdate();
			nodeUpdateRequest.reset();
			dbContext.commit();
		} catch (Exception e) {
			logger.logError("releaseAllocatedWorkers() Exception 1" + e.getLocalizedMessage());
			try {
				dbContext.rollback();
			} catch (Exception re) {
				logger.logError("releaseAllocatedWorkers() Exception 2" + re.getLocalizedMessage());
			}
		} finally {
			dbContext.close();
		}
	}

	public String generateInventoryNumbers(String sequenceName, int requiredNumbers) {
		String inventoryNumber = JobConstants.EMPTY_STRING;
		DBContext dbContext = new DBContext();
		try {
			dbContext.setAutoCommit(false);
			DBUtil dbUtil = dbContext.createUtilInstance();
			dbUtil.reset();
			dbUtil.setMode(DBUtil.CALLABLE);
			dbUtil.setSql("CALL FN_NEXTVALUES(?,?,?)");
			dbUtil.setString(1, sequenceName);
			dbUtil.setInt(2, requiredNumbers);
			dbUtil.registerOutParameter(3, Types.VARCHAR);
			dbUtil.execute();
			inventoryNumber = dbUtil.getString(3);
			dbUtil.reset();
			dbContext.commit();
		} catch (Exception e) {
			logger.logError("generateInventoryNumbers() Exception" + e.getLocalizedMessage());
		} finally {
			dbContext.close();
		}
		return inventoryNumber;
	}

	public final void releaseWorkers() {
		DBContext dbContext = new DBContext();
		try {
			dbContext.setAutoCommit(false);
			String nodeUpdateSqlQuery = "UPDATE WORKERS SET STATUS='A',LOCK_ON=NULL";
			DBUtil nodeUpdateRequest = dbContext.createUtilInstance();
			nodeUpdateRequest.reset();
			nodeUpdateRequest.setSql(nodeUpdateSqlQuery);
			nodeUpdateRequest.executeUpdate();
			nodeUpdateRequest.reset();
			dbContext.commit();
		} catch (Exception e) {
			logger.logError("releaseWorkers() Exception 1" + e);
			try {
				dbContext.rollback();
			} catch (Exception re) {
				logger.logError("releaseWorkers() Exception 2" + re);
			}
		} finally {
			dbContext.close();
		}
	}
	public DTObject processMessageTemplate(DTObject inputDTO, TemplateBean templateBean) {
		 
	    String subject = RegularConstants.EMPTY_STRING;
	    String body = RegularConstants.EMPTY_STRING;
	    String footer = RegularConstants.EMPTY_STRING;
	 
	    subject = templateBean.getSubjectEn();
	    body = templateBean.getBodyEn();
	    footer = templateBean.getFooterEn();
	 
	    for (String key : inputDTO.getKeys()) {
	        if (inputDTO.get(key) != null) {
	            if (subject != null) {
	                subject = subject.replace("$$" + key + "$$", inputDTO.get(key));
	            }
	            if (body != null) {
	                body = body.replace("$$" + key + "$$", inputDTO.get(key));
	            }
	            if (footer != null) {
	                footer = footer.replace("$$" + key + "$$", inputDTO.get(key));
	            }
	        }
	    }
	    if (subject == null) {
	        subject = RegularConstants.EMPTY_STRING;
	    }
	    if (body == null) {
	        body = RegularConstants.EMPTY_STRING;
	    }
	    if (footer == null) {
	        footer = RegularConstants.EMPTY_STRING;
	    }
	    inputDTO.set("BODY", body);
	    inputDTO.set("SUBJECT", subject);
	    return inputDTO;
	}

}
