package com.patterns.jobs.processor;

import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;

import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;

import com.patterns.jobs.JobContext;
import com.patterns.jobs.exceptions.JobsException;
import com.patterns.jobs.utility.EmailOutUtils;

public class EmailOutProcessor extends AbstractProcessor {
	private String entityCode;

	public void process(JobContext context) {
		logger.logDebug("process() Begin");

		List<String> peList = new LinkedList<String>();
		entityCode = context.getEntityCode();

		double numberOfRecords = context.getNumberOfRecords();
		int currentRecordCount = 0;

		DBContext dbContext = new DBContext();
		try {
			String emailOutPeSql = "SELECT INVENTORY_NO FROM EMAILOUTQPE WHERE ENTITY_CODE = ?";
			DBUtil emailOutPeUtil = dbContext.createUtilInstance();
			emailOutPeUtil.setSql(emailOutPeSql);
			emailOutPeUtil.setString(1, entityCode);
			ResultSet emailOutPeRset = emailOutPeUtil.executeQuery();
			while (emailOutPeRset.next()) {
				peList.add(emailOutPeRset.getString(1));
				++currentRecordCount;
				if (currentRecordCount >= numberOfRecords) {
					break;
				}
			}
			emailOutPeUtil.reset();
		} catch (Exception e) {
			logger.logError("process() Exception" + e.getLocalizedMessage());
		} finally {
			dbContext.close();
		}

		if (peList.size() > 0) {
			try {
				EmailOutUtils emailUtils = new EmailOutUtils(entityCode, peList);
				emailUtils.process();
			} catch (JobsException e) {
				logger.logError("process() Exception 2" + e.getErrorCode());
			}
		}
		logger.logDebug("process() End");
	}
}