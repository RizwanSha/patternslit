package com.patterns.jobs.processor;

import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;

import com.patterns.jobs.JobContext;
import com.patterns.jobs.exceptions.JobsException;
import com.patterns.jobs.utility.InvoiceAuthRecordProcessorUtils;
import com.patterns.jobs.utility.InvoiceAuthUtils;

public class InvoiceAuthProcessor extends AbstractProcessor {
	private String entityCode;

	public void process(JobContext context) {
		logger.logDebug("InvoiceAuthProcessor process() Begin");
		entityCode = context.getEntityCode();
		DBContext dbContext = new DBContext();
		boolean isRowPresent = false;
		
		List<InvoiceAuthUtils> invoiceAuthPeList = new LinkedList<InvoiceAuthUtils>();
		try {
			
			DBUtil invAuthPeUtil = dbContext.createUtilInstance();
			invAuthPeUtil.setSql("SELECT * FROM INVAUTHPE WHERE ENTITY_CODE=?");
			invAuthPeUtil.setString(1, entityCode);
			ResultSet invPeRset = invAuthPeUtil.executeQuery();
			while (invPeRset.next()) {
				InvoiceAuthUtils invAuth = new InvoiceAuthUtils();
				invAuth.setEntityCode(invPeRset.getLong("ENTITY_CODE"));
				invAuth.setGrpEntryDate(invPeRset.getDate("GRP_ENTRY_DATE"));
				invAuth.setGrpEntrySl(invPeRset.getInt("GRP_ENTRY_SL"));
				invAuth.setAuthEntryDate(invPeRset.getDate("AUTH_ENTRY_DATE"));
				invAuth.setAuthEntrySl(invPeRset.getInt("AUTH_ENTRY_SL"));
				invAuth.setInvoiceSL(invPeRset.getLong("INVOICE_SL"));
				invoiceAuthPeList.add(invAuth);
				isRowPresent = true;
			}
			invAuthPeUtil.reset();
		} catch (Exception e) {
			logger.logError("InvoiceAuthProcessor process() Exception" + e.getLocalizedMessage());
		} finally {
			dbContext.close();
		}
		if (isRowPresent) {
			try {
				InvoiceAuthRecordProcessorUtils invUtils = new InvoiceAuthRecordProcessorUtils(invoiceAuthPeList);
				invUtils.process();
			} catch (JobsException e) {
				logger.logError("InvoiceAuthProcessor process() Exception 2" + e.getErrorCode());
			}

		}
		logger.logDebug("InvoiceAuthProcessor process() End");
	}
	
}
