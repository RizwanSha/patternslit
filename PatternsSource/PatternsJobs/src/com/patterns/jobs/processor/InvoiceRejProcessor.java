package com.patterns.jobs.processor;

import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;

import com.patterns.jobs.JobContext;
import com.patterns.jobs.exceptions.JobsException;
import com.patterns.jobs.utility.InvoiceRejRecordProcessorUtils;
import com.patterns.jobs.utility.InvoiceRejUtils;

public class InvoiceRejProcessor extends AbstractProcessor {
	private String entityCode;

	public void process(JobContext context) {
		logger.logDebug("InvoiceRejProcessor process() Begin");
		entityCode = context.getEntityCode();
		DBContext dbContext = new DBContext();
		boolean isRowPresent = false;
		
		List<InvoiceRejUtils> invoiceRejPeList = new LinkedList<InvoiceRejUtils>();
		try {
			
			DBUtil dbUtil = dbContext.createUtilInstance();
			dbUtil.setSql("SELECT * FROM INVREJPE WHERE ENTITY_CODE=?");
			dbUtil.setString(1, entityCode);
			ResultSet resultSet = dbUtil.executeQuery();
			while (resultSet.next()) {
				InvoiceRejUtils invRejUtils = new InvoiceRejUtils();
				invRejUtils.setEntityCode(resultSet.getLong("ENTITY_CODE"));
				invRejUtils.setGrpEntryDate(resultSet.getDate("GRP_ENTRY_DATE"));
				invRejUtils.setGrpEntrySl(resultSet.getInt("GRP_ENTRY_SL"));
				invRejUtils.setAuthEntryDate(resultSet.getDate("AUTH_ENTRY_DATE"));
				invRejUtils.setAuthEntrySl(resultSet.getInt("AUTH_ENTRY_SL"));
				invoiceRejPeList.add(invRejUtils);
				isRowPresent = true;
			}
			dbUtil.reset();
		} catch (Exception e) {
			logger.logError("InvoiceRejProcessor process() Exception" + e.getLocalizedMessage());
		} finally {
			dbContext.close();
		}
		if (isRowPresent) {
			try {
				InvoiceRejRecordProcessorUtils invUtils = new InvoiceRejRecordProcessorUtils(invoiceRejPeList);
				invUtils.process();
			} catch (JobsException e) {
				logger.logError("InvoiceRejProcessor process() Exception 2" + e.getErrorCode());
			}

		}
		logger.logDebug("InvoiceRejProcessor process() End");
	}
	
}
