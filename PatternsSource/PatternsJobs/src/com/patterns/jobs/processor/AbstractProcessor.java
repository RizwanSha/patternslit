package com.patterns.jobs.processor;

import org.quartz.JobExecutionException;

import patterns.config.framework.loggers.ApplicationLogger;

import com.patterns.jobs.JobContext;

public abstract class AbstractProcessor {

	protected final ApplicationLogger logger;

	public AbstractProcessor() {
		logger = ApplicationLogger.getInstance(this.getClass().getSimpleName());
		logger.logDebug("()");
	}

	public void execute(JobContext context) throws JobExecutionException {
		logger.logDebug("execute() Begin");
		try {
			process(context);
		} catch (Exception e) {
			logger.logError("execute() Exception" + e.getLocalizedMessage());
		}
		logger.logDebug("execute() End");
	}

	public abstract void process(JobContext context);
}
