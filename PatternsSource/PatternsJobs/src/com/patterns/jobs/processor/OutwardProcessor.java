package com.patterns.jobs.processor;

import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;

import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;

import com.patterns.jobs.JobContext;
import com.patterns.jobs.exceptions.JobsException;
import com.patterns.jobs.utility.OutwardMessageDispatchUtils;

public class OutwardProcessor extends AbstractProcessor {
	private String entityCode;

	public void process(JobContext context) {
		logger.logDebug("process() Begin");

		List<String> peList = new LinkedList<String>();
		entityCode = context.getEntityCode();

		double numberOfRecords = context.getNumberOfRecords();
		int currentRecordCount = 0;

		DBContext dbContext = new DBContext();
		try {
			String inwardMessagePeSql = "SELECT MSG_REF_ID FROM JTCOUTPE WHERE ENTITY_CODE = ?";
			DBUtil inwardMessagePeUtil = dbContext.createUtilInstance();
			inwardMessagePeUtil.setSql(inwardMessagePeSql);
			inwardMessagePeUtil.setString(1, entityCode);
			ResultSet inwardMessagePeRset = inwardMessagePeUtil.executeQuery();
			while (inwardMessagePeRset.next()) {
				peList.add(inwardMessagePeRset.getString(1));
				++currentRecordCount;
				if (currentRecordCount >= numberOfRecords) {
					break;
				}
			}
			inwardMessagePeUtil.reset();
		} catch (Exception e) {
			logger.logError("process() Exception" + e.getLocalizedMessage());
		} finally {
			dbContext.close();
		}

		if (peList.size() > 0) {
			try {
				OutwardMessageDispatchUtils messageUtils = new OutwardMessageDispatchUtils(entityCode, peList);
				messageUtils.process();
			} catch (JobsException e) {
				logger.logError("process() Exception 2" + e.getErrorCode());
			}
		}
		logger.logDebug("process() End");
	}
}