/*
 * Copyright (c) 2014, 2015, Patterns Software Design Institute. All
 * rights reserved. Unauthorized copying of this file, via any medium is
 * strictly prohibited. PATTERNS SDI PROPRIETARY/CONFIDENTIAL. Use is
 * subject to license terms.
 */
package com.patterns.jobs.processor;

import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;

import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;

import com.patterns.jobs.JobContext;
import com.patterns.jobs.exceptions.JobsException;
import com.patterns.jobs.utility.SmsOutUtils;


/**
 * The Class SmsOutProcessor processes and dispatches the messages from
 * outgoing sms queue
 */
public class SmsOutProcessor extends AbstractProcessor {

	/** The entity code. */
	private String entityCode;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.patterns.jobs.processor.AbstractProcessor#process(com.patterns
	 * .jobs.JobContext)
	 */
	@Override
	public void process(JobContext context) {
		logger.logDebug("process() Begin");

		List<String> peList = new LinkedList<String>();
		entityCode = context.getEntityCode();

		double numberOfRecords = context.getNumberOfRecords();
		int currentRecordCount = 0;

		DBContext dbContext = new DBContext();
		try {
			String smsOutPeSql = "SELECT INVENTORY_NO FROM SMSOUTQPE WHERE ENTITY_CODE = ?";
			DBUtil smsOutPeUtil = dbContext.createUtilInstance();
			smsOutPeUtil.setSql(smsOutPeSql);
			smsOutPeUtil.setString(1, entityCode);
			ResultSet smsOutPeRSet = smsOutPeUtil.executeQuery();
			while (smsOutPeRSet.next()) {
				peList.add(smsOutPeRSet.getString(1));
				++currentRecordCount;
				if (currentRecordCount >= numberOfRecords) {
					break;
				}
			}
			smsOutPeUtil.reset();
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError(e.getLocalizedMessage());
			logger.logError("process() Exception" + e);
		} finally {
			dbContext.close();
		}

		if (peList.size() > 0) {
			try {
				SmsOutUtils smsUtils = new SmsOutUtils(entityCode, peList);
				smsUtils.process();
			} catch (JobsException e) {
				e.printStackTrace();
				logger.logError(e.getLocalizedMessage());
				logger.logError("process() Exception 2" + e);
			} catch (Exception e) {
				e.printStackTrace();
				logger.logError(e.getLocalizedMessage());
				logger.logError("process() Exception 3" + e);
			}
		}
		logger.logDebug("process() End");
	}
}