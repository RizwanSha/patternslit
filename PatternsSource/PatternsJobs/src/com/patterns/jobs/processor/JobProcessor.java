package com.patterns.jobs.processor;

import patterns.config.framework.monitor.JobApplicationContext;
import patterns.config.framework.service.DTObject;

import com.patterns.jobs.JobContext;
import com.patterns.jobs.application.monitor.ApplicationValidator;

public class JobProcessor extends AbstractProcessor {

	public void process(JobContext context) {
		logger.logDebug("process() Begin");
		ApplicationValidator applicationValidator = new ApplicationValidator();
		try {
			DTObject applicationContext = JobApplicationContext.getInstance().getContext();

			applicationValidator.acquireWorkers(applicationContext);

			applicationValidator.scheduleWorkers();
		} catch (Exception e) {
			logger.logError("process() Exception" + e.getLocalizedMessage());
		} finally {
			applicationValidator.close();
		}
		logger.logDebug("process() End");
	}
}