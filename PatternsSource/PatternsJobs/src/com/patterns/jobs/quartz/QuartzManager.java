package com.patterns.jobs.quartz;

import static org.quartz.CronScheduleBuilder.cronSchedule;
import static org.quartz.JobBuilder.newJob;
import static org.quartz.JobKey.jobKey;
import static org.quartz.TriggerBuilder.newTrigger;
import static org.quartz.TriggerKey.triggerKey;

import java.util.HashMap;

import org.quartz.CronTrigger;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.impl.StdSchedulerFactory;

import com.patterns.jobs.JobContext;
import com.patterns.jobs.application.validator.JobConstants;
import com.patterns.jobs.controller.JobController;

public class QuartzManager {

	private static class QuartzManagerHelper {
		private static final QuartzManager _INSTANCE = new QuartzManager();
	}

	public static QuartzManager getInstance() {
		return QuartzManagerHelper._INSTANCE;
	}

	private Scheduler scheduler = null;
	private HashMap<String, String> cronMap = new HashMap<String, String>();

	private QuartzManager() {

	}

	public void startScheduler() {
		try {
			SchedulerFactory sf = new StdSchedulerFactory();
			scheduler = sf.getScheduler();
			scheduler.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void stopScheduler() {
		try {
			scheduler.shutdown();
		} catch (SchedulerException e) {
			e.printStackTrace();
		}
	}

	public void startJobManager() {
		try {
			String triggerName = "cTrigger";
			String triggerGroup = "cTriggerGroup";
			String cronExpression = "0/30 * * ? * SUN-SAT";
			CronTrigger trigger = newTrigger().withIdentity(triggerKey(triggerName, triggerGroup)).withSchedule(cronSchedule(cronExpression)).startNow().build();
//			Trigger trigger = newTrigger().withIdentity(triggerKey(triggerName, triggerGroup)).startNow().build();
			String taskName = "JobManager";
			String taskGroup = "Manager";

			JobDataMap jobDataMap = new JobDataMap();
			jobDataMap.put(JobConstants.JOB_CONTEXT, new JobContext());

			JobDetail jobDetail = newJob(JobController.class).withIdentity(jobKey(taskName, taskGroup)).usingJobData(jobDataMap).build();
			scheduler.scheduleJob(jobDetail, trigger);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void stopJobManager() {
		try {
			String triggerName = "cTrigger";
			String triggerGroup = "cTriggerGroup";
			scheduler.unscheduleJob(triggerKey(triggerName, triggerGroup));
			String taskName = "JobManager";
			String taskGroup = "Manager";
			scheduler.deleteJob(jobKey(taskName, taskGroup));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public JobDetail getJobDetail(String jobName, String jobGroup) {
		JobDetail jobDetail = null;
		try {
			jobDetail = scheduler.getJobDetail(jobKey(jobName, jobGroup));
		} catch (SchedulerException e) {
			e.printStackTrace();
		}
		return jobDetail;
	}

	public void scheduleJob(JobContext jobConfigurationBean) {

		String jobCode = jobConfigurationBean.getJob().getCode();
		String jobController = jobConfigurationBean.getJob().getController();

		String cronExpression = jobConfigurationBean.getCronExpression();

		String entitySuffix = "";
		String entityWorkerSuffix = "";
		if (jobConfigurationBean.isEntityJob()) {
			String entityCode = jobConfigurationBean.getEntityCode();
			String workerCode = jobConfigurationBean.getWorkerInstance().getWorkerCode();
			entitySuffix = ("-").concat(entityCode);
			entityWorkerSuffix = ("-").concat(entityCode).concat("-").concat(workerCode);
		}

		String jobName = jobCode.concat(entityWorkerSuffix);
		String jobGroup = jobCode.concat(entitySuffix).concat("-jobGroup");
		String triggerName = jobCode.concat(entityWorkerSuffix).concat("-trigger");
		String triggerGroup = jobCode.concat(entityWorkerSuffix).concat("-triggerGroup");

		try {
			@SuppressWarnings("unchecked")
			Class<? extends Job> jobClass = (Class<? extends Job>) Class.forName(jobController);
			CronTrigger trigger = newTrigger().withIdentity(triggerKey(triggerName, triggerGroup)).withSchedule(cronSchedule(cronExpression)).startNow().build();
			JobDataMap jobDataMap = new JobDataMap();
			jobDataMap.put(JobConstants.JOB_CONTEXT, jobConfigurationBean);
			JobDetail jobDetail = newJob(jobClass).withIdentity(jobKey(jobName, jobGroup)).usingJobData(jobDataMap).build();
			scheduler.scheduleJob(jobDetail, trigger);
			cronMap.put(jobGroup, cronExpression);
			System.out.println("Scheduling Job End - " + jobCode + " - " + jobGroup);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
		}
	}

	public void unscheduleJob(JobContext jobConfigurationBean) {
		String jobCode = jobConfigurationBean.getJob().getCode();

		String entitySuffix = "";
		String entityWorkerSuffix = "";
		if (jobConfigurationBean.isEntityJob()) {
			String entityCode = jobConfigurationBean.getEntityCode();
			String workerCode = jobConfigurationBean.getWorkerInstance().getWorkerCode();
			entitySuffix = ("-").concat(entityCode);
			entityWorkerSuffix = ("-").concat(entityCode).concat("-").concat(workerCode);
		}

		String jobName = jobCode.concat(entityWorkerSuffix);
		String jobGroup = jobCode.concat(entitySuffix).concat("-jobGroup");
		String triggerName = jobCode.concat(entityWorkerSuffix).concat("-trigger");
		String triggerGroup = jobCode.concat(entityWorkerSuffix).concat("-triggerGroup");

		System.out.println("Unscheduling Job Begin - " + jobCode);
		try {
			scheduler.unscheduleJob(triggerKey(triggerName, triggerGroup));
			scheduler.deleteJob(jobKey(jobName, jobGroup));
			cronMap.remove(jobGroup);
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("Unscheduling Job End - " + jobCode + " - " + jobGroup);
	}

	public void disableJob(String jobCode, String jobGroup) {
		System.out.println("Disabling Job Begin - " + jobCode);
		try {
			scheduler.deleteJob(jobKey(jobCode, jobGroup));
			cronMap.remove(jobGroup);
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("Disabling Job End - " + jobCode);
	}

	public void enableJob(String jobName, String jobGroup) {
		try {
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String getCronExpression(String jobName, String jobGroup) throws Exception {
		String cronExp = null;
		try {
			cronExp = cronMap.get(jobGroup);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return cronExp;
	}

}