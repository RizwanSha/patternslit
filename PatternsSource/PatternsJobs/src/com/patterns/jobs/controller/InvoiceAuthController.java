package com.patterns.jobs.controller;

import org.quartz.JobExecutionException;

import com.patterns.jobs.processor.InvoiceAuthProcessor;

public class InvoiceAuthController extends AbstractController {

	public void process() throws JobExecutionException {
		try {
			InvoiceAuthProcessor processor = new InvoiceAuthProcessor();
			processor.execute(getJobContext());
		} catch (Exception e) {
			logger.logError("InvoiceAuthController process() Exception" + e.getLocalizedMessage());
		} finally {
		}
	}
}