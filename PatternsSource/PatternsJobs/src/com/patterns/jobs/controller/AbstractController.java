package com.patterns.jobs.controller;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import patterns.config.framework.loggers.ApplicationLogger;

import com.patterns.jobs.JobContext;
import com.patterns.jobs.application.validator.JobConstants;

@DisallowConcurrentExecution
public abstract class AbstractController implements Job {

	protected final ApplicationLogger logger;

	public AbstractController() {
		logger = ApplicationLogger.getInstance(this.getClass().getSimpleName());
		logger.logDebug("()");
	}

	private JobContext jobContext;

	protected JobContext getJobContext() {
		return jobContext;
	}

	public void execute(JobExecutionContext context) throws JobExecutionException {
		logger.logDebug("execute() Begin");
		try {
			this.jobContext = (JobContext) context.getJobDetail().getJobDataMap().get(JobConstants.JOB_CONTEXT);
			process();
		} catch (Exception e) {
			logger.logError("execute() Exception"+ e.getLocalizedMessage());
		}
		logger.logDebug("execute() End");
	}

	public abstract void process() throws JobExecutionException;
}
