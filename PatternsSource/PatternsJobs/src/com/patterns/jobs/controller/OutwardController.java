package com.patterns.jobs.controller;

import org.quartz.JobExecutionException;

import com.patterns.jobs.processor.OutwardProcessor;

public class OutwardController extends AbstractController {

	public void process() throws JobExecutionException {
		try {
			OutwardProcessor processor = new OutwardProcessor();
			processor.execute(getJobContext());
		} catch (Exception e) {
			logger.logError("process() Exception" + e.getLocalizedMessage());
		} finally {
		}
	}
}