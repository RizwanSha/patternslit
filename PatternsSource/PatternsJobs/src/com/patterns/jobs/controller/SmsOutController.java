/*
 * Copyright (c) 2014, 2015, Patterns Software Design Institute. All
 * rights reserved. Unauthorized copying of this file, via any medium is
 * strictly prohibited. PATTERNS SDI PROPRIETARY/CONFIDENTIAL. Use is
 * subject to license terms.
 */
package com.patterns.jobs.controller;

import org.quartz.JobExecutionException;

import com.patterns.jobs.processor.SmsOutProcessor;


/**
 * The Class SmsOutController provides controller for sending sms
 * messages
 */
public class SmsOutController extends AbstractController {

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.patterns.jobs.controller.AbstractController#process()
	 */
	@Override
	public void process() throws JobExecutionException {
		try {
			SmsOutProcessor processor = new SmsOutProcessor();
			processor.execute(getJobContext());
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError(e.getLocalizedMessage());
			logger.logError("process() Exception" + e.getLocalizedMessage());
		} finally {
		}
	}
}