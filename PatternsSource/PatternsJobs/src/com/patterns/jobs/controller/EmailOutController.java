package com.patterns.jobs.controller;

import org.quartz.JobExecutionException;

import com.patterns.jobs.processor.EmailOutProcessor;

public class EmailOutController extends AbstractController {

	public void process() throws JobExecutionException {
		try {
			EmailOutProcessor processor = new EmailOutProcessor();
			processor.execute(getJobContext());
		} catch (Exception e) {
			logger.logError("process() Exception" + e.getLocalizedMessage());
		} finally {
		}
	}
}