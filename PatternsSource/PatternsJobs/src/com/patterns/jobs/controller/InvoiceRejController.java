package com.patterns.jobs.controller;

import org.quartz.JobExecutionException;
import com.patterns.jobs.processor.InvoiceRejProcessor;

public class InvoiceRejController extends AbstractController {

	public void process() throws JobExecutionException {
		try {
			InvoiceRejProcessor processor = new InvoiceRejProcessor();
			processor.execute(getJobContext());
		} catch (Exception e) {
			logger.logError("InvoiceRejController process() Exception" + e.getLocalizedMessage());
		} finally {
		}
	}
}