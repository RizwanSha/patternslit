package com.patterns.jobs.controller;

import org.quartz.JobExecutionException;

import com.patterns.jobs.processor.JobProcessor;

public class JobController extends AbstractController {

	public void process() throws JobExecutionException {
		try {
			JobProcessor processor = new JobProcessor();
			processor.execute(getJobContext());
		} catch (Exception e) {
			logger.logError("process() Exception" + e.getLocalizedMessage());
		} finally {
		}
	}
}