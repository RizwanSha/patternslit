package com.patterns.jobs.controller;

import org.quartz.JobExecutionException;

import com.patterns.jobs.processor.InwardMessageProcessor;

public class InwardMessageController extends AbstractController {

	public void process() throws JobExecutionException {
		try {
			InwardMessageProcessor processor = new InwardMessageProcessor();
			processor.execute(getJobContext());
		} catch (Exception e) {
			logger.logError("process() Exception" + e.getLocalizedMessage());
		} finally {
		}
	}
}