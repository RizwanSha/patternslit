package com.patterns.jobs.controller;

import org.quartz.JobExecutionException;

import com.patterns.jobs.processor.InwardRecordProcessor;

public class InwardRecordController extends AbstractController {

	public void process() throws JobExecutionException {
		try {
			InwardRecordProcessor processor = new InwardRecordProcessor();
			processor.execute(getJobContext());
		} catch (Exception e) {
			logger.logError("process() Exception" + e.getLocalizedMessage());
		} finally {
		}
	}
}