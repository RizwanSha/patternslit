package com.patterns.jobs.message.value.parser.handler;

import java.util.List;
import java.util.Map;

import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.loggers.ApplicationLogger;

import com.patterns.interfaces.config.manager.bean.PreProcessorRecordConfigBean;

public abstract class AbstractValueProcessor {
	protected final ApplicationLogger logger;

	private Map<String, Object> processContext;
	private String[] record;
	private DBContext dbContext;
	private String entityCode;
	private List<PreProcessorRecordConfigBean> configDetails;
	private String sourceTableName;
	private String sourceColumnName;

	public Map<String, Object> getProcessContext() {
		return processContext;
	}

	public String[] getRecord() {
		return record;
	}

	public DBContext getDbContext() {
		return dbContext;
	}

	public String getEntityCode() {
		return entityCode;
	}

	public List<PreProcessorRecordConfigBean> getConfigDetails() {
		return configDetails;
	}

	public String getSourceColumnName() {
		return sourceColumnName;
	}

	public String getSourceTableName() {
		return sourceTableName;
	}

	public AbstractValueProcessor(Map<String, Object> context) {
		logger = ApplicationLogger.getInstance(this.getClass().getSimpleName());
		logger.logDebug("()");

		this.processContext = context;
		this.record = (String[]) context.get("RECORD");
		this.entityCode = (String) context.get("ENTITY_CODE");
		this.dbContext = (DBContext) context.get("DBCONTEXT");
		this.configDetails = (List<PreProcessorRecordConfigBean>) context.get("CONFIG_DETAILS");
		this.sourceTableName = (String) context.get("TABLE_NAME");
		this.sourceColumnName = (String) context.get("COLUMN_NAME");

	}

	public String getOperation() {
		return (String) getProcessContext().get("OPERATION");
	}

}
