package com.patterns.jobs.message.value.parser.handler;

import java.util.Map;

import patterns.config.framework.database.RegularConstants;

public class CommonMessageParser extends AbstractValueProcessor {

	public CommonMessageParser(Map<String, Object> context) {
		super(context);
	}

	public String getPatientRoomSL() {
		String result = null;
		int sl = 0;
		String code = getRecord()[2];
		try {
			if (code != null && (!code.equals(RegularConstants.EMPTY_STRING))) {
				sl = Integer.parseInt(code);
				if (sl == 1) {
					result = "1";
				} else {
					result = "0";
				}
			}
		} catch (Exception e) {
			logger.logError("CommonMessageParser() - getPatientRoomSL()");
			e.printStackTrace();
		}
		return result;
	}

}
