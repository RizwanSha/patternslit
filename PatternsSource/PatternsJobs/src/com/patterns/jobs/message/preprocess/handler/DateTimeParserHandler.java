package com.patterns.jobs.message.preprocess.handler;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import com.patterns.interfaces.config.manager.bean.PreProcessorRecordConfigBean;
import com.patterns.jobs.exceptions.JobsErrorCodes;
import com.patterns.jobs.exceptions.JobsException;

public class DateTimeParserHandler extends AbstractPreProcessor {

	@Override
	public Map<String, Object> process() throws JobsException {
		try {
			Object date = null;
			StringBuffer result = new StringBuffer("");
			String dateFormat = "ddMMyyyyhhmm";
			for (PreProcessorRecordConfigBean bean : getConfigDetails()) {
				int index = bean.getIndex();
				String value = getRecord()[index];
				if (value == null) {
					break;
				}
				if (value != null) {
					result.append(value.trim());
				}
			}
			if (!result.toString().equals("") && !result.toString().equals("000000000000")) {
				date = convertTimeStamp(dateFormat, result.toString());
			}
			getProcessContext().put(getSourceColumnName(), date);
		} catch (Exception e) {
			logger.logError("process() - Error - " + e.getLocalizedMessage());
			throw new JobsException(JobsErrorCodes.FAILURE_PREPROCESSING_E);
		} finally {
		}
		return getProcessContext();
	}

	private Timestamp convertTimeStamp(String dateFormat, String date) throws JobsException {
		Timestamp timestamp = null;
		try {
			SimpleDateFormat format = new SimpleDateFormat(dateFormat);
			Date parsedDate = format.parse(date);
			timestamp = new java.sql.Timestamp(parsedDate.getTime());
		} catch (Exception e) {
			logger.logError("process() - Error - " + e.getLocalizedMessage());
			throw new JobsException(JobsErrorCodes.FAILURE_PREPROCESSING_E);
		}
		return timestamp;
	}
}
