package com.patterns.jobs.message.preprocess.handler;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Map;

import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBUtil;

import com.patterns.jobs.exceptions.JobsErrorCodes;
import com.patterns.jobs.exceptions.JobsException;

public class BedOTNHandler extends AbstractPreProcessor {

	@Override
	public Map<String, Object> process() throws JobsException {
		String internalBedNumber = RegularConstants.EMPTY_STRING;
		DBUtil dbUtil = getDbContext().createUtilInstance();
		try {
			String wardCode = getRecord()[0].trim();
			String roomCode = getRecord()[1].trim();
			String bedCode = getRecord()[2].trim();
			String roomCodeWithOutZero = Integer.valueOf(roomCode).toString();
			String bedCodeWithOutZero = Integer.valueOf(bedCode).toString();
			if (getOperation().equals("W")) {
				int count = 0;
				ArrayList<String> bedList = new ArrayList<String>();
				bedList.add(bedCode);
				while (bedCode.charAt(0) == '0') {
					bedList.add(bedCode.substring(1));
					bedCode = bedCode.substring(1);
				}

				String sqlQuery = "SELECT COUNT(1) FROM BEDS WHERE ENTITY_CODE=? AND WARD_CODE=? AND ROOM_CODE=? AND BED_CODE=?";
				dbUtil.reset();
				dbUtil.setMode(DBUtil.PREPARED);
				dbUtil.setSql(sqlQuery);
				dbUtil.setString(1, getEntityCode());
				dbUtil.setString(2, wardCode);
				dbUtil.setString(3, roomCodeWithOutZero);
				dbUtil.setString(4, bedCodeWithOutZero);
				ResultSet rset = dbUtil.executeQuery();
				if (rset.next()) {
					count = rset.getInt(1);
				}
				if (count == 0) {
					String selectInternalBedsNumber = "SELECT IFNULL(MAX(INTERNAL_BED_CODE),0)+1 FROM BEDS WHERE ENTITY_CODE=?";
					dbUtil.reset();
					dbUtil.setMode(DBUtil.PREPARED);
					dbUtil.setSql(selectInternalBedsNumber);
					dbUtil.setString(1, getEntityCode());
					ResultSet rsetInternal = dbUtil.executeQuery();
					if (rsetInternal.next()) {
						internalBedNumber = rsetInternal.getString(1);
					}
					dbUtil.reset();
					int n = bedList.size() - 1;
					for (int i = 0; i < n; i++) {
						String wardCodeAppendOfArraryOFRoomAndBed = wardCode + "|" + roomCodeWithOutZero + "|" + bedList.get(i);
						String insertGeneralOTN = "INSERT INTO GENERALOTN (ENTITY_CODE,TABLE_NAME,OLD_KEY_VALUE,NEW_KEY_NUMERIC_VALUE) VALUES (?,?,?,?)";
						dbUtil.setMode(DBUtil.PREPARED);
						dbUtil.setSql(insertGeneralOTN);
						dbUtil.setString(1, getEntityCode());
						dbUtil.setString(2, getSourceTableName());
						dbUtil.setString(3, wardCodeAppendOfArraryOFRoomAndBed);
						dbUtil.setString(4, internalBedNumber);
						dbUtil.executeUpdate();
						dbUtil.reset();
					}
				}
			} else if (getOperation().equals("R")) {
				String Combination = wardCode + "|" + roomCodeWithOutZero + "|" + bedCode;
				String selectNewStringValue = "SELECT NEW_KEY_NUMERIC_VALUE FROM GENERALOTN WHERE ENTITY_CODE=? AND TABLE_NAME=? AND OLD_KEY_VALUE=?";
				dbUtil.reset();
				dbUtil.setMode(DBUtil.PREPARED);
				dbUtil.setSql(selectNewStringValue);
				dbUtil.setString(1, getEntityCode());
				dbUtil.setString(2, getSourceTableName());
				dbUtil.setString(3, Combination);
				ResultSet rset = dbUtil.executeQuery();
				if (rset.next()) {
					internalBedNumber = rset.getString(1);
				}
				dbUtil.reset();
				if (internalBedNumber.equals("")) {
					logger.logError("process() - Error - NO Record in GENERALOTN");
					throw new JobsException(JobsErrorCodes.FAILURE_PREPROCESSING_E);
				}
			}
			getProcessContext().put(getSourceColumnName(), internalBedNumber);
			getProcessContext().put("ROOM_CODE", roomCodeWithOutZero);
			getProcessContext().put("BED_CODE", bedCodeWithOutZero);
		} catch (Exception e) {
			logger.logError("process() - Error - " + e.getLocalizedMessage());
			throw new JobsException(JobsErrorCodes.FAILURE_PREPROCESSING_E);
		} finally {
			dbUtil.reset();
		}
		return getProcessContext();
	}
}
