package com.patterns.jobs.message.preprocess.handler;

import java.sql.ResultSet;
import java.util.Map;

import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBUtil;

import com.patterns.interfaces.config.manager.bean.PreProcessorRecordConfigBean;
import com.patterns.jobs.exceptions.JobsErrorCodes;
import com.patterns.jobs.exceptions.JobsException;

public class GetInternalBedCode extends AbstractPreProcessor {

	@Override
	public Map<String, Object> process() throws JobsException {
		String InternalBedNumber = RegularConstants.EMPTY_STRING;
		String test = RegularConstants.EMPTY_STRING;
		DBUtil dbUtil = getDbContext().createUtilInstance();
		try {

			for (PreProcessorRecordConfigBean bean : getConfigDetails()) {
				int index = bean.getIndex();
				String value = getRecord()[index];
				if (value != null) {
					test += value.trim() + "|";
				}
			}
			String[] dataList = test.split("\\|");
			String WardCode = dataList[0].trim();
			String RoomCode = dataList[1].trim();
			String BedCode = dataList[2].trim();
			String RoomCodeWithOutZero = Integer.valueOf(RoomCode).toString();
			String BedCodeWithOutZero = Integer.valueOf(BedCode).toString();

			String sqlQuery = "SELECT INTERNAL_BED_CODE FROM BEDITFOTN WHERE ENTITY_CODE=? AND WARD_CODE=? AND ROOM_CODE=? AND BED_CODE=?";
			dbUtil.reset();
			dbUtil.setMode(DBUtil.PREPARED);
			dbUtil.setSql(sqlQuery);
			dbUtil.setString(1, getEntityCode());
			dbUtil.setString(2, WardCode);
			dbUtil.setString(3, RoomCodeWithOutZero);
			dbUtil.setString(4, BedCodeWithOutZero);
			ResultSet rset = dbUtil.executeQuery();
			if (rset.next()) {
				InternalBedNumber = rset.getString(1);
			}
			if (InternalBedNumber.equals("")) {
				String sql = "SELECT INTERNAL_BED_CODE FROM BEDS WHERE ENTITY_CODE=? AND WARD_CODE=? AND ROOM_CODE=? AND BED_CODE=?";
				dbUtil.reset();
				dbUtil.setMode(DBUtil.PREPARED);
				dbUtil.setSql(sql);
				dbUtil.setString(1, getEntityCode());
				dbUtil.setString(2, WardCode);
				dbUtil.setString(3, RoomCodeWithOutZero);
				dbUtil.setString(4, BedCodeWithOutZero);
				ResultSet rset1 = dbUtil.executeQuery();
				if (rset1.next()) {
					InternalBedNumber = rset1.getString(1);
				}
			}
			if (InternalBedNumber.equals("")) {
				logger.logError("process() - Error - No record in BEDS TABLE");
				throw new JobsException(JobsErrorCodes.FAILURE_PREPROCESSING_E);
			}
			getProcessContext().put(getSourceColumnName(), InternalBedNumber);

		} catch (Exception e) {
			logger.logError("process() - Error - " + e.getLocalizedMessage());
			throw new JobsException(JobsErrorCodes.FAILURE_PREPROCESSING_E);
		} finally {
			dbUtil.reset();
		}
		return getProcessContext();
	}
}
