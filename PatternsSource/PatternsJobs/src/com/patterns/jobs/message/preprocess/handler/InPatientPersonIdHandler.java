package com.patterns.jobs.message.preprocess.handler;

import java.sql.ResultSet;
import java.util.Map;

import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBUtil;

import com.patterns.jobs.exceptions.JobsErrorCodes;
import com.patterns.jobs.exceptions.JobsException;

public class InPatientPersonIdHandler extends AbstractPreProcessor {

	@Override
	public Map<String, Object> process() throws JobsException {
		String patRegYear = RegularConstants.EMPTY_STRING;
		String patRegSl = RegularConstants.EMPTY_STRING;
		String personId = RegularConstants.EMPTY_STRING;
		DBUtil dbUtil = getDbContext().createUtilInstance();
		try {
			String ipYear = getRecord()[4];
			String ipSl = getRecord()[5];
			String selectRegistrationNumber = "SELECT PATIENT_REG_YEAR,PATIENT_REG_SL FROM INPATIENT WHERE ENTITY_CODE=? AND IP_NO_YEAR=? AND IP_NO_SERIAL=?";
			dbUtil.reset();
			dbUtil.setMode(DBUtil.PREPARED);
			dbUtil.setSql(selectRegistrationNumber);
			dbUtil.setString(1, getEntityCode());
			dbUtil.setString(2, ipYear);
			dbUtil.setString(3, ipSl);
			ResultSet rset = dbUtil.executeQuery();
			if (rset.next()) {
				patRegYear = rset.getString(1);
				patRegSl= rset.getString(2);
			}
			dbUtil.reset();
			if (patRegYear.equals(RegularConstants.EMPTY_STRING) && patRegSl.equals(RegularConstants.EMPTY_STRING) ) {
				logger.logError("process() - Error - No record in INPATIENT");
				throw new JobsException(JobsErrorCodes.FAILURE_PREPROCESSING_E);
			}
			String selectPersonId = "SELECT PERSON_ID FROM PATIENTREG WHERE ENTITY_CODE=? AND PATIENT_REG_YEAR=? AND PATIENT_REG_SL=? ";
			dbUtil.reset();
			dbUtil.setMode(DBUtil.PREPARED);
			dbUtil.setSql(selectPersonId);
			dbUtil.setString(1, getEntityCode());
			dbUtil.setString(2, patRegYear);
			dbUtil.setString(3, patRegSl);
			ResultSet rset1 = dbUtil.executeQuery();
			if (rset1.next()) {
				personId = rset1.getString(1);
			}
			dbUtil.reset();

			if (personId.equals(RegularConstants.EMPTY_STRING)) {
				logger.logError("process() - Error - No record in PATIENTREG");
				throw new JobsException(JobsErrorCodes.FAILURE_PREPROCESSING_E);
			}
			getProcessContext().put("PATIENT_REG_YEAR", patRegYear);
			getProcessContext().put("PATIENT_REG_SL", patRegSl);
			getProcessContext().put("PERSON_ID", personId);
		} catch (Exception e) {
			logger.logError("process() - Error - " + e.getLocalizedMessage());
			throw new JobsException(JobsErrorCodes.FAILURE_PREPROCESSING_E);
		} finally {
			dbUtil.reset();
		}
		return getProcessContext();
	}
}