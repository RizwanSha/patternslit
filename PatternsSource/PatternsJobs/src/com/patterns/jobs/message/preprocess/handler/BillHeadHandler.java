package com.patterns.jobs.message.preprocess.handler;

import java.sql.ResultSet;
import java.util.Map;

import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBUtil;

import com.patterns.interfaces.config.manager.bean.PreProcessorRecordConfigBean;
import com.patterns.jobs.exceptions.JobsErrorCodes;
import com.patterns.jobs.exceptions.JobsException;

public class BillHeadHandler extends AbstractPreProcessor {

	@Override
	public Map<String, Object> process() throws JobsException {
		String inventoryNumber = RegularConstants.NULL;
		String headCode = RegularConstants.EMPTY_STRING;
		DBUtil dbUtil = getDbContext().createUtilInstance();
		try {
			for (PreProcessorRecordConfigBean bean : getConfigDetails()) {
				headCode = (getRecord()[bean.getIndex()]).trim();
			}
			if (headCode != null && !headCode.equals("") && !headCode.equals("000000")) {
				String selectIternalNumber = "SELECT BHOTN_INTERNAL_NO FROM BILLHEADOTN WHERE ENTITY_CODE=? AND BHOTN_CODE=?";
				dbUtil.reset();
				dbUtil.setMode(DBUtil.PREPARED);
				dbUtil.setSql(selectIternalNumber);
				dbUtil.setString(1, getEntityCode());
				dbUtil.setString(2, headCode);
				ResultSet rset = dbUtil.executeQuery();
				if (rset.next()) {
					inventoryNumber = rset.getString(1);
				}
				dbUtil.reset();

				if (inventoryNumber == null) {
					logger.logError("process() - Error - NO Record in BILLHEADOTN");
					throw new JobsException(JobsErrorCodes.FAILURE_PREPROCESSING_E);
				}
			}
			getProcessContext().put(getSourceColumnName(), inventoryNumber);

		} catch(JobsException ex){
			throw ex;
		}catch (Exception e) {
			logger.logError("process() - Error - " + e.getLocalizedMessage());
			new JobsException(JobsErrorCodes.FAILURE_PREPROCESSING_E);
		} finally {
			dbUtil.reset();
		}
		return getProcessContext();
	}
}
