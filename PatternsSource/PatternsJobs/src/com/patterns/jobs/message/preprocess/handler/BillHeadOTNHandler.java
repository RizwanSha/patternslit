package com.patterns.jobs.message.preprocess.handler;

import java.sql.ResultSet;
import java.util.Map;

import patterns.config.framework.database.utils.DBUtil;

import com.patterns.interfaces.config.manager.bean.PreProcessorRecordConfigBean;
import com.patterns.jobs.exceptions.JobsErrorCodes;
import com.patterns.jobs.exceptions.JobsException;

public class BillHeadOTNHandler extends AbstractPreProcessor {

	@Override
	public Map<String, Object> process() throws JobsException {
		String inventoryNumber = "";
		DBUtil dbUtil = getDbContext().createUtilInstance();
		try {
			String code = "";
			for (PreProcessorRecordConfigBean bean : getConfigDetails()) {
				int index = bean.getIndex();
				code = getRecord()[index];
			}
			if (getOperation().equals("W")) {
				String selectIternalNumber = "SELECT IFNULL(MAX(BHOTN_INTERNAL_NO),0)+1 FROM BILLHEADOTN WHERE ENTITY_CODE=?";
				dbUtil.reset();
				dbUtil.setMode(DBUtil.PREPARED);
				dbUtil.setSql(selectIternalNumber);
				dbUtil.setString(1, getEntityCode());
				ResultSet rset = dbUtil.executeQuery();
				if (rset.next()) {
					inventoryNumber = rset.getString(1);
				}
				dbUtil.reset();

				String insertBillHeadOTN = "INSERT INTO BILLHEADOTN (ENTITY_CODE,BHOTN_CODE,BHOTN_INTERNAL_NO) VALUES (?,?,?)";
				dbUtil.setMode(DBUtil.PREPARED);
				dbUtil.setSql(insertBillHeadOTN);
				dbUtil.setString(1, getEntityCode());
				dbUtil.setString(2, code);
				dbUtil.setString(3, inventoryNumber);
				dbUtil.executeUpdate();
				dbUtil.reset();
			} else if (getOperation().equals("R")) {
				String selectIternalNumber = "SELECT BHOTN_INTERNAL_NO FROM BILLHEADOTN WHERE ENTITY_CODE=? AND BHOTN_CODE=?";
				dbUtil.reset();
				dbUtil.setMode(DBUtil.PREPARED);
				dbUtil.setSql(selectIternalNumber);
				dbUtil.setString(1, getEntityCode());
				dbUtil.setString(2, code);
				ResultSet rset = dbUtil.executeQuery();
				if (rset.next()) {
					inventoryNumber = rset.getString(1);
				}
				dbUtil.reset();

				if (inventoryNumber.equals("")) {
					logger.logError("process() - Error - NO Record in BILLHEADOTN");
					throw new JobsException(JobsErrorCodes.FAILURE_PREPROCESSING_E);
				}
			}
			getProcessContext().put(getSourceColumnName(), inventoryNumber);

		} catch (Exception e) {
			logger.logError("process() - Error - " + e.getLocalizedMessage());
			throw new JobsException(JobsErrorCodes.FAILURE_PREPROCESSING_E);
		} finally {
			dbUtil.reset();
		}
		return getProcessContext();
	}
}
