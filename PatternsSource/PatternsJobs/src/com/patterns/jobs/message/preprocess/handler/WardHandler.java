package com.patterns.jobs.message.preprocess.handler;

import java.util.Map;

import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;

import com.patterns.jobs.exceptions.JobsErrorCodes;
import com.patterns.jobs.exceptions.JobsException;
import com.patterns.jobs.message.handler.RecordConfiguration;

public class WardHandler extends AbstractPreProcessor {

	@Override
	public Map<String, Object> process() throws JobsException {
		DBUtil dbUtil = getDbContext().createUtilInstance();
		try {
			String codeAllowed = "0";
			String femaleWard = getRecord()[2].trim();
			String paWard = getRecord()[3].trim();

			String wardTypeCode = getRecord()[0];
			String desc = getRecord()[1].trim();
			String sql = RegularConstants.EMPTY_STRING;
			if (desc.trim().length() > 0) {
				if (desc.trim().length() > 20) {
					desc = desc.substring(0, 20);
				}
			}

			if (femaleWard.equals("N") && paWard.equals("N")) {
				codeAllowed = "1";
			}
			getProcessContext().put("ALL_LAB_PATCAT_CODE_ALLOWED", codeAllowed);

			String srvcType = "0";
			String genWard = getRecord()[4].trim();
			String splWard = getRecord()[5].trim();
			if (genWard.equals("Y")) {
				srvcType = "G";
			}
			if (splWard.equals("Y")) {
				srvcType = "S";
			}

			getProcessContext().put("WARD_SERVICE_TYPE", srvcType);

			if (getOperation().equals("W")) {
				if (femaleWard.equals("Y")) {
					sql = "INSERT INTO WARDLABPATCAT SELECT ENTITY_CODE,?,LAB_PATCAT_CODE,'0',CURDATE(),'0' FROM LABPATCAT WHERE ENTITY_CODE=? AND APPL_FOR_GENDER NOT IN ('F','A') AND ENABLED='1'";
					dbUtil.setMode(DBUtil.PREPARED);
					dbUtil.setSql(sql);
					dbUtil.setString(1, wardTypeCode);
					dbUtil.setString(2, getEntityCode());
					dbUtil.executeUpdate();
					dbUtil.reset();
				}
				sql = "INSERT INTO WARDLABPATCAT SELECT ENTITY_CODE,?,LAB_PATCAT_CODE,'1',CURDATE(),'0' FROM LABPATCAT WHERE ENTITY_CODE=? AND APPL_FOR_GENDER IN ('F','A') AND ENABLED='1'";
				dbUtil.setMode(DBUtil.PREPARED);
				dbUtil.setSql(sql);
				dbUtil.setString(1, wardTypeCode);
				dbUtil.setString(2, getEntityCode());
				dbUtil.executeUpdate();
				dbUtil.reset();

				sql = "INSERT INTO WARDS (ENTITY_CODE, WARD_CODE, DESCRIPTION, CONCISE_DESCRIPTION, WARD_TYPE_CODE, ENABLED) VALUES(?,?,?,?,?,'1')";
				dbUtil.setMode(DBUtil.PREPARED);
				dbUtil.setSql(sql);
				dbUtil.setString(1, getEntityCode());
				dbUtil.setString(2, wardTypeCode);
				dbUtil.setString(3, getRecord()[1].trim());
				dbUtil.setString(4, desc);
				dbUtil.setString(5, wardTypeCode);
				dbUtil.executeUpdate();
				dbUtil.reset();

				sql = "INSERT INTO WARDLABPATCAT SELECT ENTITY_CODE,?,LAB_PATCAT_CODE,'1',CURDATE(),'0' FROM LABPATCAT WHERE ENTITY_CODE=? AND ? NOT IN (SELECT WARD_TYPE_CODE FROM WARDLABPATCAT)";
				dbUtil.setMode(DBUtil.PREPARED);
				dbUtil.setSql(sql);
				dbUtil.setString(1, wardTypeCode);
				dbUtil.setString(2, getEntityCode());
				dbUtil.setString(3, wardTypeCode);
				dbUtil.executeUpdate();
				dbUtil.reset();

			} else if (getOperation().equals("R")) {
				sql = "UPDATE WARDS SET DESCRIPTION=?, CONCISE_DESCRIPTION=?,WARD_TYPE_CODE=? WHERE ENTITY_CODE=? AND WARD_CODE=?";
				dbUtil.setMode(DBUtil.PREPARED);
				dbUtil.setSql(sql);
				dbUtil.setString(1, getRecord()[1]);
				dbUtil.setString(2, desc);
				dbUtil.setString(3, wardTypeCode);
				dbUtil.setString(4, getEntityCode());
				dbUtil.setString(5, wardTypeCode);
				dbUtil.executeUpdate();
				dbUtil.reset();
			}

			String crOn = getRecord()[10].trim();
			String moOn = getRecord()[14].trim();
			String auOn = getRecord()[12].trim();

			Object enOn = null, modOn = null, autOn = null;

			if (crOn != null
					&& !crOn.trim().equals(RegularConstants.EMPTY_STRING)
					&& !crOn.trim().equals("00000000")) {
				enOn = BackOfficeFormatUtils.getDate(crOn, "ddMMyyyy");
			}

			if (moOn != null
					&& !moOn.trim().equals(RegularConstants.EMPTY_STRING)
					&& !moOn.trim().equals("00000000")) {
				modOn = BackOfficeFormatUtils.getDate(moOn, "ddMMyyyy");
			}
			if (auOn != null
					&& !auOn.trim().equals(RegularConstants.EMPTY_STRING)
					&& !auOn.trim().equals("00000000")) {
				autOn = BackOfficeFormatUtils.getDate(auOn, "ddMMyyyy");
			}

			DTObject input = new DTObject();
			input.set("ENTITY_CODE", getEntityCode());
			input.set("CR_BY", getRecord()[9]);
			input.setObject("CR_ON", enOn);
			input.set("MO_BY", getRecord()[13]);
			input.setObject("MO_ON", modOn);
			input.set("AU_BY", getRecord()[11]);
			input.setObject("AU_ON", autOn);
			input.set("PROGRAM_ID", "MWARD");
			input.set("TABLE", "WARDS");
			input.set("OPERATION", getOperation());
			input.setObject("DBUTIL", getDbContext().createUtilInstance());
			input.set("PK", getEntityCode() + "|" + wardTypeCode);

			RecordConfiguration configuration = new RecordConfiguration();
			configuration.MasterUpdateAuditOperations(input);
		} catch (Exception e) {
			logger.logError("process() - Error - " + e.getLocalizedMessage());
			throw new JobsException(JobsErrorCodes.FAILURE_PREPROCESSING_E);
		} finally {
			dbUtil.reset();
		}
		return getProcessContext();
	}
}