package com.patterns.jobs.message.preprocess.handler;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Map;

import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBUtil;

import com.patterns.jobs.exceptions.JobsErrorCodes;
import com.patterns.jobs.exceptions.JobsException;

public class RoomOTNHandler extends AbstractPreProcessor {

	@Override
	public Map<String, Object> process() throws JobsException {
		String internalRoomNumber = RegularConstants.EMPTY_STRING;
		DBUtil dbUtil = getDbContext().createUtilInstance();
		try {
			String wardCode = getRecord()[0].trim();
			String roomCode = getRecord()[1].trim();
			String roomCodeWithOutZero = Integer.valueOf(roomCode).toString();
			if (getOperation().equals("W")) {
				int count = 0;
				ArrayList<String> roomList = new ArrayList<String>();
				roomList.add(roomCode);
				while (roomCode.charAt(0) == '0') {
					roomList.add(roomCode.substring(1));
					roomCode = roomCode.substring(1);
				}

				String sqlQuery = "SELECT COUNT(1) FROM ROOMS WHERE ENTITY_CODE=? AND WARD_CODE=? AND ROOM_CODE=? ";
				dbUtil.reset();
				dbUtil.setMode(DBUtil.PREPARED);
				dbUtil.setSql(sqlQuery);
				dbUtil.setString(1, getEntityCode());
				dbUtil.setString(2, wardCode);
				dbUtil.setString(3, roomCodeWithOutZero);
				ResultSet rset = dbUtil.executeQuery();
				if (rset.next()) {
					count = rset.getInt(1);
				}
				if (count == 0) {
					String selectInternalRoomNumber = "SELECT IFNULL(MAX(INTERNAL_ROOM_CODE),0)+1 FROM ROOMS WHERE ENTITY_CODE=?";
					dbUtil.reset();
					dbUtil.setMode(DBUtil.PREPARED);
					dbUtil.setSql(selectInternalRoomNumber);
					dbUtil.setString(1, getEntityCode());
					ResultSet rsetInternal = dbUtil.executeQuery();
					if (rsetInternal.next()) {
						internalRoomNumber = rsetInternal.getString(1);
					}
					dbUtil.reset();
					int n = roomList.size() - 1;
					for (int i = 0; i < n; i++) {
						String wardCodeAppendOfArraryOFRoom = wardCode + "|" + roomList.get(i);
						String insertGeneralOTN = "INSERT INTO GENERALOTN (ENTITY_CODE,TABLE_NAME,OLD_KEY_VALUE,NEW_KEY_NUMERIC_VALUE) VALUES (?,?,?,?)";
						dbUtil.setMode(DBUtil.PREPARED);
						dbUtil.setSql(insertGeneralOTN);
						dbUtil.setString(1, getEntityCode());
						dbUtil.setString(2, getSourceTableName());
						dbUtil.setString(3, wardCodeAppendOfArraryOFRoom);
						dbUtil.setString(4, internalRoomNumber);
						dbUtil.executeUpdate();
						dbUtil.reset();
					}
				}
			} else if (getOperation().equals("R")) {
				String Combination = wardCode + "|" + roomCode;
				String selectNewStringValue = "SELECT NEW_KEY_NUMERIC_VALUE FROM GENERALOTN WHERE ENTITY_CODE=? AND TABLE_NAME=? AND OLD_KEY_VALUE=?";
				dbUtil.reset();
				dbUtil.setMode(DBUtil.PREPARED);
				dbUtil.setSql(selectNewStringValue);
				dbUtil.setString(1, getEntityCode());
				dbUtil.setString(2, getSourceTableName());
				dbUtil.setString(3, Combination);
				ResultSet rset = dbUtil.executeQuery();
				if (rset.next()) {
					internalRoomNumber = rset.getString(1);
				}
				dbUtil.reset();
				if (internalRoomNumber.equals("")) {
					logger.logError("process() - Error - NO Record in GENERALOTN");
					throw new JobsException(JobsErrorCodes.FAILURE_PREPROCESSING_E);
				}
			}
			getProcessContext().put(getSourceColumnName(), internalRoomNumber);
			getProcessContext().put("ROOM_CODE", roomCodeWithOutZero);
		} catch (Exception e) {
			logger.logError("process() - Error - " + e.getLocalizedMessage());
			throw new JobsException(JobsErrorCodes.FAILURE_PREPROCESSING_E);
		} finally {
			dbUtil.reset();
		}
		return getProcessContext();
	}
}
