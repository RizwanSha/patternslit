package com.patterns.jobs.message.preprocess.handler;

import java.sql.ResultSet;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Map;

import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.thread.ApplicationContext;

import com.patterns.jobs.exceptions.JobsErrorCodes;
import com.patterns.jobs.exceptions.JobsException;

public class GenerateSerial extends AbstractPreProcessor {
	protected final ApplicationContext context = ApplicationContext.getInstance();

	@Override
	public Map<String, Object> process() throws JobsException {
		String daySerial = RegularConstants.EMPTY_STRING;
		StringBuffer selectSql = new StringBuffer("SELECT ");
		DBUtil dbUtil = getDbContext().createUtilInstance();
		try {
			String departmentCode = getRecord()[0].trim();
			String SIRMCode = getRecord()[1].trim();
			String queueDate = getRecord()[2].trim();
			SimpleDateFormat format = new SimpleDateFormat("ddMMyyyy");
			java.util.Date parsedDate = format.parse(queueDate);

			Timestamp labQdate = new java.sql.Timestamp(parsedDate.getTime());

			if (getOperation().equals("W")) {
				selectSql.append("IFNULL(MAX(LABQ_DAY_SL),0)+1 FROM LABQUEUE WHERE ENTITY_CODE=? AND DEPT_CODE =? AND SIRM_CODE=? AND LABQ_DATE=?");
				dbUtil.reset();
				dbUtil.setMode(DBUtil.PREPARED);
				dbUtil.setSql(selectSql.toString());
				dbUtil.setString(1, getEntityCode());
				dbUtil.setString(2, departmentCode);
				dbUtil.setString(3, SIRMCode);
				dbUtil.setTimestamp(4, labQdate);
				ResultSet rset = dbUtil.executeQuery();
				if (rset.next()) {
					daySerial = rset.getString(1);
				}
				dbUtil.reset();
				getProcessContext().put("LABQ_DAY_SL", daySerial);
			} else {
				if (getRecord()[17].trim().equals("000000")) {
					Timestamp entryDate = null;
					if (getRecord()[6].trim().equals("00000000")) {
						java.util.Date parsedEntry = format.parse("01011900");
						entryDate = new java.sql.Timestamp(parsedEntry.getTime());
					} else {
						java.util.Date parsedEntry = format.parse(getRecord()[6].trim());
						entryDate = new java.sql.Timestamp(parsedEntry.getTime());
					}
					StringBuffer selectsql = new StringBuffer("SELECT LABQ_DAY_SL FROM LABQUEUE WHERE ENTITY_CODE=? AND DEPT_CODE =? AND SIRM_CODE=? AND LABQ_DATE=? AND INPATIENT_OUTPATIENT=? AND IP_NO_YEAR=? AND IP_NO_SERIAL=? AND ENTRY_DATE=? AND ENTRY_SL=? AND DTL_SL=? AND BILL_TYPE_CODE=? AND BILL_YEAR=? AND BILL_SERIAL=? AND BILL_DTL_SL=? AND TEST_DEL_ON_SRC_SYSTEM IS NULL");
					dbUtil.reset();
					dbUtil.setMode(DBUtil.PREPARED);
					dbUtil.setSql(selectsql.toString());
					dbUtil.setString(1, getEntityCode());
					dbUtil.setString(2, departmentCode);
					dbUtil.setString(3, SIRMCode);
					dbUtil.setTimestamp(4, labQdate);
					dbUtil.setString(5, getRecord()[3].trim());
					dbUtil.setString(6, getRecord()[4].trim());
					dbUtil.setString(7, getRecord()[5].trim());
					dbUtil.setTimestamp(8, entryDate);
					dbUtil.setString(9, getRecord()[7].trim());
					dbUtil.setString(10, getRecord()[8].trim());
					dbUtil.setString(11, getRecord()[9].trim());
					dbUtil.setString(12, getRecord()[10].trim());
					dbUtil.setString(13, getRecord()[11].trim());
					dbUtil.setString(14, getRecord()[12].trim());
					ResultSet rset = dbUtil.executeQuery();
					if (rset.next()) {
						daySerial = rset.getString(1);
					}
					dbUtil.reset();
					getProcessContext().put("LABQ_DAY_SL", daySerial);
				} else {
					getProcessContext().put("LABQ_DAY_SL", getRecord()[17].trim());

				}
			}

		} catch (Exception e) {
			logger.logError("process() - Error - " + e.getLocalizedMessage());
			throw new JobsException(JobsErrorCodes.FAILURE_PREPROCESSING_E);
		} finally {
			dbUtil.reset();
		}
		return getProcessContext();
	}
}