package com.patterns.jobs.message.preprocess.handler;

import java.sql.ResultSet;
import java.util.Map;

import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBUtil;

import com.patterns.jobs.exceptions.JobsErrorCodes;
import com.patterns.jobs.exceptions.JobsException;

public class PatientLabInfo extends AbstractPreProcessor {

	@Override
	public Map<String, Object> process() throws JobsException {
		String labPatCatCode = RegularConstants.EMPTY_STRING;
		DBUtil dbUtil = getDbContext().createUtilInstance();
		try {
			String billYear = getRecord()[4];
			String billSl = getRecord()[5];
			String selectLabPatCatCode = "SELECT LAB_PATCAT_CODE FROM PATIENTREG WHERE ENTITY_CODE=?  AND PATIENT_REG_YEAR=? AND PATIENT_REG_SL=?";
			dbUtil.reset();
			dbUtil.setMode(DBUtil.PREPARED);
			dbUtil.setSql(selectLabPatCatCode);
			dbUtil.setString(1, getEntityCode());
			dbUtil.setString(2, billYear);
			dbUtil.setString(3, billSl);
			ResultSet rset = dbUtil.executeQuery();
			if (rset.next()) {
				labPatCatCode = rset.getString(1);
			} else {
				logger.logError("process() - Error - No record in PATIENTREG");
				throw new JobsException(JobsErrorCodes.FAILURE_PREPROCESSING_E);
			}
			dbUtil.reset();
			getProcessContext().put("LAB_PATCAT_CODE", labPatCatCode);
		} catch (Exception e) {
			logger.logError("process() - Error - " + e.getLocalizedMessage());
			throw new JobsException(JobsErrorCodes.FAILURE_PREPROCESSING_E);
		} finally {
			dbUtil.reset();
		}
		return getProcessContext();
	}
}