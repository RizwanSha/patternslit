package com.patterns.jobs.message.preprocess.handler;

import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Map;

import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBUtil;

import com.patterns.jobs.exceptions.JobsErrorCodes;
import com.patterns.jobs.exceptions.JobsException;

public class OutPatientPersonIdHandler extends AbstractPreProcessor {

	@Override
	public Map<String, Object> process() throws JobsException {
		String patRegYear = RegularConstants.EMPTY_STRING;
		String patRegSl = RegularConstants.EMPTY_STRING;
		String personId = RegularConstants.EMPTY_STRING;
		DBUtil dbUtil = getDbContext().createUtilInstance();
		try {
			String billTypeCode = getRecord()[9];
			String billYear = getRecord()[10];
			String billSl = getRecord()[11];
			String selectRegistrationNumber = "SELECT PATIENT_REG_YEAR,PATIENT_REG_SL FROM OUTPATIENTBILL WHERE ENTITY_CODE=? AND BILL_TYPE_CODE=? AND BILL_YEAR=? AND BILL_SERIAL=?";
			dbUtil.reset();
			dbUtil.setMode(DBUtil.PREPARED);
			dbUtil.setSql(selectRegistrationNumber);
			dbUtil.setString(1, getEntityCode());
			dbUtil.setString(2, billTypeCode);
			dbUtil.setString(3, billYear);
			dbUtil.setString(4, billSl);
			ResultSet rset = dbUtil.executeQuery();
			if (rset.next()) {
				patRegYear = rset.getString(1);
				patRegSl = rset.getString(2);

			}
			dbUtil.reset();
			if (patRegYear.equals(RegularConstants.EMPTY_STRING)) {
				logger.logError("process() - Error - No record in OUTPATIENTBILL");
				throw new JobsException(JobsErrorCodes.FAILURE_PREPROCESSING_E);
			}

			String selectPersonId = "SELECT PERSON_ID FROM PATIENTREG WHERE ENTITY_CODE=? AND PATIENT_REG_YEAR=? AND PATIENT_REG_SL=?";
			dbUtil.reset();
			dbUtil.setMode(DBUtil.PREPARED);
			dbUtil.setSql(selectPersonId);
			dbUtil.setString(1, getEntityCode());
			dbUtil.setString(2, patRegYear);
			dbUtil.setString(3, patRegSl);
			ResultSet rset1 = dbUtil.executeQuery();
			if (rset1.next()) {
				personId = rset1.getString(1);
			}
			dbUtil.reset();
			if (personId.equals("")) {
				logger.logError("process() - Error - No record in PATIENTREG");
				new JobsException(JobsErrorCodes.FAILURE_PREPROCESSING_E);
				return getProcessContext();
			}
			getProcessContext().put("PATIENT_REG_YEAR", patRegYear);
			getProcessContext().put("PATIENT_REG_SL", patRegSl);
			getProcessContext().put("PERSON_ID", personId);
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Calendar cal = Calendar.getInstance();
			String currentDateTime = dateFormat.format(cal.getTime()).toString();
			getProcessContext().put("Q_CREATED_DATETIME", currentDateTime);
		} catch (Exception e) {
			logger.logError("process() - Error - " + e.getLocalizedMessage());
			throw new JobsException(JobsErrorCodes.FAILURE_PREPROCESSING_E);
		} finally {
			dbUtil.reset();
		}
		return getProcessContext();
	}
}
