package com.patterns.jobs.message.preprocess.handler;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.quartz.JobExecutionException;

import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.loggers.ApplicationLogger;

import com.patterns.interfaces.config.manager.bean.PreProcessorRecordConfigBean;
import com.patterns.jobs.exceptions.JobsErrorCodes;
import com.patterns.jobs.exceptions.JobsException;

public abstract class AbstractPreProcessor {
	protected final ApplicationLogger logger;

	private Map<String, Object> processContext;
	private String[] record;
	private DBContext dbContext;
	private String entityCode;
	private List<PreProcessorRecordConfigBean> configDetails;
	private String sourceTableName;
	private String sourceColumnName;

	public Map<String, Object> getProcessContext() {
		return processContext;
	}

	public String[] getRecord() {
		return record;
	}

	public DBContext getDbContext() {
		return dbContext;
	}

	public String getEntityCode() {
		return entityCode;
	}

	public List<PreProcessorRecordConfigBean> getConfigDetails() {
		return configDetails;
	}

	public String getSourceColumnName() {
		return sourceColumnName;
	}

	public String getSourceTableName() {
		return sourceTableName;
	}

	public AbstractPreProcessor() {
		logger = ApplicationLogger.getInstance(this.getClass().getSimpleName());
		logger.logDebug("()");
	}
	
	public String getOperation(){
		return (String) getProcessContext().get("OPERATION");
	}

	@SuppressWarnings("unchecked")
	public Map<String, Object> execute(Map<String, Object> context) throws JobsException {
		logger.logDebug("execute() Begin");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			this.processContext = context;
			this.record = (String[]) context.get("RECORD");
			this.entityCode = (String) context.get("ENTITY_CODE");
			this.dbContext = (DBContext) context.get("DBCONTEXT");
			this.configDetails = (List<PreProcessorRecordConfigBean>) context.get("CONFIG_DETAILS");
			this.sourceTableName = (String) context.get("TABLE_NAME");
			this.sourceColumnName = (String) context.get("COLUMN_NAME");

			result = process();
		} catch(JobsException ex){
			throw ex;
		}catch (Exception e) {
			logger.logError("execute() Exception" + e.getLocalizedMessage());
			throw new JobsException(JobsErrorCodes.MSG_HANDLER_PROCCESSING_FAIL_E);
		}
		logger.logDebug("execute() End");
		return result;
	}

	public abstract Map<String, Object> process() throws JobExecutionException, JobsException;

}
